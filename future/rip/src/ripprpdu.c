 /******************************************************************
 * $Id: ripprpdu.c,v 1.81 2017/09/20 13:08:00 siva Exp $
 *
 * Description:This file contains the functions that process
 *             the RIP protocol data unit for RIP request   
 *             and response according to rfc1058, rfc1723.  
 *
 *******************************************************************/
#include "ripinc.h"

    /*|**********************************************************|**
     *** <<<<      local prototypes are delared here      >>>> ***
     **|**********************************************************|* */

PRIVATE INT4        rip_validate_packet
ARG_LIST ((tRip *, tRipInfo *, UINT2, UINT4, UINT4, UINT2, tRipCxt *));
PRIVATE INT4 rip_validate_sending_host ARG_LIST ((UINT2, UINT4, tRipCxt *));
PRIVATE INT4        rip_process_request
ARG_LIST ((tRip *, tRipInfo *, UINT2, UINT4, UINT2, UINT2, tRipCxt *));
PRIVATE INT4        rip_validate_route_info
ARG_LIST ((tRipInfo *, UINT2, UINT1, tRipCxt *));
PRIVATE VOID rip_copy_route_information ARG_LIST ((tRipRtEntry *, tRipInfo *));
PRIVATE INT4        rip_process_response
ARG_LIST ((UINT1, tRipInfo *, UINT2, UINT4, UINT2, UINT4, UINT1, tRipCxt *));
PRIVATE INT4        RipLowPriorityPacket (UINT1);

    /*|**********************************************************|**
     *** <<<<     Global Variable Declarations Start Here >>>> ***
     **|**********************************************************|* */

    /***************************************************************
    >>>>>>>>> PUBLIC Routines of this Module Start Here <<<<<<<<<<
    ***************************************************************/

/*******************************************************************************

Function        :    rip_generate_update_message.

Description        :    This function is called in any of the following cases.

            1. By input processing when a request is seen. In this
            case, the resulting message is sent to only one
            destination.
            2. By the regular routing update. Every (UPDATE TIMER,
            default 30 seconds), a respnose containing the whole
            routing table is sent to every neighbouring gateway.
            3. By triggered updates. Whenever the metric for a route
            is changed, an update is triggered.

            Depending on the preference flag for type of routes, it
            calls procedures to handle static or dynamic route
            propagations. During propagation, it acts according to
            the broadcast status.

Input Parameters    :    u1BcastFlag, a flag indicating the type of broadcast.
            u4DestNet, the destination to which the packets need to
            be sent.
            u2DestUdpPort, the destination UDP port to which the RIP
            packets need to be sent.
            u2If, the interface through which the RIP response need
            to be sent.

Output Parameters   :    None.

Global Variables
Affected        :    1. RipGblCfg.u1TrigDesired.
            2. RipGblCfg.u1TrigUpdFlag.

Return Value        :    Success | Failure.

*******************************************************************************/
EXPORT INT4
rip_generate_update_message (UINT1 u1Version, UINT1 u1BcastFlag,
                             UINT4 u4DestNet, UINT2 u2DestUdpPort, UINT2 u2If,
                             tRipCxt * pRipCxtEntry)
{
    UINT2               u2NoOfRoutes = 0;
    UINT2               i2OffSet =
        (INT2) RIP_OFFSET (tRipRtEntry, RipCxtRtNode);
    INT4                i4SendStat;
    INT4                i4SpaceStat = 0;
    INT4                i4RtCnt;
    static tRipInfoBlk  aRipInfoBlk[RIP_MAX_ROUTES_PER_PKT];
    tRipInfoBlk        *pRipInfoBlk = &(aRipInfoBlk[0]);
    tRipRtEntry        *pRtSend = NULL;
    tRipRtEntry        *pRipRtTmp = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    UINT2               u2RtsToCompose = 0;
    UINT2               u2NoOfPktsSend = 0;
    tRipIfAggRtInfo    *pRipIfAggRtInfo = NULL;
    UINT4               u4Address = 0, u4Mask = 0;
    UINT4               u4PktRt;
    INT1                i1PropagateStaticFlag;
    INT1                i1RtExists = FALSE;
    INT1                i1DefRtExist = FALSE;

    /*
     * Before, we start sending routes outside, we need to make sure that we
     * aren't flooding the triggered update once again (Determined by a certain
     * time duration within which we are not supposed to send triggered updates
     * again (Acc, rfc1812)
     */

    /* Clear bufer with output packets to inforce route filtering */
    MEMSET (pRipInfoBlk, 0, RIP_MAX_ROUTES_PER_PKT * sizeof (tRipInfoBlk));

    if (u1BcastFlag == RIP_TRIG_UPDATE)
    {

        if (pRipCxtEntry->RipGblCfg.u1TrigUpdFlag == RIP_TRIG_TIMER_ACTIVE)
        {

            RIP_TRC (RIP_MOD_TRC,
                     pRipCxtEntry->u4RipTrcFlag,
                     pRipCxtEntry->i4CxtId,
                     CONTROL_PLANE_TRC, RIP_NAME,
                     "Looks like, triggered timer is active \n");
            RIP_TRC (RIP_MOD_TRC,
                     pRipCxtEntry->u4RipTrcFlag,
                     pRipCxtEntry->i4CxtId,
                     CONTROL_PLANE_TRC, RIP_NAME,
                     "We aren't supposed to send trig , "
                     "Updates when it is so setting the desired flag ON \n");

            pRipCxtEntry->RipGblCfg.u1TrigDesired = RIP_TRIG_UPD_DESIRED;
            return RIP_SUCCESS;
        }
        else if (pRipCxtEntry->RipGblCfg.u1TrigDesired ==
                 RIP_TRIG_UPD_NOT_DESIRED)
        {
            pRipCxtEntry->RipTrigUpdTimer.u1Id = RIP_TRIG_UPD_TIMER_ID;

            RIP_START_TIMER (RIP_TIMER_ID,
                             &((pRipCxtEntry->RipTrigUpdTimer).Timer_node),
                             RIP_MIN_TRIG_UPD_TIME);

            /* Update the global trig on status flag */

            pRipCxtEntry->RipGblCfg.u1TrigUpdFlag = RIP_TRIG_TIMER_ACTIVE;
            pRipCxtEntry->RipGblCfg.u1TrigDesired = RIP_TRIG_UPD_DESIRED;
            return RIP_SUCCESS;
        }

    }

    /*
     * The RIP information update broadcast can be for a specific interface or
     * for all interfaces. At each and every interface, RIP configuration can
     * vary and we need to compose the RIP packet for every interface according
     * to their configuration variables status (i.e., Sending type,
     * authentication etc). To avoid scanning through multiple times, we scan
     * once and after MAX_ROUTES are composed, then it is flooded over on all
     * the interfaces depending on the configuration.
     */

    i1PropagateStaticFlag = pRipCxtEntry->RipGblCfg.i1PropagateStatic;

    /* If auto-summary is enabled, generate the update */
    if (RIP_AUTO_SUMMARY_STATUS (pRipCxtEntry) == RIP_AUTO_SUMMARY_ENABLE)
    {
        RipGenerateUpdateForSummaryEnable (u1Version, u1BcastFlag, u2If,
                                           pRipInfoBlk, &u2NoOfRoutes,
                                           u4DestNet, u2DestUdpPort,
                                           pRipCxtEntry, RIP_UPDATE_SEND);
    }
    else
    {
        if (u2If != RIPIF_INVALID_INDEX)
        {
            pRipIfRec = RipGetIfRec (u2If, pRipCxtEntry);
            if (pRipIfRec == NULL)
            {
                RIP_TRC (RIP_MOD_TRC,
                         pRipCxtEntry->u4RipTrcFlag,
                         pRipCxtEntry->i4CxtId,
                         ALL_FAILURE_TRC,
                         RIP_NAME,
                         "Failure in sending the triggered update. \n");

                return RIP_FAILURE;
            }
        }
        u2RtsToCompose = rip_get_routes_to_compose (u2If, pRipCxtEntry);
        /* If auto-summary is disabled */
        switch (u1BcastFlag)
        {
            case RIP_REG_UPDATE:
            case RIP_SPACE_UPDATE:
            case RIP_RES_FOR_REQ:
            case RIP_UNICAST_UPDATE:
                if ((u1BcastFlag == RIP_REG_UPDATE) &&
                    (RIP_SPACING_STATUS (pRipCxtEntry) == RIP_SPACING_ENABLE))
                {
                    RipFindNumOfPktsPerSpace (u2If, u2RtsToCompose,
                                              pRipCxtEntry);
                }
                /*
                 * In the spacing enable case send the update packet 
                 * from the last send route entry
                 * Otherwise Send from the starting.
                 *
                 * */
                if (u1BcastFlag == RIP_SPACE_UPDATE
                    && u2If != RIPIF_INVALID_INDEX)
                {
                    if (pRipIfRec != NULL)
                    {
                        pRtSend = (tRipRtEntry *) pRipIfRec->pRipNextSendRt;
                    }
                }

                else
                {
                    pRtSend = (tRipRtEntry *)
                        TMO_DLL_First (&(pRipCxtEntry->RipCxtRtList));
                }
                while (pRtSend != NULL)
                {
                    pRtSend = (tRipRtEntry *) ((FS_ULONG) pRtSend - i2OffSet);

                    /*
                     *checking default route entry exists in the List.
                     *
                     * */
                    if ((u2If != RIPIF_INVALID_INDEX) &&
                        (pRipIfRec->RipIfaceCfg.u2DefaultMetric !=
                         RIP_NO_DEF_RT_ORIGINATE) && (i1DefRtExist == FALSE))
                    {
                        if (pRtSend->RtInfo.u4DestNet == 0)
                        {
                            /* Default route exist in the list */
                            rip_copy_valid_entries (pRipInfoBlk, &u2NoOfRoutes,
                                                    u2If, pRtSend,
                                                    pRipCxtEntry);
                            u2NoOfRoutes++;
                            i1DefRtExist = TRUE;
                        }
                        else
                        {
                            /* if default route entry does not exist and rip 
                             * default route originate is set, then explicitly 
                             * add a default route to update with the configured
                             * metric.*/
                            RipAddDefRtToUpdate (pRipInfoBlk, &u2NoOfRoutes,
                                                 u2If, pRipCxtEntry);
                            u2NoOfRoutes++;
                            i1DefRtExist = TRUE;

                        }
                    }

                    /*
                     *If the route entry is static route entry
                     *Check static propogate flag is set or 
                     *not.If the flag set propogate the entry
                     *else it is not static route entry 
                     *propogate it.
                     * */
                    if ((pRtSend->RtInfo.u4DestNet != 0) &&
                        (((pRtSend->RtInfo.u2RtProto == CIDR_STATIC_ID) &&
                          (i1PropagateStaticFlag == RIP_PROP_STATIC_ENABLE))
                         || (pRtSend->RtInfo.u2RtProto != CIDR_STATIC_ID)
                         || ((pRtSend->RtInfo.u2RtProto == CIDR_STATIC_ID) &&
                             (pRtSend->RtInfo.i4Metric1 ==
                              (INT4) RIP_INFINITY))))
                    {

                        u4Address = pRtSend->RtInfo.u4DestNet;
                        u4Mask = pRtSend->RtInfo.u4DestMask;
                        pRipIfAggRtInfo = RipCheckIfAgg (u2If, u4Address,
                                                         u4Mask, pRipCxtEntry);

                        if (pRipIfAggRtInfo == NULL)
                        {
                            rip_copy_valid_entries (pRipInfoBlk, &u2NoOfRoutes,
                                                    u2If, pRtSend,
                                                    pRipCxtEntry);
                            u2NoOfRoutes++;
                        }
                        else
                        {
                            /*
                             *Check the aggreation route already added 
                             *to Pkt Info, If it already exists no
                             *need to copy.
                             * */
                            i1RtExists = FALSE;
                            for (i4RtCnt = 0; i4RtCnt < u2NoOfRoutes; i4RtCnt++)
                            {
                                u4PktRt =
                                    RIP_NTOHL (pRipInfoBlk[i4RtCnt].Route.
                                               u4DestNet);
                                if (pRipIfAggRtInfo->u4AggRtAddr == u4PktRt)
                                {
                                    i1RtExists = TRUE;
                                    break;
                                }
                            }
                            if ((i1RtExists != TRUE) &&
                                (pRipIfAggRtInfo->u2Count != 0)
                                && (pRipIfAggRtInfo->u2Metric != RIP_INFINITY)
                                &&
                                (RipComposeRipRtFromIfAggRt
                                 (pRipIfAggRtInfo,
                                  (tRipRtEntry **) (&pRipRtTmp)) !=
                                 RIP_FAILURE))
                            {
                                RipAddAggRtToUpdateMsg (pRipInfoBlk,
                                                        &u2NoOfRoutes,
                                                        (tRipRtEntry *)
                                                        pRipRtTmp,
                                                        pRipCxtEntry);
                                u2NoOfRoutes++;

                            }
                        }
                    }

                    pRtSend = (tRipRtEntry *)
                        TMO_DLL_Next (&(pRipCxtEntry->RipCxtRtList),
                                      (tTMO_DLL_NODE *)
                                      & pRtSend->RipCxtRtNode);
                    /*
                     * One datagram is full,
                     * Send it over the specified interface(s).
                     */
                    if (u2NoOfRoutes == u2RtsToCompose)
                    {

                        if (rip_send_update_datagram (u1Version, u1BcastFlag,
                                                      u4DestNet, u2DestUdpPort,
                                                      pRipInfoBlk, u2If,
                                                      u2RtsToCompose,
                                                      pRipCxtEntry) ==
                            RIP_FAILURE)
                        {
                            if (pRipIfRec != NULL)
                            {
                                (pRipIfRec->RipIfaceStats.
                                 u4RipGenerateUpdate1FailCount)++;
                            }

                            if (pRipRtTmp != NULL)
                            {
                                RIP_ROUTE_FREE (pRipRtTmp);
                            }
                            return (RIP_FAILURE);
                        }
                        u2NoOfPktsSend++;
                        u2NoOfRoutes = 0;
                    }

                    if ((pRipIfRec != NULL) &&
                        ((((u1BcastFlag == RIP_REG_UPDATE) ||
                           (u1BcastFlag == RIP_SPACE_UPDATE)) &&
                          (RIP_SPACING_STATUS (pRipCxtEntry) ==
                           RIP_SPACING_ENABLE)
                          && (pRipIfRec->RipIfaceCfg.u2SpacingTmrVal != 0))))
                    {
                        if ((pRtSend != NULL) &&
                            (pRipIfRec->u2NoOfPktsPerSpace == u2NoOfPktsSend))
                        {
                            pRipIfRec->pRipNextSendRt = pRtSend;
                            TMO_DLL_Init_Node ((tTMO_DLL_NODE *) & pRipIfRec->
                                               RipIfRecNode);
                            TMO_DLL_Add (&(pRipCxtEntry->RipIfSpaceLst),
                                         (tTMO_DLL_NODE *) & pRipIfRec->
                                         RipIfRecNode);
                            RIP_START_TIMER (RIP_TIMER_ID,
                                             &(pRipIfRec->RipSpacingTimer.
                                               TimerNode),
                                             pRipIfRec->RipIfaceCfg.
                                             u2SpacingTmrVal);

                            break;
                        }
                        i4SpaceStat = RIP_LAST_SPACE_UPDATE;
                    }

                }
                break;
            case RIP_TRIG_UPDATE:
                if (rip_send_rip_rt_info (u1Version, u1BcastFlag, u2If,
                                          pRipInfoBlk, &u2NoOfRoutes, u4DestNet,
                                          u2DestUdpPort, IP_PROP_DISABLE,
                                          pRipCxtEntry) == RIP_FAILURE)
                {
                    RIP_TRC (RIP_MOD_TRC,
                             pRipCxtEntry->u4RipTrcFlag,
                             pRipCxtEntry->i4CxtId,
                             ALL_FAILURE_TRC,
                             RIP_NAME,
                             "Failure in sending the unicast update \n");
                    if (pRipIfRec != NULL)
                    {
                        (pRipIfRec->RipIfaceStats.
                         u4RipGenerateUpdate2FailCount)++;
                    }
                    return RIP_FAILURE;
                }
                break;

            default:
                return RIP_FAILURE;

        }                        /* end of switch */
    }                            /* end of else */
    /*
     * After the above phase is over, there may be a possibility of some of the
     * routes being left out, so send the remaining routes.
     */

    if (u1BcastFlag != RIP_UNICAST_UPDATE)
    {
        i4SendStat =
            rip_send_update_datagram (u1Version, u1BcastFlag, u4DestNet,
                                      u2DestUdpPort, pRipInfoBlk, u2If,
                                      u2NoOfRoutes, pRipCxtEntry);
    }
    else
        i4SendStat =
            RipSendUnicastDatagram (pRipInfoBlk, u2NoOfRoutes, pRipCxtEntry);

    if (i4SendStat != RIP_FAILURE)
    {

        /*
         * Send status signifies the successful sending of the update to the
         * lower layers. If a regular update was sent, we need to do further
         * processing regarding IfCounters.
         */

        if (u1BcastFlag == RIP_REG_UPDATE && i4SendStat == RIP_REG_UPDATE)
        {

            rip_decrement_ifcounter_for_routes (pRipCxtEntry);

            /*
             * And also, reset the trig timer active flag, since we are 
             * sending  regular update.
             */
        }
        else
        {
            if (u1BcastFlag == RIP_SPACE_UPDATE &&
                i4SpaceStat == RIP_LAST_SPACE_UPDATE)
            {
                rip_decrement_ifcounter_for_routes (pRipCxtEntry);
            }
        }
        /*
         * If it was a triggered update, we need to take care of timer
         * operations for the same.
         */
        if (pRipRtTmp != NULL)
        {
            RIP_ROUTE_FREE (pRipRtTmp);
        }
        return RIP_SUCCESS;

    }                            /* End of if (i4SendStat != RIP_FAILURE) */

    if (pRipIfRec != NULL)
    {
        (pRipIfRec->RipIfaceStats.u4RipGenerateUpdate4FailCount)++;
    }

    if (pRipRtTmp != NULL)
    {
        RIP_ROUTE_FREE (pRipRtTmp);
    }
    return RIP_FAILURE;
}

/*******************************************************************************

    Function        :   rip_send_update_datagram.

    Description        :   This procedure is responsible for sending the RIP
                updates which exist in the route array table over
                the specified interface. If the specified interface
                is for all the interfaces, the RIP update is sent
                over on all the interfaces.
                On a iface by iface basis, after a set of
                preliminary checks to determine whether the
                interface is valid, enabled or disabled, rip is
                enabled, what type of send status for RIP, it
                prepares to construct the RIP outgoing packet
                accordingly. It applies split horizon with poison
                reverse for all the routes and then delivers the
                packet to a procedure which will enqueue to the UDP
                module.

    Input Parameters    :   1. u1BcastFlag, a parameter indicating the type of
                   broadcast.
                2. u4DestNet, the destination network address to which
                   the update needs to be sent.
                3. u2DestUdpPort, the destination UDP port.
                4. pRipInfoBlk, an array of route informations.
                5. u2If, the interface index through which the
                   update needs to be sent, INVALID INDEX denotes
                   the update is meant for all the interfaces.
                6. u2NoOfRoutes, the number of routes that exist
                   in the route array.

    Output Parameters    :   None.

    Global Variables
    Affected        :   The field, u4SentTrigUpdates is updated for the
                specified interface(s).

    Return Value    :   Success / Failure / Rip_Reg_Update / Rip_Trig_Update

*******************************************************************************/

EXPORT INT4
rip_send_update_datagram (UINT1 u1ResponseVersion,
                          UINT1 u1BcastFlag, UINT4 u4DestNet,
                          UINT2 u2DestUdpPort,
                          tRipInfoBlk * pRipInfoBlk, UINT2 u2If,
                          UINT2 u2NoOfRoutes, tRipCxt * pRipCxtEntry)
{
    tRipIfAggRtInfo    *pRipIfAggRtInfo = NULL;
    tRipRtEntry        *pSummaryRt = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    static tRipInfoBlk  aRipInfoBlk[RIP_MAX_ROUTES_PER_PKT];
    tRipInfoBlk        *RipInfoBlk = &(aRipInfoBlk[0]);
    static tRipPkt      RipPkt;
    UINT1               u1Version, u1Mcastflag;
    UINT2               u2IfIndex, u2NoOfValidRoutes, u2RouteInd;
    INT4                i4Status = RIP_SUCCESS;
    INT4                i4UpdStat = RIP_FAILURE;
    UINT4               u4RemainingTime = 0;
    UINT4               u4SummryAddr = 0;    /* Variable used to track whether 
                                               a particular summary route 
                                               have been updated or not */
    UINT4               u4RtAddress;
    UINT4               u4RtMask;
    UINT4               u4HashIndex = 0;
    UINT2               u2Val = FALSE;
    UINT2               u2Cnt = 0;
    UINT1               u1UpdateSummary = TRUE;
    INT1                i1IsDC = RIP_SUCCESS;
    INT1                i1IfAggFlag = FALSE;
    INT1                i1ReturnVal = 0;
    static tRip        *pRipHdr = &(RipPkt.RipHdr);
    tRipInfo           *pRipInfo = NULL;
    tRtInfo            *pRipRt = NULL;
    tRipRtEntry        *pRipRtEntry = NULL;

    MEMSET (RipInfoBlk, 0, RIP_MAX_ROUTES_PER_PKT * sizeof (tRipInfoBlk));

    /* Added for Unicast update */

    /* The variable u2If can be a single interface or be equal to
     * INVALID_INDEX meaning this RIP send operation is for all the available
     * interfaces.*/

    if (u2NoOfRoutes == 0)
    {
        /* We don't have anything to send, better send a RIP_SUCCESS reply */
        RIP_TRC (RIP_MOD_TRC,
                 pRipCxtEntry->u4RipTrcFlag,
                 pRipCxtEntry->i4CxtId,
                 CONTROL_PLANE_TRC, RIP_NAME,
                 "Suppressing NULL update on all RIP enabled interfaces \n");

        return i4Status;
    }

    /*  Scan through the list of valid interfaces. */
    TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                              pRipIfRec, tRipIfaceRec *)
        {
            pRipHdr = &(RipPkt.RipHdr);
            pRipInfo = (tRipInfo *) & (RipPkt.aRipInfo[0]);
            pRipRt = NULL;

            MEMSET (pRipInfo, 0, sizeof (tRipInfo) * RIP_MAX_ROUTES_PER_PKT);
            i4Status = RIP_SUCCESS;
            u2IfIndex = (UINT2) RIP_GET_IFACE_INDEX (pRipIfRec->IfaceId);

            /* If the update is for a specified interface, then we have to 
             * reassign the u2IfIndex to the specified one.*/
            if (u2If != RIPIF_INVALID_INDEX)
            {
                u2IfIndex = u2If;
                if ((pRipIfRec = RipGetIfRec (u2If, pRipCxtEntry)) == NULL)
                {
                    return (RIP_FAILURE);
                }
            }
            else if (pRipIfRec->pRipCxt != pRipCxtEntry)
            {
                continue;
            }

            i1IsDC =
                RipIsDemandCircuit (pRipIfRec->RipIfaceCfg.u2RipSendStatus);
            /* check if it is WAN iface */
            if ((pRipIfRec->u1Persistence == RIP_WAN_TYPE)
                && (i1IsDC != RIP_SUCCESS))
            {
                continue;
            }

            RIP_GET_REMAINING_TIME (RIP_TIMER_ID,
                                    &(pRipIfRec->RipUpdateTimer.
                                      TimerNode), &u4RemainingTime);
            if ((u1BcastFlag == RIP_TRIG_UPDATE) &&
                (RIP_AUTO_SUMMARY_STATUS (pRipCxtEntry) ==
                 RIP_AUTO_SUMMARY_DISABLE))
            {
                /* Update the interface specifc aggregation and summary 
                 * aggregation routes with proper metric and count */
                for (u2RouteInd = 0; u2RouteInd < u2NoOfRoutes; u2RouteInd++)
                {
                    u4RtAddress =
                        RIP_NTOHL (pRipInfoBlk[u2RouteInd].Route.u4DestNet);
                    u4RtMask =
                        RIP_NTOHL (pRipInfoBlk[u2RouteInd].Route.u4SubnetMask);

                    /* If default route,don't update summary or interface 
                     * specific aggregation */
                    if (u4RtAddress == 0)
                    {
                        continue;
                    }
                    /* Update the interface specific aggregation with the 
                     * proper metric, count */
                    pRipIfAggRtInfo = RipCheckIfAgg (u2IfIndex, u4RtAddress,
                                                     u4RtMask, pRipCxtEntry);
                    if (pRipIfAggRtInfo != NULL)
                    {
                        if (RIP_IF_AGG_RT_STATUS (pRipIfAggRtInfo) !=
                            RIP_IF_AGG_RT_DEF_STATUS)
                        {
                            /* we have already done the updation for this 
                             * interface specific aggregation*/
                            continue;
                        }
                        RipUpdateIfAggRt (pRipIfAggRtInfo, u2IfIndex);
                        RipUpdateIfAggRtAsSinkRt (pRipIfAggRtInfo);
                    }

                    /* Update the summary routes */
                    /*Summary route need to be updated only once for route
                     * change as it is independent of interface.So update
                     * it for the first interface.No need to update for
                     * all the interfaces*/
                    if (u1UpdateSummary == TRUE)
                    {
                        if (u4SummryAddr != (u4RtAddress & RIP_SUMMARY_MASK))
                        {
                            /* Before updating the existing summary route, 
                             * check if it was recently updated.If recently 
                             * updated, then don't update it again.
                             * Continue with the next route*/
                            pSummaryRt = RipCheckSummaryAgg (u4RtAddress,
                                                             u4RtMask,
                                                             pRipCxtEntry);
                            if (pSummaryRt == NULL)
                            {
                                /* Summary route does not exist for this route.
                                 * Add a new summary aggregation for the same*/
                                pSummaryRt = RipAddSummaryRoute (u4RtAddress,
                                                                 pRipCxtEntry);
                                if (pSummaryRt == NULL)
                                {
                                    continue;
                                }
                                if (pSummaryRt->RtInfo.u2IfCounter == 0)
                                {
                                    RipDeleteSummaryRt (pSummaryRt,
                                                        pRipCxtEntry);
                                }
                                else
                                {
                                    RipUpdateSummaryRtAsSinkRt (pSummaryRt,
                                                                pRipCxtEntry);
                                }
                                u4SummryAddr = pSummaryRt->RtInfo.u4DestNet;
                            }
                            else
                            {
                                RipUpdateSummaryAggRoute (pSummaryRt,
                                                          pRipCxtEntry);
                                if (pSummaryRt->RtInfo.u2IfCounter == 0)
                                {
                                    RipDeleteSummarySinkRt (pSummaryRt,
                                                            pRipCxtEntry);
                                    RipDeleteSummaryRt (pSummaryRt,
                                                        pRipCxtEntry);
                                }
                                else
                                {
                                    RipUpdateSummaryRtAsSinkRt (pSummaryRt,
                                                                pRipCxtEntry);
                                }
                                u4SummryAddr = pSummaryRt->RtInfo.u4DestNet;
                            }
                        }
                    }
                }
                u1UpdateSummary = FALSE;
                /* All the interface specific aggregations have been 
                 * updated over this interface have been updated.
                 * Set their status from updated to default status*/
                RipUpdateIfAggRtStatus (u2IfIndex, pRipCxtEntry);
                /*
                 *It won't send the trigger update
                 *Reset Aggregation Status
                 * */
                if ((((pRipIfRec->RipIfaceCfg.u2UpdateInterval) -
                      u4RemainingTime) < RIP_MIN_UPD_TIME)
                    || (u4RemainingTime < RIP_MIN_UPD_TIME))
                {
                    if (pRipIfAggRtInfo != NULL)
                    {
                        pRipIfAggRtInfo->u2Status = RIP_IF_AGG_RT_DEF_STATUS;
                    }
                }
            }

            /*  Let us validate the interface for admin & oper status and
             *  also get the info about outgoing version, address. */
            if (rip_get_send_status
                (u2IfIndex, &u1Version, &u1Mcastflag,
                 pRipCxtEntry) == RIP_FAILURE)
            {
                RIP_TRC_ARG1 (RIP_MOD_TRC,
                              pRipCxtEntry->u4RipTrcFlag,
                              pRipCxtEntry->i4CxtId,
                              CONTROL_PLANE_TRC, RIP_NAME,
                              "Interface with index %d ,"
                              "Sending may have been disabled \n", u2IfIndex);
                RIP_TRC (RIP_MOD_TRC,
                         pRipCxtEntry->u4RipTrcFlag,
                         pRipCxtEntry->i4CxtId,
                         CONTROL_PLANE_TRC,
                         RIP_NAME,
                         "continue with the other interfaces as "
                         "the current interface is invalid \n");

                if (u2IfIndex == u2If)
                {
                    if (pRipIfRec != NULL)
                    {
                        (pRipIfRec->RipIfaceStats.u4RipSentUpdate1FailCount)++;
                    }
                    i4Status = RIP_FAILURE;
                    break;
                }
                else
                    continue;
            }

            /* When spacing is enabled, we are not sure that the changed route
             * belongs to the periodic update's packet or space update's packet.
             * So we should send the changed routes as a triggered update even 
             * if the periodic update was just sent or going to be sent 
             * shortly.*/
            if (RIP_SPACING_STATUS (pRipCxtEntry) == RIP_SPACING_DISABLE)
            {
                if (u1BcastFlag == RIP_TRIG_UPDATE)
                {
                    /* If Update is just sent (OR) we are about to send 
                     * an update within a short time, do not overload 
                     * interface with triggered update.*/
                    if ((((pRipIfRec->RipIfaceCfg.u2UpdateInterval) -
                          u4RemainingTime) < RIP_MIN_UPD_TIME)
                        || (u4RemainingTime < RIP_MIN_UPD_TIME))
                    {
                        /* Update the summary and interface specific routes */
                        RIP_TRC (RIP_MOD_TRC,
                                 pRipCxtEntry->u4RipTrcFlag,
                                 pRipCxtEntry->i4CxtId,
                                 CONTROL_PLANE_TRC, RIP_NAME,
                                 "Trig update is not send.. "
                                 "updating all the aggregations..... \n");
                        continue;
                    }
                }
            }
            else
            {
                if (pRipIfRec->RipIfaceCfg.u1RipAdminFlag == RIP_ADMIN_PASSIVE)
                {
                    /* Silently ignore Triggered update for passive interface */
                    RIP_TRC (RIP_MOD_TRC,
                             pRipCxtEntry->u4RipTrcFlag,
                             pRipCxtEntry->i4CxtId,
                             ALL_FAILURE_TRC, RIP_NAME,
                             "Trig update is not send.. "
                             "on passive interface .... \n");
                    continue;
                }
            }

            RIP_TRC_ARG1 (RIP_MOD_TRC,
                          pRipCxtEntry->u4RipTrcFlag,
                          pRipCxtEntry->i4CxtId,
                          CONTROL_PLANE_TRC, RIP_NAME,
                          "Sending RIP update through Port %d\n", u2IfIndex);

            /* Almost all the checks for sending of RIP response over the 
             * interface have been over and we can very well proceed 
             * with construction of the message. As a first step, 
             * put the RIP header in the outgoing message.
             */
            if (u1BcastFlag == RIP_RES_FOR_REQ)
            {
                u1Version = u1ResponseVersion;
            }
            pRipHdr->u1Command = RIP_RESPONSE;
            pRipHdr->u1Version = u1Version;
            pRipHdr->u2Reserved = 0;
            if (u1Version == RIP_VERSION_2 &&
                (pRipIfRec->RipIfaceCfg.u2AuthType) != RIPIF_NO_AUTHENTICATION)
            {
                /* If authentication is enabled for this interface leave
                 * the first tuple for the construction of the authentication
                 * information. Lets construct that later.*/
                pRipInfo++;
            }

            i1IfAggFlag = FALSE;
            u2NoOfValidRoutes = 0;
            for (u2RouteInd = 0; u2RouteInd < u2NoOfRoutes; u2RouteInd++)
            {
                if (u1BcastFlag == RIP_TRIG_UPDATE)
                {
                    u4RtAddress =
                        RIP_NTOHL (pRipInfoBlk[u2RouteInd].Route.u4DestNet);
                    u4RtMask =
                        RIP_NTOHL (pRipInfoBlk[u2RouteInd].Route.u4SubnetMask);

                    /* Default route */
                    if ((u4RtAddress == 0) && (u4RtMask == 0))
                    {
                        /* Copy the default route if default route 
                         * origination is configured over the interface*/
                        if (pRipIfRec->RipIfaceCfg.u2DefaultMetric !=
                            RIP_NO_DEF_RT_ORIGINATE)
                        {
                            MEMCPY ((VOID *) &(pRipInfo->RipMesg.Route),
                                    (const VOID *) &((pRipInfoBlk[u2RouteInd]).
                                                     Route), sizeof (tRoute));
                            if (RipUpdateRipPktToSend (u1Version, u2IfIndex,
                                                       pRipInfo,
                                                       &pRipInfoBlk[u2RouteInd],
                                                       pRipCxtEntry) !=
                                RIP_FAILURE)
                            {
                                u2NoOfValidRoutes++;
                                pRipInfo++;    /* proceed to the next entries */
                            }

                        }
                        continue;
                    }

                    /* For triggered update case, when summary is disabled,
                     * check if any interface aggregation exist . Copy the 
                     * route info into the outgoing RIP packet after the 
                     * check.*/
                    if (RIP_AUTO_SUMMARY_STATUS (pRipCxtEntry) ==
                        RIP_AUTO_SUMMARY_DISABLE)
                    {
                        if (pRipIfRec->RipIfaceCfg.u2RipSendStatus !=
                            RIPIF_VERSION_1_SND)
                        {
                            /* Check interface specific aggregation is present for
                             * the route.Also check whether the route network 
                             * matches with the local interface network.. */
                            pRipIfAggRtInfo =
                                RipCheckIfAgg (u2IfIndex, u4RtAddress,
                                               u4RtMask, pRipCxtEntry);
                            if (pRipIfAggRtInfo != NULL)
                            {

                                /*Interface specific aggregation exist */
                                if (pRipIfAggRtInfo->u2Status
                                    != RIP_IF_AGG_RT_CHANGED_VAL)
                                {
                                    /* Aggregated route exist but changed bit is 
                                     * not set for aggregated route.So we need 
                                     * not send thies route and this aggregated 
                                     * route in triggered update*/
                                    continue;
                                }

                                /* Changed bit field of if agg route is set.
                                 * Add the interface aggregation to the update.
                                 * Then reset the change bit field.*/
                                pRipIfAggRtInfo->u2Status =
                                    RIP_IF_AGG_RT_DEF_STATUS;

                                if (RipComposeRipRtFromIfAggRt
                                    (pRipIfAggRtInfo,
                                     (tRipRtEntry **) (&pRipRtEntry)) ==
                                    RIP_FAILURE)
                                {
                                    continue;
                                }
                                pRipRt = (tRtInfo *) pRipRtEntry;
                                RipAddAggRtToUpdateMsg (RipInfoBlk,
                                                        &u2Cnt,
                                                        (tRipRtEntry *) pRipRt,
                                                        pRipCxtEntry);
                                RIP_ROUTE_FREE (pRipRt);
                                /* Copy the aggregated route */
                                MEMCPY ((VOID *) &(pRipInfo->RipMesg.Route),
                                        (const VOID *) &(RipInfoBlk[u2RouteInd].
                                                         Route),
                                        sizeof (tRoute));
                                i1IfAggFlag = TRUE;

                            }
                            else
                            {
                                /* Aggregated route is not present.So add the
                                 * route to trig update send.*/
                                MEMCPY ((VOID *) &(pRipInfo->RipMesg.Route),
                                        (const VOID *)
                                        &((pRipInfoBlk[u2RouteInd]).Route),
                                        sizeof (tRoute));
                            }
                        }
                    }
                    else        /* Summary enable */
                    {
                        /* Check whether the route network matches with the 
                         * local interface network.. */
                        if (u4RtAddress == ((pRipIfRec->u4Addr) & u4RtMask))
                        {
                            /* Traverse the rip trie and add all the routes 
                             * falling under this summary aggregation whose 
                             * changed bit is set.
                             * Do not add this summary aggregation to the pkt
                             * to be sent*/
                            RipAddRipRtsForSummaryEnabledTrigUpdate
                                (u4RtAddress, u4RtMask, pRipInfo,
                                 &u2NoOfValidRoutes, u2IfIndex, u1BcastFlag,
                                 &RipPkt, u4DestNet, u2DestUdpPort, &i4UpdStat,
                                 pRipCxtEntry);
                            continue;
                        }
                        else
                        {
                            /* Copy the summary route */
                            MEMCPY ((VOID *) &(pRipInfo->RipMesg.Route),
                                    (const VOID *) &((pRipInfoBlk[u2RouteInd]).
                                                     Route), sizeof (tRoute));
                        }
                    }
                }
                else
                {
                    /* Copy the route information into the outgoing RIP 
                     * packet */
                    MEMCPY ((VOID *) &(pRipInfo->RipMesg.Route),
                            (const VOID *) &((pRipInfoBlk[u2RouteInd]).Route),
                            sizeof (tRoute));
                }

                if (i1IfAggFlag == TRUE)
                {
                    i1IfAggFlag = FALSE;
                    i1ReturnVal = RipUpdateRipPktToSend (u1Version, u2IfIndex,
                                                         pRipInfo, RipInfoBlk,
                                                         pRipCxtEntry);
                }
                else
                {
                    i1ReturnVal = RipUpdateRipPktToSend (u1Version, u2IfIndex,
                                                         pRipInfo,
                                                         &pRipInfoBlk
                                                         [u2RouteInd],
                                                         pRipCxtEntry);
                }
                if (i1ReturnVal == RIP_FAILURE)
                {
                    RIP_TRC (RIP_MOD_TRC,
                             pRipCxtEntry->u4RipTrcFlag,
                             pRipCxtEntry->i4CxtId,
                             CONTROL_PLANE_TRC,
                             RIP_NAME,
                             "Route Not Added To Rip Pkt After Split Horizon ,"
                             "Check \n");
                    continue;
                }
                else
                {
                    u2NoOfValidRoutes++;
                    pRipInfo++;    /* proceed to the next entries        */
                }
            }

            if (u2NoOfValidRoutes != 0)
            {
                /* Send the update formed over the specified interface */
                i4Status = RipSendUpdateOverInterface (u2IfIndex,
                                                       u2NoOfValidRoutes,
                                                       u1BcastFlag,
                                                       &RipPkt,
                                                       u4DestNet, u2DestUdpPort,
                                                       &i4UpdStat,
                                                       pRipCxtEntry);
            }

            /* We don't need to go through all the interfaces if the update is
             * meant only for a specific interface, so exit from this 
             * vicious loop.*/
            if (u2If != RIPIF_INVALID_INDEX)
            {
                u2Val = TRUE;
                break;
            }

        }                        /*  End of interface scan (u2IfIndex)  */

        if (u2Val == TRUE)
        {
            break;
        }
    }
    /* When returning back, we need to return the status also. If the update is
     * for ALL_THE_INTERFACES, then we might as well send the status as 
     * RIP_SUCCESS as we can't keep track status on a per interface basis.
     * If sending is failure for all the interface, we return 
     * failure.Else success is returned.*/
    if (u2If == RIPIF_INVALID_INDEX)
    {
        return i4UpdStat;
    }
    return (i4Status);
}

/**************************************************************************/
/* Function Name     :  RipSendUpdateOverInterface                        */
/*                                                                        */
/* Description       :  This function send the composed update message    */
/*                      over the specified interface                      */
/*                                                                        */
/* Input(s)          :  u2IfIndex : The interface over which updateis to  */
/*                                  send                                  */
/*                      pu2NoOfValidRoutes : Number of valid routes to    */
/*                                           in the composed update       */
/*                      pRipPkt : The rip packet to be sent               */
/*                      u4DestNet : The destination network               */
/*                      u2DestUdpPort : The destination port              */
/*                      pi4UpdStat : The send status                      */
/*                                                                        */
/* Output(s)         :  pu1UpdStat  : The send status                     */
/*                                                                        */
/* Returns           :  RIP_SUCCESS / RIP_FAILURE                         */
/**************************************************************************/
INT1
RipSendUpdateOverInterface (UINT2 u2IfIndex, UINT2 u2NoOfValidRoutes,
                            UINT1 u1BcastFlag, tRipPkt * pRipPkt,
                            UINT4 u4DestNet, UINT2 u2DestUdpPort,
                            INT4 *pi4UpdStat, tRipCxt * pRipCxtEntry)
{
    tRipUnicastNBRS    *pRipUnicastNBR = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    INT4                i4Status = RIP_SUCCESS;
    UINT4               u4BcastAddr;
    UINT4               u4UcastAddr;
    UINT2               u2RipMsgLen = 0;
    UINT1               u1Version;
    UINT1               u1Mcastflag;
    UINT1               u1AuthFlag = RIP_AUTH_NOT_NEEDED;
    UINT4               u4SrcAddress = 0;

    pRipIfRec = RipGetIfRec (u2IfIndex, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return RIP_FAILURE;
    }

    if (rip_get_send_status
        (u2IfIndex, &u1Version, &u1Mcastflag, pRipCxtEntry) == RIP_FAILURE)
    {
        RIP_TRC (RIP_MOD_TRC,
                 pRipCxtEntry->u4RipTrcFlag,
                 pRipCxtEntry->i4CxtId,
                 ALL_FAILURE_TRC,
                 RIP_NAME, "Failed while getting send status\n");
        (pRipIfRec->RipIfaceStats.u4RipSentUpdate2FailCount)++;
        return RIP_FAILURE;
    }

    if ((u1Version == RIP_VERSION_2) && (pRipIfRec != NULL) &&
        ((pRipIfRec->RipIfaceCfg.u2AuthType) != RIPIF_NO_AUTHENTICATION))
    {
        u1AuthFlag = RIP_AUTH_NEEDED;
    }

    /* We have to push the packet out. Before that we need to make sure
     * that we are sending to the correct address type, such as unicast,
     * multicast or broadcast.*/
    if (u1BcastFlag != RIP_RES_FOR_REQ)
    {
        u4BcastAddr = RipIfGetBaddrFromPort (u2IfIndex);

        if (RIPIF_IS_UNNUMBERED (u2IfIndex) == FALSE)
        {
            if (u4BcastAddr == (UINT4) 0)
            {
                i4Status = RIP_FAILURE;
                (pRipIfRec->RipIfaceStats.u4RipSentUpdate3FailCount)++;
                return ((INT1) i4Status);
            }
        }

        u4DestNet = (u1Mcastflag == RIP_MCAST_ON) ?
            RIP_MULTICAST_ADDRESS : u4BcastAddr;
    }

    /* Update should be net directed broadcast, rather General broadcast. */

    if (u4DestNet == RIP_BROADCAST_ADDRESS)
    {
        u4BcastAddr = RipIfGetBaddrFromPort (u2IfIndex);

    }
    /* Calculate the length of the message that is getting passed
     * depending upon the version.*/
    u2RipMsgLen = (UINT2) (((u2NoOfValidRoutes) * RIP_INFO_LEN) + RIP_HDR_LEN);
    if (u1AuthFlag == RIP_AUTH_NEEDED)
    {
        /* Reset to the default  */
        u1AuthFlag = RIP_AUTH_NOT_NEEDED;
    }

    /* Construct the authentication tuple here. */
    if ((u1Version == RIP_VERSION_2) && (pRipIfRec != NULL)
        && (pRipIfRec->RipIfaceCfg.u2AuthType != RIPIF_NO_AUTHENTICATION))
    {
        i4Status = rip_construct_auth_info ((UINT1 *) pRipPkt, &u2RipMsgLen,
                                            u2IfIndex, pRipCxtEntry);
    }

    /* Send the RIP packet down the stack over this interface. */
    if (i4Status == RIP_SUCCESS)
    {
        /* If We need to send the Unicast update, we need to duplicate
         * the buffer.*/
        RIP_SLL_Scan (&(pRipIfRec->RipUnicastNBRS),
                      pRipUnicastNBR, tRipUnicastNBRS *)
        {
            u4UcastAddr = pRipUnicastNBR->u4UnicastNBR;
            if (rip_task_udp_send
                ((UINT1 *) pRipPkt, UDP_RIP_PORT, u4UcastAddr,
                 u2DestUdpPort, u2RipMsgLen,
                 (UINT1) (RIP_TTL (u1BcastFlag)), u2IfIndex,
                 u1BcastFlag, 0, pRipCxtEntry) == RIP_FAILURE)
            {
                (pRipIfRec->RipIfaceStats.u4RipSentUpdate4FailCount)++;
                break;
            }
            /*
             *For V1/v2 compitable sending the v1 packets
             *and v2 packets.
             *
             * */
            if ((u1Version == RIP_VERSION_2) && (u1Mcastflag == RIP_MCAST_OFF))
            {
                pRipPkt->RipHdr.u1Version = RIP_VERSION_1;
                if (rip_task_udp_send
                    ((UINT1 *) pRipPkt, UDP_RIP_PORT, u4UcastAddr,
                     u2DestUdpPort, u2RipMsgLen,
                     (UINT1) (RIP_TTL (u1BcastFlag)), u2IfIndex, u1BcastFlag,
                     u4SrcAddress, pRipCxtEntry) == RIP_FAILURE)
                {
                    (pRipIfRec->RipIfaceStats.u4RipSentUpdate4FailCount)++;
                    break;
                }
                /*
                 *For v1/v2 sends the Unicast packet with V1.
                 *To send Normal Updation Packets  
                 *resetting the version.
                 */
                pRipPkt->RipHdr.u1Version = u1Version;
            }
        }

        i4Status = rip_task_udp_send ((UINT1 *) pRipPkt,
                                      UDP_RIP_PORT, u4DestNet,
                                      u2DestUdpPort, u2RipMsgLen,
                                      (UINT1) (RIP_TTL
                                               (u1BcastFlag)), u2IfIndex,
                                      u1BcastFlag, u4SrcAddress, pRipCxtEntry);
        if (i4Status == RIP_SUCCESS)
        {
            if (u1BcastFlag == RIP_TRIG_UPDATE)
            {
                (pRipIfRec->RipIfaceStats.u4SentTrigUpdates)++;
            }
            else
            {
                /* Incrementing number of periodic updates sent on the
                 * interface */
                if ((u1BcastFlag == RIP_REG_UPDATE) ||
                    (u1BcastFlag == RIP_LAST_SPACE_UPDATE))
                {
                    pRipCxtEntry->u4RipPeriodicUpdates++;
                    (pRipIfRec->RipIfaceStats.u4SentPeriodicUpdates)++;
                }
            }

            /* We are keeping two different status variables, since we want
             * to differentiate between update for all interfaces and a
             * single interface. In case of all interfaces, we don't want to
             * return RIP_FAILURE message which might occur for some
             * interfaces.*/
            *pi4UpdStat =
                (u1BcastFlag ==
                 RIP_REG_UPDATE) ? RIP_REG_UPDATE : RIP_TRIG_UPDATE;
            i4Status = *pi4UpdStat;    /* i think ths is not needed */
        }
    }
    return ((INT1) i4Status);
}

/**************************************************************************/
/* Function Name     :  RipUpdateRipPktToSend                             */
/*                                                                        */
/* Description       :  This function update the rip packet to be sent    */
/*                      based on the version and split horizon check      */
/*                                                                        */
/* Input(s)          :  u1Version : Version configured over the interface */
/*                      u2IfIndex : The interface index                   */
/*                      pRipInfo : Route to be sent                       */
/*                      pRipInfoBlk : The route with ifcounter and        */
/*                                    IfIndex information                 */
/*                                                                        */
/* Output(s)         :  pRipInfo : The updated route after split horizon  */
/*                                 check                                  */
/*                                                                        */
/* Returns           :  RIP_SUCCESS / RIP_FAILURE                         */
/**************************************************************************/
INT1
RipUpdateRipPktToSend (UINT1 u1Version, UINT2 u2IfIndex,
                       tRipInfo * pRipInfo, tRipInfoBlk * pRipInfoBlk,
                       tRipCxt * pRipCxtEntry)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    UINT4               u4RetVal = 0;
    UINT4               u4SubNetMask;
    UINT4               u4DefaultMetric;

    RIP_ADDRESS_FAMILY (pRipInfo) = RIP_HTONS (INET_ADDRESS_FAMILY);
    pRipIfRec = RipGetIfRec (u2IfIndex, pRipCxtEntry);

    if (pRipIfRec == NULL)
    {
        return RIP_FAILURE;
    }

    /* Apply Split Horizon With Poisoned Reverse. i.e., if 
     * the interface through which this route information is 
     * learnt is the same as the interface we are trying to 
     * send over, then set the cost to infinity. But, before 
     * this, we need to make checks for whether we can do
     * poisoning of routes based on a set of parameters 
     * (IfCounter etc.)*/

    /* If aggregated route, if atleast one subnet route is 
     * learned over a different interface, send the aggregated 
     * route in update msg.*/
    if (pRipInfoBlk->u2If == RIPIF_INVALID_INDEX)
    {
        /* it is an aggregatedroute */
        if (RipSetAggRtSplitHorizonStatus (pRipInfo, u2IfIndex, pRipCxtEntry) ==
            RIP_FAILURE)
        {
            RIP_TRC (RIP_MOD_TRC,
                     pRipCxtEntry->u4RipTrcFlag,
                     pRipCxtEntry->i4CxtId,
                     ALL_FAILURE_TRC, RIP_NAME,
                     "FAILED while setting metric for Aggregated route");
            /* We won't be sending the route through this interface */

            (pRipIfRec->RipIfaceStats.u4RipUpdateRipPktToSend1FailCount)++;
            return RIP_FAILURE;
        }
    }

    if (pRipInfoBlk->u2If == u2IfIndex)
    {
        if ((u4RetVal = (UINT4) rip_is_poison_reverse_applicable (pRipInfoBlk,
                                                                  u2IfIndex,
                                                                  pRipCxtEntry))
            == RIP_POIS_REV_APPLICABLE)
        {
            RIP_METRIC (pRipInfo) = RIP_HTONL (RIP_INFINITY);
            RIP_NEXT_HOP (pRipInfo) = 0;
        }
        else if (u4RetVal == RIP_POIS_REV_NOT_APPLICABLE)
        {
            /* Apply split horizon, and so we won't be sending this
             * route through this interface.*/

            (pRipIfRec->RipIfaceStats.u4RipUpdateRipPktToSend2FailCount)++;
            return RIP_FAILURE;
        }
    }
    else
    {
        /* in case next hop is not directly reachable
         * on this interface*/
        if (pRipIfRec != NULL)
        {
            if (RIPIF_IS_UNNUMBERED (u2IfIndex) == TRUE)
            {
                RIP_NEXT_HOP (pRipInfo) =
                    RIP_HTONL (pRipIfRec->RipIfaceCfg.u4SrcAddr);
            }
            else
            {
                RIP_NEXT_HOP (pRipInfo) = RIP_HTONL (pRipIfRec->u4Addr);
            }
        }
    }

    /* Here, we need to check for whether the route is a 
     * default route originated from this interface, and if so,
     * the metric should be put to the configured value for
     * default routes. If the deafult route metric is 0, this 
     * route will not be propagated. A default route via another 
     * route may be propagated.
     * We have to mind a fact that all the PDU entries are 
     * already in the network byte order.*/

    if (((RIP_NTOHL (RIP_DEST_NET (pRipInfo)) == 0)
         && (RIP_NTOHL (RIP_SUBNET_MASK (pRipInfo)) == 0))
        && (u4RetVal != RIP_POIS_REV_APPLICABLE))
    {
        if ((pRipIfRec != NULL)
            && (pRipIfRec->RipIfaceCfg.u2DefaultMetric != 0))
        {
            /* Assign the metric field in the tRtRipInfo to the
             * configured metric.*/
            u4DefaultMetric = (UINT4) pRipIfRec->RipIfaceCfg.u2DefaultMetric;
            RIP_METRIC (pRipInfo) = RIP_HTONL (u4DefaultMetric);
            UNUSED_PARAM (u4DefaultMetric);
        }
        else
        {
            (pRipIfRec->RipIfaceStats.u4RipUpdateRipPktToSend3FailCount)++;
            return RIP_FAILURE;
        }
    }

    /* Depending on the version, we may have to zero out some of the
     * Reserved entries in the route information for version 1.*/
    if (u1Version == RIP_VERSION_1)
    {
        /* We better not send any host routes to RIP1 interface */
        u4SubNetMask = rip_task_ip_get_net_mask (u2IfIndex,
                                                 RIP_NTOHL
                                                 (RIP_DEST_NET (pRipInfo)), -1);

        /* As destination is checked against zero, no need to 
         * convert to host order.
         * The routes (including aggregate routes)
         * are not sent outside when
         * their mask is more specific than the natural class based
         * mask.
         * when mask is less than the natural mask
         * we will send it assuming that natural mask is
         * covered by the bigger mask.
         */
        if ((RIP_DEST_NET (pRipInfo) != 0) &&
            (u4SubNetMask < RIP_NTOHL (RIP_SUBNET_MASK (pRipInfo))))
        {
            (pRipIfRec->RipIfaceStats.u4RipUpdateRipPktToSend4FailCount)++;
            return RIP_FAILURE;
        }
        MEMSET (RIP_ROUTE_TAG (pRipInfo), RIP_NULL_RTAG,
                sizeof (RIP_ROUTE_TAG (pRipInfo)));
        RIP_SUBNET_MASK (pRipInfo) = 0;
        RIP_NEXT_HOP (pRipInfo) = 0;
    }
    return RIP_SUCCESS;
}

/**************************************************************************/
/* Function Name     :  RipAddRipRtsForSummaryEnabledTrigUpdate           */
/*                                                                        */
/* Description       :  This function adds all the rip route falling      */
/*                      the specified summary aggregation range for       */
/*                      triggered update to the info block.If the number  */
/*                      of routes exeeds the maximum routes to be         */
/*                      composed over the specified interface, it will    */
/*                      send the packet over the over the interface after */
/*                      performing all the require checks                 */
/*                                                                        */
/* Input(s)          :  u4Address : The summary aggregation address       */
/*                      u4Mask : Mask                                     */
/*                      pRipInfo : Route to be sent                       */
/*                                  send                                  */
/*                      pu2NoOfValidRoutes : Number of valid routes to    */
/*                                           in the composed update       */
/*                      u2IfIndex : The interface over which update is to */
/*                      u1BcastFlag : Flag indicating the type of brdcst  */
/*                      pRipPkt : The rip packet to be sent               */
/*                      u4DestNet : The destination network               */
/*                      u2DestUdpPort : The destination port              */
/*                      pi4UpdStat : The send status                      */
/*                                                                        */
/* Output(s)         :  pu1UpdStat  : The send status                     */
/*                                                                        */
/* Returns           :  None                                              */
/**************************************************************************/

VOID
RipAddRipRtsForSummaryEnabledTrigUpdate (UINT4 u4Address, UINT4 u4Mask,
                                         tRipInfo * pRipInfo,
                                         UINT2 *pu2NoOfValidRoutes,
                                         UINT2 u2IfIndex,
                                         UINT1 u1BcastFlag,
                                         tRipPkt * pRipPkt,
                                         UINT4 u4DestNet,
                                         UINT2 u2DestUdpPort, INT4 *pi4UpdStat,
                                         tRipCxt * pRipCxtEntry)
{
    tRipRtEntry        *pRipRt = NULL;
    tRipRtEntry        *pBestRt = NULL;
    static tRipInfoBlk  aRipInfoBlk[RIP_MAX_ROUTES_PER_PKT];
    tRipInfoBlk        *RipInfoBlk = &(aRipInfoBlk[0]);
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipInfo           *pRtBase = pRipInfo - (*pu2NoOfValidRoutes);
    tInputParams        InParams;
    tOutputParams       OutParams;
    INT4                i4OutCome = TRIE_FAILURE;
    INT4                i4AppEntry;
    UINT4               au4Indx[2];    /* Index for Trie is made of 2 members */
    UINT4               u4RtAddr = 0;
    UINT4               u4RtMask = 0;
    INT4                i4LowMetric = 0xFFFF;
    UINT2               u2Count = 0;
    UINT2               u2RtsToCompose = 0;
    UINT1               u1Version = RIP_VERSION_0;
    VOID               *apAppSpecInfo[MAX_ROUTING_PROTOCOLS];

    RIP_TRC (RIP_MOD_TRC,
             pRipCxtEntry->u4RipTrcFlag,
             pRipCxtEntry->i4CxtId,
             CONTROL_PLANE_TRC,
             RIP_NAME,
             "Adding Rip Routes To Updates For Summary Enable Triggered,"
             "Updates\n");

    MEMSET (RipInfoBlk, 0, RIP_MAX_ROUTES_PER_PKT * sizeof (tRipInfoBlk));
    pRipIfRec = RipGetIfRec (u2IfIndex, pRipCxtEntry);
    if (pRipIfRec != NULL)
    {
        if (pRipIfRec->RipIfaceCfg.u2RipSendStatus == RIPIF_VERSION_1_SND)
        {
            u1Version = RIP_VERSION_1;
        }
        else
        {
            u1Version = RIP_VERSION_2;
        }
    }
    u2RtsToCompose = rip_get_routes_to_compose (u2IfIndex, pRipCxtEntry);

    /* HTONL is needed as we store in trie in the network byte order */
    au4Indx[0] = RIP_HTONL (u4Address);
    au4Indx[1] = RIP_HTONL (u4Mask);
    InParams.pRoot = pRipCxtEntry->pRipRoot;
    InParams.i1AppId = ALL_ROUTING_PROTOCOL;
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    OutParams.Key.pKey = NULL;
    MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * sizeof (tRtInfo *));
    OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;
    OutParams.pObject1 = NULL;
    OutParams.pObject2 = NULL;

    i4OutCome = TrieSearchEntry ((&InParams), (&OutParams));
    if (i4OutCome != TRIE_FAILURE)
    {
        for (i4AppEntry = 0; i4AppEntry < MAX_ROUTING_PROTOCOLS; i4AppEntry++)
        {
            if ((((tRipRtEntry **) (OutParams.pAppSpecInfo))[i4AppEntry]
                 != NULL) && (i4AppEntry != (OTHERS_ID - 1)))
            {
                pBestRt = ((tRipRtEntry **) OutParams.pAppSpecInfo)[i4AppEntry];
                if ((i4AppEntry == (CIDR_LOCAL_ID - 1)) ||
                    (i4AppEntry == (CIDR_STATIC_ID - 1)))
                {
                    pRipRt = pBestRt;
                    break;
                }
                if (i4LowMetric > pBestRt->RtInfo.i4Metric1)
                {
                    i4LowMetric = pBestRt->RtInfo.i4Metric1;
                    pRipRt = pBestRt;
                }
            }
        }

        if (pRipRt != NULL)
        {
            /* Triggered update */
            if (RIP_RT_CHANGED_STATUS (pRipRt) != 0)
            {
                rip_copy_valid_entries (RipInfoBlk, &u2Count,
                                        u2IfIndex, (VOID *) pRipRt,
                                        pRipCxtEntry);
                RIP_RT_RESET_CHANGED_STATUS (pRipRt);
                /* Copy the route to rip packet to be sent */
                MEMCPY ((VOID *) &(pRipInfo->RipMesg.Route),
                        (const VOID *) &(RipInfoBlk[u2Count].Route),
                        sizeof (tRoute));
                /* Update the route info in the rip packet based on 
                 * version, split horizon status etc*/
                if (RipUpdateRipPktToSend (u1Version, u2IfIndex, pRipInfo,
                                           RipInfoBlk,
                                           pRipCxtEntry) == RIP_SUCCESS)
                {
                    RIP_TRC (RIP_MOD_TRC,
                             pRipCxtEntry->u4RipTrcFlag,
                             pRipCxtEntry->i4CxtId,
                             CONTROL_PLANE_TRC,
                             RIP_NAME,
                             "Successfully Updated The Rip Pkt To Be Sent\n");
                    (*pu2NoOfValidRoutes)++;
                    pRipInfo++;
                }
            }
        }
    }

    if (*pu2NoOfValidRoutes == (u2RtsToCompose - 1))
    {
        /* One complete datagram is full.Send the rip update over the specified 
         * interface*/
        RipSendUpdateOverInterface (u2IfIndex, *pu2NoOfValidRoutes,
                                    u1BcastFlag, pRipPkt,
                                    u4DestNet, u2DestUdpPort, pi4UpdStat,
                                    pRipCxtEntry);
        RIP_TRC (RIP_MOD_TRC,
                 pRipCxtEntry->u4RipTrcFlag,
                 pRipCxtEntry->i4CxtId,
                 CONTROL_PLANE_TRC, RIP_NAME,
                 "Successfully Send Rip Pkt Over The Specified Interface\n");
        (*pu2NoOfValidRoutes) = 0;
        pRipInfo = pRtBase;
    }

    i4OutCome = TRIE_SUCCESS;
    while (i4OutCome == TRIE_SUCCESS)
    {
        i4OutCome = TrieGetNextEntry ((&InParams), (&OutParams));
        if (i4OutCome == TRIE_FAILURE)
        {
            break;
        }

        for (i4AppEntry = 0; i4AppEntry < MAX_ROUTING_PROTOCOLS; i4AppEntry++)
        {
            if (((tRipRtEntry **) (OutParams.pAppSpecInfo))[i4AppEntry] != NULL)
            {
                (pRipRt) =
                    ((tRipRtEntry **) (OutParams.pAppSpecInfo))[i4AppEntry];
                if (i4AppEntry != (OTHERS_ID - 1))
                {
                    break;
                }
            }
        }

        if (pRipRt != NULL)
        {
            u4RtAddr = pRipRt->RtInfo.u4DestNet;
            u4RtMask = pRipRt->RtInfo.u4DestMask;

            if (pRipRt->RtInfo.u2RtProto == OTHERS_ID)
            {
                au4Indx[0] = RIP_HTONL (u4RtAddr);
                au4Indx[1] = RIP_HTONL (u4RtMask);
                MEMSET (apAppSpecInfo, 0,
                        MAX_ROUTING_PROTOCOLS * sizeof (VOID *));
                continue;
            }
            /* we have exited the aggregation range */
            if (u4Address != (u4RtAddr & u4Mask))
            {
                break;
            }

            if (RIP_RT_CHANGED_STATUS (pRipRt) != 0)
            {
                rip_copy_valid_entries (RipInfoBlk, &u2Count,
                                        u2IfIndex, (VOID *) pRipRt,
                                        pRipCxtEntry);
                RIP_RT_RESET_CHANGED_STATUS (pRipRt);
                MEMCPY ((VOID *) &(pRipInfo->RipMesg.Route),
                        (const VOID *) &(RipInfoBlk[u2Count].Route),
                        sizeof (tRoute));
                if (RipUpdateRipPktToSend
                    (u1Version, u2IfIndex, pRipInfo, RipInfoBlk,
                     pRipCxtEntry) == RIP_SUCCESS)
                {
                    (*pu2NoOfValidRoutes)++;
                    pRipInfo++;
                    RIP_TRC (RIP_MOD_TRC,
                             pRipCxtEntry->u4RipTrcFlag,
                             pRipCxtEntry->i4CxtId,
                             CONTROL_PLANE_TRC,
                             RIP_NAME,
                             "Successfully Updated The Rip Pkt To Be Sent\n");
                }

                if (*pu2NoOfValidRoutes == (u2RtsToCompose - 1))
                {
                    /* One complete datagram is full.Send the rip 
                     * update over the specified interface*/
                    RipSendUpdateOverInterface (u2IfIndex,
                                                *pu2NoOfValidRoutes,
                                                u1BcastFlag,
                                                pRipPkt,
                                                u4DestNet, u2DestUdpPort,
                                                pi4UpdStat, pRipCxtEntry);
                    (*pu2NoOfValidRoutes) = 0;

                    pRipInfo = pRtBase;

                    RIP_TRC (RIP_MOD_TRC,
                             pRipCxtEntry->u4RipTrcFlag,
                             pRipCxtEntry->i4CxtId,
                             CONTROL_PLANE_TRC,
                             RIP_NAME,
                             "Rip Pkt Send Successfully Over The Specified ,"
                             "Interface\n");
                }
            }
        }
        au4Indx[0] = RIP_HTONL (u4RtAddr);
        au4Indx[1] = RIP_HTONL (u4RtMask);
        MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * sizeof (tRtInfo *));
    }

    RIP_TRC (RIP_MOD_TRC,
             pRipCxtEntry->u4RipTrcFlag,
             pRipCxtEntry->i4CxtId,
             CONTROL_PLANE_TRC,
             RIP_NAME,
             "Rip Routes Successfully Added To Updates For Summary Enable,"
             "Triggered Updates\n");
}

/*******************************************************************************

    Function        :   RipSendUnicastDatagram.

    Description     :   This procedure is responsible for sending the unicast RIP
                           updates for passive interfaces. This function 
                           is designed for neighbor unicast feature. As we don't associate
                           a timer with each interface. There is a common passive timer 
                           running. Whenever that timer fires this function is called.
                           This function sends the packet to all configured neighbors on the 
                           passive interfaces.
                                                                           
                          

    Input Parameters    : 1. pRipInfoBlk, an array of route informations.
                          2. u2NoOfRoutes, the number of routes that exist
                                in the route array.

    Output Parameters    :   None.

    Global Variables
    Affected        :   

    Return Value    :   Success / Failure / Rip_Pass_Update

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = RipSendUnicastDatagram
    $$TRACE_PROCEDURE_LEVEL = INTMD
*******************************************************************************/

EXPORT INT4
RipSendUnicastDatagram (tRipInfoBlk * pRipInfoBlk, UINT2 u2NoOfRoutes,
                        tRipCxt * pRipCxtEntry)
{
    UINT1               u1Version, u1Mcastflag;
    UINT2               u2IfIndex, u2RipMsgLen, u2NoOfValidRoutes, u2RouteInd;
    INT4                i4Status = RIP_SUCCESS;
    static tRipPkt      RipPkt;

    tRipUnicastNBRS    *pRipUnicastNBR = NULL;
    UINT4               u4RetVal;
    UINT4               u4DestNet;
    tRipIfaceRec       *pRipIfRec = NULL;
    UINT4               u4HashIndex;
    UINT1               u1AuthFlag = RIP_AUTH_NOT_NEEDED;
    INT1                i1IsDC = RIP_SUCCESS;
    static tRip        *pRipHdr = &(RipPkt.RipHdr);
    tRipInfo           *pRipInfo = NULL;

    if (u2NoOfRoutes == 0)
    {

        /*
         * We don't have anything to send, better send a RIP_SUCCESS reply.
         */

        RIP_TRC (RIP_MOD_TRC,
                 pRipCxtEntry->u4RipTrcFlag,
                 pRipCxtEntry->i4CxtId,
                 CONTROL_PLANE_TRC, RIP_NAME, "Suppressing NULL update \n");

        return i4Status;
    }

    /*
     * Scan through the list of valid interfaces.
     */
    TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                              pRipIfRec, tRipIfaceRec *)
        {
            if (pRipCxtEntry != pRipIfRec->pRipCxt)
            {
                continue;
            }
            u1AuthFlag = RIP_AUTH_NOT_NEEDED;
            pRipHdr = &(RipPkt.RipHdr);
            pRipInfo = &(RipPkt.aRipInfo[0]);

            u2IfIndex = (UINT2) RIP_GET_IFACE_INDEX (pRipIfRec->IfaceId);
            i1IsDC =
                RipIsDemandCircuit (pRipIfRec->RipIfaceCfg.u2RipSendStatus);
            /* Unicast update is not done for WAN interface */
            if (((pRipIfRec->u1Persistence == RIP_WAN_TYPE)
                 && (i1IsDC != RIP_SUCCESS))
                || (pRipIfRec->RipIfaceCfg.u1RipAdminFlag != RIP_ADMIN_PASSIVE)
                || (pRipIfRec->RipIfaceCfg.u2AdminStatus != RIPIF_ADMIN_ACTIVE))
            {
                continue;
            }
            /*
             * Let us validate the interface for admin & oper status and also get
             * the info about outgoing version, address.
             */

            if (rip_get_send_status
                (u2IfIndex, &u1Version, &u1Mcastflag,
                 pRipCxtEntry) == RIP_FAILURE)
            {

                RIP_TRC_ARG1 (RIP_MOD_TRC,
                              pRipCxtEntry->u4RipTrcFlag,
                              pRipCxtEntry->i4CxtId,
                              CONTROL_PLANE_TRC, RIP_NAME,
                              "Interface with index %d ,"
                              "Sending may have been disabled \n", u2IfIndex);
                RIP_TRC (RIP_MOD_TRC,
                         pRipCxtEntry->u4RipTrcFlag,
                         pRipCxtEntry->i4CxtId,
                         CONTROL_PLANE_TRC,
                         RIP_NAME,
                         "continue with the other interfaces as "
                         "the current interface is invalid \n");

                continue;
            }

            /* The interface sent status should be RipV1Compatible */
            /*
             * Almost all the checks for sending of RIP response over the 
             * interface
             * have been over and we can very well proceed with construction of 
             * the
             * message. As a first step, put the RIP header in the outgoing mess
             * age.
             */
            pRipHdr->u1Command = RIP_RESPONSE;
            pRipHdr->u1Version = u1Version;
            pRipHdr->u2Reserved = 0;
            if (u1Version == RIP_VERSION_2 &&
                pRipIfRec->RipIfaceCfg.u2AuthType != RIPIF_NO_AUTHENTICATION)
            {

                /* 
                 * If authentication is enabled for this interface leave
                 * the first tuple for the construction of the authentication
                 * information. Lets construct that later.
                 */
                u1AuthFlag = RIP_AUTH_NEEDED;
                /* For later stage reference  */
                pRipInfo++;
            }

            u2NoOfValidRoutes = 0;

            for (u2RouteInd = 0; u2RouteInd < u2NoOfRoutes; u2RouteInd++)
            {

                /* Copy the route information into the outgoing RIP packet */

                MEMCPY ((VOID *) &(pRipInfo->RipMesg.Route),
                        (const VOID *) &((pRipInfoBlk[u2RouteInd]).Route),
                        sizeof (tRoute));

                RIP_ADDRESS_FAMILY (pRipInfo) = RIP_HTONS (INET_ADDRESS_FAMILY);

                /*
                 * Apply Split Horizon With Poisoned Reverse. i.e., if the interface
                 * through which this route information is learnt is the same as the
                 * interface we are trying to send over, then set the cost to
                 * infinity.
                 * But, before this, we need to make checks for whether we can do
                 * poisoning of routes based on a set of parameters (IfCounter etc.)
                 */

                if ((pRipInfoBlk[u2RouteInd]).u2If == u2IfIndex)
                {
                    if ((u4RetVal =
                         (UINT4) rip_is_poison_reverse_applicable (&
                                                                   (pRipInfoBlk
                                                                    [u2RouteInd]),
                                                                   u2IfIndex,
                                                                   pRipCxtEntry))
                        == RIP_POIS_REV_APPLICABLE)
                    {

                        RIP_METRIC (pRipInfo) = RIP_HTONL (RIP_INFINITY);
                        /* while poisoning we have to indicate that
                           the destination is not reachable through this
                           interface, not the actual next hop
                         */

                        RIP_NEXT_HOP (pRipInfo) = 0;
                    }
                    else if (u4RetVal == RIP_POIS_REV_NOT_APPLICABLE)
                    {

                        /*
                         * Apply split horizon, and so we won't be sending this
                         * route through this interface.
                         */

                        continue;    /* Proceed to the next route      */
                    }
                }                /* If the interface is same as that of outgoing interface */

                else
                {
                    /* in case next hop is not directly reachable
                     * on this interface
                     */

                    RIP_NEXT_HOP (pRipInfo) = 0;
                }

                /*
                 * Here, we need to check for whether the route is a default rou
                 * te originated from this interface, and if so, the metric shou
                 * ld be put to the configured value for default routes. If the 
                 * deafult route metric is 0, this route will not be propagated 
                 * A default route via another route may be propagated.
                 */

                /*
                 * We have to mind a fact that all the PDU entries are already 
                 * in the network byte order.
                 */

                if ((RIP_NTOHL (RIP_DEST_NET (pRipInfo)) == 0) &&
                    ((pRipInfoBlk[u2RouteInd]).u2If == u2IfIndex))
                {
                    if (pRipIfRec->RipIfaceCfg.u2DefaultMetric != 0)
                    {

                        /*
                         * Assign the metric field in the tRtRipInfo to the
                         * configured metric.
                         */
                        RIP_ADDRESS_FAMILY (pRipInfo) =
                            (UINT2)
                            RIP_HTONS (pRipIfRec->RipIfaceCfg.u2DefaultMetric);
                    }
                    else
                    {

                        continue;    /*  Defer this entry.  */
                    }
                }                /*  End of if default entry()  */

                u2NoOfValidRoutes++;
                pRipInfo++;        /* proceed to the next entries        */

            }                    /* for ( u2RouteInd ) */

            if (u2NoOfValidRoutes != 0)
            {

                /*
                 * Calculate the length of the message that is getting passed
                 * depending upon the version.
                 */

                u2RipMsgLen = (UINT1) (((u2NoOfValidRoutes) * RIP_INFO_LEN) +
                                       RIP_HDR_LEN);
                if (u1AuthFlag == RIP_AUTH_NEEDED)
                {

                    u1AuthFlag = RIP_AUTH_NOT_NEEDED;
                    /* Reset to the default  */
                }

                /*
                 * Construct the authentication tuple here.
                 */
                if (pRipIfRec->RipIfaceCfg.u2AuthType !=
                    RIPIF_NO_AUTHENTICATION)
                {
                    i4Status = rip_construct_auth_info ((UINT1 *) &RipPkt,
                                                        &u2RipMsgLen,
                                                        u2IfIndex,
                                                        pRipCxtEntry);
                }

                /*
                 * Send the RIP packet down the stack over this interface.
                 */
                if (i4Status == RIP_SUCCESS)
                {

                    /* If We need to send the Unicast update, we need to 
                     * duplicate
                     * the buffer.
                     */
                    RIP_SLL_Scan (&(pRipIfRec->RipUnicastNBRS), pRipUnicastNBR,
                                  tRipUnicastNBRS *)
                    {

                        u4DestNet = pRipUnicastNBR->u4UnicastNBR;
                        RIP_TRC_ARG1 (RIP_MOD_TRC,
                                      pRipCxtEntry->u4RipTrcFlag,
                                      pRipCxtEntry->i4CxtId,
                                      CONTROL_PLANE_TRC, RIP_NAME,
                                      "Sending unicast update to %x \n",
                                      u4DestNet);

                        if (rip_task_udp_send
                            ((UINT1 *) &RipPkt, UDP_RIP_PORT, u4DestNet,
                             UDP_RIP_PORT, u2RipMsgLen, RIP_INITIAL_TTL_VALUE,
                             u2IfIndex, RIP_INITIAL_BROADCAST_VALUE,
                             0, pRipCxtEntry) == RIP_FAILURE)
                        {
                            if (pRipIfRec != NULL)
                            {
                                (pRipIfRec->RipIfaceStats.
                                 u4RipSendUnicastDatagram1FailCount)++;
                            }
                            break;
                        }

                        if ((u1Version == RIP_VERSION_2)
                            && (u1Mcastflag == RIP_MCAST_OFF))
                        {
                            /*
                             *For v1/v2 compitability sending 
                             *v1 & v2 packets.
                             */
                            pRipHdr->u1Version = RIP_VERSION_1;
                            if (rip_task_udp_send
                                ((UINT1 *) &RipPkt, UDP_RIP_PORT, u4DestNet,
                                 UDP_RIP_PORT, u2RipMsgLen,
                                 RIP_INITIAL_TTL_VALUE,
                                 u2IfIndex, RIP_INITIAL_BROADCAST_VALUE,
                                 0, pRipCxtEntry) == RIP_FAILURE)
                            {
                                if (pRipIfRec != NULL)
                                {
                                    (pRipIfRec->RipIfaceStats.
                                     u4RipSendUnicastDatagram2FailCount)++;
                                }

                                break;

                            }
                            pRipHdr->u1Version = u1Version;
                        }
                    }

                }

            }                    /* End of if (u2NoOfValidRoutes != 0) */

        }                        /*  End of RIPIF_LOGICAL_IFACES_SCAN(u2IfIndex) */
    }

    /*
     * When returning back, we need to return the status also. If the update is
     * for ALL_THE_INTERFACES, then we might as well send the status as RIP_SUCC
     * ESS as we can't keep track status on a per interface basis.
     */

    return i4Status;

}

/*******************************************************************************

    Function        :   rip_process_input().

    Description        :   This function is called when a RIP packet arrives
                over RIP validated interface and it extracts the RIP
                header and validates the packet and demultiplexes
                the packet based on the command type in the header
                whether it is a request or response and accordingly
                calls the respective procedures to handle them and
                simultaneously it updates the statistics wherever
                necessary.

    Input parameters    :   1. u2IfIndex, Of Type UINT2, which indicates the
                   interface on which the packet was received.
                2. pBuf, Of Type tRIP_BUF_CHAIN_HEADER*,which is the
                   data buffer with IP & UDP header.
                3. u2Len, Of Type UINT2, which is the length of the
                   RIP packet alone in bytes.

    Output parameters    :   None.

    Global variables
    Affected        :   Statistics like,
                1. u4InMessages, u4InRequests, u4InResponses,
                u4UnKnownCmds, u4InPktDiscards of tRipIfaceStats.
                2. Elements in tRipPeerRec structure.
                The statistics are updated depending upon the
                incoming packet structure.

    Return value    :   success | failure.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_process_input
    $$TRACE_PROCEDURE_LEVEL = MAIN
*******************************************************************************/
EXPORT INT4
rip_process_input (UINT2 u2IfIndex, UINT1 *pu1RipPkt,
                   UINT2 u2Len, UINT4 u4SrcAddr, UINT4 u4DestAddr,
                   UINT2 u2SrcUdpPort, tRipCxt * pRipCxtEntry)
{
    INT4                i4Status;
    UINT1               au1RtTag[2];
    tRipPkt            *pRipPkt = NULL;
    tTrigRipPkt        *pTrigRipPkt = NULL;
    tRip               *pRipHdr = (tRip *) (VOID *) pu1RipPkt;
    tUpdHdr            *pUpdHdr = NULL;
    tRipInfo           *pRipInfo = NULL;
    tRipInfo           *pRipFillRtInfo = NULL;
    UINT4               u4SeqNo = 0;    /* Used for MD5 Authentication */
    INT4                i4ProcessQMsg = 0;
    INT4                i4DigestLen = 0;
    UINT1               u1KeyId = 0;
    INT1                i1IsDC = RIP_SUCCESS;
    tRipIfaceRec       *pRipIfRec = NULL;

    pRipIfRec = RipGetUtilIfRecForPrimSecAddr (u4SrcAddr, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        i4Status = RIP_FAILURE;
        /*Interface Record Not Found */
        return i4Status;
    }

    i1IsDC = RipIsDemandCircuit (pRipIfRec->RipIfaceCfg.u2RipSendStatus);
    if ((pRipHdr->u1Command >= RIP_UPDATE_REQUEST) &&
        (pRipHdr->u1Command <= RIP_UPDATE_ACK))
    {
        if (pRipIfRec->u1Persistence != RIP_WAN_TYPE)
        {
            return RIP_FAILURE;
        }
        pTrigRipPkt = (tTrigRipPkt *) (VOID *) pu1RipPkt;
        pRipInfo = (tRipInfo *) & (pTrigRipPkt->aRipInfo[0]);
        pRipFillRtInfo = (tRipInfo *) & (pTrigRipPkt->aRipInfo[0]);
        pUpdHdr = (tUpdHdr *) & (pTrigRipPkt->UpdHdr);
    }
    else
    {
        if ((pRipIfRec->u1Persistence == RIP_WAN_TYPE)
            && (i1IsDC != RIP_SUCCESS))
        {
            /* normal RIP update on WAN circuit - we dont have backward comp */
            return RIP_FAILURE;
        }
        pRipPkt = (tRipPkt *) (VOID *) pu1RipPkt;
        pRipInfo = (tRipInfo *) & (pRipPkt->aRipInfo[0]);
    }
    i4Status = rip_check_authentication (pRipInfo, pu1RipPkt, u4SrcAddr,
                                         u2IfIndex, &u2Len, &u4SeqNo, &u1KeyId,
                                         pRipCxtEntry);
    if (i4Status == RIP_SUCCESS)
    {

        if (pTrigRipPkt != NULL)
        {

            u2Len = (UINT2) (u2Len - sizeof (tUpdHdr));
            /* Validate update header */
            if (pUpdHdr == NULL)
            {
                return RIP_FAILURE;
            }

            if ((pUpdHdr->u1Version != RIP_VERSION_1) ||
                /* Ver field should be 1 */
                ((pRipHdr->u1Command == RIP_UPDATE_REQUEST) &&
                 /* If UPDATE_REQ, flush and seq MBZ */
                 ((pUpdHdr->u1Flush != 0) || (pUpdHdr->u2SeqNo != 0))))
            {

                RIP_TRC_ARG1 (RIP_MOD_TRC,
                              pRipCxtEntry->u4RipTrcFlag,
                              pRipCxtEntry->i4CxtId,
                              ALL_FAILURE_TRC,
                              RIP_NAME,
                              "Triggered packet received on interface %d "
                              "is dropped for bad update header \n", u2IfIndex);

                if (pRipIfRec != NULL)
                {
                    (pRipIfRec->RipIfaceStats.u4RipBadUpdateHeaderFailCount)++;
                }
                return RIP_FAILURE;
            }
        }
    }
    if ((i4Status == RIP_SUCCESS) &&
        (rip_validate_packet (pRipHdr, pRipInfo,
                              u2IfIndex, u4DestAddr, u4SrcAddr, u2Len,
                              pRipCxtEntry)) == RIP_SUCCESS)
    {

        /*
         * For any type of command, we need to find out the length of the
         * message containing routes alone excluding the authentication entry.
         * It basically involves, checking whether the first entry is an
         * authentication entry and then discarding that entry in the
         * calculation.
         */

        if ((pRipHdr->u1Version == RIP_VERSION_2) &&
            (RIP_NTOHS (RIP_ADDRESS_FAMILY (pRipInfo)) ==
             RIP_AUTH_ADDRESS_FAMILY))
        {

            switch (pRipIfRec->RipIfaceCfg.u2AuthType)
            {
                case RIPIF_SIMPLE_PASSWORD:
                    /* Fall through */
                case RIPIF_MD5_AUTHENTICATION:
                    i4DigestLen = RIP_MD5_DIGEST_LEN;
                    break;

                case RIPIF_SHA1_AUTHENTICATION:
                    i4DigestLen = RIP_SHA1_DIGEST_LEN;
                    break;

                case RIPIF_SHA256_AUTHENTICATION:
                    i4DigestLen = RIP_SHA256_DIGEST_LEN;
                    break;

                case RIPIF_SHA384_AUTHENTICATION:
                    i4DigestLen = RIP_SHA384_DIGEST_LEN;
                    break;

                case RIPIF_SHA512_AUTHENTICATION:
                    i4DigestLen = RIP_SHA512_DIGEST_LEN;
                    break;
                default:
                    break;
            }

            /*
             * In this case, we need to set the pRipInfo variable to the next 
             * entry, so that it points to the exact information entry and 
             * also update the length of the rip information entry.
             */

            pRipInfo++;
            u2Len = (UINT2) (u2Len - (i4DigestLen + RIP_AUTH_HDR_LEN));
        }

        /* CST: ANVL - Trailing Junk problem Fix. */
        if ((u2Len == 0) || (u2Len > sizeof (tRipPkt)) ||
            (((u2Len - sizeof (tRip)) % sizeof (tRipInfo)) != 0))
        {

            RIP_TRC_ARG1 (RIP_MOD_TRC,
                          pRipCxtEntry->u4RipTrcFlag,
                          pRipCxtEntry->i4CxtId,
                          ALL_FAILURE_TRC,
                          RIP_NAME,
                          "Packet received on interface %d "
                          "is dropped for trailing junk \n", u2IfIndex);

            if ((pRipHdr->u1Command == RIP_UPDATE_RESPONSE) ||
                (pRipHdr->u1Command == RIP_RESPONSE))
            {
                (pRipIfRec->RipIfaceStats.u4InBadPackets)++;
                (pRipIfRec->RipIfaceStats.u4InBadPackets1)++;

            }
            else
            {
                (pRipIfRec->RipIfaceStats.u4RipTrailingJunkFailCount)++;
                return RIP_FAILURE;
            }
        }

        if (RipLowPriorityPacket (pRipHdr->u1Command) == RIP_TRUE)
        {
            if (u2Len == (UINT2) RIP_HDR_LEN)
            {
                return RIP_SUCCESS;
            }

            /*
             * Responses can be accepted only from the RIP-UDP port and also
             * if the sending host is a valid one.
             */

            if ((u2SrcUdpPort == UDP_RIP_PORT) &&
                (rip_validate_sending_host
                 (u2IfIndex, u4SrcAddr, pRipCxtEntry) == RIP_SUCCESS))
            {
                if (pRipHdr->u1Command == RIP_UPDATE_RESPONSE)
                {
                    (pRipIfRec->RipIfaceStats.u4RipUpdatesResponseReceived)++;

                }

                if (pRipHdr->u1Command == RIP_RESPONSE)
                {
                    (pRipIfRec->RipIfaceStats.u4InRipResponseProcessed)++;

                }

                i4Status = RipSendRespPktToQ (pRipHdr, pRipInfo, u2Len,
                                              u4SrcAddr, u2IfIndex,
                                              u4SeqNo, u1KeyId, pRipCxtEntry);
                if ((OsixGetNumMsgsInQ ((UINT4) 0,
                                        RIP_RESP_Q_NAME,
                                        (UINT4 *) &i4ProcessQMsg) ==
                     OSIX_SUCCESS) && (i4ProcessQMsg >= RIP_RESP_MSG_NUM))
                {
                    RipProcessRespMessages ();
                }
            }
            else
            {
                (pRipIfRec->RipIfaceStats.u4InBadPackets)++;
                (pRipIfRec->RipIfaceStats.u4InBadPackets2)++;
                i4Status = RIP_FAILURE;
            }
            return i4Status;
        }
        switch (pRipHdr->u1Command)
        {

            case RIP_REQUEST:

                if (u2Len == (UINT2) RIP_HDR_LEN)
                {
                    return RIP_SUCCESS;
                }

                /*
                 * If we are active, Requests can be answered.
                 * If we are passive, then the requests should come from the
                 * port other then the standard RIP application port in UDP.
                 */
                if ((RIP_IS_ACTIVE (pRipIfRec)) ||
                    (u2SrcUdpPort != UDP_RIP_PORT))
                {

                    RIP_TRC_ARG1 (RIP_MOD_TRC,
                                  pRipCxtEntry->u4RipTrcFlag,
                                  pRipCxtEntry->i4CxtId,
                                  CONTROL_PLANE_TRC, RIP_NAME,
                                  "Received request from %x \n", u4SrcAddr);

                    (pRipIfRec->RipIfaceStats.u4InRipRequestReceived)++;
                    i4Status = rip_process_request (pRipHdr, pRipInfo, u2Len,
                                                    u4SrcAddr, u2SrcUdpPort,
                                                    u2IfIndex, pRipCxtEntry);

                    if (i4Status == RIP_FAILURE)
                    {
                        RIP_TRC (RIP_MOD_TRC,
                                 pRipCxtEntry->u4RipTrcFlag,
                                 pRipCxtEntry->i4CxtId,
                                 ALL_FAILURE_TRC, RIP_NAME,
                                 "Failed while processing the RIP request\n");
                    }

                }
                break;
            case RIP_RESPONSE:

                if (u2Len == (UINT2) RIP_HDR_LEN)
                {
                    return RIP_SUCCESS;
                }

                /* Responses can be accepted only from the RIP-UDP port and also
                   if the sending host is a valid one */

                if ((u2SrcUdpPort == UDP_RIP_PORT) &&
                    (rip_validate_sending_host
                     (u2IfIndex, u4SrcAddr, pRipCxtEntry) == RIP_SUCCESS))
                {
                    (pRipIfRec->RipIfaceStats.u4InRipResponseProcessed)++;
                    i4Status = rip_process_response (pRipHdr->u1Version,
                                                     pRipInfo, u2Len,
                                                     u4SrcAddr, u2IfIndex,
                                                     u4SeqNo, u1KeyId,
                                                     pRipCxtEntry);
                }
                else
                {
                    (pRipIfRec->RipIfaceStats.u4InBadPackets)++;
                    (pRipIfRec->RipIfaceStats.u4InBadPackets2)++;
                    i4Status = RIP_FAILURE;
                }
                break;

            case RIP_UPDATE_RESPONSE:
            {
                if (pUpdHdr != NULL)
                {
                    if (pUpdHdr->u1Flush == RIP_FLUSH_FLAG)
                    {
                        pRipIfRec->u1ReTxCounter = 0;
                        RIP_STOP_TIMER (RIP_TIMER_ID,
                                        &(pRipIfRec->RipSpacingTimer.
                                          TimerNode));

                        pRipIfRec->RipSpacingTimer.u1Data = 0;

                        RipUpdateRoutesThroIf (u2IfIndex, RIP_START_DBASE_TIMER,
                                               pRipIfRec->RipIfaceCfg.u4SrcAddr,
                                               pRipCxtEntry);

                        /* Even if we are passive we have to send ACK */
                        /* change only the command and send the ack */
                        pRipHdr->u1Command = RIP_UPDATE_ACK;
                        u2Len = (UINT2) (sizeof (tRip) + sizeof (tUpdHdr));
                        if (pRipIfRec->RipIfaceCfg.u2AuthType !=
                            RIPIF_NO_AUTHENTICATION)
                        {
                            if (rip_construct_auth_info
                                (pu1RipPkt, &u2Len, u2IfIndex,
                                 pRipCxtEntry) != RIP_SUCCESS)
                            {
                                (pRipIfRec->RipIfaceStats.u4SentTrigUpdates) -=
                                    1;
                                return RIP_FAILURE;
                            }
                        }
                        rip_task_udp_send (pu1RipPkt, UDP_RIP_PORT,
                                           pRipIfRec->u4PeerAddress,
                                           UDP_RIP_PORT, u2Len,
                                           RIP_INITIAL_TTL_VALUE, u2IfIndex,
                                           RIP_INITIAL_BROADCAST_VALUE,
                                           0, pRipCxtEntry);

                        i4Status = RIP_SUCCESS;
                        break;
                    }
                }
                if (pRipIfRec->RipSpacingTimer.u1Data == 0)
                {

                    if (u2Len == (UINT2) RIP_HDR_LEN)
                    {
                        i4Status = RIP_SUCCESS;
                        break;
                    }

                    /*
                     * Responses can be accepted only from the RIP-UDP port and 
                     * also if the sending host is a valid one.
                     */

                    if ((u2SrcUdpPort == UDP_RIP_PORT) &&
                        (rip_validate_sending_host (u2IfIndex,
                                                    u4SrcAddr,
                                                    pRipCxtEntry) ==
                         RIP_SUCCESS))
                    {
                        (pRipIfRec->RipIfaceStats.
                         u4RipUpdatesResponseReceived)++;
                        i4Status =
                            rip_process_response (pRipHdr->u1Version, pRipInfo,
                                                  u2Len, u4SrcAddr, u2IfIndex,
                                                  u4SeqNo, u1KeyId,
                                                  pRipCxtEntry);
                        if (i4Status != RIP_FAILURE)
                        {
                            /* Even if we are passive we have to send ACK */
                            /* change only the command and send the ack */
                            pRipHdr->u1Command = RIP_UPDATE_ACK;
                            u2Len = (UINT2) (sizeof (tRip) + sizeof (tUpdHdr));
                            if (pRipIfRec->RipIfaceCfg.u2AuthType !=
                                RIPIF_NO_AUTHENTICATION)
                            {
                                if (rip_construct_auth_info
                                    (pu1RipPkt, &u2Len, u2IfIndex,
                                     pRipCxtEntry) != RIP_SUCCESS)
                                {
                                    (pRipIfRec->RipIfaceStats.u4SentTrigUpdates)
                                        -= 1;
                                    return RIP_FAILURE;
                                }
                            }
                            rip_task_udp_send (pu1RipPkt, UDP_RIP_PORT,
                                               pRipIfRec->u4PeerAddress,
                                               UDP_RIP_PORT, u2Len,
                                               RIP_INITIAL_TTL_VALUE,
                                               u2IfIndex,
                                               RIP_INITIAL_BROADCAST_VALUE, 0,
                                               pRipCxtEntry);
                        }
                    }
                    else
                    {
                        i4Status = RIP_FAILURE;
                    }
                }
                break;
            }

            case RIP_UPDATE_REQUEST:
            {

                /* stop any retx of update response as the whole database is 
                 * going to be sent shortly.
                 */
                RIP_STOP_TIMER (RIP_TIMER_ID,
                                &(pRipIfRec->RipUpdateTimer.TimerNode));
                if (pRipIfRec->RipIfaceCfg.u2RipSendStatus == RIPIF_DO_NOT_SEND)
                {
                    return RIP_FAILURE;
                }
                pRipIfRec->u4PeerAddress = u4SrcAddr;
                (pRipIfRec->RipIfaceStats.u4SentTrigUpdates) += 1;
                MEMSET (pu1RipPkt, 0, sizeof (tTrigRipPkt));
                RIP_GENERATE_RESPONSE_HDR (*(tTrigRipPkt *) (VOID *) pu1RipPkt,
                                           pRipIfRec, RIP_FLUSH_FLAG);
                u2Len = (UINT2) (sizeof (tRip) + sizeof (tUpdHdr));

                if (pRipIfRec->RipIfaceCfg.u2AuthType !=
                    RIPIF_NO_AUTHENTICATION)
                {
                    if (rip_construct_auth_info
                        (pu1RipPkt, &u2Len, u2IfIndex,
                         pRipCxtEntry) != RIP_SUCCESS)
                    {
                        pRipIfRec->RipIfaceStats.u4SentTrigUpdates -= 1;
                        (pRipIfRec->RipIfaceStats.
                         u4RipUpdateRequestFailCount)++;
                        return RIP_FAILURE;
                    }
                }
                (pRipIfRec->RipIfaceStats.u4RipUpdatesRequestReceived)++;
                rip_task_udp_send (pu1RipPkt, UDP_RIP_PORT,
                                   pRipIfRec->u4PeerAddress,
                                   UDP_RIP_PORT, u2Len, RIP_INITIAL_TTL_VALUE,
                                   u2IfIndex, RIP_INITIAL_BROADCAST_VALUE,
                                   0, pRipCxtEntry);
                RIPIF_FILL_RES_RETX_TIMER (pRipIfRec, RIP_FLUSH_FLAG);

                RIP_START_TIMER (RIP_TIMER_ID,
                                 &(pRipIfRec->RipUpdateTimer.TimerNode),
                                 RIP_CFG_GET_RETX_TIMEOUT (pRipCxtEntry));
                break;
            }

            case RIP_UPDATE_ACK:
            {
                tRipRtNode         *pListNode = NULL;
                tTMO_DLL_NODE      *pDllLink;
                if (pUpdHdr != NULL)
                {
                    if (RIP_NTOHS (pUpdHdr->u2SeqNo) ==
                        (UINT2) (pRipIfRec->RipIfaceStats.u4SentTrigUpdates))
                    {
                        /* received ack stop retx timer */
                        if (RIP_STOP_TIMER (RIP_TIMER_ID,
                                            &(pRipIfRec->RipUpdateTimer.
                                              TimerNode)) != TMR_SUCCESS)
                        {
                            /* we hadn`t sent any update so ignore this ack */
                            break;
                        }
                        pRipIfRec->u1ReTxCounter = 0;

                        if (pUpdHdr->u1Flush == RIP_FLUSH_FLAG)
                        {
                            /* ack with flush start from the begining */
                            pRipIfRec->pReTxFirstRoute =
                                (tRipRtNode *) TMO_DLL_First (&pRipCxtEntry->
                                                              RipRtList);
                            pRipIfRec->pReTxLastRoute = NULL;
                        }
                        else
                        {
                            pListNode = pRipIfRec->pReTxLastRoute;
                            /* if we are here, the last sent route should never 
                             * be NULL, however to avoid crash in abnormal situa
                             * tions this check is required.
                             */
                            if (pListNode != NULL)
                            {
                                pDllLink =
                                    (tTMO_DLL_NODE *) & (pListNode->DllLink);
                                pListNode =
                                    (tRipRtNode *) TMO_DLL_Next (&pRipCxtEntry->
                                                                 RipRtList,
                                                                 pDllLink);
                                pRipIfRec->pReTxFirstRoute = pListNode;
                            }
                        }
                        if ((pRipIfRec->pReTxFirstRoute != NULL) &&
                            (pRipIfRec->RipIfaceCfg.u2RipSendStatus !=
                             RIPIF_DO_NOT_SEND))
                        {

                            /* sending the next set of routes */
                            (pRipIfRec->RipIfaceStats.u4SentTrigUpdates) += 1;

                            RIP_GENERATE_RESPONSE_HDR (*
                                                       ((tTrigRipPkt *) (VOID *)
                                                        pu1RipPkt), pRipIfRec,
                                                       0);

                            u2Len = (UINT2) (sizeof (tRip) + sizeof (tUpdHdr));

                            if (RipGenerateUpdateResponse
                                (pRipFillRtInfo, u2IfIndex, &u2Len,
                                 pRipCxtEntry, RIP_REG_UPDATE) != RIP_SUCCESS)
                            {
                                (pRipIfRec->RipIfaceStats.u4SentTrigUpdates)
                                    -= 1;
                                RIP_TRC_ARG1 (RIP_MOD_TRC,
                                              pRipCxtEntry->u4RipTrcFlag,
                                              pRipCxtEntry->i4CxtId,
                                              CONTROL_PLANE_TRC, RIP_NAME,
                                              "RipGenerateUpdateResponse() for "
                                              "interface %d - returned FAILURE\n",
                                              u2IfIndex);

                                return RIP_FAILURE;
                            }
                            if (pRipIfRec->RipIfaceCfg.u2AuthType !=
                                RIPIF_NO_AUTHENTICATION)
                            {
                                if (rip_construct_auth_info
                                    (pu1RipPkt, &u2Len, u2IfIndex,
                                     pRipCxtEntry) != RIP_SUCCESS)
                                {
                                    RIP_TRC_ARG1 (RIP_MOD_TRC,
                                                  pRipCxtEntry->u4RipTrcFlag,
                                                  pRipCxtEntry->i4CxtId,
                                                  ALL_FAILURE_TRC, RIP_NAME,
                                                  "Failed while filling authentication structure other than simple password for an index %d.\n",
                                                  (u2IfIndex));
                                    (pRipIfRec->RipIfaceStats.
                                     u4SentTrigUpdates) -= 1;
                                    (pRipIfRec->RipIfaceStats.
                                     u4RipUpdateAckFailCount)++;
                                    return RIP_FAILURE;
                                }
                            }
                            rip_task_udp_send (pu1RipPkt, UDP_RIP_PORT,
                                               pRipIfRec->u4PeerAddress,
                                               UDP_RIP_PORT, u2Len,
                                               RIP_INITIAL_TTL_VALUE, u2IfIndex,
                                               RIP_INITIAL_BROADCAST_VALUE,
                                               0, pRipCxtEntry);
                            /* start retx timer */

                            (pRipIfRec->RipIfaceStats.
                             u4RipUpdatesAcknowledged)++;
                            RIPIF_FILL_RES_RETX_TIMER (pRipIfRec, 0);

                            RIP_START_TIMER (RIP_TIMER_ID,
                                             &(pRipIfRec->RipUpdateTimer.
                                               TimerNode),
                                             RIP_CFG_GET_RETX_TIMEOUT
                                             (pRipCxtEntry));
                        }
                    }
                }
                break;
            }

            default:
                (pRipIfRec->RipIfaceStats.u4InBadPackets)++;
                (pRipIfRec->RipIfaceStats.u4InBadPackets3)++;
                i4Status = RIP_FAILURE;
                break;

        }                        /*  End of switch (pRipHdr->u1Command)  */

    }                            /*  End of if (rip_validate_packet()...)  */

    else
    {
        if (pRipHdr->u1Command == RIP_RESPONSE)
        {

            /*
             * We need to update the peer record correspondingly and this is the
             * case for updation of ReceivedBadPackets for this peer through
             * this port. The other variant parameter ReceivedBadRoutes will be
             * passed with a 0 value. We will pass the route tag for the first
             * entry in the incoming RIP packet as the peer route tag.
             */

            au1RtTag[0] = RIP_ROUTE_TAG (pRipInfo)[0];
            au1RtTag[1] = RIP_ROUTE_TAG (pRipInfo)[1];

            if (rip_update_peer_record (u2IfIndex, u4SrcAddr, au1RtTag,
                                        pRipHdr->u1Version,
                                        RIP_INCR_BAD_PACKETS, 0, u4SeqNo,
                                        u1KeyId, pRipCxtEntry) == RIP_FAILURE)
            {
                RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                         pRipCxtEntry->i4CxtId,
                         ALL_FAILURE_TRC, RIP_NAME,
                         "Updation of peer record statistics for the current packet Failed\n");

                (pRipIfRec->RipIfaceStats.u4RipFailUpdatePeerCount)++;
                i4Status = RIP_FAILURE;
            }
        }

        /* End of if (pRipHdr->u1Command == RIP_RESPONSE) */

        if (pRipHdr->u1Command == RIP_UPDATE_RESPONSE)
        {
            (pRipIfRec->RipIfaceStats.u4RipUpdatesResponseReceived)++;

        }

        if (pRipHdr->u1Command == RIP_RESPONSE)
        {
            (pRipIfRec->RipIfaceStats.u4InRipResponseProcessed)++;

        }

        if ((pRipHdr->u1Command == RIP_UPDATE_RESPONSE) ||
            (pRipHdr->u1Command == RIP_RESPONSE))
        {
            (pRipIfRec->RipIfaceStats.u4InBadPackets)++;
            (pRipIfRec->RipIfaceStats.u4InBadPackets4)++;
        }

    }
    /* End of else part of if (rip_validate_packet()) */

    /*** $$TRACE_LOG (EXIT, " Exiting function rip_process_input..  \n"); ***/

    return i4Status;
}

    /***************************************************************
    >>>>>>>>> PRIVATE Routines of this Module Start Here <<<<<<<<<<
    ***************************************************************/

/*******************************************************************************

    Function        :   rip_validate_packet.

    Description        :   This procedure is called to validate the RIP packets
                that are received. The checks are made based on the
                version field in the RIP header.
                1. When the version corresponds to < 1, then the
                   packet is ignored and the return status is sent
                   as RIP_FAILURE.
                2. When the version corresponds to RIP-1, the check
                   is made for whether the interface is configured
                   for receiving RIP1 traffic also (May be in
                   conjunction with RIP2 or alone) and if so, the
                   checks are made for the individual route entries
                   on whether the reserved fields are zero and the
                   decision is taken based on this.
                3. When the version corresponds to 2(RIP Version 2),
                   a check is made for whether we are configured for
                   RIP2 reception over this interface, and then the
                   entries are validated and then the decision is
                   taken.
                4. Additionally, multicast address reception packets
                   are also checked.

    Input Parameters    :   1. pRipPkt Of Type tRipPkt*.
                2. u2IfIndex Of Type UINT2, which is the interface
                   index through which the packets are coming.
                3. u4DestAddr Of Type UINT4, which is the IP address
                   of the destination for multicast address
                   verification.
                4. u2Len Of Type UINT2, which is the length of the
                   RIP packet alone.

    Output Parameters    :   None.

    Global Variables
    Affected        :   Statistics corrsponding to other versions packet
                received.

    Return Value    :   success | failure as an INT4 type value.

******************************************************************************/

/*****************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_validate_packet
    $$TRACE_PROCEDURE_LEVEL = LOW
******************************************************************************/
PRIVATE INT4
rip_validate_packet (tRip * pRipHdr,
                     tRipInfo * pRipInfo,
                     UINT2 u2IfIndex, UINT4 u4DestAddr, UINT4 u4SrcAddr,
                     UINT2 u2Len, tRipCxt * pRipCxtEntry)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    pRipIfRec = RipGetIfRec (u2IfIndex, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return RIP_FAILURE;
    }

    /*
     * We assign the pointers for the RIP header, information to a local
     * temporary variable for easy accessing.
     */

    /*
     * First, check whether the receive status for this packet with command and
     * the destination address is acceptable.
     */

    if (rip_check_recv_status
        (pRipHdr->u1Version, u2IfIndex, u4DestAddr, u4SrcAddr,
         pRipCxtEntry) == RIP_FAILURE)
    {
        RIP_TRC (RIP_MOD_TRC,
                 pRipCxtEntry->u4RipTrcFlag,
                 pRipCxtEntry->i4CxtId,
                 ALL_FAILURE_TRC, RIP_NAME,
                 "Failed while checking receive status for the packet\n");

        (pRipIfRec->RipIfaceStats.u4RipValidatePkt1FailCount)++;
        return RIP_FAILURE;
    }

    /*
     * Check for authentication of the packet, by passing this structure and the
     * interface from which this is received and also the version of the packet.
     */

    if (pRipHdr->u1Version == RIP_VERSION_1)
    {

        /*
         * Version 1 is special in the sense that all the MBZ (Must Be Zero)
         * fields have to be validated for each entry.
         */

        UINT2               u2NoOfRoutes, u2Count;
        tRoute             *pRoute = NULL;

        /*
         * In the RIP header, the 2-byte Reserved field must be zero
         * irrespective of the version.
         * Not exactly, on second thought (rather reading). it is MBZ
         * only for RIPV1.
         */

        if (RIP_NTOHS (pRipHdr->u2Reserved) != 0)
        {
            RIP_TRC (RIP_MOD_TRC,
                     pRipCxtEntry->u4RipTrcFlag,
                     pRipCxtEntry->i4CxtId,
                     ALL_FAILURE_TRC, RIP_NAME,
                     "Exited due to Reserved bit as NON-ZERO\n");
            (pRipIfRec->RipIfaceStats.u4RipValidatePkt2FailCount)++;
            return RIP_FAILURE;
        }

        u2NoOfRoutes = (UINT2) ((u2Len - RIP_HDR_LEN) / RIP_INFO_LEN);

        for (u2Count = 0; u2Count < u2NoOfRoutes; u2Count++)
        {

            pRoute = &(pRipInfo->RipMesg.Route);    /* For easy accessing      */

            if (pRoute->au1RouteTag[0] != RIP_NULL_RTAG ||
                pRoute->au1RouteTag[1] != RIP_NULL_RTAG ||
                RIP_NTOHL (pRoute->u4SubnetMask) != 0 ||
                RIP_NTOHL (pRoute->u4NextHop) != 0)
            {
                RIP_TRC (RIP_MOD_TRC,
                         pRipCxtEntry->u4RipTrcFlag,
                         pRipCxtEntry->i4CxtId,
                         ALL_FAILURE_TRC, RIP_NAME,
                         "Failed while validating the routes\n");
                (pRipIfRec->RipIfaceStats.u4RipValidatePkt3FailCount)++;
                return RIP_FAILURE;
            }

            pRipInfo++;
        }                        /* End of for ( u2Count ) */

    }                            /*  End of if (pRipHdr->u1Version == RIP_VERSION_1)  */

    /*
     * All versions higher than 1 and 2 are assumed to be a vaild ones and
     * allowed.
     */

    return RIP_SUCCESS;
}

/*******************************************************************************

    Function        :   rip_validate_sending_host().

    Description        :   This function checks,
                        1. The source address to see whether the datagram is
                   from a valid neighbour.
                2. For whether the source of the datagram is on a
                   directly connected network.
                3. Whether the response is from one of the host's
                   own addresses.

    Input Parameters    :   1. u2IfIndex, thorugh which the packet has come.
                2. u4Src, the address of the source which has sent
                   the RIP packet.

    Output Parameters    :   None.

    Return Value    :   Success | Failure.

******************************************************************************/

/*****************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_validate_sending_host
    $$TRACE_PROCEDURE_LEVEL = INTMD
******************************************************************************/

PRIVATE INT4
rip_validate_sending_host (UINT2 u2IfIndex, UINT4 u4Src, tRipCxt * pRipCxtEntry)
{
    UINT1               u1IfType;
    INT1                i1IsDC = RIP_SUCCESS;
    tRipIfaceRec       *pRipIfRec = NULL;

    pRipIfRec = RipGetUtilIfRecForPrimSecAddr (u4Src, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return RIP_FAILURE;
    }

    i1IsDC = RipIsDemandCircuit (pRipIfRec->RipIfaceCfg.u2RipSendStatus);

    /* When the Neighbour List feature is enabled, we should check whether we ca
     * n recieve update packets from this Neighbour
     */
    if ((pRipIfRec != NULL)
        && ((pRipIfRec->u1Persistence == RIP_WAN_TYPE)
            && (i1IsDC != RIP_SUCCESS)))
    {
        if ((RipGetNeighborListStatus (pRipCxtEntry) == RIP_NBR_LIST_ENABLE) &&
            (RipCheckForValidNeighbor (u4Src, pRipCxtEntry) == FALSE))
        {
            (pRipIfRec->RipIfaceStats.u4RipValidateHost1FailCount)++;
            RipIncNbrListCheckNoPktsDropped (pRipCxtEntry);
            return RIP_FAILURE;
        }
    }

    /*
     * The above way of finding whether the route is directly connected & is an
     * immediate neighbour is improper.
     */

/* vishu - we need to bypass this check for UNNUMBERED iface. */

    u1IfType = RIPIF_IS_UNNUMBERED (u2IfIndex);
    if (u1IfType == FALSE)
    {
        if ((pRipIfRec != NULL)
            && (((pRipIfRec->u4Addr) & RIPIF_GLBTAB_MASK_OF (u2IfIndex)) !=
                (u4Src & RIPIF_GLBTAB_MASK_OF (u2IfIndex))))
        {
            if (pRipIfRec != NULL)
            {
                (pRipIfRec->RipIfaceStats.u4RipValidateHost2FailCount)++;
            }
            return RIP_FAILURE;
        }
    }
    /*
     * Having known the source is on the directly connected net, we need to 
     * make sure that the packet is not a loopback packet, originated from 
     * us, by checking for whether it matches with any of our interface 
     * addresses */

    if (rip_check_for_our_own_address (u4Src, pRipCxtEntry) == TRUE)
    {
        RIP_TRC (RIP_MOD_TRC,
                 pRipCxtEntry->u4RipTrcFlag,
                 pRipCxtEntry->i4CxtId,
                 ALL_FAILURE_TRC, RIP_NAME,
                 "Failed while matching given address with our own address\n");
        if (pRipIfRec != NULL)
        {
            (pRipIfRec->RipIfaceStats.u4RipValidateHost3FailCount)++;
        }
        return RIP_FAILURE;
    }

    return RIP_SUCCESS;
}

/*******************************************************************************

    Function        :   rip_process_request ( ).

    Description        :   This procedure is intended for reading the request
                that is coming from the remote host and sending back
                the response for the valid entries.

                First the route to the source who has sent the
                request is searched for and if it is not avaliable,
                the process is stopped and it sends a failure reply.

                On learning the outgoing port through which the host
                can be reached, it extracts the rip information
                entries one by one, validate them and construct the
                response. The rules given under the section REQUEST
                PROCESSING in the rfc1058 is followed in addition to
                checking for rip2 send status, what version can be
                sent, etc. Support for authentication is also
                inserted on a per message basis, if necessary. This
                procedure processes any number of routes in a
                request and there is no limitation in this context.

    Input Parameters    :   1. pRipHdr Of Type tRip*.
                2. pRipInfo Of Type tRipInfo*.
                3. u2Len Of Type UINT2.
                4. u4Src Of Type UINT4.
                5. u2SrcUdpPort Of Type UINT2.
                6. u2Port Of Type UINT2.

    Output Parameters    :   None.

    Global Variables
    Affected        :   1. u4RipGlobalQueries of RipGblStats structure,
                2. u4InBadRoutes of RipIfGblRec structure.

    Return Value    :   RIP_SUCCESS | RIP_FAILURE.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_process_request
    $$TRACE_PROCEDURE_LEVEL = INTMD
*******************************************************************************/

PRIVATE INT4
rip_process_request (tRip * pRipHdr, tRipInfo * pRipInfo,
                     UINT2 u2Len, UINT4 u4Src,
                     UINT2 u2SrcUdpPort, UINT2 u2IfIndex,
                     tRipCxt * pRipCxtEntry)
{
    UINT1               u1AuthFlag = RIP_AUTH_NOT_NEEDED;
    UINT2               u2OutIf = 0, u2RouteInd, u2NoOfRoutes = 0, u2OutCount =
        0;
    tRtInfo            *pRouteEntry = NULL;
    static tRipPkt      RipPkt;    /* For temporary usage, i.e., while
                                   constructing a reply message       */
    tRip               *pOutRipHdr = &(RipPkt.RipHdr);
    tRipInfo           *pOutRipInfo = (tRipInfo *) & (RipPkt.aRipInfo[0]);

    UINT2               u2LenToSend;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipIfaceRec       *pRipIfOutRec = NULL;

    pRipIfRec = RipGetIfRec ((UINT4) u2IfIndex, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return RIP_FAILURE;
    }

    /*
     * Calculate the number of routes in the request packet from the length of
     * the packets containing routes alone.
     */

    u2NoOfRoutes = (UINT2) ((u2Len - RIP_HDR_LEN) / RIP_INFO_LEN);

    if (u2NoOfRoutes == 0)
    {

        /*
         * We haven't received any routes and better we discard the current
         * packet without sending any reply.
         */

        return RIP_SUCCESS;
    }

    /*
     * Check whether any route exist for the host which is sending the request.
     */

    if (((pRouteEntry = (tRtInfo *) rip_rt_get_info (u4Src,
                                                     RIPIF_GLBTAB_MASK_OF
                                                     (u2IfIndex),
                                                     pRipCxtEntry)) == NULL)
        || (pRouteEntry->u2RtType != FSIP_LOCAL)
        || (pRouteEntry->u2RtProto != CIDR_LOCAL_ID))
    {
        (pRipIfRec->RipIfaceStats.u4RipProcessRequest1FailCount)++;
        return RIP_FAILURE;
    }
    else
    {

        u2OutIf = (UINT2) (pRouteEntry->u4RtIfIndx);
    }

    /*
     * Check if we have permission to send RIP packets over the outgoing port,
     * if not, there is no point in proceeding further.
     */

    if (rip_check_send_status (pRipHdr->u1Version, u2OutIf, pRipCxtEntry) ==
        RIP_FAILURE)
    {
        RIP_TRC (RIP_MOD_TRC,
                 pRipCxtEntry->u4RipTrcFlag,
                 pRipCxtEntry->i4CxtId,
                 ALL_FAILURE_TRC, RIP_NAME,
                 "Failed while validating permission to send RIp packets\n");
        (pRipIfRec->RipIfaceStats.u4RipProcessRequest2FailCount)++;
        return RIP_FAILURE;
    }

    /*
     * If we are here, we have a recepient who has been validated for version
     * compatibility and whose route is known.
     * Then, look at the individual destinations requested for and form an
     * update message. Find metric from our database, if route doesn't exist
     * then send metric as RIP_INFINITY.
     */

    if (u2NoOfRoutes == 1)
    {                            /* A specific check, acc, rfc1058 */

        if ((RIP_NTOHS (RIP_ADDRESS_FAMILY (pRipInfo)) == 0) &&
            (RIP_NTOHL (RIP_METRIC (pRipInfo)) == RIP_INFINITY))
        {

            /*
             * This is a special case where the requesting node wants our entire
             * routing table to be sent as response. So just generate an update
             * message for this node.
             */

            return (rip_generate_update_message (pRipHdr->u1Version,
                                                 RIP_RES_FOR_REQ,
                                                 u4Src, u2SrcUdpPort, u2OutIf,
                                                 pRipCxtEntry));

        }

    }                            /*  End of if ( u2NoOfRoutes == 1 )  */

    /*
     *  At this point, there may a set of requests for destination networks and
     *  we need to process them individually, constructing message response.
     *  Before that, if authentication is necessary for the outgoing packet over
     *  the interface, we need to make a check for that also.
     */

    pRipIfOutRec = RipGetIfRec ((UINT4) u2OutIf, pRipCxtEntry);
    if (pRipIfOutRec == NULL)
    {
        return RIP_FAILURE;
    }

    if (pRipHdr->u1Version == RIP_VERSION_2 &&
        pRipIfOutRec->RipIfaceCfg.u2AuthType != RIPIF_NO_AUTHENTICATION)
    {

        u1AuthFlag = RIP_AUTH_NEEDED;
    }

    /*
     * As a general step, copy the header from the incoming packet to the
     * outgoing and reassign the command.
     */

    MEMCPY ((VOID *) pOutRipHdr, (const VOID *) pRipHdr, sizeof (tRip));

    /* Assign the command to RIP RESPONSE */
    pOutRipHdr->u1Command = RIP_RESPONSE;

    /*
     * There may be a maximum of 25 number of route requests, the response for
     * which need to be sent thro an iface with authentication support. In this
     * scenario, it boils down to sending of 2 packets for a single request,
     * since we process always in a Maximum of 25 routes response packets each.
     */

    for (u2RouteInd = 0; u2RouteInd < u2NoOfRoutes; u2RouteInd++, ++pRipInfo)
    {

        UINT4               u4SubNetMask;

        if (u2OutCount == 0)
        {
            /* this is the first tuple */
            /* Check whether authentication is needed for this interface */
            /* If so leave this tuple - lets construct that later */

            if (u1AuthFlag == RIP_AUTH_NEEDED)
            {

                u2OutCount++;
            }
        }
        if (rip_validate_route_info (pRipInfo,
                                     u2OutIf, RIP_REQUEST,
                                     pRipCxtEntry) == RIP_FAILURE)
        {
            RIP_TRC (RIP_MOD_TRC,
                     pRipCxtEntry->u4RipTrcFlag,
                     pRipCxtEntry->i4CxtId,
                     ALL_FAILURE_TRC, RIP_NAME,
                     "Failed while validating route information\n");

            /*
             * We have the current rip information entry as an invalid one and
             * we should not consider this entry for further processing, better
             * we will proceed to the next entry.
             */
            (pRipIfRec->RipIfaceStats.u4InBadRoutes)++;
            (pRipIfRec->RipIfaceStats.u4InBadRoutes1)++;
            continue;
        }                        /* End of if rip_validate_route_info() */

        /*
         * Copy the route from the PDU to the outgoing route structure.
         */

        MEMCPY ((VOID *) &(pOutRipInfo[u2OutCount]), (const VOID *) pRipInfo,
                sizeof (tRipInfo));

        if (pRipHdr->u1Version == RIP_VERSION_1)
        {
            u4SubNetMask = rip_task_ip_get_net_mask (u2IfIndex,
                                                     RIP_NTOHL (RIP_DEST_NET
                                                                (pRipInfo)), 0);
        }
        else
        {
            /* only RIP 2 is another valid version */
            u4SubNetMask = RIP_NTOHL (RIP_SUBNET_MASK (pRipInfo));
        }
        if ((pRouteEntry =
             (tRtInfo *) rip_rt_get_info (RIP_NTOHL (RIP_DEST_NET (pRipInfo)),
                                          u4SubNetMask, pRipCxtEntry)) != NULL)
        {

            rip_copy_route_information ((tRipRtEntry *) pRouteEntry,
                                        &(pOutRipInfo[u2OutCount]));

            if (pRipHdr->u1Version == RIP_VERSION_1)
            {

                /* as host routes are not supported
                 * we might think entry which is not having its natural
                 * mask should not be given out.
                 * however, RFC 1058 thinks that typically request
                 * for a part of the table comes for debugging.
                 * and no information should be hidden in such case.
                 */

                /*
                 * Make the MBZ fields as zero.
                 */

                MEMSET (&(RIP_ROUTE_TAG (pOutRipInfo)), RIP_NULL_RTAG,
                        sizeof (RIP_ROUTE_TAG (pOutRipInfo)));
                RIP_SUBNET_MASK (pOutRipInfo) = 0;
                RIP_NEXT_HOP (pOutRipInfo) = 0;
            }

        }                        /* End of if (pRouteEntry = rip_rt_get_info ()) */

        else
        {
            /*   Assign the Infinite Metric, Route is not found   */

            RIP_METRIC (pOutRipInfo) = RIP_HTONL ((UINT4) RIP_INFINITY);
        }

        /*
         * Check if we have composed maximum number of entries , if so, send the
         * composed packet out.
         */

        u2OutCount++;

        if ((u2OutCount == (RIP_MAX_ROUTES_PER_PKT)) ||
            (u2RouteInd == (u2NoOfRoutes - 1)))
        {

            /* Construct the authentication info here */

            u2LenToSend = (UINT2) (RIP_HDR_LEN + (u2OutCount * RIP_INFO_LEN));

            if (pRipIfOutRec->RipIfaceCfg.u2AuthType != RIPIF_NO_AUTHENTICATION)
            {
                if (rip_construct_auth_info ((UINT1 *) &RipPkt, &u2LenToSend,
                                             u2OutIf,
                                             pRipCxtEntry) != RIP_SUCCESS)
                {
                    RIP_TRC (RIP_MOD_TRC,
                             pRipCxtEntry->u4RipTrcFlag,
                             pRipCxtEntry->i4CxtId,
                             ALL_FAILURE_TRC, RIP_NAME,
                             "Failed while filling authentication structure other than simple password for an index\n");
                    (pRipIfRec->RipIfaceStats.u4RipProcessRequest3FailCount)++;
                    return RIP_FAILURE;
                }
            }

            /* Send the packet outside over UDP. */

            if (rip_task_udp_send ((UINT1 *) &RipPkt, UDP_RIP_PORT, u4Src,
                                   u2SrcUdpPort, u2LenToSend,
                                   (UINT1) RIP_TTL (RIP_RES_FOR_REQ),
                                   u2OutIf, RIP_INITIAL_BROADCAST_VALUE,
                                   0, pRipCxtEntry) == RIP_SUCCESS)
            {

                /*
                 * For response to a request, we will put the TTL value as 0 and
                 * IP module will put the appropriate value.
                 */

                pRipCxtEntry->RipGblStats.u4GlobalQueries++;    /* Statistics Updation    */
                u2OutCount = 0;    /* Reinitialise the count */
            }
            else
            {
                (pRipIfRec->RipIfaceStats.u4RipProcessRequest4FailCount)++;
                return RIP_FAILURE;
            }

        }                        /* End of if (u2OutCount == (RIP_MAX_ROUTES_PER_PKT)) &&
                                   (u2RouteInd == (u2NoOfRoutes-1)) */

    }                            /* End of for (u2RouteInd) */

    return RIP_SUCCESS;

}                                /*  End of Function rip_process_request() */

/*******************************************************************************

    Function        :   rip_validate_route_info.

    Description        :   This function validates the route information on an
                entry basis and follows the validation rules
                mentioned in rfc1058, under the section "REQUEST".
                It checks for address family as a valid inet family,
                metric is less than or equal to the the infinite
                limit, destination is default (Which is significant
                only for incoming responses), the destination
                address is of CLASS D or CLASS E type, and also the
                address is neither a loopback address nor a
                broadcast address.

    Input Parameters    :   pRipInfo Of type t_RIPINFO *.

    Output Parameters    :   None.

    Global Variables
    Affected        :   None.

    Return Value    :   Success/Failure/Rip_default_route/Rip_host_route.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_validate_route_info
    $$TRACE_PROCEDURE_LEVEL = LOW
*******************************************************************************/
PRIVATE INT4
rip_validate_route_info (tRipInfo * pRipInfo, UINT2 u2If, UINT1 u1Cmd,
                         tRipCxt * pRipCxtEntry)
{
    UINT4               u4SubNetMask;
    UINT4               u4Addr;
    UINT4               u4Mask;
    INT1                i1IsDC = RIP_SUCCESS;
    tRipIfaceRec       *pRipIfRec = NULL;

    pRipIfRec = RipGetIfRec ((UINT4) u2If, pRipCxtEntry);

    if (RIP_NTOHS (RIP_ADDRESS_FAMILY (pRipInfo)) !=
        (UINT4) INET_ADDRESS_FAMILY)
    {
        if (pRipIfRec != NULL)
        {
            (pRipIfRec->RipIfaceStats.u4RipValidateRouteInfo1FailCount)++;
        }
        return RIP_FAILURE;
    }
    if (((u1Cmd == RIP_RESPONSE) || (u1Cmd == RIP_UPDATE_RESPONSE)) &&
        ((RIP_NTOHL (RIP_METRIC (pRipInfo)) > (UINT4) RIP_INFINITY) ||
         (RIP_NTOHL (RIP_METRIC (pRipInfo)) < (UINT4) RIP_LEAST_METRIC)))
    {
        if (pRipIfRec != NULL)
        {
            (pRipIfRec->RipIfaceStats.u4RipValidateRouteInfo2FailCount)++;
        }
        return RIP_FAILURE;
    }

    if ((RIP_NTOHL (RIP_DEST_NET (pRipInfo)) == 0) &&
        (RIP_NTOHL (RIP_SUBNET_MASK (pRipInfo)) == 0))
    {
        /* if it is configured not to install default route lerned,
         * return failure*/
        if ((pRipIfRec != NULL) && (pRipIfRec->RipIfaceCfg.u1DefRtInstallStatus
                                    == RIP_DONT_INSTALL_DEF_RT))
        {
            return RIP_DONT_INSTALL_DEF_RT;
        }
        return RIP_DEFAULT_ROUTE;
    }

    if (RIP_IS_ADDR_CLASS_D (RIP_NTOHL (RIP_DEST_NET (pRipInfo))) != FALSE ||
        RIP_IS_ADDR_CLASS_E (RIP_NTOHL (RIP_DEST_NET (pRipInfo))) != FALSE)
    {
        if (pRipIfRec != NULL)
        {
            (pRipIfRec->RipIfaceStats.u4RipValidateRouteInfo3FailCount)++;
        }
        return RIP_FAILURE;
    }

    /*
     * Retrieve the subnetwork mask either from the rip information entry or
     * from the default net mask function for class based networks.
     */

    u4SubNetMask = RIP_NTOHL (RIP_SUBNET_MASK (pRipInfo));

    u4SubNetMask = (u4SubNetMask != 0) ? u4SubNetMask :
        rip_task_ip_get_net_mask (u2If, RIP_NTOHL (RIP_DEST_NET (pRipInfo)), 0);

    /*
     * Check for loopback & if it is not Host route entry
     * check broadcast addresses.
     */

    if (((RIP_NTOHL (RIP_DEST_NET (pRipInfo)) & u4SubNetMask) ==
         RIP_LOOPBACK_NET_ADDRESS) ||
        ((u4SubNetMask != RIP_BROADCAST_ADDRESS) &&
         (RIP_NTOHL (RIP_DEST_NET (pRipInfo)) & ~u4SubNetMask) ==
         ~u4SubNetMask))
    {
        if (pRipIfRec != NULL)
        {
            (pRipIfRec->RipIfaceStats.u4RipValidateRouteInfo4FailCount)++;
        }
        return RIP_FAILURE;
    }

    /*
     * If Most significant byte is zero then return RIP_FAILURE.
     */

    if (((RIP_NTOHL (RIP_DEST_NET (pRipInfo)) & RIP_CLASS_A_NETMASK) ==
         RIP_INVALID_CLASS_A_NET)
        && (RIP_NTOHL (RIP_SUBNET_MASK (pRipInfo)) == 0))
    {
        if (pRipIfRec != NULL)
        {
            (pRipIfRec->RipIfaceStats.u4RipValidateRouteInfo5FailCount)++;
        }
        return RIP_FAILURE;
    }

    /* RFC 2091  Sec 5.5 
     * Next Hop MUST be zero, since Triggered RIP can NOT advertise routes
     * on behalf of other WAN routers.
     */
    if (pRipIfRec == NULL)
    {
        return RIP_FAILURE;
    }

    i1IsDC = RipIsDemandCircuit (pRipIfRec->RipIfaceCfg.u2RipSendStatus);
    if (((pRipIfRec->u1Persistence == RIP_WAN_TYPE) && (i1IsDC != RIP_SUCCESS))
        && (RIP_NTOHL (RIP_NEXT_HOP (pRipInfo)) != 0))
    {
        (pRipIfRec->RipIfaceStats.u4RipValidateRouteInfo6FailCount)++;
        return RIP_FAILURE;
    }

    u4Addr = RIPIF_GLBTAB_ADDR_OF (u2If);
    u4Mask = RIPIF_GLBTAB_MASK_OF (u2If);

    if (RIPIF_IS_UNNUMBERED (u2If) == FALSE)
    {
        if (u4Addr == (UINT4) RIP_FAILURE || u4Mask == (UINT4) 0)
        {
            if (pRipIfRec != NULL)
            {
                (pRipIfRec->RipIfaceStats.u4RipValidateRouteInfo7FailCount)++;
            }
            return RIP_FAILURE;
        }
    }
    return RIP_SUCCESS;
}

/*******************************************************************************

    Function        :   rip_copy_route_information.

    Description        :   This function copies the route information from the
                existing structure to the new one. It takes care of
                ENDIAN level dependence while copying.

    Input Parameters    :   1. pRouteEntry, a pointer Of type tRipRtEntry*,
                   the source memory address.
                2. pDestRipInfo, Of type tRipInfo*, the destination
                   memory address.

    Output Parameters    :   pDestRipInfo, is copied with the relevent
                information and then returned.

    Global Variables
    Affected        :   None.

    Return Value    :   None.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_copy_route_information
    $$TRACE_PROCEDURE_LEVEL = LOW
*******************************************************************************/

PRIVATE VOID
rip_copy_route_information (tRipRtEntry * pRouteEntry, tRipInfo * pRipInfo)
{

    UINT2               u2RtTag = 0;

    u2RtTag = (UINT2) ((pRouteEntry->RtInfo.u4RtNxtHopAS) & 0x0000ffff);
    u2RtTag = RIP_HTONS (u2RtTag);
    MEMCPY (RIP_ROUTE_TAG (pRipInfo), &u2RtTag, sizeof (UINT2));

    RIP_DEST_NET (pRipInfo) = RIP_HTONL (pRouteEntry->RtInfo.u4DestNet);
    RIP_SUBNET_MASK (pRipInfo) = RIP_HTONL (pRouteEntry->RtInfo.u4DestMask);

    /* if it is any other protocol entry second condition will not be checked
     * so though u4Gw is not the field of other protocols
     * comparison is ok.
     */

    if ((pRouteEntry->RtInfo.u2RtProto == RIP_ID) &&
        (pRouteEntry->RtInfo.u4NextHop != pRouteEntry->RtInfo.u4Gw))
    {
        RIP_NEXT_HOP (pRipInfo) = RIP_HTONL (pRouteEntry->RtInfo.u4NextHop);
    }
    else
    {
        RIP_NEXT_HOP (pRipInfo) = 0;
    }

    RIP_METRIC (pRipInfo) =
        (UINT4) RIP_HTONL ((UINT4) pRouteEntry->RtInfo.i4Metric1);

}

/*******************************************************************************

    Function        :   rip_process_response ().

    Description        :   This function, as the name itself implies, reads the
                route informations from the incoming response packet
                and validates the route information, and then
                proceeds to add the route information to the rip
                route table. If everything is successful, it sends a
                triggered broadcast over all the enabled interfaces.
                Here, it takes care of subnet mask assignment for
                rip version 2 packets and ordinary default net mask
                for rip version 1 packets.

    Input Parameters    :   1. u1Version - version number.
                2. pRipInfo, a message buffer having the RIP
                   information.
                3. u2Len, the length of the RIP packet alone.
                4. u4Src, the address of the source.
                5. u2If, the interface through which the RIP
                   information has come.

    Output Parameters    :   None.

    Global Variables
    Affected        :   None.

    Return Value    :   Success/Failure/Rip_trigger_update.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_process_response
    $$TRACE_PROCEDURE_LEVEL = INTMD
*******************************************************************************/
PRIVATE INT4
rip_process_response (UINT1 u1Version, tRipInfo * pRipInfo,
                      UINT2 u2Len, UINT4 u4Src, UINT2 u2If,
                      UINT4 u4SeqNo, UINT1 u1KeyId, tRipCxt * pRipCxtEntry)
{

    UINT1               u1BcastFlag = RIP_REG_UPDATE, au1RtTag[2];
    UINT2               u2RouteNo, u2PeerRcvBadRts = 0, u2NoOfRoutes = 0;
    UINT4               u4SubNetMask, u4GwAddr, u4Metric;
    INT4                i4RtStatus;
    UINT4               u4Addr, u4Mask;
    UINT1               u1Distance;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipRtInfo          RtInfo;

    MEMSET (au1RtTag, 0, sizeof (UINT2));
    pRipIfRec = RipGetUtilIfRecForPrimSecAddr (u4Src, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return RIP_FAILURE;
    }

    /*
     * Calculate the number of routes in the response packet from the length of
     * the packets containing routes alone.
     */

    u2NoOfRoutes = (UINT2) ((u2Len - RIP_HDR_LEN) / RIP_INFO_LEN);

    if (!u2NoOfRoutes)
    {

        /*
         * We haven't received any routes and better we discard the current
         * packet without proceeding further.
         */

        /*
         * We have to update the peer record statictics for the current packet that
         * was received.
         */

        if (rip_update_peer_record (u2If, u4Src, au1RtTag, u1Version,
                                    0, 0, u4SeqNo, u1KeyId,
                                    pRipCxtEntry) == RIP_FAILURE)
        {
            RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                     pRipCxtEntry->i4CxtId,
                     ALL_FAILURE_TRC, RIP_NAME,
                     "Updation of peer record statistics for the current packet Failed\n");
            (pRipIfRec->RipIfaceStats.u4RipFailUpdatePeerCount)++;
            return RIP_FAILURE;
        }
        return RIP_SUCCESS;
    }

    /*
     * For the purpose of peer record updation, we will pass the route
     * tag value for the first entry in the RIP packet as the peer
     * route tag value.
     */

    /* Scanning the route entries */
    for (u2RouteNo = 0; u2RouteNo < u2NoOfRoutes; u2RouteNo++, ++pRipInfo)
    {
        /*  Calculate the metric  */
        MEMSET (au1RtTag, 0, sizeof (UINT2));
        MEMSET (&RtInfo, 0, sizeof (tRipRtInfo));
        au1RtTag[0] = RIP_ROUTE_TAG (pRipInfo)[0];
        au1RtTag[1] = RIP_ROUTE_TAG (pRipInfo)[1];
        u4Metric = RIP_NTOHL (RIP_METRIC (pRipInfo));

        /* add cost of the if  before adding in the routing table */
        if (u4Metric <= RIP_INFINITY)
        {
            u4Metric += RIP_DEFAULT_METRIC;
        }

        RtInfo.u4DestNet = OSIX_NTOHL (pRipInfo->RipMesg.Route.u4DestNet);
        RtInfo.u4DestMask = OSIX_NTOHL (pRipInfo->RipMesg.Route.u4SubnetMask);
        RtInfo.u4NextHop = RIP_NTOHL (pRipInfo->RipMesg.Route.u4NextHop);
        RtInfo.u4RtIfIndx = (UINT4) u2If;
        RtInfo.i4Metric1 = (INT4) u4Metric;
        RtInfo.u4RtNxtHopAS = au1RtTag[0];
        RtInfo.u4RtNxtHopAS = (RtInfo.u4RtNxtHopAS << 8) + au1RtTag[1];
        RtInfo.u2RtType = RIP_RT_INDIRECT;
        u1Distance = RipFilterRouteSourceInCxt (pRipCxtEntry, pRipInfo);
        if (RipApplyInOutFilter (pRipCxtEntry->pDistributeInFilterRMap,
                                 &RtInfo, u4Src) != RIP_SUCCESS)
        {
            /* Stop processing this route and go to processing next route. */
            continue;
        }

        i4RtStatus =
            rip_validate_route_info (pRipInfo, u2If, RIP_RESPONSE,
                                     pRipCxtEntry);
        if ((i4RtStatus == RIP_DEFAULT_ROUTE) || (i4RtStatus == RIP_SUCCESS))
        {
            u4Addr = RIPIF_GLBTAB_ADDR_OF (u2If);
            u4Mask = RIPIF_GLBTAB_MASK_OF (u2If);

            if (RIP_NEXT_HOP (pRipInfo) != 0)
            {
                if ((RIP_NTOHL (RIP_NEXT_HOP (pRipInfo)) & u4Mask) !=
                    (u4Addr & u4Mask))
                {
                    RIP_NEXT_HOP (pRipInfo) = RIP_HTONL (u4Src);
                }
            }
            /*
             * Retrieve the subnetwork mask either from the rip information
             * entry or from the default net mask function for class based
             * networks. For default routes, we need to assign the
             * mask in a different manner.
             * hosts are not supported.
             */

            u4SubNetMask = RIP_NTOHL (RIP_SUBNET_MASK (pRipInfo));

            if (!u4SubNetMask)
            {
                if (i4RtStatus == RIP_DEFAULT_ROUTE)
                {
                    u4SubNetMask = 0;    /* default route with address and mask 0 */
                }
                else
                {
                    u4SubNetMask = rip_task_ip_get_net_mask (u2If,
                                                             RIP_NTOHL
                                                             (RIP_DEST_NET
                                                              (pRipInfo)), 0);
                }
            }                    /* End of if (u4SubNetMask == 0) */

            /* Assign the gateway address from the source itself */

            u4GwAddr = u4Src;    /* Assign the source itself       */

            /* u4Metric can not have higher value than the RIP_INF */
            if (u4Metric >= RIP_INFINITY)
            {
                u4Metric = RIP_INFINITY;
            }
            else
            {
                /*Update metric value if any filter is applied */
                u4Metric = (UINT4) RtInfo.i4Metric1;
            }
            i4RtStatus = rip_add_route (RIP_NTOHL (RIP_DEST_NET (pRipInfo)),
                                        RIP_ROUTE_TAG (pRipInfo), u4SubNetMask,
                                        u2If, u4GwAddr,
                                        RIP_NTOHL (RIP_NEXT_HOP (pRipInfo)),
                                        u4Metric, RIP_RT_INDIRECT,
                                        u1Distance,
                                        pRipIfRec->RipIfaceCfg.u4SrcAddr,
                                        pRipCxtEntry);

            if (i4RtStatus == RIP_FAILURE)
            {
                RIP_TRC (RIP_MOD_TRC,
                         pRipCxtEntry->u4RipTrcFlag,
                         pRipCxtEntry->i4CxtId,
                         ALL_FAILURE_TRC, RIP_NAME,
                         "Failed while adding Routes\n");
                (pRipIfRec->RipIfaceStats.u4RipFailRouteAddCount)++;
            }

            if (i4RtStatus == RIP_TRIGGER_UPDATE)
            {
                /* number of route changes made to ip route database by rip */
                pRipCxtEntry->RipGblStats.u4GlobalRouteChanges++;
            }

            /*
             * Just check for triggered update needs to be generated or not.
             */

            if (i4RtStatus == RIP_TRIGGER_UPDATE)
            {
                u1BcastFlag = RIP_TRIG_UPDATE;
            }

            /*
             * if there is no change in the database, we might as well proceed
             * to the next route that is received.
             */

        }                        /* End of if (rip_validate_route_info) check */

        else
        {
            if (i4RtStatus == RIP_FAILURE)
            {
                RIP_TRC (RIP_MOD_TRC,
                         pRipCxtEntry->u4RipTrcFlag,
                         pRipCxtEntry->i4CxtId,
                         ALL_FAILURE_TRC, RIP_NAME,
                         "Failed while validating route information\n");
                if (pRipIfRec != NULL)
                {
                    (pRipIfRec->RipIfaceStats.
                     u4RipValidateRouteInfoFailCount)++;
                }
            }

            if (i4RtStatus != RIP_DONT_INSTALL_DEF_RT)
            {
                if (pRipIfRec != NULL)
                {
                    (pRipIfRec->RipIfaceStats.u4InBadRoutes)++;

                    (pRipIfRec->RipIfaceStats.u4InBadRoutes2)++;
                }
                u2PeerRcvBadRts++;    /* Increment the statistics  */
            }
        }

    }                            /* End of for ( u2RouteNo ) */

    /*
     * We have to update the peer record statictics for the current packet that
     * was received.
     * We need to update the peer record correspondingly and this is the case
     * for updation of ReceivedBadRoutes for this peer through this port. The
     * other variant parameter ReceivedBadPackets will be passed with a 0 
     * value */

    if (rip_update_peer_record (u2If, u4Src, au1RtTag, u1Version,
                                0, u2PeerRcvBadRts, u4SeqNo,
                                u1KeyId, pRipCxtEntry) == RIP_FAILURE)
    {
        RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                 pRipCxtEntry->i4CxtId,
                 ALL_FAILURE_TRC, RIP_NAME,
                 "Updation of peer record statistics for the current packet Failed\n");
        (pRipIfRec->RipIfaceStats.u4RipFailUpdatePeerCount)++;
        return RIP_FAILURE;
    }

    if (u1BcastFlag == RIP_TRIG_UPDATE)
    {

        return RIP_TRIGGER_UPDATE;
    }
    return RIP_SUCCESS;
}

/*******************************************************************************

    Function        :   rip_is_poison_reverse_applicable.

    Description        :   This function checks whether poison reverse is
                applicable for the given route block based on the
                configuration parameters and returns the status
                accordingly.

    Input Parameters    :   pRipInfoBlk, the route information block.

    Output Parameters    :   None.

    Global Variables
    Affected        :   Route structure, in the route database, is affected
                if modification occurred in the IfCounter value.

    Return Value    :   Rip_pois_rev_applicable |
                Rip_pois_rev_not_applicable.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_is_poison_reverse_applicable
    $$TRACE_PROCEDURE_LEVEL = INTMD
*******************************************************************************/
INT4
rip_is_poison_reverse_applicable (tRipInfoBlk * pRipInfoBlk, UINT2 u2If,
                                  tRipCxt * pRipCxtEntry)
{
    INT4                i4RetVal;
    tRipIfaceRec       *pRipIfRec = NULL;

    pRipIfRec = RipGetIfRec ((UINT4) u2If, pRipCxtEntry);

    if (pRipIfRec == NULL)
    {
        return RIP_FAILURE;
    }

    i4RetVal = RIP_POIS_REV_NOT_APPLICABLE;
    if ((pRipIfRec->RipIfaceCfg.u2SplitHorizonOperStatus) ==
        RIP_NO_SPLIT_HORIZON)
    {
        i4RetVal = RIP_NO_SPLIT_HOR_APPLICABLE;
    }
    else if ((pRipIfRec->RipIfaceCfg.u2SplitHorizonOperStatus) !=
             RIP_SPLIT_HORIZON)
    {

        /*
         * Check the global reset timer on whether it is active or not.
         */
        if (pRipIfRec->i2ResetCount <= RIP_MAX_RESET_COUNT)
        {

            i4RetVal = RIP_POIS_REV_APPLICABLE;
        }

        /* Check the ifCounter for this route. */
        if (pRipInfoBlk->u2IfCounter == 0)
        {

            i4RetVal = RIP_POIS_REV_APPLICABLE;
        }
    }
    return i4RetVal;
}

/* Added for getting RIP statistics */
INT4
RipGetIfStatsPeriodicUpdates (UINT4 u4RipIfAddr, UINT4 *pu4PeriodicUpdates,
                              tRipCxt * pRipCxtEntry)
{
    tRipIfaceRec       *pRipIfRec = NULL;

    pRipIfRec = RipGetIfRecFromAddr (u4RipIfAddr, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return RIP_FAILURE;
    }

    *pu4PeriodicUpdates = (pRipIfRec->RipIfaceStats.u4SentPeriodicUpdates);
    return RIP_SUCCESS;
}

/* Added for getting RIP statistics */
INT4
RipGetIfStatsRequestReceived (UINT4 u4RipIfAddr, UINT4 *pu4RequestReceived,
                              tRipCxt * pRipCxtEntry)
{
    tRipIfaceRec       *pRipIfRec = NULL;

    pRipIfRec = RipGetIfRecFromAddr (u4RipIfAddr, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return RIP_FAILURE;
    }

    *pu4RequestReceived = (pRipIfRec->RipIfaceStats.u4InRipRequestReceived);
    return RIP_SUCCESS;
}

INT4
RipGetIfStatsResponseReceived (UINT4 u4RipIfAddr, UINT4 *pu4ResponseReceived,
                               tRipCxt * pRipCxtEntry)
{
    tRipIfaceRec       *pRipIfRec = NULL;

    pRipIfRec = RipGetIfRecFromAddr (u4RipIfAddr, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return RIP_FAILURE;
    }

    *pu4ResponseReceived = (pRipIfRec->RipIfaceStats.u4InRipResponseProcessed);
    return RIP_SUCCESS;
}

INT4
RipGetIfStatsUpdateRequestReceived (UINT4 u4RipIfAddr,
                                    UINT4 *pu4UpdateRequestReceived,
                                    tRipCxt * pRipCxtEntry)
{
    tRipIfaceRec       *pRipIfRec = NULL;

    pRipIfRec = RipGetIfRecFromAddr (u4RipIfAddr, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return RIP_FAILURE;
    }

    *pu4UpdateRequestReceived =
        (pRipIfRec->RipIfaceStats.u4RipUpdatesRequestReceived);
    return RIP_SUCCESS;
}

INT4
RipGetIfStatsUpdateResponseReceived (UINT4 u4RipIfAddr,
                                     UINT4 *pu4UpdateResponseReceived,
                                     tRipCxt * pRipCxtEntry)
{
    tRipIfaceRec       *pRipIfRec = NULL;

    pRipIfRec = RipGetIfRecFromAddr (u4RipIfAddr, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return RIP_FAILURE;
    }

    *pu4UpdateResponseReceived =
        (pRipIfRec->RipIfaceStats.u4RipUpdatesResponseReceived);
    return RIP_SUCCESS;
}

INT4
RipGetIfStatsUpdatesAcknowledged (UINT4 u4RipIfAddr,
                                  UINT4 *pu4UpdatesAcknowledged,
                                  tRipCxt * pRipCxtEntry)
{
    tRipIfaceRec       *pRipIfRec = NULL;

    pRipIfRec = RipGetIfRecFromAddr (u4RipIfAddr, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return RIP_FAILURE;
    }

    *pu4UpdatesAcknowledged =
        (pRipIfRec->RipIfaceStats.u4RipUpdatesAcknowledged);
    return RIP_SUCCESS;
}

INT4
RipGetIfStatsRipFailcount (UINT4 u4RipIfAddr, tRipInFailCount * pInFailcount,
                           tRipCxt * pRipCxtEntry)
{
    tRipIfaceRec       *pRipIfRec = NULL;

    pRipIfRec = RipGetIfRecFromAddr (u4RipIfAddr, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return RIP_FAILURE;
    }
    pInFailcount->u4RipV2NoAuthFailCount =
        (pRipIfRec->RipIfaceStats.u4RipV2NoAuthFailCount);
    pInFailcount->u4RipV1SimplePasswdFailCount =
        (pRipIfRec->RipIfaceStats.u4RipV1SimplePasswdFailCount);
    pInFailcount->u4RipV2UnAuthenticatedPktFailCount =
        (pRipIfRec->RipIfaceStats.u4RipV2UnAuthenticatedPktFailCount);
    pInFailcount->u4RipV2SimplePasswdFailCount =
        (pRipIfRec->RipIfaceStats.u4RipV2SimplePasswdFailCount);
    pInFailcount->u4RipV1Md5AuthenticatedPktFailCount =
        (pRipIfRec->RipIfaceStats.u4RipV1Md5AuthenticatedPktFailCount);
    pInFailcount->u4RipV2Md5UnAuthenticatedPktFailCount =
        (pRipIfRec->RipIfaceStats.u4RipV2Md5UnAuthenticatedPktFailCount);
    pInFailcount->u4RipV2Md5InvalidLengthPktFailCount =
        (pRipIfRec->RipIfaceStats.u4RipV2Md5InvalidLengthPktFailCount);
    pInFailcount->u4RipV2Md5InvalidKeyFailCount =
        (pRipIfRec->RipIfaceStats.u4RipV2Md5InvalidKeyFailCount);
    pInFailcount->u4RipV2Md5InvalidDigestFailCount =
        (pRipIfRec->RipIfaceStats.u4RipV2Md5InvalidDigestFailCount);
    pInFailcount->u4RipBadUpdateHeaderFailCount =
        (pRipIfRec->RipIfaceStats.u4RipBadUpdateHeaderFailCount);
    pInFailcount->u4RipValidatePacketFailCount =
        (pRipIfRec->RipIfaceStats.u4RipValidatePacketFailCount);
    pInFailcount->u4RipTrailingJunkFailCount =
        (pRipIfRec->RipIfaceStats.u4RipTrailingJunkFailCount);
    pInFailcount->u4RipValidateHost1FailCount =
        (pRipIfRec->RipIfaceStats.u4RipValidateHost1FailCount);
    pInFailcount->u4RipValidateHost2FailCount =
        (pRipIfRec->RipIfaceStats.u4RipValidateHost2FailCount);
    pInFailcount->u4RipValidateHost3FailCount =
        (pRipIfRec->RipIfaceStats.u4RipValidateHost3FailCount);
    pInFailcount->u4RipV2NoAuthFailCount =
        (pRipIfRec->RipIfaceStats.u4RipV2NoAuthFailCount);
    pInFailcount->u4RipValidatePkt1FailCount =
        (pRipIfRec->RipIfaceStats.u4RipValidatePkt1FailCount);
    pInFailcount->u4RipValidatePkt2FailCount =
        (pRipIfRec->RipIfaceStats.u4RipValidatePkt2FailCount);
    pInFailcount->u4RipValidatePkt3FailCount =
        (pRipIfRec->RipIfaceStats.u4RipValidatePkt3FailCount);
    pInFailcount->u4RipProcessRequest1FailCount =
        (pRipIfRec->RipIfaceStats.u4RipProcessRequest1FailCount);
    pInFailcount->u4RipProcessRequest2FailCount =
        (pRipIfRec->RipIfaceStats.u4RipProcessRequest2FailCount);
    pInFailcount->u4RipProcessRequest3FailCount =
        (pRipIfRec->RipIfaceStats.u4RipProcessRequest3FailCount);
    pInFailcount->u4RipProcessRequest4FailCount =
        (pRipIfRec->RipIfaceStats.u4RipProcessRequest4FailCount);
    pInFailcount->u4RipFailUpdatePeerCount =
        (pRipIfRec->RipIfaceStats.u4RipFailUpdatePeerCount);
    pInFailcount->u4RipValidateRouteInfoFailCount =
        (pRipIfRec->RipIfaceStats.u4RipValidateRouteInfoFailCount);
    pInFailcount->u4RipSendUpdateAndStartSpaceTmrFailCount =
        (pRipIfRec->RipIfaceStats.u4RipSendUpdateAndStartSpaceTmrFailCount);
    pInFailcount->u4RipFailRouteAddCount =
        (pRipIfRec->RipIfaceStats.u4RipFailRouteAddCount);
    /* more rip counters for agg route failures */
    pInFailcount->u4RipAggTblInitFailCount =
        (pRipIfRec->RipIfaceStats.u4RipAggTblInitFailCount);
    pInFailcount->u4RipDelIfAggNullEntryCount =
        (pRipIfRec->RipIfaceStats.u4RipDelIfAggNullEntryCount);
    pInFailcount->u4RipAggDelTrieFailCount =
        (pRipIfRec->RipIfaceStats.u4RipAggDelTrieFailCount);
    pInFailcount->u4RipAggIntfSpecificDelTrieFailCount =
        (pRipIfRec->RipIfaceStats.u4RipAggIntfSpecificDelTrieFailCount);

    pInFailcount->u4RipAggChkLimitFailCount =
        (pRipIfRec->RipIfaceStats.u4RipAggChkLimitFailCount);
    pInFailcount->u4RipDelIfAggRouteFailCount =
        (pRipIfRec->RipIfaceStats.u4RipDelIfAggRouteFailCount);

    pInFailcount->u4RipDelIfAggTbl1FailCount =
        (pRipIfRec->RipIfaceStats.u4RipDelIfAggTbl1FailCount);
    pInFailcount->u4RipDelIfAggTbl2FailCount =
        (pRipIfRec->RipIfaceStats.u4RipDelIfAggTbl2FailCount);
    pInFailcount->u4RipSplitHorizonFailCount =
        (pRipIfRec->RipIfaceStats.u4RipSplitHorizonFailCount);
    pInFailcount->u4RipIfAggRtNullEntryCount =
        (pRipIfRec->RipIfaceStats.u4RipIfAggRtNullEntryCount);
    pInFailcount->u4RipGenerateUpdate1FailCount =
        (pRipIfRec->RipIfaceStats.u4RipGenerateUpdate1FailCount);
    pInFailcount->u4RipGenerateUpdate2FailCount =
        (pRipIfRec->RipIfaceStats.u4RipGenerateUpdate2FailCount);
    pInFailcount->u4RipGenerateUpdate3FailCount =
        (pRipIfRec->RipIfaceStats.u4RipGenerateUpdate3FailCount);
    pInFailcount->u4RipGenerateUpdate4FailCount =
        (pRipIfRec->RipIfaceStats.u4RipGenerateUpdate4FailCount);
    pInFailcount->u4RipSentUpdate1FailCount =
        (pRipIfRec->RipIfaceStats.u4RipSentUpdate1FailCount);
    pInFailcount->u4RipSentUpdate2FailCount =
        (pRipIfRec->RipIfaceStats.u4RipSentUpdate2FailCount);
    pInFailcount->u4RipSentUpdate3FailCount =
        (pRipIfRec->RipIfaceStats.u4RipSentUpdate3FailCount);
    pInFailcount->u4RipSentUpdate4FailCount =
        (pRipIfRec->RipIfaceStats.u4RipSentUpdate4FailCount);
    pInFailcount->u4RipSentUpdate5FailCount =
        (pRipIfRec->RipIfaceStats.u4RipSentUpdate5FailCount);

    pInFailcount->u4RipUpdateRipPktToSend1FailCount =
        (pRipIfRec->RipIfaceStats.u4RipUpdateRipPktToSend1FailCount);
    pInFailcount->u4RipUpdateRipPktToSend2FailCount =
        (pRipIfRec->RipIfaceStats.u4RipUpdateRipPktToSend2FailCount);
    pInFailcount->u4RipUpdateRipPktToSend3FailCount =
        (pRipIfRec->RipIfaceStats.u4RipUpdateRipPktToSend3FailCount);
    pInFailcount->u4RipUpdateRipPktToSend4FailCount =
        (pRipIfRec->RipIfaceStats.u4RipUpdateRipPktToSend4FailCount);
    pInFailcount->u4RipSendUnicastDatagram1FailCount =
        (pRipIfRec->RipIfaceStats.u4RipSendUnicastDatagram1FailCount);
    pInFailcount->u4RipSendUnicastDatagram2FailCount =
        (pRipIfRec->RipIfaceStats.u4RipSendUnicastDatagram2FailCount);
    pInFailcount->u4RipUpdateRequestFailCount =
        (pRipIfRec->RipIfaceStats.u4RipUpdateRequestFailCount);
    pInFailcount->u4RipUpdateAckFailCount =
        (pRipIfRec->RipIfaceStats.u4RipUpdateAckFailCount);
    pInFailcount->u4RipValidateRouteInfo1FailCount =
        (pRipIfRec->RipIfaceStats.u4RipValidateRouteInfo1FailCount);
    pInFailcount->u4RipValidateRouteInfo2FailCount =
        (pRipIfRec->RipIfaceStats.u4RipValidateRouteInfo2FailCount);
    pInFailcount->u4RipValidateRouteInfo3FailCount =
        (pRipIfRec->RipIfaceStats.u4RipValidateRouteInfo3FailCount);
    pInFailcount->u4RipValidateRouteInfo4FailCount =
        (pRipIfRec->RipIfaceStats.u4RipValidateRouteInfo4FailCount);
    pInFailcount->u4RipValidateRouteInfo5FailCount =
        (pRipIfRec->RipIfaceStats.u4RipValidateRouteInfo5FailCount);
    pInFailcount->u4RipValidateRouteInfo6FailCount =
        (pRipIfRec->RipIfaceStats.u4RipValidateRouteInfo6FailCount);
    pInFailcount->u4RipValidateRouteInfo7FailCount =
        (pRipIfRec->RipIfaceStats.u4RipValidateRouteInfo7FailCount);
    pInFailcount->u4RipConstructAuthInfoFailCount =
        (pRipIfRec->RipIfaceStats.u4RipConstructAuthInfoFailCount);
    pInFailcount->u4RipGetOperStatusFailCount =
        (pRipIfRec->RipIfaceStats.u4RipGetOperStatusFailCount);
    pInFailcount->u4RipGetSendStatusFailCount =
        (pRipIfRec->RipIfaceStats.u4RipGetSendStatusFailCount);
    pInFailcount->u4RipFailCount = (pRipIfRec->RipIfaceStats.u4RipFailCount);

    return RIP_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : RipProcessRespMessages                             */
/*   Description     : This function dequeue messages from queue and      */
/*                     calls the rip_process_response to process response */
/*                     message.                                           */
/*   Input(s)        : None                                               */
/*   Output(s)       : calls rip_process_response funtion.                */
/*   Return Value    : RIP_FAILURE / RIP_SUCCESS                          */
/**************************************************************************/
INT4
RipProcessRespMessages ()
{
    tRipRespQMsg       *pRespQMsg = NULL;
    INT4                i4NumOfMsgs = 0;
    INT4                i4ProcessQMsg = 0;
    tOsixMsg           *pOsixMsg = NULL;

    if (OsixGetNumMsgsInQ ((UINT4) 0,
                           RIP_RESP_Q_NAME,
                           (UINT4 *) &i4ProcessQMsg) != OSIX_SUCCESS)
    {
        RIP_TRC (RIP_MOD_TRC,
                 RIP_TRC_GLOBAL_FLAG,
                 RIP_INVALID_CXT_ID,
                 ALL_FAILURE_TRC, RIP_NAME,
                 "Failed while geeting resource-id by name");
        return RIP_FAILURE;
    }
    if ((i4ProcessQMsg == 0) || (gRipRtr.i4RtCal != RIP_RT_COMPLETE))
    {
        RIP_TRC (RIP_MOD_TRC,
                 RIP_TRC_GLOBAL_FLAG,
                 RIP_INVALID_CXT_ID,
                 ALL_FAILURE_TRC, RIP_NAME, "Failed due to empty queue");
        return RIP_FAILURE;
    }
    /*
     * Process 5% of process messages from the Queue.
     */
    i4ProcessQMsg = (i4ProcessQMsg * 5) / 100;
    while ((i4NumOfMsgs <= i4ProcessQMsg) &&
           (OsixReceiveFromQ ((UINT4) 0,
                              RIP_RESP_Q_NAME,
                              OSIX_NO_WAIT, 0, &pOsixMsg) == OSIX_SUCCESS))
    {
        pRespQMsg = (tRipRespQMsg *) pOsixMsg;
        if (gRipRtr.apRipCxt[pRespQMsg->u4CxtId] != NULL)
        {
            if (rip_process_response (pRespQMsg->u1Version,
                                      &(pRespQMsg->aRipRtInfo[0]),
                                      pRespQMsg->u2Len,
                                      pRespQMsg->u4SrcAddr,
                                      pRespQMsg->u2IfIndex,
                                      pRespQMsg->u4SeqNo,
                                      pRespQMsg->u1KeyId,
                                      gRipRtr.apRipCxt[pRespQMsg->u4CxtId])
                == RIP_TRIGGER_UPDATE)
            {
                RipSendTriggeredUpdate (gRipRtr.apRipCxt[pRespQMsg->u4CxtId]);
            }
        }
        RIP_FREE_RESP_ENTRY (pRespQMsg);
        i4NumOfMsgs++;
    }

    return RIP_SUCCESS;

}

/**************************************************************************/
/*   Function Name   : RipLowPriorityPacket                               */
/*   Description     : If the Packet Type is RESPONSE Packet returns      */
/*                     TRUE */
/*                     message.                                           */
/*   Input(s)        : u1PktType - PacketType.                            */
/*   Output(s)       : None.                                              */
/*   Return Value    : TRUE / FALSE.                                      */
/**************************************************************************/
PRIVATE INT4
RipLowPriorityPacket (UINT1 u1PktType)
{
    if (u1PktType == RIP_RESPONSE)
    {
        return RIP_TRUE;
    }
    return RIP_FALSE;
}

/**************************************************************************/
/*   Function Name   : RipSendUpdateAndStartSpaceTmr.                     */
/*   Description     : Sends the Packet and If Spacing is enabled         */
/*                     starts the space timer.                            */
/*   Input(s)        : u1Version - Packet Version.                        */
/*                     u1BcastFlag - BroadCastFlag                        */
/*                     u4DestNest - Destination Address.                  */
/*                     u2DestUdpPort - Destination Port Number.           */
/*                     tRipInfoBlk  - RipInfiPacket                       */
/*                     u2If - u2IfaceIndex.                               */
/*                     *pu2RtsToCompose - Num of routes 
 *                     to send in the packet                              */
/*                     pu2NoOfPktsSend - NoOf Pkts Send.                  */
/*                     pu2NoOfRoutes - Numer of Route in pRipInfoBlk      */
/*                     pRipIfRec - Pointer to Interface record.           */
/*                     pRipCxtEntry - Context Entry Pointer.              */
/*                     pRtSend - Next Route to be send if Spacing enabled */
/*   Output(s)       : None.                                              */
/*   Return Value    : TRUE / FALSE.                                      */
/**************************************************************************/
INT4
RipSendUpdateAndStartSpaceTmr (UINT1 u1Version, UINT1 u1BcastFlag,
                               UINT4 u4DestNet, UINT2 u2DestUdpPort,
                               tRipInfoBlk * pRipInfoBlk, UINT2 u2If,
                               UINT2 *pu2RtsToCompose, UINT2 *pu2NoOfPktsSend,
                               UINT2 *pu2NoOfRoutes, tRipIfaceRec * pRipIfRec,
                               tRipCxt * pRipCxtEntry, tTMO_DLL_NODE * pRtSend)
{
    if (rip_send_update_datagram (u1Version, u1BcastFlag,
                                  u4DestNet, u2DestUdpPort,
                                  pRipInfoBlk, u2If,
                                  *pu2RtsToCompose,
                                  pRipCxtEntry) == RIP_FAILURE)
    {
        return (RIP_FAILURE);
    }
    (*pu2NoOfPktsSend)++;
    *pu2NoOfRoutes = 0;
    if (((u1BcastFlag == RIP_REG_UPDATE) ||
         (u1BcastFlag == RIP_SPACE_UPDATE)) &&
        (RIP_SPACING_STATUS (pRipCxtEntry) == RIP_SPACING_ENABLE) &&
        (pRipIfRec != NULL) && (pRipIfRec->RipIfaceCfg.u2SpacingTmrVal != 0))
    {
        if ((pRtSend != NULL) && (pRipIfRec->u2NoOfPktsPerSpace
                                  == *pu2NoOfPktsSend))
        {
            pRipIfRec->pRipNextSendRt = pRtSend;
            TMO_DLL_Init_Node ((tTMO_DLL_NODE *) & pRipIfRec->RipIfRecNode);
            TMO_DLL_Add (&(pRipCxtEntry->RipIfSpaceLst),
                         (tTMO_DLL_NODE *) & pRipIfRec->RipIfRecNode);
            RIP_START_TIMER (RIP_TIMER_ID,
                             &(pRipIfRec->RipSpacingTimer.
                               TimerNode),
                             pRipIfRec->RipIfaceCfg.u2SpacingTmrVal);
            return RIP_SPACE_START;
        }
    }
    return RIP_SUCCESS;
}
