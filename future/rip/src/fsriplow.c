/******************************************************************************
 * Copyright (C) Future Software Limited, 2001
 *
 * $Id: fsriplow.c,v 1.18 
 *
 * Description:
 *
 *****************************************************************************/
#include "ripinc.h"
#include "fsmiricli.h"
#include "fsmistdripcli.h"
#ifdef L3_SWITCHING_WANTED
#include "ipnp.h"
#endif
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRipAdminStatus
 Input       :  The Indices

                The Object
                retValFsRipAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRipAdminStatus (INT4 *pi4RetValFsRipAdminStatus)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRipAdminStatus = pRipCxtEntry->u1AdminStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRipRtCount
 Input       :  The Indices

                The Object 
                retValFsRipRtCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRipRtCount (INT4 *pi4RetValFsRipRtCount)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRipRtCount =
        (INT4) (pRipCxtEntry->u4TotalEntryCount +
                pRipCxtEntry->u4TotalSummaryCount);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRip2RtIfIndex
 Input       :  The Indices
                FsRip2DestNet
                FsRip2DestMask
                FsRip2Tos
                FsRip2NextHop

                The Object 
                retValFsRip2RtIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRip2RtIfIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRip2RtIfIndex (UINT4 u4FsRip2DestNet, UINT4 u4FsRip2DestMask,
                       INT4 i4FsRip2Tos, UINT4 u4FsRip2NextHop,
                       INT4 *pi4RetValFsRip2RtIfIndex)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (RipLocalRtTblGetObject
        (u4FsRip2DestNet, u4FsRip2DestMask, i4FsRip2Tos,
         u4FsRip2NextHop, (UINT4 *) pi4RetValFsRip2RtIfIndex,
         RIP_ROUTE_IFINDEX, pRipCxtEntry) == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRip2RtType
 Input       :  The Indices
                FsRip2DestNet
                FsRip2DestMask
                FsRip2Tos
                FsRip2NextHop

                The Object 
                retValFsRip2RtType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRip2RtType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRip2RtType (UINT4 u4FsRip2DestNet, UINT4 u4FsRip2DestMask,
                    INT4 i4FsRip2Tos, UINT4 u4FsRip2NextHop,
                    INT4 *pi4RetValFsRip2RtType)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (RipLocalRtTblGetObject
        (u4FsRip2DestNet, u4FsRip2DestMask, i4FsRip2Tos,
         u4FsRip2NextHop, (UINT4 *) pi4RetValFsRip2RtType,
         RIP_ROUTE_RTTYPE, pRipCxtEntry) == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRip2Proto
 Input       :  The Indices
                FsRip2DestNet
                FsRip2DestMask
                FsRip2Tos
                FsRip2NextHop

                The Object 
                retValFsRip2Proto
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRip2Proto ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRip2Proto (UINT4 u4FsRip2DestNet, UINT4 u4FsRip2DestMask,
                   INT4 i4FsRip2Tos, UINT4 u4FsRip2NextHop,
                   INT4 *pi4RetValFsRip2Proto)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (RipLocalRtTblGetObject
        (u4FsRip2DestNet, u4FsRip2DestMask, i4FsRip2Tos,
         u4FsRip2NextHop, (UINT4 *) pi4RetValFsRip2Proto,
         RIP_ROUTE_PROTO, pRipCxtEntry) == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRip2ChgTime
 Input       :  The Indices
                FsRip2DestNet
                FsRip2DestMask
                FsRip2Tos
                FsRip2NextHop

                The Object 
                retValFsRip2ChgTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRip2ChgTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRip2ChgTime (UINT4 u4FsRip2DestNet, UINT4 u4FsRip2DestMask,
                     INT4 i4FsRip2Tos, UINT4 u4FsRip2NextHop,
                     INT4 *pi4RetValFsRip2ChgTime)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (RipLocalRtTblGetObject
        (u4FsRip2DestNet, u4FsRip2DestMask, i4FsRip2Tos,
         u4FsRip2NextHop, (UINT4 *) pi4RetValFsRip2ChgTime,
         RIP_ROUTE_CHGTIME, pRipCxtEntry) == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRip2Metric
 Input       :  The Indices
                FsRip2DestNet
                FsRip2DestMask
                FsRip2Tos
                FsRip2NextHop

                The Object 
                retValFsRip2Metric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRip2Metric ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRip2Metric (UINT4 u4FsRip2DestNet, UINT4 u4FsRip2DestMask,
                    INT4 i4FsRip2Tos, UINT4 u4FsRip2NextHop,
                    INT4 *pi4RetValFsRip2Metric)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (RipLocalRtTblGetObject
        (u4FsRip2DestNet, u4FsRip2DestMask, i4FsRip2Tos,
         u4FsRip2NextHop, (UINT4 *) pi4RetValFsRip2Metric,
         RIP_ROUTE_METRIC, pRipCxtEntry) == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRip2RowStatus
 Input       :  The Indices
                FsRip2DestNet
                FsRip2DestMask
                FsRip2Tos
                FsRip2NextHop

                The Object 
                retValFsRip2RowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRip2RowStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRip2RowStatus (UINT4 u4FsRip2DestNet, UINT4 u4FsRip2DestMask,
                       INT4 i4FsRip2Tos, UINT4 u4FsRip2NextHop,
                       INT4 *pi4RetValFsRip2RowStatus)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (RipLocalRtTblGetObject
        (u4FsRip2DestNet, u4FsRip2DestMask, i4FsRip2Tos,
         u4FsRip2NextHop, (UINT4 *) pi4RetValFsRip2RowStatus,
         RIP_ROUTE_ROWSTATUS, pRipCxtEntry) == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRip2Gateway
 Input       :  The Indices
                FsRip2DestNet
                FsRip2DestMask
                FsRip2Tos
                FsRip2NextHop

                The Object 
                retValFsRip2Gateway
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRip2Gateway ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRip2Gateway (UINT4 u4FsRip2DestNet, UINT4 u4FsRip2DestMask,
                     INT4 i4FsRip2Tos, UINT4 u4FsRip2NextHop,
                     UINT4 *pu4RetValFsRip2Gateway)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (RipLocalRtTblGetObject
        (u4FsRip2DestNet, u4FsRip2DestMask, i4FsRip2Tos,
         u4FsRip2NextHop, pu4RetValFsRip2Gateway,
         RIP_ROUTE_GATEWAY, pRipCxtEntry) == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;
}

/*************************************************************************/
/* Function Name     :  RipLocalRtTblGetObject                              */
/*                                                                       */
/* Description       :  This fuction does a GET Object in  the  IP Cidr  */
/*                      forwarding table based on the Object type        */
/*                      specified.                                       */
/*                                                                       */
/* Global Variables  :  None                                             */
/*                                                                       */
/* Input(s)          :  u4Dest - The Destination address.                */
/*                      u4Mask - The Desination Mask                     */
/*                      i4Tos  - The Type of service                     */
/*                      u4NextHop - The Next hop gateway address         */
/*                      i4Proto -   The routing protocol                 */
/*                      u1ObjType - The Object type being accessed       */
/*                                                                       */
/* Output(s)         :  *pi4Val - The Value of the object for which the  */
/*                      GET has been performed                           */
/*                                                                       */
/* Returns           :  SNMP_SUCCESS | SNMP_FAILURE                      */
/*************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = IpFwdTblGetObject                        ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                     ***/
/*************************************************************************/

INT4
RipLocalRtTblGetObject (UINT4 u4Dest, UINT4 u4Mask, INT4 i4Tos,
                        UINT4 u4NextHop, UINT4 *pu4Val, INT4 i4ObjType,
                        tRipCxt * pRipCxtEntry)
{
    UINT1               u1FoundFlag;
    UINT4               au4Indx[2];    /*The Key of Trie is made-up of address and mask */
    INT4                i4count;
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRipRtEntry        *pRt = NULL;
    tRipRtInfo         *apAppSpecInfo[MAX_ROUTING_PROTOCOLS];

    au4Indx[0] = IP_HTONL (u4Dest);
    au4Indx[1] = IP_HTONL (u4Mask);
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    InParams.i1AppId = ALL_ROUTING_PROTOCOL;
    InParams.pRoot = pRipCxtEntry->pRipRoot;

    OutParams.Key.pKey = NULL;
    MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * (sizeof (tRtInfo *)));
    OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;

    if (TrieSearchEntry (&(InParams), &(OutParams)) == TRIE_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* Check whether we have the route for the given index */
    u1FoundFlag = FALSE;
    for (i4count = 0; i4count < MAX_ROUTING_PROTOCOLS; i4count++)
    {
        if (apAppSpecInfo[i4count] != NULL)
        {
            pRt = ((tRipRtEntry **) OutParams.pAppSpecInfo)[i4count];
        }
        if (pRt != NULL)
        {
            while ((pRt != NULL) && (u1FoundFlag == FALSE))
            {
                /* Makefile Changes - comparison btwn signed and unsigned */
                if ((pRt->RtInfo.u4NextHop == u4NextHop)
                    && (pRt->RtInfo.u4Tos == (UINT4) i4Tos))
                {
                    u1FoundFlag = TRUE;
                }
                else
                {
                    pRt = pRt->pNextAlternatepath;
                }
            }
            if (u1FoundFlag == TRUE)
            {
                break;
            }
        }
    }

    if ((u1FoundFlag == FALSE) || (pRt == NULL))
    {
        return SNMP_FAILURE;
    }

    switch (i4ObjType)
    {

        case RIP_ROUTE_IFINDEX:
            *pu4Val = pRt->RtInfo.u4RtIfIndx;
            break;

        case RIP_ROUTE_RTTYPE:
            *pu4Val = pRt->RtInfo.u2RtType;
            break;

        case RIP_ROUTE_PROTO:
            *pu4Val = pRt->RtInfo.u2RtProto;
            break;

        case RIP_ROUTE_CHGTIME:
            OsixGetSysTime (pu4Val);
            *pu4Val = pRt->RtInfo.u4ChgTime;
            break;

        case RIP_ROUTE_METRIC:
            *pu4Val = (UINT4) (pRt->RtInfo.i4Metric1);
            break;

        case RIP_ROUTE_ROWSTATUS:
            *pu4Val = pRt->RtInfo.u4RowStatus;
            break;

        case RIP_ROUTE_GATEWAY:
            *pu4Val = pRt->RtInfo.u4Gw;
            break;

        default:
            return SNMP_FAILURE;
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRip2LocalRoutingTable
 Input       :  The Indices
                FsRip2DestNet
                FsRip2DestMask
                FsRip2Tos
                FsRip2NextHop
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceFsRip2LocalRoutingTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhValidateIndexInstanceFsRip2LocalRoutingTable (UINT4 u4FsRip2DestNet,
                                                 UINT4 u4FsRip2DestMask,
                                                 INT4 i4FsRip2Tos,
                                                 UINT4 u4FsRip2NextHop)
{
    if (RIP_IS_ADDR_CLASS_D (u4FsRip2DestNet) ||
        RIP_IS_ADDR_CLASS_E (u4FsRip2DestNet) ||
        RIP_IS_ADDR_CLASS_D (u4FsRip2NextHop) ||
        ((u4FsRip2DestNet & u4FsRip2DestMask) != u4FsRip2DestNet))
    {
        return SNMP_FAILURE;
    }

    if (i4FsRip2Tos < 0)
    {
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRip2LocalRoutingTable
 Input       :  The Indices
                FsRip2DestNet
                FsRip2DestMask
                FsRip2Tos
                FsRip2NextHop
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstFsRip2LocalRoutingTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFirstIndexFsRip2LocalRoutingTable (UINT4 *pu4FsRip2DestNet,
                                         UINT4 *pu4FsRip2DestMask,
                                         INT4 *pi4FsRip2Tos,
                                         UINT4 *pu4FsRip2NextHop)
{
    INT4                i4OutCome;
    INT4                i4count;
    UINT4               au4Indx[2];
    /* Index of Trie is made-up of address and mask */
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRtInfo            *apAppSpecInfo[MAX_ROUTING_PROTOCOLS];
    tRipRtEntry        *pTmpRt = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    au4Indx[0] = 0;
    au4Indx[1] = 0;
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    InParams.pRoot = pRipCxtEntry->pRipRoot;
    InParams.i1AppId = ALL_ROUTING_PROTOCOL;

    OutParams.Key.pKey = NULL;

    MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * (sizeof (tRtInfo *)));

    OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;

    i4OutCome = TrieSearchEntry (&(InParams), &(OutParams));

    if (i4OutCome == TRIE_FAILURE)
    {
        i4OutCome = TrieGetNextEntry (&(InParams), &(OutParams));
    }

    if (i4OutCome == TRIE_SUCCESS)
    {
        /* route is present */
        for (i4count = 0; i4count < MAX_ROUTING_PROTOCOLS; i4count++)
        {
            pTmpRt = ((tRipRtEntry **) OutParams.pAppSpecInfo)[i4count];
            /*
             * Find out if we have this route already in the database.
             * Next Hop uniquely identifies a route.
             */
            if (pTmpRt != NULL)
            {

                *pu4FsRip2DestNet = pTmpRt->RtInfo.u4DestNet;
                *pu4FsRip2DestMask = pTmpRt->RtInfo.u4DestMask;
                *pi4FsRip2Tos = (INT4) pTmpRt->RtInfo.u4Tos;
                *pu4FsRip2NextHop = pTmpRt->RtInfo.u4NextHop;
                break;
            }
        }
    }
    else
    {
        return SNMP_FAILURE;
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRip2LocalRoutingTable
 Input       :  The Indices
                FsRip2DestNet
                nextFsRip2DestNet
                FsRip2DestMask
                nextFsRip2DestMask
                FsRip2Tos
                nextFsRip2Tos
                FsRip2NextHop
                nextFsRip2NextHop
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextFsRip2LocalRoutingTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

INT1
nmhGetNextIndexFsRip2LocalRoutingTable (UINT4 u4FsRip2DestNet,
                                        UINT4 *pu4NextFsRip2DestNet,
                                        UINT4 u4FsRip2DestMask,
                                        UINT4 *pu4NextFsRip2DestMask,
                                        INT4 i4FsRip2Tos,
                                        INT4 *pi4NextFsRip2Tos,
                                        UINT4 u4FsRip2NextHop,
                                        UINT4 *pu4NextFsRip2NextHop)
{
    INT4                i4OutCome;
    INT4                i4Count;
    UINT4               au4Indx[2];    /* The Key of Trie is made-up of address and mask */
    UINT1               u1FoundFlag = FALSE;
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRtInfo            *apAppSpecInfo[MAX_ROUTING_PROTOCOLS];
    tRipRtEntry        *pTmpRt = NULL;
    tRipRtEntry        *pNxtRt = NULL;
    tRipRtEntry        *pAltRt = NULL;
    tRipRtEntry        *pBestRt = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Makefile Changes - unused parameter */
    UNUSED_PARAM (i4FsRip2Tos);
    au4Indx[0] = IP_HTONL (u4FsRip2DestNet);
    au4Indx[1] = IP_HTONL (u4FsRip2DestMask);
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    InParams.i1AppId = ALL_ROUTING_PROTOCOL;
    InParams.pRoot = pRipCxtEntry->pRipRoot;

    OutParams.Key.pKey = NULL;
    MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * (sizeof (tRtInfo *)));
    OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;
    /* Scan the route table */

    i4OutCome = TrieSearchEntry (&(InParams), &(OutParams));

    if (i4OutCome == TRIE_SUCCESS)
    {
        /* This function returns only the RIP routes. Look for RIP_ID routes. */
        pTmpRt = ((tRipRtEntry **) OutParams.pAppSpecInfo)[RIP_ID - 1];
        if (pTmpRt != NULL)
        {
            pAltRt = pTmpRt->pNextAlternatepath;
            if ((pAltRt != NULL) &&
                (pAltRt->RtInfo.i4Metric1 == pTmpRt->RtInfo.i4Metric1))
            {
                /* ECMP routes : return the route in the ascending order
                 * of nexthop*/
                pNxtRt = pTmpRt;
                while ((pNxtRt != NULL) &&
                       (pTmpRt->RtInfo.i4Metric1 == pNxtRt->RtInfo.i4Metric1))
                {
                    if (u1FoundFlag == FALSE)
                    {
                        if (pNxtRt->RtInfo.u4NextHop > u4FsRip2NextHop)
                        {
                            pBestRt = pNxtRt;
                            u1FoundFlag = TRUE;
                        }
                    }
                    else
                    {
                        if ((pNxtRt->RtInfo.u4NextHop > u4FsRip2NextHop) &&
                            (pNxtRt->RtInfo.u4NextHop <
                             pBestRt->RtInfo.u4NextHop))
                        {
                            pBestRt = pNxtRt;
                        }
                    }
                    pNxtRt = pNxtRt->pNextAlternatepath;
                }

                if (u1FoundFlag == TRUE)
                {
                    *pu4NextFsRip2DestNet = pBestRt->RtInfo.u4DestNet;
                    *pu4NextFsRip2DestMask = pBestRt->RtInfo.u4DestMask;
                    *pi4NextFsRip2Tos = (INT4) pBestRt->RtInfo.u4Tos;
                    *pu4NextFsRip2NextHop = pBestRt->RtInfo.u4NextHop;
                    return (SNMP_SUCCESS);
                }
            }
            else
            {
                if (u4FsRip2NextHop == 0)
                {
                    *pu4NextFsRip2DestNet = pTmpRt->RtInfo.u4DestNet;
                    *pu4NextFsRip2DestMask = pTmpRt->RtInfo.u4DestMask;
                    *pi4NextFsRip2Tos = (INT4) pTmpRt->RtInfo.u4Tos;
                    *pu4NextFsRip2NextHop = pTmpRt->RtInfo.u4NextHop;
                    return (SNMP_SUCCESS);
                }
            }
        }
    }

    /* Search on alternate routes failed. Proceed with next trie entry. */
    pTmpRt = NULL;
    OutParams.Key.pKey = NULL;
    MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * (sizeof (tRtInfo *)));

    i4OutCome = TrieGetNextEntry (&(InParams), &(OutParams));

    if (i4OutCome == TRIE_SUCCESS)
    {
        for (i4Count = 0; i4Count < MAX_ROUTING_PROTOCOLS; i4Count++)

        {
            pTmpRt = ((tRipRtEntry **) OutParams.pAppSpecInfo)[i4Count];

            if (pTmpRt != NULL)
            {
                /* Found the route. */
                *pu4NextFsRip2DestNet = pTmpRt->RtInfo.u4DestNet;
                *pu4NextFsRip2DestMask = pTmpRt->RtInfo.u4DestMask;
                *pi4NextFsRip2Tos = (INT4) pTmpRt->RtInfo.u4Tos;
                *pu4NextFsRip2NextHop = pTmpRt->RtInfo.u4NextHop;
                return (SNMP_SUCCESS);
            }

        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsRipAdminStatus
 Input       :  The Indices

                The Object
                setValFsRipAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRipAdminStatus (INT4 i4SetValFsRipAdminStatus)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pRipCxtEntry->u1AdminStatus == i4SetValFsRipAdminStatus)
    {
        return SNMP_SUCCESS;
    }
    if (i4SetValFsRipAdminStatus == RIP_ADMIN_ENABLE)
    {
        if (RipEnableAdminStat (pRipCxtEntry) == RIP_FAILURE)
        {
            return SNMP_FAILURE;
        }
#ifdef L3_SWITCHING_WANTED
        RipFsNpRipInit ();
#endif
    }

    if (i4SetValFsRipAdminStatus == RIP_ADMIN_DISABLE)
    {
        if (RipDisableAdminStat (pRipCxtEntry) == RIP_FAILURE)
        {
            return SNMP_FAILURE;
        }
#ifdef L3_SWITCHING_WANTED
        RipFsNpRipDeInit ();
#endif
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIRipAdminStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIRipAdminStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", pRipCxtEntry->i4CxtId,
                      i4SetValFsRipAdminStatus));
#endif

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRipAdminStatus
 Input       :  The Indices

                The Object
                testValFsRipAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRipAdminStatus (UINT4 *pu4ErrorCode, INT4 i4TestValFsRipAdminStatus)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsRipAdminStatus < RIP_ADMIN_ENABLE) ||
        (i4TestValFsRipAdminStatus > RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRipAdminStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRipAdminStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRip2LastAuthKeyLifetimeStatus
 Input       :  The Indices

                The Object 
                retValFsRip2LastAuthKeyLifetimeStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsRip2LastAuthKeyLifetimeStatus
    (INT4 *pi4RetValFsRip2LastAuthKeyLifetimeStatus)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRip2LastAuthKeyLifetimeStatus =
        pRipCxtEntry->b1LastKeyLifeTimeStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRip2LastAuthKeyLifetimeStatus
 Input       :  The Indices

                The Object 
                setValFsRip2LastAuthKeyLifetimeStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsRip2LastAuthKeyLifetimeStatus
    (INT4 i4SetValFsRip2LastAuthKeyLifetimeStatus)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRipCxtEntry->b1LastKeyLifeTimeStatus =
        (BOOL1) i4SetValFsRip2LastAuthKeyLifetimeStatus;
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIRip2LastAuthKeyLifetimeStatus;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsMIRip2LastAuthKeyLifetimeStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", pRipCxtEntry->i4CxtId,
                      i4SetValFsRip2LastAuthKeyLifetimeStatus));
#endif
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRip2LastAuthKeyLifetimeStatus
 Input       :  The Indices

                The Object 
                testValFsRip2LastAuthKeyLifetimeStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsRip2LastAuthKeyLifetimeStatus
    (UINT4 *pu4ErrorCode, INT4 i4TestValFsRip2LastAuthKeyLifetimeStatus)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsRip2LastAuthKeyLifetimeStatus < RIP_FALSE) ||
        (i4TestValFsRip2LastAuthKeyLifetimeStatus > RIP_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRip2LastAuthKeyLifetimeStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRip2LastAuthKeyLifetimeStatus (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsRipDistInOutRouteMapTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRipDistInOutRouteMapTable
 Input       :  The Indices
                FsRipDistInOutRouteMapName
                FsRipDistInOutRouteMapType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRipDistInOutRouteMapTable (tSNMP_OCTET_STRING_TYPE *
                                                     pFsRipDistInOutRouteMapName,
                                                     INT4
                                                     i4FsRipDistInOutRouteMapType)
{
#ifdef ROUTEMAP_WANTED
    tRipCxt            *pRipCxt = RIP_MGMT_CXT;
    if (pRipCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pFsRipDistInOutRouteMapName == NULL
        || pFsRipDistInOutRouteMapName->i4_Length <= 0)
    {
        return SNMP_FAILURE;
    }

    if (i4FsRipDistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
    {
        if (CmpFilterRMapName
            (pRipCxt->pDistanceFilterRMap, pFsRipDistInOutRouteMapName) == 0)
        {
            return SNMP_SUCCESS;
        }
    }
    else if (i4FsRipDistInOutRouteMapType == FILTERING_TYPE_DISTRIB_IN)
    {
        if (CmpFilterRMapName
            (pRipCxt->pDistributeInFilterRMap,
             pFsRipDistInOutRouteMapName) == 0)
        {
            return SNMP_SUCCESS;
        }
    }
    else if (i4FsRipDistInOutRouteMapType == FILTERING_TYPE_DISTRIB_OUT)
    {
        if (CmpFilterRMapName
            (pRipCxt->pDistributeOutFilterRMap,
             pFsRipDistInOutRouteMapName) == 0)
        {
            return SNMP_SUCCESS;
        }
    }
#else
    UNUSED_PARAM (pFsRipDistInOutRouteMapName);
    UNUSED_PARAM (i4FsRipDistInOutRouteMapType);
#endif
    return SNMP_FAILURE;
}

/*Filtering nmh processing utils*/
void                FilteringRMapSortByIndexRip (tRipCxt * pRipCxt,
                                                 tFilteringRMap * ppFiltr[]);
INT4                GetFlterTypeByFlterPtrRip (tRipCxt * pRipCxt,
                                               tFilteringRMap * pFltr1);
INT4                FilteringRMapCompareRip (tRipCxt * pRipCxt,
                                             tFilteringRMap * pFltr1,
                                             tFilteringRMap * pFltr2);

#define MAX_FILTER_AMOUNT 3

/****************************************************************************
 Function    :  GetFlterTypeByFlterPtrRip
 Input       :  pFltr1 filter to search
 Output      :  none
 Returns     :  positive fisrt bigger, negative second one bigger
****************************************************************************/
INT4
GetFlterTypeByFlterPtrRip (tRipCxt * pRipCxt, tFilteringRMap * pFltr1)
{
    if (pRipCxt->pDistanceFilterRMap == pFltr1)
    {
        return FILTERING_TYPE_DISTANCE;
    }
    if (pRipCxt->pDistributeInFilterRMap == pFltr1)
    {
        return FILTERING_TYPE_DISTRIB_IN;
    }
    if (pRipCxt->pDistributeOutFilterRMap == pFltr1)
    {
        return FILTERING_TYPE_DISTRIB_OUT;
    }
    return 0;
}

/****************************************************************************
 Function    :  FilteringRMapCompareRip
 Input       :  compare filters by OID names
 Output      :  none
 Returns     :  positive fisrt bigger, negative second one bigger
****************************************************************************/
INT4
FilteringRMapCompareRip (tRipCxt * pRipCxt, tFilteringRMap * pFltr1,
                         tFilteringRMap * pFltr2)
{
#define MAX_VALUE 256
#define EQ_VALUE 0

    INT4                i4CmpResult = EQ_VALUE;

    if ((NULL == pFltr1) && (NULL == pFltr2))
    {
        return EQ_VALUE;
    }
    if (NULL == pFltr1)
    {
        return MAX_VALUE;
    }
    if (NULL == pFltr2)
    {
        return -MAX_VALUE;
    }

    i4CmpResult =
        (INT4) STRLEN (pFltr1->au1DistInOutFilterRMapName) -
        (INT4) STRLEN (pFltr2->au1DistInOutFilterRMapName);
    if (i4CmpResult != 0)
    {
        return i4CmpResult;
    }
    i4CmpResult =
        STRCMP (pFltr1->au1DistInOutFilterRMapName,
                pFltr2->au1DistInOutFilterRMapName);
    if (i4CmpResult != 0)
    {
        return i4CmpResult;
    }

    if (pRipCxt->pDistributeOutFilterRMap == pFltr1)
    {
        return FILTERING_TYPE_DISTRIB_OUT;
    }
    if (pRipCxt->pDistributeOutFilterRMap == pFltr2)
    {
        return -FILTERING_TYPE_DISTRIB_OUT;
    }

    if (pRipCxt->pDistributeInFilterRMap == pFltr1)
    {
        return FILTERING_TYPE_DISTRIB_IN;
    }
    if (pRipCxt->pDistributeInFilterRMap == pFltr2)
    {
        return -FILTERING_TYPE_DISTRIB_IN;
    }

    if (pRipCxt->pDistanceFilterRMap == pFltr1)
    {
        return -FILTERING_TYPE_DISTANCE;
    }

    return i4CmpResult;
}

/****************************************************************************
 Function    :  FilteringRMapSortByIndexRip
 Input       :  tFilteringRMap pointer to array of 3 tFilteringRMap elements
 Output      :  sorted array
 Returns     :  none
****************************************************************************/
void
FilteringRMapSortByIndexRip (tRipCxt * pRipCxt, tFilteringRMap * ppFiltr[])
{
    BOOL1               b1UpdatedArray = FALSE;
    tFilteringRMap     *pExFilteringRMap = NULL;
    INT4                i4Idx;

    ppFiltr[0] = pRipCxt->pDistanceFilterRMap;
    ppFiltr[1] = pRipCxt->pDistributeInFilterRMap;
    ppFiltr[2] = pRipCxt->pDistributeOutFilterRMap;

    do
    {
        b1UpdatedArray = FALSE;
        for (i4Idx = 0; i4Idx < MAX_FILTER_AMOUNT - 1; i4Idx++)
            if (FilteringRMapCompareRip
                (pRipCxt, ppFiltr[i4Idx], ppFiltr[i4Idx + 1]) > 0)
            {
                pExFilteringRMap = ppFiltr[i4Idx + 1];
                ppFiltr[i4Idx + 1] = ppFiltr[i4Idx];
                ppFiltr[i4Idx] = pExFilteringRMap;
                b1UpdatedArray = TRUE;
            }
    }
    while (TRUE == b1UpdatedArray);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRipDistInOutRouteMapTable
 Input       :  The Indices
                FsRipDistInOutRouteMapName
                FsRipDistInOutRouteMapType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsRipDistInOutRouteMapTable (tSNMP_OCTET_STRING_TYPE *
                                             pFsRipDistInOutRouteMapName,
                                             INT4
                                             *pi4FsRipDistInOutRouteMapType)
{
    tRipCxt            *pRipCxt = RIP_MGMT_CXT;
    if (pRipCxt != NULL)
    {
        tFilteringRMap     *FilteringRMapArray[3];
        FilteringRMapSortByIndexRip (pRipCxt, FilteringRMapArray);

        if (NULL != FilteringRMapArray[0])
        {
            *pi4FsRipDistInOutRouteMapType =
                GetFlterTypeByFlterPtrRip (pRipCxt, FilteringRMapArray[0]);

            pFsRipDistInOutRouteMapName->i4_Length =
                (INT4) STRLEN (FilteringRMapArray[0]->
                               au1DistInOutFilterRMapName);
            MEMCPY (pFsRipDistInOutRouteMapName->pu1_OctetList,
                    FilteringRMapArray[0]->au1DistInOutFilterRMapName,
                    pFsRipDistInOutRouteMapName->i4_Length);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRipDistInOutRouteMapTable
 Input       :  The Indices
                FsRipDistInOutRouteMapName
                nextFsRipDistInOutRouteMapName
                FsRipDistInOutRouteMapType
                nextFsRipDistInOutRouteMapType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRipDistInOutRouteMapTable (tSNMP_OCTET_STRING_TYPE *
                                            pFsRipDistInOutRouteMapName,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pNextFsRipDistInOutRouteMapName,
                                            INT4 i4FsRipDistInOutRouteMapType,
                                            INT4
                                            *pi4NextFsRipDistInOutRouteMapType)
{
    tRipCxt            *pRipCxt = RIP_MGMT_CXT;
    INT4                i4Idx;

    if (pFsRipDistInOutRouteMapName == NULL
        || pFsRipDistInOutRouteMapName->i4_Length <= 0)
    {
        return SNMP_FAILURE;
    }

    if (pRipCxt != NULL)
    {
        tFilteringRMap     *FilteringRMapArray[3];
        FilteringRMapSortByIndexRip (pRipCxt, FilteringRMapArray);

        for (i4Idx = 0; i4Idx < MAX_FILTER_AMOUNT - 1; i4Idx++)
        {
            if ((NULL != FilteringRMapArray[i4Idx])
                && (NULL != FilteringRMapArray[i4Idx + 1]))
            {
                if ((pFsRipDistInOutRouteMapName->i4_Length <=
                     RMAP_MAX_NAME_LEN)
                    && (0 ==
                        MEMCMP (pFsRipDistInOutRouteMapName->pu1_OctetList,
                                FilteringRMapArray[i4Idx]->
                                au1DistInOutFilterRMapName,
                                pFsRipDistInOutRouteMapName->i4_Length))
                    && (i4FsRipDistInOutRouteMapType ==
                        GetFlterTypeByFlterPtrRip (pRipCxt,
                                                   FilteringRMapArray[i4Idx])))

                {
                    pNextFsRipDistInOutRouteMapName->i4_Length =
                        (INT4) STRLEN (FilteringRMapArray[i4Idx + 1]->
                                       au1DistInOutFilterRMapName);
                    MEMCPY (pNextFsRipDistInOutRouteMapName->pu1_OctetList,
                            FilteringRMapArray[i4Idx +
                                               1]->au1DistInOutFilterRMapName,
                            pNextFsRipDistInOutRouteMapName->i4_Length);
                    *pi4NextFsRipDistInOutRouteMapType =
                        GetFlterTypeByFlterPtrRip (pRipCxt,
                                                   FilteringRMapArray[i4Idx +
                                                                      1]);
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRipDistInOutRouteMapValue
 Input       :  The Indices
                FsRipDistInOutRouteMapName
                FsRipDistInOutRouteMapType

                The Object 
                retValFsRipDistInOutRouteMapValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRipDistInOutRouteMapValue (tSNMP_OCTET_STRING_TYPE *
                                   pFsRipDistInOutRouteMapName,
                                   INT4 i4FsRipDistInOutRouteMapType,
                                   INT4 *pi4RetValFsRipDistInOutRouteMapValue)
{
#ifdef ROUTEMAP_WANTED
    tRipCxt            *pRipCxt = RIP_MGMT_CXT;
    if (pRipCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pFsRipDistInOutRouteMapName == NULL
        || pFsRipDistInOutRouteMapName->i4_Length <= 0)
    {
        return SNMP_FAILURE;
    }

    if (i4FsRipDistInOutRouteMapType == FILTERING_TYPE_DISTANCE
        && CmpFilterRMapName (pRipCxt->pDistanceFilterRMap,
                              pFsRipDistInOutRouteMapName) == 0)
    {
        *pi4RetValFsRipDistInOutRouteMapValue =
            pRipCxt->pDistanceFilterRMap->u1RMapDistance;
    }
    else
    {
        *pi4RetValFsRipDistInOutRouteMapValue = 0;
    }
#else
    UNUSED_PARAM (pFsRipDistInOutRouteMapName);
    UNUSED_PARAM (i4FsRipDistInOutRouteMapType);
    UNUSED_PARAM (pi4RetValFsRipDistInOutRouteMapValue);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRipDistInOutRouteMapRowStatus
 Input       :  The Indices
                FsRipDistInOutRouteMapName
                FsRipDistInOutRouteMapType

                The Object 
                retValFsRipDistInOutRouteMapRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRipDistInOutRouteMapRowStatus (tSNMP_OCTET_STRING_TYPE *
                                       pFsRipDistInOutRouteMapName,
                                       INT4 i4FsRipDistInOutRouteMapType,
                                       INT4
                                       *pi4RetValFsRipDistInOutRouteMapRowStatus)
{
#ifdef ROUTEMAP_WANTED
    tRipCxt            *pRipCxt = RIP_MGMT_CXT;
    if (pRipCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pFsRipDistInOutRouteMapName == NULL
        || pFsRipDistInOutRouteMapName->i4_Length <= 0)
    {
        return SNMP_FAILURE;
    }

    if (i4FsRipDistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
    {
        if (CmpFilterRMapName
            (pRipCxt->pDistanceFilterRMap, pFsRipDistInOutRouteMapName) == 0)
        {
            *pi4RetValFsRipDistInOutRouteMapRowStatus =
                pRipCxt->pDistanceFilterRMap->u1RowStatus;
            return SNMP_SUCCESS;
        }
    }
    else if (i4FsRipDistInOutRouteMapType == FILTERING_TYPE_DISTRIB_IN)
    {
        if (CmpFilterRMapName
            (pRipCxt->pDistributeInFilterRMap,
             pFsRipDistInOutRouteMapName) == 0)
        {
            *pi4RetValFsRipDistInOutRouteMapRowStatus =
                pRipCxt->pDistributeInFilterRMap->u1RowStatus;
            return SNMP_SUCCESS;
        }
    }
    else if (i4FsRipDistInOutRouteMapType == FILTERING_TYPE_DISTRIB_OUT)
    {
        if (CmpFilterRMapName
            (pRipCxt->pDistributeOutFilterRMap,
             pFsRipDistInOutRouteMapName) == 0)
        {
            *pi4RetValFsRipDistInOutRouteMapRowStatus =
                pRipCxt->pDistributeOutFilterRMap->u1RowStatus;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
#else
    UNUSED_PARAM (pFsRipDistInOutRouteMapName);
    UNUSED_PARAM (i4FsRipDistInOutRouteMapType);
    UNUSED_PARAM (pi4RetValFsRipDistInOutRouteMapRowStatus);
    return SNMP_SUCCESS;
#endif
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRipDistInOutRouteMapValue
 Input       :  The Indices
                FsRipDistInOutRouteMapName
                FsRipDistInOutRouteMapType

                The Object 
                setValFsRipDistInOutRouteMapValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRipDistInOutRouteMapValue (tSNMP_OCTET_STRING_TYPE *
                                   pFsRipDistInOutRouteMapName,
                                   INT4 i4FsRipDistInOutRouteMapType,
                                   INT4 i4SetValFsRipDistInOutRouteMapValue)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    UINT4               u4ErrCode = 0;
    tRipCxt            *pRipCxt = RIP_MGMT_CXT;

    if (pRipCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pFsRipDistInOutRouteMapName == NULL
        || pFsRipDistInOutRouteMapName->i4_Length <= 0)
    {
        return SNMP_FAILURE;
    }
    if (nmhTestv2FsRipDistInOutRouteMapValue (&u4ErrCode,
                                              pFsRipDistInOutRouteMapName,
                                              i4FsRipDistInOutRouteMapType,
                                              i4SetValFsRipDistInOutRouteMapValue)
        == SNMP_SUCCESS)
    {
        if (i4FsRipDistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
        {
            pRipCxt->pDistanceFilterRMap->u1RMapDistance =
                (UINT1) i4SetValFsRipDistInOutRouteMapValue;
        }
    }
    else
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsMIRipDistInOutRouteMapValue;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsMIRipDistInOutRouteMapValue) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 3;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %i %i", pRipCxt->i4CxtId,
                      pFsRipDistInOutRouteMapName, i4FsRipDistInOutRouteMapType,
                      i4SetValFsRipDistInOutRouteMapValue));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRipDistInOutRouteMapRowStatus
 Input       :  The Indices
                FsRipDistInOutRouteMapName
                FsRipDistInOutRouteMapType

                The Object 
                setValFsRipDistInOutRouteMapRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRipDistInOutRouteMapRowStatus (tSNMP_OCTET_STRING_TYPE *
                                       pFsRipDistInOutRouteMapName,
                                       INT4 i4FsRipDistInOutRouteMapType,
                                       INT4
                                       i4SetValFsRipDistInOutRouteMapRowStatus)
{
#ifdef ROUTEMAP_WANTED
    UINT4               u4ErrCode = 0;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    tRipCxt            *pRipCxt = RIP_MGMT_CXT;

    if (pRipCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pFsRipDistInOutRouteMapName->i4_Length >= RMAP_MAX_NAME_LEN)
    {
        return SNMP_FAILURE;
    }

    if (nmhTestv2FsRipDistInOutRouteMapRowStatus (&u4ErrCode,
                                                  pFsRipDistInOutRouteMapName,
                                                  i4FsRipDistInOutRouteMapType,
                                                  i4SetValFsRipDistInOutRouteMapRowStatus)
        == SNMP_SUCCESS)
    {

        switch (i4SetValFsRipDistInOutRouteMapRowStatus)
        {
            case CREATE_AND_WAIT:
            {
                tFilteringRMap     *pFilter = NULL;
                UINT4               u4Status = FILTERNIG_STAT_DEFAULT;

                if ((RIP_ALLOC_RMAP_FILTER_NODE (pRipCxt, pFilter)) != NULL)
                {
                    MEMSET (pFilter, 0, sizeof (tFilteringRMap));
                    pFilter->u1RowStatus =
                        (UINT1) i4SetValFsRipDistInOutRouteMapRowStatus;
                    MEMCPY (pFilter->au1DistInOutFilterRMapName,
                            pFsRipDistInOutRouteMapName->pu1_OctetList,
                            MEM_MAX_BYTES ((RMAP_MAX_NAME_LEN - 1),
                                           (UINT4) pFsRipDistInOutRouteMapName->
                                           i4_Length));

                    u4Status =
                        RMapGetInitialMapStatus (pFsRipDistInOutRouteMapName->
                                                 pu1_OctetList);
                    if (u4Status != 0)
                    {
                        pFilter->u1Status = FILTERNIG_STAT_ENABLE;
                    }
                    else
                    {
                        pFilter->u1Status = FILTERNIG_STAT_DISABLE;
                    }

                    if (i4FsRipDistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
                    {
                        pRipCxt->pDistanceFilterRMap = pFilter;
                    }
                    else if (i4FsRipDistInOutRouteMapType ==
                             FILTERING_TYPE_DISTRIB_IN)
                    {
                        pRipCxt->pDistributeInFilterRMap = pFilter;
                    }
                    else if (i4FsRipDistInOutRouteMapType ==
                             FILTERING_TYPE_DISTRIB_OUT)
                    {
                        pRipCxt->pDistributeOutFilterRMap = pFilter;
                    }
                }
                else
                {
                    return SNMP_FAILURE;
                }
            }
                break;
            case DESTROY:
                if (i4FsRipDistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
                {
                    RIP_FREE_RMAP_FILTER_NODE (pRipCxt,
                                               pRipCxt->pDistanceFilterRMap);
                    pRipCxt->pDistanceFilterRMap = NULL;
                }
                else if (i4FsRipDistInOutRouteMapType ==
                         FILTERING_TYPE_DISTRIB_IN)
                {
                    RIP_FREE_RMAP_FILTER_NODE (pRipCxt,
                                               pRipCxt->
                                               pDistributeInFilterRMap);
                    pRipCxt->pDistributeInFilterRMap = NULL;
                }
                else if (i4FsRipDistInOutRouteMapType ==
                         FILTERING_TYPE_DISTRIB_OUT)
                {
                    RIP_FREE_RMAP_FILTER_NODE (pRipCxt,
                                               pRipCxt->
                                               pDistributeOutFilterRMap);
                    pRipCxt->pDistributeOutFilterRMap = NULL;
                }
                break;
            default:
                if (i4FsRipDistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
                {
                    pRipCxt->pDistanceFilterRMap->u1RowStatus =
                        (UINT1) i4SetValFsRipDistInOutRouteMapRowStatus;
                }
                else if (i4FsRipDistInOutRouteMapType ==
                         FILTERING_TYPE_DISTRIB_IN)
                {
                    pRipCxt->pDistributeInFilterRMap->u1RowStatus =
                        (UINT1) i4SetValFsRipDistInOutRouteMapRowStatus;
                }
                else if (i4FsRipDistInOutRouteMapType ==
                         FILTERING_TYPE_DISTRIB_OUT)
                {
                    pRipCxt->pDistributeOutFilterRMap->u1RowStatus =
                        (UINT1) i4SetValFsRipDistInOutRouteMapRowStatus;
                }
        }
    }
    else
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsMIRipDistInOutRouteMapRowStatus;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsMIRipDistInOutRouteMapRowStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 3;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %i %i", pRipCxt->i4CxtId,
                      pFsRipDistInOutRouteMapName, i4FsRipDistInOutRouteMapType,
                      i4SetValFsRipDistInOutRouteMapRowStatus));

#else
    UNUSED_PARAM (pFsRipDistInOutRouteMapName);
    UNUSED_PARAM (i4FsRipDistInOutRouteMapType);
    UNUSED_PARAM (i4SetValFsRipDistInOutRouteMapRowStatus);
#endif
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRipDistInOutRouteMapValue
 Input       :  The Indices
                FsRipDistInOutRouteMapName
                FsRipDistInOutRouteMapType

                The Object 
                testValFsRipDistInOutRouteMapValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRipDistInOutRouteMapValue (UINT4 *pu4ErrorCode,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsRipDistInOutRouteMapName,
                                      INT4 i4FsRipDistInOutRouteMapType,
                                      INT4 i4TestValFsRipDistInOutRouteMapValue)
{
#ifdef ROUTEMAP_WANTED
    tRipCxt            *pRipCxt = RIP_MGMT_CXT;
    if (pRipCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pFsRipDistInOutRouteMapName == NULL
        || pFsRipDistInOutRouteMapName->i4_Length <= 0)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;

    }

    if ((UINT4) i4TestValFsRipDistInOutRouteMapValue & 0xFFFFFF00)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4FsRipDistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
    {
        if (CmpFilterRMapName
            (pRipCxt->pDistanceFilterRMap, pFsRipDistInOutRouteMapName) == 0)
        {
            return SNMP_SUCCESS;
        }
    }
    else if (i4FsRipDistInOutRouteMapType == FILTERING_TYPE_DISTRIB_IN)
    {
        if (CmpFilterRMapName
            (pRipCxt->pDistributeInFilterRMap,
             pFsRipDistInOutRouteMapName) == 0)
        {
            return SNMP_SUCCESS;
        }
    }
    else if (i4FsRipDistInOutRouteMapType == FILTERING_TYPE_DISTRIB_OUT)
    {
        if (CmpFilterRMapName
            (pRipCxt->pDistributeOutFilterRMap,
             pFsRipDistInOutRouteMapName) == 0)
        {
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pFsRipDistInOutRouteMapName);
    UNUSED_PARAM (i4FsRipDistInOutRouteMapType);
    UNUSED_PARAM (i4TestValFsRipDistInOutRouteMapValue);
#endif
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsRipDistInOutRouteMapRowStatus
 Input       :  The Indices
                FsRipDistInOutRouteMapName
                FsRipDistInOutRouteMapType

                The Object 
                testValFsRipDistInOutRouteMapRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRipDistInOutRouteMapRowStatus (UINT4 *pu4ErrorCode,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsRipDistInOutRouteMapName,
                                          INT4 i4FsRipDistInOutRouteMapType,
                                          INT4
                                          i4TestValFsRipDistInOutRouteMapRowStatus)
{
#ifdef ROUTEMAP_WANTED

    INT1                i1Exists = 0;
    INT1                i1Match = 0;
    tRipCxt            *pRipCxt = RIP_MGMT_CXT;
    if (pRipCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pFsRipDistInOutRouteMapName == NULL
        || pFsRipDistInOutRouteMapName->i4_Length <= 0
        || pFsRipDistInOutRouteMapName->i4_Length >= RMAP_MAX_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4FsRipDistInOutRouteMapType == FILTERING_TYPE_DISTANCE)

    {
        if (pRipCxt->pDistanceFilterRMap != NULL)
        {
            i1Exists = 1;
            if (CmpFilterRMapName
                (pRipCxt->pDistanceFilterRMap,
                 pFsRipDistInOutRouteMapName) == 0)
            {
                i1Match = 1;
            }
        }
    }
    else if (i4FsRipDistInOutRouteMapType == FILTERING_TYPE_DISTRIB_IN)
    {
        if (pRipCxt->pDistributeInFilterRMap != NULL)
        {
            i1Exists = 1;
            if (CmpFilterRMapName
                (pRipCxt->pDistributeInFilterRMap,
                 pFsRipDistInOutRouteMapName) == 0)
            {
                i1Match = 1;
            }
        }
    }
    else if (i4FsRipDistInOutRouteMapType == FILTERING_TYPE_DISTRIB_OUT)
    {
        if (pRipCxt->pDistributeOutFilterRMap != NULL)
        {
            i1Exists = 1;
            if (CmpFilterRMapName
                (pRipCxt->pDistributeOutFilterRMap,
                 pFsRipDistInOutRouteMapName) == 0)
            {
                i1Match = 1;
            }
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch (i4TestValFsRipDistInOutRouteMapRowStatus)
    {
        case ACTIVE:
        case DESTROY:
            if (i1Match)
            {
                return SNMP_SUCCESS;
            }
            else if (i1Exists)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            }
            break;
        case CREATE_AND_WAIT:
            if (!i1Exists)
            {
                return SNMP_SUCCESS;
            }
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            break;
    }
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pFsRipDistInOutRouteMapName);
    UNUSED_PARAM (i4FsRipDistInOutRouteMapType);
    UNUSED_PARAM (i4TestValFsRipDistInOutRouteMapRowStatus);
#endif

    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRipDistInOutRouteMapTable
 Input       :  The Indices
                FsRipDistInOutRouteMapName
                FsRipDistInOutRouteMapType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRipDistInOutRouteMapTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRipPreferenceValue
 Input       :  The Indices

                The Object 
                retValFsRipPreferenceValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRipPreferenceValue (INT4 *pi4RetValFsRipPreferenceValue)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    *pi4RetValFsRipPreferenceValue = pRipCxtEntry->u1Distance;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRipPreferenceValue
 Input       :  The Indices

                The Object 
                setValFsRipPreferenceValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRipPreferenceValue (INT4 i4SetValFsRipPreferenceValue)
{
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    if (i4SetValFsRipPreferenceValue == 0)
    {
        pRipCxtEntry->u1Distance = RIP_DEFAULT_PREFERENCE;
    }
    else
    {
        pRipCxtEntry->u1Distance = (UINT1) i4SetValFsRipPreferenceValue;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIRipPreferenceValue;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIRipPreferenceValue) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", pRipCxtEntry->i4CxtId,
                      i4SetValFsRipPreferenceValue));
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRipPreferenceValue
 Input       :  The Indices

                The Object 
                testValFsRipPreferenceValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRipPreferenceValue (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsRipPreferenceValue)
{
    if ((UINT4) i4TestValFsRipPreferenceValue & 0xFFFFFF00)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRipPreferenceValue
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRipPreferenceValue (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
