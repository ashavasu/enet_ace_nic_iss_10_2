/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* $Id: vcmvctre.c,v 1.7 2013/03/05 11:43:54 siva Exp $*/
/* Licensee Aricent Inc., 2004-2005                         */
/*****************************************************************************/
/*    FILE  NAME            : vcmvctre.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : VCM module                                     */
/*    MODULE NAME           : VCM module                                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 16 Jun 2006                                    */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains utility functions related to*/
/*                            virtual context Table of VCM module.           */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    16 Jun 2006 / MITeam   Initial Create.                         */
/*---------------------------------------------------------------------------*/

#ifndef _VCMVCTRE_C
#define _VCMVCTRE_C

#include "vcminc.h"
#include "vcmtree.h"
#include "cli.h"
#include "vcmcli.h"
#include "vcmglob.h"
#include "vcmmacr.h"

/*****************************************************************************/
/* Function Name      : VcmContextEntryCompare                               */
/*                                                                           */
/* Description        : Action routine that is passed as parameter to the    */
/*                      the tree create used for comparing the keys.         */
/*                                                                           */
/* Input(s)           : e1 and e2 are pointer to two nodes present in the    */
/*                      tree                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : 0  - if e1 and e2 keys are equal                     */
/*                      1  - if e1 key > e2 key                              */
/*                      -1 - if e1 key < e2 key                              */
/*****************************************************************************/
INT4
VcmContextEntryCompare (tVcmRBElem * e1, tVcmRBElem * e2)
{
    UINT4               u4VcNum1 = ((tVirtualContextInfo *) e1)->u4VcNum;
    UINT4               u4VcNum2 = ((tVirtualContextInfo *) e2)->u4VcNum;

    if (u4VcNum1 < u4VcNum2)
    {
        return -1;
    }
    else if (u4VcNum1 > u4VcNum2)
    {
        return 1;
    }
    return 0;
}

/*****************************************************************************/
/* Function Name      : VcmContextTableInit                                  */
/*                                                                           */
/* Description        : Create the RB TREE for storing the context entries.  */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS / FAILURE                                    */
/*****************************************************************************/
INT4
VcmContextTableInit ()
{
    /* Create the TREE for storing the Virtual Context entries. */
    VCM_CONTEXT_TABLE = VCM_RB_TREE_CREATE_EMBED
        (FSAP_OFFSETOF (tVirtualContextInfo, VcNumRbNode),
         VcmContextEntryCompare);
    if (NULL == VCM_CONTEXT_TABLE)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC, "vcmvctre: vc tree create failed.\n");
        return FAILURE;
    }
    return SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmContextTableDeInit                                */
/*                                                                           */
/* Description        : Delete the RB TREE created for vc.                   */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VcmContextTableDeInit ()
{
    /* Delete the TREE created for Virtual Context entries. */
    VCM_RB_TREE_DELETE (VCM_CONTEXT_TABLE);
    VCM_CONTEXT_TABLE = NULL;
}

/*****************************************************************************/
/* Function Name      : VcmAddVcEntry                                        */
/*                                                                           */
/* Description        : Adds an element context Entry to RB tree             */
/*                                                                           */
/* Input(s)           : pNode - pointer to the new element to be inserted    */
/*                      into the tree                                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS / FAILURE                                    */
/*****************************************************************************/
INT4
VcmAddVcEntry (VOID *pNode)
{

    if (RB_FAILURE == VCM_RB_TREE_ADD (VCM_CONTEXT_TABLE, (tVcmRBElem *) pNode))
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC, "vcmvctre: Add to Vc tree failed.\n");
        return FAILURE;
    }
    return SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmRemoveVcEntry                                     */
/*                                                                           */
/* Description        : Removes an element context Entry from RB tree        */
/*                                                                           */
/* Input(s)           : pNode - pointer to the element to be removed         */
/*                      from the tree                                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS / FAILURE                                    */
/*****************************************************************************/
INT4
VcmRemoveVcEntry (VOID *pNode)
{
    if (RB_FAILURE ==
        VCM_RB_TREE_REMOVE (VCM_CONTEXT_TABLE, (tVcmRBElem *) pNode))
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC,
                 "vcmvctre: Remove from Vc tree failed.\n");
        return FAILURE;
    }
    return SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmFindVcEntry                                       */
/*                                                                           */
/* Description        : Returns a pointer to a context Entry for a given key */
/*                                                                           */
/* Input(s)           : pNode - pointer to the element to be found           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : pointer to the node if key found. NULL if not found  */
/*****************************************************************************/
VOID               *
VcmFindVcEntry (VOID *pNode)
{
    tRBElem            *pEle = NULL;
    /* pNode is a dummy node in which VC number is filled. */
    if (VCM_IS_INITIALISED () != VCM_TRUE)
    {
        return NULL;
    }
    pEle = VCM_RB_TREE_GET (VCM_CONTEXT_TABLE, pNode);
    return pEle;
}

/*****************************************************************************/
/* Function Name      : VcmGetFirstVc                                        */
/*                                                                           */
/* Description        : Returns the first context Entry from the tree        */
/*                                                                           */
/* Input(s)           : pNode - pointer to the element to be found           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : pointer to the first element or NULL if tree is      */
/*                      empty                                                */
/*****************************************************************************/
tVirtualContextInfo *
VcmGetFirstVc ()
{
    tVirtualContextInfo *pVc = NULL;

    if (VCM_IS_INITIALISED () != VCM_TRUE)
    {
        return NULL;
    }
    pVc = (tVirtualContextInfo *) (VCM_RB_TREE_GETFIRST (VCM_CONTEXT_TABLE));
    return pVc;
}

/*****************************************************************************/
/* Function Name      : VcmGetNextVc                                         */
/*                                                                           */
/* Description        : Returns the next context Entry from the tree         */
/*                                                                           */
/* Input(s)           : pNode - pointer to the element next to be found      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : pointer to the next element or NULL if not found     */
/*****************************************************************************/
tVirtualContextInfo *
VcmGetNextVc (VOID *Temp)
{
    tVirtualContextInfo *pVc = NULL;

    if (VCM_IS_INITIALISED () != VCM_TRUE)
    {
        return NULL;
    }
    pVc = (tVirtualContextInfo *)
        (VCM_RB_TREE_GETNEXT (VCM_CONTEXT_TABLE, Temp, NULL));
    return pVc;
}
#endif
