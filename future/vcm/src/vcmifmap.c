/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* $Id: vcmifmap.c,v 1.11 2013/12/18 12:48:04 siva Exp $                */
/* Licensee Aricent Inc., 2004-2005                                          */
/*****************************************************************************/
/*    FILE  NAME            : vcmifmap.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : VCM module                                     */
/*    MODULE NAME           : VCM module                                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 16 Jun 2006                                    */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains functions related to        */
/*                            interface mapping by VCM module                */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    16 JUN 2006 / MITeam   Initial Create.                         */
/*---------------------------------------------------------------------------*/

#ifndef _VCMIFMAP_C
#define _VCMIFMAP_C

#include "vcminc.h"

/*****************************************************************************/
/* Function Name      : VcmCreateIfMapEntry                                  */
/*                                                                           */
/* Description        : This interface routine is called by Management module*/
/*                      when a new IfMap is created for an interface, will   */
/*                      check entry already present, if not it will create an*/
/*                      entry in IfMap Table.                                */
/*                                                                           */
/* Input(s)           : u4IfIndex   - Port Number                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : Returns the pointer to the IfMap Entry if successful.*/
/*                      If entry already exists, this function return NULL.  */
/*****************************************************************************/
INT4
VcmCreateIfMapEntry (UINT4 u4IfIndex)
{
    tVcmIfMapEntry      Dummy;

    /* Its better to check whether the entry already exists. 
     * Safe check*/

    VCM_LOCK ();

    Dummy.u4IfIndex = u4IfIndex;
    if (NULL == VcmFindIfMapEntry (&Dummy))
    {
        /*Given ifIndex does not exist in the table. Create it. */
        if (NULL == VcmRowCreate (u4IfIndex))
        {
            VCM_TRC (VCM_OS_RESOURCE_TRC,
                     "VcmCreateIfMapEntry: Not able to create row\n");
            VCM_UNLOCK ();
            return VCM_FAILURE;
        }

        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/* Function Name      : VcmDeleteIfMapEntry                                  */
/*                                                                           */
/* Description        : This routine is called by Management module          */
/*                      when an IfMap is removed for an interface.           */
/*                                                                           */
/* Input(s)           : u4IfIndex   - Port Number                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS / FAILURE                                    */
/*****************************************************************************/
INT4
VcmDeleteIfMapEntry (UINT4 u4IfIndex)
{
    tVcmIfMapEntry     *pIfMapEntry = NULL;
    tVcmIfMapEntry      Dummy;

    Dummy.u4IfIndex = u4IfIndex;

    VCM_LOCK ();

    if (NULL != (pIfMapEntry = VcmFindIfMapEntry (&Dummy)))
    {
        /* Remove from table, rel mem. Nothing to do in L2, since
         * the iface would not have been created in L2 earlier.*/
        if (FAILURE == VcmDeleteFromVcPortList (pIfMapEntry))
        {
            VCM_TRC (VCM_ALL_FAILURE_TRC | VCM_OS_RESOURCE_TRC,
                     "VcmDeleteFromVcPortList function return Failure.\n");
            VCM_UNLOCK ();
            return FAILURE;
        }

        if (FAILURE == VcmRemoveIfMapEntry (pIfMapEntry))
        {
            VCM_UNLOCK ();
            return FAILURE;
        }
        VCM_RELEASE_IFMAPENTRY_MEMBLK (pIfMapEntry);

        VCM_UNLOCK ();

        if (VCM_NODE_STATUS () == VCM_RED_ACTIVE)
        {
            if (VcmRedSendSyncMessages (u4IfIndex, 0,
                                        VCM_RED_CLEAR_SYNCUP_DATA) !=
                VCM_SUCCESS)

            {
                VCM_TRC (VCM_ALL_FAILURE_TRC | VCM_CONTROL_PATH_TRC,
                         "VCMRED: Vcm Send Sync Message to Standby Failed\n");
            }
        }

        return SUCCESS;
    }

    VCM_UNLOCK ();
    return FAILURE;
}

/*****************************************************************************/
/* Function Name      : VcmRowCreate                                         */
/*                                                                           */
/* Description        : Creates and add a new IfMap entry node to the IfMap  */
/*                      Table.                                               */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Number                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : Returns pointer to IfMap Entry if successful.        */
/*                      If fails returns NULL.                               */
/*****************************************************************************/
tVcmIfMapEntry     *
VcmRowCreate (UINT4 u4IfIndex)
{
    tVcmIfMapEntry     *pIfMapEntry = NULL;

    pIfMapEntry = VCM_ALLOCATE_IFMAPENTRY_MEMBLK ();
    if (pIfMapEntry == NULL)
    {
        /*Unable to allocate memory */
        VCM_TRC (VCM_OS_RESOURCE_TRC,
                 "VcmRowCreate: There is no memory for VcmIfMap Entry in VcmMapIfIndexToVc\n");
        return NULL;
    }

    VCM_MEMSET (pIfMapEntry, 0, sizeof (tVcmIfMapEntry));
    /* Initialize the IfEntry and add it to table. */

    pIfMapEntry->u4IfIndex = u4IfIndex;
    pIfMapEntry->u4PhyIfIndex = u4IfIndex;
    pIfMapEntry->u4VcNum = VCM_INVALID_VC;
    pIfMapEntry->u2HlPortId = VCM_INVALID_VAL;
    pIfMapEntry->u1RowStatus = NOT_READY;
    pIfMapEntry->u4L2ContextId = VCM_INVALID_VAL;

    if (FAILURE == VcmAddIfMapEntry (pIfMapEntry))
    {
        VCM_TRC (VCM_OS_RESOURCE_TRC,
                 "VcmRowCreate: Unable to add pIfMapEntry to table\n");
        VCM_RELEASE_IFMAPENTRY_MEMBLK (pIfMapEntry);
        return NULL;
    }
    return pIfMapEntry;
}

#endif
