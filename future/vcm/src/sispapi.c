/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/*$Id: sispapi.c,v 1.6 2014/12/09 12:46:07 siva Exp $                          */
/* Licensee Aricent Inc., 2009-2010                                          */
/*****************************************************************************/
/*    FILE  NAME            : sispapi.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc   .                                */
/*    SUBSYSTEM NAME        : VCM module                                     */
/*    MODULE NAME           : VCM module                                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 01 Mar 2009                                    */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains the APIs exported by  SISP  */
/*                            feature.                                       */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    31 APR 2009 / SISPTeam   Initial Create.                       */
/*---------------------------------------------------------------------------*/
#ifndef _SISPAPI_C
#define _SISPAPI_C

#include "vcminc.h"

/*****************************************************************************/
/* Function Name      : VcmSispGetContextInfoForPortVlan                     */
/*                                                                           */
/* Description        : This function used to get the Primary/Secondary ctx  */
/*                      in which the particular VLAN tagged packet has to be */
/*                      processed.                                           */
/*                                                                           */
/*                      This function will return the secondary context,     */
/*                      SISP port and its local port, if the SISP port is    */
/*                      member of the particular VLAN in the secondary ctxt. */
/*                                                                           */
/*                      This function will return the primary context,       */
/*                      input physical port and its local port, if the port  */
/*                      is member of the particular VLAN in the primary ctxt.*/
/*                                                                           */
/*                      This function should be called, only when SISP is    */
/*                      enabled on the particular physical or port channel   */
/*                      port.                                                */
/*                                                                           */
/* Input(s)           : u4PhyIfIndex - Physical Port Interface Index         */
/*                      VlanId - Vlan Identifier in the ingress packet       */
/*                                                                           */
/* Output(s)          : *pu4LogicalIfIndex - Physical/Logical Port Identifier*/
/*                      *pu4ContextId   - Context Identifier                 */
/*                      *pu2LocalPortId - Local Port identifier in the given */
/*                                        context.                           */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispGetContextInfoForPortVlan (UINT4 u4PhyIfIndex, tVlanId VlanId,
                                  UINT4 *pu4ContextId, UINT4 *pu4LogicalIfIndex,
                                  UINT2 *pu2LocalPortId)
{
    tVcmIfMapEntry      VcmIfMapEntry;

    VCM_MEMSET (&VcmIfMapEntry, VCM_INIT_VAL, sizeof (tVcmIfMapEntry));

    if (VcmSispPortVlanMapEntryGet (u4PhyIfIndex, VlanId, &VcmIfMapEntry)
        == VCM_FAILURE)
    {
        VCM_TRC_ARG2 (VCM_CONTROL_PATH_TRC | VCM_ALL_FAILURE_TRC, "GetCo"
                      "ntextInfoForPortVlan fail - Port Vlan Map entry not "
                      "found for Port -%u & Vlan -%d\n", u4PhyIfIndex, VlanId);
        return VCM_FAILURE;
    }

    *pu4ContextId = VcmIfMapEntry.u4VcNum;
    *pu4LogicalIfIndex = VcmIfMapEntry.u4IfIndex;
    *pu2LocalPortId = VcmIfMapEntry.u2HlPortId;

    VCM_TRC_ARG3 (VCM_CONTROL_PATH_TRC, "GetContextInfoForPortVlan - "
                  "Port-Vlan-Map entry get successful for Port -%u & Vlan -%d "
                  "in context %u\n", u4PhyIfIndex, VlanId, *pu4ContextId);

    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispUpdatePortVlanMapping                         */
/*                                                                           */
/* Description        : This API will update the Port-VLAN-Context Mapping   */
/*                      table with the input parameters. The Local ports     */
/*                      will be converted to the underlying physical or port */
/*                      channel interface before updating the entry in the   */
/*                      table.                                               */
/*                                                                           */
/* Input(s)           :  u4ContextId    - ContextId                          */
/*                       tVlanId        - VlanId,                            */
/*                       AddedPorts     - List of newly added ports          */
/*                       DeletedPorts   - List of deleted ports              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispUpdatePortVlanMapping (UINT4 u4ContextId, tVlanId VlanId,
                              tLocalPortList AddedPorts,
                              tLocalPortList DeletedPorts)
{
    UINT1              *pNullPortList = NULL;
    tVcmIfMapEntry     *pIfMapEntry = NULL;
    UINT4               u4IfIndex = VCM_INVALID_IFINDEX;
    UINT2               u2LocalPort = 1;
    UINT2               u2PortListSize = sizeof (tLocalPortList);
    UINT1               u1Result = OSIX_FALSE;

    VCM_LOCK ();

    if (VCM_GET_SISP_STATUS == SISP_SHUTDOWN)
    {
        VCM_UNLOCK ();
        VCM_TRC (VCM_CONTROL_PATH_TRC, "PVC Update:SISP feature is globally "
                 "shutdown.\n");
        return VCM_SUCCESS;
    }
    pNullPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pNullPortList == NULL)
    {
        VCM_TRC (VCM_CONTROL_PATH_TRC,
                 "VcmSispUpdatePortVlanMapping :Error in allocating memory"
                 "for pNullPortList\r\n");
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }
    MEMSET (pNullPortList, 0, sizeof (tLocalPortList));

    /* For addition of entries */
    if ((VCM_MEMCMP (AddedPorts, pNullPortList, u2PortListSize)) !=
        VCM_INIT_VAL)
    {
        for (; u2LocalPort <= MAX_PORTS_PER_CXT; u2LocalPort++)
        {
            OSIX_BITLIST_IS_BIT_SET (AddedPorts, u2LocalPort, u2PortListSize,
                                     u1Result);

            if (u1Result == OSIX_FALSE)
            {
                continue;
            }

            pIfMapEntry = VcmGetIfMapEntryFromLocalPort (u4ContextId,
                                                         u2LocalPort);
            if (pIfMapEntry == NULL)
            {
                /* Unknown logical port */
                continue;
            }

            if (SISP_IS_VALID_PHY_PORT (pIfMapEntry->u4PhyIfIndex) == VCM_FALSE)
            {
                continue;
            }
            if (SISP_PVC_TABLE_ENTRY (pIfMapEntry->u4PhyIfIndex) == NULL)
            {
                VCM_TRC_ARG1 (VCM_CONTROL_PATH_TRC | VCM_MGMT_TRC,
                              "VCM-SISP: VcmSispUpdatePortVlanMapping"
                              "SISP not enabled on IfIndex %d\n",
                              pIfMapEntry->u4PhyIfIndex);
                continue;
            }

            if (VcmSispAddPortVlanEntry (pIfMapEntry, VlanId) == VCM_FAILURE)
            {
                VCM_TRC_ARG2 (VCM_CONTROL_PATH_TRC | VCM_ALL_FAILURE_TRC,
                              "VcmSispUpdatePortVlanMapping - Addition failed "
                              "for VLAN %d and IfIndex %d\n", u4IfIndex,
                              VlanId);
                UtilPlstReleaseLocalPortList (pNullPortList);
                VCM_UNLOCK ();
                return VCM_FAILURE;
            }
        }
    }

    VCM_UNLOCK ();

    /* For deletion of entries */
    if ((VCM_MEMCMP (DeletedPorts, pNullPortList, u2PortListSize)) !=
        VCM_INIT_VAL)
    {
        for (u2LocalPort = 1; u2LocalPort <= MAX_PORTS_PER_CXT; u2LocalPort++)
        {
            OSIX_BITLIST_IS_BIT_SET (DeletedPorts, u2LocalPort, u2PortListSize,
                                     u1Result);

            if (u1Result == OSIX_FALSE)
            {
                continue;
            }

            VcmUtilGetIfIndexFromLocalPort (u4ContextId, u2LocalPort,
                                            &u4IfIndex);

            if (SISP_IS_VALID_PHY_PORT (u4IfIndex) == VCM_FALSE)
            {
                continue;
            }
            if (VcmSispDelPortVlanEntry (u4ContextId, u4IfIndex, VlanId)
                == VCM_FAILURE)
            {
                VCM_TRC_ARG2 (VCM_CONTROL_PATH_TRC | VCM_ALL_FAILURE_TRC,
                              "VcmSispUpdatePortVlanMapping - Deletion failed "
                              "for VLAN %d and IfIndex %d\n", u4IfIndex,
                              VlanId);
                UtilPlstReleaseLocalPortList (pNullPortList);
                return VCM_FAILURE;
            }
        }
    }
    UtilPlstReleaseLocalPortList (pNullPortList);
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispUpdatePortVlanMappingOnPort                   */
/*                                                                           */
/* Description        : This API will update the Port-VLAN-Context Mapping   */
/*                      table for the given port.                            */
/*                                                                           */
/* Input(s)           :  u4ContextId    - ContextId                          */
/*                       tVlanId        - VlanId,                            */
/*                       u4IfIndex      - Physical Port Identifier           */
/*                       u1Status       - Add or Delete flag                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispUpdatePortVlanMappingOnPort (UINT4 u4ContextId, tVlanId VlanId,
                                    UINT4 u4IfIndex, UINT1 u1Status)
{
    tVcmIfMapEntry      Dummy;    /* No need to memset; will be filled */
    tVcmIfMapEntry     *pIfMapEntry = NULL;

    VCM_LOCK ();

    if (VCM_GET_SISP_STATUS == SISP_SHUTDOWN)
    {
        VCM_UNLOCK ();
        VCM_TRC (VCM_CONTROL_PATH_TRC, "PVC Update on Port:SISP feature is "
                 "globally shutdown.\n");
        return VCM_SUCCESS;
    }

    if (SISP_IS_VALID_PHY_PORT (u4IfIndex) == VCM_FALSE)
    {
        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }
    VCM_UNLOCK ();

    if (u1Status == SISP_ADD)
    {
        /* For addition of entries */
        VCM_LOCK ();

        Dummy.u4IfIndex = u4IfIndex;
        pIfMapEntry = VcmFindIfMapEntry (&Dummy);

        if (pIfMapEntry == NULL)
        {
            /* Unknown logical port */
            VCM_UNLOCK ();
            return VCM_FAILURE;
        }

        if (SISP_PVC_TABLE_ENTRY (pIfMapEntry->u4PhyIfIndex) == NULL)
        {
            VCM_TRC_ARG1 (VCM_CONTROL_PATH_TRC | VCM_MGMT_TRC,
                          "VCM-SISP: VcmSispUpdatePortVlanMappingOnPort"
                          "SISP not enabled on IfIndex %d\n",
                          pIfMapEntry->u4PhyIfIndex);
            VCM_UNLOCK ();
            return VCM_FAILURE;
        }

        if (VcmSispAddPortVlanEntry (pIfMapEntry, VlanId) == VCM_FAILURE)
        {
            VCM_TRC_ARG2 (VCM_CONTROL_PATH_TRC | VCM_ALL_FAILURE_TRC,
                          "Sisp PVC Updation - Addition failed "
                          "for VLAN %d and IfIndex %d\n", VlanId, u4IfIndex);
            VCM_UNLOCK ();
            return VCM_FAILURE;
        }

        VCM_UNLOCK ();
    }
    else
    {
        /* For deletion of entries */
        if (VcmSispDelPortVlanEntry (u4ContextId, u4IfIndex,
                                     VlanId) == VCM_FAILURE)
        {
            VCM_TRC_ARG2 (VCM_CONTROL_PATH_TRC | VCM_ALL_FAILURE_TRC,
                          "Sisp PVC Updation - Deletion failed "
                          "for VLAN %d and IfIndex %d\n", VlanId, u4IfIndex);
            return VCM_FAILURE;
        }
    }
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispDeletePortVlanMapEntriesForPort               */
/*                                                                           */
/* Description        : This API will delete all the Port-VLAN-Context Map   */
/*                      table for the particular Physical Port or logical    */
/*                      port.                                                */
/*                                                                           */
/* Input(s)           :  u4IfIndex      - Physical Port Identifier           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispDeletePortVlanMapEntriesForPort (UINT4 u4IfIndex)
{
    return (VcmSispDeleteAllPortVlanMapEntriesForPort (u4IfIndex, VCM_FALSE));
}

/*****************************************************************************/
/* Function Name      : VcmSispIsPortVlanMappingValid                        */
/*                                                                           */
/* Description        : This API will used to verify whether the Port-VLAN-  */
/*                      Context Mapping Table configuration is Valid or not. */
/*                      The Local ports will be converted to the underlying  */
/*                      physical or port channel interface before verifying  */
/*                      the entry in the table.                              */
/*                                                                           */
/* Input(s)           : u4ContextId    - ContextId                           */
/*                      tVlanId        - VlanId,                             */
/*                      tLocalPortList - AddedPorts                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispIsPortVlanMappingValid (UINT4 u4ContextId, tVlanId VlanId,
                               tLocalPortList AddedPorts)
{
    UINT2               u2LocalPort = 1;
    UINT2               u2Size = sizeof (tLocalPortList);
    UINT1               u1Result = OSIX_FALSE;

    VCM_LOCK ();
    if (VCM_GET_SISP_STATUS == SISP_SHUTDOWN)
    {
        VCM_UNLOCK ();
        VCM_TRC (VCM_CONTROL_PATH_TRC, "PVC Update on Port:SISP feature is "
                 "globally shutdown.\n");
        return VCM_TRUE;
    }
    VCM_UNLOCK ();

    for (; u2LocalPort <= MAX_PORTS_PER_CXT; u2LocalPort++)
    {
        OSIX_BITLIST_IS_BIT_SET (AddedPorts, u2LocalPort, u2Size, u1Result);

        if (u1Result == OSIX_FALSE)
        {
            continue;
        }
        if (VcmSispValidatePortVlanEntry (u4ContextId, u2LocalPort,
                                          VlanId) == VCM_FALSE)
        {
            VCM_TRC_ARG3 (VCM_CONTROL_PATH_TRC | VCM_ALL_FAILURE_TRC,
                          "Port Vlan Entry add - Validation check failed "
                          "for Local port:%d & Vlan %d in Context %u\n",
                          u2LocalPort, VlanId, u4ContextId);
            return VCM_FALSE;
        }
    }
    return VCM_TRUE;
}

/*****************************************************************************/
/* Function Name      : VcmSispGetPhysicalPortOfSispPort                     */
/*                                                                           */
/* Description        : This function will provide the physical IfIndex      */
/*                      of the input Logical IfIndex.                        */
/*                                                                           */
/* Input(s)           : u4ILogicalfIndex - Logical Port identifier.          */
/*                                                                           */
/* Output(s)          : pu4PhysicalIndex - Physical Index of the logical port*/
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispGetPhysicalPortOfSispPort (UINT4 u4LogicalIfIndex,
                                  UINT4 *pu4PhysicalIndex)
{
    tVcmIfMapEntry      VcmIfMapEntry;

    VCM_MEMSET (&VcmIfMapEntry, VCM_INIT_VAL, sizeof (tVcmIfMapEntry));

    if (VcmGetIfMapEntry (u4LogicalIfIndex, &VcmIfMapEntry) == VCM_SUCCESS)
    {
        *pu4PhysicalIndex = VcmIfMapEntry.u4PhyIfIndex;

        VCM_TRC_ARG2 (VCM_CONTROL_PATH_TRC, "VcmSispGetPhysicalPortOfLogical"
                      "Port - Logical port %u's physical port is %u \n",
                      u4LogicalIfIndex, *pu4PhysicalIndex);

        return VCM_SUCCESS;
    }

    VCM_TRC_ARG1 (VCM_CONTROL_PATH_TRC | VCM_ALL_FAILURE_TRC, "VcmSispGet"
                  "PhysicalPortOfLogicalPort failed for Logical port %u \n",
                  u4LogicalIfIndex);

    return VCM_FAILURE;
}

/*****************************************************************************/
/* Function Name      : VcmSispGetSispPortsInfoOfPhysicalPort                */
/*                                                                           */
/* Description        : This function used to get the SISP port information  */
/*                      of a particual physical port. This API will return   */
/*                      the logical ports in IfIndex or LocalPort based on   */
/*                      the u1RetLocalPorts variable. paSispPort array will  */
/*                      be typecasted to UINT4 if the return value is to be  */
/*                      in IfIndex; will be typecasted to UINT2, if the      */
/*                      return value is to be in LocalPort.                  */
/*                                                                           */
/*                      This API can be used to get the number of SISP ports */
/*                      existing for the particular physical port. When      */
/*                      paSispPorts is NULL, SISP port count alone will be   */
/*                      returned.                                            */
/*                                                                           */
/*                      This API will fill up the PortList continously for   */
/*                      number of Sisp Ports in IfIndex based retrieval. For */
/*                      LocalPort based retrieval, bit position corresponding*/
/*                      to context will be set.                              */
/*                                                                           */
/* Input(s)           : u4PhyIfIndex   - Physical or port channel port Id.   */
/*                      u1RetLocalPorts- Flag to tell if the output is needed*/
/*                                       in LocalPorts.                      */
/*                                       VCM_TRUE  - Local Port format       */
/*                                       VCM_FALSE - IfIndex format          */
/*                                                                           */
/* Output(s)          : paSispPorts    - Array of Sisp Ports of the Phy port */
/*                      pu4PortCount   - Number of SISP Port count           */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispGetSispPortsInfoOfPhysicalPort (UINT4 u4PhyIfIndex,
                                       UINT1 u1RetLocalPorts,
                                       VOID *paSispPorts, UINT4 *pu4PortCount)
{
    tVcmIfMapEntry      VcmIfMapEntry;
    UINT4               u4ContextId = VCM_INIT_VAL;
    UINT4               u4PortCnt = VCM_INIT_VAL;

    VCM_MEMSET (&VcmIfMapEntry, VCM_INIT_VAL, sizeof (tVcmIfMapEntry));

    *pu4PortCount = VCM_INIT_VAL;

    if (VcmSispIsEnabled (u4PhyIfIndex) == VCM_FALSE)
    {
        VCM_TRC (VCM_CONTROL_PATH_TRC, "VcmSispGetSispPortsInfoOfPhysicalPort : VcmSispIsEnabled returns failure \n");
        return VCM_FAILURE;
    }

    /* For default context */
    if (VcmSispPortMapEntryGet (u4PhyIfIndex, u4ContextId,
                                &VcmIfMapEntry) == VCM_SUCCESS)
    {
        /* If paSispPorts is NULL, only Sisp ports count will be returned */
        if (paSispPorts != NULL)
        {
            if (u1RetLocalPorts == VCM_TRUE)
            {
                ((UINT2 *) paSispPorts)[VcmIfMapEntry.u4VcNum] =
                    VcmIfMapEntry.u2HlPortId;
            }
            else
            {
                ((UINT4 *) paSispPorts)[u4PortCnt] = VcmIfMapEntry.u4IfIndex;
            }
        }
        u4PortCnt++;
    }

    while (VcmSispPortMapEntryGetNext (u4PhyIfIndex, u4ContextId,
                                       &VcmIfMapEntry) != VCM_FAILURE)
    {
        if (u4PhyIfIndex != VcmIfMapEntry.u4PhyIfIndex)
        {
            /* Next entry is of some other physical port's - Break */
            break;
        }

        if (paSispPorts != NULL)
        {
            if (u1RetLocalPorts == VCM_TRUE)
            {
                ((UINT2 *) paSispPorts)[VcmIfMapEntry.u4VcNum] =
                    VcmIfMapEntry.u2HlPortId;
            }
            else
            {
                ((UINT4 *) paSispPorts)[u4PortCnt] = VcmIfMapEntry.u4IfIndex;
            }
        }

        u4PortCnt++;

        u4PhyIfIndex = VcmIfMapEntry.u4PhyIfIndex;
        u4ContextId = VcmIfMapEntry.u4VcNum;
    }

    *pu4PortCount = u4PortCnt;

    if (u4PortCnt == VCM_INIT_VAL)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC, "VcmSispGetSispPortsInfoOfPhysicalPort failed \n");
        return VCM_FAILURE;
    }
    else
    {
        
        VCM_TRC (VCM_ALL_FAILURE_TRC, "VcmSispGetSispPortsInfoOfPhysicalPort provided the SISP port information of the"
                                            " physical port and returns success \n");
        return VCM_SUCCESS;
    }
}

/*****************************************************************************/
/* Function Name      : VcmSispIsSispPortPresentInCtxt                       */
/*                                                                           */
/* Description        : This API will return TRUE when there is any SISP     */
/*                      enabled physical port or any logical port is present */
/*                      in the context.                                      */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier.                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_TRUE / VCM_FALSE                                 */
/*****************************************************************************/
INT1
VcmSispIsSispPortPresentInCtxt (UINT4 u4ContextId)
{
    tVirtualContextInfo *pVc = NULL;
    tVirtualContextInfo Dummy;

    VCM_MEMSET (&Dummy, VCM_INIT_VAL, sizeof (tVirtualContextInfo));

    VCM_LOCK ();

    if (VCM_GET_SISP_STATUS == SISP_SHUTDOWN)
    {
        VCM_UNLOCK ();
        return VCM_FALSE;
    }

    Dummy.u4VcNum = u4ContextId;

    pVc = VcmFindVcEntry (&Dummy);

    if (NULL != pVc)
    {
        if (pVc->u4SispPortCount > 0)
        {
            VCM_UNLOCK ();
            return VCM_TRUE;
        }
    }

    VCM_UNLOCK ();
    return VCM_FALSE;
}

/*****************************************************************************/
/* Function Name      : VcmSispGetSispPortOfPhysicalPortInCtx                */
/*                                                                           */
/* Description        : This function used to get the SISP interface index   */
/*                      and local port information from the  PhyIfIndex and  */
/*                      context ID.                                          */
/*                                                                           */
/* Input(s)           : u4PhyIfIndex   - Physical or port channel port Id.   */
/*                      u4ContextId    - Secondary context ID.               */
/*                                                                           */
/* Output(s)          : pu4SispPort    - Sisp IfIndex                        */
/*                      pu2LocalPort   - Local port of SispPort              */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispGetSispPortOfPhysicalPortInCtx (UINT4 u4PhyIfIndex, UINT4 u4ContextId,
                                       UINT4 *pu4SispPort, UINT2 *pu2LocalPort)
{
    tVcmIfMapEntry      VcmIfMapEntry;

    if (VcmSispIsEnabled (u4PhyIfIndex) == VCM_FALSE)
    {
        return VCM_FAILURE;
    }

    VCM_MEMSET (&VcmIfMapEntry, VCM_INIT_VAL, sizeof (tVcmIfMapEntry));

    if (VcmSispPortMapEntryGet (u4PhyIfIndex, u4ContextId,
                                &VcmIfMapEntry) == VCM_SUCCESS)
    {
        *pu4SispPort = VcmIfMapEntry.u4IfIndex;
        *pu2LocalPort = VcmIfMapEntry.u2HlPortId;
        return VCM_SUCCESS;
    }
    return VCM_FAILURE;
}

/*****************************************************************************/
/* Function Name      : VcmSispUpdatePortVlanMappingInHw                     */
/*                                                                           */
/* Description        : This API will update the Port-VLAN-Context Mapping   */
/*                      table with the input parameters. The Local ports     */
/*                      will be converted to the underlying physical or port */
/*                      channel interface before updating the entry in the   */
/*                      table.                                               */
/*                                                                           */
/* Input(s)           :  u4ContextId    - ContextId                          */
/*                       tVlanId        - VlanId,                            */
/*                       PortList       - List of local ports                */
/*                       u1Action       - ADD or DEL in Hardware             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispUpdatePortVlanMappingInHw (UINT4 u4ContextId, tVlanId VlanId,
                                  tLocalPortList PortList, UINT1 u1Action)
{
#ifdef NPAPI_WANTED
    tVcmIfMapEntry     *pIfMapEntry = NULL;
    tLocalPortList      NullPortList;
    UINT2               u2LocalPort = 1;
    UINT2               u2PortListSize = sizeof (tLocalPortList);
    UINT1               u1Result = OSIX_FALSE;
    UINT1               u1Status = L2IWF_DISABLED;

    VCM_LOCK ();

    if (VCM_NODE_STATUS () != VCM_RED_ACTIVE)
    {
        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    if (VCM_GET_SISP_STATUS == SISP_SHUTDOWN)
    {
        VCM_UNLOCK ();
        VCM_TRC (VCM_CONTROL_PATH_TRC, "PVC Update:SISP feature is globally "
                 "shutdown.\n");
        return VCM_SUCCESS;
    }

    MEMSET (NullPortList, VCM_INIT_VAL, u2PortListSize);

    /* For addition of entries */
    if ((VCM_MEMCMP (PortList, NullPortList, u2PortListSize)) != VCM_INIT_VAL)
    {
        for (; u2LocalPort <= MAX_PORTS_PER_CXT; u2LocalPort++)
        {
            OSIX_BITLIST_IS_BIT_SET (PortList, u2LocalPort, u2PortListSize,
                                     u1Result);

            if (u1Result == OSIX_FALSE)
            {
                continue;
            }

            if (NULL == (pIfMapEntry = VcmGetIfMapEntryFromLocalPort
                         (u4ContextId, u2LocalPort)))

            {
                continue;
            }

            if (SISP_IS_LOGICAL_PORT (pIfMapEntry->u4IfIndex) != VCM_TRUE)
            {
                /* Verify whether SISP is enabled for the physical port */
                L2IwfGetSispPortCtrlStatus (pIfMapEntry->u4IfIndex, &u1Status);

                if (u1Status != L2IWF_ENABLED)
                {
                    continue;
                }
            }

            VcmFsVcmSispHwSetPortVlanMapping (pIfMapEntry->u4PhyIfIndex, VlanId,
                                              u4ContextId, u1Action);
        }
    }
    VCM_UNLOCK ();
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (PortList);
    UNUSED_PARAM (u1Action);
#endif
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispPortAddedToPortChannel                        */
/*                                                                           */
/* Description        : This API will destroy all the secondary context      */
/*                      mappings for the specified physical interface. This  */
/*                      should be invoked when a SISP enabled physical port  */
/*                      is moved to port channel. This API simply disables   */
/*                      the SISP functionality over the provided interface.  */
/*                                                                           */
/* Input(s)           : u4IfIndex      - Interface Index.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispPortAddedToPortChannel (UINT4 u4IfIndex)
{
    VCM_LOCK ();

    if (VCM_GET_SISP_STATUS == SISP_SHUTDOWN)
    {
        VCM_UNLOCK ();
        VCM_TRC (VCM_CONTROL_PATH_TRC, "Port Delete:SISP feature is globally "
                 "shutdown.\n");

        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();

    if (VcmSispDisableOnPort (u4IfIndex) != VCM_SUCCESS)
    {
        VCM_TRC (VCM_CONTROL_PATH_TRC, "Port Delete:VcmSispDisableOnPort Fn"
                 "returned failure.\n");
        return VCM_FAILURE;
    }

    return VCM_SUCCESS;
}
#endif
