/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2004-2005                         */
/* $Id: vcmmain.c,v 1.22 2014/11/20 10:54:52 siva Exp $                */
/*****************************************************************************/
/*    FILE  NAME            : vcmmain.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : VCM module                                     */
/*    MODULE NAME           : VCM module                                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 16 Jun 2006                                    */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains functions pertaining to the */
/*                            Message Processing Module, Task Initilisation  */
/*                            and VCM Initialization Routines                */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    16 JUN 2006 / MITeam   Initial Create.                         */
/*---------------------------------------------------------------------------*/

#ifndef _VCMMAIN_C
#define _VCMMAIN_C

#include "vcminc.h"
#include "bridge.h"

/*****************************************************************************/
/* Function Name      : VcmTaskInit                                          */
/*                                                                           */
/* Description        : This function allocates memory pools for the         */
/*                      TaskQueue and creates the protocol semaphore.        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : All variables in the Global structure are            */
/*                      initialised.                                         */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success                              */
/*                      LA_FAILURE - On failure                              */
/*                      LA_ERR_MEM_FAILURE - On memory allocation failure    */
/*****************************************************************************/
INT4
VcmTaskInit (VOID)
{
    if (VCM_CREATE_SEM (VCM_SEM_NAME, VCM_SEM_COUNT,
                        VCM_SEM_FLAGS, &(VCM_SEM_ID)) != OSIX_SUCCESS)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC | VCM_INIT_SHUT_TRC,
                 "VCM Creation of Semaphore Failed ... \n");
        return VCM_FAILURE;
    }

    if (VCM_CREATE_SEM (VCM_CONF_SEM_NAME, VCM_SEM_COUNT,
                        VCM_SEM_FLAGS, &(VCM_CONF_SEM_ID)) != OSIX_SUCCESS)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC | VCM_INIT_SHUT_TRC,
                 "VCM Creation of Semaphore Failed ... \n");
        VcmTaskDeInit ();
        return VCM_FAILURE;
    }

    if (VCM_CREATE_QUEUE (VCM_CTRL_QUEUE, OSIX_MAX_Q_MSG_LEN,
                          VCM_CTRLQ_DEPTH, &(VCM_CTRLQ_ID)) != OSIX_SUCCESS)
    {
        VCM_TRC (VCM_INIT_SHUT_TRC | VCM_CONTROL_PATH_TRC,
                 "VcmTaskInit: CtrlQueue Creation Failed... \n");
        VcmTaskDeInit ();
        return VCM_FAILURE;
    }

    if (VCM_CREATE_QUEUE (VCM_LNX_VRF_QUEUE, OSIX_MAX_Q_MSG_LEN,
                          VCM_CTRLQ_DEPTH, &(VCM_LNX_VRFQ_ID)) != OSIX_SUCCESS)
    {
        VCM_TRC (VCM_INIT_SHUT_TRC | VCM_CONTROL_PATH_TRC,
                 "VcmTaskInit: CtrlQueue Creation Failed... \n");
        VcmTaskDeInit ();
        return VCM_FAILURE;
    }

    VCM_TRC (VCM_INIT_SHUT_TRC | VCM_CONTROL_PATH_TRC,
             "VcmTaskInit: VCM Task Initialization Succeed... \n");
    return VCM_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : VcmTaskDeInit                                        */
/*                                                                           */
/* Description        : This function de-initialises the VCM Module. It      */
/*                      deletes the CtrlQ Message Memory Pool, the VCM Queue */
/*                      and the VCM Task.                                    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmTaskDeInit (VOID)
{
    tVcmQMsg           *pMsg = NULL;

    if (VCM_CTRLQ_ID != (tOsixQId) VCM_INIT_VAL)
    {
        while (VCM_RECV_FROM_QUEUE (VCM_CTRLQ_ID, (UINT1 *) &pMsg,
                                    OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
        {
            if (pMsg == NULL)
            {
                continue;
            }

#ifdef L2RED_WANTED
            if (VCM_QMSG_TYPE (pMsg) == VCM_RM_QMSG)
            {
                if ((pMsg->RmMsg.u1Event == RM_MESSAGE) &&
                    (pMsg->RmMsg.pFrame != NULL))
                {
                    RM_FREE (pMsg->RmMsg.pFrame);
                }
                else if (((pMsg->RmMsg.u1Event == RM_STANDBY_UP) ||
                          (pMsg->RmMsg.u1Event == RM_STANDBY_DOWN)) &&
                         (pMsg->RmMsg.pFrame != NULL))
                {
                    VcmRmReleaseMemoryForMsg ((UINT1 *) pMsg->RmMsg.pFrame);
                }

                if (VCM_RELEASE_CTRLQ_MEM_BLOCK (pMsg) != VCM_MEM_SUCCESS)
                {
                    VCM_TRC (VCM_INIT_SHUT_TRC | VCM_CONTROL_PATH_TRC,
                             "VcmTaskDeInit: CtrlQ Memory Block Release FAILED\n");
                }
            }
#endif
        }

        VCM_DELETE_QUEUE (VCM_CTRLQ_ID);
        VCM_CTRLQ_ID = (tOsixQId) VCM_INIT_VAL;
    }

    if (VCM_SEM_ID != (tOsixSemId) VCM_INIT_VAL)
    {
        VCM_DELETE_SEM (VCM_SEM_ID);
        VCM_SEM_ID = (tOsixSemId) VCM_INIT_VAL;
    }

    if (VCM_CONF_SEM_ID != (tOsixSemId) VCM_INIT_VAL)
    {
        VCM_DELETE_SEM (VCM_CONF_SEM_ID);
        VCM_CONF_SEM_ID = (tOsixSemId) VCM_INIT_VAL;
    }

    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmTaskMain                                          */
/*                                                                           */
/* Description        : This is the main entry point function for the VCM    */
/*                      Task. This waits continuously in a loop to receive   */
/*                      events that are posted to this task and it then calls*/
/*                      the corresponding functions to process the events.   */
/*                                                                           */
/* Input(s)           : pi1Param - Pointer to the parameter value that can be*/
/*                                 passed to this task entry point function. */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
VcmTaskMain (INT1 *pi1Param)
{
    UINT4               u4Events = VCM_INIT_VAL;
    tVcmQMsg           *pQMsg = NULL;
#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    tVcmLnxvrfStatusQMsg *pLnxVrfQMsg;

#endif

    VCM_UNUSED (pi1Param);

    /* Initialise the global structure variable */
    VCM_MEMSET (&gVcmGlobals, 0, sizeof (tVcmGlobals));
    TMO_SLL_Init (&gVcmGlobals.VcmProtoRegTable);

    if (VcmTaskInit () == VCM_FAILURE)
    {
        VCM_TRC (VCM_INIT_SHUT_TRC | VCM_CONTROL_PATH_TRC,
                 "VcmTaskMain: VCM Task Initialization Failed... \n");
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    if (VcmHandleModuleStart () != VCM_SUCCESS)
    {
        VcmHandleModuleShutdown ();
        VcmTaskDeInit ();
        lrInitComplete (OSIX_FAILURE);
        return;
    }
    if (VcmCreateVirtualContext (VCM_DEFAULT_CONTEXT) != VCM_SUCCESS)
    {
        VCM_TRC (VCM_INIT_SHUT_TRC | VCM_CONTROL_PATH_TRC,
                 "VcmTaskMain: Default Context Creation FAILED... \n");
        VcmHandleModuleShutdown ();
        VcmTaskDeInit ();
        lrInitComplete (OSIX_FAILURE);
        return;
    }
    if (VCM_GET_TASK_ID (&VCM_TASK_ID) != OSIX_SUCCESS)
    {
        VcmHandleModuleShutdown ();
        VcmTaskDeInit ();
        lrInitComplete (OSIX_FAILURE);
        return;
    }

#ifdef SNMP_2_WANTED
    /* Register the protocol MIBs with SNMP */
    RegisterFSVCM ();
#endif

    SispInit ();

    /* Update the global sizing stucture  and calculate the dynamic memory
     * allocated by this task
     */

    /* Indicate the status of initialization to the main routine */
    lrInitComplete (OSIX_SUCCESS);

    VCM_TRC (VCM_INIT_SHUT_TRC | VCM_CONTROL_PATH_TRC,
             "VcmTaskMain: VCM Task Running... \n");

    while (1)
    {
        VCM_TRC (VCM_INIT_SHUT_TRC | VCM_CONTROL_PATH_TRC,
                 "VcmTaskMain: Waiting for an Event to be received... \n");

        if (VCM_RECEIVE_EVENT (VCM_TASK_ID,
                               VCM_MSG_EVENT | VCM_RED_BULK_UPD_EVENT | VCM_LNX_VRF_STATUS_EVENT,
                               OSIX_WAIT, &u4Events) == OSIX_SUCCESS)
        {
            if (u4Events & VCM_MSG_EVENT)
            {
                while (VCM_RECV_FROM_QUEUE (VCM_CTRLQ_ID, (UINT1 *) &pQMsg,
                                            OSIX_DEF_MSG_LEN, 0)
                       == OSIX_SUCCESS)
                {
                    switch (VCM_QMSG_TYPE (pQMsg))
                    {
#ifdef L2RED_WANTED
                        case VCM_RM_QMSG:

                            VcmProcessRmEvent (pQMsg);

                            if (VCM_RELEASE_CTRLQ_MEM_BLOCK (pQMsg) !=
                                VCM_MEM_SUCCESS)
                            {
                                VCM_TRC (VCM_INIT_SHUT_TRC |
                                         VCM_CONTROL_PATH_TRC,
                                         "VcmTaskMain: Release of CTRLQ Msg Memory Block FAILED... \n");
                            }
                            break;
#endif
                        default:
                            break;
                    }
                }
	    }
#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
	    if (u4Events & VCM_LNX_VRF_STATUS_EVENT)
	    {
                while (VCM_RECV_FROM_QUEUE (VCM_LNX_VRFQ_ID, (UINT1 *) &pLnxVrfQMsg,
                                            OSIX_DEF_MSG_LEN, 0)
                       == OSIX_SUCCESS)
                {
                    switch (VCM_LNXVRFQMSG_TYPE(pLnxVrfQMsg))
                    {
                        case VCM_LNX_VRF_CREATE_CONTEXT:
                            if(VCM_LNXVRFQMSG_STATUS(pLnxVrfQMsg) == OSIX_SUCCESS) /*SUCCESS*/
                            {
                                VcmCreateContextInNp(pLnxVrfQMsg->u4VrfId);
                            }
                            else
                            {
                                VcmDeleteMemRelease( pLnxVrfQMsg->u4VrfId);

                                UtlTrcPrint ("Creating context failed\n");
                            }
                            break;
                        case VCM_LNX_VRF_DELETE_CONTEXT:
                            if(VCM_LNXVRFQMSG_STATUS(pLnxVrfQMsg) == OSIX_SUCCESS) /*SUCCESS*/
                            {
                                VcmDeleteContextInNp(pLnxVrfQMsg->u4VrfId);
                                VcmDeleteMemRelease( pLnxVrfQMsg->u4VrfId);
                            }
                            else
                            {
                                UtlTrcPrint("Deleting VRF failed\n");
                            }

                            break;
                        case VCM_LNX_VRF_IF_MAP_CONTEXT:
                        case VCM_LNX_VRF_IF_UNMAP_CONTEXT: 
                            if(VCM_LNXVRFQMSG_STATUS(pLnxVrfQMsg) == OSIX_SUCCESS)
                            {
                                VcmIfMapContextInNp(pLnxVrfQMsg->u4IfIndex); 
                            }
                            break;
                        default:
                            break;
                    }
                    VCM_RELEASE_LNX_VRF_MEM_BLOCK (pLnxVrfQMsg);
                }
	    }
#endif


#ifdef L2RED_WANTED
            if (u4Events & VCM_RED_BULK_UPD_EVENT)
            {
                VcmRedHandleBulkUpdateEvent ();
            }
#endif
        }
    }
}

/*****************************************************************************/
/* Function Name      : VcmHandleModuleStart                                 */
/*                                                                           */
/* Description        : This function starts the VCM module                  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS/VCM_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
VcmHandleModuleStart (VOID)
{
    /* Create the TREE for storing the virtual context entries. */
    if (FAILURE == VcmContextTableInit ())
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC | VCM_OS_RESOURCE_TRC,
                 "VcmHandleModuleStart: Tree creation failed VC Table\n");

        return VCM_FAILURE;
    }

    if (VcmSizingMemCreateMemPools () != VCM_SUCCESS)
    {
        VCM_TRC (VCM_INIT_SHUT_TRC | VCM_CONTROL_PATH_TRC,
                 "VcmHandleModuleStart: VCM Creating Memory Pool Failed... \n");
        return VCM_FAILURE;
    }

    VcmAssignMempoolIds ();

    /* Create the TREE for storing the IfMap entries. */
    if (FAILURE == VcmIfMapTableInit ())
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC | VCM_OS_RESOURCE_TRC,
                 "VcmHandleModuleStart: Tree creation failed IfMap Table\n");
        VcmSizingMemDeleteMemPools ();
        VcmContextTableDeInit ();
        return VCM_FAILURE;
    }
    /* Initialise Ip interface mapping entries */
    VcmIpInit ();

    gVcmGlobals.u2NextFreeCxtId = 0;

    VCM_IS_INITIALISED () = VCM_TRUE;

    VcmRedRmInit ();

    if (VcmRedRegisterWithRM () != VCM_SUCCESS)
    {
        VCM_TRC (VCM_INIT_SHUT_TRC | VCM_CONTROL_PATH_TRC,
                 "VcmHandleModuleStart: VCM Redundancy Registration with RM Failed..\n");
        return VCM_FAILURE;
    }
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmHandleModuleShutdown                              */
/*                                                                           */
/* Description        : This function deletes all the contexts and shutdown  */
/*                      the VCM module                                       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS/VCM_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
VcmHandleModuleShutdown (VOID)
{
    tVcmQMsg           *pMsg = NULL;
    UINT4               u4VcId = 0;

    /* Delete all the contexts */

#ifdef L2RED_WANTED
    VCM_NODE_STATUS () = VCM_RED_IDLE;
#endif

    while (VcmGetFirstVcId (&u4VcId) == VCM_SUCCESS)
    {
        if (VcmDeleteVirtualContextIncDefault (u4VcId) == VCM_FAILURE)
        {
            VCM_TRC (VCM_INIT_SHUT_TRC | VCM_CONTROL_PATH_TRC,
                     "VcmHandleModuleShutdown: Context deletion is failed\n");
            return VCM_FAILURE;
        }
    }

    VCM_IS_INITIALISED () = VCM_FALSE;
    VCM_LOCK ();

    VcmContextTableDeInit ();

    VcmIfMapTableDeInit ();

    VcmSizingMemDeleteMemPools ();

    if (VCM_CTRLQ_ID != (tOsixQId) VCM_INIT_VAL)
    {
        while (VCM_RECV_FROM_QUEUE (VCM_CTRLQ_ID, (UINT1 *) &pMsg,
                                    OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
        {
            if (pMsg == NULL)
            {
                continue;
            }

#ifdef L2RED_WANTED
            if (VCM_QMSG_TYPE (pMsg) == VCM_RM_QMSG)
            {
                if ((pMsg->RmMsg.u1Event == RM_MESSAGE) &&
                    (pMsg->RmMsg.pFrame != NULL))
                {
                    RM_FREE (pMsg->RmMsg.pFrame);
                }
                else if (((pMsg->RmMsg.u1Event == RM_STANDBY_UP) ||
                          (pMsg->RmMsg.u1Event == RM_STANDBY_DOWN)) &&
                         (pMsg->RmMsg.pFrame != NULL))
                {
                    VcmRmReleaseMemoryForMsg ((UINT1 *) pMsg->RmMsg.pFrame);
                }

                if (VCM_RELEASE_CTRLQ_MEM_BLOCK (pMsg) != VCM_MEM_SUCCESS)
                {
                    VCM_TRC (VCM_INIT_SHUT_TRC | VCM_CONTROL_PATH_TRC,
                             "VcmTaskDeInit: CtrlQ Memory Block Release FAILED\n");
                }
            }
#endif
        }
    }
    gVcmGlobals.u2NextFreeCxtId = 0;

#ifdef L2RED_WANTED
    /*Initialize the globals used for redundancy */
    VcmRedRmInit ();
    if (VcmRmDeRegisterProtocols () == RM_FAILURE)
    {
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }
#endif
    VCM_UNLOCK ();
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmAssignMempoolIds                                 */
/*                                                                           */
/* Description        : This function is called when VCM Module is STARTED. */
/*                      Assign mempoolIds for mempools created in VCM module*/
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
VcmAssignMempoolIds (VOID)
{
    /*tVirtualContextInfo */
    VCM_VCINFO_MEMPOOL_ID = VCMMemPoolIds[MAX_VCM_VCINFO_SIZE_SIZING_ID];

    /*tVcmIfMapEntry */
    VCM_IFMAPENTRY_MEMPOOL_ID =
        VCMMemPoolIds[MAX_VCM_IFMAPENTRY_SIZE_SIZING_ID];

    /*tVcPortEntry */
    VCM_VCPORTENTRY_MEMPOOL_ID =
        VCMMemPoolIds[MAX_VCM_VCPORTENTRY_SIZE_SIZING_ID];

    /*tVcmIpIfMapEntry */
    VCM_IPIFENTRY_MEMPOOL_ID = VCMMemPoolIds[MAX_VCM_IPIFENTRY_SIZE_SIZING_ID];

    /*tVcmHLProtoRegEntry */
    VCM_PROTO_REG_MEMPOOL_ID = VCMMemPoolIds[MAX_VCM_PROTO_REG_SIZE_SIZING_ID];

    /*tVcmQMsg */
    VCM_CTRLQ_MEMPOOL_ID =
        VCMMemPoolIds[MAX_VCM_CTRLQ_MSG_MEMBLK_SIZE_SIZING_ID];
   
#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    /*tVcmLnxvrfStatusQMsg*/ 
    VCM_LNX_VRF_STATUS_MEMPOOL_ID =
        VCMMemPoolIds[MAX_VCM_LNX_VRF_STATUS_SIZE_SIZING_ID];
#endif

}

#endif /* _VCMMAIN_C */
