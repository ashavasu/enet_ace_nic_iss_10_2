/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: sispred.c,v 1.6 2014/02/27 12:33:59 siva Exp $
 *
 * Description: This file contains the functions to support
 *              Redundancy for VCM's SISP module.
 *
 *******************************************************************/
#include "vcminc.h"

/*****************************************************************************/
/* Function Name      : VcmSispStartHwAudit                                  */
/*                                                                           */
/* Description        : This function audits the SISP information            */
/*                      present in sw and the information present in hw and  */
/*                      syncs hw with that of sw.                            */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VcmSispStartHwAudit (VOID)
{
#ifdef NPAPI_WANTED
    tVcmIfMapEntry     *pIfMap = NULL;
    tVcmIfMapEntry      VcmIfMapEntry;
    tVlanId             VlanId;
    UINT4               u4PhyIfIndex = 1;
    UINT4               u4ContextId;

    if (VCM_GET_SISP_STATUS == SISP_SHUTDOWN)
    {
        VCM_TRC (VCM_INIT_SHUT_TRC, "SISP feature is globally shutdown."
                 "- No need of any Hardware Audit\n");
        return;
    }

    if ((VCM_SISP_RED_SYNC_AUDIT_ACTION () == VCM_INIT_VAL) ||
        (VCM_SISP_RED_SYNC_PHYIFINDEX () == VCM_INIT_VAL))
    {
        /* No action was being done, when ACTIVE crashed * 
         * Hardware Audit not required in this case      */
        VCM_TRC (VCM_INIT_SHUT_TRC, "No action was being done, when ACTIVE "
                 "failover or switchover - No need of any Hardware Audit\n");
        return;
    }

    VCM_LOCK ();

    VcmIfMapEntry.u4IfIndex = VCM_SISP_RED_SYNC_PHYIFINDEX ();

    if (NULL == (pIfMap = VcmFindIfMapEntry (&VcmIfMapEntry)))
    {
        VCM_UNLOCK ();
        return;
    }

    u4ContextId = pIfMap->u4VcNum;

    VCM_UNLOCK ();

    switch (VCM_SISP_RED_SYNC_AUDIT_ACTION ())
    {
        case SISP_RED_ENABLE_START:

            /* ACTIVE Crashed while doing SISP enable  on port */

            /*--------------------------------------------------*
            | If the active crashed while doing SISP enable on  |
            | port, all the PVC entries added in the Hardware   |
            | for the PhyPort will be deleted.                  |
            |                                                   |
            | PVC entries will be deleted for all the Vlan 1 to |
            | LastSyncVlan(Received thru dynamic sync up). And  |
            | also for the VLANs from LastSyncVlan to NextMile  |
            | Stone Vlan(Vlan ID after which the ACTIVE would   |
            | have send the next sync up).                      |
            *--------------------------------------------------*/

            VCM_LOCK ();

            VCM_TRC_ARG1 (VCM_INIT_SHUT_TRC, "VCM-SISP-RED:SISP Hw Audit start "
                          "for SISP_ENABLE action failed on port %u in ACTIVE "
                          "node.\n", VCM_SISP_RED_SYNC_PHYIFINDEX ());

            if (VCM_SISP_RED_SYNC_LAST_SYNCED_VLAN () == 0)
            {
                /* Didn't receive sync up for any VLAN Milestone, then
                 * delete the PVC entries for the Vlan 1 to First
                 * mile stone */
                for (VlanId = 1; VlanId <= SISP_RED_MAX_VLAN_PER_PVC_UPDATE;
                     VlanId++)
                {
                    /* Error message for this API is ignored */
                    VcmFsVcmSispHwSetPortVlanMapping
                        (VCM_SISP_RED_SYNC_PHYIFINDEX (), VlanId,
                         u4ContextId, SISP_DELETE);
                }
            }
            else
            {
                /* Received sync up for some VLAN Milestone for which
                 * PVC entry addition to hardware has been already done. 
                 * Delete all the PVC entries in hardware for Vlan 1 to
                 * the MileStone VLAN for which PVC entries are already
                 * added.
                 *
                 * After the LastSyncedVlan, we are not sure for how 
                 * many Vlans PVC entry has been added to the HW by the 
                 * active. So delete all the PVC entries for the next
                 * milestone VLAN (Vlan in which ACTIVE would have send
                 * the sync up for LasySyncedVlan) */

                for (VlanId = 1;
                     ((VCM_SISP_RED_SYNC_LAST_SYNCED_VLAN () >= VlanId) ||
                      ((VCM_SISP_RED_SYNC_LAST_SYNCED_VLAN () <
                        VLAN_MAX_VLAN_ID)
                       && (VlanId % SISP_RED_MAX_VLAN_PER_PVC_UPDATE != 0)));
                     VlanId++)
                {
                    /* Error message for this API is ignored */
                    VcmFsVcmSispHwSetPortVlanMapping
                        (VCM_SISP_RED_SYNC_PHYIFINDEX (), VlanId,
                         u4ContextId, SISP_DELETE);
                }
            }

            VCM_TRC_ARG2 (VCM_INIT_SHUT_TRC, "VCM-SISP-RED:PVC entries for port"
                          " %u, for Vlan 1 to %d and vlan upto next milestone"
                          " is deleted from the hardware\n",
                          VCM_SISP_RED_SYNC_PHYIFINDEX (),
                          VCM_SISP_RED_SYNC_LAST_SYNCED_VLAN ());

            /* SISP Port Ctrl status would have set as ENABLED in the HW, by
             * ACTIVE, revert that to DISABLE */
            VcmFsVcmSispHwSetPortCtrlStatus (VCM_SISP_RED_SYNC_PHYIFINDEX (),
                                             SISP_DISABLE);

            VCM_TRC_ARG1 (VCM_INIT_SHUT_TRC, "VCM-SISP-RED: SISP Hw Audit done "
                          "for SISP_ENABLE action failed on port %u in ACTIVE "
                          "node.\n", VCM_SISP_RED_SYNC_PHYIFINDEX ());
            VCM_UNLOCK ();

            break;

        case SISP_RED_MOD_SHUT_START:

            /* ACTIVE Crashed while doing SISP shutdown */

            /*--------------------------------------------------*
            | If the active crashed while doing SISP shutdown   |
            | SISP status would be set as ENABLED in the HW     |
            | for the SISP enabled ports.                       |
            |                                                   |
            | SISP PortCtrl status & PVC entry programming for  |
            | all the interfaces before the before the          |
            | PVC_PHYIFINDEX received through SYNC up.  ACTIVE  |
            | has crashed after the PHY_IFINDEX received thru   |
            | SYNC up. Add the PVC entries for PVC_PHYIFINDEX   |
            *--------------------------------------------------*/

            VCM_TRC (VCM_INIT_SHUT_TRC, "VCM-SISP-RED:SISP Hw Audit start for"
                     "SISP module shutdown  action failed in ACTIVE node.\n");

            for (u4PhyIfIndex = 1;
                 u4PhyIfIndex < VCM_SISP_RED_SYNC_PHYIFINDEX (); u4PhyIfIndex++)
            {
                VCM_LOCK ();

                if (SISP_PVC_TABLE_ENTRY (u4PhyIfIndex) != NULL)
                {
                    VcmFsVcmSispHwSetPortCtrlStatus (u4PhyIfIndex, SISP_ENABLE);

                    for (VlanId = 1; VlanId <= VLAN_MAX_VLAN_ID; VlanId++)
                    {
                        if (VcmSispGetPvcTableEntry (u4PhyIfIndex, VlanId,
                                                     &VcmIfMapEntry)
                            == VCM_FAILURE)
                        {
                            continue;
                        }

                        /* Error message for this API is ignored */
                        VcmFsVcmSispHwSetPortVlanMapping (u4PhyIfIndex, VlanId,
                                                          VcmIfMapEntry.u4VcNum,
                                                          SISP_ADD);
                    }
                }

                VCM_UNLOCK ();
            }
            /* Intentional fall through */
            /* To program PVC entries for the failed PHY_IFINDEX */

        case SISP_RED_DISABLE_START:

            /* ACTIVE Crashed while doing SISP disable  on port */

            /*--------------------------------------------------*
            | If the active crashed while doing SISP disable on |
            | port, all the PVC entries deleted in the Hardware |
            | for the PhyPort will be added again.              |
            |                                                   |
            | PVC entries will be added for all the Vlan 1 to   |
            | LastSyncVlan(Received thru dynamic sync up). And  |
            | also for the VLANs from LastSyncVlan to NextMile  |
            | Stone Vlan(Vlan ID after which the ACTIVE would   |
            | have send the next sync up).                      |
            *--------------------------------------------------*/

            VCM_LOCK ();

            VCM_TRC_ARG1 (VCM_INIT_SHUT_TRC, "VCM-SISP-RED:SISP Hw Audit start "
                          "for SISP_DISABLE action failed on port %u in ACTIVE "
                          "node.\n", VCM_SISP_RED_SYNC_PHYIFINDEX ());

            /* SISP Port Ctrl status would have set as DISABLED in the HW, by
             * ACTIVE, revert that to ENABLE. This has to be done, before
             * adding PVC entries to the HW.*/

            VcmFsVcmSispHwSetPortCtrlStatus (VCM_SISP_RED_SYNC_PHYIFINDEX (),
                                             SISP_ENABLE);

            if (VCM_SISP_RED_SYNC_LAST_SYNCED_VLAN () == 0)
            {
                /* Didn't receive sync up for any VLAN Milestone, then
                 * add the PVC entries for the Vlan 1 to First
                 * mile stone */

                for (VlanId = 1; VlanId <= SISP_RED_MAX_VLAN_PER_PVC_UPDATE;
                     VlanId++)
                {
                    if (VcmSispGetPvcTableEntry
                        (VCM_SISP_RED_SYNC_PHYIFINDEX (), VlanId,
                         &VcmIfMapEntry) == VCM_FAILURE)
                    {
                        continue;
                    }

                    /* Error message for this API is ignored */
                    FsVcmSispHwSetPortVlanMapping
                        (VCM_SISP_RED_SYNC_PHYIFINDEX (), VlanId,
                         VcmIfMapEntry.u4VcNum, SISP_ADD);
                }
            }
            else
            {
                /* Received sync up for some VLAN Milestone for which
                 * PVC entry deletion to hardware has been already done. 
                 * Add all the PVC entries in hardware for Vlan 1 to
                 * the MileStone VLAN for which PVC entries are already
                 * deleted.
                 *
                 * After the LastSyncedVlan, we are not sure for how 
                 * many Vlans PVC entry has been deleted to the HW by the 
                 * active. So add all the PVC entries for the next
                 * milestone VLAN (Vlan in which ACTIVE would have send
                 * the sync up for LasySyncedVlan) */

                for (VlanId = 1;
                     ((VCM_SISP_RED_SYNC_LAST_SYNCED_VLAN () >= VlanId) ||
                      ((VCM_SISP_RED_SYNC_LAST_SYNCED_VLAN () <
                        VLAN_MAX_VLAN_ID)
                       && (VlanId % SISP_RED_MAX_VLAN_PER_PVC_UPDATE != 0)));
                     VlanId++)
                {
                    if (VcmSispGetPvcTableEntry
                        (VCM_SISP_RED_SYNC_PHYIFINDEX (), VlanId,
                         &VcmIfMapEntry) == VCM_FAILURE)
                    {
                        continue;
                    }

                    /* Error message for this API is ignored */
                    VcmFsVcmSispHwSetPortVlanMapping
                        (VCM_SISP_RED_SYNC_PHYIFINDEX (), VlanId,
                         VcmIfMapEntry.u4VcNum, SISP_ADD);
                }
            }

            VCM_TRC_ARG2 (VCM_INIT_SHUT_TRC, "VCM-SISP-RED:PVC entries for port"
                          " %u, for Vlan 1 to %d and vlan upto next milestone"
                          " is added to the hardware\n",
                          VCM_SISP_RED_SYNC_PHYIFINDEX (),
                          VCM_SISP_RED_SYNC_LAST_SYNCED_VLAN ());

            VCM_TRC_ARG1 (VCM_INIT_SHUT_TRC, "VCM-SISP-RED: SISP Hw Audit for "
                          "SISP_DISABLE action failed on port %u in ACTIVE "
                          "node.\n", VCM_SISP_RED_SYNC_PHYIFINDEX ());

            VCM_UNLOCK ();

            break;
    }

    /* AUDIT is over, Clear PVC HW Audit information */
    VCM_MEMSET (&(VCM_SISP_HW_AUDIT_INFO ()), VCM_INIT_VAL,
                sizeof (tVcmSispHwAuditInfo));

    VCM_SISP_RED_STATIC_SYNC_RCVD () = VCM_FALSE;

    VCM_TRC (VCM_INIT_SHUT_TRC, "VCM-SISP-RED: SISP Hw Audit done\n");
#endif
    return;
}

/*****************************************************************************/
/* Function Name      : VcmSispRedSendSispSyncMsg                            */
/*                                                                           */
/* Description        : This function is invoked to  send sync up message    */
/*                      for SISP informations. SYNC Up message for various   */
/*                      scenarios will be send. Various scenarios are        */
/*                      identified using the u1Action variable               */
/*                                                                           */
/* Input(s)           : u4PhyIfIndex - Physical Port Index                   */
/*                      u1Action     - Various Actions                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS/VCM_FAILURE.                             */
/*****************************************************************************/
INT4
VcmSispRedSendSispSyncMsg (UINT1 u1SubType, tVcmSispHwAuditInfo SispHwAuditInfo)
{
    tRmProtoEvt         ProtoEvt;
    VOID               *pBuf = NULL;
    UINT4               u4Offset = VCM_INIT_VAL;
    UINT2               u2Len = VCM_SISP_RED_SYNC_MSG_LEN;
    UINT1               u1MessageType = VCM_SISP_SYNC_MSG;

    if (VCM_NODE_STATUS () != VCM_RED_ACTIVE)
    {
        return VCM_SUCCESS;
    }

    if (u1SubType == VCM_SISP_PVC_STATUS_MSG)
    {
        /* Send SYNC UP only for Mile Stone VLANs. Milestone vlans
         * will be identified based on SISP_RED_MAX_VLAN_PER_PVC_UPDATE.
         * i.e. Sync up will be done for the multiples for 
         * SISP_RED_MAX_VLAN_PER_PVC_UPDATE Vlan IDs. 
         *
         * Also, the SYNC up would be send for the MAX_VLAN_ID(4094) */

        if (((SispHwAuditInfo.LastSyncVlan % SISP_RED_MAX_VLAN_PER_PVC_UPDATE)
             != 0) && (SispHwAuditInfo.LastSyncVlan != VLAN_MAX_VLAN_ID))
        {
            return VCM_SUCCESS;
        }
    }

    ProtoEvt.u4AppId = RM_VCM_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if ((pBuf = RM_ALLOC_TX_BUF (u2Len)) == NULL)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC, "RM Allocation Failed\n");
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        VcmRmHandleProtocolEvent (&ProtoEvt);
        return VCM_FAILURE;
    }

    VCM_RM_PUT_1_BYTE (pBuf, &u4Offset, u1MessageType);
    VCM_RM_PUT_2_BYTE (pBuf, &u4Offset, u2Len);

    /* Synchronize the SISP Port Control status information 
     * 
     * <-1 byte->|<-2 bytes->|<-1 bytes->| <- 4 bytes->|<-2 byte->|<-1 byte->|
     * -----------------------------------------------------------------------
     * |Msg. Type|  Length   |  Sub Type |u4PhyIfIndex| Last Sync| u1Action  |
     * |   (1)   |    11     |           |            |   VLAN   | (Enable / |
     * |         |           |           |            |          |  Disable/ |
     * |         |           |           |            |          | Mod Shut  |
     * |---------------------------------------------------------------------|
     *
     * The RM Header will be appended by RM.
     */

    switch (u1SubType)
    {
        case VCM_SISP_PORT_CTRL_MSG:

            VCM_RM_PUT_1_BYTE (pBuf, &u4Offset, VCM_SISP_PORT_CTRL_MSG);
            VCM_RM_PUT_4_BYTE (pBuf, &u4Offset, SispHwAuditInfo.u4PhyIfIndex);
            VCM_RM_PUT_2_BYTE (pBuf, &u4Offset, VCM_INIT_VAL);
            VCM_RM_PUT_1_BYTE (pBuf, &u4Offset, SispHwAuditInfo.u1Action);

            VCM_TRC_ARG2 (VCM_INIT_SHUT_TRC, "VCM-SISP-RED: SISP Port ctrl for "
                          "port %u action %d is sent\n",
                          SispHwAuditInfo.u4PhyIfIndex,
                          SispHwAuditInfo.u1Action);
            break;

        case VCM_SISP_PVC_STATUS_MSG:

            VCM_RM_PUT_1_BYTE (pBuf, &u4Offset, VCM_SISP_PVC_STATUS_MSG);
            VCM_RM_PUT_4_BYTE (pBuf, &u4Offset, SispHwAuditInfo.u4PhyIfIndex);
            VCM_RM_PUT_2_BYTE (pBuf, &u4Offset, SispHwAuditInfo.LastSyncVlan);
            VCM_RM_PUT_1_BYTE (pBuf, &u4Offset, VCM_INIT_VAL);

            VCM_TRC_ARG2 (VCM_INIT_SHUT_TRC, "VCM-SISP-RED: SISP PVC update "
                          "for port %u & Vlan %d is sent\n",
                          SispHwAuditInfo.u4PhyIfIndex,
                          SispHwAuditInfo.LastSyncVlan);
            break;

        case VCM_SISP_SYNC_COMPLETE:

            VCM_RM_PUT_1_BYTE (pBuf, &u4Offset, VCM_SISP_SYNC_COMPLETE);
            VCM_RM_PUT_4_BYTE (pBuf, &u4Offset, VCM_INIT_VAL);
            VCM_RM_PUT_2_BYTE (pBuf, &u4Offset, VCM_INIT_VAL);
            VCM_RM_PUT_1_BYTE (pBuf, &u4Offset, VCM_INIT_VAL);

            VCM_TRC (VCM_INIT_SHUT_TRC, "VCM-SISP-RED: SISP Operationcomplete "
                     "sync sent\n");
            break;

        default:
            RM_FREE (pBuf);
            return VCM_FAILURE;
    }

    if (VcmRedSendUpdateToRM ((tRmMsg *) pBuf, u2Len) == VCM_FAILURE)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        VcmRmHandleProtocolEvent (&ProtoEvt);

        VCM_TRC (VCM_INIT_SHUT_TRC | VCM_ALL_FAILURE_TRC, "VCM-SISP-RED: SISP "
                 "Sending Sync up failed.\n");
        return VCM_FAILURE;
    }
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispRedRecvSispSyncMsg                            */
/*                                                                           */
/* Description        : This function is invoked to  recv sync up message    */
/*                      for SISP informations. SYNC Up message for various   */
/*                      scenarios will be send. Various scenarios are        */
/*                      identified using the u1Action variable               */
/*                                                                           */
/* Input(s)           : pData        - Sync Up data                          */
/*                      pu4Offset    - Identifies the offset in pData        */
/*                      u2Len        - Length of the Sync up msg recieved    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS/VCM_FAILURE.                             */
/*****************************************************************************/
INT4
VcmSispRedRecvSispSyncMsg (VOID *pData, UINT4 *pu4Offset, UINT2 u2Len)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4PhyIfIndex;
    tVlanId             LastSyncVlan;
    UINT1               u1SubType;
    UINT1               u1Action;

    if (VCM_NODE_STATUS () != VCM_RED_STANDBY)
    {
        return VCM_SUCCESS;
    }

    ProtoEvt.u4AppId = RM_VCM_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (u2Len != VCM_SISP_RED_SYNC_MSG_LEN)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        VcmRmHandleProtocolEvent (&ProtoEvt);
        return VCM_FAILURE;
    }

    VCM_RM_GET_1_BYTE (pData, pu4Offset, u1SubType);
    VCM_RM_GET_4_BYTE (pData, pu4Offset, u4PhyIfIndex);
    VCM_RM_GET_2_BYTE (pData, pu4Offset, LastSyncVlan);
    VCM_RM_GET_1_BYTE (pData, pu4Offset, u1Action);

    switch (u1SubType)
    {
        case VCM_SISP_SYNC_COMPLETE:

            if (VCM_SISP_RED_STATIC_SYNC_RCVD () == VCM_TRUE)
            {
                VCM_SISP_RED_STATIC_SYNC_RCVD () = VCM_FALSE;
            }

            break;

        case VCM_SISP_PORT_CTRL_MSG:

            if (VCM_SISP_RED_STATIC_SYNC_RCVD () == VCM_FALSE)
            {
                VCM_SISP_RED_SYNC_PHYIFINDEX () = u4PhyIfIndex;
                VCM_SISP_RED_SYNC_AUDIT_ACTION () = u1Action;

                VCM_TRC_ARG2 (VCM_INIT_SHUT_TRC, "VCM-SISP-RED: SISP Port ctrl"
                              " syncup rcvd for port %u action %d\n",
                              u4PhyIfIndex, u1Action);
            }

            break;

        case VCM_SISP_PVC_STATUS_MSG:

            if (VCM_SISP_RED_STATIC_SYNC_RCVD () == VCM_FALSE)
            {
                VCM_SISP_RED_SYNC_PHYIFINDEX () = u4PhyIfIndex;
                VCM_SISP_RED_SYNC_LAST_SYNCED_VLAN () = LastSyncVlan;

                VCM_TRC_ARG2 (VCM_INIT_SHUT_TRC, "VCM-SISP-RED: PVC update sync"
                              "up rcvd for port %u vlan %d\n",
                              u4PhyIfIndex, LastSyncVlan);
            }

            break;
        default:
            break;
    }
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispRedStaticSyncRcvd                             */
/*                                                                           */
/* Description        : This function will be invoked when nmhSet* function  */
/*                      called. This function is used to clear the Dynamic   */
/*                      sync up information received in the standby, when    */
/*                      static sync up reached.                              */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VcmSispRedStaticSyncRcvd (VOID)
{
    if (VCM_NODE_STATUS () != VCM_RED_STANDBY)
    {
        return;
    }

    VCM_SISP_RED_STATIC_SYNC_RCVD () = VCM_TRUE;

    return;
}

/*****************************************************************************/
/* Function Name      : VcmSispRedSendModuleStatus                           */
/*                                                                           */
/* Description        : This function used to send SISP module start/shudown */
/*                      sync up information to the standby.                  */
/*                                                                           */
/* Input(s)           : u1Status                                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispRedSendModuleStatus (UINT1 u1Status)
{
    tVcmSispHwAuditInfo SispHwAuditInfo;
    INT4                i4RetVal = VCM_SUCCESS;

    if (u1Status == SISP_SHUTDOWN)
    {
        VcmSispRedStaticSyncRcvd ();

        SispHwAuditInfo.u4PhyIfIndex = VCM_INIT_VAL;
        SispHwAuditInfo.u1Action = SISP_RED_MOD_SHUT_START;

        i4RetVal = VcmSispRedSendSispSyncMsg (VCM_SISP_PORT_CTRL_MSG,
                                              SispHwAuditInfo);
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : VcmSispRedSendPortCtrlStatus                         */
/*                                                                           */
/* Description        : This function used to send SISP port ctrl status     */
/*                      (SISP_ENABLE / SISP_DISABLE) sync up information to  */
/*                      the standby.                                         */
/*                                                                           */
/* Input(s)           : u4PhyIfIndex - PhyIfIndex                            */
/*                      u1Status     - SISP_ENABLE/SISP_DISABLE              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispRedSendPortCtrlStatus (UINT4 u4PhyIfIndex, UINT1 u1Status)
{
    tVcmSispHwAuditInfo SispHwAuditInfo;
    UINT1               u1Action;

    VcmSispRedStaticSyncRcvd ();

    u1Action = ((u1Status == SISP_ENABLE) ? SISP_RED_ENABLE_START
                : SISP_RED_DISABLE_START);

    SispHwAuditInfo.u4PhyIfIndex = u4PhyIfIndex;
    SispHwAuditInfo.u1Action = u1Action;

    return (VcmSispRedSendSispSyncMsg
            (VCM_SISP_PORT_CTRL_MSG, SispHwAuditInfo));
}

/*****************************************************************************/
/* Function Name      : VcmSispRedSendPvcStatus                              */
/*                                                                           */
/* Description        : This function used to send Port Vlan Context Entry   */
/*                      updation status information to the stand by.         */
/*                                                                           */
/* Input(s)           : u4PhyIfIndex - PhyIfIndex                            */
/*                      LastSyncVlan - Vlan Identifier                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispRedSendPvcStatus (UINT4 u4PhyIfIndex, tVlanId LastSyncVlan)
{
    tVcmSispHwAuditInfo SispHwAuditInfo;

    SispHwAuditInfo.u4PhyIfIndex = u4PhyIfIndex;
    SispHwAuditInfo.LastSyncVlan = LastSyncVlan;

    return (VcmSispRedSendSispSyncMsg
            (VCM_SISP_PVC_STATUS_MSG, SispHwAuditInfo));
}

/*****************************************************************************/
/* Function Name      : VcmSispRedSendCompleteStatus                         */
/*                                                                           */
/* Description        : This function used to send Operation complete        */
/*                      status information to the stand by.                  */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispRedSendCompleteStatus (VOID)
{
    tVcmSispHwAuditInfo SispHwAuditInfo;

    return (VcmSispRedSendSispSyncMsg
            (VCM_SISP_SYNC_COMPLETE, SispHwAuditInfo));
}
