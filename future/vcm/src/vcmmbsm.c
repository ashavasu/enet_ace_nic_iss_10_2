/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vcmmbsm.c,v 1.13 2014/12/09 12:46:07 siva Exp $
 *
 *******************************************************************/
#ifdef MBSM_WANTED
#include  "vcminc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VcmMbsmProcessUpdate                             */
/*                                                                           */
/*    Description         : This function is an API for mbsm module to       */
/*                          update the VCM module a new LC                   */
/*                          is inserted or removed                           */
/*                                                                           */
/*    Input(s)            : pPortInfo - Inserted port list                   */
/*                          pSlotInfo - Information about inserted slot      */
/*                          i4Event   - Event type.                          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : MBSM_SUCCESS on success                           */
/*                         MBSM_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/
INT4
VcmMbsmProcessUpdate (tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo,
                      INT4 i4Event)
{
    INT4                i4RetVal;

    switch (i4Event)
    {
        case MBSM_MSG_CARD_INSERT:

            i4RetVal = VcmMbsmUpdateOnCardInsertion (pPortInfo, pSlotInfo);
            break;

        case MBSM_MSG_CARD_REMOVE:

            VCM_TRC (VCM_CONTROL_PATH_TRC, "VcmMbsmProcessUpdate updated the VCM module on card removal and"
                               " returns success \n");
            i4RetVal = MBSM_SUCCESS;
            break;

        default:
            VCM_TRC (VCM_ALL_FAILURE_TRC, "VcmMbsmProcessUpdate returns failure \n");
            i4RetVal = MBSM_FAILURE;
    }

    if (i4RetVal == MBSM_SUCCESS)
    {
	VCM_TRC (VCM_CONTROL_PATH_TRC, "VcmMbsmProcessUpdate updated the VCM module on a card insertion and"
                               " returns success \n");
    }
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VcmMbsmUpdateOnCardInsertion                     */
/*                                                                           */
/*    Description         : This function walks both the Context and         */
/*                          Interface-Mapping table and programs the         */
/*                          hardware on Line Card Insertion.                 */
/*                                                                           */
/*    Input(s)            : pPortInfo - Inserted port list                   */
/*                          pSlotInfo - Information about inserted slot      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : MBSM_SUCCESS on success                           */
/*                         MBSM_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/
INT4
VcmMbsmUpdateOnCardInsertion (tMbsmPortInfo * pPortInfo,
                              tMbsmSlotInfo * pSlotInfo)
{
    UINT4               au4SispPorts[L2IWF_MAX_CONTEXTS];
    UINT4               u4PortCount;
    UINT4               u4PortNo;
    UINT4               u4PrevContextId;
    UINT4               u4ContextId;
    UINT4               u4PortCnt = VCM_INIT_VAL;
    UINT4               u4TempPortCnt = VCM_INIT_VAL;
    UINT4               u4PortNum = VCM_INIT_VAL;
    UINT2               u2LocalPortId;
    UINT1               u1IsSetInPortList = OSIX_FALSE;
    tContextMapInfo     ContextInfo;
    INT4                i4CxtType;
    tVcmIfMapEntry     *pIfMap = NULL;
    tVcmIfMapEntry     *pNextIfMap = NULL;
    tVcmIfMapEntry      NextIfMap;
    tVrMapAction        VrMapAction;
    tFsNpVcmVrMapInfo   VcmVrMapInfo;
    tCfaIfInfo          CfaIfInfo;
    UINT1               u1IfType = 0;
    UINT1               u1IsSetInPortListStatus = OSIX_FALSE;

    VCM_MEMSET (&ContextInfo, 0, sizeof (tContextMapInfo));
    VCM_MEMSET (&au4SispPorts, VCM_INIT_VAL, sizeof (au4SispPorts));

    u4PortCount = MBSM_PORT_INFO_PORTCOUNT (pPortInfo);
    u4PortNo = MBSM_PORT_INFO_STARTINDEX (pPortInfo);

    if (VcmGetFirstVcId (&u4ContextId) == VCM_SUCCESS)
    {
        do
        {
            u4PrevContextId = u4ContextId;

            VcmGetVcCxtType (u4ContextId, &i4CxtType);
            if ((i4CxtType == VCM_L2_CONTEXT) ||
                (i4CxtType == VCM_L2_L3_CONTEXT))
            {
                if (OSIX_FALSE == MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
                    /* Skip this for Port Insert/Remove messages */
                {
                    if (VcmFsVcmMbsmHwCreateContext (u4ContextId, pSlotInfo) ==
                        FNP_FAILURE)
                    {
                        VCM_TRC (VCM_ALL_FAILURE_TRC,
                                 "Card Insertion: FsVcmMbsmHwCreateContext Failed \n");
                        return MBSM_FAILURE;
                    }
                    if (i4CxtType == VCM_L2_L3_CONTEXT)
                    {
                        VrMapAction = NP_HW_CREATE_VIRTUAL_ROUTER;
                        VcmVrMapInfo.u4VrfId = u4ContextId;
                        VcmGetVRMacAddr (u4ContextId, VcmVrMapInfo.MacAddr);

                        if (VcmFsVcmMbsmHwMapVirtualRouter
                            (VrMapAction, &VcmVrMapInfo,
                             pSlotInfo) == FNP_FAILURE)
                        {
                            VCM_TRC (VCM_ALL_FAILURE_TRC,
                                     "Card Insertion: FsVcmMbsmHwMapVirtualRouter Failed \n");
                            return MBSM_FAILURE;
                        }
                    }

                }
            }
            else if (i4CxtType == VCM_L3_CONTEXT)
            {
                VrMapAction = NP_HW_CREATE_VIRTUAL_ROUTER;
                VcmVrMapInfo.u4VrfId = u4ContextId;
                VcmGetVRMacAddr (u4ContextId, VcmVrMapInfo.MacAddr);

                if (OSIX_FALSE == MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
                    /* Skip this for Port Insert/Remove messages */
                {
                    if (VcmFsVcmMbsmHwMapVirtualRouter
                        (VrMapAction, &VcmVrMapInfo, pSlotInfo) == FNP_FAILURE)
                    {
                        VCM_TRC (VCM_ALL_FAILURE_TRC,
                                 "Card Insertion: FsVcmMbsmHwMapVirtualRouter Failed \n");
                        return MBSM_FAILURE;
                    }
                }
            }

        }
        while (VcmGetNextVcId (u4PrevContextId, &u4ContextId) == VCM_SUCCESS);
    }

    while (u4PortCount != 0)
    {
        OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST (pPortInfo), u4PortNo,
                                 sizeof (tPortList), u1IsSetInPortList);
        OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST_STATUS (pPortInfo),
                                 u4PortNo, sizeof (tPortList),
                                 u1IsSetInPortListStatus);

        if ((u1IsSetInPortList == OSIX_TRUE)
            && (u1IsSetInPortListStatus == OSIX_FALSE))
        {
            if (VcmIsIfMapExist (u4PortNo) == VCM_TRUE)
            {
                VcmGetContextInfoFromIfIndex (u4PortNo, &u4ContextId,
                                              &u2LocalPortId);

                if (VcmFsVcmMbsmHwMapPortToContext
                    (u4ContextId, u4PortNo, pSlotInfo) == FNP_FAILURE)
                {
                    VCM_TRC (VCM_ALL_FAILURE_TRC, "Card Insertion: "
                             "VcmFsVcmMbsmHwMapPortToContext Failed \n");
                    return MBSM_FAILURE;
                }

                ContextInfo.u2LocalPortId = u2LocalPortId;
                ContextInfo.u1IsPrimaryCtx = VCM_TRUE;

                if (VcmFsVcmMbsmHwMapIfIndexToBrgPort
                    (u4ContextId, u4PortNo, ContextInfo, pSlotInfo) ==
                    FNP_FAILURE)
                {
                    VCM_TRC (VCM_ALL_FAILURE_TRC, "Card Insertion: "
                             "VcmFsVcmMbsmHwMapIfIndexToBrgPort Failed \n");
                    return MBSM_FAILURE;
                }

                if (VcmSispIsEnabled (u4PortNo) != VCM_FALSE)
                {
                    /* Program the SISP enabled status in the hardware */

                    VcmFsVcmSispMbsmHwSetPortCtrlStatus (u4PortNo, SISP_ENABLE,
                                                         pSlotInfo);

                    /* Invoke the API provided by SISP to get the set of lo 
                     * ports  */
                    if (VcmSispGetSispPortsInfoOfPhysicalPort (u4PortNo,
                                                               VCM_FALSE,
                                                               (VOID *)
                                                               au4SispPorts,
                                                               &u4PortCnt)
                        != VCM_SUCCESS)
                    {
                        VCM_TRC (VCM_ALL_FAILURE_TRC, "Card Insertion: "
                                 "VcmSispGetSispPortsInfoOfPhysicalPort Failed "
                                 "\n");
                        u4PortCount--;
                        continue;
                    }

                    for (u4TempPortCnt = 0; u4TempPortCnt < u4PortCnt;)
                    {
                        u4PortNum = au4SispPorts[u4TempPortCnt];

                        if (u4PortNum == VCM_INIT_VAL)
                        {
                            /* The Array should consist of if index upto 
                             * u4PortCnt. Hence, u4PortNum cannot be zero in 
                             * between 
                             * */
                            continue;
                        }

                        u4TempPortCnt++;

                        VcmGetContextInfoFromIfIndex (u4PortNum, &u4ContextId,
                                                      &u2LocalPortId);

                        if (VcmFsVcmMbsmHwMapPortToContext
                            (u4ContextId, u4PortNo, pSlotInfo) == FNP_FAILURE)
                        {
                            VCM_TRC (VCM_ALL_FAILURE_TRC, "Card Insertion: "
                                     "VcmFsVcmMbsmHwMapPortToContext Failed \n");
                            return MBSM_FAILURE;
                        }

                        ContextInfo.u2LocalPortId = u2LocalPortId;
                        ContextInfo.u1IsPrimaryCtx = VCM_FALSE;

                        if (VcmFsVcmMbsmHwMapIfIndexToBrgPort
                            (u4ContextId, u4PortNo, ContextInfo, pSlotInfo) ==
                            FNP_FAILURE)
                        {
                            VCM_TRC (VCM_ALL_FAILURE_TRC, "Card Insertion: "
                                     "VcmFsVcmMbsmHwMapIfIndexToBrgPort Fail\n");
                            return MBSM_FAILURE;
                        }
                    }            /*For all SISP logical interfaces */
                }                /*SISP enabled interface in attached slot */

            }
        }
        u4PortCount--;
        u4PortNo++;
    }
    pNextIfMap = VcmGetFirstIfMap ();

    while (pNextIfMap != NULL)
    {
        pIfMap = pNextIfMap;
        MEMSET (&NextIfMap, 0, sizeof (tVcmIfMapEntry));
        NextIfMap.u4IfIndex = pIfMap->u4IfIndex;
        pNextIfMap = VcmGetNextIfMap (&NextIfMap);

        MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
        CfaGetIfaceType (pIfMap->u4IfIndex, &u1IfType);
        CfaGetIfInfo (pIfMap->u4IfIndex, &CfaIfInfo);
        VrMapAction = NP_HW_MAP_INVALID;

        if ((u1IfType == CFA_ENET) && CfaIfInfo.u1BridgedIface != CFA_ENABLED)
        {
            VrMapAction = NP_HW_MAP_RPORT_TO_VR;
        }
        else if (u1IfType == CFA_L3IPVLAN)
        {
            VrMapAction = NP_HW_MAP_INTF_TO_VR;
            VcmVrMapInfo.u2VlanId = pIfMap->u2VlanId;
        }
        else if (u1IfType == CFA_LAGG)
        {
            VcmGetContextInfoFromIfIndex (pIfMap->u4IfIndex, &u4ContextId,
                                          &u2LocalPortId);

            if (VcmFsVcmMbsmHwMapPortToContext
                (u4ContextId, pIfMap->u4IfIndex, pSlotInfo) == FNP_FAILURE)
            {
                VCM_TRC (VCM_ALL_FAILURE_TRC, "Card Insertion: "
                         "VcmFsVcmMbsmHwMapPortToContext Failed \n");
                return MBSM_FAILURE;
            }

            ContextInfo.u2LocalPortId = u2LocalPortId;
            ContextInfo.u1IsPrimaryCtx = VCM_TRUE;

            if (VcmFsVcmMbsmHwMapIfIndexToBrgPort
                (u4ContextId, pIfMap->u4IfIndex, ContextInfo, pSlotInfo) ==
                FNP_FAILURE)
            {
                VCM_TRC (VCM_ALL_FAILURE_TRC, "Card Insertion: "
                         "VcmFsVcmMbsmHwMapIfIndexToBrgPort Failed \n");
                return MBSM_FAILURE;
            }
        }
        else
        {
            continue;
        }

        VcmVrMapInfo.u4VrfId = pIfMap->u4VcNum;
        VcmVrMapInfo.u4IfIndex = pIfMap->u4IfIndex;
        VcmVrMapInfo.u4L2CxtId = pIfMap->u4L2ContextId;
        VcmGetVRMacAddr (pIfMap->u4VcNum, VcmVrMapInfo.MacAddr);
        if (VcmFsVcmMbsmHwMapVirtualRouter
            (VrMapAction, &VcmVrMapInfo, pSlotInfo) == FNP_FAILURE)
        {
            VCM_TRC (VCM_ALL_FAILURE_TRC,
                     "Card Insertion: FsVcmMbsmHwMapVirtualRouter Failed \n");
            return MBSM_FAILURE;
        }
    }
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VcmSispMbsmProcessUpdate                         */
/*                                                                           */
/*    Description         : This function is an API for mbsm module to       */
/*                          update the VCM module a new LC                   */
/*                          is inserted or removed                           */
/*                                                                           */
/*    Input(s)            : pPortInfo - Inserted port list                   */
/*                          pSlotInfo - Information about inserted slot      */
/*                          i4Event   - Event type.                          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : MBSM_SUCCESS on success                           */
/*                         MBSM_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/
INT4
VcmSispMbsmProcessUpdate (tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo,
                          INT4 i4Event)
{
    INT4                i4RetVal;

    switch (i4Event)
    {
        case MBSM_MSG_CARD_INSERT:

            i4RetVal = VcmSispMbsmUpdateOnCardInsertion (pPortInfo, pSlotInfo);
            break;

        case MBSM_MSG_CARD_REMOVE:

            i4RetVal = VcmSispMbsmUpdateOnCardRemoval (pPortInfo, pSlotInfo);
            break;

        default:
            VCM_TRC (VCM_ALL_FAILURE_TRC, "VcmSispMbsmProcessUpdate returns failure \n");
            i4RetVal = MBSM_FAILURE;
    }

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VcmSispMbsmUpdateOnCardInsertion                 */
/*                                                                           */
/*    Description         : This function walks both the Context and         */
/*                          Interface-Mapping table and programs the         */
/*                          hardware on Line Card Insertion.                 */
/*                                                                           */
/*    Input(s)            : pPortInfo - Inserted port list                   */
/*                          pSlotInfo - Information about inserted slot      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : MBSM_SUCCESS on success                           */
/*                         MBSM_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/
INT4
VcmSispMbsmUpdateOnCardInsertion (tMbsmPortInfo * pPortInfo,
                                  tMbsmSlotInfo * pSlotInfo)
{
    UINT4               u4PortCount = VCM_INIT_VAL;
    UINT4               u4PortNo = VCM_INIT_VAL;
    UINT1               u1IsSetInPortList = OSIX_FALSE;
    tVlanId             VlanId = VCM_INIT_VAL;
    tVlanId             NextVlanId = VCM_INIT_VAL;
    tVcmIfMapEntry      VcmIfMapEntry;
    UINT1               u1IsSetInPortListStatus = OSIX_FALSE;

    u4PortCount = MBSM_PORT_INFO_PORTCOUNT (pPortInfo);
    u4PortNo = MBSM_PORT_INFO_STARTINDEX (pPortInfo);

    for (; u4PortCount != 0; u4PortCount--, u4PortNo++)
    {

        OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST (pPortInfo), u4PortNo,
                                 sizeof (tPortList), u1IsSetInPortList);
        OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST_STATUS (pPortInfo),
                                 u4PortNo, sizeof (tPortList),
                                 u1IsSetInPortListStatus);

        if ((u1IsSetInPortList == OSIX_TRUE)
            && (u1IsSetInPortListStatus == OSIX_FALSE))
        {
            if (VcmIsIfMapExist (u4PortNo) != VCM_TRUE)
            {
                continue;
            }

            if (VcmSispIsEnabled (u4PortNo) != VCM_TRUE)
            {
                continue;
            }

            VlanId = VCM_INIT_VAL;

            /* Walk the PVC Table for programming the hardware */

            while (VcmSispPortVlanMapEntryGetNext (u4PortNo, VlanId,
                                                   &NextVlanId,
                                                   &VcmIfMapEntry) !=
                   VCM_FAILURE)
            {
                /* Program the hardware for this entry */
                if (VcmIfMapEntry.u4PhyIfIndex != u4PortNo)
                {
                    /* Database has skipped for next port entry
                     * This may or may not be present in this slot
                     * Hence, ignore this and loop for the next port present
                     * in this slot*/
                    break;
                }

                if (VcmFsVcmSispMbsmHwSetPortVlanMapping (u4PortNo, NextVlanId,
                                                          VcmIfMapEntry.u4VcNum,
                                                          SISP_ADD, pSlotInfo)
                    == FNP_FAILURE)
                {
                    VCM_TRC (VCM_ALL_FAILURE_TRC, "Card Insertion: "
                             "FsVcmSispMbsmHwSetPortVlanMapping Failed \n");
                    return MBSM_FAILURE;
                }
                VlanId = NextVlanId;
            }                    /* Walk of PVC table */
        }                        /* Walk of PVC table */
    }                            /* For all the ports present in the slot */
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VcmSispMbsmUpdateOnCardRemoval                   */
/*                                                                           */
/*    Description         : This function walks both the Context and         */
/*                          Interface-Mapping table and programs the         */
/*                          hardware on Line Card deletion.                  */
/*                                                                           */
/*    Input(s)            : pPortInfo - Inserted port list                   */
/*                          pSlotInfo - Information about inserted slot      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : MBSM_SUCCESS on success                           */
/*                         MBSM_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/
INT4
VcmSispMbsmUpdateOnCardRemoval (tMbsmPortInfo * pPortInfo,
                                tMbsmSlotInfo * pSlotInfo)
{
    UINT4               u4PortCount = VCM_INIT_VAL;
    UINT4               u4PortNo = VCM_INIT_VAL;
    UINT1               u1IsSetInPortListStatus = OSIX_FALSE;
    tVlanId             VlanId = VCM_INIT_VAL;
    tVlanId             NextVlanId = VCM_INIT_VAL;
    tVcmIfMapEntry      VcmIfMapEntry;

    u4PortCount = MBSM_PORT_INFO_PORTCOUNT (pPortInfo);
    u4PortNo = MBSM_PORT_INFO_STARTINDEX (pPortInfo);

    for (; u4PortCount != 0; u4PortCount--, u4PortNo++)
    {

        OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST_STATUS (pPortInfo),
                                 u4PortNo, sizeof (tPortList),
                                 u1IsSetInPortListStatus);

        if (u1IsSetInPortListStatus != OSIX_TRUE)
        {
            continue;
        }

        if (VcmIsIfMapExist (u4PortNo) != VCM_TRUE)
        {
            continue;
        }

        if (VcmSispIsEnabled (u4PortNo) != VCM_TRUE)
        {
            continue;
        }

        /* Program the SISP enabled status in the hardware */

        VcmFsVcmSispMbsmHwSetPortCtrlStatus (u4PortNo, SISP_DISABLE, pSlotInfo);

        VlanId = VCM_INIT_VAL;

        /* Walk the PVC Table for programming the hardware */

        while (VcmSispPortVlanMapEntryGetNext (u4PortNo, VlanId,
                                               &NextVlanId,
                                               &VcmIfMapEntry) != VCM_FAILURE)
        {
            /* Program the hardware for this entry */
            if (VcmIfMapEntry.u4PhyIfIndex != u4PortNo)
            {
                /* Database has skipped for next port entry
                 * This may or may not be present in this slot
                 * Hence, ignore this and loop for the next port present
                 * in this slot*/
                break;
            }

            if (VcmFsVcmSispMbsmHwSetPortVlanMapping (u4PortNo, NextVlanId,
                                                      VcmIfMapEntry.u4VcNum,
                                                      SISP_DELETE, pSlotInfo)
                == FNP_FAILURE)
            {
                VCM_TRC (VCM_ALL_FAILURE_TRC, "Card Insertion: "
                         "FsVcmSispMbsmHwSetPortVlanMapping Failed \n");
                return MBSM_FAILURE;
            }

            VlanId = NextVlanId;
        }                        /* Walk of PVC table */
    }                            /* For all the ports present in the slot */
    return MBSM_SUCCESS;
}
#endif
