/* $Id: fsvcmwr.c,v 1.12 2017/11/14 07:31:19 siva Exp $*/
# include  "lr.h"
# include  "vcminc.h"
# include  "fssnmp.h"
# include  "fsvcmwr.h"
# include  "fsvcmdb.h"

VOID
RegisterFSVCM ()
{
    SNMPRegisterMib (&fsvcmOID, &fsvcmEntry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fsvcmOID, (const UINT1 *) "fsvcm");
}

VOID
UnRegisterFSVCM ()
{
    SNMPUnRegisterMib (&fsvcmOID, &fsvcmEntry);
    SNMPDelSysorEntry (&fsvcmOID, (const UINT1 *) "fsvcm");
}

INT4
FsVcmTraceOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VCM_CONF_LOCK ();

    i4RetVal = nmhGetFsVcmTraceOption (&(pMultiData->i4_SLongValue));

    VCM_CONF_UNLOCK ();
    return i4RetVal;
}

INT4
FsVcmTraceOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VCM_CONF_LOCK ();

    i4RetVal = nmhSetFsVcmTraceOption (pMultiData->i4_SLongValue);

    VCM_CONF_UNLOCK ();
    return i4RetVal;
}

INT4
FsVcmTraceOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    VCM_CONF_LOCK ();

    i4RetVal = nmhTestv2FsVcmTraceOption (pu4Error, pMultiData->i4_SLongValue);

    VCM_CONF_UNLOCK ();
    return i4RetVal;
}

INT4
FsVcmTraceOptionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsVcmTraceOption (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsVcmConfigTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsVcmVcId;

    VCM_CONF_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsVcmConfigTable (&i4FsVcmVcId) == SNMP_FAILURE)
        {
            VCM_CONF_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsVcmConfigTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4FsVcmVcId) == SNMP_FAILURE)
        {
            VCM_CONF_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsVcmVcId;
    VCM_CONF_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsVCNextFreeHlPortIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VCM_CONF_LOCK ();

    if (nmhValidateIndexInstanceFsVcmConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VCM_CONF_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsVCNextFreeHlPortId (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue));

    VCM_CONF_UNLOCK ();
    return i4RetVal;
}

INT4
FsVCMacAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VCM_CONF_LOCK ();

    if (nmhValidateIndexInstanceFsVcmConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VCM_CONF_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    i4RetVal = nmhGetFsVCMacAddress (pMultiIndex->pIndex[0].i4_SLongValue,
                                     (tMacAddr *) pMultiData->pOctetStrValue->
                                     pu1_OctetList);

    VCM_CONF_UNLOCK ();
    return i4RetVal;
}

INT4
FsVcAliasGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VCM_CONF_LOCK ();

    if (nmhValidateIndexInstanceFsVcmConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VCM_CONF_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsVcAlias (pMultiIndex->pIndex[0].i4_SLongValue,
                                pMultiData->pOctetStrValue);

    VCM_CONF_UNLOCK ();
    return i4RetVal;
}

INT4
FsVCStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VCM_CONF_LOCK ();

    if (nmhValidateIndexInstanceFsVcmConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VCM_CONF_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsVCStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                 &(pMultiData->i4_SLongValue));

    VCM_CONF_UNLOCK ();
    return i4RetVal;
}

INT4
FsVcCxtTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VCM_CONF_LOCK ();

    if (nmhValidateIndexInstanceFsVcmConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VCM_CONF_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsVcCxtType (pMultiIndex->pIndex[0].i4_SLongValue,
                                  &(pMultiData->i4_SLongValue));

    VCM_CONF_UNLOCK ();
    return i4RetVal;

}

INT4
FsVRMacAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VCM_CONF_LOCK ();

    if (nmhValidateIndexInstanceFsVcmConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VCM_CONF_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    i4RetVal = nmhGetFsVRMacAddress
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList);
    VCM_CONF_UNLOCK ();
    return i4RetVal;
}

INT4
FsVcAliasSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VCM_CONF_LOCK ();

    i4RetVal = nmhSetFsVcAlias (pMultiIndex->pIndex[0].i4_SLongValue,
                                pMultiData->pOctetStrValue);

    VCM_CONF_UNLOCK ();
    return i4RetVal;
}

INT4
FsVCStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VCM_CONF_LOCK ();

    i4RetVal = nmhSetFsVCStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiData->i4_SLongValue);

    VCM_CONF_UNLOCK ();
    return i4RetVal;
}

INT4
FsVcCxtTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VCM_CONF_LOCK ();

    i4RetVal = nmhSetFsVcCxtType (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiData->i4_SLongValue);

    VCM_CONF_UNLOCK ();
    return i4RetVal;
}

INT4
FsVcAliasTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VCM_CONF_LOCK ();

    i4RetVal = nmhTestv2FsVcAlias (pu4Error,
                                   pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiData->pOctetStrValue);

    VCM_CONF_UNLOCK ();
    return i4RetVal;
}

INT4
FsVCStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VCM_CONF_LOCK ();

    i4RetVal = nmhTestv2FsVCStatus (pu4Error,
                                    pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiData->i4_SLongValue);

    VCM_CONF_UNLOCK ();
    return i4RetVal;
}

INT4
FsVcCxtTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VCM_CONF_LOCK ();

    i4RetVal = nmhTestv2FsVcCxtType (pu4Error,
                                     pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue);

    VCM_CONF_UNLOCK ();
    return i4RetVal;
}

INT4
FsVcmConfigTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsVcmConfigTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsVcmIfMappingTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    INT4                i4FsVcmIfIndex;

    VCM_CONF_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsVcmIfMappingTable (&i4FsVcmIfIndex) ==
            SNMP_FAILURE)
        {
            VCM_CONF_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsVcmIfMappingTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4FsVcmIfIndex) == SNMP_FAILURE)
        {
            VCM_CONF_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4FsVcmIfIndex;
    VCM_CONF_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsVcIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VCM_CONF_LOCK ();

    if (nmhValidateIndexInstanceFsVcmIfMappingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VCM_CONF_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsVcId (pMultiIndex->pIndex[0].i4_SLongValue,
                             &(pMultiData->i4_SLongValue));

    VCM_CONF_UNLOCK ();
    return i4RetVal;
}

INT4
FsVcHlPortIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VCM_CONF_LOCK ();

    if (nmhValidateIndexInstanceFsVcmIfMappingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VCM_CONF_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsVcHlPortId (pMultiIndex->pIndex[0].i4_SLongValue,
                                   &(pMultiData->i4_SLongValue));

    VCM_CONF_UNLOCK ();
    return i4RetVal;
}

INT4
FsVcIfRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VCM_CONF_LOCK ();

    if (nmhValidateIndexInstanceFsVcmIfMappingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VCM_CONF_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsVcIfRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue));

    VCM_CONF_UNLOCK ();
    return i4RetVal;
}

INT4
FsVcL2ContextIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VCM_CONF_LOCK ();

    if (nmhValidateIndexInstanceFsVcmIfMappingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        VCM_CONF_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsVcL2ContextId (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue));

    VCM_CONF_UNLOCK ();
    return i4RetVal;

}

INT4
FsVcIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VCM_CONF_LOCK ();

    i4RetVal = nmhSetFsVcId (pMultiIndex->pIndex[0].i4_SLongValue,
                             pMultiData->i4_SLongValue);

    VCM_CONF_UNLOCK ();
    return i4RetVal;
}

INT4
FsVcIfRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VCM_CONF_LOCK ();

    i4RetVal = nmhSetFsVcIfRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->i4_SLongValue);

    VCM_CONF_UNLOCK ();
    return i4RetVal;
}

INT4
FsVcL2ContextIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VCM_CONF_LOCK ();

    i4RetVal = nmhSetFsVcL2ContextId (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->i4_SLongValue);

    VCM_CONF_UNLOCK ();
    return i4RetVal;
}

INT4
FsVcIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VCM_CONF_LOCK ();

    i4RetVal = nmhTestv2FsVcId (pu4Error,
                                pMultiIndex->pIndex[0].i4_SLongValue,
                                pMultiData->i4_SLongValue);

    VCM_CONF_UNLOCK ();
    return i4RetVal;
}

INT4
FsVcIfRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VCM_CONF_LOCK ();

    i4RetVal = nmhTestv2FsVcIfRowStatus (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue);

    VCM_CONF_UNLOCK ();
    return i4RetVal;
}

INT4
FsVcL2ContextIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VCM_CONF_LOCK ();

    i4RetVal = nmhTestv2FsVcL2ContextId (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue);

    VCM_CONF_UNLOCK ();
    return i4RetVal;
}

INT4
FsVcmIfMappingTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsVcmIfMappingTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsVcmL2CxtAndVlanToIPIfaceMapTable (tSnmpIndex * pFirstMultiIndex,
                                                tSnmpIndex * pNextMultiIndex)
{
    VCM_CONF_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsVcmL2CxtAndVlanToIPIfaceMapTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            VCM_CONF_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsVcmL2CxtAndVlanToIPIfaceMapTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            VCM_CONF_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    VCM_CONF_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsVcmL2VcNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VCM_CONF_LOCK ();

    if (nmhValidateIndexInstanceFsVcmL2CxtAndVlanToIPIfaceMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        VCM_CONF_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsVcmL2VcName (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    pMultiData->pOctetStrValue);
    VCM_CONF_UNLOCK ();
    return i4RetVal;

}

INT4
FsVcmIPIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    VCM_CONF_LOCK ();

    if (nmhValidateIndexInstanceFsVcmL2CxtAndVlanToIPIfaceMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        VCM_CONF_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsVcmIPIfIndex (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue));
    VCM_CONF_UNLOCK ();
    return i4RetVal;

}

INT4
GetNextIndexFsVcConfigExtTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsVcConfigExtTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsVcConfigExtTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsVcOwnerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVcConfigExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVcOwner (pMultiIndex->pIndex[0].i4_SLongValue,
                             pMultiData->pOctetStrValue));

}

INT4
FsVcOwnerSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVcOwner (pMultiIndex->pIndex[0].i4_SLongValue,
                             pMultiData->pOctetStrValue));

}

INT4
FsVcOwnerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsVcOwner (pu4Error,
                                pMultiIndex->pIndex[0].i4_SLongValue,
                                pMultiData->pOctetStrValue));

}

INT4
FsVcConfigExtTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsVcConfigExtTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsVcmFreeVcIdTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsVcmFreeVcIdTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsVcmFreeVcIdTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsVcmFreeVcIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVcmFreeVcIdTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[0].i4_SLongValue;

    return SNMP_SUCCESS;

}

INT4
GetNextIndexFsVcmCounterTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsVcmCounterTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsVcmCounterTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsVcCounterRxUnicastGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVcmCounterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVcCounterRxUnicast (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsVcCounterRxMulticastGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVcmCounterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVcCounterRxMulticast (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
FsVcCounterRxBroadcastGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVcmCounterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVcCounterRxBroadcast (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
FsVcCounterStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVcmCounterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVcCounterStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FsVcClearCounterGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVcmCounterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVcClearCounter (pMultiIndex->pIndex[0].i4_SLongValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
FsVcCounterStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVcCounterStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsVcClearCounterSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVcClearCounter (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiData->i4_SLongValue));

}

INT4
FsVcCounterStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsVcCounterStatus (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsVcClearCounterTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2FsVcClearCounter (pu4Error,
                                       pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsVcmCounterTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsVcmCounterTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
