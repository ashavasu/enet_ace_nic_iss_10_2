/* $Id: fsvcmnc.c,v 1.1 2016/06/16 12:16:55 siva Exp $
    ISS Wrapper module
    module VCM-MIB

 */
# include  "lr.h"
# include  "cli.h"
# include  "vcminc.h"
# include  "fssnmp.h"
# include  "fsvcmnc.h"

/********************************************************************
* FUNCTION NcFsVcmTraceOptionSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsVcmTraceOptionSet (
                INT4 i4FsVcmTraceOption )
{

    INT1 i1RetVal;

    i1RetVal = nmhSetFsVcmTraceOption(
                i4FsVcmTraceOption);

    return i1RetVal;


} /* NcFsVcmTraceOptionSet */

/********************************************************************
* FUNCTION NcFsVcmTraceOptionTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsVcmTraceOptionTest (UINT4 *pu4Error,
                INT4 i4FsVcmTraceOption )
{

    INT1 i1RetVal;

    i1RetVal = nmhTestv2FsVcmTraceOption(pu4Error,
                i4FsVcmTraceOption);

    return i1RetVal;


} /* NcFsVcmTraceOptionTest */

/********************************************************************
* FUNCTION NcFsVCNextFreeHlPortIdGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsVCNextFreeHlPortIdGet (
                INT4 i4FsVCId,
                INT4 *pi4FsVCNextFreeHlPortId )
{

    INT1 i1RetVal;
	VCM_CONF_LOCK ();
    if (nmhValidateIndexInstanceFsVcmConfigTable(
                 i4FsVCId) == SNMP_FAILURE)

    {
		VCM_CONF_UNLOCK ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsVCNextFreeHlPortId(
                 i4FsVCId,
                 pi4FsVCNextFreeHlPortId );

	VCM_CONF_UNLOCK ();
    return i1RetVal;


} /* NcFsVCNextFreeHlPortIdGet */

/********************************************************************
* FUNCTION NcFsVCMacAddressGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsVCMacAddressGet (
                INT4 i4FsVCId,
                UINT1 *pFsVCMacAddress )
{

    INT1 i1RetVal;
	UINT1 au1pFsVCMacAddress[MAC_ADDR_LEN];
    memset(au1pFsVCMacAddress, '\0', 21);

	VCM_CONF_LOCK ();
    if (nmhValidateIndexInstanceFsVcmConfigTable(
                 i4FsVCId) == SNMP_FAILURE)

    {
		VCM_CONF_UNLOCK ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsVCMacAddress(
                 i4FsVCId,
                 &au1pFsVCMacAddress );

	CliMacToDotStr(au1pFsVCMacAddress, pFsVCMacAddress);

	VCM_CONF_UNLOCK ();
    return i1RetVal;


} /* NcFsVCMacAddressGet */

/********************************************************************
* FUNCTION NcFsVcAliasSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsVcAliasSet (
                INT4 i4FsVCId,
                UINT1 *pFsVcAlias )
{

    INT1 i1RetVal;
    tSNMP_OCTET_STRING_TYPE FsVcAlias;
    MEMSET (&FsVcAlias, 0,  sizeof (FsVcAlias));

    FsVcAlias.i4_Length = (INT4) STRLEN (pFsVcAlias);
    FsVcAlias.pu1_OctetList = pFsVcAlias;

	VCM_CONF_LOCK ();
    i1RetVal = nmhSetFsVcAlias(
                 i4FsVCId,
                &FsVcAlias);

	VCM_CONF_UNLOCK ();
    return i1RetVal;


} /* NcFsVcAliasSet */

/********************************************************************
* FUNCTION NcFsVcAliasTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsVcAliasTest (UINT4 *pu4Error,
                INT4 i4FsVCId,
                UINT1 *pFsVcAlias )
{

    INT1 i1RetVal;
    tSNMP_OCTET_STRING_TYPE FsVcAlias;
    MEMSET (&FsVcAlias, 0,  sizeof (FsVcAlias));

    FsVcAlias.i4_Length = (INT4) STRLEN (pFsVcAlias);
    FsVcAlias.pu1_OctetList = pFsVcAlias;

	VCM_CONF_LOCK ();
    i1RetVal = nmhTestv2FsVcAlias(pu4Error,
                 i4FsVCId,
                &FsVcAlias);

	VCM_CONF_UNLOCK ();
    return i1RetVal;


} /* NcFsVcAliasTest */

/********************************************************************
* FUNCTION NcFsVcCxtTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsVcCxtTypeSet (
                INT4 i4FsVCId,
                INT4 i4FsVcCxtType )
{

    INT1 i1RetVal;

	VCM_CONF_LOCK ();
    i1RetVal = nmhSetFsVcCxtType(
                 i4FsVCId,
                i4FsVcCxtType);

	VCM_CONF_UNLOCK ();
    return i1RetVal;


} /* NcFsVcCxtTypeSet */

/********************************************************************
* FUNCTION NcFsVcCxtTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsVcCxtTypeTest (UINT4 *pu4Error,
                INT4 i4FsVCId,
                INT4 i4FsVcCxtType )
{

    INT1 i1RetVal;

	VCM_CONF_LOCK ();
    i1RetVal = nmhTestv2FsVcCxtType(pu4Error,
                 i4FsVCId,
                i4FsVcCxtType);

	VCM_CONF_UNLOCK ();
    return i1RetVal;


} /* NcFsVcCxtTypeTest */

/********************************************************************
* FUNCTION NcFsVCStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsVCStatusSet (
                INT4 i4FsVCId,
                INT4 i4FsVCStatus )
{

    INT1 i1RetVal;

	VCM_CONF_LOCK ();
    i1RetVal = nmhSetFsVCStatus(
                 i4FsVCId,
                i4FsVCStatus);

	VCM_CONF_UNLOCK ();
    return i1RetVal;


} /* NcFsVCStatusSet */

/********************************************************************
* FUNCTION NcFsVCStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsVCStatusTest (UINT4 *pu4Error,
                INT4 i4FsVCId,
                INT4 i4FsVCStatus )
{

    INT1 i1RetVal;

	VCM_CONF_LOCK ();
    i1RetVal = nmhTestv2FsVCStatus(pu4Error,
                 i4FsVCId,
                i4FsVCStatus);

	VCM_CONF_UNLOCK ();
    return i1RetVal;


} /* NcFsVCStatusTest */

/********************************************************************
* FUNCTION NcFsVRMacAddressGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsVRMacAddressGet (
                INT4 i4FsVCId,
                UINT1 *pFsVRMacAddress )
{

    INT1 i1RetVal;
	unsigned char au1FsVRMacAddress[MAC_ADDR_LEN];
    memset(au1FsVRMacAddress, '\0', 21);


	VCM_CONF_LOCK ();
    if (nmhValidateIndexInstanceFsVcmConfigTable(
                 i4FsVCId) == SNMP_FAILURE)

    {
		VCM_CONF_UNLOCK ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsVRMacAddress(
                 i4FsVCId,
                 &au1FsVRMacAddress );

	CliMacToDotStr(au1FsVRMacAddress,pFsVRMacAddress);
	VCM_CONF_UNLOCK ();
    return i1RetVal;


} /* NcFsVRMacAddressGet */

/********************************************************************
* FUNCTION NcFsVcOwnerSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsVcOwnerSet (
                INT4 i4FsVCId,
                UINT1 *pFsVcOwner )
{

    INT1 i1RetVal;
    tSNMP_OCTET_STRING_TYPE FsVcOwner;
    MEMSET (&FsVcOwner, 0,  sizeof (FsVcOwner));

    FsVcOwner.i4_Length = (INT4) STRLEN (pFsVcOwner);
    FsVcOwner.pu1_OctetList = pFsVcOwner;

	VCM_CONF_LOCK ();
    i1RetVal = nmhSetFsVcOwner(
                 i4FsVCId,
                &FsVcOwner);

	VCM_CONF_UNLOCK ();
    return i1RetVal;


} /* NcFsVcOwnerSet */

/********************************************************************
* FUNCTION NcFsVcOwnerTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsVcOwnerTest (UINT4 *pu4Error,
                INT4 i4FsVCId,
                UINT1 *pFsVcOwner )
{

    INT1 i1RetVal;
    tSNMP_OCTET_STRING_TYPE FsVcOwner;
    MEMSET (&FsVcOwner, 0,  sizeof (FsVcOwner));

    FsVcOwner.i4_Length = (INT4) STRLEN (pFsVcOwner);
    FsVcOwner.pu1_OctetList = pFsVcOwner;

	VCM_CONF_LOCK ();
    i1RetVal = nmhTestv2FsVcOwner(pu4Error,
                 i4FsVCId,
                &FsVcOwner);

	VCM_CONF_UNLOCK ();
    return i1RetVal;


} /* NcFsVcOwnerTest */

/********************************************************************
* FUNCTION NcFsVcIdSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsVcIdSet (
                INT4 i4FsVcmIfIndex,
                INT4 i4FsVcId )
{

    INT1 i1RetVal;

	VCM_CONF_LOCK ();
    i1RetVal = nmhSetFsVcId(
                 i4FsVcmIfIndex,
                i4FsVcId);

	VCM_CONF_UNLOCK ();
    return i1RetVal;


} /* NcFsVcIdSet */

/********************************************************************
* FUNCTION NcFsVcIdTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsVcIdTest (UINT4 *pu4Error,
                INT4 i4FsVcmIfIndex,
                INT4 i4FsVcId )
{

    INT1 i1RetVal;

	VCM_CONF_LOCK ();
    i1RetVal = nmhTestv2FsVcId(pu4Error,
                 i4FsVcmIfIndex,
                i4FsVcId);

	VCM_CONF_UNLOCK ();
    return i1RetVal;


} /* NcFsVcIdTest */

/********************************************************************
* FUNCTION NcFsVcHlPortIdGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsVcHlPortIdGet (
                INT4 i4FsVcmIfIndex,
                INT4 *pi4FsVcHlPortId )
{

    INT1 i1RetVal;

	VCM_CONF_LOCK ();
    if (nmhValidateIndexInstanceFsVcmIfMappingTable(
                 i4FsVcmIfIndex) == SNMP_FAILURE)

    {
		VCM_CONF_UNLOCK ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsVcHlPortId(
                 i4FsVcmIfIndex,
                 pi4FsVcHlPortId );

	VCM_CONF_UNLOCK ();
    return i1RetVal;


} /* NcFsVcHlPortIdGet */

/********************************************************************
* FUNCTION NcFsVcL2ContextIdSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsVcL2ContextIdSet (
                INT4 i4FsVcmIfIndex,
                INT4 i4FsVcL2ContextId )
{

    INT1 i1RetVal;

	VCM_CONF_LOCK ();
    i1RetVal = nmhSetFsVcL2ContextId(
                 i4FsVcmIfIndex,
                i4FsVcL2ContextId);

	VCM_CONF_UNLOCK ();
    return i1RetVal;


} /* NcFsVcL2ContextIdSet */

/********************************************************************
* FUNCTION NcFsVcL2ContextIdTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsVcL2ContextIdTest (UINT4 *pu4Error,
                INT4 i4FsVcmIfIndex,
                INT4 i4FsVcL2ContextId )
{

    INT1 i1RetVal;

	VCM_CONF_LOCK ();
    i1RetVal = nmhTestv2FsVcL2ContextId(pu4Error,
                 i4FsVcmIfIndex,
                i4FsVcL2ContextId);

	VCM_CONF_UNLOCK ();
    return i1RetVal;


} /* NcFsVcL2ContextIdTest */

/********************************************************************
* FUNCTION NcFsVcIfRowStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsVcIfRowStatusSet (
                INT4 i4FsVcmIfIndex,
                INT4 i4FsVcIfRowStatus )
{

    INT1 i1RetVal;

	VCM_CONF_LOCK ();
    i1RetVal = nmhSetFsVcIfRowStatus(
                 i4FsVcmIfIndex,
                i4FsVcIfRowStatus);

	VCM_CONF_UNLOCK ();
    return i1RetVal;


} /* NcFsVcIfRowStatusSet */

/********************************************************************
* FUNCTION NcFsVcIfRowStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsVcIfRowStatusTest (UINT4 *pu4Error,
                INT4 i4FsVcmIfIndex,
                INT4 i4FsVcIfRowStatus )
{

    INT1 i1RetVal;

	VCM_CONF_LOCK ();
    i1RetVal = nmhTestv2FsVcIfRowStatus(pu4Error,
                 i4FsVcmIfIndex,
                i4FsVcIfRowStatus);

	VCM_CONF_UNLOCK ();
    return i1RetVal;


} /* NcFsVcIfRowStatusTest */

/********************************************************************
* FUNCTION NcFsVcmL2VcNameGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsVcmL2VcNameGet (
                INT4 i4FsVcmL2VcId,
                INT4 i4FsVcmVlanId,
                UINT1 *pFsVcmL2VcName )
{

    INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE FsVcmL2VcName;
    MEMSET (&FsVcmL2VcName, 0,  sizeof (FsVcmL2VcName));
	MEMSET (pFsVcmL2VcName, '\0', CFA_MAX_DESCR_LENGTH);

    FsVcmL2VcName.pu1_OctetList = pFsVcmL2VcName;


	VCM_CONF_LOCK ();

    if (nmhValidateIndexInstanceFsVcmL2CxtAndVlanToIPIfaceMapTable(
                 i4FsVcmL2VcId,
                 i4FsVcmVlanId) == SNMP_FAILURE)

    {
		VCM_CONF_UNLOCK ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsVcmL2VcName(
                 i4FsVcmL2VcId,
                 i4FsVcmVlanId,
                 &FsVcmL2VcName );

	VCM_CONF_UNLOCK ();
    return i1RetVal;


} /* NcFsVcmL2VcNameGet */

/********************************************************************
* FUNCTION NcFsVcmIPIfIndexGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcFsVcmIPIfIndexGet (
                INT4 i4FsVcmL2VcId,
                INT4 i4FsVcmVlanId,
                INT4 *pi4FsVcmIPIfIndex )
{

    INT1 i1RetVal;

	VCM_CONF_LOCK ();
    if (nmhValidateIndexInstanceFsVcmL2CxtAndVlanToIPIfaceMapTable(
                 i4FsVcmL2VcId,
                 i4FsVcmVlanId) == SNMP_FAILURE)

    {
		VCM_CONF_UNLOCK ();
        return SNMP_FAILURE;
    }

    i1RetVal = nmhGetFsVcmIPIfIndex(
                 i4FsVcmL2VcId,
                 i4FsVcmVlanId,
                 pi4FsVcmIPIfIndex );
	VCM_CONF_UNLOCK ();

    return i1RetVal;


} /* NcFsVcmIPIfIndexGet */

/* END i_VCM_MIB.c */
