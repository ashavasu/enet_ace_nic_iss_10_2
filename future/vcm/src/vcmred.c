/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: vcmred.c,v 1.41 2016/05/04 11:38:15 siva Exp $
 *
 * Description: This file contains the functions to support
 *              Redundancy for VCM module.
 *
 *******************************************************************/
#ifndef _VCMRED_C
#define _VCMRED_C
#include "vcminc.h"
#include "eoam.h"

tVcmRedGlobalInfo   gVcmRedGlobalInfo;
tVcmRedStates       gVcmPrevNodeState = VCM_RED_IDLE;
extern INT4         MbsmRcvPktFromRm (UINT1, tRmMsg *, UINT2);
/*****************************************************************************/
/* Function Name      : VcmRedRmInit                                         */
/*                                                                           */
/* Description        : Initialise the RED Global variables pertaining to VCM*/
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gVcmRedGlobalInfo                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gVcmRedGlobalInfo                                    */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS                                          */
/*****************************************************************************/
INT4
VcmRedRmInit (VOID)
{
    VCM_MEMSET (&gVcmRedGlobalInfo, VCM_INIT_VAL, sizeof (tVcmRedGlobalInfo));

    gVcmRedGlobalInfo.u4BulkUpdNextPort = VCM_MIN_NUM_PORTS;

    VCM_BULK_REQ_RECD () = VCM_FALSE;

    VCM_NODE_STATUS () = VCM_RED_IDLE;

    gVcmPrevNodeState = VCM_RED_IDLE;

    VCM_NUM_STANDBY_NODES () = 0;

    VCM_SISP_RED_STATIC_SYNC_RCVD () = VCM_FALSE;

    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmRedRegisterWithRM                                 */
/*                                                                           */
/* Description        : Creates a queue for receiving messages from RM and   */
/*                      registers VCM with RM.                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if registration is success then VCM_SUCCESS          */
/*                      Otherwise VCM_FAILURE                                */
/*****************************************************************************/
INT4
VcmRedRegisterWithRM (VOID)
{
    tRmRegParams        RmRegParams;

    RmRegParams.u4EntId = RM_VCM_APP_ID;
    RmRegParams.pFnRcvPkt = VcmHandleUpdateEvents;

    if (VcmRmRegisterProtocols (&RmRegParams) == RM_FAILURE)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC | VCM_CONTROL_PATH_TRC |
                 VCM_OS_RESOURCE_TRC,
                 "VCMRED: Vcm Registration with RM FAILED\n");
        return VCM_FAILURE;
    }
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmHandleUpdateEvents()                              */
/* Description        : RM processing Entry point                            */
/* Input(s)           : u1Event - The Event from RM to be handled            */
/*                      pData - The RM message                               */
/*                      u2DataLen - Length of Message                        */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmHandleUpdateEvents (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tVcmQMsg           *pMsg = NULL;

    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        /* Message not present */
        return RM_FAILURE;
    }

    if (VCM_IS_INITIALISED () != VCM_TRUE)
    {
        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            VcmRmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        return RM_FAILURE;
    }

    VCM_ALLOC_CTRLQ_MEM_BLOCK (pMsg, tVcmQMsg);

    if (pMsg == NULL)
    {
        VCM_TRC_ARG1 (VCM_INIT_SHUT_TRC | VCM_BUFFER_TRC | VCM_ALL_FAILURE_TRC,
                      "VCMRED: Ctrl Mesg ALLOC_MEM_BLOCK FAILED for event\n",
                      u1Event);

        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            VcmRmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        return RM_FAILURE;
    }

    VCM_MEMSET (pMsg, VCM_INIT_VAL, sizeof (tVcmQMsg));

    VCM_QMSG_TYPE (pMsg) = VCM_RM_QMSG;

    pMsg->RmMsg.pFrame = pData;

    pMsg->RmMsg.u2Length = u2DataLen;

    pMsg->RmMsg.u1Event = u1Event;

    if (VCM_SEND_TO_QUEUE (VCM_CTRLQ_ID, (UINT1 *) &pMsg,
                           OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        VCM_TRC (VCM_INIT_SHUT_TRC | VCM_ALL_FAILURE_TRC,
                 "VCMRED: Rm Message enqueue FAILED\n");

        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            VcmRmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        VCM_RELEASE_CTRLQ_MEM_BLOCK (pMsg);

        return RM_FAILURE;
    }

    if (VCM_SEND_EVENT (VCM_TASK_ID, VCM_MSG_EVENT) != OSIX_SUCCESS)
    {
        VCM_TRC (VCM_INIT_SHUT_TRC | VCM_ALL_FAILURE_TRC,
                 "VCMRED: Rm Event send FAILED\n");

        return RM_FAILURE;
    }

    return RM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmProcessRmEvent                                    */
/*                                                                           */
/* Description        : This function is invoked to process the following    */
/*                      from RM module:-                                     */
/*                           - RM events and                                 */
/*                           - update messages.                              */
/*                      This function interprets the RM events and calls the */
/*                      corresponding module functions to process those      */
/*                      events.                                              */
/*                                                                           */
/* Input(s)           : pMsg - Pointer to the  input buffer.                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VcmProcessRmEvent (tVcmQMsg * pMsg)
{
    tRmNodeInfo        *pData = NULL;
    tRmProtoEvt         ProtoEvt;
    tRmProtoAck         ProtoAck;
    UINT4               u4SeqNum = 0;
#ifdef NPAPI_WANTED
    tContextMapInfo     ContextMapInfo;
    UINT4               u4IfIndex = 0;
    UINT4               u4MaxIfaces = 0;
    UINT2               u2ProtocolId = 0;

    VCM_MEMSET (&ContextMapInfo, VCM_INIT_VAL, sizeof (tContextMapInfo));
#endif
    ProtoEvt.u4AppId = RM_VCM_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    switch (pMsg->RmMsg.u1Event)
    {
        case GO_ACTIVE:

            if (VCM_NODE_STATUS () == VCM_RED_ACTIVE)
            {
                break;
            }

            VCM_NUM_STANDBY_NODES () = VcmRmGetStandbyNodeCount ();
            gVcmPrevNodeState = VCM_NODE_STATUS ();

            if (gVcmPrevNodeState == VCM_RED_STANDBY)
            {
                VcmRedClearAllSyncUpData ();
                ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
            }
            else
            {
                ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
            }

            VCM_NODE_STATUS () = VCM_RED_ACTIVE;
#ifdef NPAPI_WANTED
            if (gVcmPrevNodeState != VCM_RED_STANDBY)
            {
                if (SYS_DEF_MAX_NUM_CONTEXTS == 1)
                {
                    u4MaxIfaces = SYS_DEF_MAX_PORTS_PER_CONTEXT;
                }
                else
                {
                    u4MaxIfaces = SYS_DEF_MAX_PHYSICAL_INTERFACES;
                }
                if (VcmGetSystemModeExt (u2ProtocolId) == VCM_SI_MODE)
                {
                    for (u4IfIndex = 1; u4IfIndex <= u4MaxIfaces; u4IfIndex++)
                    {
                        if (VcmFsVcmHwMapPortToContext (0, u4IfIndex) ==
                            FNP_FAILURE)
                        {
                            continue;
                        }
                        ContextMapInfo.u2LocalPortId = u4IfIndex;

                        if (VcmFsVcmHwMapIfIndexToBrgPort
                            (0, u4IfIndex, ContextMapInfo) == FNP_FAILURE)
                        {
                            continue;
                        }

                    }
                }
            }
#endif

            /* In Redundancy force-switchover case, there are chances of 
             * Bulk Request may come from Standby before GO_ACTIVE */
            if ((VCM_BULK_REQ_RECD () == VCM_TRUE) &&
                (RmGetStaticConfigStatus () == RM_STATIC_CONFIG_RESTORED) &&
                (VCM_NUM_STANDBY_NODES () > 0))
            {
                VCM_BULK_REQ_RECD () = VCM_FALSE;
                gVcmRedGlobalInfo.u4BulkUpdNextPort = VCM_MIN_NUM_PORTS;
                VcmRedHandleBulkUpdateEvent ();
            }

            VcmRmHandleProtocolEvent (&ProtoEvt);

            break;

        case GO_STANDBY:

            if (VCM_NODE_STATUS () == VCM_RED_STANDBY)
            {
                break;
            }

            VCM_BULK_REQ_RECD () = VCM_FALSE;
            gVcmPrevNodeState = VCM_NODE_STATUS ();
            VCM_NODE_STATUS () = VCM_RED_STANDBY;
            if (gVcmPrevNodeState == VCM_RED_IDLE)
            {
                VCM_NUM_STANDBY_NODES () = 0;
            }
            else if (gVcmPrevNodeState == VCM_RED_ACTIVE)
            {
                VcmRedClearAllSyncUpData ();
            }

            ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
            VcmRmHandleProtocolEvent (&ProtoEvt);
            break;

        case RM_INIT_HW_AUDIT:

            if (VCM_NODE_STATUS () == VCM_RED_ACTIVE)
            {
                VcmRedInitHardwareAudit ();
            }

            break;

        case L2_INITIATE_BULK_UPDATES:
            VcmRedSendBulkRequest ();
            break;

        case RM_STANDBY_UP:
            pData = (tRmNodeInfo *) pMsg->RmMsg.pFrame;
            VCM_NUM_STANDBY_NODES () = pData->u1NumStandby;
            VcmRmReleaseMemoryForMsg ((UINT1 *) pData);
            if ((VCM_BULK_REQ_RECD () == VCM_TRUE) &&
                (RmGetStaticConfigStatus () == RM_STATIC_CONFIG_RESTORED))
            {
                VCM_BULK_REQ_RECD () = VCM_FALSE;
                /* Bulk request msg is recieved before RM_STANDBY_UP
                 * event.So we are sending bulk updates now.
                 */
                gVcmRedGlobalInfo.u4BulkUpdNextPort = VCM_MIN_NUM_PORTS;
                VcmRedHandleBulkUpdateEvent ();
            }
            break;

        case RM_CONFIG_RESTORE_COMPLETE:
            if ((VCM_BULK_REQ_RECD () == VCM_TRUE) &&
                (VCM_NUM_STANDBY_NODES () > 0))
            {
                VCM_BULK_REQ_RECD () = VCM_FALSE;
                /* Bulk request msg is recieved before RM_STANDBY_UP
                 * event.So we are sending bulk updates now.
                 */
                gVcmRedGlobalInfo.u4BulkUpdNextPort = VCM_MIN_NUM_PORTS;
                VcmRedHandleBulkUpdateEvent ();
            }
            break;

        case RM_STANDBY_DOWN:
            pData = (tRmNodeInfo *) pMsg->RmMsg.pFrame;
            VCM_NUM_STANDBY_NODES () = pData->u1NumStandby;
            VcmRmReleaseMemoryForMsg ((UINT1 *) pData);
            break;

        case RM_MESSAGE:

            /* Read the sequence number from the RM Header */
            RM_PKT_GET_SEQNUM (pMsg->RmMsg.pFrame, &u4SeqNum);
            /* Remove the RM Header */
            RM_STRIP_OFF_RM_HDR (pMsg->RmMsg.pFrame, pMsg->RmMsg.u2Length);

            ProtoAck.u4AppId = RM_VCM_APP_ID;
            ProtoAck.u4SeqNumber = u4SeqNum;

            VcmRedHandleSyncUpMessage (pMsg);
            /* Processing of message is over, hence free the RM message. */
            RM_FREE ((tRmMsg *) (pMsg->RmMsg.pFrame));

            /* Sending ACK to RM */
            RmApiSendProtoAckToRM (&ProtoAck);
            break;

        case RM_DYNAMIC_SYNCH_AUDIT:
            VcmRedHandleDynSyncAudit ();
            break;
        default:
            VCM_TRC (VCM_ALL_FAILURE_TRC | VCM_CONTROL_PATH_TRC,
                     "VCM Receieved Unknown event from RM\n");
            break;
    }
}

/*****************************************************************************/
/* Function Name      : VcmRedHandleSyncUpMessage                            */
/*                                                                           */
/* Description        : This function is invoked whenever VCM module         */
/*                      receives a sync up request Bulk as well as update    */
/*                                                                           */
/* Input(s)           : pMsg - RM Message                                    */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VcmRedHandleSyncUpMessage (tVcmQMsg * pMsg)
{
    VOID               *pData = NULL;
    UINT4               u4Offset = VCM_INIT_VAL;
    UINT1               u1MessageType = 0;
    UINT2               u2Len;
    UINT4               u4PortIfIndex;
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_VCM_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    pData = (VOID *) pMsg->RmMsg.pFrame;

    while (1)
    {
        if (u4Offset >= pMsg->RmMsg.u2Length)
            break;

        VCM_RM_GET_1_BYTE ((tRmMsg *) pData, &u4Offset, u1MessageType);
        /* Length of the Individual Message */
        VCM_RM_GET_2_BYTE ((tRmMsg *) pData, &u4Offset, u2Len);
        /* While Loop to get all messages and store them */
        /* Decode Message */

        if (u1MessageType == VCM_RED_BULK_REQ)
        {
            VCM_TRC (VCM_TRC_FLAG, "VCMRED: Rcvd Bulk Req Message \n");

            if(IssuGetMaintModeOperation() == OSIX_TRUE)
            {
                /* During ISSU Maintenance Mode load version,Peer down 
                 * wan not notified to higher layer protocols. Hence 
                 * when standby is coming up in ISSU Maintenance Mode,  
                 * verifying Peer node status from RM Database.
                 * */
                if ((RmGetIssuPeerNodeCount () == 0) ||
                    (RmGetStaticConfigStatus () != RM_STATIC_CONFIG_RESTORED)) 
                {
                    /* This is a special case, where bulk request msg from
                     * standby is coming before RM_STANDBY_UP. So no need to
                     * process the bulk request now. Bulk updates will be send
                     * on RM_STANDBY_UP event.
                     */
                    VCM_BULK_REQ_RECD () = VCM_TRUE;
                    VCM_TRC (VCM_TRC_FLAG,"VCMRED: Setting VCM_BULK_REQ_RECD"
                            " Flag during ISSU Maintenance Mode \n");
                    return;
                }
            }
            if ((VCM_IS_STANDBY_UP () == VCM_FALSE) ||
                (RmGetStaticConfigStatus () != RM_STATIC_CONFIG_RESTORED))
            {
                /* This is a special case, where bulk request msg from
                 * standby is coming before RM_STANDBY_UP. So no need to
                 * process the bulk request now. Bulk updates will be send
                 * on RM_STANDBY_UP event.
                 */
                VCM_BULK_REQ_RECD () = VCM_TRUE;
                VCM_TRC (VCM_TRC_FLAG,"VCMRED: Setting VCM_BULK_REQ_RECD Flag \n");
                return;
            }

            VCM_BULK_REQ_RECD () = VCM_FALSE;
            /* On recieving VCM_RED_BULK_REQ, Bulk updation process
             * should be restarted.
             */
            gVcmRedGlobalInfo.u4BulkUpdNextPort = VCM_MIN_NUM_PORTS;
            VcmRedHandleBulkUpdateEvent ();

            return;
        }

        if (VCM_NODE_STATUS () != VCM_RED_STANDBY)
        {
            return;
        }

        switch (u1MessageType)
        {
            case VCM_RED_IFMAP:
                VcmRedStoreIfMap (pData, &u4Offset, u2Len);
                break;

            case VCM_RED_CLEAR_SYNCUP_DATA:
                VCM_RM_GET_4_BYTE ((tRmMsg *) pData, &u4Offset, u4PortIfIndex);

                VcmRedClearSyncUpData (u4PortIfIndex);

                break;

            case VCM_RED_BULK_UPD_TAIL_MSG:
                /* In case of IDLE-STANDBY transition, VCM on completing
                 * its syncup will give GO_STANDBY to MBSM Module. 
                 * But in case of ACTIVE-STANDBY transition, VCM will not 
                 * do anything. Is just save the synced up infm*/
                /* IDLE-STANDBY transition */
                if ((gVcmPrevNodeState == VCM_RED_IDLE) &&
                    (VCM_NODE_STATUS () == VCM_RED_STANDBY))
                {
#ifdef MBSM_WANTED
                    MbsmRcvPktFromRm (GO_STANDBY, 0, 0);
#else
                    RmSendEventToAppln (RM_VCM_APP_ID);
#endif
                }

                /* ACTIVE - STANDBY transition */
                else
                {
                    VCM_NUM_STANDBY_NODES () = 0;
                }
                ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;
                VcmRmHandleProtocolEvent (&ProtoEvt);
                break;

            case VCM_SISP_SYNC_MSG:

                if (VCM_NODE_STATUS () == VCM_RED_STANDBY)
                {
                    VcmSispRedRecvSispSyncMsg (pData, &u4Offset, u2Len);
                }

                break;

            default:
                break;
        }
    }
}

/*****************************************************************************/
/* Function Name      : VcmRedSendBulkRequest                                */
/*                                                                           */
/* Description        : This function is invoked whenever Bulk Request       */
/*                       needs to be send                                    */
/*                                                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VcmRedSendBulkRequest (VOID)
{
    /* Initialte a Bulk Request to the ACTIVE */
    VcmRedSendSyncMessages (0, 0, VCM_RED_BULK_REQ);
}

/*****************************************************************************/
/* Function Name      : VcmRedHandleBulkRequest                              */
/*                                                                           */
/* Description        : This function is invoked whenever VCM module         */
/*                      requests for Bulk Update                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : Constructs Bulk Update                               */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VcmRedHandleBulkRequest (VOID)
{
    tVcmIfMapEntry     *pIfMapEntry = NULL;
    tRmMsg             *pBuf = NULL;
    tVcmIfMapEntry      Dummy;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2LocalPortId;
    UINT4               u4Offset = 0;
    UINT4               u4BulkUpdPortCnt = VCM_RED_NO_OF_PORTS_PER_SUB_UPDATE;
    UINT1               u1MessageType = VCM_RED_IFMAP;
    UINT2               u2Len = VCM_RED_IFMAP_MSG_LEN;

    ProtoEvt.u4AppId = RM_VCM_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    VCM_LOCK ();

    if (gVcmRedGlobalInfo.u4BulkUpdNextPort == VCM_MIN_NUM_PORTS)
    {
        pIfMapEntry = VcmGetFirstIfMap ();
    }
    else
    {
        Dummy.u4IfIndex = gVcmRedGlobalInfo.u4BulkUpdNextPort;
        pIfMapEntry = VcmFindIfMapEntry (&Dummy);
    }

    if (pIfMapEntry == NULL)
    {
        VCM_UNLOCK ();

        /* VCM completes it's sync up during standby boot-up and this
         * needs to be informed to RMGR */
        VcmRmSetBulkUpdatesStatus (RM_VCM_APP_ID);

        /* Send the tail msg to indicate the completion of Bulk
         * update process*/
        VcmRedSendBulkUpdateTailMsg ();

        VCM_TRC (VCM_ALL_FAILURE_TRC, "VCM isn't initialized or no mapping "
                 "is found\n");
        return;
    }
    
    /* Buffer Not available */
    /* in Alloc macro, RM lock will be taken, so release VCM lock
     * till this allocation is over
     */ 
    VCM_UNLOCK();
    
    if ((pBuf = (VOID *) RM_ALLOC_TX_BUF (VCM_MAX_RM_BUF_SIZE)) == NULL)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC, "RM Allocation Failed\n");
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        VcmRmHandleProtocolEvent (&ProtoEvt);
        return;
    }
    VCM_LOCK();

    for (; pIfMapEntry != NULL && u4BulkUpdPortCnt > 0;)
    {
        /* Update u4BulkUpdNextPort, to resume the next sub bulk update
         * from where the previous sub bulk update left it out */

        u4BulkUpdPortCnt--;

        u2LocalPortId = VCM_IFMAP_HLPORTID (pIfMapEntry);

        /* Place it in the buffer */
        if (u2LocalPortId > 0)
        {
            VCM_RM_PUT_1_BYTE (pBuf, &u4Offset, u1MessageType);
            VCM_RM_PUT_2_BYTE (pBuf, &u4Offset, u2Len);
            VCM_RM_PUT_4_BYTE (pBuf, &u4Offset, pIfMapEntry->u4IfIndex);
            VCM_RM_PUT_2_BYTE (pBuf, &u4Offset, u2LocalPortId);
        }

        Dummy.u4IfIndex = pIfMapEntry->u4IfIndex;

        if (NULL != (pIfMapEntry = VcmGetNextIfMap (&Dummy)))
        {
            gVcmRedGlobalInfo.u4BulkUpdNextPort = pIfMapEntry->u4IfIndex;
        }
        else
        {
            gVcmRedGlobalInfo.u4BulkUpdNextPort = VCM_MIN_NUM_PORTS;
        }

        if ((VCM_MAX_RM_BUF_SIZE - u4Offset) < u2Len)
        {
            break;
        }
    }
    VCM_UNLOCK ();

    /* Send and Allocate */
    if (pBuf != NULL && u4Offset > 0)
    {
        if (VcmRedSendUpdateToRM ((tRmMsg *) pBuf, (UINT2) u4Offset) ==
            VCM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            VcmRmHandleProtocolEvent (&ProtoEvt);
        }
    }
    else if (pBuf != NULL)
    {
        RM_FREE (pBuf);
    }

    /* Still some ports needs to be synced up */
    if (gVcmRedGlobalInfo.u4BulkUpdNextPort != VCM_MIN_NUM_PORTS)
    {
        if (VCM_SEND_EVENT (VCM_TASK_ID, VCM_RED_BULK_UPD_EVENT)
            != OSIX_SUCCESS)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            VcmRmHandleProtocolEvent (&ProtoEvt);
            VCM_TRC (VCM_ALL_FAILURE_TRC, "Send Event to VCM Task Failed\n");
        }
    }
    else
    {
        /* VCM completes it's sync up during standby boot-up and this
         * needs to be informed to RMGR */
        VcmRmSetBulkUpdatesStatus (RM_VCM_APP_ID);

        /* Send the tail msg to indicate the completion of Bulk
         * update process.*/
        VcmRedSendBulkUpdateTailMsg ();
    }
}

/*****************************************************************************/
/* Function Name      : VcmRedSendSyncMessages                               */
/*                                                                           */
/* Description        : This function is invoked to  Alloc and Send Bulk     */
/*                      Sync Up Messages                                     */
/*                                                                           */
/* Input(s)           : u4PortIfIndex - Physical Port Index                  */
/*                      u2LocalPortId - Context based local Port Number      */
/*                      u1MessageType - Message Type                         */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS/VCM_FAILURE.                             */
/*****************************************************************************/
INT4
VcmRedSendSyncMessages (UINT4 u4PortIfIndex, UINT2 u2LocalPortId,
                        UINT1 u1MessageType)
{
    UINT4               u4Offset = VCM_INIT_VAL;
    UINT2               u2Len = 0;
    VOID               *pBuf = NULL;
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_VCM_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (u1MessageType == VCM_RED_BULK_REQ)
    {
        u2Len = VCM_RED_BULK_REQ_MSG_LEN;
        if ((pBuf = RM_ALLOC_TX_BUF (u2Len)) == NULL)
        {
            VCM_TRC (VCM_ALL_FAILURE_TRC, "RM Allocation Failed\n");
            ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
            VcmRmHandleProtocolEvent (&ProtoEvt);
            return VCM_FAILURE;
        }
        VCM_RM_PUT_1_BYTE (pBuf, &u4Offset, u1MessageType);
        VCM_RM_PUT_2_BYTE (pBuf, &u4Offset, u2Len);
    }
    else
    {
        /* else all messages are from Active to Standby */
        if (VCM_NUM_STANDBY_NODES () == 0)
        {
            VCM_TRC (VCM_CONTROL_PATH_TRC, "None of the Peer Nodes  are up \
                     No Need to send Dynamic Updates\n");
            return VCM_SUCCESS;
        }

        switch (u1MessageType)
        {
            case VCM_RED_IFMAP:
                u2Len = VCM_RED_IFMAP_MSG_LEN;
                if ((pBuf = RM_ALLOC_TX_BUF (u2Len)) == NULL)
                {
                    VCM_TRC (VCM_ALL_FAILURE_TRC, "RM Allocation Failed\n");
                    ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                    VcmRmHandleProtocolEvent (&ProtoEvt);
                    return VCM_FAILURE;
                }
                VCM_RM_PUT_1_BYTE (pBuf, &u4Offset, u1MessageType);
                VCM_RM_PUT_2_BYTE (pBuf, &u4Offset, u2Len);
                VCM_RM_PUT_4_BYTE (pBuf, &u4Offset, u4PortIfIndex);
                VCM_RM_PUT_2_BYTE (pBuf, &u4Offset, u2LocalPortId);
                break;

            case VCM_RED_CLEAR_SYNCUP_DATA:
                u2Len = VCM_RED_CLEAR_SYNCUP_DATA_MSG_LEN;
                if ((pBuf = RM_ALLOC_TX_BUF (u2Len)) == NULL)
                {
                    VCM_TRC (VCM_ALL_FAILURE_TRC, "RM Allocation Failed\n");
                    ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                    VcmRmHandleProtocolEvent (&ProtoEvt);
                    return VCM_FAILURE;
                }
                VCM_RM_PUT_1_BYTE (pBuf, &u4Offset, u1MessageType);
                VCM_RM_PUT_2_BYTE (pBuf, &u4Offset, u2Len);
                VCM_RM_PUT_4_BYTE (pBuf, &u4Offset, u4PortIfIndex);
                break;

            default:
                VCM_ASSERT ();
                return VCM_FAILURE;
        }
    }

    if (VcmRedSendUpdateToRM ((tRmMsg *) pBuf, u2Len) == VCM_FAILURE)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        VcmRmHandleProtocolEvent (&ProtoEvt);
    }
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmRedClearSyncUpData                                */
/* Description        : Clears stale Data On Port                            */
/* Input(s)           : u4PortIfIndex - Port If index                        */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : tVcmRedGlobalInfo.                                   */
/* Global Variables                                                          */
/* Modified           : tVcmRedGlobalInfo.                                   */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VcmRedClearSyncUpData (UINT4 u4PortIfIndex)
{
    VCM_RED_SET_LOCAL_PORT_ID (u4PortIfIndex, 0);
}

/*****************************************************************************/
/* Function Name      : VcmRedClearAllSyncUpData                             */
/* Description        : Clears stale Data On all Ports                       */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VcmRedClearAllSyncUpData (VOID)
{
    UINT4               u4PortIfIndex;

    for (u4PortIfIndex = 1; u4PortIfIndex <= BRG_MAX_PHY_PLUS_LOG_PORTS;
         u4PortIfIndex++)
    {
        VcmRedClearSyncUpData (u4PortIfIndex);
    }
    for (u4PortIfIndex = CFA_MIN_SISP_IF_INDEX;
         u4PortIfIndex <= CFA_MAX_SISP_IF_INDEX; u4PortIfIndex++)
    {
        VcmRedClearSyncUpData (u4PortIfIndex);
    }
}

/*****************************************************************************/
/* Function Name      : VcmRedStoreIfMap                                     */
/*                                                                           */
/* Description        : Stores the mapping of each Physical Port to          */
/*                      Local PortId.                                        */
/* Input(s)           : pData - Pointer to RM Data                           */
/*                      pu4Offset - Offset of RM Data                        */
/*                      u2Len     - Length of Data                           */
/* Output(s)          :                                                      */
/* Global Variables                                                          */
/* Referred           : tVcmRedGlobalInfo.                                   */
/* Global Variables                                                          */
/* Modified           : tVcmRedGlobalInfo.                                   */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmRedStoreIfMap (VOID *pData, UINT4 *pu4Offset, UINT2 u2Len)
{
    INT4                i4RowStatus = 0;
    UINT4               u4PortIfIndex = VCM_INIT_VAL;
    UINT4               u4CxtId = 0;
    UINT2               u2LocalPortId = VCM_INIT_VAL;
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_VCM_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
    if (u2Len != VCM_RED_IFMAP_MSG_LEN)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        VcmRmHandleProtocolEvent (&ProtoEvt);
        return VCM_FAILURE;
    }

    VCM_RM_GET_4_BYTE (pData, pu4Offset, u4PortIfIndex);
    VCM_RM_GET_2_BYTE (pData, pu4Offset, u2LocalPortId);
    if (u4PortIfIndex != VCM_INIT_VAL)
    {
        VCM_RED_SET_LOCAL_PORT_ID (u4PortIfIndex, u2LocalPortId);
    }

    /* In Standby node, interface creation should be done only after getting
     * the local port number from the Active*/
    if ((VCM_RED_RM_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_RESTORED)
        && (VcmIsIfMapExist (u4PortIfIndex) == VCM_TRUE))
    {
        if (VcmGetIfMapStatus (u4PortIfIndex, &i4RowStatus) != VCM_SUCCESS)
        {
            return VCM_FAILURE;
        }

        if (i4RowStatus == NOT_READY)
        {
            VcmSetIfMapStatus (u4PortIfIndex, ACTIVE);
            VcmGetIfMapVcId (u4PortIfIndex, &u4CxtId);
            if (VcmCreateInterface (u4PortIfIndex) != VCM_SUCCESS)
            {
                return VCM_FAILURE;
            }
            /* When a new port is mapped to the virtual context, EoamApiNotifyIfCreate
             * and IssCreate Port, is invoked to post message to notify interface creation */
#ifdef EOAM_WANTED
            EoamApiNotifyIfCreate (u4PortIfIndex);
#endif

#ifdef ISS_WANTED
            IssCreatePort ((UINT2) u4PortIfIndex, ISS_ALL_TABLES);
#endif

            VcmIncrementPortCount (u4CxtId);
        }
    }
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmRedSendUpdateToRM                                 */
/*                                                                           */
/* Description        : This function sends Update to RM                     */
/*                                                                           */
/* Input(s)           : pMsg - RM Message                                    */
/*                      u2Len - Length of RM message                         */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS/VCM_FAILURE.                             */
/*****************************************************************************/
INT4
VcmRedSendUpdateToRM (tRmMsg * pMsg, UINT2 u2Len)
{
    /* Call the API provided by RM to send the data to RM */
    if (VcmRmEnqMsgToRmFromAppl
        (pMsg, u2Len, RM_VCM_APP_ID, RM_VCM_APP_ID) == RM_FAILURE)
    {
        VCM_TRC (VCM_TRC_FLAG, "Enq to RM from appl failed\n");
        /* pMsg is reed only in failure case. RM will free the buf
         * in success case. */

        RM_FREE (pMsg);
        return VCM_FAILURE;
    }
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmRedHandleBulkUpdateEvent                          */
/*                                                                           */
/* Description        : It Handles the bulk update event. This event is used */
/*                      to start the next sub bulk update. So                */
/*                      VcmRedHandleBulkRequest is triggered.                */
/*                                                                           */
/* Input(s)           :                                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VcmRedHandleBulkUpdateEvent (VOID)
{
    if (VCM_NODE_STATUS () == VCM_RED_ACTIVE)
    {
        VcmRedHandleBulkRequest ();
    }
    else
    {
        VCM_BULK_REQ_RECD () = VCM_TRUE;
    }
}

/*****************************************************************************/
/* Function Name      : vcmRedHandleDynSyncAudit                             */
/*                                                                           */
/* Description        : This function Handles the Dynamic Sync-up Audit      */
/*                      event. This event is used to start the audit         */
/*                      between the active and standby units for the         */
/*                      dynamic sync-up happened                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VcmRedHandleDynSyncAudit (VOID)
{
/*On receiving this event, vcm should execute show cmd and calculate checksum*/
    VcmExecuteCmdAndCalculateChkSum ();
    return;
}

/*****************************************************************************/
/* Function Name      : VcmRedSendBulkUpdateTailMsg                          */
/*                                                                           */
/* Description        : This function will send the tail msg to the standy   */
/*                      node, which indicates the completion of Bulk update  */
/*                      process.                                             */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE.                           */
/*****************************************************************************/
INT4
VcmRedSendBulkUpdateTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = VCM_INIT_VAL;
    UINT2               u2BufLen;
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_VCM_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (VCM_NODE_STATUS () != VCM_RED_ACTIVE)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC, "VCM: Node is not active."
                 "Bulk update tail msg not sent.\n");
        return VCM_SUCCESS;
    }

    /* Form a bulk update tail message.

     *        <-----------1 Byte----------><----2 Byte------>
     *******************************************************
     *        *                           *                *
     * RM Hdr * VCM_RED_BULK_UPD_TAIL_MSG * Msg Length     *
     * *      *                           *                *
     *******************************************************

     * The RM Hdr shall be included by RM.
     */

    u2BufLen = VCM_RED_BULK_TAIL_MSG_LEN;

    /* Allocate memory for data to be sent to RM. This memory will be freed by
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufLen)) == NULL)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC, "Rm alloc failed\n");
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        VcmRmHandleProtocolEvent (&ProtoEvt);
        return (VCM_FAILURE);
    }

    /* Fill the message type. */
    VCM_RM_PUT_1_BYTE (pMsg, &u4Offset, VCM_RED_BULK_UPD_TAIL_MSG);

    /* This length field will not be used. It is just to keep the
     * this message structure similar to that of other sync msgs */
    VCM_RM_PUT_2_BYTE (pMsg, &u4Offset, u2BufLen);

    if (VcmRedSendUpdateToRM (pMsg, u2BufLen) == VCM_FAILURE)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        VcmRmHandleProtocolEvent (&ProtoEvt);
    }

    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmRedGetNodeStatus                                 */
/*                                                                           */
/* Description        : This function returns the node status after getting  */
/*                      it from the RM module.                               */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : tVcmRedStates                                        */
/*****************************************************************************/
tVcmRedStates
VcmRedGetNodeStatus (VOID)
{
    tVcmRedStates       NodeStatus;
    UINT4               u4RmNodeState;

    VCM_TRC (VCM_CONTROL_PATH_TRC, "Node status of RM is ");

    u4RmNodeState = VcmRmGetNodeState ();

    if (VCM_RED_RM_GET_STATIC_CONFIG_STATUS () != RM_STATIC_CONFIG_RESTORED)
    {
        NodeStatus = VCM_RED_IDLE;
        return NodeStatus;
    }

    switch (u4RmNodeState)
    {
        case RM_INIT:
            VCM_TRC (VCM_CONTROL_PATH_TRC, "IDLE");
            NodeStatus = VCM_RED_IDLE;
            break;

        case RM_ACTIVE:
            VCM_TRC (VCM_CONTROL_PATH_TRC, "ACTIVE");
            NodeStatus = VCM_RED_ACTIVE;
            break;

        case RM_STANDBY:
            VCM_TRC (VCM_CONTROL_PATH_TRC, "STANDBY");
            NodeStatus = VCM_RED_STANDBY;
            break;

        default:
            NodeStatus = VCM_RED_IDLE;
            break;
    }

    return NodeStatus;
}

/*****************************************************************************/
/* Function Name      : VcmRedInitHardwareAudit                              */
/*                                                                           */
/* Description        : This function spawns a low priority task for the     */
/*                      hardware audit for the VCM & SISP related            */
/*                      functionalities                                      */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VcmRedInitHardwareAudit (VOID)
{
    INT4                i4RetVal;

    i4RetVal = (INT4) VCM_TASK_CREATE (VCM_AUDIT_TASK, VCM_AUDIT_TASK_PRIORITY,
                                       OSIX_DEFAULT_STACK_SIZE,
                                       (OsixTskEntry) VcmRedAuditTaskMain,
                                       0, &VCM_RED_AUDIT_TASK_ID ());

    if (i4RetVal != OSIX_SUCCESS)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC | VCM_CONTROL_PATH_TRC |
                 VCM_OS_RESOURCE_TRC,
                 "VCMRED: VCM Audit Task Creation FAILED\n");
    }
}

/*****************************************************************************/
/* Function Name      : VcmRedAuditTaskMain                                  */
/*                                                                           */
/* Description        : This function audits the VCM & SISP information      */
/*                      present in sw and the information present in hw and  */
/*                      syncs hw with that of sw.                            */
/*                                                                           */
/* Input(s)           : pi1Param - Unused parameter.                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VcmRedAuditTaskMain (INT1 *pi1Param)
{
    UNUSED_PARAM (pi1Param);

    VcmSispStartHwAudit ();
}

/*****************************************************************************/
/* Function Name      : VcmExecuteCmdAndCalculateChkSum                     */
/*                                                                           */
/* Description        : This function Handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VcmExecuteCmdAndCalculateChkSum (VOID)
{
    /*Execute CLI commands and calculate checksum */
    UINT2               u2AppId = RM_VCM_APP_ID;
    UINT2               u2ChkSum = 0;

    VCM_UNLOCK ();
    if (VcmGetShowCmdOutputAndCalcChkSum (&u2ChkSum) == VCM_FAILURE)
    {
        VCM_LOCK ();
        VCM_TRC (VCM_ALL_FAILURE_TRC,
                 "Checksum of calculation failed for VCM\n");
        return;
    }

#ifdef L2RED_WANTED
    if (VcmRmEnqChkSumMsgToRm (u2AppId, u2ChkSum) == VCM_FAILURE)
    {
        VCM_LOCK ();
        VCM_TRC (VCM_ALL_FAILURE_TRC, "Sending checkum to RM failed\n");
        return;
    }
#endif
    VCM_LOCK ();
    return;
}
#endif
