/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2009-2010                                          */
/*****************************************************************************/
/*    FILE  NAME            : sispport.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc   .                                */
/*    SUBSYSTEM NAME        : VCM module                                     */
/*    MODULE NAME           : VCM module                                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 01 Mar 2009                                    */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains the APIs used by  SISP      */
/*                            feature.                                       */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    31 APR 2009 / SISPTeam   Initial Create.                       */
/*---------------------------------------------------------------------------*/
#ifndef _SISPPORT_C
#define _SISPPORT_C

#include "vcminc.h"

/*****************************************************************************/
/* Function Name      : VcmSispL2IwfUpdateSispPortCtrlStatus                 */
/*                                                                           */
/* Description        : API used to indicate L2IWF and higher layer modules  */
/*                      on enabling or disabling  SISP on a particular       */
/*                      physical or port channel port                        */
/*                                                                           */
/* Input(s)           : u4IfIndex - IfIndex of the port or port channel port */
/*                      u1Status  - Enable/Disable                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispL2IwfUpdateSispPortCtrlStatus (UINT4 u4IfIndex, UINT1 u1Status)
{
    return (L2IwfUpdateSispPortCtrlStatus (u4IfIndex, u1Status));
}

/*****************************************************************************/
/* Function Name        : VcmSispL2IwfGetPortVlanMemberList                  */
/*                                                                           */
/* Description          : Returns the List of VLANs for which the particular */
/*                        port is member of. Lock should be taken before     */
/*                        calling this function.                             */
/*                                                                           */
/* Input (s)            : u2VlanId - VLAN ID value.                          */
/*                        pu1VlanList - VLAN List                            */
/*                                                                           */
/* Output               : None                                               */
/*                                                                           */
/* Return Value         : L2IWF_SUCCESS / L2IWF_FAILURE.                     */
/*****************************************************************************/
INT4
VcmSispL2IwfGetPortVlanMemberList (UINT4 u4IfIndex, UINT1 *pu1VlanList)
{
    return (L2IwfGetPortVlanMemberList (u4IfIndex, pu1VlanList));
}

/*****************************************************************************/
/* Function Name      : VcmSispCfaCopyPhysicalIfaceProperty                  */
/*                                                                           */
/* Description        : This API is invoked by VCM-SISP module whenever SISP */
/*                      is enabled on a port. CFA will inherit the properties*/
/*                      for the logical port from the provided physical port.*/
/*                                                                           */
/* Input(s)           : u4LogicalIndex - Logical port's interface index      */
/*                    : u4IfIndex - Interface index of the physical over     */
/*                                  this logical runs.                       */
/*                                                                           */
/* Output             : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
VcmSispCfaCopyPhysicalIfaceProperty (UINT4 u4LogicalIndex, UINT4 u4IfIndex)
{
    return (CfaCopyPhysicalIfaceProperty (u4LogicalIndex, u4IfIndex));
}

/****************************************************************************/
/*                                                                          */
/*    Function Name       : VcmSispCfaSispResetInterfaceProp                */
/*                                                                          */
/*    Description         : This function will initialise the properties of */
/*                          the SISP logical interfaces that will be        */
/*                          inherited from the corresponding physical       */
/*                          interfaces. This should be invoked when SISP    */
/*                          interface is unmapped from a physical port.     */
/*                                                                          */
/*    Input(s)            : u4LogicalIndex - interface index                */
/*                                                                          */
/*    Output(s)           : NONE                                            */
/*                                                                          */
/*    Global Variables Referred : None.                                     */
/*                                                                          */
/*    Global Variables Modified : None.                                     */
/*                                                                          */
/*    Exceptions or Operating                                               */
/*    System Error Handling    : None.                                      */
/*                                                                          */
/*    Use of Recursion        : None.                                       */
/*                                                                          */
/*    Returns            : CFA_SUCCESS                                      */
/****************************************************************************/
INT4
VcmSispCfaSispResetInterfaceProp (UINT4 u4LogicalIndex)
{
    return (CfaSispResetInterfaceProp (u4LogicalIndex));
}

/*****************************************************************************/
/* Function Name      : VcmSispAstIsContextStpCompatible                     */
/*                                                                           */
/* Description        : Called by other modules to know if STP compatibility */
/*                      is enabled in the given virtual context.             */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispAstIsContextStpCompatible (UINT4 u4ContextId)
{
    return (AstIsContextStpCompatible (u4ContextId));
}

/*****************************************************************************/
/* Function Name      : VcmSispL2IwfPbGetLocalVidListFromVIDTransTable       */
/*                                                                           */
/* Description        : This routine returns the list of local VLANs         */
/*                      configured for the given port.                       */
/*                                                                           */
/* Input(s)           : u4IfIndex   - IfIndex                                */
/*                      pu1LocalVidList - Local VID list                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
VcmSispL2IwfPbGetLocalVidListFromVIDTransTable (UINT4 u4IfIndex,
                                                UINT1 *pu1LocalVidList)
{
    return (L2IwfPbGetLocalVidListFromVIDTransTable (u4IfIndex,
                                                     pu1LocalVidList));
}

/*****************************************************************************/
/* Function Name      : VcmSispL2IwfGetPbPortType                            */
/*                                                                           */
/* Description        : This function is used to get the port type of a      */
/*                      Physical port                                        */
/*                                                                           */
/* Input(s)           : u4ContextId - Context ID                             */
/*                      u4IfIndex   - IfIndex of the port.                   */
/*                                                                           */
/* Output(s)          : *pu1PortType - CEP/CNP/PNP/PCEP/PCNP/PPNP            */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/
INT4
VcmSispL2IwfGetPbPortType (UINT4 u4IfIndex, UINT1 *pu1PortType)
{
    return (L2IwfGetPbPortType (u4IfIndex, pu1PortType));
}

/*****************************************************************************/
/* Function Name      : VcmSispAstIsRstStartedInContext                      */
/*                                                                           */
/* Description        : This function is called by SISP to check if RSTP is  */
/*                      running in given context.                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispAstIsRstStartedInContext (UINT4 u4ContextId)
{
    return (AstIsRstStartedInContext (u4ContextId));
}
#endif
