/*****************************************************************************/
/* Copyright (C) Future Software                                             */
/* Licensee Future Communication Software                                    */
/*    FILE  NAME            : vcml3if.c                                      */
/*    DESCRIPTION           : This file contains functions to interact with  */
/*                            L3 Protocols by VCM                            */
/*---------------------------------------------------------------------------*/

#ifndef _VCML3IF_C
#define _VCML3IF_C

#include "vcminc.h"

/***************************************************************************/
/*                  Interfaces with Layer3 modules.                        */
/***************************************************************************/

/*****************************************************************************/
/* Function Name      : VcmL3DeleteVirtualContextIndication                  */
/*                                                                           */
/* Description        : This routine calls the corresponding L3 protocols    */
/*                      Shutdown functions to delete the instance            */
/*                      for the context deleted.                             */
/*                                                                           */
/* Input(s)           : u4VcNum  - Virtual Context number                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
VcmL3DeleteVirtualContextIndication (UINT4 u4VcNum)
{
    UINT4               u4ProtocolId = 0;

    while (u4ProtocolId < IP_MAX_PROTOCOLS)
    {
        if ((gaVcmHLProtoRegTbl[u4ProtocolId].u4CxtCount != 0) &&
            (gaVcmHLProtoRegTbl[u4ProtocolId].u1RegFlag
             & VCM_CXT_STATUS_CHG_REQ))
        {
            (*(gaVcmHLProtoRegTbl[u4ProtocolId].pIfMapChngAndCxtChng))
                (VCM_MAX_IP_INTERFACES, u4VcNum, VCM_CXT_STATUS_CHG_REQ);
        }

        u4ProtocolId++;
    }
}

/*****************************************************************************/
/* Function Name      : VcmL3DeleteInterfaceIndication                       */
/*                                                                           */
/* Description        : This routine intimates the L3 protocols about the    */
/*                      interface deletion.                                  */
/*                                                                           */
/* Input(s)           : u4IpIfIndex - Interface Index.                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
VcmL3DeleteInterfaceIndication (UINT4 u4IpIfIndex)
{
    UINT4               u4VcNum = 0;
    UINT4               u4ProtocolId = 0;

    u4VcNum = aVcmIpIfMapEntry[u4IpIfIndex].u4FsVcIpId;
    if (u4VcNum == SYS_DEF_MAX_NUM_CONTEXTS)
    {
        return;
    }

    while (u4ProtocolId < IP_MAX_PROTOCOLS)
    {
        if ((gaVcmHLProtoRegTbl[u4ProtocolId].u4CxtCount != 0) &&
            (gaVcmHLProtoRegTbl[u4ProtocolId].u1RegFlag & VCM_IF_MAP_CHG_REQ))
        {
            (*(gaVcmHLProtoRegTbl[u4ProtocolId].pIfMapChngAndCxtChng))
                (u4IpIfIndex, u4VcNum, VCM_IF_MAP_CHG_REQ);
        }

        u4ProtocolId++;
    }
}

#endif
