/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* $Id: vcml2if.c,v 1.16 2014/11/03 12:17:02 siva Exp $                                                   */
/* Licensee Aricent Inc., 2004-2005                                          */
/*****************************************************************************/
/*    FILE  NAME            : vcml2if.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : VCM module                                     */
/*    MODULE NAME           : VCM module                                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 16 Jun 2006                                    */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains functions to interact with  */
/*                            L2 Protocols by VCM                            */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    16 JUN 2006 / MITeam   Initial Create.                         */
/*---------------------------------------------------------------------------*/

#ifndef _VCML2IF_C
#define _VCML2IF_C

#include "vcminc.h"
#include "pnac.h"
#include "bridge.h"
#include "fsvlan.h"
#include "rstp.h"

/*****************************************************************************/
/* Function Name      : VcmCreateInterface                                   */
/*                                                                           */
/* Description        : This routine intimates the L2 protocols about the    */
/*                      interface creation. This routine is called           */
/*                      when interface create indication comes from CFA.     */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index.                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmCreateInterface (UINT4 u4IfIndex)
{
    tVcmIfMapEntry     *pIfMapEntry = NULL;
    tVcmIfMapEntry      Dummy;
    UINT4               u4VcNum = 0;
    UINT2               u2HlPortId = 0;
    tCfaIfInfo          IfInfo;
    UINT4               u4PhyIfIndex;
#ifdef NPAPI_WANTED
    tContextMapInfo     ContextMapInfo;
    MEMSET (&ContextMapInfo, 0, sizeof (tContextMapInfo));
#endif
    Dummy.u4IfIndex = u4IfIndex;

    VCM_LOCK ();

    if (NULL != (pIfMapEntry = VcmFindIfMapEntry (&Dummy)))
    {
        if (FAILURE == VcmAddToVcPortList (pIfMapEntry))
        {
            VCM_TRC (VCM_ALL_FAILURE_TRC | VCM_OS_RESOURCE_TRC,
                     "VcmAddToVcPortList function return Failure.\n");
            VCM_UNLOCK ();
            return VCM_FAILURE;
        }
        u4VcNum = pIfMapEntry->u4VcNum;
        u2HlPortId = pIfMapEntry->u2HlPortId;
        u4PhyIfIndex = pIfMapEntry->u4PhyIfIndex;

        VCM_UNLOCK ();

        CfaGetIfInfo (u4PhyIfIndex, &IfInfo);

        if (VCM_NODE_STATUS () == VCM_RED_ACTIVE)
        {
            if (VcmRedSendSyncMessages (u4IfIndex, u2HlPortId, VCM_RED_IFMAP)
                != VCM_SUCCESS)
            {
                VCM_TRC (VCM_ALL_FAILURE_TRC | VCM_CONTROL_PATH_TRC,
                         "VCMRED: Vcm Send Sync Message to Standby Failed\n");
            }
#ifdef NPAPI_WANTED
            if (VCM_NODE_STATUS () == VCM_RED_ACTIVE)
            {
                if ((IfInfo.u1IfType != CFA_LAGG) ||
                    (IfInfo.u1IfOperStatus == CFA_IF_UP))
                {
                    if (VcmFsVcmHwMapPortToContext (u4VcNum, u4PhyIfIndex)
                        == FNP_FAILURE)
                    {
                        return VCM_FAILURE;
                    }

                    ContextMapInfo.u2LocalPortId = u2HlPortId;

                    /* For the physical port primary context has to be set as 
                     * TRUE*/
                    if (SISP_IS_LOGICAL_PORT (u4IfIndex) == VCM_TRUE)
                    {
                        ContextMapInfo.u1IsPrimaryCtx = VCM_FALSE;
                    }
                    else
                    {
                        ContextMapInfo.u1IsPrimaryCtx = VCM_TRUE;
                    }

                    if (VcmFsVcmHwMapIfIndexToBrgPort
                        (u4VcNum, u4PhyIfIndex, ContextMapInfo) == FNP_FAILURE)
                    {
                        return VCM_FAILURE;
                    }
                }
            }
#endif
        }

        /* For Lag this should not return Success here as we need to create a port in vlan also */
        if (IfInfo.u1IfType != CFA_LAGG)
        {
            if (IfInfo.u1BridgedIface == CFA_DISABLED)
            {
                return VCM_SUCCESS;
            }
        }
        /* Create interface in L2 */
        if (L2CreateInterface (u4VcNum, u4IfIndex, u2HlPortId) != L2IWF_SUCCESS)
        {
            VCM_TRC (VCM_ALL_FAILURE_TRC | VCM_OS_RESOURCE_TRC,
                     "L2IwfPortMapIndication function Failed.\n");
        }

        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;
}

#endif
