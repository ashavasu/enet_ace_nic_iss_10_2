/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* $Id: vcmnpapi.c,v 1.5 2017/11/14 07:31:19 siva Exp $                                */
/*****************************************************************************/
/*    FILE  NAME            : vcmnpapi.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Virtual Context Manager Module                 */
/*    MODULE NAME           : Virtual Context Manager Module                 */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 16 June 2006                                   */
/*    AUTHOR                : MI Team                                        */
/*    DESCRIPTION           : This file contains NPAPI wrapper functions.    */
/*****************************************************************************/

/***************************************************************************
 *                                                                          
 *    Function Name       : VcmFsVcmHwCreateContext                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsVcmHwCreateContext
 *                                                                          
 *    Input(s)            : Arguments of FsVcmHwCreateContext
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#ifndef __VCM_NP_API_C
#define __VCM_NP_API_C

#include "nputil.h"

UINT1
VcmFsVcmHwCreateContext (UINT4 u4ContextId)
{
    tFsHwNp             FsHwNp;
    tVcmNpModInfo      *pVcmNpModInfo = NULL;
    tVcmNpWrFsVcmHwCreateContext *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VCM_MOD,    /* Module ID */
                         FS_VCM_HW_CREATE_CONTEXT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVcmNpModInfo = &(FsHwNp.VcmNpModInfo);
    pEntry = &pVcmNpModInfo->VcmNpFsVcmHwCreateContext;

    pEntry->u4ContextId = u4ContextId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VcmFsVcmHwDeleteContext                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsVcmHwDeleteContext
 *                                                                          
 *    Input(s)            : Arguments of FsVcmHwDeleteContext
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VcmFsVcmHwDeleteContext (UINT4 u4ContextId)
{
    tFsHwNp             FsHwNp;
    tVcmNpModInfo      *pVcmNpModInfo = NULL;
    tVcmNpWrFsVcmHwDeleteContext *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VCM_MOD,    /* Module ID */
                         FS_VCM_HW_DELETE_CONTEXT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVcmNpModInfo = &(FsHwNp.VcmNpModInfo);
    pEntry = &pVcmNpModInfo->VcmNpFsVcmHwDeleteContext;

    pEntry->u4ContextId = u4ContextId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VcmFsVcmHwMapIfIndexToBrgPort                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsVcmHwMapIfIndexToBrgPort
 *                                                                          
 *    Input(s)            : Arguments of FsVcmHwMapIfIndexToBrgPort
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VcmFsVcmHwMapIfIndexToBrgPort (UINT4 u4ContextId, UINT4 u4IfIndex,
                               tContextMapInfo ContextInfo)
{
    tFsHwNp             FsHwNp;
    tVcmNpModInfo      *pVcmNpModInfo = NULL;
    tVcmNpWrFsVcmHwMapIfIndexToBrgPort *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VCM_MOD,    /* Module ID */
                         FS_VCM_HW_MAP_IF_INDEX_TO_BRG_PORT,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVcmNpModInfo = &(FsHwNp.VcmNpModInfo);
    pEntry = &pVcmNpModInfo->VcmNpFsVcmHwMapIfIndexToBrgPort;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->ContextInfo = ContextInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VcmFsVcmHwMapPortToContext                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsVcmHwMapPortToContext
 *                                                                          
 *    Input(s)            : Arguments of FsVcmHwMapPortToContext
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VcmFsVcmHwMapPortToContext (UINT4 u4ContextId, UINT4 u4IfIndex)
{
    tFsHwNp             FsHwNp;
    tVcmNpModInfo      *pVcmNpModInfo = NULL;
    tVcmNpWrFsVcmHwMapPortToContext *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VCM_MOD,    /* Module ID */
                         FS_VCM_HW_MAP_PORT_TO_CONTEXT,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVcmNpModInfo = &(FsHwNp.VcmNpModInfo);
    pEntry = &pVcmNpModInfo->VcmNpFsVcmHwMapPortToContext;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VcmFsVcmHwMapVirtualRouter                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsVcmHwMapVirtualRouter
 *                                                                          
 *    Input(s)            : Arguments of FsVcmHwMapVirtualRouter
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VcmFsVcmHwMapVirtualRouter (tVrMapAction VrMapAction,
                            tFsNpVcmVrMapInfo * pVcmVrMapInfo)
{
    tFsHwNp             FsHwNp;
    tVcmNpModInfo      *pVcmNpModInfo = NULL;
    tVcmNpWrFsVcmHwMapVirtualRouter *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VCM_MOD,    /* Module ID */
                         FS_VCM_HW_MAP_VIRTUAL_ROUTER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVcmNpModInfo = &(FsHwNp.VcmNpModInfo);
    pEntry = &pVcmNpModInfo->VcmNpFsVcmHwMapVirtualRouter;

    pEntry->VrMapAction = VrMapAction;
    pEntry->pVcmVrMapInfo = pVcmVrMapInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VcmFsVcmHwUnMapPortFromContext                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsVcmHwUnMapPortFromContext
 *                                                                          
 *    Input(s)            : Arguments of FsVcmHwUnMapPortFromContext
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VcmFsVcmHwUnMapPortFromContext (UINT4 u4ContextId, UINT4 u4IfIndex)
{
    tFsHwNp             FsHwNp;
    tVcmNpModInfo      *pVcmNpModInfo = NULL;
    tVcmNpWrFsVcmHwUnMapPortFromContext *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VCM_MOD,    /* Module ID */
                         FS_VCM_HW_UN_MAP_PORT_FROM_CONTEXT,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVcmNpModInfo = &(FsHwNp.VcmNpModInfo);
    pEntry = &pVcmNpModInfo->VcmNpFsVcmHwUnMapPortFromContext;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VcmFsVcmSispHwSetPortCtrlStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsVcmSispHwSetPortCtrlStatus
 *                                                                          
 *    Input(s)            : Arguments of FsVcmSispHwSetPortCtrlStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VcmFsVcmSispHwSetPortCtrlStatus (UINT4 u4IfIndex, UINT1 u1Status)
{
    tFsHwNp             FsHwNp;
    tVcmNpModInfo      *pVcmNpModInfo = NULL;
    tVcmNpWrFsVcmSispHwSetPortCtrlStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VCM_MOD,    /* Module ID */
                         FS_VCM_SISP_HW_SET_PORT_CTRL_STATUS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVcmNpModInfo = &(FsHwNp.VcmNpModInfo);
    pEntry = &pVcmNpModInfo->VcmNpFsVcmSispHwSetPortCtrlStatus;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1Status = u1Status;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VcmFsVcmSispHwSetPortVlanMapping                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsVcmSispHwSetPortVlanMapping
 *                                                                          
 *    Input(s)            : Arguments of FsVcmSispHwSetPortVlanMapping
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VcmFsVcmSispHwSetPortVlanMapping (UINT4 u4IfIndex, tVlanId VlanId,
                                  UINT4 u4ContextId, UINT1 u1Status)
{
    tFsHwNp             FsHwNp;
    tVcmNpModInfo      *pVcmNpModInfo = NULL;
    tVcmNpWrFsVcmSispHwSetPortVlanMapping *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VCM_MOD,    /* Module ID */
                         FS_VCM_SISP_HW_SET_PORT_VLAN_MAPPING,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVcmNpModInfo = &(FsHwNp.VcmNpModInfo);
    pEntry = &pVcmNpModInfo->VcmNpFsVcmSispHwSetPortVlanMapping;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->VlanId = VlanId;
    pEntry->u4ContextId = u4ContextId;
    pEntry->u1Status = u1Status;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VcmFsVcmHwVrfCounter                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsVcmSispHwSetPortVlanMapping
 *                                                                          
 *    Input(s)            : Arguments of VcmFsVcmHwVrfCounter
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
UINT1
VcmFsVcmHwVrfCounter (tVrfCounter VrfCounter)
{

    tFsHwNp             FsHwNp;
    tVcmNpModInfo      *pVcmNpModInfo = NULL;
    tVcmNpWrFsVrfCounters *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VCM_MOD,    /* Module ID */
                         FS_VCM_HW_VRF_COUNTERS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVcmNpModInfo = &(FsHwNp.VcmNpModInfo);
    pEntry = &pVcmNpModInfo->VcmNpWrFsVrfCounters;

    pEntry->VrfCounter.u4VrfId = VrfCounter.u4VrfId;
    pEntry->VrfCounter.u1StatsType = VrfCounter.u1StatsType;
    pEntry->VrfCounter.pu4VrfStatsValue = VrfCounter.pu4VrfStatsValue;

    return (NpUtilHwProgram (&FsHwNp));
}

#ifdef MBSM_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : VcmFsVcmMbsmHwCreateContext                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsVcmMbsmHwCreateContext
 *                                                                          
 *    Input(s)            : Arguments of FsVcmMbsmHwCreateContext
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VcmFsVcmMbsmHwCreateContext (UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVcmNpModInfo      *pVcmNpModInfo = NULL;
    tVcmNpWrFsVcmMbsmHwCreateContext *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VCM_MOD,    /* Module ID */
                         FS_VCM_MBSM_HW_CREATE_CONTEXT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVcmNpModInfo = &(FsHwNp.VcmNpModInfo);
    pEntry = &pVcmNpModInfo->VcmNpFsVcmMbsmHwCreateContext;

    pEntry->u4ContextId = u4ContextId;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VcmFsVcmMbsmHwMapIfIndexToBrgPort                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsVcmMbsmHwMapIfIndexToBrgPort
 *                                                                          
 *    Input(s)            : Arguments of FsVcmMbsmHwMapIfIndexToBrgPort
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VcmFsVcmMbsmHwMapIfIndexToBrgPort (UINT4 u4ContextId, UINT4 u4IfIndex,
                                   tContextMapInfo ContextInfo,
                                   tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVcmNpModInfo      *pVcmNpModInfo = NULL;
    tVcmNpWrFsVcmMbsmHwMapIfIndexToBrgPort *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VCM_MOD,    /* Module ID */
                         FS_VCM_MBSM_HW_MAP_IF_INDEX_TO_BRG_PORT,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVcmNpModInfo = &(FsHwNp.VcmNpModInfo);
    pEntry = &pVcmNpModInfo->VcmNpFsVcmMbsmHwMapIfIndexToBrgPort;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->ContextInfo = ContextInfo;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VcmFsVcmMbsmHwMapPortToContext                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsVcmMbsmHwMapPortToContext
 *                                                                          
 *    Input(s)            : Arguments of FsVcmMbsmHwMapPortToContext
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VcmFsVcmMbsmHwMapPortToContext (UINT4 u4ContextId, UINT4 u4IfIndex,
                                tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVcmNpModInfo      *pVcmNpModInfo = NULL;
    tVcmNpWrFsVcmMbsmHwMapPortToContext *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VCM_MOD,    /* Module ID */
                         FS_VCM_MBSM_HW_MAP_PORT_TO_CONTEXT,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVcmNpModInfo = &(FsHwNp.VcmNpModInfo);
    pEntry = &pVcmNpModInfo->VcmNpFsVcmMbsmHwMapPortToContext;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VcmFsVcmMbsmHwMapVirtualRouter                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsVcmMbsmHwMapVirtualRouter
 *                                                                          
 *    Input(s)            : Arguments of FsVcmMbsmHwMapVirtualRouter
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VcmFsVcmMbsmHwMapVirtualRouter (tVrMapAction VrMapAction,
                                tFsNpVcmVrMapInfo * pVcmVrMapInfo,
                                tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVcmNpModInfo      *pVcmNpModInfo = NULL;
    tVcmNpWrFsVcmMbsmHwMapVirtualRouter *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VCM_MOD,    /* Module ID */
                         FS_VCM_MBSM_HW_MAP_VIRTUAL_ROUTER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVcmNpModInfo = &(FsHwNp.VcmNpModInfo);
    pEntry = &pVcmNpModInfo->VcmNpFsVcmMbsmHwMapVirtualRouter;

    pEntry->VrMapAction = VrMapAction;
    pEntry->pVcmVrMapInfo = pVcmVrMapInfo;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VcmFsVcmSispMbsmHwSetPortCtrlStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsVcmSispMbsmHwSetPortCtrlStatus
 *                                                                          
 *    Input(s)            : Arguments of FsVcmSispMbsmHwSetPortCtrlStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VcmFsVcmSispMbsmHwSetPortCtrlStatus (UINT4 u4IfIndex, UINT1 u1Status,
                                     tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVcmNpModInfo      *pVcmNpModInfo = NULL;
    tVcmNpWrFsVcmSispMbsmHwSetPortCtrlStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VCM_MOD,    /* Module ID */
                         FS_VCM_SISP_MBSM_HW_SET_PORT_CTRL_STATUS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVcmNpModInfo = &(FsHwNp.VcmNpModInfo);
    pEntry = &pVcmNpModInfo->VcmNpFsVcmSispMbsmHwSetPortCtrlStatus;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1Status = u1Status;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : VcmFsVcmSispMbsmHwSetPortVlanMapping                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsVcmSispMbsmHwSetPortVlanMapping
 *                                                                          
 *    Input(s)            : Arguments of FsVcmSispMbsmHwSetPortVlanMapping
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
VcmFsVcmSispMbsmHwSetPortVlanMapping (UINT4 u4IfIndex, tVlanId VlanId,
                                      UINT4 u4ContextId, UINT1 u1Status,
                                      tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tVcmNpModInfo      *pVcmNpModInfo = NULL;
    tVcmNpWrFsVcmSispMbsmHwSetPortVlanMapping *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_VCM_MOD,    /* Module ID */
                         FS_VCM_SISP_MBSM_HW_SET_PORT_VLAN_MAPPING,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pVcmNpModInfo = &(FsHwNp.VcmNpModInfo);
    pEntry = &pVcmNpModInfo->VcmNpFsVcmSispMbsmHwSetPortVlanMapping;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->VlanId = VlanId;
    pEntry->u4ContextId = u4ContextId;
    pEntry->u1Status = u1Status;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}
#endif /* MBSM_WANTED */

#endif /*__VCM_NP_API_C */
