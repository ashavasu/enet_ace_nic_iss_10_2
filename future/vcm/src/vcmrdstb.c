/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: vcmrdstb.c,v 1.5 2009/09/24 14:30:15 prabuc Exp $
 *
 * Description: This file contains the Dummy functions to support Redundancy
 *              for VCM module.
 *
 *******************************************************************/
#ifndef L2RED_WANTED
#include "vcminc.h"

/*****************************************************************************/
/* Function Name      : VcmRedRmInit                                         */
/*                                                                           */
/* Description        : Initialises the Memory Pools and Registers with RM   */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gVcmRedGlobalInfo                                    */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gVcmRedGlobalInfo                                    */
/*                                                                           */
/* Return Value(s)    : if registration is success then VCM_SUCCESS          */
/*                      Otherwise VCM_FAILURE                                */
/*****************************************************************************/

INT4
VcmRedRmInit (VOID)
{
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmRedRegisterWithRM                                 */
/*                                                                           */
/* Description        : Creates a queue for receiving messages from RM and   */
/*                      registers VCMP/MSTP with RM.                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if registration is success then VCM_SUCCESS          */
/*                      Otherwise VCM_FAILURE                                 */
/*****************************************************************************/
INT4
VcmRedRegisterWithRM (VOID)
{
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmRedSendSyncMessages                               */
/*                                                                           */
/* Description        : This function is invoked to  Alloc and Send Bulk     */
/*                      Sync Up Messages                                     */
/*                                                                           */
/* Input(s)           : u4PortIfIndex - Physical Port Index                  */
/*                      u2LocalPortId - Context based local Port Number      */
/*                      u1MessageType - Message Type                         */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS/VCM_FAILURE.                             */
/*****************************************************************************/
INT4
VcmRedSendSyncMessages (UINT4 u4PortIfIndex, UINT2 u2LocalPortId,
                        UINT1 u1MessageType)
{
    UNUSED_PARAM (u4PortIfIndex);
    UNUSED_PARAM (u2LocalPortId);
    UNUSED_PARAM (u1MessageType);
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmRedClearSyncUpData                                */
/* Description        : Clears stale Data On Port                            */
/* Input(s)           : u4PortIfIndex - Port If index                        */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : tVcmRedGlobalInfo.                                   */
/* Global Variables                                                          */
/* Modified           : tVcmRedGlobalInfo.                                   */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VcmRedClearSyncUpData (UINT4 u4PortIfIndex)
{
    UNUSED_PARAM (u4PortIfIndex);
}

/*****************************************************************************/
/* Function Name      : VcmSispRedSendModuleStatus                           */
/*                                                                           */
/* Description        : This function used to send SISP module start/shudown */
/*                      sync up information to the standby.                  */
/*                                                                           */
/* Input(s)           : u1Status                                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispRedSendModuleStatus (UINT1 u1Status)
{
    UNUSED_PARAM (u1Status);
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispRedSendPortCtrlStatus                         */
/*                                                                           */
/* Description        : This function used to send SISP port ctrl status     */
/*                      (SISP_ENABLE / SISP_DISABLE) sync up information to  */
/*                      the standby.                                         */
/*                                                                           */
/* Input(s)           : u4PhyIfIndex - PhyIfIndex                            */
/*                      u1Status     - SISP_ENABLE/SISP_DISABLE              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispRedSendPortCtrlStatus (UINT4 u4PhyIfIndex, UINT1 u1Status)
{
    UNUSED_PARAM (u4PhyIfIndex);
    UNUSED_PARAM (u1Status);

    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispRedSendPvcStatus                              */
/*                                                                           */
/* Description        : This function used to send Port Vlan Context Entry   */
/*                      updation status information to the stand by.         */
/*                                                                           */
/* Input(s)           : u4PhyIfIndex - PhyIfIndex                            */
/*                      LastSyncVlan - Vlan Identifier                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispRedSendPvcStatus (UINT4 u4PhyIfIndex, tVlanId LastSyncVlan)
{
    UNUSED_PARAM (u4PhyIfIndex);
    UNUSED_PARAM (LastSyncVlan);

    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispRedSendCompleteStatus                         */
/*                                                                           */
/* Description        : This function used to send Operation complete        */
/*                      status information to the stand by.                  */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispRedSendCompleteStatus (VOID)
{
    return VCM_SUCCESS;
}
#endif
