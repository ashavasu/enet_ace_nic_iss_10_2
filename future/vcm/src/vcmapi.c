/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* $Id: vcmapi.c,v 1.62 2016/10/03 10:34:46 siva Exp $                                */
/*****************************************************************************/
/*    FILE  NAME            : vcmapi.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Virtual Context Manager Module                 */
/*    MODULE NAME           : Virtual Context Manager Module                 */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 16 June 2006                                   */
/*    AUTHOR                : MI Team                                      */
/*    DESCRIPTION           : This file contains API related functions.      */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    16 June 2006  /             Initial Create.                    */
/*            MI Team                                                        */
/*****************************************************************************/

/*****************************************************************************/
/*                           INCLUDE FILES                                   */
/*****************************************************************************/

#ifndef _VCMAPI_C
#define _VCMAPI_C

#include "vcminc.h"
#include "fsvcmcli.h"
#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
extern  INT4 RtmActOnStaticRoutesForIfDeleteInCxt (UINT4 u4ContextId, UINT4 u4IpPort);
#endif
/*****************************************************************************/
/* Function Name      : VcmModuleStart                                       */
/*                                                                           */
/* Description        : This function is an API to start the VCM module      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS/VCM_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
VcmModuleStart (VOID)
{
    VCM_LOCK ();
    if (VcmHandleModuleStart () == VCM_FAILURE)
    {
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }
    VCM_UNLOCK ();

    if (VcmCreateVirtualContext (VCM_DEFAULT_CONTEXT) != VCM_SUCCESS)
    {
        VCM_TRC (VCM_INIT_SHUT_TRC | VCM_CONTROL_PATH_TRC,
                 "VcmModuleStart: Default Context Creation FAILED... \n");
        return VCM_FAILURE;
    }

    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmModuleShutdown                                    */
/*                                                                           */
/* Description        : This function is an API to shutdown the VCM module.  */
/*                      This also takes care of shutting down SISP module.   */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS/VCM_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
VcmModuleShutdown (VOID)
{
    /* No extra processing for red needs to be done in case of SISP. It can
     * be shutdown and started along with VCM
     * */
    if (VCM_GET_SISP_STATUS == SISP_START)
    {
        if (VcmSispModuleShutdown () == VCM_FAILURE)
        {
            return VCM_FAILURE;
        }
    }

    if (VcmHandleModuleShutdown () == VCM_FAILURE)
    {
        return VCM_FAILURE;
    }

    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmConfLock                                          */
/*                                                                           */
/* Description        : This function is used to take the VCM mutual         */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gVcmGlobals.VcmConfSemId                             */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS or VCM_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
VcmConfLock (VOID)
{
    if (VCM_TAKE_SEM (gVcmGlobals.VcmConfSemId) != OSIX_SUCCESS)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC,
                 "vlanapi.c: Failed to Take VCM Mutual Exclusion Sema4 \n");
        return VCM_FAILURE;
    }
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmConfUnLock                                        */
/*                                                                           */
/* Description        : This function is used to give the VCM mutual         */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gVcmGlobals.VcmConfSemId                             */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS or VCM_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
VcmConfUnLock (VOID)
{
    VCM_RELEASE_SEM (gVcmGlobals.VcmConfSemId);
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmLock                                              */
/*                                                                           */
/* Description        : This function is used to take the VCM mutual         */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gVcmGlobals.VcmSemId                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS or VCM_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
VcmLock (VOID)
{
    if (VCM_TAKE_SEM (gVcmGlobals.VcmSemId) != OSIX_SUCCESS)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC,
                 "vlanapi.c: Failed to Take VCM Mutual Exclusion Sema4 \n");
        return VCM_FAILURE;
    }
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmUnLock                                            */
/*                                                                           */
/* Description        : This function is used to give the VCM mutual         */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gVcmGlobals.VcmSemId                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS or VCM_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
VcmUnLock (VOID)
{
    VCM_RELEASE_SEM (gVcmGlobals.VcmSemId);
    return ((INT4) VCM_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : VcmGetSystemMode                                     */
/*                                                                           */
/* Description        : This function is used to get the mode (SI / MI)      */
/*                      of the switch for the corresponding protocol.        */
/*                                                                           */
/* Input(s)           : u2ProtocolId   - Protocol Identifier.                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SI_MODE or VCM_MI_MODE                           */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetSystemMode (UINT2 u2ProtocolId)
{

    if ((u2ProtocolId > L3_PROTOCOL_ID) && (u2ProtocolId < MAX_PROTOCOL_ID))
    {
#ifdef VRF_WANTED
        return VCM_MI_MODE;
#else
        return VCM_SI_MODE;
#endif
    }

#ifdef MI_WANTED
    return VCM_MI_MODE;
#else
    return VCM_SI_MODE;
#endif
}

/*****************************************************************************/
/* Function Name      : VcmGetSystemModeExt                                  */
/*                                                                           */
/* Description        : This function is used to get the mode (SI / MI)      */
/*                      of the switch for the corresponding protocol.        */
/*                                                                           */
/* Input(s)           : u2ProtocolId   - Protocol Identifier.                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SI_MODE or VCM_MI_MODE                           */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetSystemModeExt (UINT2 u2ProtocolId)
{
    if ((u2ProtocolId > L3_PROTOCOL_ID) && (u2ProtocolId < MAX_PROTOCOL_ID))
    {
#ifdef VRF_WANTED
        if (SYS_DEF_MAX_NUM_CONTEXTS == 1)
            return VCM_SI_MODE;
        else
            return VCM_MI_MODE;
#else
        return VCM_SI_MODE;
#endif
    }

#ifdef MI_WANTED
    if (SYS_DEF_MAX_NUM_CONTEXTS == 1)
        return VCM_SI_MODE;
    else
        return VCM_MI_MODE;
#else
    return VCM_SI_MODE;
#endif
}

/*****************************************************************************/
/* Function Name      : VcmGetContextInfoFromIfIndex                         */
/*                                                                           */
/* Description        : This function is used to get the Context-Id and      */
/*                      the LocalPort-Id for the given IfIndex.              */
/*                                                                           */
/* Input(s)           : u4IfIndex      - Interface Index                     */
/*                                                                           */
/* Output(s)          : pu4ContextId   - Corresponding Context-Id            */
/*                      pu2LocalPortId - Corresponding Local Port Id         */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS or VCM_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetContextInfoFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4ContextId,
                              UINT2 *pu2LocalPortId)
{
    tVcmIfMapEntry     *pIfMapEntry = NULL;
    tVcmIfMapEntry      Dummy;

    Dummy.u4IfIndex = u4IfIndex;

    VCM_LOCK ();

    if (NULL == (pIfMapEntry = VcmFindIfMapEntry (&Dummy)))
    {
        /* Row does not exist. */
        VCM_UNLOCK ();
        VCM_TRC (VCM_ALL_FAILURE_TRC, "VcmGetContextInfoFromIfIndex : Row does not exist. Returns Failure \n");
        return VCM_FAILURE;
    }
    if (pIfMapEntry->u1RowStatus == NOT_READY)
    {
        /* Row Status is not ready */
        VCM_UNLOCK ();
        VCM_TRC (VCM_ALL_FAILURE_TRC, "VcmGetContextInfoFromIfIndex : Row status is not ready. Returns Failure \n");
        return VCM_FAILURE;
    }
    *pu4ContextId = pIfMapEntry->u4VcNum;
    *pu2LocalPortId = pIfMapEntry->u2HlPortId;

    VCM_UNLOCK ();

    VCM_TRC (VCM_CONTROL_PATH_TRC, "VcmGetContextInfoFromIfIndex provided the Context-Id and"
                      "the LocalPort-Id for the given IfIndex and returns success \n");

    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmGetFirstActiveContext                             */
/*                                                                           */
/* Description        : This function is used to get the first               */
/*                      active context present in the system.                */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu4ContextId - First Active Context.                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS or VCM_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetFirstActiveContext (UINT4 *pu4ContextId)
{
    tVirtualContextInfo *pVcInfo = NULL;

    VCM_LOCK ();

    pVcInfo = VcmGetFirstVc ();

    if (NULL != pVcInfo)
    {
        *pu4ContextId = pVcInfo->u4VcNum;

        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/* Function Name      : VcmGetFirstActiveL3Context                           */
/*                                                                           */
/* Description        : This function is used to get the first               */
/*                      active L3 context present in the system.             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu4ContextId - First Active L3 Context.              */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS or VCM_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetFirstActiveL3Context (UINT4 *pu4ContextId)
{
    tVirtualContextInfo Dummy;
    tVirtualContextInfo *pVcInfo = NULL;

    VCM_LOCK ();

    pVcInfo = VcmGetFirstVc ();

    while (NULL != pVcInfo)
    {
        if ((pVcInfo->u1CurrCxtType == VCM_L3_CONTEXT) ||
            (pVcInfo->u1CurrCxtType == VCM_L2_L3_CONTEXT))
        {
            *pu4ContextId = pVcInfo->u4VcNum;
            VCM_UNLOCK ();
            return VCM_SUCCESS;
        }
        Dummy.u4VcNum = pVcInfo->u4VcNum;
        pVcInfo = VcmGetNextVc (&Dummy);
    }
    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/* Function Name      : VcmGetNextActiveContext                              */
/*                                                                           */
/* Description        : This function is used to get the next                */
/*                      active context present in the system.                */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Id.                            */
/*                                                                           */
/* Output(s)          : pu4NextContextId - Next Active Context.              */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS or VCM_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetNextActiveContext (UINT4 u4ContextId, UINT4 *pu4NextContextId)
{
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo Dummy;

    Dummy.u4VcNum = u4ContextId;

    VCM_LOCK ();

    if (NULL != (pVcInfo = VcmGetNextVc (&Dummy)))
    {
        *pu4NextContextId = pVcInfo->u4VcNum;

        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/* Function Name      : VcmGetNextActiveL2Context                            */
/*                                                                           */
/* Description        : This function is used to get the next                */
/*                      active L2 context present in the system.             */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Id.                            */
/*                                                                           */
/* Output(s)          : pu4NextContextId - Next Active Context.              */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS or VCM_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetNextActiveL2Context (UINT4 u4ContextId, UINT4 *pu4NextContextId)
{
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo Dummy;

    Dummy.u4VcNum = u4ContextId;
    VCM_LOCK ();

    if (NULL != (pVcInfo = VcmGetNextVc (&Dummy)))
    {
        if ((pVcInfo->u1CurrCxtType == VCM_L2_CONTEXT) ||
            (pVcInfo->u1CurrCxtType == VCM_L2_L3_CONTEXT))
        {
            *pu4NextContextId = pVcInfo->u4VcNum;
            VCM_UNLOCK ();
            return VCM_SUCCESS;
        }
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/* Function Name      : VcmGetNextActiveL3Context                            */
/*                                                                           */
/* Description        : This function is used to get the next                */
/*                      active l3 context present in the system.             */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Id.                            */
/*                                                                           */
/* Output(s)          : pu4NextContextId - Next Active l3 context.           */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS or VCM_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetNextActiveL3Context (UINT4 u4ContextId, UINT4 *pu4NextContextId)
{
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo Dummy;

    Dummy.u4VcNum = u4ContextId;

    VCM_LOCK ();
    pVcInfo = VcmGetNextVc (&Dummy);
    while (NULL != pVcInfo)
    {
        if ((pVcInfo->u1CurrCxtType == VCM_L3_CONTEXT) ||
            (pVcInfo->u1CurrCxtType == VCM_L2_L3_CONTEXT))
        {
            *pu4NextContextId = pVcInfo->u4VcNum;
            VCM_UNLOCK ();
            return VCM_SUCCESS;
        }
        Dummy.u4VcNum = pVcInfo->u4VcNum;
        pVcInfo = VcmGetNextVc (&Dummy);
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmIsSwitchExist                                   */
/*                                                                           */
/*     DESCRIPTION      : This function check whether the entry is present   */
/*                        for the correponding Switch-name. if yes it will   */
/*                        return the Context-Id of the Switch.               */
/*                                                                           */
/*     INPUT            : pu1Alias   - Name of the Switch.                   */
/*                                                                           */
/*     OUTPUT           : pu4VcNum   - Context-Id of the switch              */
/*                                                                           */
/*     RETURNS          : VCM_TRUE / VCM_FALSE                               */
/*                                                                           */
/*****************************************************************************/
INT4
VcmIsSwitchExist (UINT1 *pu1Alias, UINT4 *pu4VcNum)
{
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo *pTempVcInfo = NULL;
    INT4                i4Len = 0;

    VCM_LOCK ();

    pVcInfo = VcmGetFirstVc ();

    if (pVcInfo == NULL)
    {
        VCM_UNLOCK ();
        return VCM_FALSE;
    }

    while (pVcInfo != NULL)
    {
        i4Len =
            (VCM_STRLEN (pu1Alias) > VCM_STRLEN (pVcInfo->au1Alias)) ?
            (INT4) VCM_STRLEN (pu1Alias) : (INT4) VCM_STRLEN (pVcInfo->
                                                              au1Alias);
        if (VCM_MEMCMP (pu1Alias, pVcInfo->au1Alias, i4Len) == 0)
        {
            if ((pVcInfo->u1CurrCxtType == VCM_L2_CONTEXT)
                || (pVcInfo->u1CurrCxtType == VCM_L2_L3_CONTEXT))
            {
                *pu4VcNum = pVcInfo->u4VcNum;
                VCM_UNLOCK ();
                return VCM_TRUE;
            }
        }
        pTempVcInfo = pVcInfo;

        pVcInfo = VcmGetNextVc (pTempVcInfo);
    }
    VCM_UNLOCK ();
    return VCM_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmIsVrfExist                                      */
/*                                                                           */
/*     DESCRIPTION      : This function check whether the entry is present   */
/*                        for the correponding Switch-name. if yes it will   */
/*                        return the Context-Id of the Switch.               */
/*                                                                           */
/*     INPUT            : pu1Alias   - Name of the Switch.                   */
/*                                                                           */
/*     OUTPUT           : pu4VcNum   - Context-Id of the switch              */
/*                                                                           */
/*     RETURNS          : VCM_TRUE / VCM_FALSE                               */
/*                                                                           */
/*****************************************************************************/
INT4
VcmIsVrfExist (UINT1 *pu1Alias, UINT4 *pu4VcNum)
{
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo *pTempVcInfo = NULL;
    INT4                i4Len = 0;

    VCM_LOCK ();

    pVcInfo = VcmGetFirstVc ();

    if (pVcInfo == NULL)
    {
        VCM_UNLOCK ();
        return VCM_FALSE;
    }

    while (pVcInfo != NULL)
    {
        i4Len =
            (VCM_STRLEN (pu1Alias) > VCM_STRLEN (pVcInfo->au1Alias)) ?
            (INT4) VCM_STRLEN (pu1Alias) : (INT4) VCM_STRLEN (pVcInfo->
                                                              au1Alias);
        if (VCM_MEMCMP (pu1Alias, pVcInfo->au1Alias, i4Len) == 0)
        {
            if ((pVcInfo->u1CurrCxtType == VCM_L3_CONTEXT) ||
                (pVcInfo->u1CurrCxtType == VCM_L2_L3_CONTEXT))
            {
                *pu4VcNum = pVcInfo->u4VcNum;
                VCM_UNLOCK ();
                return VCM_TRUE;
            }
            /* Context name is unique. So returning failure */
            VCM_UNLOCK ();
            return VCM_FALSE;
        }
        pTempVcInfo = pVcInfo;

        pVcInfo = VcmGetNextVc (pTempVcInfo);
    }
    VCM_UNLOCK ();
    return VCM_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetAliasName                                    */
/*                                                                           */
/*     DESCRIPTION      : This function will return the alias name of the    */
/*                        given context.                                     */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : pu1Alias       - Switch Alias Name.                */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

INT4
VcmGetAliasName (UINT4 u4ContextId, UINT1 *pu1Alias)
{
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo Dummy;

    Dummy.u4VcNum = u4ContextId;

    VCM_LOCK ();

    if (NULL != (pVcInfo = VcmFindVcEntry (&Dummy)))
    {
        VCM_MEMCPY (pu1Alias, pVcInfo->au1Alias, VCM_ALIAS_MAX_LEN);

        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmUnMapPortFromContext                            */
/*                                                                           */
/*     DESCRIPTION      : This function will unmap the given port from the   */
/*                        given context                                      */
/*                                                                           */
/*     INPUT            : u4IfIndex      - Port Identifier                   */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

INT4
VcmUnMapPortFromContext (UINT4 u4IfIndex)
{
    tVcmIfMapEntry      VcmIfMapEntry;
    UINT4               u4ContextId = 0;

    if (SISP_IS_LOGICAL_PORT (u4IfIndex) == VCM_TRUE)
    {
        if (VcmGetIfMapEntry (u4IfIndex, &VcmIfMapEntry) == VCM_FAILURE)
        {
            return VCM_FAILURE;
        }
        if (VcmSispDeleteSispPort (VcmIfMapEntry.u4PhyIfIndex,
                                   VcmIfMapEntry.u4VcNum) == VCM_FAILURE)
        {
            return VCM_FAILURE;
        }
    }
    else
    {
        if (VcmGetIfMapVcId (u4IfIndex, &u4ContextId) != VCM_SUCCESS)
        {
            return VCM_FAILURE;
        }

#ifdef NPAPI_WANTED
        VcmUnMapPortFromContextInHw (u4ContextId, u4IfIndex);
#endif
        if (FAILURE == VcmDeleteIfMapEntry (u4IfIndex))
        {
            return VCM_FAILURE;
        }
        VcmDecrementPortCount (u4ContextId);
    }
    return VCM_SUCCESS;
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmMapPortToContextInHw                            */
/*                                                                           */
/*     DESCRIPTION      : This function will map the given port to the       */
/*                        given context in the hardware                      */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                        u4IfIndex      - Port Identifier                   */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

INT4
VcmMapPortToContextInHw (UINT4 u4ContextId, UINT4 u4IfIndex)
{
    /* Mapping the Port to Context in Hardware */
    if (VcmUpdatePortToContextInHw (u4ContextId, u4IfIndex, VCM_TRUE)
        != VCM_SUCCESS)
    {
        return VCM_FAILURE;
    }

    /* Map all the Sisp ports of physical port to the hardware */
    if (VcmSispUpdateSispPortsInHw (u4IfIndex, VCM_TRUE) != VCM_SUCCESS)
    {
        return VCM_FAILURE;
    }

    return VCM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmUnMapPortFromContextInHw                        */
/*                                                                           */
/*     DESCRIPTION      : This function will unmap the given port from the   */
/*                        given context in the hardware                      */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                        u4IfIndex      - Port Identifier                   */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

INT4
VcmUnMapPortFromContextInHw (UINT4 u4ContextId, UINT4 u4IfIndex)
{
    if (VCM_NODE_STATUS () == VCM_RED_ACTIVE)
    {
        if (VcmFsVcmHwUnMapPortFromContext (u4ContextId, u4IfIndex) ==
            FNP_FAILURE)
        {
            return VCM_FAILURE;
        }
    }
    if (VcmSispUpdateSispPortsInHw (u4IfIndex, VCM_FALSE) != VCM_SUCCESS)
    {
        return VCM_FAILURE;
    }
    return VCM_SUCCESS;
}
#endif

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetIfIndexFromLocalPort                         */
/*                                                                           */
/*     DESCRIPTION      : This function will return the Interface Index      */
/*                        from the given Localport and Context-Id.           */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                        u2LocalPort    - LocalPort.                        */
/*                                                                           */
/*     OUTPUT           : pu4IfIndex     - Interface Index.                  */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

INT4
VcmGetIfIndexFromLocalPort (UINT4 u4ContextId, UINT2 u2LocalPort,
                            UINT4 *pu4IfIndex)
{
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo Dummy;
    tVcPortEntry       *pTempPort = NULL;

    Dummy.u4VcNum = u4ContextId;

    VCM_LOCK ();

    pVcInfo = VcmFindVcEntry (&Dummy);

    if (NULL == pVcInfo)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC | VCM_OS_RESOURCE_TRC,
                 "VcmGetIfIndexFromLocalPort: Invalid Context-Id\n");
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }

    TMO_SLL_Scan (&pVcInfo->VcPortList, pTempPort, tVcPortEntry *)
    {
        if (pTempPort->pIf->u2HlPortId == u2LocalPort)
        {
            *pu4IfIndex = pTempPort->pIf->u4IfIndex;
            VCM_UNLOCK ();
            return VCM_SUCCESS;
        }
    }

    VCM_TRC (VCM_ALL_FAILURE_TRC | VCM_OS_RESOURCE_TRC,
             "VcmGetIfIndexFromLocalPort: LocalPort not found\n");
    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmIsVcExist                                       */
/*                                                                           */
/*     DESCRIPTION      : This function will return, whether any context     */
/*                        with the given Context ID exists or not. The       */
/*                        context can be either an L2 context, an L3 context */
/*                        or an L2_L3 context.                               */
/*                                                                           */
/*     INPUT            : u4ContextId - Context-Id                           */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VCM_TRUE / VCM_FALSE                               */
/*                                                                           */
/*****************************************************************************/
INT4
VcmIsVcExist (UINT4 u4ContextId)
{
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo Dummy;

    Dummy.u4VcNum = u4ContextId;

    VCM_LOCK ();
    pVcInfo = VcmFindVcEntry (&Dummy);
    if (pVcInfo != NULL)
    {
        VCM_UNLOCK ();
        return VCM_TRUE;
    }

    VCM_UNLOCK ();
    return VCM_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmIsL2VcExist                                     */
/*                                                                           */
/*     DESCRIPTION      : This function will return true if the given        */
/*                        context ID is an L2 or an L2_L3 context.  This     */
/*                        function will return false otherwise.              */
/*                                                                           */
/*     INPUT            : u4ContextId - Context-Id                           */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VCM_TRUE  - If the given context exists, and the   */
/*                                    context type is of either L2 or L2_L3  */
/*                        VCM_FALSE - (*) If the given context does not      */
/*                                        exists                             */
/*                                    (*) Context exists, but if its of type */
/*                                        L3                                 */
/*                                                                           */
/*****************************************************************************/
INT4
VcmIsL2VcExist (UINT4 u4ContextId)
{
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo Dummy;

    Dummy.u4VcNum = u4ContextId;

    VCM_LOCK ();

    pVcInfo = VcmFindVcEntry (&Dummy);

    if ((pVcInfo != NULL) && ((pVcInfo->u1CurrCxtType == VCM_L2_CONTEXT) ||
                              (pVcInfo->u1CurrCxtType == VCM_L2_L3_CONTEXT)))
    {
        VCM_UNLOCK ();
        return VCM_TRUE;
    }

    VCM_UNLOCK ();
    return VCM_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmCfaCreateIfaceMapping                           */
/*                                                                           */
/*     DESCRIPTION      : This function will create the interface mapping.   */
/*                                                                           */
/*     INPUT            : u4ContextId - Context Identifier                   */
/*                        u4IfIndex   - Interface Index                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
VcmCfaCreateIfaceMapping (UINT4 u4ContextId, UINT4 u4IfIndex)
{
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo DummyVc;
    tVcmIfMapEntry     *pIfMapEntry = NULL;
    tVcmIfMapEntry      Dummy;
    UINT2               u2HlPortId = 0;

    VCM_LOCK ();

    Dummy.u4IfIndex = u4IfIndex;

    if (NULL != VcmFindIfMapEntry (&Dummy))
    {
        VCM_TRC (VCM_OS_RESOURCE_TRC,
                 "VcmCfaCreateIfaceMapping : Interface already exists\n");
        VCM_UNLOCK ();
        return;
    }

    DummyVc.u4VcNum = u4ContextId;

    if (NULL == (pVcInfo = VcmFindVcEntry (&DummyVc)))
    {
        VCM_TRC (VCM_OS_RESOURCE_TRC,
                 "VcmCfaCreateIfaceMapping : Context not exists\n");
        VCM_UNLOCK ();
        return;
    }
    else
    {
        /* Issue: No of S-Channel interfaces on the SI context are not
         *        created more than 8.
         * Fix : VcmGetL2ModeExt = SI_MODE when context as 1. so this 
         *      check has to be by passed */
        if ((pVcInfo->u2PortCount == L2IWF_MAX_PORTS_PER_CONTEXT) &&
            (VcmGetL2ModeExt () == VCM_MI_MODE))
        {
            VCM_TRC (VCM_OS_RESOURCE_TRC,
                     "VcmCfaCreateIfaceMapping : Already reached the"
                     "Maximum number of Ports mapped\n");
            VCM_UNLOCK ();
            return;
        }
    }

    pIfMapEntry = VcmRowCreate (u4IfIndex);

    if (pIfMapEntry == NULL)
    {
        VCM_TRC (VCM_OS_RESOURCE_TRC,
                 "VcmCfaCreateIfaceMapping : Interface creation failed\n");
        VCM_UNLOCK ();
        return;
    }

    pIfMapEntry->u4VcNum = u4ContextId;

    if (VcmAddToVcPortList (pIfMapEntry) == FAILURE)
    {
        VCM_TRC (VCM_OS_RESOURCE_TRC, "VcmCfaCreateIfaceMapping : "
                 "Adding the port to Context failed\n");
        VCM_UNLOCK ();
        return;
    }

    u2HlPortId = pIfMapEntry->u2HlPortId;
    if (u2HlPortId == 0)
    {
        pIfMapEntry->u1RowStatus = NOT_READY;
    }
    else
    {
        pIfMapEntry->u1RowStatus = ACTIVE;
    }
    VCM_UNLOCK ();
    /* Increment the number of ports mapped to this context */
    VcmIncrementPortCount (u4ContextId);

    if (VCM_NODE_STATUS () == VCM_RED_ACTIVE)
    {
        if (VcmRedSendSyncMessages (u4IfIndex, u2HlPortId, VCM_RED_IFMAP)
            != VCM_SUCCESS)
        {
            VCM_TRC (VCM_ALL_FAILURE_TRC | VCM_CONTROL_PATH_TRC,
                     "VCMRED: Vcm Send Sync Message to Standby Failed\n");
        }
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmCfaCreateIPIfaceMapping                         */
/*                                                                           */
/*     DESCRIPTION      : This function will is called from CFA when an IP   */
/*                        interface is created. This function maps the       */
/*                        interface to the default context and create an     */
/*                        entry in the IfMap table                           */
/*                                                                           */
/*     INPUT            : u4ContextId - Virtual context identifier           */
/*                        u4IfIndex   - Interface Index                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmCfaCreateIPIfaceMapping (UINT4 u4ContextId, UINT4 u4IfIndex)
{
    tVirtualContextInfo DummyVc;
    tVcmIfMapEntry      Dummy;
    tVcmIfMapEntry     *pIfMapEntry = NULL;
    tVirtualContextInfo *pVcInfo = NULL;
    UINT1               u1IfType = 0;
    UINT1               u1BridgedIfStatus = CFA_DISABLED;

    CfaGetIfaceType (u4IfIndex, &u1IfType);
    CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgedIfStatus);
    VCM_LOCK ();

    DummyVc.u4VcNum = u4ContextId;
    pVcInfo = VcmFindVcEntry (&DummyVc);
    if (pVcInfo == NULL)
    {
        /* Context does not exist */
        VCM_TRC (VCM_OS_RESOURCE_TRC,
                 "VcmCfaCreateIfaceMapping : Context not exists\n");
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }

    Dummy.u4IfIndex = u4IfIndex;

    if (NULL != (pIfMapEntry = VcmFindIfMapEntry (&Dummy)))
    {
        /* With L2SI, mapping will be present for CFA_ENET port. 
         * Need to convert the mapping to L3 mapping. */
        if ((u1IfType == CFA_ENET) || (u1IfType == CFA_LAGG))
        {
            /* Get the context to which the port is mapped and remove
             * the port from PortList. Then add to IpIfMap List.
             */
            VcmDeleteFromVcPortList (pIfMapEntry);
            pIfMapEntry->u4VcNum = u4ContextId;
            pIfMapEntry->u4CfgVcNum = u4ContextId;
            pIfMapEntry->u4L2ContextId = VCM_DEFAULT_CONTEXT;
            if (VcmAddToIpIfList (u4ContextId, pIfMapEntry) == VCM_FAILURE)
            {
                VCM_TRC (VCM_OS_RESOURCE_TRC, "VcmCfaCreateIfaceMapping : "
                         "Adding the interface to context's IpIfList failed\n");
                VCM_UNLOCK ();
                return VCM_FAILURE;
            }
            /* Send MSR trigger for the context id updation. */
            VCM_UNLOCK ();
            return VCM_SUCCESS;
        }
        else
        {
            /* Context does not exist */
            VCM_TRC (VCM_OS_RESOURCE_TRC,
                     "VcmCfaCreateIfaceMapping : Interface already exists\n");
            VCM_UNLOCK ();
            return VCM_FAILURE;
        }
    }

    pIfMapEntry = VcmRowCreate (u4IfIndex);

    if (pIfMapEntry == NULL)
    {
        VCM_TRC (VCM_OS_RESOURCE_TRC,
                 "VcmCfaCreateIfaceMapping : Interface creation failed\n");
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }

    pIfMapEntry->u4VcNum = u4ContextId;
    pIfMapEntry->u4CfgVcNum = u4ContextId;
    pIfMapEntry->u4L2ContextId = VCM_DEFAULT_CONTEXT;
    pIfMapEntry->u1RowStatus = NOT_READY;
    VCM_UNLOCK ();
    return VCM_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmCfaMakeIPIfaceMappingActive                     */
/*                                                                           */
/*     DESCRIPTION      : This function will be called from CFA when an IP   */
/*                        interface is made active for the first time.       */
/*                        This function makes the IPIfMap entry active and   */
/*                        add the entry to L2context+vlanId to interface     */
/*                        mapping RBTree                                     */
/*                                                                           */
/*     INPUT            : u4IfIndex - Interface Index                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmCfaMakeIPIfaceMappingActive (UINT4 u4ContextId, UINT4 u4IfIndex)
{
    tVcmIfMapEntry      Dummy;
    tVcmIfMapEntry     *pIfMapEntry = NULL;
    tCfaIfInfo          CfaIfInfo;
    UINT1               u1IfType = 0;
#ifdef NPAPI_WANTED
    tVrMapAction        VrMapAction;
    tFsNpVcmVrMapInfo   VcmVrMapInfo;
#endif

    CfaGetIfaceType (u4IfIndex, &u1IfType);

    if ((u1IfType == CFA_L3IPVLAN) || (u1IfType == CFA_ENET) ||
        (u1IfType == CFA_LOOPBACK))
    {
        MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
        if (CfaGetIfInfo (u4IfIndex, &CfaIfInfo) == CFA_FAILURE)
        {
            return VCM_FAILURE;
        }
    }

    VCM_LOCK ();

    Dummy.u4IfIndex = u4IfIndex;

    if (NULL == (pIfMapEntry = VcmFindIfMapEntry (&Dummy)))
    {
        /* Mapping doesnot exists, so cannot be made active */
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }

    if (pIfMapEntry->u1RowStatus == ACTIVE)
    {
        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    pIfMapEntry->u1RowStatus = ACTIVE;

    /* Add this IfMap entry to L2CxtAndVlanIdToIPIfaceMapTable only if this 
     * interface is an IVR interface */
    if (u1IfType == CFA_L3IPVLAN)
    {
        pIfMapEntry->u2VlanId = CfaIfInfo.u2VlanId;
        if (VcmAddL2CxToIPIfMapEntry (pIfMapEntry) == VCM_FAILURE)
        {
            VCM_UNLOCK ();
            return VCM_FAILURE;
        }
    }

    if (VcmAddToIpIfList (u4ContextId, pIfMapEntry) == VCM_FAILURE)
    {
        VCM_TRC (VCM_OS_RESOURCE_TRC, "VcmCfaCreateIfaceMapping : "
                 "Adding the interface to context's IpIfList failed\n");
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }

#ifdef NPAPI_WANTED
    MEMSET (&VcmVrMapInfo, 0, sizeof (tFsNpVcmVrMapInfo));
    VrMapAction = NP_HW_MAP_INVALID;

    if (CfaIfInfo.u1IfType == CFA_L3IPVLAN)
    {
        VrMapAction = NP_HW_MAP_INTF_TO_VR;
        VcmVrMapInfo.u2VlanId = pIfMapEntry->u2VlanId;
        VcmVrMapInfo.u4L2CxtId = pIfMapEntry->u4L2ContextId;
    }
    else if ((CfaIfInfo.u1IfType == CFA_ENET) ||
             (CfaIfInfo.u1IfType == CFA_LOOPBACK))
    {
        VrMapAction = NP_HW_MAP_RPORT_TO_VR;
    }
    VcmVrMapInfo.u4VrfId = pIfMapEntry->u4VcNum;
    VcmVrMapInfo.u4IfIndex = u4IfIndex;
    if (VCM_NODE_STATUS () == VCM_RED_ACTIVE)
    {
        VcmFsVcmHwMapVirtualRouter (VrMapAction, &VcmVrMapInfo);
    }
#endif

    VCM_UNLOCK ();
    return VCM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmCfaDeleteIPIfaceMapping                         */
/*                                                                           */
/*     DESCRIPTION      : This function is called from CFA when an IP        */
/*                        interface is deleted. This function deletes the    */
/*                        the IfMap entry from IfMap table and frees the     */
/*                        memory.                                            */
/*                                                                           */
/*     INPUT            : u4IfIndex   - Interface Index                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmCfaDeleteIPIfaceMapping (UINT4 u4IfIndex, UINT1 u1Status)
{
    tVirtualContextInfo DummyVc;
    tVcmIfMapEntry      Dummy;
    tCfaIfInfo          CfaIfInfo;
    tVirtualContextInfo *pVcInfo = NULL;
    tVcmIfMapEntry     *pIfMapEntry = NULL;
    tVcmIpIfMapEntry   *pIpIfNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    INT1                i1Found = VCM_FALSE;

#ifdef NPAPI_WANTED
    tVrMapAction        VrMapAction;
    tFsNpVcmVrMapInfo   VcmVrMapInfo;
#endif
#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    tLnxVrfEventInfo  LnxVrfEventInfo;
    tLnxVrfIfInfo *pLnxVrfIfInfo = NULL;
    INT4              i4SockFd = -1;
    INT4              i4IpPort = 0;
    UINT1             au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    MEMSET (&LnxVrfEventInfo, 0, sizeof (LnxVrfEventInfo));

#endif


    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
    VCM_LOCK ();

    Dummy.u4IfIndex = u4IfIndex;

    if (NULL == (pIfMapEntry = VcmFindIfMapEntry (&Dummy)))
    {
        /* Mapping does not exists */
        VCM_TRC (VCM_OS_RESOURCE_TRC,
                 "VcmCfaDeleteIPIfaceMapping : Interface does not exists\n");
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }

    DummyVc.u4VcNum = pIfMapEntry->u4VcNum;
    if (NULL == (pVcInfo = VcmFindVcEntry (&DummyVc)))
    {
        /* Context does not exist */
        VCM_TRC (VCM_OS_RESOURCE_TRC,
                 "VcmCfaCreateIfaceMapping : Context not exists\n");
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }

    if (CfaGetIfInfo (u4IfIndex, &CfaIfInfo) == CFA_FAILURE)
    {
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }

    /* Delete the interface mapping from RBTree. */
    if (u1Status == CFA_TRUE)
    {
#ifdef NPAPI_WANTED
        MEMSET (&VcmVrMapInfo, 0, sizeof (tFsNpVcmVrMapInfo));
        VrMapAction = NP_HW_MAP_INVALID;

        if ((CfaIfInfo.u1IfType == CFA_ENET) ||
            (CfaIfInfo.u1IfType == CFA_LOOPBACK))
        {
            VrMapAction = NP_HW_UNMAP_RPORT_TO_VR;
        }
        else if (CfaIfInfo.u1IfType == CFA_L3IPVLAN)
        {
            VrMapAction = NP_HW_UNMAP_INTF_TO_VR;
            VcmVrMapInfo.u2VlanId = pIfMapEntry->u2VlanId;
        }
        VcmVrMapInfo.u4VrfId = pIfMapEntry->u4VcNum;
        VcmVrMapInfo.u4IfIndex = u4IfIndex;
        if (VCM_NODE_STATUS () == VCM_RED_ACTIVE)
        {
            if (VcmFsVcmHwMapVirtualRouter (VrMapAction, &VcmVrMapInfo) ==
                FNP_FAILURE)
            {
                VCM_UNLOCK ();
                return VCM_FAILURE;
            }
        }
#endif
#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
        if(pIfMapEntry->u4VcNum != VCM_DEFAULT_CONTEXT)
        {
            pLnxVrfIfInfo = LnxVrfIfInfoGet(CfaIfInfo.au1IfName);
            if (pLnxVrfIfInfo != NULL)
            {
                /*Cfa Socket parameters filled*/
                LnxVrfEventInfo.i4Sockdomain = PF_PACKET;
                LnxVrfEventInfo.i4SockType = SOCK_RAW;
                LnxVrfEventInfo.i4SockProto = htons (ETH_P_ALL);
                LnxVrfEventInfo.u4IfIndex = u4IfIndex;
                LnxVrfEventInfo.u1MsgType = LNX_VRF_IF_UNMAP_CONTEXT;
                LnxVrfEventInfo.u4ContextId = pIfMapEntry->u4VcNum ;
                LnxVrfSockLock();
                if(LnxVrfEventHandling(&LnxVrfEventInfo,&i4SockFd) == NETIPV4_FAILURE)
                {
                    LnxVrfSockUnLock();
                    VCM_UNLOCK ();
                    return VCM_FAILURE;
                }
                LnxVrfSockUnLock();

                /* Set the appropriate falgs for the interface using the socket*/
                if((CfaIfInfo.u1IfType != CFA_L3IPVLAN) &&( i4SockFd != -1) )
                {
                    VCM_UNLOCK();
                    CFA_UNLOCK();
                    CfaVrfUpdateInterfaceSockInfo (u4IfIndex, i4SockFd);
                    CFA_LOCK();
                    VCM_LOCK();
                }
                else
                {
                    close(i4SockFd);
                }
                MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                i4IpPort = CfaGetIfIpPort (u4IfIndex);
                VCM_UNLOCK ();
                CFA_UNLOCK();
                RtmActOnStaticRoutesForIfDeleteInCxt (pIfMapEntry->u4VcNum, (UINT4) i4IpPort);
                CFA_LOCK();
                VCM_LOCK ();
            }
        }
#endif

        VcmRemoveIfMapEntry (pIfMapEntry);
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVcIfRowStatus, 0, TRUE,
                              VcmConfLock, VcmConfUnLock, 1, SNMP_SUCCESS);
        SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", u4IfIndex, DESTROY));

    }
    else
    {
        pIfMapEntry->u1RowStatus = NOT_READY;
    }

    if (CfaIfInfo.u1IfType == CFA_L3IPVLAN)
    {
        VcmRemoveL2CxToIPIfMapEntry (pIfMapEntry);
    }

    /* Remove the interface from context's list. */
    if (u1Status == CFA_TRUE)
    {
#ifdef LNXIP4_WANTED
        gapVcmIpIfMapEntry[(CfaIfInfo.u4IndexMgrPort) - VCM_ONE] = NULL;
#else
        gapVcmIpIfMapEntry[CfaIfInfo.i4IpPort] = NULL;
#endif
    }

    TMO_DLL_Scan (&pVcInfo->VcIPIntfList, pIpIfNode, tVcmIpIfMapEntry *)
    {
        if (pIpIfNode->pIfMapEntry->u4IfIndex == u4IfIndex)
        {
            i1Found = VCM_TRUE;
            break;
        }
    }

    if (i1Found == VCM_TRUE)
    {
        TMO_DLL_Delete (&pVcInfo->VcIPIntfList, &pIpIfNode->NextIfEntry);
        VCM_RELEASE_IPIFENTRY_MEMBLK (pIpIfNode);
    }

    if (u1Status == CFA_TRUE)
    {
        VCM_RELEASE_IFMAPENTRY_MEMBLK (pIfMapEntry);
    }

    if (VCM_NODE_STATUS () == VCM_RED_ACTIVE)
    {
        if (VcmRedSendSyncMessages (u4IfIndex, 0,
                    VCM_RED_CLEAR_SYNCUP_DATA) !=
                VCM_SUCCESS)

        {
            VCM_TRC (VCM_ALL_FAILURE_TRC | VCM_CONTROL_PATH_TRC,
                    "VCMRED: Vcm Send Sync Message to Standby Failed\n");
        }
    }

    VCM_UNLOCK ();
    return VCM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmUpdateIpPortNumber                              */
/*                                                                           */
/*     DESCRIPTION      : This functions is called to update the IP port     */
/*                        number of the specified interface                  */
/*                                                                           */
/*     INPUT            : u4IpIfIndex   - IP Port Number                     */
/*                                                                           */
/*     OUTPUT           : Nonde                                              */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS  / VCM_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
VcmUpdateIpPortNumber (UINT4 u4CfaIfIndex, UINT4 u4IpPortNum)
{
    tVcmIfMapEntry      Dummy;
    tVcmIfMapEntry     *pIfMapEntry = NULL;
#ifdef LNXIP4_WANTED
    tCfaIfInfo          CfaIfInfo;
#endif

    Dummy.u4IfIndex = u4CfaIfIndex;

#ifdef LNXIP4_WANTED
    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    CfaGetIfInfo (u4CfaIfIndex, &CfaIfInfo);
#endif
    VCM_LOCK ();
    if (NULL == (pIfMapEntry = VcmFindIfMapEntry (&Dummy)))
    {
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }

#ifdef LNXIP4_WANTED
    gapVcmIpIfMapEntry[(CfaIfInfo.u4IndexMgrPort) - VCM_ONE] = pIfMapEntry;
    UNUSED_PARAM (u4IpPortNum);
#else
    gapVcmIpIfMapEntry[u4IpPortNum] = pIfMapEntry;
#endif

    VCM_UNLOCK ();
    return VCM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetContextInfoFromIpIfIndex                     */
/*                                                                           */
/*     DESCRIPTION      : This functions gets the context id associated      */
/*                        with the given IP Port Number.                     */
/*                                                                           */
/*     INPUT            : u4IpIfIndex   - IP Port Number                     */
/*                                                                           */
/*     OUTPUT           : *pu4CxtId - Context id of the interface            */
/*                                    having port number as u4IfIndex        */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS    - If interface is found and the     */
/*                                        context is associated              */
/*                                        with that interface is returned    */
/*                        VCM_FAILURE    - Otherwise                         */
/*                                                                           */
/*****************************************************************************/

INT4
VcmGetContextInfoFromIpIfIndex (UINT4 u4IpIfIndex, UINT4 *pu4CxtId)
{
 
#ifdef LNXIP4_WANTED
    UINT4               u4CfaIfIndex = CFA_INVALID_INDEX;
    tCfaIfInfo          CfaIfInfo;
    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
    
#endif

    VCM_LOCK ();
#ifdef LNXIP4_WANTED
    if (NetIpv4GetCfaIfIndexFromPort (u4IpIfIndex, &u4CfaIfIndex) 
            == VCM_FAILURE)
    {
            return VCM_FAILURE;
    }
    CfaGetIfInfo (u4CfaIfIndex, &CfaIfInfo);
    if ((CfaIfInfo.u4IndexMgrPort < VCM_MAX_IP_INTERFACES)
            && (NULL != gapVcmIpIfMapEntry[(CfaIfInfo.u4IndexMgrPort) - VCM_ONE]))
    {
            *pu4CxtId = gapVcmIpIfMapEntry[(CfaIfInfo.u4IndexMgrPort) - VCM_ONE]->u4VcNum;
            VCM_UNLOCK ();
            return VCM_SUCCESS;
    }
#else

    if ((u4IpIfIndex < VCM_MAX_IP_INTERFACES)
        && (NULL != gapVcmIpIfMapEntry[u4IpIfIndex]))
    {
        *pu4CxtId = gapVcmIpIfMapEntry[u4IpIfIndex]->u4VcNum;
        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }
#endif
    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetContextIdFromCfaIfIndex                      */
/*                                                                           */
/*     DESCRIPTION      : This functions gets the context id associated      */
/*                        with the given cfa ifIndex                         */
/*                                                                           */
/*     INPUT            : u4CfaIfIndex  - Cfa interface index                */
/*                                                                           */
/*     OUTPUT           : *pu4CxtId - Context id of the interface            */
/*                                    having cfa IfIndex as u4CfaIfIndex     */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS    - If interface is found and the     */
/*                                        context is associated              */
/*                                        with that interface is returned    */
/*                        VCM_FAILURE    - Otherwise                         */
/*                                                                           */
/*****************************************************************************/

INT4
VcmGetContextIdFromCfaIfIndex (UINT4 u4CfaIfIndex, UINT4 *pu4CxtId)
{
    tVcmIfMapEntry      Dummy;
    tVcmIfMapEntry     *pIfMapEntry = NULL;

    Dummy.u4IfIndex = u4CfaIfIndex;

    VCM_LOCK ();
    if (NULL == (pIfMapEntry = VcmFindIfMapEntry (&Dummy)))
    {
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }

    *pu4CxtId = pIfMapEntry->u4VcNum;
    VCM_UNLOCK ();
    return VCM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetL2CxtIdForIpIface                            */
/*                                                                           */
/*     DESCRIPTION      : This functions gets the l2 context to which the    */
/*                        specified interace is mapped.                      */
/*                                                                           */
/*     INPUT            : u4IfIndex   - Cfa interface index                  */
/*                                                                           */
/*     OUTPUT           : *pu4L2CxtId - L2 context to which the interface    */
/*                                      is mapped.                           */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS    - If interface is found and the     */
/*                                        context is associated              */
/*                                        with that interface is returned    */
/*                        VCM_FAILURE    - Otherwise                         */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetL2CxtIdForIpIface (UINT4 u4IfIndex, UINT4 *pu4L2CxtId)
{
    tVcmIfMapEntry     *pIfMapEntry = NULL;
    tVcmIfMapEntry      Dummy;

    Dummy.u4IfIndex = u4IfIndex;

    VCM_LOCK ();
    if (NULL == (pIfMapEntry = VcmFindIfMapEntry (&Dummy)))
    {
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }

    *pu4L2CxtId = pIfMapEntry->u4L2ContextId;
    VCM_UNLOCK ();
    return VCM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmRegisterHLProtocol                              */
/*                                                                           */
/*     DESCRIPTION      : This function is used by Higher Layer Protocols    */
/*                        to register with VCM for getting notification      */
/*                        on context deletion and interface to               */
/*                        context Mapping deletion.                          */
/*                                                                           */
/*     INPUT            : pRegInfo  - Information about the registering      */
/*                                    protocol and the call back functions.  */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS    - If registration is done           */
/*                                        successfully                       */
/*                        VCM_FAILURE    - Otherwise                         */
/*                                                                           */
/*****************************************************************************/

INT4
VcmRegisterHLProtocol (tVcmRegInfo * pVcmRegInfo)
{
    tVcmHLProtoRegEntry *pVcmProtoRegEntry = NULL;
    tVcmHLProtoRegEntry *pRegEntry = NULL;
    tVcmHLProtoRegEntry *pPrevRegEntry = NULL;
    UINT1               u1PrevFound = FALSE;

    VCM_LOCK ();

    /* Check if the protocol is already registered. If so, just increment the 
     * counter and return. Else add an entry for this protocol to the registration
     * table
     */
    TMO_SLL_Scan (&(gVcmGlobals.VcmProtoRegTable), pRegEntry,
                  tVcmHLProtoRegEntry *)
    {
        if (pRegEntry->u1ProtoId == pVcmRegInfo->u1ProtoId)
        {
            pRegEntry->u4CxtCount++;
            VCM_UNLOCK ();
            return VCM_SUCCESS;
        }
        if (pRegEntry->u1ProtoId < pVcmRegInfo->u1ProtoId)
        {
            u1PrevFound = TRUE;
            pPrevRegEntry = pRegEntry;
        }
        else
        {
            break;
        }
    }

    /* New entry needs to be added to registration table. Allocate memory and
     * add the registration entry
     */
    pVcmProtoRegEntry = VCM_ALLOCATE_PROTOREG_ENTRY_MEMBLK ();

    if (pVcmProtoRegEntry == NULL)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC,
                 "vcmapi.c: Regtable entry mem alloc failed \n");
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }

    TMO_SLL_Init_Node (&pVcmProtoRegEntry->nextProtoRegNode);
    pVcmProtoRegEntry->pIfMapChngAndCxtChng = pVcmRegInfo->pIfMapChngAndCxtChng;
    pVcmProtoRegEntry->u4CxtCount = 1;
    pVcmProtoRegEntry->u1ProtoId = pVcmRegInfo->u1ProtoId;
    if (TMO_SLL_Count (&gVcmGlobals.VcmProtoRegTable) == 0)
    {
        /* Add the entry at first */
        TMO_SLL_Add (&gVcmGlobals.VcmProtoRegTable,
                     &pVcmProtoRegEntry->nextProtoRegNode);
        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    if (u1PrevFound == FALSE)
    {
        /* Entry to be added at first */
        TMO_SLL_Insert (&gVcmGlobals.VcmProtoRegTable,
                        &(gVcmGlobals.VcmProtoRegTable.Head),
                        &pVcmProtoRegEntry->nextProtoRegNode);
    }
    else
    {
        TMO_SLL_Insert (&gVcmGlobals.VcmProtoRegTable,
                        &pPrevRegEntry->nextProtoRegNode,
                        &pVcmProtoRegEntry->nextProtoRegNode);
    }

    VCM_UNLOCK ();
    return VCM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmDeRegisterHLProtocol                            */
/*                                                                           */
/*     DESCRIPTION      : This function De-Registers the Protocol            */
/*                        specified by the Application.                      */
/*                                                                           */
/*     INPUT            : u4ContextId - Context-Id                           */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS    - If De-registration is done         */
/*                                        successfully.                      */
/*                        VCM_FAILURE    - Otherwise                          */
/*                                                                           */
/*****************************************************************************/

INT4
VcmDeRegisterHLProtocol (UINT1 u1ProtoId)
{
    tVcmHLProtoRegEntry *pRegEntry = NULL;

    VCM_LOCK ();

    VCM_SLL_SCAN (&gVcmGlobals.VcmProtoRegTable, pRegEntry,
                  tVcmHLProtoRegEntry *)
    {
        if (pRegEntry->u1ProtoId == u1ProtoId)
        {
            /* Protocol is deregistering. So decrement the count */
            pRegEntry->u4CxtCount--;
            break;
        }
    }

    if (pRegEntry == NULL)
    {
        /* This protocol is not registered. So return failure */
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }
    if (pRegEntry->u4CxtCount == 0)
    {
        /* Count is zero. All the protocol contexts have deregistered. So delete
         * the entry from registration table
         */
        VCM_SLL_DELETE (&gVcmGlobals.VcmProtoRegTable,
                        &pRegEntry->nextProtoRegNode);
        /* Release the memory */
        VCM_RELEASE_PROTOREG_ENTRY_MEMBLK (pRegEntry);
    }

    VCM_UNLOCK ();
    return VCM_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : VcmTestv2FsVcIfRowStatus
 *
 *    Description          : This function is called from PBB to test destroy 
 *                           of mapping of switch to  VIP rowstatus.
 *
 *    Input(s)             : None.
 *
 *    Output(s)            : None.
 *
 *    Returns              : None.     
 *
 *****************************************************************************/

INT4
VcmTestv2FsVcIfRowStatus (UINT4 *pu4ErrorCode, INT4 u4IfIndex, INT4 u4RowStatus)
{
    if (nmhTestv2FsVcIfRowStatus (pu4ErrorCode, u4IfIndex, u4RowStatus) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;

    }

}

/*****************************************************************************
 *
 *    Function Name        : VcmTestv2FsVcIfRowStatus
 *
 *    Description          : This function is called from PBB to set destroy 
 *                           of mapping of switch to  VIP rowstatus.
 *
 *    Input(s)             : None.
 *
 *    Output(s)            : None.
 *
 *    Returns              : None.     
 *
 *****************************************************************************/

INT4
VcmSetFsVcIfRowStatus (INT4 u4IfIndex, INT4 u4RowStatus)
{
    VCM_CONF_LOCK ();

    if (nmhSetFsVcIfRowStatus (u4IfIndex, u4RowStatus) == SNMP_FAILURE)
    {
        VCM_CONF_UNLOCK ();
        return SNMP_FAILURE;
    }
    else
    {
        VCM_CONF_UNLOCK ();
        return SNMP_SUCCESS;

    }
}

/*****************************************************************************
 *
 *    Function Name        : VcmTestFsVcId
 *
 *    Description          : This function is called from PBB to test the VC id  
 *                           mapping with Vip if index
 *
 *    Input(s)             : None.
 *
 *    Output(s)            : None.
 *
 *    Returns              : None.     
 *
 *****************************************************************************/
INT1
VcmTestFsVcId (UINT4 *pu4ErrorCode, INT4 i4FsVcmIfIndex, INT4 i4TestValFsVcId)
{
    if (nmhTestv2FsVcId (pu4ErrorCode, i4FsVcmIfIndex, i4TestValFsVcId) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;

    }
}

/*****************************************************************************
 *
 *    Function Name        : VcmSetFsVcId
 *
 *    Description          : This function is called from PBB to set the VC id  
 *                           mapping with Vip if index
 *
 *    Input(s)             : None.
 *
 *    Output(s)            : None.
 *
 *    Returns              : None.     
 *
 *****************************************************************************/

INT1
VcmSetFsVcId (INT4 i4FsVcmIfIndex, INT4 i4SetValFsVcId)
{
    VCM_CONF_LOCK ();
    if (nmhSetFsVcId (i4FsVcmIfIndex, i4SetValFsVcId) == SNMP_FAILURE)
    {
        VCM_CONF_UNLOCK ();
        return SNMP_FAILURE;
    }
    else
    {
        VCM_CONF_UNLOCK ();
        return SNMP_SUCCESS;

    }
}

/*****************************************************************************/
/* Function Name      : VcmGetContextPortList                                */
/*                                                                           */
/* Description        : This function is used to get the list of interfaces  */
/*                      mapped to the given context.                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Context - Id                           */
/*                                                                           */
/* Output(s)          : PortList    - PortList for the given context         */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS or VCM_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetContextPortList (UINT4 u4ContextId, tPortList PortList)
{
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo VcInfo;
    tVcPortEntry       *pPort = NULL;

    VCM_LOCK ();

    VcInfo.u4VcNum = u4ContextId;
    pVcInfo = VcmFindVcEntry (&VcInfo);

    if (pVcInfo == NULL)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC,
                 "VcmGetContextPortList: Get Context Entry FAILED... \n");
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }

    TMO_SLL_Scan (&pVcInfo->VcPortList, pPort, tVcPortEntry *)
    {
        OSIX_BITLIST_SET_BIT (PortList, pPort->pIf->u4IfIndex,
                              BRG_PORT_LIST_SIZE);
    }

    VCM_UNLOCK ();
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmGetCxtIpIfaceList                                 */
/*                                                                           */
/* Description        : This function is used to get the list of interfaces  */
/*                      mapped to the given context.                         */
/*                                                                           */
/* Input (s)          : u4ContextId - the context id                         */
/*                                                                           */
/* OutPut (s)         : pu4IfIndexArray - Array that holds the IP interfaces */
/*                      that are mapped to the specified context. First      */
/*                      element in the array is always the total number of   */
/*                      IP interfaces mapped to the contex                   */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS or VCM_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetCxtIpIfaceList (UINT4 u4ContextId, UINT4 *pu4IfIndexArray)
{
    tVirtualContextInfo VcInfo;
    tVirtualContextInfo *pVcInfo = NULL;
    tVcmIpIfMapEntry   *pIpIfMapEntry = NULL;
    UINT2               u2Var = 0;

    VCM_LOCK ();

    VcInfo.u4VcNum = u4ContextId;
    pVcInfo = VcmFindVcEntry (&VcInfo);

    if (pVcInfo == NULL)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC,
                 "VcmGetContextPortList: Get Context Entry FAILED... \n");
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }
    if (u2Var >= IPIF_MAX_LOGICAL_IFACES)
    {
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }
    pu4IfIndexArray[u2Var] = TMO_DLL_Count (&pVcInfo->VcIPIntfList);
    u2Var++;

    TMO_DLL_Scan (&pVcInfo->VcIPIntfList, pIpIfMapEntry, tVcmIpIfMapEntry *)
    {
        if (u2Var >= IPIF_MAX_LOGICAL_IFACES)
        {
            VCM_UNLOCK ();
            return VCM_FAILURE;
        }
        pu4IfIndexArray[u2Var++] = pIpIfMapEntry->pIfMapEntry->u4IfIndex;
    }

    VCM_UNLOCK ();
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSetL2CxtId                                        */
/*                                                                           */
/* Description        : This function is used to set the l2 context id. This */
/*                      function is called from CFA when IVR interface is    */
/*                      configured with l2 context id                        */
/*                                                                           */
/* Input (s)          : u4IfIndex - the interface index                      */
/*                      u4L2CxtId - the l2 context id to which the IVR       */
/*                                  iface is mapped.                         */
/*                                                                           */
/* OutPut (s)         : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS or VCM_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT1
VcmSetL2CxtId (UINT4 u4IfIndex, UINT4 u4L2CxtId)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    VCM_CONF_LOCK ();
    if (nmhSetFsVcL2ContextId (u4IfIndex, u4L2CxtId) == SNMP_FAILURE)
    {
        VCM_CONF_UNLOCK ();
        return VCM_FAILURE;
    }
    VCM_CONF_UNLOCK ();
    MEMSET (&SnmpNotifyInfo, 0, sizeof (SnmpNotifyInfo));
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsVcL2ContextId;
    SnmpNotifyInfo.u4OidLen = sizeof (FsVcL2ContextId) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", (INT4) u4IfIndex,
                      (INT4) u4L2CxtId));
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmTestL2CxtId                                       */
/*                                                                           */
/* Description        : This function is used to set the l2 context id. This */
/*                      function is called from CFA when IVR interface is    */
/*                      configured with l2 context id                        */
/*                                                                           */
/* Input (s)          : pu4ErrCode - Error code from nmhTest routine         */
/*                      u4IfIndex - the interface index                      */
/*                      u4L2CxtId - the l2 context id to which the IVR       */
/*                                  iface is mapped.                         */
/*                                                                           */
/* OutPut (s)         : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS or VCM_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT1
VcmTestL2CxtId (UINT4 *pu4ErrCode, UINT4 u4IfIndex, UINT4 u4L2CxtId)
{
    VCM_CONF_LOCK ();
    if (nmhTestv2FsVcL2ContextId
        (pu4ErrCode, (INT4) u4IfIndex, (INT4) u4L2CxtId) == SNMP_FAILURE)
    {
        VCM_CONF_UNLOCK ();
        return VCM_FAILURE;
    }
    VCM_CONF_UNLOCK ();
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmGetIfIndexFrmL2CxtIdAndVlanId                     */
/*                                                                           */
/* Description        : This function is used to get the Interface index     */
/*                      from L2 context Id and Vlan Id.                      */
/*                                                                           */
/* Input (s)          : u4L2ContextId - L2 context id                        */
/*                      u2VlanId  - VlanId                                   */
/*                                                                           */
/* OutPut (s)         : pu4IfIndex - Interface index                         */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS or VCM_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT1
VcmGetIfIndexFrmL2CxtIdAndVlanId (UINT4 u4L2ContextId, UINT2 u2VlanId,
                                  UINT4 *pu4IfIndex)
{
    tVcmIfMapEntry     *pIfMapEntry = NULL;
    tVcmIfMapEntry      Dummy;

    Dummy.u4L2ContextId = u4L2ContextId;
    Dummy.u2VlanId = u2VlanId;

    VCM_LOCK ();
    if (NULL == (pIfMapEntry = VcmFindL2CxtToIPIfMapEntry (&Dummy)))
    {
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }

    *pu4IfIndex = pIfMapEntry->u4IfIndex;
    VCM_UNLOCK ();
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmCliGetL2CxtNameForIpInterface                     */
/*                                                                           */
/* Description        : This function is used to get the L2 context name     */
/*                      for the specified IP interface. If the interface is  */
/*                      mapped to a non-default l2 context, function returns */
/*                      the context name.                                    */
/*                                                                           */
/* Input (s)          : u4IfIndex - Interface index                          */
/*                                                                           */
/* OutPut (s)         : pu1L2CxtName - L2 context Name                       */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS if the interface is mapped to a          */
/*                      non-default l2 context.                              */
/*                      VCM_FAILURE if IP interface is mapped to default     */
/*                      context or if mapping does not exist.                */
/*                                                                           */
/*****************************************************************************/
INT1
VcmCliGetL2CxtNameForIpInterface (UINT4 u4IfIndex, UINT1 *pu1L2CxtName)
{
    UINT4               u4L2CxtId = 0;

    if (VcmGetL2CxtIdForIpIface (u4IfIndex, &u4L2CxtId) == VCM_FAILURE)
    {
        return VCM_FAILURE;
    }

    if (u4L2CxtId == VCM_DEFAULT_CONTEXT)
    {
        return VCM_FAILURE;
    }

    if (VcmGetAliasName (u4L2CxtId, pu1L2CxtName) == VCM_FAILURE)
    {
        return VCM_FAILURE;
    }
    return VCM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmIsL3Interface                                   */
/*                                                                           */
/*     DESCRIPTION      : This function checks if the specified interface    */
/*                        is an l3 interface or l2 interface. This function  */
/*                        returns true if the interface is l3 interface.     */
/*                        Else it returns false.                             */
/*                                                                           */
/*     INPUT            : u4IfIndex      - Interface Index.                  */
/*                                                                           */
/*     RETURNS          : VCM_TRUE / VCM_FALSE                               */
/*                                                                           */
/*****************************************************************************/
INT4
VcmIsL3Interface (UINT4 u4IfIndex)
{
    tCfaIfInfo          CfaIfInfo;
    UINT1               u1IfType = 0;

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    if (CfaGetIfaceType (u4IfIndex, &u1IfType) == CFA_FAILURE)
    {
        return VCM_FALSE;
    }

    if ((u1IfType == CFA_L3IPVLAN) || (u1IfType == CFA_LOOPBACK) ||
        (u1IfType == CFA_TUNNEL) || (u1IfType == CFA_PPP) || 
        (u1IfType == CFA_L3SUB_INTF))
    {
        return VCM_TRUE;
    }

    if ((u1IfType == CFA_ENET) || (u1IfType == CFA_PSEUDO_WIRE)
        || (u1IfType == CFA_LAGG))
    {
        if (CfaGetIfInfo (u4IfIndex, &CfaIfInfo) != CFA_FAILURE)
        {
                   if( (u1IfType == CFA_ENET) && (L2IwfIsPortInPortChannel(u4IfIndex) == CFA_SUCCESS))
                   {
                       if( CfaIfInfo.u1BridgedIface != CFA_DISABLED)
                       {
                           return VCM_FALSE;
                       }
                   }
            if (CfaIfInfo.u1BridgedIface != CFA_ENABLED)
            {
                return VCM_TRUE;
            }
        }
    }

    return VCM_FALSE;
}

/*****************************************************************************/
/* Function Name      : VcmGetL2Mode                                         */
/*                                                                           */
/* Description        : This function is used to get the mode (SI / MI)      */
/*                      of the L2 switch                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SI_MODE or VCM_MI_MODE                           */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetL2Mode (VOID)
{

#ifdef MI_WANTED
    return VCM_MI_MODE;
#else
    return VCM_SI_MODE;
#endif
}

/* vivek */
/*****************************************************************************/
/* Function Name      : VcmGetL2ModeExt                                      */
/*                                                                           */
/* Description        : This function is used to get the mode (SI / MI)      */
/*                      of the L2 switch                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SI_MODE or VCM_MI_MODE                           */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetL2ModeExt (VOID)
{
#ifdef MI_WANTED
    if (SYS_DEF_MAX_NUM_CONTEXTS == 1)
        return VCM_SI_MODE;
    else
        return VCM_MI_MODE;
#else
    return VCM_SI_MODE;
#endif
}

/*****************************************************************************/
/* Function Name      : VcmGetL3Mode                                         */
/*                                                                           */
/* Description        : This function is used to get the mode (SI / MI)      */
/*                      of the L3 router                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SI_MODE or VCM_MI_MODE                           */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetL3Mode (VOID)
{

#ifdef VRF_WANTED
    return VCM_MI_MODE;
#else
    return VCM_SI_MODE;
#endif
}

/*****************************************************************************/
/* Function Name      : VcmGetL3ModeExt                                      */
/*                                                                           */
/* Description        : This function is used to get the mode (SI / MI)      */
/*                      of the L3 router                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SI_MODE or VCM_MI_MODE                           */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetL3ModeExt (VOID)
{
#ifdef VRF_WANTED
    if (SYS_DEF_MAX_NUM_CONTEXTS == 1)
        return VCM_SI_MODE;
    else
        return VCM_MI_MODE;
#else
    return VCM_SI_MODE;
#endif
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetIfMapCfgVcId                                 */
/*                                                                           */
/*     DESCRIPTION      : This function will return the configured context   */
/*                        for this interfcae                                 */
/*                                                                           */
/*     INPUT            : u4IfIndex      - Interface Index.                  */
/*                                                                           */
/*     OUTPUT           : pu4CfgCxtId    - Configured context id             */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetIfMapCfgVcId (UINT4 u4IfIndex, UINT4 *pu4CfgCxtId)
{
    tVcmIfMapEntry     *pIfMap = NULL;
    tVcmIfMapEntry      Dummy;

    Dummy.u4IfIndex = u4IfIndex;

    VCM_LOCK ();

    if (NULL != (pIfMap = VcmFindIfMapEntry (&Dummy)))
    {
        *pu4CfgCxtId = pIfMap->u4CfgVcNum;

        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetVcCxtType                                    */
/*                                                                           */
/*     DESCRIPTION      : This function will return the context type of the  */
/*                        given context.                                     */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : pi4CxtType     - Context type                      */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetVcCxtType (UINT4 u4ContextId, INT4 *pi4CxtType)
{
    tVirtualContextInfo Dummy;
    tVirtualContextInfo *pVcInfo = NULL;

    Dummy.u4VcNum = u4ContextId;

    VCM_LOCK ();

    pVcInfo = VcmFindVcEntry (&Dummy);
    if (pVcInfo != NULL)
    {
        *pi4CxtType = (INT4) pVcInfo->u1CurrCxtType;
        VCM_UNLOCK ();
        VCM_TRC (VCM_CONTROL_PATH_TRC, "VcmGetVcCxtType provided the context type of the given context and "
                     "returns success \n");
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    VCM_TRC (VCM_ALL_FAILURE_TRC, "VcmGetVcCxtType Failed \n");
    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmIsL3VcExist                                     */
/*                                                                           */
/*     DESCRIPTION      : This function will return true if the given        */
/*                        context ID is an L3 or an L2_L3 context.  This     */
/*                        function will return false otherwise.              */
/*                                                                           */
/*     INPUT            : u4ContextId - Context-Id                           */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VCM_TRUE  - If the given context exists, and the   */
/*                                    context type is of either L3 or L2_L3  */
/*                        VCM_FALSE - If the given context does not exist    */
/*                                    If the context is of type L2           */
/*****************************************************************************/
INT4
VcmIsL3VcExist (UINT4 u4ContextId)
{
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo Dummy;

    Dummy.u4VcNum = u4ContextId;

    VCM_LOCK ();

    pVcInfo = VcmFindVcEntry (&Dummy);

    if (pVcInfo != NULL)
    {
        if ((pVcInfo->u1CurrCxtType == VCM_L3_CONTEXT) ||
            (pVcInfo->u1CurrCxtType == VCM_L2_L3_CONTEXT))
        {
            VCM_UNLOCK ();
            return VCM_TRUE;
        }
    }

    VCM_UNLOCK ();
    return VCM_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmMapPortExt                                      */
/*                                                                           */
/*     DESCRIPTION      : This is an API to create a mapping of interface to */
/*                        virtual context by creating an entry in IfMap table*/
/*                        This is invoked from VLAN module.                  */
/*     INPUT            : u4IfIndex  - Interface to Get Map.                 */
/*                        pu1Alias   - Alias name of the context.            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmMapPortExt (UINT4 u4IfIndex, UINT1 *pu1Alias)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4IfMapVcNum = 0;
    UINT4               u4VcNum = 0;
    INT4                i4RetVal = 0;
    UINT1               au1IfAlias[VCM_ALIAS_MAX_LEN];
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];

    if (pu1Alias == NULL)
    {
        return VCM_FAILURE;
    }

    MEMSET (au1IfAlias, 0, VCM_ALIAS_MAX_LEN);
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

    if (VcmIsSwitchExist (pu1Alias, &u4VcNum) != VCM_TRUE)
    {
        return VCM_FAILURE;
    }

    i4RetVal = VcmIsIfMapExist (u4IfIndex);
    VcmGetIfMapVcId (u4IfIndex, &u4IfMapVcNum);

    if (i4RetVal == VCM_TRUE)
    {
        /* Mapping Entry exists. */
        if (CfaCliGetIfName (u4IfIndex, (INT1 *) au1IfName) == VCM_SUCCESS)
        {

            /* Check whether this interface is mapped to this VC or different. */
            if (u4VcNum == u4IfMapVcNum)
            {
                return VCM_SUCCESS;
            }
            else
            {
                VcmGetVcAlias (u4IfMapVcNum, au1IfAlias);
                return VCM_FAILURE;
            }
        }
        else
        {
            return VCM_FAILURE;
        }
    }
    else
    {
        /* There is no mapping. Create the row with create and wait.
         * Set the mapping. Make the row as active. */

        if (SNMP_SUCCESS != nmhTestv2FsVcIfRowStatus (&u4ErrorCode,
                                                      (INT4) u4IfIndex,
                                                      CREATE_AND_WAIT))
        {
            return VCM_FAILURE;
        }

        if (SNMP_SUCCESS != nmhSetFsVcIfRowStatus ((INT4) u4IfIndex,
                                                   (INT4) CREATE_AND_WAIT))
        {
            return VCM_FAILURE;
        }

        if (SNMP_SUCCESS != nmhTestv2FsVcId (&u4ErrorCode, (INT4) u4IfIndex,
                                             (INT4) u4VcNum))
        {
            VcmDelIfMapEntryExt (u4IfIndex);
            return VCM_FAILURE;
        }

        if (SNMP_SUCCESS != nmhSetFsVcId ((INT4) u4IfIndex, (INT4) u4VcNum))
        {
            VcmDelIfMapEntryExt (u4IfIndex);
            return VCM_FAILURE;
        }

        if (SNMP_SUCCESS != nmhTestv2FsVcIfRowStatus (&u4ErrorCode,
                                                      (INT4) u4IfIndex, ACTIVE))
        {
            VcmDelIfMapEntryExt (u4IfIndex);
            return VCM_FAILURE;
        }

        if (SNMP_SUCCESS != nmhSetFsVcIfRowStatus
            ((INT4) u4IfIndex, (INT4) ACTIVE))
        {
            VcmDelIfMapEntryExt (u4IfIndex);
            return VCM_FAILURE;
        }
        return VCM_SUCCESS;
    }
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmUnMapPortExt                                    */
/*                                                                           */
/*     DESCRIPTION      : This is an API to  delete mapping of interface     */
/*                        from virtual context by deleting an entry in       */
/*                        IfMap tables. This API is invoked from VLAN module */
/*     INPUT            : u4IfIndex  - Interface to Get Map.                 */
/*                        pu1Alias   - Alias name of the context.            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmUnMapPortExt (UINT4 u4IfIndex, UINT1 *pu1Alias)
{
    tCfaIfInfo          IfInfo;
    UINT4               u4ErrorCode = 0;
    UINT4               u4IfMapVcNum = 0;
    UINT4               u4VcNum = 0;
    INT4                i4RetVal = 0;
    INT1                ai1IfName[CFA_CLI_MAX_IF_NAME_LEN];

    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));
    MEMSET (ai1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);

    if (pu1Alias == NULL)
    {
        return VCM_FAILURE;
    }

    i4RetVal = VcmIsIfMapExist (u4IfIndex);
    VcmGetIfMapVcId (u4IfIndex, &u4IfMapVcNum);

    if (i4RetVal == VCM_TRUE)
    {
        if (VcmIsSwitchExist (pu1Alias, &u4VcNum) != VCM_TRUE)
        {
            return VCM_FAILURE;
        }
        /* Check whether this interface is mapped to this VC or not. */
        if (u4VcNum != u4IfMapVcNum)
        {
            return VCM_FAILURE;
        }
        else
        {

            /* Through CLI, VIP interfaces can't be created directly. It
             * gets created internally, when Service-Instance command is
             * given. Hence, VIP interfaces are not allowed to unmap from
             * a context, through CLI. */

            VcmCfaGetIfInfo (u4IfIndex, &IfInfo);

            if (IfInfo.u1BrgPortType == VCM_VIRTUAL_INSTANCE_PORT)
            {
                return VCM_FAILURE;
            }

            /* Its for NO command. Destroy the row. */
            if (SNMP_SUCCESS == nmhTestv2FsVcIfRowStatus (&u4ErrorCode,
                                                          (INT4) u4IfIndex,
                                                          DESTROY))
            {
                if (SNMP_SUCCESS == nmhSetFsVcIfRowStatus ((INT4) u4IfIndex,
                                                           (INT4) DESTROY))
                {
                    return VCM_SUCCESS;
                }
            }
            return VCM_FAILURE;
        }
    }
    else
    {
        CfaCliGetIfName (u4IfIndex, ai1IfName);

        return VCM_FAILURE;
    }
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetVcNextFreeLocalPortIdExt                     */
/*                                                                           */
/*     DESCRIPTION      : This function will return the Next free local      */
/*                        port Id in the context. It is invoked from VLAN    */
/*                        module                                             */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : pu2LocalPortId - Next free local port Id.          */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetVcNextFreeHlPortIdExt (UINT4 u4ContextId, UINT2 *pu2LocalPortId)
{
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo Dummy;

    MEMSET (&Dummy, 0, sizeof (tVirtualContextInfo));
    Dummy.u4VcNum = u4ContextId;

    if (pu2LocalPortId == NULL)
    {
        return VCM_FAILURE;
    }

    VCM_LOCK ();

    if (NULL != (pVcInfo = VcmFindVcEntry (&Dummy)))
    {
        *pu2LocalPortId = VCM_NEXTFREE_HLPORTID (pVcInfo);

        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/* Function Name      : VcmGetShowCmdOutputAndCalcChkSum                     */
/*                                                                           */
/* Description        : This funcion handles the execution of show commands  */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu2SwAudChkSum                                       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : MBSM_SUCESS/MBSM_FAILURE                             */
/*****************************************************************************/
INT4
VcmGetShowCmdOutputAndCalcChkSum (UINT2 *pu2SwAudChkSum)
{
#if (defined CLI_WANTED && defined RM_WANTED)

    if (VCM_NODE_STATUS () == VCM_RED_ACTIVE)
    {
        if (VcmCliGetShowCmdOutputToFile ((UINT1 *) VCM_AUDIT_FILE_ACTIVE) !=
            VCM_SUCCESS)
        {
            VCM_TRC (VCM_ALL_FAILURE_TRC, "GetShRunFile Failed\n");
            return VCM_FAILURE;
        }
        if (VcmCliCalcSwAudCheckSum
            ((UINT1 *) VCM_AUDIT_FILE_ACTIVE, pu2SwAudChkSum) != VCM_SUCCESS)
        {
            VCM_TRC (VCM_ALL_FAILURE_TRC, "CalcSwAudChkSum Failed\n");
            return VCM_FAILURE;
        }
    }
    else if (VCM_NODE_STATUS () == VCM_RED_STANDBY)
    {
        if (VcmCliGetShowCmdOutputToFile ((UINT1 *) VCM_AUDIT_FILE_STDBY) !=
            VCM_SUCCESS)
        {
            VCM_TRC (VCM_ALL_FAILURE_TRC, "GetShRunFile Failed\n");
            return VCM_FAILURE;
        }
        if (VcmCliCalcSwAudCheckSum
            ((UINT1 *) VCM_AUDIT_FILE_STDBY, pu2SwAudChkSum) != VCM_SUCCESS)
        {
            VCM_TRC (VCM_ALL_FAILURE_TRC, "CalcSwAudChkSum Failed\n");
            return VCM_FAILURE;
        }
    }
    else
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC,
                 "Node State is neither active nor standby\n");
        return VCM_FAILURE;
    }
#else
    UNUSED_PARAM (pu2SwAudChkSum);
#endif
    return VCM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmCfaDeleteInterfaceMapping                       */
/*                                                                           */
/*     DESCRIPTION      : This function is called from CFA when an IP        */
/*                        interface is row status is in create and wait and  */
/*                        and the interface is deleted.This function deletes */
/*                        the IfMap entry from IfMap table and frees the     */
/*                        memory.                                            */
/*                                                                           */
/*     INPUT            : u4IfIndex   - Interface Index                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmCfaDeleteInterfaceMapping (UINT4 u4IfIndex)
{
    tVirtualContextInfo DummyVc;
    tVcmIfMapEntry      Dummy;
    tVirtualContextInfo *pVcInfo = NULL;
    tVcmIfMapEntry     *pIfMapEntry = NULL;
    tVcmIpIfMapEntry   *pIpIfNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    INT1                i1Found = VCM_FALSE;

    VCM_LOCK ();

    Dummy.u4IfIndex = u4IfIndex;

    if (NULL == (pIfMapEntry = VcmFindIfMapEntry (&Dummy)))
    {
        /* Mapping does not exists */
        VCM_TRC (VCM_OS_RESOURCE_TRC,
                 "VcmCfaDeleteInterfaceMapping : Interface does not exists\n");
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }

    DummyVc.u4VcNum = pIfMapEntry->u4VcNum;
    if (NULL == (pVcInfo = VcmFindVcEntry (&DummyVc)))
    {
        /* Context does not exist */
        VCM_TRC (VCM_OS_RESOURCE_TRC,
                 "VcmCfaDeleteInterfaceMapping : Context not exists\n");
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }

    /* Delete the interface mapping from RBTree. */

    VcmRemoveIfMapEntry (pIfMapEntry);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVcIfRowStatus, 0, TRUE,
                          VcmConfLock, VcmConfUnLock, 1, SNMP_SUCCESS);
    SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", u4IfIndex, DESTROY));

    TMO_DLL_Scan (&pVcInfo->VcIPIntfList, pIpIfNode, tVcmIpIfMapEntry *)
    {
        if (pIpIfNode->pIfMapEntry->u4IfIndex == u4IfIndex)
        {
            i1Found = VCM_TRUE;
            break;
        }
    }

    if (i1Found == VCM_TRUE)
    {
        TMO_DLL_Delete (&pVcInfo->VcIPIntfList, &pIpIfNode->NextIfEntry);
        VCM_RELEASE_IPIFENTRY_MEMBLK (pIpIfNode);
    }

    VCM_RELEASE_IFMAPENTRY_MEMBLK (pIfMapEntry);
    VCM_UNLOCK ();
    return VCM_SUCCESS;
}
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmIsL3SubIfParentPort                             */
/*                                                                           */
/*     DESCRIPTION      : This function Validates whether the given index    */
/*                        is parent port of any of the l3subinterfaces       */
/*                                                                           */
/*     INPUT            : u4IfIndex   - Interface Index                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
UINT4
VcmIsL3SubIfParentPort (UINT4 u4IfIndex)
{
    if (CfaIsParentPort (u4IfIndex) == CFA_TRUE)
    {
        /* Given index is a Parent Port of L3Subinterface */
        return VCM_TRUE;
    }
    /* No L3Subinterface is associated with this port */
    return VCM_FALSE;
}   
#endif
