/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* $Id: vcmvcs.c,v 1.58 2018/02/15 10:22:24 siva Exp $                    */
/* Licensee Aricent Inc., 2004-2005                                          */
/*****************************************************************************/
/*    FILE  NAME            : vcmvcs.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : VCM module                                     */
/*    MODULE NAME           : VCM module                                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 16 Jun 2006                                    */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains functions related to        */
/*                            virtual context support by VCM module          */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    16 JUN 2006 / MITeam   Initial Create.                         */
/*---------------------------------------------------------------------------*/

#ifndef _VCMVCS_C
#define _VCMVCS_C

#include "vcminc.h"
#include "vcmcli.h"
#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
extern INT4         RtmActOnStaticRoutesForIfDeleteInCxt (UINT4 u4ContextId,
                                                          UINT4 u4IpPort);
#endif
/*****************************************************************************/
/* Function Name      : VcmCreateVirtualContext                              */
/*                                                                           */
/* Description        : This interface routine is called by Management module*/
/*                      when a new virtual context is created in the system. */
/*                      This routine will get the required memory for the    */
/*                      virtual context node from memory pool and add VC     */
/*                      entry to VCM table.                                  */
/*                                                                           */
/* Input(s)           : u4VcNum  - virtual context number                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS/VCM_FAILURE                              */
/*****************************************************************************/
INT4
VcmCreateVirtualContext (UINT4 u4VcNum)
{
    tVirtualContextInfo *pVc = NULL;
    tVirtualContextInfo Dummy;
#if defined (IP_WANTED) && defined (SNMP_3_WANTED)
    tSNMP_OCTET_STRING_TYPE VcName;
    UINT1               u1Length = 0;
#endif

    UINT2               u2Index;
    UINT1               au1VcAliasName[VCM_ALIAS_MAX_LEN];

    MEMSET (au1VcAliasName, 0, VCM_ALIAS_MAX_LEN);
    if (u4VcNum > MAX_CXT_PER_SYS)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC,
                 "VcmCreateVirtualcontext: Invalid virtual context No.\n");
        return VCM_FAILURE;
    }

    if (VcmIsVcExist (u4VcNum) == VCM_TRUE)
    {
        return VCM_FAILURE;
    }
    else
    {
        VCM_LOCK ();
        /*Given Virtual Context does not exist in the table. Create it. */
        pVc = VCM_ALLOCATE_VCINFO_MEMBLK ();
        if (pVc == NULL)
        {
            VCM_TRC (VCM_ALL_FAILURE_TRC | VCM_OS_RESOURCE_TRC,
                     "VcmCreateVirtualcontext: Memalloc failed\n");
            VCM_UNLOCK ();
            return VCM_FAILURE;
        }

        VCM_MEMSET (pVc, 0, sizeof (tVirtualContextInfo));

        /* Initialize the Context */
        pVc->u4VcNum = u4VcNum;
        pVc->u1RowStatus = ACTIVE;
        MEMSET (pVc->au1Owner, 0, VCM_OWNER_MAX_SIZE);
        VcmIssGetContextMacAddress (u4VcNum, pVc->VcBridgeAddr);

        /* This function will be called from SNMP for l2 context creation when
         * row status is made active directly without create and wait. Also this
         * function will be called from vcmmain.c during taks initialisation
         * for default context creation. So based on the context creation,
         * the context type is set in this function. 
         */
        if (u4VcNum == VCM_DEFAULT_CONTEXT)
        {
            VCM_STRNCPY (pVc->au1Alias, "default", STRLEN ("default"));
            pVc->au1Alias[STRLEN ("default")] = '\0';
            pVc->u1CurrCxtType = VCM_L2_L3_CONTEXT;
            pVc->u1CfgCxtType = VCM_L2_L3_CONTEXT;
            VcmIssGetVRMacAddr (u4VcNum, &(pVc->VRMacAddr));
        }
        else
        {
            SPRINTF ((CHR1 *) pVc->au1Alias, "switch%d", u4VcNum);
            pVc->u1CurrCxtType = VCM_L2_CONTEXT;
            pVc->u1CfgCxtType = VCM_L2_CONTEXT;
        }
        VCM_NEXTFREE_HLPORTID (pVc) = 1;    /* HLPortId starts from 1 */
        pVc->u2PortCount = VCM_INVALID_VAL;    /* No ports mapped to the context */
        pVc->u4SispPortCount = VCM_INVALID_VAL;

        VCM_SLL_INIT (&pVc->VcPortList);
        VCM_DLL_INIT (&pVc->VcIPIntfList);    /* Initialise the doubly linked 
                                               list containing layer 3
                                               interfaces */
        /* Add it to VC List */
        if (FAILURE == VcmAddVcEntry ((VOID *) pVc))
        {
            VCM_TRC (VCM_ALL_FAILURE_TRC | VCM_OS_RESOURCE_TRC,
                     "VcmCreateVirtualContext: Unable to add VC entry\n");
            VCM_UNLOCK ();
            VCM_RELEASE_VCINFO_MEMBLK (pVc);
            return VCM_FAILURE;
        }

        VCM_UNLOCK ();
#ifdef IP_WANTED
#ifdef NPAPI_WANTED
        if (VCM_NODE_STATUS () == VCM_RED_ACTIVE)
        {
            if (VcmFsVcmHwCreateContext (u4VcNum) != FNP_SUCCESS)
            {
                pVc = NULL;
                Dummy.u4VcNum = u4VcNum;
                VCM_LOCK ();
                pVc = VcmFindVcEntry (&Dummy);
                VcmRemoveVcEntry (pVc);
                VCM_UNLOCK ();
                VCM_TRC (VCM_ALL_FAILURE_TRC,
                         "FsVcmHwCreateContext: Failed to create in Hardware.\n");
                return VCM_FAILURE;
            }
        }
#endif
#endif
        /* Create the Virtual context in layer2 modules */
        if (L2CreateVirtualContext (u4VcNum) != VCM_SUCCESS)
        {
            VCM_TRC (VCM_ALL_FAILURE_TRC,
                     "VcmCreateVirtualcontext: Failed to create in L2 modules.\n");
#ifdef IP_WANTED
#ifdef NPAPI_WANTED
            if (VCM_NODE_STATUS () == VCM_RED_ACTIVE)
            {
                if (VcmFsVcmHwDeleteContext (u4VcNum) != FNP_SUCCESS)
                {
                    VCM_TRC (VCM_ALL_FAILURE_TRC,
                             "FsVcmHwDeleteContext: Failed to delete in Hardware.\n");
                    return VCM_FAILURE;
                }
            }
#endif
#endif
            pVc = NULL;
            Dummy.u4VcNum = u4VcNum;
            VCM_LOCK ();
            pVc = VcmFindVcEntry (&Dummy);
            VcmRemoveVcEntry (pVc);
            VCM_UNLOCK ();
            return VCM_FAILURE;
        }
#if defined (IP_WANTED) && defined (SNMP_3_WANTED)
        u1Length = (UINT1) STRLEN (pVc->au1Alias);

        VcName.pu1_OctetList = au1VcAliasName;
        MEMCPY (VcName.pu1_OctetList, pVc->au1Alias, u1Length);
        VcName.i4_Length = u1Length;

        if (SnmpCreateVirtualContext (u4VcNum, &VcName) != SNMP_SUCCESS)
        {
            VCM_TRC (VCM_ALL_FAILURE_TRC,
                     "VcmCreateVirtualcontext: Failed to create in SNMPv3 module.\n");
            L2DeleteVirtualContext (u4VcNum);

#ifdef NPAPI_WANTED
            if (VCM_NODE_STATUS () == VCM_RED_ACTIVE)
            {
                if (VcmFsVcmHwDeleteContext (u4VcNum) != FNP_SUCCESS)
                {
                    VCM_TRC (VCM_ALL_FAILURE_TRC,
                             "FsVcmHwDeleteContext: Failed to delete in Hardware.\n");
                    return VCM_FAILURE;
                }
            }
#endif
            pVc = NULL;
            Dummy.u4VcNum = u4VcNum;
            VCM_LOCK ();
            pVc = VcmFindVcEntry (&Dummy);
            VcmRemoveVcEntry (pVc);
            VCM_UNLOCK ();
            return VCM_FAILURE;
        }
#endif

        /* TO get the Next free Context-Id. If all the context are get 
         * created it assigns 0 to gVcmGlobals.u2NextFreeCxtId */

        gVcmGlobals.u2NextFreeCxtId = VCM_INVALID_VAL;

        for (u2Index = VCM_DEFAULT_CONTEXT;
             u2Index <= MAX_CXT_PER_SYS; u2Index++)
        {
            if (VcmIsContextExist (u2Index) != VCM_TRUE)
            {
                gVcmGlobals.u2NextFreeCxtId = u2Index;
                break;
            }
        }
        VcmSnmpSendTrap (VCM_CREATE_CONTEXT_TRAP,
                         (INT1 *) VCM_CREATE_CONTEXT_TRAP_OID,
                         SNMP_TRAP_OID_LEN, &u4VcNum);
        VCM_TRC (VCM_MGMT_TRC,
                 "VcmCreateVirtualContext: VCM Context created Successfully .\n");

        return VCM_SUCCESS;
    }
}

/*****************************************************************************/
/* Function Name      : VcmCreateContext                                     */
/*                                                                           */
/* Description        : This function is called from Management module when  */
/*                      a new virtual context is created in the system with  */
/*                      row status as create and wait.                       */
/*                                                                           */
/* Input(s)           : u4VcNum  - virtual context number                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS/VCM_FAILURE                              */
/*****************************************************************************/
INT4
VcmCreateContext (UINT4 u4VcNum)
{
    tVirtualContextInfo *pVc = NULL;
    UINT2               u2Index;

    VCM_LOCK ();

    /*Given Virtual Context does not exist in the context table. Create it. */
    pVc = VCM_ALLOCATE_VCINFO_MEMBLK ();
    if (pVc == NULL)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC | VCM_OS_RESOURCE_TRC,
                 "VcmCreateContext: Memalloc failed\n");
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }

    VCM_MEMSET (pVc, 0, sizeof (tVirtualContextInfo));

    /* Initialize the Context */
    pVc->u4VcNum = u4VcNum;
    pVc->u1RowStatus = NOT_READY;
    pVc->u1CurrCxtType = VCM_INVALID_CONTEXT_TYPE;
    pVc->u1CfgCxtType = VCM_L2_CONTEXT;
    MEMSET (pVc->au1Owner, 0, VCM_OWNER_MAX_SIZE);
    SPRINTF ((CHR1 *) pVc->au1Alias, "switch%d", u4VcNum);
    VCM_NEXTFREE_HLPORTID (pVc) = 1;    /* HLPortId starts from 1 */
    pVc->u2PortCount = VCM_INVALID_VAL;    /* No ports mapped to the context */
    VCM_SLL_INIT (&pVc->VcPortList);
    VCM_DLL_INIT (&pVc->VcIPIntfList);    /* Initialise the doubly linked 
                                           list containing layer 3
                                           interfaces */
    /* Add it to VC List */
    if (FAILURE == VcmAddVcEntry ((VOID *) pVc))
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC | VCM_OS_RESOURCE_TRC,
                 "VcmCreateVirtualContext: Unable to add VC entry\n");
        VCM_RELEASE_VCINFO_MEMBLK (pVc);
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }

    VCM_UNLOCK ();

    /* TO get the Next free Context-Id. If all the context are get 
     * created it assigns 0 to gVcmGlobals.u2NextFreeCxtId */

    gVcmGlobals.u2NextFreeCxtId = VCM_INVALID_VAL;

    for (u2Index = VCM_DEFAULT_CONTEXT; u2Index <= MAX_CXT_PER_SYS; u2Index++)
    {
        if (VcmIsContextExist (u2Index) != VCM_TRUE)
        {
            gVcmGlobals.u2NextFreeCxtId = u2Index;
            break;
        }
    }
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmMakeContextActive                                 */
/*                                                                           */
/* Description        : This routine is called by Management module when the */
/*                      virtual context is made active from NOT_READY or     */
/*                      NOT_IN_SERVICE state. This function takes care of    */
/*                      checking the configured and current context type and */
/*                      give indication to l2 and l3 protocols accordingly.  */
/*                                                                           */
/* Input(s)           : u4VcNum  - virtual context number                    */
/*                      u4CurrRowStatus - Row status                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS/VCM_FAILURE                              */
/*****************************************************************************/
INT4
VcmMakeContextActive (UINT4 u4VcNum, UINT4 u4CurrRowStatus)
{
    INT4                i4RetVal = VCM_FAILURE;

    switch (u4CurrRowStatus)
    {
        case NOT_READY:
            i4RetVal = VcmMakeCxtActiveFromNotReady (u4VcNum);
            break;
        case NOT_IN_SERVICE:
            i4RetVal = VcmMakeCxtActiveFromNIS (u4VcNum);
            break;
        default:
            break;
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : VcmMakeCxtActiveFromNotReady                         */
/*                                                                           */
/* Description        : This routine is called by Management module when the */
/*                      virtual context is made active from NOT_READY state  */
/*                      This function takes care of creating the context in  */
/*                      hardware, takes care of indicating l2/l3 protocols   */
/*                      about context creation and indicates snmp about      */
/*                      context creation.                                    */
/*                                                                           */
/* Input(s)           : u4VcNum  - virtual context number                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS/VCM_FAILURE                              */
/*****************************************************************************/
INT4
VcmMakeCxtActiveFromNotReady (UINT4 u4VcNum)
{
    tVirtualContextInfo *pVc = NULL;
    tVirtualContextInfo Dummy;
#if defined (IP_WANTED) && defined (SNMP_3_WANTED)
    tSNMP_OCTET_STRING_TYPE VcName;
#endif
    INT4                i4RetVal = VCM_SUCCESS;
    UINT1               u1CurrCxtType = 0;
    UINT1               u1CfgCxtType = 0;
    UINT1               u1RowStatus = 0;
    tMacAddr            MacAddr;
#ifdef IP_WANTED
#ifdef NPAPI_WANTED
    tVrMapAction        VrMapAction;
    tFsNpVcmVrMapInfo   VcmVrMapInfo;
#endif
#endif

    MEMSET (MacAddr, 0, sizeof (tMacAddr));

    Dummy.u4VcNum = u4VcNum;

    VCM_LOCK ();
    pVc = VcmFindVcEntry (&Dummy);
    if (pVc == NULL)
    {
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }
    u1RowStatus = pVc->u1RowStatus;
    u1CurrCxtType = pVc->u1CurrCxtType;
    u1CfgCxtType = pVc->u1CfgCxtType;
    pVc->u1RowStatus = ACTIVE;
    pVc->u1CurrCxtType = pVc->u1CfgCxtType;

    if ((u1CfgCxtType == VCM_L3_CONTEXT) || (u1CfgCxtType == VCM_L2_L3_CONTEXT))
    {
        VcmIssGetVRMacAddr (u4VcNum, &(pVc->VRMacAddr));
        VCM_MEMCPY (MacAddr, pVc->VRMacAddr, MAC_ADDR_LEN);
    }

#if defined (IP_WANTED) && defined (SNMP_3_WANTED)
    VcName.pu1_OctetList = pVc->au1Alias;
    VcName.i4_Length = (INT4) STRLEN (pVc->au1Alias);
#endif
    VCM_UNLOCK ();
#ifdef IP_WANTED
#ifdef NPAPI_WANTED
    MEMSET (&VcmVrMapInfo, 0, sizeof (tFsNpVcmVrMapInfo));
    VrMapAction = NP_HW_MAP_INVALID;
    if (VCM_NODE_STATUS () == VCM_RED_ACTIVE)
    {
        /* Create the context in hardware */
        if ((u1CfgCxtType == VCM_L2_CONTEXT) ||
            (u1CfgCxtType == VCM_L2_L3_CONTEXT))
        {
            if (VcmFsVcmHwCreateContext (u4VcNum) != FNP_SUCCESS)
            {
                pVc = NULL;
                Dummy.u4VcNum = u4VcNum;
                VCM_LOCK ();
                pVc = VcmFindVcEntry (&Dummy);
                if (pVc == NULL)
                {
                    VCM_UNLOCK ();
                    VCM_TRC (VCM_ALL_FAILURE_TRC,
                             "FsVcmHwCreateContext: Failed to create in Hardware.\n");
                    return VCM_FAILURE;
                }

                pVc->u1RowStatus = u1RowStatus;
                pVc->u1CurrCxtType = u1CurrCxtType;
                VCM_UNLOCK ();
                VCM_TRC (VCM_ALL_FAILURE_TRC,
                         "FsVcmHwCreateContext: Failed to create in Hardware.\n");
                return VCM_FAILURE;
            }
        }
        if ((u1CfgCxtType == VCM_L3_CONTEXT) ||
            (u1CfgCxtType == VCM_L2_L3_CONTEXT))
        {
            VrMapAction = NP_HW_CREATE_VIRTUAL_ROUTER;
            VcmVrMapInfo.u4VrfId = u4VcNum;
            VCM_MEMCPY (VcmVrMapInfo.MacAddr, MacAddr, MAC_ADDR_LEN);
            if (VcmFsVcmHwMapVirtualRouter (VrMapAction, &VcmVrMapInfo) ==
                FNP_FAILURE)
            {
                pVc = NULL;
                Dummy.u4VcNum = u4VcNum;
                VCM_LOCK ();
                pVc = VcmFindVcEntry (&Dummy);
                if (pVc == NULL)
                {
                    VCM_UNLOCK ();
                    VCM_TRC (VCM_ALL_FAILURE_TRC,
                             "FsVcmHwMapVirtualRouter: Failed to create virtual router in Hardware.\n");
                    return VCM_FAILURE;
                }
                pVc->u1RowStatus = u1RowStatus;
                pVc->u1CurrCxtType = u1CurrCxtType;
                MEMSET (pVc->VRMacAddr, 0, sizeof (tMacAddr));
                VCM_UNLOCK ();
                VCM_TRC (VCM_ALL_FAILURE_TRC,
                         "FsVcmHwMapVirtualRouter: Failed to create virtual router in Hardware.\n");
                return VCM_FAILURE;
            }
        }
    }
#endif
#endif

#if defined (IP_WANTED) && defined (SNMP_3_WANTED)
    if (SnmpCreateVirtualContext (u4VcNum, &VcName) != SNMP_SUCCESS)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC,
                 "VcmCreateVirtualcontext: Failed to create in SNMPv3 module.\n");

#ifdef IP_WANTED
#ifdef NPAPI_WANTED
        if (VCM_NODE_STATUS () == VCM_RED_ACTIVE)
        {
            if ((u1CfgCxtType == VCM_L2_CONTEXT) ||
                (u1CfgCxtType == VCM_L2_L3_CONTEXT))
            {
                if (VcmFsVcmHwDeleteContext (u4VcNum) != FNP_SUCCESS)
                {
                    VCM_TRC (VCM_ALL_FAILURE_TRC,
                             "FsVcmHwDeleteContext: Failed to delete in Hardware.\n");
                    return VCM_FAILURE;
                }
            }
            if ((u1CfgCxtType == VCM_L3_CONTEXT) ||
                (u1CfgCxtType == VCM_L2_L3_CONTEXT))
            {
                VrMapAction = NP_HW_DELETE_VIRTUAL_ROUTER;
                VcmVrMapInfo.u4VrfId = u4VcNum;
                if (VcmFsVcmHwMapVirtualRouter (VrMapAction, &VcmVrMapInfo) ==
                    FNP_FAILURE)
                {
                    VCM_TRC (VCM_ALL_FAILURE_TRC,
                             "FsVcmHwMapVirtualRouter: Failed to delete virtual router in Hardware.\n");
                    return VCM_FAILURE;
                }
            }
        }
#endif
#endif
    }
#endif

    /* Row status is made active from not-ready state. So based on the 
     * configured context type, give indication to l2/l3 modules.
     */
    switch (u1CfgCxtType)
    {
        case VCM_L2_CONTEXT:

            Dummy.u4VcNum = u4VcNum;

            VCM_LOCK ();
            pVc = VcmFindVcEntry (&Dummy);
            if (pVc == NULL)
            {
                VCM_UNLOCK ();
                return VCM_FAILURE;
            }

            VcmIssGetContextMacAddress (u4VcNum, pVc->VcBridgeAddr);
            VCM_UNLOCK ();
            i4RetVal = VcmIndicateL2CxtUpdate (u4VcNum, VCM_CONTEXT_CREATE);
            break;

        case VCM_L3_CONTEXT:
            VcmIndicateL3CxtUpdate (u4VcNum, VCM_CONTEXT_CREATE);
            gVcmGlobals.u2L3ContextCount++;
            break;

        case VCM_L2_L3_CONTEXT:

            Dummy.u4VcNum = u4VcNum;

            VCM_LOCK ();
            pVc = VcmFindVcEntry (&Dummy);
            if (pVc == NULL)
            {
                VCM_UNLOCK ();
                return VCM_FAILURE;
            }

            VcmIssGetContextMacAddress (u4VcNum, pVc->VcBridgeAddr);
            VCM_UNLOCK ();
            i4RetVal = VcmIndicateL2CxtUpdate (u4VcNum, VCM_CONTEXT_CREATE);
            VcmIndicateL3CxtUpdate (u4VcNum, VCM_CONTEXT_CREATE);
            gVcmGlobals.u2L3ContextCount++;
            break;

        default:
            i4RetVal = VCM_FAILURE;
            break;
    }

    if (i4RetVal == VCM_FAILURE)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC,
                 "VcmCreateVirtualcontext: Failed to create in L2/L3  modules.\n");
#ifdef IP_WANTED
#ifdef NPAPI_WANTED
        if (VCM_NODE_STATUS () == VCM_RED_ACTIVE)
        {
            if ((u1CfgCxtType == VCM_L2_CONTEXT) ||
                (u1CfgCxtType == VCM_L2_L3_CONTEXT))
            {
                if (VcmFsVcmHwDeleteContext (u4VcNum) != FNP_SUCCESS)
                {
                    VCM_TRC (VCM_ALL_FAILURE_TRC,
                             "FsVcmHwDeleteContext: Failed to delete in Hardware.\n");
                }
            }
            if ((u1CfgCxtType == VCM_L3_CONTEXT) ||
                (u1CfgCxtType == VCM_L2_L3_CONTEXT))
            {
                VrMapAction = NP_HW_DELETE_VIRTUAL_ROUTER;
                VcmVrMapInfo.u4VrfId = u4VcNum;
                if (VcmFsVcmHwMapVirtualRouter (VrMapAction, &VcmVrMapInfo) ==
                    FNP_FAILURE)
                {
                    VCM_TRC (VCM_ALL_FAILURE_TRC,
                             "FsVcmHwMapVirtualRouter: Failed to delete virtual router in Hardware.\n");
                }
            }
        }
#endif
#endif
#if defined (IP_WANTED) && defined (SNMP_3_WANTED)
        SnmpDeleteVirtualContext (u4VcNum);
#endif
        pVc = NULL;
        Dummy.u4VcNum = u4VcNum;
        VCM_LOCK ();
        pVc = VcmFindVcEntry (&Dummy);
        if (NULL != pVc)
        {
            pVc->u1RowStatus = u1RowStatus;
            pVc->u1CurrCxtType = u1CurrCxtType;
            MEMSET (pVc->VcBridgeAddr, 0, sizeof (tMacAddr));
            MEMSET (pVc->VRMacAddr, 0, sizeof (tMacAddr));
        }
        VCM_UNLOCK ();
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : VcmMakeCxtActiveFromNIS                              */
/*                                                                           */
/* Description        : This routine is called by Management module when the */
/*                      virtual context is made active from NOT_IN_SERVICE   */
/*                      state. This function takes care of indicating l2/l3  */
/*                      protocols about context creation/deletion based on   */
/*                      the context type configuration                       */
/*                                                                           */
/* Input(s)           : u4VcNum  - virtual context number                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS/VCM_FAILURE                              */
/*****************************************************************************/
INT4
VcmMakeCxtActiveFromNIS (UINT4 u4VcNum)
{
    tVirtualContextInfo Dummy;
    tVirtualContextInfo *pVc = NULL;
    INT4                i4RetVal = VCM_SUCCESS;
    UINT1               u1CurrCxtType = 0;
    UINT1               u1CfgCxtType = 0;
    UINT1               u1RowStatus = 0;
    tMacAddr            MacAddr;
#ifdef NPAPI_WANTED
    tVrMapAction        VrMapAction;
    tFsNpVcmVrMapInfo   VcmVrMapInfo;
#endif

    MEMSET (MacAddr, 0, MAC_ADDR_LEN);
    Dummy.u4VcNum = u4VcNum;

    VCM_LOCK ();
    pVc = VcmFindVcEntry (&Dummy);
    if (pVc == NULL)
    {
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }
    u1RowStatus = pVc->u1RowStatus;
    u1CurrCxtType = pVc->u1CurrCxtType;
    u1CfgCxtType = pVc->u1CfgCxtType;
    pVc->u1RowStatus = ACTIVE;
    pVc->u1CurrCxtType = pVc->u1CfgCxtType;
    VCM_UNLOCK ();

    /* Create the Virtual context in layer2 /layer3 modules */
    if (u1CurrCxtType == u1CfgCxtType)
    {
        return VCM_SUCCESS;
    }

    switch (u1CurrCxtType)
    {
        case VCM_L2_CONTEXT:
            if (u1CfgCxtType == VCM_L3_CONTEXT)
            {
                /* L2 context is changed to l3 context. So give delete indication
                 * to l2 protocols and give context create indication to 
                 * l3 protocols.
                 */
                VcmIssGetVRMacAddr (u4VcNum, &MacAddr);
#ifdef NPAPI_WANTED
                MEMSET (&VcmVrMapInfo, 0, sizeof (tFsNpVcmVrMapInfo));
                /* Create the context in hardware */
                if (VcmFsVcmHwDeleteContext (u4VcNum) != FNP_SUCCESS)
                {
                    VCM_TRC (VCM_ALL_FAILURE_TRC,
                             "FsVcmHwDeleteContext: Failed to create in Hardware.\n");
                    i4RetVal = VCM_FAILURE;
                }
                VrMapAction = NP_HW_CREATE_VIRTUAL_ROUTER;
                VcmVrMapInfo.u4VrfId = u4VcNum;
                VCM_MEMCPY (VcmVrMapInfo.MacAddr, MacAddr, MAC_ADDR_LEN);
                if (VcmFsVcmHwMapVirtualRouter (VrMapAction, &VcmVrMapInfo) ==
                    FNP_FAILURE)
                {
                    VCM_TRC (VCM_ALL_FAILURE_TRC,
                             "FsVcmHwMapVirtualRouter: Failed to create virtual router in Hardware.\n");
                    i4RetVal = VCM_FAILURE;
                }
#endif
                i4RetVal = VcmIndicateL2CxtUpdate (u4VcNum, VCM_CONTEXT_DELETE);
                MEMSET (pVc->VcBridgeAddr, 0, sizeof (tMacAddr));
                VcmIndicateL3CxtUpdate (u4VcNum, VCM_CONTEXT_CREATE);
                /* A new l3 context is added. So increment the l3 context
                 * count
                 */
                gVcmGlobals.u2L3ContextCount++;
            }
            else
            {
                /* L2 context is changed to both l2 and l3 context. So give
                 * context create indication to l3 protocols.
                 */
                VcmIssGetVRMacAddr (u4VcNum, &MacAddr);
#ifdef NPAPI_WANTED
                MEMSET (&VcmVrMapInfo, 0, sizeof (tFsNpVcmVrMapInfo));
                VrMapAction = NP_HW_CREATE_VIRTUAL_ROUTER;
                VcmVrMapInfo.u4VrfId = u4VcNum;
                VCM_MEMCPY (VcmVrMapInfo.MacAddr, MacAddr, MAC_ADDR_LEN);
                if (VcmFsVcmHwMapVirtualRouter (VrMapAction, &VcmVrMapInfo) ==
                    FNP_FAILURE)
                {
                    VCM_TRC (VCM_ALL_FAILURE_TRC,
                             "FsVcmHwMapVirtualRouter: Failed to create virtual router in Hardware.\n");
                    i4RetVal = VCM_FAILURE;
                }
#endif
                VcmIndicateL3CxtUpdate (u4VcNum, VCM_CONTEXT_CREATE);
                gVcmGlobals.u2L3ContextCount++;
            }
            break;

        case VCM_L3_CONTEXT:
            if (u1CfgCxtType == VCM_L2_CONTEXT)
            {
                /* Existing L3 context is changed to l2 context.So give context
                 * delete indication to l3 protocols and give context create
                 * indication to l2 protocols.
                 */
#ifdef NPAPI_WANTED
                MEMSET (&VcmVrMapInfo, 0, sizeof (tFsNpVcmVrMapInfo));
                /* Create the context in hardware */
                if (VcmFsVcmHwCreateContext (u4VcNum) != FNP_SUCCESS)
                {
                    VCM_TRC (VCM_ALL_FAILURE_TRC,
                             "FsVcmHwCreateContext: Failed to create in Hardware.\n");
                    i4RetVal = VCM_FAILURE;
                }
                VrMapAction = NP_HW_DELETE_VIRTUAL_ROUTER;
                VcmVrMapInfo.u4VrfId = u4VcNum;
                if (VcmFsVcmHwMapVirtualRouter (VrMapAction, &VcmVrMapInfo) ==
                    FNP_FAILURE)
                {
                    VCM_TRC (VCM_ALL_FAILURE_TRC,
                             "FsVcmHwMapVirtualRouter: Failed to delete virtual router in Hardware.\n");
                    i4RetVal = VCM_FAILURE;
                }
#endif
                VcmIssGetContextMacAddress (u4VcNum, pVc->VcBridgeAddr);
                i4RetVal = VcmIndicateL2CxtUpdate (u4VcNum, VCM_CONTEXT_CREATE);
                MEMSET (MacAddr, 0, MAC_ADDR_LEN);
                VcmIndicateL3CxtUpdate (u4VcNum, VCM_CONTEXT_DELETE);

                /* Existing l3 context is deleted. So decrement the l3 context
                 * count
                 */
                gVcmGlobals.u2L3ContextCount--;
            }
            else
            {
                /* L3 context is changed to both l2 and l3 protocols. So give
                 * context create indication to l2 protocols.
                 */
#ifdef NPAPI_WANTED
                /* Create the context in hardware */
                if (VcmFsVcmHwCreateContext (u4VcNum) != FNP_SUCCESS)
                {
                    VCM_TRC (VCM_ALL_FAILURE_TRC,
                             "FsVcmHwCreateContext: Failed to create in Hardware.\n");
                    i4RetVal = VCM_FAILURE;
                }
#endif
                VcmIssGetContextMacAddress (u4VcNum, pVc->VcBridgeAddr);
                i4RetVal = VcmIndicateL2CxtUpdate (u4VcNum, VCM_CONTEXT_CREATE);
            }
            break;
        case VCM_L2_L3_CONTEXT:
            if (u1CfgCxtType == VCM_L3_CONTEXT)
            {
                /* L2 and L3 context is changed to l3 context. So give context
                 * delete indication to l2 protocols.
                 */
#ifdef NPAPI_WANTED
                if (VcmFsVcmHwDeleteContext (u4VcNum) != FNP_SUCCESS)
                {
                    VCM_TRC (VCM_ALL_FAILURE_TRC,
                             "FsVcmHwDeleteContext: Failed to create in Hardware.\n");
                    i4RetVal = VCM_FAILURE;
                }
#endif
                MEMSET (pVc->VcBridgeAddr, 0, sizeof (tMacAddr));
                i4RetVal = VcmIndicateL2CxtUpdate (u4VcNum, VCM_CONTEXT_DELETE);
            }
            else
            {
                /* L2 and L3 context is changed to l2 context. So give context
                 * delete indication to l3 protocols.
                 */
#ifdef NPAPI_WANTED
                MEMSET (&VcmVrMapInfo, 0, sizeof (tFsNpVcmVrMapInfo));
                VrMapAction = NP_HW_DELETE_VIRTUAL_ROUTER;
                VcmVrMapInfo.u4VrfId = u4VcNum;
                if (VcmFsVcmHwMapVirtualRouter (VrMapAction, &VcmVrMapInfo) ==
                    FNP_FAILURE)
                {
                    VCM_TRC (VCM_ALL_FAILURE_TRC,
                             "FsVcmHwMapVirtualRouter: Failed to delete virtual router in Hardware.\n");
                    i4RetVal = VCM_FAILURE;
                }
#endif
                MEMSET (MacAddr, 0, MAC_ADDR_LEN);
                VcmIndicateL3CxtUpdate (u4VcNum, VCM_CONTEXT_DELETE);
                gVcmGlobals.u2L3ContextCount--;
            }
            break;
        default:
            i4RetVal = VCM_FAILURE;
            break;
    }

    if (i4RetVal == VCM_FAILURE)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC,
                 "VcmCreateVirtualcontext: Failed to create in L2/L3  modules.\n");
        pVc = NULL;
        Dummy.u4VcNum = u4VcNum;
        VCM_LOCK ();
        pVc = VcmFindVcEntry (&Dummy);
        if (NULL != pVc)
        {
            pVc->u1RowStatus = u1RowStatus;
            pVc->u1CurrCxtType = u1CurrCxtType;
        }
        VCM_UNLOCK ();
    }
    else
    {
        Dummy.u4VcNum = u4VcNum;
        VCM_LOCK ();
        pVc = VcmFindVcEntry (&Dummy);
        if (NULL != pVc)
        {
            VCM_MEMCPY (pVc->VRMacAddr, MacAddr, MAC_ADDR_LEN);
        }
        VCM_UNLOCK ();
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : VcmIndicateL2CxtUpdate                               */
/*                                                                           */
/* Description        : This routine is called by Management to give context */
/*                      creation and deletion indication to L2               */
/*                      protocols.                                           */
/*                                                                           */
/* Input(s)           : u4VcNum  - virtual context number                    */
/*                      u1UpdateType - Context creation/deletion             */
/*                                                                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS/VCM_FAILURE                              */
/*****************************************************************************/
INT4
VcmIndicateL2CxtUpdate (UINT4 u4VcNum, UINT1 u1UpdateType)
{

    INT4                i4RetVal = VCM_FAILURE;

    if (u1UpdateType == VCM_CONTEXT_CREATE)
    {
        i4RetVal = L2CreateVirtualContext (u4VcNum);
    }

    if (u1UpdateType == VCM_CONTEXT_DELETE)
    {
        i4RetVal = L2DeleteVirtualContext (u4VcNum);
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : VcmIndicateL3CxtUpdate                               */
/*                                                                           */
/* Description        : This routine is called by Management to give context */
/*                      creation and deletion indication to L3               */
/*                      protocols.                                           */
/*                                                                           */
/* Input(s)           : u4VcNum  - virtual context number                    */
/*                      u1UpdateType - Context creation/deletion             */
/*                                                                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS/VCM_FAILURE                              */
/*****************************************************************************/
INT4
VcmIndicateL3CxtUpdate (UINT4 u4VcNum, UINT1 u1UpdateType)
{
    tVcmHLProtoRegEntry *pRegEntry = NULL;
    UINT4               u4IfIndex = 0;

    TMO_SLL_Scan (&(gVcmGlobals.VcmProtoRegTable), pRegEntry,
                  tVcmHLProtoRegEntry *)
    {
        if (pRegEntry->pIfMapChngAndCxtChng != NULL)
        {
            (*(pRegEntry->pIfMapChngAndCxtChng)) (u4IfIndex, u4VcNum,
                                                  u1UpdateType);
        }
    }
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmDeleteVirtualContext                              */
/*                                                                           */
/* Description        : This interface routine is called by Management module*/
/*                      when a virtual context is deleted in the system.     */
/*                      This routine will remove the IfMap entries for this  */
/*                      context, remove from L2 and removes the VC entry from*/
/*                      VCM table. Releases the memory to pool.              */
/*                                                                           */
/* Input(s)           : u4VcNum  - virtual context number                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS / FAILURE                                    */
/*****************************************************************************/
INT4
VcmDeleteVirtualContext (UINT4 u4VcNum)
{
    if ((u4VcNum <= VCM_DEFAULT_CONTEXT) || (u4VcNum > MAX_CXT_PER_SYS))
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC,
                 "VcmDeleteVirtualcontext: Invalid virtual context No.\n");
        return VCM_FAILURE;
    }
    VcmSnmpSendTrap (VCM_DELETE_CONTEXT_TRAP,
                     (INT1 *) VCM_DELETE_CONTEXT_TRAP_OID, SNMP_TRAP_OID_LEN,
                     &u4VcNum);
    return (VcmDeleteVirtualContextIncDefault (u4VcNum));
}

/*****************************************************************************/
/* Function Name      : VcmDeleteVirtualContextIncDefault                    */
/*                                                                           */
/* Description        : Virtual contexts are deleted in the system including */
/*                      default context.                                     */
/*                      This routine will remove the IfMap entries for this  */
/*                      context, remove from L2 and removes the VC entry from*/
/*                      VCM table. Releases the memory to pool.              */
/*                                                                           */
/* Input(s)           : u4VcNum  - virtual context number                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS / FAILURE                                    */
/*****************************************************************************/
INT4
VcmDeleteVirtualContextIncDefault (UINT4 u4VcNum)
{
    INT4                i4CxtType = 0;
    INT4                i4Result = VCM_TRUE;
    UINT2               u2PortCount = 0;
    UINT2               u2IpIfCount = 0;
    INT1                i1IfMapFlag = VCM_FALSE;
#if IP_WANTED
#ifdef NPAPI_WANTED
    tVrMapAction        VrMapAction;
    tFsNpVcmVrMapInfo   VcmVrMapInfo;
#endif
#endif

    if (u4VcNum > MAX_CXT_PER_SYS)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC,
                 "VcmDeleteVirtualcontext: Invalid virtual context No.\n");
        return VCM_FAILURE;
    }

    if (VcmIsContextExist (u4VcNum) == VCM_FALSE)
    {
        /* Given VcNum is not there in Virtual context table. */
        VCM_TRC (VCM_ALL_FAILURE_TRC, "Vc already removed.\n");
        return VCM_SUCCESS;
    }

    VcmGetVcPortCount (u4VcNum, &u2PortCount);
    i4Result = VcmIsL3IfMappedToL2Cxt (u4VcNum);
    VcmGetIpIfCount (u4VcNum, &u2IpIfCount);

    if ((u2PortCount != 0) || (i4Result != VCM_FALSE) || (u2IpIfCount != 0))
    {
        /* There is some interface mapped to this context */
        i1IfMapFlag = VCM_TRUE;
    }

    if (i1IfMapFlag == VCM_TRUE)
    {
        /* Remove the interfaces mapped to the context before deleting the 
         * context. */
        VcmRemoveIfMapEntriesForContext (u4VcNum);
    }

    /* Delete the Virtual context from l2/l3 modules based on the context type */
    VcmGetVcCxtType (u4VcNum, &i4CxtType);
    switch (i4CxtType)
    {
        case VCM_L2_CONTEXT:
            L2DeleteVirtualContext (u4VcNum);
            break;

        case VCM_L3_CONTEXT:
            VcmIndicateL3CxtUpdate (u4VcNum, VCM_CONTEXT_DELETE);
            gVcmGlobals.u2L3ContextCount--;
            break;

        case VCM_L2_L3_CONTEXT:
            L2DeleteVirtualContext (u4VcNum);
            VcmIndicateL3CxtUpdate (u4VcNum, VCM_CONTEXT_DELETE);
            gVcmGlobals.u2L3ContextCount--;
            break;

        default:
            break;
    }

#if IP_WANTED
#ifdef NPAPI_WANTED
    MEMSET (&VcmVrMapInfo, 0, sizeof (tFsNpVcmVrMapInfo));
    if (VCM_NODE_STATUS () == VCM_RED_ACTIVE)
    {
        if ((i4CxtType == VCM_L2_CONTEXT) || (i4CxtType == VCM_L2_L3_CONTEXT))
        {
            if (VcmFsVcmHwDeleteContext (u4VcNum) != FNP_SUCCESS)
            {
                VCM_TRC (VCM_ALL_FAILURE_TRC,
                         "FsVcmHwDeleteContext: Failed to delete in Hardware.\n");
                return VCM_FAILURE;
            }
        }
        if ((i4CxtType == VCM_L3_CONTEXT) || (i4CxtType == VCM_L2_L3_CONTEXT))
        {
            VrMapAction = NP_HW_DELETE_VIRTUAL_ROUTER;
            VcmVrMapInfo.u4VrfId = u4VcNum;
            if (VcmFsVcmHwMapVirtualRouter (VrMapAction, &VcmVrMapInfo) ==
                FNP_FAILURE)
            {
                VCM_TRC (VCM_ALL_FAILURE_TRC,
                         "FsVcmHwMapVirtualRouter: Failed to delete virtual router in Hardware.\n");
                return VCM_FAILURE;
            }
        }
    }
#endif
#endif
#if defined (IP_WANTED) && defined (SNMP_3_WANTED)
    if (u4VcNum != VCM_DEFAULT_CONTEXT)
    {
        if (SnmpDeleteVirtualContext (u4VcNum) != SNMP_SUCCESS)
        {
            VCM_TRC (VCM_ALL_FAILURE_TRC,
                     "VcmDeleteVirtualcontext: Failed to delete in SNMPv3 module.\n");
            return VCM_FAILURE;
        }
    }
#endif
    VCM_LOCK ();

    VcmDeleteMemRelease (u4VcNum);
    VCM_UNLOCK ();

    VCM_TRC (VCM_MGMT_TRC,
             "VcmDeleteVirtualContextIncDefault : VCM Context Deleted Successfully .\n");

    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmAddToVcPortList                                   */
/*                                                                           */
/* Description        : Adds the node to the virtual contex port list.       */
/*                                                                           */
/* Input(s)           : pIfMapEntry - Pointer to IfMap Entry                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS / FAILURE.                                   */
/*****************************************************************************/
INT4
VcmAddToVcPortList (tVcmIfMapEntry * pIfMapEntry)
{
    tVirtualContextInfo *pVc = NULL;
    tVcPortEntry       *pPort = NULL;
    tVcPortEntry       *pPrev = NULL;
    tVcPortEntry       *pTempPort = NULL;
    tVirtualContextInfo Dummy;
    UINT2               u2Counter = 1;
    UINT1               u1AddFlag = VCM_FALSE;

    Dummy.u4VcNum = pIfMapEntry->u4VcNum;
    pVc = VcmFindVcEntry (&Dummy);

    if (NULL == pVc)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC | VCM_OS_RESOURCE_TRC,
                 "VcmAddToVcPortList : invalid IfMap\n");
        return FAILURE;
    }
    /* Check whether the port is already present in pVc->VcPortList. */
    TMO_SLL_Scan (&pVc->VcPortList, pTempPort, tVcPortEntry *)
    {
        if (VCM_IFMAP_HLPORTID (pTempPort->pIf) >
            VCM_IFMAP_HLPORTID (pIfMapEntry))
        {
            /* This port is not present in the VcPortList. */
            break;
        }
        else
        {
            /* Port is present in the VcPortList. So return success. */
            if (pTempPort->pIf == pIfMapEntry)
            {
                return SUCCESS;
            }
        }
    }

    if (VcmGetSystemMode (VCM_PROTOCOL_ID) == VCM_SI_MODE)
    {
        /* In case of SI both the IfIndex and Localport are same. */
        VCM_IFMAP_HLPORTID (pIfMapEntry) = (UINT2) (pIfMapEntry->u4IfIndex);
    }
    else
    {
        if ((VCM_NODE_STATUS () == VCM_RED_ACTIVE) || ((VCM_NODE_STATUS () == 0)
                                                       &&
                                                       (VcmGetSystemModeExt
                                                        (VCM_PROTOCOL_ID) ==
                                                        VCM_SI_MODE)))
        {
            VCM_IFMAP_HLPORTID (pIfMapEntry) = VCM_NEXTFREE_HLPORTID (pVc);
        }
        else if (VCM_NODE_STATUS () == VCM_RED_STANDBY)
        {
            /* HlPortId will be copied from sync up database in Standby */
            VCM_RED_GET_LOCAL_PORT_ID (pIfMapEntry->u4IfIndex,
                                       VCM_IFMAP_HLPORTID (pIfMapEntry));
        }
    }

    if (VCM_IFMAP_HLPORTID (pIfMapEntry) != 0)
    {
        pPort = VCM_ALLOCATE_VCPORTENTRY_MEMBLK ();
        if (pPort == NULL)
        {
            /*Unable to allocate memory */
            VCM_TRC (VCM_ALL_FAILURE_TRC | VCM_OS_RESOURCE_TRC,
                     "There is no memory for VC Port Entry in VcmAddToVcPortList\n");
            return FAILURE;
        }
        memset (pPort, 0, sizeof (tVcPortEntry));
        pPort->pIf = pIfMapEntry;
        pPrev = NULL;

        /* Check whether the new node to be inserted in middle. */
        TMO_SLL_Scan (&pVc->VcPortList, pTempPort, tVcPortEntry *)
        {
            if (VCM_IFMAP_HLPORTID (pTempPort->pIf) <
                VCM_IFMAP_HLPORTID (pIfMapEntry))
            {
                pPrev = pTempPort;
                continue;
            }
            else
            {
                /* New port is some where before the last node */
                TMO_SLL_Insert (&pVc->VcPortList, &pPrev->Next, &pPort->Next);
                u1AddFlag = VCM_TRUE;
                break;
            }
        }
        if ((pTempPort == NULL) && (u1AddFlag == VCM_FALSE))
        {
            /* Either list is NULL OR all existing ports are lesser than
             * the new port. Add the new port to the tail. */
            TMO_SLL_Add (&pVc->VcPortList, &pPort->Next);
        }

        /* In case of SI both the IfIndex and Localport are same. So 
         * there is no need to update the VCM_NEXTFREE_HLPORTID */
        if (VcmGetSystemMode (VCM_PROTOCOL_ID) != VCM_SI_MODE)
        {
            /*Update next free HlPort-Id */
            VCM_NEXTFREE_HLPORTID (pVc) = VCM_INVALID_HLPORT;
            u2Counter = 0;

            TMO_SLL_Scan (&pVc->VcPortList, pTempPort, tVcPortEntry *)
            {
                if (VCM_IFMAP_HLPORTID (pTempPort->pIf) != ++u2Counter)
                {
                    VCM_NEXTFREE_HLPORTID (pVc) = u2Counter;
                    break;
                }
            }

            if ((VCM_NEXTFREE_HLPORTID (pVc) == VCM_INVALID_HLPORT) &&
                (u2Counter < MAX_PORTS_PER_CXT))
            {
                VCM_NEXTFREE_HLPORTID (pVc) = ++u2Counter;
            }
        }
    }
    return SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmAddToIpIfList                                     */
/*                                                                           */
/* Description        : Adds the node to the specified context's  IP         */
/*                      interface list.                                      */
/*                                                                           */
/* Input(s)           : pIfMapEntry - Pointer to IfMap Entry                 */
/*                      u4ContextId - The context to which the entry needs   */
/*                                    to be added                            */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS / FAILURE.                                   */
/*****************************************************************************/
INT4
VcmAddToIpIfList (UINT4 u4ContextId, tVcmIfMapEntry * pIfMapEntry)
{
    tVirtualContextInfo Dummy;
    tVirtualContextInfo *pVc = NULL;
    tVcmIpIfMapEntry   *pIfNode = NULL;
    tVcmIpIfMapEntry   *pTmpIfNode = NULL;

    Dummy.u4VcNum = u4ContextId;
    pVc = VcmFindVcEntry (&Dummy);

    if (NULL == pVc)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC | VCM_OS_RESOURCE_TRC,
                 "VcmAddToVcPortList : invalid IfMap\n");
        return FAILURE;
    }

    /* Check whether the port is already present in pVc->VcPortList. */
    TMO_DLL_Scan (&pVc->VcIPIntfList, pTmpIfNode, tVcmIpIfMapEntry *)
    {
        if (pTmpIfNode->pIfMapEntry->u4IfIndex == pIfMapEntry->u4IfIndex)
        {
            return VCM_SUCCESS;
        }
    }

    pIfNode = VCM_ALLOCATE_IPIFENTRY_MEMBLK ();
    if (pIfNode == NULL)
    {
        /*Unable to allocate memory */
        VCM_TRC (VCM_ALL_FAILURE_TRC | VCM_OS_RESOURCE_TRC,
                 "There is no memory for tVcmIpIfMapEntry\n");
        return VCM_FAILURE;
    }

    MEMSET (pIfNode, 0, sizeof (tVcmIpIfMapEntry));
    TMO_DLL_Init_Node (&pIfNode->NextIfEntry);

    pIfNode->pIfMapEntry = pIfMapEntry;

    /* Check whether the new node to be inserted in middle. */
    TMO_DLL_Add (&pVc->VcIPIntfList, &pIfNode->NextIfEntry);

    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmDeleteFromVcPortList                              */
/*                                                                           */
/* Description        : Removes the node from the virtual contex port list.  */
/*                                                                           */
/* Input(s)           : pIfMapEntry - Pointer to IfMap Entry                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS / FAILURE.                                   */
/*****************************************************************************/
INT4
VcmDeleteFromVcPortList (tVcmIfMapEntry * pIfMapEntry)
{
    tVirtualContextInfo *pVc = NULL;
    tVcPortEntry       *pDelPort = NULL;
    tVcPortEntry       *pTempPort = NULL;
    tVirtualContextInfo Dummy;

    if (VCM_INVALID_VC == pIfMapEntry->u4VcNum)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC | VCM_OS_RESOURCE_TRC,
                 "VcmAddToVcPortList : Port not yet mapped to any context.\n");
        return SUCCESS;
    }

    Dummy.u4VcNum = pIfMapEntry->u4VcNum;
    pVc = VcmFindVcEntry (&Dummy);
    if (NULL == pVc)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC | VCM_OS_RESOURCE_TRC,
                 "VcmAddToVcPortList : invalid IfMap.\n");
        return FAILURE;
    }
    /* Check whether the new node to be inserted in middle. */
    TMO_SLL_Scan (&pVc->VcPortList, pTempPort, tVcPortEntry *)
    {
        if (pTempPort->pIf == pIfMapEntry)
        {
            pDelPort = pTempPort;
            break;
        }
    }

    if (NULL != pDelPort)
    {
        TMO_SLL_Delete (&pVc->VcPortList, &pDelPort->Next);

        /* In case of SI both the IfIndex and Localport are same. So 
         * there is no need to update the VCM_NEXTFREE_HLPORTID */
        if (VcmGetSystemMode (VCM_PROTOCOL_ID) != VCM_SI_MODE)
        {
            if ((VCM_NEXTFREE_HLPORTID (pVc) == VCM_INVALID_HLPORT) ||
                (VCM_NEXTFREE_HLPORTID (pVc) >
                 VCM_IFMAP_HLPORTID (pDelPort->pIf)))
            {
                VCM_NEXTFREE_HLPORTID (pVc) =
                    VCM_IFMAP_HLPORTID (pDelPort->pIf);
            }
        }

        if (VCM_RELEASE_VCPORTENTRY_MEMBLK (pDelPort) == VCM_MEM_FAILURE)
        {
            /*Unable to rel to memory pool */
            VCM_TRC (VCM_ALL_FAILURE_TRC | VCM_OS_RESOURCE_TRC,
                     "VcmDeleteFromVcPortList: There is something wrong in the node. Not able to release to mem pool. Some corruption ?\n");
            return FAILURE;
        }
    }
    return SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmRemoveIfMapEntriesForContext                      */
/*                                                                           */
/* Description        : Removes all the IfMap entries from the port list for */
/*                      the virtual context.                                 */
/*                                                                           */
/* Input(s)           : u4VcNum - virtual contex number                      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VcmRemoveIfMapEntriesForContext (UINT4 u4VcNum)
{
    tCfaIfInfo          CfaIfInfo;
    tVcmIfMapEntry     *pIfMapEntry = NULL;
    tVcmIfMapEntry     *pNextIf = NULL;
    UINT4               u4PortIfIndex = 0;

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
    VCM_LOCK ();
    pIfMapEntry = VcmGetFirstIfMap ();
    while (NULL != pIfMapEntry)
    {
        pNextIf = VcmGetNextIfMap (pIfMapEntry);
        if (VcmIsL3Interface (pIfMapEntry->u4IfIndex) == VCM_FALSE)
        {
            if (pIfMapEntry->u4VcNum == u4VcNum)
            {
                u4PortIfIndex = pIfMapEntry->u4IfIndex;
                VcmRemoveIfMapEntry (pIfMapEntry);
                VCM_RELEASE_IFMAPENTRY_MEMBLK (pIfMapEntry);

                if (VCM_NODE_STATUS () == VCM_RED_STANDBY)
                {
                    VcmRedClearSyncUpData (u4PortIfIndex);
                }

            }
        }
        else
        {
            /* There can be IP interface mapped to this context
             * or there can be IP interface whose l2 context is this context.
             */
            if ((pIfMapEntry->u4VcNum == u4VcNum) ||
                (pIfMapEntry->u4L2ContextId == u4VcNum))
            {
                MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
                VcmRemoveIfMapEntry (pIfMapEntry);
                CfaGetIfInfo (pIfMapEntry->u4IfIndex, &CfaIfInfo);
#ifdef LNXIP4_WANTED
                gapVcmIpIfMapEntry[(CfaIfInfo.u4IndexMgrPort) - VCM_ONE] = NULL;
#else
                gapVcmIpIfMapEntry[CfaIfInfo.i4IpPort] = NULL;
#endif
                VCM_RELEASE_IFMAPENTRY_MEMBLK (pIfMapEntry);
            }
        }
        pIfMapEntry = pNextIf;
    }
    VCM_UNLOCK ();
}

/*****************************************************************************/
/* Function Name      : VcmGetVCMacAddr                                      */
/*                                                                           */
/* Description        : Given the VC Id, the Mac Address of the VC is        */
/*                      stored in the Return val.                            */
/*                                                                           */
/* Input(s)           : i4VCId  - virtual contex number                      */
/*                                                                           */
/* Output(s)          : pu1BridgeAddr - MAC Address.                         */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS / FAILURE.                                   */
/*****************************************************************************/
INT1
VcmGetVCMacAddr (INT4 i4VCId, UINT1 *pu1BridgeAddr)
{
    tVirtualContextInfo *pVc = NULL;
    tVirtualContextInfo Dummy;

    VCM_MEMSET (pu1BridgeAddr, 0, VCM_MAC_ADDR_SIZE);

    if ((i4VCId < VCM_DEFAULT_CONTEXT) || (i4VCId > MAX_CXT_PER_SYS))
    {
        return FAILURE;
    }

    Dummy.u4VcNum = (UINT4) i4VCId;

    VCM_LOCK ();

    if (NULL != (pVc = VcmFindVcEntry (&Dummy)))
    {
        VCM_MEMCPY (pu1BridgeAddr, pVc->VcBridgeAddr, VCM_MAC_ADDR_SIZE);

        VCM_UNLOCK ();
        return SUCCESS;
    }

    VCM_UNLOCK ();
    return FAILURE;
}

/*****************************************************************************/
/* Function Name      : VcmValidateL3IfMapping                               */
/*                                                                           */
/* Description        : This function validates the mapping of specified     */
/*                      l3 interface to the specified l3 context             */
/*                                                                           */
/* Input(s)           : u4IfIndex    - the interface index for which mapping */
/*                                     is done                               */
/*                      u4ContextId  - The context to which mapping is done  */
/*                                                                           */
/* Output(s)          : pu4ErrorCode - The error code retuned based on       */
/*                                     validation                            */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE.                           */
/*****************************************************************************/
INT4
VcmValidateL3IfMapping (UINT4 u4IfIndex, UINT4 u4ContextId, UINT4 *pu4ErrorCode)
{

    tVcmIfMapEntry     *pIfMap = NULL;
    tVcmIfMapEntry      Dummy;
    UINT4               u4CurrVcId = 0;
    UINT1               u1RowStatus = 0;

    Dummy.u4IfIndex = u4IfIndex;
    VCM_LOCK ();
    pIfMap = VcmFindIfMapEntry (&Dummy);

    if (pIfMap == NULL)
    {
        VCM_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return VCM_FAILURE;
    }

    u4CurrVcId = pIfMap->u4VcNum;
    u1RowStatus = pIfMap->u1RowStatus;
    VCM_UNLOCK ();

    /* An interface mapped to default context can be mapped to a non-default 
     * context. An interface mapped to a non-default context can be mapped
     * to default context. Mapping from a non-default context to another
     * non-default context is not allowed.
     */
    if (u4CurrVcId == u4ContextId)
    {
        /* No change in context */
        return VCM_SUCCESS;
    }
    if ((u4CurrVcId != VCM_DEFAULT_CONTEXT) &&
        (u4ContextId != VCM_DEFAULT_CONTEXT))
    {
        CLI_SET_ERR (CLI_VCM_IPIF_MAP_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return VCM_FAILURE;
    }

    if ((u1RowStatus != NOT_IN_SERVICE) && (u1RowStatus != NOT_READY))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return VCM_FAILURE;
    }

    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmHandleL3IfaceMapping                              */
/*                                                                           */
/* Description        : This function handles L3 interface mapping and       */
/*                      unmapping. This function call IP and IP6 API         */
/*                      for interface mapping and unmapping updation         */
/*                                                                           */
/* Input(s)           : u4IfIndex    - the interface index for which mapping */
/*                                     or unmapping is done                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE.                           */
/*****************************************************************************/
INT4
VcmHandleL3IfaceMapping (UINT4 u4IfIndex)
{
    tVcmIfMapEntry      Dummy;
    tVcmIfMapEntry     *pIfMap = NULL;
    UINT4               u4CurrVcId = 0;
    UINT4               u4CfgVcId = 0;
    UINT1               u1MapType = 0;
#ifdef IP_WANTED
    UINT1               u1Found = VCM_FALSE;
    tVirtualContextInfo *pVc = NULL;
    tVcmIpIfMapEntry   *pIfNode = NULL;
    tVirtualContextInfo DummyVc;
#endif
    UINT1               u1CurrVcRowStatus = 0;
    UINT1               u1IfType = 0;
#if defined (IP_WANTED) && defined (NPAPI_WANTED)
    tVrMapAction        VrMapAction;
    tFsNpVcmVrMapInfo   VcmVrMapInfo;
#endif
#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    tLnxVrfEventInfo    LnxVrfEventInfo;
    tCfaIfInfo          IfInfo;
    INT4                i4SockFd = -1;
    INT4                i4IpPort = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
#endif

    Dummy.u4IfIndex = u4IfIndex;
    VCM_LOCK ();
    pIfMap = VcmFindIfMapEntry (&Dummy);

    if (pIfMap != NULL)
    {
        u4CurrVcId = pIfMap->u4VcNum;
        u4CfgVcId = pIfMap->u4CfgVcNum;
        u1CurrVcRowStatus = pIfMap->u1RowStatus;
#ifdef IP_WANTED
        pIfMap->u4VcNum = u4CfgVcId;
#endif
        pIfMap->u1RowStatus = ACTIVE;
    }
    VCM_UNLOCK ();

    if (u4CurrVcId == u4CfgVcId)
    {
        /* No change in VCId configuration.So dont do anything, simply return
         */
        return VCM_SUCCESS;
    }

    /* There is change is VCId configuration. It could be that an interface
     * mapped to non-default context is now unmapped from that context
     * or an interface is freshly mapped to a non-default context
     */
    CfaGetIfaceType (u4IfIndex, &u1IfType);
    if (u4CurrVcId == VCM_DEFAULT_CONTEXT)
    {
        /* Interface is currently mapped default context and the current VcNum
         * and configured VcNum is not same. This mean the interface is
         * freshly mapped to a non-defult context
         */
        u1MapType = VCM_IFACE_MAP;
    }
    else
    {
        /* Interface is currently mapped to a non-default context and the current
         * VcNum and configured VcNum is not same. This means that the interface
         * was mapped to non-default context and is being unmapped from that
         * context.
         */
        u1MapType = VCM_IFACE_UNMAP;
    }

    /* Update this mapping/unmapping to CFA as the ip address configured over
     * this interface needs to be cleared.
     */
#if defined (IP_WANTED) && defined (NPAPI_WANTED)
    MEMSET (&VcmVrMapInfo, 0, sizeof (tFsNpVcmVrMapInfo));
    VrMapAction = NP_HW_MAP_INVALID;

    if (u4CurrVcId == VCM_DEFAULT_CONTEXT)
    {
        /* Interface is currently mapped default context and the current VcNum
         * and configured VcNum is not same. This mean the interface is
         * freshly mapped to a non-defult context
         */
        if (u1IfType == CFA_ENET)
        {
            VrMapAction = NP_HW_MAP_RPORT_TO_VR;
        }
        else if (u1IfType == CFA_LOOPBACK)
        {
            VrMapAction = NP_HW_MAP_RPORT_TO_VR;
        }

        else if (u1IfType == CFA_L3IPVLAN)
        {
            VrMapAction = NP_HW_MAP_INTF_TO_VR;
            VcmVrMapInfo.u2VlanId = pIfMap->u2VlanId;
            VcmVrMapInfo.u4L2CxtId = pIfMap->u4L2ContextId;
        }
        VcmVrMapInfo.u4VrfId = u4CfgVcId;
        VcmGetVRMacAddr (u4CfgVcId, VcmVrMapInfo.MacAddr);
    }
    else
    {
        /* Interface is currently mapped to a non-default context and the current
         * VcNum and configured VcNum is not same. This means that the interface
         * was mapped to non-default context and is being unmapped from that
         * context.
         */
        if (u1IfType == CFA_ENET)
        {
            VrMapAction = NP_HW_UNMAP_RPORT_TO_VR;
        }
        else if (u1IfType == CFA_LOOPBACK)
        {
            VrMapAction = NP_HW_UNMAP_RPORT_TO_VR;
        }

        else if (u1IfType == CFA_L3IPVLAN)
        {
            VrMapAction = NP_HW_UNMAP_INTF_TO_VR;
            VcmVrMapInfo.u2VlanId = pIfMap->u2VlanId;
        }
        /* interface is unmapped from the Virtual context u4CurrVcId
         * configured virtual context in this case is default context
         * hence for unmapping pass the current context Id
         */
        VcmVrMapInfo.u4VrfId = u4CurrVcId;
        VcmGetVRMacAddr (u4CfgVcId, VcmVrMapInfo.MacAddr);
    }
    /* Update IP/IP6 about the interface mapping/unmapping. */
    VcmVrMapInfo.u4IfIndex = u4IfIndex;

    if (VCM_NODE_STATUS () == VCM_RED_ACTIVE)
    {
        if (VcmFsVcmHwMapVirtualRouter (VrMapAction, &VcmVrMapInfo) ==
            FNP_FAILURE)
        {
            pIfMap->u4VcNum = u4CurrVcId;
            pIfMap->u1RowStatus = u1CurrVcRowStatus;
            return VCM_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u1CurrVcRowStatus);
#endif

#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)

#ifdef LNXIP6_WANTED
    Lip6KernelDeleteAddrForIndex (u4IfIndex);
#endif

    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));
    /*Cfa Socket parameters filled */
    LnxVrfEventInfo.i4Sockdomain = PF_PACKET;
    LnxVrfEventInfo.i4SockType = SOCK_RAW;
    LnxVrfEventInfo.i4SockProto = htons (ETH_P_ALL);
    LnxVrfEventInfo.u4IfIndex = u4IfIndex;
    LnxVrfEventInfo.u1IfType = u1IfType;
    if (u1MapType == VCM_IFACE_MAP)
    {
        LnxVrfEventInfo.u4ContextId = u4CfgVcId;
        LnxVrfEventInfo.u1MsgType = LNX_VRF_IF_MAP_CONTEXT;
    }
    else
    {
        LnxVrfEventInfo.u1MsgType = LNX_VRF_IF_UNMAP_CONTEXT;
        LnxVrfEventInfo.u4ContextId = u4CurrVcId;
    }
    LnxVrfSockLock ();
    if (LnxVrfEventHandling (&LnxVrfEventInfo, &i4SockFd) != NETIPV4_SUCCESS)
    {
        LnxVrfSockUnLock ();
        return VCM_FAILURE;
    }
    LnxVrfSockUnLock ();

    /* Set the appropriate falgs for the interface using the socket */
    if ((u1IfType != CFA_L3IPVLAN) && (u1IfType != CFA_LOOPBACK)
        && (i4SockFd != -1))
    {
        CfaVrfUpdateInterfaceSockInfo (u4IfIndex, i4SockFd);
    }
    else
    {
        close (i4SockFd);
    }
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    i4IpPort = CfaGetIfIpPort (u4IfIndex);

    RtmActOnStaticRoutesForIfDeleteInCxt (u4CurrVcId, (UINT4) i4IpPort);

    CfaGetInterfaceNameFromIndex (u4IfIndex, au1IfName);

    if (CfaGetIfInfo (u4IfIndex, &IfInfo) == CFA_SUCCESS)
    {
        if ((IfInfo.u1IfAdminStatus == CFA_IF_UP)
            && (IfInfo.u1IfOperStatus == CFA_IF_UP))
        {
            LinuxIpUpdateInterfaceStatus (au1IfName, u1IfType, CFA_IF_UP);
        }
    }
#endif
#ifdef IP_WANTED
    IpIfaceMapping (u4CfgVcId, u4IfIndex, u1MapType);
#endif

#ifdef IP6_WANTED
#ifndef LNXIP4_WANTED
    if (u1MapType == VCM_IFACE_MAP)
    {
        Ip6IfaceMapping (u4CfgVcId, u4IfIndex, u1MapType);
    }
    else
    {
        Ip6IfaceMapping (u4CurrVcId, u4IfIndex, u1MapType);
    }
#endif
#endif
    CfaUpdateIfaceMapping (u4IfIndex, u4CurrVcId);
    CfaUpdateVrfInterfaceMac (u4IfIndex, u4CfgVcId);

#ifdef TUNNEL_WANTED
    /* Update VRF unmapping for tunl interface */
    CfaTnlIfaceUnMapping (u4IfIndex, u4CurrVcId);
#endif
    /* Delete the interface from the context DLL to which it is currently
     * mapped. Then add the interface to the context DLL to which it is 
     * getting newly mapped.
     */
#ifdef IP_WANTED
    VCM_LOCK ();
    DummyVc.u4VcNum = u4CurrVcId;
    pVc = VcmFindVcEntry (&DummyVc);
    if (pVc != NULL)
    {
        VCM_DLL_SCAN (&pVc->VcIPIntfList, pIfNode, tVcmIpIfMapEntry *)
        {
            if (pIfNode->pIfMapEntry->u4IfIndex == u4IfIndex)
            {
                u1Found = VCM_TRUE;
                break;
            }
        }
        if (u1Found == VCM_TRUE)
        {
            VCM_DLL_DELETE (&pVc->VcIPIntfList, &pIfNode->NextIfEntry);
        }
    }

    pVc = NULL;
    DummyVc.u4VcNum = u4CfgVcId;
    pVc = VcmFindVcEntry (&DummyVc);
    if (pVc != NULL)
    {
        if (NULL != pIfNode)
        {
            VCM_DLL_ADD (&pVc->VcIPIntfList, &pIfNode->NextIfEntry);
        }
    }

    VCM_UNLOCK ();
#endif
#ifdef MPLS_L3VPN_WANTED
    /* Post event to L3VPN about mapping event */
    if (u1MapType == VCM_IFACE_MAP)
    {
        L3VpnIfConfMapUnmapEventHandler (L3VPN_VCM_IF_MAP, (INT4) u4IfIndex,
                                         u4CfgVcId);
    }
    else
    {
        L3VpnIfConfMapUnmapEventHandler (L3VPN_VCM_IF_UNMAP, (INT4) u4IfIndex,
                                         u4CurrVcId);
    }
#endif
    VCM_TRC (VCM_MGMT_TRC,
             "VcmHandleL3IfaceMapping: L3 interface mapped Successfully. \n");

    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmGetVRMacAddr                                      */
/*                                                                           */
/* Description        : Given the VR Id, the Mac Address of the VR is        */
/*                      returned                                             */
/*                                                                           */
/* Input(s)           : i4VRId  - virtual router id                          */
/*                                                                           */
/* Output(s)          : pu1MacAddr - MAC Address.                            */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE.                           */
/*****************************************************************************/
INT1
VcmGetVRMacAddr (UINT4 u4VRId, UINT1 *pu1MacAddr)
{
    tVirtualContextInfo *pVc = NULL;
    tVirtualContextInfo Vc;

    VCM_MEMSET (pu1MacAddr, 0, VCM_MAC_ADDR_SIZE);

    if (u4VRId > VCM_MAX_L3_CONTEXT)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC,
                 "VcmGetVRMacAddr : Exceeds the maximum number of context. Returns Failure \n");
        return VCM_FAILURE;
    }

    Vc.u4VcNum = u4VRId;

    VCM_LOCK ();

    pVc = VcmFindVcEntry (&Vc);
    if (pVc != NULL)
    {
        VCM_MEMCPY (pu1MacAddr, pVc->VRMacAddr, VCM_MAC_ADDR_SIZE);

        VCM_UNLOCK ();
        VCM_TRC (VCM_CONTROL_PATH_TRC,
                 "VcmGetVRMacAddr obtained the MAC address of the given VR id and returns success");
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    VCM_TRC (VCM_ALL_FAILURE_TRC, "VcmGetVRMacAddr failed");
    return VCM_FAILURE;
}

/*****************************************************************************/
/* Function Name      : VcmDeleteMemRelease                                      */
/*                                                                           */
/* Description        : This function will delete the context and release the 
 *                      memory allocated                                    */
/*                                                     */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u4VcNum  - virtual router id                          */
/*                                                                           */
/* Output(s)          : .                            */
/*                                                                           */
/*****************************************************************************/
VOID
VcmDeleteMemRelease (UINT4 u4VcNum)
{
    tVirtualContextInfo Dummy;
    tVirtualContextInfo *pVc = NULL;
    tVcPortEntry       *pNode = NULL;
    tVcmIpIfMapEntry   *pIpIfNode = NULL;

    Dummy.u4VcNum = u4VcNum;
    pVc = VcmFindVcEntry (&Dummy);
    if (NULL == pVc)
    {
        /* Given VcNum is not there in Virtual context table. */
        VCM_TRC (VCM_ALL_FAILURE_TRC, "Vc already removed.\n");
        VCM_UNLOCK ();
        return;
    }

    /* Remove from table, delete context, rel free port list, rel mem */
    VcmRemoveVcEntry (pVc);

    /* Scan trhough the VcPortList and release the memory for port entries */
    while (NULL != (pNode = (tVcPortEntry *) VCM_SLL_GET (&pVc->VcPortList)))
    {
        VCM_RELEASE_VCPORTENTRY_MEMBLK (pNode);
        /*Irrespective of u4MemRet, continue the scanning of the list. */
    }

    /* Scan trhough the IpIfList and release the memory for port entries */
    while (NULL != (pIpIfNode =
                    (tVcmIpIfMapEntry *) VCM_DLL_GET (&pVc->VcIPIntfList)))
    {
        VCM_RELEASE_IPIFENTRY_MEMBLK (pIpIfNode);
        /*Irrespective of u4MemRet, continue the scanning of the list. */
    }

    VCM_RELEASE_VCINFO_MEMBLK (pVc);

    /* Update the Next-Free Context-Id */

    if ((gVcmGlobals.u2NextFreeCxtId == VCM_INVALID_VAL) ||
        (gVcmGlobals.u2NextFreeCxtId > u4VcNum))
    {
        gVcmGlobals.u2NextFreeCxtId = (UINT2) u4VcNum;
    }

    return;
}

#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
/*****************************************************************************/
/* Function Name      : VcmHandleStatusForLnxVrf                                */
/*                                                                           */
/* Description        : This function Handles the status from LinuxIp Vrf   */
/*                      creation or deletion                                */
/*                                                                           */
/*                                                                           */
/* Input(s)           : i4VRId  - virtual router id                          */
/*                                                                           */
/* Output(s)          : pu1MacAddr - MAC Address.                            */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE.                           */
/*****************************************************************************/
INT1
VcmHandleStatusForLnxVrf (UINT4 u4VrfId, UINT1 u1MsgType, UINT4 u4status,
                          UINT4 u4IfIndex)
{

    tVcmLnxvrfStatusQMsg *pMsg = NULL;

    VCM_ALLOC_LNX_VRF_MEM_BLOCK (pMsg, tVcmLnxvrfStatusQMsg);
    if (pMsg != NULL)
    {
        VCM_MEMSET (pMsg, VCM_INIT_VAL, sizeof (tVcmLnxvrfStatusQMsg));
        pMsg->u4VrfId = u4VrfId;
        pMsg->u1MsgType = u1MsgType;
        pMsg->u4Status = u4status;
        pMsg->u4IfIndex = u4IfIndex;

        if (VCM_SEND_TO_QUEUE (VCM_LNX_VRFQ_ID, (UINT1 *) &pMsg,
                               OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
        {
            VCM_RELEASE_LNX_VRF_MEM_BLOCK (pMsg);
            return VCM_FAILURE;
        }

        if (VCM_SEND_EVENT (VCM_TASK_ID, VCM_LNX_VRF_STATUS_EVENT) !=
            OSIX_SUCCESS)
        {
            VCM_RELEASE_LNX_VRF_MEM_BLOCK (pMsg);
            return VCM_FAILURE;
        }
    }
    return VCM_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : VcmCreateContextInNp                                */
/*                                                                           */
/* Description        : This function will configure the vrf created in NP   */
/*                                                     */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u4VcNum  - virtual router id                          */
/*                                                                           */
/* Output(s)          : .                            */
/*                                                                           */
/* Return Value(s)    : NONE.                           */
/****************************************************************************/
VOID
VcmCreateContextInNp (UINT4 u4VcNum)
{

    tVirtualContextInfo *pVc = NULL;
    tVirtualContextInfo Dummy;
#ifdef SNMP_3_WANTED
    tSNMP_OCTET_STRING_TYPE VcName;
#endif
    UINT1               u1CfgCxtType = 0;
    tMacAddr            MacAddr;

#ifdef NPAPI_WANTED
    tVrMapAction        VrMapAction;
    tFsNpVcmVrMapInfo   VcmVrMapInfo;
    UINT1               u1CurrCxtType = 0;
    UINT1               u1RowStatus = 0;
#endif

    MEMSET (MacAddr, 0, sizeof (tMacAddr));

    Dummy.u4VcNum = u4VcNum;

    VCM_LOCK ();
    pVc = VcmFindVcEntry (&Dummy);
    if (pVc == NULL)
    {
        VCM_UNLOCK ();
        return;
    }
#ifdef NPAPI_WANTED
    u1RowStatus = pVc->u1RowStatus;
    u1CurrCxtType = pVc->u1CurrCxtType;
#endif
    u1CfgCxtType = pVc->u1CfgCxtType;
    pVc->u1RowStatus = ACTIVE;
    pVc->u1CurrCxtType = pVc->u1CfgCxtType;

    if ((u1CfgCxtType == VCM_L3_CONTEXT) || (u1CfgCxtType == VCM_L2_L3_CONTEXT))
    {
        VcmIssGetVRMacAddr (u4VcNum, &(pVc->VRMacAddr));
        VCM_MEMCPY (MacAddr, pVc->VRMacAddr, MAC_ADDR_LEN);
    }

#ifdef SNMP_3_WANTED
    VcName.pu1_OctetList = pVc->au1Alias;
    VcName.i4_Length = (INT4) STRLEN (pVc->au1Alias);
#endif
    VCM_UNLOCK ();

#ifdef SNMP_3_WANTED
    VcName.pu1_OctetList = pVc->au1Alias;
    VcName.i4_Length = (INT4) STRLEN (pVc->au1Alias);
#endif

#ifdef NPAPI_WANTED
    MEMSET (&VcmVrMapInfo, 0, sizeof (tFsNpVcmVrMapInfo));
    VrMapAction = NP_HW_MAP_INVALID;
    if (VCM_NODE_STATUS () == VCM_RED_ACTIVE)
    {
        /* Create the context in hardware */
        if ((u1CfgCxtType == VCM_L2_CONTEXT) ||
            (u1CfgCxtType == VCM_L2_L3_CONTEXT))
        {
            if (VcmFsVcmHwCreateContext (u4VcNum) != FNP_SUCCESS)
            {
                pVc = NULL;
                Dummy.u4VcNum = u4VcNum;
                VCM_LOCK ();
                pVc = VcmFindVcEntry (&Dummy);
                if (pVc == NULL)
                {
                    VCM_UNLOCK ();
                    VCM_TRC (VCM_ALL_FAILURE_TRC,
                             "FsVcmHwCreateContext: Failed to create in Hardware.\n");
                    return;
                }

                pVc->u1RowStatus = u1RowStatus;
                pVc->u1CurrCxtType = u1CurrCxtType;
                VCM_UNLOCK ();
                VCM_TRC (VCM_ALL_FAILURE_TRC,
                         "FsVcmHwCreateContext: Failed to create in Hardware.\n");
                return;
            }
        }
        if ((u1CfgCxtType == VCM_L3_CONTEXT) ||
            (u1CfgCxtType == VCM_L2_L3_CONTEXT))
        {
            VrMapAction = NP_HW_CREATE_VIRTUAL_ROUTER;
            VcmVrMapInfo.u4VrfId = u4VcNum;
            VCM_MEMCPY (VcmVrMapInfo.MacAddr, MacAddr, MAC_ADDR_LEN);
            if (VcmFsVcmHwMapVirtualRouter (VrMapAction, &VcmVrMapInfo) ==
                FNP_FAILURE)
            {
                pVc = NULL;
                Dummy.u4VcNum = u4VcNum;
                VCM_LOCK ();
                pVc = VcmFindVcEntry (&Dummy);
                if (pVc == NULL)
                {
                    VCM_UNLOCK ();
                    VCM_TRC (VCM_ALL_FAILURE_TRC,
                             "FsVcmHwMapVirtualRouter: Failed to create virtual router in Hardware.\n");
                    return;
                }
                pVc->u1RowStatus = u1RowStatus;
                pVc->u1CurrCxtType = u1CurrCxtType;
                MEMSET (pVc->VRMacAddr, 0, sizeof (tMacAddr));
                VCM_UNLOCK ();
                VCM_TRC (VCM_ALL_FAILURE_TRC,
                         "FsVcmHwMapVirtualRouter: Failed to create virtual router in Hardware.\n");
                return;
            }
        }
    }
#endif

#ifdef SNMP_3_WANTED
    if (SnmpCreateVirtualContext (u4VcNum, &VcName) != SNMP_SUCCESS)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC,
                 "VcmCreateVirtualcontext: Failed to create in SNMPv3 module.\n");

#ifdef NPAPI_WANTED
        if (VCM_NODE_STATUS () == VCM_RED_ACTIVE)
        {
            if ((u1CfgCxtType == VCM_L2_CONTEXT) ||
                (u1CfgCxtType == VCM_L2_L3_CONTEXT))
            {
                if (VcmFsVcmHwDeleteContext (u4VcNum) != FNP_SUCCESS)
                {
                    VCM_TRC (VCM_ALL_FAILURE_TRC,
                             "FsVcmHwDeleteContext: Failed to delete in Hardware.\n");
                    return;
                }
            }
            if ((u1CfgCxtType == VCM_L3_CONTEXT) ||
                (u1CfgCxtType == VCM_L2_L3_CONTEXT))
            {
                VrMapAction = NP_HW_DELETE_VIRTUAL_ROUTER;
                VcmVrMapInfo.u4VrfId = u4VcNum;
                if (VcmFsVcmHwMapVirtualRouter (VrMapAction, &VcmVrMapInfo) ==
                    FNP_FAILURE)
                {
                    VCM_TRC (VCM_ALL_FAILURE_TRC,
                             "FsVcmHwMapVirtualRouter: Failed to delete virtual router in Hardware.\n");
                    return;
                }
            }
        }
#endif
    }
#endif

}

/*****************************************************************************/
/* Function Name      : VcmDeleteContextInNp                              */
/*                                                                           */
/* Description        : This function will delete the vrf created in NP   */
/*                                                     */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u4VcNum  - virtual router id                          */
/*                                                                           */
/* Output(s)          : .                            */
/*                                                                           */
/* Return Value(s)    : NONE.                           */
/****************************************************************************/
VOID
VcmDeleteContextInNp (UINT4 u4VcNum)
{

    INT4                i4CxtType = 0;
#ifdef NPAPI_WANTED
    tVrMapAction        VrMapAction;
    tFsNpVcmVrMapInfo   VcmVrMapInfo;
#endif
    if (u4VcNum > MAX_CXT_PER_SYS)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC,
                 "VcmDeleteVirtualcontext: Invalid virtual context No.\n");
        return;
    }

    if (VcmIsContextExist (u4VcNum) == VCM_FALSE)
    {
        /* Given VcNum is not there in Virtual context table. */
        VCM_TRC (VCM_ALL_FAILURE_TRC, "Vc already removed.\n");
        return;
    }

    /* Delete the Virtual context from l2/l3 modules based on the context type */
    VcmGetVcCxtType (u4VcNum, &i4CxtType);

#ifdef NPAPI_WANTED
    MEMSET (&VcmVrMapInfo, 0, sizeof (tFsNpVcmVrMapInfo));
    if (VCM_NODE_STATUS () == VCM_RED_ACTIVE)
    {
        if ((i4CxtType == VCM_L2_CONTEXT) || (i4CxtType == VCM_L2_L3_CONTEXT))
        {
            if (VcmFsVcmHwDeleteContext (u4VcNum) != FNP_SUCCESS)
            {
                VCM_TRC (VCM_ALL_FAILURE_TRC,
                         "FsVcmHwDeleteContext: Failed to delete in Hardware.\n");
                return;
            }
        }
        if ((i4CxtType == VCM_L3_CONTEXT) || (i4CxtType == VCM_L2_L3_CONTEXT))
        {
            VrMapAction = NP_HW_DELETE_VIRTUAL_ROUTER;
            VcmVrMapInfo.u4VrfId = u4VcNum;
            if (VcmFsVcmHwMapVirtualRouter (VrMapAction, &VcmVrMapInfo) ==
                FNP_FAILURE)
            {
                VCM_TRC (VCM_ALL_FAILURE_TRC,
                         "FsVcmHwMapVirtualRouter: Failed to delete virtual router in Hardware.\n");
                return;
            }
        }
    }
#endif
#ifdef SNMP_3_WANTED
    if (u4VcNum != VCM_DEFAULT_CONTEXT)
    {
        if (SnmpDeleteVirtualContext (u4VcNum) != SNMP_SUCCESS)
        {
            VCM_TRC (VCM_ALL_FAILURE_TRC,
                     "VcmDeleteVirtualcontext: Failed to delete in SNMPv3 module.\n");
            return;
        }
    }
#endif
    VCM_TRC (VCM_MGMT_TRC,
             "VcmDeleteVirtualContextIncDefault : VCM Context Deleted Successfully .\n");

    return;
}

/*****************************************************************************/
/* Function Name      : VcmIfMapContextInNp                                      */
/*                                                                           */
/* Description        : This function will map the interface to the context   */
/*                                                     */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u4VcNum  - virtual router id                          */
/*                                                                           */
/* Output(s)          : .                            */
/*                                                                           */
/* Return Value(s)    : NONE.                           */
/*****************************************************************************/
VOID
VcmIfMapContextInNp (UINT4 u4IfIndex)
{
    UINT4               u4CurrVcId = 0;
    UINT1               u1CurrVcRowStatus = 0;
    UINT4               u4CfgVcId = 0;
    UINT1               u1IfType = 0;
    UINT1               u1Found = VCM_FALSE;
    tVcmIfMapEntry     *pIfMap = NULL;
    tVirtualContextInfo *pVc = NULL;
    tVcmIfMapEntry      Dummy;
    tVcmIpIfMapEntry   *pIfNode = NULL;
    tVirtualContextInfo DummyVc;
#ifdef NPAPI_WANTED
    tVrMapAction        VrMapAction;
    tFsNpVcmVrMapInfo   VcmVrMapInfo;
#endif

    Dummy.u4IfIndex = u4IfIndex;
    VCM_LOCK ();
    pIfMap = VcmFindIfMapEntry (&Dummy);

    if (pIfMap != NULL)
    {
        u4CurrVcId = pIfMap->u4VcNum;
        u4CfgVcId = pIfMap->u4CfgVcNum;
        u1CurrVcRowStatus = pIfMap->u1RowStatus;
        pIfMap->u4VcNum = u4CfgVcId;
        pIfMap->u1RowStatus = ACTIVE;
    }
    VCM_UNLOCK ();

    CfaGetIfaceType (u4IfIndex, &u1IfType);
#ifdef NPAPI_WANTED
    MEMSET (&VcmVrMapInfo, 0, sizeof (tFsNpVcmVrMapInfo));
    VrMapAction = NP_HW_MAP_INVALID;

    if (u4CurrVcId == VCM_DEFAULT_CONTEXT)
    {
        if (u1IfType == CFA_ENET)
        {
            VrMapAction = NP_HW_UNMAP_RPORT_TO_VR;
        }
        else if (u1IfType == CFA_L3IPVLAN)
        {
            VrMapAction = NP_HW_UNMAP_INTF_TO_VR;
            VcmVrMapInfo.u2VlanId = pIfMap->u2VlanId;
        }

        VcmVrMapInfo.u4VrfId = u4CurrVcId;
        VcmGetVRMacAddr (u4CfgVcId, VcmVrMapInfo.MacAddr);
        VcmVrMapInfo.u4IfIndex = u4IfIndex;
        if (VCM_NODE_STATUS () == VCM_RED_ACTIVE)
        {
            if (VcmFsVcmHwMapVirtualRouter (VrMapAction, &VcmVrMapInfo) ==
                FNP_FAILURE)
            {
                pIfMap->u4VcNum = u4CurrVcId;
                pIfMap->u1RowStatus = u1CurrVcRowStatus;
                return VCM_FAILURE;
            }
        }

        /* Interface is currently mapped default context and the current VcNum
         *          * and configured VcNum is not same. This mean the interface is
         *                   * freshly mapped to a non-defult context
         *                            */

        if (u1IfType == CFA_ENET)
        {
            VrMapAction = NP_HW_MAP_RPORT_TO_VR;
        }
        else if (u1IfType == CFA_L3IPVLAN)
        {
            VrMapAction = NP_HW_MAP_INTF_TO_VR;
            VcmVrMapInfo.u2VlanId = pIfMap->u2VlanId;
            VcmVrMapInfo.u4L2CxtId = pIfMap->u4L2ContextId;
        }
        VcmVrMapInfo.u4VrfId = u4CfgVcId;
        VcmGetVRMacAddr (u4CfgVcId, VcmVrMapInfo.MacAddr);
    }
    else
    {
        /* Interface is currently mapped to a non-default context and the current
         *          * VcNum and configured VcNum is not same. This means that the interface
         *                   * was mapped to non-default context and is being unmapped from that
         *                            * context.
         *                                     */
        if (u1IfType == CFA_ENET)
        {
            VrMapAction = NP_HW_UNMAP_RPORT_TO_VR;
        }
        else if (u1IfType == CFA_L3IPVLAN)
        {
            VrMapAction = NP_HW_UNMAP_INTF_TO_VR;
            VcmVrMapInfo.u2VlanId = pIfMap->u2VlanId;
        }
        /* interface is unmapped from the Virtual context u4CurrVcId
         *          * configured virtual context in this case is default context
         *                   * hence for unmapping pass the current context Id
         *                            */
        VcmVrMapInfo.u4VrfId = u4CurrVcId;
        VcmGetVRMacAddr (u4CfgVcId, VcmVrMapInfo.MacAddr);

        VcmVrMapInfo.u4IfIndex = u4IfIndex;
        if (VCM_NODE_STATUS () == VCM_RED_ACTIVE)
        {
            if (VcmFsVcmHwMapVirtualRouter (VrMapAction, &VcmVrMapInfo) ==
                FNP_FAILURE)
            {
                pIfMap->u4VcNum = u4CurrVcId;
                pIfMap->u1RowStatus = u1CurrVcRowStatus;
                return VCM_FAILURE;
            }
        }
        if (u1IfType == CFA_ENET)
        {
            VrMapAction = NP_HW_MAP_RPORT_TO_VR;
        }
        else if (u1IfType == CFA_L3IPVLAN)
        {
            VrMapAction = NP_HW_MAP_INTF_TO_VR;
            VcmVrMapInfo.u2VlanId = pIfMap->u2VlanId;
        }

        VcmVrMapInfo.u4VrfId = VCM_DEFAULT_CONTEXT;
        VcmGetVRMacAddr (u4CfgVcId, VcmVrMapInfo.MacAddr);

    }
    /* Update IP/IP6 about the interface mapping/unmapping. */
    VcmVrMapInfo.u4IfIndex = u4IfIndex;

    if (VCM_NODE_STATUS () == VCM_RED_ACTIVE)
    {
        if (VcmFsVcmHwMapVirtualRouter (VrMapAction, &VcmVrMapInfo) ==
            FNP_FAILURE)
        {
            pIfMap->u4VcNum = u4CurrVcId;
            pIfMap->u1RowStatus = u1CurrVcRowStatus;
            return;
        }
    }
#else
    UNUSED_PARAM (u1CurrVcRowStatus);
#endif
    VCM_LOCK ();
    DummyVc.u4VcNum = u4CurrVcId;
    pVc = VcmFindVcEntry (&DummyVc);
    if (pVc != NULL)
    {
        VCM_DLL_SCAN (&pVc->VcIPIntfList, pIfNode, tVcmIpIfMapEntry *)
        {
            if (pIfNode->pIfMapEntry->u4IfIndex == u4IfIndex)
            {
                u1Found = VCM_TRUE;
                break;
            }
        }
        if (u1Found == VCM_TRUE)
        {
            VCM_DLL_DELETE (&pVc->VcIPIntfList, &pIfNode->NextIfEntry);
        }
    }

    pVc = NULL;
    DummyVc.u4VcNum = u4CfgVcId;
    pVc = VcmFindVcEntry (&DummyVc);
    if (pVc != NULL)
    {
        if (NULL != pIfNode)
        {
            VCM_DLL_ADD (&pVc->VcIPIntfList, &pIfNode->NextIfEntry);
        }
    }

    VCM_UNLOCK ();
    return;

}
#endif
#endif
