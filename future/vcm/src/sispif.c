/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/*$Id: sispif.c,v 1.6 2014/01/24 12:29:05 siva Exp $                          */
/* Licensee Aricent Inc., 2009-2010                                          */
/*****************************************************************************/
/*    FILE  NAME            : sispif.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc   .                                */
/*    SUBSYSTEM NAME        : VCM module                                     */
/*    MODULE NAME           : VCM module                                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 01 Mar 2009                                    */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains the logical port related    */
/*                            functions of the sisp feature.                 */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    30 JAN 2009 / MITeam   Initial Create.                         */
/*---------------------------------------------------------------------------*/
#ifndef _SISPIF_C
#define _SISPIF_C

#include "vcminc.h"

/*****************************************************************************/
/* Function Name      : VcmSispCreatePortMapEntry                            */
/*                                                                           */
/* Description        : This function creates an VcmIfMapEntry for a         */
/*                      secondary context mapping in the SispLogicalPort     */
/*                      table.                                               */
/*                                                                           */
/* Input(s)           : u4PhyIfIndex   - Physical/Port ChannelPort Number    */
/*                      u4ContextId    - Context ID                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS/VCM_FAILURE                             .*/
/*****************************************************************************/
INT4
VcmSispCreatePortMapEntry (UINT4 u4PhyIfIndex, UINT4 u4ContextId)
{
    tVcmIfMapEntry     *pIfMapEntry = NULL;
    tVcmIfMapEntry      Dummy;

    VCM_MEMSET (&Dummy, VCM_INIT_VAL, sizeof (tVcmIfMapEntry));

    Dummy.u4PhyIfIndex = u4PhyIfIndex;
    Dummy.u4VcNum = u4ContextId;

    VCM_LOCK ();
    pIfMapEntry = VcmSispFindPortMapEntry (&Dummy);
    if (pIfMapEntry == NULL)
    {
        /*Given ifIndex does not exist in the table. Create it. */
        pIfMapEntry = VCM_ALLOCATE_IFMAPENTRY_MEMBLK ();
        if (pIfMapEntry == NULL)
        {
            VCM_UNLOCK ();
            /*Unable to allocate memory */
            VCM_TRC (VCM_OS_RESOURCE_TRC, "VcmSispCreatePortMapEntry: There is"
                     "no memory for VcmIfMap Entry\n");
            return VCM_FAILURE;
        }

        VCM_MEMSET (pIfMapEntry, VCM_INIT_VAL, sizeof (tVcmIfMapEntry));

        /* Initialize the IfEntry and add it to the SISP's Logical 
         * port table. */

        pIfMapEntry->u4PhyIfIndex = u4PhyIfIndex;
        pIfMapEntry->u4VcNum = u4ContextId;
        pIfMapEntry->u4IfIndex = VCM_INVALID_VAL;
        pIfMapEntry->u2HlPortId = VCM_INVALID_VAL;
        pIfMapEntry->u1RowStatus = NOT_READY;

        if (RB_FAILURE == VCM_RB_TREE_ADD (SISP_LOGICAL_PORT_TABLE,
                                           (tVcmRBElem *) pIfMapEntry))
        {
            VCM_RELEASE_IFMAPENTRY_MEMBLK (pIfMapEntry);
            VCM_UNLOCK ();
            VCM_TRC (VCM_ALL_FAILURE_TRC, "VcmSispCreatePortMapEntry: Add to "
                     "SispIfMap table failed.\n");
            return VCM_FAILURE;
        }
    }
    VCM_UNLOCK ();

    VCM_TRC_ARG2 (VCM_CONTROL_PATH_TRC, "VcmSispCreatePortMapEntry: successful"
                  "for PhysicalPort %u in Context %u\n", u4PhyIfIndex,
                  u4ContextId);

    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispDeletePortMapEntry                            */
/*                                                                           */
/* Description        : This function deletes an VcmIfMapEntry for a         */
/*                      secondary context mapping in the SispLogicalPort     */
/*                      table.                                               */
/*                                                                           */
/* Input(s)           : u4PhyIfIndex   - Physical/Port ChannelPort Number    */
/*                      u4ContextId    - Context ID                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS/VCM_FAILURE                             .*/
/*****************************************************************************/
INT4
VcmSispDeletePortMapEntry (UINT4 u4PhyIfIndex, UINT4 u4ContextId)
{
    tVcmIfMapEntry     *pIfMapEntry = NULL;
    tVcmIfMapEntry      Dummy;

    VCM_MEMSET (&Dummy, VCM_INIT_VAL, sizeof (tVcmIfMapEntry));

    Dummy.u4PhyIfIndex = u4PhyIfIndex;
    Dummy.u4VcNum = u4ContextId;

    VCM_LOCK ();

    if (NULL == (pIfMapEntry = VcmSispFindPortMapEntry (&Dummy)))
    {
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }

    /* Remove from Logical Port Table */
    if (RB_FAILURE ==
        VCM_RB_TREE_REMOVE (SISP_LOGICAL_PORT_TABLE,
                            (tVcmRBElem *) pIfMapEntry))
    {
        VCM_UNLOCK ();
        VCM_TRC (VCM_ALL_FAILURE_TRC, "VcmIfMap: Remove IfMap Entry failed.\n");
        return VCM_FAILURE;
    }

    /* Node should be removed both from the LogicalPort RBTree & VcmIfMap 
     * RBTree. Now free the memory */

    VCM_RELEASE_IFMAPENTRY_MEMBLK (pIfMapEntry);

    VCM_UNLOCK ();

    VCM_TRC_ARG2 (VCM_CONTROL_PATH_TRC, "VcmSispDeletePortMapEntry: successful"
                  "for PhysicalPort %u in Context %u\n", u4PhyIfIndex,
                  u4ContextId);

    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispFindPortMapEntry                              */
/*                                                                           */
/* Description        : Returns a pointer to a IfMap Entry for a given key   */
/*                                                                           */
/* Input(s)           : pNode - pointer to the element to be found           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : pointer to the node if key found. NULL if not found  */
/*****************************************************************************/
VOID               *
VcmSispFindPortMapEntry (VOID *pNode)
{
    tRBElem            *pEle = NULL;
    /* pNode is a dummy node in which ifIndex is filled. */
    pEle = VCM_RB_TREE_GET (SISP_LOGICAL_PORT_TABLE, pNode);
    return pEle;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmSispSetPortMapIfIndex                           */
/*                                                                           */
/*     DESCRIPTION      : This function will set the Logical port Index of   */
/*                        the physical ifindex in a context.                 */
/*                                                                           */
/*     INPUT            : u4PhyIfIndex      - Physical Interface Index.      */
/*                        u4ContextId       - Context Identifier             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmSispSetPortMapIfIndex (UINT4 u4PhyIfIndex, UINT4 u4ContextId,
                          UINT4 u4IfIndex)
{
    tVcmIfMapEntry     *pIfMap = NULL;
    tVcmIfMapEntry      Dummy;

    VCM_MEMSET (&Dummy, VCM_INIT_VAL, sizeof (tVcmIfMapEntry));

    Dummy.u4PhyIfIndex = u4PhyIfIndex;
    Dummy.u4VcNum = u4ContextId;

    VCM_LOCK ();

    if (NULL != (pIfMap = VcmSispFindPortMapEntry (&Dummy)))
    {
        pIfMap->u4IfIndex = u4IfIndex;

        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }
    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmSispSetPortMapStatus                            */
/*                                                                           */
/*     DESCRIPTION      : This function will set the RowStatus for the       */
/*                        interface.                                         */
/*                                                                           */
/*     INPUT            : u4IfIndex      - Interface Index.                  */
/*                                                                           */
/*     OUTPUT           : i4RowStatus    - RowStatus.                        */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmSispSetPortMapStatus (UINT4 u4PhyIfIndex, UINT4 u4ContextId,
                         UINT1 u1RowStatus)
{
    tVcmIfMapEntry     *pIfMap = NULL;
    tVcmIfMapEntry      Dummy;

    VCM_MEMSET (&Dummy, VCM_INIT_VAL, sizeof (tVcmIfMapEntry));

    Dummy.u4PhyIfIndex = u4PhyIfIndex;
    Dummy.u4VcNum = u4ContextId;

    VCM_LOCK ();

    if (NULL != (pIfMap = VcmSispFindPortMapEntry (&Dummy)))
    {
        pIfMap->u1RowStatus = u1RowStatus;

        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmSispCreateSispPortInIfMapTable                  */
/*                                                                           */
/*     DESCRIPTION      : This function will create an entry for the logical */
/*                        port in the VCM. Also higher layers will be        */
/*                        indicated to create the logical port.              */
/*                                                                           */
/*     INPUT            : u4PhyIfIndex   - Interface Index.                  */
/*                        u4ContextId    - Context Identifier                */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmSispCreateSispPortInIfMapTable (UINT4 u4PhyIfIndex, UINT4 u4ContextId)
{
    tVcmIfMapEntry     *pIfMapEntry = NULL;
    tVcmIfMapEntry      Dummy;

    VCM_MEMSET (&Dummy, VCM_INIT_VAL, sizeof (tVcmIfMapEntry));

    Dummy.u4PhyIfIndex = u4PhyIfIndex;
    Dummy.u4VcNum = u4ContextId;

    VCM_LOCK ();

    if (NULL == (pIfMapEntry = VcmSispFindPortMapEntry (&Dummy)))
    {
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }

    if (FAILURE == VcmAddIfMapEntry (pIfMapEntry))
    {
        VCM_UNLOCK ();
        VCM_TRC (VCM_OS_RESOURCE_TRC,
                 "VcmRowCreate: Unable to add pIfMapEntry to table\n");
        return VCM_FAILURE;
    }

    VCM_UNLOCK ();
    return VCM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmSispDeleteSispPortFromIfMapTable                */
/*                                                                           */
/*     DESCRIPTION      : This function will delete an entry for the logical */
/*                        port from the VCM.                                 */
/*                                                                           */
/*     INPUT            : u4PhyIfIndex   - Interface Index.                  */
/*                        u4ContextId    - Context Identifier                */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmSispDeleteSispPortFromIfMapTable (UINT4 u4PhyIfIndex, UINT4 u4ContextId)
{
    tVcmIfMapEntry     *pIfMapEntry = NULL;
    tVcmIfMapEntry      Dummy;
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo DummyCtx;
#ifdef NPAPI_WANTED
    tCfaIfInfo          IfInfo;
#endif

    VCM_MEMSET (&Dummy, VCM_INIT_VAL, sizeof (tVcmIfMapEntry));
    VCM_MEMSET (&DummyCtx, VCM_INIT_VAL, sizeof (tVcmIfMapEntry));

    Dummy.u4PhyIfIndex = u4PhyIfIndex;
    Dummy.u4VcNum = u4ContextId;

    VCM_LOCK ();

    if (NULL == (pIfMapEntry = VcmSispFindPortMapEntry (&Dummy)))
    {
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }

#ifdef NPAPI_WANTED
    if (VcmCfaGetIfInfo (u4PhyIfIndex, &IfInfo) == CFA_SUCCESS)
    {
        if ((IfInfo.u1IfType != CFA_LAGG) && (IfInfo.u1IfRowStatus == ACTIVE))
        {
            if (VCM_IS_NP_PROGRAMMING_ALLOWED () == VCM_TRUE)
            {
                if (VcmFsVcmHwUnMapPortFromContext (pIfMapEntry->u4VcNum,
                                                    pIfMapEntry->u4IfIndex)
                    == FNP_FAILURE)
                {
                    VCM_UNLOCK ();
                    VCM_TRC (VCM_ALL_FAILURE_TRC, "VcmSispDeleteSispPortFrom"
                             "IfMapTable function failed in removal from HW\n");
                    return VCM_FAILURE;
                }
            }
        }
    }
#endif

    if (FAILURE == VcmDeleteFromVcPortList (pIfMapEntry))
    {
        VCM_UNLOCK ();
        VCM_TRC (VCM_ALL_FAILURE_TRC | VCM_OS_RESOURCE_TRC,
                 "VcmDeleteFromVcPortList function return Failure.\n");
        return VCM_FAILURE;
    }

    if (FAILURE == VcmRemoveIfMapEntry (pIfMapEntry))
    {
        VCM_UNLOCK ();
        VCM_TRC (VCM_OS_RESOURCE_TRC,
                 "VcmRowCreate: Unable to add pIfMapEntry to table\n");
        return VCM_FAILURE;
    }

    if (VCM_IS_NP_PROGRAMMING_ALLOWED () == VCM_TRUE)
    {
        if (VcmRedSendSyncMessages (pIfMapEntry->u4IfIndex, 0,
                                    VCM_RED_CLEAR_SYNCUP_DATA) != VCM_SUCCESS)
        {
            VCM_TRC (VCM_ALL_FAILURE_TRC | VCM_CONTROL_PATH_TRC,
                     "VCMRED: Vcm Send Sync Message to Standby Failed\n");
        }
    }
    /* Decrement the Port Count */

    DummyCtx.u4VcNum = u4ContextId;

    if (NULL != (pVcInfo = VcmFindVcEntry (&DummyCtx)))
    {
        pVcInfo->u2PortCount--;

        /* Decrement the SISP port count for the secondary Context */
        pVcInfo->u4SispPortCount--;
    }

    VCM_UNLOCK ();

    return VCM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmSispDeleteSispPort                              */
/*                                                                           */
/*     DESCRIPTION      : This function will delete an entry for the sisp    */
/*                        port in the VCM. Also higher layers will be        */
/*                        indicated to delete the logical port.              */
/*                                                                           */
/*     INPUT            : u4PhyIfIndex   - Interface Index.                  */
/*                        u4ContextId    - Context Identifier                */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmSispDeleteSispPort (UINT4 u4PhyIfIndex, UINT4 u4ContextId)
{
    tVcmIfMapEntry      VcmIfMapEntry;

    VCM_MEMSET (&VcmIfMapEntry, VCM_INIT_VAL, sizeof (tVcmIfMapEntry));

    if (VcmSispPortMapEntryGet (u4PhyIfIndex, u4ContextId, &VcmIfMapEntry)
        == VCM_FAILURE)
    {
        VCM_TRC_ARG2 (VCM_CONTROL_PATH_TRC | VCM_ALL_FAILURE_TRC,
                      "No logical port exist for the physical port %u in "
                      "context %u\n", u4PhyIfIndex, u4ContextId);
        return VCM_FAILURE;
    }

    if (VcmIfMapEntry.u1RowStatus == ACTIVE)
    {
        /* Delete indication to Higher layers  for the SISP port */
        L2DeleteInterface (VcmIfMapEntry.u4IfIndex);

        /* Clear the entry in VCM's IFMAP entry */
        if (VcmSispDeleteSispPortFromIfMapTable (u4PhyIfIndex, u4ContextId)
            == VCM_FAILURE)
        {
            VCM_TRC_ARG2 (VCM_CONTROL_PATH_TRC | VCM_ALL_FAILURE_TRC,
                          "Deletion of logical port of Physical port %u in "
                          "context %u failed in IfMap deletion\n", u4PhyIfIndex,
                          u4ContextId);
            return VCM_FAILURE;
        }

        VcmSispCfaSispResetInterfaceProp (VcmIfMapEntry.u4IfIndex);
    }

    /* Clear the entry in SISP's logical port map entry */
    if (VcmSispDeletePortMapEntry (u4PhyIfIndex, u4ContextId) != VCM_SUCCESS)
    {
        VCM_TRC_ARG2 (VCM_CONTROL_PATH_TRC | VCM_ALL_FAILURE_TRC,
                      "Deletion of logical port of Physical port %u in "
                      "context %u failed in PortMap entry deletion\n",
                      u4PhyIfIndex, u4ContextId);
        return VCM_FAILURE;
    }
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispDeleteAllSispPorts                            */
/*                                                                           */
/* Description        : This function used to delete all the logical ports   */
/*                      configured in the system.                            */
/*                                                                           */
/* Input(s)           :                                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispDeleteAllSispPorts (VOID)
{
    tVcmIfMapEntry      VcmIfMapEntry;
    tVcmIfMapEntry      VcmNextIfMapEntry;

    VCM_MEMSET (&VcmNextIfMapEntry, VCM_INIT_VAL, sizeof (tVcmIfMapEntry));
    VCM_MEMSET (&VcmIfMapEntry, VCM_INIT_VAL, sizeof (tVcmIfMapEntry));

    /* Getting the First entry */
    if (VcmSispPortMapEntryGetNext (0, 0, &VcmIfMapEntry) == VCM_FAILURE)
    {
        return VCM_SUCCESS;
    }

    for (;;)
    {
        VcmSispDeleteSispPort (VcmIfMapEntry.u4PhyIfIndex,
                               VcmIfMapEntry.u4VcNum);

        if (VcmSispPortMapEntryGetNext (VcmIfMapEntry.u4PhyIfIndex,
                                        VcmIfMapEntry.u4VcNum,
                                        &VcmNextIfMapEntry) == VCM_FAILURE)
        {
            break;
        }

        VCM_MEMCPY (&VcmIfMapEntry, &VcmNextIfMapEntry,
                    sizeof (tVcmIfMapEntry));
    }
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      :  VcmSispPortMapEntryGetNext                          */
/*                                                                           */
/* Description        :  This function used to the get the next SISP logical */
/*                       port map entry of the given IfIndex and ContextId   */
/*                                                                           */
/* Input(s)           :  u4PhyIfIndex  - Physical or PortChannel IfIndex     */
/*                       u4ContextId   - Context Identifier                  */
/*                                                                           */
/* Output(s)          :  pRetVcmIfmapEntry - Pointer to the Logical Port     */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispPortMapEntryGetNext (UINT4 u4PhyIfIndex, UINT4 u4ContextId,
                            tVcmIfMapEntry * pRetVcmIfmapEntry)
{
    tVcmIfMapEntry     *pVcmIfMapEntry = NULL;
    tVcmIfMapEntry      VcmIfMapEntry;

    VCM_MEMSET (&VcmIfMapEntry, VCM_INIT_VAL, sizeof (tVcmIfMapEntry));

    VcmIfMapEntry.u4PhyIfIndex = u4PhyIfIndex;
    VcmIfMapEntry.u4VcNum = u4ContextId;

    VCM_LOCK ();

    pVcmIfMapEntry = (tVcmIfMapEntry *) VCM_RB_TREE_GETNEXT
        (SISP_LOGICAL_PORT_TABLE, &VcmIfMapEntry, NULL);

    if (pVcmIfMapEntry == NULL)
    {
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }

    VCM_MEMCPY (pRetVcmIfmapEntry, pVcmIfMapEntry, sizeof (tVcmIfMapEntry));

    VCM_UNLOCK ();
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      :  VcmSispPortMapEntryGet                              */
/*                                                                           */
/* Description        :  This function used to the get the SISP logical      */
/*                       port map entry of the given IfIndex and ContextId   */
/*                                                                           */
/* Input(s)           :  u4PhyIfIndex  - Physical or PortChannel IfIndex     */
/*                       u4ContextId   - Context Identifier                  */
/*                                                                           */
/* Output(s)          :  pRetVcmIfmapEntry - Pointer to the Logical Port     */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispPortMapEntryGet (UINT4 u4PhyIfIndex, UINT4 u4ContextId,
                        tVcmIfMapEntry * pRetVcmIfmapEntry)
{
    tVcmIfMapEntry     *pVcmIfMapEntry = NULL;
    tVcmIfMapEntry      VcmIfMapEntry;

    VCM_MEMSET (&VcmIfMapEntry, VCM_INIT_VAL, sizeof (tVcmIfMapEntry));

    VcmIfMapEntry.u4PhyIfIndex = u4PhyIfIndex;
    VcmIfMapEntry.u4VcNum = u4ContextId;

    VCM_LOCK ();

    pVcmIfMapEntry =
        (tVcmIfMapEntry *) VCM_RB_TREE_GET (SISP_LOGICAL_PORT_TABLE,
                                            &VcmIfMapEntry);
    if (pVcmIfMapEntry == NULL)
    {
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }

    VCM_MEMCPY (pRetVcmIfmapEntry, pVcmIfMapEntry, sizeof (tVcmIfMapEntry));

    VCM_UNLOCK ();
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispMakeSispIntfActive                            */
/*                                                                           */
/* Description        : This function configures the row status as ACTIVE    */
/*                      for the provided entry in the corresponding          */
/*                      secondary context.                                   */
/*                                                                           */
/* Input(s)           : pVcmIfMapEntry - Pointer to VCMap Entry              */
/*                      u4ContextId    - Context ID                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS/VCM_FAILURE                             .*/
/*****************************************************************************/
INT4
VcmSispMakeSispIntfActive (tVcmIfMapEntry * pVcmIfMapEntry, UINT4 u4ContextId)
{
    /* Create the logical port in VCM's IfMap table */
    VcmSispCreateSispPortInIfMapTable (pVcmIfMapEntry->u4PhyIfIndex,
                                       u4ContextId);

    /* Port Map indication to CFA */
    VcmSispCfaCopyPhysicalIfaceProperty (pVcmIfMapEntry->u4IfIndex,
                                         pVcmIfMapEntry->u4PhyIfIndex);

    if ((VCM_IS_NP_PROGRAMMING_ALLOWED () == VCM_TRUE) ||
        (VCM_RED_RM_GET_STATIC_CONFIG_STATUS () != RM_STATIC_CONFIG_RESTORED))
    {
        VcmSispSetPortMapStatus (pVcmIfMapEntry->u4PhyIfIndex,
                                 u4ContextId, ACTIVE);

        if (VcmCreateInterface (pVcmIfMapEntry->u4IfIndex) != VCM_SUCCESS)
        {
            return VCM_FAILURE;
        }

        /* Increment the number of ports mapped to this context */
        VcmIncrementPortCount (u4ContextId);
    }

    /* Increment the SISP Port Count for the Primary Context */
    VcmIncrementSispPortCount (u4ContextId);

    return VCM_SUCCESS;
}
#endif
