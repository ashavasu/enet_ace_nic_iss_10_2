/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vcmsz.c,v 1.2 2013/11/29 11:04:15 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _VCMSZ_C
#include "vcminc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
VcmSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < VCM_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal = MemCreateMemPool (FsVCMSizingParams[i4SizingId].u4StructSize,
                                     FsVCMSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(VCMMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            VcmSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
VcmSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsVCMSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, VCMMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
VcmSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < VCM_MAX_SIZING_ID; i4SizingId++)
    {
        if (VCMMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (VCMMemPoolIds[i4SizingId]);
            VCMMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
