/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/*$Id: sisppvc.c,v 1.6 2014/12/09 12:46:07 siva Exp $                          */
/* Licensee Aricent Inc., 2009-2010                                          */
/*****************************************************************************/
/*    FILE  NAME            : sisppvc.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc   .                                */
/*    SUBSYSTEM NAME        : VCM module                                     */
/*    MODULE NAME           : VCM module                                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 01 Mar 2009                                    */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains the functions for Port Vlan */
/*                            mapping functionalities of SISP feature.       */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    30 JAN 2009 / MITeam   Initial Create.                         */
/*---------------------------------------------------------------------------*/
#ifndef _SISPPVC_C
#define _SISPPVC_C

#include "vcminc.h"

/*****************************************************************************/
/* Function Name      : VcmSispPortVlanMapEntryGetNext                       */
/*                                                                           */
/* Description        : This function used to get the next Port Vlan Mapping */
/*                      entry of the given Physical IfIndex and VlanId.      */
/*                                                                           */
/* Input(s)           : u4PhyIfIndex  - Physical/PortChannel IfIndex         */
/*                      VlanId        - Vlan Identifier                      */
/*                                                                           */
/* Output(s)          : pNextVcmSispIfCtxInfo - Pointer to the next port     */
/*                                                 vlan mapping entry        */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispPortVlanMapEntryGetNext (UINT4 u4PhyIfIndex, tVlanId VlanId,
                                tVlanId * pNextVlanId,
                                tVcmIfMapEntry * pNextIfMapEntry)
{
    tVlanId             NextVlan = VCM_INIT_VAL;
    UINT4               u4NextIfIndex = VCM_INIT_VAL;

    u4PhyIfIndex = (u4PhyIfIndex == VCM_INIT_VAL) ? 1 : u4PhyIfIndex;

    VCM_LOCK ();

    for (u4NextIfIndex = u4PhyIfIndex;
         u4NextIfIndex <= BRG_MAX_PHY_PLUS_LOG_PORTS; u4NextIfIndex++)
    {
        if (SISP_PVC_TABLE_ENTRY (u4NextIfIndex) == NULL)
        {
            continue;
        }

        for (NextVlan = ((tVlanId) (VlanId + 1)); NextVlan <= VLAN_MAX_VLAN_ID;
             NextVlan++)
        {
            if (SISP_PVC_TABLE_BLOCK (u4NextIfIndex, NextVlan) == NULL)
            {
                continue;
            }

            if (SISP_PVC_TABLE_PORT_ENTRY (u4NextIfIndex, NextVlan) != NULL)
            {
                *pNextVlanId = NextVlan;
                VCM_MEMCPY (pNextIfMapEntry,
                            (SISP_PVC_TABLE_PORT_ENTRY
                             (u4NextIfIndex, NextVlan)),
                            sizeof (tVcmIfMapEntry));
                VCM_UNLOCK ();

                VCM_TRC (VCM_CONTROL_PATH_TRC, "VcmSispPortVlanMapEntryGetNext provided the next Port Vlan Mapping"
                             "entry of the given Physical IfIndex and VlanId and returns success \n");

                return VCM_SUCCESS;
            }
        }

        VlanId = VCM_INIT_VAL;
    }
    VCM_UNLOCK ();
    VCM_TRC (VCM_ALL_FAILURE_TRC, "VcmSispPortVlanMapEntryGetNext returns failure \n");
    return VCM_FAILURE;
}

/*****************************************************************************/
/* Function Name      : VcmSispPortVlanMapEntryGet                           */
/*                                                                           */
/* Description        : This function used to get the Port Vlan Mapping      */
/*                      entry of the given Physical IfIndex and VlanId.      */
/*                                                                           */
/* Input(s)           : u4PhyIfIndex  - Physical/PortChannel IfIndex         */
/*                      VlanId        - Vlan Identifier                      */
/*                                                                           */
/* Output(s)          : pRetVcmSispIfCtxInfo - Pointer to the port           */
/*                                                 vlan mapping entry        */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispPortVlanMapEntryGet (UINT4 u4PhyIfIndex, tVlanId VlanId,
                            tVcmIfMapEntry * pRetVcmIfMapEntry)
{
    VCM_LOCK ();

    if (SISP_PVC_TABLE_ENTRY (u4PhyIfIndex) == NULL)
    {
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }

    if (VcmSispGetPvcTableEntry (u4PhyIfIndex, VlanId, pRetVcmIfMapEntry)
        != VCM_FAILURE)
    {
        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/* Function Name      : VcmSispAddPortVlanEntry                              */
/*                                                                           */
/* Description        : This function used to add the Port Vlan Mapping      */
/*                      entry of the given local port and VlanId in the      */
/*                      particular context                                   */
/*                                                                           */
/* Input(s)           : pIfMapEntry   - Pointer to the Interface map entry   */
/*                      VlanId        - Vlan Identifier                      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispAddPortVlanEntry (tVcmIfMapEntry * pIfMapEntry, tVlanId VlanId)
{
    /* ASSUMPTIONS:
     * -----------
     * Following validations are done before invoking this function
     * (1) IfMapEntry for this u4IfIndex
     * (2) And SISP_PVC_TABLE_ENTRY for this u4PhyIfIndex of u4IfIndex
     * (3) VCM_LOCK is taken already
     * */

    /* PVC Entry has to be added for either SISP enabled physical port
     * or for the sisp port */

    if (VcmSispSetPvcTableEntry (pIfMapEntry->u4PhyIfIndex, VlanId, pIfMapEntry)
        == VCM_FAILURE)
    {
        VCM_TRC_ARG2 (VCM_CONTROL_PATH_TRC | VCM_MGMT_TRC | VCM_ALL_FAILURE_TRC,
                      "VCM-SISP: VcmSispAddPortVlanEntry fail "
                      "for VLAN %d and IfIndex %d\n",
                      pIfMapEntry->u4IfIndex, VlanId);

        return VCM_FAILURE;
    }

#ifdef NPAPI_WANTED
    if (VCM_IS_NP_PROGRAMMING_ALLOWED () == VCM_TRUE)
    {
        if (VcmFsVcmSispHwSetPortVlanMapping (pIfMapEntry->u4PhyIfIndex, VlanId,
                                              pIfMapEntry->u4VcNum,
                                              SISP_ADD) == FNP_FAILURE)
        {
            /* Delete from Software also */
            VcmSispReSetPvcTableEntry (pIfMapEntry->u4PhyIfIndex, VlanId);

            VCM_TRC_ARG2 (VCM_CONTROL_PATH_TRC | VCM_MGMT_TRC |
                          VCM_ALL_FAILURE_TRC,
                          "VCM-SISP: Port Vlan Entry HW addition fail "
                          "for VLAN %d and IfIndex %d\n",
                          pIfMapEntry->u4IfIndex, VlanId);
            return VCM_FAILURE;
        }
    }
#endif

    VCM_TRC_ARG2 (VCM_CONTROL_PATH_TRC | VCM_MGMT_TRC,
                  "VCM-SISP: Port Vlan Entry addition successful "
                  "for VLAN %d and IfIndex %u\n", VlanId,
                  pIfMapEntry->u4IfIndex);

    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispDelPortVlanEntry                              */
/*                                                                           */
/* Description        : This function used to delete the Port Vlan Mapping   */
/*                      entry of the given local port and VlanId in the      */
/*                      particular context                                   */
/*                                                                           */
/* Input(s)           : u4ContextId   - Context identifier                   */
/*                      VlanId        - Vlan Identifier                      */
/*                      u4IfIndex     - Interface Identifier.                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispDelPortVlanEntry (UINT4 u4ContextId, UINT4 u4IfIndex, tVlanId VlanId)
{
    tVcmIfMapEntry      Dummy;
    tVcmIfMapEntry     *pIfMapEntry = NULL;

    VCM_MEMSET (&Dummy, VCM_INIT_VAL, sizeof (tVcmIfMapEntry));
#ifndef NPAPI_WANTED
    UNUSED_PARAM (u4ContextId);
#endif

    VCM_LOCK ();

    /* PVC Entry has to be deleted for either SISP enabled physical port
     * or for the logical port */

    Dummy.u4IfIndex = u4IfIndex;

    pIfMapEntry = VcmFindIfMapEntry (&Dummy);

    if (pIfMapEntry == NULL)
    {
        /* Unknown port */
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }

    if (SISP_PVC_TABLE_ENTRY (pIfMapEntry->u4PhyIfIndex) == NULL)
    {
        /* Unknown logical port */
        VCM_UNLOCK ();
        VCM_TRC_ARG1 (VCM_CONTROL_PATH_TRC | VCM_MGMT_TRC,
                      "VCM-SISP: PVC Entry del not done - SISP not enabled"
                      "on IfIndex %d\n", pIfMapEntry->u4PhyIfIndex);
        return VCM_SUCCESS;
    }

    if (VcmSispReSetPvcTableEntry (pIfMapEntry->u4PhyIfIndex, VlanId)
        == VCM_SUCCESS)
    {
#ifdef NPAPI_WANTED
        if (VCM_IS_NP_PROGRAMMING_ALLOWED () == VCM_TRUE)
        {
            if (VcmFsVcmSispHwSetPortVlanMapping (pIfMapEntry->u4PhyIfIndex,
                                                  VlanId,
                                                  u4ContextId, SISP_DELETE)
                == FNP_FAILURE)
            {
                /* Adding to the software for the failed entry */
                VcmSispSetPvcTableEntry (pIfMapEntry->u4PhyIfIndex, VlanId,
                                         pIfMapEntry);
                VCM_UNLOCK ();
                VCM_TRC_ARG2 (VCM_CONTROL_PATH_TRC | VCM_MGMT_TRC,
                              "VCM-SISP: Port Vlan Entry HW deletion failed "
                              "for VLAN %d and IfIndex %d\n", u4IfIndex,
                              VlanId);
                return VCM_FAILURE;
            }
        }
#endif
        VCM_TRC_ARG2 (VCM_CONTROL_PATH_TRC | VCM_MGMT_TRC,
                      "VCM-SISP: Port Vlan Entry deletion successful "
                      "for VLAN %d and IfIndex %d\n", u4IfIndex, VlanId);
    }
    VCM_UNLOCK ();
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispDeleteAllPortVlanMapEntriesForPort            */
/*                                                                           */
/* Description        : This API will delete all the Port-VLAN-Context Map   */
/*                      table for the particular Physical Port or logical    */
/*                      port.                                                */
/*                                                                           */
/* Input(s)           :  u4IfIndex      - Physical Port Identifier           */
/*                       u1SyncFlag     - Flag to indicate that dynamic SYNC */
/*                                        up of PVC deletion status is needed*/
/*                                        or not. Last deleted VLAN will be  */
/*                                        sent as the dynamic sync up.       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispDeleteAllPortVlanMapEntriesForPort (UINT4 u4IfIndex, UINT1 u1SyncFlag)
{
    tVcmIfMapEntry      Dummy;
    tVcmIfMapEntry     *pIfMapEntry = NULL;
    tVlanId             Vlan = VCM_INIT_VAL;

    VCM_MEMSET (&Dummy, VCM_INIT_VAL, sizeof (tVcmIfMapEntry));

    VCM_LOCK ();

    if (VCM_GET_SISP_STATUS == SISP_SHUTDOWN)
    {
        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    Dummy.u4IfIndex = u4IfIndex;

    pIfMapEntry = VcmFindIfMapEntry (&Dummy);

    if (pIfMapEntry == NULL)
    {
        /* Unknown port */
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }
    if (SISP_IS_VALID_PHY_PORT (pIfMapEntry->u4PhyIfIndex) == VCM_FALSE)
    {
        /* Its not a valid physical port */
        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }
    if (SISP_PVC_TABLE_ENTRY (pIfMapEntry->u4PhyIfIndex) == NULL)
    {
        /* Its not a SISP enabled port */
        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    for (Vlan = 1; Vlan <= VLAN_MAX_VLAN_ID; Vlan++)
    {
        if (u1SyncFlag == VCM_TRUE)
        {
            VcmSispRedSendPvcStatus (pIfMapEntry->u4PhyIfIndex, Vlan);
        }

        if (SISP_PVC_TABLE_BLOCK (pIfMapEntry->u4PhyIfIndex, Vlan) == NULL)
        {
            continue;
        }

        if (SISP_PVC_TABLE_PORT_ENTRY (pIfMapEntry->u4PhyIfIndex, Vlan) == NULL)
        {
            continue;
        }

        if ((SISP_PVC_TABLE_PORT_ENTRY (pIfMapEntry->u4PhyIfIndex, Vlan))->
            u4IfIndex != u4IfIndex)
        {
            continue;
        }

        VcmSispReSetPvcTableEntry (pIfMapEntry->u4PhyIfIndex, Vlan);

#ifdef NPAPI_WANTED
        if (VCM_IS_NP_PROGRAMMING_ALLOWED () == VCM_TRUE)
        {
            if (VcmFsVcmSispHwSetPortVlanMapping
                (pIfMapEntry->u4PhyIfIndex, Vlan, pIfMapEntry->u4VcNum,
                 SISP_DELETE) == FNP_FAILURE)
            {
                VcmSispSetPvcTableEntry (pIfMapEntry->u4PhyIfIndex, Vlan,
                                         pIfMapEntry);
                VCM_TRC_ARG2 (VCM_CONTROL_PATH_TRC | VCM_MGMT_TRC,
                              "VCM-SISP: Port Vlan Entry HW deletion "
                              "failed for VLAN %d and IfIndex %d\n",
                              pIfMapEntry->u4PhyIfIndex, Vlan);
                VCM_UNLOCK ();
                return VCM_FAILURE;
            }
        }
#endif
    }
    VCM_UNLOCK ();
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispValidatePortVlanEntry                         */
/*                                                                           */
/* Description        : This fn will used to verify whether the Port-VLAN-   */
/*                      Context Mapping Table configuration is allowable.    */
/*                      The Local ports will be converted to the underlying  */
/*                      physical or port channel interface before verifying  */
/*                      the entry in the table.                              */
/*                                                                           */
/* Input(s)           : u4ContextId   - Context identifier                   */
/*                      VlanId        - Vlan Identifier                      */
/*                      u2LocalPort   - Local port identifier                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_TRUE / VCM_FALSE                                 */
/*****************************************************************************/
INT4
VcmSispValidatePortVlanEntry (UINT4 u4ContextId, UINT2 u2LocalPort,
                              tVlanId VlanId)
{
    tVcmIfMapEntry      Dummy;
    tVcmIfMapEntry     *pIfMapEntry = NULL;
    UINT4               u4IfIndex = VCM_INIT_VAL;

    VCM_MEMSET (&Dummy, VCM_INIT_VAL, sizeof (tVcmIfMapEntry));

    if (VcmUtilGetIfIndexFromLocalPort (u4ContextId, u2LocalPort, &u4IfIndex)
        == VCM_FAILURE)
    {
        return VCM_FALSE;
    }

    if (SISP_IS_VALID_PHY_PORT (u4IfIndex) == VCM_FALSE)
    {
        return VCM_TRUE;
    }
    Dummy.u4IfIndex = u4IfIndex;

    VCM_LOCK ();

    pIfMapEntry = VcmFindIfMapEntry (&Dummy);

    if (pIfMapEntry == NULL)
    {
        /* Unknown logical port */
        VCM_UNLOCK ();
        return VCM_FALSE;
    }

    if (SISP_PVC_TABLE_ENTRY (pIfMapEntry->u4PhyIfIndex) == NULL)
    {
        VCM_UNLOCK ();
        return VCM_TRUE;
    }

    if (VcmSispGetPvcTableEntry (pIfMapEntry->u4PhyIfIndex, VlanId,
                                 &Dummy) == VCM_FAILURE)
    {
        /* VLAN is not configured already */
        VCM_UNLOCK ();
        return VCM_TRUE;
    }
    else
    {
        /* = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
           =                                                             =
           =  -> If the IfIndex are same; then it means, the same entry  =
           =     is confiugred again.                                    =
           =  -> If the Context IDs are same, then it implies that,      =
           =     * Either the same entry is configured again.            =
           =     * Or VLAN translation entry with the particular VLAN    =
           =       as Local VLAN is configured for the particular port.  =
           =       In this case, the Configuration should be allowed.    =
           = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = */

        if ((u4IfIndex == Dummy.u4IfIndex) || (u4ContextId == Dummy.u4VcNum))
        {
            VCM_UNLOCK ();
            return VCM_TRUE;
        }
    }

    VCM_UNLOCK ();
    return VCM_FALSE;
}

/*****************************************************************************/
/* Function Name      : VcmSispSetPvcTableEntry                              */
/*                                                                           */
/* Description        : This function used to configure the PVC Table entry. */
/*                      Particular Physical or port channel's pointer        */
/*                      (pVcmIfMapEntry) will be stored in the corresponding */
/*                      location of Physical Port & VLAN.                    */
/*                                                                           */
/* Input(s)           : u4PhyIfIndex  - Physical or LAG IfIndex              */
/*                      VlanId        - Vlan Identifier                      */
/*                      pIfMapEntry   - Pointer to the IfMapEntry Node.      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispSetPvcTableEntry (UINT4 u4PhyIfIndex, tVlanId VlanId,
                         tVcmIfMapEntry * pIfMapEntry)
{
    tVcmIfMapEntry    **pVcmIfMapBlock = NULL;

    /* If the particular VLAN's block is not yet created,
     * it has to be created */
    if (SISP_PVC_TABLE_BLOCK (u4PhyIfIndex, VlanId) == NULL)
    {
        SISP_VLAN_IF_MAP_ALLOCATE_MEMBLK (pVcmIfMapBlock);

        if (pVcmIfMapBlock == NULL)
        {
            VCM_TRC_ARG2 (VCM_CONTROL_PATH_TRC | VCM_ALL_FAILURE_TRC,
                          "VCM-SISP: If Map allocation failed, while "
                          "adding entry for VLAN %d and IfIndex %d\n",
                          u4PhyIfIndex, VlanId);
            return VCM_FAILURE;
        }

        VCM_MEMSET (pVcmIfMapBlock, VCM_INIT_VAL,
                    ((sizeof (tVcmIfMapEntry *)) * SISP_MAX_VLAN_PER_BLOCK));

        SISP_PVC_TABLE_BLOCK (u4PhyIfIndex, VlanId) = pVcmIfMapBlock;
    }

    SISP_PVC_TABLE_PORT_ENTRY (u4PhyIfIndex, VlanId) = pIfMapEntry;

    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispReSetPvcTableEntry                            */
/*                                                                           */
/* Description        : This function used to reset the PVC Table entry.     */
/*                      Particular Physical or port channel's pointer        */
/*                      (pVcmIfMapEntry)will be restored in the corresponding*/
/*                      location of Physical Port & VLAN to NULL.            */
/*                                                                           */
/* Input(s)           : u4PhyIfIndex  - Physical or LAG IfIndex              */
/*                      VlanId        - Vlan Identifier                      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispReSetPvcTableEntry (UINT4 u4PhyIfIndex, tVlanId VlanId)
{
    UINT2               u2Pos = VCM_INIT_VAL;

    if (SISP_PVC_TABLE_BLOCK (u4PhyIfIndex, VlanId) == NULL)
    {
        /* Block for the particular VLAN is not created */
        return VCM_FAILURE;
    }
    else if (SISP_PVC_TABLE_PORT_ENTRY (u4PhyIfIndex, VlanId) == NULL)
    {
        /* PVC entry is not created already  - Nothing to reset */
        return VCM_FAILURE;
    }

    SISP_PVC_TABLE_PORT_ENTRY (u4PhyIfIndex, VlanId) = NULL;

    /* Search in the Block, if still any other port entry is existing,
     * If no other port entry is existing for the Block, then the block
     * will be deleted */
    for (u2Pos = VCM_INIT_VAL; u2Pos < SISP_MAX_VLAN_PER_BLOCK; u2Pos++)
    {
        if ((SISP_PVC_TABLE_BLOCK (u4PhyIfIndex, VlanId)[u2Pos]) != NULL)
        {
            /* Some other Physical port is configured in this VLAN,
             * so block should not be deleted - Return */
            return VCM_SUCCESS;
        }
    }

    /* Deleting the block */
    SISP_VLAN_IF_MAP_FREE_MEMBLK (SISP_PVC_TABLE_BLOCK (u4PhyIfIndex, VlanId));
    SISP_PVC_TABLE_BLOCK (u4PhyIfIndex, VlanId) = NULL;

    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispGetPvcTableEntry                              */
/*                                                                           */
/* Description        : This function used to get the PVC Table entry.       */
/*                      Particular Physical or port channel's pointer        */
/*                      (pVcmIfMapEntry) will be stored in the corresponding */
/*                      location of Physical Port & VLAN.                    */
/*                                                                           */
/* Input(s)           : u4PhyIfIndex  - Physical or LAG IfIndex              */
/*                      VlanId        - Vlan Identifier                      */
/*                      pIfMapEntry   - Pointer to the IfMapEntry Node.      */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispGetPvcTableEntry (UINT4 u4PhyIfIndex, tVlanId VlanId,
                         tVcmIfMapEntry * pRetVcmIfMapEntry)
{
    /* Verify whether the block for the VLAN is already created */
    if (SISP_PVC_TABLE_BLOCK (u4PhyIfIndex, VlanId) != NULL)
    {
        /* Verify whether the entry is existing */
        if (SISP_PVC_TABLE_PORT_ENTRY (u4PhyIfIndex, VlanId) != NULL)
        {
            VCM_MEMCPY (pRetVcmIfMapEntry,
                        (SISP_PVC_TABLE_PORT_ENTRY (u4PhyIfIndex, VlanId)),
                        sizeof (tVcmIfMapEntry));
            return VCM_SUCCESS;
        }
    }
    pRetVcmIfMapEntry = NULL;
    return VCM_FAILURE;
}
#endif
