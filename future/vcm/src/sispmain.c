/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2009-2010                                          */
/*****************************************************************************/
/* $Id: sispmain.c,v 1.10 2013/03/08 13:34:21 siva Exp $                */
/*                                                                           */
/*****************************************************************************/
/*    FILE  NAME            : sispmain.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc   .                                */
/*    SUBSYSTEM NAME        : VCM module                                     */
/*    MODULE NAME           : VCM module                                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 01 Mar 2009                                    */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains the functions for SISP      */
/*                            feature.                                       */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    31 APR 2009 / SISPTeam   Initial Create.                       */
/*---------------------------------------------------------------------------*/
#ifndef _SISPMAIN_C
#define _SISPMAIN_C

#include "vcminc.h"

/*****************************************************************************/
/* Function Name      : VcmSispModuleStart                                   */
/*                                                                           */
/* Description        : This function is used to start SISP feature in the   */
/*                      system.                                              */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gVcmSispGlobalInfo                                   */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gVcmSispGlobalInfo                                   */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispModuleStart (VOID)
{

    /* Create the RBTree for Logical Port entries */
    SISP_LOGICAL_PORT_TABLE = VCM_RB_TREE_CREATE_EMBED
        (FSAP_OFFSETOF (tVcmIfMapEntry, IfPhyRbNode), VcmSispPortMapTableCmp);

    if (SISP_LOGICAL_PORT_TABLE == NULL)
    {
        return VCM_FAILURE;
    }

    VCM_GET_SISP_STATUS = SISP_START;

    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispModuleShutdown                                */
/*                                                                           */
/* Description        : This function is used to shut  SISP feature in the   */
/*                      system. When SISP feature is shutdown, all the       */
/*                      logical ports and Port Vlan Mapping entries will be  */
/*                      deleted.                                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gVcmSispGlobalInfo                                   */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gVcmSispGlobalInfo                                   */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispModuleShutdown (VOID)
{
    /* Delete all the Sisp ports configured in the system */
    VcmSispDeleteAllSispPorts ();

    /* Disable all the SISP enabled ports */
    VcmSispDisableOnAllPorts ();

    /* Delete the RBTree for Logical Port entries */
    VCM_RB_TREE_DELETE (SISP_LOGICAL_PORT_TABLE);

    VCM_GET_SISP_STATUS = SISP_SHUTDOWN;

    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SispInit                                             */
/*                                                                           */
/* Description        : Sisp Global Initialization function.                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gVcmSispGlobalInfo                                   */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gVcmSispGlobalInfo                                   */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SispInit (VOID)
{
    VCM_MEMSET (&gVcmSispGlobalInfo, VCM_INIT_VAL, sizeof (tVcmSispGlobals));

    /*tVcmSispIfCtxInfo */
    SISP_IF_CTX_INFO_MEMPOOL_ID =
        VCMMemPoolIds[MAX_SISP_IF_CXT_INFO_SIZE_SIZING_ID];

    /*SISP_VLAN_IF_MAP */
    SISP_VLAN_IF_MAP_MEMPOOL_ID =
        VCMMemPoolIds[MAX_SISP_VLAN_IF_MAP_SIZE_SIZING_ID];

    /*VLAN_LIST_SIZE */
    SISP_VLAN_LIST_MEMPOOL_ID = VCMMemPoolIds[MAX_SISP_BLOCKS_SIZING_ID];

    VCM_GET_SISP_STATUS = SISP_SHUTDOWN;

#ifdef SNMP_2_WANTED
    /* Register the SISP MIBs with SNMP */
    RegisterFSSISP ();
#endif
}

/*****************************************************************************/
/* Function Name      : VcmSispEnableOnPort                                  */
/*                                                                           */
/* Description        : This function is used to enable SISP feature on the  */
/*                      particular physical or port channel port.            */
/*                                                                           */
/*                        * Higher layers will be given indication about     */
/*                          Sisp enabled status.                             */
/*                        * Vlans configured for this port will be populated */
/*                          in the PVC table                                 */
/*                        * PVC table entry will be added for Local & Relay  */
/*                          VLAN when any VLAN translation entry is          */
/*                          configured for the port                          */
/*                                                                           */
/* Input(s)           : u4PhyIfIndex - Physical or port channel IfIndex      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gVcmSispGlobalInfo                                   */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gVcmSispGlobalInfo                                   */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispEnableOnPort (UINT4 u4PhyIfIndex)
{
    tVcmSispIfCtxInfo  *pVcmSispIfCtxInfo = NULL;
    tVcmIfMapEntry      Dummy;    /* No need to memset; will be filled */
    tVcmIfMapEntry     *pIfMapEntry = NULL;
    UINT1              *pu1VlanList = NULL;
    UINT1              *pu1LocalVlanList = NULL;
    tVlanId             VlanId;
    UINT4               u4ContextId = VCM_INVALID_CONTEXT_ID;
    UINT2               u2LocalPort = VCM_INVALID_HLPORT;
    UINT1               u1Result = VCM_FALSE;
    INT4                i4VlanRetVal = L2IWF_FAILURE;
    INT4                i4RlyVlanRetVal = L2IWF_FAILURE;

    VcmGetContextInfoFromIfIndex (u4PhyIfIndex, &u4ContextId, &u2LocalPort);

#ifdef NPAPI_WANTED
    if (VCM_IS_NP_PROGRAMMING_ALLOWED () == VCM_TRUE)
    {
        if (VcmFsVcmSispHwSetPortCtrlStatus (u4PhyIfIndex, SISP_ENABLE)
            != FNP_SUCCESS)
        {
            VCM_TRC (VCM_ALL_FAILURE_TRC, "VcmSispEnableOnPort function failed"
                     "in setting SISP Port status to HW\n");
            return VCM_FAILURE;
        }
    }
#endif
    SISP_IF_CTX_INFO_ALLOCATE_MEMBLK (pVcmSispIfCtxInfo);

    if (pVcmSispIfCtxInfo == NULL)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC, "VcmSispEnableOnPort function failed"
                 "- Memory allocation failed\n");
        return VCM_FAILURE;
    }

    VCM_MEMSET (pVcmSispIfCtxInfo, VCM_INIT_VAL, sizeof (tVcmSispIfCtxInfo));

    gVcmSispGlobalInfo.apPVCTable[u4PhyIfIndex] = pVcmSispIfCtxInfo;

    /* Indicate higher layers about SISP feature enable on this port */
    VcmSispL2IwfUpdateSispPortCtrlStatus (u4PhyIfIndex, SISP_ENABLE);

    /* Increment the SISP Port Count for the Primary Context */
    VcmIncrementSispPortCount (u4ContextId);

    /* Populate the Port Vlan Context Map table for the SISP enabled ports,
     * by configuring VLANs on which the particular phy port is member of */

    SISP_VLAN_LIST_ALLOCATE_MEMBLK (pu1VlanList);

    if (pu1VlanList == NULL)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC,
                 "pu1VlanList:VcmSispEnableOnPort function failed"
                 "- Memory allocation failed\n");

        return VCM_FAILURE;
    }

    pu1LocalVlanList = UtlShMemAllocVlanList ();

    if (pu1LocalVlanList == NULL)
    {
        SISP_VLAN_LIST_FREE_MEMBLK (pu1VlanList);
        return VCM_FAILURE;
    }

    MEMSET (pu1VlanList, 0, VLAN_LIST_SIZE);
    MEMSET (pu1LocalVlanList, 0, VLAN_LIST_SIZE);

    i4VlanRetVal = VcmSispL2IwfGetPortVlanMemberList (u4PhyIfIndex,
                                                      pu1VlanList);

    /* PVC table should be populated for the Local VLAN's configured in
     * the VID translation entry for the particular physical port 
     * -> Entry for RelayVlans need not be configured, that will be
     *    created on the creation of Relay VLAN                     */

    i4RlyVlanRetVal =
        VcmSispL2IwfPbGetLocalVidListFromVIDTransTable (u4PhyIfIndex,
                                                        pu1LocalVlanList);

    /* If any of the MemberVlan List or RelayVlanList get is successful,
     * then scan for the returned VLAN List and Populated the PVC tables
     * If both the get are FAILURE, then it means that, particular port
     * is not member of any VLAN nor configured in VID translation table */

    if ((i4VlanRetVal == L2IWF_SUCCESS) || (i4RlyVlanRetVal == L2IWF_SUCCESS))
    {
        /* Both the VlanMemberList & LocalVlanList has to be added together,
         * to avoid multiple addition calls to PVC table. For example, the
         * port can be member of Vlan 20, and also Vlan 20 is configured
         * as LocalVID; then the PVC table addition has to be called for
         * only one time for Vlan 20 */
        OSIX_ADD_PORT_LIST (pu1VlanList, pu1LocalVlanList, VLAN_LIST_SIZE,
                            VLAN_LIST_SIZE);

        VCM_LOCK ();

        Dummy.u4IfIndex = u4PhyIfIndex;

        pIfMapEntry = VcmFindIfMapEntry (&Dummy);

        if (pIfMapEntry == NULL)
        {
            /* Unknown logical port */
            VCM_TRC_ARG1 (VCM_CONTROL_PATH_TRC | VCM_MGMT_TRC,
                          "VCM-SISP: VcmSispEnableOnPort Invalid IfIndex %d\n",
                          u4PhyIfIndex);
            VCM_UNLOCK ();
            SISP_VLAN_LIST_FREE_MEMBLK (pu1VlanList);
            UtlShMemFreeVlanList (pu1LocalVlanList);
            return VCM_FAILURE;
        }

        /* SISP_PVC_TABLE_ENTRY is initialised in this function only.
         * No need to do null check 
         * */

        for (VlanId = 1; VlanId <= VLAN_MAX_VLAN_ID; VlanId++)
        {
            OSIX_BITLIST_IS_BIT_SET (pu1VlanList, VlanId, VLAN_LIST_SIZE,
                                     u1Result);
            if (u1Result == OSIX_TRUE)
            {
                VcmSispAddPortVlanEntry (pIfMapEntry, VlanId);
            }

            VcmSispRedSendPvcStatus (u4PhyIfIndex, VlanId);
        }
        VCM_UNLOCK ();
    }
    SISP_VLAN_LIST_FREE_MEMBLK (pu1VlanList);
    UtlShMemFreeVlanList (pu1LocalVlanList);
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispDisableOnPort                                 */
/*                                                                           */
/* Description        : This function is used to disable SISP feature on the */
/*                      particular physical or port channel port.            */
/*                                                                           */
/* Input(s)           : u4PhyIfIndex - Physical or port channel IfIndex      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gVcmSispGlobalInfo                                   */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gVcmSispGlobalInfo                                   */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispDisableOnPort (UINT4 u4PhyIfIndex)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPort;

    VcmGetContextInfoFromIfIndex (u4PhyIfIndex, &u4ContextId, &u2LocalPort);

    VcmSispDeleteAllPortVlanMapEntriesForPort (u4PhyIfIndex, VCM_TRUE);

    /* Indicate higher layers about SISP feature disable on this port */
    VcmSispL2IwfUpdateSispPortCtrlStatus (u4PhyIfIndex, SISP_DISABLE);

#ifdef NPAPI_WANTED
    if (VCM_IS_NP_PROGRAMMING_ALLOWED () == VCM_TRUE)
    {
        if (VcmFsVcmSispHwSetPortCtrlStatus (u4PhyIfIndex, SISP_DISABLE)
            != FNP_SUCCESS)
        {
            VCM_TRC (VCM_ALL_FAILURE_TRC, "VcmSispDisableOnPort function failed"
                     "in setting SISP Port status to HW\n");
            return VCM_FAILURE;
        }
    }
#endif
    /* Decrement the SISP Port Count for the Primary Context */
    VcmDecrementSispPortCount (u4ContextId);

    VCM_LOCK ();

    SISP_IF_CTX_INFO_FREE_MEMBLK (gVcmSispGlobalInfo.apPVCTable[u4PhyIfIndex]);

    SISP_PVC_TABLE_ENTRY (u4PhyIfIndex) = NULL;

    VCM_UNLOCK ();

    VCM_TRC_ARG1 (VCM_CONTROL_PATH_TRC, "SISP successfully disabled on"
                  " port %u \r\n", u4PhyIfIndex);
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispDisableOnAllPorts                             */
/*                                                                           */
/* Description        : This function is used to disable SISP feature on all */
/*                      the SISP enabled physical or port channel port.      */
/*                                                                           */
/* Input(s)           : u4PhyIfIndex - Physical or port channel IfIndex      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
VcmSispDisableOnAllPorts (VOID)
{
    UINT4               u4PhyIfIndex = 1;

    for (; u4PhyIfIndex <= BRG_MAX_PHY_PLUS_LOG_PORTS; u4PhyIfIndex++)
    {
        if (SISP_PVC_TABLE_ENTRY (u4PhyIfIndex) != NULL)
        {
            VcmSispDisableOnPort (u4PhyIfIndex);
        }
    }
}
#endif
