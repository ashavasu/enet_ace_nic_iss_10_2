/*****************************************************************************/
/* Copyright (C) Future Software,                                            */
/* Licensee Future Communication Software,                                   */
/*****************************************************************************/
/*    FILE  NAME            : vcmipifmap.c                                   */
/*    DESCRIPTION           : This file contains functions related to        */
/*                            ip interface mapping by VCM module             */
/*---------------------------------------------------------------------------*/

#ifndef _VCMIPIFMAP_C
#define _VCMIPIFMAP_C

#include "vcminc.h"

/****************************************************************************
 Function    :  VcmIpAddIfMap          
 Description :  adds the interface mapping entry to the DLL       
                corresponding to the given context pointer
 Input       :  pVirtualContextInfo - pointer to context structure
                pVcmIpIfMapEntry    - pointer to tVcmIpIfMapEntry structure
 Output      :  none                  
 Returns     :  none
****************************************************************************/

PUBLIC VOID
VcmIpAddIfMap (tVirtualContextInfo * pVirtualContextInfo,
               tVcmIpIfMapEntry * pVcmIpIfMapEntry)
{
    TMO_DLL_Init_Node (&(pVcmIpIfMapEntry->NextIfEntry));

    TMO_DLL_Add (&(pVirtualContextInfo->VcIPIntfList),
                 &(pVcmIpIfMapEntry->NextIfEntry));
}

/****************************************************************************
 Function    :  VcmIpDeleteIfMap  
 Description :  deletes the interface details from the list        
 Input       :  u4IpIfIndex - Interface index of the interface to be
                              unmapped
 Output      :  none
 Returns     :  none
****************************************************************************/

PUBLIC VOID
VcmIpDeleteIfMap (UINT4 u4IpIfIndex)
{
    tVirtualContextInfo Dummy;
    tVirtualContextInfo *pVc = NULL;

    Dummy.u4VcNum = aVcmIpIfMapEntry[u4IpIfIndex].u4FsVcIpId;
    pVc = VcmFindVcEntry (&Dummy);

    if (pVc != NULL)
    {
        TMO_DLL_Delete (&(pVc->VcIPIntfList),
                        &(aVcmIpIfMapEntry[u4IpIfIndex].NextIfEntry));
    }
}

/****************************************************************************
 Function    :  VcmIpDeleteIfMapEntries
 Description :  Deletes all the interface entries belonging to the
                given context id
 Input       :  u4ContextId - context id
 Output      :  none                  
 Returns     :  none
****************************************************************************/

PUBLIC VOID
VcmIpDeleteIfMapEntries (UINT4 u4ContextId)
{
    tVcmIpIfMapEntry   *pVcmIpIfMapEntry = NULL;
    tVirtualContextInfo *pVc = NULL;
    tVirtualContextInfo Dummy;

    Dummy.u4VcNum = u4ContextId;
    pVc = VcmFindVcEntry (&Dummy);

    while ((pVcmIpIfMapEntry =
            (tVcmIpIfMapEntry *) TMO_DLL_First (&(pVc->VcIPIntfList))) != NULL)
    {
        TMO_DLL_Delete (&(pVc->VcIPIntfList), &(pVcmIpIfMapEntry->NextIfEntry));
        VcmIpIfMapInit (pVcmIpIfMapEntry->u4FsVcIpIfIndex);
    }
}

#endif
