/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* $Id: vcmcli.c,v 1.68 2018/01/17 10:30:00 siva Exp $                    */
/*****************************************************************************/
/*    FILE  NAME            : vcmcli.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Virtual Context Manager Module                 */
/*    MODULE NAME           : Virtual Context Manager Module                 */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 16 June 2006                                   */
/*    AUTHOR                : L2MI Team                                      */
/*    DESCRIPTION           : This file contains CLI related functions.      */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    16 June 2006  /             Initial Create.                    */
/*            MI Team                                                        */
/*****************************************************************************/

/*****************************************************************************/
/*                           INCLUDE FILES                                   */
/*****************************************************************************/

#ifndef __VCMCLI_C__
#define __VCMCLI_C__

#include "vcminc.h"
#include "iss.h"
#include "vcmcli.h"
#include "snmputil.h"
#include "stpcli.h"
#include "vlancli.h"
#include "fsvcmcli.h"

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : cli_process_vcm_cmd                                */
/*                                                                           */
/*     DESCRIPTION      : This function takes in variable no. of arguments   */
/*                        and process the commands for the CFA module as     */
/*                        defined in vcmcmd.def                              */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                        u4Command -  Command Identifier                    */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4
cli_process_vcm_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[VCM_MAX_ARGS];
    INT1                i1argno = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RetStatus = 0;
    UINT4               u4PortId = 0;
    UINT4               u4Set = 0;
    UINT4               u4VcId = 0;
    va_start (ap, u4Command);

    /* third arguement is always interface name/index */
    va_arg (ap, UINT4 *);

    /* Walk through the rest of the arguements and store in args array.
     * Store 16 arguements at the max. This is because vlan commands do not
     * take more than sixteen inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give
     * second input. In that case first arg will be null and second arg only
     * has value */
    while (1)
    {
        args[i1argno++] = va_arg (ap, UINT4 *);
        if (i1argno == VCM_MAX_ARGS)
        {
            break;
        }
    }

    va_end (ap);

    CliRegisterLock (CliHandle, VcmConfLock, VcmConfUnLock);

    VCM_CONF_LOCK ();

    switch (u4Command)
    {
        case CLI_VCM_SHOW_ALL:
        case CLI_VCM_SHOW_BRIEF:
        case CLI_VCM_SHOW_DETAIL:
        case CLI_VCM_SHOW_INTERFACES:

            i4RetStatus = VcmShowCommand (CliHandle,
                                          (UINT1 *) args[0], u4Command);
            break;

        case CLI_VCM_SHOW_CTX_MAPPING:

            VcmPrintVcMapDetail (CliHandle, (CLI_PTR_TO_U4 (args[0])));
            break;

        case CLI_VCM_SHOW_OWNER:

            i4RetStatus = VcmShowOwner (CliHandle, *((UINT4 *) args[0]));
            break;

        case CLI_VCM_CREATE_OR_DELETE:

            u4Set = CLI_PTR_TO_U4 (args[1]);

            i4RetStatus =
                VcmCreateorDeleteContext (CliHandle, (UINT1 *) args[0],
                                          (UINT1) u4Set);
            break;

        case CLI_VCM_MAP_PORTS:
            u4PortId = (UINT4) CLI_GET_IFINDEX ();

            i4RetStatus = VcmMapPort (CliHandle, u4PortId, (UINT1 *) args[0]);
            break;

        case CLI_VCM_UNMAP_PORTS:

            u4PortId = (UINT4) CLI_GET_IFINDEX ();

            i4RetStatus = VcmUnMapPort (CliHandle, u4PortId, (UINT1 *) args[0]);
            break;

        case CLI_VCM_CREATE_OWNER:

            u4VcId = CLI_GET_CXT_ID ();

            i4RetStatus = VcmCreateOwner (CliHandle, u4VcId, (UINT1 *) args[0]);
            break;
#ifdef VRF_WANTED
        case CLI_VCM_VRF_CREATE:

            i4RetStatus = VcmCreateVRContext (CliHandle, (UINT1 *) args[0]);
            break;

        case CLI_VCM_VRF_DELETE:

            i4RetStatus = VcmDeleteVRContext (CliHandle, (UINT1 *) args[0]);
            break;
        case CLI_VCM_SHOW_VRF_BRIEF:
        case CLI_VCM_SHOW_VRF_DETAIL:
        case CLI_VCM_SHOW_VRF_INTERFACE:
        case CLI_VCM_SHOW_VRF_ALL:

            i4RetStatus = VcmVrfShowCommand (CliHandle, (UINT1 *) args[0],
                                             u4Command);
            break;
        case CLI_VCM_MAP_IP_INTERFACE:

            u4PortId = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = VcmAddOrDeleteIpIfMap (CliHandle,
                                                 VCM_IPIFMAP_ADD,
                                                 u4PortId, (UINT1 *) args[0]);
            break;

        case CLI_VCM_UNMAP_IP_INTERFACE:

            u4PortId = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = VcmAddOrDeleteIpIfMap (CliHandle,
                                                 VCM_IPIFMAP_DELETE,
                                                 u4PortId, (UINT1 *) args[0]);
            break;

        case CLI_VCM_VRF_STAT_STATUS:
            i4RetStatus =
                VcmCreateOrDeleteVrfCounters (CliHandle, (UINT1 *) args[0],
                                              CLI_PTR_TO_U4 (args[1]));
            break;

        case CLI_VCM_VRF_STAT_CLEAR:
            i4RetStatus = VcmClearVrfCounters (CliHandle, (UINT1 *) args[0]);
            break;

        case CLI_VCM_SHOW_VRF_COUNTERS:
            i4RetStatus = VcmVrfShowCounters (CliHandle, (UINT1 *) args[0]);
            break;
#endif

        default:

            VCM_CLI_PRINTF ((tCliHandle) CliHandle, "\r\nUnknown command\r\n");
            VCM_CONF_UNLOCK ();
            return CLI_FAILURE;

    }
    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode >= CLI_ERR_START_ID_VCM) &&
            (u4ErrCode < CLI_VCM_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%% %s",
                       VcmCliErrString[CLI_ERR_OFFSET_VCM (u4ErrCode)]);
        }

        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

    VCM_CONF_UNLOCK ();

    CliUnRegisterLock (CliHandle);

    return i4RetStatus;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmShowCommand                                     */
/*                                                                           */
/*     DESCRIPTION      : This function displays the VC information          */
/*                        depending upon the u4Command.                      */
/*                                                                           */
/*     INPUT            : CliHandle  - Context in which the CLI              */
/*                                             command is processed          */
/*                        pu1SwitchName - Switch Name                        */
/*                        u4Command     - Show Command                       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
VcmShowCommand (tCliHandle CliHandle, UINT1 *pu1SwitchName, UINT4 u4Command)
{
    UINT4               u4VcNum;

    if (pu1SwitchName == NULL)
    {
        u4VcNum = VCM_ALL_VCS;
    }
    else
    {
        if (VcmIsSwitchExist (pu1SwitchName, &u4VcNum) != VCM_TRUE)
        {
            VCM_CLI_PRINTF (CliHandle, "\r%% Switch %s Not Present.\r\n",
                            pu1SwitchName);
            return CLI_FAILURE;
        }
    }

    switch (u4Command)
    {
        case CLI_VCM_SHOW_ALL:
        case CLI_VCM_SHOW_BRIEF:
            VCM_CLI_PRINTF (CliHandle, "Virtual Context Table\r\n");
            VCM_CLI_PRINTF (CliHandle, "---------------------\r\n");
            VCM_CLI_PRINTF (CliHandle,
                            "%-5s %-32s %-16s %-10s %-25s\r\n", "VcId",
                            "Vc-Name ", "Owner", "Protocol ", "Interfaces");
            VCM_CLI_PRINTF (CliHandle,
                            "%-5s %-32s %-16s %-10s %-25s\r\n", "----",
                            "------- ", "------", "-------- ", "----------");

            VcmPrintVcBrief (CliHandle, u4VcNum);
            break;

        case CLI_VCM_SHOW_DETAIL:
            VcmPrintVcDetail (CliHandle, u4VcNum);
            break;

        case CLI_VCM_SHOW_INTERFACES:
            VCM_CLI_PRINTF (CliHandle, "Interface map table\r\n");
            VCM_CLI_PRINTF (CliHandle, "-------------------\r\n");
            VCM_CLI_PRINTF (CliHandle,
                            "%-8s %6s %-32s %11s\r\n",
                            "IfIndex", "VcNum ", "Vc-Name", "LocalPortId");
            VCM_CLI_PRINTF (CliHandle,
                            "%-8s %6s %-32s %11s\r\n",
                            "-------", "----- ", "-------", "-----------");
            VcmPrintIfMapEntries (CliHandle, u4VcNum);
            break;

        default:
            VCM_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

#ifdef VRF_WANTED
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmVrfShowCommand                                  */
/*                                                                           */
/*     DESCRIPTION      : This function displays the VRF information         */
/*                        depending upon the u4Command.                      */
/*                                                                           */
/*     INPUT            : CliHandle  - Context in which the CLI              */
/*                                             command is processed          */
/*                        pu1SwitchName - Switch Name                        */
/*                        u4Command     - Show Command                       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
VcmVrfShowCommand (tCliHandle CliHandle, UINT1 *pu1VrfName, UINT4 u4Command)
{
    UINT4               u4VcNum = 0;

    if (pu1VrfName == NULL)
    {
        u4VcNum = VCM_ALL_VCS;
    }
    else
    {
        if (VcmIsVrfExist (pu1VrfName, &u4VcNum) != VCM_TRUE)
        {
            VCM_CLI_PRINTF (CliHandle, "\r%% Vrf %s Not Present.\r\n",
                            pu1VrfName);
            return CLI_FAILURE;
        }
    }

    switch (u4Command)
    {
        case CLI_VCM_SHOW_VRF_ALL:
        case CLI_VCM_SHOW_VRF_BRIEF:
        case CLI_VCM_SHOW_VRF_DETAIL:
            VCM_CLI_PRINTF (CliHandle, "Virtual Context Table\r\n");
            VCM_CLI_PRINTF (CliHandle, "---------------------\r\n");

            VCM_CLI_PRINTF (CliHandle,
                            "%-5s %-32s %-25s %-25s\r\n", "VcId",
                            "VRF-Name ", "Switch MAC-Address", "Interfaces");
            VCM_CLI_PRINTF (CliHandle,
                            "%-5s %-32s %-25s %-25s\r\n", "----",
                            "------- ", "-----------------", "-----------");

            VcmPrintVrfBrief (CliHandle, u4VcNum);
            break;

        case CLI_VCM_SHOW_VRF_INTERFACE:
            VCM_CLI_PRINTF (CliHandle, "Interface map table\r\n");
            VCM_CLI_PRINTF (CliHandle, "-------------------\r\n");
            VCM_CLI_PRINTF (CliHandle,
                            "%-10s %-6s %-32s %-25s\r\n",
                            "IfName", "VcNum ", "Vc-Name",
                            "Switch MAC-Address");
            VCM_CLI_PRINTF (CliHandle, "%-10s %-6s %-32s %-25s\r\n", "-------",
                            "----- ", "-------", "---------------");
            VcmPrintVrfIfMapEntries (CliHandle, u4VcNum);
            break;

        default:
            VCM_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}
#endif
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmCreateorDeleteContext                           */
/*                                                                           */
/*     DESCRIPTION      : This function creates/deletes entry in virtual     */
/*                        context table.                                     */
/*                                                                           */
/*     INPUT            : CliHandle  - Context in which the CLI              */
/*                                             command is processed          */
/*                        pu1Alias   - virtual context name.                 */
/*                        u1Set       - OSIX_TRUE / OSIX_FALSE               */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS / CLI_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmCreateorDeleteContext (tCliHandle CliHandle, UINT1 *pu1Alias, UINT1 u1Set)
{
    UINT1               au1IfName[CLI_VCM_VC_MODE_LEN];
    UINT4               u4ErrorCode = 0;
    UINT4               u4VcNum = 0;
    UINT4               u4DelVcNum = 0;
    INT4                i4RetVal = 0;
    INT4                i4CxtType = 0;
    tSNMP_OCTET_STRING_TYPE *pOctetAlias = NULL;

    i4RetVal = VcmIsSwitchExist (pu1Alias, &u4DelVcNum);

    if (u1Set == OSIX_TRUE)
    {
        /* Check whether entry already exit for this VC,
         * if exist just return SUCCESS */
        if (i4RetVal != VCM_TRUE)
        {
            /* Check if any L3 context exist with the same name. If so,just change
             * the context type to both. */

            if (VcmIsVrfExist (pu1Alias, &u4VcNum) == VCM_TRUE)
            {
                if (nmhTestv2FsVCStatus (&u4ErrorCode, (INT4) u4VcNum,
                                         NOT_IN_SERVICE) == SNMP_FAILURE)
                {
                    VCM_CLI_PRINTF (CliHandle,
                                    "\r%% Error in creating l3 context.\r\n");
                    return CLI_FAILURE;
                }

                if (nmhSetFsVCStatus ((INT4) u4VcNum, NOT_IN_SERVICE)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsVcCxtType
                    (&u4ErrorCode, (INT4) u4VcNum,
                     VCM_L2_L3_CONTEXT) == SNMP_FAILURE)
                {
                    VCM_CLI_PRINTF (CliHandle,
                                    "\r%% Error in setting context type to "
                                    "BOTH\r\n");
                    return CLI_FAILURE;
                }

                if (nmhSetFsVcCxtType (u4VcNum, VCM_L2_L3_CONTEXT) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsVCStatus (&u4ErrorCode, (INT4) u4VcNum, ACTIVE)
                    == SNMP_FAILURE)
                {
                    VCM_CLI_PRINTF (CliHandle,
                                    "\r%% Error in creating l3 context.\r\n");
                    return CLI_FAILURE;
                }

                if (nmhSetFsVCStatus ((INT4) u4VcNum, ACTIVE) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                /* The execution of this command must return the VCM configuration
                 * mode */

                SPRINTF ((CHR1 *) au1IfName, "%s%u", CLI_VCM_MODE, u4VcNum);
                CliChangePath ((CHR1 *) au1IfName);
                return CLI_SUCCESS;
            }

            /* New L2 context creation */
            if (VcmGetNextAvailableVcNum (&u4VcNum) != VCM_SUCCESS)
            {
                VCM_CLI_PRINTF (CliHandle, "\r%% MAX VC Reached \r\n");
                return CLI_FAILURE;
            }
            if (SNMP_FAILURE == nmhTestv2FsVCStatus (&u4ErrorCode,
                                                     (INT4) u4VcNum, ACTIVE))
            {
                VCM_CLI_PRINTF (CliHandle,
                                "\r%% Error while Creating New Context.\r\n");
                return CLI_FAILURE;
            }
            if (SNMP_FAILURE == nmhSetFsVCStatus ((INT4) u4VcNum, ACTIVE))
            {
                VCM_CLI_PRINTF (CliHandle,
                                "\r%% Error while Creating New Context.\r\n");
                return CLI_FAILURE;
            }
            pOctetAlias =
                SNMP_AGT_FormOctetString (pu1Alias,
                                          (INT4) STRLEN (pu1Alias) + 1);

            if (pOctetAlias == NULL)
            {
                VCM_CLI_PRINTF (CliHandle, "\r%% Error: in setting alias \r\n");
                return CLI_FAILURE;
            }

            pOctetAlias->pu1_OctetList[STRLEN (pu1Alias)] = '\0';
            pOctetAlias->i4_Length = (INT4) STRLEN (pu1Alias);

            if (SNMP_FAILURE == nmhTestv2FsVcAlias (&u4ErrorCode,
                                                    (INT4) u4VcNum,
                                                    pOctetAlias))
            {
                VCM_CLI_PRINTF (CliHandle, "\r%% Error: in setting alias \r\n");
                SNMP_AGT_FreeOctetString (pOctetAlias);
                return CLI_FAILURE;
            }
            if (SNMP_FAILURE == nmhSetFsVcAlias (u4VcNum, pOctetAlias))
            {
                VCM_CLI_PRINTF (CliHandle, "\r%% Error: in setting alias \r\n");
                SNMP_AGT_FreeOctetString (pOctetAlias);
                return CLI_FAILURE;
            }
            SNMP_AGT_FreeOctetString (pOctetAlias);
        }
        else
        {
            u4VcNum = u4DelVcNum;
        }
    }
    else
    {
        if (i4RetVal != VCM_TRUE)
        {
            VCM_CLI_PRINTF (CliHandle,
                            "\r%% virtual context %s doesn't exist.\r\n",
                            pu1Alias);
            return CLI_FAILURE;
        }
        else
        {
            if (u4DelVcNum == VCM_DEFAULT_CONTEXT)
            {
                VCM_CLI_PRINTF (CliHandle,
                                "\r%% Deleting default context not allowed.\r\n");
                return CLI_FAILURE;
            }

            nmhGetFsVcCxtType ((INT4) u4DelVcNum, &i4CxtType);
            if (i4CxtType == VCM_L2_L3_CONTEXT)
            {
                /* Context was both l2 and l3 context. So now change the context
                 * type to l3 context alone */

                if (nmhTestv2FsVCStatus (&u4ErrorCode, (INT4) u4DelVcNum,
                                         NOT_IN_SERVICE) == SNMP_FAILURE)
                {
                    VCM_CLI_PRINTF (CliHandle,
                                    "\r%% Error in deleting l2 context.\r\n");
                    return CLI_FAILURE;
                }

                if (nmhSetFsVCStatus ((INT4) u4DelVcNum, NOT_IN_SERVICE)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsVcCxtType
                    (&u4ErrorCode, (INT4) u4DelVcNum,
                     VCM_L3_CONTEXT) == SNMP_FAILURE)
                {
                    VCM_CLI_PRINTF (CliHandle,
                                    "\r%% Error in setting context type to "
                                    "L3 context\r\n");
                    return CLI_FAILURE;
                }

                if (nmhSetFsVcCxtType (u4DelVcNum, VCM_L3_CONTEXT) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsVCStatus
                    (&u4ErrorCode, (INT4) u4DelVcNum, ACTIVE) == SNMP_FAILURE)
                {
                    VCM_CLI_PRINTF (CliHandle,
                                    "\r%% Error in deleting l3 context.\r\n");
                    return CLI_FAILURE;
                }

                if (nmhSetFsVCStatus ((INT4) u4DelVcNum, ACTIVE) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                return CLI_SUCCESS;

            }
            if (SNMP_FAILURE == nmhTestv2FsVCStatus (&u4ErrorCode,
                                                     (INT4) u4DelVcNum,
                                                     DESTROY))
            {
                VCM_CLI_PRINTF (CliHandle,
                                "\r%% Some Ports are still mapped to this context, "
                                "Unmap all the ports before deleting the context.\r\n");
                return CLI_FAILURE;
            }
            if (SNMP_FAILURE == nmhSetFsVCStatus ((INT4) u4DelVcNum, DESTROY))
            {
                VCM_CLI_PRINTF (CliHandle,
                                "\r%% Error while Deleting Context.\r\n");
                return CLI_FAILURE;
            }
            return CLI_SUCCESS;
        }
    }

    /* The execution of this command must return the VCM configuration
     * mode */

    SPRINTF ((CHR1 *) au1IfName, "%s%u", CLI_VCM_MODE, u4VcNum);
    CliChangePath ((CHR1 *) au1IfName);
    return CLI_SUCCESS;
}

#ifdef VRF_WANTED
INT4
VcmCreateVRContext (tCliHandle CliHandle, UINT1 *pu1Alias)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4VcNum = 0;
    INT4                i4RetVal = 0;
    tSNMP_OCTET_STRING_TYPE *pOctetAlias = NULL;

    i4RetVal = VcmIsVrfExist (pu1Alias, &u4VcNum);

    /* Check whether entry already exists for this VC,
     * if it exists just return SUCCESS */
    if (i4RetVal != VCM_TRUE)
    {
        /* Check if an l2 context exist. If So just change the context type
         * to both and return. */
        if (VcmIsSwitchExist (pu1Alias, &u4VcNum) == VCM_TRUE)
        {
            if (nmhTestv2FsVCStatus
                (&u4ErrorCode, (INT4) u4VcNum, NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                VCM_CLI_PRINTF (CliHandle,
                                "\r%% Error in creating l3 context.\r\n");
                return CLI_FAILURE;
            }

            if (nmhSetFsVCStatus ((INT4) u4VcNum, NOT_IN_SERVICE) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhTestv2FsVcCxtType
                (&u4ErrorCode, (INT4) u4VcNum,
                 VCM_L2_L3_CONTEXT) == SNMP_FAILURE)
            {
                VCM_CLI_PRINTF (CliHandle,
                                "\r%% Error in setting context type to BOTH\r\n");
                return CLI_FAILURE;
            }

            if (nmhSetFsVcCxtType (u4VcNum, VCM_L2_L3_CONTEXT) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhTestv2FsVCStatus (&u4ErrorCode, (INT4) u4VcNum, ACTIVE)
                == SNMP_FAILURE)
            {
                VCM_CLI_PRINTF (CliHandle,
                                "\r%% Error in creating l3 context.\r\n");
                return CLI_FAILURE;
            }

            if (nmhSetFsVCStatus ((INT4) u4VcNum, ACTIVE) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            return CLI_SUCCESS;
        }

        /* New context creation */
        if (VcmGetNextAvailableVcNum (&u4VcNum) != VCM_SUCCESS)
        {
            VCM_CLI_PRINTF (CliHandle, "\r%% MAX VC Reached \r\n");
            return CLI_FAILURE;
        }
        if (SNMP_FAILURE == nmhTestv2FsVCStatus (&u4ErrorCode,
                                                 (INT4) u4VcNum,
                                                 CREATE_AND_WAIT))
        {
            VCM_CLI_PRINTF (CliHandle,
                            "\r%% Error while Creating New Context.\r\n");
            return CLI_FAILURE;
        }
        if (SNMP_FAILURE == nmhSetFsVCStatus ((INT4) u4VcNum, CREATE_AND_WAIT))
        {
            VCM_CLI_PRINTF (CliHandle,
                            "\r%% Error while Creating New Context.\r\n");
            return CLI_FAILURE;
        }
        if (SNMP_FAILURE == nmhTestv2FsVcCxtType (&u4ErrorCode,
                                                  (INT4) u4VcNum,
                                                  VCM_L3_CONTEXT))
        {
            VCM_CLI_PRINTF (CliHandle,
                            "\r%% Error while Creating New Context. \r\n");
            SNMP_AGT_FreeOctetString (pOctetAlias);
            return CLI_FAILURE;
        }
        if (SNMP_FAILURE == nmhSetFsVcCxtType (u4VcNum, VCM_L3_CONTEXT))
        {
            VCM_CLI_PRINTF (CliHandle,
                            "\r%% Error while Creating New Context. \r\n");
            SNMP_AGT_FreeOctetString (pOctetAlias);
            return CLI_FAILURE;
        }

        pOctetAlias =
            SNMP_AGT_FormOctetString (pu1Alias, (INT4) (STRLEN (pu1Alias) + 1));

        if (pOctetAlias == NULL)
        {
            VCM_CLI_PRINTF (CliHandle, "\r%% Error: in setting alias \r\n");
            return CLI_FAILURE;
        }

        pOctetAlias->pu1_OctetList[STRLEN (pu1Alias)] = '\0';
        pOctetAlias->i4_Length = (INT4) STRLEN (pu1Alias);

        if (SNMP_FAILURE == nmhTestv2FsVcAlias (&u4ErrorCode,
                                                (INT4) u4VcNum, pOctetAlias))
        {
            VCM_CLI_PRINTF (CliHandle, "\r%% Error: in setting alias \r\n");
            SNMP_AGT_FreeOctetString (pOctetAlias);
            return CLI_FAILURE;
        }
        if (SNMP_FAILURE == nmhSetFsVcAlias (u4VcNum, pOctetAlias))
        {
            VCM_CLI_PRINTF (CliHandle, "\r%% Error: in setting alias \r\n");
            SNMP_AGT_FreeOctetString (pOctetAlias);
            return CLI_FAILURE;
        }
        if (SNMP_FAILURE == nmhTestv2FsVCStatus (&u4ErrorCode,
                                                 (INT4) u4VcNum, ACTIVE))
        {
            VCM_CLI_PRINTF (CliHandle,
                            "\r%% Error while Creating New Context.\r\n");
            return CLI_FAILURE;
        }

        if (SNMP_FAILURE == nmhSetFsVCStatus ((INT4) u4VcNum, ACTIVE))
        {
            VCM_CLI_PRINTF (CliHandle,
                            "\r%% Error while Creating New Context.\r\n");
            return CLI_FAILURE;
        }

        SNMP_AGT_FreeOctetString (pOctetAlias);
    }

    return CLI_SUCCESS;
}

INT4
VcmDeleteVRContext (tCliHandle CliHandle, UINT1 *pu1Alias)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4VcNum = 0;
    INT4                i4RetVal = 0;
    INT4                i4CxtType = 0;

    i4RetVal = VcmIsVrfExist (pu1Alias, &u4VcNum);

    if (i4RetVal != VCM_TRUE)
    {
        VCM_CLI_PRINTF (CliHandle,
                        "\r%% virtual context %s doesn't exist.\r\n", pu1Alias);
        return CLI_FAILURE;
    }
    else
    {
        if (u4VcNum == VCM_DEFAULT_CONTEXT)
        {
            VCM_CLI_PRINTF (CliHandle,
                            "\r%% Deleting default context not allowed.\r\n");
            return CLI_FAILURE;
        }

        nmhGetFsVcCxtType ((INT4) u4VcNum, &i4CxtType);
        if (i4CxtType == VCM_L2_L3_CONTEXT)
        {
            /* Context was both l2 and l3 context. So now change the context
             * type to l2 context alone */

            if (nmhTestv2FsVCStatus (&u4ErrorCode, (INT4) u4VcNum,
                                     NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                VCM_CLI_PRINTF (CliHandle,
                                "\r%% Error in deleting l2 context.\r\n");
                return CLI_FAILURE;
            }

            if (nmhSetFsVCStatus ((INT4) u4VcNum, NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhTestv2FsVcCxtType
                (&u4ErrorCode, (INT4) u4VcNum, VCM_L2_CONTEXT) == SNMP_FAILURE)
            {
                VCM_CLI_PRINTF (CliHandle,
                                "\r%% Error in setting context type to "
                                "L3 context\r\n");
                return CLI_FAILURE;
            }

            if (nmhSetFsVcCxtType (u4VcNum, VCM_L2_CONTEXT) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhTestv2FsVCStatus (&u4ErrorCode, (INT4) u4VcNum, ACTIVE)
                == SNMP_FAILURE)
            {
                VCM_CLI_PRINTF (CliHandle,
                                "\r%% Error in deleting l3 context.\r\n");
                return CLI_FAILURE;
            }

            if (nmhSetFsVCStatus ((INT4) u4VcNum, ACTIVE) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            return CLI_SUCCESS;
        }

        if (SNMP_FAILURE == nmhTestv2FsVCStatus (&u4ErrorCode,
                                                 (INT4) u4VcNum, DESTROY))
        {
            return CLI_FAILURE;
        }
        if (SNMP_FAILURE == nmhSetFsVCStatus ((INT4) u4VcNum, DESTROY))
        {
            VCM_CLI_PRINTF (CliHandle,
                            "\r%% Error while Deleting Context.\r\n");
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}
#endif
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmMapPort                                         */
/*                                                                           */
/*     DESCRIPTION      : This function creates a mapping of interface to    */
/*                        virtual conext by creating an entry in IfMap table.*/
/*                                                                           */
/*     INPUT            : CliHandle  - Context in which the CLI              */
/*                                             command is processed          */
/*                        u4IfIndex  - Interface to Get Map.                 */
/*                        pu1Alias   - Alias name of the context.            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS / CLI_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmMapPort (tCliHandle CliHandle, UINT4 u4IfIndex, UINT1 *pu1Alias)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4IfMapVcNum = 0;
    UINT4               u4VcNum = 0;
    INT4                i4RetVal = 0;
    UINT1               au1IfAlias[VCM_ALIAS_MAX_LEN];
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];

    if (VcmIsSwitchExist (pu1Alias, &u4VcNum) != VCM_TRUE)
    {
        VCM_CLI_PRINTF (CliHandle, "\r%% Switch %s Does not exist.\r\n",
                        pu1Alias);
        return CLI_FAILURE;
    }

    i4RetVal = VcmIsIfMapExist (u4IfIndex);
    VcmGetIfMapVcId (u4IfIndex, &u4IfMapVcNum);

    if (i4RetVal == VCM_TRUE)
    {
        /* Mapping Entry exists. */
        if (VcmCfaCliGetIfName (u4IfIndex, (INT1 *) au1IfName) == CLI_SUCCESS)
        {

            /* Check whether this interface is mapped to this VC or different. */
            if (u4VcNum == u4IfMapVcNum)
            {
                return CLI_SUCCESS;
            }
            else
            {
                VcmGetVcAlias (u4IfMapVcNum, au1IfAlias);
                VCM_CLI_PRINTF (CliHandle,
                                "\r%% Interface %s already mapped to VC %s.\r\n",
                                au1IfName, au1IfAlias);
                return CLI_FAILURE;
            }
        }
        else
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        /* There is no mapping. Create the row with create and wait.
         * Set the mapping. Make the row as active. */

        if (SNMP_SUCCESS != nmhTestv2FsVcIfRowStatus (&u4ErrorCode,
                                                      (INT4) u4IfIndex,
                                                      CREATE_AND_WAIT))
        {
            VCM_CLI_PRINTF (CliHandle, "\r%% Failed to create the Mapping\r\n");
            return CLI_FAILURE;
        }

        if (SNMP_SUCCESS != nmhSetFsVcIfRowStatus ((INT4) u4IfIndex,
                                                   (INT4) CREATE_AND_WAIT))
        {
            VCM_CLI_PRINTF (CliHandle, "\r%% Failed to Create IfMap Entry\r\n");
            return CLI_FAILURE;
        }

        if (SNMP_SUCCESS != nmhTestv2FsVcId (&u4ErrorCode, (INT4) u4IfIndex,
                                             (INT4) u4VcNum))
        {
            if (u4ErrorCode == SNMP_ERR_WRONG_LENGTH)
            {
                VCM_CLI_PRINTF (CliHandle, "\r%% Maximum Ports for this "
                                "context already reached.\r\n");
            }
            else
            {
                VCM_CLI_PRINTF (CliHandle, "\r%% Failed while mapping \r\n");
            }
            VcmDelIfMapEntry (CliHandle, u4IfIndex);
            return CLI_FAILURE;
        }

        if (SNMP_SUCCESS != nmhSetFsVcId ((INT4) u4IfIndex, (INT4) u4VcNum))
        {
            VCM_CLI_PRINTF (CliHandle, "\r%% Setting Virtual Context %d to "
                            "Interface Failed.\r\n", u4VcNum);
            VcmDelIfMapEntry (CliHandle, u4IfIndex);
            return CLI_FAILURE;
        }

        if (SNMP_SUCCESS != nmhTestv2FsVcIfRowStatus (&u4ErrorCode,
                                                      (INT4) u4IfIndex, ACTIVE))
        {
            VCM_CLI_PRINTF (CliHandle,
                            "\r%% Error while mapping the interface.\r\n");
            VcmDelIfMapEntry (CliHandle, u4IfIndex);
            return CLI_FAILURE;
        }

        if (SNMP_SUCCESS != nmhSetFsVcIfRowStatus
            ((INT4) u4IfIndex, (INT4) ACTIVE))
        {
            VCM_CLI_PRINTF (CliHandle,
                            "\r%% Mapping Interface %d to VC %d Failed.\r\n",
                            u4IfIndex, u4VcNum);
            VcmDelIfMapEntry (CliHandle, u4IfIndex);
            return CLI_FAILURE;
        }
        return CLI_SUCCESS;
    }
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmUnMapPort                                       */
/*                                                                           */
/*     DESCRIPTION      : This function deletes mapping of interface from    */
/*                        virtual conext by deleting an entry in IfMap table.*/
/*                                                                           */
/*     INPUT            : CliHandle  - Context in which the CLI              */
/*                                             command is processed          */
/*                        u4IfIndex  - Interface to Get Map.                 */
/*                        pu1Alias   - Alias name of the context.            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS / CLI_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmUnMapPort (tCliHandle CliHandle, UINT4 u4IfIndex, UINT1 *pu1Alias)
{
    tCfaIfInfo          IfInfo;
    UINT4               u4ErrorCode = 0;
    UINT4               u4IfMapVcNum = 0;
    UINT4               u4VcNum = 0;
    INT4                i4RetVal = 0;
    INT1                ai1IfName[CFA_CLI_MAX_IF_NAME_LEN];

    i4RetVal = VcmIsIfMapExist (u4IfIndex);
    VcmGetIfMapVcId (u4IfIndex, &u4IfMapVcNum);

    if (i4RetVal == VCM_TRUE)
    {
        if (VcmIsSwitchExist (pu1Alias, &u4VcNum) != VCM_TRUE)
        {
            VCM_CLI_PRINTF (CliHandle, "\r%% Switch %s not exist.\r\n",
                            pu1Alias);
            return CLI_FAILURE;
        }
        /* Check whether this interface is mapped to this VC or not. */
        if (u4VcNum != u4IfMapVcNum)
        {
            VCM_CLI_PRINTF (CliHandle,
                            "\r%% This Interface is not mapped to %s.\r\n",
                            pu1Alias);
            return CLI_FAILURE;
        }
        else
        {

            /* Through CLI, VIP interfaces can't be created directly. It
             * gets created internally, when Service-Instance command is
             * given. Hence, VIP interfaces are not allowed to unmap from
             * a context, through CLI. */

            VcmCfaGetIfInfo (u4IfIndex, &IfInfo);

            if (IfInfo.u1BrgPortType == VCM_VIRTUAL_INSTANCE_PORT)
            {
                VCM_CLI_PRINTF (CliHandle, "\r%% VIP interface can't be "
                                "unmapped from the Switch\r\n");
                return CLI_FAILURE;
            }

            /* Its for NO command. Destroy the row. */
            if (SNMP_SUCCESS == nmhTestv2FsVcIfRowStatus (&u4ErrorCode,
                                                          (INT4) u4IfIndex,
                                                          DESTROY))
            {
                if (SNMP_SUCCESS == nmhSetFsVcIfRowStatus ((INT4) u4IfIndex,
                                                           (INT4) DESTROY))
                {
                    return CLI_SUCCESS;
                }
            }
            VCM_CLI_PRINTF (CliHandle,
                            "\r%% UnMapping of Interface %d  failed.\r\n",
                            u4IfIndex);
            return CLI_FAILURE;
        }
    }
    else
    {
        /* Destroy for non-existant row. Return SNMP_FAILURE */
        MEMSET (ai1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
        CfaCliGetIfName (u4IfIndex, ai1IfName);

        VCM_CLI_PRINTF (CliHandle,
                        "\r%% There is no mapping exists for Interface %s\r\n",
                        ai1IfName);
        return CLI_FAILURE;
    }
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetVcmPrompt                                    */
/*                                                                           */
/*     DESCRIPTION      : This function is prompt function for the vcm       */
/*                        mode commands.                                     */
/*                                                                           */
/*     INPUT            : pi1ModeName - Mode Name                            */
/*                                                                           */
/*     OUTPUT           : pi1DispStr  - Prompt to be printed.                */
/*                                                                           */
/*     RETURNS          : SUCCESS if mode is valid else FAILURE              */
/*                                                                           */
/*****************************************************************************/
INT1
VcmGetVcmPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = 0;
    INT4                i4VcmIndex = 0;
    UINT1               au1IfName[CLI_VCM_VC_MODE_LEN];

    if ((!pi1DispStr) || (!pi1ModeName))
    {
        return VCM_FALSE;
    }

    u4Len = STRLEN (CLI_VCM_MODE);
    if (STRNCMP (pi1ModeName, CLI_VCM_MODE, u4Len) != 0)
    {
        return VCM_FALSE;
    }

    pi1ModeName = pi1ModeName + u4Len;
    u4Len = STRLEN (pi1ModeName);

    i4VcmIndex = CLI_ATOI (pi1ModeName);

    CLI_SET_CXT_ID ((UINT4) i4VcmIndex);

    SPRINTF ((CHR1 *) au1IfName, "(config-switch)#");

    STRNCPY (pi1DispStr, au1IfName, STRLEN (au1IfName));
    pi1DispStr[STRLEN (au1IfName)] = '\0';

    return VCM_TRUE;
}

/*****************************************************************************/
/* Function Name      : VcmPrintVcBrief                                      */
/*                                                                           */
/* Description        : Prints the information in a brief manner.            */
/*                                                                           */
/* Input(s)           : CliHandle  - Context in which the CLI                */
/*                                             command is processed          */
/*                      u4ContextId - If VCM_ALL_VCS we have to display      */
/*                                    for all the VC's else for the          */
/*                                    specified Context.                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VcmPrintVcBrief (tCliHandle CliHandle, UINT4 u4ContextId)
{
    tCfaIfInfo          IfInfo;
    UINT4               u4PrevContextId = VCM_INVALID_VC;
    UINT4               u4CurrContextId = 0;
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo Dummy;
    tVcPortEntry       *pPort = NULL;
    UINT2               u2Protocol = CLI_FALSE;
    UINT1               au1SwitchAlias[VCM_ALIAS_MAX_LEN];
    UINT1               u1ExitFlag = VCM_FALSE;
    tSNMP_OCTET_STRING_TYPE *pContextOwner = NULL;
    UINT1               au1NameStr[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               au1BlankSpace[CLI_MAX_COLS];
    UINT1               u1Counter = 0;
    UINT1               u1FirstLine = 0;
    UINT1               u1MaxPortsPerLine = 2;
    UINT1               u1Column = 66;

    CliRegisterLock (CliHandle, VcmLock, VcmUnLock);
    VCM_LOCK ();

    pVcInfo = VcmGetFirstVc ();
    if (pVcInfo == NULL)
    {
        VCM_CLI_PRINTF (CliHandle, "\rNo Context Exist in the system \r\n");
        VCM_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return;
    }

    do
    {
        u4PrevContextId = pVcInfo->u4VcNum;

        if ((pVcInfo->u1CurrCxtType == VCM_L3_CONTEXT) ||
            (pVcInfo->u1RowStatus == NOT_READY))
        {
            /* Context is an L3 context. Dont print this context info and proceed 
             * to next context.
             */
            Dummy.u4VcNum = u4PrevContextId;

            pVcInfo = RBTreeGetNext (VCM_CONTEXT_TABLE, &Dummy, NULL);

            if (pVcInfo == NULL)
            {
                u1ExitFlag = VCM_TRUE;
            }
            continue;
        }

        if ((u4ContextId == VCM_ALL_VCS) || (u4ContextId == pVcInfo->u4VcNum))
        {
            u4CurrContextId = pVcInfo->u4VcNum;

            VCM_MEMSET (au1SwitchAlias, 0, VCM_ALIAS_MAX_LEN);
            VCM_MEMCPY (au1SwitchAlias, pVcInfo->au1Alias, VCM_ALIAS_MAX_LEN);

            TMO_SLL_Scan (&pVcInfo->VcPortList, pPort, tVcPortEntry *)
            {
                VcmCfaGetIfInfo (pPort->pIf->u4IfIndex, &IfInfo);
                if (u2Protocol == CLI_FALSE)
                {
                    if (IfInfo.u1IfOperStatus == CFA_IF_UP)
                    {
                        u2Protocol = CLI_TRUE;
                        break;
                    }
                }
            }
            pContextOwner = allocmem_octetstring (VCM_OWNER_MAX_SIZE);
            /* get the owner of the corresponding context */
            if (pContextOwner != NULL)
            {
                nmhGetFsVcOwner ((INT4) pVcInfo->u4VcNum, pContextOwner);
            }
            VCM_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            if (pContextOwner != NULL)
            {
                VCM_CLI_PRINTF (CliHandle, "%-5d %-32s %-16s ",
                                u4CurrContextId, au1SwitchAlias,
                                pContextOwner->pu1_OctetList);

                SNMP_AGT_FreeOctetString (pContextOwner);
            }
            if (u2Protocol == CLI_TRUE)
            {
                VCM_CLI_PRINTF (CliHandle, "%-10s", " Up");
                u2Protocol = CLI_FALSE;
            }
            else
            {
                VCM_CLI_PRINTF (CliHandle, "%-10s", " Down");
            }
            u1Counter = 0;
            u1FirstLine = CLI_TRUE;
            u1MaxPortsPerLine = 2;
            TMO_SLL_Scan (&pVcInfo->VcPortList, pPort, tVcPortEntry *)
            {
                VcmCfaGetIfInfo (pPort->pIf->u4IfIndex, &IfInfo);
                MEMSET (au1BlankSpace, 0, CLI_MAX_COLS);

                if (u1Column >= (CLI_MAX_COLS - (u1MaxPortsPerLine * 6) - 1))
                {
                    VCM_CLI_PRINTF (CliHandle,
                                    "%% Column position is out of range\r\n");
                }
                /* 0x20 is Ascii code for SPACE */
                MEMSET (au1BlankSpace, CLI_BLANKSPACE_ASCII, u1Column);
                au1BlankSpace[u1Column] = '\0';

                if (VcmCfaCliGetIfName
                    (pPort->pIf->u4IfIndex, (INT1 *) au1NameStr) == CLI_FAILURE)
                {
                    VCM_CLI_PRINTF (CliHandle, "%% Invalid Port %d\r\n",
                                    pPort->pIf->u4IfIndex);
                }

                if (u1Counter == 0)
                {
                    if (u1FirstLine == CLI_TRUE)
                    {
                        VCM_CLI_PRINTF (CliHandle, "%s", au1NameStr);
                        u1FirstLine = CLI_FALSE;
                    }
                    else
                    {
                        VCM_CLI_PRINTF (CliHandle, "%s%s",
                                        au1BlankSpace, au1NameStr);
                    }
                }
                else
                {
                    VCM_CLI_PRINTF (CliHandle, ", %s", au1NameStr);
                }
                u1Counter++;
                /* Printing upto u1MaxPortsPerLine ports per line */
                if (u1Counter == u1MaxPortsPerLine)
                {
                    VCM_CLI_PRINTF (CliHandle, "\r\n");
                    u1Counter = 0;
                }

            }
            VCM_CLI_PRINTF (CliHandle, "\r\n");

            CliRegisterLock (CliHandle, VcmLock, VcmUnLock);
            VCM_LOCK ();
        }

        Dummy.u4VcNum = u4PrevContextId;

        pVcInfo = RBTreeGetNext (VCM_CONTEXT_TABLE, &Dummy, NULL);

        if (pVcInfo == NULL)
        {
            u1ExitFlag = VCM_TRUE;
        }

    }
    while (u1ExitFlag == VCM_FALSE);
    VCM_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return;
}

/*****************************************************************************/
/* Function Name      : VcmPrintVrfBrief                                     */
/*                                                                           */
/* Description        : Prints the information in a brief manner.            */
/*                                                                           */
/* Input(s)           : CliHandle  - Context in which the CLI                */
/*                                             command is processed          */
/*                      u4ContextId - If VCM_ALL_VCS we have to display      */
/*                                    for all the VC's else for the          */
/*                                    specified Context.                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VcmPrintVrfBrief (tCliHandle CliHandle, UINT4 u4ContextId)
{
    INT4                i4PrevContextId = (INT4) VCM_INVALID_VC;
    UINT4               au4IfArray[IPIF_MAX_LOGICAL_IFACES];
    UINT4               au4IpIfArray[IPIF_MAX_LOGICAL_IFACES];
    UINT4               u4PagingStatus = 0;
    UINT4               u4Var = 0;
    INT4                i4CurrCxtId = 0;
    INT4                i4RowStatus = 0;
    UINT2               u2Index = 0;
    UINT1               au1VrfAlias[VCM_ALIAS_MAX_LEN];
    UINT1               u1ExitFlag = VCM_FALSE;
    tSNMP_OCTET_STRING_TYPE CxtAlias;
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4CxtType = 0;
    UINT1               au1String[21];
    tMacAddr            MacAddr;
    MEMSET (au1VrfAlias, 0, VCM_ALIAS_MAX_LEN);
    CxtAlias.pu1_OctetList = au1VrfAlias;
    CxtAlias.i4_Length = 0;

    i1RetVal = nmhGetFirstIndexFsVcmConfigTable (&i4CurrCxtId);
    if (i1RetVal == SNMP_FAILURE)
    {
        VCM_CLI_PRINTF (CliHandle, "\rNo Context Exist in the system \r\n");
        return;
    }

    do
    {
        u2Index = 0;
        i4PrevContextId = i4CurrCxtId;

        nmhGetFsVcCxtType (i4CurrCxtId, &i4CxtType);
        nmhGetFsVCStatus (i4CurrCxtId, &i4RowStatus);
        VcmIssGetVRMacAddr ((UINT4) i4CurrCxtId, &MacAddr);
        if ((i4CxtType == VCM_L2_CONTEXT) || (i4RowStatus == NOT_READY))
        {
            /* Context is an L3 context. Dont print this context info and proceed 
             * to next context.
             */
            i1RetVal = nmhGetNextIndexFsVcmConfigTable (i4PrevContextId,
                                                        &i4CurrCxtId);
            if (i1RetVal == SNMP_FAILURE)
            {
                u1ExitFlag = VCM_TRUE;
            }
            continue;
        }

        if ((u4ContextId == VCM_ALL_VCS)
            || (u4ContextId == (UINT4) i4CurrCxtId))
        {
            VCM_MEMSET (au1VrfAlias, 0, VCM_ALIAS_MAX_LEN);
            nmhGetFsVcAlias (i4CurrCxtId, &CxtAlias);

            VCM_MEMSET (au4IpIfArray, 0, sizeof (au4IpIfArray));

            if (VcmGetCxtIpIfaceList ((UINT4) i4CurrCxtId, au4IpIfArray) ==
                VCM_SUCCESS)
            {
                for (u4Var = 1; ((u4Var <= au4IpIfArray[0])
                                 && (u4Var < IPIF_MAX_LOGICAL_IFACES)); u4Var++)
                {
                    au4IfArray[u2Index++] = au4IpIfArray[u4Var];
                }
                VCM_CLI_PRINTF (CliHandle, "%-5d %-32s ", i4CurrCxtId,
                                au1VrfAlias);
                PrintMacAddress (MacAddr, au1String);
                VCM_CLI_PRINTF (CliHandle, "%-25s", au1String);
                VcmPrintPortArray (CliHandle, au4IfArray, u2Index, 50,
                                   &u4PagingStatus, 3);
            }
        }
        i1RetVal = nmhGetNextIndexFsVcmConfigTable (i4PrevContextId,
                                                    &i4CurrCxtId);
        if (i1RetVal == SNMP_FAILURE)
        {
            u1ExitFlag = VCM_TRUE;
        }

    }
    while (u1ExitFlag == VCM_FALSE);
    return;
}

/*****************************************************************************/
/* Function Name      : VcmPrintVcDetail                                     */
/*                                                                           */
/* Description        : Prints all the elements present in the VC tree.      */
/*                                                                           */
/* Input(s)           : CliHandle  - Context in which the CLI                */
/*                                             command is processed          */
/*                      u4ContextId - If VCM_ALL_VCS we have to display      */
/*                                    for all the VC's else for the          */
/*                                    specified Context.                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VcmPrintVcDetail (tCliHandle CliHandle, UINT4 u4ContextId)
{
    UINT4               u4PrevContextId = VCM_INVALID_VC;
    UINT4               u4CurrContextId = 0;
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo Dummy;
    tVcPortEntry       *pPort = NULL;
    UINT2               u2CurrFreeLocalHlPort = 0;
    UINT1               au1SwitchAlias[VCM_ALIAS_MAX_LEN];
    UINT1               au1String[21];
    UINT1               u1ExitFlag = VCM_FALSE;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    tSNMP_OCTET_STRING_TYPE *pContextOwner = NULL;
    CliRegisterLock (CliHandle, VcmLock, VcmUnLock);
    VCM_LOCK ();

    pVcInfo = VcmGetFirstVc ();
    if (pVcInfo == NULL)
    {
        VCM_CLI_PRINTF (CliHandle, "\rNo Context Exist in the system \r\n");
        VCM_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return;
    }
    do
    {
        u4PrevContextId = pVcInfo->u4VcNum;

        if ((pVcInfo->u1CurrCxtType == VCM_L3_CONTEXT) ||
            (pVcInfo->u1RowStatus == NOT_READY))
        {
            /* Context is an L3 context. Dont print this context info and proceed 
             * to next context. Also don't print the context if it is under 
             * creation as we don't know the context type 
             */
            Dummy.u4VcNum = u4PrevContextId;

            pVcInfo = RBTreeGetNext (VCM_CONTEXT_TABLE, &Dummy, NULL);

            if (pVcInfo == NULL)
            {
                u1ExitFlag = VCM_TRUE;
            }
            continue;
        }

        if ((u4ContextId == VCM_ALL_VCS) || (u4ContextId == pVcInfo->u4VcNum))
        {
            u4CurrContextId = pVcInfo->u4VcNum;
            VCM_MEMSET (au1SwitchAlias, 0, VCM_ALIAS_MAX_LEN);
            VCM_MEMCPY (au1SwitchAlias, pVcInfo->au1Alias, VCM_ALIAS_MAX_LEN);
            PrintMacAddress (pVcInfo->VcBridgeAddr, au1String);
            u2CurrFreeLocalHlPort = pVcInfo->u2NextFreeHlPortId;

            pContextOwner = allocmem_octetstring (VCM_OWNER_MAX_SIZE);
            /* get the owner of the corresponding context */

            if (pContextOwner != NULL)
            {
                nmhGetFsVcOwner ((INT4) pVcInfo->u4VcNum, pContextOwner);
            }
            VCM_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            VCM_CLI_PRINTF (CliHandle, "Switch Name : %s\r\n", au1SwitchAlias);
            VCM_CLI_PRINTF (CliHandle,
                            "Switch Context-Id : %d\r\n", u4CurrContextId);
            VCM_CLI_PRINTF (CliHandle,
                            "Switch MAC-Address : %s\r\n", au1String);

            if (pContextOwner != NULL)
            {
                VCM_CLI_PRINTF (CliHandle, "Switch Owner : %s\r\n",
                                pContextOwner->pu1_OctetList);
                SNMP_AGT_FreeOctetString (pContextOwner);
            }
            if (u2CurrFreeLocalHlPort == VCM_INVALID_HLPORT)
            {
                VCM_CLI_PRINTF (CliHandle,
                                "Next Free Local Port-Id : None\r\n");
            }
            else
            {
                VCM_CLI_PRINTF (CliHandle, "Next Free Local Port-Id : %d\r\n",
                                u2CurrFreeLocalHlPort);
            }
            VCM_CLI_PRINTF (CliHandle, "Ports present in this context:\r\n");

            TMO_SLL_Scan (&pVcInfo->VcPortList, pPort, tVcPortEntry *)
            {
                if (VcmCfaCliGetIfName (pPort->pIf->u4IfIndex,
                                        (INT1 *) au1NameStr) == CLI_SUCCESS)
                {
                    VCM_CLI_PRINTF (CliHandle,
                                    "\t%s with LocalPort Id as %d \r\n",
                                    au1NameStr,
                                    VCM_IFMAP_HLPORTID (pPort->pIf));
                }
            }
            VCM_CLI_PRINTF (CliHandle, "\r\n");

            CliRegisterLock (CliHandle, VcmLock, VcmUnLock);
            VCM_LOCK ();
        }

        Dummy.u4VcNum = u4PrevContextId;

        pVcInfo = RBTreeGetNext (VCM_CONTEXT_TABLE, &Dummy, NULL);

        if (pVcInfo == NULL)
        {
            u1ExitFlag = VCM_TRUE;
        }

    }
    while (u1ExitFlag == VCM_FALSE);
    VCM_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return;
}

/*****************************************************************************/
/* Function Name      : VcmPrintVcMapDetail                                  */
/*                                                                           */
/* Description        : Prints Mapping of a particular port or all the ports */
/*                      to the contexts. It displays Primary context and list*/
/*                      of secondary contexts to which the particular port   */
/*                      is mapped                                            */
/*                                                                           */
/* Input(s)           : CliHandle  - Context in which the CLI                */
/*                                             command is processed          */
/*                      u4IfIndex   - IfIndex                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VcmPrintVcMapDetail (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    tVirtualContextInfo Dummy;
    tVcmIfMapEntry     *pIfMapEntry = NULL;
    tVcmIfMapEntry      DummyIfMap;
    tVcmIfMapEntry      VcmIfLogMapEntry;
    tVcmIfMapEntry     *pVcmIfLogMapEntry = NULL;
    tVirtualContextInfo *pVcInfo = NULL;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1SwitchAlias[VCM_ALIAS_MAX_LEN];
    UINT1               u1Res;
    UINT1               u1First;

    /* This command should not be executed for SISP interfaces
     * */

    if (SISP_IS_LOGICAL_PORT (u4IfIndex) != VCM_FALSE)
    {
        VCM_CLI_PRINTF (CliHandle, "\r\n%% Invalid Interface Index\r\n");
        return;
    }

    VCM_CLI_PRINTF (CliHandle, "\rPort Context Mapping Info \r\n");
    VCM_CLI_PRINTF (CliHandle, "\r========================= \r\n");

    CliRegisterLock (CliHandle, VcmLock, VcmUnLock);
    VCM_LOCK ();

    if (u4IfIndex == 0)
    {
        pIfMapEntry = VcmGetFirstIfMap ();
        if (pIfMapEntry == NULL)
        {
            VCM_CLI_PRINTF (CliHandle,
                            "\rNo Mapping Present in the system \r\n");

            VCM_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return;
        }
    }
    else
    {
        DummyIfMap.u4IfIndex = u4IfIndex;
        pIfMapEntry = VcmFindIfMapEntry (&DummyIfMap);

        if (pIfMapEntry == NULL)
        {
            VCM_CLI_PRINTF (CliHandle, "\rPort not Present in the system \r\n");
            VCM_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return;
        }
    }

    VCM_CLI_PRINTF (CliHandle,
                    "\r------------------------------------------ \r\n");
    do
    {
        if (SISP_IS_LOGICAL_PORT (pIfMapEntry->u4IfIndex) == VCM_FALSE)
        {
            u1First = 1;
            VCM_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            if (VcmCfaCliGetIfName (pIfMapEntry->u4IfIndex, (INT1 *) au1IfName)
                == CLI_FAILURE)
            {
                return;
            }
            CliRegisterLock (CliHandle, VcmLock, VcmUnLock);
            VCM_LOCK ();

            Dummy.u4VcNum = pIfMapEntry->u4VcNum;
            pVcInfo = VcmFindVcEntry (&Dummy);

            VCM_MEMSET (au1SwitchAlias, 0, VCM_ALIAS_MAX_LEN);
            if (pVcInfo != NULL)
            {
                VCM_MEMCPY (au1SwitchAlias, pVcInfo->au1Alias,
                            VCM_ALIAS_MAX_LEN);
            }

            VCM_CLI_PRINTF (CliHandle, "\rPort               : %s\r\n",
                            au1IfName);
            VCM_CLI_PRINTF (CliHandle, "\rPrimary Context    : %s\r\n",
                            au1SwitchAlias);
            VCM_CLI_PRINTF (CliHandle, "\rSecondary Contexts : ");

            L2IwfGetSispPortCtrlStatus (pIfMapEntry->u4IfIndex, &u1Res);

            if (u1Res == SISP_ENABLE)
            {
                /* Getting the First entry */
                /* Getting the First entry */
                VcmIfLogMapEntry.u4PhyIfIndex = pIfMapEntry->u4IfIndex - 1;
                VcmIfLogMapEntry.u4VcNum = VCM_INVALID_CONTEXT_ID;

                pVcmIfLogMapEntry = (tVcmIfMapEntry *)
                    VCM_RB_TREE_GETNEXT (SISP_LOGICAL_PORT_TABLE,
                                         &VcmIfLogMapEntry, NULL);

                while ((pVcmIfLogMapEntry != NULL) &&
                       (pIfMapEntry->u4IfIndex ==
                        pVcmIfLogMapEntry->u4PhyIfIndex))
                {
                    if (u1First)
                    {
                        VCM_CLI_PRINTF (CliHandle, "VcName       SispPort\n");
                        VCM_CLI_PRINTF (CliHandle,
                                        "                     ------       --------\r\n");
                        u1First = 0;
                    }
                    VCM_UNLOCK ();
                    CliUnRegisterLock (CliHandle);

                    if (VcmCfaCliGetIfName (pVcmIfLogMapEntry->u4IfIndex,
                                            (INT1 *) au1IfName) == CLI_FAILURE)
                    {
                        return;
                    }
                    CliRegisterLock (CliHandle, VcmLock, VcmUnLock);

                    VCM_LOCK ();

                    Dummy.u4VcNum = pVcmIfLogMapEntry->u4VcNum;
                    pVcInfo = VcmFindVcEntry (&Dummy);

                    VCM_MEMSET (au1SwitchAlias, 0, VCM_ALIAS_MAX_LEN);
                    if (pVcInfo != NULL)
                    {
                        VCM_MEMCPY (au1SwitchAlias, pVcInfo->au1Alias,
                                    VCM_ALIAS_MAX_LEN);
                    }

                    VCM_CLI_PRINTF (CliHandle,
                                    "                      %s       -     %s\r\n",
                                    au1SwitchAlias, au1IfName);

                    VcmIfLogMapEntry.u4PhyIfIndex =
                        pVcmIfLogMapEntry->u4PhyIfIndex;

                    VcmIfLogMapEntry.u4VcNum = pVcmIfLogMapEntry->u4VcNum;

                    pVcmIfLogMapEntry = (tVcmIfMapEntry *) VCM_RB_TREE_GETNEXT
                        (SISP_LOGICAL_PORT_TABLE, &VcmIfLogMapEntry,
                         VcmSispPortMapTableCmp);
                }
                if (u1First)
                {
                    /* SISP is enabled but no logical ports configured */
                    VCM_CLI_PRINTF (CliHandle, "None\r\n");
                }
            }
            else
            {
                VCM_CLI_PRINTF (CliHandle, "None\r\n");
            }

            VCM_CLI_PRINTF (CliHandle,
                            "\r------------------------------------------ \r\n");
        }

        DummyIfMap.u4IfIndex = pIfMapEntry->u4IfIndex;

        pIfMapEntry = RBTreeGetNext (VCM_IF_MAP_TABLE, &DummyIfMap, NULL);

    }
    while ((pIfMapEntry != NULL) && (u4IfIndex == 0));
    VCM_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return;
}

/*****************************************************************************/
/* Function Name      : VcmPrintIfMapEntries                                 */
/*                                                                           */
/* Description        : Prints all the elements present in the IfMap tree.   */
/*                                                                           */
/* Input(s)           : CliHandle   - Context in which the CLI               */
/*                                             command is processed          */
/*                      u4ContextId - If VCM_ALL_VCS we have to display      */
/*                                    for all the VC's else for the          */
/*                                    specified Context.                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VcmPrintIfMapEntries (tCliHandle CliHandle, UINT4 u4VcNum)
{
    UINT4               u4PrevIfIndex = VCM_INVALID_VAL;
    UINT4               u4CurrContextId;
    UINT4               u4CurrIfIndex;
    tVcmIfMapEntry     *pIfMapEntry = NULL;
    tVcmIfMapEntry      DummyIfMap;
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo Dummy;
    UINT2               u2LocalPortId;
    UINT1               au1SwitchAlias[VCM_ALIAS_MAX_LEN];
    UINT1               u1ExitFlag = VCM_FALSE;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];

    CliRegisterLock (CliHandle, VcmLock, VcmUnLock);
    VCM_LOCK ();

    pIfMapEntry = VcmGetFirstIfMap ();
    if (pIfMapEntry == NULL)
    {
        VCM_CLI_PRINTF (CliHandle, "\rNo Mapping Present in the system \r\n");
        VCM_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return;
    }

    do
    {
        u4PrevIfIndex = pIfMapEntry->u4IfIndex;
        if (VcmIsL3Interface (pIfMapEntry->u4IfIndex) == VCM_TRUE)
        {
            /* The interface is an L3 interface. So proceed with the next
             * interface.
             */
            DummyIfMap.u4IfIndex = u4PrevIfIndex;

            pIfMapEntry = RBTreeGetNext (VCM_IF_MAP_TABLE, &DummyIfMap, NULL);

            if (pIfMapEntry == NULL)
            {
                u1ExitFlag = VCM_TRUE;
            }
            continue;
        }

        if (((u4VcNum == VCM_ALL_VCS) || (u4VcNum == pIfMapEntry->u4VcNum))
            && (pIfMapEntry->u1RowStatus == ACTIVE))
        {
            u4CurrIfIndex = pIfMapEntry->u4IfIndex;
            u4CurrContextId = pIfMapEntry->u4VcNum;
            u2LocalPortId = VCM_IFMAP_HLPORTID (pIfMapEntry);

            VCM_MEMSET (au1SwitchAlias, 0, VCM_ALIAS_MAX_LEN);

            Dummy.u4VcNum = pIfMapEntry->u4VcNum;
            pVcInfo = VcmFindVcEntry (&Dummy);
            if (pVcInfo != NULL)
            {
                VCM_MEMCPY (au1SwitchAlias, pVcInfo->au1Alias,
                            VCM_ALIAS_MAX_LEN);

                VCM_UNLOCK ();
                CliUnRegisterLock (CliHandle);

                if (VcmCfaCliGetIfName (u4CurrIfIndex, (INT1 *) au1NameStr)
                    == CLI_SUCCESS)
                {
                    VCM_CLI_PRINTF (CliHandle, "%-8s %-5d  %-32s %-11d\r\n",
                                    au1NameStr, u4CurrContextId,
                                    au1SwitchAlias, u2LocalPortId);
                }

                CliRegisterLock (CliHandle, VcmLock, VcmUnLock);
                VCM_LOCK ();
            }
        }

        DummyIfMap.u4IfIndex = u4PrevIfIndex;

        pIfMapEntry = RBTreeGetNext (VCM_IF_MAP_TABLE, &DummyIfMap, NULL);

        if (pIfMapEntry == NULL)
        {
            u1ExitFlag = VCM_TRUE;
        }

    }
    while (u1ExitFlag == VCM_FALSE);
    VCM_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return;
}

/*****************************************************************************/
/* Function Name      : VcmPrintIfMapEntries                                 */
/*                                                                           */
/* Description        : Prints all the elements present in the IfMap tree.   */
/*                                                                           */
/* Input(s)           : CliHandle   - Context in which the CLI               */
/*                                             command is processed          */
/*                      u4ContextId - If VCM_ALL_VCS we have to display      */
/*                                    for all the VC's else for the          */
/*                                    specified Context.                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VcmPrintVrfIfMapEntries (tCliHandle CliHandle, UINT4 u4VcNum)
{
    UINT4               u4PrevIfIndex = VCM_INVALID_VAL;
    UINT4               u4CurrContextId;
    UINT4               u4CurrIfIndex;
    tVcmIfMapEntry     *pIfMapEntry = NULL;
    tVcmIfMapEntry      DummyIfMap;
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo Dummy;
    UINT1               au1SwitchAlias[VCM_ALIAS_MAX_LEN];
    UINT1               u1ExitFlag = VCM_FALSE;
    UINT1               au1NameStr[CFA_CLI_MAX_IF_NAME_LEN];
    tCfaIfInfo          IfInfo;
    UINT1               au1String[21];
    tMacAddr            MacAddr;

    CliRegisterLock (CliHandle, VcmLock, VcmUnLock);
    VCM_LOCK ();

    pIfMapEntry = VcmGetFirstIfMap ();
    if (pIfMapEntry == NULL)
    {
        VCM_CLI_PRINTF (CliHandle, "\rNo Mapping Present in the system \r\n");
        VCM_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return;
    }

    do
    {
        u4PrevIfIndex = pIfMapEntry->u4IfIndex;
        if (VcmIsL3Interface (pIfMapEntry->u4IfIndex) != VCM_TRUE)
        {
            /* The interface is not an L3 interface. So proceed with the next
             * interface.
             */
            DummyIfMap.u4IfIndex = u4PrevIfIndex;

            pIfMapEntry = RBTreeGetNext (VCM_IF_MAP_TABLE, &DummyIfMap, NULL);

            if (pIfMapEntry == NULL)
            {
                u1ExitFlag = VCM_TRUE;
            }
            continue;
        }

        if (((u4VcNum == VCM_ALL_VCS) || (u4VcNum == pIfMapEntry->u4VcNum)) &&
            (pIfMapEntry->u1RowStatus == ACTIVE))
        {
            VcmCfaGetIfInfo (pIfMapEntry->u4IfIndex, &IfInfo);
            u4CurrIfIndex = pIfMapEntry->u4IfIndex;
            u4CurrContextId = pIfMapEntry->u4VcNum;
            VCM_MEMSET (au1SwitchAlias, 0, VCM_ALIAS_MAX_LEN);
            VcmIssGetVRMacAddr (u4CurrContextId, &MacAddr);
            Dummy.u4VcNum = pIfMapEntry->u4VcNum;
            pVcInfo = VcmFindVcEntry (&Dummy);
            if (pVcInfo != NULL)
            {
                VCM_MEMCPY (au1SwitchAlias, pVcInfo->au1Alias,
                            VCM_ALIAS_MAX_LEN);

                VCM_UNLOCK ();
                CliUnRegisterLock (CliHandle);

                if (VcmCfaCliGetIfName (u4CurrIfIndex, (INT1 *) au1NameStr)
                    == CLI_SUCCESS)
                {
                    VCM_CLI_PRINTF (CliHandle, "%-10s %-6d  %-32s",
                                    au1NameStr, u4CurrContextId,
                                    au1SwitchAlias);
                    PrintMacAddress (MacAddr, au1String);
                    VCM_CLI_PRINTF (CliHandle, "%-25s\r\n", au1String);
                }

                CliRegisterLock (CliHandle, VcmLock, VcmUnLock);
                VCM_LOCK ();
            }
        }

        DummyIfMap.u4IfIndex = u4PrevIfIndex;

        pIfMapEntry = RBTreeGetNext (VCM_IF_MAP_TABLE, &DummyIfMap, NULL);

        if (pIfMapEntry == NULL)
        {
            u1ExitFlag = VCM_TRUE;
        }

    }
    while (u1ExitFlag == VCM_FALSE);
    VCM_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmDelIfMapEntry                                   */
/*                                                                           */
/*     DESCRIPTION      : This function deletes the mapping if creating of   */
/*                        an entry fails.                                    */
/*                                                                           */
/*     INPUT            : CliHandle  - Context in which the CLI              */
/*                                             command is processed          */
/*                        u4IfIndex  - Interface to Get Map.                 */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS / CLI_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
VOID
VcmDelIfMapEntry (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    UINT4               u4ErrorCode = 0;

    if (SNMP_SUCCESS == nmhTestv2FsVcIfRowStatus (&u4ErrorCode,
                                                  (INT4) u4IfIndex, DESTROY))
    {
        if (SNMP_SUCCESS == nmhSetFsVcIfRowStatus ((INT4) u4IfIndex,
                                                   (INT4) DESTROY))
        {
            return;
        }
    }

    VCM_CLI_PRINTF (CliHandle, "Error in deleting IfMap Entry\r\n");

    return;
}

/*************************************************************************/
/*                                                                       */
/* FUNCTION NAME    : VcmPrintPortArray                                 */
/*                                                                       */
/* DESCRIPTION      : This function converts PortArray to port and print */
/*                    the Interface names.                                */
/*                                                                       */
/* INPUT            : pu1PortArray     - Ports as Port Array             */
/*                    u4ArrayLen       - Length of port array            */
/*                    u1Column         - starting location of printing   */
/*                    pu4PagingStatus  - Paging Status                   */
/*                    u1MaxPortsPerLine- Maximum Ports to be printed in  */
/*                                       a line                          */
/* OUTPUT           : CliHandle - Contains error messages                */
/*                                                                       */
/* RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                       */
/*************************************************************************/
INT4
VcmPrintPortArray (tCliHandle CliHandle, UINT4 *pu4PortArray, UINT4 u4ArrayLen,
                   UINT1 u1Column, UINT4 *pu4PagingStatus,
                   UINT1 u1MaxPortsPerLine)
{
    UINT1               au1NameStr[CFA_CLI_MAX_IF_NAME_LEN];
    UINT4               u4Port = 0;
    UINT2               u2Index = 0;
    UINT1               u1Counter = 0;
    UINT1               au1BlankSpace[CLI_MAX_COLS];
    UINT1               u1FirstLine = CLI_TRUE;

    MEMSET (au1BlankSpace, 0, CLI_MAX_COLS);

    if (u1Column >= (CLI_MAX_COLS - (u1MaxPortsPerLine * 6) - 1))
    {
        VCM_CLI_PRINTF (CliHandle, "%% Column position is out of range\r\n");
        return CLI_FAILURE;
    }
    /* 0x20 is Ascii code for SPACE */
    MEMSET (au1BlankSpace, CLI_BLANKSPACE_ASCII, u1Column);
    au1BlankSpace[u1Column] = '\0';

    for (u2Index = 0; u2Index < u4ArrayLen; u2Index++)
    {
        if (VcmCfaCliGetIfName (pu4PortArray[u2Index], (INT1 *) au1NameStr) ==
            CLI_FAILURE)
        {
            VCM_CLI_PRINTF (CliHandle, "%% Invalid Port %d\r\n", u4Port);
            return CLI_FAILURE;
        }
        if (u1Counter == 0)
        {
            if (u1FirstLine == CLI_TRUE)
            {
                *pu4PagingStatus =
                    (UINT4) VCM_CLI_PRINTF (CliHandle, "%s", au1NameStr);
                u1FirstLine = CLI_FALSE;
            }
            else
            {
                *pu4PagingStatus = (UINT4) VCM_CLI_PRINTF (CliHandle, "%s%s",
                                                           au1BlankSpace,
                                                           au1NameStr);
            }
        }
        else
        {
            *pu4PagingStatus =
                (UINT4) VCM_CLI_PRINTF (CliHandle, ", %s", au1NameStr);
        }
        u1Counter++;

        /* Printing upto u1MaxPortsPerLine ports per line */
        if (u1Counter == u1MaxPortsPerLine)
        {
            VCM_CLI_PRINTF (CliHandle, "\r\n");
            u1Counter = 0;
        }
    }
    VCM_CLI_PRINTF (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmShowInterfaceMapping                                 */
/*                                                                           */
/* Description        : Disaplays  Mapping of an interface                   */
/*                                                                           */
/* Input(s)           : pCliHandle  - Context in which the CLI               */
/*                                             command is processed          */
/*                      u4IfIndex - If Index of the Interface                */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/

INT4
VcmShowInterfaceMapping (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    INT4                i4RetVal = 0;
    UINT4               u4IfMapVcNum = 0;
    UINT1               au1IfAlias[VCM_ALIAS_MAX_LEN];
    UINT1               u1IfType = 0;

    i4RetVal = VcmIsIfMapExist (u4IfIndex);

    if (i4RetVal == VCM_TRUE)
    {
        /* map entry exsist */
        if (VcmGetIfMapVcId (u4IfIndex, &u4IfMapVcNum) == VCM_SUCCESS)
        {
            VcmGetVcAlias (u4IfMapVcNum, au1IfAlias);
            if ((VcmIsL3Interface (u4IfIndex) == VCM_FALSE)
                && (SYS_DEF_MAX_NUM_CONTEXTS > 1))
            {
                CliPrintf (CliHandle, "map switch %s\r\n", au1IfAlias);
            }
            else if (u4IfMapVcNum != VCM_DEFAULT_CONTEXT)
            {
                CfaGetIfType (u4IfIndex, &u1IfType);
                if ((u1IfType == CFA_ENET) || (u1IfType == CFA_LAGG))
                {
                    CliPrintf (CliHandle, "no switchport\r\n");
                }
                CliPrintf (CliHandle, "ip vrf forwarding %s\r\n", au1IfAlias);
            }
        }
        return CLI_SUCCESS;
    }
    return CLI_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmCreateOwner                                     */
/*                                                                           */
/*     DESCRIPTION      : This function will configure the value for the     */
/*                        owner of the context                               */
/*     INPUT            : u4VcId   - Context Identifier                      */
/*                        pu1Owner -the owner of the context                 */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VcmCreateOwner (tCliHandle CliHandle, UINT4 u4VcId, UINT1 *pu1Owner)
{
    tSNMP_OCTET_STRING_TYPE *pOctetOwner = NULL;
    UINT4               u4ErrCode = 0;
    pOctetOwner =
        SNMP_AGT_FormOctetString (pu1Owner, (INT4) STRLEN (pu1Owner) + 1);

    if (pOctetOwner == NULL)
    {
        VCM_CLI_PRINTF (CliHandle, "\r%% Error: in setting owner \r\n");
        return CLI_FAILURE;
    }
    pOctetOwner->pu1_OctetList[STRLEN (pu1Owner)] = '\0';

    /* set the value for owner */
    if ((nmhTestv2FsVcOwner (&u4ErrCode, (INT4) u4VcId, pOctetOwner))
        != SNMP_SUCCESS)
    {
        VCM_CLI_PRINTF (CliHandle, "\r%% Error: in setting owner \r\n");
        SNMP_AGT_FreeOctetString (pOctetOwner);
        return CLI_FAILURE;
    }

    if ((nmhSetFsVcOwner (u4VcId, pOctetOwner)) != SNMP_SUCCESS)
    {
        VCM_CLI_PRINTF (CliHandle, "\r%% Error: in setting owner \r\n");
        SNMP_AGT_FreeOctetString (pOctetOwner);
        return CLI_FAILURE;
    }
    SNMP_AGT_FreeOctetString (pOctetOwner);
    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : VcmShowOwner                                        */
/*                                                                        */
/*  Description     : Calls the low level routine to show the owner       */
/*                    of the port index given                             */
/*                                                                        */
/*  Input(s)        : CliHandle      - CliContext ID                      */
/*                    UINT4          - Port-number                        */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/
INT4
VcmShowOwner (tCliHandle CliHandle, UINT4 u4Port)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    tSNMP_OCTET_STRING_TYPE *pContextOwner = NULL;

    /* get the context Id of the port */
    if ((VcmGetContextInfoFromIfIndex (u4Port, &u4ContextId,
                                       &u2LocalPortId)) != VCM_SUCCESS)
    {
        CliPrintf (CliHandle, "Unable to get the context of the port\r\n");
        return CLI_FAILURE;
    }
    pContextOwner = allocmem_octetstring (VCM_OWNER_MAX_SIZE);

    if (NULL == pContextOwner)
    {
        CliPrintf (CliHandle, "pContextOwner is Null \r\n");
        return CLI_FAILURE;
    }
    /* get the owner of the corresponding context */
    if ((nmhGetFsVcOwner ((INT4) u4ContextId, pContextOwner)) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "Unable to get the owner of the context \r\n");
        SNMP_AGT_FreeOctetString (pContextOwner);
        return CLI_FAILURE;
    }

    /* display the owner of the port */
    if (pContextOwner->i4_Length == 0)
    {
        CliPrintf (CliHandle, "No owner set \r\n");
        SNMP_AGT_FreeOctetString (pContextOwner);
        return CLI_SUCCESS;
    }
    CliPrintf (CliHandle, "The owner of the port %d is %s\r\n", u4Port,
               pContextOwner->pu1_OctetList);
    SNMP_AGT_FreeOctetString (pContextOwner);
    return CLI_SUCCESS;

}

#ifdef VRF_WANTED
/**************************************************************************/
/*  Function Name   : VcmAddOrDeleteIpIfMap                               */
/*                                                                        */
/*  Description     : Calls the low level routine to set the context      */
/*                    and the interface information                       */
/*                                                                        */
/*  Input(s)        : CliHandle      - CliContext ID                      */
/*                    u4Flag         - Add/Delete                         */
/*                    u4IpIfIndex    - Interface IfIndex                  */
/*                    pu1L3ContextName -L3 Context name                   */
/*                                                                        */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/

INT4
VcmAddOrDeleteIpIfMap (tCliHandle CliHandle, UINT4 u4Flag,
                       UINT4 u4IpIfIndex, UINT1 *pu1L3ContextName)
{
    INT4                i4RetStatus = VCM_FAILURE;
    UINT4               u4VcNum = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4ErrorCode = 0;

    i4RetStatus = VcmIsVrfExist (pu1L3ContextName, &u4VcNum);
    if (i4RetStatus == VCM_FALSE)
    {
        VCM_CLI_PRINTF (CliHandle, "\r%% L3 Context does not exist\r\n");
        return CLI_FAILURE;
    }

    /* Check if the interface is mapped to the specified context */
    if (VcmGetContextIdFromCfaIfIndex (u4IpIfIndex, &u4ContextId)
        == VCM_FAILURE)
    {
        VCM_CLI_PRINTF (CliHandle, "\r%% Iface mapping does not exist\r\n");
        return CLI_FAILURE;
    }

    if (u4Flag == VCM_IPIFMAP_ADD)
    {
        if (L2IwfIsPortInPortChannel (u4IpIfIndex) == CFA_SUCCESS)
        {
            if (u4VcNum != VCM_DEFAULT_CONTEXT)
            {
                VCM_CLI_PRINTF (CliHandle,
                                "\r %% Interface is a member of port-channel. \r\n "
                                "Should be unmapped from port-channel before mapping it to a different vrf.\r\n");
                return CLI_FAILURE;
            }
        }
        if ((u4ContextId != VCM_DEFAULT_CONTEXT))
        {
            VCM_CLI_PRINTF (CliHandle, "\r %% Interface should be unmapped "
                            "before mapping it to a different vrf\r\n");
            return CLI_FAILURE;

        }
        if (u4VcNum == VCM_DEFAULT_CONTEXT)
        {
            VCM_CLI_PRINTF (CliHandle,
                            "\r%% Interface can not be mapped to "
                            "default vrf\r\n");
            return CLI_FAILURE;
        }
        if (u4ContextId == u4VcNum)
        {
            return CLI_SUCCESS;
        }

        u4ContextId = u4VcNum;
    }
    else
    {
        if (u4ContextId != u4VcNum)
        {
            VCM_CLI_PRINTF (CliHandle, "\r%% Interface is not mapped to this "
                            "vrf\r\n");
            return CLI_FAILURE;
        }
        if (u4VcNum == VCM_DEFAULT_CONTEXT)
        {
            VCM_CLI_PRINTF (CliHandle,
                            "\r%% Interface can not be unmapped from "
                            "default vrf\r\n");
            return CLI_FAILURE;
        }

        u4ContextId = VCM_DEFAULT_CONTEXT;
    }

    if ((nmhTestv2FsVcIfRowStatus (&u4ErrorCode,
                                   (INT4) u4IpIfIndex,
                                   NOT_IN_SERVICE)) == SNMP_FAILURE)
    {
        VCM_CLI_PRINTF (CliHandle, "\r%% Unable to set row status NIS"
                        "for FsVcmIfMappingEntry\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsVcIfRowStatus (u4IpIfIndex, NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if ((nmhTestv2FsVcId (&u4ErrorCode, (INT4) u4IpIfIndex,
                          (INT4) u4ContextId)) == SNMP_FAILURE)
    {
        nmhSetFsVcIfRowStatus (u4IpIfIndex, ACTIVE);
        return CLI_FAILURE;
    }

    if (nmhSetFsVcId (u4IpIfIndex, u4ContextId) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if ((nmhTestv2FsVcIfRowStatus (&u4ErrorCode,
                                   (INT4) u4IpIfIndex, ACTIVE)) == SNMP_FAILURE)
    {
        VCM_CLI_PRINTF (CliHandle, "\r%% Unable to set row status active"
                        "for FsVcmIfMappingEntry\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsVcIfRowStatus (u4IpIfIndex, ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : VcmCreateOrDeleteVrfCounters                        */
/*                                                                        */
/*  Description     : Calls the low level routine to create or delete     */
/*                    the VRF statistics and the interface information    */
/*                                                                        */
/*  Input(s)        : CliHandle      - CliContext ID                */
/*                  pu1Alias       - Alias                         */
/*                    i1Status         - Status of counter                  */
/*                                                                        */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/

INT4
VcmCreateOrDeleteVrfCounters (tCliHandle CliHandle, UINT1 *pu1Alias,
                              UINT4 u4Status)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4VcNum = VCM_ZERO;
    INT4                i4RetVal = VCM_ZERO;
    i4RetVal = VcmIsVrfExist (pu1Alias, &u4VcNum);
    if (i4RetVal == VCM_FALSE)
    {
        VCM_CLI_PRINTF (CliHandle, "\r%% L3 Context does not exist\r\n");
        return CLI_FAILURE;
    }
    if (i4RetVal == VCM_TRUE)
    {
        if (SNMP_FAILURE ==
            nmhTestv2FsVcCounterStatus (&u4ErrorCode, (INT4) u4VcNum,
                                        (INT4) u4Status))
        {
            return CLI_FAILURE;
        }

        if (SNMP_FAILURE ==
            nmhSetFsVcCounterStatus ((INT4) u4VcNum, (INT4) u4Status))
        {
            return CLI_FAILURE;
        }

    }
    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : VcmVrfShowCounters                                  */
/*                                                                        */
/*  Description     : Calls the low level routine to show the VRF         */
/*                    statistics                                          */
/*                                                                        */
/*  Input(s)        : CliHandle      - CliContext ID                      */
/*                    pu1VrfName -      L3 Context name                   */
/*                                                                        */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/

INT4
VcmVrfShowCounters (tCliHandle CliHandle, UINT1 *pu1VrfName)
{

    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo Dummy;
    UINT4               u4VcNum = VCM_ZERO;
    UINT4               u4Frames = VCM_ZERO;
    UINT4               u4PrevContextId = VCM_ZERO;
    UINT1               u1Flag = VCM_FALSE;
    UINT1               u1ExitFlag = VCM_FALSE;
    UINT1               i1Status = VRF_STAT_DISABLE;
    if (pu1VrfName == NULL)
    {
        u4VcNum = VCM_ALL_VCS;
        u1Flag = VCM_TRUE;
    }
    else
    {
        if (VcmIsVrfExist (pu1VrfName, &u4VcNum) != VCM_TRUE)
        {
            VCM_CLI_PRINTF (CliHandle, "\r%% Vrf %s Not Present.\r\n",
                            pu1VrfName);
            return CLI_FAILURE;
        }
    }

    if (VCM_TRUE == u1Flag)
    {
        CliRegisterLock (CliHandle, VcmLock, VcmUnLock);
        VCM_LOCK ();
        pVcInfo = VcmGetFirstVc ();
        if (pVcInfo == NULL)
        {
            VCM_CLI_PRINTF (CliHandle, "\rNo Context Exist in the system \r\n");
            VCM_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
        }

        do
        {
            u4PrevContextId = pVcInfo->u4VcNum;

            VCM_CLI_PRINTF (CliHandle, "VRF Statistics     \n");
            VCM_CLI_PRINTF (CliHandle,
                            " -----------------------------------\n");
            VCM_CLI_PRINTF (CliHandle, " VRF Name                    : %s\r\n",
                            pVcInfo->au1Alias);
            u4Frames = 0;
            nmhGetFsVcCounterRxUnicast ((INT4) pVcInfo->u4VcNum, &u4Frames);
            VCM_CLI_PRINTF (CliHandle, " Unicast Packets Recieved    : %u\r\n",
                            u4Frames);
            u4Frames = 0;
            nmhGetFsVcCounterRxMulticast ((INT4) pVcInfo->u4VcNum, &u4Frames);
            VCM_CLI_PRINTF (CliHandle, " Multicast Packets Recieved  : %u\r\n",
                            u4Frames);
            u4Frames = 0;
            nmhGetFsVcCounterRxBroadcast ((INT4) pVcInfo->u4VcNum, &u4Frames);
            VCM_CLI_PRINTF (CliHandle, " Broadcast Packets Recieved  : %u\r\n",
                            u4Frames);
            if (VRF_STAT_ENABLE == pVcInfo->u1VrfCounter)
            {
                VCM_CLI_PRINTF (CliHandle,
                                " VRF Counter Status          : ENABLED\r\n");
            }
            else
            {
                VCM_CLI_PRINTF (CliHandle,
                                " VRF Counter Status          : DISABLED\r\n");
            }

            Dummy.u4VcNum = u4PrevContextId;

            pVcInfo = RBTreeGetNext (VCM_CONTEXT_TABLE, &Dummy, NULL);

            if (pVcInfo == NULL)
            {
                u1ExitFlag = VCM_TRUE;
            }

        }
        while (u1ExitFlag == VCM_FALSE);
        VCM_UNLOCK ();
        CliUnRegisterLock (CliHandle);
    }
    else
    {

        VCM_CLI_PRINTF (CliHandle, "VRF Statistics     \n");
        VCM_CLI_PRINTF (CliHandle, " -----------------------------------\n");
        VCM_CLI_PRINTF (CliHandle, " VRF Name                    : %s\r\n",
                        pu1VrfName);
        u4Frames = 0;
        nmhGetFsVcCounterRxUnicast ((INT4) u4VcNum, &u4Frames);
        VCM_CLI_PRINTF (CliHandle, " Unicast Packets Recieved    : %u\r\n",
                        u4Frames);
        u4Frames = 0;
        nmhGetFsVcCounterRxMulticast ((INT4) u4VcNum, &u4Frames);
        VCM_CLI_PRINTF (CliHandle, " Multicast Packets Recieved  : %u\r\n",
                        u4Frames);
        u4Frames = 0;
        nmhGetFsVcCounterRxBroadcast ((INT4) u4VcNum, &u4Frames);
        VCM_CLI_PRINTF (CliHandle, " Broadcast Packets Recieved  : %u\r\n",
                        u4Frames);

        if (VCM_FAILURE == VcmGetVrfCounterStatus (u4VcNum, &i1Status))
        {
            VCM_CLI_PRINTF (CliHandle, "\r%% Vrf %s Not Present.\r\n",
                            pu1VrfName);
            return VCM_SUCCESS;
        }
        if (VRF_STAT_ENABLE == i1Status)
        {
            VCM_CLI_PRINTF (CliHandle,
                            " VRF Counter Status          : ENABLED\r\n");
        }
        else
        {
            VCM_CLI_PRINTF (CliHandle,
                            " VRF Counter Status          : DISABLED\r\n");
        }

    }

    return VCM_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : VcmClearVrfCounters                                 */
/*                                                                        */
/*  Description     : Calls the low level routine to                      */
/*                    clear VRF counters                                  */
/*                                                                        */
/*  Input(s)        : CliHandle      - CliContext ID                      */
/*                    pu1L3ContextName -L3 Context name                   */
/*                                                                        */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/

INT4
VcmClearVrfCounters (tCliHandle CliHandle, UINT1 *pu1L3ContextName)
{

    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo Dummy;
    UINT4               u4ErrorCode = 0;
    UINT4               u4VcNum = VCM_ZERO;
    UINT1               u1Flag = VCM_FALSE;
    UINT1               u1ExitFlag = VCM_FALSE;

    if (pu1L3ContextName == NULL)
    {
        u4VcNum = VCM_ALL_VCS;
        u1Flag = VCM_TRUE;
    }
    else
    {
        if (VcmIsVrfExist (pu1L3ContextName, &u4VcNum) != VCM_TRUE)
        {
            VCM_CLI_PRINTF (CliHandle, "\r%% Vrf %s Not Present.\r\n",
                            pu1L3ContextName);
            return CLI_FAILURE;
        }
        if (SNMP_FAILURE ==
            nmhTestv2FsVcCounterStatus (&u4ErrorCode, (INT4) u4VcNum, VCM_TRUE))
        {
            return CLI_FAILURE;
        }

        if (SNMP_FAILURE == nmhSetFsVcClearCounter ((INT4) u4VcNum, VCM_TRUE))
        {
            return CLI_FAILURE;
        }
    }
    if (VCM_TRUE == u1Flag)
    {

        CliRegisterLock (CliHandle, VcmLock, VcmUnLock);
        VCM_LOCK ();
        pVcInfo = VcmGetFirstVc ();
        if (pVcInfo == NULL)
        {
            VCM_CLI_PRINTF (CliHandle, "\rNo Context Exist in the system \r\n");
            VCM_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
        }

        do
        {
            VCM_UNLOCK ();
            if (SNMP_FAILURE ==
                nmhTestv2FsVcCounterStatus (&u4ErrorCode,
                                            (INT4) pVcInfo->u4VcNum, VCM_TRUE))
            {
                return CLI_FAILURE;
            }

            if (SNMP_FAILURE ==
                nmhSetFsVcClearCounter ((INT4) pVcInfo->u4VcNum, VCM_TRUE))
            {
                return CLI_FAILURE;
            }
            VCM_LOCK ();
            Dummy.u4VcNum = pVcInfo->u4VcNum;

            pVcInfo = RBTreeGetNext (VCM_CONTEXT_TABLE, &Dummy, NULL);

            if (pVcInfo == NULL)
            {
                u1ExitFlag = VCM_TRUE;
            }

        }
        while (u1ExitFlag == VCM_FALSE);
        VCM_UNLOCK ();

    }

    return CLI_SUCCESS;
}
#endif

/*****************************************************************************/
/* Function Name      : VcmCliGetShowCmdOutputToFile                         */
/*                                                                           */
/* Description        : This function handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu1FileName - The output of the show cmd is          */
/*                      redirected to this file                              */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS/VCM_FAILURE                              */
/*****************************************************************************/
INT4
VcmCliGetShowCmdOutputToFile (UINT1 *pu1FileName)
{
    if (FileStat ((const CHR1 *) pu1FileName) == OSIX_SUCCESS)
    {
        if (0 != FileDelete (pu1FileName))
        {
            return VCM_FAILURE;
        }
    }
    if (CliGetShowCmdOutputToFile (pu1FileName, (UINT1 *) VCM_AUDIT_SHOW_CMD)
        == CLI_FAILURE)
    {
        return VCM_FAILURE;
    }
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmCliCalcSwAudCheckSum                              */
/*                                                                           */
/* Description        : This function handles the Calculation of checksum    */
/*                      for the data in the given input file.                */
/*                                                                           */
/* Input(s)           : pu1FileName - The checksum is calculated for the data*/
/*                      in this file                                         */
/*                                                                           */
/* Output(s)          : pu2ChkSum - The calculated checksum is stored here   */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS/VCM_FAILURE                              */
/*****************************************************************************/
INT4
VcmCliCalcSwAudCheckSum (UINT1 *pu1FileName, UINT2 *pu2ChkSum)
{
    UINT4               u4Sum = 0;
    UINT4               u4Last = 0;
    INT4                i4Fd;
    UINT2               u2CkSum = 0;
    INT2                i2ReadLen;
    INT1                ai1Buf[VCM_CLI_MAX_GROUPS_LINE_LEN + 1];

    i4Fd = FileOpen ((CONST UINT1 *) pu1FileName, VCM_CLI_RDONLY);
    if (i4Fd == -1)
    {
        return VCM_FAILURE;
    }
    MEMSET (ai1Buf, 0, VCM_CLI_MAX_GROUPS_LINE_LEN + 1);
    while (VcmCliReadLineFromFile (i4Fd, ai1Buf, VCM_CLI_MAX_GROUPS_LINE_LEN,
                                   &i2ReadLen) != VCM_CLI_EOF)

    {
        if (i2ReadLen > 0)
        {
            UtilCompCheckSum ((UINT1 *) ai1Buf, &u4Sum, &u4Last,
                              &u2CkSum, (UINT4) i2ReadLen, CKSUM_DATA);
            MEMSET (ai1Buf, '\0', VCM_CLI_MAX_GROUPS_LINE_LEN + 1);
        }
    }
    /*CheckSum for last line */
    if ((u4Sum != 0) || (i2ReadLen != 0))
    {
        UtilCompCheckSum ((UINT1 *) ai1Buf, &u4Sum, &u4Last,
                          &u2CkSum, (UINT4) i2ReadLen, CKSUM_LASTDATA);

    }
    *pu2ChkSum = u2CkSum;
    if (FileClose (i4Fd) < 0)
    {
        return VCM_FAILURE;
    }
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmCliReadLineFromFile                               */
/*                                                                           */
/* Description        : It is a utility to read a line from the given file   */
/*                      descriptor.                                          */
/*                                                                           */
/* Input(s)           : i4Fd - File Descriptor for the file                  */
/*                      i2MaxLen - Maximum length that can be read and store */
/*                                                                           */
/* Output(s)          : pi1Buf - Buffer to store the line read from the file */
/*                      pi2ReadLen - the total length of the data read       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS/VCM_FAILURE                              */
/*****************************************************************************/
INT1
VcmCliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen,
                        INT2 *pi2ReadLen)
{
    INT1                i1Char;

    *pi2ReadLen = 0;

    while (FileRead (i4Fd, (CHR1 *) & i1Char, 1) == 1)
    {
        pi1Buf[*pi2ReadLen] = i1Char;
        (*pi2ReadLen)++;
        if (i1Char == '\n')
        {
            return (VCM_CLI_NO_EOF);
        }
        if (*pi2ReadLen == i2MaxLen)
        {
            *pi2ReadLen = 0;
            break;
        }
    }
    pi1Buf[*pi2ReadLen] = '\0';
    return (VCM_CLI_EOF);
}

#endif
