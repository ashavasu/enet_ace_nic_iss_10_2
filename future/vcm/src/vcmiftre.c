/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2004-2005                         */
/*****************************************************************************/
/*    FILE  NAME            : vcmiftre.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : VCM module                                     */
/*    MODULE NAME           : VCM module                                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 16 Jun 2006                                    */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains utility functions related to*/
/*                            Interface Mapping Table of VCM module          */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    16 JUN 2006 / MITeam   Initial Create.                         */
/*---------------------------------------------------------------------------*/

#ifndef _VCMIFTRE_C
#define _VCMIFTRE_C

#include "vcminc.h"
#include "vcmtree.h"
#include "cli.h"
#include "vcmcli.h"
#include "vcmmacr.h"
#include "vcmglob.h"

/*****************************************************************************/
/* Function Name      : VcmIfEntryCompare                                    */
/*                                                                           */
/* Description        : Action routine that is passed as parameter to the    */
/*                      the tree create used for comparing the keys.         */
/*                                                                           */
/* Input(s)           : e1 and e2 are pointer to two nodes present in the    */
/*                      tree                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : 0  - if e1 and e2 keys are equal                     */
/*                      1  - if e1 key > e2 key                              */
/*                      -1 - if e1 key < e2 key                              */
/*****************************************************************************/
INT4
VcmIfEntryCompare (tVcmRBElem * e1, tVcmRBElem * e2)
{
    UINT4               u4IfIndex1 = ((tVcmIfMapEntry *) e1)->u4IfIndex;
    UINT4               u4IfIndex2 = ((tVcmIfMapEntry *) e2)->u4IfIndex;

    if (u4IfIndex1 < u4IfIndex2)
    {
        return -1;
    }
    else if (u4IfIndex1 > u4IfIndex2)
    {
        return 1;
    }
    return 0;
}

/*****************************************************************************/
/* Function Name      : VcmL2CxtVlanToIfCompare                              */
/*                                                                           */
/* Description        : Action routine that is passed as parameter to the    */
/*                      the tree create used for comparing the keys.         */
/*                                                                           */
/* Input(s)           : e1 and e2 are pointer to two nodes present in the    */
/*                      tree                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : 0  - if e1 and e2 keys are equal                     */
/*                      1  - if e1 key > e2 key                              */
/*                      -1 - if e1 key < e2 key                              */
/*****************************************************************************/
INT4
VcmL2CxtVlanToIfCompare (tVcmRBElem * e1, tVcmRBElem * e2)
{
    UINT4               u4L2CxtId1 = ((tVcmIfMapEntry *) e1)->u4L2ContextId;
    UINT4               u4L2CxtId2 = ((tVcmIfMapEntry *) e2)->u4L2ContextId;
    UINT2               u2VlanId1 = ((tVcmIfMapEntry *) e1)->u2VlanId;
    UINT2               u2VlanId2 = ((tVcmIfMapEntry *) e2)->u2VlanId;

    if (u4L2CxtId1 < u4L2CxtId2)
    {
        return -1;
    }
    else if (u4L2CxtId1 > u4L2CxtId2)
    {
        return 1;
    }
    else if (u2VlanId1 < u2VlanId2)
    {
        return -1;
    }
    else if (u2VlanId1 > u2VlanId2)
    {
        return 1;
    }
    return 0;
}

/*****************************************************************************/
/* Function Name      : VcmIfMapTableInit                                    */
/*                                                                           */
/* Description        : Create the RB TREE for storing the IfMap entries.    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS / FAILURE                                    */
/*****************************************************************************/
INT4
VcmIfMapTableInit (VOID)
{
    /* Create the TREE for storing the IfMap entries. */
    VCM_IF_MAP_TABLE = VCM_RB_TREE_CREATE_EMBED
        (FSAP_OFFSETOF (tVcmIfMapEntry, IfMapRbNode), VcmIfEntryCompare);
    if (NULL == VCM_IF_MAP_TABLE)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC, "VcmIfMap: IfMap tree create failed.\n");
        return FAILURE;
    }

    VCM_CXT_TO_IPIF_MAP_TABLE = VCM_RB_TREE_CREATE_EMBED
        (FSAP_OFFSETOF (tVcmIfMapEntry, L2CxtToIpIfMapRBNode),
         VcmL2CxtVlanToIfCompare);
    if (NULL == VCM_CXT_TO_IPIF_MAP_TABLE)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC, "VcmIfMap: Failed to create tree for"
                 "storing l2context+vlanId to IP interface mapping\n");
        return FAILURE;
    }

    return SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmIfMapTableDeInit                                  */
/*                                                                           */
/* Description        : Deletes the RB TREE created for IfMap entries.       */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
VcmIfMapTableDeInit ()
{
    /* Delete the TREE created for IfMap entries. */
    VCM_RB_TREE_DELETE (VCM_IF_MAP_TABLE);
    VCM_IF_MAP_TABLE = NULL;
    VCM_RB_TREE_DELETE (VCM_CXT_TO_IPIF_MAP_TABLE);
    VCM_CXT_TO_IPIF_MAP_TABLE = NULL;
}

/*****************************************************************************/
/* Function Name      : VcmAddIfMapEntry                                     */
/*                                                                           */
/* Description        : Adds an element IfMap Entry to RB tree               */
/*                                                                           */
/* Input(s)           : pNode - pointer to the new element to be inserted    */
/*                      into the tree                                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS / FAILURE                                    */
/*****************************************************************************/
INT4
VcmAddIfMapEntry (VOID *pNode)
{
    if (RB_FAILURE == VCM_RB_TREE_ADD (VCM_IF_MAP_TABLE, (tVcmRBElem *) pNode))
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC, "VcmIfMap: Add to IfMap table failed.\n");
        return FAILURE;
    }
    return SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmAddL2CxToIPIfMapEntry                             */
/*                                                                           */
/* Description        : Adds an element IfMap Entry to RB tree               */
/*                                                                           */
/* Input(s)           : pNode - pointer to the new element to be inserted    */
/*                      into the tree                                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS / FAILURE                                    */
/*****************************************************************************/
INT4
VcmAddL2CxToIPIfMapEntry (VOID *pNode)
{
    if (RB_FAILURE == VCM_RB_TREE_ADD (VCM_CXT_TO_IPIF_MAP_TABLE, pNode))
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC, "VcmIfMap: Add to L2context+vlanId"
                 " to interface map table failed.\n");
        return VCM_FAILURE;
    }
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmRemoveIfMapEntry                                  */
/*                                                                           */
/* Description        : Removes an element IfMap Entry from RB tree          */
/*                                                                           */
/* Input(s)           : pNode - pointer to the element to be removed         */
/*                      from the tree                                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS / FAILURE                                    */
/*****************************************************************************/
INT4
VcmRemoveIfMapEntry (VOID *pNode)
{
    if (RB_FAILURE ==
        VCM_RB_TREE_REMOVE (VCM_IF_MAP_TABLE, (tVcmRBElem *) pNode))
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC, "VcmIfMap: Remove IfMap Entry failed.\n");
        return FAILURE;
    }
    return SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmRemoveL2CxToIPIfMapEntry                          */
/*                                                                           */
/* Description        : Removes an element IfMap Entry from RB tree          */
/*                                                                           */
/* Input(s)           : pNode - pointer to the element to be removed         */
/*                      from the tree                                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS / FAILURE                                    */
/*****************************************************************************/
INT4
VcmRemoveL2CxToIPIfMapEntry (VOID *pNode)
{
    if (RB_FAILURE ==
        VCM_RB_TREE_REMOVE (VCM_CXT_TO_IPIF_MAP_TABLE, (tVcmRBElem *) pNode))
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC, "VcmIfMap: Remove IfMap Entry from"
                 "l2context+vlanId to interface map table failed.\n");
        return VCM_FAILURE;
    }
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmFindIfMapEntry                                    */
/*                                                                           */
/* Description        : Returns a pointer to a IfMap Entry for a given key   */
/*                                                                           */
/* Input(s)           : pNode - pointer to the element to be found           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : pointer to the node if key found. NULL if not found  */
/*****************************************************************************/
VOID               *
VcmFindIfMapEntry (VOID *pNode)
{
    tRBElem            *pEle = NULL;
    /* pNode is a dummy node in which ifIndex is filled. */
    if (VCM_IS_INITIALISED () != VCM_TRUE)
    {
        return NULL;
    }
    pEle = VCM_RB_TREE_GET (VCM_IF_MAP_TABLE, pNode);
    return pEle;
}

/*****************************************************************************/
/* Function Name      : VcmGetFirstIfMap                                     */
/*                                                                           */
/* Description        : Returns the first IfMap Entry from the tree          */
/*                                                                           */
/* Input(s)           : pNode - pointer to the element to be found           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : pointer to the first element or NULL if tree is      */
/*                      empty                                                */
/*****************************************************************************/
tVcmIfMapEntry     *
VcmGetFirstIfMap ()
{
    tVcmIfMapEntry     *pIfMap = NULL;

    if (VCM_IS_INITIALISED () != VCM_TRUE)
    {
        return NULL;
    }
    pIfMap = (tVcmIfMapEntry *) (VCM_RB_TREE_GETFIRST (VCM_IF_MAP_TABLE));
    return pIfMap;
}

/*****************************************************************************/
/* Function Name      : VcmGetNextIfMap                                      */
/*                                                                           */
/* Description        : Returns the next IfMap Entry from the tree           */
/*                                                                           */
/* Input(s)           : pNode - pointer to the element next to be found      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : pointer to the next element or NULL if not found     */
/*****************************************************************************/
tVcmIfMapEntry     *
VcmGetNextIfMap (VOID *Temp)
{
    tVcmIfMapEntry     *pIfMap = NULL;

    if (VCM_IS_INITIALISED () != VCM_TRUE)
    {
        return NULL;
    }
    pIfMap =
        (tVcmIfMapEntry *) (VCM_RB_TREE_GETNEXT (VCM_IF_MAP_TABLE, Temp, NULL));
    return pIfMap;
}

/*****************************************************************************/
/* Function Name      : VcmGetFirstCxtToIpIfMapEntry                         */
/*                                                                           */
/* Description        : Returns the first L2Cxt+VlanId to IP Iface mapping   */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : pointer to the first element or NULL if tree is      */
/*                      empty                                                */
/*****************************************************************************/
tVcmIfMapEntry     *
VcmGetFirstCxtToIpIfMapEntry ()
{
    tVcmIfMapEntry     *pIfMap = NULL;

    if (VCM_IS_INITIALISED () != VCM_TRUE)
    {
        return NULL;
    }
    pIfMap =
        (tVcmIfMapEntry *) (VCM_RB_TREE_GETFIRST (VCM_CXT_TO_IPIF_MAP_TABLE));
    return pIfMap;
}

/*****************************************************************************/
/* Function Name      : VcmGetNextCxtToIpIfMapEntry                          */
/*                                                                           */
/* Description        : Returns the next IfMap Entry from the tree           */
/*                                                                           */
/* Input(s)           : pNode - pointer to the element next to be found      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : pointer to the next element or NULL if not found     */
/*****************************************************************************/
tVcmIfMapEntry     *
VcmGetNextCxtToIpIfMapEntry (VOID *Temp)
{
    tVcmIfMapEntry     *pIfMap = NULL;

    if (VCM_IS_INITIALISED () != VCM_TRUE)
    {
        return NULL;
    }
    pIfMap =
        (tVcmIfMapEntry *) (VCM_RB_TREE_GETNEXT (VCM_CXT_TO_IPIF_MAP_TABLE,
                                                 Temp, NULL));
    return pIfMap;
}

/*****************************************************************************/
/* Function Name      : VcmFindL2CxtToIPIfMapEntry                           */
/*                                                                           */
/* Description        : Returns a pointer to a IPIfMap Entry for a given key */
/*                                                                           */
/* Input(s)           : pNode - pointer to the element to be found           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : pointer to the node if key found. NULL if not found  */
/*****************************************************************************/
VOID               *
VcmFindL2CxtToIPIfMapEntry (VOID *pNode)
{
    tRBElem            *pEle = NULL;
    /* pNode is a dummy node in which ifIndex is filled. */
    if (VCM_IS_INITIALISED () != VCM_TRUE)
    {
        return NULL;
    }
    pEle = VCM_RB_TREE_GET (VCM_CXT_TO_IPIF_MAP_TABLE, pNode);
    return pEle;
}

#endif
