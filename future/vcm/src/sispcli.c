/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* $Id: sispcli.c,v 1.7 2016/03/19 12:57:31 siva Exp $                      */
/* Licensee Aricent Inc., 2009-2010                                          */
/*****************************************************************************/
/*    FILE  NAME            : sispcli.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc   .                                */
/*    SUBSYSTEM NAME        : VCM module                                     */
/*    MODULE NAME           : VCM module                                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 01 Mar 2009                                    */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains the Command Line Interface  */
/*                            functions of SISP feature.                     */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    30 JAN 2009 / MITeam   Initial Create.                         */
/*---------------------------------------------------------------------------*/
#ifndef __SISPCLI_C__
#define __SISPCLI_C__

#include "vcminc.h"
#include "sispcli.h"
#include "fssispcli.h"

INT4
cli_process_sisp_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4               u4IfIndex = VCM_INVALID_IFINDEX;
    UINT4               u4ContextId = VCM_INVALID_CONTEXT_ID;
    UINT4               u4ErrCode = VCM_INIT_VAL;
    UINT1              *args[SISP_MAX_ARGS];
    UINT1              *pu1Inst = NULL;
    INT1                argno = VCM_INIT_VAL;
    INT4                i4RetStatus = VCM_INIT_VAL;

    va_start (ap, u4Command);

    /* third arguement is always interface name/index */
    pu1Inst = (UINT1 *) va_arg (ap, UINT4 *);

    if (pu1Inst != VCM_INIT_VAL)
    {
        u4IfIndex = CLI_PTR_TO_U4 (pu1Inst);
    }
    else
    {
        u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
    }

    /* Walk through the rest of the arguements and store in args array.
     * Store 6 arguements at the max. This is because vcm commands do not
     * take more than sixteen inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give
     * second input. In that case first arg will be null and second arg only
     * has value */

    while (SISP_ALWAYS)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == SISP_MAX_ARGS)
        {
            break;
        }
    }

    va_end (ap);

    CliRegisterLock (CliHandle, VcmConfLock, VcmConfUnLock);

    VCM_CONF_LOCK ();

    switch (u4Command)
    {
        case CLI_SISP_SHUT:

            i4RetStatus = VcmSispCliSetSystemControl (CliHandle, SISP_SHUTDOWN);

            break;

        case CLI_SISP_NOSHUT:

            i4RetStatus = VcmSispCliSetSystemControl (CliHandle, SISP_START);

            break;

        case CLI_SISP_PORT_ENABLE:

            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();

            i4RetStatus = VcmSispCliSetPortControl (CliHandle, u4IfIndex,
                                                    SISP_ENABLE);

            break;

        case CLI_SISP_PORT_DISABLE:

            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();

            i4RetStatus = VcmSispCliSetPortControl (CliHandle, u4IfIndex,
                                                    SISP_DISABLE);

            break;

        case CLI_SISP_MAP_LOGICALPORT:

            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();

            if (args[1] != NULL)
            {
                if (VcmIsSwitchExist (args[1], &u4ContextId) != VCM_TRUE)
                {
                    CliPrintf (CliHandle,
                               "Switch %s Does not exist.\r\n", args[1]);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
                i4RetStatus = VcmSispCliAddLogicalPortMap (CliHandle,
                                                           u4IfIndex,
                                                           u4ContextId,
                                                           (CLI_PTR_TO_U4
                                                            (args[0])));
            }

            break;

        case CLI_SISP_UNMAP_LOGICALPORT:

            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();

            if (args[1] != NULL)
            {
                if (VcmIsSwitchExist (args[1], &u4ContextId) != VCM_TRUE)
                {
                    CliPrintf (CliHandle,
                               "Switch %s Does not exist.\r\n", args[1]);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }
            i4RetStatus = VcmSispCliDelLogicalPortMap (CliHandle, u4IfIndex,
                                                       u4ContextId,
                                                       (CLI_PTR_TO_U4
                                                        (args[0])));
            break;

        case CLI_SISP_SHOW:

            i4RetStatus = VcmSispShow (CliHandle, (CLI_PTR_TO_U4 ((args[0]))));

            break;

        case CLI_SISP_SHOW_VLAN_INFO:

            i4RetStatus = VcmSispPortVlanMapShow (CliHandle,
                                                  (CLI_PTR_TO_U4 ((args[0]))));

            break;

        default:
            break;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_VCM_SISP_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%% %s", VcmSispCliErrString[u4ErrCode]);
        }

        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

    VCM_CONF_UNLOCK ();

    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispCliSetSystemControl                           */
/*                                                                           */
/* Description        : This function used to configure the SISP feature     */
/*                      as global enable or disable                          */
/*                                                                           */
/* Input(s)           : u1Status - Enable/disable                            */
/*                      CliHandle - Contains error messages                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS / CLI_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispCliSetSystemControl (tCliHandle CliHandle, UINT1 u1Status)
{
    UINT4               u4ErrorCode = VCM_INIT_VAL;

    if (nmhTestv2FsSispSystemControl (&u4ErrorCode, (INT4) u1Status)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsSispSystemControl (u1Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispCliSetPortControl                             */
/*                                                                           */
/* Description        : This function used to configure the SISP feature     */
/*                      as enable or disable for the particular port.        */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                      u1Status - Enable/disable                            */
/*                      CliHandle - Contains error messages                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS / CLI_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispCliSetPortControl (tCliHandle CliHandle, UINT4 u4IfIndex, UINT1 u1Status)
{
    UINT4               u4ErrorCode = VCM_INIT_VAL;

    if (nmhTestv2FsSispPortCtrlStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                       (INT4) u1Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsSispPortCtrlStatus ((INT4) u4IfIndex, (INT4) u1Status)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispCliAddLogicalPortMap                          */
/*                                                                           */
/* Description        : This function used to map the physical or port       */
/*                      channel port to the secondary context using logical  */
/*                      port.                                                */
/*                                                                           */
/* Input(s)           : u4PhyIfIndex - Interface Index                       */
/*                      u4ContextId - ContextIdentifier                      */
/*                      u4LogicalIfIdx - Logical port index                  */
/*                      CliHandle - Contains error messages                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS / CLI_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispCliAddLogicalPortMap (tCliHandle CliHandle, UINT4 u4PhyIfIndex,
                             UINT4 u4ContextId, UINT4 u4LogicalIfIdx)
{
    tVcmIfMapEntry      VcmIfMapEntry;
    UINT4               u4ErrorCode = VCM_INIT_VAL;

    MEMSET (&VcmIfMapEntry, VCM_INIT_VAL, sizeof (tVcmIfMapEntry));

    if (VcmSispPortMapEntryGet (u4PhyIfIndex, u4ContextId, &VcmIfMapEntry)
        == VCM_FAILURE)
    {
        if (nmhTestv2FsSispPortMapRowStatus (&u4ErrorCode, (INT4) u4PhyIfIndex,
                                             (INT4) u4ContextId,
                                             (INT4) CREATE_AND_WAIT)
            != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsSispPortMapRowStatus ((INT4) u4PhyIfIndex,
                                          (INT4) u4ContextId,
                                          (INT4) CREATE_AND_WAIT) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhTestv2FsSispPortMapSharedPort (&u4ErrorCode, (INT4) u4PhyIfIndex,
                                              (INT4) u4ContextId,
                                              (INT4) u4LogicalIfIdx)
            != SNMP_SUCCESS)
        {
            if (nmhSetFsSispPortMapRowStatus ((INT4) u4PhyIfIndex,
                                              (INT4) u4ContextId,
                                              (INT4) DESTROY) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
            return CLI_FAILURE;
        }
        if (nmhSetFsSispPortMapSharedPort ((INT4) u4PhyIfIndex,
                                           (INT4) u4ContextId,
                                           (INT4) u4LogicalIfIdx)
            != SNMP_SUCCESS)
        {
            if (nmhSetFsSispPortMapRowStatus ((INT4) u4PhyIfIndex,
                                              (INT4) u4ContextId,
                                              (INT4) DESTROY) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhTestv2FsSispPortMapRowStatus (&u4ErrorCode,
                                             (INT4) u4PhyIfIndex,
                                             (INT4) u4ContextId,
                                             (INT4) ACTIVE) != SNMP_SUCCESS)
        {
            if (nmhSetFsSispPortMapRowStatus ((INT4) u4PhyIfIndex,
                                              (INT4) u4ContextId,
                                              (INT4) DESTROY) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
            return CLI_FAILURE;
        }
        if (nmhSetFsSispPortMapRowStatus ((INT4) u4PhyIfIndex,
                                          (INT4) u4ContextId,
                                          (INT4) ACTIVE) != SNMP_SUCCESS)
        {
            if (nmhSetFsSispPortMapRowStatus ((INT4) u4PhyIfIndex,
                                              (INT4) u4ContextId,
                                              (INT4) DESTROY) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (VcmIfMapEntry.u4IfIndex != u4LogicalIfIdx)
        {
            CLI_SET_ERR (SISP_PORT_MAP_EXISTS);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispCliDelLogicalPortMap                          */
/*                                                                           */
/* Description        : This function used to remove the physical or port    */
/*                      channel port frome the secondary context associated  */
/*                      using logical port                                   */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                      u4ContextId - ContextIdentifier                      */
/*                      u4LogicalIfIdx - Logical port index                  */
/*                      CliHandle - Contains error messages                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS / CLI_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispCliDelLogicalPortMap (tCliHandle CliHandle, UINT4 u4IfIndex,
                             UINT4 u4ContextId, UINT4 u4LogicalIfIdx)
{
    tVcmIfMapEntry      VcmIfMapEntry;
    UINT4               u4ErrorCode = VCM_INIT_VAL;
    UINT4               u4TestIfIndex = VCM_INVALID_IFINDEX;
    INT4                i4NextIfIndex = (INT4) VCM_INVALID_IFINDEX;
    INT4                i4NextContextId = (INT4) VCM_INVALID_CONTEXT_ID;
    INT4                i4RetRowStatus;

    MEMSET (&VcmIfMapEntry, VCM_INIT_VAL, sizeof (tVcmIfMapEntry));

    /* Delete All the logical ports of a physical port */
    if ((u4ContextId == VCM_INVALID_CONTEXT_ID) &&
        (u4LogicalIfIdx == VCM_INIT_VAL))
    {
        u4ContextId = 0;

        /* Verify whether any SISP port is existing for the given port in
         * Default context (Context 0) */
        if (nmhGetFsSispPortMapRowStatus ((INT4) u4IfIndex, (INT4) u4ContextId,
                                          &i4RetRowStatus) == SNMP_SUCCESS)
        {
            if (nmhTestv2FsSispPortMapRowStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                                 (INT4) u4ContextId,
                                                 (INT4) DESTROY) !=
                SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
            if (nmhSetFsSispPortMapRowStatus ((INT4) u4IfIndex,
                                              (INT4) u4ContextId,
                                              (INT4) DESTROY) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }

        while (nmhGetNextIndexFsSispPortMapTable ((INT4) u4IfIndex,
                                                  &i4NextIfIndex,
                                                  (INT4) u4ContextId,
                                                  &i4NextContextId) !=
               SNMP_FAILURE)
        {
            if (u4IfIndex != (UINT4) i4NextIfIndex)
            {
                break;
            }
            if (nmhTestv2FsSispPortMapRowStatus (&u4ErrorCode, i4NextIfIndex,
                                                 i4NextContextId,
                                                 (INT4) DESTROY) !=
                SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
            if (nmhSetFsSispPortMapRowStatus (i4NextIfIndex, i4NextContextId,
                                              (INT4) DESTROY) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            u4ContextId = (UINT4) i4NextContextId;
        }
    }
    else
    {
        if (u4LogicalIfIdx != VCM_INIT_VAL)
        {
            /* Delete based on the Logical port ID */
            if (VcmSispGetPhysicalPort (u4LogicalIfIdx, &u4TestIfIndex)
                == VCM_SUCCESS)
            {
                /*Verify this logical port belongs to this phy port */
                if (u4TestIfIndex != u4IfIndex)
                {
                    return CLI_FAILURE;
                }
                VcmGetIfMapVcId (u4LogicalIfIdx, &u4ContextId);
            }
            else
            {
                return CLI_FAILURE;
            }
        }

        if (nmhValidateIndexInstanceFsSispPortMapTable ((INT4) u4IfIndex,
                                                        (INT4) u4ContextId)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhTestv2FsSispPortMapRowStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                             (INT4) u4ContextId,
                                             (INT4) DESTROY) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsSispPortMapRowStatus ((INT4) u4IfIndex, (INT4) u4ContextId,
                                          (INT4) DESTROY) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispShow                                          */
/*                                                                           */
/* Description        : This function used to display the Switch Instance    */
/*                      Shared Port feature's global status and also port    */
/*                      specific status.                                     */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                      CliHandle - CliHandle                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS / CLI_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispShow (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    tVcmIfMapEntry     *pIfMapEntry = NULL;
    tVcmIfMapEntry      DummyIfMap;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1Res = OSIX_FALSE;
    UINT1               u1ExitFlag = VCM_FALSE;

    MEMSET (au1NameStr, VCM_INIT_VAL, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (&DummyIfMap, VCM_INIT_VAL, sizeof (tVcmIfMapEntry));

    CliPrintf (CliHandle, "\r\n SISP feature is globally ");

    VCM_LOCK ();

    if (VCM_GET_SISP_STATUS == SISP_START)
    {
        CliPrintf (CliHandle, "started\r\n\n");
    }
    else
    {
        CliPrintf (CliHandle, "shutdown\r\n\n");
    }

    if (u4IfIndex == VCM_INVALID_IFINDEX)
    {
        pIfMapEntry = VcmGetFirstIfMap ();

        if (pIfMapEntry == NULL)
        {
            CliPrintf (CliHandle, "\r No Mapping Present in the system \r\n");
            VCM_UNLOCK ();
            return CLI_SUCCESS;
        }

        do
        {
            if (SISP_IS_LOGICAL_PORT (pIfMapEntry->u4IfIndex) == VCM_FALSE)
            {
                VCM_UNLOCK ();
                if (VcmCfaCliGetIfName (pIfMapEntry->u4IfIndex,
                                        (INT1 *) au1NameStr) == CLI_SUCCESS)
                {
                    CliPrintf (CliHandle, " %-8s   ", au1NameStr);
                }
                VCM_LOCK ();

                L2IwfGetSispPortCtrlStatus (pIfMapEntry->u4IfIndex, &u1Res);

                if (u1Res == SISP_ENABLE)
                {
                    CliPrintf (CliHandle, "Enabled\r\n");
                }
                else
                {
                    CliPrintf (CliHandle, "Disabled\r\n");
                }
            }

            DummyIfMap.u4IfIndex = pIfMapEntry->u4IfIndex;

            pIfMapEntry = VcmGetNextIfMap (&DummyIfMap);

            if (pIfMapEntry == NULL)
            {
                u1ExitFlag = VCM_TRUE;
            }
        }
        while (u1ExitFlag == VCM_FALSE);
    }
    else
    {
        /* Display only for Physical & Port channel ports */
        if ((u4IfIndex > VCM_INVALID_VAL) &&
            (u4IfIndex <= BRG_MAX_PHY_PLUS_LOG_PORTS))
        {
            VCM_UNLOCK ();
            if (VcmCfaCliGetIfName (u4IfIndex, (INT1 *) au1NameStr)
                == CLI_SUCCESS)
            {
                CliPrintf (CliHandle, "%-8s   ", au1NameStr);
            }
            VCM_LOCK ();

            L2IwfGetSispPortCtrlStatus (u4IfIndex, &u1Res);

            if (u1Res == SISP_ENABLE)
            {
                CliPrintf (CliHandle, "Enabled\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "Disabled\r\n");
            }
        }
    }
    CliPrintf (CliHandle, "\r\n");

    VCM_UNLOCK ();
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispPortVlanMapShow                               */
/*                                                                           */
/* Description        : This function used to display the Port Vlan Context  */
/*                      mapping information of the SISP feature.             */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                      CliHandle - CliHandle                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS / CLI_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispPortVlanMapShow (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];
    UINT1               u1CxtDisplayFlag = FALSE;
    UINT1               u1DigitCount = SISP_SINGLE_DIGIT;
    UINT2               u2MaxNumVlansInRow = VCM_INIT_VAL;
    UINT2               u2VlanCount = VCM_INIT_VAL;
    INT1                ai1DisplayString[20];
    INT1               *pi1DisplayString = NULL;
    INT4                i4NextIfIndex = VCM_INIT_VAL;
    INT4                i4CurrIfIndex = (INT4) u4IfIndex;
    INT4                i4CurrContextId = (INT4) VCM_INVALID_VC;
    INT4                i4NextContextId = (INT4) VCM_INVALID_VC;
    INT4                i4CurrVlanId = VCM_INIT_VAL;
    INT4                i4NextVlanId = VCM_INIT_VAL;
    UINT4               u4PagingStatus = CLI_SUCCESS;

    pi1DisplayString = &ai1DisplayString[0];

    CLI_MEMSET (ai1DisplayString, VCM_INIT_VAL, sizeof (ai1DisplayString));

    CliPrintf (CliHandle, "\r\n     Port Vlan Context Mapping Information ");
    CliPrintf (CliHandle,
               "\r\n----------------------------------------------- \r\n");

    CliPrintf (CliHandle, " Port               Vlan ID          Vc-Name\r\n");

    CliPrintf (CliHandle, "---------      -----------------   ------------- ");

    if (u4IfIndex != VCM_INIT_VAL)
    {
        if (VcmCfaCliGetIfName (u4IfIndex, (INT1 *) au1NameStr) == CLI_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n\r\n %-14s", au1NameStr);
        }
    }

    while ((u4PagingStatus == CLI_SUCCESS) &&
           (nmhGetNextIndexFsSispCxtClassificationTable (i4CurrIfIndex,
                                                         &i4NextIfIndex,
                                                         i4CurrVlanId,
                                                         &i4NextVlanId)
            != SNMP_FAILURE))
    {
        if ((u4IfIndex != VCM_INIT_VAL) && (i4NextIfIndex != (INT4) u4IfIndex))
        {
            break;
        }

        nmhGetFsSispCxtClassificationCxtId (i4NextIfIndex, i4NextVlanId,
                                            &i4NextContextId);

        if (i4CurrIfIndex != i4NextIfIndex)
        {

            if (ai1DisplayString[0] != VCM_INIT_VAL)
            {
                u4PagingStatus =
                    (UINT4) CliPrintf (CliHandle, "%-20s", ai1DisplayString);
            }
            pi1DisplayString = &ai1DisplayString[0];

            if ((i4CurrIfIndex != VCM_INIT_VAL) && (u1CxtDisplayFlag == FALSE))
            {
                VcmGetAliasName ((UINT4) i4CurrContextId, au1ContextName);
                u4PagingStatus =
                    (UINT4) CliPrintf (CliHandle, "%-34s", au1ContextName);
                u1CxtDisplayFlag = TRUE;
            }

            CLI_MEMSET
                (ai1DisplayString, VCM_INIT_VAL, sizeof (ai1DisplayString));

            if (VcmCfaCliGetIfName ((UINT4) i4NextIfIndex,
                                    (INT1 *) au1NameStr) == CLI_SUCCESS)
            {
                u4PagingStatus =
                    (UINT4) CliPrintf (CliHandle, "\r\n\r\n %-14s", au1NameStr);
            }

            u1CxtDisplayFlag = FALSE;
            u2VlanCount = VCM_INIT_VAL;
        }

        if ((i4CurrIfIndex == i4NextIfIndex) &&
            (i4CurrContextId != i4NextContextId))
        {
            u1CxtDisplayFlag = FALSE;
        }

        if ((u1CxtDisplayFlag == FALSE) && (i4CurrIfIndex == i4NextIfIndex) &&
            (i4CurrContextId != (INT4) VCM_INVALID_VC)
            && (i4CurrContextId != i4NextContextId))
        {
            if (ai1DisplayString[0] != VCM_INIT_VAL)
            {
                u4PagingStatus =
                    (UINT4) CliPrintf (CliHandle, "%-20s", ai1DisplayString);
            }

            VcmGetAliasName ((UINT4) i4CurrContextId, au1ContextName);
            u4PagingStatus =
                (UINT4) CliPrintf (CliHandle, "%-34s\r\n", au1ContextName);
            u4PagingStatus = (UINT4) CliPrintf (CliHandle, "%15s", " ");

            pi1DisplayString = &ai1DisplayString[0];

            CLI_MEMSET
                (ai1DisplayString, VCM_INIT_VAL, sizeof (ai1DisplayString));

            u2VlanCount = VCM_INIT_VAL;
        }

        if (i4NextVlanId < SISP_VLAN_ID_10)
        {
            u2MaxNumVlansInRow = SISP_MAX_CHAR_10VLAN;
            u1DigitCount = SISP_SINGLE_DIGIT;
        }
        else if (i4NextVlanId < SISP_VLAN_ID_100)
        {
            u2MaxNumVlansInRow = SISP_MAX_CHAR_100VLAN;
            u1DigitCount = SISP_DOUBLE_DIGIT;
        }
        else if (i4NextVlanId < SISP_VLAN_ID_1000)
        {
            u2MaxNumVlansInRow = SISP_MAX_CHAR_1000VLAN;
            u1DigitCount = SISP_TRIPLE_DIGIT;
        }
        else
        {
            u2MaxNumVlansInRow = SISP_MAX_CHAR_DEFAULT;
            u1DigitCount = SISP_QUADRUPLE_DIGIT;
        }

        if ((i4NextVlanId > i4CurrVlanId) || (i4CurrIfIndex != i4NextIfIndex))
        {
            if ((u2VlanCount != VCM_INIT_VAL) &&
                (u2VlanCount < u2MaxNumVlansInRow))
            {
                SPRINTF ((CHR1 *) pi1DisplayString++, "%s", ",");
            }
        }

        u2VlanCount++;

        /*
         * If there is change in u2MaxNumVlansInRow due to DigitCount Change,
         * the previous comma would not have displayed, then print
         * comma explicitly.
         */
        if (u2VlanCount > u2MaxNumVlansInRow)
        {
            SPRINTF ((CHR1 *) pi1DisplayString++, "%s", ",");
        }

        SPRINTF ((CHR1 *) pi1DisplayString, "%d", i4NextVlanId);
        pi1DisplayString += u1DigitCount;

        if (u2VlanCount >= u2MaxNumVlansInRow)

        {
            u4PagingStatus =
                (UINT4) CliPrintf (CliHandle, "%-20s", ai1DisplayString);

            if (u1CxtDisplayFlag == FALSE)
            {
                VcmGetAliasName ((UINT4) i4CurrContextId, au1ContextName);
                u4PagingStatus =
                    (UINT4) CliPrintf (CliHandle, "%-34s", au1ContextName);
                u1CxtDisplayFlag = TRUE;
            }

            u4PagingStatus = (UINT4) CliPrintf (CliHandle, "\r\n%15s", " ");

            pi1DisplayString = &ai1DisplayString[0];

            CLI_MEMSET (ai1DisplayString, 0, sizeof (ai1DisplayString));

            u2VlanCount = VCM_INIT_VAL;
        }

        i4CurrIfIndex = i4NextIfIndex;
        i4CurrVlanId = i4NextVlanId;
        i4CurrContextId = i4NextContextId;
    }

    if (ai1DisplayString[0] != VCM_INIT_VAL)
    {
        CliPrintf (CliHandle, "%-20s", ai1DisplayString);
    }
    if ((i4CurrContextId != (INT4) VCM_INVALID_VC) &&
        (u1CxtDisplayFlag == FALSE))
    {
        VcmGetAliasName ((UINT4) i4CurrContextId, au1ContextName);
        CliPrintf (CliHandle, "%-34s\r\n", au1ContextName);
    }
    else
    {
        CliPrintf (CliHandle, "\r\n");
    }

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispShowRunningConfig                             */
/*                                                                           */
/* Description        : This function used to display the Show Running       */
/*                      config information of the SISP feature.              */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                      CliHandle - CliHandle                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS / CLI_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispShowRunningConfig (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];
    INT1               *piIfName = NULL;
    INT4                i4Status = SISP_SHUTDOWN;
    UINT4               u4Port = 1;
    INT4                i4NextIfIndex = (INT4) VCM_INVALID_IFINDEX;
    INT4                i4PrevIfIndex = (INT4) VCM_INIT_VAL;
    INT4                i4NextCtx = (INT4) VCM_INVALID_VC;
    INT4                i4LogicalIfIndex = (INT4) VCM_INVALID_IFINDEX;

    MEMSET (au1IfName, VCM_INIT_VAL, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1ContextName, VCM_INIT_VAL, VCM_ALIAS_MAX_LEN);
    piIfName = (INT1 *) &au1IfName[0];

    CliRegisterLock (CliHandle, VcmConfLock, VcmConfUnLock);

    nmhGetFsSispSystemControl (&i4Status);

    if (i4Status == SISP_START)
    {
        if (u4IfIndex == VCM_INIT_VAL)
        {
            CliPrintf (CliHandle,
                       "no shutdown switch-instance-shared-port\r\n");
        }
    }
    else
    {
        CliUnRegisterLock (CliHandle);
        return CLI_SUCCESS;
    }

    if (u4IfIndex == VCM_INIT_VAL)
    {
        for (; u4Port <= BRG_MAX_PHY_PLUS_LOG_PORTS; u4Port++)
        {
            nmhGetFsSispPortCtrlStatus ((INT4) u4Port, &i4Status);

            if (i4Status != SISP_ENABLE)
            {
                continue;
            }

            if (VcmCfaCliConfGetIfName (u4Port, piIfName) == CLI_SUCCESS)
            {
                CliPrintf (CliHandle, "interface %s\r\n", piIfName);
                CliPrintf (CliHandle, "switch-instance-shared-port enable\r\n");
                CliPrintf (CliHandle, "!\r\n");
            }
        }
    }
    else
    {
        /* SISP Show running config only for Physical & Port channel ports */
        if (u4IfIndex >= BRG_MAX_PHY_PLUS_LOG_PORTS)
        {
            CliUnRegisterLock (CliHandle);
            return CLI_SUCCESS;
        }

        nmhGetFsSispPortCtrlStatus ((INT4) u4IfIndex, &i4Status);

        if (i4Status == SISP_ENABLE)
        {
            if (VcmCfaCliConfGetIfName (u4IfIndex, piIfName) == CLI_SUCCESS)
            {
                CliPrintf (CliHandle, "interface %s\r\n", piIfName);
                CliPrintf (CliHandle, "switch-instance-shared-port enable\r\n");
                CliPrintf (CliHandle, "!\r\n");
            }
        }
        else
        {
            CliUnRegisterLock (CliHandle);
            return CLI_SUCCESS;
        }
    }

    if (u4IfIndex == VCM_INIT_VAL)
    {
        if (nmhGetFirstIndexFsSispPortMapTable (&i4NextIfIndex, &i4NextCtx)
            != SNMP_SUCCESS)
        {
            CliUnRegisterLock (CliHandle);
            return CLI_SUCCESS;
        }
    }
    else
    {
        /* To start from the first entry for the particular port */
        if (nmhGetNextIndexFsSispPortMapTable ((INT4) (u4IfIndex - 1),
                                               &i4NextIfIndex,
                                               (INT4) VCM_INVALID_VC,
                                               &i4NextCtx) == SNMP_FAILURE)
        {
            CliUnRegisterLock (CliHandle);
            return CLI_SUCCESS;
        }
        else
        {
            if ((UINT4) i4NextIfIndex != u4IfIndex)
            {
                CliUnRegisterLock (CliHandle);
                return CLI_SUCCESS;
            }
        }
    }

    for (;;)
    {
        nmhGetFsSispPortMapRowStatus (i4NextIfIndex, i4NextCtx, &i4Status);

        if (i4Status == ACTIVE)
        {
            nmhGetFsSispPortMapSharedPort (i4NextIfIndex, i4NextCtx,
                                           &i4LogicalIfIndex);

            if (i4PrevIfIndex != i4NextIfIndex)
            {
                VcmCfaCliConfGetIfName ((UINT4) i4NextIfIndex, piIfName);
                CliPrintf (CliHandle, "interface %s\r\n", piIfName);
            }

            VcmCfaCliConfGetIfName ((UINT4) i4LogicalIfIndex, piIfName);
            VcmGetAliasName ((UINT4) i4NextCtx, au1ContextName);

            CliPrintf (CliHandle, "map %s switch %s \r\n", piIfName,
                       au1ContextName);
        }

        i4PrevIfIndex = i4NextIfIndex;

        if (nmhGetNextIndexFsSispPortMapTable (i4PrevIfIndex, &i4NextIfIndex,
                                               i4NextCtx, &i4NextCtx)
            == SNMP_FAILURE)
        {
            break;
        }
        if ((u4IfIndex != VCM_INIT_VAL) && (i4PrevIfIndex != i4NextIfIndex))
        {
            CliPrintf (CliHandle, "!\r\n");
            break;
        }
    };

    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}
#endif
