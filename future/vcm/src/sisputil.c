/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* $Id: sisputil.c,v 1.4 2012/07/18 09:57:52 siva Exp $                          */
/* Licensee Aricent Inc., 2009-2010                                          */
/*****************************************************************************/
/*    FILE  NAME            : sisputil.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc   .                                */
/*    SUBSYSTEM NAME        : VCM module                                     */
/*    MODULE NAME           : VCM module                                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 01 Mar 2009                                    */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains the utility functions for   */
/*                            SISP feature.                                  */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    30 JAN 2009 / MITeam   Initial Create.                         */
/*---------------------------------------------------------------------------*/
#ifndef _SISPUTIL_C
#define _SISPUTIL_C

#include "vcminc.h"

extern UINT4        fssisp[9];
extern UINT4        FsSispSystemControl[11];
/*****************************************************************************/
/*    Function Name       : VcmSispPortMapTableCmp                           */
/*    Description         : User compare function for VCM's SISP Port map    */
/*                          table                                            */
/*    Input(s)            : Two Sisp port entries to be compared             */
/*    Output(s)           : None                                             */
/*    Returns             : 1  - pRBElem1 > pRBElem2                         */
/*                          -1 - pRBElem1 < pRBElem2                         */
/*                          0  = both are equal                              */
/*****************************************************************************/
INT4
VcmSispPortMapTableCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    UINT4               u4PhyIfIndex1 = VCM_INVALID_IFINDEX;
    UINT4               u4PhyIfIndex2 = VCM_INVALID_IFINDEX;
    UINT4               u4ContextId1 = VCM_INVALID_VC;
    UINT4               u4ContextId2 = VCM_INVALID_VC;

    u4PhyIfIndex1 = ((tVcmIfMapEntry *) pRBElem1)->u4PhyIfIndex;
    u4PhyIfIndex2 = ((tVcmIfMapEntry *) pRBElem2)->u4PhyIfIndex;

    u4ContextId1 = ((tVcmIfMapEntry *) pRBElem1)->u4VcNum;
    u4ContextId2 = ((tVcmIfMapEntry *) pRBElem2)->u4VcNum;

    if (u4PhyIfIndex1 < u4PhyIfIndex2)
    {
        return SISP_INDEX_LESSER;
    }
    if (u4PhyIfIndex1 > u4PhyIfIndex2)
    {
        return SISP_INDEX_GREATER;
    }
    if (u4ContextId1 < u4ContextId2)
    {
        return SISP_INDEX_LESSER;
    }
    if (u4ContextId1 > u4ContextId2)
    {
        return SISP_INDEX_GREATER;
    }
    return SISP_INDEX_EQUAL;
}

/*****************************************************************************/
/* Function Name      :  VcmSispGetPhysicalPort                              */
/*                                                                           */
/* Description        :  This function used to the get the physical IfIndex  */
/*                       of the given logical port.                          */
/*                                                                           */
/* Input(s)           :  u4LogicalIfIdx - Logical port IfIndex               */
/*                                                                           */
/* Output(s)          :  pu4IfIndex     - Physical IfIndex of logical port.  */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispGetPhysicalPort (UINT4 u4LogicalIfIdx, UINT4 *pu4IfIndex)
{
    tVcmIfMapEntry      VcmIfMapEntry;

    VCM_MEMSET (&VcmIfMapEntry, VCM_INIT_VAL, sizeof (tVcmIfMapEntry));

    if (VcmGetIfMapEntry (u4LogicalIfIdx, &VcmIfMapEntry) == VCM_SUCCESS)
    {
        *pu4IfIndex = VcmIfMapEntry.u4PhyIfIndex;
        return VCM_SUCCESS;
    }
    return VCM_FAILURE;
}

/*****************************************************************************/
/* Function Name      :  VcmSispIsEnabled                                    */
/*                                                                           */
/* Description        :  This function used to the verify whether SISP       */
/*                       configuration is allowed on this port. SISP config  */
/*                       will be allowed, only when SISP is enabled globally */
/*                       and on the port.                                    */
/*                                                                           */
/* Input(s)           :  u4IfIndex - Physical port IfIndex                   */
/*                                                                           */
/* Output(s)          :  None.                                               */
/*                                                                           */
/* Return Value(s)    : VCM_TRUE / VCM_FALSE                                 */
/*****************************************************************************/
INT4
VcmSispIsEnabled (UINT4 u4IfIndex)
{
    UINT1               u1Res = OSIX_FALSE;

    VCM_LOCK ();

    if (VCM_GET_SISP_STATUS == SISP_SHUTDOWN)
    {
        VCM_UNLOCK ();
        VCM_TRC (VCM_CONTROL_PATH_TRC | VCM_ALL_FAILURE_TRC, "SISP feature is "
                 "globally shutdown.\n");
        return VCM_FALSE;
    }

    L2IwfGetSispPortCtrlStatus (u4IfIndex, &u1Res);

    if (u1Res == SISP_DISABLE)
    {
        VCM_UNLOCK ();
        VCM_TRC_ARG1 (VCM_CONTROL_PATH_TRC | VCM_ALL_FAILURE_TRC,
                      "SISP feature is not enabled on the port - %d\n",
                      u4IfIndex);
        return VCM_FALSE;
    }
    VCM_UNLOCK ();
    return VCM_TRUE;
}

/*****************************************************************************/
/* Function Name      : VcmSispLogicalPortExistForPhysicalPort               */
/*                                                                           */
/* Description        : This function used to verify if there are any logical*/
/*                      port exists for the particular physical port.        */
/*                                                                           */
/* Input(s)           : u4PhyIfIndex   - Physical or port channel port Id.   */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_TRUE / VCM_FALSE                                 */
/*****************************************************************************/
INT1
VcmSispLogicalPortExistForPhysicalPort (UINT4 u4PhyIfIndex)
{
    tVcmIfMapEntry      VcmIfMapEntry;
    UINT4               u4ContextId = VCM_INIT_VAL;

    VCM_MEMSET (&VcmIfMapEntry, VCM_INIT_VAL, sizeof (tVcmIfMapEntry));

    if (VcmSispIsEnabled (u4PhyIfIndex) == VCM_FALSE)
    {
        return VCM_FALSE;
    }

    /* For default context */
    if (VcmSispPortMapEntryGet (u4PhyIfIndex, u4ContextId,
                                &VcmIfMapEntry) == VCM_SUCCESS)
    {
        return VCM_TRUE;
    }

    if (VcmSispPortMapEntryGetNext (u4PhyIfIndex, u4ContextId,
                                    &VcmIfMapEntry) == VCM_SUCCESS)
    {
        if (u4PhyIfIndex == VcmIfMapEntry.u4PhyIfIndex)
        {
            return VCM_TRUE;
        }
    }
    return VCM_FALSE;
}

/****************************************************************************
 Function    :  VcmSispNotifyProtocolShutdownStatus
 Input       :  None
 Output      :  None
 Returns     :  None
****************************************************************************/
VOID
VcmSispNotifyProtocolShutdownStatus (VOID)
{
#ifdef ISS_WANTED
    UINT1               au1ObjectOid[2][SNMP_MAX_OID_LENGTH];
    UINT2               u2Objects = 2;

    /* The oid list contains the list of Oids that has been registered 
     * for the protocol + the oid of the SystemControl object. */
    SNMPGetOidString (fssisp, (sizeof (fssisp) / sizeof (UINT4)),
                      au1ObjectOid[0]);

    SNMPGetOidString (FsSispSystemControl,
                      (sizeof (FsSispSystemControl) / sizeof (UINT4)),
                      au1ObjectOid[u2Objects - 1]);

    /* Send a notification to MSR to process the Rstp shutdown, with 
     * Rstp oids and its system control object */
    MsrNotifyProtocolShutdown (au1ObjectOid, u2Objects, MSR_INVALID_CNTXT);
#endif
    return;
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmSispUpdateSispPortsInHw                         */
/*                                                                           */
/*     DESCRIPTION      : This function will map SISP ports of the given     */
/*                        physical or port channel to hardware.              */
/*                                                                           */
/*     INPUT            : u4PhyIfIndex   - Physical or portchannel port      */
/*                        u1Status       - Indicated MAP or UNMAP from hw    */
/*                                         VCM_TRUE  - Map to HW             */
/*                                         VCM_FALSE - UnMap from HW         */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmSispUpdateSispPortsInHw (UINT4 u4PhyIfIndex, UINT1 u1Status)
{
    tVcmIfMapEntry      VcmIfMapEntry;
    UINT4               u4ContextId = VCM_INIT_VAL;
    UINT1               u1SispStatus = SISP_ENABLE;

    /* Function has to be called only for Physical and port channel ports */
    if ((u4PhyIfIndex <= VCM_INVALID_VAL) ||
        (u4PhyIfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        return VCM_SUCCESS;
    }

    if (VcmSispIsEnabled (u4PhyIfIndex) == VCM_FALSE)
    {
        return VCM_SUCCESS;
    }

    u1SispStatus = ((u1Status == VCM_FALSE) ? SISP_DISABLE : SISP_ENABLE);

    /* If u1Status is Enabled/Disabled recently by configuration, then
     * program the status into the hardware. */

    if (VcmFsVcmSispHwSetPortCtrlStatus (u4PhyIfIndex, u1SispStatus) ==
        FNP_FAILURE)
    {
        return VCM_FAILURE;
    }

    VCM_MEMSET (&VcmIfMapEntry, VCM_INIT_VAL, sizeof (tVcmIfMapEntry));

    /* For default context */
    if (VcmSispPortMapEntryGet (u4PhyIfIndex, u4ContextId,
                                &VcmIfMapEntry) == VCM_SUCCESS)
    {
        if (VcmUpdatePortToContextInHw (u4ContextId, VcmIfMapEntry.u4IfIndex,
                                        u1Status) == VCM_FAILURE)
        {
            return VCM_FAILURE;
        }
    }

    /* Verify in other contexts */
    while (VcmSispPortMapEntryGetNext (u4PhyIfIndex, u4ContextId,
                                       &VcmIfMapEntry) != VCM_FAILURE)
    {
        if (u4PhyIfIndex != VcmIfMapEntry.u4PhyIfIndex)
        {
            /* Next entry is of some other physical port's - Break */
            break;
        }

        if (VcmUpdatePortToContextInHw (u4ContextId, VcmIfMapEntry.u4IfIndex,
                                        u1Status) == VCM_FAILURE)
        {
            return VCM_FAILURE;
        }

        u4PhyIfIndex = VcmIfMapEntry.u4PhyIfIndex;
        u4ContextId = VcmIfMapEntry.u4VcNum;
    }
    return VCM_SUCCESS;
}
#endif
#endif
