/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* $Id: vcmnpwr.c,v 1.5 2017/11/14 07:31:19 siva Exp $                                */
/*****************************************************************************/
/*    FILE  NAME            : vcmapi.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Virtual Context Manager Module                 */
/*    MODULE NAME           : Virtual Context Manager Module                 */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 16 June 2006                                   */
/*    AUTHOR                : MI Team                                        */
/*    DESCRIPTION           : This file contains NPAPI wrapper functions.    */
/*****************************************************************************/

/***************************************************************************
 *                                                                          
 *    Function Name       : VcmNpWrHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tVcmNpModInfo
 *                                                                          
 *    Input(s)            : FsHwNpParam of type tfsHwNp                     
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#ifndef __VCM_NP_WR_C
#define __VCM_NP_WR_C

#include "nputil.h"

PUBLIC UINT1
VcmNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    INT4                i4RetVal = FNP_SUCCESS;
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tVcmNpModInfo      *pVcmNpModInfo = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pVcmNpModInfo = &(pFsHwNp->VcmNpModInfo);

    if (NULL == pVcmNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
        case FS_VCM_HW_CREATE_CONTEXT:
        {
            tVcmNpWrFsVcmHwCreateContext *pEntry = NULL;
            pEntry = &pVcmNpModInfo->VcmNpFsVcmHwCreateContext;
            i4RetVal = FsVcmHwCreateContext (pEntry->u4ContextId);
            break;
        }
        case FS_VCM_HW_DELETE_CONTEXT:
        {
            tVcmNpWrFsVcmHwDeleteContext *pEntry = NULL;
            pEntry = &pVcmNpModInfo->VcmNpFsVcmHwDeleteContext;
            i4RetVal = FsVcmHwDeleteContext (pEntry->u4ContextId);
            break;
        }
        case FS_VCM_HW_MAP_IF_INDEX_TO_BRG_PORT:
        {
            tVcmNpWrFsVcmHwMapIfIndexToBrgPort *pEntry = NULL;
            pEntry = &pVcmNpModInfo->VcmNpFsVcmHwMapIfIndexToBrgPort;
            i4RetVal =
                FsVcmHwMapIfIndexToBrgPort (pEntry->u4ContextId,
                                            pEntry->u4IfIndex,
                                            pEntry->ContextInfo);
            break;
        }
        case FS_VCM_HW_MAP_PORT_TO_CONTEXT:
        {
            tVcmNpWrFsVcmHwMapPortToContext *pEntry = NULL;
            pEntry = &pVcmNpModInfo->VcmNpFsVcmHwMapPortToContext;
            i4RetVal =
                FsVcmHwMapPortToContext (pEntry->u4ContextId,
                                         pEntry->u4IfIndex);
            break;
        }
        case FS_VCM_HW_MAP_VIRTUAL_ROUTER:
        {
            tVcmNpWrFsVcmHwMapVirtualRouter *pEntry = NULL;
            pEntry = &pVcmNpModInfo->VcmNpFsVcmHwMapVirtualRouter;
            i4RetVal =
                FsVcmHwMapVirtualRouter (pEntry->VrMapAction,
                                         pEntry->pVcmVrMapInfo);
            break;
        }
        case FS_VCM_HW_UN_MAP_PORT_FROM_CONTEXT:
        {
            tVcmNpWrFsVcmHwUnMapPortFromContext *pEntry = NULL;
            pEntry = &pVcmNpModInfo->VcmNpFsVcmHwUnMapPortFromContext;
            i4RetVal =
                FsVcmHwUnMapPortFromContext (pEntry->u4ContextId,
                                             pEntry->u4IfIndex);
            break;
        }
        case FS_VCM_SISP_HW_SET_PORT_CTRL_STATUS:
        {
            tVcmNpWrFsVcmSispHwSetPortCtrlStatus *pEntry = NULL;
            pEntry = &pVcmNpModInfo->VcmNpFsVcmSispHwSetPortCtrlStatus;
            i4RetVal =
                FsVcmSispHwSetPortCtrlStatus (pEntry->u4IfIndex,
                                              pEntry->u1Status);
            break;
        }
        case FS_VCM_SISP_HW_SET_PORT_VLAN_MAPPING:
        {
            tVcmNpWrFsVcmSispHwSetPortVlanMapping *pEntry = NULL;
            pEntry = &pVcmNpModInfo->VcmNpFsVcmSispHwSetPortVlanMapping;
            i4RetVal =
                FsVcmSispHwSetPortVlanMapping (pEntry->u4IfIndex,
                                               pEntry->VlanId,
                                               pEntry->u4ContextId,
                                               pEntry->u1Status);
            break;
        }
        case FS_VCM_HW_VRF_COUNTERS:
        {
            tVcmNpWrFsVrfCounters *pEntry = NULL;
            pEntry = &pVcmNpModInfo->VcmNpWrFsVrfCounters;
            i4RetVal = FsVcmHwHandleVrfCounter (pEntry->VrfCounter);
            break;
        }
#ifdef MBSM_WANTED
        case FS_VCM_MBSM_HW_CREATE_CONTEXT:
        {
            tVcmNpWrFsVcmMbsmHwCreateContext *pEntry = NULL;
            pEntry = &pVcmNpModInfo->VcmNpFsVcmMbsmHwCreateContext;
            i4RetVal =
                FsVcmMbsmHwCreateContext (pEntry->u4ContextId,
                                          pEntry->pSlotInfo);
            break;
        }
        case FS_VCM_MBSM_HW_MAP_IF_INDEX_TO_BRG_PORT:
        {
            tVcmNpWrFsVcmMbsmHwMapIfIndexToBrgPort *pEntry = NULL;
            pEntry = &pVcmNpModInfo->VcmNpFsVcmMbsmHwMapIfIndexToBrgPort;
            i4RetVal =
                FsVcmMbsmHwMapIfIndexToBrgPort (pEntry->u4ContextId,
                                                pEntry->u4IfIndex,
                                                pEntry->ContextInfo,
                                                pEntry->pSlotInfo);
            break;
        }
        case FS_VCM_MBSM_HW_MAP_PORT_TO_CONTEXT:
        {
            tVcmNpWrFsVcmMbsmHwMapPortToContext *pEntry = NULL;
            pEntry = &pVcmNpModInfo->VcmNpFsVcmMbsmHwMapPortToContext;
            i4RetVal =
                FsVcmMbsmHwMapPortToContext (pEntry->u4ContextId,
                                             pEntry->u4IfIndex,
                                             pEntry->pSlotInfo);
            break;
        }
        case FS_VCM_MBSM_HW_MAP_VIRTUAL_ROUTER:
        {
            tVcmNpWrFsVcmMbsmHwMapVirtualRouter *pEntry = NULL;
            pEntry = &pVcmNpModInfo->VcmNpFsVcmMbsmHwMapVirtualRouter;
            i4RetVal =
                FsVcmMbsmHwMapVirtualRouter (pEntry->VrMapAction,
                                             pEntry->pVcmVrMapInfo,
                                             pEntry->pSlotInfo);
            break;
        }
        case FS_VCM_SISP_MBSM_HW_SET_PORT_CTRL_STATUS:
        {
            tVcmNpWrFsVcmSispMbsmHwSetPortCtrlStatus *pEntry = NULL;
            pEntry = &pVcmNpModInfo->VcmNpFsVcmSispMbsmHwSetPortCtrlStatus;
            i4RetVal =
                FsVcmSispMbsmHwSetPortCtrlStatus (pEntry->u4IfIndex,
                                                  pEntry->u1Status,
                                                  pEntry->pSlotInfo);
            break;
        }
        case FS_VCM_SISP_MBSM_HW_SET_PORT_VLAN_MAPPING:
        {
            tVcmNpWrFsVcmSispMbsmHwSetPortVlanMapping *pEntry = NULL;
            pEntry = &pVcmNpModInfo->VcmNpFsVcmSispMbsmHwSetPortVlanMapping;
            i4RetVal =
                FsVcmSispMbsmHwSetPortVlanMapping (pEntry->u4IfIndex,
                                                   pEntry->VlanId,
                                                   pEntry->u4ContextId,
                                                   pEntry->u1Status,
                                                   pEntry->pSlotInfo);
            break;
        }
#endif /* MBSM_WANTED */
        default:
            i4RetVal = FNP_FAILURE;
            break;

    }                            /* switch */
    u1RetVal = (UINT1) i4RetVal;
    return (u1RetVal);
}
#endif /*__VCM_NP_WR_C */
