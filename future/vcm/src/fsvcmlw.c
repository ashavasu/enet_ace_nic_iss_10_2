/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsvcmlw.c,v 1.59 2017/11/17 12:10:14 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "vcminc.h"
# include  "fssnmp.h"
# include  "vcmcli.h"
# include  "msr.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVcmTraceOption
 Input       :  The Indices

                The Object 
                retValFsVcmTraceOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVcmTraceOption (INT4 *pi4RetValFsVcmTraceOption)
{
    *pi4RetValFsVcmTraceOption = (INT4) gVcmGlobals.u4TraceOption;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVcmTraceOption
 Input       :  The Indices

                The Object 
                setValFsVcmTraceOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVcmTraceOption (INT4 i4SetValFsVcmTraceOption)
{
    gVcmGlobals.u4TraceOption = (UINT4) i4SetValFsVcmTraceOption;
    VCM_TRC_ARG1 (VCM_MGMT_TRC,
                  " fsvcmlw: Trace Option is set with value: %d \n",
                  i4SetValFsVcmTraceOption);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVcmTraceOption
 Input       :  The Indices

                The Object 
                testValFsVcmTraceOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVcmTraceOption (UINT4 *pu4ErrorCode, INT4 i4TestValFsVcmTraceOption)
{
    if ((i4TestValFsVcmTraceOption < VCM_MIN_TRACE_VAL) ||
        (i4TestValFsVcmTraceOption > VCM_MAX_TRACE_VAL))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsVcmTraceOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVcmTraceOption (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsVcmConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsVcmConfigTable
 Input       :  The Indices
                FsVCId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsVcmConfigTable (INT4 i4FsVCId)
{
    if ((i4FsVCId < VCM_DEFAULT_CONTEXT) || (i4FsVCId > MAX_CXT_PER_SYS))
    {
        return SNMP_FAILURE;
    }

    if (VcmIsContextExist ((UINT4) i4FsVCId) != VCM_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsVcmConfigTable
 Input       :  The Indices
                FsVCId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsVcmConfigTable (INT4 *pi4FsVCId)
{
    UINT4               u4CxtId = 0;

    if (VcmGetFirstVcId (&u4CxtId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4FsVCId = (INT4) u4CxtId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsVcmConfigTable
 Input       :  The Indices
                FsVCId
                nextFsVCId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsVcmConfigTable (INT4 i4FsVCId, INT4 *pi4NextFsVCId)
{
    UINT4               u4CxtId = 0;

    if (VcmGetNextVcId ((UINT4) i4FsVCId, &u4CxtId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsVCId = (INT4) u4CxtId;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVCNextFreeHlPortId
 Input       :  The Indices
                FsVCId

                The Object 
                retValFsVCNextFreeHlPortId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVCNextFreeHlPortId (INT4 i4FsVCId, INT4 *pi4RetValFsVCNextFreeHlPortId)
{
    UINT2               u2HlPortId = 0;

    if (VcmGetVcNextFreeHlPortId ((UINT4) i4FsVCId, &u2HlPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsVCNextFreeHlPortId = (INT4) u2HlPortId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsVCMacAddress
 Input       :  The Indices
                FsVCId

                The Object 
                retValFsVCMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVCMacAddress (INT4 i4FsVCId, tMacAddr * pRetValFsVCMacAddress)
{
    if (VcmGetVCMacAddr (i4FsVCId, (UINT1 *) pRetValFsVCMacAddress) == FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsVcAlias
 Input       :  The Indices
                FsVCId

                The Object 
                retValFsVcAlias
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVcAlias (INT4 i4FsVCId, tSNMP_OCTET_STRING_TYPE * pRetValFsVcAlias)
{
    UINT1               u1Length = 0;
    UINT1               au1VcAlias[VCM_ALIAS_MAX_LEN];

    VCM_MEMSET (au1VcAlias, 0, VCM_ALIAS_MAX_LEN);

    if (VcmGetVcAlias ((UINT4) i4FsVCId, au1VcAlias) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    u1Length = (UINT1) (STRLEN (au1VcAlias));
    MEMCPY (pRetValFsVcAlias->pu1_OctetList, au1VcAlias, u1Length);
    pRetValFsVcAlias->i4_Length = u1Length;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsVCStatus
 Input       :  The Indices
                FsVCId

                The Object 
                retValFsVCStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVCStatus (INT4 i4FsVCId, INT4 *pi4RetValFsVCStatus)
{
    if (VcmGetVcRowStatus ((UINT4) i4FsVCId,
                           pi4RetValFsVCStatus) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsVcCxtType
 Input       :  The Indices
                FsVCId

                The Object 
                retValFsVcCxtType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVcCxtType (INT4 i4FsVCId, INT4 *pi4RetValFsVcCxtType)
{
    if (VcmGetVcCxtType ((UINT4) i4FsVCId, pi4RetValFsVcCxtType) == VCM_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsVRMacAddress
 Input       :  The Indices
                FsVCId

                The Object 
                retValFsVRMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVRMacAddress (INT4 i4FsVCId, tMacAddr * pRetValFsVRMacAddress)
{
    if (VcmGetVRMacAddr ((UINT4) i4FsVCId, (UINT1 *) pRetValFsVRMacAddress)
        == VCM_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVcAlias
 Input       :  The Indices
                FsVCId

                The Object 
                setValFsVcAlias
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVcAlias (INT4 i4FsVCId, tSNMP_OCTET_STRING_TYPE * pSetValFsVcAlias)
{
    tVirtualContextInfo *pVc = NULL;
    tVirtualContextInfo Dummy;
    UINT1               u1CxtType = 0;

    Dummy.u4VcNum = (UINT4) i4FsVCId;

    VCM_LOCK ();

    if (NULL != (pVc = VcmFindVcEntry (&Dummy)))
    {
        MEMSET (pVc->au1Alias, 0, VCM_ALIAS_MAX_LEN);
        MEMCPY (pVc->au1Alias, pSetValFsVcAlias->pu1_OctetList,
                pSetValFsVcAlias->i4_Length);
        pVc->au1Alias[pSetValFsVcAlias->i4_Length] = '\0';

        u1CxtType = pVc->u1CurrCxtType;
        VCM_UNLOCK ();
        /* Update L2 protocols if the context type is L2Context or both l2 and 
         * l3 context 
         */
        if ((u1CxtType == VCM_L2_CONTEXT) || (u1CxtType == VCM_L2_L3_CONTEXT))
        {
            L2UpdateAliasName ((UINT4) i4FsVCId);
        }
#ifdef SNMP_3_WANTED
        SnmpUpdateAliasName ((UINT4) i4FsVCId, pSetValFsVcAlias);
#endif
        return SNMP_SUCCESS;
    }

    VCM_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsVCStatus
 Input       :  The Indices
                FsVCId

                The Object 
                setValFsVCStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVCStatus (INT4 i4FsVCId, INT4 i4SetValFsVCStatus)
{
    tVirtualContextInfo Dummy;
    tVirtualContextInfo *pVcInfo = NULL;
    INT4                i4Result = VCM_FALSE;
    INT4                i4RowStatus = 0;

    i4Result = VcmIsContextExist ((UINT4) i4FsVCId);
    if (i4Result == VCM_TRUE)
    {
        VcmGetVcRowStatus ((UINT4) i4FsVCId, &i4RowStatus);
    }

    switch (i4SetValFsVCStatus)
    {
        case CREATE_AND_WAIT:
            if (i4Result == VCM_FALSE)
            {
                /* Context does not exist. So it is a new context creation */
                if (VcmCreateContext ((UINT4) i4FsVCId) == VCM_SUCCESS)
                {
                    return SNMP_SUCCESS;
                }
            }
            break;
        case NOT_IN_SERVICE:
            if (i4Result == VCM_TRUE)
            {
                if (i4RowStatus == i4SetValFsVCStatus)
                {
                    /* User trying to set row status as NOT_IN_SERVICE which is
                     * already NOT_IS_SERVICE
                     */
                    return SNMP_SUCCESS;
                }

                /* NOT_IN_SERVICE can be set from Active, not from NOT_READY
                 * state. 
                 */
                if (i4RowStatus == NOT_READY)
                {
                    return SNMP_FAILURE;
                }

                /* Set row status to NOT_IN_SERVICE */
                Dummy.u4VcNum = (UINT4) i4FsVCId;
                VCM_LOCK ();
                pVcInfo = VcmFindVcEntry (&Dummy);
                if (pVcInfo != NULL)
                {
                    pVcInfo->u1RowStatus = (UINT1) i4SetValFsVCStatus;
                    VCM_UNLOCK ();
                    return SNMP_SUCCESS;
                }

                VCM_UNLOCK ();
            }
            break;

        case CREATE_AND_GO:
            if (i4Result == VCM_FALSE)
            {
                /* Entry does not exist. Create the entry and make it
                 * active. It is an l2 context creation. 
                 */
                if (VcmCreateVirtualContext ((UINT4) i4FsVCId) == VCM_SUCCESS)
                {
                    return SNMP_SUCCESS;
                }
            }
            break;

        case ACTIVE:
            if (i4Result == VCM_TRUE)
            {
                /* Entry exist. Row status is being made active. */

                if (i4RowStatus == i4SetValFsVCStatus)
                {
                    /* User trying to set row status to active which is already
                     * active
                     */
                    return SNMP_SUCCESS;
                }

                if (VcmMakeContextActive ((UINT4) i4FsVCId, (UINT1) i4RowStatus)
                    == VCM_SUCCESS)
                {
                    return SNMP_SUCCESS;
                }
            }
            else
            {
                /* Entry does not exist. A new entry is being created directly
                 * by making row status active. So create a l2 context entry. 
                 * This is retained for back ward compatability.
                 */
                if (VcmCreateVirtualContext ((UINT4) i4FsVCId) == VCM_SUCCESS)
                {
                    return SNMP_SUCCESS;
                }
            }
            break;

        case DESTROY:
            if (i4Result == VCM_FALSE)
            {
                return SNMP_SUCCESS;
            }
            else
            {
                if (VcmDeleteVirtualContext ((UINT4) i4FsVCId) == VCM_SUCCESS)
                {
                    MsrNotifyContextDeletion ((UINT4) i4FsVCId);
                    return SNMP_SUCCESS;
                }
            }
            break;

        default:
            break;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsVcCxtType
 Input       :  The Indices
                FsVCId

                The Object 
                setValFsVcCxtType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVcCxtType (INT4 i4FsVCId, INT4 i4SetValFsVcCxtType)
{
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo Dummy;

    Dummy.u4VcNum = (UINT4) i4FsVCId;

    VCM_LOCK ();
    pVcInfo = VcmFindVcEntry (&Dummy);
    if (pVcInfo != NULL)
    {
        pVcInfo->u1CfgCxtType = (UINT1) i4SetValFsVcCxtType;
        VCM_UNLOCK ();
        return SNMP_SUCCESS;
    }

    VCM_UNLOCK ();
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVcAlias
 Input       :  The Indices
                FsVCId

                The Object 
                testValFsVcAlias
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVcAlias (UINT4 *pu4ErrorCode, INT4 i4FsVCId,
                    tSNMP_OCTET_STRING_TYPE * pTestValFsVcAlias)
{
    INT4                i4VcId = 0;
    if (pTestValFsVcAlias->i4_Length > VCM_ALIAS_LEN)
    {
        CLI_SET_ERR (CLI_VCM_CXT_ALIAS_LEN_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

#ifdef SNMP_2_WANTED
    if (SNMPCheckForNVTChars (pTestValFsVcAlias->pu1_OctetList,
                              pTestValFsVcAlias->i4_Length) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

    if (VcmIsContextExist ((UINT4) i4FsVCId) != VCM_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VcmIsSwitchExist (pTestValFsVcAlias->pu1_OctetList, (UINT4 *) &i4VcId)
        == VCM_TRUE)
    {
        if (i4FsVCId != i4VcId)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (VcmIsVrfExist (pTestValFsVcAlias->pu1_OctetList, (UINT4 *) &i4VcId)
        == VCM_TRUE)
    {
        if (i4FsVCId != i4VcId)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsVCStatus
 Input       :  The Indices
                FsVCId

                The Object 
                testValFsVCStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVCStatus (UINT4 *pu4ErrorCode, INT4 i4FsVCId,
                     INT4 i4TestValFsVCStatus)
{
    INT4                i4Result = VCM_FALSE;
    INT4                i4RowStatus = 0;
    INT4                i4CxtType = 0;
    INT4                i4CfgCxtType = 0;
    UINT2               u2PortCount = 0;
    UINT2               u2IpIfCount = 0;

    if ((i4FsVCId < VCM_DEFAULT_CONTEXT) || (i4FsVCId > MAX_CXT_PER_SYS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (i4FsVCId == VCM_DEFAULT_CONTEXT)
    {
        /*Modification related to default context is not allowed */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsVCStatus < ACTIVE) || (i4TestValFsVCStatus > DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    i4Result = VcmIsContextExist ((UINT4) i4FsVCId);
    if (i4Result == VCM_TRUE)
    {
        VcmGetVcRowStatus ((UINT4) i4FsVCId, &i4RowStatus);
    }

    switch (i4TestValFsVCStatus)
    {
        case CREATE_AND_WAIT:
            if (i4Result == VCM_TRUE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
        case ACTIVE:
            if (i4Result == VCM_TRUE)
            {
                /* Entry exist. Row status can be made active from either 
                 * NOT_READY or NOT_IN_SERVICE state
                 */
                if ((i4RowStatus != NOT_IN_SERVICE) &&
                    (i4RowStatus != NOT_READY) && (i4RowStatus != ACTIVE))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }

                VcmGetVcCxtType ((UINT4) i4FsVCId, &i4CxtType);
                VcmGetVcCfgCxtType ((UINT4) i4FsVCId, &i4CfgCxtType);
                if (i4CfgCxtType == VCM_INVALID_CONTEXT_TYPE)
                {
                    /* Entry can be made active only when the context type is
                     * configured.
                     */
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }

                if ((i4CfgCxtType == VCM_L2_CONTEXT) &&
                    (VcmGetL2Mode () == VCM_SI_MODE))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
                /* Return failure if the system is already configured with
                 * max l3 contexts and the user try to configure a new l3 context.
                 */

                i4Result = VcmIsMaxL3CxtCreated ();
                if (i4Result == VCM_TRUE)
                {
                    /* Max L3 context is created in the system. Check if user is
                     * trying to create a new l3 context 
                     */
                    if ((i4CxtType == VCM_INVALID_CONTEXT_TYPE) &&
                        ((i4CfgCxtType == VCM_L3_CONTEXT) ||
                         (i4CfgCxtType == VCM_L2_L3_CONTEXT)))
                    {
                        CLI_SET_ERR (CLI_VCM_MAX_L3_CXT_ERR);
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        return SNMP_FAILURE;
                    }
                    if ((i4CxtType == VCM_L2_CONTEXT) &&
                        ((i4CfgCxtType == VCM_L3_CONTEXT) ||
                         (i4CfgCxtType == VCM_L2_L3_CONTEXT)))
                    {
                        CLI_SET_ERR (CLI_VCM_MAX_L3_CXT_ERR);
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        return SNMP_FAILURE;
                    }
                }

                /* If an l2 context is changed to l3 context or an l3 context is
                 * changed to l2 context, l2 context and l3 context gets deleted
                 * respectively. Context deletion should be allowed only when
                 * all the configured ports are unmapped. So check if any
                 * l2 and l3 interfaces are mapped to this context
                 */

                if (((i4CxtType == VCM_L2_CONTEXT) ||
                     (i4CxtType == VCM_L2_L3_CONTEXT)) &&
                    (i4CfgCxtType == VCM_L3_CONTEXT))
                {
                    /* l2 context will be deleted. Check whether any ports are 
                     * present in the context */
                    VcmGetVcPortCount ((UINT4) i4FsVCId, &u2PortCount);
                    if (u2PortCount != 0)
                    {
                        CLI_SET_ERR (CLI_VCM_IFACE_MAPPED_ERR);
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        return SNMP_FAILURE;
                    }

                    /* Check if this l2 context is mapped to any of the IVR 
                     * interfaces.
                     */
                    if (VcmIsL3IfMappedToL2Cxt ((UINT4) i4FsVCId) == VCM_TRUE)
                    {
                        CLI_SET_ERR (CLI_VCM_IFACE_MAPPED_ERR);
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        return SNMP_FAILURE;
                    }
                }
                if (((i4CxtType == VCM_L3_CONTEXT) ||
                     (i4CxtType == VCM_L2_L3_CONTEXT)) &&
                    (i4CfgCxtType == VCM_L2_CONTEXT))
                {
                    /* L3 context will be deleted. Check if any interfaces 
                     * are mapped to the context
                     */
                    VcmGetIpIfCount ((UINT4) i4FsVCId, &u2IpIfCount);
                    if (u2IpIfCount != 0)
                    {
                        CLI_SET_ERR (CLI_VCM_IFACE_MAPPED_ERR);
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        return SNMP_FAILURE;
                    }
                }
            }
            else
            {
                if (VcmGetL2Mode () == VCM_SI_MODE)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
            break;

        case CREATE_AND_GO:
            if (i4Result == VCM_TRUE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            else
            {
                if (VcmGetL2Mode () == VCM_SI_MODE)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
            break;

        case NOT_IN_SERVICE:
            if (i4Result == VCM_FALSE)
            {
                /* Context does not exist. So cannot be made not-in-service
                 */
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            else
            {
                if ((i4RowStatus != NOT_IN_SERVICE) && (i4RowStatus != ACTIVE))
                {
                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                    return SNMP_FAILURE;
                }
            }
            break;

        case DESTROY:
            if (i4Result != VCM_TRUE)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }

            /* To delete a context, 
             * there should not be any l2 port mapped to the context, 
             * there should not be any l3 interface mapped to the context
             * there should not be any IVR interface whose l2 context is mapped 
             * to the context.
             */
            VcmGetVcPortCount ((UINT4) i4FsVCId, &u2PortCount);
            VcmGetIpIfCount ((UINT4) i4FsVCId, &u2IpIfCount);
            i4Result = VcmIsL3IfMappedToL2Cxt ((UINT4) i4FsVCId);

            if ((u2PortCount != 0) || (i4Result == VCM_TRUE) ||
                (u2IpIfCount != 0))
            {
                CLI_SET_ERR (CLI_VCM_IFACE_MAPPED_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsVcCxtType
 Input       :  The Indices
                FsVCId

                The Object 
                testValFsVcCxtType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVcCxtType (UINT4 *pu4ErrorCode, INT4 i4FsVCId,
                      INT4 i4TestValFsVcCxtType)
{
    INT4                i4RowStatus = 0;

    if ((i4TestValFsVcCxtType != VCM_L2_CONTEXT) &&
        (i4TestValFsVcCxtType != VCM_L3_CONTEXT) &&
        (i4TestValFsVcCxtType != VCM_L2_L3_CONTEXT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VcmIsContextExist ((UINT4) i4FsVCId) == VCM_FALSE)
    {
        /* Context does not exist */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    VcmGetVcRowStatus ((UINT4) i4FsVCId, &i4RowStatus);

    /* Context type can be changed only if the row status is in NOT_IN_SERVICE
     * or NOT_READY state
     */
    if ((i4RowStatus != NOT_IN_SERVICE) && (i4RowStatus != NOT_READY))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* If L2SI, then context type can't be set to l3 context or both l2 and l3
     * context
     */
    if ((VcmGetL2Mode () == VCM_SI_MODE) &&
        ((i4TestValFsVcCxtType == VCM_L2_CONTEXT) ||
         (i4TestValFsVcCxtType == VCM_L2_L3_CONTEXT)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* If L3SI, then context type can't be set to l3 context of both l2 and l3
     * context
     */
    if ((VcmGetL3Mode () == VCM_SI_MODE) &&
        ((i4TestValFsVcCxtType == VCM_L3_CONTEXT) ||
         (i4TestValFsVcCxtType == VCM_L2_L3_CONTEXT)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsVcmConfigTable
 Input       :  The Indices
                FsVCId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVcmConfigTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsVcmIfMappingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsVcmIfMappingTable
 Input       :  The Indices
                FsVcmIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsVcmIfMappingTable (INT4 i4FsVcmIfIndex)
{
    if ((i4FsVcmIfIndex <= VCM_INVALID_VAL) ||
        (i4FsVcmIfIndex > VCM_MAX_IFINDEX))
    {
        return SNMP_FAILURE;
    }
    if (VcmIsIfMapExist ((UINT4) i4FsVcmIfIndex) != VCM_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsVcmIfMappingTable
 Input       :  The Indices
                FsVcmIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsVcmIfMappingTable (INT4 *pi4FsVcmIfIndex)
{
    UINT4               u4IfIndex = 0;

    if (VcmGetFirstIfMapIfIndex (&u4IfIndex) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4FsVcmIfIndex = (INT4) u4IfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsVcmIfMappingTable
 Input       :  The Indices
                FsVcmIfIndex
                nextFsVcmIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsVcmIfMappingTable (INT4 i4FsVcmIfIndex,
                                    INT4 *pi4NextFsVcmIfIndex)
{
    UINT4               u4IfIndex = 0;

    if (VcmGetNextIfMapIfIndex ((UINT4) i4FsVcmIfIndex,
                                &u4IfIndex) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsVcmIfIndex = (INT4) u4IfIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVcId
 Input       :  The Indices
                FsVcmIfIndex

                The Object 
                retValFsVcId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVcId (INT4 i4FsVcmIfIndex, INT4 *pi4RetValFsVcId)
{
    UINT4               u4CxtId = 0;

    if (VcmGetIfMapVcId ((UINT4) i4FsVcmIfIndex, &u4CxtId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsVcId = (INT4) u4CxtId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsVcHlPortId
 Input       :  The Indices
                FsVcmIfIndex

                The Object 
                retValFsVcHlPortId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVcHlPortId (INT4 i4FsVcmIfIndex, INT4 *pi4RetValFsVcHlPortId)
{
    UINT2               u2HlPortId = 0;

    if (VcmGetIfMapHlPortId ((UINT4) i4FsVcmIfIndex,
                             &u2HlPortId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsVcHlPortId = (INT4) u2HlPortId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsVcIfRowStatus
 Input       :  The Indices
                FsVcmIfIndex

                The Object 
                retValFsVcIfRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVcIfRowStatus (INT4 i4FsVcmIfIndex, INT4 *pi4RetValFsVcIfRowStatus)
{
    if (VcmGetIfMapStatus ((UINT4) i4FsVcmIfIndex,
                           pi4RetValFsVcIfRowStatus) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsVcL2ContextId
 Input       :  The Indices
                FsVcmIfIndex

                The Object 
                retValFsVcL2ContextId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVcL2ContextId (INT4 i4FsVcmIfIndex, INT4 *pi4RetValFsVcL2ContextId)
{
    if (VcmGetL2ContextId ((UINT4) i4FsVcmIfIndex, pi4RetValFsVcL2ContextId)
        == VCM_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVcId
 Input       :  The Indices
                FsVcmIfIndex

                The Object 
                setValFsVcId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVcId (INT4 i4FsVcmIfIndex, INT4 i4SetValFsVcId)
{
    UINT4               u4VcNum = 0;
    UINT4               u4BridgeMode = L2IWF_INVALID_BRIDGE_MODE;
    INT4                i4RetVal = L2IWF_SUCCESS;

    if ((i4SetValFsVcId < VCM_DEFAULT_CONTEXT) ||
        (i4SetValFsVcId > MAX_CXT_PER_SYS))
    {
        return SNMP_FAILURE;
    }

    if (VcmIsIfMapExist ((UINT4) i4FsVcmIfIndex) == VCM_FALSE)
    {
        return SNMP_FAILURE;
    }

    VcmGetIfMapVcId ((UINT4) i4FsVcmIfIndex, &u4VcNum);

    if (u4VcNum == (UINT4) i4SetValFsVcId)
    {
        return SNMP_SUCCESS;
    }

    /* Check if the interface is an l3 interface or l2 interface.
     */
    i4RetVal = VcmIsL3Interface ((UINT4) i4FsVcmIfIndex);
    if (i4RetVal == VCM_TRUE)
    {
        /* The interface is an l3 interface */
        if (VcmSetIfMapVcId ((UINT4) i4FsVcmIfIndex,
                             (UINT4) i4SetValFsVcId) != VCM_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        return SNMP_SUCCESS;
    }

    /* The interface is an l2 interface */
    /* Check whether bridge mode is configured for this VC.
     * If bridge mode is not configured, then don't allow 
     * mapping of any port to that VC. */
    i4RetVal = VcmL2IwfGetBridgeMode ((UINT4) i4SetValFsVcId, &u4BridgeMode);
    if ((i4RetVal == L2IWF_FAILURE) ||
        (u4BridgeMode == VLAN_INVALID_BRIDGE_MODE))
    {
        return SNMP_FAILURE;
    }

    if (VcmSetIfMapVcId ((UINT4) i4FsVcmIfIndex,
                         (UINT4) i4SetValFsVcId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsVcIfRowStatus
 Input       :  The Indices
                FsVcmIfIndex

                The Object 
                setValFsVcIfRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVcIfRowStatus (INT4 i4FsVcmIfIndex, INT4 i4SetValFsVcIfRowStatus)
{
    tVcmIfMapEntry      Dummy;
    UINT4               u4CxtId = 0;
    INT4                i4RetVal = 0;
    INT4                i4RowStatus = 0;
#ifdef NPAPI_WANTED
    tCfaIfInfo          IfInfo;
#endif
    tVcmIfMapEntry     *pIfMapEntry = NULL;
    UINT1               u1Status = 0;

    /* Check entry already existing */
    i4RetVal = VcmIsIfMapExist ((UINT4) i4FsVcmIfIndex);

    if (i4RetVal == VCM_TRUE)
    {
        VcmGetIfMapStatus ((UINT4) i4FsVcmIfIndex, &i4RowStatus);
        VcmGetIfMapVcId ((UINT4) i4FsVcmIfIndex, &u4CxtId);
        if (i4RowStatus == i4SetValFsVcIfRowStatus)
        {
            return SNMP_SUCCESS;
        }
    }

    switch (i4SetValFsVcIfRowStatus)
    {
        case CREATE_AND_WAIT:
            if (i4RetVal != VCM_TRUE)
            {
                /* Entry does not exist */
                if (VcmCreateIfMapEntry ((UINT4) i4FsVcmIfIndex) != VCM_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
            else
            {
                return SNMP_FAILURE;
            }
            break;

        case DESTROY:

            /* Destroy is allowed only for l2 interface mapping. For l3 interfaces
             * destroy is not supported. On unmapping the l3 interface, it will
             * get internally mapped to default context. So here only l2
             * interfaces are handled
             */
            if (i4RetVal != VCM_TRUE)
            {
                return SNMP_FAILURE;
            }
            if (i4RowStatus != ACTIVE)
            {
                /* If the entry is not ACTIVE then no need to give delete 
                 * indication to Layer 2 modules and the underlying hardware. */
                VcmDeleteIfMapEntry ((UINT4) i4FsVcmIfIndex);
                return SNMP_SUCCESS;
            }
            /* Disable SISP status in case it is enabled. This can happed when
             * SISP is enabled over an interface and no secondary contexts are
             * present. Now if port is unmapped, then SISP needs to be 
             * destroyed
             * */
            L2IwfGetSispPortCtrlStatus ((UINT4) i4FsVcmIfIndex, &u1Status);
            if (u1Status == L2IWF_ENABLED)
            {
                if (VcmSispDisableOnPort ((UINT4) i4FsVcmIfIndex) !=
                    VCM_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }

            if (L2DeleteInterface ((UINT4) i4FsVcmIfIndex) != VCM_SUCCESS)
            {
                return SNMP_FAILURE;
            }
#ifdef NPAPI_WANTED
            if (VcmCfaGetIfInfo ((UINT4) i4FsVcmIfIndex,
                                 &IfInfo) == CFA_SUCCESS)
            {
                if ((IfInfo.u1IfType != CFA_LAGG) && (i4RowStatus == ACTIVE))
                {
                    if (VCM_NODE_STATUS () == VCM_RED_ACTIVE)
                    {
                        if (VcmFsVcmHwUnMapPortFromContext (u4CxtId,
                                                            (UINT4)
                                                            i4FsVcmIfIndex) !=
                            FNP_SUCCESS)
                        {
                            VcmCreateInterface ((UINT4) i4FsVcmIfIndex);
                            return SNMP_FAILURE;
                        }
                    }
                }
            }
#endif
            if (SUCCESS == VcmDeleteIfMapEntry ((UINT4) i4FsVcmIfIndex))
            {
                VcmDecrementPortCount (u4CxtId);
                MsrNotifyPortDeletion ((UINT4) i4FsVcmIfIndex);
            }
            else
            {
                return SNMP_FAILURE;
            }
            break;

        case NOT_IN_SERVICE:
            /* NOT_IN_SERVICE is supported only for l3 interfaces 
             */
            if (i4RetVal == VCM_TRUE)
            {
                Dummy.u4IfIndex = (UINT4) i4FsVcmIfIndex;

                VCM_LOCK ();
                pIfMapEntry = VcmFindIfMapEntry (&Dummy);
                if (pIfMapEntry != NULL)
                {
                    pIfMapEntry->u1RowStatus = (UINT1) i4SetValFsVcIfRowStatus;

                }
                VCM_UNLOCK ();
            }

            break;

        case ACTIVE:
            if (i4RetVal == VCM_TRUE)
            {
                if (i4RowStatus == NOT_READY)
                {
                    /* Rowstatus will be in NOT_READY state for l2 interfaces 
                     * only as create and wait is supported only for l2 interfaces
                     */
                    if ((VCM_NODE_STATUS () == VCM_RED_ACTIVE) ||
                        (VCM_RED_RM_GET_STATIC_CONFIG_STATUS ()
                         != RM_STATIC_CONFIG_RESTORED))
                    {
                        VcmSetIfMapStatus ((UINT4) i4FsVcmIfIndex, ACTIVE);
                    }

                    /* Interface creation should be done on Standby node only after
                     * getting the local port number from the Active node. Static
                     * configuration restoration is done only after the VCM module
                     * completes the BULK updates.
                     */
                    if ((VCM_NODE_STATUS () == VCM_RED_ACTIVE) ||
                        (VCM_RED_RM_GET_STATIC_CONFIG_STATUS ()
                         != RM_STATIC_CONFIG_RESTORED))
                    {
                        if (VcmCreateInterface ((UINT4) i4FsVcmIfIndex)
                            != VCM_SUCCESS)
                        {
                            return SNMP_FAILURE;
                        }

                        VcmGetIfMapVcId ((UINT4) i4FsVcmIfIndex, &u4CxtId);
                        VcmIncrementPortCount (u4CxtId);
                    }
                }
                else
                {
                    /* L3 interface mapping. */
                    if (VcmHandleL3IfaceMapping ((UINT4) i4FsVcmIfIndex) ==
                        VCM_FAILURE)
                    {
                        return SNMP_FAILURE;
                    }
                    CfaApiUpdtIvrInterface ((UINT4) i4FsVcmIfIndex,
                                            CFA_IF_CREATE);
                }
            }
            else
            {
                return SNMP_FAILURE;
            }
            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsVcL2ContextId
 Input       :  The Indices
                FsVcmIfIndex

                The Object 
                setValFsVcL2ContextId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVcL2ContextId (INT4 i4FsVcmIfIndex, INT4 i4SetValFsVcL2ContextId)
{
    tVcmIfMapEntry      Dummy;
    tVcmIfMapEntry     *pIfMap = NULL;

    Dummy.u4IfIndex = (UINT4) i4FsVcmIfIndex;

    VCM_LOCK ();

    pIfMap = VcmFindIfMapEntry (&Dummy);
    if (pIfMap != NULL)
    {
        pIfMap->u4L2ContextId = (UINT4) i4SetValFsVcL2ContextId;

        VCM_UNLOCK ();
        return SNMP_SUCCESS;
    }

    VCM_UNLOCK ();
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVcId
 Input       :  The Indices
                FsVcmIfIndex

                The Object 
                testValFsVcId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVcId (UINT4 *pu4ErrorCode, INT4 i4FsVcmIfIndex, INT4 i4TestValFsVcId)
{
    tCfaIfInfo          CfaIfInfo;
    INT4                i4RetVal = L2IWF_SUCCESS;
    INT4                i4Result = VCM_FALSE;
    INT4                i4RowStatus = 0;
    INT4                i4VcRowStatus = 0;
    INT4                i4ContextType = 0;
    UINT4               u4VcNum = VCM_ZERO;
    UINT4               u4BridgeMode = L2IWF_INVALID_BRIDGE_MODE;
    UINT2               u2PortCount = 0;

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    if ((i4TestValFsVcId < VCM_DEFAULT_CONTEXT) ||
        (i4TestValFsVcId > MAX_CXT_PER_SYS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VcmIsIfMapExist ((UINT4) i4FsVcmIfIndex) != VCM_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Validate the virtual context id is valid or not */
    if (VcmIsContextExist ((UINT4) i4TestValFsVcId) != VCM_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    VcmGetVcCxtType ((UINT4) i4TestValFsVcId, &i4ContextType);
    VcmGetVcRowStatus ((UINT4) i4TestValFsVcId, &i4VcRowStatus);
    if ((i4ContextType == VCM_INVALID_CONTEXT_TYPE) ||
        (i4VcRowStatus == NOT_READY))
    {
        /* Only when the context type is properly set, interfaces can be
         * mapped to the context.
         * Also if the context is in creation, this context can't be mapped
         * to interfaces
         */
        CLI_SET_ERR (CLI_VCM_CXT_UNDER_CREATION_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Check if the interface is ICCL or MCLAG and the VC ID is non-default
     * one, then the configuration should be blocked */

    if ((VcmIcchIsIcclInterface (i4FsVcmIfIndex) == OSIX_TRUE) &&
        (i4TestValFsVcId != VCM_DEFAULT_CONTEXT))
    {
        CLI_SET_ERR (CLI_VCM_MAP_ICCL_MCLAG_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    i4Result = VcmIsL3Interface ((UINT4) i4FsVcmIfIndex);
    if (i4Result == VCM_TRUE)
    {
        /* L3 interface must be mapped to l3 context only
         */
        if ((i4ContextType != VCM_L3_CONTEXT) &&
            (i4ContextType != VCM_L2_L3_CONTEXT))
        {
            CLI_SET_ERR (CLI_VCM_L3IFACE_L2CXT_MAP_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        i4Result =
            VcmValidateL3IfMapping ((UINT4) i4FsVcmIfIndex,
                                    (UINT4) i4TestValFsVcId, pu4ErrorCode);
        return ((i4Result == VCM_SUCCESS) ? SNMP_SUCCESS : SNMP_FAILURE);

    }
    else
    {
        /* L2 interface must be mapped to l2 context only
         */
        if ((i4ContextType != VCM_L2_CONTEXT) &&
            (i4ContextType != VCM_L2_L3_CONTEXT))
        {
            CLI_SET_ERR (CLI_VCM_L2IFACE_L3CXT_MAP_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    /* Check whether bridge mode is configured for this VC.
     * If bridge mode is not configured, then don't allow 
     * mapping of any port to that VC. */
    i4RetVal = VcmL2IwfGetBridgeMode ((UINT4) i4TestValFsVcId, &u4BridgeMode);

    if ((i4RetVal == L2IWF_FAILURE) ||
        (u4BridgeMode == VLAN_INVALID_BRIDGE_MODE))
    {
        CLI_SET_ERR (CLI_VCM_MAP_BRG_MODE_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((VcmCfaIsVirtualInterface ((UINT4) i4FsVcmIfIndex)) == CFA_TRUE)
    {
        /* Virtual Ports can be mapped to a context only if
           bridge mode is PBB */
        if ((u4BridgeMode != VLAN_PBB_ICOMPONENT_BRIDGE_MODE) &&
            (u4BridgeMode != VLAN_PBB_BCOMPONENT_BRIDGE_MODE))
        {
            CLI_SET_ERR (CLI_VCM_MAP_PBB_BRG_MODE_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    VcmGetVcPortCount ((UINT4) i4TestValFsVcId, &u2PortCount);
    if (u2PortCount >= MAX_PORTS_PER_CXT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    /* User is allowd to set fsVcId when ifMap entry NOT_READY or NOT_IN_SERVICE
     * If the ifMap entry is already active, user is not allowed to modify
     * the VcId to different value.  If user configures to the same value
     * just return success */

    VcmGetIfMapVcId ((UINT4) i4FsVcmIfIndex, &u4VcNum);
    VcmGetIfMapStatus ((UINT4) i4FsVcmIfIndex, &i4RowStatus);

    if (ACTIVE == i4RowStatus)
    {
        if (u4VcNum != (UINT4) i4TestValFsVcId)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    if (VcmCfaGetIfInfo ((UINT4) i4FsVcmIfIndex, &CfaIfInfo) == CFA_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    /* S-Channel interfaces are created internally in the system.
     * They are present in the context where their UAP is present.
     * So mapping an S-Channel interface to a context is not allowed. */
    if (CfaIfInfo.u1BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VCM_SCHANNEL_CXT_MAP_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsVcIfRowStatus
 Input       :  The Indices
                FsVcmIfIndex

                The Object 
                testValFsVcIfRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVcIfRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsVcmIfIndex,
                          INT4 i4TestValFsVcIfRowStatus)
{
    tCfaIfInfo          IfInfo;
    INT4                i4RetVal = VCM_FALSE;
    INT4                i4RowStatus = 0;
    UINT4               u4CfgVcId = 0;

    if ((i4TestValFsVcIfRowStatus < ACTIVE) ||
        (i4TestValFsVcIfRowStatus > DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    i4RetVal = VcmIsIfMapExist ((UINT4) i4FsVcmIfIndex);

    switch (i4TestValFsVcIfRowStatus)
    {
        case CREATE_AND_WAIT:

            /* This row status is supported only for l2 interfaces as l3 
             * interfaces will always be mapped to default context on interface
             * creation itself.
             */
            if (i4RetVal == VCM_TRUE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                VCM_TRC (VCM_MGMT_TRC | VCM_ALL_FAILURE_TRC,
                         "nmhTestv2FsVcIfRowStatus: Inconsistent value.\n");
                return SNMP_FAILURE;
            }

            /* Check if interface exists, before allowing the interface-to-context 
             * mapping. 
             */
            if (VcmCfaGetIfInfo ((UINT4) i4FsVcmIfIndex, &IfInfo) !=
                CFA_SUCCESS)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            /* Create and wait is not supported for l3 interface.
             * When ever interface gets created in cfa, an IfMap entry gets
             * created in VCM. 
             */
            if (VcmIsL3Interface ((UINT4) i4FsVcmIfIndex) == VCM_TRUE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            /* Allow l2 interface mapping only if L2MI is enabled */
            if (VcmGetL2Mode () == VCM_SI_MODE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            break;

        case NOT_IN_SERVICE:

            /* This state is supported for l3 interfaces only. */
            if (i4RetVal == VCM_FALSE)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                VCM_TRC (VCM_MGMT_TRC | VCM_ALL_FAILURE_TRC,
                         "nmhTestv2FsVcIfRowStatus: NoCreation Error.\n");
                return SNMP_FAILURE;
            }

            if (VcmIsL3Interface ((UINT4) i4FsVcmIfIndex) == VCM_FALSE)
            {
                /* It is an l2 interface. NOT_IN_SERVICE is supported only
                 * for l3 interfaces
                 */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                VCM_TRC (VCM_MGMT_TRC | VCM_ALL_FAILURE_TRC,
                         "nmhTestv2FsVcIfRowStatus: Inconsistent value.\n");
                return SNMP_FAILURE;
            }

            /* Allow l3 interface mapping only if L3MI is enabled */
            if (VcmGetL3Mode () == VCM_SI_MODE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            VcmGetIfMapStatus ((UINT4) i4FsVcmIfIndex, &i4RowStatus);
            if ((i4RowStatus != ACTIVE) && (i4RowStatus != NOT_IN_SERVICE))
            {
                /* NOT_IN_SERVICE can be set from ACTIVE state only */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                VCM_TRC (VCM_MGMT_TRC | VCM_ALL_FAILURE_TRC,
                         "nmhTestv2FsVcIfRowStatus: Inconsisteant value.\n");
                return SNMP_FAILURE;
            }
            break;

        case ACTIVE:
            if (i4RetVal == VCM_FALSE)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                VCM_TRC (VCM_MGMT_TRC | VCM_ALL_FAILURE_TRC,
                         "nmhTestv2FsVcIfRowStatus: NoCreation Error.\n");
                return SNMP_FAILURE;
            }

            /* IfMapping must be made active only if the interace stat is 
             * active */
            if (VcmCfaGetIfInfo ((UINT4) i4FsVcmIfIndex, &IfInfo) !=
                CFA_SUCCESS)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            /* Row status can be made active only after configuring a valid
             * VC Id.
             */
            VcmGetIfMapCfgVcId ((UINT4) i4FsVcmIfIndex, &u4CfgVcId);
            if (u4CfgVcId == VCM_INVALID_VC)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                VCM_TRC (VCM_MGMT_TRC | VCM_ALL_FAILURE_TRC,
                         "nmhTestv2FsVcIfRowStatus: Inconsistent value.\n");
                return SNMP_FAILURE;
            }
            break;

        case DESTROY:

            if (VcmSispLogicalPortExistForPhysicalPort ((UINT4) i4FsVcmIfIndex)
                == VCM_TRUE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_VCM_SISP_LOG_PORT_EXIST_ERR);
                return SNMP_FAILURE;
            }

            if (VcmIsL3SubIfParentPort ((UINT4) i4FsVcmIfIndex) == VCM_TRUE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_VCM_L3SUBIF_EXIST_ERR);
                return SNMP_FAILURE;
            }

            if (VcmIsL3Interface ((UINT4) i4FsVcmIfIndex) == VCM_TRUE)
            {
                /* It is an l3 interface. DESTROY is supported only
                 * for l2 interfaces
                 */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                VCM_TRC (VCM_MGMT_TRC | VCM_ALL_FAILURE_TRC,
                         "nmhTestv2FsVcIfRowStatus: Inconsisteant value.\n");
                return SNMP_FAILURE;
            }
            if (OSIX_FALSE ==
                VcmL2IwfIsPortChannelConfigAllowed ((UINT4) i4FsVcmIfIndex))
            {
                CLI_SET_ERR (CLI_VCM_LAGG_CONFIG_ERR);
                /* Portchannel unmap is not allowed by L2IWF */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                VCM_TRC (VCM_MGMT_TRC | VCM_ALL_FAILURE_TRC,
                         "nmhTestv2FsVcIfRowStatus: Inconsistent value.\n");
                return SNMP_FAILURE;
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            VCM_TRC (VCM_MGMT_TRC | VCM_ALL_FAILURE_TRC,
                     "nmhTestv2FsVcIfRowStatus: Inconsistent value.\n");
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsVcL2ContextId
 Input       :  The Indices
                FsVcmIfIndex

                The Object 
                testValFsVcL2ContextId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVcL2ContextId (UINT4 *pu4ErrorCode, INT4 i4FsVcmIfIndex,
                          INT4 i4TestValFsVcL2ContextId)
{
    tCfaIfInfo          CfaIfInfo;
    INT4                i4ContextType = 0;
    INT4                i4Status = 0;

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    if ((i4TestValFsVcL2ContextId < VCM_DEFAULT_CONTEXT) ||
        (i4TestValFsVcL2ContextId >= SYS_DEF_MAX_NUM_CONTEXTS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VcmIsIfMapExist ((UINT4) i4FsVcmIfIndex) != VCM_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (VcmGetIfMapStatus ((UINT4) i4FsVcmIfIndex, &i4Status) != VCM_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (i4Status != NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate the virtual context id is valid or not */

    if (VcmIsContextExist ((UINT4) i4TestValFsVcL2ContextId) != VCM_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Check if the context is an l2 context */
    VcmGetVcCxtType ((UINT4) i4TestValFsVcL2ContextId, &i4ContextType);
    if ((i4ContextType != VCM_L2_CONTEXT) &&
        (i4ContextType != VCM_L2_L3_CONTEXT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* L2ContextId can be set only when interface alias name is not set as
     * alias name for IVR interface is no longer unique. Name + l2 cotext
     * id will be unique across context. . 
     */
    if (CfaIsAliasNameSet ((UINT4) i4FsVcmIfIndex) == CFA_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsVcmIfMappingTable
 Input       :  The Indices
                FsVcmIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVcmIfMappingTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsVcmL2CxtAndVlanToIPIfaceMapTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsVcmL2CxtAndVlanToIPIfaceMapTable
 Input       :  The Indices
                FsVcmL2VcId
                FsVcmVlanId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsVcmL2CxtAndVlanToIPIfaceMapTable (INT4 i4FsVcmL2VcId,
                                                            INT4 i4FsVcmVlanId)
{
    if ((i4FsVcmL2VcId < VCM_DEFAULT_CONTEXT) ||
        (i4FsVcmL2VcId > SYS_DEF_MAX_NUM_CONTEXTS))
    {
        return SNMP_FAILURE;
    }
    if ((i4FsVcmVlanId < VLAN_MIN_VLAN_ID)
        || (i4FsVcmVlanId > VLAN_MAX_VLAN_ID))
    {
        return SNMP_FAILURE;
    }
    if (VcmIsL2CxtToIPIfMapExist ((UINT4) i4FsVcmL2VcId, (UINT2) i4FsVcmVlanId)
        != VCM_TRUE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsVcmL2CxtAndVlanToIPIfaceMapTable
 Input       :  The Indices
                FsVcmL2VcId
                FsVcmVlanId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsVcmL2CxtAndVlanToIPIfaceMapTable (INT4 *pi4FsVcmL2VcId,
                                                    INT4 *pi4FsVcmVlanId)
{
    UINT2               u2VlanId = 0;
    UINT4               u4L2CxtId = 0;

    if (VcmGetFirstL2CxtToIPIfaceMapIndex (&u4L2CxtId, &u2VlanId) !=
        VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4FsVcmL2VcId = (INT4) u4L2CxtId;
    *pi4FsVcmVlanId = (INT4) u2VlanId;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsVcmL2CxtAndVlanToIPIfaceMapTable
 Input       :  The Indices
                FsVcmL2VcId
                nextFsVcmL2VcId
                FsVcmVlanId
                nextFsVcmVlanId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsVcmL2CxtAndVlanToIPIfaceMapTable (INT4 i4FsVcmL2VcId,
                                                   INT4 *pi4NextFsVcmL2VcId,
                                                   INT4 i4FsVcmVlanId,
                                                   INT4 *pi4NextFsVcmVlanId)
{
    UINT2               u2VlanId = 0;
    UINT2               u2NextVlanId = 0;
    UINT4               u4L2CxtId = 0;
    UINT4               Nextu4L2CxtId = 0;

    u4L2CxtId = (UINT4) i4FsVcmL2VcId;
    u2VlanId = (UINT2) i4FsVcmVlanId;

    if (VcmGetNextL2CxtToIPIfaceMapIndex (u4L2CxtId, u2VlanId, &Nextu4L2CxtId,
                                          &u2NextVlanId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsVcmL2VcId = (INT4) Nextu4L2CxtId;
    *pi4NextFsVcmVlanId = (INT4) u2NextVlanId;
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVcmL2VcName
 Input       :  The Indices
                FsVcmL2VcId
                FsVcmVlanId

                The Object 
                retValFsVcmL2VcName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVcmL2VcName (INT4 i4FsVcmL2VcId, INT4 i4FsVcmVlanId,
                     tSNMP_OCTET_STRING_TYPE * pRetValFsVcmL2VcName)
{
    UINT1               u1Length = 0;
    UINT1               au1VcAlias[VCM_ALIAS_MAX_LEN];

    UNUSED_PARAM (i4FsVcmVlanId);
    VCM_MEMSET (au1VcAlias, 0, VCM_ALIAS_MAX_LEN);

    if (VcmGetVcAlias ((UINT4) i4FsVcmL2VcId, au1VcAlias) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    u1Length = (UINT1) STRLEN (au1VcAlias);
    MEMCPY (pRetValFsVcmL2VcName->pu1_OctetList, au1VcAlias, u1Length);
    pRetValFsVcmL2VcName->i4_Length = u1Length;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsVcmIPIfIndex
 Input       :  The Indices
                FsVcmL2VcId
                FsVcmVlanId

                The Object 
                retValFsVcmIPIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVcmIPIfIndex (INT4 i4FsVcmL2VcId, INT4 i4FsVcmVlanId,
                      INT4 *pi4RetValFsVcmIPIfIndex)
{
    if (VcmGetIPIfIndexForL2CxtMap
        ((UINT4) i4FsVcmL2VcId, (UINT2) i4FsVcmVlanId,
         (UINT4 *) pi4RetValFsVcmIPIfIndex) == VCM_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : FsVcConfigExtTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsVcConfigExtTable
 Input       :  The Indices
                FsVCId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsVcConfigExtTable (INT4 i4FsVCId)
{
    return (nmhValidateIndexInstanceFsVcmConfigTable (i4FsVCId));

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsVcConfigExtTable
 Input       :  The Indices
                FsVCId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsVcConfigExtTable (INT4 *pi4FsVCId)
{
    return (nmhGetFirstIndexFsVcmConfigTable (pi4FsVCId));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsVcConfigExtTable
 Input       :  The Indices
                FsVCId
                nextFsVCId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsVcConfigExtTable (INT4 i4FsVCId, INT4 *pi4NextFsVCId)
{
    return (nmhGetNextIndexFsVcmConfigTable (i4FsVCId, pi4NextFsVCId));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVcOwner
 Input       :  The Indices
                FsVCId

                The Object 
                retValFsVcOwner
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFsVcOwner (INT4 i4FsVCId, tSNMP_OCTET_STRING_TYPE * pRetValFsVcOwner)
{
    INT4                i4Length = 0;
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo Dummy;

    Dummy.u4VcNum = (UINT4) i4FsVCId;
    if (NULL != (pVcInfo = VcmFindVcEntry (&Dummy)))
    {
        i4Length = (INT4) STRLEN (pVcInfo->au1Owner);
        MEMSET (pRetValFsVcOwner->pu1_OctetList, 0, VCM_OWNER_MAX_SIZE);
        if (i4Length >= (VCM_OWNER_MAX_SIZE - 1))
        {
            i4Length = VCM_OWNER_MAX_SIZE - 1;
        }
        MEMCPY (pRetValFsVcOwner->pu1_OctetList, pVcInfo->au1Owner, i4Length);
        pRetValFsVcOwner->i4_Length = i4Length;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVcOwner
 Input       :  The Indices
                FsVCId

                The Object 
                setValFsVcOwner
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVcOwner (INT4 i4FsVCId, tSNMP_OCTET_STRING_TYPE * pSetValFsVcOwner)
{
    tVirtualContextInfo *pVc = NULL;
    tVirtualContextInfo Dummy;
    Dummy.u4VcNum = (UINT4) i4FsVCId;

    VCM_LOCK ();
    if (NULL != (pVc = VcmFindVcEntry (&Dummy)))
    {
        MEMSET (pVc->au1Owner, 0, VCM_OWNER_MAX_SIZE);
        MEMCPY (pVc->au1Owner,
                pSetValFsVcOwner->pu1_OctetList, pSetValFsVcOwner->i4_Length);
        VCM_UNLOCK ();
        return SNMP_SUCCESS;
    }
    VCM_UNLOCK ();
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVcOwner
 Input       :  The Indices
                FsVCId

                The Object 
                testValFsVcOwner
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVcOwner (UINT4 *pu4ErrorCode, INT4 i4FsVCId,
                    tSNMP_OCTET_STRING_TYPE * pTestValFsVcOwner)
{
    if (pTestValFsVcOwner->i4_Length > (VCM_OWNER_MAX_SIZE - 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

#ifdef SNMP_2_WANTED
    if (SNMPCheckForNVTChars (pTestValFsVcOwner->pu1_OctetList,
                              pTestValFsVcOwner->i4_Length) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

    if (VcmIsContextExist ((UINT4) i4FsVCId) != VCM_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsVcConfigExtTable
 Input       :  The Indices
                FsVCId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVcConfigExtTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsVcmFreeVcIdTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsVcmFreeVcIdTable
 Input       :  The Indices
                FsVcmFreeVcId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsVcmFreeVcIdTable (INT4 i4FsVcmFreeVcId)
{
    if ((i4FsVcmFreeVcId < VCM_DEFAULT_CONTEXT)
        || (i4FsVcmFreeVcId > MAX_CXT_PER_SYS))
    {
        return SNMP_FAILURE;
    }

    if (VcmIsContextExist ((UINT4) i4FsVcmFreeVcId) == VCM_TRUE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsVcmFreeVcIdTable
 Input       :  The Indices
                FsVcmFreeVcId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsVcmFreeVcIdTable (INT4 *pi4FsVcmFreeVcId)
{
    if (gVcmGlobals.u2NextFreeCxtId != 0)    /*checking free context is available */
    {
        *pi4FsVcmFreeVcId = gVcmGlobals.u2NextFreeCxtId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsVcmFreeVcIdTable
 Input       :  The Indices
                FsVcmFreeVcId
                nextFsVcmFreeVcId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsVcmFreeVcIdTable (INT4 i4FsVcmFreeVcId,
                                   INT4 *pi4NextFsVcmFreeVcId)
{
    UINT2               u2Index = 0;

    for (u2Index = (UINT2) (i4FsVcmFreeVcId + 1);
         u2Index <= MAX_CXT_PER_SYS; u2Index++)
    {
        if (VcmIsContextExist (u2Index) != VCM_TRUE)
        {
            *pi4NextFsVcmFreeVcId = u2Index;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsVcmCounterTable
 Input       :  The Indices
                i4FsVCCounterId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsVcmCounterTable (INT4 i4FsVCCounterId)
{
    if ((i4FsVCCounterId < VCM_DEFAULT_CONTEXT)
        || (i4FsVCCounterId > MAX_CXT_PER_SYS))
    {
        return SNMP_FAILURE;
    }

    if (VcmIsContextExist ((UINT4) i4FsVCCounterId) == VCM_FALSE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsVcmCounterTable
 Input       :  The Indices
                FsVCCounterId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsVcmCounterTable (INT4 *pi4FsVCCounterId)
{
    UINT4               u4CxtId = VCM_ZERO;

    if (VcmGetFirstVcId (&u4CxtId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4FsVCCounterId = (INT4) u4CxtId;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsVcmCounterTable
 Input       :  The Indices
                FsVCCounterId
                nextFsVCCounterId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsVcmCounterTable (INT4 i4FsVCCounterId,
                                  INT4 *pi4NextFsVCCounterId)
{

    UINT4               u4CxtId = VCM_ZERO;

    if (VcmGetNextVcId ((UINT4) i4FsVCCounterId, &u4CxtId) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsVCCounterId = (INT4) u4CxtId;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVcCounterRxUnicast
 Input       :  The Indices
                FsVCCounterId

                The Object 
                retValFsVcCounterRxUnicast
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVcCounterRxUnicast (INT4 i4FsVCCounterId,
                            UINT4 *pu4RetValFsVcCounterRxUnicast)
{

#ifdef NPAPI_WANTED
    tVrfCounter         VrfCounter;
    MEMSET (&VrfCounter, VCM_FALSE, sizeof (tVrfCounter));
    VrfCounter.u4VrfId = (UINT4) i4FsVCCounterId;
    VrfCounter.u1StatsType = VRF_GET_UNICAST_COUNTERS;

    if (FNP_FAILURE == VcmFsVcmHwVrfCounter (VrfCounter))
    {
        return SNMP_FAILURE;
    }
    pu4RetValFsVcCounterRxUnicast = VrfCounter.pu4VrfStatsValue;
#else

    UNUSED_PARAM (i4FsVCCounterId);
    *pu4RetValFsVcCounterRxUnicast = VCM_FALSE;

#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsVcCounterRxMulticast
 Input       :  The Indices
                FsVCCounterId

                The Object 
                retValFsVcCounterRxMulticast
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVcCounterRxMulticast (INT4 i4FsVCCounterId,
                              UINT4 *pu4RetValFsVcCounterRxMulticast)
{

#ifdef NPAPI_WANTED

    tVrfCounter         VrfCounter;
    MEMSET (&VrfCounter, VCM_FALSE, sizeof (tVrfCounter));
    VrfCounter.u4VrfId = (UINT4) i4FsVCCounterId;
    VrfCounter.u1StatsType = VRF_GET_MULTICAST_COUNTERS;

    if (FNP_FAILURE == VcmFsVcmHwVrfCounter (VrfCounter))
    {
        return SNMP_FAILURE;
    }
    pu4RetValFsVcCounterRxMulticast = VrfCounter.pu4VrfStatsValue;
#else

    UNUSED_PARAM (i4FsVCCounterId);
    *pu4RetValFsVcCounterRxMulticast = VCM_FALSE;

#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsVcCounterRxBroadcast
 Input       :  The Indices
                FsVCCounterId

                The Object 
                retValFsVcCounterRxBroadcast
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVcCounterRxBroadcast (INT4 i4FsVCCounterId,
                              UINT4 *pu4RetValFsVcCounterRxBroadcast)
{

#ifdef NPAPI_WANTED

    tVrfCounter         VrfCounter;
    MEMSET (&VrfCounter, VCM_FALSE, sizeof (tVrfCounter));
    VrfCounter.u4VrfId = (UINT4) i4FsVCCounterId;
    VrfCounter.u1StatsType = VRF_GET_BROADCAST_COUNTERS;

    if (FNP_FAILURE == VcmFsVcmHwVrfCounter (VrfCounter))
    {
        return SNMP_FAILURE;
    }
    pu4RetValFsVcCounterRxBroadcast = VrfCounter.pu4VrfStatsValue;
#else

    UNUSED_PARAM (i4FsVCCounterId);
    *pu4RetValFsVcCounterRxBroadcast = VCM_FALSE;

#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsVcCounterStatus
 Input       :  The Indices
                FsVCCounterId

                The Object 
                retValFsVcCounterStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVcCounterStatus (INT4 i4FsVCCounterId, INT4 *pi4RetValFsVcCounterStatus)
{
    UINT1               u1Status;
    if (VCM_FAILURE == VcmGetVrfCounterStatus ((UINT4) i4FsVCCounterId,
                                               &u1Status))
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsVcCounterStatus = u1Status;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsVcClearCounter
 Input       :  The Indices
                FsVCCounterId

                The Object 
                retValFsVcClearCounter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVcClearCounter (INT4 i4FsVCCounterId, INT4 *pi4RetValFsVcClearCounter)
{

    UNUSED_PARAM (i4FsVCCounterId);
    *pi4RetValFsVcClearCounter = VCM_FALSE;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVcCounterStatus
 Input       :  The Indices
                FsVCCounterId

                The Object 
                setValFsVcCounterStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVcCounterStatus (INT4 i4FsVCCounterId, INT4 i4SetValFsVcCounterStatus)
{

#ifdef NPAPI_WANTED

    tVrfCounter         VrfCounter;
    UINT1               i1Status = VCM_ZERO;

    MEMSET (&VrfCounter, VCM_FALSE, sizeof (tVrfCounter));
    VcmGetVrfCounterStatus ((UINT4) i4FsVCCounterId, &i1Status);
    if (i4SetValFsVcCounterStatus == i1Status)
    {
        return SNMP_SUCCESS;
    }
    VrfCounter.u4VrfId = (UINT4) i4FsVCCounterId;
    VrfCounter.pu4VrfStatsValue = VCM_FALSE;
    if (VRF_STAT_ENABLE == i4SetValFsVcCounterStatus)
    {
        VrfCounter.u1StatsType = VRF_CREATE_COUNTER;
        if (FNP_FAILURE == VcmFsVcmHwVrfCounter (VrfCounter))
        {
            CLI_SET_ERR (CLI_VRF_STAT_SET);
            return SNMP_FAILURE;
        }
    }
    else if (VRF_STAT_DISABLE == i4SetValFsVcCounterStatus)
    {
        VrfCounter.u1StatsType = VRF_DELETE_COUNTER;

        if (FNP_FAILURE == VcmFsVcmHwVrfCounter (VrfCounter))
        {
            CLI_SET_ERR (CLI_VRF_STAT_SET);
            return SNMP_FAILURE;
        }

    }
    VcmSetVrfCounterStatus (i4FsVCCounterId, (UINT1) i4SetValFsVcCounterStatus);

#else

    UNUSED_PARAM (i4FsVCCounterId);
    UNUSED_PARAM (i4SetValFsVcCounterStatus);

#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsVcClearCounter
 Input       :  The Indices
                FsVCCounterId

                The Object 
                setValFsVcClearCounter
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVcClearCounter (INT4 i4FsVCCounterId, INT4 i4SetValFsVcClearCounter)
{

#ifdef NPAPI_WANTED

    tVrfCounter         VrfCounter;
    UINT1               i1Status = VCM_ZERO;
    MEMSET (&VrfCounter, VCM_FALSE, sizeof (tVrfCounter));
    VrfCounter.u4VrfId = (UINT4) i4FsVCCounterId;
    VrfCounter.u1StatsType = VRF_RESET_COUNTER;
    VrfCounter.pu4VrfStatsValue = VCM_FALSE;
    VcmGetVrfCounterStatus ((UINT4) i4FsVCCounterId, &i1Status);
    if ((VCM_TRUE == i4SetValFsVcClearCounter) && (VRF_STAT_ENABLE == i1Status))
    {
        if (FNP_FAILURE == VcmFsVcmHwVrfCounter (VrfCounter))
        {
            CLI_SET_ERR (CLI_VRF_STAT_CLEAR);
            return SNMP_FAILURE;
        }
    }
#else
    UNUSED_PARAM (i4FsVCCounterId);
    UNUSED_PARAM (i4SetValFsVcClearCounter);
#endif
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVcCounterStatus
 Input       :  The Indices
                FsVCCounterId

                The Object 
                testValFsVcCounterStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVcCounterStatus (UINT4 *pu4ErrorCode,
                            INT4 i4FsVCCounterId,
                            INT4 i4TestValFsVcCounterStatus)
{
    INT4                i4ContextType = 0;

    if ((i4FsVCCounterId < VCM_DEFAULT_CONTEXT) ||
        (i4FsVCCounterId > MAX_CXT_PER_SYS))
    {
        CLI_SET_ERR (CLI_VCM_INVLAID_VC_ID);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (VcmIsContextExist ((UINT4) i4FsVCCounterId) != VCM_TRUE)
    {
        CLI_SET_ERR (CLI_VCM_VC_ERROR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    VcmGetVcCxtType ((UINT4) i4FsVCCounterId, &i4ContextType);
    if ((i4ContextType != VCM_L3_CONTEXT) &&
        (i4ContextType != VCM_L2_L3_CONTEXT))
    {
        CLI_SET_ERR (CLI_VCM_L3IFACE_L2CXT_MAP_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((VRF_STAT_ENABLE != i4TestValFsVcCounterStatus) &&
        (VRF_STAT_DISABLE != i4TestValFsVcCounterStatus))
    {
        CLI_SET_ERR (CLI_VRF_COUNTER_STATUS);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsVcClearCounter
 Input       :  The Indices
                FsVCCounterId

                The Object 
                testValFsVcClearCounter
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVcClearCounter (UINT4 *pu4ErrorCode,
                           INT4 i4FsVCCounterId, INT4 i4TestValFsVcClearCounter)
{

    INT4                i4ContextType = VCM_ZERO;

    if ((i4FsVCCounterId < VCM_DEFAULT_CONTEXT) ||
        (i4FsVCCounterId > MAX_CXT_PER_SYS))
    {
        CLI_SET_ERR (CLI_VCM_INVLAID_VC_ID);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (VcmIsContextExist ((UINT4) i4FsVCCounterId) != VCM_TRUE)
    {
        CLI_SET_ERR (CLI_VCM_VC_ERROR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    VcmGetVcCxtType ((UINT4) i4FsVCCounterId, &i4ContextType);
    if ((i4ContextType != VCM_L3_CONTEXT) &&
        (i4ContextType != VCM_L2_L3_CONTEXT))
    {
        CLI_SET_ERR (CLI_VCM_L3IFACE_L2CXT_MAP_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (VRF_RESET_COUNTER != i4TestValFsVcClearCounter)
    {
        CLI_SET_ERR (CLI_VRF_COUNTER_RESET);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsVcmCounterTable
 Input       :  The Indices
                FsVCCounterId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVcmCounterTable (UINT4 *pu4ErrorCode,
                           tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
