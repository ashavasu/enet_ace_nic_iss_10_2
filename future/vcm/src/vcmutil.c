/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* $Id: vcmutil.c,v 1.24 2017/11/14 07:31:19 siva Exp $                                                    */
/* Licensee Aricent Inc., 2004-2005                                          */
/*****************************************************************************/
/*    FILE  NAME            : vcmutil.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : VCM module                                     */
/*    MODULE NAME           : VCM module                                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 16 Jun 2006                                    */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains the utility functions used  */
/*                            by VCM module.                                 */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    16 JUN 2006 / MITeam   Initial Create.                         */
/*---------------------------------------------------------------------------*/
#ifndef _VCMUTIL_C
#define _VCMUTIL_C

#include "vcminc.h"
#include  "fssnmp.h"
PUBLIC tSNMP_OID_TYPE *alloc_oid (INT4 i4Size);
PUBLIC tSNMP_OID_TYPE *SNMP_AGT_GetOidFromString (INT1 *pi1_str);
PUBLIC tSNMP_OCTET_STRING_TYPE *SNMP_AGT_FormOctetString (UINT1 *u1_string,
                                                          INT4 i4_length);
PUBLIC VOID         SNMP_FreeOid (tSNMP_OID_TYPE * pOidPtr);
PUBLIC VOID         SNMP_AGT_FreeVarBindList (tSNMP_VAR_BIND * pVarBindLst);
PUBLIC tSNMP_VAR_BIND *SNMP_AGT_FormVarBind (tSNMP_OID_TYPE * oid,
                                             INT2 i2_type,
                                             UINT4 u4_ul_value,
                                             INT4 i4_sl_value,
                                             tSNMP_OCTET_STRING_TYPE * os_value,
                                             tSNMP_OID_TYPE * oid_value,
                                             tSNMP_COUNTER64_TYPE
                                             u8_Counter64Value);
PUBLIC tSNMP_OID_TYPE *VcmMakeObjIdFromDotNew PROTO ((UINT1 *));
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmUtilGetIfIndexFromLocalPort                     */
/*                                                                           */
/*     DESCRIPTION      : This function will return the Interface Index      */
/*                        from the given Localport and Context-Id.           */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                        u2LocalPort    - LocalPort.                        */
/*                                                                           */
/*     OUTPUT           : pu4IfIndex     - Interface Index.                  */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmUtilGetIfIndexFromLocalPort (UINT4 u4ContextId, UINT2 u2LocalPort,
                                UINT4 *pu4IfIndex)
{
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo Dummy;
    tVcPortEntry       *pTempPort = NULL;

    Dummy.u4VcNum = u4ContextId;

    VCM_LOCK ();

    pVcInfo = VcmFindVcEntry (&Dummy);

    if (NULL == pVcInfo)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC | VCM_OS_RESOURCE_TRC,
                 "VcmGetIfIndexFromLocalPort: Invalid Context-Id\n");
        VCM_UNLOCK ();
        return VCM_FAILURE;
    }

    TMO_SLL_Scan (&pVcInfo->VcPortList, pTempPort, tVcPortEntry *)
    {
        if (pTempPort->pIf->u2HlPortId == u2LocalPort)
        {
            *pu4IfIndex = pTempPort->pIf->u4IfIndex;
            VCM_UNLOCK ();
            return VCM_SUCCESS;
        }
    }

    VCM_TRC (VCM_ALL_FAILURE_TRC | VCM_OS_RESOURCE_TRC,
             "VcmGetIfIndexFromLocalPort: LocalPort not found\n");
    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetNextAvailableVcNum                           */
/*                                                                           */
/*     DESCRIPTION      : This function will give the next context id        */
/*                        availble in the system.                            */
/*                                                                           */
/*     INPUT            : None.                                              */
/*                                                                           */
/*     OUTPUT           : pu4FreeVcNum - Next Free Context-Id in the switch  */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetNextAvailableVcNum (UINT4 *pu4FreeVcNum)
{
    UINT2               u2Index = 0;
    UINT2               u2MaxCtx = MAX_CXT_PER_SYS;

    if (gVcmGlobals.u2NextFreeCxtId == VCM_INVALID_VAL)
    {
        /* This scenario will occur, after creating all the context
         * if some deletes the context and create a new context */
        for (u2Index = 1; u2Index < u2MaxCtx; u2Index++)
        {
            if (VcmIsContextExist (u2Index) != VCM_TRUE)
            {
                gVcmGlobals.u2NextFreeCxtId = u2Index;
                *pu4FreeVcNum = u2Index;
                return VCM_SUCCESS;
            }
        }
        return VCM_FAILURE;
    }
    *pu4FreeVcNum = gVcmGlobals.u2NextFreeCxtId;
    return VCM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetFirstVcId                                    */
/*                                                                           */
/*     DESCRIPTION      : This function will return the First Context-Id     */
/*                        present in the system.                             */
/*                                                                           */
/*     INPUT            : None.                                              */
/*                                                                           */
/*     OUTPUT           : pu4ContextId - Context-Id.                         */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetFirstVcId (UINT4 *pu4ContextId)
{
    tVirtualContextInfo *pVcInfo = NULL;

    VCM_LOCK ();

    if (NULL != (pVcInfo = VcmGetFirstVc ()))
    {
        *pu4ContextId = pVcInfo->u4VcNum;

        VCM_UNLOCK ();

        VCM_TRC (VCM_CONTROL_PATH_TRC,
                 "VcmGetFirstVcId obtained the First Context-Id present in the system and"
                 " returns success \n");
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();

    VCM_TRC (VCM_ALL_FAILURE_TRC, "VcmGetFirstVcId Failed \n");

    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetNextVcId                                     */
/*                                                                           */
/*     DESCRIPTION      : This function will return the Next Context-Id      */
/*                        present in the system.                             */
/*                                                                           */
/*     INPUT            : u4ContextId      - Context-Id.                     */
/*                                                                           */
/*     OUTPUT           : pu4NextContextId - Next Context-Id.                */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetNextVcId (UINT4 u4ContextId, UINT4 *pu4NextContextId)
{
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo Dummy;

    Dummy.u4VcNum = u4ContextId;

    VCM_LOCK ();

    if (NULL != (pVcInfo = VcmGetNextVc (&Dummy)))
    {
        *pu4NextContextId = pVcInfo->u4VcNum;

        VCM_UNLOCK ();
        VCM_TRC (VCM_CONTROL_PATH_TRC,
                 "VcmGetNextVcId obtained the Next Context-Id present in the system and"
                 " returns Success \n");
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    VCM_TRC (VCM_ALL_FAILURE_TRC, "VcmGetNextVcId Failed \n");
    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetVcNextFreeLocalPortId                        */
/*                                                                           */
/*     DESCRIPTION      : This function will return the Next free local      */
/*                        port Id in the context.                            */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : pu2LocalPortId - Next free local port Id.          */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetVcNextFreeHlPortId (UINT4 u4ContextId, UINT2 *pu2LocalPortId)
{
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo Dummy;

    Dummy.u4VcNum = u4ContextId;

    VCM_LOCK ();

    if (NULL != (pVcInfo = VcmFindVcEntry (&Dummy)))
    {
        *pu2LocalPortId = VCM_NEXTFREE_HLPORTID (pVcInfo);

        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetVcAlias                                      */
/*                                                                           */
/*     DESCRIPTION      : This function will return the alias name of the    */
/*                        given context.                                     */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : pu1Alias       - Alias name.                       */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetVcAlias (UINT4 u4ContextId, UINT1 *pu1Alias)
{
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo Dummy;

    VCM_MEMSET (pu1Alias, 0, VCM_ALIAS_MAX_LEN);

    Dummy.u4VcNum = u4ContextId;

    VCM_LOCK ();

    if (NULL != (pVcInfo = VcmFindVcEntry (&Dummy)))
    {
        VCM_MEMCPY (pu1Alias, pVcInfo->au1Alias, VCM_ALIAS_MAX_LEN);

        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetVcRowStatus                                  */
/*                                                                           */
/*     DESCRIPTION      : This function will return the rowstatus of the     */
/*                        given context.                                     */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : pi4RowStatus   - RowStatus.                        */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetVcRowStatus (UINT4 u4ContextId, INT4 *pi4RowStatus)
{
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo Dummy;

    Dummy.u4VcNum = u4ContextId;

    VCM_LOCK ();

    if (NULL != (pVcInfo = VcmFindVcEntry (&Dummy)))
    {
        *pi4RowStatus = (INT4) pVcInfo->u1RowStatus;

        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetVcCfgCxtType                                 */
/*                                                                           */
/*     DESCRIPTION      : This function will return the configured context   */
/*                        type for the given context.                        */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : pi4CfgCxtType  - Configured Context type           */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetVcCfgCxtType (UINT4 u4ContextId, INT4 *pi4CfgCxtType)
{
    tVirtualContextInfo Dummy;
    tVirtualContextInfo *pVcInfo = NULL;

    *pi4CfgCxtType = 0;
    Dummy.u4VcNum = u4ContextId;

    VCM_LOCK ();

    pVcInfo = VcmFindVcEntry (&Dummy);
    if (pVcInfo != NULL)
    {
        *pi4CfgCxtType = (INT4) pVcInfo->u1CfgCxtType;
        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmIsContextExist                                  */
/*                                                                           */
/*     DESCRIPTION      : This function will return whether the given        */
/*                        context exist or not.                              */
/*                                                                           */
/*     INPUT            : u4ContextId - Context-Id                           */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VCM_TRUE / VCM_FALSE                               */
/*                                                                           */
/*****************************************************************************/
INT4
VcmIsContextExist (UINT4 u4ContextId)
{
    tVirtualContextInfo Dummy;

    Dummy.u4VcNum = u4ContextId;

    VCM_LOCK ();

    if (NULL != VcmFindVcEntry (&Dummy))
    {
        VCM_UNLOCK ();
        return VCM_TRUE;
    }

    VCM_UNLOCK ();
    return VCM_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmIsIfMapExist                                    */
/*                                                                           */
/*     DESCRIPTION      : This function will check whether the mapping entry */
/*                        is exist for the given Interface.                  */
/*                                                                           */
/*     INPUT            : u4IfIndex      - Interface.                        */
/*                                                                           */
/*     OUTPUT           : None.                                              */
/*                                                                           */
/*     RETURNS          : VCM_TRUE / VCM_FALSE                               */
/*                                                                           */
/*****************************************************************************/
INT4
VcmIsIfMapExist (UINT4 u4IfIndex)
{
    tVcmIfMapEntry      Dummy;

    Dummy.u4IfIndex = u4IfIndex;

    VCM_LOCK ();

    if (NULL != VcmFindIfMapEntry (&Dummy))
    {
        VCM_UNLOCK ();
        return VCM_TRUE;
    }

    VCM_UNLOCK ();
    return VCM_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetFirstIfMapIfIndex                            */
/*                                                                           */
/*     DESCRIPTION      : This function will return first interface in the   */
/*                        IfMap table.                                       */
/*                                                                           */
/*     INPUT            : None.                                              */
/*                                                                           */
/*     OUTPUT           : pu4IfIndex     - Interface Index.                  */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetFirstIfMapIfIndex (UINT4 *pu4IfIndex)
{
    tVcmIfMapEntry     *pIfMap = NULL;

    VCM_LOCK ();

    if (NULL != (pIfMap = VcmGetFirstIfMap ()))
    {
        *pu4IfIndex = pIfMap->u4IfIndex;

        /* Though SISP ports are existing in the IfMap table, it can't ever
         * be returned as First port, because if SISP ports are existing
         * atleast one Physical port will be existing. So there is no need to 
         * check for SISP port or not. */

        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetNextIfMapIfIndex                             */
/*                                                                           */
/*     DESCRIPTION      : This function will return next  interface in the   */
/*                        IfMap table.                                       */
/*                                                                           */
/*     INPUT            : u4IfIndex      - Interface Index.                  */
/*                                                                           */
/*     OUTPUT           : pu4NextIfIndex - Next Interface Index.             */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetNextIfMapIfIndex (UINT4 u4IfIndex, UINT4 *pu4NextIfIndex)
{
    tVcmIfMapEntry     *pIfMap = NULL;
    tVcmIfMapEntry      Dummy;

    Dummy.u4IfIndex = u4IfIndex;

    VCM_LOCK ();

    if (NULL != (pIfMap = VcmGetNextIfMap (&Dummy)))
    {
        *pu4NextIfIndex = pIfMap->u4IfIndex;

        /* Get next should not return SISP ports */
        if (SISP_IS_LOGICAL_PORT (*pu4NextIfIndex) == VCM_TRUE)
        {
            /* Once SISP Ports are encountered, there will not be any more 
             * physical ports, Because IfIndex range is fixed, SISP IfIndices 
             * will always come after Physical IfIndices.
             * Simply return FAILURE */

            /* But there can be VIP interfaces after SISP */
            MEMSET (&Dummy, 0, sizeof (tVcmIfMapEntry));

            Dummy.u4IfIndex = CFA_MIN_VIP_IF_INDEX - 1;

            if (NULL == (pIfMap = VcmGetNextIfMap (&Dummy)))
            {
                VCM_UNLOCK ();
                return VCM_FAILURE;
            }

            *pu4NextIfIndex = pIfMap->u4IfIndex;
        }

        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetIfMapEntry                                   */
/*                                                                           */
/*     DESCRIPTION      : This function will return IfMap entry node for the */
/*                        given interface.                                   */
/*                                                                           */
/*     INPUT            : u4IfIndex      - Interface Index.                  */
/*                                                                           */
/*     OUTPUT           : pVcmIfMapEntry - Copy of IfMapEntry node.          */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetIfMapEntry (UINT4 u4IfIndex, tVcmIfMapEntry * pVcmIfMapEntry)
{
    tVcmIfMapEntry     *pIfMap = NULL;
    tVcmIfMapEntry      Dummy;

    Dummy.u4IfIndex = u4IfIndex;

    VCM_LOCK ();

    if (NULL != (pIfMap = VcmFindIfMapEntry (&Dummy)))
    {
        VCM_MEMCPY (pVcmIfMapEntry, pIfMap, sizeof (tVcmIfMapEntry));

        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetIfMapEntryFromLocalPort                      */
/*                                                                           */
/*     DESCRIPTION      : This function will return IfMap entry node for the */
/*                        given interface from the local port.               */
/*                                                                           */
/*     INPUT            : u2LocalPort    - Local Port Identifier             */
/*                        u4ContextId    - Context Identifier                */
/*                                                                           */
/*     OUTPUT           : pVcmIfMapEntry - Copy of IfMapEntry node.          */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
tVcmIfMapEntry     *
VcmGetIfMapEntryFromLocalPort (UINT4 u4ContextId, UINT2 u2LocalPort)
{
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo Dummy;
    tVcPortEntry       *pTempPort = NULL;

    Dummy.u4VcNum = u4ContextId;

    pVcInfo = VcmFindVcEntry (&Dummy);

    if (NULL == pVcInfo)
    {
        VCM_TRC (VCM_ALL_FAILURE_TRC,
                 "VcmGetIfMapEntryFromLocalPort: Invalid Context-Id\n");
        return NULL;
    }

    TMO_SLL_Scan (&pVcInfo->VcPortList, pTempPort, tVcPortEntry *)
    {
        if (pTempPort->pIf->u2HlPortId == u2LocalPort)
        {
            return pTempPort->pIf;
        }
    }
    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetIfMapVcId                                    */
/*                                                                           */
/*     DESCRIPTION      : This function will return Context-id which this    */
/*                        interface is mapped.                               */
/*                                                                           */
/*     INPUT            : u4IfIndex      - Interface Index.                  */
/*                                                                           */
/*     OUTPUT           : pu4ContextId   - Context Id.                       */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetIfMapVcId (UINT4 u4IfIndex, UINT4 *pu4ContextId)
{
    tVcmIfMapEntry     *pIfMap = NULL;
    tVcmIfMapEntry      Dummy;

    Dummy.u4IfIndex = u4IfIndex;

    VCM_LOCK ();

    if (NULL != (pIfMap = VcmFindIfMapEntry (&Dummy)))
    {
        *pu4ContextId = pIfMap->u4VcNum;

        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetIfMapHlPortId                                */
/*                                                                           */
/*     DESCRIPTION      : This function will return corresponding Local port */
/*                        Id for this Interface.                             */
/*                                                                           */
/*     INPUT            : u4IfIndex      - Interface Index.                  */
/*                                                                           */
/*     OUTPUT           : pu2LocalPortId - Local Port Id.                    */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetIfMapHlPortId (UINT4 u4IfIndex, UINT2 *pu2LocalPortId)
{
    tVcmIfMapEntry     *pIfMap = NULL;
    tVcmIfMapEntry      Dummy;

    Dummy.u4IfIndex = u4IfIndex;

    VCM_LOCK ();

    if (NULL != (pIfMap = VcmFindIfMapEntry (&Dummy)))
    {
        *pu2LocalPortId = pIfMap->u2HlPortId;

        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetIfMapStatus                                  */
/*                                                                           */
/*     DESCRIPTION      : This function will return the Rowstatus of the     */
/*                        Interface.                                         */
/*                                                                           */
/*     INPUT            : u4IfIndex      - Interface Index.                  */
/*                                                                           */
/*     OUTPUT           : pi4RowStatus   - RowStatus.                        */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetIfMapStatus (UINT4 u4IfIndex, INT4 *pi4Status)
{
    tVcmIfMapEntry     *pIfMap = NULL;
    tVcmIfMapEntry      Dummy;

    Dummy.u4IfIndex = u4IfIndex;

    VCM_LOCK ();

    if (NULL != (pIfMap = VcmFindIfMapEntry (&Dummy)))
    {
        *pi4Status = pIfMap->u1RowStatus;

        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetL2ContextId                                  */
/*                                                                           */
/*     DESCRIPTION      : This function will return the l2context to which   */
/*                        the specified interface is mapped. This mapping    */
/*                        will be useful for IVR interfaces only. For other  */
/*                        interfaces, l2context id will be zero.             */
/*                                                                           */
/*     INPUT            : u4IfIndex      - Interface Index.                  */
/*                                                                           */
/*     OUTPUT           : pi4L2CxtId     - The l2 context id.                */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetL2ContextId (UINT4 u4IfIndex, INT4 *pi4L2CxtId)
{
    tVcmIfMapEntry      Dummy;
    tVcmIfMapEntry     *pIfMap = NULL;

    Dummy.u4IfIndex = u4IfIndex;

    VCM_LOCK ();

    pIfMap = VcmFindIfMapEntry (&Dummy);
    if (pIfMap != NULL)
    {
        *pi4L2CxtId = (INT4) pIfMap->u4L2ContextId;

        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmSetIfMapVcId                                    */
/*                                                                           */
/*     DESCRIPTION      : This function will set the Context-id for the      */
/*                        interface.                                         */
/*                                                                           */
/*     INPUT            : u4IfIndex      - Interface Index.                  */
/*                                                                           */
/*     OUTPUT           : u4ContextId    - Context Id.                       */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmSetIfMapVcId (UINT4 u4IfIndex, UINT4 u4ContextId)
{
    tVcmIfMapEntry     *pIfMap = NULL;
    tVcmIfMapEntry      Dummy;

    Dummy.u4IfIndex = u4IfIndex;

    VCM_LOCK ();

    if (NULL != (pIfMap = VcmFindIfMapEntry (&Dummy)))
    {
        pIfMap->u4CfgVcNum = u4ContextId;

        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmSetIfMapStatus                                  */
/*                                                                           */
/*     DESCRIPTION      : This function will set the RowStatus for the       */
/*                        interface.                                         */
/*                                                                           */
/*     INPUT            : u4IfIndex      - Interface Index.                  */
/*                                                                           */
/*     OUTPUT           : i4RowStatus    - RowStatus.                        */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmSetIfMapStatus (UINT4 u4IfIndex, INT4 i4RowStatus)
{
    tVcmIfMapEntry     *pIfMap = NULL;
    tVcmIfMapEntry      Dummy;

    Dummy.u4IfIndex = u4IfIndex;

    VCM_LOCK ();

    if (NULL != (pIfMap = VcmFindIfMapEntry (&Dummy)))
    {
        pIfMap->u1RowStatus = (UINT1) i4RowStatus;
        pIfMap->u4VcNum = pIfMap->u4CfgVcNum;

        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmIncrementPortCount                              */
/*                                                                           */
/*     DESCRIPTION      : This function will increament the portcount for    */
/*                        the given context.                                 */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : None.                                              */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmIncrementPortCount (UINT4 u4ContextId)
{
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo Dummy;

    Dummy.u4VcNum = u4ContextId;

    VCM_LOCK ();

    if (NULL != (pVcInfo = VcmFindVcEntry (&Dummy)))
    {
        pVcInfo->u2PortCount++;

        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmDecrementPortCount                              */
/*                                                                           */
/*     DESCRIPTION      : This function will decreament the portcount for    */
/*                        the given context.                                 */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : None.                                              */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmDecrementPortCount (UINT4 u4ContextId)
{
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo Dummy;

    Dummy.u4VcNum = u4ContextId;

    VCM_LOCK ();

    if (NULL != (pVcInfo = VcmFindVcEntry (&Dummy)))
    {
        pVcInfo->u2PortCount--;

        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetVcPortCount                                  */
/*                                                                           */
/*     DESCRIPTION      : This function will return the number of ports      */
/*                        mapped for the given context.                      */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : pu2PortCount   - No. of port in context.           */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetVcPortCount (UINT4 u4ContextId, UINT2 *pu2PortCount)
{
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo Dummy;

    Dummy.u4VcNum = u4ContextId;

    VCM_LOCK ();

    if (NULL != (pVcInfo = VcmFindVcEntry (&Dummy)))
    {
        *pu2PortCount = pVcInfo->u2PortCount;

        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetVrfCounterStatus                             */
/*                                                                           */
/*     DESCRIPTION      : This function will return the VRF counter status   */
/*                        mapped for the given context.                      */
/*                                                                           */
/*     INPUT            : u4ContextId      -  Context-Id.                    */
                                                                                                                       /*              pu1CounterStatus -  Counter Status             *//*                                       */
/*     OUTPUT           : get the VRF counter status                             */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetVrfCounterStatus (UINT4 u4ContextId, UINT1 *pu1CounterStatus)
{
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo Dummy;

    Dummy.u4VcNum = u4ContextId;

    VCM_LOCK ();

    if (NULL != (pVcInfo = VcmFindVcEntry (&Dummy)))
    {
        *pu1CounterStatus = pVcInfo->u1VrfCounter;

        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmSetVrfCounterStatus                             */
/*                                                                           */
/*     DESCRIPTION      : This function will set  the VRF counter status     */
/*                        mapped for the given context.                      */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*              u1Status     - Counter status                */
/*                                                                           */
/*     OUTPUT           : None.                                    */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmSetVrfCounterStatus (UINT4 u4ContextId, UINT1 u1Status)
{
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo Dummy;

    Dummy.u4VcNum = u4ContextId;

    VCM_LOCK ();

    if (NULL != (pVcInfo = VcmFindVcEntry (&Dummy)))
    {
        pVcInfo->u1VrfCounter = u1Status;

        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    return VCM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmIncrementSispPortCount                          */
/*                                                                           */
/*     DESCRIPTION      : This function will increament the SISP portcount   */
/*                        for the given context.                             */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : None.                                              */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmIncrementSispPortCount (UINT4 u4ContextId)
{
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo Dummy;

    Dummy.u4VcNum = u4ContextId;

    VCM_LOCK ();

    if (NULL != (pVcInfo = VcmFindVcEntry (&Dummy)))
    {
        pVcInfo->u4SispPortCount++;
        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmDecrementSispPortCount                          */
/*                                                                           */
/*     DESCRIPTION      : This function will decreament the SISP portcount   */
/*                        for the given context.                             */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : None.                                              */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmDecrementSispPortCount (UINT4 u4ContextId)
{
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo Dummy;

    Dummy.u4VcNum = u4ContextId;

    VCM_LOCK ();

    if (NULL != (pVcInfo = VcmFindVcEntry (&Dummy)))
    {
        pVcInfo->u4SispPortCount--;
        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetIpIfCount                                    */
/*                                                                           */
/*     DESCRIPTION      : This function will return the number of IP         */
/*                        interfaces mapped to the given context.            */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : pu2PortCount   - No. of interfaces in context.     */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetIpIfCount (UINT4 u4ContextId, UINT2 *pu2PortCount)
{
    tVirtualContextInfo *pVcInfo = NULL;
    tVirtualContextInfo Dummy;

    Dummy.u4VcNum = u4ContextId;

    VCM_LOCK ();

    pVcInfo = VcmFindVcEntry (&Dummy);
    if (pVcInfo != NULL)
    {
        *pu2PortCount = (UINT2) (TMO_SLL_Count (&pVcInfo->VcIPIntfList));
        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/* Function Name      : VcmIsMaxL3CxtCreated                                 */
/*                                                                           */
/* Description        : This function is used to check if the system already */
/*                      have max l3 context created or not                   */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_TRUE if max l3 context is created                */
/*                    : VCM_FALSE if max l3 context is not created           */
/*                                                                           */
/*****************************************************************************/
INT4
VcmIsMaxL3CxtCreated (VOID)
{
    if (gVcmGlobals.u2L3ContextCount < VCM_MAX_L3_CONTEXT)
    {
        return VCM_FALSE;
    }
    return VCM_TRUE;
}

/*****************************************************************************/
/* Function Name      : VcmIsL3IfMappedToL2Cxt                               */
/*                                                                           */
/* Description        : This function is used to check if any l3 interface   */
/*                      is mapped to the specified l2 context                */
/*                                                                           */
/* Input(s)           : u4ContextId - The l2 context id                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_TRUE if l3 interface is mapped                   */
/*                    : VCM_FALSE if l3 interface is not mapped              */
/*                                                                           */
/*****************************************************************************/
INT4
VcmIsL3IfMappedToL2Cxt (UINT4 u4ContextId)
{
    tVcmIfMapEntry     *pIfMap = NULL;
    tVcmIfMapEntry      Dummy;

    VCM_LOCK ();
    pIfMap = VcmGetFirstIfMap ();

    while (pIfMap != NULL)
    {
        Dummy.u4IfIndex = pIfMap->u4IfIndex;
        if (pIfMap->u4L2ContextId == u4ContextId)
        {
            /* An interface is mapped to the l2context */
            VCM_UNLOCK ();
            return VCM_TRUE;
        }
        pIfMap = VcmGetNextIfMap (&Dummy);
    }

    VCM_UNLOCK ();
    return VCM_FALSE;

}

/****************************************************************************
 *  Function    :  VcmIpInit
 *  Description :  Initializes all the instatnces of the structure
 *  Input       :  none
 *  Output      :  none
 *  Returns     :  none
 * ****************************************************************************/

PUBLIC VOID
VcmIpInit (VOID)
{
    UINT4               u4IpIfIndex = 0;

    while (u4IpIfIndex < VCM_MAX_IP_INTERFACES)
    {
        gapVcmIpIfMapEntry[u4IpIfIndex] = NULL;
        u4IpIfIndex++;
    }
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetFirstL2CxtToIPIfaceMapIndex                  */
/*                                                                           */
/*     DESCRIPTION      : This function will return first entry in the       */
/*                        FsVcmL2CxtAndVlanToIPIfaceMapTable.                */
/*                                                                           */
/*     INPUT            : None.                                              */
/*                                                                           */
/*     OUTPUT           : pu4L2CxtId  - Pointer to l2 context id             */
/*                       *pu2VlanId   - Pointer to vlan identifier           */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetFirstL2CxtToIPIfaceMapIndex (UINT4 *pu4L2CxtId, UINT2 *pu2VlanId)
{
    tVcmIfMapEntry     *pIfMap = NULL;

    VCM_LOCK ();

    if (NULL != (pIfMap = VcmGetFirstCxtToIpIfMapEntry ()))
    {
        *pu4L2CxtId = pIfMap->u4L2ContextId;
        *pu2VlanId = pIfMap->u2VlanId;

        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetNextL2CxtToIPIfaceMapIndex                   */
/*                                                                           */
/*     DESCRIPTION      : This function will return next entry in the        */
/*                        FsVcmL2CxtAndVlanToIPIfaceMapTable.                */
/*                                                                           */
/*     INPUT            : u4L2CxtId - ContextId                              */
/*                        VlanId - Vlan identifier                           */
/*                                                                           */
/*     OUTPUT           : pu4L2CxtId  - Pointer to l2 context id             */
/*                       *pVlanId     - Pointer to vlan identifier           */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetNextL2CxtToIPIfaceMapIndex (UINT4 u4L2CxtId, UINT2 u2VlanId,
                                  UINT4 *pu4L2CxtId, UINT2 *pu2VlanId)
{
    tVcmIfMapEntry      DummyIfMap;
    tVcmIfMapEntry     *pIfMap = NULL;

    DummyIfMap.u4L2ContextId = u4L2CxtId;
    DummyIfMap.u2VlanId = u2VlanId;

    VCM_LOCK ();

    if (NULL != (pIfMap = VcmGetNextCxtToIpIfMapEntry (&DummyIfMap)))
    {
        *pu4L2CxtId = pIfMap->u4L2ContextId;
        *pu2VlanId = pIfMap->u2VlanId;

        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmIsL2CxtToIPIfMapExist                           */
/*                                                                           */
/*     DESCRIPTION      : This function will check whether the mapping entry */
/*                        is exist for the given l2context and vlan id       */
/*                                                                           */
/*     INPUT            : u4L2CxtId      - L2 context id                     */
/*                        u2VlanId       - L2 vlan id                        */
/*                                                                           */
/*     OUTPUT           : None.                                              */
/*                                                                           */
/*     RETURNS          : VCM_TRUE / VCM_FALSE                               */
/*                                                                           */
/*****************************************************************************/
INT4
VcmIsL2CxtToIPIfMapExist (UINT4 u4L2CxtId, UINT2 u2VlanId)
{
    tVcmIfMapEntry      Dummy;

    Dummy.u4L2ContextId = u4L2CxtId;
    Dummy.u2VlanId = u2VlanId;

    VCM_LOCK ();

    if (NULL != VcmFindL2CxtToIPIfMapEntry (&Dummy))
    {
        VCM_UNLOCK ();
        return VCM_TRUE;
    }

    VCM_UNLOCK ();
    return VCM_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetIPIfIndexForL2CxtMap                         */
/*                                                                           */
/*     DESCRIPTION      : This function will return the IP interface index   */
/*                        which is mapped to the l2context and vlan id       */
/*                                                                           */
/*     INPUT            : u4L2CxtId      - L2 context id                     */
/*                        u2VlanId       - L2 vlan id                        */
/*                                                                           */
/*     OUTPUT           : pu4IfIndex - cfa ifIndex of the IP interface       */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetIPIfIndexForL2CxtMap (UINT4 u4L2CxtId, UINT2 u2VlanId, UINT4 *pu4IfIndex)
{
    tVcmIfMapEntry     *pIfMap = NULL;
    tVcmIfMapEntry      Dummy;

    Dummy.u4L2ContextId = u4L2CxtId;
    Dummy.u2VlanId = u2VlanId;

    VCM_LOCK ();

    if (NULL != (pIfMap = VcmFindL2CxtToIPIfMapEntry (&Dummy)))
    {
        *pu4IfIndex = pIfMap->u4IfIndex;
        VCM_UNLOCK ();
        return VCM_SUCCESS;
    }

    VCM_UNLOCK ();
    return VCM_FAILURE;

}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmUpdatePortToContextInHw                         */
/*                                                                           */
/*     DESCRIPTION      : This function will map the given port to the       */
/*                        given context in the hardware                      */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                        u4IfIndex      - Port Identifier                   */
/*                        u1Status       - Indicated MAP or UNMAP from hw    */
/*                                         VCM_TRUE  - Map to HW             */
/*                                         VCM_FALSE - UnMap from HW         */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmUpdatePortToContextInHw (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1Status)
{
    tContextMapInfo     ContextMapInfo;
    tVcmIfMapEntry      VcmIfMapEntry;

    if (VCM_NODE_STATUS () != VCM_RED_ACTIVE)
    {
        return VCM_SUCCESS;
    }

    if (VcmGetIfMapEntry (u4IfIndex, &VcmIfMapEntry) == VCM_FAILURE)
    {
        /* Port not mapped - hence return */
        return VCM_FAILURE;
    }

    if (u1Status == VCM_TRUE)    /* Add to Hardware */
    {
        MEMSET (&ContextMapInfo, 0, sizeof (tContextMapInfo));

        if (VcmFsVcmHwMapPortToContext (u4ContextId, VcmIfMapEntry.u4PhyIfIndex)
            == FNP_FAILURE)
        {
            return VCM_FAILURE;
        }
        ContextMapInfo.u2LocalPortId = VcmIfMapEntry.u2HlPortId;

        /* For the physical port primary context has to be set as TRUE */
        ContextMapInfo.u1IsPrimaryCtx =
            ((SISP_IS_LOGICAL_PORT (u4IfIndex) == VCM_TRUE) ? VCM_FALSE
             : VCM_TRUE);

        if (VcmFsVcmHwMapIfIndexToBrgPort
            (u4ContextId, VcmIfMapEntry.u4PhyIfIndex,
             ContextMapInfo) == FNP_FAILURE)
        {
            return VCM_FAILURE;
        }
    }
    else                        /* Remove from Hardware */
    {
        if (VcmFsVcmHwUnMapPortFromContext
            (u4ContextId, VcmIfMapEntry.u4PhyIfIndex) == FNP_FAILURE)
        {
            return VCM_FAILURE;
        }
    }
    return VCM_SUCCESS;
}
#endif /* NPAPI_WANTED */
/*****************************************************************************/
/*                                                                           */
/* Function     : VcmSnmpSendTrap                                           */
/*                                                                           */
/* Description  : Routine to send trap to SNMP .                             */
/*                                                                           */
/* Input        : i4TrapId   : Trap Identifier                               */
/*                pi1TrapOid : Pointer to the trap OID                       */
/*                u1OidLen   : OID Length                                    */
/*                pTrapInfo  : Pointer to the trap information.              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
VcmSnmpSendTrap (UINT1 u1TrapId, INT1 *pi1TrapOid, UINT1 u1OidLen,
                 VOID *pTrapInfo)
{
#ifdef SNMP_3_WANTED
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid;
    tSNMP_OID_TYPE     *pOid;
    UINT1               au1Buf[VCM_OBJECT_NAME_LEN];
    UINT4               u4GenTrapType;
    UINT4               u4SpecTrapType;
    UINT4              *pu4ContextId = NULL;
    tSNMP_COUNTER64_TYPE SnmpCounter64Type;

    UNUSED_PARAM (u1OidLen);

    MEMSET (au1Buf, 0, VCM_OBJECT_NAME_LEN);

    pEnterpriseOid = (tSNMP_OID_TYPE *) SNMP_AGT_GetOidFromString (pi1TrapOid);
    if (pEnterpriseOid == NULL)
    {
        return;
    }
    u4GenTrapType = ENTERPRISE_SPECIFIC;
    u4SpecTrapType = (UINT4) u1TrapId;
    SnmpCounter64Type.msn = 0;
    SnmpCounter64Type.lsn = 0;

    switch (u1TrapId)
    {

        case VCM_CREATE_CONTEXT_TRAP:
        case VCM_DELETE_CONTEXT_TRAP:

            pu4ContextId = (UINT4 *) pTrapInfo;
            SPRINTF ((char *) au1Buf, "fsVCId");
            pOid = (tSNMP_OID_TYPE *) VcmMakeObjIdFromDotNew ((UINT1 *) au1Buf);
            if (pOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) *pu4ContextId,
                                      NULL, NULL, SnmpCounter64Type);
            if (pVbList == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pStartVb = pVbList;
            break;

        default:
            SNMP_FreeOid (pEnterpriseOid);
            return;
    }

    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_AGT_RIF_Notify_Trap (pEnterpriseOid, u4GenTrapType, u4SpecTrapType,
                              pStartVb);

#else /* SNMP_3_WANTED */
    UNUSED_PARAM (u1TrapId);
    UNUSED_PARAM (*pi1TrapOid);
    UNUSED_PARAM (u1OidLen);
    UNUSED_PARAM (pTrapInfo);
#endif /* SNMP_3_WANTED */

}

/**********************************************************************
* Function             : VcmMakeObjIdFromDotNew                       *
*                                                                     *
* Role of the function : This function returns the pointer to the     *
*                         object id of the passed object to it.       * 
* Formal Parameters    : textStr  :  pointer to the object whose      *
                                      object id is to be found        *
* Global Variables     : None                                         *
* Side Effects         : None                                         *
* Exception Handling   : None                                         *
* Use of Recursion     : None                                         *
* Return Value         : pOid : pointer to object id.                 *
**********************************************************************/

PUBLIC tSNMP_OID_TYPE *
VcmMakeObjIdFromDotNew (UINT1 *textStr)
{
    tSNMP_OID_TYPE     *pOid = NULL;
    INT1               *pTemp = NULL, *pDot = NULL;
    UINT1               au1tempBuffer[VCM_OBJECT_NAME_LEN + 1];

    UINT4               au4VCId[] = { 1, 3, 6, 1, 4, 1, 2076, 93, 1, 2, 1, 1 };

    /* Is there an alpha descriptor at begining ?? */
    if (VCM_ISALPHA (*textStr))
    {
        pDot = ((INT1 *) STRCHR ((char *) textStr, '.'));

        /* if no dot, point to end of string */
        if (pDot == NULL)
        {
            pDot = ((INT1 *) (textStr + STRLEN ((char *) textStr)));
        }
        pTemp = ((INT1 *) textStr);
        MEMSET (au1tempBuffer, 0, VCM_OBJECT_NAME_LEN + 1);
        STRNCPY (au1tempBuffer, pTemp, (UINT4) (pDot - pTemp));
        au1tempBuffer[(pDot - pTemp)] = '\0';

        if ((pOid = alloc_oid (SNMP_MAX_OID_LENGTH)) == NULL)
        {
            return (NULL);
        }

        if (STRCMP ("fsVCId", (INT1 *) au1tempBuffer) == 0)
        {
            MEMCPY (pOid->pu4_OidList, au4VCId, sizeof (au4VCId));
            pOid->u4_Length = (sizeof (au4VCId) / sizeof (UINT4));
            return (pOid);
        }
    }

    if (pOid != NULL)
    {
        MEM_FREE (pOid);
    }
    return NULL;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmDelIfMapEntryExt                                */
/*                                                                           */
/*     DESCRIPTION      : This is an API to delete the IF-Context mapping    */
/*                        if creating of an entry fails.                     */
/*                                                                           */
/*     INPUT            : CliHandle  - Context in which the CLI              */
/*                                             command is processed          */
/*                        u4IfIndex  - Interface to Get Map.                 */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS / CLI_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
VOID
VcmDelIfMapEntryExt (UINT4 u4IfIndex)
{
    UINT4               u4ErrorCode = 0;

    if (SNMP_SUCCESS == nmhTestv2FsVcIfRowStatus (&u4ErrorCode,
                                                  (INT4) u4IfIndex, DESTROY))
    {
        if (SNMP_SUCCESS == nmhSetFsVcIfRowStatus ((INT4) u4IfIndex,
                                                   (INT4) DESTROY))
        {
            return;
        }
    }
    return;
}

#endif
