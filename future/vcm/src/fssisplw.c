/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssisplw.c,v 1.6 2014/01/24 12:29:05 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "vcminc.h"
# include  "sispcli.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSispSystemControl
 Input       :  The Indices

                The Object 
                retValFsSispSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSispSystemControl (INT4 *pi4RetValFsSispSystemControl)
{
    *pi4RetValFsSispSystemControl = VCM_GET_SISP_STATUS;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSispSystemControl
 Input       :  The Indices

                The Object 
                setValFsSispSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSispSystemControl (INT4 i4SetValFsSispSystemControl)
{
    INT4                i4RetVal;

    if (i4SetValFsSispSystemControl == VCM_GET_SISP_STATUS)
    {
        return SNMP_SUCCESS;
    }
    if (i4SetValFsSispSystemControl == SISP_START)
    {
        i4RetVal = VcmSispModuleStart ();
    }
    else
    {
        VcmSispRedSendModuleStatus ((UINT1) i4SetValFsSispSystemControl);

        i4RetVal = VcmSispModuleShutdown ();

        /* Trigger MSR to delete all the SISP related objects from
         * RB-Tree */
        VcmSispNotifyProtocolShutdownStatus ();

        VcmSispRedSendCompleteStatus ();
    }

    return ((i4RetVal == VCM_SUCCESS) ? SNMP_SUCCESS : SNMP_FAILURE);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSispSystemControl
 Input       :  The Indices

                The Object 
                testValFsSispSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSispSystemControl (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsSispSystemControl)
{
    if ((i4TestValFsSispSystemControl != SISP_START) &&
        (i4TestValFsSispSystemControl != SISP_SHUTDOWN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsSispSystemControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSispSystemControl (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsSispPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsSispPortTable
 Input       :  The Indices
                FsSispPortIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsSispPortTable (INT4 i4FsSispPortIndex)
{
    if ((i4FsSispPortIndex <= VCM_INVALID_VAL) ||
        (i4FsSispPortIndex > BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsSispPortTable
 Input       :  The Indices
                FsSispPortIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsSispPortTable (INT4 *pi4FsSispPortIndex)
{
    return (nmhGetNextIndexFsSispPortTable (0, pi4FsSispPortIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsSispPortTable
 Input       :  The Indices
                FsSispPortIndex
                nextFsSispPortIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsSispPortTable (INT4 i4FsSispPortIndex,
                                INT4 *pi4NextFsSispPortIndex)
{
    UINT4               u4IfIndex;

    *pi4NextFsSispPortIndex = VCM_INIT_VAL;

    if (VcmGetNextIfMapIfIndex ((UINT4) i4FsSispPortIndex,
                                &u4IfIndex) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* SISP Port map table only for Physical & Port channel ports */
    if (u4IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsSispPortIndex = (INT4) u4IfIndex;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSispPortCtrlStatus
 Input       :  The Indices
                FsSispPortIndex

                The Object 
                retValFsSispPortCtrlStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSispPortCtrlStatus (INT4 i4FsSispPortIndex,
                            INT4 *pi4RetValFsSispPortCtrlStatus)
{
    UINT1               u1Res;

    *pi4RetValFsSispPortCtrlStatus = SISP_DISABLE;

    L2IwfGetSispPortCtrlStatus ((UINT4) i4FsSispPortIndex, &u1Res);

    *pi4RetValFsSispPortCtrlStatus = (INT4) u1Res;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSispPortCtrlStatus
 Input       :  The Indices
                FsSispPortIndex

                The Object 
                setValFsSispPortCtrlStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSispPortCtrlStatus (INT4 i4FsSispPortIndex,
                            INT4 i4SetValFsSispPortCtrlStatus)
{
    INT4                i4RetVal = SNMP_FAILURE;
    UINT1               u1Status = SISP_DISABLE;

    L2IwfGetSispPortCtrlStatus ((UINT4) i4FsSispPortIndex, &u1Status);

    if (u1Status == (UINT1) i4SetValFsSispPortCtrlStatus)
    {
        return SNMP_SUCCESS;
    }

    VcmSispRedSendPortCtrlStatus ((UINT4) i4FsSispPortIndex,
                                  (UINT1) i4SetValFsSispPortCtrlStatus);

    if (i4SetValFsSispPortCtrlStatus == SISP_ENABLE)
    {
        i4RetVal = VcmSispEnableOnPort ((UINT4) i4FsSispPortIndex);
    }
    else
    {
        i4RetVal = VcmSispDisableOnPort ((UINT4) i4FsSispPortIndex);
    }

    VcmSispRedSendCompleteStatus ();

    return ((i4RetVal == VCM_SUCCESS) ? SNMP_SUCCESS : SNMP_FAILURE);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSispPortCtrlStatus
 Input       :  The Indices
                FsSispPortIndex

                The Object 
                testValFsSispPortCtrlStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSispPortCtrlStatus (UINT4 *pu4ErrorCode, INT4 i4FsSispPortIndex,
                               INT4 i4TestValFsSispPortCtrlStatus)
{
    UINT4               u4ContextId;
    UINT4               u4BridgeMode;
    UINT1               u1PbPortType;
    UINT1               u1Status = SISP_DISABLE;

    if ((i4TestValFsSispPortCtrlStatus != SISP_ENABLE)
        && (i4TestValFsSispPortCtrlStatus != SISP_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VCM_GET_SISP_STATUS == SISP_SHUTDOWN)
    {
        /* SISP has to be globally enabled before enabling on a port */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (SISP_GLOBAL_SHUT);
        return SNMP_FAILURE;
    }

    L2IwfGetSispPortCtrlStatus ((UINT4) i4FsSispPortIndex, &u1Status);

    if (u1Status == (UINT1) i4TestValFsSispPortCtrlStatus)
    {
        return SNMP_SUCCESS;
    }

    if ((i4FsSispPortIndex <= VCM_INVALID_VAL) ||
        (i4FsSispPortIndex > BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        /* Whether port is eithe Physical or PortChannel interface */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (SISP_PORT_TYPE_ERR);
        return SNMP_FAILURE;
    }
    if (i4TestValFsSispPortCtrlStatus == SISP_ENABLE)
    {
        if (MemGetFreeUnits (SISP_IF_CTX_INFO_MEMPOOL_ID) == 0)
        {
            /* MAX allowable SISP enabled ports reached */
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (SISP_MAX_ENABLED_PORTS_ERR);
            return SNMP_FAILURE;
        }
        if (VcmGetIfMapVcId ((UINT4) i4FsSispPortIndex, &u4ContextId)
            == VCM_FAILURE)
        {
            /* Verify whether port is created or not */
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (SISP_PORT_NOT_ACTIVE);
            return SNMP_FAILURE;
        }
        if (L2IsPortInPortChannel ((UINT4) i4FsSispPortIndex) == L2IWF_SUCCESS)
        {
            /* SISP can't be enabled on port that is in a port channel. */
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (SISP_PORT_CHANNEL_ERR);
            return SNMP_FAILURE;
        }
        if (VcmSispAstIsContextStpCompatible (u4ContextId) == RST_SUCCESS)
        {
            /* SISP can be enabled only in MSTP enabled contexts  */
            CLI_SET_ERR (SISP_PORT_STP_MODE_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if (VcmSispAstIsRstStartedInContext (u4ContextId))
        {
            /* SISP can be enabled only in MSTP enabled contexts  */
            CLI_SET_ERR (SISP_PORT_STP_MODE_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if (L2IwfGetProtocolEnabledStatusOnPort ((UINT4) i4FsSispPortIndex,
                                                 L2_PROTO_GVRP,
                                                 &u1Status) == L2IWF_SUCCESS)
        {
            if (u1Status == L2IWF_ENABLED)
            {
                /* SISP cannot be enabled when GVRP is enabled on the port */
                CLI_SET_ERR (SISP_PORT_GVRP_MVRP_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }

        if (L2IwfGetProtocolEnabledStatusOnPort ((UINT4) i4FsSispPortIndex,
                                                 L2_PROTO_MVRP,
                                                 &u1Status) == L2IWF_SUCCESS)
        {
            if (u1Status == L2IWF_ENABLED)
            {
                /* SISP cannot be enabled when GVRP is enabled on the port */
                CLI_SET_ERR (SISP_PORT_GVRP_MVRP_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
        VcmL2IwfGetBridgeMode (u4ContextId, &u4BridgeMode);

        if ((u4BridgeMode != VLAN_CUSTOMER_BRIDGE_MODE) &&
            (u4BridgeMode != VLAN_PROVIDER_EDGE_BRIDGE_MODE) &&
            (u4BridgeMode != VLAN_PROVIDER_CORE_BRIDGE_MODE))
        {
            /* SISP can be enabled only on Customer, PEB & PCB Bridges */
            CLI_SET_ERR (SISP_BRIDGE_TYPE_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        if ((u4BridgeMode == VLAN_PROVIDER_EDGE_BRIDGE_MODE) ||
            (u4BridgeMode == VLAN_PROVIDER_CORE_BRIDGE_MODE))
        {
            VcmSispL2IwfGetPbPortType ((UINT4) i4FsSispPortIndex,
                                       &u1PbPortType);

            if ((u1PbPortType != CFA_PROVIDER_NETWORK_PORT) &&
                (u1PbPortType != CFA_PROP_PROVIDER_NETWORK_PORT))
            {
                /* SISP can be enabled only on PNP & P-PNP type of ports */
                CLI_SET_ERR (SISP_BRG_PORT_TYPE_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
    }
    else                        /* For disable */
    {
        /* SISP Can't be disabled on a port, when there is any logical
         * port existing for the phyical port */
        if (VcmSispLogicalPortExistForPhysicalPort ((UINT4) i4FsSispPortIndex)
            == VCM_TRUE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (SISP_LOGICAL_PORT_EXIST_ERR);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsSispPortTable
 Input       :  The Indices
                FsSispPortIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSispPortTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsSispPortMapTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsSispPortMapTable
 Input       :  The Indices
                FsSispPortIndex
                FsSispPortMapContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsSispPortMapTable (INT4 i4FsSispPortIndex,
                                            INT4 i4FsSispPortMapContextId)
{
    tVcmIfMapEntry      VcmIfMapEntry;

    if (VCM_GET_SISP_STATUS != SISP_START)
    {
        return SNMP_FAILURE;
    }

    if ((i4FsSispPortIndex <= VCM_INVALID_VAL) ||
        (i4FsSispPortIndex > BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        return SNMP_FAILURE;
    }

    if ((i4FsSispPortMapContextId < 0) ||
        (i4FsSispPortMapContextId >= SYS_DEF_MAX_NUM_CONTEXTS))
    {
        return SNMP_FAILURE;
    }

    if (VcmSispPortMapEntryGet ((UINT4) i4FsSispPortIndex,
                                (UINT4) i4FsSispPortMapContextId,
                                &VcmIfMapEntry) != VCM_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsSispPortMapTable
 Input       :  The Indices
                FsSispPortIndex
                FsSispPortMapContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsSispPortMapTable (INT4 *pi4FsSispPortIndex,
                                    INT4 *pi4FsSispPortMapContextId)
{
    return (nmhGetNextIndexFsSispPortMapTable (0, pi4FsSispPortIndex,
                                               0, pi4FsSispPortMapContextId));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsSispPortMapTable
 Input       :  The Indices
                FsSispPortIndex
                nextFsSispPortIndex
                FsSispPortMapContextId
                nextFsSispPortMapContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsSispPortMapTable (INT4 i4FsSispPortIndex,
                                   INT4 *pi4NextFsSispPortIndex,
                                   INT4 i4FsSispPortMapContextId,
                                   INT4 *pi4NextFsSispPortMapContextId)
{
    tVcmIfMapEntry      VcmIfMapEntry;
    INT4                i4RetVal = SNMP_FAILURE;

    *pi4NextFsSispPortIndex = VCM_INIT_VAL;
    *pi4NextFsSispPortMapContextId = VCM_INIT_VAL;

    i4RetVal = VcmSispPortMapEntryGetNext ((UINT4) i4FsSispPortIndex,
                                           (UINT4) i4FsSispPortMapContextId,
                                           &VcmIfMapEntry);

    if (i4RetVal == VCM_SUCCESS)
    {
        *pi4NextFsSispPortIndex = (INT4) VcmIfMapEntry.u4PhyIfIndex;
        *pi4NextFsSispPortMapContextId = (INT4) VcmIfMapEntry.u4VcNum;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSispPortMapSharedPort
 Input       :  The Indices
                FsSispPortIndex
                FsSispPortMapContextId

                The Object 
                retValFsSispPortMapSharedPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSispPortMapSharedPort (INT4 i4FsSispPortIndex,
                               INT4 i4FsSispPortMapContextId,
                               INT4 *pi4RetValFsSispPortMapSharedPort)
{
    tVcmIfMapEntry      VcmIfMapEntry;

    *pi4RetValFsSispPortMapSharedPort = VCM_INIT_VAL;

    if (VcmSispPortMapEntryGet ((UINT4) i4FsSispPortIndex,
                                (UINT4) i4FsSispPortMapContextId,
                                &VcmIfMapEntry) == VCM_SUCCESS)
    {
        *pi4RetValFsSispPortMapSharedPort = (INT4) VcmIfMapEntry.u4IfIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsSispPortMapHlPortId
 Input       :  The Indices
                FsSispPortIndex
                FsSispPortMapContextId

                The Object 
                retValFsSispPortMapHlPortId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSispPortMapHlPortId (INT4 i4FsSispPortIndex,
                             INT4 i4FsSispPortMapContextId,
                             INT4 *pi4RetValFsSispPortMapHlPortId)
{
    tVcmIfMapEntry      VcmIfMapEntry;

    *pi4RetValFsSispPortMapHlPortId = VCM_INIT_VAL;

    if (VcmSispPortMapEntryGet ((UINT4) i4FsSispPortIndex,
                                (UINT4) i4FsSispPortMapContextId,
                                &VcmIfMapEntry) == VCM_SUCCESS)
    {
        *pi4RetValFsSispPortMapHlPortId = VcmIfMapEntry.u2HlPortId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsSispPortMapRowStatus
 Input       :  The Indices
                FsSispPortIndex
                FsSispPortMapContextId

                The Object 
                retValFsSispPortMapRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSispPortMapRowStatus (INT4 i4FsSispPortIndex,
                              INT4 i4FsSispPortMapContextId,
                              INT4 *pi4RetValFsSispPortMapRowStatus)
{
    tVcmIfMapEntry      VcmIfMapEntry;

    *pi4RetValFsSispPortMapRowStatus = VCM_INIT_VAL;

    if (VcmSispPortMapEntryGet ((UINT4) i4FsSispPortIndex,
                                (UINT4) i4FsSispPortMapContextId,
                                &VcmIfMapEntry) == VCM_SUCCESS)
    {
        *pi4RetValFsSispPortMapRowStatus = VcmIfMapEntry.u1RowStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSispPortMapSharedPort
 Input       :  The Indices
                FsSispPortIndex
                FsSispPortMapContextId

                The Object 
                setValFsSispPortMapSharedPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSispPortMapSharedPort (INT4 i4FsSispPortIndex,
                               INT4 i4FsSispPortMapContextId,
                               INT4 i4SetValFsSispPortMapSharedPort)
{
    if ((VcmSispSetPortMapIfIndex ((UINT4) i4FsSispPortIndex,
                                   (UINT4) i4FsSispPortMapContextId,
                                   (UINT4) i4SetValFsSispPortMapSharedPort))
        == VCM_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsSispPortMapRowStatus
 Input       :  The Indices
                FsSispPortIndex
                FsSispPortMapContextId

                The Object 
                setValFsSispPortMapRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSispPortMapRowStatus (INT4 i4FsSispPortIndex,
                              INT4 i4FsSispPortMapContextId,
                              INT4 i4SetValFsSispPortMapRowStatus)
{
    tVcmIfMapEntry      VcmIfMapEntry;

    VCM_MEMSET (&VcmIfMapEntry, VCM_INIT_VAL, sizeof (tVcmIfMapEntry));

    /* Check entry already existing */
    if (VcmSispPortMapEntryGet ((UINT4) i4FsSispPortIndex,
                                (UINT4) i4FsSispPortMapContextId,
                                &VcmIfMapEntry) == VCM_SUCCESS)
    {
        if (VcmIfMapEntry.u1RowStatus == i4SetValFsSispPortMapRowStatus)
        {
            return SNMP_SUCCESS;
        }
    }

    switch (i4SetValFsSispPortMapRowStatus)
    {
        case CREATE_AND_WAIT:

            if (VcmSispCreatePortMapEntry ((UINT4) i4FsSispPortIndex,
                                           (UINT4) i4FsSispPortMapContextId)
                != VCM_SUCCESS)
            {
                return SNMP_FAILURE;
            }

            break;

        case ACTIVE:

            if (VcmSispMakeSispIntfActive (&VcmIfMapEntry,
                                           (UINT4) i4FsSispPortMapContextId)
                != VCM_SUCCESS)
            {
                return SNMP_FAILURE;
            }

            break;

        case DESTROY:

            /* Delete the sisp port in VCM's IfMap table */
            if (VcmSispDeleteSispPort ((UINT4) i4FsSispPortIndex,
                                       (UINT4) i4FsSispPortMapContextId)
                != VCM_SUCCESS)
            {
                return SNMP_FAILURE;
            }

            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSispPortMapSharedPort
 Input       :  The Indices
                FsSispPortIndex
                FsSispPortMapContextId

                The Object 
                testValFsSispPortMapSharedPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSispPortMapSharedPort (UINT4 *pu4ErrorCode, INT4 i4FsSispPortIndex,
                                  INT4 i4FsSispPortMapContextId,
                                  INT4 i4TestValFsSispPortMapSharedPort)
{
    tVcmIfMapEntry      VcmIfMapEntry;
    tCfaIfInfo          IfInfo;

    VCM_MEMSET (&VcmIfMapEntry, VCM_INIT_VAL, sizeof (tVcmIfMapEntry));

    if ((i4TestValFsSispPortMapSharedPort < VCM_SISP_MIN_LOGICAL_IFINDEX) ||
        (i4TestValFsSispPortMapSharedPort > VCM_SISP_MAX_LOGICAL_IFINDEX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VcmSispPortMapEntryGet ((UINT4) i4FsSispPortIndex,
                                (UINT4) i4FsSispPortMapContextId,
                                &VcmIfMapEntry) != VCM_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Verify whether the Physical port is not mapped to the particular
     * context through any other logical port */
    if (VcmIfMapEntry.u1RowStatus == ACTIVE)
    {
        if (VcmIfMapEntry.u4IfIndex == (UINT4) i4TestValFsSispPortMapSharedPort)
        {
            /* Same entry is created again */
            return SNMP_SUCCESS;
        }
        else
        {
            /* This physical port is already mapped to the same
             * context using, some other SISP port */
            CLI_SET_ERR (SISP_PORT_MAP_EXISTS);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (VcmGetIfMapEntry ((UINT4) i4TestValFsSispPortMapSharedPort,
                          &VcmIfMapEntry) == VCM_SUCCESS)
    {
        if (VcmIfMapEntry.u4VcNum != (UINT4) i4FsSispPortMapContextId)
        {
            /* Logical port is already mapped to some other context */
            CLI_SET_ERR (SISP_PORT_ALREADY_MAPPED_TO_CTX);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if (VcmIfMapEntry.u4PhyIfIndex != (UINT4) i4FsSispPortIndex)
        {
            /* Logical port is already mapped to some other physical port */
            CLI_SET_ERR (SISP_PORT_ALREADY_MAPPED_TO_PORT);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (VcmCfaGetIfInfo ((UINT4) i4TestValFsSispPortMapSharedPort, &IfInfo)
        != CFA_SUCCESS)
    {
        /* Sisp port is either not created in CFA or not active in CFA */
        CLI_SET_ERR (SISP_PORT_NO_CREATION);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsSispPortMapRowStatus
 Input       :  The Indices
                FsSispPortIndex
                FsSispPortMapContextId

                The Object 
                testValFsSispPortMapRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSispPortMapRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsSispPortIndex,
                                 INT4 i4FsSispPortMapContextId,
                                 INT4 i4TestValFsSispPortMapRowStatus)
{
    tVcmIfMapEntry      VcmIfMapEntry;
    UINT4               u4SecBridgeMode = L2IWF_INVALID_BRIDGE_MODE;
    UINT4               u4PrimBridgeMode = L2IWF_INVALID_BRIDGE_MODE;
    UINT2               u2PortCount = VCM_INIT_VAL;
    UINT1               u1Res = VCM_INIT_VAL;

    VCM_MEMSET (&VcmIfMapEntry, VCM_INIT_VAL, sizeof (tVcmIfMapEntry));

    if ((i4TestValFsSispPortMapRowStatus < ACTIVE) ||
        (i4TestValFsSispPortMapRowStatus > DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4FsSispPortIndex <= VCM_INVALID_VAL) ||
        (i4FsSispPortIndex > BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4FsSispPortMapContextId < VCM_INIT_VAL) ||
        (i4FsSispPortMapContextId >= SYS_DEF_MAX_NUM_CONTEXTS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsSispPortMapRowStatus == CREATE_AND_GO) ||
        (i4TestValFsSispPortMapRowStatus == NOT_IN_SERVICE))
    {
        /* CREATE_AND_GO is not supported for this table, as MapSharedPort
         * is the mandatory object, it can't take any default value 
         * NOT_IN_SERVICE is also not supported, since to change
         * the mapping of context, port should be explicitly unmapped.*/

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    L2IwfGetSispPortCtrlStatus ((UINT4) i4FsSispPortIndex, &u1Res);

    if (u1Res != SISP_ENABLE)
    {
        CLI_SET_ERR (SISP_NOT_ENABLE_ON_PORT);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (VcmSispPortMapEntryGet ((UINT4) i4FsSispPortIndex,
                                (UINT4) i4FsSispPortMapContextId,
                                &VcmIfMapEntry) == VCM_SUCCESS)
    {
        if (VcmIfMapEntry.u1RowStatus ==
            (UINT1) i4TestValFsSispPortMapRowStatus)
        {
            return SNMP_SUCCESS;
        }

        if (i4TestValFsSispPortMapRowStatus == CREATE_AND_WAIT)
        {
            /* CREATE_AND_WAIT can't be configured for the existing entry */
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        if ((i4TestValFsSispPortMapRowStatus != DESTROY) &&
            (i4TestValFsSispPortMapRowStatus != ACTIVE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        if ((i4TestValFsSispPortMapRowStatus == ACTIVE) &&
            (VcmIfMapEntry.u4IfIndex == VCM_INVALID_VAL))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (i4TestValFsSispPortMapRowStatus != CREATE_AND_WAIT)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (i4TestValFsSispPortMapRowStatus == CREATE_AND_WAIT)
    {
        /* Validate the secondary context id is valid or not */
        if (VcmIsL2VcExist ((UINT4) i4FsSispPortMapContextId) != VCM_TRUE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        if (VcmGetIfMapEntry ((UINT4) i4FsSispPortIndex, &VcmIfMapEntry) ==
            VCM_FAILURE)
        {
            return SNMP_FAILURE;
        }

        if (VcmIfMapEntry.u4VcNum == (UINT4) i4FsSispPortMapContextId)
        {
            /* Secondary context & Primary context of the physical  *
             * port should not be same */
            CLI_SET_ERR (SISP_PORT_CTX_SAME_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if (VcmSispAstIsContextStpCompatible ((UINT4) i4FsSispPortMapContextId)
            == RST_SUCCESS)
        {
            /* SISP can be enabled only in MSTP enabled contexts  */
            CLI_SET_ERR (SISP_PORT_STP_MODE_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if (VcmSispAstIsRstStartedInContext ((UINT4) i4FsSispPortMapContextId))
        {
            /* SISP can be enabled only in MSTP enabled contexts  */
            CLI_SET_ERR (SISP_PORT_STP_MODE_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        VcmL2IwfGetBridgeMode ((UINT4) i4FsSispPortMapContextId,
                               &u4SecBridgeMode);
        VcmL2IwfGetBridgeMode (VcmIfMapEntry.u4VcNum, &u4PrimBridgeMode);

        switch (u4PrimBridgeMode)
        {
            case VLAN_PROVIDER_EDGE_BRIDGE_MODE:
            case VLAN_PROVIDER_CORE_BRIDGE_MODE:

                /* Bridge mode of Primary & Secondary contexts can be
                 * either VLAN_PROVIDER_EDGE_BRIDGE_MODE or 
                 * VLAN_PROVIDER_CORE_BRIDGE_MODE */
                if ((u4SecBridgeMode != VLAN_PROVIDER_EDGE_BRIDGE_MODE)
                    && (u4SecBridgeMode != VLAN_PROVIDER_CORE_BRIDGE_MODE))
                {
                    /* Secondary context d only in MSTP enabled contexts  */
                    CLI_SET_ERR (SISP_BRG_MODE_ERR);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }

                break;

            default:
                /* For all other bridge modes - Primary & Secondary
                 * context's bridge mode has to be same */
                if (u4SecBridgeMode != u4PrimBridgeMode)
                {
                    CLI_SET_ERR (SISP_BRG_MODE_ERR);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
                break;
        }

        /* Verify MAX ports for a context are already mapped */
        VcmGetVcPortCount ((UINT4) i4FsSispPortMapContextId, &u2PortCount);
        if (u2PortCount >= MAX_PORTS_PER_CXT)
        {
            CLI_SET_ERR (SISP_MAX_PORTS_REACHED_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsSispPortMapTable
 Input       :  The Indices
                FsSispPortIndex
                FsSispPortMapContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSispPortMapTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsSispCxtClassificationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsSispCxtClassificationTable
 Input       :  The Indices
                FsSispPortIndex
                FsSispCxtClassificationVlanId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsSispCxtClassificationTable (INT4 i4FsSispPortIndex,
                                                      INT4
                                                      i4FsSispCxtClassificationVlanId)
{
    UINT1               u1Res = SISP_DISABLE;
    tVcmIfMapEntry      VcmIfMapEntry;

    if ((i4FsSispPortIndex > VCM_INVALID_VAL) &&
        (i4FsSispPortIndex <= BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        L2IwfGetSispPortCtrlStatus ((UINT4) i4FsSispPortIndex, &u1Res);

        if (u1Res != SISP_ENABLE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        return SNMP_FAILURE;
    }

    if ((i4FsSispCxtClassificationVlanId < 1) ||
        (i4FsSispCxtClassificationVlanId > VLAN_MAX_VLAN_ID))
    {
        return SNMP_FAILURE;
    }

    if (VcmSispPortVlanMapEntryGet ((UINT4) i4FsSispPortIndex,
                                    (tVlanId) i4FsSispCxtClassificationVlanId,
                                    &VcmIfMapEntry) == VCM_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsSispCxtClassificationTable
 Input       :  The Indices
                FsSispPortIndex
                FsSispCxtClassificationVlanId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsSispCxtClassificationTable (INT4 *pi4FsSispPortIndex,
                                              INT4
                                              *pi4FsSispCxtClassificationVlanId)
{
    return (nmhGetNextIndexFsSispCxtClassificationTable
            (0, pi4FsSispPortIndex, 0, pi4FsSispCxtClassificationVlanId));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsSispCxtClassificationTable
 Input       :  The Indices
                FsSispPortIndex
                nextFsSispPortIndex
                FsSispCxtClassificationVlanId
                nextFsSispCxtClassificationVlanId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsSispCxtClassificationTable (INT4 i4FsSispPortIndex,
                                             INT4 *pi4NextFsSispPortIndex,
                                             INT4
                                             i4FsSispCxtClassificationVlanId,
                                             INT4
                                             *pi4NextFsSispCxtClassificationVlanId)
{
    tVcmIfMapEntry      VcmIfMapEntry;
    tVlanId             NextVlanId = VCM_INIT_VAL;

    VCM_MEMSET (&VcmIfMapEntry, VCM_INIT_VAL, sizeof (tVcmIfMapEntry));

    if (VcmSispPortVlanMapEntryGetNext ((UINT4) i4FsSispPortIndex,
                                        (tVlanId)
                                        i4FsSispCxtClassificationVlanId,
                                        &NextVlanId,
                                        &VcmIfMapEntry) == VCM_SUCCESS)
    {
        *pi4NextFsSispPortIndex = (INT4) VcmIfMapEntry.u4PhyIfIndex;
        *pi4NextFsSispCxtClassificationVlanId = NextVlanId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSispCxtClassificationCxtId
 Input       :  The Indices
                FsSispPortIndex
                FsSispCxtClassificationVlanId

                The Object 
                retValFsSispCxtClassificationCxtId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSispCxtClassificationCxtId (INT4 i4FsSispPortIndex,
                                    INT4 i4FsSispCxtClassificationVlanId,
                                    INT4 *pi4RetValFsSispCxtClassificationCxtId)
{
    tVcmIfMapEntry      VcmIfMapEntry;

    VCM_MEMSET (&VcmIfMapEntry, VCM_INIT_VAL, sizeof (tVcmIfMapEntry));

    if (VcmSispPortVlanMapEntryGet ((UINT4) i4FsSispPortIndex,
                                    (tVlanId) i4FsSispCxtClassificationVlanId,
                                    &VcmIfMapEntry) == VCM_SUCCESS)
    {
        *pi4RetValFsSispCxtClassificationCxtId = (INT4) VcmIfMapEntry.u4VcNum;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}
