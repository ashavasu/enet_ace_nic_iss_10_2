/* $Id: vcmport.c,v 1.13 2015/06/29 05:51:12 siva Exp $*/
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2004-2005                                          */
/*****************************************************************************/
/*    FILE  NAME            : vcml2if.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : VCM module                                     */
/*    MODULE NAME           : VCM module                                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 14 Nov 2007                                    */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains functions to interact with  */
/*                            other protocols by VCM.                        */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    14 Nov 2007 / L2Team   Initial Create.                         */
/*---------------------------------------------------------------------------*/

#ifndef _VCMPORT_C
#define _VCMPORT_C

#include "vcminc.h"
#include "l2iwf.h"
#include "iss.h"

#ifdef CLI_WANTED
/*****************************************************************************/
/* Function Name      : VcmCfaCliGetIfName                                   */
/*                                                                           */
/* Description        : This function calls the CFA Module to get the        */
/*                      interface name from the given interface index.       */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pi1IfName - Interface name of the given IfIndex.     */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
VcmCfaCliGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName)
{
    return (CfaCliGetIfName (u4IfIndex, pi1IfName));
}

/*****************************************************************************
 *
 *    Function Name       : VcmCfaCliConfGetIfName
 *
 *    Description         : This function returns Interface name for specified
 *                          interface
 *
 *    Input(s)            : u4IfIndex - Interface Index 
 *
 *    Output(s)           : pi1IfName - Pointer to buffer
 *
 *
 *    Returns            : CLI_SUCCESS if name assigned for pi1IfName
 *                         CLI_FAILURE if name is not assign pi1IfName
 *                         CFA_FAILURE if u4IfIndex is not valid interface
 *
 *****************************************************************************/
INT4
VcmCfaCliConfGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName)
{
    return (CfaCliConfGetIfName (u4IfIndex, pi1IfName));
}
#endif
/*****************************************************************************/
/* Function Name      : VcmCfaGetIfInfo                                      */
/*                                                                           */
/* Description        : This function calls the CFA Module to get the        */
/*                      interface name from the given interface index.       */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pIfInfo   - Interface information.                   */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
VcmCfaGetIfInfo (UINT4 u4IfIndex, tCfaIfInfo * pIfInfo)
{
    return (CfaGetIfInfo (u4IfIndex, pIfInfo));
}

/*****************************************************************************/
/* Function Name      : VcmCfaIsVirtualInterface                             */
/*                                                                           */
/* Description        : This function calls the CFA Module to check if       */
/*                      given interface index is a virtual Interface.        */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          :                                                      */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
VcmCfaIsVirtualInterface (UINT4 u4IfIndex)
{
    return (CfaIsVirtualInterface (u4IfIndex));
}

/*****************************************************************************/
/* Function Name      : VcmIssGetContextMacAddress                           */
/*                                                                           */
/* Description        : This function calls the CFA module to get the        */
/*                      system MAC address.                                  */
/*                                                                           */
/* Input(s)           : u4ContextId- Instance Id                             */
/*                                                                           */
/* Output(s)          : pSwitchMac - pointer to the switch MAC address       */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
VcmIssGetContextMacAddress (UINT4 u4ContextId, tMacAddr pSwitchMac)
{
    IssGetContextMacAddress (u4ContextId, pSwitchMac);
}

/*****************************************************************************/
/* Function Name      : VcmL2IwfGetBridgeMode                                */
/*                                                                           */
/* Description        : This function calls the L2IWF module to get the      */
/*                      bridge mode.                                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                                                                           */
/* Output(s)          : pu4BridgeMode  - bridge mode.                        */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
VcmL2IwfGetBridgeMode (UINT4 u4ContextId, UINT4 *pu4BridgeMode)
{
    return (L2IwfGetBridgeMode (u4ContextId, pu4BridgeMode));
}

/*****************************************************************************/
/* Function Name      : VcmL2IwfIsPortChannelConfigAllowed                   */
/*                                                                           */
/* Description        : This function calls the L2IWF module to check        */
/*                      Portchannel configuration allowed or not             */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                                                                           */
/* Output(s)          : pu4BridgeMode  - bridge mode.                        */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
VcmL2IwfIsPortChannelConfigAllowed (UINT4 u4IfIndex)
{
    return (L2IwfIsPortChannelConfigAllowed (u4IfIndex));
}

/***************************************************************************/
/*                  Interfaces with Layer2 modules.                        */
/***************************************************************************/

/*****************************************************************************/
/* Function Name      : L2CreateVirtualContext                               */
/*                                                                           */
/* Description        : This routine calls the corresponding L2 protocols    */
/*                      Initialization functions to create a new instance    */
/*                      for the context created.                             */
/*                                                                           */
/* Input(s)           : u4VcNum  - Virtual Context number                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE.                           */
/*****************************************************************************/
INT4
L2CreateVirtualContext (UINT4 u4VcNum)
{
    if (L2IwfContextCreateIndication (u4VcNum) != L2IWF_SUCCESS)
    {
        return VCM_FAILURE;
    }
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2DeleteVirtualContext                               */
/*                                                                           */
/* Description        : This routine calls the corresponding L2 protocols    */
/*                      Shutdown functions to delete the instance            */
/*                      for the context deleted.                             */
/*                                                                           */
/* Input(s)           : u4VcNum  - Virtual Context number                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE.                           */
/*****************************************************************************/
INT4
L2DeleteVirtualContext (UINT4 u4VcNum)
{
    L2IwfContextDeleteIndication (u4VcNum);

    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2CreateInterface                                    */
/*                                                                           */
/* Description        : This routine intimates the L2 protocols about the    */
/*                      interface creation. This routine is called           */
/*                      when interface create indication comes from CFA.     */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index.                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
L2CreateInterface (UINT4 u4VcNum, UINT4 u4IfIndex, UINT2 u2HlPortId)
{
    if (L2IwfPortMapIndication (u4VcNum, u4IfIndex,
                                u2HlPortId) != L2IWF_SUCCESS)
    {
        return VCM_FAILURE;
    }
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2DeleteInterface                                    */
/*                                                                           */
/* Description        : This routine intimates the L2 protocols about the    */
/*                      interface deletion. This routine is called           */
/*                      when interface delete indication comes from CFA.     */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index.                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
L2DeleteInterface (UINT4 u4IfIndex)
{
    if (L2IwfPortUnmapIndication (u4IfIndex) != L2IWF_SUCCESS)
    {
        return VCM_FAILURE;
    }
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2UpdateAliasName                                    */
/*                                                                           */
/* Description        : This routine intimates the L2 protocols about the    */
/*                      new alias name for the context.                      */
/*                                                                           */
/* Input(s)           : u4VcNum  - Virtual Context number.                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
L2UpdateAliasName (UINT4 u4VcNum)
{
    L2IwfUpdateContextAliasName (u4VcNum);
}

/*****************************************************************************/
/* Function Name      : L2IsPortInPortChannel                                */
/*                                                                           */
/* Description        : This routine checks whether the port is a member of  */
/*                      any Port Channel and if so returns SUCCESS.          */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global IfIndex of the port               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IsPortInPortChannel (UINT4 u4IfIndex)
{
    return (L2IwfIsPortInPortChannel (u4IfIndex));
}

#ifdef L2RED_WANTED
/*****************************************************************************/
/* Function Name      : VcmRmEnqMsgToRmFromAppl                              */
/*                                                                           */
/* Description        : This function calls the RM Module to enqueue the     */
/*                      message from applications to RM task.                */
/*                                                                           */
/* Input(s)           : pRmMsg - msg from appl.                              */
/*                      u2DataLen - Len of msg.                              */
/*                      u4SrcEntId - EntId from which the msg has arrived.   */
/*                      u4DestEntId - EntId to which the msg has to be sent. */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
VcmRmEnqMsgToRmFromAppl (tRmMsg * pRmMsg, UINT2 u2DataLen, UINT4 u4SrcEntId,
                         UINT4 u4DestEntId)
{
    return (RmEnqMsgToRmFromAppl (pRmMsg, u2DataLen, u4SrcEntId, u4DestEntId));
}

/*****************************************************************************/
/* Function Name      : VcmRmEnqChkSumMsgToRm                                */
/*                                                                           */
/* Description        : This function calls the RM Module to enqueue the     */
/*                      message from applications to RM task.                */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS/VCM_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
VcmRmEnqChkSumMsgToRm (UINT2 u2AppId, UINT2 u2ChkSum)
{
    if ((RmEnqChkSumMsgToRmFromApp (u2AppId, u2ChkSum)) == RM_FAILURE)
    {
        return VCM_FAILURE;
    }
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmRmGetNodeState                                    */
/*                                                                           */
/* Description        : This function calls the RM module to get the node    */
/*                      state.                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
VcmRmGetNodeState (VOID)
{
    return (RmGetNodeState ());
}

/*****************************************************************************/
/* Function Name      : VcmRmGetStandbyNodeCount                             */
/*                                                                           */
/* Description        : This function calls the RM module to get the number  */
/*                      of peer nodes that are up.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT1
VcmRmGetStandbyNodeCount (VOID)
{
    return (RmGetStandbyNodeCount ());
}

/*****************************************************************************/
/* Function Name      : VcmRmGetStaticConfigStatus                           */
/*                                                                           */
/* Description        : This function calls the RM module to get the         */
/*                      status of static configuration restoration.          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT1
VcmRmGetStaticConfigStatus (VOID)
{
    return (RmGetStaticConfigStatus ());
}

/*****************************************************************************/
/* Function Name      : VcmRmHandleProtocolEvent                          */
/*                                                                           */
/* Description        : This function calls the RM module to intimate about  */
/*                      the protocol operations                              */
/*                                                                           */
/* Input(s)           : pEvt->u4AppId - Application Id                       */
/*                      pEvt->u4Event - Event send by protocols to RM        */
/*                                      (RM_PROTOCOL_BULK_UPDT_COMPLETION /  */
/*                                      RM_BULK_UPDT_ABORT /                 */
/*                                      RM_STANDBY_TO_ACTIVE_EVT_PROCESSED / */
/*                                      RM_IDLE_TO_ACTIVE_EVT_PROCESSED /    */
/*                                      RM_STANDBY_EVT_PROCESSED)            */
/*                      pEvt->u4Error - Error code (RM_MEMALLOC_FAIL /       */
/*                                      RM_SENDTO_FAIL / RM_PROCESS_FAIL)    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1
VcmRmHandleProtocolEvent (tRmProtoEvt * pEvt)
{
    return (RmApiHandleProtocolEvent (pEvt));
}

/*****************************************************************************/
/* Function Name      : VcmRmRegisterProtocols                               */
/*                                                                           */
/* Description        : This function calls the RM module to register.       */
/*                                                                           */
/* Input(s)           : tRmRegParams - Reg. params to be provided by         */
/*                      protocols.                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
VcmRmRegisterProtocols (tRmRegParams * pRmReg)
{
    return (RmRegisterProtocols (pRmReg));
}

/*****************************************************************************/
/* Function Name      : VcmRmDeRegisterProtocols                             */
/*                                                                           */
/* Description        : This function calls the RM module to deregister.     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RM_SUCCESS/RM_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
UINT4
VcmRmDeRegisterProtocols (VOID)
{
    return (RmDeRegisterProtocols (RM_VCM_APP_ID));
}

/*****************************************************************************/
/* Function Name      : VcmRmReleaseMemoryForMsg                             */
/*                                                                           */
/* Description        : This function calls the RM module to release the     */
/*                      memory allocated for sending the Standby node count. */
/*                                                                           */
/* Input(s)           : pu1Block - Mmemory block.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
VcmRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    return (RmReleaseMemoryForMsg (pu1Block));
}

/*****************************************************************************/
/* Function Name      : VcmRmSetBulkUpdatesStatus                            */
/*                                                                           */
/* Description        : This function calls the RM module to set the Status  */
/*                      of the bulk updates.                                 */
/*                                                                           */
/* Input(s)           : u4AppId - Application Identifier.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
VcmRmSetBulkUpdatesStatus (UINT4 u4AppId)
{
    return (RmSetBulkUpdatesStatus (u4AppId));
}
#endif
/*****************************************************************************/
/* Function Name      : VcmIssGetVRMacAddr                                   */
/*                                                                           */
/* Description        : This function obtains the Virtual Router Mac address */
/*                      from the ISS module.                                 */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Router Id.                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
VcmIssGetVRMacAddr (UINT4 u4ContextId, tMacAddr * pVRMacAddr)
{
    IssGetVRMacAddr (u4ContextId, pVRMacAddr);
    return;
}

/*****************************************************************************/
/* Function Name      : VcmIcchIsIcclInterface                               */
/*                                                                           */
/* Description        : This function calls the ICCH module to check whether */
/*                      the given index is ICCL interface                    */
/*                                                                           */
/* Input(s)           : i4FsVcmIfIndex - Interface index.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE/OSIX_FALSE                                 */
/*                                                                           */
/*****************************************************************************/
INT1
VcmIcchIsIcclInterface (INT4 i4FsVcmIfIndex)
{
#ifdef ICCH_WANTED
    if (IcchIsIcclInterface((UINT4) i4FsVcmIfIndex) == OSIX_TRUE)
    {
        return OSIX_TRUE;
    }
    else
    {
        return OSIX_FALSE;
    }
#else
    UNUSED_PARAM (i4FsVcmIfIndex);
    return OSIX_FALSE;
#endif
}

#endif
