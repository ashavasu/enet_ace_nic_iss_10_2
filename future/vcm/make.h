#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                     |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX ( Slackware 1.2.1 )                     |
# |                                                                          |
# |   DATE                   : 16th Jun 2006                                 |
# |                                                                          |
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################

VCM_SWITCHES = -DVCM_DEBUG -DVCM_TRAP_WANTED


TOTAL_OPNS = ${VCM_SWITCHES} $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

CFA_BASE_DIR   = ${BASE_DIR}/cfa2

VCM_BASE_DIR      = ${BASE_DIR}/vcm
VCM_SRC_DIR       = ${VCM_BASE_DIR}/src
VCM_INC_DIR       = ${VCM_BASE_DIR}/inc
VCM_OBJ_DIR       = ${VCM_BASE_DIR}/obj
SNMP_INC_DIR      = ${BASE_DIR}/inc/snmp
CFA_INCD          = ${CFA_BASE_DIR}/inc
CMN_INC_DIR       = ${BASE_DIR}/inc
FSAP_INC_DIR      = ${BASE_DIR}/fsap2/linux

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  = -I${VCM_INC_DIR}
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################
