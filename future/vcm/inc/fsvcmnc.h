/* $Id: fsvcmnc.h,v 1.1 2016/06/16 12:16:55 siva Exp $
    ISS Wrapper header
    module VCM-MIB

 */


#ifndef _H_i_VCM_MIB
#define _H_i_VCM_MIB
/********************************************************************
* FUNCTION NcFsVcmTraceOptionSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsVcmTraceOptionSet (
                INT4 i4FsVcmTraceOption );

/********************************************************************
* FUNCTION NcFsVcmTraceOptionTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsVcmTraceOptionTest (UINT4 *pu4Error,
                INT4 i4FsVcmTraceOption );

/********************************************************************
* FUNCTION NcFsVCNextFreeHlPortIdGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsVCNextFreeHlPortIdGet (
                INT4 i4FsVCId,
                INT4 *pi4FsVCNextFreeHlPortId );

/********************************************************************
* FUNCTION NcFsVCMacAddressGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsVCMacAddressGet (
                INT4 i4FsVCId,
                UINT1 *pFsVCMacAddress );

/********************************************************************
* FUNCTION NcFsVcAliasSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsVcAliasSet (
                INT4 i4FsVCId,
                UINT1 *pFsVcAlias );

/********************************************************************
* FUNCTION NcFsVcAliasTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsVcAliasTest (UINT4 *pu4Error,
                INT4 i4FsVCId,
                UINT1 *pFsVcAlias );

/********************************************************************
* FUNCTION NcFsVcCxtTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsVcCxtTypeSet (
                INT4 i4FsVCId,
                INT4 i4FsVcCxtType );

/********************************************************************
* FUNCTION NcFsVcCxtTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsVcCxtTypeTest (UINT4 *pu4Error,
                INT4 i4FsVCId,
                INT4 i4FsVcCxtType );

/********************************************************************
* FUNCTION NcFsVCStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsVCStatusSet (
                INT4 i4FsVCId,
                INT4 i4FsVCStatus );

/********************************************************************
* FUNCTION NcFsVCStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsVCStatusTest (UINT4 *pu4Error,
                INT4 i4FsVCId,
                INT4 i4FsVCStatus );

/********************************************************************
* FUNCTION NcFsVRMacAddressGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsVRMacAddressGet (
                INT4 i4FsVCId,
               	UINT1 *pFsVRMacAddress );

/********************************************************************
* FUNCTION NcFsVcOwnerSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsVcOwnerSet (
                INT4 i4FsVCId,
                UINT1 *pFsVcOwner );

/********************************************************************
* FUNCTION NcFsVcOwnerTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsVcOwnerTest (UINT4 *pu4Error,
                INT4 i4FsVCId,
                UINT1 *pFsVcOwner );

/********************************************************************
* FUNCTION NcFsVcIdSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsVcIdSet (
                INT4 i4FsVcmIfIndex,
                INT4 i4FsVcId );

/********************************************************************
* FUNCTION NcFsVcIdTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsVcIdTest (UINT4 *pu4Error,
                INT4 i4FsVcmIfIndex,
                INT4 i4FsVcId );

/********************************************************************
* FUNCTION NcFsVcHlPortIdGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsVcHlPortIdGet (
                INT4 i4FsVcmIfIndex,
                INT4 *pi4FsVcHlPortId );

/********************************************************************
* FUNCTION NcFsVcL2ContextIdSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsVcL2ContextIdSet (
                INT4 i4FsVcmIfIndex,
                INT4 i4FsVcL2ContextId );

/********************************************************************
* FUNCTION NcFsVcL2ContextIdTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsVcL2ContextIdTest (UINT4 *pu4Error,
                INT4 i4FsVcmIfIndex,
                INT4 i4FsVcL2ContextId );

/********************************************************************
* FUNCTION NcFsVcIfRowStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsVcIfRowStatusSet (
                INT4 i4FsVcmIfIndex,
                INT4 i4FsVcIfRowStatus );

/********************************************************************
* FUNCTION NcFsVcIfRowStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsVcIfRowStatusTest (UINT4 *pu4Error,
                INT4 i4FsVcmIfIndex,
                INT4 i4FsVcIfRowStatus );

/********************************************************************
* FUNCTION NcFsVcmL2VcNameGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsVcmL2VcNameGet (
                INT4 i4FsVcmL2VcId,
                INT4 i4FsVcmVlanId,
                UINT1 *pFsVcmL2VcName );

/********************************************************************
* FUNCTION NcFsVcmIPIfIndexGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsVcmIPIfIndexGet (
                INT4 i4FsVcmL2VcId,
                INT4 i4FsVcmVlanId,
                INT4 *pi4FsVcmIPIfIndex );

/* END i_VCM_MIB.c */


#endif
