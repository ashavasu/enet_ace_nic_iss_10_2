/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2009-2010                                          */
/*****************************************************************************/
/* $Id: sispmacr.h,v 1.4 2012/03/30 13:45:35 siva Exp $                */
/*                                                                           */
/*****************************************************************************/
/*    FILE  NAME            : sispmacr.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc   .                                */
/*    SUBSYSTEM NAME        : VCM module                                     */
/*    MODULE NAME           : VCM module                                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 31 Apr 2009                                    */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains the MACROs used by the      */
/*                            SISP functionality of VCM                      */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    31 APR 2009 / SISPTeam   Initial Create.                       */
/*---------------------------------------------------------------------------*/

#define VCM_SET_CMD 1
#define VCM_RESET_CMD 2

#define SISP_VLAN_BLOCK_NO(Vlan) ((Vlan - 1) / SISP_MAX_VLAN_PER_BLOCK)

#define SISP_VLAN_POS_IN_BLOCK(Vlan) ((Vlan - 1) % SISP_MAX_VLAN_PER_BLOCK) 

#define SISP_IF_CTX_INFO_MEMPOOL_ID gVcmSispGlobalInfo.u4SispIfCtxInfoPoolId
#define SISP_VLAN_IF_MAP_MEMPOOL_ID gVcmSispGlobalInfo.u4SispVlanIfMapPoolId
#define SISP_VLAN_LIST_MEMPOOL_ID   gVcmSispGlobalInfo.u1VlanListPoolId



#define SISP_PVC_TABLE_BLOCK(u4PhyPort, Vlan)\
    ((SISP_PVC_TABLE_ENTRY(u4PhyPort))->apVlanIfMapList[SISP_VLAN_BLOCK_NO(Vlan)])

#define SISP_PVC_TABLE_PORT_ENTRY(u4PhyPort, Vlan)\
    SISP_PVC_TABLE_BLOCK(u4PhyPort, Vlan)[SISP_VLAN_POS_IN_BLOCK(Vlan)]

#define SISP_PVC_TABLE_ENTRY(u4PhyPort) \
             gVcmSispGlobalInfo.apPVCTable[u4PhyPort]

#define SISP_IS_VALID_PHY_PORT(u4IfIndex)\
     (((u4IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS)|| \
       (u4IfIndex <= VCM_INVALID_VAL)) ? VCM_FALSE : VCM_TRUE)

#define SISP_IF_CTX_INFO_ALLOCATE_MEMBLK(pNode)  \
   (pNode=(tVcmSispIfCtxInfo *) MemAllocMemBlk(SISP_IF_CTX_INFO_MEMPOOL_ID))

#define SISP_IF_CTX_INFO_FREE_MEMBLK(pNode) \
{\
    MemReleaseMemBlock(SISP_IF_CTX_INFO_MEMPOOL_ID, (UINT1 *)pNode);\
    pNode = NULL;\
}

#define SISP_VLAN_IF_MAP_ALLOCATE_MEMBLK(pNode)  \
   (pNode=(tVcmIfMapEntry **) MemAllocMemBlk(SISP_VLAN_IF_MAP_MEMPOOL_ID))

#define SISP_VLAN_IF_MAP_FREE_MEMBLK(pNode) \
{\
    MemReleaseMemBlock(SISP_VLAN_IF_MAP_MEMPOOL_ID, (UINT1 *)pNode);\
    pNode = NULL;\
}

#define SISP_VLAN_LIST_ALLOCATE_MEMBLK(pNode)  \
   (pNode=(UINT1 *) MemAllocMemBlk(SISP_VLAN_LIST_MEMPOOL_ID))

#define SISP_VLAN_LIST_FREE_MEMBLK(pNode) \
{\
    MemReleaseMemBlock(SISP_VLAN_LIST_MEMPOOL_ID, (UINT1 *)pNode);\
    pNode = NULL;\
}

#define SISP_LOGICAL_PORT_TABLE gVcmSispGlobalInfo.SispLogicalPortTable

#define VCM_GET_SISP_STATUS gVcmSispGlobalInfo.u1SispModuleStatus

#define SISP_INDEX_LESSER         -1
#define SISP_INDEX_GREATER         1
#define SISP_INDEX_EQUAL           0
