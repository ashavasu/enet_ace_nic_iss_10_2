/*****************************************************************
*  * $Id: vcmsz.h,v 1.3 2014/11/20 10:54:45 siva Exp $
*********************************************************************/
enum {
    MAX_SISP_BLOCKS_SIZING_ID,
    MAX_SISP_IF_CXT_INFO_SIZE_SIZING_ID,
    MAX_SISP_VLAN_IF_MAP_SIZE_SIZING_ID,
    MAX_VCM_CTRLQ_MSG_MEMBLK_SIZE_SIZING_ID,
    MAX_VCM_IFMAPENTRY_SIZE_SIZING_ID,
    MAX_VCM_IPIFENTRY_SIZE_SIZING_ID,
    MAX_VCM_PROTO_REG_SIZE_SIZING_ID,
    MAX_VCM_VCINFO_SIZE_SIZING_ID,
    MAX_VCM_VCPORTENTRY_SIZE_SIZING_ID,
#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    MAX_VCM_LNX_VRF_STATUS_SIZE_SIZING_ID,
#endif
    VCM_MAX_SIZING_ID
};


#ifdef  _VCMSZ_C
tMemPoolId VCMMemPoolIds[ VCM_MAX_SIZING_ID];
INT4  VcmSizingMemCreateMemPools(VOID);
VOID  VcmSizingMemDeleteMemPools(VOID);
INT4  VcmSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _VCMSZ_C  */
extern tMemPoolId VCMMemPoolIds[ ];
extern INT4  VcmSizingMemCreateMemPools(VOID);
extern VOID  VcmSizingMemDeleteMemPools(VOID);
#endif /*  _VCMSZ_C  */


#ifdef  _VCMSZ_C
tFsModSizingParams FsVCMSizingParams [] = {
{ "UINT1[520]", "MAX_SISP_BLOCKS", sizeof(UINT1[520]),MAX_SISP_BLOCKS, MAX_SISP_BLOCKS,0 },
{ "tVcmSispIfCtxInfo", "MAX_SISP_IF_CXT_INFO_SIZE", sizeof(tVcmSispIfCtxInfo),MAX_SISP_IF_CXT_INFO_SIZE, MAX_SISP_IF_CXT_INFO_SIZE,0 },
{ "UINT1[2048]", "MAX_SISP_VLAN_IF_MAP_SIZE", sizeof(UINT1[2048]),MAX_SISP_VLAN_IF_MAP_SIZE, MAX_SISP_VLAN_IF_MAP_SIZE,0 },
{ "tVcmQMsg", "MAX_VCM_CTRLQ_MSG_MEMBLK_SIZE", sizeof(tVcmQMsg),MAX_VCM_CTRLQ_MSG_MEMBLK_SIZE, MAX_VCM_CTRLQ_MSG_MEMBLK_SIZE,0 },
{ "tVcmIfMapEntry", "MAX_VCM_IFMAPENTRY_SIZE", sizeof(tVcmIfMapEntry),MAX_VCM_IFMAPENTRY_SIZE, MAX_VCM_IFMAPENTRY_SIZE,0 },
{ "tVcmIpIfMapEntry", "MAX_VCM_IPIFENTRY_SIZE", sizeof(tVcmIpIfMapEntry),MAX_VCM_IPIFENTRY_SIZE, MAX_VCM_IPIFENTRY_SIZE,0 },
{ "tVcmHLProtoRegEntry", "MAX_VCM_PROTO_REG_SIZE", sizeof(tVcmHLProtoRegEntry),MAX_VCM_PROTO_REG_SIZE, MAX_VCM_PROTO_REG_SIZE,0 },
{ "tVirtualContextInfo", "MAX_VCM_VCINFO_SIZE", sizeof(tVirtualContextInfo),MAX_VCM_VCINFO_SIZE, MAX_VCM_VCINFO_SIZE,0 },
{ "tVcPortEntry", "MAX_VCM_VCPORTENTRY_SIZE", sizeof(tVcPortEntry),MAX_VCM_VCPORTENTRY_SIZE, MAX_VCM_VCPORTENTRY_SIZE,0 },
#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
{ "tVcmLnxvrfStatusQMsg", "MAX_VCM_LNX_VRF_STATUS_SIZE", sizeof(tVcmLnxvrfStatusQMsg),MAX_VCM_LNX_VRF_STATUS_SIZE, MAX_VCM_LNX_VRF_STATUS_SIZE,0 },
#endif
{"\0","\0",0,0,0,0}
};
#else  /*  _VCMSZ_C  */
extern tFsModSizingParams FsVCMSizingParams [];
#endif /*  _VCMSZ_C  */


