/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2004-2005                         */
/*****************************************************************************/
/*    FILE  NAME            : vcmglob.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : VCM module                                     */
/*    MODULE NAME           : VCM module                                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 16 Jun 2006                                    */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains the global variables        */
/*                            declaration used in VCM module.                */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    16 JUN 2006 / MITeam   Initial Create.                         */
/*---------------------------------------------------------------------------*/

#ifndef _VCMGLOB_H
#define _VCMGLOB_H

#ifdef _VCMMAIN_C

      /* Global VCM System Information */
      tVcmGlobals         gVcmGlobals;
      UINT1               gu1IsVcmInitialised = VCM_FALSE;
      tVcmIfMapEntry     *gapVcmIpIfMapEntry[VCM_MAX_IP_INTERFACES];

/* Sizing Params Structure */
tFsModSizingParams gFsVcmSizingParams [] =
{
    {"VCM_CTRLQ_MSG", "VCM_CTRLQ_MSG_MEMPOOL",
     sizeof (tVcmQMsg), VCM_CTRLQ_MSG_MEMBLK_COUNT,
     VCM_CTRLQ_MSG_MEMBLK_COUNT, 0},

    {"VCM_VCINFO", "VCM_VCINFO_MEMPOOL",
     VCM_VCINFO_MEMBLK_SIZE, SYS_DEF_MAX_NUM_CONTEXTS,
     SYS_DEF_MAX_NUM_CONTEXTS, 0},

    /* Since non embedded RBTree is used, memory is added for
     * RB Tree allocation
     */
    {"VCM_VCINFO_TREE", "VCM_VCINFO_RBTREE",
     sizeof (tRBNode), SYS_DEF_MAX_NUM_CONTEXTS,
     SYS_DEF_MAX_NUM_CONTEXTS, 0},

    {"VCM_IFMAPENTRY", "VCM_IFMAPENTRY_MEMPOOL",
     VCM_IFMAPENTRY_MEMBLK_SIZE, VCM_MAX_PORTS_IN_SYS,
     VCM_MAX_PORTS_IN_SYS, 0},

    {"VCM_VCPORTENTRY", "VCM_VCPORTENTRY_MEMPOOL",
     VCM_VCPORTENTRY_MEMBLK_SIZE, VCM_MAX_PORTS_IN_SYS,
     VCM_MAX_PORTS_IN_SYS, 0},

    {"VCM_IFMAPENTRY_TREE", "VCM_IFMAPENTRY_RBTREE",
     sizeof (tRBNode), VCM_MAX_PORTS_IN_SYS,
     VCM_MAX_PORTS_IN_SYS, 0},

    {"0", "0", 0,0,0,0}
};

tFsModSizingInfo gFsVcmSizingInfo;

#else /* _VCMMAIN_C */
      /* Global VCM System Information */
      extern tVcmGlobals      gVcmGlobals;
      extern UINT1            gu1IsVcmInitialised;
      extern tVcmIfMapEntry  *gapVcmIpIfMapEntry[VCM_MAX_IP_INTERFACES];
      extern tFsModSizingParams gFsVcmSizingParams [];
      extern tFsModSizingInfo gFsVcmSizingInfo;

#endif /* _VCMMAIN_C */

#endif /* _VCMGLOB_H */

