/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: vcmmacr.h,v 1.14 2014/11/20 10:54:45 siva Exp $
 *
 * Description: This file contains the macros used by
 *              VCM module.
 *
 ******************************************************************/

#ifndef _VCMMACR_H
#define _VCMMACR_H

/* General Macros */
#define   VCM_UNUSED(x)         {x = x;}
#define   VCM_SELF              SELF
#define   VCM_IS_INITIALISED()  gu1IsVcmInitialised

#define VCM_AUDIT_SHOW_CMD    "end;show switch interfaces > "
#define VCM_CLI_EOF                  2
#define VCM_CLI_NO_EOF                 1
#define VCM_CLI_RDONLY               OSIX_FILE_RO
#define VCM_CLI_WRONLY               OSIX_FILE_WO
#define VCM_CLI_MAX_GROUPS_LINE_LEN  200

/* Task, Queue creation related functions*/
#define   VCM_TASK_ID                gVcmGlobals.VcmTaskId
#define   VCM_GET_TASK_ID            OsixTskIdSelf
#define   VCM_TASK_CREATE            OsixTskCrt
#define   VCM_CREATE_QUEUE           OsixQueCrt
#define   VCM_DELETE_QUEUE           OsixQueDel
#define   VCM_SEND_TO_QUEUE          OsixQueSend
#define   VCM_RECV_FROM_QUEUE        OsixQueRecv
#define   VCM_OSIX_MSG_NORMAL        OSIX_MSG_NORMAL
#define   VCM_OSIX_WAIT_FOREVER      OSIX_WAIT_FOREVER

#define   VCM_AUDIT_TASK_PRIORITY     240 /* should have lower priority */
#define   VCM_AUDIT_TASK            ((UINT1*)"VCMAUD")

#define VCM_AUDIT_FILE_ACTIVE "/tmp/vcm_output_file_active"
#define VCM_AUDIT_FILE_STDBY "/tmp/vcm_output_file_stdby"
/* Defines of Library functions used */
#define   VCM_MEMCPY               MEMCPY
#define   VCM_MEMCMP               MEMCMP
#define   VCM_MEMSET               MEMSET

#define   VCM_STRCPY               STRCPY
#define   VCM_STRNCPY              STRNCPY
#define   VCM_STRLEN               STRLEN
#define   VCM_SPRINTF              SPRINTF 

/* FSAP related macros */
#define tVcmMemPoolId          tMemPoolId
#define tVcmOsixSemId          tOsixSemId
#define tVcmOsixTaskId         tOsixTaskId

/* Red Black Tree related Macros */
typedef tRBTree                tVcmRBTree;
typedef tRBNode                tVcmRBNode;
typedef tRBElem                tVcmRBElem;

#define VCM_RB_TREE_CREATE        RBTreeCreate
#define VCM_RB_TREE_CREATE_EMBED  RBTreeCreateEmbedded
#define VCM_RB_TREE_DELETE        RBTreeDelete
#define VCM_RB_TREE_ADD           RBTreeAdd
#define VCM_RB_TREE_REMOVE        RBTreeRemove
#define VCM_RB_TREE_GET           RBTreeGet
#define VCM_RB_TREE_GETFIRST      RBTreeGetFirst
#define VCM_RB_TREE_GETNEXT       RBTreeGetNext
#define VCM_RB_TREE_WALK          RBTreeWalk

/* Singly Linked List related Macros */
#define tVcmSLL     tTMO_SLL 
#define tVcmSLLNode tTMO_SLL_NODE
#define tVcmDLL     tTMO_DLL
#define tVcmDLLNode tTMO_DLL_NODE

#define VCM_SLL_INIT(pList)                TMO_SLL_Init(pList)
#define VCM_SLL_INIT_NODE(pNode)           TMO_SLL_Init_Node(pNode)
#define VCM_SLL_ADD(pList, pNode)          TMO_SLL_Add(pList, pNode)
#define VCM_SLL_DELETE(pList, pNode)       TMO_SLL_Delete (pList, pNode)
#define VCM_SLL_COUNT(pList)               TMO_SLL_Count(pList)
#define VCM_SLL_NEXT(pList, pNode)         TMO_SLL_Next(pList,pNode)
#define VCM_SLL_FIRST(pList)               TMO_SLL_First(pList)
#define VCM_SLL_GET(pList)                 TMO_SLL_Get(pList)
#define VCM_SLL_LAST(pList)                TMO_SLL_Last(pList)
#define VCM_SLL_IS_NODE_IN_LIST(pNode)     TMO_SLL_Is_Node_In_List(pNode)
#define VCM_SLL_SCAN(pList,pNode,TypeCast) TMO_SLL_Scan(pList,pNode,TypeCast)
#define VCM_DLL_INIT(pList)                TMO_DLL_Init(pList)
#define VCM_DLL_INIT_NODE(pNode)           TMO_DLL_Init_Node(pNode)
#define VCM_DLL_ADD(pList, pNode)          TMO_DLL_Add(pList, pNode)
#define VCM_DLL_DELETE(pList, pNode)       TMO_DLL_Delete (pList, pNode)
#define VCM_DLL_COUNT(pList)               TMO_DLL_Count(pList)
#define VCM_DLL_SCAN(pList,pNode,TypeCast) TMO_DLL_Scan(pList,pNode,TypeCast)
#define VCM_DLL_GET(pList)                 TMO_DLL_Get(pList)
   
/* Memory Pool related macros */
#define   VCM_VCINFO_MEMBLK_SIZE      (sizeof(tVirtualContextInfo))
#define   VCM_IFMAPENTRY_MEMBLK_SIZE  (sizeof(tVcmIfMapEntry))
#define   VCM_VCPORTENTRY_MEMBLK_SIZE (sizeof( tVcPortEntry ))
#define   VCM_IPIFENTRY_MEMBLK_SIZE (sizeof( tVcmIpIfMapEntry ))

/* Memory Pool Ids definitions */
#define   VCM_VCINFO_MEMPOOL_ID      gVcmGlobals.u4VcPoolId
#define   VCM_IFMAPENTRY_MEMPOOL_ID  gVcmGlobals.u4IfMapPoolId 
#define   VCM_VCPORTENTRY_MEMPOOL_ID gVcmGlobals.u4VcPortPoolId
#define   VCM_IPIFENTRY_MEMPOOL_ID   gVcmGlobals.u4IpIfPoolId
#define   VCM_PROTO_REG_MEMPOOL_ID   gVcmGlobals.u4VcProtoRegPoolId
#define   VCM_CTRLQ_MEMPOOL_ID        gVcmGlobals.u4CtrlQMemPooIId 
#define   VCM_LNX_VRF_STATUS_MEMPOOL_ID        gVcmGlobals.u4LnxVrfStatusPooIId 

/* Queue Id definition */
#define   VCM_CTRLQ_ID                 gVcmGlobals.u4CtrlQId
#define   VCM_LNX_VRFQ_ID              gVcmGlobals.u4LnxVrfQId

#define   VCM_CTRLQ_MSG_MEMBLK_COUNT   VCM_CTRLQ_DEPTH 

#define   VCM_IS_MCASTADDR(pMacAddr) \
        (((pMacAddr[0] & 0x01) == 0) ? \
         FAILURE : SUCCESS)

/* Return Values */
#define   VCM_MEM_SUCCESS         MEM_SUCCESS
#define   VCM_MEM_FAILURE         MEM_FAILURE

/* Macros for creating memory pools */
#define   VCM_MEMORY_TYPE      MEM_DEFAULT_MEMORY_TYPE

#define   VCM_CREATE_MEM_POOL(x,y,pPoolId)\
             MemCreateMemPool(x,y,VCM_MEMORY_TYPE,(tVcmMemPoolId*)pPoolId)

/* Macros for deleting Memory Pools */
#define   VCM_DELETE_MEM_POOL(pPoolId)\
             MemDeleteMemPool((tVcmMemPoolId) pPoolId)

#define   VCM_DELETE_QMSG_MEM_POOL(PoolId) \
               MemDeleteMemPool(PoolId)

/* Macros for allocating memory block */
#define   VCM_ALLOC_MEM_BLOCK(poolid,ppnode) \
             MemAllocateMemBlock((poolid), (UINT1 **)(ppnode))

#define   VCM_ALLOCATE_VCINFO_MEMBLK()  \
    (tVirtualContextInfo *) MemAllocMemBlk (VCM_VCINFO_MEMPOOL_ID)

#define   VCM_ALLOCATE_IFMAPENTRY_MEMBLK()  \
    (tVcmIfMapEntry *) MemAllocMemBlk (VCM_IFMAPENTRY_MEMPOOL_ID)

#define   VCM_ALLOCATE_VCPORTENTRY_MEMBLK() \
    (tVcPortEntry *) MemAllocMemBlk( VCM_VCPORTENTRY_MEMPOOL_ID)

#define   VCM_ALLOC_CTRLQ_MEM_BLOCK(ppNode,ptype) \
    ppNode = (ptype *) \
(MemAllocMemBlk(VCM_CTRLQ_MEMPOOL_ID));

#define   VCM_ALLOC_LNX_VRF_MEM_BLOCK(ppNode,ptype) \
    ppNode = (ptype *) \
(MemAllocMemBlk(VCM_LNX_VRF_STATUS_MEMPOOL_ID));

#define   VCM_ALLOCATE_IPIFENTRY_MEMBLK() \
          (tVcmIpIfMapEntry *) MemAllocMemBlk( VCM_IPIFENTRY_MEMPOOL_ID)

#define   VCM_ALLOCATE_PROTOREG_ENTRY_MEMBLK() \
          (tVcmHLProtoRegEntry *) MemAllocMemBlk( VCM_PROTO_REG_MEMPOOL_ID)


/* Macros for freeing memory block */
#define   VCM_RELEASE_MEM_BLOCK(poolid, pNode) \
              MemReleaseMemBlock((poolid), (UINT1 *)(pNode))

#define   VCM_RELEASE_VCINFO_MEMBLK(pNode) \
           MemReleaseMemBlock ( VCM_VCINFO_MEMPOOL_ID, \
                              (UINT1 *)pNode)
                              
#define   VCM_RELEASE_IFMAPENTRY_MEMBLK(pNode) \
           MemReleaseMemBlock (VCM_IFMAPENTRY_MEMPOOL_ID,  \
                                (UINT1 *)pNode) 

#define   VCM_RELEASE_VCPORTENTRY_MEMBLK(pNode) \
             MemReleaseMemBlock( VCM_VCPORTENTRY_MEMPOOL_ID, \
                               (UINT1 *)pNode)

#define   VCM_RELEASE_CTRLQ_MEM_BLOCK(pNode)  \
             MemReleaseMemBlock (VCM_CTRLQ_MEMPOOL_ID, (UINT1 *)pNode)
    
#define   VCM_RELEASE_IPIFENTRY_MEMBLK(pNode) \
             MemReleaseMemBlock( VCM_IPIFENTRY_MEMPOOL_ID, \
                               (UINT1 *)pNode)

#define   VCM_RELEASE_PROTOREG_ENTRY_MEMBLK(pNode) \
             MemReleaseMemBlock( VCM_PROTO_REG_MEMPOOL_ID, \
                               (UINT1 *)pNode)

#define   VCM_RELEASE_LNX_VRF_MEM_BLOCK(pNode) \
             MemReleaseMemBlock( VCM_LNX_VRF_STATUS_MEMPOOL_ID, \
                               (UINT1 *)pNode)

#define   VCM_QMSG_TYPE(pQMsg)    pQMsg->u2MsgType
#define   VCM_LNXVRFQMSG_TYPE(pQMsg)    pQMsg->u1MsgType
#define   VCM_LNXVRFQMSG_STATUS(pQMsg)    pQMsg->u4Status

/* SEM Related Macros */
#define   VCM_SEM_ID      gVcmGlobals.VcmSemId
#define   VCM_CONF_SEM_ID gVcmGlobals.VcmConfSemId

#define VCM_CREATE_SEM(pu1Sem, u4Count, u4Flags, pSemId) \
        OsixCreateSem ((pu1Sem), (u4Count), (u4Flags), (pSemId))

#define VCM_DELETE_SEM(SemId)  OsixSemDel ((SemId))

#define VCM_TAKE_SEM(SemId) OsixSemTake(SemId)

#define VCM_RELEASE_SEM(SemId) OsixSemGive(SemId)

/* Other Macros used by VCM Module */
#define VCM_STATUS(pIfMap)             pIfMap->u1RowStatus
#define VCM_NEXTFREE_HLPORTID(pVc)     pVc->u2NextFreeHlPortId
#define VCM_IFMAP_HLPORTID(pIfMap)     pIfMap->u2HlPortId

#define VCM_IF_MAP_TABLE               gVcmGlobals.VcmIfMapTable
#define VCM_CXT_TO_IPIF_MAP_TABLE      gVcmGlobals.VcmCxtAndVlanToIPIfMapTable
#define VCM_CONTEXT_TABLE              gVcmGlobals.VcmContextTable
    
/* Event Related Macros */
#define VCM_RECEIVE_EVENT     OsixEvtRecv
#define VCM_SEND_EVENT        OsixEvtSend
#define VCM_OSIX_NO_WAIT      OSIX_NO_WAIT
    
#define VCM_INVALID_IFINDEX            0
#define VCM_ISALPHA(c) (((c) >= 'a' && (c) <= 'z') || ((c) >= 'A' && (c) <= 'Z'))    
#define VCM_OBJECT_NAME_MAX_LENGTH     256
    
#endif /* _VCMMACR_H */


