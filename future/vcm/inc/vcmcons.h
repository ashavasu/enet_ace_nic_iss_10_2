/* $Id: vcmcons.h,v 1.12 2014/11/20 10:54:44 siva Exp $*/
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                 */
/* Licensee Aricent Inc., 2004-2005                         */
/*****************************************************************************/
/*    FILE  NAME            : vcmcons.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : VCM module                                     */
/*    MODULE NAME           : VCM module                                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 16 Jun 2006                                    */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains defined constants used      */
/*                            by VCM Module.                                 */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    16 JUN 2006 / MITeam    Initial Create.                        */
/*---------------------------------------------------------------------------*/

#ifndef _VCMCONS_H
#define _VCMCONS_H
 
#define VCM_ALL_VCS          0xfffffffe

#define VCM_MAC_ADDR_SIZE    6

#define VCM_SEM_NAME         ((const UINT1 *)"VCM")
#define VCM_CONF_SEM_NAME    ((const UINT1 *)"VCMCONF")
#define VCM_SEM_COUNT        1
#define VCM_SEM_FLAGS        OSIX_DEFAULT_SEM_MODE
#define VCM_SEM_NODE_ID      SELF

#define VCM_MSG_EVENT              0x0001
#define VCM_RED_BULK_UPD_EVENT     0x0002
#define VCM_LNX_VRF_STATUS_EVENT   0x0004

#define VCM_ALIAS_LEN      32
#define VCM_RM_QMSG          1

#define VCM_RM_QMSG          1
#define VCM_LNX_VRF_CREATE_CONTEXT          1
#define VCM_LNX_VRF_DELETE_CONTEXT          2
#define VCM_LNX_VRF_IF_MAP_CONTEXT          3
#define VCM_LNX_VRF_IF_UNMAP_CONTEXT        4


#define VCM_INIT_VAL         0

#define VCM_INVALID_VAL      0
#define VCM_INVALID_HLPORT   65535

#define VCM_MIN_TRACE_VAL    0
#define VCM_MAX_TRACE_VAL    0x00ff

#define VCM_INVALID_CONTEXT_TYPE     0xff
#define VCM_MAX_PROTO_REG            20
#define VCM_L2_IF_MAPPING            1
#define VCM_L3_IF_MAPPING            2
#define VCM_MAX_IP_INTERFACES     IPIF_MAX_LOGICAL_IFACES

/* Macros for layer 3 interface interface mapping */
#define VCMALIAS_MAX_ARRAY_LEN    VCM_ALIAS_MAX_LEN + 4
#define VCM_INVALID_STATUS        0
#define VCM_OBJECT_NAME_LEN 256
#define VCM_CREATE_CONTEXT_TRAP_OID "1.3.6.1.4.1.2076.93.2.1"
#define VCM_DELETE_CONTEXT_TRAP_OID "1.3.6.1.4.1.2076.93.2.2"

typedef enum
{
    VCM_CREATE_CONTEXT_TRAP=1,
    VCM_DELETE_CONTEXT_TRAP=2
} tCfaTrapVal;
#endif /* _VCMCONS_H */
