
/* $Id: vcmproto.h,v 1.27 2015/06/29 05:51:12 siva Exp $*/
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2004-2005                         */
/*****************************************************************************/
/*    FILE  NAME            : vcmproto.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : VCM module                                     */
/*    MODULE NAME           : VCM module                                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 16 Jun 2006                                    */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains the function prototypes for */
/*                            all the functions in VCM module.               */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    16 JUN 2006 / MITeam   Initial Create.                         */
/*---------------------------------------------------------------------------*/

#ifndef _VCMPROTO_H
#define _VCMPROTO_H

/* Prototypes from vcmmain.c */

INT4 VcmHandleModuleStart (VOID);

INT4 VcmHandleModuleShutdown (VOID);

INT4 VcmTaskInit (VOID);

INT4 VcmTaskDeInit (VOID);

INT4 VcmDeInit (VOID);
    
PUBLIC VOID VcmIpInit PROTO ((VOID));
/* Prototypes from vcmifmap.c */

INT4
VcmCreateIfMapEntry PROTO ((UINT4 ));

INT4 
VcmDeleteIfMapEntry PROTO ((UINT4 ));

tVcmIfMapEntry *
VcmRowCreate PROTO ((UINT4 ));

/* Prototypes from vcmiftre.c */

INT4 
VcmIfMapTableInit PROTO ((VOID));

VOID 
VcmIfMapTableDeInit PROTO ((VOID));

INT4 
VcmAddIfMapEntry PROTO ((VOID *));

INT4 
VcmAddL2CxToIPIfMapEntry PROTO ((VOID *));

INT4 
VcmRemoveIfMapEntry PROTO ((VOID *));

INT4 
VcmRemoveL2CxToIPIfMapEntry PROTO ((VOID *));

VOID *
VcmFindIfMapEntry PROTO ((VOID *));

tVcmIfMapEntry *
VcmGetFirstIfMap PROTO ((VOID));

tVcmIfMapEntry *
VcmGetNextIfMap PROTO ((VOID *));

/* Prototypes from vcmvcs.c */

INT4
VcmCreateVirtualContext PROTO ((UINT4 ));

INT4 
VcmCreateContext PROTO ((UINT4 ));

INT4 
VcmDeleteVirtualContext PROTO ((UINT4 ));

INT4 
VcmAddToVcPortList PROTO ((tVcmIfMapEntry *));

INT4 
VcmDeleteFromVcPortList PROTO ((tVcmIfMapEntry *));

VOID 
VcmRemoveIfMapEntriesForContext PROTO ((UINT4 ));

INT4
VcmMakeContextActive PROTO ((UINT4 u4VcNum, UINT4 u4CurrRowStatus));
    
INT4
VcmMakeCxtActiveFromNotReady PROTO ((UINT4 u4VcNum));

INT4
VcmMakeCxtActiveFromNIS PROTO ((UINT4 u4VcNum));

INT4
VcmIndicateL3CxtUpdate PROTO ((UINT4 u4VcNum, UINT1 u1UpdateType));

INT4
VcmIndicateL2CxtUpdate PROTO ((UINT4 u4VcNum, UINT1 u1UpdateType));

INT4
VcmValidateL3IfMapping PROTO ((UINT4 u4IfIndex, UINT4 u4ContextId, 
                               UINT4 *pu4ErrorCode));
    
INT4 
VcmHandleL3IfaceMapping PROTO ((UINT4 u4IfIndex));

INT4
VcmAddToIpIfList PROTO ((UINT4 u4ContextId, tVcmIfMapEntry * pIfMapEntry));

/* Prototypes from vcmvctre.c */

INT4 
VcmContextTableInit PROTO ((VOID));

VOID 
VcmContextTableDeInit PROTO ((VOID));

INT4 
VcmAddVcEntry PROTO ((VOID *));

INT4 
VcmRemoveVcEntry PROTO ((VOID *));

VOID *
VcmFindVcEntry PROTO ((VOID *));

tVirtualContextInfo *
VcmGetFirstVc PROTO ((VOID));

tVirtualContextInfo *
VcmGetNextVc PROTO ((VOID *));
INT4 VcmDeleteVirtualContextIncDefault (UINT4 u4VcNum);

tVcmIfMapEntry *
VcmGetFirstCxtToIpIfMapEntry PROTO ((VOID));

tVcmIfMapEntry *
VcmGetNextCxtToIpIfMapEntry PROTO ((VOID *Temp));

VOID *
VcmFindL2CxtToIPIfMapEntry PROTO ((VOID *pNode));

/* Prototypes from vcml2if.c */
INT4
VcmCreateInterface (UINT4 );

/* Prototypes from vcmport.c */
INT4
VcmCfaCliGetIfName PROTO ((UINT4 , INT1 *));
/*
INT4
VcmCfaCliGetIfNameInCxt PROTO ((UINT4, UINT4 , INT1 *));
*/
INT4
VcmCfaCliConfGetIfName PROTO ((UINT4 u4IfIndex, INT1 *pi1IfName));

INT4
VcmCfaGetIfInfo PROTO ((UINT4 , tCfaIfInfo * ));

INT4
VcmCfaIsVirtualInterface PROTO ((UINT4 u4IfIndex));

VOID
VcmIssGetContextMacAddress PROTO ((UINT4 , tMacAddr ));

INT4
VcmL2IwfGetBridgeMode PROTO ((UINT4 , UINT4 *));

INT4
VcmL2IwfIsPortChannelConfigAllowed PROTO ((UINT4 u4IfIndex));

INT4 
L2CreateVirtualContext PROTO ((UINT4 ));

INT4 
L2DeleteVirtualContext PROTO ((UINT4 ));

INT4 
L2CreateInterface PROTO ((UINT4, UINT4, UINT2 ));

INT4 
L2DeleteInterface PROTO ((UINT4 ));

VOID 
L2UpdateAliasName PROTO ((UINT4 ));

INT4
L2IsPortInPortChannel PROTO ((UINT4));

INT4
VcmRmEnqChkSumMsgToRm PROTO ((UINT2, UINT2 ));

INT4
VcmGetShowCmdOutputAndCalcChkSum (UINT2 *);
INT4
VcmCliGetShowCmdOutputToFile (UINT1 *);

INT4
VcmCliCalcSwAudCheckSum (UINT1 *, UINT2 *);

INT1
VcmCliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen, INT2 *pi2ReadLen);

#ifdef L2IWF_WANTED
UINT4
VcmRmEnqMsgToRmFromAppl PROTO ((tRmMsg * , UINT2 , UINT4 , UINT4 ));

UINT4
VcmRmGetNodeState PROTO ((VOID));

UINT1
VcmRmGetStandbyNodeCount PROTO ((VOID));

UINT1
VcmRmGetStaticConfigStatus PROTO ((VOID));

UINT1
VcmRmHandleProtocolEvent PROTO ((tRmProtoEvt * ));

UINT4
VcmRmRegisterProtocols PROTO ((tRmRegParams * ));

UINT4
VcmRmDeRegisterProtocols PROTO ((VOID));

UINT4
VcmRmReleaseMemoryForMsg PROTO ((UINT1 *));

INT4
VcmRmSetBulkUpdatesStatus PROTO ((UINT4 ));
#endif

VOID
VcmIssGetVRMacAddr PROTO ((UINT4 u4ContextId, tMacAddr * pVRMacAddr));

/* Prototypes from vcmutil.c */
INT4
VcmUtilGetIfIndexFromLocalPort PROTO ((UINT4 u4ContextId, UINT2 u2LocalPort,
           UINT4 *pu4IfIndex));

INT4
VcmGetNextAvailableVcNum PROTO ((UINT4 *pu4FreeVcNum));

INT4
VcmGetFirstVcId PROTO ((UINT4 *));

INT4
VcmGetNextVcId PROTO ((UINT4 , UINT4 *));

INT4
VcmGetVcNextFreeHlPortId PROTO ((UINT4 , UINT2 *));

INT4
VcmGetVcAlias PROTO ((UINT4 , UINT1 *));

INT4
VcmGetVcRowStatus PROTO ((UINT4 , INT4 *));

INT4
VcmGetFirstIfMapIfIndex PROTO ((UINT4 *));

INT4
VcmGetNextIfMapIfIndex PROTO ((UINT4 , UINT4 *));

INT4
VcmGetIfMappingType PROTO ((UINT4 , INT4 *));

INT4
VcmGetL2ContextId PROTO ((UINT4 , INT4 *));

INT4
VcmGetIfMapEntry PROTO ((UINT4 u4IfIndex, tVcmIfMapEntry *pVcmIfMapEntry));

tVcmIfMapEntry *
VcmGetIfMapEntryFromLocalPort PROTO ((UINT4 u4ContextId, UINT2 u2LocalPort)); 

INT4
VcmGetIfMapStatus PROTO ((UINT4 , INT4 *));

INT4
VcmSetIfMapVcId PROTO ((UINT4 , UINT4 ));

INT4
VcmSetIfMapStatus PROTO ((UINT4 , INT4 ));

INT4
VcmIncrementPortCount PROTO ((UINT4 ));

INT4
VcmDecrementPortCount PROTO ((UINT4 ));

INT4
VcmGetVcPortCount PROTO ((UINT4 , UINT2 *));


INT4
VcmGetVcCfgCxtType PROTO ((UINT4 u4ContextId, INT4 *pi4CfgCxtType));

INT4
VcmIsContextExist PROTO ((UINT4 u4ContextId));

INT4
VcmIsMaxL3CxtCreated PROTO ((VOID));

INT4
VcmGetIpIfCount PROTO ((UINT4 u4ContextId, UINT2 *pu2PortCount));

INT4
VcmIsL3IfMappedToL2Cxt PROTO ((UINT4 u4ContextId));

VOID
VcmHandleL3IfMapForCxtDelete PROTO ((UINT4 u4VcNum, tVcmIfMapEntry *pIfMapEntry));

INT4                                                     
VcmGetFirstL2CxtToIPIfaceMapIndex PROTO ((UINT4 *pu4L2CxtId, UINT2 *pu2VlanId));

INT4
VcmGetNextL2CxtToIPIfaceMapIndex PROTO ((UINT4 u4L2CxtId, UINT2 u2VlanId,
                                         UINT4 *pu4L2CxtId, UINT2 *pu2VlanId));
INT4
VcmIsL2CxtToIPIfMapExist PROTO ((UINT4 u4L2CxtId, UINT2 u2VlanId));

INT4
VcmGetIPIfIndexForL2CxtMap PROTO ((UINT4 u4L2CxtId, UINT2 u2VlanId, 
                                   UINT4 *pu4IfIndex));


INT4
VcmIncrementSispPortCount PROTO ((UINT4 ));

INT4
VcmDecrementSispPortCount PROTO ((UINT4 ));

VOID VcmAssignMempoolIds PROTO ((VOID));
#ifdef MBSM_WANTED
INT4 
VcmMbsmUpdateOnCardInsertion PROTO ((tMbsmPortInfo *pPortInfo, 
                                     tMbsmSlotInfo *pSlotInfo));

INT4 VcmSispMbsmUpdateOnCardInsertion (tMbsmPortInfo * pPortInfo,
           tMbsmSlotInfo * pSlotInfo);

INT4
VcmSispMbsmUpdateOnCardRemoval (tMbsmPortInfo * pPortInfo,
    tMbsmSlotInfo * pSlotInfo);
#endif
#ifdef VRF_WANTED

PUBLIC VOID VcmIpIfMapInit PROTO ((UINT4 u4IpIfIndex));

PUBLIC INT4 VcmIpIfMapCheck PROTO ((UINT4 u4IpIfIndex));

PUBLIC VOID VcmHLProtoRegInit PROTO ((VOID));

/* Prototypes from vcml3if.c */
VOID 
VcmL3DeleteVirtualContextIndication PROTO ((UINT4));

VOID 
VcmL3DeleteInterfaceIndication PROTO ((UINT4));

/* Prototypes for vcmipifmap.c */
PUBLIC VOID VcmIpAddIfMap PROTO ((tVirtualContextInfo * pVirtualContextInfo,
                    tVcmIpIfMapEntry * pVcmIpIfMapEntry));

PUBLIC VOID VcmIpDeleteIfMap PROTO ((UINT4 u4IpIfIndex));

PUBLIC VOID VcmIpDeleteIfMapEntries PROTO ((UINT4 u4ContextId));

#endif
#ifdef NPAPI_WANTED
INT4
VcmUpdatePortToContextInHw PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, 
                                   UINT1 u1Status));
#endif
PUBLIC VOID
VcmSnmpSendTrap PROTO ((UINT1, INT1 *, UINT1 ,VOID *));

VOID
VcmDelIfMapEntryExt ( UINT4 u4IfIndex);
VOID VcmDeleteMemRelease ( UINT4 u4VcNum);
VOID
VcmCreateContextInNp PROTO ((UINT4));
VOID
VcmDeleteContextInNp PROTO ((UINT4));
VOID VcmIfMapContextInNp PROTO ((UINT4));
INT1
VcmIcchIsIcclInterface (INT4 i4FsVcmIfIndex);
#endif /* _VCMPROTO_H */
