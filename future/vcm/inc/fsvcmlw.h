/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsvcmlw.h,v 1.11 2017/11/14 07:31:19 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVcmTraceOption ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVcmTraceOption ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVcmTraceOption ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsVcmTraceOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsVcmConfigTable. */
INT1
nmhValidateIndexInstanceFsVcmConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsVcmConfigTable  */

INT1
nmhGetFirstIndexFsVcmConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsVcmConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVCNextFreeHlPortId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsVCMacAddress ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetFsVcAlias ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsVcCxtType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsVCStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsVRMacAddress ARG_LIST((INT4 ,tMacAddr * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVcAlias ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsVcCxtType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsVCStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVcAlias ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsVcCxtType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsVCStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsVcmConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsVcmIfMappingTable. */
INT1
nmhValidateIndexInstanceFsVcmIfMappingTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsVcmIfMappingTable  */

INT1
nmhGetFirstIndexFsVcmIfMappingTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsVcmIfMappingTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVcId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsVcHlPortId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsVcL2ContextId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsVcIfRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVcId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsVcL2ContextId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsVcIfRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVcId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsVcL2ContextId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsVcIfRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsVcmIfMappingTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsVcmL2CxtAndVlanToIPIfaceMapTable. */
INT1
nmhValidateIndexInstanceFsVcmL2CxtAndVlanToIPIfaceMapTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsVcmL2CxtAndVlanToIPIfaceMapTable  */

INT1
nmhGetFirstIndexFsVcmL2CxtAndVlanToIPIfaceMapTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsVcmL2CxtAndVlanToIPIfaceMapTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVcmL2VcName ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsVcmIPIfIndex ARG_LIST((INT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for FsVcConfigExtTable. */
INT1
nmhValidateIndexInstanceFsVcConfigExtTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsVcConfigExtTable  */

INT1
nmhGetFirstIndexFsVcConfigExtTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsVcConfigExtTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVcOwner ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVcOwner ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVcOwner ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsVcConfigExtTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsVcmFreeVcIdTable. */
INT1
nmhValidateIndexInstanceFsVcmFreeVcIdTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsVcmFreeVcIdTable  */

INT1
nmhGetFirstIndexFsVcmFreeVcIdTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsVcmFreeVcIdTable ARG_LIST((INT4 , INT4 *));

/* Proto Validate Index Instance for FsVcmCounterTable. */
INT1
nmhValidateIndexInstanceFsVcmCounterTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsVcmCounterTable  */

INT1
nmhGetFirstIndexFsVcmCounterTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsVcmCounterTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVcCounterRxUnicast ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVcCounterRxMulticast ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVcCounterRxBroadcast ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsVcCounterStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsVcClearCounter ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVcCounterStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsVcClearCounter ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVcCounterStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsVcClearCounter ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsVcmCounterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
