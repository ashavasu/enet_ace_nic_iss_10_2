/****************************************************************
 *
 * $Id: fsvcmwr.h,v 1.10 2017/11/14 07:31:19 siva Exp $
 *
 **************************************************************/

#ifndef _FSVCMWR_H
#define _FSVCMWR_H

VOID RegisterFSVCM(VOID);

VOID UnRegisterFSVCM(VOID);
INT4 FsVcmTraceOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsVcmTraceOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsVcmTraceOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVcmTraceOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

INT4 GetNextIndexFsVcmConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsVCNextFreeHlPortIdGet(tSnmpIndex *, tRetVal *);
INT4 FsVCMacAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsVcAliasGet(tSnmpIndex *, tRetVal *);
INT4 FsVcCxtTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsVCStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsVRMacAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsVcAliasSet(tSnmpIndex *, tRetVal *);
INT4 FsVcCxtTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsVCStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsVcAliasTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVcCxtTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVCStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVcmConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsVcmIfMappingTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsVcIdGet(tSnmpIndex *, tRetVal *);
INT4 FsVcHlPortIdGet(tSnmpIndex *, tRetVal *);
INT4 FsVcL2ContextIdGet(tSnmpIndex *, tRetVal *);
INT4 FsVcIfRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsVcIdSet(tSnmpIndex *, tRetVal *);
INT4 FsVcL2ContextIdSet(tSnmpIndex *, tRetVal *);
INT4 FsVcIfRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsVcIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVcL2ContextIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVcIfRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVcmIfMappingTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsVcmL2CxtAndVlanToIPIfaceMapTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsVcmL2VcNameGet(tSnmpIndex *, tRetVal *);
INT4 FsVcmIPIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsVcConfigExtTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsVcOwnerGet(tSnmpIndex *, tRetVal *);
INT4 FsVcOwnerSet(tSnmpIndex *, tRetVal *);
INT4 FsVcOwnerTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVcConfigExtTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsVcmFreeVcIdTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsVcmFreeVcIdGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsVcmCounterTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsVcCounterRxUnicastGet(tSnmpIndex *, tRetVal *);
INT4 FsVcCounterRxMulticastGet(tSnmpIndex *, tRetVal *);
INT4 FsVcCounterRxBroadcastGet(tSnmpIndex *, tRetVal *);
INT4 FsVcCounterStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsVcClearCounterGet(tSnmpIndex *, tRetVal *);
INT4 FsVcCounterStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsVcClearCounterSet(tSnmpIndex *, tRetVal *);
INT4 FsVcCounterStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVcClearCounterTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVcmCounterTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _FSVCMWR_H */
