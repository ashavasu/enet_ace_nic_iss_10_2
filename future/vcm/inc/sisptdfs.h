/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2009-2010                                          */
/*****************************************************************************/
/* $Id: sisptdfs.h,v 1.4 2012/03/30 13:45:35 siva Exp $                */
/*                                                                           */
/*****************************************************************************/
/*    FILE  NAME            : sisptdfs.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc   .                                */
/*    SUBSYSTEM NAME        : VCM module                                     */
/*    MODULE NAME           : VCM module                                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 01 Mar 2009                                    */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains the type definitions and    */
/*                            data structures in SISP functionality of VCM   */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    26 MAR 2009 / SISP Team.                                       */
/*---------------------------------------------------------------------------*/

#ifndef _SISPTDFS_H
#define _SISPTDFS_H

typedef struct _tVcmSispIfCtxInfo   
{
      tVcmIfMapEntry  ** apVlanIfMapList[SISP_NUMBER_OF_VLAN_BLOCK];
}tVcmSispIfCtxInfo;


typedef struct _tVcmSispGlobals
{
      tVcmRBTree          SispLogicalPortTable;
      tVcmMemPoolId       u4SispIfCtxInfoPoolId;
      tVcmMemPoolId       u4SispVlanIfMapPoolId;
      tVcmMemPoolId       u1VlanListPoolId;
      tVcmSispIfCtxInfo * apPVCTable[BRG_MAX_PHY_PLUS_LOG_PORTS + 1];
      UINT1               u1SispModuleStatus;
      UINT1               au1Reserved[3];
}tVcmSispGlobals;

/* Enum for sizing parameters corresponding to gFsSispSizingParams */
enum
{
    SISP_IF_CXT_INFO_SIZING_ID = 0,
    SISP_VLAN_IF_MAP_SIZING_ID,
    SISP_DEF_INTERFACE_SIZING_ID,
    SISP_VLAN_LIST_SIZING_ID,
    SISP_NO_SIZING_ID
};

#endif
