/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2009-2010                                          */
/*****************************************************************************/
/*    FILE  NAME            : sispprot.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc   .                                */
/*    SUBSYSTEM NAME        : VCM module                                     */
/*    MODULE NAME           : VCM module                                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 01 Mar 2009                                    */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains the function prototypes of  */
/*                            the functions of SISP functionality            */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    31 APR 2009 / SISPTeam   Initial Create.                       */
/*---------------------------------------------------------------------------*/
#ifndef _SISPPROT_H
#define _SISPPROT_H

/* Functions of sispmain.c */

INT4 VcmSispModuleStart PROTO ((VOID));

INT4 VcmSispModuleShutdown PROTO ((VOID));

VOID SispInit PROTO ((VOID));

INT4 VcmSispEnableOnPort PROTO ((UINT4 u4IfIndex));

INT4 VcmSispDisableOnPort PROTO ((UINT4 u4IfIndex));

VOID VcmSispDisableOnAllPorts PROTO ((VOID));

/* Functions of sisputil.c */
INT4 VcmSispPortMapTableCmp PROTO ((tRBElem * pRBElem1, tRBElem * pRBElem2));

INT4 VcmSispGetPhysicalPort PROTO ((UINT4 u4LogicalIfIdx, UINT4 *pu4IfIndex));

INT4 VcmSispIsEnabled PROTO ((UINT4 u4IfIndex));

INT1 VcmSispLogicalPortExistForPhysicalPort PROTO ((UINT4 u4PhyIfIndex));

VOID VcmSispNotifyProtocolShutdownStatus PROTO ((VOID));

#ifdef NPAPI_WANTED
INT4
VcmSispUpdateSispPortsInHw PROTO ((UINT4 u4PhyIfIndex, UINT1 u1Status));
#endif

/* Functions of sispif.c */
INT4 VcmSispCreatePortMapEntry PROTO ((UINT4 u4PhyIfIndex, UINT4 u4ContextId));

INT4 VcmSispDeletePortMapEntry PROTO ((UINT4 u4PhyIfIndex, UINT4 u4ContextId));

VOID  * VcmSispFindPortMapEntry PROTO ((VOID *pNode));

INT4 VcmSispSetPortMapIfIndex PROTO ((UINT4 u4PhyIfIndex, UINT4 u4ContextId, 
				      UINT4 u4IfIndex));

INT4 VcmSispSetPortMapStatus PROTO ((UINT4 u4PhyIfIndex, UINT4 u4ContextId,
				     UINT1 u1RowStatus));

INT4 VcmSispCreateSispPortInIfMapTable PROTO ((UINT4 u4PhyIfIndex, 
					       UINT4 u4ContextId));

INT4 VcmSispDeleteSispPortFromIfMapTable PROTO ((UINT4 u4PhyIfIndex, 
						 UINT4 u4ContextId));

INT4 VcmSispDeleteSispPort PROTO ((UINT4 u4PhyIfIndex, UINT4 u4ContextId));

INT4 VcmSispDeleteAllSispPorts PROTO ((VOID));

INT4 VcmSispPortMapEntryGetNext PROTO ((UINT4 u4PhyIfIndex, UINT4 u4ContextId,
					tVcmIfMapEntry  *pRetVcmIfmapEntry));
INT4 VcmSispPortMapEntryGet PROTO ((UINT4 u4PhyIfIndex, UINT4 u4ContextId,
				    tVcmIfMapEntry  *pRetVcmIfmapEntry));

/* Functions of sispport.c */

INT4 VcmSispL2IwfUpdateSispPortCtrlStatus PROTO ((UINT4 u4IfIndex, UINT1 u1Status));

INT4
VcmSispL2IwfGetPortVlanMemberList PROTO ((UINT4 u4IfIndex, UINT1 *pu1VlanList));

INT4
VcmSispCfaCopyPhysicalIfaceProperty (UINT4 u4LogicalIndex, UINT4 u4IfIndex);

INT4
VcmSispCfaSispResetInterfaceProp (UINT4 u4LogicalIndex);

INT4
VcmSispAstIsContextStpCompatible (UINT4 u4ContextId);

INT4
VcmSispL2IwfPbGetLocalVidListFromVIDTransTable (UINT4 u4IfIndex, 
						UINT1 *pu1LocalVidList);

INT4
VcmSispL2IwfGetPbPortType (UINT4 u4IfIndex, UINT1 *pu1PortType);

/* Functions of sisppvc.c */
INT4 VcmSispPortVlanMapEntryGetNext PROTO ((UINT4 u4PhyIfIndex, tVlanId VlanId,
					    tVlanId *pNextVlanId,
					    tVcmIfMapEntry * pNextIfMapEntry));

INT4 VcmSispPortVlanMapEntryGet PROTO ((UINT4 u4PhyIfIndex, tVlanId VlanId,
					tVcmIfMapEntry * pRetVcmIfMapEntry));

INT4 VcmSispAddPortVlanEntry PROTO ((tVcmIfMapEntry * pIfMapEntry,
				     tVlanId VlanId));

INT4 VcmSispDelPortVlanEntry PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex, 
				     tVlanId VlanId));

INT4 VcmSispDeleteAllPortVlanMapEntriesForPort PROTO ((UINT4 u4IfIndex,
                                                       UINT1 u1SyncFlag));

INT4 VcmSispValidatePortVlanEntry PROTO ((UINT4 u4ContextId, UINT2 u2LocalPort, 
					 tVlanId VlanId));

INT4 VcmSispSetPvcTableEntry PROTO ((UINT4 u4PhyIfIndex, tVlanId VlanId, 
				     tVcmIfMapEntry *pIfMapEntry));

INT4 VcmSispReSetPvcTableEntry PROTO ((UINT4 u4PhyIfIndex, tVlanId VlanId));

INT4 VcmSispGetPvcTableEntry PROTO ((UINT4 u4PhyIfIndex, tVlanId VlanId, 
				     tVcmIfMapEntry *pRetVcmIfMapEntry));

INT4 VcmSispAstIsRstStartedInContext (UINT4 u4ContextId);

INT4 VcmSispMakeSispIntfActive (tVcmIfMapEntry * pVcmIfMapEntry, UINT4 u4ContextId);
#endif
