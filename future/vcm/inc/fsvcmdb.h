/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsvcmdb.h,v 1.11 2017/11/14 07:31:19 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSVCMDB_H
#define _FSVCMDB_H

UINT1 FsVcmConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsVcmIfMappingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsVcmL2CxtAndVlanToIPIfaceMapTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsVcConfigExtTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsVcmFreeVcIdTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsVcmCounterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 fsvcm [] ={1,3,6,1,4,1,2076,93};
tSNMP_OID_TYPE fsvcmOID = {8, fsvcm};


UINT4 FsVcmTraceOption [ ] ={1,3,6,1,4,1,2076,93,1,1};
UINT4 FsVCId [ ] ={1,3,6,1,4,1,2076,93,1,2,1,1};
UINT4 FsVCNextFreeHlPortId [ ] ={1,3,6,1,4,1,2076,93,1,2,1,2};
UINT4 FsVCMacAddress [ ] ={1,3,6,1,4,1,2076,93,1,2,1,3};
UINT4 FsVcAlias [ ] ={1,3,6,1,4,1,2076,93,1,2,1,4};
UINT4 FsVcCxtType [ ] ={1,3,6,1,4,1,2076,93,1,2,1,5};
UINT4 FsVCStatus [ ] ={1,3,6,1,4,1,2076,93,1,2,1,6};
UINT4 FsVRMacAddress [ ] ={1,3,6,1,4,1,2076,93,1,2,1,7};
UINT4 FsVcmIfIndex [ ] ={1,3,6,1,4,1,2076,93,1,3,1,1};
UINT4 FsVcId [ ] ={1,3,6,1,4,1,2076,93,1,3,1,2};
UINT4 FsVcHlPortId [ ] ={1,3,6,1,4,1,2076,93,1,3,1,3};
UINT4 FsVcL2ContextId [ ] ={1,3,6,1,4,1,2076,93,1,3,1,4};
UINT4 FsVcIfRowStatus [ ] ={1,3,6,1,4,1,2076,93,1,3,1,5};
UINT4 FsVcmL2VcId [ ] ={1,3,6,1,4,1,2076,93,1,4,1,1};
UINT4 FsVcmVlanId [ ] ={1,3,6,1,4,1,2076,93,1,4,1,2};
UINT4 FsVcmL2VcName [ ] ={1,3,6,1,4,1,2076,93,1,4,1,3};
UINT4 FsVcmIPIfIndex [ ] ={1,3,6,1,4,1,2076,93,1,4,1,4};
UINT4 FsVcOwner [ ] ={1,3,6,1,4,1,2076,93,1,5,1,1};
UINT4 FsVcmFreeVcId [ ] ={1,3,6,1,4,1,2076,93,1,6,1,1};
UINT4 FsVCCounterId [ ] ={1,3,6,1,4,1,2076,93,1,7,1,1};
UINT4 FsVcCounterRxUnicast [ ] ={1,3,6,1,4,1,2076,93,1,7,1,2};
UINT4 FsVcCounterRxMulticast [ ] ={1,3,6,1,4,1,2076,93,1,7,1,3};
UINT4 FsVcCounterRxBroadcast [ ] ={1,3,6,1,4,1,2076,93,1,7,1,4};
UINT4 FsVcCounterStatus [ ] ={1,3,6,1,4,1,2076,93,1,7,1,5};
UINT4 FsVcClearCounter [ ] ={1,3,6,1,4,1,2076,93,1,7,1,6};




tMbDbEntry fsvcmMibEntry[]= {

{{10,FsVcmTraceOption}, NULL, FsVcmTraceOptionGet, FsVcmTraceOptionSet, FsVcmTraceOptionTest, FsVcmTraceOptionDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{12,FsVCId}, GetNextIndexFsVcmConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsVcmConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsVCNextFreeHlPortId}, GetNextIndexFsVcmConfigTable, FsVCNextFreeHlPortIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsVcmConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsVCMacAddress}, GetNextIndexFsVcmConfigTable, FsVCMacAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, FsVcmConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsVcAlias}, GetNextIndexFsVcmConfigTable, FsVcAliasGet, FsVcAliasSet, FsVcAliasTest, FsVcmConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsVcmConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsVcCxtType}, GetNextIndexFsVcmConfigTable, FsVcCxtTypeGet, FsVcCxtTypeSet, FsVcCxtTypeTest, FsVcmConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVcmConfigTableINDEX, 1, 0, 0, "1"},

{{12,FsVCStatus}, GetNextIndexFsVcmConfigTable, FsVCStatusGet, FsVCStatusSet, FsVCStatusTest, FsVcmConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVcmConfigTableINDEX, 1, 0, 1, NULL},

{{12,FsVRMacAddress}, GetNextIndexFsVcmConfigTable, FsVRMacAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, FsVcmConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsVcmIfIndex}, GetNextIndexFsVcmIfMappingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsVcmIfMappingTableINDEX, 1, 0, 0, NULL},

{{12,FsVcId}, GetNextIndexFsVcmIfMappingTable, FsVcIdGet, FsVcIdSet, FsVcIdTest, FsVcmIfMappingTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsVcmIfMappingTableINDEX, 1, 0, 0, NULL},

{{12,FsVcHlPortId}, GetNextIndexFsVcmIfMappingTable, FsVcHlPortIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsVcmIfMappingTableINDEX, 1, 0, 0, NULL},

{{12,FsVcL2ContextId}, GetNextIndexFsVcmIfMappingTable, FsVcL2ContextIdGet, FsVcL2ContextIdSet, FsVcL2ContextIdTest, FsVcmIfMappingTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsVcmIfMappingTableINDEX, 1, 0, 0, NULL},

{{12,FsVcIfRowStatus}, GetNextIndexFsVcmIfMappingTable, FsVcIfRowStatusGet, FsVcIfRowStatusSet, FsVcIfRowStatusTest, FsVcmIfMappingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVcmIfMappingTableINDEX, 1, 0, 1, NULL},

{{12,FsVcmL2VcId}, GetNextIndexFsVcmL2CxtAndVlanToIPIfaceMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsVcmL2CxtAndVlanToIPIfaceMapTableINDEX, 2, 0, 0, NULL},

{{12,FsVcmVlanId}, GetNextIndexFsVcmL2CxtAndVlanToIPIfaceMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsVcmL2CxtAndVlanToIPIfaceMapTableINDEX, 2, 0, 0, NULL},

{{12,FsVcmL2VcName}, GetNextIndexFsVcmL2CxtAndVlanToIPIfaceMapTable, FsVcmL2VcNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsVcmL2CxtAndVlanToIPIfaceMapTableINDEX, 2, 0, 0, NULL},

{{12,FsVcmIPIfIndex}, GetNextIndexFsVcmL2CxtAndVlanToIPIfaceMapTable, FsVcmIPIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsVcmL2CxtAndVlanToIPIfaceMapTableINDEX, 2, 0, 0, NULL},

{{12,FsVcOwner}, GetNextIndexFsVcConfigExtTable, FsVcOwnerGet, FsVcOwnerSet, FsVcOwnerTest, FsVcConfigExtTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsVcConfigExtTableINDEX, 1, 0, 0, NULL},

{{12,FsVcmFreeVcId}, GetNextIndexFsVcmFreeVcIdTable, FsVcmFreeVcIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsVcmFreeVcIdTableINDEX, 1, 0, 0, NULL},

{{12,FsVCCounterId}, GetNextIndexFsVcmCounterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsVcmCounterTableINDEX, 1, 0, 0, NULL},

{{12,FsVcCounterRxUnicast}, GetNextIndexFsVcmCounterTable, FsVcCounterRxUnicastGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVcmCounterTableINDEX, 1, 0, 0, NULL},

{{12,FsVcCounterRxMulticast}, GetNextIndexFsVcmCounterTable, FsVcCounterRxMulticastGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVcmCounterTableINDEX, 1, 0, 0, NULL},

{{12,FsVcCounterRxBroadcast}, GetNextIndexFsVcmCounterTable, FsVcCounterRxBroadcastGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsVcmCounterTableINDEX, 1, 0, 0, NULL},

{{12,FsVcCounterStatus}, GetNextIndexFsVcmCounterTable, FsVcCounterStatusGet, FsVcCounterStatusSet, FsVcCounterStatusTest, FsVcmCounterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVcmCounterTableINDEX, 1, 0, 0, "2"},

{{12,FsVcClearCounter}, GetNextIndexFsVcmCounterTable, FsVcClearCounterGet, FsVcClearCounterSet, FsVcClearCounterTest, FsVcmCounterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVcmCounterTableINDEX, 1, 0, 0, "2"},
};
tMibData fsvcmEntry = { 25, fsvcmMibEntry };

#endif /* _FSVCMDB_H */

