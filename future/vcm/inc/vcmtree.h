/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2004-2005                         */
/*****************************************************************************/
/*    FILE  NAME            : vcmtree.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : VCM module                                     */
/*    MODULE NAME           : VCM module                                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 16 Jun 2006                                    */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains the utility function        */
/*                            prototypes for tree implementation of          */
/*                            VCM tables.                                    */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    16 JUN 2006 / MITeam   Initial Create.                         */
/*---------------------------------------------------------------------------*/

#ifndef _VCMTREE_H
#define _VCMTREE_H

INT4 
VcmIfEntryCompare PROTO ((tRBElem *e1, tRBElem *e2));

INT4 
VcmL2CxtVlanToIfCompare PROTO ((tRBElem *e1, tRBElem *e2));

INT4 
VcmContextEntryCompare PROTO ((tRBElem *e1, tRBElem *e2));

#endif /* _VCMTREE_H */
