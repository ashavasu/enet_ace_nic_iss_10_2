/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: vcmred.h,v 1.12 2014/06/27 11:17:52 siva Exp $
 *
 * Description: This file contains the definitions and data structures
 *              and prototypes to support Redundancy for VCM module.
 *
 ******************************************************************/
#ifndef VCM_RED_H
#define VCM_RED_H

/* VCM Module states */ 
typedef UINT4 tVcmRedStates;
#define VCM_RED_IDLE                             RM_INIT
#define VCM_RED_ACTIVE                           RM_ACTIVE
#define VCM_RED_STANDBY                          RM_STANDBY

#define VCM_MAX_NUM_PORTS              BRG_MAX_PHY_PLUS_LAG_INT_PORTS 
#define VCM_MIN_NUM_PORTS                1

typedef struct _tVcmSispPvcAuditInfo{
    UINT4        u4PhyIfIndex;
    tVlanId      LastSyncVlan;
    UINT1        u1Action;
    UINT1        u1StaticSyncRcvd;
}tVcmSispHwAuditInfo;

typedef struct _tVcmRedGlobalInfo{
   tOsixTaskId             VcmAuditTaskId;       /* VCM Hardware Audit TaskId */
   tVcmSispHwAuditInfo     SispHwAuditInfo;
   UINT4               u4BulkUpdNextPort;
   UINT2               au2LocalPortId[VCM_MAX_NUM_PORTS];
   UINT1               u1VcmNodeStatus;
   UINT1               u1NumPeerPresent;
   BOOL1               bBulkReqRcvd;
   UINT1               au1pad[1 + ((VCM_MAX_NUM_PORTS % 2) * 2) ];


} tVcmRedGlobalInfo;

typedef enum {
   VCM_RED_IFMAP,
   VCM_RED_CLEAR_SYNCUP_DATA,
   VCM_RED_BULK_REQ,
   VCM_RED_BULK_UPD_TAIL_MSG,
   VCM_SISP_SYNC_MSG
}tVcmRmMsgTypes;

typedef enum {
   VCM_SISP_PORT_CTRL_MSG = 1,
   VCM_SISP_PVC_STATUS_MSG,
   VCM_SISP_SYNC_COMPLETE
}tVcmSispSyncTypes;


typedef enum {
   SISP_RED_ENABLE_START = 1,
   SISP_RED_DISABLE_START,
   SISP_RED_MOD_SHUT_START
}tVcmSispRedActions;

extern tVcmRedGlobalInfo  gVcmRedGlobalInfo;
extern tVcmRedStates      gVcmPrevNodeState;
/* RM Interface Funtions */

INT4 VcmRedRmInit(VOID);
INT4 VcmRedRegisterWithRM (VOID);
VOID VcmProcessRmEvent (tVcmQMsg *);
VOID VcmRedHandleSyncUpMessage (tVcmQMsg *);
VOID VcmRedSendBulkRequest (VOID);
VOID VcmRedHandleBulkRequest (VOID);
VOID VcmRedHandleDynSyncAudit (VOID);
VOID VcmExecuteCmdAndCalculateChkSum  (VOID);
INT4 VcmRedSendSyncMessages (UINT4, UINT2, UINT1);
VOID VcmRedClearSyncUpData (UINT4);
VOID VcmRedClearAllSyncUpData (VOID);
INT4 VcmRedStoreIfMap (VOID *, UINT4 *, UINT2);
INT4 VcmRedSendUpdateToRM (tRmMsg *, UINT2);
VOID VcmRedHandleBulkUpdateEvent (VOID);
INT4 VcmRedSendBulkUpdateTailMsg (VOID);
tVcmRedStates VcmRedGetNodeStatus (VOID);
VOID VcmRedInitHardwareAudit(VOID);
VOID VcmRedAuditTaskMain (INT1 *pi1Param);

/* Function of sispred.c */
VOID VcmSispStartHwAudit(VOID);
INT4 VcmSispRedSendSispSyncMsg (UINT1 u1Type, 
                                tVcmSispHwAuditInfo SispHwAuditInfo);
INT4 VcmSispRedRecvSispSyncMsg (VOID *pData, UINT4 *pu4Offset, UINT2 u2Len);
VOID VcmSispRedStaticSyncRcvd (VOID);
INT4 VcmSispRedSendModuleStatus (UINT1 u1Status);
INT4 VcmSispRedSendPortCtrlStatus(UINT4 u4PhyIfIndex, UINT1 u1Status);
INT4 VcmSispRedSendPvcStatus(UINT4 u4PhyIfIndex, tVlanId LastSyncVlan);
INT4 VcmSispRedSendCompleteStatus(VOID);
    

#define VCM_RED_CLEAR_SYNCUP_DATA_MSG_LEN   7
#define VCM_RED_BULK_REQ_MSG_LEN            3
#define VCM_RED_BULK_TAIL_MSG_LEN           3
#define VCM_RED_IFMAP_MSG_LEN               9
#define VCM_SISP_RED_SYNC_MSG_LEN           11

#define VCM_MAX_RM_BUF_SIZE                 1450
#define VCM_RED_NO_OF_PORTS_PER_SUB_UPDATE  10
#define VCM_RED_TRUE                        1
#define VCM_RED_FALSE                       0

#define SISP_RED_MAX_VLAN_PER_PVC_UPDATE    500

#define VCM_RED_GET_LOCAL_PORT_ID(Port, LocPort)\
{\
    LocPort= gVcmRedGlobalInfo.au2LocalPortId[Port-1];\
}

#define VCM_RED_SET_LOCAL_PORT_ID(Port, LocPort)\
{\
    gVcmRedGlobalInfo.au2LocalPortId[Port - 1] = LocPort;\
}

#define VCM_RED_LOCAL_PORT_ID(x)            gVcmRedGlobalInfo.au2LocalPortId[x-1]
#define VCM_NODE_STATUS()                   gVcmRedGlobalInfo.u1VcmNodeStatus
#define VCM_NUM_STANDBY_NODES()             gVcmRedGlobalInfo.u1NumPeerPresent
#define VCM_BULK_REQ_RECD()                 gVcmRedGlobalInfo.bBulkReqRcvd
#define VCM_RED_AUDIT_TASK_ID()        gVcmRedGlobalInfo.VcmAuditTaskId

#define VCM_SISP_HW_AUDIT_INFO() \
                    gVcmRedGlobalInfo.SispHwAuditInfo
#define VCM_SISP_RED_SYNC_PHYIFINDEX() \
                    gVcmRedGlobalInfo.SispHwAuditInfo.u4PhyIfIndex
#define VCM_SISP_RED_SYNC_LAST_SYNCED_VLAN() \
                    gVcmRedGlobalInfo.SispHwAuditInfo.LastSyncVlan
#define VCM_SISP_RED_SYNC_AUDIT_ACTION() \
                    gVcmRedGlobalInfo.SispHwAuditInfo.u1Action
#define VCM_SISP_RED_STATIC_SYNC_RCVD() \
                    gVcmRedGlobalInfo.SispHwAuditInfo.u1StaticSyncRcvd

#define VCM_MIN_NUM_PORTS                1
#define VCM_IS_STANDBY_UP()  \
             ((gVcmRedGlobalInfo.u1NumPeerPresent > 0) ? VCM_TRUE : VCM_FALSE)
#define VCM_RED_RM_GET_STATIC_CONFIG_STATUS()     VcmRmGetStaticConfigStatus()
    
    
#ifdef RM_WANTED
#define  VCM_IS_NP_PROGRAMMING_ALLOWED() \
       ((L2RED_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE) ? VCM_TRUE: VCM_FALSE)
#else
#define  VCM_IS_NP_PROGRAMMING_ALLOWED() VCM_TRUE
#endif

#define VCM_RM_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
         RM_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
                     *(pu4Offset) += 4;\
}while (0)

#define VCM_RM_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
         RM_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
                     *(pu4Offset) += 2;\
}while (0)

#define VCM_RM_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
         RM_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
                     *(pu4Offset) += 1;\
}while (0)

#define VCM_RM_PUT_N_BYTE(pdest, psrc, pu4Offset, u4Size) \
do { \
         RM_COPY_TO_OFFSET(pdest, psrc, *(pu4Offset), u4Size); \
                     *(pu4Offset) +=u4Size; \
}while (0)

#define VCM_RM_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
         RM_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
                     *(pu4Offset) += 1;\
}while (0)

#define VCM_RM_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
         RM_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
                     *(pu4Offset) += 2;\
}while (0)

#define VCM_RM_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
        RM_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
                    *(pu4Offset) += 4;\
}while (0)

#define VCM_RM_GET_N_BYTE(psrc, pdest, pu4Offset, u4Size) \
do { \
        RM_GET_DATA_N_BYTE (psrc, pdest, *(pu4Offset), u4Size); \
                    *(pu4Offset) +=u4Size; \
}while (0)

#define VCM_ASSERT() printf("%s ,%d\n",__FILE__,__LINE__);\
do{\
}while(0);

#endif
