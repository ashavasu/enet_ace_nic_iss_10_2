/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssispdb.h,v 1.2 2009/09/24 14:30:13 prabuc Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSSISPDB_H
#define _FSSISPDB_H

UINT1 FsSispPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsSispPortMapTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsSispCxtClassificationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};

UINT4 fssisp [] ={1,3,6,1,4,1,29601,2,20};
tSNMP_OID_TYPE fssispOID = {9, fssisp};


UINT4 FsSispSystemControl [ ] ={1,3,6,1,4,1,29601,2,20,1,1};
UINT4 FsSispPortIndex [ ] ={1,3,6,1,4,1,29601,2,20,2,1,1,1};
UINT4 FsSispPortCtrlStatus [ ] ={1,3,6,1,4,1,29601,2,20,2,1,1,2};
UINT4 FsSispPortMapContextId [ ] ={1,3,6,1,4,1,29601,2,20,2,2,1,1};
UINT4 FsSispPortMapSharedPort [ ] ={1,3,6,1,4,1,29601,2,20,2,2,1,2};
UINT4 FsSispPortMapHlPortId [ ] ={1,3,6,1,4,1,29601,2,20,2,2,1,3};
UINT4 FsSispPortMapRowStatus [ ] ={1,3,6,1,4,1,29601,2,20,2,2,1,4};
UINT4 FsSispCxtClassificationVlanId [ ] ={1,3,6,1,4,1,29601,2,20,3,1,1,1};
UINT4 FsSispCxtClassificationCxtId [ ] ={1,3,6,1,4,1,29601,2,20,3,1,1,2};


tMbDbEntry fssispMibEntry[]= {

{{11,FsSispSystemControl}, NULL, FsSispSystemControlGet, FsSispSystemControlSet, FsSispSystemControlTest, FsSispSystemControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{13,FsSispPortIndex}, GetNextIndexFsSispPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsSispPortTableINDEX, 1, 0, 0, NULL},

{{13,FsSispPortCtrlStatus}, GetNextIndexFsSispPortTable, FsSispPortCtrlStatusGet, FsSispPortCtrlStatusSet, FsSispPortCtrlStatusTest, FsSispPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSispPortTableINDEX, 1, 0, 0, "2"},

{{13,FsSispPortMapContextId}, GetNextIndexFsSispPortMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsSispPortMapTableINDEX, 2, 0, 0, NULL},

{{13,FsSispPortMapSharedPort}, GetNextIndexFsSispPortMapTable, FsSispPortMapSharedPortGet, FsSispPortMapSharedPortSet, FsSispPortMapSharedPortTest, FsSispPortMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSispPortMapTableINDEX, 2, 0, 0, NULL},

{{13,FsSispPortMapHlPortId}, GetNextIndexFsSispPortMapTable, FsSispPortMapHlPortIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsSispPortMapTableINDEX, 2, 0, 0, NULL},

{{13,FsSispPortMapRowStatus}, GetNextIndexFsSispPortMapTable, FsSispPortMapRowStatusGet, FsSispPortMapRowStatusSet, FsSispPortMapRowStatusTest, FsSispPortMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSispPortMapTableINDEX, 2, 0, 1, NULL},

{{13,FsSispCxtClassificationVlanId}, GetNextIndexFsSispCxtClassificationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsSispCxtClassificationTableINDEX, 2, 0, 0, NULL},

{{13,FsSispCxtClassificationCxtId}, GetNextIndexFsSispCxtClassificationTable, FsSispCxtClassificationCxtIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsSispCxtClassificationTableINDEX, 2, 0, 0, NULL},
};
tMibData fssispEntry = { 9, fssispMibEntry };
#endif /* _FSSISPDB_H */

