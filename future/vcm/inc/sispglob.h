/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2009-2010                                          */
/*****************************************************************************/
/*      $Id: sispglob.h,v 1.4 2012/05/30 06:04:55 siva Exp $                 */
/*****************************************************************************/
/*    FILE  NAME            : sispglob.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc   .                                */
/*    SUBSYSTEM NAME        : VCM module                                     */
/*    MODULE NAME           : VCM module                                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 01 Mar 2009                                    */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains the global variables and    */
/*                            declaration used in SISP functionality of VCM  */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    31 APR 2009 / SISPTeam   Initial Create.                       */
/*---------------------------------------------------------------------------*/
#ifndef _SISPGLOB_H
#define _SISPGLOB_H

#ifdef _SISPMAIN_C
tVcmSispGlobals gVcmSispGlobalInfo;

/* Sizing Params Structure  for SISP
 * Separate global array is used for SISP since the memory pools will
 * be created only when the module is started.
 * By default, the module is shutdown
 */
tFsModSizingParams gFsSispSizingParams [] =
{
    {"SISP_IF_CXT_INFO", "SISP_IF_CXT_INFO_MEMPOOL",
     sizeof (tVcmSispIfCtxInfo), SISP_MAX_SISP_ENABLED_PORTS,
     SISP_MAX_SISP_ENABLED_PORTS, 0},

    {"SISP_VLAN_IF_MAP", "SISP_VLAN_IF_MAP_MEMPOOL",
     (sizeof (tVcmIfMapEntry *) * SISP_MAX_VLAN_PER_BLOCK),
     (SISP_NUMBER_OF_VLAN_BLOCK * SISP_MAX_SISP_ENABLED_PORTS),
     (SISP_NUMBER_OF_VLAN_BLOCK * SISP_MAX_SISP_ENABLED_PORTS), 0},

    /* Since non embedded RBTree is used, memory is added for
     * RB Tree allocation
     */
    {"SISP_DEF_INTERFACE", "SISP_DEF_INTERFACE_RBTREE",
     sizeof (tRBNode), SYS_DEF_MAX_SISP_IFACES,
     SYS_DEF_MAX_SISP_IFACES, 0},

     {"VLAN_LIST_SISP", "VLAN_LIST_ID",
     VLAN_LIST_SIZE, MAX_SISP_BLOCKS,
     MAX_SISP_BLOCKS, 0},


    {"0", "0", 0,0,0,0}
};

tFsModSizingInfo gFsSispSizingInfo;

#else
extern tVcmSispGlobals gVcmSispGlobalInfo;
extern tFsModSizingParams gFsSispSizingParams [];
extern tFsModSizingInfo gFsSispSizingInfo;
#endif

#endif
