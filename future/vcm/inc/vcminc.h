/* $Id: vcminc.h,v 1.14 2016/05/04 11:38:15 siva Exp $*/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2004-2005                         */
/*****************************************************************************/
/*    FILE  NAME            : vcminc.h                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : VCM module                                     */
/*    MODULE NAME           : VCM module                                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 16 Jun 2006                                    */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file includes all common header files     */
/*                            used in the VCM module.                        */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    16 JUN 2006 / MITeam   Initial Create.                         */
/*---------------------------------------------------------------------------*/

#ifndef _VCMHDRS_H
#define _VCMHDRS_H

#include "lr.h"
#include "ip.h"
#include "cfa.h"
#include "bridge.h"

#include "snmccons.h"
#include "snmcdefn.h"
#include "snmctdfs.h"
#include "fsutil.h"
#include "fsvlan.h"
#include "rstp.h"
#ifdef PBB_WANTED
#include "pbb.h"
#endif
#include "vcm.h"
#include "la.h"
#include "l2iwf.h"
#include "iss.h"
#include "npapi.h"

#include "vcmmacr.h"
#include "vcmtdfs.h"
#include "vcmproto.h"
#include "vcmcons.h"
#include "vcmglob.h"

#include "sisptdfs.h"
#include "sispglob.h"
#include "sispprot.h"
#include "fssisplw.h"
#include "sispmacr.h"
#include "fssispwr.h"
#include "vcmsz.h"

#ifdef L2RED_WANTED
#include "rmgr.h"
#include "issu.h"
#include "vcmred.h"
#else
#include "vcmrdstb.h"
#endif

#ifdef ICCH_WANTED
#include "icch.h"
#endif

#include "fsvcmlw.h"
#include "fsvcmwr.h"
#include "vcmtrc.h"
#ifdef NPAPI_WANTED
#include "vcmnp.h"
#include "vcmnpwr.h"
#endif

#ifdef MBSM_WANTED
#include "mbsm.h"
#endif

#ifdef IP6_WANTED
#include "ipv6.h"
#endif

#ifdef LNXIP4_WANTED
#include "lnxip.h"
#include "fssocket.h"
#endif
#endif /* _VCMHDRS_H */
