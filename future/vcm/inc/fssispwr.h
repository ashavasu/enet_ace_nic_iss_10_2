#ifndef _FSSISPWR_H
#define _FSSISPWR_H

VOID RegisterFSSISP(VOID);

VOID UnRegisterFSSISP(VOID);
INT4 FsSispSystemControlGet(tSnmpIndex *, tRetVal *);
INT4 FsSispSystemControlSet(tSnmpIndex *, tRetVal *);
INT4 FsSispSystemControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSispSystemControlDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsSispPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsSispPortCtrlStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsSispPortCtrlStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsSispPortCtrlStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSispPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsSispPortMapTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsSispPortMapSharedPortGet(tSnmpIndex *, tRetVal *);
INT4 FsSispPortMapHlPortIdGet(tSnmpIndex *, tRetVal *);
INT4 FsSispPortMapRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsSispPortMapSharedPortSet(tSnmpIndex *, tRetVal *);
INT4 FsSispPortMapRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsSispPortMapSharedPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSispPortMapRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSispPortMapTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsSispCxtClassificationTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsSispCxtClassificationCxtIdGet(tSnmpIndex *, tRetVal *);
#endif /* _FSSISPWR_H */
