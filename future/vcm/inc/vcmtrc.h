/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                 */
/* Licensee Aricent Inc., 2004-2005                         */
/*****************************************************************************/
/*    FILE  NAME            : vcmtrc.h                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : VCM module                                     */
/*    MODULE NAME           : VCM module                                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 16 Jun 2006                                    */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains declarations of traces used */
/*                            in VCM Module.                                 */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                      DESCRIPTION OF CHANGE/              */
/*            MODIFIED BY                FAULT REPORT NO                     */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    16 JUN 2006 / MITeam       Initial Create.                     */
/*---------------------------------------------------------------------------*/
#ifndef _VCMTRC_H_
#define _VCMTRC_H_

/* Trace and debug flags */
#define   VCM_TRC_FLAG           gVcmGlobals.u4TraceOption

/* Module names */
#define   VCM_MOD_NAME           ((const char *)"VCM")

#define   VCM_INIT_SHUT_TRC      INIT_SHUT_TRC     
#define   VCM_MGMT_TRC           MGMT_TRC          
#define   VCM_DATA_PATH_TRC      DATA_PATH_TRC     
#define   VCM_CONTROL_PATH_TRC   CONTROL_PLANE_TRC 
#define   VCM_DUMP_TRC           DUMP_TRC          
#define   VCM_OS_RESOURCE_TRC    OS_RESOURCE_TRC   
#define   VCM_ALL_FAILURE_TRC    ALL_FAILURE_TRC   
#define   VCM_BUFFER_TRC         BUFFER_TRC        

/* Trace definitions */
#ifdef  TRACE_WANTED

#define VCM_PKT_DUMP(TraceType, pBuf, Length, Str)                           \
        MOD_PKT_DUMP(VCM_TRC_FLAG, TraceType, VCM_MOD_NAME, pBuf, Length, (const char *)Str)

#define VCM_TRC(TraceType, Str)                                              \
        MOD_TRC(VCM_TRC_FLAG, TraceType, VCM_MOD_NAME, (const char *)Str)

#define VCM_TRC_ARG1(TraceType, Str, Arg1)                                   \
        MOD_TRC_ARG1(VCM_TRC_FLAG, TraceType, VCM_MOD_NAME, (const char *)Str, Arg1)

#define VCM_TRC_ARG2(TraceType, Str, Arg1, Arg2)                             \
        MOD_TRC_ARG2(VCM_TRC_FLAG, TraceType, VCM_MOD_NAME, (const char *)Str, Arg1, Arg2)

#define VCM_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)                       \
        MOD_TRC_ARG3(VCM_TRC_FLAG, TraceType, VCM_MOD_NAME, (const char *)Str, Arg1, Arg2, Arg3)

#else  /* TRACE_WANTED */

#define VCM_PKT_DUMP(TraceType, pBuf, Length, Str)
#define VCM_TRC(TraceType, Str)
#define VCM_TRC_ARG1(TraceType, Str, Arg1)
#define VCM_TRC_ARG2(TraceType, Str, Arg1, Arg2)
#define VCM_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)

#endif /* TRACE_WANTED */

#endif/* _VCMTRC_H_ */

