/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vcmtdfs.h,v 1.18 2017/11/14 07:31:19 siva Exp $
 *
 * Description: Definitions used for IP-CFA Interface
 *
 *******************************************************************/
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2004-2005                         */
/*****************************************************************************/
/*    FILE  NAME            : vcmtdfs.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : VCM module                                     */
/*    MODULE NAME           : VCM module                                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 16 Jun 2006                                    */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains the type definitions and    */
/*                            data structures in VCM module.                 */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    16 JUN 2006 / MITeam   Initial Create.                         */
/*---------------------------------------------------------------------------*/

#ifndef _VCMTDFS_H
#define _VCMTDFS_H

typedef struct sVcmIfMapEntry {
    tRBNodeEmbd   IfMapRbNode;         
    tRBNodeEmbd   L2CxtToIpIfMapRBNode;
    tRBNodeEmbd   IfPhyRbNode;         
    UINT4    u4IfIndex;           /* Physical Interface index or SISP logical
         interface index. If this is SISP logical
         interface index, then u4PhyIfIndex will
         provide the IfIndex, over which this will
         run.*/
    UINT4    u4PhyIfIndex;        /* Physical Iface Index for logical ports */
    UINT4    u4VcNum;             /* virtual context number */
    UINT4    u4CfgVcNum;          /* Configured virtual context number. This 
                                     value will take into effect when row status
                                     is made active. This configuration will
                                     be allowed only for l3 interface mapping
                                     */
    UINT4    u4L2ContextId;       /* The l2 context to which the IVR interface
                                     is mapped. */
    UINT2    u2HlPortId;          /* The port index of this interface 
                                     in the virtual context */
    UINT2         u2VlanId;        /* The l2 vlan identifier to which
                                     the IVR interface will be mapped. */
    UINT1    u1RowStatus;         /* Row status of the entry. Entry valid only  
                                     if it is 'active' */
    UINT1         au1Reserved[3];      /* Structure Alignment */
}tVcmIfMapEntry;

typedef struct sVirtualContextInfo {

    tRBNodeEmbd   VcNumRbNode;         
    UINT1        au1Alias[VCM_ALIAS_MAX_LEN]; /* Alias for Virtual Context */
    UINT4        u4VcNum;            /* virtual context number */
    tVcmSLL      VcPortList;         /* List of ports added to this 
                                        virtual context */
    tVcmDLL      VcIPIntfList;       /* List of layer 3 interfaces added to 
                         this virtual context */
    UINT4        u4SispPortCount;   /* Stores count of SISP enabled(Physical or
                                       port channel) interfaces and SISP
                                       interfaces existing in this context*/
    tMacAddr     VcBridgeAddr;
    tMacAddr     VRMacAddr;
    UINT2        u2NextFreeHlPortId; /* The next free HLPortIndex for this 
                                        virtual context */
    UINT2        u2PortCount;        /* Counter to store the No. of Ports
                                        mapped to a particular context */
    UINT1        au1Owner[VCM_OWNER_MAX_SIZE];/* Owner of this context */
    UINT1        u1RowStatus;
    UINT1        u1CurrCxtType;     /* This variable specifies whether the context
                                       is an l2 context or l3 context */
    UINT1        u1CfgCxtType;      /* This variable specifies the configured
                                       context type. Configured context type comes
                                       into effect once the row status is made
                                       active. */
    UINT1        u1VrfCounter;       /* Contain the VRF Counter Status */ 
    UINT1        u1Pad[3];
}tVirtualContextInfo;

typedef struct sVcPortEntry {
    tVcmSLLNode        Next;        /* Pointer to the next IfMap Entry 
                                       node in the List */
    tVcmIfMapEntry     *pIf;        /* Pointer to the IfMap Entry node */
}tVcPortEntry;

#ifdef L2RED_WANTED
/* Peer node Id. */
typedef VOID * tVcmRedPeerId;

/* To handle message/events given by redundancy manager. */
typedef struct {
    tRmMsg        *pFrame;     /* Message given by RM module. */
    tVcmRedPeerId  PeerId;     /* This field will be used in case of 1:n
                                * redundancy support. */
    UINT2          u2Length;   /* Length of message given by RM module. */
    UINT1          u1Event;    /* Event given by RM module. */
    UINT1          u1Reserved;
}tVcmRedRmMsg;
#endif

typedef struct VcmQMsg {
#ifdef L2RED_WANTED
    tVcmRedRmMsg  RmMsg;
#endif
    UINT2         u2MsgType;
    UINT1         au1Reserved[2];
}tVcmQMsg;

#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
typedef struct VcmLnxvrfStatusQMsg {
    UINT4         u4VrfId;
    UINT4         u4Status;
    UINT4         u4IfIndex;
    UINT1         u1MsgType;
    UINT1         au1Reserved[3];
}tVcmLnxvrfStatusQMsg;
#endif

typedef struct sVcmGlobals {
    UINT4               u4TraceOption; /* For enabling Trace messages */
    tVcmMemPoolId       u4IfMapPoolId; /* Memory Pool Id of
                                        * the pool used for
                                         * allocating memory
                                         * block of size
                                         * tVcmIfMapEntry */
    tVcmMemPoolId       u4VcPoolId;    /* Memory Pool Id of
                                         * the pool used for
                                         * allocating memory
                                         * block of size
                                         * tVirtualContextInfo */
    tVcmMemPoolId       u4VcPortPoolId;/* Memory Pool Id of
                                         * the pool used for
                                         * allocating memory
                                         * block of size
                                         * tVcPortEntry */
    tVcmMemPoolId       u4CtrlQMemPooIId;/* Memory Pool Id of
                                         * the pool used for
                                         * allocating memory
                                         * blocks for Messages
                                         * posted to VCM Queue */
    tVcmMemPoolId       u4VcProtoRegPoolId; /* Memory pool Id of the pool used for
                                              allocating memory blocks for 
                                              protocol registration table entry
                                              */
    tVcmMemPoolId       u4IpIfPoolId;   /* Memory pool id of the pool used for
                                           allocating block of size 
                                           tVcmIpIfMapEntry
                                           */
    tVcmMemPoolId       u4LnxVrfStatusPooIId;   /* Memory pool id of the pool used for
                                           allocating block of size 
                                           tVcmLnxvrfStatusQMsg
                                           */
    tOsixQId            u4CtrlQId;
    tOsixQId            u4LnxVrfQId;

    tVcmOsixSemId       VcmSemId;
    tVcmOsixSemId       VcmConfSemId;
    tVcmOsixTaskId      VcmTaskId;
    tVcmRBTree          VcmIfMapTable;
    tVcmRBTree          VcmCxtAndVlanToIPIfMapTable;
    tVcmRBTree          VcmContextTable;
    tTMO_SLL            VcmProtoRegTable; /* This list holds the various protocols 
                                             registered for context change 
                                             indication */

    UINT2               u2NextFreeCxtId;
    UINT2               u2L3ContextCount;
}tVcmGlobals;

/* Structure for storing layer 3 protocol information */
typedef struct _VcmHLProtoRegEntry
{
    tTMO_SLL_NODE  nextProtoRegNode; /* Node points to the next protocol
                                        registration entry */

    VOID (*pIfMapChngAndCxtChng) 
        (UINT4 u4IpIfIndex, UINT4 u4VcmCxtId, UINT1 u1RegFlag);
                               /* Call back function */
    UINT4 u4CxtCount;          /* Reference Count for total number 
      of context */
    UINT1 u1RegFlag;
    UINT1 u1ProtoId;
    UINT1 u1AlignmentByte[2];  /* 3 bytes to ensure a 4-byte boundary */
} tVcmHLProtoRegEntry;

/* Structure for storing layer 3 interface to context mapping information */
typedef struct _VcmIpIfMapEntry
{
    tVcmDLLNode  NextIfEntry;
    tVcmIfMapEntry     *pIfMapEntry;
}tVcmIpIfMapEntry;

/* Enum for sizing parameters corresponding to gFsVcmSizingParams */
enum
{
    VCM_CTRLQ_MSG_SIZING_ID = 0,
    VCM_VCINFO_SIZING_ID,
    VCM_VCINFO_TREE_SIZING_ID,
    VCM_IFMAPENTRY_SIZING_ID,
    VCM_VCPORTENTRY_SIZING_ID,
    VCM_IFMAPENTRY_TREE_SIZING_ID,
    VCM_NO_SIZING_ID
};

#endif /* _VCMTDFS_H */
