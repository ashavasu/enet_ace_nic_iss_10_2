/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: vcmrdstb.h,v 1.8 2009/09/24 14:30:14 prabuc Exp $
 *
 * Description: This file contains the definitions and data structures
 *              and prototypes to support High Availability for VCM module.
 *
 ******************************************************************/
#ifndef VCM_RD_STB_H
#define VCM_RD_STB_H

typedef enum {
   VCM_RED_IFMAP,
   VCM_RED_CLEAR_SYNCUP_DATA,
   VCM_RED_BULK_REQ,
   VCM_RED_BULK_UPD_TAIL_MSG,
   VCM_SISP_SYNC_MSG
}tVcmRmMsgTypes;

INT4 VcmRedRmInit(VOID);
INT4 VcmRedRegisterWithRM(VOID);
INT4 VcmRedSendSyncMessages(UINT4, UINT2, UINT1);
VOID VcmRedClearSyncUpData (UINT4);

/* Function of sispred.c */
INT4 VcmSispRedSendModuleStatus (UINT1 u1Status);
INT4 VcmSispRedSendPortCtrlStatus(UINT4 u4PhyIfIndex, UINT1 u1Status);
INT4 VcmSispRedSendPvcStatus(UINT4 u4PhyIfIndex, tVlanId LastSyncVlan);
INT4 VcmSispRedSendCompleteStatus(VOID);

typedef UINT4 tVcmRedStates;
#define VCM_RED_IDLE                             RM_INIT
#define VCM_RED_ACTIVE                           RM_ACTIVE
#define VCM_RED_STANDBY                          RM_STANDBY
#define VCM_RED_RM_GET_STATIC_CONFIG_STATUS()    0

#define VCM_NODE_STATUS()              VCM_RED_ACTIVE
#define VCM_RED_LOCAL_PORT_ID(x)             0
#define VCM_RED_CLEAR_SYNCUP_DATA_MSG_LEN    7 

#define VCM_RED_GET_LOCAL_PORT_ID(Port, LocPort)\
{\
    LocPort = 0;\
}
#define VCM_RED_SET_LOCAL_PORT_ID(Port, LocPort)\
{\
    LocPort = 0;\
}

#define VCM_IS_NP_PROGRAMMING_ALLOWED()  VCM_TRUE 
#endif /* VCM_RD_STB_H */
