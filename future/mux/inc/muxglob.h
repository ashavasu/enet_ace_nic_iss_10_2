/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: muxglob.h,v 1.2 2010/08/10 13:27:23 prabuc Exp $
 *
 * Description: This file contains Global Variables related to the
 *              MUX Module
 *******************************************************************/
#ifndef _MUX_GLOB_H_
#define _MUX_GLOB_H_

#ifdef _MUXDEV_C_
tMuxDevInfo        *gpMuxDevInfo = NULL;
UINT4               gu4MuxTrace;
#else
extern tMuxDevInfo        *gpMuxDevInfo;
extern UINT4               gu4MuxTrace;
#endif /* _MUXDEV_C_ */

#ifdef VRRP_WANTED

typedef struct {
   tTMO_SLL VrrpList;    /* Link list for VRRP Master instances */
} tIfTable;

#ifdef _MUX_VRRP_C_
tMemPoolId  gVrrpTblMemPoolId;
tMemPoolId  gVrrpAssocIpMemPoolId;
tIfTable    gaIfTable[IP_DEV_MAX_IP_INTF + 1];
#else
extern tMemPoolId  gVrrpTblMemPoolId;
extern tMemPoolId  gVrrpAssocIpMemPoolId;
#endif /* _MUX_VRRP_C_ */
#endif /* VRRP_WANTED */

/* ----------------------- Global Declarations --------------------- */

#ifdef _MUX_REG_C_

tMemPoolId      gIssRegnTblPoolId;   /* Memory Pool Identifier for ISS Protocol 
                                        Registration Table */
tRBTree         gpIssRegnTree;       /* Tuple based RB Tree for ISS Protocol
                                        Registration Table */

tMemPoolId      gIPRegnTblPoolId;   /* Memory Pool Identifier for IP Protocol 
                                       Registration Table */
tRBTree         gpIPRegnTree;       /* Tuple based RB Tree for IP Protocol
                                       Registration Table */
#endif

#endif /* _MUX_GLOB_H_ */

/*-----------------------------------------------------------------------*/
/*                       End of the file muxglob.h                       */
/*-----------------------------------------------------------------------*/
