/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: muxmrvl.h,v 1.3 2010/09/25 08:29:41 prabuc Exp $
 *
 * Description: This file contains the Macro's, Constants and prototypes 
 *              required for porting MUX code to Marvell Platform
 *
 *              NOTE : Based on the Target Board Architecture the values
 *                     specified in this file needs to be updated
 *              
 *******************************************************************/
#ifndef _MUX_MRVL_H_
#define _MUX_MRVL_H_

/* ----------------- Constant & Macro Definitions ------------------ */

/* Constants pertaining to Marvell LinkStreet [6095] Board */

/* Marvell DSA Tag Size [Refer muxmrvl.c for its format] */

#define MRVL_HDR_SIZE              4

/* Underlying Switch Driver's Header size. This is a portable parameter and
 * changes depending on the platform */
#define MUX_SW_DRV_HDR_SIZE        MRVL_HDR_SIZE

#define MRVL_DEVICE_ID             (1 << 4)

/* Name of the CPU Port to which the Front Panel Ports are connected */

#define MRVL_CPU_PORT_NAME         "eth0"

#endif /* _MUX_MRVL_H_ */

/*-----------------------------------------------------------------------*/
/*                       End of the File muxmrvl.h                       */
/*-----------------------------------------------------------------------*/
