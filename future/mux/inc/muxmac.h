/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: muxmac.h,v 1.2 2010/08/10 13:27:23 prabuc Exp $
 *
 * Description: This file contains the Macros and Defines that are 
 *              required for the MUX Module          
 *******************************************************************/
#ifndef _MUX_MAC_H_
#define _MUX_MAC_H_

#define  IOCTL_SUCCESS             0
        
#define  MUX_KERN_MAX_MEMPOOLS     20

#define  MUX_CHAR_DEV_Q_SIZE       200

#ifdef VRRP_WANTED
#define MUX_VRRP_TBL_POOLID          gVrrpTblMemPoolId
#define MUX_VRRP_ASSOC_TBL_POOLID    gVrrpAssocIpMemPoolId
#define MUX_VRRP_MAX_TBL_SIZE        VRRP_MAX_OPER_ENTRY
#define MUX_VRRP_ASSOC_TBL_SIZE      VRRP_MAX_OPER_ENTRY * MAX_ASSO_ADDR_ENTRY
#define MUX_VRRP_HASH_TBL_SIZE       10
#endif

#define HIWORD(u4Dword)                (u4Dword >> 16)
#define LOWORD(u4Dword)                (u4Dword & 0xffff)

/* Trace Related Macros's */

#define MUX_TRC_FLAG  gu4MuxTrace
#define MUX_MOD_NAME  ((const char *)"MUX")

#define MUX_TRC(TraceType, Str)    \
        MOD_TRC(MUX_TRC_FLAG, TraceType, MUX_MOD_NAME, (const char *)Str)
    
#define MUX_TRC_ARG1(TraceType, Str, Arg1)               \
MOD_TRC_ARG1(MUX_TRC_FLAG, TraceType, MUX_MOD_NAME, (const char *)Str, Arg1)

#define MUX_TRC_ARG2(TraceType, Str, Arg1, Arg2)               \
MOD_TRC_ARG2(MUX_TRC_FLAG, TraceType, MUX_MOD_NAME, (const char *)Str, Arg1, Arg2)
    
/* Basic Trace Types are inherited from inc/trace.h. Additional Types have 
 * been defined here */
#define	MUX_DEBUG_TRC	     0x00001000	/* Function Entry/Exit Traces */
#define	MUX_INFO_TRC 	     0x00002000	/* Information Traces */

/* Macro to Scan the entries present in a RB Tree */
#define MUX_SCAN_TREE(Tree, pNode, pNext, Typecast) \
for ( (pNode = RBTreeGetFirst(Tree)); ( pNext=((pNode==NULL) ? NULL : \
    ((Typecast) RBTreeGetNext (Tree, pNode, NULL))) ), pNode != NULL; \
      pNode = pNext)

#endif/* _MUX_MAC_H_ */
