/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: muxport.h,v 1.2 2010/08/10 13:27:23 prabuc Exp $
 *
 * Description: This file contains Prototypes related to Portable API
 *              that are available in the MUX Module
 *******************************************************************/
#ifndef _MUX_PORT_H_
#define _MUX_PORT_H_

/********************* Portable Prototypes ****************************/

/* Packet Transmit/Reception Functions */

INT4 MuxPortPrcsRxFrame   PROTO ((tSkb * pSkb, UINT2 u2SrcPort,
                                  tVlanId VlanId));
INT4 MuxPortXmitDevPacket PROTO ((tSkb * pSkb, tNetDevice * pDev));
INT4 MuxPortXmitCPUPacket PROTO ((tSkb *pSkb, tVlanId VlanId, 
                                  UINT4 u4OutPort));

/* Platform Specific Initialization/Deinitialization Functions */

INT4 MuxCustInit    PROTO ((VOID));
VOID MuxCustDeInit  PROTO ((VOID));

#endif /* _MUX_PORT_H_ */
