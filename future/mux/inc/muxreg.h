/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: muxreg.h,v 1.2 2010/08/10 13:27:23 prabuc Exp $
 *
 * Description: This file contains Structures & Macro  related to
 *              MUX Protocol Registration Module
 *******************************************************************/
#ifndef _MUX_REG_H_
#define _MUX_REG_H_

/* ----------------- Constant & Macro Definitions ------------------ */

/* Max No. of Protocol Registration that is possible */

#define MUX_REGISTER                 1
#define MUX_DEREGISTER               2

/* Max Number of ISS Modules and IP Protocols that can register with MUX */

#define MUX_ISS_REG_TBL_MAX_SIZE     40

#define MUX_IP_REG_TBL_MAX_SIZE      10

/* Macro's for easier accessibility of elements in the Registration Table */

#define MUX_REG_MAC_INFO(x)          ((x)->MacInfo)
#define MUX_REG_MAC_ADDR(x)          ((x)->MacInfo.MacAddr)
#define MUX_REG_MAC_MASK(x)          ((x)->MacInfo.MacMask)
#define MUX_REG_ETH_PROTO(x)         ((x)->EthInfo.u2Protocol)
#define MUX_REG_ETH_MASK(x)          ((x)->EthInfo.u2Mask)
#define MUX_REG_IP_PROTO(x)          ((x)->IPInfo.u2Protocol)
#define MUX_REG_IP_MASK(x)           ((x)->IPInfo.u2Mask)
#define MUX_REG_STATUS(x)            ((x)->u1RegStatus)
#define MUX_REG_SENDTO_FLAG(x)       ((x)->u1SendToFlag)
#define MUX_REG_MODULE_ID(x)         ((x)->u4ModuleId)

#define MUX_REG_ETH_PROTO_CMP(x,y) \
           (MUX_REG_ETH_PROTO (x) == MUX_REG_ETH_PROTO (y))

#define MUX_REG_IP_PROTO_CMP(x,y) \
           (MUX_REG_IP_PROTO (x) == MUX_REG_IP_PROTO (y))

/* ------------------------ Type Definitions ----------------------- */

/* Structure of Protocol Registration Table Instances */

typedef struct _ProtRegTbl
{
   tRBNode       rbTupleNode;  /* RBTree Based on 3 tuple ProtRegTuple */
   tProtRegTuple ProtRegTuple; /* Tuples used to Register/De Register
                                  a Protocol */
   UINT4         u4ModuleId;   /* Unique Module Identifier */
   UINT1         u1RegStatus;  /* Registration Status    */
   UINT1         u1SendToFlag; /* Indicates to which module this 
                                  packet needs to be handed over. 
                                  This can be set to 
                                  MUX_REG_SEND_TO_ISS and/or 
                                  MUX_REG_SEND_TO_IP */         
   UINT1         u1Align[2];   /* For Alignment Purpose  */
} tProtRegTbl;

/* ----------------------- Function Prototypes --------------------- */

INT4 MuxRegInitRegnTable   PROTO ((VOID));
VOID MuxRegDeInitRegnTable PROTO ((VOID));
INT4 MuxRegistrationHdlr   PROTO ((FS_ULONG u4IoctlParam));
VOID MuxRegExtractRegnTuple PROTO ((UINT1 *pu1Data, tProtRegTuple * 
                                    pIncomingTuple));
tProtRegTbl * MuxRegGetRegnTableEntry PROTO ((tProtRegTuple * pProtRegnTuple,
                                              INT4 i4TreeId));
tProtRegTbl * MuxRegSearchMatchingEntry PROTO ((tProtRegTuple * pProtRegnTuple,
                                                INT4 i4TreeId));

#endif /* _MUX_REG_H_ */

/*-----------------------------------------------------------------------*/
/*                       End of the file muxreg.h                        */
/*-----------------------------------------------------------------------*/
