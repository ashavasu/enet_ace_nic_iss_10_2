/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: muxprot.h,v 1.2 2010/08/10 13:27:23 prabuc Exp $
 *
 * Description: This file contains Function Prototypes related to
 *              MUX Module
 *******************************************************************/
#ifndef _MUX_PROT_H_
#define _MUX_PROT_H_

/********************* muxrx.c prototypes ****************************/

INT4 MuxRxProcessIncomingFrame PROTO ((tSkb *pSkb , UINT2 u2IfIndex ,
					                             tVlanId VlanId));
INT4 MuxRxProcessIncomingL3Pkt PROTO ((tSkb *pSkb , UINT2 u2IfIndex ,
					                             tVlanId VlanId));

VOID MuxRxNetFilterInit PROTO ((VOID));
VOID MuxRxNetFilterDeInit PROTO ((VOID));

/********************* muxtx.c  Prototypes ****************************/

INT4 MuxTxCPUPacket PROTO ((tSkb *pSkb, tVlanId VlanId, UINT4 u4OutPort));

/********************* muxkern.c prototypes****************************/

INT4 MuxKernelLoadModule    PROTO ((VOID));
VOID MuxKernelUnLoadModule  PROTO ((VOID));

/********************* muxdev.c prototypes****************************/

INT4 MuxDeviceRegister   PROTO ((VOID));
VOID MuxDeviceDeRegister PROTO ((VOID));
INT4 MuxDevIoctlHandler  PROTO ((FS_ULONG u4IoctlParam));
INT4 MuxCustInit (VOID);
VOID MuxCustDeInit (VOID);

/********************* muxutil.c prototypes****************************/

VOID MuxUtilDumpBuffer PROTO ((VOID *pBuf, INT4 i4Len));
VOID MuxUtilDumpSKB    PROTO ((UINT1 *pu1Name, tSkb *pSkb));
VOID MuxUtilUnTagPkt   PROTO ((tSkb *pSkb));
INT4 MuxUtilPostMsgToUser PROTO ((tMuxDevInfo *pChrDevInfo, VOID *pMsg,
                                  INT4 i4MsgLen, VOID *pPkt,
                                  INT4 i4PktLen));
VOID MuxUtilPostPktToUser PROTO ((UINT1 *pu1Data, UINT4 u4PktSize,
                                  UINT4 u4IfIndex, UINT4 u4ModuleId));
VOID MuxUtilGetVlanIdFromDev PROTO ((tNetDevice *Dev, tVlanId *pVlanId));
VOID MuxUtilPrintMacAddress  PROTO ((UINT1 *pMacAddr, UINT1 *pu1MacString));
VOID MuxUtilTagVlanInfo      PROTO ((tSkb *pSkb, UINT2 u2EtherType,
      				                       tVlanId VlanId, UINT1 u1Priority));
INT4 MuxUtilIsMacAllZeros PROTO ((UINT1 *pMacAddr));

#ifdef VRRP_WANTED
/********************* muxvrrp.c prototypes****************************/

INT4 MuxVrrpPrcsIncomingPkt  PROTO ((tSkb *pSkb, tNetDevice *pL3Intf)); 
INT4 MuxVrrpPrcsOutgoingPkt  PROTO ((tSkb *pSkb, UINT2 u2IpPort));
INT4 MuxVrrpHandleL3IfDelete PROTO ((UINT2 u2IpPort));
INT4 MuxVrrpIoctlHandler  PROTO ((FS_ULONG u4IoctlParam));

#endif /* VRRP_WANTED */

#endif /* _MUX_PROT_H_ */

/*-----------------------------------------------------------------------*/
/*                       End of the file muxcmn.h                        */
/*-----------------------------------------------------------------------*/
