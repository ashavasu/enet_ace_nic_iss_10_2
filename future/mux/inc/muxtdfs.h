/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: muxtdfs.h,v 1.2 2010/08/10 13:27:23 prabuc Exp $
 *
 * Description: This file contains Structures related to
 *              MUX Charater Devices                
 *******************************************************************/
#ifndef _MUX_TDFS_H
#define _MUX_TDFS_H

/******************** Structure definition of Char Device **************/

typedef struct _tMuxDevInfo {
    
   tSema4              ChrDevSem;     /* Semaphore for accessing the
                                         Shared Queue in Atomic Mode */
   tWaitQ              ChrDevWQ;      /* Wait Queue */
   tSkbHead            ChrDevBufHead; /* Queue for User Land Communication */
   UINT4               u4Limit;       /* Max Messages that can be Queue up 
                                         when reader does not read */
} tMuxDevInfo;

#ifdef VRRP_WANTED

typedef struct
{
   tTMO_SLL_NODE       Next;
   tTMO_HASH_TABLE     *pIpTable;
   UINT1               au1Mac[MAC_ADDR_LEN];
   UINT2               u2Reserved;
} tVrrpEntryNode;

typedef struct
{
   tTMO_HASH_NODE      Next;
   UINT4               u4IpAddress;
} tVrrpAssocNode;

#endif /* VRRP_WANTED */

#endif /*_MUX_TDFS_H*/
