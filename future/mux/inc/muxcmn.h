/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: muxcmn.h,v 1.3 2010/08/10 13:27:23 prabuc Exp $
 *
 * Description: This file contains the Common Includes that are 
 *              required for the MUX Module          
 *******************************************************************/
#ifndef _MUX_CMN_H_
#define _MUX_CMN_H_

/* Kernel headers */
#include "kerninc.h"

#include "lr.h"
#include "osxinc.h"
#include "osxstd.h"
#include "cfa.h"
#include "trace.h"
#include "fsvlan.h"
#include "oskern.h"
#include "chrdev.h"
#include "muxtdfs.h"
#include "muxreg.h"
#include "muxglob.h"
#include "muxmac.h"
#include "muxprot.h"
#include "muxport.h"

#ifdef NPSIM_WANTED
#include "muxnpsim.h"
#endif /* NPSIM_WANTED */

#ifdef MRVLLS
#include "muxmrvl.h"
#endif /* MRVLLS */

#endif /* _MUX_CMN_H_ */

/*-----------------------------------------------------------------------*/
/*                       End of the file muxcmn.h                        */
/*-----------------------------------------------------------------------*/
