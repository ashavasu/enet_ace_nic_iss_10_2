/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved]
 *
 * $Id: muxnpsim.h,v 1.3 2010/09/25 08:29:41 prabuc Exp $
 *
 * Description: This file contains Function Prototypes related to
 *              MUX NPSIM Interaction Module
 *              
 *******************************************************************/
#ifndef _MUX_NPSIM_H_
#define _MUX_NPSIM_H_

/* NPSIM Header length */
#define NPSIM_MS_HDR_LEN      4

/* Underlying Switch Driver's Header size. This is a portable parameter and
 * changes depending on the platform */
#define MUX_SW_DRV_HDR_SIZE   NPSIM_MS_HDR_LEN

/********************* muxnpsim.c prototypes ****************************/

INT4 MuxNpSimDevRegister  PROTO ((VOID));
VOID MuxNpSimDevDeRegister  PROTO ((VOID));

#endif /* _MUX_NPSIM_H_ */
