/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved]
 *
 * $Id: muxkern.h,v 1.2 2010/08/10 13:27:23 prabuc Exp $
 *
 * Description : This file contains the Common Linux related Includes 
 *               required for compiling the Mux in Kernel Mode
 *******************************************************************/
#ifndef _MUX_KERN_H
#define _MUX_KERN_H

/* Kernel headers */
#include <linux/autoconf.h>
#include <linux/fs.h>  
#include <linux/module.h>  
#include <linux/init.h>
#include <linux/skbuff.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/if_arp.h>
#include <linux/inetdevice.h>
#include <linux/in.h>
#include <linux/version.h>

#endif /* _MUX_KERN_H */
