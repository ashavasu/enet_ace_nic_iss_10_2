/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: muxutil.c,v 1.3 2010/08/25 11:03:56 prabuc Exp $
 *
 * Description : This file contains all utility routines used by kernel
 *               modules 
 *******************************************************************/
#include "muxcmn.h"
#include "netdev.h"
#include "arp.h"
#include "iss.h"

/*****************************************************************************
 *    Function Name      : MuxUtilDumpBuffer  
 *
 *    Description        : Utility function to print the given buffer in HEXA
 *
 *    Input(s)           : Buffer and Length                        
 *
 *    Output(s)          : None
 *
 *    Returns            : None                            
 ****************************************************************************/
VOID
MuxUtilDumpBuffer (VOID *pBuf, INT4 len)
{
    INT4                i4Count;

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    for (i4Count = 0; i4Count < len; i4Count++)
    {
        PRINTF ("%02x", ((u8 *) pBuf)[i4Count]);

        if (i4Count % 2)
        {
            PRINTF (" ");
        }
        if (15 == i4Count % 16)
        {
            PRINTF ("\n");
        }
    }
    PRINTF ("\n");
    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);
}

/*****************************************************************************
 *    Function Name      : MuxUtilDumpSKB  
 *
 *    Description        : Utility function to print the given SKB buffer
 *                        in HEX Format
 *
 *    Input(s)           : Buffer and Name of the device.           
 *
 *    Output(s)          : None
 *
 *    Returns            : None                            
 ****************************************************************************/
VOID
MuxUtilDumpSKB (UINT1 *pu1Name, tSkb * pSkb)
{
    /* Check and Dump the SKB Contents only if DUMP_TRC Flag is enabled */
    if (gu4MuxTrace & DUMP_TRC)
    {
        PRINTF ("+++++++++++++++++++++++++++\n"
                "%s SKB dump skb=%p, data=%p, tail=%p, len=%d, end=%p",
                pu1Name, pSkb, pSkb->data, pSkb->tail, pSkb->len, pSkb->end);
        PRINTF (">> data:\n");

        MuxUtilDumpBuffer (pSkb->data, pSkb->len);

        PRINTF ("\n-------------------------\n");
    }
}

/******************************************************************************
 *    Function Name      : MuxUtilPrintMacAddress   
 *
 *    Description        : This Utility Function converts the given MAC 
 *                         Address in to a String of form xx:xx:xx:xx:xx:xx                         
 *
 *    Input(s)           : pMacAdrr     - Pointer to the Mac address                    
 *
 *    Output(s)          : pu1MacString - Pointer to the Converted MAC String
 *
 *    Returns            : None
 *****************************************************************************/
VOID
MuxUtilPrintMacAddress (UINT1 *pMacAddr, UINT1 *pu1MacString)
{
    UINT1               au1MacAddr[MAC_ADDR_LEN];
    UINT1               u1MacLen;

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    if (!(pMacAddr) || !(pu1MacString))
        return;

    MEMCPY (au1MacAddr, pMacAddr, MAC_ADDR_LEN);

    for (u1MacLen = 0; u1MacLen < MAC_ADDR_LEN; u1MacLen++)
    {
        if (au1MacAddr[u1MacLen] < 16)
        {
            SPRINTF ((CHR1 *) pu1MacString, "0%x", au1MacAddr[u1MacLen]);
        }
        else
        {
            SPRINTF ((CHR1 *) pu1MacString, "%x", au1MacAddr[u1MacLen]);
        }
        pu1MacString += STRLEN (pu1MacString);

        if (u1MacLen != (MAC_ADDR_LEN - 1))
        {
            STRCPY (pu1MacString, ":");
            pu1MacString += STRLEN (pu1MacString);
        }
    }
    SPRINTF ((CHR1 *) pu1MacString, "   ");
    pu1MacString += STRLEN (pu1MacString);

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);
}

/******************************************************************************
 *    Function Name      : MuxUtilUnTagPkt     
 *
 *    Description        : This is a utility function to untag an incoming
 *                         tagged packet                         
 *
 *    Input(s)           : pSkb - Pointer to Packet Buffer                      
 *
 *    Returns            : None
 *****************************************************************************/
VOID
MuxUtilUnTagPkt (tSkb * pSkb)
{
    UINT1              *pFrame = pSkb->data;
    tEnetV2Header      *pEthHdr = NULL;

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, "\n Entering %s \n", __FUNCTION__);

    pEthHdr = (tEnetV2Header *) pFrame;

    /* Check whether the incoming packet buffer is Tagged. If so, untag it and
     * return the new buffer size */

    if (OSIX_NTOHS (pEthHdr->u2LenOrType) == VLAN_PROTOCOL_ID)
    {
        memmove (pFrame + VLAN_TAG_OFFSET,
                 pFrame + VLAN_TAG_OFFSET + VLAN_TAG_PID_LEN,
                 pSkb->len - (VLAN_TAG_OFFSET + VLAN_TAG_PID_LEN));

        pSkb->len = pSkb->len - VLAN_TAG_PID_LEN;
        pSkb->tail = pSkb->tail - VLAN_TAG_PID_LEN;
    }

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxUtilTagVlanInfo
 *                                                                          
 *    DESCRIPTION      : Routine to Add VLAN Tag to the SKB packet buffer
 *                       NOTE : SKB SHOULD have sufficient Headroom to have
 *                       provide room for the VLAN Info
 *
 *    INPUT            : pSkb         - Pointer to the SKB
 *                       u2EtherType  - Protocol Identifier
 *                       VlanId       - VLAN Id
 *                       u1Priority   - Prority to be set
 *                                                                          
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MuxUtilTagVlanInfo (tSkb * pSkb, UINT2 u2EtherType, tVlanId VlanId,
                    UINT1 u1Priority)
{
    UINT1               au1Buf[VLAN_TAGGED_HEADER_SIZE];
    UINT2               u2Tag = 0;
    UINT2               u2Protocol = OSIX_HTONS (u2EtherType);

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, "\n Entering %s \n", __FUNCTION__);

    /* Copy Source and Dest MAC address and form Tagged Header 
     * (MAC + VLAN) to be prepended over the SKB */
    MEMCPY (au1Buf, pSkb->data, VLAN_TAG_OFFSET);

    u2Tag = (UINT2) u1Priority;

    u2Tag = (UINT2) (u2Tag << VLAN_TAG_PRIORITY_SHIFT);

    u2Tag = (UINT2) (u2Tag | (VlanId & VLAN_ID_MASK));

    u2Tag = (UINT2) (OSIX_HTONS (u2Tag));

    MEMCPY (&au1Buf[VLAN_TAG_OFFSET], (UINT1 *) &u2Protocol,
            VLAN_TYPE_OR_LEN_SIZE);

    MEMCPY (&au1Buf[VLAN_TAG_VLANID_OFFSET], (UINT1 *) &u2Tag, VLAN_TAG_SIZE);

    /* Prepend [MAC + VLAN] Info instead of [MAC] Info in the SKB */

    pSkb->data = pSkb->data - VLAN_TAG_PID_LEN;
    pSkb->len += VLAN_TAG_PID_LEN;

    MEMCPY (pSkb->data, au1Buf, VLAN_TAGGED_HEADER_SIZE);

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, "\n Exiting %s \n", __FUNCTION__);
}

/******************************************************************************
 *    Function Name      : MuxUtilPostMsgToUser     
 *
 *    Description        : This is a utility function to post the message from 
 *                         kernel to user plane.                         
 *
 *    Input(s)           : pChrDevInfo  - Char Device over which this Msg has 
 *                                        to be Enqueued
 *                         pMsg         - Pointer to message                      
 *                         i4MsgLen     - Length of the message          
 *                         pPkt         - Pointer to Packet Buffer               
 *                         i4PktLen     - Length of the Packet Buffer          
 *
 *    Output(s)          : None
 *
 *    Returns            : Error Code on Failure else no. of bytes posted
 *****************************************************************************/
INT4
MuxUtilPostMsgToUser (tMuxDevInfo * pChrDevInfo, VOID *pMsg, INT4 i4MsgLen,
                      VOID *pPkt, INT4 i4PktLen)
{
    tSkbHead           *pBuffHead = NULL;
    tSkb               *pSkb = NULL;
    tMuxDevInfo        *pDevInfo = pChrDevInfo;

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    pBuffHead = &(pDevInfo->ChrDevBufHead);

    if (pBuffHead->qlen >= pDevInfo->u4Limit)
    {
        MUX_TRC (ALL_FAILURE_TRC,
                 "MuxUtilPostMsgToUser : Invalid Buffer in Queue \n");
        return -EXFULL;

    }

    if ((pPkt != NULL) && (i4PktLen > 0))
    {
        pSkb = KERN_ALLOC_SKB (i4MsgLen + i4PktLen + 40, GFP_ATOMIC);

        if (NULL == pSkb)

        {
            MUX_TRC (OS_RESOURCE_TRC,
                     "MuxUtilPostMsgToUser : Memory Allocation Failed \n");
            return -EFAULT;

        }

        if (NULL != pMsg)
        {
            MEMCPY (skb_put (pSkb, i4MsgLen), pMsg, i4MsgLen);
        }

        MEMCPY (skb_put (pSkb, i4PktLen), pPkt, i4PktLen);
    }
    else
    {
        pSkb = KERN_ALLOC_SKB (i4MsgLen, GFP_ATOMIC);

        if (NULL == pSkb)

        {
            MUX_TRC (OS_RESOURCE_TRC,
                     "MuxUtilPostMsgToUser : Memory Allocation Failed \n");
            return -EFAULT;

        }

        MEMCPY (skb_put (pSkb, i4MsgLen), pMsg, i4MsgLen);
    }

    KERN_ENQUEUE_BUF (pBuffHead, pSkb);

    MUX_TRC (MUX_INFO_TRC, "MuxUtilPostMsgToUser : Released the lock\n");

    KERN_WAKE_UP_PRCS (&pDevInfo->ChrDevWQ);

    MUX_TRC_ARG1 (MUX_INFO_TRC,
                  "MuxUtilPostMsgToUser : Posted %d bytes of Data \n",
                  i4MsgLen + i4PktLen);

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return (i4MsgLen + i4PktLen);
}

/*****************************************************************************
 *    Function Name      : MuxUtilPostPktToUser
 *
 *    Description        : This function is called whenever any packet is 
 *                         received from the driver. This will take care of 
 *                         posting the packet to the input Character device 
 *                         queue which will be received by the user application
 *                         reading over this queue. The Packet is then enqueued
 *                         to its respective task queue based on the input 
 *                         Module Identifier
 *
 *    Input(s)           : u4IfIndex - Interface on which the packet is received
 *                         u4PktSize - Size of the packet
 *                         pu1Data   - Linear packet buffer     
 *                         u4ModuleId  - Module Identifier
 *
 *    Output(s)          : None
 *
 *    Returns            : None.                   
 ****************************************************************************/
VOID
MuxUtilPostPktToUser (UINT1 *pu1Data, UINT4 u4PktSize, UINT4 u4IfIndex,
                      UINT4 u4ModuleId)
{
    tData               Data;
    INT4                i4Size = 0;

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    MEMSET (&Data, 0, sizeof (tData));
    Data.Hdr.startpattern = 0xdead;
    Data.Hdr.type = PACKET_RX;
    Data.Hdr.len = sizeof (tHandlePacketRxCallBack) + u4PktSize;
    Data.Hdr.endpattern = 0xbeef;
    Data.u.RxCbk.u4IfIndex = u4IfIndex;
    Data.u.RxCbk.u4PktLen = u4PktSize;
    Data.u.RxCbk.u4ModuleId = u4ModuleId;

    i4Size = sizeof (tHeader) + sizeof (tHandlePacketRxCallBack);

    if (MuxUtilPostMsgToUser (gpMuxDevInfo, (void *) &Data, i4Size,
                              (void *) pu1Data, u4PktSize) < 0)
    {
        MUX_TRC (ALL_FAILURE_TRC,
                 "MuxUtilPostPktToUser : Failed to Post the Data"
                 "    to User Space \n");
    }

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxUtilGetVlanIdFromDev  
 *                                                                          
 *    DESCRIPTION      : This function is used to get the VlanId from the
 *                       Device name. For Router Ports, Vlan ID is returned
 *                       as 0
 *
 *    INPUT            : pDev         - Pointer to the device structure        
 *                       
 *    OUTPUT           : pVlanId      -  Pointer to VlanId
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MuxUtilGetVlanIdFromDev (tNetDevice * Dev, tVlanId * pVlanId)
{
    CHR1               *pc1Name = NULL;

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    pc1Name = Dev->name;

    if (STRNCMP (pc1Name, CFA_STACK_VLAN_INTERFACE,
                 STRLEN (CFA_STACK_VLAN_INTERFACE)) == 0)
    {
        pc1Name = pc1Name + 4;
        *pVlanId = (tVlanId) ATOI (pc1Name);
    }
    else if (STRNCMP (pc1Name, ISS_RPORT_ALIAS_PREFIX,
                      STRLEN (ISS_RPORT_ALIAS_PREFIX)) == 0)
    {
        *pVlanId = 0;
    }

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxUtilIsMacAllZeros  
 *                                                                          
 *    DESCRIPTION      : Checks whether a MAC Address contains All Zero's 
 *                       i.e 00:00:00:00:00:00
 *
 *    INPUT            : pMacAddr - Input MAC Address to check
 *                       
 *    OUTPUT           : None  
 *                                                                          
 *    RETURNS          : OSIX_TRUE if input MAC has all Zero's else
 *                       returns OSIX_FALSE
 *                                                                          
 ****************************************************************************/
INT4
MuxUtilIsMacAllZeros (UINT1 *pMacAddr)
{
    UINT4               u4Index = 0;

    for (; u4Index < MAC_ADDR_LEN; u4Index++)
    {
        if (pMacAddr[u4Index] != 0)
        {
            return OSIX_FALSE;
        }
    }
    return OSIX_TRUE;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file muxutil.c                       */
/*-----------------------------------------------------------------------*/
