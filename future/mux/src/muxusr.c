/****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: muxusr.c,v 1.4 2010/08/25 11:03:56 prabuc Exp $
 *
 * Description: This file contains API's that handles the ioctl requests from 
 *              ISS. 
 ***************************************************************************/
#ifndef _MUX_USR_C_
#define _MUX_USR_C_

#include "lr.h"
#include "iss.h"
#include "fsvlan.h"
#include "chrdev.h"
#include "netdev.h"

PRIVATE
    INT4 MuxUsrIoctlHandler PROTO ((INT4 i4Command, tKernCmdParam * pCmdParam));

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : KAPIUpdateInfo
 *                                                                          
 *    DESCRIPTION      : This API handles the Updations that needs to be done
 *                       to the Kernel Space MUX Component by calling 
 *                       MuxUsrIoctlHandler (). 
 *                                                                          
 *    INPUT            : i4Command - Command that is to be passed to Kernel 
 *                       pCmdParam - Parameters for the resp. commands
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
INT4
KAPIUpdateInfo (INT4 i4Command, tKernCmdParam * pCmdParam)
{
    /* Call the MUX user space API to configure kernel module via ioctl ()'s */
    return (MuxUsrIoctlHandler (i4Command, pCmdParam));
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxUsrIoctlHandler
 *                                                                          
 *    DESCRIPTION      : This API handles the Updations that needs to be done
 *                       to the Kernel Space MUX Component by calling 
 *                       ioctl ()'s with appropriate command options depending
 *                       on the Requests triggered from the User Space Modules
 *                                                                          
 *    INPUT            : i4Command - Command that is to be passed to Kernel 
 *                       pCmdParam - Parameters for the resp. commands
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
MuxUsrIoctlHandler (INT4 i4Command, tKernCmdParam * pCmdParam)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    switch (i4Command)
    {
        case MUX_PROT_REG_IOCTL:

            /* Register or Deregister a Module from the MUX Protocol Registration
             * Table */
            i4RetVal = ioctl (gi4DevFd, i4Command, pCmdParam);
            break;

#ifdef VRRP_WANTED
        case VRRP_ASSOCIP_IOCTL:
            i4RetVal = ioctl (gi4DevFd, i4Command, pCmdParam);
            break;
#endif /* VRRP_WANTED */

            /* Unsupported ioctl request's. Backward compatability with the
             *  ioctl () request done for BCM platform. Return Success by 
             *  default */
        case GDD_INTERFACE:
        case GDD_OOB_IOCTL:
        case IPAUTH_PORT_IOCTL:
#ifdef WGS_WANTED
        case MGMT_VLAN_IOCTL:
#endif
#ifdef MBSM_WANTED
        case STACK_INFO_IOCTL:
#endif
        case VLAN_FDB_IOCTL:
        case VLAN_FDB_REMOVE_IOCTL:
        case IPAUTH_NMH_IOCTL:
            i4RetVal = OSIX_SUCCESS;
            break;

        default:
            /* Operation Not Supported */
            i4RetVal = OSIX_FAILURE;
            break;
    }

    return i4RetVal;
}

#endif /* _MUX_USR_C_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  muxusr.c                       */
/*-----------------------------------------------------------------------*/
