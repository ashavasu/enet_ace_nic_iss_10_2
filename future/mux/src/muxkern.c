/****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: muxkern.c,v 1.2 2010/08/10 13:27:21 prabuc Exp $
 *
 * Description: This File contains API's for Loading/UnLoading Mux Module 
 *              in Kernel
 ***************************************************************************/
#include "muxcmn.h"

PRIVATE INT4 MuxKernInitDefaults PROTO ((VOID));
PRIVATE VOID MuxKernRevertDefaults PROTO ((VOID));

/* Memory Pool Id for the MUX Module */
tMemPoolCfg         gMuxKernMemPoolCfg;

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxKernelLoadModule
 *                                                                          
 *    DESCRIPTION      : Function to load MUX Module into Kernel
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : 0 for success -1 for failure
 *                                                                          
 ****************************************************************************/
INT4
MuxKernelLoadModule (VOID)
{
    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    /* Intialize the Kernel Defaults */
    if (MuxKernInitDefaults () == OSIX_FAILURE)
    {
        MUX_TRC (ALL_FAILURE_TRC, " Kernel DataBase Init : Failed");
        return -1;
    }

    /* Register the Interfaces with User Space and does the Default 
     * Initializaitions required for the MUX Module */

    if (MuxDeviceRegister () == OSIX_FAILURE)
    {
        MuxKernRevertDefaults ();
        MUX_TRC (ALL_FAILURE_TRC,
                 "\r\n Registering MUX<->ISS Character Device : Failed");
        return -1;
    }

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return 0;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxKernelUnLoadModule
 *                                                                          
 *    DESCRIPTION      : DeInit Function for the MUX Module. Used to Cleanup the
 *                       memory allocated during this module's init'n 
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MuxKernelUnLoadModule (VOID)
{
    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    /* De-Register the Interfaces with User Space and revert the Default 
     * Initializaitions done for the MUX Module */
    MuxDeviceDeRegister ();

    MuxKernRevertDefaults ();

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxKernInitDefaults
 *                                                                          
 *    DESCRIPTION      : Function to initialise Default & Custom features
 *                       that are mandatory for the Mux Module to operate
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
MuxKernInitDefaults (VOID)
{
    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    /* Initialize MemPool manager */
    gMuxKernMemPoolCfg.u4MaxMemPools = MUX_KERN_MAX_MEMPOOLS;
    gMuxKernMemPoolCfg.u4NumberOfMemTypes = 0;
    gMuxKernMemPoolCfg.MemTypes[0].u4MemoryType = MEM_DEFAULT_MEMORY_TYPE;
    gMuxKernMemPoolCfg.MemTypes[0].u4NumberOfChunks = 0;
    gMuxKernMemPoolCfg.MemTypes[0].MemChunkCfg[0].u4StartAddr = 0;
    gMuxKernMemPoolCfg.MemTypes[0].MemChunkCfg[0].u4Length = 0;

    if (MemInitMemPool (&gMuxKernMemPoolCfg) != MEM_SUCCESS)
    {
        MUX_TRC (ALL_FAILURE_TRC, "MuxKernInitDefaults : "
                 "MemInitMemPool Failure \n");
        return OSIX_FAILURE;
    }

    /* Do the Protocol Registration Table Initialization */
    if (MuxRegInitRegnTable () == OSIX_FAILURE)
    {
        MUX_TRC (ALL_FAILURE_TRC, "MuxKernInitDefaults : "
                 "Failed to Initialize Protocol Registration Table \n");
        return OSIX_FAILURE;
    }
#ifdef VRRP_WANTED
    /* Creating MEM POOL for VRRP Table entries */
    if (MemCreateMemPool (sizeof (tVrrpEntryNode),
                          MUX_VRRP_MAX_TBL_SIZE,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &gVrrpTblMemPoolId) != MEM_SUCCESS)
    {
        MuxRegDeInitRegnTable ();
        MUX_TRC (ALL_FAILURE_TRC, "MuxKernInitDefaults : "
                 "Failed to create Memory Pool for VRRP IF Table\n");
        return OSIX_FAILURE;
    }

    if (MemCreateMemPool (sizeof (tVrrpAssocNode),
                          MUX_VRRP_ASSOC_TBL_SIZE,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &gVrrpAssocIpMemPoolId) != MEM_SUCCESS)
    {
        MuxRegDeInitRegnTable ();
        MUX_TRC (ALL_FAILURE_TRC, "MuxKernInitDefaults : "
                 "Failed to create Memory Pool for VRRP Assoc. Table\n");
        return OSIX_FAILURE;
    }

#endif

    /* Do Custom Initialization specific to any platform */
    if (MuxCustInit () == OSIX_FAILURE)
    {
        MuxRegDeInitRegnTable ();
        MUX_TRC (ALL_FAILURE_TRC, "MuxKernInitDefaults : "
                 "Failed to do Custom Initializions \n");
        return OSIX_FAILURE;
    }

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxKernRevertDefaults
 *                                                                          
 *    DESCRIPTION      : Function to de-init Default & Custom features
 *                       that have been done while loading up the module 
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
MuxKernRevertDefaults (VOID)
{
    gu4MuxTrace = 0;

    /* Do the Protocol Registration Table De-Initialization */
    MuxRegDeInitRegnTable ();

#ifdef VRRP_WANTED
    if (gVrrpTblMemPoolId > 0)
    {
        MemDeleteMemPool (gVrrpTblMemPoolId);
    }

    if (gVrrpAssocIpMemPoolId > 0)
    {
        MemDeleteMemPool (gVrrpAssocIpMemPoolId);
    }
#endif
    /* Do Custom De-Initialization specific to any platform */
    MuxCustDeInit ();

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return;
}

/* Define the Init/Deinit Fn's to be called when this module gets loaded to 
 * the Kernel */
module_init (MuxKernelLoadModule);

module_exit (MuxKernelUnLoadModule);

MODULE_LICENSE ("Aricent");
/*-----------------------------------------------------------------------*/
/*                       End of the file  muxinit.c                      */
/*-----------------------------------------------------------------------*/
