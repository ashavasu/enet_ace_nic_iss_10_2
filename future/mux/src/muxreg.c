/****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: muxreg.c,v 1.2 2010/08/10 13:27:21 prabuc Exp $
 *
 * Description: This File contains the routines for...
 *
 *              1) MUX Protocol Registration Module Management
 *              2) Handling Registeration/Deregisteration of an ISS Module 
 *                 with MUX from User Space
 *              3) Utility routine to extract Qualifiers from the Incoming
 *                 Packet Buffer
 *              4) Doing Registration Table Lookup with the Incoming Packet's
 *                 Qualifier's and return the Matching entry 
 * 
 ***************************************************************************/
#ifndef _MUX_REG_C_
#define _MUX_REG_C_

#include "muxcmn.h"
#include "ip.h"
#include "ipv6.h"

/************************** PRIVATE DECLARATIONS ******************************/

PRIVATE INT4 MuxRegUpdateProtRegTable PROTO ((tProtRegTuple * pRegnTuple,
                                              UINT4 u4ModuleId,
                                              UINT1 u1SendToFlag,
                                              UINT1 u1RegStatus));

PRIVATE INT4 MuxRegTupleTreeCmpFun PROTO ((tRBElem * prbTuple1,
                                           tRBElem * prbTuple2));
PRIVATE tProtRegTbl *MuxRegFindMatchingTblEntry PROTO ((tRBTree pRbTree,
                                                        tProtRegTuple *
                                                        pSearchTuple));

/********************** END OF PRIVATE DECLARATIONS **************************/

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxRegInitRegnTable
 *                                                                          
 *    DESCRIPTION      : This function initializes the Protocol Registration
 *                       table by allocating the Memory pools required for it
 *                       database and assign their default values.
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : Returns OSIX_SUCCESS on success and OSIX_FAILURE on
 *                       memory pool allocation failure 
 *                                                                          
 ****************************************************************************/
INT4
MuxRegInitRegnTable (VOID)
{
    MUX_TRC_ARG1 (MUX_DEBUG_TRC, "\n Entering %s \n", __FUNCTION__);

    /* Create the memory pool for ISS Protocol Registration table */

    if (MemCreateMemPool (sizeof (tProtRegTbl), MUX_ISS_REG_TBL_MAX_SIZE,
                          MEM_HEAP_MEMORY_TYPE,
                          &gIssRegnTblPoolId) == MEM_FAILURE)
    {
        MUX_TRC (OS_RESOURCE_TRC,
                 "Memory pool creation for Protocol Registration table "
                 "failed\r\n");
        return OSIX_FAILURE;
    }

    /* Create RB Tree for maintaining the ISS Registration Table */

    gpIssRegnTree =
        RBTreeCreate (MUX_ISS_REG_TBL_MAX_SIZE, MuxRegTupleTreeCmpFun);

    if (NULL == gpIssRegnTree)
    {
        MemDeleteMemPool (gIssRegnTblPoolId);
        MUX_TRC (OS_RESOURCE_TRC,
                 "Memory creation of Protocol Registration RB Tree failed\r\n");
        return OSIX_FAILURE;
    }

    /* Create the memory pool for Linux IP Protocol Registration table
     *
     * NOTE : IP Registration Table is Unused as of now. This would be used in
     * future for filtering IP packets that should not be posted to IP 
     * [LinuxIP or Aricent IP Stack] */

    if (MemCreateMemPool (sizeof (tProtRegTbl), MUX_IP_REG_TBL_MAX_SIZE,
                          MEM_HEAP_MEMORY_TYPE,
                          &gIPRegnTblPoolId) == MEM_FAILURE)
    {
        MUX_TRC (OS_RESOURCE_TRC,
                 "Memory pool creation for Protocol Registration table "
                 "failed\r\n");
        return OSIX_FAILURE;
    }

    /* Create RB Tree for maintaining the Linux IP Registration Table */

    gpIPRegnTree =
        RBTreeCreate (MUX_IP_REG_TBL_MAX_SIZE, MuxRegTupleTreeCmpFun);

    if (NULL == gpIPRegnTree)
    {
        MemDeleteMemPool (gIPRegnTblPoolId);
        MUX_TRC (OS_RESOURCE_TRC,
                 "Memory creation of Protocol Registration RB Tree failed\r\n");
        return OSIX_FAILURE;
    }

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, "\n Exiting %s \n", __FUNCTION__);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxRegDeInitRegnTable
 *                                                                          
 *    DESCRIPTION      : This function free's up the memory pool and RBTtree
 *                       allocated for maintaining Protocol Registration table
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MuxRegDeInitRegnTable (VOID)
{
    MUX_TRC_ARG1 (MUX_DEBUG_TRC, "\n Entering %s \n", __FUNCTION__);

    /* Delete the ISS & Linux IP Registration Table RB Tree's if exists */

    if (gpIssRegnTree != NULL)
    {
        RBTreeDelete (gpIssRegnTree);
    }

    if (gpIPRegnTree != NULL)
    {
        RBTreeDelete (gpIPRegnTree);
    }

    /* Delete the Memory Pool's allocated for the ISS and Linux IP 
     * Registration table */

    if (gIssRegnTblPoolId > 0)
    {
        MemDeleteMemPool (gIssRegnTblPoolId);
    }

    if (gIPRegnTblPoolId > 0)
    {
        MemDeleteMemPool (gIPRegnTblPoolId);
    }

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, "\n Exiting %s \n", __FUNCTION__);
}

/*****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxRegistrationHdlr
 *                                                                          
 *    DESCRIPTION      : I/O Control Function to register/deregister a Protocol
 *                       Module that is interested to receive packets to ISS 
 *                       from MUX using the Tuple Information passed 
 *                       from the User Space
 *
 *    INPUT            : u4IoctlParam - Contains Tuple Information, Module ID
 *                                      and Registration Status to set/update
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : Returns IOCTL_SUCCESS on Success else appropriate Error
 *                       Code is returned for cases like copy_from_user () 
 *                       failure while copying user space input parameters 
 *                       and registration/deregistration failure.
 *                                                                          
 *****************************************************************************/
INT4
MuxRegistrationHdlr (FS_ULONG u4IoctlParam)
{
    tMuxProtRegParams   MuxProtRegParams;
    INT4                i4RetVal = IOCTL_SUCCESS;

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, "\n Entering %s \n", __FUNCTION__);

    if (KERN_COPY_FROM_USER (&MuxProtRegParams,
                             (tMuxProtRegParams *) u4IoctlParam,
                             sizeof (tMuxProtRegParams)))
    {
        MUX_TRC (ALL_FAILURE_TRC,
                 "MuxRegistrationHdlr : Failed to get Protocol "
                 "Reg'n Params \n");
        return -EFAULT;
    }

    switch (MuxProtRegParams.i4Command)
    {
        case MUX_PROT_REG_HDLR:

            i4RetVal = MuxRegUpdateProtRegTable (&MuxProtRegParams.ProtRegTuple,
                                                 MuxProtRegParams.u4ModuleId,
                                                 MuxProtRegParams.u1SendToFlag,
                                                 MUX_REGISTER);

            break;

        case MUX_PROT_DEREG_HDLR:

            i4RetVal = MuxRegUpdateProtRegTable (&MuxProtRegParams.ProtRegTuple,
                                                 MuxProtRegParams.u4ModuleId,
                                                 MuxProtRegParams.u1SendToFlag,
                                                 MUX_DEREGISTER);
            break;

        default:
            i4RetVal = -EOPNOTSUPP;
            MUX_TRC (ALL_FAILURE_TRC,
                     "MuxRegistrationHdlr : Invalid Ioctl Request \n");
            break;
    }

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, "\n Exiting %s \n", __FUNCTION__);

    return ((i4RetVal == OSIX_SUCCESS) ? IOCTL_SUCCESS : i4RetVal);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxRegGetRegnTableEntry
 *                                                                          
 *    DESCRIPTION      : This Function gets a matching 3 tuple entry from the
 *                       Protocol Registration table by calling RBTreeGet () 
 *                       with the input tuples from an incoming packet. 
 *                       This Funciton gets called while registering/
 *                       deregistering an module from ISS 
 *
 *    INPUT            : pProtRegnTuple - Tuple Information to be used for table
 *                                        lookup
 *                       i4TreeId       - Table (RB Tree) in which this tuple
 *                                        lookup has to be done
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : Pointer to tProtRegTbl entry if Success else NULL is
 *                       returned
 *                                                                          
 ****************************************************************************/
tProtRegTbl        *
MuxRegGetRegnTableEntry (tProtRegTuple * pProtRegnTuple, INT4 i4TreeId)
{
    tProtRegTbl         ProtRegEntry;

    MEMCPY (&ProtRegEntry.ProtRegTuple, pProtRegnTuple, sizeof (tProtRegTuple));

    /* Based on the Flag, select the Tree in which the Incoming Tuple lookup
     * has to be done */

    if (i4TreeId == MUX_REG_SEND_TO_ISS)
    {
        return (RBTreeGet (gpIssRegnTree, &ProtRegEntry));
    }
    else if (i4TreeId == MUX_REG_SEND_TO_IP)
    {
        return (RBTreeGet (gpIPRegnTree, &ProtRegEntry));
    }
    else
    {
        return NULL;
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxRegSearchMatchingEntry
 *                                                                          
 *    DESCRIPTION      : This Function gets a matching ISS Regn Table entry 
 *                       for the input tuples extracted from an incoming packet 
 *                       This is done by scanning the entire table since normal
 *                       compare routine does not handle presence of Hole
 *                       entries in the table which eventually lead to Search
 *                       failure
 *
 *    INPUT            : pProtRegnTuple - Tuple Information to be used for table
 *                                        lookup
 *                       i4TreeId       - Table (RB Tree) in which this tuple
 *                                        lookup has to be done
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : Pointer to tProtRegTbl entry if Success else NULL is
 *                       returned
 *                                                                          
 ****************************************************************************/
tProtRegTbl        *
MuxRegSearchMatchingEntry (tProtRegTuple * pProtRegnTuple, INT4 i4TreeId)
{
    /* Based on the Flag, select the Tree in which the Incoming Tuple lookup
     * has to be done */

    if (i4TreeId == MUX_REG_SEND_TO_ISS)
    {
        return (MuxRegFindMatchingTblEntry (gpIssRegnTree, pProtRegnTuple));
    }
    else if (i4TreeId == MUX_REG_SEND_TO_IP)
    {
        return (MuxRegFindMatchingTblEntry (gpIPRegnTree, pProtRegnTuple));
    }
    else
    {
        return NULL;
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxRegFindMatchingTblEntry
 *                                                                          
 *    DESCRIPTION      : This Function scans the given RB Tree with the incoming
 *                       search tuple extracted from the packet and returns
 *                       the matching entry from the tree ng packet buffer 
 *                                               
 *    INPUT            : pRbTree - RB Tree to be Searched
 *                       pSearchTuple - Incoming Tuple entries
 *                                                                          
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : Pointer to tProtRegTbl entry if Success else NULL is
 *                       returned
 *                                                                          
 ****************************************************************************/
PRIVATE tProtRegTbl *
MuxRegFindMatchingTblEntry (tRBTree pRbTree, tProtRegTuple * pSearchTuple)
{
    tProtRegTuple       IncomingTuple;
    tProtRegTuple      *pRegnTblTuple = NULL;
    tProtRegTbl        *pRegnNode = NULL;
    tProtRegTbl        *pNextRegnNode = NULL;
    INT4                i4ByteCount = 0;

    MUX_SCAN_TREE (pRbTree, pRegnNode, pNextRegnNode, tProtRegTbl *)
    {
        pRegnTblTuple = &(pRegnNode->ProtRegTuple);

        IncomingTuple.EthInfo.u2Protocol =
            pSearchTuple->EthInfo.u2Protocol & pRegnTblTuple->EthInfo.u2Mask;
        IncomingTuple.IPInfo.u2Protocol =
            pSearchTuple->IPInfo.u2Protocol & pRegnTblTuple->IPInfo.u2Mask;

        /* Masking the Incoming Registration Tuple's MAC Information */
        for (i4ByteCount = 0; i4ByteCount < MAC_ADDR_LEN; i4ByteCount++)
        {
            IncomingTuple.MacInfo.MacAddr[i4ByteCount] =
                pSearchTuple->MacInfo.MacAddr[i4ByteCount] &
                pRegnTblTuple->MacInfo.MacMask[i4ByteCount];
        }

        if (MUX_REG_ETH_PROTO_CMP (&IncomingTuple, pRegnTblTuple) &&
            MUX_REG_IP_PROTO_CMP (&IncomingTuple, pRegnTblTuple))
        {
            if (MEMCMP (&IncomingTuple.MacInfo.MacAddr,
                        pRegnTblTuple->MacInfo.MacAddr, sizeof (tMacAddr)) == 0)
            {
                return pRegnNode;
            }
        }
    }

    return NULL;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxRegExtractRegnTuple
 *                                                                          
 *    DESCRIPTION      : This Function extracts the Protocol Registration 
 *                       Tuple information from the incoming packet buffer 
 *                                               
 *    INPUT            : pu1Data - Incoming packet's data buffer
 *                                                                          
 *    OUTPUT           : pIncomingTuple - Filled up Tuple entries
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MuxRegExtractRegnTuple (UINT1 *pu1Data, tProtRegTuple * pIncomingTuple)
{
    tEnetV2Header      *pEthHdr = NULL;
    t_IP_HEADER        *pIPHdr = NULL;

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, "\n Entering %s \n", __FUNCTION__);

    pEthHdr = (tEnetV2Header *) pu1Data;

    /* Get the 3 Tuple <D-MAC, Ethernet Protocol, IP Protocol number from the 
     * incoming packet */

    /* Copy the Destination MAC from the incoming packet (first 6 bytes) */

    MEMCPY (MUX_REG_MAC_ADDR (pIncomingTuple), pu1Data, CFA_ENET_ADDR_LEN);

    /* Copy the Other 2 Tuples based on the Ethernet Protocol Type */

    MUX_REG_ETH_PROTO (pIncomingTuple) = OSIX_NTOHS (pEthHdr->u2LenOrType);

    switch (OSIX_NTOHS (pEthHdr->u2LenOrType))
    {
        case VLAN_PROTOCOL_ID:

            /* Skip the VLAN Header and copy the Ethernet and IP Protocol 
             * numbers */
            MEMCPY (&MUX_REG_ETH_PROTO (pIncomingTuple),
                    (pu1Data + CFA_VLAN_TAGGED_HEADER_SIZE),
                    CFA_VLAN_PROTOCOL_SIZE);
            MUX_REG_ETH_PROTO (pIncomingTuple) =
                OSIX_NTOHS (MUX_REG_ETH_PROTO (pIncomingTuple));
            pIPHdr = (t_IP_HEADER *) pu1Data + CFA_VLAN_TAGGED_HEADER_SIZE;
            MUX_REG_IP_PROTO (pIncomingTuple) = pIPHdr->u1Proto;
            break;

        case CFA_ENET_IPV4:

            pIPHdr = (t_IP_HEADER *) (pu1Data + CFA_ENET_V2_HEADER_SIZE);
            MUX_REG_IP_PROTO (pIncomingTuple) = pIPHdr->u1Proto;
            break;

        case CFA_ENET_IPV6:
            /* For MLDS packets.
             * Check Whether it is an ICMPv6 packet in the Next header. 
             * If so, the ICMP type should be Added in the IP Proto field. */
            if (pu1Data[CFA_ENET_V2_HEADER_SIZE + IPV6_HEADER_LEN] ==
                ICMPV6_PROTOCOL_ID)
            {
                /* Assign the Icmpv6 Type from the ICMPv6 header */
                MUX_REG_IP_PROTO (pIncomingTuple) =
                    (UINT1) pu1Data[CFA_ENET_V2_HEADER_SIZE +
                                    IPV6_HEADER_LEN +
                                    IPV6_HOP_BY_HOP_HEADER_LEN];
            }
            break;

        case CFA_ENET_SLOW_PROTOCOL:

            /* Copy the Subtype (For EOAM & LA packets) as a Qualifier since 
             * they both have same MAC and Ethertype [01-80-C2-00-00-02 and
             * 0x8809].
             * Subtype is a Single byte field and is present in the 14th byte
             * (just after Ethernet Protocol) */
            MUX_REG_IP_PROTO (pIncomingTuple) =
                (UINT1) pu1Data[CFA_ENET_V2_HEADER_SIZE];

            break;

        default:

            /* Just return the MAC as Key for Table Lookup [Case for all L2
             * Packets] */

            break;
    }

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, "\n Exiting %s \n", __FUNCTION__);
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxRegUpdateProtRegTable
 *                                                                          
 *    DESCRIPTION      : This Function registers/deregisters a Protocol Module
 *                       with MUX using the Tuple Information passed from the 
 *                       User Space. If a lookup with the incoming tuple fails,
 *                       then a new entry is allocated and added to the 
 *                       Registration table and status is updated. If entry is
 *                       present, then status updation is done.
 *
 *    INPUT            : pRegnTuple   - Contains 3 Tuple Information
 *                       u4ModuleId   - Unique Module Identifier
 *                       u1SendToFlag - Send to Flag. Specifies whether the
 *                                      packet needs to be posted to ISS and/or 
 *                                      Linux
 *                       u1RegStatus  - Registration Status to set/update 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : Returns OSIX_SUCCESS on Success else appropriate Error
 *                       Code like -ENOMEM is returned when memory allocation 
 *                       for an registration entry fails or -EFAULT while 
 *                       the Registration Table entry addition fails
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
MuxRegUpdateProtRegTable (tProtRegTuple * pRegnTuple,
                          UINT4 u4ModuleId,
                          UINT1 u1SendToFlag, UINT1 u1RegStatus)
{
    tProtRegTbl        *pRegnEntry = NULL;

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, "\n Entering %s \n", __FUNCTION__);

    /* Logic of this routine is...
     *
     * After Sanity Checks of the incoming arguments..
     *
     * 1) Add Incoming Tuples to ISS Regn Table if they are not present in
     *    the Database
     * 2) If a Reg Table Entry is present with the same input tuples, then
     *    get the entry and update its registration status [REGISTER/DEREGISTER]
     * 3) If entry doesn't exists, then create a new Reg'n Table entry with the 
     *    input tuples and set its Registrstion Status to REGISTER 
     * 4) If no room is available to register an input protocol tuple, then
     *    Log Error Msg and return Failure */

    if ((u1SendToFlag == 0) ||
        (u1SendToFlag & ~(MUX_REG_SEND_TO_ISS | MUX_REG_SEND_TO_IP)) != 0)
    {
        /* Invalid Send To Flag. Return Invalid Value error code */
        MUX_TRC (ALL_FAILURE_TRC,
                 "MuxRegUpdateProtRegTable : Invalid Send To Flag Value "
                 "Failed to update Registration table \n");
        return -EINVAL;
    }

    if ((pRegnEntry =
         MuxRegSearchMatchingEntry (pRegnTuple,
                                    (u1SendToFlag & MUX_REG_SEND_TO_ISS))) !=
        NULL)
    {
        /* Entry Exists ... Update the Registration Status & Flag */
        MUX_REG_STATUS (pRegnEntry) = u1RegStatus;
        MUX_REG_SENDTO_FLAG (pRegnEntry) |= u1SendToFlag;
    }
    else
    {
        if (u1RegStatus == MUX_DEREGISTER)
        {
            MUX_TRC (ALL_FAILURE_TRC,
                     "MuxRegUpdateProtRegTable : Trying to de-register an"
                     " non existing entry \r\n");
            return -EFAULT;
        }
        /* New Entry ... 
         * Allocate memory and add it to ISS Registration Table */

        if (MemAllocateMemBlock (gIssRegnTblPoolId, (UINT1 **) &pRegnEntry) ==
            MEM_SUCCESS)
        {
            MEMSET (pRegnEntry, 0, sizeof (tProtRegTbl));
            MEMCPY (&pRegnEntry->ProtRegTuple, pRegnTuple,
                    sizeof (tProtRegTuple));

            MUX_REG_SENDTO_FLAG (pRegnEntry) = u1SendToFlag;
            MUX_REG_STATUS (pRegnEntry) = u1RegStatus;
            MUX_REG_MODULE_ID (pRegnEntry) = u4ModuleId;

            if (RBTreeAdd (gpIssRegnTree, pRegnEntry) == RB_FAILURE)
            {
                MemReleaseMemBlock (gIssRegnTblPoolId, (UINT1 *) pRegnEntry);
                MUX_TRC (ALL_FAILURE_TRC,
                         "MuxRegUpdateProtRegTable : RB Tree Addition failed "
                         "while adding a Registraion Table Entry \n");
                return -EFAULT;
            }
        }
        else
        {
            MUX_TRC (ALL_FAILURE_TRC,
                     "MuxRegUpdateProtRegTable : Memory Allocation failed "
                     "while adding Registration Table Entry \n");
            return -ENOMEM;
        }
    }

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, "\n Exiting %s \n", __FUNCTION__);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxRegTupleTreeCmpFun
 *                                                                          
 *    DESCRIPTION      : This Function gets a matching 3 tuple entry from the
 *                       Protocol Registration table. This is used while 
 *                       processing the incoming packets and also while 
 *                       registering/deregistering an entry from the table
 *                       from ISS using ioctl ()
 *                                               
 *    INPUT            : prbTuple1 & prbTuple2 Pointer to the Tuple entries 
 *                       to be matched.
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : Returns 0 if Success. -1 or 1 in case of failure
 *                       scenarios (i.e no matching entry)
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
MuxRegTupleTreeCmpFun (tRBElem * prbElem1, tRBElem * prbElem2)
{
    tProtRegTuple      *pTuple1 = &((tProtRegTbl *) prbElem1)->ProtRegTuple;
    tProtRegTuple      *pTuple2 = &((tProtRegTbl *) prbElem2)->ProtRegTuple;
    tProtRegTuple       IncomingTuple;
    tProtRegTuple       RegnTblInfo;
    INT4                i4ByteCount = 0;
    INT4                i4CmpVal = 0;

    MEMSET (&IncomingTuple, 0, sizeof (tProtRegTuple));
    MEMSET (&RegnTblInfo, 0, sizeof (tProtRegTuple));

    /* Incoming packets will not have Mask Information and the
     * Modules registering from ISS WILL set valid Mask if Qualifier 
     * needs to be considered as a Valid Key or else it sets an invalid mask
     * say 0 to indicate that this Qualifier is a "don't care" key */

    if ((MuxUtilIsMacAllZeros (pTuple1->MacInfo.MacMask) == OSIX_TRUE) &&
        (pTuple1->EthInfo.u2Mask == 0) && (pTuple1->IPInfo.u2Mask == 0))

    {
        IncomingTuple.EthInfo.u2Protocol =
            pTuple1->EthInfo.u2Protocol & pTuple2->EthInfo.u2Mask;
        IncomingTuple.IPInfo.u2Protocol =
            pTuple1->IPInfo.u2Protocol & pTuple2->IPInfo.u2Mask;

        /* Masking the Incoming Packet MAC Information with the
         * Registration Table entry's Mask */
        for (; i4ByteCount < MAC_ADDR_LEN; i4ByteCount++)
        {
            IncomingTuple.MacInfo.MacAddr[i4ByteCount] =
                pTuple1->MacInfo.MacAddr[i4ByteCount] & pTuple2->MacInfo.
                MacMask[i4ByteCount];
            RegnTblInfo.MacInfo.MacAddr[i4ByteCount] =
                pTuple2->MacInfo.MacAddr[i4ByteCount] & pTuple2->MacInfo.
                MacMask[i4ByteCount];
        }
    }
    else
    {
        IncomingTuple.EthInfo.u2Protocol =
            pTuple1->EthInfo.u2Protocol & pTuple1->EthInfo.u2Mask;
        IncomingTuple.IPInfo.u2Protocol =
            pTuple1->IPInfo.u2Protocol & pTuple1->IPInfo.u2Mask;

        /* Masking the Incoming Registration Tuple's MAC Information */
        for (; i4ByteCount < MAC_ADDR_LEN; i4ByteCount++)
        {
            IncomingTuple.MacInfo.MacAddr[i4ByteCount] =
                pTuple1->MacInfo.MacAddr[i4ByteCount] & pTuple1->MacInfo.
                MacMask[i4ByteCount];
            RegnTblInfo.MacInfo.MacAddr[i4ByteCount] =
                pTuple2->MacInfo.MacAddr[i4ByteCount] & pTuple2->MacInfo.
                MacMask[i4ByteCount];
        }
    }

    RegnTblInfo.EthInfo.u2Protocol =
        pTuple2->EthInfo.u2Protocol & pTuple2->EthInfo.u2Mask;

    RegnTblInfo.IPInfo.u2Protocol =
        pTuple2->IPInfo.u2Protocol & pTuple2->IPInfo.u2Mask;

    /* If MAC Information matches, return the current entry */
    i4CmpVal = MEMCMP (IncomingTuple.MacInfo.MacAddr,
                       RegnTblInfo.MacInfo.MacAddr, sizeof (tMacAddr));
    if (i4CmpVal == 0)
    {

        /* Check for matching Ethernet Protocol number */

        if (IncomingTuple.EthInfo.u2Protocol > RegnTblInfo.EthInfo.u2Protocol)
        {
            return 1;
        }
        else if (IncomingTuple.EthInfo.u2Protocol <
                 RegnTblInfo.EthInfo.u2Protocol)
        {
            return -1;
        }

        /* Check for matching IP Protocol number */

        if (IncomingTuple.IPInfo.u2Protocol > RegnTblInfo.IPInfo.u2Protocol)
        {
            return 1;
        }
        else if (IncomingTuple.IPInfo.u2Protocol <
                 RegnTblInfo.IPInfo.u2Protocol)
        {
            return -1;
        }

        /* Incoming Tuple matches with the Registration Table Entry */
        return 0;
    }
    else
    {
        return i4CmpVal;
    }
}

#endif /* _MUX_REG_C_ */

/*-----------------------------------------------------------------------*/
/*                       End of the File muxreg.c                        */
/*-----------------------------------------------------------------------*/
