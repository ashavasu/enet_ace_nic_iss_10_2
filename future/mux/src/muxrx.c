/*********************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: muxrx.c,v 1.2 2010/08/10 13:27:21 prabuc Exp $
 *
 * Description: This file contains the Packet RX & processing thread      
 *              implementations. Rx thread will receive packet from NP
 *              and will classify the packet & decide whether to give the packet 
 *              to L2 (via NP User call back's) or to L3 (via Linux IP).
 ********************************************************************************/
#include "muxcmn.h"
#include "iss.h"
#include "netdev.h"

/************************** PRIVATE DECLARATIONS ******************************/

#if LINUX_VERSION_CODE == KERNEL_VERSION(2,6,24)
PRIVATE UINT4
 
 
 
MuxRxPrcsIncomingL2BridgePkt (UINT4 u4Hook, tSkb * pSkb,
                              CONST tNetDevice * pInDev,
                              CONST tNetDevice * pOutDev,
                              INT4 (*okfn) (tSkb *));
#else
PRIVATE UINT4
 
 
 
MuxRxPrcsIncomingL2BridgePkt (UINT4 u4Hook, tSkb ** ppSkb,
                              CONST tNetDevice * pInDev,
                              CONST tNetDevice * pOutDev,
                              INT4 (*okfn) (tSkb *));
#endif

/* Defining the Network Filter Hook Operation Structure with the MUX Callback 
 * Routines */
PRIVATE tNfHookOps  NetHookOps[2] = {
    {{NULL, NULL}, MuxRxPrcsIncomingL2BridgePkt, THIS_MODULE, PF_BRIDGE,
     NF_BR_PRE_ROUTING, NF_BR_PRI_FIRST},
    {{NULL, NULL}, MuxRxPrcsIncomingL2BridgePkt, THIS_MODULE, PF_BRIDGE,
     NF_BR_LOCAL_IN, NF_BR_PRI_FIRST}
};

/********************** END OF PRIVATE DECLARATIONS **************************/

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxRxPrcsIncomingL2BridgePkt
 *                                                                          
 *    DESCRIPTION      : This function gets registered as Net Filter's bridge
 *                       hook routine to fetch the incoming packets from the 
 *                       Linux Networking Stack. These packets are then given
 *                       to MUX for further processing's
 *
 *    INPUT            : u4Hook  - Bridge hook identifier
 *                       ppskb   - Reference to incoming packet buffer
 *                       indev   - Incoming Network Device associated with this
 *                                 packet
 *                       outdev  - Outgoing Network Device  associated with this
 *                                 packet
 *                       (*okfn) - Callback Routine that has to be called if
 *                                 this packet is sent back again to Linux 
 *                                 Networking stack
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : NF_STOLEN on Success else NF_DROP 
 *                                                                          
 ****************************************************************************/
#if LINUX_VERSION_CODE == KERNEL_VERSION(2,6,24)
PRIVATE UINT4
MuxRxPrcsIncomingL2BridgePkt (UINT4 u4Hook, tSkb * pSkb,
                              CONST tNetDevice * pInDev,
                              CONST tNetDevice * pOutDev, INT4 (*okfn) (tSkb *))
{
#else
PRIVATE UINT4
MuxRxPrcsIncomingL2BridgePkt (UINT4 u4Hook, tSkb ** ppSkb,
                              CONST tNetDevice * pInDev,
                              CONST tNetDevice * pOutDev, INT4 (*okfn) (tSkb *))
{
    tSkb               *pSkb = *ppSkb;
#endif
    INT4                i4RetVal = 0;

    UNUSED_PARAM (u4Hook);
    UNUSED_PARAM (pInDev);
    UNUSED_PARAM (pOutDev);
    UNUSED_PARAM (okfn);

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, "Entering %s \r\n", __FUNCTION__);

    skb_push (pSkb, CFA_ENET_V2_HEADER_SIZE);

    MuxUtilDumpSKB ((UINT1 *) "BR-HOOK->MUX", pSkb);

    /* Handing over the incoming packet to the Portable RX routine which in turn
     * sends it the interested modules */

    i4RetVal = MuxPortPrcsRxFrame (pSkb, 0, 0);

    if (i4RetVal == OSIX_SUCCESS)
    {
        /* Indicate Kernel NetFilter Module that we are done with the
         * processing and no further processing is required for this packet */
        return NF_STOLEN;
    }

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, "Exiting %s \r\n", __FUNCTION__);

    /* Indicate Kernel to Free up the Memory(SKB) associated with this packet */
    return NF_DROP;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxRxProcessIncomingFrame
 *                                                                          
 *    DESCRIPTION      : This function processes incoming frames on Ethernet
 *                       Interfaces, classifes them as L2 or L3 and posts
 *                       them to their appropriate handlers 
 *
 *    INPUT            : pSkb         - Packet buffer         
 *                       u2IfIndex    - Interface Index
 *                       VlanId       - Device on which the packet is recvd
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS If the packet gets processed 
 *                       successfully
 *                      
 *                       OSIX_FAILURE in case of
 *                         a) Status of the Matching Registration Entry is 
 *                            DEREGISTER i.e Shutdown
 *                         b) Non L3 Packet and ISS is not interested to 
 *                            receive this packet
 *
 *                       For the above 2 failure conditions, SKB Memory is free
 *                       up by the NetFilter Module in Linux Kernel. This is
 *                       accomplished by returning NF_DROP from the caller
 *                       routine. So care has to be taken while returning
 *                       failures, in future.
 *                                                                          
 ****************************************************************************/
INT4
MuxRxProcessIncomingFrame (tSkb * pSkb, UINT2 u2IfIndex, tVlanId VlanId)
{
    tProtRegTuple       IncomingTuple;
    tProtRegTbl        *pProtRegEntry = NULL;
    UINT1              *pu1Data = pSkb->data;
    UINT4               u4PktSize = pSkb->len;
    BOOL1               b1PktSentToIP = OSIX_FALSE;

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, "Entering %s \r\n", __FUNCTION__);

    MEMSET (&IncomingTuple, 0, sizeof (tProtRegTuple));

    MuxUtilDumpSKB ((UINT1 *) "MUX-RX", pSkb);

    /* Extract the 3 Tuple Fields from the Incoming Packet Buffer */

    MuxRegExtractRegnTuple (pu1Data, &IncomingTuple);

    /* Incoming Packet Processing Logic is ...
     *
     * 1) Do ISS Registration Table lookup with the incoming packet tuples and 
     *    send it to Linux and/or ISS based on the u1SendToFlag present in the
     *    matched table entry
     * 2) After the Table Lookup, if the matching table entry's Registration
     *    Status happens to be in DEREGISTER State, then the incoming packet 
     *    gets dropped 
     * 3) L3 Packets pertaining to an unregistered module is sent to Linux by
     *    default */

    if ((pProtRegEntry = MuxRegSearchMatchingEntry (&IncomingTuple,
                                                    MUX_REG_SEND_TO_ISS)) !=
        NULL)
    {
        if (MUX_REG_STATUS (pProtRegEntry) == MUX_DEREGISTER)
        {
            return OSIX_FAILURE;
        }
        else
        {
            if (MUX_REG_SENDTO_FLAG (pProtRegEntry) & MUX_REG_SEND_TO_ISS)
            {
                /* Post the Incoming Packet along with the Module ID to which 
                 * this packet has to be enqueued */

                MuxUtilPostPktToUser (pu1Data, u4PktSize, u2IfIndex,
                                      MUX_REG_MODULE_ID (pProtRegEntry));
            }

            if (MUX_REG_SENDTO_FLAG (pProtRegEntry) & MUX_REG_SEND_TO_IP)
            {
                MuxRxProcessIncomingL3Pkt (pSkb, u2IfIndex, VlanId);
                b1PktSentToIP = OSIX_TRUE;
            }
        }
    }

    /* All L3 Packets (IP V4/V6) pertaining to an unregistered module are sent
     * to Linux Networking Stack by default. Rest of the packets are dropped */

    if (b1PktSentToIP == OSIX_FALSE)
    {
        if ((MUX_REG_ETH_PROTO (&IncomingTuple) == CFA_ENET_IPV4) ||
            (MUX_REG_ETH_PROTO (&IncomingTuple) == CFA_ENET_IPV6))
        {
            MuxRxProcessIncomingL3Pkt (pSkb, u2IfIndex, VlanId);
        }
        else
        {
            /* All other packets [Non L3 packets and packets that are not
             * required for any ISS module] gets dropped here... */
            return OSIX_FAILURE;
        }
    }

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, "Exiting %s \r\n", __FUNCTION__);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxRxProcessIncomingL3Pkt
 *                                                                          
 *    DESCRIPTION      : This function processes incoming L3 packets and post
 *                       them to Linux IP and takes care of updating counters
 *
 *    INPUT            : pSkb         - Packet buffer         
 *                       u2IfIndex    - Interface Index
 *                       VlanId       - VLAN ID over which the packet is 
 *                                      received
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS If the packet gets processed 
 *                       succeessfully, otherwise OSIX_FAILURE.
 *                                                                          
 ****************************************************************************/
INT4
MuxRxProcessIncomingL3Pkt (tSkb * pSkb, UINT2 u2IfIndex, tVlanId VlanId)
{
    tNetDevice         *pDev = NULL;
    tNetDevPrivData    *pDevPrivData = NULL;
    UINT1               au1DeviceName[CFA_MAX_PORT_NAME_LENGTH];

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, "Entering %s \r\n", __FUNCTION__);

    MEMSET (au1DeviceName, 0, CFA_MAX_PORT_NAME_LENGTH);

    /* Check whether the packet had arrived over Router Port Interface. If No,
     * then check whether it had arrived from VLAN Interface. Associate the 
     * respective Netdevices to the SKB and post them to Linux Networking Stack
     * Drop the packets which is not associated to any of these L3 Interfaces */

    SPRINTF ((CHR1 *) & au1DeviceName, "%s", ISS_RPORT_ALIAS_PREFIX);
    SPRINTF ((CHR1 *) & au1DeviceName[STRLEN (ISS_RPORT_ALIAS_PREFIX)],
             "%d", u2IfIndex);

    pDev = KERN_DEV_GET_BY_NAME (au1DeviceName);

    if (pDev == NULL)
    {
        SPRINTF ((CHR1 *) & au1DeviceName, "%s", CFA_STACK_VLAN_INTERFACE);
        SPRINTF ((CHR1 *) & au1DeviceName[STRLEN (CFA_STACK_VLAN_INTERFACE)],
                 "%d", VlanId);

        pDev = KERN_DEV_GET_BY_NAME (au1DeviceName);
        if (pDev == NULL)
        {
            MUX_TRC (ALL_FAILURE_TRC, "Unknown Network Device");
            /* Free up the Incoming Buffer before dropping the Packet */
            KERN_FREE_SKB (pSkb);
            return OSIX_FAILURE;
        }
    }

    KERN_DEV_GIVE_SEM (pDev);

    /* Packets to be posted to Linux Networking Stack has to be Untagged
     * since it does not understand the Tags */

    MuxUtilUnTagPkt (pSkb);

    /* Check whether this packet needs VRRP processing and associate the
     * device information (VRRP network device) if needed */

    pSkb->dev = pDev;

    /* Get the Private Info for updating the Statistical Counters */
    pDevPrivData = KERN_NETDEV_PRIV_DATA (pDev);

    pSkb->ip_summed = CHECKSUM_NONE;

    pSkb->protocol = KERN_GET_L2PROT_ID (pSkb, pDev);

    pSkb->pkt_type = 0;

#ifdef VRRP_WANTED
    MuxVrrpPrcsIncomingPkt (pSkb, pDev);
#endif

    MuxUtilDumpSKB ((UINT1 *) "MUX->LNX", pSkb);

    KERN_POST_TO_LNXIP (pSkb);

    pDevPrivData->NetDevStats.rx_bytes += pSkb->len;
    pDevPrivData->NetDevStats.rx_packets++;

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, "Exiting %s \r\n", __FUNCTION__);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxRxNetFilterInit
 *                                                                          
 *    DESCRIPTION      : This function register the Bridge Hook for getting
 *                       the packets from the Linux Networking Stack to MUX 
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MuxRxNetFilterInit (VOID)
{
    MUX_TRC_ARG1 (MUX_DEBUG_TRC, "Entering %s \r\n", __FUNCTION__);

    /* Register the Hook Points required to receive incoming packets from Linux
     * Networking Stack */

    nf_register_hook (&NetHookOps[0]);
    nf_register_hook (&NetHookOps[1]);

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, "Exiting %s \r\n", __FUNCTION__);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxRxNetFilterDeInit
 *                                                                          
 *    DESCRIPTION      : This function deregister's the Bridge Hook that is 
 *                       used for getting the packets from the Linux Networking
 *                       Stack to MUX 
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MuxRxNetFilterDeInit (VOID)
{
    MUX_TRC_ARG1 (MUX_DEBUG_TRC, "Entering %s \r\n", __FUNCTION__);

    /* Deregister the Hook points done earlier */

    nf_unregister_hook (&NetHookOps[0]);
    nf_unregister_hook (&NetHookOps[1]);

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, "Exiting %s \r\n", __FUNCTION__);
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  muxrx.c                        */
/*-----------------------------------------------------------------------*/
