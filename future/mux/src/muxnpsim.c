/****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: muxnpsim.c,v 1.2 2010/08/10 13:27:21 prabuc Exp $
 *
 * Description: This file contains API's for managing the Character Device
 *              that exists b/w NPSIM & MUX Module
 ***************************************************************************/
#include "muxcmn.h"
#include "netdev.h"

/************************** PRIVATE DECLARATIONS ******************************/

PRIVATE INT4 MuxNpSimDevOpen PROTO ((struct inode * pINode,
                                     struct file * pFile));
PRIVATE INT4 MuxNpSimDevRelease PROTO ((struct inode * pINode,
                                        struct file * pFile));
PRIVATE INT4 MuxNpSimIoctlHandler PROTO ((struct inode * pINode,
                                          struct file * pFile,
                                          UINT4 u4IoctlNum,
                                          FS_ULONG u4IoctlParam));
PRIVATE SSIZE_T MuxNpSimDevRead PROTO ((struct file * pFile, CHR1 * pu1Buf,
                                        SIZE_T BufLen, OFFST_T * pPos));
PRIVATE SSIZE_T MuxNpSimDevWrite PROTO ((struct file * pFile, CONST CHR1 * pBuf,
                                         SIZE_T BufLen, OFFST_T * pPos));

PRIVATE struct file_operations FileOps = {
  owner:THIS_MODULE,
  read:MuxNpSimDevRead,
  write:MuxNpSimDevWrite,
  ioctl:MuxNpSimIoctlHandler,
  open:MuxNpSimDevOpen,
  release:MuxNpSimDevRelease,
};

/********************** END OF PRIVATE DECLARATIONS **************************/

tMuxDevInfo        *gpMuxNpsimInfo = NULL;

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxNpSimDevRegister
 *                                                                          
 *    DESCRIPTION      : Function to Register MUX<->NPSIM Char Device with Linux
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_FAILURE if Registration Failed else OSIX_SUCCESS
 *                                                                          
 ****************************************************************************/
INT4
MuxNpSimDevRegister (VOID)
{
    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    /* Register the Character Device */
    if (KERN_REG_CHRDEV (NPSIM_DEVICE_MAJOR_NUMBER, NPSIM_DEVICE_FILE_NAME,
                         &FileOps) < 0)
    {
        MUX_TRC (ALL_FAILURE_TRC, " MuxNpSimDevRegister : Registration Failed");
        return OSIX_FAILURE;
    }

    gpMuxNpsimInfo = MEM_MALLOC (sizeof (tMuxDevInfo), tMuxDevInfo);

    if (NULL == gpMuxNpsimInfo)
    {
        KERN_DEREG_CHRDEV (NPSIM_DEVICE_MAJOR_NUMBER, NPSIM_DEVICE_FILE_NAME);
        MUX_TRC (OS_RESOURCE_TRC, " MuxNpSimDevRegister : Memory Allocation "
                 "Failed");
        return OSIX_FAILURE;
    }

    /* Intialize the Wait Queue */

    KERN_INIT_WAIT_QUEUE (gpMuxNpsimInfo)
        gpMuxNpsimInfo->u4Limit = MUX_CHAR_DEV_Q_SIZE;

    MUX_TRC (MUX_INFO_TRC, " MuxNpSimDevRegister : NPSIM <-> MUX Character "
             "Device Registration : Success");

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxNpSimDevDeRegister
 *                                                                          
 *    DESCRIPTION      : Function to DeRegister MUX<->NPSIM Char Device
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MuxNpSimDevDeRegister (VOID)
{
    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    MEM_FREE (gpMuxNpsimInfo);

    KERN_DEREG_CHRDEV (NPSIM_DEVICE_MAJOR_NUMBER, NPSIM_DEVICE_FILE_NAME);

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxPortXmitCPUPacket
 *                                                                          
 *    DESCRIPTION      : This is a Portable API called from MuxTxCPUPacket
 *                       when ISS tries to send a packet out
 *
 *    INPUT            : pSkb      - Pointer to the Socket Buffer
 *                       VlanId    - Outgoing VLAN Identifier
 *                       u4OutPort - Outgoing Port
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : No. of bytes posted Successfully or appropriate
 *                       Error Code on failure cases
 *                                                                          
 ****************************************************************************/
INT4
MuxPortXmitCPUPacket (tSkb * pSkb, tVlanId VlanId, UINT4 u4OutPort)
{
    INT4                i4BytesSent = 0;

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4OutPort);

    if ((i4BytesSent = MuxUtilPostMsgToUser (gpMuxNpsimInfo, NULL, 0,
                                             pSkb->data,
                                             pSkb->len)) != (INT4) pSkb->len)
    {
        MUX_TRC (ALL_FAILURE_TRC, " MuxPortXmitCPUPacket : TX Failed "
                 "Posting Packet to NPSIM Failed \n");
    }

    /* Free up the SKB here since the data would have been already sent to 
     * NPSIM for further transmission */
    KERN_FREE_SKB (pSkb);
    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);
    return i4BytesSent;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxPortXmitDevPacket
 *                                                                          
 *    DESCRIPTION      : This is a Portable API called from MuxTxDevPacket 
 *                       for transmitting a packet from the Logical Net Device 
 *
 *    INPUT            : pSkb   - Pointer to the Socket Buffer
 *                       pDev   - Pointer to the Net Device Structure over 
 *                                which packet is to be TX'd
 *
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : Returns OSIX_SUCCESS on successful packet transmission
 *                       or OSIX_FAILURE in case failure scenarios
 *                                                                          
 ****************************************************************************/
INT4
MuxPortXmitDevPacket (tSkb * pSkb, tNetDevice * pDev)
{
    tNetDevPrivData    *pDevPriv = KERN_NETDEV_PRIV_DATA (pDev);
    tSkb               *pTempSkb = NULL;
    tVlanId             VlanId;
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT2               u2NewHeadRoom = NPSIM_MS_HDR_LEN;
    UINT1               u1Priority = 0;

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    /* If the packet is to be sent over VLAN Interface, then Add the VLAN 
     * Header to the incoming packet buffer and send it out */

    MuxUtilGetVlanIdFromDev (pDev, &VlanId);

    if (VlanId != 0)
    {
        u2NewHeadRoom += VLAN_TAG_PID_LEN;
    }

    /* Packets originating from the Linux has to be added with VLAN Header (if
     * Outgoing IF is VLAN) and a MS Header before posting to NPSIM for proper 
     * processing. A Dummy MS header is added so that NPSIM module processes
     * these packets properly */
    if ((UINT4) skb_headroom (pSkb) < u2NewHeadRoom)
    {
        if ((pTempSkb = skb_copy_expand (pSkb, u2NewHeadRoom, 0,
                                         GFP_ATOMIC)) != NULL)
        {
            KERN_FREE_SKB (pSkb);
            pSkb = pTempSkb;
        }
        else
        {
            /* We drop the packet; If we are unable to allocate */
            KERN_FREE_SKB (pSkb);
            pDevPriv->NetDevStats.tx_errors++;
            MUX_TRC (OS_RESOURCE_TRC, " MuxPortXmitDevPacket : TX Failed "
                     "Unable to allocate additional memory for MS and VLAN Header \n");
            return OSIX_FAILURE;
        }
    }

    if (VlanId != 0)
    {
        MuxUtilTagVlanInfo (pSkb, VLAN_PROTOCOL_ID, VlanId, u1Priority);
    }

    /* CLearing NPSIM MS Header contents and adjusting SKB Data Buffer Length */

    pSkb->data = pSkb->data - NPSIM_MS_HDR_LEN;
    pSkb->len += NPSIM_MS_HDR_LEN;
    MEMSET (pSkb->data, 0, NPSIM_MS_HDR_LEN);

    pDev->trans_start = jiffies;

    if (MuxUtilPostMsgToUser (gpMuxNpsimInfo, NULL, 0, pSkb->data,
                              pSkb->len) != (INT4) pSkb->len)
    {
        i4RetVal = OSIX_FAILURE;
    }

    /* update the statistic counters */
    if (OSIX_FAILURE == i4RetVal)
    {
        pDevPriv->NetDevStats.tx_errors++;
        MUX_TRC (ALL_FAILURE_TRC, " MuxPortXmitDevPacket : TX Failed "
                 "Posting Packet to NPSIM Failed \n");
    }
    else
    {
        pDevPriv->NetDevStats.tx_bytes += pSkb->len;
        pDevPriv->NetDevStats.tx_packets++;
    }

    /* Free up the SKB here since the data would have been already sent to 
     * NPSIM for further transmission */
    KERN_FREE_SKB (pSkb);

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);
    return i4RetVal;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxCustInit
 *                                                                          
 *    DESCRIPTION      : Does Custom Init'n specific to NPSIM
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS on successful Char Device registration
 *                       Else returns OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MuxCustInit (VOID)
{
    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    if (MuxNpSimDevRegister () == OSIX_FAILURE)
    {
        MUX_TRC (ALL_FAILURE_TRC,
                 "\r\n Registering MUX<->NPSIM Character Device : Failed");
        return OSIX_FAILURE;
    }

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxCustDeInit
 *                                                                          
 *    DESCRIPTION      : Revert Custom changes done specific for NPSIM
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MuxCustDeInit (VOID)
{
    MuxNpSimDevDeRegister ();

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxNpSimDevOpen  
 *                                                                          
 *    DESCRIPTION      : Function to Open the Char device. 
 *
 *    INPUT            : pINode - Pointer to the INode Structure 
 *                       pFile  - Pointer to the File Structure 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : 0 for Success and -1 for failure 
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
MuxNpSimDevOpen (struct inode *pINode, struct file *pFile)
{
    UNUSED_PARAM (pINode);

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    pFile->private_data = (void *) gpMuxNpsimInfo;

    KERN_INC_MOD_USAGE_COUNT ();

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);
    return 0;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxNpSimDevRelease
 *                                                                          
 *    DESCRIPTION      : Function to Release (close) the Char device. 
 *
 *    INPUT            : pINode - Pointer to the INode Structure 
 *                       pFile  - Pointer to the File Structure 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : 0 for Success and -1 for failure 
 *                                                                          
 ****************************************************************************/

PRIVATE INT4
MuxNpSimDevRelease (struct inode *pINode, struct file *pFile)
{
    UNUSED_PARAM (pINode);

    UNUSED_PARAM (pFile);

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    KERN_DEC_MOD_USAGE_COUNT ();

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);
    return 0;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxNpSimDevRead
 *                                                                          
 *    DESCRIPTION      : API to read from this Char Device from ISS
 *
 *    INPUT            : pFile        - Pointer to the File Structure 
 *                       BufLen   - Sice of User Space Buffer  
 *                       pPos          - postion to start the reading
 *                                                                          
 *    OUTPUT           : pu1Buf       - Buffer to store
 *                                                                          
 *    RETURNS          : Length of the Data Read or Appropiate Error Code
 *                                                                          
 ****************************************************************************/
PRIVATE             SSIZE_T
MuxNpSimDevRead (struct file * pFile, CHR1 * pu1Buf, SIZE_T BufLen,
                 OFFST_T * pPos)
{
    tSkbHead           *pBuffHead = NULL;
    tSkb               *pSkb = NULL;
    tMuxDevInfo        *pMuxNpSimDevInfo = NULL;

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    pMuxNpSimDevInfo = (tMuxDevInfo *) pFile->private_data;

    if (BufLen == 0)
    {
        MUX_TRC (ALL_FAILURE_TRC,
                 "MuxNpSimDevRead : Invalid Buffer Length Specified \n");
        return 0;
    }

    /* Acquire the lock */
    KERN_TAKE_SEM (&pMuxNpSimDevInfo->ChrDevSem);

    pBuffHead = &(pMuxNpSimDevInfo->ChrDevBufHead);

    /* Check for Data  Availability */
    if (pBuffHead->qlen == 0)
    {
        if (pFile->f_flags & O_NONBLOCK)
        {
            KERN_GIVE_SEM (&pMuxNpSimDevInfo->ChrDevSem);
            return -EFAULT;
        }

        MUX_TRC (MUX_INFO_TRC, "MuxNpSimDevRead : No Data Available \n");

        /* Release the Lock */
        KERN_GIVE_SEM (&pMuxNpSimDevInfo->ChrDevSem);

        /* suspend the process */
        KERN_SLEEP_CUR_PRCS (pMuxNpSimDevInfo->ChrDevWQ, pBuffHead->qlen);

        /* when woken up check for signals */
        if (KERN_CHECK_FOR_SIGNAL (current))
        {
            return -ERESTART;
        }

        /* Acquire the lock */
        KERN_TAKE_SEM (&pMuxNpSimDevInfo->ChrDevSem);
    }

    MUX_TRC (MUX_INFO_TRC, "MuxNpSimDevRead : Data Available \n");

    pSkb = KERN_DEQUEUE_BUF (pBuffHead);

    if (NULL == pSkb)
    {
        KERN_GIVE_SEM (&pMuxNpSimDevInfo->ChrDevSem);    /* Release the Lock */
        MUX_TRC (ALL_FAILURE_TRC,
                 "MuxNpSimDevRead : Invalid Buffer in Queue \n");
        return -EFAULT;
    }

    if (pSkb->len <= *pPos)
    {

        KERN_FREE_SKB (pSkb);
        KERN_GIVE_SEM (&pMuxNpSimDevInfo->ChrDevSem);    /* Release the Lock */
        MUX_TRC (ALL_FAILURE_TRC,
                 "MuxNpSimDevRead : Invalid Offset Specified \n");
        return -EFAULT;
    }

    /* check if requested len is greater than available len */
    if (BufLen > (pSkb->len - *pPos))
    {
        BufLen = pSkb->len - *pPos;

    }
    else
    {
        KERN_FREE_SKB (pSkb);
        KERN_GIVE_SEM (&pMuxNpSimDevInfo->ChrDevSem);    /* Release the Lock */
        return -EFAULT;
    }

    /* copy the data to the user */
    if (KERN_COPY_TO_USER (pu1Buf, pSkb->data + *pPos, BufLen))
    {
        KERN_FREE_SKB (pSkb);
        /* Release the Lock */
        KERN_GIVE_SEM (&pMuxNpSimDevInfo->ChrDevSem);
        MUX_TRC (ALL_FAILURE_TRC,
                 "MuxNpSimDevRead : Failed to copy the data to User Space \n");
        return -EFAULT;
    }

    KERN_FREE_SKB (pSkb);

    KERN_GIVE_SEM (&pMuxNpSimDevInfo->ChrDevSem);    /* Release the Lock */

    MUX_TRC (MUX_INFO_TRC, "MuxNpSimDevRead : Read Success \n");

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);
    return BufLen;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxNpSimDevWrite
 *                                                                          
 *    DESCRIPTION      : API to write into this Char Device from ISS
 *
 *    INPUT            : pFile        - Pointer to the File Structure 
 *                       pu1Buf       - Buffer to store
 *                       BufLen   - Sice of User Space Buffer  
 *                       pPos          - postion to start the reading
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : Length of the Data writen or Appropiate Error Code
 *                                                                          
 ****************************************************************************/
PRIVATE             SSIZE_T
MuxNpSimDevWrite (struct file * pFile, CONST CHR1 * pBuf, SIZE_T BufLen,
                  OFFST_T * pPos)
{
    tSkbHead           *pBuffHead = NULL;
    tSkb               *pSkb = NULL;
    tMuxDevInfo        *pMuxNpSimDevInfo = NULL;

    UNUSED_PARAM (pPos);

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    pMuxNpSimDevInfo = (tMuxDevInfo *) pFile->private_data;

    if (BufLen == 0)
    {
        MUX_TRC (ALL_FAILURE_TRC,
                 "MuxNpSimDevWrite : Invalid Buffer Length Specified \n");
        return 0;
    }

    MUX_TRC (MUX_INFO_TRC, "MuxNpSimDevWrite : Trying to acquire the lock\n");

    if (KERN_WAIT_FOR_SEM (&pMuxNpSimDevInfo->ChrDevSem))
    {
        if (KERN_CHECK_FOR_SIGNAL (current))
        {
            return -ERESTARTSYS;
        }

        return -EFAULT;
    }

    MUX_TRC (MUX_INFO_TRC, "MuxNpSimDevWrite : Acquired the lock\n");

    pBuffHead = &(pMuxNpSimDevInfo->ChrDevBufHead);

    if (pBuffHead->qlen >= pMuxNpSimDevInfo->u4Limit)
    {
        MUX_TRC (ALL_FAILURE_TRC,
                 "MuxNpSimDevWrite : Dropped the Msg. No queue len Available \n");
        KERN_GIVE_SEM (&pMuxNpSimDevInfo->ChrDevSem);
        return -EFAULT;
    }

    pSkb = KERN_ALLOC_SKB (BufLen, GFP_KERNEL);

    if (NULL == pSkb)
    {
        /* Release the Lock */
        KERN_GIVE_SEM (&pMuxNpSimDevInfo->ChrDevSem);
        MUX_TRC (OS_RESOURCE_TRC,
                 "MuxNpSimDevWrite : Allocation of Buffer Failed\n");
        return -EFAULT;
    }

    if (KERN_COPY_FROM_USER (skb_put (pSkb, BufLen), pBuf, BufLen))
    {
        /* Release the Lock */
        KERN_FREE_SKB (pSkb);
        KERN_GIVE_SEM (&pMuxNpSimDevInfo->ChrDevSem);
        MUX_TRC (ALL_FAILURE_TRC,
                 "MuxNpSimDevWrite : Failed to copy data from User " "Space\n");
        return -EFAULT;
    }
    KERN_ENQUEUE_BUF (pBuffHead, pSkb);

    KERN_GIVE_SEM (&pMuxNpSimDevInfo->ChrDevSem);

    KERN_WAKE_UP_PRCS (&pMuxNpSimDevInfo->ChrDevWQ);

    MUX_TRC_ARG1 (MUX_INFO_TRC,
                  "MuxNpSimDevWrite : Written %d bytes of data \n", BufLen);

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return BufLen;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxNpsimIoctlHandler
 *                                                                          
 *    DESCRIPTION      : Function for handling ioctl () requests from NPSIM 
 *
 *    INPUT            : pINode - Inode Pointer
 *                       pFile  - File Pointer
 *                       u4IoctlNum   - ioctl Command number                                                   
 *                       u4IoctlParam - Parameters for a particular command
 *
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : IOCTL_SUCCESS if Success or else Error Code
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
MuxNpSimIoctlHandler (struct inode *pINode, struct file *pFile,
                      UINT4 u4IoctlNum, FS_ULONG u4IoctlParam)
{
    tSkb               *pSkb = NULL;
    tKernCmdParam       MuxCmdParams;
    INT4                i4RetVal = 0;
    UINT2               u2IfIndex = 0;
    UINT4               u4PktSize = 0;

    UNUSED_PARAM (pINode);
    UNUSED_PARAM (pFile);

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    MEMSET (&MuxCmdParams, 0, sizeof (tKernCmdParam));

    switch (u4IoctlNum)
    {
        case NPSIM_ISS_DEV_WRITE:
        {
            if (KERN_COPY_FROM_USER (&MuxCmdParams.MuxDevWriteParams,
                                     (tMuxDevWriteParams *) u4IoctlParam,
                                     sizeof (tMuxDevWriteParams)))
            {
                MUX_TRC (ALL_FAILURE_TRC, "MuxNpSimIoctlHandler : "
                         "Failed to get the Write Params \n");
                return (-EFAULT);
            }

            /* MS Header has Switch number and If Index 2 bytes each */
            MEMCPY (&u2IfIndex,
                    (MuxCmdParams.MuxDevWriteParams.pu1DataBuf + 2),
                    sizeof (UINT2));

            /* Removing the NPSIM Header Length */
            u4PktSize =
                MuxCmdParams.MuxDevWriteParams.u4PktSize - NPSIM_MS_HDR_LEN;

            /* SKB Allocation is done & passed to the Packet Classifier Module 
             * where it is given to either ISS/Linux */

            pSkb = dev_alloc_skb (u4PktSize + 2);

            if (NULL == pSkb)
            {
                MUX_TRC (OS_RESOURCE_TRC,
                         " MuxNpSimIoctlHandler : SKB Allocation Failed \r\n");
                return (-ENOMEM);
            }

            skb_reserve (pSkb, 2);

            /* Strip off the MS Header while forming the Socket Buffer */
            MEMCPY (skb_put (pSkb, u4PktSize),
                    (MuxCmdParams.MuxDevWriteParams.pu1DataBuf +
                     NPSIM_MS_HDR_LEN), u4PktSize);

            if (MuxPortPrcsRxFrame (pSkb, u2IfIndex,
                                    MuxCmdParams.MuxDevWriteParams.u2VlanId)
                == OSIX_SUCCESS)
            {

                /* Since NPSIM Header Length is not taken into account while 
                 * forming SKB, On Successfull processing by MUX RX routine
                 * return the actual no. of bytes passed by the NPSIM */
                MuxCmdParams.MuxDevWriteParams.i4RetVal =
                    MuxCmdParams.MuxDevWriteParams.u4PktSize;
            }
            else
            {
                /* MUX RX processing failure case */
                MuxCmdParams.MuxDevWriteParams.i4RetVal = (-EFAULT);
            }

            /* Copy NPAPI return value(s) back to user */
            if (KERN_COPY_TO_USER ((tMuxDevWriteParams *) u4IoctlParam,
                                   &MuxCmdParams.MuxDevWriteParams,
                                   sizeof (tMuxDevWriteParams)))
            {
                MUX_TRC (ALL_FAILURE_TRC, "MuxNpSimIoctlHandler : "
                         "Failed to copy back the return values \n");
                return (-EFAULT);
            }
            else
            {
                i4RetVal = IOCTL_SUCCESS;
            }
            break;
        }
        default:
            MUX_TRC (ALL_FAILURE_TRC,
                     "MuxNpSimIoctlHandler : Invalid Ioctl Request \n");
            break;
    }
    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);
    return (OSIX_SUCCESS);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxPortPrcsRxFrame 
 *                                                                          
 *    DESCRIPTION      : This portable API receives the packet from NPSIM and
 *                       calls the Mux RX routine (MuxRxProcessIncomingFrame())
 *
 *    INPUT            : pSkb       - Socket Buffer
 *                       u2SrcPort  - Incoming Port
 *                       VlanId     - Vlan ID                                                  
 *
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS If the packet gets processed successfully,
 *                       otherwise OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MuxPortPrcsRxFrame (tSkb * pSkb, UINT2 u2SrcPort, tVlanId VlanId)
{

    return (MuxRxProcessIncomingFrame (pSkb, u2SrcPort, VlanId));
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  muxnpsim.c                     */
/*-----------------------------------------------------------------------*/
