/****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: muxtx.c,v 1.2 2010/08/10 13:27:21 prabuc Exp $
 *
 * Description: This File contains the routines to transmit a packet out of
 *              an interface managed by MUX Module
 ***************************************************************************/
#include "muxcmn.h"
#include "muxport.h"
#include "netdev.h"

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxTxCPUPacket
 *                                                                          
 *    DESCRIPTION      : Function to TX a packet over an interface using the
 *                       Portable API MuxPortXmitCPUPacket ().
 *
 *    INPUT            : pSkb      - Pointer to the Socket Buffer
 *                       VlanId    - Outgoing VLAN Identifier
 *                       u2LPort   - Outgoing LogicalPort
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : No. of bytes posted/transmitted Successfully or 
 *                       appropriate error code on failure cases
 *                                                                          
 ****************************************************************************/
INT4
MuxTxCPUPacket (tSkb * pSkb, tVlanId VlanId, UINT4 u4LPort)
{
    /* Just Call the Portable Routine for transmitting the packets arriving
     * from ISS */
    return (MuxPortXmitCPUPacket (pSkb, VlanId, u4LPort));
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxTxDevPacket
 *                                                                          
 *    DESCRIPTION      : Function to TX a packet over an Logical L3 Interface
 *                       using the Portable API MuxPortXmitDevPacket ()
 *
 *    INPUT            : pSkb   - Pointer to the Socket Buffer
 *                       pDev   - Pointer to the Net Device Structure over 
 *                                which packet is to be TX'd
 *
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : Returns OSIX_SUCCESS on successful packet transmission
 *                       or OSIX_FAILURE in case failure scenarios
 *                                                                          
 ****************************************************************************/
INT4
MuxTxDevPacket (tSkb * pSkb, tNetDevice * pDev)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

#ifdef VRRP_WANTED
    MuxVrrpPrcsOutgoingPkt (pSkb, pDev->ifindex);
#endif

    /* Transmit the packet to the underlying switch driver by calling up the
     * MUX Module's exported TX routine */
    i4RetVal = MuxPortXmitDevPacket (pSkb, pDev);

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return i4RetVal;
}

/* Exporting the above routine so that ISS NetDevice Module shall use this to 
 * transmit a packet to the underlying Switch Driver 
 * [eg: NPSIM or Marvell Chip] */

EXPORT_SYMBOL (MuxTxDevPacket);

/*-----------------------------------------------------------------------*/
/*                       End of the file  muxtx.c                        */
/*-----------------------------------------------------------------------*/
