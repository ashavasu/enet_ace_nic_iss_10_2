/****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: muxdev.c,v 1.4 2010/09/25 08:29:42 prabuc Exp $
 *
 * Description: This File contains API's for managing the Character Device
 *              that exists b/w ISS & MUX Module
 ***************************************************************************/
#ifndef _MUXDEV_C_
#define _MUXDEV_C_

#include "muxcmn.h"

/************************** PRIVATE DECLARATIONS ******************************/

PRIVATE INT4 MuxDevOpen PROTO ((struct inode * pINode, struct file * pFile));
PRIVATE INT4 MuxDevRelease PROTO ((struct inode * pINode, struct file * pFile));
PRIVATE INT4 MuxDevIoctl PROTO ((struct inode * pINode, struct file * pFile,
                                 UINT4 u4IoctlNum, FS_ULONG u4IoctlParam));
PRIVATE SSIZE_T MuxDevRead PROTO ((struct file * pFile, CHR1 * pu1Buf,
                                   SIZE_T BufLen, OFFST_T * pPos));
PRIVATE SSIZE_T MuxDevWrite PROTO ((struct file * pFile, CONST CHR1 * pBuf,
                                    SIZE_T BufLen, OFFST_T * pPos));

static struct file_operations FileOps = {
  owner:THIS_MODULE,
  read:MuxDevRead,
  write:MuxDevWrite,
  ioctl:MuxDevIoctl,
  open:MuxDevOpen,
  release:MuxDevRelease,
};

/********************** END OF PRIVATE DECLARATIONS **************************/

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxDevOpen  
 *                                                                          
 *    DESCRIPTION      : Function to Open the Char device. 
 *
 *    INPUT            : pINode - Pointer to the INode Structure 
 *                       pFile  - Pointer to the File Structure 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : 0 for Success and -1 for failure 
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
MuxDevOpen (struct inode *pINode, struct file *pFile)
{
    UNUSED_PARAM (pINode);

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    pFile->private_data = (void *) gpMuxDevInfo;

    KERN_INC_MOD_USAGE_COUNT ();

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return 0;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxDevRelease
 *                                                                          
 *    DESCRIPTION      : Function to Release (close) the Char device. 
 *
 *    INPUT            : pINode - Pointer to the INode Structure 
 *                       pFile  - Pointer to the File Structure 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : 0 for Success and -1 for failure 
 *                                                                          
 ****************************************************************************/

PRIVATE INT4
MuxDevRelease (struct inode *pINode, struct file *pFile)
{
    UNUSED_PARAM (pINode);

    UNUSED_PARAM (pFile);

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    KERN_DEC_MOD_USAGE_COUNT ();

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return 0;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxDevIoctl 
 *                                                                          
 *    DESCRIPTION      : I/O Control Function for the MUX<->ISS char device 
 *
 *    INPUT            : pINode       - Pointer to the INode Structure 
 *                       pFile        - Pointer to the File Structure 
 *                       u4IoctlNum   - Ioctl Number              
 *                       u4IoctlParam - Arguments for the IOCTL    
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : 0 for Success and Error Codes on failure 
 *                                                                          
 ****************************************************************************/

PRIVATE INT4
MuxDevIoctl (struct inode *pINode, struct file *pFile, UINT4 u4IoctlNum,
             FS_ULONG u4IoctlParam)
{
    INT4                i4RetVal = 0;

    UNUSED_PARAM (pINode);
    UNUSED_PARAM (pFile);

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    switch (u4IoctlNum)
    {
        case MUX_NP_IOCTL:
            i4RetVal = MuxDevIoctlHandler (u4IoctlParam);
            break;

        case MUX_PROT_REG_IOCTL:
            i4RetVal = MuxRegistrationHdlr (u4IoctlParam);
            break;

#ifdef VRRP_WANTED
        case VRRP_ASSOCIP_IOCTL:
            i4RetVal = MuxVrrpIoctlHandler (u4IoctlParam);
            break;
#endif

        case IPAUTH_NMH_IOCTL:
        case IPAUTH_PORT_IOCTL:
            /* Feature not present */
            break;

        default:
            i4RetVal = -EOPNOTSUPP;
            MUX_TRC (ALL_FAILURE_TRC, "MuxDevIoctl : Invalid Ioctl Request \n");
            break;

    }

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return i4RetVal;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxDevRead
 *                                                                          
 *    DESCRIPTION      : API to read from this Char Device from ISS
 *
 *    INPUT            : pFile        - Pointer to the File Structure 
 *                       BufLen   - Sice of User Space Buffer  
 *                       pPos          - postion to start the reading
 *                                                                          
 *    OUTPUT           : pu1Buf       - Buffer to store
 *                                                                          
 *    RETURNS          : Length of the Data Read or Appropiate Error Code
 *                                                                          
 ****************************************************************************/
PRIVATE             SSIZE_T
MuxDevRead (struct file * pFile, CHR1 * pu1Buf, SIZE_T BufLen, OFFST_T * pPos)
{
    tSkbHead           *pBuffHead = NULL;
    tSkb               *pSkb = NULL;
    tMuxDevInfo        *pMuxDevInfo = NULL;

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    pMuxDevInfo = (tMuxDevInfo *) pFile->private_data;

    if (BufLen == 0)
    {
        MUX_TRC (ALL_FAILURE_TRC,
                 "MuxDevRead : Invalid Buffer Length Specified \n");
        return 0;
    }

    /* Acquire the lock */
    KERN_TAKE_SEM (&pMuxDevInfo->ChrDevSem);

    pBuffHead = &(pMuxDevInfo->ChrDevBufHead);

    /* Check for Data  Availability */
    if (pBuffHead->qlen == 0)
    {

        MUX_TRC (MUX_INFO_TRC, "MuxDevRead : No Data Available \n");

        if (pFile->f_flags & O_NONBLOCK)
        {
            KERN_GIVE_SEM (&pMuxDevInfo->ChrDevSem);
            MUX_TRC (MUX_INFO_TRC, "MuxDevRead : NONBLOCK Mode Quitting...\n");
            return -EFAULT;
        }

        /* Release the Lock */
        KERN_GIVE_SEM (&pMuxDevInfo->ChrDevSem);

        /* suspend the process */
        KERN_SLEEP_CUR_PRCS (pMuxDevInfo->ChrDevWQ, pBuffHead->qlen);

        /* when woken up check for signals */
        if (KERN_CHECK_FOR_SIGNAL (current))
        {
            return -ERESTART;
        }

        /* Acquire the lock */
        KERN_TAKE_SEM (&pMuxDevInfo->ChrDevSem);

    }

    MUX_TRC (MUX_INFO_TRC, "MuxDevRead : Data Available \n");

    pSkb = KERN_DEQUEUE_BUF (pBuffHead);

    if (NULL == pSkb)
    {
        /* Release the Lock */
        KERN_GIVE_SEM (&pMuxDevInfo->ChrDevSem);
        MUX_TRC (ALL_FAILURE_TRC, "MuxDevRead : Invalid Buffer in Queue\n");
        return -EFAULT;
    }

    if (pSkb->len <= *pPos)
    {

        KERN_FREE_SKB (pSkb);
        /* Release the Lock */
        KERN_GIVE_SEM (&pMuxDevInfo->ChrDevSem);
        MUX_TRC (ALL_FAILURE_TRC, "MuxDevRead : Invalid Offset \n");
        return -EFAULT;
    }

    /* check if requested len is greater than available len */
    if (BufLen > (pSkb->len - *pPos))
    {
        BufLen = pSkb->len - *pPos;

    }
    else
    {
        /* Release the Lock */
        KERN_FREE_SKB (pSkb);
        KERN_GIVE_SEM (&pMuxDevInfo->ChrDevSem);
        MUX_TRC (ALL_FAILURE_TRC,
                 "MuxDevRead : Invalid Length Specified for read () operation \n");
        return -EFAULT;
    }

    /* copy the data to the user */
    if (KERN_COPY_TO_USER (pu1Buf, pSkb->data + *pPos, BufLen))
    {
        /* Release the Lock */
        KERN_FREE_SKB (pSkb);
        KERN_GIVE_SEM (&pMuxDevInfo->ChrDevSem);
        MUX_TRC (ALL_FAILURE_TRC, "MuxDevRead : Failed to copy"
                 " the data to User Space \n");
        return -EFAULT;
    }

    KERN_FREE_SKB (pSkb);

    KERN_GIVE_SEM (&pMuxDevInfo->ChrDevSem);    /* Release the Lock */

    MUX_TRC (MUX_INFO_TRC, "MuxDevRead : Read Success \n");

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return BufLen;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxDevWrite
 *                                                                          
 *    DESCRIPTION      : API to write into this Char Device from ISS
 *
 *    INPUT            : pFile        - Pointer to the File Structure 
 *                       pu1Buf       - Buffer to store
 *                       BufLen       - Size of User Space Buffer  
 *                       pPos         - Postion to start the reading
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : Length of the Data writen or Appropiate Error Code
 *                                                                          
 ****************************************************************************/
PRIVATE             SSIZE_T
MuxDevWrite (struct file * pFile, CONST CHR1 * pBuf, SIZE_T BufLen,
             OFFST_T * pPos)
{
    tSkbHead           *pBuffHead = NULL;
    tSkb               *pSkb = NULL;
    tMuxDevInfo        *pMuxDevInfo = NULL;

    UNUSED_PARAM (pPos);
    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    pMuxDevInfo = (tMuxDevInfo *) pFile->private_data;

    if (BufLen == 0)
    {
        MUX_TRC (ALL_FAILURE_TRC,
                 "MuxDevWrite : Invalid Buffer Length Specified \n");
        return 0;
    }

    MUX_TRC (MUX_INFO_TRC, "MuxDevWrite : Trying to acquire the lock\n");

    if (KERN_WAIT_FOR_SEM (&pMuxDevInfo->ChrDevSem))
    {
        if (KERN_CHECK_FOR_SIGNAL (current))
        {
            return -ERESTARTSYS;
        }

        return -EFAULT;
    }

    MUX_TRC (MUX_INFO_TRC, "MuxDevWrite : Acquired the lock\n");

    pBuffHead = &(pMuxDevInfo->ChrDevBufHead);

    if (pBuffHead->qlen >= pMuxDevInfo->u4Limit)
    {
        KERN_GIVE_SEM (&pMuxDevInfo->ChrDevSem);
        MUX_TRC (ALL_FAILURE_TRC, "MuxDevWrite : Dropped the Msg. "
                 "No queue len Available \n");
        return -EFAULT;
    }

    pSkb = KERN_ALLOC_SKB (BufLen, GFP_KERNEL);

    if (NULL == pSkb)
    {
        /* Release the Lock */
        KERN_GIVE_SEM (&pMuxDevInfo->ChrDevSem);
        MUX_TRC (OS_RESOURCE_TRC, "MuxDevWrite : Buffer Allocation Failed \n");
        return -EFAULT;
    }

    if (KERN_COPY_FROM_USER (skb_put (pSkb, BufLen), pBuf, BufLen))
    {
        /* Release the Lock */
        KERN_GIVE_SEM (&pMuxDevInfo->ChrDevSem);
        KERN_FREE_SKB (pSkb);
        MUX_TRC (ALL_FAILURE_TRC, "MuxDevWrite : Failed to copy data"
                 " from User Space\n");
        return -EFAULT;
    }

    KERN_ENQUEUE_BUF (pBuffHead, pSkb);

    KERN_GIVE_SEM (&pMuxDevInfo->ChrDevSem);

    MUX_TRC (MUX_INFO_TRC, "MuxDevWrite : Released the lock\n");

    KERN_WAKE_UP_PRCS (&pMuxDevInfo->ChrDevWQ);

    MUX_TRC_ARG1 (MUX_INFO_TRC,
                  "MuxDevWrite : Written %d bytes of data \n", BufLen);

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return BufLen;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxDeviceRegister
 *                                                                          
 *    DESCRIPTION      : Function to Register MUX<->ISS Char Device with Linux
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_FAILURE if Registration Failed else OSIX_SUCCESS
 *                                                                          
 ****************************************************************************/
INT4
MuxDeviceRegister (VOID)
{
    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    /* Register the MUX<->ISS Character Device and the initialize the associated
     * Queue to read/write */

    if (KERN_REG_CHRDEV (MUX_MAJOR_NUMBER, MUX_DEVICE_FILE_NAME, &FileOps) < 0)
    {
        MUX_TRC (ALL_FAILURE_TRC, " MuxDeviceRegister : Registration Failed");
        return OSIX_FAILURE;
    }

    gpMuxDevInfo = MEM_MALLOC (sizeof (tMuxDevInfo), tMuxDevInfo);

    if (NULL == gpMuxDevInfo)
    {
        KERN_DEREG_CHRDEV (MUX_MAJOR_NUMBER, MUX_DEVICE_FILE_NAME);
        MUX_TRC (OS_RESOURCE_TRC, " MuxDeviceRegister : Memory Allocation "
                 "Failed");
        return OSIX_FAILURE;
    }

    /* Intialize the Wait Queue */

    KERN_INIT_WAIT_QUEUE (gpMuxDevInfo);

    gpMuxDevInfo->u4Limit = MUX_CHAR_DEV_Q_SIZE;

    MUX_TRC (MUX_INFO_TRC, " MuxDeviceRegister : MUX <->ISS Character "
             "Device Registration : Success");

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxDeviceDeRegister
 *                                                                          
 *    DESCRIPTION      : Function to DeRegister MUX<->ISS Char Device from Linux
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MuxDeviceDeRegister (VOID)
{
    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    /* DeRegister the MUX<->ISS Char Device and release the memory allocated
     * for it */
    KERN_DEREG_CHRDEV (MUX_MAJOR_NUMBER, MUX_DEVICE_FILE_NAME);

    MEM_FREE (gpMuxDevInfo);

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxDevIoctlHandler
 *                                                                          
 *    DESCRIPTION      : Function for handling ioctl () requests from the
 *                       ISS
 *
 *    INPUT            : u4IoctlParam - Paramater's passed from User Space
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : IOCTL_SUCCESS if Success or else Error Code
 *                                                                          
 ****************************************************************************/
INT4
MuxDevIoctlHandler (FS_ULONG u4IoctlParam)
{
    INT4                i4Command = 0;
    INT4                i4RetVal = 0;
    tKernCmdParam       MuxCmdParams;
    UINT4               u4PaddingBytes = 0;

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    MEMSET (&MuxCmdParams, 0, sizeof (tKernCmdParam));

    if (KERN_COPY_FROM_USER (&i4Command, (INT4 *) u4IoctlParam, sizeof (INT4)))
    {
        MUX_TRC (ALL_FAILURE_TRC,
                 "MuxDevIoctlHandler : Failed to get the Command No. \n");
        return (-EFAULT);
    }

    switch (i4Command)
    {
        case MUX_DEV_WRITE:
        {
            tSkb               *pSkb = NULL;

            if (KERN_COPY_FROM_USER (&MuxCmdParams.MuxDevWriteParams,
                                     (tMuxDevWriteParams *) u4IoctlParam,
                                     sizeof (tMuxDevWriteParams)))
            {
                MUX_TRC (ALL_FAILURE_TRC, "MuxDevIoctlHandler : "
                         "Failed to get the Write Params \n");
                return (-EFAULT);
            }

            /* Ensure that that the packets are properly padded if the incoming
             * packets are leass than 60 bytes */
            if (MuxCmdParams.MuxDevWriteParams.u4PktSize <
                CFA_ENET_MIN_UNTAGGED_FRAME_SIZE)
            {
                u4PaddingBytes = CFA_ENET_MIN_UNTAGGED_FRAME_SIZE -
                    MuxCmdParams.MuxDevWriteParams.u4PktSize;
            }

            /* Allocate SKB and send it to the TX handler */
            pSkb = KERN_ALLOC_SKB (MuxCmdParams.MuxDevWriteParams.u4PktSize +
                                   MUX_SW_DRV_HDR_SIZE + u4PaddingBytes,
                                   GFP_ATOMIC);

            if (pSkb == NULL)
            {
                MUX_TRC (OS_RESOURCE_TRC, "MuxDevIoctlHandler : "
                         "SKB Allocation Failed while doing Packet TX \n");
                return (-EFAULT);
            }

            skb_reserve (pSkb, MUX_SW_DRV_HDR_SIZE);

            MEMCPY (skb_put (pSkb, MuxCmdParams.MuxDevWriteParams.u4PktSize),
                    MuxCmdParams.MuxDevWriteParams.pu1DataBuf,
                    MuxCmdParams.MuxDevWriteParams.u4PktSize);

            skb_put (pSkb, u4PaddingBytes);

            /* Call the TX Handler routine & send the packet from CPU */
            MuxCmdParams.MuxDevWriteParams.i4RetVal =
                MuxTxCPUPacket (pSkb, MuxCmdParams.MuxDevWriteParams.u2VlanId,
                                MuxCmdParams.MuxDevWriteParams.u4IfIndex);

            /* Copy NPAPI return value(s) back to user */
            if (KERN_COPY_TO_USER ((tMuxDevWriteParams *) u4IoctlParam,
                                   &MuxCmdParams.MuxDevWriteParams,
                                   sizeof (tMuxDevWriteParams)))
            {
                MUX_TRC (ALL_FAILURE_TRC, "MuxDevIoctlHandler : "
                         "Failed to copy back the return values \n");
                return (-EFAULT);
            }
            else
            {
                i4RetVal = IOCTL_SUCCESS;
            }
        }
            break;

        default:
            MUX_TRC (ALL_FAILURE_TRC,
                     "MuxDevIoctlHandler : Invalid Ioctl Request \n");
            break;
    }

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return i4RetVal;
}
#endif /* _MUXDEV_C_ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  muxdev.c                       */
/*-----------------------------------------------------------------------*/
