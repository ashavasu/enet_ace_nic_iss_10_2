/******************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: muxport.c,v 1.1 2010/09/29 12:40:20 prabuc Exp $
 *
 * Description: This file contains the Portable API's and Private functions
 *              specific to handle packet Tx/Rx to and from the underlying
 *              Switch Driver
 *
 ******************************************************************************/
#include "muxcmn.h"
#include "iss.h"
#include "netdev.h"

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxPortXmitCPUPacket
 *                                                                          
 *    DESCRIPTION      : This is a Portable API called from MuxTxCPUPacket
 *                       when ISS tries to send a packet out
 *
 *    INPUT            : pSkb      - Pointer to the Socket Buffer
 *                       VlanId    - Outgoing VLAN Identifier
 *                       u4OutPort - Outgoing Port
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : No. of bytes posted Successfully or appropriate
 *                       Error Code on failure cases
 *                                                                          
 ****************************************************************************/
INT4
MuxPortXmitCPUPacket (tSkb * pSkb, tVlanId VlanId, UINT4 u4OutPort)
{
   /* Called for transmitting L2 packets out from ISS */
   UNUSED_PARAM (pSkb);
   UNUSED_PARAM (VlanId);
   UNUSED_PARAM (u4OutPort);
   return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxPortXmitDevPacket
 *                                                                          
 *    DESCRIPTION      : This is a Portable API called from MuxTxDevPacket 
 *                       for transmitting a packet from the Logical Net Device 
 *
 *    INPUT            : pSkb   - Pointer to the Socket Buffer
 *                       pDev   - Pointer to the Net Device Structure over 
 *                                which packet is to be TX'd
 *
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : Returns OSIX_SUCCESS on successful packet transmission
 *                       or OSIX_FAILURE in case failure scenarios
 *                                                                          
 ****************************************************************************/
INT4
MuxPortXmitDevPacket (tSkb * pSkb, tNetDevice * pDev)
{
   /* Called for transmitting L3 packets out from ISS via NetDev Module */
   UNUSED_PARAM (pSkb);
   UNUSED_PARAM (pDev);
   return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxPortPrcsRxFrame
 *                                                                          
 *    DESCRIPTION      : This portable API receives the packet from the Bridge
 *                       hook and processes it
 *                       extract the VLAN & Port info from it and post the 
 *                       packet to MUX Rx routine 
 *                       (MuxRxProcessIncomingFrame ())
 *
 *    INPUT            : pSkb       - SKB Pointer
 *                       u2SrcPort  - Incoming Port
 *                       VlanId     - Vlan ID                                                  
 *                       
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS If the packet gets processed successfully,
 *                       otherwise OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MuxPortPrcsRxFrame (tSkb * pSkb, UINT2 u2SrcPort, tVlanId VlanId)
{
    /* Process the incoming buffer and pass the packet buffer to MUX Reception
     * routine */
    return (MuxRxProcessIncomingFrame (pSkb, u2SrcPort, VlanId));
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxCustInit
 *                                                                          
 *    DESCRIPTION      : Does Custom Init'n specific to MRVLLS Platform
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MuxCustInit (VOID)
{
    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    /* Register Network Filter Hooks to recieve incoming packets from the Linux
     * Networking Stack  */

    MuxRxNetFilterInit ();

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxCustDeInit
 *                                                                          
 *    DESCRIPTION      : Revert Custom changes done for this Platform
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MuxCustDeInit (VOID)
{
    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    /* DeRegister Network Filter Hooks */
    MuxRxNetFilterDeInit ();

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  muxport.c                      */
/*-----------------------------------------------------------------------*/
