/****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: muxvrrp.c,v 1.2 2010/08/10 13:27:21 prabuc Exp $
 *
 * Description: This File contains API's for VRRP Database Management and
 *              update the Incoming and Outgoing ARP packets by making
 *              use of this DB
 ***************************************************************************/
#ifndef _MUX_VRRP_C_
#define _MUX_VRRP_C_

#include "muxcmn.h"
#include "arp.h"
#include <net/arp.h>

/************************** PRIVATE DECLARATIONS ******************************/

PRIVATE INT4 MuxVrrpCreateInterface PROTO ((UINT2 u2IpPort,
                                            UINT4 u4MasterAddress,
                                            UINT1 *pu1MacAddress));
PRIVATE INT4 MuxVrrpDeleteInterface PROTO ((UINT2 u2IpPort,
                                            UINT4 u4MasterAddress,
                                            UINT1 *pu1MacAddress));
PRIVATE INT4 MuxVrrpAddAssocIpAddr PROTO ((UINT2 u2IpPort,
                                           UINT4 u4AsscoIpAddress,
                                           UINT1 *pu1MacAddress));
PRIVATE INT4 MuxVrrpDelAssocIpAddr PROTO ((UINT2 u2IpPort,
                                           UINT4 u4AsscoIpAddress,
                                           UINT1 *pu1MacAddress));
PRIVATE VOID MuxVrrpFreeHashNode PROTO ((tTMO_HASH_NODE * pAssocEntry));
PRIVATE INT4 MuxVrrpComputeHashIndex PROTO ((UINT4 u4IpAddress));

/********************** END OF PRIVATE DECLARATIONS **************************/

/*****************************************************************************/
/*    Function Name       : MuxVrrpIoctlHandler                              */
/*                                                                           */
/*    Description         : This function handles the ioctl () requests from */
/*                          the User Land and updates the VRRP Kernel        */
/*                          database                                         */
/*                                                                           */
/*    Input(s)            : u4IoctlParam - Arguments for the IOCTL           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS / OSIX_FAILURE                      */
/*****************************************************************************/
INT4
MuxVrrpIoctlHandler (FS_ULONG u4IoctlParam)
{
    tVrrpAssocIpParams  VrrpIpParams;
    UINT2               u2IpPort;
    UINT4               u4IpAddress;
    UINT1              *pu1MacAddress;
    INT4                i4RetVal = OSIX_SUCCESS;

    if (KERN_COPY_FROM_USER (&VrrpIpParams, (tVrrpAssocIpParams *) u4IoctlParam,
                             sizeof (tVrrpAssocIpParams)))
    {
        MUX_TRC (ALL_FAILURE_TRC,
                 "MuxVrrpIoctlHandler : Failed to get VRRP Parameters \n");
        return (-EFAULT);
    }

    u2IpPort = VrrpIpParams.u2IpPort;
    u4IpAddress = VrrpIpParams.u4IpAddr;
    pu1MacAddress = VrrpIpParams.pu1MacAddr;

    switch (VrrpIpParams.u4Action)
    {
        case VRRP_KERN_IF_CREATE:
            i4RetVal = MuxVrrpCreateInterface (u2IpPort, u4IpAddress,
                                               pu1MacAddress);
            break;

        case VRRP_KERN_IF_DELETE:
            i4RetVal = MuxVrrpDeleteInterface (u2IpPort, u4IpAddress,
                                               pu1MacAddress);
            break;

        case VRRP_KERN_ADD:
            i4RetVal = MuxVrrpAddAssocIpAddr (u2IpPort, u4IpAddress,
                                              pu1MacAddress);
            break;
        case VRRP_KERN_DEL:
            i4RetVal = MuxVrrpDelAssocIpAddr (u2IpPort, u4IpAddress,
                                              pu1MacAddress);
            break;
    }
    return i4RetVal;
}

/*****************************************************************************/
/*    Function Name       : MuxVrrpInitIntfTable                             */
/*                                                                           */
/*    Description         : This function initializes the VRRP List for the  */
/*                          specified L3 Interface                           */
/*                                                                           */
/*    Input(s)            : u4IfIndex    - L3 Interface Index                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*****************************************************************************/
VOID
MuxVrrpInitIntfTable (UINT4 u4IfIndex)
{
    TMO_SLL_Init (&(gaIfTable[u4IfIndex].VrrpList));
    return;
}

/*****************************************************************************/
/*    Function Name       : MuxVrrpCreateInterface                           */
/*                                                                           */
/*    Description         : This function Adds a new entry to kernel vrrp    */
/*                          database when an interface becomes master        */
/*                                                                           */
/*    Input(s)            : u2IpPort - IP Port Number                        */
/*                          u4MasterAddress - Virtual IP address             */
/*                          pu1MacAddress - virtual mac address              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS / OSIX_FAILURE                      */
/*****************************************************************************/
INT4
MuxVrrpCreateInterface (UINT2 u2IpPort, UINT4 u4MasterAddress,
                        UINT1 *pu1MacAddress)
{
    tVrrpEntryNode     *pVrrpEntry = NULL;
    tVrrpAssocNode     *pAssocEntry = NULL;
    UINT4               u4RetVal;

    /*Allocate memory for new VRRP entry */
    u4RetVal =
        MemAllocateMemBlock (MUX_VRRP_TBL_POOLID, (UINT1 **) &pVrrpEntry);
    if (u4RetVal == MEM_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    /*Create a hash table to store Associated IP for this entry */
    pVrrpEntry->pIpTable = TMO_HASH_Create_Table (MUX_VRRP_HASH_TBL_SIZE,
                                                  NULL, FALSE);
    if (pVrrpEntry->pIpTable == NULL)
    {
        MemReleaseMemBlock (MUX_VRRP_TBL_POOLID, (UINT1 *) pVrrpEntry);
        return (OSIX_FAILURE);
    }

    /*Allocate memory for the first associated IP i.e. master IP address */
    u4RetVal =
        MemAllocateMemBlock (MUX_VRRP_ASSOC_TBL_POOLID,
                             (UINT1 **) &pAssocEntry);
    if (u4RetVal == MEM_FAILURE)
    {
        TMO_HASH_Delete_Table (pVrrpEntry->pIpTable, MuxVrrpFreeHashNode);
        MemReleaseMemBlock (MUX_VRRP_TBL_POOLID, (UINT1 *) pVrrpEntry);
        return (OSIX_FAILURE);
    }

    /*Initialise VRRP entry and associated IP node */
    TMO_SLL_Init_Node ((tTMO_SLL_NODE *) pVrrpEntry);
    TMO_HASH_Init_Node ((tTMO_HASH_NODE *) pAssocEntry);

    /*Copy the corresponding values for Mac address and associated IP */
    MEMCPY (pVrrpEntry->au1Mac, pu1MacAddress, CFA_ENET_ADDR_LEN);
    pAssocEntry->u4IpAddress = u4MasterAddress;

    /*Add the associate IP node into the hash table */
    TMO_HASH_Add_Node (pVrrpEntry->pIpTable, (tTMO_HASH_NODE *) pAssocEntry,
                       MuxVrrpComputeHashIndex (u4MasterAddress), NULL);

    /*Add the VRRP entry into singly linked list for this vlan */
    TMO_SLL_Add (&(gaIfTable[u2IpPort].VrrpList), (tTMO_SLL_NODE *) pVrrpEntry);

    return (OSIX_SUCCESS);

}

/*****************************************************************************/
/*    Function Name       : MuxVrrpDeleteInterface                           */
/*                                                                           */
/*    Description         : This function deletes an entry from kernel vrrp  */
/*                          database when an interface retires from master   */
/*                          state                                            */
/*                                                                           */
/*    Input(s)            : u2IpPort - IP Port Number                        */
/*                          u4MasterAddress - Virtual IP address             */
/*                          pu1MacAddress - virtual mac address              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS / OSIX_FAILURE                      */
/*****************************************************************************/
INT4
MuxVrrpDeleteInterface (UINT2 u2IpPort, UINT4 u4MasterAddress,
                        UINT1 *pu1MacAddress)
{
    tVrrpEntryNode     *pVrrpEntry = NULL;

    /* Get the corresponding VRRP entry by scanning the singly link list for 
     * L3 Interfaces by comparing the mac address */
    TMO_SLL_Scan (&(gaIfTable[u2IpPort].VrrpList), pVrrpEntry, tVrrpEntryNode *)
    {

        if (MEMCMP (pVrrpEntry->au1Mac, pu1MacAddress, CFA_ENET_ADDR_LEN) == 0)
        {
            break;
        }
    }

    if (pVrrpEntry != NULL)
    {
        if (pVrrpEntry->pIpTable != NULL)
        {
            /* Delete the hash table for this VRRP entry. when deleting each
             * node in the hash table, MuxVrrpFreeHashNode will be called.
             * This will release the memory for corresponding node*/
            TMO_HASH_Delete_Table (pVrrpEntry->pIpTable, MuxVrrpFreeHashNode);
        }

        /* Finally Delete the VRRP entry */
        TMO_SLL_Delete (&(gaIfTable[u2IpPort].VrrpList),
                        (tTMO_SLL_NODE *) pVrrpEntry);
        MemReleaseMemBlock (MUX_VRRP_TBL_POOLID, (UINT1 *) pVrrpEntry);
    }

    UNUSED_PARAM (u4MasterAddress);
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/*    Function Name       : MuxVrrpAddAssocIpAddr                            */
/*                                                                           */
/*    Description         : This function adds an IP address entry into      */
/*                          kernel vrrp database when that IP is added into  */
/*                          associated IP list                               */
/*                                                                           */
/*    Input(s)            : u2IpPort - IP Port Number                        */
/*                          u4MasterAddress - Virtual IP address             */
/*                          pu1MacAddress - virtual mac address              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS / OSIX_FAILURE                      */
/*****************************************************************************/
INT4
MuxVrrpAddAssocIpAddr (UINT2 u2IpPort, UINT4 u4AsscoIpAddress,
                       UINT1 *pu1MacAddress)
{
    tVrrpEntryNode     *pVrrpEntry = NULL;
    tVrrpAssocNode     *pAssocEntry = NULL;
    UINT4               u4RetVal;

    /* Get the corresponding VRRP entry by scanning the singly link list for 
     * L3 Interfaces  by comparing the mac address */
    TMO_SLL_Scan (&(gaIfTable[u2IpPort].VrrpList), pVrrpEntry, tVrrpEntryNode *)
    {
        if (MEMCMP (pVrrpEntry->au1Mac, pu1MacAddress, CFA_ENET_ADDR_LEN) == 0)
        {
            break;
        }
    }

    /* if VRRP table or hash table is unavailable, return failure */
    if ((pVrrpEntry == NULL) || (pVrrpEntry->pIpTable == NULL))
    {
        return (OSIX_FAILURE);
    }

    /* Allocate memory for associated IP */
    u4RetVal =
        MemAllocateMemBlock (MUX_VRRP_ASSOC_TBL_POOLID,
                             (UINT1 **) &pAssocEntry);
    if (u4RetVal == MEM_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    /* Initiate the node */
    TMO_HASH_Init_Node ((tTMO_HASH_NODE *) pAssocEntry);

    /* Add the node into the hash table */
    pAssocEntry->u4IpAddress = u4AsscoIpAddress;
    TMO_HASH_Add_Node (pVrrpEntry->pIpTable, (tTMO_HASH_NODE *) pAssocEntry,
                       MuxVrrpComputeHashIndex (u4AsscoIpAddress), NULL);

    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/*    Function Name       : MuxVrrpDelAssocIpAddr                            */
/*                                                                           */
/*    Description         : This function deletes an IP address entry from   */
/*                          kernel vrrp database when that IP is deleted from*/
/*                          associated IP list                               */
/*                                                                           */
/*    Input(s)            : u2IpPort - IP Port Number                        */
/*                          u4MasterAddress - Virtual IP address             */
/*                          pu1MacAddress - virtual mac address              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS / OSIX_FAILURE                      */
/*****************************************************************************/
INT4
MuxVrrpDelAssocIpAddr (UINT2 u2IpPort, UINT4 u4AsscoIpAddress,
                       UINT1 *pu1MacAddress)
{
    tVrrpEntryNode     *pVrrpEntry = NULL;
    tVrrpAssocNode     *pAssocEntry = NULL;

    /* Get the corresponding VRRP entry by scanning the singly link list for 
     * L3 Interfaces  by comparing the mac address*/
    TMO_SLL_Scan (&(gaIfTable[u2IpPort].VrrpList), pVrrpEntry, tVrrpEntryNode *)
    {
        if (MEMCMP (pVrrpEntry->au1Mac, pu1MacAddress, CFA_ENET_ADDR_LEN) == 0)
        {
            break;
        }
    }

    /* if VRRP table or hash table is unavailable, return failure */
    if ((pVrrpEntry == NULL) || (pVrrpEntry->pIpTable == NULL))
    {
        return (OSIX_FAILURE);
    }

    /* Scan the hash bucket where for the IP address entry */
    TMO_HASH_Scan_Bucket (pVrrpEntry->pIpTable,
                          MuxVrrpComputeHashIndex (u4AsscoIpAddress),
                          pAssocEntry, tVrrpAssocNode *)
    {
        if (pAssocEntry->u4IpAddress == u4AsscoIpAddress)
        {
            /* Delete the ip address enty from bucket */
            TMO_HASH_Delete_Node (pVrrpEntry->pIpTable,
                                  (tTMO_HASH_NODE *) pAssocEntry,
                                  MuxVrrpComputeHashIndex (u4AsscoIpAddress));
            /* Release the memory for IP entry */
            MemReleaseMemBlock (MUX_VRRP_ASSOC_TBL_POOLID,
                                (UINT1 *) pAssocEntry);
            return (OSIX_SUCCESS);
        }
    }

    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/*    Function Name       : MuxVrrpHandleL3IfDelete                          */
/*                                                                           */
/*    Description         : This function deletes all entries from kernel    */
/*                          vrrp database when an L3 interface is deleted    */
/*                                                                           */
/*    Input(s)            : u2IpPort - IP Port Number                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS / OSIX_FAILURE                      */
/*****************************************************************************/
INT4
MuxVrrpHandleL3IfDelete (UINT2 u2IpPort)
{
    tVrrpEntryNode     *pVrrpEntry = NULL;

    while ((pVrrpEntry =
            (tVrrpEntryNode *) TMO_SLL_First (&(gaIfTable[u2IpPort].VrrpList)))
           != NULL)
    {
        if (pVrrpEntry->pIpTable != NULL)
        {
            TMO_HASH_Delete_Table (pVrrpEntry->pIpTable, MuxVrrpFreeHashNode);
        }
        TMO_SLL_Delete (&(gaIfTable[u2IpPort].VrrpList),
                        (tTMO_SLL_NODE *) pVrrpEntry);
        MemReleaseMemBlock (MUX_VRRP_TBL_POOLID, (UINT1 *) pVrrpEntry);

    }
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/*    Function Name       : MuxVrrpPrcsIncomingPkt                           */
/*                                                                           */
/*    Description         : 1. This function sets the packet type as         */
/*                             PACKET_HOST, if the destination mac is one of */
/*                             the VRRP mac on that interface                */
/*                          2. Sends an ARP REPLY to incoming ARP REQUEST for*/
/*                             a VRRP ip address, if it is not an interface  */
/*                             ip address                                    */
/*                                                                           */
/*    Input(s)            : pSkb   - Incoming packet buffer                  */
/*                          pIntf  - Pointer to the L3 device info           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS / OSIX_FAILURE                      */
/*****************************************************************************/

INT4
MuxVrrpPrcsIncomingPkt (tSkb * pSkb, tNetDevice * pIntf)
{
    tVrrpEntryNode     *pVrrpEntry = NULL;
    tVrrpAssocNode     *pAssocEntry = NULL;
    UINT1              *pArpPtr = NULL;
    INT4                i4MacFlag = TRUE;
    INT4                i4ArpFlag = FALSE;
    UINT2               u2IpPort = pIntf->ifindex;
    UINT4               u4TargetIp = 0;
    UINT4               u4SenderIp = 0;
    UINT4               u4IntfIp = 0;
    struct ethhdr      *pEthHdr = NULL;
    struct in_device   *pIpInfo = NULL;
    struct arphdr      *pArpHdr = NULL;

    if (TMO_SLL_Count (&(gaIfTable[u2IpPort].VrrpList)) <= 0)
    {
        /* There is no vrrp entry in this table */
        return (OSIX_SUCCESS);
    }

#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,0)
    pEthHdr = eth_hdr (pSkb);
#else
    pEthHdr = pSkb->mac.ethernet;
#endif

    pArpHdr = (struct arphdr *) pSkb->data;

    if ((pSkb->protocol == CFA_ENET_ARP) &&
        ((pArpHdr->ar_op) == OSIX_HTONS (ARP_REQUEST)))
    {

        i4MacFlag = FALSE;        /* Destination mac address in Eth header will 
                                   be Broadcast MAC. It will not be VRRP mac */

        pIpInfo = pIntf->ip_ptr;

        /*Get the interface ip address of this device */
        if ((pIpInfo != NULL) && (pIpInfo->ifa_list != NULL))
        {
            u4IntfIp = (pIpInfo->ifa_list)->ifa_address;
        }

        /* Get the pointer beyond arp header. 
           i.e. Get the pointer to sender hw address */
        pArpPtr = (UINT1 *) (pArpHdr + 1);

        /* To get sender ip address, Advance the pointer beyond sender 
         * hw address*/
        pArpPtr += pArpHdr->ar_hln;

        MEMCPY (&u4SenderIp, pArpPtr, pArpHdr->ar_pln);

        u4SenderIp = OSIX_NTOHL (u4SenderIp);

        /* To get target Ip address, advance the pointer beyond sender i
         * ip address and Target hw address*/
        pArpPtr += pArpHdr->ar_pln;    /*ip address size */
        pArpPtr += pArpHdr->ar_hln;    /*hw address size */

        MEMCPY (&u4TargetIp, pArpPtr, pArpHdr->ar_pln);

        u4TargetIp = OSIX_NTOHL (u4TargetIp);

        /* Enable the flag for sending arp reply for this arp request */
        if (u4IntfIp != u4TargetIp)
        {
            i4ArpFlag = TRUE;    /* else linux Ip will reply to this arp request */
        }
    }

    /* Scan the singly linked list for VRRP entry */
    TMO_SLL_Scan (&(gaIfTable[u2IpPort].VrrpList), pVrrpEntry, tVrrpEntryNode *)
    {
        if ((i4MacFlag == TRUE) &&
            (MEMCMP (pVrrpEntry->au1Mac, pEthHdr->h_dest,
                     CFA_ENET_ADDR_LEN) == 0))
        {
            pSkb->pkt_type = PACKET_HOST;
            i4MacFlag = FALSE;
        }

        if (i4ArpFlag == TRUE)
        {
            /* Scan hash bucket corresponding to VRRP entry for IP address */
            TMO_HASH_Scan_Bucket (pVrrpEntry->pIpTable,
                                  MuxVrrpComputeHashIndex (u4TargetIp),
                                  pAssocEntry, tVrrpAssocNode *)
            {
                if (pAssocEntry->u4IpAddress == u4TargetIp)
                {
                    /*   Send an arp reply with 
                     *   type      = ARP reply.
                     *   ptype     = ARP Protocol (0x806).
                     *   dest_ip   = source address of incoming ARP request.
                     *   dev       = pIntf.
                     *   src_ip    = vrrp master ip address.
                     *   dest_hw   = source mac of incoming arp request.
                     *   src_hw    = vrrp mac address.
                     *   target_hw = source mac of incoming arp request.
                     */
                    arp_send (ARPOP_REPLY, CFA_ENET_ARP, u4SenderIp, pIntf,
                              u4TargetIp, pEthHdr->h_source, pVrrpEntry->au1Mac,
                              pEthHdr->h_source);
                    i4ArpFlag = FALSE;
                    break;
                }
            }
        }

        if ((i4MacFlag == FALSE) && (i4ArpFlag == FALSE))
            break;
    }
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/*    Function Name       : MuxVrrpPrcsOutgoingPkt                           */
/*                                                                           */
/*    Description         : This function scans the outgoing ARP packet and  */
/*                          changes the senders hw address to Vrrp mac       */
/*                          if senders ip is one of the vrrp master          */
/*                          configured                                       */
/*                                                                           */
/*    Input(s)            : pSkb     - Incoming packet buffer                */
/*                          u2IpPort - IP Port Number                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS / OSIX_FAILURE                      */
/*****************************************************************************/
INT4
MuxVrrpPrcsOutgoingPkt (tSkb * pSkb, UINT2 u2IpPort)
{
    UINT1              *pArpPtr = NULL;
    UINT4               u4SenderIp = 0;
    tVrrpEntryNode     *pVrrpEntry = NULL;
    tVrrpAssocNode     *pAssocEntry = NULL;
    struct arphdr      *pArpHdr = NULL;
    struct ethhdr      *pEthHdr = NULL;

    if (TMO_SLL_Count (&(gaIfTable[u2IpPort].VrrpList)) <= 0)
    {
        /* There is no vrrp entry in this table */
        return (OSIX_SUCCESS);
    }

    /* determine which protocol is being carried */
    if (pSkb->protocol == CFA_ENET_ARP)
    {
        /* Get the ethernet header */
        pEthHdr = (struct ethhdr *) pSkb->data;

        /*Get the ARP packet header (Just after eth header!!) */
        pArpHdr = (struct arphdr *) (pEthHdr + 1);

        /* Get the pointer to Senders mac (Just after Arp header) */
        pArpPtr = (UINT1 *) (pArpHdr + 1);

        /* Finally senders ip addres is after senders mac */
        MEMCPY (&u4SenderIp, (pArpPtr + pArpHdr->ar_hln), pArpHdr->ar_pln);

        u4SenderIp = OSIX_NTOHL (u4SenderIp);

        /* Scan the singly link list for VRRP entry */
        TMO_SLL_Scan (&(gaIfTable[u2IpPort].VrrpList), pVrrpEntry,
                      tVrrpEntryNode *)
        {
            /* Scan the hash bucket for sender IP */
            TMO_HASH_Scan_Bucket (pVrrpEntry->pIpTable,
                                  MuxVrrpComputeHashIndex (u4SenderIp),
                                  pAssocEntry, tVrrpAssocNode *)
            {
                if (pAssocEntry->u4IpAddress == u4SenderIp)
                {
                    MEMCPY (pEthHdr->h_source, pVrrpEntry->au1Mac,
                            CFA_ENET_ADDR_LEN);

                    MEMCPY (pArpPtr, pVrrpEntry->au1Mac, CFA_ENET_ADDR_LEN);
                    return (OSIX_SUCCESS);
                }
            }
        }
    }
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/*    Function Name       : MuxVrrpFreeHashNode                              */
/*                                                                           */
/*    Description         : Free the meory allocated for associated IP node  */
/*                          This is a call back function for                 */
/*                          TMO_HASH_Delete_Table                            */
/*                                                                           */
/*    Input(s)            : pAssocEntry - Associated IP node                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

VOID
MuxVrrpFreeHashNode (tTMO_HASH_NODE * pAssocEntry)
{
    MemReleaseMemBlock (MUX_VRRP_ASSOC_TBL_POOLID, (UINT1 *) pAssocEntry);
    return;
}

/*****************************************************************************/
/*    Function Name       : MuxVrrpComputeHashIndex                          */
/*                                                                           */
/*    Description         : Computes the hash index for given IP address     */
/*                                                                           */
/*    Input(s)            : u4IpAddress  - IP address                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : u2Hashval - Hash index                           */
/*****************************************************************************/
INT4
MuxVrrpComputeHashIndex (UINT4 u4IpAddress)
{
    UINT2               u2Hashval = 0;

    u2Hashval = HIWORD (u4IpAddress);
    u2Hashval ^= LOWORD (u4IpAddress);
    u2Hashval %= MUX_VRRP_HASH_TBL_SIZE;

    return u2Hashval;
}

EXPORT_SYMBOL (gaIfTable);
EXPORT_SYMBOL (MuxVrrpInitIntfTable);

#endif /* _MUX_VRRP_C_ */

/*-----------------------------------------------------------------------*/
/*                       End of the file muxvrrp.c                       */
/*-----------------------------------------------------------------------*/
