/******************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: muxmrvl.c,v 1.3 2010/09/25 08:29:42 prabuc Exp $
 *
 * Description: This file contains the Portable API's and Private functions
 *              specific to handle packet Tx/Rx to and from the Marvell Chip
 *
 ******************************************************************************/
#include "muxcmn.h"
#include "iss.h"
#include "netdev.h"
#include "muxmrvl.h"

/************************** PRIVATE DECLARATIONS ******************************/

PRIVATE INT4 MuxPortAddMrvllHeader PROTO ((tSkb ** ppSkb, tVlanId VlanId,
                                           UINT4 u4OutPort,
                                           BOOL1 b1PktFromISS));
/*********************** END OF PRIVATE DECLARATIONS **************************/

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxPortXmitCPUPacket
 *                                                                          
 *    DESCRIPTION      : This is a Portable API called from MuxTxCPUPacket
 *                       when ISS tries to send a packet out
 *
 *    INPUT            : pSkb      - Pointer to the Socket Buffer
 *                       VlanId    - Outgoing VLAN Identifier
 *                       u4OutPort - Outgoing Port
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : No. of bytes posted Successfully or appropriate
 *                       Error Code on failure cases
 *                                                                          
 ****************************************************************************/
INT4
MuxPortXmitCPUPacket (tSkb * pSkb, tVlanId VlanId, UINT4 u4OutPort)
{
    tNetDevice         *pOutDev = NULL;    /* Outgoing Physical Device */
    INT4                i4BytesSent = 0;
    INT4                i4RetVal = OSIX_SUCCESS;

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    /* Since the outgoing port is available, set the From_CPU DSA Tag in the 
     * Marvell Header by passing the incoming Outgoing Port info. while calling
     * MuxPortAddMrvllHeader () */

    if ((i4RetVal = MuxPortAddMrvllHeader (&pSkb, VlanId, u4OutPort, OSIX_TRUE))
        == OSIX_SUCCESS)
    {
        /* Transmit the packets to the CPU Port "eth0" which would send
         * the packets to the front panel port(s) based on the DSA Tag */
        pOutDev = KERN_DEV_GET_BY_NAME (MRVL_CPU_PORT_NAME);

        if (pOutDev == NULL)
        {
            i4RetVal = -ENODEV;
        }
        else
        {
            KERN_DEV_GIVE_SEM (pOutDev);

            pSkb->dev = pOutDev;

            /* Adjust the Socket Buffer pointer with the newly modified Data 
             * pointers for avoiding unneccessary kernel panics */

            KERN_SKB_MAC_LAYER_HDR (pSkb) = pSkb->data;
            KERN_SKB_NETWORK_HDR (pSkb) = pSkb->data;
            KERN_SKB_TRANSPORT_HDR (pSkb) = pSkb->data;

            MuxUtilDumpSKB ((UINT1 *) "BEFORE-CPU_PKT_TX", pSkb);

            /* Send the packet out via the CPU Port ("eth0") */
            if ((i4RetVal = KERN_TXMIT_PKT_TO_LNX (pSkb)) != 0)
            {
                /* Packet TX Failure */
                MUX_TRC (ALL_FAILURE_TRC, "MuxPortXmitCPUPacket : "
                         "Failed to Transmit the packet \n");
            }
        }
    }

    if (i4RetVal == OSIX_SUCCESS)
    {
        /* Send the no. of bytes successfully transmitted 
         * Subtract the Marvell Header from the total bytes tx'ed */
        i4BytesSent = pSkb->len - MRVL_HDR_SIZE;
    }
    else
    {
        KERN_FREE_SKB (pSkb);
        /* return the Error Code to the Caller */
        i4BytesSent = i4RetVal;
    }

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);
    return i4BytesSent;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxPortXmitDevPacket
 *                                                                          
 *    DESCRIPTION      : This is a Portable API called from MuxTxDevPacket 
 *                       for transmitting a packet from the Logical Net Device 
 *
 *    INPUT            : pSkb   - Pointer to the Socket Buffer
 *                       pDev   - Pointer to the Net Device Structure over 
 *                                which packet is to be TX'd
 *
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : Returns OSIX_SUCCESS on successful packet transmission
 *                       or OSIX_FAILURE in case failure scenarios
 *                                                                          
 ****************************************************************************/
INT4
MuxPortXmitDevPacket (tSkb * pSkb, tNetDevice * pDev)
{
    tNetDevPrivData    *pDevPriv = KERN_NETDEV_PRIV_DATA (pDev);
    tVlanId             VlanId;
    CHR1               *pc1Port = NULL;
    UINT2               u2PktLen = pSkb->len;
    UINT2               u2LPort = 0;

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    MuxUtilGetVlanIdFromDev (pDev, &VlanId);

    /* Router Port Case : Since the Outgoing Port can be derived for packets 
     * outgoing from Router Ports, we use "From CPU" Tag for these outgoing
     * packets [treat them as if they have are sent from ISS] */

    if (VlanId == 0)
    {
        pc1Port = &pDev->name[STRLEN (ISS_RPORT_ALIAS_PREFIX)];
        /* IF Index to LPort Mapping [taken from CFA_GET_LPORT_FROM_IFIDX  present
         *  in npcfa.h] */
        u2LPort = ATOI (pc1Port) - 1;
        if (MuxPortXmitCPUPacket (pSkb, 0, u2LPort) < 0)
        {
            pDevPriv->NetDevStats.tx_errors++;
            return OSIX_FAILURE;
        }
        else
        {
            pDevPriv->NetDevStats.tx_bytes += u2PktLen;
            pDevPriv->NetDevStats.tx_packets++;
            return OSIX_SUCCESS;
        }
    }

    /* VLAN Case : Since the outgoing port is not available, set FORWARD DSA 
     * Tag in the Marvell Header by passing Outgoing Port as 0 while calling
     * MuxPortAddMrvllHeader () */

    if (MuxPortAddMrvllHeader (&pSkb, VlanId, 0, OSIX_FALSE) == OSIX_SUCCESS)
    {
        /* Transmit the packets to the CPU Port "eth0" which would send
         * the packets to the front panel port(s) based on the DSA Tag */
        pDev = KERN_DEV_GET_BY_NAME (MRVL_CPU_PORT_NAME);

        if (pDev == NULL)
        {
            KERN_FREE_SKB (pSkb);
            pDevPriv->NetDevStats.tx_errors++;
            MUX_TRC (ALL_FAILURE_TRC,
                     "MuxPortXmitDevPacket : Failed while sending the packet"
                     " through the CPU Port \r\n");
            return OSIX_FAILURE;
        }
        else
        {
            KERN_DEV_GIVE_SEM (pDev);

            pSkb->dev = pDev;

            KERN_SKB_MAC_LAYER_HDR (pSkb) = pSkb->data;
            KERN_SKB_NETWORK_HDR (pSkb) = pSkb->data;
            KERN_SKB_TRANSPORT_HDR (pSkb) = pSkb->data;

            MuxUtilDumpSKB ((UINT1 *) "BEFORE-DEV_PKT_TX", pSkb);

            if (KERN_TXMIT_PKT_TO_LNX (pSkb) != 0)
            {
                KERN_FREE_SKB (pSkb);
                pDevPriv->NetDevStats.tx_errors++;
                MUX_TRC (ALL_FAILURE_TRC,
                         "MuxPortXmitDevPacket : Failed while transmitting the "
                         "packet through the CPU Port \r\n");
                return OSIX_FAILURE;
            }
        }
    }
    else
    {
        KERN_FREE_SKB (pSkb);
        pDevPriv->NetDevStats.tx_errors++;
        MUX_TRC (ALL_FAILURE_TRC,
                 "MuxPortXmitDevPacket : Failed while adding up Marvell "
                 "Header \r\n");
        return OSIX_FAILURE;
    }

    pDevPriv->NetDevStats.tx_bytes += u2PktLen;
    pDevPriv->NetDevStats.tx_packets++;

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxPortPrcsRxFrame
 *                                                                          
 *    DESCRIPTION      : This portable API receives the packet from the Bridge
 *                       hook and processes it by stripping off the DSA tag,
 *                       extract the VLAN & Port info from it and post the 
 *                       packet to MUX Rx routine 
 *                       (MuxRxProcessIncomingFrame ())
 *
 *    INPUT            : pSkb       - SKB Pointer (with DSA tag)
 *                       u2SrcPort  - Incoming Port
 *                       VlanId     - Vlan ID                                                  
 *                       
 *                       NOTE : u2SrcPort & VlanId are passed as "0" since 
 *                              they can be extracted from the Marvell Header
 *
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS If the packet gets processed successfully,
 *                       otherwise OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MuxPortPrcsRxFrame (tSkb * pSkb, UINT2 u2SrcPort, tVlanId VlanId)
{
    tEnetV2Header       EthHdr;
    UINT1              *pPktBuf = pSkb->data;
    UINT4               u4SrcDevice = 0;
    UINT1               u1Priority = 0;
    UINT1               u1IsTrunkFlag = OSIX_FALSE;
    UINT1               u1Tagged = OSIX_FALSE;

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    /* Reference DSA Tag Format (12 - 15 (4 bytes)) :
     *
     *   ------------------------------------------------------
     *  |   Byte(s)  |                Contents                 |
     *   ------------------------------------------------------
     *  |   0 - 11   |              DST & SRC MAC              | 
     *
     *  |     12     |  X | X | T (Tag) |    Device (5bits)    |
     *
     *  |     13     |    Port (5 bits)          | 0 | 0 | CFI |
     *
     *  |     14     |   Pri (3 bits) | 0 |    VID (4 bits)    |
     *
     *  |     15     |              VID (8 bits )              |
     *   ------------------------------------------------------
     *       Byte 14 & 15 are similar to IEEE 802.1q Tag
     *
     * */

    /* Extract the information from DSA Tag */
    u4SrcDevice = (pPktBuf[12] & 0x1F);
    u1Tagged = ((pPktBuf[12] & 0x20) ? (OSIX_TRUE) : (OSIX_FALSE));
    u2SrcPort = (pPktBuf[13] >> 3);
    u1IsTrunkFlag = ((pPktBuf[13] & 0x04) ? (OSIX_TRUE) : (OSIX_FALSE));
    u1Priority = (pPktBuf[14] >> 5);
    VlanId = ((pPktBuf[15]) | ((pPktBuf[14] & 0xF) << 8));

    /* Check whether the incoming packet is tagged/not. If untagged, remove
     * the Marvell tag. Else update the Marvell header since 802.1q &  
     * Marvel format changes only by first 2 bytes */

    if (u1Tagged == OSIX_FALSE)
    {
        /* Save Ethernet Header Info and update protocol field from the packet 
         * [16 & 17] byte just after Marvell Header and overwrite it after
         * shifting the SKB Data pointer by MRVL_HDR_SIZE */
        MEMCPY (&EthHdr, pSkb->data, CFA_ENET_V2_HEADER_SIZE);
        MEMCPY (&EthHdr.u2LenOrType,
                (pSkb->data + CFA_ENET_V2_HEADER_SIZE + MRVL_HDR_SIZE - 2), 2);
        pSkb->data += MRVL_HDR_SIZE;
        pSkb->len -= MRVL_HDR_SIZE;
        MEMCPY (pSkb->data, &EthHdr, CFA_ENET_V2_HEADER_SIZE);
    }
    else
    {
        /* Tagged Packet. Since byte 14 & 15 are same in Marvell header, Change 
         * Marvell Tag to IEEE 802.1q Tag by overwriting the byte 12 & 13 */
        pPktBuf[12] = 0x81;
        pPktBuf[13] = 0x00;
    }

    if (u2SrcPort > SYS_DEF_MAX_PHYSICAL_INTERFACES)
    {
        MUX_TRC (ALL_FAILURE_TRC,
                 "MuxPortPrcsRxFrame : Invalid Incoming Port \r\n");
        return OSIX_FAILURE;
    }

    /* Port number's present in Marvel Header start with 0 i.e 0 indicates Port 1
     * So incrementing the Port number by one */
    u2SrcPort++;

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return (MuxRxProcessIncomingFrame (pSkb, u2SrcPort, VlanId));
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxCustInit
 *                                                                          
 *    DESCRIPTION      : Does Custom Init'n specific to MRVLLS Platform
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MuxCustInit (VOID)
{
    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    /* Register Network Filter Hooks to recieve incoming packets from the Linux
     * Networking Stack  */

    MuxRxNetFilterInit ();

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxCustDeInit
 *                                                                          
 *    DESCRIPTION      : Revert Custom changes done for Marvell Platform
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MuxCustDeInit (VOID)
{
    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    /* DeRegister Network Filter Hooks */
    MuxRxNetFilterDeInit ();

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MuxPortAddMrvllHeader
 *                                                                         
 *    DESCRIPTION      : This Function inserts the Marvell Header to the input
 *                       SKB and adjusts the SKB fields properly. Marvell Header
 *                       (4 bytes) is inserted in between of Src MAC (byte 13) 
 *                       and Ethernet Protocol Information (byte 14)
 *
 *    INPUT            : ppSkb     - Reference to Socket Buffer pointer
 *                       VlanId    - Outgoing VLAN Identifier
 *                       u4OutPort - Outgoing Port
 *                       b1PktFromISS - Indicates from where this packet had 
 *                                      Originated
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS or appropriate Error Code on failure cases
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
MuxPortAddMrvllHeader (tSkb ** ppSkb, tVlanId VlanId,
                       UINT4 u4OutPort, BOOL1 b1PktFromISS)
{
    tSkb               *pSkb = *ppSkb;
    tSkb               *pNewSkb = NULL;
    UINT1               au1MrvlHdr[CFA_ENET_V2_HEADER_SIZE + MRVL_HDR_SIZE - 2];
    UINT1               u1Priority = 0;
    UINT1               b1Tagged = OSIX_FALSE;
    UINT4               u4PaddingBytes = 0;
    UINT4               u4NewHeadRoom = 0;
    BOOL1               b1NeedToPad = OSIX_FALSE;

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    MuxUtilDumpSKB ((UINT1 *) "BEFORE-HDR_ADDTN", pSkb);

    /* Reference DSA Tag Format [12 - 15 (4 bytes)] :
     *
     *   ------------------------------------------------------
     *  |   Byte(s)  |                Contents                 |
     *   ------------------------------------------------------
     *  |   0 - 11   |              DST & SRC MAC              | 
     *
     *  |     12     |  X | X | T (Tag) |    Device (5bits)    |
     *
     *  |     13     |    Port (5 bits)          | 0 | 0 | CFI |
     *
     *  |     14     |   Pri (3 bits) | 0 |    VID (4 bits)    |
     *
     *  |     15     |              VID (8 bits )              |
     *
     *  |   16 - 17  |        Ethernet Protocol Number         |
     *   ------------------------------------------------------
     *       Byte 14 & 15 are similar to IEEE 802.1q Tag
     *
     * Bit's 1 & 2 of the 1st Octect (marked X in above format) in the Marvell
     * Header (Byte - 12) has to be set as `01` and `11` for specifying From_CPU
     * and Forward DSA Tag's respectively based on the availability of outgoing 
     * port information
     *
     * NOTE : Marvell Header Format might differ from Chip to Chip
     *
     * Reference : Doc No. MV-S102629-00 Rev.C */

    /* Copy the DST & SRC MAC Info from the incoming SKB and construct Marvel 
     * Header 
     * [Ethernet Protocol (byte 16-17) remains untouched (that is why (-2) is 
     * done in whereever au1MrvlHdr's size is referred)] */

    MEMSET (au1MrvlHdr, 0, CFA_ENET_V2_HEADER_SIZE + MRVL_HDR_SIZE - 2);

    MEMCPY (au1MrvlHdr, pSkb->data, CFA_ENET_V2_HEADER_SIZE - 2);

    /* Packets arriving from ISS (CPU) has the outgoing port info where as
     * packets transmitted out from Logical L3 Interfaces from Linux will not 
     * have it */
    if (b1PktFromISS == OSIX_FALSE)
    {
        /* Setting Forward DSA Tag bits */
        au1MrvlHdr[12] = 0xC0;
        /* When sending packets with Forward_DSA Tag, device id should
         * be different from the Src Dev Id. Otherwise, the chip will drop
         * the packets assuming that it is looped back to the same source */
        au1MrvlHdr[12] |= MRVL_DEVICE_ID ^ 0x1f;
    }
    else
    {
        /* Setting From_CPU DSA Tag bits */
        au1MrvlHdr[12] = 0x40;
        /* When sending packets with From_CPU tag, device id should be set as
         * the chip's own Device id. Only then the chip will send out the
         * packet */
        au1MrvlHdr[12] |= MRVL_DEVICE_ID;
    }

    /* No way to know this is tagged port/not. Chip has any bit to
     * override this ??? */

    if (b1Tagged == OSIX_TRUE)
    {
        au1MrvlHdr[12] |= 0x20;
    }

    au1MrvlHdr[13] = ((u4OutPort & 0x1F) << 3);
    au1MrvlHdr[14] = ((u1Priority & 7) << 5) | ((VlanId >> 8) & 0xF);
    au1MrvlHdr[15] = (VlanId & 0xFF);

    /* Check whether the SKB has enough headroom to insert Marvel Tag and
     * tailroom to pad the packets having < 60 */

    if (pSkb->len < CFA_ENET_MIN_UNTAGGED_FRAME_SIZE)
    {
        if ((UINT4) skb_tailroom (pSkb) <
            CFA_ENET_MIN_UNTAGGED_FRAME_SIZE - pSkb->len)
        {
            b1NeedToPad = OSIX_TRUE;
        }
        u4PaddingBytes = CFA_ENET_MIN_UNTAGGED_FRAME_SIZE - pSkb->len;
    }

    if ((UINT4) skb_headroom (pSkb) < MRVL_HDR_SIZE)
    {
        u4NewHeadRoom = MRVL_HDR_SIZE;
    }

    /* Creating new SKB for accomodating additional Headroom or Tailroom */
    if ((u4NewHeadRoom != 0) || (b1NeedToPad == OSIX_TRUE))
    {
        if ((pNewSkb = skb_copy_expand (pSkb, u4NewHeadRoom, u4PaddingBytes,
                                        GFP_ATOMIC)) != NULL)
        {
            /* release the old SKB and return the newly allocated SKB to the 
             * caller routine */
            KERN_FREE_SKB (pSkb);
            *ppSkb = pSkb = pNewSkb;
        }
        else
        {
            MUX_TRC (OS_RESOURCE_TRC, " MuxPortAddMrvllHeader : TX Failed "
                     "Unable to allocate memory for Marvell Header \n");
            return -ENOMEM;
        }
    }

    /* Overwrite Ethernet Header with [MAC + Marvel Header] Information by
     * moving the data pointer by 4 bytes (MRVL_HDR_SIZE) and adjusting the
     * SKB's Length field */

    pSkb->data = pSkb->data - MRVL_HDR_SIZE;
    if (b1NeedToPad == FALSE)
    {
        pSkb->len += MRVL_HDR_SIZE + u4PaddingBytes;
    }
    else
    {
        skb_put (pSkb, u4PaddingBytes);
        MEMSET ((pSkb->tail - u4PaddingBytes), 0, u4PaddingBytes);
        pSkb->len += MRVL_HDR_SIZE;
    }

    MEMCPY (pSkb->data, au1MrvlHdr,
            CFA_ENET_V2_HEADER_SIZE + MRVL_HDR_SIZE - 2);

    MuxUtilDumpSKB ((UINT1 *) "AFTER-HDR_ADDTN", pSkb);

    MUX_TRC_ARG1 (MUX_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);
    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  muxmrvl.c                      */
/*-----------------------------------------------------------------------*/
