/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bufmap.h,v 1.2 2014/03/11 14:02:48 siva Exp $
 *
 * Description:This file contains the constants and definitions for 
 *             BUFFER related functions of the PPP Subsystem.
 *
 *******************************************************************/
#ifndef __PPP_BUFMAP_H__
#define __PPP_BUFMAP_H__


/* Message structure */
#define     t_MSG_DESC              tCRU_BUF_CHAIN_DESC

/* Write to the buffer at the given offset. */

#define     ASSIGN1BYTE(pBuf, Offset, Value)                                 \
    {                                                                        \
        UINT1   u1Val = Value;                                               \
        CRU_BUF_Copy_OverBufChain(pBuf,(UINT1 *)&u1Val,(UINT4)Offset,(UINT1)1);            \
        CB_WRITE_OFFSET((pBuf)) = (UINT4)((Offset) + 1);                              \
    }

#define     ASSIGN2BYTE(pBuf, Offset, Value)                                 \
    {                                                                        \
        UINT2 u2Val = OSIX_HTONS((Value));                                   \
        CRU_BUF_Copy_OverBufChain(pBuf,(UINT1 *)&u2Val,(UINT4)Offset,(UINT1)2);            \
        CB_WRITE_OFFSET((pBuf)) = (UINT4)((Offset) + 2);                              \
    }

#define     ASSIGN4BYTE(pBuf, Offset, Value)                                 \
    {                                                                        \
        UINT4 u4Val = OSIX_HTONL((Value));                                   \
        CRU_BUF_Copy_OverBufChain(pBuf,(UINT1 *)&u4Val,(UINT4)Offset,(UINT1)4);            \
        CB_WRITE_OFFSET((pBuf)) = (UINT4)((Offset) + 4);                              \
    } 


#define     ASSIGNSTR(pBuf, pSourceStr, Offset, Length)                      \
    {                                                                        \
        CRU_BUF_Copy_OverBufChain(pBuf,(UINT1 *)(pSourceStr),(UINT4)Offset,(UINT1)Length); \
        CB_WRITE_OFFSET(pBuf) = (UINT4)((Offset) + (Length));                         \
    }


/* Append to the buffer. */

#define     APPEND1BYTE(pBuf, Value)                                         \
    {                                                                        \
        UINT1   u1Val = (UINT1) Value;                                               \
        CRU_BUF_Copy_OverBufChain(pBuf,(UINT1 *)&u1Val,                      \
                                      CB_WRITE_OFFSET(pBuf),(UINT1)1);              \
        CB_WRITE_OFFSET((pBuf)) +=  1;                                       \
    }

#define     APPEND2BYTE(pBuf, Value)                                         \
    {                                                                        \
        UINT2 u2Val = OSIX_HTONS((Value));                                   \
        CRU_BUF_Copy_OverBufChain(pBuf,(UINT1 *)&u2Val,                      \
                                       CB_WRITE_OFFSET(pBuf),(UINT1)2);              \
        CB_WRITE_OFFSET((pBuf)) +=  2;                                       \
    }

#define     APPEND4BYTE(pBuf, Value)                                         \
    {                                                                        \
        UINT4 u4Val = OSIX_HTONL((Value));                                   \
        CRU_BUF_Copy_OverBufChain(pBuf,(UINT1 *)&u4Val,                      \
                                       CB_WRITE_OFFSET(pBuf),(UINT1)4);              \
        CB_WRITE_OFFSET((pBuf)) +=  4;                                       \
    }

#define     APPENDSTR(pBuf, pSourceStr, Length)                              \
    {                                                                        \
        CRU_BUF_Copy_OverBufChain(pBuf,(UINT1 *)pSourceStr,                  \
                                           CB_WRITE_OFFSET(pBuf),(UINT1)Length);     \
        CB_WRITE_OFFSET((pBuf)) += Length;                                   \
    }


/* Read the buffer from the specified offset. */

#define     GET1BYTE(pBuf,Offset,Value)                                      \
    {                                                                        \
        UINT1   u1Val;                                                       \
        CRU_BUF_Copy_FromBufChain (pBuf,(UINT1 *)&u1Val,(UINT4)Offset,(UINT1)1);           \
        Value = u1Val;                                                       \
        CB_READ_OFFSET((pBuf)) = (UINT4)((Offset) + 1);                               \
    }

#define     GET2BYTE(pBuf,Offset,Value)                                      \
    {                                                                        \
        UINT2   u2Val;                                                       \
        CRU_BUF_Copy_FromBufChain (pBuf,(UINT1 *)&u2Val,(UINT4)Offset,(UINT1)2);           \
        Value = OSIX_NTOHS(u2Val);                                           \
        CB_READ_OFFSET((pBuf)) = (UINT4)((Offset) + 2);                               \
    }

#define     GET4BYTE(pBuf,Offset,Value)                                      \
    {                                                                        \
        UINT4   u4Val;                                                       \
        CRU_BUF_Copy_FromBufChain (pBuf,(UINT1 *)&u4Val,(UINT4)Offset,(UINT1)4);           \
        Value = OSIX_NTOHL(u4Val);                                           \
        CB_READ_OFFSET((pBuf)) = (UINT4)((Offset) + 4);                               \
    }

#define     GETSTR(pBuf, pStr, Offset, Size)                                 \
    {                                                                        \
        CRU_BUF_Copy_FromBufChain (pBuf,(UINT1 *)pStr,(UINT4)Offset,Size);          \
        CB_READ_OFFSET((pBuf)) = (UINT4)((Offset) + Size);                            \
    }


/* Read the buffer from the Buffer's ReadOffset */

#define     EXTRACT1BYTE(pBuf, Value)                                        \
    {                                                                        \
        CRU_BUF_Copy_FromBufChain (pBuf,(UINT1 *)&Value,                     \
                                            CB_READ_OFFSET(pBuf),1);         \
        CB_READ_OFFSET((pBuf)) += 1;                                         \
    }

#define     EXTRACT2BYTE(pBuf, Value)                                        \
    {                                                                        \
        CRU_BUF_Copy_FromBufChain (pBuf,(UINT1 *)&Value,                     \
                                        CB_READ_OFFSET(pBuf),2);             \
        Value = OSIX_NTOHS(Value);                                           \
        CB_READ_OFFSET((pBuf)) += 2;                                         \
    }

#define     EXTRACT4BYTE(pBuf, Value)                                        \
    {                                                                        \
        CRU_BUF_Copy_FromBufChain (pBuf,(UINT1 *)&Value,                     \
                                        CB_READ_OFFSET(pBuf),4);             \
        CB_READ_OFFSET((pBuf)) += 4;                                         \
        Value = OSIX_NTOHL(Value);                                           \
    }


#define     EXTRACTSTR(pBuf, pDestStr, Size)                                 \
    {                                                                        \
        CRU_BUF_Copy_FromBufChain ((pBuf), (pDestStr) ,                      \
                                       (CB_READ_OFFSET(pBuf)), (Size));      \
        CB_READ_OFFSET((pBuf)) += (Size);                                    \
}


#define MOVE_BACK_PTR(pBuf,Size)                                             \
         if (CRU_BUF_Prepend_BufChain(pBuf, NULL, Size) == CRU_SUCCESS) {    \
            CB_WRITE_OFFSET((pBuf)) = 0;                                     \
         }
 
#define MOVE_OFFSET(pBuf, Offset)                                            \
            if (CRU_BUF_Move_ValidOffset(pBuf, (UINT4)Offset) == CRU_SUCCESS) {     \
                CB_READ_OFFSET((pBuf)) = 0;                                  \
                CB_WRITE_OFFSET((pBuf)) = 0;                                 \
            }
   

#define VALID_BYTES(pBuf)                                                    \
        CRU_BUF_Get_ChainValidByteCount(pBuf)

#define CRU_BUF_COPY_MSGS(pDest, pSrc, DestOffset, SrcOffset, Size)          \
    {                                                                        \
        UINT1   *pLinear;                                                    \
        pLinear = MEM_MALLOC(Size,UINT1);                                    \
        CRU_BUF_Copy_FromBufChain(pSrc,pLinear,SrcOffset,Size);              \
        CRU_BUF_Copy_OverBufChain(pDest,pLinear,DestOffset,Size);            \
        MEM_FREE(pLinear);                                                   \
        CB_READ_OFFSET((pSrc)) = (UINT4)(SrcOffset + Size);                           \
        CB_WRITE_OFFSET((pDest)) = (UINT4)(DestOffset + Size);                        \
    }

        

#define COMPARE_BUFS(pBuf1,Offset1,pBuf2,Offset2,Size)                       \
        MEMCMP((((UINT1 *)CB_FVB(pBuf1))+Offset1),                           \
           (((UINT1 *)CB_FVB(pBuf2))+Offset2), Size)

#define  DELETE_BYTES_AT_END(pBuf, Size)                                     \
          if (CRU_BUF_Delete_BufChainAtEnd(pBuf,Size) != NULL) {             \
             if ( ((CB_VBC(pBuf)) - Size) < CB_WRITE_OFFSET(pBuf) )          \
                CB_WRITE_OFFSET(pBuf) =  CB_VBC(pBuf) - Size;                \
             if (((CB_VBC(pBuf)) - Size) < CB_READ_OFFSET(pBuf))             \
                CB_READ_OFFSET(pBuf) = CB_VBC(pBuf) - Size;                  \
          }\


 #define SPLIT_INTO_FRAGMENTS(pOldBuf,Offset,ppFragBuf)   \
     ((CRU_BUF_Fragment_BufChain(pOldBuf,Offset,ppFragBuf) == CRU_SUCCESS) ?  \
        (CB_WRITE_OFFSET((pOldBuf)) = 0, CB_READ_OFFSET((*ppFragBuf))= 0, \
         CB_WRITE_OFFSET((*ppFragBuf)) = 0, CRU_SUCCESS) :   \
           (OutAllocCounter++, CRU_FAILURE)) 


#define CRU_BUF_CONCAT_MSGS(pBuf1,pBuf2)   \
        CRU_BUF_Concat_MsgBufChains(pBuf1,pBuf2)

    
#define VALID_BYTES(pBuf)  CRU_BUF_Get_ChainValidByteCount(pBuf)

       
#define ALLOCATE_BUFFER(Size,Offset)  \
         (((pAllocPtr = (UINT1 *)PPPAllocateBuffer((UINT4)Size,(UINT4)Offset)) == NULL)  \
         ?  (NULL)  \
         :  (OutAllocCounter++,pAllocPtr))

#define   ALLOCATE_BUFFER_FOR_INC_MSG(Size,Offset)  \
          (((pAllocPtr = (UINT1 *)PPPAllocateBuffer(Size,Offset)) == NULL)  \
          ?  (NULL) \
          :  (InAllocCounter++,pAllocPtr))

#define   ALLOCATE_BUFFER_FOR_HL_MSG(Size,Offset)  \
          (((pAllocPtr = (UINT1 *)PPPAllocateBuffer(Size,Offset)) == NULL)  \
          ?  (NULL) \
          :  (pAllocPtr))

#define   RELEASE_HL_BUFFER(p1) \
            CRU_BUF_Release_MsgBufChain(p1,FALSE);

#define   RELEASE_BUFFER(p1)    \
                    CRU_BUF_Release_MsgBufChain(p1,FALSE);      \
                        OutRelCounter++;    \
                        p1 = NULL;

#define   RELEASE_INC_BUFFER(p1)    \
            CRU_BUF_Release_MsgBufChain(p1,FALSE);    \
            InRelCounter++;

#define   DUPLICATE_BUFFER(pBuf, Flag)  \
            (((pAllocPtr =  (UINT1 *)PPPDuplicateBuffer(pBuf)) == NULL) \
            ?  (NULL)   \
            :  (OutAllocCounter++, pAllocPtr))
 
 /* 
  * COPY_TO_BUFFER is used only by vj-compression module. 
  * If TCP header is assumed to be part of linear buffer, then this macro
  * can be mapped to GETSTR()
  */

#define COPY_TO_BUFFER(pBuf, pArr, Offset, Size)    \
        CRU_BUF_Copy_OverBufChain(pBuf, pArr, Offset, Size); \
        CB_WRITE_OFFSET((pBuf)) = Offset + Size  ;  \
        
        
 /* 
  * If the Buffer is linear , then this macro
  * can be mapped to ASSIGNSTR()
  */

#define COPY_FROM_BUFFER(pBuf, pArr, Offset, Size)    \
        CRU_BUF_Copy_FromBufChain(pBuf, pArr, Offset, Size); \
        CB_READ_OFFSET((pBuf)) = Offset + Size  ;  \
    
#define GET_DATA_POINTER(pBuf)     CB_FVB(pBuf)

#define PPP_GET_MODULE_DATA_IF_NUMBER(pBuf)   \
                      (pBuf->ModuleData.u4Reserved1)
#define PPP_GET_MODULE_DATA_IF_ENCAP(pBuf)   \
                      (pBuf->ModuleData.u4Reserved3)
#define PPP_GET_MODULE_DATA_PROTOCOL(pBuf)   \
                      (pBuf->ModuleData.u2Reserved)
        
#define PPP_GET_PORT_NUMBER(pBuf) PPP_GET_MODULE_DATA_IF_NUMBER(pBuf)

#define PPP_GET_SRC_MODID(pBuf)                                             \
                                          (pBuf->ModuleData.u4Reserved2)

 
#endif
