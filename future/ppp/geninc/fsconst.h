/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsconst.h,v 1.2 2011/07/12 11:08:28 siva Exp $
 *
 * Description:This files contains type definitions for the constants 
 * used by the PPP stack
 *
 *******************************************************************/
#ifndef NULL
#define NULL    (0)
#endif

#if !defined(NOT_OK) || (NOT_OK != (-1))
#undef NOT_OK
#define NOT_OK (-1)
#endif

#if !defined(TMO_NOT_OK) || (TMO_NOT_OK != (-1))
#undef TMO_NOT_OK
#define TMO_NOT_OK (-1)
#endif

#ifndef EXPORT
#define EXPORT
#endif

#if !defined(FALSE) || (FALSE != 0)
#undef FALSE
#define FALSE  (0)
#endif

#if !defined(TRUE) || (TRUE != 1)
#undef TRUE
#define TRUE   (1)
#endif

