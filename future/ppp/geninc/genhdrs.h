/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: genhdrs.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description: This file contains the include files used by the PPP
 * stack
 *
 *******************************************************************/
#include "fsconst.h"
#include "sllmap.h"
#include "bufmap.h"

#include "lr.h"
#include  "cfa.h"
#include "ppp.h"
