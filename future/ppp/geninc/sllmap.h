/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: sllmap.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains mapping of SLI functions to TMO
 * functions
 *
 *******************************************************************/
#define t_SLL      		       tTMO_SLL
#define t_SLL_NODE 		       tTMO_SLL_NODE

#define   SLL_INIT(node)               TMO_SLL_Init(node)
#define   SLL_INIT_NODE(node)          TMO_SLL_Init_Node(node)
#define   SLL_SCAN(List,node,Type)     TMO_SLL_Scan(List,node,Type)
#define   SLL_DELETE(List,node)        TMO_SLL_Delete(List,node)
#define   SLL_ADD(List,Node)           TMO_SLL_Add(List,Node)
#define   SLL_INSERT(List,Prev,node)   TMO_SLL_Insert(List,Prev,node)
#define   SLL_COUNT(List)              TMO_SLL_Count(List)
#define   SLL_PREV(List,Node)          TMO_SLL_Previous(List,Node)
#define   SLL_FIRST(List)              TMO_SLL_First(List)
#define   SLL_LAST(List)               TMO_SLL_Last(List)
#define   SLL_NEXT(List,Node)          TMO_SLL_Next(List,Node)
#define   SLL_CONCAT(pDstLst,pAddLst)  TMO_SLL_Concat(pDstLst,pAddLst)
#define   SLL_GET(List)				   TMO_SLL_Get(List)
#define   SLL_DELETE_LIST(pList, DelFuncOrMacro)    {   \
			while (SLL_COUNT(pList))    				 \
				DelFuncOrMacro(SLL_GET(pList));     \
		  }


#define   SLL_DELETE_TILL_NODE(List,Start,End,Fn)  \
			TMO_SLL_Delete_Till_Node(List,Start,End,Fn)


/*
 * Changed TMO_SLL_Scan_Offset() to PPP_SLL_Scan_Offset
 * as this is not a TMO Macro
 */
#define   SLL_SCAN_OFFSET(List,Node,Type,Offset) \
			PPP_SLL_Scan_Offset(List,Node,Type,Offset)


#define PPP_SLL_Scan_Offset(pList,pNode,TypeCast,Offset) \
           for(pNode = (TypeCast)(VOID *)(TMO_SLL_First((pList))); \
                      ((pNode != NULL) ? (pNode = (TypeCast)(VOID *)((UINT1 *) pNode - (Offset)), pNode) :  NULL); \
                             (pNode = ((TypeCast)(VOID *)((UINT1 *)pNode+ (Offset))), pNode = (TypeCast) (VOID *)TMO_SLL_Next((pList),((t_SLL_NODE *)(VOID *)(pNode)))))
 
