/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: baprm.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This files contains the test data structures and  
 *			   macros of RM module.
 *
 *******************************************************************/
#ifndef __PPP_BAPRM_H__
#define __PPP_BAPRM_H__

#define RM_MAX_PH_NUMBERS	10

typedef struct RMInfo {
	tPPPIf		*pIf;
	tPhNum		LocFirstLnkPhNum;
	tPhNum		RemFirstLnkPhNum;
	tPhNum		AttemptedPhNum;
	tReqBlock	ReqBlock;
	UINT1		IsInitiator;
	UINT1		TempPhNumString [PPP_MAX_PH_NUM_SIZE];
	UINT1		PhNumCounter;
	UINT1		Requester;
	UINT1		u1Rsvd;
} tRMInfo;

typedef	struct RMReqEntry {
	t_SLL_NODE	Next;
	UINT1		Requester;
	UINT1		Id;
	UINT1		ReqType;
	UINT1		u1Rsvd;
	tPPPIf		*pIf;
	tPPPIf		*pBundleIf;
} tRMReqEntry;



#endif  /* __PPP_BAPRM_H__ */
