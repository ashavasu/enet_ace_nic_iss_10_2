/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bapprot.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the funtion prototypes for 
 *             BAP functions used internally by the BAP module.
 *
 *******************************************************************/
#ifndef __PPP_BAPPROT_H__
#define __PPP_BAPPROT_H__

#include "baprm.h"

INT1 BAPAddLinkToBundle(tPPPIf *pBundleIf, tReqHandle *pRequestHandle, UINT1 ReqType, tBAPOpts *pBAPOptions);
INT1 BAPDeleteLinkFromBundle(tPPPIf *pBundleIf, tPPPIf *pIf, tReqHandle *pRequestHandle);
VOID BAPCallNotificationFromRM(tPPPIf *pBundleIf, tReqHandle *pRequestHandle, tPhNum PhNumber, UINT1 MsgCode, UINT1 Action );
INT1 BAPVerifyPeerRequestFromRM(tPPPIf *pBundleIf, tReqHandle *pRequestHandle, UINT1 RequestType, tBAPOpts *pBAPOptions);
VOID BAPInformRequestStatusToRM(tPPPIf *pBundleIf, tReqHandle *pRequestHandle, UINT1 Status, tBAPOpts *pBAPOptions);
VOID BAPInput(tPPPIf  *pBundleIf, t_MSG_DESC *pInPkt, UINT2 Length );
tBAPInfo *BAPCreateAndInit ( tPPPIf  *pBundleIf );
VOID BAPDelete ( tPPPIf  *pBundleIf );
INT1 BAPProcessPktHeader ( t_MSG_DESC *pInPkt, UINT2 *pLength, tReqBlock *pReqBlock );
INT1 BAPProcessOptions ( tBAPInfo *pBAPInfo, t_MSG_DESC *pInPkt, UINT2 Length, tReqBlock *pReqBlock );

VOID BAPPrependBAPHeaderAndTxBAPPkt (tBAPInfo *pBAPInfo, UINT1 ReqType, UINT1 Id, UINT1 RespCode, t_MSG_DESC *pPacket, UINT2 PktLen);
VOID BAPConstructAndTxResponse (tBAPInfo *pBAPInfo, UINT1 ReqType, UINT1 RespType, UINT1 Id, UINT1 ReasonCode);
VOID BAPConstructAndTxAckResponse (tBAPInfo *pBAPInfo, tReqBlock *pReqBlock, UINT1 ReqType );
VOID BAPConstructAndTxRequestPkts (UINT1 ReqType, tPendingRequest *pRequest);
UINT2 BAPAddLinkTypeOption (t_MSG_DESC  *pPacket, UINT2 Offset, UINT2 LinkSpeed, UINT1 LinkType);
UINT2 BAPAddMultiplePhoneDeltaOptions (t_MSG_DESC  *pPacket, UINT2 Offset, tPhNumbers *pPhoneNumbers, tPhNum FirstLnkPhNum);
UINT1 BAPFindUniqueDigits (tPhNum PhNum, tPhNum FirstLnkPhNum);
UINT2 BAPAddPhoneDeltaSubOptions (t_MSG_DESC  *pPacket, UINT2 Offset, tPhNum *pPhNum, UINT1 UniqueDigits);
UINT2 BAPAddNoPhoneNumberNeededOption (t_MSG_DESC  *pPacket, UINT2 Offset);
UINT2 BAPAddReasonOption (t_MSG_DESC  *pPacket, UINT2 Offset, UINT1 ReasonCode);
UINT2 BAPAddLinkDiscrOption (t_MSG_DESC *pPacket, UINT2 Offset, UINT2 LinkDiscr);
UINT2 BAPAddCallStatusOption (t_MSG_DESC  *pPacket, UINT2 Offset, UINT1 MsgCode, UINT1 Action);

VOID RMInit (VOID);
VOID RMDelete (VOID);
VOID RMCreateInitAndAddReqEntry (tPPPIf *pBundleIf, UINT1 Id, UINT1 Requester, UINT1 ReqType, tPPPIf *pIf);
tRMReqEntry *RMGetReqEntry (tPPPIf *pBundleIf, UINT1 Id, UINT1 Requester);
VOID RMCallInfoToMP (tPPPIf *pIf, UINT1 *pIsInitiator, tPhNum *pLocalPhNum, tPhNum *pRemotePhNum);
INT1 RMVerifyPeerRequest (tPPPIf *pBundleIf, tReqHandle *pRequestHandle, UINT1 RequestType, tBAPOpts *pOptions );
VOID RMRequestStatusFromBAP (tPPPIf *pBundleIf, tReqHandle *pRequestHandle, UINT1 Status, tBAPOpts *pOptions);


UINT1 BAPSETDestroyReqEntryOnFailure (tPendingRequest *pRequest);
UINT1 BAPSETDestroyOnFullNakResponse (tPendingRequest *pRequest);
UINT1 BAPSETDestroyOnRejectResponse (tPendingRequest *pRequest);
UINT1 BAPSETResendCallRequest (tPendingRequest *pRequest);
UINT1 BAPSETResendCallBackRequest (tPendingRequest *pRequest);
UINT1 BAPSETResendLinkDropRequest (tPendingRequest *pRequest);
UINT1 BAPSETResendStatusIndication (tPendingRequest *pRequest);
UINT1 BAPSETMoveToCallAckRxd (tPendingRequest *pRequest);
UINT1 BAPSETMoveToCallBackAckRxd (tPendingRequest *pRequest);
UINT1 BAPSETInformRMToDropLink (tPendingRequest *pRequest);
UINT1 BAPSETMoveToCallStatusSent (tPendingRequest *pRequest);
UINT1 BAPSETMoveToCallBackStatusSent (tPendingRequest *pRequest);
UINT1 BAPSETSendStatusRespAndDestroy (tPendingRequest *pRequest);
UINT1 BAPSETSendStatusResp(tPendingRequest *pRequest);
UINT1 BAPSETLeaveCStatSentOrDestroy (tPendingRequest *pRequest);
UINT1 BAPSETLeaveCBStatSentOrDestroy (tPendingRequest *pRequest);
UINT1 BAPSETMoveToCNakRxdOrDestroy (tPendingRequest *pRequest);
UINT1 BAPSETMoveToCBNakRxdOrDestroy (tPendingRequest *pRequest);
UINT1 BAPSETMoveToLDNakRxdOrDestroy (tPendingRequest *pRequest);
UINT1 BAPSETIdleWait (tPendingRequest *pRequest);

UINT1 BAPIsRetryRequestActionTaken (tPendingRequest *pRequest);

VOID BAPUpdatePendingReqStatus (tPendingRequest *pRequest, UINT1 Event);
INT1 BAPCreateAndAddPendingRequest (tBAPInfo *pBAPInfo, tReqBlock *pReqBlock, UINT1 State);
INT1 BAPPendingRequestInit (tPendingRequest *pRequest, tReqBlock *pReqBlock, UINT1 State);
VOID BAPDeletePendingRequest (tPendingRequest *pRequest);

VOID BAPTimeOut (VOID *pRequest);

INT1 BAPGenerateEvent (tBAPInfo *pBAPInfo, UINT1 Event , tReqBlock *pReqBlock);

INT1 BAPProcessPeerResponse (tBAPInfo *pBAPInfo, UINT1 Event , tReqBlock *pReqBlock);
INT1 BAPProcessPeerRequest (tBAPInfo *pBAPInfo, UINT1 Event , tReqBlock *pReqBlock);
INT1 BAPProcessStatusIndication (tBAPInfo *pBAPInfo, tReqBlock *pReqBlock);
INT1 BAPProcessRMRequest (tBAPInfo *pBAPInfo, UINT1 Event, tReqBlock *pReqBlock);
UINT2 BAPGetSearchStateFrmEvt (UINT1 Event);

tPendingRequest *BAPMatchWithPendingRequest (t_SLL *pRequestList, UINT1 Id, UINT2 DesiredState, tPhNumbers *pPhNumbers);

INT1 BAPFindFavoredPeerIfNecessary (tBAPInfo *pBAPInfo, UINT1 ReqType, tPendingRequest *pNodeToBeDeleted);

INT1 PPPBAPAddLinkToBundle (UINT2 u2BundleIfIndex,
                            tReqHandle *pRequestHandle,
                            UINT1 u1ReqType,
                            tBAPOpts *pBAPOptions);


INT1 PPPBAPDeleteLinkFromBundle (UINT2 u2BundleIfIndex,
                                 UINT2 u2MemberIfIndex, 
                                 tReqHandle *pRequestHandle);

VOID PPPBAPCallNotificationFromRM (UINT2 u2BundleIfIndex, 
                                   tReqHandle *pRequestHandle,
                                   tPhNum PhNumber,
                                   UINT1 u1MsgCode,
                                   UINT1 u1Action); 
#endif  /* __PPP_BAPPROT_H__ */
