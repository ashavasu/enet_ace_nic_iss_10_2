/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rmexts.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This files contains the (test) external interface 
 *			   procedures used by the other modules.
 *
 *******************************************************************/
#ifndef __PPP_RMEXTS_H__
#define __PPP_RMEXTS_H__


/**************************************************
 Global variable used by the test low level routines
 and other test stubs/drivers. 
***************************************************/

extern	tRMInfo	RMInfo;
extern	t_SLL	RMRequestDataBase;

/**************************************************
		Function Prototypes
***************************************************/

VOID RMInit (VOID);
VOID RMDelete (VOID);
VOID RMCreateInitAndAddReqEntry (tPPPIf *pBundleIf, UINT1 Id, UINT1 Requester, UINT1 ReqType, tPPPIf *pIf);
INT1 RMVerifyPeerRequest (tPPPIf *pBundleIf, tReqHandle *pRequestHandle, UINT1 RequestType, tBAPOpts *pOptions );
VOID RMRequestStatusFromBAP (tPPPIf *pBundleIf, tReqHandle *pRequestHandle, UINT1 Status, tBAPOpts *pOptions);
VOID RMCallInfoToMP (tPPPIf *pIf, UINT1 *pIsInitiator, tPhNum *pLocalPhNum, tPhNum *pRemotePhNum);



#endif  /* __PPP_RMEXTS_H__ */
