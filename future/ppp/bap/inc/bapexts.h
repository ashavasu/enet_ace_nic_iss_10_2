/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bapexts.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the function prototypes used by
 *			   other modules.
 *
 *******************************************************************/
#ifndef __PPP_BAPEXTS_H__
#define __PPP_BAPEXTS_H__

extern	tBAPMemCounters	BAPMemCounters;

#define	BAP_SUCCESS_CODE		0

#endif  /* __PPP_BAPEXTS_H__ */
