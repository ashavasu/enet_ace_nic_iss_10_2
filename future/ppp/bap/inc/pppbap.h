/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppbap.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the data structures and macros
 *             used by BAP Module.
 *
 *******************************************************************/
#ifndef __PPP_PPPBAP_H__
#define __PPP_PPPBAP_H__

/* Possible Requestor values of the Requester field. */
#define		BAP_LOCAL_RM				0
#define		BAP_REMOTE_PEER				1

/* 
	Used by internal functions and external interfaces with the RM to keep 
	track of a BAP Request.
*/
typedef struct ReqHandle {
	UINT1		Requester;		/* Specifies who originated this request */
	UINT1		Id;				/* Unique Identifier of the request */
	UINT2		u2Rsvd;
} tReqHandle;

/* Stores several phone numbers each of type tPhNum */
typedef struct PhNumbers {
	UINT1		NumPhoneNumbers;	/* Number of phone numbers in the array */
	UINT1		u1Rsvd;
	UINT2		u2Rsvd;
	tPhNum		PhoneNumbers[1];	/* Set of NumPhoneNumbers phone numbers */
 } tPhNumbers;

/* Temporary structure used for phone number construction */
typedef struct {
	t_SLL_NODE	NextPhNum;
	tPhNum		PhoneNumber;
} tTempPhNode;

/* Counters for BAP packets */
typedef struct BAPPktCounters {
	UINT4		CallRequests;
	UINT4		CallBackRequests;
	UINT4		LinkDropRequests;
	UINT4		CallReqsAcked;
	UINT4		CallBackReqsAcked;
	UINT4		LinkDropReqsAcked;
} tBAPPktCounters;

/* BAP Statistics of incoming and outgoing packets */
typedef struct BAPStats {
	tBAPPktCounters		Incoming;
	tBAPPktCounters		Outgoing;
} tBAPStats;

/* BAP options structure */
typedef struct BAPOpts {
	UINT1		IsPhNumNeeded;  /* Specifies if phone number is needed */
	UINT1		LinkType;       /* The type of link requested */
	UINT2		LinkSpeed       /* The Speed of the requested link */;
	UINT2		LinkDiscr;  	/*Link discriminator of a link-drop request*/
	UINT2		u2Rsvd;
	tPhNumbers	*pPhNumbers;    /* Phone numbers for the request */
} tBAPOpts;

/* Maximum value that the link discriminator can take. */
#define	BAP_MAX_LINK_DISCR_VAL	0xffff

/* Maximum value that the RequestId field can take. */
#define	MAX_LINK_DROP_ID	0xff	

/* Information pertaining BAP on a bundle */
typedef struct BAPInfo { 	
	UINT2		LocalNextLinkDiscrValue;
	UINT2		NextNakLinkDiscrValue;
	tPhNum		LocFirstLnkPhNum;
	tPhNum		RemFirstLnkPhNum;
	t_SLL		BAPRequestInstanceList;	/* List of BAP Pending Requests */
	tBACPIf		BACPIf;					/* BACP interface for the bundle */
	tPPPIf		*pBundleIf;				/* Pointer to Bundle Interface */ 
	UINT1		RequestId;		/* Identifier of the last request sent */
	UINT1		LastLinkDropId;	/* Indetifier of the last link drop request */
					/* Possible request types that peer is allowed to make */
	UINT2		RemoteAllowedReqTypes;	
	UINT2		RetransmitTimeOutVal;	/* Time to retransmit BAP packets */
	UINT2		MaxRetransmits;			/* Maximum allowed retransmits */
							/* Time to retry a BAP request on Full-Nak */
	UINT2		RetryTimeOutVal;
	UINT2		u2Rsvd;
	tBAPStats	Stats;		/* BAP Packet Statistics for the Bundle */
} tBAPInfo;

/* States of the PENDING REQUEST's SET */
#define		BAP_CALL_REQ_SENT				0x00
#define		BAP_CALL_BACK_REQ_SENT			0x01
#define		BAP_LINK_DROP_REQ_SENT			0x02
#define		BAP_CALL_ACK_RXD				0x03
#define		BAP_CALL_BACK_ACK_RXD			0x04
#define		BAP_CALL_NAK_RXD				0x05
#define		BAP_CALL_BACK_NAK_RXD			0x06
#define		BAP_LINK_DROP_NAK_RXD			0x07
#define		BAP_CALL_STATUS_SENT			0x08
#define		BAP_CALL_BACK_STATUS_SENT		0x09
#define		BAP_CALL_RESP_SENT				0x0A
#define		BAP_CALL_BACK_RESP_SENT			0x0B
#define		BAP_MAX_STATES					BAP_CALL_BACK_RESP_SENT + 1

/* Possible values of Call Status Action Octet */
#define		BAP_NO_RETRY			0
#define		BAP_RETRY				1

/* Q.931 Codes - Possible value of MsgCode */
#define	BAP_USER_BUSY	17

/* Information on a BAP Request in the Pending Request List */
typedef struct PendingRequest {
	t_SLL_NODE		NextMember;		/* Points to the next pending request */
	tReqHandle		RequestHandle;	/* Request Identifier */
	tBAPInfo		*pBAPInfo;		/* BAP information for the bundle */
	UINT1			State;			/* Current State of the request */
	UINT1			u1Rsvd;
	UINT2			RetransmitCounter;	/* No of requests retransmitted */
	tPPPTimer		RetransmitTimer;	/* Retransmit Timer */
	tBAPOpts		Options;		/* BAP option information */
	/* Following are required for constructing the retransmission packet */
	UINT1			LastActionSent;		/* The last action code sent */
	UINT1			MsgCode;		/* The Q.931 code */
	UINT2			u2Rsvd;
} tPendingRequest;

/*	
	Structure to collect information needed by the internal BAP routines from 
	the external interface functions.
*/
typedef struct ReqBlock {
	tBAPOpts	Options;	/* BAP option information */
	UINT1		ReqType;	/* The type of request */
	UINT1		Id;			/* Unique Identifier of the request */
	UINT1		RespCode;	/* Response Type */
	UINT1		ActionSent;	/* The action code sent in the request */ 
	UINT1		MsgCode;	/* The Q.931 code in the request */
	UINT1		u1Rsvd;
	UINT2		u2Rsvd;
} tReqBlock;

	/* Memory allocation and free counters for BAP data structures */
typedef struct BAPCounters {
	UINT4	BAPAllocCounter;
	UINT4	BAPFreeCounter;
	UINT4	BAPInfoAllocCounter;
	UINT4	BAPInfoFreeCounter;
} tBAPMemCounters;

/* Packet Information used for packet validation */
typedef	struct PktInfo {
	UINT1	PktType;
	UINT1	MandatoryOptions;
	UINT1	AllowedOptions;
	UINT1	u1Rsvd;
} tBAPPktInfo;

/* Option Information used for packet validation */
typedef struct {
	UINT1 OptionType;
	UINT1 OptionMask;
	UINT2 u2Rsvd;
	INT1 ( *pProcessBAPOption ) ( tBAPInfo *, t_MSG_DESC *, UINT2, tReqBlock *, UINT1*, t_SLL * );
} tBAPOptInfo;


/******************************************************************
	Macros related to the BAP SET and event generation to the SET 
******************************************************************/
/* Events generated by the external interafces to the BAP Module */
#define		BAP_CALL_REQUEST			0x01
#define		BAP_CALL_RESPONSE			0x02
#define		BAP_CALL_BACK_REQUEST		0x03
#define		BAP_CALL_BACK_RESPONSE		0x04
#define		BAP_LINK_DROP_REQUEST		0x05
#define		BAP_LINK_DROP_RESPONSE		0x06
#define		BAP_STATUS_INDICATION		0x07
#define		BAP_STATUS_RESPONSE			0x08
#define		BAP_TX_CALL_REQUEST			0x09
#define		BAP_TX_CALL_BACK_REQUEST	0x0A
#define		BAP_TX_LINK_DROP_REQUEST	0x0B


/* Bit Masks correspponding to states of the PENDING REQUESTs */
#define		BAP_CALL_REQ_SENT_MASK				0x0001
#define		BAP_CALL_BACK_REQ_SENT_MASK			0x0002
#define		BAP_LINK_DROP_REQ_SENT_MASK			0x0004
#define		BAP_CALL_ACK_RXD_MASK				0x0008
#define		BAP_CALL_BACK_ACK_RXD_MASK			0x0010
#define		BAP_CALL_NAK_RXD_MASK				0x0020
#define		BAP_CALL_BACK_NAK_RXD_MASK			0x0040
#define		BAP_LINK_DROP_NAK_RXD_MASK			0x0080
#define		BAP_CALL_STATUS_SENT_MASK			0x0100
#define		BAP_CALL_BACK_STATUS_SENT_MASK		0x0200
#define		BAP_CALL_RESP_SENT_MASK				0x0400
#define		BAP_CALL_BACK_RESP_SENT_MASK		0x0800

#define		BAP_DESTROY						0x7f

/* Events to the SET of PENDING REQUEST */
#define		BAP_RRA				0x00
#define		BAP_RRN				0x01
#define		BAP_RRR				0x02
#define		BAP_RFN				0x03
#define		BAP_TO_PLUS			0x04
#define		BAP_TO_MINUS		0x05
#define		BAP_RSI_PLUS		0x06
#define		BAP_RSI_MINUS		0x07
#define		BAP_RSR				0x08
#define		BAP_RX_CALL_STATUS	0x09

/******************************************************************
	Macros used for BAP packet construction and decoding
******************************************************************/

/* BAP Packet Response Codes */
#define		BAP_REQUEST_TYPE		0xff
#define		BAP_REQUEST_ACK			0x00
#define		BAP_REQUEST_NAK			0x01
#define		BAP_REQUEST_REJECT		0x02
#define		BAP_REQUEST_FULL_NAK	0x03	


/* Bap Option Types */
#define		BAP_LINK_TYPE			1
#define		BAP_PHONE_DELTA			2
#define		BAP_NO_PHONE_NUM_NEEDED	3
#define		BAP_REASON				4
#define		BAP_LINK_DISCR			5
#define		BAP_CALL_STATUS			6

#define		BAP_MIN_OPT_TYPES	1
#define		BAP_MAX_OPT_TYPES	6

/* Bap options field length */
#define BAP_OPT_HDR_LEN			2
#define BAP_LINK_TYPE_LEN		5
#define BAP_LINK_TYPE_VAL_LEN	3	/* BAP_LINK_TYPE_LEN - BAP_OPT_HDR_LEN */
#define BAP_NO_PHONE_NUM_NEEDED_LEN	2
#define BAP_LINK_DISCR_LEN		4
#define BAP_LINK_DISCR_VAL_LEN	2	/* BAP_LINK_TYPE_LEN - BAP_OPT_HDR_LEN */
#define BAP_CALL_STATUS_LEN		4
#define BAP_CALL_STATUS_VAL_LEN	2	/* BAP_CALL_STATUS_LEN - BAP_OPT_HDR_LEN*/

/* Bap sub-options */
#define		BAP_UNIQUE_DIGITS		1
#define		BAP_SUBSCRIBER_NUMBER	2
#define		BAP_SUB_ADDRESS			3

/* Bap sub-option length */
#define		BAP_UNIQUE_DIGITS_LEN	3

/* Bap option type bit masks */
#define 	BAP_LINK_TYPE_MASK		0x01
#define 	BAP_PH_DELTA_MASK		0x02
#define 	BAP_NO_PH_NUM_MASK		0x04
#define 	BAP_REASON_MASK			0x08
#define 	BAP_LINK_DISCR_MASK		0x10
#define 	BAP_CALL_STATUS_MASK	0x20

/* Bap sub-option bit masks */
#define 	BAP_UNIQUE_DIGITS_MASK		0x01
#define 	BAP_SUBSCRIBER_NUM_MASK		0x02
#define 	BAP_SUB_ADDR_MASK			0x04

/******************************************************************
	Macros used by BAP Module internally. 
******************************************************************/
/* Counter related info*/
#define		BAP_DEF_MAX_RETRANSMITS	2
#define		BAP_DEF_TIME_OUT_VAL	3
#define		BAP_RESP_WAIT_FACTOR	5

/* Reason Codes used for Reason option */
#define		BAP_NO_REASON			0
#define		BAP_MAX_REASONS			5
#define		BAP_REASON_NOT_ALLOWED	0
#define		BAP_REASON_FAVORED_PEER	1
#define		BAP_REASON_BAP_RETRY	2
#define		BAP_REASON_BW			3
#define		BAP_REASON_RETRANS		4

/******************************************************************
	Macros used by the external interface routines of BAP 
******************************************************************/
/* Allowed remote request types */
#define		BAP_REMOTE_CALL_REQ_ALLOWED				0x01
#define		BAP_REMOTE_CALL_BACK_REQ_ALLOWED		0x02
#define		BAP_REMOTE_BOTH_ALLOWED					0x03
#define		BAP_REMOTE_NONE_ALLOWED					0x04

/* RM's response for peer's request */
#define		BAP_NOT_ACCEPTABLE	2
#define		BAP_BW_EXTREME		3

/* Status of a Transactio To RM */
#define		BAP_SUCCESS			1
#define		BAP_FAILURE			2
#define		BAP_END_SUCCESS		4
#define		BAP_END_FAILURE		5
#define		BAP_RETRY_LATER		6

#define    RETRY_TRUE              1
#define    RETRY_FALSE             2
#define    MIN_ALLOWED_REQ_TYPE    1
#define    MAX_ALLOWED_REQ_TYPE    4

/* Macro functions */
#define		BAP_PRINT_STR(Str, Size)

#define		ALLOC_BAP_PENDING_REQUEST()	\
		(((pAllocPtr = (UINT1 *)MEM_CALLOC(1,sizeof(tPendingRequest),void))==NULL)	\
			?	(NULL)	\
			:	(BAPMemCounters.BAPAllocCounter++,pAllocPtr))

#define		FREE_BAP_PENDING_REQUEST(ptr)	{ \
				MEM_FREE(ptr);	\
				BAPMemCounters.BAPFreeCounter++;	\
}

#define		ALLOCATE_BAP_INFO()	\
		(((pAllocPtr = (UINT1 *)MEM_CALLOC(1,sizeof(tBAPInfo),void))==NULL)	\
			?	(NULL)	\
			:	(BAPMemCounters.BAPInfoAllocCounter++,pAllocPtr))

#define		FREE_BAP_INFO(ptr)	{ \
				MEM_FREE(ptr);	\
				BAPMemCounters.BAPInfoFreeCounter++;	\
}


#endif  /* __PPP_PPPBAP_H__ */
