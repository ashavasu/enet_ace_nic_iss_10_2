/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bapcom.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description: This file contains the PPP common files for the BAP
 * module
 *
 *******************************************************************/
#ifndef __PPP_BAPCOM_H__
#define __PPP_BAPCOM_H__

#include "bacpcom.h"
#include "pppmp.h"
#include "pppbap.h"
#include "bapprot.h"
#include "lcpdefs.h"


#endif  /* __PPP_BAPCOM_H__ */
