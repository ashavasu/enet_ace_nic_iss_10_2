/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bapset.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This files contains the State Event Table used  
 *             by the BAP module.
 *
 *******************************************************************/
#ifndef __PPP_BAPSET_H__
#define __PPP_BAPSET_H__

#define		BAP_MAX_SIZE	22
#define		BAP_MAX_EVENTS	10

/*********************************************************************/
/*          CONSTANTS  USED BY THE BAPSET  MODULE                      */
/*********************************************************************/

#define			A0				0
#define			A1				1
#define			A2				2
#define			A3				3
#define			A4				4
#define			A5				5
#define			A6				6
#define			A7				7
#define			A8				8
#define			A9				9
#define			A10				10
#define			A11				11
#define			A12				12
#define			A13				13
#define			A14				14
#define			A15				15
#define			A16				16
#define			A17				17
#define			A18				18
#define			A19				19
#define			A20				20
#define			A21				21

UINT1 (*BAPActionProcedure [BAP_MAX_SIZE]) (tPendingRequest *pRequest) = {
	/*	A0	*/	NULL,
	/*	A1	*/	NULL,
	/*	A2	*/	BAPSETDestroyReqEntryOnFailure,
	/*	A3	*/	BAPSETDestroyOnFullNakResponse,
	/*	A4	*/	BAPSETDestroyOnRejectResponse,

	/*	A5	*/	BAPSETResendCallRequest,
	/*	A6	*/	BAPSETResendCallBackRequest,
	/*	A7	*/	BAPSETResendLinkDropRequest,
	/*	A8	*/	BAPSETResendStatusIndication,

	/*	A9	*/	BAPSETMoveToCallAckRxd,
	/*	A10	*/	BAPSETMoveToCallBackAckRxd,
	/*	A11	*/	BAPSETInformRMToDropLink,

	/*	A12	*/	BAPSETMoveToCNakRxdOrDestroy,
	/*	A13	*/	BAPSETMoveToCBNakRxdOrDestroy,
	/*	A14	*/	BAPSETMoveToLDNakRxdOrDestroy,

	/*	A15	*/	BAPSETSendStatusRespAndDestroy,
	/*	A16	*/	BAPSETSendStatusResp,

	/*	A17	*/	BAPSETLeaveCStatSentOrDestroy,
	/*	A18	*/	BAPSETLeaveCBStatSentOrDestroy,

	/*	A19	*/	BAPSETMoveToCallStatusSent,
	/*	A20	*/	BAPSETMoveToCallBackStatusSent,

	/*	A21	*/	BAPSETIdleWait
};

UINT1 BAPSETable [BAP_MAX_EVENTS][BAP_MAX_STATES] = {
/*
States:    Call Call Link Call Call Call Call Link Call Call Call Call 
  |----->  Req  Back Drop Ack  Back Nak  Back Drop Stat Back Resp Back
           Sent Req  Req  Rxd  Ack  Rxd  Nak  Nak  Sent Stat Sent Resp
                Sent Sent      Rxd       Rxd  Rxd       Sent      Sent
Events: 
____________________________________________________________________
*/ 
/*RRA */ { A9,  A10, A11, A1,  A1,  A1,  A1,  A1,  A1,  A1,  A1,  A1 },
/*RRN */ { A12, A13, A14, A1,  A1,  A1,  A1,  A1,  A1,  A1,  A1,  A1 },
/*RRR */ { A4,  A4,  A1,  A1,  A1,  A1,  A1,  A1,  A1,  A1,  A1,  A1 },
/*RFN */ { A3,  A3,  A3,  A1,  A1,  A1,  A1,  A1,  A1,  A1,  A1,  A1 },

/*TO+ */ { A5,  A6,  A7, A21, A21,  A5,  A6,  A7,  A8,  A8, A21, A21 },
/*TO- */ { A2,  A2,  A2,  A2,  A2,  A2,  A2,  A2,  A2,  A2,  A2,  A2 },

/*RSI+*/ { A1,  A1,  A1,  A1,  A15, A1,  A1,  A1,  A1,  A1,  A15, A1 },
/*RSI-*/ { A1,  A1,  A1,  A1,  A16, A1,  A1,  A1,  A1,  A1,  A16, A1 },

/*RSR */ { A1,  A1,  A1,  A1,  A1,  A1,  A1,  A1,  A17, A18, A1,  A1 },

/*CALL*/ { A1,  A1,  A1,  A19, A1,  A1,  A1,  A1,  A1,  A1,  A1,  A20 }
/*Status*/
};


#endif  /* __PPP_BAPSET_H__ */
