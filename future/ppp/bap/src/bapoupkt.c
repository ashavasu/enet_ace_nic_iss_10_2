/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bapoupkt.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This files contains the packet construction related
 *             routines of BAP module.
 *
 *******************************************************************/
#include "bapcom.h"
#include "globexts.h"

/* Pkt construction Offset */
#define    RESP_OFFSET                4
#define    OPT_VAL_OFFSET            2
#define    REQUEST_HEADER_LEN        4
#define    RESPONSE_HEADER_LEN        5
#define    BAP_MAX_REASON_SIZE        40

/***********************************************************************
            Function Prototypes
************************************************************************/
VOID                BAPPrependBAPHeaderAndTxBAPPkt (tBAPInfo * pBAPInfo,
                                                    UINT1 ReqType, UINT1 Id,
                                                    UINT1 RespCode,
                                                    t_MSG_DESC * pPacket,
                                                    UINT2 PktLen);
UINT2               BAPAddLinkTypeOption (t_MSG_DESC * pPacket, UINT2 Offset,
                                          UINT2 LinkSpeed, UINT1 LinkType);
UINT2               BAPAddMultiplePhoneDeltaOptions (t_MSG_DESC * pPacket,
                                                     UINT2 Offset,
                                                     tPhNumbers * pPhoneNumbers,
                                                     tPhNum FirstLnkPhNum);
UINT1               BAPFindUniqueDigits (tPhNum PhNum, tPhNum FirstLnkPhNum);
UINT2               BAPAddPhoneDeltaSubOptions (t_MSG_DESC * pPacket,
                                                UINT2 Offset, tPhNum * pPhNum,
                                                UINT1 UniqueDigits);
UINT2               BAPAddNoPhoneNumberNeededOption (t_MSG_DESC * pPacket,
                                                     UINT2 Offset);
UINT2               BAPAddReasonOption (t_MSG_DESC * pPacket, UINT2 Offset,
                                        UINT1 ReasonCode);
UINT2               BAPAddLinkDiscrOption (t_MSG_DESC * pPacket, UINT2 Offset,
                                           UINT2 LinkDiscr);
UINT2               BAPAddCallStatusOption (t_MSG_DESC * pPacket, UINT2 Offset,
                                            UINT1 MsgCode, UINT1 Action);

/*********************************************************************
*    Function Name    :    BAPPrependBAPHeaderAndTxBAPPkt
*
*    Description        :
*                This function sends the BAP packet to the PPP module
*    after prepinding the BAP header (Type, Identifier, Length and Response 
*    Code (optional)) to BAP Data field.
*
*    Parameter(s)      :
*         pBAPInfo    -    Points to the tBAPInfo structure corresponds
*                        to the bundle over which the packet has to be
*                        sent. 
*            ReqType    -    Received request type. Possible values are :
*                        BAP_CALL_REQUEST, BAP_CALL_BACK_REQUEST, BAP_LINK_DROP_REQUEST,
*                        BAP_STATUS_INDICATION, BAP_CALL_RESPONSE, BAP_CALL_BACK_RESPONSE,
*                        BAP_LINK_DROP_RESPONSE, BAP_STATUS_RESPONSE.
*                Id    -    Identifier received along with the packet.
*        RespCode    -    Received response type. Possible values are :
*                        BAP_REQUEST_ACK, BAP_REQUEST_NAK, BAP_REQUEST_FULL_NAK,
*                        BAP_REQUEST_REJECT and BAP_REQUEST_TYPE (if not a response pkt.)
*             pPacket    -    Points to the incoming packet.
*            PktLen    -    Length of the packet (data portion alone).
*
*    Return Value     :    VOID
*********************************************************************/
VOID
BAPPrependBAPHeaderAndTxBAPPkt (tBAPInfo * pBAPInfo, UINT1 ReqType, UINT1 Id,
                                UINT1 RespCode, t_MSG_DESC * pPacket,
                                UINT2 PktLen)
{

    if (RespCode == BAP_REQUEST_TYPE)
    {
        PktLen += REQUEST_HEADER_LEN;
    }
    else
    {
        PktLen += RESPONSE_HEADER_LEN;
        ASSIGN1BYTE (pPacket, RESP_OFFSET, RespCode);
    }
    FILL_PKT_HEADER (pPacket, ReqType, Id, PktLen);

    /* Transmit the packet across the bundle interface */
    PPPRxPktFromHL (pBAPInfo->pBundleIf, pPacket, PktLen, BAP_PROTOCOL);

    return;
}

/*********************************************************************
*    Function Name :    BAPConstructAndTxResponse
*
*    Description   :
*                This function is called by internal routines of BAP
*    module to construct and send Status Response OR Responses other
*    than Ack (Nak, FullNak, Reject) for the peers request to add/delete
*   a link to/from bundle.
*
*    Parameter(s)  :
*         pBAPInfo    -    Points to the tBAPInfo structure corressponds
*                        to the bundle over which the packet has to be
*                        sent. 
*            ReqType    -    Received request type. Possible values are :
*                        BAP_CALL_REQUEST, BAP_CALL_BACK_REQUEST,
*                        BAP_LINK_DROP_REQUEST and BAP_STATUS_INDICATION.
*        RespType    -    Received response type. Possible values are :
*                        BAP_REQUEST_NAK, BAP_REQUEST_FULL_NAK, BAP_REQUEST_REJECT
*                Id    -    Identifier of the request packet.
*        ReasonCode    -    Reason code for sending an negative response.
*
*    Return Value : VOID
*********************************************************************/
VOID
BAPConstructAndTxResponse (tBAPInfo * pBAPInfo, UINT1 ReqType, UINT1 RespType,
                           UINT1 Id, UINT1 ReasonCode)
{
    UINT2               Length;
    t_MSG_DESC         *pPacket;

    /* Allocate buffer for constructing the response packet */
    if ((pPacket =
         (t_MSG_DESC *) ALLOCATE_BUFFER (MAX_PKT_SIZE,
                                         PPP_INFO_OFFSET)) == NULL)
    {
        PRINT_MEM_ALLOC_FAILURE;
        return;
    }

    Length = BAPAddReasonOption (pPacket, RESP_OFFSET + 1, ReasonCode);
    BAPPrependBAPHeaderAndTxBAPPkt (pBAPInfo, (UINT1) (ReqType + 1), Id,
                                    RespType, pPacket, Length);

    return;
}

/*********************************************************************
*    Function Name :    BAPConstructAndTxAckResponse
*    Description   :
*                This function is called by internal routines of BAP
*    module to construct and send Ack Response for the peers
*    request to add/delete a link to/from bundle.
*
*    Parameter(s)  :
*         pBAPInfo    -    Points to the tBAPInfo structure corressponds
*                        to the bundle over which the packet has to be
*                        sent. 
*         pReqBlock    -    Points to the ReqBlock structure which stores the
*                        information collected from the calling functions.
*            ReqType    -    Received request type. Possible values are :
*                        BAP_CALL_REQUEST, BAP_CALL_BACK_REQUEST, BAP_LINK_DROP_REQUEST.
*
*    Return Value     :    VOID
*********************************************************************/
VOID
BAPConstructAndTxAckResponse (tBAPInfo * pBAPInfo, tReqBlock * pReqBlock,
                              UINT1 ReqType)
{
    UINT2               Offset;
    t_MSG_DESC         *pPacket;

    /* Allocate buffer for constructing the ack packet */
    if ((pPacket =
         (t_MSG_DESC *) ALLOCATE_BUFFER (MAX_PKT_SIZE,
                                         PPP_INFO_OFFSET)) == NULL)
    {
        PRINT_MEM_ALLOC_FAILURE;
        return;
    }

    Offset = RESPONSE_HEADER_LEN;

    if (ReqType != BAP_LINK_DROP_REQUEST)
    {
        if (pReqBlock->Options.LinkType != 0)
        {
            Offset +=
                BAPAddLinkTypeOption (pPacket, Offset,
                                      pReqBlock->Options.LinkSpeed,
                                      pReqBlock->Options.LinkType);
        }
        if ((ReqType == BAP_CALL_REQUEST)
            && (pReqBlock->Options.IsPhNumNeeded == TRUE))
        {
            Offset +=
                BAPAddMultiplePhoneDeltaOptions (pPacket, Offset,
                                                 pReqBlock->Options.pPhNumbers,
                                                 pBAPInfo->LocFirstLnkPhNum);
        }
    }

    BAPPrependBAPHeaderAndTxBAPPkt (pBAPInfo, (UINT1) (ReqType + 1),
                                    pReqBlock->Id, BAP_REQUEST_ACK, pPacket,
                                    (UINT2) (Offset - RESPONSE_HEADER_LEN));

    return;
}

/*********************************************************************
*    Function Name    :    BAPConstructAndTxRequestPkts
*    Description        :
*                This function is called by internal routines of BAP 
*    module to construct and send a BAP request packet. The request 
*    retransmission timer will be started in this routine after sending the
*    request.
*
*    Parameter(s)    :
*            ReqType    -    Received request type. Possible values are :
*                        BAP_CALL_REQUEST, BAP_CALL_BACK_REQUEST, BAP_LINK_DROP_REQUEST,
*                        BAP_STATUS_INDICATION.
*        pRequest    -    Points to the corresponding tPendingRequest structure.
*
*    Return Value    :    VOID
*********************************************************************/
VOID
BAPConstructAndTxRequestPkts (UINT1 ReqType, tPendingRequest * pRequest)
{
    UINT2               Offset;
    t_MSG_DESC         *pPacket;

    /* Allocate buffer for constructing the request packet */
    if ((pPacket =
         (t_MSG_DESC *) ALLOCATE_BUFFER (MAX_PKT_SIZE,
                                         PPP_INFO_OFFSET)) == NULL)
    {
        PRINT_MEM_ALLOC_FAILURE;
        return;
    }

    Offset = REQUEST_HEADER_LEN;

    switch (ReqType)
    {

        case BAP_CALL_REQUEST:
        case BAP_CALL_BACK_REQUEST:
            Offset +=
                BAPAddLinkTypeOption (pPacket, Offset,
                                      pRequest->Options.LinkSpeed,
                                      pRequest->Options.LinkType);
            if ((ReqType == BAP_CALL_REQUEST)
                && (pRequest->Options.IsPhNumNeeded == FALSE))
            {
                Offset += BAPAddNoPhoneNumberNeededOption (pPacket, Offset);
            }
            if (ReqType == BAP_CALL_BACK_REQUEST)
            {
                Offset +=
                    BAPAddMultiplePhoneDeltaOptions (pPacket, Offset,
                                                     pRequest->Options.
                                                     pPhNumbers,
                                                     pRequest->pBAPInfo->
                                                     LocFirstLnkPhNum);
            }
            break;

        case BAP_LINK_DROP_REQUEST:
            Offset +=
                BAPAddLinkDiscrOption (pPacket, Offset,
                                       pRequest->Options.LinkDiscr);
            break;

        case BAP_STATUS_INDICATION:
            Offset +=
                BAPAddCallStatusOption (pPacket, Offset, pRequest->MsgCode,
                                        pRequest->LastActionSent);
            Offset +=
                BAPAddMultiplePhoneDeltaOptions (pPacket, Offset,
                                                 pRequest->Options.pPhNumbers,
                                                 pRequest->pBAPInfo->
                                                 RemFirstLnkPhNum);
            break;

        default:
            return;
    }

    BAPPrependBAPHeaderAndTxBAPPkt (pRequest->pBAPInfo, ReqType,
                                    pRequest->RequestHandle.Id,
                                    BAP_REQUEST_TYPE, pPacket,
                                    (UINT2) (Offset - REQUEST_HEADER_LEN));

    pRequest->RetransmitCounter--;

    pRequest->RetransmitTimer.Param1 = (UINT4) pRequest;
    PPPStartTimer (&pRequest->RetransmitTimer, BAP_REQUEST_TIMER,
                   pRequest->pBAPInfo->RetransmitTimeOutVal);

    return;
}

/*********************************************************************
*    Function Name    :    BAPAddLinkTypeOption
*    Description        :
*                This function is called by packet construction routines 
*    of BAP module to add Link-Type option to the outgoing packet.
*
*    Parameter(s)    :
*            pPacket    -    Points to the outgoing packet buffer.
*            Offset    -    Indicates the place in the outgoing buffer at which
*                        the option value is appended.
*        LinkSpeed    -    Desired bandwith of link to be added to the bundle.
*        LinkType    -    Desired Link-Type.
*
*    Return Value : Link-Type option length.
*********************************************************************/
UINT2
BAPAddLinkTypeOption (t_MSG_DESC * pPacket, UINT2 Offset, UINT2 LinkSpeed,
                      UINT1 LinkType)
{

    ASSIGN1BYTE (pPacket, Offset, BAP_LINK_TYPE);
    ASSIGN1BYTE (pPacket, Offset + OPT_LEN_OFFSET, BAP_LINK_TYPE_LEN);
    ASSIGN2BYTE (pPacket, Offset + OPT_VAL_OFFSET, LinkSpeed);
    ASSIGN1BYTE (pPacket, Offset + OPT_VAL_OFFSET + 2, LinkType);

    return (BAP_LINK_TYPE_LEN);
}

/*********************************************************************
*    Function Name    :    BAPAddMultiplePhoneDeltaOptions
*
*    Description        :
*                This function is called by packet construction routines 
*    of BAP module to add multiple (if requested) Phone-Delta options
*    to the outgoing packet.
*
*    Parameter(s)    :
*            pPacket    -    Points to the outgoing packet buffer.
*            Offset    -    Indicates the place in the outgoing buffer at which
*                        the option value is appended.
*    pPhoneNumbers    -    Points to the phone number information structure. The
*                        structure may contain more than one phone number.
*    FirstLnkPhNum    -    The phone number of the first member link.
*
*    Return Value    :    Cumulative length of the added phone delta options.
*********************************************************************/
UINT2
BAPAddMultiplePhoneDeltaOptions (t_MSG_DESC * pPacket, UINT2 Offset,
                                 tPhNumbers * pPhoneNumbers,
                                 tPhNum FirstLnkPhNum)
{
    UINT1               Count, UniqueDigits;
    UINT2               Length, Len;

    Length = 0;
    if (pPhoneNumbers != NULL)
    {

        /*
           As there can be more than one phone number in the phone number
           information structure, multiple phone-delta options have to be
           added for each phone number.
         */
        for (Count = 0; Count < pPhoneNumbers->NumPhoneNumbers; Count++)
        {

            ASSIGN1BYTE (pPacket, Offset, BAP_PHONE_DELTA);

            /*  Get unique digits of phone number (wrt the first member link). */
            UniqueDigits =
                BAPFindUniqueDigits (pPhoneNumbers->PhoneNumbers[Count],
                                     FirstLnkPhNum);
            PPP_TRC1 (CONTROL_PLANE, "Unique Digits : %d", UniqueDigits);

            Len =
                (UINT2) (BAP_OPT_HDR_LEN + BAPAddPhoneDeltaSubOptions (pPacket,
                                                                       (UINT2)
                                                                       (Offset +
                                                                        OPT_VAL_OFFSET),
                                                                       &pPhoneNumbers->
                                                                       PhoneNumbers
                                                                       [Count],
                                                                       UniqueDigits));
            ASSIGN1BYTE (pPacket, Offset + OPT_LEN_OFFSET, (UINT1) Len);
            Length += Len;
            Offset += Len;
        }
    }
    return (Length);
}

/*********************************************************************
*    Function Name    :    BAPFindUniqueDigits
*    Description        :
*                This function is called by BAPAddMultiplePhoneDeltaOptions
*    routine to find the unique digits in the phone number with respect
*    to the phone number of the first member link.
*
*    Parameter(s)    :
*        PhNum        -    The phone number.
*    FirstLnkPhNum    -    The phone number of the first member link.
*
*    Return Value    :    Unique digits in the phone number.
*********************************************************************/
UINT1
BAPFindUniqueDigits (tPhNum PhNum, tPhNum FirstLnkPhNum)
{
    UINT1               Count;
    UINT1               Size1, Size2;

    if (FirstLnkPhNum.Size == 0)
    {
        return (PhNum.Size);
    }
    Size1 = (UINT1) (PhNum.Size - PhNum.SubAddrSize);
    Size2 = (UINT1) (FirstLnkPhNum.Size - FirstLnkPhNum.SubAddrSize);
    Count = 0;

    while ((Count < Size1) && (Count < Size2)
           && (PhNum.String[Count] == FirstLnkPhNum.String[Count]))
    {
        Count++;
    }

    return ((UINT1) (Size1 - Count));
}

/*********************************************************************
*    Function Name    :    BAPAddPhoneDeltaSubOptions
*    Description        :
*                This function is called by BAPAddMultiplePhoneDeltaOptions
*    routine to add Phone-Delta sub-options to the outgoing packet.
*
*    Parameter(s)    :
*            pPacket    -    Points to the outgoing packet buffer.
*            Offset    -    Indicates the place in the outgoing buffer at which
*                        the suboption is to be appended.
*            pPhNum    -    Points to the phone number.
*    UniqueDigits    -    Unique digits in the phone number.
*
*    Return Value : Cumulative length of the added phone delta sub-options.
*********************************************************************/
UINT2
BAPAddPhoneDeltaSubOptions (t_MSG_DESC * pPacket, UINT2 Offset, tPhNum * pPhNum,
                            UINT1 UniqueDigits)
{
    UINT2               InitOffset;

    InitOffset = Offset;

    /* Append Unique Digits Suboption */
    ASSIGN1BYTE (pPacket, Offset, BAP_UNIQUE_DIGITS);
    ASSIGN1BYTE (pPacket, Offset + OPT_LEN_OFFSET, BAP_UNIQUE_DIGITS_LEN);
    ASSIGN1BYTE (pPacket, Offset + OPT_VAL_OFFSET, UniqueDigits);
    Offset += BAP_UNIQUE_DIGITS_LEN;

    /* Append Subscriber Number Suboption */
    ASSIGN1BYTE (pPacket, Offset, BAP_SUBSCRIBER_NUMBER);
    ASSIGN1BYTE (pPacket, Offset + OPT_LEN_OFFSET, (UINT1) (UniqueDigits + 2));
    ASSIGNSTR (pPacket,
               pPhNum->String + (pPhNum->Size - pPhNum->SubAddrSize -
                                 UniqueDigits), Offset + OPT_VAL_OFFSET,
               UniqueDigits);
    Offset += UniqueDigits + 2;

    /* Append Pnone Number Sub Address Suboption, if required */
    if (pPhNum->SubAddrSize != 0)
    {
        ASSIGN1BYTE (pPacket, Offset, BAP_SUB_ADDRESS);
        ASSIGN1BYTE (pPacket, Offset + OPT_LEN_OFFSET,
                     (UINT1) (pPhNum->SubAddrSize + 2));
        ASSIGNSTR (pPacket,
                   pPhNum->String + (pPhNum->Size - pPhNum->SubAddrSize),
                   Offset + OPT_VAL_OFFSET, pPhNum->SubAddrSize);
        Offset += pPhNum->SubAddrSize + 2;
    }

    return ((UINT2) (Offset - InitOffset));
}

/*********************************************************************
*    Function Name    :    BAPAddNoPhoneNumberNeededOption
*    Description        :
*                This function is called by packet construction routines 
*    of BAP module to add No-Phone-Number-Needed option to the outgoing
*    packet.
*
*    Parameter(s)    :
*            pPacket    -    Points to the outgoing packet buffer.
*            Offset    -    Indicates the place in the outgoing buffer at which
*                        the option value is appended.
*
*    Return Value    :    No-Phone-Number-Needed option length.
*********************************************************************/
UINT2
BAPAddNoPhoneNumberNeededOption (t_MSG_DESC * pPacket, UINT2 Offset)
{

    ASSIGN1BYTE (pPacket, Offset, BAP_NO_PHONE_NUM_NEEDED);
    ASSIGN1BYTE (pPacket, Offset + OPT_LEN_OFFSET, BAP_NO_PHONE_NUM_NEEDED_LEN);

    return (BAP_NO_PHONE_NUM_NEEDED_LEN);
}

/*********************************************************************
*    Function Name    :    BAPAddReasonOption
*    Description        :
*                This function is called by packet construction routines 
*    of BAP module to add Reason option to the outgoing packet.
*
*    Parameter(s)    :
*            pPacket    -    Points to the outgoing packet buffer.
*            Offset    -    Indicates the place in the outgoing buffer at which
*                        the option value is appended.
*        ReasonCode    -   Reason code, if any.
*
*    Return Value    :    The option length.
*********************************************************************/
UINT2
BAPAddReasonOption (t_MSG_DESC * pPacket, UINT2 Offset, UINT1 ReasonCode)
{
    typedef struct Reason
    {
        UINT1               String[BAP_MAX_REASON_SIZE];
        UINT1               Length;
    }
    tReasonString;
    tReasonString       ReasonStrings[] = {
        {"Not allowed to send this request type", 37},    /*BAP_REASON_NOT_ALLOWED */
        {"Favored Peer Problem", 20},    /*BAP_REASON_FAVORED_PEER */
        {"Try some time later", 19},    /*BAP_REASON_BAP_RETRY */
        {"Extreme bandwidth reached", 25},    /*BAP_REASON_BW */
        {"Retransmitted Response", 22}    /*BAP_REASON_RETRANS */
    };

    UINT2               Length;

    Length = 0;
    if ((ReasonCode != BAP_NO_REASON) && (ReasonCode < BAP_MAX_REASONS))
    {
        Length = (UINT2) (ReasonStrings[ReasonCode].Length + BAP_OPT_HDR_LEN);
        ASSIGN1BYTE (pPacket, Offset, BAP_REASON);
        ASSIGN1BYTE (pPacket, Offset + OPT_LEN_OFFSET, (UINT1) Length);
        ASSIGNSTR (pPacket, ReasonStrings[ReasonCode].String,
                   Offset + OPT_VAL_OFFSET, Length - BAP_OPT_HDR_LEN);
    }

    return (Length);
}

/*********************************************************************
*    Function Name    :    BAPAddLinkDiscrOption
*    Description        :
*                This function is called by packet construction routines 
*    of BAP module to add Link-Dicriminator option to the outgoing packet.
*
*    Parameter(s)    :
*            pPacket    -    Points to the outgoing packet buffer.
*            Offset    -    Indicates the place in the outgoing buffer at which
*                        the option value is appended.
*        LinkDiscr    -    The Link-Discriminator value of the link to be dropped
*                        from the bundle.
*
*    Return Value    :    Link-Dicriminator option length.
*********************************************************************/
UINT2
BAPAddLinkDiscrOption (t_MSG_DESC * pPacket, UINT2 Offset, UINT2 LinkDiscr)
{

    ASSIGN1BYTE (pPacket, Offset, BAP_LINK_DISCR);
    ASSIGN1BYTE (pPacket, Offset + OPT_LEN_OFFSET, BAP_LINK_DISCR_LEN);
    ASSIGN2BYTE (pPacket, Offset + OPT_VAL_OFFSET, LinkDiscr);

    return (BAP_LINK_DISCR_LEN);
}

/*********************************************************************
*    Function Name    :    BAPAddCallStatusOption
*    Description        :
*                This function is called by packet construction routines 
*    of BAP module to add Call-Status option to the outgoing packet.
*
*    Parameter(s)    :
*            pPacket    -    Points to the outgoing packet buffer.
*            Offset    -    Indicates the place in the outgoing buffer at which
*                        the option value is appended.
*            MsgCode    -    Indicates the status of the call (might be a 
*                        Q.931 cause value).
*            Action    -    Indicates the action is taken after a call setup
*                        procedure.
*
*    Return Value    :    Call-Status option length.
*********************************************************************/
UINT2
BAPAddCallStatusOption (t_MSG_DESC * pPacket, UINT2 Offset, UINT1 MsgCode,
                        UINT1 Action)
{

    ASSIGN1BYTE (pPacket, Offset, BAP_CALL_STATUS);
    ASSIGN1BYTE (pPacket, Offset + OPT_LEN_OFFSET, BAP_CALL_STATUS_LEN);
    ASSIGN1BYTE (pPacket, Offset + OPT_VAL_OFFSET, MsgCode);
    ASSIGN1BYTE (pPacket, Offset + OPT_VAL_OFFSET + 1, Action);

    return (BAP_CALL_STATUS_LEN);
}
