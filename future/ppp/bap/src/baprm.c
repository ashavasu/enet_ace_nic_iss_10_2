/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: baprm.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This files contains the test routines of RM module.
 *
 *******************************************************************/

#include "bapcom.h"
#include "bapexts.h"
#include "lcpexts.h"
#include "lcpdefs.h"
#include "gsemdefs.h"
#include "baprm.h"
#include "globexts.h"

/**************************************************
 Global variable used by the test low level routines
 and other test stubs/drivers. 
***************************************************/

tRMInfo             RMInfo;
t_SLL               RMRequestDataBase;

VOID
RMInit (VOID)
{

    BZERO (&RMInfo, sizeof (tRMInfo));
    if ((RMInfo.ReqBlock.Options.pPhNumbers =
         (tPhNumbers *) ALLOC_STR ((RM_MAX_PH_NUMBERS - 1) * sizeof (tPhNum) +
                                   sizeof (tPhNumbers))) == NULL)
    {
        PRINT_MEM_ALLOC_FAILURE;
        return;
    }
    BZERO (RMInfo.ReqBlock.Options.pPhNumbers, sizeof (tPhNumbers));
    RMInfo.ReqBlock.Options.IsPhNumNeeded = TRUE;
    RMInfo.ReqBlock.ActionSent = BAP_RETRY;
    SLL_INIT (&RMRequestDataBase);
}

VOID
RMDelete (VOID)
{
    tRMReqEntry        *pReqEntry;

    FREE_STR (RMInfo.ReqBlock.Options.pPhNumbers);
    SLL_SCAN (&RMRequestDataBase, pReqEntry, tRMReqEntry *)
    {
        pReqEntry->pIf = NULL;
        /* makefile changes - incompatible pointer type */
        SLL_DELETE (&RMRequestDataBase, (t_SLL_NODE *) pReqEntry);
        FREE_STR (pReqEntry);
    }
}

VOID
RMCreateInitAndAddReqEntry (tPPPIf * pBundleIf, UINT1 Id, UINT1 Requester,
                            UINT1 ReqType, tPPPIf * pIf)
{
    tRMReqEntry        *pReqEntry;

    if ((pReqEntry = (tRMReqEntry *) ALLOC_STR (sizeof (tRMReqEntry))) == NULL)
    {
        PRINT_MEM_ALLOC_FAILURE;
        return;
    }
    pReqEntry->Id = Id;
    pReqEntry->Requester = Requester;
    pReqEntry->pIf = pIf;
    pReqEntry->pBundleIf = pBundleIf;
    pReqEntry->ReqType = ReqType;

    /* makefile changes - incompatible pointer type */
    SLL_ADD (&RMRequestDataBase, (t_SLL_NODE *) pReqEntry);

    return;
}

tRMReqEntry        *
RMGetReqEntry (tPPPIf * pBundleIf, UINT1 Id, UINT1 Requester)
{
    tRMReqEntry        *pReqEntry;
    tRMReqEntry        *pTempEntry;

    pReqEntry = NULL;
    SLL_SCAN (&RMRequestDataBase, pTempEntry, tRMReqEntry *)
    {
        if ((pTempEntry->pBundleIf->LinkInfo.IfID.porttype ==
             pBundleIf->LinkInfo.IfID.porttype) && (pTempEntry->Id == Id)
            && (pTempEntry->Requester == Requester))
        {
            pReqEntry = pTempEntry;
            break;
        }
    }
    return (pReqEntry);
}

VOID
RMCallInfoToMP (tPPPIf * pIf, UINT1 *pIsInitiator, tPhNum * pLocalPhNum,
                tPhNum * pRemotePhNum)
{
    tBAPInfo           *pBAPInfo;

    PPP_UNUSED (pIsInitiator);
    PPP_UNUSED (pLocalPhNum);
    PPP_UNUSED (pRemotePhNum);
    pBAPInfo =
        (tBAPInfo *) (pIf->MPInfo.MemberInfo.pBundlePtr->MPInfo.BundleInfo.
                      BWMProtocolInfo.pProtocolInfo);
    if (SLL_COUNT
        (&pIf->MPInfo.MemberInfo.pBundlePtr->MPInfo.BundleInfo.MemberList) == 0)
    {
        pIf->MPInfo.MemberInfo.pBundlePtr->MPInfo.BundleInfo.IsInitiator =
            RMInfo.IsInitiator;
    }
    if (pBAPInfo->LocFirstLnkPhNum.Size == 0)
    {
        MEMCPY (&pBAPInfo->LocFirstLnkPhNum, &RMInfo.LocFirstLnkPhNum,
                sizeof (tPhNum));
        PPP_TRC (CONTROL_PLANE, "Local First Link phone number is:");
        BAP_PRINT_STR (pBAPInfo->LocFirstLnkPhNum.String,
                       pBAPInfo->LocFirstLnkPhNum.Size);
    }
    if (pBAPInfo->RemFirstLnkPhNum.Size == 0)
    {
        MEMCPY (&pBAPInfo->RemFirstLnkPhNum, &RMInfo.RemFirstLnkPhNum,
                sizeof (tPhNum));
        PPP_TRC (CONTROL_PLANE, "Remote First Link phone number is:");
        BAP_PRINT_STR (pBAPInfo->RemFirstLnkPhNum.String,
                       pBAPInfo->LocFirstLnkPhNum.Size);
    }
    return;
}

INT1
RMVerifyPeerRequest (tPPPIf * pBundleIf, tReqHandle * pRequestHandle,
                     UINT1 RequestType, tBAPOpts * pOptions)
{
    INT4                PhNumSize;
    /* Makefile changes - unused variable */

    if (RMInfo.ReqBlock.RespCode == OK)
    {
        RMCreateInitAndAddReqEntry (pBundleIf, pRequestHandle->Id,
                                    BAP_REMOTE_PEER, RequestType, NULL);
        if ((RequestType == BAP_CALL_REQUEST)
            || (RequestType == BAP_CALL_BACK_REQUEST))
        {
            pOptions->LinkType = RMInfo.ReqBlock.Options.LinkType;
            pOptions->LinkSpeed = RMInfo.ReqBlock.Options.LinkSpeed;

            CfaIfmAddNewLinkToBundle (pBundleIf->LinkInfo.IfIndex);
        }
        if (RequestType == BAP_CALL_REQUEST)
        {
            if (pOptions->IsPhNumNeeded == TRUE)
            {
                PhNumSize =
                    (RMInfo.ReqBlock.Options.pPhNumbers->NumPhoneNumbers -
                     1) * sizeof (tPhNum) + sizeof (tPhNumbers);
                if ((pOptions->pPhNumbers =
                     (tPhNumbers *) ALLOC_STR (PhNumSize)) == NULL)
                {
                    PRINT_MEM_ALLOC_FAILURE;
                    return (BAP_NOT_ACCEPTABLE);
                }
                MEMCPY (pOptions->pPhNumbers,
                        RMInfo.ReqBlock.Options.pPhNumbers, PhNumSize);

            }
        }

    }
    return (RMInfo.ReqBlock.RespCode);
}

VOID
RMRequestStatusFromBAP (tPPPIf * pBundleIf, tReqHandle * pRequestHandle,
                        UINT1 Status, tBAPOpts * pOptions)
{
    /* makefile changes - unused variable */
    /* tPPPIfId            IfId;
       UINT1               IsUnique; */
    tPPPIf             *pIf;
    tRMReqEntry        *pReqEntry;

    PPP_UNUSED (pOptions);
    pReqEntry =
        RMGetReqEntry (pBundleIf, pRequestHandle->Id,
                       pRequestHandle->Requester);

    if (pReqEntry != NULL)
    {
        /* Makefile changes */
        if ((pReqEntry->ReqType != BAP_TX_LINK_DROP_REQUEST)
            && (Status == BAP_SUCCESS))
        {
            /* Create a new link and add to the bundle */
            CfaIfmAddNewLinkToBundle (pBundleIf->LinkInfo.IfIndex);
        }
        if ((pReqEntry->ReqType == BAP_TX_LINK_DROP_REQUEST)
            && ((Status == BAP_END_SUCCESS) || (Status == BAP_END_FAILURE)))
        {
            if ((pReqEntry->Requester == BAP_LOCAL_RM)
                && ((pIf = pReqEntry->pIf) != NULL))
            {
                PPP_TRC (CONTROL_PLANE, "Terminating the link.");
                CfaIfmDeletePppLink (pIf->LinkInfo.IfIndex);
            }
        }
        if (Status != BAP_SUCCESS)
        {
            pReqEntry->pIf = NULL;
            /* Makefile changes - incomptaible pointer type */
            SLL_DELETE (&RMRequestDataBase, (t_SLL_NODE *) pReqEntry);
            FREE_STR (pReqEntry);
            if (Status == BAP_END_FAILURE)
            {
                PPP_TRC (ALL_FAILURE, "Transaction Failed.");
            }
            if (Status == BAP_BW_EXTREME)
            {
                PPP_TRC (ALL_FAILURE,
                         "Bandwidth Extreme. Entry Deleted and RM informed of failure");
            }
            if (Status == BAP_FAILURE)
            {
                PPP_TRC (ALL_FAILURE,
                         "Remote Entity sent reject for the request. Entry Deleted and RM informed of failure");
            }
            if (Status == BAP_RETRY_LATER)
            {
                PPP_TRC (ALL_FAILURE,
                         "Request Unsuccessful. Entry Deleted and RM informed about the Failure.");
            }
        }
        else
        {
            PPP_TRC (EVENT_TRC, "Recd. Ack from the Peer.");
        }
    }
    else
    {
        PPP_TRC (CONTROL_PLANE,
                 "We will appreciate if u enter correct id/requester pair.");
    }

    return;
}
