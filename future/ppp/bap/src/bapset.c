/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bapset.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the action procedures of 
 *             BAPSET module.
 *
 *******************************************************************/

#include "bapcom.h"
#include "bapexts.h"
#include "globexts.h"

UINT1               BAPIsRetryRequestActionTaken (tPendingRequest * pRequest);

/**********************************************************************
*    Function Name    :    BAPSETIllegalState
*
*    Description        :
*                 This procedure is called to process an illegal state transition.
*    It logs an error messages and returns back without any state transition.
*
*    Parameter(s)    :
*        pRequest    -    Points to the tPendingRequest structure.
*
*    Return Value    :    Current State of the pending request.
*********************************************************************/

/**********************************************************************
*    Function Name    :    BAPSETDestroyReqEntryOnFailure
*    Description        :
*                 This procedure is called when a BAP_TO_MINUS event is received
*                in any of the SET states. It indicates the calling function
*                to destroy the request entry from the list of pending
*                requests, and inform RM of the failed transaction status.
*    Parameter(s)    :
*        pRequest    -    Points to the tPendingRequest structure.
*    Return Value    :    BAP_DESTROY
*********************************************************************/
UINT1
BAPSETDestroyReqEntryOnFailure (tPendingRequest * pRequest)
{
    BAPInformRequestStatusToRM (pRequest->pBAPInfo->pBundleIf,
                                &pRequest->RequestHandle, BAP_END_FAILURE,
                                &pRequest->Options);
    return (BAP_DESTROY);
}

/**********************************************************************
*    Function Name    :    BAPSETDestroyOnFullNakResponse
*
*    Description        :
*                 This procedure is called when a Full-Nak-Response is received 
*                in any of the BAP_CALL_REQ_SENT, BAP_CALL_BACK_REQ_SENT or
*                BAP_LINK_DROP_REQ_SENT state. It informs RM that a Response-Full-Nak
*                is received for his previously made Request to add/drop a link
*                to/from the bundle.
*
*    Parameter(s)    :
*        pRequest    -    Points to the tPendingRequest structure.
*
*    Return Value    :    BAP_DESTROY.
*********************************************************************/
UINT1
BAPSETDestroyOnFullNakResponse (tPendingRequest * pRequest)
{
    BAPInformRequestStatusToRM (pRequest->pBAPInfo->pBundleIf,
                                &pRequest->RequestHandle, BAP_BW_EXTREME,
                                &pRequest->Options);
    return (BAP_DESTROY);
}

/**********************************************************************
*    Function Name    :    BAPSETDestroyOnRejectResponse
*
*    Description        :
*                 This procedure is called when a Reject-Response is received 
*                in any of the BAP_CALL_REQ_SENT or BAP_CALL_BACK_REQ_SENT state.
*                It informs RM that a Response-Rej is received for his
*                previously made Request to add a link to the bundle.
*
*    Parameter(s)    :
*        pRequest    -    Points to the tPendingRequest structure.
*
*    Return Value    :    BAP_DESTROY.
*********************************************************************/
UINT1
BAPSETDestroyOnRejectResponse (tPendingRequest * pRequest)
{
    BAPInformRequestStatusToRM (pRequest->pBAPInfo->pBundleIf,
                                &pRequest->RequestHandle, BAP_FAILURE,
                                &pRequest->Options);
    return (BAP_DESTROY);
}

/**********************************************************************
*    Function Name    :    BAPSETResendCallRequest
*
*    Description        :
*                 This procedure is called to retransmit a Call-Request
*                packet.
*
*    Parameter(s)    :
*        pRequest    -    Points to the tPendingRequest structure.
*
*    Return Value    :    BAP_CALL_REQ_SENT irrespective of the current state
*                        of the pending request.
*********************************************************************/
UINT1
BAPSETResendCallRequest (tPendingRequest * pRequest)
{
    BAPConstructAndTxRequestPkts (BAP_CALL_REQUEST, pRequest);
    return (BAP_CALL_REQ_SENT);
}

/**********************************************************************
*    Function Name    :    BAPSETResendCallBackRequest
*    Description        :
*                 This procedure is called to retransmit a Call-Back-Request
*                packet.
*    Parameter(s)    :
*        pRequest    -    Points to the tPendingRequest structure.
*    Return Value    :    BAP_CALL_BACK_REQ_SENT irrespective of the current state
*                        of the pending request.
*********************************************************************/
UINT1
BAPSETResendCallBackRequest (tPendingRequest * pRequest)
{
    BAPConstructAndTxRequestPkts (BAP_CALL_BACK_REQUEST, pRequest);
    return (BAP_CALL_BACK_REQ_SENT);
}

/**********************************************************************
*    Function Name    :    BAPSETResendLinkDropRequest
*    Description        :
*                 This procedure is called to retransmit a Link-Drop-Request
*                packet.
*    Parameter(s)    :
*        pRequest    -    Points to the tPendingRequest structure.
*    Return Value    :    BAP_LINK_DROP_REQ_SENT irrespective of the current state
*                        of the pending request.
*********************************************************************/
UINT1
BAPSETResendLinkDropRequest (tPendingRequest * pRequest)
{
    BAPConstructAndTxRequestPkts (BAP_LINK_DROP_REQUEST, pRequest);
    return (BAP_LINK_DROP_REQ_SENT);
}

/**********************************************************************
*    Function Name    :    BAPSETResendStatusIndication
*    Description        :
*                 This procedure is called to retransmit a Status-Indication 
*                packet.
*    Parameter(s)    :
*        pRequest    -    Points to the tPendingRequest structure.
*    Return Value    :    Current State of the pending request.
*********************************************************************/
UINT1
BAPSETResendStatusIndication (tPendingRequest * pRequest)
{
    BAPConstructAndTxRequestPkts (BAP_STATUS_INDICATION, pRequest);
    return (pRequest->State);
}

/**********************************************************************
*    Function Name    :    BAPSETMoveToCallAckRxd
*    Description        :
*                 This procedure is called when there is a BAP_RRA event in the 
*                BAP_CALL_REQ_SENT state. It informs RM that a Response-Ack is
*                received for the previously made Call-Request by him.
*    Parameter(s)    :
*        pRequest    -    Points to the tPendingRequest structure.
*    Return Value    :    BAP_CALL_ACK_RXD.
*********************************************************************/
UINT1
BAPSETMoveToCallAckRxd (tPendingRequest * pRequest)
{
    PPPStopTimer (&pRequest->RetransmitTimer);
    BAPInformRequestStatusToRM (pRequest->pBAPInfo->pBundleIf,
                                &pRequest->RequestHandle, BAP_SUCCESS,
                                &pRequest->Options);
    pRequest->RetransmitTimer.Param1 = (UINT4) pRequest;
    PPPStartTimer (&pRequest->RetransmitTimer, BAP_REQUEST_TIMER,
                   pRequest->pBAPInfo->RetransmitTimeOutVal);
    pRequest->RetransmitCounter =
        (UINT2) (pRequest->pBAPInfo->MaxRetransmits * BAP_RESP_WAIT_FACTOR);
    return (BAP_CALL_ACK_RXD);
}

/**********************************************************************
*    Function Name    :    BAPSETMoveToCallBackAckRxd
*    Description        :
*                 This procedure is called when there is a BAP_RRA event in the 
*                BAP_CALL_BACK_REQ_SENT state. It informs RM that a Response-Ack is
*                received for the previously made Call-Back-Request by him.
*    Parameter(s)    :
*        pRequest    -    Points to the tPendingRequest structure.
*    Return Value    :    BAP_CALL_BACK_ACK_RXD.
*********************************************************************/
UINT1
BAPSETMoveToCallBackAckRxd (tPendingRequest * pRequest)
{
    PPPStopTimer (&pRequest->RetransmitTimer);
    BAPInformRequestStatusToRM (pRequest->pBAPInfo->pBundleIf,
                                &pRequest->RequestHandle, BAP_SUCCESS,
                                &pRequest->Options);
    pRequest->RetransmitTimer.Param1 = (UINT4) pRequest;
    PPPStartTimer (&pRequest->RetransmitTimer, BAP_REQUEST_TIMER,
                   pRequest->pBAPInfo->RetransmitTimeOutVal);
    pRequest->RetransmitCounter =
        (UINT2) (pRequest->pBAPInfo->MaxRetransmits * BAP_RESP_WAIT_FACTOR);
    return (BAP_CALL_BACK_ACK_RXD);
}

/**********************************************************************
*    Function Name    :    BAPSETInformRMToDropLink
*    Description        :
*                 This procedure is called when there is a BAP_RRA event in the 
*                BAP_LINK_DROP_REQ_SENT state. It informs RM that a Response-Ack is
*                received for the previously made Link-Drop-Request by him.
*    Parameter(s)    :
*        pRequest    -    Points to the tPendingRequest structure.
*    Return Value    :    BAP_DESTROY.
*********************************************************************/
UINT1
BAPSETInformRMToDropLink (tPendingRequest * pRequest)
{
    PPPStopTimer (&pRequest->RetransmitTimer);
    BAPInformRequestStatusToRM (pRequest->pBAPInfo->pBundleIf,
                                &pRequest->RequestHandle, BAP_END_SUCCESS,
                                &pRequest->Options);
    pRequest->pBAPInfo->Stats.Outgoing.LinkDropReqsAcked++;
    return (BAP_DESTROY);
}

/**********************************************************************
*    Function Name    :    BAPSETMoveToCallStatusSent
*    Description        :
*                 This procedure is called to transmit a Status-Indication 
*                packet when there is a RX_BAP_CALL_STATUS event in the 
*                BAP_CALL_ACK_RXD state.
*    Parameter(s)    :
*        pRequest    -    Points to the tPendingRequest structure.
*    Return Value    :    BAP_CALL_STATUS_SENT.
*********************************************************************/
UINT1
BAPSETMoveToCallStatusSent (tPendingRequest * pRequest)
{
    PPPStopTimer (&pRequest->RetransmitTimer);
    pRequest->RetransmitCounter = pRequest->pBAPInfo->MaxRetransmits;
    BAPConstructAndTxRequestPkts (BAP_STATUS_INDICATION, pRequest);
    return (BAP_CALL_STATUS_SENT);
}

/**********************************************************************
*    Function Name    :    BAPSETMoveToCallBackStatusSent
*    Description        :
*                 This procedure is called to transmit a Status-Indication 
*                packet when there is a RX_BAP_CALL_STATUS event in the 
*                BAP_CALL_BACK_RESP_SENT state.
*    Parameter(s)    :
*        pRequest    -    Points to the tPendingRequest structure.
*    Return Value    :    BAP_CALL_BACK_STATUS_SENT.
*********************************************************************/
UINT1
BAPSETMoveToCallBackStatusSent (tPendingRequest * pRequest)
{
    PPPStopTimer (&pRequest->RetransmitTimer);
    pRequest->RetransmitCounter = pRequest->pBAPInfo->MaxRetransmits;
    BAPConstructAndTxRequestPkts (BAP_STATUS_INDICATION, pRequest);
    return (BAP_CALL_BACK_STATUS_SENT);
}

/**********************************************************************
*    Function Name    :    BAPSETSendStatusRespAndDestroy
*    Description        :
*                 This procedure is called when there is a RSI_PLUS
*                (i.e., LastActionSent field of the request structure is
*                set to BAP_NO_RETRY) event in the BAP_CALL_RESP_SENT or in the
*                BAP_CALL_BACK_ACK_RXD state. If the call status code received
*                in the last received Status-Indication packet is BAP_SUCCESS_CODE,
*                which indicates the successful completion of the transaction,
*                otherwise the transaction failed. In any of the above
*                cases RM is informed about the final status of the 
*                transaction and a Status-Response is send.
*    Parameter(s)    :
*        pRequest    -    Points to the tPendingRequest structure.
*    Return Value    :    BAP_DESTROY.
*********************************************************************/
UINT1
BAPSETSendStatusRespAndDestroy (tPendingRequest * pRequest)
{

    UINT1               RespToRM;

    /* Inform RM about the transaction's final status */
    RespToRM =
        (pRequest->MsgCode ==
         BAP_SUCCESS_CODE) ? BAP_END_SUCCESS : BAP_END_FAILURE;
    BAPConstructAndTxResponse (pRequest->pBAPInfo, BAP_STATUS_INDICATION,
                               BAP_REQUEST_ACK, pRequest->RequestHandle.Id,
                               BAP_NO_REASON);
    BAPInformRequestStatusToRM (pRequest->pBAPInfo->pBundleIf,
                                &pRequest->RequestHandle, RespToRM,
                                &pRequest->Options);
    if (RespToRM == BAP_END_SUCCESS)
    {

        (pRequest->State ==
         BAP_CALL_BACK_ACK_RXD) ? pRequest->pBAPInfo->Stats.Outgoing.
CallBackReqsAcked++ : pRequest->pBAPInfo->Stats.Incoming.CallReqsAcked++;
    }
    return (BAP_DESTROY);
}

/**********************************************************************
*    Function Name    :    BAPSETSendStatusResp
*    Description        :
*                 This procedure is called when there is a RSI_MINUS
*                (i.e., LastActionSent field of the request structure is
*                set to BAP_RETRY) event in the BAP_CALL_RESP_SENT or in the
*                BAP_CALL_BACK_ACK_RXD state. It sends a Status-Response packet
*                and waits passively in the same state for transaction to
*                get over.
*    Parameter(s)    :
*        pRequest    -    Points to the tPendingRequest structure.
*    Return Value    :    Current State of the pending request.
*********************************************************************/
UINT1
BAPSETSendStatusResp (tPendingRequest * pRequest)
{
    PPPStopTimer (&pRequest->RetransmitTimer);
    BAPConstructAndTxResponse (pRequest->pBAPInfo, BAP_STATUS_INDICATION,
                               BAP_REQUEST_ACK, pRequest->RequestHandle.Id,
                               BAP_NO_REASON);
    pRequest->RetransmitTimer.Param1 = (UINT4) pRequest;
    PPPStartTimer (&pRequest->RetransmitTimer, BAP_REQUEST_TIMER,
                   pRequest->pBAPInfo->RetransmitTimeOutVal);
    pRequest->RetransmitCounter =
        (UINT2) (pRequest->pBAPInfo->MaxRetransmits * BAP_RESP_WAIT_FACTOR);
    return (pRequest->State);
}

/**********************************************************************
*    Function Name    :    BAPSETLeaveCStatSentOrDestroy
*    Description        :
*                 This procedure is called when there is a RSR
*                event in the BAP_CALL_STATUS_SENT. If the last sent Status-
*                Indication packet was sent with the action code set to
*                BAP_NO_RETRY, which indicates transaction is complete (either
*                failed or succeded), and result is passed to RM. Otherwise,
*                (the BAP_RETRY case) it indicates that RM is retrying the 
*                establish the Call so move to BAP_CALL_ACK_RXD state.
*    Parameter(s)    :
*        pRequest    -    Points to the tPendingRequest structure.
*    Return Value    :    BAP_DESTROY, if the LastActionSent field of the
*                        request structure is set to BAP_NO_RETRY,
*                        BAP_CALL_ACK_RXD, otherwise.
*********************************************************************/
UINT1
BAPSETLeaveCStatSentOrDestroy (tPendingRequest * pRequest)
{
    UINT1               State, RespToRM;

    if (pRequest->LastActionSent == BAP_NO_RETRY)
    {
        /* Inform RM about the transaction's final status */
        RespToRM =
            (pRequest->MsgCode ==
             BAP_SUCCESS_CODE) ? BAP_END_SUCCESS : BAP_END_FAILURE;
        BAPInformRequestStatusToRM (pRequest->pBAPInfo->pBundleIf,
                                    &pRequest->RequestHandle, RespToRM,
                                    &pRequest->Options);
        if (RespToRM == BAP_END_SUCCESS)
        {
            pRequest->pBAPInfo->Stats.Outgoing.CallReqsAcked++;
        }
        State = BAP_DESTROY;
    }
    else
    {
        PPPStopTimer (&pRequest->RetransmitTimer);
        pRequest->RetransmitTimer.Param1 = (UINT4) pRequest;
        PPPStartTimer (&pRequest->RetransmitTimer, BAP_REQUEST_TIMER,
                       pRequest->pBAPInfo->RetransmitTimeOutVal);
        pRequest->RetransmitCounter =
            (UINT2) (pRequest->pBAPInfo->MaxRetransmits * BAP_RESP_WAIT_FACTOR);
        State = BAP_CALL_ACK_RXD;
    }

    return (State);
}

/**********************************************************************
*    Function Name    :    BAPSETLeaveCBStatSentOrDestroy
*    Description        :
*                 This procedure is called when there is a RSR
*                event in the BAP_CALL_BACK_STATUS_SENT. If the last sent Status-
*                Indication packet was sent with the action code set to
*                BAP_NO_RETRY, which indicates transaction is complete (either
*                failed or succeded), and result is passed to RM. Otherwise,
*                (the BAP_RETRY case) it indicates that RM is retrying the 
*                establish the Call so move to BAP_CALL_BACK_RESP_SENT state.
*    Parameter(s)    :
*        pRequest    -    Points to the tPendingRequest structure.
*    Return Value    :    BAP_DESTROY, if the LastActionSent field of the
*                        request structure is set to BAP_NO_RETRY,
*                        BAP_CALL_BACK_RESP_SENT, otherwise.
*********************************************************************/
UINT1
BAPSETLeaveCBStatSentOrDestroy (tPendingRequest * pRequest)
{
    UINT1               State, RespToRM;

    if (pRequest->LastActionSent == BAP_NO_RETRY)
    {
        /* Inform RM about the transaction's final status */
        RespToRM =
            (pRequest->MsgCode ==
             BAP_SUCCESS_CODE) ? BAP_END_SUCCESS : BAP_END_FAILURE;
        BAPInformRequestStatusToRM (pRequest->pBAPInfo->pBundleIf,
                                    &pRequest->RequestHandle, RespToRM,
                                    &pRequest->Options);
        if (RespToRM == BAP_END_SUCCESS)
        {
            pRequest->pBAPInfo->Stats.Incoming.CallBackReqsAcked++;
        }
        State = BAP_DESTROY;
    }
    else
    {
        PPPStopTimer (&pRequest->RetransmitTimer);
        pRequest->RetransmitTimer.Param1 = (UINT4) pRequest;
        PPPStartTimer (&pRequest->RetransmitTimer, BAP_REQUEST_TIMER,
                       pRequest->pBAPInfo->RetransmitTimeOutVal);
        pRequest->RetransmitCounter =
            (UINT2) (pRequest->pBAPInfo->MaxRetransmits * BAP_RESP_WAIT_FACTOR);
        State = BAP_CALL_BACK_RESP_SENT;
    }

    return (State);
}

/**********************************************************************
*    Function Name    :    BAPSETMoveToCNakRxdOrDestroy
*    Description        :
*                 This procedure is called when there is a RRN event in the 
*                BAP_CALL_REQ_SENT state. If the implementation is configured
*                to retry the request after the configurable period of time,
*                it starts the request timer and moves to CallNakRxdState.
*                Otherwise, reception of Nak-Response indicates failure
*                of the transaction and RM is informed of the same.
*    Parameter(s)    :
*        pRequest    -    Points to the tPendingRequest structure.
*    Return Value    :    BAP_CALL_NAK_RXD, if the implementation is configured
*                        to retry the request,
*                        BAP_DESTROY, otherwise.
*********************************************************************/
UINT1
BAPSETMoveToCNakRxdOrDestroy (tPendingRequest * pRequest)
{
    if (BAPIsRetryRequestActionTaken (pRequest) == PPP_YES)
    {
        return (BAP_CALL_NAK_RXD);
    }
    return (BAP_DESTROY);
}

/**********************************************************************
*    Function Name    :    BAPSETMoveToCBNakRxdOrDestroy
*    Description        :
*                 This procedure is called when there is a RRN event in the 
*                BAP_CALL_BACK_REQ_SENT state. If the implementation is configured
*                to retry the request after the configurable period of time,
*                it starts the request timer and moves to CallBAckNakRxdState.
*                Otherwise, reception of Nak-Response indicates failure
*                of the transaction and RM is informed of the same.
*    Parameter(s)    :
*        pRequest    -    Points to the tPendingRequest structure.
*    Return Value    :    BAP_CALL_BACK_NAK_RXD, if the implementation is configured
*                        to retry the request,
*                        BAP_DESTROY, otherwise.
*********************************************************************/
UINT1
BAPSETMoveToCBNakRxdOrDestroy (tPendingRequest * pRequest)
{
    if (BAPIsRetryRequestActionTaken (pRequest) == PPP_YES)
    {
        return (BAP_CALL_BACK_NAK_RXD);
    }
    return (BAP_DESTROY);
}

/**********************************************************************
*    Function Name    :    BAPSETMoveToLDNakRxdOrDestroy
*    Description        :
*                 This procedure is called when there is a RRN event in the 
*                BAP_LINK_DROP_REQ_SENT state. If the implementation is configured
*                to retry the request after the configurable period of time,
*                it starts the request timer and moves to LinkDropNakRxdState.
*                Otherwise, reception of Nak-Response indicates failure
*                of the transaction and RM is informed of the same.
*    Parameter(s)    :
*        pRequest    -    Points to the tPendingRequest structure.
*    Return Value    :    BAP_LINK_DROP_NAK_RXD, if the implementation is configured
*                        to retry the request,
*                        BAP_DESTROY, otherwise.
*********************************************************************/
UINT1
BAPSETMoveToLDNakRxdOrDestroy (tPendingRequest * pRequest)
{
    if (BAPIsRetryRequestActionTaken (pRequest) == PPP_YES)
    {
        return (BAP_LINK_DROP_NAK_RXD);
    }
    return (BAP_DESTROY);
}

/**********************************************************************
*    Function Name    :    BAPSETIdleWait
*    Description        :
*                 This procedure is called when there is a TO_PLUS event in the 
*                BAP_CALL_ACK_RXD, BAP_CALL_BACK_ACK_RXD, BAP_CALL_RESP_SENT or
*                BAP_CALL_BACK_RESP_SENT state. The pending request entry wait
*                idly in the same state.
*    Parameter(s)    :
*        pRequest    -    Points to the tPendingRequest structure.
*    Return Value    :    Current State of the pending request.
*********************************************************************/
UINT1
BAPSETIdleWait (tPendingRequest * pRequest)
{
    pRequest->RetransmitCounter--;
    pRequest->RetransmitTimer.Param1 = (UINT4) pRequest;
    PPPStartTimer (&pRequest->RetransmitTimer, BAP_REQUEST_TIMER,
                   pRequest->pBAPInfo->RetransmitTimeOutVal);
    return (pRequest->State);
}

/*********************************************************************
*    Function Name    :    BAPIsRetryRequestActionTaken
*
*    Description        :
*            This function is called by BAP SET routines when a Request-Nak
*            response is received from the peer in the response of a BAP 
*            request being sent to negotiate add/drop of a link to/from the 
*            bundle. If the implementation is configured to retry the request
*            after the configurable period of time, it starts the retry
*            request timer. Otherwise, reception of Nak-Response indicates
*            failure of the transaction and RM is informed of the same.
*
*    Parameter(s)    :
*        pRequest    -    Points to the corresponding pending request.
* 
*    Return Value    :    PPP_YES, if the implementation is configured to retry 
*                        the request,
*                        PPP_NO, otherwise.
*********************************************************************/
UINT1
BAPIsRetryRequestActionTaken (tPendingRequest * pRequest)
{

    if (pRequest->pBAPInfo->RetryTimeOutVal != 0)
    {
        /* Implementation is configured to retry the request */

        PPPStopTimer (&pRequest->RetransmitTimer);
        pRequest->RetransmitTimer.Param1 = (UINT4) pRequest;
        PPPStartTimer (&pRequest->RetransmitTimer, BAP_REQUEST_TIMER,
                       pRequest->pBAPInfo->RetryTimeOutVal);
        pRequest->RetransmitCounter = pRequest->pBAPInfo->MaxRetransmits;
        return (PPP_YES);
    }

    /* Inform RM about the transaction's failure */
    BAPInformRequestStatusToRM (pRequest->pBAPInfo->pBundleIf,
                                &pRequest->RequestHandle, BAP_RETRY_LATER,
                                &pRequest->Options);
    return (PPP_NO);
}
