/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bapwrap.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This files contains the wrapper routines for the 
 *             BAP interface procedures.
 *
 *******************************************************************/
#include "bapcom.h"
#include "bapexts.h"
#include "gsemdefs.h"
#include "globexts.h"

/*********************************************************************
*    Function Name    :    PPPBAPAddLinkToBundle
*
*    Description    : Invoked by the RM to request the BAP module 
*                         to negotiate the addition of a link to the specified
*                         bundle.
*
*    Parameter(s)    : 
*
*       u2BundleIfIndex - Interface index of the bundle to which
*                         a new link is to be added.
*
*         pRequestHandle    - Pointer to unique identifier of the request,
*                         assigned by the BAP module. Memory should be
*                         allocated for this variable by the calling module. 
*
*       u1ReqType           - Specifies type of request made. The possible 
*                         values are    :
*                          BAP_TX_CALL_REQUEST for Call Request type,
*                              BAP_TX_CALL_BACK_REQUEST for Call-Back Request
*                              type.
*
*    pBAPOptions          - Points to the BAP options structure.
*
*    Return Value :     OK on success,
*                         NOT_OK otherwise (memory allocation failure, other
*                                               internal errors)
*********************************************************************/
INT1
PPPBAPAddLinkToBundle (UINT2 u2BundleIfIndex,
                       tReqHandle * pRequestHandle,
                       UINT1 u1ReqType, tBAPOpts * pBAPOptions)
{
    INT1                i1RetVal;
    tPPPIf             *pBundleIf;

    pBundleIf = SNMPGetBundleIfptr (u2BundleIfIndex);

    i1RetVal =
        BAPAddLinkToBundle (pBundleIf, pRequestHandle, u1ReqType, pBAPOptions);

    return i1RetVal;
}

/*********************************************************************
*    Function Name    :    PPPBAPDeleteLinkFromBundle
*
*    Description        : Invoked by the RM to request the BAP module 
*                   to negotiate the deletion of a link 
*                   from the specified bundle.
*
*
*    Parameter(s)    :
*        u2BundleIfIndex -    Interface Index of the MP bundle from which
*                       a link is to be deleted.
*
*        u2MemberIfIndex  - Interface Index of the link to be deleted 
*                        from the bundle.
*                  *
*        pReqHandle - Pointer to unique identifier of the request, assigned
*                         by the BAP module. Memory should be allocated for this
*                         variable by the calling module. 
*
*    Return Value    :    OK on success,
*                           NOT_OK otherwise (memory allocation failure, other
*                                              internal errors)
*********************************************************************/
INT1
PPPBAPDeleteLinkFromBundle (UINT2 u2BundleIfIndex,
                            UINT2 u2MemberIfIndex, tReqHandle * pRequestHandle)
{
    INT1                i1RetVal;
    tPPPIf             *pBundleIf, *pMemberIf;

    pBundleIf = SNMPGetBundleIfptr (u2BundleIfIndex);
    pMemberIf = SNMPGetMemberIfptr (u2MemberIfIndex);

    i1RetVal = BAPDeleteLinkFromBundle (pBundleIf, pMemberIf, pRequestHandle);

    return i1RetVal;
}

/*********************************************************************
*    Function Name    :    PPPBAPCallNotificationFromRM 
*
*    Description        :
*            Invoked by the RM to indicate the status of a call and 
*          the action being taken to BAP module.
*
*    Parameter(s)    :
*        u2BundleIfIndex - Interface Index of the MP bundle for which 
*                       the call setup    procedure was initiated. 
*
*        pReqHandle      -    Points to the unique identifier of the request for
*                              which the call has been setup by the RM.
*
*        PhNumber           -    The phone number corresponding to the attempted call.
*
*        MsgCode            -    The result of the call setup procedure which may
*                               be Q.931 cause value. If the call is successful,
*                              this parameter must be set to SUCCESS_CODE (=0).
*
*        Action            - The action taken by RM if the call has failed. If 
*                              the call is successful, this parameter must be set 
*                              to BAP_NO_RETRY.
*                              Possible values are BAP_RETRY, BAP_NO_RETRY.
*
*       Return Value    :    VOID    
*********************************************************************/
VOID
PPPBAPCallNotificationFromRM (UINT2 u2BundleIfIndex,
                              tReqHandle * pRequestHandle,
                              tPhNum PhNumber, UINT1 u1MsgCode, UINT1 u1Action)
{
    tPPPIf             *pBundleIf;

    pBundleIf = SNMPGetBundleIfptr (u2BundleIfIndex);

    BAPCallNotificationFromRM (pBundleIf, pRequestHandle, PhNumber, u1MsgCode,
                               u1Action);

    return;

}
