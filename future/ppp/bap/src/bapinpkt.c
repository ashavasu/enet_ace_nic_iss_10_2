/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bapinpkt.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This files contains the packet extraction and  
 *             validation routines of BAP module.
 *
 *******************************************************************/
#include "bapcom.h"
#include "globexts.h"

#define    MIN_PH_DELTA_SUB_OPTS_SIZE    4
#define BAP_HDR_LEN                    4

void                BAPGetPhNumFromDelta (tPhNum * pFirstLinkPhNum,
                                          UINT1 UniqueDigits,
                                          UINT1 *pSubscriberNum,
                                          UINT1 SubscriberNumLen,
                                          UINT1 *pSubAddr, UINT1 SubAddressLen,
                                          tPhNum *);
INT1                BAPProcessLinkTypeOption (tBAPInfo * pBAPInfo,
                                              t_MSG_DESC * pInPkt, UINT2 OptLen,
                                              tReqBlock * pReqBlock,
                                              UINT1 *pOptMask,
                                              t_SLL * pPhNumList);
INT1                BAPProcessPhoneDeltaOption (tBAPInfo * pBAPInfo,
                                                t_MSG_DESC * pInPkt,
                                                UINT2 OptLen,
                                                tReqBlock * pReqBlock,
                                                UINT1 *pOptMask,
                                                t_SLL * pPhNumList);
INT1                BAPProcessNoPhNumOption (tBAPInfo * pBAPInfo,
                                             t_MSG_DESC * pInPkt, UINT2 OptLen,
                                             tReqBlock * pReqBlock,
                                             UINT1 *pOptMask,
                                             t_SLL * pPhNumList);
INT1                BAPProcessReasonOption (tBAPInfo * pBAPInfo,
                                            t_MSG_DESC * pInPkt, UINT2 OptLen,
                                            tReqBlock * pReqBlock,
                                            UINT1 *pOptMask,
                                            t_SLL * pPhNumList);
INT1                BAPProcessLinkDiscrOption (tBAPInfo * pBAPInfo,
                                               t_MSG_DESC * pInPkt,
                                               UINT2 OptLen,
                                               tReqBlock * pReqBlock,
                                               UINT1 *pOptMask,
                                               t_SLL * pPhNumList);
INT1                BAPProcessCallStatusOption (tBAPInfo * pBAPInfo,
                                                t_MSG_DESC * pInPkt,
                                                UINT2 OptLen,
                                                tReqBlock * pReqBlock,
                                                UINT1 *pOptMask,
                                                t_SLL * pPhNumList);

VOID                BAPClearTempListNode (tTempPhNode * pPhNode);

tBAPOptInfo         BAPOptInfo[] = {
    {BAP_LINK_TYPE, BAP_LINK_TYPE_MASK, BAPProcessLinkTypeOption},
    {BAP_PHONE_DELTA, BAP_PH_DELTA_MASK, BAPProcessPhoneDeltaOption},
    {BAP_NO_PHONE_NUM_NEEDED, BAP_NO_PH_NUM_MASK, BAPProcessNoPhNumOption},
    {BAP_REASON, BAP_REASON_MASK, BAPProcessReasonOption},
    {BAP_LINK_DISCR, BAP_LINK_DISCR_MASK, BAPProcessLinkDiscrOption},
    {BAP_CALL_STATUS, BAP_CALL_STATUS_MASK, BAPProcessCallStatusOption}
};

/* Following table, used for packet validation. Ref.:Appendix in rfc 2125 */
tBAPPktInfo         BAPPktInfo[] = {
/*    PktType                    Mandatory            Allowed(Includes Mandatory) */

    {BAP_CALL_REQUEST, BAP_LINK_TYPE_MASK,
     BAP_LINK_TYPE_MASK | BAP_NO_PH_NUM_MASK | BAP_REASON_MASK},
    {BAP_CALL_RESPONSE, 0,
     BAP_LINK_TYPE_MASK | BAP_PH_DELTA_MASK | BAP_REASON_MASK},
    {BAP_CALL_BACK_REQUEST, BAP_LINK_TYPE_MASK | BAP_PH_DELTA_MASK,
     BAP_LINK_TYPE_MASK | BAP_PH_DELTA_MASK | BAP_REASON_MASK},
    {BAP_CALL_BACK_RESPONSE, 0, BAP_LINK_TYPE_MASK | BAP_REASON_MASK},

    {BAP_LINK_DROP_REQUEST, BAP_LINK_DISCR_MASK,
     BAP_LINK_DISCR_MASK | BAP_REASON_MASK},
    {BAP_LINK_DROP_RESPONSE, 0, BAP_REASON_MASK},

    {BAP_STATUS_INDICATION, BAP_CALL_STATUS_MASK,
     BAP_CALL_STATUS_MASK | BAP_PH_DELTA_MASK | BAP_REASON_MASK},
    {BAP_STATUS_RESPONSE, 0, BAP_REASON_MASK}
};

/*********************************************************************
                BAP In Packet Processing Functions
*********************************************************************/

/*********************************************************************
*    Function Name    :    BAPProcessPktHeader
*
*    Description        :
*                    Extracts and validates the BAP packet header.
*
*    Parameter(s)    :
*            pInPkt    -    Points to the incoming BAP datagram.
*            pLength    -    Length of the incoming packet.
*        pReqBlock    -    Points to the request block. The ReqType field and 
*                        the RespCode field (if the packet is of response type)
*                        of this structure is filled.
*
*    Return Value    :    DISCARD on invalid header / 
*                        The Type of the packet if the header is valid.
*********************************************************************/
INT1
BAPProcessPktHeader (t_MSG_DESC * pInPkt, UINT2 *pLength, tReqBlock * pReqBlock)
{
    INT1                PktType;
    UINT2               Len, Length;

    PktType = DISCARD;

    if (*pLength < BAP_HDR_LEN)
    {
    }
    else
    {

        CB_READ_OFFSET (pInPkt) = 0;

        /* Extract the Packet Header */
        EXTRACT1BYTE (pInPkt, PktType);
        EXTRACT1BYTE (pInPkt, pReqBlock->Id);
        EXTRACT2BYTE (pInPkt, Len);
        Length = *pLength;
        (*pLength) -= BAP_HDR_LEN;

        pReqBlock->ReqType = (UINT1) PktType;
        /* 
           If the Packet is of response type, ( Even Packet Type numbers ) 
           extract the response code.
         */
        if ((PktType == BAP_CALL_RESPONSE)
            || (PktType == BAP_CALL_BACK_RESPONSE)
            || (PktType == BAP_LINK_DROP_RESPONSE)
            || (PktType == BAP_STATUS_RESPONSE))
        {
            if (*pLength < 1)
            {
                PktType = DISCARD;
            }
            else
            {
                (*pLength)--;
                EXTRACT1BYTE (pInPkt, pReqBlock->RespCode);
            }
        }
        if ((PktType != DISCARD)
            && ((PktType < BAP_CALL_REQUEST) || (PktType > BAP_STATUS_RESPONSE)
                || (Length != Len)
                || (pReqBlock->RespCode > BAP_REQUEST_FULL_NAK)
                || ((PktType == BAP_STATUS_RESPONSE)
                    && (pReqBlock->RespCode != BAP_REQUEST_ACK))))
        {
            PktType = DISCARD;
        }
    }
    return (PktType);
}

/*********************************************************************
*    Function Name    :    BAPProcessOptions 
*
*    Description        :
*                    Extracts and validates options from the BAP packet.    
*
*    Parameter(s)    :
*        pBAPInfo    -    Points to the BAP information structure corresponding
*                        to the bundle over which the packet whose options are 
*                        being processed is received.
*            pInPkt    -    Points to the incoming BAP datagram.
*            Length    -    Length of the incoming packet.
*        pReqBlock    -    Points to the request block. This function fills up 
*                        the fields of this structure based on the options / 
*                        values of the options in the incoming packet.
*
*    Return Value    :    NOT_OK if packet is invalid / OK otherwise. 
*********************************************************************/
INT1
BAPProcessOptions (tBAPInfo * pBAPInfo, t_MSG_DESC * pInPkt, UINT2 Length,
                   tReqBlock * pReqBlock)
{

    t_SLL               TempPhNumList;

    UINT1               NumPhNums, OptMask, OptType, OptLen, Index;

    INT1                RetCode;

    UINT2               Offset;

    tTempPhNode        *pTempPhNum;

    Offset = 0;
    OptMask = 0;
    RetCode = OK;

    SLL_INIT (&TempPhNumList);

    while (Offset < Length)
    {
        EXTRACT1BYTE (pInPkt, OptType);
        EXTRACT1BYTE (pInPkt, OptLen);

        if ((OptType < BAP_MIN_OPT_TYPES) || (OptType > BAP_MAX_OPT_TYPES)
            || (OptLen > (Length - Offset)) || (OptLen < BAP_OPT_HDR_LEN)
            || ((OptType != BAP_PHONE_DELTA)
                && (BAPOptInfo[OptType - 1].OptionMask & OptMask))
            ||
            (((*BAPOptInfo
               [OptType - 1].pProcessBAPOption) (pBAPInfo, pInPkt,
                                                 (UINT2) (OptLen -
                                                          BAP_OPT_HDR_LEN),
                                                 pReqBlock, &OptMask,
                                                 &TempPhNumList)) == NOT_OK))
        {
            if (SLL_COUNT (&TempPhNumList) != 0)
            {

                SLL_DELETE_TILL_NODE (&TempPhNumList,
                                      SLL_FIRST (&TempPhNumList),
                                      SLL_LAST (&TempPhNumList),
                                      (VOID (*)(t_SLL_NODE *))
                                      BAPClearTempListNode);
            }
            return (NOT_OK);
            break;
        }

        Offset += OptLen;
    }

    /*
       The phone numbers received, if any are assembled into the 
       pPhoneNumbers field of the RequestBlock. This is done by scanning
       the SLL and getting the phone numbers.
     */
    Index = 0;
    if ((NumPhNums = (UINT1) SLL_COUNT (&TempPhNumList)) != 0)
    {
        if ((pReqBlock->Options.pPhNumbers =
             (tPhNumbers *) ALLOC_STR ((NumPhNums * sizeof (tPhNum)) + 1)) ==
            NULL)
        {
            RetCode = NOT_OK;
        }
        else
        {
            pReqBlock->Options.pPhNumbers->NumPhoneNumbers = NumPhNums;
            SLL_SCAN (&TempPhNumList, pTempPhNum, tTempPhNode *)
            {
                MEMCPY (&pReqBlock->Options.pPhNumbers->PhoneNumbers[Index],
                        &pTempPhNum->PhoneNumber, sizeof (tPhNum));
                Index++;
            }
        }

        /*
           The phone numbers are no longer needed. Delete all nodes of the 
           temporary phone number SLL. 
         */
        SLL_DELETE_TILL_NODE (&TempPhNumList, SLL_FIRST (&TempPhNumList),
                              SLL_LAST (&TempPhNumList),
                              (VOID (*)(t_SLL_NODE *)) BAPClearTempListNode);
    }

    if ((RetCode == OK)
        &&
        (((OptMask
           & BAPPktInfo[pReqBlock->ReqType - 1].MandatoryOptions) !=
          BAPPktInfo[pReqBlock->ReqType - 1].MandatoryOptions)
         || ((OptMask & ~(BAPPktInfo[pReqBlock->ReqType - 1].AllowedOptions)) !=
             0)))
    {
        RetCode = NOT_OK;
    }

    return (RetCode);
}

/*********************************************************************
*    Function Name    :    BAPProcessLinkTypeOption
*
*    Description        :    Extracts and validates the link type option in a 
*                        BAP Packet.
*
*    Parameter(s)    :
*        pBAPInfo    -    Points to the BAP information structure.
*            pInPkt    -    Points to the incoming BAP datagram.
*            OptLen    -    Length of the option excluding the Option Type and
*                        Option Length fields.
*        pReqBlock    -    Points to the request block. This function fills up 
*                        the LinkType and LinkSpeed fields of this structure. 
*        pOptMask    -    Points to the bit mask of received options. This 
*                        function sets the link type bit of the bitmask. 
*
*    Return Value    :    NOT_OK if packet is invalid / OK otherwise. 
*********************************************************************/
INT1
BAPProcessLinkTypeOption (tBAPInfo * pBAPInfo, t_MSG_DESC * pInPkt,
                          UINT2 OptLen, tReqBlock * pReqBlock, UINT1 *pOptMask,
                          t_SLL * pPhNumList)
{
    UINT1               LinkTypeArray[BAP_LINK_TYPE_VAL_LEN], Temp, LinkTypeIdx;

    PPP_UNUSED (pBAPInfo);
    PPP_UNUSED (pPhNumList);
    if (OptLen == 0)
    {
    }
    LinkTypeIdx = 0;
    BZERO (LinkTypeArray, BAP_LINK_TYPE_VAL_LEN);

    /*
       Extract as many bytes as specified by Length field. Only the first 3
       are actually valid.
     */
    while (LinkTypeIdx != OptLen)
    {
        EXTRACT1BYTE (pInPkt, Temp);
        if (LinkTypeIdx < BAP_LINK_TYPE_VAL_LEN)
        {
            LinkTypeArray[LinkTypeIdx] = Temp;
        }
        LinkTypeIdx++;
    }

    /* Fill up the request block with the link type and speed values */
    pReqBlock->Options.LinkSpeed =
        (UINT2) ((LinkTypeArray[0] << 8) | LinkTypeArray[1]);
    pReqBlock->Options.LinkType = LinkTypeArray[2];

    /* Updata Option Received Mask */
    *pOptMask |= BAP_LINK_TYPE_MASK;

    return (OK);
}

/*********************************************************************
*    Function Name    :    BAPProcessPhoneDeltaOption    
*
*    Description        :    Extracts and validates the Phone Delta option in a 
*                        BAP Packet. The extracted Phone delta is converted to
*                        a phone number string and added to a linked list. This
*                        list is scanned and all the phone numbers are collected
*                        in a single string at the end of the packet processing.
*
*    Parameter(s)    :
*        pBAPInfo    -    Points to the BAP information structure.
*            pInPkt    -    Points to the incoming BAP datagram.
*            OptLen    -    Length of the option excluding the Option Type and 
*                        Option Length fields.
*        pReqBlock    -    Points to the request block. This function does not 
*                        use / modify any fields of this structure.
*        pOptMask    -    Points to the bit mask of received options. This 
*                        function sets the Phone Delta bit of the bitmask. 
*
*    Return Value    :    NOT_OK if packet is invalid / OK otherwise. 
*********************************************************************/
INT1
BAPProcessPhoneDeltaOption (tBAPInfo * pBAPInfo, t_MSG_DESC * pInPkt,
                            UINT2 OptLen, tReqBlock * pReqBlock,
                            UINT1 *pOptMask, t_SLL * pPhNumList)
{
    UINT1               PhSubOptType,
        PhSubOptLen,
        SubOptOffset,
        UniqueDigits = 0,
        SubscriberNum[PPP_MAX_PH_NUM_SIZE],
        SubscriberNumLen,
        SubAddr[PPP_MAX_PH_NUM_SIZE], SubAddressLen, PhSubOptFlag;
    INT1                RetCode;
    tTempPhNode        *pPhNode;
    tPhNum             *pPhDecodePtr;

    /* Makefile changes */
    BZERO (SubscriberNum, PPP_MAX_PH_NUM_SIZE);
    SubAddressLen = 0;
    SubscriberNumLen = 0;

    PhSubOptFlag = 0;
    SubOptOffset = 0;
    pPhNode = NULL;
    RetCode = OK;

    /* Check if size is >= the size of the two mandatory sub-options. */
    if (OptLen < MIN_PH_DELTA_SUB_OPTS_SIZE)
    {
        return (NOT_OK);
    }

    /* Extract the sub-options */
    while (SubOptOffset != OptLen)
    {

        EXTRACT1BYTE (pInPkt, PhSubOptType);
        EXTRACT1BYTE (pInPkt, PhSubOptLen);

        if ((PhSubOptLen < BAP_OPT_HDR_LEN) || (PhSubOptLen > OptLen))
        {
            RetCode = NOT_OK;
        }
        else
        {
            /* Extract and validate the sub-options */
            switch (PhSubOptType)
            {
                case BAP_UNIQUE_DIGITS:
                    /*
                       Check if there is any repetetion of Unique-Digits
                       sub-option in this Phone-Delta option.
                     */
                    if ((PhSubOptLen != BAP_UNIQUE_DIGITS_LEN)
                        || (PhSubOptFlag & BAP_UNIQUE_DIGITS_MASK))
                    {
                        RetCode = NOT_OK;
                    }
                    else
                    {
                        EXTRACT1BYTE (pInPkt, UniqueDigits);
                        PhSubOptFlag |= BAP_UNIQUE_DIGITS_MASK;
                    }
                    break;

                case BAP_SUBSCRIBER_NUMBER:
                    if (PhSubOptFlag & BAP_SUBSCRIBER_NUM_MASK)
                    {
                        RetCode = NOT_OK;
                    }
                    else
                    {
                        SubscriberNumLen =
                            (UINT1) (PhSubOptLen - BAP_OPT_HDR_LEN);
                        EXTRACTSTR (pInPkt, SubscriberNum, SubscriberNumLen);
                        PhSubOptFlag |= BAP_SUBSCRIBER_NUM_MASK;
                    }
                    break;

                case BAP_SUB_ADDRESS:
                    if (PhSubOptFlag & BAP_SUB_ADDR_MASK)
                    {
                        RetCode = NOT_OK;
                    }
                    else
                    {
                        SubAddressLen = (UINT1) (PhSubOptLen - BAP_OPT_HDR_LEN);
                        EXTRACTSTR (pInPkt, SubAddr, SubAddressLen);
                        PhSubOptFlag |= BAP_SUB_ADDR_MASK;
                    }
                    break;

                default:
                    RetCode = NOT_OK;
            }
        }
        if (RetCode == NOT_OK)
        {
            break;
        }
        SubOptOffset += PhSubOptLen;
    }

    /* 
       The PhSubOptFlag checks for the prescence of the mandatory sub-options
       - Unique Digits and Subscriber number.
     */
    if (((PhSubOptFlag & BAP_UNIQUE_DIGITS_MASK) == NOT_SET)
        || ((PhSubOptFlag & BAP_SUBSCRIBER_NUM_MASK) == NOT_SET)
        || (SubscriberNumLen < UniqueDigits) || (RetCode == NOT_OK))
    {
        return (NOT_OK);
    }

    /*
       Construct the phone number from the phone delta and add to the list of
       incoming phone numbers.
     */
    if ((pPhNode = (tTempPhNode *) ALLOC_STR (sizeof (tTempPhNode))) == NULL)
    {
        return (NOT_OK);
    }

    pPhDecodePtr = &pBAPInfo->RemFirstLnkPhNum;
    if (pReqBlock->ReqType == BAP_STATUS_INDICATION)
    {
        pPhDecodePtr = &pBAPInfo->LocFirstLnkPhNum;
    }
    BAPGetPhNumFromDelta (pPhDecodePtr, UniqueDigits, SubscriberNum,
                          SubscriberNumLen, SubAddr, SubAddressLen,
                          &pPhNode->PhoneNumber);
    SLL_ADD (pPhNumList, (t_SLL_NODE *) pPhNode);

    /* Updata Option Received Mask */
    *pOptMask |= BAP_PH_DELTA_MASK;

    return (OK);
}

/*********************************************************************
*    Function Name    :    BAPProcessNoPhNumOption 
*
*    Description        :    Extracts and validates the "No-Phone-Number-Needed" 
*                        option in a BAP Packet.
*
*    Parameter(s)    :
*        pBAPInfo    -    Points to the BAP information structure.
*            pInPkt    -    Points to the incoming BAP datagram.
*            OptLen    -    Length of the option excluding the Option Type and 
*                        Option Length fields.
*        pReqBlock    -    Points to the request block. This function fills up 
*                        the IsPhNumNeeded field of this structure. 
*        pOptMask    -    Points to the bit mask of received options. This 
*                        function sets the NoPhNum bit of the bitmask. 
*
*    Return Value    :    NOT_OK if packet is invalid / OK otherwise. 
*********************************************************************/
INT1
BAPProcessNoPhNumOption (tBAPInfo * pBAPInfo, t_MSG_DESC * pInPkt, UINT2 OptLen,
                         tReqBlock * pReqBlock, UINT1 *pOptMask,
                         t_SLL * pPhNumList)
{
    INT1                RetCode;

    PPP_UNUSED (pBAPInfo);
    PPP_UNUSED (pPhNumList);
    PPP_UNUSED (pInPkt);
    RetCode = OK;

    if (OptLen != 0)
    {
        RetCode = NOT_OK;
    }
    else
    {
        pReqBlock->Options.IsPhNumNeeded = FALSE;

        /* Update Option Received Mask */
        *pOptMask |= BAP_NO_PH_NUM_MASK;
    }

    return (RetCode);
}

/*********************************************************************
*    Function Name    :    BAPProcessReasonOption 
*
*    Description        :    Extracts and validates the Reason String 
*                        option in a BAP Packet.
*
*    Parameter(s)    :
*        pBAPInfo    -    Points to the BAP information structure.
*            pInPkt    -    Points to the incoming BAP datagram.
*            OptLen    -    Length of the option excluding the Option Type and 
*                        Option Length fields.
*        pReqBlock    -    Points to the request block. This function does not 
*                        use / modify any fields of this structure.
*        pOptMask    -    Points to the bit mask of received options. This 
*                        function sets the Reason String bit of the bit mask. 
*
*    Return Value    :    NOT_OK if packet is invalid / OK otherwise. 
*********************************************************************/
INT1
BAPProcessReasonOption (tBAPInfo * pBAPInfo, t_MSG_DESC * pInPkt, UINT2 OptLen,
                        tReqBlock * pReqBlock, UINT1 *pOptMask,
                        t_SLL * pPhNumList)
{

    PPP_UNUSED (pBAPInfo);
    PPP_UNUSED (pInPkt);
    PPP_UNUSED (OptLen);
    PPP_UNUSED (pReqBlock);
    PPP_UNUSED (pPhNumList);
    *pOptMask |= BAP_REASON_MASK;

    return (OK);
}

/*********************************************************************
*    Function Name    :    BAPProcessLinkDiscrOption 
*
*    Description        :    Extracts and validates the Link Discriminator 
*                        option in a BAP Packet.
*
*    Parameter(s)    :
*        pBAPInfo    -    Points to the BAP information structure.
*            pInPkt    -    Points to the incoming BAP datagram.
*            OptLen    -    Length of the option excluding the Option Type and 
*                        Option Length fields.
*        pReqBlock    -    Points to the request block. This function fills up 
*                        the Link Discriminator field of this structure. 
*        pOptMask    -    Points to the bit mask of received options. This 
*                        function sets the Link Discriminator bit of the 
*                        bit mask. 
*
*    Return Value    :    NOT_OK if packet is invalid / OK otherwise. 
*********************************************************************/
INT1
BAPProcessLinkDiscrOption (tBAPInfo * pBAPInfo, t_MSG_DESC * pInPkt,
                           UINT2 OptLen, tReqBlock * pReqBlock,
                           UINT1 *pOptMask, t_SLL * pPhNumList)
{
    INT1                RetVal;
    tPPPIf             *pIf;

    PPP_UNUSED (pPhNumList);
    if (OptLen != BAP_LINK_DISCR_VAL_LEN)
    {
        RetVal = NOT_OK;
    }
    else
    {
        EXTRACT2BYTE (pInPkt, pReqBlock->Options.LinkDiscr);

        /*
           Check if the received link discriminator is present in any of the 
           member links' negotiated parameters. 
         */
        RetVal = NOT_OK;
        SLL_SCAN_OFFSET (&pBAPInfo->pBundleIf->MPInfo.BundleInfo.MemberList,
                         pIf, tPPPIf *, FIND_OFFSET (tPPPIf,
                                                     MPInfo.MemberInfo.
                                                     NextMPIf))
        {
            if (pIf->LcpOptionsAckedByPeer.LinkDiscriminator ==
                pReqBlock->Options.LinkDiscr)
            {
                RetVal = OK;
                break;
            }
        }
    }

    /* Update Option Received Mask */
    *pOptMask |= BAP_LINK_DISCR_MASK;

    return (RetVal);
}

/*********************************************************************
*    Function Name    :    BAPProcessCallStatusOption
*
*    Description        :    Extracts and validates the Call Status 
*                        option in a BAP Packet.
*
*    Parameter(s)    :
*        pBAPInfo    -    Points to the BAP information structure.
*            pInPkt    -    Points to the incoming BAP datagram.
*            OptLen    -    Length of the option excluding the Option Type and 
*                        Option Length fields.
*        pReqBlock    -    Points to the request block. This function fills up 
*                        the MsgCode and ActionSent fields of this structure. 
*        pOptMask    -    Points to the bit mask of received options. This 
*                        function sets the Call Status bit of the 
*                        bit mask. 
*
*    Return Value    :    NOT_OK if packet is invalid / OK otherwise. 
*********************************************************************/
INT1
BAPProcessCallStatusOption (tBAPInfo * pBAPInfo, t_MSG_DESC * pInPkt,
                            UINT2 OptLen, tReqBlock * pReqBlock,
                            UINT1 *pOptMask, t_SLL * pPhNumList)
{
    INT1                RetCode;

    PPP_UNUSED (pBAPInfo);
    PPP_UNUSED (pPhNumList);
    RetCode = OK;

    if (OptLen != BAP_CALL_STATUS_VAL_LEN)
    {
        RetCode = NOT_OK;
    }
    else
    {
        EXTRACT1BYTE (pInPkt, pReqBlock->MsgCode);
        EXTRACT1BYTE (pInPkt, pReqBlock->ActionSent);
        if (pReqBlock->ActionSent > BAP_RETRY)
        {
            RetCode = NOT_OK;
        }
    }

    /* Update Option Received Mask */
    *pOptMask |= BAP_CALL_STATUS_MASK;

    return (RetCode);
}

/*********************************************************************
*    Function Name    :    BAPGetPhNumFromDelta 
*
*    Description        :
*                    Converts the Phone Delta into a Phone number using 
*    the unique digits and first member links' phone number.
*
*    Parameter(s)    :
*    pFirstLinkPhNum    -    Points to the first member links phone number.
*    UniqueDigits    -    The number of digits unique from the first member's
*                        phone number.
*    pSubscriberNum    -    Points to the subscriber number portion of the phone
*                        number.
*    SubscriberNumLen-    Length of the Subscriber Number.
*        pSubAddr    -    Points to the (ISDN) sub-address, if any.
*    SubAddressLen    -    Length of the sub-address.
*
*    Return Value    :    Returns the constructed phone number.
*********************************************************************/
void
BAPGetPhNumFromDelta (tPhNum * pFirstLinkPhNum, UINT1 UniqueDigits,
                      UINT1 *pSubscriberNum, UINT1 SubscriberNumLen,
                      UINT1 *pSubAddr, UINT1 SubAddressLen, tPhNum * pTempPh)
{
    tPhNum              PhNumber;
    INT1                FirstLinkSubNumSize;
    UINT1               Index, BaseSize;

    /* BaseSize is the number of digits that are common to both addresses */
    BaseSize = 0;
    if ((pFirstLinkPhNum->Size != 0)
        &&
        ((FirstLinkSubNumSize
          =
          (INT1) (pFirstLinkPhNum->Size - pFirstLinkPhNum->SubAddrSize)) >=
         UniqueDigits))
    {
        BaseSize = (INT1) (FirstLinkSubNumSize - UniqueDigits);
        /* Copy the common digits */
        MEMCPY (PhNumber.String, pFirstLinkPhNum->String, BaseSize);
    }
    PhNumber.Size = (INT1) (BaseSize + UniqueDigits + SubAddressLen);
    PhNumber.SubAddrSize = SubAddressLen;

    /* Add in reverse, first the sub-address, then the unique digits */
    for (Index = PhNumber.Size; Index < BaseSize; Index--)
    {
        if (Index > (BaseSize + UniqueDigits))
        {
            PhNumber.String[Index] = pSubAddr[--SubAddressLen];
        }
        else
        {
            PhNumber.String[Index] = pSubscriberNum[--SubscriberNumLen];
        }
    }

    MEMCPY (pTempPh, &PhNumber, sizeof (tPhNum));
}

VOID
BAPClearTempListNode (tTempPhNode * pPhNode)
{

    FREE_STR (pPhNode);
    pPhNode = NULL;
}
