/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bapif.c,v 1.2 2014/03/11 14:02:45 siva Exp $ 
 *
 * Description:This files contains the interface procedures of the 
 *             of the BAP module.
 *
 *******************************************************************/

#include "bapcom.h"
#include "bapexts.h"
#include "gsemdefs.h"
#include "baprm.h"
#include "globexts.h"

tBAPMemCounters     BAPMemCounters;

/*********************************************************************
                        BAP Interface Functions
*********************************************************************/

/*********************************************************************
*    Function Name    :    BAPAddLinkToBundle
*
*    Description        :
*                Invoked by the RM to request the BAP module to negotiate the
*    addition of a link to the specified bundle.
*
*    Parameter(s)    :
*        pBundleIf    -    Points to the MP bundle to which a new link is to be
*                        added.
*    pRequestHandle    -    Pointer to unique identifier of the request, assigned
*                        by the BAP module. Memory should be allocated for this
*                        variable by the calling module. 
*        ReqType        -    Specifies type of request made. The possible values
*                        are    :    BAP_TX_CALL_REQUEST for Call Request type,
*                                BAP_TX_CALL_BACK_REQUEST for Call-Back Request
*                                type.
*    pBAPOptions        -    Points to the BAP options structure.
*
*    Return Value    :    OK on success,
*                        NOT_OK otherwise (memory allocation failure, other
*                        internal errors)
*********************************************************************/
INT1
BAPAddLinkToBundle (tPPPIf * pBundleIf, tReqHandle * pRequestHandle,
                    UINT1 ReqType, tBAPOpts * pBAPOptions)
{
    tBAPInfo           *pBAPInfo;
    UINT2               PhoneNumbersSize;
    tReqBlock           RequestBlock;

    if ((pBundleIf->MPInfo.BundleInfo.BWMProtocolInfo.Protocol != BAP_PROTOCOL)
        ||
        (((tBAPInfo
           *) pBundleIf->MPInfo.BundleInfo.BWMProtocolInfo.pProtocolInfo)->
         BACPIf.OperStatus != STATUS_UP))
    {
        return (NOT_OK);
    }

    BZERO (&RequestBlock, sizeof (tReqBlock));
    pBAPInfo =
        (tBAPInfo *) pBundleIf->MPInfo.BundleInfo.BWMProtocolInfo.pProtocolInfo;

    /* Fill up the request block parameters */
    MEMCPY (&RequestBlock.Options, pBAPOptions, sizeof (tBAPOpts));
    RequestBlock.Options.pPhNumbers = NULL;
    RequestBlock.ReqType = ReqType;
    RequestBlock.Id = pBAPInfo->RequestId;

    /* Fill the phone number information in the request block */
    if ((pBAPOptions->pPhNumbers != NULL)
        && (pBAPOptions->pPhNumbers->NumPhoneNumbers != 0))
    {

        PhoneNumbersSize =
            (UINT2) ((sizeof (tPhNum) *
                      (pBAPOptions->pPhNumbers->NumPhoneNumbers - 1)) +
                     sizeof (tPhNumbers));

        /* Allocate and copy phone numbers passed by the peer */
        if ((RequestBlock.Options.pPhNumbers =
             (tPhNumbers *) ALLOC_STR (PhoneNumbersSize)) == NULL)
        {
            return (NOT_OK);
        }
        MEMCPY (RequestBlock.Options.pPhNumbers, pBAPOptions->pPhNumbers,
                PhoneNumbersSize);
    }

    /* Generate event to the BAP Main module - Event is the Request Type */
    if (BAPGenerateEvent (pBAPInfo, ReqType, &RequestBlock) == NOT_OK)
    {
        return (NOT_OK);
    }
    else
    {
        /* Fill up the Request Handle to be returned to the RM */
        pRequestHandle->Requester = BAP_LOCAL_RM;
        pRequestHandle->Id = pBAPInfo->RequestId;
    }

    pBAPInfo->RequestId++;

    if (RequestBlock.Options.pPhNumbers != NULL)
    {
        FREE_STR (RequestBlock.Options.pPhNumbers);
    }

    return (OK);
}

/*********************************************************************
*    Function Name    :    BAPDeleteLinkFromBundle
*
*    Description        :
*                Invoked by the RM to request the BAP module to negotiate the
*    deletion of a link from the specified bundle.
*
*    Parameter(s)    :
*        pBundleIf    -    Points to the MP bundle from which a link is to be 
*                        deleted.
*                pIf    -    Points to the link to be deleted from the bundle. This
*                        function accesses the Link Discriminator field of the 
*                        LcpOptionsAckedByLocal structure within pIf to
*                        construct a Link drop request.
*        pReqHandle    -    Pointer to unique identifier of the request, assigned
*                        by the BAP module. Memory should be allocated for this
*                        variable by the calling module. 
*
*    Return Value    :    OK on success,
*                        NOT_OK otherwise (memory allocation failure, other
*                        internal errors)
*********************************************************************/
INT1
BAPDeleteLinkFromBundle (tPPPIf * pBundleIf, tPPPIf * pIf,
                         tReqHandle * pRequestHandle)
{
    tReqBlock           RequestBlock;
    tBAPInfo           *pBAPInfo;

    if ((pBundleIf->MPInfo.BundleInfo.BWMProtocolInfo.Protocol != BAP_PROTOCOL)
        ||
        (((tBAPInfo
           *) pBundleIf->MPInfo.BundleInfo.BWMProtocolInfo.pProtocolInfo)->
         BACPIf.OperStatus != STATUS_UP))
    {
        return (NOT_OK);
    }

    BZERO (&RequestBlock, sizeof (tReqBlock));
    pBAPInfo =
        (tBAPInfo *) pBundleIf->MPInfo.BundleInfo.BWMProtocolInfo.pProtocolInfo;

    /* Fill up the request block parameters */
    RequestBlock.ReqType = BAP_TX_LINK_DROP_REQUEST;
    RequestBlock.Id = pBAPInfo->RequestId;
    RequestBlock.Options.LinkDiscr =
        pIf->LcpOptionsAckedByLocal.LinkDiscriminator;

    /* Generate Link Drop event to the BAP Main module */
    if (BAPGenerateEvent (pBAPInfo, BAP_TX_LINK_DROP_REQUEST, &RequestBlock) ==
        NOT_OK)
    {
        return (NOT_OK);
    }

    /* Fill up the Request Handle to be returned to the RM */
    pRequestHandle->Requester = BAP_LOCAL_RM;
    pRequestHandle->Id = pBAPInfo->RequestId;

    pBAPInfo->RequestId++;

    return (OK);
}

/*********************************************************************
*    Function Name    :    BAPCallNotificationFromRM 
*
*    Description        :
*            Invoked by the RM to indicate the status of a call and 
*    the action being taken to BAP module.
*
*    Parameter(s)    :
*        pBundleIf    -    Points to the MP bundle for which the call setup 
*                        procedure was initiated. 
*        pReqHandle    -    Points to the unique identifier of the request for
*                        which the call has been setup by the RM.
*        PhNumber    -    The phone number corresponding to the attempted call.
*        MsgCode        -    The result of the call setup procedure which may
*                        be Q.931 cause value. If the call is successful,
*                        this parameter must be set to SUCCESS_CODE (=0).
*        Action        -    The action taken by RM if the call has failed. If 
*                        the call is successful, this parameter must be set 
*                        to BAP_NO_RETRY.
*                        Possible values are BAP_RETRY, BAP_NO_RETRY.
*
*    Return Value    :    VOID    
*********************************************************************/
VOID
BAPCallNotificationFromRM (tPPPIf * pBundleIf, tReqHandle * pRequestHandle,
                           tPhNum PhNumber, UINT1 MsgCode, UINT1 Action)
{
    tPendingRequest    *pPendingRequest, *pMatchingRequest;

    tBAPInfo           *pBAPInfo;

    pPendingRequest = NULL;
    pMatchingRequest = NULL;
    pBAPInfo =
        (tBAPInfo *) pBundleIf->MPInfo.BundleInfo.BWMProtocolInfo.pProtocolInfo;

    /*
       Search list of BAP Pending Request instances for instance with 
       matching RequestHandle and generate the RX_BAP_CALL_STATUS event to the 
       corresponding instance. 
     */
    SLL_SCAN (&pBAPInfo->BAPRequestInstanceList, pPendingRequest,
              tPendingRequest *)
    {
        if (MEMCMP
            (&pPendingRequest->RequestHandle, pRequestHandle,
             sizeof (tReqHandle)) == 0)
        {
            pMatchingRequest = pPendingRequest;
            break;
        }
    }

    if (pMatchingRequest != NULL)
    {
        if (pMatchingRequest->Options.pPhNumbers != NULL)
        {
            pMatchingRequest->Options.pPhNumbers->NumPhoneNumbers = 0;
            if (PhNumber.Size != 0)
            {
                MEMCPY (&pMatchingRequest->Options.pPhNumbers->PhoneNumbers[0],
                        &PhNumber, sizeof (tPhNum));
                pMatchingRequest->Options.pPhNumbers->NumPhoneNumbers = 1;
            }
        }
        pMatchingRequest->MsgCode = MsgCode;
        pMatchingRequest->LastActionSent = Action;
        BAPUpdatePendingReqStatus (pMatchingRequest, BAP_RX_CALL_STATUS);
    }

    return;
}

/*********************************************************************
*    Function Name    :    BAPVerifyPeerRequestFromRM
*
*    Description        :
*                Called by BAP module to validate peer's request for 
*    addition/deletion of a link to/from the bundle.
*
*    Parameter(s)    :
*        pBundleIf    -    Points to the MP bundle on which the BAP Packet
*                        has been received.
*    pRequestHandle    -    Points to the unique identifier of the peers's request.
*        RequestType    -    Specifies type of  the request received. 
*                        Possible values are : 
*                        BAP_CALL_REQUEST for Call Request type, 
*                        BAP_CALL_BACK_REQUEST for Call-Back Request type, and 
*                        BAP_LINK_DROP_REQUEST for Link-Drop Request type.
*    pBAPOptions        -    Points to the BAP options structure. If the
*                        peer has sent phone numbers in the request packet,
*                        then BAP module will allocate and fill phone number
*                        structure field (pPhNumbers) and pass it to the RM.
*                        If no phone numbers are present in the peer's packet,
*                        then the BAP module will not allocate for pPhNumbers
*                        field. The RM must allocate and fill this field if it
*                        desires passing phone numbers to the BAP module.
*                        Similarly, LinkType and LinkSpeed fields will
*                        be filled by the BAP module if the peer
*                        has sent these options in the request packet, otherwise
*                        if needed, RM will fill these fields. 
*
*    Return Value    :    OK if agreeable to the request,
*                        BAP_BW_EXTREME if maximum or minimum allowed 
*                        bandwidth is reached,
*                        BAP_NOT_ACCEPTABLE if requested value is not acceptable 
*                        to the RM.
*********************************************************************/
INT1
BAPVerifyPeerRequestFromRM (tPPPIf * pBundleIf, tReqHandle * pRequestHandle,
                            UINT1 RequestType, tBAPOpts * pBAPOptions)
{
    tBAPInfo           *pBAPInfo;
    INT1                RespToSend;

    pBAPInfo =
        (tBAPInfo *) pBundleIf->MPInfo.BundleInfo.BWMProtocolInfo.pProtocolInfo;

    /* The contents of this function are to be modified during porting */
    RespToSend =
        RMVerifyPeerRequest (pBundleIf, pRequestHandle, RequestType,
                             pBAPOptions);

    PPP_UNUSED (pBAPInfo);
    return (RespToSend);
}

/*********************************************************************
*    Function Name    :    BAPInformRequestStatusToRM
*
*    Description        :
*                Called by BAP to inform the RM, of the status of a Call, 
*    Callback or Link Drop request initiated by either  the peer or the RM.
*
*    Parameter(s)    :
*        pBundleIf    -    Points to the MP bundle on which the RM or peer has
*                        requested to add or delete a link.
*    pRequestHandle    -    Points to the unique identifier of the request for 
*                        which status is being sent.
*            Status    -    Status of the request. Possible values are :
*                        BAP_SUCCESS if  ACK is received from peer,
*                        BAP_BW_EXTREME if FULL_NAK is received from peer,
*                        BAP_FAILURE if REJECT is received from  peer,
*                        BAP_END_SUCCESS if the transaction completed,
*                        BAP_RETRY_LATER if retry of request not configured
*                        and NAK is received from peer,
*                        BAP_END_FAILURE if the transaction was not
*                        complete.
*    pBAPOptions        -    Points to the BAP options structure.
*
*    Return Value    :    VOID    
*********************************************************************/
VOID
BAPInformRequestStatusToRM (tPPPIf * pBundleIf, tReqHandle * pRequestHandle,
                            UINT1 Status, tBAPOpts * pBAPOptions)
{

/* The contents of this function are to be modified during porting */
    RMRequestStatusFromBAP (pBundleIf, pRequestHandle, Status, pBAPOptions);

    return;
}

/*********************************************************************
*    Function Name    :    BapInput    
*
*    Description        :
*                    This function receives BAP datagrams from the PPP module,
*    validates them, determines and generates the event to the BAP main module. 
*
*    Parameter(s)    :
*        pBundleIf    -    The MP bundle on which the BAP datagram was received.
*            pInPkt    -    Points to the incoming BAP datagram.
*            Length    -    Length of the incoming datagram.
*
*    Return Value    :    VOID    
*********************************************************************/
VOID
BAPInput (tPPPIf * pBundleIf, t_MSG_DESC * pInPkt, UINT2 Length)
{
    tReqBlock           RequestBlock;
    tBAPInfo           *pBAPInfo;
    UINT1               RespCode;
    INT1                Event;

    RespCode = 0;
    PPP_UNUSED (RespCode);
    pBAPInfo =
        (tBAPInfo *) pBundleIf->MPInfo.BundleInfo.BWMProtocolInfo.pProtocolInfo;
    BZERO (&RequestBlock, sizeof (tReqBlock));
    RequestBlock.Options.IsPhNumNeeded = TRUE;

    /*
       BAPProcessOptions will fill the RequestBlock for request 
       type packets with the option values received. 
       01 - 08 Extracted type is the event passed to the SEM after packet
       and option validation/extraction.
     */
    if ((Event = BAPProcessPktHeader (pInPkt, &Length, &RequestBlock)) ==
        DISCARD)
    {
        DISCARD_PKT (pBundleIf, "\n Error : Invalid Packet Header");
    }
    else
    {
        if (BAPProcessOptions (pBAPInfo, pInPkt, Length, &RequestBlock) ==
            DISCARD)
        {
            DISCARD_PKT (pBundleIf, "\n Error : Invalid Option / Option Value");
        }
        else
        {
            if (BAPGenerateEvent (pBAPInfo, Event, &RequestBlock) == NOT_OK)
            {
                DISCARD_PKT (pBundleIf,
                             "\n Error : Invalid Event/Critical Error");
            }
        }
    }

    if (RequestBlock.Options.pPhNumbers != NULL)
    {
        FREE_STR (RequestBlock.Options.pPhNumbers);
    }

    RELEASE_BUFFER (pInPkt);
    ReleaseFlag = PPP_NO;
    pInPkt = NULL;
    return;
}

/*********************************************************************
*    Function Name    :    BAPCreateAndInit
*
*    Description        :
*                    This function allocates and initializes BAP related 
*    information for a MP bundle.
*
*    Parameter(s)    :
*        pBundleIf    -    Points to the MP bundle for which the BAP related 
*                        information has to be updated.
*
*    Return Value    :    Points to the tBAPInfo structure on success / NULL
*                        on failure.                    
*********************************************************************/
tBAPInfo           *
BAPCreateAndInit (tPPPIf * pBundleIf)
{
    tBAPInfo           *pBAPInfo;

    if ((pBAPInfo = (tBAPInfo *) ALLOCATE_BAP_INFO ()) == NULL)
    {
        return (NULL);
    }

    BZERO (pBAPInfo, sizeof (tBAPInfo));
    pBAPInfo->NextNakLinkDiscrValue = BAP_MAX_LINK_DISCR_VAL;
    SLL_INIT (&pBAPInfo->BAPRequestInstanceList);
    pBAPInfo->pBundleIf = pBundleIf;
    pBAPInfo->LastLinkDropId = MAX_LINK_DROP_ID;
    pBAPInfo->RemoteAllowedReqTypes = BAP_REMOTE_BOTH_ALLOWED;
    pBAPInfo->RetransmitTimeOutVal = BAP_DEF_TIME_OUT_VAL;
    pBAPInfo->MaxRetransmits = BAP_DEF_MAX_RETRANSMITS;
    pBAPInfo->RetryTimeOutVal = BAP_DEF_TIME_OUT_VAL;

    return (pBAPInfo);
}

/*********************************************************************
*    Function Name    :    BAPDelete
*
*    Description        :
*                    This function deallocates BAP related information 
*    for a MP bundle.
*
*    Parameter(s)    :
*        pBundleIf    -    Points to the MP bundle for which the BAP related 
*                        information has to be deleted.
*
*    Return Value    :    VOID    
*********************************************************************/
VOID
BAPDelete (tPPPIf * pBundleIf)
{
    /* Makefile changes - unused variable */
    tBAPInfo           *pBAPInfo;

    pBAPInfo =
        (tBAPInfo *) pBundleIf->MPInfo.BundleInfo.BWMProtocolInfo.pProtocolInfo;

    GSEMDelete (&(pBAPInfo->BACPIf.BACPGSEM));

    SLL_DELETE_LIST (&pBAPInfo->BAPRequestInstanceList,
                     ((VOID (*)(t_SLL_NODE *)) BAPDeletePendingRequest));

    pBAPInfo->pBundleIf = NULL;
    FREE_BAP_INFO (pBAPInfo);

    return;
}
