/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bapsetif.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the interface procedures
 *             and other primitive functions of the BAP SET 
 *             module.
 *
 *******************************************************************/
#include "bapcom.h"
#include "bapset.h"
#include "bapexts.h"
#include "globexts.h"

extern tBAPPktCounters BAPPktCounters;

/*********************************************************************
                Function Prototypes
*********************************************************************/
INT1                BAPPendingRequestInit (tPendingRequest * pRequest,
                                           tReqBlock * pReqBlock, UINT1 State);

static UINT1        BAP_SET_States[BAP_MAX_STATES][40] = {
    {"CALL_REQ_SENT"}, {"CALL_BACK_REQ_SENT"},
    {"LINK_DROP_REQ_SENT"}, {"CALL_ACK_RXD"},
    {"CALL_BACK_ACK_RXD"}, {"CALL_NAK_RXD"},
    {"CALL_BACK_NAK_RXD"}, {"LINK_DROP_NAK_RXD"},
    {"CALL_STATUS_SENT"}, {"CALL_BACK_STATUS_SENT"},
    {"CALL_RESP_SENT"}, {"CALL_BACK_RESP_SENT"}
};
static UINT1        BAP_SET_Events[BAP_MAX_EVENTS][20] = {
    {"RRA"}, {"RRN"}, {"RRR"}, {"RFN"},
    {"TO_PLUS"}, {"TO_MINUS"},
    {"RSI_PLUS"}, {"RSI_MINUS"}, {"RSR"},
    {"RX_CALL_STATUS"}
};

/*********************************************************************
*    Function Name    :    BAPUpdatePendingReqStatus
*
*    Description        :
*                This function is called by BAPGenerateEvent to update
*                the status of the matching (pending) request.
*
*    Parameter(s)    :
*    pRequest-    Points to the pending request whose status 
*                        has to be updated.
*            Event    -    Event passed to the matching request. 
*
*    Return Values    :    VOID
*********************************************************************/
VOID
BAPUpdatePendingReqStatus (tPendingRequest * pRequest, UINT1 Event)
{
    UINT1               CurrentState, ActionIndex;

    CurrentState = pRequest->State;

    /*  SET action procedure is called only if processing is reqd.. */
    ActionIndex = BAPSETable[Event][CurrentState];
    if (BAPActionProcedure[ActionIndex] != NULL)
    {
        CurrentState = (*BAPActionProcedure[ActionIndex]) (pRequest);
    }

    PPP_TRC3 (CONTROL_PLANE, "BAPSEM[ %ld : Requester:Id %d:%d]  ",
              pRequest->pBAPInfo->pBundleIf->LinkInfo.IfIndex,
              pRequest->RequestHandle.Requester, pRequest->RequestHandle.Id);

    PPP_TRC2 (CONTROL_PLANE,
              "[Prev State : %s] ---[Event : %s]--> [Current State  : ",
              BAP_SET_States[pRequest->State], BAP_SET_Events[Event]);
    if (CurrentState == BAP_DESTROY)
    {
        PPP_TRC1 (CONTROL_PLANE, "%s]", "DELETE_ENTRY");
    }
    else
    {
        PPP_TRC1 (CONTROL_PLANE, "%s]", BAP_SET_States[CurrentState]);
    }

    if (CurrentState == BAP_DESTROY)
    {
        PPP_TRC (CONTROL_PLANE, "Deleting the pending request.");
        BAPDeletePendingRequest (pRequest);
    }
    else
    {
        pRequest->State = CurrentState;
    }

    return;
}

/*********************************************************************
*    Function Name    :    BAPCreateAndAddPendingRequest
*    Description        :
*                This function is called whenever a new instance of pending 
*                request has to be created and to be added to the list of the 
*                pending requests.
*
*    Parameter(s)    :
*            pBAPInfo-    Points to the tBAPInfo structure corressponds to the 
*                        bundle over which the request is made.
*        pReqBlock    -    Points to the ReqBlock structure which stores the
*                        information collected from the calling functions.
*            State    -    Initial state of the created pending request. Possible
*                        values are : BAP_CALL_REQ_SENT, BAP_CALL_BACK_REQ_SENT,
*                        BAP_LINK_DROP_REQ_SENT, BAP_CALL_RESP_SENT and 
*                        BAP_CALL_BACK_RESP_SENT.
*
*    Return Values    :    NOT_OK if memory allocation problem,
*                        OK otherwise.
*********************************************************************/
INT1
BAPCreateAndAddPendingRequest (tBAPInfo * pBAPInfo, tReqBlock * pReqBlock,
                               UINT1 State)
{
    tPendingRequest    *pRequest;
    INT1                Result;
    UINT1               ReqType;

    Result = OK;

    if ((pRequest = (tPendingRequest *) ALLOC_BAP_PENDING_REQUEST ()) == NULL)
    {
        PRINT_MEM_ALLOC_FAILURE;
        return (NOT_OK);
    }

    if (BAPPendingRequestInit (pRequest, pReqBlock, State) == NOT_OK)
    {
        PPP_TRC (ALL_FAILURE, "BAP PendingRequest Initialization failure");
        Result = NOT_OK;
    }
    else
    {
        pRequest->pBAPInfo = pBAPInfo;

        /* Add the request to the list */
        SLL_ADD (&pBAPInfo->BAPRequestInstanceList, (t_SLL_NODE *) pRequest);

        /*
           Initialize the retransmit counter. If the request is
           initiated by the RM, set the counter to MaxRetransmit value.
           Otherwise if peer is initiator, set the counter to some multiple
           of MaxRetransmit counter (to avoid indefinite waiting in
           some response sent state.
         */
        pRequest->RetransmitCounter = pBAPInfo->MaxRetransmits;
        if ((State == BAP_CALL_RESP_SENT) || (State == BAP_CALL_BACK_RESP_SENT))
        {
            pRequest->RetransmitCounter *= BAP_RESP_WAIT_FACTOR;
            pRequest->RetransmitTimer.Param1 = (UINT4) pRequest;
            PPPStartTimer (&pRequest->RetransmitTimer, BAP_REQUEST_TIMER,
                           pBAPInfo->RetransmitTimeOutVal);
        }
        else
        {
            /* Construct and send request packet */
            ReqType =
                (State == BAP_CALL_REQ_SENT) ? BAP_CALL_REQUEST : (State ==
                                                                   BAP_CALL_BACK_REQ_SENT)
                ? BAP_CALL_BACK_REQUEST : BAP_LINK_DROP_REQUEST;
            BAPConstructAndTxRequestPkts (ReqType, pRequest);
        }
    }

    PPP_TRC1 (CONTROL_PLANE, "Result Code: %d", Result);

    return (Result);
}

/*********************************************************************
*    Function Name    :    BAPPendingRequestInit
*    Description        :
*                This function is called to initialize an instance of
*                pending request.
*
*    Parameter(s)    :
*            pRequest-    Points to the request structure which has to
*                        be initialized.
*        pReqBlock    -    Points to the ReqBlock structure which stores the
*                        information collected from the calling functions.
*            State    -    Initial state of the created pending request. Possible
*                        values are : BAP_CALL_REQ_SENT, BAP_CALL_BACK_REQ_SENT,
*                        BAP_LINK_DROP_REQ_SENT, BAP_CALL_RESP_SENT and 
*                        BAP_CALL_BACK_RESP_SENT.
*
*    Return Values    :    NOT_OK if memory allocation problem,
*                        OK otherwise.
*********************************************************************/
INT1
BAPPendingRequestInit (tPendingRequest * pRequest, tReqBlock * pReqBlock,
                       UINT1 State)
{
    UINT2               PhNumSize;

    pRequest->State = State;

    pRequest->RequestHandle.Id = pReqBlock->Id;
    pRequest->RequestHandle.Requester =
        (State <= BAP_LINK_DROP_REQ_SENT) ? BAP_LOCAL_RM : BAP_REMOTE_PEER;

    MEMCPY (&pRequest->Options, &pReqBlock->Options, sizeof (tBAPOpts));
    pRequest->LastActionSent = BAP_NO_RETRY;
    pRequest->MsgCode = 0;
    pRequest->Options.pPhNumbers = NULL;

    if (((State != BAP_CALL_REQ_SENT)
         || (pReqBlock->Options.IsPhNumNeeded == TRUE))
        && (pReqBlock->Options.pPhNumbers != NULL))
    {
        PhNumSize =
            (UINT2) ((sizeof (tPhNum) *
                      (pReqBlock->Options.pPhNumbers->NumPhoneNumbers - 1)) +
                     sizeof (tPhNumbers));

        /* Allocate and copy phone numbers passed by the peer */
        if ((pRequest->Options.pPhNumbers =
             (tPhNumbers *) ALLOC_STR (PhNumSize)) == NULL)
        {
            PRINT_MEM_ALLOC_FAILURE;
            return (NOT_OK);
        }
        MEMCPY (pRequest->Options.pPhNumbers, pReqBlock->Options.pPhNumbers,
                PhNumSize);
    }

    PPP_INIT_TIMER (pRequest->RetransmitTimer);

    return (OK);
}

/*********************************************************************
*    Function Name    :    BAPDeletePendingRequest
*
*    Description        :
*                This function is called whenever a pending request
*                has to be deleted.
*
*    Parameter(s)    :
*        pRequest    -    Points to the request structure which has to
*                        be deleted.
*
*    Return Values    :    VOID
*********************************************************************/
VOID
BAPDeletePendingRequest (tPendingRequest * pRequest)
{

    /* Makefile changes - incompatible pointer type */
    SLL_DELETE (&pRequest->pBAPInfo->BAPRequestInstanceList,
                (t_SLL_NODE *) pRequest);

    if (pRequest->Options.pPhNumbers != NULL)
    {
        FREE_STR (pRequest->Options.pPhNumbers);
    }
    pRequest->pBAPInfo = NULL;

    PPPStopTimer (&pRequest->RetransmitTimer);

    FREE_BAP_PENDING_REQUEST (pRequest);

    return;
}

/*********************************************************************
*    Function Name    :    BAPTimeOut
*
*    Description        :
*            This function is called when there is a time out after transmission
*    of a BAP request or response.
*
*    Parameter(s)    :
*        pRequest    -    Points to the corresponding pending request. 
*
*    Return Value : VOID
*********************************************************************/
VOID
BAPTimeOut (VOID *pPenRequest)
{
    tPendingRequest    *pRequest;
    pRequest = (tPendingRequest *) pPenRequest;

    if (pRequest->RetransmitCounter == 0)
    {
        PPP_TRC (EVENT_TRC, "BAP_TO_MINUS Event........");
        BAPUpdatePendingReqStatus (pRequest, BAP_TO_MINUS);
    }
    else
    {
        PPP_TRC (EVENT_TRC, "BAP_TO_PLUS Event........");
        BAPUpdatePendingReqStatus (pRequest, BAP_TO_PLUS);
    }

    return;
}
