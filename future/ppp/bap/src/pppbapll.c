/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppbapll.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description: Low Level Routines for bap module.
 *
 *******************************************************************/
#include "pppsnmpm.h"
#include "bacpcom.h"
#include "mpdefs.h"
#include "pppmp.h"
#include "pppbap.h"
#include "bapexts.h"
#include "snmccons.h"
#include "llproto.h"

tBAPInfo           *SNMPGetBAPInfoPtr (UINT4 Index);

tBAPInfo           *
SNMPGetBAPInfoPtr (UINT4 Index)
{
    tPPPIf             *pIf;

    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if ((pIf != NULL) && (pIf->LinkInfo.IfIndex == Index)
            && (pIf->BundleFlag == BUNDLE)
            && (pIf->MPInfo.BundleInfo.BWMProtocolInfo.Protocol ==
                BAP_PROTOCOL))
        {
            return ((tBAPInfo *) pIf->MPInfo.BundleInfo.BWMProtocolInfo.
                    pProtocolInfo);
        }
    }
    return (NULL);
}

/* LOW LEVEL Routines for Table : PppBAPConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppBAPConfigTable
 Input       :  The Indices
                PppBAPConfigIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppBAPConfigTable (INT4 i4PppBAPConfigIfIndex)
{
    if (SNMPGetBAPInfoPtr (i4PppBAPConfigIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppBAPConfigTable
 Input       :  The Indices
                PppBAPConfigIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppBAPConfigTable (INT4 *pi4PppBAPConfigIfIndex)
{
    if (nmhGetFirstIndex (pi4PppBAPConfigIfIndex, BACPInstance, FIRST_INST) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppBAPConfigTable
 Input       :  The Indices
                PppBAPConfigIfIndex
                nextPppBAPConfigIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppBAPConfigTable (INT4 i4PppBAPConfigIfIndex,
                                  INT4 *pi4NextPppBAPConfigIfIndex)
{
    if (nmhGetNextIndex (&i4PppBAPConfigIfIndex, BACPInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextPppBAPConfigIfIndex = i4PppBAPConfigIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppBAPConfigRetransmitTimeOutValue
 Input       :  The Indices
                PppBAPConfigIfIndex

                The Object 
                retValPppBAPConfigRetransmitTimeOutValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppBAPConfigRetransmitTimeOutValue (INT4 i4PppBAPConfigIfIndex,
                                          INT4
                                          *pi4RetValPppBAPConfigRetransmitTimeOutValue)
{
    tBAPInfo           *pBAPInfo;

    if ((pBAPInfo = SNMPGetBAPInfoPtr (i4PppBAPConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppBAPConfigRetransmitTimeOutValue =
        pBAPInfo->RetransmitTimeOutVal;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppBAPConfigMaxReTransmits
 Input       :  The Indices
                PppBAPConfigIfIndex

                The Object 
                retValPppBAPConfigMaxReTransmits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppBAPConfigMaxReTransmits (INT4 i4PppBAPConfigIfIndex,
                                  INT4 *pi4RetValPppBAPConfigMaxReTransmits)
{
    tBAPInfo           *pBAPInfo;

    if ((pBAPInfo = SNMPGetBAPInfoPtr (i4PppBAPConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppBAPConfigMaxReTransmits = pBAPInfo->MaxRetransmits;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppBAPConfigRetryRequestFlag
 Input       :  The Indices
                PppBAPConfigIfIndex

                The Object 
                retValPppBAPConfigRetryRequestFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppBAPConfigRetryRequestFlag (INT4 i4PppBAPConfigIfIndex,
                                    INT4 *pi4RetValPppBAPConfigRetryRequestFlag)
{
    tBAPInfo           *pBAPInfo;

    if ((pBAPInfo = SNMPGetBAPInfoPtr (i4PppBAPConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppBAPConfigRetryRequestFlag =
        (pBAPInfo->RetryTimeOutVal == 0) ? RETRY_FALSE : RETRY_TRUE;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppBAPConfigRetryRequestTimeOutValue
 Input       :  The Indices
                PppBAPConfigIfIndex

                The Object 
                retValPppBAPConfigRetryRequestTimeOutValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppBAPConfigRetryRequestTimeOutValue (INT4 i4PppBAPConfigIfIndex,
                                            INT4
                                            *pi4RetValPppBAPConfigRetryRequestTimeOutValue)
{
    tBAPInfo           *pBAPInfo;

    if ((pBAPInfo = SNMPGetBAPInfoPtr (i4PppBAPConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppBAPConfigRetryRequestTimeOutValue = pBAPInfo->RetryTimeOutVal;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppBAPConfigRemoteAllowedRequestTypes
 Input       :  The Indices
                PppBAPConfigIfIndex

                The Object 
                retValPppBAPConfigRemoteAllowedRequestTypes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppBAPConfigRemoteAllowedRequestTypes (INT4 i4PppBAPConfigIfIndex,
                                             INT4
                                             *pi4RetValPppBAPConfigRemoteAllowedRequestTypes)
{
    tBAPInfo           *pBAPInfo;

    if ((pBAPInfo = SNMPGetBAPInfoPtr (i4PppBAPConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppBAPConfigRemoteAllowedRequestTypes =
        pBAPInfo->RemoteAllowedReqTypes;
    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppBAPConfigRetransmitTimeOutValue
 Input       :  The Indices
                PppBAPConfigIfIndex

                The Object 
                setValPppBAPConfigRetransmitTimeOutValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppBAPConfigRetransmitTimeOutValue (INT4 i4PppBAPConfigIfIndex,
                                          INT4
                                          i4SetValPppBAPConfigRetransmitTimeOutValue)
{
    tBAPInfo           *pBAPInfo;

    if ((pBAPInfo = SNMPGetBAPInfoPtr (i4PppBAPConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pBAPInfo->RetransmitTimeOutVal =
        (UINT2) i4SetValPppBAPConfigRetransmitTimeOutValue;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppBAPConfigMaxReTransmits
 Input       :  The Indices
                PppBAPConfigIfIndex

                The Object 
                setValPppBAPConfigMaxReTransmits
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppBAPConfigMaxReTransmits (INT4 i4PppBAPConfigIfIndex,
                                  INT4 i4SetValPppBAPConfigMaxReTransmits)
{
    tBAPInfo           *pBAPInfo;

    if ((pBAPInfo = SNMPGetBAPInfoPtr (i4PppBAPConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pBAPInfo->MaxRetransmits = (UINT2) i4SetValPppBAPConfigMaxReTransmits;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppBAPConfigRetryRequestFlag
 Input       :  The Indices
                PppBAPConfigIfIndex

                The Object 
                setValPppBAPConfigRetryRequestFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppBAPConfigRetryRequestFlag (INT4 i4PppBAPConfigIfIndex,
                                    INT4 i4SetValPppBAPConfigRetryRequestFlag)
{
    tBAPInfo           *pBAPInfo;

    if ((pBAPInfo = SNMPGetBAPInfoPtr (i4PppBAPConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pBAPInfo->RetryTimeOutVal =
        (i4SetValPppBAPConfigRetryRequestFlag ==
         RETRY_TRUE) ? BAP_DEF_TIME_OUT_VAL : 0;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppBAPConfigRetryRequestTimeOutValue
 Input       :  The Indices
                PppBAPConfigIfIndex

                The Object 
                setValPppBAPConfigRetryRequestTimeOutValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppBAPConfigRetryRequestTimeOutValue (INT4 i4PppBAPConfigIfIndex,
                                            INT4
                                            i4SetValPppBAPConfigRetryRequestTimeOutValue)
{
    tBAPInfo           *pBAPInfo;

    if (((pBAPInfo = SNMPGetBAPInfoPtr (i4PppBAPConfigIfIndex)) == NULL)
        || (pBAPInfo->RetryTimeOutVal == 0))
    {
        return (SNMP_FAILURE);
    }
    pBAPInfo->RetryTimeOutVal =
        (UINT2) i4SetValPppBAPConfigRetryRequestTimeOutValue;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppBAPConfigRemoteAllowedRequestTypes
 Input       :  The Indices
                PppBAPConfigIfIndex

                The Object 
                setValPppBAPConfigRemoteAllowedRequestTypes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppBAPConfigRemoteAllowedRequestTypes (INT4 i4PppBAPConfigIfIndex,
                                             INT4
                                             i4SetValPppBAPConfigRemoteAllowedRequestTypes)
{
    tBAPInfo           *pBAPInfo;

    if ((pBAPInfo = SNMPGetBAPInfoPtr (i4PppBAPConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pBAPInfo->RemoteAllowedReqTypes =
        (UINT2) i4SetValPppBAPConfigRemoteAllowedRequestTypes;
    return (SNMP_SUCCESS);

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppBAPConfigRetransmitTimeOutValue
 Input       :  The Indices
                PppBAPConfigIfIndex

                The Object 
                testValPppBAPConfigRetransmitTimeOutValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppBAPConfigRetransmitTimeOutValue (UINT4 *pu4ErrorCode,
                                             INT4 i4PppBAPConfigIfIndex,
                                             INT4
                                             i4TestValPppBAPConfigRetransmitTimeOutValue)
{

    if ((SNMPGetBAPInfoPtr (i4PppBAPConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppBAPConfigRetransmitTimeOutValue < 1
        || i4TestValPppBAPConfigRetransmitTimeOutValue > 60)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppBAPConfigMaxReTransmits
 Input       :  The Indices
                PppBAPConfigIfIndex

                The Object 
                testValPppBAPConfigMaxReTransmits
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppBAPConfigMaxReTransmits (UINT4 *pu4ErrorCode,
                                     INT4 i4PppBAPConfigIfIndex,
                                     INT4 i4TestValPppBAPConfigMaxReTransmits)
{

    if ((SNMPGetBAPInfoPtr (i4PppBAPConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppBAPConfigMaxReTransmits < 1
        || i4TestValPppBAPConfigMaxReTransmits > 30)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppBAPConfigRetryRequestFlag
 Input       :  The Indices
                PppBAPConfigIfIndex

                The Object 
                testValPppBAPConfigRetryRequestFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppBAPConfigRetryRequestFlag (UINT4 *pu4ErrorCode,
                                       INT4 i4PppBAPConfigIfIndex,
                                       INT4
                                       i4TestValPppBAPConfigRetryRequestFlag)
{

    if ((SNMPGetBAPInfoPtr (i4PppBAPConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((i4TestValPppBAPConfigRetryRequestFlag != RETRY_TRUE)
        && (i4TestValPppBAPConfigRetryRequestFlag != RETRY_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppBAPConfigRetryRequestTimeOutValue
 Input       :  The Indices
                PppBAPConfigIfIndex

                The Object 
                testValPppBAPConfigRetryRequestTimeOutValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppBAPConfigRetryRequestTimeOutValue (UINT4 *pu4ErrorCode,
                                               INT4 i4PppBAPConfigIfIndex,
                                               INT4
                                               i4TestValPppBAPConfigRetryRequestTimeOutValue)
{
    if ((SNMPGetBAPInfoPtr (i4PppBAPConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppBAPConfigRetryRequestTimeOutValue < 1
        || i4TestValPppBAPConfigRetryRequestTimeOutValue > 60)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppBAPConfigRemoteAllowedRequestTypes
 Input       :  The Indices
                PppBAPConfigIfIndex

                The Object 
                testValPppBAPConfigRemoteAllowedRequestTypes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppBAPConfigRemoteAllowedRequestTypes (UINT4 *pu4ErrorCode,
                                                INT4 i4PppBAPConfigIfIndex,
                                                INT4
                                                i4TestValPppBAPConfigRemoteAllowedRequestTypes)
{
    if ((SNMPGetBAPInfoPtr (i4PppBAPConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppBAPConfigRemoteAllowedRequestTypes < MIN_ALLOWED_REQ_TYPE
        || i4TestValPppBAPConfigRemoteAllowedRequestTypes >
        MAX_ALLOWED_REQ_TYPE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/* LOW LEVEL Routines for Table : PppBAPStatusTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppBAPStatusTable
 Input       :  The Indices
                PppBAPStatIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppBAPStatusTable (INT4 i4PppBAPStatIfIndex)
{
    if (SNMPGetBAPInfoPtr (i4PppBAPStatIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppBAPStatusTable
 Input       :  The Indices
                PppBAPStatIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppBAPStatusTable (INT4 *pi4PppBAPStatIfIndex)
{
    if (nmhGetFirstIndex (pi4PppBAPStatIfIndex, BACPInstance, FIRST_INST) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppBAPStatusTable
 Input       :  The Indices
                PppBAPStatIfIndex
                nextPppBAPStatIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppBAPStatusTable (INT4 i4PppBAPStatIfIndex,
                                  INT4 *pi4NextPppBAPStatIfIndex)
{
    if (nmhGetNextIndex (&i4PppBAPStatIfIndex, BACPInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextPppBAPStatIfIndex = i4PppBAPStatIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppBAPStatInCallReqs
 Input       :  The Indices
                PppBAPStatIfIndex

                The Object 
                retValPppBAPStatInCallReqs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppBAPStatInCallReqs (INT4 i4PppBAPStatIfIndex,
                            UINT4 *pu4RetValPppBAPStatInCallReqs)
{
    tBAPInfo           *pBAPInfo;

    if ((pBAPInfo = SNMPGetBAPInfoPtr (i4PppBAPStatIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValPppBAPStatInCallReqs = pBAPInfo->Stats.Incoming.CallRequests;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppBAPStatInCallBackReqs
 Input       :  The Indices
                PppBAPStatIfIndex

                The Object 
                retValPppBAPStatInCallBackReqs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppBAPStatInCallBackReqs (INT4 i4PppBAPStatIfIndex,
                                UINT4 *pu4RetValPppBAPStatInCallBackReqs)
{
    tBAPInfo           *pBAPInfo;

    if ((pBAPInfo = SNMPGetBAPInfoPtr (i4PppBAPStatIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValPppBAPStatInCallBackReqs =
        pBAPInfo->Stats.Incoming.CallBackRequests;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppBAPStatInLinkDropReqs
 Input       :  The Indices
                PppBAPStatIfIndex

                The Object 
                retValPppBAPStatInLinkDropReqs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppBAPStatInLinkDropReqs (INT4 i4PppBAPStatIfIndex,
                                UINT4 *pu4RetValPppBAPStatInLinkDropReqs)
{
    tBAPInfo           *pBAPInfo;

    if ((pBAPInfo = SNMPGetBAPInfoPtr (i4PppBAPStatIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValPppBAPStatInLinkDropReqs =
        pBAPInfo->Stats.Incoming.LinkDropRequests;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppBAPStatInSuccessCallReqs
 Input       :  The Indices
                PppBAPStatIfIndex

                The Object 
                retValPppBAPStatInSuccessCallReqs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppBAPStatInSuccessCallReqs (INT4 i4PppBAPStatIfIndex,
                                   UINT4 *pu4RetValPppBAPStatInSuccessCallReqs)
{
    tBAPInfo           *pBAPInfo;

    if ((pBAPInfo = SNMPGetBAPInfoPtr (i4PppBAPStatIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValPppBAPStatInSuccessCallReqs =
        pBAPInfo->Stats.Incoming.CallReqsAcked;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppBAPStatInSuccessCallBackReqs
 Input       :  The Indices
                PppBAPStatIfIndex

                The Object 
                retValPppBAPStatInSuccessCallBackReqs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppBAPStatInSuccessCallBackReqs (INT4 i4PppBAPStatIfIndex,
                                       UINT4
                                       *pu4RetValPppBAPStatInSuccessCallBackReqs)
{
    tBAPInfo           *pBAPInfo;

    if ((pBAPInfo = SNMPGetBAPInfoPtr (i4PppBAPStatIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValPppBAPStatInSuccessCallBackReqs =
        pBAPInfo->Stats.Incoming.CallBackReqsAcked;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppBAPStatInSuccessLinkDropReqs
 Input       :  The Indices
                PppBAPStatIfIndex

                The Object 
                retValPppBAPStatInSuccessLinkDropReqs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppBAPStatInSuccessLinkDropReqs (INT4 i4PppBAPStatIfIndex,
                                       UINT4
                                       *pu4RetValPppBAPStatInSuccessLinkDropReqs)
{
    tBAPInfo           *pBAPInfo;

    if ((pBAPInfo = SNMPGetBAPInfoPtr (i4PppBAPStatIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValPppBAPStatInSuccessLinkDropReqs =
        pBAPInfo->Stats.Incoming.LinkDropReqsAcked;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppBAPStatOutCallReqs
 Input       :  The Indices
                PppBAPStatIfIndex

                The Object 
                retValPppBAPStatOutCallReqs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppBAPStatOutCallReqs (INT4 i4PppBAPStatIfIndex,
                             UINT4 *pu4RetValPppBAPStatOutCallReqs)
{
    tBAPInfo           *pBAPInfo;

    if ((pBAPInfo = SNMPGetBAPInfoPtr (i4PppBAPStatIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValPppBAPStatOutCallReqs = pBAPInfo->Stats.Outgoing.CallRequests;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppBAPStatOutCallBackReqs
 Input       :  The Indices
                PppBAPStatIfIndex

                The Object 
                retValPppBAPStatOutCallBackReqs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppBAPStatOutCallBackReqs (INT4 i4PppBAPStatIfIndex,
                                 UINT4 *pu4RetValPppBAPStatOutCallBackReqs)
{
    tBAPInfo           *pBAPInfo;

    if ((pBAPInfo = SNMPGetBAPInfoPtr (i4PppBAPStatIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValPppBAPStatOutCallBackReqs =
        pBAPInfo->Stats.Outgoing.CallBackRequests;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppBAPStatOutLinkDropReqs
 Input       :  The Indices
                PppBAPStatIfIndex

                The Object 
                retValPppBAPStatOutLinkDropReqs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppBAPStatOutLinkDropReqs (INT4 i4PppBAPStatIfIndex,
                                 UINT4 *pu4RetValPppBAPStatOutLinkDropReqs)
{
    tBAPInfo           *pBAPInfo;

    if ((pBAPInfo = SNMPGetBAPInfoPtr (i4PppBAPStatIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValPppBAPStatOutLinkDropReqs =
        pBAPInfo->Stats.Outgoing.LinkDropRequests;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppBAPStatOutSuccessCallReqs
 Input       :  The Indices
                PppBAPStatIfIndex

                The Object 
                retValPppBAPStatOutSuccessCallReqs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppBAPStatOutSuccessCallReqs (INT4 i4PppBAPStatIfIndex,
                                    UINT4
                                    *pu4RetValPppBAPStatOutSuccessCallReqs)
{
    tBAPInfo           *pBAPInfo;

    if ((pBAPInfo = SNMPGetBAPInfoPtr (i4PppBAPStatIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValPppBAPStatOutSuccessCallReqs =
        pBAPInfo->Stats.Outgoing.CallReqsAcked;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppBAPStatOutSuccessCallBackReqs
 Input       :  The Indices
                PppBAPStatIfIndex

                The Object 
                retValPppBAPStatOutSuccessCallBackReqs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppBAPStatOutSuccessCallBackReqs (INT4 i4PppBAPStatIfIndex,
                                        UINT4
                                        *pu4RetValPppBAPStatOutSuccessCallBackReqs)
{
    tBAPInfo           *pBAPInfo;

    if ((pBAPInfo = SNMPGetBAPInfoPtr (i4PppBAPStatIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValPppBAPStatOutSuccessCallBackReqs =
        pBAPInfo->Stats.Outgoing.CallBackReqsAcked;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppBAPStatOutSuccessLinkDropReqs
 Input       :  The Indices
                PppBAPStatIfIndex

                The Object 
                retValPppBAPStatOutSuccessLinkDropReqs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppBAPStatOutSuccessLinkDropReqs (INT4 i4PppBAPStatIfIndex,
                                        UINT4
                                        *pu4RetValPppBAPStatOutSuccessLinkDropReqs)
{
    tBAPInfo           *pBAPInfo;

    if ((pBAPInfo = SNMPGetBAPInfoPtr (i4PppBAPStatIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValPppBAPStatOutSuccessLinkDropReqs =
        pBAPInfo->Stats.Outgoing.LinkDropReqsAcked;
    return (SNMP_SUCCESS);

}
