/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdipcdb.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDIPCDB_H
#define _STDIPCDB_H

UINT1 PppIpConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};

UINT4 stdipc [] ={1,3,6,1,2,1,10,23,3};
tSNMP_OID_TYPE stdipcOID = {9, stdipc};


UINT4 PppIpConfigAdminStatus [ ] ={1,3,6,1,2,1,10,23,3,1,1,1};
UINT4 PppIpConfigCompression [ ] ={1,3,6,1,2,1,10,23,3,1,1,2};


tMbDbEntry stdipcMibEntry[]= {

{{12,PppIpConfigAdminStatus}, GetNextIndexPppIpConfigTable, PppIpConfigAdminStatusGet, PppIpConfigAdminStatusSet, PppIpConfigAdminStatusTest, PppIpConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PppIpConfigTableINDEX, 1, 0, 0, NULL},

{{12,PppIpConfigCompression}, GetNextIndexPppIpConfigTable, PppIpConfigCompressionGet, PppIpConfigCompressionSet, PppIpConfigCompressionTest, PppIpConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PppIpConfigTableINDEX, 1, 0, 0, "1"},
};
tMibData stdipcEntry = { 2, stdipcMibEntry };
#endif /* _STDIPCDB_H */

