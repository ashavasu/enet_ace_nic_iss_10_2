/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppteslow.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

/* Proto Validate Index Instance for PppTestConfigTable. */
INT1
nmhValidateIndexInstancePppTestConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppTestConfigTable  */

INT1
nmhGetFirstIndexPppTestConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppTestConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppTestConfigMTU ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestConfigSpeed ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestConfigDiscardTest ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestConfigIdentification ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppTestConfigMTU ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestConfigSpeed ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestConfigDiscardTest ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestConfigIdentification ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppTestConfigMTU ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestConfigSpeed ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestConfigDiscardTest ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestConfigIdentification ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for PppTestIpxTable. */
INT1
nmhValidateIndexInstancePppTestIpxTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppTestIpxTable  */

INT1
nmhGetFirstIndexPppTestIpxTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppTestIpxTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppTestIpxLocalNetNumber ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppTestIpxLocalNodeNumber ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppTestIpxRemoteNodeNumber ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppTestIpxRoutingProtocol ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestIpxRoutingProtoSupport ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestIpxRouterName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppTestIpxNetNumNegFlag ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestIpxNodeNumNegFlag ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestIpxTelebitCompAvailable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestIpxWanSupport ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppTestIpxLocalNetNumber ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppTestIpxLocalNodeNumber ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppTestIpxRemoteNodeNumber ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppTestIpxRoutingProtocol ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestIpxRoutingProtoSupport ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestIpxRouterName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppTestIpxNetNumNegFlag ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestIpxNodeNumNegFlag ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestIpxTelebitCompAvailable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestIpxWanSupport ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppTestIpxLocalNetNumber ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppTestIpxLocalNodeNumber ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppTestIpxRemoteNodeNumber ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppTestIpxRoutingProtocol ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestIpxRoutingProtoSupport ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestIpxRouterName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppTestIpxNetNumNegFlag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestIpxNodeNumNegFlag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestIpxTelebitCompAvailable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestIpxWanSupport ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for PppTestMpConfigTable. */
INT1
nmhValidateIndexInstancePppTestMpConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppTestMpConfigTable  */

INT1
nmhGetFirstIndexPppTestMpConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppTestMpConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppTestMpConfigTd ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppTestMpConfigTd ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppTestMpConfigTd ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for PppTestRMConfigTable. */
INT1
nmhValidateIndexInstancePppTestRMConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppTestRMConfigTable  */

INT1
nmhGetFirstIndexPppTestRMConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppTestRMConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppTestRMConfigLocFirstLinkNumber ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppTestRMConfigRemFirstLinkNumber ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppTestRMConfigRequestType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestRMConfigIsPhoneNumberNeeded ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestRMConfigNumberOfPhoneNumbers ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestRMConfigPhoneNumber ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppTestRMConfigLinkType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestRMConfigLinkSpeed ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestRMConfigLinkIndexToBeDeleted ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestRMConfigRequestId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestRMConfigRequester ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestRMConfigAttemptedPhoneNumber ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppTestRMConfigCallStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestRMConfigActionTaken ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestRMConfigResponseToSend ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestRMConfigAddLinkToBundle ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestRMConfigDeleteLinkFromBundle ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestRMConfigCallNotification ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestRMConfigVerifyPeerRequest ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestRMConfigIsInitiator ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppTestRMConfigLocFirstLinkNumber ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppTestRMConfigRemFirstLinkNumber ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppTestRMConfigRequestType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestRMConfigIsPhoneNumberNeeded ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestRMConfigNumberOfPhoneNumbers ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestRMConfigPhoneNumber ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppTestRMConfigLinkType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestRMConfigLinkSpeed ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestRMConfigLinkIndexToBeDeleted ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestRMConfigRequestId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestRMConfigRequester ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestRMConfigAttemptedPhoneNumber ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppTestRMConfigCallStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestRMConfigActionTaken ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestRMConfigResponseToSend ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestRMConfigAddLinkToBundle ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestRMConfigDeleteLinkFromBundle ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestRMConfigCallNotification ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestRMConfigVerifyPeerRequest ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestRMConfigIsInitiator ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppTestRMConfigLocFirstLinkNumber ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppTestRMConfigRemFirstLinkNumber ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppTestRMConfigRequestType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestRMConfigIsPhoneNumberNeeded ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestRMConfigNumberOfPhoneNumbers ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestRMConfigPhoneNumber ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppTestRMConfigLinkType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestRMConfigLinkSpeed ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestRMConfigLinkIndexToBeDeleted ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestRMConfigRequestId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestRMConfigRequester ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestRMConfigAttemptedPhoneNumber ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppTestRMConfigCallStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestRMConfigActionTaken ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestRMConfigResponseToSend ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestRMConfigAddLinkToBundle ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestRMConfigDeleteLinkFromBundle ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestRMConfigCallNotification ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestRMConfigVerifyPeerRequest ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestRMConfigIsInitiator ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for PppTestRMStatusTable. */
INT1
nmhValidateIndexInstancePppTestRMStatusTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppTestRMStatusTable  */

INT1
nmhGetFirstIndexPppTestRMStatusTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppTestRMStatusTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppTestRMStatusRequestId ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppTestRMStatusIfIndex ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));
