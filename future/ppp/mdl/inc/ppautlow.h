/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppautlow.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

/* Proto Validate Index Instance for PppExtSecurityConfigTable. */
INT1
nmhValidateIndexInstancePppExtSecurityConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppExtSecurityConfigTable  */

INT1
nmhGetFirstIndexPppExtSecurityConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppExtSecurityConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppExtSecurityConfigReChallingInterval ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppExtSecurityConfigReChallingInterval ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppExtSecurityConfigReChallingInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for PppExtSecuritySecretsTable. */
INT1
nmhValidateIndexInstancePppExtSecuritySecretsTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppExtSecuritySecretsTable  */

INT1
nmhGetFirstIndexPppExtSecuritySecretsTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppExtSecuritySecretsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppExtSecuritySecretsCharSet ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetPppExtSecuritySecretsLangTag ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppExtSecuritySecretsCharSet ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetPppExtSecuritySecretsLangTag ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppExtSecuritySecretsCharSet ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2PppExtSecuritySecretsLangTag ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Proto Validate Index Instance for PppExtSecurityEapConfigTable. */
INT1
nmhValidateIndexInstancePppExtSecurityEapConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppExtSecurityEapConfigTable  */

INT1
nmhGetFirstIndexPppExtSecurityEapConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppExtSecurityEapConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppExtSecurityEapConfigAuthProtocol ARG_LIST((INT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetPppExtSecurityEapConfigMaxIdentityRetries ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtSecurityEapConfigFailedIdentityRequests ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtSecurityEapConfigSendIdentityRequest ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppExtSecurityEapConfigAuthProtocol ARG_LIST((INT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetPppExtSecurityEapConfigMaxIdentityRetries ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtSecurityEapConfigSendIdentityRequest ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppExtSecurityEapConfigAuthProtocol ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2PppExtSecurityEapConfigMaxIdentityRetries ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtSecurityEapConfigSendIdentityRequest ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
