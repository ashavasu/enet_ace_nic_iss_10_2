/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdautdb.h,v 1.2 2010/12/21 08:36:48 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDAUTDB_H
#define _STDAUTDB_H

UINT1 PppSecuritySecretsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};

UINT4 stdaut [] ={1,3,6,1,2,1,10,23,2};
tSNMP_OID_TYPE stdautOID = {9, stdaut};


UINT4 PppSecuritySecretsLink [ ] ={1,3,6,1,2,1,10,23,2,2,1,1};
UINT4 PppSecuritySecretsIdIndex [ ] ={1,3,6,1,2,1,10,23,2,2,1,2};
UINT4 PppSecuritySecretsDirection [ ] ={1,3,6,1,2,1,10,23,2,2,1,3};
UINT4 PppSecuritySecretsProtocol [ ] ={1,3,6,1,2,1,10,23,2,2,1,4};
UINT4 PppSecuritySecretsIdentity [ ] ={1,3,6,1,2,1,10,23,2,2,1,5};
UINT4 PppSecuritySecretsSecret [ ] ={1,3,6,1,2,1,10,23,2,2,1,6};
UINT4 PppSecuritySecretsStatus [ ] ={1,3,6,1,2,1,10,23,2,2,1,7};


tMbDbEntry stdautMibEntry[]= {

{{12,PppSecuritySecretsLink}, GetNextIndexPppSecuritySecretsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, PppSecuritySecretsTableINDEX, 2, 0, 0, NULL},

{{12,PppSecuritySecretsIdIndex}, GetNextIndexPppSecuritySecretsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, PppSecuritySecretsTableINDEX, 2, 0, 0, NULL},

{{12,PppSecuritySecretsDirection}, GetNextIndexPppSecuritySecretsTable, PppSecuritySecretsDirectionGet, PppSecuritySecretsDirectionSet, PppSecuritySecretsDirectionTest, PppSecuritySecretsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PppSecuritySecretsTableINDEX, 2, 0, 0, NULL},

{{12,PppSecuritySecretsProtocol}, GetNextIndexPppSecuritySecretsTable, PppSecuritySecretsProtocolGet, PppSecuritySecretsProtocolSet, PppSecuritySecretsProtocolTest, PppSecuritySecretsTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, PppSecuritySecretsTableINDEX, 2, 0, 0, NULL},

{{12,PppSecuritySecretsIdentity}, GetNextIndexPppSecuritySecretsTable, PppSecuritySecretsIdentityGet, PppSecuritySecretsIdentitySet, PppSecuritySecretsIdentityTest, PppSecuritySecretsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, PppSecuritySecretsTableINDEX, 2, 0, 0, NULL},

{{12,PppSecuritySecretsSecret}, GetNextIndexPppSecuritySecretsTable, PppSecuritySecretsSecretGet, PppSecuritySecretsSecretSet, PppSecuritySecretsSecretTest, PppSecuritySecretsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, PppSecuritySecretsTableINDEX, 2, 0, 0, NULL},

{{12,PppSecuritySecretsStatus}, GetNextIndexPppSecuritySecretsTable, PppSecuritySecretsStatusGet, PppSecuritySecretsStatusSet, PppSecuritySecretsStatusTest, PppSecuritySecretsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PppSecuritySecretsTableINDEX, 2, 0, 0, "2"},
};
tMibData stdautEntry = { 7, stdautMibEntry };
#endif /* _STDAUTDB_H */

