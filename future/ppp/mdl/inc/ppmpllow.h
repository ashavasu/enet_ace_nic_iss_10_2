/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppmpllow.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

/* Proto Validate Index Instance for PppMplsTable. */
INT1
nmhValidateIndexInstancePppMplsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppMplsTable  */

INT1
nmhGetFirstIndexPppMplsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppMplsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppMplsOperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppMplsAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppMplsRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppMplsAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppMplsRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppMplsAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppMplsRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
