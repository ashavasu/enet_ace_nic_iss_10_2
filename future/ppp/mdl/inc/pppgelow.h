/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppgelow.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppExtIPAddressPoolSelector ARG_LIST((INT4 *));

INT1
nmhGetPppDebugLevelMask ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppExtIPAddressPoolSelector ARG_LIST((INT4 ));

INT1
nmhSetPppDebugLevelMask ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppExtIPAddressPoolSelector ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhDepv2PppDebugLevelMask ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhTestv2PppDebugLevelMask ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2PppExtIPAddressPoolSelector ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for PppIfTable. */
INT1
nmhValidateIndexInstancePppIfTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppIfTable  */

INT1
nmhGetFirstIndexPppIfTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppIfTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppIfRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppIfRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppIfRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for MpIfTable. */
INT1
nmhValidateIndexInstanceMpIfTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for MpIfTable  */

INT1
nmhGetFirstIndexMpIfTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMpIfTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMpIfRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMpIfRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MpIfRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
