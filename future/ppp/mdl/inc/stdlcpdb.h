/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdlcpdb.h,v 1.2 2010/12/21 08:36:48 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDLCPDB_H
#define _STDLCPDB_H

UINT1 PppLinkConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};

UINT4 stdlcp [] ={1,3,6,1,2,1,10,23};
tSNMP_OID_TYPE stdlcpOID = {8, stdlcp};


UINT4 PppLinkConfigInitialMRU [ ] ={1,3,6,1,2,1,10,23,1,1,1,1,1};
UINT4 PppLinkConfigMagicNumber [ ] ={1,3,6,1,2,1,10,23,1,1,1,1,2};
UINT4 PppLinkConfigFcsSize [ ] ={1,3,6,1,2,1,10,23,1,1,1,1,3};


tMbDbEntry stdlcpMibEntry[]= {

{{13,PppLinkConfigInitialMRU}, GetNextIndexPppLinkConfigTable, PppLinkConfigInitialMRUGet, PppLinkConfigInitialMRUSet, PppLinkConfigInitialMRUTest, PppLinkConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, PppLinkConfigTableINDEX, 1, 0, 0, "1500"},

{{13,PppLinkConfigMagicNumber}, GetNextIndexPppLinkConfigTable, PppLinkConfigMagicNumberGet, PppLinkConfigMagicNumberSet, PppLinkConfigMagicNumberTest, PppLinkConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PppLinkConfigTableINDEX, 1, 0, 0, "1"},

{{13,PppLinkConfigFcsSize}, GetNextIndexPppLinkConfigTable, PppLinkConfigFcsSizeGet, PppLinkConfigFcsSizeSet, PppLinkConfigFcsSizeTest, PppLinkConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, PppLinkConfigTableINDEX, 1, 0, 0, "16"},
};
tMibData stdlcpEntry = { 3, stdlcpMibEntry };
#endif /* _STDLCPDB_H */

