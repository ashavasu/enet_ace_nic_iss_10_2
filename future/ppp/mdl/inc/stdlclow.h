/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: stdlclow.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 ********************************************************************/

/* Proto Validate Index Instance for PppLinkConfigTable. */
INT1
nmhValidateIndexInstancePppLinkConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppLinkConfigTable  */

INT1
nmhGetFirstIndexPppLinkConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppLinkConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppLinkConfigInitialMRU ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppLinkConfigMagicNumber ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppLinkConfigFcsSize ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppLinkConfigInitialMRU ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppLinkConfigMagicNumber ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppLinkConfigFcsSize ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppLinkConfigInitialMRU ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppLinkConfigMagicNumber ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppLinkConfigFcsSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2PppLinkConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
