/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: stdiplow.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 ********************************************************************/

/* Proto Validate Index Instance for PppIpTable. */
INT1
nmhValidateIndexInstancePppIpTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppIpTable  */

INT1
nmhGetFirstIndexPppIpTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppIpTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppIpOperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppIpLocalToRemoteCompressionProtocol ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppIpRemoteToLocalCompressionProtocol ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppIpRemoteMaxSlotId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppIpLocalMaxSlotId ARG_LIST((INT4 ,INT4 *));

/* Proto Validate Index Instance for PppIpConfigTable. */
INT1
nmhValidateIndexInstancePppIpConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppIpConfigTable  */

INT1
nmhGetFirstIndexPppIpConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppIpConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppIpConfigAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppIpConfigCompression ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppIpConfigAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppIpConfigCompression ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppIpConfigAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppIpConfigCompression ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2PppIpConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
