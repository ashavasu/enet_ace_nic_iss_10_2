/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: stdlcpwr.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 ********************************************************************/

#ifndef _STDLCPWR_H
#define _STDLCPWR_H
INT4 GetNextIndexPppLinkConfigTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterSTDLCP(VOID);

VOID UnRegisterSTDLCP(VOID);
INT4 PppLinkConfigInitialMRUGet(tSnmpIndex *, tRetVal *);
INT4 PppLinkConfigMagicNumberGet(tSnmpIndex *, tRetVal *);
INT4 PppLinkConfigFcsSizeGet(tSnmpIndex *, tRetVal *);
INT4 PppLinkConfigInitialMRUSet(tSnmpIndex *, tRetVal *);
INT4 PppLinkConfigMagicNumberSet(tSnmpIndex *, tRetVal *);
INT4 PppLinkConfigFcsSizeSet(tSnmpIndex *, tRetVal *);
INT4 PppLinkConfigInitialMRUTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppLinkConfigMagicNumberTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppLinkConfigFcsSizeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppLinkConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _STDLCPWR_H */
