/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppccplow.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

/* Proto Validate Index Instance for PppCCPConfigTable. */
INT1
nmhValidateIndexInstancePppCCPConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppCCPConfigTable  */

INT1
nmhGetFirstIndexPppCCPConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppCCPConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppCCPConfigAdminStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppCCPConfigAdminStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppCCPConfigAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for PppCCPTxConfigTable. */
INT1
nmhValidateIndexInstancePppCCPTxConfigTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppCCPTxConfigTable  */

INT1
nmhGetFirstIndexPppCCPTxConfigTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppCCPTxConfigTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppCCPTxConfigOptionNum ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppCCPTxConfigOptionNum ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppCCPTxConfigOptionNum ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Proto Validate Index Instance for PppCCPRxConfigTable. */
INT1
nmhValidateIndexInstancePppCCPRxConfigTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppCCPRxConfigTable  */

INT1
nmhGetFirstIndexPppCCPRxConfigTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppCCPRxConfigTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppCCPRxConfigAdminStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppCCPRxConfigAdminStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppCCPRxConfigAdminStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Proto Validate Index Instance for PppCCPStatsTable. */
INT1
nmhValidateIndexInstancePppCCPStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppCCPStatsTable  */

INT1
nmhGetFirstIndexPppCCPStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppCCPStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppCCPStatsOperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppCCPStatsRxResetReqs ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppCCPStatsTxResetReqs ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppCCPStatsRxResetAcks ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppCCPStatsTxResetAcks ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppCCPStatsTxNegotiatedOption ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppCCPStatsRxNegotiatedOption ARG_LIST((INT4 ,INT4 *));
