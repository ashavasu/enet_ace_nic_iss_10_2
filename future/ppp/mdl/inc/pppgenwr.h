/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppgenwr.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

#ifndef _PPPGENWR_H
#define _PPPGENWR_H

VOID RegisterPPPGEN(VOID);

VOID UnRegisterPPPGEN(VOID);
INT4 PppExtIPAddressPoolSelectorGet(tSnmpIndex *, tRetVal *);
INT4 PppDebugLevelMaskGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIPAddressPoolSelectorSet(tSnmpIndex *, tRetVal *);
INT4 PppDebugLevelMaskSet(tSnmpIndex *, tRetVal *);
INT4 PppExtIPAddressPoolSelectorTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppDebugLevelMaskTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtIPAddressPoolSelectorDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 PppDebugLevelMaskDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
#endif /* _PPPGENWR_H */
