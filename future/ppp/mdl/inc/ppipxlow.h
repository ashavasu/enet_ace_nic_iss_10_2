/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppipxlow.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

/* Proto Validate Index Instance for PppIpxTable. */
INT1
nmhValidateIndexInstancePppIpxTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppIpxTable  */

INT1
nmhGetFirstIndexPppIpxTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppIpxTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppIpxOperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppIpxLoctoRemCompProtocol ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppIpxRemtoLocCompProtocol ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppIpxLocalMaxSlotId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppIpxRemoteMaxSlotId ARG_LIST((INT4 ,INT4 *));

/* Proto Validate Index Instance for PppIpxConfigTable. */
INT1
nmhValidateIndexInstancePppIpxConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppIpxConfigTable  */

INT1
nmhGetFirstIndexPppIpxConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppIpxConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppIpxConfigAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppIpxConfigCompression ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppIpxConfigConfComplete ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppIpxConfigLocalEntityStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppIpxConfigAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppIpxConfigCompression ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppIpxConfigConfComplete ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppIpxConfigLocalEntityStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppIpxConfigAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppIpxConfigCompression ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppIpxConfigConfComplete ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppIpxConfigLocalEntityStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for PppExtIpxConfigTable. */
INT1
nmhValidateIndexInstancePppExtIpxConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppExtIpxConfigTable  */

INT1
nmhGetFirstIndexPppExtIpxConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppExtIpxConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppExtIpxConfigLocalMaxSlotId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtIpxConfigLocalCompSlotId ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppExtIpxConfigLocalMaxSlotId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtIpxConfigLocalCompSlotId ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppExtIpxConfigLocalMaxSlotId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtIpxConfigLocalCompSlotId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for PppExtIpxTable. */
INT1
nmhValidateIndexInstancePppExtIpxTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppExtIpxTable  */

INT1
nmhGetFirstIndexPppExtIpxTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppExtIpxTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppExtIpxLocalCompSlotId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtIpxRemoteCompSlotId ARG_LIST((INT4 ,INT4 *));
