/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: stdipcwr.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 ********************************************************************/

#ifndef _STDIPCWR_H
#define _STDIPCWR_H
INT4 GetNextIndexPppIpConfigTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterSTDIPC(VOID);

VOID UnRegisterSTDIPC(VOID);
INT4 PppIpConfigAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 PppIpConfigCompressionGet(tSnmpIndex *, tRetVal *);
INT4 PppIpConfigAdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 PppIpConfigCompressionSet(tSnmpIndex *, tRetVal *);
INT4 PppIpConfigAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppIpConfigCompressionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppIpConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _STDIPCWR_H */
