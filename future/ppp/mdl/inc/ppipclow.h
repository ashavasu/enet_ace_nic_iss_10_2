/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppipclow.h,v 1.2 2011/10/13 10:31:35 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

/* Proto Validate Index Instance for PppExtIpTable. */
INT1
nmhValidateIndexInstancePppExtIpTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppExtIpTable  */

INT1
nmhGetFirstIndexPppExtIpTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppExtIpTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppExtIpLocToRemoteAddress ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppExtIpRemoteToLocAddress ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppExtIpAllowVJForPeer ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtIpLocAddressNegFlag ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtIpRemoteAddressNegFlag ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtIpLocToRemotePrimaryDNSAddress ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppExtIpLocToRemoteSecondaryDNSAddress ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppExtIpLocToRemotePrimaryNBNSAddress ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppExtIpLocToRemoteSecondaryNBNSAddress ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppExtIpRemoteToLocPrimaryDNSAddress ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppExtIpRemoteToLocSecondaryDNSAddress ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppExtIpRemoteToLocPrimaryNBNSAddress ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppExtIpRemoteToLocSecondaryNBNSAddress ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppExtIpLocPrimaryDNSAddressNegFlag ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtIpLocSecondaryDNSAddressNegFlag ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtIpLocPrimaryNBNSAddressNegFlag ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtIpLocSecondaryNBNSAddressNegFlag ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtIpAllowIPHCForPeer ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtIpRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppExtIpLocToRemoteAddress ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetPppExtIpRemoteToLocAddress ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetPppExtIpAllowVJForPeer ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtIpLocAddressNegFlag ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtIpRemoteAddressNegFlag ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtIpLocToRemotePrimaryDNSAddress ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetPppExtIpLocToRemoteSecondaryDNSAddress ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetPppExtIpLocToRemotePrimaryNBNSAddress ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetPppExtIpLocToRemoteSecondaryNBNSAddress ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetPppExtIpRemoteToLocPrimaryDNSAddress ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetPppExtIpRemoteToLocSecondaryDNSAddress ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetPppExtIpRemoteToLocPrimaryNBNSAddress ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetPppExtIpRemoteToLocSecondaryNBNSAddress ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetPppExtIpLocPrimaryDNSAddressNegFlag ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtIpLocSecondaryDNSAddressNegFlag ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtIpLocPrimaryNBNSAddressNegFlag ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtIpLocSecondaryNBNSAddressNegFlag ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtIpAllowIPHCForPeer ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtIpRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppExtIpLocToRemoteAddress ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2PppExtIpRemoteToLocAddress ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2PppExtIpAllowVJForPeer ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtIpLocAddressNegFlag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtIpRemoteAddressNegFlag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtIpLocToRemotePrimaryDNSAddress ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2PppExtIpLocToRemoteSecondaryDNSAddress ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2PppExtIpLocToRemotePrimaryNBNSAddress ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2PppExtIpLocToRemoteSecondaryNBNSAddress ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2PppExtIpRemoteToLocPrimaryDNSAddress ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2PppExtIpRemoteToLocSecondaryDNSAddress ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2PppExtIpRemoteToLocPrimaryNBNSAddress ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2PppExtIpRemoteToLocSecondaryNBNSAddress ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2PppExtIpLocPrimaryDNSAddressNegFlag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtIpLocSecondaryDNSAddressNegFlag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtIpLocPrimaryNBNSAddressNegFlag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtIpLocSecondaryNBNSAddressNegFlag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtIpAllowIPHCForPeer ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtIpRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2PppExtIpTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for PppExtIpCompConfigTable. */
INT1
nmhValidateIndexInstancePppExtIpCompConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppExtIpCompConfigTable  */

INT1
nmhGetFirstIndexPppExtIpCompConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppExtIpCompConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppExtIpConfigLocalMaxSlotId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtIpConfigLocalCompSlotId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtIpConfigTcpSpace ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtIpConfigNonTcpSpace ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtIpConfigFMaxPeriod ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtIpConfigFMaxTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtIpConfigMaxHeader ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtIpConfigRtpCompression ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppExtIpConfigLocalMaxSlotId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtIpConfigLocalCompSlotId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtIpConfigTcpSpace ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtIpConfigNonTcpSpace ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtIpConfigFMaxPeriod ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtIpConfigFMaxTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtIpConfigMaxHeader ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtIpConfigRtpCompression ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppExtIpConfigLocalMaxSlotId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtIpConfigLocalCompSlotId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtIpConfigTcpSpace ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtIpConfigNonTcpSpace ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtIpConfigFMaxPeriod ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtIpConfigFMaxTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtIpConfigMaxHeader ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtIpConfigRtpCompression ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2PppExtIpCompConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for PppExtIpCompStatusTable. */
INT1
nmhValidateIndexInstancePppExtIpCompStatusTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppExtIpCompStatusTable  */

INT1
nmhGetFirstIndexPppExtIpCompStatusTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppExtIpCompStatusTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppExtIpRemoteCompSlotId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtIpLocalCompSlotId ARG_LIST((INT4 ,INT4 *));

/* Proto Validate Index Instance for PppExtIPAddressConfigTable. */
INT1
nmhValidateIndexInstancePppExtIPAddressConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppExtIPAddressConfigTable  */

INT1
nmhGetFirstIndexPppExtIPAddressConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppExtIPAddressConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppExtIPAddressPoolLowerRange ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppExtIPAddressPoolUpperRange ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppExtIPAddressPoolStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppExtIPAddressPoolLowerRange ARG_LIST((INT4  ,UINT4 ));


/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppExtIPAddressPoolLowerRange ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2PppExtIPAddressPoolUpperRange ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2PppExtIPAddressPoolStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2PppExtIPAddressConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for PppExtIPAddressStatusTable. */
INT1
nmhValidateIndexInstancePppExtIPAddressStatusTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppExtIPAddressStatusTable  */

INT1
nmhGetFirstIndexPppExtIPAddressStatusTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppExtIPAddressStatusTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppExtNoOfFreeIPAddresses ARG_LIST((INT4 ,INT4 *));
