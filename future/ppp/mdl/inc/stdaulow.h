/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: stdaulow.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

/* Proto Validate Index Instance for PppSecuritySecretsTable. */
INT1
nmhValidateIndexInstancePppSecuritySecretsTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppSecuritySecretsTable  */

INT1
nmhGetFirstIndexPppSecuritySecretsTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppSecuritySecretsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppSecuritySecretsDirection ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetPppSecuritySecretsProtocol ARG_LIST((INT4  , INT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetPppSecuritySecretsIdentity ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppSecuritySecretsSecret ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppSecuritySecretsStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppSecuritySecretsDirection ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetPppSecuritySecretsProtocol ARG_LIST((INT4  , INT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetPppSecuritySecretsIdentity ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppSecuritySecretsSecret ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppSecuritySecretsStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppSecuritySecretsDirection ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2PppSecuritySecretsProtocol ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2PppSecuritySecretsIdentity ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppSecuritySecretsSecret ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppSecuritySecretsStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2PppSecuritySecretsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
