/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdbcpdb.h,v 1.2 2010/12/21 08:36:48 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDBCPDB_H
#define _STDBCPDB_H

UINT1 PppBridgeTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 PppBridgeConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 PppBridgeMediaTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 PppBridgeMediaConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};

UINT4 stdbcp [] ={1,3,6,1,2,1,10,23,4};
tSNMP_OID_TYPE stdbcpOID = {9, stdbcp};


UINT4 PppBridgeOperStatus [ ] ={1,3,6,1,2,1,10,23,4,1,1,1};
UINT4 PppBridgeLocalToRemoteTinygramCompression [ ] ={1,3,6,1,2,1,10,23,4,1,1,2};
UINT4 PppBridgeRemoteToLocalTinygramCompression [ ] ={1,3,6,1,2,1,10,23,4,1,1,3};
UINT4 PppBridgeLocalToRemoteLanId [ ] ={1,3,6,1,2,1,10,23,4,1,1,4};
UINT4 PppBridgeRemoteToLocalLanId [ ] ={1,3,6,1,2,1,10,23,4,1,1,5};
UINT4 PppBridgeConfigAdminStatus [ ] ={1,3,6,1,2,1,10,23,4,2,1,1};
UINT4 PppBridgeConfigTinygram [ ] ={1,3,6,1,2,1,10,23,4,2,1,2};
UINT4 PppBridgeConfigRingId [ ] ={1,3,6,1,2,1,10,23,4,2,1,3};
UINT4 PppBridgeConfigLineId [ ] ={1,3,6,1,2,1,10,23,4,2,1,4};
UINT4 PppBridgeConfigLanId [ ] ={1,3,6,1,2,1,10,23,4,2,1,5};
UINT4 PppBridgeMediaMacType [ ] ={1,3,6,1,2,1,10,23,4,3,1,1};
UINT4 PppBridgeMediaLocalStatus [ ] ={1,3,6,1,2,1,10,23,4,3,1,2};
UINT4 PppBridgeMediaRemoteStatus [ ] ={1,3,6,1,2,1,10,23,4,3,1,3};
UINT4 PppBridgeMediaConfigMacType [ ] ={1,3,6,1,2,1,10,23,4,4,1,1};
UINT4 PppBridgeMediaConfigLocalStatus [ ] ={1,3,6,1,2,1,10,23,4,4,1,2};


tMbDbEntry stdbcpMibEntry[]= {

{{12,PppBridgeOperStatus}, GetNextIndexPppBridgeTable, PppBridgeOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, PppBridgeTableINDEX, 1, 0, 0, NULL},

{{12,PppBridgeLocalToRemoteTinygramCompression}, GetNextIndexPppBridgeTable, PppBridgeLocalToRemoteTinygramCompressionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, PppBridgeTableINDEX, 1, 0, 0, NULL},

{{12,PppBridgeRemoteToLocalTinygramCompression}, GetNextIndexPppBridgeTable, PppBridgeRemoteToLocalTinygramCompressionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, PppBridgeTableINDEX, 1, 0, 0, NULL},

{{12,PppBridgeLocalToRemoteLanId}, GetNextIndexPppBridgeTable, PppBridgeLocalToRemoteLanIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, PppBridgeTableINDEX, 1, 0, 0, NULL},

{{12,PppBridgeRemoteToLocalLanId}, GetNextIndexPppBridgeTable, PppBridgeRemoteToLocalLanIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, PppBridgeTableINDEX, 1, 0, 0, NULL},

{{12,PppBridgeConfigAdminStatus}, GetNextIndexPppBridgeConfigTable, PppBridgeConfigAdminStatusGet, PppBridgeConfigAdminStatusSet, PppBridgeConfigAdminStatusTest, PppBridgeConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PppBridgeConfigTableINDEX, 1, 0, 0, NULL},

{{12,PppBridgeConfigTinygram}, GetNextIndexPppBridgeConfigTable, PppBridgeConfigTinygramGet, PppBridgeConfigTinygramSet, PppBridgeConfigTinygramTest, PppBridgeConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PppBridgeConfigTableINDEX, 1, 0, 0, NULL},

{{12,PppBridgeConfigRingId}, GetNextIndexPppBridgeConfigTable, PppBridgeConfigRingIdGet, PppBridgeConfigRingIdSet, PppBridgeConfigRingIdTest, PppBridgeConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PppBridgeConfigTableINDEX, 1, 0, 0, NULL},

{{12,PppBridgeConfigLineId}, GetNextIndexPppBridgeConfigTable, PppBridgeConfigLineIdGet, PppBridgeConfigLineIdSet, PppBridgeConfigLineIdTest, PppBridgeConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PppBridgeConfigTableINDEX, 1, 0, 0, NULL},

{{12,PppBridgeConfigLanId}, GetNextIndexPppBridgeConfigTable, PppBridgeConfigLanIdGet, PppBridgeConfigLanIdSet, PppBridgeConfigLanIdTest, PppBridgeConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PppBridgeConfigTableINDEX, 1, 0, 0, NULL},

{{12,PppBridgeMediaMacType}, GetNextIndexPppBridgeMediaTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, PppBridgeMediaTableINDEX, 2, 0, 0, NULL},

{{12,PppBridgeMediaLocalStatus}, GetNextIndexPppBridgeMediaTable, PppBridgeMediaLocalStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, PppBridgeMediaTableINDEX, 2, 0, 0, NULL},

{{12,PppBridgeMediaRemoteStatus}, GetNextIndexPppBridgeMediaTable, PppBridgeMediaRemoteStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, PppBridgeMediaTableINDEX, 2, 0, 0, NULL},

{{12,PppBridgeMediaConfigMacType}, GetNextIndexPppBridgeMediaConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, PppBridgeMediaConfigTableINDEX, 2, 0, 0, NULL},

{{12,PppBridgeMediaConfigLocalStatus}, GetNextIndexPppBridgeMediaConfigTable, PppBridgeMediaConfigLocalStatusGet, PppBridgeMediaConfigLocalStatusSet, PppBridgeMediaConfigLocalStatusTest, PppBridgeMediaConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PppBridgeMediaConfigTableINDEX, 2, 0, 0, NULL},
};
tMibData stdbcpEntry = { 15, stdbcpMibEntry };
#endif /* _STDBCPDB_H */

