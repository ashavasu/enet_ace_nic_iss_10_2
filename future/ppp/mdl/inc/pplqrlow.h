/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pplqrlow.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

/* Proto Validate Index Instance for PppExtLqrConfigTable. */
INT1
nmhValidateIndexInstancePppExtLqrConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppExtLqrConfigTable  */

INT1
nmhGetFirstIndexPppExtLqrConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppExtLqrConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppExtLqrConfigQualityRate ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtLqrConfigLQRsToExamine ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtLqrConfigMaxFailsAllowed ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtLqrConfigMaxReTrials ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppExtLqrConfigQualityRate ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtLqrConfigLQRsToExamine ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtLqrConfigMaxFailsAllowed ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtLqrConfigMaxReTrials ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppExtLqrConfigQualityRate ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtLqrConfigLQRsToExamine ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtLqrConfigMaxFailsAllowed ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtLqrConfigMaxReTrials ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for PppExtLqrTable. */
INT1
nmhValidateIndexInstancePppExtLqrTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppExtLqrTable  */

INT1
nmhGetFirstIndexPppExtLqrTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppExtLqrTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppExtLqrQualityRate ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtLqrLQRsToExamine ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtLqrMaxFailsAllowed ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtLqrMaxReTrials ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtLqrInGoodOctets ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetPppExtLqrOutLQRs ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetPppExtLqrInLQRs ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));
