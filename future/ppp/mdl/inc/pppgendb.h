/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: pppgendb.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _PPPGENDB_H
#define _PPPGENDB_H


UINT4 pppgen [] ={1,3,6,1,4,1,2076,5};
tSNMP_OID_TYPE pppgenOID = {8, pppgen};


UINT4 PppExtIPAddressPoolSelector [ ] ={1,3,6,1,4,1,2076,5,2,1};
UINT4 PppDebugLevelMask [ ] ={1,3,6,1,4,1,2076,5,2,2};


tMbDbEntry pppgenMibEntry[]= {

{{10,PppExtIPAddressPoolSelector}, NULL, PppExtIPAddressPoolSelectorGet, PppExtIPAddressPoolSelectorSet, PppExtIPAddressPoolSelectorTest, PppExtIPAddressPoolSelectorDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,PppDebugLevelMask}, NULL, PppDebugLevelMaskGet, PppDebugLevelMaskSet, PppDebugLevelMaskTest, PppDebugLevelMaskDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "256"},
};
tMibData pppgenEntry = { 2, pppgenMibEntry };
#endif /* _PPPGENDB_H */

