/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppmuxlow.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

/* Proto Validate Index Instance for PppExtMuxCpTable. */
INT1
nmhValidateIndexInstancePppExtMuxCpTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppExtMuxCpTable  */

INT1
nmhGetFirstIndexPppExtMuxCpTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppExtMuxCpTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppExtMuxCpOperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtMuxCpRemToLocDefaultPID ARG_LIST((INT4 ,INT4 *));

/* Proto Validate Index Instance for PppExtMuxCpConfigTable. */
INT1
nmhValidateIndexInstancePppExtMuxCpConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppExtMuxCpConfigTable  */

INT1
nmhGetFirstIndexPppExtMuxCpConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppExtMuxCpConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppExtMuxCpConfigAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtMuxCpConfigLocToRemDefaultPID ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtMuxCpConfigMaxSubFrameLen ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtMuxCpConfigRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppExtMuxCpConfigAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtMuxCpConfigLocToRemDefaultPID ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtMuxCpConfigMaxSubFrameLen ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtMuxCpConfigRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppExtMuxCpConfigAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtMuxCpConfigLocToRemDefaultPID ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtMuxCpConfigMaxSubFrameLen ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtMuxCpConfigRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
