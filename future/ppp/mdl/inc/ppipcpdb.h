/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: ppipcpdb.h,v 1.3 2014/09/24 13:21:55 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _PPIPCPDB_H
#define _PPIPCPDB_H

UINT1 PppExtLinkConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 PppExtIpTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 PppExtIpCompConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 PppExtIpCompStatusTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 PppExtIPAddressConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 PppExtIPAddressStatusTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 ppipcp [] ={1,3,6,1,4,1,2076,5};
tSNMP_OID_TYPE ppipcpOID = {8, ppipcp};


UINT4 PppExtLinkConfigTxACFC [ ] ={1,3,6,1,4,1,2076,5,1,1,1,1,1};
UINT4 PppExtLinkConfigRxACFC [ ] ={1,3,6,1,4,1,2076,5,1,1,1,1,2};
UINT4 PppExtLinkConfigTxPFC [ ] ={1,3,6,1,4,1,2076,5,1,1,1,1,3};
UINT4 PppExtLinkConfigRxPFC [ ] ={1,3,6,1,4,1,2076,5,1,1,1,1,4};
UINT4 PppExtLinkConfigLowerIfType [ ] ={1,3,6,1,4,1,2076,5,1,1,1,1,5};
UINT4 PppExtLinkConfigHostName [ ] ={1,3,6,1,4,1,2076,5,1,1,1,1,6};
UINT4 PppExtKeepAliveTimeOut [ ] ={1,3,6,1,4,1,2076,5,1,1,1,1,7};
UINT4 PppExtIpLocToRemoteAddress [ ] ={1,3,6,1,4,1,2076,5,1,2,1,1,1,1};
UINT4 PppExtIpRemoteToLocAddress [ ] ={1,3,6,1,4,1,2076,5,1,2,1,1,1,2};
UINT4 PppExtIpAllowVJForPeer [ ] ={1,3,6,1,4,1,2076,5,1,2,1,1,1,3};
UINT4 PppExtIpLocAddressNegFlag [ ] ={1,3,6,1,4,1,2076,5,1,2,1,1,1,4};
UINT4 PppExtIpRemoteAddressNegFlag [ ] ={1,3,6,1,4,1,2076,5,1,2,1,1,1,5};
UINT4 PppExtIpLocToRemotePrimaryDNSAddress [ ] ={1,3,6,1,4,1,2076,5,1,2,1,1,1,6};
UINT4 PppExtIpLocToRemoteSecondaryDNSAddress [ ] ={1,3,6,1,4,1,2076,5,1,2,1,1,1,7};
UINT4 PppExtIpLocToRemotePrimaryNBNSAddress [ ] ={1,3,6,1,4,1,2076,5,1,2,1,1,1,8};
UINT4 PppExtIpLocToRemoteSecondaryNBNSAddress [ ] ={1,3,6,1,4,1,2076,5,1,2,1,1,1,9};
UINT4 PppExtIpRemoteToLocPrimaryDNSAddress [ ] ={1,3,6,1,4,1,2076,5,1,2,1,1,1,10};
UINT4 PppExtIpRemoteToLocSecondaryDNSAddress [ ] ={1,3,6,1,4,1,2076,5,1,2,1,1,1,11};
UINT4 PppExtIpRemoteToLocPrimaryNBNSAddress [ ] ={1,3,6,1,4,1,2076,5,1,2,1,1,1,12};
UINT4 PppExtIpRemoteToLocSecondaryNBNSAddress [ ] ={1,3,6,1,4,1,2076,5,1,2,1,1,1,13};
UINT4 PppExtIpLocPrimaryDNSAddressNegFlag [ ] ={1,3,6,1,4,1,2076,5,1,2,1,1,1,14};
UINT4 PppExtIpLocSecondaryDNSAddressNegFlag [ ] ={1,3,6,1,4,1,2076,5,1,2,1,1,1,15};
UINT4 PppExtIpLocPrimaryNBNSAddressNegFlag [ ] ={1,3,6,1,4,1,2076,5,1,2,1,1,1,16};
UINT4 PppExtIpLocSecondaryNBNSAddressNegFlag [ ] ={1,3,6,1,4,1,2076,5,1,2,1,1,1,17};
UINT4 PppExtIpAllowIPHCForPeer [ ] ={1,3,6,1,4,1,2076,5,1,2,1,1,1,18};
UINT4 PppExtIpRowStatus [ ] ={1,3,6,1,4,1,2076,5,1,2,1,1,1,19};
UINT4 PppExtIpConfigLocalMaxSlotId [ ] ={1,3,6,1,4,1,2076,5,1,2,1,2,1,1};
UINT4 PppExtIpConfigLocalCompSlotId [ ] ={1,3,6,1,4,1,2076,5,1,2,1,2,1,2};
UINT4 PppExtIpConfigTcpSpace [ ] ={1,3,6,1,4,1,2076,5,1,2,1,2,1,3};
UINT4 PppExtIpConfigNonTcpSpace [ ] ={1,3,6,1,4,1,2076,5,1,2,1,2,1,4};
UINT4 PppExtIpConfigFMaxPeriod [ ] ={1,3,6,1,4,1,2076,5,1,2,1,2,1,5};
UINT4 PppExtIpConfigFMaxTime [ ] ={1,3,6,1,4,1,2076,5,1,2,1,2,1,6};
UINT4 PppExtIpConfigMaxHeader [ ] ={1,3,6,1,4,1,2076,5,1,2,1,2,1,7};
UINT4 PppExtIpConfigRtpCompression [ ] ={1,3,6,1,4,1,2076,5,1,2,1,2,1,8};
UINT4 PppExtIpRemoteCompSlotId [ ] ={1,3,6,1,4,1,2076,5,1,2,1,3,1,1};
UINT4 PppExtIpLocalCompSlotId [ ] ={1,3,6,1,4,1,2076,5,1,2,1,3,1,2};
UINT4 PppExtAddressPoolIndex [ ] ={1,3,6,1,4,1,2076,5,1,2,1,7,1,1};
UINT4 PppExtIPAddressPoolLowerRange [ ] ={1,3,6,1,4,1,2076,5,1,2,1,7,1,2};
UINT4 PppExtIPAddressPoolUpperRange [ ] ={1,3,6,1,4,1,2076,5,1,2,1,7,1,3};
UINT4 PppExtIPAddressPoolStatus [ ] ={1,3,6,1,4,1,2076,5,1,2,1,7,1,4};
UINT4 PppExtAddressPoolStatusIndex [ ] ={1,3,6,1,4,1,2076,5,1,2,1,8,1,1};
UINT4 PppExtNoOfFreeIPAddresses [ ] ={1,3,6,1,4,1,2076,5,1,2,1,8,1,2};
UINT4 PppExtIPAddressPoolSelector [ ] ={1,3,6,1,4,1,2076,5,2,1};
UINT4 PppDebugLevelMask [ ] ={1,3,6,1,4,1,2076,5,2,2};




tMbDbEntry ppipcpMibEntry[]= {

{{13,PppExtLinkConfigTxACFC}, GetNextIndexPppExtLinkConfigTable, PppExtLinkConfigTxACFCGet, PppExtLinkConfigTxACFCSet, PppExtLinkConfigTxACFCTest, PppExtLinkConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PppExtLinkConfigTableINDEX, 1, 0, 0, NULL},

{{13,PppExtLinkConfigRxACFC}, GetNextIndexPppExtLinkConfigTable, PppExtLinkConfigRxACFCGet, PppExtLinkConfigRxACFCSet, PppExtLinkConfigRxACFCTest, PppExtLinkConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PppExtLinkConfigTableINDEX, 1, 0, 0, NULL},

{{13,PppExtLinkConfigTxPFC}, GetNextIndexPppExtLinkConfigTable, PppExtLinkConfigTxPFCGet, PppExtLinkConfigTxPFCSet, PppExtLinkConfigTxPFCTest, PppExtLinkConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PppExtLinkConfigTableINDEX, 1, 0, 0, NULL},

{{13,PppExtLinkConfigRxPFC}, GetNextIndexPppExtLinkConfigTable, PppExtLinkConfigRxPFCGet, PppExtLinkConfigRxPFCSet, PppExtLinkConfigRxPFCTest, PppExtLinkConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PppExtLinkConfigTableINDEX, 1, 0, 0, NULL},

{{13,PppExtLinkConfigLowerIfType}, GetNextIndexPppExtLinkConfigTable, PppExtLinkConfigLowerIfTypeGet, PppExtLinkConfigLowerIfTypeSet, PppExtLinkConfigLowerIfTypeTest, PppExtLinkConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, PppExtLinkConfigTableINDEX, 1, 1, 0, NULL},

{{13,PppExtLinkConfigHostName}, GetNextIndexPppExtLinkConfigTable, PppExtLinkConfigHostNameGet, PppExtLinkConfigHostNameSet, PppExtLinkConfigHostNameTest, PppExtLinkConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, PppExtLinkConfigTableINDEX, 1, 0, 0, NULL},

{{13,PppExtKeepAliveTimeOut}, GetNextIndexPppExtLinkConfigTable, PppExtKeepAliveTimeOutGet, PppExtKeepAliveTimeOutSet, PppExtKeepAliveTimeOutTest, PppExtLinkConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, PppExtLinkConfigTableINDEX, 1, 0, 0, "10"},

{{14,PppExtIpLocToRemoteAddress}, GetNextIndexPppExtIpTable, PppExtIpLocToRemoteAddressGet, PppExtIpLocToRemoteAddressSet, PppExtIpLocToRemoteAddressTest, PppExtIpTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, PppExtIpTableINDEX, 1, 0, 0, NULL},

{{14,PppExtIpRemoteToLocAddress}, GetNextIndexPppExtIpTable, PppExtIpRemoteToLocAddressGet, PppExtIpRemoteToLocAddressSet, PppExtIpRemoteToLocAddressTest, PppExtIpTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, PppExtIpTableINDEX, 1, 0, 0, NULL},

{{14,PppExtIpAllowVJForPeer}, GetNextIndexPppExtIpTable, PppExtIpAllowVJForPeerGet, PppExtIpAllowVJForPeerSet, PppExtIpAllowVJForPeerTest, PppExtIpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PppExtIpTableINDEX, 1, 0, 0, "1"},

{{14,PppExtIpLocAddressNegFlag}, GetNextIndexPppExtIpTable, PppExtIpLocAddressNegFlagGet, PppExtIpLocAddressNegFlagSet, PppExtIpLocAddressNegFlagTest, PppExtIpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PppExtIpTableINDEX, 1, 0, 0, "2"},

{{14,PppExtIpRemoteAddressNegFlag}, GetNextIndexPppExtIpTable, PppExtIpRemoteAddressNegFlagGet, PppExtIpRemoteAddressNegFlagSet, PppExtIpRemoteAddressNegFlagTest, PppExtIpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PppExtIpTableINDEX, 1, 0, 0, "2"},

{{14,PppExtIpLocToRemotePrimaryDNSAddress}, GetNextIndexPppExtIpTable, PppExtIpLocToRemotePrimaryDNSAddressGet, PppExtIpLocToRemotePrimaryDNSAddressSet, PppExtIpLocToRemotePrimaryDNSAddressTest, PppExtIpTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, PppExtIpTableINDEX, 1, 0, 0, NULL},

{{14,PppExtIpLocToRemoteSecondaryDNSAddress}, GetNextIndexPppExtIpTable, PppExtIpLocToRemoteSecondaryDNSAddressGet, PppExtIpLocToRemoteSecondaryDNSAddressSet, PppExtIpLocToRemoteSecondaryDNSAddressTest, PppExtIpTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, PppExtIpTableINDEX, 1, 0, 0, NULL},

{{14,PppExtIpLocToRemotePrimaryNBNSAddress}, GetNextIndexPppExtIpTable, PppExtIpLocToRemotePrimaryNBNSAddressGet, PppExtIpLocToRemotePrimaryNBNSAddressSet, PppExtIpLocToRemotePrimaryNBNSAddressTest, PppExtIpTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, PppExtIpTableINDEX, 1, 0, 0, NULL},

{{14,PppExtIpLocToRemoteSecondaryNBNSAddress}, GetNextIndexPppExtIpTable, PppExtIpLocToRemoteSecondaryNBNSAddressGet, PppExtIpLocToRemoteSecondaryNBNSAddressSet, PppExtIpLocToRemoteSecondaryNBNSAddressTest, PppExtIpTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, PppExtIpTableINDEX, 1, 0, 0, NULL},

{{14,PppExtIpRemoteToLocPrimaryDNSAddress}, GetNextIndexPppExtIpTable, PppExtIpRemoteToLocPrimaryDNSAddressGet, PppExtIpRemoteToLocPrimaryDNSAddressSet, PppExtIpRemoteToLocPrimaryDNSAddressTest, PppExtIpTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, PppExtIpTableINDEX, 1, 0, 0, NULL},

{{14,PppExtIpRemoteToLocSecondaryDNSAddress}, GetNextIndexPppExtIpTable, PppExtIpRemoteToLocSecondaryDNSAddressGet, PppExtIpRemoteToLocSecondaryDNSAddressSet, PppExtIpRemoteToLocSecondaryDNSAddressTest, PppExtIpTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, PppExtIpTableINDEX, 1, 0, 0, NULL},

{{14,PppExtIpRemoteToLocPrimaryNBNSAddress}, GetNextIndexPppExtIpTable, PppExtIpRemoteToLocPrimaryNBNSAddressGet, PppExtIpRemoteToLocPrimaryNBNSAddressSet, PppExtIpRemoteToLocPrimaryNBNSAddressTest, PppExtIpTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, PppExtIpTableINDEX, 1, 0, 0, NULL},

{{14,PppExtIpRemoteToLocSecondaryNBNSAddress}, GetNextIndexPppExtIpTable, PppExtIpRemoteToLocSecondaryNBNSAddressGet, PppExtIpRemoteToLocSecondaryNBNSAddressSet, PppExtIpRemoteToLocSecondaryNBNSAddressTest, PppExtIpTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, PppExtIpTableINDEX, 1, 0, 0, NULL},

{{14,PppExtIpLocPrimaryDNSAddressNegFlag}, GetNextIndexPppExtIpTable, PppExtIpLocPrimaryDNSAddressNegFlagGet, PppExtIpLocPrimaryDNSAddressNegFlagSet, PppExtIpLocPrimaryDNSAddressNegFlagTest, PppExtIpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PppExtIpTableINDEX, 1, 0, 0, NULL},

{{14,PppExtIpLocSecondaryDNSAddressNegFlag}, GetNextIndexPppExtIpTable, PppExtIpLocSecondaryDNSAddressNegFlagGet, PppExtIpLocSecondaryDNSAddressNegFlagSet, PppExtIpLocSecondaryDNSAddressNegFlagTest, PppExtIpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PppExtIpTableINDEX, 1, 0, 0, NULL},

{{14,PppExtIpLocPrimaryNBNSAddressNegFlag}, GetNextIndexPppExtIpTable, PppExtIpLocPrimaryNBNSAddressNegFlagGet, PppExtIpLocPrimaryNBNSAddressNegFlagSet, PppExtIpLocPrimaryNBNSAddressNegFlagTest, PppExtIpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PppExtIpTableINDEX, 1, 0, 0, NULL},

{{14,PppExtIpLocSecondaryNBNSAddressNegFlag}, GetNextIndexPppExtIpTable, PppExtIpLocSecondaryNBNSAddressNegFlagGet, PppExtIpLocSecondaryNBNSAddressNegFlagSet, PppExtIpLocSecondaryNBNSAddressNegFlagTest, PppExtIpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PppExtIpTableINDEX, 1, 0, 0, NULL},

{{14,PppExtIpAllowIPHCForPeer}, GetNextIndexPppExtIpTable, PppExtIpAllowIPHCForPeerGet, PppExtIpAllowIPHCForPeerSet, PppExtIpAllowIPHCForPeerTest, PppExtIpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PppExtIpTableINDEX, 1, 0, 0, "1"},

{{14,PppExtIpRowStatus}, GetNextIndexPppExtIpTable, PppExtIpRowStatusGet, PppExtIpRowStatusSet, PppExtIpRowStatusTest, PppExtIpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PppExtIpTableINDEX, 1, 0, 1, NULL},

{{14,PppExtIpConfigLocalMaxSlotId}, GetNextIndexPppExtIpCompConfigTable, PppExtIpConfigLocalMaxSlotIdGet, PppExtIpConfigLocalMaxSlotIdSet, PppExtIpConfigLocalMaxSlotIdTest, PppExtIpCompConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, PppExtIpCompConfigTableINDEX, 1, 0, 0, NULL},

{{14,PppExtIpConfigLocalCompSlotId}, GetNextIndexPppExtIpCompConfigTable, PppExtIpConfigLocalCompSlotIdGet, PppExtIpConfigLocalCompSlotIdSet, PppExtIpConfigLocalCompSlotIdTest, PppExtIpCompConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PppExtIpCompConfigTableINDEX, 1, 0, 0, NULL},

{{14,PppExtIpConfigTcpSpace}, GetNextIndexPppExtIpCompConfigTable, PppExtIpConfigTcpSpaceGet, PppExtIpConfigTcpSpaceSet, PppExtIpConfigTcpSpaceTest, PppExtIpCompConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, PppExtIpCompConfigTableINDEX, 1, 0, 0, "15"},

{{14,PppExtIpConfigNonTcpSpace}, GetNextIndexPppExtIpCompConfigTable, PppExtIpConfigNonTcpSpaceGet, PppExtIpConfigNonTcpSpaceSet, PppExtIpConfigNonTcpSpaceTest, PppExtIpCompConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, PppExtIpCompConfigTableINDEX, 1, 0, 0, "15"},

{{14,PppExtIpConfigFMaxPeriod}, GetNextIndexPppExtIpCompConfigTable, PppExtIpConfigFMaxPeriodGet, PppExtIpConfigFMaxPeriodSet, PppExtIpConfigFMaxPeriodTest, PppExtIpCompConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, PppExtIpCompConfigTableINDEX, 1, 0, 0, "256"},

{{14,PppExtIpConfigFMaxTime}, GetNextIndexPppExtIpCompConfigTable, PppExtIpConfigFMaxTimeGet, PppExtIpConfigFMaxTimeSet, PppExtIpConfigFMaxTimeTest, PppExtIpCompConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, PppExtIpCompConfigTableINDEX, 1, 0, 0, "5"},

{{14,PppExtIpConfigMaxHeader}, GetNextIndexPppExtIpCompConfigTable, PppExtIpConfigMaxHeaderGet, PppExtIpConfigMaxHeaderSet, PppExtIpConfigMaxHeaderTest, PppExtIpCompConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, PppExtIpCompConfigTableINDEX, 1, 0, 0, "168"},

{{14,PppExtIpConfigRtpCompression}, GetNextIndexPppExtIpCompConfigTable, PppExtIpConfigRtpCompressionGet, PppExtIpConfigRtpCompressionSet, PppExtIpConfigRtpCompressionTest, PppExtIpCompConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PppExtIpCompConfigTableINDEX, 1, 0, 0, "2"},

{{14,PppExtIpRemoteCompSlotId}, GetNextIndexPppExtIpCompStatusTable, PppExtIpRemoteCompSlotIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, PppExtIpCompStatusTableINDEX, 1, 0, 0, NULL},

{{14,PppExtIpLocalCompSlotId}, GetNextIndexPppExtIpCompStatusTable, PppExtIpLocalCompSlotIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, PppExtIpCompStatusTableINDEX, 1, 0, 0, NULL},

{{14,PppExtAddressPoolIndex}, GetNextIndexPppExtIPAddressConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, PppExtIPAddressConfigTableINDEX, 1, 0, 0, NULL},

{{14,PppExtIPAddressPoolLowerRange}, GetNextIndexPppExtIPAddressConfigTable, PppExtIPAddressPoolLowerRangeGet, PppExtIPAddressPoolLowerRangeSet, PppExtIPAddressPoolLowerRangeTest, PppExtIPAddressConfigTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, PppExtIPAddressConfigTableINDEX, 1, 0, 0, NULL},

{{14,PppExtIPAddressPoolUpperRange}, GetNextIndexPppExtIPAddressConfigTable, PppExtIPAddressPoolUpperRangeGet, PppExtIPAddressPoolUpperRangeSet, PppExtIPAddressPoolUpperRangeTest, PppExtIPAddressConfigTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, PppExtIPAddressConfigTableINDEX, 1, 0, 0, NULL},

{{14,PppExtIPAddressPoolStatus}, GetNextIndexPppExtIPAddressConfigTable, PppExtIPAddressPoolStatusGet, PppExtIPAddressPoolStatusSet, PppExtIPAddressPoolStatusTest, PppExtIPAddressConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PppExtIPAddressConfigTableINDEX, 1, 0, 0, NULL},

{{14,PppExtAddressPoolStatusIndex}, GetNextIndexPppExtIPAddressStatusTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, PppExtIPAddressStatusTableINDEX, 1, 0, 0, NULL},

{{14,PppExtNoOfFreeIPAddresses}, GetNextIndexPppExtIPAddressStatusTable, PppExtNoOfFreeIPAddressesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, PppExtIPAddressStatusTableINDEX, 1, 0, 0, NULL},

{{10,PppExtIPAddressPoolSelector}, NULL, PppExtIPAddressPoolSelectorGet, PppExtIPAddressPoolSelectorSet, PppExtIPAddressPoolSelectorTest, PppExtIPAddressPoolSelectorDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,PppDebugLevelMask}, NULL, PppDebugLevelMaskGet, PppDebugLevelMaskSet, PppDebugLevelMaskTest, PppDebugLevelMaskDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "256"},
};
tMibData ppipcpEntry = { 44, ppipcpMibEntry };

#endif /* _PPIPCPDB_H */

