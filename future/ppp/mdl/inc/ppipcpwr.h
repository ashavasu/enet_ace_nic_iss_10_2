/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppipcpwr.h,v 1.2 2014/09/24 13:21:55 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

#ifndef _PPIPCPWR_H
#define _PPIPCPWR_H
INT4 GetNextIndexPppExtLinkConfigTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterPPIPCP(VOID);

VOID UnRegisterPPIPCP(VOID);
INT4 PppExtLinkConfigTxACFCGet(tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigRxACFCGet(tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigTxPFCGet(tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigRxPFCGet(tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigLowerIfTypeGet(tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigHostNameGet(tSnmpIndex *, tRetVal *);
INT4 PppExtKeepAliveTimeOutGet(tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigTxACFCSet(tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigRxACFCSet(tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigTxPFCSet(tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigRxPFCSet(tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigLowerIfTypeSet(tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigHostNameSet(tSnmpIndex *, tRetVal *);
INT4 PppExtKeepAliveTimeOutSet(tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigTxACFCTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigRxACFCTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigTxPFCTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigRxPFCTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigLowerIfTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigHostNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtKeepAliveTimeOutTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexPppExtIpTable(tSnmpIndex *, tSnmpIndex *);
INT4 PppExtIpLocToRemoteAddressGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpRemoteToLocAddressGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpAllowVJForPeerGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpLocAddressNegFlagGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpRemoteAddressNegFlagGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpLocToRemotePrimaryDNSAddressGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpLocToRemoteSecondaryDNSAddressGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpLocToRemotePrimaryNBNSAddressGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpLocToRemoteSecondaryNBNSAddressGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpRemoteToLocPrimaryDNSAddressGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpRemoteToLocSecondaryDNSAddressGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpRemoteToLocPrimaryNBNSAddressGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpRemoteToLocSecondaryNBNSAddressGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpLocPrimaryDNSAddressNegFlagGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpLocSecondaryDNSAddressNegFlagGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpLocPrimaryNBNSAddressNegFlagGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpLocSecondaryNBNSAddressNegFlagGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpAllowIPHCForPeerGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpLocToRemoteAddressSet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpRemoteToLocAddressSet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpAllowVJForPeerSet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpLocAddressNegFlagSet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpRemoteAddressNegFlagSet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpLocToRemotePrimaryDNSAddressSet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpLocToRemoteSecondaryDNSAddressSet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpLocToRemotePrimaryNBNSAddressSet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpLocToRemoteSecondaryNBNSAddressSet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpRemoteToLocPrimaryDNSAddressSet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpRemoteToLocSecondaryDNSAddressSet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpRemoteToLocPrimaryNBNSAddressSet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpRemoteToLocSecondaryNBNSAddressSet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpLocPrimaryDNSAddressNegFlagSet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpLocSecondaryDNSAddressNegFlagSet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpLocPrimaryNBNSAddressNegFlagSet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpLocSecondaryNBNSAddressNegFlagSet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpAllowIPHCForPeerSet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpLocToRemoteAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtIpRemoteToLocAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtIpAllowVJForPeerTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtIpLocAddressNegFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtIpRemoteAddressNegFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtIpLocToRemotePrimaryDNSAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtIpLocToRemoteSecondaryDNSAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtIpLocToRemotePrimaryNBNSAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtIpLocToRemoteSecondaryNBNSAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtIpRemoteToLocPrimaryDNSAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtIpRemoteToLocSecondaryDNSAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtIpRemoteToLocPrimaryNBNSAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtIpRemoteToLocSecondaryNBNSAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtIpLocPrimaryDNSAddressNegFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtIpLocSecondaryDNSAddressNegFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtIpLocPrimaryNBNSAddressNegFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtIpLocSecondaryNBNSAddressNegFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtIpAllowIPHCForPeerTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtIpRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtIpTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexPppExtIpCompConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 PppExtIpConfigLocalMaxSlotIdGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpConfigLocalCompSlotIdGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpConfigTcpSpaceGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpConfigNonTcpSpaceGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpConfigFMaxPeriodGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpConfigFMaxTimeGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpConfigMaxHeaderGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpConfigRtpCompressionGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpConfigLocalMaxSlotIdSet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpConfigLocalCompSlotIdSet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpConfigTcpSpaceSet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpConfigNonTcpSpaceSet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpConfigFMaxPeriodSet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpConfigFMaxTimeSet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpConfigMaxHeaderSet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpConfigRtpCompressionSet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpConfigLocalMaxSlotIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtIpConfigLocalCompSlotIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtIpConfigTcpSpaceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtIpConfigNonTcpSpaceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtIpConfigFMaxPeriodTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtIpConfigFMaxTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtIpConfigMaxHeaderTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtIpConfigRtpCompressionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtIpCompConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexPppExtIpCompStatusTable(tSnmpIndex *, tSnmpIndex *);
INT4 PppExtIpRemoteCompSlotIdGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIpLocalCompSlotIdGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexPppExtIPAddressConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 PppExtIPAddressPoolLowerRangeGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIPAddressPoolUpperRangeGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIPAddressPoolStatusGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIPAddressPoolLowerRangeSet(tSnmpIndex *, tRetVal *);
INT4 PppExtIPAddressPoolUpperRangeSet(tSnmpIndex *, tRetVal *);
INT4 PppExtIPAddressPoolStatusSet(tSnmpIndex *, tRetVal *);
INT4 PppExtIPAddressPoolLowerRangeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtIPAddressPoolUpperRangeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtIPAddressPoolStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtIPAddressConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexPppExtIPAddressStatusTable(tSnmpIndex *, tSnmpIndex *);
INT4 PppExtNoOfFreeIPAddressesGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIPAddressPoolSelectorGet(tSnmpIndex *, tRetVal *);
INT4 PppDebugLevelMaskGet(tSnmpIndex *, tRetVal *);
INT4 PppExtIPAddressPoolSelectorSet(tSnmpIndex *, tRetVal *);
INT4 PppDebugLevelMaskSet(tSnmpIndex *, tRetVal *);
INT4 PppExtIPAddressPoolSelectorTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppDebugLevelMaskTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtIPAddressPoolSelectorDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 PppDebugLevelMaskDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
#endif /* _PPIPCPWR_H */
