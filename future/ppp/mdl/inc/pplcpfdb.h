/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: pplcpfdb.h,v 1.2 2010/12/21 08:36:48 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _PPLCPFDB_H
#define _PPLCPFDB_H

UINT1 PppExtLinkConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};

UINT4 pplcpf [] ={1,3,6,1,4,1,2076,5};
tSNMP_OID_TYPE pplcpfOID = {8, pplcpf};


UINT4 PppExtLinkConfigTxACFC [ ] ={1,3,6,1,4,1,2076,5,1,1,1,1,1};
UINT4 PppExtLinkConfigRxACFC [ ] ={1,3,6,1,4,1,2076,5,1,1,1,1,2};
UINT4 PppExtLinkConfigTxPFC [ ] ={1,3,6,1,4,1,2076,5,1,1,1,1,3};
UINT4 PppExtLinkConfigRxPFC [ ] ={1,3,6,1,4,1,2076,5,1,1,1,1,4};
UINT4 PppExtLinkConfigLowerIfType [ ] ={1,3,6,1,4,1,2076,5,1,1,1,1,5};
UINT4 PppExtLinkConfigHostName [ ] ={1,3,6,1,4,1,2076,5,1,1,1,1,6};
UINT4 PppExtKeepAliveTimeOut [ ] ={1,3,6,1,4,1,2076,5,1,1,1,1,7};


tMbDbEntry pplcpfMibEntry[]= {

{{13,PppExtLinkConfigTxACFC}, GetNextIndexPppExtLinkConfigTable, PppExtLinkConfigTxACFCGet, PppExtLinkConfigTxACFCSet, PppExtLinkConfigTxACFCTest, PppExtLinkConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PppExtLinkConfigTableINDEX, 1, 0, 0, NULL},

{{13,PppExtLinkConfigRxACFC}, GetNextIndexPppExtLinkConfigTable, PppExtLinkConfigRxACFCGet, PppExtLinkConfigRxACFCSet, PppExtLinkConfigRxACFCTest, PppExtLinkConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PppExtLinkConfigTableINDEX, 1, 0, 0, NULL},

{{13,PppExtLinkConfigTxPFC}, GetNextIndexPppExtLinkConfigTable, PppExtLinkConfigTxPFCGet, PppExtLinkConfigTxPFCSet, PppExtLinkConfigTxPFCTest, PppExtLinkConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PppExtLinkConfigTableINDEX, 1, 0, 0, NULL},

{{13,PppExtLinkConfigRxPFC}, GetNextIndexPppExtLinkConfigTable, PppExtLinkConfigRxPFCGet, PppExtLinkConfigRxPFCSet, PppExtLinkConfigRxPFCTest, PppExtLinkConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PppExtLinkConfigTableINDEX, 1, 0, 0, NULL},

{{13,PppExtLinkConfigLowerIfType}, GetNextIndexPppExtLinkConfigTable, PppExtLinkConfigLowerIfTypeGet, PppExtLinkConfigLowerIfTypeSet, PppExtLinkConfigLowerIfTypeTest, PppExtLinkConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, PppExtLinkConfigTableINDEX, 1, 1, 0, NULL},

{{13,PppExtLinkConfigHostName}, GetNextIndexPppExtLinkConfigTable, PppExtLinkConfigHostNameGet, PppExtLinkConfigHostNameSet, PppExtLinkConfigHostNameTest, PppExtLinkConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, PppExtLinkConfigTableINDEX, 1, 0, 0, NULL},

{{13,PppExtKeepAliveTimeOut}, GetNextIndexPppExtLinkConfigTable, PppExtKeepAliveTimeOutGet, PppExtKeepAliveTimeOutSet, PppExtKeepAliveTimeOutTest, PppExtLinkConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, PppExtLinkConfigTableINDEX, 1, 0, 0, "10"},
};
tMibData pplcpfEntry = { 7, pplcpfMibEntry };
#endif /* _PPLCPFDB_H */

