/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pplcpfwr.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

#ifndef _PPLCPFWR_H
#define _PPLCPFWR_H
INT4 GetNextIndexPppExtLinkConfigTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterPPLCPF(VOID);

VOID UnRegisterPPLCPF(VOID);
INT4 PppExtLinkConfigTxACFCGet(tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigRxACFCGet(tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigTxPFCGet(tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigRxPFCGet(tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigLowerIfTypeGet(tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigHostNameGet(tSnmpIndex *, tRetVal *);
INT4 PppExtKeepAliveTimeOutGet(tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigTxACFCSet(tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigRxACFCSet(tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigTxPFCSet(tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigRxPFCSet(tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigLowerIfTypeSet(tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigHostNameSet(tSnmpIndex *, tRetVal *);
INT4 PppExtKeepAliveTimeOutSet(tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigTxACFCTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigRxACFCTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigTxPFCTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigRxPFCTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigLowerIfTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigHostNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtKeepAliveTimeOutTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppExtLinkConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _PPLCPFWR_H */
