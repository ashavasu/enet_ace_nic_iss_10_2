/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppbaplow.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

/* Proto Validate Index Instance for PppBAPConfigTable. */
INT1
nmhValidateIndexInstancePppBAPConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppBAPConfigTable  */

INT1
nmhGetFirstIndexPppBAPConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppBAPConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppBAPConfigRetransmitTimeOutValue ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppBAPConfigMaxReTransmits ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppBAPConfigRetryRequestFlag ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppBAPConfigRetryRequestTimeOutValue ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppBAPConfigRemoteAllowedRequestTypes ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppBAPConfigRetransmitTimeOutValue ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppBAPConfigMaxReTransmits ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppBAPConfigRetryRequestFlag ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppBAPConfigRetryRequestTimeOutValue ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppBAPConfigRemoteAllowedRequestTypes ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppBAPConfigRetransmitTimeOutValue ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppBAPConfigMaxReTransmits ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppBAPConfigRetryRequestFlag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppBAPConfigRetryRequestTimeOutValue ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppBAPConfigRemoteAllowedRequestTypes ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for PppBAPStatusTable. */
INT1
nmhValidateIndexInstancePppBAPStatusTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppBAPStatusTable  */

INT1
nmhGetFirstIndexPppBAPStatusTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppBAPStatusTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppBAPStatInCallReqs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppBAPStatInCallBackReqs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppBAPStatInLinkDropReqs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppBAPStatInSuccessCallReqs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppBAPStatInSuccessCallBackReqs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppBAPStatInSuccessLinkDropReqs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppBAPStatOutCallReqs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppBAPStatOutCallBackReqs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppBAPStatOutLinkDropReqs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppBAPStatOutSuccessCallReqs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppBAPStatOutSuccessCallBackReqs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppBAPStatOutSuccessLinkDropReqs ARG_LIST((INT4 ,UINT4 *));
