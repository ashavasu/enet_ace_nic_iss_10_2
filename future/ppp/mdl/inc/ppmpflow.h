/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppmpflow.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

/* Proto Validate Index Instance for MpConfigTable. */
INT1
nmhValidateIndexInstanceMpConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for MpConfigTable  */

INT1
nmhGetFirstIndexMpConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMpConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMpConfigSeqNoHeaderFormat ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetMpConfigBundleClass ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetMpConfigBundleAddr ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMpConfigHeaderFormatNegFlag ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetMpConfigHeaderFormatCode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetMpConfigHeaderFormatSuspClass ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetMpConfigNullFragTimeOutValue ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetMpConfigReAssemblyTimeOutValue ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetMpConfigBundleDirection ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetMpConfigMinThruput ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetMpConfigMaxThruput ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetMpConfigBandwidthManagementProtocol ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetMpConfigMRRU ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetMpConfigHeaderFormatCodeLocalToRemote ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetMpConfigHeaderFormatCodeRemoteToLocal ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetMpConfigHeaderFormatSuspClassLocalToRemote ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetMpConfigHeaderFormatSuspClassRemoteToLocal ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetMpConfigHeaderFormatCodeRemoteVal ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetMpConfigHeaderFormatCodeLocalVal ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetMpConfigHeaderFormatSuspClassRemoteVal ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetMpConfigHeaderFormatSuspClassLocalVal ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMpConfigSeqNoHeaderFormat ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetMpConfigBundleClass ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetMpConfigBundleAddr ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetMpConfigHeaderFormatNegFlag ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetMpConfigHeaderFormatCode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetMpConfigHeaderFormatSuspClass ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetMpConfigNullFragTimeOutValue ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetMpConfigReAssemblyTimeOutValue ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetMpConfigBundleDirection ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetMpConfigMinThruput ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetMpConfigMaxThruput ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetMpConfigBandwidthManagementProtocol ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetMpConfigMRRU ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetMpConfigHeaderFormatCodeLocalToRemote ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetMpConfigHeaderFormatCodeRemoteToLocal ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetMpConfigHeaderFormatSuspClassLocalToRemote ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetMpConfigHeaderFormatSuspClassRemoteToLocal ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MpConfigSeqNoHeaderFormat ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2MpConfigBundleClass ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2MpConfigBundleAddr ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2MpConfigHeaderFormatNegFlag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2MpConfigHeaderFormatCode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2MpConfigHeaderFormatSuspClass ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2MpConfigNullFragTimeOutValue ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2MpConfigReAssemblyTimeOutValue ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2MpConfigBundleDirection ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2MpConfigMinThruput ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2MpConfigMaxThruput ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2MpConfigBandwidthManagementProtocol ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2MpConfigMRRU ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2MpConfigHeaderFormatCodeLocalToRemote ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2MpConfigHeaderFormatCodeRemoteToLocal ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2MpConfigHeaderFormatSuspClassLocalToRemote ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2MpConfigHeaderFormatSuspClassRemoteToLocal ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for MpPrefixElisionConfigTable. */
INT1
nmhValidateIndexInstanceMpPrefixElisionConfigTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for MpPrefixElisionConfigTable  */

INT1
nmhGetFirstIndexMpPrefixElisionConfigTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMpPrefixElisionConfigTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMpPrefixElisionConfigValue ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMpPrefixElisionRemoteVal ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMpPrefixElisionConfigValue ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MpPrefixElisionConfigValue ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));
