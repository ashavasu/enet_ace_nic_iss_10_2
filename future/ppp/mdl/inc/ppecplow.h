/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppecplow.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

/* Proto Validate Index Instance for PppECPConfigTable. */
INT1
nmhValidateIndexInstancePppECPConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppECPConfigTable  */

INT1
nmhGetFirstIndexPppECPConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppECPConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppECPConfigAdminStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppECPConfigAdminStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppECPConfigAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for PppECPTxConfigTable. */
INT1
nmhValidateIndexInstancePppECPTxConfigTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppECPTxConfigTable  */

INT1
nmhGetFirstIndexPppECPTxConfigTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppECPTxConfigTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppECPTxConfigOptionNum ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppECPTxConfigOptionNum ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppECPTxConfigOptionNum ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Proto Validate Index Instance for PppECPRxConfigTable. */
INT1
nmhValidateIndexInstancePppECPRxConfigTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppECPRxConfigTable  */

INT1
nmhGetFirstIndexPppECPRxConfigTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppECPRxConfigTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppECPRxConfigAdminStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppECPRxConfigAdminStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppECPRxConfigAdminStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Proto Validate Index Instance for PppECPStatsTable. */
INT1
nmhValidateIndexInstancePppECPStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppECPStatsTable  */

INT1
nmhGetFirstIndexPppECPStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppECPStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppECPStatsOperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppECPStatsRxResetReqs ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppECPStatsTxResetReqs ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppECPStatsRxResetAcks ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppECPStatsTxResetAcks ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppECPStatsTxNegotiatedOption ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppECPStatsRxNegotiatedOption ARG_LIST((INT4 ,INT4 *));
