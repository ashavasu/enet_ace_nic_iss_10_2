/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppoelow.h,v 1.5 2014/12/16 10:49:13 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPPPoEMode ARG_LIST((INT4 *));

INT1
nmhGetPPPoEConfigMaxTotalSessions ARG_LIST((INT4 *));

INT1
nmhGetPPPoEConfigMaxSessionsPerHost ARG_LIST((INT4 *));

INT1
nmhGetPPPoEConfigPADITxInterval ARG_LIST((INT4 *));

INT1
nmhGetPPPoEConfigPADIMaxNumberOfRetries ARG_LIST((INT4 *));

INT1
nmhGetPPPoEConfigPADRMaxNumberOfRetries ARG_LIST((INT4 *));

INT1
nmhGetPPPoEConfigPADRWaitTime ARG_LIST((INT4 *));

INT1
nmhGetPPPoEHostUniqueEnabled ARG_LIST((INT4 *));

INT1
nmhGetPPPoEACName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPPPoEMode ARG_LIST((INT4 ));

INT1
nmhSetPPPoEConfigMaxTotalSessions ARG_LIST((INT4 ));

INT1
nmhSetPPPoEConfigMaxSessionsPerHost ARG_LIST((INT4 ));

INT1
nmhSetPPPoEConfigPADITxInterval ARG_LIST((INT4 ));

INT1
nmhSetPPPoEConfigPADIMaxNumberOfRetries ARG_LIST((INT4 ));

INT1
nmhSetPPPoEConfigPADRMaxNumberOfRetries ARG_LIST((INT4 ));

INT1
nmhSetPPPoEConfigPADRWaitTime ARG_LIST((INT4 ));

INT1
nmhSetPPPoEHostUniqueEnabled ARG_LIST((INT4 ));

INT1
nmhSetPPPoEACName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PPPoEMode ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2PPPoEConfigMaxTotalSessions ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2PPPoEConfigMaxSessionsPerHost ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2PPPoEConfigPADITxInterval ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2PPPoEConfigPADIMaxNumberOfRetries ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2PPPoEConfigPADRMaxNumberOfRetries ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2PPPoEConfigPADRWaitTime ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2PPPoEHostUniqueEnabled ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2PPPoEACName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2PPPoEMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2PPPoEConfigMaxSessionsPerHost ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2PPPoEConfigPADITxInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2PPPoEConfigPADRMaxNumberOfRetries ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2PPPoEConfigPADRWaitTime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2PPPoEHostUniqueEnabled ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2PPPoEACName ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2PPPoEConfigMaxTotalSessions ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for PPPoEConfigServiceNameTable. */
INT1
nmhValidateIndexInstancePPPoEConfigServiceNameTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for PPPoEConfigServiceNameTable  */

INT1
nmhGetFirstIndexPPPoEConfigServiceNameTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPPPoEConfigServiceNameTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPPPoEConfigServiceRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPPPoEConfigServiceName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPPPoEConfigServiceRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PPPoEConfigServiceName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PPPoEConfigServiceRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2PPPoEConfigServiceNameTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for PPPoEBindingsTable. */
INT1
nmhValidateIndexInstancePPPoEBindingsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PPPoEBindingsTable  */

INT1
nmhGetFirstIndexPPPoEBindingsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPPPoEBindingsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPPPoEBindingsEnabled ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPPPoEBindingsEnabled ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PPPoEBindingsEnabled ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2PPPoEBindingsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for PPPoEVlanTable. */
INT1
nmhValidateIndexInstancePPPoEVlanTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PPPoEVlanTable  */

INT1
nmhGetFirstIndexPPPoEVlanTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPPPoEVlanTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPPPoEVlanID ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPPPoECFI ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPPPoECoS ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPPPoEVlanRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPPPoEVlanID ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPPPoECFI ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPPPoECoS ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPPPoEVlanRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PPPoEVlanID ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PPPoECFI ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PPPoECoS ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PPPoEVlanRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2PPPoEVlanTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
INT1
nmhTestv2PadrMaxRetryCount ARG_LIST((UINT4 *pu4ErrorCode, INT4 i4TestValPppPadrRetryVal));
VOID
nmhSetPppPadrMaxRetryCount ARG_LIST((INT4 i4SetValPppPadrMaxRertyCount));



