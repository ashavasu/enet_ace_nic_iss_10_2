/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: stdautwr.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the common LCP files
 *
 *******************************************************************/

#ifndef _STDAUTWR_H
#define _STDAUTWR_H
INT4 GetNextIndexPppSecuritySecretsTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterSTDAUT(VOID);

VOID UnRegisterSTDAUT(VOID);
INT4 PppSecuritySecretsDirectionGet(tSnmpIndex *, tRetVal *);
INT4 PppSecuritySecretsProtocolGet(tSnmpIndex *, tRetVal *);
INT4 PppSecuritySecretsIdentityGet(tSnmpIndex *, tRetVal *);
INT4 PppSecuritySecretsSecretGet(tSnmpIndex *, tRetVal *);
INT4 PppSecuritySecretsStatusGet(tSnmpIndex *, tRetVal *);
INT4 PppSecuritySecretsDirectionSet(tSnmpIndex *, tRetVal *);
INT4 PppSecuritySecretsProtocolSet(tSnmpIndex *, tRetVal *);
INT4 PppSecuritySecretsIdentitySet(tSnmpIndex *, tRetVal *);
INT4 PppSecuritySecretsSecretSet(tSnmpIndex *, tRetVal *);
INT4 PppSecuritySecretsStatusSet(tSnmpIndex *, tRetVal *);
INT4 PppSecuritySecretsDirectionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppSecuritySecretsProtocolTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppSecuritySecretsIdentityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppSecuritySecretsSecretTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppSecuritySecretsStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppSecuritySecretsTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _STDAUTWR_H */
