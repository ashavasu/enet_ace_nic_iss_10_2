/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppdeslow.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

/* Proto Validate Index Instance for PppDesConfigTable. */
INT1
nmhValidateIndexInstancePppDesConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppDesConfigTable  */

INT1
nmhGetFirstIndexPppDesConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppDesConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppDesConfigRxIfKey ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppDesConfigTxIfKey ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppDesConfigRxIfKey ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppDesConfigTxIfKey ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppDesConfigRxIfKey ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppDesConfigTxIfKey ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Proto Validate Index Instance for PppDesStatusTable. */
INT1
nmhValidateIndexInstancePppDesStatusTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppDesStatusTable  */

INT1
nmhGetFirstIndexPppDesStatusTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppDesStatusTable ARG_LIST((INT4 , INT4 *));

/* Proto Validate Index Instance for PppDesConfigTable. */
INT1
nmhValidateIndexInstancePppDesConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppDesConfigTable  */

INT1
nmhGetFirstIndexPppDesConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppDesConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppDesConfigRxIfKey ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppDesConfigTxIfKey ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for PppDesStatusTable. */
INT1
nmhValidateIndexInstancePppDesStatusTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppDesStatusTable  */

INT1
nmhGetFirstIndexPppDesStatusTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppDesStatusTable ARG_LIST((INT4 , INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppDesConfigRxIfKey ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppDesConfigTxIfKey ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppDesConfigRxIfKey ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppDesConfigTxIfKey ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));


/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppDesStatusRxIfNonce ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppDesStatusRxIfPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppDesStatusRxIfDecryptFailedPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppDesStatusTxIfNonce ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppDesStatusTxIfPkts ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for PppTrplDesConfigTable. */
INT1
nmhValidateIndexInstancePppTrplDesConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppTrplDesConfigTable  */

INT1
nmhGetFirstIndexPppTrplDesConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppTrplDesConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppTrplDesConfigKey1 ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppTrplDesConfigKey2 ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppTrplDesConfigKey3 ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppTrplDesConfigIfIndex ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTrplDesConfigKey1 ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppTrplDesConfigKey2 ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppTrplDesConfigKey3 ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppTrplDesConfigIfIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTrplDesConfigKey1 ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppTrplDesConfigKey2 ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppTrplDesConfigKey3 ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));
