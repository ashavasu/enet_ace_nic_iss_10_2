/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: pppoedb.h,v 1.4 2014/09/24 13:21:55 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _PPPOEDB_H
#define _PPPOEDB_H

UINT1 PPPoEConfigServiceNameTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 PPPoEBindingsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 PPPoEVlanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};

UINT4 pppoe [] ={1,3,6,1,4,1,2076,37};
tSNMP_OID_TYPE pppoeOID = {8, pppoe};


UINT4 PPPoEModeOid [ ] ={1,3,6,1,4,1,2076,37,1,1,1};
UINT4 PPPoEConfigMaxTotalSessions [ ] ={1,3,6,1,4,1,2076,37,1,1,2};
UINT4 PPPoEConfigMaxSessionsPerHost [ ] ={1,3,6,1,4,1,2076,37,1,1,3};
UINT4 PPPoEConfigPADITxInterval [ ] ={1,3,6,1,4,1,2076,37,1,1,4};
UINT4 PPPoEConfigPADRMaxNumberOfRetries [ ] ={1,3,6,1,4,1,2076,37,1,1,5};
UINT4 PPPoEConfigPADRWaitTime [ ] ={1,3,6,1,4,1,2076,37,1,1,6};
UINT4 PPPoEHostUniqueEnabled [ ] ={1,3,6,1,4,1,2076,37,1,1,7};
UINT4 PPPoEACName [ ] ={1,3,6,1,4,1,2076,37,1,1,8};
UINT4 PPPoEConfigServiceName [ ] ={1,3,6,1,4,1,2076,37,1,1,9,1,1};
UINT4 PPPoEConfigServiceRowStatus [ ] ={1,3,6,1,4,1,2076,37,1,1,9,1,2};
UINT4 PPPoEBindingsIndex [ ] ={1,3,6,1,4,1,2076,37,1,2,1,1,1};
UINT4 PPPoEBindingsEnabled [ ] ={1,3,6,1,4,1,2076,37,1,2,1,1,2};
UINT4 PPPoEVlanPPPIndex [ ] ={1,3,6,1,4,1,2076,37,1,3,1,1,1};
UINT4 PPPoEVlanID [ ] ={1,3,6,1,4,1,2076,37,1,3,1,1,2};
UINT4 PPPoECFI [ ] ={1,3,6,1,4,1,2076,37,1,3,1,1,3};
UINT4 PPPoECoS [ ] ={1,3,6,1,4,1,2076,37,1,3,1,1,4};
UINT4 PPPoEVlanRowStatus [ ] ={1,3,6,1,4,1,2076,37,1,3,1,1,5};


tMbDbEntry pppoeMibEntry[]= {

{{11,PPPoEModeOid}, NULL, PPPoEModeGet, PPPoEModeSet, PPPoEModeTest, PPPoEModeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,PPPoEConfigMaxTotalSessions}, NULL, PPPoEConfigMaxTotalSessionsGet, PPPoEConfigMaxTotalSessionsSet, PPPoEConfigMaxTotalSessionsTest, PPPoEConfigMaxTotalSessionsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,PPPoEConfigMaxSessionsPerHost}, NULL, PPPoEConfigMaxSessionsPerHostGet, PPPoEConfigMaxSessionsPerHostSet, PPPoEConfigMaxSessionsPerHostTest, PPPoEConfigMaxSessionsPerHostDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,PPPoEConfigPADITxInterval}, NULL, PPPoEConfigPADITxIntervalGet, PPPoEConfigPADITxIntervalSet, PPPoEConfigPADITxIntervalTest, PPPoEConfigPADITxIntervalDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,PPPoEConfigPADRMaxNumberOfRetries}, NULL, PPPoEConfigPADRMaxNumberOfRetriesGet, PPPoEConfigPADRMaxNumberOfRetriesSet, PPPoEConfigPADRMaxNumberOfRetriesTest, PPPoEConfigPADRMaxNumberOfRetriesDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "5"},

{{11,PPPoEConfigPADRWaitTime}, NULL, PPPoEConfigPADRWaitTimeGet, PPPoEConfigPADRWaitTimeSet, PPPoEConfigPADRWaitTimeTest, PPPoEConfigPADRWaitTimeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "60"},

{{11,PPPoEHostUniqueEnabled}, NULL, PPPoEHostUniqueEnabledGet, PPPoEHostUniqueEnabledSet, PPPoEHostUniqueEnabledTest, PPPoEHostUniqueEnabledDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,PPPoEACName}, NULL, PPPoEACNameGet, PPPoEACNameSet, PPPoEACNameTest, PPPoEACNameDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{13,PPPoEConfigServiceName}, GetNextIndexPPPoEConfigServiceNameTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, PPPoEConfigServiceNameTableINDEX, 1, 0, 0, NULL},

{{13,PPPoEConfigServiceRowStatus}, GetNextIndexPPPoEConfigServiceNameTable, PPPoEConfigServiceRowStatusGet, PPPoEConfigServiceRowStatusSet, PPPoEConfigServiceRowStatusTest, PPPoEConfigServiceNameTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PPPoEConfigServiceNameTableINDEX, 1, 0, 1, NULL},

{{13,PPPoEBindingsIndex}, GetNextIndexPPPoEBindingsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, PPPoEBindingsTableINDEX, 1, 0, 0, NULL},

{{13,PPPoEBindingsEnabled}, GetNextIndexPPPoEBindingsTable, PPPoEBindingsEnabledGet, PPPoEBindingsEnabledSet, PPPoEBindingsEnabledTest, PPPoEBindingsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PPPoEBindingsTableINDEX, 1, 0, 0, NULL},

{{13,PPPoEVlanPPPIndex}, GetNextIndexPPPoEVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, PPPoEVlanTableINDEX, 1, 0, 0, NULL},

{{13,PPPoEVlanID}, GetNextIndexPPPoEVlanTable, PPPoEVlanIDGet, PPPoEVlanIDSet, PPPoEVlanIDTest, PPPoEVlanTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, PPPoEVlanTableINDEX, 1, 0, 0, NULL},

{{13,PPPoECFI}, GetNextIndexPPPoEVlanTable, PPPoECFIGet, PPPoECFISet, PPPoECFITest, PPPoEVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PPPoEVlanTableINDEX, 1, 0, 0, "0"},

{{13,PPPoECoS}, GetNextIndexPPPoEVlanTable, PPPoECoSGet, PPPoECoSSet, PPPoECoSTest, PPPoEVlanTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, PPPoEVlanTableINDEX, 1, 0, 0, NULL},

{{13,PPPoEVlanRowStatus}, GetNextIndexPPPoEVlanTable, PPPoEVlanRowStatusGet, PPPoEVlanRowStatusSet, PPPoEVlanRowStatusTest, PPPoEVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, PPPoEVlanTableINDEX, 1, 0, 1, NULL},
};
tMibData pppoeEntry = { 17, pppoeMibEntry };

#endif /* _PPPOEDB_H */

