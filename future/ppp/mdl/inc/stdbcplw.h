/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdbcplw.h,v 1.2 2010/12/21 08:36:48 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for PppBridgeTable. */
INT1
nmhValidateIndexInstancePppBridgeTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppBridgeTable  */

INT1
nmhGetFirstIndexPppBridgeTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppBridgeTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppBridgeOperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppBridgeLocalToRemoteTinygramCompression ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppBridgeRemoteToLocalTinygramCompression ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppBridgeLocalToRemoteLanId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppBridgeRemoteToLocalLanId ARG_LIST((INT4 ,INT4 *));

/* Proto Validate Index Instance for PppBridgeConfigTable. */
INT1
nmhValidateIndexInstancePppBridgeConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppBridgeConfigTable  */

INT1
nmhGetFirstIndexPppBridgeConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppBridgeConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppBridgeConfigAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppBridgeConfigTinygram ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppBridgeConfigRingId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppBridgeConfigLineId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppBridgeConfigLanId ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppBridgeConfigAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppBridgeConfigTinygram ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppBridgeConfigRingId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppBridgeConfigLineId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppBridgeConfigLanId ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppBridgeConfigAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppBridgeConfigTinygram ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppBridgeConfigRingId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppBridgeConfigLineId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppBridgeConfigLanId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2PppBridgeConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for PppBridgeMediaTable. */
INT1
nmhValidateIndexInstancePppBridgeMediaTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppBridgeMediaTable  */

INT1
nmhGetFirstIndexPppBridgeMediaTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppBridgeMediaTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppBridgeMediaLocalStatus ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetPppBridgeMediaRemoteStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for PppBridgeMediaConfigTable. */
INT1
nmhValidateIndexInstancePppBridgeMediaConfigTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppBridgeMediaConfigTable  */

INT1
nmhGetFirstIndexPppBridgeMediaConfigTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppBridgeMediaConfigTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppBridgeMediaConfigLocalStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppBridgeMediaConfigLocalStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppBridgeMediaConfigLocalStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2PppBridgeMediaConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
