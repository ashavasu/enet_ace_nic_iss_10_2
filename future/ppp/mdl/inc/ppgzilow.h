/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppgzilow.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppGzipLongestMatch ARG_LIST((INT4 *));

INT1
nmhGetPppGzipMinMatch ARG_LIST((INT4 *));

INT1
nmhGetPppGzipHashBits ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppGzipLongestMatch ARG_LIST((INT4 ));

INT1
nmhSetPppGzipMinMatch ARG_LIST((INT4 ));

INT1
nmhSetPppGzipHashBits ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppGzipLongestMatch ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2PppGzipMinMatch ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2PppGzipHashBits ARG_LIST((UINT4 *  ,INT4 ));

/* Proto Validate Index Instance for PppGzipConfigTable. */
INT1
nmhValidateIndexInstancePppGzipConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppGzipConfigTable  */

INT1
nmhGetFirstIndexPppGzipConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppGzipConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppGzipConfigIfWindowSize ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppGzipConfigIfWindowSize ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppGzipConfigIfWindowSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for PppGzipStatusTable. */
INT1
nmhValidateIndexInstancePppGzipStatusTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppGzipStatusTable  */

INT1
nmhGetFirstIndexPppGzipStatusTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppGzipStatusTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppGzipStatusRxIfWindowSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppGzipStatusRxIfPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppGzipStatusRxIfDiscardPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppGzipStatusRxIfDecompressionFailedPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppGzipStatusRxIfDecompressedDataSize ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppGzipStatusRxIfCompressedDataSize ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppGzipStatusTxIfWindowSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppGzipStatusTxIfPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppGzipStatusTxIfCompressionFailedPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppGzipStatusTxIfCompressedDataSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppGzipStatusTxIfDecompressedDataSize ARG_LIST((INT4 ,UINT4 *));
