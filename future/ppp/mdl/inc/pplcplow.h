/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pplcplow.h,v 1.2 2011/10/13 10:31:35 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

#ifndef __PPLCP_LOW_H__
#define __PPLCP_LOW_H__
/* Proto Validate Index Instance for PppExtLinkConfigTable. */
INT1
nmhValidateIndexInstancePppExtLinkConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppExtLinkConfigTable  */

INT1
nmhGetFirstIndexPppExtLinkConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppExtLinkConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppExtLinkConfigRestartTimeOutValue ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtLinkConfigRetransmitTimeOutValue ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtLinkConfigMaxReTransmits ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtLinkConfigMaxConfTransmits ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtLinkConfigMaxTermTransmits ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtLinkConfigMaxNakAllowed ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtLinkConfigMaxMagicLoop ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtLinkConfigEchoCharacter ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtLinkConfigPassiveOption ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtLinkConfigTxACCM ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppExtLinkConfigTxACFC ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtLinkConfigRxACFC ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtLinkConfigTxPFC ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtLinkConfigRxPFC ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtLinkConfigLowerIfType ARG_LIST((INT4 ,INT4 *));



INT1
nmhGetPppExtLinkConfigHostName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppExtLinkConfigIdleTimeOut ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppKeepAliveTimeOutVal ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtKeepAliveTimeOut ARG_LIST((INT4 ,INT4 *));
INT1
nmhGetPppExtLinkConfigLLFraming ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtLinkConfigScrambleEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtLinkConfigTROption ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtLinkConfigTRVal ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtLinkConfigIdentification ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppExtLinkConfigTxACFC ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtLinkConfigRxACFC ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtLinkConfigTxPFC ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtLinkConfigRxPFC ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtLinkConfigLowerIfType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtLinkConfigHostName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppExtLinkConfigIdleTimeOut ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtKeepAliveTimeOut ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppKeepAliveTimeOutVal ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtLinkConfigLLFraming ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtLinkConfigScrambleEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtLinkConfigTROption ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtLinkConfigTRVal ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtLinkConfigIdentification ARG_LIST((INT4  ,INT4 ));



/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppExtLinkConfigTxACFC ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtLinkConfigRxACFC ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtLinkConfigTxPFC ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtLinkConfigRxPFC ARG_LIST((UINT4 *  ,INT4  ,INT4 ));




INT1
nmhTestv2PppExtLinkConfigLowerIfType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtLinkConfigHostName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppExtLinkConfigIdleTimeOut ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppKeepAliveTimeOutVal ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtKeepAliveTimeOut ARG_LIST((UINT4 *  ,INT4  ,INT4 ));


INT1
nmhTestv2PppExtLinkConfigLLFraming ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtLinkConfigScrambleEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtLinkConfigTROption ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtLinkConfigTRVal ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtLinkConfigIdentification ARG_LIST((UINT4 *  ,INT4  ,INT4 ));


/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppExtLinkStatusLocalLinkDiscrValue ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtLinkStatusRemoteLinkDiscrValue ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtLinkStatusBadAddresses ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetPppExtLinkStatusBadControls ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetPppExtLinkStatusPacketTooLongs ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetPppExtLinkStatusBadFCSs ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2PppExtLinkConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
#endif
