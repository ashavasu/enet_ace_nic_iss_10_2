/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppoewr.h,v 1.3 2014/03/11 14:02:52 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

#ifndef _PPPOEWR_H
#define _PPPOEWR_H

VOID RegisterPPPOE(VOID);

VOID UnRegisterPPPOE(VOID);
INT4 PPPoEModeGet(tSnmpIndex *, tRetVal *);
INT4 PPPoEConfigMaxTotalSessionsGet(tSnmpIndex *, tRetVal *);
INT4 PPPoEConfigMaxSessionsPerHostGet(tSnmpIndex *, tRetVal *);
INT4 PPPoEConfigPADITxIntervalGet(tSnmpIndex *, tRetVal *);
INT4 PPPoEConfigPADRMaxNumberOfRetriesGet(tSnmpIndex *, tRetVal *);
INT4 PPPoEConfigPADRWaitTimeGet(tSnmpIndex *, tRetVal *);
INT4 PPPoEHostUniqueEnabledGet(tSnmpIndex *, tRetVal *);
INT4 PPPoEACNameGet(tSnmpIndex *, tRetVal *);
INT4 PPPoEModeSet(tSnmpIndex *, tRetVal *);
INT4 PPPoEConfigMaxTotalSessionsSet(tSnmpIndex *, tRetVal *);
INT4 PPPoEConfigMaxSessionsPerHostSet(tSnmpIndex *, tRetVal *);
INT4 PPPoEConfigPADITxIntervalSet(tSnmpIndex *, tRetVal *);
INT4 PPPoEConfigPADRMaxNumberOfRetriesSet(tSnmpIndex *, tRetVal *);
INT4 PPPoEConfigPADRWaitTimeSet(tSnmpIndex *, tRetVal *);
INT4 PPPoEHostUniqueEnabledSet(tSnmpIndex *, tRetVal *);
INT4 PPPoEACNameSet(tSnmpIndex *, tRetVal *);
INT4 PPPoEModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PPPoEConfigMaxTotalSessionsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PPPoEConfigMaxSessionsPerHostTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PPPoEConfigPADITxIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PPPoEConfigPADRMaxNumberOfRetriesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PPPoEConfigPADRWaitTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PPPoEHostUniqueEnabledTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PPPoEACNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PPPoEModeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 PPPoEConfigMaxTotalSessionsDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 PPPoEConfigMaxSessionsPerHostDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 PPPoEConfigPADITxIntervalDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 PPPoEConfigPADRMaxNumberOfRetriesDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 PPPoEConfigPADRWaitTimeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 PPPoEHostUniqueEnabledDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 PPPoEACNameDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexPPPoEConfigServiceNameTable(tSnmpIndex *, tSnmpIndex *);
INT4 PPPoEConfigServiceRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 PPPoEConfigServiceRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 PPPoEConfigServiceRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PPPoEConfigServiceNameTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexPPPoEBindingsTable(tSnmpIndex *, tSnmpIndex *);
INT4 PPPoEBindingsEnabledGet(tSnmpIndex *, tRetVal *);
INT4 PPPoEBindingsEnabledSet(tSnmpIndex *, tRetVal *);
INT4 PPPoEBindingsEnabledTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PPPoEBindingsTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexPPPoEVlanTable(tSnmpIndex *, tSnmpIndex *);
INT4 PPPoEVlanIDGet(tSnmpIndex *, tRetVal *);
INT4 PPPoECFIGet(tSnmpIndex *, tRetVal *);
INT4 PPPoECoSGet(tSnmpIndex *, tRetVal *);
INT4 PPPoEVlanRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 PPPoEVlanIDSet(tSnmpIndex *, tRetVal *);
INT4 PPPoECFISet(tSnmpIndex *, tRetVal *);
INT4 PPPoECoSSet(tSnmpIndex *, tRetVal *);
INT4 PPPoEVlanRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 PPPoEVlanIDTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PPPoECFITest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PPPoECoSTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PPPoEVlanRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PPPoEVlanTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _PPPOEWR_H */
