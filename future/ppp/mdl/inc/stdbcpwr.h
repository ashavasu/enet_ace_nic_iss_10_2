/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: stdbcpwr.h,v 1.2 2010/12/21 08:36:48 siva Exp $
 *
 * Description:
 *
 ********************************************************************/

#ifndef _STDBCPWR_H
#define _STDBCPWR_H
INT4 GetNextIndexPppBridgeTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterSTDBCP(VOID);

VOID UnRegisterSTDBCP(VOID);
INT4 PppBridgeOperStatusGet(tSnmpIndex *, tRetVal *);
INT4 PppBridgeLocalToRemoteTinygramCompressionGet(tSnmpIndex *, tRetVal *);
INT4 PppBridgeRemoteToLocalTinygramCompressionGet(tSnmpIndex *, tRetVal *);
INT4 PppBridgeLocalToRemoteLanIdGet(tSnmpIndex *, tRetVal *);
INT4 PppBridgeRemoteToLocalLanIdGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexPppBridgeConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 PppBridgeConfigAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 PppBridgeConfigTinygramGet(tSnmpIndex *, tRetVal *);
INT4 PppBridgeConfigRingIdGet(tSnmpIndex *, tRetVal *);
INT4 PppBridgeConfigLineIdGet(tSnmpIndex *, tRetVal *);
INT4 PppBridgeConfigLanIdGet(tSnmpIndex *, tRetVal *);
INT4 PppBridgeConfigAdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 PppBridgeConfigTinygramSet(tSnmpIndex *, tRetVal *);
INT4 PppBridgeConfigRingIdSet(tSnmpIndex *, tRetVal *);
INT4 PppBridgeConfigLineIdSet(tSnmpIndex *, tRetVal *);
INT4 PppBridgeConfigLanIdSet(tSnmpIndex *, tRetVal *);
INT4 PppBridgeConfigAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppBridgeConfigTinygramTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppBridgeConfigRingIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppBridgeConfigLineIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppBridgeConfigLanIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppBridgeConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexPppBridgeMediaTable(tSnmpIndex *, tSnmpIndex *);
INT4 PppBridgeMediaLocalStatusGet(tSnmpIndex *, tRetVal *);
INT4 PppBridgeMediaRemoteStatusGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexPppBridgeMediaConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 PppBridgeMediaConfigLocalStatusGet(tSnmpIndex *, tRetVal *);
INT4 PppBridgeMediaConfigLocalStatusSet(tSnmpIndex *, tRetVal *);
INT4 PppBridgeMediaConfigLocalStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 PppBridgeMediaConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _STDBCPWR_H */
