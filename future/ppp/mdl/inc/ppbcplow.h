/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppbcplow.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

/* Proto Validate Index Instance for PppExtBcpTable. */
INT1
nmhValidateIndexInstancePppExtBcpTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppExtBcpTable  */

INT1
nmhGetFirstIndexPppExtBcpTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppExtBcpTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppExtBcpSpanningTreeProtocol ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtBcpRemtoLocLanSegmentNumber ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtBcpRemtoLocBridgeNumber ARG_LIST((INT4 ,INT4 *));

/* Proto Validate Index Instance for PppExtBcpConfigTable. */
INT1
nmhValidateIndexInstancePppExtBcpConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppExtBcpConfigTable  */

INT1
nmhGetFirstIndexPppExtBcpConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppExtBcpConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppExtBcpConfigSpanningTreeProtocol ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtBcpConfigLoctoRemLanSegmentNumber ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtBcpConfigLoctoRemBridgeNumber ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtBcpConfigBridgeId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtBcpConfigLoctoRemMACAddress ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppExtBcpConfigRemtoLocMACAddress ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppExtBcpConfigIEEE802TagFrameSupport ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtBcpConfigRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppExtBcpConfigSpanningTreeProtocol ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtBcpConfigLoctoRemLanSegmentNumber ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtBcpConfigLoctoRemBridgeNumber ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtBcpConfigBridgeId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtBcpConfigLoctoRemMACAddress ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppExtBcpConfigRemtoLocMACAddress ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppExtBcpConfigIEEE802TagFrameSupport ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtBcpConfigRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppExtBcpConfigSpanningTreeProtocol ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtBcpConfigLoctoRemLanSegmentNumber ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtBcpConfigLoctoRemBridgeNumber ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtBcpConfigBridgeId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtBcpConfigLoctoRemMACAddress ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppExtBcpConfigRemtoLocMACAddress ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppExtBcpConfigIEEE802TagFrameSupport ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtBcpConfigRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
