/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppbaclow.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

/* Proto Validate Index Instance for PppBACPTable. */
INT1
nmhValidateIndexInstancePppBACPTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppBACPTable  */

INT1
nmhGetFirstIndexPppBACPTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppBACPTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppBACPOperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppBACPFavoredEnd ARG_LIST((INT4 ,INT4 *));

/* Proto Validate Index Instance for PppBACPConfigTable. */
INT1
nmhValidateIndexInstancePppBACPConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppBACPConfigTable  */

INT1
nmhGetFirstIndexPppBACPConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppBACPConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppBACPConfigAdminStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppBACPConfigAdminStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppBACPConfigAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
