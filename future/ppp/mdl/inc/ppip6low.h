/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppip6low.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

/* Proto Validate Index Instance for PppIp6Table. */
INT1
nmhValidateIndexInstancePppIp6Table ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppIp6Table  */

INT1
nmhGetFirstIndexPppIp6Table ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppIp6Table ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppIp6OperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppIp6LocToRemIfId ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppIp6RemToLocIfId ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppIp6LocToRemCompProt ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppIp6RemToLocCompProt ARG_LIST((INT4 ,INT4 *));

/* Proto Validate Index Instance for PppIp6ConfigTable. */
INT1
nmhValidateIndexInstancePppIp6ConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppIp6ConfigTable  */

INT1
nmhGetFirstIndexPppIp6ConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppIp6ConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppIp6ConfigAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppIp6ConfigLocToRemIfId ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppIp6ConfigRemToLocIfId ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppIp6ConfigMethodToGenIfId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppIp6ConfigCompression ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppIp6ConfigRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppIp6ConfigAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppIp6ConfigLocToRemIfId ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppIp6ConfigRemToLocIfId ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppIp6ConfigMethodToGenIfId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppIp6ConfigCompression ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppIp6ConfigRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppIp6ConfigAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppIp6ConfigLocToRemIfId ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppIp6ConfigRemToLocIfId ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppIp6ConfigMethodToGenIfId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppIp6ConfigCompression ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppIp6ConfigRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for PppExtIp6CompConfigTable. */
INT1
nmhValidateIndexInstancePppExtIp6CompConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppExtIp6CompConfigTable  */

INT1
nmhGetFirstIndexPppExtIp6CompConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppExtIp6CompConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppExtIp6ConfigTcpSpace ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtIp6ConfigNonTcpSpace ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtIp6ConfigFMaxPeriod ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtIp6ConfigFMaxTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtIp6ConfigMaxHeader ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtIp6ConfigRtpCompression ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppExtIp6ConfigTcpSpace ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtIp6ConfigNonTcpSpace ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtIp6ConfigFMaxPeriod ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtIp6ConfigFMaxTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtIp6ConfigMaxHeader ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtIp6ConfigRtpCompression ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppExtIp6ConfigTcpSpace ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtIp6ConfigNonTcpSpace ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtIp6ConfigFMaxPeriod ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtIp6ConfigFMaxTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtIp6ConfigMaxHeader ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtIp6ConfigRtpCompression ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
