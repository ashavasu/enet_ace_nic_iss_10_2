/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppstlow.h,v 1.3 2011/10/13 10:31:34 siva Exp $
 *
 * Description:
 *
 ********************************************************************/

/* Proto Validate Index Instance for PppLinkStatusTable. */
INT1
nmhValidateIndexInstancePppLinkStatusTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppLinkStatusTable  */

INT1
nmhGetFirstIndexPppLinkStatusTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppLinkStatusTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppLinkStatusPhysicalIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppLinkStatusBadAddresses ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppLinkStatusBadControls ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppLinkStatusPacketTooLongs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppLinkStatusBadFCSs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppLinkStatusLocalMRU ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppLinkStatusRemoteMRU ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppLinkStatusLocalToPeerACCMap ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppLinkStatusPeerToLocalACCMap ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppLinkStatusLocalToRemoteProtocolCompression ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppLinkStatusRemoteToLocalProtocolCompression ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppLinkStatusLocalToRemoteACCompression ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppLinkStatusRemoteToLocalACCompression ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppLinkStatusTransmitFcsSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppLinkStatusReceiveFcsSize ARG_LIST((INT4 ,INT4 *));


/* Proto type for Low Level GET Routine All Objects.  */


INT1
nmhGetPppLinkConfigReceiveACCMap ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppLinkConfigTransmitACCMap ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppLinkConfigReceiveACCMap ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppLinkConfigTransmitACCMap ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));


/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppLinkConfigReceiveACCMap ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppLinkConfigTransmitACCMap ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Proto Validate Index Instance for PppLqrTable. */
INT1
nmhValidateIndexInstancePppLqrTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppLqrTable  */

INT1
nmhGetFirstIndexPppLqrTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppLqrTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppLqrQuality ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppLqrInGoodOctets ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppLqrLocalPeriod ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppLqrRemotePeriod ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppLqrOutLQRs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppLqrInLQRs ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for PppLqrConfigTable. */
INT1
nmhValidateIndexInstancePppLqrConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppLqrConfigTable  */

INT1
nmhGetFirstIndexPppLqrConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppLqrConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppLqrConfigPeriod ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppLqrConfigStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppLqrConfigPeriod ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppLqrConfigStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppLqrConfigPeriod ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppLqrConfigStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for PppLqrExtnsTable. */
INT1
nmhValidateIndexInstancePppLqrExtnsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppLqrExtnsTable  */

INT1
nmhGetFirstIndexPppLqrExtnsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppLqrExtnsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppLqrExtnsLastReceivedLqrPacket ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for PppSecurityConfigTable. */
INT1
nmhValidateIndexInstancePppSecurityConfigTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppSecurityConfigTable  */

INT1
nmhGetFirstIndexPppSecurityConfigTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppSecurityConfigTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppSecurityConfigProtocol ARG_LIST((INT4  , INT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetPppSecurityConfigStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppSecurityConfigLink ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetPppSecurityConfigPreference ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetPppSecurityConfigProtocol ARG_LIST((INT4  , INT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetPppSecurityConfigStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppSecurityConfigLink ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2PppSecurityConfigPreference ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2PppSecurityConfigProtocol ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2PppSecurityConfigStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));


