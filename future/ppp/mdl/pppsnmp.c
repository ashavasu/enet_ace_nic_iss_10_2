/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppsnmp.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains registering functions with
 *             FutureSNMP
 *******************************************************************/

#ifdef FUTURE_SNMP_WANTED

#include "include.h"
#include "pppstmbdb.h"
#include "pppexmbdb.h"
#include "pppoembdb.h"

INT4 RegisterStdPPPwithFutureSNMP PROTO ((void));
INT4 RegisterExtPPPwithFutureSNMP PROTO ((void));
INT4 RegisterPPPOEwithFutureSNMP PROTO ((void));
extern INT4 SNMP_AGT_RegisterMib PROTO ((tSNMP_GroupOIDType *,
                                         tSNMP_BaseOIDType *,
                                         tSNMP_MIBObjectDescrType *,
                                         tSNMP_GLOBAL_STRUCT, INT4));

INT4
RegisterStdPPPwithFutureSNMP ()
{
    if (SNMP_AGT_RegisterMib
        ((tSNMP_GroupOIDType *) & pppstd_FMAS_GroupOIDTable,
         (tSNMP_BaseOIDType *) & pppstd_FMAS_BaseOIDTable,
         (tSNMP_MIBObjectDescrType *) & pppstd_FMAS_MIBObjectTable,
         (tSNMP_GLOBAL_STRUCT) pppstd_FMAS_Global_data,
         (INT4) pppstd_MAX_OBJECTS) != SNMP_SUCCESS)
        return FAILURE;
    return SUCCESS;
}

INT4
RegisterExtPPPwithFutureSNMP ()
{
    if (SNMP_AGT_RegisterMib
        ((tSNMP_GroupOIDType *) & pppext_FMAS_GroupOIDTable,
         (tSNMP_BaseOIDType *) & pppext_FMAS_BaseOIDTable,
         (tSNMP_MIBObjectDescrType *) & pppext_FMAS_MIBObjectTable,
         (tSNMP_GLOBAL_STRUCT) pppext_FMAS_Global_data,
         (INT4) pppext_MAX_OBJECTS) != SNMP_SUCCESS)
        return FAILURE;
    return SUCCESS;
}

INT4
RegisterPPPOEwithFutureSNMP ()
{
    if (SNMP_AGT_RegisterMib ((tSNMP_GroupOIDType *) & pppoe_FMAS_GroupOIDTable,
                              (tSNMP_BaseOIDType *) & pppoe_FMAS_BaseOIDTable,
                              (tSNMP_MIBObjectDescrType *) &
                              pppoe_FMAS_MIBObjectTable,
                              (tSNMP_GLOBAL_STRUCT) pppoe_FMAS_Global_data,
                              (INT4) pppoe_MAX_OBJECTS) != SNMP_SUCCESS)
        return FAILURE;
    return SUCCESS;
}
#endif /* FUTURE_SNMP_WANTED */
