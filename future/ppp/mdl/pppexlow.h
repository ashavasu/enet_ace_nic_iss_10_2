/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppexlow.h,v 1.3 2014/09/29 12:36:17 siva Exp $
 *
 * Description:
 *
 ********************************************************************/

/* Proto Validate Index Instance for PppIpxTable. */
INT1
nmhValidateIndexInstancePppIpxTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppIpxTable  */

INT1
nmhGetFirstIndexPppIpxTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppIpxTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppIpxOperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppIpxLoctoRemCompProtocol ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppIpxRemtoLocCompProtocol ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppIpxLocalMaxSlotId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppIpxRemoteMaxSlotId ARG_LIST((INT4 ,INT4 *));

/* Proto Validate Index Instance for PppIpxConfigTable. */
INT1
nmhValidateIndexInstancePppIpxConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppIpxConfigTable  */

INT1
nmhGetFirstIndexPppIpxConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppIpxConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppIpxConfigAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppIpxConfigCompression ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppIpxConfigConfComplete ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppIpxConfigAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppIpxConfigCompression ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppIpxConfigConfComplete ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppIpxConfigAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppIpxConfigCompression ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppIpxConfigConfComplete ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppIpxLocalEntityStatus ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppIpxLocalEntityStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppIpxLocalEntityStatus ARG_LIST((UINT4 *  ,INT4 ));

/* Proto Validate Index Instance for PppECPConfigTable. */
INT1
nmhValidateIndexInstancePppECPConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppECPConfigTable  */

INT1
nmhGetFirstIndexPppECPConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppECPConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppECPConfigAdminStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppECPConfigAdminStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppECPConfigAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for PppECPTxConfigTable. */
INT1
nmhValidateIndexInstancePppECPTxConfigTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppECPTxConfigTable  */

INT1
nmhGetFirstIndexPppECPTxConfigTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppECPTxConfigTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppECPTxConfigOptionNum ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppECPTxConfigOptionNum ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppECPTxConfigOptionNum ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Proto Validate Index Instance for PppECPRxConfigTable. */
INT1
nmhValidateIndexInstancePppECPRxConfigTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppECPRxConfigTable  */

INT1
nmhGetFirstIndexPppECPRxConfigTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppECPRxConfigTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppECPRxConfigAdminStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppECPRxConfigAdminStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppECPRxConfigAdminStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Proto Validate Index Instance for PppECPStatsTable. */
INT1
nmhValidateIndexInstancePppECPStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppECPStatsTable  */

INT1
nmhGetFirstIndexPppECPStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppECPStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppECPStatsOperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppECPStatsRxResetReqs ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppECPStatsTxResetReqs ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppECPStatsRxResetAcks ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppECPStatsTxResetAcks ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppECPStatsTxNegotiatedOption ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppECPStatsRxNegotiatedOption ARG_LIST((INT4 ,INT4 *));

/* Proto Validate Index Instance for PppDesConfigTable. */
INT1
nmhValidateIndexInstancePppDesConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppDesConfigTable  */

INT1
nmhGetFirstIndexPppDesConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppDesConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppDesConfigRxIfKey ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppDesConfigTxIfKey ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppDesConfigRxIfKey ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppDesConfigTxIfKey ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppDesConfigRxIfKey ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppDesConfigTxIfKey ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Proto Validate Index Instance for PppDesStatusTable. */
INT1
nmhValidateIndexInstancePppDesStatusTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppDesStatusTable  */

INT1
nmhGetFirstIndexPppDesStatusTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppDesStatusTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppDesStatusRxIfNonce ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppDesStatusRxIfPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppDesStatusRxIfDecryptFailedPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppDesStatusTxIfNonce ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppDesStatusTxIfPkts ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for PppTrplDesConfigTable. */
INT1
nmhValidateIndexInstancePppTrplDesConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppTrplDesConfigTable  */

INT1
nmhGetFirstIndexPppTrplDesConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppTrplDesConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppTrplDesConfigKey1 ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppTrplDesConfigKey2 ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppTrplDesConfigKey3 ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppTrplDesConfigIfIndex ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTrplDesConfigKey1 ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppTrplDesConfigKey2 ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppTrplDesConfigKey3 ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppTrplDesConfigIfIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTrplDesConfigKey1 ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppTrplDesConfigKey2 ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppTrplDesConfigKey3 ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Proto Validate Index Instance for PppCCPConfigTable. */
INT1
nmhValidateIndexInstancePppCCPConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppCCPConfigTable  */

INT1
nmhGetFirstIndexPppCCPConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppCCPConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppCCPConfigAdminStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppCCPConfigAdminStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppCCPConfigAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for PppCCPTxConfigTable. */
INT1
nmhValidateIndexInstancePppCCPTxConfigTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppCCPTxConfigTable  */

INT1
nmhGetFirstIndexPppCCPTxConfigTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppCCPTxConfigTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppCCPTxConfigOptionNum ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppCCPTxConfigOptionNum ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppCCPTxConfigOptionNum ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Proto Validate Index Instance for PppCCPRxConfigTable. */
INT1
nmhValidateIndexInstancePppCCPRxConfigTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppCCPRxConfigTable  */

INT1
nmhGetFirstIndexPppCCPRxConfigTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppCCPRxConfigTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppCCPRxConfigAdminStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppCCPRxConfigAdminStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppCCPRxConfigAdminStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Proto Validate Index Instance for PppCCPStatsTable. */
INT1
nmhValidateIndexInstancePppCCPStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppCCPStatsTable  */

INT1
nmhGetFirstIndexPppCCPStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppCCPStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppCCPStatsOperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppCCPStatsRxResetReqs ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppCCPStatsTxResetReqs ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppCCPStatsRxResetAcks ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppCCPStatsTxResetAcks ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppCCPStatsTxNegotiatedOption ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppCCPStatsRxNegotiatedOption ARG_LIST((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppGzipLongestMatch ARG_LIST((INT4 *));

INT1
nmhGetPppGzipMinMatch ARG_LIST((INT4 *));

INT1
nmhGetPppGzipHashBits ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppGzipLongestMatch ARG_LIST((INT4 ));

INT1
nmhSetPppGzipMinMatch ARG_LIST((INT4 ));

INT1
nmhSetPppGzipHashBits ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppGzipLongestMatch ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2PppGzipMinMatch ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2PppGzipHashBits ARG_LIST((UINT4 *  ,INT4 ));

/* Proto Validate Index Instance for PppGzipConfigTable. */
INT1
nmhValidateIndexInstancePppGzipConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppGzipConfigTable  */

INT1
nmhGetFirstIndexPppGzipConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppGzipConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppGzipConfigIfWindowSize ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppGzipConfigIfWindowSize ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppGzipConfigIfWindowSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for PppGzipStatusTable. */
INT1
nmhValidateIndexInstancePppGzipStatusTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppGzipStatusTable  */

INT1
nmhGetFirstIndexPppGzipStatusTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppGzipStatusTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppGzipStatusRxIfWindowSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppGzipStatusRxIfPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppGzipStatusRxIfDiscardPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppGzipStatusRxIfDecompressionFailedPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppGzipStatusRxIfDecompressedDataSize ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppGzipStatusRxIfCompressedDataSize ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppGzipStatusTxIfWindowSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppGzipStatusTxIfPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppGzipStatusTxIfCompressionFailedPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppGzipStatusTxIfCompressedDataSize ARG_LIST((INT4 ,INT4 *));


INT1
nmhGetPppGzipStatusTxIfDecompressedDataSize ARG_LIST((INT4 ,UINT4 *));


/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMpConfigHeaderFormat ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMpConfigHeaderFormat ARG_LIST((INT4  ,INT4 ));



/* Low Level TEST Routines for.  */

INT1
nmhTestv2MpConfigHeaderFormat ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for PppBACPTable. */
INT1
nmhValidateIndexInstancePppBACPTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppBACPTable  */

INT1
nmhGetFirstIndexPppBACPTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppBACPTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppBACPOperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppBACPFavoredEnd ARG_LIST((INT4 ,INT4 *));

/* Proto Validate Index Instance for PppBACPConfigTable. */
INT1
nmhValidateIndexInstancePppBACPConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppBACPConfigTable  */

INT1
nmhGetFirstIndexPppBACPConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppBACPConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppBACPConfigAdminStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppBACPConfigAdminStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppBACPConfigAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for PppBAPConfigTable. */
INT1
nmhValidateIndexInstancePppBAPConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppBAPConfigTable  */

INT1
nmhGetFirstIndexPppBAPConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppBAPConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppBAPConfigRetransmitTimeOutValue ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppBAPConfigMaxReTransmits ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppBAPConfigRetryRequestFlag ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppBAPConfigRetryRequestTimeOutValue ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppBAPConfigRemoteAllowedRequestTypes ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppBAPConfigRetransmitTimeOutValue ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppBAPConfigMaxReTransmits ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppBAPConfigRetryRequestFlag ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppBAPConfigRetryRequestTimeOutValue ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppBAPConfigRemoteAllowedRequestTypes ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppBAPConfigRetransmitTimeOutValue ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppBAPConfigMaxReTransmits ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppBAPConfigRetryRequestFlag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppBAPConfigRetryRequestTimeOutValue ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppBAPConfigRemoteAllowedRequestTypes ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for PppBAPStatusTable. */
INT1
nmhValidateIndexInstancePppBAPStatusTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppBAPStatusTable  */

INT1
nmhGetFirstIndexPppBAPStatusTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppBAPStatusTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppBAPStatInCallReqs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppBAPStatInCallBackReqs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppBAPStatInLinkDropReqs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppBAPStatInSuccessCallReqs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppBAPStatInSuccessCallBackReqs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppBAPStatInSuccessLinkDropReqs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppBAPStatOutCallReqs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppBAPStatOutCallBackReqs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppBAPStatOutLinkDropReqs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppBAPStatOutSuccessCallReqs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppBAPStatOutSuccessCallBackReqs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppBAPStatOutSuccessLinkDropReqs ARG_LIST((INT4 ,UINT4 *));


/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppExtLinkConfigIfType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtLinkConfigInternationOption ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtLinkConfigLowerIfIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppExtLinkConfigLowerIfDlci ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppExtLinkConfigRestartTimeOutValue ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtLinkConfigRetransmitTimeOutValue ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtLinkConfigMaxReTransmits ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtLinkConfigMaxConfTransmits ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtLinkConfigMaxTermTransmits ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtLinkConfigMaxNakAllowed ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtLinkConfigMaxMagicLoop ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtLinkConfigEchoCharacter ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtLinkConfigPassiveOption ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtLinkConfigIfType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtLinkConfigInternationOption ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtLinkConfigLowerIfIndex ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppExtLinkConfigLowerIfDlci ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppExtLinkConfigRestartTimeOutValue ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtLinkConfigRetransmitTimeOutValue ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtLinkConfigMaxReTransmits ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtLinkConfigMaxConfTransmits ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtLinkConfigMaxTermTransmits ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtLinkConfigMaxNakAllowed ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtLinkConfigMaxMagicLoop ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtLinkConfigEchoCharacter ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtLinkConfigPassiveOption ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtLinkConfigIfType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtLinkConfigInternationOption ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtLinkConfigLowerIfIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppExtLinkConfigLowerIfDlci ARG_LIST((UINT4 *  ,INT4  ,INT4 ));


INT1
nmhTestv2PppTestConfigDiscardTest ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestConfigIdentification ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestConfigMRU ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for PppTestSimulationTable. */
INT1
nmhValidateIndexInstancePppTestSimulationTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppTestSimulationTable  */

INT1
nmhGetFirstIndexPppTestSimulationTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppTestSimulationTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppTestSimulationTxd ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestSimulationPacketType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestSimulationLength ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestSimulationSaveInCounters ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppTestSimulationTxd ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestSimulationPacketType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestSimulationLength ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestSimulationSaveInCounters ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppTestSimulationTxd ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestSimulationPacketType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestSimulationLength ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestSimulationSaveInCounters ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for PppTestLinkConfigTable. */
INT1
nmhValidateIndexInstancePppTestLinkConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppTestLinkConfigTable  */

INT1
nmhGetFirstIndexPppTestLinkConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppTestLinkConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppTestLinkConfigTxACFC ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestLinkConfigRxACFC ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestLinkConfigTxPFC ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestLinkConfigRxPFC ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppTestLinkConfigIfIndex ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestLinkConfigTxACFC ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestLinkConfigRxACFC ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestLinkConfigTxPFC ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestLinkConfigRxPFC ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppTestLinkConfigIfIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestLinkConfigTxACFC ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestLinkConfigRxACFC ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestLinkConfigTxPFC ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestLinkConfigRxPFC ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto type for Low Level GET Routine All Objects.  */


INT1
nmhGetPppDebugModuleMask ARG_LIST((INT4 *));

INT1
nmhGetPppaaaMethod ARG_LIST((INT4 *));

INT1
nmhGetPppMaxNoOfInterfaces ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppDebugModuleMask ARG_LIST((INT4 ));

INT1
nmhSetPppaaaMethod ARG_LIST((INT4 ));

INT1
nmhSetPppMaxNoOfInterfaces ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhGetPPPAuthMode (INT4 *pi4RetValAuthMode);

INT1
nmhTestv2PPPAuthMode (UINT4 *pu4ErrorCode, INT4 i4TestValAuthMode);

INT1
nmhSetPPPAuthMode (INT4 i4SetValAuthMode);


INT1
nmhTestv2PppDebugModuleMask ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2PppaaaMethod ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2PppMaxNoOfInterfaces ARG_LIST((UINT4 *  ,INT4 ));

/* Proto Validate Index Instance for PppTestNcpTable. */
INT1
nmhValidateIndexInstancePppTestNcpTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppTestNcpTable  */

INT1
nmhGetFirstIndexPppTestNcpTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppTestNcpTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppTestNcpIpLocToRemoteAddress ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppTestNcpIpRemoteToLocAddress ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppTestNcpAllowVJForPeer ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestNcpLocAddressNegFlag ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestNcpRemoteAddressNegFlag ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestNcpIpLocToRemotePrimaryDNSAddress ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppTestNcpIpLocToRemoteSecondaryDNSAddress ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppTestNcpIpLocToRemotePrimaryNBNSAddress ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppTestNcpIpLocToRemoteSecondaryNBNSAddress ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppTestNcpIpRemoteToLocPrimaryDNSAddress ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppTestNcpIpRemoteToLocSecondaryDNSAddress ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppTestNcpIpRemoteToLocPrimaryNBNSAddress ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppTestNcpIpRemoteToLocSecondaryNBNSAddress ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPppTestNcpLocPrimaryDNSAddressNegFlag ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestNcpLocSecondaryDNSAddressNegFlag ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestNcpLocPrimaryNBNSAddressNegFlag ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestNcpLocSecondaryNBNSAddressNegFlag ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppTestNcpIpLocToRemoteAddress ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetPppTestNcpIpRemoteToLocAddress ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetPppTestNcpAllowVJForPeer ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestNcpLocAddressNegFlag ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestNcpRemoteAddressNegFlag ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestNcpIpLocToRemotePrimaryDNSAddress ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetPppTestNcpIpLocToRemoteSecondaryDNSAddress ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetPppTestNcpIpLocToRemotePrimaryNBNSAddress ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetPppTestNcpIpLocToRemoteSecondaryNBNSAddress ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetPppTestNcpIpRemoteToLocPrimaryDNSAddress ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetPppTestNcpIpRemoteToLocSecondaryDNSAddress ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetPppTestNcpIpRemoteToLocPrimaryNBNSAddress ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetPppTestNcpIpRemoteToLocSecondaryNBNSAddress ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetPppTestNcpLocPrimaryDNSAddressNegFlag ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestNcpLocSecondaryDNSAddressNegFlag ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestNcpLocPrimaryNBNSAddressNegFlag ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestNcpLocSecondaryNBNSAddressNegFlag ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppTestNcpIpLocToRemoteAddress ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2PppTestNcpIpRemoteToLocAddress ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2PppTestNcpAllowVJForPeer ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestNcpLocAddressNegFlag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestNcpRemoteAddressNegFlag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestNcpIpLocToRemotePrimaryDNSAddress ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2PppTestNcpIpLocToRemoteSecondaryDNSAddress ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2PppTestNcpIpLocToRemotePrimaryNBNSAddress ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2PppTestNcpIpLocToRemoteSecondaryNBNSAddress ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2PppTestNcpIpRemoteToLocPrimaryDNSAddress ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2PppTestNcpIpRemoteToLocSecondaryDNSAddress ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2PppTestNcpIpRemoteToLocPrimaryNBNSAddress ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2PppTestNcpIpRemoteToLocSecondaryNBNSAddress ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2PppTestNcpLocPrimaryDNSAddressNegFlag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestNcpLocSecondaryDNSAddressNegFlag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestNcpLocPrimaryNBNSAddressNegFlag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestNcpLocSecondaryNBNSAddressNegFlag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for PppTestBcpTable. */
INT1
nmhValidateIndexInstancePppTestBcpTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppTestBcpTable  */

INT1
nmhGetFirstIndexPppTestBcpTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppTestBcpTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppTestBcpLoctoRemMACAddress ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppTestBcpRemtoLocMACAddress ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppTestBcpLoctoRemMACAddress ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppTestBcpRemtoLocMACAddress ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppTestBcpLoctoRemMACAddress ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppTestBcpRemtoLocMACAddress ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Proto Validate Index Instance for PppTestIpxTable. */
INT1
nmhValidateIndexInstancePppTestIpxTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppTestIpxTable  */

INT1
nmhGetFirstIndexPppTestIpxTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppTestIpxTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppTestIpxLocalNetNumber ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppTestIpxLocalNodeNumber ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppTestIpxRemoteNodeNumber ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppTestIpxRoutingProtocol ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestIpxRoutingProtoSupport ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestIpxRouterName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppTestIpxNetNumNegFlag ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestIpxNodeNumNegFlag ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestIpxTelebitCompAvailable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestIpxWanSupport ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppTestIpxLocalNetNumber ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppTestIpxLocalNodeNumber ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppTestIpxRemoteNodeNumber ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppTestIpxRoutingProtocol ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestIpxRoutingProtoSupport ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestIpxRouterName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppTestIpxNetNumNegFlag ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestIpxNodeNumNegFlag ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestIpxTelebitCompAvailable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestIpxWanSupport ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppTestIpxLocalNetNumber ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppTestIpxLocalNodeNumber ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppTestIpxRemoteNodeNumber ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppTestIpxRoutingProtocol ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestIpxRoutingProtoSupport ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestIpxRouterName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppTestIpxNetNumNegFlag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestIpxNodeNumNegFlag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestIpxTelebitCompAvailable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestIpxWanSupport ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for PppTestMpConfigTable. */
INT1
nmhValidateIndexInstancePppTestMpConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppTestMpConfigTable  */

INT1
nmhGetFirstIndexPppTestMpConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppTestMpConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppTestMpConfigBundleId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestMpConfigTd ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestMpConfigStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppTestMpConfigBundleId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestMpConfigTd ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestMpConfigStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppTestMpConfigBundleId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestMpConfigTd ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestMpConfigStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));


INT1
nmhSetPppExtIPAddressPoolUpperRange ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetPppExtIPAddressPoolStatus ARG_LIST((INT4  ,INT4 ));


/* Proto Validate Index Instance for PppTestRMConfigTable. */
INT1
nmhValidateIndexInstancePppTestRMConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppTestRMConfigTable  */

INT1
nmhGetFirstIndexPppTestRMConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppTestRMConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppTestRMConfigLocFirstLinkNumber ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppTestRMConfigRemFirstLinkNumber ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppTestRMConfigRequestType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestRMConfigIsPhoneNumberNeeded ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestRMConfigNumberOfPhoneNumbers ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestRMConfigPhoneNumber ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppTestRMConfigLinkType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestRMConfigLinkSpeed ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestRMConfigLinkIndexToBeDeleted ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestRMConfigRequestId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestRMConfigRequester ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestRMConfigAttemptedPhoneNumber ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppTestRMConfigCallStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestRMConfigActionTaken ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestRMConfigResponseToSend ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestRMConfigAddLinkToBundle ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestRMConfigDeleteLinkFromBundle ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestRMConfigCallNotification ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestRMConfigVerifyPeerRequest ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppTestRMConfigIsInitiator ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppTestRMConfigLocFirstLinkNumber ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppTestRMConfigRemFirstLinkNumber ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppTestRMConfigRequestType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestRMConfigIsPhoneNumberNeeded ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestRMConfigNumberOfPhoneNumbers ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestRMConfigPhoneNumber ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppTestRMConfigLinkType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestRMConfigLinkSpeed ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestRMConfigLinkIndexToBeDeleted ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestRMConfigRequestId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestRMConfigRequester ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestRMConfigAttemptedPhoneNumber ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppTestRMConfigCallStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestRMConfigActionTaken ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestRMConfigResponseToSend ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestRMConfigAddLinkToBundle ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestRMConfigDeleteLinkFromBundle ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestRMConfigCallNotification ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestRMConfigVerifyPeerRequest ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppTestRMConfigIsInitiator ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppTestRMConfigLocFirstLinkNumber ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppTestRMConfigRemFirstLinkNumber ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppTestRMConfigRequestType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestRMConfigIsPhoneNumberNeeded ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestRMConfigNumberOfPhoneNumbers ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestRMConfigPhoneNumber ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppTestRMConfigLinkType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestRMConfigLinkSpeed ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestRMConfigLinkIndexToBeDeleted ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestRMConfigRequestId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestRMConfigRequester ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestRMConfigAttemptedPhoneNumber ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppTestRMConfigCallStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestRMConfigActionTaken ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestRMConfigResponseToSend ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestRMConfigAddLinkToBundle ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestRMConfigDeleteLinkFromBundle ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestRMConfigCallNotification ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestRMConfigVerifyPeerRequest ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppTestRMConfigIsInitiator ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for PppTestRMStatusTable. */
INT1
nmhValidateIndexInstancePppTestRMStatusTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppTestRMStatusTable  */

INT1
nmhGetFirstIndexPppTestRMStatusTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppTestRMStatusTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppTestRMStatusRequestId ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppTestRMStatusIfIndex ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for CvTable. */
INT1
nmhValidateIndexInstanceCvTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for CvTable  */

INT1
nmhGetFirstIndexCvTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexCvTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetCvRxACCM ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetCvTxACCM ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetCvIfAdminStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetCvIfAdminStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2CvIfAdminStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Proto Validate Index Instance for PppMplsTable. */
INT1
nmhValidateIndexInstancePppMplsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppMplsTable  */

INT1
nmhGetFirstIndexPppMplsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppMplsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppMplsOperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppMplsAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppMplsRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppMplsAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppMplsRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppMplsAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppMplsRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for PppIp6Table. */
INT1
nmhValidateIndexInstancePppIp6Table ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppIp6Table  */

INT1
nmhGetFirstIndexPppIp6Table ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppIp6Table ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppIp6OperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppIp6LocToRemIfId ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppIp6RemToLocIfId ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppIp6LocToRemCompProt ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppIp6RemToLocCompProt ARG_LIST((INT4 ,INT4 *));

/* Proto Validate Index Instance for PppIp6ConfigTable. */
INT1
nmhValidateIndexInstancePppIp6ConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PppIp6ConfigTable  */

INT1
nmhGetFirstIndexPppIp6ConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPppIp6ConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPppIp6ConfigAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppIp6ConfigLocToRemIfId ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppIp6ConfigRemToLocIfId ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetPppIp6ConfigMethodToGenIfId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppIp6ConfigCompression ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPppIp6ConfigRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPppIp6ConfigAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppIp6ConfigLocToRemIfId ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppIp6ConfigRemToLocIfId ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetPppIp6ConfigMethodToGenIfId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppIp6ConfigCompression ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPppIp6ConfigRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PppIp6ConfigAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppIp6ConfigLocToRemIfId ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppIp6ConfigRemToLocIfId ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2PppIp6ConfigMethodToGenIfId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppIp6ConfigCompression ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PppIp6ConfigRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
