/********************************************************************
* Copyright (C) Future Software Limited, 1997-98,2001
*
* $Id: pppsnmp.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
*
* Description:This file contains registering functions with
*             FutureSNMP
********************************************************************/

#ifdef FUTURE_SNMP_WANTED

#include    "include.h"

#include     "pplcpmdb.h"
#include    "pppoemdb.h"
#include    "stdipmdb.h"
#include    "pppgemdb.h"
#include    "stdaumdb.h"
#include    "stdlcmdb.h"
#include    "ppipcmdb.h"

INT4                RegisterPPPMibswithFutureSNMP (VOID);

extern INT4 SNMP_AGT_RegisterMib PROTO ((tSNMP_GroupOIDType *,
                                         tSNMP_BaseOIDType *,
                                         tSNMP_MIBObjectDescrType *,
                                         tSNMP_GLOBAL_STRUCT, INT4));

static INT4
RegisterStdLCPwithFutureSNMP ()
{
    if (SNMP_AGT_RegisterMib
        ((tSNMP_GroupOIDType *) & stdlcp_FMAS_GroupOIDTable,
         (tSNMP_BaseOIDType *) & stdlcp_FMAS_BaseOIDTable,
         (tSNMP_MIBObjectDescrType *) & stdlcp_FMAS_MIBObjectTable,
         stdlcp_FMAS_Global_data, (INT4) stdlcp_MAX_OBJECTS) != SNMP_SUCCESS)
        return FAILURE;
    return SUCCESS;
}

static INT4
RegisterStdIPCPwithFutureSNMP ()
{
    if (SNMP_AGT_RegisterMib
        ((tSNMP_GroupOIDType *) & stdipcp_FMAS_GroupOIDTable,
         (tSNMP_BaseOIDType *) & stdipcp_FMAS_BaseOIDTable,
         (tSNMP_MIBObjectDescrType *) & stdipcp_FMAS_MIBObjectTable,
         stdipcp_FMAS_Global_data, (INT4) stdipcp_MAX_OBJECTS) != SNMP_SUCCESS)
        return FAILURE;
    return SUCCESS;
}

static INT4
RegisterStdAuthwithFutureSNMP ()
{
    if (SNMP_AGT_RegisterMib
        ((tSNMP_GroupOIDType *) & stdauth_FMAS_GroupOIDTable,
         (tSNMP_BaseOIDType *) & stdauth_FMAS_BaseOIDTable,
         (tSNMP_MIBObjectDescrType *) & stdauth_FMAS_MIBObjectTable,
         stdauth_FMAS_Global_data, (INT4) stdauth_MAX_OBJECTS) != SNMP_SUCCESS)
        return FAILURE;
    return SUCCESS;
}

static INT4
RegisterIpCpwithFutureSNMP ()
{
    if (SNMP_AGT_RegisterMib
        ((tSNMP_GroupOIDType *) & ppipcpfs_FMAS_GroupOIDTable,
         (tSNMP_BaseOIDType *) & ppipcpfs_FMAS_BaseOIDTable,
         (tSNMP_MIBObjectDescrType *) & ppipcpfs_FMAS_MIBObjectTable,
         ppipcpfs_FMAS_Global_data,
         (INT4) ppipcpfs_MAX_OBJECTS) != SNMP_SUCCESS)
        return FAILURE;
    return SUCCESS;
}

static INT4
RegisterLcpExtwithFutureSNMP ()
{
    if (SNMP_AGT_RegisterMib
        ((tSNMP_GroupOIDType *) & pplcpfs_FMAS_GroupOIDTable,
         (tSNMP_BaseOIDType *) & pplcpfs_FMAS_BaseOIDTable,
         (tSNMP_MIBObjectDescrType *) & pplcpfs_FMAS_MIBObjectTable,
         pplcpfs_FMAS_Global_data, (INT4) pplcpfs_MAX_OBJECTS) != SNMP_SUCCESS)
        return FAILURE;
    return SUCCESS;
}

static INT4
RegisterPppGenwithFutureSNMP ()
{
    if (SNMP_AGT_RegisterMib
        ((tSNMP_GroupOIDType *) & pppgenfs_FMAS_GroupOIDTable,
         (tSNMP_BaseOIDType *) & pppgenfs_FMAS_BaseOIDTable,
         (tSNMP_MIBObjectDescrType *) & pppgenfs_FMAS_MIBObjectTable,
         pppgenfs_FMAS_Global_data,
         (INT4) pppgenfs_MAX_OBJECTS) != SNMP_SUCCESS)
        return FAILURE;
    return SUCCESS;
}

static INT4
RegisterPPPOEwithFutureSNMP ()
{
    if (SNMP_AGT_RegisterMib
        ((tSNMP_GroupOIDType *) & pppoe_FMAS_GroupOIDTable,
         (tSNMP_BaseOIDType *) & pppoe_FMAS_BaseOIDTable,
         (tSNMP_MIBObjectDescrType *) & pppoe_FMAS_MIBObjectTable,
         pppoe_FMAS_Global_data, (INT4) pppoe_MAX_OBJECTS) != SNMP_SUCCESS)
        return FAILURE;
    return SUCCESS;
}

INT4
RegisterPPPMibswithFutureSNMP (VOID)
{

    RegisterStdLCPwithFutureSNMP ();
    RegisterStdIPCPwithFutureSNMP ();
    RegisterStdAuthwithFutureSNMP ();
    RegisterIpCpwithFutureSNMP ();
    RegisterLcpExtwithFutureSNMP ();
    RegisterPppGenwithFutureSNMP ();
    RegisterPPPOEwithFutureSNMP ();
    return SUCCESS;

}

#endif /* FUTURE_SNMP_WANTED */
