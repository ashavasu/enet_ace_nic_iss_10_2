/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: stdipcwr.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 ********************************************************************/

# include  "lr.h"
# include  "fssnmp.h"
# include  "stdiplow.h"
# include  "stdipcwr.h"
# include  "stdipcdb.h"

INT4
GetNextIndexPppIpConfigTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexPppIpConfigTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexPppIpConfigTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

VOID
RegisterSTDIPC ()
{
#ifdef LOCK_WANTED
    SNMPRegisterMibWithLock (&stdipcOID, &stdipcEntry, PPP_LOCK, PPP_UNLOCK);
#else
    SNMPRegisterMib (&stdipcOID, &stdipcEntry, SNMP_MSR_TGR_FALSE);
#endif
    SNMPAddSysorEntry (&stdipcOID, (const UINT1 *) "stdipcp");
}

VOID
UnRegisterSTDIPC ()
{
    SNMPUnRegisterMib (&stdipcOID, &stdipcEntry);
    SNMPDelSysorEntry (&stdipcOID, (const UINT1 *) "stdipcp");
}

INT4
PppIpConfigAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePppIpConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPppIpConfigAdminStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
PppIpConfigCompressionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePppIpConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPppIpConfigCompression (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
PppIpConfigAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPppIpConfigAdminStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
PppIpConfigCompressionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPppIpConfigCompression (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
PppIpConfigAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2PppIpConfigAdminStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
PppIpConfigCompressionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2PppIpConfigCompression (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4 PppIpConfigTableDep (UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList,
                          tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return (nmhDepv2PppIpConfigTable (pu4Error, 
                                          pSnmpIndexList, pSnmpvarbinds));
}
