/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: stdlcpwr.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 ********************************************************************/

# include  "lr.h"
# include  "fssnmp.h"
# include  "stdlclow.h"
# include  "stdlcpwr.h"
# include  "stdlcpdb.h"

INT4
GetNextIndexPppLinkConfigTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexPppLinkConfigTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexPppLinkConfigTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

VOID
RegisterSTDLCP ()
{
#ifdef LOCK_WANTED
    SNMPRegisterMibWithLock (&stdlcpOID, &stdlcpEntry, PPP_LOCK, PPP_UNLOCK);
#else
    SNMPRegisterMib (&stdlcpOID, &stdlcpEntry, SNMP_MSR_TGR_FALSE);
#endif
    SNMPAddSysorEntry (&stdlcpOID, (const UINT1 *) "stdlcp");
}

VOID
UnRegisterSTDLCP ()
{
    SNMPUnRegisterMib (&stdlcpOID, &stdlcpEntry);
    SNMPDelSysorEntry (&stdlcpOID, (const UINT1 *) "stdlcp");
}

INT4
PppLinkConfigInitialMRUGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePppLinkConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPppLinkConfigInitialMRU (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
PppLinkConfigMagicNumberGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePppLinkConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPppLinkConfigMagicNumber
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
PppLinkConfigFcsSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePppLinkConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPppLinkConfigFcsSize (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
PppLinkConfigInitialMRUSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPppLinkConfigInitialMRU (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
PppLinkConfigMagicNumberSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPppLinkConfigMagicNumber
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
PppLinkConfigFcsSizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPppLinkConfigFcsSize (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
PppLinkConfigInitialMRUTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2PppLinkConfigInitialMRU (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
PppLinkConfigMagicNumberTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2PppLinkConfigMagicNumber (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
PppLinkConfigFcsSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2PppLinkConfigFcsSize (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4 
PppLinkConfigTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, 
                      tSNMP_VAR_BIND *pSnmpvarbinds)
{
    return (nmhDepv2PppLinkConfigTable (pu4Error, 
                                        pSnmpIndexList, pSnmpvarbinds));
}
