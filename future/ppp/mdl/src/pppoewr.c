/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppoewr.c,v 1.3 2014/03/11 14:02:53 siva Exp $
 *
 * Description:
 *
 ********************************************************************/

# include  "lr.h"
# include  "fssnmp.h"
# include  "pppoelow.h"
# include  "pppoewr.h"
# include  "pppoedb.h"

VOID
RegisterPPPOE ()
{
#ifdef LOCK_WANTED
    SNMPRegisterMibWithLock (&pppoeOID, &pppoeEntry, PPP_LOCK, PPP_UNLOCK);
#else
    SNMPRegisterMib (&pppoeOID, &pppoeEntry, SNMP_MSR_TGR_FALSE);
#endif
    SNMPAddSysorEntry (&pppoeOID, (const UINT1 *) "pppoe");
}

VOID
UnRegisterPPPOE ()
{
    SNMPUnRegisterMib (&pppoeOID, &pppoeEntry);
    SNMPDelSysorEntry (&pppoeOID, (const UINT1 *) "pppoe");
}

INT4
PPPoEModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetPPPoEMode (&(pMultiData->i4_SLongValue)));
}

INT4
PPPoEConfigMaxTotalSessionsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetPPPoEConfigMaxTotalSessions (&(pMultiData->i4_SLongValue)));
}

INT4
PPPoEConfigMaxSessionsPerHostGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetPPPoEConfigMaxSessionsPerHost (&(pMultiData->i4_SLongValue)));
}

INT4
PPPoEConfigPADITxIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetPPPoEConfigPADITxInterval (&(pMultiData->i4_SLongValue)));
}

INT4
PPPoEConfigPADRMaxNumberOfRetriesGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetPPPoEConfigPADRMaxNumberOfRetries
            (&(pMultiData->i4_SLongValue)));
}

INT4
PPPoEConfigPADRWaitTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetPPPoEConfigPADRWaitTime (&(pMultiData->i4_SLongValue)));
}

INT4
PPPoEHostUniqueEnabledGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetPPPoEHostUniqueEnabled (&(pMultiData->i4_SLongValue)));
}

INT4
PPPoEACNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetPPPoEACName (pMultiData->pOctetStrValue));
}

INT4
PPPoEModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetPPPoEMode (pMultiData->i4_SLongValue));
}

INT4
PPPoEConfigMaxTotalSessionsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetPPPoEConfigMaxTotalSessions (pMultiData->i4_SLongValue));
}

INT4
PPPoEConfigMaxSessionsPerHostSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetPPPoEConfigMaxSessionsPerHost (pMultiData->i4_SLongValue));
}

INT4
PPPoEConfigPADITxIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetPPPoEConfigPADITxInterval (pMultiData->i4_SLongValue));
}

INT4
PPPoEConfigPADRMaxNumberOfRetriesSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetPPPoEConfigPADRMaxNumberOfRetries
            (pMultiData->i4_SLongValue));
}

INT4
PPPoEConfigPADRWaitTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetPPPoEConfigPADRWaitTime (pMultiData->i4_SLongValue));
}

INT4
PPPoEHostUniqueEnabledSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetPPPoEHostUniqueEnabled (pMultiData->i4_SLongValue));
}

INT4
PPPoEACNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetPPPoEACName (pMultiData->pOctetStrValue));
}

INT4
PPPoEModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2PPPoEMode (pu4Error, pMultiData->i4_SLongValue));
}

INT4
PPPoEConfigMaxTotalSessionsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2PPPoEConfigMaxTotalSessions
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
PPPoEConfigMaxSessionsPerHostTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2PPPoEConfigMaxSessionsPerHost
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
PPPoEConfigPADITxIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2PPPoEConfigPADITxInterval
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
PPPoEConfigPADRMaxNumberOfRetriesTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2PPPoEConfigPADRMaxNumberOfRetries
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
PPPoEConfigPADRWaitTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2PPPoEConfigPADRWaitTime
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
PPPoEHostUniqueEnabledTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2PPPoEHostUniqueEnabled
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
PPPoEACNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2PPPoEACName (pu4Error, pMultiData->pOctetStrValue));
}

INT4
PPPoEModeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2PPPoEMode (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
PPPoEConfigMaxTotalSessionsDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2PPPoEConfigMaxTotalSessions
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
PPPoEConfigMaxSessionsPerHostDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2PPPoEConfigMaxSessionsPerHost
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
PPPoEConfigPADITxIntervalDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2PPPoEConfigPADITxInterval
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
PPPoEConfigPADRMaxNumberOfRetriesDep (UINT4 *pu4Error,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2PPPoEConfigPADRMaxNumberOfRetries
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
PPPoEConfigPADRWaitTimeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2PPPoEConfigPADRWaitTime
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
PPPoEHostUniqueEnabledDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2PPPoEHostUniqueEnabled
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
PPPoEACNameDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2PPPoEACName (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexPPPoEConfigServiceNameTable (tSnmpIndex * pFirstMultiIndex,
                                         tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexPPPoEConfigServiceNameTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexPPPoEConfigServiceNameTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
PPPoEConfigServiceRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePPPoEConfigServiceNameTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPPPoEConfigServiceRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
PPPoEConfigServiceRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPPPoEConfigServiceRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
PPPoEConfigServiceRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2PPPoEConfigServiceRowStatus (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  pOctetStrValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
PPPoEConfigServiceNameTableDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2PPPoEConfigServiceNameTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexPPPoEBindingsTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexPPPoEBindingsTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexPPPoEBindingsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
PPPoEBindingsEnabledGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePPPoEBindingsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPPPoEBindingsEnabled (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
PPPoEBindingsEnabledSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPPPoEBindingsEnabled (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
PPPoEBindingsEnabledTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2PPPoEBindingsEnabled (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
PPPoEBindingsTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2PPPoEBindingsTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexPPPoEVlanTable (tSnmpIndex * pFirstMultiIndex,
                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexPPPoEVlanTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexPPPoEVlanTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
PPPoEVlanIDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePPPoEVlanTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPPPoEVlanID (pMultiIndex->pIndex[0].i4_SLongValue,
                               &(pMultiData->i4_SLongValue)));
}

INT4
PPPoECFIGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePPPoEVlanTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPPPoECFI (pMultiIndex->pIndex[0].i4_SLongValue,
                            &(pMultiData->i4_SLongValue)));
}

INT4
PPPoECoSGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePPPoEVlanTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPPPoECoS
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));
}

INT4
PPPoEVlanRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstancePPPoEVlanTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetPPPoEVlanRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));
}

INT4
PPPoEVlanIDSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPPPoEVlanID
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));
}

INT4
PPPoECFISet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPPPoECFI
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));
}

INT4
PPPoECoSSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPPPoECoS
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));
}

INT4
PPPoEVlanRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetPPPoEVlanRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));
}

INT4
PPPoEVlanIDTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{
    return (nmhTestv2PPPoEVlanID
            (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiData->i4_SLongValue));

}

INT4
PPPoECFITest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2PPPoECFI
            (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiData->i4_SLongValue));
}

INT4
PPPoECoSTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2PPPoECoS
            (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiData->i4_SLongValue));
}

INT4
PPPoEVlanRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2PPPoEVlanRowStatus
            (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiData->i4_SLongValue));
}

INT4
PPPoEVlanTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2PPPoEVlanTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
