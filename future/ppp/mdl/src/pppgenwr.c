/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppgenwr.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 ********************************************************************/

# include  "lr.h"
# include  "fssnmp.h"
# include  "pppgelow.h"
# include  "pppgenwr.h"
# include  "pppgendb.h"

VOID
RegisterPPPGEN ()
{
#ifdef LOCK_WANTED
    SNMPRegisterMibWithLock (&pppgenOID, &pppgenEntry, PPP_LOCK, PPP_UNLOCK);
#else
    SNMPRegisterMib (&pppgenOID, &pppgenEntry, SNMP_MSR_TGR_FALSE);
#endif
    SNMPAddSysorEntry (&pppgenOID, (const UINT1 *) "pppgenfs");
}

VOID
UnRegisterPPPGEN ()
{
    SNMPUnRegisterMib (&pppgenOID, &pppgenEntry);
    SNMPDelSysorEntry (&pppgenOID, (const UINT1 *) "pppgenfs");
}

INT4 PppExtIPAddressPoolSelectorGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhGetPppExtIPAddressPoolSelector(&(pMultiData->i4_SLongValue)));
}

INT4
PppDebugLevelMaskGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetPppDebugLevelMask (&(pMultiData->i4_SLongValue)));
}

INT4
PppExtIPAddressPoolSelectorSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetPppExtIPAddressPoolSelector (pMultiData->i4_SLongValue));
}

INT4
PppDebugLevelMaskSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetPppDebugLevelMask (pMultiData->i4_SLongValue));
}

INT4
PppExtIPAddressPoolSelectorTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2PppExtIPAddressPoolSelector
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
PppDebugLevelMaskTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2PppDebugLevelMask (pu4Error, pMultiData->i4_SLongValue));
}

INT4 PppExtIPAddressPoolSelectorDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
        return(nmhDepv2PppExtIPAddressPoolSelector(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 PppDebugLevelMaskDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
        return(nmhDepv2PppDebugLevelMask(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
