/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: genproto.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains GSEM function prototype used 
 * internally by GSEM-GCP module
 *
 *******************************************************************/
#ifndef __PPP_GENPROTO_H__
#define __PPP_GENPROTO_H__


VOID     GSEMRestartIfNeeded(tGSEM *pGSEM);
INT1     GSEMInit(tGSEM *pGSEM, UINT1 NumOpts);
VOID     GSEMRun(tGSEM *pGSEM, UINT1    Event);
UINT1    GSEMillegalState(tGSEM *pGSEM);
UINT1    GSEMReleaseBuffer(tGSEM *pGSEM);
UINT1    GSEMMoveToClosed(tGSEM *pGSEM);
UINT1    GSEMSendConfReq(tGSEM *pGSEM);
UINT1    GSEMMoveToInitial(tGSEM *pGSEM);
UINT1    GSEMReqLLToUp(tGSEM *pGSEM);
UINT1    GSEMMoveToStarting(tGSEM *pGSEM);
UINT1    GSEMLeavingOpened(tGSEM *pGSEM);
UINT1    GSEMMoveToStopping(tGSEM *pGSEM);
UINT1    GSEMRelLLMovToInitial(tGSEM *pGSEM);
UINT1    GSEMMoveToClosing(tGSEM *pGSEM);
UINT1    GSEMSendTermReq(tGSEM *pGSEM);
UINT1    GSEMLeaveOpenSendTermReq(tGSEM *pGSEM);
UINT1    GSEMReSendTermReq(tGSEM *pGSEM);
UINT1    GSEMResendTermReq(tGSEM *pGSEM);
UINT1    GSEMResendConfReq(tGSEM *pGSEM);
UINT1    GSEMResendConfReqMovToReqSent(tGSEM *pGSEM);
UINT1    GSEMRelLLMovToClosed(tGSEM *pGSEM);
UINT1    GSEMRelLLMovToStopped(tGSEM *pGSEM);
UINT1    GSEMRelLLMovToStoppedPassive(tGSEM *pGSEM);
UINT1    GSEMSendTermAck(tGSEM *pGSEM);
UINT1    GSEMSendConfReqConfAck(tGSEM *pGSEM);
UINT1    GSEMSendConfAck(tGSEM *pGSEM);
UINT1    GSEMSendConfAckEnterOpened(tGSEM *pGSEM);
UINT1    GSEMLeaveOpenedSendConfReqAck(tGSEM *pGSEM);
UINT1    GSEMSendConfReqConfNak(tGSEM *pGSEM);
UINT1    GSEMSendConfNak(tGSEM *pGSEM);
UINT1    GSEMSendConfNakMovToReqSent(tGSEM *pGSEM);
UINT1    GSEMLeaveOpenedSendConfReqNak(tGSEM *pGSEM);
UINT1    GSEMMovToAckRcvd(tGSEM *pGSEM);
UINT1    GSEMSendConfReqCross(tGSEM *pGSEM);
UINT1    GSEMEnterOpened(tGSEM *pGSEM);
UINT1    GSEMLeaveOpenedSendConfReqCross(tGSEM *pGSEM);
UINT1    GSEMSendConfReqInAckSent(tGSEM *pGSEM);
UINT1    GSEMSendTermAckMovToReqSent(tGSEM *pGSEM);
UINT1    GSEMLeaveOpenedSendTermAck(tGSEM *pGSEM);
UINT1    GSEMMovToReqSent(tGSEM *pGSEM);
UINT1    GSEMLeaveOpenedSendConfReq(tGSEM *pGSEM);
UINT1    GSEMSendCodeRej(tGSEM *pGSEM);
UINT1    GSEMLeaveOpenedMovToStopped(tGSEM *pGSEM);
UINT1    GSEMProcessRxrPkt(tGSEM *pGSEM);
UINT1    GSEMTxData(tGSEM *pGSEM);
UINT1    GSEMRxData(tGSEM *pGSEM);

UINT1    GSEMProcessResetRequest ( tGSEM *pGSEM );
UINT1    GSEMProcessResetAck ( tGSEM *pGSEM );
UINT1    GSEMProcessRxrPktInOtherStates(tGSEM *pGSEM);

INT2  GCPAddConfigInfo ( tGSEM *pGSEM, t_MSG_DESC *);
INT1     GCPRecdConfigAck(tGSEM *, t_MSG_DESC *pInpacket, UINT2   Length, UINT1    Identifier);
INT1     GCPRecdConfigNak(tGSEM *, t_MSG_DESC *pInpacket, UINT2   Length, UINT1    Identifier);
INT1     GCPRecdConfigRej(tGSEM *, t_MSG_DESC *pInpacket, UINT2   Length, UINT1    Identifier);
INT1     GCPRecdConfigReq(tGSEM *, t_MSG_DESC *pInpacket, UINT2   Length, UINT1    Identifer);
INT1     GCPRecdTermReq(tGSEM *, t_MSG_DESC *pInpacket, UINT2   Length, UINT1    Identifier);
INT1     GCPRecdTermAck(tGSEM *, t_MSG_DESC *pInpacket, UINT2   Length, UINT1    Identifer);
INT1     GCPRecdCodeRej(tGSEM *, t_MSG_DESC *pInpacket, UINT2   Length, UINT1    Identifier);

INT1     GCPExtractAndValidateOption ( tGSEM *pGSEM, t_MSG_DESC *pInPkt, UINT1 *Type, UINT1 *Len, tOptVal *Info, UINT2 Offset ,UINT2 RemLen );

INT1     GCPMatchProtNCode ( UINT2 Prot, UINT2 Code );

INT1     GCPGetReqResp ( tGSEM *pGSEM, t_MSG_DESC *pInPkt, UINT2 RemLen );

INT1    GCPHandleMaxNakLoops(tGSEM *pGSEM, t_MSG_DESC *pInPkt, UINT2 InPktLength, UINT2 *ConstructedNakPktLen);

VOID     GSEMCleanUp(tGSEM *pGSEM);

UINT1    UTILSendReqPkt(tGSEM *pGSEM, UINT1 TxFlag, UINT1 Code);
UINT1    UTILSendTermAck(tGSEM *pGSEM);



#endif  /* __PPP_GENPROTO_H__ */
