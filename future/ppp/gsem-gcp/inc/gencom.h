/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: gencom.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the common PPP files used by the 
 * GSEM-GCP module
 *
 *******************************************************************/
#ifndef __PPP_GENCOM_H__
#define __PPP_GENCOM_H__




#include "genhdrs.h"

/* Global PPP includes */
#include "pppmacro.h"
#include "pppdefs.h"
#include "pppdebug.h"
#include "ppptimer.h"


/* Local includes */
#include "pppgsem.h"
#include "pppgcp.h"

/* Includes from other modules */
#include "frmtdfs.h"
#include "ppplcp.h"

#include "pppexts.h"

#include "genproto.h"

#include "pppdbgex.h"


#endif  /* __PPP_GENCOM_H__ */
