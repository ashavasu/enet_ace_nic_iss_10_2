/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppgcp.h,v 1.2 2011/01/06 13:52:41 siva Exp $
 *
 * Description:
 *
 *******************************************************************/
#ifndef __PPP_PPPGCP_H__
#define __PPP_PPPGCP_H__


typedef union {
    UINT4               LongVal;
    UINT2               ShortVal;
        UINT1           CharVal;
    UINT1               u1Rsvd;
    UINT1               *StrVal;
}tOptVal;

typedef struct{
        UINT4           NoOfDiscEntries;
}tDiscHeader;

typedef struct {
        tOptVal         MinVal;
        tOptVal         MaxVal;
}tRangeInfo;

typedef struct {
        INT1            (*pHandleConfReq)(tGSEM *, t_MSG_DESC *, tOptVal, UINT2 , UINT1);
        INT1            (*pHandleConfNak)(tGSEM *, t_MSG_DESC *, tOptVal);
        INT1            (*pHandleConfRej)(tGSEM *);
        VOID            *(*pRetDtaPtr)( tGSEM  *, UINT1 *);
        INT1            (*pHandleAddSubOpt)( tGSEM *, tOptVal, t_MSG_DESC *);
}tOptHandlerFuns;

typedef struct GenOptInfo{
        UINT1                           OptType;
        INT1                            Category;
        INT1                            BasicType;
        UINT1                           MaxOptLen;
    tOptHandlerFuns             *pOptHandlerFuns;
        union {
            tRangeInfo          *pRangeInfo;
            VOID                        *pDiscHeader;
        }OptPossibleInfo;
        UINT1                           RepeatAllowFlag;
	UINT1				u1Rsvd1;
	UINT2				u2Rsvd2;
}tGenOptionInfo;

typedef struct NegFlagsPerIf {
        UINT1           OptType;
        UINT1           FlagMask;
	UINT2		u2Rsvd;
}tOptNegFlagsPerIf;

typedef struct {
        UINT2           Protocol;
        UINT1           Code;
	UINT1		u1Rsvd;
        INT1            (*pPktHandler)(tGSEM *,t_MSG_DESC *, UINT2, UINT1);
}tMiscPktHandler;


typedef struct { 
        UINT2           ProtID;
	UINT2		u2Rsvd;
        tGSEM           *(*pGetSEMPtr)(struct pppif *);
        INT1            (*pCPCopyOptions)(tGSEM *, UINT1, UINT1);
}tProtSEM;

#endif  /* __PPP_PPPGCP_H__ */
