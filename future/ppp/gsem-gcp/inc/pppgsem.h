/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppgsem.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the typedefs of GSEM 
 *             module of PPP subsystem.
 *
 *******************************************************************/
#ifndef __PPP_PPPGSEM_H__
#define __PPP_PPPGSEM_H__

#define    DEF_RESTART_TIMEOUT          3
#define    DEF_MAX_TERM_REQ             2
#define    DEF_MAX_CONF_REQ             10
#define    DEF_MAXNAK_LOOPS             5

typedef struct GSEM {
	UINT1                 CurrentState;
	UINT1                 CurrentEvent;
	UINT1                 LastState;
	UINT1                 LastEvent;
	struct pppif         *pIf;
	UINT2                 Protocol;      /*   Data Link Layer Protocol field value */
	UINT1                 OptionFlags;   /*   Close and Passive Option bits        */
	UINT1                 CurrentId;     /*   Current id                           */
	UINT1                 RequestId;     /*   Current request id                   */
	UINT1                 NakLoops;
	UINT2                 Retransmits;          /* Number of retransmissions left  */
	tPPPTimer             RestartTimer;
	t_MSG_DESC           *pInParam;       /*   points to the incoming PDU           */
	t_MSG_DESC           *pOutParam;      /*   points to the outgoing PDU           */
	struct GSEMCallBacks	*pCallbacks;	    /*  Callback routines                     */
	struct GenOptInfo    *pGenOptInfo;
	struct NegFlagsPerIf *pNegFlagsPerIf; 
	UINT1                 MaxOptTypes;
	UINT1				  NumOptReq;
	UINT2			u2Rsvd;
	t_MSG_DESC			 *pStoredPkt;
	union {
		UINT1             CharValue;
		UINT2             ShortValue;
		UINT4             LongValue;
	} MiscParam;
} tGSEM;

typedef struct GSEMCallBacks {
	VOID	(*pUp)(tGSEM *);		/* Called when GSEM reaches OPENED state */
	VOID	(*pDown)(tGSEM *);	/* Called when GSEM leaves OPENED state  */
	VOID	(*pStarting)(tGSEM *); /* Called when we want the lower layer */
	VOID	(*pFinished)(tGSEM *);	/* Called when we don't want the LL      */
	VOID	(*pRxDataHandler)(tGSEM *);	/* Called to process received data 	*/
	VOID	(*pTxDataHandler)(tGSEM *);	/* Called to transmit data 	*/
	VOID	(*pHandleRRR) (tGSEM *); /* For ECP/CCP to handle Reset Request */	
	VOID	(*pHandleRRA) (tGSEM *);	/* For ECP/CCP to handle Reset Ack */
} tGSEMCallbacks;


#endif  /* __PPP_PPPGSEM_H__ */
