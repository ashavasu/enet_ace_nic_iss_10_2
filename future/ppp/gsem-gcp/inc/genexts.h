/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: genexts.h,v 1.2 2011/10/13 10:31:23 siva Exp $
 *
 * Description:This file contains definitions and prototypes for 
 *             GSEM-GCP functions  used by external modules.
 *
 *******************************************************************/
#ifndef __PPP_GENEXTS_H__
#define __PPP_GENEXTS_H__


extern  UINT1               *pRecdOptFlags;
extern  UINT1               AuthInitFlag;
extern  tOptHandlerFuns   BooleanHandlingFuns;
extern  tProtSEM          ProtSEMTable[];
extern  INT2              ProtIndex;    
extern  INT2              DataIndex;    

/*** SKR001 ***/
extern  INT2              OutDataPktIndex;
extern   UINT1             OutReleaseFlag;

extern  tGSEM                *pTermGSEM;
extern  tGSEM                *pRestartGSEM;

/* Function prototypes */

VOID GCPInput(tGSEM *pGSEM, t_MSG_DESC *pInPkt, UINT2 Length);
INT1 GCPGetOptIndex(tGSEM *pGSEM, UINT1 OptType);
INT1 GCPGetProtIndex ( UINT2 Protocol );

VOID NCPRxData(tGSEM *pGSEM);
VOID NCPTxData(tGSEM *pGSEM);

VOID GSEMTimeOut(VOID *pGSEM);
VOID GSEMDelete(tGSEM *pGSEM);



#endif  /* __PPP_GENEXTS_H__ */
