/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: gcpdefs.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains definitions used by the GCP module
 *
 *******************************************************************/
#ifndef __PPP_GCPDEFS_H__
#define __PPP_GCPDEFS_H__


#define  MIN_OPT_LEN         2
#define  OPT_LEN_OFFSET      1

#define  CP_HDR_LEN          4
#define  OPT_HDR_LEN         2
#define  MAX_PROT_SIZE       6

#define  STRING_TYPE        -1
#define  BOOL_TYPE           0
#define  BYTE_LEN_0          0
#define  BYTE_LEN_1          1
#define  BYTE_LEN_2          2
#define  BYTE_LEN_3          3
#define  BYTE_LEN_4          4
#define  BYTE_LEN_5          5
#define  BYTE_LEN_6          6

/* IPv6CP Changes -- start */
#define  BYTE_LEN_8          8
/* IPv6CP Changes -- End */

#define  RANGE               1
#define  BOOLEAN_CATEGORY    2
#define  DISCRETE            3


#define  ACK                 0

#define  MATCH               0

#define  FREE_ALL            3

#define  MIN_GEN_CODE        1
#define  MAX_GEN_CODE        7


#endif  /* __PPP_GCPDEFS_H__ */
