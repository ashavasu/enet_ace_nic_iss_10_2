/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: gsemdefs.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 *******************************************************************/
#ifndef __PPP_GSEMDEFS_H__
#define __PPP_GSEMDEFS_H__


#define  NO_ECHO                  1
#define  ECHO_RCVD                2
#define  LOOP_BACK                3

#define  CLOSE_OPTION          0x02
#define  NO_CLOSE_OPTION       0xfd

#define  PPP_UP                   0
#define  PPP_DOWN                 1
#define  OPEN                     2
#define  CLOSE                    3
#define  TO_PLUS                  4
#define  TO_MINUS                 5
#define  RCR_PLUS                 6
#define  RCR_MINUS                7
#define  RCA                      8
#define  RCN                      9
#define  RTR                     10
#define  RTA                     11
#define  RUC                     12
#define  RXJ_PLUS                13
#define  RXJ_MINUS               14
#define  RXR                     15
#define  RST                     16
#define  TXD                     17
#define  RXD                     18
#define  RRR                     19
#define  RRA                     20
#define  DISCARD                 -1
#define  STATES                  10

#define  MAX_SIZE                45
#define  EVENTS                  21


#define  INITIAL                  0
#define  STARTING                 1
#define  CLOSED                   2
#define  STOPPED                  3
#define  CLOSING                  4
#define  STOPPING                 5
#define  REQSENT                  6
#define  ACKRCVD                  7
#define  ACKSENT                  8
#define  OPENED                   9

#define  RETRANSMISSION           1
#define  NORMAL_TRANSMISSION      0

#define  CLOSE_SEM                1
#define  NO_CLOSE_SEM             2

/*********************************************************************/
/*          CONSTANTS  USED BY THE GSEM  MODULE                      */
/*********************************************************************/

#define      A0           0
#define      A1           1
#define      A2           2
#define      A3           3
#define      A4           4
#define      A5           5
#define      A6           6
#define      A7           7
#define      A8           8
#define      A9           9
#define      A10          10
#define      A11          11
#define      A12          12
#define      A13          13
#define      A14          14
#define      A15          15
#define      A16          16
#define      A17          17
#define      A18          18
#define      A19          19
#define      A20          20
#define      A21          21
#define      A22          22
#define      A23          23
#define      A24          24
#define      A25          25
#define      A26          26
#define      A27          27
#define      A28          28
#define      A29          29
#define      A30          30
#define      A31          31
#define      A32          32
#define      A33          33
#define      A34          34
#define      A35          35
#define      A36          36
#define      A37          37
#define      A38          38
#define      A39          39
#define      A40          40
#define      A41          41
#define      A42          42
#define      A43          43
#define      A44          44



#endif  /* __PPP_GSEMDEFS_H__ */
