/********************************************************************
 * Copyright (C) Future Software Limited, 1997-98,2001
 *
 * $Id: pppcom.c,v 1.2 2014/03/11 14:02:48 siva Exp $
 *
 * Description:This file contains the utility functions used by 
 *             various modules of PPP subsystem.
 *
 *******************************************************************/
#include "gencom.h"
#include "gcpdefs.h"
#include "genexts.h"
#include "globexts.h"

/*********************************************************************
*    Function Name    :    UTILFillPktHeader 
*    Description        :
*        This function is used to fill the outgoing packet header.
*    Parameter(s)    :
*        pOutBuf    -    Points to the output buffer. 
*        Protocol    -    Protocol field of the packet.
*        Code    -    Code field of the packet.
*        Identifier    -    Identifier field of the packet.
*        Length    -    Length field of the packet.
*    Return Values    :    VOID
*********************************************************************/
VOID
UTILFillPktHeader (t_MSG_DESC * pOutBuf, UINT1 Code, UINT1 Identifier,
                   UINT2 Length)
{
    ASSIGN1BYTE (pOutBuf, CODE_OFFSET, Code);
    ASSIGN1BYTE (pOutBuf, ID_OFFSET, Identifier);
    ASSIGN2BYTE (pOutBuf, LEN_OFFSET, Length);
}

/*********************************************************************
*    Function Name    :    UTILDiscardPkt
*    Description        :
*    This function is invoked when the packet is discarded. It updates
*    the interface counter and logs error messages when the debugging 
*    is enabled.
*    Parameter(s)    :
*            pIf    -    Pointer to the PPP interface structure.
*            Msg    -    Error message to be logged
*    Return Values    :    VOID
*********************************************************************/
VOID
UTILDiscardPkt (tPPPIf * pIf, UINT1 const *Msg)
{
    PPP_TRC1 (ALL_FAILURE, "DISCARD:%s", Msg);

    if (pIf != NULL)
    {
        pIf->LLHCounters.InErrors++;
    }
    OutReleaseFlag = PPP_YES;
    return;
}

/*********************************************************************
*   Function Name   :    UtilExtractAndValidateCPHdr 
*   Description     :
*    This function extracts the code, id and length from a control
*    protocol packet and validates the length.
*    Parameter(s)    : 
*    pIf    -    Pointer to the PPP interface structure over which the 
*                        packet has been received.
*    pInPkt    -    Pointer to the incoming packet. 
*    Length    -    Length of the incoming packet (as given by LLI). 
*    Code    -    Pointer to the extracted code value be filled up.  
*    Id    -    Pointer to the extracted identifier value to be 
*            filled up. 
*    Len    -    Pointer to the extracted length to be filled up. 
*    Return Vale    :    DISCARD if packet is invalid / OK otherwise.   
*********************************************************************/
INT1
UtilExtractAndValidateCPHdr (tPPPIf * pIf, t_MSG_DESC * pInPkt, UINT2 Length,
                             UINT1 *Code, UINT1 *Id, UINT2 *Len)
{

    if (pIf == NULL)
    {
        PPP_TRC1 (ALL_FAILURE, "DISCARD:%s", "No Interface Record");
        return (DISCARD);
    }

    if (Length < CP_HDR_LEN)
    {
        DISCARD_PKT (pIf, "Rcvd too short header.");
        return (DISCARD);
    }

    EXTRACT1BYTE (pInPkt, *Code);
    EXTRACT1BYTE (pInPkt, *Id);
    EXTRACT2BYTE (pInPkt, *Len);

    if (pIf->BundleFlag == BUNDLE)
    {
        if (*Len != Length)
        {
            DISCARD_PKT (pIf, "Illegal length control/authentication packet.");
            return (DISCARD);
        }
    }
    else
    {
        if ((*Len != Length) ||
            ((*Len > DEF_MRU_SIZE) && (*Len > pIf->LcpOptionsAckedByPeer.MRU)))
        {

            DISCARD_PKT (pIf, "Illegal length control/authentication packet.");
            return (DISCARD);
        }
    }

    *Len = (UINT2) (*Len - CP_HDR_LEN);
    return (OK);
}
