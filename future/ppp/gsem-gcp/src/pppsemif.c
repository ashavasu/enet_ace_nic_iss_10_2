/********************************************************************
 * Copyright (C) Future Software Limited, 1997-98,2001
 *
 * $Id: pppsemif.c,v 1.5 2014/12/16 11:47:23 siva Exp $
 *
 * Description:This file contains the interface procedures and 
 *             other primitive functions of the GSEM module.
 *
 *******************************************************************/
#include "gencom.h"
#include "gsemdefs.h"
#include "genexts.h"
#include "gcpdefs.h"
#include "lcpdefs.h"
/* Makefile changes */
#include "globexts.h"

/* #ifdef L2TP_WANTED */
#include "pppl2tp.h"
/* #endif */
#include "authcom.h"
#include "pppccpmd.h"
#include "ppccplow.h"

UINT2               StoredPktCounter;

UINT1               (*ActionProcedure[MAX_SIZE]) (tGSEM * pGSEMPtr) =
{
    /* A0   */ GSEMReleaseBuffer,
        /* A1   */ GSEMillegalState,
        /* A2   */ GSEMMoveToClosed,
        /* A3   */ GSEMSendConfReq,
        /* A4   */ GSEMMoveToInitial,
        /* A5   */ GSEMReqLLToUp,
        /* A6   */ GSEMMoveToStarting,
        /* A7   */ GSEMLeavingOpened,
        /* A8   */ GSEMMoveToStopping,
        /* A9   */ GSEMRelLLMovToInitial,
        /* A10  */ GSEMMoveToClosing,
        /* A11  */ GSEMSendTermReq,
        /* A12  */ GSEMLeaveOpenSendTermReq,
        /* A13  */ GSEMResendTermReq,
        /* A14  */ GSEMResendConfReq,
        /* A15  */ GSEMResendConfReqMovToReqSent,
        /* A16  */ GSEMRelLLMovToClosed,
        /* A17  */ GSEMRelLLMovToStopped,
        /* A18  */ GSEMRelLLMovToStoppedPassive,
        /* A19  */ GSEMSendTermAck,
        /* A20  */ GSEMSendConfReqConfAck,
        /* A21  */ GSEMSendConfAck,
        /* A22  */ GSEMSendConfAckEnterOpened,
        /* A23  */ GSEMLeaveOpenedSendConfReqAck,
        /* A24  */ GSEMSendConfReqConfNak,
        /* A25  */ GSEMSendConfNak,
        /* A26  */ GSEMSendConfNakMovToReqSent,
        /* A27  */ GSEMLeaveOpenedSendConfReqNak,
        /* A28  */ GSEMMovToAckRcvd,
        /* A29  */ GSEMSendConfReqCross,
        /* A30  */ GSEMEnterOpened,
        /* A31  */ GSEMLeaveOpenedSendConfReqCross,
        /* A32  */ GSEMSendConfReqInAckSent,
        /* A33  */ GSEMSendTermAckMovToReqSent,
        /* A34  */ GSEMLeaveOpenedSendTermAck,
        /* A35  */ GSEMMovToReqSent,
        /* A36  */ GSEMLeaveOpenedSendConfReq,
        /* A37  */ GSEMSendCodeRej,
        /* A38  */ GSEMLeaveOpenedMovToStopped,
        /* A39  */ GSEMProcessRxrPkt,
        /* A40  */ GSEMTxData,
        /* A41  */ GSEMRxData
        ,
        /* A42  */ GSEMProcessResetRequest,
        /* A43  */ GSEMProcessResetAck,
        /* A44  */ GSEMProcessRxrPktInOtherStates
};

/* Makefile changes - Missing braces around GSET[0] */
UINT1               GSET[EVENTS][STATES] = {
/*
States:   Initial Start closed Stopped Closing Stopping Reqsent Ackrcvd Acksent Opened
Events
*/
/*Up */
    {A2, A3, A1, A1, A1, A1, A1, A1, A1, A1},
/*Down*/
    {A1, A1, A4, A5, A4, A6, A6, A6, A6, A7},
/*Open*/
    {A5, A0, A3, A0, A8, A0, A0, A0, A0, A0},
/*Close*/
    {A0, A9, A0, A2, A0, A10, A11, A11, A11, A12},
/*T0_PLUS*/
    {A1, A1, A1, A1, A13, A13, A15, A15, A14, A1},
/*T0_MINUS*/
    {A1, A1, A1, A1, A16, A17, A18, A18, A18, A1},
/*RCR_PLUS*/
    {A1, A1, A19, A20, A0, A0, A21, A22, A21, A23},
/*RCR_MINUS*/
    {A1, A1, A19, A24, A0, A0, A25, A25, A26, A27},
     /*RCA*/ {A1, A1, A19, A19, A0, A0, A28, A29, A30, A31},
     /*RCN*/ {A1, A1, A19, A19, A0, A0, A3, A29, A32, A31},
     /*RTR*/ {A1, A1, A19, A19, A19, A19, A19, A33, A33, A34},
     /*RTA*/ {A1, A1, A0, A0, A16, A17, A0, A35, A0, A36},
     /*RUC*/ {A1, A1, A37, A37, A37, A37, A37, A37, A37, A37},
/*RXJ_PLUS*/
    {A1, A1, A0, A0, A0, A0, A0, A35, A0, A0},
/*RXJ_MINUS*/
    {A1, A1, A16, A17, A16, A17, A17, A17, A17, A38},
     /*RXR*/ {A1, A1, A44, A44, A44, A44, A44, A44, A44, A39},
     /*RST*/ {A1, A1, A0, A0, A0, A0, A3, A3, A3, A1},
     /*TXD*/ {A1, A1, A1, A1, A1, A1, A1, A1, A1, A40},
     /*RXD*/ {A1, A1, A0, A0, A0, A0, A0, A0, A0, A41}

    ,
     /*RRR*/ {A1, A1, A0, A0, A0, A0, A0, A0, A0, A42},
     /*RRA*/ {A1, A1, A0, A0, A0, A0, A0, A0, A0, A43}
};

/*********************************************************************
*  Function Name : GSEMInit 
*  Description   :
*               This function initialises the GSEMStructure.
*  packet.
*  Parameter(s)  :
*           pGSEM        -   points to the tGSEM  structure to be initialized.
*            NumOptions    -    Number of options supported by the control protocol.
*                            for which this SEM is being initialized.
*  Return Value : OK/NOT_OK
*********************************************************************/
INT1
GSEMInit (tGSEM * pGSEM, UINT1 NumOptions)
{
    pGSEM->CurrentState = INITIAL;
    pGSEM->CurrentEvent = (UINT1) INIT_VAL;
    pGSEM->LastState = INITIAL;
    pGSEM->LastEvent = (UINT1) INIT_VAL;
    pGSEM->OptionFlags = 0;
    pGSEM->CurrentId = 0;
    pGSEM->RequestId = 0;
    pGSEM->NakLoops = 0;
    pGSEM->pInParam = NULL;
    pGSEM->pOutParam = NULL;
    pGSEM->pStoredPkt = NULL;
    pGSEM->pCallbacks = NULL;
    pGSEM->MaxOptTypes = NumOptions;

    PPP_INIT_TIMER (pGSEM->RestartTimer);
    if ((pGSEM->pNegFlagsPerIf =
         (struct NegFlagsPerIf *) (VOID *) ALLOC_NEG_FLAGS (pGSEM->
                                                            MaxOptTypes)) ==
        NULL)
    {
        return (NOT_OK);
    }
    return (OK);
}

/*********************************************************************
*  Function Name : GSEMRun
*  Description   :
*                         This is an interface function of GSEM module.
*  It executes the action procedure corressponding to the event that triggered
*  the call and the current state.                
*  Parameter(s)  :  
*   pGSEM - points to the tGSEM copy of the calling module
*        Event - indicates the event that triggered this function call
*  Return Value : VOID
*********************************************************************/
VOID
GSEMRun (tGSEM * pGSEM, UINT1 Event)
{
    UINT1               ActionProcedureIndex;
    UINT1               State;

    /*  SEM action procedure is called only if processing is reqd.. */

    pGSEM->LastEvent = pGSEM->CurrentEvent;
    pGSEM->CurrentEvent = Event;

    PrintState (pGSEM->CurrentState);
    PrintEvent (pGSEM->CurrentEvent);

    State = pGSEM->CurrentState;

    if (pGSEM->OptionFlags & CLOSE_OPTION)
    {
        /* Link termination due to unsuccessful Option Negotiation... */
        pGSEM->Retransmits = pGSEM->pIf->CurrentLinkConfigs.MaxTermtransmits;
        if (UTILSendReqPkt (pGSEM, NORMAL_TRANSMISSION, TERM_REQ_CODE) == TRUE)
        {
            pGSEM->OptionFlags &= NO_CLOSE_OPTION;
            State = STOPPING;    /*02032001 */
        }
    }
    else
    {
        ActionProcedureIndex = GSET[pGSEM->CurrentEvent][pGSEM->CurrentState];

        if (ActionProcedure[ActionProcedureIndex] != NULL)
        {
            State = (*ActionProcedure[ActionProcedureIndex]) (pGSEM);
        }

    }

    PPP_TRC (EVENT_TRC, "Return [State] :  ");
    PrintState (State);

    pGSEM->LastState = pGSEM->CurrentState;
    pGSEM->CurrentState = State;

    if ((pGSEM->CurrentState == INITIAL) || (pGSEM->CurrentState == STARTING)
        || (pGSEM->CurrentState == CLOSED) || (pGSEM->CurrentState == STOPPED)
        || (pGSEM->CurrentState == OPENED))
    {
        GSEMCleanUp (pGSEM);
    }

    return;
}

/**********************************************************************
*  Function Name :UTILSendReqPkt
*  Description   :
*     This function is used to construct and transmit a configuration 
*  request to the peer. It is used by the action procedures of the GSEM
*  module.
*  Parameter(s)  :  
*     pGSEM          -  points to the tGSEM copy of the calling module
*     Retransmission -  indicates whether the request is retransmitted.
*     Code           -  indicates whether the pkt is CONF_REQ/TERM_REQ  
*  Return Value :  TRUE/FALSE
*********************************************************************/
UINT1
UTILSendReqPkt (tGSEM * pGSEM, UINT1 TxFlag, UINT1 Code)
{
    UINT2               Length;
    t_MSG_DESC         *pOutBuf;
#ifdef CCP
    tAUTHIf            *pAuthPtr = NULL;
    INT4                OptionType = 0;
#endif
    Length = CP_HDR_LEN;

    if ((Code == TERM_REQ_CODE) && (pGSEM->Protocol == LCP_PROTOCOL)
        && ((pGSEM->CurrentState == OPENED) || (pGSEM->CurrentState == CLOSING)
            || (pGSEM->CurrentState == STOPPING)))
    {
        pGSEM->pIf->TermReqFlag = SET;
    }
    /*
     * If the negotiated authentication protocol is MSCHAP, then only
     * CCP conf request for MPPE is sent out.
     */
    if ((Code == CONF_REQ_CODE) &&
        (pGSEM->Protocol == CCP_OVER_INDIVIDUAL_LINK))
    {
#ifdef CCP
        nmhGetPppCCPTxConfigOptionNum ((INT4) pGSEM->pIf->LinkInfo.IfIndex,
                                       1, &OptionType);
        if (OptionType == OPTION_MPPE)
        {
            pAuthPtr = pGSEM->pIf->pAuthPtr;
            if (pAuthPtr != NULL)
            {
                /* 
                 * In case of server, pAuthPtr->ServerProt will 
                 * have negotiated auth protocol value and 
                 * pAuthPtr->ClientProt will be zero. 
                 * In case of client it will be vice versa.
                 */
                if ((pAuthPtr->ServerProt != MSCHAP_PROTOCOL) &&
                    (pAuthPtr->ClientProt != MSCHAP_PROTOCOL))
                {
                    return FALSE;
                }
            }
        }
#endif
    }
    if (TxFlag == NORMAL_TRANSMISSION)
    {
        GSEMCleanUp (pGSEM);
        if ((pOutBuf =
             (t_MSG_DESC *) (VOID *) ALLOCATE_BUFFER (MAX_PKT_SIZE,
                                                      DATA_OFFSET)) != NULL)
        {
            pGSEM->RequestId = pGSEM->CurrentId++;
            if (Code == CONF_REQ_CODE)
            {
                PPP_TRC1 (CONTROL_PLANE, "INTERFACE :[%ld]\tOUT",
                          pGSEM->pIf->LinkInfo.IfIndex);
                PPPPrintProtocol (pGSEM->Protocol);
                PPP_TRC1 (CONTROL_PLANE, "[CONF REQ]\tId: [%d]",
                          pGSEM->RequestId);
                Length = (UINT2) (Length + GCPAddConfigInfo (pGSEM, pOutBuf));
            }
            MOVE_BACK_PTR (pOutBuf, CP_HDR_LEN);
            FILL_PKT_HEADER (pOutBuf, Code, pGSEM->RequestId, Length);
#ifdef L2TP_WANTED
            /* Sending First Sent and Last Sent Conf-Req packets to L2TP */
            if ((pGSEM->pIf->LinkInfo.IfID.IfType == ASYNC_IF_TYPE) &&
                (pGSEM->Protocol == LCP_PROTOCOL))
            {
                PPPLACSendConfReqPkt (pGSEM->pIf, pOutBuf, Code);
            }
#endif
        }
        else
        {
            UPDATE_OUT_COUNTER (pGSEM->pIf);
            return (FALSE);
        }
    }
    else
    {
        pOutBuf = pGSEM->pStoredPkt;
        Length = (UINT2) VALID_BYTES (pOutBuf);
        pGSEM->pStoredPkt = NULL;
    }

    Length = (UINT2) VALID_BYTES (pOutBuf);
    if (PPPDuplicateCruBuffer (pOutBuf, &(pGSEM->pStoredPkt), Length)
        == PPP_FAILURE)
    {
        PPP_TRC (BUFFER_TRC, "PPPDuplicateCruBuffer failed");
        return (FALSE);
    }
    if (pGSEM->pStoredPkt == NULL)
    {
        PPP_TRC (BUFFER_TRC, "PPPDuplicateCruBuffer pGSEM->pStoredPkt failed");
        return (FALSE);
    }

    /* Starts the restart timer */
    pGSEM->RestartTimer.Param1 = PTR_TO_U4 (pGSEM);
    PPPRestartTimer (&pGSEM->RestartTimer, RESTART_TIMER,
                     pGSEM->pIf->CurrentLinkConfigs.RestartTimeOutVal);

    PPP_DBG ("After restarting timer");
    pGSEM->Retransmits--;

    PPPLLITxPkt (pGSEM->pIf, pOutBuf, Length, pGSEM->Protocol);

    return (TRUE);
}

/*********************************************************************
*  Function Name :GSEMTimeOut 
*  Description   :
*            This function is called when there is a time out after transmission
*  of Configuration request.
*  Parameter(s)  :
*           pGSEM    -   points to the  tGSEM copy of the
*                                        calling  module
*            Type     -   TimerId
*  Return Value : VOID 
*********************************************************************/
VOID
GSEMTimeOut (VOID *pSem)
{

    tGSEM              *pGSEM;
    pGSEM = (tGSEM *) pSem;
    if (pGSEM->Retransmits == 0)
    {
        PPP_TRC (CONTROL_PLANE, "Retransmission over[ TO_MINUS ]");
        GSEMRun (pGSEM, TO_MINUS);
        if (pGSEM->Protocol == IPCP_PROTOCOL)
        {
            GSEMRestartIfNeeded (pGSEM);
        }
    }
    else
    {
        PPP_TRC (CONTROL_PLANE, "Retransmitting [TO_PLUS]...");
        GSEMRun (pGSEM, TO_PLUS);
    }

    return;
}

/*********************************************************************
*  Function Name :GSEMRestartIfNeeded
*  Description   :
*        This function is used to re-start the SEM , based on 
*  current state.
*  Parameter(s)  :
*           pGSEM    -   points to the  tGSEM  structure
*  Return Value : VOID 
*********************************************************************/
VOID
GSEMRestartIfNeeded (tGSEM * pGSEM)
{

    if ((pGSEM->CurrentState == OPENED) || (pGSEM->CurrentState == STOPPED)
        || (pGSEM->CurrentState == STOPPING))
    {
        GSEMRun (pGSEM, PPP_DOWN);
        GSEMRun (pGSEM, PPP_UP);
    }

    return;
}

/*********************************************************************
*   Function Name   :   GSEMDelete
*   Description     :
*           This function releases memory allocated to GSEM variables and
*   stops the Restart Timer also.
*   Parameter(s)    :
*           pGSEM   -   Points to the tGSEM structure.
*   Return Value    :   VOID
*********************************************************************/

VOID
GSEMDelete (tGSEM * pGSEM)
{
    GSEMCleanUp (pGSEM);
    if (pGSEM->pNegFlagsPerIf != NULL)
    {
        FREE_NEG_FLAGS (pGSEM->pNegFlagsPerIf);
        pGSEM->pNegFlagsPerIf = NULL;
    }
    pGSEM->pIf = NULL;
    return;
}

/*********************************************************************
*    Function Name    :    GSEMCleanUp
*    Description        :
*            This function releases the  stored buffer (if any) and stops
*    the Restart Timer. 
*    Parameter(s)    :
*            pGSEM    -    Points to the tGSEM structure.
*    Return Value    :    VOID 
*********************************************************************/
VOID
GSEMCleanUp (tGSEM * pGSEM)
{

    PPPStopTimer (&pGSEM->RestartTimer);

    PPP_DBG (" GSEM Timer Stopped");

    if (pGSEM->pStoredPkt != NULL)
    {
        RELEASE_BUFFER (pGSEM->pStoredPkt);
        pGSEM->pStoredPkt = NULL;
        PPP_DBG (" Buffer released");

    }
    return;
}

/*********************************************************************
*  Function Name : UTILSendTermAck
*  Description   :
*               This function constructs and transmits TERMINATE_ACK
*  packet.
*  Parameter(s)  :
*           pGSEM    -   points to the  tGSEM copy of the
*                                        calling  module
*  Return Value : 1 - Success
*                 0 - Fail
*********************************************************************/
UINT1
UTILSendTermAck (tGSEM * pGSEM)
{
    t_MSG_DESC         *pOutBuf;

    if ((pOutBuf =
         (t_MSG_DESC *) (VOID *) ALLOCATE_BUFFER (MAX_PKT_SIZE,
                                                  DATA_OFFSET)) != NULL)
    {
        /*  Copy the id so that it is same as the request */

        FILL_PKT_HEADER (pOutBuf, TERM_ACK_CODE, pGSEM->MiscParam.CharValue,
                         CP_HDR_LEN);
        PPPLLITxPkt (pGSEM->pIf, pOutBuf, CP_HDR_LEN, pGSEM->Protocol);
        return (TRUE);
    }
    return (FALSE);
}
