/********************************************************************
 * Copyright (C) Future Software Limited, 1997-98,2001
 *
 * $Id: pppgsem.c,v 1.10 2014/11/23 09:33:37 siva Exp $
 *
 * Description:This file contains the action procedures of 
 *             GSEM module.
 *
 *******************************************************************/

#include "gencom.h"
#include "gsemdefs.h"
#include "gcpdefs.h"
#include "lcpdefs.h"
#include "mpdefs.h"
#include "globexts.h"
#include "cfa.h"
#include "pppdpproto.h"

UINT1               AuthInitFlag;
tGSEM              *pRestartGSEM;
tGSEM              *pTermGSEM;

/**********************************************************************
*    Function Name    :    GSEMillegalState
*    Description        :
*                This procedure is called to process an illegal state transition.
*    It logs an error messages and returns back without any state transition. 
*    Parameter(s)    :
*            pGSEM    -    Points to the tGSEM copy of the calling  module.
*    Return Value    : 
*                Current State of the SEM.
*********************************************************************/
UINT1
GSEMillegalState (tGSEM * pGSEM)
{
    PPP_DBG ("llegal event occurred.");

    if (pGSEM->pOutParam != NULL)
    {
        RELEASE_BUFFER (pGSEM->pOutParam);
        pGSEM->pOutParam = NULL;
    }
    return (pGSEM->CurrentState);
}

/**********************************************************************
*    Function Name    :    GSEMReleaseBuffer
*    Description     :
*                This procedure is called to Release Buffer In Case there is
*                no Action for the Current State and the Received event
*    It logs an error messages and returns back without any state transition. 
*    Parameter(s)    :
*            pGSEM    -    Points to the tGSEM copy of the calling  module.
*    Return Value    : 
*                Current State of the SEM.
*********************************************************************/
UINT1
GSEMReleaseBuffer (tGSEM * pGSEM)
{
    PPP_DBG ("No Action Procedure For The Current State and Received Event.");

    if (pGSEM->pOutParam != NULL)
    {
        RELEASE_BUFFER (pGSEM->pOutParam);
        pGSEM->pOutParam = NULL;
    }
    return (pGSEM->CurrentState);
}

/*********************************************************************
*    Function Name    :    GSEMMoveToClosed
*    Description        :
*                        This function returns the SEM state as CLOSED. It 
*    is called when the state is STOPPED and the event that triggered the 
*    SEM is  CLOSE or when there is an UP event,in the INITIAL State.
*    Parameter(s)    :
*            pGSEM    -    Points to the tGSEM structure corresponding to the 
*                        calling  module.
*    Return Value    :    CLOSED
*********************************************************************/
UINT1
GSEMMoveToClosed (tGSEM * pGSEM)
{
    PPP_UNUSED (pGSEM);
    return (CLOSED);
}

/*********************************************************************
*    Function Name    :    GSEMSendConfReq
*    Description        :
*                This procedure is executed for any of the following 
*    state-event combinations :
*        UP event in the STARTING state
*        OPEN event in the CLOSED state
*        RCN event in the REQSENT state
*    It initializes the retransmits field of pGSEM to MaxConfigReqTransmits and 
*    sends a configuration request. It returns the SEM state as REQSENT.
*    Parameter(s)    :
*            pGSEM    -    Points to the tGSEM structure corresponding to the 
*                        calling  module.
*    Return Value    :    REQSENT
*********************************************************************/
UINT1
GSEMSendConfReq (tGSEM * pGSEM)
{
    /* If this link is a bundle member, then add it to the bundle in Data plane
     * just before sending the FIRST LCP Config request on this link */
    if ((pGSEM->pIf->MPInfo.MemberInfo.pBundlePtr != NULL) &&
        (pGSEM->pIf->u1RemovedLinkInDP == PPP_TRUE))
    {
#ifdef DPIF
        if (MPAddLinkToBundleToDP (pGSEM->pIf,
                                   pGSEM->pIf->MPInfo.MemberInfo.pBundlePtr)
            == PPP_FAILURE)
        {
            PPP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                     "AddLink to bundle failed in DP\n");
            return (pGSEM->CurrentState);
        }
#endif

        /* Set removed flag to FALSE now that the link
         * has been added to the bundle in DP */
        pGSEM->pIf->u1RemovedLinkInDP = PPP_FALSE;
    }

    pGSEM->Retransmits = pGSEM->pIf->CurrentLinkConfigs.MaxConfigReqTransmits;
    if (UTILSendReqPkt (pGSEM, NORMAL_TRANSMISSION, CONF_REQ_CODE) == TRUE)
    {
        return (REQSENT);
    }
    return (pGSEM->CurrentState);
}

/*********************************************************************
*    Function Name    :    GSEMMoveToInitial
*    Description        : 
*                This procedure returns the SEM state  as INITIAL.
*    It is called when there is a DOWN event in the CLOSED/CLOSING states.
*    Parameter(s)    :
*            pGSEM    -    Points to the tGSEM copy of the calling module.
*    Return Value    :    INITIAL
*********************************************************************/
UINT1
GSEMMoveToInitial (tGSEM * pGSEM)
{
    UINT4               u4IfIndex = 0;
    UINT1               u1Persistence = 0;
    UINT1               u1AdminStatus = 0;

    if ((pGSEM == NULL) || (pGSEM->pIf == NULL))
    {
        return (INITIAL);
    }
    u4IfIndex = pGSEM->pIf->LinkInfo.IfIndex;

    if ((CfaApiGetWanEntryPersist (u4IfIndex, &u1Persistence) == CFA_SUCCESS))
    {
        if (u1Persistence == CFA_PERS_DEMAND)
        {
            if (CfaApiGetIfAdminStatus
                (u4IfIndex, &u1AdminStatus) == CFA_SUCCESS)
            {
                CfaIfmHandleInterfaceStatusChange (u4IfIndex,
                                                   u1AdminStatus, CFA_IF_DOWN,
                                                   FALSE);
            }
        }
    }
    return (INITIAL);
}

/*********************************************************************
*    Function Name    :    GSEMReqLLToUp
*    Description        :
*                This procedure  is invoked if  there is 
*                -    a DOWN event in  the STOPPED state, 
*                -    an OPEN  event in the INITIAL state.
*    When called, it executes the function pointed by pStarting field of 
*    tGSEMCallbacks structure, which requests the lower layer to come UP.   
*    Parameter(s)    :
*            pGSEM    -    Points to the  tGSEM copy of the calling  module.
*    Return Value    :    STARTING
*********************************************************************/
UINT1
GSEMReqLLToUp (tGSEM * pGSEM)
{

    if (pGSEM->pCallbacks->pStarting != NULL)
    {
        (*pGSEM->pCallbacks->pStarting) (pGSEM);
    }
    return (STARTING);
}

/*********************************************************************
*    Function Name    :    GSEMMoveToStarting
*    Description        :
*                This function is called when there is a DOWN event and the 
*    current state is  either  REQSENT, ACKRECD, ACKSENT or STOPPING. It 
*    cancels time out values (if any) and  sets the SEM state to STARTING.
*    Parameter(s)    :
*            pGSEM    -    Points to the  tGSEM copy of the calling module.
*    Return Value    :    STARTING
*********************************************************************/
UINT1
GSEMMoveToStarting (tGSEM * pGSEM)
{

    PPP_UNUSED (pGSEM);
    /*  When a DOWN event is received due to processing of Terminate ACK,
     *  after the config -ack has been sent on a multilink . This issue arises and from here on 
     *  ppp enters into an illegal state if the call back is not called to move
     *  the link to starting state. Added the callback function - CAS - HDC - 41291, 
     *  RFC - 26345 */
    if (pGSEM->pIf->MPInfo.MemberInfo.pBundlePtr != NULL)
    {
        if (pGSEM->pCallbacks->pStarting != NULL)
        {
            (*pGSEM->pCallbacks->pStarting) (pGSEM);
        }
    }
    return (STARTING);
}

/*********************************************************************
*    Function Name    :    GSEMLeavingOpened
*    Description        :
*            This procedure is executed when the GSEM is in Opened state 
*    and there is a down event. It executes the function pointed by the 
*    pDown field  of pGSEM and  sets the next state to STARTING. 
*    If any one of the NCP module is invoking this call, then the function 
*    pointed to, by this field informs the higher layers  that the 
*    GSEM is leaving the Opened state.
*    Parameter(s)    :
*            pGSEM    -    Points to the  tGSEM copy of the calling module.
*    Return Value    :    STARTING
*********************************************************************/
UINT1
GSEMLeavingOpened (tGSEM * pGSEM)
{

    if (pGSEM->pCallbacks->pDown != NULL)
    {
        (*pGSEM->pCallbacks->pDown) (pGSEM);
    }
    pGSEM->NakLoops = 0;

    return (STARTING);
}

/*********************************************************************
*    Function Name    :    GSEMMoveToStopping
*    Description        :
*                This procedure is executed when there is an OPEN event in 
*    the CLOSING state. It sets the next state to STOPPING and returns back.
*    Parameter(s)    :
*            pGSEM    -    Points to the  tGSEM copy of the calling module.
*    Return Value    :    STOPPING
*********************************************************************/
UINT1
GSEMMoveToStopping (tGSEM * pGSEM)
{
    PPP_UNUSED (pGSEM);
    return (STOPPING);
}

/*********************************************************************
*    Function Name    :    GSEMRelLLMovToInitial
*    Description        :
*                This procedure is  executed when there is a CLOSE event 
*    in the  STARTING state. It indicates to the lower layer that the lower 
*    layer is not needed by executing the function pointed by the pFinished 
*    field of the pGSEM and returns to INITIAL State.
*    Parameter(s)    :
*            pGSEM    -    Points to the tGSEM copy of the calling module.
*    Return Value    :    INITIAL
*********************************************************************/
UINT1
GSEMRelLLMovToInitial (tGSEM * pGSEM)
{
    UINT4               u4IfIndex = 0;
    UINT1               u1Persistence = 0;
    UINT1               u1AdminStatus = 0;

    if (pGSEM == NULL) 
    {
        return (INITIAL);
    }
     if (pGSEM->pCallbacks->pFinished != NULL)
    {
       (*pGSEM->pCallbacks->pFinished) (pGSEM);
    }
    if (pGSEM->pIf == NULL)
    {
        return (INITIAL);
    }

    pGSEM->NakLoops = 0;
    u4IfIndex = pGSEM->pIf->LinkInfo.IfIndex;

    /* CFA LOCK is not required from PPP module since the lock was already taken
     * when configuring static and unnumbered ip from CFA module. */
    CFA_UNLOCK ();
    if ((CfaApiGetWanEntryPersist (u4IfIndex, &u1Persistence) == CFA_SUCCESS))
    {
        if (u1Persistence == CFA_PERS_DEMAND)
        {
            if (CfaApiGetIfAdminStatus
                (u4IfIndex, &u1AdminStatus) == CFA_SUCCESS)
            {
                CfaIfmHandleInterfaceStatusChange ((UINT2) u4IfIndex,
                                                   u1AdminStatus, CFA_IF_DOWN,
                                                   FALSE);
            }
        }
    }
    CFA_LOCK ();
    return (INITIAL);
}

/*********************************************************************
*    Function Name    :    GSEMMoveToClosing
*    Description        :
*            This procedure is executed when there is a CLOSE event in 
*    the STOPPING state. It just returns the SEM state as CLOSING.
*    Parameter(s)    :
*            pGSEM    -    Points to the  tGSEM copy of the calling module.
*    Return Value    :    CLOSING
*********************************************************************/
UINT1
GSEMMoveToClosing (tGSEM * pGSEM)
{
    PPP_UNUSED (pGSEM);
    return (CLOSING);
}

/*********************************************************************
*    Function Name    :    GSEMSendTermReq
*    Description        :
*                This procedure is executed when there is a CLOSE event 
*    in the REQSENT,ACKSENT or ACKRCVD state. It sets the retransmit counter
*    to maximum value and sends a TERMINATE REQUEST to the peer. 
*    It also returns the SEM state as CLOSING.
*    Parameter(s)    :
*            pGSEM    -    Points to the tGSEM copy of the calling module.
*    Return Value    :    CLOSING
*********************************************************************/
UINT1
GSEMSendTermReq (tGSEM * pGSEM)
{

    pGSEM->Retransmits = pGSEM->pIf->CurrentLinkConfigs.MaxTermtransmits;
    pGSEM->NakLoops = 0;
    if (UTILSendReqPkt (pGSEM, NORMAL_TRANSMISSION, TERM_REQ_CODE) == TRUE)
    {
        return (CLOSING);
    }
    return (pGSEM->CurrentState);
}

/*********************************************************************
*    Function Name    :    GSEMLeaveOpenSendTermReq
*    Description        :
*            This procedure is  executed when there is a CLOSE event in the
*    Opened state. It indicates to the upper layer that the GSEM is leaving 
*    the Opened state by executing the function pointed by the pDown field of 
*    the tGSEMCallbacks structure. 
*    Parameter(s)    :
*            pGSEM    -    Points to the tGSEM copy of the calling  module.
*    Return Value    : 
*            CLOSING if the TERM_REQ packet was sent successfully / 
*            CurrentState otherwise.    
*********************************************************************/
UINT1
GSEMLeaveOpenSendTermReq (tGSEM * pGSEM)
{

    if (pGSEM->pCallbacks->pDown != NULL)
    {
        (*pGSEM->pCallbacks->pDown) (pGSEM);
    }

    pGSEM->NakLoops = 0;
    pGSEM->Retransmits = pGSEM->pIf->CurrentLinkConfigs.MaxTermtransmits;

    if (UTILSendReqPkt (pGSEM, NORMAL_TRANSMISSION, TERM_REQ_CODE) == TRUE)
    {
        return (CLOSING);
    }
    return (pGSEM->CurrentState);
}

/*********************************************************************
*    Function Name    :    GSEMResendTermReq
*    Description        :
*                This procedure is executed when there is a TO+ event in the 
*    CLOSING/STOPPING state. It sends a TERMINATE REQUEST to the peer. 
*    Parameter(s)    :
*            pGSEM    -    Points to the tGSEM copy of the calling module.
*    Return Value    :    CLOSING
*********************************************************************/
UINT1
GSEMResendTermReq (tGSEM * pGSEM)
{

    UTILSendReqPkt (pGSEM, RETRANSMISSION, TERM_REQ_CODE);
    return (pGSEM->CurrentState);
}

/*********************************************************************
*    Function Name    :    GSEMResendConfReq
*    Description        :
*            This procedure is executed when there is a TO+ event in the 
*    REQSENT/ACKSENT state. It re-transmits CONFIGRURATION REQUEST and 
*    decrements *  the Retransmit field of pGSEM.The state remains unchanged.
*    Parameter(s)    :
*            pGSEM    -    Points to the  tGSEM copy of the calling module.
*    Return Value    :    CurrentState field of the tGSEM structure.
*********************************************************************/
UINT1
GSEMResendConfReq (tGSEM * pGSEM)
{

    UTILSendReqPkt (pGSEM, RETRANSMISSION, CONF_REQ_CODE);
    return (pGSEM->CurrentState);
}

/*********************************************************************
*    Function Name    :    GSEMResendConfReqMovToReqSent
*    Description        :
*            This procedure is executed when there is a TO+ event in the 
*    ACKRCVD state. It retransmits CONFIGRURATION REQUEST and updates the 
*    Retransmit field of pGSEM.
*    Parameter(s)    :
*            pGSEM    -    Points to the  tGSEM copy of the calling module.
*    Return Value    :    REQSENT on successful transmission of CONF_REQ /
*                        CurrentState otherwise.
*********************************************************************/
UINT1
GSEMResendConfReqMovToReqSent (tGSEM * pGSEM)
{

    if (UTILSendReqPkt (pGSEM, RETRANSMISSION, CONF_REQ_CODE) == TRUE)
    {
        return (REQSENT);
    }
    return (pGSEM->CurrentState);
}

/*********************************************************************
*    Function Name    :    GSEMRelLLMovToClosed
*    Description        :
*            This function is executed for any of the following state event 
*    combinations :
*        TO-/RTA            event in the CLOSING state.         
*        RXJ-            event in the CLOSED/CLOSING state.
*    It indicates the lower layer that the link is no more needed by executing 
*    the function pointed by the finished field of the tGSEMCallbacks structure  
*    and returns to the CLOSED state.
*    Parameter(s)    :
*            pGSEM    -    Points to the  tGSEM copy of the calling  module.
*    Return Value    :    CLOSED
*********************************************************************/
UINT1
GSEMRelLLMovToClosed (tGSEM * pGSEM)
{

    if (pGSEM->pCallbacks->pFinished != NULL)
    {
        (*pGSEM->pCallbacks->pFinished) (pGSEM);
    }
    pGSEM->NakLoops = 0;

    return (CLOSED);
}

/*********************************************************************
*    Function Name    :    GSEMRelLLMovToStopped
*    Description        :
*            This function is executed for any of the following state and 
*    event combinations :
*        TO-    event in the  STOPPING state. 
*        RXJ-  event in the  STOPPED/STOPPING/REQSENT/ACKRCVD/ACKSENT states
*        RTA   event in the STOPPING state.
*    It indicates the lower layer that the link is no more needed by executing
*    the function pointed by the finished field of the tGSEMCallbacks structure  
*    and returns to the STOPPED state.
*    Parameter(s)    :
*            pGSEM    -    Points to the  tGSEM copy of the calling  module.
*    Return Value    :    STOPPED
*********************************************************************/
UINT1
GSEMRelLLMovToStopped (tGSEM * pGSEM)
{

    if (pGSEM->pCallbacks->pFinished != NULL)
    {
        (*pGSEM->pCallbacks->pFinished) (pGSEM);
    }
    pGSEM->NakLoops = 0;
    return (STOPPED);
}

/*********************************************************************
*    Function Name    :    GSEMRelLLMovToStoppedPassive
*    Description        :
*                This function is executed when there is a TO- event in any 
*    of the REQSENT, ACKSENT or ACKRCVD state. If PASSIVE option is not enabled, 
*    it moves to the STOPPED state after executing the function pointed by the 
*    finished field of the tGSEMCallbacks. If the PASSIVE option is enabled, 
*    then it remains in the same state.
*    Parameter(s)    :
*            pGSEM    -    Points to the  tGSEM copy of the calling  module
*    Return Value    : 
*            STOPPED, if the PASSIVE option is not enabled / Current state of 
*            the GSEM if the PASSIVE option is enabled.
*********************************************************************/
UINT1
GSEMRelLLMovToStoppedPassive (tGSEM * pGSEM)
{

    pGSEM->NakLoops = 0;
    /* Check if the PASSIVE option is enabled */
    if (pGSEM->pIf->CurrentLinkConfigs.PassiveOption == NOT_SET)
    {
        if (pGSEM->pCallbacks->pFinished != NULL)
        {
            (*pGSEM->pCallbacks->pFinished) (pGSEM);
        }
        return (STOPPED);
    }
    PPP_TRC (CONTROL_PLANE, " Waiting for the other end to start first...");
    return (STOPPED);
}

/*********************************************************************
*    Function Name    :    GSEMSendTermAck
*    Description        :
*            This function is executed  for any of the following state,
*    event combination :  
*            RCR+/RCR-/RCA/RCN/RTR  event in the CLOSED state
*            RCA/RCN/RTR   event in the STOPPED state
*            RTR  event in the CLOSING /STOPPING/REQSENT  state  
*    It sends a terminate acknowledge packet and remains in the same state.
*    Parameter(s)    :
*            pGSEM    -    Points to the  tGSEM copy of the calling  module.
*    Return Value    :    Current State field of GSEM.
*********************************************************************/
UINT1
GSEMSendTermAck (tGSEM * pGSEM)
{

    if (pGSEM->pOutParam != NULL)
    {
        RELEASE_BUFFER (pGSEM->pOutParam);
        pGSEM->pOutParam = NULL;
    }

    if (UTILSendTermAck (pGSEM) == FALSE)
    {
            PPP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                     "PPP Send Term Ack Failed \n");
    }   

    return (pGSEM->CurrentState);
}

/*********************************************************************
*    Function Name    :    GSEMSendConfReqConfAck
*    Description        :
*                This function is executed when there is a RCR+ event in the 
*    STOPPED state. It sets the restart counter to maximum value and sends the 
*    CONFIGRATION REQUEST to the Peer. It also sends CONFIGURATION ACK for the 
*    received request and moves to ACKSENT state.
*    Parameter(s)    :
*            pGSEM    -    Points to the  tGSEM copy of the calling  module
*    Return Value    :    ACKSENT if Config-Request has been sent successfully /
*                        Current State of the SEM otherwise.    
*********************************************************************/
UINT1
GSEMSendConfReqConfAck (tGSEM * pGSEM)
{

    pGSEM->Retransmits = pGSEM->pIf->CurrentLinkConfigs.MaxConfigReqTransmits;

    /* Received onfig request in STOPPED state. 
     * If this link is a bundle member, it would have been removed
     * from the bundle in data plane when in STOPPED state.
     * So first add it to the bundle in Data plane just before
     * sending the LCP Config ack on this link */
    if ((pGSEM->pIf->MPInfo.MemberInfo.pBundlePtr != NULL) &&
        (pGSEM->pIf->u1RemovedLinkInDP == PPP_TRUE))
    {
#ifdef DPIF
        if (MPAddLinkToBundleToDP (pGSEM->pIf,
                                   pGSEM->pIf->MPInfo.MemberInfo.pBundlePtr)
            == PPP_FAILURE)
        {
            PPP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                     "AddLink to bundle failed in DP\n");
            return (pGSEM->CurrentState);
        }
#endif

        /* Set removed flag to FALSE now that the link
         * has been added to the bundle in DP */
        pGSEM->pIf->u1RemovedLinkInDP = PPP_FALSE;
    }

    if (UTILSendReqPkt (pGSEM, NORMAL_TRANSMISSION, CONF_REQ_CODE) == TRUE)
    {
        PPPLLITxPkt (pGSEM->pIf, pGSEM->pOutParam,
                     (UINT2) (VALID_BYTES (pGSEM->pOutParam)), pGSEM->Protocol);
        return (ACKSENT);
    }

    return (pGSEM->CurrentState);
}

/*********************************************************************
*    Function Name    :    GSEMSendConfAck
*    Description        :
*                This function is executed when there is a RCR+ event in the 
*    REQSENT/ACKSENT state. It  sends back the configuration acknowledge packet 
*    and moves to  the ACKSENT  state.
*    Parameter(s)    :
*            pGSEM    -    Points to the  tGSEM copy of the calling  module.
*    Return Value    :    ACKSENT
*********************************************************************/
UINT1
GSEMSendConfAck (tGSEM * pGSEM)
{

    PPP_TRC1 (CONTROL_PLANE, "INTERFACE :[%ld]\tOUT",
              pGSEM->pIf->LinkInfo.IfIndex);
    PPPPrintProtocol (pGSEM->Protocol);
    PPP_TRC (CONTROL_PLANE, "[CONF ACK]");
    PPPLLITxPkt (pGSEM->pIf, pGSEM->pOutParam,
                 (UINT2) (VALID_BYTES (pGSEM->pOutParam)), pGSEM->Protocol);

    return (ACKSENT);
}

/*********************************************************************
*    Function Name    :    GSEMSendConfAckEnterOpened
*    Description        :
*            This function is executed when there is a RCR+ event in the 
*    ACKRCVD state. It  sends  back the acknowledge packet for the received 
*    configuration request and moves to the Opened state. The function pointed 
*    by the pUp field of the pGSEM is executed here.
*        When the calling module is LCP, this procedure indicates about
*    the reaching of Opened state to the higher layer, NCP,LQM and 
*    authentication modules. If any of the NCP module invoked the GSEM, then 
*    this function just indicates the higher layer that the datagaram of the 
*    corresponding NCP protocol is now acceptable.
*    Parameter(s)    :
*            pGSEM    -    Points to the tGSEM copy of the calling module.
*    Return Value    :    OPENED
*********************************************************************/
UINT1
GSEMSendConfAckEnterOpened (tGSEM * pGSEM)
{

    PPPLLITxPkt (pGSEM->pIf, pGSEM->pOutParam,
                 (UINT2) (VALID_BYTES (pGSEM->pOutParam)), pGSEM->Protocol);

    if (pGSEM->pCallbacks->pUp != NULL)
    {
        (*pGSEM->pCallbacks->pUp) (pGSEM);
    }

    return (OPENED);
}

/*********************************************************************
*    Function Name    :    GSEMLeaveOpenedSendConfReqAck
*    Description        :
*            This function is executed when there is a RCR+ event in the 
*    Opened state.It  indicates the higher layer that the GSEM is leaving the 
*    Opened state by executing the function pointed by the pDown field of 
*    pGSEM and sends the CONFIGURATION REQUEST to the Peer. It then issues 
*    CONFIGURATION ACK for the received request and moves to ACKSENT state.
*    Parameter(s)    :
*            pGSEM    -    Points to the tGSEM copy of the calling module.
*    Return Value    :    ACKSENT if Config-Request was sent successfully /
*                        Current State of the SEM otherwise. 
*********************************************************************/
UINT1
GSEMLeaveOpenedSendConfReqAck (tGSEM * pGSEM)
{

    AuthInitFlag = TRUE;
    if (pGSEM->pCallbacks->pDown != NULL)
    {
        (*pGSEM->pCallbacks->pDown) (pGSEM);
    }
    pGSEM->NakLoops = 0;

    if (UTILSendReqPkt (pGSEM, NORMAL_TRANSMISSION, CONF_REQ_CODE) == TRUE)
    {
        PPPLLITxPkt (pGSEM->pIf, pGSEM->pOutParam,
                     (UINT2) (VALID_BYTES (pGSEM->pOutParam)), pGSEM->Protocol);
        return (ACKSENT);
    }

    return (pGSEM->CurrentState);
}

/*********************************************************************
*    Function Name    :    GSEMSendConfReqConfNak
*    Description        :
*            This function is executed when there is a RCR- event in the 
*    STOPPED state. It  sets the restart counter to maximum value and sends the 
*    CONFIGRATION REQUEST to the Peer. It also sends the CONFIGURATION NAK 
*    for the received request  and  moves to the REQSENT state.
*    Parameter(s)    :
*            pGSEM    -    Points to the  tGSEM copy of the calling  module.
*    Return Value    :    REQSENT
*********************************************************************/
UINT1
GSEMSendConfReqConfNak (tGSEM * pGSEM)
{

    pGSEM->Retransmits = pGSEM->pIf->CurrentLinkConfigs.MaxConfigReqTransmits;
    if (UTILSendReqPkt (pGSEM, NORMAL_TRANSMISSION, CONF_REQ_CODE) == TRUE)
    {
        PPPLLITxPkt (pGSEM->pIf, pGSEM->pOutParam,
                     (UINT2) (VALID_BYTES (pGSEM->pOutParam)), pGSEM->Protocol);
        return (REQSENT);
    }
    return (pGSEM->CurrentState);
}

/*********************************************************************
*    Function Name    :    GSEMSendConfNak
*    Description        :
*            This function is executed when there is a RCR- event in the 
*    ACKRCVD or REQ_SENT state. It  sends  back the CONFIGURATION NAK and 
*    remains in the same state.
*    Parameter(s)    :
*            pGSEM    -    Points to the tGSEM copy of the calling  module.
*    Return Value    :    State field of pGSEM
*********************************************************************/
UINT1
GSEMSendConfNak (tGSEM * pGSEM)
{

    PPP_TRC1 (CONTROL_PLANE, "INTERFACE :[%ld]\tOUT",
              pGSEM->pIf->LinkInfo.IfIndex);
    PPPPrintProtocol (pGSEM->Protocol);
    PPP_TRC (CONTROL_PLANE, "[CONF NAK]");
    PPPLLITxPkt (pGSEM->pIf, pGSEM->pOutParam,
                 (UINT2) (VALID_BYTES (pGSEM->pOutParam)), pGSEM->Protocol);
    return (pGSEM->CurrentState);
}

/*********************************************************************
*    Function Name    :    GSEMSendConfNakMovToReqSent 
*    Description        :
*        This function is executed when there is a RCR- event in the 
*    ACKSENT state. It  sends  the negative acknowledge for the received 
*    configuration request and goes to the REQSENT state.
*    Parameter(s)    :
*            pGSEM    -    Points to the tGSEM copy of the calling module.
*    Return Value    :    REQSENT
*********************************************************************/
UINT1
GSEMSendConfNakMovToReqSent (tGSEM * pGSEM)
{

    PPPLLITxPkt (pGSEM->pIf, pGSEM->pOutParam,
                 (UINT2) (VALID_BYTES (pGSEM->pOutParam)), pGSEM->Protocol);
    return (REQSENT);
}

/*********************************************************************
*    Function Name    :    GSEMLeaveOpenedSendConfReqNak
*    Description        :
*            This function is executed when there is a RCR- event in the 
*    Opened state. It indicates the higher layer that the GSEM is leaving 
*    the Opened state and sends the CONFIGURATION REQUEST to the Peer.  
*    It then issues CONFIGURATION NAK for the received request and moves to 
*    REQSENT  state.
*    Parameter(s)    :
*            pGSEM    -    Points to the  tGSEM copy of the calling  module.
*    Return Value    :    REQSENT
*********************************************************************/
UINT1
GSEMLeaveOpenedSendConfReqNak (tGSEM * pGSEM)
{

    if (pGSEM->pCallbacks->pDown != NULL)
    {
        (*pGSEM->pCallbacks->pDown) (pGSEM);
    }
    pGSEM->NakLoops = 0;

    if (UTILSendReqPkt (pGSEM, NORMAL_TRANSMISSION, CONF_REQ_CODE) == TRUE)
    {
        PPPLLITxPkt (pGSEM->pIf, pGSEM->pOutParam,
                     (UINT2) (VALID_BYTES (pGSEM->pOutParam)), pGSEM->Protocol);
        return (REQSENT);
    }
    return (pGSEM->CurrentState);
}

/*********************************************************************
*    Function Name    :    GSEMMovToAckRcvd
*    Description        :
*                This function is executed when there is a RCA event in the 
*    REQSENT state. It sets the restart counter to a maximum value and  moves 
*    to the ACKRCVD state.
*    Parameter(s)    :
*            pGSEM    -    Points to the  tGSEM copy of the calling  module.
*  Return Value : ACKRCVD
*********************************************************************/
UINT1
GSEMMovToAckRcvd (tGSEM * pGSEM)
{

    pGSEM->Retransmits = pGSEM->pIf->CurrentLinkConfigs.MaxConfigReqTransmits;
    return (ACKRCVD);
}

/*********************************************************************
*  Function Name : GSEMSendConfReqCross
*  Description   :
*                        This function is executed when there is a RCA/RCN event in the 
*  ACKRCVD state. It  sends  back the CONFIGURATION REQ packet by calling the 
*  function pointed by pAddConfigInfo field of pGSEM and goes to the 
*  REQSENT state.It  also logs an error message saying that the correct ack is 
*  already received.
*  Parameter(s)  :
*            pGSEM    -   points to the  tGSEM copy of the 
*                                         calling  module
*  Return Value : REQSENT
*********************************************************************/
UINT1
GSEMSendConfReqCross (tGSEM * pGSEM)
{

    if (UTILSendReqPkt (pGSEM, NORMAL_TRANSMISSION, CONF_REQ_CODE) == TRUE)
    {
        return (REQSENT);
    }
    return (pGSEM->CurrentState);
}

/*********************************************************************
*  Function Name : GSEMEnterOpened
*  Description   :
*                        This function is executed when there is a RCA event in the  
*    ACKSENT state. It sets the restart counter to a maximum value and  indicates 
*  the upper layer that it has reached the Opened state by executing the 
*  function pointed by the pFinished field of the pGSEM. Then it sets 
*  the state to OPENED and returns.
*  Parameter(s)  :
*            pGSEM    -   points to the  tGSEM copy of the 
*                                         calling  module
*  Return Value : OPENED
*********************************************************************/
UINT1
GSEMEnterOpened (tGSEM * pGSEM)
{

    pGSEM->Retransmits = pGSEM->pIf->CurrentLinkConfigs.MaxConfigReqTransmits;
    if (pGSEM->pCallbacks->pUp != NULL)
    {
        (*pGSEM->pCallbacks->pUp) (pGSEM);
    }
    return (OPENED);
}

/*********************************************************************
*  Function Name : GSEMLeaveOpenedSendConfReqCross
*  Description   :
*                        This function is executed when there is a RCA/RCN event in the 
*    Opened state. It  indicates the higher layer that the GSEM is leaving the 
*  Opened state by executing the function pointed by the pDown field of 
*    pGSEM and sends the CONFIGURATION REQUEST to the Peer.  It then  
*  moves to REQSENT  state.
*  Parameter(s)  :
*            pGSEM    -   points to the  tGSEM copy of the 
*                                         calling  module
*  Return Value : REQSENT
*********************************************************************/
UINT1
GSEMLeaveOpenedSendConfReqCross (tGSEM * pGSEM)
{

    if (pGSEM->pCallbacks->pDown != NULL)
    {
        (*pGSEM->pCallbacks->pDown) (pGSEM);
    }
    pGSEM->NakLoops = 0;

    if (UTILSendReqPkt (pGSEM, NORMAL_TRANSMISSION, CONF_REQ_CODE) == TRUE)
    {
        return (REQSENT);
    }
    return (pGSEM->CurrentState);
}

/*********************************************************************
*  Function Name : GSEMSendConfReqInAckSent
*  Description   :
*    This function is executed when there is an RCN event in the 
*  the ACKSENT    state. It sets the restart counter to a maximum value and  
*    sends  back the modified CONFIGURATION REQUEST to the peer. 
*  Parameter(s)  :
*            pGSEM    -   points to the  tGSEM copy of the 
*                                         calling  module
*  Return Value :ACKSENT 
*********************************************************************/
UINT1
GSEMSendConfReqInAckSent (tGSEM * pGSEM)
{

    pGSEM->Retransmits = pGSEM->pIf->CurrentLinkConfigs.MaxConfigReqTransmits;
    if (UTILSendReqPkt (pGSEM, NORMAL_TRANSMISSION, CONF_REQ_CODE) ==  TRUE)
    {
        PPP_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                 "PPP Conf Req In Ack Failed \n");
    }

    return (pGSEM->CurrentState);
}

/*********************************************************************
*  Function Name : GSEMSendTermAckMovToReqSent
*  Description   :
*                        This function is executed when there is an RTR event in  the 
*    ACKRCVD/ACKSENT state. It sends back the TERMINATE ACK  and moves to the 
*  REQSENT state.
*  Parameter(s)  :
*            pGSEM    -   points to the  tGSEM copy of the 
*                                         calling  module
*  Return Value : REQSENT 
*********************************************************************/
UINT1
GSEMSendTermAckMovToReqSent (tGSEM * pGSEM)
{

    if (UTILSendTermAck (pGSEM) == TRUE)
    {
        return (REQSENT);
    }
    return (pGSEM->CurrentState);
}

/*********************************************************************
*  Function Name : GSEMLeaveOpenedSendTermAck
*  Description   :
*                This function is executed when there is an RTR event in the 
*    Opened state. It  indicates the higher layer that the GSEM is leaving the 
*    Opened state by executing the function pointed by the pDown field of the 
*  pGSEM . It also resets the restart counter value to zero and sends 
*  back the TERMINATE ACK to the peer. 
*  Parameter(s)  :
*            pGSEM    -   points to the  tGSEM copy of the 
*                                         calling  module
*  Return Value : STOPPING
*********************************************************************/
UINT1
GSEMLeaveOpenedSendTermAck (tGSEM * pGSEM)
{

    if (pGSEM->pCallbacks->pDown != NULL)
    {
        (*pGSEM->pCallbacks->pDown) (pGSEM);
    }
    pGSEM->NakLoops = 0;
    pGSEM->Retransmits = 0;

    pGSEM->RestartTimer.Param1 = PTR_TO_U4 (pGSEM);
    PPPRestartTimer (&pGSEM->RestartTimer, RESTART_TIMER,
                     pGSEM->pIf->CurrentLinkConfigs.RestartTimeOutVal);

    if (UTILSendTermAck (pGSEM) == TRUE)
    {
        return (STOPPING);
    }

    return (pGSEM->CurrentState);
}

/*********************************************************************
*  Function Name : GSEMMovToReqSent
*  Description   :
*                        This function is executed when there is an RTA event in the 
*    ACKRCVD state. It just sets the next state to  REQSENT state.
*  Parameter(s)  :
*            pGSEM    -   points to the  tGSEM copy of the 
*                                         calling  module
*  Return Value : REQSENT 
*********************************************************************/
UINT1
GSEMMovToReqSent (tGSEM * pGSEM)
{
    PPP_UNUSED (pGSEM);
    return (REQSENT);
}

/*********************************************************************
*  Function Name : GSEMLeaveOpenedSendConfReq
*  Description   :
*                        This function is executed when there is an RTA event in 
*    Opened state. It  indicates the higher layer that the GSEM is leaving the 
*    Opened state by executing the function pointed by pDown field of pGSEM
*  and sends the CONFIGRATION REQUEST to the peer. 
*  Parameter(s)  :
*            pGSEM    -   points to the  tGSEM copy of the 
*                                         calling  module
*  Return Value : REQSENT
*********************************************************************/
UINT1
GSEMLeaveOpenedSendConfReq (tGSEM * pGSEM)
{

    if (pGSEM->pCallbacks->pDown != NULL)
    {
        (*pGSEM->pCallbacks->pDown) (pGSEM);
    }
    pGSEM->NakLoops = 0;

    if (UTILSendReqPkt (pGSEM, NORMAL_TRANSMISSION, CONF_REQ_CODE) == TRUE)
    {
        return (REQSENT);
    }
    return (pGSEM->CurrentState);
}

/*********************************************************************
*  Function Name : GSEMSendCodeRej
*  Description   :
*                This function is executed when there is an RUC event and the 
*    current state is CLOSED/STOPPED/STOPPING/CLOSING/REQSENT/ACKRCVD/ACKSENT/
*    OPENED. It sends back the code reject packet to the peer and remains in the 
*  same state.
*  Parameter(s)  :
*            pGSEM    -   points to the  tGSEM copy of the calling  module
*  Return Value : value of State field of pGSEM
*********************************************************************/
UINT1
GSEMSendCodeRej (tGSEM * pGSEM)
{

    PPPSendRejPkt (pGSEM->pIf, pGSEM->pInParam, pGSEM->MiscParam.ShortValue,
                   pGSEM->Protocol, CODE_REJ_CODE);

    return (pGSEM->CurrentState);
}

/*********************************************************************
*  Function Name : GSEMLeaveOpenedMovToStopped
*  Description   :
*    This function is executed when there is an RXJ- event and the 
*  current state is OPENED. It  indicates the higher layer that the GSEM is 
*  leaving the Opened state by executing the function pointed by the pDown field*  of the pGSEM and sets the restart counter to it maximum value. It 
*  then sends the TERMINATE REQUEST to the peer and moves to STOPPING
*  Parameter(s)  :
*            pGSEM    -   points to the  tGSEM copy of the 
*                                         calling  module
*  Return Value : STOPPING
*********************************************************************/
UINT1
GSEMLeaveOpenedMovToStopped (tGSEM * pGSEM)
{

    if (pGSEM->pCallbacks->pDown != NULL)
    {
        (*pGSEM->pCallbacks->pDown) (pGSEM);
    }
    pGSEM->NakLoops = 0;
    pGSEM->Retransmits = pGSEM->pIf->CurrentLinkConfigs.MaxTermtransmits;

    if (UTILSendReqPkt (pGSEM, NORMAL_TRANSMISSION, TERM_REQ_CODE) == TRUE)
    {
        return (STOPPING);
    }

    return (pGSEM->CurrentState);
}

/*********************************************************************
*    Function Name    :    GSEMProcessRxrPkt
*    Description        :
*            This function is executed when there is an RXR event and the 
*    current state is OPENED. It just sends back the ECHO REPLY to the peer 
*    and remains in the same state.
*    Parameter(s)    :
*            pGSEM    -    Points to the  tGSEM copy of the calling  module.
*    Return Value    :    OPENED
*********************************************************************/
UINT1
GSEMProcessRxrPkt (tGSEM * pGSEM)
{
    UINT4               Magic;
    tPPPIf             *pIf;
    UINT1               CICode, Offset, *pMsg, Id;
    UINT2               Length, Index, EchoLen;
    UINT4               u4CurTicks;

    Offset = 0;
    pMsg = NULL;
    pIf = pGSEM->pIf;

    Offset =
        (UINT1) (Offset + pIf->LinkInfo.RxAddrCtrlLen +
                 pIf->LinkInfo.RxProtLen);

    GET1BYTE (pGSEM->pInParam, Offset, CICode);
    Offset++;

    GET1BYTE (pGSEM->pInParam, Offset, Id);

    /*  Skip the Id field */
    Offset = (UINT1) (Offset + 1);

    GET2BYTE (pGSEM->pInParam, Offset, Length);

    /* Move to the magic field */
    Offset = (UINT1) (Offset + 2);

    GET4BYTE (pGSEM->pInParam, Offset, Magic);
    Offset = (UINT1) (Offset + 4);

    EchoLen = (UINT2) (Length - CP_HDR_LEN - 4);

    if (EchoLen != 0)
    {
        if ((pMsg = ALLOC_STR ((size_t) (EchoLen + 1))) == NULL)
        {
            PPP_TRC (ALL_FAILURE, "Error in memory allocation.....");
            return (OPENED);
        }
        GETSTR (pGSEM->pInParam, pMsg, Offset, EchoLen);
        pMsg[EchoLen] = '\0';
    }

    if (Magic == pIf->LcpOptionsAckedByPeer.MagicNumber)
    {
        if (Magic != 0)
        {
            DISCARD_PKT (pIf, " Pkt indicates that the link may be in loop");
            pIf->EchoInfo.TestResult = LOOP_BACK;
            return (OPENED);
        }
    }
    else
    {
        if (Magic != pIf->LcpOptionsAckedByLocal.MagicNumber)
        {
            DISCARD_PKT (pIf, "pkt is discarded : Magic number Problem");
            pIf->EchoInfo.TestResult = NO_ECHO;
            PPP_DBG1 ("Echo Result : %d", pIf->EchoInfo.TestResult);
            return (OPENED);
        }
    }

    switch (CICode)
    {
        case ECHO_REQ_CODE:
            LCPSendEchoResponse (pIf, Id, pMsg, EchoLen);
            break;

        case ECHO_REPLY_CODE:
            for (Index = 0; Index < EchoLen; Index++)
            {
                if (pMsg[Index] != pIf->EchoInfo.EchoChar)
                {
                    pIf->EchoInfo.TestResult = NO_ECHO;
                    PPP_DBG1 ("Echo Result received:%d",
                              pIf->EchoInfo.TestResult);
                    break;
                }
            }
            if (Index == EchoLen)
            {
                PPPStopTimer (&pIf->EchoInfo.EchoTimer);
                pIf->EchoInfo.TestResult = ECHO_RCVD;
                pIf->EchoInfo.EchoTransmits = 0;
            }
            /* 
               Calculate the transit delay only if the link is a member of an 
               active bundle. 
             */
            if (pIf->MPInfo.MemberInfo.PartOfBundle == PPP_YES)
            {
                if (OsixGetSysTime (&u4CurTicks) == OSIX_FAILURE)
                {
                    PPP_TRC (ALL_FAILURE, "Failure in  OsixGetSysTime()");
                }
                else
                {
                    pIf->MPInfo.MemberInfo.LastTransitDelay =
                        pIf->MPInfo.MemberInfo.TransitDelay;
                    pIf->MPInfo.MemberInfo.TransitDelay =
                        (UINT4) (u4CurTicks -
                                 pIf->MPInfo.MemberInfo.LastTransitTicks) / 2;
                }
                /* if the  Td is xtremely low , make it as 1 */
                if (pIf->MPInfo.MemberInfo.TransitDelay == 0)
                {
                    pIf->MPInfo.MemberInfo.TransitDelay++;
                }
                PPP_DBG3 (" Td calculated as : %ld = (%ld - %ld)/2",
                          pIf->MPInfo.MemberInfo.TransitDelay, u4CurTicks,
                          pIf->MPInfo.MemberInfo.LastTransitTicks);
            }
            break;

        case DISCARD_REQ_CODE:
            PPP_TRC (CONTROL_PLANE, "Received Discard-Request");
            break;
        case IDENTIFICATION_CODE:
            PPP_TRC (CONTROL_PLANE, "Received Identification packet from peer");
            break;
        default:
            PPP_DBG ("No code like this");
    }

    if (pMsg != NULL)
    {
        FREE_STR (pMsg);
        pMsg = NULL;
    }
    return (OPENED);
}

/*********************************************************************
*   Function Name   :   GSEMProcessRxrPktInOtherStates
*   Description     :
*       This function is called when there is an RXR event in a state 
*       other than the OPENED state. This function processes Identification 
*       packets and discards the Echo-Req and Discard-Req packets.
*   Parameter(s)    :
*           pGSEM   -   Points to the  tGSEM copy of the calling  module.
*   Return Value    :   Current state of the SEM
*********************************************************************/
UINT1
GSEMProcessRxrPktInOtherStates (tGSEM * pGSEM)
{
    UINT4               Magic;
    tPPPIf             *pIf;
    UINT1               PktCode, Offset, *pMsg, Id;
    UINT2               Length, MesgLength;

    Offset = 0;
    pMsg = NULL;
    pIf = pGSEM->pIf;

    Offset =
        (UINT1) (Offset + pIf->LinkInfo.RxAddrCtrlLen +
                 pIf->LinkInfo.RxProtLen);

    GET1BYTE (pGSEM->pInParam, Offset, PktCode);
    Offset++;

    GET1BYTE (pGSEM->pInParam, Offset, Id);
    Offset = (UINT1) (Offset + 1);

    GET2BYTE (pGSEM->pInParam, Offset, Length);
    Offset = (UINT1) (Offset + 2);

    /* Don't validate the Magic Number as negotiation is under progress. */
    GET4BYTE (pGSEM->pInParam, Offset, Magic);
    Offset = (UINT1) (Offset + 4);

    switch (PktCode)
    {
        case ECHO_REQ_CODE:
            pIf->LLHCounters.InErrors++;
            PPP_TRC (EVENT_TRC, "Received Echo-Req in illegal state");
            break;
        case ECHO_REPLY_CODE:
            pIf->LLHCounters.InErrors++;
            PPP_TRC (EVENT_TRC, "Received Echo-Reply in illegal state");
            break;
        case DISCARD_REQ_CODE:
            pIf->LLHCounters.InErrors++;
            PPP_TRC (EVENT_TRC, "Received Discard-Req in illegal state");
            break;
        case IDENTIFICATION_CODE:
            PPP_TRC (EVENT_TRC, "Received Identification packet");
            MesgLength = (UINT2) (Length - CP_HDR_LEN - 4);
            if ((MesgLength != 0) && ((pMsg = ALLOC_STR (MesgLength)) != NULL))
            {
                GETSTR (pGSEM->pInParam, pMsg, Offset, MesgLength);
                pMsg[MesgLength -1] = '\0';
            }
            break;
        default:
            PPP_TRC (EVENT_TRC, "Illegal code in packet");
    }

    if (pMsg != NULL)
    {
        FREE_STR (pMsg);
        pMsg = NULL;
    }

    PPP_UNUSED (Id);
    PPP_UNUSED (Magic);

    return (pGSEM->CurrentState);
}

/*********************************************************************
*    Function Name    :    GSEMTxData
*    Description        :
*            This function is executed when there is an TXD event and the 
*    current state is OPENED. It calls the callback function to process the 
*    data appropriately. 
*    Parameter(s)    :
*            pGSEM    -    Points to the  tGSEM copy of the calling  module.
*    Return Value    :    Current State. 
*********************************************************************/
UINT1
GSEMTxData (tGSEM * pGSEM)
{

    if (pGSEM->pCallbacks->pTxDataHandler != NULL)
    {
        (*pGSEM->pCallbacks->pTxDataHandler) (pGSEM);
    }
    return (pGSEM->CurrentState);
}

/*********************************************************************
*    Function Name    :    GSEMRxData
*    Description        :
*            This function is executed when there is an RXD event and the
*    current state is OPENED. It calls the callback function to process 
*    the data appropriately. 
*    Parameter(s)    :
*            pGSEM    -    Points to the tGSEM copy of the calling module.
*    Return Value    :    Current State of the SEM.
*********************************************************************/
UINT1
GSEMRxData (tGSEM * pGSEM)
{

    /* Check if the offset has been updated already. */
    PPP_TRC2 (EVENT_TRC, " ACLen : %d ProtLen : %d",
              pGSEM->pIf->LinkInfo.RxAddrCtrlLen,
              pGSEM->pIf->LinkInfo.RxProtLen);

    /* Update the offset */
    MOVE_OFFSET (pGSEM->pInParam,
                 (UINT4) (pGSEM->pIf->LinkInfo.RxAddrCtrlLen +
                          pGSEM->pIf->LinkInfo.RxProtLen));
    pGSEM->pIf->LinkInfo.RxAddrCtrlLen = 0;
    pGSEM->pIf->LinkInfo.RxProtLen = 0;

    if (pGSEM->pCallbacks->pRxDataHandler != NULL)
    {
        (*pGSEM->pCallbacks->pRxDataHandler) (pGSEM);
    }
    return (pGSEM->CurrentState);
}

/*********************************************************************
*    Function Name    :    NCPRxData
*    Description        :
*        This function is called by the GSEM call back function when a 
*    data packet meant for any of the higher layers is received. This function
*    forwards the packets to the HL after moving the offset appropriately.
*    Parameter(s)    :
*            pGSEM    -    Points to the tGSEM copy of the calling module.
*    Return Value    :    OPENED
*********************************************************************/
VOID
NCPRxData (tGSEM * pGSEM)
{
    UINT2               Length;

    NoOfPktsSentToHL++;
    Length = (UINT2) VALID_BYTES (pGSEM->pInParam);
    pGSEM->pIf->LLHCounters.InGoodOctets += Length;

    /* Forward it to the HLI */
    PPPHLIRxPkt (pGSEM->pIf, pGSEM->pInParam, Length,
                 pGSEM->MiscParam.ShortValue);

    ReleaseFlag = PPP_NO;
    return;
}

/*********************************************************************
*    Function Name    :    NCPTxData
*    Description        :
*        This function is called by the GSEM call back function when the 
*    higher layers send a data packet to the GSEM for transmission. 
*    This function sends the data to MP module if MP has been negotiated.
*    If not it sends the packet to the LLI. 
*    Parameter(s)    :
*            pGSEM    -    Points to the tGSEM copy of the calling module.
*    Return Value    :    OPENED
*********************************************************************/
VOID
NCPTxData (tGSEM * pGSEM)
{
    UINT2               Protocol;
    UINT2               Length;
    tPPPIf             *pIf;

#ifndef NPAPI_WANTED
    INT1                PFCEnableFlag;
#endif

    pIf = pGSEM->pIf;
    Length = (UINT2) VALID_BYTES (pGSEM->pOutParam);

    PPP_TRC1 (CONTROL_PLANE, "INTERFACE :[%ld]\tOUT", pIf->LinkInfo.IfIndex);

    if (pIf->BundleFlag == MEMBER_LINK)
    {
        PPP_DBG ("Member link : Directly sent..");
        if (Length > pIf->LcpOptionsAckedByLocal.MRU)
        {
            DISCARD_PKT (pIf, "Invalid Length");
        }
        else
        {

            Protocol = (UINT2) pGSEM->MiscParam.ShortValue;
            PPPPrintProtocol (Protocol);

            NoOfPktsRcvdFromHL++;

            PPPLLITxPkt (pIf, pGSEM->pOutParam, Length, Protocol);
        }
    }
    else
    {
        PPP_DBG ("Bundle link : Sent to MP Module..");
        if (((pIf->
              MPInfo.BundleInfo.MPFlagsPerIf[MRRU_IDX].
              FlagMask & ACKED_BY_LOCAL_MASK) == NOT_SET)
            &&
            ((pIf->
              MPInfo.BundleInfo.MPFlagsPerIf[SEQ_IDX].
              FlagMask & ACKED_BY_LOCAL_MASK) == NOT_SET))
        {
            DISCARD_PKT (pIf, "Bundle not negotiated...");
        }
        else
        {
            if (Length > pIf->MPInfo.BundleInfo.BundleOptAckedByLocal.MRRU)
            {
                DISCARD_PKT (pIf, "Invalid Length");
            }
            else
            {
                NoOfPktsRcvdFromHL++;
#ifdef NPAPI_WANTED
                /* If an external processor is used for multilink
                 * fragmentation and reassembly then -
                 *   - Buffer passed to NP should contain the
                 *     entire PPP packet.
                 *   - Tx interface index passed should be that of the
                 *     MP interface directly instead of any lower layer
                 *     physical interface index.
                 */

                Protocol = (UINT2) pGSEM->MiscParam.ShortValue;
                PPPPrintProtocol (Protocol);
                PPPLLITxPkt (pIf, pGSEM->pOutParam, Length, Protocol);
#else
                PFCEnableFlag = PPP_NO;
                if ((pGSEM->MiscParam.ShortValue & DATA_PKT_MASK) == 0)
                {
                    PFCEnableFlag = PPP_YES;
                }
                PPPLLIPutPID (pGSEM->pOutParam, (UINT1) PFCEnableFlag,
                              (UINT2) pGSEM->MiscParam.ShortValue);
                if ((pGSEM->MiscParam.ShortValue == BAP_PROTOCOL)
                    ||
                    (PPPLLIEncapPID
                     (pIf, &pGSEM->pOutParam, (UINT1) PFCEnableFlag) == OK))
                {
                    pIf->LcpGSEM.MiscParam.CharValue =
                        MPCheckForInSequence (pGSEM->MiscParam.ShortValue);
#ifdef MP
                    MPSplitAndAllocate (pIf, pGSEM->pOutParam,
                                        (UINT2) (VALID_BYTES
                                                 (pGSEM->pOutParam)),
                                        pGSEM->MiscParam.ShortValue);
#endif
                }
#endif
            }
        }
    }
    return;
}

UINT1
GSEMProcessResetRequest (tGSEM * pGSEM)
{
    if (pGSEM->pCallbacks->pHandleRRR != NULL)
    {
        (*pGSEM->pCallbacks->pHandleRRR) (pGSEM);
    }
    else
    {
        DISCARD_PKT (pGSEM->pIf, "Rxd RRR with invalid Protocol");
    }
    return (pGSEM->CurrentState);
}

UINT1
GSEMProcessResetAck (tGSEM * pGSEM)
{
    if (pGSEM->pCallbacks->pHandleRRA != NULL)
    {
        (*pGSEM->pCallbacks->pHandleRRA) (pGSEM);
    }
    else
    {
        DISCARD_PKT (pGSEM->pIf, "Rxd RRA with invalid Protocol");
    }
    return (pGSEM->CurrentState);
}
