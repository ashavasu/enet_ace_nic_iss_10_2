/********************************************************************
 * Copyright (C) Future Software Limited, 1997-98,2001
 *
 * $Id: pppgcp.c,v 1.7 2014/11/23 09:33:37 siva Exp $
 *
 * Description:This file contains the routines of GCP module.
 *
 *******************************************************************/

#include "gencom.h"
#include "gcpdefs.h"
#include "gsemdefs.h"
#include "lcpdefs.h"
#include "genexts.h"
#include "lcpexts.h"
#include "lcpprot.h"

#include "mpdefs.h"
#include "pppipcp.h"
#include "ipexts.h"

#include "pppipv6cp.h"
#include "ipv6exts.h"
#include "pppmuxcp.h"

#include "pppmplscp.h"
#include "mplsexts.h"
#include "pppipxcp.h"
#include "ipxexts.h"
#include "pppeccmn.h"
#include "pppccp.h"
#include "pppeccmn.h"
#include "pppecp.h"
#include "ecpexts.h"
#include "pppbcp.h"
#include "bcpexts.h"
#ifdef BAP
#include "pppbacp.h"
#include "bacpexts.h"
#endif
#include "ccpproto.h"
#include "globexts.h"
#include "bcpproto.h"
/* #ifdef L2TP_WANTED */
#include "pppl2tp.h"
#include "l2tp.h"
/* #endif */

#define        WRONG_LEN        -3
#define        UNKNOWN_CODE        -2
#define        NOT_FOUND            -1
#define        OPT_LEN_OFFSET        1

UINT1               gu1WrongLenOptIndex;

tOptHandlerFuns     BooleanHandlingFuns = {
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
};

static INT1         (*GenEvent[]) (tGSEM *, t_MSG_DESC *, UINT2, UINT1) =
{
NULL,
        GCPRecdConfigReq,
        GCPRecdConfigAck,
        GCPRecdConfigNak,
        GCPRecdConfigRej, GCPRecdTermReq, GCPRecdTermAck, GCPRecdCodeRej};

tMiscPktHandler     ProtSpecificEvents[] = {
#ifdef CCP
    {0x80fd, 14, 0, CCPRecdResetReq},
    {0x80fb, 14, 0, CCPRecdResetReq},
    {0x80fd, 15, 0, CCPRecdResetAck},
    {0x80fb, 15, 0, CCPRecdResetAck},
#endif
    {0x8053, 14, 0, ECPRecdResetReq},
    {0x8055, 14, 0, ECPRecdResetReq},
    {0x8053, 15, 0, ECPRecdResetAck},
    {0x8055, 15, 0, ECPRecdResetAck},
    {0xc021, 8, 0, LCPRecdProtRej},
    {0xc021, 9, 0, LCPRecdRxrReqPkt},
    {0xc021, 10, 0, LCPRecdEchoReply},
    {0xc021, 11, 0, LCPRecdRxrReqPkt},
    {0xc021, 12, 0, LCPRecdRxrReqPkt}
};

tProtSEM            ProtSEMTable[] = {
    {LCP_PROTOCOL, 0, LCPGetSEMPtr, LCPCopyOptions},
    {IPCP_PROTOCOL, 0, IPCPGetSEMPtr, IPCPCopyOptions},
#ifdef IPv6CP
    {IPV6CP_PROTOCOL, 0, IP6CPGetSEMPtr, IP6CPCopyOptions},
#endif
    {MUXCP_PROTOCOL, 0, MuxCPGetSEMPtr, MuxCPCopyOptions},
    {MPLSCP_PROTOCOL, 0, MPLSCPGetSEMPtr, MPLSCPCopyOptions},
    {IPXCP_PROTOCOL, 0, IPXCPGetSEMPtr, IPXCPCopyOptions},
#ifdef BCP
    {BCP_PROTOCOL, 0, BCPGetSEMPtr, BCPCopyOptions},
#endif
#ifdef BACP
    {BACP_PROTOCOL, 0, BACPGetSEMPtr, BACPCopyOptions},
#endif
    {ECP_OVER_MEMBER_LINK, 0, ECPGetSEMPtr, ECPCopyOptions},
    {ECP_OVER_INDIVIDUAL_LINK, 0, ECPGetSEMPtr, ECPCopyOptions},
#ifdef CCP
    {CCP_OVER_MEMBER_LINK, 0, CCPGetSEMPtr, CCPCopyOptions},
    {CCP_OVER_INDIVIDUAL_LINK, 0, CCPGetSEMPtr, CCPCopyOptions},
#endif
    {IP_DATAGRAM, 0, IPCPGetSEMPtr, NULL},
    {IP_COMP_TCP_DATA, 0, IPCPGetSEMPtr, NULL},
    {IP_UNCOMP_TCP_DATA, 0, IPCPGetSEMPtr, NULL},
#ifdef IPv6CP
/* IPv6CP Changes -- start */
    {IPv6_DATAGRAM, 0, IP6CPGetSEMPtr, NULL},
/* IPv6CP Changes -- End */
#endif
    {MPLS_UNICAST_DATAGRAM, 0, MPLSCPGetSEMPtr, NULL},
    {MPLS_MULTICAST_DATAGRAM, 0, MPLSCPGetSEMPtr, NULL},
    {IPX_DATAGRAM, 0, IPXCPGetSEMPtr, NULL},
    {TELEBIT_COMP_PROTO, 0, IPXCPGetSEMPtr, NULL},
    {SHIVA_COMP_PROTO, 0, IPXCPGetSEMPtr, NULL},
#ifdef BCP
    {BCP_SPAN_IEEE_DATA, 0, BCPSpanIeeeGetSEMPtr, NULL},
    {BCP_IBM_SRC_RT_DATA, 0, BCPIbmSrcRtGetSEMPtr, NULL},
    {BCP_DEC_LAN_DATA, 0, BCPDecLanGetSEMPtr, NULL},
    {BCP_BRIDGED_LAN_DATA, 0, BCPGetSEMPtr, NULL},
#endif
    {ECP_DATA_OVER_MEMBER_LINK, 0, ECPGetSEMPtr, NULL},
    {ECP_DATA_OVER_INDIVIDUAL_LINK, 0, ECPGetSEMPtr, NULL},
#ifdef BAP
    {BAP_PROTOCOL, 0, BACPGetSEMPtr, NULL},
#endif
#ifdef CCP
    {CCP_DATA_OVER_MEMBER_LINK, 0, CCPGetSEMPtr, NULL},
    {CCP_DATA_OVER_INDIVIDUAL_LINK, 0, CCPGetSEMPtr, NULL}
#endif
};

UINT1              *pRecdOptFlags;
UINT1              *pOptStrVal;
INT2                ProtIndex;

INT2                OutDataPktIndex;
INT2                DataIndex;

UINT1               OutReleaseFlag;

/*********************************************************************
*    Function Name    :    GCPInput
*      Description        :
*    Function called by LLI when it receives a packet meant for any of the CPs.
*      Depending on the packet code, this fn. will call the appropriate packet 
*    handler. Then it will invoke the GSEM with the event returned by the event 
*    handler. 
*    Parameter(s)    :
*            pGSEM    -    Pointer to the tGSEM structure of the control protocol 
*                        corresponding to the incoming packet.
*            pInPkt    -    Points to the incoming packet.
*            Length    -    Length of the incoming packet.
*    Return Value    :    VOID 
*********************************************************************/
VOID
GCPInput (tGSEM * pGSEM, t_MSG_DESC * pInPkt, UINT2 Length)
{
    UINT1               Code =0;
    UINT1               Id =0;
    INT2                Index, Event;
    UINT2               Len;
    t_MSG_DESC         *pL2TPForwardBuf;

    pL2TPForwardBuf = NULL;
    /* Extract and validate the control protocol header */
    if (UtilExtractAndValidateCPHdr
        (pGSEM->pIf, pInPkt, Length, &Code, &Id, &Len) == DISCARD)
    {
        return;
    }

    pRecdOptFlags = NULL;
    /* Allocate an array of flags for use by the other GCP functions */
    if ((pRecdOptFlags = ALLOC_STR (pGSEM->MaxOptTypes)) == NULL)
    {
        PRINT_MEM_ALLOC_FAILURE;
        DISCARD_PKT (pGSEM->pIf, "GCPInput: Discarding on mem. alloc error.");
        return;
    }

    /*  Initialize params that may be used by the other GCP functions */
    pGSEM->pInParam = NULL;
    pGSEM->pOutParam = NULL;
    pGSEM->MiscParam.CharValue = Id;
    pOptStrVal = NULL;
    pCopyPtr = NULL;

    if (Code < MIN_GEN_CODE)
    {
        Event = RUC;
    }
    else
    {
        if (Code <= MAX_GEN_CODE)
        {
            /* Check for identifier match in case of a response packet */
            if (((Code == CONF_ACK_CODE) || (Code == CONF_NAK_CODE)
                 || (Code == CONF_REJ_CODE)) && ((Id != pGSEM->RequestId)
                                                 || (pGSEM->pStoredPkt ==
                                                     NULL)))
            {
                PPP_TRC (CONTROL_PLANE,
                         "Invalid Id in response Pkt or Pkt Rxd in stopped state.");
                PPP_TRC2 (CONTROL_PLANE, " Ids are : %d, %d", Id,
                          pGSEM->RequestId);
                Event = DISCARD;
            }
            else
            {
#ifdef L2TP_WANTED
                if (PPPIsL2TPTunnelModeLAC (pGSEM->pIf) == TRUE)
                {
                    if (Code == CONF_REQ_CODE)
                    {
                        if ((pL2TPForwardBuf =
                             (t_MSG_DESC *) ALLOCATE_BUFFER (Length -
                                                             CP_HDR_LEN,
                                                             0)) != NULL)
                        {
                            CRU_BUF_COPY_MSGS (pL2TPForwardBuf, pInPkt, 0,
                                               (pGSEM->pIf->LinkInfo.
                                                RxAddrCtrlLen +
                                                pGSEM->pIf->LinkInfo.RxProtLen +
                                                CP_HDR_LEN),
                                               Length - CP_HDR_LEN);
                        }
                        else
                        {
                        }
                    }
                }
#endif

                /*Calling Gen Event Handler */
                Event = (*GenEvent[Code]) (pGSEM, pInPkt, Len, Id);

#ifdef L2TP_WANTED
                if (PPPIsL2TPTunnelModeLAC (pGSEM->pIf) == TRUE)
                {
                    if ((Code == CONF_REQ_CODE) && (pL2TPForwardBuf != NULL))
                    {
                        if (Event != DISCARD)
                        {
                            PPPIndicateConfReqPktToLAC (pGSEM->pIf,
                                                        pL2TPForwardBuf,
                                                        Len - CP_HDR_LEN,
                                                        L2TP_RCVD_LCP_CONF_REQ);
                        }
                        else
                        {
                            if (pL2TPForwardBuf != NULL)
                            {
                                RELEASE_BUFFER (pL2TPForwardBuf);
                            }
                        }
                    }
                }
#endif
            }
        }
        else
        {
            PPP_TRC (CONTROL_PLANE, "Misc Code Rxd.");
            if ((Index = GCPMatchProtNCode (pGSEM->Protocol, Code)) == -1)
            {
                PPP_TRC (CONTROL_PLANE,
                         "Misc Unknown code Rxd.(Does not match with any protocol specific packets.");
                Event = RUC;
            }
            else
            {
                if (Index >=
                    (INT2) (sizeof (ProtSpecificEvents) /
                            sizeof (tMiscPktHandler)))
                {
                    return;
                }
                /*Calling ProtSpecific special packet handler */
                Event =
                    (*ProtSpecificEvents[Index].pPktHandler) (pGSEM, pInPkt,
                                                              Len, Id);

            }
        }
    }

    /* Free all GCP allocated variables which are no longer needed. */
    if (pRecdOptFlags != NULL)
    {
        FREE_STR (pRecdOptFlags);
        pRecdOptFlags = NULL;
    }
    if (pOptStrVal != NULL)
    {
        FREE_STR (pOptStrVal);
        pOptStrVal = NULL;
    }

    /* Update statistics counter if event is RUC */
    if (Event == RUC)
    {
        pGSEM->pIf->LLHCounters.InUnknownProtos++;
        pGSEM->pInParam = pInPkt;
        pGSEM->MiscParam.ShortValue = Length;
    }

    if (Event != DISCARD)
    {
        PPP_DBG1 ("Generating Event %d to GSEM module.", Event);
        PPP_TRC (CONTROL_PLANE, PPP_TRC_LINE_SEPERATOR);
        GSEMRun (pGSEM, (UINT1) (Event));
        if (pGSEM->pIf != NULL)
        {
            pGSEM->pIf->LLHCounters.InGoodOctets =
                (UINT4) (pGSEM->pIf->LLHCounters.InGoodOctets + Length +
                         pGSEM->pIf->LinkInfo.RxAddrCtrlLen +
                         pGSEM->pIf->LinkInfo.RxProtLen);
        }
    }
    else
    {
        DISCARD_PKT (pGSEM->pIf,
                     "GCP Input: Invalid Packet silently discarded");
    }
    pGSEM->pOutParam = NULL;
    PPP_UNUSED (pL2TPForwardBuf);
    return;
}

/*********************************************************************
*    Function Name    :    GCPRecdConfigReq 
*    Description        :
*        This function is invoked to process a CONF_REQ packet and
*    prepare the appropriate response.
*    Parameter(s)    :
*        pGSEM        -    Pointer to the GSEM structure corresponding to the
*                        packet.
*        pInPkt        -    Points to the incoming packet. 
*        Length        -    Length of the packet (option portion alone).
*        Identifier    -    Identifier received along with the packet.
*    Return Value    :    Event to be generated to the SEM  
*********************************************************************/
INT1
GCPRecdConfigReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt, UINT2 Length,
                  UINT1 Identifier)
{
    INT1                RespCode;

    INT2                Index;
    UINT2               OutPktLen, RetCode;

    PPP_TRC2 (CONTROL_PLANE, "[CONF REQ]\tId: [%d]\tLen :[%d]", Identifier,
              Length);
    /* Allocate buffer for constructing the response */
    if ((pGSEM->pOutParam =
         (t_MSG_DESC *) (VOID *) ALLOCATE_BUFFER (MAX_PKT_SIZE,
                                                  PPP_INFO_OFFSET)) == NULL)
    {
        PPP_TRC (ALL_FAILURE, "Unable to allocate buffer..");
        return (DISCARD);
    }

    /* Make a copy of the AckedByLocal variables. These will be required if 
       packet is found to be invalid during further processing.
     */
    if (((*ProtSEMTable[ProtIndex].pCPCopyOptions)
         (pGSEM, NOT_SET, CONF_REQ_CODE)) == DISCARD)
    {
        PRINT_MEM_ALLOC_FAILURE;
        RELEASE_BUFFER (pGSEM->pOutParam);
        pGSEM->pOutParam = NULL;
        return (DISCARD);
    }

    /* Determine and construct the response to the request */
    if ((RespCode = GCPGetReqResp (pGSEM, pInPkt, Length)) == DISCARD)
    {
        RELEASE_BUFFER (pGSEM->pOutParam);
        pGSEM->pOutParam = NULL;

        /* Copy back the value(s) of the AckedByLocal variables
         * from the temporary variable(s) and free the temporary 
         * variable(s). */
        (*ProtSEMTable[ProtIndex].pCPCopyOptions) (pGSEM, SET, CONF_REQ_CODE);
        return (DISCARD);
    }
    PPP_DBG1 ("RespCode from GCPGetReqResp: %d", RespCode);

    /* Free the temporary variable(s) allocated by first call 
     * to CopyOptions.  They are no longer needed since the 
     * packet is valid.  */
    (*ProtSEMTable[ProtIndex].pCPCopyOptions) (pGSEM, FREE_ALL, CONF_REQ_CODE);

    if (RespCode == CONF_ACK_CODE)
    {
        /* Acking request - No seperate response constructed.
         * Incoming buffer is resent with only the packet type
         * modified.  */
        RELEASE_BUFFER (pGSEM->pOutParam);
        pGSEM->pOutParam = NULL;
        MOVE_OFFSET (pInPkt,
                     (UINT4) (pGSEM->pIf->LinkInfo.RxAddrCtrlLen +
                              pGSEM->pIf->LinkInfo.RxProtLen));
        ASSIGN1BYTE (pInPkt, CODE_OFFSET, CONF_ACK_CODE);
        /*ReleaseFlag tells LLI not to release the incoming buffer
         * as it has now become the outgoing buffer */
        ReleaseFlag = PPP_NO;
        pGSEM->pOutParam = pInPkt;
        NoOfPktsAcked++;

        /* SET the AckedByLocal flags for all the incoming options
         * ( Unset for options not present in incoming packet ).  */
        for (Index = 0; Index < pGSEM->MaxOptTypes; Index++)
        {
            if (pRecdOptFlags[Index] == SET)
            {
                pGSEM->pNegFlagsPerIf[Index].FlagMask |= ACKED_BY_LOCAL_SET;
            }
            else
            {
                pGSEM->pNegFlagsPerIf[Index].FlagMask &= ACKED_BY_LOCAL_NOT_SET;
            }
        }

        return (RCR_PLUS);
    }
    /* The following ( == 0 ) should never occur (Just in case) - Shenoy */
    if ((OutPktLen = (UINT2) VALID_BYTES (pGSEM->pOutParam)) == 0)
    {
        OutPktLen = CP_HDR_LEN;
    }

    RetCode = RCR_MINUS;
    if (RespCode == CONF_NAK_CODE)
    {
        pGSEM->NakLoops++;
        if (pGSEM->NakLoops > pGSEM->pIf->CurrentLinkConfigs.MaxNakLoops)
        {
            if ((RespCode =
                 GCPHandleMaxNakLoops (pGSEM, pInPkt, Length,
                                       &OutPktLen)) == CONF_ACK_CODE)
            {
                PPP_TRC (CONTROL_PLANE, " Reached MaxNakLoops. Acking. ");
                RetCode = RCR_PLUS;
            }
            PPP_TRC (CONTROL_PLANE, " Reached MaxNakLoops. Sending Reject. ");
            PPP_TRC1 (CONTROL_PLANE, "INTERFACE :[%ld]\tOUT",
                      pGSEM->pIf->LinkInfo.IfIndex);
            PPPPrintProtocol (pGSEM->Protocol);
            PPP_TRC (CONTROL_PLANE, "[CONF REJ]");
        }
    }

    FILL_PKT_HEADER (pGSEM->pOutParam, RespCode, Identifier, OutPktLen);
    return ((INT1) RetCode);
}

/*********************************************************************
*    Function Name    :    GCPGetReqResp 
*    Description        :
*        This function determines and constructs the response to be sent when
*    a request packet is received.
*    Parameter(s)    :
*    pGSEM            -    Pointer to the GSEM structure corresponding to the
*                        packet.
*    pInPkt            -    Points to the incoming packet buffer
*    Length            -    Length of the packet (option portion alone)
*    Return Value    : 
*                    Response event.  
*********************************************************************/
INT1
GCPGetReqResp (tGSEM * pGSEM, t_MSG_DESC * pInPkt, UINT2 Length)
{
    /* Makefile changes - unused variables */
    UINT1               RejectFlag, OptType, OptLen;
    tOptVal             OptVal;
    UINT2               InPktOffset, OutPktOffset;
    INT2                OptIndex;
    INT1                RespCode, RetCode = ACK;
    INT2                Index;
    UINT1               InCorrectLen = NOT_SET;

    /* Initializations */
    RespCode = CONF_ACK_CODE;
    OutPktOffset = CP_HDR_LEN;
    InPktOffset =
        (UINT2) (pGSEM->pIf->LinkInfo.RxProtLen +
                 pGSEM->pIf->LinkInfo.RxAddrCtrlLen + CP_HDR_LEN);

    AuthInitFlag = FALSE;

    while (Length != 0)
    {

        RetCode = ACK;
        RejectFlag = NOT_SET;

        /* Extract and validate the next option */
        if ((OptIndex =
             GCPExtractAndValidateOption (pGSEM, pInPkt, &OptType, &OptLen,
                                          &OptVal, InPktOffset,
                                          Length)) == DISCARD)
        {
            PPP_TRC (CONTROL_PLANE, "Invalid option value in Pkt.");
            return (DISCARD);
        }
        PPP_DBG1
            ("GCPExtractAndValidateOption returned code: %d to GCPGetReqResp",
             OptIndex);
        if (OptIndex == UNKNOWN_CODE)
        {
            RejectFlag = SET;
        }
        else
        {

            if (OptIndex == WRONG_LEN)
            {
                PPP_DBG
                    ("Wrong Length Received for RANGE Option!! NAKing with Correct Length.");
                OptIndex = gu1WrongLenOptIndex;
                InCorrectLen = SET;
            }

            PPP_DBG1 ("OptIndex is :..%d", OptIndex);

            /* Check if option has already occurred in this packet */
            if (pRecdOptFlags[OptIndex] == SET)
            {

                /* Check if option is allowed to repeat */
                if (pGSEM->pGenOptInfo[OptIndex].RepeatAllowFlag == NOT_SET)
                {
                    PPP_TRC (CONTROL_PLANE,
                             " Option occurred 2nd time in Rx. Pkt. - Not allowed to repeat. Discarding packet. ");
                    return (DISCARD);
                }
            }

            pRecdOptFlags[OptIndex] = SET;

            /* Check if option is allowed for negotiation */
            if ((pGSEM->pNegFlagsPerIf[OptIndex].
                 FlagMask & ALLOWED_FOR_PEER_MASK) == NOT_SET)
            {
                PPP_TRC (ALL_FAILURE, " This option not allowed for peer. ");
                RejectFlag = SET;
            }
        }

        /* Check if option is to be rejected */
        if (RejectFlag == SET)
        {

            /* Check if a previous option has been put into the reject buffer */
            if (RespCode != CONF_REJ_CODE)
            {
                OutPktOffset = CP_HDR_LEN;
                RespCode = CONF_REJ_CODE;
            }

            /*  Copy the option along with its sub-options into the reject buffer. */
            CRU_BUF_COPY_MSGS (pGSEM->pOutParam, pInPkt, OutPktOffset,
                               InPktOffset, OptLen);
            OutPktOffset = (UINT2) (OutPktOffset + OptLen);
        }

        if (RespCode != CONF_REJ_CODE)
        {
            if (InCorrectLen == SET)
            {
                if (pGSEM->pGenOptInfo[OptIndex].pOptHandlerFuns->
                    pHandleConfReq != NULL)
                {
                    if ((RetCode =
                         (*pGSEM->pGenOptInfo[OptIndex].pOptHandlerFuns->
                          pHandleConfReq) (pGSEM, pInPkt, OptVal,
                                           (UINT2) (OutPktOffset + OPT_HDR_LEN),
                                           NOT_SET)) == DISCARD)
                        return DISCARD;
                }
                else
                    RetCode = (INT1) (OptLen - OPT_HDR_LEN);
            }
            else
            {

                /*  Check if the Option is a boolean option which has to be acked. */
                if (pGSEM->pGenOptInfo[OptIndex].pOptHandlerFuns->
                    pHandleConfReq != NULL)
                {
                    PPP_DBG
                        (" Not Boolean. Calling Option Specific Req.Handler");

                    /* Call the option specific request handler */
                    if ((RetCode =
                         (*pGSEM->pGenOptInfo[OptIndex].pOptHandlerFuns->
                          pHandleConfReq) (pGSEM, pInPkt, OptVal,
                                           (UINT2) (OutPktOffset + OPT_HDR_LEN),
                                           SET)) == DISCARD)
                    {
                        return (DISCARD);
                    }
                }
            }                    /* InCorrectLen */
            PPP_DBG1 ("Handler returned %d", RetCode);
            if ((InCorrectLen == SET) || (RetCode != ACK))
            {

                /* Naking non-boolean option - fill the option header */
                ASSIGN1BYTE (pGSEM->pOutParam, OutPktOffset, OptType);
                ASSIGN1BYTE (pGSEM->pOutParam,
                             (OutPktOffset + OPT_LEN_OFFSET),
                             (UINT1) (RetCode + OPT_HDR_LEN));
                OutPktOffset = (UINT2) (OutPktOffset + RetCode + OPT_HDR_LEN);
                RespCode = CONF_NAK_CODE;
            }
        }
        Length = (UINT2) (Length - OptLen);
        InPktOffset = (UINT2) (InPktOffset + OptLen);
    }

    /* Check for ForcedToRequest flag setting for all options */
    if (RespCode != CONF_REJ_CODE)
    {
        for (Index = 0; Index < pGSEM->MaxOptTypes; Index++)
        {
            if ((pRecdOptFlags[Index] == NOT_SET)
                && (pGSEM->pNegFlagsPerIf[Index].
                    FlagMask & FORCED_TO_REQUEST_MASK))
            {
                RetCode = 0;
                PPP_TRC1 (CONTROL_PLANE,
                          "Enforcing Option on peer; Index is : %d", Index);
                if (pGSEM->pGenOptInfo[Index].Category != BOOLEAN_CATEGORY)
                {
                    RetCode =
                        (*pGSEM->pGenOptInfo[Index].pOptHandlerFuns->
                         pHandleConfReq) (pGSEM, pInPkt, OptVal,
                                          (UINT2) (OutPktOffset + OPT_HDR_LEN),
                                          NOT_SET);
                }
                ASSIGN1BYTE (pGSEM->pOutParam, OutPktOffset,
                             pGSEM->pGenOptInfo[Index].OptType);
                ASSIGN1BYTE (pGSEM->pOutParam,
                             (OutPktOffset + OPT_LEN_OFFSET),
                             (UINT1) (RetCode + OPT_HDR_LEN));
                OutPktOffset = (UINT2) (OutPktOffset + RetCode + OPT_HDR_LEN);
                RespCode = CONF_NAK_CODE;
            }
        }
    }

    /* Check for an overwritten buffer which may have extra bytes at the end. */
    if (OutPktOffset < VALID_BYTES (pGSEM->pOutParam))
    {
        DELETE_BYTES_AT_END (pGSEM->pOutParam,
                             (VALID_BYTES (pGSEM->pOutParam) - OutPktOffset));
    }

    return (RespCode);
}

/*********************************************************************
*    Function Name    :    GCPRecdConfigNak
*    Description        :
*            This function is invoked to process the CONF_NAK packet and
*    prepare appropriate response.
*    Parameter(s)    :
*            pGSEM    -    Pointer to the GSEM structure corresponding to the
*                        packet.
*            pInPkt    -    Points to the incoming packet buffer.
*            Length    -    Length of the packet (option portion alone)
*        Identifier    -    Identifier received along with the packet
*    Return Value    :    Event to the GSEM  
*********************************************************************/
INT1
GCPRecdConfigNak (tGSEM * pGSEM, t_MSG_DESC * pInPkt, UINT2 Length,
                  UINT1 Identifier)
{
    UINT1               OptType, OptLen, InPktOffset;
    INT1                RetCode;
    INT2                Index;
    UINT2               RemLen;
    tOptVal             OptVal;
    INT2                OptIndex, PrevOptIndex;
    INT1                RespCode;

    PPP_TRC2 (CONTROL_PLANE, "[CONF NAK]\tId: [%d]\tLen :[%d]", Identifier,
              Length);

    RespCode = RCN;

    /* Make a copy of the option values */
    if (((*ProtSEMTable[ProtIndex].pCPCopyOptions)
         (pGSEM, NOT_SET, CONF_NAK_CODE)) == DISCARD)
    {
        PRINT_MEM_ALLOC_FAILURE;
        return (DISCARD);
    }

    RemLen = Length;
    PrevOptIndex = INIT_VAL;    /* -1 */
    InPktOffset =
        (UINT1) (pGSEM->pIf->LinkInfo.RxProtLen +
                 pGSEM->pIf->LinkInfo.RxAddrCtrlLen + CP_HDR_LEN);

    while (RemLen != 0)
    {
        OptIndex =
            GCPExtractAndValidateOption (pGSEM, pInPkt, &OptType, &OptLen,
                                         &OptVal, InPktOffset, RemLen);
        if ((OptIndex == DISCARD) || (OptIndex == WRONG_LEN))
        {
            PPP_TRC (CONTROL_PLANE, "Invalid option value in Pkt.");
            RespCode = DISCARD;
            break;
        }
        if (OptIndex != UNKNOWN_CODE)
        {
            if (((pGSEM->
                  pNegFlagsPerIf[OptIndex].FlagMask & ALLOW_FORCED_NAK_MASK) ==
                 NOT_SET)
                &&
                ((pGSEM->
                  pNegFlagsPerIf[OptIndex].FlagMask & ACKED_BY_PEER_MASK) ==
                 NOT_SET))
            {
                PPP_TRC (CONTROL_PLANE,
                         "Rxd. option with NAK not allowed. Discarding Packet. ");

                RespCode = DISCARD;
                break;
            }

            if ((PrevOptIndex != INIT_VAL)
                && ((pRecdOptFlags[OptIndex] == SET)
                    ||
                    ((pGSEM->
                      pNegFlagsPerIf[OptIndex].FlagMask & ACKED_BY_PEER_MASK)
                     &&
                     ((pGSEM->
                       pNegFlagsPerIf[PrevOptIndex].
                       FlagMask & ACKED_BY_PEER_MASK) == NOT_SET))
                    || ((OptIndex < PrevOptIndex)
                        && (pGSEM->pNegFlagsPerIf[OptIndex].
                            FlagMask & ACKED_BY_PEER_MASK))))
            {
                PPP_TRC (CONTROL_PLANE,
                         "Rxd.Invalid NAK Packet - Sequence error.");
                RespCode = DISCARD;
                break;
            }
            pRecdOptFlags[OptIndex] = SET;
            if ((*pGSEM->pGenOptInfo[OptIndex].pOptHandlerFuns->
                 pHandleConfNak != NULL)
                && ((pGSEM->OptionFlags & CLOSE_OPTION) == NOT_SET))
            {
                if ((RetCode =
                     (*pGSEM->pGenOptInfo[OptIndex].pOptHandlerFuns->
                      pHandleConfNak) (pGSEM, pInPkt, OptVal)) != OK)
                {
                    if (RetCode == CLOSE)
                    {
                        PPP_DBG
                            ("Option Specific Nak Handler returned Close SEM.");
                        pGSEM->OptionFlags |= CLOSE_OPTION;
                    }
                    else
                    {
                        PPP_DBG
                            ("Option Specific Nak Handler returned DISCARD");
                        RespCode = DISCARD;
                        break;
                    }
                }
            }
            PrevOptIndex = OptIndex;
        }

        InPktOffset = (UINT1) (InPktOffset + OptLen);
        RemLen = (UINT2) (RemLen - OptLen);
    }

    if (RespCode == DISCARD)
    {
        /* Copy back the options and free the temporary array */
        (*ProtSEMTable[ProtIndex].pCPCopyOptions) (pGSEM, SET, CONF_NAK_CODE);
        pGSEM->OptionFlags &= NO_CLOSE_OPTION;
        return (DISCARD);
    }

    /* Set (or unset) AckedByPeer flag for received options appropriately */
    for (Index = 0; Index < pGSEM->MaxOptTypes; Index++)
    {
        if (pRecdOptFlags[Index] == SET)
        {

/* Receiving Nak for Bool options which we have sent is treated as reject */
            if ((pGSEM->pGenOptInfo[Index].BasicType == BOOL_TYPE)
                && (pGSEM->pNegFlagsPerIf[Index].FlagMask & ACKED_BY_PEER_MASK))
            {
                pGSEM->pNegFlagsPerIf[Index].FlagMask &= ACKED_BY_PEER_NOT_SET;
            }
            else
            {
                pGSEM->pNegFlagsPerIf[Index].FlagMask |= ACKED_BY_PEER_SET;
            }
        }
    }

    /* Free the temporary variables as they are no longer needed. */
    (*ProtSEMTable[ProtIndex].pCPCopyOptions) (pGSEM, FREE_ALL, CONF_NAK_CODE);

    return (RCN);
}

/*********************************************************************
*    Function Name    :    GCPRecdConfigRej
*    Description        :
*            This function is invoked to process the CONF_REJ packet and
*    prepare appropriate response.
*    Parameter(s)    :
*            pGSEM    -    Pointer to the GSEM structure corresponding to the
*                        packet.
*            pInPkt    -    Points to the incoming packet buffer.
*            Length    -    Length of the packet (option portion alone)
*        Identifier    -    Identifier received along with the packet
*    Return Value    :    Event to the GSEM  
*********************************************************************/
INT1
GCPRecdConfigRej (tGSEM * pGSEM, t_MSG_DESC * pInPkt, UINT2 Length,
                  UINT1 Identifier)
{
    UINT1               StoredOptLen, MatchFlag = NOT_SET, Type, Len;

    INT2                Index;
    UINT2               InPktOffset, StoredPktOffset, StoredPktLen, InPktLen;

    INT1                Flag;
    INT2                OptIndex;

    PPP_TRC2 (CONTROL_PLANE, "[CONF REJ]\tId: [%d]\tLen :[%d]", Identifier,
              Length);

    /* Initializations */
    Flag = RCN;
    StoredPktLen = (UINT2) (VALID_BYTES (pGSEM->pStoredPkt) - CP_HDR_LEN);
    InPktLen = Length;
    StoredPktOffset = CP_HDR_LEN;
    InPktOffset =
        (UINT2) (pGSEM->pIf->LinkInfo.RxProtLen +
                 pGSEM->pIf->LinkInfo.RxAddrCtrlLen + CP_HDR_LEN);

    /* Check for invalid packet sizes */
    if ((InPktLen > StoredPktLen) || (InPktLen < MIN_OPT_LEN))
    {
        return (DISCARD);
    }

    while (InPktLen != 0)
    {

        /* Extract an option from incoming packet */
        GET1BYTE (pInPkt, InPktOffset, Type);
        GET1BYTE (pInPkt, InPktOffset + OPT_LEN_OFFSET, Len);
        PPP_DBG1 ("Extracted Rej type :%d", Type);

        /* Check if the option has been sent by us. */
        while (StoredPktLen != 0)
        {
            MatchFlag = NOT_SET;
            GET1BYTE (pGSEM->pStoredPkt, StoredPktOffset + OPT_LEN_OFFSET,
                      StoredOptLen);
            StoredPktLen = (UINT2) (StoredPktLen - StoredOptLen);
            if (COMPARE_BUFS
                (pGSEM->pStoredPkt, StoredPktOffset, pInPkt, InPktOffset,
                 Len) == MATCH)
            {
                MatchFlag = SET;
                StoredPktOffset = (UINT2) (StoredPktOffset + StoredOptLen);
                break;
            }
            StoredPktOffset = (UINT2) (StoredPktOffset + StoredOptLen);
        }
        InPktLen = (UINT2) (InPktLen - Len);
        InPktOffset = (UINT2) (InPktOffset + Len);

        if (((OptIndex = GCPGetOptIndex (pGSEM, Type)) == NOT_FOUND)
            || ((StoredPktLen == 0)
                && ((InPktLen != 0) || (MatchFlag == NOT_SET)))
            || (pRecdOptFlags[OptIndex] == SET))
        {

            /*Invalid or unsent option present in incoming packet */
            Flag = DISCARD;
            break;
        }

        pRecdOptFlags[OptIndex] = SET;

        /* Check if Reject is allowed for this option. */
        if ((pGSEM->pNegFlagsPerIf[OptIndex].FlagMask & ALLOW_REJECT_MASK) ==
            NOT_SET)
        {
            PPP_DBG ("Closing : AllowReject is NOT_SET ");
            pGSEM->OptionFlags |= CLOSE_OPTION;
        }

        /* Call the option specific reject handler if it exists */
        if (pGSEM->pGenOptInfo[OptIndex].pOptHandlerFuns->pHandleConfRej !=
            NULL)
        {
            if ((*pGSEM->pGenOptInfo[OptIndex].pOptHandlerFuns->pHandleConfRej)
                (pGSEM) == CLOSE_SEM)
            {
                PPP_DBG
                    ("Closing SEM : Option specific rej. handler returned CLOSE_SEM ");
                pGSEM->OptionFlags |= CLOSE_OPTION;
            }
        }
    }

    if (Flag != DISCARD)
    {

        /* Unset the AckedByPeer flag for options that have got reject */
        for (Index = 0; Index < pGSEM->MaxOptTypes; Index++)
        {
            if (pRecdOptFlags[Index] == SET)
            {
                PPP_DBG1 ("Unsetting Flag of Index: %d ", Index);
                pGSEM->pNegFlagsPerIf[Index].FlagMask &= ACKED_BY_PEER_NOT_SET;
            }
        }
    }
    else
    {

        /* If the packet is to be discarded, the SEM_CLOSE flag should be unset */
        pGSEM->OptionFlags &= NO_CLOSE_OPTION;
    }
    return (Flag);
}

/*********************************************************************
*    Function Name    :    GCPRecdTermReq 
*    Description        :
*            This function is invoked to process the TERM_REQ packet and
*    prepare appropriate response.
*    Parameter(s)    :
*            pGSEM    -    Pointer to the GSEM structure corresponding to the
*                        packet.
*            pInPkt    -    Points to the incoming packet buffer.
*            Length    -    Length of the packet (option portion alone)
*        Identifier    -    Identifier received along with the packet
*    Return Value    :    Event to the GSEM  
*********************************************************************/
INT1
GCPRecdTermReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt, UINT2 Length,
                UINT1 Identifier)
{
    /* Makefile changes - unused */

    PPP_UNUSED (pGSEM);
    PPP_UNUSED (pInPkt);
    PPP_UNUSED (Length);
    PPP_TRC1 (CONTROL_PLANE, "[TERM REQ]\tId: [%d]", Identifier);

    return (RTR);
}

/*********************************************************************
*    Function Name    :    GCPRecdTermAck
*    Description        :
*            This function is invoked to process the TERM_ACK packet and
*    prepare appropriate response.
*    Parameter(s)    :
*            pGSEM    -    Pointer to the GSEM structure corresponding to the
*                        packet.
*            pInPkt    -    Points to the incoming packet buffer.
*            Length    -    Length of the packet (option portion alone)
*        Identifier    -    Identifier received along with the packet
*    Return Value    :    Event to the GSEM  
*********************************************************************/
INT1
GCPRecdTermAck (tGSEM * pGSEM, t_MSG_DESC * pInPkt, UINT2 Length,
                UINT1 Identifier)
{

    /* Makefile changes - unused */

    PPP_UNUSED (pInPkt);
    PPP_UNUSED (pGSEM);
    PPP_UNUSED (Length);
    PPP_TRC1 (CONTROL_PLANE, "[TERM ACK]\tId: [%d]", Identifier);

    /* Incoming packet is not compared with stored packet.
       This follows discussion of this issue in the ietf-ppp mailing list.
     */
    if ((pGSEM->CurrentState != OPENED) && (Identifier != pGSEM->RequestId))
    {
        return (DISCARD);
    }
    return (RTA);
}

/*********************************************************************
*    Function Name    :    GCPRecdCodeRej 
*    Description        :
*            This function is invoked to process the CODE_REJ packet and
*    prepare appropriate response.
*    Parameter(s)    :
*            pGSEM    -    Pointer to the GSEM structure corresponding to the
*                        packet.
*            pInPkt    -    Points to the incoming packet buffer.
*            Length    -    Length of the packet (option portion alone)
*        Identifier    -    Identifier received along with the packet
*    Return Value    :    Event to the GSEM  
*********************************************************************/
INT1
GCPRecdCodeRej (tGSEM * pGSEM, t_MSG_DESC * pInPkt, UINT2 Length,
                UINT1 Identifier)
{
    UINT1               CodeVal;

    PPP_TRC2 (CONTROL_PLANE, "[CODE REJ]\tId: [%d]\tLen :[%d]", Identifier,
              Length);

    if (Length != 0)
    {
        EXTRACT1BYTE (pInPkt, CodeVal);

        if (((CodeVal >= MIN_GEN_CODE) && (CodeVal <= MAX_GEN_CODE))
            || (GCPMatchProtNCode (pGSEM->Protocol, CodeVal) != NOT_FOUND))
        {
            return (RXJ_MINUS);
        }
    }
    return (RXJ_PLUS);
}

/*********************************************************************
*    Function Name    :    GCPRecdConfigAck 
*    Description        :
*            This function is invoked to process the CONFIG_ACK packet and
*    prepare appropriate response.
*    Parameter(s)    :
*            pGSEM    -    Pointer to the GSEM structure corresponding to the
*                        packet.
*        x    pInPkt    -    Points to the incoming packet buffer.
*            Length    -    Length of the packet (option portion alone)
*        Identifier    -    Identifier received along with the packet
*    Return Value    :    Event to the GSEM  
*********************************************************************/
INT1
GCPRecdConfigAck (tGSEM * pGSEM, t_MSG_DESC * pInPkt, UINT2 Length,
                  UINT1 Identifier)
{

    PPP_TRC2 (CONTROL_PLANE, "[CONF ACK]\tId: [%d]\tLen :[%d]", Identifier,
              Length);

    /* Check if the incoming ack and the sent request packets are identical */

    if (((Length == 0) && ((VALID_BYTES (pGSEM->pStoredPkt) - CP_HDR_LEN) == 0))
        || ((Length == (VALID_BYTES (pGSEM->pStoredPkt) - CP_HDR_LEN))
            &&
            (COMPARE_BUFS
             (pGSEM->pStoredPkt, CP_HDR_LEN, pInPkt,
              (pGSEM->pIf->LinkInfo.RxProtLen +
               pGSEM->pIf->LinkInfo.RxAddrCtrlLen + CP_HDR_LEN),
              Length) == MATCH)))
    {
        if (pGSEM->pIf->BundleFlag == BUNDLE)
        {
            pGSEM->pIf->MPInfo.BundleInfo.RespRecd = PPP_YES;
        }
        else
        {
            if (pGSEM->pIf->MPInfo.MemberInfo.pBundlePtr != NULL)
            {
                pGSEM->pIf->MPInfo.MemberInfo.pBundlePtr->MPInfo.BundleInfo.
                    RespRecd = PPP_YES;
            }
        }
        return (RCA);
    }
    return (DISCARD);
}

/*********************************************************************
*      Function Name     :    GCPExtractAndValidateOption 
*    Description        :            
*        Function extracts ONE option from pInPkt and validates it against 
*    the statically stored Option values.
*    Parameter(s)    :
*            pGSEM    -    Points to SEM.
*            pInPkt     -    Points to the incoming PDU.
*            Type    -    The extracted type to be returned.
*            Len        -    The extracted length to be returned.
*            Value    -    Extracted value of option and its sub-options to be 
*                        returned.
*           RemLen    -   Remaining length of the packet from which the option
*                        is to be extracted.
*    Return Value    :    Returns DISCARD if the option is invalid / index of 
*                        the option in the options array otherwise.
*********************************************************************/
INT1
GCPExtractAndValidateOption (tGSEM * pGSEM, t_MSG_DESC * pInPkt, UINT1 *Type,
                             UINT1 *Len, tOptVal * pOptVal, UINT2 Offset,
                             UINT2 RemLen)
{
    /* Makefile changes - unused variable */
    INT2                OptIndex;
    UINT1               NoOfDiscVals;
    tGenOptionInfo     *pOptionInfo;
    VOID               *pTemp;

    /* Check if packet has enough bytes left for extraction */
    if (RemLen < MIN_OPT_LEN)
    {
        return (DISCARD);
    }

    /* Extract option type and length */
    GET1BYTE (pInPkt, Offset, *Type);
    GET1BYTE (pInPkt, (Offset + OPT_LEN_OFFSET), *Len);

    /* Check if option is supported. If so get its index. */
    if ((OptIndex = GCPGetOptIndex (pGSEM, *Type)) == NOT_FOUND)
    {
        if ((*Len < MIN_OPT_LEN) || (*Len > RemLen))
        {
            PPP_TRC (CONTROL_PLANE, "Invalid option length in packet");
            return (DISCARD);
        }
        PPP_TRC (CONTROL_PLANE, "Unknown Option in packet");
        return (UNKNOWN_CODE);
    }

    pOptionInfo = &(pGSEM->pGenOptInfo[OptIndex]);

    /* DSL_ADD: if the flags are to 0 for an option you should reject
     * the option. For Naetra we have made the flags for MP options
     * as ZERO.
     */
    if (pGSEM->pNegFlagsPerIf[OptIndex].FlagMask == 0)
    {
        return (UNKNOWN_CODE);
    }

    if (((pOptionInfo->Category != DISCRETE)
         && (pOptionInfo->MaxOptLen != *Len))
        || ((pOptionInfo->Category == DISCRETE)
            && (pOptionInfo->MaxOptLen > *Len)))
    {

        if ((pOptionInfo->MaxOptLen > RemLen)
            || ((pOptionInfo->Category != STRING_TYPE)
                && (pOptionInfo->MaxOptLen < *Len)))
            return (DISCARD);

        ExtractedLength = *Len;
        *Len = pOptionInfo->MaxOptLen;
        ASSIGN_1_BYTE (pInPkt, (UINT4) (Offset + OPT_LEN_OFFSET), *Len);
        gu1WrongLenOptIndex = (UINT1) OptIndex;
        return (WRONG_LEN);
    }

    /* Check if length in within valid bounds */
    if ((*Len < MIN_OPT_LEN) || (*Len > RemLen))
    {
        PPP_TRC (CONTROL_PLANE, "Invalid option length in packet");
        return (DISCARD);
    }

    /* Validate length when the option does not have sub options */
    if ((pOptionInfo->Category == STRING_TYPE)
        && (*Len > pOptionInfo->MaxOptLen))
    {
        PPP_DBG ("Bad Length String option Rxd.");
        return (DISCARD);
    }

    if ((pOptionInfo->Category != STRING_TYPE)
        &&
        (((pOptionInfo->
           Category != DISCRETE)
          || (pOptionInfo->pOptHandlerFuns->pHandleAddSubOpt == NULL))
         && (*Len != pOptionInfo->MaxOptLen)))
    {
        PPP_DBG ("Bad Length Non Boolean option Rxd.");
        return (DISCARD);
    }

    /*The following global variable, ExtractedLength, to be used only
     * by handlers of discrete options which have sub-options */
    ExtractedLength = *Len;

    /* Extract the option value and validate it based on the BasicType. */
    switch (pOptionInfo->BasicType)
    {

        case BOOL_TYPE:
            return ((INT1) OptIndex);

        case BYTE_LEN_1:
            GET1BYTE (pInPkt, (Offset + OPT_HDR_LEN), pOptVal->CharVal);
            if (pOptionInfo->Category == RANGE)
            {
                if ((pOptVal->CharVal <
                     pOptionInfo->OptPossibleInfo.pRangeInfo->MinVal.CharVal)
                    || (pOptVal->CharVal >
                        pOptionInfo->OptPossibleInfo.pRangeInfo->MaxVal.
                        CharVal))
                {
                    return (DISCARD);
                }
                return ((INT1) OptIndex);
            }
            else
            {
                NoOfDiscVals =
                    (UINT1) (((tDiscHeader *) pOptionInfo->OptPossibleInfo.
                              pDiscHeader)->NoOfDiscEntries);
                pTemp =
                    (UINT1 *) (pOptionInfo->OptPossibleInfo.pDiscHeader) +
                    sizeof (tDiscHeader);
                while (NoOfDiscVals != 0)
                {
                    if (*((UINT1 *) pTemp) == pOptVal->CharVal)
                    {
                        return ((INT1) OptIndex);
                    }
                    pTemp = ((UINT1 *) pTemp) + 1;
                    NoOfDiscVals--;
                }
                return (DISCARD);
            }
            break;

        case BYTE_LEN_2:
            GET2BYTE (pInPkt, (Offset + OPT_HDR_LEN), pOptVal->ShortVal);
            if (pOptionInfo->Category == RANGE)
            {
                if ((pOptVal->ShortVal <
                     pOptionInfo->OptPossibleInfo.pRangeInfo->MinVal.ShortVal)
                    || (pOptVal->ShortVal >
                        pOptionInfo->OptPossibleInfo.pRangeInfo->MaxVal.
                        ShortVal))
                {
                    return (DISCARD);
                }
                return ((INT1) OptIndex);
            }
            else
            {
                NoOfDiscVals =
                    (UINT1) (((tDiscHeader *) pOptionInfo->OptPossibleInfo.
                              pDiscHeader)->NoOfDiscEntries);
                pTemp =
                    (UINT1 *) (pOptionInfo->OptPossibleInfo.pDiscHeader) +
                    sizeof (tDiscHeader);

                while (NoOfDiscVals != 0)
                {
                    if (*((UINT2 *) pTemp) == pOptVal->ShortVal)
                    {
                        return ((INT1) OptIndex);
                    }
                    pTemp = ((UINT2 *) pTemp) + 1;
                    NoOfDiscVals--;
                }
                return ((INT1) OptIndex);
            }
            break;

        case BYTE_LEN_4:
            GET4BYTE (pInPkt, (Offset + OPT_HDR_LEN), pOptVal->LongVal);
            if (pOptionInfo->Category == RANGE)
            {
                if ((pOptVal->LongVal <
                     pOptionInfo->OptPossibleInfo.pRangeInfo->MinVal.LongVal)
                    || (pOptVal->LongVal >
                        pOptionInfo->OptPossibleInfo.pRangeInfo->MaxVal.
                        LongVal))
                {
                    return (DISCARD);
                }
                return ((INT1) OptIndex);
            }
            else
            {
                NoOfDiscVals =
                    (UINT1) (((tDiscHeader *) pOptionInfo->OptPossibleInfo.
                              pDiscHeader)->NoOfDiscEntries);
                pTemp =
                    (UINT1 *) (pOptionInfo->OptPossibleInfo.pDiscHeader) +
                    sizeof (tDiscHeader);
                while (NoOfDiscVals != 0)
                {
                    if (*((UINT4 *) pTemp) == pOptVal->LongVal)
                    {
                        return ((INT1) OptIndex);
                    }
                    pTemp = ((UINT4 *) pTemp) + 1;
                    NoOfDiscVals--;
                }
                return (DISCARD);
            }
            break;

        case STRING_TYPE:
            if (pOptStrVal != NULL)
            {
                FREE_STR (pOptStrVal);
                pOptStrVal = NULL;
            }
            if ((pOptStrVal = ALLOC_STR (*Len)) == NULL)
            {
                PRINT_MEM_ALLOC_FAILURE;
                return (DISCARD);
            }
            pOptVal->StrVal = pOptStrVal;
            GETSTR (pInPkt, pOptVal->StrVal, (Offset + OPT_HDR_LEN), *Len);
            return ((INT1) OptIndex);

        default:
            OptIndex = DISCARD;
            break;
    }
    /* Return here with OptIndex: No further validation necessary because
     * NAK processing routines will handle the case. */
    return ((INT1) OptIndex);
}

/*********************************************************************
*    Function Name    :    GCPAddConfigInfo
*    Description        :
*        Function constructs a configuration request packet.
*    Parameter(s)    :
*            pGSEM    :    Points to the protocol SEM.
*            pOutBuf    :    Outgoing buffer to be filled up.
*    Return Value    :    Size of constructed packet.
*********************************************************************/
INT2
GCPAddConfigInfo (tGSEM * pGSEM, t_MSG_DESC * pOutBuf)
{
    UINT1               OptLen;
    INT2                Index;
    UINT2               Offset, Length;
    tOptVal             OptionValue;
    INT1                SubOptLen;

    /* Initializations */
    pGSEM->NumOptReq = 0;
    Length = 0;
    Offset = 0;

    PPP_DBG1 ("MaxOpt Types:%d", pGSEM->MaxOptTypes);
    for (Index = 0; Index < pGSEM->MaxOptTypes; Index++)
    {
        PPP_DBG1 ("AddC Idx is:%d", Index);

        /* Check if this option is to be requested. */
        if (pGSEM->pNegFlagsPerIf[Index].FlagMask & ACKED_BY_PEER_MASK)
        {
            PPP_DBG1 ("Adding option of Idx : %d", Index);

            /* Initializations */
            pGSEM->NumOptReq++;
            SubOptLen = 0;
            OptLen =
                (UINT1) (OPT_HDR_LEN + pGSEM->pGenOptInfo[Index].BasicType);

            PPP_DBG1 ("OptLen :%d", OptLen);
            /* Depending on the BasicType add the option values. */
            switch (pGSEM->pGenOptInfo[Index].BasicType)
            {

                case BOOL_TYPE:
                    break;

                case BYTE_LEN_1:
                    OptionValue.CharVal =
                        *((UINT1 *)
                          (pGSEM->pGenOptInfo[Index].pOptHandlerFuns->
                           pRetDtaPtr (pGSEM, &OptLen)));
                    ASSIGN1BYTE (pOutBuf, (Offset + OPT_HDR_LEN),
                                 OptionValue.CharVal);
                    break;

                case BYTE_LEN_2:
                    OptionValue.ShortVal =
                        *((UINT2 *)
                          (pGSEM->pGenOptInfo[Index].pOptHandlerFuns->
                           pRetDtaPtr (pGSEM, &OptLen)));
                    ASSIGN2BYTE (pOutBuf, (Offset + OPT_HDR_LEN),
                                 OptionValue.ShortVal);
                    break;

                case BYTE_LEN_4:
                    OptionValue.LongVal =
                        *((UINT4 *)
                          (pGSEM->pGenOptInfo[Index].pOptHandlerFuns->
                           pRetDtaPtr (pGSEM, &OptLen)));
                    ASSIGN4BYTE (pOutBuf, (Offset + OPT_HDR_LEN),
                                 OptionValue.LongVal);
                    break;

                case STRING_TYPE:
                    OptionValue.StrVal =
                        (UINT1 *) (pGSEM->pGenOptInfo[Index].pOptHandlerFuns->
                                   pRetDtaPtr (pGSEM, &OptLen));
                    ASSIGNSTR (pOutBuf, OptionValue.StrVal,
                               (Offset + OPT_HDR_LEN), OptLen);
                    OptLen = (UINT1) (OptLen + OPT_HDR_LEN);
                    break;

                default:
                    return (DISCARD);
            }

            /* Check if option has suboptions. If so call appropiate handler */
            if (pGSEM->pGenOptInfo[Index].pOptHandlerFuns->pHandleAddSubOpt !=
                NULL)
            {
                SubOptLen =
                    (*pGSEM->pGenOptInfo[Index].pOptHandlerFuns->
                     pHandleAddSubOpt) (pGSEM, OptionValue, pOutBuf);
            }

            /* Fill the option header */
            ASSIGN1BYTE (pOutBuf, Offset, pGSEM->pGenOptInfo[Index].OptType);
            if (pGSEM->pGenOptInfo[Index].RepeatAllowFlag == SET)
            {
                ASSIGN1BYTE (pOutBuf, Offset + OPT_LEN_OFFSET, OptLen);
            }
            else
            {
                ASSIGN1BYTE (pOutBuf, Offset + OPT_LEN_OFFSET,
                             (UINT1) (OptLen + SubOptLen));
            }

            /* Update packet size and offset */
            Length = (UINT2) (Length + OptLen + SubOptLen);
            Offset = (UINT2) (Offset + OptLen + SubOptLen);
        }
    }

    return ((INT2) Length);
}

/*********************************************************************
                    UTILITY FUNCTIONS FOR GCP
*********************************************************************/

/*********************************************************************
*    Function Name   :    GCPMatchProtNCode 
*    Description     :
*        This function matches the protocol and code fields sent as 
*    parameters and returns index of the matched values in the array of 
*    structures. 
*    Parameter(s)    :
*            Prot    :    The protocol to be matched.
*            Code    :    The code to be matched.
*    Return Vale        :     Index of matched pair in the array if found /
*                        NOT_MATCHED otherwise.
*********************************************************************/
INT1
GCPMatchProtNCode (UINT2 Prot, UINT2 Code)
{
    INT2                Index;

    static UINT2        MaxMiscCode =
        (sizeof (ProtSpecificEvents) / sizeof (tMiscPktHandler));

    for (Index = 0; Index < MaxMiscCode; Index++)
    {
        if ((Prot == ProtSpecificEvents[Index].Protocol)
            && (Code == ProtSpecificEvents[Index].Code))
        {
            return ((INT1) Index);
        }
    }
    return (NOT_FOUND);
}

/*********************************************************************
*    Function Name    :    GCPGetOptIndex 
*    Description        :
*            This function finds the index of an option in the array of  
*    of options. 
*    Parameter(s)    :
*            *pGSEM    :    Pointer to the protocol SEM.
*            OptType    :    The option whose index is to be to be found.    
*    Return Vale        :   Index of the matched option in the option array if 
*                        found / NOT_FOUND otherwise.
*********************************************************************/
INT1
GCPGetOptIndex (tGSEM * pGSEM, UINT1 OptType)
{
    INT2                Index;

    for (Index = 0; Index < pGSEM->MaxOptTypes; Index++)
    {
        if (OptType == pGSEM->pGenOptInfo[Index].OptType)
        {
            return ((INT1) Index);
        }
    }
    return (NOT_FOUND);
}

/*********************************************************************
*    Function Name    :    GCPGetProtIndex 
*    Description        :
*            This function finds the index of a protocol in the array of a
*    from the array of protocols.
*    Parameter(s)    :
*        Protocol    :    The protocol whose index is to be to be found.
*    Return Vale        :    Index of the matched protocol if found / 
*                        NOT_FOUND otherwise. 
*********************************************************************/
INT1
GCPGetProtIndex (UINT2 Protocol)
{
    INT2                Index;

    static UINT2        MaxProtId = (sizeof (ProtSEMTable) / sizeof (tProtSEM));

    for (Index = 0; Index < MaxProtId; Index++)
    {
        if (Protocol == ProtSEMTable[Index].ProtID)
        {
            return ((INT1) Index);
        }
    }
    return (NOT_FOUND);
}

/* By Shenoy - To handle MaxNakLoops */
/*********************************************************************
*    Function Name    :    GCPHandleMaxNakLoops 
*    Description        :
*                      This function handles when the no. of nak sent exceeds
*   the MaxNakLoops.
*   
*    Parameter(s)    :
*           pGSEM   :   pointer to the corresponding SEM.
*           pInPkt  :   pointer to Incoming Packet .
*         InPktLen  :   length of the incoming packet.
*  pConstructedNakPktLen  : pointer to the constructed nak packet .
*  
*    Return Value    :  Return code indicates the action that has to be taken whe*   MaxNaKLoop condition occurs. Discard or CONF_ACK_CODE or CONF_REJ_CODE    
*********************************************************************/
INT1
GCPHandleMaxNakLoops (tGSEM * pGSEM, t_MSG_DESC * pInPkt, UINT2 InPktLength,
                      UINT2 *pConstructedNakPktLen)
{
    t_MSG_DESC         *pConstructedNakPkt;
    UINT1               ReqOptLen,
        NakOptLen, NakOptionType, RequestOptionType, MRRUFlag, FoundFlag;
    UINT2               InitOffset, InPktOffset, OutPktOffset, NakPktOffset;

    MRRUFlag = NOT_SET;
    FoundFlag = NOT_SET;
    pConstructedNakPkt = pGSEM->pOutParam;
    NakPktOffset = CP_HDR_LEN;
    OutPktOffset = CP_HDR_LEN;
    pGSEM->pOutParam = NULL;
    InitOffset =
        (UINT2) (pGSEM->pIf->LinkInfo.RxProtLen +
                 pGSEM->pIf->LinkInfo.RxAddrCtrlLen + CP_HDR_LEN);

    if ((pGSEM->pOutParam =
         (t_MSG_DESC *) (VOID *) ALLOCATE_BUFFER (MAX_PKT_SIZE,
                                                  PPP_INFO_OFFSET)) == NULL)
    {
        PRINT_MEM_ALLOC_FAILURE;
        return (DISCARD);
    }

    while (NakPktOffset != *pConstructedNakPktLen)
    {
        GET1BYTE (pConstructedNakPkt, NakPktOffset, NakOptionType);
        GET1BYTE (pConstructedNakPkt, NakPktOffset + 1, NakOptLen);
        if (NakOptionType == 17)
        {
            MRRUFlag = SET;
        }
        InPktOffset = InitOffset;
        while ((InPktOffset - InitOffset) != InPktLength)
        {
            GET1BYTE (pInPkt, InPktOffset, RequestOptionType);
            GET1BYTE (pInPkt, InPktOffset + 1, ReqOptLen);
            if (NakOptionType == RequestOptionType)
            {
                CRU_BUF_COPY_MSGS (pGSEM->pOutParam, pInPkt, OutPktOffset,
                                   InPktOffset, ReqOptLen);
                OutPktOffset = (UINT2) (OutPktOffset + ReqOptLen);
                FoundFlag = SET;
            }
            InPktOffset = (UINT2) (InPktOffset + ReqOptLen);
        }
        NakPktOffset = (UINT2) (NakPktOffset + NakOptLen);
    }
    if (FoundFlag == NOT_SET)
    {
        ReleaseFlag = PPP_NO;
        RELEASE_BUFFER (pGSEM->pOutParam);
        pGSEM->pOutParam = pInPkt;
        MOVE_OFFSET (pGSEM->pOutParam,
                     pGSEM->pIf->LinkInfo.RxProtLen +
                     pGSEM->pIf->LinkInfo.RxAddrCtrlLen);
        *pConstructedNakPktLen = (UINT2) (InPktLength + CP_HDR_LEN);
        return (CONF_ACK_CODE);
    }
    *pConstructedNakPktLen = (UINT2) VALID_BYTES (pGSEM->pOutParam);
    RELEASE_BUFFER (pConstructedNakPkt);
    PPP_UNUSED (MRRUFlag);
    return (CONF_REJ_CODE);
}

void
PPPPrintProtocol (UINT2 Protocol)
{
    UINT1               au1Temp[20];
    switch (Protocol)
    {
        case LCP_PROTOCOL:
            STRCPY (au1Temp, "LCP");
            break;
        case IPCP_PROTOCOL:
            STRCPY (au1Temp, "IPCP");
            break;
        case MPLSCP_PROTOCOL:
            STRCPY (au1Temp, "MPLSCP");
            break;
        case IPV6CP_PROTOCOL:
            STRCPY (au1Temp, "IPv6CP");
            break;
        case IPXCP_PROTOCOL:
            STRCPY (au1Temp, "IPXCP");
            break;
        case BCP_PROTOCOL:
            STRCPY (au1Temp, "BCP");
            break;
        case BACP_PROTOCOL:
            STRCPY (au1Temp, "BACP");
            break;
        case ECP_OVER_MEMBER_LINK:
            STRCPY (au1Temp, "ECP MP");
            break;
        case ECP_OVER_INDIVIDUAL_LINK:
            STRCPY (au1Temp, "ECP LINK");
            break;
        case CCP_OVER_MEMBER_LINK:
            STRCPY (au1Temp, "CCP MP");
            break;
        case CCP_OVER_INDIVIDUAL_LINK:
            STRCPY (au1Temp, "CCP LINK");
            break;
        case IP_DATAGRAM:
            STRCPY (au1Temp, "IP DATA");
            break;
        case IP_COMP_TCP_DATA:
            STRCPY (au1Temp, "IP COMP TCP");
            break;
        case IP_UNCOMP_TCP_DATA:
            STRCPY (au1Temp, "IP UNCOMP TCP");
            break;
        case MPLS_UNICAST_DATAGRAM:
            STRCPY (au1Temp, "MPLS UNI DATA");
            break;
        case MPLS_MULTICAST_DATAGRAM:
            STRCPY (au1Temp, "MPLS MULTI DATA");
            break;
        case IPX_DATAGRAM:
            STRCPY (au1Temp, "IPX DATA");
            break;
        case TELEBIT_COMP_PROTO:
            STRCPY (au1Temp, "TELEBIT COMP");
            break;
        case SHIVA_COMP_PROTO:
            STRCPY (au1Temp, "SHIVA COMP");
            break;
        case BCP_SPAN_IEEE_DATA:
            STRCPY (au1Temp, "BCP SPAN IEEE");
            break;
        case BCP_IBM_SRC_RT_DATA:
            STRCPY (au1Temp, "BCP IBM DATA");
            break;
        case BCP_DEC_LAN_DATA:
            STRCPY (au1Temp, "BCP DEC LAN");
            break;
        case BCP_BRIDGED_LAN_DATA:
            STRCPY (au1Temp, "BCP BRIDGED LAN");
            break;
        case ECP_DATA_OVER_MEMBER_LINK:
            STRCPY (au1Temp, "ECP DATA BUNDLE");
            break;
        case ECP_DATA_OVER_INDIVIDUAL_LINK:
            STRCPY (au1Temp, "ECP DATA LINK");
            break;
        case BAP_PROTOCOL:
            STRCPY (au1Temp, "BAP");
            break;
        case MP_DATAGRAM:
            STRCPY (au1Temp, "MP");
            break;
        case CHAP_PROTOCOL:
            STRCPY (au1Temp, "CHAP");
            break;
        case PAP_PROTOCOL:
            STRCPY (au1Temp, "PAP");
            break;
        case EAP_PROTOCOL:
            STRCPY (au1Temp, "EAP");
            break;
        case LQM_PROTOCOL:
            STRCPY (au1Temp, "LQM");
            break;
        default:
            STRCPY (au1Temp, "UNKNOWN");
            break;
    }

    PPP_TRC2 (CONTROL_PLANE, "Protocol:  %s [0x%x]", au1Temp, Protocol);
}
