/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppccp.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the typedefs, #defines and data
 *             structures used by the CCP Module.      
 *
 *******************************************************************/
/*
#include "ccpcom.h"
#include "gcpdefs.h"
#include "lcpdefs.h"
#include "gsemdefs.h"
#include "pppgzif.h"
#include "gzip.h"
#include "gzipprot.h"
#include "genexts.h"
#include "globexts.h"

*/
#include  "pppccpmd.h"

#ifndef __PPP_PPPCCP_H__
#define __PPP_PPPCCP_H__

/*********************************************************************/
/*                              CONSTANTS                           */
/*********************************************************************/

#define  GZIP_IDX             0                              
#define  MPPE_IDX             1                              
#define  MAX_CCP_OPTIONS      2                              
#define  COMPRESSED           1                              
#define  UNCOMPRESSED         2                              
#define  CODE_LEN             1                              
#define  ID_LEN               1                              
#define  LENGTH_LEN           2                              
#define  TYPE_OFFSET          0                              
#define  LENGTH_OFFSET        1                              
#define  WNDSIZE_LENGTH       2                              
#define  RESET_HEADER_LENGTH  CODE_LEN + ID_LEN + LENGTH_LEN 
#define  GZIP_PARAMS_LENGTH   4                              
#define  CCP_ENABLED          1                              
#define  NM_OVER_BUNDLE       1
#define  DISCARD_CCP_PKT     -2
#define  DISCARD_ECP_PKT     -2


typedef struct ccpStat {
 
 UINT1 Id;
 UINT1 u1Rsvd1;
 UINT2 RxResetReqs;
 UINT2 TxResetReqs;
 UINT2 RxResetAcks;
 UINT2 TxResetAcks;
 UINT2 u2Rsvd2;

}tCCPStatus;


typedef struct ccpif{

 UINT1  OperStatus;
 UINT1  AdminStatus;
 UINT2  u2Rsvd1;
 tCCPStatus Status;
 t_SLL  OptionsDesired;
 t_SLL  OptionsAllowedForPeer; 
 t_SLL  OptionsAckedByPeer;
 t_SLL  OptionsAckedByLocal; 
 tGSEM  CCPGSEM;

 /* The purpose of the following two elements is to store the compression
  algorithm related information. As an example, the History related
  information of the LZS algorithm is stored here. Each element points
  to the structure which contains per interface information of the Stac.
  This structure contains History number, Check mode and pointer to
  History. Each Network Layer Protocol (NLP) is assigned unique History
  number and the compression is done based on this History number. When
  the packet has to be compressed 'OutCompIf' is passed as one of the 
  parameters and the operation is performed based on the history number
  contained in this structure. For decompression, 'InCompIf' is used. This
  information is maintained in the LZS module and here, it is stored as
  VOID*.  */

 VOID*  pInCompIf;
 VOID*  pOutCompIf;
/****************** SKR004 new variable added to trace index of Compression algorothm **********/
 UINT1   CCPCurrentIndex;
 UINT1	 u1Rsvd2;
 UINT2	 u2Rsvd3;
 t_MSG_DESC *pResetReq;

}   tCCPIf;



typedef struct {

 UINT1 OptType;
 UINT1 u1Rsvd1;
 UINT2 u2Rsvd2;
 INT1 (*InitCompressionInfo)(VOID **,tCompressOperation);
 INT1 (*CreateCompressionInfo)(VOID *,VOID *,tCompressOperation);
 INT1 (*DeleteCompressionInfo)(VOID *);
 INT1 (*ValidateResetPkt)(UINT2,UINT1);
 INT1 (*ResetInfo)(VOID *,UINT2 );
 INT1 (*Decompress)(t_MSG_DESC **,VOID *);
 INT1 (*Compress)(t_MSG_DESC **,VOID *);
 UINT2 (*ConstructResetReqPkt)(t_MSG_DESC *,VOID *,VOID*,UINT2);

}tCCPFNPOINTER;



#endif  /* __PPP_PPPCCP_H__ */
