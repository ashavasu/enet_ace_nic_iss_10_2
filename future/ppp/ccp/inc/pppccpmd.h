/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppccpmd.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description: This file contains definitions for GZIP module
 *
 *******************************************************************/
#ifndef __PPP_PPPCCPMD_H__
#define __PPP_PPPCCPMD_H__

#define OPTION_GZIP	4	/* The option type 4 to 15 can be assigned to any of 
							the public domain algorithms. Here i have chosen
							type as 4 and the type of the GZIP algorithm is
							yet to be finalised. */
#define OPTION_MPPE	18

typedef enum CompressOperation {compression =0, decompression =1} tCompressOperation;



#endif  /* __PPP_PPPCCPMD_H__ */
