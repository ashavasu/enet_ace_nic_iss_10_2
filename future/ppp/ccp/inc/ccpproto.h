/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ccpproto.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description: This file contains the function prototypes for CCP
 * functions used internally by the CCP module
 *
 *******************************************************************/
#ifndef __PPP_CCPPROTO_H__
#define __PPP_CCPPROTO_H__


tCCPIf *CCPCreateIf(tPPPIf *);
VOID CCPDeleteIf(tCCPIf *pCCPIf);
VOID CCPEnableIf(tCCPIf *pCCP);
VOID CCPDisableIf(tCCPIf *pCCP);
VOID CCPInit(tCCPIf *pCCPIf);

VOID CCPRxData(tGSEM *pGSEM);
VOID CCPTxData(tGSEM *pGSEM);

INT1 CCPRecdResetReq(tGSEM *pGSEM, t_MSG_DESC *pIn, UINT2 Length, UINT1 Id);
VOID CCPProcessResetReq(tGSEM *pGSEM);
INT1 CCPRecdResetAck(tGSEM *pGSEM, t_MSG_DESC *pIn, UINT2 Length, UINT1 Id);
VOID CCPProcessResetAck(tGSEM *pGSEM);
INT1 CCPProcessConfRej(tGSEM  *pGSEM);

INT2 GetCCPOptionIndex(UINT1 Type);
tGSEM *CCPGetSEMPtr(tPPPIf *pIf);
INT1 CCPCopyOptions ( tGSEM *pGSEM, UINT1 Flag, UINT1 PktType );

VOID CCPUp(tGSEM *pCCPGSEM);
VOID CCPDown(tGSEM *pCCPGSEM);

VOID CreateCompressionSpecificInfo(tGSEM *pCCPGSEM, tHeader *pHeader, UINT1 Mode, UINT1 Index);
VOID DeleteCompressionSpecificInfo(tGSEM *pCCPGSEM, UINT1 Mode, UINT1 Index);
VOID CCPDeleteUnAckedNodes(tGSEM *pGSEM);
VOID CCPCopyDesiredToAckedByPeer(tCCPIf *pCCP);

INT1 ValidateGZIPResetAck(tCCPIf * pCCPIf, UINT2 Length, UINT1 Id);

/* Prototypes related to SNMP */
INT1 CheckAndGetCCPTxPtr(tPPPIf *pIf, UINT4 *n1, UINT4 *n2, UINT4 SecondIndex);
INT1 CheckAndGetCCPRxPtr(tPPPIf *pIf, UINT4 *n1, UINT4 *n2, UINT4 SecondIndex);
INT1 CheckAndGetCCPPtr(tPPPIf *pIf);
/* Makefile changes - <variable with no type> */
tCCPIf *SNMPGetCCPIfPtr(UINT4 Index);
tCCPIf *SNMPGetOrCreateCCPIfPtr(UINT4 Index);


#endif  /* __PPP_CCPPROTO_H__ */
