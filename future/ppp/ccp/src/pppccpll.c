/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppccpll.c,v 1.4 2014/03/11 14:02:46 siva Exp $
 *
 * Description:This file contains SNMP low level routines for 
 *             CCP module 
 *
 *******************************************************************/
#include "pppsnmpm.h"
#include "ccpcom.h"
#include "snmccons.h"            /* Mani */
#include "pppmppeif.h"
#ifdef GZIP
#include "pppgzif.h"
#include "gzip.h"
#include "gzipprot.h"
#endif
#include  "pppccpmd.h"
#include "mppeprot.h"
#include "gsemdefs.h"
#include "genexts.h"
#include "pppccpmd.h"
#include "llproto.h"
#include "ppccplow.h"

#define CCP_RX_ADMIN_STATUS_CLOSED 2
#define CCP_RX_ADMIN_STATUS_OPENED 1
VOID                InsertNode (tCCPIf * pCCPIf, tHeader * pCurrentHeader,
                                UINT4 Priority);

/****************** Start of Low Level get routines for pppCCPConfig*************/

INT1
CheckAndGetCCPTxPtr (tPPPIf * pIf, UINT4 *n1, UINT4 *n2, UINT4 SecondIndex)
{
    tHeader            *pHeader;
    tCCPIf             *pCCPIf;

    pCCPIf = (tCCPIf *) pIf->pCCPIf;

    if (pCCPIf != NULL)
    {
        SLL_SCAN (&pCCPIf->OptionsDesired, pHeader, tHeader *)
        {
            if ((pHeader != NULL) && (pHeader->Priority >= SecondIndex)
                && (pHeader->Priority < *n2))
            {
                *n2 = pHeader->Priority;
                *n1 = pIf->LinkInfo.IfIndex;
            }
            if (pHeader == NULL)
            {
                break;
            }
        }
    }
    return (PPP_SNMP_ERR);
}

INT1
CheckAndGetCCPRxPtr (tPPPIf * pIf, UINT4 *n1, UINT4 *n2, UINT4 SecondIndex)
{
    tHeader            *pHeader;
    tCCPIf             *pCCPIf;

    pCCPIf = (tCCPIf *) pIf->pCCPIf;

    if (pCCPIf != NULL)
    {
        SLL_SCAN (&pCCPIf->OptionsAllowedForPeer, pHeader, tHeader *)
        {
            if ((pHeader != NULL) && (pHeader->Type >= SecondIndex)
                && (pHeader->Type < *n2))
            {
                *n2 = pHeader->Type;
                *n1 = pIf->LinkInfo.IfIndex;
            }
            if (pHeader == NULL)
            {
                break;
            }
        }
    }
    return (PPP_SNMP_OK);
}

INT1
CheckAndGetCCPPtr (tPPPIf * pIf)
{
    if (pIf->pCCPIf != NULL)
    {
        return (PPP_SNMP_OK);
    }
    return (PPP_SNMP_ERR);
}

/**************************************************************************/
tCCPIf             *
SNMPGetCCPIfPtr (Index)
     UINT4               Index;
{
    tPPPIf             *pIf;

    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if ((pIf != NULL) && (pIf->LinkInfo.IfIndex == Index))
        {
            return ((tCCPIf *) pIf->pCCPIf);
        }
    }
    return ((tCCPIf *) NULL);
}

tCCPIf             *
SNMPGetOrCreateCCPIfPtr (Index)
     UINT4               Index;
{
    tPPPIf             *pIf;
    tPPPIf             *pMemberIf;
    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if ((pIf != NULL) && (pIf->LinkInfo.IfIndex == Index))
        {
            if (pIf->BundleFlag != BUNDLE
                && pIf->MPInfo.MemberInfo.pBundlePtr != NULL
                && pIf->MPInfo.MemberInfo.pBundlePtr->pCCPIf != NULL)
            {
                return (NULL);
            }
            if (pIf->BundleFlag == BUNDLE)
            {
                SLL_SCAN (&PPPIfList, pMemberIf, tPPPIf *)
                {
                    if (pMemberIf->BundleFlag != BUNDLE
                        && pMemberIf->MPInfo.MemberInfo.pBundlePtr == pIf)
                    {
                        if (pMemberIf->pCCPIf != NULL)
                        {
                            return (NULL);
                        }
                    }
                }
            }

            if (pIf->pCCPIf == NULL)
            {
                if ((pIf->pCCPIf = (VOID *) CCPCreateIf (pIf)) == NULL)
                {
                    return (NULL);
                }
            }
            return (tCCPIf *) (pIf->pCCPIf);
        }
    }
    return (NULL);
}

/* LOW LEVEL Routines for Table : PppCCPConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppCCPConfigTable
 Input       :  The Indices
                PppCCPConfigIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppCCPConfigTable (INT4 i4PppCCPConfigIfIndex)
{
    if (NULL == SNMPGetCCPIfPtr ((UINT4) i4PppCCPConfigIfIndex))
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppCCPConfigTable
 Input       :  The Indices
                PppCCPConfigIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppCCPConfigTable (INT4 *pi4PppCCPConfigIfIndex)
{
    if (nmhGetFirstIndex (pi4PppCCPConfigIfIndex, CCPInstance, FIRST_INST) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppCCPConfigTable
 Input       :  The Indices
                PppCCPConfigIfIndex
                nextPppCCPConfigIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppCCPConfigTable (INT4 i4PppCCPConfigIfIndex,
                                  INT4 *pi4NextPppCCPConfigIfIndex)
{
    if (nmhGetNextIndex (&i4PppCCPConfigIfIndex, CCPInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextPppCCPConfigIfIndex = i4PppCCPConfigIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppCCPConfigAdminStatus
 Input       :  The Indices
                PppCCPConfigIfIndex

                The Object 
                retValPppCCPConfigAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppCCPConfigAdminStatus (INT4 i4PppCCPConfigIfIndex,
                               INT4 *pi4RetValPppCCPConfigAdminStatus)
{
    tCCPIf             *pCCPIf;

    if (NULL == (pCCPIf = SNMPGetCCPIfPtr ((UINT4) i4PppCCPConfigIfIndex)))
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppCCPConfigAdminStatus = pCCPIf->AdminStatus;
    return (SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppCCPConfigAdminStatus
 Input       :  The Indices
                PppCCPConfigIfIndex

                The Object 
                setValPppCCPConfigAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppCCPConfigAdminStatus (INT4 i4PppCCPConfigIfIndex,
                               INT4 i4SetValPppCCPConfigAdminStatus)
{
    tCCPIf             *pCCPIf;
    if (NULL ==
        (pCCPIf = SNMPGetOrCreateCCPIfPtr ((UINT4) i4PppCCPConfigIfIndex)))
    {
        return (SNMP_FAILURE);
    }

    pCCPIf->AdminStatus = (UINT1) i4SetValPppCCPConfigAdminStatus;
    if (i4SetValPppCCPConfigAdminStatus == ADMIN_OPEN)
    {
        CCPEnableIf (pCCPIf);
    }
    else
    {
        CCPDisableIf (pCCPIf);
    }
    return (SNMP_SUCCESS);

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppCCPConfigAdminStatus
 Input       :  The Indices
                PppCCPConfigIfIndex

                The Object 
                testValPppCCPConfigAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppCCPConfigAdminStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4PppCCPConfigIfIndex,
                                  INT4 i4TestValPppCCPConfigAdminStatus)
{
    PPP_UNUSED (i4PppCCPConfigIfIndex);
    if (i4TestValPppCCPConfigAdminStatus != ADMIN_OPEN
        && i4TestValPppCCPConfigAdminStatus != ADMIN_CLOSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/* LOW LEVEL Routines for Table : PppCCPTxConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppCCPTxConfigTable
 Input       :  The Indices
                PppCCPTxConfigIfIndex
                PppCCPTxConfigPriority
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppCCPTxConfigTable (INT4 i4PppCCPTxConfigIfIndex,
                                             INT4 i4PppCCPTxConfigPriority)
{
    tCCPIf             *pCCPIf;
    tHeader            *pHeader;

    if (NULL == (pCCPIf = SNMPGetCCPIfPtr ((UINT4) i4PppCCPTxConfigIfIndex)))
    {
        return (SNMP_FAILURE);
    }
    SLL_SCAN (&pCCPIf->OptionsDesired, pHeader, tHeader *)
    {
        if (pHeader->Priority == (UINT4) i4PppCCPTxConfigPriority)
        {
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppCCPTxConfigTable
 Input       :  The Indices
                PppCCPTxConfigIfIndex
                PppCCPTxConfigPriority
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppCCPTxConfigTable (INT4 *pi4PppCCPTxConfigIfIndex,
                                     INT4 *pi4PppCCPTxConfigPriority)
{
    if (nmhGetFirstDoubleIndex
        (pi4PppCCPTxConfigIfIndex, pi4PppCCPTxConfigPriority,
         (INT1) CCPTxInstance, FIRST_INST) == (SNMP_SUCCESS))
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppCCPTxConfigTable
 Input       :  The Indices
                PppCCPTxConfigIfIndex
                nextPppCCPTxConfigIfIndex
                PppCCPTxConfigPriority
                nextPppCCPTxConfigPriority
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppCCPTxConfigTable (INT4 i4PppCCPTxConfigIfIndex,
                                    INT4 *pi4NextPppCCPTxConfigIfIndex,
                                    INT4 i4PppCCPTxConfigPriority,
                                    INT4 *pi4NextPppCCPTxConfigPriority)
{
    if (nmhGetNextDoubleIndex
        (&i4PppCCPTxConfigIfIndex, &i4PppCCPTxConfigPriority,
         (INT1) CCPTxInstance, NEXT_INST) == SNMP_SUCCESS)
    {
        *pi4NextPppCCPTxConfigIfIndex = i4PppCCPTxConfigIfIndex;
        *pi4NextPppCCPTxConfigPriority = i4PppCCPTxConfigPriority;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppCCPTxConfigOptionNum
 Input       :  The Indices
                PppCCPTxConfigIfIndex
                PppCCPTxConfigPriority

                The Object 
                retValPppCCPTxConfigOptionNum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppCCPTxConfigOptionNum (INT4 i4PppCCPTxConfigIfIndex,
                               INT4 i4PppCCPTxConfigPriority,
                               INT4 *pi4RetValPppCCPTxConfigOptionNum)
{
    tCCPIf             *pCCPIf;
    tHeader            *pHeader;

    if (NULL == (pCCPIf = SNMPGetCCPIfPtr ((UINT4) i4PppCCPTxConfigIfIndex)))
    {
        return (SNMP_FAILURE);
    }
    SLL_SCAN (&pCCPIf->OptionsDesired, pHeader, tHeader *)
    {
        if (pHeader->Priority == (UINT4) i4PppCCPTxConfigPriority)
        {
            *pi4RetValPppCCPTxConfigOptionNum = pHeader->Type;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppCCPTxConfigOptionNum
 Input       :  The Indices
                PppCCPTxConfigIfIndex
                PppCCPTxConfigPriority

                The Object 
                setValPppCCPTxConfigOptionNum
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppCCPTxConfigOptionNum (INT4 i4PppCCPTxConfigIfIndex,
                               INT4 i4PppCCPTxConfigPriority,
                               INT4 i4SetValPppCCPTxConfigOptionNum)
{
    tCCPIf             *pCCPIf;
    tHeader            *pHeader;
    t_SLL_NODE         *pNextHeader;
#ifdef GZIP
    tGZIPParams        *pGzipPtr;
#endif
#ifdef MPPE
    tMPPEParams        *pMppePtr;
#endif
    UINT1               GenOptIndex;

    pCCPIf = NULL;
    pHeader = NULL;

    if (NULL ==
        (pCCPIf = SNMPGetOrCreateCCPIfPtr ((UINT4) i4PppCCPTxConfigIfIndex)))
    {
        return (SNMP_FAILURE);
    }

    pHeader = (tHeader *) SLL_FIRST (&pCCPIf->OptionsDesired);
    while (pHeader != NULL)
    {
        pNextHeader =
            SLL_NEXT (&pCCPIf->OptionsDesired, (t_SLL_NODE *) pHeader);

        if ((pHeader->Priority == (UINT4) i4PppCCPTxConfigPriority)
            && (pHeader->Type == i4SetValPppCCPTxConfigOptionNum))
        {
            return (SNMP_SUCCESS);
        }
        if ((pHeader->Priority == (UINT4) i4PppCCPTxConfigPriority)
            || (pHeader->Type == i4SetValPppCCPTxConfigOptionNum))
        {
            switch (pHeader->Type)
            {
                case OPTION_GZIP:
                    GenOptIndex =
                        (UINT1) (GCPGetOptIndex
                                 (&pCCPIf->CCPGSEM, OPTION_GZIP));
                    break;
                case OPTION_MPPE:
                    GenOptIndex =
                        (UINT1) (GCPGetOptIndex
                                 (&pCCPIf->CCPGSEM, OPTION_MPPE));
                    break;
                default:
                    return (SNMP_FAILURE);
            }
            pCCPIf->CCPGSEM.pNegFlagsPerIf[GenOptIndex].FlagMask &=
                DESIRED_NOT_SET;
            pCCPIf->CCPGSEM.pNegFlagsPerIf[GenOptIndex].FlagMask &=
                ACKED_BY_PEER_NOT_SET;
            SLL_DELETE (&pCCPIf->OptionsDesired, (t_SLL_NODE *) pHeader);
            MEM_FREE (pHeader);
            break;
        }
        pHeader = (tHeader *) pNextHeader;
    }

    if (i4SetValPppCCPTxConfigOptionNum != 0)
    {
        switch (i4SetValPppCCPTxConfigOptionNum)
        {
#ifdef GZIP
            case OPTION_GZIP:

                pGzipPtr = GzipCreateIf (DEFAULT_WINDOW_SIZE);
                pGzipPtr->Header.Priority = i4PppCCPTxConfigPriority;

                InsertNode (pCCPIf, &pGzipPtr->Header,
                            i4PppCCPTxConfigPriority);
                pCCPIf->CCPGSEM.pNegFlagsPerIf[GZIP_IDX].FlagMask |=
                    DESIRED_SET;
                break;
#endif
#ifdef MPPE
            case OPTION_MPPE:
                pMppePtr = MppeCreateIf ();
                if (pMppePtr != NULL)
                {
                    pMppePtr->Header.Priority = i4PppCCPTxConfigPriority;
                    InsertNode (pCCPIf, &pMppePtr->Header,
                                i4PppCCPTxConfigPriority);
                    pCCPIf->CCPGSEM.pNegFlagsPerIf[MPPE_IDX].FlagMask |=
                        DESIRED_SET;
                }
                break;
#endif
            default:
                return (SNMP_FAILURE);
        }
    }
    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppCCPTxConfigOptionNum
 Input       :  The Indices
                PppCCPTxConfigIfIndex
                PppCCPTxConfigPriority

                The Object 
                testValPppCCPTxConfigOptionNum
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppCCPTxConfigOptionNum (UINT4 *pu4ErrorCode,
                                  INT4 i4PppCCPTxConfigIfIndex,
                                  INT4 i4PppCCPTxConfigPriority,
                                  INT4 i4TestValPppCCPTxConfigOptionNum)
{
    PPP_UNUSED (i4PppCCPTxConfigIfIndex);
    PPP_UNUSED (i4PppCCPTxConfigPriority);
    switch (i4TestValPppCCPTxConfigOptionNum)
    {
        case OPTION_GZIP:
        case OPTION_MPPE:
        case 0:
            return (SNMP_SUCCESS);
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (SNMP_FAILURE);
    }
}

/* LOW LEVEL Routines for Table : PppCCPRxConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppCCPRxConfigTable
 Input       :  The Indices
                PppCCPRxConfigIfIndex
                PppCCPRxConfigOptionNum
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppCCPRxConfigTable (INT4 i4PppCCPRxConfigIfIndex,
                                             INT4 i4PppCCPRxConfigOptionNum)
{
    tCCPIf             *pCCPIf;
    tHeader            *pHeader;

    if (NULL == (pCCPIf = SNMPGetCCPIfPtr ((UINT4) i4PppCCPRxConfigIfIndex)))
    {
        return (SNMP_FAILURE);
    }
    SLL_SCAN (&pCCPIf->OptionsAllowedForPeer, pHeader, tHeader *)
    {
        if (pHeader->Type == i4PppCCPRxConfigOptionNum)
        {
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppCCPRxConfigTable
 Input       :  The Indices
                PppCCPRxConfigIfIndex
                PppCCPRxConfigOptionNum
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppCCPRxConfigTable (INT4 *pi4PppCCPRxConfigIfIndex,
                                     INT4 *pi4PppCCPRxConfigOptionNum)
{
    if (nmhGetFirstDoubleIndex
        (pi4PppCCPRxConfigIfIndex, pi4PppCCPRxConfigOptionNum,
         (INT1) CCPRxInstance, FIRST_INST) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppCCPRxConfigTable
 Input       :  The Indices
                PppCCPRxConfigIfIndex
                nextPppCCPRxConfigIfIndex
                PppCCPRxConfigOptionNum
                nextPppCCPRxConfigOptionNum
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppCCPRxConfigTable (INT4 i4PppCCPRxConfigIfIndex,
                                    INT4 *pi4NextPppCCPRxConfigIfIndex,
                                    INT4 i4PppCCPRxConfigOptionNum,
                                    INT4 *pi4NextPppCCPRxConfigOptionNum)
{
    if (nmhGetNextDoubleIndex
        (&i4PppCCPRxConfigIfIndex, &i4PppCCPRxConfigOptionNum,
         (INT1) CCPRxInstance, NEXT_INST) == SNMP_SUCCESS)
    {
        *pi4NextPppCCPRxConfigIfIndex = i4PppCCPRxConfigIfIndex;
        *pi4NextPppCCPRxConfigOptionNum = i4PppCCPRxConfigOptionNum;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppCCPRxConfigAdminStatus
 Input       :  The Indices
                PppCCPRxConfigIfIndex
                PppCCPRxConfigOptionNum

                The Object 
                retValPppCCPRxConfigAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppCCPRxConfigAdminStatus (INT4 i4PppCCPRxConfigIfIndex,
                                 INT4 i4PppCCPRxConfigOptionNum,
                                 INT4 *pi4RetValPppCCPRxConfigAdminStatus)
{
    tCCPIf             *pCCPIf;
    tHeader            *pHeader;

    if (NULL == (pCCPIf = SNMPGetCCPIfPtr ((UINT4) i4PppCCPRxConfigIfIndex)))
    {
        return (SNMP_FAILURE);
    }
    SLL_SCAN (&pCCPIf->OptionsAllowedForPeer, pHeader, tHeader *)
    {
        if (pHeader->Type == i4PppCCPRxConfigOptionNum)
        {
            *pi4RetValPppCCPRxConfigAdminStatus = CCP_RX_ADMIN_STATUS_OPENED;
            return (SNMP_SUCCESS);
        }
    }
    *pi4RetValPppCCPRxConfigAdminStatus = CCP_RX_ADMIN_STATUS_CLOSED;
    return (SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppCCPRxConfigAdminStatus
 Input       :  The Indices
                PppCCPRxConfigIfIndex
                PppCCPRxConfigOptionNum

                The Object 
                setValPppCCPRxConfigAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppCCPRxConfigAdminStatus (INT4 i4PppCCPRxConfigIfIndex,
                                 INT4 i4PppCCPRxConfigOptionNum,
                                 INT4 i4SetValPppCCPRxConfigAdminStatus)
{
    tCCPIf             *pCCPIf;
    tHeader            *pHeader;
#ifdef GZIP
    tGZIPParams        *pGzipPtr;
#endif
#ifdef MPPE
    tMPPEParams        *pMppePtr;
#endif
    UINT1               GenOptIndex;
    pCCPIf = NULL;
    pHeader = NULL;

    if (NULL ==
        (pCCPIf = SNMPGetOrCreateCCPIfPtr ((UINT4) i4PppCCPRxConfigIfIndex)))
    {
        return (SNMP_FAILURE);
    }
    if (i4SetValPppCCPRxConfigAdminStatus == CCP_RX_ADMIN_STATUS_OPENED)
    {
        SLL_SCAN (&pCCPIf->OptionsAllowedForPeer, pHeader, tHeader *)
        {
            if (pHeader->Type == i4PppCCPRxConfigOptionNum)
            {
                return (SNMP_SUCCESS);
            }
        }
        switch (i4PppCCPRxConfigOptionNum)
        {
#ifdef GZIP
            case OPTION_GZIP:
                pGzipPtr = GzipCreateIf (DEFAULT_WINDOW_SIZE);
                SLL_ADD (&pCCPIf->OptionsAllowedForPeer,
                         (t_SLL_NODE *) & pGzipPtr->Header);
                pCCPIf->CCPGSEM.pNegFlagsPerIf[GZIP_IDX].FlagMask |=
                    ALLOWED_FOR_PEER_SET;
                break;
#endif
#ifdef MPPE
            case OPTION_MPPE:
                pMppePtr = MppeCreateIf ();
                SLL_ADD (&pCCPIf->OptionsAllowedForPeer,
                         (t_SLL_NODE *) & pMppePtr->Header);
                pCCPIf->CCPGSEM.pNegFlagsPerIf[GZIP_IDX].FlagMask |=
                    ALLOWED_FOR_PEER_SET;
                break;
#endif
            default:
                break;
        }
    }
    else
    {
        /* SLL_DELETE inside SLL_SCAN will not create a problem here because
         * SLL_DELETE is called only once and the SCAN loop is broken
         * immediately - Mahesh */
        SLL_SCAN (&pCCPIf->OptionsAllowedForPeer, pHeader, tHeader *)
        {
            if (pHeader->Type == i4PppCCPRxConfigOptionNum)
            {
                SLL_DELETE (&pCCPIf->OptionsAllowedForPeer,
                            (t_SLL_NODE *) pHeader);
                GenOptIndex =
                    (UINT1) (GCPGetOptIndex (&pCCPIf->CCPGSEM,
                                             (UINT1)
                                             (i4PppCCPRxConfigOptionNum)));
                pCCPIf->CCPGSEM.pNegFlagsPerIf[GenOptIndex].FlagMask &=
                    ALLOWED_FOR_PEER_NOT_SET;
                break;
            }
        }
    }
    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppCCPRxConfigAdminStatus
 Input       :  The Indices
                PppCCPRxConfigIfIndex
                PppCCPRxConfigOptionNum

                The Object 
                testValPppCCPRxConfigAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppCCPRxConfigAdminStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4PppCCPRxConfigIfIndex,
                                    INT4 i4PppCCPRxConfigOptionNum,
                                    INT4 i4TestValPppCCPRxConfigAdminStatus)
{
    PPP_UNUSED (i4PppCCPRxConfigIfIndex);
    PPP_UNUSED (i4PppCCPRxConfigOptionNum);
    if (i4TestValPppCCPRxConfigAdminStatus != CCP_RX_ADMIN_STATUS_OPENED
        && i4TestValPppCCPRxConfigAdminStatus != CCP_RX_ADMIN_STATUS_CLOSED)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/* LOW LEVEL Routines for Table : PppCCPStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppCCPStatsTable
 Input       :  The Indices
                PppCCPStatsIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppCCPStatsTable (INT4 i4PppCCPStatsIfIndex)
{
    if (NULL == SNMPGetCCPIfPtr ((UINT4) i4PppCCPStatsIfIndex))
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppCCPStatsTable
 Input       :  The Indices
                PppCCPStatsIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppCCPStatsTable (INT4 *pi4PppCCPStatsIfIndex)
{
    if (nmhGetFirstIndex (pi4PppCCPStatsIfIndex, CCPInstance, FIRST_INST) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppCCPStatsTable
 Input       :  The Indices
                PppCCPStatsIfIndex
                nextPppCCPStatsIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppCCPStatsTable (INT4 i4PppCCPStatsIfIndex,
                                 INT4 *pi4NextPppCCPStatsIfIndex)
{
    if (nmhGetNextIndex (&i4PppCCPStatsIfIndex, CCPInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextPppCCPStatsIfIndex = i4PppCCPStatsIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppCCPStatsOperStatus
 Input       :  The Indices
                PppCCPStatsIfIndex

                The Object 
                retValPppCCPStatsOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppCCPStatsOperStatus (INT4 i4PppCCPStatsIfIndex,
                             INT4 *pi4RetValPppCCPStatsOperStatus)
{
    tCCPIf             *pCCPIf;

    if (NULL == (pCCPIf = SNMPGetCCPIfPtr ((UINT4) i4PppCCPStatsIfIndex)))
    {
        return (SNMP_FAILURE);
    }
    if (pCCPIf->OperStatus == STATUS_UP)
    {
        *pi4RetValPppCCPStatsOperStatus = 1;
    }
    else
    {
        *pi4RetValPppCCPStatsOperStatus = 2;
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppCCPStatsRxResetReqs
 Input       :  The Indices
                PppCCPStatsIfIndex

                The Object 
                retValPppCCPStatsRxResetReqs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppCCPStatsRxResetReqs (INT4 i4PppCCPStatsIfIndex,
                              INT4 *pi4RetValPppCCPStatsRxResetReqs)
{
    tCCPIf             *pCCPIf;

    if (NULL == (pCCPIf = SNMPGetCCPIfPtr ((UINT4) i4PppCCPStatsIfIndex)))
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppCCPStatsRxResetReqs = pCCPIf->Status.RxResetReqs;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppCCPStatsTxResetReqs
 Input       :  The Indices
                PppCCPStatsIfIndex

                The Object 
                retValPppCCPStatsTxResetReqs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppCCPStatsTxResetReqs (INT4 i4PppCCPStatsIfIndex,
                              INT4 *pi4RetValPppCCPStatsTxResetReqs)
{
    tCCPIf             *pCCPIf;

    if (NULL == (pCCPIf = SNMPGetCCPIfPtr ((UINT4) i4PppCCPStatsIfIndex)))
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppCCPStatsTxResetReqs = pCCPIf->Status.TxResetReqs;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppCCPStatsRxResetAcks
 Input       :  The Indices
                PppCCPStatsIfIndex

                The Object 
                retValPppCCPStatsRxResetAcks
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppCCPStatsRxResetAcks (INT4 i4PppCCPStatsIfIndex,
                              INT4 *pi4RetValPppCCPStatsRxResetAcks)
{
    tCCPIf             *pCCPIf;

    if (NULL == (pCCPIf = SNMPGetCCPIfPtr ((UINT4) i4PppCCPStatsIfIndex)))
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppCCPStatsRxResetAcks = pCCPIf->Status.RxResetAcks;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppCCPStatsTxResetAcks
 Input       :  The Indices
                PppCCPStatsIfIndex

                The Object 
                retValPppCCPStatsTxResetAcks
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppCCPStatsTxResetAcks (INT4 i4PppCCPStatsIfIndex,
                              INT4 *pi4RetValPppCCPStatsTxResetAcks)
{
    tCCPIf             *pCCPIf;

    if (NULL == (pCCPIf = SNMPGetCCPIfPtr ((UINT4) i4PppCCPStatsIfIndex)))
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppCCPStatsTxResetAcks = pCCPIf->Status.TxResetAcks;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppCCPStatsTxNegotiatedOption
 Input       :  The Indices
                PppCCPStatsIfIndex

                The Object 
                retValPppCCPStatsTxNegotiatedOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppCCPStatsTxNegotiatedOption (INT4 i4PppCCPStatsIfIndex,
                                     INT4
                                     *pi4RetValPppCCPStatsTxNegotiatedOption)
{
    tCCPIf             *pCCPIf;
    tHeader            *pHeader;

    if (NULL == (pCCPIf = SNMPGetCCPIfPtr ((UINT4) i4PppCCPStatsIfIndex)))
    {
        return (SNMP_FAILURE);
    }
    pHeader = (tHeader *) (TMO_SLL_First ((&pCCPIf->OptionsAckedByLocal)));
    if (pHeader != NULL)
    {
        *pi4RetValPppCCPStatsTxNegotiatedOption = pHeader->Type;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetPppCCPStatsRxNegotiatedOption
 Input       :  The Indices
                PppCCPStatsIfIndex

                The Object 
                retValPppCCPStatsRxNegotiatedOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppCCPStatsRxNegotiatedOption (INT4 i4PppCCPStatsIfIndex,
                                     INT4
                                     *pi4RetValPppCCPStatsRxNegotiatedOption)
{
    tCCPIf             *pCCPIf;
    tHeader            *pHeader;

    if (NULL == (pCCPIf = SNMPGetCCPIfPtr ((UINT4) i4PppCCPStatsIfIndex)))
    {
        return (SNMP_FAILURE);
    }
    pHeader = (tHeader *) (TMO_SLL_First ((&pCCPIf->OptionsAckedByPeer)));
    if (pHeader != NULL)
    {
        *pi4RetValPppCCPStatsRxNegotiatedOption = pHeader->Type;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

VOID
InsertNode (tCCPIf * pCCPIf, tHeader * pCurrentHeader, UINT4 Priority)
{
    tHeader            *pHeader;
    t_SLL_NODE         *pPrevHeader;

    SLL_SCAN (&pCCPIf->OptionsDesired, pHeader, tHeader *)
    {
        if (Priority < pHeader->Priority)
        {
            break;
        }
    }
    pPrevHeader = SLL_PREV (&pCCPIf->OptionsDesired, (t_SLL_NODE *) pHeader);
    SLL_INSERT (&pCCPIf->OptionsDesired, (t_SLL_NODE *) pPrevHeader,
                (t_SLL_NODE *) pCurrentHeader);
}
