/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppccp.c,v 1.5 2011/10/13 10:31:19 siva Exp $
 *
 * Description:This file contains callback routines of CCP 
 *             module.It also contains routines for the input 
 *             CCP packet processing.
 *
 *******************************************************************/
#include "ccpcom.h"
#include "gcpdefs.h"
#include "lcpdefs.h"
#include "gsemdefs.h"
#include "pppmppeif.h"
#ifdef GZIP
#include "pppgzif.h"
#include "gzip.h"
#include "gzipprot.h"
#endif
#include  "pppccpmd.h"
#include "mppeprot.h"
#include "genexts.h"
#include "pppccpmd.h"
#include "globexts.h"
#include "genproto.h"

/********** SKR004 ********* commented after making it as part of CCPIf ***/

UINT2               CCPAllocCounter;
UINT2               CCPIfFreeCounter;

tCCPFNPOINTER       CCPFnPointer[] = {
    {
     OPTION_GZIP,
     0, 0,
#ifdef GZIP
     GzipHistoryInit,
     GzipCreateHistory,
     GzipDeleteHistory,
     GzipValidateResetPkt,
     GzipHandleResetPkt,
     GzipDeCompress,
     GzipCompress,
     GzipConstructResetPkt
#else
     NULL,
     NULL,
     NULL,
     NULL,
     NULL,
     NULL,
     NULL,
     NULL,
#endif
     },
#ifdef MPPE
    {
     OPTION_MPPE,
     0, 0,
     MppeHistoryInit,
     MppeCreateHistory,
     MppeDeleteHistory,
     NULL,
     NULL,
     MppeDeCrypt,
     MppeCrypt,
     NULL}
#endif
};

tRangeInfo          StacRange = { {0}
, {65535}
};
tRangeInfo          GZIPRange = { {0x100}
, {0x2000}
};
tRangeInfo          MPPERange = { {0x00000000}
, {0x010000f1}
};
tOptHandlerFuns     GZIPHandlingFuns = {
#ifdef GZIP
    CCPProcessGZIPConfReq,
    CCPProcessGZIPConfNak,
    CCPProcessConfRej,
    CCPReturnGZIPAddrPtr,
#else
    NULL,
    NULL,
    NULL,
    NULL,
#endif
    NULL
};
tOptHandlerFuns     MPPEHandlingFuns = {
#ifdef MPPE
    CCPProcessMPPEConfReq,
    CCPProcessMPPEConfNak,
#else
    NULL,
    NULL,
#endif
    CCPProcessConfRej,
#ifdef MPPE
    CCPReturnMPPEAddrPtr,
#else
    NULL,
#endif
    NULL
};

/* Makefile changes - <missing braces> */

tGenOptionInfo      CCPGenOptionInfo[] = {
    {OPTION_GZIP, RANGE, BYTE_LEN_2, 4, &GZIPHandlingFuns, {&GZIPRange}
     , NOT_SET, 0, 0},            /* SRINI */
    {OPTION_MPPE, RANGE, BYTE_LEN_4, 6, &MPPEHandlingFuns, {&MPPERange}
     , NOT_SET, 0, 0}
};

tOptNegFlagsPerIf   CCPOptPerIntf[] = {
    {OPTION_GZIP, ALLOW_FORCED_NAK_SET | ALLOW_REJECT_SET, 0}
    ,
    {OPTION_MPPE,
     ALLOW_FORCED_NAK_SET | ALLOW_REJECT_SET | ALLOWED_FOR_PEER_SET |
     DESIRED_SET, 0}
};

tGSEMCallbacks      CCPGSEMCallback = {
    CCPUp,
    CCPDown,
    NULL,
    NULL,
    CCPRxData,
    CCPTxData,
    CCPProcessResetReq,
    CCPProcessResetAck
};

/***********************************************************************/
/*                      INTERFACE  FUNCTIONS                           */
/***********************************************************************
*  Function Name : CCPCreateIf
*  Description   :
*          This procedure creates an entry in the CCP interface data 
*  base corresponding to IfId. It also initializes the created entry to the 
*  default values  by calling the CCPinit() function.
*  Parameter(s)  :
*         pIf -    pointer to the PPP interface structure
*  Return Values : 
*      pointer to the newly created  interface structure , if the creation is 
*        success
*      NULL if it fails to create the interface.
*********************************************************************/
tCCPIf             *
CCPCreateIf (tPPPIf * pIf)
{
    tCCPIf             *pCCPIf;

    if ((pCCPIf = (tCCPIf *) ALLOC_CCP_IF ()) != NULL)
    {
        if (pIf->BundleFlag == BUNDLE)
        {
            pIf->LcpStatus.CCPEnableFlag = CCP_ENABLED;
            pCCPIf->CCPGSEM.Protocol = CCP_OVER_INDIVIDUAL_LINK;
        }
        else if (pIf->MPInfo.MemberInfo.pBundlePtr != NULL)
        {
            PPP_DBG ("With MP - CCP Over a Member Link...");
            pIf->LcpStatus.CCPEnableFlag = CCP_ENABLED;
            pCCPIf->CCPGSEM.Protocol = CCP_OVER_MEMBER_LINK;
        }
        else
        {
            pIf->LcpStatus.CCPEnableFlag = CCP_ENABLED;
            pCCPIf->CCPGSEM.Protocol = CCP_OVER_INDIVIDUAL_LINK;
        }
        CCPInit (pCCPIf);
        pCCPIf->CCPGSEM.pIf = pIf;
        pIf->pCCPIf = (VOID *) pCCPIf;
        return (pCCPIf);
    }
    else
    {
        PPP_TRC (ALL_FAILURE, "Error: Unable to alloc. mem. for CCP entry .");
    }

    return (NULL);
}

/*********************************************************************
*  Function Name : CCPDeleteIf
*  Description   :
*               This procedure deletes an CCP interface  entry from the 
*  database and deallocates memory corresponding to the interface.
*  Parameter(s)  :
*         pCCPPtr -  points to  the CCP interface structure to be deleted.
*  Return Values : VOID
*********************************************************************/
VOID
CCPDeleteIf (tCCPIf * pCCPIf)
{

    DeleteNodes (&pCCPIf->OptionsDesired);
    DeleteNodes (&pCCPIf->OptionsAckedByPeer);
    DeleteNodes (&pCCPIf->OptionsAllowedForPeer);
    DeleteNodes (&pCCPIf->OptionsAckedByLocal);

    pCCPIf->CCPGSEM.pIf->pCCPIf = NULL;

    GSEMDelete (&(pCCPIf->CCPGSEM));

    FREE_CCP_PTR (pCCPIf);

}

/*********************************************************************
*  Function Name : CCPEnableIf
*  Description   :
*              This function is called when there is an  CCP Admin Open 
*  for an interface from the SNMP It  triggers an OPEN event to the GSEM and 
*  passes the CCP tGSEM corresponds to this interface as parameter.
*  Parameter(s)  :
*            pCCP    -  points to the CCP interface structure to be enabled
*  Return Values : VOID
*********************************************************************/
VOID
CCPEnableIf (tCCPIf * pCCP)
{
    tPPPIf             *pIf;

    if (pCCP->CCPGSEM.pIf->BundleFlag == BUNDLE)
    {
        SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
        {
            if (pIf->MPInfo.MemberInfo.pBundlePtr != NULL
                && pIf->MPInfo.MemberInfo.pBundlePtr == pCCP->CCPGSEM.pIf)
            {
                pIf->pCCPIf = pCCP;
            }
        }
    }
    pCCP->AdminStatus = ADMIN_OPEN;
    pCCP->CCPCurrentIndex = 0;

    DeleteNodes (&pCCP->OptionsAckedByPeer);
    CCPCopyDesiredToAckedByPeer (pCCP);

    /* Check whether the link is already in the Operational state. 
       If so, then indicate the GSEM about the  link UP 
     */

    if (pCCP->CCPGSEM.pIf->LcpStatus.LinkAvailability == LINK_AVAILABLE)
    {

        PPP_DBG ("Upping SEM \n");
        GSEMRun (&pCCP->CCPGSEM, PPP_UP);
    }

    PPP_DBG ("Opening SEM");
    GSEMRun (&pCCP->CCPGSEM, OPEN);

    GSEMRestartIfNeeded (&pCCP->CCPGSEM);

    return;
}

/*********************************************************************
*  Function Name : CCPDisableIf
*  Description   :
*          This function is called when there is an Admin Close for an 
*  interface from the SNMP It triggers a CLOSE event to the GSEM and passes 
*  the CCP GSEMStructure corresponds to this interface as a parameter. 
*  Parameter(s)  :
*            pCCP - points to  the CCP interface structure to be  disabled
*  Return Values : VOID
*********************************************************************/
VOID
CCPDisableIf (tCCPIf * pCCP)
{
    pCCP->AdminStatus = ADMIN_CLOSE;
    GSEMRun (&pCCP->CCPGSEM, CLOSE);
    return;
}

/*********************************************************************
*  Function Name : CCPInit
*  Description   :
*              This function initializes the CCP GSEM and  sets 
*  the  CCP configuration option variables to their default values. It is 
*  invoked by the CCPCreateIf() function.
*  Parameter(s)  :
*        pCCPPtr -   points to the CCP interface structure
*  Return Values : VOID
*********************************************************************/
VOID
CCPInit (tCCPIf * pCCPIf)
{
    StacRange.MinVal.ShortVal = 0;
    StacRange.MaxVal.ShortVal = 0xffff;
    GZIPRange.MinVal.ShortVal = 0x100;
    GZIPRange.MaxVal.ShortVal = 0x2000;

    /* Initialize the Option fields */

    OptionInit (&pCCPIf->OptionsDesired);
    OptionInit (&pCCPIf->OptionsAllowedForPeer);
    OptionInit (&pCCPIf->OptionsAckedByPeer);
    OptionInit (&pCCPIf->OptionsAckedByLocal);

    pCCPIf->AdminStatus = STATUS_DOWN;
    pCCPIf->OperStatus = STATUS_DOWN;

    GSEMInit (&pCCPIf->CCPGSEM, MAX_CCP_OPTIONS);
    pCCPIf->CCPGSEM.pCallbacks = &CCPGSEMCallback;
    pCCPIf->CCPGSEM.pGenOptInfo = CCPGenOptionInfo;
    pCCPIf->pInCompIf = pCCPIf->pOutCompIf = NULL;
    MEMCPY (pCCPIf->CCPGSEM.pNegFlagsPerIf, &CCPOptPerIntf,
            MEM_MAX_BYTES ((sizeof (CCPOptPerIntf) /
                            sizeof (tOptNegFlagsPerIf)),
                           (sizeof (tOptNegFlagsPerIf) *
                            pCCPIf->CCPGSEM.MaxOptTypes)));
    return;
}

/**************************************************************************
*  Function Name : CCPRxData
*  Description   :
*              This function handles the incoming compressaed data. It
*  decompresses the data and forwards to the HL, if there is no error
*  in decompression. If there are any error in the decompression, it
*    sends RESET_REQ to the peer.
*
*  Parameter(s)  :
*            pGSEM            - Pointer to GSEM structure.
*
*  Return Values : VOID
***************************************************************************/
VOID
CCPRxData (tGSEM * pGSEM)
{
    INT2                RetVal;
    UINT2               Length;
    INT2                Index;
    tHeader            *pHeader;
    tCCPIf             *pCCPIf;

    pCCPIf = (tCCPIf *) pGSEM->pIf->pCCPIf;

    pHeader = (tHeader *) SLL_FIRST (&pCCPIf->OptionsAckedByPeer);

    if (pHeader == NULL)
    {
        return;
    }

    if ((Index = GetCCPOptionIndex (pHeader->Type)) == DISCARD_OPT)
    {

        PPP_DBG ("Invalid Type in CCPRxData()..., Returning");
        return;
    }

    if (Index >= (INT2) (sizeof (CCPFnPointer) / sizeof (tCCPFNPOINTER)))
    {
        return;
    }

    Length = (UINT2) VALID_BYTES (pGSEM->pInParam);
    RetVal =
        (*CCPFnPointer[Index].Decompress) (&pGSEM->pInParam, pCCPIf->pInCompIf);

/*
 *RetVal =
        (*CCPFnPointer[Index].Decompress) (&pGSEM->pInParam, pCCPIf->pInCompIf,
                                           pHeader);

*/

    if (RetVal == ERROR || RetVal == DISCARD_CCP_PKT)
    {

        if ((pGSEM->pOutParam =
             (t_MSG_DESC *) ALLOCATE_BUFFER (MAX_PKT_SIZE,
                                             DATA_OFFSET)) != NULL)
        {

            if (RetVal == ERROR)
            {

                pCCPIf->Status.Id++;
            }

            Length =
                (*CCPFnPointer[Index].ConstructResetReqPkt) (pGSEM->pOutParam,
                                                             pCCPIf->pInCompIf,
                                                             pHeader,
                                                             pGSEM->MiscParam.
                                                             ShortValue);

            MOVE_BACK_PTR (pGSEM->pOutParam, CP_HDR_LEN);
            FILL_PKT_HEADER (pGSEM->pOutParam, RESET_REQ_CODE,
                             pCCPIf->Status.Id, (UINT2) (Length + CP_HDR_LEN));

            PPP_TRC (ALL_FAILURE, "Decryption Failed. Sending Reset_Req.... ");

            pCCPIf->pResetReq = (t_MSG_DESC *) DUPLICATE_BUFFER (pGSEM->pOutParam, TRUE);    /* Copy the the content of the Reset Request into the buffer so that the buffer can be compared with the Reset Ack buffer while validating */

            pCCPIf->Status.TxResetReqs++;    /* To maintain the stats. of
                                               the Txmitted reset pkts. */

            PPPLLITxPkt (pGSEM->pIf, pGSEM->pOutParam,
                         (UINT2) (VALID_BYTES (pGSEM->pOutParam)),
                         pGSEM->Protocol);

            pGSEM->MiscParam.CharValue = PKT_DISCARDED;

        }
    }
    else
    {

        pGSEM->MiscParam.CharValue = PKT_NOT_DISCARDED;
    }

}

/**************************************************************************
*  Function Name : CCPTxData
*  Description   :
*                This function receives the data from the higher layer, 
*                compresses and sends to the Lower Layer.
*  Parameter(s)  :
*            pGSEM            - Pointer to GSEM structure.
*
*  Return Values : VOID
***************************************************************************/
VOID
CCPTxData (tGSEM * pGSEM)
{
    tHeader            *pHeader;
    INT2                Index;
    tCCPIf             *pCCPIf;

    pCCPIf = (tCCPIf *) pGSEM->pIf->pCCPIf;
    pGSEM->MiscParam.CharValue = UNCOMPRESSED;

    if ((pHeader = (tHeader *) SLL_FIRST (&pCCPIf->OptionsAckedByLocal)) ==
        NULL)
    {

        PPP_DBG ("No Compression Algo. negotiated");
        return;
    }

    if ((Index = GetCCPOptionIndex (pHeader->Type)) == DISCARD_OPT)
    {

        return;
    }
    if (Index >= (INT2) (sizeof (CCPFnPointer) / sizeof (tCCPFNPOINTER)))
    {
        return;
    }

    if ((*CCPFnPointer[Index].Compress)
        (&pGSEM->pOutParam, pCCPIf->pOutCompIf) == DISCARD)

        /*
         *
         *if ((*CCPFnPointer[Index].Compress)
         (&pGSEM->pOutParam, pCCPIf->pOutCompIf, pHeader,
         pGSEM->MiscParam.ShortValue) == DISCARD)

         */

    {

        PPP_DBG ("Compression Failed ...");
        return;
    }
    pGSEM->MiscParam.CharValue = COMPRESSED;
    PPP_DBG ("Compression Successful...");

    return;
}

/**************************************************************************
*  Function Name : CCPRecdResetReq 
*  Description   :
*                This function return RRR event to the GCP module if the 
*                Reset_Req packet validation is successful.
*  Parameter(s)  :
*            pGSEM            - Pointer to GSEM structure.
*            pIn                - Incoming Buffer
*            Length            - Length of the Buffer
*            Id                - Identifier of the incoming packet
*
*  Return Values : INT1
***************************************************************************/
INT1
CCPRecdResetReq (tGSEM * pGSEM, t_MSG_DESC * pIn, UINT2 Length, UINT1 Id)
{

    UINT1               Type;
    INT2                Index;
    tHeader            *pHeader;
    tCCPIf             *pCCPIf;

    pCCPIf = (VOID *) pGSEM->pIf->pCCPIf;
    pHeader = (tHeader *) SLL_FIRST (&pCCPIf->OptionsAckedByLocal);

    if (pHeader == NULL)
    {
        return DISCARD_OPT;
    }

    Type = pHeader->Type;
    pCCPIf->Status.RxResetReqs++;

    if ((Index = GetCCPOptionIndex (Type)) != DISCARD_OPT)
    {

        /* Makefile changes - <too few arguments> */

        if ((*CCPFnPointer[Index].ValidateResetPkt) (Length, RESET_REQ_CODE) ==
            OK)
        {

            pGSEM->pInParam = pIn;
            pGSEM->MiscParam.CharValue = Id;
            return RRR;

        }
    }

    PPP_DBG (" Returning DISCARD_OPT...");
    return DISCARD_OPT;
}

/**************************************************************************
*  Function Name : CCPProcessResetReq
*  Description   :
*                This function receives the reset request, processes it and
*                sends the acknowledgement to the peer.
*  Parameter(s)  :
*            pGSEM            - Pointer to GSEM structure.
*
*  Return Values : VOID
***************************************************************************/
VOID
CCPProcessResetReq (tGSEM * pGSEM)
{
    tHeader            *pHeader;
    tCCPIf             *pCCPIf;
    INT2                Index;

    pCCPIf = (tCCPIf *) pGSEM->pIf->pCCPIf;
    pHeader = (tHeader *) SLL_FIRST (&pCCPIf->OptionsAckedByLocal);

    if (pHeader == NULL)
    {
        return;
    }

    if ((Index = GetCCPOptionIndex (pHeader->Type)) == DISCARD_OPT)
    {

        return;
    }
    if (Index >= (INT2) (sizeof (CCPFnPointer) / sizeof (tCCPFNPOINTER)))
    {
        return;
    }

    (*CCPFnPointer[Index].ResetInfo) (pCCPIf->pOutCompIf,
                                      pGSEM->MiscParam.ShortValue);

    MOVE_OFFSET (pGSEM->pInParam,
                 pGSEM->pIf->LinkInfo.RxAddrCtrlLen +
                 pGSEM->pIf->LinkInfo.RxProtLen);

    ASSIGN1BYTE (pGSEM->pInParam, 0, RESET_ACK_CODE);
    pGSEM->pOutParam = pGSEM->pInParam;
    ReleaseFlag = PPP_NO;

    PPPLLITxPkt (pGSEM->pIf, pGSEM->pOutParam,
                 (UINT2) (VALID_BYTES (pGSEM->pOutParam)), pGSEM->Protocol);
    pCCPIf->Status.TxResetAcks++;

}

/**************************************************************************
*  Function Name : CCPRecdResetAck 
*  Description   :
*                This function return RRA event to the GCP module if the
*                packet validation is successfull.
*  Parameter(s)  :
*            pGSEM            - Pointer to GSEM structure.
*            pIn                - Incoming Buffer
*            Length            - Length of the incoming buffer
*            Id                - Identifier of the incoming buffer
*
*  Return Values : INT1
***************************************************************************/
INT1
CCPRecdResetAck (tGSEM * pGSEM, t_MSG_DESC * pIn, UINT2 Length, UINT1 Id)
{
    UINT1               Type;
    INT2                Index;
    UINT2               Offset;
    tHeader            *pHeader;
    tCCPIf             *pCCPIf;

    pCCPIf = (tCCPIf *) pGSEM->pIf->pCCPIf;

    if (pCCPIf->Status.Id == Id)
    {

        /* Makefile changes - <stmt has no effect> */
        /* RxPkt should be incremented for each Rx reset packet received
         * from the peer -Mani*/
        pCCPIf->Status.RxResetAcks++;

        pHeader = (tHeader *) SLL_FIRST (&pCCPIf->OptionsAckedByLocal);
        if (pHeader == NULL)
        {
            return DISCARD_OPT;
        }

        Type = pHeader->Type;

        if ((Index = GetCCPOptionIndex (Type)) != DISCARD_OPT)
        {

            /* Makefile changes - <too few arguments> */
            if (Index >=
                (INT2) (sizeof (CCPFnPointer) / sizeof (tCCPFNPOINTER)))
            {
                return FAILURE;
            }

            if ((*CCPFnPointer[Index].ValidateResetPkt) (Length, RESET_ACK_CODE)
                == DISCARD_OPT)
            {

                return DISCARD_OPT;

            }

            Offset =
                (UINT2) (pGSEM->pIf->LinkInfo.RxAddrCtrlLen +
                         pGSEM->pIf->LinkInfo.RxProtLen + CP_HDR_LEN);

            if (((Length == 0)
                 && ((VALID_BYTES (pCCPIf->pResetReq) - CP_HDR_LEN) == 0))
                ||
                (COMPARE_BUFS
                 (pIn, (UINT4) Offset, pCCPIf->pResetReq, (UINT4) CP_HDR_LEN,
                  (UINT4) (VALID_BYTES (pIn) - Offset)) == 0))
            {

                PPP_DBG ("Returning RRA...");
                return RRA;
            }

            PPP_TRC (ALL_FAILURE, "COMPARE_BUFS FAILED");
        }
    }

    PPP_DBG ("Returning DISCARD_OPT...");
    return DISCARD_OPT;
}

/**************************************************************************
*  Function Name : CCPProcessResetAck
*  Description   :
*                This function processes the received Reset_Ack packet.
*
*  Parameter(s)  :
*            pGSEM        - Pointer to OptionAckedBye.
*
*  Return Values : UINT1
***************************************************************************/
VOID
CCPProcessResetAck (tGSEM * pGSEM)
{
    tHeader            *pHeader;
    tCCPIf             *pCCPIf;
    INT2                Index;

    pCCPIf = (tCCPIf *) pGSEM->pIf->pCCPIf;
    pHeader = (tHeader *) SLL_FIRST (&pCCPIf->OptionsAckedByPeer);

    if (pHeader == NULL)
    {
        return;
    }

    if ((Index = GetCCPOptionIndex (pHeader->Type)) == DISCARD_OPT)
    {

        return;
    }
    if (Index >= (INT2) (sizeof (CCPFnPointer) / sizeof (tCCPFNPOINTER)))
    {
        return;
    }

    (*CCPFnPointer[Index].ResetInfo) (pCCPIf->pInCompIf,
                                      pGSEM->MiscParam.ShortValue);

}

/**************************************************************************
*  Function Name : GetCCPOptionIndex 
*  Description   :
*                This function searches the FnPointer array of CCP for the
*                desired Type and returns the index, if it is found.
*
*  Parameter(s)  :
*            Type         - Type of the Option.
*
*  Return Values : INT2
***************************************************************************/
INT2
GetCCPOptionIndex (UINT1 Type)
{
    INT2                LoopCount;

    for (LoopCount = 0; LoopCount < MAX_CCP_OPTIONS; LoopCount++)
    {
        if (LoopCount >=
            (INT2) (sizeof (CCPFnPointer) / sizeof (tCCPFNPOINTER)))
        {
            break;
        }

        if (CCPFnPointer[LoopCount].OptType == Type)
        {

            return LoopCount;
        }
    }

    return DISCARD_OPT;
}

/*********************************************************************
*  Function Name : CCPProcessConfRej
*  Description   :
*    This function is used to process the configuration reject for
*  CCP Options.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*  Return Value  : CLOSE/NO_CLOSE
*********************************************************************/
INT1
CCPProcessConfRej (tGSEM * pGSEM)
{
    INT2                Index;

    for (Index = 0; Index < MAX_CCP_OPTIONS; Index++)
    {
        if (pGSEM->pNegFlagsPerIf[Index].FlagMask & ACKED_BY_PEER_SET)
        {

            pGSEM->pNegFlagsPerIf[Index].FlagMask &= ACKED_BY_PEER_NOT_SET;
        }
    }

    if ((Index = GetNextOptionFromList (pGSEM, CCP_MOD)) == DISCARD_OPT)
    {

        PPP_DBG ("AckedByPeer = NULL, returing CLOSE SEM");
        return CLOSE_SEM;
    }

    pGSEM->pNegFlagsPerIf[Index].FlagMask |= ACKED_BY_PEER_MASK;

    PPP_DBG1 ("Num. options requested : %d", pGSEM->NumOptReq);
    if (pGSEM->NumOptReq-- == 0)
    {

        PPP_DBG ("ALL options Rejected.... Closing CCP SEM...");
        return (CLOSE_SEM);
    }
    return (NO_CLOSE_SEM);
}

/**************************************************************************
*  Function Name : CCPGetSEMPtr
*  Description   :
*                This function returns the pointer to the CCP GSEM from the
*                pIf.
*
*  Parameter(s)  :
*                pIf    - Poniter to pppIf structure.
*
*  Return Values : tGSEM *
***************************************************************************/
tGSEM              *
CCPGetSEMPtr (tPPPIf * pIf)
{
    tCCPIf             *pCCPIf;
        /******* SKR004 *************/
    if (pIf->pCCPIf != NULL)
    {
        pCCPIf = (tCCPIf *) pIf->pCCPIf;
        PPP_TRC (CONTROL_PLANE, "CCP:");
        return (&pCCPIf->CCPGSEM);
    }
    return (NULL);
}

/*********************************************************************
*  Function Name : CCPCopyOptions 
*  Description   :
*    This function is used to process the configuration request for
*  CCP Options.
*  Input         :
*    pGSEM        -     Points to the GSEM structure
*    Flag        -     Specifies whether to alloc or free the temporary 
*                    variable.
*    PktType        -    Specifies whether the function is called from request
*                    processing or Nak processing.                  
*  Return Value        :
*                    DISCARD on allocation error, OK otherwise.
*********************************************************************/
INT1
CCPCopyOptions (tGSEM * pGSEM, UINT1 Flag, UINT1 PktType)
{
    t_SLL              *pTemp;
    tHeader            *pHeader, *pTmp;
    static t_SLL        CopyList;
    static UINT1        Count;    /* srini */
    tCCPIf             *pCCPIf;

/******** After all the modifications made for the bug fix, please
          restructure this fn. for effective modularity. - srini. ******/

    if (Count == 0)
    {                            /* srini */

        SLL_INIT (&CopyList);
        Count++;
    }
    pCCPIf = (tCCPIf *) pGSEM->pIf->pCCPIf;

    if (PktType == CONF_REQ_CODE)
    {
        pTemp = &pCCPIf->OptionsAckedByLocal;
    }
    else
    {
        pTemp = &pCCPIf->OptionsAckedByPeer;
    }

    if (Flag == NOT_SET)
    {

        SLL_SCAN (pTemp, pHeader, tHeader *)
        {

            if ((pTmp = (tHeader *) ALLOC_CCP_PARAMS (pHeader->Length)) == NULL)
            {
                PPP_TRC (ALL_FAILURE, " ALLOC_CCP_PARAMS failed...");
                /* Makefile changes - <return with no value> */
                return DISCARD;
            }
            MEMCPY (pTmp, pHeader, pHeader->Length);
            SLL_INIT_NODE (&pTmp->pNext);
            SLL_ADD (&CopyList, (t_SLL_NODE *) pTmp);
        }

        if (PktType == CONF_REQ_CODE)
        {
            /* Check if the nodes of the SLL are deleted. If not so, please delete
             * them and call the OptionInit()  - srini */
            DeleteNodes (&pCCPIf->OptionsAckedByLocal);
            OptionInit (&pCCPIf->OptionsAckedByLocal);
        }
    }
    else
    {
        if (Flag == SET)
        {
            SLL_CONCAT (pTemp, &CopyList);
        }
        DeleteNodes (&CopyList);
        SLL_INIT (&CopyList);
    }

    return (OK);
}

/*********************************************************************
*  Function Name : CCPUp
*  Description   :
*                This function intimates the higher layer is CCP is 
*                negotiated successfully. It also initialises the 
*                compression algo. related variables. 
*  Parameter(s)  :
*           pCCPGSEM  - pointer to the CCP tGSEM
*                                   corresponding to the current interface.
*  Return Values : VOID
*********************************************************************/
VOID
CCPUp (tGSEM * pCCPGSEM)
{
    tHeader            *pHeader, *pHeader1;
    INT2                Index;
    tCCPIf             *pCCPIf;

    PPP_DBG (" CCP is UP...");

    pCCPIf = (tCCPIf *) pCCPGSEM->pIf->pCCPIf;

    pCCPIf->OperStatus = STATUS_UP;

    CCPDeleteUnAckedNodes (pCCPGSEM);

    pHeader = (tHeader *) SLL_FIRST (&pCCPIf->OptionsAckedByPeer);
    pHeader1 = (tHeader *) SLL_FIRST (&pCCPIf->OptionsAckedByLocal);

    if (pHeader == NULL && pHeader1 == NULL)
    {

        GSEMRun (pCCPGSEM, CLOSE);
    }

    if (pHeader != NULL)
    {

        if ((Index = GetCCPOptionIndex (pHeader->Type)) == DISCARD_OPT)
        {

            return;
        }
        if (Index >= (INT2) (sizeof (CCPFnPointer) / sizeof (tCCPFNPOINTER)))
        {
            return;
        }

        PPP_TRC1 (PPP_MUST, " INTERFACE [%ld]: ",
                  pCCPGSEM->pIf->LinkInfo.IfIndex);
        PPP_TRC (PPP_MUST, " \t\tCCP  State Changed to    UP\n");

        (*CCPFnPointer[Index].InitCompressionInfo) (&pCCPIf->pInCompIf,
                                                    decompression);
        PPP_DBG ("Creating CompressionInfo... while Decompressing");

        CreateCompressionSpecificInfo (pCCPGSEM, pHeader, decompression,
                                       (UINT1) (Index));

    }

    if (pHeader1 != NULL)
    {

        if ((Index = GetCCPOptionIndex (pHeader1->Type)) == DISCARD_OPT)
        {

            return;
        }

        if (Index >= (INT2) (sizeof (CCPFnPointer) / sizeof (tCCPFNPOINTER)))
        {
            return;
        }

        (*CCPFnPointer[Index].InitCompressionInfo) (&pCCPIf->pOutCompIf,
                                                    compression);
        CreateCompressionSpecificInfo (pCCPGSEM, pHeader1, compression,
                                       (UINT1) (Index));

    }
}

/*********************************************************************
*  Function Name : CCPDown
*  Description   :
*  Parameter(s)  :
*           pCCPGSEM  - pointer to the CCP GSEM
*                                   corresponding to the current interface.
*  Return Values : VOID
*********************************************************************/
VOID
CCPDown (tGSEM * pCCPGSEM)
{

    tPPPIf             *pIf;
    INT2                Index;
    tHeader            *pHeader;
    tCCPIf             *pCCPIf;

    PPP_DBG (" CCP is DOWN...");

    pIf = pCCPGSEM->pIf;
    pCCPIf = (tCCPIf *) pIf->pCCPIf;

    pCCPIf->OperStatus = STATUS_DOWN;

    pHeader = (tHeader *) SLL_FIRST (&pCCPIf->OptionsAckedByPeer);

    if (pHeader != NULL)
    {

        if ((Index = GetCCPOptionIndex (pHeader->Type)) == DISCARD_OPT)
        {

            return;
        }
        if (Index >= (INT2) (sizeof (CCPFnPointer) / sizeof (tCCPFNPOINTER)))
        {
            return;
        }

        DeleteCompressionSpecificInfo (pCCPGSEM, (UINT1) decompression,
                                       (UINT1) (Index));

    }

    pHeader = (tHeader *) SLL_FIRST (&pCCPIf->OptionsAckedByLocal);

    if (pHeader != NULL)
    {

        if ((Index = GetCCPOptionIndex (pHeader->Type)) == DISCARD_OPT)
        {

            return;
        }

        DeleteCompressionSpecificInfo (pCCPGSEM, (UINT1) compression,
                                       (UINT1) (Index));

    }
    DeleteNodes (&pCCPIf->OptionsAckedByPeer);
    CCPCopyDesiredToAckedByPeer (pCCPIf);
}

/*********************************************************************
*  Function Name : CreateCompressionSpecificInfo
*  Description   : This functions creates the compression Info.
*  Parameter(s)  :
*           pGSEM  - pointer to the CCP GSEM
*            pHeader- pointer to Header structure 
*           UINT1  - Compression/Decompression
*            UINT1  - Index in the CCPFnPointer array
*  Return Values : VOID
*********************************************************************/
VOID
CreateCompressionSpecificInfo (tGSEM * pCCPGSEM, tHeader * pHeader, UINT1 Mode,
                               UINT1 Index)
{
    tCCPIf             *pCCPIf;

    pCCPIf = pCCPGSEM->pIf->pCCPIf;

/*        if( ( pCCPGSEM->pIf->pIpcpPtr != NULL) && ( pCCPGSEM->pIf->pIpcpPtr->OperStatus == UP )) {
*/
    {
        if (Mode == decompression)
        {
            if (Index == MPPE_IDX)
            {
#ifdef MPPE
                if ((*CCPFnPointer[Index].CreateCompressionInfo)
                    (pCCPIf, pHeader, Mode) == (INT1) FALSE)
                {
                    return;
                }
#endif
            }
            else
            {
                if ((*CCPFnPointer[Index].CreateCompressionInfo)
                    (pCCPIf->pInCompIf, pHeader, IPCP_PROTOCOL) == (INT1) FALSE)
                {
                    return;
                }
            }
        }
        else if (Mode == compression)
        {
            if (Index == MPPE_IDX)
            {
#ifdef MPPE
                if ((*CCPFnPointer[Index].CreateCompressionInfo)
                    (pCCPIf, pHeader, Mode) == (INT1) FALSE)
                {
                    return;
                }
#endif
            }
            else
            {
                if ((*CCPFnPointer[Index].CreateCompressionInfo)
                    (pCCPIf->pOutCompIf, pHeader,
                     IPCP_PROTOCOL) == (INT1) FALSE)
                {
                    return;
                }
            }
        }
    }

}

/*********************************************************************
*  Function Name : DeleteCompressionSpecificInfo
*  Description   : This functions deletes the compression Info.
*  Parameter(s)  :
*           pGSEM  - pointer to the CCP GSEM
*           Mode  - Compression/Decompression
*           Index  - Compression/Decompression
*  Return Values : VOID
*********************************************************************/
VOID
DeleteCompressionSpecificInfo (tGSEM * pCCPGSEM, UINT1 Mode, UINT1 Index)
{
    tCCPIf             *pCCPIf;

    pCCPIf = (tCCPIf *) pCCPGSEM->pIf->pCCPIf;
    if (Mode == compression)
    {

        if ((*CCPFnPointer[Index].DeleteCompressionInfo) (pCCPIf->pOutCompIf) ==
            (INT1) FALSE)
        {

        }
        pCCPIf->pOutCompIf = NULL;
        return;
    }

    if ((*CCPFnPointer[Index].DeleteCompressionInfo) (pCCPIf->pInCompIf) ==
        (INT1) FALSE)
    {

    }
    pCCPIf->pInCompIf = NULL;
    return;

}

/*********************************************************************
*  Function Name : DeleteUnAckedNodes 
*  Description   : This functions deletes the unacked nodes from the
*                    AckedByPeer 
*  Parameter(s)  :
*           pGSEM  - pointer to the CCP GSEM
*  Return Values : VOID
*********************************************************************/
VOID
CCPDeleteUnAckedNodes (tGSEM * pGSEM)
{
    tHeader            *pHeader, *ptmpHeader;
    INT2                LoopCount;
    tCCPIf             *pCCPIf;

    pCCPIf = (tCCPIf *) pGSEM->pIf->pCCPIf;

    for (LoopCount = 0; LoopCount < MAX_CCP_OPTIONS; LoopCount++)
    {

        if ((pGSEM->pNegFlagsPerIf[LoopCount].FlagMask & ACKED_BY_PEER_MASK) ==
            NOT_SET)
        {

            SLL_SCAN (&pCCPIf->OptionsAckedByPeer, pHeader, tHeader *)
            {

                if (pHeader->Type == pGSEM->pNegFlagsPerIf[LoopCount].OptType)
                {

                    ptmpHeader = pHeader;
                    pHeader =
                        (tHeader *) SLL_NEXT (&pCCPIf->OptionsAckedByPeer,
                                              (t_SLL_NODE *) ptmpHeader);
                    SLL_DELETE (&pCCPIf->OptionsAckedByPeer,
                                (t_SLL_NODE *) ptmpHeader);
                }

            }

        }

    }

}

/*********************************************************************
*  Function Name : CCPCopyDesiredToAckedByPeer 
*  Description   : This functions  copies OptionsDesired to AckedByPeer
*  Parameter(s)  :
*           pCCP - pointer to the CCP interface structure
*  Return Values : VOID
*********************************************************************/
VOID
CCPCopyDesiredToAckedByPeer (tCCPIf * pCCP)
{
    tHeader            *pHeader, *pTmp;
    INT2                Index;
    INT2                Count = 0;

    SLL_INIT (&pCCP->OptionsAckedByPeer);
    SLL_SCAN (&pCCP->OptionsDesired, pHeader, tHeader *)
    {

        if ((Index = GCPGetOptIndex (&pCCP->CCPGSEM, pHeader->Type)) ==
            NOT_FOUND)
        {

            return;
        }
        if (Count == 0)
        {

            if (pCCP->CCPGSEM.pNegFlagsPerIf[Index].FlagMask & DESIRED_SET)
            {

                pCCP->CCPGSEM.pNegFlagsPerIf[Index].FlagMask |=
                    ACKED_BY_PEER_SET;
                Count++;
            }
        }

        if ((pTmp = (tHeader *) ALLOC_CCP_PARAMS (pHeader->Length)) == NULL)
        {
            PPP_TRC (ALL_FAILURE, "ALLOC_CCP_PARAMS failed...");
            return;
        }

        MEMCPY (pTmp, pHeader, pHeader->Length);
        SLL_INIT_NODE (&pTmp->pNext);
        SLL_ADD (&pCCP->OptionsAckedByPeer, (t_SLL_NODE *) pTmp);
    }
}

/* This function is to be moved to pppgzif.c */
/**************************************************************************
*  Function Name : ValidateGZIPResetAck 
*  Description   :
*                This function validates the length and Id of the Reset_Ack
*                packet for the GZIP.
*  Parameter(s)  :
*            pCCPIf            - Pointer to CCPIf structure.
*            Length            - Length of the incoming buffer
*            Id                - Identifier of the incoming buffer
*
*  Return Values : UINT1
***************************************************************************/
INT1
ValidateGZIPResetAck (tCCPIf * pCCPIf, UINT2 Length, UINT1 Id)
{

    UNUSED_PARAM (pCCPIf);
    UNUSED_PARAM (Length);
    UNUSED_PARAM (Id);
#ifdef GZIP
    if (Length == GZIP_RESET_PKT_LENGTH && Id == pCCPIf->Status.Id)
    {

        return OK;
    }
#endif

    return DISCARD_OPT;
}
