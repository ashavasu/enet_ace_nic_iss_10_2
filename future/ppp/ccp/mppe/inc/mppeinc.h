/********************************************************************
 * Copyright (C) Future Software Limited, 1997-98,2001
 *
 * $Id: mppeinc.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description: This file contains the common PPP files for the CCP
 * module
 *
 *******************************************************************/
#ifndef __PPP_MPPEINC_H__
#define __PPP_MPPEINC_H__

#include "genhdrs.h"
#include "pppcom.h"
#include "pppeccmn.h"
#include "pppmppeif.h"
#include "pppccpmd.h"
#include "mppeprot.h"

#endif  /* __PPP_MPPEINC_H__ */
