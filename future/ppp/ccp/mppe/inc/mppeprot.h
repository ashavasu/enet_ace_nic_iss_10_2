/********************************************************************
 * Copyright (C) Future Software Limited, 1997-98,2001
 *
 * $Id: mppeprot.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the prototypes of MPPE functions used
 * internally by MPPE
 *
 *******************************************************************/
#ifndef __PPP_MPPEPROT_H__
#define __PPP_MPPEPROT_H__

INT1 MppeHistoryInit(VOID** pHistoryInfo, tCompressOperation Operation);
INT1 MppeCreateHistory(VOID *pHistoryInfo, VOID * pParams,tCompressOperation Dummy);
INT1 MppeDeleteHistory(VOID *pHistoryInfo);
INT1 MppeCrypt(t_MSG_DESC **pCruBuffer, VOID *pHistoryInfo);
INT1 MppeDeCrypt(t_MSG_DESC **pCruBuffer, VOID *pHistoryInfo);

tMppeParams* MppeCreateIf(VOID);
INT1 CCPProcessMPPEConfReq(tGSEM *pGSEM, t_MSG_DESC *pIn, tOptVal OptVal, UINT2 Offset, UINT1 PresenceFlag);
INT1 CCPProcessMPPEConfNak(tGSEM *pGSEM, t_MSG_DESC *pIn, tOptVal OptVal);
VOID *CCPReturnMPPEAddrPtr(tGSEM *pGSEM, UINT1 *OptLen);
VOID MppeGetMasterKey(UINT1 *u1Challenge, UINT1 *u1NtPasswordHash, UINT1 *u1MasterKey);
VOID MppeGetKey(UINT1 *u1InitKey, UINT1 *u1NewKey, UINT1 u1Length);
VOID MppeGetNewKey(UINT1 *u1InitKey, UINT1 *u1NewKey, UINT1 u1EncryptType);
VOID MppeReduceKey (UINT1 *sessionKey, UINT4 u4EncryptType);
VOID MppeSetEncryptionLevel (UINT1 u1EncryptionLevel);
#endif  /* __PPP_MPPEPROT_H__ */
