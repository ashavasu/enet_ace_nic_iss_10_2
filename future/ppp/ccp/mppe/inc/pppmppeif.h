/********************************************************************
 * Copyright (C) Future Software Limited, 1997-98,2001
 *
 * $Id: pppmppeif.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:Adheres to rfc-1974.txt the interface func.s for 
 *             implementing this rfc
 *
 *******************************************************************/
#ifndef __PPP_PPPMPPEIF_H__
#define __PPP_PPPMPPEIF_H__

#ifndef MPPEIF_H
#define MPPEIF_H
#ifdef calloc
#undef calloc
#endif

#define DEFAULT_ENCRYPTION  	0x000000e0
#define DEFAULT_MODE	 	MPPE_STATELESS

#define MPPE_40BIT		0x00000020
#define MPPE_56BIT		0x00000080
#define MPPE_128BIT		0x00000040
#define MPPC_BIT        0x00000001
#define OBSOLETE_BIT    0x00000010
#define MPPE_ENCRYPT_BIT_MASK 	0x000000e0
#define MPPE_40BIT_MASK		0xffffff2f
#define MPPE_56BIT_MASK		0xffffff8f
#define MPPE_128BIT_MASK	0xffffff4f
#define MPPE_ENCRYPT_40		40
#define MPPE_ENCRYPT_56		56
#define MPPE_ENCRYPT_128	128

#define MPPE_CHALLENGE_LEN	8
#define MPPE_SHA1_DIGEST_LEN	20
#define MPPE_SHA1_PAD_LEN	40
#define MPPE_KEY_LEN_40_56_BIT	8
#define MPPE_KEY_LEN_128_BIT	16	
#define MPPE_KEY_LEN		MPPE_KEY_LEN_128_BIT

#define MPPE_ENCRYPTION_BIT 	0x1000
#define MPPE_FLUSHED_BIT 	0x8000
#define MPPE_STATEFULL 		0x00
#define MPPE_STATELESS	 	0x01
#define MPPE_COH_MASK 		0x0fff
#define MPPE_FLAG_MASK 		0x00ff
#define MPPE_STATE_MASK 	0xff000000

#define SESSIONKEY0 		0xd1
#define SESSIONKEY1 		0x26
#define SESSIONKEY2 		0x9e

typedef struct MppeParams {
    tHeader	Header;
    UINT4	u4SupportBits; 
    UINT1	u1State;
    UINT1	u1EncryptType;
    UINT2	u2Rsvd;
} tMPPEParams;

#define tMppeParams tMPPEParams

typedef struct MppeHistoryInfo
{
    UINT1	u1State; /* Stateless or stateful */
    UINT1	u1EncryptType; /* 40, 56 or 128 bit*/
    UINT1	u1KeyLen; /* 8 bytes for 40 or 56 bit and 16 for 128 bits */
    UINT1	u1ProtoLength; /* 1 for compressed protocol */
    BOOLEAN	FlushReq;
    BOOLEAN	ResetRequestSent;
    UINT2	u2CohCount;
    UINT1	au1MastKey[MPPE_KEY_LEN];
    UINT1	au1SessKey[MPPE_KEY_LEN];
} tMppeHistoryInfo;

#endif
#endif  /* __PPP_PPPGZIF_H__ */
