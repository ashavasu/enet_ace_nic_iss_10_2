/********************************************************************
 * Copyright (C) Future Software Limited, 1997-98,2001
 *
 * $Id: pppmppeif.c,v 1.3 2011/03/18 10:11:32 siva Exp $
 *
 * Description:Adheres to rfc-1974.txt the interface func.s for 
 *             implementing this rfc
 *
 *******************************************************************/
#include "mppeinc.h"
#include "gcpdefs.h"
#include "gsemdefs.h"
#include "pppccpmd.h"
#include "pppccp.h"
#include "shaarinc.h"
#include "rc4proto.h"
#include "authcom.h"
/* Pads used in key derivation */
unsigned char       SHApad1[MPPE_SHA1_PAD_LEN] = {
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

unsigned char       SHApad2[MPPE_SHA1_PAD_LEN] = {
    0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2,
    0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2,
    0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2,
    0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2, 0xf2
};

UINT1               gu1EncryptionLevel = DEFAULT_ENCRYPTION;
UINT1               gu1EncryptionMode = DEFAULT_MODE;

/*************************************************************************/
/* Function Name     : MppeGetMasterKey                                  */
/* Description       : Returns the master key from the challenge rcvd.   */
/*                     and the NTPasswordHash fo r128 bit encryption.    */
/* Input(s)          : u1Challenge - 8 Byte challenge received           */
/*                     u1NtPasswordHash - Nt Password Hash               */
/* Output(s)         : u1MasterKey - Master key                          */
/* Returns           : VOID                                              */
/*************************************************************************/
VOID
MppeGetMasterKey (UINT1 *u1Challenge, UINT1 *u1NtPasswordHash,
                  UINT1 *u1MasterKey)
{
    UINT1               au1Digest[MPPE_SHA1_DIGEST_LEN];
    unArCryptoHash      sha1Ctx;

    MEMSET (&sha1Ctx, 0, sizeof (unArCryptoHash));
    MEMSET (au1Digest, 0, MPPE_SHA1_DIGEST_LEN);
    Sha1ArStart (&sha1Ctx);
    Sha1ArUpdate (&sha1Ctx, u1NtPasswordHash, MPPE_KEY_LEN_128_BIT);
    Sha1ArUpdate (&sha1Ctx, u1NtPasswordHash, MPPE_KEY_LEN_128_BIT);
    Sha1ArUpdate (&sha1Ctx, u1Challenge, MPPE_CHALLENGE_LEN);
    Sha1ArFinish (&sha1Ctx, au1Digest);
    MEMCPY (u1MasterKey, au1Digest, MPPE_KEY_LEN_128_BIT);
    return;
}

/*************************************************************************/
/* Function Name     : MppeGetKey                                        */
/* Description       : Returns the initial key from the master key.      */
/*                                                                       */
/* Input(s)          : u1InitKey - Initial Key                           */
/*                     u1NewKey - Same as init key                       */
/*                     u1Length - Length of hte key                      */
/* Output(s)         : u1NewKey - New Key generated                      */
/* Returns           : VOID                                              */
/*************************************************************************/
VOID
MppeGetKey (UINT1 *u1InitKey, UINT1 *u1NewKey, UINT1 u1Length)
{
    UINT1               au1Digest[MPPE_SHA1_DIGEST_LEN];
    unArCryptoHash      sha1Ctx;

    MEMSET (&sha1Ctx, 0, sizeof (unArCryptoHash));
    MEMSET (au1Digest, 0, MPPE_SHA1_DIGEST_LEN);
    Sha1ArStart (&sha1Ctx);
    Sha1ArUpdate (&sha1Ctx, u1InitKey, u1Length);
    Sha1ArUpdate (&sha1Ctx, SHApad1, MPPE_SHA1_PAD_LEN);
    Sha1ArUpdate (&sha1Ctx, u1NewKey, u1Length);
    Sha1ArUpdate (&sha1Ctx, SHApad2, MPPE_SHA1_PAD_LEN);
    Sha1ArFinish (&sha1Ctx, au1Digest);
    MEMCPY (u1NewKey, au1Digest, u1Length);
    return;
}

/*************************************************************************/
/* Function Name     : MppeGetNewKey                                     */
/* Description       : Returns the initial key from the master key.      */
/*                                                                       */
/* Input(s)          : u1InitKey - The master Key                        */
/*                     u1NewKey - Session key                            */
/*                     u1EncryptType - Type of Encryption 50, 56 or 1128 */
/* Output(s)         : u1NewKey - New Session Key generated              */
/* Returns           : VOID                                              */
/*************************************************************************/
VOID
MppeGetNewKey (UINT1 *u1InitKey, UINT1 *u1NewKey, UINT1 u1EncryptType)
{
    UINT1               au1Digest[MPPE_SHA1_DIGEST_LEN];
    unArCryptoHash      sha1Ctx;
    tArRc4State         rc4Ctx;
    UINT1               u1Length = 0;

    MEMSET (&sha1Ctx, 0, sizeof (unArCryptoHash));
    MEMSET (au1Digest, 0, MPPE_SHA1_DIGEST_LEN);
    if (u1EncryptType == MPPE_128BIT)
        u1Length = MPPE_KEY_LEN_128_BIT;
    else
        u1Length = MPPE_KEY_LEN_40_56_BIT;

    Sha1ArStart (&sha1Ctx);
    Sha1ArUpdate (&sha1Ctx, u1InitKey, u1Length);
    Sha1ArUpdate (&sha1Ctx, SHApad1, MPPE_SHA1_PAD_LEN);
    Sha1ArUpdate (&sha1Ctx, u1NewKey, u1Length);
    Sha1ArUpdate (&sha1Ctx, SHApad2, MPPE_SHA1_PAD_LEN);
    Sha1ArFinish (&sha1Ctx, au1Digest);
    MEMCPY (u1NewKey, au1Digest, u1Length);
    Rc4ArSetup (&rc4Ctx, u1NewKey, u1Length);
    Rc4ArCrypt (&rc4Ctx, u1NewKey, u1Length);
    MppeReduceKey (u1NewKey, u1EncryptType);
    return;
}

/*************************************************************************/
/* Function Name     : MppeReduceKey                                     */
/* Description       : Reduce the session key for 40 or 56 bit encryption*/
/*                                                                       */
/* Input(s)          : u1SessionKey - Session Key                        */
/*                     u4EncryptType - Type of Encryption 50, 56 or 1128 */
/* Output(s)         : u1SessionKey - New Session Key generated          */
/* Returns           : VOID                                              */
/*************************************************************************/
VOID
MppeReduceKey (UINT1 *u1SessionKey, UINT4 u4EncryptType)
{
    if (u4EncryptType == MPPE_40BIT)
    {
        u1SessionKey[0] = SESSIONKEY0;
        u1SessionKey[1] = SESSIONKEY1;
        u1SessionKey[2] = SESSIONKEY2;
    }
    else if (u4EncryptType == MPPE_56BIT)
    {
        u1SessionKey[0] = SESSIONKEY0;
    }
    return;
}

tMppeParams        *
MppeCreateIf (VOID)
{
    tMppeParams        *pMppeParams;

    CHECK (((pMppeParams =
             (tMppeParams *) PPP_MALLOC (sizeof (tMppeParams))) == NULL), NOMEM,
           "MppeCreateIf", NULL);

    pMppeParams->u4SupportBits = 0;
    pMppeParams->Header.Type = OPTION_MPPE;
    pMppeParams->Header.Length = sizeof (tMppeParams);
    pMppeParams->u1State = gu1EncryptionMode;
    pMppeParams->u1EncryptType = gu1EncryptionLevel;
    pMppeParams->u4SupportBits = (((pMppeParams->u4SupportBits |
                                    gu1EncryptionMode) << 24) |
                                  gu1EncryptionLevel);

    SLL_INIT_NODE (&pMppeParams->Header.pNext);

    return pMppeParams;
}

INT1
MppeHistoryInit (VOID **pInfo, tCompressOperation Operation)
{
    tMppeHistoryInfo  **pHistoryInfo;
    pHistoryInfo = (tMppeHistoryInfo **) pInfo;
    PPP_UNUSED (Operation);
    DEBUG_CHECK (*pHistoryInfo != NULL, "already present!", "MppeHistoryInit",
                 TRUE);
    CHECK ((*pHistoryInfo =
            (tMppeHistoryInfo *) PPP_MALLOC (sizeof (tMppeHistoryInfo))) ==
           NULL, NOMEM, "MppeHistoryInit", FALSE);
    return TRUE;
}

INT1
MppeCreateHistory (VOID *pInfo, VOID *pParams, tCompressOperation Mode)
{
    tMppeParams        *pMppeParams;
    tMppeHistoryInfo   *pHistoryInfo;
    tSecret            *pScanList;
    UINT1               au1Password[MSCHAP_PASSWD_HASH_LENGTH];
    UINT1               au1PasswordHash[MSCHAP_PASSWD_HASH_LENGTH];
    UINT1               au1Challenge[MSCHAP_CHALLENGE_LENGTH];
    UINT1               u1Mode = 0;
    UINT2               u2Index = 0;
    UINT2               u2Count = 0;
    UINT2               au1UnicodePass[MAX_SECRET_SIZE * 2];
    UINT1               au1Passwd[MAX_SECRET_SIZE];

    MEMSET (au1Password, 0, MSCHAP_PASSWD_HASH_LENGTH);
    MEMSET (au1PasswordHash, 0, MSCHAP_PASSWD_HASH_LENGTH);
    MEMSET (au1Challenge, 0, MSCHAP_CHALLENGE_LENGTH);
    MEMSET (au1UnicodePass, 0, MAX_SECRET_SIZE * 2);
    MEMSET (au1Passwd, 0, MAX_SECRET_SIZE);

    if (((tCCPIf *) pInfo)->CCPGSEM.pIf->pAuthPtr->ClientProt == 1)
    {
        MEMCPY (au1Challenge,
                ((tCCPIf *) pInfo)->CCPGSEM.pIf->pAuthPtr->ClientInfo.
                ClientChap.pChallenge, MSCHAP_CHALLENGE_LENGTH);
        u1Mode = CLIENT;
    }
    else if (((tCCPIf *) pInfo)->CCPGSEM.pIf->pAuthPtr->ServerProt == 1)
    {
        MEMCPY (au1Challenge,
                ((tCCPIf *) pInfo)->CCPGSEM.pIf->pAuthPtr->ServerInfo.
                ServerChap.ChallengeValue, MSCHAP_CHALLENGE_LENGTH);
        u1Mode = SERVER;
    }
    else
    {
        return FALSE;
    }
    if (Mode == decompression)
    {
        pHistoryInfo = (tMppeHistoryInfo *) ((tCCPIf *) pInfo)->pInCompIf;
        pHistoryInfo->u1ProtoLength = 2;
        if (((tCCPIf *) pInfo)->CCPGSEM.pIf->LcpGSEM.pNegFlagsPerIf[PFC_IDX].
            FlagMask & ACKED_BY_PEER_MASK)
        {
            pHistoryInfo->u1ProtoLength = 1;
        }
    }
    else
    {
        pHistoryInfo = (tMppeHistoryInfo *) ((tCCPIf *) pInfo)->pOutCompIf;
        pHistoryInfo->u1ProtoLength = 2;
        if (((tCCPIf *) pInfo)->CCPGSEM.pIf->LcpGSEM.pNegFlagsPerIf[PFC_IDX].
            FlagMask & ACKED_BY_LOCAL_MASK)
        {
            pHistoryInfo->u1ProtoLength = 1;
        }
    }

    pMppeParams = (tMppeParams *) pParams;
    DEBUG_CHECK (pMppeParams == NULL
                 || (pHistoryInfo == NULL),
                 "one or more prmtrs are NULL OR Illegal Operation (last prmt)",
                 "MppeCreateHistory", FALSE);
    pHistoryInfo->u2CohCount = 0;
    pHistoryInfo->u1State = pMppeParams->u1State;
    pHistoryInfo->u1EncryptType = pMppeParams->u1EncryptType;
    pHistoryInfo->FlushReq = FALSE;
    pHistoryInfo->ResetRequestSent = FALSE;

    SLL_SCAN (&((tCCPIf *) pInfo)->CCPGSEM.pIf->pAuthPtr->SecurityInfo.
              SecretList, pScanList, tSecret *)
    {
        MEMCPY (au1Passwd, pScanList->Secret, pScanList->SecretLen);
        FsUtlDecryptPasswd ((CHR1 *) au1Passwd);
        if (u1Mode == CLIENT)
        {
            if ((pScanList->Protocol == MSCHAP_PROTOCOL) &&
                (pScanList->Direction == CLIENT) &&
                (pScanList->IdentityLen == ((tCCPIf *) pInfo)->CCPGSEM.pIf->
                 pAuthPtr->ClientInfo.ClientChap.pCHAPClientSecret->IdentityLen)
                && (MEMCMP (pScanList->Identity,
                            ((tCCPIf *) pInfo)->CCPGSEM.pIf->pAuthPtr->
                            ClientInfo.ClientChap.pCHAPClientSecret->Identity,
                            pScanList->IdentityLen) == 0))
            {
                if (pHistoryInfo->u1EncryptType == MPPE_128BIT)
                {
                    for (u2Index = 0; u2Index < pScanList->SecretLen; u2Index++)
                    {
                        au1UnicodePass[u2Count++] = au1Passwd[u2Index];
                        au1UnicodePass[u2Count++] = 0x00;
                    }
                    MSChapNtPasswordHash (au1UnicodePass, au1Password,
                                          pScanList->SecretLen);
                }
                else
                {
                    MSChapLmPasswordHash (au1Passwd, au1Password,
                                          pScanList->SecretLen);
                }
                break;
            }
        }
        else if (u1Mode == SERVER)
        {
            if ((pScanList->Protocol == MSCHAP_PROTOCOL) &&
                (pScanList->Direction == SERVER) &&
                (pScanList->IdentityLen == ((tCCPIf *) pInfo)->CCPGSEM.pIf->
                 pAuthPtr->ServerInfo.ServerChap.pCHAPCurrentSecret->
                 IdentityLen) &&
                (MEMCMP (pScanList->Identity,
                         ((tCCPIf *) pInfo)->CCPGSEM.pIf->pAuthPtr->ServerInfo.
                         ServerChap.pCHAPCurrentSecret->Identity,
                         pScanList->IdentityLen) == 0))
            {
                if (pHistoryInfo->u1EncryptType == MPPE_128BIT)
                {
                    MEMCPY (au1Password, au1Passwd, pScanList->SecretLen);
                }
                else
                {
                    MEMCPY (au1Password, pScanList->SecretLmHash,
                            pScanList->SecretLen);
                }
                break;
            }
        }
    }
    if (pHistoryInfo->u1EncryptType == MPPE_40BIT
        || pHistoryInfo->u1EncryptType == MPPE_56BIT)
    {
        pHistoryInfo->u1KeyLen = MPPE_KEY_LEN_40_56_BIT;
        MEMCPY (pHistoryInfo->au1MastKey, au1Password, MPPE_KEY_LEN_40_56_BIT);
        MEMCPY (pHistoryInfo->au1SessKey, au1Password, MPPE_KEY_LEN_40_56_BIT);
        MppeGetKey (pHistoryInfo->au1MastKey, pHistoryInfo->au1SessKey,
                    MPPE_KEY_LEN_40_56_BIT);
        MppeReduceKey (pHistoryInfo->au1SessKey, pHistoryInfo->u1EncryptType);
    }
    else if (pHistoryInfo->u1EncryptType == MPPE_128BIT)
    {
        pHistoryInfo->u1KeyLen = MPPE_KEY_LEN_128_BIT;
        /* Dividing key len by 2, since fn internally multiplies it by 2 !!! */
        MSChapNtPasswordHash ((UINT2 *) au1Password, au1PasswordHash,
                              (MPPE_KEY_LEN_128_BIT / 2));
        MppeGetMasterKey (au1Challenge, au1PasswordHash,
                          pHistoryInfo->au1MastKey);
        MEMCPY (pHistoryInfo->au1SessKey, pHistoryInfo->au1MastKey,
                MPPE_KEY_LEN_128_BIT);
        MppeGetKey (pHistoryInfo->au1MastKey, pHistoryInfo->au1SessKey,
                    MPPE_KEY_LEN_128_BIT);
    }
    return TRUE;
}

INT1
MppeDeleteHistory (VOID *pInfo)
{
    tMppeHistoryInfo   *pHistoryInfo;

    pHistoryInfo = (tMppeHistoryInfo *) pInfo;
    if (pHistoryInfo != NULL)
    {
        FREE (pHistoryInfo);
        return TRUE;
    }
    DEBUG_PRINT ("already deleted.", "MppeDeleteHistory");
    return FALSE;
}

INT1
MppeCrypt (t_MSG_DESC ** pCruBuffer, VOID *pInfo)
{
    tArRc4State         rc4Ctx;
    tMppeHistoryInfo   *pHistoryInfo;
    UINT1              *pu1Clear;
    UINT1               u1ProtoLength = 0;
    UINT2               u2Proto = 0;
    UINT2               u2Header = 0;
    UINT4               u4Len = 0;

    pHistoryInfo = (tMppeHistoryInfo *) pInfo;
    DEBUG_CHECK ((*pCruBuffer == NULL
                  || pHistoryInfo == NULL), "illegal input prmt value",
                 "MppeCrypt", ERROR);
    u1ProtoLength = pHistoryInfo->u1ProtoLength;
    u4Len = CRU_BUF_Get_ChainValidByteCount (*pCruBuffer);
    pu1Clear = MEM_MALLOC (u4Len, UINT1);
    CRU_BUF_Copy_FromBufChain (*pCruBuffer, (UINT1 *) &u2Proto, 0,
                               u1ProtoLength);
    CRU_BUF_Copy_FromBufChain (*pCruBuffer, pu1Clear, 0, u4Len);
    if (u1ProtoLength == 2)
    {
        u2Proto = OSIX_NTOHS (u2Proto);
    }
    if (u2Proto < 0x0021 || u2Proto > 0x00FA)
    {
        PPP_DBG ("Encryption not needed.");
        return FALSE;
    }
    u2Header = MPPE_ENCRYPTION_BIT | pHistoryInfo->u2CohCount;
    /*ReKey if stateless mode has been negotiated or
     * if the lower order octet of the coherency count equals 0xff or
     * if CCP Reset request packet has been received. */
    if ((pHistoryInfo->u1State == MPPE_STATELESS)
        || ((pHistoryInfo->u2CohCount & MPPE_FLAG_MASK) == MPPE_FLAG_MASK)
        || (pHistoryInfo->FlushReq == TRUE))
    {
        MppeGetNewKey (pHistoryInfo->au1MastKey, pHistoryInfo->au1SessKey,
                       pHistoryInfo->u1EncryptType);
        u2Header |= MPPE_FLUSHED_BIT;
        pHistoryInfo->FlushReq = FALSE;
    }

    u2Header = OSIX_HTONS (u2Header);
    Rc4ArSetup (&rc4Ctx, pHistoryInfo->au1SessKey, pHistoryInfo->u1KeyLen);
    Rc4ArCrypt (&rc4Ctx, pu1Clear, u4Len);
    CRU_BUF_Copy_OverBufChain (*pCruBuffer, pu1Clear, 0, u4Len);
    CRU_BUF_Prepend_BufChain (*pCruBuffer, (UINT1 *) &u2Header, 2);
    pHistoryInfo->u2CohCount++;
    pHistoryInfo->u2CohCount &= MPPE_COH_MASK;
    MEM_FREE (pu1Clear);
    return FALSE;
}

INT1
MppeDeCrypt (t_MSG_DESC ** pCruBuffer, VOID *pInfo)
{
    tMppeHistoryInfo   *pHistoryInfo;
    tArRc4State         rc4Ctx;
    UINT1              *pu1Clear;
    UINT2               u2Header = 0;
    UINT2               u2CohCount = 0;
    UINT4               u4Len = 0;
    UINT1               u1Flushed = 0;

    pHistoryInfo = (tMppeHistoryInfo *) pInfo;
    DEBUG_CHECK ((*pCruBuffer == NULL
                  || pHistoryInfo == NULL), "illegal input prmt value",
                 "MppeDeCrypt", ERROR);
    CRU_BUF_Copy_FromBufChain (*pCruBuffer, (UINT1 *) &u2Header, 0, 2);
    CRU_BUF_Move_ValidOffset (*pCruBuffer, 2);
    u4Len = CRU_BUF_Get_ChainValidByteCount (*pCruBuffer);
    pu1Clear = MEM_MALLOC (u4Len, UINT1);

    u2Header = OSIX_NTOHS (u2Header);
    u2CohCount = u2Header & MPPE_COH_MASK;
    if ((u2Header & MPPE_ENCRYPTION_BIT) != MPPE_ENCRYPTION_BIT)
    {
        PPP_DBG
            ("Invalid Packet : Encryption bit not set in encrypted packet.");
        MEM_FREE (pu1Clear);
        return FALSE;
    }
    if ((pHistoryInfo->u1State == MPPE_STATELESS)
        && ((u2Header & MPPE_FLUSHED_BIT) != MPPE_FLUSHED_BIT))
    {
        PPP_DBG ("Invalid Packet : Flushed bit not set in stateless mode.");
        MEM_FREE (pu1Clear);
        return FALSE;
    }
    if ((pHistoryInfo->u1State == MPPE_STATEFULL)
        && ((u2CohCount & MPPE_FLAG_MASK) == MPPE_FLAG_MASK)
        && ((u2Header & MPPE_FLUSHED_BIT) != MPPE_FLUSHED_BIT))
    {
        PPP_DBG ("Flushed bit not set on Flag Packet\n");
        MEM_FREE (pu1Clear);
        return FALSE;
    }
    if ((u2Header & MPPE_FLUSHED_BIT) == MPPE_FLUSHED_BIT)
        u1Flushed = 1;
    else
        u1Flushed = 0;

    if (pHistoryInfo->u1State == MPPE_STATELESS)
    {
        MppeGetNewKey (pHistoryInfo->au1MastKey, pHistoryInfo->au1SessKey,
                       pHistoryInfo->u1EncryptType);
        while (pHistoryInfo->u2CohCount != u2CohCount)
        {
            MppeGetNewKey (pHistoryInfo->au1MastKey, pHistoryInfo->au1SessKey,
                           pHistoryInfo->u1EncryptType);
            pHistoryInfo->u2CohCount++;
            pHistoryInfo->u2CohCount &= MPPE_COH_MASK;
        }
    }
    else
    {
        /* If received coh count is same as expected and no flag bit set, just 
         * decrypt the packet. If flag bit is set ReKey and then decrypt.
         * If lower order octet of the coherency count in the received packet,
         * is not equal to that in the expected packet, send CCP Reset request.
         * If Reset Request sent, and the received packet does not have the 
         * flushed bit set, ignore the packet, do not sent reset request again
         * If the Flushed bit is set rekey for each missed flag packet. */

        if (pHistoryInfo->ResetRequestSent == FALSE)
        {
            if (pHistoryInfo->u2CohCount != u2CohCount)
            {
                /* Send reset request */
                pHistoryInfo->ResetRequestSent = TRUE;
                MEM_FREE (pu1Clear);
                return FALSE;
            }
        }
        else
        {
            if (u1Flushed == 0)
            {
                /* We have sent Reset Request, hence,
                 * silently Ignore any packets which do not have flush bit set*/
                MEM_FREE (pu1Clear);
                return TRUE;
            }
            else
            {
                while ((pHistoryInfo->u2CohCount &= ~MPPE_FLAG_MASK) !=
                       (u2CohCount &= ~MPPE_FLAG_MASK))
                {
                    MppeGetNewKey (pHistoryInfo->au1MastKey,
                                   pHistoryInfo->au1SessKey,
                                   pHistoryInfo->u1EncryptType);
                    pHistoryInfo->u2CohCount =
                        (UINT2) (pHistoryInfo->u2CohCount + 256);
                    pHistoryInfo->u2CohCount &= MPPE_COH_MASK;
                }
                pHistoryInfo->ResetRequestSent = FALSE;
                pHistoryInfo->u2CohCount = (UINT2) (u2CohCount + 1);
            }
        }
        if (u1Flushed == 1)
        {
            MppeGetNewKey (pHistoryInfo->au1MastKey, pHistoryInfo->au1SessKey,
                           pHistoryInfo->u1EncryptType);
        }
    }
    CRU_BUF_Copy_FromBufChain (*pCruBuffer, pu1Clear, 0, u4Len);
    Rc4ArSetup (&rc4Ctx, pHistoryInfo->au1SessKey, pHistoryInfo->u1KeyLen);
    Rc4ArCrypt (&rc4Ctx, pu1Clear, u4Len);
    CRU_BUF_Copy_OverBufChain (*pCruBuffer, pu1Clear, 0, u4Len);
    pHistoryInfo->u2CohCount++;
    pHistoryInfo->u2CohCount &= MPPE_COH_MASK;
    MEM_FREE (pu1Clear);
    return TRUE;
}

/***********************************************************************/
/*                      INTERNAL  FUNCTIONS                            */
/**************************************************************************
*  Function Name : CCPProcessMPPEConfReq
*  Description   :
*              This function handles the config_request when MPPE is
*  negotiated as Option. This function calls MPPE function to validate
*  and verify the value of the parameters negotiated.
*
*  Parameter(s)  :
*            pGSEM            - Pointer to GSEM structure.
*            pInPkt            - Pointer to the incoming packet.
*            tOptVal            - Option value to be processed.    
*            Offset            - Indicates the place in the outgoing buffer
*                                  at which the option value is appended.
*            PresenceFlag    - Indicates whether the option is present in
*                                the received CONFIG_REQ packet
*
*  Return Values : Returns the number of bytes appended in the outgoing 
*                    buffer
***************************************************************************/
INT1
CCPProcessMPPEConfReq (tGSEM * pGSEM, t_MSG_DESC * pIn, tOptVal OptVal,
                       UINT2 Offset, UINT1 PresenceFlag)
{
    tMPPEParams        *pMPPEAcked, *pMPPEDesired = NULL;
    tHeader            *pHeader;
    t_MSG_DESC         *pOut;
    tCCPIf             *pCCPIf;
    INT2                RetVal;
    UINT1               u1StateModeRcvd = 0;
    UINT4               u4SupportBits = 0;
    UINT4               u4EncryptBitsRcvd = 0;
    UINT4               u4EncryptBitsRslt = 0;

    PPP_UNUSED (pIn);
    PPP_UNUSED (Offset);
    PPP_UNUSED (PresenceFlag);
    RetVal = ACK;
    pOut = pGSEM->pOutParam;
    pCCPIf = (tCCPIf *) pGSEM->pIf->pCCPIf;

    SLL_SCAN (&pCCPIf->OptionsAllowedForPeer, pHeader, tHeader *)
    {
        if (pHeader->Type == OPTION_MPPE)
        {
            pMPPEDesired = (tMPPEParams *) pHeader;
            u4SupportBits = OptVal.LongVal;
            u4EncryptBitsRcvd = (OptVal.LongVal & MPPE_ENCRYPT_BIT_MASK);
            u1StateModeRcvd =
                (UINT1) ((OptVal.LongVal & MPPE_STATE_MASK) >> 24);
            /* If none of the encryption bits match */
            if ((u4EncryptBitsRslt =
                 (pMPPEDesired->u1EncryptType & u4EncryptBitsRcvd)) == 0)
            {
                ASSIGN4BYTE (pGSEM->pOutParam, Offset,
                             pMPPEDesired->u4SupportBits);
                RetVal = BYTE_LEN_4;
            }
            else
            {
                if (u4EncryptBitsRcvd == MPPE_40BIT
                    || u4EncryptBitsRcvd == MPPE_56BIT
                    || u4EncryptBitsRcvd == MPPE_128BIT)
                {
                    if (u4EncryptBitsRcvd == MPPE_40BIT)
                    {
                        pMPPEDesired->u4SupportBits &= MPPE_40BIT_MASK;
                    }
                    else if (u4EncryptBitsRcvd == MPPE_56BIT)
                    {
                        pMPPEDesired->u4SupportBits &= MPPE_56BIT_MASK;
                    }
                    else if (u4EncryptBitsRcvd == MPPE_128BIT)
                    {
                        pMPPEDesired->u4SupportBits &= MPPE_128BIT_MASK;
                    }
                    /* If Mode is not stateless or MPPC is set */
                    if ((pMPPEDesired->u1State != u1StateModeRcvd) ||
                        (u4SupportBits & MPPC_BIT) ||
                        (u4SupportBits & OBSOLETE_BIT))
                    {
                        ASSIGN4BYTE (pGSEM->pOutParam, Offset,
                                     pMPPEDesired->u4SupportBits);
                        RetVal = BYTE_LEN_4;
                    }
                    else
                    {
                        RetVal = ACK;
                    }
                }
                else if ((u4EncryptBitsRslt & MPPE_128BIT) == MPPE_128BIT)
                {
                    pMPPEDesired->u4SupportBits &= MPPE_128BIT_MASK;
                    ASSIGN4BYTE (pGSEM->pOutParam, Offset,
                                 pMPPEDesired->u4SupportBits);
                    RetVal = BYTE_LEN_4;
                }
                else if ((u4EncryptBitsRslt & MPPE_56BIT) == MPPE_56BIT)
                {
                    pMPPEDesired->u4SupportBits &= MPPE_56BIT_MASK;
                    ASSIGN4BYTE (pGSEM->pOutParam, Offset,
                                 pMPPEDesired->u4SupportBits);
                    RetVal = BYTE_LEN_4;
                }
                else if ((u4EncryptBitsRslt & MPPE_40BIT) == MPPE_40BIT)
                {
                    pMPPEDesired->u4SupportBits &= MPPE_40BIT_MASK;
                    ASSIGN4BYTE (pGSEM->pOutParam, Offset,
                                 pMPPEDesired->u4SupportBits);
                    RetVal = BYTE_LEN_4;
                }
            }

            pMPPEDesired->u1State =
                (UINT1) ((pMPPEDesired->u4SupportBits & MPPE_STATE_MASK) >> 24);
            pMPPEDesired->u1EncryptType =
                (UINT1) (pMPPEDesired->u4SupportBits & MPPE_ENCRYPT_BIT_MASK);
        }
    }

    if (RetVal == ACK)
    {
        if ((pMPPEAcked = (tMPPEParams *) ALLOC_MPPE_PARAMS ()) == NULL)
        {
            PPP_TRC (ALL_FAILURE,
                     "ALLOC_MPPE_PARAMS failed in CCPProcessMPPEConfReq() ...");
            return DISCARD_OPT;
        }
        pMPPEAcked->Header.Type = OPTION_MPPE;
        pMPPEAcked->Header.Length = sizeof (tMPPEParams);
        if (pMPPEDesired != NULL)
        {
            pMPPEAcked->u4SupportBits = pMPPEDesired->u4SupportBits;
            pMPPEAcked->u1State = pMPPEDesired->u1State;
            pMPPEAcked->u1EncryptType = pMPPEDesired->u1EncryptType;
        }
        SLL_ADD (&pCCPIf->OptionsAckedByLocal,
                 (t_SLL_NODE *) & pMPPEAcked->Header);
    }
    return ((INT1) RetVal);
}

/**************************************************************************
*  Function Name : CCPProcessMPPEConfNak
*  Description   :
*              This function handles the config_Nak when MPPE is
*  negotiated as Option. This function copies the Nacked values into
*  AckedByPeer structure.
*
*  Parameter(s)  :
*            pGSEM            - Pointer to GSEM structure.
*            pInPkt            - Pointer to the incoming packet.
*            tOptVal            - Option value to be processed.    
*
*  Return Values : OK/NOT_OK
***************************************************************************/

INT1
CCPProcessMPPEConfNak (tGSEM * pGSEM, t_MSG_DESC * pIn, tOptVal OptVal)
{
    tMPPEParams        *pMPPEParams = NULL;
    tHeader            *pHeader;
    tCCPIf             *pCCPIf;

    PPP_UNUSED (pIn);

    pCCPIf = (tCCPIf *) pGSEM->pIf->pCCPIf;
    SLL_SCAN (&pCCPIf->OptionsAckedByPeer, pHeader, tHeader *)
    {
        if (pHeader->Type == OPTION_MPPE)
        {
            pMPPEParams = (tMPPEParams *) pHeader;
            pMPPEParams->u4SupportBits &= OptVal.LongVal;
            pMPPEParams->u1State =
                (UINT1) ((pMPPEParams->u4SupportBits & MPPE_STATE_MASK) >> 24);
            pMPPEParams->u1EncryptType =
                (UINT1) (pMPPEParams->u4SupportBits & MPPE_ENCRYPT_BIT_MASK);
        }
    }
    if ((pMPPEParams != NULL) && (pMPPEParams->u1EncryptType == 0))
    {
        /* If MPPE negotiation fails, bring the LCP connection down */
        PPPSendEventToPPPTask (pGSEM->pIf->LinkInfo.IfIndex, RXJ_MINUS);
        /* Send CCP terminate request */
        return CLOSE;
    }
    return OK;
}

/**************************************************************************
*  Function Name : CCPReturnMPPEAddrPtr
*  Description   :
*              This function returns the address of the MPPE Parameters.
*
*  Parameter(s)  :
*            pGSEM            - Pointer to GSEM structure.
*            OptLen            - Pointer to total Length of the parameters.
*
*  Return Values : VOID *, the pointer to MPPE Parameters.
***************************************************************************/
VOID               *
CCPReturnMPPEAddrPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    tMPPEParams        *pMPPEParams;
    tHeader            *pHeader;
    tCCPIf             *pCCPIf;

    PPP_UNUSED (OptLen);

    pCCPIf = (tCCPIf *) pGSEM->pIf->pCCPIf;
    SLL_SCAN (&pCCPIf->OptionsAckedByPeer, pHeader, tHeader *)
    {
        if (pHeader->Type == OPTION_MPPE)
        {
            pMPPEParams = (tMPPEParams *) pHeader;
            return (VOID *) &(pMPPEParams->u4SupportBits);
        }
    }
    return NULL;
}

VOID
MppeSetEncryptionLevel (UINT1 u1EncryptionLevel)
{
    switch (u1EncryptionLevel)
    {
        case MPPE_ENCRYPT_40:
            gu1EncryptionLevel = MPPE_40BIT;
            break;
        case MPPE_ENCRYPT_56:
            gu1EncryptionLevel = MPPE_56BIT | MPPE_40BIT;
            break;
        case MPPE_ENCRYPT_128:
            gu1EncryptionLevel = MPPE_128BIT | MPPE_56BIT | MPPE_40BIT;
            break;
    }
}
