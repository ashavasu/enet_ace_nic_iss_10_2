/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppptr69.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 *********************************************************************/

#include "pppcom.h"
#include "tr.h"
#include "tr69dim.h"
#include "ppptr69.h"
#include "gsemdefs.h"
#include "globexts.h"
#include "pppchap.h"
#include "ppppap.h"
#include "nat.h"
#include "pppeap.h"
#include "pppchap.h"
#include "pppauth.h"
#include "pppstlow.h"

/************************************************************************
 *  Function Name   : TrAdd_PPPConnection 
 *  Description     : Get function for InternetGatewayDevice_QueueManagement
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    idx1,... - list of TR indices.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
tPPPConnection     *
TrAdd_PPPConnection (tWANDevice * pDev, tWANConnDev * pWANConnDev,
                     UINT4 u4IfIndex)
{

    INT4                i4rc = 0;
    UINT4               u4TrIdx = 0;
    tPPPConnection     *pPPPConnection = NULL;

    CHR1                ac1ObjName[200];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    OctetStr.pu1_OctetList = au1OctetList;

    MEMSET (ac1ObjName, '\0', 200);

    SNPRINTF (ac1ObjName, sizeof (ac1ObjName), "%s%lu%s%lu%s",
              "InternetGatewayDevice.WANDevice.", pDev->u4TrInstance,
              ".WANConnectionDevice.", pWANConnDev->u4TrInstance,
              ".WANPPPConnection");

    i4rc = addObjectIntern (ac1ObjName, (int *) &u4TrIdx);

    if (i4rc != 0)
    {
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "addObjectIntern WANPPPCONNECTION failed, rc = %ld\n",
                       i4rc);
        return NULL;
    }

    pPPPConnection = MEM_MALLOC (sizeof (tPPPConnection), tPPPConnection);
    MEMSET (pPPPConnection, 0, sizeof (tPPPConnection));

    if (pPPPConnection == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "TrAdd_PPPConnection : Malloc failed \n");
        return NULL;
    }
    pPPPConnection->u4TrInstance = u4TrIdx;
    pPPPConnection->u4ValMask = PPP_CONNECTION_VAL_MASK;
    pPPPConnection->u4IfIndex = u4IfIndex;
    pPPPConnection->u4RowStatus = ACTIVE;

    /* Name in a PPP connection is obtained from nmhGetIfAlias only at the start. So this part of the code 
       has been added in TrAdd_PPPConnection itself */
    /*  If ifAlias ( Name ) is  set to a different value, then it is cached in pNode alone and returned to the user. 
       Snmp Set of ifAlias is not called */

    i4rc =
        MynmhGet (*Join (IfAlias, 11, IfXTableINDEX, 1, u4IfIndex),
                  SNMP_DATA_TYPE_OCTET_PRIM, &OctetStr);
    if (i4rc == SNMP_FAILURE)
    {
        TR69_TRC (TR69_DBG_TRC, "-E- Get failed for IfAlias\n");
        return NULL;
    }

    MEMCPY (pPPPConnection->au1Name, OctetStr.pu1_OctetList,
            OctetStr.i4_Length);

    TR69_TRC_ARG3 (TR69_DBG_TRC,
                   "TrAdd_PPPConnection : Adding PPPConnectionList"
                   "- TrIndex = %lu IfIndex = %lu Name = %s \n", u4TrIdx,
                   u4IfIndex, pPPPConnection->au1Name);

    TMO_SLL_Add (&pWANConnDev->PPPConnectionList, &pPPPConnection->Link);
    UTL_SLL_Init (&pPPPConnection->PortMappingList, 0);

    return pPPPConnection;

}

/************************************************************************
 *  Function Name   : TrSetIfMainAdminStatus
 *  Description     : 
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    idx1,... - list of TR indices.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrSetIfMainAdminStatus (UINT4 u4IfIndex, UINT4 u4Value)
{
    INT4                i4rc = 0;
    i4rc =
        MynmhTest (*Join
                   (IfMainAdminStatus, 12, IfMainTableINDEX, 1, u4IfIndex),
                   SNMP_DATA_TYPE_INTEGER, (VOID *) u4Value);
    if (i4rc == SNMP_FAILURE)
    {
        TR69_TRC (TR69_DBG_TRC, "-E- Test failed for IfMainAdminStatus\n");
        return -1;
    }

    i4rc =
        MynmhSet (*Join (IfMainAdminStatus, 12, IfMainTableINDEX, 1, u4IfIndex),
                  SNMP_DATA_TYPE_INTEGER, (VOID *) u4Value);
    if (i4rc == SNMP_FAILURE)
    {
        TR69_TRC (TR69_DBG_TRC, "-E- Set failed for IfMainAdminStatus\n");
        return -1;
    }
    return 0;
}

/************************************************************************
 *  Function Name   : InternetGatewayDevice_WANDevice_WANConnectionDevice_WANPPPConnection
 *  Description     : Get function for InternetGatewayDevice_WANDevice_WANConnectionDevice_WANPPPConnection
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    idx1,... - list of TR indices.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
int
TrGetInternetGatewayDevice_WANDevice_WANConnectionDevice_WANPPPConnection (char
                                                                           *name,
                                                                           int
                                                                           idx1,
                                                                           int
                                                                           idx2,
                                                                           int
                                                                           idx3,
                                                                           ParameterValue
                                                                           *
                                                                           value)
{
    UINT4               u4val = 0;
    INT4                i4IfMainWanType = 0;
    INT4                i4rc = 0;
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    /* INDICES: */
    UINT4               u4Idx0 = 0;
    UINT4               u4Idx1 = 0;
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
    UINT2               u2AuthProt = 0;

    tWANDevice         *pWANDevice = NULL;
    tWANDevice         *pNextWANDevice = NULL;
    tWANConnDev        *pWANConnectionDevice = NULL;
    tWANConnDev        *pNextWANConnectionDevice = NULL;
    tPPPConnection     *pPPPConnection = NULL;
    tPPPConnection     *pNextPPPConnection = NULL;
    tPPPConnection     *pNode = NULL;
    tPPPIf             *pIf = NULL;
    tIPCPIf            *pIpcpPtr = NULL;
    BOOL1               b1ValidateFlag = TRUE;
    CHR1               *pu1TempIp = NULL;
    tUtlInAddr          IpAddr;
    tTMO_SLL           *pWANConnectionDeviceList;
    tTMO_SLL           *pPPPConnectionList;

    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);

    OctetStr.pu1_OctetList = au1OctetList;
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;

    pWANConnectionDeviceList = NULL;
    UTL_SLL_OFFSET_SCAN (&WANDeviceList, pWANDevice, pNextWANDevice,
                         tWANDevice *)
    {
        if (pWANDevice->u4TrInstance == (UINT4) idx1)
        {
            pWANConnectionDeviceList = &pWANDevice->ConnDevList;
            break;
        }
    }

    if (pWANConnectionDeviceList == NULL)
    {
        TR69_TRC_ARG2 (TR69_DBG_TRC,
                       "-E- WANConnectionDevice %d.%d unmatched \n", idx1,
                       idx2);
        return -1;
    }

    pPPPConnectionList = NULL;
    UTL_SLL_OFFSET_SCAN (pWANConnectionDeviceList, pWANConnectionDevice,
                         pNextWANConnectionDevice, tWANConnDev *)
    {
        if (pWANConnectionDevice->u4TrInstance == (UINT4) idx2)
        {
            pPPPConnectionList = &pWANConnectionDevice->PPPConnectionList;
            break;
        }
    }

    if (pPPPConnectionList == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "-E- WANConnectionDeviceList is NULL");
        return -1;
    }

    UTL_SLL_OFFSET_SCAN (pPPPConnectionList, pPPPConnection, pNextPPPConnection,
                         tPPPConnection *)
    {
        if (pPPPConnection->u4TrInstance == (UINT4) idx3)
        {
            /* Get SNMP indices. */

            u4Idx0 = pPPPConnection->u4IfIndex;
            u4Idx1 = 1;
            break;
        }
    }

    if (pPPPConnection == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "-E- PPPconnectionList is NULL");
        return -1;
    }

    if (nmhValidateIndexInstanceIfMainTable (pWANDevice->u4IfIdx) !=
        SNMP_SUCCESS)
    {
        i4rc = TrGetNonWANDevice_ppp (name, value);
        return i4rc;
    }

    /* Checking if interface is WAN or not */
    nmhGetIfMainWanType (pWANDevice->u4IfIdx, &i4IfMainWanType);
    if (i4IfMainWanType != CFA_WAN_TYPE_PUBLIC)
    {
        i4rc = TrGetNonWANDevice_ppp (name, value);
        return i4rc;
    }

    pNode = pPPPConnection;

    /* If PPP connection is deleted from other management servers like CLI,SNMP 
       and then if TrGet is accessed ,the exe might crash . SO validate routines
       are added to check if appropriate entries are present */

    if (nmhValidateIndexInstanceIfMainTable (u4Idx0) == SNMP_FAILURE)
    {
        b1ValidateFlag = FALSE;
    }

    if (STRCMP (name, "Enable") == 0)
    {
        if ((pNode->u4ValMask & PPP_CONNECTION_VAL_MASK) ==
            PPP_CONNECTION_VAL_MASK)
        {
            if (b1ValidateFlag == FALSE)
            {
                value->out_uint = FALSE;
                return 0;
            }

            i4rc =
                MynmhGet (*Join
                          (IfMainAdminStatus, 12, IfMainTableINDEX, 1, u4Idx0),
                          SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- Get failed for IfMainAdminStatus\n");
                return -1;
            }

            /* AdminStatus CFA_IF_UP is mapped to 1 and CFA_IF_DOWN to 0 as per TR spec */

            if (u4val == CFA_IF_DOWN)
            {
                value->out_uint = FALSE;
            }
            if (u4val == CFA_IF_UP)
            {
                value->out_uint = TRUE;
            }
            pNode->u4Enable = u4val;
            TR69_TRC_ARG2 (TR69_DBG_TRC,
                           "Get: Enable ==> IfMainAdminStatus ret  %lu val = %lu\n",
                           value->out_uint, u4val);
            return 0;
        }
        else
        {
            value->out_uint = pNode->u4Enable;
            TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: IfMainAdminStatus ret %d\n",
                           u4val);
            return 0;
        }
    }

    else if (STRCMP (name, "ConnectionStatus") == 0)
    {
        if ((pNode->u4ValMask & PPP_CONNECTION_VAL_MASK) ==
            PPP_CONNECTION_VAL_MASK)
        {
            if (b1ValidateFlag == FALSE)
            {
                value->out_cval = (UINT1 *) "";
                return 0;
            }

            /* Get the Admin Status of the PPP Connection. If it is CFA_IF_DOWN then ConnectionStatus is 
               mapped to "Unconfigured" as per TR spec 
               else
               Get the status from tPPPIf structure.
               If LCP status is ReqSent,then ConnectionStatus is mapped to "Disconnecting" as per TR spec. 
               If Auth status is ,then ConnectionStatus is mapped to "Authenticating" as per TR spec. 

             */

            nmhGetIfMainAdminStatus (u4Idx0, (INT4 *) &u4val);
            if (u4val == CFA_IF_DOWN)
            {
                value->out_cval = (UINT1 *) "Unconfigured";
            }
            else
            {
                pIf = PppGetIfPtr (u4Idx0);
                pIpcpPtr = pIf->pIpcpPtr;
                if (pIf != NULL)
                {
                    if (pIf->LcpStatus.OperStatus == STATUS_DOWN)
                    {
                        value->out_cval = (UINT1 *) "Disconnected";
                    }
                    else if (pIf->LcpGSEM.CurrentState == CLOSING)
                    {
                        value->out_cval = (UINT1 *) "Disconnecting";
                    }
                    else if (pIf->LcpGSEM.CurrentState == STOPPING)
                    {
                        value->out_cval = (UINT1 *) "PendingDisconnect";
                    }
                    else if (pIpcpPtr != NULL)
                    {
                        if (pIpcpPtr->OperStatus == STATUS_UP)
                        {
                            value->out_cval = (UINT1 *) "Connected";
                        }
                    }
                    else
                    {
                        if (pIf->pAuthPtr != NULL)
                        {
                            if (pIf->pAuthPtr->ClientProt == CHAP_PROTOCOL)
                            {
                                if ((pIf->pAuthPtr->ClientInfo.ClientChap.
                                     State == CHAP_CLIENT_STATE_PENDING)
                                    || (pIf->pAuthPtr->ClientInfo.ClientChap.
                                        State == CHAP_CLIENT_STATE_INITIAL)
                                    || (pIf->pAuthPtr->ClientInfo.ClientChap.
                                        State == CHAP_CLIENT_STATE_LISTEN))
                                {
                                    value->out_cval =
                                        (UINT1 *) "Authenticating";
                                }
                            }
                            if (pIf->pAuthPtr->ClientProt == PAP_PROTOCOL)
                            {
                                if ((pIf->pAuthPtr->ClientInfo.ClientPap.
                                     State == PAP_CLIENT_STATE_PENDING)
                                    || (pIf->pAuthPtr->ClientInfo.ClientPap.
                                        State == PAP_CLIENT_STATE_AUTHREQ)
                                    || (pIf->pAuthPtr->ClientInfo.ClientPap.
                                        State == PAP_CLIENT_STATE_INITIAL))
                                {
                                    value->out_cval =
                                        (UINT1 *) "Authenticating";
                                }
                            }
                        }
                    }
                }
            }
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "TrGet : ConnectionStatus returns %s \n",
                           value->out_cval);
            return 0;
        }
    }

    else if (STRCMP (name, "PossibleConnectionTypes") == 0)
    {
        /* Only IP_Routed type is supported */

        value->out_cval = (UINT1 *) "IP_Routed";
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: PossibleConnectionTypes  ret %s\n",
                       value->out_cval);
        return 0;
    }

    else if (STRCMP (name, "ConnectionType") == 0)
    {
        if (b1ValidateFlag == FALSE)
        {
            value->out_cval = (UINT1 *) "";
            return 0;
        }
        if ((pNode->u4ValMask & PPP_CONNECTION_VAL_MASK) ==
            PPP_CONNECTION_VAL_MASK)
        {

            i4rc =
                MynmhGet (*Join
                          (IfIvrBridgedIface, 12, IfIvrTableINDEX, 1, u4Idx0),
                          SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- Get failed for IfIvrBridgedIface\n");
                return -1;
            }
            if (u4val == CFA_DISABLED)
            {
                value->out_cval = (UINT1 *) "IP_Routed";

            }
            else
            {
                value->out_cval = (UINT1 *) " ";
            }
            pNode->u4ConnectionType = u4val;
            TR69_TRC_ARG2 (TR69_DBG_TRC,
                           "Get: ConnectionType == >IfIvrBridgedIface ret %s val =  %d\n",
                           value->out_cval, u4val);
            return 0;
        }
        else
        {
            if (pNode->u4ConnectionType == CFA_ENABLED)
            {
                value->out_cval = (UINT1 *) "IP_Routed";
            }
            else
            {
                value->out_cval = (UINT1 *) " ";
            }
            TR69_TRC_ARG2 (TR69_DBG_TRC,
                           "Get: ConnectionType == >IfIvrBridgedIface ret %s val =  %d\n",
                           value->out_cval, u4val);
            return 0;
        }
    }
    else if (STRCMP (name, "Name") == 0)
    {
        /* Name in a PPP connection is obtained from nmhGetIfAlias only at the start. If ifAlias ( Name ) is 
           set to a different value, then it is cached in pNode alone and returned to the user. Snmp Set 
           of ifAlias is not called */

        if ((pNode->u4ValMask & PPP_CONNECTION_VAL_MASK) ==
            PPP_CONNECTION_VAL_MASK)
        {
            pIf = PppGetIfPtr (u4Idx0);
            if (pIf == NULL)
            {
                value->out_cval = (UINT1 *) "";
                return 0;
            }
            value->out_cval = pNode->au1Name;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: Name ==> IfAlias ret %s\n", value->out_cval);
            return 0;
        }
    }
    else if (STRCMP (name, "Uptime") == 0)
    {
        if ((pNode->u4ValMask & PPP_CONNECTION_VAL_MASK) ==
            PPP_CONNECTION_VAL_MASK)
        {
            if (b1ValidateFlag == FALSE)
            {
                value->out_uint = 0;
                return 0;
            }

            i4rc =
                MynmhGet (*Join (IfLastChange, 10, IfTableINDEX, 1, u4Idx0),
                          SNMP_DATA_TYPE_TIME_TICKS, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC, "-E- Get failed for IfLastChange\n");
                return -1;
            }
            u4val = u4val / SYS_TIME_TICKS_IN_A_SEC;
            value->out_uint = u4val;
            pNode->u4Uptime = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: Uptime ===> IfLastChange ret secs = %lu \n",
                           u4val);
            return 0;
        }
        else
        {
            value->out_uint = pNode->u4Uptime;
            TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: IfLastChange ret secs = %lu\n",
                           u4val);
            return 0;
        }
    }
    else if (STRCMP (name, "NATEnabled") == 0)
    {
        if ((pNode->u4ValMask & PPP_CONNECTION_VAL_MASK) ==
            PPP_CONNECTION_VAL_MASK)
        {

            i4rc =
                MynmhGet (*Join (NatIfNat, 12, NatIfTableINDEX, 1, u4Idx0),
                          SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC, "-E- Get failed for NatIfNat\n");
                return -1;
            }
            if (u4val == NAT_ENABLE)
            {
                value->out_uint = TRUE;
            }
            if (u4val == NAT_DISABLE)
            {
                value->out_uint = FALSE;
            }
            pNode->u4NATEnabled = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: NATEnabled ==> NatIfNat ret %d\n", u4val);
            return 0;
        }
        else
        {
            value->out_uint = pNode->u4NATEnabled;
            TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: NatIfNat ret %d\n", u4val);
            return 0;
        }
    }
    else if (STRCMP (name, "Username") == 0)
    {
        if ((pNode->u4ValMask & PPP_CONNECTION_VAL_MASK) ==
            PPP_CONNECTION_VAL_MASK)
        {

            i4rc =
                MynmhGet (*Join
                          (PppSecuritySecretsIdentity, 12,
                           PppSecuritySecretsTableINDEX, 2, u4Idx0, u4Idx1),
                          SNMP_DATA_TYPE_OCTET_PRIM, &OctetStr);

            if (i4rc == SNMP_FAILURE)
            {
                /* If user name , and password has not been configured by the 
                   user ,then empty string is returned */
                pIf = PppGetIfPtr (u4Idx0);
                if ((pIf == NULL)
                    || (TMO_SLL_Count (&pIf->pAuthPtr->SecurityInfo.SecretList))
                    == 0)
                {
                    value->out_cval = (UINT1 *) "";
                    return 0;
                }
                TR69_TRC (TR69_DBG_TRC,
                          "-E- Get failed for PppSecuritySecretsIdentity\n");
                return -1;
            }
            MEMCPY (pNode->au1Username, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);

            value->out_cval = pNode->au1Username;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: Username ==> PppSecuritySecretsIdentity ret %s\n",
                           value->out_cval);
            return 0;
        }
        else
        {
            value->out_cval = pNode->au1Username;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: PppSecuritySecretsIdentity ret %s\n",
                           value->out_cval);
            return 0;
        }
    }
    else if (STRCMP (name, "Password") == 0)
    {
        if ((pNode->u4ValMask & PPP_CONNECTION_VAL_MASK) ==
            PPP_CONNECTION_VAL_MASK)
        {

            i4rc =
                MynmhGet (*Join
                          (PppSecuritySecretsSecret, 12,
                           PppSecuritySecretsTableINDEX, 2, u4Idx0, u4Idx1),
                          SNMP_DATA_TYPE_OCTET_PRIM, &OctetStr);
            if (i4rc == SNMP_FAILURE)
            {
                /* If user name , and password has not been configured by the 
                   user ,then empty string is returned */

                pIf = PppGetIfPtr (u4Idx0);
                if ((pIf == NULL)
                    || (TMO_SLL_Count (&pIf->pAuthPtr->SecurityInfo.SecretList))
                    == 0)
                {
                    value->out_cval = (UINT1 *) "";
                    return 0;
                }

                TR69_TRC (TR69_DBG_TRC,
                          "-E- Get failed for PppSecuritySecretsSecret\n");
                return -1;
            }

            MEMCPY (pNode->au1Password, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);

            /* As per TR spec,password must not be displayed */

            value->out_cval = (UINT1 *) "";
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: Password ==> PppSecuritySecretsSecret ret %s\n",
                           value->out_cval);
            return 0;

        }
        else
        {
            value->out_cval = (UINT1 *) "";
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: PppSecuritySecretsSecret ret %s\n",
                           value->out_cval);
            return 0;
        }
    }
    else if (STRCMP (name, "PPPEncryptionProtocol") == 0)
    {
        pIf = PppGetIfPtr (u4Idx0);
        if (pIf == NULL)
        {
            value->out_cval = (UINT1 *) "";
            return 0;
        }
        value->out_cval = (UINT1 *) "None";
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: PPPEncryptionProtocol ret %s\n", value->out_cval);
        return 0;
    }
    else if (STRCMP (name, "PPPAuthenticationProtocol") == 0)
    {
        pIf = PppGetIfPtr (u4Idx0);
        if (pIf != NULL)
        {
            if (pIf->pAuthPtr != NULL)
            {
                u2AuthProt = pIf->pAuthPtr->ClientProt;
                if (u2AuthProt == CHAP_PROTOCOL)
                {
                    value->out_cval = (UINT1 *) "CHAP";
                }
                if (u2AuthProt == PAP_PROTOCOL)
                {
                    value->out_cval = (UINT1 *) "PAP";
                }
                pNode->u4PPPAuthProto = u2AuthProt;
                TR69_TRC_ARG1 (TR69_DBG_TRC,
                               "Get: PPPAuthenticationProtocol ret %s\n",
                               value->out_cval);
            }
            else
            {
                TR69_TRC (TR69_DBG_TRC, "PPP AUTH INFO NOT YET FILLED !!!\n");
            }
        }
        else
        {
            value->out_cval = (UINT1 *) "";
        }
        return 0;
    }
    else if (STRCMP (name, "ExternalIPAddress") == 0)
    {
        if ((pNode->u4ValMask & PPP_CONNECTION_VAL_MASK) ==
            PPP_CONNECTION_VAL_MASK)
        {
            pIf = PppGetIfPtr (u4Idx0);
            if (pIf == NULL)
            {
                value->out_cval = (UINT1 *) "";
                return 0;
            }

            i4rc =
                MynmhGet (*Join (IfIpAddr, 12, IfIpTableINDEX, 1, u4Idx0),
                          SNMP_DATA_TYPE_IP_ADDR_PRIM, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC, "-E- Get failed for IfIpAddr\n");
                return -1;
            }
            MEMSET (pNode->au1ExternalIPAddress, '\0', 16);
            IpAddr.u4Addr = OSIX_NTOHL (u4val);
            pu1TempIp = UtlInetNtoa (IpAddr);
            SNPRINTF ((CHR1 *) pNode->au1ExternalIPAddress,
                      sizeof (pNode->au1ExternalIPAddress), "%s", pu1TempIp);
            value->out_cval = pNode->au1ExternalIPAddress;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: ExternalIPAddress ==> IfIpAddr ret %s\n",
                           value->out_cval);
            return 0;
        }
        else
        {
            value->out_cval = pNode->au1ExternalIPAddress;
            TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: IfIpAddr ret %s\n",
                           value->out_cval);
            return 0;
        }
    }
    else if (STRCMP (name, "MaxMRUSize") == 0)
    {
        if ((pNode->u4ValMask & PPP_CONNECTION_VAL_MASK) ==
            PPP_CONNECTION_VAL_MASK)
        {
            if (b1ValidateFlag == FALSE)
            {
                value->out_uint = 0;
                return 0;
            }

            i4rc =
                MynmhGet (*Join (IfMainMtu, 12, IfMainTableINDEX, 1, u4Idx0),
                          SNMP_DATA_TYPE_INTEGER32, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC, "-E- Get failed for IfMainMtu\n");
                return -1;
            }
            value->out_uint = u4val;
            pNode->u4MaxMRUSize = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "1. Get: MaxMRUSize ==> IfMainMtu ret %d\n", u4val);
            return 0;
        }
        else
        {
            value->out_uint = pNode->u4MaxMRUSize;
            TR69_TRC_ARG1 (TR69_DBG_TRC, "2. Get: IfMainMtu ret %d\n", u4val);
            return 0;
        }
    }
    else if (STRCMP (name, "CurrentMRUSize") == 0)
    {
        if ((pNode->u4ValMask & PPP_CONNECTION_VAL_MASK) ==
            PPP_CONNECTION_VAL_MASK)
        {
            if (b1ValidateFlag == FALSE)
            {
                value->out_uint = 0;
                return 0;
            }

            i4rc =
                MynmhGet (*Join (IfMainMtu, 12, IfMainTableINDEX, 1, u4Idx0),
                          SNMP_DATA_TYPE_INTEGER32, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC, "-E- Get failed for IfMainMtu\n");
                return -1;
            }
            value->out_uint = u4val;
            pNode->u4CurrentMRUSize = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: CurrentMRUSize ==> IfMainMtu ret %d\n", u4val);
            return 0;
        }
        else
        {
            value->out_uint = pNode->u4CurrentMRUSize;
            TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: IfMainMtu ret %d\n", u4val);
            return 0;
        }
    }
#ifdef LNXAPP_WANTED
    else if (STRCMP (name, "DNSEnabled") == 0)
    {
        nmhGetLnxappDnsStatus ((INT4 *) &u4val);
        pNode->u4DNSEnabled = u4val;
        if (u4val == TR_ENABLE)
        {
            value->out_uint = TRUE;
        }
        if (u4val == TR_DISABLE)
        {
            value->out_uint = FALSE;
        }
        TR69_TRC_ARG1 (TR69_DBG_TRC, "1.Get: DNSEnabled ==>  ret %lu\n", u4val);
        return 0;
    }
#endif
    else if (STRCMP (name, "TransportType") == 0)
    {
        if ((pNode->u4ValMask & PPP_CONNECTION_VAL_MASK) ==
            PPP_CONNECTION_VAL_MASK)
        {
            pIf = PppGetIfPtr (u4Idx0);
            if (pIf == NULL)
            {
                value->out_cval = (UINT1 *) "";
                return 0;
            }

            i4rc =
                MynmhGet (*Join
                          (PppExtLinkConfigLowerIfType, 13,
                           PppExtLinkConfigTableINDEX, 1, u4Idx0),
                          SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- Get failed for PppExtLinkConfigLowerIfType\n");
                return -1;
            }
            if (u4val == PPTP_IF_TYPE)
            {
                value->out_cval = (UINT1 *) "PPTP";
            }
            if (u4val == PPP_OE_IFTYPE)
            {
                value->out_cval = (UINT1 *) "PPPoE";
            }
            if (u4val == L2TP_IF_TYPE)
            {
                value->out_cval = (UINT1 *) "L2TP";
            }
            pNode->u4TransportType = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "1.Get: TransportType ret %s\n", value->out_cval);
            return 0;
        }
        else
        {
            if (pNode->u4TransportType == PPTP_IF_TYPE)
            {
                value->out_cval = (UINT1 *) "PPTP";
            }
            if (pNode->u4TransportType == PPP_OE_IFTYPE)
            {
                value->out_cval = (UINT1 *) "PPPoE";
            }
            if (pNode->u4TransportType == L2TP_IF_TYPE)
            {
                value->out_cval = (UINT1 *) "L2TP";
            }
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "2. Get: TransportType ret %s\n", value->out_cval);
            return 0;
        }
    }
    else if (STRCMP (name, "ConnectionTrigger") == 0)
    {
        pIf = PppGetIfPtr (u4Idx0);
        if (pIf == NULL)
        {
            value->out_cval = (UINT1 *) "";
            return 0;
        }
        value->out_cval = (UINT1 *) "AlwaysOn";
        pNode->u4ConnectionTrigger = CFA_PERS_DEMAND;
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: ConnectionTrigger ret %s \n", value->out_cval);
        return 0;
    }
    return i4rc;
}

/************************************************************************
 *  Function Name   : InternetGatewayDevice_WANDevice_WANConnectionDevice_WANPPPConnection
 *  Description     : Get function for InternetGatewayDevice_WANDevice_WANConnectionDevice_WANPPPConnection
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    idx1,... - list of TR indices.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
int
TrSetInternetGatewayDevice_WANDevice_WANConnectionDevice_WANPPPConnection (char
                                                                           *name,
                                                                           int
                                                                           idx1,
                                                                           int
                                                                           idx2,
                                                                           int
                                                                           idx3,
                                                                           ParameterValue
                                                                           *
                                                                           value)
{
    UINT4               u4val = 0;
    UINT4               u4MakeActive = 0;
    INT4                i4rc = 0;
    INT4                i4IfMainWanType = 0;
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    /* INDICES: */
    UINT4               u4Idx0 = 0;
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
    UINT1               u1IfType = 0;
    INT4                i4IfMainRowStatus = 0;
    INT4                i4NatIfEntryStatus = 0;
    tWANDevice         *pWANDevice = NULL;
    tWANDevice         *pNextWANDevice = NULL;
    tWANConnDev        *pWANConnectionDevice = NULL;
    tWANConnDev        *pNextWANConnectionDevice = NULL;
    tPPPConnection     *pPPPConnection = NULL;
    tPPPConnection     *pNextPPPConnection = NULL;
    tPPPConnection     *pNode = NULL;
    tTMO_SLL           *pWANConnectionDeviceList;
    tTMO_SLL           *pPPPConnectionList;

    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);

    OctetStr.pu1_OctetList = au1OctetList;
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;

    pWANConnectionDeviceList = NULL;
    UTL_SLL_OFFSET_SCAN (&WANDeviceList, pWANDevice, pNextWANDevice,
                         tWANDevice *)
    {
        if (pWANDevice->u4TrInstance == (UINT4) idx1)
        {
            pWANConnectionDeviceList = &pWANDevice->ConnDevList;
            break;
        }
    }

    if (pWANConnectionDeviceList == NULL)
    {
        TR69_TRC_ARG2 (TR69_DBG_TRC,
                       "-E- WANConnectionDevice %d.%d unmatched \n", idx1,
                       idx2);
        return -1;
    }

    pPPPConnectionList = NULL;
    UTL_SLL_OFFSET_SCAN (pWANConnectionDeviceList, pWANConnectionDevice,
                         pNextWANConnectionDevice, tWANConnDev *)
    {
        if (pWANConnectionDevice->u4TrInstance == (UINT4) idx2)
        {
            pPPPConnectionList = &pWANConnectionDevice->PPPConnectionList;
            break;
        }
    }

    if (pPPPConnectionList == NULL)
    {
        TR69_TRC_ARG2 (TR69_DBG_TRC,
                       "-E- PPPConnection %d.%d unmatched \n", idx1, idx2);
        return -1;
    }

    UTL_SLL_OFFSET_SCAN (pPPPConnectionList, pPPPConnection, pNextPPPConnection,
                         tPPPConnection *)
    {
        if (pPPPConnection->u4TrInstance == (UINT4) idx3)
        {
            u4Idx0 = pPPPConnection->u4IfIndex;
            break;
        }
    }

    if (pPPPConnection == NULL)
    {
        return -1;
    }

    if (nmhValidateIndexInstanceIfMainTable (pWANDevice->u4IfIdx) !=
        SNMP_SUCCESS)
    {
        i4rc = TrSetNonWANDevice_ppp (name);
        return i4rc;
    }

    nmhGetIfMainWanType (pWANDevice->u4IfIdx, &i4IfMainWanType);

    if (i4IfMainWanType != CFA_WAN_TYPE_PUBLIC)
    {
        i4rc = TrSetNonWANDevice_ppp (name);
        return i4rc;
    }

    pNode = pPPPConnection;

    u4MakeActive = 0;

    /* Get the rowstatus of  Nat Table */
    nmhGetNatIfEntryStatus (u4Idx0, &i4NatIfEntryStatus);

    if ((pNode->u4ValMask & PPP_CONNECTION_VAL_MASK) == PPP_CONNECTION_VAL_MASK)
    {

        /* If the object to be set belongs to ifMainTable , check if the PPP interface is attached to some physical 
           interface. If not, return ERROR  */

        if (STRCMP (name, "MaxMRUSize") == 0)
        {
            i4rc = nmhGetIfMainRowStatus (u4Idx0, &i4IfMainRowStatus);
            if (i4rc == SNMP_FAILURE)
            {
                return -1;
            }
            CfaGetIfType (u4Idx0, &u1IfType);

            if ((u1IfType == CFA_PPP) && (i4IfMainRowStatus != ACTIVE))
            {
                TR69_TRC (TR69_DBG_TRC, "Attach the PPP interface to an"
                          "underlying physical interface first\r\n");
                return -1;
            }

        }

        u4MakeActive = 1;
    }
    i4rc =
        TrSetNonMand_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANPPPConnection
        (name, value, pNode);
    if (i4rc == TR_NOT_MAND_PARAM)
    {
        TR69_TRC (TR69_DBG_TRC, " CALLING SET MAND PARAM - ERROR !!! \n");
        /*i4rc = TrSetMand_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANPPPConnection (name, value, pNode); */
    }
    if (u4MakeActive)
    {
        /* Set RowStatus back to ACTIVE */
        u4val = ACTIVE;
        u4Idx0 = pNode->u4IfIndex;
        if (STRCMP (name, "NATEnabled") == 0)
        {
            i4rc = MynmhTest (*Join (NatIfEntryStatus, 12,
                                     NatIfTableINDEX, 1, u4Idx0),
                              SNMP_DATA_TYPE_INTEGER32, (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSet : Test failed for NatIfEntryStatus\n");
                return -1;
            }
            i4rc = MynmhSet (*Join (NatIfEntryStatus, 12,
                                    NatIfTableINDEX, 1, u4Idx0),
                             SNMP_DATA_TYPE_INTEGER32, (VOID *) u4val);

            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSet : Set failed for NatIfEntryStatus\n");
                return -1;
            }
        }

    }
    return i4rc;
}

/************************************************************************
 *  Function Name   : TrSetNonMand_InternetGatewayDevice_WANDevice_WANConnectionDevice_WAN-PPPConnection
 *  Description     : Set function for non-mandatory objects.
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    pNode - Pointer to a tPPPConnection entry.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4 
     
     
     
     
     
     
     
    TrSetNonMand_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANPPPConnection
    (char *name, ParameterValue * value, tPPPConnection * pNode)
{
    INT4                i4rc = 0;
    UINT4               u4val = 0;
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    /* INDICES: */
    UINT4               u4Idx0 = 0;
    UINT4               u4Idx1 = 0;
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
    UINT4               u4ErrorCode = 0;
    UINT4               u4NatStatus = 0;
    INT4                i4RetVal = 0;
    INT4                i4RowStatus = 0;
    INT4                i4AdminStatusFlag = FALSE;
    tPPPIf             *pIf = NULL;

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;
    OctetStr.pu1_OctetList = au1OctetList;

    /*  Copy the SNMP indices (u4Idx0) */

    u4Idx0 = pNode->u4IfIndex;

    if (STRCMP (name, "Enable") == 0)
    {
        if ((pNode->u4ValMask & PPP_CONNECTION_VAL_MASK) ==
            PPP_CONNECTION_VAL_MASK)
        {
            if (value->in_uint == FALSE)
            {
                u4val = CFA_IF_DOWN;
            }
            if (value->in_uint == TRUE)
            {
                u4val = CFA_IF_UP;
            }
            pNode->u4Enable = u4val;

            i4rc = TrSetIfMainAdminStatus (u4Idx0, u4val);

            return i4rc;
        }
    }
    else if (STRCMP (name, "ConnectionType") == 0)
    {
        if ((pNode->u4ValMask & PPP_CONNECTION_VAL_MASK) ==
            PPP_CONNECTION_VAL_MASK)
        {
            /*  Convert value->in_cval to u4val */
            if (STRCMP (value->in_cval, "IP_Routed") == 0)
            {
                u4val = CFA_DISABLED;
            }
            else
            {
                TR69_TRC (TR69_DBG_TRC,
                          "%% ERROR - Unsupported Connection Type \n");;
            }
            pNode->u4ConnectionType = u4val;

            return 0;
        }
    }
    else if (STRCMP (name, "Name") == 0)
    {
        OctetStr.i4_Length = STRLEN (value->in_cval);
        MEMCPY (OctetStr.pu1_OctetList, value->in_cval, OctetStr.i4_Length);
        MEMSET (pNode->au1Name, 0, PPPCONNECTION_NAME);
        MEMCPY (pNode->au1Name, OctetStr.pu1_OctetList, OctetStr.i4_Length);
        return 0;
    }
    else if (STRCMP (name, "NATEnabled") == 0)
    {
        if ((pNode->u4ValMask & PPP_CONNECTION_VAL_MASK) ==
            PPP_CONNECTION_VAL_MASK)
        {
            /* 0 is mapped to NAT_DISABLE and 1 to NAT_ENABLE according to TR spec */
            if (value->in_uint == TRUE)
            {
                u4val = NAT_ENABLE;
            }
            if (value->in_uint == FALSE)
            {
                u4val = NAT_DISABLE;
            }
            pNode->u4NATEnabled = u4val;

            /* The foll code  is similar to the implementation of API NatCliAddNatInterfaceEntry */

            /* Get the row status of Nat Table. If an entry doesnot exist , create a new one for 
               the incoming IfIndex . If an entry already exists, make the row status to NOT_IN_SERVICE */

            i4RetVal = nmhGetNatIfEntryStatus (u4Idx0, &i4RowStatus);

            if (i4RetVal != SNMP_SUCCESS)
            {
                if (nmhTestv2NatIfEntryStatus
                    (&u4ErrorCode, u4Idx0, CREATE_AND_WAIT) != SNMP_SUCCESS)
                {
                    TR69_TRC (TR69_DBG_TRC, "TrSetNonMand: Failed for"
                              "C&W in nmhTestv2NatIfEntryStatus\n");
                    return -1;
                }

                if (nmhSetNatIfEntryStatus (u4Idx0, CREATE_AND_WAIT) !=
                    SNMP_SUCCESS)
                {
                    TR69_TRC (TR69_DBG_TRC, "TrSetNonMand: Failed for"
                              "C&W in nmhSetNatIfEntryStatus\n");
                    return -1;
                }
            }
            else if (i4RetVal == SNMP_SUCCESS)
            {
                if (nmhTestv2NatIfEntryStatus
                    (&u4ErrorCode, u4Idx0, NOT_IN_SERVICE) != SNMP_SUCCESS)
                {
                    TR69_TRC (TR69_DBG_TRC,
                              "TrSetNonMand: Failed for NIS in nmhTestv2NatIfEntryStatus\n");
                    return -1;
                }

                if (nmhSetNatIfEntryStatus (u4Idx0, NOT_IN_SERVICE) !=
                    SNMP_SUCCESS)
                {
                    TR69_TRC (TR69_DBG_TRC,
                              "TrSetNonMand: Failed for NIS in nmhSetNatIfEntryStatus\n");
                    return -1;
                }
            }

            /* Set the status of Nat */

            i4rc =
                MynmhTest (*Join (NatIfNat, 12, NatIfTableINDEX, 1, u4Idx0),
                           SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E-TrNonMand :  Test failed for NatIfNat\n");
                return -1;
            }

            i4rc =
                MynmhSet (*Join (NatIfNat, 12, NatIfTableINDEX, 1, u4Idx0),
                          SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrNonMand  : Set failed for NatIfNat\n");
                return -1;
            }
            /* NAPT can be set only if NAT is enabled on the interface */
            if (u4val == NAT_ENABLE)
            {
                nmhGetNatIfNat (u4Idx0, (INT4 *) &u4NatStatus);

                if (u4NatStatus == NAT_ENABLE)
                {
                    nmhSetNatIfNapt (u4Idx0, u4val);
                }
                else
                {
                    TR69_TRC (TR69_DBG_TRC,
                              "TrSetNonMand: Failed in nmhSetNatIfNapt\n");
                    return -1;
                }

            }
            else if (u4val == NAT_DISABLE)
            {
                nmhSetNatIfNapt (u4Idx0, u4val);
            }

            nmhGetNatIfNat (u4Idx0, (INT4 *) &u4NatStatus);

            if (u4NatStatus == NAT_ENABLE)
            {
                nmhSetNatIfTwoWayNat (u4Idx0, u4NatStatus);
            }
            else
            {
                TR69_TRC (TR69_DBG_TRC,
                          "TrSetNonMand: Failed in nmhSetNatIfTwoWayNat\n");
                return -1;
            }
            return 0;
        }

    }
#ifdef LNXAPP_WANTED
    else if (STRCMP (name, "DNSEnabled") == 0)
    {
        if (value->in_uint == FALSE)
        {
            u4val = TR_DISABLE;
        }
        if (value->in_uint == TRUE)
        {
            u4val = TR_ENABLE;
        }
        pNode->u4DNSEnabled = u4val;
        if (nmhTestv2LnxappDnsStatus (&u4ErrorCode, u4val) == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "TrSetNonMand : Failed in Test Dns Status \n");
            return -1;
        }
        i4rc = nmhSetLnxappDnsStatus (u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "TrSetNonMand : Failed in Set Dns Status \n");
            return -1;
        }
        return 0;
    }
#endif
    else if (STRCMP (name, "Username") == 0)
    {
        if ((pNode->u4ValMask & PPP_CONNECTION_VAL_MASK) ==
            PPP_CONNECTION_VAL_MASK)
        {
            if ((pIf = PppGetIfPtr (u4Idx0)) == NULL)
            {
                TR69_TRC (TR69_DBG_TRC, "Invalid PPP Index"
                          "while trying to set UserName");
                return -1;
            }

            /* Make the Admin Status DOWN,Set the user name and again make the admin status UP */

            if (pIf->LcpStatus.AdminStatus == ADMIN_OPEN)
            {
                i4rc = TrSetIfMainAdminStatus (u4Idx0, CFA_IF_DOWN);
                i4AdminStatusFlag = TRUE;
                if (i4rc == -1)
                {
                    return i4rc;
                }
            }
            OctetStr.i4_Length = STRLEN (value->in_cval);
            MEMCPY (OctetStr.pu1_OctetList, value->in_cval, OctetStr.i4_Length);
            MEMSET (pNode->au1Username, 0, PPPCONNECTION_USERNAME);
            MEMCPY (pNode->au1Username, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);

            /* Set UserName  for both  PAP ( u4Idx1 = 1 ) and CHAP ( u4Idx1 = 2) */

            for (u4Idx1 = 1; u4Idx1 <= 2; u4Idx1++)
            {
                i4rc =
                    MynmhTest (*Join
                               (PppSecuritySecretsIdentity, 12,
                                PppSecuritySecretsTableINDEX, 2, u4Idx0,
                                u4Idx1), SNMP_DATA_TYPE_OCTET_PRIM,
                               (VOID *) &OctetStr);
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC, "-E- TrNonMand :"
                              "Test failed for PppSecuritySecretsIdentity\n");
                    return -1;
                }

                i4rc =
                    MynmhSet (*Join
                              (PppSecuritySecretsIdentity, 12,
                               PppSecuritySecretsTableINDEX, 2, u4Idx0, u4Idx1),
                              SNMP_DATA_TYPE_OCTET_PRIM, (VOID *) &OctetStr);
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC, "-E- TrNonMand :"
                              "Set failed for PppSecuritySecretsIdentity\n");
                    return -1;
                }
            }
            if (i4AdminStatusFlag == TRUE)
            {
                i4rc = TrSetIfMainAdminStatus (u4Idx0, CFA_IF_UP);
            }
            return i4rc;
        }
    }
    else if (STRCMP (name, "Password") == 0)
    {
        if ((pNode->u4ValMask & PPP_CONNECTION_VAL_MASK) ==
            PPP_CONNECTION_VAL_MASK)
        {
            if ((pIf = PppGetIfPtr (u4Idx0)) == NULL)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "Invalid PPP Index;while trying to set Password\r\n");
                return -1;
            }

            /* Make the Admin Status DOWN,Set the user name and again make the admin status UP */

            if (pIf->LcpStatus.AdminStatus == ADMIN_OPEN)
            {
                i4rc = TrSetIfMainAdminStatus (u4Idx0, CFA_IF_DOWN);
                i4AdminStatusFlag = TRUE;
                if (i4rc == -1)
                {
                    return i4rc;
                }
            }

            OctetStr.i4_Length = STRLEN (value->in_cval);
            MEMCPY (OctetStr.pu1_OctetList, value->in_cval, OctetStr.i4_Length);
            MEMSET (pNode->au1Password, 0, PPPCONNECTION_PASSWORD);
            MEMCPY (pNode->au1Password, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);

            /* Set all Entries for PAP ( u4Idx1 = 1 ) and CHAP ( u4Idx1 = 2) */

            for (u4Idx1 = 1; u4Idx1 <= 2; u4Idx1++)
            {
                /* Set the PppSecuritySecretsProtocol for PAP (u4Idx1 = 1) and CHAP (u4Idx1 = 2)
                   while setting the password ( PppSecuritySecretsSecret ) */
                if (u4Idx1 == 1)
                {
                    i4rc =
                        nmhTestv2PppSecuritySecretsProtocol (&u4ErrorCode,
                                                             u4Idx0, u4Idx1,
                                                             &pppSecurityPapProtocolOID);
                }
                else
                {
                    i4rc =
                        nmhTestv2PppSecuritySecretsProtocol (&u4ErrorCode,
                                                             u4Idx0, u4Idx1,
                                                             &pppSecurityChapMD5ProtocolOID);
                }
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC, "-E- NonMand :"
                              "Test failed for PppSecuritySecretsProtocol\n");
                    return -1;
                }

                if (u4Idx1 == 1)
                {
                    i4rc =
                        nmhSetPppSecuritySecretsProtocol (u4Idx0, u4Idx1,
                                                          &pppSecurityPapProtocolOID);
                }
                else
                {
                    i4rc =
                        nmhSetPppSecuritySecretsProtocol (u4Idx0, u4Idx1,
                                                          &pppSecurityChapMD5ProtocolOID);
                }
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC, "-E- NonMand : Set"
                              "failed for PppSecuritySecretsProtocol\n");
                    return -1;
                }

                /* Set the PppSecuritySecretsDirection for PAP (u4Idx1 = 1) and CHAP (u4Idx1 = 2)
                   while setting the password ( PppSecuritySecretsSecret ) */

                i4rc =
                    nmhTestv2PppSecuritySecretsDirection (&u4ErrorCode, u4Idx0,
                                                          u4Idx1,
                                                          PPP_LOCAL_TO_REMOTE);
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC, "-E- NonMand : Test"
                              "failed for PppSecuritySecretsDirection\n");
                    return -1;
                }

                i4rc =
                    nmhSetPppSecuritySecretsDirection (u4Idx0, u4Idx1,
                                                       PPP_LOCAL_TO_REMOTE);
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC, "-E- NonMand : Set"
                              "failed for PppSecuritySecretsDirection\n");
                    return -1;
                }

                /* Setting the PppSecuritySecretsSecret  for PAP (u4Idx1 = 1) and CHAP (u4Idx1 = 2) */

                i4rc =
                    MynmhTest (*Join
                               (PppSecuritySecretsSecret, 12,
                                PppSecuritySecretsTableINDEX, 2, u4Idx0,
                                u4Idx1), SNMP_DATA_TYPE_OCTET_PRIM,
                               (VOID *) &OctetStr);
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC,
                              "-E- Test failed for PppSecuritySecretsSecret\n");
                    return -1;
                }

                i4rc =
                    MynmhSet (*Join
                              (PppSecuritySecretsSecret, 12,
                               PppSecuritySecretsTableINDEX, 2, u4Idx0, u4Idx1),
                              SNMP_DATA_TYPE_OCTET_PRIM, (VOID *) &OctetStr);
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC,
                              "-E- Get failed for PppSecuritySecretsSecret\n");
                    return -1;
                }

                /* Set the PppSecuritySecretsStatus for PAP (u4Idx1 = 1) and CHAP (u4Idx1 = 2)
                   while setting the password ( PppSecuritySecretsSecret ) */

                nmhTestv2PppSecuritySecretsStatus (&u4ErrorCode, u4Idx0, u4Idx1,
                                                   SECRETS_STATUS_VALID);

                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC, "-E- NonMand : Test"
                              "failed for PppSecuritySecretsStatus\n");
                    return -1;
                }

                i4rc =
                    nmhSetPppSecuritySecretsStatus (u4Idx0, u4Idx1,
                                                    SECRETS_STATUS_VALID);
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC, "-E- NonMand : Set"
                              "failed for PppSecuritySecretsStatus\n");
                    return -1;
                }
            }
            if (i4AdminStatusFlag == TRUE)
            {
                i4rc = TrSetIfMainAdminStatus (u4Idx0, CFA_IF_UP);
            }
            return i4rc;
        }
    }
    else if (STRCMP (name, "MaxMRUSize") == 0)
    {
        if ((pNode->u4ValMask & PPP_CONNECTION_VAL_MASK) ==
            PPP_CONNECTION_VAL_MASK)
        {
            pNode->u4MaxMRUSize = u4val = value->in_uint;
            if ((pIf = PppGetIfPtr (u4Idx0)) == NULL)
            {
                TR69_TRC (TR69_DBG_TRC,
                          " Invalid PPP Index;while trying to set MTU\r\n");
                return -1;
            }
            /* Making the Admin Status DOWN */
            if (pIf->LcpStatus.AdminStatus == ADMIN_OPEN)
            {
                i4rc = TrSetIfMainAdminStatus (u4Idx0, CFA_IF_DOWN);
                i4AdminStatusFlag = TRUE;
                if (i4rc == -1)
                {
                    TR69_TRC (TR69_DBG_TRC, "TrSetNonMand : Failure"
                              "while setting Admin status to DOWN for Mtu \n");
                    return i4rc;
                }
            }

            /* Setting the MTU Value */

            i4rc =
                MynmhTest (*Join (IfMainMtu, 12, IfMainTableINDEX, 1, u4Idx0),
                           SNMP_DATA_TYPE_INTEGER32, (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC, "-E- Test failed for IfMainMtu\n");
                return -1;
            }

            i4rc =
                MynmhSet (*Join (IfMainMtu, 12, IfMainTableINDEX, 1, u4Idx0),
                          SNMP_DATA_TYPE_INTEGER32, (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC, "-E- Set failed for IfMainMtu\n");
                return -1;
            }

            /* Making the Admin Status - UP */
            if (i4AdminStatusFlag == TRUE)
            {
                i4rc = TrSetIfMainAdminStatus (u4Idx0, CFA_IF_UP);
            }
            if (i4rc == -1)
            {
                TR69_TRC (TR69_DBG_TRC, "TrSetNonMand : Failure"
                          "while setting Admin status to UP for Mtu \n");
            }
            return 0;
        }
    }
    else if (STRCMP (name, "ConnectionTrigger") == 0)
    {
        if ((pNode->u4ValMask & PPP_CONNECTION_VAL_MASK) ==
            PPP_CONNECTION_VAL_MASK)
        {
            /* Convert value->in_cval to u4val */
            if (STRCMP (value->in_cval, "AlwaysOn") == 0)
            {
                u4val = CFA_PERS_DEMAND;
            }
            else
            {
                u4val = CFA_PERS_OTHER;
            }

            pNode->u4ConnectionTrigger = u4val;

            i4rc =
                MynmhTest (*Join
                           (IfWanPersistence, 12, IfWanTableINDEX, 1, u4Idx0),
                           SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrNonMand : Test failed for IfWanPersistence\n");
                return -1;
            }

            i4rc =
                MynmhSet (*Join
                          (IfWanPersistence, 12, IfWanTableINDEX, 1, u4Idx0),
                          SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrNonMand : Set failed for IfWanPersistence\n");
                return -1;
            }
            return 0;
        }
    }
    return TR_NOT_MAND_PARAM;
}
