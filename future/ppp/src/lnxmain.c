/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lnxmain.c,v 1.8 2014/12/16 11:47:26 siva Exp $
 *
 * Description:This file contains main routines  of PPP module.
 *
 *******************************************************************/

#define GLOBAL_VAR

#include  "pppcom.h"
#include  "ppptest.h"
#include  "ppptask.h"
#include  "globexts.h"
#include  "pppproto.h"
#include  "pppoeport.h"
#include  "pppdpproto.h"
#include  "gsemdefs.h"
#include  "lcpdefs.h"
#include  "stdautwr.h"
#include  "stdlcpwr.h"
#include  "stdipcwr.h"
#include  "stdbcpwr.h"
#include  "ppipcpwr.h"
#include  "pppoewr.h"
#include "genproto.h"
UINT1               TestIndex = 1;
tOsixQId            PppInputQId;

#ifdef RADIUS_WANTED
extern VOID         PppRadReceivedResponse (VOID *);
#endif

tOsixSemId          gPppSemId;
tOsixSemId          gPppLcpDownSemId;

/*********** PPPStart ***********/
VOID
PPPStart (VOID)
{
}

/*********** PPPMain ***********/
VOID
PPPMain (INT1 *pParam)
{
    UINT4               u4Event;

    UNUSED_PARAM (pParam);

    /* Create semaphore for PPP task lock/unlock */
    if (OsixCreateSem (PPP_PROTOCOL_LOCK, PPP_INITIAL_COUNT, PPP_FLAGS,
                       &gPppSemId) != OSIX_SUCCESS)
    {
        PPP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    if (OsixCreateSem ((const UINT1 *) "PLCP", PPP_INITIAL_COUNT, PPP_FLAGS,
                       &gPppLcpDownSemId) != OSIX_SUCCESS)
    {
        PPP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Create the packet arrival Q */
    if (OsixCreateQ (PPP_QUEUE_NAME,
                     PPP_QUEUE_DEPTH, OSIX_LOCAL, &PppInputQId) != OSIX_SUCCESS)
    {
        PPP_TRC (ALL_FAILURE, "LL Queue Creation Failure ");
    }

    PPPInit ();

    PPP_INIT_COMPLETE (OSIX_SUCCESS);

#ifdef SNMP_2_WANTED
    RegisterSTDAUT ();
    RegisterSTDLCP ();
    RegisterSTDIPC ();
#ifdef BCP
    RegisterSTDBCP ();
#endif
    RegisterPPIPCP ();
    RegisterPPPOE ();
#endif

    while (1)
    {

        if (OsixReceiveEvent ((PPP_QUEUE_EVENT | PPP_TIMER_EXPIRY_EVENT),
                              OSIX_WAIT, (UINT4) 0,
                              &u4Event) != OSIX_SUCCESS)
        {
            PPP_TRC (ALL_FAILURE, "OsixReceive Event Failure");
            continue;
        }

        PPP_LOCK ();
        if ((u4Event & PPP_QUEUE_EVENT) != 0)
        {
            PppDecodeMsg ();
        }

        if ((u4Event & PPP_TIMER_EXPIRY_EVENT) != 0)
        {
            PPPProcessTimeOut ();
        }
        PPP_UNLOCK ();

    }                            /* while (1) */
}                                /* PPPMain */

/*************************************************************************/
/* Function Name     : PppDecodeMsg                                      */
/* Description       : This procedure decodes the message and            */
/*                     correspondingly calls the processing functions    */
/*                     depending on the message type.                    */
/* Input(s)          : None                                              */
/* Output(s)         : None                                              */
/* <Optional Fields> : None                                              */
/* Use of Recursion  : None                                              */
/* Returns           : void                                              */
/*************************************************************************/
VOID
PppDecodeMsg (VOID)
{
    t_MSG_DESC         *pMsg = NULL;
    UINT2               u2Length;
    tPPPIf             *pIf = NULL;
    UINT4               u4SrcModId;
    UINT2               u2Protocol;
    UINT2               u2PppOePortNo;
    UINT4               u4IfNum;
    tGSEM              *pGSEM;
#ifdef RADIUS_WANTED
    UINT4               pRad;
#endif
    UINT4               u4IfIndex;
    UINT1               u1Event;
    UINT1               u1OperStatus;
    UINT1               u1AdminStatus;

    while (OsixReceiveFromQ (SELF, PPP_QUEUE_NAME, OSIX_NO_WAIT, 0,
                             &pMsg) == OSIX_SUCCESS)
    {

        u4SrcModId = PPP_GET_SRC_MODID (pMsg);

        /* Message From Higher Layer */
        switch (u4SrcModId)
        {
            case PPPMSG_FROM_HIGHER_LAYER:
                u2Protocol = PPP_GET_MODULE_DATA_PROTOCOL (pMsg);
                u4IfNum = PPP_GET_MODULE_DATA_IF_NUMBER (pMsg);
                ReleaseFlag = PPP_YES;
                u2Length = (UINT2) VALID_BYTES (pMsg);
                pIf = PppGetIfPtr (u4IfNum);
                if (pIf == NULL)
                {
                    PPP_TRC1 (ALL_FAILURE, "No PPP interface with index : [%u]",
                              u4IfNum);
                    RELEASE_INC_BUFFER (pMsg);
                    continue;
                }
                else
                {
                    PPPRxPktFromHL (pIf, pMsg, u2Length, u2Protocol);
                }
                break;

                /* Message From Lower Layer */

            case PPPMSG_FROM_LOWER_LAYER:
                u4IfNum = PPP_GET_MODULE_DATA_IF_NUMBER (pMsg);
                ReleaseFlag = PPP_YES;
                u2Length = (UINT2) VALID_BYTES (pMsg);
                if ((pIf = PppGetIfPtr (u4IfNum)) == NULL)
                {
                    PPP_TRC1 (ALL_FAILURE, "No PPP interface with index : [%u]",
                              u4IfNum);
                    RELEASE_INC_BUFFER (pMsg);
                    continue;
                }
                PPPRxPktFromLL (pIf, pMsg, u2Length);

                if (ReleaseFlag == PPP_YES)
                {
                    RELEASE_INC_BUFFER (pMsg);
                }
                break;
            case PPPMSG_FROM_PPPoE:
                PPPoERelFlag = PPP_YES;
                u2PppOePortNo = (UINT2) (PPP_GET_PORT_NUMBER (pMsg));
                u2Length = (UINT2) VALID_BYTES (pMsg);
                PPPoERxPktFromLL (&pMsg, u2Length, u2PppOePortNo);
                if (PPPoERelFlag == PPP_YES)
                {
                    RELEASE_BUFFER (pMsg);
                }
                break;

#ifdef RADIUS_WANTED
            case PPPMSG_FROM_RADIUS_CLIENT:
                EXTRACT4BYTE (pMsg, pRad);
                PppRadReceivedResponse ((VOID *) (FS_ULONG) pRad);
                RELEASE_BUFFER (pMsg);
                break;
#endif
            case PPPEVENT_TO_PPP_GSEM:
                EXTRACT4BYTE (pMsg, u4IfIndex);
                EXTRACT1BYTE (pMsg, u1Event);
                pIf = PppGetIfPtr (u4IfIndex);
                if (pIf != NULL)
                {
                    if (u1Event == PPP_DESTROY)
                    {
                        CfaUpdateInterfaceStatus (u4IfIndex, DESTROY);
                    }
                    else
                    {
                        pGSEM = &pIf->LcpGSEM;
                        GSEMRun (pGSEM, u1Event);
                    }
                }
                RELEASE_BUFFER (pMsg);
                break;

            case PPPMSG_UPDATE_INTF_STATUS:
                EXTRACT4BYTE (pMsg, u4IfIndex);
                EXTRACT1BYTE (pMsg, u1AdminStatus);
                EXTRACT1BYTE (pMsg, u1OperStatus);
                pIf = PppGetIfPtr (u4IfIndex);
                if (pIf != NULL)
                {
                    switch (u1AdminStatus)
                    {
                        case 0:
                            break;
                        case 1:
                        case 2:
                            PppUpdateAdminStatus (u4IfIndex, u1AdminStatus);
                            break;
                        default:
                            break;
                    }            /* switch - AdminStatus */
                    if (u1AdminStatus != 0
                        && (pIf->LinkInfo.IfID.IfType == CFA_NONE))
                        return;

                    switch (u1OperStatus)
                    {
                        case 0:
                            break;
                        case 4:
                            if (pIf->LinkInfo.IfID.IfType != PPP_OE_IFTYPE)
                            {
                                PppUpdateLLOperStatus (u4IfIndex, PPP_UP);
                            }
                            break;
                        case 7:
                            if (pIf->LinkInfo.IfID.IfType == PPP_OE_IFTYPE)
                            {
                                PPPoEHandleLinkDown (pIf);
                            }
                            else
                            {
                                PppUpdateLLOperStatus (u4IfIndex, PPP_DOWN);
                            }
                            break;
                        default:
                            break;
                    }
                }
                RELEASE_BUFFER (pMsg);
                break;
            default:
                break;
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name      : PppLock                                              */
/*                                                                           */
/* Description        : This function is used to take the PPP  protocol      */
/*                      SEMa4 to avoid simultaneous access to                */
/*                      PPP database                                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gCfaSemId                                            */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gCfaSemId                                            */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
PppLock (VOID)
{
    if (OsixSemTake (gPppSemId) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PppUnLock                                            */
/*                                                                           */
/* Description        : This function is used to give the PPP                */
/*                      SEMa4 to avoid simultaneous access to                */
/*                      PPP database                                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS                                         */
/*                                                                           */
/*****************************************************************************/
INT4
PppUnlock (VOID)
{
    OsixSemGive (gPppSemId);
    return SNMP_SUCCESS;
}
