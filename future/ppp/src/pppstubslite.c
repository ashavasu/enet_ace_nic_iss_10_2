/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppstubslite.c,v 1.2 2010/12/21 08:36:51 siva Exp $
 *
 * Description:
 *
 *********************************************************************/

#include "pppstubs.h"
#include "pppproto.h"
VOID
CCPEnableIf (tCCPIf * pCCP)
{
    UNUSED_PARAM (pCCP);
    return;
}

VOID
IP6CPEnableIf (tIP6CPIf * pIp6cpPtr)
{
    UNUSED_PARAM (pIp6cpPtr);
    return;
}

VOID               *
BCPEnable (tBCPIf * pBcpPtr)
{
    VOID               *dummy = NULL;
    UNUSED_PARAM (pBcpPtr);
    return dummy;
}

VOID
IP6CPDisableIf (tIP6CPIf * pIp6cpPtr)
{
    UNUSED_PARAM (pIp6cpPtr);
    return;
}

VOID
CCPDisableIf (tCCPIf * pCCP)
{
    UNUSED_PARAM (pCCP);
    return;
}

VOID
BCPDisableIf (tBCPIf * pBcpPtr)
{
    UNUSED_PARAM (pBcpPtr);
    return;
}

INT1
nmhGetPppCCPTxConfigOptionNum (INT4 i4PppCCPTxConfigIfIndex,
                               INT4 i4PppCCPTxConfigPriority,
                               INT4 *pi4RetValPppCCPTxConfigOptionNum)
{
    UNUSED_PARAM (i4PppCCPTxConfigIfIndex);
    UNUSED_PARAM (i4PppCCPTxConfigPriority);
    UNUSED_PARAM (pi4RetValPppCCPTxConfigOptionNum);
    return OSIX_SUCCESS;
}

tBCPIf             *
BCPCreateIf (tPPPIf * pIf)
{
    tBCPIf             *dummy = NULL;
    UNUSED_PARAM (pIf);
    return dummy;
}

VOID
BCPDeleteIf (tBCPIf * pBcpPtr)
{
    UNUSED_PARAM (pBcpPtr);
    return;
}

VOID
CCPDeleteIf (tCCPIf * pCCPIf)
{
    UNUSED_PARAM (pCCPIf);
    return;
}

VOID
IP6CPDeleteIf (tIP6CPIf * pIp6cpPtr)
{
    UNUSED_PARAM (pIp6cpPtr);
    return;
}

INT1
nmhGetFirstIndexPppBridgeTable (INT4 *pi4PppBridgeIfIndex)
{
    UNUSED_PARAM (pi4PppBridgeIfIndex);
    return OSIX_SUCCESS;
}

INT1
nmhGetNextIndexPppBridgeTable (INT4 i4PppBridgeIfIndex,
                               INT4 *pi4NextPppBridgeIfIndex)
{
    UNUSED_PARAM (i4PppBridgeIfIndex);
    UNUSED_PARAM (pi4NextPppBridgeIfIndex);
    return OSIX_SUCCESS;
}

INT1
nmhValidateIndexInstancePppBridgeTable (INT4 i4PppBridgeIfIndex)
{
    UNUSED_PARAM (i4PppBridgeIfIndex);
    return OSIX_SUCCESS;
}

INT1
nmhGetPppBridgeOperStatus (INT4 i4PppBridgeIfIndex,
                           INT4 *pi4RetValPppBridgeOperStatus)
{
    UNUSED_PARAM (i4PppBridgeIfIndex);
    UNUSED_PARAM (pi4RetValPppBridgeOperStatus);
    return OSIX_SUCCESS;
}

INT1
nmhGetPppBridgeLocalToRemoteTinygramCompression (INT4 i4PppBridgeIfIndex,
                                                 INT4
                                                 *pi4RetValPppBridgeLocalToRemoteTinygramCompression)
{
    UNUSED_PARAM (i4PppBridgeIfIndex);
    UNUSED_PARAM (pi4RetValPppBridgeLocalToRemoteTinygramCompression);
    return OSIX_SUCCESS;
}

INT1
nmhGetPppBridgeRemoteToLocalTinygramCompression (INT4 i4PppBridgeIfIndex,
                                                 INT4
                                                 *pi4RetValPppBridgeRemoteToLocalTinygramCompression)
{
    UNUSED_PARAM (i4PppBridgeIfIndex);
    UNUSED_PARAM (pi4RetValPppBridgeRemoteToLocalTinygramCompression);
    return OSIX_SUCCESS;
}

INT1
nmhGetPppBridgeLocalToRemoteLanId (INT4 i4PppBridgeIfIndex,
                                   INT4 *pi4RetValPppBridgeLocalToRemoteLanId)
{
    UNUSED_PARAM (i4PppBridgeIfIndex);
    UNUSED_PARAM (pi4RetValPppBridgeLocalToRemoteLanId);
    return OSIX_SUCCESS;
}

INT1
nmhGetPppBridgeRemoteToLocalLanId (INT4 i4PppBridgeIfIndex,
                                   INT4 *pi4RetValPppBridgeRemoteToLocalLanId)
{
    UNUSED_PARAM (i4PppBridgeIfIndex);
    UNUSED_PARAM (pi4RetValPppBridgeRemoteToLocalLanId);
    return OSIX_SUCCESS;
}

INT1
nmhGetFirstIndexPppBridgeConfigTable (INT4 *pi4PppBridgeConfigIfIndex)
{
    UNUSED_PARAM (pi4PppBridgeConfigIfIndex);
    return OSIX_SUCCESS;
}

INT1
nmhGetNextIndexPppBridgeConfigTable (INT4 i4PppBridgeConfigIfIndex,
                                     INT4 *pi4NextPppBridgeConfigIfIndex)
{
    UNUSED_PARAM (i4PppBridgeConfigIfIndex);
    UNUSED_PARAM (pi4NextPppBridgeConfigIfIndex);
    return OSIX_SUCCESS;
}

INT1
nmhValidateIndexInstancePppBridgeConfigTable (INT4 i4PppBridgeConfigIfIndex)
{
    UNUSED_PARAM (i4PppBridgeConfigIfIndex);
    return OSIX_SUCCESS;
}

INT1
nmhGetPppBridgeConfigAdminStatus (INT4 i4PppBridgeConfigIfIndex,
                                  INT4 *pi4RetValPppBridgeConfigAdminStatus)
{
    UNUSED_PARAM (i4PppBridgeConfigIfIndex);
    UNUSED_PARAM (pi4RetValPppBridgeConfigAdminStatus);

    return OSIX_SUCCESS;
}

INT1
nmhGetPppBridgeConfigTinygram (INT4 i4PppBridgeConfigIfIndex,
                               INT4 *pi4RetValPppBridgeConfigTinygram)
{
    UNUSED_PARAM (i4PppBridgeConfigIfIndex);
    UNUSED_PARAM (pi4RetValPppBridgeConfigTinygram);
    return OSIX_SUCCESS;
}

INT1
nmhGetPppBridgeConfigRingId (INT4 i4PppBridgeConfigIfIndex,
                             INT4 *pi4RetValPppBridgeConfigRingId)
{
    UNUSED_PARAM (i4PppBridgeConfigIfIndex);
    UNUSED_PARAM (pi4RetValPppBridgeConfigRingId);
    return OSIX_SUCCESS;
}

INT1
nmhGetPppBridgeConfigLineId (INT4 i4PppBridgeConfigIfIndex,
                             INT4 *pi4RetValPppBridgeConfigLineId)
{
    UNUSED_PARAM (i4PppBridgeConfigIfIndex);
    UNUSED_PARAM (pi4RetValPppBridgeConfigLineId);
    return OSIX_SUCCESS;
}

INT1
nmhGetPppBridgeConfigLanId (INT4 i4PppBridgeConfigIfIndex,
                            INT4 *pi4RetValPppBridgeConfigLanId)
{
    UNUSED_PARAM (i4PppBridgeConfigIfIndex);
    UNUSED_PARAM (pi4RetValPppBridgeConfigLanId);
    return OSIX_SUCCESS;
}

INT1
nmhSetPppBridgeConfigAdminStatus (INT4 i4PppBridgeConfigIfIndex,
                                  INT4 i4SetValPppBridgeConfigAdminStatus)
{
    UNUSED_PARAM (i4PppBridgeConfigIfIndex);
    UNUSED_PARAM (i4SetValPppBridgeConfigAdminStatus);
    return OSIX_SUCCESS;
}

INT1
nmhSetPppBridgeConfigTinygram (INT4 i4PppBridgeConfigIfIndex,
                               INT4 i4SetValPppBridgeConfigTinygram)
{
    UNUSED_PARAM (i4PppBridgeConfigIfIndex);
    UNUSED_PARAM (i4SetValPppBridgeConfigTinygram);
    return OSIX_SUCCESS;
}

INT1
nmhSetPppBridgeConfigRingId (INT4 i4PppBridgeConfigIfIndex,
                             INT4 i4SetValPppBridgeConfigRingId)
{
    UNUSED_PARAM (i4PppBridgeConfigIfIndex);
    UNUSED_PARAM (i4SetValPppBridgeConfigRingId);
    return OSIX_SUCCESS;
}

INT1
nmhSetPppBridgeConfigLineId (INT4 i4PppBridgeConfigIfIndex,
                             INT4 i4SetValPppBridgeConfigLineId)
{
    UNUSED_PARAM (i4PppBridgeConfigIfIndex);
    UNUSED_PARAM (i4SetValPppBridgeConfigLineId);
    return OSIX_SUCCESS;
}

INT1
nmhSetPppBridgeConfigLanId (INT4 i4PppBridgeConfigIfIndex,
                            INT4 i4SetValPppBridgeConfigLanId)
{
    UNUSED_PARAM (i4PppBridgeConfigIfIndex);
    UNUSED_PARAM (i4SetValPppBridgeConfigLanId);
    return OSIX_SUCCESS;
}

INT1
nmhTestv2PppBridgeConfigAdminStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4PppBridgeConfigIfIndex,
                                     INT4 i4TestValPppBridgeConfigAdminStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4PppBridgeConfigIfIndex);
    UNUSED_PARAM (i4TestValPppBridgeConfigAdminStatus);
    return OSIX_SUCCESS;
}

INT1
nmhTestv2PppBridgeConfigTinygram (UINT4 *pu4ErrorCode,
                                  INT4 i4PppBridgeConfigIfIndex,
                                  INT4 i4TestValPppBridgeConfigTinygram)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4PppBridgeConfigIfIndex);
    UNUSED_PARAM (i4TestValPppBridgeConfigTinygram);
    return OSIX_SUCCESS;
}

INT1
nmhTestv2PppBridgeConfigRingId (UINT4 *pu4ErrorCode,
                                INT4 i4PppBridgeConfigIfIndex,
                                INT4 i4TestValPppBridgeConfigRingId)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4PppBridgeConfigIfIndex);
    UNUSED_PARAM (i4TestValPppBridgeConfigRingId);
    return OSIX_SUCCESS;
}

INT1
nmhTestv2PppBridgeConfigLineId (UINT4 *pu4ErrorCode,
                                INT4 i4PppBridgeConfigIfIndex,
                                INT4 i4TestValPppBridgeConfigLineId)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4PppBridgeConfigIfIndex);
    UNUSED_PARAM (i4TestValPppBridgeConfigLineId);
    return OSIX_SUCCESS;
}

INT1
nmhTestv2PppBridgeConfigLanId (UINT4 *pu4ErrorCode,
                               INT4 i4PppBridgeConfigIfIndex,
                               INT4 i4TestValPppBridgeConfigLanId)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4PppBridgeConfigIfIndex);
    UNUSED_PARAM (i4TestValPppBridgeConfigLanId);
    return OSIX_SUCCESS;
}

INT1
nmhGetFirstIndexPppBridgeMediaTable (INT4 *pi4PppBridgeMediaIfIndex,
                                     INT4 *pi4PppBridgeMediaMacType)
{
    UNUSED_PARAM (pi4PppBridgeMediaIfIndex);
    UNUSED_PARAM (pi4PppBridgeMediaMacType);
    return OSIX_SUCCESS;
}

INT1
nmhGetNextIndexPppBridgeMediaTable (INT4 i4PppBridgeMediaIfIndex,
                                    INT4 *pi4NextPppBridgeMediaIfIndex,
                                    INT4 i4PppBridgeMediaMacType,
                                    INT4 *pi4NextPppBridgeMediaMacType)
{
    UNUSED_PARAM (i4PppBridgeMediaIfIndex);
    UNUSED_PARAM (pi4NextPppBridgeMediaIfIndex);
    UNUSED_PARAM (i4PppBridgeMediaMacType);
    UNUSED_PARAM (pi4NextPppBridgeMediaMacType);
    return OSIX_SUCCESS;
}

INT1
nmhValidateIndexInstancePppBridgeMediaTable (INT4 i4PppBridgeMediaIfIndex,
                                             INT4 i4PppBridgeMediaMacType)
{
    UNUSED_PARAM (i4PppBridgeMediaIfIndex);
    UNUSED_PARAM (i4PppBridgeMediaMacType);
    return OSIX_SUCCESS;
}

INT1
nmhGetPppBridgeMediaLocalStatus (INT4 i4PppBridgeMediaIfIndex,
                                 INT4 i4PppBridgeMediaMacType,
                                 INT4 *pi4RetValPppBridgeMediaLocalStatus)
{
    UNUSED_PARAM (i4PppBridgeMediaIfIndex);
    UNUSED_PARAM (i4PppBridgeMediaMacType);
    UNUSED_PARAM (pi4RetValPppBridgeMediaLocalStatus);
    return OSIX_SUCCESS;
}

INT1
nmhGetPppBridgeMediaRemoteStatus (INT4 i4PppBridgeMediaIfIndex,
                                  INT4 i4PppBridgeMediaMacType,
                                  INT4 *pi4RetValPppBridgeMediaRemoteStatus)
{
    UNUSED_PARAM (i4PppBridgeMediaIfIndex);
    UNUSED_PARAM (i4PppBridgeMediaMacType);
    UNUSED_PARAM (pi4RetValPppBridgeMediaRemoteStatus);
    return OSIX_SUCCESS;
}

INT1
nmhGetFirstIndexPppBridgeMediaConfigTable (INT4 *pi4PppBridgeMediaConfigIfIndex,
                                           INT4 *pi4PppBridgeMediaConfigMacType)
{
    UNUSED_PARAM (pi4PppBridgeMediaConfigIfIndex);
    UNUSED_PARAM (pi4PppBridgeMediaConfigMacType);
    return OSIX_SUCCESS;
}

INT1
nmhGetNextIndexPppBridgeMediaConfigTable (INT4 i4PppBridgeMediaConfigIfIndex,
                                          INT4
                                          *pi4NextPppBridgeMediaConfigIfIndex,
                                          INT4 i4PppBridgeMediaConfigMacType,
                                          INT4
                                          *pi4NextPppBridgeMediaConfigMacType)
{
    UNUSED_PARAM (i4PppBridgeMediaConfigIfIndex);
    UNUSED_PARAM (pi4NextPppBridgeMediaConfigIfIndex);
    UNUSED_PARAM (i4PppBridgeMediaConfigMacType);
    UNUSED_PARAM (pi4NextPppBridgeMediaConfigMacType);
    return OSIX_SUCCESS;
}

INT1
nmhValidateIndexInstancePppBridgeMediaConfigTable (INT4
                                                   i4PppBridgeMediaConfigIfIndex,
                                                   INT4
                                                   i4PppBridgeMediaConfigMacType)
{
    UNUSED_PARAM (i4PppBridgeMediaConfigIfIndex);
    UNUSED_PARAM (i4PppBridgeMediaConfigMacType);
    return OSIX_SUCCESS;
}

INT1
nmhGetPppBridgeMediaConfigLocalStatus (INT4 i4PppBridgeMediaConfigIfIndex,
                                       INT4 i4PppBridgeMediaConfigMacType,
                                       INT4
                                       *pi4RetValPppBridgeMediaConfigLocalStatus)
{
    UNUSED_PARAM (i4PppBridgeMediaConfigIfIndex);
    UNUSED_PARAM (i4PppBridgeMediaConfigMacType);
    UNUSED_PARAM (pi4RetValPppBridgeMediaConfigLocalStatus);
    return OSIX_SUCCESS;
}

INT1
nmhSetPppBridgeMediaConfigLocalStatus (INT4 i4PppBridgeMediaConfigIfIndex,
                                       INT4 i4PppBridgeMediaConfigMacType,
                                       INT4
                                       i4SetValPppBridgeMediaConfigLocalStatus)
{
    UNUSED_PARAM (i4PppBridgeMediaConfigIfIndex);
    UNUSED_PARAM (i4PppBridgeMediaConfigMacType);
    UNUSED_PARAM (i4SetValPppBridgeMediaConfigLocalStatus);
    return OSIX_SUCCESS;
}

INT1
nmhTestv2PppBridgeMediaConfigLocalStatus (UINT4 *pu4ErrorCode,
                                          INT4 i4PppBridgeMediaConfigIfIndex,
                                          INT4 i4PppBridgeMediaConfigMacType,
                                          INT4
                                          i4TestValPppBridgeMediaConfigLocalStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4PppBridgeMediaConfigIfIndex);
    UNUSED_PARAM (i4PppBridgeMediaConfigMacType);
    UNUSED_PARAM (i4TestValPppBridgeMediaConfigLocalStatus);
    return OSIX_SUCCESS;
}

tGSEM              *
IP6CPGetSEMPtr (tPPPIf * pIf)
{
    tGSEM              *dummy = NULL;
    UNUSED_PARAM (pIf);
    return dummy;
}

INT1
IP6CPCopyOptions (tGSEM * pGSEM, UINT1 Flag, UINT1 PktType)
{
    UNUSED_PARAM (pGSEM);
    UNUSED_PARAM (Flag);
    UNUSED_PARAM (PktType);
    return OSIX_SUCCESS;
}

tGSEM              *
BCPGetSEMPtr (tPPPIf * pIf)
{
    tGSEM              *dummy = NULL;
    UNUSED_PARAM (pIf);
    return dummy;
}

INT1
BCPCopyOptions (tGSEM * pGSEM, UINT1 Flag, UINT1 PktType)
{
    UNUSED_PARAM (pGSEM);
    UNUSED_PARAM (Flag);
    UNUSED_PARAM (PktType);
    return OSIX_SUCCESS;
}

tGSEM              *
CCPGetSEMPtr (tPPPIf * pIf)
{
    tGSEM              *dummy = NULL;
    UNUSED_PARAM (pIf);
    return dummy;
}

INT1
CCPCopyOptions (tGSEM * pGSEM, UINT1 Flag, UINT1 PktType)
{
    UNUSED_PARAM (pGSEM);
    UNUSED_PARAM (Flag);
    UNUSED_PARAM (PktType);
    return OSIX_SUCCESS;
}

tGSEM              *
BCPSpanIeeeGetSEMPtr (tPPPIf * pIf)
{
    tGSEM              *dummy = NULL;
    UNUSED_PARAM (pIf);
    return dummy;
}

tGSEM              *
BCPIbmSrcRtGetSEMPtr (tPPPIf * pIf)
{
    tGSEM              *dummy = NULL;
    UNUSED_PARAM (pIf);
    return dummy;
}

tGSEM              *
BCPDecLanGetSEMPtr (tPPPIf * pIf)
{
    tGSEM              *dummy = NULL;
    UNUSED_PARAM (pIf);
    return dummy;
}

INT1
CCPRecdResetReq (tGSEM * pGSEM, t_MSG_DESC * pIn, UINT2 Length, UINT1 Id)
{
    UNUSED_PARAM (pGSEM);
    UNUSED_PARAM (pIn);
    UNUSED_PARAM (Length);
    UNUSED_PARAM (Id);
    return OSIX_SUCCESS;
}

INT1
CCPRecdResetAck (tGSEM * pGSEM, t_MSG_DESC * pIn, UINT2 Length, UINT1 Id)
{
    UNUSED_PARAM (pGSEM);
    UNUSED_PARAM (pIn);
    UNUSED_PARAM (Length);
    UNUSED_PARAM (Id);
    return OSIX_SUCCESS;
}

INT1
CheckAndGetCCPTxPtr (tPPPIf * pIf, UINT4 *n1, UINT4 *n2, UINT4 SecondIndex)
{
    UNUSED_PARAM (pIf);
    UNUSED_PARAM (n1);
    UNUSED_PARAM (n2);
    UNUSED_PARAM (SecondIndex);
    return OSIX_SUCCESS;
}

INT1
CheckAndGetCCPRxPtr (tPPPIf * pIf, UINT4 *n1, UINT4 *n2, UINT4 SecondIndex)
{
    UNUSED_PARAM (pIf);
    UNUSED_PARAM (n1);
    UNUSED_PARAM (n2);
    UNUSED_PARAM (SecondIndex);
    return OSIX_SUCCESS;
}

INT1
CheckAndGetBCPMacPtr (tPPPIf * pIf, UINT4 *n1, UINT4 *n2, UINT4 SecondIndex)
{
    UNUSED_PARAM (pIf);
    UNUSED_PARAM (n1);
    UNUSED_PARAM (n2);
    UNUSED_PARAM (SecondIndex);
    return OSIX_SUCCESS;
}

INT1
CheckAndGetIp6Ptr (tPPPIf * pIf)
{
    UNUSED_PARAM (pIf);
    return OSIX_SUCCESS;
}

INT1
CheckAndGetCCPPtr (tPPPIf * pIf)
{
    UNUSED_PARAM (pIf);
    return OSIX_SUCCESS;
}

INT1
CheckAndGetBCPPtr (tPPPIf * pIf)
{
    UNUSED_PARAM (pIf);
    return OSIX_SUCCESS;
}

VOID
BcpInit (VOID)
{
    return;
}

tIP6CPIf           *
ALLOC_IP6CP_IF ()
{
    tIP6CPIf           *dummy = NULL;
    return dummy;
}

tBCPIf             *
ALLOC_BCP_IF ()
{
    tBCPIf             *dummy = NULL;
    return dummy;
}
