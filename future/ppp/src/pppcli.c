/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppcli.c,v 1.28 2016/07/12 12:39:10 siva Exp $
 *
 * Description: CLI functions of PPP module.
 *
 *******************************************************************/
#ifndef __PPPCLI_C__
#define __PPPCLI_C__

#include "pppcom.h"
#include "pppoedefs.h"
#include "pppoelow.h"
#include "l2tp.h"
#include "pppexlow.h"
#include "ppipclow.h"
#include "pplcplow.h"
#include "pppstlow.h"
#include "pppexts.h"
#include "authcom.h"
#include "globexts.h"
#include "stdaulow.h"
#include "pppgelow.h"
#include "pppoelp.h"
#include "pppoeport.h"
#include "pppmemmac.h"
#include "ipcpcom.h"
#include "ppp.h"
#include "msr.h"
#include "iss.h"
#define PPP_USRNAME_LEN   10
#define PPP_PASSWORD_LEN  10
#define PPP_SERNAME_LEN   10

#define MSAD_WAN_PPP_INT_NAME   "ppp0"

extern tSNMP_OID_TYPE pppSecurityChapMD5ProtocolOID;
extern tSNMP_OID_TYPE pppSecurityMSCHAPProtocolOID;
extern tSNMP_OID_TYPE pppSecurityPapProtocolOID;

INT4                cli_set_access_info (tWanInfo *);

tWanInfo            gCfaWanInfo;
UINT1               gu1AccessType = CFA_WAN_TYPE_ENET_DYNAMIC;
UINT1               gu1WanType = CFA_WAN_TYPE_ENET_DYNAMIC;
UINT1               gu1PrvAccessType = CFA_WAN_TYPE_ENET_DYNAMIC;    /*Previous access method */

INT4
cli_process_ppp_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    INT4                i4RetVal = CLI_FAILURE;
    INT4                i4AuthServer = 0;
    UINT4               u4AddrPoolIndex = 0;
    UINT4               u4StartAddress = 0, u4EndAddress = 0;
    UINT4               u4AuthType = 0;
    INT1                argno = 0;
    UINT1              *args[CLI_MAX_ARGS];
    UINT4               u4ServerIpAddr = 0;
    UINT1              *pu1UserName = NULL;
    UINT1              *pu1Password = NULL;
    UINT1              *pu1ServerName = NULL;
    UINT1              *pu1ServiceName = NULL;
    UINT4               u4IdleTimeOut = 0;
    UINT4               u4ConnectType = L2TP_MANUAL;
    UINT4               u4IpAddr = 0;
    UINT4               u4KeepAlive = 0;
    UINT4               u4PadrRetry = 0;
    UINT4               u4Mask = 0;
    UINT4               u4Gateway = 0;
    UINT4               u4TraceLevel = 0;
    UINT1              *pu1PriDns = NULL;
    UINT1              *pu1SecDns = NULL;
    UINT1              *pu1Inst = NULL;
    UINT4               u4IfIndex = 0;
    INT4                i4Mode = 0;
    INT4                i4AuthMode = 0;
    UINT4               u4PrimaryDnsIpAddr;
    UINT4               u4SecondaryDnsIpAddr;
    UINT1              *pu1HostName = NULL;
    tCfaIfInfo          IfInfo;
    UINT4               u4Action;
    UINT1               u1Direction;
    UINT4               u4ErrCode = 0;
    UINT4               u4StrLen = 0;
    INT4                i4EncapsulationVlanid = 0;
    INT4                i4Priority = 0;
    INT4                i4Cfi = 0;
    va_start (ap, u4Command);
    /* second arguement is always interface name/index */
    pu1Inst = va_arg (ap, UINT1 *);
    UNUSED_PARAM (pu1Inst);
    PPP_UNUSED (pu1ServerName);

    UINT1               au1UserName[PPP_USRNAME_LEN+1];
    UINT1               au1Password[PPP_PASSWORD_LEN+1];
    UINT1               au1ServerName[PPP_SERNAME_LEN];
    UINT1               au1PriDns[12];
    UINT1               au1SecDns[12];

    MEMSET (au1UserName, 0, PPP_USRNAME_LEN+1);
    MEMSET (au1Password, 0, PPP_PASSWORD_LEN+1);
    MEMSET (au1ServerName, 0, PPP_SERNAME_LEN);
    MEMSET (au1PriDns, 0, 12);
    MEMSET (au1SecDns, 0, 12);

    /* Walk through the rest of the arguements and store in args array. 
     * Store fourteen arguements at the max. This is because fwl commands do not
     * take more than fourteen inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give 
     * second input. In that case first arg will be null and second arg only 
     * has value */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == PPP_CLI_MAX_COMMANDS_PARAMS)
            break;
    }

    va_end (ap);

    switch (u4Command)
    {
        case CLI_PPP_L2TP_STATIC_MODE:

            pu1UserName = au1UserName;
            pu1Password = au1Password;
            pu1ServerName = au1ServerName;
            pu1PriDns = au1PriDns;
            pu1SecDns = au1SecDns;

            u4ServerIpAddr = (*(UINT4 *) (VOID *) args[0]);
            if (pu1UserName != NULL)
            {
                MEMSET (pu1UserName, 0, PPP_USRNAME_LEN);
                u4StrLen = MEM_MAX_BYTES (STRLEN (args[1]), PPP_USRNAME_LEN);
                STRNCPY (pu1UserName, args[1], u4StrLen);
                pu1UserName[u4StrLen] = '\0';
            }
            if (pu1Password != NULL)
            {
                MEMSET (pu1Password, 0, PPP_PASSWORD_LEN);
                u4StrLen = MEM_MAX_BYTES (STRLEN (args[2]), PPP_USRNAME_LEN);
                STRNCPY (pu1Password, (UINT1 *) args[2], u4StrLen);
                pu1Password[u4StrLen] = '\0';
            }
            u4AuthType = CLI_PTR_TO_U4 (args[3]);
            u4IdleTimeOut = CLI_PTR_TO_U4 (args[4]);
            u4KeepAlive = CLI_PTR_TO_U4 (args[5]);
            u4ConnectType = CLI_PTR_TO_U4 (args[6]);

            i4RetVal = cli_set_l2tp_static_mode (u4ServerIpAddr, pu1UserName,
                                                 pu1Password, u4AuthType,
                                                 u4IdleTimeOut, u4KeepAlive,
                                                 u4ConnectType, u4IpAddr,
                                                 u4Mask, u4Gateway,
                                                 pu1PriDns, pu1SecDns);
            break;

        case CLI_PPP_ADDRESS_POOL:
            u4AddrPoolIndex = *((UINT4 *) (VOID *) args[0]);
            u4StartAddress = *((UINT4 *) (VOID *) args[1]);
            u4EndAddress = *((UINT4 *) (VOID *) args[2]);
            i4RetVal = CliPppCreateAddressPool (CliHandle, u4AddrPoolIndex,
                                                u4StartAddress, u4EndAddress);
            break;
        case CLI_PPP_NO_ADDRESS_POOL:
            i4RetVal = CliPppDeleteAddressPool (CliHandle, u4AddrPoolIndex);
            break;

	case PPP_CLI_SHOW_PPPOE_STATISTICS:
            i4RetVal = CliPppShowPppoeStatistics (CliHandle);
            break;

	case PPP_CLI_CLEAR_PPPOE_STATISTICS:
            i4RetVal = CliPppClearPppoeStatistics (CliHandle);
            break;

        case CLI_PPP_AUTH_SERVER:
            i4AuthServer = *((INT4 *) (VOID *) args[0]);
            i4RetVal = CliPppSetAuthServer (CliHandle, i4AuthServer);
            break;
        case CLI_PPP_PPPOE_MODE:
            i4Mode = CLI_PTR_TO_I4 (args[0]);
            i4RetVal = CliPppSetMode (CliHandle, i4Mode);
            break;
        case CLI_PPP_AUTH_MODE:
            i4AuthMode = CLI_PTR_TO_I4 (args[0]);
            i4RetVal = CliPppSetAuthMode (CliHandle, i4AuthMode);
            break;
        case CLI_PPP_KEEP_ALIVE_TIMEOUT:
            u4IfIndex = (UINT4) CLI_PTR_TO_U4 (args[0]);

            if (CfaValidateIfIndex (u4IfIndex) == CFA_SUCCESS)
            {
                if ((UINT4 *) (VOID *) args[1] == NULL)
                {
                    u4KeepAlive = 0;
                }
                else
                {
                    u4KeepAlive = *(UINT4 *) (VOID *) args[1];
                }

                i4RetVal =
                    PppCliSetKeepAliveTimeout (CliHandle, (INT4) u4IfIndex,
                                               (INT4) u4KeepAlive);
            }
            else
            {
                CliPrintf (CliHandle,
                           "\r\n%% Attach the PPP interface to an underlying"
                           " physical interface first\r\n");
                i4RetVal = CLI_FAILURE;
            }
            break;

        case CLI_PPP_PEER_PARAMS_ENABLE:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();

            if (CfaGetIfInfo (u4IfIndex, &IfInfo) == CFA_SUCCESS)
            {
                if (args[0] != NULL)
                {
                    u4IpAddr = *(UINT4 *) (VOID *) (args[0]);
                    i4RetVal = PppCliSetPeerIpAddress (CliHandle, u4IfIndex,
                                                       u4IpAddr);
                }
                if (args[1] != NULL)
                {
                    u4PrimaryDnsIpAddr = *(UINT4 *) (VOID *) (args[1]);
                    i4RetVal =
                        PppCliSetPeerPrimaryDnsAddress (CliHandle, u4IfIndex,
                                                        u4PrimaryDnsIpAddr);
                }
                if (args[2] != NULL)
                {
                    u4SecondaryDnsIpAddr = *(UINT4 *) (VOID *) (args[2]);
                    i4RetVal =
                        PppCliSetPeerSecondaryDnsAddress (CliHandle, u4IfIndex,
                                                          u4SecondaryDnsIpAddr);
                }
            }
            else
            {
                i4RetVal = CLI_FAILURE;
                CliPrintf (CliHandle,
                           "\r\n%% Attach the PPP interface to an underlying"
                           " physical interface first\r\n");
            }
            break;

        case CLI_PPP_PEER_PARAMS_DISABLE:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();

            if (CfaGetIfInfo (u4IfIndex, &IfInfo) == CFA_SUCCESS)
            {
                if (args[0] != NULL)
                {
                    i4RetVal = PppCliSetPeerIpAddress (CliHandle, u4IfIndex, 0);
                }
                if (args[1] != NULL)
                {
                    i4RetVal = PppCliSetPeerPrimaryDnsAddress (CliHandle,
                                                               u4IfIndex, 0);
                }
                if (args[2] != NULL)
                {
                    i4RetVal = PppCliSetPeerSecondaryDnsAddress (CliHandle,
                                                                 u4IfIndex, 0);
                }
            }
            else
            {
                i4RetVal = CLI_FAILURE;
                CliPrintf (CliHandle,
                           "\r\n%% Attach the PPP interface to an underlying"
                           " physical interface first\r\n");
            }

            break;

        case CLI_PPP_LINK_HOSTNAME:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            pu1HostName = args[0];

            if (CfaGetIfInfo (u4IfIndex, &IfInfo) == CFA_SUCCESS)
            {
                PppCliHostNameCfg (CliHandle, (INT4) u4IfIndex, pu1HostName);
            }
            else
            {
                i4RetVal = CLI_FAILURE;
                CliPrintf (CliHandle,
                           "\r\n%% Attach the PPP interface to an underlying"
                           " physical interface first\r\n");
            }
            break;

        case CLI_PPP_PAP_USRNAME_PWD:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();

            if (CfaGetIfInfo (u4IfIndex, &IfInfo) == CFA_SUCCESS)
            {
                pu1UserName = args[0];
                pu1Password = args[1];
                u4Action = CLI_PTR_TO_U4 (args[2]);
                u1Direction = PPP_LOCAL_TO_REMOTE;
                i4RetVal = PppCliPapChapCfg (CliHandle, (INT4) u4IfIndex,
                                             u1Direction, pu1UserName,
                                             pu1Password, (UINT1) u4Action);
            }
            else
            {
                i4RetVal = CLI_FAILURE;
                CliPrintf (CliHandle,
                           "\r\n%% Attach the PPP interface to an underlying"
                           " physical interface first\r\n");
            }
            break;

        case CLI_PPP_PAP_AUTH_USRNAME_PWD:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();

            if (CfaGetIfInfo (u4IfIndex, &IfInfo) == CFA_SUCCESS)
            {
                pu1UserName = args[0];
                pu1Password = args[1];
                u4Action = CLI_PTR_TO_U4 (args[2]);
                u1Direction = PPP_REMOTE_TO_LOCAL;
                i4RetVal = PppCliPapChapCfg (CliHandle, (INT4) u4IfIndex,
                                             u1Direction, pu1UserName,
                                             pu1Password, (UINT1) u4Action);
            }
            else
            {
                i4RetVal = CLI_FAILURE;
                CliPrintf (CliHandle,
                           "\r\n%% Attach the PPP interface to an underlying"
                           " physical interface first\r\n");
            }
            break;

        case PPP_SET_CLI_TRACE_LEVEL:
            u4TraceLevel = CLI_PTR_TO_U4 (args[0]);
            i4RetVal =
                CliPppSetTraceLevel (CliHandle, u4TraceLevel, CLI_TRACE_ENABLE);
            break;

        case PPP_RESET_CLI_TRACE_LEVEL:
            u4TraceLevel = CLI_PTR_TO_U4 (args[0]);
            i4RetVal =
                CliPppSetTraceLevel (CliHandle, u4TraceLevel,
                                     CLI_TRACE_DISABLE);
            break;

        case PPP_CLI_SHOW_ADDRESS_POOL:
            i4RetVal = CliPppShowPppLocalPools (CliHandle);
            break;

        case PPP_CLI_SHOW_BINDINGS:
            i4RetVal = CliPppShowPppBindings (CliHandle);
            break;

        case CLI_PPP_ENCAPSULATION_VLANID:

            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();

            if (CfaValidateIfIndex (u4IfIndex) == CFA_SUCCESS)
            {
                if ((VOID *) args[1] == NULL)
                {
                    i4EncapsulationVlanid = 0;
                }
                else
                {
                    i4EncapsulationVlanid = *(INT4 *) (VOID *) (args[1]);
                }
                i4RetVal =
                    PppCliSetEncapsulationVlanId (CliHandle, (INT4) u4IfIndex,
                                                  i4EncapsulationVlanid);
            }
            else
            {
                i4RetVal = CLI_FAILURE;
                CliPrintf (CliHandle,
                           "\r\n%% Attach the PPP interface to an underlying"
                           " physical interface first\r\n");
            }
            break;

        case CLI_PPP_ENCAPSULATION_PRIORITY:
             

            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();

            if (CfaValidateIfIndex (u4IfIndex) == CFA_SUCCESS)
            {
                if ((VOID *) args[1] == NULL)
                {
                    i4Priority = 0;
                }
                else
                {
                    i4Priority = *(INT4 *) (VOID *)( args[1]);
                }
                i4RetVal =
                    PppCliSetEncapsulationPriority (CliHandle, (INT4) u4IfIndex,
                                                    i4Priority);
            }
            else
            {
                i4RetVal = CLI_FAILURE;
                CliPrintf (CliHandle,
                           "\r\n%% Attach the PPP interface to an underlying"
                           " physical interface first\r\n");
            }
            break;

        case CLI_PPP_ENCAPSULATION_CFI:
            

            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();

            if (CfaValidateIfIndex (u4IfIndex) == CFA_SUCCESS)
            {
                if ((VOID *) args[1] == NULL)
                {
                    i4Cfi = 0;
                }
                else
                {
                    i4Cfi = *(INT4 *) (VOID *)(args[1]);
                }
                i4RetVal =
                    PppCliSetEncapsulationCfi (CliHandle, (INT4) u4IfIndex,
                                                     i4Cfi);
            }
            else
            {
                i4RetVal = CLI_FAILURE;
                CliPrintf (CliHandle,
                           "\r\n%% Attach the PPP interface to an underlying"
                           " physical interface first\r\n");
            }
            break;
        case CLI_PPPOE_SERVICE_NAME:
            pu1ServiceName = args[0];    
            u4Action = CLI_PTR_TO_U4 (args[1]);
            i4RetVal = PppCliServiceNameCfg(CliHandle, pu1ServiceName, u4Action); 
	    break;
        case CLI_PPP_PADR_MAX_RETRY:
            u4PadrRetry = *(UINT4 *) (VOID *)args[0];
            i4RetVal = PppCliSetPadrRetryCount(CliHandle,(INT4)u4PadrRetry);         
	    break;
        default:
            break;
    }
    if ((CLI_FAILURE == i4RetVal) && (CLI_SUCCESS == CLI_GET_ERR (&u4ErrCode)))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_PPP_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", PppCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS ((UINT4) i4RetVal);
    return i4RetVal;
}

/***************************************************************************
 * FUNCTION NAME : PppCliSetPeerIpAddress                                  *
 * DESCRIPTION   : Configures the IP address that will be supplied to peer *
 *                 during negotiation.                                     *
 * INPUT         : CliHandle     - CLI context id to be used by CliPrintf  *
 *                 u4IfIndex     - Index of the ppp interface              *
 *                 u4IpAddr      - IP address for peer                     *
 * RETURNS       : returns 0 on CLI_SUCCESS                                *
 *                 returns -1 on CLI_FAILURE                               *
 ***************************************************************************/

INT4
PppCliSetPeerIpAddress (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4IpAddr)
{
    UINT4               u4ErrCode;

    if (nmhTestv2PppExtIpRemoteToLocAddress (&u4ErrCode,
                                             (INT4) u4IfIndex, u4IpAddr)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetPppExtIpRemoteToLocAddress ((INT4) u4IfIndex, u4IpAddr) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME : PppCliSetPeerDnsAddress                                 *
 * DESCRIPTION   : Configures the DNS address that will be supplied to the *
 *                 peer during negotiation.                                *
 * INPUT         : CliHandle     - CLI context id to be used by CliPrintf  *
 *                 u4IfIndex     - Index of the ppp interface              *
 *                 u4DnsIpAddr   - DNS IP address for peer                 *
 * RETURNS       : returns 0 on CLI_SUCCESS                                *
 *                 returns -1 on CLI_FAILURE                               *
 ***************************************************************************/

INT4
PppCliSetPeerPrimaryDnsAddress (tCliHandle CliHandle, UINT4 u4IfIndex,
                                UINT4 u4PrimaryDnsIpAddr)
{
    UINT4               u4ErrCode;

    if (nmhTestv2PppExtIpRemoteToLocPrimaryDNSAddress (&u4ErrCode,
                                                       (INT4) u4IfIndex,
                                                       u4PrimaryDnsIpAddr)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetPppExtIpRemoteToLocPrimaryDNSAddress
        ((INT4) u4IfIndex, u4PrimaryDnsIpAddr) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME : PppCliSetPeerSecondaryDnsAddress                        *
 * DESCRIPTION   : Configures the DNS address that will be supplied to the *
 *                 peer during negotiation.                                *
 * INPUT         : CliHandle     - CLI context id to be used by CliPrintf  *
 *                 u4IfIndex     - Index of the ppp interface              *
 *                 u4SecondaryDnsIpAddr -Secondary DNS IP address for peer *
 * RETURNS       : returns 0 on CLI_SUCCESS                                *
 *                 returns -1 on CLI_FAILURE                               *
 ***************************************************************************/

INT4
PppCliSetPeerSecondaryDnsAddress (tCliHandle CliHandle, UINT4 u4IfIndex,
                                  UINT4 u4SecondaryDnsIpAddr)
{
    UINT4               u4ErrCode;

    if (nmhTestv2PppExtIpRemoteToLocSecondaryDNSAddress (&u4ErrCode,
                                                         (INT4) u4IfIndex,
                                                         u4SecondaryDnsIpAddr)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetPppExtIpRemoteToLocSecondaryDNSAddress
        ((INT4) u4IfIndex, u4SecondaryDnsIpAddr) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME : PppCliSetKeepAliveTimeout                               *
 * DESCRIPTION   : Configures the Idle timeout value of the PPP Link       *
 * INPUT         : CliHandle     - CLI context id to be used by CliPrintf  *
 *                 i4PppIfaceNo  - Index of the ppp interface              *
 *                 i4IdleTimeout - Idle timeout value                      *
 * RETURNS       : returns 0 on CLI_SUCCESS                                *
 *                 returns -1 on CLI_FAILURE                               *
 ***************************************************************************/
INT4
PppCliSetKeepAliveTimeout (tCliHandle CliHandle, INT4 i4PppIfaceNo,
                           INT4 i4KeepAliveTimeout)
{
    UINT4               u4ErrorCode;

    /* Set the Maximum Idle Timeout for the PPP Link */
    if (nmhTestv2PppExtKeepAliveTimeOut (&u4ErrorCode, i4PppIfaceNo,
                                         i4KeepAliveTimeout) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% Invalid keep-alive timeout value\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetPppExtKeepAliveTimeOut
        (i4PppIfaceNo, i4KeepAliveTimeout) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME : PppCliSetEncapsulationVlanId                            *
 * DESCRIPTION   : Configures the Encapsulation VLAN-Id                    *
 * INPUT         : CliHandle     - CLI context id to be used by CliPrintf  *
 *                 i4VlanId      - VLAN ID for encapsulation of PPP.       *
 * RETURNS       : returns 0 on CLI_SUCCESS                                *
 *                 returns -1 on CLI_FAILURE                               *
 ***************************************************************************/
INT4
PppCliSetEncapsulationVlanId (tCliHandle CliHandle, INT4 i4PppIfaceNo,
                              INT4 i4VlanId)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2PPPoEVlanID (&u4ErrorCode, i4PppIfaceNo, i4VlanId) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% Invalid VLAN ID value\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetPPPoEVlanID (i4PppIfaceNo, i4VlanId) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME : PppCliSetEncapsulationPriority                          *
 * DESCRIPTION   : Configures the Encapsulation priority                   *
 * INPUT         : CliHandle     - CLI context id to be used by CliPrintf  *
 *                 i4Priority - Priority to be set                         *
 * RETURNS       : returns 0 on CLI_SUCCESS                                *
 *                 returns -1 on CLI_FAILURE                               *
 ****************************************************************************/
INT4
PppCliSetEncapsulationPriority (tCliHandle CliHandle, INT4 i4PppIfaceNo,
                                INT4 i4Priority)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2PPPoECoS (&u4ErrorCode, i4PppIfaceNo, i4Priority) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% Invalid CoS value\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetPPPoECoS (i4PppIfaceNo, i4Priority) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME : PppCliSetEncapsulationCfi                               *
 * DESCRIPTION   : Configures the Encapsulation CFI bit                    *
 * INPUT         : CliHandle     - CLI context id to be used by CliPrintf  *
 *                 i4CFI - CFI to be set                              *
 * RETURNS       : returns 0 on CLI_SUCCESS                                *
 *                 returns -1 on CLI_FAILURE                               *
 *****************************************************************************/
INT4
PppCliSetEncapsulationCfi (tCliHandle CliHandle, INT4 i4PppIfaceNo, INT4 i4Cfi)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2PPPoECFI (&u4ErrorCode, i4PppIfaceNo, i4Cfi) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% Invalid CFI value\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetPPPoECFI (i4PppIfaceNo, i4Cfi) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*****************************************************************************/
INT4
CliPppSetTraceLevel (tCliHandle CliHandle, UINT4 u4PppTraceLevel,
                     UINT1 u1TraceFlag)
{
    UINT4               u4TraceLevel = 0;

    /* Do an OR operation with the current TraceLevel, if the SET value is
     * not DISABLE_ALL, in which case all bits are to be reset */
    if (SNMP_FAILURE == nmhGetPppDebugLevelMask ((INT4 *) &u4TraceLevel))
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (u1TraceFlag == CLI_TRACE_ENABLE)
    {
        u4PppTraceLevel = u4TraceLevel | u4PppTraceLevel;
    }

    else if (u1TraceFlag == CLI_TRACE_DISABLE)
    {
        u4PppTraceLevel = u4TraceLevel & (~u4PppTraceLevel);
    }

    if (nmhSetPppDebugLevelMask ((INT4) u4PppTraceLevel) == SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }
    else
    {
        CliPrintf (CliHandle, "\r\n%% Could not set trace level!\r\n");
        return CLI_FAILURE;
    }
}

/***************************************************************************  
 * FUNCTION NAME : CliPppShowPppBindings
 * DESCRIPTION   : This function is used to display ppp client bindings address.
 * INPUT         : pPppCliConfigParams - Config params
 * OUTPUT        : ppRespMsg - Formatted output message 
 * RETURNS       : NONE
 ***************************************************************************/
INT4
CliPppShowPppBindings (tCliHandle CliHandle)
{
    INT4                i4PppIndex;
    INT4                i4PoolIndex;
    CHR1               *pi1IpAddr;
    UINT4               u4StartAddr;
    UINT4               u4EndAddr;
    UINT4               u4ServerAddr;
    UINT4               u4ClientAddr;
    INT4                i4PPPoeMode = 0;
    tPPPIf              pIf;

    MEMSET (&pIf, 0, sizeof (tPPPIf));

    nmhGetPPPoEMode (&i4PPPoeMode);
    if (i4PPPoeMode == PPPOE_CLIENT)
    {
        CliPrintf (CliHandle, "\r\nPPPOE Mode : PPPOE Client\r\n");
        CliPrintf (CliHandle,
                   "\r\nNo Binding IP associated for PPPOE Client!\r\n");
        return CLI_SUCCESS;
    }
    else if (i4PPPoeMode == PPPOE_SERVER)
    {
        CliPrintf (CliHandle, "\r\nPPPOE Mode : PPPOE Server\r\n");
        if (nmhGetFirstIndexPppExtIpTable (&i4PppIndex) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nNo PPP Interface created !\r\n");
            return CLI_SUCCESS;
        }
        CliPrintf (CliHandle,
                   "\r\nPool ID        Server Address       Client Address\r\n"
                   "-------        -------------        -----------\r\n");
        do
        {
            if (nmhGetPppExtIpLocToRemoteAddress (i4PppIndex, &u4ServerAddr) ==
                SNMP_FAILURE)
            {
                break;
            }

            pIf.LinkInfo.IfIndex = (UINT4) i4PppIndex;
            if (IPCPCheckAssignedAddrForInterface (&pIf, &u4ClientAddr) !=
                FAILURE)
            {
                if (nmhGetFirstIndexPppExtIPAddressConfigTable (&i4PoolIndex) ==
                    SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\nNo address pools are configured !\r\n");
                    return CLI_SUCCESS;
                }
                do
                {
                    nmhGetPppExtIPAddressPoolLowerRange (i4PoolIndex,
                                                         &u4StartAddr);
                    nmhGetPppExtIPAddressPoolUpperRange (i4PoolIndex,
                                                         &u4EndAddr);
                    if ((u4ClientAddr >= u4StartAddr)
                        && (u4ClientAddr <= u4EndAddr))
                    {
                        break;
                    }
                }
                while (nmhGetNextIndexPppExtIPAddressConfigTable
                       (i4PoolIndex, &i4PoolIndex) == SNMP_SUCCESS);
                CliPrintf (CliHandle, "%-15ld", i4PoolIndex);
                CLI_CONVERT_IPADDR_TO_STR (pi1IpAddr, u4ServerAddr);
                CliPrintf (CliHandle, "%-21s", pi1IpAddr);
                CLI_CONVERT_IPADDR_TO_STR (pi1IpAddr, u4ClientAddr);
                CliPrintf (CliHandle, "%s\n", pi1IpAddr);
            }
        }
        while (nmhGetNextIndexPppExtIpTable (i4PppIndex, &i4PppIndex) ==
               SNMP_SUCCESS);
    }
    return CLI_SUCCESS;
}

/***************************************************************************  
 * FUNCTION NAME : CliPppShowPppLocalPools
 * DESCRIPTION   : This function is used to display ppp local address pools.
 * INPUT         : pPppCliConfigParams - Config params
 * OUTPUT        : ppRespMsg - Formatted output message 
 * RETURNS       : NONE
 ***************************************************************************/
INT4
CliPppShowPppLocalPools (tCliHandle CliHandle)
{
    INT4                i4Index;
    UINT4               u4Count = 0;
    CHR1               *pi1IpAddr;
    UINT4               u4StartAddr = 0;
    UINT4               u4EndAddr = 0;
    INT4                i4PPPoeMode = 0;

    nmhGetPPPoEMode (&i4PPPoeMode);
    if (i4PPPoeMode == PPPOE_SERVER)
    {
        CliPrintf (CliHandle, "PPPOE Mode : PPPOE Server\r\n");
    }
    else if (i4PPPoeMode == PPPOE_CLIENT)
    {
        CliPrintf (CliHandle, "PPPOE Mode : PPPOE Client\r\n");
    }

    if (nmhGetFirstIndexPppExtIPAddressConfigTable (&i4Index) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\nNo address pools are configured !\r\n");
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle,
               "\r\nPool ID        Start Adddress       End Address\r\n"
               "-------        -------------        -----------\r\n");
    do
    {

        CliPrintf (CliHandle, "%-15ld", i4Index);

        nmhGetPppExtIPAddressPoolLowerRange (i4Index, &u4StartAddr);
        CLI_CONVERT_IPADDR_TO_STR (pi1IpAddr, u4StartAddr);
        CliPrintf (CliHandle, "%-21s", pi1IpAddr);

        nmhGetPppExtIPAddressPoolUpperRange (i4Index, &u4EndAddr);
        CLI_CONVERT_IPADDR_TO_STR (pi1IpAddr, u4EndAddr);
        CliPrintf (CliHandle, "%s\r\n", pi1IpAddr);

        u4Count++;
        if (u4Count == PPP_MAX_IP_POOL)
            break;

    }
    while (nmhGetNextIndexPppExtIPAddressConfigTable (i4Index, &i4Index)
           == SNMP_SUCCESS);
    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}
/***************************************************************************
 * * FUNCTION NAME : CliPppShowPppoeStatistics
 * * DESCRIPTION   : This function is used to display ppp statistics.
 * * INPUT         : pPppCliConfigParams - Config params
 * * OUTPUT        : ppRespMsg - Formatted output message
 * * RETURNS       : NONE
 *  ***************************************************************************/
INT4
CliPppShowPppoeStatistics (tCliHandle CliHandle)
{

    INT4                i4IfIdx;
    UINT4               u4PADIRX = 0;
    UINT4               u4PADIRej = 0;
    UINT4               u4PADOTX = 0;
    UINT4               u4PADRRX = 0;
    UINT4               u4PADRRej = 0;
    UINT4               u4PADSTX = 0;
    UINT4               u4PADTTX = 0;
    UINT4               u4PADTRX = 0;
    UINT4               u4PADITX = 0;
    UINT4               u4PADORX = 0;
    UINT4               u4PADSRX = 0;
    UINT4               u4PADORej = 0;
    UINT4               u4PADRTX = 0;
    UINT4               u4PADSRej = 0;
    UINT4               u4PADTRej = 0;    
    /* Display All */
    CliPrintf (CliHandle, "PPPoE Statistics\r\n");
    CliPrintf (CliHandle, "----------------\r\n");

    if (nmhGetFirstIndexPPPoEBindingsTable (&i4IfIdx) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "No address pools are configured !\r\n");
        return CLI_SUCCESS;
    }
    do
    {
             
             nmhGetPPPoEBindingsNumberPADSReceived (i4IfIdx, &u4PADSRX);
             nmhGetPPPoEBindingsNumberPADOReceived (i4IfIdx, &u4PADORX);
	     nmhGetPPPoEBindingsNumberPADITransmitted (i4IfIdx, &u4PADITX);  
             nmhGetPPPoEBindingsNumberPADIReceived (i4IfIdx, &u4PADIRX);
             nmhGetPPPoEBindingsNumberPADIRejected (i4IfIdx, &u4PADIRej);
             nmhGetPPPoEBindingsNumberPADOTransmitted (i4IfIdx, &u4PADOTX);
             nmhGetPPPoEBindingsNumberPADRReceived (i4IfIdx, &u4PADRRX);
             nmhGetPPPoEBindingsNumberPADRRejected (i4IfIdx, &u4PADRRej);
             nmhGetPPPoEBindingsNumberPADSTransmitted (i4IfIdx, &u4PADSTX);
             nmhGetPPPoEBindingsNumberPADTTransmitted (i4IfIdx, &u4PADTTX);
             nmhGetPPPoEBindingsNumberPADTReceived (i4IfIdx, &u4PADTRX);
             nmhGetPPPoEBindingsNumberPADORejected (i4IfIdx, &u4PADORej);
             nmhGetPPPoEBindingsNumberPADRTransmitted(i4IfIdx, &u4PADRTX);
             nmhGetPPPoEBindingsNumberPADSRejected (i4IfIdx, &u4PADSRej);
             nmhGetPPPoEBindingsNumberPADTRejected (i4IfIdx, &u4PADTRej);
             

       }
       while (nmhGetNextIndexPPPoEBindingsTable (i4IfIdx, &i4IfIdx)
                             == SNMP_SUCCESS);

    /*CliPrintf (CliHandle, "%-21s : %d\r\n", "PADIRX", u4QIdx);*/
    CliPrintf (CliHandle, "%-21s : %-151d\r\n", "PPPOE PADI PACKETS TRANSMITTED", u4PADITX);
    CliPrintf (CliHandle, "%-21s    : %-151d\r\n", "PPPOE PADI PACKETS RECEIVED", u4PADIRX);
    CliPrintf (CliHandle, "%-21s    : %-151d\r\n", "PPPOE PADI PACKETS REJECTED", u4PADIRej);
    CliPrintf (CliHandle, "%-21s : %-151d\r\n", "PPPOE PADO PACKETS TRANSMITTED", u4PADOTX);
    CliPrintf (CliHandle, "%-21s    : %-151d\r\n", "PPPOE PADO PACKETS RECEIVED", u4PADORX);
    CliPrintf (CliHandle, "%-21s    : %-151d\r\n", "PPPOE PADO PACKETS REJECTED", u4PADORej);
    CliPrintf (CliHandle, "%-21s : %-151d\r\n", "PPPOE PADR PACKETS TRANSMITTED", u4PADRTX);
    CliPrintf (CliHandle, "%-21s    : %-151d\r\n", "PPPOE PADR PACKETS RECEIVED", u4PADRRX);
    CliPrintf (CliHandle, "%-21s    : %-151d\r\n", "PPPOE PADR PACKETS REJECTED", u4PADRRej);
    CliPrintf (CliHandle, "%-21s : %-151d\r\n", "PPPOE PADS PACKETS TRANSMITTED", u4PADSTX);
    CliPrintf (CliHandle, "%-21s    : %-151d\r\n", "PPPOE PADS PACKETS RECEIVED", u4PADSRX);
    CliPrintf (CliHandle, "%-21s    : %-151d\r\n", "PPPOE PADS PACKETS REJECTED", u4PADSRej);
    CliPrintf (CliHandle, "%-21s : %-151d\r\n", "PPPOE PADT PACKETS TRANSMITTED", u4PADTTX);
    CliPrintf (CliHandle, "%-21s    : %-151d\r\n", "PPPOE PADT PACKETS RECEIVED", u4PADTRX);
    CliPrintf (CliHandle, "%-21s    : %-151d\r\n", "PPPOE PADT PACKETS REJECTED", u4PADTRej);
    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);
}

/***************************************************************************
* FUNCTION NAME : ShowClearStat
* DESCRIPTION   : This function is used to display ppp statistics.
* INPUT         : pPppCliConfigParams - Config params
* OUTPUT        : ppRespMsg - Formatted output message
* RETURNS       : NONE
 ***************************************************************************/


INT4
ShowClearStat(tCliHandle CliHandle,INT4 i4PPPoEBindingsIndex )
{

    tPPPoEBinding      *pPPPoEIf;
    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEBindingsIndex)) == NULL)
    {
        CliPrintf (CliHandle, "No address pools are configured !\r\n");
        return CLI_SUCCESS;
    }
    else
    {
 
        CliPrintf (CliHandle, "%-21s : %-151d\r\n", "PPPOE PADI PACKETS TRANSMITTED", pPPPoEIf->NoOfPADITrans);
        CliPrintf (CliHandle, "%-21s    : %-151d\r\n", "PPPOE PADI PACKETS RECEIVED", pPPPoEIf->NoOfPADIRecv);
        CliPrintf (CliHandle, "%-21s    : %-151d\r\n", "PPPOE PADI PACKETS REJECTED", pPPPoEIf->NoOfPADIRej);
        CliPrintf (CliHandle, "%-21s : %-151d\r\n", "PPPOE PADO PACKETS TRANSMITTED", pPPPoEIf->NoOfPADOTrans);
        CliPrintf (CliHandle, "%-21s    : %-151d\r\n", "PPPOE PADO PACKETS RECEIVED", pPPPoEIf->NoOfPADORecv);
        CliPrintf (CliHandle, "%-21s    : %-151d\r\n", "PPPOE PADO PACKETS REJECTED", pPPPoEIf->NoOfPADORej);
        CliPrintf (CliHandle, "%-21s : %-151d\r\n", "PPPOE PADR PACKETS TRANSMITTED", pPPPoEIf->NoOfPADRTrans);
        CliPrintf (CliHandle, "%-21s    : %-151d\r\n", "PPPOE PADR PACKETS RECEIVED", pPPPoEIf->NoOfPADRRecv);
        CliPrintf (CliHandle, "%-21s    : %-151d\r\n", "PPPOE PADR PACKETS REJECTED", pPPPoEIf->NoOfPADRRej);
        CliPrintf (CliHandle, "%-21s : %-151d\r\n", "PPPOE PADS PACKETS TRANSMITTED", pPPPoEIf->NoOfPADSTrans);
        CliPrintf (CliHandle, "%-21s    : %-151d\r\n", "PPPOE PADS PACKETS RECEIVED", pPPPoEIf->NoOfPADSRecv);
        CliPrintf (CliHandle, "%-21s    : %-151d\r\n", "PPPOE PADS PACKETS REJECTED", pPPPoEIf->NoOfPADSRej);
        CliPrintf (CliHandle, "%-21s : %-151d\r\n", "PPPOE PADT PACKETS TRANSMITTED", pPPPoEIf->NoOfPADTTrans);
        CliPrintf (CliHandle, "%-21s    : %-151d\r\n", "PPPOE PADT PACKETS RECEIVED", pPPPoEIf->NoOfPADTRecv);
        CliPrintf (CliHandle, "%-21s    : %-151d\r\n", "PPPOE PADT PACKETS REJECTED", pPPPoEIf->NoOfPADTRej);
        CliPrintf (CliHandle, "\r\n");
   }
   return CLI_SUCCESS;


}

/***************************************************************************
 * * FUNCTION NAME : ClearPPPoEStat
 * * DESCRIPTION   : This function is used to clear ppp statistics.
 * * INPUT         : pPppCliConfigParams - Config params
 * * OUTPUT        : ppRespMsg - Formatted output message
 * * RETURNS       : NONE
 *  ***************************************************************************/


INT4
ClearPPPoEStat (tCliHandle CliHandle,INT4 i4PPPoEBindingsIndex)
{
    tPPPoEBinding      *pPPPoEIf;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEBindingsIndex)) == NULL)
    {
        CliPrintf (CliHandle, "No address pools are configured !\r\n");
        return CLI_SUCCESS;
    }
    else
    {
        pPPPoEIf->NoOfPADITrans = 0;
        pPPPoEIf->NoOfPADIRecv = 0;
        pPPPoEIf->NoOfPADIRej = 0;
        pPPPoEIf->NoOfPADOTrans = 0;
        pPPPoEIf->NoOfPADORecv = 0;
        pPPPoEIf->NoOfPADORej = 0;
        pPPPoEIf->NoOfPADRTrans = 0;
        pPPPoEIf->NoOfPADRRecv = 0;
        pPPPoEIf->NoOfPADRRej = 0;
        pPPPoEIf->NoOfPADSTrans = 0;
        pPPPoEIf->NoOfPADSRecv = 0;
        pPPPoEIf->NoOfPADSRej = 0;
        pPPPoEIf->NoOfPADTTrans = 0;
        pPPPoEIf->NoOfPADTRecv = 0;
        pPPPoEIf->NoOfPADTRej = 0;


    }
    return (CLI_SUCCESS);

}
/***************************************************************************
* FUNCTION NAME : CliPppClearPppoestatistics
* DESCRIPTION   : This function is used to clear ppp statistics.
* INPUT         : pPppCliConfigParams - Config params
* OUTPUT        : ppRespMsg - Formatted output message
* RETURNS       : NONE
***************************************************************************/

INT4
CliPppClearPppoeStatistics (tCliHandle CliHandle)
{

    INT4                i4IfIdx;
    /* Display All */
    CliPrintf (CliHandle, "PPPoE Statistics\r\n");
    CliPrintf (CliHandle, "----------------\r\n");

    if (nmhGetFirstIndexPPPoEBindingsTable (&i4IfIdx) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "No address pools are configured !\r\n");
        return CLI_SUCCESS;
    }
    do
    {

        ClearPPPoEStat(CliHandle,i4IfIdx);

    }
    while (nmhGetNextIndexPPPoEBindingsTable (i4IfIdx, &i4IfIdx)
                             == SNMP_SUCCESS);


    ShowClearStat(CliHandle, i4IfIdx);


    /*CliPrintf (CliHandle, "%-21s : %d\r\n", "PADIRX", u4QIdx);*/

    return (CLI_SUCCESS);
}






/*****************************************************************************/
/*****************************************************************************/
INT4
cli_set_l2tp_static_mode (UINT4 u4ServerIpAddr, UINT1 *pu1UserName,
                          UINT1 *pu1Password, UINT4 u4AuthType,
                          UINT4 u4IdleTimeOut, UINT4 u4KeepAlive,
                          UINT4 u4ConnectType, UINT4 u4IpAddr,
                          UINT4 u4Mask, UINT4 u4Gateway,
                          UINT1 *pu1PriDns, UINT1 *pu1SecDns)
{
    gu1PrvAccessType = gu1AccessType;
#ifndef L2TP_WANTED
    UNUSED_PARAM (u4ServerIpAddr);
#endif
#ifdef REVISIT
    /* Delete the pptp and l2tp server configurations, if present */
    DeleteServerConf ();

    /*Delete the Current WAN Access interface */
    if (CliDeleteWanIntfConf () == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
#endif
    MEMSET (&gCfaWanInfo, 0, sizeof (tWanInfo));

    cli_set_access_method (CFA_WAN_TYPE_L2TP);

#ifdef REVISIT
    CliStoreEthernetStaticConf (u4IpAddr, u4Mask, u4Gateway, pu1PriDns,
                                pu1SecDns);

    CliStoreL2tpConf (u4ServerIpAddr, pu1UserName, pu1Password, u4AuthType,
                      u4IdleTimeOut, u4KeepAlive);

    if (CreateEthernetInterfaceStatic (u4IpAddr, u4Mask, u4Gateway,
                                       pu1PriDns, pu1SecDns) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }
#else
    UNUSED_PARAM (u4IpAddr);
    UNUSED_PARAM (u4Mask);
    UNUSED_PARAM (u4Gateway);
    UNUSED_PARAM (pu1PriDns);
    UNUSED_PARAM (pu1SecDns);
#endif
    if (CreateL2tpPppInterface (pu1UserName, pu1Password, u4AuthType,
                                u4IdleTimeOut, u4KeepAlive, u4ConnectType, NULL)
        != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* For Manual connection we do not make ppp iface admin status up immediately
     * Hence, CustomTrigger is not called and also IkeSetTunnTermAddr is not
     * called. Thus, for manual connection we do this explictly here */

#ifdef REVISIT
    if (STRCMP (pu1ConnectType, "manual") == 0)
    {
        IkeSetTunnTermAddr (gCfaWanInfo.AccessMethod.Pptp.u4PhysIfIndex);
    }

    if (((u4ServerIpAddr) & (u4Mask)) != ((u4IpAddr) & u4Mask))
    {
        IpRouteAdd ((u4ServerIpAddr), 0xffffffff, 1, (u4Gateway), 1);
    }
#endif

#ifdef L2TP_WANTED
    L2TPSetRemoteServerAddr (u4ServerIpAddr);
#endif

    cli_set_access_info (&gCfaWanInfo);

#ifdef REVISIT
    /* Wan interface should be set as External Firewall
       to prevent attacks . 1 - internal, 2 - external */
    if (cli_process_fwl_cmd (CLI_SET_FWL_IFTYPE, NULL, "2") == CLI_ERROR)
    {
        mmi_printf ("ERROR: Unable to set Firewalling on Wan Interface!\r\n");
        return (CLI_ERROR);
    }

    /*Delete the Previous WAN Access command */
    DeleteExistingConf ();
    L2TPAddFwlFilterClient (u4ServerIpAddr);
#endif
    return CLI_SUCCESS;
}

/***************************************************************************  
 * FUNCTION NAME : CliPppCreateAddressPool
 * DESCRIPTION   : This function is used to create ppp local address pool.
 * INPUT         : u4StartAddress, u4EndAddress, u4AddrPoolIndex
 * OUTPUT        : None
 * RETURNS       : CLI_SUCCESS/CLI_FAILURE
 ***************************************************************************/

INT4
CliPppCreateAddressPool (tCliHandle CliHandle, UINT4 u4AddrPoolIndex,
                         UINT4 u4StartAddress, UINT4 u4EndAddress)
{
    UINT4 u4NetMask = DEFAULT_PPP_NETMASK;
    UINT4 u4NetworkId = 0;
#ifdef L2TP_WANTED
    if (L2TPValidateIpcpAddressPool (u4StartAddress, u4EndAddress)
        != L2TP_SUCCESS)
    {
        CliPrintf (CliHandle, "L2tpServer address might be configured in this",
                   " range. Change either Pool range or l2tp",
                   " server address \r\n");
        return CLI_FAILURE;
    }
#endif

    if (!(u4StartAddress & 0x000000ff))
    {
        /* If the start IP begins in the format of x.x.x.0, then the start
         * IP should start from x.x.x.1 */
         u4StartAddress = u4StartAddress + 1;
    }
    u4NetworkId = (u4StartAddress & u4NetMask);
    if (((u4StartAddress & u4NetMask) != u4NetworkId))
    {
        CliPrintf (CliHandle, "\r%% Invalid Network & Subnet Mask combination\r\n");
	return CLI_FAILURE;
    }
    else if (((u4EndAddress & u4NetMask) != u4NetworkId))
    {
        CliPrintf (CliHandle, "\r%% Invalid Subnet."
                   "Check if End IP address belongs to the same subnet\r\n");
	return CLI_FAILURE;
    }
    if (u4StartAddress > u4EndAddress)
    {
        CliPrintf (CliHandle, "\r%% Start IP address should be Less than End IP address\r\n");
	return CLI_FAILURE;
    }
    nmhSetPppExtIPAddressPoolSelector (IPCP_LOCAL_POOL);
    if (nmhSetPppExtIPAddressPoolLowerRange
        ((INT4) u4AddrPoolIndex, u4StartAddress) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Address Pool might be in use or Already "
                   "%d pools are configured !\r\n", PPP_MAX_IP_POOL);
        return CLI_FAILURE;
    }
    if (nmhSetPppExtIPAddressPoolUpperRange
        ((INT4) u4AddrPoolIndex, u4EndAddress) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Address Pool might be in use or Already "
                   "%d pools are configured !\r\n", PPP_MAX_IP_POOL);
        return CLI_FAILURE;
    }
    if (nmhSetPppExtIPAddressPoolStatus ((INT4) u4AddrPoolIndex, IPCP_UP)
        == SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************  
 * FUNCTION NAME : CliPppDeleteAddressPool
 * DESCRIPTION   : This function is used to delete ppp local address pool.
 * INPUT         : u4AddrPoolIndex
 * OUTPUT        : None
 * RETURNS       : CLI_SUCCESS/CLI_FAILURE
 ***************************************************************************/
INT4
CliPppDeleteAddressPool (tCliHandle CliHandle, UINT4 u4AddrPoolIndex)
{
    if (nmhSetPppExtIPAddressPoolStatus ((INT4) u4AddrPoolIndex, IPCP_DELETE)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Can not delete, Pool might be in use !\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************  
 * FUNCTION NAME : CliPppSetAuthServer
 * DESCRIPTION   : This function is used to set Authentication Server
 * INPUT         : i4AuthServer
 * OUTPUT        : None
 * RETURNS       : CLI_SUCCESS/CLI_FAILURE
 ***************************************************************************/
INT4
CliPppSetAuthServer (tCliHandle CliHandle, INT4 i4AuthServer)
{
    UINT4               u4SnmpErrorStatus;

    if (nmhTestv2PppaaaMethod (&u4SnmpErrorStatus, i4AuthServer) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Could not set auth server!\r\n");
        return CLI_FAILURE;
    }
    nmhSetPppaaaMethod (i4AuthServer);
    return CLI_SUCCESS;
}

INT4
CliPppSetAuthMode (tCliHandle CliHandle, INT4 i4AuthMode)
{
    UINT4               u4SnmpErrorStatus;
    INT4                i4tempAuthMode;

    nmhGetPPPAuthMode (&i4tempAuthMode);
    if (i4tempAuthMode == i4AuthMode)
        return CLI_SUCCESS;
    if (nmhTestv2PPPAuthMode (&u4SnmpErrorStatus, i4AuthMode) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Wrong Value!\r\n");
        return CLI_FAILURE;
    }
    else
    {
        nmhSetPPPAuthMode (i4AuthMode);
        return CLI_SUCCESS;
    }
}

 /***************************************************************************
 * FUNCTION NAME : CliPppSetMode
 * DESCRIPTION   : This function is used to set MODE
 * INPUT         : i4Mode
 * OUTPUT        : None
 * RETURNS       : CLI_SUCCESS/CLI_FAILURE
 ***************************************************************************/
INT4
CliPppSetMode (tCliHandle CliHandle, INT4 i4Mode)
{

    UINT4               u4SnmpErrorStatus;
    INT4                i4tempMode;

    nmhGetPPPoEMode (&i4tempMode);
    if (i4tempMode == i4Mode)
        return CLI_SUCCESS;
    if (nmhTestv2PPPoEMode (&u4SnmpErrorStatus, i4Mode) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Wrong Value!\r\n");
        return CLI_FAILURE;
    }
    else
    {
        nmhSetPPPoEMode (i4Mode);
        return CLI_SUCCESS;
    }

}

/******************************************************************************/
/*                                                                            */
/* FUNCTION NAME    : PppGetPppConfigPrompt                                   */
/*                                                                            */
/* DESCRIPTION      : This function validates the given pi1ModeName           */
/*                    and returns the prompt in pi1DispStr if valid.          */
/*                    Returns TRUE if given pi1ModeName is valid.             */
/*                    Returns FALSE if the given pi1ModeName is not valid     */
/*                    pi1ModeName is NULL to display the mode tree with       */
/*                    mode name and prompt string.                            */
/*                                                                            */
/* INPUT            : pi1ModeName- Mode Name                                  */
/*                                                                            */
/* OUTPUT           : pi1DispStr- DIsplay string                              */
/*                                                                            */
/* RETURNS          : TRUE/FALSE                                              */
/*                                                                            */
/******************************************************************************/

INT1
PppGetPppConfigPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;

    if ((pi1DispStr == NULL) || (pi1ModeName == NULL))
    {
        return FALSE;
    }

    u4Len = STRLEN (PPP_CLI_MODE);

    if (STRNCMP (pi1ModeName, PPP_CLI_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    STRCPY (pi1DispStr, "(config-ppp)#");
    return TRUE;
}

/***************************************************************************
 * FUNCTION NAME : PppCliPapChapCfg                                        *
 * DESCRIPTION   : Configures the PAP and CHAP related information         *
 * INPUT         : CliHandle     - CLI context id to be used by CliPrintf  *
 *                 i4PppIfaceNo  - Index of the ppp interface              *
 *                 pu1UserName   - Username for authentication             *
 *                 pu1Password   - Password for authentication             *
 * RETURNS       : returns 0 on CFA_SUCCESS                                *
 *                 returns -1 on CFA_FAILURE                               *
 ***************************************************************************/
INT4
PppCliPapChapCfg (tCliHandle CliHandle, INT4 i4PppIfaceNo,
                  UINT1 u1Direction, UINT1 *pu1UserName,
                  UINT1 *pu1Password, UINT1 u1Action)
{
    tPPPIf             *pIf;
    INT4                i4SecretIdIndex;
    tSNMP_OCTET_STRING_TYPE Identity;
    tSNMP_OCTET_STRING_TYPE Secret;
    UINT1               au1Identity[MAX_SECRET_SIZE];
    UINT1               au1Secret[MAX_SECRET_SIZE];
    INT4                i4AuthMode;

    UNUSED_PARAM (CliHandle);

    MEMSET (au1Identity, 0, MAX_SECRET_SIZE);
    MEMSET (au1Secret, 0, MAX_SECRET_SIZE);

    nmhGetPPPAuthMode (&i4AuthMode);
    if (i4AuthMode == 0)
    {
        CliPrintf (CliHandle, "\r\n%% Configure Authentication mode first\r\n");
    }

    Identity.pu1_OctetList = au1Identity;
    Secret.pu1_OctetList = au1Secret;

    /* Indexes 1,2 are allocated for local-to-remote (calling-side) secrets
     * (one for PAP and one for CHAP)
     * Indexes 3,4 are allocated for remote-to-local (called-side) secrets
     * (one for PAP and one for CHAP)
     */
    if (u1Direction == PPP_LOCAL_TO_REMOTE)
    {
        i4SecretIdIndex = 1;
    }
    else
    {
        i4SecretIdIndex = 3;
    }

    if ((pIf = (tPPPIf *) PppGetIfPtr ((UINT4) i4PppIfaceNo)) == NULL)
    {
        CliPrintf (CliHandle, "\r\n%% Invalid PPP Index\r\n");
        return CLI_FAILURE;
    }

    if (pIf->LcpStatus.AdminStatus == ADMIN_OPEN)
    {
        CliPrintf (CliHandle,
                   "\r\n%% Interface must first be made administratively down\r\n");
        return CLI_FAILURE;
    }

    if (pIf->MPInfo.MemberInfo.pBundlePtr != NULL)
    {
        CliPrintf (CliHandle,
                   "\r\n%% Cannot set username on multilink bundle members\r\n");
        return CLI_FAILURE;
    }

    if (u1Action == CLI_PPP_AUTH_DELETE)
    {
        /* Delete the entry for PAP */
        nmhSetPppSecuritySecretsStatus (i4PppIfaceNo, i4SecretIdIndex,
                                        SECRETS_STATUS_INVALID);
        /* Delete the entry for CHAP */
        i4SecretIdIndex++;
        nmhSetPppSecuritySecretsStatus (i4PppIfaceNo, i4SecretIdIndex,
                                        SECRETS_STATUS_INVALID);
    }
    else
    {
        /* Add PAP entry */
        if (i4AuthMode == PPP_AUTH_PAP)
        {
            nmhSetPppSecuritySecretsProtocol (i4PppIfaceNo, i4SecretIdIndex,
                                              &pppSecurityPapProtocolOID);
            nmhSetPppSecuritySecretsDirection (i4PppIfaceNo, i4SecretIdIndex,
                                               u1Direction);

            Identity.i4_Length = (INT4) STRLEN (pu1UserName);
            STRNCPY (au1Identity, pu1UserName,Identity.i4_Length);
            nmhSetPppSecuritySecretsIdentity (i4PppIfaceNo, i4SecretIdIndex,
                                              &Identity);

            Secret.i4_Length = (INT4) STRLEN (pu1Password);
            STRNCPY (au1Secret, pu1Password,Secret.i4_Length);
            nmhSetPppSecuritySecretsSecret (i4PppIfaceNo, i4SecretIdIndex,
                                            &Secret);

            nmhSetPppSecuritySecretsStatus (i4PppIfaceNo, i4SecretIdIndex,
                                            SECRETS_STATUS_VALID);

            ++i4SecretIdIndex;
        }
        else if (i4AuthMode == PPP_AUTH_CHAP)    /* Add CHAP entry */
        {
            nmhSetPppSecuritySecretsProtocol (i4PppIfaceNo, i4SecretIdIndex,
                                              &pppSecurityChapMD5ProtocolOID);
            nmhSetPppSecuritySecretsDirection (i4PppIfaceNo, i4SecretIdIndex,
                                               u1Direction);
            STRCPY (au1Identity, pu1UserName);
            Identity.i4_Length = (INT4) STRLEN (pu1UserName);
            nmhSetPppSecuritySecretsIdentity (i4PppIfaceNo, i4SecretIdIndex,
                                              &Identity);
            STRCPY (au1Secret, pu1Password);
            Secret.i4_Length = (INT4) STRLEN (pu1Password);
            nmhSetPppSecuritySecretsSecret (i4PppIfaceNo, i4SecretIdIndex,
                                            &Secret);
            nmhSetPppSecuritySecretsStatus (i4PppIfaceNo, i4SecretIdIndex,
                                            SECRETS_STATUS_VALID);
        }
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME : PppCliHostNameCfg                                       *
 * DESCRIPTION   : Configures the Link HostName for use in CHAP Challenge  *
 * INPUT         : CliHandle     - CLI context id to be used by CliPrintf  *
 *                 i4PppIfaceNo  - Index of the ppp interface              *
 *                 pu1HostName   - Username for authentication             *
 * RETURNS       : returns 0 on CFA_SUCCESS                                *
 *                 returns -1 on CFA_FAILURE                               *
 ***************************************************************************/
INT4
PppCliHostNameCfg (tCliHandle CliHandle, INT4 i4PppIfaceNo, UINT1 *pu1HostName)
{
    tSNMP_OCTET_STRING_TYPE tOctetStr;
    UINT1               au1OctetStr[MAX_SECRET_HOSTNAME_SIZE];
    /* Set the Host Name */
    tOctetStr.i4_Length = (INT4) STRLEN (pu1HostName);
    MEMSET (au1OctetStr, 0, MAX_SECRET_HOSTNAME_SIZE);
    tOctetStr.pu1_OctetList = au1OctetStr;
    MEMCPY (tOctetStr.pu1_OctetList, pu1HostName, tOctetStr.i4_Length);

    if (SNMP_FAILURE ==
        nmhSetPppExtLinkConfigHostName (i4PppIfaceNo, &tOctetStr))
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}


INT4
PppCliServiceNameCfg (tCliHandle CliHandle, UINT1 *pu1ServiceName, UINT4 u4Action)
{
    tSNMP_OCTET_STRING_TYPE tOctetStr;
    UINT1               au1OctetStr[PPPOE_MAX_SERVICE_NAME_LEN];
    INT4                i4ValPPPoEConfigServiceRowStatus = 0;
    /* Set the Host Name */
    tOctetStr.i4_Length = (INT4) STRLEN (pu1ServiceName);
    MEMSET (au1OctetStr, 0, PPPOE_MAX_SERVICE_NAME_LEN);
    tOctetStr.pu1_OctetList = au1OctetStr;
    MEMCPY (tOctetStr.pu1_OctetList, pu1ServiceName, tOctetStr.i4_Length);

    if(u4Action == CLI_PPP_SERVICE_CREATE)
    {
        i4ValPPPoEConfigServiceRowStatus = CREATE_AND_GO;
    }
    else if(u4Action == CLI_PPP_SERVICE_DELETE)
    {
        i4ValPPPoEConfigServiceRowStatus = DESTROY; 
    }
    if (SNMP_FAILURE ==
            nmhSetPPPoEConfigServiceRowStatus(&tOctetStr,
            i4ValPPPoEConfigServiceRowStatus)) 
    {
        return CLI_FAILURE;
    }
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

INT4 PppCliSetPadrRetryCount(tCliHandle CliHandle,INT4 i4MaxRetryVal)
{
    UINT4 pu4ErrorCode;
    if(nmhTestv2PadrMaxRetryCount(&pu4ErrorCode,i4MaxRetryVal) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Invalid PADR retry value!\r\n");
        return CLI_FAILURE;
    }
    nmhSetPppPadrMaxRetryCount(i4MaxRetryVal);
    return CLI_SUCCESS;
}


/*****************************************************************************/
/*****************************************************************************/
INT4
CreateL2tpPppInterface (UINT1 *pu1UserName, UINT1 *pu1Password,
                        UINT4 u4AuthType, UINT4 u4IdleTimeOut,
                        UINT4 u4KeepAlive, UINT4 u4ConnectType,
                        INT4 *pi4PppIfIndex)
{
    tPPPIf             *pPppIf = NULL;
    t_SECRET_DEPENDENCY SecretsDependency;
    tSNMP_OCTET_STRING_TYPE Alias;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    INT4                i4PppIfaceNo = 0;
    INT4                i4SecretIdIndex = 1;
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (u4IdleTimeOut);
    UINT1               au1AliasStr[STRLEN (MSAD_WAN_PPP_INT_NAME)];
    UINT1               au1OctetStr[PPP_USRNAME_LEN + 1];
    MEMSET (&SecretsDependency, 0, sizeof (t_SECRET_DEPENDENCY));
    /* Get Free Index */
    if (CfaGetFreeInterfaceIndex ((UINT4 *) &i4PppIfaceNo, CFA_PPP) !=
        OSIX_SUCCESS)
    {
        mmi_printf ("Cannot Get Free Interface Index for PPP\n");
        return CLI_ERROR;
    }

    if (nmhSetIfMainRowStatus (i4PppIfaceNo, CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        mmi_printf ("Cannot create PPP interface!\n");
        return CLI_ERROR;
    }

    if (nmhSetIfMainType (i4PppIfaceNo, CFA_PPP) == SNMP_FAILURE)
    {
        mmi_printf ("Cannot Set Interface Type as PPP\n");
        nmhSetIfMainRowStatus (i4PppIfaceNo, DESTROY);
        return CLI_ERROR;
    }

    Alias.i4_Length = (INT4) STRLEN (MSAD_WAN_PPP_INT_NAME);

    MEMSET (au1AliasStr, 0, STRLEN (MSAD_WAN_PPP_INT_NAME));
    Alias.pu1_OctetList = au1AliasStr;
    if (Alias.pu1_OctetList != NULL)
    {
        MEMCPY (Alias.pu1_OctetList, MSAD_WAN_PPP_INT_NAME, Alias.i4_Length);
    }
    if (nmhSetIfAlias (i4PppIfaceNo, &Alias) == SNMP_FAILURE)
    {
        mmi_printf ("Cannot Set Alias Name for PPP Interface\n");
        nmhSetIfMainRowStatus (i4PppIfaceNo, DESTROY);
        return CLI_ERROR;
    }

    if (nmhSetIfIpAddrAllocMethod (i4PppIfaceNo, CFA_IP_ALLOC_NEGO) ==
        SNMP_FAILURE)
    {
        mmi_printf ("Cannot set PPP Interface IP Address Type \n");
        nmhSetIfMainRowStatus (i4PppIfaceNo, DESTROY);
        return CLI_ERROR;
    }
    if (nmhSetIfMainRowStatus (i4PppIfaceNo, ACTIVE) == SNMP_FAILURE)
    {
        mmi_printf ("Cannot Bring PPP Interface Row Status Active\n");
        nmhSetIfMainRowStatus (i4PppIfaceNo, DESTROY);
        return CLI_ERROR;
    }
    CfaSetIfActiveStatus ((UINT2) i4PppIfaceNo, CFA_TRUE);
    if (nmhSetPppExtLinkConfigLowerIfType (i4PppIfaceNo, CFA_ASYNC) ==
        SNMP_FAILURE)
    {
        mmi_printf ("Cannot Set Encaptulation Type as PPTP\n");
        nmhSetIfMainRowStatus (i4PppIfaceNo, DESTROY);
        return CLI_ERROR;
    }
    if (u4KeepAlive != 0)
    {
        if (nmhTestv2PppExtKeepAliveTimeOut (&u4ErrorCode, i4PppIfaceNo,
                                             (INT4) u4KeepAlive) ==
            SNMP_FAILURE)
        {
            mmi_printf
                ("ERROR: Cannot Set ppp keep-alive. Valid Range: 0 - 600\r\n");
            return CLI_ERROR;
        }

        if (nmhSetPppExtKeepAliveTimeOut (i4PppIfaceNo, (INT4) u4KeepAlive) ==
            0)
        {
            mmi_printf ("ERROR: Cannot Set ppp idle timeout\n");
            nmhSetIfMainRowStatus (i4PppIfaceNo, DESTROY);
            return CLI_ERROR;
        }
    }
    if (u4ConnectType == L2TP_ON_DEMAND)
    {
        if (nmhSetIfWanPersistence (i4PppIfaceNo, CFA_PERS_DEMAND) == 0)
        {
            mmi_printf
                ("ERROR: Cannot Set ppp interface for Wan Persistence\n");
            return CLI_ERROR;
        }
    }

    /* Enable Primary DNS Server address Negotiation */
    nmhSetPppExtIpLocPrimaryDNSAddressNegFlag (i4PppIfaceNo, PPP_ENABLE);
    /* Enable Secondary DNS Server address Negotiation */
    nmhSetPppExtIpLocSecondaryDNSAddressNegFlag (i4PppIfaceNo, PPP_ENABLE);

    if ((pu1UserName != NULL) && (pu1Password != NULL))
    {
        /* Set the parameters for PAP protocol */

        SecretsDependency.IdentityLen = (UINT1)MEM_MAX_BYTES(STRLEN (pu1UserName), MAX_SECRET_SIZE);
        STRNCPY (SecretsDependency.Identity, pu1UserName, SecretsDependency.IdentityLen);
        
        SecretsDependency.SecretLen =(UINT1) MEM_MAX_BYTES(STRLEN (pu1Password), MAX_SECRET_SIZE);
        STRNCPY (SecretsDependency.Secret,pu1Password,SecretsDependency.SecretLen);

        SecretsDependency.IfIndex = (UINT4) i4PppIfaceNo;

        SecretsDependency.Direction = PPP_LOCAL_TO_REMOTE;

        SecretsDependency.Status = VALID;
        SecretsDependency.StatusValid = (UINT1) 1;

        /* Set the parameters for CHAP protocol.For CHAP protocol set the 
         * username as the hostname and the host name should be set as user 
         * name in the security secrets table. */

        /* Set the Host Name */

        OctetStr.i4_Length = (INT4) STRLEN (pu1UserName);
        MEMSET (au1OctetStr, 0, (PPP_USRNAME_LEN + 1));
        OctetStr.pu1_OctetList = au1OctetStr;

        MEMCPY (OctetStr.pu1_OctetList, pu1UserName, OctetStr.i4_Length);

        nmhSetPppExtLinkConfigHostName (i4PppIfaceNo, &OctetStr);

        if (u4AuthType & L2TP_AUTH_MSCHAP_MASK)
        {
            SecretsDependency.IdIndex = (UINT4) i4SecretIdIndex;
            SecretsDependency.Protocol = MSCHAP_PROTOCOL;
            SNMPSetSecuritySecret ((UINT4) i4PppIfaceNo,
                                   (UINT4) i4SecretIdIndex, &SecretsDependency);
        }

        if (u4AuthType & L2TP_AUTH_CHAP_MASK)
        {
            i4SecretIdIndex++;
            SecretsDependency.IdIndex = (UINT4) i4SecretIdIndex;
            SecretsDependency.Protocol = CHAP_PROTOCOL;
            SNMPSetSecuritySecret ((UINT4) i4PppIfaceNo,
                                   (UINT4) i4SecretIdIndex, &SecretsDependency);
        }

        if (u4AuthType & L2TP_AUTH_PAP_MASK)
        {
            i4SecretIdIndex++;
            SecretsDependency.IdIndex = (UINT4) i4SecretIdIndex;
            SecretsDependency.Protocol = PAP_PROTOCOL;
            SNMPSetSecuritySecret ((UINT4) i4PppIfaceNo,
                                   (UINT4) i4SecretIdIndex, &SecretsDependency);
        }
    }

    gCfaWanInfo.u1AccessMethod = CFA_WAN_TYPE_L2TP;
    gCfaWanInfo.AccessMethod.L2tp.u4PppIfIndex = (UINT4) i4PppIfaceNo;
    gCfaWanInfo.AccessMethod.L2tp.u4PppIpAddr = 0;

    if (nmhSetIfAdminStatus (i4PppIfaceNo, CFA_IF_UP) == SNMP_FAILURE)
    {
        mmi_printf ("Cannot Make PPP interface Admin Status UP\n");
        nmhSetIfMainRowStatus (i4PppIfaceNo, CFA_RS_DESTROY);
        return CLI_ERROR;
    }

    if (pi4PppIfIndex)
    {
        *pi4PppIfIndex = i4PppIfaceNo;
    }

    if ((pPppIf = PppGetPppIfPtr ((UINT4) i4PppIfaceNo)) != NULL)
    {
        pPppIf->u1IsCreatedForL2tp = PPP_TRUE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : cli_set_access_method                              */
/*                                                                           */
/*     DESCRIPTION      : Sets Access method of the Router                   */
/*                                                                           */
/*     INPUT            : u1AccessMethod - the New access method             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS                                        */
/*                                                                           */
/*****************************************************************************/

INT4
cli_set_access_method (UINT1 u1AccessMethod)
{
    gu1AccessType = gu1WanType = u1AccessMethod;
#ifdef REVISIT
#ifdef NPAPI_WANTED
    FsNpSetAccessMethod (u1AccessMethod);
#endif
#endif
    return (CLI_SUCCESS);
}

INT4
cli_set_access_info (tWanInfo * pWanInfo)
{
    gu1AccessType = gu1WanType = pWanInfo->u1AccessMethod;
#ifdef REVISIT
#ifdef NPAPI_WANTED
    FsNpSetAccessInfo (pWanInfo);
#endif
#endif
    return (CLI_SUCCESS);
}

INT1
PppGetTestPppPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    if (!(pi1DispStr) || !(pi1ModeName))
    {
        return FALSE;
    }

    if (CLI_STRCMP (pi1ModeName, "TEST-PPP") != 0)
    {
        return FALSE;
    }

    CLI_STRCPY (pi1DispStr, "(config-test)#");
    return TRUE;
}

INT1
PppGetTestPppIntfPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Index;

    if ((pi1DispStr == NULL) || (pi1ModeName == NULL))
    {
        return FALSE;
    }

    if (STRCMP (pi1ModeName, "TEST-INT-PPP") != 0)
    {
        return FALSE;
    }

    u4Index = (UINT4) CLI_GET_IFINDEX ();

    SPRINTF ((CHR1 *) pi1DispStr, "(config-test-ppp-%d)#", u4Index);
    return TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssPPPShowDebugging                                */
/*                                                                           */
/*     DESCRIPTION      : This function prints the PPP debug level           */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

VOID
IssPPPShowDebugging (tCliHandle CliHandle)
{

    INT4                i4DbgLevel = 0;

    i4DbgLevel = (INT4) DesiredLevel;

    if (i4DbgLevel == 0)
    {
        return;
    }

    CliPrintf (CliHandle, "\rPPP :");

    if ((i4DbgLevel & INIT_SHUT_TRC) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  PPP Initialization/Shutdown debugging is on");
    }
    if ((i4DbgLevel & MGMT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  PPP Management debugging is on");
    }
    if ((i4DbgLevel & DATA_PATH_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  PPP data debugging is on");
    }
    if ((i4DbgLevel & CONTROL_PLANE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  PPP Control plane debugging is on");
    }
    if ((i4DbgLevel & DUMP_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  PPP dump debugging is on");
    }
    if ((i4DbgLevel & OS_RESOURCE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  PPP os debugging is on");
    }
    if ((i4DbgLevel & ALL_FAILURE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  PPP failure debugging is on");
    }
    if ((i4DbgLevel & BUFFER_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  PPP buffer debugging is on");
    }
    if ((i4DbgLevel & PPP_MUST) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  PPP Protocol state change debugging is on");
    }

    CliPrintf (CliHandle, "\r\n");
    return;
}

/* PPP SHOW RUNNING COFIG COMMAND */
/*****************************************************************************/
/*     FUNCTION NAME    : PppShowRunningConfig                               */
/*                                                                           */
/*     DESCRIPTION      : Displays PPP Configuration                         */
/*                                                                           */
/*     INPUT            : tCliHandle - Handle to the Cli Context             */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS / CLI_FAILURE                          */
/*****************************************************************************/
INT4
PppShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module)
{
    INT4                i4Index = 0;
    INT4                i4PrevIndex = 0;
    INT1                i1OutCome = 0;
    INT4                i4PPPoeMode = 0;
    INT4                i4PoolIndex = 0;
    UINT4               u4StartAddr = 0;
    UINT4		u4EndAddr = 0;
    CHR1                *pi1IpAddr;

    CliRegisterLock (CliHandle, PppLock, PppUnlock);
    PPP_LOCK ();

    if (u4Module == ISS_PPP_SHOW_RUNNING_CONFIG
        || u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
	nmhGetPPPoEMode(&i4PPPoeMode);
        if (i4PPPoeMode != PPPOE_DEF_MODE)
	{
	    if (i4PPPoeMode == PPPOE_SERVER)
	    {
    		 CliPrintf (CliHandle, "configure terminal\r\n");
	         CliPrintf (CliHandle, "pppoe mode server\r\n");
		 if (nmhGetFirstIndexPppExtIPAddressConfigTable (&i4PoolIndex) != SNMP_FAILURE)
		 {
		     do
		     {
	                 CliPrintf (CliHandle, "ip ppp address local-pool ");
			 CliPrintf (CliHandle, "%d ", i4PoolIndex);
			 nmhGetPppExtIPAddressPoolLowerRange (i4PoolIndex, &u4StartAddr);
        		 CLI_CONVERT_IPADDR_TO_STR (pi1IpAddr, u4StartAddr);
        		 CliPrintf (CliHandle, "%s ", pi1IpAddr);
			 nmhGetPppExtIPAddressPoolUpperRange (i4PoolIndex, &u4EndAddr);
        		 CLI_CONVERT_IPADDR_TO_STR (pi1IpAddr, u4EndAddr);
        		 CliPrintf (CliHandle, "%s\r\n", pi1IpAddr);
		     } 	
		     while (nmhGetNextIndexPppExtIPAddressConfigTable (i4PoolIndex, &i4PoolIndex) 
		            == SNMP_SUCCESS);
		 }
	    }
      }
	    if (i4PPPoeMode == PPPOE_CLIENT)
	    {
	        CliPrintf (CliHandle, "pppoe mode client\r\n");		
	    }
	  else
		CliPrintf (CliHandle, "end\r\n");
         
        i1OutCome = nmhGetFirstIndexPppExtIpTable (&i4Index);
        while (i1OutCome != SNMP_FAILURE)
        {
            PppShowRunningConfigIntf (CliHandle, i4Index);
            i4PrevIndex = i4Index;
            i1OutCome = nmhGetNextIndexPppExtIpTable (i4PrevIndex, &i4Index);
            CliPrintf (CliHandle, "! \r\n");
        }
    }

    PPP_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return CLI_SUCCESS;
}

/* PPP SHOW RUNNING COFIG COMMAND :*/
/*****************************************************************************/
/*     FUNCTION NAME    : PppShowRunningConfigIntf                           */
/*                                                                           */
/*     DESCRIPTION      : Displays PPP Interface Configuration               */
/*                                                                           */
/*     INPUT            : tCliHandle - Handle to the Cli Context             */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS / CLI_FAILURE                          */
/*****************************************************************************/
VOID
PppShowRunningConfigIntf (tCliHandle CliHandle, INT4 i4Index)
{
    INT4                i4PhyIndex = 0;
    INT1                ai1PhyIfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1                ai1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1                ai1AssocIfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1		u1IfType = 0;
    UINT4		u4HdlcPhyIndex = 0;
    UINT4 		u4IpAddr = 0;
    UINT4 		u4IpSubnetMask = 0;
    INT4 		i4IpAllocMethod = 0;
    UINT4 		u4IpAssocUnnumIndex = 0;
    UINT4 		u4IpAssocAddr = 0;
    UINT4 		u4IpAssocSubnetMask = 0;
    CHR1                *pi1IpAddr;
    INT1		i1RetVal = 0;

    MEMSET (ai1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (ai1AssocIfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    getPhysicalIndexOfPPP (i4Index, &i4PhyIndex);
    CfaCliConfGetIfName ((UINT4) i4PhyIndex, &ai1PhyIfName[0]);
    CfaCliConfGetIfName ((UINT4) i4Index, &ai1IfName[0]);
    CfaGetIfType ((UINT4) i4PhyIndex, &u1IfType);
    i1RetVal = nmhGetIfIpAddr (i4Index, &u4IpAddr);
    i1RetVal = nmhGetIfIpSubnetMask (i4Index, &u4IpSubnetMask);
    i1RetVal = nmhGetIfIpAddrAllocMethod (i4Index, &i4IpAllocMethod); 
    i1RetVal = nmhGetIfIpUnnumAssocIPIf (i4Index, &u4IpAssocUnnumIndex);

    if (i1RetVal == SNMP_SUCCESS)
    {
        i1RetVal = nmhGetIfIpAddr ((INT4) u4IpAssocUnnumIndex, &u4IpAssocAddr);
    	i1RetVal = nmhGetIfIpSubnetMask ((INT4) u4IpAssocUnnumIndex, &u4IpAssocSubnetMask);
    }
    CliPrintf (CliHandle, "configure terminal\r\n");
    if (u4IpAssocUnnumIndex != 0)
    {
	CfaCliConfGetIfName (u4IpAssocUnnumIndex, &ai1AssocIfName[0]);
       	CliPrintf (CliHandle, "interface %s\r\n", ai1AssocIfName);
	CliPrintf (CliHandle, "ip address "); 
        CLI_CONVERT_IPADDR_TO_STR (pi1IpAddr, u4IpAssocAddr);
        CliPrintf (CliHandle, "%s ", pi1IpAddr);
        CLI_CONVERT_IPADDR_TO_STR (pi1IpAddr, u4IpAssocSubnetMask);
        CliPrintf (CliHandle, "%s\r\n", pi1IpAddr);
       	CliPrintf (CliHandle, "no shutdown\r\n");
    	CliPrintf (CliHandle, "exit\r\n");
    }	
    if (u1IfType == CFA_HDLC)
    {
      	CliPrintf (CliHandle, "controller %d\r\n",atoi((CHR1 *) &ai1PhyIfName[7]));
   	CliPrintf (CliHandle, "channel-group %d\r\n",atoi((CHR1 *) &ai1PhyIfName[9]));
   	CliPrintf (CliHandle, "exit\r\n");
   	CliPrintf (CliHandle, "interface %s\r\n", ai1IfName);
       	CliPrintf (CliHandle, "shutdown\r\n");
   	CliPrintf (CliHandle, "layer %s\r\n", ai1PhyIfName);
    	if (u4IpAssocUnnumIndex != 0)
        {
   	    CliPrintf (CliHandle, "ip unnumbered loopback %d\r\n",atoi((CHR1 *) &ai1AssocIfName[9]));
	}
       	CliPrintf (CliHandle, "no shutdown\r\n");
       	CliPrintf (CliHandle, "exit\r\n");
   	if(CfaPppGetPhyIndexFromHdlcIndex((UINT4) i4PhyIndex, &u4HdlcPhyIndex) == CFA_SUCCESS)
   	{
            CliPrintf (CliHandle, "interface gigabitethernet 0/%d\r\n", u4HdlcPhyIndex);
   	    CliPrintf (CliHandle, "map switch default\r\n");
       	    CliPrintf (CliHandle, "no shutdown\r\n");
       	    CliPrintf (CliHandle, "exit\r\n");
   	}	 
    }
    else if (u1IfType == CFA_ENET)
    {
       	CliPrintf (CliHandle, "interface %s\r\n", ai1PhyIfName);
       	CliPrintf (CliHandle, "shutdown\r\n");
       	CliPrintf (CliHandle, "no switchport\r\n");
       	CliPrintf (CliHandle, "exit\r\n");
       	CliPrintf (CliHandle, "interface %s\r\n", ai1IfName);
       	CliPrintf (CliHandle, "shutdown\r\n");
       	CliPrintf (CliHandle, "layer %s\r\n", ai1PhyIfName);
    	if (u4IpAssocUnnumIndex != 0)
        {
   	    CliPrintf (CliHandle, "ip unnumbered loopback %d\r\n",atoi((CHR1 *) &ai1AssocIfName[9]));
	}
       	CliPrintf (CliHandle, "no shutdown\r\n");
       	CliPrintf (CliHandle, "exit\r\n");
        CliPrintf (CliHandle, "interface %s\r\n", ai1PhyIfName);
       	CliPrintf (CliHandle, "no shutdown\r\n");
    	CliPrintf (CliHandle, "exit\r\n");
    }
    if ((u4IpAddr != 0) && (u4IpSubnetMask != 0) && (i4IpAllocMethod == 1))
    {
       	CliPrintf (CliHandle, "interface %s\r\n", ai1IfName);
       	CliPrintf (CliHandle, "shutdown\r\n");
	CliPrintf (CliHandle, "ip address ");
        CLI_CONVERT_IPADDR_TO_STR (pi1IpAddr, u4IpAddr);
        CliPrintf (CliHandle, "%s ", pi1IpAddr);
        CLI_CONVERT_IPADDR_TO_STR (pi1IpAddr, u4IpSubnetMask);
        CliPrintf (CliHandle, "%s\r\n", pi1IpAddr);
       	CliPrintf (CliHandle, "no shutdown\r\n");
    	CliPrintf (CliHandle, "exit\r\n");
    }
    CliPrintf (CliHandle, "interface %s\r\n", ai1IfName);
    CliPrintf (CliHandle, "shutdown\r\n");
    if (nmhValidateIndexInstancePppExtIpTable (i4Index) == SNMP_SUCCESS)
    {
        PppLcpShowRunningConfig (CliHandle, i4Index);
        PppNcpShowRunningConfig (CliHandle, i4Index);
        PppSecurityShowRunningConfig (CliHandle, i4Index);
    }
    CliPrintf (CliHandle, "no shutdown\r\n");
    CliPrintf (CliHandle, "end\r\n");
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : PppLcpShowRunningConfig                            */
/*                                                                           */
/*     DESCRIPTION      : Displays PPP LCP Configuration                     */
/*                                                                           */
/*     INPUT            : tCliHandle - Handle to the Cli Context             */
/*                        i4IfIndex - Index of the ppp interface             */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/
VOID
PppLcpShowRunningConfig (tCliHandle CliHandle, INT4 i4IfIndex)
{
    INT4                i4KeepAliveTimeOutVal = 0;
    INT4                i4VlanID = 0;
    INT4                i4CoS = 0;
    INT4                i4CFI = 0;
    UINT1               u1Array[PPP_MAX_NAME_LENGTH];
    tSNMP_OCTET_STRING_TYPE tOctetStr;

    tOctetStr.pu1_OctetList = &u1Array[0];
    tOctetStr.i4_Length = 0;
    MEMSET (&u1Array, 0, PPP_MAX_NAME_LENGTH);

    nmhGetPppExtKeepAliveTimeOut (i4IfIndex, &i4KeepAliveTimeOutVal);
    if ((i4KeepAliveTimeOutVal != PPP_DEF_KEEPALIVE_TIME) &&
        (i4KeepAliveTimeOutVal != 0))
    {
        CliPrintf (CliHandle,
                   "keep-alive timeout %d\r\n", i4KeepAliveTimeOutVal);
    }
    if (i4KeepAliveTimeOutVal == 0)
    {
        CliPrintf (CliHandle, "no keep-alive timeout \r\n");
    }
    nmhGetPppExtLinkConfigHostName (i4IfIndex, &tOctetStr);
    if (tOctetStr.i4_Length != 0)
    {
        CliPrintf (CliHandle,
                   "ppp chap hostname %s\r\n", tOctetStr.pu1_OctetList);
    }

    nmhGetPPPoEVlanID (i4IfIndex, &i4VlanID);
    if (i4VlanID >= PPPOE_MIN_ENCAPSULATION_VLAN_ID &&
        i4VlanID <= PPPOE_MAX_ENCAPSULATION_VLAN_ID)
    {
        CliPrintf (CliHandle, "dot1q encapsulation vlan %d\r\n", i4VlanID);
    }
    else if (i4VlanID == 0)
    {
        CliPrintf (CliHandle, "no dot1q encapsulation vlan\r\n");
    }

    nmhGetPPPoECoS (i4IfIndex, &i4CoS);
    if (i4CoS >= PPPOE_MIN_ENCAPSULATION_COS &&
        i4CoS <= PPPOE_MAX_ENCAPSULATION_COS)
    {
        CliPrintf (CliHandle, "dot1q encapsulation priority %d\r\n", i4CoS);
    }
    else if (i4CoS == 0)
    {
        CliPrintf (CliHandle, "no dot1q encapsulation priority\r\n");
    }

    nmhGetPPPoECFI (i4IfIndex, &i4CFI);
    if (i4CFI == PPPOE_ENABLE_ENCAPSULATION_CFI)
    {
        CliPrintf (CliHandle, "dot1q encapsuation cfi %d\r\n", i4CFI);
    }
    else if (i4CFI == PPPOE_DISABLE_ENCAPSULATION_CFI)
    {
        CliPrintf (CliHandle, "no dot1q encapsulation cfi\r\n");
    }

    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : PppNcpShowRunningConfig                            */
/*                                                                           */
/*     DESCRIPTION      : Displays PPP NCP Configuration                     */
/*                                                                           */
/*     INPUT            : tCliHandle - Handle to the Cli Context             */
/*                        i4Index - Index of the ppp interface               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/
VOID
PppNcpShowRunningConfig (tCliHandle CliHandle, INT4 i4Index)
{
    UINT4               u4IpAddress = 0;
    UINT4               u4PrimaryDNSIpAddress = 0;
    UINT4               u4SecondaryDNSIpAddress = 0;
    CHR1               *pString = NULL;

    nmhGetPppExtIpRemoteToLocAddress (i4Index, &u4IpAddress);
    nmhGetPppExtIpRemoteToLocPrimaryDNSAddress (i4Index,
                                                &u4PrimaryDNSIpAddress);
    nmhGetPppExtIpRemoteToLocSecondaryDNSAddress (i4Index,
                                                  &u4SecondaryDNSIpAddress);

    if (u4IpAddress != 0 || u4PrimaryDNSIpAddress != 0
        || u4SecondaryDNSIpAddress != 0)
    {
        CliPrintf (CliHandle, "peer");
        if (u4IpAddress != 0)
        {
            CLI_CONVERT_IPADDR_TO_STR (pString, u4IpAddress);
            CliPrintf (CliHandle, " ip address %s\r\n", pString);
        }
        if (u4PrimaryDNSIpAddress != 0)
        {
            CLI_CONVERT_IPADDR_TO_STR (pString, u4PrimaryDNSIpAddress);
            CliPrintf (CliHandle, "Primary DNS-address %s\r\n", pString);
        }
        if (u4SecondaryDNSIpAddress != 0)
        {
            CLI_CONVERT_IPADDR_TO_STR (pString, u4SecondaryDNSIpAddress);
            CliPrintf (CliHandle, "Secondary DNS-address %s \r\n", pString);
        }
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : PppSecurityShowRunningConfig                       */
/*                                                                           */
/*     DESCRIPTION      : Displays PPP Security Table Configuration          */
/*                                                                           */
/*     INPUT            : tCliHandle - Handle to the Cli Context             */
/*                        i4Index - Index of the ppp interface               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/
VOID
PppSecurityShowRunningConfig (tCliHandle CliHandle, INT4 i4Index)
{
    INT4                i4LinkIndex = 0;
    INT4                i4IdIndex = 0;
    INT4                i4PrevIdIndex = 0;
    INT4                i4Direction = 0;
    INT4                i4ResType = 0;
    INT1                i1OutCome = 0;
    UINT1               u1Array1[MAX_SECRETS_IDENTITY_LENGTH];
    UINT1               u1Array2[MAX_SECRETS_IDENTITY_LENGTH];
    tSNMP_OCTET_STRING_TYPE tOctetStr1;
    tSNMP_OCTET_STRING_TYPE tOctetStr2;

    tOctetStr1.pu1_OctetList = &u1Array1[0];
    tOctetStr2.pu1_OctetList = &u1Array2[0];
    tOctetStr1.i4_Length = 0;
    tOctetStr2.i4_Length = 0;

    MEMSET (&u1Array1, 0, MAX_SECRETS_IDENTITY_LENGTH);
    MEMSET (&u1Array2, 0, MAX_SECRETS_IDENTITY_LENGTH);

    i1OutCome = nmhGetNextIndexPppSecuritySecretsTable (i4Index,
                                                        &i4LinkIndex, 0,
                                                        &i4IdIndex);
    /* Get Restoration Type - whether CSR or MSR */
    i4ResType = IssGetRestoreTypeFromNvRam ();

    while ((i1OutCome != SNMP_FAILURE) && (i4LinkIndex == i4Index))
    {
        nmhGetPppSecuritySecretsDirection (i4LinkIndex, i4IdIndex,
                                           &i4Direction);
        nmhGetPppSecuritySecretsIdentity (i4LinkIndex, i4IdIndex, &tOctetStr1);
        nmhGetPppSecuritySecretsSecret (i4LinkIndex, i4IdIndex, &tOctetStr2);
        if ((tOctetStr1.i4_Length != 0) && (tOctetStr2.i4_Length != 0))
        {
            if ((i4ResType == SET_FLAG) && (MsrGetSaveStatus () == ISS_TRUE))
			{
				if (i4Direction == PPP_LOCAL_TO_REMOTE)
				{
					CliPrintf (CliHandle,
							"ppp username %s password %s\r\n", tOctetStr1.pu1_OctetList, 
							tOctetStr2.pu1_OctetList);
					MEMSET (&u1Array1, 0, MAX_SECRETS_IDENTITY_LENGTH);
					MEMSET (&u1Array2, 0, MAX_SECRETS_IDENTITY_LENGTH);
				}
				if (i4Direction == PPP_REMOTE_TO_LOCAL)
				{
					CliPrintf (CliHandle,
							"ppp authenticate username %s password %s\r\n",
							tOctetStr1.pu1_OctetList,
							tOctetStr2.pu1_OctetList);
					MEMSET (&u1Array1, 0, MAX_SECRETS_IDENTITY_LENGTH);
					MEMSET (&u1Array2, 0, MAX_SECRETS_IDENTITY_LENGTH);
				}
			}
            else
			{
            if (i4Direction == PPP_LOCAL_TO_REMOTE)
            {
                CliPrintf (CliHandle,
                           "ppp username %s\r\n", tOctetStr1.pu1_OctetList);
                MEMSET (&u1Array1, 0, MAX_SECRETS_IDENTITY_LENGTH);
                MEMSET (&u1Array2, 0, MAX_SECRETS_IDENTITY_LENGTH);
            }

            if (i4Direction == PPP_REMOTE_TO_LOCAL)
            {
                CliPrintf (CliHandle,
                           "ppp authenticate username %s\r\n",
                           tOctetStr1.pu1_OctetList);
                MEMSET (&u1Array1, 0, MAX_SECRETS_IDENTITY_LENGTH);
                MEMSET (&u1Array2, 0, MAX_SECRETS_IDENTITY_LENGTH);
            }
			}

        }
        i4IdIndex++;
        i4PrevIdIndex = i4IdIndex;
        i1OutCome = nmhGetNextIndexPppSecuritySecretsTable (i4LinkIndex,
                                                            &i4LinkIndex,
                                                            i4PrevIdIndex,
                                                            &i4IdIndex);
    }
    return;
}


#endif /*__PPPCLI_C__*/
