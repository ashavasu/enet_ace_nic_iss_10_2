/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppphli.c,v 1.9 2014/11/23 09:35:23 siva Exp $
 *
 * Description:This file contains procedures that interfaces with
 *             higher layer
 *
 *******************************************************************/

#include     "pppcom.h"
#include     "genexts.h"
#include     "gsemdefs.h"
#include     "pppchap.h"
#include     "ppppap.h"
#include     "pppeap.h"
#include     "pppauth.h"
#include     "pppmp.h"
#include     "mpexts.h"
#include     "mpdefs.h"
#include     "globexts.h"
#include     "pppproto.h"
#include     "pppipcp.h"
#include     "ipexts.h"
#include     "pppipv6cp.h"
#include     "ipv6exts.h"
#include     "pppmuxcp.h"
#include     "pppmplscp.h"
#include     "mplsexts.h"
#include     "pppipxcp.h"
#include     "ipxexts.h"
#include     "pppbcp.h"
#include     "bcpexts.h"
#ifdef BAP
#include     "pppbacp.h"
#include     "bacpexts.h"
#include     "pppbap.h"
#include     "bapprot.h"
#endif
#include    "pppdpproto.h"
#ifdef BRIDGE_WANTED
#include    "bridge.h"
#endif
#include "genproto.h"

extern UINT2        VJCompressPkt (t_MSG_DESC ** pBuffer, tPPPIf * pIf);
extern t_MSG_DESC  *VJUncompressPkt (t_MSG_DESC * pBuffer, tPPPIf * pIf,
                                     UINT2 *Type, UINT4);
/*********************************************************************
 *                 Procedures called by other subsystems                  * 
*********************************************************************/

/*********************************************************************
*    Function Name    :    PPPRxPktFromHL
*    Description        :
*        This procedure sends the higher layer data packet over the PPP
*  link after encapsulating. It is called by the HLI to send data to PPP.
*    Parameter(s)    :
*            pIf        -    Points to the Interface structure.
*            Length    -    Length of the PDU.
*             pPDU    -    Pointer to the PDU buffer.
*       Protocol    -    Protocol of the packet.
*    Return Values    :    VOID
*********************************************************************/
VOID
PPPRxPktFromHL (tPPPIf * pIf, t_MSG_DESC * pPDU, UINT2 Length, UINT2 Protocol)
{
    tGSEM              *pGSEM;

    PPP_UNUSED (Length);

    if (pIf == NULL)
    {
        if (pPDU != NULL)
            RELEASE_HL_BUFFER (pPDU);
        return;
    }

    PPP_PKT_DUMP (DUMP, pPDU, Length, "Pkt From HL:");
    /* Copy the protocol value into module data for
     * processing by other modules */
    PPP_PROT_TYPE (pPDU) = (UINT4) Protocol;
    pGSEM = NULL;
    OutReleaseFlag = PPP_NO;
    if ((OutDataPktIndex = GCPGetProtIndex (Protocol)) != NOT_FOUND)
    {
        if ((pGSEM = ((*ProtSEMTable[OutDataPktIndex].pGetSEMPtr) (pIf))) !=
            NULL)
        {

            /* For IP packets compress , if VJ is negotiated */
            if ((Protocol == IP_DATAGRAM) &&
                (pIf->pVjComp != NULL) &&
                (pGSEM->pNegFlagsPerIf[IP_COMP_IDX].FlagMask
                 & ACKED_BY_LOCAL_MASK))
            {
                Protocol = VJCompressPkt (&pPDU, pIf);
                Length = (UINT2) VALID_BYTES (pPDU);
            }

            pGSEM->pOutParam = pPDU;
            pGSEM->MiscParam.ShortValue = Protocol;

            GSEMRun (pGSEM, TXD);
            pGSEM->pOutParam = NULL;

        }
        else
        {
            DISCARD_PKT (pIf, "Error : pGSEM is NULL ");
        }
    }
    else
    {
        DISCARD_PKT (pIf, "Error : Unsupported protocol pkt ");
    }
    OutDataPktIndex = 0;
    if ((OutReleaseFlag == PPP_YES) && (pPDU != NULL))
    {
        RELEASE_HL_BUFFER (pPDU);

    }

    return;
}

/*********************************************************************
*    Function Name    :    PPPHLIRxPkt
*    Description        :
*            This procedure sends the data packet to the higher layer. This
*    procrdure is called by the PPP module to pass on data to the HL modules.    
*    Parameter(s)    :
*                pIf    -    Points to the Interface structure.
*            Length    -    Length of the PDU.
*            pPDU    -    Pointer to the PDU buffer.
*    Return Values    :    VOID
*********************************************************************/
VOID
PPPHLIRxPkt (tPPPIf * pIf, t_MSG_DESC * pPDU, UINT2 Length, UINT2 Protocol)
{
#ifdef BAP
    if (Protocol == BAP_PROTOCOL)
    {
        BAPInput (pIf, pPDU, Length);
        return;
    }
#endif

    /* Uncompress the packets - VJ Compression */
    if ((Protocol == IP_COMP_TCP_DATA) || (Protocol == IP_UNCOMP_TCP_DATA))
    {

        tIPCPIf            *pIpcpIf;
        pIpcpIf = (tIPCPIf *) pIf->pIpcpPtr;
        if ((pIf->pVjComp != NULL)
            && (pIpcpIf->IpcpGSEM.pNegFlagsPerIf[IP_COMP_IDX].
                FlagMask & ACKED_BY_PEER_MASK))
        {
            pPDU =
                (t_MSG_DESC *) VJUncompressPkt (pPDU, pIf, &Protocol, Length);
            if (pPDU == NULL)
                return;
            Length = (UINT2) VALID_BYTES (pPDU);
        }
        else
        {
            ReleaseFlag = PPP_YES;
            return;
        }
    }

    /* send the packet to higher layer */
    PPPTxPktToHL (pIf, pPDU, Length, Protocol);
    return;

}

/*********************************************************************
*  Function Name    :    PPPHLINotifyProtStatusToHL
*  Description        :
*              This function is used to indicate the status of the higher layer
*    control protocols over an interface.
*    Parameter(s)    :
*           pIf -    points to Interface structure for which this message 
            is intended for.
*     Status   -  Operational Status of the PPP protocol
*     Protocol -  indicates whether the status is for IPXCP/IPCP.
*  Return Values    :    VOID
*********************************************************************/
VOID
PPPHLINotifyProtStatusToHL (tPPPIf * pIf, UINT2 Protocol, UINT1 Status)
{
    /* here the function provided by higher layer to store the handle given by
       ppp system is called. so that when HL wants to send data to ppp , that
       stored  handled can be used.- for porting */

    UINT4               u4LinkMtu;
    UINT4               u4CurMtu;
#ifdef PPTP_WANTED
    INT1                i1PptpMode = 0;
#endif
#ifdef CFA_WANTED
    tIpInterfaceParams  IpParams;
    tIpConfigInfo       IpIfInfo;

    MEMSET (&IpParams, 0, sizeof (tIpInterfaceParams));;
    MEMSET (&IpIfInfo, 0, sizeof (IpIfInfo));
#endif

    PPP_UNUSED (Status);
    PPP_TRC (CONTROL_PLANE, "----------------------------------------");

    if (pIf->BundleFlag == BUNDLE)
    {
        /* While giving MRU value to HL(IP), the minimum among the 
         * following should be given
         * OptionsAckedByLocal, OptionsAckedByPeer.
         * Since MRRU is always negotiated AllowedForPeer need not
         * be considered */
        if (pIf->MPInfo.BundleInfo.BundleOptAckedByLocal.MRRU
            < pIf->MPInfo.BundleInfo.BundleOptAckedByPeer.MRRU)
        {
            u4LinkMtu = pIf->MPInfo.BundleInfo.BundleOptAckedByLocal.MRRU;
        }
        else
        {
            u4LinkMtu = pIf->MPInfo.BundleInfo.BundleOptAckedByPeer.MRRU;
        }
    }
    else
    {
        /* While giving MRU value to HL(IP), the minimum among the 
         * following should be given
         * LCPOptionsAckedByLocal, LCPOptionsAckedByPeer
         * LCPOptionsAllowedForPeer */
        CfaGetIfMtu(pIf->LinkInfo.IfIndex,&u4CurMtu);
        if(u4CurMtu != 0)
        {
           u4LinkMtu = u4CurMtu;      
        }
        else if (pIf->LcpOptionsAckedByLocal.MRU < pIf->LcpOptionsAckedByPeer.MRU)
        {
            u4LinkMtu = (pIf->LcpOptionsAllowedForPeer.MRU <
                         pIf->LcpOptionsAckedByLocal.MRU) ?
                pIf->LcpOptionsAllowedForPeer.MRU :
                pIf->LcpOptionsAckedByLocal.MRU;
        }
        else if (pIf->LcpOptionsAckedByLocal.MRU > pIf->LcpOptionsAckedByPeer.MRU)
        {

            u4LinkMtu = pIf->LcpOptionsAckedByLocal.MRU;
        }
        else 
        {
            u4LinkMtu = (pIf->LcpOptionsAllowedForPeer.MRU <
                         pIf->LcpOptionsAckedByPeer.MRU) ?
                pIf->LcpOptionsAllowedForPeer.MRU :
                pIf->LcpOptionsAckedByPeer.MRU;
        }
    }

    switch (Protocol)
    {
#ifdef CFA_WANTED
            /* Porting Issue. Higher Layer or Control Module  Functions 
             * Should be Called here with required  parameters */

            tIPCPIf            *pIpcpIf;
            tBCPIf             *pBCPIf;
            tBcpConfigStruct    PppBcpConfigUpdate;

            /* for LCP */
        case LCP_PROTOCOL:

            if (Status == PPP_UP)
            {
#ifdef DPIF
                if (PppLCPUpToDp (pIf) == PPP_FAILURE)
                {
                    PPP_TRC (ALL_FAILURE, "LCPUp indication to DP failed");
                }
#endif

                PPP_TRC (CONTROL_PLANE, " LCP UP\n");

                if (pIf->MPInfo.MemberInfo.pBundlePtr != NULL)
                {
                    IpParams.u4StatusFlag = CFA_MEMBER_LINK_UP;
                    IpParams.u4Mtu = u4LinkMtu;
                    CfaInterfaceConfigIpParams (pIf->LinkInfo.IfIndex,
                                                IpParams);

                    CfaInterfaceStatusChangeIndication ((UINT2)
                                                        (pIf->LinkInfo.IfIndex),
                                                        STATUS_UP);
                }
            }
            else
            {
                if (Status == PPP_DOWN)
                {
                    PPP_TRC (CONTROL_PLANE, " LCP DOWN\n");

                    if (pIf->MPInfo.MemberInfo.pBundlePtr != NULL)
                    {
#ifdef DPIF
                        if (PppCPDownToDp (pIf, Protocol) == PPP_FAILURE)
                        {
                            PPP_TRC (ALL_FAILURE,
                                     "CP Down indication to DP failed");
                        }
#endif
                        IpParams.u4StatusFlag = CFA_MEMBER_LINK_DOWN;
                        IpParams.u4Mtu = 0;
                        CfaInterfaceConfigIpParams (pIf->LinkInfo.IfIndex,
                                                    IpParams);

                        CfaInterfaceStatusChangeIndication ((UINT2)
                                                            (pIf->LinkInfo.
                                                             IfIndex),
                                                            STATUS_DOWN);
                    }
                }
            }
            break;                /* LCP */

        case IPCP_PROTOCOL:

            pIpcpIf = (tIPCPIf *) pIf->pIpcpPtr;
            MEMSET (&IpParams, 0, sizeof (tIpInterfaceParams));

            if (Status == PPP_UP)
            {
                PPP_TRC (CONTROL_PLANE, " IPCP UP\n");

                IpParams.u4StatusFlag = CFA_IP_ACQUIRED;
                /* 
                 * Next Hop should be set to ZERO,
                 * so that Net link socket will add an interface specific
                 * default route, rather than adding a gateway(IP)
                 * specific route. This was an issue with Airtel ISP,
                 * where ISP Provided us an IP 40.0.0.1 and Peer IP
                 * as 80.0.0.1.
                 */
                if (pIpcpIf->IpcpOptionsAckedByLocal.IPAddress != 0)
                {
                    PPP_TRC (ALL_FAILURE, "Default route found");
                }
                else
                {
                    PPP_TRC (ALL_FAILURE, "Default route not found");
                }

                IpParams.u4Mtu = u4LinkMtu;
                IpParams.u4IpAddress =
                    pIpcpIf->IpcpOptionsAckedByPeer.IPAddress;
		if (CfaIpIfGetIfInfo (pIf->LinkInfo.IfIndex, &IpIfInfo) == CFA_SUCCESS)
		{
                    IpParams.u4SubnetMask = IpIfInfo.u4NetMask;
		}
                IpParams.u4DNSPrimaryIp =
                    pIpcpIf->IpcpOptionsAckedByPeer.PrimaryDNSAddress;
                IpParams.u4DNSSecondaryIp =
                    pIpcpIf->IpcpOptionsAckedByPeer.SecondaryDNSAddress;

                CfaInterfaceConfigIpParams (pIf->LinkInfo.IfIndex, IpParams);
                /*
                 * We need to Bring Up the interface in order to 
                 * add route in Linux. If gateway interface is down
                 * Linux will not allow us to add route through it.
                 * CfaInterfaceConfigIpParams will internally add
                 * default route in case of ppp, so we are making
                 * interface status of ppp to up.
                 */
                CfaInterfaceStatusChangeIndication ((UINT2)
                                                    (pIf->LinkInfo.IfIndex),
                                                    STATUS_UP);
                /* Make the link status up so that route addition will pass */
                CfaSetIfActiveStatus (pIf->LinkInfo.IfIndex, CFA_TRUE);
                /* If this PPP interface is created for L2TP, then add a 
                 * host route to the peer */
                if (pIf->u1IsCreatedForL2tp == PPP_TRUE)
                {
                    CfaIpUpdateHostRoute
                        ((UINT2) pIf->LinkInfo.IfIndex,
                         pIpcpIf->IpcpOptionsAckedByLocal.IPAddress,
                         PPP_DEF_ROUTE_METRIC, CFA_NET_IF_NEW);
                }
#ifdef PPTP_WANTED
                i1PptpMode = PptpGetMode (pIf->LinkInfo.IfIndex);
#endif
#ifdef DPIF
                if (PppIPCPUpToDp (pIf) == PPP_FAILURE)
                {
                    PPP_TRC (ALL_FAILURE, "IPCPUp Indication to DP failed\n");
                }
#endif
            }                    /* STATUS == UP */
            else                /* Status is down */
            {
#ifdef DPIF
                if (PppCPDownToDp (pIf, Protocol) == PPP_FAILURE)
                {
                    PPP_TRC (ALL_FAILURE, "CP Down indication to DP failed");
                }
#endif
                PPP_TRC (CONTROL_PLANE, " IPCP DOWN\n");

                CfaInterfaceStatusChangeIndication ((UINT2)
                                                    (pIf->LinkInfo.IfIndex),
                                                    STATUS_DOWN);
                IpParams.u4StatusFlag = CFA_IP_RELEASED;
                CfaInterfaceConfigIpParams (pIf->LinkInfo.IfIndex, IpParams);
#ifdef PPTP_WANTED
                nmhSetFsIpRouteStatus (pIpcpIf->IpcpOptionsAckedByLocal.IPAddress, 0xffffffff, 0, 0x00000000, 3,    /*IPROUTE_TYPE_LOCAL */
                                       6 /*IPFWD_DESTROY */ );
                PptpDelRouteInDp (pIf->LinkInfo.IfIndex,
                                  pIpcpIf->IpcpOptionsAckedByLocal.IPAddress);
#endif
                if (pIf->u1IsCreatedForL2tp == PPP_TRUE)
                {
                    CfaIpUpdateHostRoute
                        ((UINT2) pIf->LinkInfo.IfIndex,
                         pIpcpIf->IpcpOptionsAckedByLocal.IPAddress,
                         PPP_DEF_ROUTE_METRIC, CFA_NET_IF_DEL);
                }

            }
            break;                /* IPCP */

/* IPv6CP Changes -- start */
        case IPV6CP_PROTOCOL:

            if (Status == PPP_UP)
            {
                PPP_TRC (PPP_MUST, " \t\tIP6CP State Changed to    UP\n");
#ifdef DPIF
                if (IPv6CPUpToDP (pIf) == PPP_FAILURE)
                {
                    PPP_TRC (ALL_FAILURE, "IPv6CPUp Indication to DP failed\n");
                }
#endif
            }                    /* STATUS == UP */
            else                /* Status is down */
            {
                PPP_TRC (PPP_MUST, " \t\tIP6CP State Changed to  DOWN\n");
            }
            break;                /* IP6CP */
/* IPv6CP Changes -- End */
        case MUXCP_PROTOCOL:
            if (Status == PPP_UP)
            {
                PPP_TRC (PPP_MUST, " \t\tMUXCP State Changed to    UP\n");
#ifdef DPIF
                if (MuxCPUpToDP (pIf) == PPP_FAILURE)
                {
                    PPP_TRC (ALL_FAILURE, "MuxCPUp Indication to DP failed\n");
                }
#endif
            }                    /* STATUS == PPP_UP */
            else                /* Status is down */
            {
                PPP_TRC (PPP_MUST, " \t\tMuxCP State Changed to  DOWN\n");
            }
            break;

        case MPLSCP_PROTOCOL:
            if (Status == PPP_UP)
            {
                PPP_TRC (PPP_MUST, " \t\tMPLSCP State Changed to    UP\n");
#ifdef DPIF
                if (MPLSCPUpToDP (pIf) == PPP_FAILURE)
                {
                    PPP_TRC (ALL_FAILURE,
                             "MPLSCP UP Indication to DP failed\n");
                }
#endif
            }
            else                /* Status is down */
            {
                PPP_TRC (PPP_MUST, " \t\tMPLSCP State Changed to  DOWN\n");
            }

            break;
        case BACP_PROTOCOL:
            if (Status == PPP_UP)
            {
                PPP_TRC (PPP_MUST, " \t\tBACP State Changed to    UP\n");
            }
            else                /* Status is down */
            {
                PPP_TRC (PPP_MUST, " \t\tBACP State Changed to  DOWN\n");
            }

            break;

        case BCP_PROTOCOL:
            PppBcpConfigUpdate.u1TinygramFlag = TINYGRAM_FALSE;
            PppBcpConfigUpdate.u1IEEE802TagFrameSupport = OSIX_FALSE;
            PppBcpConfigUpdate.u1MgmtInlineSupport = OSIX_FALSE;

            pBCPIf = (tBCPIf *) pIf->pBcpPtr;

            if (Status == PPP_UP)
            {
                PPP_TRC (CONTROL_PLANE, " \t\tBCP  State Changed to    UP\n");

                if ((pBCPIf->BcpGSEM.pNegFlagsPerIf[TINYGRAM_IDX].FlagMask &
                     ACKED_BY_LOCAL_MASK)
                    && (pBCPIf->BcpGSEM.pNegFlagsPerIf[TINYGRAM_IDX].FlagMask &
                        ACKED_BY_PEER_MASK))
                {
                    PppBcpConfigUpdate.u1TinygramFlag = TINYGRAM_TRUE;

                }
                /* 2878 changes */
                if (pBCPIf->BcpGSEM.pNegFlagsPerIf[IEEE_802_TAGGED_FRAME_IDX].
                    FlagMask & ACKED_BY_PEER_MASK)
                {
                    PppBcpConfigUpdate.u1IEEE802TagFrameSupport = OSIX_TRUE;
                }
                if (pBCPIf->BcpGSEM.pNegFlagsPerIf[MANAGEMENT_INLINE_IDX].
                    FlagMask & ACKED_BY_PEER_MASK)
                {
                    PppBcpConfigUpdate.u1MgmtInlineSupport = OSIX_TRUE;
                }
                /* 2878 changes */
                PppBcpConfigUpdate.Stp =
                    pBCPIf->BcpOptionsAckedByPeer.SpanningTreeProto;
                PppBcpConfigUpdate.u1BridgeLineId =
                    (UINT1) (pBCPIf->BcpOptionsAckedByLocal.BridgeLineId);
                PppBcpConfigUpdate.u4Index = pIf->LinkInfo.IfIndex;
                MEMCPY (&PppBcpConfigUpdate.u1LocalMediaAddr,
                        &pBCPIf->BcpOptionsAckedByPeer.MACAddress,
                        sizeof (PppBcpConfigUpdate.u1LocalMediaAddr));;

                MEMCPY (&PppBcpConfigUpdate.u1PeerMediaAddr,
                        &pBCPIf->BcpOptionsAckedByLocal.MACAddress,
                        sizeof (PppBcpConfigUpdate.u1PeerMediaAddr));

                PppBcpConfigUpdate.u4Mtu = u4LinkMtu;
                PppBcpConfigUpdate.u4IdleTimeout =
                    (UINT4) (3 * pIf->EchoInfo.KeepAliveTimeOutVal);
#ifdef BRIDGE_WANTED
                CfaIfmHandleBcpConfigUpdate (PppBcpConfigUpdate);
#endif
                CfaInterfaceStatusChangeIndication ((UINT2)
                                                    (pIf->LinkInfo.IfIndex),
                                                    STATUS_UP);

            }                    /* STATUS == UP */
            else                /* Status is down */
            {
                CfaInterfaceStatusChangeIndication ((UINT2)
                                                    (pIf->LinkInfo.IfIndex),
                                                    STATUS_DOWN);

                PPP_TRC (CONTROL_PLANE, " \t\tBCP  State Changed to  DOWN\n");
            }
            break;

            /* Other Protocol changes can be informed to the higher Layer
             * under other switch case's
             * */
        default:
            break;                /* DO NOTHING */
#endif /* CFA_WANTED */
    }
    PPP_TRC (CONTROL_PLANE, "----------------------------------------\n");

}

/*********************************************************************
*    Function Name    :    PPPHLINotifyIfStatusToHL
*    Description        :
*            This function is used to pass the interface status to the
*    HL modules. 
*    Parameter(s)    :
*                 pIf    -    Points to the interface structure.
*           Status    -    Status of the PPP interface.
*    Return Values    :    VOID
*********************************************************************/
VOID
PPPHLINotifyIfStatusToHL (tPPPIf * pIf, UINT1 Status)
{
    PPP_TRC2 (CONTROL_PLANE, "PPPIf = %p, Status = %d", pIf, Status);
}

/*  Changes done to implement RFC 1877 - PPP Internet Protocol
 * Control Protocol Extensions for Name Server Addresses */
/*********************************************************************
*    Function Name    :    PPPHLIDNSRelayUpdateNS
*    Description      :
*            This function is used to update the primary and secondary 
*            DNS adresses obtained from PPP peer during IPCP options
*            negotiation. 
*    Parameter(s)     :
*           IfIndex               -    Interface index of PPP.
*           u4PrimaryDNSAddress   -    Primary DNS address
*           u4SecondaryDNSAddress -    Secondary DNS address
*    Return Values    :    VOID
*********************************************************************/
VOID
PPPHLIDNSRelayUpdateNS (UINT2 IfIndex,
                        UINT4 u4PrimaryDNSAddress, UINT4 u4SecondaryDNSAddress)
{
#ifdef DNS_RELAY_WANTED

    UINT1               au1AliasName[16];

    /* Get the alias name and check if it is a "ppp0" interface */
    if (CfaGetInterfaceNameFromIndex (IfIndex, au1AliasName) != OSIX_FAILURE)
    {
        if (STRCMP (au1AliasName, "ppp0") == 0)
        {
            DnsRelayUpdateNS (u4PrimaryDNSAddress, u4SecondaryDNSAddress,
                              "com");
        }
    }
#else
    UNUSED_PARAM (IfIndex);
    UNUSED_PARAM (u4PrimaryDNSAddress);
    UNUSED_PARAM (u4SecondaryDNSAddress);
#endif /* DNS_RELAY_WANTED */
}

/*  */
