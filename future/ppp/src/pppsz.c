#define _PPPSZ_C
#include "pppcom.h"
#include "pppmemmac.h"
extern INT4  IssSzRegisterModuleSizingParams( CHR1 *pu1ModName, tFsModSizingParams *pModSizingParams); 
extern INT4  IssSzRegisterModulePoolId( CHR1 *pu1ModName, tMemPoolId *pModPoolId); 
INT4  PppSizingMemCreateMemPools()
{
    INT4 i4RetVal;
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < PPP_MAX_SIZING_ID; i4SizingId++) {
        i4RetVal = MemCreateMemPool( 
                          FsPPPSizingParams[i4SizingId].u4StructSize,
                          FsPPPSizingParams[i4SizingId].u4PreAllocatedUnits,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &(PPPMemPoolIds[ i4SizingId]));
        if( i4RetVal == (INT4) MEM_FAILURE) {
            PppSizingMemDeleteMemPools();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}


INT4   PppSzRegisterModuleSizingParams( CHR1 *pu1ModName)
{
      /* Copy the Module Name */ 
       IssSzRegisterModuleSizingParams( pu1ModName, FsPPPSizingParams); 
      IssSzRegisterModulePoolId( pu1ModName, PPPMemPoolIds); 
      return OSIX_SUCCESS; 
}


VOID  PppSizingMemDeleteMemPools()
{
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < PPP_MAX_SIZING_ID; i4SizingId++) {
        if(PPPMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool( PPPMemPoolIds[ i4SizingId] );
            PPPMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
