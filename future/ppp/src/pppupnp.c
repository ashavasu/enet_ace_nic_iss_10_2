/****************************************************************************
 *  FILE NAME   : pppupnp.c                                                 *
 *                                                                          *
 *  DESCRIPTION : This file abstracts the ppp related UPnP Actions.         *
 *                                                                          *
 *  $Id: pppupnp.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *                                                                          *
 *  Copyright 2009, Aricent Inc. All Rights Reserved                        *
 *                                                                          *
 ****************************************************************************/

#include "pppcom.h"
#include "arupnp.h"
#include "pppupnp.h"
#include "gsemdefs.h"
#include "globexts.h"
#include "pppchap.h"
#include "ppppap.h"
#include "nat.h"
#include "pppeap.h"
#include "pppchap.h"
#include "pppauth.h"
#include "pppstlow.h"

/****************************************************************************
 * Function Name : PppUPnPGetPPPAuthenticationProtocol                      *
 *                                                                          *
 * Description   : To Handle GetPPPAuthenticationProtocol Action which is   *
 *                 invoked from  Control Point                              *
 *                                                                          *
 * Input(s)      : pActReq - Pointer to Document of action request          *
 *                                                                          *
 * Output(s)     : None                                                     *
 *                                                                          *
 * Returns       : UPNP_E_SUCCESS / UPNP_SUCCESS/ UPNP_FAILURE/             *
 *                 UPNP_ACT_E_VENDOR_SPE                                    *
 ****************************************************************************/
INT4
PppUPnPGetPPPAuthenticationProtocol (tUPnPActionReq * pActReq)
{
    UINT1               au1Buf[MAX_OCTETSTRING_SIZE];
    VOID              **pActRes = pActReq->ppActionResp;

    if (UPnPUtlGetValForGetPPPAuthenticationProtocol (pActReq->pu1ServiceID,
                                                      au1Buf) == OSIX_FAILURE)
    {
        *(pActReq->pu4ErrCode) = UPNP_ACT_E_ACTION_FAILED;
        *(pActReq->ppActionResp) = NULL;
        return *(pActReq->pu4ErrCode);
    }

    *pActRes =
        (VOID *)
        UPnPArwrStackMakeActionResponse ("GetPPPAuthenticationProtocol",
                                         pActReq->pu1ServiceID, 1,
                                         "NewPPPAuthenticationProtocol",
                                         au1Buf);
    if (*pActRes == NULL)
    {
        UPNP_MOD_TRC (UPNP_FAIL_TRC,
                      "Failed in PppUPnPGetPPPAuthenticationProtocol:"
                      "Failed to Construct Action Response\r\n");
        *(pActReq->pu4ErrCode) = UPNP_ACT_E_ACTION_FAILED;
        *(pActReq->ppActionResp) = NULL;
        return *(pActReq->pu4ErrCode);
    }

    *(pActReq->pu4ErrCode) = UPNP_E_SUCCESS;
    return *(pActReq->pu4ErrCode);

}

/****************************************************************************
 * Function Name : PppUPnPConfigureConnection                               *
 *                                                                          *
 * Description   : To Handle ConfigureConnection Action which is            *
 *                 invoked from  Control Point                              *
 *                                                                          *
 * Input(s)      : pActReq - Pointer to Document of action request          *
 *                                                                          *
 * Output(s)     : None                                                     *
 *                                                                          *
 * Returns       : UPNP_E_SUCCESS / UPNP_SUCCESS/ UPNP_FAILURE/             *
 *                 UPNP_ACT_E_VENDOR_SPE                                    *
 ****************************************************************************/
INT4
PppUPnPConfigureConnection (tUPnPActionReq * pActReq)
{
    CHR1               *pu1NewUserName = NULL;
    CHR1               *pu1NewPassword = NULL;
    UINT4               u4ErrorCode = UPNP_ACT_E_ACTION_FAILED;
    UINT4               u4NumArgs = 0;
    VOID              **pActRes = pActReq->ppActionResp;

    pu1NewUserName = UPnPArwrStackGetFirstDocItem (pActReq->pActionRequest,
                                                   "NewUserName");
    u4NumArgs++;
    pu1NewPassword = UPnPArwrStackGetFirstDocItem (pActReq->pActionRequest,
                                                   "NewPassword");
    u4NumArgs++;
    if ((pu1NewUserName == NULL) || (pu1NewPassword == NULL))
    {
        UPnPUtlSetErrAndFreeStateVar (pActReq, UPNP_ACT_E_INVALID_ARGS,
                                      u4NumArgs, pu1NewUserName,
                                      pu1NewPassword);
        *(pActReq->ppActionResp) = NULL;
        return *(pActReq->pu4ErrCode);
    }
    if (UPnPUtlSetValForConfigureConnection (pActReq->pu1ServiceID,
                                             pu1NewUserName, pu1NewPassword,
                                             &u4ErrorCode) == OSIX_FAILURE)
    {
        UPnPUtlSetErrAndFreeStateVar (pActReq, u4ErrorCode, u4NumArgs,
                                      pu1NewUserName, pu1NewPassword);
        *(pActReq->ppActionResp) = NULL;
        return *(pActReq->pu4ErrCode);
    }
    *pActRes =
        (VOID *) UPnPArwrStackMakeActionResponse ("ConfigureConnection",
                                                  pActReq->pu1ServiceID, 0);
    if (*pActRes == NULL)
    {
        UPNP_MOD_TRC (UPNP_FAIL_TRC, "Failed in PppUPnPConfigureConnection:"
                      "Failed to Construct Action Response\r\n");
        *(pActReq->pu4ErrCode) = UPNP_ACT_E_ACTION_FAILED;
        *(pActReq->ppActionResp) = NULL;
        return *(pActReq->pu4ErrCode);
    }

    /* In Success case free the all local pointers */
    UPnPUtlSetErrAndFreeStateVar (pActReq, UPNP_E_SUCCESS, u4NumArgs,
                                  pu1NewUserName, pu1NewPassword);
    *(pActReq->pu4ErrCode) = UPNP_E_SUCCESS;
    return *(pActReq->pu4ErrCode);
}

/****************************************************************************
 * Function Name : PppUPnPGetUserName                                       *
 *                                                                          *
 * Description   : To Handle GetUserName Action which is                    *
 *                 invoked from  Control Point                              *
 *                                                                          *
 * Input(s)      : pActReq - Pointer to Document of action request          *
 *                                                                          *
 * Output(s)     : None                                                     *
 *                                                                          *
 * Returns       : UPNP_E_SUCCESS / UPNP_SUCCESS/ UPNP_FAILURE/             *
 *                 UPNP_ACT_E_VENDOR_SPE                                    *
 ****************************************************************************/
INT4
PppUPnPGetUserName (tUPnPActionReq * pActReq)
{
    CHR1               *pu1PppSecuritySecretsIdentity = NULL;
    INT4                i4PppSecuritySecretsLink = 0;
    INT4                i4PppSecuritySecretsIdIndex = 0;

    INT4                i4RetVal = 0;
    VOID              **pActRes = pActReq->ppActionResp;

    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    OctetStr.pu1_OctetList = au1OctetList;

    /* Get the all SNMP Indices value by using known values 
     * If fails set Error Code and Free the State Varaibles */
    if (UPnPUtlGetIdxGetUserName
        (pActReq->pu1ServiceID, &i4PppSecuritySecretsLink,
         &i4PppSecuritySecretsIdIndex) == OSIX_FAILURE)
    {
        *(pActReq->pu4ErrCode) = UPNP_ACT_E_ACTION_FAILED;
        *(pActReq->ppActionResp) = NULL;
        return *(pActReq->pu4ErrCode);
    }

    /* Call the corresponding low leval nmh routines to get the value */
    i4RetVal =
        UPnPSnmpNmhGet (*UPnPSnmpJoin
                        (PppSecuritySecretsIdentity, 12,
                         PppSecuritySecretsTableINDEX, 2,
                         i4PppSecuritySecretsLink, i4PppSecuritySecretsIdIndex),
                        SNMP_DATA_TYPE_OCTET_PRIM, &OctetStr);
    if (i4RetVal == SNMP_FAILURE)
    {
        UPNP_MOD_TRC2 (UPNP_FAIL_TRC, "Failed in PppUPnPGetUserName:"
                       "nmhGetPppSecuritySecretsIdentity failed -"
                       "IN Args.%d, %d \r\n", i4PppSecuritySecretsLink,
                       i4PppSecuritySecretsIdIndex);
    }

    pu1PppSecuritySecretsIdentity = (CHR1 *) OctetStr.pu1_OctetList;

    /* Send the Action Response with the values of OUT State Varaible */
    *pActRes =
        (VOID *) UPnPArwrStackMakeActionResponse ("GetUserName",
                                                  pActReq->pu1ServiceID, 1,
                                                  "NewUserName",
                                                  pu1PppSecuritySecretsIdentity);
    if (*pActRes == NULL)
    {
        UPNP_MOD_TRC (UPNP_FAIL_TRC, "Failed in PppUPnPGetUserName:"
                      "Failed to Construct Action Response\r\n");
        *(pActReq->pu4ErrCode) = UPNP_ACT_E_ACTION_FAILED;
        *(pActReq->ppActionResp) = NULL;
        return *(pActReq->pu4ErrCode);
    }
    *(pActReq->pu4ErrCode) = UPNP_E_SUCCESS;
    return *(pActReq->pu4ErrCode);
}

/****************************************************************************
 * Function Name : PppUPnPGetPassword                                       *
 *                                                                          *
 * Description   : To Handle GetPassword Action which is                    *
 *                 invoked from  Control Point                              *
 *                                                                          *
 * Input(s)      : pActReq - Pointer to Document of action request          *
 *                                                                          *
 * Output(s)     : None                                                     *
 *                                                                          *
 * Returns       : UPNP_E_SUCCESS / UPNP_SUCCESS/ UPNP_FAILURE/             *
 *                 UPNP_ACT_E_VENDOR_SPE                                    *
 ****************************************************************************/
INT4
PppUPnPGetPassword (tUPnPActionReq * pActReq)
{
    CHR1               *pu1PppSecuritySecretsSecret = NULL;
    INT4                i4PppSecuritySecretsLink = 0;
    INT4                i4PppSecuritySecretsIdIndex = 0;

    INT4                i4RetVal = 0;
    VOID              **pActRes = pActReq->ppActionResp;

    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    OctetStr.pu1_OctetList = au1OctetList;

    /* Get the all SNMP Indices value by using known values 
     * If fails set Error Code and Free the State Varaibles */
    if (UPnPUtlGetIdxGetPassword
        (pActReq->pu1ServiceID, &i4PppSecuritySecretsLink,
         &i4PppSecuritySecretsIdIndex) == OSIX_FAILURE)
    {
        *(pActReq->pu4ErrCode) = UPNP_ACT_E_ACTION_FAILED;
        *(pActReq->ppActionResp) = NULL;
        return *(pActReq->pu4ErrCode);
    }

    /* Call the corresponding low leval nmh routines to get the value */
    i4RetVal =
        UPnPSnmpNmhGet (*UPnPSnmpJoin
                        (PppSecuritySecretsSecret, 12,
                         PppSecuritySecretsTableINDEX, 2,
                         i4PppSecuritySecretsLink, i4PppSecuritySecretsIdIndex),
                        SNMP_DATA_TYPE_OCTET_PRIM, &OctetStr);
    if (i4RetVal == SNMP_FAILURE)
    {
        UPNP_MOD_TRC2 (UPNP_FAIL_TRC, "Failed in PppUPnPGetPassword:"
                       "nmhGetPppSecuritySecretsSecret failed -"
                       "IN Args.%d, %d \r\n", i4PppSecuritySecretsLink,
                       i4PppSecuritySecretsIdIndex);
    }

    pu1PppSecuritySecretsSecret = (CHR1 *) OctetStr.pu1_OctetList;

    /* Send the Action Response with the values of OUT State Varaible */
    *pActRes =
        (VOID *) UPnPArwrStackMakeActionResponse ("GetPassword",
                                                  pActReq->pu1ServiceID, 1,
                                                  "NewPassword",
                                                  pu1PppSecuritySecretsSecret);
    if (*pActRes == NULL)
    {
        UPNP_MOD_TRC (UPNP_FAIL_TRC, "Failed in PppUPnPGetPassword:"
                      "Failed to Construct Action Response\r\n");
        *(pActReq->pu4ErrCode) = UPNP_ACT_E_ACTION_FAILED;
        *(pActReq->ppActionResp) = NULL;
        return *(pActReq->pu4ErrCode);
    }
    *(pActReq->pu4ErrCode) = UPNP_E_SUCCESS;
    return *(pActReq->pu4ErrCode);
}

/****************************************************************************
 * Function Name : PppUPnPGetPPPEncryptionProtocol                          *
 *                                                                          *
 * Description   : To Handle GetPPPEncryptionProtocol Action which is       *
 *                 invoked from  Control Point                              *
 *                                                                          *
 * Input(s)      : pActReq - Pointer to Document of action request          *
 *                                                                          *
 * Output(s)     : None                                                     *
 *                                                                          *
 * Returns       : UPNP_E_SUCCESS / UPNP_SUCCESS/ UPNP_FAILURE/             *
 *                 UPNP_ACT_E_VENDOR_SPE                                    *
 ****************************************************************************/
INT4
PppUPnPGetPPPEncryptionProtocol (tUPnPActionReq * pActReq)
{
    *(pActReq->pu4ErrCode) = UPNP_ACT_E_ACTION_NOT_SUPPORTED;
    *(pActReq->ppActionResp) = NULL;
    return *(pActReq->pu4ErrCode);

}

/****************************************************************************
 * Function Name : PppUPnPGetPPPCompressionProtocol                         *
 *                                                                          *
 * Description   : To Handle GetPPPCompressionProtocol Action which is      *
 *                 invoked from  Control Point                              *
 *                                                                          *
 * Input(s)      : pActReq - Pointer to Document of action request          *
 *                                                                          *
 * Output(s)     : None                                                     *
 *                                                                          *
 * Returns       : UPNP_E_SUCCESS / UPNP_SUCCESS/ UPNP_FAILURE/             *
 *                 UPNP_ACT_E_VENDOR_SPE                                    *
 ****************************************************************************/
INT4
PppUPnPGetPPPCompressionProtocol (tUPnPActionReq * pActReq)
{
    *(pActReq->pu4ErrCode) = UPNP_ACT_E_ACTION_NOT_SUPPORTED;
    *(pActReq->ppActionResp) = NULL;
    return *(pActReq->pu4ErrCode);

}

/****************************************************************************
 * Function Name : UPnPUtilGetIndicesForGetUserName                         *
 *                                                                          *
 * Description   : To get Interface Index and Secrets ID Idx using serviceId*
 *                                                                          *
 * Input(s)      : pu1ServiceID - Pointer to Service ID                     *
 *                                                                          *
 * Output(s)     : pi4PppSecuritySecretsLink - Pointer to Interface Index   *
 *                 pi4PppSecuritySecretsIdIndex - Pointer to Secrets ID Idx *
 *                                                                          *
 * Returns       : UPNP_SUCCESS/ UPNP_FAILURE                               *
 ****************************************************************************/
UINT4
UPnPUtlGetIdxGetUserName (CONST UINT1 *pu1ServiceID,
                          INT4 *pi4PppSecuritySecretsLink,
                          INT4 *pi4PppSecuritySecretsIdIndex)
{
    *pi4PppSecuritySecretsIdIndex = 1;
    if (UPnPUtlGetIndexFromServiceId (pu1ServiceID, pi4PppSecuritySecretsLink,
                                      NULL) == OSIX_FAILURE)
    {
        UPNP_MOD_TRC (UPNP_FAIL_TRC, "UPnPUtlGetIdxGetUserName: "
                      "Failed to get Interface Index form ServiceID\r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function Name : UPnPUtlSetValForConfigureConnection                      *
 *                                                                          *
 * Description   : Util function to set the User name and Password values   *
 *                                                                          *
 * Input(s)      : pu1ServiceID - Pointer to Service ID                     *
 *                 pu1NewUserName - Pointer to the value of Username to set *
 *                 pu1NewPassword - Pointer to the value of Password to set *
 *                                                                          *
 * Output(s)     : pu4ErrCode - Pointer to Error Code                       *
 *                                                                          *
 * Returns       : UPNP_SUCCESS/ UPNP_FAILURE                               *
 ****************************************************************************/
INT4
UPnPUtlSetValForConfigureConnection (CONST UINT1 *pu1ServiceID,
                                     CHR1 * pu1NewUserName,
                                     CHR1 * pu1NewPassword, UINT4 *pu4ErrCode)
{
    tPPPIf             *pIf;
    INT4                i4PppIfaceNo = 0;
    INT4                i4SecretIdIndex = 0;
    UINT1               u1Direction = PPP_LOCAL_TO_REMOTE;
    UINT1               au1Identity[MAX_SECRET_SIZE];
    UINT1               au1Secret[MAX_SECRET_SIZE];
    tSNMP_OCTET_STRING_TYPE Identity;
    tSNMP_OCTET_STRING_TYPE Secret;

    MEMSET (au1Identity, 0, MAX_SECRET_SIZE);
    MEMSET (au1Secret, 0, MAX_SECRET_SIZE);

    Identity.pu1_OctetList = au1Identity;
    Secret.pu1_OctetList = au1Secret;

    if (UPnPUtlGetIndexFromServiceId (pu1ServiceID, &i4PppIfaceNo,
                                      NULL) == OSIX_FAILURE)
    {
        *pu4ErrCode = UPNP_ACT_E_ACTION_FAILED;
        UPNP_MOD_TRC (UPNP_FAIL_TRC, "UPnPUtlSetValForConfigureConnection: "
                      "Failed to get Interface Index form ServiceID\r\n");
        return OSIX_FAILURE;
    }

    /* For PPP_LOCAL_TO_REMOTE, Secret Id Index is 1 */
    i4SecretIdIndex = 1;

    if ((pIf = (tPPPIf *) PppGetIfPtr (i4PppIfaceNo)) == NULL)
    {
        *pu4ErrCode = UPNP_ACT_E_ACTION_FAILED;
        UPNP_MOD_TRC (UPNP_FAIL_TRC, "UPnPUtlSetValForConfigureConnection: "
                      "Invalid PPP Index\r\n");
        return OSIX_FAILURE;
    }

    if (pIf->LcpStatus.AdminStatus == ADMIN_OPEN)
    {
        *pu4ErrCode = UPNP_ACT_E_INACTIVE_CONNECTION_STATE_REQUIRED;
        UPNP_MOD_TRC (UPNP_FAIL_TRC, "UPnPUtlSetValForConfigureConnection: "
                      "Interface must first be made administratively down\r\n");
        return OSIX_FAILURE;
    }

    if (pIf->MPInfo.MemberInfo.pBundlePtr != NULL)
    {
        *pu4ErrCode = UPNP_ACT_E_ACTION_FAILED;
        UPNP_MOD_TRC (UPNP_FAIL_TRC, "UPnPUtlSetValForConfigureConnection: "
                      "Cannot set username on multilink bundle members\r\n");
        return OSIX_FAILURE;
    }

    /* Add PAP entry */
    nmhSetPppSecuritySecretsProtocol (i4PppIfaceNo, i4SecretIdIndex,
                                      &pppSecurityPapProtocolOID);
    nmhSetPppSecuritySecretsDirection (i4PppIfaceNo, i4SecretIdIndex,
                                       u1Direction);

    STRCPY (au1Identity, pu1NewUserName);
    Identity.i4_Length = STRLEN (pu1NewUserName);
    nmhSetPppSecuritySecretsIdentity (i4PppIfaceNo, i4SecretIdIndex, &Identity);

    STRCPY (au1Secret, pu1NewPassword);
    Secret.i4_Length = STRLEN (pu1NewPassword);
    nmhSetPppSecuritySecretsSecret (i4PppIfaceNo, i4SecretIdIndex, &Secret);

    nmhSetPppSecuritySecretsStatus (i4PppIfaceNo, i4SecretIdIndex,
                                    SECRETS_STATUS_VALID);

    ++i4SecretIdIndex;

    /* Add CHAP entry */
    nmhSetPppSecuritySecretsProtocol (i4PppIfaceNo, i4SecretIdIndex,
                                      &pppSecurityChapMD5ProtocolOID);
    nmhSetPppSecuritySecretsDirection (i4PppIfaceNo, i4SecretIdIndex,
                                       u1Direction);

    STRCPY (au1Identity, pu1NewUserName);
    Identity.i4_Length = STRLEN (pu1NewUserName);
    nmhSetPppSecuritySecretsIdentity (i4PppIfaceNo, i4SecretIdIndex, &Identity);

    STRCPY (au1Secret, pu1NewPassword);
    Secret.i4_Length = STRLEN (pu1NewPassword);
    nmhSetPppSecuritySecretsSecret (i4PppIfaceNo, i4SecretIdIndex, &Secret);

    nmhSetPppSecuritySecretsStatus (i4PppIfaceNo, i4SecretIdIndex,
                                    SECRETS_STATUS_VALID);

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function Name : UPnPUtlGetValForGetPPPAuthenticationProtocol             *
 *                                                                          *
 * Description   : Util function to get the Authentication Protocol value   *
 *                                                                          *
 * Input(s)      : pu1ServiceID - Pointer to Service ID                     *
 *                                                                          *
 * Output(s)     : pu1AuthProtocol - Pointer to AuthenticationProtocol Type *
 *                                                                          *
 * Returns       : UPNP_SUCCESS/ UPNP_FAILURE                               *
 ****************************************************************************/
UINT4
UPnPUtlGetValForGetPPPAuthenticationProtocol (CONST UINT1 *pu1ServiceID,
                                              CHR1 * pu1AuthProtocol)
{
    tPPPIf             *pIf;
    INT4                i4PppSecuritySecretsLink;

    if (UPnPUtlGetIndexFromServiceId (pu1ServiceID, &i4PppSecuritySecretsLink,
                                      NULL) == OSIX_FAILURE)
    {
        UPNP_MOD_TRC (UPNP_FAIL_TRC,
                      "UPnPUtlGetValForGetPPPAuthenticationProtocol: "
                      "Failed to get Interface Index form ServiceID\r\n");
        return OSIX_FAILURE;
    }

    if ((pIf = PppGetIfPtr (i4PppSecuritySecretsLink)) == NULL)
    {
        return OSIX_FAILURE;
    }

    if (((tAUTHIf *) pIf->pAuthPtr)->SecurityInfo.PrefList[ZERO].Protocol
        == CHAP_PROTOCOL)
    {
        STRCPY (pu1AuthProtocol, "CHAP");
    }
    else if (((tAUTHIf *) pIf->pAuthPtr)->SecurityInfo.PrefList[ZERO].Protocol
             == PAP_PROTOCOL)
    {
        STRCPY (pu1AuthProtocol, "PAP");
    }
    else
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
