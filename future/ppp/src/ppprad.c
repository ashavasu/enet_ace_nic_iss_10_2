/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppprad.c,v 1.8 2014/12/16 11:47:26 siva Exp $
 *
 * Description:This file provides the routines to contact RADIUS  
 *             Server  using FutureRADIUS Client            
 *
 *******************************************************************/
#ifdef RADIUS_WANTED

#include     "pppcom.h"
#include     "lcpdefs.h"
#include     "frmdefs.h"
#include     "gsemdefs.h"
#include     "genexts.h"
#include    "ppptask.h"
#include    "gcpdefs.h"
#include    "lcpprot.h"
#include    "pppipcp.h"
#include "authcom.h"
#include "pppproto.h"
#include "globexts.h"

#include "ppprad.h"
#include "pppmemmac.h"
/* external varibles definitions */
UINT1               gu1PppRandSession1;
UINT1               gu1PppRandSession2;
UINT4               gu4PppSessionId;
/*****************************************************************************/
/*    Function Name             :PppRadPapAuthenticate                 */
/*    Description               :                             */
/*                                                                           */
/*    Input(s)                  :                                            */
/*                                                                           */
/*    Output(s)                 : None.                                      */
/*    Returns                   : SUCCESS if the packet is to permitted,     */
/*                                otherwise FAILURE.                         */
/*****************************************************************************/

INT4
PppRadPapAuthenticate (UINT1 *pPeerId, UINT1 PeerIdLen, UINT1 *pPasswd,
                       UINT1 PasswdLen, UINT4 ifIndex)
{
    tRADIUS_INPUT_AUTH  RadInputAuth;
    tUSER_INFO_PAP      UserInfoPap;
    INT4                Id;

    RadInputAuth.u1_ProtocolType = PRO_PAP;
    RadInputAuth.p_UserInfoPAP = &UserInfoPap;

    MEMCPY (UserInfoPap.a_u1UserName, pPeerId, PeerIdLen);
    MEMCPY (UserInfoPap.a_u1UserPasswd, pPasswd, PasswdLen);

    PppRadFillOthersAuth (&RadInputAuth, ifIndex);

    Id = radiusAuthentication (&RadInputAuth, NULL, PppRadResponse, ifIndex, 0);

/* free the allocated memmory : pServises is a linked list ! */
    PppMemFreeServices (RadInputAuth.p_ServicesAuth);
    return Id;

}

/**************************************************************************/
INT4
PppRadCHAPAuthenticate (UINT1 *pPeerId, UINT1 u1PeerIdLen, UINT1 *pPasswd,
                        UINT1 u1PasswdLen, UINT1 Identifier, UINT1 *Challenge,
                        UINT1 ChallengeLength, UINT4 ifIndex)
{
    tRADIUS_INPUT_AUTH  RadInputAuth;
    tUSER_INFO_CHAP     UserInfoCHAP;
    INT4                Id;

    RadInputAuth.u1_ProtocolType = PRO_CHAP;
    RadInputAuth.p_UserInfoCHAP = &UserInfoCHAP;

    /* Fill CHAP Attributes */
    MEMCPY (UserInfoCHAP.a_u1UserName, pPeerId, u1PeerIdLen);
    UserInfoCHAP.u1_Identifier = Identifier;

    MEMCPY (UserInfoCHAP.a_u1Response, pPasswd, u1PasswdLen);
    MEMCPY (UserInfoCHAP.a_u1Challenge, Challenge, ChallengeLength);

    PppRadFillOthersAuth (&RadInputAuth, ifIndex);

    Id = radiusAuthentication (&RadInputAuth, NULL, PppRadResponse, ifIndex, 0);

/* free the allocated memmory : pServises is a linked list ! */
    PppMemFreeServices (RadInputAuth.p_ServicesAuth);

    return Id;

}

/************************************************/
INT4
PppRadMSCHAPAuthenticate (UINT1 *pPeerId, UINT1 u1PeerIdLen, UINT1 Identifier,
                          UINT1 *Challenge, UINT1 ChallengeLength,
                          UINT1 *LMResponse, UINT1 *NTResponse, UINT4 ifIndex)
{
    tRADIUS_INPUT_AUTH  RadInputAuth;
    tUSER_INFO_MS_CHAP  UserInfoMSCHAP;
    INT4                Id;

    RadInputAuth.u1_ProtocolType = PRO_MS_CHAP;
    RadInputAuth.p_UserInfoMschap = &UserInfoMSCHAP;

    /* Fill MS-CHAP Attributes */
    MEMCPY (UserInfoMSCHAP.a_u1UserName, pPeerId, u1PeerIdLen);
    MEMCPY (UserInfoMSCHAP.a_u1UserPasswd, pPeerId, u1PeerIdLen);
    UserInfoMSCHAP.u1_Identifier = Identifier;
    MEMCPY (UserInfoMSCHAP.a_u1Challenge, Challenge, ChallengeLength);
    MEMCPY (UserInfoMSCHAP.a_u1ResponseLM, LMResponse,
            MSCHAP_LM_CHAL_RESP_LENGTH);
    MEMCPY (UserInfoMSCHAP.a_u1ResponseNT, NTResponse,
            MSCHAP_NT_CHAL_RESP_LENGTH);

    PppRadFillOthersAuth (&RadInputAuth, ifIndex);

    Id = radiusAuthentication (&RadInputAuth, NULL, PppRadResponse, ifIndex, 0);

/* free the allocated memmory : pServises is a linked list ! */
    PppMemFreeServices (RadInputAuth.p_ServicesAuth);

    return Id;

}

INT4
PppRadEapAuthenticate (UINT1 *pPeerId, UINT1 u1PeerIdLen, UINT1 *au1EapPkt,
                       UINT2 u2Length, UINT4 ifIndex,
                       UINT1 *pEapState, UINT1 u1EapStateLen)
{
    tRADIUS_INPUT_AUTH  RadInputAuth;
    tUSER_INFO_EAP      UserInfoEAP;
    INT4                i4_Id;
    UINT1               u1_StateLen;

    RadInputAuth.u1_ProtocolType = PRO_EAP;
    RadInputAuth.p_UserInfoEAP = &UserInfoEAP;

    /* Fill EAP Attributes */
    MEMCPY (UserInfoEAP.a_u1UserName, pPeerId, u1PeerIdLen);
    MEMCPY (UserInfoEAP.a_u1Response, au1EapPkt, u2Length);
    if (pEapState)
    {
        u1_StateLen = u1EapStateLen;
        RadInputAuth.u1_StateLen = u1EapStateLen;
        MEMCPY (RadInputAuth.a_u1State, pEapState, u1_StateLen);
    }

    PppRadFillOthersAuth (&RadInputAuth, ifIndex);

    i4_Id = radiusAuthentication (&RadInputAuth, NULL, PppRadResponse, ifIndex,
                                  0);

/* free the allocated memmory : pServises is a linked list ! */
    PppMemFreeServices (RadInputAuth.p_ServicesAuth);

    return i4_Id;

}

/************************************************/
VOID
PppRadFillOthersAuth (tRADIUS_INPUT_AUTH * pRadInputAuth, UINT4 ifIndex)
{
    tPPPIf             *pIf;
    tSERVICES          *pTServices;
/* according to rfc Access Request MUST Contain either NAS-IP-Address 
 * or NAS-Identifier.*/
    pRadInputAuth->u4_NasIPAddress = DEFAULT_NAS_IPADDRESS;
    STRCPY (pRadInputAuth->a_u1NasId, RAD_NAS_IDENTIFIER_STRING);
    pRadInputAuth->u4_NasPort = NO_ATTRIBUTE;
    pRadInputAuth->u4_NasPortType = NO_ATTRIBUTE;
/* if you dont want to include these attributes in the access request
 * comment/modify the following if statement*/
    if ((pIf = PppGetIfPtr (ifIndex)))
    {
        switch (pIf->LinkInfo.IfID.IfType)
        {
            case SERIAL_IF_TYPE:
                pRadInputAuth->u4_NasPortType = NASP_SYNC;
                break;
            case ASYNC_IF_TYPE:
                pRadInputAuth->u4_NasPortType = NASP_ASYNC;
                break;
            case ISDN_IF_TYPE:
                pRadInputAuth->u4_NasPortType = NASP_ISDN_SYNC;
                break;
            default:
                break;
        }
        pRadInputAuth->u4_NasPort = (INT4) pIf->LinkInfo.PhysIfIndex;
    }
/* for including extra attributes use pservices . copy the string value or 
 * Numerical value the structure.*/
    if (MemAllocateMemBlock
        (PPP_SERVICES_MEMPOOL_ID,
         (UINT1 **) (VOID *) &pTServices) == MEM_FAILURE)
    {

        PRINT_MEM_ALLOC_FAILURE;
        return;
    }
    BZERO (pTServices, sizeof (tSERVICES));
    pTServices->u1_ServiceType = A_SERVICE_TYPE;
    pTServices->u4_NumericalValue = SERT_FRAMED;

    pRadInputAuth->p_ServicesAuth = pTServices;
/* repeat the following section for adding any new attributes.*/
    if (MemAllocateMemBlock
        (PPP_SERVICES_MEMPOOL_ID,
         (UINT1 **) (VOID *) &(pTServices->p_NextService)) == MEM_FAILURE)
    {
        PRINT_MEM_ALLOC_FAILURE;
        return;
    }

    pTServices = pTServices->p_NextService;
    BZERO (pTServices, sizeof (tSERVICES));
    pTServices->u1_ServiceType = A_FRAMED_PROTOCOL;
    pTServices->u4_NumericalValue = FP_PPP;

/* In case of string/text attributes use the following method
 * pTServices->p_NextService = ( tSERVICES *)malloc ( sizeof(tSERVICES) );
 * pTServices = pTServices->p_NextService; 
 * BZERO(pTServices, sizeof(tSERVICES) ); 
 * pTServices->u1_ServiceType = A_STATE;
 * STRCPY(pTServices->a_u1StringValue,"STATE_STRING"); 
 * */

}

/**************************************************************************/
INT4
PppRadAccounting (tPPPIf * pIf, UINT1 u1Flag)
{
    tRADIUS_INPUT_ACC   RadInputAcct;
    tACCOUNT_STAT       AccountStat;
    INT4                Id;
    INT4               iRetVal;

    /* create a unique session id and store it */
    if (u1Flag == PPP_ACCT_START)
    {
        pIf->RadInfo.pu1SessionId = PppRadGetAcctSessionId ();
        if (pIf->RadInfo.pu1SessionId == NULL)
        {
            PRINT_MEM_ALLOC_FAILURE;
            return NOT_OK;
        }
    }

    /* Fill the Session Id, NAS_ID , etc */
    if (STRLEN (pIf->RadInfo.Username) != 0)
    {
        STRCPY (&RadInputAcct.a_u1UserName, pIf->RadInfo.Username);
    }

    /* session Id */
    iRetVal = (INT4)STRLEN(pIf->RadInfo.pu1SessionId);
    STRNCPY (&RadInputAcct.a_u1SessionId,pIf->RadInfo.pu1SessionId,iRetVal);
    RadInputAcct.u4_AuthType = AUTH_RADIUS;

    /* fill other attributes */
    PppRadFillOthersAcct (&RadInputAcct, pIf->LinkInfo.IfIndex);

    if (u1Flag == PPP_ACCT_STOP)
    {

        FREE (pIf->RadInfo.pu1SessionId);    /* free the memmory allocated for
                                             * session id */

        /* Fill the accounting statistics */
        AccountStat.u4_InputOctets = pIf->LLHCounters.InOctets;
        AccountStat.u4_OutputOctets = pIf->LLHCounters.OutOctets;
        AccountStat.u4_SessionTime =
            pIf->LLHCounters.LastChangeTicks -
            pIf->LLHCounters.SessionStartTicks;

#ifdef SYS_NUM_OF_TIME_UNITS_IN_A_SEC
        AccountStat.u4_SessionTime /= SYS_NUM_OF_TIME_UNITS_IN_A_SEC;
#endif
        AccountStat.u4_TerminateCause = pIf->RadInfo.AcctTerminateCause;

#if PPP0
        AccountStat.u4_InputPackets = pIf->LLHCounters.InNUniPackets;
        AccountStat.u4_OutputPackets = pIf->LLHCounters.OutNUniPackets;
#endif
        /*pAccountStat->u4_TerminateCause   = 1;    */

        RadInputAcct.p_AccountStat = &AccountStat;

    } /*== PPP_ACCT_STOP) */

    /* call the API provided by radius */
    Id = radiusAccounting (&RadInputAcct, NULL, PppRadResponse,
                           pIf->LinkInfo.IfIndex);
    pIf->RadInfo.RadReqId = (INT1) Id;

/* free the allocated memmory : pServises is a linked list ! */
    PppMemFreeServices (RadInputAcct.p_ServicesAcc);

    return Id;

}

/*****************************************/
UINT1              *
PppRadGetAcctSessionId (VOID)
{
    UINT1              *u1SessionId = NULL;
    UINT1              *TempStrval = NULL;

    u1SessionId = CALLOC(1, 14);
    if (u1SessionId != NULL)
    {
        u1SessionId[0] = (UINT1) ((UINT1) '0' + gu1PppRandSession1);
        u1SessionId[1] = (UINT1) ((UINT1) '0' + gu1PppRandSession2);
        TempStrval = u1SessionId + 2;

        SPRINTF ((char *) TempStrval, "%d", gu4PppSessionId);
    }
    gu4PppSessionId++;

    return (u1SessionId);

}

/**************************************/
VOID
PppRadFillOthersAcct (tRADIUS_INPUT_ACC * pRadInputAcct, UINT4 ifIndex)
{
    tPPPIf             *pIf;
/* according to rfc Access Request MUST Contain either NAS-IP-Address 
 * or NAS-Identifier.*/
    pRadInputAcct->u4_NasIPAddress = DEFAULT_NAS_IPADDRESS;
    STRCPY (pRadInputAcct->a_u1NasId, RAD_NAS_IDENTIFIER_STRING);
    pRadInputAcct->u4_NasPort = NO_ATTRIBUTE;
    pRadInputAcct->u4_NasPortType = NO_ATTRIBUTE;
/* if you dont want to include these attributes in the access request
 * comment/modify the following if statement*/
    if ((pIf = PppGetIfPtr (ifIndex)) != NULL)
    {
        switch (pIf->LinkInfo.IfID.IfType)
        {
            case SERIAL_IF_TYPE:
                pRadInputAcct->u4_NasPortType = NASP_SYNC;
                break;
            case ASYNC_IF_TYPE:
                pRadInputAcct->u4_NasPortType = NASP_ASYNC;
                break;
            case ISDN_IF_TYPE:
                pRadInputAcct->u4_NasPortType = NASP_ISDN_SYNC;
                break;
            default:
                break;
        }
        pRadInputAcct->u4_NasPort = (INT4) pIf->LinkInfo.PhysIfIndex;
    }
}

/**************************************************************************/

VOID
PppMemFreeServices (tSERVICES * pServicesToFree)
{

    tSERVICES          *pServices, *pTServices;

    for (pServices = pServicesToFree; pServices != NULL; pServices = pTServices)
    {
        pTServices = pServices->p_NextService;
        MemReleaseMemBlock (PPP_SERVICES_MEMPOOL_ID, (UINT1 *) pTServices);

    }

}

/**************************************************************************/
/**************************************************************************/
VOID
PppRadResponse (VOID *pInterface)
{
    t_MSG_DESC         *pBuf;
    UINT4               u4Temp;
    u4Temp = PTR_TO_U4 (pInterface);
    if ((pBuf = (t_MSG_DESC *) (VOID *) ALLOCATE_BUFFER (8, 0)) == NULL)
        return;

    ASSIGN4BYTE (pBuf, 0, u4Temp);
    PPP_GET_SRC_MODID (pBuf) = PPPMSG_FROM_RADIUS_CLIENT;

    if (OsixSendToQ (SELF, PPP_QUEUE_NAME, pBuf, OSIX_MSG_NORMAL) !=
        OSIX_SUCCESS)
    {
        PPP_TRC (ALL_FAILURE, "Send to PPP Queue Failure.");
        RELEASE_BUFFER (pBuf);
        return;
    }

    if (OsixSendEvent (SELF, PPP_TASK_NAME, PPP_QUEUE_EVENT) != OSIX_SUCCESS)
    {
        PPP_TRC (ALL_FAILURE, "Radius Message Failure.");
        RELEASE_BUFFER (pBuf);
        return;
    }

    return;
}

/**************************************************************************/
VOID
PppRadReceivedResponse (VOID *pRad)
{
    tRadInterface      *pInterface;
    tPPPIf             *pIf;
    tAUTHIf            *pAuthPtr;
    UINT1               Msg[MAX_MSG_SIZE];
    tEAPMD5ServerInfo  *pServerEAPMD5;
    tSecret            *pSecret;
    UINT1               u1_StateLen;
    UINT2               u2_EapPktLen;
    INT4                i4RetVal=0 ;

    pInterface = (tRadInterface *) pRad;
    MEMSET (Msg, 0, MAX_MSG_SIZE);
    /* return if no if pIf is NULL or if the Id is different.
     * RadReqId is the return value by radiusAuthentication
     * */
    pIf = PppGetIfPtr (pInterface->ifIndex);
    if ((pIf == NULL) || (pIf->RadInfo.RadReqId != pInterface->RequestId))
        return;

    pAuthPtr = pIf->pAuthPtr;

    pServerEAPMD5 =
        &pAuthPtr->ServerInfo.ServerEap.AuthTypeOptions.ServerEAPMD5;
    if (MemAllocateMemBlock
        (PPP_SECRET_MEMPOOL_ID, (UINT1 **) (VOID *) &pSecret) == MEM_FAILURE)
        return;

    switch (pInterface->Access)
    {

        case ACCESS_ACCEPT:
            PppUpdateAuthorizationParams (pIf, pInterface);
            i4RetVal = (INT4)STRLEN(pInterface->Reply_Message);
            if (i4RetVal<= MAX_MSG_SIZE)
            {
                STRNCPY (Msg, pInterface->Reply_Message,i4RetVal);
            }
            else
                STRCPY (Msg, "You have successfully logged in...");

            /*****************************/
            switch (pAuthPtr->ServerProt)
            {

                case PAP_PROTOCOL:
                    PppRadSendPapStatus (pIf, PAP_AUTH_ACK, Msg,
                                         pIf->RadInfo.PppAuthId);
                    pAuthPtr->ServerInfo.ServerPap.State =
                        PAP_SERVER_STATE_OPEN;
                    LCPUpdateAuthStatus (pIf, PAP_PEER_TRUE);
                    break;
                case CHAP_PROTOCOL:
                    CHAPSendStatus (pAuthPtr, CHAP_TRUE);
                    pAuthPtr->ServerInfo.ServerChap.State =
                        CHAP_SERVER_STATE_OPEN;

                    if (pIf->RadInfo.ChapState ==
                        CHAP_SERVER_STATE_INITIAL_CHAL)
                    {
                        LCPUpdateAuthStatus (pIf, CHAP_PEER_TRUE);
                    }

                    if (pAuthPtr->ServerInfo.ServerChap.ChallengeInterval != 0)
                    {
                        pAuthPtr->ServerInfo.ServerChap.ChapReChalTimer.Param1 =
                            PTR_TO_U4 (pAuthPtr);
                        PPPStartTimer (&pAuthPtr->ServerInfo.
                                       ServerChap.ChapReChalTimer,
                                       CHAP_RECHAL_TIMER,
                                       pAuthPtr->ServerInfo.
                                       ServerChap.ChallengeInterval);
                    }

                    break;
                case EAP_PROTOCOL:
                    EAPSendStatus (pAuthPtr, EAP_SUCCESS);
                    pAuthPtr->ServerInfo.ServerEap.State =
                        EAP_SERVER_STATE_OPEN;

                    if (pAuthPtr->ServerInfo.ServerEap.AuthTypeOptions.
                        ServerEAPMD5.State == EAPMD5_SERVER_STATE_INITIAL_CHAL)
                    {
                        LCPUpdateAuthStatus (pIf, EAP_PEER_TRUE);
                    }
                    if (pAuthPtr->ServerInfo.ServerEap.AuthTypeOptions.
                        ServerEAPMD5.ChallengeInterval != 0)
                    {
                        pAuthPtr->ServerInfo.ServerEap.AuthTypeOptions.
                            ServerEAPMD5.ReChalTimer.Param1 =
                            PTR_TO_U4 (pAuthPtr);
                        PPPStartTimer (&pAuthPtr->ServerInfo.ServerEap.
                                       AuthTypeOptions.ServerEAPMD5.ReChalTimer,
                                       EAP_MD5_RECHAL_TIMER,
                                       pAuthPtr->ServerInfo.ServerEap.
                                       AuthTypeOptions.ServerEAPMD5.
                                       ChallengeInterval);
                    }

                    break;

                case MSCHAP_PROTOCOL:
                    MSCHAPSendStatus (pAuthPtr, CHAP_TRUE,
                                      (const UINT1 *) NULL);
                    pAuthPtr->ServerInfo.ServerChap.State =
                        CHAP_SERVER_STATE_OPEN;

                    if (pIf->RadInfo.ChapState ==
                        CHAP_SERVER_STATE_INITIAL_CHAL)
                    {
                        LCPUpdateAuthStatus (pIf, MSCHAP_PEER_TRUE);
                    }

                    if (pAuthPtr->ServerInfo.ServerChap.ChallengeInterval != 0)
                    {
                        pAuthPtr->ServerInfo.ServerChap.ChapReChalTimer.Param1 =
                            PTR_TO_U4 (pAuthPtr);
                        PPPStartTimer (&pAuthPtr->ServerInfo.
                                       ServerChap.ChapReChalTimer,
                                       CHAP_RECHAL_TIMER,
                                       pAuthPtr->ServerInfo.
                                       ServerChap.ChallengeInterval);
                    }
                    break;

                default:
                    break;
            }
    /*****************/

            break;                /* For Access Accept */

        case ACCESS_REJECT:

             i4RetVal = (INT4)STRLEN(pInterface->Reply_Message);
            if ( i4RetVal<= MAX_MSG_SIZE)
            {
                STRNCPY (Msg, pInterface->Reply_Message,i4RetVal);
            }
            else
                STRCPY (Msg, "You have failed to log in....");

        /*****************************/
            switch (pAuthPtr->ServerProt)
            {

                case PAP_PROTOCOL:
                    pAuthPtr->ServerInfo.ServerPap.State =
                        PAP_SERVER_STATE_BADAUTH;
                    PppRadSendPapStatus (pIf, PAP_AUTH_NAK, Msg,
                                         pIf->RadInfo.PppAuthId);
                    LCPAuthProcessFailed (pIf);
                    break;
                case EAP_PROTOCOL:
                    pAuthPtr->ServerInfo.ServerEap.State =
                        EAP_SERVER_STATE_BADAUTH;
                    EAPSendStatus (pAuthPtr, EAP_FAILURE);
                    LCPAuthProcessFailed (pIf);
                    break;

                case CHAP_PROTOCOL:
                    pAuthPtr->ServerInfo.ServerChap.State =
                        CHAP_SERVER_STATE_BADAUTH;
                    CHAPSendStatus (pAuthPtr, CHAP_FALSE);
                    LCPAuthProcessFailed (pIf);
                    break;

                case MSCHAP_PROTOCOL:
                    pAuthPtr->ServerInfo.ServerChap.State =
                        CHAP_SERVER_STATE_BADAUTH;
                    MSCHAPSendStatus (pAuthPtr, CHAP_FALSE,
                                      (const UINT1 *) "691");
                    LCPAuthProcessFailed (pIf);
                    break;

                default:
                    LCPAuthProcessFailed (pIf);
                    break;
            }
    /*****************/

            break;                /* For Access Reject */

        case ACCESS_CHALLENGE:

            i4RetVal = (INT4)STRLEN(pInterface->Reply_Message);
            if (i4RetVal<= MAX_MSG_SIZE)
            {
                STRNCPY (Msg, pInterface->Reply_Message,i4RetVal);
            }
    /*****************/
            switch (pAuthPtr->ServerProt)
            {
                case PAP_PROTOCOL:
                    PppRadSendPapStatus (pIf, PAP_AUTH_NAK, Msg,
                                         pIf->RadInfo.PppAuthId);
                    pAuthPtr->ServerInfo.ServerPap.State =
                        PAP_SERVER_STATE_LISTEN;
                    break;
                case EAP_PROTOCOL:
                    pServerEAPMD5->ChallengeId =
                        pInterface->au1EapPkt[EAP_CODE_FIELD_LEN];
                    pServerEAPMD5->ChallengeValueLength =
                        pInterface->au1EapPkt[EAP_HDR_LEN + EAP_TYPE_FIELD_LEN];
                    MEMCPY (&pServerEAPMD5->ChallengeValue,
                            &pInterface->au1EapPkt[EAP_HDR_LEN +
                                                   EAP_TYPE_FIELD_LEN +
                                                   VALUE_SIZE_FIELD_LEN],
                            pServerEAPMD5->ChallengeValueLength);

                    u1_StateLen = pInterface->u1StateLen;
                    pAuthPtr->pIf->RadInfo.u1EapStateLen = u1_StateLen;
                    MEMSET (pAuthPtr->pIf->RadInfo.EapState, 0, MAX_STATE_LEN);
                    MEMCPY (pAuthPtr->pIf->RadInfo.EapState, pInterface->State,
                            u1_StateLen);
                    MEMCPY
                        (&u2_EapPktLen,
                         &pInterface->au1EapPkt[EAP_CODE_FIELD_LEN +
                                                EAP_ID_FIELD_LEN], 2);
                    u2_EapPktLen = OSIX_NTOHS (u2_EapPktLen);
                    if (pSecret != NULL)
                    {
                        pSecret->IdentityLen =
                            (UINT1) (u2_EapPktLen -
                                     (EAP_HDR_LEN + EAP_TYPE_FIELD_LEN +
                                      pServerEAPMD5->ChallengeValueLength));

                        MEMCPY
                            (pSecret->Identity,
                             &pInterface->au1EapPkt[EAP_HDR_LEN +
                                                    EAP_TYPE_FIELD_LEN +
                                                    VALUE_SIZE_FIELD_LEN +
                                                    pServerEAPMD5->
                                                    ChallengeValueLength],
                             pSecret->IdentityLen);

                        pServerEAPMD5->pEAPMD5CurrentSecret = pSecret;
                    }
                    pAuthPtr->ServerInfo.ServerEap.State =
                        EAP_SERVER_STATE_AUTH_TYPE;
                    pAuthPtr->ServerInfo.ServerEap.AuthTypeOptions.ServerEAPMD5.
                        State = EAPMD5_SERVER_STATE_INITIAL_CHAL;
                    EAPMD5SendChallenge (pAuthPtr);

                    break;

                default:
                    LCPAuthProcessFailed (pIf);
                    break;
            }
    /*****************/

            break;                /* For Access Challenge */

        case RAD_TIMEOUT:
            STRCPY (Msg,
                    "Sorry.RADIUS Server Not Avaialble.Try After Sometime.");

        /*****************************/
            switch (pAuthPtr->ServerProt)
            {

                case PAP_PROTOCOL:
                    PppRadSendPapStatus (pIf, PAP_AUTH_NAK, Msg,
                                         pIf->RadInfo.PppAuthId);
                    LCPAuthProcessFailed (pIf);
                    break;

                case CHAP_PROTOCOL:
                    CHAPSendStatus (pAuthPtr, CHAP_FALSE);
                    LCPAuthProcessFailed (pIf);
                    break;
                case EAP_PROTOCOL:
                    EAPSendStatus (pAuthPtr, EAP_FAILURE);
                    LCPAuthProcessFailed (pIf);
                    break;

                case MSCHAP_PROTOCOL:
                    MSCHAPSendStatus (pAuthPtr, CHAP_FALSE,
                                      (const UINT1 *) "691");
                    LCPAuthProcessFailed (pIf);
                    break;

                default:
                    LCPAuthProcessFailed (pIf);
                    break;
            }

            break;
        default:
            break;
    }

    MemReleaseMemBlock (PPP_SECRET_MEMPOOL_ID, (UINT1 *) pSecret);
/* calling function provided by radius to free the allocated memory
 * for pInterface */
    UtlShMemFreeRadInterface ((UINT1 *) pInterface);

}

/**************************************************************************/
VOID
PppUpdateAuthorizationParams (tPPPIf * pIf, tRadInterface * pInterface)
{

    UINT4               u4PeerIpAddr;
    INT4                i4Temp;
    UINT1               au1UserName[RAD_IF_MEMBER_MAX_SIZE];

    MEMSET (au1UserName, 0, RAD_IF_MEMBER_MAX_SIZE);
    pIf->RadInfo.AcctNeeded = PPP_NO;
    u4PeerIpAddr = pInterface->Framed_IP_Address;
    switch (u4PeerIpAddr)
    {
        case 0xffffffff:
            break;
        case 0xfffffffe:
            break;
        default:
            gu1IPCPAddrModeSelector = IPCP_ADDRESS_MODE_RADIUS;

            ((tIPCPIf *) (pIf->pIpcpPtr))->IpcpGSEM.pNegFlagsPerIf[IP_ADDR_IDX].
                FlagMask |= FORCED_TO_REQUEST_SET;
            ((tIPCPIf *) (pIf->pIpcpPtr))->IpcpOptionsAllowedForPeer.IPAddress =
                u4PeerIpAddr;
    }                            /* for framed IP Address */

    switch (pInterface->Framed_Compression)
    {
        case COMP_VJ_TCP_IP:
            if (VJCompAvailable == PPP_YES)
            {

                ((tIPCPIf *) pIf->pIpcpPtr)->IpcpGSEM.pNegFlagsPerIf
                    [IP_COMP_IDX].FlagMask |=
                    DESIRED_SET | ALLOWED_FOR_PEER_SET;
                ((tIPCPIf *) pIf->pIpcpPtr)->IpcpGSEM.pNegFlagsPerIf
                    [IP_COMP_IDX].FlagMask |= ACKED_BY_PEER_SET;
            }

            break;
        default:
            break;
    }                            /* for framed compression */

        if ((i4Temp = (INT4) STRLEN (pInterface->Class)) != 0)
        {

            pIf->RadInfo.Class = au1UserName;
            if (pIf->RadInfo.Class == NULL)
            {
                PRINT_MEM_ALLOC_FAILURE;
                return;
            }

            STRCPY (pIf->RadInfo.Class, pInterface->Class);
            UNUSED_PARAM (i4Temp);
        }
}

/************************************************************/
VOID
PppRadSendPapStatus (tPPPIf * pIf, UINT1 Code, UINT1 *Msg, UINT1 Id)
{
    t_MSG_DESC         *pOutBuf;
    UINT2               Len = 0;
    UINT2               MsgLen = 0;

    Len = CP_HDR_LEN + MSG_LEN_FIELD_LEN;
    MsgLen = (UINT2) STRLEN (Msg);
    Len = (UINT2) (Len + MsgLen);

    pOutBuf =
        (t_MSG_DESC *) (VOID *)
        ALLOCATE_BUFFER ((UINT4) (PPP_INFO_OFFSET + Len),
                         (UINT4) PPP_INFO_OFFSET);
    if (pOutBuf == NULL)
        return;

    FILL_PKT_HEADER (pOutBuf, Code, Id, Len);
    APPEND1BYTE (pOutBuf, MsgLen);
    APPENDSTR (pOutBuf, Msg, MsgLen);
    PPPLLITxPkt (pIf, pOutBuf, Len, PAP_PROTOCOL);
    return;
}

#endif
