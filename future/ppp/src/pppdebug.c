/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppdebug.c,v 1.5 2014/12/16 11:47:26 siva Exp $
 *
 * Description: Debug/Trace Routines for PPP.
 *
 *******************************************************************/
#include "pppcom.h"
#include "gsemdefs.h"
#include "globexts.h"
#include "pppproto.h"
#include "pppipcp.h"
#include "pppmuxcp.h"
#include "pppipv6cp.h"
#include "pppmplscp.h"

#define PPP_MAX_DEBUG_STR_LEN 255

VOID
PrintOutStat (tPPPIf * pIf)
{

    PPP_UNUSED (pIf);
    PPP_TRC (DUMP, "\n_______________________________________________\n");
    PPP_TRC (DUMP, "\n    Out BUFFER  STATISTICS             ");
    PPP_TRC (DUMP, "\n----------------------------------------------\n");
    PPP_TRC1 (DUMP, "  OutAlloc                    : %d\n", OutAllocCounter);
    PPP_TRC1 (DUMP, "  OutRel                      : %d\n", OutRelCounter);
    PPP_TRC1 (DUMP, "  RecdFromHL                  : %d\n", NoOfPktsRcvdFromHL);
    PPP_TRC1 (DUMP, "  NoOfPktsAcked               : %d\n", NoOfPktsAcked);
    PPP_TRC1 (DUMP, "  TotalOutNUniPkt         : %d\n", TotalOutNUniPkts);
    PPP_TRC (DUMP, "\n----------------------------------------------\n");

    return;
}

VOID
PrintInStat (tPPPIf * pIf)
{
    PPP_TRC (DUMP, "\n_______________________________________________\n");
    PPP_TRC (DUMP, "\n    In Buffer STATISTICS             ");
    PPP_TRC (DUMP, "\n----------------------------------------------\n");
    PPP_TRC1 (DUMP, "    InAlloc              : %d\n", InAllocCounter);
    PPP_TRC1 (DUMP, "    SentToHL             : %d\n", NoOfPktsSentToHL);
    PPP_TRC1 (DUMP, "    InRel                : %d\n", InRelCounter);
    PPP_TRC1 (DUMP, "  NoOfPktsAcked          : %d\n", NoOfPktsAcked);
    PPP_TRC1 (DUMP, "  SentToHL+InRel => %d\n",
              InRelCounter + NoOfPktsSentToHL);
    PPP_TRC1 (DUMP, "  TotalInNUniPkt         : %d\n", TotalInNUniPkts);
    PPP_TRC (DUMP, "----------------------------------------------\n");
    PPP_TRC1 (DUMP, "\n InLQRs               :  %ld  ",
              pIf->LLHCounters.InLQRs);
    PPP_TRC1 (DUMP, "\n InUniPackets           :  %ld  ",
              pIf->LLHCounters.InNUniPackets);
    PPP_TRC1 (DUMP, "\n InOctets               :  %ld  ",
              pIf->LLHCounters.InOctets);
    PPP_TRC1 (DUMP, "\n InErrors               :  %ld  ",
              pIf->LLHCounters.InErrors);
    PPP_TRC1 (DUMP, "\n InGoodOctets           :  %ld  ",
              pIf->LLHCounters.InGoodOctets);
    PPP_TRC1 (DUMP, "\n InDiscards             :  %ld  ",
              pIf->LLHCounters.InDiscards);
    PPP_TRC1 (DUMP, "\n BadAddressCounter      :  %ld  ",
              pIf->LLHCounters.BadAddressCounter);
    PPP_TRC (DUMP, "\n-----------------------------------------------\n");
}

VOID
PrintBuffer (t_MSG_DESC * pMsg, UINT2 Length)
{
    PPP_UNUSED (pMsg);
    PPP_UNUSED (Length);

}

VOID
PrintState (UINT1 State)
{
    PPP_TRC (EVENT_TRC, "[State]: ");
    switch (State)
    {

        case INITIAL:
            PPP_TRC (EVENT_TRC, "\t\t\tINITIAL ");
            break;
        case STARTING:
            PPP_TRC (EVENT_TRC, "\t\t\tSTARTING");
            break;
        case CLOSED:
            PPP_TRC (EVENT_TRC, "\t\t\tCLOSED  ");
            break;
        case STOPPED:
            PPP_TRC (EVENT_TRC, "\t\t\tSTOPPED ");
            break;
        case CLOSING:
            PPP_TRC (EVENT_TRC, "\t\t\tCLOSING ");
            break;
        case STOPPING:
            PPP_TRC (EVENT_TRC, "\t\t\tSTOPPING");
            break;
        case REQSENT:
            PPP_TRC (EVENT_TRC, "\t\t\tREQSENT");
            break;
        case ACKRCVD:
            PPP_TRC (EVENT_TRC, "\t\t\tACKRCVD");
            break;
        case ACKSENT:
            PPP_TRC (EVENT_TRC, "\t\t\tACKSENT");
            break;
        case OPENED:
            PPP_TRC (EVENT_TRC, "\t\t\tOPENED");
            break;
        default:
            PPP_TRC (EVENT_TRC, "\t\t\tPRINTSTATE DEFAULT");
            break;
    }
}

VOID
PrintEvent (UINT1 Event)
{
    PPP_TRC (EVENT_TRC, "[Event]: ");
    switch (Event)
    {

        case PPP_UP:
            PPP_TRC (EVENT_TRC, "\t\t\tUP");
            break;
        case PPP_DOWN:
            PPP_TRC (EVENT_TRC, "\t\t\tDOWN");
            break;
        case OPEN:
            PPP_TRC (EVENT_TRC, "\t\t\tOPEN");
            break;
        case CLOSE:
            PPP_TRC (EVENT_TRC, "\t\t\tCLOSE");
            break;
        case TO_PLUS:
            PPP_TRC (EVENT_TRC, "\t\t\tTO_PLUS");
            break;
        case TO_MINUS:
            PPP_TRC (EVENT_TRC, "\t\t\tTO_MINUS");
            break;

        case RCR_PLUS:
            PPP_TRC (EVENT_TRC, "\t\t\tRCR_PLUS");
            break;
        case RCR_MINUS:
            PPP_TRC (EVENT_TRC, "\t\t\tRCR_MINUS");
            break;
        case RCA:
            PPP_TRC (EVENT_TRC, "\t\t\tRCA");
            break;
        case RCN:
            PPP_TRC (EVENT_TRC, "\t\t\tRCN");
            break;

        case RTR:
            PPP_TRC (EVENT_TRC, "\t\t\tRTR");
            break;
        case RTA:
            PPP_TRC (EVENT_TRC, "\t\t\tRTA");
            break;
        case RUC:
            PPP_TRC (EVENT_TRC, "\t\t\tRUC");
            break;

        case RXJ_PLUS:
            PPP_TRC (EVENT_TRC, "\t\t\tRXJ_PLUS");
            break;
        case RXJ_MINUS:
            PPP_TRC (EVENT_TRC, "\t\t\tRXJ_MINUS");
            break;
        case RXR:
            PPP_TRC (EVENT_TRC, "\t\t\tRXR");
            break;

        case RST:
            PPP_TRC (EVENT_TRC, "\t\t\tRST");
            break;
        case TXD:
            PPP_TRC (EVENT_TRC, "\t\t\tTXD");
            break;
        case RXD:
            PPP_TRC (EVENT_TRC, "\t\t\tRXD");
            break;
        default:
            PPP_TRC (EVENT_TRC, "\t\t\tPRINTEVENT DEFAULT");
            break;

    }
}

VOID
PrintFlags (tGSEM * pGSEM)
{
    PPP_UNUSED (pGSEM);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : PppPrintLcpOptions                                         */
/*                                                                           */
/* Description  : This function is called to Print the LCP Options Negotiated*/
/*                                                                           */
/*                                                                           */
/* Input        : pPppAlarm                                                  */
/*                                                                           */
/*                                                                           */
/* Output       : none                                                       */
/*                                                                           */
/* Returns      :                                                            */
/*                                                                           */
/*****************************************************************************/
static VOID
PppPrintLcpOptions (tPPPIf * pIf)
{
    char                au1Str[PPP_MAX_DEBUG_STR_LEN];
    INT1                i1BoolStr[16];
    tLCPOptions        *pOptions = NULL;
    UINT4               u4Index, u4Count;
    UINT1               u1Mask;
    UINT4               u4Flag;

    for (u4Count = 0; u4Count < 2; u4Count++)
    {
        if (u4Count)
        {
            UtlTrcPrint
                ("\n\n\nLCP Options Negotiated in the LOCAL TO REMOTE direction:\n");
            UtlTrcPrint
                ("========================================================\n");
            pOptions = &pIf->LcpOptionsAckedByPeer;
            u1Mask = ACKED_BY_PEER_MASK;
        }
        else
        {
            UtlTrcPrint
                ("\n\n\nLCP Options Negotiated in the REMOTE TO LOCAL direction:\n");
            UtlTrcPrint
                ("=========================================================\n");
            pOptions = &pIf->LcpOptionsAckedByLocal;
            u1Mask = ACKED_BY_LOCAL_MASK;
        }

        for (u4Index = 0; u4Index < MAX_LCP_OPT_TYPES; u4Index++)
        {
            u4Flag = 1;
            if (pIf->LcpGSEM.pNegFlagsPerIf[u4Index].FlagMask & u1Mask)
            {
                STRCPY (i1BoolStr, "PPP_YES");
            }
            else
            {
                STRCPY (i1BoolStr, " PPP_NO");
            }

            switch (u4Index)
            {
                case MRU_IDX:
                    MEMSET (au1Str, 0, PPP_MAX_DEBUG_STR_LEN);
                    SNPRINTF (au1Str, PPP_MAX_DEBUG_STR_LEN,
                              "         MRU: %s\tValue: %d\n",
                              i1BoolStr, pOptions->MRU);
                    break;

                case ACCM_IDX:
                    MEMSET (au1Str, 0, PPP_MAX_DEBUG_STR_LEN);
                    SNPRINTF (au1Str, PPP_MAX_DEBUG_STR_LEN,
                              "    AsyncMap: %s\tValue: %x%x%x%x\n",
                              i1BoolStr, pOptions->AsyncMap[0],
                              pOptions->AsyncMap[1],
                              pOptions->AsyncMap[2], pOptions->AsyncMap[3]);
                    break;

                case MAGIC_IDX:
                    MEMSET (au1Str, 0, PPP_MAX_DEBUG_STR_LEN);
                    SNPRINTF (au1Str, PPP_MAX_DEBUG_STR_LEN,
                              "Magic Number: %s\tValue: %ld\n",
                              i1BoolStr, pOptions->MagicNumber);
                    break;

                case FCS_IDX:
                    MEMSET (au1Str, 0, PPP_MAX_DEBUG_STR_LEN);
                    SNPRINTF (au1Str, PPP_MAX_DEBUG_STR_LEN,
                              "     FCSSize: %s\tValue: %d\n",
                              i1BoolStr, pOptions->FCSSize);
                    break;

                case PADDING_IDX:
                    MEMSET (au1Str, 0, PPP_MAX_DEBUG_STR_LEN);
                    SNPRINTF (au1Str, PPP_MAX_DEBUG_STR_LEN,
                              "    MaxPadVal: %s\tValue: %d\n",
                              i1BoolStr, pOptions->MaxPadVal);
                    break;
                default:
                    u4Flag = 0;
                    break;

            }
            if (u4Flag)
                UtlTrcPrint (au1Str);

        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : PppPrintIpcpOptions                                        */
/*                                                                           */
/* Description  : This function is called to Print the IPCP Options Negotiated*/
/*                                                                           */
/*                                                                           */
/* Input        : pPppAlarm                                                  */
/*                                                                           */
/*                                                                           */
/* Output       : none                                                       */
/*                                                                           */
/* Returns      :                                                            */
/*                                                                           */
/*****************************************************************************/
static VOID
PppPrintIpcpOptions (tPPPIf * pIf)
{
    char                au1Str[PPP_MAX_DEBUG_STR_LEN];
    INT1                i1BoolStr[16];
    tIPCPOptions       *pOptions = NULL;
    tIPCPIf            *pIpcpIf;
    UINT4               u4Index, u4Count;
    UINT1               u1Mask;
    UINT4               u4Flag;

    pIpcpIf = (tIPCPIf *) pIf->pIpcpPtr;
    if (pIpcpIf == NULL)
    {
        return;
    }

    if (pIpcpIf->IpcpGSEM.CurrentState != OPENED)
    {
        return;
    }
    else
    {
        UtlTrcPrint (" IPCP Current State :");
        PrintState (pIpcpIf->IpcpGSEM.CurrentState);
    }

    /* IPCP Options Negotiated from Local to Remote */

    for (u4Count = 0; u4Count < 2; u4Count++)
    {
        if (u4Count)
        {
            UtlTrcPrint
                ("\n\n\nIPCP Options Negotiated in the LOCAL TO REMOTE direction:\n");
            UtlTrcPrint
                ("=========================================================\n");
            pOptions = &pIpcpIf->IpcpOptionsAckedByPeer;
            u1Mask = ACKED_BY_PEER_MASK;
        }
        else
        {
            UtlTrcPrint
                ("\n\n\nIPCP Options Negotiated in the REMOTE TO LOCAL direction:\n");
            UtlTrcPrint
                ("=========================================================\n");

            pOptions = &pIpcpIf->IpcpOptionsAckedByLocal;
            u1Mask = ACKED_BY_LOCAL_MASK;
        }
        for (u4Index = 0; u4Index < MAX_IPCP_OPT_TYPES; u4Index++)
        {
            u4Flag = 1;
            if (pIpcpIf->IpcpGSEM.pNegFlagsPerIf[u4Index].FlagMask & u1Mask)
            {
                STRCPY (i1BoolStr, "PPP_YES");
            }
            else
            {
                STRCPY (i1BoolStr, " PPP_NO");
            }
            switch (u4Index)
            {
                case IP_COMP_IDX:
                    MEMSET (au1Str, 0, PPP_MAX_DEBUG_STR_LEN);
                    SNPRINTF (au1Str, PPP_MAX_DEBUG_STR_LEN,
                              "Compression Protocol: %s\t Value: %d\n",
                              i1BoolStr, pOptions->u2CompProto);
                    UtlTrcPrint (au1Str);
                    MEMSET (au1Str, 0, PPP_MAX_DEBUG_STR_LEN);
                    SNPRINTF (au1Str, PPP_MAX_DEBUG_STR_LEN,
                              "Comp Slot Id          : %s\t Value: %d\n",
                              i1BoolStr, pOptions->CompInfo.VJInfo.CompSlotId);
                    UtlTrcPrint (au1Str);
                    MEMSET (au1Str, 0, PPP_MAX_DEBUG_STR_LEN);
                    SNPRINTF (au1Str, PPP_MAX_DEBUG_STR_LEN,
                              "Max Slot Id      : %s\t Value: %d\n",
                              i1BoolStr, pOptions->CompInfo.VJInfo.MaxSlotId);
                    UtlTrcPrint (au1Str);
                    break;

                case IP_ADDR_IDX:
                    MEMSET (au1Str, 0, PPP_MAX_DEBUG_STR_LEN);
                    SNPRINTF (au1Str, PPP_MAX_DEBUG_STR_LEN,
                              "IPAddress: %s\t Value: %ld\n", i1BoolStr,
                              pOptions->IPAddress);
                    break;
                    /*
                       case PRIMARY_DNS_ADDR_IDX:
                       sprintf(au1Str,"PrimaryDNSAddress: %s\t Va\nlue: %ld",i1BoolStr,pOptions->PrimaryDNSAddress);
                       break;
                       case SECONDARY_DNS_ADDR_IDX:
                       sprintf(au1Str,"SecondaryDNSAddress: %s\t Value: %ld",i1BoolStr,pOptions->SecondaryDNSAddress);
                       break;
                       case PRIMARY_NBNS_ADDR_IDX:
                       sprintf(au1Str,"PrimaryNBNSAddress: %s\t Value: %ld",i1BoolStr,pOptions->PrimaryNBNSAddress);
                       break;
                       case SECONDARY_NBNS_ADDR_IDX:
                       sprintf(au1Str,"SecondaryNBNSAddress: %s\t Value: %ld",i1BoolStr,pOptions->SecondaryNBNSAddress);
                       break;
                     */

                default:
                    u4Flag = 0;
                    break;
            }
            if (u4Flag)
                UtlTrcPrint (au1Str);

        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : PppPrintIp6cpOptions                                        */
/*                                                                           */
/* Description  : This function is called to Print the IP6CP Options Negotiated*/
/*                                                                           */
/*                                                                           */
/* Input        : pPppAlarm                                                  */
/*                                                                           */
/*                                                                           */
/* Output       : none                                                       */
/*                                                                           */
/* Returns      :                                                            */
/*                                                                           */
/*****************************************************************************/
static VOID
PppPrintIp6cpOptions (tPPPIf * pIf)
{
    char                au1Str[PPP_MAX_DEBUG_STR_LEN];
    INT1                i1BoolStr[16];
    tIP6CPOptions      *pOptions = NULL;
    tIP6CPIf           *pIp6cpIf;
    UINT4               u4Index, u4Count;
    UINT1               u1Mask;
    UINT4               u4Flag;

    pIp6cpIf = (tIP6CPIf *) pIf->pIp6cpPtr;

    if (pIp6cpIf == NULL)
    {
        return;
    }

    if (pIp6cpIf->Ip6cpGSEM.CurrentState != OPENED)
    {
        return;
    }
    else
    {
        UtlTrcPrint ("IPv6CP  SEM State :");
        PrintState (pIp6cpIf->Ip6cpGSEM.CurrentState);
    }
    for (u4Count = 0; u4Count < 2; u4Count++)
    {
        if (u4Count)
        {
            UtlTrcPrint
                ("\n\n\nIPv6CP Options Negotiated in the LOCAL TO REMOTE direction:\n");
            UtlTrcPrint
                ("=========================================================\n");
            pOptions = &pIp6cpIf->Ip6cpOptionsAckedByPeer;
            u1Mask = ACKED_BY_PEER_MASK;
        }
        else
        {
            UtlTrcPrint
                ("\n\n\nIPv6CP Options Negotiated in the REMOTE TO LOCAL direction:\n");
            UtlTrcPrint
                ("===========================================================\n");

            pOptions = &pIp6cpIf->Ip6cpOptionsAckedByLocal;
            u1Mask = ACKED_BY_LOCAL_MASK;
        }
        for (u4Index = 0; u4Index <= MAX_IP6CP_OPT_TYPES; u4Index++)
        {
            u4Flag = 1;
            if (pIp6cpIf->Ip6cpGSEM.pNegFlagsPerIf[u4Index].FlagMask & u1Mask)
            {
                STRCPY (i1BoolStr, "PPP_YES");
            }
            else
            {
                STRCPY (i1BoolStr, " PPP_NO");
            }
            switch (u4Index)
            {
                case IP6_INTFID_IDX:
                    MEMSET (au1Str, 0, PPP_MAX_DEBUG_STR_LEN);
                    SNPRINTF (au1Str, PPP_MAX_DEBUG_STR_LEN,
                              "Interface Id: %s\t Value: %x %x %x %x %x %x %x %x \n",
                              i1BoolStr,
                              pOptions->InterfaceId[0],
                              pOptions->InterfaceId[1],
                              pOptions->InterfaceId[2],
                              pOptions->InterfaceId[3],
                              pOptions->InterfaceId[4],
                              pOptions->InterfaceId[5],
                              pOptions->InterfaceId[6],
                              pOptions->InterfaceId[7]);
                    break;

                case IP6_COMP_IDX:
                    MEMSET (au1Str, 0, PPP_MAX_DEBUG_STR_LEN);
                    SNPRINTF (au1Str, PPP_MAX_DEBUG_STR_LEN,
                              "Ip6CompProtocol: %s\t Value: %d\n",
                              i1BoolStr, pOptions->Ip6CompProtocol);
                    UtlTrcPrint (au1Str);
                    MEMSET (au1Str, 0, PPP_MAX_DEBUG_STR_LEN);
                    SNPRINTF (au1Str, PPP_MAX_DEBUG_STR_LEN,
                              "Tcp Space           : %s\t Value: %d\n",
                              i1BoolStr, pOptions->IPHCInfo.u2TcpSpace);
                    UtlTrcPrint (au1Str);
                    MEMSET (au1Str, 0, PPP_MAX_DEBUG_STR_LEN);
                    SNPRINTF (au1Str, PPP_MAX_DEBUG_STR_LEN,
                              "Non Tcp Space       : %s\t Value: %d\n",
                              i1BoolStr, pOptions->IPHCInfo.u2NonTcpSpace);
                    UtlTrcPrint (au1Str);
                    MEMSET (au1Str, 0, PPP_MAX_DEBUG_STR_LEN);
                    SNPRINTF (au1Str, PPP_MAX_DEBUG_STR_LEN,
                              "Full Max Period     : %s\t Value: %d\n",
                              i1BoolStr, pOptions->IPHCInfo.u2FMaxPeriod);
                    UtlTrcPrint (au1Str);
                    MEMSET (au1Str, 0, PPP_MAX_DEBUG_STR_LEN);
                    SNPRINTF (au1Str, PPP_MAX_DEBUG_STR_LEN,
                              "Full Max Time       : %s\t Value: %d\n",
                              i1BoolStr, pOptions->IPHCInfo.u2FMaxTime);
                    UtlTrcPrint (au1Str);
                    MEMSET (au1Str, 0, PPP_MAX_DEBUG_STR_LEN);
                    SNPRINTF (au1Str, PPP_MAX_DEBUG_STR_LEN,
                              "Max Header          : %s\t Value: %d\n",
                              i1BoolStr, pOptions->IPHCInfo.u2MaxHdr);
                    UtlTrcPrint (au1Str);
                    MEMSET (au1Str, 0, PPP_MAX_DEBUG_STR_LEN);
                    SNPRINTF (au1Str, PPP_MAX_DEBUG_STR_LEN,
                              "Rtp Compression     : %s\t Value: %d\n",
                              i1BoolStr, pOptions->IPHCInfo.u2RtpSubOptFlag);
                    break;
                default:
                    u4Flag = 0;
                    break;
            }
            if (u4Flag)
                UtlTrcPrint (au1Str);

        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : PppPrintMuxcpOptions                                        */
/*                                                                           */
/* Description  : This function is called to Print the MUXCP Options Negotiated*/
/*                                                                           */
/*                                                                           */
/* Input        : pPppAlarm                                                  */
/*                                                                           */
/*                                                                           */
/* Output       : none                                                       */
/*                                                                           */
/* Returns      :                                                            */
/*                                                                           */
/*****************************************************************************/
static VOID
PppPrintMuxcpOptions (tPPPIf * pIf)
{
    char                au1Str[PPP_MAX_DEBUG_STR_LEN];
    INT1                i1BoolStr[16];
    tMuxCPOptions      *pOptions = NULL;
    tMuxCPIf           *pMuxcpIf;
    UINT4               u4Index, u4Count;
    UINT1               u1Mask;
    UINT4               u4Flag;

    pMuxcpIf = (tMuxCPIf *) pIf->pMuxcpPtr;

    if (pMuxcpIf == NULL)
    {
        return;
    }

    if (pMuxcpIf->MuxCPGSEM.CurrentState != OPENED)
    {
        return;
    }
    else
    {
        UtlTrcPrint ("MUX CP  SEM State :");
        PrintState (pMuxcpIf->MuxCPGSEM.CurrentState);
    }

    for (u4Count = 0; u4Count < 2; u4Count++)
    {
        if (u4Count)
        {
            UtlTrcPrint
                ("\n\n\nMux CP Options Negotiated in the LOCAL TO REMOTE direction:\n");
            UtlTrcPrint
                ("===========================================================\n");
            pOptions = &pMuxcpIf->MuxCPOptionsAckedByPeer;
            u1Mask = ACKED_BY_PEER_MASK;
        }
        else
        {
            UtlTrcPrint
                ("\n\n\nMux CP Options Negotiated in the REMOTE TO LOCAL direction:\n");
            UtlTrcPrint
                ("===========================================================\n");

            pOptions = &pMuxcpIf->MuxCPOptionsAckedByLocal;
            u1Mask = ACKED_BY_LOCAL_MASK;
        }
        for (u4Index = 0; u4Index < MAX_MUXCP_OPT_TYPES; u4Index++)
        {
            u4Flag = 1;
            if (pMuxcpIf->MuxCPGSEM.pNegFlagsPerIf[u4Index].FlagMask & u1Mask)
            {
                STRCPY (i1BoolStr, "PPP_YES");
            }
            else
            {
                STRCPY (i1BoolStr, " PPP_NO");
            }
            switch (u4Index)
            {
                case MUXCP_DEFPID_IDX:
                    MEMSET (au1Str, 0, PPP_MAX_DEBUG_STR_LEN);
                    SNPRINTF (au1Str, PPP_MAX_DEBUG_STR_LEN,
                              "u2CompProto: %s\t Value: %d\n", i1BoolStr,
                              pOptions->u2DefPID);
                    break;
                default:
                    u4Flag = 0;
                    break;
            }
            if (u4Flag)
                UtlTrcPrint (au1Str);

        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : PppPrintIfInfo                                             */
/*                                                                           */
/* Description  : This function is called to Print the SEM States of LCP/IPCP*/
/*                /IPv6CP/MPLSCP/MuXCP                                       */
/*                                                                           */
/* Input        : u4IfIndex                                                  */
/*                                                                           */
/*                                                                           */
/* Output       : none                                                       */
/*                                                                           */
/* Returns      :                                                            */
/*                                                                           */
/*****************************************************************************/

VOID
PppPrintIfInfo (UINT4 u4IfIndex)
{
    tPPPIf             *pIf = NULL;
    char                au1Str[PPP_MAX_DEBUG_STR_LEN];
    UINT4               u4MpIfCnt;

    pIf = PppGetIfPtr (u4IfIndex);

    if (pIf == NULL)
    {
        MEMSET (au1Str, 0, PPP_MAX_DEBUG_STR_LEN);
        SNPRINTF (au1Str, PPP_MAX_DEBUG_STR_LEN,
                  "\n\nInterface %ld doesnt exit\n\n", u4IfIndex);
        UtlTrcPrint (au1Str);
        return;
    }

    if (pIf->BundleFlag == BUNDLE)
    {
        u4MpIfCnt = SLL_COUNT (&pIf->MPInfo.BundleInfo.MemberList);
        MEMSET (au1Str, 0, PPP_MAX_DEBUG_STR_LEN);
        SNPRINTF (au1Str, PPP_MAX_DEBUG_STR_LEN,
                  "\n\nInterface %ld is MP Interface and has %ld PPP Links\n\n",
                  u4IfIndex, u4MpIfCnt);
        UtlTrcPrint (au1Str);
    }
    else if (pIf->MPInfo.MemberInfo.PartOfBundle == PPP_YES)
    {
        MEMSET (au1Str, 0, PPP_MAX_DEBUG_STR_LEN);
        SNPRINTF (au1Str, PPP_MAX_DEBUG_STR_LEN,
                  "\n\nInterface %ld is part of an MP Bundle\n\n", u4IfIndex);
        UtlTrcPrint (au1Str);
    }
    else
    {
        MEMSET (au1Str, 0, PPP_MAX_DEBUG_STR_LEN);
        SNPRINTF (au1Str, PPP_MAX_DEBUG_STR_LEN,
                  "\n\nInterface %ld is PPP Interface\n\n", u4IfIndex);
        UtlTrcPrint (au1Str);
    }

    MEMSET (au1Str, 0, PPP_MAX_DEBUG_STR_LEN);
    SNPRINTF (au1Str, PPP_MAX_DEBUG_STR_LEN,
              "\nAdmin Status of PPP Interface is %s\n\n",
              (pIf->LcpStatus.AdminStatus == STATUS_UP) ? "UP" : "DOWN");
    UtlTrcPrint (au1Str);

    MEMSET (au1Str, 0, PPP_MAX_DEBUG_STR_LEN);
    SNPRINTF (au1Str, PPP_MAX_DEBUG_STR_LEN,
              "\nOper Status of PPP Interface is %s\n\n",
              (pIf->LcpStatus.OperStatus == STATUS_UP) ? "UP" : "DOWN");
    UtlTrcPrint (au1Str);

    PppPrintLcpOptions (pIf);
    PppPrintIpcpOptions (pIf);
    PppPrintIp6cpOptions (pIf);
    PppPrintMuxcpOptions (pIf);

}
