/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppplli.c,v 1.7 2014/12/16 11:47:26 siva Exp $
 *
 * Description:This files contains the interface procedures of the 
 *             LLI module.                            
 *
 *******************************************************************/

#include     "pppcom.h"
#include     "lcpdefs.h"
#include     "frmdefs.h"

#include     "gsemdefs.h"
#include     "genexts.h"

#include "mpdefs.h"

#include "pppeccmn.h"
#include "pppccp.h"

#include "pppeccmn.h"
#include "pppecp.h"

#include "authcom.h"
#include "lqmcom.h"

#include "pppproto.h"
#include "globexts.h"
#include "hdlcprot.h"
#include "cfa.h"
#include "ppp.h"
#include "genproto.h"
#include "frmprots.h"
#include "pppmemmac.h"
#define PROT_LEN                2
void
 
 
 
         DumpLcpData (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 Offset, UINT1 u1Dir);
void
 
 
 
         DumpIpcpData (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 Offset, UINT1 u1Dir);
void
 
 
 
         DumpBcpData (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 Offset, UINT1 u1Dir);
INT4                PPPRemovePadding (t_MSG_DESC ** pInBuf, UINT2 BufLen,
                                      UINT2 *);
/*********************************************************************
*   Function Name   :   PPPLLICompressEncryptPkt
*   Description     :
*       This function is used to PF Compress, Compress through CCP and
*   encrypt through ECP.
*   Parameter(s)    :
*               pIf -   Points to the Interface structure that corressponds
*                       the  PDU transmission.
*           pOutPDU -   Points to the final transmit PDU, with Protocol Field
*                       as a  starting  byte.
*           Length  -   Length of the message.
*       Protocol    -   Protocol field of the pkt.
*   Return Values   :   OK/NOT_OK
*********************************************************************/
INT1
PPPLLICompressEncryptPkt (tPPPIf * pIf, t_MSG_DESC ** pOutPDU, UINT2 Length,
                          UINT2 Protocol)
{
    UINT1               PFCEnableFlag;

    PFCEnableFlag = PPP_NO;
    PPP_UNUSED (Length);

    /* Set PFC if negotiated, for data packets only */

    /*  DataPkt Index checking is removed - MPLS data packets are having protcol
     *  field > 255.
     *  
     if (((Protocol & DATA_PKT_MASK) == 0) || (OutDataPktIndex >= DataIndex))
     *
     */
    if ((Protocol & DATA_PKT_MASK) == 0)
    {
        if ((pIf->LcpGSEM.
             pNegFlagsPerIf[PFC_IDX].FlagMask & ACKED_BY_LOCAL_MASK)
            && ((Protocol & DATA_PKT_MASK) == 0))
        {
            PFCEnableFlag = PPP_YES;
        }

        /*
           Encapsulate the PID only when it is not done already.
           When MP calls this function, the protocol field will be compressed.
         */
        if (Protocol != 0)
        {
            PPPLLIPutPID (*pOutPDU, PFCEnableFlag, Protocol);
        }

#ifdef BAP
        if (ProtSEMTable[OutDataPktIndex].ProtID != BAP_PROTOCOL)
        {
#endif
            /* If ECP or CCP is being supported, compress and/or encrypt 
             * the packet. */
            if (PPPLLIEncapPID (pIf, pOutPDU, PFCEnableFlag) == NOT_OK)
            {
                return (NOT_OK);
            }
#ifdef BAP
        }
#endif
    }
    else
    {
        /* Don't PFC the control packets */
        /* The IF condition has been added for NCPs Over Bundle ** */

        if ((pIf->BundleFlag == BUNDLE)
            || (pIf->MPInfo.MemberInfo.PartOfBundle == PPP_NO)
            || (Protocol == ECP_OVER_MEMBER_LINK)
            || (Protocol == CCP_OVER_MEMBER_LINK) || (Protocol == LCP_PROTOCOL)
            || ((pIf->BundleFlag != BUNDLE)
                && ((Protocol == CHAP_PROTOCOL) || (Protocol == PAP_PROTOCOL)
                    || (Protocol == EAP_PROTOCOL)
                    || (Protocol == LQM_PROTOCOL))))
        {

            PPPLLIPutPID (*pOutPDU, PFCEnableFlag, Protocol);
        }
    }
    return (OK);
}

/*********************************************************************
*    Function Name    :    PPPLLITxPkt
*    Description        :
*                This function is called by the internal modules of PPP 
*    subsystem to transmit packets to the lower layer.
*    Parameter(s)    :
*                pIf    -    Points to the Interface structure that corressponds
*                        the  PDU transmission.
*            pOutPDU    -    Points to the final transmit PDU, with Protocol Field
*                        as a  starting  byte.
*            Length    -    Length of the message.
*        Protocol    -    Protocol field of the pkt.
*    Return Values    :    VOID
*********************************************************************/
VOID
PPPLLITxPkt (tPPPIf * pIf, t_MSG_DESC * pOutPDU, UINT2 Length, UINT2 Protocol)
{
    INT1                ResultCode;
    BOOLEAN             bIsACFC;

    UINT2               PhysIfIndex;
    UINT2               Dlci;

    if (pIf->BundleFlag == BUNDLE)
    {
        PPP_DBG ("Bundle link : Sent to MP Module..");
        if (((pIf->MPInfo.
              BundleInfo.
              MPFlagsPerIf[MRRU_IDX].FlagMask & ACKED_BY_LOCAL_MASK) == NOT_SET)
            &&
            ((pIf->MPInfo.
              BundleInfo.
              MPFlagsPerIf[SEQ_IDX].FlagMask & ACKED_BY_LOCAL_MASK) == NOT_SET))
        {
            PPP_TRC (ALL_FAILURE, "Bundle not negotiated...");
            RELEASE_BUFFER (pOutPDU);
            return;
        }
        if (Length > pIf->MPInfo.BundleInfo.BundleOptAckedByLocal.MRRU)
        {
            PPP_TRC (ALL_FAILURE, "Invalid Length");
            RELEASE_BUFFER (pOutPDU);
            return;
        }

        if (PPPLLICompressEncryptPkt (pIf, &pOutPDU, Length, Protocol) ==
            NOT_OK)
        {
            PPP_TRC (ALL_FAILURE, "Compression/Encryption Failure ");
            RELEASE_BUFFER (pOutPDU);
            return;
        }

#ifdef NPAPI_WANTED
        /* But if an external processor is used for multilink fragmentation
         * and reassembly then -
         *   - Buffer passed to NP should contain the entire PPP packet.
         *   - Tx interface index passed should be that of the MP interface
         *     directly instead of any lower layer physical interface index.
         */

        if (Protocol == IPCP_PROTOCOL)
        {
            DumpIpcpData (pOutPDU, 0, 2);
        }
        if (Protocol == BCP_PROTOCOL)
        {
            DumpBcpData (pOutPDU, 0, 2);
        }

        /* Add framing irrespective of whether ACFC is negotiated or not.
         * The LLP will take care of compressing the address field based on
         * which member link it is transmitting on */
        bIsACFC = FALSE;
        ResultCode =
            FrmAddFramingReq (pIf->LinkInfo.IfID.IfType,
                              &pIf->AsyncParams, bIsACFC, &pOutPDU, &Length);
        if (ResultCode != OK)
        {
            return;
        }

        pIf->LLHCounters.OutNUniPackets++;
        /* Interface function to send away the packet to Lower Layer */
        PPPTxPktToLL (pIf, pOutPDU, Length, Protocol);
#else
        if (((Protocol & DATA_PKT_MASK) == 0) || (OutDataPktIndex >= DataIndex))
        {
            pIf->LcpGSEM.MiscParam.CharValue = MPCheckForInSequence (Protocol);
        }
        else
        {
            pIf->LcpGSEM.MiscParam.CharValue = NOT_MUST;
        }
#ifdef MP
        MPSplitAndAllocate (pIf, pOutPDU, (UINT2) (VALID_BYTES (pOutPDU)),
                            Protocol);
#endif
#endif
    }
    else
    {
        if (Length > pIf->LcpOptionsAckedByLocal.MRU)
        {
            PPP_TRC (ALL_FAILURE, "Invalid Length");
            RELEASE_BUFFER (pOutPDU);
        }
        else
        {
            if (PPPLLICompressEncryptPkt (pIf, &pOutPDU, Length, Protocol) ==
                NOT_OK)
            {
                PPP_TRC (ALL_FAILURE, "Compression/Encryption Failure.");
                RELEASE_BUFFER (pOutPDU);
                return;
            }
            if (Length > pIf->LcpOptionsAckedByLocal.MRU + PROT_LEN)
            {
                PPP_TRC (ALL_FAILURE, "Invalid Length");
                RELEASE_BUFFER (pOutPDU);
            }
            else
            {
                pIf->LLHCounters.OutOctets += Length;

                switch (pIf->LinkInfo.IfID.IfType)
                {
                    case FR_IF_TYPE:
                        PhysIfIndex = (UINT2) (pIf->LinkInfo.PhysIfIndex);
                        Dlci = pIf->LinkInfo.IfID.VCNumber;
                        FrmTxFR ((VOID **) (VOID *) (&pOutPDU), &Length,
                                 PhysIfIndex, Dlci);
                        /* Update the timer */
                        if (OsixGetSysTime (&pIf->LinkInfo.LastPktArrivalTime)
                            == OSIX_FAILURE)
                        {
                            PPP_TRC (OS_RESOURCE,
                                     "Failure in  OsixGetSysTime()");
                        }

                        pIf->LLHCounters.OutNUniPackets++;
                        /*Interface function to send away the packet to LL */
                        PPPTxPktToLL (pIf, pOutPDU, Length, Protocol);
                        return;
                    default:
                        break;
                }

                if (Protocol == LCP_PROTOCOL)
                {
                    DumpLcpData (pOutPDU, 0, 2);
                }

                if (Protocol == IPCP_PROTOCOL)
                {
                    DumpIpcpData (pOutPDU, 0, 2);
                }

                if (Protocol == BCP_PROTOCOL)
                {
                    DumpBcpData (pOutPDU, 0, 2);
                }

                if (pIf->LinkInfo.u1FrmFlag == PPP_TRUE)
                {
                    bIsACFC =
                        ((pIf->LcpGSEM.
                          pNegFlagsPerIf[ACFC_IDX].
                          FlagMask & ACKED_BY_LOCAL_MASK)
                         && (Protocol != LCP_PROTOCOL)) ? TRUE : FALSE;
                    if (pIf->TermReqFlag == SET)
                    {
                        PPPIndicateAsyncParamsToLL (pIf, NEGOTIATED_ASYNC_VALS);
                    }
#ifdef FRAMING
                    ResultCode =
                        FrmAddFramingReq (pIf->LinkInfo.IfID.IfType,
                                          &pIf->AsyncParams, bIsACFC, &pOutPDU,
                                          &Length);

                    if (ResultCode != OK)
                    {
                        return;
                    }
                    if (pIf->LinkInfo.IfID.IfType == SONET_IF_TYPE)
                        PPPoSProcessTxPkt (&pOutPDU, &Length, &pIf->AsyncParams,
                                           pIf);
#endif

                    if (pIf->TermReqFlag == SET)
                    {
                        pIf->TermReqFlag = NOT_SET;
                        PPPIndicateAsyncParamsToLL (pIf, DEFAULT_ASYNC_VALS);
                    }
                }

                pIf->LLHCounters.OutNUniPackets++;
                /* Interface function to send away the packet to Lower Layer */
                PPPTxPktToLL (pIf, pOutPDU, Length, Protocol);
                return;
            }
        }
    }
    return;
}

/*********************************************************************
*    Function Name    :    PPPRxPktFromLL
*    Description        :
*                This module is called by the  Lower Layer Modules  for 
*    passing the PDU received for the PPP subsystem. It demultiplexes the 
*    received PDU and indicates the appropriate module within the PPP subsystem.
*    Parameter(s)    :
*                pIf    -    Points to the interface structure over which this 
*                        PDU was received
*            pInPDU    -    Points to the PDU just received with Protocol Field as 
*                        starting  byte.  DLL header and FCS (if async hdlc 
*                        framing is not supported by PPP system)will be removed  
*                        before invoking this function. In the case of HDLC 
*                        framing, Addr and Ctrl fields also come along with 
*                        the pkt.
*            Length    -    Length of the PDU (without DLL HEADER and TAILER)
*    Return Values    :    VOID
*********************************************************************/
VOID
PPPRxPktFromLL (tPPPIf * pIf, t_MSG_DESC * pInPDU, UINT2 Length)
{
    UINT2               Protocol, Offset;
    INT1                ResultCode;
    BOOLEAN             bIsACFCNeg;
    UINT2               PhysIfIndex;
    UINT2               Dlci;
    UINT2               u2TmpLength;

    ResultCode = AC_NOT_COMPRESSED;
    Protocol = 0;
    Offset = 0;

    if (pIf == NULL)
        return;

    PPP_TRC2 (DUMP, "Link: %d ; Length: %d", pIf->LinkInfo.IfIndex, Length);
    PPP_PKT_DUMP (DUMP, pInPDU, Length, "[Rx]: ");

    if (VALID_BYTES (pInPDU) != Length)
    {
        DISCARD_PKT (pIf, "Incomplete packet recd. Discarded!!");
        return;
    }

    if (pIf->LinkInfo.IfID.IfType == FR_IF_TYPE)
    {
        PhysIfIndex = (UINT2) (pIf->LinkInfo.PhysIfIndex);
        Dlci = pIf->LinkInfo.IfID.VCNumber;
        /* This fn removes the FR framing and give the packet to PPP module */
        FrmRxFR ((VOID *) pInPDU, Length, PhysIfIndex, Dlci);
        return;
    }

#ifdef FRAMING
    if (pIf->LinkInfo.IfID.IfType == SONET_IF_TYPE)
    {
        if (PPPHandlePPPoSPkt (&pInPDU, &Length, &pIf->AsyncParams, pIf) ==
            FAILURE)
            return;
    }
#endif
    if (pIf->LinkInfo.IfID.IfType == ATM_IF_TYPE)
    {
        u2TmpLength = Length;
        if (PPPRemovePadding (&pInPDU, u2TmpLength, &Length) == FAILURE)
        {
            return;
        }
    }
    if (pIf->LinkInfo.u1FrmFlag == PPP_TRUE)
    {
        if (pIf->LcpGSEM.pNegFlagsPerIf == NULL)
        {
            /* Interface has been deleted */
            return;
        }

        bIsACFCNeg =
            (pIf->LcpGSEM.
             pNegFlagsPerIf[ACFC_IDX].
             FlagMask & ACKED_BY_PEER_MASK) ? TRUE : FALSE;

#ifdef FRAMING
        ResultCode =
            FrmLLRemoveFramingReq (pIf->LinkInfo.IfID.IfType, &pIf->AsyncParams,
                                   bIsACFCNeg, pInPDU, &Length);
#endif

        /* Update the Idle timer */
        if (OsixGetSysTime (&pIf->LinkInfo.LastPktArrivalTime) == OSIX_FAILURE)
        {
            PPP_TRC (OS_RESOURCE, "Failure in  OsixGetSysTime()");
        }

        /* Do not update the "LinkInfo.LastPktArrivalTime" here 
         * as we should not consider the WAN side traffic for idle-timeout.
         */
        pIf->LLHCounters.InNUniPackets++;
        pIf->LLHCounters.InOctets += Length;

        pIf->LinkInfo.RxAddrCtrlLen = 0;
        pIf->LinkInfo.RxProtLen = 0;

        if ((ResultCode == INVALID_FCS_VALUE)
            || (ResultCode == FRAM_INVALID_IF_TYPE))
        {
            DISCARD_PKT (pIf,
                         "\nDiscarding on bad FCS value or Wrong if type.");
            return;
        }
        if ((ResultCode == BAD_ADDR_FIELD) || (ResultCode == BAD_CTRL_FIELD))
        {
            DISCARD_PKT (pIf, "\n DeEncap Failure!!!");

            (ResultCode ==
             BAD_ADDR_FIELD) ? pIf->LLHCounters.
  BadAddressCounter++ : pIf->LLHCounters.BadControlCounter++;
            return;
        }
        if ((ResultCode == BAD_DEST_SAP) || (ResultCode == BAD_SRC_SAP)
            || (ResultCode == BAD_CONTROL_UI) || (ResultCode == BAD_NLPID_PPP))
        {
            DISCARD_PKT (pIf, "\n DeEncap Failure!!!");
            return;
        }

        /* dont increment LinkInfo.RxAddrCtrlLen for ATM and PPPoE */
        if (ResultCode == AC_NOT_COMPRESSED)
        {
            if ((pIf->LinkInfo.IfID.IfType != ATM_IF_TYPE)
                && (pIf->LinkInfo.IfID.IfType != PPP_OE_IFTYPE)
                && (pIf->LinkInfo.IfID.IfType != PPP_OE_OUSB_IFTYPE))
            {
                pIf->LinkInfo.RxAddrCtrlLen =
                    (UINT1) (pIf->LinkInfo.RxAddrCtrlLen + HDLC_ADDR_LEN);
                Offset = (UINT2) (Offset + HDLC_ADDR_LEN);
            }
        }
    }
    else if (pIf->LinkInfo.IfID.IfType == CFA_PPP)
    {
        pIf->LinkInfo.RxAddrCtrlLen = 0;
        pIf->LinkInfo.RxProtLen = 0;
        bIsACFCNeg = FALSE;
#ifdef FRAMING
        ResultCode =
            FrmLLRemoveFramingReq (pIf->LinkInfo.IfID.IfType, &pIf->AsyncParams,
                                   bIsACFCNeg, pInPDU, &Length);
#endif

        if (ResultCode == AC_NOT_COMPRESSED)
        {
            /* pIf->LinkInfo.RxAddrCtrlLen += HDLC_ADDR_LEN;
               Offset += HDLC_ADDR_LEN; */
            pIf->LinkInfo.RxAddrCtrlLen =
                (UINT1) (pIf->LinkInfo.RxAddrCtrlLen + HDLC_ADDR_LEN);
            Offset = (UINT2) (Offset + HDLC_ADDR_LEN);
        }
    }
    /* Check protocol field compression */
    if (PPPLLIGetProtField
        (pIf, pInPDU, &Protocol, (UINT1) (Offset), CALLED_FROM_LLI) == DISCARD)
    {
        DISCARD_PKT (pIf, "Invalid pkt. Discarded!!");
        return;
    }

    if (Protocol == LCP_PROTOCOL)
    {
        DumpLcpData (pInPDU, (UINT1) Offset, 1);
    }
    else if (Protocol == IPCP_PROTOCOL)
    {
        DumpIpcpData (pInPDU, (UINT1) Offset, 1);
    }
    else if (Protocol == BCP_PROTOCOL)
    {
        DumpBcpData (pInPDU, (UINT1) Offset, 1);
    }

    Offset = (UINT2) (Offset + pIf->LinkInfo.RxProtLen);
    /* Copy the protocol value into module data for
     * processing by other modules */
    PPP_PROT_TYPE (pInPDU) = (UINT4) Protocol;

    /* Address and control field of LCP packets shouldn't be compressed. */
    if (Protocol == LCP_PROTOCOL)
    {

        if ((ResultCode == AC_COMPRESSED)
            && (pIf->LinkInfo.IfID.IfType != X25_IF_TYPE)
            && (pIf->LinkInfo.IfID.IfType != FR_IF_TYPE))
        {
            DISCARD_PKT (pIf,
                         "AC Field of LCP Packets shouldn't be compressed");
            return;
        }
    }

    /* Update the Length by the bytes so far read */
    Length = (UINT2) (Length - Offset);
    if (pIf->BundleFlag != BUNDLE)
    {
        if ((Length > DEF_MRU_SIZE)
            && (Length > pIf->LcpOptionsAckedByPeer.MRU))
        {
            DISCARD_PKT (pIf, "LLI : Packet recd. is too Long !!");
            pIf->LLHCounters.PacketTooLongCounter++;
            return;
        }

        /* Any non-LCP packets received, during the Link Establishment phase,
           should be silently discarded. 
         */
        if ((pIf->LcpStatus.LinkAvailability == LINK_NOT_AVAILABLE)
            && (Protocol != LCP_PROTOCOL))

        {
            DISCARD_PKT (pIf, "Pkt. not acceptable in this phase ");
            return;
        }
    }

    if (PPPLLIProcessPID
        (pIf, pInPDU, Protocol, Offset, CALLED_FROM_LLI,
         Length) == NOT_PROCESSED)
    {

        if (pIf->LcpStatus.OperStatus != STATUS_UP)
        {
            DISCARD_PKT (pIf, "Pkt. not acceptable in this phase ");
            return;
        }

        pIf->LLHCounters.InUnknownProtos++;
        if (PPPSendRejPkt (pIf, pInPDU, Length, LCP_PROTOCOL, PROT_REJ_CODE) ==
            FALSE)
        {
            PPP_DBG ("Unable to send protocol reject..");
        }
    }

    if (pRestartGSEM != NULL)
    {
        GSEMRestartIfNeeded (pRestartGSEM);
        pRestartGSEM = NULL;
    }
    if (pTermGSEM != NULL)
    {
        GSEMRun (pTermGSEM, RXJ_MINUS);
        pTermGSEM = NULL;
    }
    return;
}

/*********************************************************************
*    Function Name    :    PPPLLIGetProtField
*    Description        :
*                This function is used extract the protocol field.
*    Parameter(s)    :
*                pIf    -    Points to the interface structure over which this 
*                        PDU was received.
*            pInPDU    -    Points to the PDU just received 
*        Protocol    -    Points to the protocol field to be extracted.
*            Offset    -    Read Offset 
*        CallFlag    -    Indicates the module calling this function - MP or LLI.
*    Return Values    :    Length of the extracted PID.
*********************************************************************/
INT1
PPPLLIGetProtField (tPPPIf * pIf, t_MSG_DESC * pInPDU, UINT2 *Protocol,
                    UINT1 Offset, UINT1 CallFlag)
{
    UINT1               CharInfo;

    /* Extract 1B of the Protocol Field. */
    GET1BYTE (pInPDU, Offset, CharInfo);

    /* If the extracted value is even, then it is a Control Packet */
    if ((CharInfo % 2) == 0)
    {

        /* Extract the entire protocol field */
        GET2BYTE (pInPDU, Offset, *Protocol);
        pIf->LinkInfo.RxProtLen = UNCOMPRESSED_PID_LEN;
        return (UNCOMPRESSED_PID_LEN);
    }

    /*  
       Compressed protocol field is valid if :
       Function has been called from MP.
       Function called from LLI and with PFC negotiated.
       Function called from LLI, PFC not negotiated, but data is on 
       MP member link without MP header.
     */

    if ((CallFlag == CALLED_FROM_MP) ||
        ((CallFlag == CALLED_FROM_LLI)
         &&
         ((pIf->LcpGSEM.pNegFlagsPerIf[PFC_IDX].FlagMask & ACKED_BY_PEER_MASK)
          ||
          (((pIf->LcpGSEM.
             pNegFlagsPerIf[PFC_IDX].FlagMask & ACKED_BY_PEER_MASK) == NOT_SET)
           && (pIf->MPInfo.MemberInfo.PartOfBundle == PPP_YES)
           && (CharInfo != CCP_DATA_OVER_MEMBER_LINK)
           && (CharInfo != ECP_DATA_OVER_MEMBER_LINK)
           && (CharInfo != MP_DATAGRAM)))))
    {
        *Protocol = CharInfo;
        pIf->LinkInfo.RxProtLen = COMPRESSED_PID_LEN;
        return (COMPRESSED_PID_LEN);
    }
    return (DISCARD);
}

/*********************************************************************
*    Function Name    :    PPPLLIProcessPID
*    Description        :
*        This function is used to extract the protocol field.
*    Parameter(s)    :
*                pIf    -    Points to the interface structure over which this 
*                        PDU was received.
*            pInPDU    -    Points to the PDU just received. 
*            Offset    -    Read Offset of pInPDU. 
*        CallFlag    -    Indicates module calling this function - MP or LLI.
*    Return Values    :    PROCESSED/NOT_PROCESSED    
*********************************************************************/
UINT1
PPPLLIProcessPID (tPPPIf * pIf, t_MSG_DESC * pInPDU, UINT2 Protocol,
                  UINT2 Offset, UINT1 CallFlag, UINT2 Length)
{
    UINT1               CompFlag, EncFlag, RetVal;
    tGSEM              *pGSEM;
    tECPIf             *pECPIf;
    tCCPIf             *pCCPIf;

    PPP_UNUSED (Offset);

    CompFlag = PPP_NO;
    EncFlag = PPP_NO;
    RetVal = PROCESSED;
    pGSEM = NULL;

    /*  If ECP is negotiated, all incoming datagrams SHOULD be encrypted */
    pECPIf = (tECPIf *) pIf->pECPIf;
    if ((CallFlag == CALLED_FROM_LLI)
        && (Protocol == ECP_DATA_OVER_INDIVIDUAL_LINK)
        && (pIf->MPInfo.MemberInfo.PartOfBundle == PPP_YES))
    {
        PPP_DBG (" Datagram recd. over member lnks.");
#ifdef MP
        MPInput (pIf, pInPDU, (UINT2) (VALID_BYTES (pInPDU)),
                 WITHOUT_MP_HEADER);
#endif
        return (PROCESSED);
    }

    if (((pECPIf != NULL) && (pECPIf->AdminStatus == STATUS_UP))
        && ((Protocol & DATA_PKT_MASK) == 0))
    {
        if (((Protocol != ECP_DATA_OVER_INDIVIDUAL_LINK)
             && (Protocol != ECP_DATA_OVER_MEMBER_LINK))
            || (pECPIf->OperStatus == STATUS_DOWN)
            || ((PPPLLIDecodePkt (pIf, &pInPDU, &Protocol, CallFlag)) ==
                FAILURE))
        {
            DISCARD_PKT (pIf, " LLI : ECP not yet over!!");
            return (PROCESSED);
        }
        EncFlag = PPP_YES;
    }

    if ((CallFlag == CALLED_FROM_LLI)
        && (Protocol == CCP_DATA_OVER_INDIVIDUAL_LINK)
        && (pIf->MPInfo.MemberInfo.PartOfBundle == PPP_YES))
    {
#ifdef MP
        MPInput (pIf, pInPDU, (UINT2) (VALID_BYTES (pInPDU)),
                 WITHOUT_MP_HEADER);
#endif
        return (PROCESSED);
    }
    /* Decompress the packet , if necessary */
    pCCPIf = (tCCPIf *) pIf->pCCPIf;
    if ((pCCPIf != NULL)
        && ((Protocol == CCP_DATA_OVER_INDIVIDUAL_LINK)
            || (Protocol == CCP_DATA_OVER_MEMBER_LINK))
        && (pCCPIf->OperStatus == STATUS_UP))
    {
        if ((PPPLLIDecodePkt (pIf, &pInPDU, &Protocol, CallFlag)) == FAILURE)
        {
            DISCARD_PKT (pIf, " LLI : CCP problem !!");
            return (PROCESSED);
        }
        CompFlag = PPP_YES;
    }

    /* Get the Index and the pGSEM structure of the data packet.
       If not found , then, log error message in the case of MP call.
       Pass any datagrams received over the member link to the respective
       bundle.
       Trigger RXD event to the respective SEM
     */

    PPP_TRC1 (CONTROL_PLANE, "INTERFACE :[%ld]\t IN", pIf->LinkInfo.IfIndex);
    PPPPrintProtocol (Protocol);

    if ((ProtIndex = GCPGetProtIndex (Protocol)) != NOT_FOUND)
    {

        if ((pGSEM = (*ProtSEMTable[ProtIndex].pGetSEMPtr) (pIf)) != NULL)
        {

            pGSEM->pInParam = pInPDU;
            /* Data Pkts   */
            if (ProtIndex >= DataIndex)
            {
                if ((CallFlag == CALLED_FROM_LLI)
                    && (pIf->MPInfo.MemberInfo.PartOfBundle == PPP_YES))
                {
#ifdef MP
                    MPInput (pIf, pInPDU, (UINT2) (VALID_BYTES (pInPDU)),
                             WITHOUT_MP_HEADER);
#endif
                }
                else
                {
                    pGSEM->MiscParam.ShortValue = Protocol;
                    GSEMRun (pGSEM, RXD);
                }
            }
            else
            {
                if ((EncFlag == PPP_YES) || (CompFlag == PPP_YES))
                {
                    DISCARD_PKT (pIf, "Pkt not accepted...\n");
                }
                else
                {
                    Length =
                        (UINT2) ((UINT2) VALID_BYTES (pGSEM->pInParam) -
                                 (pIf->LinkInfo.RxProtLen +
                                  pIf->LinkInfo.RxAddrCtrlLen));

                    if ((CallFlag == CALLED_FROM_LLI)
                        && (pIf->BundleFlag != BUNDLE)
                        && (pIf->MPInfo.MemberInfo.pBundlePtr != NULL)
                        && (Protocol != LCP_PROTOCOL)
                        && Protocol != ECP_OVER_MEMBER_LINK
                        && Protocol != CCP_OVER_MEMBER_LINK)
                    {
#ifdef MP
                        MPInput (pIf, pInPDU, (UINT2) (VALID_BYTES (pInPDU)),
                                 WITHOUT_MP_HEADER);
#endif
                    }
                    else
                    {
                        GCPInput (pGSEM, pGSEM->pInParam, Length);
                    }
                }
            }
        }
        else
        {
            DISCARD_PKT (pIf, " LLI : Entry not found!!");
            RetVal = NOT_PROCESSED;    /*02032001 */
        }
    }
    else
    {
        if (CallFlag == CALLED_FROM_LLI)
        {
            if (((EncFlag == PPP_YES) || (CompFlag == PPP_YES))
                && (Protocol != MP_DATAGRAM))
            {
                DISCARD_PKT (pIf, "Pkt not accepted...\n");
                return (PROCESSED);
            }
            RetVal = PPPLLIProcessMiscProtPkts (pIf, pInPDU, Length, Protocol);
        }
        else
        {
            RetVal = NOT_PROCESSED;
        }
    }
    return (RetVal);
}

/*********************************************************************
*    Function Name    :    PPPLLIPutPID
*    Description        :
*            This function is used append the protocol field.
*    Parameter(s)    :
*            pInPDU    -    Points to the PDU just received 
*    PFCEnableFlag    -    PFC Enable/Disable Flag
*   Protocol        -    Protocol to be appended
*    Return Values    :    VOID
*********************************************************************/
VOID
PPPLLIPutPID (t_MSG_DESC * pOutPDU, UINT1 PFCEnableFlag, UINT2 Protocol)
{
    UINT1               Temp;

    if (PFCEnableFlag == PPP_YES)
    {
        Temp = (UINT1) Protocol;
        MOVE_BACK_PTR (pOutPDU, COMPRESSED_PID_LEN);
        APPEND1BYTE (pOutPDU, Temp);
    }
    else
    {
        MOVE_BACK_PTR (pOutPDU, UNCOMPRESSED_PID_LEN);
        APPEND2BYTE (pOutPDU, Protocol);
    }
    return;
}

/*********************************************************************
*    Function Name    :    PPPLLIEncapPID
*    Description        :
*            This function is used to compress and/or encrypt the outgoing 
*    packet, if compression and/or encryption is successfully negotiated. 
*    Parameter(s)    :
*            pInPDU    -    Points to the PDU just received 
*    PFCEnableFlag    -    PFC Enable/Disable Flag
*        Protocol    -    Protocol to be appended
*    Return Values    :    OK/NOT_OK
*********************************************************************/
INT1
PPPLLIEncapPID (tPPPIf * pIf, t_MSG_DESC ** pOutPDU, UINT1 PFCEnableFlag)
{
    tGSEM              *pGSEM;

    tECPIf             *pECPIf;

    tCCPIf             *pCCPIf;

    pCCPIf = (tCCPIf *) pIf->pCCPIf;

    if ((pIf->LcpStatus.CCPEnableFlag == SET)
        && (pCCPIf->OperStatus == STATUS_UP))
    {
        pGSEM = &pCCPIf->CCPGSEM;
        pGSEM->pOutParam = *pOutPDU;

        pGSEM->MiscParam.ShortValue = pGSEM->Protocol;
        GSEMRun (pGSEM, TXD);
        *pOutPDU = pGSEM->pOutParam;

        if (pGSEM->MiscParam.CharValue == COMPRESSED)
        {
            if (pGSEM->Protocol == CCP_OVER_INDIVIDUAL_LINK)
            {
                PPPLLIPutPID (*pOutPDU, PFCEnableFlag,
                              CCP_DATA_OVER_INDIVIDUAL_LINK);
            }
            if (pGSEM->Protocol == CCP_OVER_MEMBER_LINK)
            {
                PPPLLIPutPID (*pOutPDU, PFCEnableFlag,
                              CCP_DATA_OVER_MEMBER_LINK);
            }
        }
    }
    pECPIf = (tECPIf *) pIf->pECPIf;
    if ((pIf->LcpStatus.ECPEnableFlag == SET)
        && (pECPIf->OperStatus == STATUS_UP))
    {
        pGSEM = &pECPIf->ECPGSEM;
        pGSEM->pOutParam = *pOutPDU;
        GSEMRun (pGSEM, TXD);
        *pOutPDU = pGSEM->pOutParam;
        if (pGSEM->MiscParam.CharValue == ENCRYPTED)
        {
            if (pGSEM->Protocol == ECP_OVER_INDIVIDUAL_LINK)
            {
                PPPLLIPutPID (*pOutPDU, PFCEnableFlag,
                              ECP_DATA_OVER_INDIVIDUAL_LINK);
            }
            else
            {
                PPPLLIPutPID (*pOutPDU, PFCEnableFlag,
                              ECP_DATA_OVER_MEMBER_LINK);
            }
        }
        else
        {
            DISCARD_PKT (pIf, "Error : Encryption failure...!!\n");
            return (NOT_OK);
        }
    }
    return (OK);
}

/*********************************************************************
*    Function Name    :    PPPLLITxHDLC
*    Description        :
*    Parameter(s)    :
*                pIf    -    Points to the interface structure over which 
*                        this PDU was received.
*            pOutPDU    -    Points to the PDU to be transmitted.
*    Return Values    :    VOID
*********************************************************************/
VOID
PPPLLITxHDLC (tPPPIf * pIf, t_MSG_DESC * pOutPDU, UINT2 Protocol)
{

    if (((pIf->LcpGSEM.
          pNegFlagsPerIf[ACFC_IDX].FlagMask & ACKED_BY_LOCAL_MASK) == NOT_SET)
        ||
        ((pIf->LcpGSEM.pNegFlagsPerIf[ACFC_IDX].FlagMask & ACKED_BY_LOCAL_MASK)
         && (Protocol == LCP_PROTOCOL)))
    {
        MOVE_BACK_PTR (pOutPDU, HDLC_ADDR_LEN);
        APPEND1BYTE (pOutPDU, ALL_STATIONS);
        APPEND1BYTE (pOutPDU, UI);
    }
    return;
}

/*********************************************************************
*    Function Name    :    PPPLLIDecodePkt
*    Description        :
*            This function is used to process the Encrpyted/Compressed Data 
*    packet.
*    Parameter(s)    :
*                pIf    -    Points to the interface structure over which this 
*                        PDU was received.
*            pInPDU    -    Points to the incoming PDU.
*        Protocol    -    Points to the protocol.
*        CallFlag    -    Indicates the module calling this function - MP or LLI.    
*    Return Values    :    SUCCESS/FAILURE
*********************************************************************/
INT1
PPPLLIDecodePkt (tPPPIf * pIf, t_MSG_DESC ** pInPDU, UINT2 *Protocol,
                 UINT1 CallFlag)
{
    INT1                i1ProtIndex;
    tGSEM              *pGSEM;

    i1ProtIndex = GCPGetProtIndex (*Protocol);

    if (i1ProtIndex == NOT_FOUND)
    {
        return FAILURE;
    }

    if ((pGSEM = (*ProtSEMTable[i1ProtIndex].pGetSEMPtr) (pIf)) != NULL)
    {
        pGSEM->pInParam = *pInPDU;
        pGSEM->MiscParam.CharValue = PKT_DISCARDED;
        GSEMRun (pGSEM, RXD);
        if (pGSEM->MiscParam.CharValue != PKT_DISCARDED)
        {
            *pInPDU = pGSEM->pInParam;
            if (PPPLLIGetProtField (pIf, *pInPDU, Protocol, 0, CallFlag) !=
                DISCARD)
            {
                return (SUCCESS);
            }
        }
    }
    PPP_TRC (ALL_FAILURE, " Decoding unsuccessful....");
    return (FAILURE);
}

/*********************************************************************
*    Function Name    :    LLIProcessMiscProtPkts 
*    Description        :
*        This function is used to process the miscellaneous packets like
*    CHAP/PAP/LQM and MP packets. This function invoked only when the 
*    packet is determined as neither as a control nor a data packet.
*    packet.
*    Parameter(s)    :
*                pIf    -    Points to the interface structure over which this 
*                        PDU was received.
*    Return Values    : 
*                    PROCESSED / NOT_PROCESSED
*********************************************************************/
UINT1
PPPLLIProcessMiscProtPkts (tPPPIf * pIf, t_MSG_DESC * pInPDU, UINT2 Length,
                           UINT2 Protocol)
{

    switch (Protocol)
    {
        case MP_DATAGRAM:
            if (pIf->MPInfo.MemberInfo.PartOfBundle == PPP_YES)
            {
#ifdef MP
                MPInput (pIf, pInPDU, Length, WITH_MP_HEADER);
#endif
            }
            else
            {
                DISCARD_PKT (pIf, " Pkt is discarded. Entry is empty");
            }
            return (PROCESSED);
            break;

        case CHAP_PROTOCOL:
            if (pIf->pAuthPtr != NULL)
            {
                CHAPInput (pIf->pAuthPtr, pInPDU, Length);
            }
            else
            {
                DISCARD_PKT (pIf, " Pkt is discarded. Entry is empty");
            }
            break;

        case PAP_PROTOCOL:
            if (pIf->pAuthPtr != NULL)
            {
                PAPInput (pIf->pAuthPtr, pInPDU, Length);
            }
            else
            {
                DISCARD_PKT (pIf, " Pkt is discarded. Entry is empty");
            }
            break;

        case EAP_PROTOCOL:
            if (pIf->pAuthPtr != NULL)
            {
                EAPInput (pIf->pAuthPtr, pInPDU, Length);
            }
            else
            {
                DISCARD_PKT (pIf, " Pkt is discarded. Entry is empty");
            }
            break;

        case LQM_PROTOCOL:
            if (pIf->pLqmPtr != NULL)
            {
#ifdef LQM
                LQMInput (pIf->pLqmPtr, pInPDU, Length);
#endif
            }
            else
            {
                DISCARD_PKT (pIf, " Pkt is discarded. Entry is empty");
            }
            break;

        default:
            return (NOT_PROCESSED);
    }

    return (PROCESSED);
}

#define MAX_DUMP_LEN        512

void
DumpLcpData (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 Offset, UINT1 u1Dir)
{
    UINT4               u4Count, u4Code;
    UINT1               Linear[MAX_DUMP_LEN + 1];
    UINT4               u4Length;
    UINT2               u2PppLength;
    FILE               *fp = stdout;
    UINT1               strCode[12][18] =
        { "Invalid", "Config-Request", "Config-Ack", "Config-Nak",
        "Config-Reject", "Terminate-Request", "Terminate-Ack", "Code-Rej",
        "Protocol-Reject", "Echo-Request",
        "Echo-Reply", "Discard-Request"
    };
    UINT1               strOpt[9][18] =
        { "Invalid", "MRU", "Invalid", "Auth-Proto", "Quality-Protocol",
        "Magic-Number", "Invalid", "PFC", "ACFC"
    };
    UINT1               strOptMp[3][16] =
        { "MRRU", "ShortSeqNumber", "EndPtDscrmtr" };
    UINT1               u1OptLen, u1OptCount;

    if ((DesiredLevel & DUMP) == 0)
    {
        return;
    }

    u4Length = CRU_BUF_Get_ChainValidByteCount (pBuf);

    if (u4Length > MAX_DUMP_LEN)
        u4Length = MAX_DUMP_LEN;

    u4Count = 0;

    CRU_BUF_Copy_FromBufChain (pBuf, Linear, (UINT4) (Offset + 2),
                               (UINT4) (u4Length - (UINT2) (Offset + 2)));
    if (u1Dir == 1)
        fprintf (fp, "\n >>>> LCP IN\n");
    else
        fprintf (fp, "\n <<<< LCP OUT \n");

    u4Code = Linear[u4Count];
    if (u4Code <= 11)
        fprintf (fp, "Code\t\t: %s\n", strCode[u4Code]);
    else
        fprintf (fp, "Code\t\t: %02x\n", u4Code);
    u4Count++;
    fprintf (fp, "Identifier\t: %02x\n", Linear[u4Count]);
    u4Count++;
    memcpy (&u2PppLength, &Linear[u4Count], 2);
    u2PppLength = (UINT2) OSIX_NTOHS (u2PppLength);
    fprintf (fp, "Length\t\t: %02d\n", u2PppLength);
    u4Count += 2;

    if (u4Code < 5)
    {
        while (u4Count < u2PppLength)
        {
            if (Linear[u4Count] <= 8)
                fprintf (fp, "Option\t\t: %s", strOpt[Linear[u4Count]]);
            else if ((Linear[u4Count] >= 17) && (Linear[u4Count] <= 19))
                fprintf (fp, "Option\t\t: %s", strOptMp[Linear[u4Count] - 17]);
            else
                fprintf (fp, "Option\t\t: %02x", Linear[u4Count]);
            u4Count++;
            if (u4Count > MAX_DUMP_LEN)
            {
                return;
            }
            u1OptLen = Linear[u4Count];
            u4Count++;
            if (u4Count > MAX_DUMP_LEN)
            {
                return;
            }
            if ((u1OptLen > 2) && (u1OptLen < 12))
            {
                fprintf (fp, ",\tValue -");
                u1OptCount = 3;
                while (u1OptCount <= u1OptLen)
                {
                    fprintf (fp, " %02x", Linear[u4Count]);
                    u4Count++;
                    if (u4Count > MAX_DUMP_LEN)
                    {
                        return;
                    }
                    u1OptCount++;
                }
                fprintf (fp, ", Len - %02d", u1OptLen);
            }
            else
            {
                fprintf (fp, ",\tLen - %02d", u1OptLen);
            }
            fprintf (fp, "\n");
        }
    }
}

void
DumpIpcpData (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 Offset, UINT1 u1Dir)
{
    UINT4               u4Count, u4Code;
    UINT1               Linear[MAX_DUMP_LEN + 1];
    UINT4               u4Length;
    UINT2               u2PppLength;
    FILE               *fp = stdout;
    UINT1               strCode[9][20] =
        { "Invalid", "Config-Request", "Config-Ack", "Config-Nak",
        "Config-Reject", "Terminate-Request", "Terminate-Ack", "Code-Rej",
        "Protocol-Reject"
    };
    UINT1               strOpt[5][20] =
        { "Invalid", "Invalid", "IP-Compress-Proto", "IP-Address",
        "VJ-TCP/IP-Compress"
    };
    UINT1               u1OptLen, u1OptCount;

    if ((DesiredLevel & DUMP) == 0)
    {
        return;
    }

    u4Length = CRU_BUF_Get_ChainValidByteCount (pBuf);

    if (u4Length > MAX_DUMP_LEN)
        u4Length = MAX_DUMP_LEN;

    u4Count = 0;

    CRU_BUF_Copy_FromBufChain (pBuf, Linear, (UINT4) (Offset + 2),
                               (UINT4) (u4Length - (UINT1) (Offset + 2)));

    if (u1Dir == 1)
        fprintf (fp, "\n >>>> IPCP IN\n");
    else
        fprintf (fp, "\n <<<< IPCP OUT \n");

    u4Code = Linear[u4Count];
    if (u4Code <= 8)
        fprintf (fp, "Code\t\t: %s\n", strCode[u4Code]);
    else
        fprintf (fp, "Code\t\t: %02x\n", u4Code);
    u4Count++;
    fprintf (fp, "Identifier\t: %02x\n", Linear[u4Count]);
    u4Count++;
    memcpy (&u2PppLength, &Linear[u4Count], 2);
    u2PppLength = (UINT2) OSIX_NTOHS (u2PppLength);
    fprintf (fp, "Length\t\t: %02d\n", u2PppLength);
    u4Count += 2;

    if (u4Code < 5)
    {
        while (u4Count < u2PppLength)
        {
            if (Linear[u4Count] <= 9)
                fprintf (fp, "Option\t\t: %s", strOpt[Linear[u4Count]]);
            else
                fprintf (fp, "Option\t\t: %02x", Linear[u4Count]);
            u4Count++;
            if (u4Count > MAX_DUMP_LEN)
            {
                return;
            }
            u1OptLen = Linear[u4Count];
            u4Count++;
            if (u4Count > MAX_DUMP_LEN)
            {
                return;
            }
            if ((u1OptLen > 2) && (u1OptLen < 12))
            {
                fprintf (fp, ",\tValue -");
                u1OptCount = 3;
                while (u1OptCount <= u1OptLen)
                {
                    fprintf (fp, " %02x", Linear[u4Count]);
                    u4Count++;
                    if (u4Count > MAX_DUMP_LEN)
                    {
                        return;
                    }
                    u1OptCount++;
                }
                fprintf (fp, ", Len - %02d", u1OptLen);
            }
            else
            {
                fprintf (fp, ",\tLen - %02d", u1OptLen);
            }
            fprintf (fp, "\n");
        }
    }
}

void
DumpBcpData (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 Offset, UINT1 u1Dir)
{
    UINT4               u4Count, u4Code;
    UINT1               Linear[MAX_DUMP_LEN + 1];
    UINT4               u4Length;
    UINT2               u2PppLength;
    FILE               *fp = stdout;
    UINT1               strCode[9][20] =
        { "Invalid", "Config-Request", "Config-Ack", "Config-Nak",
        "Config-Reject", "Terminate-Request", "Terminate-Ack", "Code-Rej",
        "Protocol-Reject"
    };
    UINT1               strOpt[10][20] =
        { "Invalid", "Bridge-Id", "Line-Id", "MAC-Type", "Tinygram-Compress",
        "Invalid", "MAC-address", "Old-STP-Proto", "IEEE802Tag", "Mgmt-Inline"
    };
    UINT1               u1OptLen, u1OptCount;

    if ((DesiredLevel & DUMP) == 0)
    {
        return;
    }

    u4Length = CRU_BUF_Get_ChainValidByteCount (pBuf);

    if (u4Length > MAX_DUMP_LEN)
        u4Length = MAX_DUMP_LEN;

    u4Count = 0;

    CRU_BUF_Copy_FromBufChain (pBuf, Linear, (UINT4) (Offset + 2),
                               (UINT4) (u4Length - (UINT1) (Offset + 2)));

    if (u1Dir == 1)
        fprintf (fp, "\n >>>> BCP IN\n");
    else
        fprintf (fp, "\n <<<< BCP OUT \n");

    u4Code = Linear[u4Count];
    if (u4Code <= 8)
        fprintf (fp, "Code\t\t: %s\n", strCode[u4Code]);
    else
        fprintf (fp, "Code\t\t: %02x\n", u4Code);
    u4Count++;
    fprintf (fp, "Identifier\t: %02x\n", Linear[u4Count]);
    u4Count++;
    memcpy (&u2PppLength, &Linear[u4Count], 2);
    u2PppLength = (UINT2) OSIX_NTOHS (u2PppLength);
    fprintf (fp, "Length\t\t: %02d\n", u2PppLength);
    u4Count += 2;

    if (u4Code < 5)
    {
        while (u4Count < u2PppLength)
        {
            if (Linear[u4Count] <= 9)
                fprintf (fp, "Option\t\t: %s", strOpt[Linear[u4Count]]);
            else
                fprintf (fp, "Option\t\t: %02x", Linear[u4Count]);
            u4Count++;
            if (u4Count > MAX_DUMP_LEN)
            {
                return;
            }
            u1OptLen = Linear[u4Count];
            u4Count++;
            if (u4Count > MAX_DUMP_LEN)
            {
                return;
            }
            if ((u1OptLen > 2) && (u1OptLen < 12))
            {
                fprintf (fp, ",\tValue -");
                u1OptCount = 3;
                while (u1OptCount <= u1OptLen)
                {
                    fprintf (fp, " %02x", Linear[u4Count]);
                    u4Count++;
                    if (u4Count > MAX_DUMP_LEN)
                    {
                        return;
                    }
                    u1OptCount++;
                }
                fprintf (fp, ", Len - %02d", u1OptLen);
            }
            else
            {
                fprintf (fp, ",\tLen - %02d", u1OptLen);
            }
            fprintf (fp, "\n");
        }
    }
}

/************************************************************************/
/*  Function Name   : PPPRemovePAdding                                  */
/*  Description     : This Function Removes the Padding added by        */
/*  Input(s)        : pInBuf:Points to the incoming Buffer              */
/*                  : PayLoadLen: Length of the Ethernet PayLoad        */
/*                  : BufLen:Length of the Incoming Packet              */
/*  Output(s)       : None                                              */
/*  Returns         : Returns SUCCESS or FAILURE                        */
/************************************************************************/
INT4
PPPRemovePadding (t_MSG_DESC ** pInBuf, UINT2 BufLen, UINT2 *u2CorrectLen)
{
    UINT1              *pLinearPkt;
    UINT2               u2Val;
    UINT2               u2Length;

    pLinearPkt = (UINT1 *) CRU_BMC_Get_DataPointer (*pInBuf);
    /* Extracting Length of the Payload */
    u2Val = *(UINT2 *) (VOID *) (pLinearPkt + 4);
    u2Length = OSIX_NTOHS (u2Val);

    *u2CorrectLen = (UINT2) (u2Length + 2);

    if (*u2CorrectLen < BufLen)
    {
        UINT1              *pTempLinear;
        t_MSG_DESC         *pBuf;
        pBuf = *pInBuf;

        if (MemAllocateMemBlock
            (PPP_BUF_MEMPOOL_ID,
             (UINT1 **) (VOID *) &pTempLinear) == MEM_FAILURE)
        {
            return FAILURE;
        }
        CRU_BUF_Copy_FromBufChain (pBuf, pTempLinear, 0, *u2CorrectLen);
        RELEASE_BUFFER (pBuf);
        *pInBuf = CRU_BUF_Allocate_MsgBufChain (*u2CorrectLen, 0);
        CRU_BUF_Copy_OverBufChain (*pInBuf, pTempLinear, 0, *u2CorrectLen);
        MemReleaseMemBlock (PPP_BUF_MEMPOOL_ID, (UINT1 *) pTempLinear);
    }

    return SUCCESS;
}
