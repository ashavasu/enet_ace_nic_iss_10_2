/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppinit.c,v 1.7 2014/12/16 11:47:26 siva Exp $
 *
 * Description:This file contains procedures that interfaces with
 *             higher layer
 *
 *******************************************************************/
#include <stdlib.h>

#include    "pppcom.h"
#include    "genexts.h"
#include    "gsemdefs.h"
#include    "pppchap.h"
#include    "ppppap.h"
#include    "pppeap.h"
#include    "pppauth.h"
#include    "ppptest.h"

#include     "pppmp.h"
#include    "mpexts.h"

#include    "pppipcp.h"
#include    "ipexts.h"
#include    "pppipv6cp.h"
#include    "ipv6exts.h"
#include    "pppmuxcp.h"
#include    "pppmplscp.h"
#include    "mplsexts.h"
#include    "pppipxcp.h"
#include    "ipxproto.h"
#include    "ipxexts.h"
#include    "pppbcp.h"
#include    "bcpexts.h"
#ifdef BAP
#include    "mpdefs.h"
#include     "pppbacp.h"
#include     "bacpexts.h"
#include     "pppbap.h"
#endif
#include    "pppeccmn.h"
#include    "pppccp.h"

#include    "pppeccmn.h"
#include    "pppecp.h"
#include    "pppdpproto.h"

#include    "globexts.h"
#include    "pppproto.h"
#include    "pppoeport.h"
#include    "pppnp.h"
#include "genproto.h"
#include "authprot.h"
#include "ecpproto.h"
#include "ipv6proto.h"
#include "ccpproto.h"
#include "bcpproto.h"

extern void         SNMPInitInstance (VOID);
extern tSecurityInfo IndexZeroSecurityInfo;

#ifdef RADIUS_WANTED
extern UINT4        gu4PppSessionId;
extern UINT1        gu1PppRandSession1;
extern UINT1        gu1PppRandSession2;
#endif

/* SLL for storing Options and Registration functions */
tTMO_SLL            gPPPIPCPOptionRegList;
INT4               gi4PppSysLogId;
INT4               gi4PppoeSysLogId;

/*********************************************************************
*    Function Name    :    PPPHLIProtocolEnable
*    Description        : 
*            This function is called by the SNMP module when there is an 
*    Admin OPEN event to be passed to the appropriate Control Protocol SEM. 
*    Parameter(s)    :
*                pIf    -    Pointer to the Interface Structure over which the 
*                        protocol is to be enabled. 
*        Protocol    -    The higher layer control protocol to be enabled.        
*        pNcpCtrlBlk    -    Pointer to NCP Control Block 
*                        ( Parameterd as set by SNMP ). 
*    Return Values    :    VOID
*********************************************************************/
VOID
PPPHLIProtocolEnable (tPPPIf * pIf, UINT2 Protocol, VOID *pNcpCtrlBlk)
{
#ifdef BAP
    tPPPIf             *pBundleIf;
    tBAPInfo           *pBAPInfo;
#endif

    switch (Protocol)
    {
#ifdef REVISIT
        case IPXCP_PROTOCOL:
            IPXCPEnableIf (pIf->pIpxcpPtr, pNcpCtrlBlk);
            break;
#endif
        case IPCP_PROTOCOL:
            IPCPEnableIf (pIf->pIpcpPtr, pNcpCtrlBlk);
            break;
#ifdef IPv6CP
        case IPV6CP_PROTOCOL:
            IP6CPEnableIf (pIf->pIp6cpPtr);
            break;
#endif
        case MUXCP_PROTOCOL:
            MuxCPEnableIf (pIf->pMuxcpPtr);
            break;
#ifdef MPLS_WANTED
        case MPLSCP_PROTOCOL:
            MPLSCPEnableIf (pIf->pMplscpPtr);
            break;
#endif
#ifdef BCP
        case BCP_PROTOCOL:
            pNcpCtrlBlk = BCPEnable (pIf->pBcpPtr);
            UNUSED_PARAM (pNcpCtrlBlk);
            break;
#endif
#ifdef CCP
        case CCP_OVER_INDIVIDUAL_LINK:
        case CCP_OVER_MEMBER_LINK:
            CCPEnableIf (pIf->pCCPIf);
            break;
#endif
        case ECP_OVER_MEMBER_LINK:
        case ECP_OVER_INDIVIDUAL_LINK:
            ECPEnableIf (pIf->pECPIf);
            break;
#ifdef BAP
        case BACP_PROTOCOL:
            pBundleIf =
                (pIf->BundleFlag ==
                 BUNDLE) ? pIf : pIf->MPInfo.MemberInfo.pBundlePtr;
            pBAPInfo =
                (tBAPInfo *) pBundleIf->MPInfo.BundleInfo.
                BWMProtocolInfo.pProtocolInfo;
            BACPEnableIf (&pBAPInfo->BACPIf);
            break;
#endif
        default:
            break;
    }
    return;
}

/*********************************************************************
*    Function Name    :    PPPHLIProtocolDisable
*            This function is called by the SNMP module when there is an 
*    Admin Close Event to be passed to the appropriate Control Protocol SEM. 
*    Description        : 
*    Parameter(s)    :
*                pIf    -     Points to the interface structure over which 
*                        the protocol is to be disabled.
*        Protocol    -    The higher layer control protocol to be disabled.        
*    Return Values    :    VOID
*********************************************************************/
VOID
PPPHLIProtocolDisable (tPPPIf * pIf, UINT2 Protocol)
{
    switch (Protocol)
    {
#ifdef REVISIT
        case IPXCP_PROTOCOL:
            IPXCPDisableIf (pIf->pIpxcpPtr);
            break;
#endif
        case IPCP_PROTOCOL:
            IPCPDisableIf (pIf->pIpcpPtr);
            break;
#ifdef IPv6CP
        case IPV6CP_PROTOCOL:
            IP6CPDisableIf (pIf->pIp6cpPtr);
            break;
#endif
        case MUXCP_PROTOCOL:
            MuxCPDisableIf (pIf->pMuxcpPtr);
            break;
        case MPLSCP_PROTOCOL:
            MPLSCPDisableIf (pIf->pMplscpPtr);
            break;
#ifdef BCP
        case BCP_PROTOCOL:
            BCPDisableIf (pIf->pBcpPtr);
            break;
#endif
#ifdef CCP
        case CCP_OVER_INDIVIDUAL_LINK:
        case CCP_OVER_MEMBER_LINK:
            CCPDisableIf (pIf->pCCPIf);
            break;
#endif
        case ECP_OVER_MEMBER_LINK:
        case ECP_OVER_INDIVIDUAL_LINK:
            ECPDisableIf (pIf->pECPIf);
            break;
        default:
            break;
    }
    return;
}

/*********************************************************************
*    Function Name    :    PPPIfenable
*    Description        : 
*            This function is used to enable a PPP-LCP interface.   
*    Parameter(s)    :
*                pIf    -    Points to the PPP interface structure to be enabled.
*    Return Values    :    VOID
*********************************************************************/
VOID
PPPIfEnable (tPPPIf * pIf)
{
    LCPEnableIf (pIf);
    return;
}

/*********************************************************************
*    Function Name    :    PPPDisableIf
*    Description        : 
*            This function is used to disable a PPP-LCP interface.   
*    Parameter(s)    :
*                pIf    -    Points to the PPP interface structure to be disabled.
*    Return Values    :    VOID
*********************************************************************/
VOID
PPPIfdisable (tPPPIf * pIf)
{
    LCPDisableIf (pIf);
    return;
}

/*********************************************************************
*    Function Name    :    PPPLowerLinkUp
*    Description        :
*        This function is invoked by the control module to pass Lower layer 
*    UP message to the LCP module.
*    Parameter(s)    :
*                pIf    -    Points to the PPP interface structure to be enabled.
*  Return Values : VOID
*********************************************************************/
VOID
PPPLowerLinkUp (tPPPIf * pIf)
{
    GSEMRun (&pIf->LcpGSEM, PPP_UP);

    return;
}

/*********************************************************************
*  Function Name :  PPPLowerLinkDown
*  Description   :  This function is invoked by the CFA to pass Lower 
                    layer DOWN message to the LCP module.
*  Parameter(s)  :
*        pIf  -  points to the PPP interface structure to be enabled
*  Return Values : VOID
*********************************************************************/
VOID
PPPLowerLinkDown (tPPPIf * pIf)
{
    GSEMRun (&pIf->LcpGSEM, PPP_DOWN);
    return;
}

/*********************************************************************
*  Function Name :  PPPInit
*  Description   : 
*                    This procedure  initializes the global variables used 
*   by the PPP subsystem. It also allocates memory needed for the data 
*   structures. It is called during system initialization procedure.
*  Parameter(s)  :
*
*  Return Values : VOID
*********************************************************************/
VOID
PPPInit (VOID)
{

    INIT_RANDOM_GENERATOR ();

    DesiredLevel = PPP_MUST;
    DesiredModule = 0;
    gu1IPCPAddrModeSelector = IPCP_LOCAL_POOL;
    gu1aaaMethod = AAA_LOCAL;
    NoOfActiveTimers = 0;
    FragNum = 0;
    OutReleaseFlag = PPP_NO;
    OutDataPktIndex = 0;
    ReleaseFlag = PPP_YES;
#ifdef MP
    ResetSeqNumFlag = PPP_NO;
#endif
    DataIndex = PPP_DATA_INDEX;

#ifdef BAP
    DataIndex++;
#endif
#ifdef BCP
    DataIndex++;
#endif
#ifdef CCP
    DataIndex++;
    DataIndex++;
#endif
#ifdef IPv6CP
    DataIndex++;
#endif

#ifdef IPXCP_WANTED
    BZERO (&IPXCPCtrlBlk, sizeof (tIPXCPCtrlBlk));
#endif

#ifdef BCP
    BcpInit ();
#endif
    BZERO (&IPCPCtrlBlk, sizeof (tIPCPCtrlBlk));

    AUTHInitSecurityInfo (&IndexZeroSecurityInfo);
    GetFCSAvailability ();
    GetVJCompAvailability ();
    PPPInitTimerList ();
    SNMPInitInstance ();

    SLL_INIT (&PPPIfList);
    SLL_INIT (&gPPPIPCPOptionRegList);
    SLL_INIT (&AddrPoolList);

    pAllocPtr = NULL;
    pRestartGSEM = NULL;
    pTermGSEM = NULL;

#ifdef BAP
    RMInit ();
#endif

#ifdef RADIUS_WANTED

    gu4PppSessionId = 0;
    gu1PppRandSession1 = (UINT1) GET_RANDOM_NUMBER ();
    gu1PppRandSession2 = (UINT1) GET_RANDOM_NUMBER ();

#endif /* RADIUS_WANTED */

    PPPoEInitialize ();
#ifdef NPAPI_WANTED
    FsNpPppInit ();
#endif
    gi4PppSysLogId = SYS_LOG_REGISTER ((CONST UINT1 *) "PPP",
                                       SYSLOG_CRITICAL_LEVEL);
    gi4PppoeSysLogId = SYS_LOG_REGISTER ((CONST UINT1 *) "PPPoE",
                                         SYSLOG_CRITICAL_LEVEL);
    return;
}

/*********************************************************************
*  Function Name : PPPDeInit
*  Description   : 
*            This function is used to de-initialize the PPP subsystem. It
*  releases all the allocated memory.
*
*  Parameter(s)  : VOID
*  Return Values : VOID
*********************************************************************/
VOID
PPPDeInit (VOID)
{
    /* Changed function name for readability - FSAP2 */
    PPPDeInitTimerList ();
    SYS_LOG_DEREGISTER ((UINT4)gi4PppSysLogId);
    SYS_LOG_DEREGISTER ((UINT4)gi4PppoeSysLogId);
    return;
}

/*********************************************************************
*  Function Name : PPPDeleteIf
*  Description   :
*            This function is used to delete the PPP link entry from the 
*  interface table.
*  Parameter(s)  :
*       pIf   -   points to the interface to be disabled
*  Return Value  : VOID
*********************************************************************/
VOID
PPPDeleteIf (tPPPIf * pIf)
{

    /* start 98100001 */
    PPP_DBG1 ("NegFlagsAllocCounter: %d", NegFlagsAllocCounter);
    PPP_DBG1 ("NegFlagsFreeCounter : %d", NegFlagsFreeCounter);
    /* end 98100001 */
    /* start 98100001 */
    PPP_DBG1 ("NegFlagsAllocCounter: %d", NegFlagsAllocCounter);
    PPP_DBG1 ("NegFlagsFreeCounter : %d", NegFlagsFreeCounter);
    /* end 98100001 */
#ifdef DPIF
    PPPDeleteIfToDP (pIf);
#endif
    LCPDeleteIf (pIf);
}
