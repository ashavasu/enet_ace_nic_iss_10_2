/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppoid.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains global declarations for SNMP OID.
 *
 *******************************************************************/

#include "genhdrs.h"
#include "snmphdrs.h"
#include "pppsnmpm.h"
#include "snmpexts.h"

UINT4               pppSecurityPapProtocolOIDVal[11] =
    { 1, 3, 6, 1, 2, 1, 10, 23, 2, 1, 1 };
UINT4               pppSecurityChapMD5ProtocolOIDVal[11] =
    { 1, 3, 6, 1, 2, 1, 10, 23, 2, 1, 2 };

    /* Following hash defines are used to set the OID's of authentication
     * protocols. Set the value of PPP_EXT_SECURITY_OID to the OID of 
     * 'pppExtSecurity', replacing '.' by ','. Current value is assumed as
     *  pppExtSecurity ::= { futuresoftware(2076) ppp(5) pppext(2) 3}
     */
#define PPP_EXT_SECURITY_OID          1, 3, 6, 1, 4, 1, 2076, 5, 1, 4
#define PPP_EXT_SECURITY_PROTO_OID    PPP_EXT_SECURITY_OID, 1
#define PPP_MSCHAP_OID                PPP_EXT_SECURITY_PROTO_OID, 1
#define PPP_EAP_OID                   PPP_EXT_SECURITY_PROTO_OID, 2
#define PPP_EAP_MD5_OID               PPP_EAP_OID, 3

UINT4               pppSecurityMSCHAPProtocolOIDVal[] = { PPP_MSCHAP_OID };

UINT4               pppSecurityEapProtocolOIDVal[] = { PPP_EAP_OID };

UINT4               pppSecurityEapMD5ProtocolOIDVal[] = { PPP_EAP_MD5_OID };

UINT4               nullOIDVal[2] = { 0, 0 };
UINT4               pppBaseOIDVal[10] = { 1, 3, 6, 1, 2, 1, 10, 23 };
UINT4               ifIndexOIDVal[11] = { 1, 3, 6, 1, 2, 1, 2, 2, 1, 1 };

const tSNMP_OID_TYPE nullOID = { 2, nullOIDVal };
const tSNMP_OID_TYPE p_pppBaseOID = { 8, pppBaseOIDVal };
const tSNMP_OID_TYPE ifIndexOID = { 10, ifIndexOIDVal };

const tSNMP_OID_TYPE pppSecurityPapProtocolOID =
    { (sizeof (pppSecurityPapProtocolOIDVal) / sizeof (UINT4)),
    pppSecurityPapProtocolOIDVal
};
const tSNMP_OID_TYPE pppSecurityChapMD5ProtocolOID =
    { (sizeof (pppSecurityChapMD5ProtocolOIDVal) / sizeof (UINT4)),
    pppSecurityChapMD5ProtocolOIDVal
};
const tSNMP_OID_TYPE pppSecurityMSCHAPProtocolOID =
    { (sizeof (pppSecurityMSCHAPProtocolOIDVal) / sizeof (UINT4)),
    pppSecurityMSCHAPProtocolOIDVal
};

const tSNMP_OID_TYPE pppSecurityEapProtocolOID =
    { (sizeof (pppSecurityEapProtocolOIDVal) / sizeof (UINT4)),
    pppSecurityEapProtocolOIDVal
};

const tSNMP_OID_TYPE pppSecurityEapMD5ProtocolOID =
    { (sizeof (pppSecurityEapMD5ProtocolOIDVal) / sizeof (UINT4)),
    pppSecurityEapMD5ProtocolOIDVal
};
