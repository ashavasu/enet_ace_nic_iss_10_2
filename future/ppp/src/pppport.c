/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppport.c,v 1.6 2014/11/25 13:03:36 siva Exp $
 *
 * Description:This file contains procedures that  should be modified
 *             while porting.
 *
 *******************************************************************/

#include "pppport.h"
#include "ipproto.h"
#include "l2tp.h"

extern UINT4        gu4PptpRemoteServerIpAddr;

/*********************************************************************
*  Function Name :  PPPTxPktToHL
*  Description   : 
*      This function is called by the modules of the PPP subsystem 
*  to pass the incoming PDU to the higher layer.
*  Parameter(s)  :
*         pIf  -  points to the LCP interface structure
*       pInPDU -  points to the incoming PDU  buffer
*       Length -  Length of the PDU
*     Protocol -  protocol type of the data packet
*  Return Values : VOID
*********************************************************************/
#define PACKET_FROM_SERIAL_INTERFACE 4

VOID
PPPTxPktToHL (tPPPIf * pIf, t_MSG_DESC * pInPDU, UINT2 Length, UINT2 Protocol)
{
#ifdef CFA_WANTED
    CfaHandlePktFromPpp (pInPDU, pIf->LinkInfo.IfIndex, PPP_TO_IP, Protocol);
    UNUSED_PARAM (Length);
#else
    UNUSED_PARAM (pIf);
    UNUSED_PARAM (pInPDU);
    UNUSED_PARAM (Length);
    UNUSED_PARAM (Protocol);
#endif

    return;
}

/*********************************************************************
*  Function Name : PPPTxPktToLL
*  Description   :
*                    This function gets the PDU from the  LLITxPkt function
*  and  forwards the same to the underlying module , based on
*  the  link selected(X21/HDLC/ISDN/FR) .
*  Parameter(s)  :
*    pIf       -   points to the Interface structure that corressponds to 
*                  the  PDU transmission
*    pOutPDU   -   points to the final transmit PDU, with Protocol Field
*                             as a  starting  byte.
*    Length    -   Length of the message
*  Return Values : VOID
*********************************************************************/
VOID
PPPTxPktToLL (tPPPIf * pIf, t_MSG_DESC * pOutPDU, UINT2 Length, UINT2 Protocol)
{
    UINT1              *pu1Buf = NULL;
    t_MSG_DESC         *pBuf = NULL;
    UINT2               u2Len = 0;
    PPP_UNUSED (Length);
    PPP_UNUSED (pOutPDU);
    PPP_UNUSED (pIf);
    PPP_UNUSED (Protocol);

    UNUSED_PARAM (pu1Buf);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u2Len);

#ifdef PPTP_WANTED
    if (pIf->LinkInfo.IfID.IfType == PPTP_IF_TYPE)
    {
        u2Len = CRU_BUF_Get_ChainValidByteCount (pOutPDU);
        pu1Buf = MEM_MALLOC (u2Len, UINT1);
        if (pu1Buf == NULL)
        {
            PPP_TRC (OS_RESOURCE, "PPPTxPktToLL:- MemAlloc Failed For pu1Buf");
            CRU_BUF_Release_MsgBufChain (pOutPDU, 0);
            return;
        }
        CRU_BUF_Copy_FromBufChain (pOutPDU, (UINT1 *) pu1Buf, 0, u2Len);
        CRU_BUF_Release_MsgBufChain (pOutPDU, 0);
        pBuf = PPPAllocateBuffer (u2Len, 0);
        if (pBuf == NULL)
        {
            PPP_TRC (OS_RESOURCE, "PPPTxPktToLL:- MemAlloc Failed For pBuf");
            MEM_FREE (pu1Buf);
            return;
        }
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) pu1Buf, 0, u2Len);
        MEM_FREE (pu1Buf);
        PptpRcvPppData (pBuf, pIf->LinkInfo.IfIndex);
        return;
    }
#endif

#ifdef L2TP_WANTED
    if (TRUE == PPPIsL2TPSessionExist (pIf))    /* DSL_ADD */
    {
        if ((FALSE == PPPIsL2TPTunnelVoluntary (pIf))
            || (FALSE == PPPIsL2TPTunnelModeLAC (pIf)))
        {
            PPPSendDataToL2TP (pIf, pOutPDU, Length);
            return;
        }
    }
#endif

    PPP_TRC2 (DUMP, "Link: %d ; Length: %d", pIf->LinkInfo.IfIndex, Length);
    PPP_PKT_DUMP (DUMP, pOutPDU, Length, "[Tx]: ");

    /* if the PPP interface is over ethernet give the pkt to PPPoE */
    if (pIf->LinkInfo.pPppoeSession != NULL)
    {
        PPPSendDataToPPPoE (pIf->LinkInfo.pPppoeSession, pOutPDU, Length);
        return;
    }

#if FRAMING
    if (pIf->LinkInfo.IfID.IfType == SONET_IF_TYPE)
    {
        PPPSendDataToPPPoS (pOutPDU, (UINT2) (pIf->LinkInfo.IfIndex), PPP_TO_LL,
                            Protocol);
        return;
    }
#endif

#ifdef CFA_WANTED
    CfaHandlePktFromPpp (pOutPDU, pIf->LinkInfo.IfIndex, PPP_TO_LL, Protocol);
#endif
    PPP_TRC (CONTROL_PLANE, PPP_TRC_LINE_SEPERATOR);

    return;
}

/***************************************************************************
  Function Name     :             GetVJCompAvailability 
  Description       :  This routine is used to get info. on availability of 
                       VJ compression.
  Parameters        :  VOID
  Return Values     :  VOID
 ***************************************************************************/
VOID
GetVJCompAvailability (VOID)
{
    VJCompAvailable = PPP_YES;
}

/***************************************************************************
  Function Name     :           GetFCSAvailability 
  Description       :  This routine is used to get information on type of
                       the FrameCheckSequence available in the system. 
  Parameters        :  VOID
  Return Values     :  VOID
 ***************************************************************************/
VOID
GetFCSAvailability (VOID)
{
    FCSAvailability |= FCS_16_AVAILABLE;
    FCSAvailability |= FCS_32_AVAILABLE;
}

/***************************************************************************
  Function Name     :          GetIfSpeed 
  Description       :  This routine is used to get information on
                       the speed of the interface based  on the lower layer
                       interface type.
  Parameters        :
  * pIf             -  This is ppp interface , whose speed has to be 
                     identified based on the lower layer interface layer.
  return value      : It returns the speed of the interface.
 ***************************************************************************/
UINT4
GetIfSpeed (tPPPIf * pIf)
{
    /* CFA Function will be called here to determine the speed */

    switch (pIf->LinkInfo.IfID.IfType)
    {
        case SERIAL_IF_TYPE:
            return (19200);
        case ISDN_IF_TYPE:
            return (32000);
        case FR_IF_TYPE:
            return (64000);
        case X25_IF_TYPE:
            return (9600);
        case ATM_IF_TYPE:
            return (25000000);
        default:
            return (64000);
    }
}

/***************************************************************************
  Function Name     :          PPPIndicateAsyncParamsToLL 
  Description       :  This routine is used to set information on
                       async params based on the negotiated FCS and 
                       asynchronous control character map information.
  Parameters        :
  pIf               -  This is the ppp interface , whose  asynchronous 
                       information has to be set.
  Flag          - This indicates whether the Asynchronous parameters
                 are negotiated or not over the pIf.
  return values     : VOID.
 ***************************************************************************/
VOID
PPPIndicateAsyncParamsToLL (tPPPIf * pIf, UINT1 Flag)
{
    if ((pIf->LinkInfo.IfID.IfType != ASYNC_IF_TYPE)
        && (pIf->LinkInfo.IfID.IfType != SONET_IF_TYPE))
        return;

    if (Flag == NEGOTIATED_ASYNC_VALS)
    {
        pIf->AsyncParams.TxFCSSize =
            (UINT1) ((pIf->LcpOptionsAckedByLocal.FCSSize ==
                      NO_FCS_REQ) ? 0 : pIf->LcpOptionsAckedByLocal.FCSSize *
                     8);
        pIf->AsyncParams.RxFCSSize =
            (UINT1) ((pIf->LcpOptionsAckedByPeer.FCSSize ==
                      NO_FCS_REQ) ? 0 : pIf->LcpOptionsAckedByPeer.FCSSize * 8);
        pIf->AsyncParams.pTxACCMap = pIf->ExtTxACCM;
        pIf->AsyncParams.pRxACCMap = pIf->LcpOptionsAckedByPeer.AsyncMap;

        CfaIfmHandleAsyncParamsUpdate ((UINT2) (pIf->LinkInfo.PhysIfIndex),
                                       &(pIf->AsyncParams));

    }
    else
    {
        pIf->AsyncParams.TxFCSSize = DEF_FCS_SIZE;
        pIf->AsyncParams.RxFCSSize = DEF_FCS_SIZE;
#ifdef FRAMING
        pIf->AsyncParams.pTxACCMap = gDefaultTxACCM;
        pIf->AsyncParams.pRxACCMap = gDefaultRxACCM;
#endif
    }
    return;
}

/* Makefile changes - funtion returns an aggregate */

/***************************************************************************
  Function Name     :     GetIfIndex          
  Description       :  This routine is used to get the interface index 
                      based on Iftype (i.e whether it's MP_TYPE or  PPP_TYPE

  Parameters        :
  
  Iftype            -  This is the interface type, which specifies 
                       whether the interface is of MP_TYPE or PPP_TYPE.
  
   pIfId               pointer to the IfID.
  
  return values     : VOID.
 ***************************************************************************/
VOID
GetIfIndex (UINT1 IfType, tPPPIfId * pIfId)
{

    static UINT1        PPPifIndex = 0;
    static UINT1        MPifIndex = 0;

    if (IfType == MP_TYPE)
    {
        MPifIndex = (UINT1) (MPifIndex + 5);
        pIfId->porttype = MPifIndex;
    }
    else
    {
        if ((++PPPifIndex % 5) == 0)
            ++PPPifIndex;
        pIfId->porttype = PPPifIndex;
    }
    return;
}

/***************************************************************************
  Function Name     :  MPIndicateBundleStatus        
  Description       :  This routine is used to indicate the bundle's operational                        status to NM. 
  Parameters        :
  *  pBundleIf      -  This is the Bundle interface , whose  status 
                       information has to be sent to NM.
  return values     :  VOID.
 ***************************************************************************/
VOID
MPIndicateBundleStatus (tPPPIf * pBundleIf)
{
    PPP_UNUSED (pBundleIf);
}

/***************************************************************************
  Function Name     :      PPPLLICallTerminate       
  Description       :  This routine is used to  inform the lower layer to
                       terminate the call.
  Parameters        :
  *  pIf            -  This is the ppp interface , whose LL has to be 
                       informed. 
  return values     : VOID 
 ***************************************************************************/
VOID
PPPLLICallTerminate (tPPPIf * pIf)
{
    UINT1               u1Mode = 0;
    UNUSED_PARAM (u1Mode);

#ifdef DPIF
    PPPLLICallTerminateToDP (pIf);
#endif
    /* if the lower layer is ethernet ( PPPoE! ) terminate the session
     * else just return ! */
    if ((pIf->LinkInfo.IfID.IfType == PPP_OE_IFTYPE)
        && (pIf->LinkInfo.pPppoeSession != NULL))
        PPPoETlfEventFromPPP (pIf);
#ifdef PPTP_WANTED
    else if (pIf->LinkInfo.IfID.IfType == PPTP_IF_TYPE)
    {
        if (PptpGetMode (pIf->LinkInfo.IfIndex) == 1 /*PPTP_PNS */ )
        {
            PPPSendEventToPPPTask (pIf->LinkInfo.IfIndex, PPP_CLOSE);
            PPPSendEventToPPPTask (pIf->LinkInfo.IfIndex, PPP_DOWN);
            PptpStopConnReq (gu4PptpRemoteServerIpAddr);
        }
    }
#endif
#ifdef L2TP_WANTED
    else if (pIf->LinkInfo.IfID.IfType == ASYNC_IF_TYPE)
    {
        PPPIndicateLCPDownToL2TP (pIf);
    }
#else
    /* Giving the DOWN indic back to PPP so that 
     * it can move from STOPPED state to the 
     * STARTING state, if this PPP entity has not 
     * been CLOSED.
     */
    else if ((pIf->LinkInfo.IfID.IfType == ASYNC_IF_TYPE) ||
             (pIf->LinkInfo.IfID.IfType == SERIAL_IF_TYPE))
    {
        PPPSendEventToPPPTask (pIf->LinkInfo.IfIndex, PPP_DOWN);
    }
#endif
    else if (pIf->LinkInfo.IfID.IfType == SONET_IF_TYPE)
    {
        MEM_FREE (pIf->pSonetIf);
    }

    return;
}

/***************************************************************************
  Function Name     :      PPPLLICallSetUp
  Description       :  This routine is used to  inform the lower layer to
                       setup  the call.
  Parameters        :
  * pIf             -  This is the ppp interface , whose LL has to be 
                       informed. 
  return values     :  VOID.
 ***************************************************************************/
VOID
PPPLLICallSetUp (tPPPIf * pIf)
{
    tCfaIfInfo          IfInfo;
    UINT4               u4RetVal = OSIX_FAILURE;
    UINT4               u4Index = 0;

#ifdef DPIF
    PPPLLICallSetUpToDP (pIf);
#endif
    /* if the lower layer is ethernet ( PPPoE! ) Indicate PPPoE module to 
     * start the session.Else just return !.
     * */
    if ((pIf->LinkInfo.IfID.IfType == PPP_OE_IFTYPE) ||
        (pIf->LinkInfo.IfID.IfType == PPP_OE_OUSB_IFTYPE))
        u4RetVal = PPPoETlsEventFromPPP (pIf);
#ifdef PPTP_WANTED
    else if (pIf->LinkInfo.IfID.IfType == PPTP_IF_TYPE)
        u4RetVal = PPTPEventFromPPP ();
#endif
#ifdef L2TP_WANTED
    else if (pIf->LinkInfo.IfID.IfType == ASYNC_IF_TYPE)
    {
        u4RetVal = LACDialOut ();
    }
#else
    else if ((pIf->LinkInfo.IfID.IfType == ASYNC_IF_TYPE) ||
             (pIf->LinkInfo.IfID.IfType == SERIAL_IF_TYPE))
    {
        /* Giving the UP indic back to PPP so that 
         * it can send a Config Request and move from 
         * the STARTING state to the REQ_SENT state.
         */
        CfaGetIfInfo (pIf->LinkInfo.IfIndex, &IfInfo);
        if ((IfInfo.u1IfOperStatus == CFA_IF_UNK) ||
            (IfInfo.u1IfOperStatus == CFA_IF_DOWN))
        {
            /* Lower layer oper UP */
            PPPSendEventToPPPTask (pIf->LinkInfo.IfIndex, PPP_UP);
        }
        u4RetVal = OSIX_SUCCESS;
    }
#endif

    if (u4RetVal == OSIX_FAILURE)
    {
        u4Index = pIf->LinkInfo.IfIndex;
        if (u4Index == 0)
        {
            PPP_TRC (ALL_FAILURE,
                     "PPPLLICallSetUp Exit Failure... u4Index < 0\n");
            return;
        }
        PPPSendEventToPPPTask (u4Index, PPP_CLOSE);
        PPPSendEventToPPPTask (u4Index, PPP_DOWN);
    }
    return;
}

/***************************************************************************
  Function Name     :          MPCheckForInSequence
  Description       :  This routine is used to get the sequence flag for 
                       any NCP protocol.
  Parameters        :
  * Protocol        -  This is the NCP protocol value. can be IPCP, IPXCP ,...

  return values     : It returns the sequenceflag for the corresponding NCP.
 ***************************************************************************/
UINT1
MPCheckForInSequence (UINT2 Protocol)
{
    struct tag
    {
        UINT2               Protocol;
        UINT2               SequenceFlag;
    }

    /* If any protocol doesn't want packets to be delivered in order to
     * the network layer, we can mark the value as NOT_MUST.
     * This function will return MUST by default, i.e., All network layer
     * packets will be transmitted with MP_HEADER
     * */

    SeqTable[MAX_NCP] =
    {
        {
        IP_DATAGRAM, MUST}
        ,
        {
        0, NOT_MUST}
    };

    UINT2               Index;

    for (Index = 0; SeqTable[Index].Protocol != 0;)
    {
        if (SeqTable[Index].Protocol == Protocol)
        {
            return ((UINT1) SeqTable[Index].SequenceFlag);
        }
        Index++;
        if (Index >= MAX_NCP)
        {
            break;
        }
    }
    return (MUST);
}

/***************************************************************************
  Function Name     :          TakeLCPCallBackAction
  Description       :  This routine is used to get info. on whether
                       callback should be done immediately or after some time.
  Parameters        :
  * pIf             -  This is the ppp interface , whose callback info. has
                       to be identified.
                       
  return values     : It returns whether call back action has to be performed 
                      immediately or how long we have to wait until we perform 
                      callback action.
 ***************************************************************************/
UINT1
TakeLCPCallBackAction (tPPPIf * pIf)
{

    PPP_DBG1 ("TimeBeforeDisconnect : %d",
              pIf->CallBackTimeInfo.TimeBeforeDisconnect);

    if (pIf->CallBackTimeInfo.TimeBeforeDisconnect == 0)
    {
        pTermGSEM = &pIf->LcpGSEM;
        if (pIf->CallBackTimeInfo.TimeToDialAfterDisconnect != 0)
        {
            pIf->CallBackTimeInfo.CallBackNegFlags |= CALL_BACK_ACKED;
            pIf->CallBackTimeInfo.CallBackDialTimer.Param1 =
                PTR_TO_U4 (&pIf->LcpGSEM);
            PPPStartTimer (&pIf->CallBackTimeInfo.CallBackDialTimer,
                           CALLBACK_DIAL_TIMER,
                           pIf->CallBackTimeInfo.TimeToDialAfterDisconnect);
        }
        return (IMMEDIATE);
    }

    pIf->CallBackTimeInfo.CallBackDisconnectTimer.Param1 =
        PTR_TO_U4 (&pIf->LcpGSEM);
    PPPStartTimer (&pIf->CallBackTimeInfo.CallBackDisconnectTimer,
                   CALLBACK_DISC_TIMER,
                   pIf->CallBackTimeInfo.TimeBeforeDisconnect);
    return (DELAYED);

}

/***************************************************************************
  Function Name     :   CallBackDisconnectTimeOut       
  Description       :  This routine is used to set information on
                       howlong we should wait to perform callback action 
                       after disconnecting the link.
  Parameters        :
  *  pGSEM          -  This is the pointer to the SEM corresponding to the
                       interface in which the call back action has to be done.
  return values      : VOID.
 ***************************************************************************/
VOID
CallBackDisconnectTimeOut (VOID *pSem)
{
    tPPPIf             *pIf;
    tGSEM              *pGSEM;
    pGSEM = (tGSEM *) pSem;
    pIf = pGSEM->pIf;
    if (!(pIf->CallBackTimeInfo.CallBackNegFlags & CALL_BACK_ACKED))
    {
        GSEMRun (pGSEM, RXJ_MINUS);
        pIf->CallBackTimeInfo.CallBackNegFlags |= CALL_BACK_ACKED;

        if (pIf->CallBackTimeInfo.TimeToDialAfterDisconnect == 0)
        {
            return;
        }
        pIf->CallBackTimeInfo.CallBackDialTimer.Param1 = PTR_TO_U4 (pGSEM);
        PPPStartTimer (&pIf->CallBackTimeInfo.CallBackDialTimer,
                       CALLBACK_DIAL_TIMER,
                       pIf->CallBackTimeInfo.TimeToDialAfterDisconnect);

        pIf->CallBackTimeInfo.CallBackNegFlags |= CALL_BACK_ACKED;

    }
    return;
}

/***************************************************************************
  Function Name     :    PPPCreateIf        
  Description       :  This routine is used to create a PPP interface.
  Parameters        :
  pIfId           - This is   the interface information.
  IfIndex         - this is the logical index value.
  PhyIfInde       - This is Physical interface index.
  return value      : PPP_SUCCESS/PPP_FAILURE
 ***************************************************************************/
INT4
PPPCreateIf (UINT4 u4IfIndex)
{
    tPPPIfId            IfId;

    IfId.porttype = (UINT1) u4IfIndex;
    IfId.IfType = NONE_IF_TYPE;

    if (LCPCreateIf (&IfId, u4IfIndex, 0) == NULL)
    {
        return PPP_FAILURE;
    }
    else
    {
        PPP_TRC1 (CONTROL_PLANE, "PPP Interface [ifIndex : %d] created\n",
                  u4IfIndex);
        return PPP_SUCCESS;
    }
}

/***************************************************************************
  Function Name     :  CallBackDialTimeOut        
  Description       :  This routine is used to setup a call whenever 
                       callbackdialtimeout occurs.
  Parameters        :
  * pGSEM           -  This is the pointer to the SEM corresponding to the
                       interface in which call back action has to be done.
  return values     :  VOID 
 ***************************************************************************/
VOID
CallBackDialTimeOut (VOID *pSem)
{
    tGSEM              *pGSEM;
    pGSEM = (tGSEM *) pSem;
    if (pGSEM->pIf->LinkInfo.IfID.IfType != PPP_OE_IFTYPE)
    {
        GSEMRun (pGSEM, PPP_DOWN);
        GSEMRun (pGSEM, PPP_UP);
    }
    /******* SKR003 ****************/
    /********* To Initiate the Lower Layer to start Dialing Procedure *******/
    PPPLLICallSetUp (pGSEM->pIf);
}

/**************************************************************** 
    IDLE TIMER CONFIGURATION TO BE TAKEN CARE BY THE TARGET SYSTEM
*****************************************************************/

/***************************************************************************
  Function Name     :  FrLinkUp        
  Description       :  This Function used by the FR to inform the PPP that the
                       FR link is ready to operate. This calls PPPLowerLinkUp()
                              to generate an UP event to the SEM of the interface.
  Calling Condition :  When the status of the FR becomes active
  Access Previleges :  Invoked by LLI Module.
  Input(s)          :  IfIndex - interface index of FR
                              Dlci - VC connection identifier.
  Output(s)         :  None.
  return values     :  None.
 ***************************************************************************/
VOID
FRLinkUp (UINT2 IfIndex, UINT2 Dlci)
{

    PPP_UNUSED (IfIndex);
    PPP_UNUSED (Dlci);

}

/***************************************************************************
  Function Name     :  FrLinkDown      
  Description       :  This Function used by the FR to inform the PPP that the
                       FR link is not ready to operate. This calls
                              PPPLowerLinkDown() to generate an Down event to the 
                              SEM of the interface.
  Calling Condition :  When the status of the FR becomes inactive
  Access Previleges :  Invoked by LLI Module.
  Input(s)          :  IfIndex - interface index of FR
                              Dlci - VC connection identifier.
  Output(s)         :  None.
  return values     :  None.
 ***************************************************************************/
VOID
FRLinkDown (UINT2 IfIndex, UINT2 Dlci)
{
    PPP_UNUSED (IfIndex);
    PPP_UNUSED (Dlci);

}

/***************************************************************************
  Function Name     :  FrmRxFR        
  Description       :  This Function receives the PPP packet from the FR
                       and passes it to the appropriate module within
                              the PPP subsystem.
  Calling Conditions:  Whenever a ppp packet is received in FR, this function
                       is called.
  Access Previleges :  Invoked by LLI Module.
  Input(s)          :  pInPDU - pointer to the packet received from the FR
                       Length - length of the recveived PDU.
                              IfIndex - interface index of FR
                              Dlci - VC connection identifier.
  Output(s)         :  None.
  return values     :  None.
 ***************************************************************************/
VOID
FrmRxFR (VOID *pInPDU, UINT2 Length, UINT2 IfIndex, UINT2 Dlci)
{
    UINT2               Protocol;
    UINT2               Offset;
    UINT2               PktDlci;
    UINT1               PktCtrl;
    UINT1               PktFlag;
    UINT1               PktNlpid;
    t_MSG_DESC         *pFRPkt;
    tPPPIf             *pIf = NULL;

    Protocol = 0;
    Offset = 0;

    PPP_UNUSED (Dlci);

    pFRPkt = (t_MSG_DESC *) pInPDU;

    GET1BYTE (pFRPkt, Offset, PktFlag);
    Offset++;
    GET2BYTE (pFRPkt, Offset, PktDlci);
    Offset = (UINT2) (Offset + 2);
    GET1BYTE (pFRPkt, Offset, PktCtrl);
    Offset++;
    GET1BYTE (pFRPkt, Offset, PktNlpid);
    Offset++;

    PPP_UNUSED (PktNlpid);

    if ((PktFlag != FR_FLAG_VALUE) && (PktCtrl != FR_CTRL_VALUE) &&
        (PktFlag != FR_NLPID_VALUE))
    {
        DISCARD_PKT (pIf, "Invalid FR pkt. Discarded!!");
        return;
    }

    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if ((pIf->LinkInfo.IfID.IfType == FR_IF_TYPE) &&
            (pIf->LinkInfo.IfID.VCNumber == PktDlci) &&
            (pIf->LinkInfo.PhysIfIndex == IfIndex))
        {                        /*PPP Lower layer index */
            break;
        }
    }

    if (pIf == NULL)
    {
        DISCARD_PKT (pIf,
                     "PPP interface isn't found for the FR Link. Discarded!!");
        return;
    }
    /* Update the Idle timer */
    if (OsixGetSysTime (&pIf->LinkInfo.LastPktArrivalTime) == OSIX_FAILURE)
    {
        PPP_TRC (OS_RESOURCE, "Failure in  OsixGetSysTime()");
    }
    pIf->LLHCounters.InNUniPackets++;
    pIf->LLHCounters.InOctets += Length;

    pIf->LinkInfo.RxAddrCtrlLen = 0;
    pIf->LinkInfo.RxProtLen = 0;

    pIf->LinkInfo.RxAddrCtrlLen =
        (UINT1) (pIf->LinkInfo.RxAddrCtrlLen + FR_ADDR_LEN);

    /* Check protocol field compression */
    if (PPPLLIGetProtField
        (pIf, pFRPkt, &Protocol, (UINT1) (Offset), CALLED_FROM_LLI) == DISCARD)
    {
        DISCARD_PKT (pIf, "Invalid pkt. Discarded!!");
        return;
    }

    Offset = (UINT2) (Offset + pIf->LinkInfo.RxProtLen);

    /* Update the Length by the bytes so far read */
    Length = (UINT2) (Length - Offset);

    if (Length > pIf->LcpOptionsAckedByPeer.MRU)
    {
        DISCARD_PKT (pIf, "LLI : Packet recd. is too Long !!");
        pIf->LLHCounters.PacketTooLongCounter++;
        return;
    }

    /* 
       Any non-LCP packets received, during the Link Establishment phase,
       should be silently discarded. 
     */
    /* For BAP/BACP compliance, data packets can be accepted */
    if ((pIf->LcpStatus.LinkAvailability == LINK_NOT_AVAILABLE)
        && (Protocol != LCP_PROTOCOL) && ((Protocol & 0xff00) != 0))
    {

        DISCARD_PKT (pIf, "Pkt. not acceptable in this phase ");
        return;
    }

    if (PPPLLIProcessPID
        (pIf, pFRPkt, Protocol, Offset, CALLED_FROM_LLI,
         Length) == NOT_PROCESSED)
    {
        pIf->LLHCounters.InUnknownProtos++;
        if (PPPSendRejPkt (pIf, pFRPkt, Length, LCP_PROTOCOL, PROT_REJ_CODE) ==
            FALSE)
        {
        }
    }

    if (pRestartGSEM != NULL)
    {
        GSEMRestartIfNeeded (pRestartGSEM);
        pRestartGSEM = NULL;
    }
    if (pTermGSEM != NULL)
    {
        GSEMRun (pTermGSEM, RXJ_MINUS);
        pTermGSEM = NULL;
    }
    return;

}

/***************************************************************************
  Function Name     :  FrmTxFR        
  Description       :  This Function sends the PPP packet to the FR
                       which prepends the flag(0x7e), Q.992 address, control
          field and NLPID(oxcf) to the ppp packet and queues
          packets for transmission.
  Calling Condition :  Called whenever a packet needs to be transmitted over FR
                       Link.
  Access Previleges :  Invoked by LLI Module.
  Input(s)          :  pOutPDU - pointer to the packet to be transmitted   
                       Length - length of the PDU to be transmitted.
          IfIndex - interface index of FR
          Dlci - VC connection identifier.
  Output(s)         :  None.
  return values     :  None.
 ***************************************************************************/

VOID
FrmTxFR (VOID **pOutPDU, UINT2 *pLength, UINT2 IfIndex, UINT2 Dlci)
{
    t_MSG_DESC         *pFRPkt;

    pFRPkt = (t_MSG_DESC *) * pOutPDU;
    PPP_UNUSED (IfIndex);

    MOVE_BACK_PTR (pFRPkt, FR_ADDR_LEN);
    APPEND1BYTE (pFRPkt, FR_FLAG_VALUE);
    APPEND2BYTE (pFRPkt, Dlci);
    APPEND1BYTE (pFRPkt, FR_CTRL_VALUE);
    APPEND1BYTE (pFRPkt, FR_NLPID_VALUE);
    *pLength = (UINT2) VALID_BYTES (pFRPkt);
}

/***************************************************************************
  Function Name     :  DisplayMessage      
  Description       :  This Function used to display the string of particular
                       language identified by the CharSet and LangTag 
          parameters. This call should be ported for different
                       language supported in the system.          
  Calling Condition :  Called to print strings                   
  Access Previleges :  Invoked by LCP and Authentication module.
  Input(s)          :  pIf - pointer to the PPP Interface structure.
                       pStr - pointer to the display string.
                              Length of the string.
                              CharSet - number identifies particular charecter set
                              LangTag - pointer to the language tag name.
  Output(s)         :  None.
  return values     :  None.
 ***************************************************************************/
VOID
DisplayMessage (tPPPIf * pIf, VOID *pStr, UINT2 Length, UINT4 CharSet,
                UINT1 *LangTag)
{

    PPP_UNUSED (pIf);
    PPP_UNUSED (pStr);
    PPP_UNUSED (Length);
    PPP_UNUSED (CharSet);
    PPP_UNUSED (LangTag);
    switch (CharSet)
    {
        case CHAR_SET_UTF8:
            /* Call the fn which actually writes the chars on the 
             * output screen/device *
             */
            break;
        default:
            break;
    }

}

UINT4
PppGenerateMagicNumber (VOID)
{
    static UINT4        u4CurTick;

    INIT_RANDOM_GENERATOR ();
    u4CurTick = (UINT4) (u4CurTick + (UINT4) GET_RANDOM_NUMBER ());
    return u4CurTick;
}

INT4
PppHandlePacketFromHl (t_MSG_DESC * pBuf, UINT4 u4OutgoingIfIndex,
                       UINT4 u4PktSize, UINT2 u2Protocol)
{

    PPP_GET_MODULE_DATA_IF_NUMBER (pBuf) = u4OutgoingIfIndex;
    PPP_GET_MODULE_DATA_PROTOCOL (pBuf) = u2Protocol;
    PPP_GET_SRC_MODID (pBuf) = PPPMSG_FROM_HIGHER_LAYER;

    PPP_UNUSED (u4PktSize);

    if (OsixSendToQ (SELF, PPP_QUEUE_NAME, pBuf, OSIX_MSG_NORMAL) !=
        OSIX_SUCCESS)
    {
        PPP_TRC (ALL_FAILURE, "Send to PPP Queue Failure.");
        return PPP_FAILURE;
    }
    if (OsixSendEvent (SELF, PPP_TASK_NAME, PPP_QUEUE_EVENT) != OSIX_SUCCESS)
    {
        PPP_TRC (ALL_FAILURE, "PppHigher Layer Message Failure.");
        return PPP_FAILURE;
    }

    return PPP_SUCCESS;

}

INT4
PppHandlePacketFromLl (t_MSG_DESC * pBuf, UINT4 u4IncomingIfIndex,
                       UINT4 u4PktSize, UINT1 u1EncapType)
{
    PPP_UNUSED (u4PktSize);
    PPP_UNUSED (u1EncapType);
    PPP_GET_MODULE_DATA_IF_NUMBER (pBuf) = u4IncomingIfIndex;
    PPP_GET_SRC_MODID (pBuf) = PPPMSG_FROM_LOWER_LAYER;

    if (OsixSendToQ (SELF, PPP_QUEUE_NAME, pBuf, OSIX_MSG_NORMAL) !=
        OSIX_SUCCESS)
    {
        PPP_TRC (ALL_FAILURE, "Send to Ppp Queue Failure.");
        return PPP_FAILURE;
    }
    if (OsixSendEvent (SELF, PPP_TASK_NAME, PPP_QUEUE_EVENT) != OSIX_SUCCESS)
    {
        PPP_TRC (ALL_FAILURE, "PppLower Layer Message Failure.");
        return PPP_FAILURE;
    }

    return PPP_SUCCESS;

}

/* This function give the UP/DOWN/OPEN/CLOSE events to the PPP task.
 * Currenly this is used only by PPPoE Module
 * */
VOID
PPPSendEventToPPPTask (UINT4 u4IfIndex, UINT1 Event)
{

    t_MSG_DESC         *pBuf;

    if ((pBuf = (t_MSG_DESC *) (VOID *) ALLOCATE_BUFFER (8, 0)) == NULL)
        return;
    PPP_GET_SRC_MODID (pBuf) = PPPEVENT_TO_PPP_GSEM;

    if ((Event != PPP_DESTROY) && (Event > RXR))
    {
        PPP_TRC (CONTROL_PLANE, "Ileagal Event to PPP Sem");
        return;
    }

    if (Event < TO_PLUS)
    {
        PPP_TRC1 (CONTROL_PLANE, "Sending  Event : [ %s ] to PPP SEM",
                  gaPPPDebEvent[Event]);
    }

    ASSIGN4BYTE (pBuf, 0, u4IfIndex);
    ASSIGN1BYTE (pBuf, 4, Event);

    PPP_GET_SRC_MODID (pBuf) = PPPEVENT_TO_PPP_GSEM;

    if (OsixSendToQ (SELF, PPP_QUEUE_NAME, pBuf, OSIX_MSG_NORMAL) !=
        OSIX_SUCCESS)
    {
        PPP_TRC (ALL_FAILURE, "Send to Ppp Queue Failure.");
        RELEASE_BUFFER (pBuf);
        return;
    }
    if (OsixSendEvent (SELF, PPP_TASK_NAME, PPP_QUEUE_EVENT) != OSIX_SUCCESS)
    {
        PPP_TRC (ALL_FAILURE, "Ppp Queue Event Failure.");
        return;
    }

    return;
}

/***************************************************************************
  Function Name     :  PPPIsLLIL2TP   
  Description       :  This Function used to check the Lower layer inter
                       face is L2TP or Not for the Given PPP Interface.
  Input(s)          :  u4IfIndex - PPP IfIndex
  Output(s)         :  None.
  return values     :  PPP_SUCCESS / PPP_FAILURE
 ***************************************************************************/
UINT4
PPPIsLLIL2TP (UINT4 u4IfIndex)
{
    tPPPIf             *pIf = NULL;

    pIf = PppGetIfPtr (u4IfIndex);
    if (pIf != NULL)
    {
        if (pIf->u1IsCreatedForL2tp == PPP_TRUE)
        {
            return (PPP_SUCCESS);
        }
    }
    return (PPP_FAILURE);
}

/***************************************************************************
  Function Name     :  PPPGetFreePoolIndex
  Description       :  This Function used to get a free Pool Index based on 
                       the passed 'index' argument. If the index value passed
                       is not free, then the Index is selected on incremental
                       basis.
  Input(s)          :  Index - Pool Index
  Output(s)         :  None.
  return values     :  the allocated index on SUCCESS / 0 on FAILURE
***************************************************************************/
UINT4
PPPGetFreePoolIndex (UINT4 Index)
{
    /* give some new pool index and update the same in vpdnGroup struct */
    tIPCPAddrPool      *tempPool = NULL;
    UINT4               maxPool = 0;

    maxPool = Index + PPP_MAX_IP_POOL;

    while (Index < maxPool)
    {
        tempPool = IPCPGetOrCreateAddrPool (Index);
        if (tempPool != NULL && tempPool->u1PoolStatus == IPCP_DELETE)
        {
            /* make sure that we break the loop with free index */
            return Index;
        }
        Index++;
    }
    return 0;
}

/*****************************************************************************/
/*    Function Name       : PPPportGetIfIndexFromSessionId                   */
/*    Description         : This function fetches the PPP interface index    */
/*                          corresponding to the session id.                 */
/*    Input(s)            : u2SessionId - PPPoE session identifier.          */
/*    Output(s)           : pu4IfPppIndex - PPP interface index corresponding*/
/*                                          to the PPPoE session ID.         */
/*    Global Variables Referred : None.                                      */
/*    Global Variables Modified : None.                                      */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*    Use of Recursion        : None.                                        */
/*    Returns            : OSIX_FAILURE -> When there does not exist a       */
/*                                         session corressponding to the     */
/*                                         u2SessionID.                      */
/*                         OSIX_SUCCESS -> If there exists a session         */
/*                                         in the global PPPoE session list. */
/*****************************************************************************/
INT4
PPPportGetIfIndexFromSessionId (UINT2 u2SessionId, UINT4 *pu4IfPppIndex)
{
    tPPPoESessionNode  *pNode = NULL;

    PPP_LOCK ();
    SLL_SCAN (&PPPoESessionList, pNode, tPPPoESessionNode *)
    {
        if (pNode->u2SessionId == u2SessionId)
        {
            *pu4IfPppIndex = pNode->pIf->LinkInfo.IfIndex;
            PPP_UNLOCK ();
            return OSIX_SUCCESS;
        }
    }
    PPP_UNLOCK ();
    return OSIX_FAILURE;
}
/*****************************************************************************/
/*     FUNCTION NAME    : PppGetPeerIpAddress                                */
/*                                                                           */
/*     DESCRIPTION      : Gets PPP Server Address for PPP Client             */
/*                                                                           */
/*     INPUT            : tCliHandle - Handle to the Cli Context             */
/*                        i4Index - Index of the ppp interface               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/
INT4
PppGetPeerIpAddress (UINT4 u4IfIndex, UINT4 *pu4PeerIpAddr)
{
    INT4                i4PPPoeMode = 0;
    INT4		i4RetVal = 0;

    *pu4PeerIpAddr = 0;
    nmhGetPPPoEMode (&i4PPPoeMode);
    if (i4PPPoeMode == PPPOE_SERVER)
    {
	return CLI_FAILURE;
    }

    PPP_LOCK ();
    i4RetVal = IPCPGetPeerIpAddr (pu4PeerIpAddr, u4IfIndex);
    PPP_UNLOCK ();

    UNUSED_PARAM (i4RetVal);
    return CLI_SUCCESS; 
}
