/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppl2tp.c,v 1.3 2011/07/12 11:04:38 siva Exp $
 *
 * Description: Contains interface with FutureL2TP module Contains interface with FutureL2TP module
 *
 *******************************************************************/
#define __PPPL2TP_C__

#ifdef L2TP_WANTED

#include "genhdrs.h"
#include "lcpcom.h"
#include "gcpdefs.h"
#include "globexts.h"
#include "authsnmp.h"
#include "fssnmp.h"

#include "pppl2tp.h"
#include "radius.h"
#include "gsemdefs.h"
#include "frmdefs.h"
#include "pppmd5.h"
#include "l2tp.h"
#include "dhcp.h"
#include "pppipcp.h"
#include "pppproto.h"
#define MSAD_WAN_PPP_INT_NAME             "ppp0"
#define IP_ADDR_STR_LEN                    17
#define DHCPSRV_CLI_OPTVAL_SIZE            256
#define DHCP_OPT_DNS_NS_LEN                8
#define DHCP_OPT_DNS_NS                    6

extern tSecurityInfo IndexZeroSecurityInfo;

/********************************************************************
 *  Function Name   : DhcpSrvGetDnsAddresses                           
 *  Description     : This function is used to get the primary and 
 *                    secondary dns server addresses that are 
 *                    configured as dhcp generic option.
 *  Input(s)        : None                                        
 *  Output(s)       : pu4PriDnsIpAddr - primary dns server address
 *                    pu4SecDnsIpAddr - secondary dns server address
 *  Returns         : None
 ********************************************************************/
VOID
DhcpSrvGetDnsAddresses (UINT4 *pu4PriDnsIpAddr, UINT4 *pu4SecDnsIpAddr)
{
    UINT1               au1Array[IP_ADDR_STR_LEN];
    UINT1               au1Data[DHCPSRV_CLI_OPTVAL_SIZE];
    UINT4               u4IpAddr = 0;
    INT4                i4NextPoolIndex = 0;
    INT4                i4NextOptType = 0;
    INT4                i4PoolIndex = 0;
    INT4                i4OptType = 0;
    UINT4               u4OptionLen = 0;
    tSNMP_OCTET_STRING_TYPE OptionValue;

    if (nmhGetFirstIndexDhcpSrvSubnetOptTable (&i4PoolIndex, &i4OptType) ==
        SNMP_SUCCESS)
    {
        i4NextPoolIndex = i4PoolIndex;
        i4NextOptType = i4OptType;
        do
        {
            OptionValue.pu1_OctetList = au1Data;
            OptionValue.i4_Length = 0;
            i4PoolIndex = i4NextPoolIndex;
            i4OptType = i4NextOptType;

            nmhGetDhcpSrvSubnetOptLen (i4PoolIndex,
                                       i4OptType, (INT4 *) &u4OptionLen);

            if ((u4OptionLen == DHCP_OPT_DNS_NS_LEN) &&
                (i4OptType == DHCP_OPT_DNS_NS))
            {
                if (nmhGetDhcpSrvSubnetOptVal (i4PoolIndex,
                                               i4OptType,
                                               &OptionValue) == SNMP_SUCCESS)
                {
                    MEMSET (au1Array, 0, IP_ADDR_STR_LEN);
                    SNPRINTF (au1Array, IP_ADDR_STR_LEN, "%d.%d.%d.%d",
                              OptionValue.pu1_OctetList[0],
                              OptionValue.pu1_OctetList[1],
                              OptionValue.pu1_OctetList[2],
                              OptionValue.pu1_OctetList[3]);
                    u4IpAddr = INET_ADDR (au1Array);
                    *pu4PriDnsIpAddr = u4IpAddr;

                    MEMSET (au1Array, 0, IP_ADDR_STR_LEN);
                    SNPRINTF (au1Array, IP_ADDR_STR_LEN, "%d.%d.%d.%d",
                              OptionValue.pu1_OctetList[4],
                              OptionValue.pu1_OctetList[5],
                              OptionValue.pu1_OctetList[6],
                              OptionValue.pu1_OctetList[7]);
                    u4IpAddr = INET_ADDR (au1Array);
                    *pu4SecDnsIpAddr = u4IpAddr;
                }
                /* Found the DNS information */
                return;
            }
        }
        while (nmhGetNextIndexDhcpSrvSubnetOptTable
               (i4PoolIndex, &i4NextPoolIndex,
                i4OptType, &i4NextOptType) == SNMP_SUCCESS);
    }
    *pu4PriDnsIpAddr = 0;
    *pu4SecDnsIpAddr = 0;
}

/* OIDs from PPP */
#define L2TP_PPP_EXT_SECURITY_OID          1, 3, 6, 1, 4, 1, 2076, 5, 1, 4
#define L2TP_PPP_EXT_SECURITY_PROTO_OID    L2TP_PPP_EXT_SECURITY_OID, 1
#define L2TP_PPP_MSCHAP_OID                L2TP_PPP_EXT_SECURITY_PROTO_OID, 1
UINT4               l2tpPppSecurityPapProtocolOIDVal[11] =
    { 1, 3, 6, 1, 2, 1, 10, 23, 2, 1, 1 };
UINT4               l2tpPppSecurityChapMD5ProtocolOIDVal[11] =
    { 1, 3, 6, 1, 2, 1, 10, 23, 2, 1, 2 };
UINT4               l2tpPppSecurityMSCHAPProtocolOIDVal[] =
    { L2TP_PPP_MSCHAP_OID };
const tSNMP_OID_TYPE l2tpPppSecurityPapProtocolOID =
    { (sizeof (l2tpPppSecurityPapProtocolOIDVal) / sizeof (UINT4)),
    l2tpPppSecurityPapProtocolOIDVal
};
const tSNMP_OID_TYPE l2tpPppSecurityChapMD5ProtocolOID =
    { (sizeof (l2tpPppSecurityChapMD5ProtocolOIDVal) / sizeof (UINT4)),
    l2tpPppSecurityChapMD5ProtocolOIDVal
};
const tSNMP_OID_TYPE l2tpPppSecurityMSCHAPProtocolOID =
    { (sizeof (l2tpPppSecurityMSCHAPProtocolOIDVal) / sizeof (UINT4)),
    l2tpPppSecurityMSCHAPProtocolOIDVal
};
UINT4               gu4L2tpPppIpAddr = 0;
UINT4               gu4L2tpIdleTime = 0;
UINT1               gu1PrefL2tpCount = 0;

VOID
PPPLACSendConfReqPkt (tPPPIf * pIf, t_MSG_DESC * pOutBuf, UINT1 Code)
{

    UINT1               IsLAC;
    t_MSG_DESC         *pL2TPForwardBuf;
    UINT2               Length;

    Length = VALID_BYTES (pOutBuf);
    if (Code == CONF_REQ_CODE)
    {

        PPP_TRC1 (CONTROL_PLANE, "L2TP:Conf Req Pkt Length %d.",
                  Length - CP_HDR_LEN);

        if ((pL2TPForwardBuf = (t_MSG_DESC *) ALLOCATE_BUFFER
             (Length - CP_HDR_LEN, 0)) != NULL)
        {

            CRU_BUF_COPY_MSGS (pL2TPForwardBuf, pOutBuf, 0,
                               (pIf->LinkInfo.RxAddrCtrlLen +
                                pIf->LinkInfo.RxProtLen + CP_HDR_LEN),
                               Length - CP_HDR_LEN);
            PPPIndicateConfReqPktToLAC (pIf, pL2TPForwardBuf,
                                        Length - CP_HDR_LEN,
                                        L2TP_SENT_LCP_CONF_REQ);
        }
        else
        {
            PPP_TRC (ALL_FAILURE,
                     "L2TP:Unable to duplicate buffer to pass it to L2TP LAC..");
        }
    }
}

VOID
PPPSendDataToL2TP (tPPPIf * pIf, t_MSG_DESC * pOutPDU, UINT4 Length)
{

    L2TPHandlePktFromPPP (pIf->LinkInfo.IfIndex, pOutPDU, Length);
    return;
}

VOID
PPPIndicateLCPUpToLNS (tPPPIf * pIf)
{
    UINT1              *pTxACCM = NULL;
    UINT1              *pRxACCM = NULL;

    if ((pIf->LcpGSEM.pNegFlagsPerIf[ACCM_IDX].FlagMask & ACKED_BY_PEER_SET)
        || (pIf->LcpGSEM.
            pNegFlagsPerIf[ACCM_IDX].FlagMask & ACKED_BY_LOCAL_SET))
    {
        pTxACCM = pIf->LcpOptionsAckedByPeer.AsyncMap;
        pRxACCM = pIf->LcpOptionsAckedByLocal.AsyncMap;
    }

    L2TPPPPLCPUpInd (pIf->LinkInfo.IfIndex, pTxACCM, pRxACCM);

}

VOID
PPPIndicateLCPDownToL2TP (tPPPIf * pIf)
{
    L2TPPPPLCPDownInd (pIf->LinkInfo.IfIndex);
}

VOID
PPPIndicateConfReqPktToLAC (tPPPIf * pIf, t_MSG_DESC * pBuf, UINT2 Len,
                            UINT1 Type)
{
    LACPPPConfReqPktInd (pIf->LinkInfo.IfIndex, pBuf, Len, Type);
}

BOOLEAN
PPPIsL2TPTunnelModeLAC (tPPPIf * pIf)
{
    return (L2TPIsTunnelModeLAC (pIf->LinkInfo.IfIndex));
}

/* DSL_ADD -S */
BOOLEAN
PPPIsL2TPSessionExist (tPPPIf * pIf)
{
    return (L2TPIsSessionExist (pIf->LinkInfo.IfIndex));
}

/* DSL_ADD -E */

BOOLEAN
PPPIsL2TPTunnelVoluntary (tPPPIf * pIf)
{
    return (L2TPIsTunnelVoluntary (pIf->LinkInfo.IfIndex));
}

VOID
PPPSetAuthOver (UINT4 IfIndex)
{

    tPPPIf             *pIf;
    pIf = PppGetIfPtr (IfIndex);
    pIf = PppGetIfPtr (IfIndex);
    pIf->LcpStatus.AuthStatus = ALL_AUTH_OVER;
    pIf->LcpGSEM.pNegFlagsPerIf[AUTH_IDX].FlagMask &= ACKED_BY_PEER_NOT_SET;
    pIf->LcpGSEM.pNegFlagsPerIf[AUTH_IDX].FlagMask &= ACKED_BY_LOCAL_NOT_SET;
    LCPUp (&pIf->LcpGSEM);
    pIf->LcpGSEM.CurrentState = OPENED;
    return;

}

/*********************************************************************
*    Function Name    :    PPPProcessLastSentConfReq
*    Description        :
*                This function is called at LNS to process Last Sent
*    LCP Config Request AVP sent by LAC.
*    Parameter(s)    :
*        pSessionIf    -    Points to the L2TP session interface structure over
*                        which the AVP is received.
*    Return Values    :    OK if the parameters in the config-request are agreeable
*                        to the local PPP, NOT_OK otherwise.
*********************************************************************/
INT1
PPPProcessLastSentConfReq (UINT4 IfIndex, t_MSG_DESC * pInPkt, UINT1 ConfMode)
{
    tPPPIf             *pIf;
    tGSEM              *pGSEM;
    tOptVal             OptVal;
    UINT2               RemLen, Offset;
    INT4                OptIndex;
    UINT1               RecvdOptFlags[MAX_LCP_OPT_TYPES];
    UINT1               OptType, OptLen, Index;

    OsixTakeSem (0, (const UINT1 *) "PSEM", OSIX_WAIT, 0);

    pIf = PppGetIfPtr (IfIndex);
    pGSEM = &(pIf->LcpGSEM);
    MEMSET (RecvdOptFlags, 0, MAX_LCP_OPT_TYPES);
    if (pInPkt != NULL)
    {
        Offset = 0;
        RemLen = VALID_BYTES (pInPkt);
        while (RemLen != 0)
        {
            if ((OptIndex =
                 (INT4) GCPExtractAndValidateOption (pGSEM, pInPkt, &OptType,
                                                     &OptLen, &OptVal, Offset,
                                                     RemLen)) < 0)
            {
                OsixGiveSem (0, (const UINT1 *) "PSEM");

                return (NOT_OK);
            }
            switch (OptType)
            {
                case MRU_OPTION:

                    if ((OptVal.ShortVal > pIf->LcpOptionsDesired.MRU))
                    {
                        OsixGiveSem (0, (const UINT1 *) "PSEM");
                        return (NOT_OK);
                    }
                    pIf->LcpOptionsAckedByPeer.MRU = OptVal.ShortVal;
                    break;
                case AUTH_OPTION:
                    if ((PPPIsAuthProtocolFound
                         (pIf->LinkInfo.IfIndex, OptVal.ShortVal) == NOT_OK))
                    {
                        OsixGiveSem (0, (const UINT1 *) "PSEM");
                        return (NOT_OK);
                    }
                    break;
                case MAGIC_OPTION:
                    if ((OptVal.LongVal == 0))
                    {
                        OsixGiveSem (0, (const UINT1 *) "PSEM");
                        return (NOT_OK);
                    }
                    pIf->LcpOptionsAckedByPeer.MagicNumber = OptVal.LongVal;
                    break;
                case ACCM_OPTION:
                    MEMCPY (pIf->LcpOptionsAckedByPeer.AsyncMap, OptVal.StrVal,
                            4);
                    break;
                case PFC_OPTION:
                case ACFC_OPTION:
                    break;
                case FCS_OPTION:
                    pIf->LcpOptionsAckedByPeer.FCSSize = OptVal.CharVal;
                    break;
                case PADDING_OPTION:
                    pIf->LcpOptionsAckedByPeer.MaxPadVal = OptVal.ShortVal;
                    break;
                default:
                    /* LQR, CALL_BACK, LINK_DISCR, MP options */
                    OsixGiveSem (0, (const UINT1 *) "PSEM");
                    return (NOT_OK);
            }
            pGSEM->pNegFlagsPerIf[OptIndex].FlagMask |= ACKED_BY_PEER_SET;
            RecvdOptFlags[OptIndex] = SET;
            RemLen -= OptLen;
            Offset += OptLen;
        }
    }
    else
    {
        PPP_DBG ("Default LCP request is been sent by LAC to client.\n");
    }

    /*
       Check if there is any option that we were configured for, but that was
       not sent by the LAC's PPP in its config request(or for which it
       received a reject). The case of authentication configured at LNS
       PPP but LAC's PPP having not sent authentication or got reject of the
       auth option is taken care by the following loop.
     */
    if (ConfMode == L2TP_DEPENDS_ON_PEER_INFO)
    {
        for (Index = 0; Index < MAX_LCP_OPT_TYPES; Index++)
        {
            if (((pGSEM->pNegFlagsPerIf[Index].FlagMask & DESIRED_MASK) ==
                 DESIRED_MASK) && (RecvdOptFlags[Index] == NOT_SET))
            {
                if ((pGSEM->pNegFlagsPerIf[Index].FlagMask & ALLOW_REJECT_MASK)
                    == ALLOW_REJECT_MASK)
                {
                    PPPGetDefaultVal (&(pIf->LcpOptionsAckedByPeer), Index);
                    pIf->LcpGSEM.pNegFlagsPerIf[Index].FlagMask &=
                        ACKED_BY_PEER_NOT_SET;
                }
                else
                {
                    PPP_DBG1
                        ("Option for which reject is not allowd was rejected. Option Index is: %d.\n",
                         Index);
                    PPP_DBG1 (FUNCTION_EXIT, FUNCTION);
                    OsixGiveSem (0, (const UINT1 *) "PSEM");
                    return (NOT_OK);
                }
            }
        }
    }
    PPP_DBG1 (FUNCTION_EXIT, FUNCTION);
    OsixGiveSem (0, (const UINT1 *) "PSEM");

    return (OK);
}

/*********************************************************************
*    Function Name    :    PPPIntProcessLastRcvdConfReq
*    Description        :
*                This function is called at LNS to process Last Received
*    LCP Config Request AVP sent by LAC.
*    Parameter(s)    :
*        pSessionIf    -    Points to the L2TP session interface structure over
*                        which the AVP is received.
*    Return Values    :    OK if the parameters in the config-request are agreeable
*                        to the local PPP, NOT_OK otherwise.
*********************************************************************/
INT1
PPPProcessLastRcvdConfReq (UINT4 IfIndex, t_MSG_DESC * pInPkt, UINT1 ConfMode)
{
    tPPPIf             *pIf;
    tGSEM              *pGSEM;
    tOptVal             OptVal;
    UINT2               RemLen, Offset;
    INT4                OptIndex;
    UINT1               RecvdOptFlags[MAX_LCP_OPT_TYPES];
    UINT1               OptType, OptLen, Index;

    PPP_DBG1 (FUNCTION_ENTRY, FUNCTION);

    pIf = PppGetIfPtr (IfIndex);
    pGSEM = &(pIf->LcpGSEM);
    MEMSET (RecvdOptFlags, 0, MAX_LCP_OPT_TYPES);
    if (pInPkt != NULL)
    {
        Offset = 0;
        RemLen = VALID_BYTES (pInPkt);
        while (RemLen != 0)
        {
            if ((OptIndex =
                 (INT4) GCPExtractAndValidateOption (pGSEM, pInPkt, &OptType,
                                                     &OptLen, &OptVal, Offset,
                                                     RemLen)) < 0)
            {
                PPP_TRC (ALL_FAILURE_TRC, "Invalid option value in Pkt.\n");
                PPP_DBG1 (FUNCTION_EXIT, FUNCTION);
                OsixGiveSem (0, (const UINT1 *) "PSEM");
                return (NOT_OK);
            }
            if ((ConfMode == L2TP_DEPENDS_ON_PEER_INFO)
                &&
                ((pGSEM->
                  pNegFlagsPerIf[OptIndex].FlagMask & ALLOWED_FOR_PEER_MASK) ==
                 NOT_SET))
            {
                PPP_TRC1 (ALL_FAILURE_TRC,
                          "LAC has acked option %d which is not allowed for peer.\n",
                          OptIndex);
                PPP_DBG1 (FUNCTION_EXIT, FUNCTION);
                OsixGiveSem (0, (const UINT1 *) "PSEM");
                return (NOT_OK);
            }
            switch (OptType)
            {
                case MRU_OPTION:
                    if ((ConfMode == L2TP_DEPENDS_ON_PEER_INFO)
                        && (OptVal.ShortVal < MIN_MRU_SIZE))
                    {
                        PPP_DBG1 (FUNCTION_EXIT, FUNCTION);
                        OsixGiveSem (0, (const UINT1 *) "PSEM");
                        return (NOT_OK);
                    }
                    pIf->LcpOptionsAckedByLocal.MRU = OptVal.ShortVal;
                    break;
                case AUTH_OPTION:
                    /* Client authentication to LNS. Better re-negotiate PPP */
                    PPP_DBG1 (FUNCTION_EXIT, FUNCTION);
                    OsixGiveSem (0, (const UINT1 *) "PSEM");
                    return (OK);
                case MAGIC_OPTION:
                    /*
                       Note a check is made whether magic no negotiated both
                       sides are not same. So the last tx AVP must be processed
                       before processing lasr rx AVP.
                     */
                    if ((ConfMode == L2TP_DEPENDS_ON_PEER_INFO)
                        && ((OptVal.LongVal == 0)
                            || (OptVal.LongVal ==
                                pIf->LcpOptionsAckedByPeer.MagicNumber)))
                    {
                        PPP_DBG1 (FUNCTION_EXIT, FUNCTION);
                        OsixGiveSem (0, (const UINT1 *) "PSEM");
                        return (NOT_OK);
                    }
                    pIf->LcpOptionsAckedByLocal.MagicNumber = OptVal.LongVal;
                    break;
                case ACCM_OPTION:
                    MEMCPY (pIf->LcpOptionsAckedByLocal.AsyncMap, OptVal.StrVal,
                            4);
                    break;
                case PFC_OPTION:
                case ACFC_OPTION:
                    break;
                case FCS_OPTION:
                    pIf->LcpOptionsAckedByLocal.FCSSize = OptVal.CharVal;
                    break;
                case PADDING_OPTION:
                    pIf->LcpOptionsAckedByLocal.MaxPadVal = OptVal.ShortVal;
                    break;
                default:
                    /* LQR, CALL_BACK, LINK_DISCR, MP options */
                    PPP_DBG1 (FUNCTION_EXIT, FUNCTION);
                    OsixGiveSem (0, (const UINT1 *) "PSEM");
                    return (NOT_OK);
            }
            pGSEM->pNegFlagsPerIf[OptIndex].FlagMask |= ACKED_BY_LOCAL_SET;
            RecvdOptFlags[OptIndex] = SET;
            RemLen -= OptLen;
            Offset += OptLen;
        }
    }
    else
    {
        PPP_DBG ("Default LCP request is been sent by client to LAC.\n");
    }

    /*
       Check if there is any option that we were configured for, but that was
       not sent by the client's PPP in its config request (or for which it
       received a reject form LAC).
     */
    if (ConfMode == L2TP_DEPENDS_ON_PEER_INFO)
    {
        for (Index = 0; Index < MAX_LCP_OPT_TYPES; Index++)
        {
            if (((pGSEM->
                  pNegFlagsPerIf[Index].FlagMask & FORCED_TO_REQUEST_MASK) ==
                 FORCED_TO_REQUEST_MASK) && (RecvdOptFlags[Index] == NOT_SET))
            {
                PPP_TRC1 (ALL_FAILURE_TRC,
                          "Option which MUST have been sent by client in its conf-req was not sent. Option Index is: %d\n",
                          Index);
                PPP_DBG1 (FUNCTION_EXIT, FUNCTION);
                OsixGiveSem (0, (const UINT1 *) "PSEM");
                return (NOT_OK);
            }
        }
    }
    PPP_DBG1 (FUNCTION_EXIT, FUNCTION);
    OsixGiveSem (0, (const UINT1 *) "PSEM");
    return (OK);
}

/*********************************************************************
*    Function Name    :    PPPIsAuthMatchFound
*    Description        :
*                This function is called at LNS to verify whether the server
*    authentication protocal negotiated by LAC is valid, and also whether it matches
*    with the Proxy Authentication AVPs received by LAC.
*    Parameter(s)    :
*        pSessionIf    -    Points to the L2TP session interface structure over
*                        which the Proxy AVPs are received.
*        Protocol    -    The serverauthentication protocal negotiated by LAC.
*    Return Values    :    OK if the negotiated protocol is valid,
*                        NOT_OK otherwise.
*********************************************************************/
INT1
PPPIsAuthProtocolFound (UINT4 IfIndex, UINT2 Protocol)
{
    tPPPIf             *pIf;
    INT1                AuthProtMatch;
    INT4                Index;

    PPP_DBG1 (FUNCTION_ENTRY, FUNCTION);
    pIf = PppGetIfPtr (IfIndex);

    AuthProtMatch = NOT_OK;
    if ((pIf->LcpGSEM.pNegFlagsPerIf[AUTH_IDX].FlagMask & DESIRED_MASK) ==
        DESIRED_MASK)
    {
        for (Index = 0; Index < MAX_AUTH_PROT; Index++)
        {
            if ((Protocol ==
                 pIf->pAuthPtr->SecurityInfo.PrefList[Index].Protocol)
                || (Protocol == IndexZeroSecurityInfo.PrefList[Index].Protocol))
            {
                AuthProtMatch = OK;
                break;
            }
        }
    }
    PPP_DBG1 (FUNCTION_EXIT, FUNCTION);
    OsixGiveSem (0, (const UINT1 *) "PSEM");
    return (AuthProtMatch);
}

/*********************************************************************
*    Function Name    :    PPPGetDefaultVal
*    Description        :
*                This function is called at LNS to get default value for
*    an LCP option.
*    Parameter(s)    :
*            pLCPOpt -    Points to the LCP option structure.
*        OptIndex    -    The LCP option index.
*    Return Values    :    <NONE>
*********************************************************************/
VOID
PPPGetDefaultVal (tLCPOptions * pLCPOpt, UINT1 OptIndex)
{
    PPP_DBG1 (FUNCTION_ENTRY, FUNCTION);
    switch (OptIndex)
    {
        case MRU_IDX:
            pLCPOpt->MRU = DEF_MRU_SIZE;
            break;
        case ACCM_IDX:
            pLCPOpt->AsyncMap[0] = 0xff;
            pLCPOpt->AsyncMap[1] = 0xff;
            pLCPOpt->AsyncMap[2] = 0xff;
            pLCPOpt->AsyncMap[3] = 0xff;
            break;
        case MAGIC_IDX:
            pLCPOpt->MagicNumber = 0;
            break;
        case FCS_IDX:
            pLCPOpt->FCSSize = FCS_16_REQ;
            break;
        case PADDING_IDX:
            pLCPOpt->MaxPadVal = DEF_PAD_VALUE;
            break;
        case CALL_BACK_IDX:
            MEMSET (&(pLCPOpt->LCPCallBackInfo), 0, sizeof (tLCPCallBackInfo));
            break;
#ifdef BAP
        case LINK_DISCR_IDX:
            pLCPOpt->LinkDiscriminator = 0;
            break;
#endif
        default:
            break;
    }
    PPP_DBG1 (FUNCTION_EXIT, FUNCTION);
    return;
}

VOID
PPPSetAsyncVars (UINT4 IfIndex, UINT1 *pTxACCM, UINT1 *pRxACCM)
{
    tPPPIf             *pIf;

    pIf = PppGetIfPtr (IfIndex);
    /* AKR - For some reason, TX_ACCM_SIZE has been defined as 32??? */
    MEMCPY (pIf->LcpOptionsAckedByLocal.AsyncMap, pTxACCM, RX_ACCM_SIZE);
    MEMCPY (pIf->LcpOptionsAllowedForPeer.AsyncMap, pRxACCM, RX_ACCM_SIZE);
    PPPIndicateAsyncParamsToLL (pIf, 0);

    return;
}

/*********************************************************************
*    Function Name    :    LACFrmDataInd
*    Description        :
*        This procedure is called by L2TP to remove CRC, transparency bytes and
*    lower layer framing from a received PPP packet.
*
*    Parameter(s)    :
*        pSessionIf    -    Points to the L2TP interface structure corresponding
*                        to the PPP interface over which the packet was
*                        received.
*            pInPkt    -    Points to the incoming PPP packet.
*        *pLength    -    Holds the length of the incoming packet.
*    Return Values    :    NOT_OK on error, OK otherwise.
*********************************************************************/
INT1
PPPRemoveFraming (UINT4 IfIndex, t_MSG_DESC * pInPkt, UINT2 *pLength)
{
    UINT2               IfType;
    INT1                ResultCode;
    tPPPIf             *pPPPIf;

    PPP_DBG1 (FUNCTION_ENTRY, FUNCTION);

    pPPPIf = PppGetIfPtr (IfIndex);
    IfType = pPPPIf->LinkInfo.IfID.IfType;
    ResultCode =
        FrmLLRemoveFramingReq (IfType, &pPPPIf->AsyncParams, NOT_SET, pInPkt,
                               pLength);

    if ((ResultCode == INVALID_FCS_VALUE)
        || (ResultCode == FRAM_INVALID_IF_TYPE))
    {
        PPP_TRC (ALL_FAILURE_TRC,
                 "Discarding on bad FCS value or Wrong if type. Exiting LACFrmDataInd.\n");
        return (NOT_OK);
    }
    if ((ResultCode == BAD_ADDR_FIELD) || (ResultCode == BAD_CTRL_FIELD))
    {

        (ResultCode ==
         BAD_ADDR_FIELD) ? pPPPIf->LLHCounters.BadAddressCounter++ : pPPPIf->
        LLHCounters.BadControlCounter++;
        PPP_TRC (ALL_FAILURE_TRC,
                 "DeEncap Failure!!! Exiting LACFrmDataInd.\n");
        PPP_DBG1 (FUNCTION_EXIT, FUNCTION);
        return (NOT_OK);
    }
    if (ResultCode == AC_NOT_COMPRESSED)
    {
        MOVE_OFFSET (pInPkt, HDLC_ADDR_LEN);
        *pLength -= HDLC_ADDR_LEN;
    }
    PPP_DBG1 (FUNCTION_EXIT, FUNCTION);
    return (OK);
}

/*********************************************************************
*    Function Name    :    LACFrmDataReq
*    Description        :
*        This procedure is invoked by L2TP to insert CRC and transparency
*    bytes and appropriately frame a PPP packet before sending it to the
*    LL for transmission to the remote user.
*
*    Parameter(s)    :
*        pSessionIf    -    Points to the L2TP session interface structure
*                        corresponding to the LL interface over which the
*                        packet is being sent.
*        ppOutPkt    -    Points to the address of the outgoing PPP packet.
*            pLength -    Points to the length of the outgoing packet.
*    Return Values    :    NOT_OK on framing error, OK otherwise.
*********************************************************************/
INT1
PPPAddFraming (UINT4 IfIndex, t_MSG_DESC ** ppOutPkt, UINT2 *pLength)
{
    UINT2               IfType;
    tPPPIf             *pPPPIf;

    pPPPIf = PppGetIfPtr (IfIndex);
    IfType = pPPPIf->LinkInfo.IfID.IfType;

    if (FrmAddFramingReq
        (IfType, &pPPPIf->AsyncParams, NOT_SET, ppOutPkt, pLength) != OK)
    {
        return (NOT_OK);
    }
    return (OK);
}

INT1
PPPProcessProxyAuth (UINT4 IfIndex, UINT2 AuthProtocol, UINT2 AuthID,
                     UINT1 *pAuthName, UINT2 AuthLength, UINT1 *pAuthChallenge,
                     UINT2 ChalLength, UINT1 *pAuthResp, UINT2 RespLen)
{

    UINT1               au1Password[MAX_SECRET_SIZE];
    tPPPIf             *pPPPIf;
    tSecret            *pServerSecret;
    tMD5_CTX            MDContext;

    BZERO (au1Password, MAX_SECRET_SIZE);

    pPPPIf = PppGetIfPtr (IfIndex);

    if ((pPPPIf->LcpGSEM.pNegFlagsPerIf[AUTH_IDX].FlagMask & DESIRED_MASK) !=
        DESIRED_MASK)
    {
        PPP_TRC (ALL_FAILURE_TRC, "No server authentication configured.\n");
        PPP_DBG1 (FUNCTION_EXIT, FUNCTION);
        OsixGiveSem (0, (const UINT1 *) "PSEM");
        return (OK);
    }

    if (L2TP_CHAP_AUTH == AuthProtocol)
        AuthProtocol = CHAP_PROTOCOL;
    else if (L2TP_PAP_AUTH == AuthProtocol)
        AuthProtocol = PAP_PROTOCOL;
    else
    {
        PPP_TRC (ALL_FAILURE_TRC, "Authentication protocol not supported.\n");
    }

    if ((pServerSecret =
         AUTHGetSecret (pPPPIf->pAuthPtr, pAuthName,
                        AuthLength, AuthProtocol, SERVER)) == NULL)
    {
        PPP_TRC (ALL_FAILURE_TRC,
                 "Unable to send reply due to all manadatory AVPs are \
          not received or invalid response length or insufficient secret info.\n");
        PPP_DBG1 (FUNCTION_EXIT, FUNCTION);
        OsixGiveSem (0, (const UINT1 *) "PSEM");
        return (NOT_OK);
    }

    if (AuthProtocol == CHAP_PROTOCOL)
    {
        STRCPY (au1Password, pServerSecret->Secret);
        FsUtlDecryptPasswd ((CHR1 *) au1Password);

        PppMD5Init (&MDContext);
        PppMD5Update (&MDContext, (UINT1 *) &(AuthID), 1);
        PppMD5Update (&MDContext, au1Password, pServerSecret->SecretLen);
        PppMD5Update (&MDContext, pAuthChallenge, ChalLength);
        PppMD5Final (&MDContext);

        /* Compare local and remote MDs and send the appropriate status
         */

        if (MEMCMP (MDContext.Digest, pAuthResp, MD5_SIGNATURE_SIZE) != 0)
        {
            PPP_TRC (ALL_FAILURE_TRC,
                     "Secrets do no match, User autherization failure.\n");
            PPP_DBG1 (FUNCTION_EXIT, FUNCTION);
            return (INT_AUTH_FAILED);
        }
        else
        {
            PPP_TRC (ALL_FAILURE_TRC, "Secret Match Auth Success\n");
            return (INT_AUTH_SUCC);
        }
    }
    else if (AuthProtocol == PAP_PROTOCOL)
    {

        if (MEMCMP (pServerSecret->Secret, pAuthResp,
                    pServerSecret->SecretLen) != 0)
        {
            PPP_TRC (ALL_FAILURE_TRC,
                     "Secrets do no match, User autherization failure.\n");
            PPP_DBG1 (FUNCTION_EXIT, FUNCTION);
            OsixGiveSem (0, (const UINT1 *) "PSEM");
            return (INT_AUTH_FAILED);
        }
        else
        {

            PPP_TRC (ALL_FAILURE_TRC,
                     "Secrets match, User autherization Success.\n");
            return (INT_AUTH_SUCC);
        }
    }
    else
    {
        PPP_TRC (ALL_FAILURE_TRC, "Auth Protocol not supported \n");
        return (INT_AUTH_FAILED);
    }
}

INT1
PPPGenerateLinkUpEvt (UINT4 u4IfIndex, UINT1 u1AdminStatus, UINT1 u1OperStatus)
{
    OsixTakeSem (0, (const UINT1 *) "PSEM", OSIX_WAIT, 0);
    PppUpdateInterfaceStatus (u4IfIndex, u1AdminStatus, u1OperStatus);
    OsixGiveSem (0, (const UINT1 *) "PSEM");
}

UINT4
PPPGetOrCreatePPPIf (UINT4 u4IfNum, UINT4 u4Mode)
{
    tPPPIf             *pIf;
    UINT4               u4IfIndex;
    INT4                i4Status;

    tSNMP_OCTET_STRING_TYPE Identity;
    tSNMP_OCTET_STRING_TYPE tAlias;
    tSNMP_OCTET_STRING_TYPE Secret;
    tSNMP_OCTET_STRING_TYPE HostName;
    UINT1               au1Identity[L2TP_MAX_SECRET_SIZE + 1];
    UINT1               au1Secret[L2TP_MAX_SECRET_SIZE + 1];
    UINT1               au1HostName[L2TP_MAX_SECRET_SIZE];
    tTMO_SLL_NODE      *pNode = NULL;
    UINT4               u4DeviceIfIndex = 0;
    UINT4               u4DeviceIpAddr = 0;
    UINT4               u4PriDnsIpAddr = 0;
    UINT4               u4SecDnsIpAddr = 0;

    if ((pIf = PppGetPppIfPtr (u4IfNum)) != NULL)
    {
        return (pIf->LinkInfo.IfIndex);
    }

    /* Check that any Free PPP session index exists */
    tAlias.i4_Length = (STRLEN (CFA_L2TP_TUN_NAME_PREFIX) + 2);
    tAlias.pu1_OctetList = MEM_MALLOC (tAlias.i4_Length, UINT1);
    if (L2TPPPPGetFreePppIfName (tAlias.pu1_OctetList) == NOT_OK)
    {
        PPP_TRC (ALL_FAILURE_TRC, "No Free  L2TP Session Interface\n");
        MEM_FREE (tAlias.pu1_OctetList);
        return 0;
    }
    /* Add code below for creating PPP Interface dynamically */
    if (CfaGetFreeInterfaceIndex (&u4IfIndex, CFA_PPP) != OSIX_SUCCESS)
    {
        PPP_TRC (ALL_FAILURE_TRC, "Cannot Get Free Interface Index for PPP\n");
        MEM_FREE (tAlias.pu1_OctetList);
        return 0;
    }

    if (nmhSetIfMainRowStatus (u4IfIndex, CREATE_AND_WAIT) == 0)
    {
        PPP_TRC (ALL_FAILURE_TRC, "Cannot create PPP interface!\n");
        MEM_FREE (tAlias.pu1_OctetList);
        return 0;
    }

    if (nmhSetIfMainType (u4IfIndex, CFA_PPP) == 0)
    {
        PPP_TRC (ALL_FAILURE_TRC, "Cannot Set Interface Type as PPP\n");
        nmhSetIfMainRowStatus (u4IfIndex, DESTROY);
        MEM_FREE (tAlias.pu1_OctetList);
        return 0;
    }
    if (nmhSetIfAlias (u4IfIndex, &tAlias) == 0)
    {
        PPP_TRC (ALL_FAILURE_TRC, "Cannot Set L2TP Session Alias\n");
        nmhSetIfMainRowStatus (u4IfIndex, DESTROY);
        MEM_FREE (tAlias.pu1_OctetList);
        return 0;
    }

    if (nmhSetIfMainWanType (u4IfIndex, CFA_WAN_TYPE_PRIVATE) == 0)
    {
        PPP_TRC (ALL_FAILURE_TRC, "Cannot Set Wan Type as PRIVATE\n");
        nmhSetIfMainRowStatus (u4IfIndex, DESTROY);
        MEM_FREE (tAlias.pu1_OctetList);
        return 0;
    }
    if (u4Mode == L2TP_LNS_IF)
    {
        if (nmhSetIfIpAddrAllocMethod (u4IfIndex, CFA_IP_ALLOC_MAN) == 0)
        {
            PPP_TRC (ALL_FAILURE_TRC,
                     "Cannot set PPP Interface IP Address Type\n");
            nmhSetIfMainRowStatus (u4IfIndex, DESTROY);
            MEM_FREE (tAlias.pu1_OctetList);
            return 0;
        }

        if (nmhSetIfIpAddr (u4IfIndex, gu4L2tpPppIpAddr) == 0)
        {
            PPP_TRC (ALL_FAILURE_TRC, "Cannot set PPP Interface Ip Address\n");
            nmhSetIfMainRowStatus (u4IfIndex, DESTROY);
            MEM_FREE (tAlias.pu1_OctetList);
            return 0;
        }
    }
    else if (u4Mode == L2TP_LAC_IF)
    {
        if (nmhSetIfIpAddrAllocMethod (u4IfIndex, CFA_IP_ALLOC_NEGO) == 0)
        {
            PPP_TRC (ALL_FAILURE_TRC,
                     "Cannot set PPP Interface IP Address Type\n");
            nmhSetIfMainRowStatus (u4IfIndex, DESTROY);
            MEM_FREE (tAlias.pu1_OctetList);
            return 0;
        }
    }
    if (nmhSetIfMainRowStatus (u4IfIndex, ACTIVE) == 0)
    {
        PPP_TRC (ALL_FAILURE_TRC,
                 "Cannot Bring PPP Interface Row Status Active\n");
        nmhSetIfMainRowStatus (u4IfIndex, DESTROY);
        MEM_FREE (tAlias.pu1_OctetList);
        return 0;
    }
    if (nmhSetPppExtLinkConfigLowerIfType (u4IfIndex, ASYNC_IF_TYPE) == 0)
    {
        PPP_TRC (ALL_FAILURE_TRC, "Cannot Set Encaptulation Type as Async\n");
        nmhSetIfMainRowStatus (u4IfIndex, DESTROY);
        MEM_FREE (tAlias.pu1_OctetList);
        return 0;
    }
    if (u4Mode == L2TP_LNS_IF)
    {
        pIf = PppGetPppIfPtr (u4IfIndex);
        /* This PPP interface is created for L2TP */
        pIf->u1IsCreatedForL2tp = PPP_TRUE;
        if (IndexZeroSecurityInfo.NumActiveProt > 0)
        {
            pIf->LcpGSEM.pNegFlagsPerIf[AUTH_IDX].FlagMask |= DESIRED_SET;
        }
        if (nmhSetIfAdminStatus (u4IfIndex, CFA_IF_UP) == 0)
        {
            PPP_TRC (ALL_FAILURE_TRC,
                     "Cannot Make PPP interface Admin Status UP\n");
            nmhSetIfMainRowStatus (u4IfIndex, DESTROY);
            MEM_FREE (tAlias.pu1_OctetList);
            return 0;
        }
    }
    MEM_FREE (tAlias.pu1_OctetList);
    return (u4IfIndex);
}

INT1
PPPIndicateAuthInfoToLAC (UINT4 u4IfIndex,
                          UINT2 u2AuthType,
                          UINT2 Id,
                          UINT1 PeerId[PPP_MAX_NAME_LENGTH],
                          UINT2 IdLen,
                          UINT1 Passwd[MAX_SECRET_LENGTH],
                          UINT2 PasswdLen,
                          UINT1 Challenge[MAX_CHALLENGE_LENGTH], UINT2 ChalLen)
{
    return (LACHandlePPPAuthOverEvt (u4IfIndex,
                                     u2AuthType,
                                     Id,
                                     PeerId, IdLen,
                                     Passwd, PasswdLen, Challenge, ChalLen));
}

VOID
L2TTPPPPGetSecretIndexFromName (UINT1 *pu1PPPUsername, UINT4 u4Length,
                                UINT4 u4Protocol, UINT4 u4Direction,
                                UINT4 *pu4Retval)
{
    tSecret            *pSecret = NULL;
    pSecret = AUTHGetSecret (NULL, pu1PPPUsername, u4Length, u4Protocol,
                             u4Direction);
    if (pSecret != NULL)
    {
        *pu4Retval = pSecret->IdIndex;
    }
    else
    {
        *pu4Retval = 0;
    }
}

VOID
L2TPPPPDeleteSecretEntry (UINT1 *pu1PPPUsername)
{
    tSecret            *pSecret = NULL;

    /* Delete For PAP */
    pSecret = AUTHGetSecret (NULL, pu1PPPUsername, STRLEN (pu1PPPUsername),
                             PAP_PROTOCOL, PPP_REMOTE_TO_LOCAL);
    if (pSecret != NULL)
    {
        AUTHDeleteSecretEntry (&IndexZeroSecurityInfo, pSecret);
    }
    /* Delete For Chap */
    pSecret = AUTHGetSecret (NULL, pu1PPPUsername, STRLEN (pu1PPPUsername),
                             CHAP_PROTOCOL, PPP_REMOTE_TO_LOCAL);
    if (pSecret != NULL)
    {
        AUTHDeleteSecretEntry (&IndexZeroSecurityInfo, pSecret);
    }
}
INT4
L2TPPPPGetFreePppIfName (UINT1 *pu1Name)
{
    UINT4               u4IfIndex = 0;
    UINT4               u4PPPIntfCount = 0;

    for (u4PPPIntfCount = 1; u4PPPIntfCount <= L2TP_MAX_INTERFACES;
         u4PPPIntfCount++)
    {
        SPRINTF (pu1Name, "%s%d", CFA_L2TP_TUN_NAME_PREFIX, u4PPPIntfCount);
        if (CfaGetInterfaceIndexFromName (pu1Name, &u4IfIndex) != OSIX_SUCCESS)
        {
            /* Interface does not exist */
            return (OK);
        }
    }
    /* No Free interface exists */
    return (NOT_OK);
}
#endif
