/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppptest.c,v 1.2 2011/10/13 10:31:40 siva Exp $
 *
 * Description: Contains some of the test functions.
 *
 *******************************************************************/
#include     "pppcom.h"
#include     "genexts.h"
#include     "gsemdefs.h"

#include     "pppchap.h"
#include     "ppppap.h"
#include    "pppeap.h"

#include     "pppauth.h"

#include    "lcpdefs.h"
#include    "lcpexts.h"

#include    "pppipcp.h"
#include    "ipexts.h"
#include    "pppipv6cp.h"
#include    "ipv6exts.h"
#include    "pppmuxcp.h"
#include    "pppmplscp.h"
#include    "mplsexts.h"

#include    "pppipxcp.h"
#include    "ipxexts.h"

#include    "pppbcp.h"
#include    "bcpexts.h"

#include    "mpdefs.h"
#include     "pppmp.h"
#include     "mpexts.h"

#ifdef BAP
#include     "pppbacp.h"
#include     "bacpexts.h"
#include     "pppbap.h"
#endif

#include "pppproto.h"
#include "globexts.h"

/* Removed unused parameter tIPCPIf */
tIPCPCtrlBlk       *
GetIPCPCtrlInfo (VOID)
{
    return (&IPCPCtrlBlk);
}

#ifdef IPXCP_WANTED
/* Removed unused parameter tIPXCPIf */
tIPXCPCtrlBlk      *
GetIPXCPCtrlInfo (VOID)
{
    return (&IPXCPCtrlBlk);
}
#endif

INT1
GetIfType (UINT2 PortType)
{
    if (PortType == 1)
        return (ISDN_IF_TYPE);
    if (PortType == 2)
        return (SERIAL_IF_TYPE);
    if (PortType == 3)
        return (FR_IF_TYPE);
    if (PortType == 4)
        return (X25_IF_TYPE);
    return (SERIAL_IF_TYPE);
}
