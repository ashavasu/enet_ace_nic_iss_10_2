/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppstubs.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 *********************************************************************/

#include "pppstubs.h"

INT1
CheckAndGetECPRxPtr (tPPPIf * pIf, UINT4 *n1, UINT4 *n2, UINT4 SecondIndex)
{
    UNUSED_PARAM (pIf);
    UNUSED_PARAM (n1);
    UNUSED_PARAM (n2);
    UNUSED_PARAM (SecondIndex);
    return (PPP_SNMP_OK);
}

INT1
CheckAndGetECPTxPtr (tPPPIf * pIf, UINT4 *n1, UINT4 *n2, UINT4 SecondIndex)
{
    UNUSED_PARAM (pIf);
    UNUSED_PARAM (n1);
    UNUSED_PARAM (n2);
    UNUSED_PARAM (SecondIndex);
    return (PPP_SNMP_OK);
}

INT1
CheckAndGetRMInfo (tPPPIf * pIf)
{
    UNUSED_PARAM (pIf);
    return (PPP_SNMP_OK);
}

INT1
CheckAndGetBACPPtr (tPPPIf * pIf)
{
    UNUSED_PARAM (pIf);
    return (PPP_SNMP_OK);
}

INT1
CheckAndGetECPPtr (tPPPIf * pIf)
{
    UNUSED_PARAM (pIf);
    return (PPP_SNMP_OK);
}

INT1
CheckAndGetIpxPtr (tPPPIf * pIf)
{
    UNUSED_PARAM (pIf);
    return (PPP_SNMP_OK);
}

INT1
CheckAndGetMplsPtr (tPPPIf * pIf)
{
    UNUSED_PARAM (pIf);
    return (PPP_SNMP_OK);
}

INT1
CheckAndGetMuxCPPtr (tPPPIf * pIf)
{
    UNUSED_PARAM (pIf);
    return (PPP_SNMP_OK);
}

INT4
CvHandleIncomingPkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                     UINT2 u2IfIndex, UINT4 u4PktSize, UINT2 u2Protocol,
                     UINT1 u1EncapType)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u2IfIndex);
    UNUSED_PARAM (u4PktSize);
    UNUSED_PARAM (u2Protocol);
    UNUSED_PARAM (u1EncapType);
    return (SUCCESS);
}

VOID
BAPInput (tPPPIf * pBundleIf, t_MSG_DESC * pInPkt, UINT2 Length)
{
    UNUSED_PARAM (pBundleIf);
    UNUSED_PARAM (pInPkt);
    UNUSED_PARAM (Length);
    return;
}

VOID
RMInit (VOID)
{
    return;
}

void
MuxCPDeleteIf (tMuxCPIf * pMuxcpPtr)
{
    UNUSED_PARAM (pMuxcpPtr);
    return;
}

void
MPLSCPDeleteIf (tMPLSCPIf * pMplscpPtr)
{
    UNUSED_PARAM (pMplscpPtr);
    return;
}

void
IPXCPDeleteIf (tIPXCPIf * pIpxcpPtr)
{
    UNUSED_PARAM (pIpxcpPtr);
    return;
}

void
ECPDeleteIf (tECPIf * pECP)
{
    UNUSED_PARAM (pECP);
    return;
}

tECPIf             *
SNMPGetECPIfPtr (UINT4 Index)
{
    UNUSED_PARAM (Index);
    return (NULL);
}

void
BAPTimeOut (VOID *pPenRequest)
{
    UNUSED_PARAM (pPenRequest);
    return;
}

INT1
ECPRecdResetReq (tGSEM * pGSEM, t_MSG_DESC * pIn, UINT2 Length, UINT1 Id)
{
    UNUSED_PARAM (pGSEM);
    UNUSED_PARAM (pIn);
    UNUSED_PARAM (Length);
    UNUSED_PARAM (Id);
    return DISCARD_OPT;
}

tGSEM              *
MuxCPGetSEMPtr (tPPPIf * pIf)
{
    UNUSED_PARAM (pIf);
    return NULL;
}

INT1
MuxCPCopyOptions (tGSEM * pGSEM, UINT1 Flag, UINT1 PktType)
{
    UNUSED_PARAM (pGSEM);
    UNUSED_PARAM (Flag);
    UNUSED_PARAM (PktType);
    return (DISCARD);
}

tGSEM              *
IPXCPGetSEMPtr (tPPPIf * pIf)
{
    UNUSED_PARAM (pIf);
    return NULL;
}

INT1
IPXCPCopyOptions (tGSEM * pGSEM, UINT1 Flag, UINT1 PktType)
{
    UNUSED_PARAM (pGSEM);
    UNUSED_PARAM (Flag);
    UNUSED_PARAM (PktType);
    return (OK);
}

tGSEM              *
BACPGetSEMPtr (tPPPIf * pIf)
{
    UNUSED_PARAM (pIf);
    return NULL;
}

INT1
BACPCopyOptions (tGSEM * pGSEM, UINT1 Flag, UINT1 PktType)
{
    UNUSED_PARAM (pGSEM);
    UNUSED_PARAM (Flag);
    UNUSED_PARAM (PktType);
    return (OK);
}

tGSEM              *
ECPGetSEMPtr (tPPPIf * pIf)
{
    UNUSED_PARAM (pIf);
    return NULL;
}

INT1
ECPCopyOptions (tGSEM * pGSEM, UINT1 Flag, UINT1 PktType)
{
    UNUSED_PARAM (pGSEM);
    UNUSED_PARAM (Flag);
    UNUSED_PARAM (PktType);
    return (OK);
}

tGSEM              *
MPLSCPGetSEMPtr (tPPPIf * pIf)
{
    UNUSED_PARAM (pIf);
    return NULL;
}

INT1
MPLSCPCopyOptions (tGSEM * pGSEM, UINT1 Flag, UINT1 PktType)
{
    UNUSED_PARAM (pGSEM);
    UNUSED_PARAM (Flag);
    UNUSED_PARAM (PktType);
    return (OK);
}

void
IPXCPEnableIf (tIPXCPIf * pIpxcpPtr, tIPXCPCtrlBlk * pIpxcpCtrlBlk)
{
    UNUSED_PARAM (pIpxcpPtr);
    UNUSED_PARAM (pIpxcpCtrlBlk);
    return;
}

void
MuxCPEnableIf (tMuxCPIf * pMuxcpPtr)
{
    UNUSED_PARAM (pMuxcpPtr);
    return;
}

void
MPLSCPEnableIf (tMPLSCPIf * pMplscpPtr)
{
    UNUSED_PARAM (pMplscpPtr);
    return;
}

void
ECPEnableIf (tECPIf * pECP)
{
    UNUSED_PARAM (pECP);
    return;
}

void
BACPEnableIf (tBACPIf * pBACPIf)
{
    UNUSED_PARAM (pBACPIf);
    return;
}

void
IPXCPDisableIf (tIPXCPIf * pIpxcpPtr)
{
    UNUSED_PARAM (pIpxcpPtr);
    return;
}

void
MuxCPDisableIf (tMuxCPIf * pMuxcpPtr)
{
    UNUSED_PARAM (pMuxcpPtr);
    return;
}

void
MPLSCPDisableIf (tMPLSCPIf * pMplscpPtr)
{
    UNUSED_PARAM (pMplscpPtr);
    return;
}

void
ECPDisableIf (tECPIf * pECP)
{
    UNUSED_PARAM (pECP);
    return;
}

INT1
ECPRecdResetAck (tGSEM * pGSEM, t_MSG_DESC * pIn, UINT2 Length, UINT1 Id)
{
    UNUSED_PARAM (pGSEM);
    UNUSED_PARAM (pIn);
    UNUSED_PARAM (Length);
    UNUSED_PARAM (Id);
    return (DISCARD_OPT);
}
