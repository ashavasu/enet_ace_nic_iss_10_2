/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppptimer.c,v 1.4 2014/11/23 09:35:23 siva Exp $
 *
 * Description:This file contains timer related routines.
 *
 *******************************************************************/
#include    "pppcom.h"
#include    "ppptask.h"

#include     "pppmp.h"
#ifdef BAP
#include    "pppbacp.h"
#include    "pppbap.h"
#endif

#include    "pppchap.h"
#include    "ppppap.h"
#include    "pppeap.h"
#include    "pppauth.h"
#include    "ppplqm.h"

#include    "pppproto.h"
#include    "globexts.h"

tTimerListId        PPPTimerListId;
extern VOID PPPoEPADTimeOut ARG_LIST ((VOID *));
extern VOID         GSEMTimeOut (VOID *pGSEM);
extern VOID         PAPTimeOut (VOID *pAuthPtr);
extern VOID         CHAPChalTimeOut (VOID *pAuthPtr);
extern VOID         CHAPReChalTimeOut (VOID *pAuthPtr);
extern VOID         CHAPRespTimeOut (VOID *pAuthPtr);
extern VOID         EAPIdentityTimeOut (VOID *pAuthPtr);
extern VOID         EAPMD5ChalTimeOut (VOID *pAuthPtr);
extern VOID         EAPMD5ReChalTimeOut (VOID *pAuthPtr);

/* *INDENT-OFF* */
static VOID         (*TimerRoutines[]) (VOID*) =
{
#ifdef MP
    MPNullFragTimeOut,
    MPReassemblyTimeOut,
    MPPeriodicEchoTimeOut,
#endif
    LCPProcessKeepAliveTimeOut,
#ifdef BAP
    BAPTimeOut,
#endif
    PPPoEPADTimeOut,
    CallBackDisconnectTimeOut,
    CallBackDialTimeOut,
    GSEMTimeOut,
    PAPTimeOut,
    PAPResponseTimeOut,
    CHAPChalTimeOut,
    CHAPReChalTimeOut,
    CHAPRespTimeOut,
    EAPIdentityTimeOut,
    EAPMD5ChalTimeOut,
    EAPMD5ReChalTimeOut,
    LCPEchoReqTimeOut,
    LCPIdleTimeOut
/*    LQMSendTimeOut,*/
/*    LQMExpectTimeOut*/
};
/* *INDENT-ON* */
/*********************************************************************
*  Function Name : PPPStartTimer
*  Description   :
*                        This procedure adds the 'pTimer' to the timer list.  
*  Parameter(s)  :
*      pTimer  -  pointer to the Timer block
*      TimerId -  indicates the type of the timer,whether it is RESTART or
*                         ECHO or IDLE timer
*      TimeOut -  Time out value
*  Return Values : VOID
*********************************************************************/
VOID
PPPStartTimer (tPPPTimer * pTimer, ePPPTimerId TimerId, UINT4 TimeOut)
{

    pTimer->TimerId = TimerId;

    if (TmrStartTimer
        (PPPTimerListId, &pTimer->TimerNode,
         (SYS_NUM_OF_TIME_UNITS_IN_A_SEC * TimeOut)) != TMR_SUCCESS)
    {
        PPP_TRC (OS_RESOURCE, "Start Timer Failure.");
    }
    return;
}

/*********************************************************************
*  Function Name : PPPStopTimer
*  Description   :
*            This procedure stops the timer 'pTimer' if it is ACTIVE.
*  Parameter(s)  :
*      pTimer  -  pointer to the Timer block
*  Return Values : VOID
*********************************************************************/
VOID
PPPStopTimer (tPPPTimer * pTimer)
{

    if (pTimer->TimerId != TIMER_INACTIVE)
    {

        if (TmrStopTimer (PPPTimerListId, &pTimer->TimerNode) != TMR_SUCCESS)
        {
            PPP_TRC (OS_RESOURCE, "Stop Timer Failure.");
        }
        PPP_INIT_TIMER ((*pTimer));
    }
    return;
}

/******************************************************************************
 *  Name         : PPPRestartTimer
 *  Description  : This functions stops the timer and restarts the timer with
 *                 new value
 *  Parameters   : pTimer  : Pointer to the Timer Node
 *                 TimerId : Timer Identifier
 *                 TimeOut : The new timeout value in seconds
 *  Return Values: VOID
 ******************************************************************************/
VOID
PPPRestartTimer (tPPPTimer * pTimer, ePPPTimerId TimerId, UINT4 TimeOut)
{

    if (pTimer->TimerId != TIMER_INACTIVE)
    {
        if (TmrStopTimer (PPPTimerListId, &pTimer->TimerNode) != TMR_SUCCESS)
        {
            PPP_TRC (OS_RESOURCE, "PPP: Stop Timer Failure.");
        }
    }

    pTimer->TimerId = TimerId;

    if (TmrStartTimer (PPPTimerListId, &pTimer->TimerNode,
                       (SYS_NUM_OF_TIME_UNITS_IN_A_SEC * TimeOut)) !=
        TMR_SUCCESS)
    {
        PPP_TRC (OS_RESOURCE, "Start Timer Failure.");
    }

    return;
}

/*********************************************************************
*  Function Name : PPPProcessTimeOut
*  Description   :
*                This procedure calls the appropriate procedures to handle 
*  the timer expiry condition.
*  Parameter(s)  :
*      pTimer  -  pointer to the Timer block
*  Return Values : VOID
*********************************************************************/
VOID
PPPProcessTimeOut (VOID)
{

    tTimer             *pListHead, *pTimer;
    ePPPTimerId         TimerId;

    pListHead = TmrGetNextExpiredTimer (PPPTimerListId);
    while (pListHead != NULL)
    {

        pTimer = pListHead;
        TimerId = ((tPPPTimer *) pTimer)->TimerId;
        if (TimerId >= sizeof (TimerRoutines) / sizeof (UINT4))
        {
            PPP_TRC (OS_RESOURCE, "Invalid Timer Id...");
            break;
        }
        PPP_INIT_TIMER ((*(tPPPTimer *) (pTimer)));
        (*TimerRoutines[TimerId]) ((VOID *) (FS_ULONG) ((tPPPTimer *) pTimer)->
                                   Param1);
        pListHead = TmrGetNextExpiredTimer (PPPTimerListId);
    }
    return;

}

/*********************************************************************
*  Function Name : PPPInitTimerList
*  Description   :
*     This function initializes the timer list.
*  Parameter(s)  : VOID
*  Return Values : VOID
*********************************************************************/
VOID
PPPInitTimerList (VOID)
{
    UINT1               au1TaskName[10];
    STRCPY (au1TaskName, PPP_TASK_NAME);

    if (TmrCreateTimerList
        (au1TaskName, (UINT4) PPP_TIMER_EXPIRY_EVENT, NULL,
         &PPPTimerListId) != TMR_SUCCESS)
    {
        PPP_TRC (OS_RESOURCE, "TimerList creation Failure.");
    }
    return;
}

/*********************************************************************
*  Function Name : PPPDeInitTimerList
*  Description   :
*     This function de-initializes the timer list.
*  Parameter(s)  : VOID
*  Return Values : VOID
*********************************************************************/
VOID
PPPDeInitTimerList (VOID)
{

    if (TmrDeleteTimerList (PPPTimerListId) != TMR_SUCCESS)
    {
        PPP_TRC (OS_RESOURCE, "TimerList deletion Failure.");
    }
    return;
}
