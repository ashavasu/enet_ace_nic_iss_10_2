/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: snmpdbg.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the SNMP MIDDLE level routines of
 *             pppDebugLevel group.
 *
 *******************************************************************/

#include "genhdrs.h"
#include "snmphdrs.h"
#include "pppsnmpm.h"
#include "pppdbgex.h"

#define A_PPPDEBUGLEVELID    2
#define    A_PPPDEBUGMODULEID    1

/* Local Prototypes */
VarBind            *SNMPDebugLevelGet (tSNMP_OID_TYPE * InDataBasePtr,
                                       tSNMP_OID_TYPE * InNamePtr, UINT1Arg,
                                       UINT1 SearchType);

INT4                SNMPDebugLevelTest (tSNMP_OID_TYPE * InDataBasePtr,
                                        tSNMP_OID_TYPE * InNamePtr, UINT1 Arg,
                                        tSNMP_MULTI_DATA_TYPE * Value);

SNMPDebugLevelSet (tSNMP_OID_TYPE * InDataBasePtr, tSNMP_OID_TYPE * InNamePtr,
                   UINT1 Arg, tSNMP_MULTI_DATA_TYPE * value);

/*********************************************************************
*  Function Name : SNMPDebugLevelGet
*  Description   :
*  Parameter(s)  :
*   InDataBasePtr -
*   InNamePtr     -
*   Arg           -
*   SearchType    -
*  Return Value  :
*
***********************************************************************/

VarBind            *
SNMPDebugLevelGet (InDataBasePtr, InNamePtr, Arg, SearchType)
     tSNMP_OID_TYPE     *InDataBasePtr;
     tSNMP_OID_TYPE     *InNamePtr;
     UINT1               Arg;
     UINT1               SearchType;
{
    tSNMP_OCTET_STRING_TYPE *os_ptr;
    INT4                sl_value;
    UINT1               str[8];
    if (InNamePtr->u4_Length > (InDataBasePtr->u4_Length + 1))
        return (NULL);

    if ((SearchType == NEXT) && (CmpOIDClass (InNamePtr, InDataBasePtr) == 0)
        && (InNamePtr->u4_Length > InDataBasePtr->u4_Length))
        return (NULL);

    if ((SearchType == NEXT)
        || ((InNamePtr->u4_Length == InDataBasePtr->u4_Length + 1)
            && (InNamePtr->pu4_OidList[InNamePtr->u4_Length - 1] == 0)))
    {

        InDataBasePtr->pu4_OidList[InDataBasePtr->u4_Length] = 0;
        InDataBasePtr->u4_Length++;
        switch (Arg)
        {
            case A_PPPDEBUGLEVELID:
                sl_value = DesiredLevel;
                break;
            case A_PPPDEBUGMODULEID:
                sl_value = DesiredModule;
                break;
            default:
                return (NULL);

        }

        return (FormVarBind
                (InDataBasePtr, INTEGER_TAG, 0L, sl_value, os_ptr, NULL));

    }
    else
    {
        return (NULL);
    }

}

/*************************************************************************/
INT4
SNMPDebugLevelTest (InDataBasePtr, InNamePtr, Arg, Value)
     tSNMP_OID_TYPE     *InDataBasePtr;
     tSNMP_OID_TYPE     *InNamePtr;
     UINT1               Arg;
     tSNMP_MULTI_DATA_TYPE *Value;
{
    if (InNamePtr->u4_Length != (InDataBasePtr->u4_Length + 1))
    {
        return (NO_SUCH_NAME_ERROR);
    }
    switch (Arg)
    {

        case A_PPPDEBUGLEVELID:
        case A_PPPDEBUGMODULEID:
            break;
        default:
            return (NO_SUCH_NAME_ERROR);
    }
    return (NO_ERROR);
}

/*********************************************************************
*  Function Name : SNMPDebugLevelSet
*  Description   :
*  Parameter(s)  :
*   InDataBasePtr -
*   InNamePtr     -
*   Arg           -
*   value         -
*  Return Value  :
*
*********************************************************************/
INT4
SNMPDebugLevelSet (InDataBasePtr, InNamePtr, Arg, value)
     tSNMP_OID_TYPE     *InDataBasePtr;
     tSNMP_OID_TYPE     *InNamePtr;
     UINT1               Arg;
     tSNMP_MULTI_DATA_TYPE *value;
{
    switch (Arg)
    {
        case A_PPPDEBUGLEVELID:
            DesiredLevel = value->i4_SLongValue;
            break;
        case A_PPPDEBUGMODULEID:
            DesiredModule = value->i4_SLongValue;

            break;
    }
    return (NO_ERROR);
}
