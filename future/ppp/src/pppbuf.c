/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppbuf.c,v 1.3 2014/10/27 10:54:25 siva Exp $
 *
 * Description:This file contains the interfaces to the buffer
 *             management library.
 *
 *******************************************************************/

#include "genhdrs.h"
#include "globexts.h"
#include "pppmacro.h"
/*********************************************************************
*    Function Name    :    PPPAllocateBuffer
*    Description        : 
*           This function is called by the PPP subsystem to allocate buffer
*           for control messages and incoming messages.
*    Parameter(s)    :
*        Size        -   Size of the required buffer.
*        Offset      -   Offset in the buffer from which all furthur buffer 
*        actions are performed.
*    Return Values    :    Pointer to the allocated buffer.
*********************************************************************/
t_MSG_DESC         *
PPPAllocateBuffer (UINT4 Size, UINT4 Offset)
{
    t_MSG_DESC         *pBuffer;

    if ((pBuffer = CRU_BUF_Allocate_MsgBufChain (Size, Offset)) != NULL)
    {
        CB_READ_OFFSET (pBuffer) = 0;
        CB_WRITE_OFFSET (pBuffer) = 0;
	    MEMSET (pBuffer->pFirstValidDataDesc->pu1_FirstValidByte, 0, pBuffer->pFirstValidDataDesc->u4_ValidByteCount);
        return (pBuffer);
    }

    return (pBuffer);
}

/*********************************************************************
*    Function Name    :    PPPDuplicateBuffer
*    Description        : 
*           This function is called by the PPP subsystem to duplicate the
*           contents of a buffer.
*    Parameter(s)    :
*        pSrcBuf     -   Pointer to the buffer to be duplicated.
*    Return Values    :    Pointer to the buffer copy.
*********************************************************************/
t_MSG_DESC         *
PPPDuplicateBuffer (t_MSG_DESC * pSrcBuf)
{
    t_MSG_DESC         *pRetBuf = NULL;
    if ((pRetBuf = CRU_BUF_Duplicate_BufChain (pSrcBuf)) != NULL)
    {
        CB_READ_OFFSET (pRetBuf) = 0;
        CB_WRITE_OFFSET (pRetBuf) = 0;
    }
    return (pRetBuf);
}

/*****************************************************************************
 *
 *    Function Name        : PPPDuplicateCruBuffer
 *
 *    Description        : This function makes a copy the CRU buffer by making
 *                use of the linear buffer and the FSAP calls.
 *
 *    Input(s)            : tCRU_BUF_CHAIN_HEADER *pBuf,
 *                        tCRU_BUF_CHAIN_HEADER *pDupBuf,
 *                          UINT4 u4PktSize.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : PPP_SUCCESS if frame is processed
 *                succeessfully, otherwise PPP_FAILURE.
 *
 *****************************************************************************/
INT4
PPPDuplicateCruBuffer (tCRU_BUF_CHAIN_HEADER * pBuf,
                       tCRU_BUF_CHAIN_HEADER ** pDupBuf, UINT4 u4PktSize)
{
    UINT1              *pu1DataBuf = NULL;

    /* Allocate the Buffer chain for the Duplicate buffer */
    if (((*pDupBuf) = CRU_BUF_Allocate_MsgBufChain ((u4PktSize +
                                                     PPP_MAX_FRAME_HEADER_SIZE),
                                                    PPP_MAX_FRAME_HEADER_SIZE))
        == NULL)
    {
        return PPP_FAILURE;
    }
    else
    {
        /* Allocate memory for the linear buffer */
        if ((pu1DataBuf = MEM_MALLOC ((PPP_MAX_DRIVER_MTU * 2), UINT1)) == NULL)
        {
            CRU_BUF_Release_MsgBufChain ((*pDupBuf), FALSE);
            return PPP_FAILURE;
        }
        else
        {
            /* Copy from the pBuf to the Linear buffer */
            if (CRU_BUF_Copy_FromBufChain (pBuf, pu1DataBuf, 0, u4PktSize)
                == CRU_FAILURE)
            {
                CRU_BUF_Release_MsgBufChain ((*pDupBuf), FALSE);
                MEM_FREE (pu1DataBuf);
                return PPP_FAILURE;
            }
            else
            {
                /* Copy from linear buffer to the CRU buffer */
                if (CRU_BUF_Copy_OverBufChain
                    ((*pDupBuf), pu1DataBuf, 0, u4PktSize) == CRU_FAILURE)
                {
                    CRU_BUF_Release_MsgBufChain ((*pDupBuf), FALSE);
                    MEM_FREE (pu1DataBuf);
                    return PPP_FAILURE;
                }

                MEMCPY (&(*pDupBuf)->ModuleData, &pBuf->ModuleData,
                        sizeof (tMODULE_DATA));
            }
        }
    }

    MEM_FREE (pu1DataBuf);
    return PPP_SUCCESS;
}
