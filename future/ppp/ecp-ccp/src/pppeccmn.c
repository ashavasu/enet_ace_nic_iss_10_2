/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppeccmn.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains routines common to both ECP and CCP.
 *
 *******************************************************************/

#include "genhdrs.h"
#include "pppmacro.h"
#include "pppdefs.h"
#include "pppexts.h"
#include "pppdbgex.h"
#include "pppdebug.h"
#include "ppptimer.h"
#include "frmtdfs.h"
#include "pppgsem.h"
#include "pppgcp.h"
#include "ppplcp.h"

#include "pppeccmn.h"
#include "pppccp.h"
#include "pppecp.h"

extern UINT2        CCPAllocCounter, CCPIfFreeCounter;
VOID                DeleteNodes (t_SLL * pSll);
VOID                OptionInit (t_SLL * InSll);
INT2                GetNextOptionFromList (tGSEM * pGSEM, UINT1 Module);
INT1                GCPGetOptIndex (tGSEM * pGSEM, UINT1 OptType);
/*********************************************************************
*  Function Name : DeleteNodes
*  Description   :
*             This procedure deletes all the nodes of the given SLL.
*  Parameter(s)  :
*         pSLL -  pointer to the SLL in which the nodes have to be 
*                    deleted
*  Return Values : VOID
*********************************************************************/
VOID
DeleteNodes (t_SLL * pSll)
{

    SLL_DELETE_LIST (pSll, FREE_CCP_OR_ECP_PARAMS);
}

/*********************************************************************
*  Function Name : GetNextOptionFromList
*  Description   :
*    This function scans the AckedByPeer list and returns the next
*     next high priority option of CCP.
*  ECP Options.
*  Input         : t_SLL
*  Return Value  : DISCARD/Index in the list
*********************************************************************/
INT2
GetNextOptionFromList (tGSEM * pGSEM, UINT1 Module)
{
    UINT1               Count, OptType;
    tHeader            *pHeader;
    t_SLL              *pAckedByPeer = NULL;
    INT2                Index, CurrIndex = 0;
    tCCPIf             *pCCPIf;
    tECPIf             *pECPIf;

    Count = 0;
    Index = 0;

    pCCPIf = (tCCPIf *) pGSEM->pIf->pCCPIf;
    if (Module == CCP_MOD)
    {

        pAckedByPeer = &pCCPIf->OptionsAckedByPeer;
    /*******SKR004 now CurrentIndex is part of the pCCPIf *****/
        CurrIndex = pCCPIf->CCPCurrentIndex;
    }

    pECPIf = (tECPIf *) pGSEM->pIf->pECPIf;
    if (Module == ECP_MOD)
    {

        pAckedByPeer = &pECPIf->OptionsAckedByPeer;
    /*******SKR004 now ECPCurrentIndex is part of the pECPIf *****/
        CurrIndex = pECPIf->ECPCurrentIndex;
    }

    if (pAckedByPeer == NULL)
    {
        return DISCARD_OPT;
    }

    SLL_SCAN (pAckedByPeer, pHeader, tHeader *)
    {

        if (Count > CurrIndex)
        {

            OptType = pHeader->Type;
            if ((Index = (INT2) GCPGetOptIndex (pGSEM, OptType)) == NOT_FOUND)
            {

                PPP_DBG ("  GCPGetOptIndex()...");
                return DISCARD_OPT;
            }
            PPP_DBG2 (" Reting Index .... CurrentIndex = %d, Index = %d",
                      CurrIndex, Index);
            CurrIndex++;
            if (Module == CCP_MOD)
            {
                /*******SKR004 now CCPCurrentIndex is part of the pCCPIf *****/
                pCCPIf->CCPCurrentIndex = (UINT1) CurrIndex;
            }
            else
            {

                /*******SKR004 now ECPCurrentIndex is part of the pECPIf *****/
                pECPIf->ECPCurrentIndex = (UINT1) CurrIndex;
            }
            return Index;
        }
        Count++;
    }

    if (Module == CCP_MOD)
    {
        /*******SKR004 now CCPCurrentIndex is part of the pCCPIf *****/
        pCCPIf->CCPCurrentIndex = (UINT1) CurrIndex;
    }
    else
    {
        /*******SKR004 now ECPCurrentIndex is part of the pECPIf *****/
        pECPIf->ECPCurrentIndex = (UINT1) CurrIndex;
    }
    return DISCARD_OPT;
}

/***********************************************************************
*  Function Name : OptionInit
*  Description   :
*               This function initializes the Option structure and is
*  called by the ECPInit/CCPInit function.
*  Parameter(s)  :
*       pInSll -  points to the SLL that has to be initialized.
*  Return Value  :
*********************************************************************/
VOID
OptionInit (t_SLL * InSll)
{

    SLL_INIT (InSll);

    return;
}
