/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppeccmn.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the definitions required for the ECP
 * module
 *
 *******************************************************************/
#ifndef __PPP_PPPECCMN_H__
#define __PPP_PPPECCMN_H__

#ifndef CMN
#define  CMN                               
#define  CCP_MOD         0                 
#define  ECP_MOD         1                 

#ifndef DEBUB_CHAN
#define  DEBUG_CHAN      stderr            
#endif
#define  DISCARD_OPT     -1                

#define  RESET_REQ_CODE  14                
#define  RESET_ACK_CODE  15                

#define  FREE_SLL        TMO_SLL_FreeNodes 

#define  ERROR           -1                

typedef struct Header {

        t_SLL_NODE pNext; 
        UINT4 Priority;
        UINT1 Type;
	UINT1 u1Rsvd;
        UINT2 Length; /* Length of the Algorithm structure */
}tHeader;


#define NOMEM "malloc failed"

#define DEBUG_PRINT(msg,str)    PPP_DBG2("%s in  %s",msg,str)
#define FUNC(func) 
#define DEBUG_CHECK( condition, msg, funcName, retVal)\
if((condition)) \
{\
        DEBUG_PRINT(msg,funcName);\
        return retVal;\
}


#define CHECK( condition, msg, funcname, retVal) \
if((condition)) \
{\
   DEBUG_PRINT(msg,funcname);\
   return retVal;\
}

#ifndef OK
#define OK 1
#endif
#ifndef NOT_OK
#define NOT_OK 0
#endif
#endif


#endif  /* __PPP_PPPECCMN_H__ */
