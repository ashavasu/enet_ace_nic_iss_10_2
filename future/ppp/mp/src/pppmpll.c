/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppmpll.c,v 1.5 2014/12/16 11:47:25 siva Exp $
 *
 * Description:This file contains the SNMP low level routines of
 *             pppMp group.
 *
 *******************************************************************/

#define __MP_LOW_H__
#include "mpcom.h"
#include "snmphdrs.h"
#include "pppsnmpm.h"
#ifdef BAP
#include "pppbacp.h"
#include "pppbap.h"
#include "bapexts.h"
#endif

#include "globexts.h"
#include "llproto.h"
#include "pppexlow.h"
#include "ppmpflow.h"

#define MIN_MP_CFG_BUNDLE_RDIRCT 1
#define MAX_MP_CFG_BUNDLE_RDIRCT 3
#define SHORT_MP_CFG_HDR_FORMAT 1
#define LONG_MP_CFG_HDR_FORMAT 2
#define MIN_MP_ADDRESS_SIZE     0
#define MAX_MP_ADDRESS_SIZE     15

#if (BAP || MPPLUS)
#define NO_PROTOCOL_INDEX    1
#endif
#ifdef BAP
#define BAP_PROTOCOL_INDEX    2
#endif

INT1                get_pppMpConfigMRRU (UINT4 Index, INT4 *sl_value);

/****************************************************************************/
INT1
CheckAndGetMpPtr (tPPPIf * pIf)
{
    if (pIf->BundleFlag == BUNDLE)
    {
        return (PPP_SNMP_OK);
    }
    return (PPP_SNMP_ERR);
}

tPPPIf             *
SNMPGetBundleIfptr (Index)
     UINT4               Index;
{
    tPPPIf             *pIf;

    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if ((pIf->LinkInfo.IfIndex == Index) && (pIf->BundleFlag == BUNDLE))
            return (pIf);
    }
    return (NULL);
}

tPPPIf             *
SNMPGetMemberIfptr (Index)
     UINT4               Index;
{
    tPPPIf             *pIf;

    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if ((pIf->LinkInfo.IfIndex == Index) &&
            (pIf->MPInfo.MemberInfo.pBundlePtr->BundleFlag == BUNDLE))
            return (pIf);
    }
    return (NULL);
}

/* LOW LEVEL Routines for Table : MpConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMpConfigTable
 Input       :  The Indices
                MpConfigIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMpConfigTable (INT4 i4MpConfigIfIndex)
{
    if (SNMPGetBundleIfptr ((UINT4) i4MpConfigIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMpConfigTable
 Input       :  The Indices
                MpConfigIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMpConfigTable (INT4 *pi4MpConfigIfIndex)
{
    if (nmhGetFirstIndex (pi4MpConfigIfIndex, MpInstance, FIRST_INST) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexMpConfigTable
 Input       :  The Indices
                MpConfigIfIndex
                nextMpConfigIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMpConfigTable (INT4 i4MpConfigIfIndex,
                              INT4 *pi4NextMpConfigIfIndex)
{
    if (nmhGetNextIndex (&i4MpConfigIfIndex, MpInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextMpConfigIfIndex = i4MpConfigIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMpConfigSeqNoHeaderFormat
 Input       :  The Indices
                MpConfigIfIndex

                The Object 
                retValMpConfigHeaderFormat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMpConfigSeqNoHeaderFormat (INT4 i4MpConfigIfIndex,
                                 INT4 *pi4RetValMpConfigHeaderFormat)
{
    tPPPIf             *pIf;

    if ((pIf =
         (tPPPIf *) SNMPGetBundleIfptr ((UINT4) i4MpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValMpConfigHeaderFormat =
        (pIf->MPInfo.BundleInfo.MPFlagsPerIf[SEQ_IDX].
         FlagMask & DESIRED_MASK) ? 1 : 2;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetMpConfigBundleClass
 Input       :  The Indices
                MpConfigIfIndex

                The Object 
                retValMpConfigBundleClass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMpConfigBundleClass (INT4 i4MpConfigIfIndex,
                           INT4 *pi4RetValMpConfigBundleClass)
{
    tPPPIf             *pIf;

    if ((pIf =
         (tPPPIf *) SNMPGetBundleIfptr ((UINT4) i4MpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValMpConfigBundleClass =
        pIf->MPInfo.BundleInfo.BundleOptDesired.BundleId.Class;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetMpConfigBundleAddr
 Input       :  The Indices
                MpConfigIfIndex

                The Object 
                retValMpConfigBundleAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMpConfigBundleAddr (INT4 i4MpConfigIfIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValMpConfigBundleAddr)
{
    tPPPIf             *pIf;

    if ((pIf =
         (tPPPIf *) SNMPGetBundleIfptr ((UINT4) i4MpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    /* RFC007 *Str = pIf->MPInfo.BundleInfo.BundleOptDesired.BundleId.Address;
     * */
    pRetValMpConfigBundleAddr->i4_Length =
        pIf->MPInfo.BundleInfo.BundleOptDesired.BundleId.Length;
    MEMCPY (pRetValMpConfigBundleAddr->pu1_OctetList,
            pIf->MPInfo.BundleInfo.BundleOptDesired.BundleId.Address,
            pRetValMpConfigBundleAddr->i4_Length);
    return (SNMP_SUCCESS);
}

#ifdef PPP_STACK_WANTED
/****************************************************************************
 Function    :  nmhGetMpConfigHeaderFormatCodeLocalToRemote
 Input       :  The Indices
                IfIndex

                The Object 
                retValMpConfigHeaderFormatCodeLocalToRemote
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMpConfigHeaderFormatCodeLocalToRemote (INT4 i4IfIndex,
                                             INT4
                                             *pi4RetValMpConfigHeaderFormatCodeLocalToRemote)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = SNMPGetBundleIfptr (i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValMpConfigHeaderFormatCodeLocalToRemote =
        pIf->MPInfo.BundleInfo.BundleOptDesired.u1MCMLCodeVal;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetMpConfigHeaderFormatCodeRemoteToLocal
 Input       :  The Indices
                IfIndex

                The Object 
                retValMpConfigHeaderFormatCodeRemoteToLocal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMpConfigHeaderFormatCodeRemoteToLocal (INT4 i4IfIndex,
                                             INT4
                                             *pi4RetValMpConfigHeaderFormatCodeRemoteToLocal)
{
    tPPPIf             *pIf;

    if ((pIf = SNMPGetBundleIfptr (i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValMpConfigHeaderFormatCodeRemoteToLocal =
        pIf->MPInfo.BundleInfo.BundleOptAllowedForPeer.u1MCMLCodeVal;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetMpConfigHeaderFormatSuspClassLocalToRemote
 Input       :  The Indices
                IfIndex

                The Object 
                retValMpConfigHeaderFormatSuspClassLocalToRemote
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMpConfigHeaderFormatSuspClassLocalToRemote (INT4 i4IfIndex,
                                                  INT4
                                                  *pi4RetValMpConfigHeaderFormatSuspClassLocalToRemote)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = SNMPGetBundleIfptr (i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValMpConfigHeaderFormatSuspClassLocalToRemote =
        pIf->MPInfo.BundleInfo.BundleOptDesired.u1MCMLClassVal;
    return SNMP_SUCCESS;
}
#endif /* PPP_STACK_WANTED */
/****************************************************************************
 Function    :  nmhGetMpConfigNullFragTimeOutValue
 Input       :  The Indices
                MpConfigIfIndex

                The Object 
                retValMpConfigNullFragTimeOutValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMpConfigNullFragTimeOutValue (INT4 i4MpConfigIfIndex,
                                    INT4 *pi4RetValMpConfigNullFragTimeOutValue)
{
    tPPPIf             *pIf;

    if ((pIf =
         (tPPPIf *) SNMPGetBundleIfptr ((UINT4) i4MpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValMpConfigNullFragTimeOutValue =
        (INT4) (pIf->MPInfo.BundleInfo.NullFragTimeOutVal);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetMpConfigReAssemblyTimeOutValue
 Input       :  The Indices
                MpConfigIfIndex

                The Object 
                retValMpConfigReAssemblyTimeOutValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMpConfigReAssemblyTimeOutValue (INT4 i4MpConfigIfIndex,
                                      INT4
                                      *pi4RetValMpConfigReAssemblyTimeOutValue)
{
    tPPPIf             *pIf;

    if ((pIf =
         (tPPPIf *) SNMPGetBundleIfptr ((UINT4) i4MpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValMpConfigReAssemblyTimeOutValue =
        (INT4) (pIf->MPInfo.BundleInfo.ReassemblyTimeOutVal);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetMpConfigBundleDirection
 Input       :  The Indices
                MpConfigIfIndex

                The Object 
                retValMpConfigBundleDirection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMpConfigBundleDirection (INT4 i4MpConfigIfIndex,
                               INT4 *pi4RetValMpConfigBundleDirection)
{
    tPPPIf             *pIf;

    if ((pIf =
         (tPPPIf *) SNMPGetBundleIfptr ((UINT4) i4MpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValMpConfigBundleDirection = pIf->MPInfo.BundleInfo.BundleDirection;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetMpConfigMinThruput
 Input       :  The Indices
                MpConfigIfIndex

                The Object 
                retValMpConfigMinThruput
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMpConfigMinThruput (INT4 i4MpConfigIfIndex,
                          INT4 *pi4RetValMpConfigMinThruput)
{
    tPPPIf             *pIf;

    if ((pIf =
         (tPPPIf *) SNMPGetBundleIfptr ((UINT4) i4MpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValMpConfigMinThruput = (INT4) pIf->MPInfo.BundleInfo.MinThruput;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetMpConfigMaxThruput
 Input       :  The Indices
                MpConfigIfIndex

                The Object 
                retValMpConfigMaxThruput
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMpConfigMaxThruput (INT4 i4MpConfigIfIndex,
                          INT4 *pi4RetValMpConfigMaxThruput)
{
    tPPPIf             *pIf;

    if ((pIf =
         (tPPPIf *) SNMPGetBundleIfptr ((UINT4) i4MpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValMpConfigMaxThruput = (INT4) (pIf->MPInfo.BundleInfo.MaxThruput);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetMpConfigBandwidthManagementProtocol
 Input       :  The Indices
                MpConfigIfIndex

                The Object 
                retValMpConfigBandwidthManagementProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef BAP
INT1
nmhGetMpConfigBandwidthManagementProtocol (INT4 i4MpConfigIfIndex,
                                           INT4
                                           *pi4RetValMpConfigBandwidthManagementProtocol)
{
    tPPPIf             *pIf;

    if ((pIf =
         (tPPPIf *) SNMPGetBundleIfptr ((UINT4) 4 MpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    switch (pIf->MPInfo.BundleInfo.BWMProtocolInfo.Protocol)
    {
#ifdef BAP
        case BAP_PROTOCOL:
            *pi4RetValMpConfigBandwidthManagementProtocol = BAP_PROTOCOL_INDEX;
            break;
#endif
        default:
            *pi4RetValMpConfigBandwidthManagementProtocol = NO_PROTOCOL_INDEX;
            break;
    }
    return (SNMP_SUCCESS);
}
#endif

#ifdef PPP_STACK_WANTED
/****************************************************************************
 Function    :  nmhGetMpConfigHeaderFormatSuspClassRemoteToLocal
 Input       :  The Indices
                IfIndex

                The Object 
                retValMpConfigHeaderFormatSuspClassRemoteToLocal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMpConfigHeaderFormatSuspClassRemoteToLocal (INT4 i4IfIndex,
                                                  INT4
                                                  *pi4RetValMpConfigHeaderFormatSuspClassRemoteToLocal)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = SNMPGetBundleIfptr (i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValMpConfigHeaderFormatSuspClassRemoteToLocal =
        pIf->MPInfo.BundleInfo.BundleOptAllowedForPeer.u1MCMLClassVal;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMpConfigHeaderFormatCodeRemoteVal
 Input       :  The Indices
                IfIndex

                The Object 
                retValMpConfigHeaderFormatCodeRemoteVal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMpConfigHeaderFormatCodeRemoteVal (INT4 i4IfIndex,
                                         INT4
                                         *pi4RetValMpConfigHeaderFormatCodeRemoteVal)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = SNMPGetBundleIfptr (i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValMpConfigHeaderFormatCodeRemoteVal =
        pIf->MPInfo.BundleInfo.BundleOptAckedByPeer.u1MCMLCodeVal;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMpConfigHeaderFormatCodeLocalVal
 Input       :  The Indices
                IfIndex

                The Object 
                retValMpConfigHeaderFormatCodeLocalVal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMpConfigHeaderFormatCodeLocalVal (INT4 i4IfIndex,
                                        INT4
                                        *pi4RetValMpConfigHeaderFormatCodeLocalVal)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = SNMPGetBundleIfptr (i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValMpConfigHeaderFormatCodeLocalVal =
        pIf->MPInfo.BundleInfo.BundleOptAckedByLocal.u1MCMLCodeVal;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMpConfigHeaderFormatSuspClassRemoteVal
 Input       :  The Indices
                IfIndex

                The Object 
                retValMpConfigHeaderFormatSuspClassRemoteVal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMpConfigHeaderFormatSuspClassRemoteVal (INT4 i4IfIndex,
                                              INT4
                                              *pi4RetValMpConfigHeaderFormatSuspClassRemoteVal)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = SNMPGetBundleIfptr (i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValMpConfigHeaderFormatSuspClassRemoteVal =
        pIf->MPInfo.BundleInfo.BundleOptAckedByLocal.u1MCMLClassVal;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMpConfigHeaderFormatSuspClassLocalVal
 Input       :  The Indices
                IfIndex

                The Object 
                retValMpConfigHeaderFormatSuspClassLocalVal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMpConfigHeaderFormatSuspClassLocalVal (INT4 i4IfIndex,
                                             INT4
                                             *pi4RetValMpConfigHeaderFormatSuspClassLocalVal)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = SNMPGetBundleIfptr (i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValMpConfigHeaderFormatSuspClassLocalVal =
        pIf->MPInfo.BundleInfo.BundleOptAckedByLocal.u1MCMLClassVal;
    return SNMP_SUCCESS;
}
#endif /* PPP_STACK_WANTED */

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMpConfigSeqNoHeaderFormat
 Input       :  The Indices
                MpConfigIfIndex

                The Object 
                setValMpConfigHeaderFormat
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMpConfigSeqNoHeaderFormat (INT4 i4MpConfigIfIndex,
                                 INT4 i4SetValMpConfigHeaderFormat)
{
    tPPPIf             *pIf;

    if ((pIf =
         (tPPPIf *) SNMPGetBundleIfptr ((UINT4) i4MpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (i4SetValMpConfigHeaderFormat == SHORT_MP_CFG_HDR_FORMAT)
    {
        pIf->MPInfo.BundleInfo.MPFlagsPerIf[SEQ_IDX].FlagMask |= DESIRED_SET;
    }
    else
    {
        pIf->MPInfo.BundleInfo.MPFlagsPerIf[SEQ_IDX].FlagMask &=
            DESIRED_NOT_SET;

    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetMpConfigBundleClass
 Input       :  The Indices
                MpConfigIfIndex

                The Object 
                setValMpConfigBundleClass
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMpConfigBundleClass (INT4 i4MpConfigIfIndex,
                           INT4 i4SetValMpConfigBundleClass)
{
    tPPPIf             *pIf;

    if ((pIf =
         (tPPPIf *) SNMPGetBundleIfptr ((UINT4) i4MpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIf->MPInfo.BundleInfo.BundleOptDesired.BundleId.Class =
        (UINT1) i4SetValMpConfigBundleClass;
    if (i4SetValMpConfigBundleClass != NULL_CLASS)
    {
        pIf->MPInfo.BundleInfo.MPFlagsPerIf[EPD_IDX].FlagMask |= DESIRED_SET;
    }
    else
    {
        pIf->MPInfo.BundleInfo.MPFlagsPerIf[EPD_IDX].FlagMask &=
            DESIRED_NOT_SET;
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetMpConfigBundleAddr
 Input       :  The Indices
                MpConfigIfIndex

                The Object 
                setValMpConfigBundleAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMpConfigBundleAddr (INT4 i4MpConfigIfIndex,
                          tSNMP_OCTET_STRING_TYPE * pSetValMpConfigBundleAddr)
{
    tPPPIf             *pIf;

    if ((pIf =
         (tPPPIf *) SNMPGetBundleIfptr ((UINT4) i4MpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    MEMCPY (pIf->MPInfo.BundleInfo.BundleOptDesired.BundleId.Address,
            pSetValMpConfigBundleAddr->pu1_OctetList,
            pSetValMpConfigBundleAddr->i4_Length);
    pIf->MPInfo.BundleInfo.BundleOptDesired.BundleId.Length =
        (UINT1) (pSetValMpConfigBundleAddr->i4_Length);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetMpConfigNullFragTimeOutValue
 Input       :  The Indices
                MpConfigIfIndex

                The Object 
                setValMpConfigNullFragTimeOutValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMpConfigNullFragTimeOutValue (INT4 i4MpConfigIfIndex,
                                    INT4 i4SetValMpConfigNullFragTimeOutValue)
{
    tPPPIf             *pIf;

    if ((pIf =
         (tPPPIf *) SNMPGetBundleIfptr ((UINT4) i4MpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIf->MPInfo.BundleInfo.NullFragTimeOutVal =
        (UINT4) (i4SetValMpConfigNullFragTimeOutValue);
    return (SNMP_SUCCESS);

}

#ifdef PPP_STACK_WANTED
/****************************************************************************
 Function    :  nmhSetMpConfigHeaderFormatCodeLocalToRemote
 Input       :  The Indices
                IfIndex

                The Object 
                setValMpConfigHeaderFormatCodeLocalToRemote
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMpConfigHeaderFormatCodeLocalToRemote (INT4 i4IfIndex,
                                             INT4
                                             i4SetValMpConfigHeaderFormatCodeLocalToRemote)
{
    tPPPIf             *pIf;

    if ((pIf = SNMPGetBundleIfptr (i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIf->MPInfo.BundleInfo.BundleOptDesired.u1MCMLCodeVal =
        i4SetValMpConfigHeaderFormatCodeLocalToRemote;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetMpConfigHeaderFormatCodeRemoteToLocal
 Input       :  The Indices
                IfIndex

                The Object 
                setValMpConfigHeaderFormatCodeRemoteToLocal
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMpConfigHeaderFormatCodeRemoteToLocal (INT4 i4IfIndex,
                                             INT4
                                             i4SetValMpConfigHeaderFormatCodeRemoteToLocal)
{
    tPPPIf             *pIf;

    if ((pIf = SNMPGetBundleIfptr (i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIf->MPInfo.BundleInfo.BundleOptAllowedForPeer.u1MCMLCodeVal =
        i4SetValMpConfigHeaderFormatCodeRemoteToLocal;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetMpConfigHeaderFormatSuspClassLocalToRemote
 Input       :  The Indices
                IfIndex

                The Object 
                setValMpConfigHeaderFormatSuspClassLocalToRemote
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMpConfigHeaderFormatSuspClassLocalToRemote (INT4 i4IfIndex,
                                                  INT4
                                                  i4SetValMpConfigHeaderFormatSuspClassLocalToRemote)
{
    tPPPIf             *pIf;

    if ((pIf = SNMPGetBundleIfptr (i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIf->MPInfo.BundleInfo.BundleOptDesired.u1MCMLClassVal =
        i4SetValMpConfigHeaderFormatSuspClassLocalToRemote;
    return SNMP_SUCCESS;
}
#endif /* PPP_STACK_WANTED */

/****************************************************************************
 Function    :  nmhSetMpConfigReAssemblyTimeOutValue
 Input       :  The Indices
                MpConfigIfIndex

                The Object 
                setValMpConfigReAssemblyTimeOutValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMpConfigReAssemblyTimeOutValue (INT4 i4MpConfigIfIndex,
                                      INT4
                                      i4SetValMpConfigReAssemblyTimeOutValue)
{
    tPPPIf             *pIf;

    if ((pIf =
         (tPPPIf *) SNMPGetBundleIfptr ((UINT4) i4MpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIf->MPInfo.BundleInfo.ReassemblyTimeOutVal =
        (UINT4) (i4SetValMpConfigReAssemblyTimeOutValue);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetMpConfigBundleDirection
 Input       :  The Indices
                MpConfigIfIndex

                The Object 
                setValMpConfigBundleDirection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMpConfigBundleDirection (INT4 i4MpConfigIfIndex,
                               INT4 i4SetValMpConfigBundleDirection)
{
    tPPPIf             *pIf;

    if ((pIf =
         (tPPPIf *) SNMPGetBundleIfptr ((UINT4) i4MpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIf->MPInfo.BundleInfo.BundleDirection =
        (UINT1) i4SetValMpConfigBundleDirection;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetMpConfigMinThruput
 Input       :  The Indices
                MpConfigIfIndex

                The Object 
                setValMpConfigMinThruput
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMpConfigMinThruput (INT4 i4MpConfigIfIndex,
                          INT4 i4SetValMpConfigMinThruput)
{
    tPPPIf             *pIf;

    if ((pIf =
         (tPPPIf *) SNMPGetBundleIfptr ((UINT4) i4MpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIf->MPInfo.BundleInfo.MinThruput = (UINT4) i4SetValMpConfigMinThruput;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetMpConfigMaxThruput
 Input       :  The Indices
                MpConfigIfIndex

                The Object 
                setValMpConfigMaxThruput
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMpConfigMaxThruput (INT4 i4MpConfigIfIndex,
                          INT4 i4SetValMpConfigMaxThruput)
{
    tPPPIf             *pIf;

    if ((pIf =
         (tPPPIf *) SNMPGetBundleIfptr ((UINT4) i4MpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIf->MPInfo.BundleInfo.MaxThruput = (UINT4) i4SetValMpConfigMaxThruput;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetMpConfigBandwidthManagementProtocol
 Input       :  The Indices
                MpConfigIfIndex

                The Object 
                setValMpConfigBandwidthManagementProtocol
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef BAP
INT1
nmhSetMpConfigBandwidthManagementProtocol (INT4 i4MpConfigIfIndex,
                                           INT4
                                           i4SetValMpConfigBandwidthManagementProtocol)
{
    tPPPIf             *pIf;
#ifdef BAP
    tBAPInfo           *pBAPInfo;
#endif

    pIf = (tPPPIf *) SNMPGetBundleIfptr (i4MpConfigIfIndex);
    if (pIf->MPInfo.BundleInfo.BWMProtocolInfo.Protocol != 0)
    {
        /*
           Currently only BAP and NONE protocols are supported, so if
           this value is 0 (No protocol), delete the previous BAP/MPPLUS info
           structure and then create it again.
         */
        switch (pIf->MPInfo.BundleInfo.BWMProtocolInfo.Protocol)
        {
#ifdef BAP
            case BAP_PROTOCOL:
                BAPDelete (pIf);
                break;
#endif
            default:
                return (SNMP_FAILURE);
        }
    }

    switch (i4SetValMpConfigBandwidthManagementProtocol)
    {
#ifdef BAP
        case BAP_PROTOCOL_INDEX:
            if ((SLL_COUNT (&pIf->MPInfo.BundleInfo.MemberList) == 0)
                && ((pBAPInfo = BAPCreateAndInit (pIf)) != NULL))
            {
                pIf->MPInfo.BundleInfo.BWMProtocolInfo.pProtocolInfo = pBAPInfo;
                pIf->MPInfo.BundleInfo.BWMProtocolInfo.Protocol = BAP_PROTOCOL;
                BACPInit (pIf);
                pBAPInfo->BACPIf.AdminStatus = ADMIN_OPEN;
                BACPEnableIf (&pBAPInfo->BACPIf);
            }
            else
            {
                return (SNMP_FAILURE);
            }
            break;
#endif

        default:
            pIf->MPInfo.BundleInfo.BWMProtocolInfo.Protocol = 0;
            break;
    }
    return (SNMP_SUCCESS);

}
#endif

/****************************************************************************
 Function    :  nmhGetMpConfigHeaderFormatCode
 Input       :  The Indices
                IfIndex

                The Object 
                retValMpConfigHeaderFormatCode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMpConfigHeaderFormatCode (INT4 i4IfIndex,
                                INT4 *pi4RetValMpConfigHeaderFormatCode)
{
    PPP_UNUSED (i4IfIndex);
    PPP_UNUSED (pi4RetValMpConfigHeaderFormatCode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMpConfigHeaderFormatSuspClass
 Input       :  The Indices
                IfIndex

                The Object 
                retValMpConfigHeaderFormatSuspClass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMpConfigHeaderFormatSuspClass (INT4 i4IfIndex,
                                     INT4
                                     *pi4RetValMpConfigHeaderFormatSuspClass)
{
    PPP_UNUSED (i4IfIndex);
    PPP_UNUSED (*pi4RetValMpConfigHeaderFormatSuspClass);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetMpConfigHeaderFormatCode
 Input       :  The Indices
                IfIndex

                The Object 
                setValMpConfigHeaderFormatCode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMpConfigHeaderFormatCode (INT4 i4IfIndex,
                                INT4 i4SetValMpConfigHeaderFormatCode)
{
    PPP_UNUSED (i4IfIndex);
    PPP_UNUSED (i4SetValMpConfigHeaderFormatCode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetMpConfigHeaderFormatSuspClass
 Input       :  The Indices
                IfIndex

                The Object 
                setValMpConfigHeaderFormatSuspClass
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMpConfigHeaderFormatSuspClass (INT4 i4IfIndex,
                                     INT4 i4SetValMpConfigHeaderFormatSuspClass)
{

    PPP_UNUSED (i4IfIndex);
    PPP_UNUSED (i4SetValMpConfigHeaderFormatSuspClass);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MpConfigHeaderFormatCode
 Input       :  The Indices
                IfIndex

                The Object 
                testValMpConfigHeaderFormatCode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MpConfigHeaderFormatCode (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                   INT4 i4TestValMpConfigHeaderFormatCode)
{
    PPP_UNUSED (*pu4ErrorCode);
    PPP_UNUSED (i4IfIndex);
    PPP_UNUSED (i4TestValMpConfigHeaderFormatCode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MpConfigHeaderFormatSuspClass
 Input       :  The Indices
                IfIndex

                The Object 
                testValMpConfigHeaderFormatSuspClass
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MpConfigHeaderFormatSuspClass (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                        INT4
                                        i4TestValMpConfigHeaderFormatSuspClass)
{
    PPP_UNUSED (*pu4ErrorCode);
    PPP_UNUSED (i4IfIndex);
    PPP_UNUSED (i4TestValMpConfigHeaderFormatSuspClass);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMpConfigHeaderFormatNegFlag
 Input       :  The Indices
                IfIndex

                The Object 
                retValMpConfigHeaderFormatNegFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMpConfigHeaderFormatNegFlag (INT4 i4IfIndex,
                                   INT4 *pi4RetValMpConfigHeaderFormatNegFlag)
{
    PPP_UNUSED (i4IfIndex);
    PPP_UNUSED (*pi4RetValMpConfigHeaderFormatNegFlag);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetMpConfigHeaderFormatNegFlag
 Input       :  The Indices
                IfIndex

                The Object 
                setValMpConfigHeaderFormatNegFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMpConfigHeaderFormatNegFlag (INT4 i4IfIndex,
                                   INT4 i4SetValMpConfigHeaderFormatNegFlag)
{
    PPP_UNUSED (i4IfIndex);
    PPP_UNUSED (i4SetValMpConfigHeaderFormatNegFlag);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2MpConfigHeaderFormatNegFlag
 Input       :  The Indices
                IfIndex

                The Object 
                testValMpConfigHeaderFormatNegFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MpConfigHeaderFormatNegFlag (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                      INT4 i4TestValMpConfigHeaderFormatNegFlag)
{
    PPP_UNUSED (*pu4ErrorCode);
    PPP_UNUSED (i4IfIndex);
    PPP_UNUSED (i4TestValMpConfigHeaderFormatNegFlag);
    return SNMP_SUCCESS;
}

#ifdef PPP_STACK_WANTED
/****************************************************************************
 Function    :  nmhSetMpConfigHeaderFormatSuspClassRemoteToLocal
 Input       :  The Indices
                IfIndex

                The Object 
                setValMpConfigHeaderFormatSuspClassRemoteToLocal
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMpConfigHeaderFormatSuspClassRemoteToLocal (INT4 i4IfIndex,
                                                  INT4
                                                  i4SetValMpConfigHeaderFormatSuspClassRemoteToLocal)
{
    tPPPIf             *pIf;

    if ((pIf = SNMPGetBundleIfptr (i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIf->MPInfo.BundleInfo.BundleOptAllowedForPeer.u1MCMLClassVal
        = i4SetValMpConfigHeaderFormatSuspClassRemoteToLocal;
    return SNMP_SUCCESS;
}
#endif

/****************************************************************************
 Function    :  nmhSetMpConfigMRRU
 Input       :  The Indices
                PppTestConfigIfIndex

                The Object 
                setValMpConfigMRRU
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMpConfigMRRU (INT4 i4IfIndex, INT4 i4SetValMpConfigMRRU)
{
    tPPPIf             *pIf;

    if ((pIf = SNMPGetBundleIfptr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIf->MPInfo.BundleInfo.BundleOptDesired.MRRU = (UINT2) i4SetValMpConfigMRRU;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetMpConfigMRRU
 Input       :  The Indices
                i4IfIndex

                The Object 
                retValMpConfigMRRU
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMpConfigMRRU (INT4 i4IfIndex, INT4 *pi4RetValMpConfigMRRU)
{
    tPPPIf             *pIf;

    if ((pIf = SNMPGetBundleIfptr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValMpConfigMRRU = pIf->MPInfo.BundleInfo.BundleOptDesired.MRRU;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2MpConfigMRRU
 Input       :  The Indices
                i4IfIndex

                The Object 
                testValMpConfigMRRU
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MpConfigMRRU (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                       INT4 i4TestValMpConfigMRRU)
{


    if ((SNMPGetBundleIfptr ((UINT4) i4IfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValMpConfigMRRU < 1500)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MpConfigSeqNoHeaderFormat
 Input       :  The Indices
                MpConfigIfIndex

                The Object 
                testValMpConfigHeaderFormat
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MpConfigSeqNoHeaderFormat (UINT4 *pu4ErrorCode, INT4 i4MpConfigIfIndex,
                                    INT4 i4TestValMpConfigHeaderFormat)
{


    if ((SNMPGetBundleIfptr ((UINT4) i4MpConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
    if ((i4TestValMpConfigHeaderFormat != SHORT_MP_CFG_HDR_FORMAT)
        && (i4TestValMpConfigHeaderFormat != LONG_MP_CFG_HDR_FORMAT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2MpConfigBundleClass
 Input       :  The Indices
                MpConfigIfIndex

                The Object 
                testValMpConfigBundleClass
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MpConfigBundleClass (UINT4 *pu4ErrorCode, INT4 i4MpConfigIfIndex,
                              INT4 i4TestValMpConfigBundleClass)
{


    if((SNMPGetBundleIfptr ((UINT4) i4MpConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    switch (i4TestValMpConfigBundleClass)
    {
        case NULL_CLASS:
        case IP_ADDRESS_CLASS:
        case GLOB_MAC_ADDRESS_CLASS:
        case PSND_CLASS:
        case LOC_MAC_ADDRESS_CLASS:
        case MAGIC_CLASS:
            return (SNMP_SUCCESS);

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (SNMP_FAILURE);

    }

}

/****************************************************************************
 Function    :  nmhTestv2MpConfigBundleAddr
 Input       :  The Indices
                MpConfigIfIndex

                The Object 
                testValMpConfigBundleAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MpConfigBundleAddr (UINT4 *pu4ErrorCode, INT4 i4MpConfigIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pTestValMpConfigBundleAddr)
{


    if(( SNMPGetBundleIfptr ((UINT4) i4MpConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((pTestValMpConfigBundleAddr->i4_Length < MIN_MP_ADDRESS_SIZE)
        || (pTestValMpConfigBundleAddr->i4_Length > MAX_MP_ADDRESS_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

#ifdef PPP_STACK_WANTED
/****************************************************************************
 Function    :  nmhTestv2MpConfigHeaderFormatCodeLocalToRemote
 Input       :  The Indices
                IfIndex

                The Object 
                testValMpConfigHeaderFormatCodeLocalToRemote
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MpConfigHeaderFormatCodeLocalToRemote (UINT4 *pu4ErrorCode,
                                                INT4 i4IfIndex,
                                                INT4
                                                i4TestValMpConfigHeaderFormatCodeLocalToRemote)
{
    tPPPIf             *pIf = NULL;
    INT4                i4MpConfigHeaderFormat;

    if ((pIf = (tPPPIf *) SNMPGetBundleIfptr (i4IfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
    if ((i4TestValMpConfigHeaderFormatCodeLocalToRemote !=
         MP_LONG_HEADER_FORMAT) &&
        (i4TestValMpConfigHeaderFormatCodeLocalToRemote !=
         MP_SHORT_HEADER_FORMAT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    /*
     *Short Sequence must not be configured 
     *when MultiClass is configured.
     *
     * */
    if (nmhGetMpConfigSeqNoHeaderFormat (i4IfIndex,
                                         &i4MpConfigHeaderFormat)
        == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    /*
     * One is Multilink short sequence value.
     * */
    if (i4MpConfigHeaderFormat == 1)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2MpConfigHeaderFormatCodeRemoteToLocal
 Input       :  The Indices
                IfIndex

                The Object 
                testValMpConfigHeaderFormatCodeRemoteToLocal
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MpConfigHeaderFormatCodeRemoteToLocal (UINT4 *pu4ErrorCode,
                                                INT4 i4IfIndex,
                                                INT4
                                                i4TestValMpConfigHeaderFormatCodeRemoteToLocal)
{
    tPPPIf             *pIf = NULL;
    INT4                i4MpConfigHeaderFormat;

    if ((pIf = (tPPPIf *) SNMPGetBundleIfptr (i4IfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
    if ((i4TestValMpConfigHeaderFormatCodeRemoteToLocal !=
         MP_LONG_HEADER_FORMAT) &&
        (i4TestValMpConfigHeaderFormatCodeRemoteToLocal !=
         MP_SHORT_HEADER_FORMAT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    if (nmhGetMpConfigSeqNoHeaderFormat (i4IfIndex,
                                         &i4MpConfigHeaderFormat)
        == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (i4MpConfigHeaderFormat == SHORT_MP_CFG_HDR_FORMAT)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2MpConfigHeaderFormatSuspClassLocalToRemote
 Input       :  The Indices
                IfIndex

                The Object 
                testValMpConfigHeaderFormatSuspClassLocalToRemote
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MpConfigHeaderFormatSuspClassLocalToRemote (UINT4 *pu4ErrorCode,
                                                     INT4 i4IfIndex,
                                                     INT4
                                                     i4TestValMpConfigHeaderFormatSuspClassLocalToRemote)
{
    tPPPIf             *pIf = NULL;
    INT4                i4MpConfigHeaderFormat;
    INT4                i4MpConfigCode;

    if ((pIf = (tPPPIf *) SNMPGetBundleIfptr (i4IfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
    if (nmhGetMpConfigHeaderFormatCodeLocalToRemote (i4IfIndex,
                                                     &i4MpConfigCode)
        == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
    /*
     * For Long sequence header format maximum suspended classes are 16
     * For short sequence header format maximum suspended classes are 4
     * */
    if (((i4MpConfigCode == MP_LONG_HEADER_FORMAT) &&
         ((i4TestValMpConfigHeaderFormatSuspClassLocalToRemote <
           MP_MIN_SUSP_CLASS) ||
          (i4TestValMpConfigHeaderFormatSuspClassLocalToRemote >
           MP_LONG_MAX_SUSP_CLASS))))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (((i4MpConfigCode == MP_SHORT_HEADER_FORMAT) &&
         ((i4TestValMpConfigHeaderFormatSuspClassLocalToRemote <
           MP_MIN_SUSP_CLASS) ||
          (i4TestValMpConfigHeaderFormatSuspClassLocalToRemote >
           MP_SHORT_MAX_SUSP_CLASS))))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    /*
     *Short Sequence must not be configured 
     *when MultiClass is configured.
     *
     * */
    if (nmhGetMpConfigSeqNoHeaderFormat (i4IfIndex,
                                         &i4MpConfigHeaderFormat)
        == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (i4MpConfigHeaderFormat == SHORT_MP_CFG_HDR_FORMAT)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;
}
#endif /* PPP_STACK_WANTED */

/****************************************************************************
 Function    :  nmhTestv2MpConfigNullFragTimeOutValue
 Input       :  The Indices
                MpConfigIfIndex

                The Object 
                testValMpConfigNullFragTimeOutValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MpConfigNullFragTimeOutValue (UINT4 *pu4ErrorCode,
                                       INT4 i4MpConfigIfIndex,
                                       INT4
                                       i4TestValMpConfigNullFragTimeOutValue)
{


    if ((SNMPGetBundleIfptr ((UINT4) i4MpConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValMpConfigNullFragTimeOutValue <= MIN_INTEGER)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2MpConfigReAssemblyTimeOutValue
 Input       :  The Indices
                MpConfigIfIndex

                The Object 
                testValMpConfigReAssemblyTimeOutValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MpConfigReAssemblyTimeOutValue (UINT4 *pu4ErrorCode,
                                         INT4 i4MpConfigIfIndex,
                                         INT4
                                         i4TestValMpConfigReAssemblyTimeOutValue)
{


    if ((SNMPGetBundleIfptr ((UINT4) i4MpConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValMpConfigReAssemblyTimeOutValue <= MIN_INTEGER)
       
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2MpConfigBundleDirection
 Input       :  The Indices
                MpConfigIfIndex

                The Object 
                testValMpConfigBundleDirection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MpConfigBundleDirection (UINT4 *pu4ErrorCode, INT4 i4MpConfigIfIndex,
                                  INT4 i4TestValMpConfigBundleDirection)
{


    if ((SNMPGetBundleIfptr ((UINT4) i4MpConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValMpConfigBundleDirection < MIN_MP_CFG_BUNDLE_RDIRCT
        || i4TestValMpConfigBundleDirection > MAX_MP_CFG_BUNDLE_RDIRCT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2MpConfigMinThruput
 Input       :  The Indices
                MpConfigIfIndex

                The Object 
                testValMpConfigMinThruput
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MpConfigMinThruput (UINT4 *pu4ErrorCode, INT4 i4MpConfigIfIndex,
                             INT4 i4TestValMpConfigMinThruput)
{


    if ((SNMPGetBundleIfptr ((UINT4) i4MpConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValMpConfigMinThruput < MIN_INTEGER)
    
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2MpConfigMaxThruput
 Input       :  The Indices
                MpConfigIfIndex

                The Object 
                testValMpConfigMaxThruput
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MpConfigMaxThruput (UINT4 *pu4ErrorCode, INT4 i4MpConfigIfIndex,
                             INT4 i4TestValMpConfigMaxThruput)
{


    if ((SNMPGetBundleIfptr ((UINT4) i4MpConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValMpConfigMaxThruput < MIN_INTEGER)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2MpConfigBandwidthManagementProtocol
 Input       :  The Indices
                MpConfigIfIndex

                The Object 
                testValMpConfigBandwidthManagementProtocol
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MpConfigBandwidthManagementProtocol (UINT4 *pu4ErrorCode,
                                              INT4 i4MpConfigIfIndex,
                                              INT4
                                              i4TestValMpConfigBandwidthManagementProtocol)
{


    if ((SNMPGetBundleIfptr ((UINT4) i4MpConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    switch (i4TestValMpConfigBandwidthManagementProtocol)
    {
#if (BAP || MPPLUS)
        case NO_PROTOCOL_INDEX:
            return (SNMP_SUCCESS);
#endif
#ifdef BAP
        case BAP_PROTOCOL_INDEX:
            return (SNMP_SUCCESS);
#endif
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            break;
    }
    return (SNMP_FAILURE);

}

INT1
get_pppMpConfigMRRU (UINT4 Index, INT4 *sl_value)
{
    PPP_UNUSED (sl_value);
    if ((SNMPGetBundleIfptr (Index)) == NULL)
    {
        return (PPP_SNMP_ERR);
    }

    return (PPP_SNMP_OK);
}

#ifdef PPP_STACK_WANTED
/****************************************************************************
 Function    :  nmhTestv2MpConfigHeaderFormatSuspClassRemoteToLocal
 Input       :  The Indices
                IfIndex

                The Object 
                testValMpConfigHeaderFormatSuspClassRemoteToLocal
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MpConfigHeaderFormatSuspClassRemoteToLocal (UINT4 *pu4ErrorCode,
                                                     INT4 i4IfIndex,
                                                     INT4
                                                     i4TestValMpConfigHeaderFormatSuspClassRemoteToLocal)
{
    tPPPIf             *pIf = NULL;
    INT4                i4MpConfigHeaderFormat;
    INT4                i4MpConfigCode;

    if ((pIf = (tPPPIf *) SNMPGetBundleIfptr (i4IfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
    if (nmhGetMpConfigHeaderFormatCodeLocalToRemote (i4IfIndex,
                                                     &i4MpConfigCode)
        == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
    /*
     * For Long sequence header format maximum suspended classes are 16
     * For short sequence header format maximum suspended classes are 4
     * */
    if (((i4MpConfigCode == MP_LONG_HEADER_FORMAT) &&
         ((i4TestValMpConfigHeaderFormatSuspClassRemoteToLocal <
           MP_MIN_SUSP_CLASS) ||
          (i4TestValMpConfigHeaderFormatSuspClassRemoteToLocal >
           MP_LONG_MAX_SUSP_CLASS))))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    if (((i4MpConfigCode == MP_SHORT_HEADER_FORMAT) &&
         ((i4TestValMpConfigHeaderFormatSuspClassRemoteToLocal <
           MP_MIN_SUSP_CLASS) ||
          (i4TestValMpConfigHeaderFormatSuspClassRemoteToLocal >
           MP_SHORT_MAX_SUSP_CLASS))))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    /*
     *Short Sequence must not be configured 
     *when MultiClass is configured.
     *
     * */
    if (nmhGetMpConfigSeqNoHeaderFormat (i4IfIndex,
                                         &i4MpConfigHeaderFormat)
        == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (i4MpConfigHeaderFormat == SHORT_MP_CFG_HDR_FORMAT)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MpPrefixElisionConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMpPrefixElisionConfigTable
 Input       :  The Indices
                IfIndex
                MpPrefixElisionConfigClass
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMpPrefixElisionConfigTable (INT4 i4IfIndex,
                                                    INT4
                                                    i4MpPrefixElisionConfigClass)
{
    tPPPIf             *pIf;
    tBundleInfo        *pBundleInfo = NULL;
    INT4                i4ClassMaxIndex;

    if ((pIf = (tPPPIf *) SNMPGetBundleIfptr (i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pBundleInfo = &pIf->MPInfo.BundleInfo;

    i4ClassMaxIndex = (pBundleInfo->BundleOptDesired.u1MCMLClassVal >
                       pBundleInfo->BundleOptAllowedForPeer.
                       u1MCMLClassVal) ?
        pBundleInfo->BundleOptDesired.u1MCMLClassVal :
        pBundleInfo->BundleOptAllowedForPeer.u1MCMLClassVal;
    if (i4MpPrefixElisionConfigClass > i4ClassMaxIndex)
    {
        return SNMP_FAILURE;
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMpPrefixElisionConfigTable
 Input       :  The Indices
                IfIndex
                MpPrefixElisionConfigClass
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMpPrefixElisionConfigTable (INT4 *pi4IfIndex,
                                            INT4 *pi4MpPrefixElisionConfigClass)
{
    tPPPIf             *pIf;
    tBundleInfo        *pBundleInfo = NULL;
    INT4                i4MpConfigIfIndex = 0;
    INT4                i4MPNxtConfigIfIndex = 0;
    INT4                i4ClassIndex, i4ClassMaxIndex;

    if (nmhGetFirstIndexMpConfigTable (&i4MPNxtConfigIfIndex) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    do
    {
        i4MpConfigIfIndex = i4MPNxtConfigIfIndex;

        if ((pIf = (tPPPIf *) SNMPGetBundleIfptr (i4MpConfigIfIndex)) == NULL)
        {
            return (SNMP_FAILURE);
        }
        pBundleInfo = &pIf->MPInfo.BundleInfo;

        i4ClassMaxIndex = (pBundleInfo->BundleOptDesired.u1MCMLClassVal >
                           pBundleInfo->BundleOptAllowedForPeer.
                           u1MCMLClassVal) ?
            pBundleInfo->BundleOptDesired.u1MCMLClassVal :
            pBundleInfo->BundleOptAllowedForPeer.u1MCMLClassVal;

        if (i4ClassMaxIndex == MP_MIN_SUSP_CLASS)
        {

            *pi4IfIndex = i4MpConfigIfIndex;
            *pi4MpPrefixElisionConfigClass = i4ClassMaxIndex;
            return (SNMP_SUCCESS);
        }

        for (i4ClassIndex = 2; i4ClassIndex <= i4ClassMaxIndex; i4ClassIndex++)
        {
            if ((pBundleInfo->BundleOptDesired.
                 aPrefixElisionInfo[i4ClassIndex].u1PrefixLen
                 != 0) || (pBundleInfo->BundleOptAckedByPeer.
                           aPrefixElisionInfo[i4ClassIndex].u1PrefixLen) != 0)
            {
                *pi4IfIndex = i4MpConfigIfIndex;
                *pi4MpPrefixElisionConfigClass = i4ClassIndex;
                return (SNMP_SUCCESS);
            }
        }

    }
    while (nmhGetNextIndexMpConfigTable (i4MpConfigIfIndex,
                                         &i4MPNxtConfigIfIndex)
           == SNMP_SUCCESS);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMpPrefixElisionConfigTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                MpPrefixElisionConfigClass
                nextMpPrefixElisionConfigClass
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMpPrefixElisionConfigTable (INT4 i4IfIndex,
                                           INT4 *pi4NextIfIndex,
                                           INT4 i4MpPrefixElisionConfigClass,
                                           INT4
                                           *pi4NextMpPrefixElisionConfigClass)
{
    tPPPIf             *pIf;
    tBundleInfo        *pBundleInfo = NULL;
    INT4                i4ClassIndex, i4ClassMaxIndex;
    INT4                i4MpConfigIfIndex;
    INT4                i4MPNxtConfigIfIndex = i4IfIndex;

    do
    {
        i4MpConfigIfIndex = i4MPNxtConfigIfIndex;

        if ((pIf = (tPPPIf *) SNMPGetBundleIfptr (i4MpConfigIfIndex)) == NULL)
        {
            return (SNMP_FAILURE);
        }

        pBundleInfo = &pIf->MPInfo.BundleInfo;

        i4ClassMaxIndex = (pBundleInfo->BundleOptDesired.u1MCMLClassVal >
                           pBundleInfo->BundleOptAllowedForPeer.
                           u1MCMLClassVal) ?
            pBundleInfo->BundleOptDesired.u1MCMLClassVal :
            pBundleInfo->BundleOptAllowedForPeer.u1MCMLClassVal;

        for (i4ClassIndex = i4MpPrefixElisionConfigClass + 1;
             i4ClassIndex <= i4ClassMaxIndex; i4ClassIndex++)
        {
            if ((pBundleInfo->BundleOptDesired.
                 aPrefixElisionInfo[i4ClassIndex].u1PrefixLen
                 != 0) || (pBundleInfo->BundleOptAckedByPeer.
                           aPrefixElisionInfo[i4ClassIndex].u1PrefixLen) != 0)
            {
                *pi4NextIfIndex = i4MpConfigIfIndex;
                *pi4NextMpPrefixElisionConfigClass = i4ClassIndex;
                return (SNMP_SUCCESS);
            }
        }

    }
    while (nmhGetNextIndexMpConfigTable (i4MpConfigIfIndex,
                                         &i4MPNxtConfigIfIndex)
           == SNMP_SUCCESS);
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMpPrefixElisionConfigValue
 Input       :  The Indices
                IfIndex
                MpPrefixElisionConfigClass

                The Object 
                retValMpPrefixElisionConfigValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMpPrefixElisionConfigValue (INT4 i4IfIndex,
                                  INT4 i4MpPrefixElisionConfigClass,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValMpPrefixElisionConfigValue)
{
    tPPPIf             *pIf;

    if ((pIf = (tPPPIf *) SNMPGetBundleIfptr (i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pRetValMpPrefixElisionConfigValue->i4_Length =
        pIf->MPInfo.BundleInfo.BundleOptDesired.aPrefixElisionInfo
        [i4MpPrefixElisionConfigClass].u1PrefixLen;
    MEMSET (pRetValMpPrefixElisionConfigValue->pu1_OctetList, 0,
            pRetValMpPrefixElisionConfigValue->i4_Length);

    MEMCPY (pRetValMpPrefixElisionConfigValue->pu1_OctetList,
            pIf->MPInfo.BundleInfo.BundleOptDesired.aPrefixElisionInfo
            [i4MpPrefixElisionConfigClass].u1PrefixVal,
            pRetValMpPrefixElisionConfigValue->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMpPrefixElisionRemoteVal
 Input       :  The Indices
                IfIndex
                MpPrefixElisionConfigClass

                The Object 
                retValMpPrefixElisionRemoteVal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMpPrefixElisionRemoteVal (INT4 i4IfIndex,
                                INT4 i4MpPrefixElisionConfigClass,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValMpPrefixElisionRemoteVal)
{
    tPPPIf             *pIf;

    if ((pIf = (tPPPIf *) SNMPGetBundleIfptr (i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pRetValMpPrefixElisionRemoteVal->i4_Length =
        pIf->MPInfo.BundleInfo.BundleOptAckedByLocal.
        aPrefixElisionInfo[i4MpPrefixElisionConfigClass].u1PrefixLen;

    MEMCPY (pRetValMpPrefixElisionRemoteVal->pu1_OctetList,
            pIf->MPInfo.BundleInfo.BundleOptAckedByLocal.
            aPrefixElisionInfo[i4MpPrefixElisionConfigClass].
            u1PrefixVal, pRetValMpPrefixElisionRemoteVal->i4_Length);

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMpPrefixElisionConfigValue
 Input       :  The Indices
                IfIndex
                MpPrefixElisionConfigClass

                The Object 
                setValMpPrefixElisionConfigValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMpPrefixElisionConfigValue (INT4 i4IfIndex,
                                  INT4 i4MpPrefixElisionConfigClass,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pSetValMpPrefixElisionConfigValue)
{
    tPPPIf             *pIf;
    tBundleOptions     *pBundleOpt;

    if ((pIf = (tPPPIf *) SNMPGetBundleIfptr (i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pBundleOpt = &pIf->MPInfo.BundleInfo.BundleOptDesired;
    MEMCPY (pBundleOpt->aPrefixElisionInfo[i4MpPrefixElisionConfigClass].
            u1PrefixVal,
            pSetValMpPrefixElisionConfigValue->pu1_OctetList,
            pSetValMpPrefixElisionConfigValue->i4_Length);

    pBundleOpt->aPrefixElisionInfo[i4MpPrefixElisionConfigClass].u1PrefixLen
        = pSetValMpPrefixElisionConfigValue->i4_Length;

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MpPrefixElisionConfigValue
 Input       :  The Indices
                IfIndex
                MpPrefixElisionConfigClass

                The Object 
                testValMpPrefixElisionConfigValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MpPrefixElisionConfigValue (UINT4 *pu4ErrorCode,
                                     INT4 i4IfIndex,
                                     INT4 i4MpPrefixElisionConfigClass,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pTestValMpPrefixElisionConfigValue)
{
    tPPPIf             *pIf;

    if ((pIf = (tPPPIf *) SNMPGetBundleIfptr (i4IfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
    if (pTestValMpPrefixElisionConfigValue->i4_Length > PPP_MP_PREFIX_MAX_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    if ((i4MpPrefixElisionConfigClass < MP_MIN_SUSP_CLASS) ||
        (i4MpPrefixElisionConfigClass > MP_LONG_MAX_SUSP_CLASS))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
#endif /* PPP_STACK_WANTED */
