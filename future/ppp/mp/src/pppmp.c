/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppmp.c,v 1.7 2014/12/16 11:47:25 siva Exp $
 *
 * Description:This file contains internal routines  of MP module.
 *
 *******************************************************************/

#include  "mpcom.h"
#include  "gcpdefs.h"
#include  "lcpexts.h"
#include  "lcpprot.h"
#include  "genexts.h"
#include "pppipcp.h"
#include "ipexts.h"
#include "pppipv6cp.h"
#include "ipv6exts.h"
#include "pppmuxcp.h"
#include "pppmplscp.h"
#include "mplsexts.h"
#include "pppipxcp.h"
#include "ipxexts.h"

#include "pppbcp.h"
#include "bcpexts.h"

#include "globexts.h"
#include "genproto.h"
#define        ENCAP_OFFSET               3

#define        FIRST_FRAGMENT               0
#define        MIDDLE_OR_END_FRAGMENT    1

#define     DESE_HDR_8_PAD          10
#define     DEF_FRAGMENT_SIZE       50
UINT1               ResetSeqNumFlag;
extern tIP6CPIf    *ALLOC_IP6CP_IF (VOID);
extern tBCPIf      *ALLOC_BCP_IF (VOID);

/***********************************************************************/
/*                      MP INTERNAL  FUNCTIONS                         */
/***********************************************************************
*    Function Name    :    MPSplitAndAllocate
*    Description        :
*                This function is used to split the incoming (from HLI) 
*    datagram into various fragments based on the traffic.
*    Parameter(s)    :  
*                pIf    -    Points to the Bundle Interface structure.
*        pOutData    -    Buffer containing the Datagram.
*            Length    -    Length of the data packet.
*        Protocol    -    Network layer protocol. 
*    Return Value    :    VOID
*********************************************************************/
VOID
MPSplitAndAllocate (tPPPIf * pBundleIf, t_MSG_DESC * pOutData, UINT2 Length,
                    UINT2 Protocol)
{
    tPPPIf             *pTmpPtr;
    tPPPIf             *pMemberToBeUsed;
    t_MSG_DESC         *pFragData;
    UINT2               Factor;
    UINT1               BEFlag;
    UINT1               BeginFlag;
    UINT1               MPHeaderLen;
    UINT2               RemLen;
    UINT2               FragmentLength;
    UINT2               CompLen;
    UINT4               TotalFragCount;
    UINT4               u4CurTicks;
    UINT2               MemberALMRUSize;
    PPP_UNUSED(MemberALMRUSize);
    PPP_UNUSED(RemLen);
    PPP_UNUSED(u4CurTicks);

#ifdef SINGLE_FRAG_WITH_MP_HDR
    PPP_UNUSED (Protocol);
#endif
    /*  Initializations */
    RemLen = 0;
    u4CurTicks = 0;
    pTmpPtr = NULL;
    pFragData = pOutData;
    Factor = 1;
    BEFlag = 0;
    BeginFlag = FIRST_FRAGMENT;
    MPHeaderLen = BYTE_LEN_4;
    TotalFragCount = 0;
    LinkNum = 1;

    /* Check if short sequence number format has been negotiated */
    if (pBundleIf->MPInfo.BundleInfo.MPFlagsPerIf[SEQ_IDX].
        FlagMask & ACKED_BY_LOCAL_MASK)
    {
        MPHeaderLen = BYTE_LEN_2;
    }

#ifndef MP_ALGORITHM_INTERLEAVING
    RemLen = Length;
    CompLen = Length;
#endif

#ifdef MP_ALGORITHM_INTERLEAVING
    /* Calculate the fragment length to be sent and compare it with default size of 50.If it is less then send the pkt without any fragmentation,else fragment and send */
    CompLen = Length;
    if (pBundleIf->MPInfo.BundleInfo.TotalFrag == 0)
    {
        return;
    }
    else
    {
        FragmentLength =
            (UINT2) (CompLen / pBundleIf->MPInfo.BundleInfo.TotalFrag);
    }
    if (FragmentLength < DEF_FRAGMENT_SIZE)
    {
        FragmentLength = (UINT2) VALID_BYTES (pOutData);
        TotalFragCount = 1;
    }
    else
    {
        TotalFragCount = pBundleIf->MPInfo.BundleInfo.TotalFrag;
/* Compare the fragment length with MinMTU - MP Header Len and if it is greater divide by 2. Do this until it becomes less */
        while (FragmentLength > (MinMTU - MPHeaderLen))
        {
            FragmentLength = (UINT2) (FragmentLength / 2);
            Factor = (UINT2) (Factor * 2);
            TotalFragCount *= 2;
        }
    }

/* If the fragment length is again divided after comparing with MinMTu, then it is necessary to multiply no. of frag in each link by the factor with which it is divided */

    SLL_SCAN_OFFSET (&pBundleIf->MPInfo.BundleInfo.MemberList, pTmpPtr,
                     tPPPIf *, FSAP_OFFSETOF (tPPPIf, MPInfo.MemberInfo.NextMPIf))
    {
        if (pTmpPtr != NULL)
        {
            pTmpPtr->LinkInfo.TempNoOfFrag = 0;
            if (Factor != 1)
                pTmpPtr->LinkInfo.NoOfFrag =
                    pTmpPtr->LinkInfo.ConstFragCount * Factor;
            else
                pTmpPtr->LinkInfo.NoOfFrag = pTmpPtr->LinkInfo.ConstFragCount;
        }
    }

/* Now transmit each fragment on the links obtained by calling the fn MPFindLinkToBeUsed and add the begin and end flags also */

    while (TotalFragCount != 0)
    {

        PPP_TRC1 (CONTROL_PLANE, "Length of Remaining pkt to be txmitted : %ld",
                  VALID_BYTES (pOutData));

        /* Find the next link to be enqueued. */
        if ((pMemberToBeUsed = MPFindLinkToBeUsed (pBundleIf)) == NULL)
        {
            PPP_TRC (CONTROL_PLANE, "No links available in the bundle..");
            OutReleaseFlag = PPP_YES;
            return;
        }

        PPP_TRC1 (CONTROL_PLANE, "Member Link selected : %ld",
                  pMemberToBeUsed->LinkInfo.IfIndex);

        MemberALMRUSize = pMemberToBeUsed->LcpOptionsAckedByLocal.MRU;
/*
        We send a packet without the MP header if either sequencing is
        not required by the Control Protocol whose data packet is being
        sent or if this is the only link in the MP bundle.          
        In both cases the ifMTU (the Peer's MRU ) size of the link to 
        datagram. We check for all these conditions. 
*/

        if ((pMemberToBeUsed->LcpGSEM.pNegFlagsPerIf[PADDING_IDX].
             FlagMask & ACKED_BY_LOCAL_MASK))
        {
            CompLen = (UINT2) (Length + 1);
        }

#ifndef SINGLE_FRAG_WITH_MP_HDR
        if ((MemberALMRUSize >= CompLen)
            && ((pBundleIf->LcpGSEM.MiscParam.CharValue == NOT_MUST)
                || (SLL_COUNT (&pBundleIf->MPInfo.BundleInfo.MemberList) ==
                    SINGLE_LINK)))
        {
            if ((pMemberToBeUsed->LcpGSEM.pNegFlagsPerIf[PADDING_IDX].
                 FlagMask & ACKED_BY_LOCAL_MASK))
            {
                ASSIGN1BYTE (pOutData, Length, 1);
                Length += 1;
            }

            if ((Protocol & DATA_PKT_MASK) == 0)
            {
                if (((Protocol & DATA_PKT_MASK) == 0)
                    &&
                    ((pMemberToBeUsed->LcpGSEM.pNegFlagsPerIf[PFC_IDX].
                      FlagMask & ACKED_BY_LOCAL_MASK) != ACKED_BY_LOCAL_MASK))
                {
                    MOVE_BACK_PTR (pOutData, 1);
                    /* Prepend the PID with 0x00 */
                    APPEND1BYTE (pOutData, 0x00);
                }
                PPPLLITxPkt (pMemberToBeUsed, pOutData, Length, ENCAPSULATED);
            }
            else
            {
                PPPLLITxPkt (pMemberToBeUsed, pOutData, Length, Protocol);
            }
            return;
        }
#endif /* ifndef SINGLE_FRAG_WITH_MP_HDR */

        BEFlag = 0;

        /* Fragment the datagram based on the fragment length calculated */
/* if it is the last fragment then it need to be transmitted without fragmenting since it may contain extra bytes also */
        if (TotalFragCount != 1)
        {
            if (SPLIT_INTO_FRAGMENTS (pOutData, FragmentLength, &pFragData) ==
                0)
            {
                PPP_TRC1 (CONTROL_PLANE,
                          "No of Bytes in the CHOPPED Frag : %ld",
                          VALID_BYTES (pOutData));
            }
            else
            {
                PRINT_MEM_ALLOC_FAILURE;
                return;
            }
        }
        else
        {
            if (VALID_BYTES (pFragData) < MinMTU)
                pOutData = pFragData;
        }

        /* Set the Begin Flag only if it is not set already */
        if (BeginFlag == FIRST_FRAGMENT)
        {
            BEFlag |= BEGIN_SET;
            BeginFlag = MIDDLE_OR_END_FRAGMENT;
        }

        /* Set the End Flag if it is the last fragment */
        if (TotalFragCount == 1)
            BEFlag |= END_SET;

        /* Encapsulate and send the packet. */
        MPTxPkt (pBundleIf, pMemberToBeUsed, pOutData, BEFlag);

        pOutData = pFragData;
        TotalFragCount -= 1;
    }
    pBundleIf->MPInfo.BundleInfo.NullFragTimer.Param1 = PTR_TO_U4 (pBundleIf);
    PPPRestartTimer (&pBundleIf->MPInfo.BundleInfo.NullFragTimer,
                     NULL_FRAG_TIMER,
                     pBundleIf->MPInfo.BundleInfo.NullFragTimeOutVal);

    return;

#endif /* ~MP_ALGORITHM_INTERLEAVING */

#ifndef MP_ALGORITHM_INTERLEAVING
    while (RemLen != 0)
    {
        PPP_TRC1 (CONTROL_PLANE, "Length of Remaining pkt to be txmitted : %ld",
                  VALID_BYTES (pOutData));

        /* Find the next link to be enqueued. */
        pMemberToBeUsed = MPFindLinkToBeUsed (pBundleIf);
        if (pMemberToBeUsed == NULL)
        {
            OutReleaseFlag = PPP_YES;
            return;
        }                        /*OCT-2000 */

        /********** SKR003 ***************/
        /******* Addition: the following If condition has been added to create a room for DES Haeder and Max 8 bytes padding so as to make sure of MRU size. *******/
        MemberALMRUSize = pMemberToBeUsed->LcpOptionsAckedByLocal.MRU;
        if (pMemberToBeUsed->LcpStatus.ECPEnableFlag == SET)
        {
            MemberALMRUSize -= DESE_HDR_8_PAD;
        }
        PPP_TRC1 (CONTROL_PLANE, "Member Link selected : %ld",
                  pMemberToBeUsed->LinkInfo.IfIndex);

        /* Update the CumulativeDelay. It indicates the time ticks for
           successful transmission. 
         */
        if (pMemberToBeUsed->MPInfo.MemberInfo.CumulativeDelay == 0)
        {
            if (OsixGetSysTime (&u4CurTicks) == OSIX_FAILURE)
            {
                PPP_TRC (ALL_FAILURE, "Failure in  OsixGetSysTime()");
            }
            pMemberToBeUsed->MPInfo.MemberInfo.CumulativeDelay =
                u4CurTicks + pMemberToBeUsed->MPInfo.MemberInfo.TransitDelay;
           
        }
        else
        {
            pMemberToBeUsed->MPInfo.MemberInfo.CumulativeDelay +=
                pMemberToBeUsed->MPInfo.MemberInfo.TransitDelay;
        }

        PPP_TRC1 (CONTROL_PLANE, "After Updation Tc : %ld",
                  pMemberToBeUsed->MPInfo.MemberInfo.CumulativeDelay);

        /*
           We send a packet without the MP header if either sequencing is
           not required by the Control Protocol whose data packet is being
           sent or if this is the only link in the MP bundle.       
           In both cases the ifMTU (the Peer's MRU ) size of the link to 
           be used for transmission should be large enough to hold the full 
           datagram. We check for all these conditions. 
         */
        if ((pMemberToBeUsed->LcpGSEM.pNegFlagsPerIf[PADDING_IDX].
             FlagMask & ACKED_BY_LOCAL_MASK))
        {
            CompLen = Length + 1;
        }

#ifndef SINGLE_FRAG_WITH_MP_HDR
        if ((MemberALMRUSize >= CompLen)
            && ((pBundleIf->LcpGSEM.MiscParam.CharValue == NOT_MUST)
                || (SLL_COUNT (&pBundleIf->MPInfo.BundleInfo.MemberList) ==
                    SINGLE_LINK)))
        {
            if ((pMemberToBeUsed->LcpGSEM.pNegFlagsPerIf[PADDING_IDX].
                 FlagMask & ACKED_BY_LOCAL_MASK))
            {
                ASSIGN1BYTE (pOutData, Length, 1);
                Length += 1;
            }

            if ((Protocol & DATA_PKT_MASK) == 0)
            {
                /* PFC case handling */
                if (((Protocol & DATA_PKT_MASK) == 0)
                    &&
                    ((pMemberToBeUsed->LcpGSEM.pNegFlagsPerIf[PFC_IDX].
                      FlagMask & ACKED_BY_LOCAL_MASK) != ACKED_BY_LOCAL_MASK))
                {
                    /* PFC not enabled */
                    MOVE_BACK_PTR (pOutData, 1);
                    APPEND1BYTE (pOutData, 0x00);
                }
                PPPLLITxPkt (pMemberToBeUsed, pOutData, Length, ENCAPSULATED);
            }
            else
            {
                PPPLLITxPkt (pMemberToBeUsed, pOutData, Length, Protocol);
            }
            return;
        }
#endif /* ifndef SINGLE_FRAG_WITH_MP_HDR */

        BEFlag = 0;

        /*  
           Calculate the No. of bytes to be chopped from the datagram based
           on the MP Header format. 
           If the entire datagram can be sent in a single pkt, then there 
           is no need for splitting.
         */

        if ((pMemberToBeUsed->LcpGSEM.pNegFlagsPerIf[PADDING_IDX].
             FlagMask & ACKED_BY_LOCAL_MASK))
        {
            CompLen = RemLen + MPHeaderLen + 1;
        }
        else
        {
            CompLen = RemLen + MPHeaderLen;
        }
        if (CompLen <= MemberALMRUSize)
        {
            if ((pMemberToBeUsed->LcpGSEM.pNegFlagsPerIf[PADDING_IDX].
                 FlagMask & ACKED_BY_LOCAL_MASK))
            {
                ASSIGN1BYTE (pOutData, RemLen, 1);
            }

            /* Set the End Flag */
            BEFlag |= END_SET;
        }
        else
        {
            FragmentLength = MemberALMRUSize - MPHeaderLen;
            if ((pMemberToBeUsed->LcpGSEM.pNegFlagsPerIf[PADDING_IDX].
                 FlagMask & ACKED_BY_LOCAL_MASK))
            {
                UINT4               GetOffset;
                UINT1               LastByte;
                /* Fragment the datagram by extracting the ifMTU size of bytes  */
        /******** Extract the The last byte of first fragment and check whetther it falls with in the range of 1 to MPV. if so add one SDP byte at the end of first fragment *****/
                GetOffset = CB_READ_OFFSET (pOutData);
                GET1BYTE (pOutData, MemberALMRUSize - MPHeaderLen - 1,
                          LastByte);
                CB_READ_OFFSET (pOutData) = GetOffset;
                FragmentLength = MemberALMRUSize - MPHeaderLen;
                if (LastByte >= 1
                    && LastByte <=
                    pMemberToBeUsed->LcpOptionsAckedByLocal.MaxPadVal)
                {
                    FragmentLength -= 1;
                }
            }

            /* Fragment the datagram by extracting the ifMTU size of bytes  */
            if (SPLIT_INTO_FRAGMENTS (pOutData, FragmentLength, &pFragData) ==
                0)
            {
                PPP_TRC1 (CONTROL_PLANE,
                          "No of Bytes in the CHOPPED Frag : %ld",
                          VALID_BYTES (pOutData));
            }
            else
            {
                PRINT_MEM_ALLOC_FAILURE;
                return;
            }

            if (FragmentLength == MemberALMRUSize - MPHeaderLen - 1)
            {
                ASSIGN1BYTE (pOutData, FragmentLength, 1);
                RemLen++;
            }
            else
            {
                if (RemLen == MemberALMRUSize - MPHeaderLen)
                {
                    BEFlag |= END_SET;
                }
            }

        }
        /* Set the Begin Flag only if it is not set already */
        if (BeginFlag == FIRST_FRAGMENT)
        {
            BEFlag |= BEGIN_SET;
            BeginFlag = MIDDLE_OR_END_FRAGMENT;
        }

        /* Encapsulate and send the packet. */
        MPTxPkt (pBundleIf, pMemberToBeUsed, pOutData, BEFlag);

        /* Update the Remaining length of the Datagram */
        if (RemLen + MPHeaderLen > MemberALMRUSize)
        {
            RemLen = RemLen - (MemberALMRUSize - MPHeaderLen);
        }
        else
        {
            RemLen = 0;
        }
        pOutData = pFragData;
    }
    pBundleIf->MPInfo.BundleInfo.NullFragTimer.Param1 = (UINT4) pBundleIf;
    PPPRestartTimer (&pBundleIf->MPInfo.BundleInfo.NullFragTimer,
                     NULL_FRAG_TIMER,
                     pBundleIf->MPInfo.BundleInfo.NullFragTimeOutVal);
    return;

#endif /* MP_ALGORITHM_INTERLEAVING */


}

/***********************************************************************
*    Function Name    :    MPFindLinkToBeUsed
*    Description        :
*            This function is used to find the link over the which, the next 
*    fragment to be used.
*    Parameter(s)    :
*        pBundleIf    -    Points to the MP interface data structure.
*    Return Value    :    Pointer to the member link interface structure.
*********************************************************************/
tPPPIf             *
MPFindLinkToBeUsed (tPPPIf * pBundleIf)
{
    tPPPIf             *pTmpPtr, *pMemberToBeUsed;
    UINT4               CurrentTick = 0;
#ifdef MP_ALGORITHM_INTERLEAVING
    UINT2               Count;
    UINT2               Found;
#endif

    pMemberToBeUsed = NULL;
#ifdef MP_ALGORITHM_INTERLEAVING

    PPP_UNUSED (CurrentTick);
    Found = 0;
    Count = (UINT2) (SLL_COUNT (&pBundleIf->MPInfo.BundleInfo.MemberList));

    if ((SLL_COUNT (&pBundleIf->MPInfo.BundleInfo.MemberList) == SINGLE_LINK)
        || ((pBundleIf->MPInfo.BundleInfo.MPHeaderInitFlag == PPP_YES)
            && (pBundleIf->LcpGSEM.MiscParam.CharValue == MUST)))
    {
        SLL_SCAN_OFFSET (&pBundleIf->MPInfo.BundleInfo.MemberList, pTmpPtr,
                         tPPPIf *, FSAP_OFFSETOF (tPPPIf,
                                                MPInfo.MemberInfo.NextMPIf))
        {
            if (pMemberToBeUsed == NULL)
            {

                /* Make the first node in the list as the member link to start with. */
                pMemberToBeUsed = pTmpPtr;

                /*if the number of member links is one, then just set the flag
                   and return that link.
                   Else,
                   if the flag is enabled and sequence criteria is satisfied,
                   use the first link.
                 */
                if (SLL_COUNT (&pBundleIf->MPInfo.BundleInfo.MemberList) ==
                    SINGLE_LINK)
                {

                    pBundleIf->MPInfo.BundleInfo.MPHeaderInitFlag = PPP_YES;
                    LinkNum = (UINT2) (LinkNum + 1);
                    return (pMemberToBeUsed);
                }
                else
                {
                    if ((pBundleIf->MPInfo.BundleInfo.MPHeaderInitFlag ==
                         PPP_YES)
                        && (pBundleIf->LcpGSEM.MiscParam.CharValue == MUST))
                    {
                        pBundleIf->MPInfo.BundleInfo.MPHeaderInitFlag = PPP_NO;
                        pTmpPtr->LinkInfo.TempNoOfFrag += 1;
                        LinkNum = (UINT2) (LinkNum + 1);
                        return (pMemberToBeUsed);
                    }
                }
            }
        }                        /* End of SLL_SCAN */
    }                            /* End of if condition */

/* If Above conditions are not satisfied then we need to get the link to be used and we do it in a round robin fashion keeping into consideration the no. of pkts that can be sent on each link */
    while (Found != 1)
    {
        if (LinkNum <= Count)
        {
            SLL_SCAN_OFFSET (&pBundleIf->MPInfo.BundleInfo.MemberList, pTmpPtr,
                             tPPPIf *, FSAP_OFFSETOF (tPPPIf,
                                                    MPInfo.MemberInfo.NextMPIf))
            {
                if ((Found != 1) && (LinkNum == pTmpPtr->LinkInfo.LinkIndex))
                {
                    if (pTmpPtr->LinkInfo.TempNoOfFrag <=
                        pTmpPtr->LinkInfo.NoOfFrag)
                    {
                        pMemberToBeUsed = pTmpPtr;
                        LinkNum = (UINT2) (LinkNum + 1);
                        Found = 1;
                        pTmpPtr->LinkInfo.TempNoOfFrag += 1;
                    }
                    else
                    {
                        LinkNum = (UINT2) (LinkNum + 1);
                        continue;
                    }
                }
                else if (Found != 1)
                    continue;
                else
                    break;
            }                    /* End of SLL_SCAN */
        }
        else
            LinkNum = 1;
    }                            /* End of While loop */
    if (pMemberToBeUsed == NULL)
        pMemberToBeUsed =
            (tPPPIf *) SLL_FIRST (&pBundleIf->MPInfo.BundleInfo.MemberList);
    return (pMemberToBeUsed);
#endif /* MP_ALGORITHM_INTERLEAVING */

#ifndef MP_ALGORITHM_INTERLEAVING
    if (OsixGetSysTime (&CurrentTick) == OSIX_FAILURE)
    {
        PPP_TRC (ALL_FAILURE, "Failure in  OsixGetSysTime()");
    }
    /* Search the linked list of member links for the best link */
    SLL_SCAN_OFFSET (&pBundleIf->MPInfo.BundleInfo.MemberList, pTmpPtr,
                     tPPPIf *, FSAP_OFFSETOF (tPPPIf, MPInfo.MemberInfo.NextMPIf))
    {
        PPP_TRC2 (CONTROL_PLANE, "This node Tc : %ld Td : %ld",
                  pTmpPtr->MPInfo.MemberInfo.CumulativeDelay,
                  pTmpPtr->MPInfo.MemberInfo.TransitDelay);

        /*  
           Reset the CumulativeDelay ticks, if the current tick exceeds 
           CumualativeDelay. This means that the packet would 've been 
           transmitted.
         */
        if ((pTmpPtr->MPInfo.MemberInfo.CumulativeDelay != 0)
            && (pTmpPtr->MPInfo.MemberInfo.CumulativeDelay < CurrentTick))
        {
            pTmpPtr->MPInfo.MemberInfo.CumulativeDelay = 0;
        }

        if (pMemberToBeUsed == NULL)
        {

            /* Make the first node in the list as the member link to start with. */
            pMemberToBeUsed = pTmpPtr;

            /*  if the number of member links is one, then just set the flag
               and return that link.
               Else,
               if the flag is enabled and sequence criteria is satisfied,
               use the first link, irrespective of its Tc.
             */
            if (SLL_COUNT (&pBundleIf->MPInfo.BundleInfo.MemberList) ==
                SINGLE_LINK)
            {

                pBundleIf->MPInfo.BundleInfo.MPHeaderInitFlag = PPP_YES;
                return (pMemberToBeUsed);
            }
            else
            {
                if ((pBundleIf->MPInfo.BundleInfo.MPHeaderInitFlag == PPP_YES)
                    && (pBundleIf->LcpGSEM.MiscParam.CharValue == MUST))
                {
                    pBundleIf->MPInfo.BundleInfo.MPHeaderInitFlag = PPP_NO;
                    return (pMemberToBeUsed);
                }
            }
            continue;
        }

        /*
           If the CumulativeDelay of the link is zero, then it indicates 
           the link is free and the packet can be enqueued. But before doing 
           so, check the whether the link that has been selected as member 
           currently has also the CumulativeDelay as zero. If that is the 
           case, then TransitDelay is used for selecting the member link.
           Else, It means that the member link currently selected has some 
           ticks to expire before it can be fully available. Therefore, the 
           link with the CumalativeDelay as zero is selected as member link.
           else check if the CumulativeDelay of the link is less than that of
           currently selected member link then make the link being examined 
           as the member link.
         */
        if (pTmpPtr->MPInfo.MemberInfo.CumulativeDelay == 0)
        {
            if (pMemberToBeUsed->MPInfo.MemberInfo.CumulativeDelay == 0)
            {
                if (pTmpPtr->MPInfo.MemberInfo.TransitDelay <
                    pMemberToBeUsed->MPInfo.MemberInfo.TransitDelay)
                    pMemberToBeUsed = pTmpPtr;
            }
            else
            {
                pMemberToBeUsed = pTmpPtr;
            }
        }
        else
        {
            if (pMemberToBeUsed->MPInfo.MemberInfo.CumulativeDelay != 0)
            {
                if ((pTmpPtr->MPInfo.MemberInfo.CumulativeDelay - CurrentTick) <
                    (pMemberToBeUsed->MPInfo.MemberInfo.CumulativeDelay -
                     CurrentTick))
                {
                    pMemberToBeUsed = pTmpPtr;
                }
                else
                {
                    if (pTmpPtr->MPInfo.MemberInfo.CumulativeDelay ==
                        pMemberToBeUsed->MPInfo.MemberInfo.CumulativeDelay)
                    {
                        if (pTmpPtr->MPInfo.MemberInfo.TransitDelay <
                            pMemberToBeUsed->MPInfo.MemberInfo.TransitDelay)
                            pMemberToBeUsed = pTmpPtr;
                    }
                }
            }
        }
    }

    return (pMemberToBeUsed);
#endif /* MP_ALGORITHM_INTERLEAVING */

}

/***********************************************************************
*    Function Name    :    MPUpdateM 
*    Description        :
*        This function is used to calculate the CURRENT MINIMUM OF THE MOST 
*    RECENTLY RECEIVED fragments over all the members of the bundle
*    Parameter(s)    :
*        pBundleIf    -    Points to the Bundle Interface structure
*    Return Value    :    Value of M
*********************************************************************/
INT4
MPUpdateM (tPPPIf * pBundleIf)
{
    INT4                CurrMin;
    tMPFrag            *pTmpFrag;
    tPPPIf             *pMemberIf;

    /* Initializations */
    CurrMin = LEAST_VALUE;
    pTmpFrag = NULL;

    /* Find the M value */
    SLL_SCAN_OFFSET (&pBundleIf->MPInfo.BundleInfo.MemberList, pMemberIf,
                     tPPPIf *, FSAP_OFFSETOF (tPPPIf, MPInfo.MemberInfo.NextMPIf))
    {
        if (pMemberIf != NULL)
        {
            PPP_TRC2 (CONTROL_PLANE, "Comparing : %ld with %ld ", CurrMin,
                      pMemberIf->MPInfo.MemberInfo.LastSeqNumRcvd);
            if (SLL_COUNT (&pBundleIf->MPInfo.BundleInfo.RcvdBuffer))
            {
                PPP_TRC2 (CONTROL_PLANE, "LastM : %ld with %ld ",
                          pBundleIf->MPInfo.BundleInfo.LastM,
                          ((tMPFrag *)
                           SLL_FIRST (&pBundleIf->MPInfo.BundleInfo.
                                      RcvdBuffer))->SeqNum);
            }

            if (pBundleIf->MPInfo.BundleInfo.MPFlagsPerIf[SEQ_IDX].
                FlagMask & ACKED_BY_PEER_MASK)
            {
                /* Takes care of wraparound of "short" M value  */

                if ((pBundleIf->MPInfo.BundleInfo.LastM >= SHORT_MAX_INTEGER)
                    &&
                    ((SLL_COUNT (&pBundleIf->MPInfo.BundleInfo.RcvdBuffer) == 0)
                     ||
                     (((tMPFrag *)
                       SLL_FIRST (&pBundleIf->MPInfo.BundleInfo.RcvdBuffer))->
                      SeqNum >= SHORT_MAX_INTEGER)))
                {
                    pMemberIf->MPInfo.MemberInfo.LastSeqNumRcvd -=
                        SHORT_MAX_INTEGER;
                    pBundleIf->MPInfo.BundleInfo.LastSeqFwd -=
                        SHORT_MAX_INTEGER;
                    ResetSeqNumFlag = PPP_YES;
                }
            }
            else
            {
                /*  
                   Takes care of wraparound of "long" M value. Note that in this 
                   case also, only 24-bits out of the 32 bits of an UINT are being
                   used.  
                 */
                if ((pBundleIf->MPInfo.BundleInfo.LastM >= LONG_MAX_INTEGER)
                    &&
                    ((SLL_COUNT (&pBundleIf->MPInfo.BundleInfo.RcvdBuffer) == 0)
                     ||
                     (((tMPFrag *)
                       SLL_FIRST (&pBundleIf->MPInfo.BundleInfo.RcvdBuffer))->
                      SeqNum >= LONG_MAX_INTEGER)))
                {
                    pMemberIf->MPInfo.MemberInfo.LastSeqNumRcvd -=
                        LONG_MAX_INTEGER;
                    pBundleIf->MPInfo.BundleInfo.LastSeqFwd -= LONG_MAX_INTEGER;
                    ResetSeqNumFlag = PPP_YES;
                }
            }

            /* Check if M is being updated for the first link. */
            if (CurrMin == LEAST_VALUE)
            {

                /* Take the first link as referance. */
                CurrMin = pMemberIf->MPInfo.MemberInfo.LastSeqNumRcvd;
                continue;
            }

            /*  
               If the last sequence number received on this link is less than the
               value of Current minimum value, CurrMin takes this value. 
               ( Inverse Bubble ) 
             */
            if (pMemberIf->MPInfo.MemberInfo.LastSeqNumRcvd < CurrMin)
            {
                CurrMin = pMemberIf->MPInfo.MemberInfo.LastSeqNumRcvd;
            }
        }
    }

    /* If M changes, update LastMChange ticks counter with the current tick */
    if (CurrMin != pBundleIf->MPInfo.BundleInfo.LastM)
    {
        if (OsixGetSysTime (&pBundleIf->MPInfo.BundleInfo.LastMChangeTicks) ==
            OSIX_FAILURE)
        {
            PPP_TRC (ALL_FAILURE, "Failure in  OsixGetSysTime()");
        }
    }

    PPP_UNUSED (pTmpFrag);
    return (CurrMin);
}

/***********************************************************************
*    Function Name    :    MPAssembleAndForward
*    Description        :
*        This function is used to assemble and forward the received fragments
*    to the HLI. It uses the Begin1 and End fields of tPPPIf structure to
*    identify the boundries of the complete datagram.
*    Parameter(s)    :
*        pBundleIf    -    Points to the Bundle Interface structure.
*    Return Value    :    VOID
*********************************************************************/
VOID
MPAssembleAndForward (tPPPIf * pBundleIf)
{
    tMPFrag            *pTmpFrag, *pPrevPtr, *pScan;
    UINT1               EndFlag;
    UINT2               Protocol;
    UINT1               CharVal;

    INT1                Result;

    /* Initializations */
    pPrevPtr = NULL;
    EndFlag = FALSE;
    Protocol = 0;
    CharVal = 0;

    pBundleIf->MPInfo.BundleInfo.LastSeqFwd =
        pBundleIf->MPInfo.BundleInfo.EndSeqNum;

    /*  
       Can't use SLL_SCAN here because a node may be deleted within. 
       ( using SLL_DELETE ). So 'for' loop is used to scan the list of 
       unassembled fragments.
     */
    for (pTmpFrag =
         (tMPFrag *) SLL_FIRST (&pBundleIf->MPInfo.BundleInfo.RcvdBuffer),
         pScan =
         (tMPFrag *) SLL_NEXT (&pBundleIf->MPInfo.BundleInfo.RcvdBuffer,
                               (t_SLL_NODE *) pTmpFrag); pTmpFrag != NULL;
         pTmpFrag = pScan, pScan =
         (tMPFrag *) SLL_NEXT (&pBundleIf->MPInfo.BundleInfo.RcvdBuffer,
                               (t_SLL_NODE *) pTmpFrag))
    {

        if (pTmpFrag->SeqNum == pBundleIf->MPInfo.BundleInfo.Begin1SeqNum)
        {
            pPrevPtr = pTmpFrag;
            if (pTmpFrag->SeqNum == pBundleIf->MPInfo.BundleInfo.EndSeqNum)
            {
                PPP_TRC (CONTROL_PLANE,
                         "Pkt is contained in the same fragment");
                break;
            }
            pTmpFrag = pScan;
            continue;
        }

        if (pPrevPtr != NULL)
        {

            /* Concatenate the "in sequence" fragments */
            CRU_BUF_CONCAT_MSGS (pPrevPtr->pFragments, pTmpFrag->pFragments);
            if (pTmpFrag->SeqNum == pBundleIf->MPInfo.BundleInfo.EndSeqNum)
            {
                EndFlag = TRUE;
            }

            /*  
               Delete the node corresponding to the fragment that has been 
               added to the assebly buffer. 
             */
            SLL_DELETE (&pBundleIf->MPInfo.BundleInfo.RcvdBuffer,
                        (t_SLL_NODE *) pTmpFrag);
            FREE_MP_FRAGMENT (pTmpFrag);

            /*  
               If this is the last fragment of the packet, then no more 
               assembly needed. 
             */
            if (EndFlag == TRUE)
            {
                break;
            }
        }
    }

    if (pPrevPtr == NULL)
    {
        return;
    }

    {
        UINT1              *TempBuf;
        UINT2               u2Length;
        u2Length =
            (UINT2) CRU_BUF_Get_ChainValidByteCount (pPrevPtr->pFragments);
        TempBuf = MEM_MALLOC (u2Length, UINT1);
        CRU_BUF_Copy_FromBufChain (pPrevPtr->pFragments, TempBuf, 0, u2Length);
        CRU_BUF_Release_MsgBufChain (pPrevPtr->pFragments, FALSE);
        pPrevPtr->pFragments = PPPAllocateBuffer (u2Length, 0);
        CRU_BUF_Copy_OverBufChain (pPrevPtr->pFragments, TempBuf, 0, u2Length);
        FREE (TempBuf);

    }

    /* Check if the assembled packet is OK */
if (pPrevPtr->pFragments !=NULL)
{    
    if (PPPLLIGetProtField
        (pBundleIf, pPrevPtr->pFragments, &Protocol, 0,
         CALLED_FROM_MP) == DISCARD)
    {
        /* Unreachable Code */
        DISCARD_PKT (pBundleIf, "Invalid pkt. Discarded!!");
        /*Release the discarded buffer */
        SLL_DELETE (&pBundleIf->MPInfo.BundleInfo.RcvdBuffer,
                    (t_SLL_NODE *) pPrevPtr);
        MPDiscardFragments (pPrevPtr);
        return;
    }
}
/****** added for NCP Over Bundle *********/
    if (Protocol == LCP_PROTOCOL || Protocol == LQM_PROTOCOL
        || Protocol == CHAP_PROTOCOL || Protocol == PAP_PROTOCOL)
    {
        DISCARD_PKT (pBundleIf, "LCP packet received over bundle Discarded!!");
        /*Release the discarded buffer */
        SLL_DELETE (&pBundleIf->MPInfo.BundleInfo.RcvdBuffer,
                    (t_SLL_NODE *) pPrevPtr);
        MPDiscardFragments (pPrevPtr);
        return;
    }
    /* Send away to higher layers */

    Result = MPForwardRxDToHLI (pBundleIf, pPrevPtr->pFragments, Protocol);

    /*  
       The first node is no longer needed. Delete it from the unassembled 
       fragments buffer and free the memory.
     */
    SLL_DELETE (&pBundleIf->MPInfo.BundleInfo.RcvdBuffer,
                (t_SLL_NODE *) pPrevPtr);
    if (Result == FAILURE)
    {
        PPP_TRC (CONTROL_PLANE, "Packet with Invalid network PID/ size > MRRY");
        RELEASE_INC_BUFFER (pPrevPtr->pFragments);
    }
    FREE_MP_FRAGMENT (pPrevPtr);

    /* Reset all assembly variables */
    pBundleIf->MPInfo.BundleInfo.Begin1SeqNum = INIT_SEQ_VALUE;
    pBundleIf->MPInfo.BundleInfo.EndSeqNum = INIT_SEQ_VALUE;
    pBundleIf->MPInfo.BundleInfo.Begin2SeqNum = INIT_SEQ_VALUE;

    PPP_UNUSED (CharVal);
    return;
}

/***********************************************************************
*    Function Name    :    MPForwardRxDToHLI
*    Description        :
*                    This function is used to forward the received data to
*    the HL by triggering RXD event to appropriate NCP SEM.
*    Parameter(s)    :
*        pBundleIf    -    Points to the Bundle Interface structure.
*            pInPDU    -    Points to the incoming packet buffer.
*        Protocol    -    IP/IPX
*    Return Value    :    INT1 (Changed from 'VOID' for 98080004)
*                        SUCCESS - If pkt. is successfuly sent to HLI
*                        FAILURE - If pkt. is not sent to HLI
*********************************************************************/
INT1
MPForwardRxDToHLI (tPPPIf * pBundleIf, t_MSG_DESC * pInPDU, UINT2 Protocol)
{
    tPPPIf             *pIf;

    /* 
       Check if the assembled MP packet is within the negotiated MRRU bounds 
       or packet is LCP packet 
     */
    if (((VALID_BYTES (pInPDU) - pBundleIf->LinkInfo.RxProtLen -
          pBundleIf->LinkInfo.RxAddrCtrlLen) >
         pBundleIf->MPInfo.BundleInfo.BundleOptAckedByPeer.MRRU)
        || (Protocol == LCP_PROTOCOL) || (Protocol == LQM_PROTOCOL)
        || (Protocol == CHAP_PROTOCOL) || (Protocol == PAP_PROTOCOL))
    {
        DISCARD_PKT (pBundleIf,
                     "MRRU Size error or Invalid protocol in MP Pkt....");
        return FAILURE;
    }

    /* Check if NULL fragment has been received */
    if (VALID_BYTES (pInPDU) == 0)
    {
        PPP_TRC (CONTROL_PLANE,
                 "NULL Fragment is received. But not forwarded to the HLI");
        return FAILURE;
    }

    /* Check if a supported PID has been received */
    if ((PPPLLIProcessPID
         (pBundleIf, pInPDU, Protocol, 0, CALLED_FROM_MP,
          (UINT2) (VALID_BYTES (pInPDU)))) == NOT_PROCESSED)
    {

        /* 
           If the Protocol is not supported, we find the appropriate link on which
           the packet was received and send a protocol reject over that link.
           Check This - Shenoy
         */

        /* THIS SHOULD BE CHANGED TO SLL_FIRST AS ABOVE. DUE TO SOME PROBLEM,
           THE FOLLOWING CODE IS RETAINED 
         */

        pIf =
            (tPPPIf
             *) (TMO_SLL_First ((&pBundleIf->MPInfo.BundleInfo.MemberList)));
        if (pIf != NULL)
        {
            pBundleIf->LLHCounters.InUnknownProtos++;
            if (PPPSendRejPkt
                (pIf, pInPDU, (UINT2) (VALID_BYTES (pInPDU)), LCP_PROTOCOL,
                 PROT_REJ_CODE) == FALSE)
            {
                PPP_TRC (EVENT_TRC, "Unable to send protocol reject..");
            }
        }

        return FAILURE;
    }

    return SUCCESS;
}

/***********************************************************************
*    Function Name    :    MPAddFragToList
*    Description        :
*            This function is used to add the received fragment to the linked
*    list buffer. It also checks for the completeness of a packet . In addition
*    it updates the Begin1, End and Begin2 fields of tBundleInfo structure.
*    Parameter(s)    :
*        pBundleIf    -    Points to the Bundle Interface structure.
*            pInData    -    Points to the incoming data.    
*                Len    -    Length of the incoming fragment.
*            SeqNum    -     Sequence number of the received packet.
*            BEFlag    -    The Begin-End bitmask as retrieved from the MP header.
*                M    -    The current M value.
*    Return Value    :    VOID
*********************************************************************/
/* Makefile changes - signed and unsigned number comparison */
VOID
MPAddFragToList (tPPPIf * pBundleIf, t_MSG_DESC * pInData, UINT2 Len,
                 INT4 SeqNum, UINT1 BEFlag, INT4 M)
{
    tMPFrag            *pTmpFrag,
        *pNewFrag, *pScanPtr, *pPrevPtr, *pBegin2Ptr, *pEndPtr;
    UINT1               InsertedFlag, CharVal, ContinueFlag, HoldFlag;
    INT4                ExpectedSeqNum;
    UINT2               Protocol;

    /* Makefile changes - unused */
    PPP_TRC4 (CONTROL_PLANE, "Len=%d, SeqNum=%ld, BEFlag=%x, M=%ld", Len,
              SeqNum, BEFlag, M);

    /* Initializations */
    pTmpFrag = NULL;
    pNewFrag = NULL;
    pScanPtr = NULL;
    pPrevPtr = NULL;
    pBegin2Ptr = NULL;
    pEndPtr = NULL;
    InsertedFlag = PPP_NO;
    CharVal = 0;
    ContinueFlag = 0;
    HoldFlag = PPP_NO;
    ExpectedSeqNum = INIT_SEQ_VALUE;
    Protocol = 0;

    pBundleIf->MPInfo.BundleInfo.Begin2SeqNum = INIT_SEQ_VALUE;
    pBundleIf->MPInfo.BundleInfo.Begin1SeqNum = INIT_SEQ_VALUE;
    pBundleIf->MPInfo.BundleInfo.EndSeqNum = INIT_SEQ_VALUE;

    /*  
       Scan the list and insert the new node in the appropriate place.
       Also update the Begin1SeqNum, EndSeqNum and Begin2SeqNum variables.
       In addition, check the completeness of a datagram and forward the
       same to the HL, by invoking Assemble() function

       If the list is empty and the fragment just received is  complete,
       meaning both Begin and End bit set, then it can be directly
       forwarded to the HLI. This is taken care of by the arguments of the
       "for" loop.
     */
    for (pTmpFrag =
         (tMPFrag *) SLL_FIRST (&pBundleIf->MPInfo.BundleInfo.RcvdBuffer),
         pScanPtr =
         (tMPFrag *) SLL_NEXT (&pBundleIf->MPInfo.BundleInfo.RcvdBuffer,
                               (t_SLL_NODE *) pTmpFrag); pTmpFrag != NULL;
         pTmpFrag = pScanPtr, pScanPtr =
         (tMPFrag *) SLL_NEXT (&pBundleIf->MPInfo.BundleInfo.RcvdBuffer,
                               (t_SLL_NODE *) pTmpFrag))
    {

        /* For wraparound case */
        if (ResetSeqNumFlag == PPP_YES)
        {
            if (pBundleIf->MPInfo.BundleInfo.MPFlagsPerIf[SEQ_IDX].
                FlagMask & ACKED_BY_PEER_MASK)
            {
                pTmpFrag->SeqNum -= SHORT_MAX_INTEGER;
            }
            else
            {
                pTmpFrag->SeqNum -= LONG_MAX_INTEGER;
            }
        }

        ContinueFlag = PPP_NO;

        if (InsertedFlag == PPP_NO)
        {

            /* 
               Insert the node at the right place in the list of received
               fragments. Insertion is in ascending order based on the 
               sequence number of the fragment.
             */
            if (pTmpFrag->SeqNum > SeqNum)
            {

                /*  
                   Construct a new node for the incoming fragment and 
                   fill the fields.
                 */
                if ((pNewFrag =
                     MPCreateNewFragment (pBundleIf, SeqNum, BEFlag,
                                          pInData)) == NULL)
                {
                    PPP_TRC (CONTROL_PLANE, "Not able to alloc..");
                    return;
                };

                SLL_INSERT (&pBundleIf->MPInfo.BundleInfo.RcvdBuffer,
                            (t_SLL_NODE *) pPrevPtr, (t_SLL_NODE *) pNewFrag);
                pTmpFrag = pNewFrag;
                InsertedFlag = PPP_YES;
                pScanPtr =
                    (tMPFrag *) SLL_NEXT (&pBundleIf->MPInfo.BundleInfo.
                                          RcvdBuffer, (t_SLL_NODE *) pTmpFrag);
            }
            else
            {

                /* 
                   This one to take care of the arrival of the same fragment 
                   for the second time. 
                 */
                if (pTmpFrag->SeqNum == SeqNum)
                {
                    InsertedFlag = PPP_YES;
                }
                else
                {
                    pPrevPtr = pTmpFrag;
                }
            }
        }

        if (pTmpFrag->BEFlag & END_SET)
        {
            if ((pBundleIf->MPInfo.BundleInfo.EndSeqNum == INIT_SEQ_VALUE)
                || (pBundleIf->MPInfo.BundleInfo.EndSeqNum > pTmpFrag->SeqNum))
            {
                pBundleIf->MPInfo.BundleInfo.EndSeqNum = pTmpFrag->SeqNum;
                pEndPtr = pTmpFrag;
            }
        }

        /* Updation of Begin1, End and Begin2 Variables ... */
        if (pTmpFrag->BEFlag & BEGIN_SET)
        {
            if (pTmpFrag ==
                (tMPFrag *) SLL_FIRST (&pBundleIf->MPInfo.BundleInfo.
                                       RcvdBuffer))
            {
                pBundleIf->MPInfo.BundleInfo.Begin1SeqNum = pTmpFrag->SeqNum;
                if ((pBundleIf->MPInfo.BundleInfo.LastSeqFwd ==
                     (INT4) (pTmpFrag->SeqNum - 1))
                    ||
                    ((pBundleIf->MPInfo.BundleInfo.LastSeqFwd !=
                      pTmpFrag->SeqNum - 1)
                     && (M > pBundleIf->MPInfo.BundleInfo.LastSeqFwd)))
                {
                    if (pTmpFrag->SeqNum ==
                        pBundleIf->MPInfo.BundleInfo.EndSeqNum)
                    {
                        ExpectedSeqNum = pTmpFrag->SeqNum;
                    }
                    else
                    {
                        ExpectedSeqNum = pTmpFrag->SeqNum + 1;
                        ContinueFlag = PPP_YES;
                    }
                }
            }
            else
            {
                if ((pBundleIf->MPInfo.BundleInfo.Begin2SeqNum ==
                     INIT_SEQ_VALUE)
                    || (pBundleIf->MPInfo.BundleInfo.Begin2SeqNum >
                        pTmpFrag->SeqNum))
                {
                    pBegin2Ptr = pTmpFrag;
                    pBundleIf->MPInfo.BundleInfo.Begin2SeqNum =
                        pTmpFrag->SeqNum;
                }
            }
        }

        if (ContinueFlag == PPP_NO)
        {
            /* Checking for the completeness of a datagram */
            if (ExpectedSeqNum != INIT_SEQ_VALUE)
            {
                if (pTmpFrag->SeqNum != ExpectedSeqNum)
                {
                    ExpectedSeqNum = 0;
                }
                else
                {
                    if (pTmpFrag->BEFlag & END_SET)
                    {
                        MPAssembleAndForward (pBundleIf);
                        ExpectedSeqNum = 0;
                        pPrevPtr = NULL;
                    }
                    else
                    {
                        ExpectedSeqNum++;
                    }
                }
            }
        }
        /* To detect Fragment Loss and flushing out the assembled Frags */
        /* Middle or First Fragment Loss */

        if ((M >= pBundleIf->MPInfo.BundleInfo.EndSeqNum)
            && (pBundleIf->MPInfo.BundleInfo.EndSeqNum != INIT_SEQ_VALUE))
        {
            PPP_TRC (EVENT_TRC, " MP Middle or first fragment is lost");
            if ((pEndPtr != NULL) && (pEndPtr->BEFlag & BEGIN_SET))
            {
                if (pEndPtr !=
                    (tMPFrag *) SLL_FIRST (&pBundleIf->MPInfo.BundleInfo.
                                           RcvdBuffer))
                {
                    pBundleIf->MPInfo.BundleInfo.NoOfFragDiscarded +=
                        SLL_DELETE_TILL_NODE (&pBundleIf->MPInfo.BundleInfo.
                                              RcvdBuffer,
                                              SLL_FIRST (&pBundleIf->MPInfo.
                                                         BundleInfo.RcvdBuffer),
                                              SLL_PREV (&pBundleIf->MPInfo.
                                                        BundleInfo.RcvdBuffer,
                                                        (t_SLL_NODE *) pEndPtr),
                                              (void (*)(t_SLL_NODE *))
                                              MPDiscardFragments);
                }
                pBundleIf->MPInfo.BundleInfo.Begin1SeqNum = pEndPtr->SeqNum;
                MPAssembleAndForward (pBundleIf);
            }
            else
            {
                SLL_DELETE_TILL_NODE (&pBundleIf->MPInfo.BundleInfo.RcvdBuffer,
                                      SLL_FIRST (&pBundleIf->MPInfo.BundleInfo.
                                                 RcvdBuffer),
                                      (t_SLL_NODE *) pEndPtr,
                                      (void (*)(t_SLL_NODE *))
                                      MPDiscardFragments);
            }
            pBundleIf->MPInfo.BundleInfo.Begin2SeqNum = INIT_SEQ_VALUE;
            pBundleIf->MPInfo.BundleInfo.EndSeqNum = INIT_SEQ_VALUE;
            pEndPtr = NULL;
            pBegin2Ptr = NULL;
            pPrevPtr = NULL;
        }

        /* End fragment or both Begin and End fragment lost */
        if ((M >= pBundleIf->MPInfo.BundleInfo.Begin2SeqNum)
            && (pBundleIf->MPInfo.BundleInfo.Begin2SeqNum != INIT_SEQ_VALUE))
        {
            PPP_TRC (EVENT_TRC, " MP end fragment is lost\n");
            pBundleIf->MPInfo.BundleInfo.NoOfFragDiscarded +=
                SLL_DELETE_TILL_NODE (&pBundleIf->MPInfo.BundleInfo.RcvdBuffer,
                                      SLL_FIRST (&pBundleIf->MPInfo.BundleInfo.
                                                 RcvdBuffer),
                                      SLL_PREV (&pBundleIf->MPInfo.BundleInfo.
                                                RcvdBuffer,
                                                (t_SLL_NODE *) pBegin2Ptr),
                                      (void (*)(t_SLL_NODE *))
                                      MPDiscardFragments);
            pBundleIf->MPInfo.BundleInfo.Begin2SeqNum = INIT_SEQ_VALUE;
            pBundleIf->MPInfo.BundleInfo.Begin1SeqNum = INIT_SEQ_VALUE;
            pScanPtr = pBegin2Ptr;
        }
    }                            /* SLL_SCAN */
    if (InsertedFlag == PPP_NO)
    {
        if ((BEFlag & BEGIN_SET) && (BEFlag & END_SET))
        {
            if ((pBundleIf->MPInfo.BundleInfo.LastSeqFwd == (INT4) (SeqNum - 1))
                || ((pBundleIf->MPInfo.BundleInfo.LastSeqFwd != SeqNum - 1)
                    && (M <= pBundleIf->MPInfo.BundleInfo.LastSeqFwd)))
            {

                pBundleIf->MPInfo.BundleInfo.LastSeqFwd = SeqNum;

                if (VALID_BYTES (pInData) == 0)
                {
                    PPP_TRC (CONTROL_PLANE,
                             " NULL Fragment is received. But not forwarded to the HLI");
                    return;
                }
                if (PPPLLIGetProtField
                    (pBundleIf, pInData, &Protocol, 0,
                     CALLED_FROM_MP) == DISCARD)
                {
                    DISCARD_PKT (pBundleIf, "Invalid pkt. Discarded!!");
                    return;
                }
                MPForwardRxDToHLI (pBundleIf, pInData, Protocol);

                return;
            }

        }

        if ((pNewFrag =
             MPCreateNewFragment (pBundleIf, SeqNum, BEFlag, pInData)) == NULL)
        {
            PPP_TRC (ALL_FAILURE, " Not able to alloc..");
            return;
        }

        if ((SLL_COUNT (&pBundleIf->MPInfo.BundleInfo.RcvdBuffer) == 0))
        {
            if (OsixGetSysTime
                (&pBundleIf->MPInfo.BundleInfo.ReassembleStartTicks) ==
                OSIX_FAILURE)
            {
                PPP_TRC (ALL_FAILURE, "Failure in  OsixGetSysTime()\n");
            }
            pBundleIf->MPInfo.BundleInfo.LastMChangeTicks =
                pBundleIf->MPInfo.BundleInfo.ReassembleStartTicks;

            pBundleIf->MPInfo.BundleInfo.ReassemblyTimer.Param1 =
                PTR_TO_U4 (pBundleIf);
            PPPRestartTimer (&pBundleIf->MPInfo.BundleInfo.ReassemblyTimer,
                             REASSEMBLY_TIMER,
                             pBundleIf->MPInfo.BundleInfo.ReassemblyTimeOutVal);
        }

        SLL_ADD (&pBundleIf->MPInfo.BundleInfo.RcvdBuffer,
                 (t_SLL_NODE *) pNewFrag);

        if ((pNewFrag->BEFlag & END_SET)
            && (pBundleIf->MPInfo.BundleInfo.EndSeqNum == INIT_SEQ_VALUE))
        {
            pBundleIf->MPInfo.BundleInfo.EndSeqNum = pNewFrag->SeqNum;
            if ((ExpectedSeqNum != 0) && (pNewFrag->SeqNum == ExpectedSeqNum))
            {
                MPAssembleAndForward (pBundleIf);
            }
        }
    }

    PPP_UNUSED (CharVal);
    PPP_UNUSED (HoldFlag);
    return;
}

/***********************************************************************
*    Function Name    :    MPUpdateBundleNCPInfo
*    Description        :
*        This function is used to update the NCP information of a bundle.
*    It is called by the NCP action procedures, when one of the member link
*    successfully negotiates the NCP. The function copies the NCP Interfcae
*    structure into the bundle's appropriate Interface pointer if the NCP
*    has been negotiated for the first time on any of the links of the bundle.
*    Parameter(s)    :
*        pBundleIf    -    Points to the Bundle interface structure.
*            pIf        -    Points to the Member link interface structure.
*        Protocol    -    Indicates the NCP that has reached the OPENED State.
*    Return Value    :    VOID
*********************************************************************/
VOID
MPUpdateBundleNCPInfo (tPPPIf * pBundleIf, tPPPIf * pIf, UINT2 Protocol)
{
    tIPCPIf            *pIpcpIf = NULL;
    tMPLSCPIf          *pMplscpIf = NULL;
    tMPLSCPIf          *pBundleMplscpIf = NULL;
    tIPCPIf            *pBundleIpcpIf = NULL;
#ifdef IPv6CP
    tIP6CPIf           *pIp6cpIf = NULL;
    tIP6CPIf           *pBundleIp6cpIf = NULL;
#endif
    tMuxCPIf           *pMuxcpIf = NULL;
    tMuxCPIf           *pBundleMuxcpIf = NULL;
    tIPXCPIf           *pIpxcpIf = NULL;
    tIPXCPIf           *pBundleIpxcpIf = NULL;
#ifdef BCP
    tBCPIf             *pBcpIf = NULL;
    tBCPIf             *pBundleBcpIf = NULL;
#endif

    UNUSED_PARAM (pMplscpIf);
    UNUSED_PARAM (pBundleMplscpIf);
    UNUSED_PARAM (pMuxcpIf);
    UNUSED_PARAM (pBundleMuxcpIf);
    UNUSED_PARAM (pIpxcpIf);
    UNUSED_PARAM (pBundleIpxcpIf);
    if (Protocol == IPCP_PROTOCOL)
    {
        if (pBundleIf->pIpcpPtr == NULL)
        {
            if ((pBundleIf->pIpcpPtr =
                 (tIPCPIf *) (VOID *) ALLOC_IPCP_IF ()) == NULL)
            {
                PRINT_MEM_ALLOC_FAILURE;
                return;
            }
            pIpcpIf = (tIPCPIf *) pIf->pIpcpPtr;
            pBundleIpcpIf = (tIPCPIf *) pBundleIf->pIpcpPtr;
            GSEMCleanUp (&pIpcpIf->IpcpGSEM);
            MEMCPY (pBundleIpcpIf, pIpcpIf, sizeof (tIPCPIf));
            pBundleIpcpIf->IpcpGSEM.CurrentState = OPENED;
            pBundleIpcpIf->IpcpGSEM.pIf = pBundleIf;
        }
    }

#ifdef IPv6CP
    if (Protocol == IPV6CP_PROTOCOL)
    {
        if (pBundleIf->pIp6cpPtr == NULL)
        {
            if ((pBundleIf->pIp6cpPtr = (tIP6CPIf *) ALLOC_IP6CP_IF ()) == NULL)
            {
                PRINT_MEM_ALLOC_FAILURE;
                return;
            }
            pIp6cpIf = (tIP6CPIf *) pIf->pIp6cpPtr;
            pBundleIp6cpIf = (tIP6CPIf *) pBundleIf->pIp6cpPtr;
            GSEMCleanUp (&pIp6cpIf->Ip6cpGSEM);
            MEMCPY (pBundleIp6cpIf, pIp6cpIf, sizeof (tIP6CPIf));
            pBundleIp6cpIf->Ip6cpGSEM.CurrentState = OPENED;
            pBundleIp6cpIf->Ip6cpGSEM.pIf = pBundleIf;
        }
    }
#endif
#ifdef MUXCP
    if (Protocol == MUXCP_PROTOCOL)
    {
        if (pBundleIf->pMuxcpPtr == NULL)
        {
            if ((pBundleIf->pMuxcpPtr = (tMuxCPIf *) ALLOC_MUXCP_IF ()) == NULL)
            {
                PRINT_MEM_ALLOC_FAILURE;
                return;
            }
            pMuxcpIf = (tMuxCPIf *) pIf->pMuxcpPtr;
            pBundleMuxcpIf = (tMuxCPIf *) pBundleIf->pMuxcpPtr;
            GSEMCleanUp (&pMuxcpIf->MuxCPGSEM);
            MEMCPY (pBundleMuxcpIf, pMuxcpIf, sizeof (tMuxCPIf));
            pBundleMuxcpIf->MuxCPGSEM.CurrentState = OPENED;
            pBundleMuxcpIf->MuxCPGSEM.pIf = pBundleIf;
        }
    }
#endif
#ifdef MPLSCP
    if (Protocol == MPLSCP_PROTOCOL)
    {
        if (pBundleIf->pMplscpPtr == NULL)
        {
            if ((pBundleIf->pMplscpPtr =
                 (tMPLSCPIf *) ALLOC_MPLSCP_IF ()) == NULL)
            {
                PRINT_MEM_ALLOC_FAILURE;
                return;
            }
            pMplscpIf = (tMPLSCPIf *) pIf->pMplscpPtr;
            pBundleMplscpIf = (tMPLSCPIf *) pBundleIf->pMplscpPtr;
            GSEMCleanUp (&pMplscpIf->MplscpGSEM);
            MEMCPY (pBundleMplscpIf, pMplscpIf, sizeof (tMPLSCPIf));
            pBundleMplscpIf->MplscpGSEM.CurrentState = OPENED;
            pBundleMplscpIf->MplscpGSEM.pIf = pBundleIf;
        }
    }
#endif
#ifdef IPXCP
    if (Protocol == IPXCP_PROTOCOL)
    {
        if (pBundleIf->pIpxcpPtr == NULL)
        {
            if ((pBundleIf->pIpxcpPtr = (tIPXCPIf *) ALLOC_IPXCP_IF ()) == NULL)
            {
                PRINT_MEM_ALLOC_FAILURE;
                return;
            }
            pIpxcpIf = (tIPXCPIf *) pIf->pIpxcpPtr;
            pBundleIpxcpIf = (tIPXCPIf *) pBundleIf->pIpxcpPtr;
            GSEMCleanUp (&pIpxcpIf->IpxcpGSEM);
            MEMCPY (pBundleIpxcpIf, pIpxcpIf, sizeof (tIPXCPIf));
            pBundleIpxcpIf->IpxcpGSEM.CurrentState = OPENED;
            pBundleIpxcpIf->IpxcpGSEM.pIf = pBundleIf;
        }
    }
#endif
#ifdef BCP
    if (Protocol == BCP_PROTOCOL)
    {
        if (pBundleIf->pBcpPtr == NULL)
        {
            if ((pBundleIf->pBcpPtr = (tBCPIf *) ALLOC_BCP_IF ()) == NULL)
            {
                PRINT_MEM_ALLOC_FAILURE;
                return;
            }
            pBcpIf = (tBCPIf *) pIf->pBcpPtr;
            pBundleBcpIf = (tBCPIf *) pBundleIf->pBcpPtr;
            GSEMCleanUp (&pBcpIf->BcpGSEM);
            MEMCPY (pBundleBcpIf, pBcpIf, sizeof (tBCPIf));
            pBundleBcpIf->BcpGSEM.CurrentState = OPENED;
            pBundleBcpIf->BcpGSEM.pIf = pBundleIf;
        }
    }
#endif
    return;
}

/***********************************************************************
*    Function Name    :    MPBundleInit 
*    Description        :
*            This function initializes the MP Bundle Info and is called by 
*    the MPCreate() function.
*    Parameter(s)    :
*        pBundleIf    -    Points to the tPPPIf structure corressponding to the
*                        bundle to be initialized.
*    Return Value    :    OK/NOT_OK
*********************************************************************/
INT1
MPBundleInit (tPPPIf * pBundleIf)
{

    pBundleIf->BundleFlag = BUNDLE;

    MPOptionInit (&pBundleIf->MPInfo.BundleInfo.BundleOptDesired);
    MPOptionInit (&pBundleIf->MPInfo.BundleInfo.BundleOptAllowedForPeer);
    MPOptionInit (&pBundleIf->MPInfo.BundleInfo.BundleOptAckedByPeer);
    MPOptionInit (&pBundleIf->MPInfo.BundleInfo.BundleOptAckedByLocal);

    SLL_INIT (&pBundleIf->MPInfo.BundleInfo.MemberList);
    SLL_INIT (&pBundleIf->MPInfo.BundleInfo.RcvdBuffer);

    pBundleIf->MPInfo.BundleInfo.Begin1SeqNum = INIT_SEQ_VALUE;
    pBundleIf->MPInfo.BundleInfo.EndSeqNum = INIT_SEQ_VALUE;
    pBundleIf->MPInfo.BundleInfo.Begin2SeqNum = INIT_SEQ_VALUE;
    pBundleIf->MPInfo.BundleInfo.ReassemblyTimeOutVal = DEF_ASSEMBLE_TIME_OUT;
    pBundleIf->MPInfo.BundleInfo.NullFragTimeOutVal = DEF_NULL_FRAG_TIME_OUT;
    pBundleIf->MPInfo.BundleInfo.PeriodicEchoTimeOut =
        DEF_PERIODIC_ECHO_TIME_OUT;
    pBundleIf->MPInfo.BundleInfo.TxSeqNum = 0;
    pBundleIf->MPInfo.BundleInfo.LastSeqFwd = INIT_SEQ_VALUE;
    pBundleIf->MPInfo.BundleInfo.LastM = INIT_SEQ_VALUE;
    pBundleIf->MPInfo.BundleInfo.NoOfFragRcvd = 0;
    pBundleIf->MPInfo.BundleInfo.NoOfFragDiscarded = 0;
    pBundleIf->MPInfo.BundleInfo.RespRecd = PPP_NO;
    pBundleIf->MPInfo.BundleInfo.RespSent = PPP_NO;
    pBundleIf->LinkInfo.TimeOutTime = DEF_TIMEOUT_TIME;
    pBundleIf->LinkInfo.MaxNumLoops = DEF_MAX_MAGIC_NAK_LOOPS;
    pBundleIf->LinkInfo.MaxReTransmits = DEF_MAX_RETXMISSION;

    pBundleIf->CurrentLinkConfigs.RestartTimeOutVal = DEF_RESTART_TIMEOUT;
    pBundleIf->CurrentLinkConfigs.MaxConfigReqTransmits = DEF_MAX_CONF_REQ;
    pBundleIf->CurrentLinkConfigs.MaxNakLoops = DEF_MAXNAK_LOOPS;
    pBundleIf->CurrentLinkConfigs.MaxTermtransmits = DEF_MAX_TERM_REQ;
    pBundleIf->CurrentLinkConfigs.PassiveOption = NOT_SET;
    pBundleIf->LinkConfigs.RestartTimeOutVal = DEF_RESTART_TIMEOUT;
    pBundleIf->LinkConfigs.MaxConfigReqTransmits = DEF_MAX_CONF_REQ;
    pBundleIf->LinkConfigs.MaxNakLoops = DEF_MAXNAK_LOOPS;
    pBundleIf->LinkConfigs.MaxTermtransmits = DEF_MAX_TERM_REQ;
    pBundleIf->LinkConfigs.PassiveOption = NOT_SET;

    pBundleIf->LcpStatus.AuthStatus = NO_AUTH_PROTOCOL;
    pBundleIf->LcpStatus.AdminStatus = STATUS_DOWN;
    pBundleIf->LcpStatus.OperStatus = STATUS_DOWN;
    pBundleIf->LcpStatus.LinkQualityStatus = QUALITY_NOT_DETERMINED;
    pBundleIf->LcpStatus.LinkAvailability = LINK_NOT_AVAILABLE;
    pBundleIf->LcpGSEM.Protocol = LCP_PROTOCOL;
    pBundleIf->LcpGSEM.pCallbacks = &LcpGSEMCallback;
    pBundleIf->LcpGSEM.pIf = pBundleIf;
    pBundleIf->LcpGSEM.MaxOptTypes = MAX_BUNDLE_OPTS;
    if (GSEMInit (&pBundleIf->LcpGSEM, MAX_LCP_OPT_TYPES) == NOT_OK)
    {
        return (NOT_OK);
    }

    BZERO (&pBundleIf->LLHCounters, sizeof (tLLHCounters));

    MEMCPY (pBundleIf->MPInfo.BundleInfo.MPFlagsPerIf, &LCPOptPerIntf,
            sizeof (tOptNegFlagsPerIf) * MAX_BUNDLE_OPTS);

#ifdef BAP
    BZERO (&pBundleIf->MPInfo.BundleInfo.BWMProtocolInfo,
           sizeof (tBWMProtocolInfo));
#endif

    PPP_INIT_TIMER (pBundleIf->MPInfo.BundleInfo.NullFragTimer);
    PPP_INIT_TIMER (pBundleIf->MPInfo.BundleInfo.ReassemblyTimer);

/* Initalize Link Related Timers 
 * This is to ensure that the timers are stopped properly when the interface is
 * deleted
 */
    PPP_INIT_TIMER (pBundleIf->LinkInfo.IdleTimer);
    PPP_INIT_TIMER (pBundleIf->CallBackTimeInfo.CallBackDisconnectTimer);
    PPP_INIT_TIMER (pBundleIf->CallBackTimeInfo.CallBackDialTimer);
    PPP_INIT_TIMER (pBundleIf->EchoInfo.EchoTimer);

    PPP_INIT_TIMER (pBundleIf->RecdTimer);

    return (OK);
}

/***********************************************************************
*    Function Name    :    MPOptionInit
*    Description        :
*            This function initializes the MP options.
*    Parameter(s)    :
*        pBundleOpt    -    Points to the tPPPIf structure corressponding to the
*                        bundle whose options are to be initialized.
*    Return Value    :    VOID
*********************************************************************/
VOID
MPOptionInit (tBundleOptions * pBundleOpt)
{

    pBundleOpt->MRRU = DEF_MRU_SIZE;
    BZERO (&pBundleOpt->BundleId, sizeof (tBundleId));
    return;
}

/***********************************************************************
*    Function Name    :    MPDiscardFragments
*    Description        :
*            This function is used to discard the fragments till 'SeqNum'.
*    Parameter(s)    :
*        pTmpFrag    -    Points to the MP Fragment.
*    Return Value    :    VOID
*********************************************************************/
VOID
MPDiscardFragments (tMPFrag * pTmpFrag)
{

    RELEASE_INC_BUFFER (pTmpFrag->pFragments);
    FREE_MP_FRAGMENT (pTmpFrag);
    return;
}

/***********************************************************************
*    Function Name    :    MPTxPkt
*    Description        :
*            This function is used to encapsulate the Fragment to be sent
*    over the member link. It prepends the MP header and sends the fragment 
*    to the lower layers for transmission across the link. 
*    Parameter(s)    :
*        pBundleIf    -    Points to the Bundle interface structure.
*        pMemberIf    -    Points to the member link i/f structure.
*        pOutData     -    Points to the output buffer.
*        BEFlag        -    Points to the Begin/End flag.
*    Return Value    :    VOID
*********************************************************************/
VOID
MPTxPkt (tPPPIf * pBundleIf, tPPPIf * pMemberIf, t_MSG_DESC * pOutData,
         UINT1 BEFlag)
{
    UINT4               LongSeqNum;
    UINT2               ShortSeqNum;

    /* Initializations */
    LongSeqNum = 0;
    ShortSeqNum = 0;

    /* Check if the short sequence number format has been acked by us. */
    if (pBundleIf->MPInfo.BundleInfo.MPFlagsPerIf[SEQ_IDX].
        FlagMask & ACKED_BY_LOCAL_MASK)
    {

        /* Fill the "short" header */
        if (pBundleIf->MPInfo.BundleInfo.TxSeqNum == 0x1000)
        {
            pBundleIf->MPInfo.BundleInfo.TxSeqNum = 0;
        }
        ShortSeqNum =
            (UINT2) (ShortSeqNum |
                     (pBundleIf->MPInfo.BundleInfo.TxSeqNum & 0x0fff));
        if (BEFlag & BEGIN_SET)
        {
            ShortSeqNum |= 0x8000;
        }
        if (BEFlag & END_SET)
        {
            ShortSeqNum |= 0x4000;
        }

        /* Appened the header */
        MOVE_BACK_PTR (pOutData, 2);
        ASSIGN2BYTE (pOutData, 0, ShortSeqNum);
    }
    else
    {

        /* Fill the "long" header */
        if (pBundleIf->MPInfo.BundleInfo.TxSeqNum == 0x01000000)
        {
            pBundleIf->MPInfo.BundleInfo.TxSeqNum = 0;
        }
        LongSeqNum |= pBundleIf->MPInfo.BundleInfo.TxSeqNum & 0x00ffffff;
        if (BEFlag & BEGIN_SET)
        {
            LongSeqNum |= 0x80000000;
        }
        if (BEFlag & END_SET)
        {
            LongSeqNum |= 0x40000000;
        }

        /* Appened the header */
        MOVE_BACK_PTR (pOutData, 4);
        ASSIGN4BYTE (pOutData, 0, LongSeqNum);
    }

    /* Increment the sequence number for the next fragment */
    pBundleIf->MPInfo.BundleInfo.TxSeqNum++;

    /* Send away the fragment to the LLI */
    PPPLLITxPkt (pMemberIf, pOutData, (UINT2) (VALID_BYTES (pOutData)),
                 MP_DATAGRAM);
    return;
}

/*********************************************************************
*    Function Name    :    MPCreateNewFragment
*    Description        :
*            This function is used to create and initialize a new fragment 
*    structure to store the incoming fragment.
*    Parameter(s)    :
*            SeqNum    -    Sequence number of the fragment.
*            BEFlag    -    Begin/End flag of the fragment.
*            pInData    -    Pointer to the input buffer.
*    Return Values    :    Points to the newly created structure. 
*********************************************************************/
tMPFrag            *
MPCreateNewFragment (tPPPIf * pBundleIf, INT4 SeqNum, UINT1 BEFlag,
                     t_MSG_DESC * pInData)
{
    tMPFrag            *pNewFrag;

    pNewFrag = NULL;

    if ((pNewFrag = (tMPFrag *) (VOID *) ALLOC_MP_FRAGMENT ()) == NULL)
    {
        MPDeleteTillLastUnassembledPkt (pBundleIf);
        if ((pNewFrag = (tMPFrag *) (VOID *) ALLOC_MP_FRAGMENT ()) == NULL)
        {
            PRINT_MEM_ALLOC_FAILURE;
            return (NULL);
        }
    }

    /* Initialize the newly created fragment */
    pNewFrag->SeqNum = SeqNum;
    pNewFrag->BEFlag = BEFlag;
    pNewFrag->pFragments = pInData;
    ReleaseFlag = PPP_NO;
    SLL_INIT_NODE (&pNewFrag->NextFragPtr);

    return (pNewFrag);
}
