/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppmpif.c,v 1.7 2014/12/16 11:47:25 siva Exp $
 *
 * Description:This file contains interface routines  of PPP module.
 *
 *******************************************************************/

#include <stdlib.h>
#include  "pppcom.h"
#include  "mpcom.h"
#include  "gsemdefs.h"
#include  "gcpdefs.h"
#include  "lcpexts.h"
#include  "mpexts.h"

#include  "pppchap.h"
#include  "ppppap.h"
#include  "pppeap.h"
#include  "pppauth.h"
#include "pppdpproto.h"

#ifdef BAP
#include  "pppbacp.h"
#include  "pppbap.h"
#endif

#include "pppipcp.h"
#include "pppipv6cp.h"
#include "pppmuxcp.h"
#include "pppmplscp.h"
#include "mplsproto.h"
#include "pppipxcp.h"
#include "ipxproto.h"
#include "pppeccmn.h"
#include "pppccp.h"
#include "pppeccmn.h"
#include "pppecp.h"
#include "pppbcp.h"
#include  "cfa.h"
#include  "ppp.h"
#include "genexts.h"
#include "ipv6proto.h"
#include "muxcpproto.h"
#include "bcpproto.h"
#include "ccpproto.h"
#include "ecpproto.h"

#include "globexts.h"
#include "ipproto.h"
#include "genproto.h"

#define SHORT_SEQ_FORMAT_LEN      2
#define LONG_SEQ_FORMAT_LEN       4
#define    ADD_TO_BUNDLE            1
#define    CREATE_NEW_BUNDLE        2
#define    MIN_MRRU_SIZE            1500

tOptHandlerFuns     MRRUHandlingFuns = {
    MPProcessMRRUConfReq,
    MPProcessMRRUConfNak,
    NULL,
    MPReturnMRRUPtr,
    NULL
};

tOptHandlerFuns     SeqOptHandlingFuns = {
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
};

tOptHandlerFuns     EPDHandlingFuns = {
    MPProcessEPDConfReq,
    NULL,
    NULL,
    MPReturnEPDPtr,
    MPAddEPDSubOpt
};

/***********************************************************************
*    Function Name    :    MPInput
*    Description        :
*            This function is used to process the fragments received over the 
*    bundle and is called from the PPPLLIRxPkt() function, if the link over
*    which the pkt was received is part of a bundle.
*    Parameter(s)    :
*                pIf    -    Points to the tPPPIf structure corresponding to the 
*                        member link of the bundle on which the datagram was
*                        received.    
*            pInData    -    Points to the buffer containing the incoming data
*                Len    -    Length of the data
*    MPHeaderFlag    -    Indicates if the packet has an MP header or did not
*                        have an MP header.    
*    Return Value    :    VOID
*********************************************************************/
VOID
MPInput (tPPPIf * pIf, t_MSG_DESC * pInData, UINT2 Len, UINT1 MPHeaderFlag)
{
    UINT4               CILong;
    INT4                M;
    UINT2               CIShort;
    UINT2               MPHeaderLen, Protocol;
    UINT1               BEFlag, CharVal;
    tPPPIf             *pBundleIf;

    /* Initializations */
    CILong = 0;
    MPHeaderLen = LONG_SEQ_FORMAT_LEN;
    Protocol = 0;
    BEFlag = 0;

    /* Check if bundle has been negotiated. */
    if ((pBundleIf = pIf->MPInfo.MemberInfo.pBundlePtr) == NULL)
    {
        DISCARD_PKT (pIf, "Bundle not negotiated...\n");
        return;
    }

    if ((Len < SHORT_SEQ_FORMAT_LEN)
        ||
        (!(pBundleIf->
           MPInfo.BundleInfo.MPFlagsPerIf[SEQ_IDX].
           FlagMask & ACKED_BY_PEER_MASK) && (Len < LONG_SEQ_FORMAT_LEN)))
    {
        DISCARD_PKT (pIf, "Invalid Length Fragment....\n");
        return;
    }

    /**** added by sunil for NCP over Bundle ***/
    pBundleIf->LcpOptionsAckedByPeer.MRU = pIf->LcpOptionsAckedByPeer.MRU;

/*********** added by sunil for Padding *****. After receiving the fragment it will check for the last byte of the fragment. If it falls in between 1 to MPV unpad it using self describing padding **********/

    if ((pIf->LcpGSEM.pNegFlagsPerIf[PADDING_IDX].
         FlagMask & ACKED_BY_PEER_MASK))
    {
        UINT1               PaddedBytes;
        UINT4               GetOffset;
        GetOffset = CB_READ_OFFSET (pInData);
        GET1BYTE (pInData, VALID_BYTES (pInData) - 1, PaddedBytes);
        if (PaddedBytes >= 1
            && PaddedBytes <= pIf->LcpOptionsAckedByPeer.MaxPadVal)
        {
            UINT2               PadLen;
            UINT1               CurPadByte, PadByte;
            PadLen = (UINT2) (VALID_BYTES (pInData) - 1);
            PadByte = PaddedBytes;
            for (; PadByte;)
            {
                GET1BYTE (pInData, PadLen, CurPadByte);
                if (CurPadByte != PadByte--)
                {
                    DISCARD_PKT (pIf, "Padding Error...");
                    return;
                }
                PadLen--;

            }
            CRU_BUF_Delete_BufChainAtEnd (pInData, PaddedBytes);

        }
        CB_READ_OFFSET (pInData) = GetOffset;
    }

    /* Following can be combined with the above - Shenoy */
    /* Discard the pkt if the MP Options are not successfully negotiated. */
    if ((pBundleIf->MPInfo.BundleInfo.MPFlagsPerIf[MRRU_IDX].
         FlagMask & ACKED_BY_PEER_MASK) == NOT_SET)
    {
        DISCARD_PKT (pIf, "Bundle not negotiated...\n");
        return;
    }

    /* Check if the packets are without the MP Header */
    if (MPHeaderFlag == WITHOUT_MP_HEADER)
    {
        if (pIf->LinkInfo.RxProtLen == COMPRESSED_PID_LEN)
        {
            GET1BYTE (pInData, pIf->LinkInfo.RxAddrCtrlLen, CharVal);
            Protocol = CharVal;
        }
        else
        {
            GET2BYTE (pInData, pIf->LinkInfo.RxAddrCtrlLen, Protocol);
        }
        pBundleIf->LinkInfo.RxAddrCtrlLen = pIf->LinkInfo.RxAddrCtrlLen;
        pBundleIf->LinkInfo.RxProtLen = pIf->LinkInfo.RxProtLen;

        MPForwardRxDToHLI (pBundleIf, pInData, Protocol);
        return;
    }
    /* Extract the SeqNumber and BE Flag ,Based on the options negotiated */
    if (pBundleIf->MPInfo.BundleInfo.MPFlagsPerIf[SEQ_IDX].
        FlagMask & ACKED_BY_PEER_MASK)
    {
        MPHeaderLen = SHORT_SEQ_FORMAT_LEN;

        EXTRACT2BYTE (pInData, CIShort);
        if (CIShort & SHORT_BEGIN_SET)
        {
            BEFlag |= BEGIN_SET;
        }
        if (CIShort & SHORT_END_SET)
        {
            BEFlag |= END_SET;
        }

        if (CIShort & 0x3000)
        {
            DISCARD_PKT (pIf, "Reserved bits not equal to zero...");
            return;
        }

        CIShort &= 0x0FFF;

        if (pIf->MPInfo.MemberInfo.LastSeqNumRcvd > CIShort)
        {
            if ((pIf->MPInfo.MemberInfo.LastSeqNumRcvd - CIShort) >
                (SHORT_MAX_INTEGER - 100))
            {
                /* Wrapped Around... */
                pIf->MPInfo.MemberInfo.LastSeqNumRcvd =
                    CIShort + SHORT_MAX_INTEGER;
            }
            else
            {
                DISCARD_PKT (pIf, "Sequence Reordering error...");
                return;
            }
        }
        else
        {
            if (pIf->MPInfo.MemberInfo.LastSeqNumRcvd == CIShort)
            {
                DISCARD_PKT (pIf, "Sequence Reordering error...");
                return;
            }
            else
            {
                pIf->MPInfo.MemberInfo.LastSeqNumRcvd = CIShort;
            }
        }
    }
    else
    {
        /* Extract the SeqNumber and BE Flag */

        EXTRACT4BYTE (pInData, CILong);
        if (CILong & LONG_BEGIN_SET)
        {
            BEFlag |= BEGIN_SET;
        }
        if (CILong & LONG_END_SET)
        {
            BEFlag |= END_SET;
        }

        if (CILong & 0x3F000000)
        {
            DISCARD_PKT (pIf, "Reserved bits not equal to zero...");
            return;
        }

        CILong &= 0x00FFFFFF;

        if (pIf->MPInfo.MemberInfo.LastSeqNumRcvd > (INT4) CILong)
        {
            if ((pIf->MPInfo.MemberInfo.LastSeqNumRcvd - (INT4) CILong) >
                (LONG_MAX_INTEGER - 100))
            {
                /* Wrapped Around... */
                pIf->MPInfo.MemberInfo.LastSeqNumRcvd =
                   (INT4) CILong + LONG_MAX_INTEGER;
            }
            else
            {
                DISCARD_PKT (pIf, "Sequence Reordering error...");
                return;
            }
        }
        else
        {
            if (pIf->MPInfo.MemberInfo.LastSeqNumRcvd == (INT4) CILong)
            {
                DISCARD_PKT (pIf, "Sequence Reordering error...");
                return;
            }
            else
            {
                pIf->MPInfo.MemberInfo.LastSeqNumRcvd = (INT4) CILong;
            }
        }
    }

    /* Calculate the value of M */
    M = MPUpdateM (pBundleIf);

    MOVE_OFFSET (pInData,
                 pIf->LinkInfo.RxAddrCtrlLen + pIf->LinkInfo.RxProtLen +
                 MPHeaderLen);

    pBundleIf->LinkInfo.RxAddrCtrlLen = 0;
    pBundleIf->LinkInfo.RxProtLen = 0;

    pBundleIf->MPInfo.BundleInfo.NoOfFragRcvd++;

    MPAddFragToList (pBundleIf, pInData, Len,
                     pIf->MPInfo.MemberInfo.LastSeqNumRcvd, BEFlag, M);

    if (ResetSeqNumFlag == PPP_YES)
    {
        ResetSeqNumFlag = PPP_NO;
    }
    pBundleIf->MPInfo.BundleInfo.LastM = M;

    if ((pBundleIf->MPInfo.BundleInfo.NoOfFragRcvd >
         pBundleIf->MPInfo.BundleInfo.NoOfFragDiscarded)
        &&
        (((pBundleIf->
           MPInfo.BundleInfo.NoOfFragDiscarded /
           pBundleIf->MPInfo.BundleInfo.NoOfFragRcvd) * 100) >
         MAX_DISC_ALLOWED))
    {
    }

    return;
}

/***********************************************************************
*    Function Name    :    MPCreate 
*    Description        :
*                This function is used to create a bundle entry
*    Parameter(s)    : 
*            pIfId    -    Pointer to the Identifier of the  Interface to be 
*                        created.
*            IfIndex    -    Indicates the entry of this PPP interface  in the 
*                        IfTable and used in communicating.
*    Return Value    :
*            Pointer to the newly created interface entry, if the creation is 
*    successful / NULL  if it  fails to create the interface. 
*********************************************************************/
tPPPIf             *
MPCreate (tPPPIfId * pIfID, UINT2 IfIndex)
{
    tPPPIf             *pBundlePtr;

    /* Allocate memory for a new interface structure   */
    if ((pBundlePtr = (tPPPIf *) (VOID *) ALLOC_PPP_IF ()) != NULL)
    {
        if (MPBundleInit (pBundlePtr) != NOT_OK)
        {
            pBundlePtr->LinkInfo.IfIndex = IfIndex;
            MEMCPY (&pBundlePtr->LinkInfo.IfID, pIfID, sizeof (tPPPIfId));
            SLL_ADD (&PPPIfList, (t_SLL_NODE *) pBundlePtr);
#ifdef DPIF
            if (PppCreateMpInterfaceToDP (pBundlePtr) == PPP_FAILURE)
            {
                return PPP_FAILURE;
            }
#endif
            return (pBundlePtr);
        }
    }
    PPP_TRC (ALL_FAILURE, "Error: Unable to Alloc/Init PPPIf entry .");
    return (NULL);
}

/***********************************************************************
*    Function Name    :    MPDelete 
*    Description        :
*                This function is used to delele a bundle entry.
*    Parameter(s)    :
*        pBundleIf    -    Points to the bundle interface structure.
*    Return Value    :    VOID
*********************************************************************/
VOID
MPDelete (tPPPIf * pBundleIf)
{
    tPPPIf             *pMemberIf;
    tPPPIf             *pNextMemberIf;
    t_SLL              *pMemberList;

    /* Stop the timers, if they are running ... */
    PPPStopTimer (&pBundleIf->MPInfo.BundleInfo.ReassemblyTimer);
    PPPStopTimer (&pBundleIf->MPInfo.BundleInfo.NullFragTimer);

#ifdef BAP
    if ((pBundleIf->MPInfo.BundleInfo.BWMProtocolInfo.Protocol == BAP_PROTOCOL)
        && (pBundleIf->MPInfo.BundleInfo.BWMProtocolInfo.pProtocolInfo != NULL))
    {
        BAPDelete (pBundleIf);
    }
#endif

    /* Delete each link of the bundle before deleting the bundle itself. */

/* SLL_SCAN_OFFSET replaced with a 'while' loop */

    pMemberList = &pBundleIf->MPInfo.BundleInfo.MemberList;

    pMemberIf = (tPPPIf *) SLL_FIRST (pMemberList);

    while (pMemberIf != NULL)
    {
        pNextMemberIf =
            (tPPPIf *) SLL_NEXT (pMemberList, (t_SLL_NODE *) pMemberIf);
        /* Makefile changes - passing incompatible pointer type as arg */
        MPDeleteLinkFromBundle (pBundleIf,
                                (tPPPIf *) (VOID *) (((UINT1 *) pMemberIf) -
                                                     FSAP_OFFSETOF (tPPPIf,
                                                                  MPInfo.
                                                                  MemberInfo.
                                                                  NextMPIf)));
        pMemberIf = pNextMemberIf;
    }

    /* All links deleted. Now safe to delete the bundle itself */
    SLL_DELETE (&PPPIfList, (t_SLL_NODE *) pBundleIf);
    FREE_PPP_IF (pBundleIf);
    return;
}

/***********************************************************************
*    Function Name    :    MPAddLinktoBundle 
*    Description        : 
*            This function is used to add a link to an existing bundle. This
*    function is the interface function to the SNMP low-level routines.
*   This function takes care of pointing all NCP pointers to NCP pointers of a 
*   bundel. So there will be a global pointer for all NCP pointers    
*    Parameter(s)    :
*        pBundleIf    -    Points to the MP Bundle i/f structure. 
*        pMemberIf    -    Points to the MP link i/f structure. 
*    Return Value    :    VOID
*********************************************************************/
VOID
MPAddLinkToBundle (tPPPIf * pBundleIf, tPPPIf * pMemberIf)
{
#ifdef BAP
    tBAPInfo           *pBAPInfo;
#endif

    /* Updating the member link info. */
    pMemberIf->MPInfo.MemberInfo.pBundlePtr = pBundleIf;
    MEMCPY (pMemberIf->LcpGSEM.pNegFlagsPerIf,
            pBundleIf->MPInfo.BundleInfo.MPFlagsPerIf,
            (MAX_BUNDLE_OPTS * sizeof (tOptNegFlagsPerIf)));
    pMemberIf->LcpGSEM.pNegFlagsPerIf[MRRU_IDX].FlagMask |= DESIRED_SET;
    pMemberIf->LcpGSEM.pNegFlagsPerIf[MRRU_IDX].FlagMask |= ACKED_BY_PEER_SET;

    if ((pMemberIf->pIpcpPtr != pBundleIf->pIpcpPtr) &&
        (pMemberIf->pIpcpPtr != NULL))
    {
        IPCPDeleteIf (pMemberIf->pIpcpPtr);
        pMemberIf->pIpcpPtr = NULL;
    }
    pMemberIf->pIpcpPtr = pBundleIf->pIpcpPtr;

#ifdef IPv6CP
    if ((pMemberIf->pIp6cpPtr != pBundleIf->pIp6cpPtr) &&
        (pMemberIf->pIp6cpPtr != NULL))
    {
        IP6CPDeleteIf (pMemberIf->pIp6cpPtr);
    }
    pMemberIf->pIp6cpPtr = pBundleIf->pIp6cpPtr;
#endif

    if ((pMemberIf->pMuxcpPtr != pBundleIf->pMuxcpPtr) &&
        (pMemberIf->pMuxcpPtr != NULL))
    {
        MuxCPDeleteIf (pMemberIf->pMuxcpPtr);
    }
    pMemberIf->pMuxcpPtr = pBundleIf->pMuxcpPtr;

    if ((pMemberIf->pMplscpPtr != pBundleIf->pMplscpPtr) &&
        (pMemberIf->pMplscpPtr != NULL))
    {
        MPLSCPDeleteIf (pMemberIf->pMplscpPtr);
    }
    pMemberIf->pMplscpPtr = pBundleIf->pMplscpPtr;

#ifdef REVISIT
    if ((pMemberIf->pIpxcpPtr != pBundleIf->pIpxcpPtr) &&
        (pMemberIf->pIpxcpPtr != NULL))
    {
        IPXCPDeleteIf (pMemberIf->pIpxcpPtr);
    }
#endif
    pMemberIf->pIpxcpPtr = pBundleIf->pIpxcpPtr;

    if ((pMemberIf->pBcpPtr != pBundleIf->pBcpPtr) &&
        (pMemberIf->pBcpPtr != NULL))
    {
#ifdef BCP
        BCPDeleteIf (pMemberIf->pBcpPtr);
#endif
        pMemberIf->pBcpPtr = NULL;
    }
    pMemberIf->pBcpPtr = pBundleIf->pBcpPtr;
    if (pMemberIf->pCCPIf != NULL)
    {
#ifdef CCP
        CCPDeleteIf (pMemberIf->pCCPIf);
#endif
    }

    if (pBundleIf->pCCPIf != NULL)
    {
        pMemberIf->pCCPIf = pBundleIf->pCCPIf;
    }

    if (pMemberIf->pECPIf != NULL)
    {
        ECPDeleteIf (pMemberIf->pECPIf);

    }

    if (pBundleIf->pECPIf != NULL)
    {
        pMemberIf->pECPIf = pBundleIf->pECPIf;
    }

#ifdef BAP
    pMemberIf->LcpGSEM.pNegFlagsPerIf[LINK_DISCR_IDX].FlagMask = 0;
    if (pBundleIf->MPInfo.BundleInfo.BWMProtocolInfo.Protocol == BAP_PROTOCOL)
    {
        pMemberIf->LcpGSEM.pNegFlagsPerIf[LINK_DISCR_IDX].FlagMask |=
            (DESIRED_SET | ALLOWED_FOR_PEER_SET | FORCED_TO_REQUEST_SET);
        pBAPInfo =
            (tBAPInfo *) pBundleIf->MPInfo.BundleInfo.BWMProtocolInfo.
            pProtocolInfo;
        if ((pBAPInfo->LocFirstLnkPhNum.Size == 0)
            || (pBAPInfo->RemFirstLnkPhNum.Size == 0))
        {
            MPGetCallInfoFromRM (pMemberIf,
                                 &pBAPInfo->pBundleIf->MPInfo.BundleInfo.
                                 IsInitiator, &pBAPInfo->LocFirstLnkPhNum,
                                 &pBAPInfo->RemFirstLnkPhNum);
        }
    }
#endif

    /* First remove any secret entries already existing for the member link */
    MpRemoveSecretsFromMember (pMemberIf);
    /* Now copy all secret entries in the bundle to the newly added member */
    MpAddAllSecretsToNewMember (pBundleIf, pMemberIf);

#if PPP0
    /* To have the orderly state transition  */
    GSEMRestartIfNeeded (&pMemberIf->LcpGSEM);
#endif

    return;
}

/*******************************************************************************
*  Function Name : MPSuspendBundle
*  Description   :
*                This function is called to suspend the bundle from functioning.
*  It closes all the member links of the bundle without detaching
*  from the bundle. 
*  Parameter(s)  :  
*        pIf    -  points to the MP bundle interface structure tobe suspended
*  Return Value  : 
*******************************************************************************/

VOID
MPSuspendBundle (tPPPIf * pIf)
{
    tPPPIf             *pMemberIf;
    tIPCPIf            *pIpcpIf;
#ifdef IPv6CP
    tIP6CPIf           *pIp6cpIf;
#endif
    tMuxCPIf           *pMuxcpIf;
    tMPLSCPIf          *pMplscpIf;
    tIPXCPIf           *pIpxcpIf;
    tCCPIf             *pCCPIf;
    tECPIf             *pECPIf;
#ifdef BAP
    tBACPIf            *pBacpIf;
    tBAPInfo           *pBAPInfo;
#endif

    if (pIf->BundleFlag == BUNDLE)
    {
        pIf->LcpStatus.AdminStatus = ADMIN_CLOSE;
        SLL_SCAN (&PPPIfList, pMemberIf, tPPPIf *)
        {
            if (pMemberIf->MPInfo.MemberInfo.pBundlePtr == pIf)
            {
                /* pMemberIf belongs to this Bundle */
                MPDeleteMemberFromBundle (pIf, pMemberIf);
                GSEMRun (&(pMemberIf->LcpGSEM), CLOSE);
            }
        }

        pIpcpIf = (tIPCPIf *) pIf->pIpcpPtr;
        if (pIpcpIf != NULL)
        {
            pIpcpIf->OperStatus = STATUS_DOWN;
        }
#ifdef IPv6CP
        pIp6cpIf = (tIP6CPIf *) pIf->pIp6cpPtr;
        if (pIp6cpIf != NULL)
        {
            pIp6cpIf->u1OperStatus = STATUS_DOWN;
        }
#endif
        pMuxcpIf = (tMuxCPIf *) pIf->pMuxcpPtr;
        if (pMuxcpIf != NULL)
        {
            pMuxcpIf->OperStatus = STATUS_DOWN;
        }
        pMplscpIf = (tMPLSCPIf *) pIf->pMplscpPtr;
        if (pMplscpIf != NULL)
        {
            pMplscpIf->OperStatus = STATUS_DOWN;
        }

        pIpxcpIf = (tIPXCPIf *) pIf->pIpxcpPtr;
        if (pIpxcpIf != NULL)
        {
            pIpxcpIf->OperStatus = STATUS_DOWN;
        }
        pCCPIf = (tCCPIf *) pIf->pCCPIf;
        if (pCCPIf != NULL)
        {
            pCCPIf->OperStatus = STATUS_DOWN;
        }
        pECPIf = (tECPIf *) pIf->pECPIf;
        if (pECPIf != NULL)
        {
            pECPIf->OperStatus = STATUS_DOWN;
        }
#ifdef BAP
        if (pIf->MPInfo.BundleInfo.BWMProtocolInfo.Protocol == BAP_PROTOCOL)
        {
            pBAPInfo =
                (tBAPInfo *) pIf->MPInfo.BundleInfo.BWMProtocolInfo.
                pProtocolInfo;
            pBacpIf = &pBAPInfo->BACPIf;
            pBacpIf->OperStatus = STATUS_DOWN;
        }
#endif
        LCPTriggerEventToNCP (pIf, PPP_DOWN, LINK_NOT_AVAILABLE);
    }
    else
    {
        PPP_TRC (CONTROL_PLANE, "Suspening a non-bundle is not allowed");
    }
    return;
}

/***********************************************************************
*    Function Name    :    MPDeleteLinkFromBundle
*    Description        :
*            This function is used to delete the member link from the existing
*    MP Bundle. This function is the interface function to the SNMP low-level 
*    routines.
*    Parameter(s)    :
*        pBundleIf    -    Points to the MP Bundle i/f structure. 
*        pMemberIf    -    Points to the MP link i/f structure. 
*    Return Value    :    VOID
*********************************************************************/
VOID
MPDeleteLinkFromBundle (tPPPIf * pBundleIf, tPPPIf * pMemberIf)
{
    tCCPIf             *pCCPIf;
    tECPIf             *pECPIf;
    tCfaIfInfo          IfInfo;

    /* Assumption: The member link would have been admin disabled before
     * explicitly deleting it from the bundle.
     * So the member link would have been operationally removed from the
     * bundle at that time itself */

    if (pMemberIf->MPInfo.MemberInfo.PartOfBundle == PPP_YES)
    {
        MPDeleteMemberFromBundle (pBundleIf, pMemberIf);
    }

    /* Normal operation:
     * Before a PPP link is removed from the bundle 
     *  a) it is administratively disabled or 
     *  b) it receives a terminate request from the peer
     * In both  cases, in DP the PPP link is destroyed from the bundle,
     * when Terminate Ack is received from peer in response.
     * 
     * Special case:
     * But 
     * a) If PPP link is removed from bundle by configuration
     *    before the Ack is received
     * b) If the Bundle is deleted (where this function is called for all
     *                              member links)
     * the normal scenario may not happen. Hence we destroy in DP here as well.
     */
    if (pMemberIf->u1RemovedLinkInDP == PPP_FALSE)
    {
        pMemberIf->u1RemovedLinkInDP = PPP_TRUE;
#ifdef DPIF
        MPRemoveLinkFromBundleToDP (pMemberIf, pBundleIf);
#endif
    }

    MpRemoveSecretsFromMember (pMemberIf);

    pMemberIf->pIpcpPtr = NULL;
#ifdef IPv6CP
    pMemberIf->pIp6cpPtr = NULL;
#endif
    pMemberIf->pMuxcpPtr = NULL;
    pMemberIf->pIpxcpPtr = NULL;
    pMemberIf->pBcpPtr = NULL;

    if (pMemberIf->MPInfo.MemberInfo.pBundlePtr->pCCPIf != NULL)
    {
        pMemberIf->pCCPIf = NULL;
    }
    else
    {
        pCCPIf = (tCCPIf *) pMemberIf->pCCPIf;
        if (pCCPIf != NULL)
        {
            GSEMRun (&pCCPIf->CCPGSEM, PPP_DOWN);
        }
    }
    if (pMemberIf->MPInfo.MemberInfo.pBundlePtr->pECPIf != NULL)
    {
        pMemberIf->pECPIf = NULL;
    }
    else
    {
        pECPIf = (tECPIf *) pMemberIf->pECPIf;
        if (pECPIf != NULL)
        {
            GSEMRun (&pECPIf->ECPGSEM, PPP_DOWN);
        }
    }
    if (pMemberIf->LcpGSEM.pNegFlagsPerIf != NULL)
    {
        BZERO (pMemberIf->LcpGSEM.pNegFlagsPerIf,
               sizeof (tOptNegFlagsPerIf) * MAX_BUNDLE_OPTS);
    }

    /* Updating the member link info. */
    pMemberIf->MPInfo.MemberInfo.pBundlePtr = NULL;

    /* Start IPCP on the individual PPP link */
    CfaGetIfInfo (pMemberIf->LinkInfo.IfIndex, &IfInfo);
    if (IfInfo.u1BridgedIface == CFA_DISABLED)
    {
        PppCreateAndEnableIPCPIf (pMemberIf, 0, 0);
    }
    return;
}

/***********************************************************************
*    Function Name    :    MPInsertMemberIntoBundle
*    Description        :
*        This function is used to update the member information in a bundle.
*    It adds the link to the member list of the bundle and calculates the 
*    transit delay for that link by invoking LCPSendEchoReq() Function.
*    Parameter(s)    :
*            pIf        -    Points to the member link interface structure.
*        pBundleIf    -    Points to the Bundle link interface structure.
*    Return Value    :    VOID
*********************************************************************/
VOID
MPInsertMemberIntoBundle (tPPPIf * pBundleIf, tPPPIf * pIf)
{
#ifdef BAP
    tPPPIf             *pMemberIf;
#endif

    /* Check if this is the first link to be added to the bundle */
    if (SLL_COUNT (&pBundleIf->MPInfo.BundleInfo.MemberList) == 0)
    {
        pBundleIf->MPInfo.BundleInfo.MPHeaderInitFlag = PPP_NO;

        /* Copy link's MP parameters into bundle's Parameters */
        MEMCPY (pBundleIf->MPInfo.BundleInfo.MPFlagsPerIf,
                pIf->LcpGSEM.pNegFlagsPerIf,
                (MAX_BUNDLE_OPTS * sizeof (tOptNegFlagsPerIf)));
        PrintBundleFlags (pBundleIf);
    }
    else
    {

        /*
           Add to the bundle only if the Links MP Options match the Bundles MP 
           Options. 
         */
        PrintBundleFlags (pBundleIf);
        PrintFlags (&pIf->LcpGSEM);

        if (MEMCMP
            (pBundleIf->MPInfo.BundleInfo.MPFlagsPerIf,
             pIf->LcpGSEM.pNegFlagsPerIf,
             (MAX_BUNDLE_OPTS * sizeof (tOptNegFlagsPerIf))) != 0)
        {

            /* If the options do not match, terminate the SEM */
            PPP_TRC (CONTROL_PLANE,
                     " Negotiated Options do not match bundle options. Terminating SEM.");
            pTermGSEM = &pIf->LcpGSEM;
        }
#ifdef BAP
        else
        {
            /* If Links' DiscrValue matches with one of the existing member, terminate the link. */
            if (pBundleIf->MPInfo.BundleInfo.BWMProtocolInfo.Protocol ==
                BAP_PROTOCOL)
            {
                SLL_SCAN_OFFSET (&pBundleIf->MPInfo.BundleInfo.MemberList,
                                 pMemberIf, tPPPIf *, FSAP_OFFSETOF (tPPPIf,
                                                                   MPInfo.
                                                                   MemberInfo.
                                                                   NextMPIf))
                {
                    if (pIf->LcpOptionsAckedByLocal.LinkDiscriminator ==
                        pMemberIf->LcpOptionsAckedByLocal.LinkDiscriminator)
                    {
                        PPP_TRC (CONTROL_PLANE,
                                 " Links' Discriminator Value matches with one of the existing member. Terminating !!!");
                        pTermGSEM = &pIf->LcpGSEM;
                        break;
                    }
                }
            }
        }
#endif
    }

    /*  Adding the member link into the list */
    SLL_ADD (&pBundleIf->MPInfo.BundleInfo.MemberList,
             &pIf->MPInfo.MemberInfo.NextMPIf);

    pIf->MPInfo.MemberInfo.PartOfBundle = PPP_YES;

    /* Check if the bundle has only one member. */
    if (SLL_COUNT (&pBundleIf->MPInfo.BundleInfo.MemberList) == SINGLE_LINK)
    {

        /* Notify HL of the bundle's UP status */
        pBundleIf->LcpStatus.OperStatus = STATUS_UP;
        PPPHLINotifyIfStatusToHL (pBundleIf, PPP_UP);
    }

#ifndef MP_ALGORITHM_INTERLEAVING
    /* Check if we have agreed to MP ( Check for half-duplex case ) */
    if (pIf->LcpGSEM.pNegFlagsPerIf[MRRU_IDX].FlagMask & ACKED_BY_LOCAL_MASK)
    {

        /* Start the echo timers and send an echo request */

        pIf->MPInfo.MemberInfo.PeriodicEchoTimer.Param1 = (UINT4) pIf;
        PPPStartTimer (&pIf->MPInfo.MemberInfo.PeriodicEchoTimer,
                       PERIODIC_ECHO_TIMER, DEF_PERIODIC_ECHO_TIMEOUT_VAL);

        LCPSendEchoReq (pIf, NORMAL_TRANSMISSION);
        pIf->MPInfo.MemberInfo.EchoTransmitCount = 1;
    }
#endif

#ifdef MP_ALGORITHM_INTERLEAVING
    MPGetTotalNoOfFragments (pBundleIf);
#endif
#ifdef DPIF
    MPInsertMemberIntoBundleToDP (pIf, pBundleIf);
#endif
    return;
}

/***********************************************************************
*    Function Name    :    MPDeleteMemberFromBundle
*    Description        :
*        This function is used to delete the member information in a bundle.
*    Parameter(s)    :
*        pMemberIf    -    Points to the member link interface structure.
*        pBundleIf    -    Points to the Bundle link interface structure.
*    Return Value    :    VOID
*********************************************************************/
VOID
MPDeleteMemberFromBundle (tPPPIf * pBundleIf, tPPPIf * pMemberIf)
{
    UINT1               Index;
    tMPFrag            *pTmpFrag, *pScan;

    PPP_TRC1 (CONTROL_PLANE, " Deleting the Member Link : %ld",
              pMemberIf->LinkInfo.IfIndex);

    /* Delete the member from the bundle */
    pMemberIf->MPInfo.MemberInfo.PartOfBundle = PPP_NO;
    PPPStopTimer (&pMemberIf->MPInfo.MemberInfo.PeriodicEchoTimer);
    SLL_DELETE (&pBundleIf->MPInfo.BundleInfo.MemberList,
                &pMemberIf->MPInfo.MemberInfo.NextMPIf);

    /* Initialize the MP member information */
    pMemberIf->MPInfo.MemberInfo.LastSeqNumRcvd = INIT_SEQ_VALUE;
    pMemberIf->MPInfo.MemberInfo.TransitDelay =
        (UINT4) (MIN_SPEED * MAX_DIVISIBLE_VALUE) / pMemberIf->LinkInfo.IfSpeed;
    pMemberIf->MPInfo.MemberInfo.CumulativeDelay = 0;

    /* Check if there are no more links in the bundle */
    if (SLL_COUNT (&pBundleIf->MPInfo.BundleInfo.MemberList) == 0)
    {
        PPP_TRC (CONTROL_PLANE, " Bundle is no more operational ...");
        PPP_TRC (EVENT_TRC,
                 " Re-assembly Timer is Stopped. Cleaning the Buffer Que");
        PPPStopTimer (&pBundleIf->MPInfo.BundleInfo.ReassemblyTimer);
        PPPStopTimer (&pBundleIf->MPInfo.BundleInfo.NullFragTimer);
        pTmpFrag =
            (tMPFrag *) SLL_FIRST (&pBundleIf->MPInfo.BundleInfo.RcvdBuffer);

        /* Clear out the fragments buffer by discarding all fragments */
        while (pTmpFrag != NULL)
        {
            pScan =
                (tMPFrag *) SLL_NEXT (&pBundleIf->MPInfo.BundleInfo.RcvdBuffer,
                                      (t_SLL_NODE *) pTmpFrag);
            SLL_DELETE (&pBundleIf->MPInfo.BundleInfo.RcvdBuffer,
                        (t_SLL_NODE *) pTmpFrag);
            MPDiscardFragments (pTmpFrag);
            pTmpFrag = pScan;
        }

        pBundleIf->LcpStatus.OperStatus = STATUS_DOWN;
        PPPHLINotifyIfStatusToHL (pBundleIf, PPP_DOWN);

        /* Reset all bundle parameters for next negotiation */
        MEMCPY (&pBundleIf->MPInfo.BundleInfo.BundleOptAckedByPeer,
                &pBundleIf->MPInfo.BundleInfo.BundleOptDesired,
                sizeof (tBundleOptions));
        for (Index = 0; Index < MAX_BUNDLE_OPTS; Index++)
        {
            if (pBundleIf->MPInfo.BundleInfo.MPFlagsPerIf[Index].
                FlagMask & DESIRED_SET)
            {
                pBundleIf->MPInfo.BundleInfo.MPFlagsPerIf[Index].FlagMask |=
                    ACKED_BY_PEER_SET;
            }
        }
        pBundleIf->MPInfo.BundleInfo.Begin1SeqNum = INIT_SEQ_VALUE;
        pBundleIf->MPInfo.BundleInfo.EndSeqNum = INIT_SEQ_VALUE;
        pBundleIf->MPInfo.BundleInfo.TxSeqNum = 0;
        pBundleIf->MPInfo.BundleInfo.LastSeqFwd = INIT_SEQ_VALUE;
        pBundleIf->MPInfo.BundleInfo.LastM = INIT_SEQ_VALUE;
        pBundleIf->MPInfo.BundleInfo.Begin2SeqNum = INIT_SEQ_VALUE;

        pBundleIf->MPInfo.BundleInfo.NoOfFragRcvd = 0;
        pBundleIf->MPInfo.BundleInfo.NoOfFragDiscarded = 0;
        pBundleIf->MPInfo.BundleInfo.RespRecd = PPP_NO;
        pBundleIf->MPInfo.BundleInfo.RespSent = PPP_NO;
    }
#ifdef DPIF
    MPDeleteMemberFromBundleToDP (pMemberIf, pBundleIf);
#endif
#ifdef MP_ALGORITHM_INTERLEAVING
    MPGetTotalNoOfFragments (pBundleIf);
#endif

    return;
}

/***********************************************************************
*    Function Name    :    MPNullFragTimeOut
*    Description        :
*        This function is used to transmit the NULL Fragments over all the
*    member links to indicate the peer that the transmission is over.
*    Parameter(s)    :
*        pBundleIf    -    Points to the interface data structure.
*    Return Value    :    VOID
*********************************************************************/
VOID
MPNullFragTimeOut (VOID *pIf)
{
    tPPPIf             *pMemberIf;
    t_MSG_DESC         *pOut;
    UINT2               Length;
    tPPPIf             *pBundleIf;
    pBundleIf = (tPPPIf *) pIf;
    Length = 0;

    SLL_SCAN_OFFSET (&pBundleIf->MPInfo.BundleInfo.MemberList, pMemberIf,
                     tPPPIf *, FSAP_OFFSETOF (tPPPIf, MPInfo.MemberInfo.NextMPIf))
    {
        if (pMemberIf != NULL)
        {
            Length = (UINT2) (Length +
                              (pBundleIf->MPInfo.BundleInfo.
                               MPFlagsPerIf[SEQ_IDX].
                               FlagMask & ACKED_BY_LOCAL_MASK) ?
                              SHORT_SEQ_FORMAT_LEN : LONG_SEQ_FORMAT_LEN);

        /** addition: to incorporate padding on Null gragments  *******/
            if ((pMemberIf->LcpGSEM.pNegFlagsPerIf[PADDING_IDX].
                 FlagMask & ACKED_BY_LOCAL_MASK))
            {
                Length = (UINT2) (Length + 8);
            }

            if ((pOut =
                 (t_MSG_DESC *) (VOID *) ALLOCATE_BUFFER (PPP_INFO_OFFSET +
                                                          Length,
                                                          PPP_INFO_OFFSET)) !=
                NULL)
            {
            /********* SKR004 ***********/
            /** addition: to incorporate padding on Null gragments  *******/
                if ((pMemberIf->LcpGSEM.pNegFlagsPerIf[PADDING_IDX].
                     FlagMask & ACKED_BY_LOCAL_MASK))
                {
                    ASSIGN1BYTE (pOut, 0, 1);
                }
                MPTxPkt (pBundleIf, pMemberIf, pOut, BEGIN_END_SET);
            }
            else
            {
                PPP_TRC (ALL_FAILURE, "Error in alloc..");
                return;
            }
        }

    }
    return;
}

/***********************************************************************
*    Function Name    :    MPReassemblyTimeOut
*    Description        :
*            This function is invoked when the REASSEMBLY TIMER expires. It
*    composes a complete packet from the stored fragments, if possible and 
*    passes the same to the HLI. It also discards any unassembled fragments. 
*    Parameter(s)    :
*        pBundleIf    -    Points to the bundle interface data structure
*    Return Value    :    VOID
*********************************************************************/
VOID
MPReassemblyTimeOut (VOID *pIf)
{
    UINT4               DiffTime;
    tPPPIf             *pBundleIf;

    pBundleIf = (tPPPIf *) pIf;
    /*  
       Calculate the time interval between last M update and the time the 
       reassembly timer was started.
     */
    DiffTime =
        pBundleIf->MPInfo.BundleInfo.LastMChangeTicks -
        pBundleIf->MPInfo.BundleInfo.ReassembleStartTicks;

    /* 
       Check if M value has been updated after the previous Reassembly timer
       was started.
     */
    if (DiffTime != 0)
    {

        /* 
           Reset the timer related variables and restart the timer for 
           the remaining duration. 
         */
        pBundleIf->MPInfo.BundleInfo.ReassembleStartTicks =
            pBundleIf->MPInfo.BundleInfo.LastMChangeTicks;
        pBundleIf->MPInfo.BundleInfo.ReassemblyTimer.Param1 =
            PTR_TO_U4 (pBundleIf);
        PPPStartTimer (&pBundleIf->MPInfo.BundleInfo.ReassemblyTimer,
                       REASSEMBLY_TIMER, DiffTime);
        return;
    }

    /* 
       Since M has not been updated in the past reassembly time, delete all
       fragments upto the last unassembled packet. 
     */
    MPDeleteTillLastUnassembledPkt (pBundleIf);

    PPP_TRC (EVENT_TRC, " Starting the Reassembly..");

    /* Check if any fragments are waiting in the list */
    if (SLL_COUNT (&pBundleIf->MPInfo.BundleInfo.RcvdBuffer) != 0)
    {

        /* Restart the reassembly timer again */
        if (OsixGetSysTime (&pBundleIf->MPInfo.BundleInfo.ReassembleStartTicks)
            == OSIX_FAILURE)
        {
            PPP_TRC (ALL_FAILURE, "Failure in  OsixGetSysTime()");
        }
        pBundleIf->MPInfo.BundleInfo.LastMChangeTicks =
            pBundleIf->MPInfo.BundleInfo.ReassembleStartTicks;
        pBundleIf->MPInfo.BundleInfo.ReassemblyTimer.Param1 =
            PTR_TO_U4 (pBundleIf);
        PPPRestartTimer (&pBundleIf->MPInfo.BundleInfo.ReassemblyTimer,
                         REASSEMBLY_TIMER,
                         pBundleIf->MPInfo.BundleInfo.ReassemblyTimeOutVal);
    }

    return;
}

/***********************************************************************
*    Function Name    :    MPDeleteTillLastUnassembledPkt
*    Description        :
*            This function is invoked to flush out the buffer. It constructs
*    a complete pkt, (if possible) or discards the fragments (if necessary).
*    except for the fragments that of the last unassembled packet.
*    Parameter(s)    :
*        pBundleIf    -    Points to the bundle interface data structure
*    Return Value    :    VOID
*********************************************************************/
VOID
MPDeleteTillLastUnassembledPkt (tPPPIf * pBundleIf)
{
    tMPFrag            *pTmpFrag, *pScanPtr;
    /* Makefile changes - signed and unsigned number comparison. */
    INT4                ExpectedSeqNum;

    /* Initializations */
    pTmpFrag = NULL;
    pScanPtr = NULL;
    ExpectedSeqNum = 0;

    /* Scan the linked list of fragments */
    for (pTmpFrag =
         (tMPFrag *) SLL_FIRST (&pBundleIf->MPInfo.BundleInfo.RcvdBuffer),
         pScanPtr =
         (tMPFrag *) SLL_NEXT (&pBundleIf->MPInfo.BundleInfo.RcvdBuffer,
                               (t_SLL_NODE *) pTmpFrag); pTmpFrag != NULL;
         pTmpFrag = pScanPtr, pScanPtr =
         (tMPFrag *) SLL_NEXT (&pBundleIf->MPInfo.BundleInfo.RcvdBuffer,
                               (t_SLL_NODE *) pTmpFrag))
    {
        PPP_TRC (CONTROL_PLANE, "------------ MP Fragment Info -------------");

        /* Check if there is a fragment with Begin Flag SET */
        if (pTmpFrag->BEFlag & BEGIN_SET)
        {

            /*  Check if this is the first fragment in the list */
            if (pTmpFrag !=
                (tMPFrag *) SLL_FIRST (&pBundleIf->MPInfo.BundleInfo.
                                       RcvdBuffer))
            {
                /* Delete all fragments in the list till (excluding) this one */
                pBundleIf->MPInfo.BundleInfo.NoOfFragDiscarded +=
                    SLL_DELETE_TILL_NODE (&pBundleIf->MPInfo.BundleInfo.
                                          RcvdBuffer,
                                          (t_SLL_NODE *) SLL_FIRST (&pBundleIf->
                                                                    MPInfo.
                                                                    BundleInfo.
                                                                    RcvdBuffer),
                                          SLL_PREV (&pBundleIf->MPInfo.
                                                    BundleInfo.RcvdBuffer,
                                                    (t_SLL_NODE *) pTmpFrag),
                                          (void (*)(t_SLL_NODE *))
                                          MPDiscardFragments);
            }

            /* This variable will by used in packet assembly. */
            pBundleIf->MPInfo.BundleInfo.Begin1SeqNum = pTmpFrag->SeqNum;

            /* Check if the End flag is also SET */
            if (pTmpFrag->BEFlag & END_SET)
            {

                /* Seq. num. of current fragment is also the expected seg. num */
                ExpectedSeqNum = pTmpFrag->SeqNum;
            }
            else
            {

                /* Not yet reached the last frag. Wait for the next frag. */
                ExpectedSeqNum = pTmpFrag->SeqNum + 1;
                continue;
            }
        }

        /* Check if expected fragment has been received */
        if (ExpectedSeqNum == pTmpFrag->SeqNum)
        {

            /* Check if the received fragment is the last of a packet */
            if (pTmpFrag->BEFlag & END_SET)
            {

                /* This variable will by used in packet assembly. */
                pBundleIf->MPInfo.BundleInfo.EndSeqNum = pTmpFrag->SeqNum;

                /* Assemble and send away the packet */
                MPAssembleAndForward (pBundleIf);
            }
            else
            {

                /* Not yet reached the last frag. Wait for the next frag. */
                ExpectedSeqNum++;
            }
        }
        else
        {

            /*  
               We have encountered a fragment out of sequence. Check if it is
               an end fragment or not the last in the list. 
             */
            if ((pTmpFrag->BEFlag & END_SET)
                && (pTmpFrag !=
                    (tMPFrag *) SLL_LAST (&pBundleIf->MPInfo.BundleInfo.
                                          RcvdBuffer)))
            {

                /* Discard all fragments upto and including this fragment. */
                pBundleIf->MPInfo.BundleInfo.NoOfFragDiscarded +=
                    SLL_DELETE_TILL_NODE (&pBundleIf->MPInfo.BundleInfo.
                                          RcvdBuffer,
                                          (t_SLL_NODE *) SLL_FIRST (&pBundleIf->
                                                                    MPInfo.
                                                                    BundleInfo.
                                                                    RcvdBuffer),
                                          (t_SLL_NODE *) pTmpFrag,
                                          (void (*)(t_SLL_NODE *))
                                          MPDiscardFragments);
            }
        }

    }
    return;
}

/*********************************************************************
*    Function Name    :    MPPeriodicEchoTimeOut
*    Description        :
*                        This procedure is called everytime the periodic
*    echo timer expires. It sends an LCP echo request and restarts the 
*    echo timer. 
*    Parameter(s)    :
*                pIf    -    Pointer to the member interface structure over 
*                        which the echo packet has to be sent.
*    Return Values    :    VOID
*********************************************************************/
VOID
MPPeriodicEchoTimeOut (VOID *pPppIf)
{
    UINT4               TimeOutVal;
    INT4                DiffTime;
    UINT4               u4Count;
    UINT1               TempVal1;
    UINT1               TempVal2;
    tPPPIf             *pIf;
    pIf = (tPPPIf *) pPppIf;
    LCPSendEchoReq (pIf, NORMAL_TRANSMISSION);

    pIf->MPInfo.MemberInfo.PeriodicEchoTimer.Param1 = PTR_TO_U4 (pIf);
/*    TimeOutVal = ECHO_TIMER_VALUE(pIf->MPInfo.MemberInfo.TransitDelay);*/
    DiffTime = (INT4)
        (pIf->MPInfo.MemberInfo.LastTransitDelay -
         pIf->MPInfo.MemberInfo.TransitDelay);
    if (DiffTime < 0)
        DiffTime = -DiffTime;
    u4Count = pIf->MPInfo.MemberInfo.EchoTransmitCount;
    if (DiffTime > 50)
    {
        TimeOutVal = DEF_PERIODIC_ECHO_TIMEOUT_VAL;
        u4Count = pIf->MPInfo.MemberInfo.EchoTransmitCount = 1;
    }
    else
    {
        TempVal1 = (UINT1) (100 - (2 * DiffTime) % 100);
        TempVal2 = (UINT1) (50 - 5 * u4Count);

        TimeOutVal = TempVal1;
        if ((u4Count < 10) && (TempVal1 > TempVal2))
        {
            TimeOutVal = (UINT4) (TempVal1 - TempVal2);
        }

        TimeOutVal += 10;
    }
    pIf->MPInfo.MemberInfo.EchoTransmitCount++;

    PPPStartTimer (&pIf->MPInfo.MemberInfo.PeriodicEchoTimer,
                   PERIODIC_ECHO_TIMER, TimeOutVal);

    return;
}

/*********************************************************************
*    Function Name    :    MPAddEPDSubOpt
*    Description        :
*                    This function is used to add EPD suboption in an outgoing
*    Config-Request. It is invoked by the GCP module.
*    Parameter(s)    :
*            pGSEM    -    Pointer to the GSEM structure corresponding to the
*                        packet.
*            tOptVal    -    Option Value received  to be processed.
*            pOutBuf    -    The outgoing buffer into which the sub-option has to
*                        be added.
*    Return Value    :    Returns the number of bytes appended in the outgoing
*                        buffer
*********************************************************************/
INT1
MPAddEPDSubOpt (tGSEM * pGSEM, tOptVal OptVal, t_MSG_DESC * pOutBuf)
{
    tBundleOptions     *pBundleOpt;

    PPP_TRC1 (CONTROL_PLANE, "Unused param OptVal = %d", OptVal.ShortVal);

    pBundleOpt =
        &pGSEM->pIf->MPInfo.MemberInfo.pBundlePtr->MPInfo.BundleInfo.
        BundleOptAckedByPeer;

    APPENDSTR (pOutBuf, pBundleOpt->BundleId.Address,
               pBundleOpt->BundleId.Length);
    return ((INT1) (pBundleOpt->BundleId.Length));
}

/*********************************************************************
*    Function Name    :    MPReturnMRRUPtr
*    Description        :
*        This function returns the address of the location where the MRRU
*    value to be included in the CONF_REQ, is stored.
*    Parameter(s)    :
*            pGSEM    -    Points to the GSEM structure
*            OptLen    -    Used to return the length of the option value (in
*                        case the option is of 'STRING' type. Not used here.)
*    Return Value    :    Returns the option data address.
*********************************************************************/
VOID               *
MPReturnMRRUPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    PPP_UNUSED (OptLen);
    return (&pGSEM->pIf->MPInfo.MemberInfo.pBundlePtr->MPInfo.BundleInfo.
            BundleOptAllowedForPeer.MRRU);
}

/*********************************************************************
*  Function Name : MPReturnEPDPtr
*  Description   :
*     This function returns the address of the location where the End Pt. 
*  Discriminator value to be included in the CONF_REQ, is stored
*  Input         :
*     pGSEM      -  points to the GSEM structure
*     OptLen     -  used to return the length of the option value (in
*                   case the option is of 'STRING' type. Not used here )
*  Return Value  :  returns the option data address
*********************************************************************/
VOID               *
MPReturnEPDPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    PPP_UNUSED (OptLen);
    return (&pGSEM->pIf->MPInfo.MemberInfo.pBundlePtr->MPInfo.BundleInfo.
            BundleOptAckedByPeer.BundleId.Class);
}

/*********************************************************************
*    Function Name    :    MPProcessMRRUConfNak 
*    Description        :
*            This function is invoked to process the MRRU option present
*    in the CONF_NAK and to construct appropriate response 
*    Input            :
*            pGSEM    -    Pointer to the GSEM structure corresponding to the
*                        packet.
*            pInPkt    -    Points to the incoming packet buffer. 
*            tOptVal    -    Option Value received to be processed.
*    Return Value    :    OK/NOT_OK 
*********************************************************************/
INT1
MPProcessMRRUConfNak (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal)
{
    tBundleInfo        *pBundleInfo;

    PPP_UNUSED (pInPkt);
    pBundleInfo = &pGSEM->pIf->MPInfo.MemberInfo.pBundlePtr->MPInfo.BundleInfo;

    if (pBundleInfo->RespRecd == PPP_NO)
    {
        if (OptVal.ShortVal <= pBundleInfo->BundleOptAckedByPeer.MRRU)
        {
            pBundleInfo->BundleOptAckedByPeer.MRRU = OptVal.ShortVal;
            pBundleInfo->RespRecd = PPP_YES;
            return (OK);
        }
    }
    else
    {
        if (OptVal.ShortVal == pBundleInfo->BundleOptAckedByPeer.MRRU)
        {
            return (OK);
        }
    }
    return (CLOSE);
}

/*********************************************************************
*    Function Name    :    MPProcessMRRUConfReq 
*    Description        :
*            This function is invoked to process the MRRU option present
*    in the CONF_REQ and to construct appropriate response. 
*    Parameter(s)    :
*            pGSEM    -    Pointer to the GSEM structure corresponding to the
*                       packet.
*            pInPkt    -    Points to the incoming packet buffer. 
*            tOptVal    -    Option Value received  to be processed.
*            Offset    -    Indicates the place in the outgoing buffer at which 
*                        the option value is appended.
*    PresenceFlag    -    Indicates whether the option is present in the recd.
*                       CONF_REQ packet.
*    Return Value    :    Returns the number of bytes appended in the outgoing
*                        buffer. 
*********************************************************************/
INT1
MPProcessMRRUConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal,
                      UINT2 Offset, UINT1 PresenceFlag)
{
    tBundleInfo        *pBundleInfo;

    PPP_UNUSED (pInPkt);
    PPP_UNUSED (pInPkt);
    pBundleInfo = &pGSEM->pIf->MPInfo.MemberInfo.pBundlePtr->MPInfo.BundleInfo;

    if (PresenceFlag == SET)
    {

        if (OptVal.ShortVal < 4)
        {
            ASSIGN2BYTE (pGSEM->pOutParam, Offset,
                         pBundleInfo->BundleOptAllowedForPeer.MRRU);
            return BYTE_LEN_2;
        }

        if (pBundleInfo->RespSent == PPP_NO)
        {
            pBundleInfo->BundleOptAckedByLocal.MRRU = OptVal.ShortVal;
            pBundleInfo->RespSent = PPP_YES;
            return (ACK);
        }
        if (pBundleInfo->BundleOptAckedByLocal.MRRU == OptVal.ShortVal)
        {
            return (ACK);
        }
        ASSIGN2BYTE (pGSEM->pOutParam, Offset,
                     pBundleInfo->BundleOptAckedByLocal.MRRU);
    }
    else
    {
        ASSIGN2BYTE (pGSEM->pOutParam, Offset,
                     pBundleInfo->BundleOptAllowedForPeer.MRRU);
    }
    return (BYTE_LEN_2);
}

/*********************************************************************
*    Function Name    :    MPProcessEPDConfReq 
*    Description        :
*            This function is invoked to process the EPD option present
*    in the CONF_REQ and to construct appropriate response. 
*    Parameter(s)    :
*            pGSEM    -    Pointer to the GSEM structure corresponding to the
*                        packet.
*            pInPkt    -    Points to the incoming packet buffer. 
*            tOptVal    -    Option Value received  to be processed.
*            Offset    -    Indicates the place in the outgoing buffer at which 
*                        the option value is appended.
*    PresenceFlag    -    Indicates whether the option is present in the recd.
*                        CONF_REQ packet.
*    Return Value    :    Returns the number of bytes appended in the outgoing
*                        buffer. 
*********************************************************************/
INT1
MPProcessEPDConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal,
                     UINT2 Offset, UINT1 PresenceFlag)
{
    tPPPIf             *pIf;
    tBundleInfo        *pBundleInfo;
    tBundleId           EndPtDscr;

    MEMSET (&EndPtDscr, 0, sizeof (tBundleId));
    /* Makefile changes - unused */
    PPP_TRC2 (CONTROL_PLANE, "Offset=%d, PresenceFlag=%d", Offset,
              PresenceFlag);

    /*  
       We assume that the bundle pointer of this link is pointing to some 
       bundle. Here we have two cases - 
       (1) Dynamic link addition. In this case NM will create the interface 
       structure when link is requested for. At this point NM will also 
       point the bundle pointer of this link to the bundle which requested
       additional bandwith. 
       (2) Static Link configuration. In this case NM will create both the 
       link interface structure and the bundle interface structure 
       statically and at the same time the bundle pointer of the link will
       also be made to point to a bundle interface. In this case, the 
       offered EPD has to be accepted when the first link of a bundle
       is being negotiated. 

       In both the above cases for negotiations on subsequent links, if the
       offered EPD matches that of the EPD negotiated on the first link, 
       (and all other parameters like MRRU match), then the link is added 
       to the bundle. If the EPD offered does not match the EPD of this 
       bundle, then we try to find a match with other bundles - not only
       for EPD, but also for other parameters. If EPD and all other 
       parameters match with any of the other bundles, we have to 
       renegotiate the link in both directions. 
       If EPD matches and other parameters do not match, we terminate the 
       link assuming some misconfiguration has taken place.
       If EPD does not match with any of the existing bundles, a new bundle
       has to be created. This should include resource creation if needed.

     */

    pBundleInfo = &pGSEM->pIf->MPInfo.MemberInfo.pBundlePtr->MPInfo.BundleInfo;
    if ((ExtractedLength < MIN_EPD_OPT_LEN) ||
        (ExtractedLength > VALID_BYTES (pInPkt)))
    {
        return DISCARD;
    }

    EndPtDscr.Length = (UINT1) (ExtractedLength - (OPT_HDR_LEN + BYTE_LEN_1));
    EndPtDscr.Class = OptVal.CharVal;
    EXTRACTSTR (pInPkt, EndPtDscr.Address, EndPtDscr.Length);

    PPP_TRC2 (CONTROL_PLANE, "Rcvd EPD Class : %d\t EPD Address : %s\n",
              OptVal.CharVal, EndPtDscr.Address);

    if (MPValidateEPD (&EndPtDscr) == NOT_OK)
    {
        return (DISCARD);
    }

    if (EndPtDscr.Class == NULL_CLASS)
    {
        return (ACK);
    }

    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if ((pIf->BundleFlag == BUNDLE)
            && (pIf->MPInfo.BundleInfo.BundleOptAckedByLocal.BundleId.Length ==
                EndPtDscr.Length)
            &&
            (MEMCMP
             (&pIf->MPInfo.BundleInfo.BundleOptAckedByLocal.BundleId,
              &EndPtDscr, EndPtDscr.Length) == 0))
        {
            PPP_TRC2 (CONTROL_PLANE, " [%d] == [%d] ", EndPtDscr.Length,
                      pIf->MPInfo.BundleInfo.BundleOptAckedByLocal.BundleId.
                      Length);
            pGSEM->pIf->MPInfo.MemberInfo.pEPDMatchBundleIf = pIf;
            break;
        }
    }

    if (pGSEM->pIf->MPInfo.MemberInfo.pEPDMatchBundleIf == NULL)
    {
        if ((pBundleInfo->MPFlagsPerIf[EPD_IDX].FlagMask & ACKED_BY_LOCAL_MASK)
            == NOT_SET)
        {
            MEMCPY (&pBundleInfo->BundleOptAckedByLocal.BundleId, &EndPtDscr,
                    sizeof (tBundleId));
        }
    }

    return (ACK);
}

/*********************************************************************
*    Function Name    :    MPValidateEPD
*    Description        :
*        This function is used to validate the End Point Discriminator option
*    values.
*    Parameter(s)    :
*        pBundleId    -    Pointer to the bundle identifier to be validated.
*    Return Value    :    OK/NOT_OK
*********************************************************************/
INT1
MPValidateEPD (tBundleId * pBundleId)
{
    INT1                RetCode;

    RetCode = OK;
    switch (pBundleId->Class)
    {
        case NULL_CLASS:
            if (pBundleId->Length != NULL_CLASS_SIZE)
            {
                RetCode = NOT_OK;
            }
            break;

        case LOC_MAC_ADDRESS_CLASS:
            if (pBundleId->Length > LOC_MAC_ADDRESS_CLASS_SIZE)
            {
                RetCode = NOT_OK;
            }
            break;

        case IP_ADDRESS_CLASS:
            if (pBundleId->Length != IP_CLASS_SIZE)
            {
                RetCode = NOT_OK;
            }
            break;

        case GLOB_MAC_ADDRESS_CLASS:
            if (pBundleId->Length != GLOB_MAC_CLASS_SIZE)
            {
                RetCode = NOT_OK;
            }
            break;

        case MAGIC_CLASS:
            if (pBundleId->Length > MAGIC_CLASS_SIZE)
            {
                RetCode = NOT_OK;
            }
            break;

        case PSND_CLASS:
            if (pBundleId->Length > PSND_CLASS_SIZE)
            {
                RetCode = NOT_OK;
            }
            break;

        default:
            RetCode = NOT_OK;
    }
    return (RetCode);
}

/*********************************************************************
*    Function Name    :    MPInsertIntoProperBundle
*    Description        :
*        This function is used to insert the member link into appropriate
*    MP bundle, based on the Authentication/EPD values. It is called after a
*    member link goes to Open State. If neither Authentication nor EPD values
*    match with corresponding values of existing bundles, a new bundle is
*    created.
*    Input            :
*        pBundleIf    -    Points to the Bundle interface.
*        pMemberIf    -    Points to the member interface.
*    Return Value    :    VOID
*********************************************************************/
VOID
MPInsertMemberIntoProperBundle (tPPPIf * pMemberIf, tPPPIf * pBundleIf)
{
    tPPPIf             *pEffectiveBundleIf;
    UINT1               EPDRecdFlag, AUTHRecdFlag, Action;

    /* Initialize to assumed  values */
    pEffectiveBundleIf = NULL;
    EPDRecdFlag = PPP_NO;
    AUTHRecdFlag = PPP_NO;
    Action = ADD_TO_BUNDLE;

    /* Set the EPD/AUTH flags based on the CONF-ACK sent */
    if (pMemberIf->LcpGSEM.pNegFlagsPerIf[EPD_IDX].
        FlagMask & ACKED_BY_LOCAL_SET)
    {
        EPDRecdFlag = PPP_YES;
    }

    if (pMemberIf->LcpGSEM.pNegFlagsPerIf[AUTH_IDX].
        FlagMask & ACKED_BY_PEER_SET)
    {
        AUTHRecdFlag = PPP_YES;
        AUTHSearchBundleMatch (pMemberIf);
    }

    if (pMemberIf->MPInfo.BundleInfo.BundleOptAckedByLocal.MRRU !=
        pBundleIf->MPInfo.BundleInfo.BundleOptAckedByLocal.MRRU)
    {
        Action = CREATE_NEW_BUNDLE;
    }

    if (MEMCMP
        (pBundleIf->MPInfo.BundleInfo.MPFlagsPerIf,
         pMemberIf->LcpGSEM.pNegFlagsPerIf,
         (2 * sizeof (tOptNegFlagsPerIf))) != 0)
    {
        Action = CREATE_NEW_BUNDLE;
    }

    /* 
       The following sets of statements correspond to 3 of the 4 conditions 
       mentioned in the  RFC 1990. The other condition (No EPD and no 
       authentication is the assumed default.
     */

    /* EPD present but not authentication */
    if ((EPDRecdFlag == PPP_YES) && (AUTHRecdFlag == PPP_NO))
    {
        if (pMemberIf->MPInfo.MemberInfo.pEPDMatchBundleIf == NULL)
        {
            Action = CREATE_NEW_BUNDLE;
        }
        else
        {
            pEffectiveBundleIf = pMemberIf->MPInfo.MemberInfo.pEPDMatchBundleIf;
        }
    }

    /* Authentcation present, but not EPD */
    if ((EPDRecdFlag == PPP_NO) && (AUTHRecdFlag == PPP_YES))
    {
        if (pMemberIf->MPInfo.MemberInfo.pAUTHMatchBundleIf == NULL)
        {
            Action = CREATE_NEW_BUNDLE;
        }
        else
        {
            pEffectiveBundleIf =
                pMemberIf->MPInfo.MemberInfo.pAUTHMatchBundleIf;
        }
    }

    /* Both authentication and EPD present */
    if ((EPDRecdFlag == PPP_YES) && (AUTHRecdFlag == PPP_YES))
    {
        if ((pMemberIf->MPInfo.MemberInfo.pAUTHMatchBundleIf ==
             pMemberIf->MPInfo.MemberInfo.pEPDMatchBundleIf)
            && (pMemberIf->MPInfo.MemberInfo.pEPDMatchBundleIf != NULL))
        {
            pEffectiveBundleIf =
                pMemberIf->MPInfo.MemberInfo.pAUTHMatchBundleIf;
        }
        else
        {
            Action = CREATE_NEW_BUNDLE;
        }
    }

    if (Action == CREATE_NEW_BUNDLE)
    {

        /*  If the bundle pointed to by this interface's bundle pointer has
           no other active links associated with it, then add the link to this
           bundle 
         */
        if (SLL_COUNT (&pBundleIf->MPInfo.BundleInfo.MemberList) == 0)
        {
            pEffectiveBundleIf = pBundleIf;
        }
        else
        {

#ifdef PPP0
            tPPPIfId            IfId;
            tPPPIf             *pNewBundleIf;
            /* Create the bundle resource and start negotiation afresh */
            /* Makefile changes - function returns an aggregate */
            GetIfIndex (MP_TYPE, &IfId);
            if ((pNewBundleIf = MPCreate (&IfId, IfId.porttype)) == NULL)
            {
                PRINT_MEM_ALLOC_FAILURE;
                return;
            }
            MPAddLinkToBundle (pNewBundleIf, pMemberIf);
            pRestartGSEM = &pMemberIf->LcpGSEM;
            return;
#else
            CfaCreateNewBundleIf (pMemberIf->LinkInfo.IfIndex,
                                  pBundleIf->LinkInfo.IfIndex);
            pRestartGSEM = &pMemberIf->LcpGSEM;
            return;
#endif

        }

    }
    else
    {
        if (pEffectiveBundleIf != NULL)
        {

            /*  if the matched bundle is not the same as the one configured or
               dynamically assigned by the Resource Manager, then 
               re-negotiate with this bundle.
             */
            if (pEffectiveBundleIf != pBundleIf)
            {
                MPAddLinkToBundle (pEffectiveBundleIf, pMemberIf);
                pRestartGSEM = &pMemberIf->LcpGSEM;
                return;
            }
        }
        else
        {
            /* No AUTH/EPD case - manually configured case  */
            pEffectiveBundleIf = pBundleIf;
        }
    }

    MPInsertMemberIntoBundle (pEffectiveBundleIf, pMemberIf);

    return;

}

/*********************************************************************
*    Function Name    :    AUTHSearchBundleMatch    
*    Description        :
*        This function is used to search the bundle for the 
*    matching ID/Secret pair(s) in the Local To Remote direction
*    Parameter(s)    :
*            
*    Return Values    :    VOID
*********************************************************************/
VOID
AUTHSearchBundleMatch (tPPPIf * pIf)
{
    tSecret            *pSecret, *pSearchSecret;
    tPPPIf             *pSearchIf;

    SLL_SCAN (&pIf->pAuthPtr->SecurityInfo.SecretList, pSecret, tSecret *)
    {
        if (pSecret->Direction == REMOTE_TO_LOC)
        {
            SLL_SCAN (&PPPIfList, pSearchIf, tPPPIf *)
            {
                if ((pIf->BundleFlag == MEMBER_LINK) && (pSearchIf != pIf)
                    && (pSearchIf->BundleFlag == MEMBER_LINK)
                    && (pSearchIf->pAuthPtr != NULL))
                {
                    SLL_SCAN (&pSearchIf->pAuthPtr->SecurityInfo.SecretList,
                              pSearchSecret, tSecret *)
                    {
                        if ((pSearchSecret->Direction == REMOTE_TO_LOC)
                            &&
                            (MEMCMP
                             (pSearchSecret->Identity, pSecret->Identity,
                              pSecret->IdentityLen) == 0)
                            &&
                            (MEMCMP
                             (pSearchSecret->Secret, pSecret->Secret,
                              pSecret->SecretLen) == 0)
                            && (pSearchSecret->SecretLen == pSecret->SecretLen))
                        {        /*02032001 */
                            pIf->MPInfo.MemberInfo.pAUTHMatchBundleIf =
                                pSearchIf->MPInfo.MemberInfo.pBundlePtr;
                            return;
                        }
                    }
                }
            }
        }
    }
    return;
}

#ifdef BAP
/*********************************************************************
*    Function Name    :    MPGetCallInfoFromRM 
*
*    Description        :
*                    This function is called by the MP module to get the 
*    local, remote phone numbers and the entity which initiated the 
*    link, from the RM. If either of the phone numbers are not available
*    with the RM, that field should be ignored. 
*
*    Parameter(s)    :
*                pIf    -    Points to the corresponding PPP interface.
*    pIsInitiatior    -    Indicates the entity which has initiated the link.
*        pLocalPhNum    -    Points to the local to remote phone number. The 
*                        BAP module allocates memory for this number. 
*        pRemotePhNum-    Points to the remote to local phone number. The 
*                        BAP module allocates memory for this number. 
*
*    Return Value    :    VOID
*********************************************************************/
VOID
MPGetCallInfoFromRM (tPPPIf * pIf, UINT1 *pIsInitiator, tPhNum * pLocalPhNum,
                     tPhNum * pRemotePhNum)
{
#ifdef BAP
    /* The contents of this function are to be modified during porting */
    RMCallInfoToMP (pIf, pIsInitiator, pLocalPhNum, pRemotePhNum);
#endif
    return;
}
#endif

#ifdef MP_ALGORITHM_INTERLEAVING

/*********************************************************************
*    Function Name    :    MPGetTotalNoOfFragments
*
*    Description     :  
*            This function is called by the MP module to get the 
*    LCM of the link speed and minimum MTU among the links and to calculate 
*       the total number of fragments to be sent on the bundle and number of 
*       fragments to be sent on each links. 
*
*    Parameter(s)          :    
*        pBundleIf    -    Points to the Bundle interface.
*
*    Return Value    :    VOID
*********************************************************************/

VOID
MPGetTotalNoOfFragments (tPPPIf * pBundleIf)
{
    UINT4               SpeedVal,
        Speed, CurrLcm, Lcm, Sort_Speed[25], Sort_NoOfFrag[25], TotalFrag;
    UINT2               Index, Count;
    tPPPIf             *pTmpPtr;

    /* Initialize to assumed  values */
    SpeedVal = 0;
    CurrLcm = 1;
    Lcm = 0;
    TotalFrag = 0;
    Index = 0;

    pTmpPtr = (tPPPIf *) SLL_FIRST (&pBundleIf->MPInfo.BundleInfo.MemberList);
    if (pTmpPtr == NULL)
    {
        PPP_DBG (" Leaving fun. : MPGetTotalNoOfFragments");
        return;
    }

    MinMTU = pTmpPtr->LcpOptionsAckedByLocal.MRU;

/* Getting the ifSpeed from CFA and calculating the LCM using the macro LCM_CALCULATE.Also minimum MTU value is stored in the variable MinMTU for future use*/
    SLL_SCAN_OFFSET (&pBundleIf->MPInfo.BundleInfo.MemberList, pTmpPtr,
                     tPPPIf *, FSAP_OFFSETOF (tPPPIf, MPInfo.MemberInfo.NextMPIf))
    {
        if (pTmpPtr != NULL)
        {
            CfaGetIfSpeed (pTmpPtr->LinkInfo.IfIndex,
                           &(pTmpPtr->LinkInfo.IfSpeed));
            SpeedVal = pTmpPtr->LinkInfo.IfSpeed / 1000;
            Speed = Normalize (SpeedVal);
            CurrLcm = LCM_CALCULATE (CurrLcm, Speed);
            if (pTmpPtr->LcpOptionsAckedByLocal.MRU <= MinMTU)
                MinMTU = pTmpPtr->LcpOptionsAckedByLocal.MRU;
        }
    }                            /* End of SLL_SCAN_OFFSET */

    Lcm = CurrLcm;

/* Calculating the total number of fragments to be sent on the bundle using the LCM calculated above */
    SLL_SCAN_OFFSET (&pBundleIf->MPInfo.BundleInfo.MemberList, pTmpPtr,
                     tPPPIf *, FSAP_OFFSETOF (tPPPIf, MPInfo.MemberInfo.NextMPIf))
    {
        if (pTmpPtr != NULL)
        {
            SpeedVal = pTmpPtr->LinkInfo.IfSpeed / 1000;
            Speed = Normalize (SpeedVal);
            TotalFrag = TotalFrag + (Lcm / Speed);
            pTmpPtr->LinkInfo.ConstFragCount = Lcm / Speed;
            if (Index >= sizeof (Sort_Speed) / sizeof (UINT4))
            {
                return;
            }
            Sort_Speed[Index] = pTmpPtr->LinkInfo.IfSpeed;
            if (Index >= sizeof (Sort_NoOfFrag) / sizeof (UINT4))
            {
                return;
            }
            Sort_NoOfFrag[Index++] = pTmpPtr->LinkInfo.ConstFragCount;
            pTmpPtr->LinkInfo.LinkIndex = Index;
        }
    }
    /* End of SLL_SCAN_OFFSET */

/* The number of fragemnts to be sent on each link is in descending order to the speed of the links.So sorting both the speed and fragments in ascending order using qsort() */

    pBundleIf->MPInfo.BundleInfo.TotalFrag = TotalFrag;
    Count = (UINT2) (Index - 1);
    qsort (Sort_Speed, Index, sizeof (unsigned int), Compare);
    qsort (Sort_NoOfFrag, Index, sizeof (unsigned int), Compare);

/* Assigning the number of fragments to be sent on each link*/

    SLL_SCAN_OFFSET (&pBundleIf->MPInfo.BundleInfo.MemberList, pTmpPtr,
                     tPPPIf *, FSAP_OFFSETOF (tPPPIf, MPInfo.MemberInfo.NextMPIf))
    {
        if (pTmpPtr != NULL)
        {
            for (Index = 0; Index <= Count; Index++)
            {
                if (Sort_Speed[Index] == pTmpPtr->LinkInfo.IfSpeed)
                    pTmpPtr->LinkInfo.ConstFragCount = Sort_NoOfFrag[Index];
            }
        }
    }

    return;
}

/*********************************************************************
*Function Name    :    Normalize
*
*Description    :
*                    This function is called to move the speed values
*             closest to multiples of 2 in order to facilitate LCM 
*             calculation and to avoid more number of fragments
*
*    Parameter(s)    :    
*       SpeedVal-    Speed of the link / 1000.
*
*    Return Value    :    Returns the speed closest to constant values maintained.
*
*******************************************************************/

UINT4
Normalize (UINT4 SpeedVal)
{

    UINT4               Const_Speed[13] =
        { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096 }
    , Mid_Val[13] =
    {
    0, 1, 3, 6, 12, 24, 48, 96, 192, 384, 768, 1536, 3072};

    INT4                Index, Found_Index;

    /* Initialisation */

    Index = 0;
    Found_Index = 1;

/* Finding the nearest listed speed value for the given one and finding the index of the array */

    for (Index = 0; Index <= 12; Index++)
    {
        if (SpeedVal <= Const_Speed[Index])
        {
            Found_Index = Index;
            break;
        }
    }

/* With the Index found we determine whether the given speed falls less or more than the std mid values and we get the nearest speed */

    if (Index < 13)
    {
        if (SpeedVal >= Mid_Val[Index])
            SpeedVal = Const_Speed[Index];
        else if (Index > 0)
            SpeedVal = Const_Speed[Index - 1];
    }

    PPP_UNUSED (Found_Index);
    return (SpeedVal);
}

/*********************************************************************
*Function Name    :    GcdCalculate
*
*Description    :
*            This function is called by the LCM_CALCULATE 
*                        to get the GCD of 2 numbers.
*
*Parameter(s)    :    
*    CurrLcm    -    Current LCM value
*    Speed    -    Speed of the link / 1000.
*
*Return Value    :    Returns the GCD value of 2 numbers.
*
*******************************************************************/

UINT4
GcdCalculate (UINT4 CurrLcm, UINT4 Speed)
{
    UINT4               LargeVal = 0, SmallVal = 0, Remainder = 1;
/* FInd the smallest and largest values of current Lcm and Speed */

    if (CurrLcm < Speed)
    {
        LargeVal = Speed;
        SmallVal = CurrLcm;
    }
    else
    {
        LargeVal = CurrLcm;
        SmallVal = Speed;
    }
/* Until we get the remainder as 0 divide largest num by smallest num */

    while (Remainder != 0)
    {
        Remainder = LargeVal % SmallVal;
        LargeVal = SmallVal;
        SmallVal = Remainder;
    }

    return (LargeVal);
}

/*********************************************************************
*    Function Name    :    Compare
*
*    Description    :
*                This function is called by Standard 
*               Library function qsort() to sort the array.
*
*    Parameter(s)    :    
*          p1    -    Pointer to the element to be sorted
*          p2    -    Pointer to the element to be sorted
*
*   Return Value    :    Returns -1,1 or 0 based on whether p1 is <,> 
*                           or equal.
*
********************************************************************/

int
Compare (const void *p1, const void *p2)
{
    if (*(const int *) p1 < *(const int *) p2)
        return (-1);
    else if (*(const int *) p1 > *(const int *) p2)
        return (1);
    else
        return (0);
}

#endif
