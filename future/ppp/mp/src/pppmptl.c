/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppmptl.c,v 1.4 2014/12/16 11:47:25 siva Exp $
 *
 * Description: Low Level Routines for MP module.
 *
 *******************************************************************/
#include "pppsnmpm.h"
#include "mpcom.h"
#include "snmccons.h"
#include "globexts.h"
#include "llproto.h"
#include "ppteslow.h"

INT4                BundleId;
/* LOW LEVEL Routines for Table : PppTestMpConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppTestMpConfigTable
 Input       :  The Indices
                PppTestMpConfigIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppTestMpConfigTable (INT4 i4PppTestMpConfigIfIndex)
{
    if (PppGetPppIfPtr ((UINT4) i4PppTestMpConfigIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppTestMpConfigTable
 Input       :  The Indices
                PppTestMpConfigIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppTestMpConfigTable (INT4 *pi4PppTestMpConfigIfIndex)
{
    if (nmhGetFirstIndex (pi4PppTestMpConfigIfIndex, LinkInstance, FIRST_INST)
        == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppTestMpConfigTable
 Input       :  The Indices
                PppTestMpConfigIfIndex
                nextPppTestMpConfigIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppTestMpConfigTable (INT4 i4PppTestMpConfigIfIndex,
                                     INT4 *pi4NextPppTestMpConfigIfIndex)
{
    if (nmhGetNextIndex (&i4PppTestMpConfigIfIndex, LinkInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextPppTestMpConfigIfIndex = i4PppTestMpConfigIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhTestv2PppTestMpConfigTd
 Input       :  The Indices
                PppTestMpConfigIfIndex

                The Object 
                testValPppTestMpConfigTd
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestMpConfigTd (UINT4 *pu4ErrorCode, INT4 i4PppTestMpConfigIfIndex,
                            INT4 i4TestValPppTestMpConfigTd)
{
    if (PppGetPppIfPtr ((UINT4) i4PppTestMpConfigIfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppTestMpConfigTd < 1)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPppTestMpConfigTd
 Input       :  The Indices
                PppTestMpConfigIfIndex

                The Object 
                retValPppTestMpConfigTd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestMpConfigTd (INT4 i4PppTestMpConfigIfIndex,
                         INT4 *pi4RetValPppTestMpConfigTd)
{
    tPPPIf             *pIf;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppTestMpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppTestMpConfigTd = (INT4) pIf->MPInfo.MemberInfo.TransitDelay;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetPppTestMpConfigTd
 Input       :  The Indices
                PppTestMpConfigIfIndex

                The Object 
                setValPppTestMpConfigTd
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestMpConfigTd (INT4 i4PppTestMpConfigIfIndex,
                         INT4 i4SetValPppTestMpConfigTd)
{
    tPPPIf             *pIf;
    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppTestMpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIf->MPInfo.MemberInfo.TransitDelay = (UINT4) i4SetValPppTestMpConfigTd;
    return (SNMP_SUCCESS);
}
