/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppmpdbg.c,v 1.3 2014/12/16 11:47:25 siva Exp $
 *
 * Description: Contains Debug/Trace functions for MP module.
 *
 *******************************************************************/
#include "mpcom.h"
#include "pppipcp.h"
#include "pppipv6cp.h"
#include "pppmuxcp.h"
#include "pppmplscp.h"
#include "pppipxcp.h"
#ifdef BAP
#include "pppbacp.h"
#include "pppbap.h"
#endif

#include "globexts.h"

VOID
PrintBundleFlags (tPPPIf * pIf)
{

    PPP_TRC4 (CONTROL_PLANE, "MRRU : \t%x\t\t%x\t\t%x\t\t\t%x\n",
              pIf->MPInfo.BundleInfo.MPFlagsPerIf[MRRU_IDX].
              FlagMask & DESIRED_MASK,
              pIf->MPInfo.BundleInfo.MPFlagsPerIf[MRRU_IDX].
              FlagMask & ACKED_BY_PEER_MASK,
              pIf->MPInfo.BundleInfo.MPFlagsPerIf[MRRU_IDX].
              FlagMask & ALLOWED_FOR_PEER_MASK,
              pIf->MPInfo.BundleInfo.MPFlagsPerIf[MRRU_IDX].
              FlagMask & ACKED_BY_LOCAL_MASK);
    PPP_TRC4 (CONTROL_PLANE, "SH :\t%x\t\t%x\t\t%x\t\t\t%x\n",
              pIf->MPInfo.BundleInfo.MPFlagsPerIf[SEQ_IDX].
              FlagMask & DESIRED_MASK,
              pIf->MPInfo.BundleInfo.MPFlagsPerIf[SEQ_IDX].
              FlagMask & ACKED_BY_PEER_MASK,
              pIf->MPInfo.BundleInfo.MPFlagsPerIf[SEQ_IDX].
              FlagMask & ALLOWED_FOR_PEER_MASK,
              pIf->MPInfo.BundleInfo.MPFlagsPerIf[SEQ_IDX].
              FlagMask & ACKED_BY_LOCAL_MASK);
    PPP_TRC4 (CONTROL_PLANE, "EPD :\t%x\t\t%x\t\t%x\t\t\t%x\n",
              pIf->MPInfo.BundleInfo.MPFlagsPerIf[EPD_IDX].
              FlagMask & DESIRED_MASK,
              pIf->MPInfo.BundleInfo.MPFlagsPerIf[EPD_IDX].
              FlagMask & ACKED_BY_PEER_MASK,
              pIf->MPInfo.BundleInfo.MPFlagsPerIf[EPD_IDX].
              FlagMask & ALLOWED_FOR_PEER_MASK,
              pIf->MPInfo.BundleInfo.MPFlagsPerIf[EPD_IDX].
              FlagMask & ACKED_BY_LOCAL_MASK);
    return;
}

VOID
MPPrintBundleInfo (tPPPIf * pBundleIf)
{
    tPPPIf             *pMemberIf;
    tIPCPIf            *pIpcpPtr;
    tIP6CPIf           *pIp6cpPtr;
    tMuxCPIf           *pMuxcpPtr;
    tMPLSCPIf          *pMplscpPtr;
    tIPXCPIf           *pIpxcpPtr;
#ifdef BAP
    tBAPInfo           *pBAPInfo;
    tBACPIf            *pBACPPtr;
#endif

    PPP_TRC (CONTROL_PLANE, PPP_TRC_LINE_SEPERATOR);
    PPP_TRC (CONTROL_PLANE,
             "                BUNDLE  INFORMATION                       ");
    PPP_TRC (CONTROL_PLANE, PPP_TRC_LINE_SEPERATOR);
    PPP_TRC1 (CONTROL_PLANE, " IfIndex : %ld\n", pBundleIf->LinkInfo.IfIndex);
    PPP_TRC (EVENT_TRC, " Negotiated Config Params :");
    PPP_TRC1 (CONTROL_PLANE, " RxSide :     MRRU  :  %x   Header Format : ",
              pBundleIf->MPInfo.BundleInfo.BundleOptAckedByPeer.MRRU);
    if (pBundleIf->MPInfo.BundleInfo.MPFlagsPerIf[SEQ_IDX].
        FlagMask & ACKED_BY_PEER_MASK)
    {
        PPP_TRC (CONTROL_PLANE, "SHORT");
    }
    else
    {
        PPP_TRC (CONTROL_PLANE, "LONG");
    }
    PPP_TRC1 (CONTROL_PLANE, " TxSide :  MRRU    :  %x   Header Format : ",
              pBundleIf->MPInfo.BundleInfo.BundleOptAckedByLocal.MRRU);
    if (pBundleIf->MPInfo.BundleInfo.MPFlagsPerIf[SEQ_IDX].
        FlagMask & ACKED_BY_LOCAL_MASK)
    {
        PPP_TRC (CONTROL_PLANE, "SHORT");
    }
    else
    {
        PPP_TRC (CONTROL_PLANE, "LONG");
    }
    PPP_TRC (CONTROL_PLANE, PPP_TRC_LINE_SEPERATOR);
    PPP_TRC (CONTROL_PLANE, " States of NCP Protocols over this Bundle : ");

    if ((pIpcpPtr = pBundleIf->pIpcpPtr) != NULL)
    {
        PPP_TRC (CONTROL_PLANE, " IPCP State         : ");
        PrintState (pIpcpPtr->IpcpGSEM.CurrentState);
    }
    else
    {
        PPP_TRC (CONTROL_PLANE, " IPCP ptr : NULL");
    }

    if ((pIp6cpPtr = pBundleIf->pIp6cpPtr) != NULL)
    {
        PPP_TRC (CONTROL_PLANE, " IP6CP State         : ");
        PrintState (pIp6cpPtr->Ip6cpGSEM.CurrentState);
    }
    else
    {
        PPP_TRC (CONTROL_PLANE, " IPCP ptr : NULL");
    }
    if ((pMuxcpPtr = pBundleIf->pMuxcpPtr) != NULL)
    {
        PPP_TRC (CONTROL_PLANE, " MUXCP State         : ");
        PrintState (pMuxcpPtr->MuxCPGSEM.CurrentState);
    }
    else
    {
        PPP_TRC (CONTROL_PLANE, " MUXCP ptr : NULL");
    }

    if ((pMplscpPtr = pBundleIf->pMplscpPtr) != NULL)
    {
        PPP_TRC (CONTROL_PLANE, " MPLSCP State         : ");
        PrintState (pMplscpPtr->MplscpGSEM.CurrentState);
    }
    else
    {
        PPP_TRC (CONTROL_PLANE, " MPLSCP ptr : NULL");
    }

    if ((pIpxcpPtr = pBundleIf->pIpxcpPtr) != NULL)
    {
        PPP_TRC (CONTROL_PLANE, " IPXCP State         : ");
        PrintState (pIpxcpPtr->IpxcpGSEM.CurrentState);
    }
    else
    {
        PPP_TRC (CONTROL_PLANE, " IPXCP ptr : NULL");
    }

#ifdef BAP
    if (pBundleIf->MPInfo.BundleInfo.BWMProtocolInfo.Protocol == BAP_PROTOCOL)
    {
        pBAPInfo =
            (tBAPInfo *) pBundleIf->MPInfo.MemberInfo.pBundlePtr->MPInfo.
            BundleInfo.BWMProtocolInfo.pProtocolInfo;
        pBACPPtr = &pBAPInfo->BACPIf;
        PPP_TRC (CONTROL_PLANE, " Bandwidth Management Protocol : BAP ");
        PrintState (pBACPPtr->BACPGSEM.CurrentState);
    }
    else
    {
        PPP_TRC (CONTROL_PLANE, " Bandwidth Management Protocol : NONE ");
    }
#endif

    PPP_TRC (CONTROL_PLANE, PPP_TRC_LINE_SEPERATOR);
    PPP_TRC (CONTROL_PLANE, "    Individual Member Link Information......");
    PPP_TRC (CONTROL_PLANE, PPP_TRC_LINE_SEPERATOR);
    SLL_SCAN_OFFSET (&pBundleIf->MPInfo.BundleInfo.MemberList, pMemberIf,
                     tPPPIf *, FSAP_OFFSETOF (tPPPIf, MPInfo.MemberInfo.NextMPIf))
    {

        if ((pMemberIf != NULL) && ((pIpcpPtr = pMemberIf->pIpcpPtr) != NULL))
        {
            PPP_TRC (CONTROL_PLANE, " IPCP State         : ");
            PrintState (pIpcpPtr->IpcpGSEM.CurrentState);
        }
        else
        {
            PPP_TRC (CONTROL_PLANE, " IPCP ptr : NULL");
        }
        PPP_TRC (CONTROL_PLANE, PPP_TRC_LINE_SEPERATOR);
    }
    return;
}

VOID
MPPrintBundleSeqInfo (INT4 ExpectedSeqNum, INT4 M, tPPPIf * pBundleIf)
{

    PPP_UNUSED (ExpectedSeqNum);
    PPP_UNUSED (M);
    PPP_UNUSED (pBundleIf);
}
