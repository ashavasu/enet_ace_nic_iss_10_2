/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mpexts.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the definitions used by the external
 * modules
 *
 *******************************************************************/
#ifndef __PPP_MPEXTS_H__
#define __PPP_MPEXTS_H__


extern	UINT1		ResetSeqNumFlag;

extern	tOptHandlerFuns  MRRUHandlingFuns;
extern	tOptHandlerFuns  SeqOptHandlingFuns;
extern	tOptHandlerFuns  EPDHandlingFuns;
#endif

