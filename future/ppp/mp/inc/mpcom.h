/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mpcom.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the common MP include files
 *
 *******************************************************************/
#ifndef __PPP_MPCOM_H__
#define __PPP_MPCOM_H__

#include "genhdrs.h"

/* Global PPP includes */
#include "pppmacro.h"
#include "pppdefs.h"
#include "pppdebug.h"
#include "ppptimer.h"
#include "pppdbgex.h"


#include "mpdefs.h"
#include "pppmp.h"
#include "lcpdefs.h"
/* Includes from other modules */
#include "frmtdfs.h"
#include "pppgsem.h"
#include "pppgcp.h"
#include "ppplcp.h"

#include "pppexts.h"

#include "mpproto.h"



#endif  /* __PPP_MPCOM_H__ */
