/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mpproto.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains prototypes for MP functions  
 *             used internally by MP module.
 *
 *******************************************************************/
#ifndef __PPP_MPPROTO_H__
#define __PPP_MPPROTO_H__
#include "ppplcp.h"

tPPPIf *MPFindLinkToBeUsed(tPPPIf *);
INT4 MPUpdateM(tPPPIf *pBundleIf);
VOID MPAssembleAndForward(tPPPIf *pBundleIf);
INT1 MPForwardRxDToHLI(tPPPIf *pBundleIf, t_MSG_DESC *pInPDU, UINT2 Protocol);
/* Makefile changes - signed and unsigned number comparison */
/* Using INT4 for seq numbers */
VOID MPAddFragToList(tPPPIf *pBundleIf, t_MSG_DESC *pInData, UINT2 Len, INT4 SeqNum, UINT1 BEFlag, INT4 M);
VOID MPUpdateBundleNCPInfo(tPPPIf *pBundleIf, tPPPIf *pIf, UINT2 Protocol);
INT1 MPBundleInit(tPPPIf *pBundleIf);
VOID MPOptionInit(tBundleOptions *pBundleOpt);
VOID MPDiscardFragments(tMPFrag *pTmpFrag);

VOID MPTxPkt(tPPPIf *pBundleIf, tPPPIf *pMemberIf, t_MSG_DESC *pOutData, UINT1 BEFlag);
tMPFrag *MPCreateNewFragment(tPPPIf *pBundleIf, INT4 SeqNum, UINT1 BEFlag, t_MSG_DESC *pInData);

VOID MPDelete(tPPPIf *pBundleIf);
VOID MPAddLinkToBundle(tPPPIf *pBundleIf, tPPPIf *pMemberIf);
VOID MPDeleteLinkFromBundle(tPPPIf *pBundleIf, tPPPIf *pMemberIf);
VOID MPInsertMemberIntoBundle(tPPPIf *pBundleIf, tPPPIf *pIf);
VOID MPDeleteMemberFromBundle(tPPPIf *pBundleIf, tPPPIf *pMemberIf);
VOID MPDeleteTillLastUnassembledPkt(tPPPIf   *pBundleIf);

INT1 MPAddEPDSubOpt(tGSEM *pGSEM, tOptVal OptVal, t_MSG_DESC *pOutBuf );
VOID *MPReturnMRRUPtr(tGSEM *pGSEM, UINT1 *OptLen);
VOID *MPReturnEPDPtr(tGSEM *pGSEM, UINT1 *OptLen);
INT1 MPProcessMRRUConfNak(tGSEM *pGSEM, t_MSG_DESC *pInPkt,tOptVal OptVal);
INT1 MPProcessMRRUConfReq(tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal, UINT2 Offset, UINT1 PresenceFlag);
INT1 MPProcessEPDConfReq(tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal, UINT2 Offset, UINT1 PresenceFlag);
INT1 MPValidateEPD(tBundleId *pBundleId);

VOID AUTHSearchBundleMatch (tPPPIf  *pIf);
#ifdef BAP
VOID MPGetCallInfoFromRM(tPPPIf *pIf, UINT1 *pIsInitiator, tPhNum *pLocalPhNum, tPhNum *pRemotePhNum);
#endif

VOID PrintBundleFlags(tPPPIf  *pIf);
VOID MPPrintBundleInfo(tPPPIf *pBundleIf);
VOID MPPrintBundleSeqInfo(INT4 ExpectedSeqNum, INT4 M, tPPPIf *pBundleIf);

/* Related to SNMP */
INT1 CheckAndGetMpPtr(tPPPIf *pIf);
tPPPIf *SNMPGetBundleIfptr(UINT4 Index);
tPPPIf *SNMPGetMemberIfptr(UINT4 Index);

/* Added by KK For MP */
#ifdef MP_ALGORITHM_INTERLEAVING
VOID MPGetTotalNoOfFragments( tPPPIf *pBundleIf);
UINT4 GcdCalculate (UINT4 CurrLcm, UINT4 Speed);
int Compare(const void *,const void *);
UINT4 Normalize(UINT4 SpeedVal);
#endif

#endif  /* __PPP_MPPROTO_H__ */
