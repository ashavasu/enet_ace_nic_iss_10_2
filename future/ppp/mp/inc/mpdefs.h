/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mpdefs.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This files contains the #defines used by  the
 *             MP module of PPP subsystem.
 *
 *******************************************************************/
#ifndef __PPP_MPDEFS_H__
#define __PPP_MPDEFS_H__

#define  MRRU_OPTION                             17
#define  SEQ_OPTION                              18
#define  ENDPT_OPTION                            19
#define  OPENED                                   9
#define  ENDPT_OPT_LEN                            3
#define  NEG_MRRU                                 1
#define  NEG_SEQ                                  2
#define  NEG_ENDPT                                4
#define  MRRU_RECD                                4
#define  SEQ_RECD                                 8
#define  ALLOW_SEQ_ALWAYS                         2
#define  LEAST_VALUE                             -2
#define  BUNDLE                                   1
#define  MEMBER_LINK                              2
#define  DEF_MRRU_SIZE                         1500
#define  SHORT_SEQ_FORMAT                         1
#define  LONG_SEQ_FORMAT                          2
#define  INIT_SEQ_VALUE                          -1

#define  WITH_MP_HEADER                           1
#define  WITHOUT_MP_HEADER                        2
#define  DEF_ASSEMBLE_TIME_OUT                   50
#define  DEF_NULL_FRAG_TIME_OUT                  30
#define  DEF_PERIODIC_ECHO_TIME_OUT              30

#define  BEGIN_END_NOT_SET                     0x00
#define  BEGIN_SET                             0x80
#define  END_SET                               0x40
#define  BEGIN_END_SET                         0xc0
#define  SHORT_BEGIN_SET                     0x8000
#define  SHORT_END_SET                       0x4000
#define  LONG_BEGIN_SET                  0x80000000
#define  LONG_END_SET                    0x40000000

#define  NO_NEG_MRRU                         0xFFFE
#define  NO_NEG_SEQ                          0xFFFD
#define  NO_NEG_ENDPT                        0xFFFB


#define  SHORT_MAX_INTEGER                   0x1000
#define  LONG_MAX_INTEGER                 0x1000000

#define  NULL_CLASS                               0
#define  LOC_MAC_ADDRESS_CLASS                    1
#define  IP_ADDRESS_CLASS                         2
#define  GLOB_MAC_ADDRESS_CLASS                   3
#define  MAGIC_CLASS                              4
#define  PSND_CLASS                               5

#define  NULL_CLASS_SIZE                          0
#define  LOC_MAC_ADDRESS_CLASS_SIZE              20
#define  IP_CLASS_SIZE                            4
#define  GLOB_MAC_CLASS_SIZE                      6
#define  PSND_CLASS_SIZE                         15
#define  MAGIC_CLASS_SIZE                        20

#define  MIN_SPEED                             9600
#define  MAX_DIVISIBLE_VALUE                    214
#define  MAX_DISC_ALLOWED                        20
#define  DEF_PERIODIC_ECHO_TIMEOUT_VAL           10

#define MP_LONG_HEADER_FORMAT                   2
#define MP_SHORT_HEADER_FORMAT                  6

#define MP_MIN_SUSP_CLASS                       1
#define MP_LONG_MAX_SUSP_CLASS                  16
#define MP_SHORT_MAX_SUSP_CLASS                 4
#endif  /* __PPP_MPDEFS_H__ */
