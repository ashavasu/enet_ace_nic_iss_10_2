/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppmp.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the definitions used by the MP 
 * module
 *
 *******************************************************************/
#ifndef __PPP_PPPMP_H__
#define __PPP_PPPMP_H__

/* Maximum Phone Number Size */
#define		PPP_MAX_PH_NUM_SIZE		40

/* Stores a single phone number */
typedef struct PhNum {
	UINT1 		Size;		/* Phone Number Size including Sub-Address Size */ 
	UINT1 		SubAddrSize;	/* Size of the Sub-Address alone, if any */
				/*	Phone Number String including the sub-address at the end */
	UINT1 		String[PPP_MAX_PH_NUM_SIZE];
	UINT2		u2Rsvd;
} tPhNum;


#endif  /* __PPP_PPPMP_H__ */
