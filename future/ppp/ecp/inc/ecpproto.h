/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ecpproto.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the prototypes for the ECP functions 
 * used internally by the ECP module
 *
 *******************************************************************/
#ifndef __PPP_ECPPROTO_H__
#define __PPP_ECPPROTO_H__


tECPIf	*ECPCreateIf(tPPPIf *);
VOID     ECPDeleteIf(tECPIf *);
VOID     ECPEnableIf(tECPIf *);
VOID     ECPDisableIf(tECPIf *);
VOID     ECPInit(tECPIf *);

INT1	ECPProcess3DESEConfReq(tGSEM *, t_MSG_DESC *, tOptVal , UINT2 , UINT1);
INT1	ECPProcess3DESEConfNak(tGSEM *, t_MSG_DESC *, tOptVal);
VOID	*ECPReturn3DESEAddrPtr(tGSEM *, UINT1 *);

INT1	ECPProcessDESEConfReq(tGSEM *, t_MSG_DESC *, tOptVal , UINT2 , UINT1);
INT1	ECPProcessDESEConfNak(tGSEM *, t_MSG_DESC *, tOptVal);
VOID	*ECPReturnDESEAddrPtr(tGSEM *, UINT1 *);
VOID  ECPRxData(tGSEM *);
VOID  ECPTxData(tGSEM *);
INT1	ECPRecdResetAck(tGSEM *, t_MSG_DESC *, UINT2 , UINT1);
INT1	ECPRecdResetReq(tGSEM *, t_MSG_DESC *, UINT2 , UINT1);
VOID  ECPProcessResetReq(tGSEM *pGSEM);
VOID  ECPProcessResetAck(tGSEM *pGSEM);
INT1	ECPProcessConfRej(tGSEM  *);

INT2 GetECPOptionIndex(UINT1 );
tGSEM *ECPGetSEMPtr ( tPPPIf *);
INT1 ECPCopyOptions ( tGSEM *pGSEM, UINT1 Flag, UINT1 PktType );
VOID ECPUp(tGSEM *);
VOID ECPDown(tGSEM *);
VOID ECPDeleteUnAckedNodes(tGSEM *);
VOID ECPCopyDesiredToAckedByPeer(tECPIf *);


#endif  /* __PPP_ECPPROTO_H__ */
