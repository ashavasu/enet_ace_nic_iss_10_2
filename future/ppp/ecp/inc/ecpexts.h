/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ecpexts.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the definitions and prototypes for the
 * functions used by the external module
 *
 *******************************************************************/
tECPIf *ECPCreateIf(tPPPIf *pIf);
VOID ECPDeleteIf(tECPIf *pECPIf);
VOID ECPDisableIf(tECPIf *pECP);
VOID ECPInit(tECPIf *pECPIf);
VOID    ECPRxData(tGSEM *pGSEM);
VOID ECPTxData(tGSEM *pGSEM);
INT1 ECPRecdResetReq(tGSEM *pGSEM, t_MSG_DESC *pIn, UINT2 Length, UINT1 Id);
VOID ECPProcessResetReq(tGSEM *pGSEM);
INT1 ECPRecdResetAck(tGSEM *pGSEM, t_MSG_DESC *pIn, UINT2 Length, UINT1 Id);
VOID ECPProcessResetAck(tGSEM *pGSEM);
INT1 ECPProcessConfRej(tGSEM  *pGSEM);
tGSEM * ECPGetSEMPtr(tPPPIf *pIf);
VOID ECPUp(tGSEM *pECPGSEM);
VOID ECPDown(tGSEM *pECPGSEM);
INT1 ECPCopyOptions ( tGSEM *pGSEM, UINT1 Flag, UINT1 PktType );




INT1 CheckAndGetECPTxPtr(tPPPIf *pIf, UINT4 *n1, UINT4 *n2, UINT4 SecondIndex);
INT1 CheckAndGetECPRxPtr(tPPPIf *pIf, UINT4 *n1, UINT4 *n2, UINT4 SecondIndex);
tECPIf *SNMPGetECPIfPtr(UINT4);
tECPIf *SNMPGetOrCreateECPIfPtr(UINT4);
INT1 CheckAndGetECPPtr(tPPPIf *pIf);
