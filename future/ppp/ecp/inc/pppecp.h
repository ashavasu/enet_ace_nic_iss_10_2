/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppecp.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the typedefs, #defines and data  
 *             structures used by the ECP Module.      
 *
 *******************************************************************/
#ifndef __PPP_PPPECP_H__
#define __PPP_PPPECP_H__

/*********************************************************************/
/*                              CONSTANTS                           */
/*********************************************************************/
#define  OPTION_UNUSED                   5                   
#define  DUMMY_IDX                      2                   

#define  MAX_ECP_OPTIONS                3                   
#define  DISCARD_ECP_PKT                -2                  
#define  ENCRYPTED                      1                   
#define  NON_ENCRYPTED                  2                   
#define  ECP_ENABLED                    1                   

#define  DESE_IDX                       0                   

#define  TRPL_DESE_IDX                  1                   

#define  KEY_LENGTH                     8                   
#define  DESE_NONCE_LENGTH              8                   
#define  DESE_SEQ_NUM_LENGTH            2                   
#define  DESE_RESET_PKT_LENGTH          DESE_SEQ_NUM_LENGTH 

typedef struct ecpStat{
 UINT1 Id;
 UINT1 u1Rsvd1;
 UINT2 RxResetReqs;
 UINT2 TxResetReqs;
 UINT2 RxResetAcks;
 UINT2 TxResetAcks;
 UINT2 u2Rsvd2;

}tECPStatus;
typedef struct ecpif{

 UINT1  OperStatus;
 UINT1  AdminStatus;
 UINT2  u2Rsvd1;

 tECPStatus Status;
 t_SLL  OptionsDesired;
 t_SLL  OptionsAllowedForPeer; 
 t_SLL  OptionsAckedByPeer;
 t_SLL  OptionsAckedByLocal; 
 tGSEM  ECPGSEM;
 
 VOID *  pInEncIf;
 VOID *  pOutEncIf;

 t_MSG_DESC *pResetReq;
 UINT1  ECPCurrentIndex;
 UINT1  u1Rsvd2;
 UINT2  u2Rsvd3; 

}   tECPIf;


typedef struct {

 UINT1 OptType;
 UINT1 u1Rsvd1;
 UINT2 u2Rsvd2;

 INT1    (*Init)(VOID *, VOID *, VOID **); /* This parameter is the MTU of the link */
 VOID    (*DeInit)(VOID*);

 INT1 (*ValidateResetPkt)(UINT2, UINT1);
 VOID (*Reset)(VOID *); /* The param is tDESEParams. Inorderto generalise, this parameter is used */
 INT1 (*Decrypt)(t_MSG_DESC **, VOID *);
 INT1 (*Encrypt)(t_MSG_DESC **, VOID *);
 UINT2 (*ConstructResetReqPkt)(t_MSG_DESC *); /* The param is Id of the pkt */

}tECPFNPOINTER;


#endif  /* __PPP_PPPECP_H__ */
