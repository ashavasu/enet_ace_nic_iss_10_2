/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppecpmd.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains definitions required for the ECP 
 * module
 *
 *******************************************************************/
#ifndef __PPP_PPPECPMD_H__
#define __PPP_PPPECPMD_H__

#define OPTION_3DESE 	2
#define OPTION_DESE 	3 /* changed the value 1 -> 3  : PPP DESE New
			     specification . Implementation MUST reject 
			     option 1 !! */


#endif  /* __PPP_PPPECPMD_H__ */
