/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppecp.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains callback routines of ECP module.
 *             It also contains routines for the input ECP packet
 *             processing.
 *
 *******************************************************************/
#include "ecpcom.h"
#include "pppecpmd.h"
#include "gcpdefs.h"
#include "lcpdefs.h"
#include "gsemdefs.h"
#include "pppdes.h"
#include "desproto.h"
#include "genexts.h"

#include "globexts.h"
#ifdef UNIX
#include <sys/time.h>
#include <unistd.h>
#endif

/******** SKR004 commented after adding this variable ECPIf ********/

#define NONCE_LEN    8

UINT2               ECPAllocCounter;
UINT2               ECPIfFreeCounter;

tECPFNPOINTER       ECPFnPointer[] = {
    {OPTION_DESE,
     0, 0,
     (INT1 (*)(VOID *, VOID *, VOID **)) DESEInit,
     DESEDeInit,
     DESEValidateResetPkt,
     DESEHandleResetPkt,
     DESEDecrypt,
     DESEEncrypt,
     DESEConstructResetReqPkt}
    ,
    {OPTION_3DESE,
     0, 0,
     (INT1 (*)(VOID *, VOID *, VOID **)) TripleDESEInit,
     TripleDESEDeInit,
     TripleDESEValidateResetPkt,
     TripleDESEHandleResetPkt,
     TripleDESEDecrypt,
     TripleDESEEncrypt,
     TripleDESEConstructResetReqPkt}
};

tOptHandlerFuns     DESHandlingFuns = {
    ECPProcessDESEConfReq,
    ECPProcessDESEConfNak,
    ECPProcessConfRej,
    ECPReturnDESEAddrPtr,
    NULL
};

tOptHandlerFuns     TrplDESHandlingFuns = {
    ECPProcess3DESEConfReq,
    ECPProcess3DESEConfNak,
    ECPProcessConfRej,
    ECPReturn3DESEAddrPtr,
    NULL                        /* No sub options and hence no fn */
};

tOptHandlerFuns     DummyECPHandlingFuns = {
    NULL,
    NULL,
    ECPProcessConfRej,
    NULL,
    NULL
};

/* Makefile Changes - < missing braces around initializer > */

tGenOptionInfo      ECPGenOptionInfo[] = {
    {OPTION_DESE, STRING_TYPE, STRING_TYPE, 10, &DESHandlingFuns, {NULL}
     , NOT_SET, 0, 0}
    ,
    {OPTION_3DESE, STRING_TYPE, STRING_TYPE, 10, &TrplDESHandlingFuns, {NULL}
     , NOT_SET, 0, 0}
    ,
    {OPTION_UNUSED, BOOLEAN_CATEGORY, BOOL_TYPE, 2, &DummyECPHandlingFuns, {NULL}
     , NOT_SET, 0, 0}
};

tOptNegFlagsPerIf   ECPOptPerIntf[] = {
    {OPTION_DESE, ALLOW_FORCED_NAK_SET | ALLOW_REJECT_SET, 0}
    ,
    {OPTION_3DESE, ALLOW_FORCED_NAK_SET | ALLOW_REJECT_SET, 0}
    ,
    {OPTION_UNUSED, ALLOW_FORCED_NAK_SET | ALLOW_REJECT_SET, 0}
};

tGSEMCallbacks      ECPGSEMCallback = {
    ECPUp,
    ECPDown,
    NULL,
    NULL,
    ECPRxData,
    ECPTxData,
    ECPProcessResetReq,
    ECPProcessResetAck
};

/***********************************************************************/
/*                      INTERFACE  FUNCTIONS                           */
/***********************************************************************
*  Function Name : ECPCreateIf
*  Description   :
*          This procedure creates an entry in the ECP interface data 
*  base corresponding to IfId. It also initializes the created entry to the 
*  default values  by calling the ECPinit() function.
*  Parameter(s)  :
*         pIf -    pointer to the PPP interface structure
*  Return Values : 
*      pointer to the newly created  interface structure , if the creation is 
*        success
*      NULL if it fails to create the interface.
*********************************************************************/
tECPIf             *
ECPCreateIf (tPPPIf * pIf)
{
    tECPIf             *pECPIf;

    if ((pECPIf = (tECPIf *) ALLOC_ECP_IF ()) != NULL)
    {
        if (pIf->BundleFlag == BUNDLE)
        {
            PPP_DBG ("ECP Over Bundle...");
            pIf->LcpStatus.ECPEnableFlag = ECP_ENABLED;
            pECPIf->ECPGSEM.Protocol = ECP_OVER_INDIVIDUAL_LINK;
        }
        else if (pIf->MPInfo.MemberInfo.pBundlePtr != NULL)
        {

            PPP_DBG ("With MP - ECP Over a Member Link...");
            pIf->LcpStatus.ECPEnableFlag = ECP_ENABLED;
            pECPIf->ECPGSEM.Protocol = ECP_OVER_MEMBER_LINK;
        }
        else
        {
            PPP_DBG ("Without MP - ECP Over Ind. Link...");
            pIf->LcpStatus.ECPEnableFlag = ECP_ENABLED;
            pECPIf->ECPGSEM.Protocol = ECP_OVER_INDIVIDUAL_LINK;
        }
        ECPInit (pECPIf);
        pECPIf->ECPGSEM.pIf = pIf;
        pIf->pECPIf = (VOID *) pECPIf;
        return (pECPIf);
    }
    else
    {
        PPP_TRC (ALL_FAILURE, "Error: Unable to alloc. mem. for ECP entry .");
    }

    return (NULL);
}

/*********************************************************************
*  Function Name : ECPDeleteIf
*  Description   :
*               This procedure deletes an ECP interface  entry from the 
*  database and deallocates memory corresponding to the interface.
*  Parameter(s)  :
*         pECP -  points to  the ECP interface structure to be deleted.
*  Return Values : VOID
*********************************************************************/
VOID
ECPDeleteIf (tECPIf * pECP)
{

    DeleteNodes (&pECP->OptionsDesired);
    DeleteNodes (&pECP->OptionsAckedByPeer);
    DeleteNodes (&pECP->OptionsAllowedForPeer);
    DeleteNodes (&pECP->OptionsAckedByLocal);

    pECP->ECPGSEM.pIf->pECPIf = NULL;

    GSEMDelete (&(pECP->ECPGSEM));

    FREE_ECP_PTR (pECP);

}

/*********************************************************************
*  Function Name : ECPEnableIf
*  Description   :
*              This function is called when there is an  ECP Admin Open 
*  for an interface from the SNMP It  triggers an OPEN event to the GSEM and 
*  passes the ECP tGSEM corresponds to this interface as parameter.
*  Parameter(s)  :
*            pECP    -  points to the ECP interface structure to be enabled
*  Return Values : VOID
*********************************************************************/
VOID
ECPEnableIf (tECPIf * pECP)
{
    tPPPIf             *pIf;

    if (pECP->ECPGSEM.pIf->BundleFlag == BUNDLE)
    {
        SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
        {
            if (pIf->MPInfo.MemberInfo.pBundlePtr != NULL
                && pIf->MPInfo.MemberInfo.pBundlePtr == pECP->ECPGSEM.pIf)
            {
                pIf->pECPIf = pECP;
            }
        }
    }
    pECP->AdminStatus = ADMIN_OPEN;
    pECP->ECPCurrentIndex = 0;

    DeleteNodes (&pECP->OptionsAckedByPeer);
    ECPCopyDesiredToAckedByPeer (pECP);

    /* Check whether the link is already in the Operational state. 
       If so, then indicate the GSEM about the  link UP 
     */

    if (pECP->ECPGSEM.pIf->LcpStatus.LinkAvailability == LINK_AVAILABLE)
    {

        PPP_DBG ("Upping SEM \n");
        GSEMRun (&pECP->ECPGSEM, UP);
    }

    PPP_DBG ("Opening SEM\n");
    GSEMRun (&pECP->ECPGSEM, OPEN);

    GSEMRestartIfNeeded (&pECP->ECPGSEM);

    return;
}

/*********************************************************************
*  Function Name : ECPDisableIf
*  Description   :
*          This function is called when there is an Admin Close for an 
*  interface from the SNMP It triggers a CLOSE event to the GSEM and passes 
*  the ECP GSEMStructure corresponds to this interface as a parameter. 
*  Parameter(s)  :
*            pECP - points to  the ECP interface structure to be  disabled
*  Return Values : VOID
*********************************************************************/
VOID
ECPDisableIf (tECPIf * pECP)
{
    pECP->AdminStatus = ADMIN_CLOSE;
    GSEMRun (&pECP->ECPGSEM, CLOSE);
    return;
}

/*********************************************************************
*  Function Name : ECPInit
*  Description   :
*              This function initializes the ECP GSEM and  sets 
*  the  ECP configuration option variables to their default values. It is 
*  invoked by the ECPCreateIf() function.
*  Parameter(s)  :
*        pECP -   points to the ECP interface structure
*  Return Values : VOID
*********************************************************************/
VOID
ECPInit (tECPIf * pECP)
{

    OptionInit (&pECP->OptionsDesired);
    OptionInit (&pECP->OptionsAllowedForPeer);
    OptionInit (&pECP->OptionsAckedByPeer);
    OptionInit (&pECP->OptionsAckedByLocal);

    pECP->AdminStatus = STATUS_DOWN;
    pECP->OperStatus = STATUS_DOWN;

    GSEMInit (&pECP->ECPGSEM, MAX_ECP_OPTIONS);
    pECP->ECPGSEM.pCallbacks = &ECPGSEMCallback;
    pECP->ECPGSEM.pGenOptInfo = ECPGenOptionInfo;

    MEMCPY (pECP->ECPGSEM.pNegFlagsPerIf, &ECPOptPerIntf,
            sizeof (tOptNegFlagsPerIf) * pECP->ECPGSEM.MaxOptTypes);

    return;
}

/***********************************************************************/
/*                      INTERNAL  FUNCTIONS                            */
/**************************************************************************
*  Function Name : ECPProcessDESEConfReq
*  Description   :
*              This function handles the config_request when DESE is
*  negotiated as Option. This function calls DESE function to validate
*  and verify the value of the parameters negotiated.
*
*  Parameter(s)  :
*            pGSEM            - Pointer to GSEM structure.
*            pInPkt            - Pointer to the incoming packet.
*            tOptVal            - Option value to be processed.    
*            Offset            - Indicates the place in the outgoing buffer
*                                  at which the option value is appended.
*            PresenceFlag    - Indicates whether the option is present in
*                                the received CONFIG_REQ packet
*
*  Return Values : Returns the number of bytes appended in the outgoing 
*                    buffer
***************************************************************************/
INT1
ECPProcessDESEConfReq (tGSEM * pGSEM, t_MSG_DESC * pIn, tOptVal OptVal, UINT2 Offset, UINT1 PresenceFlag)    /* PresenceFlag ISSUE has to be solved - SRINI */
{
    tDESEParams        *pDESEAcked, *pDESEDesired;
    tHeader            *pHeader;
    tECPIf             *pECPIf;

    PPP_UNUSED (pIn);
    PPP_UNUSED (Offset);
    PPP_UNUSED (PresenceFlag);

    pECPIf = (tECPIf *) pGSEM->pIf->pECPIf;

    SLL_SCAN (&pECPIf->OptionsAllowedForPeer, pHeader, tHeader *)
    {

        if (pHeader->Type == OPTION_DESE)
        {

            pDESEDesired = (tDESEParams *) pHeader;

            if ((pDESEAcked = (tDESEParams *) ALLOC_DESE_PARAMS ()) == NULL)
            {

                PPP_TRC (ALL_FAILURE,
                         "ALLOC_DESE_PARAMS failed in ECPProcessDESEConfReq() ...");
                return DISCARD_OPT;
            }

            pDESEAcked->Header.Type = OPTION_DESE;
            pDESEAcked->Header.Length = sizeof (tDESEParams);

            MEMCPY (pDESEAcked->Nonce, OptVal.StrVal, NONCE_LEN);
            MEMCPY (pDESEAcked->Key, pDESEDesired->Key, KEY_LENGTH);
            SLL_ADD (&pECPIf->OptionsAckedByLocal,
                     (t_SLL_NODE *) & pDESEAcked->Header);

        }
    }
    return ACK;
}

/**************************************************************************
*  Function Name : ECPProcessDESEConfNak
*  Description   :
*              This function handles the config_Nak when DESE is
*  negotiated as Option. This function copies the Nacked values into
*  AckedByPeer structure.
*
*  Parameter(s)  :
*            pGSEM            - Pointer to GSEM structure.
*            pInPkt            - Pointer to the incoming packet.
*            tOptVal            - Option value to be processed.    
*
*  Return Values : DISCARD_OPT
***************************************************************************/

INT1
ECPProcessDESEConfNak (tGSEM * pGSEM, t_MSG_DESC * pIn, tOptVal OptVal)
{

    PPP_UNUSED (pGSEM);
    PPP_UNUSED (pIn);
    PPP_UNUSED (OptVal);
    return DISCARD_OPT;
}

/**************************************************************************
*  Function Name : ECPReturnDESEAddrPtr
*  Description   :
*              This function returns the address of the DESE Parameters.
*
*  Parameter(s)  :
*            pGSEM            - Pointer to GSEM structure.
*            OptLen            - Pointer to total Length of the parameters.
*
*  Return Values : VOID *, the pointer to DESE Parameters.
***************************************************************************/
VOID               *
ECPReturnDESEAddrPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    tDESEParams        *pDESEParams;
    tHeader            *pHeader;
    tECPIf             *pECPIf;

    pECPIf = (tECPIf *) pGSEM->pIf->pECPIf;

    SLL_SCAN (&pECPIf->OptionsAckedByPeer, pHeader, tHeader *)
    {

        if (pHeader->Type == OPTION_DESE)
        {

            pDESEParams = (tDESEParams *) pHeader;

            *OptLen = NONCE_LEN;

            return (VOID *) (pDESEParams->Nonce);
        }
    }
    /* Makefile changes - <control-reaches non-void fn> */
    return (VOID *) NULL;

}

/**************************************************************************
*  Function Name : ECPRxData
*  Description   :
*              This function handles the incoming encrypted data. It
*  decryptes the data and forwards to the HL, if there is no error
*  in decryption. If there are any error in the decryption, it
*    sends RESET_REQ to the peer.
*
*  Parameter(s)  :
*            pGSEM            - Pointer to GSEM structure.
*
*  Return Values : VOID
***************************************************************************/
VOID
ECPRxData (tGSEM * pGSEM)
{
    INT2                RetVal;
    UINT2               Length;
    INT2                Index;
    tHeader            *pHeader;
    tECPIf             *pECPIf;

    pECPIf = (tECPIf *) pGSEM->pIf->pECPIf;

    pHeader = (tHeader *) SLL_FIRST (&pECPIf->OptionsAckedByPeer);

    if ((Index = GetECPOptionIndex (pHeader->Type)) == DISCARD_OPT)
    {

        return;
    }

    Length = (UINT2) VALID_BYTES (pGSEM->pInParam);

    RetVal =
        (*ECPFnPointer[Index].Decrypt) (&pGSEM->pInParam, pECPIf->pInEncIf);

    if (RetVal == ERROR || RetVal == DISCARD_ECP_PKT)
    {

        if ((pGSEM->pOutParam =
             (t_MSG_DESC *) ALLOCATE_BUFFER (MAX_PKT_SIZE,
                                             DATA_OFFSET)) != NULL)
        {

            if (RetVal == ERROR)
            {

                pECPIf->Status.Id++;
            }

            if (*ECPFnPointer[Index].ConstructResetReqPkt != NULL)
            {

                Length =
                    (*ECPFnPointer[Index].ConstructResetReqPkt) (pGSEM->
                                                                 pOutParam);

                MOVE_BACK_PTR (pGSEM->pOutParam, CP_HDR_LEN);
                FILL_PKT_HEADER (pGSEM->pOutParam, RESET_REQ_CODE,
                                 pECPIf->Status.Id,
                                 (UINT2) (Length + CP_HDR_LEN));

                PPP_DBG ("Decryption Failed. Sending Reset_Req.... ");

                pECPIf->Status.TxResetReqs++;
                pECPIf->pResetReq = (t_MSG_DESC *) DUPLICATE_BUFFER (pGSEM->pOutParam, TRUE);    /* Copy the the content of the Reset Request into the buffer so that the buffer can be compared with the Reset Ack buffer while validating */

                PPPLLITxPkt (pGSEM->pIf, pGSEM->pOutParam,
                             (UINT2) (VALID_BYTES (pGSEM->pOutParam)),
                             pGSEM->Protocol);

                pGSEM->MiscParam.CharValue = PKT_DISCARDED;
            }

        }
        else
        {

            PRINT_MEM_ALLOC_FAILURE;
        }
    }
    else
    {

        pGSEM->MiscParam.CharValue = PKT_NOT_DISCARDED;
    }

}

/**************************************************************************
*  Function Name : ECPTxData
*  Description   :
*                This function receives the data from the higher layer, 
*                encryptes and sends to the Lower Layer.
*  Parameter(s)  :
*            pGSEM            - Pointer to GSEM structure.
*
*  Return Values : VOID
***************************************************************************/
VOID
ECPTxData (tGSEM * pGSEM)
{
    tHeader            *pHeader;
    INT2                Index;
    tECPIf             *pECPIf;

    pECPIf = (tECPIf *) pGSEM->pIf->pECPIf;
    pGSEM->MiscParam.CharValue = NON_ENCRYPTED;

    if ((pHeader = (tHeader *) SLL_FIRST (&pECPIf->OptionsAckedByLocal)) ==
        NULL)
    {

        PPP_DBG ("No Encryption Algo. negotiated");
        return;
    }

    if ((Index = GetECPOptionIndex (pHeader->Type)) == DISCARD_OPT)
    {

        return;
    }

    if ((*ECPFnPointer[Index].Encrypt) (&pGSEM->pOutParam, pECPIf->pOutEncIf) ==
        DISCARD)
    {
        PPP_DBG ("Encryption Failed ...");
        return;
    }

    pGSEM->MiscParam.CharValue = ENCRYPTED;
    PPP_DBG ("Encryption Successful...");

    return;
}

/**************************************************************************
*  Function Name : ECPRecdResetReq 
*  Description   :
*                This function return RRR event to the GCP module if the 
*                Reset_Req packet validation is successful.
*  Parameter(s)  :
*            pGSEM            - Pointer to GSEM structure.
*            pIn                - Incoming Buffer
*            Length            - Length of the Buffer
*            Id                - Identifier of the incoming packet
*
*  Return Values : INT1
***************************************************************************/
INT1
ECPRecdResetReq (tGSEM * pGSEM, t_MSG_DESC * pIn, UINT2 Length, UINT1 Id)
{

    UINT1               Type;
    INT2                Index;
    tHeader            *pHeader;
    tECPIf             *pECPIf;

    pECPIf = pGSEM->pIf->pECPIf;
    pHeader = (tHeader *) SLL_FIRST (&pECPIf->OptionsAckedByLocal);

    Type = pHeader->Type;
    pECPIf->Status.RxResetReqs++;

    if ((Index = GetECPOptionIndex (Type)) != DISCARD_OPT)
    {

        if (*ECPFnPointer[Index].ValidateResetPkt != NULL)
        {

            if ((*ECPFnPointer[Index].ValidateResetPkt) (Length, RESET_REQ_CODE)
                == OK)
            {

                pGSEM->pInParam = pIn;
                pGSEM->MiscParam.CharValue = Id;
                return RRR;

            }

            return DISCARD_OPT;
        }
        else
        {

            PPP_DBG ("Returning RRR.....");
            return RRR;
        }
    }

    return DISCARD_OPT;
}

/**************************************************************************
*  Function Name : ECPProcessResetReq
*  Description   :
*                This function receives the reset request, processes it and
*                sends the acknowledgement to the peer.
*  Parameter(s)  :
*            pGSEM            - Pointer to GSEM structure.
*
*  Return Values : VOID
***************************************************************************/
VOID
ECPProcessResetReq (tGSEM * pGSEM)
{
    tHeader            *pHeader;
    tECPIf             *pECPIf;
    /* Makefile changes - <unused var. Id> */
    INT2                Index;

    pECPIf = (tECPIf *) pGSEM->pIf->pECPIf;
    pHeader = (tHeader *) SLL_FIRST (&pECPIf->OptionsAckedByLocal);

    if ((Index = GetECPOptionIndex (pHeader->Type)) == DISCARD_OPT)
    {

        return;
    }

    if (*ECPFnPointer[Index].Reset != NULL)
    {

        (*ECPFnPointer[Index].Reset) (pHeader);
    }

    MOVE_OFFSET (pGSEM->pInParam,
                 pGSEM->pIf->LinkInfo.RxAddrCtrlLen +
                 pGSEM->pIf->LinkInfo.RxProtLen);

    ASSIGN1BYTE (pGSEM->pInParam, 0, RESET_ACK_CODE);
    pGSEM->pOutParam = pGSEM->pInParam;
    ReleaseFlag = PPP_NO;

    PPPLLITxPkt (pGSEM->pIf, pGSEM->pOutParam,
                 (UINT2) (VALID_BYTES (pGSEM->pOutParam)), pGSEM->Protocol);
    pECPIf->Status.TxResetAcks++;

}

/**************************************************************************
*  Function Name : ECPRecdResetAck 
*  Description   :
*                This function return RRA event to the GCP module if the
*                packet validation is successfull.
*  Parameter(s)  :
*            pGSEM            - Pointer to GSEM structure.
*            pIn                - Incoming Buffer
*            Length            - Length of the incoming buffer
*            Id                - Identifier of the incoming buffer
*
*  Return Values : INT1
***************************************************************************/
INT1
ECPRecdResetAck (tGSEM * pGSEM, t_MSG_DESC * pIn, UINT2 Length, UINT1 Id)
{
    UINT1               Type;
    INT2                Index;
    UINT2               Offset;
    tHeader            *pHeader;
    tECPIf             *pECPIf;

    pECPIf = (tECPIf *) pGSEM->pIf->pECPIf;
    if (pECPIf->Status.Id == Id)
    {

        pHeader = (tHeader *) SLL_FIRST (&pECPIf->OptionsAckedByLocal);
        Type = pHeader->Type;
        /* Makefile changes - <stmt has no effect > */
        /* RxResetAcks count should be incremented for each reset ack pkts 
         *  - Mani*/
        pECPIf->Status.RxResetAcks++;

        if ((Index = GetECPOptionIndex (Type)) != DISCARD_OPT)
        {

            /* Makefile changes - <too few arguments> */

            if ((*ECPFnPointer[Index].ValidateResetPkt) != NULL)
            {

                if ((*ECPFnPointer[Index].ValidateResetPkt)
                    (Length, RESET_ACK_CODE) == DISCARD_OPT)
                {
                    return DISCARD_OPT;
                }

                Offset =
                    (UINT2) (pGSEM->pIf->LinkInfo.RxAddrCtrlLen +
                             pGSEM->pIf->LinkInfo.RxProtLen + CP_HDR_LEN);

                if (((Length == 0)
                     && ((VALID_BYTES (pECPIf->pResetReq) - CP_HDR_LEN) == 0))
                    ||
                    (COMPARE_BUFS
                     (pIn, Offset, pECPIf->pResetReq, CP_HDR_LEN,
                      (VALID_BYTES (pIn) - Offset)) == 0))
                {

                    PPP_DBG ("Returning RRA...");
                    RELEASE_BUFFER (pECPIf->pResetReq);
                    return RRA;
                }
                else
                {
                    PPP_TRC (ALL_FAILURE, "COMPARE_BUFS FAILED");
                    RELEASE_BUFFER (pECPIf->pResetReq);
                    return DISCARD_OPT;
                }

            }
            return RRA;
        }
    }

    PPP_DBG ("Returning DISCARD_OPT...");
    return DISCARD_OPT;
}

/**************************************************************************
*  Function Name : ECPProcessResetAck
*  Description   :
*                This function processes the received Reset_Ack packet.
*
*  Parameter(s)  :
*            pGSEM        - Pointer to OptionAckedBye.
*
*  Return Values : UINT1
***************************************************************************/
VOID
ECPProcessResetAck (tGSEM * pGSEM)
{
    tHeader            *pHeader;
    tECPIf             *pECPIf;
    INT2                Index;

    pECPIf = (tECPIf *) pGSEM->pIf->pECPIf;
    pHeader = (tHeader *) SLL_FIRST (&pECPIf->OptionsAckedByPeer);

    if ((Index = GetECPOptionIndex (pHeader->Type)) == DISCARD_OPT)
    {

        return;
    }

    if (*ECPFnPointer[Index].Reset != NULL)
    {

        (*ECPFnPointer[Index].Reset) (pHeader);
    }
    pRestartGSEM = pGSEM;

}

/**************************************************************************
*  Function Name : GetECPOptionIndex 
*  Description   :
*                This function searches the FnPointer array of ECP for the
*                desired Type and returns the index, if it is found.
*
*  Parameter(s)  :
*            Type         - Type of the Option.
*
*  Return Values : INT2
***************************************************************************/
INT2
GetECPOptionIndex (UINT1 Type)
{
    INT2                LoopCount;

    for (LoopCount = 0; LoopCount < MAX_ECP_OPTIONS; LoopCount++)
    {

        if (ECPFnPointer[LoopCount].OptType == Type)
        {

            return LoopCount;
        }
    }

    return DISCARD_OPT;
}

/*********************************************************************
*  Function Name : ECPProcessConfRej
*  Description   :
*    This function is used to process the configuration reject for
*  ECP Options.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*  Return Value  : DISCARD/NO_CLOSE
*********************************************************************/
INT1
ECPProcessConfRej (tGSEM * pGSEM)
{
    INT2                Index;

    if (pGSEM->NumOptReq-- == 0)
    {
        pTermGSEM = &pGSEM->pIf->LcpGSEM;
        return (CLOSE_SEM);
    }

    if ((Index = GetNextOptionFromList (pGSEM, ECP_MOD)) == DISCARD_OPT)
    {

        pTermGSEM = &pGSEM->pIf->LcpGSEM;
        return (CLOSE_SEM);
    }
    pGSEM->pNegFlagsPerIf[Index].FlagMask |= ACKED_BY_PEER_MASK;

    return (NO_CLOSE_SEM);
}

/**************************************************************************
*  Function Name : ECPGetSEMPtr
*  Description   :
*                This function returns the pointer to the ECP GSEM from the
*                pIf.
*
*  Parameter(s)  :
*                pIf    - Poniter to pppIf structure.
*
*  Return Values : tGSEM *
***************************************************************************/
tGSEM              *
ECPGetSEMPtr (tPPPIf * pIf)
{
    tECPIf             *pECPIf;
        /******* SKR004 *************/
    if (pIf->pECPIf != NULL)
    {
        pECPIf = (tECPIf *) pIf->pECPIf;
        PPP_TRC (CONTROL_PLANE, "ECP:");
        return (&pECPIf->ECPGSEM);
    }
    return (NULL);
}

/*********************************************************************
*  Function Name : ECPCopyOptions 
*  Description   :
*    This function is used to process the configuration request for
*  ECP Options.
*  Input         :
*    pGSEM        -     Points to the GSEM structure
*    Flag        -     Specifies whether to alloc or free the temporary 
*                    variable.
*    PktType        -    Specifies whether the function is called from request
*                    processing or Nak processing.                  
*  Return Value        :
*                    DISCARD on allocation error, OK otherwise.
*********************************************************************/
INT1
ECPCopyOptions (tGSEM * pGSEM, UINT1 Flag, UINT1 PktType)
{
    t_SLL              *pTemp;
    tHeader            *pHeader;
    tHeader            *pTmp = NULL;
    static t_SLL        CopyList;
    tECPIf             *pECPIf;

    SLL_INIT (&CopyList);
    pECPIf = (tECPIf *) pGSEM->pIf->pECPIf;

    if (PktType == CONF_REQ_CODE)
    {
        pTemp = &pECPIf->OptionsAckedByLocal;
    }
    else
    {
        pTemp = &pECPIf->OptionsAckedByPeer;
    }

    if (Flag == NOT_SET)
    {

        SLL_SCAN (pTemp, pHeader, tHeader *)
        {
            if ((pTmp =
                 ((tHeader *) (ALLOC_ECP_PARAMS (pHeader->Length)))) == NULL)
            {
                PPP_TRC (ALL_FAILURE, "ALLOC_ECP_PARAMS failed...");
                /* Makefile changes - <return with no value> */
                return DISCARD;
            }

            MEMCPY (pTmp, pHeader, pHeader->Length);
            SLL_INIT_NODE (&pTmp->pNext);
            SLL_ADD (&CopyList, (t_SLL_NODE *) pTmp);
        }

        if (PktType == CONF_REQ_CODE)
        {
            /* Check if the nodes of the SLL are deleted. If not so, please delete
             * them and call the OptionInit()  - srini */
            DeleteNodes (&pECPIf->OptionsAckedByLocal);
            OptionInit (&pECPIf->OptionsAckedByLocal);
        }
    }
    else
    {
        if (Flag == SET)
        {
            SLL_CONCAT (pTemp, &CopyList);

        }
        DeleteNodes (&CopyList);
        SLL_INIT (&CopyList);
    }
    return (OK);
}

/*********************************************************************
*  Function Name : ECPUp
*  Description   :
*                This function intimates the higher layer is ECP is 
*                negotiated successfully. 
*  Parameter(s)  :
*           pECPGSEM  - pointer to the ECP tGSEM
*                                   corresponding to the current interface.
*  Return Values : VOID
*********************************************************************/
VOID
ECPUp (tGSEM * pECPGSEM)
{
    tHeader            *pHeader, *pHeader1;
    INT2                Index;
    tECPIf             *pECPIf;

    pECPIf = (tECPIf *) pECPGSEM->pIf->pECPIf;

    pECPIf->OperStatus = STATUS_UP;
    ECPDeleteUnAckedNodes (pECPGSEM);

    pHeader = (tHeader *) SLL_FIRST (&pECPIf->OptionsAckedByPeer);
    pHeader1 = (tHeader *) SLL_FIRST (&pECPIf->OptionsAckedByLocal);

    if (pHeader == NULL || pHeader1 == NULL)
    {
        PPP_TRC (CONTROL_PLANE, "No option negotiated.. LCP terminating");
        pTermGSEM = &pECPGSEM->pIf->LcpGSEM;
        return;
    }

    if ((Index = GetECPOptionIndex (pHeader->Type)) == DISCARD_OPT)
    {

        return;
    }

    PPP_TRC1 (PPP_MUST, " INTERFACE [%ld]: ", pECPGSEM->pIf->LinkInfo.IfIndex);
    PPP_TRC (PPP_MUST, " \t\tECP  State Changed to    UP\n");

    (*ECPFnPointer[Index].Init) ((VOID *) &pECPGSEM->pIf->
                                 LcpOptionsAckedByLocal.MRU,
                                 (VOID *) pHeader1, &pECPIf->pOutEncIf);

    (*ECPFnPointer[Index].Init) ((VOID *) &pECPGSEM->pIf->
                                 LcpOptionsAckedByPeer.MRU,
                                 (VOID *) pHeader, &pECPIf->pInEncIf);

}

/*********************************************************************
*  Function Name : ECPDown
*  Description   :
*  Parameter(s)  :
*           pECPGSEM  - pointer to the ECP GSEM
*                                   corresponding to the current interface.
*  Return Values : VOID
*********************************************************************/
VOID
ECPDown (tGSEM * pECPGSEM)
{

    INT2                Index;
    tHeader            *pHeader;
    tECPIf             *pECPIf;

    PPP_TRC1 (PPP_MUST, " INTERFACE [%ld]: ", pECPGSEM->pIf->LinkInfo.IfIndex);
    PPP_TRC (PPP_MUST, " \t\tECP  State Changed to  DOWN\n");

    pECPIf = (tECPIf *) pECPGSEM->pIf->pECPIf;
    pECPIf->OperStatus = STATUS_DOWN;
    pHeader = (tHeader *) SLL_FIRST (&pECPIf->OptionsAckedByPeer);

    if ((Index = GetECPOptionIndex (pHeader->Type)) == DISCARD_OPT)
    {
        return;
    }

    (*ECPFnPointer[Index].DeInit) (pECPIf->pInEncIf);
    (*ECPFnPointer[Index].DeInit) (pECPIf->pOutEncIf);
    pECPIf->pInEncIf = pECPIf->pOutEncIf = NULL;
    DeleteNodes (&pECPIf->OptionsAckedByPeer);
    ECPCopyDesiredToAckedByPeer (pECPIf);
    return;

}

/*********************************************************************
*  Function Name : DeleteUnAckedNodes 
*  Description   : This functions deletes the unacked nodes from the
*                    AckedByPeer 
*  Parameter(s)  :
*           pGSEM  - pointer to the ECP GSEM
*  Return Values : VOID
*********************************************************************/
VOID
ECPDeleteUnAckedNodes (tGSEM * pGSEM)
{
    tHeader            *pHeader, *ptmpHeader;
    INT2                LoopCount;
    tECPIf             *pECPIf;

    pECPIf = (tECPIf *) pGSEM->pIf->pECPIf;
    for (LoopCount = 0; LoopCount < MAX_ECP_OPTIONS; LoopCount++)
    {

        if ((pGSEM->pNegFlagsPerIf[LoopCount].FlagMask & ACKED_BY_PEER_MASK) ==
            NOT_SET)
        {
            SLL_SCAN (&pECPIf->OptionsAckedByPeer, pHeader, tHeader *)
            {

                if (pHeader->Type == pGSEM->pNegFlagsPerIf[LoopCount].OptType)
                {
                    ptmpHeader = pHeader;
                    pHeader =
                        (tHeader *) SLL_NEXT (&pECPIf->OptionsAckedByPeer,
                                              (t_SLL_NODE *) ptmpHeader);
                    SLL_DELETE (&pECPIf->OptionsAckedByPeer,
                                (t_SLL_NODE *) ptmpHeader);
                    PPP_DBG ("Node Deleted");
                }

            }

        }

    }

}

/*********************************************************************
*  Function Name : ECPCopyDesiredToAckedByPeer 
*  Description   : This functions  copies OptionsDesired to AckedByPeer
*  Parameter(s)  :
*           pECP - pointer to the ECP interface structure
*  Return Values : VOID
*********************************************************************/
VOID
ECPCopyDesiredToAckedByPeer (tECPIf * pECP)
{
    tHeader            *pHeader, *pTmp;
    INT2                Index;
    INT2                Count = 0;

    SLL_INIT (&pECP->OptionsAckedByPeer);
    SLL_SCAN (&pECP->OptionsDesired, pHeader, tHeader *)
    {

        if ((Index = GCPGetOptIndex (&pECP->ECPGSEM, pHeader->Type)) ==
            NOT_FOUND)
        {

            PPP_DBG1 ("pHeader->Type = %d\n", pHeader->Type);
            return;
        }

        if (Count == 0)
        {

            if (pECP->ECPGSEM.pNegFlagsPerIf[Index].FlagMask & DESIRED_SET)
            {

                pECP->ECPGSEM.pNegFlagsPerIf[Index].FlagMask |=
                    ACKED_BY_PEER_SET;
                Count++;
            }
        }

        if ((pTmp = (tHeader *) ALLOC_ECP_PARAMS (pHeader->Length)) == NULL)
        {
            PPP_TRC (OS_RESOURCE, " ALLOC_ECP_PARAMS failed...");
            return;
        }

        MEMCPY (pTmp, pHeader, pHeader->Length);
        SLL_INIT_NODE (&pTmp->pNext);
        SLL_ADD (&pECP->OptionsAckedByPeer, (t_SLL_NODE *) pTmp);
    }
}

/***********************************************************************/
/*                      INTERNAL  FUNCTIONS for 3DESE                  */
/***********************************************************************
*  Function Name : ECPProcess3DESEConfReq
*  Description   :
*          This function handles the config_request when Triple-DESE is
*  negotiated as Option. This function calls Triple-DESE function to validate
*  and verify the value of the parameters negotiated.
*
*  Parameter(s)  :
*        pGSEM        - Pointer to GSEM structure.
*        pInPkt        - Pointer to the incoming packet.
*        tOptVal        - Option value to be processed.    
*        Offset        - Indicates the place in the outgoing buffer
*                at which the option value is appended.
*        PresenceFlag    - Indicates whether the option is present in
*                the received CONFIG_REQ packet
*
*  Return Values : Returns the number of bytes appended in the outgoing 
*                    buffer
***************************************************************************/
INT1
ECPProcess3DESEConfReq (tGSEM * pGSEM, t_MSG_DESC * pIn, tOptVal OptVal,
                        UINT2 Offset, UINT1 PresenceFlag)
{
    tTrplDESEParams    *pTrplDESEAcked, *pTrplDESEDesired;
    tHeader            *pHeader;
    tECPIf             *pECPIf;

    PPP_UNUSED (pIn);
    PPP_UNUSED (OptVal);
    PPP_UNUSED (PresenceFlag);
    PPP_UNUSED (Offset);
    /* Makefile changes - <unused parameter> */

    pECPIf = (tECPIf *) pGSEM->pIf->pECPIf;
    SLL_SCAN (&pECPIf->OptionsDesired, pHeader, tHeader *)
    {

        if (pHeader->Type == OPTION_3DESE)
        {

            pTrplDESEDesired = (tTrplDESEParams *) pHeader;

            if ((pTrplDESEAcked =
                 (tTrplDESEParams *) ALLOC_TRIPLE_DESE_PARAMS ()) == NULL)
            {
                PPP_TRC (ALL_FAILURE, "ALLOC_3DESE_PARAMS failed");
                return DISCARD_OPT;
            }

            pTrplDESEAcked->Header.Type = OPTION_3DESE;
            pTrplDESEAcked->Header.Length = sizeof (tTrplDESEParams);

            MEMCPY (pTrplDESEAcked->Nonce, OptVal.StrVal, NONCE_LEN);
            MEMCPY (pTrplDESEAcked->Key1, pTrplDESEDesired->Key1, KEY_LENGTH);
            MEMCPY (pTrplDESEAcked->Key2, pTrplDESEDesired->Key2, KEY_LENGTH);
            MEMCPY (pTrplDESEAcked->Key3, pTrplDESEDesired->Key3, KEY_LENGTH);
            SLL_ADD (&pECPIf->OptionsAckedByLocal,
                     (t_SLL_NODE *) & pTrplDESEAcked->Header);

        }
    }
    return ACK;
}

/**************************************************************************
*  Function Name : ECPProcess3DESEConfNak
*  Description   :
*          This function handles the config_Nak when Triple DESE is
*  negotiated as Option. This function copies the Nacked values into
*  AckedByPeer structure.
*
*  Parameter(s)  :
*        pGSEM            - Pointer to GSEM structure.
*        pInPkt            - Pointer to the incoming packet.
*        tOptVal            - Option value to be processed.    
*
*  Return Values : DISCARD_OPT
***************************************************************************/

INT1
ECPProcess3DESEConfNak (tGSEM * pGSEM, t_MSG_DESC * pIn, tOptVal OptVal)
{

    PPP_UNUSED (pGSEM);
    PPP_UNUSED (pIn);
    PPP_UNUSED (OptVal);
    return DISCARD_OPT;
}

/**************************************************************************
*  Function Name : ECPReturn3DESEAddrPtr
*  Description   :
*      This function returns the address of the Triple-DESE Parameters.
*
*  Parameter(s)  :
*    pGSEM            - Pointer to GSEM structure.
*    OptLen            - Pointer to total Length of the parameters.
*
*  Return Values : VOID *, the pointer to Triple DESE Parameters.
***************************************************************************/
VOID               *
ECPReturn3DESEAddrPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    tTrplDESEParams    *pTrplDESEParams;
    tHeader            *pHeader;
    tECPIf             *pECPIf;

    pECPIf = (tECPIf *) pGSEM->pIf->pECPIf;

    SLL_SCAN (&pECPIf->OptionsAckedByPeer, pHeader, tHeader *)
    {

        if (pHeader->Type == OPTION_3DESE)
        {

            pTrplDESEParams = (tTrplDESEParams *) pHeader;

            *OptLen = NONCE_LEN;

            return (VOID *) (pTrplDESEParams->Nonce);
        }
    }
    /* Makefile changes - <control reaches non-void fn> */
    return (VOID *) NULL;

}
