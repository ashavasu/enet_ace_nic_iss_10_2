/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppecpll.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains low level routines for ECP module
 *
 *******************************************************************/
#include "pppsnmpm.h"
#include "ecpcom.h"
#include "pppecpmd.h"
#include "snmccons.h"            /* Mani */
#include "gsemdefs.h"
#include "pppdes.h"
#include "desproto.h"
#include "llproto.h"
#include "globexts.h"
#include "ppecplow.h"

#define ECP_RX_ADMIN_STATUS_CLOSED 2
#define ECP_RX_ADMIN_STATUS_OPENED 1

VOID                InsertEcpNode (tECPIf * pECPIf, tHeader * pCurrentHeader,
                                   UINT4 Priority);
VOID                ECPInsertNode (tECPIf * pECPIf, tHeader * pCurrentHeader,
                                   UINT4 Priority);

/****************** Start of Low Level get routines for pppECPConfig*************/

INT1
CheckAndGetECPTxPtr (tPPPIf * pIf, UINT4 *n1, UINT4 *n2, UINT4 SecondIndex)
{
    tHeader            *pHeader;
    tECPIf             *pECPIf;

    if (pIf->pECPIf != NULL)
    {

        pECPIf = (tECPIf *) pIf->pECPIf;

        SLL_SCAN (&pECPIf->OptionsDesired, pHeader, tHeader *)
        {
            if ((pHeader != NULL) && (pHeader->Priority >= SecondIndex)
                && (pHeader->Priority < *n2))
            {
                *n2 = pHeader->Priority;
                *n1 = pIf->LinkInfo.IfIndex;
                return (PPP_SNMP_OK);
            }
            if (pHeader == NULL)
            {
                break;
            }
        }
    }
    return (PPP_SNMP_ERR);
}

INT1
CheckAndGetECPRxPtr (tPPPIf * pIf, UINT4 *n1, UINT4 *n2, UINT4 SecondIndex)
{
    tHeader            *pHeader;
    tECPIf             *pECPIf;

    if (pIf->pECPIf != NULL)
    {

        pECPIf = (tECPIf *) pIf->pECPIf;

        SLL_SCAN (&pECPIf->OptionsAllowedForPeer, pHeader, tHeader *)
        {
            if ((pHeader != NULL) && (pHeader->Type >= SecondIndex)
                && (pHeader->Type < *n2))
            {
                *n2 = pHeader->Type;
                *n1 = pIf->LinkInfo.IfIndex;
                return (PPP_SNMP_OK);
            }
            if (pHeader == NULL)
            {
                break;
            }
        }
    }
    return (PPP_SNMP_ERR);
}

INT1
CheckAndGetECPPtr (tPPPIf * pIf)
{
    if (pIf->pECPIf != NULL)
    {
        return (PPP_SNMP_OK);
    }
    return (PPP_SNMP_ERR);
}

/**************************************************************************/
tECPIf             *
SNMPGetECPIfPtr (Index)
     UINT4               Index;
{
    tPPPIf             *pIf;

    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if ((pIf != NULL) && (pIf->LinkInfo.IfIndex == Index))
        {
            return (pIf->pECPIf);
        }
    }
    return ((tECPIf *) NULL);
}

tECPIf             *
SNMPGetOrCreateECPIfPtr (Index)
     UINT4               Index;
{
    tPPPIf             *pIf;
    tPPPIf             *pMemberIf;
    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if ((pIf != NULL) && (pIf->LinkInfo.IfIndex == Index))
        {
            if (pIf->BundleFlag != BUNDLE
                && pIf->MPInfo.MemberInfo.pBundlePtr != NULL
                && pIf->MPInfo.MemberInfo.pBundlePtr->pECPIf != NULL)
            {
                return (NULL);
            }
            if (pIf->BundleFlag == BUNDLE)
            {
                SLL_SCAN (&PPPIfList, pMemberIf, tPPPIf *)
                {
                    if (pMemberIf->BundleFlag != BUNDLE
                        && pMemberIf->MPInfo.MemberInfo.pBundlePtr == pIf)
                    {
                        if (pMemberIf->pECPIf != NULL)
                        {
                            return (NULL);
                        }
                    }
                }
            }

            if (pIf->pECPIf == NULL)
            {
                if ((pIf->pECPIf = (VOID *) ECPCreateIf (pIf)) == NULL)
                {
                    return (NULL);
                }
            }
            return (tECPIf *) (pIf->pECPIf);
        }
    }
    return (NULL);
}

/* LOW LEVEL Routines for Table : PppECPConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppECPConfigTable
 Input       :  The Indices
                PppECPConfigIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppECPConfigTable (INT4 i4PppECPConfigIfIndex)
{
    if (SNMPGetECPIfPtr (i4PppECPConfigIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppECPConfigTable
 Input       :  The Indices
                PppECPConfigIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppECPConfigTable (INT4 *pi4PppECPConfigIfIndex)
{
    if (nmhGetFirstIndex (pi4PppECPConfigIfIndex, ECPInstance, FIRST_INST) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppECPConfigTable
 Input       :  The Indices
                PppECPConfigIfIndex
                nextPppECPConfigIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppECPConfigTable (INT4 i4PppECPConfigIfIndex,
                                  INT4 *pi4NextPppECPConfigIfIndex)
{
    if (nmhGetNextIndex (&i4PppECPConfigIfIndex, ECPInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextPppECPConfigIfIndex = i4PppECPConfigIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppECPConfigAdminStatus
 Input       :  The Indices
                PppECPConfigIfIndex

                The Object 
                retValPppECPConfigAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppECPConfigAdminStatus (INT4 i4PppECPConfigIfIndex,
                               INT4 *pi4RetValPppECPConfigAdminStatus)
{
    tECPIf             *pECPIf;

    if ((pECPIf = SNMPGetOrCreateECPIfPtr (i4PppECPConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pECPIf->AdminStatus = (UINT1) *pi4RetValPppECPConfigAdminStatus;
    if (*pi4RetValPppECPConfigAdminStatus == ADMIN_OPEN)
    {
        ECPEnableIf (pECPIf);
    }
    else
    {
        ECPDisableIf (pECPIf);
    }
    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppECPConfigAdminStatus
 Input       :  The Indices
                PppECPConfigIfIndex

                The Object 
                setValPppECPConfigAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppECPConfigAdminStatus (INT4 i4PppECPConfigIfIndex,
                               INT4 i4SetValPppECPConfigAdminStatus)
{
    tECPIf             *pECPIf;

    if ((pECPIf = SNMPGetOrCreateECPIfPtr (i4PppECPConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pECPIf->AdminStatus = (UINT1) i4SetValPppECPConfigAdminStatus;
    if (i4SetValPppECPConfigAdminStatus == ADMIN_OPEN)
    {
        ECPEnableIf (pECPIf);
    }
    else
    {
        ECPDisableIf (pECPIf);
    }
    return (SNMP_SUCCESS);

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppECPConfigAdminStatus
 Input       :  The Indices
                PppECPConfigIfIndex

                The Object 
                testValPppECPConfigAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppECPConfigAdminStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4PppECPConfigIfIndex,
                                  INT4 i4TestValPppECPConfigAdminStatus)
{
    PPP_UNUSED (i4PppECPConfigIfIndex);
    if (i4TestValPppECPConfigAdminStatus != ADMIN_OPEN
        && i4TestValPppECPConfigAdminStatus != ADMIN_CLOSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/* LOW LEVEL Routines for Table : PppECPTxConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppECPTxConfigTable
 Input       :  The Indices
                PppECPTxConfigIfIndex
                PppECPTxConfigPriority
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppECPTxConfigTable (INT4 i4PppECPTxConfigIfIndex,
                                             INT4 i4PppECPTxConfigPriority)
{
    tECPIf             *pECPIf;
    tHeader            *pHeader;

    if ((pECPIf = SNMPGetECPIfPtr (i4PppECPTxConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    SLL_SCAN (&pECPIf->OptionsDesired, pHeader, tHeader *)
    {
        if (pHeader->Priority == (UINT4) i4PppECPTxConfigPriority)
        {
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppECPTxConfigTable
 Input       :  The Indices
                PppECPTxConfigIfIndex
                PppECPTxConfigPriority
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppECPTxConfigTable (INT4 *pi4PppECPTxConfigIfIndex,
                                     INT4 *pi4PppECPTxConfigPriority)
{
    if (nmhGetFirstDoubleIndex
        (pi4PppECPTxConfigIfIndex, pi4PppECPTxConfigPriority, ECPTxInstance,
         FIRST_INST) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppECPTxConfigTable
 Input       :  The Indices
                PppECPTxConfigIfIndex
                nextPppECPTxConfigIfIndex
                PppECPTxConfigPriority
                nextPppECPTxConfigPriority
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppECPTxConfigTable (INT4 i4PppECPTxConfigIfIndex,
                                    INT4 *pi4NextPppECPTxConfigIfIndex,
                                    INT4 i4PppECPTxConfigPriority,
                                    INT4 *pi4NextPppECPTxConfigPriority)
{
    if (nmhGetNextDoubleIndex
        (&i4PppECPTxConfigIfIndex, &i4PppECPTxConfigPriority, ECPTxInstance,
         NEXT_INST) == SNMP_SUCCESS)
    {
        *pi4NextPppECPTxConfigIfIndex = i4PppECPTxConfigIfIndex;
        *pi4NextPppECPTxConfigPriority = i4PppECPTxConfigPriority;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppECPTxConfigOptionNum
 Input       :  The Indices
                PppECPTxConfigIfIndex
                PppECPTxConfigPriority

                The Object 
                retValPppECPTxConfigOptionNum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppECPTxConfigOptionNum (INT4 i4PppECPTxConfigIfIndex,
                               INT4 i4PppECPTxConfigPriority,
                               INT4 *pi4RetValPppECPTxConfigOptionNum)
{
    tECPIf             *pECPIf;
    tHeader            *pHeader;

    if ((pECPIf = SNMPGetECPIfPtr (i4PppECPTxConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    SLL_SCAN (&pECPIf->OptionsDesired, pHeader, tHeader *)
    {
        if (pHeader->Priority == (UINT4) i4PppECPTxConfigPriority)
        {
            *pi4RetValPppECPTxConfigOptionNum = pHeader->Type;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppECPTxConfigOptionNum
 Input       :  The Indices
                PppECPTxConfigIfIndex
                PppECPTxConfigPriority

                The Object 
                setValPppECPTxConfigOptionNum
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppECPTxConfigOptionNum (INT4 i4PppECPTxConfigIfIndex,
                               INT4 i4PppECPTxConfigPriority,
                               INT4 i4SetValPppECPTxConfigOptionNum)
{
    tECPIf             *pECPIf;
    tHeader            *pHeader;
    t_SLL_NODE         *pNextHeader;
    tDESEParams        *pDesPtr;
    UINT1               GenOptIndex;
    tTrplDESEParams    *pTrplDes;

    pECPIf = NULL;
    pHeader = NULL;

    if ((pECPIf = SNMPGetOrCreateECPIfPtr (i4PppECPTxConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pHeader = (tHeader *) SLL_FIRST (&pECPIf->OptionsDesired);
    while (pHeader != NULL)
    {
        pNextHeader =
            SLL_NEXT (&pECPIf->OptionsDesired, (t_SLL_NODE *) pHeader);
        if ((pHeader->Priority == (UINT4) i4PppECPTxConfigPriority)
            && (pHeader->Type == i4SetValPppECPTxConfigOptionNum))
        {
            return (SNMP_SUCCESS);
        }
        if ((pHeader->Priority == (UINT4) i4PppECPTxConfigPriority)
            || (pHeader->Type == i4SetValPppECPTxConfigOptionNum))
        {
            switch (pHeader->Type)
            {
                case OPTION_DESE:
                    GenOptIndex =
                        GCPGetOptIndex (&pECPIf->ECPGSEM, OPTION_DESE);
                    break;
                case OPTION_3DESE:
                    GenOptIndex =
                        GCPGetOptIndex (&pECPIf->ECPGSEM, OPTION_3DESE);
                    break;
                default:
                    return (SNMP_FAILURE);
            }
            pECPIf->ECPGSEM.pNegFlagsPerIf[GenOptIndex].FlagMask &=
                DESIRED_NOT_SET;
            pECPIf->ECPGSEM.pNegFlagsPerIf[GenOptIndex].FlagMask &=
                ACKED_BY_PEER_NOT_SET;
            SLL_DELETE (&pECPIf->OptionsDesired, (t_SLL_NODE *) pHeader);
            MEM_FREE (pHeader);    /* Check for algo. specific freeing tobe done 
                                   Mahesh */
            break;

        }
        pHeader = (tHeader *) pNextHeader;
    }

    if (i4SetValPppECPTxConfigOptionNum != 0)
    {
        switch (i4SetValPppECPTxConfigOptionNum)
        {
            case OPTION_DESE:
                pDesPtr = DESECreateIf ((const UINT1 *) "\0\0\0\0\0\0\0");
                pDesPtr->Header.Priority = i4PppECPTxConfigPriority;
                InsertEcpNode (pECPIf, &pDesPtr->Header,
                               i4PppECPTxConfigPriority);
                pECPIf->ECPGSEM.pNegFlagsPerIf[DESE_IDX].FlagMask |=
                    DESIRED_SET;
                break;
            case OPTION_3DESE:
                pTrplDes =
                    TripleDESECreateIf ((const UINT1 *) "\0\0\0\0\0\0\0",
                                        (const UINT1 *) "\0\0\0\0\0\0\0",
                                        (const UINT1 *) "\0\0\0\0\0\0\0");
                pTrplDes->Header.Priority = i4PppECPTxConfigPriority;
                InsertEcpNode (pECPIf, &pTrplDes->Header,
                               i4PppECPTxConfigPriority);
                pECPIf->ECPGSEM.pNegFlagsPerIf[TRPL_DESE_IDX].FlagMask |=
                    DESIRED_SET;
                break;
            default:
                return (SNMP_FAILURE);
        }
    }
    /* How to Handle Value 0? Mahesh */
    return (SNMP_SUCCESS);

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppECPTxConfigOptionNum
 Input       :  The Indices
                PppECPTxConfigIfIndex
                PppECPTxConfigPriority

                The Object 
                testValPppECPTxConfigOptionNum
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppECPTxConfigOptionNum (UINT4 *pu4ErrorCode,
                                  INT4 i4PppECPTxConfigIfIndex,
                                  INT4 i4PppECPTxConfigPriority,
                                  INT4 i4TestValPppECPTxConfigOptionNum)
{
    PPP_UNUSED (i4PppECPTxConfigIfIndex);
    PPP_UNUSED (i4PppECPTxConfigPriority);
    switch (i4TestValPppECPTxConfigOptionNum)
    {
        case OPTION_DESE:
        case 0:
        case OPTION_3DESE:
        case OPTION_UNUSED:
            return (SNMP_SUCCESS);
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (SNMP_FAILURE);
    }

}

VOID
InsertEcpNode (tECPIf * pECPIf, tHeader * pCurrentHeader, UINT4 Priority)
{
    tHeader            *pHeader;
    t_SLL_NODE         *pPrevHeader;

    SLL_SCAN (&pECPIf->OptionsDesired, pHeader, tHeader *)
    {
        if (Priority < pHeader->Priority)
        {
            break;
        }
    }
    pPrevHeader = SLL_PREV (&pECPIf->OptionsDesired, (t_SLL_NODE *) pHeader);
    SLL_INSERT (&pECPIf->OptionsDesired, pPrevHeader,
                (t_SLL_NODE *) pCurrentHeader);
}

/* LOW LEVEL Routines for Table : PppECPRxConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppECPRxConfigTable
 Input       :  The Indices
                PppECPRxConfigIfIndex
                PppECPRxConfigOptionNum
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppECPRxConfigTable (INT4 i4PppECPRxConfigIfIndex,
                                             INT4 i4PppECPRxConfigOptionNum)
{
    tECPIf             *pECPIf;
    tHeader            *pHeader;

    if ((pECPIf = SNMPGetECPIfPtr (i4PppECPRxConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    SLL_SCAN (&pECPIf->OptionsAllowedForPeer, pHeader, tHeader *)
    {
        if (pHeader->Type == i4PppECPRxConfigOptionNum)
        {
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);

}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppECPRxConfigTable
 Input       :  The Indices
                PppECPRxConfigIfIndex
                PppECPRxConfigOptionNum
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppECPRxConfigTable (INT4 *pi4PppECPRxConfigIfIndex,
                                     INT4 *pi4PppECPRxConfigOptionNum)
{
    if (nmhGetFirstDoubleIndex
        (pi4PppECPRxConfigIfIndex, pi4PppECPRxConfigOptionNum, ECPRxInstance,
         FIRST_INST) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppECPRxConfigTable
 Input       :  The Indices
                PppECPRxConfigIfIndex
                nextPppECPRxConfigIfIndex
                PppECPRxConfigOptionNum
                nextPppECPRxConfigOptionNum
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppECPRxConfigTable (INT4 i4PppECPRxConfigIfIndex,
                                    INT4 *pi4NextPppECPRxConfigIfIndex,
                                    INT4 i4PppECPRxConfigOptionNum,
                                    INT4 *pi4NextPppECPRxConfigOptionNum)
{
    if (nmhGetNextDoubleIndex
        (&i4PppECPRxConfigIfIndex, &i4PppECPRxConfigOptionNum, ECPRxInstance,
         NEXT_INST) == SNMP_SUCCESS)
    {
        *pi4NextPppECPRxConfigIfIndex = i4PppECPRxConfigIfIndex;
        *pi4NextPppECPRxConfigOptionNum = i4PppECPRxConfigOptionNum;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppECPRxConfigAdminStatus
 Input       :  The Indices
                PppECPRxConfigIfIndex
                PppECPRxConfigOptionNum

                The Object 
                retValPppECPRxConfigAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppECPRxConfigAdminStatus (INT4 i4PppECPRxConfigIfIndex,
                                 INT4 i4PppECPRxConfigOptionNum,
                                 INT4 *pi4RetValPppECPRxConfigAdminStatus)
{
    tECPIf             *pECPIf;
    tHeader            *pHeader;

    if ((pECPIf = SNMPGetECPIfPtr (i4PppECPRxConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    SLL_SCAN (&pECPIf->OptionsAllowedForPeer, pHeader, tHeader *)
    {
        if (pHeader->Type == i4PppECPRxConfigOptionNum)
        {
            *pi4RetValPppECPRxConfigAdminStatus = ECP_RX_ADMIN_STATUS_OPENED;
            return (SNMP_SUCCESS);
        }
    }
    *pi4RetValPppECPRxConfigAdminStatus = ECP_RX_ADMIN_STATUS_CLOSED;
    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppECPRxConfigAdminStatus
 Input       :  The Indices
                PppECPRxConfigIfIndex
                PppECPRxConfigOptionNum

                The Object 
                setValPppECPRxConfigAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppECPRxConfigAdminStatus (INT4 i4PppECPRxConfigIfIndex,
                                 INT4 i4PppECPRxConfigOptionNum,
                                 INT4 i4SetValPppECPRxConfigAdminStatus)
{
    tECPIf             *pECPIf;
    tHeader            *pHeader;
    tDESEParams        *pDesPtr;
    UINT1               GenOptIndex;
    tTrplDESEParams    *pTrplDes;

    pECPIf = NULL;
    pHeader = NULL;

    if ((pECPIf = SNMPGetOrCreateECPIfPtr (i4PppECPRxConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (i4SetValPppECPRxConfigAdminStatus == ECP_RX_ADMIN_STATUS_OPENED)
    {
        SLL_SCAN (&pECPIf->OptionsAllowedForPeer, pHeader, tHeader *)
        {
            if (pHeader->Type == i4PppECPRxConfigOptionNum)
            {
                return (SNMP_SUCCESS);
            }
        }
        switch (i4PppECPRxConfigOptionNum)
        {
            case OPTION_DESE:
                pDesPtr = DESECreateIf ((const UINT1 *) "\0\0\0\0\0\0\0");
                SLL_ADD (&pECPIf->OptionsAllowedForPeer,
                         (t_SLL_NODE *) & pDesPtr->Header);
                pECPIf->ECPGSEM.pNegFlagsPerIf[DESE_IDX].FlagMask |=
                    ALLOWED_FOR_PEER_SET;
                break;
            case OPTION_3DESE:
                pTrplDes =
                    TripleDESECreateIf ((const UINT1 *) "\0\0\0\0\0\0\0",
                                        (const UINT1 *) "\0\0\0\0\0\0\0",
                                        (const UINT1 *) "\0\0\0\0\0\0\0");
                SLL_ADD (&pECPIf->OptionsAllowedForPeer,
                         (t_SLL_NODE *) & pTrplDes->Header);
                pECPIf->ECPGSEM.pNegFlagsPerIf[TRPL_DESE_IDX].FlagMask |=
                    ALLOWED_FOR_PEER_SET;
                break;
            case OPTION_UNUSED:
                pHeader = (tHeader *) PPP_MALLOC (sizeof (tHeader));
                pHeader->Type = OPTION_UNUSED;
                pHeader->Length = sizeof (tHeader);
                SLL_ADD (&pECPIf->OptionsAllowedForPeer,
                         (t_SLL_NODE *) pHeader);
                pECPIf->ECPGSEM.pNegFlagsPerIf[DUMMY_IDX].FlagMask |=
                    ALLOWED_FOR_PEER_SET;
                break;
        }
    }
    else
    {
        SLL_SCAN (&pECPIf->OptionsAllowedForPeer, pHeader, tHeader *)
        {
            if (pHeader->Type == i4PppECPRxConfigOptionNum)
            {
                SLL_DELETE (&pECPIf->OptionsAllowedForPeer,
                            (t_SLL_NODE *) pHeader);
                GenOptIndex =
                    GCPGetOptIndex (&pECPIf->ECPGSEM,
                                    (UINT1) (i4PppECPRxConfigOptionNum));
                pECPIf->ECPGSEM.pNegFlagsPerIf[GenOptIndex].FlagMask &=
                    ALLOWED_FOR_PEER_NOT_SET;
                break;
            }
        }
    }

    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppECPRxConfigAdminStatus
 Input       :  The Indices
                PppECPRxConfigIfIndex
                PppECPRxConfigOptionNum

                The Object 
                testValPppECPRxConfigAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppECPRxConfigAdminStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4PppECPRxConfigIfIndex,
                                    INT4 i4PppECPRxConfigOptionNum,
                                    INT4 i4TestValPppECPRxConfigAdminStatus)
{

    INT1                Found;
    PPP_UNUSED (i4PppECPRxConfigOptionNum);
    Found = FALSE;
    PPP_UNUSED (i4PppECPRxConfigIfIndex);
    PPP_UNUSED (i4PppECPRxConfigOptionNum);
    if (i4TestValPppECPRxConfigAdminStatus != ECP_RX_ADMIN_STATUS_OPENED
        && i4TestValPppECPRxConfigAdminStatus != ECP_RX_ADMIN_STATUS_CLOSED)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : PppECPStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppECPStatsTable
 Input       :  The Indices
                PppECPStatsIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppECPStatsTable (INT4 i4PppECPStatsIfIndex)
{
    if (SNMPGetECPIfPtr (i4PppECPStatsIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppECPStatsTable
 Input       :  The Indices
                PppECPStatsIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppECPStatsTable (INT4 *pi4PppECPStatsIfIndex)
{
    if (nmhGetFirstIndex (pi4PppECPStatsIfIndex, ECPInstance, FIRST_INST) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppECPStatsTable
 Input       :  The Indices
                PppECPStatsIfIndex
                nextPppECPStatsIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppECPStatsTable (INT4 i4PppECPStatsIfIndex,
                                 INT4 *pi4NextPppECPStatsIfIndex)
{
    if (nmhGetNextIndex (&i4PppECPStatsIfIndex, ECPInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextPppECPStatsIfIndex = i4PppECPStatsIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppECPStatsOperStatus
 Input       :  The Indices
                PppECPStatsIfIndex

                The Object 
                retValPppECPStatsOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppECPStatsOperStatus (INT4 i4PppECPStatsIfIndex,
                             INT4 *pi4RetValPppECPStatsOperStatus)
{
    tECPIf             *pECPIf;

    if ((pECPIf = SNMPGetECPIfPtr (i4PppECPStatsIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pECPIf->OperStatus == STATUS_UP)
    {
        *pi4RetValPppECPStatsOperStatus = 1;
    }
    else
    {
        *pi4RetValPppECPStatsOperStatus = 2;
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppECPStatsRxResetReqs
 Input       :  The Indices
                PppECPStatsIfIndex

                The Object 
                retValPppECPStatsRxResetReqs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppECPStatsRxResetReqs (INT4 i4PppECPStatsIfIndex,
                              INT4 *pi4RetValPppECPStatsRxResetReqs)
{
    tECPIf             *pECPIf;

    if ((pECPIf = SNMPGetECPIfPtr (i4PppECPStatsIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppECPStatsRxResetReqs = pECPIf->Status.RxResetReqs;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppECPStatsTxResetReqs
 Input       :  The Indices
                PppECPStatsIfIndex

                The Object 
                retValPppECPStatsTxResetReqs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppECPStatsTxResetReqs (INT4 i4PppECPStatsIfIndex,
                              INT4 *pi4RetValPppECPStatsTxResetReqs)
{
    tECPIf             *pECPIf;

    if ((pECPIf = SNMPGetECPIfPtr (i4PppECPStatsIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppECPStatsTxResetReqs = pECPIf->Status.TxResetReqs;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppECPStatsRxResetAcks
 Input       :  The Indices
                PppECPStatsIfIndex

                The Object 
                retValPppECPStatsRxResetAcks
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppECPStatsRxResetAcks (INT4 i4PppECPStatsIfIndex,
                              INT4 *pi4RetValPppECPStatsRxResetAcks)
{
    tECPIf             *pECPIf;

    if ((pECPIf = SNMPGetECPIfPtr (i4PppECPStatsIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppECPStatsRxResetAcks = pECPIf->Status.RxResetAcks;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppECPStatsTxResetAcks
 Input       :  The Indices
                PppECPStatsIfIndex

                The Object 
                retValPppECPStatsTxResetAcks
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppECPStatsTxResetAcks (INT4 i4PppECPStatsIfIndex,
                              INT4 *pi4RetValPppECPStatsTxResetAcks)
{
    tECPIf             *pECPIf;

    if ((pECPIf = SNMPGetECPIfPtr (i4PppECPStatsIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppECPStatsTxResetAcks = pECPIf->Status.TxResetAcks;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppECPStatsTxNegotiatedOption
 Input       :  The Indices
                PppECPStatsIfIndex

                The Object 
                retValPppECPStatsTxNegotiatedOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppECPStatsTxNegotiatedOption (INT4 i4PppECPStatsIfIndex,
                                     INT4
                                     *pi4RetValPppECPStatsTxNegotiatedOption)
{
    tECPIf             *pECPIf;
    tHeader            *pHeader;

    if ((pECPIf = SNMPGetECPIfPtr (i4PppECPStatsIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    SLL_SCAN (&pECPIf->OptionsAckedByLocal, pHeader, tHeader *)
    {
        *pi4RetValPppECPStatsTxNegotiatedOption = pHeader->Type;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);

}

/****************************************************************************
 Function    :  nmhGetPppECPStatsRxNegotiatedOption
 Input       :  The Indices
                PppECPStatsIfIndex

                The Object 
                retValPppECPStatsRxNegotiatedOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppECPStatsRxNegotiatedOption (INT4 i4PppECPStatsIfIndex,
                                     INT4
                                     *pi4RetValPppECPStatsRxNegotiatedOption)
{
    tECPIf             *pECPIf;
    tHeader            *pHeader;

    if ((pECPIf = SNMPGetECPIfPtr (i4PppECPStatsIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    SLL_SCAN (&pECPIf->OptionsAckedByPeer, pHeader, tHeader *)
    {
        *pi4RetValPppECPStatsRxNegotiatedOption = pHeader->Type;
        return (SNMP_SUCCESS);
    }

    return (SNMP_FAILURE);

}

VOID
ECPInsertNode (tECPIf * pECPIf, tHeader * pCurrentHeader, UINT4 Priority)
{
    tHeader            *pHeader;
    t_SLL_NODE         *pPrevHeader;

    SLL_SCAN (&pECPIf->OptionsDesired, pHeader, tHeader *)
    {
        if (Priority < pHeader->Priority)
        {
            break;
        }
    }
    pPrevHeader = SLL_PREV (&pECPIf->OptionsDesired, (t_SLL_NODE *) pHeader);
    SLL_INSERT (&pECPIf->OptionsDesired, (t_SLL_NODE *) pPrevHeader,
                (t_SLL_NODE *) pCurrentHeader);
}
