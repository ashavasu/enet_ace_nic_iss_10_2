/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppecpds.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains routines for DESE Encryption 
 *             Option of ECP.
 *
 *******************************************************************/

#include "pppcom.h"

#include "pppdes.h"
#include "pppccp.h"
#include "pppecp.h"

static UINT2        DESESequenceNumber;

/**************************************************************************
*  Function Name : ConstructDESEResetPkt
*  Description   :
*              This function constructs the Reset packet that is specific
*              to DESE encryption Algorithm. The packet can be Reset_Req
*              or Reset_Ack depeding on the Code (which is passed as parameter).
*                
*  Parameter(s)  :
*            pOut    -  Output Buffer
*            pOption -  Poniter to the Stac Parameters Acked. by the Peer.
*            Id        -  Packet Identifier
*            Code    -  Indicating what packet to construct (Reset_Req/Reset/Ack)
*
*  Return Values : VOID
***************************************************************************/
VOID
ConstructDESEResetPkt (t_MSG_DESC * pOut, t_SLL * pOption, UINT1 Id, UINT1 Code)
{

    tDESEParams        *pDESEParams;

    pDESEParams = (tDESEParams *) SLL_FIRST (pOption);

    if (Code == RESET_REQ_CODE)
    {

        ASSIGN1BYTE (pOut, TYPE_OFFSET, RESET_REQ_CODE);

    }
    else
    {

        ASSIGN1BYTE (pOut, TYPE_OFFSET, RESET_ACK_CODE);
    }

    APPEND1BYTE (pOut, Id);
    APPEND2BYTE (pOut, DESE_SEQ_NUM_LENGTH);
    APPEND2BYTE (pOut, DESESequenceNumber);    /* DESESequenceNumber is a static variable */
}

/**************************************************************************
*  Function Name : ValidateDESEResetReq
*  Description   :
*                This function validates the length of the Reset_Req pkt of 
*                the DESE Algorithm.
*  Parameter(s)  :
*            Length        - Length of the Reset_Req packet.
*
*  Return Values : INT1
***************************************************************************/
INT1
ValidateStacResetReq (UINT2 Length)
{
    INT1                RetVal;

    RetVal = (Length == DESE_RESET_PKT_LENGTH) ? OK : NOT_OK;

    return RetVal;
}

/**************************************************************************
*  Function Name : ValidateDESEResetAck 
*  Description   :
*                This function validates the length and Id of the Reset_Ack
*                packet for the DESE.
*  Parameter(s)  :
*            pECPIf            - Pointer to ECPIf structure.
*            Length            - Length of the incoming buffer
*            Id                - Identifier of the incoming buffer
*
*  Return Values : UINT1
***************************************************************************/
INT1
ValidateDESEResetAck (tECPIf * pECPIf, UINT2 Length, UINT1 Id)
{

    if (Length == DESE_RESET_PKT_LENGTH && Id == pECPIf->Status.Id)
    {

        return OK;
    }

    return DISCARD_OPT;
}
