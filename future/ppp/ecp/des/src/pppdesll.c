/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppdesll.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description: Low Level Routines for DES module.
 *
 *******************************************************************/
#include "pppsnmpm.h"
#include "genhdrs.h"
#include "pppmacro.h"
#include "pppdefs.h"
#include "pppdebug.h"
#include "pppdebug.h"
#include "ppptimer.h"
#include "pppeccmn.h"
#include "pppecpmd.h"
#include "pppdes.h"
#include "pppdbgex.h"
#include "pppmacro.h"
#include "pppdes.h"
#include "snmccons.h"
#include "pppgsem.h"
#include "pppecp.h"
#include "desecb.h"
#ifdef PPP_STACK_WANTED
#include "ppdeslow.h"
#endif
#define __NOT_WANTED_GLOABL__
#include "llproto.h"

#ifdef PPP_STACK_WANTED            /* DSL_ADD */
extern tECPIf      *SNMPGetECPIfPtr (UINT4 Index);
/* LOW LEVEL Routines for Table : PppDesConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppDesConfigTable
 Input       :  The Indices
                PppDesConfigIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppDesConfigTable (INT4 i4PppDesConfigIfIndex)
{
    if (SNMPGetECPIfPtr (i4PppDesConfigIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppDesConfigTable
 Input       :  The Indices
                PppDesConfigIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppDesConfigTable (INT4 *pi4PppDesConfigIfIndex)
{
    if (nmhGetFirstIndex (pi4PppDesConfigIfIndex, ECPInstance, FIRST_INST) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);

}

/****************************************************************************
 Function    :  nmhGetNextIndexPppDesConfigTable
 Input       :  The Indices
                PppDesConfigIfIndex
                nextPppDesConfigIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppDesConfigTable (INT4 i4PppDesConfigIfIndex,
                                  INT4 *pi4NextPppDesConfigIfIndex)
{
    if (nmhGetNextIndex (&i4PppDesConfigIfIndex, ECPInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextPppDesConfigIfIndex = i4PppDesConfigIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppDesConfigRxIfKey
 Input       :  The Indices
                PppDesConfigIfIndex

                The Object 
                retValPppDesConfigRxIfKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppDesConfigRxIfKey (INT4 i4PppDesConfigIfIndex,
                           tSNMP_OCTET_STRING_TYPE * pRetValPppDesConfigRxIfKey)
{
    tECPIf             *pEcpIf;
    tDESEParams        *pDesPtr = NULL;
    tHeader            *pHeader;

    if ((pEcpIf = SNMPGetECPIfPtr (i4PppDesConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    SLL_SCAN (&pEcpIf->OptionsDesired, pHeader, tHeader *)
    {
        if (pHeader->Type == OPTION_DESE)
        {
            pDesPtr = (tDESEParams *) pHeader;
            /* RFC007 *Str = pDesPtr->Key; */
            pRetValPppDesConfigRxIfKey->i4_Length = KEY_LENGTH;
            MEMCPY (pRetValPppDesConfigRxIfKey->pu1_OctetList, pDesPtr->Key,
                    pRetValPppDesConfigRxIfKey->i4_Length);
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);

}

/****************************************************************************
 Function    :  nmhGetPppDesConfigTxIfKey
 Input       :  The Indices
                PppDesConfigIfIndex

                The Object 
                retValPppDesConfigTxIfKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppDesConfigTxIfKey (INT4 i4PppDesConfigIfIndex,
                           tSNMP_OCTET_STRING_TYPE * pRetValPppDesConfigTxIfKey)
{
    tECPIf             *pEcpIf;
    tDESEParams        *pDesPtr = NULL;
    tHeader            *pHeader;

    if ((pEcpIf = SNMPGetECPIfPtr (i4PppDesConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    SLL_SCAN (&pEcpIf->OptionsAllowedForPeer, pHeader, tHeader *)
    {
        if (pHeader->Type == OPTION_DESE)
        {
            pDesPtr = (tDESEParams *) pHeader;
            /* RFC007 *Str = pDesPtr->Key; */
            pRetValPppDesConfigTxIfKey->i4_Length = KEY_LENGTH;
            MEMCPY (pRetValPppDesConfigTxIfKey->pu1_OctetList, pDesPtr->Key,
                    pRetValPppDesConfigTxIfKey->i4_Length);
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppDesConfigRxIfKey
 Input       :  The Indices
                PppDesConfigIfIndex

                The Object 
                setValPppDesConfigRxIfKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppDesConfigRxIfKey (INT4 i4PppDesConfigIfIndex,
                           tSNMP_OCTET_STRING_TYPE * pSetValPppDesConfigRxIfKey)
{
    tECPIf             *pEcpIf;
    tHeader            *pHeader;
    tDESEParams        *pDesPtr = NULL;

    PPP_UNUSED (pSetValPppDesConfigRxIfKey->i4_Length);
    pEcpIf = SNMPGetECPIfPtr (i4PppDesConfigIfIndex);
    SLL_SCAN (&pEcpIf->OptionsDesired, pHeader, tHeader *)
    {
        if (pHeader->Type == OPTION_DESE)
        {
            pDesPtr = (tDESEParams *) pHeader;
            if (pDesPtr == NULL)
            {
                return (SNMP_FAILURE);
            }
            BCOPY (pSetValPppDesConfigRxIfKey->pu1_OctetList, pDesPtr->Key, 8);
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);

}

/****************************************************************************
 Function    :  nmhSetPppDesConfigTxIfKey
 Input       :  The Indices
                PppDesConfigIfIndex

                The Object 
                setValPppDesConfigTxIfKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppDesConfigTxIfKey (INT4 i4PppDesConfigIfIndex,
                           tSNMP_OCTET_STRING_TYPE * pSetValPppDesConfigTxIfKey)
{
    tECPIf             *pEcpIf;
    tHeader            *pHeader;
    tDESEParams        *pDesPtr = NULL;

    PPP_UNUSED (pSetValPppDesConfigTxIfKey->i4_Length);
    pEcpIf = SNMPGetECPIfPtr (i4PppDesConfigIfIndex);
    SLL_SCAN (&pEcpIf->OptionsAllowedForPeer, pHeader, tHeader *)
    {
        if (pHeader->Type == OPTION_DESE)
        {
            pDesPtr = (tDESEParams *) pHeader;
            if (pDesPtr == NULL)
            {
                return (SNMP_FAILURE);
            }
            BCOPY (pSetValPppDesConfigTxIfKey->pu1_OctetList, pDesPtr->Key, 8);
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppDesConfigRxIfKey
 Input       :  The Indices
                PppDesConfigIfIndex

                The Object 
                testValPppDesConfigRxIfKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppDesConfigRxIfKey (UINT4 *pu4ErrorCode, INT4 i4PppDesConfigIfIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pTestValPppDesConfigRxIfKey)
{
    INT1                Found;

    Found = FALSE;
    PPP_UNUSED (i4PppDesConfigIfIndex);
    if (pTestValPppDesConfigRxIfKey->i4_Length != KEY_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppDesConfigTxIfKey
 Input       :  The Indices
                PppDesConfigIfIndex

                The Object 
                testValPppDesConfigTxIfKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppDesConfigTxIfKey (UINT4 *pu4ErrorCode, INT4 i4PppDesConfigIfIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pTestValPppDesConfigTxIfKey)
{
    INT1                Found;

    Found = FALSE;
    PPP_UNUSED (i4PppDesConfigIfIndex);
    if (pTestValPppDesConfigTxIfKey->i4_Length != KEY_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/* LOW LEVEL Routines for Table : PppDesStatusTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppDesStatusTable
 Input       :  The Indices
                PppDesStatusIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppDesStatusTable (INT4 i4PppDesStatusIfIndex)
{
    if (SNMPGetECPIfPtr (i4PppDesStatusIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppDesStatusTable
 Input       :  The Indices
                PppDesStatusIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppDesStatusTable (INT4 *pi4PppDesStatusIfIndex)
{
    if (nmhGetFirstIndex (pi4PppDesStatusIfIndex, ECPInstance, FIRST_INST) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppDesStatusTable
 Input       :  The Indices
                PppDesStatusIfIndex
                nextPppDesStatusIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppDesStatusTable (INT4 i4PppDesStatusIfIndex,
                                  INT4 *pi4NextPppDesStatusIfIndex)
{
    if (nmhGetNextIndex (&i4PppDesStatusIfIndex, ECPInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextPppDesStatusIfIndex = i4PppDesStatusIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppDesStatusRxIfNonce
 Input       :  The Indices
                PppDesStatusIfIndex

                The Object 
                retValPppDesStatusRxIfNonce
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppDesStatusRxIfNonce (INT4 i4PppDesStatusIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValPppDesStatusRxIfNonce)
{
    tECPIf             *pEcpIf;
    tDesIfInfo         *pRxEncIf;

    if ((pEcpIf = SNMPGetECPIfPtr (i4PppDesStatusIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if ((pRxEncIf = (tDesIfInfo *) pEcpIf->pInEncIf) == NULL)
    {
        return (SNMP_FAILURE);
    }
    /* RFC007 *Str = pRxEncIf->Nonce; */
    pRetValPppDesStatusRxIfNonce->i4_Length = DESE_NONCE_LENGTH;
    MEMCPY (pRetValPppDesStatusRxIfNonce->pu1_OctetList, pRxEncIf->Nonce,
            pRetValPppDesStatusRxIfNonce->i4_Length);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppDesStatusRxIfPkts
 Input       :  The Indices
                PppDesStatusIfIndex

                The Object 
                retValPppDesStatusRxIfPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppDesStatusRxIfPkts (INT4 i4PppDesStatusIfIndex,
                            UINT4 *pu4RetValPppDesStatusRxIfPkts)
{
    tECPIf             *pEcpIf;
    tDesIfInfo         *pRxEncIf;

    if ((pEcpIf = SNMPGetECPIfPtr (i4PppDesStatusIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if ((pRxEncIf = (tDesIfInfo *) pEcpIf->pInEncIf) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValPppDesStatusRxIfPkts = pRxEncIf->DesStats.RxPkts;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppDesStatusRxIfDecryptFailedPkts
 Input       :  The Indices
                PppDesStatusIfIndex

                The Object 
                retValPppDesStatusRxIfDecryptFailedPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppDesStatusRxIfDecryptFailedPkts (INT4 i4PppDesStatusIfIndex,
                                         UINT4
                                         *pu4RetValPppDesStatusRxIfDecryptFailedPkts)
{
    tECPIf             *pEcpIf;
    tDesIfInfo         *pRxEncIf;

    if ((pEcpIf = SNMPGetECPIfPtr (i4PppDesStatusIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if ((pRxEncIf = (tDesIfInfo *) pEcpIf->pInEncIf) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValPppDesStatusRxIfDecryptFailedPkts =
        pRxEncIf->DesStats.RxFailedPkts;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppDesStatusTxIfNonce
 Input       :  The Indices
                PppDesStatusIfIndex

                The Object 
                retValPppDesStatusTxIfNonce
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppDesStatusTxIfNonce (INT4 i4PppDesStatusIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValPppDesStatusTxIfNonce)
{
    tECPIf             *pEcpIf;
    tDesIfInfo         *pTxEncIf;

    if ((pEcpIf = SNMPGetECPIfPtr (i4PppDesStatusIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if ((pTxEncIf = (tDesIfInfo *) pEcpIf->pOutEncIf) == NULL)
    {
        return (SNMP_FAILURE);
    }
    /* RFC007 *Str = pTxEncIf->Nonce; */
    pRetValPppDesStatusTxIfNonce->i4_Length = DESE_NONCE_LENGTH;
    MEMCPY (pRetValPppDesStatusTxIfNonce->pu1_OctetList, pTxEncIf->Nonce,
            pRetValPppDesStatusTxIfNonce->i4_Length);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppDesStatusTxIfPkts
 Input       :  The Indices
                PppDesStatusIfIndex

                The Object 
                retValPppDesStatusTxIfPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppDesStatusTxIfPkts (INT4 i4PppDesStatusIfIndex,
                            UINT4 *pu4RetValPppDesStatusTxIfPkts)
{
    tECPIf             *pEcpIf;
    tDesIfInfo         *pTxEncIf;

    if ((pEcpIf = SNMPGetECPIfPtr (i4PppDesStatusIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if ((pTxEncIf = (tDesIfInfo *) pEcpIf->pOutEncIf) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValPppDesStatusTxIfPkts = pTxEncIf->DesStats.TxPkts;
    return (SNMP_SUCCESS);

}
#endif /* DSL_ADD */
