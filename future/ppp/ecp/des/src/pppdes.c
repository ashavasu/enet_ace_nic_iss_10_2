/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppdes.c,v 1.3 2014/03/11 14:02:47 siva Exp $
 *
 * Description:Interface and Internal func.s for ppp ecp des 
 *             (rfc-1969)(copied from crypt.c)
 *
 *******************************************************************/
#include "genhdrs.h"
#include "pppmacro.h"
#include "pppdefs.h"
#include "pppdebug.h"
#include "ppptimer.h"
#include "pppeccmn.h"
#include "pppecpmd.h"
#include "pppdes.h"
#include "pppdbgex.h"
#include "pppmacro.h"
#include "desecb.h"
#include "desproto.h"
#include "globexts.h"
#ifdef UNIX
#include <sys/time.h>
#include <unistd.h>
#endif

/* #include <unistd.h> */

extern UINT1       *pAllocPtr;

UINT1             **SetKey (char *);

INT1
UNPAD_Packet (Buffer, size)
     UINT1              *Buffer;
     UINT2              *size;
{
    UINT2               count;
    UINT2               BufIndex;
    for (count = Buffer[*size - 1], BufIndex = (UINT2) (*size - 1); count >= 1;
         count--)
    {
        if (Buffer[BufIndex--] != count)
        {
            return (NOT_OK);
        }
    }
    *size = (UINT2) (*size - Buffer[*size - 1]);
    return (OK);
}

VOID
PAD_Packet (UINT1 *pInBuf, UINT2 *size, UINT1 PadConst)
{
    UINT1               AddBytes;
    UINT1               paddingBytes =
        (UINT1) ((*size) % PadConst ? PadConst - (*size) % PadConst : 0);

    if (paddingBytes == 0)
    {
        paddingBytes = PadConst;
    }
    AddBytes = paddingBytes;

    while (paddingBytes)
    {
        paddingBytes--;
        pInBuf[*size + paddingBytes] = (UINT1) (paddingBytes + 1);
    }
    *size = (UINT2) (*size + AddBytes);

}

INT1
DESEEncrypt (t_MSG_DESC ** pCruBuffer, VOID *pIfInfo)
{
    UINT2               Size;
    tDesIfInfo         *pDesIfInfo;

    pDesIfInfo = (tDesIfInfo *) pIfInfo;

    DEBUG_CHECK (pCruBuffer == NULL
                 || pDesIfInfo == NULL, "null prmtrs", "DesEncrypt()", 0);

    CB_READ_OFFSET ((*pCruBuffer)) = 0;
    pDesIfInfo->DesStats.RxPkts++;

    Size = (UINT2) VALID_BYTES (*pCruBuffer);
    EXTRACTSTR ((*pCruBuffer), pDesIfInfo->Buffer, Size);
    PAD_Packet (pDesIfInfo->Buffer, &Size, PAD_DES_CONST);

    CHECK (Size > pDesIfInfo->MRUSize, "Input Buffer too large", "DESEEncrypt",
           DISCARD);
    CHECK (Size == 0, "input buffer size is not in bounds", "DesEncrypt",
           NOT_OK);

    if (Crypt
        (pDesIfInfo->Buffer, Size, (UINT1 (*)[8]) pDesIfInfo->Kn,
         pDesIfInfo->Nonce, encryption) == OK)
    {
        RELEASE_BUFFER (*pCruBuffer);
        *pCruBuffer =
            (t_MSG_DESC *) (VOID *) ALLOCATE_BUFFER (Size + PPP_INFO_OFFSET +
                                                     DES_HDR_SZ,
                                                     PPP_INFO_OFFSET);
        CHECK (*pCruBuffer == NULL, NOMEM, "DesEncrypt()", NOT_OK);
        APPEND2BYTE ((*pCruBuffer), pDesIfInfo->SeqNum);
        APPENDSTR ((*pCruBuffer), pDesIfInfo->Buffer, Size);
        pDesIfInfo->SeqNum++;
        return OK;
    }

    return NOT_OK;
}

INT1
DESEDecrypt (t_MSG_DESC ** pCruBuffer, VOID *pIfInfo)
{
    UINT2               Size, Seq;
    tDesIfInfo         *pDesIfInfo;

    pDesIfInfo = (tDesIfInfo *) pIfInfo;
    pDesIfInfo->DesStats.TxPkts++;
    pDesIfInfo = (tDesIfInfo *) pIfInfo;

    DEBUG_CHECK (pCruBuffer == NULL
                 || pDesIfInfo == NULL, "null prmtrs", "DesEncrypt()", NOT_OK);

    Size = (UINT2) (VALID_BYTES (*pCruBuffer) - DES_HDR_SZ);
    DEBUG_CHECK (Size <= 2, "invalid size of buffer", "DesDecrypt", NOT_OK);
    CHECK (Size > pDesIfInfo->MRUSize, "Input Buffer too large", "DESEEncrypt",
           DISCARD);
    EXTRACT2BYTE ((*pCruBuffer), Seq);
    CHECK (pDesIfInfo->SeqNum != Seq, "seq-num mismatch\n    send reset-req",
           "DesDecrypt", DISCARD);
    EXTRACTSTR ((*pCruBuffer), pDesIfInfo->Buffer, Size);

    if (Crypt
        (pDesIfInfo->Buffer, Size, (UINT1 (*)[8]) pDesIfInfo->Kn,
         pDesIfInfo->Nonce, decryption) == OK)
    {

        RELEASE_BUFFER (*pCruBuffer);
        ReleaseFlag = PPP_NO;

        CHECK ((*pCruBuffer =
                (t_MSG_DESC *) (VOID *) ALLOCATE_BUFFER (Size - DES_HDR_SZ,
                                                         0)) == NULL, NOMEM,
               "DesEncrypt()", NOT_OK);

        if (UNPAD_Packet (pDesIfInfo->Buffer, &Size) == NOT_OK)
        {
            return (NOT_OK);
        }

        APPENDSTR ((*pCruBuffer), pDesIfInfo->Buffer, Size);
        pDesIfInfo->SeqNum++;
        return OK;
    }

    pDesIfInfo->DesStats.RxFailedPkts++;
    return NOT_OK;
}

/**********************************************************************
Function:Crypt
Description:
    Takes  buffer, Size, key and the operation.
    Does decryption/encryption aft reading chunks (SZ) and writes  to the same buffer
    operation specifies either decryption/encryption assumes only buffer in multiples if SZ
    so any necessary padding is to be done by the caller.
Ref:

Return Value: UINT1
if success
    returns OK
else
    NOT_OK

**********************************************************************/

INT1
Crypt (UINT1 *Buffer, UINT2 Size, UINT1 (*kn)[8], UINT1 *Nonce, OPER operation)
{
    UINT2               i;
    PPP_TRC1 (CONTROL_PLANE, " Crypt() Size = %d", Size);
#ifdef CHECK_8BYTE_MULT
    CHECK (Size % 8 != 0, "Input Buffer Size not in 8-byte Multiples",
           "DESEEncrypt", DISCARD);
#endif

    DEBUG_CHECK (kn == NULL, "key not initialised", "Crypt", NOT_OK);
    if (!Size)
    {
        return NOT_OK;
    }
    DEBUG_CHECK ((operation != encryption && operation != decryption),
                 "wrong operation", "Crypt()", NOT_OK);
    if (operation == encryption)
    {
        ENCRYPT_BUFFER;
    }
    else
    {
        DECRYPT_BUFFER;
    }
    return OK;
}

VOID
DESEHandleResetPkt (VOID *HistoryList)
{

    PPP_UNUSED (HistoryList);
    return;
}

INT1
DESEValidateResetPkt (UINT2 Length, UINT1 Code)
{
    PPP_UNUSED (Length);
    PPP_UNUSED (Code);
    return (OK);
}

UINT2
DESEConstructResetReqPkt (t_MSG_DESC * pIn)
{
    PPP_UNUSED (pIn);
    return 0;
}

INT1
DESEInit (VOID *Size, tDESEParams * pInParams, tDesIfInfo ** pDesIfInfo)
{
    UINT1               KeyStr[8];
    DEBUG_CHECK (Size == NULL
                 || pInParams == NULL, "NULL Params", "DESEInit", NOT_OK);
    DEBUG_CHECK ((*pDesIfInfo = MEM_CALLOC (1, sizeof (tDesIfInfo), tDesIfInfo))
                 == NULL, NOMEM, "DESEInit", NOT_OK);

    (*pDesIfInfo)->Buffer = MEM_MALLOC (*((UINT2 *) Size), UINT1);
    if ((*pDesIfInfo)->Buffer == NULL)
        return 0;

    if (desinit (0) == -1)
        return 0;

    BCOPY (pInParams->Nonce, (*pDesIfInfo)->Nonce, 8);
    (*pDesIfInfo)->MRUSize = *(UINT2 *) Size;
    (*pDesIfInfo)->SeqNum = 0;

    BCOPY (pInParams->Key, KeyStr, 8);
    ((*pDesIfInfo)->Kn) = (UINT1 (*)[8]) SetKey ((char *) KeyStr);
    endes ((char *) ((*pDesIfInfo)->Nonce), (UINT1 (*)[]) ((*pDesIfInfo)->Kn));
    return (1);
}

VOID
DESEDeInit (VOID *pDesIfInfo)
{
    if (pDesIfInfo != NULL)
    {
        MEM_FREE (((tDesIfInfo *) pDesIfInfo)->Buffer);
        MEM_FREE (((tDesIfInfo *) pDesIfInfo)->Kn);
        MEM_FREE ((tDesIfInfo *) pDesIfInfo);
    }
    return;
}

tDESEParams        *
DESECreateIf (const UINT1 *key)
{
    tDESEParams        *pDesParams;

    pDesParams = MEM_MALLOC (sizeof (tDESEParams), tDESEParams);
    CHECK (pDesParams == NULL, NOMEM, "DesCreateIf", NULL);
#ifdef UNIX
    gettimeofday ((struct timeval *) (VOID *) pDesParams->Nonce, NULL);
#else
    /*code to gen a 8 byte random value */
#endif
    pDesParams->Header.Type = OPTION_DESE;
    pDesParams->Header.Length = sizeof (tDESEParams);
    SLL_INIT_NODE (&pDesParams->Header.pNext);
    BCOPY (key, pDesParams->Key, 8);
    return pDesParams;
}
