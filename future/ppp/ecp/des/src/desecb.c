/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: desecb.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description: This file contains code for DES Encryption.
 *
 *******************************************************************/
#include "desecb.h"

void
des_set_odd_parity (key)
     des_cblock         *key;
{
    int                 i;
    /* Makefile changes - <comparison between signed and unsigned> */
    for (i = 0; i < (int) DES_KEY_SZ; i++)
        (*key)[i] = odd_parity[(*key)[i]];
}

 /* See ecb_encrypt.c for a pseudo description of these macros. 
  * #define PERM_OP(a,b,t,n,m) ((t)=((((a)>>(n))^(b))&(m)),\
  *   (b)^=(t),\
  *   (a)=((a)^((t)<<(n))))
  */

#define HPERM_OP(a,t,n,m) ((t)=((((a)<<(16-(n)))^(a))&(m)),\
    (a)=(a)^(t)^(t>>(16-(n))))

static char         shifts2[16] =
    { 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0 };

/* return 0 if key parity is odd (correct),
 * return -1 if key parity error,
 * return -2 if illegal weak key.
 */
int
des_set_key (key, schedule)
     des_cblock         *key;
     des_key_schedule    schedule;
{
    register unsigned long c, d, t, s;
    register unsigned char *in;
    register unsigned long *k;
    register int        i;

    k = (unsigned long *) schedule;
    in = (unsigned char *) key;

    c2l (in, c);
    c2l (in, d);

    /* do PC1 in 60 simple operations */
/*    PERM_OP(d,c,t,4,0x0f0f0f0f);
    HPERM_OP(c,t,-2, 0xcccc0000);
    HPERM_OP(c,t,-1, 0xaaaa0000);
    HPERM_OP(c,t, 8, 0x00ff0000);
    HPERM_OP(c,t,-1, 0xaaaa0000);
    HPERM_OP(d,t,-8, 0xff000000);
    HPERM_OP(d,t, 8, 0x00ff0000);
    HPERM_OP(d,t, 2, 0x33330000);
    d=((d&0x00aa00aa)<<7)|((d&0x55005500)>>7)|(d&0xaa55aa55);
    d=(d>>8)|((c&0xf0000000)>>4);
    c&=0x0fffffff; */

    /* I now do it in 47 simple operations :-)
     * Thanks to John Fletcher (john_fletcher@lccmail.ocf.llnl.gov)
     * for the inspiration. :-) */
    PERM_OP (d, c, t, 4, 0x0f0f0f0fL);
    HPERM_OP (c, t, -2, 0xcccc0000L);
    HPERM_OP (d, t, -2, 0xcccc0000L);
    PERM_OP (d, c, t, 1, 0x55555555L);
    PERM_OP (c, d, t, 8, 0x00ff00ffL);
    PERM_OP (d, c, t, 1, 0x55555555L);
    d = (((d & 0x000000ffL) << 16) | (d & 0x0000ff00L) |
         ((d & 0x00ff0000L) >> 16) | ((c & 0xf0000000L) >> 4));
    c &= 0x0fffffffL;

    for (i = 0; i < ITERATIONS; i++)
    {
        if (shifts2[i])
        {
            c = ((c >> 2) | (c << 26));
            d = ((d >> 2) | (d << 26));
        }
        else
        {
            c = ((c >> 1) | (c << 27));
            d = ((d >> 1) | (d << 27));
        }
        c &= 0x0fffffffL;
        d &= 0x0fffffffL;
        /* could be a few less shifts but I am to lazy at this
         * point in time to investigate */
        s = des_skb[0][(c) & 0x3f] |
            des_skb[1][((c >> 6) & 0x03) | ((c >> 7) & 0x3c)] |
            des_skb[2][((c >> 13) & 0x0f) | ((c >> 14) & 0x30)] |
            des_skb[3][((c >> 20) & 0x01) | ((c >> 21) & 0x06) |
                       ((c >> 22) & 0x38)];
        t = des_skb[4][(d) & 0x3f] |
            des_skb[5][((d >> 7) & 0x03) | ((d >> 8) & 0x3c)] |
            des_skb[6][(d >> 15) & 0x3f] |
            des_skb[7][((d >> 21) & 0x0f) | ((d >> 22) & 0x30)];

        /* table contained 0213 4657 */
        *(k++) = ((t << 16) | (s & 0x0000ffffL)) & 0xffffffffL;
        s = ((s >> 16) | (t & 0xffff0000L));

        s = (s << 4) | (s >> 28);
        *(k++) = s & 0xffffffffL;
    }
    return (0);
}

int
des_key_sched (key, schedule)
     des_cblock         *key;
     des_key_schedule    schedule;
{
    return (des_set_key (key, schedule));
}

int
des_ecb_encrypt (input, output, ks, encrypt)
     des_cblock         *input;
     des_cblock         *output;
     des_key_schedule    ks;
     int                 encrypt;
{
    register unsigned long l0, l1;
    register unsigned char *in, *out;
    unsigned long       ll[2];

    in = (unsigned char *) input;
    out = (unsigned char *) output;
    c2l (in, l0);
    c2l (in, l1);
    ll[0] = l0;
    ll[1] = l1;
    des_encrypt (ll, ll, ks, encrypt);
    l0 = ll[0];
    l1 = ll[1];
    l2c (l0, out);
    l2c (l1, out);
    l0 = l1 = ll[0] = ll[1] = 0;
    return (0);
}

int
des_encrypt (input, output, ks, encrypt)
     unsigned long      *input;
     unsigned long      *output;
     des_key_schedule    ks;
     int                 encrypt;
{
    register unsigned long l, r, t, u;
    register int        i;
    register unsigned long *s;

    l = input[0];
    r = input[1];

    IP (l, r, t);
    /* Things have been modified so that the initial rotate is
     * done outside the loop.  This required the
     * des_SPtrans values in sp.h to be rotated 1 bit to the right.
     * One perl script later and things have a 5% speed up on a sparc2.
     * Thanks to Richard Outerbridge <71755.204@CompuServe.COM>
     * for pointing this out. */
    t = (r << 1) | (r >> 31);
    r = (l << 1) | (l >> 31);
    l = t;

    /* clear the top bits on machines with 8byte longs */
    l &= 0xffffffffL;
    r &= 0xffffffffL;

    s = (unsigned long *) ks;
    /* I don't know if it is worth the effort of loop unrolling the
     * inner loop */
    if (encrypt)
    {
        for (i = 0; i < 32; i += 4)
        {
            D_ENCRYPT (l, r, i + 0);    /*  1 */
            D_ENCRYPT (r, l, i + 2);    /*  2 */
        }
    }
    else
    {
        for (i = 30; i > 0; i -= 4)
        {
            D_ENCRYPT (l, r, i - 0);    /* 16 */
            D_ENCRYPT (r, l, i - 2);    /* 15 */
        }
    }
    l = (l >> 1) | (l << 31);
    r = (r >> 1) | (r << 31);
    /* clear the top bits on machines with 8byte longs */
    l &= 0xffffffffL;
    r &= 0xffffffffL;

    FP (r, l, t);
    output[0] = l;
    output[1] = r;
    l = r = t = u = 0;
    return (0);
}

void
des_7_To_8_Byte (key)
     des_cblock         *key;
{
    int                 i;

    for (i = 7; i >= 1; i--)
    {
        key[0][i] =
            (unsigned char) ((key[0][i] >> i) | (key[0][i - 1] << (8 - i)));
        key[0][i] &= 0xfe;
    }
    key[0][i] &= 0xfe;
    des_set_odd_parity (key);
}
