/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppp3des.c,v 1.3 2011/10/13 10:31:21 siva Exp $
 *
 * Description:Interface and Internal functions for ppp ecp and 
 *             Triple-des* (rfc-2420)
 *
 *******************************************************************/
#include "genhdrs.h"
#include "pppmacro.h"
#include "pppdefs.h"
#include "pppdebug.h"
#include "ppptimer.h"

#include "pppeccmn.h"
#include "pppecpmd.h"
#include "pppdes.h"
#include "pppdbgex.h"
#include "pppmacro.h"
#include "desecb.h"
#include "globexts.h"
#ifdef UNIX
#include <sys/time.h>
#include <unistd.h>
#endif
#include "desproto.h"
UINT1             **SetKey (char *);

extern UINT1       *pAllocPtr;

INT1
TripleDESEEncrypt (t_MSG_DESC ** pCruBuffer, VOID *pIfInfo)
{
    UINT2               Size;
    tTrplDesIfInfo     *pTrplDesIfInfo;

    pTrplDesIfInfo = (tTrplDesIfInfo *) pIfInfo;

    if ((pCruBuffer == NULL) || (pTrplDesIfInfo == NULL))
    {
        return NOT_OK;
    }

    CB_READ_OFFSET ((*pCruBuffer)) = 0;
    pTrplDesIfInfo->TrplDesStats.RxPkts++;

    Size = (UINT2) VALID_BYTES (*pCruBuffer);
    if (Size == 0)
    {
        PPP_TRC (CONTROL_PLANE, " Input Data Buffer Null");
        return (NOT_OK);
    }

    EXTRACTSTR ((*pCruBuffer), pTrplDesIfInfo->Buffer, Size);
    PAD_Packet (pTrplDesIfInfo->Buffer, &Size, PAD_DES_CONST);

    if (TripleDESECrypt (pTrplDesIfInfo, Size, encryption) == OK)
    {
        RELEASE_BUFFER (*pCruBuffer);
        if ((*pCruBuffer =
             (t_MSG_DESC *) (VOID *) ALLOCATE_BUFFER (Size + PPP_INFO_OFFSET +
                                                      DES_HDR_SZ,
                                                      PPP_INFO_OFFSET)) == NULL)
        {
            PRINT_MEM_ALLOC_FAILURE;
            return (NOT_OK);
        }
        APPEND2BYTE ((*pCruBuffer), pTrplDesIfInfo->SeqNum);
        PPP_TRC1 (CONTROL_PLANE, " EncPktSeqNo = %u .", pTrplDesIfInfo->SeqNum);

        APPENDSTR ((*pCruBuffer), pTrplDesIfInfo->Buffer, Size);
        pTrplDesIfInfo->SeqNum++;
        return OK;
    }
    return NOT_OK;
}

INT1
TripleDESEDecrypt (t_MSG_DESC ** pCruBuffer, VOID *pIfInfo)
{
    UINT2               Size, Seq;
    tTrplDesIfInfo     *pTrplDesIfInfo;

    pTrplDesIfInfo = (tTrplDesIfInfo *) pIfInfo;

    if ((pCruBuffer == NULL) || (pTrplDesIfInfo == NULL))
    {
        PPP_DBG (" Null parameters to TriplDesDecrypt()");
        return NOT_OK;
    }
    pTrplDesIfInfo->TrplDesStats.TxPkts++;

    Size = (UINT2) (VALID_BYTES (*pCruBuffer) - DES_HDR_SZ);
    PPP_TRC1 (CONTROL_PLANE, "SIZE = %u", Size);
    if (Size <= 2)
    {
        PPP_TRC (ALL_FAILURE, " Invalid size of buffer");
        return (NOT_OK);
    }

    EXTRACT2BYTE ((*pCruBuffer), Seq);
    if (pTrplDesIfInfo->SeqNum != Seq)
    {
        PPP_TRC (CONTROL_PLANE, " seq-num mismatch, send reset-req");
        return (DISCARD);
    }
    EXTRACTSTR ((*pCruBuffer), pTrplDesIfInfo->Buffer, Size);

    if (TripleDESECrypt (pTrplDesIfInfo, Size, decryption) == OK)
    {

        RELEASE_BUFFER ((*pCruBuffer));
        ReleaseFlag = PPP_NO;
        if ((*pCruBuffer =
             (t_MSG_DESC *) (VOID *) ALLOCATE_BUFFER (Size - DES_HDR_SZ,
                                                      0)) == NULL)
        {
            PRINT_MEM_ALLOC_FAILURE;
            return (NOT_OK);
        }

        if (UNPAD_Packet (pTrplDesIfInfo->Buffer, &Size) == NOT_OK)
        {
            return (NOT_OK);
        }
        APPENDSTR ((*pCruBuffer), pTrplDesIfInfo->Buffer, Size);
        pTrplDesIfInfo->SeqNum++;
        return OK;
    }
    pTrplDesIfInfo->TrplDesStats.RxFailedPkts++;
    return NOT_OK;
}

INT1
TripleDESECrypt (tTrplDesIfInfo * pTrplDesIfInfo, UINT2 Size, OPER Operation)
{

    PPP_TRC1 (CONTROL_PLANE, "In Crypt() Size = %d\n", Size);
    if ((pTrplDesIfInfo->Key1 == NULL) || (pTrplDesIfInfo->Key2 == NULL)
        || (pTrplDesIfInfo->Key3 == NULL))
    {
        PPP_TRC (ALL_FAILURE, "Keys not properly initialized");
        return NOT_OK;
    }
    if (Operation == encryption)
    {
        DES_EDE3_CBC (pTrplDesIfInfo, Size);
    }
    else
    {
        DES_DED3_CBC (pTrplDesIfInfo, Size);
    }

    return OK;
}

VOID
DES_EDE3_CBC (tTrplDesIfInfo * pTrplDesIfInfo, UINT2 Size)
{
    UINT4               Count1, Count2;

    Count1 = 0;
    /* Makefile changes - <comparison between signed and unsigned */
    while (Count1 <= (UINT4) (Size - 2))
    {
        for (Count2 = 0; Count2 != 8; Count2++)
            pTrplDesIfInfo->Buffer[Count1 + Count2] ^=
                pTrplDesIfInfo->Nonce[Count2];
        endes ((char *) (pTrplDesIfInfo->Buffer + Count1),
               (UINT1 (*)[]) (pTrplDesIfInfo->Key1));
        dedes ((char *) (pTrplDesIfInfo->Buffer + Count1),
               (UINT1 (*)[]) (pTrplDesIfInfo->Key2));
        endes ((char *) (pTrplDesIfInfo->Buffer + Count1),
               (UINT1 (*)[]) (pTrplDesIfInfo->Key3));
        memcpy (pTrplDesIfInfo->Nonce, pTrplDesIfInfo->Buffer + Count1, 8);
        Count1 += 8;
    }
}

VOID
DES_DED3_CBC (tTrplDesIfInfo * pTrplDesIfInfo, UINT2 Size)
{
    UINT4               Count1, Count2;
    UINT1               TmpBuff[8];

    Count1 = 0;

    while (Count1 < Size)
    {
        memcpy (TmpBuff, pTrplDesIfInfo->Buffer + Count1, 8);

        dedes ((char *) (pTrplDesIfInfo->Buffer + Count1),
               (UINT1 (*)[]) (pTrplDesIfInfo->Key3));
        endes ((char *) (pTrplDesIfInfo->Buffer + Count1),
               (UINT1 (*)[]) (pTrplDesIfInfo->Key2));
        dedes ((char *) (pTrplDesIfInfo->Buffer + Count1),
               (UINT1 (*)[]) (pTrplDesIfInfo->Key1));

        for (Count2 = 0; Count2 != 8; Count2++)
            pTrplDesIfInfo->Buffer[Count1 + Count2] ^=
                pTrplDesIfInfo->Nonce[Count2];
        Count1 += 8;
        memcpy (pTrplDesIfInfo->Nonce, TmpBuff, 8);
    }
}

VOID
TripleDESEHandleResetPkt (VOID *HistoryList)
{
    PPP_UNUSED (HistoryList);
    return;
}

INT1
TripleDESEValidateResetPkt (UINT2 Length, UINT1 Code)
{
    PPP_UNUSED (Length);
    PPP_UNUSED (Code);
    return (OK);
}

UINT2
TripleDESEConstructResetReqPkt (t_MSG_DESC * pIn)
{
    PPP_UNUSED (pIn);
    return 0;
}

INT1
TripleDESEInit (VOID *Size, tTrplDESEParams * pInParams,
                tTrplDesIfInfo ** pTrplDesIfInfo)
{

    UINT1               KeyStr[8];

    if (Size == NULL || pInParams == NULL)
    {
        return (NOT_OK);
    }
    if ((*pTrplDesIfInfo =
         MEM_CALLOC (1, sizeof (tTrplDesIfInfo), tTrplDesIfInfo)) == NULL)
    {
        return (NOT_OK);
    }

    (*pTrplDesIfInfo)->Buffer = MEM_MALLOC (*((UINT2 *) Size), UINT1);
    if ((*pTrplDesIfInfo)->Buffer == NULL)
    {
        return (0);
    }

    if (desinit (0) == -1)
    {
        return (0);
    }

    BCOPY (pInParams->Nonce, (*pTrplDesIfInfo)->Nonce, 8);
    (*pTrplDesIfInfo)->SeqNum = 0;

    BCOPY (pInParams->Key1, KeyStr, 8);
    ((*pTrplDesIfInfo)->Key1) = (UINT1 (*)[8]) SetKey ((char *) KeyStr);
    BCOPY (pInParams->Key2, KeyStr, 8);
    ((*pTrplDesIfInfo)->Key2) = (UINT1 (*)[8]) SetKey ((char *) KeyStr);
    BCOPY (pInParams->Key3, KeyStr, 8);
    ((*pTrplDesIfInfo)->Key3) = (UINT1 (*)[8]) SetKey ((char *) KeyStr);

    endes ((char *) (*pTrplDesIfInfo)->Nonce,
           (UINT1 (*)[]) ((*pTrplDesIfInfo)->Key1));
    dedes ((char *) (*pTrplDesIfInfo)->Nonce,
           (UINT1 (*)[]) ((*pTrplDesIfInfo)->Key2));
    endes ((char *) (*pTrplDesIfInfo)->Nonce,
           (UINT1 (*)[]) ((*pTrplDesIfInfo)->Key3));

    return (1);
}

VOID
TripleDESEDeInit (VOID *pTrplDesIfInfo)
{
    if (pTrplDesIfInfo != NULL)
    {
        MEM_FREE (((tTrplDesIfInfo *) pTrplDesIfInfo)->Buffer);
        MEM_FREE (((tTrplDesIfInfo *) pTrplDesIfInfo)->Key1);
        MEM_FREE (((tTrplDesIfInfo *) pTrplDesIfInfo)->Key2);
        MEM_FREE (((tTrplDesIfInfo *) pTrplDesIfInfo)->Key3);
        MEM_FREE ((tTrplDesIfInfo *) pTrplDesIfInfo);
    }
    return;
}

tTrplDESEParams    *
TripleDESECreateIf (const UINT1 *Key1, const UINT1 *Key2, const UINT1 *Key3)
{
    tTrplDESEParams    *pTrplDesParams;

    pTrplDesParams = MEM_MALLOC (sizeof (tTrplDESEParams), tTrplDESEParams);
    if (pTrplDesParams == NULL)
    {
        return (NULL);
    }
#ifdef UNIX
    gettimeofday ((struct timeval *) (VOID *) pTrplDesParams->Nonce, NULL);
#else
    /*code to gen a 8 byte random value */
#endif
    pTrplDesParams->Header.Type = OPTION_3DESE;
    pTrplDesParams->Header.Length = sizeof (tTrplDESEParams);
    SLL_INIT_NODE (&pTrplDesParams->Header.pNext);
    BCOPY (Key1, pTrplDesParams->Key1, 8);
    BCOPY (Key2, pTrplDesParams->Key2, 8);
    BCOPY (Key3, pTrplDesParams->Key3, 8);
    return pTrplDesParams;
}
