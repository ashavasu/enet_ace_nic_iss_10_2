/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppp3desll.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:Low Level Functions for Triple-des (rfc-2420)
 *
 *******************************************************************/
#include "pppsnmpm.h"
#include "genhdrs.h"
#include "pppmacro.h"
#include "pppdefs.h"
#include "pppdebug.h"
#include "pppdebug.h"
#include "ppptimer.h"
#include "pppeccmn.h"
#include "pppecpmd.h"
#include "pppdes.h"
#include "pppdbgex.h"
#include "pppmacro.h"
#include "pppdes.h"
#include "snmccons.h"
#include "pppgsem.h"
#include "pppecp.h"
#include "llproto.h"

#ifdef PPP_STACK_WANTED
#include "ppdeslow.h"
#endif

#ifdef PPP_STACK_WANTED            /* DSL_ADD */
#define KEY_ONE 1
#define KEY_TWO 2
#define KEY_THREE 3

UINT1             **SetKey (char *);
extern tECPIf      *SNMPGetECPIfPtr (UINT4 Index);
INT1                GetPppTrplDesConfigKey (UINT4 Index, UINT1 **Str,
                                            UINT4 *StrLen, INT1 Key_No);
UINT1               TestPppTrplDesConfigKey (UINT4 Index, UINT4 length);
INT1                SetPppTrplDesConfigKey (UINT4 Index, UINT1 *Str,
                                            UINT4 StrLen, INT1 Key_No);

/* LOW LEVEL Routines for Table : PppTrplDesConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppTrplDesConfigTable
 Input       :  The Indices
                PppTrplDesConfigIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppTrplDesConfigTable (INT4 i4PppTrplDesConfigIfIndex)
{
    if (SNMPGetECPIfPtr (i4PppTrplDesConfigIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppTrplDesConfigTable
 Input       :  The Indices
                PppTrplDesConfigIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppTrplDesConfigTable (INT4 *pi4PppTrplDesConfigIfIndex)
{
    if (nmhGetFirstIndex (pi4PppTrplDesConfigIfIndex, ECPInstance, FIRST_INST)
        == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppTrplDesConfigTable
 Input       :  The Indices
                PppTrplDesConfigIfIndex
                nextPppTrplDesConfigIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppTrplDesConfigTable (INT4 i4PppTrplDesConfigIfIndex,
                                      INT4 *pi4NextPppTrplDesConfigIfIndex)
{
    if (nmhGetNextIndex (&i4PppTrplDesConfigIfIndex, ECPInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextPppTrplDesConfigIfIndex = i4PppTrplDesConfigIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppTrplDesConfigKey1
 Input       :  The Indices
                PppTrplDesConfigIfIndex

                The Object 
                retValPppTrplDesConfigKey1
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTrplDesConfigKey1 (INT4 i4PppTrplDesConfigIfIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValPppTrplDesConfigKey1)
{

    return (GetPppTrplDesConfigKey
            (i4PppTrplDesConfigIfIndex,
             &pRetValPppTrplDesConfigKey1->pu1_OctetList,
             (UINT4 *) &pRetValPppTrplDesConfigKey1->i4_Length, KEY_ONE));
}

/****************************************************************************
 Function    :  nmhGetPppTrplDesConfigKey2
 Input       :  The Indices
                PppTrplDesConfigIfIndex

                The Object 
                retValPppTrplDesConfigKey2
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTrplDesConfigKey2 (INT4 i4PppTrplDesConfigIfIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValPppTrplDesConfigKey2)
{

    return (GetPppTrplDesConfigKey
            (i4PppTrplDesConfigIfIndex,
             &pRetValPppTrplDesConfigKey2->pu1_OctetList,
             (UINT4 *) &pRetValPppTrplDesConfigKey2->i4_Length, KEY_TWO));
}

/****************************************************************************
 Function    :  nmhGetPppTrplDesConfigKey3
 Input       :  The Indices
                PppTrplDesConfigIfIndex

                The Object 
                retValPppTrplDesConfigKey3
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTrplDesConfigKey3 (INT4 i4PppTrplDesConfigIfIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValPppTrplDesConfigKey3)
{

    return (GetPppTrplDesConfigKey
            (i4PppTrplDesConfigIfIndex,
             &pRetValPppTrplDesConfigKey3->pu1_OctetList,
             (UINT4 *) &pRetValPppTrplDesConfigKey3->i4_Length, KEY_THREE));
}

INT1
GetPppTrplDesConfigKey (UINT4 Index, UINT1 **Str, UINT4 *StrLen, INT1 Key_No)
{
    tECPIf             *pEcpIf;
    tTrplDESEParams    *pTrplDes = NULL;
    tHeader            *pHeader;

    if ((pEcpIf = SNMPGetECPIfPtr (Index)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    SLL_SCAN (&pEcpIf->OptionsAllowedForPeer, pHeader, tHeader *)
    {
        if (pHeader->Type == OPTION_3DESE)
        {
            pTrplDes = (tTrplDESEParams *) pHeader;
            *StrLen = KEY_LENGTH;
            switch (Key_No)
            {
                case KEY_ONE:
                    MEMCPY (*Str, pTrplDes->Key1, *StrLen);
                case KEY_TWO:
                    MEMCPY (*Str, pTrplDes->Key2, *StrLen);
                case KEY_THREE:
                    MEMCPY (*Str, pTrplDes->Key3, *StrLen);
            }
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhTestv2PppTrplDesConfigKey1
 Input       :  The Indices
                PppTrplDesConfigIfIndex

                The Object 
                testValPppTrplDesConfigKey1
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTrplDesConfigKey1 (UINT4 *pu4ErrorCode,
                               INT4 i4PppTrplDesConfigIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValPppTrplDesConfigKey1)
{
    PPP_UNUSED (i4PppTrplDesConfigIfIndex);
    if (pTestValPppTrplDesConfigKey1->i4_Length != KEY_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppTrplDesConfigKey2
 Input       :  The Indices
                PppTrplDesConfigIfIndex

                The Object 
                testValPppTrplDesConfigKey2
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTrplDesConfigKey2 (UINT4 *pu4ErrorCode,
                               INT4 i4PppTrplDesConfigIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValPppTrplDesConfigKey2)
{
    PPP_UNUSED (i4PppTrplDesConfigIfIndex);
    if (pTestValPppTrplDesConfigKey2->i4_Length != KEY_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppTrplDesConfigKey3
 Input       :  The Indices
                PppTrplDesConfigIfIndex

                The Object 
                testValPppTrplDesConfigKey3
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTrplDesConfigKey3 (UINT4 *pu4ErrorCode,
                               INT4 i4PppTrplDesConfigIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValPppTrplDesConfigKey3)
{
    PPP_UNUSED (i4PppTrplDesConfigIfIndex);
    if (pTestValPppTrplDesConfigKey3->i4_Length != KEY_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

UINT1
TestPppTrplDesConfigKey (UINT4 Index, UINT4 length)
{
    INT1                Found;
    Found = FALSE;
    PPP_UNUSED (Index);
    if (length != KEY_LENGTH)
    {
        return (BAD_VALUE_ERROR);
    }
    return (NO_ERROR);
}

/****************************************************************************
 Function    :  nmhSetPppTrplDesConfigKey1
 Input       :  The Indices
                PppTrplDesConfigIfIndex

                The Object 
                setValPppTrplDesConfigKey1
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTrplDesConfigKey1 (INT4 i4PppTrplDesConfigIfIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValPppTrplDesConfigKey1)
{

    return (SetPppTrplDesConfigKey
            (i4PppTrplDesConfigIfIndex,
             pSetValPppTrplDesConfigKey1->pu1_OctetList,
             pSetValPppTrplDesConfigKey1->i4_Length, KEY_ONE));
}

/****************************************************************************
 Function    :  nmhSetPppTrplDesConfigKey2
 Input       :  The Indices
                PppTrplDesConfigIfIndex

                The Object 
                setValPppTrplDesConfigKey2
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTrplDesConfigKey2 (INT4 i4PppTrplDesConfigIfIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValPppTrplDesConfigKey2)
{

    return (SetPppTrplDesConfigKey
            (i4PppTrplDesConfigIfIndex,
             pSetValPppTrplDesConfigKey2->pu1_OctetList,
             pSetValPppTrplDesConfigKey2->i4_Length, KEY_TWO));
}

/****************************************************************************
 Function    :  nmhSetPppTrplDesConfigKey3
 Input       :  The Indices
                PppTrplDesConfigIfIndex

                The Object 
                setValPppTrplDesConfigKey3
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTrplDesConfigKey3 (INT4 i4PppTrplDesConfigIfIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValPppTrplDesConfigKey3)
{

    return (SetPppTrplDesConfigKey
            (i4PppTrplDesConfigIfIndex,
             pSetValPppTrplDesConfigKey3->pu1_OctetList,
             pSetValPppTrplDesConfigKey3->i4_Length, KEY_THREE));
}

INT1
SetPppTrplDesConfigKey (UINT4 Index, UINT1 *Str, UINT4 StrLen, INT1 Key_No)
{
    tECPIf             *pEcpIf;
    tHeader            *pHeader, *pHeader1;
    tTrplDESEParams    *pTrplDes = NULL;

    PPP_UNUSED (StrLen);
    pEcpIf = SNMPGetECPIfPtr (Index);
    SLL_SCAN (&pEcpIf->OptionsAllowedForPeer, pHeader, tHeader *)
    {
        if (pHeader->Type == OPTION_3DESE)
        {
            pTrplDes = (tTrplDESEParams *) pHeader;
            if (pTrplDes == NULL)
            {
                return (SNMP_FAILURE);
            }
            switch (Key_No)
            {
                case KEY_ONE:
                    BCOPY (Str, pTrplDes->Key1, 8);
                    break;
                case KEY_TWO:
                    BCOPY (Str, pTrplDes->Key2, 8);
                    break;
                case KEY_THREE:
                    BCOPY (Str, pTrplDes->Key3, 8);
                    break;
            }
        }
    }
    SLL_SCAN (&pEcpIf->OptionsDesired, pHeader1, tHeader *)
    {
        if (pHeader1->Type == OPTION_3DESE)
        {
            pTrplDes = (tTrplDESEParams *) NULL;
            pTrplDes = (tTrplDESEParams *) pHeader1;
            if (pTrplDes == NULL)
            {
                return (SNMP_FAILURE);
            }
            switch (Key_No)
            {
                case KEY_ONE:
                    BCOPY (Str, pTrplDes->Key1, 8);
                    break;
                case KEY_TWO:
                    BCOPY (Str, pTrplDes->Key2, 8);
                    break;
                case KEY_THREE:
                    BCOPY (Str, pTrplDes->Key3, 8);
                    break;
            }
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhSetPppTrplDesConfigIfIndex
 Input       :  The Indices
                PppTrplDesConfigIfIndex

                The Object 
                setValPppTrplDesConfigIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTrplDesConfigIfIndex (INT4 i4PppTrplDesConfigIfIndex,
                               INT4 i4SetValPppTrplDesConfigIfIndex)
{
    PPP_UNUSED (i4PppTrplDesConfigIfIndex);
    PPP_UNUSED (i4SetValPppTrplDesConfigIfIndex);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhTestv2PppTrplDesConfigIfIndex
 Input       :  The Indices
                PppTrplDesConfigIfIndex

                The Object 
                testValPppTrplDesConfigIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTrplDesConfigIfIndex (UINT4 *pu4ErrorCode,
                                  INT4 i4PppTrplDesConfigIfIndex,
                                  INT4 i4TestValPppTrplDesConfigIfIndex)
{
    PPP_UNUSED (i4PppTrplDesConfigIfIndex);
    PPP_UNUSED (i4TestValPppTrplDesConfigIfIndex);
    PPP_UNUSED (pu4ErrorCode);
    return (SNMP_SUCCESS);
}
#endif /* DSL_ADD */
