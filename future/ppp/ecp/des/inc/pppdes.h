/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppdes.h,v 1.2 2011/10/13 10:31:20 siva Exp $
 *
 * Description:This files contains the macros used by the
 *             modules of PPP subsystem.
 *
 *******************************************************************/
#ifndef __PPP_PPPDES_H__
#define __PPP_PPPDES_H__

#include "pppeccmn.h"

#ifndef PPPDESIF_H

typedef enum OPER {encryption =0, decryption =1}OPER;
#define DES_RESET_PKT_LENGTH 6
#define DES_HDR_SZ 2

#define PAD_DES_CONST 8

struct DesSnmp
{
    UINT2 RxPkts;
    UINT2 RxFailedPkts;
    UINT2 TxPkts;
    UINT2 u2Rsvd;
};

typedef struct DesIfInfo
{
        UINT1   (*Kn)[8];
        UINT1   Nonce[8];
        UINT2   SeqNum;  /* added by sriram for multiple-if */
        UINT2   MRUSize;
        UINT1   *Buffer; 
        struct DesSnmp DesStats;
}tDesIfInfo;

typedef struct DesParams
{
    tHeader Header;
    UINT1   Nonce[8];
    UINT1 Key[8];
}tDESEParams;

/*** TDESE: Addition for Triple-DESE Support *******/

typedef struct TrplDesIfInfo
{
   UINT1   (*Key1)[8];
   UINT1   (*Key2)[8];
   UINT1   (*Key3)[8];
   UINT1   Nonce[8];
   UINT2   SeqNum;
   UINT2   u2Rsvd;
   UINT1   *Buffer;
   struct DesSnmp TrplDesStats;
}tTrplDesIfInfo;

typedef struct TrplDesParams
{
   tHeader Header;
   UINT1   Nonce[8];
   UINT1 Key1[8];
   UINT1 Key2[8];
   UINT1 Key3[8];
}tTrplDESEParams;

/************************************************************/

#define ENCRYPT_BUFFER  \
{ \
    int j; \
    i=0; \
    while(i <= Size-2) \
    { \
        for (j = 0; j != 8; j++) \
        Buffer[i+j] ^= Nonce[j];\
        endes((char *)(Buffer+i), (UINT1(*)[])(kn));\
        memcpy(Nonce, Buffer+i, 8); i+=8; \
  } \
}
/********* SKR003  ************/
/***** Change: in while loop of the following Macro i <= Size-10 has been changed to i < Size ********/
#define DECRYPT_BUFFER \
{ \
      int j; \
      UINT1 temp[8]; \
      i=0; \
      while(i < Size)\
      {\
          memcpy(temp,Buffer+i,8);\
          dedes((char *)(Buffer+i), (UINT1(*)[])(kn));\
          for (j = 0; j != 8; j++)\
             Buffer[i+j] ^= Nonce[j];\
          i+=8;\
          memcpy(Nonce,temp,8);\
       }\
}
#ifdef malloc
#undef malloc
#undef free
#endif

/* padding adds extra bytes to the buffer , so will have to be taken care of... self-describing 
bytes is one way , though it is also misinterpreted sometimes*/
#define PAD(pInBuf)\
{\
      UINT1 paddingBytes =  size%8 ? 8- size%8 : 0; \
      if(paddingBytes) \
      { \
          UINT1 *padString; \
          padString = MEM_MALLOC( sizeof(UINT1)*(paddingBytes),UINT1); \
          if(padString) \
          { \
              while(paddingBytes) \
                   paddingBytes--,padString[paddingBytes] = paddingBytes+1; \
              CRU_BUF_APPEND_CHAIN_STRING(pInBuf, padString, paddingBytes); \
              MEM_FREE( padString); \
          } \
          else \
          { \
                fprintf(stderr, " no memory in PAD() macro in desCrypt.c\n"); \
                exit(0); \
                return FALSE; \
          } \
       } \
}

/**********UNPAD to be written...********************************************/
#define UNPAD(pInBuf)

INT1 Crypt(UINT1* ,UINT2 size, UINT1 (*kn)[8], UINT1* Nonce, OPER operation);
#define PPPDESIF_H
#endif


#endif  /* __PPP_PPPDES_H__ */
