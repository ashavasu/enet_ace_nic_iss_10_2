/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: desproto.h,v 1.2 2011/10/13 10:31:20 siva Exp $
 *
 * Description:This files contains the macros used by the
 *             modules of PPP subsystem.
 *
 *******************************************************************/
#ifndef __PPP_DESPROTO_H__
#define __PPP_DESPROTO_H__


tDESEParams* DESECreateIf(const UINT1 *);
UINT2 DESEConstructResetReqPkt(t_MSG_DESC *);
INT1 DESEEncrypt(t_MSG_DESC **pCruBuffer, VOID * pIfInfo);
INT1 DESEDecrypt(t_MSG_DESC **pCruBuffer, VOID * pIfInfo);
VOID DESEHandleResetPkt(VOID * HistoryList);
INT1 DESEValidateResetPkt(UINT2 Length, UINT1 Code);
INT1 DESEInit(VOID *Size, tDESEParams* pInParams, tDesIfInfo **pDesIfInfo);
VOID DESEDeInit(VOID *pDesIfInfo);

INT1 TripleDESEEncrypt(t_MSG_DESC **, VOID * );
INT1 TripleDESEDecrypt(t_MSG_DESC **, VOID * );
UINT2 TripleDESEConstructResetReqPkt(t_MSG_DESC *);
VOID TripleDESEHandleResetPkt(VOID * );
INT1 TripleDESEValidateResetPkt(UINT2 , UINT1 );
INT1 TripleDESEInit(VOID *, tTrplDESEParams* , tTrplDesIfInfo **);
VOID TripleDESEDeInit(VOID *);

#endif  /* __PPP_DESPROTO_H__ */
