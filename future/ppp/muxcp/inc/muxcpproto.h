/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: muxcpproto.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains Prototypes for the MuxCP functions 
 * used internally by MuxCP module
 *
 *******************************************************************/
#ifndef __PPP_MUXCP_PROTO_H__
#define __PPP_MUXCP_PROTO_H__

/* Prototypes for MuxCP functions used internally by MuxCP module */

tMuxCPIf *MuxCPCreateIf(tPPPIf *pIf);
VOID MuxCPDeleteIf(tMuxCPIf *pIp6cpPtr);
VOID MuxCPEnableIf(tMuxCPIf *pIp6cpPtr);
VOID MuxCPDisableIf(tMuxCPIf *pIp6cpPtr);
INT1 MuxCPInit(tMuxCPIf *pIp6cpPtr);
VOID MuxCPOptionInit(tMuxCPOptions *pMUXCPOpt);


VOID MuxCPUp(tGSEM *pIp6cpGSEM);
VOID MuxCPDown(tGSEM *pIp6cpGSEM);
VOID MuxCPFinished(tGSEM *pIp6cpGSEM);
VOID MuxCPRxData(tGSEM * pGSEM);
VOID MuxCPTxData(tGSEM * pGSEM);

INT1 MuxCPProcessDefPIdConfReq(tGSEM *, t_MSG_DESC *, tOptVal , UINT2 , UINT1 );
INT1 MuxCPProcessDefPIdConfNak(tGSEM *, t_MSG_DESC *, tOptVal );
INT1 MuxCPProcessDefPIdConfRej(tGSEM *);
VOID *MuxCPReturnDefPIdPtr(tGSEM  *pGSEM, UINT1 *OptLen);




tGSEM *MuxCPGetSEMPtr(tPPPIf *pIf);
INT1 MuxCPCopyOptions(tGSEM *pGSEM, UINT1 Flag, UINT1 PktType);

INT1 CheckAndGetMuxCPPtr (tPPPIf *);
tMuxCPIf *PppGetMuxCPIfPtr (UINT4 );
tMuxCPIf *PppCreateMuxCPIfPtr (UINT4 );

#endif  /* __PPP_MUXCP_PROTO_H__ */
