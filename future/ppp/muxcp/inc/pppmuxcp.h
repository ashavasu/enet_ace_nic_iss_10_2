/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: pppmuxcp.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
*
* Description:This file contains the typedefs, #defines and
*             data structures used by the MUXCP Module.
*
*******************************************************************/

#ifndef __PPP_MUXCP_H__
#define __PPP_MUXCP_H__

/*********************************************************************/
/*                              CONSTANTS                           */
/*********************************************************************/

/* CONFIGURATION OPTION VALUES  for MUXCP packets */

#define  IP6_DATAGRAM                   0x0057 
#define  IPHC_PROTOCOL                  0x0061

#define  MUXCP_DATAGRAM                 0x0059 
#define  IPHC_TCP_PROT                  0x0063 
#define  IPHC_NON_TCP_PROT              0x0065
#define  IPHC_UDP8_PROT                 0x0067
#define  IPHC_RTP8_PROT                 0x0069
#define  IPHC_TCP_NODELTA               0x2063
#define  IPHC_CONTEXT_STATE             0x2065
#define  IPHC_UDP16_PROT                0x2067
#define  IPHC_RTP16_PROT                0x2069



#define  MUXCP_PROTOCOL                 0x8059 
#define  MAX_MUXCP_OPT_TYPES           2 
#define  MUXCP_DEFPID_DISCRETE_VALUES  12 

#define  MUXCP_DEFPID_OPTION           1 


#define  MUXCPUP                       1 
#define  MUXCPDOWN                     2 
#define  MUXCPDELETE                   3 

#define  MUXCP_DEFPID_IDX              0


typedef struct {
    UINT4   DiscHeader;
    UINT2   Value[MUXCP_DEFPID_DISCRETE_VALUES];
} tMuxCPDefPIdDiscVal;

typedef struct {
    UINT2     u2DefPID;
    UINT2     u2Rsvd;
} tMuxCPOptions;

typedef struct {
    tMuxCPOptions   MuxCPOptionsDesired;
    tMuxCPOptions   MuxCPOptionsAllowedForPeer;
    tMuxCPOptions   MuxCPOptionsAckedByPeer;
    tMuxCPOptions   MuxCPOptionsAckedByLocal;
    tGSEM           MuxCPGSEM;
    VOID            *HLHandle; 
    UINT2           u2MaxSubFrmLen;
    UINT1           OperStatus;
    UINT1           AdminStatus;
    UINT1           RowStatus;
    UINT1	    u1Rsvd1;
    UINT2	    u2Rsvd2;
} tMuxCPIf;

typedef struct {
    UINT2               u2AllocCounter;
    UINT2               u2FreeCounter;
    UINT2               u2IfFreeCounter;
    UINT2		u2Rsvd;
}tMuxcpCounters;

extern tMuxcpCounters gMuxCpCounters;

#define   ALLOC_MUXCP_IF()    \
                 (((pAllocPtr = (UINT1 *)CALLOC(1,sizeof(tMuxCPIf)))==NULL)  \
                        ?  (NULL)  \
                        :  (gMuxCpCounters.u2AllocCounter++,pAllocPtr))
#define   FREE_MUXCP_IF(ptr)     {        \
                                FREE(ptr);       \
                                gMuxCpCounters.u2FreeCounter++; \
}


#endif 
