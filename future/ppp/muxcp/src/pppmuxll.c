/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppmuxll.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description: Low Level Routines for MuxCP module.
 *
 *******************************************************************/
#include "muxcpcom.h"
#include "snmphdrs.h"
#include "pppsnmpm.h"
#include "globexts.h"
#include "llproto.h"
#include "ppmuxlow.h"

#define  IP_DATAGRAM                    0x0021
#define  IP_COMP_TCP_DATA               0x002d
#define  IP_UNCOMP_TCP_DATA             0x002f

/******************************************************************************/
INT1
CheckAndGetMuxCPPtr (tPPPIf * pIf)
{
    if (pIf->pMuxcpPtr != NULL)
    {
        return (PPP_SNMP_OK);
    }
    return (PPP_SNMP_ERR);
}

tMuxCPIf           *
PppGetMuxCPIfPtr (UINT4 Index)
{
    tPPPIf             *pIf = NULL;

    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if ((pIf != NULL) && (pIf->LinkInfo.IfIndex == Index))
            return (pIf->pMuxcpPtr);
    }
    return ((tMuxCPIf *) NULL);
}

tMuxCPIf           *
PppCreateMuxCPIfPtr (UINT4 Index)
{

    tPPPIf             *pIf = NULL;

    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if ((pIf != NULL) && (pIf->LinkInfo.IfIndex == Index))
        {
            if (pIf->BundleFlag != BUNDLE
                && pIf->MPInfo.MemberInfo.pBundlePtr != NULL)
            {
                return (NULL);
            }
            if ((pIf->pMuxcpPtr = MuxCPCreateIf (pIf)) == NULL)
            {
                return (NULL);
            }
            return (pIf->pMuxcpPtr);
        }
    }
    return (NULL);
}

/* LOW LEVEL Routines for Table : PppExtMuxCpTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppExtMuxCpTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppExtMuxCpTable (INT4 i4IfIndex)
{
    if (PppGetMuxCPIfPtr (i4IfIndex) == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppExtMuxCpTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppExtMuxCpTable (INT4 *pi4IfIndex)
{
    if (nmhGetFirstIndex (pi4IfIndex, MuxcpInstance, FIRST_INST) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppExtMuxCpTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppExtMuxCpTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    if (nmhGetNextIndex (&i4IfIndex, MuxcpInstance, NEXT_INST) == SNMP_SUCCESS)
    {
        *pi4NextIfIndex = i4IfIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppExtMuxCpOperStatus
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppExtMuxCpOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtMuxCpOperStatus (INT4 i4IfIndex,
                             INT4 *pi4RetValPppExtMuxCpOperStatus)
{
    tMuxCPIf           *pMuxcpIf = NULL;

    if ((pMuxcpIf = PppGetMuxCPIfPtr (i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValPppExtMuxCpOperStatus = pMuxcpIf->OperStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetPppExtMuxCpRemToLocDefaultPID
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppExtMuxCpRemToLocDefaultPID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtMuxCpRemToLocDefaultPID (INT4 i4IfIndex,
                                     INT4
                                     *pi4RetValPppExtMuxCpRemToLocDefaultPID)
{
    tMuxCPIf           *pMuxcpIf = NULL;

    if ((pMuxcpIf = PppGetMuxCPIfPtr (i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pMuxcpIf->OperStatus == STATUS_UP)
    {
        if (pMuxcpIf->MuxCPGSEM.pNegFlagsPerIf[MUXCP_DEFPID_IDX].FlagMask
            & ACKED_BY_LOCAL_MASK)
        {
            *pi4RetValPppExtMuxCpRemToLocDefaultPID =
                pMuxcpIf->MuxCPOptionsAckedByLocal.u2DefPID;
        }
        else
        {
            return SNMP_FAILURE;
        }

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : PppExtMuxCpConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppExtMuxCpConfigTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppExtMuxCpConfigTable (INT4 i4IfIndex)
{
    if (PppGetMuxCPIfPtr (i4IfIndex) == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppExtMuxCpConfigTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppExtMuxCpConfigTable (INT4 *pi4IfIndex)
{
    if (nmhGetFirstIndex (pi4IfIndex, MuxcpInstance, FIRST_INST) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppExtMuxCpConfigTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppExtMuxCpConfigTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    if (nmhGetNextIndex (&i4IfIndex, MuxcpInstance, NEXT_INST) == SNMP_SUCCESS)
    {
        *pi4NextIfIndex = i4IfIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppExtMuxCpConfigAdminStatus
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppExtMuxCpConfigAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtMuxCpConfigAdminStatus (INT4 i4IfIndex,
                                    INT4 *pi4RetValPppExtMuxCpConfigAdminStatus)
{
    tMuxCPIf           *pMuxcpIf = NULL;

    if ((pMuxcpIf = PppGetMuxCPIfPtr (i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValPppExtMuxCpConfigAdminStatus = pMuxcpIf->AdminStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetPppExtMuxCpConfigLocToRemDefaultPID
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppExtMuxCpConfigLocToRemDefaultPID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtMuxCpConfigLocToRemDefaultPID (INT4 i4IfIndex,
                                           INT4
                                           *pi4RetValPppExtMuxCpConfigLocToRemDefaultPID)
{
    tMuxCPIf           *pMuxcpIf = NULL;

    if ((pMuxcpIf = PppGetMuxCPIfPtr (i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValPppExtMuxCpConfigLocToRemDefaultPID =
        pMuxcpIf->MuxCPOptionsDesired.u2DefPID;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetPppExtMuxCpConfigMaxSubFrameLen
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppExtMuxCpConfigMaxSubFrameLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtMuxCpConfigMaxSubFrameLen (INT4 i4IfIndex,
                                       INT4
                                       *pi4RetValPppExtMuxCpConfigMaxSubFrameLen)
{
    tMuxCPIf           *pMuxcpIf = NULL;

    if ((pMuxcpIf = PppGetMuxCPIfPtr (i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValPppExtMuxCpConfigMaxSubFrameLen = pMuxcpIf->u2MaxSubFrmLen;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetPppExtMuxCpConfigRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppExtMuxCpConfigRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtMuxCpConfigRowStatus (INT4 i4IfIndex,
                                  INT4 *pi4RetValPppExtMuxCpConfigRowStatus)
{
    tMuxCPIf           *pMuxcpIf = NULL;

    if ((pMuxcpIf = PppGetMuxCPIfPtr (i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValPppExtMuxCpConfigRowStatus = ACTIVE;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppExtMuxCpConfigAdminStatus
 Input       :  The Indices
                IfIndex

                The Object 
                setValPppExtMuxCpConfigAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtMuxCpConfigAdminStatus (INT4 i4IfIndex,
                                    INT4 i4SetValPppExtMuxCpConfigAdminStatus)
{
    tMuxCPIf           *pMuxcpIf = NULL;

    if ((pMuxcpIf = PppGetMuxCPIfPtr (i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pMuxcpIf->AdminStatus = (UINT1) i4SetValPppExtMuxCpConfigAdminStatus;

    if (i4SetValPppExtMuxCpConfigAdminStatus == ADMIN_OPEN)
    {
        MuxCPEnableIf (pMuxcpIf);
    }
    else if (i4SetValPppExtMuxCpConfigAdminStatus == ADMIN_CLOSE)
    {
        MuxCPDisableIf (pMuxcpIf);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetPppExtMuxCpConfigLocToRemDefaultPID
 Input       :  The Indices
                IfIndex

                The Object 
                setValPppExtMuxCpConfigLocToRemDefaultPID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtMuxCpConfigLocToRemDefaultPID (INT4 i4IfIndex,
                                           INT4
                                           i4SetValPppExtMuxCpConfigLocToRemDefaultPID)
{
    tMuxCPIf           *pMuxcpIf = NULL;

    if ((pMuxcpIf = PppGetMuxCPIfPtr (i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pMuxcpIf->MuxCPOptionsDesired.u2DefPID =
        (UINT2) i4SetValPppExtMuxCpConfigLocToRemDefaultPID;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetPppExtMuxCpConfigMaxSubFrameLen
 Input       :  The Indices
                IfIndex

                The Object 
                setValPppExtMuxCpConfigMaxSubFrameLen
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtMuxCpConfigMaxSubFrameLen (INT4 i4IfIndex,
                                       INT4
                                       i4SetValPppExtMuxCpConfigMaxSubFrameLen)
{
    tMuxCPIf           *pMuxcpIf = NULL;

    if ((pMuxcpIf = PppGetMuxCPIfPtr (i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pMuxcpIf->u2MaxSubFrmLen = (UINT2) i4SetValPppExtMuxCpConfigMaxSubFrameLen;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetPppExtMuxCpConfigRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                setValPppExtMuxCpConfigRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtMuxCpConfigRowStatus (INT4 i4IfIndex,
                                  INT4 i4SetValPppExtMuxCpConfigRowStatus)
{
    tMuxCPIf           *pMuxcpIf = NULL;

    switch (i4SetValPppExtMuxCpConfigRowStatus)
    {
        case CREATE_AND_GO:
            if ((pMuxcpIf = PppCreateMuxCPIfPtr (i4IfIndex)) == NULL)
            {
                return SNMP_FAILURE;
            }
            pMuxcpIf->AdminStatus = ADMIN_OPEN;
            pMuxcpIf->RowStatus = ACTIVE;
            MuxCPEnableIf (pMuxcpIf);
            break;

        case DESTROY:
            if ((pMuxcpIf = PppGetMuxCPIfPtr (i4IfIndex)) == NULL)
            {
                return SNMP_FAILURE;
            }
            PPP_TRC1 (PPP_MUST,
                      "Deleting MuxCP Interface on interface [%d]\n",
                      i4IfIndex);
            MuxCPDisableIf (pMuxcpIf);
            MuxCPDeleteIf (pMuxcpIf);
            break;
        default:
            break;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppExtMuxCpConfigAdminStatus
 Input       :  The Indices
                IfIndex

                The Object 
                testValPppExtMuxCpConfigAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtMuxCpConfigAdminStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                       INT4
                                       i4TestValPppExtMuxCpConfigAdminStatus)
{
    if (PppGetMuxCPIfPtr (i4IfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValPppExtMuxCpConfigAdminStatus != ADMIN_OPEN) &&
        (i4TestValPppExtMuxCpConfigAdminStatus != ADMIN_CLOSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PppExtMuxCpConfigLocToRemDefaultPID
 Input       :  The Indices
                IfIndex

                The Object 
                testValPppExtMuxCpConfigLocToRemDefaultPID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtMuxCpConfigLocToRemDefaultPID (UINT4 *pu4ErrorCode,
                                              INT4 i4IfIndex,
                                              INT4
                                              i4TestValPppExtMuxCpConfigLocToRemDefaultPID)
{

    if (PppGetMuxCPIfPtr (i4IfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    switch (i4TestValPppExtMuxCpConfigLocToRemDefaultPID)
    {
        case IP_DATAGRAM:
        case IP6_DATAGRAM:
        case MUXCP_DATAGRAM:
        case IPHC_PROTOCOL:
        case IPHC_TCP_PROT:
        case IPHC_NON_TCP_PROT:
        case IPHC_UDP8_PROT:
        case IPHC_RTP8_PROT:
        case IPHC_TCP_NODELTA:
        case IPHC_CONTEXT_STATE:
        case IPHC_UDP16_PROT:
        case IPHC_RTP16_PROT:
            return SNMP_SUCCESS;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2PppExtMuxCpConfigMaxSubFrameLen
 Input       :  The Indices
                IfIndex

                The Object 
                testValPppExtMuxCpConfigMaxSubFrameLen
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtMuxCpConfigMaxSubFrameLen (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                          INT4
                                          i4TestValPppExtMuxCpConfigMaxSubFrameLen)
{
    if (PppGetMuxCPIfPtr (i4IfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValPppExtMuxCpConfigMaxSubFrameLen < 0)
        || (i4TestValPppExtMuxCpConfigMaxSubFrameLen > 0xFFFF))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PppExtMuxCpConfigRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                testValPppExtMuxCpConfigRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtMuxCpConfigRowStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                     INT4 i4TestValPppExtMuxCpConfigRowStatus)
{
    tMuxCPIf           *pMuxcpIf = NULL;

    switch (i4TestValPppExtMuxCpConfigRowStatus)
    {
        case CREATE_AND_GO:
            if ((pMuxcpIf = PppGetMuxCPIfPtr (i4IfIndex)) != NULL)
            {
                PPP_TRC (MGMT, "Interface Already Created");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
        case DESTROY:
            if ((pMuxcpIf = PppGetMuxCPIfPtr (i4IfIndex)) == NULL)
            {
                PPP_TRC (MGMT, "Interface Not Created");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
            break;
    }
    return SNMP_SUCCESS;
}
