/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppmuxcp.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains callback routines of MuxCP module.
 *             It also contains routines for the input MuxCP packet 
 *             processing.
 *
 *******************************************************************/

#include "muxcpcom.h"

#define  IP_DATAGRAM                    0x0021
#define  IP_COMP_TCP_DATA               0x002d
#define  IP_UNCOMP_TCP_DATA             0x002f

 /* GLOBALS */
tMuxcpCounters      gMuxCpCounters;

tOptHandlerFuns     gMuxCPDefPIdHandlingFuns = {
    MuxCPProcessDefPIdConfReq,
    NULL,
    NULL,
    MuxCPReturnDefPIdPtr,
    NULL
};

tGSEMCallbacks      gMuxCPGSEMCallback = {
    MuxCPUp,
    MuxCPDown,
    NULL,
    MuxCPFinished,
    NULL,                        /*RxData */
    NULL,                        /*TxData */
    NULL,
    NULL
};

tMuxCPDefPIdDiscVal gMuxCPDefPIdDiscVal = {
    MUXCP_DEFPID_DISCRETE_VALUES,
    {
     IP_DATAGRAM,
     IP6_DATAGRAM,
     MUXCP_DATAGRAM,
     IPHC_PROTOCOL,
     IPHC_TCP_PROT,
     IPHC_NON_TCP_PROT,
     IPHC_UDP8_PROT,
     IPHC_RTP8_PROT,
     IPHC_TCP_NODELTA,
     IPHC_CONTEXT_STATE,
     IPHC_UDP16_PROT,
     IPHC_RTP16_PROT}
};

tGenOptionInfo      gMuxCPGenOptionInfo[] = {

    {MUXCP_DEFPID_OPTION, DISCRETE, BYTE_LEN_2, 4,
     &gMuxCPDefPIdHandlingFuns, {(VOID *) &gMuxCPDefPIdDiscVal},
     NOT_SET, 0, 0}

};

tOptNegFlagsPerIf   gMuxCPOptPerIntf[] = {
    {MUXCP_DEFPID_OPTION, ALLOWED_FOR_PEER_SET | DESIRED_SET, 0}
};

/***********************************************************************
*  Function Name : MuxCPCreateIf
*  Description   :
*          This procedure creates an entry in the MuxCP interface data 
*  base corresponding to IfId. It also initializes the created entry to the 
*  default values  by calling the MuxCPinit() function.
*  Parameter(s)  :
*         pIf -    pointer to the PPP interface structure
*  Return Values : 
*      pointer to the newly created  interface structure , if the creation is 
*        success
*      NULL if it fails to create the interface.
*********************************************************************/
tMuxCPIf           *
MuxCPCreateIf (tPPPIf * pIf)
{
    tMuxCPIf           *pMuxcpPtr = NULL;

    if ((pMuxcpPtr = (tMuxCPIf *) ALLOC_MUXCP_IF ()) != NULL)
    {
        if (MuxCPInit (pMuxcpPtr) != NOT_OK)
        {
            pMuxcpPtr->MuxCPGSEM.pIf = pIf;
            return (pMuxcpPtr);
        }
    }
    PPP_TRC (ALL_FAILURE, "ERROR: MuxCP Alloc/Init failure .\n");
    return (NULL);
}

/*********************************************************************
*  Function Name : MuxCPDeleteIf
*  Description   :
*               This procedure deletes an MuxCP interface  entry from the 
*  database and deallocates memory corresponding to the interface.
*  Parameter(s)  :
*         pMuxcpPtr -  points to  the MuxCP interface structure to be deleted.
*  Return Values : VOID
*********************************************************************/
VOID
MuxCPDeleteIf (tMuxCPIf * pMuxcpPtr)
{
    pMuxcpPtr->MuxCPGSEM.pIf->pMuxcpPtr = NULL;
    GSEMDelete (&(pMuxcpPtr->MuxCPGSEM));
    FREE_MUXCP_IF (pMuxcpPtr);
    return;
}

/*********************************************************************
*  Function Name : MuxCPEnableIf
*  Description   :
*              This function is called when there is an  MuxCP Admin Open 
*  for an interface from the SNMP It  triggers an OPEN event to the GSEM and 
*  passes the MuxCP tGSEM corresponds to this interface as parameter.
*  Parameter(s)    :
*    pMuxcpPtr    -    Points to the MuxCP interface structure to be enabled
*  Return Values : VOID
*********************************************************************/
VOID
MuxCPEnableIf (tMuxCPIf * pMuxcpPtr)
{
    UINT1               Index;
    tPPPIf             *pIf;

    if (pMuxcpPtr->MuxCPGSEM.pIf->BundleFlag == BUNDLE)
    {
        SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
        {
            if (pIf->MPInfo.MemberInfo.pBundlePtr != NULL
                && pIf->MPInfo.MemberInfo.pBundlePtr ==
                pMuxcpPtr->MuxCPGSEM.pIf)
            {
                pIf->pMuxcpPtr = pMuxcpPtr;
            }
        }
    }

    pMuxcpPtr->AdminStatus = ADMIN_OPEN;

    /* Check whether the link is already in the Operational state. 
       If so, then indicate the GSEM about the  link UP 
     */

    if ((pMuxcpPtr->MuxCPGSEM.CurrentState != REQSENT)
        || (pMuxcpPtr->MuxCPGSEM.CurrentState != ACKRCVD)
        || (pMuxcpPtr->MuxCPGSEM.CurrentState != ACKSENT))
    {
        MEMCPY (&pMuxcpPtr->MuxCPOptionsAckedByPeer,
                &pMuxcpPtr->MuxCPOptionsDesired, sizeof (tMuxCPOptions));
        for (Index = 0; Index < MAX_MUXCP_OPT_TYPES; Index++)
        {
            if (pMuxcpPtr->MuxCPGSEM.pNegFlagsPerIf[Index].
                FlagMask & DESIRED_SET)
            {
                pMuxcpPtr->MuxCPGSEM.pNegFlagsPerIf[Index].FlagMask |=
                    ACKED_BY_PEER_SET;
            }
            else
            {
                pMuxcpPtr->MuxCPGSEM.pNegFlagsPerIf[Index].FlagMask &=
                    ACKED_BY_PEER_NOT_SET;
            }
        }
    }

    GSEMRun (&pMuxcpPtr->MuxCPGSEM, OPEN);

    if ((pMuxcpPtr->MuxCPGSEM.pIf->LcpStatus.LinkAvailability ==
         LINK_AVAILABLE) && (pMuxcpPtr->MuxCPGSEM.CurrentState != CLOSED))
    {
        GSEMRun (&pMuxcpPtr->MuxCPGSEM, UP);
    }

    GSEMRestartIfNeeded (&pMuxcpPtr->MuxCPGSEM);

    return;
}

/*********************************************************************
*  Function Name : MuxCPDisableIf
*  Description   :
*          This function is called when there is an Admin Close for an 
*  interface from the SNMP It triggers a CLOSE event to the GSEM and passes 
*  the MuxCP GSEMStructure corresponds to this interface as a parameter. 
*  Parameter(s)  :
*            pMuxcpPtr - points to  the MuxCP interface structure to be  disabled
*  Return Values : VOID
*********************************************************************/
VOID
MuxCPDisableIf (tMuxCPIf * pMuxcpPtr)
{
    GSEMRun (&pMuxcpPtr->MuxCPGSEM, CLOSE);
    return;
}

/*********************************************************************
*  Function Name : MuxCPFinished
*  Description   :
*    This function is invoked when the higher layer is done with 
*  the link and the link is no longer needed. This invokes  procedures for 
*  terminating the link.
*         The pFinished  field of the GSEMCallback structure points 
*  to this function and is called from the GSEM Module.
*  Parameter(s)  :  
*            pGSEM  - pointer to MuxCP tGSEM corresponding 
*  to current interface
*  Return Value  :    VOID
*********************************************************************/
VOID
MuxCPFinished (tGSEM * pGSEM)
{
    tMuxCPIf           *pMuxcpPtr = NULL;
    UINT1               Index;

    pMuxcpPtr = pGSEM->pIf->pMuxcpPtr;
    MEMCPY (&pMuxcpPtr->MuxCPOptionsAckedByPeer,
            &pMuxcpPtr->MuxCPOptionsDesired, sizeof (tMuxCPOptions));
    for (Index = 0; Index < MAX_MUXCP_OPT_TYPES; Index++)
    {
        if (pGSEM->pNegFlagsPerIf[Index].FlagMask & DESIRED_SET)
        {
            pGSEM->pNegFlagsPerIf[Index].FlagMask |= ACKED_BY_PEER_SET;
        }
        else
        {
            pGSEM->pNegFlagsPerIf[Index].FlagMask &= ACKED_BY_PEER_NOT_SET;
        }
    }
    return;
}

/*********************************************************************
*  Function Name : MuxCPInit
*  Description   :
*              This function initializes the MuxCP GSEM and  sets 
*  the  MuxCP configuration option variables to their default values. It is 
*  invoked by the MuxCPCreateIf() function.
*  Parameter(s)  :
*        pMuxcpPtr -   points to the MuxCP interface structure
*  Return Values : OK/NOT_OK 
*********************************************************************/
INT1
MuxCPInit (tMuxCPIf * pMuxcpPtr)
{
    /* Initialize the Option fields */
    MuxCPOptionInit (&pMuxcpPtr->MuxCPOptionsDesired);
    MuxCPOptionInit (&pMuxcpPtr->MuxCPOptionsAllowedForPeer);
    MuxCPOptionInit (&pMuxcpPtr->MuxCPOptionsAckedByPeer);
    MuxCPOptionInit (&pMuxcpPtr->MuxCPOptionsAckedByLocal);

    pMuxcpPtr->AdminStatus = STATUS_DOWN;
    pMuxcpPtr->OperStatus = STATUS_DOWN;

    if (GSEMInit (&pMuxcpPtr->MuxCPGSEM, MAX_MUXCP_OPT_TYPES) == NOT_OK)
    {
        return (NOT_OK);
    }
    pMuxcpPtr->MuxCPGSEM.Protocol = MUXCP_PROTOCOL;
    pMuxcpPtr->MuxCPGSEM.pCallbacks = &gMuxCPGSEMCallback;
    pMuxcpPtr->MuxCPGSEM.pGenOptInfo = gMuxCPGenOptionInfo;
    MEMCPY (pMuxcpPtr->MuxCPGSEM.pNegFlagsPerIf, &gMuxCPOptPerIntf,
            (sizeof (tOptNegFlagsPerIf) * MAX_MUXCP_OPT_TYPES));
    return (OK);
}

/***********************************************************************/
/*                      INTERNAL  FUNCTIONS                            */
/***********************************************************************
*  Function Name : MuxCPOptionInit
*  Description   :
*               This function initializes the MuxCPOption structure and is
*  called by the MuxCPInit function.
*  Parameter(s)  :
*       pMuxCPOpt   -  points to the MuxCPOption structure  to be initialized.
*  Return Value  :
*********************************************************************/
VOID
MuxCPOptionInit (tMuxCPOptions * pMuxCPOpt)
{
    pMuxCPOpt->u2DefPID = IP_DATAGRAM;
    return;
}

/*********************************************************************
*  Function Name : MuxCPUp
*  Description   :
*              This function is called once the network level configuration  
*  option is negotiated  successfully. This function indicates  to the higher 
*  layers  that the MuxCP is up.  The pUp  field of  the MuxCPGSEMCallback points *  to this function and is called from the GSEM Module.
*  Parameter(s)  :
*            pMuxCPGSEM  - pointer to the MuxCP tGSEM 
*                                   corresponding to the current interface.
*  Return Values : VOID 
*********************************************************************/
VOID
MuxCPUp (tGSEM * pMuxCPGSEM)
{
    tMuxCPIf           *pMuxCPIf = NULL;

    pMuxCPIf = (tMuxCPIf *) pMuxCPGSEM->pIf->pMuxcpPtr;
    pMuxCPIf->OperStatus = STATUS_UP;
    PPPHLINotifyProtStatusToHL (pMuxCPGSEM->pIf, MUXCP_PROTOCOL, UP);
    return;
}

/*********************************************************************
*  Function Name : MuxCPDown
*  Description   :
*          This function is invoked  when the MuxCP protocol is leaving the 
*  OPENED state. It  indicates the higher layers  about the operational status 
*  of the MuxCP.  The pDown  field of  the MuxCPGSEMCallback structure  points to 
*  this function and is called from the GSEM Module.
*  Parameter(s)  :
*            pMuxCPGSEM  - pointer to the MuxCP tGSEM 
*                       corresponding to the current interface.
*  Return Values : VOID 
*********************************************************************/
VOID
MuxCPDown (tGSEM * pMuxCPGSEM)
{
    tPPPIf             *pIf = NULL;
    tMuxCPIf           *pMuxCPIf = NULL;

    pIf = pMuxCPGSEM->pIf;
    pMuxCPIf = (tMuxCPIf *) pMuxCPGSEM->pIf->pMuxcpPtr;
    PPPHLINotifyProtStatusToHL (pIf, MUXCP_PROTOCOL, DOWN);
    MuxCPFinished (pMuxCPGSEM);
    pMuxCPIf->OperStatus = STATUS_DOWN;

    MEMCPY (&pMuxCPIf->MuxCPOptionsAckedByPeer,
            &pMuxCPIf->MuxCPOptionsDesired, sizeof (tMuxCPOptions));
    return;
}

/*********************************************************************
*  Function Name : MuxCPCopyOptions 
*  Description   :
*    This function is used to copy the MuxCP options.
*  Input         :
*    pGSEM        -     Points to the GSEM structure
*    Flag        -     Specifies whether to alloc or free the temporary 
*                    variable.
*    PktType        -    Specifies whether the function is called from request
*                    processing or Nak processing.                  
*  Return Value        :
*                    DISCARD on allocation error, OK otherwise.
*********************************************************************/
INT1
MuxCPCopyOptions (tGSEM * pGSEM, UINT1 Flag, UINT1 PktType)
{
    UINT1               Size = 0;
    tMuxCPIf           *pMuxCPIf = NULL;
    tMuxCPOptions      *pTemp = NULL;

    pMuxCPIf = (tMuxCPIf *) pGSEM->pIf->pMuxcpPtr;
    Size = sizeof (tMuxCPOptions);

    if (PktType == CONF_REQ_CODE)
    {
        pTemp = &pMuxCPIf->MuxCPOptionsAckedByLocal;
    }
    else
    {
        pTemp = &pMuxCPIf->MuxCPOptionsAckedByPeer;
    }

    if (Flag == NOT_SET)
    {
        if ((pCopyPtr = (UINT1 *) ALLOC_STR (Size)) == NULL)
        {
            PRINT_MEM_ALLOC_FAILURE;
            return (DISCARD);
        }
        MEMCPY (pCopyPtr, (UINT1 *) pTemp, Size);
        if (PktType == CONF_REQ_CODE)
        {
            MuxCPOptionInit (&pMuxCPIf->MuxCPOptionsAckedByLocal);
        }
    }
    else
    {
        if (Flag == SET)
        {
            MEMCPY (pTemp, (tMuxCPOptions *) pCopyPtr, Size);
        }
        if (pCopyPtr != NULL)
        {
            FREE_STR (pCopyPtr);
            pCopyPtr = NULL;
        }
    }
    return (OK);
}

/*********************************************************************
*  Function Name : MuxCPGetSEMPtr
*  Description   :
*    This function returns the pointer to the MuxCP GSEM taking the tPPPIf
*   structure as a parameter.
*  Input         :
*       pIf -   Points to the PPP Interface structure.
*  Return Value  :
*           Returns a pointer to the MuxCP GSEM.
*********************************************************************/
tGSEM              *
MuxCPGetSEMPtr (tPPPIf * pIf)
{
    tMuxCPIf           *pMuxCPIf;

    if (pIf->pMuxcpPtr != NULL)
    {
        pMuxCPIf = (tMuxCPIf *) pIf->pMuxcpPtr;
        return (&pMuxCPIf->MuxCPGSEM);
    }
    return (NULL);
}

/*********************************************************************
*  Function Name : MuxCPProcessDefPIdConfReq
*  Description   :
*          This function processes the Compression Option in the
*  CONF_REQ packet and builds appropriate response.
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer
*     tOptVal    -  Option Value received  to be processed
*     Offset     -  indicates the place in the outgoing buffer at which
*                   the option value is appended
*  PresenceFlag  -  indicates whether the option is present in the recd.
*                   CONF_REQ packet
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer
*********************************************************************/
INT1
MuxCPProcessDefPIdConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt,
                           tOptVal OptVal, UINT2 Offset, UINT1 PresenceFlag)
{
    tMuxCPIf           *pMuxcpPtr = NULL;

    pMuxcpPtr = pGSEM->pIf->pMuxcpPtr;
    PPP_UNUSED (pInPkt);

    if (PresenceFlag == SET)
    {
        pMuxcpPtr->MuxCPOptionsAckedByLocal.u2DefPID = OptVal.ShortVal;
        return (ACK);
    }
    else
    {
        ASSIGN2BYTE (pGSEM->pOutParam, Offset,
                     pMuxcpPtr->MuxCPOptionsAllowedForPeer.u2DefPID);
        return (BYTE_LEN_2);
    }
}

/*********************************************************************
*  Function Name : MuxCPReturnDefPIdPtr
*  Description   :
*     This function returns the address of the location where the 
*     Compression Option value to be included in the CONF_REQ, is stored.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*     OptLen     -  used to return the length of the option value (in
*                   case the option is of 'STRING' type)
*  Return Value  :  returns the option data address
*********************************************************************/
VOID               *
MuxCPReturnDefPIdPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    tMuxCPIf           *pMuxcpPtr = NULL;

    PPP_UNUSED (OptLen);
    pMuxcpPtr = pGSEM->pIf->pMuxcpPtr;

    return (&pMuxcpPtr->MuxCPOptionsAckedByPeer.u2DefPID);
}
