/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: llproto.h,v 1.2 2011/10/13 10:31:25 siva Exp $
 *
 * Description:This file contains prototypes for the low-level 
 * functions of PPP stack
 *
 *******************************************************************/

#include "pplcplow.h"
#include "pppoelow.h"
#include "stdiplow.h"
#include "pppgelow.h"
#include "stdaulow.h"
#include "stdlclow.h"
#include "stdbclow.h"

INT1 nmhValidateIndex (INT4 IfIndex);
INT1 nmhValidateDoubleIndex (INT4 IfIndex, INT4 IfIndex1);
INT1 getInstanceForTwoIndices(INT4 *Index1, INT4 *Index2, UINT1 InstanceType, INT1 InstCode);
INT1 getInstanceForOneIndex(INT4 *Index, UINT1 InstanceType, INT1 InstCode);
INT1 nmhGetFirstIndex (INT4 *pIndex, UINT1 InstanceType, INT1 InstCode);
INT1 nmhGetNextIndex (INT4 *pIndex, UINT1 InstanceType, INT1 InstCode);
INT1 nmhGetFirstDoubleIndex (INT4 *pIndex1, INT4 *pIndex2, INT1 InstanceType, INT1 InstCode);
INT1 nmhGetNextDoubleIndex (INT4 *pIndex1, INT4 *pIndex2, INT1 InstanceType, INT1 InstCode);
VOID SNMPInitInstance(VOID);



