/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppupnp.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 ********************************************************************/

PUBLIC UINT4 PppSecuritySecretsIdentity [];
PUBLIC UINT1 PppSecuritySecretsTableINDEX [];
PUBLIC UINT4 PppSecuritySecretsSecret [];
PUBLIC tSNMP_OID_TYPE pppSecurityChapMD5ProtocolOID;
PUBLIC tSNMP_OID_TYPE pppSecurityMSCHAPProtocolOID;
PUBLIC tSNMP_OID_TYPE pppSecurityPapProtocolOID;
