/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppptimer.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description: This file contains the typedefs of timer module 
 *
 *******************************************************************/
#ifndef __PPP_PPPTIMER_H__
#define __PPP_PPPTIMER_H__

/* INCLUDE FILES */



#define   PPP_INIT_TIMER(TimerNode)    TimerNode.TimerId = TIMER_INACTIVE

/* TYPE DEFINITIONS */

typedef enum {
	NULL_FRAG_TIMER,
	REASSEMBLY_TIMER,
	PERIODIC_ECHO_TIMER,
    KEEP_ALIVE_TIMER,
#ifdef BAP
	BAP_REQUEST_TIMER,
#endif
    PAD_EXPECT_TIMER,
	CALLBACK_DISC_TIMER,
	CALLBACK_DIAL_TIMER,
	RESTART_TIMER,
	PAP_REQ_TIMER,
	PAP_RESP_TIMER,
	CHAP_CHAL_TIMER,
	CHAP_RECHAL_TIMER,
	CHAP_RESP_TIMER,
    EAP_IDENTITY_TIMER, 
	EAP_MD5_CHAL_TIMER,
	EAP_MD5_RECHAL_TIMER,
	ECHO_REQ_TIMER,
	LCP_IDLE_TIMER,
	LQM_TO_SEND_TIMER,
	LQM_TO_EXPECT_TIMER,
	TIMER_INACTIVE
} ePPPTimerId;


typedef tTmrAppTimer tTimer;

typedef struct {
	tTimer TimerNode;
	ePPPTimerId TimerId;
	UINT4 Param1;
} tPPPTimer;


#endif  /* __PPP_PPPTIMER_H__ */
