/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppptest.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 *******************************************************************/
#ifndef __PPP_PPPTEST_H__
#define __PPP_PPPTEST_H__



#define  MAX_PPP_PKT_SIZE    8500 
#define  KEY                 1999 

#define  TEST_DIAL_EVT       25   
#define  TEST_DIAL_ISP       1    
#define  TEST_CALL_RESPONSE  2    
#define  TEST_CALL_FAILURE   3    



typedef struct msgbuf1 {
	UINT4 mtype;
	UINT1 mtext[MAX_PPP_PKT_SIZE];
} tQueMsg;

#ifdef GLOBAL_VAR

UINT1 DefValue;
tQueMsg OutMsg, InMsg;
INT1    NumBundles;
INT2 OutQid;
INT2 InQid;
INT2 TestQid;
INT1 Arg1;
INT2 Arg2;
INT2 Arg3;
INT2 Arg4;
INT4 Arg5;
UINT4     FragNum;
tPPPTimer DbwDeleteTimer;
tPPPTimer DbwInsertTimer;

#else


extern UINT2 MaxIf;
extern UINT1 DefValue;
extern INT1 Arg1;
extern INT2 Arg2;
extern INT2 Arg3;
extern INT2 Arg4;
extern INT4 Arg5;
extern tQueMsg OutMsg, InMsg;
extern INT2 OutQid;
extern INT2 InQid;
extern INT2 TestQid;
extern tPPPTimer DbwDeleteTimer;
extern tPPPTimer DbwInsertTimer;
extern INT4 DummyTaskId;
extern UINT4     FragNum;
#endif


#endif  /* __PPP_PPPTEST_H__ */
