/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppproto.h,v 1.2 2011/10/13 10:31:25 siva Exp $
 *
 * Description:This file contains the Function prototypes for functions
 * used internally by the main module
 *
 *******************************************************************/
#ifndef __PPP_PPPPROTO_H__
#define __PPP_PPPPROTO_H__


VOID PPPInit      (VOID);
VOID PPPDeInit    (VOID);
VOID PPPEnable    (VOID);
VOID PPPDisable   (VOID);
VOID InitializePPP(VOID);
VOID PPPStart     (VOID);

VOID GetFCSAvailability(VOID);
INT1 GetEncapIndex     (UINT1  IfType);

VOID PPPProcessTimeOut(VOID);

/* Changed function name for readability - FSAP2 */
VOID PPPInitTimerList  (VOID);
VOID PPPDeInitTimerList(VOID);

VOID PPPListActiveTimers(VOID);
VOID PrintTimerId       (tPPPTimer *pTimer);

#ifdef __PPP_PPPLCP_H__
INT1
PPPIsAuthProtocolFound (UINT4 IfIndex, UINT2 Protocol);
INT4
L2TPPPPGetFreePppIfName (UINT1 *pu1Name);

VOID  MPIndicateBundleStatus   (tPPPIf *pBundleIf);

INT1 PPPLLICompressEncryptPkt  (tPPPIf *pIf, t_MSG_DESC **pOutPDU,
                                UINT2 Length, UINT2 Protocol);
VOID PPPLLITxHDLC              (tPPPIf *pIf, t_MSG_DESC *pOutPDU,
                                UINT2 Protocol);
INT1 PPPLLIDecodePkt           (tPPPIf *pIf, t_MSG_DESC **pInPDU,
                                UINT2 *Protocol, UINT1 CallFlag);
UINT1 PPPLLIProcessMiscProtPkts(tPPPIf *pIf,t_MSG_DESC *pInPDU, UINT2 Length,
                                UINT2 Protocol);

VOID PPPRxPktFromLL        (tPPPIf *pIf, t_MSG_DESC *pInPDU, UINT2 Length);
VOID PPPTxPktToHL          (tPPPIf *pIf, t_MSG_DESC *pInPDU, UINT2 Length,
                            UINT2 Protocol);
VOID PPPTxPktToLL          (tPPPIf *pIf,t_MSG_DESC *pOutPDU, UINT2 Length,
                            UINT2 Protocol);

VOID DisplayMessage  (tPPPIf *pIf, VOID *pStr, UINT2 Length, UINT4 CharSet,
                      UINT1 *LangTag);

#endif 

VOID GetVJCompAvailability(VOID);
#ifdef __PPP_PPPIPCP_H__
tIPCPCtrlBlk *GetIPCPCtrlInfo(VOID);
#endif


#ifdef __PPP_PPPIPXCP_H__
tIPXCPCtrlBlk *GetIPXCPCtrlInfo(VOID);
#endif

#ifdef __PPP_PPPBCP_H__
tBCPCtrlBlk *GetBCPCtrlInfo(VOID);
#endif

#endif  /* __PPP_PPPPROTO_H__ */
