/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppdebug.h,v 1.2 2014/10/08 11:00:07 siva Exp $
 *
 * Description:This file contains the headers files needed by 
 *             all the modules of the PPP subsystem
 *
 *******************************************************************/
#ifndef __PPP_PPPDEBUG_H__
#define __PPP_PPPDEBUG_H__

#include "trace.h"


#define INIT_SHUT           INIT_SHUT_TRC
#define MGMT                MGMT_TRC
#define DATA_PATH           DATA_PATH_TRC
#define CONTROL_PLANE       CONTROL_PLANE_TRC
#define DUMP                DUMP_TRC
#define OS_RESOURCE         OS_RESOURCE_TRC
#define ALL_FAILURE         ALL_FAILURE_TRC
#define BUFFER              BUFFER_TRC
#define PPP_MUST            0x00000100
#define DEBUG_TRC           0x00000200
#define EVENT_TRC           0x00000400

#define PPP_TRC_NAME "PPP"
#define PPP_TRC_LINE_SEPERATOR "----------------------"

#ifdef DEBUG_WANTED

#define PPP_DBG(fmt)\
 MOD_TRC(DesiredLevel,DEBUG_TRC,PPP_TRC_NAME,fmt)
#define PPP_DBG1(fmt,arg1)\
 MOD_TRC_ARG1(DesiredLevel,DEBUG_TRC,PPP_TRC_NAME,fmt,arg1)
#define PPP_DBG2(fmt,arg1,arg2)\
 MOD_TRC_ARG2(DesiredLevel,DEBUG_TRC,PPP_TRC_NAME,fmt,arg1,arg2)
#define PPP_DBG3(fmt,arg1,arg2,arg3)\
 MOD_TRC_ARG3(DesiredLevel,DEBUG_TRC,PPP_TRC_NAME,fmt,arg1,arg2,arg3)
#define PPP_DBG4(fmt,arg1,arg2,arg3,arg4)\
 MOD_TRC_ARG4(DesiredLevel,DEBUG_TRC,PPP_TRC_NAME,fmt,arg1,arg2,arg3,arg4)

#else

#define PPP_DBG(fmt)   
#define PPP_DBG1(fmt,arg1)
#define PPP_DBG2(fmt,a1,a2)
#define PPP_DBG3(fmt,a1,a2,a3)
#define PPP_DBG4(fmt,a1,a2,a3,a4)

#endif

#define PPP_PKT_DUMP(mask,pBuf,Length,fmt)\
 {\
  if(Length < 20) {\
   MOD_PKT_DUMP(DesiredLevel,mask,PPP_TRC_NAME,pBuf,Length,fmt)\
  }\
  else {\
   MOD_PKT_DUMP(DesiredLevel,mask,PPP_TRC_NAME,pBuf,20,fmt)\
  }\
 }

#define PPP_TRC(mask,fmt)\
 MOD_TRC(DesiredLevel,mask,PPP_TRC_NAME,fmt)
#define PPP_TRC1(mask,fmt,arg1)\
 MOD_TRC_ARG1(DesiredLevel,mask,PPP_TRC_NAME,fmt,arg1)
#define PPP_TRC2(mask,fmt,arg1,arg2)\
 MOD_TRC_ARG2(DesiredLevel,mask,PPP_TRC_NAME,fmt,arg1,arg2)
#define PPP_TRC3(mask,fmt,arg1,arg2,arg3)\
 MOD_TRC_ARG3(DesiredLevel,mask,PPP_TRC_NAME,fmt,arg1,arg2,arg3)
#define PPP_TRC4(mask,fmt,arg1,arg2,arg3,arg4)\
 MOD_TRC_ARG4(DesiredLevel,mask,PPP_TRC_NAME,fmt,arg1,arg2,arg3,arg4)
#define PPP_TRC5(mask,fmt,arg1,arg2,arg3,arg4,arg5)\
 MOD_TRC_ARG5(DesiredLevel,mask,PPP_TRC_NAME,fmt,arg1,arg2,arg3,arg4,arg5)
#define PPP_TRC6(mask,fmt,arg1,arg2,arg3,arg4,arg5,arg6)\
 MOD_TRC_ARG6(DesiredLevel,mask,PPP_TRC_NAME,fmt,arg1,arg2,arg3,arg4,arg5,arg6)


#define PRINT_MEM_ALLOC_FAILURE\
  PPP_TRC(OS_RESOURCE_TRC,"Memory Allocation Failed")


#endif  /* __PPP_PPPDEBUG_H__ */
