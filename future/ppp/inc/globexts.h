/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: globexts.h,v 1.4 2014/03/11 14:02:49 siva Exp $
 *
 * Description:This file cntains the prototypes for functions of all 
 * modules exported to other modules
 *
 *******************************************************************/
#ifndef __PPP_GLOBEXTS_H__
#define __PPP_GLOBEXTS_H__


VOID desdone(VOID); /* fn exported from des.c */


#ifdef __PPP_PPPECCMN_H__
VOID DeleteNodes(t_SLL *pSll); 
VOID OptionInit(t_SLL *InSll);
#endif



VOID FRLinkUp (UINT2 , UINT2);
VOID FRLinkDown (UINT2 , UINT2);
VOID FrmRxFR (VOID *, UINT2 , UINT2 , UINT2);
VOID FrmTxFR (VOID **, UINT2 *, UINT2 , UINT2);

VOID PrintBuffer(t_MSG_DESC *pMsg,UINT2 Length);
VOID PrintState(UINT1 State);
VOID PrintEvent(UINT1 Event);
void PPPPrintProtocol(UINT2 Protocol);



UINT1 MPCheckForInSequence(UINT2 Protocol);

#ifdef __PPP_PPPTIMER_H__
VOID PPPStartTimer(tPPPTimer *pTimer, ePPPTimerId TimerId, UINT4 TimeOut);
VOID PPPStopTimer(tPPPTimer *pTimer);
VOID PPPRestartTimer(tPPPTimer *pTimer, ePPPTimerId TimerId, UINT4 TimeOut);
#endif

#ifdef __PPP_PPPGSEM_H__
VOID PrintFlags (tGSEM *pGSEM );
VOID CallBackDisconnectTimeOut ( VOID *pGSEM );
VOID CallBackDialTimeOut ( VOID *pGSEM );
INT2 GetNextOptionFromList(tGSEM *pGSEM, UINT1 Module);
#endif

#ifdef __PPP_PPPLCP_H__
tPPPIf *PppGetPppIfPtr(UINT4 Index);
tPPPIf *PppGetIfPtr (UINT4 Index);
tPPPIf * SNMPGetPppIfPtr (UINT4 Index);

tPPPIf *CreateNewPIf(UINT4 Index);
VOID PppDecodeMsg(VOID);

INT1 CheckAndGetLinkPtr(tPPPIf *pIf);
VOID PrintOutStat(tPPPIf *pIf);
VOID PrintInStat(tPPPIf *pIf);
INT4 PPPCreateIf(UINT4 IfIndex);
VOID PPPRxPktFromHL(tPPPIf *pIf, t_MSG_DESC *pPDU, UINT2 Length, UINT2 Protocol);

VOID PPPHLIRxPkt(tPPPIf *pIf, t_MSG_DESC *pPDU, UINT2 Length, UINT2 Protocol);
VOID PPPHLINotifyProtStatusToHL(tPPPIf *pIf, UINT2 Protocol, UINT1 Status);
VOID PPPHLINotifyIfStatusToHL(tPPPIf *pIf, UINT1 Status);
VOID PPPHLIProtocolDisable(tPPPIf *pIf, UINT2 Protocol);
VOID PPPHLIProtocolEnable(tPPPIf *pIf, UINT2 Protocol, VOID *pNcpCtrlBlk);

VOID PPPIfEnable(tPPPIf *pIf);
VOID PPPIfdisable(tPPPIf *pIf);
VOID PPPLowerLinkUp(tPPPIf *pIf);
VOID PPPLowerLinkDown(tPPPIf *pIf);
VOID PPPDeleteIf(tPPPIf *pIf);

INT4 PppIsIfaceBCPStatusOpen(UINT4);
INT4 PppIsIfaceIPCPStatusOpen(UINT4);
INT4 PppIsIfaceLCPStatusOpen(UINT4);

VOID PPPLLITxPkt(tPPPIf *pIf, t_MSG_DESC *pOutPDU, UINT2 Length, UINT2 Protocol);
INT1 PPPLLIGetProtField(tPPPIf *pIf, t_MSG_DESC *pInPDU, UINT2 *Protocol, UINT1 Offset, UINT1 CallFlag);
UINT1 PPPLLIProcessPID(tPPPIf *pIf, t_MSG_DESC *pInPDU, UINT2 Protocol, UINT2 Offset, UINT1 CallFlag, UINT2 Length);
VOID PPPLLIPutPID(t_MSG_DESC *pOutPDU, UINT1  PFCEnableFlag, UINT2 Protocol);
INT1 PPPLLIEncapPID(tPPPIf *pIf, t_MSG_DESC **pOutPDU, UINT1  PFCEnableFlag);
VOID PPPLLICallTerminate ( tPPPIf *pIf );
VOID PPPLLICallSetUp ( tPPPIf *pIf );


UINT4 GetIfSpeed(tPPPIf *pIf);
INT1 GetIfType(UINT2  PortType);
VOID PPPIndicateAsyncParamsToLL (tPPPIf *pIf, UINT1 Flag);
VOID GetIfIndex(UINT1 IfType, tPPPIfId *pIfId);
UINT1 TakeLCPCallBackAction ( tPPPIf *pIf );
VOID UTILFillPktHeader(t_MSG_DESC *pOutBuf,UINT1 Code, UINT1 Identifier, UINT2 Length);
VOID UTILDiscardPkt(tPPPIf *pIf, UINT1 const *Msg);
INT1 UtilExtractAndValidateCPHdr(tPPPIf *pIf, t_MSG_DESC *pInPkt, UINT2 Length, UINT1 *Code, UINT1 *Id, UINT2 *Len);

VOID LCPAuthProcessFailed(tPPPIf *pIf);
VOID LCPUpdateAuthStatus(tPPPIf *pIf, UINT1 Status);
INT1 PPPSendRejPkt(tPPPIf *pIf, t_MSG_DESC *pErrpacket, UINT2 Length, UINT2 Protocol, UINT1 Code);
tGSEM *LCPGetSEMPtr(tPPPIf *pIf);
VOID LCPSendEchoReq(tPPPIf *pIf, UINT1 Flag);
VOID LCPSendEchoResponse(tPPPIf *pIf, UINT1 Id, UINT1 *pMsg, UINT2 MsgLen);
tPPPIf *LCPCreateIf(tPPPIfId *pIfId, UINT4 IfIndex, UINT4 PhyIfIndex);
VOID LCPDeleteIf(tPPPIf *pIf);
VOID LCPEnableIf(tPPPIf *pIf);
VOID LCPDisableIf(tPPPIf *pIf);
VOID LCPEchoReqTimeOut(VOID *pIf);
VOID LCPIdleTimeOut(VOID *pIf);
VOID LCPTriggerEventToNCP(tPPPIf *pIf, UINT1 Event, UINT1 LinkAvailStatus);
VOID LCPEnterNetworkLayerPhase(tPPPIf *pIf);
VOID LCPLeaveNetworkLayerPhase(tPPPIf *pIf);

INT1    CheckAndGetLqrPtr(tPPPIf *pIf);

INT1 CheckAndGetSecurityPtr(tPPPIf *pIf, UINT4 *n1, UINT4 *n2, UINT4 SecondIndex);
INT1 CheckAndGetSecretsPtr(tPPPIf *pIf, UINT4 *n1, UINT4 *n2, UINT4 SecondIndex);
VOID LCPProcessKeepAliveTimeOut(VOID *);

VOID PppPrintIfInfo(UINT4 u4IfIndex);
#endif

#ifdef __PPP_PPPLQM_H__
INT4 LCPIndicateQuality(tLQMIf *pLqmPtr, UINT1 Status);
VOID LQMSendTimeOut(VOID *pLqmPtr); 
VOID LQMExpectTimeOut(VOID *pLqmPtr); 
#endif



#ifdef __PPP_PPPMD5_H__
void PppMD5Init(tMD5_CTX *mdContext);
void PppMD5Update(tMD5_CTX *mdContext, unsigned char *inBuf, 
                  unsigned int inLen);
void PppMD5Final(tMD5_CTX *mdContext);
#endif

#ifdef __PPP_PPPMD4_H__
void MD4Init (MD4_CTX *context);
void MD4Update (MD4_CTX *context, unsigned char *input, unsigned int inputLen);
void MD4Final (unsigned char *digest, MD4_CTX *context);
#endif

#ifdef __PPP_PPPAUTH_H__
VOID PAPResponseTimeOut(VOID *pAuthPtr);
VOID PAPAuthWithPeer(tAUTHIf *pAuthPtr);
VOID CHAPAuthWithPeer(tAUTHIf *pAuthPtr);



tAUTHIf *AUTHCreate(tPPPIf *pIf);

INT4 PppGetNegAuthProtocol(UINT4, INT4*, INT4 *);

#endif

#ifdef __PPP_PPPIPCP_H__
VOID IPCPEnableIf(tIPCPIf *pIpcpPtr, tIPCPCtrlBlk *pIpcpCtrlBlk);
VOID IPCPDisableIf(tIPCPIf *pIpcpPtr);
INT1 IPCPCopyOptions(tGSEM *pGSEM, UINT1 Flag, UINT1 PktType);
tGSEM *IPCPGetSEMPtr(tPPPIf *pIf);
INT1 CheckAndGetIpPtr(tPPPIf *pIf);
#endif

/* IPv6CP Changes -- start */
#ifdef __PPP_PPPIPV6CP_H__
INT1 IP6CPCopyOptions(tGSEM *pGSEM, UINT1 Flag, UINT1 PktType);
tGSEM *IP6CPGetSEMPtr(tPPPIf *pIf);
INT1 CheckAndGetIp6Ptr(tPPPIf *pIf);
#endif
/* IPv6CP Changes -- End */
#ifdef __PPP_MUXCP_H__
tGSEM * MuxCPGetSEMPtr(tPPPIf *pIf);
INT1 MuxCPCopyOptions(tGSEM *pGSEM, UINT1 Flag, UINT1 PktType);
INT1 CheckAndGetMuxCPPtr(tPPPIf *pIf);
VOID MuxCPDeleteIf (tMuxCPIf *);
VOID MuxCPEnableIf (tMuxCPIf *);
VOID MuxCPDisableIf (tMuxCPIf *);
#endif
#ifdef __PPP_PPPMPLSCP_H__
tMPLSCPIf *MPLSCPCreateIf(tPPPIf *pIf);
VOID MPLSCPDeleteIf(tMPLSCPIf *pMplscpPtr);
VOID MPLSCPEnableIf(tMPLSCPIf *pMplscpPtr);
VOID MPLSCPDisableIf(tMPLSCPIf *pMplscpPtr);
tGSEM *MPLSCPGetSEMPtr(tPPPIf *pIf);
INT1 CheckAndGetMplsPtr(tPPPIf *pIf);
INT1 MPLSCPCopyOptions(tGSEM *pGSEM, UINT1 Flag, UINT1 PktType);
#endif

#ifdef __PPP_PPPVJ_H__
tPPP_COMPRESS *VJCOMPCreate (tPPPIf * );
#endif

#ifdef __PPP_PPPBCP_H__
INT1  CheckAndGetBCPMacPtr(tPPPIf *pIf, UINT4 *n1, UINT4 *n2, UINT4 SecondIndex);
INT1  CheckAndGetBCPPtr(tPPPIf *pIf);
VOID     *BCPEnable(tBCPIf *pIf);

#endif

#ifdef __PPP_PPPBAP_H__
#ifdef BAP
INT1 BAPAddLinkToBundle(tPPPIf *pBundleIf, tReqHandle *pRequestHandle, UINT1 ReqType, tBAPOpts *pBAPOptions);
INT1 BAPDeleteLinkFromBundle ( tPPPIf *pBundleIf, tPPPIf *pIf, tReqHandle *pRequestHandle );
VOID BAPCallNotificationFromRM ( tPPPIf *pBundleIf, tReqHandle *pRequestHandle, tPhNum PhNumber, UINT1 MsgCode, UINT1 Action );

VOID BAPInput ( tPPPIf  *pBundleIf, t_MSG_DESC *pInPkt, UINT2 Length );
tBAPInfo *BAPCreateAndInit ( tPPPIf  *pBundleIf );
VOID BAPDelete(tPPPIf  *pBundleIf);

VOID RMInit (VOID);
VOID RMDelete (VOID);
VOID RMCreateInitAndAddReqEntry (tPPPIf *pBundleIf, UINT1 Id, UINT1 Requester, UINT1 ReqType, tPPPIf *pIf);
VOID RMCallInfoToMP (tPPPIf *pIf, UINT1 *pIsInitiator, tPhNum *pLocalPhNum, tPhNum *pRemotePhNum);

VOID BAPTimeOut (VOID *pRequest);

#endif
#endif

#ifdef __PPP_PPPBACP_H__
#ifdef BACP
VOID BACPEnableIf(tBACPIf *pBACPIf);
VOID BACPDisableIf(tBACPIf *pBACPIf);
INT1 BACPInit(tPPPIf *pBundleIf);
INT1 BACPCopyOptions ( tGSEM *pGSEM, UINT1 Flag, UINT1 PktType );
tGSEM *BACPGetSEMPtr ( tPPPIf *pIf );

/* Related to SNMP */
INT1 CheckAndGetBACPPtr (tPPPIf* pIf);
tBACPIf *PppGetBACPIfPtr (UINT4 Index);

INT1 CheckAndGetRMInfo (tPPPIf* pIf);

#endif
#endif



#ifdef __PPP_PPPGZIPIF_H__
INT1 GzipHistoryInit(VOID ** pHistoryInfo, tCompressOperation Operation);
tGzipParams* GzipCreateIf(UINT2 WndSize);
INT1 GzipCreateHistory(VOID *pHistoryInfo,VOID *pGzipParams,tCompressOperation Dummy);
INT1 GzipDeleteHistory(VOID *pHistoryInfo);
INT1 GzipCompress(t_MSG_DESC **pCruBuffer, VOID *pHistoryInfo);
INT1 GzipDeCompress(t_MSG_DESC **pCruBuffer, VOID *pHistoryInfo);
UINT2 GzipConstructResetPkt(t_MSG_DESC *pCruBuffer, VOID* pHistoryInfo,VOID*  Dummy1, UINT2 Dummy2);
INT1 GzipHandleResetPkt(VOID* Voidp, UINT2 ummy);
INT1 GzipValidateResetPkt(UINT2 Length, UINT1 Dummy);
#endif


#ifdef __PPP_PPPDES_H__
int endes(char* block, unsigned char (*ifkeyblock)[]);
void dedes(char *block, unsigned char (*ifkeyblock)[]);
int desinit(int mode);
INT1 UNPAD_Packet(UINT1 *Buffer, UINT2 *size);
VOID PAD_Packet(UINT1 *pInBuf, UINT2 *size, UINT1 PadConst);
VOID DES_EDE3_CBC (tTrplDesIfInfo *pTrplDesIfInfo, UINT2 Size);
VOID DES_DED3_CBC (tTrplDesIfInfo *pTrplDesIfInfo, UINT2 Size);
INT1 TripleDESECrypt(tTrplDesIfInfo *pTrplDesIfInfo, UINT2 Size, OPER Operation);
tTrplDESEParams* TripleDESECreateIf(const UINT1 *,const  UINT1 *,const UINT1 *);

#endif

#ifdef __PPP_PPPLCP_H__
VOID MPSplitAndAllocate(tPPPIf *pBundleIf, t_MSG_DESC *pOutData, UINT2 Length, UINT2 Protocol);
VOID MPInput(tPPPIf *pIf, t_MSG_DESC *pInData, UINT2 Len, UINT1 MPHeaderFlag);
tPPPIf *MPCreate(tPPPIfId *pIfID, UINT2 IfIndex);
VOID MPSuspendBundle(tPPPIf *pIf);
VOID MPNullFragTimeOut(VOID *pBundleIf);
VOID MPReassemblyTimeOut(VOID *pBundleIf);
VOID MPPeriodicEchoTimeOut(VOID *pIf);
VOID MPInsertMemberIntoProperBundle(tPPPIf *pMemberIf, tPPPIf *pBundleIf);

#endif

#ifdef  __PPP_PPPGSEM_H__ 




INT4                PppCreateAndEnableIPCPIf (tPPPIf * pIf, UINT4 u4LocalIpAddr,                                              UINT4 u4PeerIpAddr);

#endif

#ifdef  __PPP_PPPIPXCP_H__ 
VOID IPXCPDeleteIf (tIPXCPIf * pIpxcpPtr);
VOID IPXCPEnableIf (tIPXCPIf * pIpxcpPtr, tIPXCPCtrlBlk * pIpxcpCtrlBlk);
VOID IPXCPDisableIf (tIPXCPIf * pIpxcpPtr);
#endif /* __PPP_PPPIPXCP_H__ */

UINT4 PppGenerateMagicNumber(VOID);
t_MSG_DESC *PPPAllocateBuffer(UINT4 Size, UINT4 Offset);
t_MSG_DESC *PPPDuplicateBuffer(t_MSG_DESC *pSrcBuf);
INT4
PPPDuplicateCruBuffer (tCRU_BUF_CHAIN_HEADER * pBuf,
                       tCRU_BUF_CHAIN_HEADER ** pDupBuf, UINT4 u4PktSize);
INT4                PppUpdateAdminStatus (UINT4 u4IfIndex, UINT1 u1AdminStatus);
INT4                PppUpdateLLOperStatus (UINT4 u4IfIndex, UINT1 u1OperStatus);

/* DSL_ADD -S- */
VOID PPPHLIDNSRelayUpdateNS (UINT2, UINT4, UINT4); 
extern VOID DnsRelayUpdateNS (UINT4, UINT4,UINT1 *);    
/* DSL_ADD -E- */

INT1
getPhysicalIndexOfPPP (INT4 pppIndex, INT4 *PhyIndex);

#endif  /* __PPP_GLOBEXTS_H__ */
