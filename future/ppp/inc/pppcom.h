/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppcom.h,v 1.3 2014/10/08 11:00:07 siva Exp $
 *
 * Description:This file contains the common PPP files
 *
 *******************************************************************/
#ifndef __PPP_PPPCOM_H__
#define __PPP_PPPCOM_H__


#include "genhdrs.h"

/* Global PPP includes */
#include "pppdefs.h"
#include "pppdebug.h"
#include "ppptimer.h"

#include "pppdbgex.h"

/* Includes from other modules */
#include "pppgsem.h"
#include "pppgcp.h"
#include "frmtdfs.h"
#include "ppplcp.h"
#include "pppexts.h"
#include "pppmacro.h"
#include "pppcli.h"
#include "utilipvx.h"
#include "radius.h"
#include "authsnmp.h"
#include "pppipcp.h"
#include "fsutil.h"
#include "fssyslog.h"

#endif  /* __PPP_PPPCOM_H__ */
