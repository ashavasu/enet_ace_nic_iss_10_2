/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppdbgex.h,v 1.2 2011/10/13 10:31:25 siva Exp $
 *
 * Description:This file contains definition/declaration of global
 *             variables used for debugging and statistics.
 *
 *******************************************************************/
#ifndef __PPP_PPPDBGEX_H__
#define __PPP_PPPDBGEX_H__


#ifdef GLOBAL_VAR
UINT4    DesiredLevel;
UINT2    StrMsgAllocCounter;
UINT2    OutAllocCounter;
UINT2    OutRelCounter;

UINT2    NoOfPktsRcvdFromHL;
UINT2    NoOfPktsSentToHL;
UINT1    ReleaseFlag;
const char *gaPPPDebEvent[4]={"UP","DOWN","OPEN","CLOSE"};

UINT2    NegFlagsAllocCounter;
UINT2    NegFlagsFreeCounter;
UINT2    StrMsgFreeCounter;

UINT2    NoOfPktsAcked;

UINT4    MpFragAllocCounter;
UINT4    MpFragFreeCounter;
UINT2    InRelCounter;

UINT2    PppAllocCounter;
UINT2    PppFreeCounter;

UINT2    TotalInNUniPkts;

UINT2    InAllocCounter;

UINT2    TotalOutNUniPkts;

UINT4    DesiredModule;

UINT2    NoOfActiveTimers;

#else

extern    UINT4    DesiredLevel;
extern    UINT2    StrMsgAllocCounter;
extern    UINT2    StrMsgFreeCounter;
extern    UINT2    OutAllocCounter;
extern    UINT2    OutRelCounter;

extern    UINT2    NoOfPktsRcvdFromHL;
extern    UINT2    NoOfPktsSentToHL;
extern    UINT1    ReleaseFlag;
extern    UINT1   *gaPPPDebEvent[];

extern    UINT2    NegFlagsAllocCounter;
/* 98100001 */
extern    UINT2    NegFlagsFreeCounter;

extern    UINT2    NoOfPktsAcked;

extern    UINT4    MpFragAllocCounter;
extern    UINT4    MpFragFreeCounter;
extern    UINT2    InRelCounter;

extern    UINT2    PppAllocCounter;
extern    UINT2    PppFreeCounter;
 
extern    UINT2    TotalInNUniPkts;

extern    UINT2    InAllocCounter;

extern    UINT2    TotalOutNUniPkts;

extern    UINT4    DesiredModule;

extern    UINT2    NoOfActiveTimers;

#endif


#endif  /* __PPP_PPPDBGEX_H__ */
