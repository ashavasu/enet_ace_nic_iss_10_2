/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppdefs.h,v 1.3 2014/11/23 09:35:22 siva Exp $
 *
 * Description:This files contains the #defines used by 
               the modules of PPP subsystem.
 *
 *******************************************************************/
#ifndef __PPP_PPPDEFS_H__
#define __PPP_PPPDEFS_H__


/*********************************************
 *   Assigne values  for protocol fields     *
 *********************************************/   

#define  LCP_PROTOCOL                   0xc021 
#define  IPCP_PROTOCOL                  0x8021 
#define  IPV6CP_PROTOCOL                0x8057 
#define  MUXCP_PROTOCOL                 0x8059 
#define  MPLSCP_PROTOCOL                0x8281 
#define  BCP_PROTOCOL                   0x8031 
#define  IPXCP_PROTOCOL                 0x802b 
#define  CCP_OVER_MEMBER_LINK           0x80FB 
#define  CCP_OVER_INDIVIDUAL_LINK       0x80FD 
#define  ECP_OVER_MEMBER_LINK           0x8055 
#define  ECP_OVER_INDIVIDUAL_LINK       0x8053 
#define  BACP_PROTOCOL                  0xc02b 

#define  PAP_PROTOCOL                   0xc023 
#define  CHAP_PROTOCOL                  0xc223 
#define  MSCHAP_PROTOCOL                0x0001     /* <- proprietary  */
#define  EAP_PROTOCOL                   0xc227 
#define  EAPMD5_PROTOCOL                0x1000 
#define  LQM_PROTOCOL                   0xc025 

#define  IP_DATAGRAM                    0x0021 
#define  IP_COMP_TCP_DATA               0x002d 
#define  IP_UNCOMP_TCP_DATA             0x002f 

#define  IP6_DATAGRAM                   0x0057 
#define  IPHC_PROTOCOL                  0x0061

#define  MUXCP_DATAGRAM                 0x0059 

#define  MPLS_UNICAST_DATAGRAM          0x0281 
#define  MPLS_MULTICAST_DATAGRAM        0x0283 

#define  BCP_SPAN_IEEE_DATA             0x0201 
#define  BCP_IBM_SRC_RT_DATA            0x0203 
#define  BCP_DEC_LAN_DATA               0x0205 
#define  BCP_BRIDGED_LAN_DATA           0x0031 

#define  IPX_DATAGRAM                   0x002b 
#define  TELEBIT_COMP_PROTO             0x0002 
#define  SHIVA_COMP_PROTO               0x0235 

#define  MP_DATAGRAM                    0x003d 
#define  BAP_PROTOCOL                   0xc02d 

#define  ECP_DATA_OVER_MEMBER_LINK      0x0055   
#define  ECP_DATA_OVER_INDIVIDUAL_LINK  0x0053   

#define  CCP_DATA_OVER_MEMBER_LINK      0x00fb   
#define  CCP_DATA_OVER_INDIVIDUAL_LINK  0x00fd   


#define  IPHC_TCP_PROT                  0x0063 
#define  IPHC_NON_TCP_PROT              0x0065
#define  IPHC_UDP8_PROT                 0x0067
#define  IPHC_RTP8_PROT                 0x0069
#define  IPHC_TCP_NODELTA               0x2063
#define  IPHC_CONTEXT_STATE             0x2065
#define  IPHC_UDP16_PROT                0x2067
#define  IPHC_RTP16_PROT                0x2069


/*********************************************
 *   Definition of MACRO's used in SNMP      *
 *   Access functions                        *
 *********************************************/   

#define  IP_COMPRESSION_NONE          1    
#define  IP_VJ_TCP_COMPRESSION        2    
#define  IP_HEADER_COMPRESSION        3    

#define   L2TP_LNS             1
#define   L2TP_LAC             2

#define PPP_DEF_ROUTE_METRIC              1
#define PPP_DEF_ROUTE_IP                  0
#define PPP_DEF_ROUTE_MASK                0

#define PPP_DATA_INDEX		7

#define  PPP_TRUE                  1
#define  PPP_FALSE                 2

#define  PPP_YES                   1
#define  PPP_NO                    0

#define  INVALID            1                            
#define  VALID              2                            

#define  ADMIN_OPEN         1                            
#define  ADMIN_CLOSE        2                            

#define  STATUS_UP          1                            
#define  STATUS_DOWN        2                            

#define  NOT_SET            0                            
#define  SET                1                            

#define  PKT_DISCARDED      1                            
#define  PKT_NOT_DISCARDED  2                            

#define  MAX_PKT_SIZE       500                          
#define  INIT_VAL           -1                           

#ifdef L2TP_WANTED
#define  L2TP_HDR_OFFSET    12                           
#define  PPP_INFO_OFFSET    (10 + L2TP_HDR_OFFSET)       
#else
#define  PPP_INFO_OFFSET    10                           
#endif

#define  DATA_OFFSET        PPP_INFO_OFFSET + CP_HDR_LEN 
#define  CODE_OFFSET        0                            
#define  ID_OFFSET          1                            
#define  LEN_OFFSET         2                            

#define  NOT_FOUND          -1                           
#define  DISCARD            -1                           

#define    DESIRED_SET                    0x01    /* 00000001 */
#define    ALLOWED_FOR_PEER_SET           0x02    /* 00000010 */
#define    ACKED_BY_PEER_SET              0x04    /* 00000100 */
#define    ACKED_BY_LOCAL_SET             0x08    /* 00001000 */
#define    FORCED_TO_REQUEST_SET          0x10    /* 00010000 */
#define    ALLOW_FORCED_NAK_SET           0x20    /* 00100000 */
#define    ALLOW_REJECT_SET               0x40    /* 01000000 */

#define    DESIRED_MASK                   0x01    /* 00000001 */
#define    ALLOWED_FOR_PEER_MASK          0x02    /* 00000010 */
#define    ACKED_BY_PEER_MASK             0x04    /* 00000100 */
#define    ACKED_BY_LOCAL_MASK            0x08    /* 00001000 */
#define    FORCED_TO_REQUEST_MASK         0x10    /* 00010000 */
#define    ALLOW_FORCED_NAK_MASK          0x20    /* 00100000 */
#define    ALLOW_REJECT_MASK              0x40    /* 01000000 */

#define    DESIRED_NOT_SET                0xFE    /* 11111110 */
#define    ALLOWED_FOR_PEER_NOT_SET       0xFD    /* 11111101 */
#define    ACKED_BY_PEER_NOT_SET          0xFB    /* 11111011 */
#define    ACKED_BY_LOCAL_NOT_SET         0xF7    /* 11110111 */
#define    FORCED_TO_REQUEST_NOT_SET      0xEF    /* 11101111 */
#define    ALLOW_FORCED_NAK_NOT_SET       0xDF    /* 11011111 */
#define    ALLOW_REJECT_NOT_SET           0xBF    /* 10111111 */

#define  NEGOTIATED_ASYNC_VALS  1 
#define  DEFAULT_ASYNC_VALS     0 

#define  ALL_AUTH_OVER          0 
#define  FCS_16_REQ             2 


#define  BUNDLE                 1
#define  MEMBER_LINK            2

#define  PPP_INIT_COMPLETE(u4Status)  lrInitComplete(u4Status)
#define  PPP_PROT_TYPE(pBuf)          (pBuf->ModuleData.u4Reserved3)

#define  PPP_MAX_IP_POOL        128

#endif  /* __PPP_PPPDEFS_H__ */
