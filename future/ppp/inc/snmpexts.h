/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: snmpexts.h,v 1.2 2011/10/13 10:31:25 siva Exp $
 *
 * Description:This file contains SNMP global variables declarations
 *
 *******************************************************************/
#ifndef __PPP_SNMPEXTS_H__
#define __PPP_SNMPEXTS_H__

#ifdef BAP
extern UINT1 BACPInstance;
extern UINT1 RMInstance;
#endif

#endif  /* __PPP_SNMPEXTS_H__ */
