/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppptask.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the constants and definitions for 
 *             TASK,EVENT, QUEUE related functions of the PPP Subsystem.
 *
 *******************************************************************/
#ifndef __PPP_PPPTASK_H__
#define __PPP_PPPTASK_H__


/* Queues */

#define  PPP_QUEUE_NAME				(CONST UINT1 *)"PPPQ"     
#define  PPP_QUEUE_DEPTH			200                         
#define  PPP_LL_INPUT_Q_NODE_ID     SELF                      

/* Events */
#define  PPP_EVENT_WAIT_TIMEOUT     0                         
#define  PPP_EVENT_WAIT_FLAGS       (OSIX_WAIT | OSIX_EV_ANY) 


#define  PPP_TIMER_EXPIRY_EVENT     0x00000001                
#define  PPP_QUEUE_EVENT			0x00000002                



#define  PPPMSG_FROM_HIGHER_LAYER   1   /* Message from Higher Layer to PPP */  
#define  PPPMSG_FROM_LOWER_LAYER    2	 /* Message from Lower Layer  to PPP */
#define  PPPMSG_FROM_RADIUS_CLIENT  3    /* Message from Radius Client */
#define  PPPMSG_FROM_PPPoE	    4    /* Message From PPPoE */	
#define  PPPEVENT_TO_PPP_GSEM	    5    /* Message to GSEM */	
#define  PPPMSG_UPDATE_INTF_STATUS  6    /* From Cfa2 */



#endif
