/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppphdrs.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the headers files needed by 
 *             all the modules of the PPP subsystem
 *
 *******************************************************************/
#ifndef __PPP_PPPHDRS_H__
#define __PPP_PPPHDRS_H__


#include    <stdio.h>

#include    "pppmacro.h"
#include    "pppdefs.h"
#include    "ppptimer.h"
#include    "pppgsem.h"
#include    "pppdebug.h"
#include    "pppgcp.h"

#include    "pppmp.h"

#include    "ppplcp.h"
#include    "ppplqm.h"
#include    "pppchap.h"
#include    "ppppap.h"
#include    "pppeap.h"
#include    "pppauth.h"

#include    "pppipcp.h"

/* IPv6CP Changes -- start */
#include "pppip6cp.h"
/* IPv6CP Changes -- End */

#include    "pppipxcp.h"
#include    "pppmplscp.h"

#include    "pppbcp.h"


#include "pppeccmn.h"
#include        "pppccpgl.h"
#include        "pppccppt.h"

#include <sys/time.h>
#include        "pppecp.h"
#include        "pppecpts.h"
#include        "pppdesif.h"
#include "pppgzipif.h"


#include <sys/time.h>
#include "pppccp.h"
#include "gzip.h"
#include "pppgzipif.h"

#include	"pppproto.h"
#include	"pppglobs.h"
#include    "ppptest.h"



#endif  /* __PPP_PPPHDRS_H__ */
