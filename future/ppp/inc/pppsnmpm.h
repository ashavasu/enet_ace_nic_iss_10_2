/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppsnmpm.h,v 1.2 2011/10/13 10:31:25 siva Exp $
 *
 * Description:This file contains definitions for the low-level 
 * functions of the PPP module
 *
 *******************************************************************/
#ifndef _PPPSNMPM_H
#define _PPPSNMPM_H
#undef OK
# include "snmctdfs.h"
# include "snmccons.h"

#define  ENABLE              2                     
#define  DISABLE             1                     
#define  AVAILABLE           2                     
#define  NOT_AVAILABLE       1                     

#define  MIN_INTEGER         0                     
#define  MAX_INTEGER         2147483647            

#define  FIRST_INST          1                     
#define  NEXT_INST           2                     

#define  PPP_SNMP_ERR        1                     
#define  PPP_SNMP_OK         0                     
#define  SNMP_GET_SUCCESS    0                     

#define  NO_ERROR            SNMP_ERR_NO_ERROR     
#define  TOO_BIG_ERROR       SNMP_ERR_TOO_BIG      
#define  NO_SUCH_NAME_ERROR  SNMP_ERR_NO_SUCH_NAME 
#define  BAD_VALUE_ERROR     SNMP_ERR_BAD_VALUE    
#define  READ_ONLY_ERROR     SNMP_ERR_READ_ONLY    
#define  GEN_ERROR           SNMP_ERR_GEN_ERR      
/* extern def's for Instances */
 extern  UINT1      IpInstance; 
 extern  UINT1      Ip6Instance; 
 extern  UINT1      MuxcpInstance; 
 extern  UINT1      MplsInstance;
 extern  UINT1      IpxInstance; 
 extern  UINT1      MpInstance;
 extern  UINT1      CCPInstance; 
 extern  UINT1      CCPTxInstance;
 extern  UINT1      CCPRxInstance;
 extern  UINT1      ECPInstance; 
 extern  UINT1      ECPTxInstance; 
 extern  UINT1      ECPRxInstance;
 extern  UINT1      BcpInstance; 
 extern  UINT1      BcpMediaInstance;
 extern  UINT1      BACPInstance; 
 extern  UINT1      RMInstance; 
 extern  UINT1      SecurityInstance; 
 extern  UINT1      SecretsInstance; 
 extern  UINT1      LinkInstance;

INT1 PPP_compare_oids (tSNMP_OID_TYPE *oid1,tSNMP_OID_TYPE *oid2);
INT1 PPPCopyOid ( tSNMP_OID_TYPE * pDestOid,tSNMP_OID_TYPE *pSrcOid); 
#endif
