/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppmacro.h,v 1.5 2014/09/01 11:02:10 siva Exp $
 *
 * Description:This files contains the macros used by the 
 *             modules of PPP subsystem.
 *
 *******************************************************************/
#ifndef __PPP_PPPMACRO_H__
#define __PPP_PPPMACRO_H__

#ifdef UNIX
#include <time.h>
#endif

#define   bzero(k,n)            memset((void *)k,0,n)
#define   BZERO                 bzero

#undef offsetof
#define offsetof(TYPE, MEMBER) ((unsigned int) &((TYPE *)0)->MEMBER)

#define    FIND_OFFSET(type,field)      offsetof(type,field)

#define   BCOPY(src, dest,len)  MEMCPY(dest, src, len)
/**********************************************************************/

/*  Memory  allocation Macros */

#define   PPP_LOCK()            PppLock ()
#define   PPP_UNLOCK()          PppUnlock ()
   
#define  PPP_MALLOC( size)    MEM_MALLOC(size,void)
#define  CALLOC(n, size)      MEM_CALLOC(n,size,void)
#define  FREE(ptr)            MEM_FREE(ptr)
      
    
#define   ALLOC_PPP_IF()      \
                 (((pAllocPtr = (UINT1 *)CALLOC(1,sizeof(tPPPIf)))==NULL)  \
                        ?  (NULL)  \
                        :  (PppAllocCounter++,pAllocPtr))

#define   ALLOC_IPCP_IF()    \
                 (((pAllocPtr = (UINT1 *)CALLOC(1,sizeof(tIPCPIf)))==NULL)  \
                        ?  (NULL)  \
                        :  (IpcpAllocCounter++,pAllocPtr))
#define   ALLOC_MPLSCP_IF()    \
                 (((pAllocPtr = (UINT1 *)CALLOC(1,sizeof(tMPLSCPIf)))==NULL)  \
                        ?  (NULL)  \
                        :  (MplscpAllocCounter++,pAllocPtr))
        
#define   ALLOC_VJCOMP_IF()    \
                 (((pAllocPtr = (UINT1 *)CALLOC(1,sizeof(tPPP_COMPRESS)))==NULL)  \
                                                          ?  (NULL)  \
                                                                                                          :  (VjAllocCounter++,pAllocPtr))


#define   ALLOC_IPXCP_IF()   \
                (((pAllocPtr = (UINT1 *)CALLOC(1,sizeof(tIPXCPIf)))==NULL)  \
                        ?  (NULL)  \
                        :  (IpxcpAllocCounter++,pAllocPtr))
#define   ALLOC_LQM_IF()    \
                (((pAllocPtr = (UINT1 *)CALLOC(1,sizeof(tLQMIf)))==NULL)  \
                        ?  (NULL)  \
                        :  (LqmAllocCounter++,pAllocPtr))

#define   ALLOC_AUTH_IF()    \
                 (((pAllocPtr = (UINT1 *)CALLOC(1,sizeof(tAUTHIf)))==NULL)  \
                        ?  (NULL)  \
                        :  (AuthAllocCounter++,pAllocPtr))

#define   ALLOC_STR(Len) \
                 (((pAllocPtr = (UINT1 *)CALLOC(1,Len))==NULL)  \
                        ?  (NULL)  \
                        :  (StrMsgAllocCounter++,pAllocPtr))

#define   ALLOC_NEG_FLAGS(Len) \
                 (((pAllocPtr = (UINT1 *)CALLOC(1, Len * sizeof(struct NegFlagsPerIf)))==NULL)  \
                        ?  (NULL)  \
                        :  (NegFlagsAllocCounter++,pAllocPtr))


#define   ALLOC_MP_FRAGMENT()      \
     (((pAllocPtr = (UINT1 *)CALLOC(1,sizeof(tMPFrag)))==NULL)  \
      ?  (NULL)  \
      :  (MpFragAllocCounter++,pAllocPtr))

/**********************************************************************/

/*  Memory Free Macros.... */

#define   FREE_PPP_IF(ptr) {              \
                                FREE(ptr);                \
                                PppFreeCounter++;         \
}

#define   FREE_LQM_IF(ptr) {            \
                                FREE(ptr);                      \
                                LqmFreeCounter++;   \
}

#define   FREE_IPCP_IF(ptr)     {        \
                                FREE(ptr);       \
                                IpcpFreeCounter++; \
}

/* IPv6CP Changes -- start */
#define   FREE_IP6CP_IF(ptr)     {        \
                                FREE(ptr);       \
                                gIP6Counters.u2FreeCounter++; \
}
/* IPv6CP Changes -- End */

#define   FREE_MPLSCP_IF(ptr)   {        \
                                FREE(ptr);       \
                                MplscpFreeCounter++; \
}

#define   FREE_IPXCP_IF(ptr)  { \
                                FREE(ptr);              \
                                IpxcpFreeCounter++; \
}

#define   FREE_BCP_IF(ptr)  {   \
                                FREE(ptr);              \
                                BcpFreeCounter++; \
}

#define   FREE_AUTH_IF(ptr) {        \
    MEM_FREE(ptr);          \
        AuthFreeCounter++;   \
}

#define   FREE_SECRET_ENTRY(ptr) {        \
        MEM_FREE(ptr);          \
}

#define   FREE_STR(ptr) {    \
        MEM_FREE(ptr);       \
        StrMsgFreeCounter++; \
}

#define   FREE_NEG_FLAGS(ptr)  {        \
                                FREE(ptr);              \
                                NegFlagsFreeCounter++; \
}

#define  FREE_PPP_IF_DB_PTRS(ptr)  {       \
            FREE(ptr);                      \
        PppIfFreeCounter++;                     \
}

#define  FREE_IPCP_IF_DB_PTRS(ptr)  {       \
    FREE(ptr);                      \
        IpcpIfFreeCounter++;                    \
}

/* IPv6CP Changes -- start */
#define  FREE_IP6CP_IF_DB_PTRS(ptr)  {       \
    FREE(ptr);                      \
        gIP6Counters.u2IfFreeCounter++;                    \
}
/* IPv6CP Changes -- End */

#define   FREE_IPXCP_IF_DB_PTRS(ptr)     {  \
                        FREE(ptr);                                              \
                          IpxcpIfFreeCounter++;                         \
}

#define   FREE_BCP_IF_DB_PTRS(ptr)     {  \
     FREE(ptr);                                         \
                          BcpIfFreeCounter++;                   \
}

#define   FREE_LQM_IF_DB_PTRS(ptr)     {    \
    FREE(ptr);                      \
        LqmIfFreeCounter++;           \
}
#define   FREE_AUTH_IF_DB_PTRS(ptr)     {   \
        FREE(ptr);                      \
        AuthIfFreeCounter++;            \
}

#define   FREE_MP_FRAGMENT(ptr) {      \
           FREE(ptr);                           \
           MpFragFreeCounter++;         \
}

/**********************************************************************/
/*  Macros  that are common to various modules  */

#define   FILL_PKT_HEADER(pOutBuf,Code,Identifier,Length)   \
                  UTILFillPktHeader(pOutBuf,(UINT1)Code,Identifier,Length)

#define   DISCARD_PKT(pIf,Msg)   UTILDiscardPkt(pIf,(UINT1 const *)Msg)

   /* DSL_REMOVE */
#define     INIT_RANDOM_GENERATOR()     {\
                                        time_t temp;\
                                        time(&temp);\
                                        srand((UINT4)temp);\
                                        }
#define    GET_RANDOM_NUMBER()          rand()


/* DSL_REMOVE */
#define     GET_RANDOM_CHALLENGE_LENGTH()    MIN_CHALLENGE_LENGTH 
#define     GET_RANDOM_CHALLENGE_VALUE()    (rand()) 

#define     GENERATE_MAGIC_NUMBER()        PppGenerateMagicNumber()



#define     EAP_GET_RANDOM_IDENTITY_STRING_LENGTH()   EAP_MAX_IDENTITY_STRING_LENGTH
#define     EAP_GET_RANDOM_IDENTITY_STRING_VALUE()    0xff


#define     UPDATE_OUT_COUNTER(pIf)                  pIf->LLHCounters.OutDiscards++
#define     UPDATE_IN_COUNTER(pIf)                   pIf->LLHCounters.InDiscards++

/**********************************************************************/
   



#define ALLOC_GZIP_PARAMS() CALLOC(1, sizeof(tGZIPParams)) /* SRINI */
#define FREE_GZIP_PARAMS(pStacParams) FREE(pGZIPParams) /* SRINI */
#define ALLOC_MPPE_PARAMS() CALLOC(1, sizeof(tMPPEParams))
#define FREE_MPPE_PARAMS(pStacParams) FREE(pMPPEParams)


#define         ALLOC_CCP_IF()    \
        (((pAllocPtr = (UINT1 *)CALLOC(1,sizeof(tCCPIf)))==NULL)  \
                ?  (NULL)  \
                        :  (CCPAllocCounter++,pAllocPtr))

#define         FREE_CCP_PTR(ptr)     {   \
                FREE(ptr);                      \
                CCPIfFreeCounter++;            \
}


#define   ALLOC_CCP_PARAMS(Length)    \
         (((pAllocPtr = CALLOC(1, Length))==NULL)  \
            ?  (NULL)  \
            :  (CCPAllocCounter++,pAllocPtr))

#define   FREE_CCP_OR_ECP_PARAMS(ptr)   {\
                FREE(ptr);                                              \
                CCPIfFreeCounter++;                             \
}

#define ALLOC_STAC_PARAMS() CALLOC(1, sizeof(tStacParams)) /* SRINI */
#define FREE_STAC_PARAMS(pStacParams) FREE(pStacParams) /* SRINI */

#define ALLOC_CCP_HEADER() CALLOC(1, sizeof(tHeader)) /* SRINI */


#define         ALLOC_ECP_IF()    \
        (((pAllocPtr = (UINT1 *)CALLOC(1,sizeof(tECPIf)))==NULL)  \
                ?  (NULL)  \
                        :  (ECPAllocCounter++,pAllocPtr))

#define         FREE_ECP_PTR(ptr)     {   \
                FREE(ptr);                      \
                ECPIfFreeCounter++;            \
}


#define   ALLOC_ECP_PARAMS(Length)    \
         (((pAllocPtr = CALLOC(1, Length))==NULL)  \
            ?  (NULL)  \
            :  (ECPAllocCounter++,pAllocPtr))

#define ALLOC_DESE_PARAMS() CALLOC(1, sizeof(tDESEParams)) /* SRINI */

#define FREE_DESE_PARAMS(pDESEParams) FREE(pDESEParams) /* SRINI */

#define ALLOC_ECP_HEADER() CALLOC(1, sizeof(tHeader)) /* SRINI */

#define ALLOC_TRIPLE_DESE_PARAMS() CALLOC(1, sizeof(tTrplDESEParams)) /*ARU*/
   
#define FREE_TRIPLE_DESE_PARAMS(pTrplDESEParams) \
                            MEM_FREE(pTrplDESEParams) /*ARU*/
    

#define    PERIOD_ZERO( Period )         Period <  100
#define    PERIOD_NON_ZERO( Period )     Period >= 100


#define     ECHO_TIMER_VALUE(TransitDelay)\
                   ( ( (random()%60) * TransitDelay )  + 60 * TransitDelay)

#define FUNCTION_ENTRY  ("\nInside function : %s", __FUNCTION__)
#define FUNCTION_EXIT   ("\nExiting function : %s", __FUNCTION__)
   
#define ALLOC_DEPENDENCYLIST() \
   (tDEPENDENCY_LIST_TYPE *)MEM_MALLOC(sizeof(tDEPENDENCY_LIST_TYPE),void)

#define ALLOC_DEPENDENCYLISTHEADER() \
       (tDEPENDENCY_LIST_HEADER_TYPE *) \
             MEM_MALLOC(sizeof(tDEPENDENCY_LIST_HEADER_TYPE),void)
   
#define FREE_DEPENDENCYLIST(DLIST) MEM_FREE(DLIST)
#define FREE_DEPENDENCYLISTHEADER(DHEADER) MEM_FREE(DHEADER)

/* Added by KK for MP.This macro is used to calculate the LCM of 2 numbers.This is based on the formula LCM(of 2 no's) = product of 2 no's / GCD(2 no's) */
                
#define LCM_CALCULATE(CurrLcm,Speed) ((CurrLcm * Speed) / (GcdCalculate(CurrLcm,Speed)))

/* PPP_UNUSED macro is used for removing the warning for unused parameters.*/
#define PPP_UNUSED(x)    ((void)x)
/*define COUNTER_64BIT if 64 bit counters wanted for PPP*/
#ifdef COUNTER_64BIT
#define PPP_EXTRACT_64BIT_MSN    0xffffffff00000000
#define PPP_EXTRACT_64BIT_LSN    0x00000000ffffffff
#define BYTE8_TO_TWO_BYTE4_CONV(byte4num, byte8num) \
         {                      \
             byte4num->msn = ((byte8num) & PPP_EXTRACT_64BIT_MSN) >> 32; \
             byte4num->lsn = (byte8num) & PPP_EXTRACT_64BIT_LSN; \
         }
#else
#define BYTE8_TO_TWO_BYTE4_CONV(byte4num, byte8num) \
         {                      \
             byte4num->msn = byte8num; \
             byte4num->lsn = byte8num; \
         }
#endif /*COUNTER_64BIT*/
#define   PPP_MAX_FRAME_HEADER_SIZE        22         /* currently only takes
                                                       * Enet into
                                                       * account
                                                       */
#define   PPP_MAX_DRIVER_MTU               9188       /* For AAL5 PDU along with
                                                       * LLC-SNAP header
                                                       */
        
#endif  /* __PPP_PPPMACRO_H__ */
