/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppipcp.h,v 1.2 2014/03/11 14:02:49 siva Exp $
 *
 * Description:This file contains the typedefs, #defines and 
 *             data structures used by the IPCP Module.
 *
 *******************************************************************/
#ifndef __PPP_PPPIPCP_H__
#define __PPP_PPPIPCP_H__

/*********************************************************************/
/*                              CONSTANTS                           */
/*********************************************************************/

/* CONFIGURATION OPTION VALUES  for IPCP packets */

#define  NO_NEG_COMPRESSION           0xFE 
#define  NO_NEG_IP_ADDRESS            0xFD 
#define  IP_COMP_OPTION               2    
#define  IP_ADDR_OPTION               3    
#define  PRIMARY_DNS_ADDR_OPTION      129  
#define  PRIMARY_NBNS_ADDR_OPTION     130  
#define  SECONDARY_DNS_ADDR_OPTION    131  
#define  SECONDARY_NBNS_ADDR_OPTION   132  
#define  NEG_COMPRESSION              1    
#define  NEG_IP_ADDRESS               2    
#define  IP_ADDR_LEN                  6    
#define  VJ_COMP_LEN                  6    
#define  MAX_IPCP_OPT_TYPES           6    
#define  MAX_IPCP_COMP_DISCR_VALUES   2    

#define  DEFAULT_COMP_SLOT_ID         0    
#define  DEFAULT_MAX_SLOT_ID          15   
#define  MIN_MAX_SLOT_ID              2    
#define  MAX_MAX_SLOT_ID              253  
#define  COMP_SLOT_ID_NOT_COMPRESSED  0    
#define  COMP_SLOT_ID_COMPRESSED      1    


#define  IP_COMP_IDX                  0
#define  IP_ADDR_IDX                  1    
#define  PRIMARY_DNS_ADDR_IDX         2    
#define  PRIMARY_NBNS_ADDR_IDX        3    
#define  SECONDARY_DNS_ADDR_IDX       4    
#define  SECONDARY_NBNS_ADDR_IDX      5    

#define  VJ_COMP_PROTOCOL             0x002d

#define PPP_ENABLE                    2
#define PPP_DISABLE                   1


#define  IPCP_UP                      1    
#define  IPCP_DOWN                    2    
#define  IPCP_DELETE                  3    

#define  IPCP_LOCAL_POOL              1    
#define  IPCP_ADDRESS_MODE_RADIUS     2    
#define  IPCP_ADDRESS_MODE_DHCP       3    
#define  IPCP_ADDRESS_MODE_NONE       4    

#define  IPCP_ALLOC_NODE()      \
         MEM_CALLOC(1,sizeof(tIPCPAllocAddrNode),void)

#define  IPCP_ALLOC_POOL()      \
         MEM_CALLOC(1,sizeof(tIPCPAddrPool),void)

#define  IPCP_FREE_POOL( pPool)      \
          MEM_FREE( pPool )

#define  IPCP_FREE_NODE( pNode )      \
         MEM_FREE( pNode )

typedef struct {
   t_SLL_NODE *pNextAddrPoolList;
   UINT4 u4PoolIndex;
   UINT4 u4LowerAddr;
   UINT4 u4UpperAddr;
   UINT4 u4NoOfFreeAddr;
   t_SLL AllocAddrList;
   UINT1 u1PoolStatus;
   UINT1 au1Rsvd[3];
}tIPCPAddrPool;


typedef struct {
   t_SLL_NODE *pNextAllocAddrNode;
   UINT4  u4Addr;
   UINT4  u4Port;
}tIPCPAllocAddrNode;



/********************************************************************/
/*          TYPEDEFS    USED BY THE IPCP MODULE                     */
/*********************************************************************/

typedef   struct {
 UINT4  DiscHeader;
 UINT2  Value[MAX_IPCP_COMP_DISCR_VALUES];
}tIPCPCompDiscVal;

typedef struct {
   UINT2        u2CompProto;
   UINT2        u2Rsvd;
   UINT4        IPAddress;
   UINT4        PrimaryDNSAddress;
   UINT4        PrimaryNBNSAddress;
   UINT4        SecondaryDNSAddress;
   UINT4        SecondaryNBNSAddress;
   union {
          tVJInfo        VJInfo;
          tIPHCInfo      IPHCInfo;
   }CompInfo;
}tIPCPOptions;
      
typedef   struct   {
 VOID            *HLHandle;            /* this is used for storing the higher 
                                          layer handle. so that if ppp has any                                             data to send to HL, this handle can                                             be used. */
 UINT4   LocalIPAddress;
 UINT4   RemoteIPAddress;
 UINT4   LocalPrimaryDNSAddress;
 UINT4   RemotePrimaryDNSAddress;
 UINT4   LocalPrimaryNBNSAddress;
 UINT4   RemotePrimaryNBNSAddress;
 UINT4   LocalSecondaryDNSAddress;
 UINT4   RemoteSecondaryDNSAddress;
 UINT4   LocalSecondaryNBNSAddress;
 UINT4   RemoteSecondaryNBNSAddress;
 UINT1   VJCompFlag;
 UINT1   LocIPAddrNegFlag;
 UINT1   RemoteIPAddrNegFlag;
 UINT1   LocPrimaryDNSAddrNegFlag;
 UINT1   LocPrimaryNBNSAddrNegFlag;
 UINT1   LocSecondaryDNSAddrNegFlag;
 UINT1   LocSecondaryNBNSAddrNegFlag;
 UINT1  u1Rsvd;
}   tIPCPCtrlBlk;

typedef   struct   ipcpif   {
 
 VOID           *HLHandle;            /* this is used for storing the higher 
                                         layer handle. so that if ppp has any                                             data to send to HL, this handle can                                             be used. */
 
 UINT1          OperStatus;
 UINT1          AdminStatus;
 UINT2  u2Rsvd;
 tIPCPOptions   IpcpOptionsDesired;
 tIPCPOptions   IpcpOptionsAllowedForPeer;
 tIPCPOptions   IpcpOptionsAckedByPeer;
 tIPCPOptions   IpcpOptionsAckedByLocal;
 tGSEM          IpcpGSEM;
}   tIPCPIf;


#endif  /* __PPP_PPPIPCP_H__ */
