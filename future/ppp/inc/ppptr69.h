/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppptr69.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains definition/declaration of global
 *             variables used for debugging and statistics.
 *
 ********************************************************************/

/* ValMasks for InternetGatewayDevice.WANDevice.0.WANConnectionDevice.0.WAN-PPPConnection.0 */
#define IFINDEX_MASK                               (1)
#define PPP_SECURITY_SECRETS_INDEX_MASK            (1 << 2)

#define PPP_CONNECTION_VAL_MASK ( IFINDEX_MASK || PPP_SECURITY_SECRETS_INDEX_MASK)

/*Prototype for PPP MOdule*/
tPPPConnection     *
TrAdd_PPPConnection (tWANDevice * pDev, tWANConnDev * pWANConnDev,
                     UINT4 u4IfIndex);
INT4
TrSetIfMainAdminStatus (UINT4 , UINT4);

int
TrSetInternetGatewayDevice_WANDevice_WANConnectionDevice_WANPPPConnection (char *,
       int, int, int, ParameterValue *);

INT4 
TrSetNonMand_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANPPPConnection
    (char *, ParameterValue * , tPPPConnection * );

INT4 
TrSetMand_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANPPPConnection
    (char *, ParameterValue * , tPPPConnection * );

INT4 
TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANPPPConnection
    (char *, tPPPConnection * , ParameterValue * ,BOOL1 );


int
TrGetInternetGatewayDevice_WANDevice_WANConnectionDevice_WANPPPConnection (char * ,
        int ,int ,int, ParameterValue *);


extern INT1 nmhGetIfMainWanType PROTO((INT4, INT4 *));

extern INT1 nmhGetIfMainAdminStatus PROTO((INT4 , INT4 *));

extern INT1
nmhSetNatIfEntryStatus PROTO((INT4  ,INT4 ));

extern INT1
nmhTestv2NatIfEntryStatus PROTO((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhGetNatIfNat PROTO((INT4 ,INT4 *));

extern INT1
nmhSetNatIfNapt PROTO((INT4  ,INT4 ));

extern INT1
nmhSetNatIfTwoWayNat PROTO((INT4  ,INT4 ));

extern INT1
nmhSetNatIfTwoWayNat PROTO((INT4  ,INT4 ));

extern INT1
nmhTestv2LnxappDnsStatus PROTO((UINT4 *  ,INT4 ));

extern INT1
nmhSetLnxappDnsStatus PROTO((INT4 ));

extern INT1
nmhGetLnxappDnsStatus ARG_LIST((INT4 *));

extern INT1
nmhGetNatIfEntryStatus ARG_LIST((INT4 ,INT4 *));

extern INT1 
nmhGetIfMainRowStatus (INT4 ,INT4 *);

extern INT1 
nmhValidateIndexInstanceIfMainTable (INT4);

extern UINT4 IfLastChange [];
extern UINT1 IfTableINDEX[];
extern tSNMP_OID_TYPE pppSecurityChapMD5ProtocolOID;
extern tSNMP_OID_TYPE pppSecurityPapProtocolOID;

/* Wan PPP Connection */
extern UINT4 SysUpTime [];
extern UINT4 IfMainOperStatus [];
extern UINT4 PppExtLinkConfigLowerType [];
extern UINT4 NatIfNat [];
extern UINT4 PppSecuritySecretsSecret [];
extern UINT4 PppSecuritySecretsProtocol [];
extern UINT4 PppSecuritySecretsIdentity [];
extern UINT4 IfMainMtu [];
extern UINT4 PPPoEACName [];
extern UINT4 IfIvrBridgedIface [];
extern UINT4 PPPoEConfigServiceName [];
extern UINT4 IfIpAddr [];
extern UINT4 IfMainAdminStatus [];
extern UINT4 IfAlias [];
extern UINT4 IfWanPersistence [];
extern UINT4 IfMainMtu [];
extern UINT1 IfMainTableINDEX [];
extern UINT1 IfWanTableINDEX [];
extern UINT1 NatIfTableINDEX [];
extern UINT1 IfXTableINDEX [];
extern UINT1 PppSecuritySecretsTableINDEX [];
extern UINT1 PPPoEConfigServiceNameTableINDEX [];
extern UINT1 IfIpTableINDEX [];
extern UINT1 IfIvrTableINDEX [];
extern UINT4 NatIfEntryStatus [];
extern UINT4 IfMainRowStatus [];

extern UINT4 PppExtLinkConfigLowerIfType [];
extern UINT1 PppExtLinkConfigTableINDEX [];


