/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppstubs.h,v 1.2 2010/12/21 08:36:46 siva Exp $
 *
 * Description:This file contains definition/declaration of global
 *             variables used in stub files.
 *
 ********************************************************************/

#include    "lr.h"
#include    "authcom.h"
#include    "pppcom.h"
#include    "genexts.h"
#include    "gsemdefs.h"
#include    "pppsnmpm.h"

#define tCCPIf void
#define tIP6CPIf void
#define tBCPIf void
#define tBCPCtrlBlk void

#define  DISCARD_OPT     -1

#define tMuxCPIf       void
#define tMPLSCPIf      void
#define tIPXCPIf       void
#define tECPIf         void
#define tBACPIf        void
#define IPXCPCtrlBlk   void
#define tIPXCPCtrlBlk  void
#undef __PPP_PPPIPXCP_H__

#undef  OK
#define OK                     0
#define   PPP_SUCCESS          1
#define   PPP_FAILURE          0

INT1 CheckAndGetECPRxPtr PROTO((tPPPIf * pIf, UINT4 *n1, UINT4 *n2, UINT4 SecondIndex));
INT1 CheckAndGetECPTxPtr PROTO((tPPPIf * pIf, UINT4 *n1, UINT4 *n2, UINT4 SecondIndex));
INT1 CheckAndGetCCPRxPtr PROTO((tPPPIf * pIf, UINT4 *n1, UINT4 *n2, UINT4 SecondIndex));
INT1 CheckAndGetCCPTxPtr PROTO((tPPPIf * pIf, UINT4 *n1, UINT4 *n2, UINT4 SecondIndex));
INT1 CheckAndGetRMInfo PROTO((tPPPIf * pIf));
INT1 CheckAndGetBACPPtr PROTO((tPPPIf * pIf));
INT1 CheckAndGetECPPtr PROTO((tPPPIf * pIf));
INT1 CheckAndGetCCPPtr PROTO((tPPPIf * pIf));
INT1 CheckAndGetMpPtr PROTO((tPPPIf * pIf));
INT1 CheckAndGetIpxPtr PROTO((tPPPIf * pIf));
INT1 CheckAndGetMplsPtr PROTO((tPPPIf * pIf));
INT1 CheckAndGetMuxCPPtr PROTO((tPPPIf * pIf));
INT4 CvHandleIncomingPkt PROTO((tCRU_BUF_CHAIN_HEADER * pBuf,
                     UINT2 u2IfIndex, UINT4 u4PktSize, UINT2 u2Protocol,
                                          UINT1 u1EncapType));
VOID BAPInput PROTO((tPPPIf * pBundleIf, t_MSG_DESC * pInPkt, UINT2 Length));
INT4 LCPUpToDP PROTO((tPPPIf * pIf));
INT4 PppCPDownToDP PROTO((tPPPIf * pIf, UINT2 u2Proto));
INT4 IPCPUpToDP PROTO((tPPPIf * pIf));
INT4 MuxCPUpToDP PROTO((tPPPIf * pIf));
INT4 MPLSCPUpToDP PROTO((tPPPIf * pIf));
VOID RMInit PROTO((VOID));
INT4 PPPDeleteIfToDP PROTO((tPPPIf * pIf));
VOID MPSplitAndAllocate PROTO((tPPPIf * pBundleIf, t_MSG_DESC * pOutData, 
                               UINT2 Length,UINT2 Protocol));
VOID MPInput PROTO((tPPPIf * pIf, t_MSG_DESC * pInData, UINT2 Len, UINT1 MPHeaderFlag));
INT4 PppTxCtrlPktToDP PROTO((UINT4 u4IfIndex, tCRU_BUF_CHAIN_DESC * pBuf, UINT4 u4Length));
INT4 PPPLLICallTerminateToDP PROTO((tPPPIf * pIf));
INT4 PPPLLICallSetUpToDP PROTO((tPPPIf * pIf));
tPPPIf * MPCreate PROTO((tPPPIfId * pIfID, UINT2 IfIndex));
tPPPIf * SNMPGetBundleIfptr PROTO((UINT4 Index));
VOID MPAddLinkToBundle PROTO((tPPPIf * pBundleIf, tPPPIf * pMemberIf));
INT4 PppCreatePppInterfaceToDP PROTO((tPPPIf * pIf));
VOID MuxCPDeleteIf PROTO((tMuxCPIf * pMuxcpPtr));
VOID MPLSCPDeleteIf PROTO((tMPLSCPIf * pMplscpPtr));
VOID IPXCPDeleteIf PROTO((tIPXCPIf * pIpxcpPtr));
VOID ECPDeleteIf PROTO((tECPIf * pECP));
VOID MPDelete PROTO((tPPPIf * pBundleIf));
VOID MPDeleteLinkFromBundle PROTO((tPPPIf * pBundleIf, tPPPIf * pMemberIf));
VOID MPInsertMemberIntoProperBundle PROTO((tPPPIf * pMemberIf, tPPPIf * pBundleIf));
VOID MPDeleteMemberFromBundle PROTO((tPPPIf * pBundleIf, tPPPIf * pMemberIf));
INT4 PppLinkConfigFramingToDP PROTO((tPPPIf * pIf));
tECPIf * SNMPGetECPIfPtr PROTO((UINT4 Index));
tECPIf * SNMPGetCCPIfPtr PROTO((UINT4 Index));
VOID MPNullFragTimeOut PROTO((VOID *pIf));
VOID MPReassemblyTimeOut PROTO((VOID *pIf));
VOID MPPeriodicEchoTimeOut PROTO((VOID *pPppIf));
VOID BAPTimeOut PROTO((VOID *pPenRequest));
INT1 ECPRecdResetReq PROTO((tGSEM * pGSEM, t_MSG_DESC * pIn, UINT2 Length, UINT1 Id));
INT1 CCPRecdResetReq PROTO((tGSEM * pGSEM, t_MSG_DESC * pIn, UINT2 Length, UINT1 Id));
tGSEM * MuxCPGetSEMPtr PROTO((tPPPIf * pIf));
INT1 MuxCPCopyOptions PROTO((tGSEM * pGSEM, UINT1 Flag, UINT1 PktType));
tGSEM * IPXCPGetSEMPtr PROTO((tPPPIf * pIf));
INT1 IPXCPCopyOptions PROTO((tGSEM * pGSEM, UINT1 Flag, UINT1 PktType));
tGSEM * BACPGetSEMPtr PROTO((tPPPIf * pIf));
INT1 BACPCopyOptions PROTO((tGSEM * pGSEM, UINT1 Flag, UINT1 PktType));
tGSEM * ECPGetSEMPtr PROTO((tPPPIf * pIf));
INT1 ECPCopyOptions PROTO((tGSEM * pGSEM, UINT1 Flag, UINT1 PktType));
tGSEM * CCPGetSEMPtr PROTO((tPPPIf * pIf));
INT1 CCPCopyOptions PROTO((tGSEM * pGSEM, UINT1 Flag, UINT1 PktType));
tGSEM * MPLSCPGetSEMPtr PROTO((tPPPIf * pIf));
INT1 MPLSCPCopyOptions PROTO((tGSEM * pGSEM, UINT1 Flag, UINT1 PktType));
VOID IPXCPEnableIf PROTO((tIPXCPIf * pIpxcpPtr, tIPXCPCtrlBlk * pIpxcpCtrlBlk));
VOID MuxCPEnableIf PROTO((tMuxCPIf * pMuxcpPtr));
VOID MPLSCPEnableIf PROTO((tMPLSCPIf * pMplscpPtr));
VOID ECPEnableIf PROTO((tECPIf * pECP));
VOID BACPEnableIf PROTO((tBACPIf * pBACPIf));
VOID IPXCPDisableIf PROTO((tIPXCPIf * pIpxcpPtr));
VOID MuxCPDisableIf PROTO((tMuxCPIf * pMuxcpPtr));
VOID MPLSCPDisableIf PROTO((tMPLSCPIf * pMplscpPtr));
VOID ECPDisableIf PROTO((tECPIf * pECP));
INT1 CCPRecdResetAck PROTO((tGSEM * pGSEM, t_MSG_DESC * pIn, UINT2 Length, UINT1 Id));
INT1 ECPRecdResetAck PROTO((tGSEM * pGSEM, t_MSG_DESC * pIn, UINT2 Length, UINT1 Id));
INT4 IPv6CPUpToDP PROTO((tPPPIf * pIf));
VOID CCPEnableIf(tCCPIf *pCCP);
VOID IP6CPEnableIf(tIP6CPIf *pIp6cpPtr);
VOID
IP6CPDisableIf (tIP6CPIf * pIp6cpPtr);
VOID
CCPDisableIf (tCCPIf * pCCP);
VOID
BCPDisableIf (tBCPIf * pBcpPtr);
INT1
nmhGetPppCCPTxConfigOptionNum (INT4 i4PppCCPTxConfigIfIndex,
                               INT4 i4PppCCPTxConfigPriority,
                               INT4 *pi4RetValPppCCPTxConfigOptionNum);
tBCPIf             *
BCPCreateIf (tPPPIf * pIf);
VOID * BCPEnable (tBCPIf * pBcpPtr);
VOID
BCPDeleteIf (tBCPIf * pBcpPtr);
VOID
CCPDeleteIf (tCCPIf * pCCPIf);
VOID
IP6CPDeleteIf (tIP6CPIf * pIp6cpPtr);
INT1
nmhGetFirstIndexPppBridgeTable (INT4 *pi4PppBridgeIfIndex);
INT1
nmhGetNextIndexPppBridgeTable (INT4 i4PppBridgeIfIndex,
                               INT4 *pi4NextPppBridgeIfIndex);
INT1
nmhValidateIndexInstancePppBridgeTable (INT4 i4PppBridgeIfIndex);
INT1
nmhGetPppBridgeOperStatus (INT4 i4PppBridgeIfIndex,
                           INT4 *pi4RetValPppBridgeOperStatus);
INT1
nmhGetPppBridgeLocalToRemoteTinygramCompression (INT4 i4PppBridgeIfIndex,
                                                 INT4
                                                 *pi4RetValPppBridgeLocalToRemoteTinygramCompression);
INT1
nmhGetPppBridgeRemoteToLocalTinygramCompression (INT4 i4PppBridgeIfIndex,
                                                 INT4
                                                 *pi4RetValPppBridgeRemoteToLocalTinygramCompression);
INT1
nmhGetPppBridgeLocalToRemoteLanId (INT4 i4PppBridgeIfIndex,
                                   INT4 *pi4RetValPppBridgeLocalToRemoteLanId);
INT1
nmhGetPppBridgeRemoteToLocalLanId (INT4 i4PppBridgeIfIndex,
                                   INT4 *pi4RetValPppBridgeRemoteToLocalLanId);
INT1
nmhGetFirstIndexPppBridgeConfigTable (INT4 *pi4PppBridgeConfigIfIndex);
INT1
nmhGetNextIndexPppBridgeConfigTable (INT4 i4PppBridgeConfigIfIndex,
                                     INT4 *pi4NextPppBridgeConfigIfIndex);
INT1
nmhValidateIndexInstancePppBridgeConfigTable (INT4 i4PppBridgeConfigIfIndex);
INT1
nmhGetPppBridgeConfigAdminStatus (INT4 i4PppBridgeConfigIfIndex,
                                  INT4 *pi4RetValPppBridgeConfigAdminStatus);
INT1
nmhGetPppBridgeConfigTinygram (INT4 i4PppBridgeConfigIfIndex,
                               INT4 *pi4RetValPppBridgeConfigTinygram);
INT1
nmhGetPppBridgeConfigRingId (INT4 i4PppBridgeConfigIfIndex,
                             INT4 *pi4RetValPppBridgeConfigRingId);
INT1
nmhGetPppBridgeConfigLineId (INT4 i4PppBridgeConfigIfIndex,
                             INT4 *pi4RetValPppBridgeConfigLineId);
INT1
nmhGetPppBridgeConfigLanId (INT4 i4PppBridgeConfigIfIndex,
                            INT4 *pi4RetValPppBridgeConfigLanId);
INT1
nmhSetPppBridgeConfigAdminStatus (INT4 i4PppBridgeConfigIfIndex,
                                  INT4 i4SetValPppBridgeConfigAdminStatus);
INT1
nmhSetPppBridgeConfigTinygram (INT4 i4PppBridgeConfigIfIndex,
                               INT4 i4SetValPppBridgeConfigTinygram);
INT1
nmhSetPppBridgeConfigRingId (INT4 i4PppBridgeConfigIfIndex,
                             INT4 i4SetValPppBridgeConfigRingId);
INT1
nmhSetPppBridgeConfigLineId (INT4 i4PppBridgeConfigIfIndex,
                             INT4 i4SetValPppBridgeConfigLineId);
INT1
nmhSetPppBridgeConfigLanId (INT4 i4PppBridgeConfigIfIndex,
                            INT4 i4SetValPppBridgeConfigLanId);
INT1
nmhTestv2PppBridgeConfigAdminStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4PppBridgeConfigIfIndex,
                                     INT4 i4TestValPppBridgeConfigAdminStatus);
INT1
nmhTestv2PppBridgeConfigTinygram (UINT4 *pu4ErrorCode,
                                  INT4 i4PppBridgeConfigIfIndex,
                                  INT4 i4TestValPppBridgeConfigTinygram);
INT1
nmhTestv2PppBridgeConfigRingId (UINT4 *pu4ErrorCode,
                                INT4 i4PppBridgeConfigIfIndex,
                                INT4 i4TestValPppBridgeConfigRingId);

INT1
nmhTestv2PppBridgeConfigLineId (UINT4 *pu4ErrorCode,
                                INT4 i4PppBridgeConfigIfIndex,
                                INT4 i4TestValPppBridgeConfigLineId);
INT1
nmhTestv2PppBridgeConfigLanId (UINT4 *pu4ErrorCode,
                               INT4 i4PppBridgeConfigIfIndex,
                               INT4 i4TestValPppBridgeConfigLanId);
INT1
nmhGetFirstIndexPppBridgeMediaTable (INT4 *pi4PppBridgeMediaIfIndex,
                                     INT4 *pi4PppBridgeMediaMacType);
INT1
nmhGetNextIndexPppBridgeMediaTable (INT4 i4PppBridgeMediaIfIndex,
                                    INT4 *pi4NextPppBridgeMediaIfIndex,
                                    INT4 i4PppBridgeMediaMacType,
                                    INT4 *pi4NextPppBridgeMediaMacType);
INT1
nmhValidateIndexInstancePppBridgeMediaTable (INT4 i4PppBridgeMediaIfIndex,
                                             INT4 i4PppBridgeMediaMacType);
INT1
nmhGetPppBridgeMediaLocalStatus (INT4 i4PppBridgeMediaIfIndex,
                                 INT4 i4PppBridgeMediaMacType,
                                 INT4 *pi4RetValPppBridgeMediaLocalStatus);
INT1
nmhGetPppBridgeMediaRemoteStatus (INT4 i4PppBridgeMediaIfIndex,
                                  INT4 i4PppBridgeMediaMacType,
                                  INT4 *pi4RetValPppBridgeMediaRemoteStatus);
INT1
nmhGetFirstIndexPppBridgeMediaConfigTable (INT4 *pi4PppBridgeMediaConfigIfIndex,
                                           INT4 *pi4PppBridgeMediaConfigMacType);
INT1
nmhGetNextIndexPppBridgeMediaConfigTable (INT4 i4PppBridgeMediaConfigIfIndex,
                                          INT4
                                          *pi4NextPppBridgeMediaConfigIfIndex,
                                          INT4 i4PppBridgeMediaConfigMacType,
                                          INT4
                                          *pi4NextPppBridgeMediaConfigMacType);
INT1
nmhValidateIndexInstancePppBridgeMediaConfigTable (INT4
                                                   i4PppBridgeMediaConfigIfIndex,
                                                   INT4
                                                   i4PppBridgeMediaConfigMacType);
INT1
nmhGetPppBridgeMediaConfigLocalStatus (INT4 i4PppBridgeMediaConfigIfIndex,
                                       INT4 i4PppBridgeMediaConfigMacType,
                                       INT4
                                       *pi4RetValPppBridgeMediaConfigLocalStatus);
INT1
nmhSetPppBridgeMediaConfigLocalStatus (INT4 i4PppBridgeMediaConfigIfIndex,
                                       INT4 i4PppBridgeMediaConfigMacType,
                                       INT4
                                       i4SetValPppBridgeMediaConfigLocalStatus);
INT1
nmhTestv2PppBridgeMediaConfigLocalStatus (UINT4 *pu4ErrorCode,
                                          INT4 i4PppBridgeMediaConfigIfIndex,
                                          INT4 i4PppBridgeMediaConfigMacType,
                                          INT4
                                          i4TestValPppBridgeMediaConfigLocalStatus);
tGSEM              *
IP6CPGetSEMPtr (tPPPIf * pIf);
INT1
IP6CPCopyOptions (tGSEM * pGSEM, UINT1 Flag, UINT1 PktType);
tGSEM              *
BCPGetSEMPtr (tPPPIf * pIf);
INT1
BCPCopyOptions (tGSEM * pGSEM, UINT1 Flag, UINT1 PktType);
tGSEM              *
BCPSpanIeeeGetSEMPtr (tPPPIf * pIf);
tGSEM              *
BCPIbmSrcRtGetSEMPtr (tPPPIf * pIf);
tGSEM              *
BCPDecLanGetSEMPtr (tPPPIf * pIf);
INT1
CheckAndGetBCPMacPtr (tPPPIf * pIf, UINT4 *n1, UINT4 *n2, UINT4 SecondIndex);
INT1
CheckAndGetIp6Ptr (tPPPIf * pIf);
INT1
CheckAndGetBCPPtr (tPPPIf * pIf);
tIP6CPIf * ALLOC_IP6CP_IF(VOID);
tBCPIf  * ALLOC_BCP_IF(VOID);
