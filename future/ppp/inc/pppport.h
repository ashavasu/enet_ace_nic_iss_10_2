/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppport.h,v 1.2 2014/11/25 13:03:36 siva Exp $
 *
 * Description:This file contains #defines that  should be modified
 *             while porting.
 *
 *******************************************************************/
#include  "gencom.h"
#include  "pppcom.h"
#include  "lcpexts.h"
#include  "lcpdefs.h"
#include  "genexts.h"
#include  "gsemdefs.h"
#include  "pppmp.h"
#include  "pppproto.h"
#include  "ppptask.h"
#include  "globexts.h"
#include  "pppoeport.h"
#include  "hdlcprot.h"
#include  "pppipcp.h"
#include    "pppdpproto.h"
#include    "cfa.h"
#include    "ppp.h"

#ifdef BAP
#include  "pppbacp.h"
#include  "pppbap.h"
#endif
/* #ifdef L2TP_WANTED */
#include "pppl2tp.h"
/* #endif */


extern UINT1        gDefaultTxACCM[];
extern UINT1        gDefaultRxACCM[];

extern UINT1        VJCompAvailable;
#define  PPP_TO_IP             0
#define  PPP_TO_LL             1

/* This global structure is used to set all the character set and languages 
 * supported by the system
 */
                            /* Charset          LangTag   LangTagLength */
tLangTable          gLangTable = { {
                                    {CHAR_SET_UTF8, "i-default", 9, 0}
                                    }
, 1                                /* Num Of Supported Languages */
, 0, 0
};

/* This two dimensional array is used to store all the messages which may go
 * in the LCP or Authentication Packet for all the supported languages. The
 * language name and the message code is used as a row and column to get the
 * particular message for that language
 */

#define ENGLISH_IDX         0    /* Row index for the gInternationMsgs[][] */

#define MAX_MESSAGES        4

#define PAP_MSG_IDX         0
#define CHAP_MSG_SUCC_IDX   1
#define CHAP_MSG_FAIL_IDX   2
#define EAP_MSG_IDTNET_IDX  3


#define     t_MSG_DESC              tCRU_BUF_CHAIN_DESC

const char         *gInternationMsgs[MAX_SUPPORTED_LANGUAGES][MAX_MESSAGES] = {

    {"You have already logged on to the system",    /* PAP_MSG_IDX */
     "You have successfully logged in....",    /* CHAP_MSG_SUCC_IDX */
     "You have failed to log in....",    /* CHAP_MSG_FAIL_IDX */
     "Sample Identity String - >"    /* EAP_MSG_IDTENT_IDX */
     }

};

#define  FR_ADDR_LEN          5
#define  FR_FLAG_VALUE     0x7E
#define  FR_CTRL_VALUE     0x03
#define  FR_NLPID_VALUE    0xCF

extern INT1 nmhGetPPPoEMode ARG_LIST((INT4 *));


