/********************************************************************
 *  * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *   *
 *    * $Id: pppmemmac.h,v 1.1 2014/09/04 10:55:18 siva Exp $
 *     *
 *      * Description:This file contains the macros for sizing params
 *       *
 *        ********************************************************************/
#ifndef _PPPMEMMAC_H_
#define _PPPMEMMAC_H_
#include "globexts.h"
#include "genhdrs.h"
#include "gsemdefs.h"
#include "pppoecom.h"
#include "pppproto.h"
#include "lcpcom.h"
#include "pppipcp.h"
#include "pppcom.h"
#include "pppsz.h"
/*IPCP*/
#define IPCP_ADDR_NODE_MEMPOOL_ID           PPPMemPoolIds[MAX_IPCP_NODE_BLOCKS_SIZING_ID]
#define IPCP_REG_OPTION_MEMPOOL_ID          PPPMemPoolIds[MAX_IPCP_REG_OPTION_BLOCKS_SIZING_ID]
#define IPCP_IF_MEMPOOL_ID                  PPPMemPoolIds[MAX_IPCP_IPCPIF_BLOCKS_SIZING_ID]
#define IPCP_ADDR_MEMPOOL_ID                PPPMemPoolIds[MAX_IPCP_ADDR_BLOCKS_SIZING_ID]
#define IPCP_SLCOMPRESS_MEMPOOL_ID          PPPMemPoolIds[MAX_IPCP_SLCOMPRESS_SIZING_ID]
/*LCP*/
#define LCP_AUTH_MEMPOOL_ID                 PPPMemPoolIds[MAX_LCP_AUTH_BLOCKS_SIZING_ID]
#define LCP_LQM_MEMPOOL_ID                  PPPMemPoolIds[MAX_LCP_LQM_BLOCKS_SIZING_ID]
#define LCP_MSG_DESC_MEMPOOL_ID             PPPMemPoolIds[MAX_LCP_MSG_DESC_BLOCKS_SIZING_ID]
#define LCP_PPPIF_MEMPOOL_ID                PPPMemPoolIds[MAX_LCP_PPPIF_BLOCKS_SIZING_ID]
/*PPPOE*/
#define PPPOE_PADOLIST_MEMPOOL_ID           PPPMemPoolIds[MAX_PPPOE_PADOLIST_BLOCKS_SIZING_ID]
#define PPPOE_SESSION_MEMPOOL_ID            PPPMemPoolIds[MAX_PPPOE_SESSION_BLOCKS_SIZING_ID]
#define PPPOE_CLIENT_DIS_MEMPOOL_ID         PPPMemPoolIds[MAX_PPPOE_CLIENT_DIS_BLOCKS_SIZING_ID]
#define PPPOE_BINDING_MEMPOOL_ID            PPPMemPoolIds[MAX_PPPOE_BINDING_BLOCKS_SIZING_ID]
#define PPPOE_VLAN_MEMPOOL_ID               PPPMemPoolIds[MAX_PPPOE_VLAN_BLOCKS_SIZING_ID]
#define PPPOE_SERVICE_MEMPOOL_ID            PPPMemPoolIds[MAX_PPPOE_SERVICE_BLOCKS_SIZING_ID]
/*SRC*/
#define PPP_SECRET_MEMPOOL_ID               PPPMemPoolIds[MAX_PPP_SECRET_BLOCKS_SIZING_ID]
#define PPP_SERVICES_MEMPOOL_ID             PPPMemPoolIds[MAX_PPP_SERVICES_BLOCKS_SIZING_ID]
#define PPP_LINEAR_BUF_MEMPOOL_ID           PPPMemPoolIds[MAX_PPP_LINEAR_BUF_SIZING_ID]
#define PPP_BUF_MEMPOOL_ID                  PPPMemPoolIds[MAX_PPP_BUF_SIZING_ID]
#endif
