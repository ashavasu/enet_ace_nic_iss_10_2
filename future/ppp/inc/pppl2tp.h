/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppl2tp.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the L2PP header files, constants
 *	           and macros used by PPP to interface with L2TP module.
 *
 *******************************************************************/
#ifndef __PPP_PPPL2TP_H__
#define __PPP_PPPL2TP_H__

#ifdef L2TP_WANTED

VOID PPPLACSendConfReqPkt(tPPPIf *pIf, t_MSG_DESC *pOutBuf, UINT1 Code);
VOID PPPSendDataToL2TP(tPPPIf *pIf, t_MSG_DESC *pOutPDU, UINT4 Length);
VOID PPPIndicateLCPUpToLNS(tPPPIf *pIf);
INT1 PPPIndicateAuthInfoToLAC ( UINT4           u4IfIndex,
                          UINT2           u2AuthType,
                          UINT2           Id, 
                          UINT1           PeerId[],
                          UINT2           IdLen, 
                          UINT1           Passwd[],
                          UINT2           PasswdLen,
                          UINT1           Challenge[],
                          UINT2           ChalLen
                        );

VOID PPPIndicateConfReqPktToLAC(tPPPIf *pPPPIf, t_MSG_DESC *pBuf, UINT2 Len, UINT1 Type);
BOOLEAN PPPIsL2TPTunnelModeLAC(tPPPIf *pIf);
BOOLEAN PPPIsL2TPTunnelVoluntary (tPPPIf *pIf);
VOID PPPIndicateLCPDownToL2TP(tPPPIf *pIf);

VOID PPPGetDefaultVal         PROTO((tLCPOptions * pLCPOpt, UINT1 OptIndex));
BOOLEAN PPPIsL2TPSessionExist PROTO((tPPPIf * pIf));
#endif
#endif
