/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppprad.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the prototypes for the functions 
 * used by the RADIUS module
 *
 *******************************************************************/
#include "radius.h"
/*Function prototypes*/
VOID	PppRadFillOthersAuth(tRADIUS_INPUT_AUTH *,UINT4 );
VOID	PppRadFillOthersAcct(tRADIUS_INPUT_ACC *,UINT4 );
VOID  PppMemFreeServices(tSERVICES	*);
VOID PppRadResponse(VOID   *);
VOID PppRadReceivedResponse(VOID  *);
VOID  PppUpdateAuthorizationParams(tPPPIf  *,tRadInterface  *);
VOID PppRadSendPapStatus(tPPPIf *, UINT1 , UINT1  *, UINT1 );
UINT1 *PppRadGetAcctSessionId(VOID);
/**********************/

#ifndef HTONL
#define HTONL	OSIX_HTONL
#endif
#ifndef NTOHL
#define NTOHL	OSIX_NTOHL
#endif

#define DEFAULT_NAS_IPADDRESS	0x0a000001

extern UINT4 gu4PppSessionId;

extern UINT1	VJCompAvailable;
extern UINT1	gu1IPCPAddrModeSelector;
