/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppl2tpexp.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the prototypes for the functions 
 * used by the PPP-L2TP module
 *
 *******************************************************************/
#ifndef __PPP_PPPL2TPEXP_H__
#define __PPP_PPPL2TPEXP_H__

#ifdef L2TP_WANTED

VOID PPPLACSendConfReqPkt(tPPPIf *pIf, t_MSG_DESC *pOutBuf, UINT1 Code);
VOID PPPSendDataToL2TP(tPPPIf *pIf, t_MSG_DESC *pOutPDU, UINT4 Length);
VOID PPPSendDataToL2TP(tPPPIf *pIf, t_MSG_DESC *pOutPDU, UINT4 Length);
VOID PPPIndicateLCPUpToL2TP(tPPPIf *pIf);
VOID PPPIndicateLCPUpToLNS(tPPPIf *pIf);
VOID PPPIndicateAuthInfoToLAC(tPPPIf *pIf, UINT1 Id, UINT1 PeerId[], UINT1 IdLen, UINT1 Passwd[], UINT1 PasswdLen);
VOID PPPIndicateConfReqPktToLAC(tPPPIf *pPPPIf, t_MSG_DESC *pBuf, UINT2 Len);
UINT1 PPPIsTunnelModeLAC(tPPPIf *pIf);
VOID PPPIndicateLCPDownToL2TP(tPPPIf *pIf);
#endif

#endif

