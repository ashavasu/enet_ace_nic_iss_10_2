/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppexts.h,v 1.5 2014/09/29 12:36:17 siva Exp $
 *
 * Description:This file contains the definitions used by the external
 * module
 *
 *******************************************************************/
#ifndef __PPP_PPPEXTS_H__
#define __PPP_PPPEXTS_H__


#ifdef GLOBAL_VAR
t_SLL           PPPIfList;
UINT1           *pAllocPtr;
const char              *pNullAddr = "\0\0\0\0\0\0";
UINT1           ExtractedLength;
UINT4           GenReturnData;
UINT1           *pCopyPtr;
UINT4           MinMTU;
UINT2           LinkNum;

#else
extern  t_SLL           PPPIfList;
extern  UINT1           *pAllocPtr;
extern  UINT1           *pNullAddr;
extern  UINT1           ExtractedLength;
extern  UINT4           GenReturnData;
extern  UINT1           *pCopyPtr;
extern UINT1             gu1AuthMode;
extern UINT4 MinMTU;    /*To keep track of minimum MTU of the links */ 
extern UINT2 LinkNum;   /* Denotes the index value of the link */


#endif

extern t_SLL    AddrPoolList;
extern INT4     i4DevFd;
extern UINT1    gu1aaaMethod;

extern INT1 nmhGetFsl2tpExtConfigSystemMode ARG_LIST((INT4 *));
extern INT1 nmhSetFsIpRouteStatus ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,
                                            INT4  ,INT4 ));
extern INT1 nmhSetFsIpRouteIfIndex ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,
                                             INT4  ,INT4));
extern INT1 nmhSetFsIpRouteType ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  , 
                                          INT4  ,INT4 ));
extern INT1 nmhSetFsIpRoutePreference ARG_LIST((UINT4  , UINT4  , INT4  , 
                                                UINT4  , INT4  ,INT4 ));
extern INT1 nmhSetIfMainRowStatus ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetIfMainAdminStatus ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetIfAlias ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetIfMainType ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetIfIpAddrAllocMethod ARG_LIST((INT4  ,INT4 ));
extern INT1
nmhTestv2IfStackStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));
extern INT1
nmhSetIfStackStatus ARG_LIST((INT4  , INT4  ,INT4 ));
extern INT1 nmhSetIfWanPersistence ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetIfAdminStatus ARG_LIST((INT4  ,INT4 ));
extern INT4 L2TPSNMPSessionGetFirstIndex (VOID);
extern INT1 nmhGetL2tpTunnelStatsActiveSessions ARG_LIST((INT4 ,UINT4 *));
extern INT4 CfaHandleDnsPacketForWan        PROTO ((UINT2 *pu2RetCode, 
                                                    UINT2 u2IfIndex));
extern INT4 CfaSetIfActiveStatus (UINT4 u4IfIndex, INT4 i4Status);
extern INT1 nmhTestv2IfAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhGetIfAdminStatus ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsl2tpConfigSystemMode(INT4 *);

#endif  /* __PPP_PPPEXTS_H__ */
