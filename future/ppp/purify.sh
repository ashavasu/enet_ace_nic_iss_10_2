#!/bin/sh
# $Id: purify.sh,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
# Desc: Code beautifier.
# 

EXTN="PP"
PUR_CFG_FILE="$HOME/.purify.cfg"
num=0
args=0
debug=1
opt_b=0
opt_h=0
opt_i=0
opt_l=0
opt_t=0
opt_I=0
opt_v=0

CheckIndentVersion()
{
    n=0
    b_pad=0
    l_pad=0

#   Get the version being used and store in l (mnemonic - local)
    l=(`indent --version  | sed -e 's/GNU indent //' -e 's/\./ /g'`);
#   We need atleast 'b' (mnemonic - base)
    b=(2 2 6)

#   Who is shorter.
    if [ "${#b[@]}" -lt "${#l[@]}" ]; then
        min_elems=${#b[@]}
    else
        min_elems=${#l[@]}
    fi

#   Who is longer.
    if [ "${#b[@]}" -gt "${#l[@]}" ]; then
        elems=${#b[@]}
        l_pad=1
    else
        elems=${#l[@]}
        b_pad=1
    fi

    your=`echo ${l[*]}|sed -e 's/ /\./g'`
    our=`echo ${b[*]}|sed -e 's/ /\./g'`

#   Iterate through max elems, 
#   pad with zeros suitably, for sh's sake.
    while [ "$n" -lt "$elems" ]
    do
        if [ "$n" -ge "$min_elems" ];then
            if [ "$l_pad" -eq "1" ]; then
                l[$n]=0
            fi
            if [ "$b_pad" -eq "1" ]; then
                b[$n]=0
            fi
        fi

        a=${l[$n]}
        b=${b[$n]}

#       This m/c has something better. Continue with purify.
        if [ "$a" -gt "$b" ];then
            return 0
        fi

#       This m/c has something older than b. Abort.
        if [ "$a" -lt "$b" ];then
            echo ""
            echo "Please upgrade to indent version $our"
            echo "You are using $your"
            echo ""
            return 1
        fi

#       Get next digit.
        n=`expr $n + 1`
    done

    return 0
}

[ -f /usr/bin/indent ]
RETVAL=$?
if [ $RETVAL -ne 0 ]; then
    echo "Could not find GNU indent in /usr/bin"
    echo "Exiting ..."
    exit 0
fi

CheckIndentVersion
RETVAL=$?

if [ $RETVAL -ne 0 ];then
    exit $RETVAL
fi

QueryUser()
{
    dir=$*

# Options b and l.
if [ $opt_l != "l" ]; then

    [ -e $PUR_CFG_FILE ]

    R1=$?

    ECHO2 "file stat returned $R1"

    if [ $R1 -eq 0 ]; then

        ECHO2 "OK CFG file exists"
        OUT=`grep "PURIFY_RECURSE=YES" $PUR_CFG_FILE`
        R2=$?
        ECHO2 "grep returned $R2"

    fi

    if [ $R1 -eq 1 ] || [ $R2 -eq 1 ]; then

        echo    
        echo    "You've chosen to run purify recursively on $dir/ dir."
        echo    "Type n to abort."
        echo    "Type y to continue."
        echo -n "Type Y if you dont want to see this question again [yYn]"
        read choice


        if [ x$choice = "xY" ]; then

            ECHO2 "OK, i'll make sure"
            echo "PURIFY_RECURSE=YES" > ~/.purify.cfg
            echo

        elif [ x$choice != "xy" ]; then

            echo "OK, try later ..."
            echo
            exit 0

        fi

    fi
fi
}

CleanFile() {

    f=$1

    if [ $opt_i = "i" ];then

        echo -n "$f ? "
        read choice

        if [ x$choice = "xy" ]; then
            if [ $opt_t != "t" ]; then
                echo "c $f"
                echo
            fi
        elif [ x$choice = "xq" ]; then
            return 2
        else
            return 1
        fi

    fi

    ECHO2 "Cleaning $f"

#   Save a copy for the -b and -t modes.
    if [ "$opt_b" = "b" ] || [ $opt_t = "t" ] || [ $opt_t = "T" ]; then
        cp -i $f $f.$EXTN
    fi


#   Do actual indent ... O/P redirected to $f.$$
    indent  -TBOOLEAN -TUINT1 -TUINT2 -TUINT4 -TINT1 -TINT2 -TINT4 -TVOID\
    -ts4 -pi4 -sob -bap -i4 -bli0 -bbo  -lc80 -l80 -di20  $f\
    -o -st -cli4 -cbi0 |\
    sed -e 's/	/    /g' > $f.$$ 2>/dev/null

#   If error in processing, do not botch the original file.
    [ -s $f.$$ ] 

    RETVAL=$?

    if [ $RETVAL -ne 0 ];then
        echo "$f - Error during processing..."
        echo "Fix indent errors if any OR send mails to cvsadmin@products."
        echo "Stopping."
        exit 1
    fi

#   Show progress for the non-i and non-t modes.
    if [ "$opt_I" != "I" ] && [  "$opt_i" != "i" ] && [ ! "$opt_t" = "t" ] &&
        [ $opt_t != "T" ]; then
        echo $f
    fi


#   Replace the original file with the indented file for all
#   modes other than -t.
    if [ $opt_t = "t" ] || [ $opt_t = "T" ]; then
        mv $f.$$ $f.$EXTN
    else
        mv $f.$$ $f

    fi

#   Update count of files cleaned
    num=`expr $num + 1`

    return 0
}

doClean () {

    f=$1

        ###################################################
        # If f is a directory and exists,                 #
        #     check if -l is set.                         #
        #     If so, call doClean on given directory.     #
        #     else call   doClean for the entire subtree. #
        # Else if f is a regular file                     #
        #     do the cleanup.                             #
        # Else                                            #
        #     Something wrong !                           #
        ###################################################


    if [ -d $f ]; then

        echo "$f - dir" ;

        QueryUser $f

        if [ $opt_l = "l" ]; then
            file=`find $1 -name '*.c' -maxdepth 1`
        else
            file=`find $1 -name '*.c'`
        fi

        for l in $file

        do

            doClean $l

            if [ $? -eq 2 ]; then
                return 2;
            fi

        done

    elif [ -f $f ]; then

        CleanFile $f

        RETVAL=$?
        if [ $RETVAL -ne 0 ]; then
            return $RETVAL
        fi


#       Diff list in brief.
        if [ $opt_t = "t" ]; then

            OUT=`diff -u $f $f.$EXTN`

            if [ "$OUT" ]; then
                echo "D $f"
            else
                echo "c $f"
            fi
            rm $f.$EXTN 2>/dev/null
        fi

#       Diff list.
        if [ $opt_t = "T" ]; then

            OUT=`diff -u $f $f.$EXTN`

            echo
            echo "Testing $f..."
            echo

            if [ "$OUT" ]; then
                echo "$OUT"
                echo
            else
                echo "$f - Already clean"
            fi
            rm $f.$EXTN 2>/dev/null
        fi

    else

        echo "$1 - File not found or not recognised, Skipping ..."

    fi

    return 0

}

# Dummy echo
ECHO2 ()
{
    _x_=1
}

# do echo if debug is set.
ECHO ()
{
    if [ $debug -ne 0 ];then
        echo $*
    fi
}

Usage ()
{
    echo
    echo "Indents file|dir as per FS guidelines."

    echo
    echo "Usage: `basename $0` [options] <file|dir> ..."
    echo "-b        Purify and Backup original file to <file>.PP"
    echo "-h        Prints this help message"
    echo "-i        Interactive mode"
    echo "-l        Do not recurse"
    echo "-t        Dirty files in brief. Does not execute indent."
    echo "-T        Dirty files length.   Do not execute indent."
    echo
}

# Get the User options, record the args, do not shift $*
GETOPT ()
{
    ECHO2 "INSIDE GETOPT"
    ECHO2 $*

    while true ; do
        getopts "I:biltThv" opt $*
        RETVAL=$?

        if [ $RETVAL != 0 ] ; then break; fi

        if [ "$opt" = "i" ]; then opt_i="i"; fi
        if [ "$opt" = "b" ]; then opt_b="b"; fi
        if [ "$opt" = "I" ]; then opt_I="I"; EXTN=$OPTARG; fi
        if [ "$opt" = "l" ]; then opt_l="l"; fi
        if [ "$opt" = "t" ]; then opt_t="t"; EXTN="PURE"; fi
        if [ "$opt" = "T" ]; then opt_t="T"; EXTN="PURE"; fi
        if [ "$opt" = "h" ]; then opt_h="h"; fi
        if [ "$opt" = "v" ]; then opt_v="v"; fi

        args=`expr $args + 1`

        if [ "$opt" = "I" ]; then shift; fi
    done
}


GETOPT $*;

if [ $opt_h = "h" ] || [ $# -eq $args ]; then
    Usage $*
    exit
fi

if [ $opt_v = "v" ]; then
    echo `basename $0`   Version $Revision: 1.3
    exit
fi


# If script is invoked without args, set $* to .
ECHO2 "opt_i = $opt_i"
ECHO2 "opt_b = $opt_b"
ECHO2 "opt_I = $opt_I"
ECHO2 "EXTN  = $EXTN";
ECHO2 "opt_l = $opt_l"
ECHO2 "opt_t = $opt_t"
ECHO2 "args = $args"

# Gobble the options.
shift $args


if [ $opt_t = "t" ] || [ $opt_t = "T" ]; then

    ECHO2 "TESTING ..."

    while true ; do

        case "$1" in

            "")
                break;;

            *.c)

#            echo "$1 - C file";
                f=$1
                doClean $1;

#               Get out if 'N' was typed.
                if [ $? -eq 2 ]; then
                    break
                fi


                shift ;;

            *)

            if [ -d $1 ]; then
                ECHO2 "$1 - dir" ;

                doClean $1

                if [ $? -eq 2 ]; then
                    break
                fi

            else
                echo "$1 - Unknown file/dir, Skipping ..."
            fi

            shift ;;
        esac
    done

    exit 0;

fi



while true ; do
	case "$1" in
        "")
            break;;

        *.c)
            ECHO2 "$1 - C file";
            f=$1
            doClean $1;

            RETVAL=$?

            shift ;;

        *.h)
            echo "$1 - Unrecognized file";echo
            shift ;;

        *)
            doClean $1

            shift ;;
    esac

    if [ $RETVAL -eq 2 ]; then
        break
    fi

done 


if [ "$opt_b" = "b" ]; then

    if [ $num -ge 1 ]; then
        echo "Original files have been saved as <file>.$EXTN"
    else
        echo "No files specified."
    fi

fi

exit
# END OF SCRIPT.
