/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppbacp.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains callback routines of BACP 
 *             module.It also contains routines for the input 
 *             BACP packet processing.
 *
 *******************************************************************/
#include "bacpcom.h"
#include "gsemdefs.h"
#include "gcpdefs.h"
#include "lcpdefs.h"
#include "mpdefs.h"
#include "pppmp.h"
#include "pppbap.h"

#include "globexts.h"

#define    GET_BAP_INFO        MPInfo.BundleInfo.BWMProtocolInfo.pProtocolInfo

#define    GET_IS_INITIATOR    pIf->MPInfo.BundleInfo.IsInitiator

/******************** GLOBALS ************************************/
tOptHandlerFuns     BACPFavoredPeerHandlingFns = {
    BACPProcessFavoredPeerConfReq,
    BACPProcessFavoredPeerConfNak,
    NULL,
    BACPReturnFavoredPeerPtr,
    NULL
};

tGSEMCallbacks      BACPGSEMCallback = {
    BACPUp,
    BACPDown,
    NULL,
    NULL,
    NCPRxData,
    NCPTxData,
    NULL,
    NULL
};

tRangeInfo          FavoredPeerRange = { {0}
, {0xffffffff}
};

tGenOptionInfo      BACPGenOptionInfo[] = {

    {FAVORED_PEER_OPTION, RANGE, BYTE_LEN_4, 6, &BACPFavoredPeerHandlingFns,
     {&FavoredPeerRange}
     , NOT_SET, 0, 0}
};

tOptNegFlagsPerIf   BACPOptPerIntf[] = {

    {FAVORED_PEER_OPTION,
     DESIRED_SET | ALLOWED_FOR_PEER_SET | FORCED_TO_REQUEST_SET, 0}
};

tBACPCounters       BACPCounters;

/******************** Function Prototypes ************************************/
VOID                BACPOptionInit (tBACPOptions * pBACPOpt);

/***********************************************************************/
/*                      INTERFACE  FUNCTIONS                           */
/*********************************************************************
*  Function Name : BACPEnableIf
*  Description   :
*              This function is called when there is an  BACP Admin Open 
*  for an interface from the SNMP It  triggers an OPEN event to the GSEM and 
*  passes the BACP tGSEM corresponds to this interface as parameter.
*  Parameter(s)    :
*    pBACPIf    -    Points to the BACP interface structure to be enabled
*  Return Values : VOID
*********************************************************************/
VOID
BACPEnableIf (tBACPIf * pBACPIf)
{

    pBACPIf->AdminStatus = ADMIN_OPEN;

    /* Check whether the link is already in the Operational state. 
       If so, then indicate the GSEM about the  link UP 
     */

    if ((pBACPIf->BACPGSEM.CurrentState != REQSENT)
        || (pBACPIf->BACPGSEM.CurrentState != ACKRCVD)
        || (pBACPIf->BACPGSEM.CurrentState != ACKSENT))
    {
        pBACPIf->BACPGSEM.pNegFlagsPerIf[FAVORED_PEER_IDX].FlagMask |=
            ACKED_BY_PEER_SET;
    }

    GSEMRun (&pBACPIf->BACPGSEM, OPEN);

    if ((pBACPIf->BACPGSEM.pIf->LcpStatus.LinkAvailability == LINK_AVAILABLE)
        && (pBACPIf->BACPGSEM.CurrentState != CLOSED))
    {
        GSEMRun (&pBACPIf->BACPGSEM, UP);
    }

    GSEMRestartIfNeeded (&pBACPIf->BACPGSEM);

}

/*********************************************************************
*  Function Name : BACPDisableIf
*  Description   :
*          This function is called when there is an Admin Close for an 
*  interface from the SNMP It triggers a CLOSE event to the GSEM and passes 
*  the BACP GSEMStructure corresponds to this interface as a parameter. 
*  Parameter(s)  :
*            pBACPIf - points to  the BACP interface structure to be  disabled
*  Return Values : VOID
*********************************************************************/
VOID
BACPDisableIf (tBACPIf * pBACPIf)
{

    pBACPIf->AdminStatus = ADMIN_CLOSE;
    GSEMRun (&pBACPIf->BACPGSEM, CLOSE);

}

/*********************************************************************
*  Function Name : BACPInit
*  Description   :
*              This function initializes the BACP GSEM and  sets 
*  the  BACP configuration option variables to their default values. It is 
*  invoked by the BACPCreateIf() function.
*  Parameter(s)  :
*        pBACPIf -   points to the BACP interface structure
*  Return Values : OK/NOT_OK 
*********************************************************************/
INT1
BACPInit (tPPPIf * pBundleIf)
{
    tBACPIf            *pBACPIf;

    FavoredPeerRange.MinVal.LongVal = 0;
    FavoredPeerRange.MaxVal.LongVal = 0xffffffff;

    pBACPIf =
        &(((tBAPInfo *) pBundleIf->MPInfo.BundleInfo.BWMProtocolInfo.
           pProtocolInfo)->BACPIf);
    pBACPIf->BACPGSEM.pIf = pBundleIf;

    /* Initialize the Option fields */
    BACPOptionInit (&pBACPIf->BACPOptionsAckedByPeer);
    BACPOptionInit (&pBACPIf->BACPOptionsAckedByLocal);

    pBACPIf->AdminStatus = STATUS_DOWN;
    pBACPIf->OperStatus = STATUS_DOWN;
    pBACPIf->FavoredEnd = LOCAL_END;

    if (GSEMInit (&pBACPIf->BACPGSEM, MAX_BACP_OPT_TYPES) == NOT_OK)
    {
        return (NOT_OK);
    }
    pBACPIf->BACPGSEM.Protocol = BACP_PROTOCOL;
    pBACPIf->BACPGSEM.pCallbacks = &BACPGSEMCallback;
    pBACPIf->BACPGSEM.pGenOptInfo = BACPGenOptionInfo;
    MEMCPY (pBACPIf->BACPGSEM.pNegFlagsPerIf, &BACPOptPerIntf,
            (sizeof (tOptNegFlagsPerIf) * MAX_BACP_OPT_TYPES));

    return (OK);
}

/***********************************************************************/
/*                      INTERNAL  FUNCTIONS                            */
/***********************************************************************
*  Function Name : BACPOptionInit
*  Description   :
*               This function initializes the BACPOption structure and is
*  called by the BACPInit function.
*  Parameter(s)  :
*       pBACPOpt   -  points to the BACPOption structure  to be initialized.
*  Return Value  :
*********************************************************************/
VOID
BACPOptionInit (tBACPOptions * pBACPOpt)
{

    pBACPOpt->FavoredPeerNumber = 0;

}

/*********************************************************************
*  Function Name : BACPUp
*  Description   :
*              This function is called once the network level configuration  
*  option is negotiated  successfully. This function indicates  to the higher 
*  layers  that the BACP is up.  The pUp  field of  the BACPGSEMCallback points *  to this function and is called from the GSEM Module.
*  Parameter(s)  :
*            pBACPGSEM  - pointer to the BACP tGSEM 
*                                   corresponding to the current interface.
*  Return Values : VOID 
*********************************************************************/
VOID
BACPUp (tGSEM * pBACPGSEM)
{
    tBACPIf            *pBACPIf;
    tBAPInfo           *pBAPInfo;
    tPPPIf             *pBundleIf;

    pBundleIf = pBACPGSEM->pIf->MPInfo.MemberInfo.pBundlePtr;
    pBAPInfo = (tBAPInfo *) pBACPGSEM->pIf->GET_BAP_INFO;
    pBACPIf = &(pBAPInfo->BACPIf);

    PPPHLINotifyProtStatusToHL (pBACPGSEM->pIf, BACP_PROTOCOL, UP);

    pBACPIf->OperStatus = STATUS_UP;
    pBACPIf->FavoredEnd =
        (pBACPIf->BACPOptionsAckedByPeer.FavoredPeerNumber <
         pBACPIf->BACPOptionsAckedByLocal.
         FavoredPeerNumber) ? LOCAL_END : REMOTE_END;

    /* If the Bundle entry is not NULL and the same has been successfully
       negotiated on either one of the side, then update the Bundle Info.
     */

    if (pBundleIf != NULL)
    {

        if ((pBundleIf->MPInfo.BundleInfo.MPFlagsPerIf[MRRU_IDX].
             FlagMask & ACKED_BY_PEER_MASK)
            || (pBundleIf->MPInfo.BundleInfo.MPFlagsPerIf[MRRU_IDX].
                FlagMask & ACKED_BY_LOCAL_MASK))
        {
            MPUpdateBundleNCPInfo (pBundleIf, pBACPGSEM->pIf, BACP_PROTOCOL);
        }
    }

}

/*********************************************************************
*  Function Name : BACPDown
*  Description   :
*          This function is invoked  when the BACP protocol is leaving the 
*  OPENED state. It  indicates the higher layers  about the operational status 
*  of the BACP.  The pDown  field of  the BACPGSEMCallback structure  points to 
*  this function and is called from the GSEM Module.
*  Parameter(s)  :
*            pBACPGSEM  - pointer to the BACP tGSEM 
*                       corresponding to the current interface.
*  Return Values : VOID 
*********************************************************************/
VOID
BACPDown (tGSEM * pBACPGSEM)
{
    tBAPInfo           *pBAPInfo;
    tBACPIf            *pBACPIf;

    pBAPInfo = (tBAPInfo *) pBACPGSEM->pIf->GET_BAP_INFO;
    pBACPIf = &(pBAPInfo->BACPIf);

    PPPHLINotifyProtStatusToHL (pBACPGSEM->pIf, BACP_PROTOCOL, DOWN);

    pBACPIf->OperStatus = STATUS_DOWN;

    BACPOptionInit (&pBACPIf->BACPOptionsAckedByPeer);
    BACPOptionInit (&pBACPIf->BACPOptionsAckedByLocal);

}

/***********************************************************************
    OPTION SPECIFIC FUNCTIONS FOR PROCESSING CONF_REQ
************************************************************************/
/*********************************************************************
*  Function Name : BACPProcessFavoredPeerConfReq
*  Description   :
*        This function is invoked to process the Favored Peer option present
*  in the CONF_REQ and to construct appropriate response
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer
*     tOptVal    -  Option Value received  to be processed
*     Offset     -  indicates the place in the outgoing buffer at which 
*                   the option value is appended
*  PresenceFlag  -  indicates whether the option is present in the recd.
*                   CONF_REQ packet
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer
*********************************************************************/
INT1
BACPProcessFavoredPeerConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt,
                               tOptVal OptVal, UINT2 Offset, UINT1 PresenceFlag)
{
    tBACPIf            *pBACPIf;
    tBAPInfo           *pBAPInfo;
    UINT1               IsInitiator;
    UINT4               MagicNumber;

    PPP_UNUSED (pInPkt);
    pBAPInfo = (tBAPInfo *) pGSEM->pIf->GET_BAP_INFO;
    pBACPIf = &pBAPInfo->BACPIf;
    IsInitiator = pGSEM->GET_IS_INITIATOR;

    if ((PresenceFlag == NOT_SET)
        || ((PresenceFlag == SET)
            &&
            ((pBACPIf->
              BACPOptionsAckedByPeer.FavoredPeerNumber == OptVal.LongVal)
             || (OptVal.LongVal == 0) || ((IsInitiator == LOCAL_END)
                                          && (OptVal.LongVal ==
                                              INITIATOR_FAVORED_PEER_NUMBER)))))
    {

        MagicNumber = GENERATE_MAGIC_NUMBER ();
        ASSIGN4BYTE (pGSEM->pOutParam, Offset, MagicNumber);
        return (BYTE_LEN_4);
    }

    pBACPIf->BACPOptionsAckedByLocal.FavoredPeerNumber = OptVal.LongVal;
    return (ACK);
}

/***********************************************************************
    OPTION SPECIFIC FUNCTIONS FOR PROCESSING CONF_NAK
************************************************************************/
/*********************************************************************
*  Function Name : BACPProcessFavoredPeerConfNak 
*  Description   :
*        This function is invoked to process the Favored Peer option present
*  in the CONF_NAK and to construct appropriate response 
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*  Return Value  :  OK/NOT_OK/CLOSE 
*********************************************************************/
INT1
BACPProcessFavoredPeerConfNak (tGSEM * pGSEM, t_MSG_DESC * pInPkt,
                               tOptVal OptVal)
{
    tBAPInfo           *pBAPInfo;
    tBACPIf            *pBACPIf;
    UINT1               IsInitiator;

    PPP_UNUSED (pInPkt);
    PPP_UNUSED (OptVal);
    pBAPInfo = (tBAPInfo *) pGSEM->pIf->GET_BAP_INFO;
    pBACPIf = &pBAPInfo->BACPIf;
    IsInitiator = pGSEM->GET_IS_INITIATOR;

    if (IsInitiator == LOCAL_END)
    {
        return (DISCARD);
    }
    pBACPIf->BACPOptionsAckedByPeer.FavoredPeerNumber =
        GENERATE_MAGIC_NUMBER ();

    return (OK);
}

/***********************************************************************
    OPTION SPECIFIC FUNCTIONS FOR RETURNING THE ADDRESS OF OPTION DATA
************************************************************************/
/*********************************************************************
*  Function Name : BACPReturnFavoredPeerPtr 
*  Description   :
*     This function returns the address of the location where the Favored  
*  Peer Number value to be included in the CONF_REQ, is stored.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*     OptLen     -  used to return the length of the option value (in
*                   case the option is of 'STRING' type)
*  Return Value  :  returns the option data address
*********************************************************************/
VOID               *
BACPReturnFavoredPeerPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    tBAPInfo           *pBAPInfo;
    tBACPIf            *pBACPIf;
    UINT1               IsInitiator;

    PPP_UNUSED (OptLen);
    pBAPInfo = (tBAPInfo *) pGSEM->pIf->GET_BAP_INFO;
    pBACPIf = &pBAPInfo->BACPIf;
    IsInitiator = pGSEM->GET_IS_INITIATOR;

    switch (IsInitiator)
    {
        case LOCAL_END:
            pBACPIf->BACPOptionsAckedByPeer.FavoredPeerNumber =
                INITIATOR_FAVORED_PEER_NUMBER;
            break;
        case REMOTE_END:
            pBACPIf->BACPOptionsAckedByPeer.FavoredPeerNumber =
                RECEIVER_FAVORED_PEER_NUMBER;
            break;
        default:
            do
            {
                pBACPIf->BACPOptionsAckedByPeer.FavoredPeerNumber =
                    GENERATE_MAGIC_NUMBER ();
            }
            while (pBACPIf->BACPOptionsAckedByPeer.FavoredPeerNumber ==
                   pBACPIf->BACPOptionsAckedByLocal.FavoredPeerNumber);
    }

    return (&pBACPIf->BACPOptionsAckedByPeer.FavoredPeerNumber);
}

/*********************************************************************
*  Function Name : BACPCopyOptions 
*  Description   :
*    This function is used to copy the BACP options.
*  Input         :
*    pGSEM        -     Points to the GSEM structure
*    Flag        -     Specifies whether to alloc or free the temporary 
*                    variable.
*    PktType        -    Specifies whether the function is called from request
*                    processing or Nak processing.                  
*  Return Value        :
*                    DISCARD on allocation error, OK otherwise.
*********************************************************************/
INT1
BACPCopyOptions (tGSEM * pGSEM, UINT1 Flag, UINT1 PktType)
{
    UINT1               Size;
    tBAPInfo           *pBAPInfo;
    tBACPIf            *pBACPIf;
    tBACPOptions       *pTemp;

    pBAPInfo = (tBAPInfo *) pGSEM->pIf->GET_BAP_INFO;
    pBACPIf = &pBAPInfo->BACPIf;
    Size = sizeof (tBACPOptions);

    if (PktType == CONF_REQ_CODE)
    {
        pTemp = &pBACPIf->BACPOptionsAckedByLocal;
    }
    else
    {
        pTemp = &pBACPIf->BACPOptionsAckedByPeer;
    }

    if (Flag == NOT_SET)
    {
        if ((pCopyPtr = (UINT1 *) ALLOC_STR (Size)) == NULL)
        {
            return (DISCARD);
        }
        MEMCPY (pCopyPtr, (UINT1 *) pTemp, Size);
        if (PktType == CONF_REQ_CODE)
        {
            BACPOptionInit (&pBACPIf->BACPOptionsAckedByLocal);
        }
    }
    else
    {
        if (Flag == SET)
        {
            MEMCPY (pTemp, (tBACPOptions *) pCopyPtr, Size);
        }
        if (pCopyPtr != NULL)
        {
            FREE_STR (pCopyPtr);
            pCopyPtr = NULL;
        }
    }

    return (OK);
}

/*********************************************************************
*  Function Name : BACPGetSEMPtr
*  Description   :
*    This function returns the pointer to the BACP GSEM taking the tPPPIf
*   structure as a parameter.
*  Input         :
*       pIf -   Points to the PPP Interface structure.
*  Return Value  :
*           Returns a pointer to the BACP GSEM.
*********************************************************************/
tGSEM              *
BACPGetSEMPtr (tPPPIf * pIf)
{
    tBAPInfo           *pBAPInfo;
    tBACPIf            *pBACPIf;

    if (pIf->BundleFlag == BUNDLE)
    {
        pBAPInfo = (tBAPInfo *) pIf->GET_BAP_INFO;
    }
    else
    {
        pBAPInfo = (tBAPInfo *) pIf->MPInfo.MemberInfo.pBundlePtr->GET_BAP_INFO;
    }

    pBACPIf = &pBAPInfo->BACPIf;
    if (pBACPIf != NULL)
    {
        return (&pBACPIf->BACPGSEM);
    }

    return (NULL);
}
