/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppprmtl.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description: Low Level Routines for RM module.
 *
 *******************************************************************/

#include "pppsnmpm.h"
#include "bacpcom.h"
#include "bacpexts.h"
#include "mpdefs.h"
#include "pppmp.h"
#include "pppbap.h"
#include "bapexts.h"
#include "baprm.h"
#include "rmexts.h"
#include "snmccons.h"
#include "llproto.h"
#include "bapprot.h"
#include "ppteslow.h"

#define    CALL_REQ_INDEX            1
#define    CALL_BACK_REQ_INDEX        2

#define    RM_NEEDED                1
#define    RM_NOT_NEEDED            2

#define    RM_ENABLE                1
#define    RM_DISABLE                2

#define    RM_LOCAL_END            1
#define    RM_REMOTE_END            2
#define    RM_NONE                    3

#define    RM_RETRY                2
#define    RM_NO_RETRY                1

#define    RM_ACCEPTABLE            1
#define    RM_NOT_ACCEPTABLE        2
#define    RM_BW_EXTREME            3

#define    RM_MIN_PH_NUM_SIZE        0

#define RM_MIN_CHAR_VALUE        0
#define RM_MAX_CHAR_VALUE        0xff
#define    RM_MIN_BYTE2_VALUE        0
#define    RM_MAX_BYTE2_VALUE        0xffff

#define RM_MIN_LINK_TYPE        1
#define RM_MAX_LINK_TYPE        5

INT1
CheckAndGetRMInfo (tPPPIf * pIf)
{
    if (pIf->BundleFlag == BUNDLE)
    {
        return (PPP_SNMP_OK);
    }
    return (PPP_SNMP_ERR);
}

tRMInfo            *
SNMPGetRMInfo (UINT4 Index)
{
    tPPPIf             *pIf;

    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if ((pIf != NULL) && (pIf->LinkInfo.IfIndex == Index)
            && (pIf->BundleFlag == BUNDLE))
        {
            return (&RMInfo);
        }
    }
    return (NULL);
}

tPPPIf             *
SNMPGetBundlePtr (UINT4 Index)
{
    tPPPIf             *pIf;

    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if ((pIf != NULL) && (pIf->LinkInfo.IfIndex == Index)
            && (pIf->BundleFlag == BUNDLE))
        {
            return (pIf);
        }
    }
    return (NULL);
}

tBAPInfo           *
SNMPGetBAPInfo (UINT4 Index)
{
    tPPPIf             *pIf;

    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if ((pIf != NULL) && (pIf->LinkInfo.IfIndex == Index)
            && (pIf->BundleFlag == BUNDLE)
            && (pIf->MPInfo.BundleInfo.BWMProtocolInfo.Protocol ==
                BAP_PROTOCOL))
        {
            return ((tBAPInfo *) pIf->MPInfo.BundleInfo.BWMProtocolInfo.
                    pProtocolInfo);
        }
    }
    return (NULL);
}

/*
    This function converts a tPhNum structure to a human readable phone
    number string.
*/
UINT1              *
RMGetPhNumString (tPhNum * pPhNum)
{
    BZERO (RMInfo.TempPhNumString, PPP_MAX_PH_NUM_SIZE);
    SPRINTF ((char *) RMInfo.TempPhNumString, "%c%c",
             pPhNum->Size, pPhNum->SubAddrSize);
    MEMCPY (RMInfo.TempPhNumString + 2, pPhNum->String, pPhNum->Size);
    return (RMInfo.TempPhNumString);
}

/*
    This function converts a human readable phone number string to
    tPhNum structure.
*/
VOID
RMGetPhNumFromString (tPhNum * pPhNum, UINT1 *pPhNumString)
{
    BZERO (pPhNum, sizeof (tPhNum));
    pPhNum->Size = (UINT1) (pPhNumString[0] - '0');
    pPhNum->SubAddrSize = (UINT1) (pPhNumString[1] - '0');
    MEMCPY (pPhNum->String, pPhNumString + 2, pPhNum->Size);
    return;
}

/* LOW LEVEL Routines for Table : PppTestRMConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppTestRMConfigTable
 Input       :  The Indices
                PppTestRMConfigIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppTestRMConfigTable (INT4 i4PppTestRMConfigIfIndex)
{
    if (SNMPGetRMInfo (i4PppTestRMConfigIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppTestRMConfigTable
 Input       :  The Indices
                PppTestRMConfigIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppTestRMConfigTable (INT4 *pi4PppTestRMConfigIfIndex)
{
    if (nmhGetFirstIndex (pi4PppTestRMConfigIfIndex, RMInstance, FIRST_INST) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppTestRMConfigTable
 Input       :  The Indices
                PppTestRMConfigIfIndex
                nextPppTestRMConfigIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppTestRMConfigTable (INT4 i4PppTestRMConfigIfIndex,
                                     INT4 *pi4NextPppTestRMConfigIfIndex)
{
    if (nmhGetNextIndex (&i4PppTestRMConfigIfIndex, RMInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextPppTestRMConfigIfIndex = i4PppTestRMConfigIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppTestRMConfigLocFirstLinkNumber
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                retValPppTestRMConfigLocFirstLinkNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestRMConfigLocFirstLinkNumber (INT4 i4PppTestRMConfigIfIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pRetValPppTestRMConfigLocFirstLinkNumber)
{
    tRMInfo            *pRMInfo;

    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    /* RFC007 *Str =  RMGetPhNumString (&pRMInfo->LocFirstLnkPhNum); */
    pRetValPppTestRMConfigLocFirstLinkNumber->i4_Length =
        pRMInfo->LocFirstLnkPhNum.Size + sizeof (tPhNum) - PPP_MAX_PH_NUM_SIZE;

    MEMCPY (pRetValPppTestRMConfigLocFirstLinkNumber->pu1_OctetList,
            RMGetPhNumString (&pRMInfo->LocFirstLnkPhNum),
            pRetValPppTestRMConfigLocFirstLinkNumber->i4_Length);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppTestRMConfigRemFirstLinkNumber
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                retValPppTestRMConfigRemFirstLinkNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestRMConfigRemFirstLinkNumber (INT4 i4PppTestRMConfigIfIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pRetValPppTestRMConfigRemFirstLinkNumber)
{
    tRMInfo            *pRMInfo;

    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    /* RFC007 *Str =  RMGetPhNumString (&pRMInfo->RemFirstLnkPhNum); */
    pRetValPppTestRMConfigRemFirstLinkNumber->i4_Length =
        pRMInfo->RemFirstLnkPhNum.Size + sizeof (tPhNum) - PPP_MAX_PH_NUM_SIZE;
    MEMCPY (pRetValPppTestRMConfigRemFirstLinkNumber->pu1_OctetList,
            RMGetPhNumString (&pRMInfo->RemFirstLnkPhNum),
            pRetValPppTestRMConfigRemFirstLinkNumber->i4_Length);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppTestRMConfigRequestType
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                retValPppTestRMConfigRequestType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestRMConfigRequestType (INT4 i4PppTestRMConfigIfIndex,
                                  INT4 *pi4RetValPppTestRMConfigRequestType)
{
    tRMInfo            *pRMInfo;

    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppTestRMConfigRequestType =
        (pRMInfo->ReqBlock.ReqType ==
         BAP_TX_CALL_BACK_REQUEST) ? CALL_BACK_REQ_INDEX : CALL_REQ_INDEX;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppTestRMConfigIsPhoneNumberNeeded
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                retValPppTestRMConfigIsPhoneNumberNeeded
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestRMConfigIsPhoneNumberNeeded (INT4 i4PppTestRMConfigIfIndex,
                                          INT4
                                          *pi4RetValPppTestRMConfigIsPhoneNumberNeeded)
{
    tRMInfo            *pRMInfo;

    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppTestRMConfigIsPhoneNumberNeeded =
        (pRMInfo->ReqBlock.Options.IsPhNumNeeded ==
         TRUE) ? RM_NEEDED : RM_NOT_NEEDED;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppTestRMConfigNumberOfPhoneNumbers
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                retValPppTestRMConfigNumberOfPhoneNumbers
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestRMConfigNumberOfPhoneNumbers (INT4 i4PppTestRMConfigIfIndex,
                                           INT4
                                           *pi4RetValPppTestRMConfigNumberOfPhoneNumbers)
{
    tRMInfo            *pRMInfo;

    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppTestRMConfigNumberOfPhoneNumbers =
        pRMInfo->ReqBlock.Options.pPhNumbers->NumPhoneNumbers;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPppTestRMConfigPhoneNumber
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                retValPppTestRMConfigPhoneNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestRMConfigPhoneNumber (INT4 i4PppTestRMConfigIfIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValPppTestRMConfigPhoneNumber)
{
    tRMInfo            *pRMInfo;

    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    /* Returns the Phone number indexed by PhNumCounter */
    if (pRMInfo->ReqBlock.Options.pPhNumbers->
        PhoneNumbers[pRMInfo->PhNumCounter].Size != 0)
    {
        /* RFC007 *Str =  RMGetPhNumString (&pRMInfo->ReqBlock.Options.pPhNumbers->PhoneNumbers[pRMInfo->PhNumCounter]); */
        pRetValPppTestRMConfigPhoneNumber->i4_Length =
            pRMInfo->ReqBlock.Options.pPhNumbers->PhoneNumbers[pRMInfo->
                                                               PhNumCounter].
            Size + sizeof (tPhNum) - PPP_MAX_PH_NUM_SIZE;
        MEMCPY (pRetValPppTestRMConfigPhoneNumber->pu1_OctetList,
                RMGetPhNumString (&pRMInfo->ReqBlock.Options.pPhNumbers->
                                  PhoneNumbers[pRMInfo->PhNumCounter]),
                pRetValPppTestRMConfigPhoneNumber->i4_Length);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppTestRMConfigLinkType
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                retValPppTestRMConfigLinkType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestRMConfigLinkType (INT4 i4PppTestRMConfigIfIndex,
                               INT4 *pi4RetValPppTestRMConfigLinkType)
{
    tRMInfo            *pRMInfo;

    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppTestRMConfigLinkType = pRMInfo->ReqBlock.Options.LinkType;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppTestRMConfigLinkSpeed
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                retValPppTestRMConfigLinkSpeed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestRMConfigLinkSpeed (INT4 i4PppTestRMConfigIfIndex,
                                INT4 *pi4RetValPppTestRMConfigLinkSpeed)
{
    tRMInfo            *pRMInfo;

    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppTestRMConfigLinkSpeed = pRMInfo->ReqBlock.Options.LinkSpeed;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppTestRMConfigLinkIndexToBeDeleted
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                retValPppTestRMConfigLinkIndexToBeDeleted
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestRMConfigLinkIndexToBeDeleted (INT4 i4PppTestRMConfigIfIndex,
                                           INT4
                                           *pi4RetValPppTestRMConfigLinkIndexToBeDeleted)
{
    tRMInfo            *pRMInfo;

    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pRMInfo->pIf != NULL)
    {
        *pi4RetValPppTestRMConfigLinkIndexToBeDeleted =
            pRMInfo->pIf->LinkInfo.IfIndex;
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPppTestRMConfigRequestId
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                retValPppTestRMConfigRequestId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestRMConfigRequestId (INT4 i4PppTestRMConfigIfIndex,
                                INT4 *pi4RetValPppTestRMConfigRequestId)
{
    tRMInfo            *pRMInfo;

    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppTestRMConfigRequestId = pRMInfo->ReqBlock.Id;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPppTestRMConfigRequester
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                retValPppTestRMConfigRequester
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestRMConfigRequester (INT4 i4PppTestRMConfigIfIndex,
                                INT4 *pi4RetValPppTestRMConfigRequester)
{
    tRMInfo            *pRMInfo;

    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppTestRMConfigRequester =
        (pRMInfo->Requester == BAP_LOCAL_RM) ? RM_LOCAL_END : RM_REMOTE_END;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppTestRMConfigAttemptedPhoneNumber
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                retValPppTestRMConfigAttemptedPhoneNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestRMConfigAttemptedPhoneNumber (INT4 i4PppTestRMConfigIfIndex,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pRetValPppTestRMConfigAttemptedPhoneNumber)
{
    tRMInfo            *pRMInfo;

    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    /* RFC007 *Str =  RMGetPhNumString (&pRMInfo->AttemptedPhNum); */
    pRetValPppTestRMConfigAttemptedPhoneNumber->i4_Length =
        pRMInfo->AttemptedPhNum.Size + sizeof (tPhNum) - PPP_MAX_PH_NUM_SIZE;
    MEMCPY (pRetValPppTestRMConfigAttemptedPhoneNumber->pu1_OctetList,
            RMGetPhNumString (&pRMInfo->AttemptedPhNum),
            pRetValPppTestRMConfigAttemptedPhoneNumber->i4_Length);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppTestRMConfigCallStatus
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                retValPppTestRMConfigCallStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestRMConfigCallStatus (INT4 i4PppTestRMConfigIfIndex,
                                 INT4 *pi4RetValPppTestRMConfigCallStatus)
{
    tRMInfo            *pRMInfo;

    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppTestRMConfigCallStatus = pRMInfo->ReqBlock.MsgCode;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppTestRMConfigActionTaken
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                retValPppTestRMConfigActionTaken
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestRMConfigActionTaken (INT4 i4PppTestRMConfigIfIndex,
                                  INT4 *pi4RetValPppTestRMConfigActionTaken)
{
    tRMInfo            *pRMInfo;

    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppTestRMConfigActionTaken =
        (pRMInfo->ReqBlock.ActionSent == BAP_RETRY) ? RM_RETRY : RM_NO_RETRY;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppTestRMConfigResponseToSend
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                retValPppTestRMConfigResponseToSend
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestRMConfigResponseToSend (INT4 i4PppTestRMConfigIfIndex,
                                     INT4
                                     *pi4RetValPppTestRMConfigResponseToSend)
{
    tRMInfo            *pRMInfo;

    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppTestRMConfigResponseToSend =
        (pRMInfo->ReqBlock.RespCode ==
         OK) ? RM_ACCEPTABLE : ((pRMInfo->ReqBlock.RespCode ==
                                 BAP_NOT_ACCEPTABLE) ? RM_NOT_ACCEPTABLE :
                                RM_BW_EXTREME);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppTestRMConfigAddLinkToBundle
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                retValPppTestRMConfigAddLinkToBundle
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestRMConfigAddLinkToBundle (INT4 i4PppTestRMConfigIfIndex,
                                      INT4
                                      *pi4RetValPppTestRMConfigAddLinkToBundle)
{
    tRMInfo            *pRMInfo;

    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppTestRMConfigAddLinkToBundle = RM_DISABLE;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppTestRMConfigDeleteLinkFromBundle
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                retValPppTestRMConfigDeleteLinkFromBundle
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestRMConfigDeleteLinkFromBundle (INT4 i4PppTestRMConfigIfIndex,
                                           INT4
                                           *pi4RetValPppTestRMConfigDeleteLinkFromBundle)
{
    tRMInfo            *pRMInfo;

    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppTestRMConfigDeleteLinkFromBundle = RM_DISABLE;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppTestRMConfigCallNotification
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                retValPppTestRMConfigCallNotification
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestRMConfigCallNotification (INT4 i4PppTestRMConfigIfIndex,
                                       INT4
                                       *pi4RetValPppTestRMConfigCallNotification)
{
    tRMInfo            *pRMInfo;

    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppTestRMConfigCallNotification = RM_DISABLE;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppTestRMConfigVerifyPeerRequest
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                retValPppTestRMConfigVerifyPeerRequest
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestRMConfigVerifyPeerRequest (INT4 i4PppTestRMConfigIfIndex,
                                        INT4
                                        *pi4RetValPppTestRMConfigVerifyPeerRequest)
{
    tRMInfo            *pRMInfo;

    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppTestRMConfigVerifyPeerRequest = RM_DISABLE;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPppTestRMConfigIsInitiator
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                retValPppTestRMConfigIsInitiator
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestRMConfigIsInitiator (INT4 i4PppTestRMConfigIfIndex,
                                  INT4 *pi4RetValPppTestRMConfigIsInitiator)
{
    tRMInfo            *pRMInfo;

    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppTestRMConfigIsInitiator = pRMInfo->IsInitiator;
    if (*pi4RetValPppTestRMConfigIsInitiator == BACP_NONE)
    {
        *pi4RetValPppTestRMConfigIsInitiator = RM_NONE;
    }
    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppTestRMConfigLocFirstLinkNumber
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                testValPppTestRMConfigLocFirstLinkNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestRMConfigLocFirstLinkNumber (UINT4 *pu4ErrorCode,
                                            INT4 i4PppTestRMConfigIfIndex,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pTestValPppTestRMConfigLocFirstLinkNumber)
{

    if ((SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pTestValPppTestRMConfigLocFirstLinkNumber->i4_Length <
        RM_MIN_PH_NUM_SIZE
        || pTestValPppTestRMConfigLocFirstLinkNumber->i4_Length >
        PPP_MAX_PH_NUM_SIZE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppTestRMConfigRemFirstLinkNumber
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                testValPppTestRMConfigRemFirstLinkNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestRMConfigRemFirstLinkNumber (UINT4 *pu4ErrorCode,
                                            INT4 i4PppTestRMConfigIfIndex,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pTestValPppTestRMConfigRemFirstLinkNumber)
{
    if ((SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pTestValPppTestRMConfigRemFirstLinkNumber->i4_Length <
        RM_MIN_PH_NUM_SIZE
        || pTestValPppTestRMConfigRemFirstLinkNumber->i4_Length >
        PPP_MAX_PH_NUM_SIZE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppTestRMConfigRequestType
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                testValPppTestRMConfigRequestType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestRMConfigRequestType (UINT4 *pu4ErrorCode,
                                     INT4 i4PppTestRMConfigIfIndex,
                                     INT4 i4TestValPppTestRMConfigRequestType)
{
    if ((SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppTestRMConfigRequestType != CALL_REQ_INDEX
        && i4TestValPppTestRMConfigRequestType != CALL_BACK_REQ_INDEX)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppTestRMConfigIsPhoneNumberNeeded
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                testValPppTestRMConfigIsPhoneNumberNeeded
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestRMConfigIsPhoneNumberNeeded (UINT4 *pu4ErrorCode,
                                             INT4 i4PppTestRMConfigIfIndex,
                                             INT4
                                             i4TestValPppTestRMConfigIsPhoneNumberNeeded)
{
    if ((SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppTestRMConfigIsPhoneNumberNeeded != RM_NEEDED
        && i4TestValPppTestRMConfigIsPhoneNumberNeeded != RM_NOT_NEEDED)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppTestRMConfigNumberOfPhoneNumbers
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                testValPppTestRMConfigNumberOfPhoneNumbers
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestRMConfigNumberOfPhoneNumbers (UINT4 *pu4ErrorCode,
                                              INT4 i4PppTestRMConfigIfIndex,
                                              INT4
                                              i4TestValPppTestRMConfigNumberOfPhoneNumbers)
{
    if ((SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppTestRMConfigNumberOfPhoneNumbers < RM_MIN_CHAR_VALUE
        || i4TestValPppTestRMConfigNumberOfPhoneNumbers > RM_MAX_PH_NUMBERS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppTestRMConfigPhoneNumber
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                testValPppTestRMConfigPhoneNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestRMConfigPhoneNumber (UINT4 *pu4ErrorCode,
                                     INT4 i4PppTestRMConfigIfIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pTestValPppTestRMConfigPhoneNumber)
{
    if ((SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pTestValPppTestRMConfigPhoneNumber->i4_Length < RM_MIN_PH_NUM_SIZE
        || pTestValPppTestRMConfigPhoneNumber->i4_Length > PPP_MAX_PH_NUM_SIZE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppTestRMConfigLinkType
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                testValPppTestRMConfigLinkType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestRMConfigLinkType (UINT4 *pu4ErrorCode,
                                  INT4 i4PppTestRMConfigIfIndex,
                                  INT4 i4TestValPppTestRMConfigLinkType)
{
    if ((SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppTestRMConfigLinkType < RM_MIN_LINK_TYPE
        || i4TestValPppTestRMConfigLinkType > RM_MAX_LINK_TYPE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppTestRMConfigLinkSpeed
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                testValPppTestRMConfigLinkSpeed
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestRMConfigLinkSpeed (UINT4 *pu4ErrorCode,
                                   INT4 i4PppTestRMConfigIfIndex,
                                   INT4 i4TestValPppTestRMConfigLinkSpeed)
{
    if ((SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppTestRMConfigLinkSpeed < RM_MIN_BYTE2_VALUE
        || i4TestValPppTestRMConfigLinkSpeed > RM_MAX_BYTE2_VALUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppTestRMConfigLinkIndexToBeDeleted
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                testValPppTestRMConfigLinkIndexToBeDeleted
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestRMConfigLinkIndexToBeDeleted (UINT4 *pu4ErrorCode,
                                              INT4 i4PppTestRMConfigIfIndex,
                                              INT4
                                              i4TestValPppTestRMConfigLinkIndexToBeDeleted)
{
    if ((SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppTestRMConfigLinkIndexToBeDeleted < RM_MIN_BYTE2_VALUE
        || i4TestValPppTestRMConfigLinkIndexToBeDeleted > RM_MAX_BYTE2_VALUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppTestRMConfigRequestId
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                testValPppTestRMConfigRequestId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestRMConfigRequestId (UINT4 *pu4ErrorCode,
                                   INT4 i4PppTestRMConfigIfIndex,
                                   INT4 i4TestValPppTestRMConfigRequestId)
{
    if ((SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppTestRMConfigRequestId < RM_MIN_CHAR_VALUE
        || i4TestValPppTestRMConfigRequestId > RM_MAX_CHAR_VALUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppTestRMConfigRequester
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                testValPppTestRMConfigRequester
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestRMConfigRequester (UINT4 *pu4ErrorCode,
                                   INT4 i4PppTestRMConfigIfIndex,
                                   INT4 i4TestValPppTestRMConfigRequester)
{
    if ((SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppTestRMConfigRequester != RM_LOCAL_END
        && i4TestValPppTestRMConfigRequester != RM_REMOTE_END)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppTestRMConfigAttemptedPhoneNumber
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                testValPppTestRMConfigAttemptedPhoneNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestRMConfigAttemptedPhoneNumber (UINT4 *pu4ErrorCode,
                                              INT4 i4PppTestRMConfigIfIndex,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pTestValPppTestRMConfigAttemptedPhoneNumber)
{
    if ((SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pTestValPppTestRMConfigAttemptedPhoneNumber->i4_Length <
        RM_MIN_PH_NUM_SIZE
        || pTestValPppTestRMConfigAttemptedPhoneNumber->i4_Length >
        PPP_MAX_PH_NUM_SIZE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppTestRMConfigCallStatus
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                testValPppTestRMConfigCallStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestRMConfigCallStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4PppTestRMConfigIfIndex,
                                    INT4 i4TestValPppTestRMConfigCallStatus)
{
    if ((SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppTestRMConfigCallStatus < RM_MIN_CHAR_VALUE
        || i4TestValPppTestRMConfigCallStatus > RM_MAX_CHAR_VALUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppTestRMConfigActionTaken
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                testValPppTestRMConfigActionTaken
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestRMConfigActionTaken (UINT4 *pu4ErrorCode,
                                     INT4 i4PppTestRMConfigIfIndex,
                                     INT4 i4TestValPppTestRMConfigActionTaken)
{
    if ((SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppTestRMConfigActionTaken != RM_RETRY
        && i4TestValPppTestRMConfigActionTaken != RM_NO_RETRY)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppTestRMConfigResponseToSend
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                testValPppTestRMConfigResponseToSend
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestRMConfigResponseToSend (UINT4 *pu4ErrorCode,
                                        INT4 i4PppTestRMConfigIfIndex,
                                        INT4
                                        i4TestValPppTestRMConfigResponseToSend)
{
    if ((SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppTestRMConfigResponseToSend != RM_ACCEPTABLE
        && i4TestValPppTestRMConfigResponseToSend != RM_NOT_ACCEPTABLE
        && i4TestValPppTestRMConfigResponseToSend != RM_BW_EXTREME)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppTestRMConfigAddLinkToBundle
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                testValPppTestRMConfigAddLinkToBundle
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestRMConfigAddLinkToBundle (UINT4 *pu4ErrorCode,
                                         INT4 i4PppTestRMConfigIfIndex,
                                         INT4
                                         i4TestValPppTestRMConfigAddLinkToBundle)
{
    if ((SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppTestRMConfigAddLinkToBundle != RM_ENABLE
        && i4TestValPppTestRMConfigAddLinkToBundle != RM_DISABLE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppTestRMConfigDeleteLinkFromBundle
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                testValPppTestRMConfigDeleteLinkFromBundle
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestRMConfigDeleteLinkFromBundle (UINT4 *pu4ErrorCode,
                                              INT4 i4PppTestRMConfigIfIndex,
                                              INT4
                                              i4TestValPppTestRMConfigDeleteLinkFromBundle)
{
    if ((SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppTestRMConfigDeleteLinkFromBundle != RM_ENABLE
        && i4TestValPppTestRMConfigDeleteLinkFromBundle != RM_DISABLE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppTestRMConfigCallNotification
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                testValPppTestRMConfigCallNotification
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestRMConfigCallNotification (UINT4 *pu4ErrorCode,
                                          INT4 i4PppTestRMConfigIfIndex,
                                          INT4
                                          i4TestValPppTestRMConfigCallNotification)
{
    if ((SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppTestRMConfigCallNotification != RM_ENABLE
        && i4TestValPppTestRMConfigCallNotification != RM_DISABLE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppTestRMConfigVerifyPeerRequest
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                testValPppTestRMConfigVerifyPeerRequest
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestRMConfigVerifyPeerRequest (UINT4 *pu4ErrorCode,
                                           INT4 i4PppTestRMConfigIfIndex,
                                           INT4
                                           i4TestValPppTestRMConfigVerifyPeerRequest)
{
    if ((SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppTestRMConfigVerifyPeerRequest != RM_ENABLE
        && i4TestValPppTestRMConfigVerifyPeerRequest != RM_DISABLE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppTestRMConfigIsInitiator
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                testValPppTestRMConfigIsInitiator
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestRMConfigIsInitiator (UINT4 *pu4ErrorCode,
                                     INT4 i4PppTestRMConfigIfIndex,
                                     INT4 i4TestValPppTestRMConfigIsInitiator)
{
    if ((SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {

        return (SNMP_FAILURE);
    }

    if (i4TestValPppTestRMConfigIsInitiator != LOCAL_END
        && i4TestValPppTestRMConfigIsInitiator != REMOTE_END
        && i4TestValPppTestRMConfigIsInitiator != RM_NONE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppTestRMConfigLocFirstLinkNumber
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                setValPppTestRMConfigLocFirstLinkNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestRMConfigLocFirstLinkNumber (INT4 i4PppTestRMConfigIfIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pSetValPppTestRMConfigLocFirstLinkNumber)
{
    tRMInfo            *pRMInfo;
    PPP_UNUSED (pSetValPppTestRMConfigLocFirstLinkNumber->i4_Length);
    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    RMGetPhNumFromString (&pRMInfo->LocFirstLnkPhNum,
                          pSetValPppTestRMConfigLocFirstLinkNumber->
                          pu1_OctetList);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetPppTestRMConfigRemFirstLinkNumber
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                setValPppTestRMConfigRemFirstLinkNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestRMConfigRemFirstLinkNumber (INT4 i4PppTestRMConfigIfIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pSetValPppTestRMConfigRemFirstLinkNumber)
{
    tRMInfo            *pRMInfo;
    PPP_UNUSED (pSetValPppTestRMConfigRemFirstLinkNumber->i4_Length);
    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    RMGetPhNumFromString (&pRMInfo->RemFirstLnkPhNum,
                          pSetValPppTestRMConfigRemFirstLinkNumber->
                          pu1_OctetList);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppTestRMConfigRequestType
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                setValPppTestRMConfigRequestType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestRMConfigRequestType (INT4 i4PppTestRMConfigIfIndex,
                                  INT4 i4SetValPppTestRMConfigRequestType)
{
    tRMInfo            *pRMInfo;

    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pRMInfo->ReqBlock.ReqType =
        (i4SetValPppTestRMConfigRequestType ==
         CALL_BACK_REQ_INDEX) ? BAP_TX_CALL_BACK_REQUEST : BAP_TX_CALL_REQUEST;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppTestRMConfigIsPhoneNumberNeeded
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                setValPppTestRMConfigIsPhoneNumberNeeded
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestRMConfigIsPhoneNumberNeeded (INT4 i4PppTestRMConfigIfIndex,
                                          INT4
                                          i4SetValPppTestRMConfigIsPhoneNumberNeeded)
{
    tRMInfo            *pRMInfo;

    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pRMInfo->ReqBlock.Options.IsPhNumNeeded =
        (i4SetValPppTestRMConfigIsPhoneNumberNeeded ==
         RM_NEEDED) ? TRUE : FALSE;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppTestRMConfigNumberOfPhoneNumbers
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                setValPppTestRMConfigNumberOfPhoneNumbers
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestRMConfigNumberOfPhoneNumbers (INT4 i4PppTestRMConfigIfIndex,
                                           INT4
                                           i4SetValPppTestRMConfigNumberOfPhoneNumbers)
{
    tRMInfo            *pRMInfo;

    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pRMInfo->ReqBlock.Options.pPhNumbers->NumPhoneNumbers =
        (UINT1) i4SetValPppTestRMConfigNumberOfPhoneNumbers;
    pRMInfo->PhNumCounter = 0;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppTestRMConfigPhoneNumber
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                setValPppTestRMConfigPhoneNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestRMConfigPhoneNumber (INT4 i4PppTestRMConfigIfIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pSetValPppTestRMConfigPhoneNumber)
{
    tRMInfo            *pRMInfo;
    PPP_UNUSED (pSetValPppTestRMConfigPhoneNumber->i4_Length);
    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pRMInfo->PhNumCounter <
        pRMInfo->ReqBlock.Options.pPhNumbers->NumPhoneNumbers)
    {
        RMGetPhNumFromString (&pRMInfo->ReqBlock.Options.pPhNumbers->
                              PhoneNumbers[pRMInfo->PhNumCounter],
                              pSetValPppTestRMConfigPhoneNumber->pu1_OctetList);
        pRMInfo->PhNumCounter++;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);

}

/****************************************************************************
 Function    :  nmhSetPppTestRMConfigLinkType
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                setValPppTestRMConfigLinkType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestRMConfigLinkType (INT4 i4PppTestRMConfigIfIndex,
                               INT4 i4SetValPppTestRMConfigLinkType)
{
    tRMInfo            *pRMInfo;

    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pRMInfo->ReqBlock.Options.LinkType =
        (UINT1) (1 << (i4SetValPppTestRMConfigLinkType - 1));
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppTestRMConfigLinkSpeed
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                setValPppTestRMConfigLinkSpeed
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestRMConfigLinkSpeed (INT4 i4PppTestRMConfigIfIndex,
                                INT4 i4SetValPppTestRMConfigLinkSpeed)
{
    tRMInfo            *pRMInfo;

    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pRMInfo->ReqBlock.Options.LinkSpeed =
        (UINT2) i4SetValPppTestRMConfigLinkSpeed;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppTestRMConfigLinkIndexToBeDeleted
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                setValPppTestRMConfigLinkIndexToBeDeleted
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestRMConfigLinkIndexToBeDeleted (INT4 i4PppTestRMConfigIfIndex,
                                           INT4
                                           i4SetValPppTestRMConfigLinkIndexToBeDeleted)
{
    tBAPInfo           *pBAPInfo;
    tPPPIf             *pIf;

    if ((pBAPInfo = SNMPGetBAPInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    SLL_SCAN_OFFSET (&pBAPInfo->pBundleIf->MPInfo.BundleInfo.MemberList, pIf,
                     tPPPIf *, FIND_OFFSET (tPPPIf, MPInfo.MemberInfo.NextMPIf))
    {
        if ((pIf != NULL)
            && (pIf->LinkInfo.IfIndex ==
                (UINT4) i4SetValPppTestRMConfigLinkIndexToBeDeleted))
        {
            RMInfo.pIf = pIf;
            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);

}

/****************************************************************************
 Function    :  nmhSetPppTestRMConfigRequestId
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                setValPppTestRMConfigRequestId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestRMConfigRequestId (INT4 i4PppTestRMConfigIfIndex,
                                INT4 i4SetValPppTestRMConfigRequestId)
{
    tRMInfo            *pRMInfo;

    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pRMInfo->ReqBlock.Id = (UINT1) i4SetValPppTestRMConfigRequestId;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppTestRMConfigRequester
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                setValPppTestRMConfigRequester
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestRMConfigRequester (INT4 i4PppTestRMConfigIfIndex,
                                INT4 i4SetValPppTestRMConfigRequester)
{
    tRMInfo            *pRMInfo;

    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pRMInfo->Requester =
        (i4SetValPppTestRMConfigRequester ==
         RM_LOCAL_END) ? BAP_LOCAL_RM : BAP_REMOTE_PEER;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppTestRMConfigAttemptedPhoneNumber
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                setValPppTestRMConfigAttemptedPhoneNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestRMConfigAttemptedPhoneNumber (INT4 i4PppTestRMConfigIfIndex,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pSetValPppTestRMConfigAttemptedPhoneNumber)
{
    tRMInfo            *pRMInfo;
    PPP_UNUSED (pSetValPppTestRMConfigAttemptedPhoneNumber->i4_Length);
    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    RMGetPhNumFromString (&pRMInfo->AttemptedPhNum,
                          pSetValPppTestRMConfigAttemptedPhoneNumber->
                          pu1_OctetList);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppTestRMConfigCallStatus
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                setValPppTestRMConfigCallStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestRMConfigCallStatus (INT4 i4PppTestRMConfigIfIndex,
                                 INT4 i4SetValPppTestRMConfigCallStatus)
{
    tRMInfo            *pRMInfo;

    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pRMInfo->ReqBlock.MsgCode = (UINT1) i4SetValPppTestRMConfigCallStatus;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppTestRMConfigActionTaken
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                setValPppTestRMConfigActionTaken
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestRMConfigActionTaken (INT4 i4PppTestRMConfigIfIndex,
                                  INT4 i4SetValPppTestRMConfigActionTaken)
{
    tRMInfo            *pRMInfo;

    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pRMInfo->ReqBlock.ActionSent =
        (i4SetValPppTestRMConfigActionTaken ==
         RM_RETRY) ? BAP_RETRY : BAP_NO_RETRY;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppTestRMConfigResponseToSend
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                setValPppTestRMConfigResponseToSend
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestRMConfigResponseToSend (INT4 i4PppTestRMConfigIfIndex,
                                     INT4 i4SetValPppTestRMConfigResponseToSend)
{
    tRMInfo            *pRMInfo;

    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pRMInfo->ReqBlock.RespCode =
        (i4SetValPppTestRMConfigResponseToSend ==
         RM_ACCEPTABLE) ? OK : ((i4SetValPppTestRMConfigResponseToSend ==
                                 RM_NOT_ACCEPTABLE) ? BAP_NOT_ACCEPTABLE :
                                BAP_BW_EXTREME);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppTestRMConfigAddLinkToBundle
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                setValPppTestRMConfigAddLinkToBundle
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestRMConfigAddLinkToBundle (INT4 i4PppTestRMConfigIfIndex,
                                      INT4
                                      i4SetValPppTestRMConfigAddLinkToBundle)
{
    tBAPInfo           *pBAPInfo;
    tPhNumbers         *pTempPhNumbers;
    tReqHandle          ReqHandle;

    if ((pBAPInfo = SNMPGetBAPInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (i4SetValPppTestRMConfigAddLinkToBundle == RM_ENABLE)
    {
        pTempPhNumbers = RMInfo.ReqBlock.Options.pPhNumbers;
        RMInfo.ReqBlock.Options.pPhNumbers =
            ((RMInfo.ReqBlock.ReqType == BAP_TX_CALL_REQUEST)
             && (RMInfo.ReqBlock.Options.IsPhNumNeeded ==
                 FALSE)) ? NULL : pTempPhNumbers;

        if (PPPBAPAddLinkToBundle
            ((UINT2) (pBAPInfo->pBundleIf->LinkInfo.IfIndex), &ReqHandle,
             RMInfo.ReqBlock.ReqType, &RMInfo.ReqBlock.Options) == OK)
        {
            RMInfo.ReqBlock.Id = ReqHandle.Id;
            RMInfo.Requester = ReqHandle.Requester;
            RMCreateInitAndAddReqEntry (pBAPInfo->pBundleIf, ReqHandle.Id,
                                        BAP_LOCAL_RM, RMInfo.ReqBlock.ReqType,
                                        NULL);
        }
        RMInfo.ReqBlock.Options.pPhNumbers = pTempPhNumbers;
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppTestRMConfigDeleteLinkFromBundle
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                setValPppTestRMConfigDeleteLinkFromBundle
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestRMConfigDeleteLinkFromBundle (INT4 i4PppTestRMConfigIfIndex,
                                           INT4
                                           i4SetValPppTestRMConfigDeleteLinkFromBundle)
{
    tBAPInfo           *pBAPInfo;
    tReqHandle          ReqHandle;

    if ((pBAPInfo = SNMPGetBAPInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (i4SetValPppTestRMConfigDeleteLinkFromBundle == RM_ENABLE)
    {
        if (PPPBAPDeleteLinkFromBundle
            ((UINT2) (pBAPInfo->pBundleIf->LinkInfo.IfIndex),
             (UINT2) (RMInfo.pIf->LinkInfo.IfIndex), &ReqHandle) != NOT_OK)
        {
            RMInfo.ReqBlock.Id = ReqHandle.Id;
            RMInfo.Requester = ReqHandle.Requester;
            RMCreateInitAndAddReqEntry (pBAPInfo->pBundleIf, ReqHandle.Id,
                                        BAP_LOCAL_RM, BAP_TX_LINK_DROP_REQUEST,
                                        RMInfo.pIf);
        }
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppTestRMConfigCallNotification
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                setValPppTestRMConfigCallNotification
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestRMConfigCallNotification (INT4 i4PppTestRMConfigIfIndex,
                                       INT4
                                       i4SetValPppTestRMConfigCallNotification)
{
    tBAPInfo           *pBAPInfo;
    tReqHandle          ReqHandle;
    if ((pBAPInfo = SNMPGetBAPInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (i4SetValPppTestRMConfigCallNotification == RM_ENABLE)
    {
        ReqHandle.Id = RMInfo.ReqBlock.Id;
        ReqHandle.Requester = RMInfo.Requester;
        PPPBAPCallNotificationFromRM ((UINT2)
                                      (pBAPInfo->pBundleIf->LinkInfo.IfIndex),
                                      &ReqHandle, RMInfo.AttemptedPhNum,
                                      RMInfo.ReqBlock.MsgCode,
                                      RMInfo.ReqBlock.ActionSent);
        BZERO (&RMInfo.AttemptedPhNum, sizeof (tPhNum));
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppTestRMConfigVerifyPeerRequest
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                setValPppTestRMConfigVerifyPeerRequest
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestRMConfigVerifyPeerRequest (INT4 i4PppTestRMConfigIfIndex,
                                        INT4
                                        i4SetValPppTestRMConfigVerifyPeerRequest)
{
    tBAPInfo           *pBAPInfo;
    PPP_UNUSED (i4SetValPppTestRMConfigVerifyPeerRequest);
    if ((pBAPInfo = SNMPGetBAPInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppTestRMConfigIsInitiator
 Input       :  The Indices
                PppTestRMConfigIfIndex

                The Object 
                setValPppTestRMConfigIsInitiator
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestRMConfigIsInitiator (INT4 i4PppTestRMConfigIfIndex,
                                  INT4 i4SetValPppTestRMConfigIsInitiator)
{
    tRMInfo            *pRMInfo;

    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pRMInfo->IsInitiator =
        (UINT1) ((i4SetValPppTestRMConfigIsInitiator ==
                  RM_NONE) ? BACP_NONE : i4SetValPppTestRMConfigIsInitiator);
    return (SNMP_SUCCESS);

}

/* LOW LEVEL Routines for Table : PppTestRMStatusTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppTestRMStatusTable
 Input       :  The Indices
                PppTestRMStatusMPIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppTestRMStatusTable (INT4 i4PppTestRMStatusMPIfIndex)
{
    if (SNMPGetBAPInfo (i4PppTestRMStatusMPIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppTestRMStatusTable
 Input       :  The Indices
                PppTestRMStatusMPIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppTestRMStatusTable (INT4 *pi4PppTestRMStatusMPIfIndex)
{
    if (nmhGetFirstIndex (pi4PppTestRMStatusMPIfIndex, RMInstance, FIRST_INST)
        == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppTestRMStatusTable
 Input       :  The Indices
                PppTestRMStatusMPIfIndex
                nextPppTestRMStatusMPIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppTestRMStatusTable (INT4 i4PppTestRMStatusMPIfIndex,
                                     INT4 *pi4NextPppTestRMStatusMPIfIndex)
{
    if (nmhGetNextIndex (&i4PppTestRMStatusMPIfIndex, RMInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextPppTestRMStatusMPIfIndex = i4PppTestRMStatusMPIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppTestRMStatusRequestId
 Input       :  The Indices
                PppTestRMStatusMPIfIndex

                The Object 
                retValPppTestRMStatusRequestId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestRMStatusRequestId (INT4 i4PppTestRMStatusMPIfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValPppTestRMStatusRequestId)
{
    tBAPInfo           *pBAPInfo;

    if ((pBAPInfo = SNMPGetBAPInfo (i4PppTestRMStatusMPIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pRetValPppTestRMStatusRequestId->i4_Length = pBAPInfo->RequestId - 1;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppTestRMStatusIfIndex
 Input       :  The Indices
                PppTestRMStatusMPIfIndex

                The Object 
                retValPppTestRMStatusIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestRMStatusIfIndex (INT4 i4PppTestRMStatusMPIfIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValPppTestRMStatusIfIndex)
{
    tRMInfo            *pRMInfo;

    if ((pRMInfo = SNMPGetRMInfo (i4PppTestRMStatusMPIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pRetValPppTestRMStatusIfIndex->i4_Length =
        STRLEN ((const char *) pRMInfo->pIf->LinkInfo.IfIndex);
    MEMCPY (pRetValPppTestRMStatusIfIndex->pu1_OctetList,
            (const char *) pRMInfo->pIf->LinkInfo.IfIndex,
            pRetValPppTestRMStatusIfIndex->i4_Length);
    return (SNMP_SUCCESS);

}
