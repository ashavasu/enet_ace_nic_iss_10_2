/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppbacpl.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description: Low level routines for bacp module.
 *
 *******************************************************************/
#include "pppsnmpm.h"
#include "bacpcom.h"
#include "mpdefs.h"
#include "pppmp.h"
#include "pppbap.h"
#include "bapexts.h"
#include "snmccons.h"
#include "llproto.h"
#include "ppbaclow.h"

INT1
CheckAndGetBACPPtr (tPPPIf * pIf)
{
#ifndef NCP_OVER_BUNDLE
    tPPPIf             *pBundleIf;

    pBundleIf =
        (pIf->BundleFlag == BUNDLE) ? pIf : pIf->MPInfo.MemberInfo.pBundlePtr;
    if ((pBundleIf != NULL)
        && (pBundleIf->MPInfo.BundleInfo.BWMProtocolInfo.Protocol ==
            BAP_PROTOCOL))
    {
#else
    if ((pIf->BundleFlag == BUNDLE)
        && (pIf->MPInfo.BundleInfo.BWMProtocolInfo.Protocol == BAP_PROTOCOL))
    {
#endif
        return (PPP_SNMP_OK);
    }
    return (PPP_SNMP_ERR);
}

tBACPIf            *
PppGetBACPIfPtr (UINT4 Index)
{
    tPPPIf             *pIf;
    tPPPIf             *pBundleIf;

    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if ((pIf != NULL) && (pIf->LinkInfo.IfIndex == Index))
        {
            pBundleIf =
                (pIf->BundleFlag ==
                 BUNDLE) ? pIf : pIf->MPInfo.MemberInfo.pBundlePtr;
            if ((pBundleIf != NULL)
                && (pBundleIf->MPInfo.BundleInfo.BWMProtocolInfo.Protocol ==
                    BAP_PROTOCOL))
            {
                return (&
                        (((tBAPInfo
                           *) pBundleIf->MPInfo.BundleInfo.BWMProtocolInfo.
                          pProtocolInfo)->BACPIf));
            }
        }
    }
    return (NULL);
}

/* LOW LEVEL Routines for Table : PppBACPTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppBACPTable
 Input       :  The Indices
                PppBACPIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppBACPTable (INT4 i4PppBACPIfIndex)
{
    if (PppGetBACPIfPtr (i4PppBACPIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppBACPTable
 Input       :  The Indices
                PppBACPIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppBACPTable (INT4 *pi4PppBACPIfIndex)
{
    if (nmhGetFirstIndex (pi4PppBACPIfIndex, BACPInstance, FIRST_INST) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppBACPTable
 Input       :  The Indices
                PppBACPIfIndex
                nextPppBACPIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppBACPTable (INT4 i4PppBACPIfIndex, INT4 *pi4NextPppBACPIfIndex)
{
    if (nmhGetNextIndex (&i4PppBACPIfIndex, BACPInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextPppBACPIfIndex = i4PppBACPIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppBACPOperStatus
 Input       :  The Indices
                PppBACPIfIndex

                The Object 
                retValPppBACPOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppBACPOperStatus (INT4 i4PppBACPIfIndex,
                         INT4 *pi4RetValPppBACPOperStatus)
{
    tBACPIf            *pBACPIf;

    if ((pBACPIf = PppGetBACPIfPtr (i4PppBACPIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppBACPOperStatus = pBACPIf->OperStatus;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppBACPFavoredEnd
 Input       :  The Indices
                PppBACPIfIndex

                The Object 
                retValPppBACPFavoredEnd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppBACPFavoredEnd (INT4 i4PppBACPIfIndex,
                         INT4 *pi4RetValPppBACPFavoredEnd)
{
    tBACPIf            *pBACPIf;

    if (((pBACPIf = PppGetBACPIfPtr (i4PppBACPIfIndex)) == NULL)
        && (pBACPIf->OperStatus == STATUS_UP))
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppBACPFavoredEnd = pBACPIf->FavoredEnd;
    return (SNMP_SUCCESS);

}

/* LOW LEVEL Routines for Table : PppBACPConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppBACPConfigTable
 Input       :  The Indices
                PppBACPConfigIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppBACPConfigTable (INT4 i4PppBACPConfigIfIndex)
{
    if (PppGetBACPIfPtr (i4PppBACPConfigIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppBACPConfigTable
 Input       :  The Indices
                PppBACPConfigIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppBACPConfigTable (INT4 *pi4PppBACPConfigIfIndex)
{
    if (nmhGetFirstIndex (pi4PppBACPConfigIfIndex, BACPInstance, FIRST_INST) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppBACPConfigTable
 Input       :  The Indices
                PppBACPConfigIfIndex
                nextPppBACPConfigIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppBACPConfigTable (INT4 i4PppBACPConfigIfIndex,
                                   INT4 *pi4NextPppBACPConfigIfIndex)
{
    if (nmhGetNextIndex (&i4PppBACPConfigIfIndex, BACPInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextPppBACPConfigIfIndex = i4PppBACPConfigIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppBACPConfigAdminStatus
 Input       :  The Indices
                PppBACPConfigIfIndex

                The Object 
                retValPppBACPConfigAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppBACPConfigAdminStatus (INT4 i4PppBACPConfigIfIndex,
                                INT4 *pi4RetValPppBACPConfigAdminStatus)
{
    tBACPIf            *pBACPIf;

    if ((pBACPIf = PppGetBACPIfPtr (i4PppBACPConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppBACPConfigAdminStatus = pBACPIf->AdminStatus;
    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppBACPConfigAdminStatus
 Input       :  The Indices
                PppBACPConfigIfIndex

                The Object 
                setValPppBACPConfigAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppBACPConfigAdminStatus (INT4 i4PppBACPConfigIfIndex,
                                INT4 i4SetValPppBACPConfigAdminStatus)
{
    tBACPIf            *pBACPIf;

    if ((pBACPIf = PppGetBACPIfPtr (i4PppBACPConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    switch (i4SetValPppBACPConfigAdminStatus)
    {
        case ADMIN_OPEN:
            pBACPIf->AdminStatus = (UINT1) i4SetValPppBACPConfigAdminStatus;
            BACPEnableIf (pBACPIf);
            break;

        case ADMIN_CLOSE:
            pBACPIf->AdminStatus = (UINT1) i4SetValPppBACPConfigAdminStatus;
            BACPDisableIf (pBACPIf);
            break;
    }
    return (SNMP_SUCCESS);

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppBACPConfigAdminStatus
 Input       :  The Indices
                PppBACPConfigIfIndex

                The Object 
                testValPppBACPConfigAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppBACPConfigAdminStatus (UINT4 *pu4ErrorCode,
                                   INT4 i4PppBACPConfigIfIndex,
                                   INT4 i4TestValPppBACPConfigAdminStatus)
{
    if ((PppGetBACPIfPtr (i4PppBACPConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((i4TestValPppBACPConfigAdminStatus != ADMIN_OPEN)
        && (i4TestValPppBACPConfigAdminStatus != ADMIN_CLOSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}
