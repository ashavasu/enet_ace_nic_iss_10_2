/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bacpexts.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains definitions and function prototypes for
 * external modules
 *
 *******************************************************************/
#ifndef __PPP_BACPEXTS_H__
#define __PPP_BACPEXTS_H__


extern tBACPCounters		BACPCounters;
extern tGenOptionInfo		BACPGenOptionInfo[];
extern tOptNegFlagsPerIf	BACPOptPerIntf[];
extern tGSEMCallbacks		BACPGSEMCallback;


#endif  /* __PPP_BACPEXTS_H__ */
