/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bacpcom.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description: This file contains PPP common include files  for BACP 
 * module
 *
 *******************************************************************/
#ifndef __PPP_BACPCOM_H__
#define __PPP_BACPCOM_H__

#include "genhdrs.h"

/* Global PPP includes */
#include "pppmacro.h"
#include "pppdefs.h"
#include "pppdebug.h"
#include "ppptimer.h"
#include "pppdbgex.h"


/* Includes from other modules */
#include "frmtdfs.h"

#include "pppgsem.h"
#include "pppgcp.h"

#include "ppplcp.h"
#include "genexts.h"
#include "pppexts.h"

#include "pppbacp.h"
#include "bacpexts.h"

#include "pppmp.h"
#include "pppbap.h"
#include "baprm.h"
#include "bacpprot.h"

#endif  /* __PPP_BACPCOM_H__ */
