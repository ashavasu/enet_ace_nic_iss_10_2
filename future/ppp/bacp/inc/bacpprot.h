/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bacpprot.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the prototypes for BACP functions used
 * internally by the BACP module
 *
 *******************************************************************/
#ifndef __PPP_BACPPROT_H__
#define __PPP_BACPPROT_H__


VOID BACPEnableIf(tBACPIf *pBACPIf);
VOID BACPDisableIf(tBACPIf *pBACPIf);
INT1 BACPInit(tPPPIf *pBundleIf);
VOID BACPOptionInit(tBACPOptions *pBACPOpt);

VOID BACPUp(tGSEM *pBACPGSEM);
VOID BACPDown(tGSEM *pBACPGSEM);

INT1 BACPProcessFavoredPeerConfReq ( tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal, UINT2 Offset, UINT1 PresenceFlag);
INT1 BACPProcessFavoredPeerConfNak ( tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal);
VOID *BACPReturnFavoredPeerPtr (tGSEM  *pGSEM, UINT1 *OptLen);

INT1 BACPCopyOptions ( tGSEM *pGSEM, UINT1 Flag, UINT1 PktType );
tGSEM *BACPGetSEMPtr ( tPPPIf *pIf );

/* Related to SNMP */
INT1 CheckAndGetBACPPtr (tPPPIf* pIf);
tBACPIf *PppGetBACPIfPtr (UINT4 Index);

INT1 CheckAndGetRMInfo (tPPPIf* pIf);
tRMInfo *SNMPGetRMInfo (UINT4 Index);
tPPPIf *SNMPGetBundlePtr (UINT4 Index);
tBAPInfo *SNMPGetBAPInfo (UINT4 Index);

UINT1 *RMGetPhNumString (tPhNum  *pPhNum);
VOID RMGetPhNumFromString (tPhNum *pPhNum, UINT1 *pPhNumString);


#endif  /* __PPP_BACPPROT_H__ */
