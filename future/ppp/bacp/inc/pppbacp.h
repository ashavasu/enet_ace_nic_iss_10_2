/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppbacp.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the typedefs, #defines and
 *  data structures used by the BACP Module.						
 *
 *******************************************************************/
#ifndef __PPP_PPPBACP_H__
#define __PPP_PPPBACP_H__

/*********************************************************************/
/*                              CONSTANTS                           */
/*********************************************************************/

/* CONFIGURATION OPTION VALUES  for BACP packets */

#define  FAVORED_PEER_OPTION		1
#define  MAX_BACP_OPT_TYPES			1

#define  FAVORED_PEER_IDX			0

#define  INITIATOR_FAVORED_PEER_NUMBER	1
#define  RECEIVER_FAVORED_PEER_NUMBER	0xffffffff

#define  BACP_NONE					0
#define  LOCAL_END					1
#define  REMOTE_END					2

/********************************************************************/
/*          TYPEDEFS    USED BY THE BACP MODULE                     */
/*********************************************************************/

typedef   struct   {
	UINT4          FavoredPeerNumber;
}   tBACPOptions;

typedef   struct   BACPif   {
	UINT1			OperStatus;
	UINT1			AdminStatus;
	UINT1			FavoredEnd;
	UINT1			u1Rsvd;
	tBACPOptions	BACPOptionsAckedByPeer;
	tBACPOptions	BACPOptionsAckedByLocal;
	tGSEM			BACPGSEM;
} tBACPIf;



typedef struct BACPCounters {
		UINT2		BACPAllocCounter;
		UINT2		BACPFreeCounter;
		UINT2		BACPIfAllocCounter;
		UINT2		BACPIfFreeCounter;
} tBACPCounters;


#endif  /* __PPP_PPPBACP_H__ */
