/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bacpgcp.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description: This file contains GCP GSEM related variables 
 *              of BACP module.
 *
 *******************************************************************/
#ifndef __PPP_BACPGCP_H__
#define __PPP_BACPGCP_H__

tOptHandlerFuns  BACPFavoredPeerHandlingFns = {
					BACPProcessFavoredPeerConfReq,
					BACPProcessFavoredPeerConfNak,
					NULL,
					BACPReturnFavoredPeerPtr,	
					NULL
}; 

tGSEMCallbacks   BACPGSEMCallback  = {
		BACPUp,
		BACPDown,
		NULL,
		NULL,
		NCPRxData,
		NCPTxData
		,
		NULL,
		NULL
};

tRangeInfo       FavoredPeerRange = { 0, 0xffffffff };

tGenOptionInfo      BACPGenOptionInfo[] = {
{ FAVORED_PEER_OPTION,	RANGE,	BYTE_LEN_4,	6,	&BACPFavoredPeerHandlingFns,	&FavoredPeerRange,	NOT_SET }
};

tOptNegFlagsPerIf   BACPOptPerIntf[] = { 
	{ FAVORED_PEER_OPTION,	DESIRED_SET | ALLOWED_FOR_PEER_SET | FORCED_TO_REQEST_SET }
};


#endif  /* __PPP_BACPGCP_H__ */
