/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppipxel.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description: Low Level Routines for IPXCP module.
 *
 *******************************************************************/
#include "pppsnmpm.h"
#include "ipxcpcom.h"
#include "snmccons.h"            /* Mani */
#include "llproto.h"
#include "ppipxlow.h"

/* LOW LEVEL Routines for Table : PppExtIpxConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppExtIpxConfigTable
 Input       :  The Indices
                PppExtIpxConfigIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppExtIpxConfigTable (INT4 i4PppExtIpxConfigIfIndex)
{
    if (SNMPGetIPXCPIfPtr (i4PppExtIpxConfigIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppExtIpxConfigTable
 Input       :  The Indices
                PppExtIpxConfigIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppExtIpxConfigTable (INT4 *pi4PppExtIpxConfigIfIndex)
{
    if (nmhGetFirstIndex (pi4PppExtIpxConfigIfIndex, IpxInstance, FIRST_INST) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppExtIpxConfigTable
 Input       :  The Indices
                PppExtIpxConfigIfIndex
                nextPppExtIpxConfigIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppExtIpxConfigTable (INT4 i4PppExtIpxConfigIfIndex,
                                     INT4 *pi4NextPppExtIpxConfigIfIndex)
{
    if (nmhGetNextIndex (&i4PppExtIpxConfigIfIndex, IpxInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextPppExtIpxConfigIfIndex = i4PppExtIpxConfigIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppExtIpxConfigLocalMaxSlotId
 Input       :  The Indices
                PppExtIpxConfigIfIndex

                The Object 
                retValPppExtIpxConfigLocalMaxSlotId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIpxConfigLocalMaxSlotId (INT4 i4PppExtIpxConfigIfIndex,
                                     INT4
                                     *pi4RetValPppExtIpxConfigLocalMaxSlotId)
{
    tIPXCPIf           *pIpxcpPtr;
    if ((pIpxcpPtr = SNMPGetIPXCPIfPtr (i4PppExtIpxConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppExtIpxConfigLocalMaxSlotId =
        pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXCPCompProt.MaxSlotId;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppExtIpxConfigLocalCompSlotId
 Input       :  The Indices
                PppExtIpxConfigIfIndex

                The Object 
                retValPppExtIpxConfigLocalCompSlotId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIpxConfigLocalCompSlotId (INT4 i4PppExtIpxConfigIfIndex,
                                      INT4
                                      *pi4RetValPppExtIpxConfigLocalCompSlotId)
{
    tIPXCPIf           *pIpxcpPtr;
    INT4                value;
    if ((pIpxcpPtr = SNMPGetIPXCPIfPtr (i4PppExtIpxConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    value = pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXCPCompProt.CompSlotId;

    switch (value)
    {

        case DISAB_COMP_ID:
            *pi4RetValPppExtIpxConfigLocalCompSlotId = 1;
            break;
        case ENAB_COMP_ID:
            *pi4RetValPppExtIpxConfigLocalCompSlotId = 2;
            break;
        case REDEFCOMPBITS:
            *pi4RetValPppExtIpxConfigLocalCompSlotId = 3;
            break;
        default:
            return (SNMP_FAILURE);
            break;
    }

    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppExtIpxConfigLocalMaxSlotId
 Input       :  The Indices
                PppExtIpxConfigIfIndex

                The Object 
                setValPppExtIpxConfigLocalMaxSlotId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIpxConfigLocalMaxSlotId (INT4 i4PppExtIpxConfigIfIndex,
                                     INT4 i4SetValPppExtIpxConfigLocalMaxSlotId)
{
    tIPXCPIf           *pIpxcpPtr;
    if ((pIpxcpPtr =
         SNMPGetOrCreateIPXCPIfPtr (i4PppExtIpxConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIpxcpPtr->IpxcpOptionsDesired.IPXCPCompProt.MaxSlotId =
        (UINT1) i4SetValPppExtIpxConfigLocalMaxSlotId;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppExtIpxConfigLocalCompSlotId
 Input       :  The Indices
                PppExtIpxConfigIfIndex

                The Object 
                setValPppExtIpxConfigLocalCompSlotId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIpxConfigLocalCompSlotId (INT4 i4PppExtIpxConfigIfIndex,
                                      INT4
                                      i4SetValPppExtIpxConfigLocalCompSlotId)
{
    tIPXCPIf           *pIpxcpPtr;
    if ((pIpxcpPtr =
         SNMPGetOrCreateIPXCPIfPtr (i4PppExtIpxConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    switch (i4SetValPppExtIpxConfigLocalCompSlotId)
    {
        case 1:
            pIpxcpPtr->IpxcpOptionsDesired.IPXCPCompProt.CompSlotId =
                DISAB_COMP_ID;
            break;

        case 2:
            pIpxcpPtr->IpxcpOptionsDesired.IPXCPCompProt.CompSlotId =
                ENAB_COMP_ID;
            break;
        case 3:
            pIpxcpPtr->IpxcpOptionsDesired.IPXCPCompProt.CompSlotId =
                REDEFCOMPBITS;
            break;
        default:
            return (SNMP_FAILURE);
            break;
    }

    return (SNMP_SUCCESS);

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppExtIpxConfigLocalMaxSlotId
 Input       :  The Indices
                PppExtIpxConfigIfIndex

                The Object 
                testValPppExtIpxConfigLocalMaxSlotId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIpxConfigLocalMaxSlotId (UINT4 *pu4ErrorCode,
                                        INT4 i4PppExtIpxConfigIfIndex,
                                        INT4
                                        i4TestValPppExtIpxConfigLocalMaxSlotId)
{
    PPP_UNUSED (i4PppExtIpxConfigIfIndex);
    if (i4TestValPppExtIpxConfigLocalMaxSlotId < 0
        || i4TestValPppExtIpxConfigLocalMaxSlotId > 255)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppExtIpxConfigLocalCompSlotId
 Input       :  The Indices
                PppExtIpxConfigIfIndex

                The Object 
                testValPppExtIpxConfigLocalCompSlotId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIpxConfigLocalCompSlotId (UINT4 *pu4ErrorCode,
                                         INT4 i4PppExtIpxConfigIfIndex,
                                         INT4
                                         i4TestValPppExtIpxConfigLocalCompSlotId)
{
    PPP_UNUSED (i4PppExtIpxConfigIfIndex);
    if (i4TestValPppExtIpxConfigLocalCompSlotId != 1
        && i4TestValPppExtIpxConfigLocalCompSlotId != 2
        && i4TestValPppExtIpxConfigLocalCompSlotId != 3)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/* LOW LEVEL Routines for Table : PppExtIpxTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppExtIpxTable
 Input       :  The Indices
                PppExtIpxIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppExtIpxTable (INT4 i4PppExtIpxIfIndex)
{
    if (SNMPGetIPXCPIfPtr (i4PppExtIpxIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppExtIpxTable
 Input       :  The Indices
                PppExtIpxIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppExtIpxTable (INT4 *pi4PppExtIpxIfIndex)
{
    if (nmhGetFirstIndex (pi4PppExtIpxIfIndex, IpxInstance, FIRST_INST) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppExtIpxTable
 Input       :  The Indices
                PppExtIpxIfIndex
                nextPppExtIpxIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppExtIpxTable (INT4 i4PppExtIpxIfIndex,
                               INT4 *pi4NextPppExtIpxIfIndex)
{
    if (nmhGetNextIndex (&i4PppExtIpxIfIndex, IpxInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextPppExtIpxIfIndex = i4PppExtIpxIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppExtIpxLocalCompSlotId
 Input       :  The Indices
                PppExtIpxIfIndex

                The Object 
                retValPppExtIpxLocalCompSlotId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIpxLocalCompSlotId (INT4 i4PppExtIpxIfIndex,
                                INT4 *pi4RetValPppExtIpxLocalCompSlotId)
{
    tIPXCPIf           *pIpxcpPtr;
    if ((pIpxcpPtr = SNMPGetIPXCPIfPtr (i4PppExtIpxIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppExtIpxLocalCompSlotId =
        pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXCPCompProt.CompSlotId;

    switch (*pi4RetValPppExtIpxLocalCompSlotId)
    {

        case DISAB_COMP_ID:
            *pi4RetValPppExtIpxLocalCompSlotId = 1;
            break;
        case ENAB_COMP_ID:
            *pi4RetValPppExtIpxLocalCompSlotId = 2;
            break;
        case REDEFCOMPBITS:
            *pi4RetValPppExtIpxLocalCompSlotId = 3;
            break;
        default:
            return (SNMP_FAILURE);
            break;
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppExtIpxRemoteCompSlotId
 Input       :  The Indices
                PppExtIpxIfIndex

                The Object 
                retValPppExtIpxRemoteCompSlotId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIpxRemoteCompSlotId (INT4 i4PppExtIpxIfIndex,
                                 INT4 *pi4RetValPppExtIpxRemoteCompSlotId)
{
    tIPXCPIf           *pIpxcpPtr;
    INT4                value;
    if ((pIpxcpPtr = SNMPGetIPXCPIfPtr (i4PppExtIpxIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    value = pIpxcpPtr->IpxcpOptionsAckedByLocal.IPXCPCompProt.CompSlotId;

    switch (value)
    {

        case DISAB_COMP_ID:
            *pi4RetValPppExtIpxRemoteCompSlotId = 1;
            break;
        case ENAB_COMP_ID:
            *pi4RetValPppExtIpxRemoteCompSlotId = 2;
            break;
        case REDEFCOMPBITS:
            *pi4RetValPppExtIpxRemoteCompSlotId = 3;
            break;
        default:
            return (SNMP_FAILURE);
            break;
    }

    return (SNMP_SUCCESS);

}
