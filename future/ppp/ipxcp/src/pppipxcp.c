/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppipxcp.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains callback routines of IPXCP module.
 *             It also contains routines for the input IPXCP packet 
 *             processing.
 *
 *******************************************************************/

#include "ipxcpcom.h"
#include "gsemdefs.h"
#include "gcpdefs.h"
#include "lcpdefs.h"
#include "genexts.h"
#include "globexts.h"

/*********************************************************************/
/* Variables  Used  by  the IPXCP Module */
/*********************************************************************/

UINT1               TelebitCompAvailable;
UINT1               LocalEntityStatus;
UINT2               IpxcpAllocCounter;
UINT2               IpxcpFreeCounter;
UINT2               IpxcpIfFreeCounter;

tIPXCPCtrlBlk       IPXCPCtrlBlk;

tGSEMCallbacks      IpxcpGSEMCallback = {
    IPXCPUp,
    IPXCPDown,
    NULL,
    IPXCPFinished,
    NCPRxData,
    NCPTxData,
    NULL,
    NULL
};

tIPXCompDiscVal     IPXCompDiscVal =
    { MAX_IPXCOMP_DISCRETE_VALUES, {0x0002, 0x0235}
};
tRoutingProtoDiscVal IPXRoutingProtoDiscVal =
    { MAX_ROUTING_PROTO_DISCRETE_VALUES, {0, 2, 4}, 0
};

/* Makefile changes */

tRangeInfo          IPXNetNumRange = { {0}
, {0xffffffff}
};

tOptHandlerFuns     IPXNetNumHandlingFuns = {
    IPXCPProcessNetNumConfReq,
    IPXCPProcessNetNumConfNak,
    IPXCPProcessNetNumConfRej,
    IPXCPReturnNetNumPtr,
    NULL
};
tOptHandlerFuns     IPXNodeNumHandlingFuns = {
    IPXCPProcessNodeNumConfReq,
    IPXCPProcessNodeNumConfNak,
    IPXCPProcessNodeNumConfRej,
    IPXCPReturnNodeNumPtr,
    NULL
};

tOptHandlerFuns     IPXTelebitHandlingFuns = {
    IPXCPProcessCompConfReq,
    IPXCPProcessCompConfNak,
    IPXCPProcessCompConfRej,
    IPXCPReturnCompPtr,
    IPXCPAddCompSubOpt
};

tOptHandlerFuns     IPXRoutingProtoHandlingFuns = {
    IPXCPProcessRoutingProtoConfReq,
    IPXCPProcessRoutingProtoConfNak,
    IPXCPProcessRoutingProtoConfRej,
    IPXCPReturnRoutingProtoPtr,
    IPXCPAddRoutingProtoSubOpt
};

tOptHandlerFuns     IPXRouterNameHandlingFuns = {
    IPXCPProcessRouterNameConfReq,
    IPXCPProcessRouterNameConfNak,
    IPXCPProcessRouterNameConfRej,
    IPXCPReturnRouterNamePtr,
    NULL
};

tGenOptionInfo      IPXCPGenOptionInfo[] = {

    {IPX_NET_NUM_OPTION, RANGE, BYTE_LEN_4, 6, &IPXNetNumHandlingFuns,
     {&IPXNetNumRange}, NOT_SET, 0, 0}
    ,

    {IPX_NODE_NUM_OPTION, STRING_TYPE, STRING_TYPE, 8,
     &IPXNodeNumHandlingFuns, {NULL}, NOT_SET, 0, 0}
    ,

    {IPX_COMP_OPTION, DISCRETE, BYTE_LEN_2, 6, &IPXTelebitHandlingFuns,
     {(VOID *) &IPXCompDiscVal}
     , NOT_SET, 0, 0}
    ,

    {IPX_ROUTING_PROTO_OPTION, DISCRETE, BYTE_LEN_2, 4,
     &IPXRoutingProtoHandlingFuns, {(VOID *) &IPXRoutingProtoDiscVal}
     , SET, 0, 0}
    ,

    {IPX_ROUTER_NAME_OPTION, STRING_TYPE, STRING_TYPE, 50,
     &IPXRouterNameHandlingFuns, {NULL}
     , NOT_SET, 0, 0}
    ,

    {IPX_CONFIG_COMP_OPTION, BOOLEAN_CATEGORY, BOOL_TYPE, 2,
     &BooleanHandlingFuns, {NULL}
     , NOT_SET, 0, 0}
};

tOptNegFlagsPerIf   IPXCPOptPerIntf[] = {
    {IPX_NET_NUM_OPTION, ALLOWED_FOR_PEER_SET | ALLOW_FORCED_NAK_SET, 0}
    ,
    {IPX_NODE_NUM_OPTION, ALLOWED_FOR_PEER_SET | ALLOW_FORCED_NAK_SET, 0}
    ,
    {IPX_COMP_OPTION, ALLOW_FORCED_NAK_SET | ALLOW_REJECT_SET, 0}
    ,
    {IPX_ROUTING_PROTO_OPTION, ALLOWED_FOR_PEER_SET | ALLOW_FORCED_NAK_SET, 0}
    ,

    {IPX_ROUTER_NAME_OPTION,
     ALLOW_FORCED_NAK_SET | ALLOWED_FOR_PEER_SET | ALLOW_REJECT_SET, 0},
    {IPX_CONFIG_COMP_OPTION, ALLOWED_FOR_PEER_SET, 0}
};

/***********************************************************************/
/*                      INTERFACE  FUNCTIONS                           */
/***********************************************************************
*  Function Name : IPXCPCreateIf
*  Description   :
*          This procedure creates an entry in the IPXCP interface data 
*  base corresponding to IfId. It also initializes the created entry to the 
*  default values  by calling the IPXCPinit() function.
*  Parameter(s)  :
*         pIf -    pointer to the PPP interface structure
*  Return Values : 
*      pointer to the newly created  interface structure , if the creation is 
*        success
*      NULL if it fails to create the interface.
*********************************************************************/
tIPXCPIf           *
IPXCPCreateIf (tPPPIf * pIf)
{
    tIPXCPIf           *pIpxcpPtr;

    if ((pIpxcpPtr = (tIPXCPIf *) ALLOC_IPXCP_IF ()) != NULL)
    {
        if (IPXCPInit (pIpxcpPtr) != NOT_OK)
        {
            pIpxcpPtr->IpxcpGSEM.pIf = pIf;
            return (pIpxcpPtr);
        }
        else
        {

            PPP_TRC (ALL_FAILURE, "Error: IPXCP Init failure .\n");
            FREE_IPXCP_IF (pIpxcpPtr);
            return (NULL);
        }
    }
    PPP_TRC (ALL_FAILURE, "Error: IPXCP Alloc/Init failure ");
    return (NULL);
}

/*********************************************************************
*  Function Name : IPXCPDeleteIf
*  Description   :
*               This procedure deletes an IPXCP interface  entry from the 
*  database and deallocates memory corresponding to the interface.
*  Parameter(s)  :
*         pIpxcpPtr -  points to  the IPXCP interface structure to be deleted.
*  Return Values : VOID
*********************************************************************/
VOID
IPXCPDeleteIf (tIPXCPIf * pIpxcpPtr)
{
    pIpxcpPtr->IpxcpGSEM.pIf->pIpxcpPtr = NULL;
    GSEMDelete (&(pIpxcpPtr->IpxcpGSEM));
    FREE_IPXCP_IF (pIpxcpPtr);
    return;
}

/*********************************************************************
*  Function Name : IPXCPEnableIf
*  Description   :
*              This function is called when there is an  IPXCP Admin Open 
*  for an interface from the SNMP It  triggers an OPEN event to the GSEM and 
*  passes the IPXCP tGSEM corresponds to this interface as parameter.
*  Parameter(s)    :
*    pIpxcpPtr    -    Points to the IPXCP interface structure to be enabled
*    pIpxcpPtrBlk -    Points to the IPXCPCtrlBlk 
*  Return Values : VOID
*********************************************************************/
VOID
IPXCPEnableIf (tIPXCPIf * pIpxcpPtr, tIPXCPCtrlBlk * pIpxcpCtrlBlk)
{
    UINT1               Index;
    tPPPIf             *pIf;

    if (pIpxcpPtr->IpxcpGSEM.pIf->BundleFlag == BUNDLE)
    {
        SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
        {
            if (pIf->MPInfo.MemberInfo.pBundlePtr != NULL
                && pIf->MPInfo.MemberInfo.pBundlePtr ==
                pIpxcpPtr->IpxcpGSEM.pIf)
            {
                pIf->pIpxcpPtr = pIpxcpPtr;
            }
        }
    }

    pIpxcpPtr->IpxcpOptionsDesired.IPXNetworkNumber =
        pIpxcpCtrlBlk->LocalIpxNetNumber;

    MEMCPY (pIpxcpPtr->IpxcpOptionsDesired.IPXNodeNumber,
            pIpxcpCtrlBlk->LoctoRemIpxNodeNumber, NODE_NUMBER_SIZE);
    MEMCPY (pIpxcpPtr->IpxcpOptionsAllowedForPeer.IPXNodeNumber,
            pIpxcpCtrlBlk->RemtoLocIpxNodeNumber, NODE_NUMBER_SIZE);

    pIpxcpPtr->IpxcpOptionsDesired.IPXRoutingProtocol =
        pIpxcpCtrlBlk->RoutingProtocol;
    pIpxcpPtr->IpxcpOptionsAllowedForPeer.IPXRoutingProtocol =
        pIpxcpCtrlBlk->RoutingProtoSupport;

    MEMCPY (pIpxcpPtr->IpxcpOptionsDesired.IPXRouterName,
            pIpxcpCtrlBlk->RouterName, ROUTER_NAME_SIZE);

    pIpxcpPtr->WanSupport = pIpxcpCtrlBlk->WanSupport;

/********** SKR003 ********************************/
/*********** && part of the  if condition is removed for setting DESIRED flag for NET_NUM and NODE_NUM  **********/
    if ((pIpxcpCtrlBlk->LocNetNumNegFlag == SET))
    {
        pIpxcpPtr->IpxcpGSEM.pNegFlagsPerIf[IPX_NET_NUM_IDX].FlagMask |=
            DESIRED_SET;
    }

    if ((pIpxcpCtrlBlk->LocNodeNumNegFlag == SET))
    {
        pIpxcpPtr->IpxcpGSEM.pNegFlagsPerIf[IPX_NODE_NUM_IDX].FlagMask |=
            DESIRED_SET;
    }

    if (pIpxcpCtrlBlk->RoutingProtocol != NOVELL_RIP_SAP)
    {
        pIpxcpPtr->IpxcpGSEM.pNegFlagsPerIf[IPX_ROUTING_PROTO_IDX].FlagMask |=
            DESIRED_SET;
    }
    else
    {
        pIpxcpPtr->IpxcpGSEM.pNegFlagsPerIf[IPX_ROUTING_PROTO_IDX].FlagMask &=
            DESIRED_NOT_SET;
    }

    pIpxcpPtr->IpxcpGSEM.pNegFlagsPerIf[IPX_ROUTER_NAME_IDX].FlagMask |=
        DESIRED_SET;

    if (LocalEntityStatus == IPX_ENDSYSTEM)
    {
        pIpxcpPtr->IpxcpGSEM.pNegFlagsPerIf[IPX_NODE_NUM_IDX].FlagMask |=
            FORCED_TO_REQUEST_SET;
    }

    if (pIpxcpPtr->WanSupport == IPX_WAN)
    {
        for (Index = 0; Index < MAX_IPXCP_OPT_TYPES; Index++)
        {
            pIpxcpPtr->IpxcpGSEM.pNegFlagsPerIf[Index].FlagMask |=
                ALLOW_REJECT_SET;
        }
    }

    if (pIpxcpPtr->WanSupport == IPX_NO_WAN)
    {
        if (LocalEntityStatus == IPX_ENDSYSTEM)
        {
            pIpxcpPtr->IpxcpGSEM.pNegFlagsPerIf[IPX_NET_NUM_IDX].FlagMask |=
                ALLOW_REJECT_SET;
        }
        if (LocalEntityStatus == IPX_ROUTER)
        {
            pIpxcpPtr->IpxcpGSEM.pNegFlagsPerIf[IPX_NODE_NUM_IDX].FlagMask |=
                ALLOW_REJECT_SET;
            pIpxcpPtr->IpxcpGSEM.pNegFlagsPerIf[IPX_ROUTING_PROTO_IDX].
                FlagMask |= ALLOW_REJECT_SET;
        }
    }

    pIpxcpPtr->AdminStatus = ADMIN_OPEN;

    /* 
       Check whether the link is already in the Operational state. If so, 
       then indicate the GSEM about the link UP 
     */

    if ((pIpxcpPtr->IpxcpGSEM.CurrentState != REQSENT)
        || (pIpxcpPtr->IpxcpGSEM.CurrentState != ACKRCVD)
        || (pIpxcpPtr->IpxcpGSEM.CurrentState != ACKSENT))
    {
        MEMCPY (&pIpxcpPtr->IpxcpOptionsAckedByPeer,
                &pIpxcpPtr->IpxcpOptionsDesired, sizeof (tIPXCPOptions));
        for (Index = 0; Index < MAX_IPXCP_OPT_TYPES; Index++)
        {
            if (pIpxcpPtr->IpxcpGSEM.pNegFlagsPerIf[Index].
                FlagMask & DESIRED_SET)
            {
                pIpxcpPtr->IpxcpGSEM.pNegFlagsPerIf[Index].FlagMask |=
                    ACKED_BY_PEER_SET;
            }
            else
            {
                pIpxcpPtr->IpxcpGSEM.pNegFlagsPerIf[Index].FlagMask &=
                    ACKED_BY_PEER_NOT_SET;
            }
        }
    }

    GSEMRun (&pIpxcpPtr->IpxcpGSEM, OPEN);

    if ((pIpxcpPtr->IpxcpGSEM.pIf->LcpStatus.LinkAvailability == LINK_AVAILABLE)
        && (pIpxcpPtr->IpxcpGSEM.CurrentState != CLOSED))
    {
        GSEMRun (&pIpxcpPtr->IpxcpGSEM, UP);
    }

    GSEMRestartIfNeeded (&pIpxcpPtr->IpxcpGSEM);

    return;
}

/*********************************************************************
*  Function Name : IPXCPDisableIf
*  Description   :
*          This function is called when there is an Admin Close for an 
*  interface from the SNMP It triggers a CLOSE event to the GSEM and passes 
*  the IPXCP GSEMStructure corresponds to this interface as a parameter. 
*  Parameter(s)  :
*            pIpxcpPtr - points to  the IPXCP interface structure to be  disabled
*  Return Values : VOID
*********************************************************************/
VOID
IPXCPDisableIf (tIPXCPIf * pIpxcpPtr)
{
    pIpxcpPtr->AdminStatus = ADMIN_CLOSE;
    GSEMRun (&pIpxcpPtr->IpxcpGSEM, CLOSE);
    return;
}

/*********************************************************************
*  Function Name : IPXCPInit
*  Description   :
*              This function initializes the IPXCP GSEM and  sets 
*  the  IPXCP configuration option variables to their default values. It is 
*  invoked by the IPXCPCreateIf() function.
*  Parameter(s)  :
*        pIpxcpPtr -   points to the IPXCP interface structure
*  Return Values : OK/NOT_OK 
*********************************************************************/
INT1
IPXCPInit (tIPXCPIf * pIpxcpPtr)
{

    IPXNetNumRange.MinVal.LongVal = 0;
    IPXNetNumRange.MaxVal.LongVal = 0xffffffff;

    IPXCPOptionInit (&pIpxcpPtr->IpxcpOptionsDesired);
    IPXCPOptionInit (&pIpxcpPtr->IpxcpOptionsAllowedForPeer);
    IPXCPOptionInit (&pIpxcpPtr->IpxcpOptionsAckedByPeer);
    IPXCPOptionInit (&pIpxcpPtr->IpxcpOptionsAckedByLocal);

    pIpxcpPtr->AdminStatus = STATUS_DOWN;
    pIpxcpPtr->OperStatus = STATUS_DOWN;

    if (GSEMInit (&pIpxcpPtr->IpxcpGSEM, MAX_IPXCP_OPT_TYPES) == NOT_OK)
    {
        return (NOT_OK);
    }
    pIpxcpPtr->IpxcpGSEM.Protocol = IPXCP_PROTOCOL;
    pIpxcpPtr->IpxcpGSEM.pCallbacks = &IpxcpGSEMCallback;
    pIpxcpPtr->IpxcpGSEM.pGenOptInfo = IPXCPGenOptionInfo;
    LocalEntityStatus = IPX_ROUTER;
    pIpxcpPtr->WanSupport = IPX_NO_WAN;
    pIpxcpPtr->RoutingProtoNaked = NOT_SET;

    MEMCPY (pIpxcpPtr->IpxcpGSEM.pNegFlagsPerIf, &IPXCPOptPerIntf,
            (sizeof (tOptNegFlagsPerIf) * MAX_IPXCP_OPT_TYPES));
    return (OK);
}

/***********************************************************************/
/*                      INTERNAL  FUNCTIONS                            */
/***********************************************************************
*  Function Name : IPXCPOptionInit
*  Description   :
*               This function initializes the IPXCPOption structure and is
*  called by the IPXCPInit function.
*  Parameter(s)  :
*       pIPXCPOpt   -  points to the IPXCPOption structure  to be initialized.
*  Return Value  :
*********************************************************************/
VOID
IPXCPOptionInit (tIPXCPOptions * pIPXCPOpt)
{
    pIPXCPOpt->IPXCPCompProt.Protocol = 0;
    pIPXCPOpt->IPXCPCompProt.MaxSlotId = 0;
    pIPXCPOpt->IPXCPCompProt.CompSlotId = 0;
    pIPXCPOpt->IPXNetworkNumber = 0;
    pIPXCPOpt->IPXRoutingProtocol = NOVELL_RIP_SAP;
    BZERO (&pIPXCPOpt->IPXNodeNumber, NODE_NUMBER_SIZE);
    BZERO (&pIPXCPOpt->IPXRouterName, ROUTER_NAME_SIZE);
    return;
}

/*********************************************************************
*  Function Name : IPXCPUp
*  Description   :
*              This function is called once the network level configuration  
*  option is negotiated  successfully. This function indicates  to the higher 
*  layers  that the IPXCP is up.  The pUp  field of  the IPXCPGSEMCallback points *  to this function and is called from the GSEM Module.
*  Parameter(s)  :
*            pIpxcpGSEM  - pointer to the IPXCP tGSEM 
*                                   corresponding to the current interface.
*  Return Values : VOID 
*********************************************************************/
VOID
IPXCPUp (tGSEM * pIpxcpGSEM)
{
    tIPXCPIf           *pIpxcpIf;

    PPP_TRC (EVENT_TRC, "IPXCP Negotiated...");

    pIpxcpIf = (tIPXCPIf *) pIpxcpGSEM->pIf->pIpxcpPtr;
    PPPHLINotifyProtStatusToHL (pIpxcpGSEM->pIf, IPXCP_PROTOCOL, UP);

    pIpxcpIf->OperStatus = STATUS_UP;

    return;
}

/*********************************************************************
*  Function Name : IPXCPDown
*  Description   :
*          This function is invoked  when the IPXCP protocol is leaving the 
*  OPENED state. It  indicates the higher layers  about the operational status 
*  of the IPXCP.  The pDown  field of  the IPXCPGSEMCallback structure  points to 
*  this function and is called from the GSEM Module.
*  Parameter(s)  :
*            pIpxcpGSEM  - pointer to the IPXCP tGSEM 
*                       corresponding to the current interface.
*  Return Values : VOID 
*********************************************************************/
VOID
IPXCPDown (tGSEM * pIpxcpGSEM)
{
    tPPPIf             *pIf;
    tIPXCPIf           *pIpxcpIf;

    PPP_TRC (EVENT_TRC, "IPXCP Terminating...");

    pIf = pIpxcpGSEM->pIf;
    pIpxcpIf = (tIPXCPIf *) pIpxcpGSEM->pIf->pIpxcpPtr;

    PPPHLINotifyProtStatusToHL (pIf, IPXCP_PROTOCOL, DOWN);

    pIpxcpIf->OperStatus = STATUS_DOWN;
    IPXCPFinished (pIpxcpGSEM);

    MEMCPY (&pIpxcpIf->IpxcpOptionsAckedByPeer, &pIpxcpIf->IpxcpOptionsDesired,
            sizeof (tIPXCPOptions));

    return;
}

/*********************************************************************
*  Function Name : IPXCPFinished
*  Description   :
*    This function is invoked when the higher layer is done with 
*  the link and the link is no longer needed. This invokes  procedures for 
*  terminating the link.
*         The pFinished  field of the GSEMCallback structure points 
*  to this function and is called from the GSEM Module.
*  Parameter(s)  :  
*            pGSEM  - pointer to IPXCP tGSEM corresponding 
*  to current interface
*  Return Value  :    VOID
*********************************************************************/
VOID
IPXCPFinished (tGSEM * pGSEM)
{
    tIPXCPIf           *pIpxcpPtr;
    UINT1               Index;
    pIpxcpPtr = pGSEM->pIf->pIpxcpPtr;
    MEMCPY (&pIpxcpPtr->IpxcpOptionsAckedByPeer,
            &pIpxcpPtr->IpxcpOptionsDesired, sizeof (tIPXCPOptions));
    for (Index = 0; Index < MAX_IPXCP_OPT_TYPES; Index++)
    {
        if (pGSEM->pNegFlagsPerIf[Index].FlagMask & DESIRED_SET)
        {
            pGSEM->pNegFlagsPerIf[Index].FlagMask |= ACKED_BY_PEER_SET;
        }
        else
        {
            pGSEM->pNegFlagsPerIf[Index].FlagMask &= ACKED_BY_PEER_NOT_SET;
        }
    }
    return;

}

/***********************************************************************
    OPTION SPECIFIC FUNCTIONS FOR PROCESSING CONF_REQ
************************************************************************/
/*********************************************************************
*  Function Name : IPXCPProcessNetNumConfReq
*  Description   :
*        This function is invoked to process the IPX Network Number option 
*  present in the CONF_REQ and to construct appropriate response
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer
*     tOptVal    -  Option Value received  to be processed
*     Offset     -  indicates the place in the outgoing buffer at which 
*                   the option value is appended
*  PresenceFlag  -  No need to check PresenceFlag.
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer
*********************************************************************/

INT1
IPXCPProcessNetNumConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal,
                           UINT2 Offset, UINT1 PresenceFlag)
{
    tIPXCPIf           *pIpxcpPtr;
    PPP_UNUSED (pInPkt);
    PPP_UNUSED (PresenceFlag);
    /* Makefile changes - unused */
    pIpxcpPtr = pGSEM->pIf->pIpxcpPtr;

    /* When this Option is not present in the req. packet, it should not be
       apppended to the Nak packet */

    if (pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXNetworkNumber > OptVal.LongVal)
    {
        ASSIGN4BYTE (pGSEM->pOutParam, Offset,
                     pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXNetworkNumber);
        return (BYTE_LEN_4);
    }

    pIpxcpPtr->IpxcpOptionsAckedByLocal.IPXNetworkNumber = OptVal.LongVal;
    return (ACK);
}

/*********************************************************************
*  Function Name : IPXCPProcessNodeNumConfReq
*  Description   :
*        This function is invoked to process the IPX Node Number
*    option present in the CONF_REQ and to construct appropriate response
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer
*     tOptVal    -  Option Value received  to be processed
*     Offset     -  indicates the place in the outgoing buffer at which 
*                   the option value is appended
*  PresenceFlag  -  indicates whether the option is present in the recd.
*                   CONF_REQ packet
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer
************************************************************************/

INT1
IPXCPProcessNodeNumConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal,
                            UINT2 Offset, UINT1 PresenceFlag)
{
    tIPXCPIf           *pIpxcpPtr;

    PPP_UNUSED (pInPkt);
    PPP_UNUSED (PresenceFlag);
    /* Makefile changes - unused */

    pIpxcpPtr = pGSEM->pIf->pIpxcpPtr;

    if ((PresenceFlag == NOT_SET)
        || ((PresenceFlag == SET)
            &&
            ((MEMCMP
              (pIpxcpPtr->IpxcpOptionsAllowedForPeer.IPXNodeNumber, pNullAddr,
               NODE_NUMBER_SIZE) != 0)
             &&
             (MEMCMP
              (pIpxcpPtr->IpxcpOptionsAllowedForPeer.IPXNodeNumber,
               OptVal.StrVal, NODE_NUMBER_SIZE) != 0))))
    {
        ASSIGNSTR (pGSEM->pOutParam,
                   pIpxcpPtr->IpxcpOptionsAllowedForPeer.IPXNodeNumber, Offset,
                   NODE_NUMBER_SIZE);
        return (NODE_NUMBER_SIZE);
    }
    MEMCPY (pIpxcpPtr->IpxcpOptionsAckedByLocal.IPXNodeNumber, OptVal.StrVal,
            NODE_NUMBER_SIZE);
    return (ACK);
}

/*********************************************************************
*  Function Name : IPXCPProcessCompConfReq
*  Description   :
*          This function processes the Compression option in the 
*  CONF_REQ packet and builds appropriate response.
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer
*     tOptVal    -  Option Value received  to be processed
*     Offset     -  indicates the place in the outgoing buffer at which 
*                   the option value is appended
*  PresenceFlag  -  indicates whether the option is present in the recd.
*                   CONF_REQ packet
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer
*********************************************************************/
INT1
IPXCPProcessCompConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal,
                         UINT2 Offset, UINT1 PresenceFlag)
{
    tIPXCPIf           *pIpxcpPtr;
    UINT1               MaxSlotId, CompSlotId;

    PPP_UNUSED (pInPkt);
    PPP_UNUSED (PresenceFlag);
    pIpxcpPtr = pGSEM->pIf->pIpxcpPtr;

    if (PresenceFlag == SET)
    {
        if (ExtractedLength != TELEBIT_OPT_LEN)
        {
            return (DISCARD);
        }
        EXTRACT1BYTE (pInPkt, MaxSlotId);
        EXTRACT1BYTE (pInPkt, CompSlotId);

        if (OptVal.ShortVal == TELEBIT_COMP_PROTO)
        {
            if (((MaxSlotId
                  ==
                  pIpxcpPtr->IpxcpOptionsAllowedForPeer.IPXCPCompProt.MaxSlotId)
                 && (CompSlotId ==
                     pIpxcpPtr->IpxcpOptionsAllowedForPeer.IPXCPCompProt.
                     CompSlotId))
                ||
                ((pIpxcpPtr->
                  IpxcpOptionsAllowedForPeer.IPXCPCompProt.CompSlotId ==
                  ACCEPT_ANY_COMP_SLOT_ID)
                 && (pIpxcpPtr->IpxcpOptionsAllowedForPeer.IPXCPCompProt.
                     MaxSlotId == ACCEPT_ANY_MAX_SLOT_ID)))
            {
                pIpxcpPtr->IpxcpOptionsAckedByLocal.IPXCPCompProt.MaxSlotId =
                    MaxSlotId;
                pIpxcpPtr->IpxcpOptionsAckedByLocal.IPXCPCompProt.CompSlotId =
                    CompSlotId;
                return (ACK);
            }
        }
    }
    ASSIGN2BYTE (pGSEM->pOutParam, Offset, TELEBIT_COMP_PROTO);
    ASSIGN1BYTE (pGSEM->pOutParam, Offset + 2,
                 pIpxcpPtr->IpxcpOptionsAllowedForPeer.IPXCPCompProt.MaxSlotId);
    ASSIGN1BYTE (pGSEM->pOutParam, Offset + 3,
                 pIpxcpPtr->IpxcpOptionsAllowedForPeer.IPXCPCompProt.
                 CompSlotId);
    return (BYTE_LEN_4);
}

/*********************************************************************
*  Function Name : IPXCPProcessRoutingProtoConfReq
*  Description   :
*        This function is invoked to process the IPX Routing Protocol 
*    option present in the CONF_REQ and to construct appropriate response
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer
*     tOptVal    -  Option Value received  to be processed
*     Offset     -  indicates the place in the outgoing buffer at which 
*                   the option value is appended
*  PresenceFlag  -  indicates whether the option is present in the recd.
*                   CONF_REQ packet
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer
************************************************************************/

INT1
IPXCPProcessRoutingProtoConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt,
                                 tOptVal OptVal, UINT2 Offset,
                                 UINT1 PresenceFlag)
{
    tIPXCPIf           *pIpxcpPtr;
    pIpxcpPtr = pGSEM->pIf->pIpxcpPtr;

    PPP_UNUSED (pInPkt);
    PPP_UNUSED (PresenceFlag);
    /* Makefile changes - unused */

    if (PresenceFlag == SET)
    {
        if (OptVal.ShortVal == NO_ROUTE_PROTO)
        {
            pIpxcpPtr->IpxcpOptionsAckedByLocal.IPXRoutingProtocol =
                OptVal.ShortVal;
            return (ACK);
        }
        else
        {
            if ((pIpxcpPtr->IpxcpOptionsAllowedForPeer.IPXRoutingProtocol ==
                 OptVal.ShortVal)
                ||
                ((pIpxcpPtr->
                  IpxcpOptionsAllowedForPeer.IPXRoutingProtocol == RIP_SAP_NLSP)
                 && ((OptVal.ShortVal == NOVELL_RIP_SAP)
                     || (OptVal.ShortVal == NLSP_PROTOCOL))))
            {
                pIpxcpPtr->IpxcpOptionsAckedByLocal.IPXRoutingProtocol =
                    OptVal.ShortVal;
                return (ACK);
            }
        }
    }
    ASSIGN2BYTE (pGSEM->pOutParam, Offset,
                 pIpxcpPtr->IpxcpOptionsAllowedForPeer.IPXRoutingProtocol);
    return (BYTE_LEN_2);
}

/*********************************************************************
*  Function Name : IPXCPProcessRouterNameConfReq
*  Description   :
*        This function is invoked to process the IPX Router Name 
*    option present in the CONF_REQ and to construct appropriate response
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer
*     tOptVal    -  Option Value received  to be processed
*     Offset     -  indicates the place in the outgoing buffer at which 
*                   the option value is appended
*  PresenceFlag  -  indicates whether the option is present in the recd.
*                   CONF_REQ packet
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer
************************************************************************/

INT1
IPXCPProcessRouterNameConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt,
                               tOptVal OptVal, UINT2 Offset, UINT1 PresenceFlag)
{
    tIPXCPIf           *pIpxcpPtr;
    INT1                RouterNameLen;
    PPP_UNUSED (Offset);
    /* Makefile changes - unused */
    PPP_UNUSED (pInPkt);
    PPP_UNUSED (PresenceFlag);
    RouterNameLen = (INT1) (STRLEN (OptVal.StrVal));
    pIpxcpPtr = pGSEM->pIf->pIpxcpPtr;
    MEMCPY (pIpxcpPtr->IpxcpOptionsAckedByLocal.IPXRouterName, OptVal.StrVal,
            RouterNameLen);
    BZERO ((&pIpxcpPtr->IpxcpOptionsAckedByLocal.IPXRouterName + RouterNameLen),
           ROUTER_NAME_SIZE - RouterNameLen);
    return (ACK);
}

/***********************************************************************
    OPTION SPECIFIC FUNCTIONS FOR PROCESSING CONF_NAK
************************************************************************/
/*********************************************************************
*  Function Name : IPXCPProcessNetNumConfNak 
*  Description   :
*        This function is invoked to process the IPX Network Number 
*  option present in the CONF_NAK and to construct appropriate response 
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*  Return Value  :  OK/NOT_OK/CLOSE 
*********************************************************************/
INT1
IPXCPProcessNetNumConfNak (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal)
{
    tIPXCPIf           *pIpxcpPtr;
    PPP_UNUSED (pInPkt);
    pIpxcpPtr = pGSEM->pIf->pIpxcpPtr;

    /* Makefile changes - unused */

    if (pGSEM->pNegFlagsPerIf[IPX_NET_NUM_IDX].FlagMask & ACKED_BY_PEER_MASK)
    {
        if ((pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXNetworkNumber == 0)
            || (pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXNetworkNumber <
                OptVal.LongVal))
        {
            pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXNetworkNumber =
                OptVal.LongVal;
            return (OK);
        }
        else
        {
            return (DISCARD);
        }
    }
    else
    {
        return (DISCARD);
    }
}

/*********************************************************************
*  Function Name : IPXCPProcessNodeNumConfNak 
*  Description   :
*        This function is invoked to process the IPX Node Number 
*  option present in the CONF_NAK and to construct appropriate response 
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*  Return Value  :  OK/NOT_OK/CLOSE 
*********************************************************************/
INT1
IPXCPProcessNodeNumConfNak (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal)
{
    tIPXCPIf           *pIpxcpPtr;

    pIpxcpPtr = pGSEM->pIf->pIpxcpPtr;

    PPP_UNUSED (pInPkt);
    /* Makefile changes - unused */

    if (pGSEM->pNegFlagsPerIf[IPX_NODE_NUM_IDX].FlagMask & ACKED_BY_PEER_MASK)
    {
        if (MEMCMP
            (pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXNodeNumber, pNullAddr,
             NODE_NUMBER_SIZE) == 0)
        {
            MEMCPY (pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXNodeNumber,
                    OptVal.StrVal, NODE_NUMBER_SIZE);
        }
        else
        {
            return (DISCARD);
        }
    }
    else
    {
            /******** SKR003 ************************/
            /********Change: Remote Node Num Negotiation when a forced Nak is received. *************/
        if (MEMCMP
            (pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXNodeNumber, OptVal.StrVal,
             NODE_NUMBER_SIZE) != 0)
        {
            if (MEMCMP
                (pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXNodeNumber, pNullAddr,
                 NODE_NUMBER_SIZE) == 0)
            {
                MEMCPY (pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXNodeNumber,
                        OptVal.StrVal, NODE_NUMBER_SIZE);
                return (OK);
            }
            return (MEMCMP (OptVal.StrVal, pNullAddr, NODE_NUMBER_SIZE) ==
                    0) ? OK : DISCARD;

        }
    }
    return (OK);
}

/*********************************************************************
*  Function Name : IPXCPProcessCompConfNak 
*  Description   :
*        This function is invoked to process the Compression option 
*    present in the CONF_NAK and to construct appropriate response .
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*  Return Value  :  OK/NOT_OK/CLOSE 
*********************************************************************/
INT1
IPXCPProcessCompConfNak (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal)
{
    tIPXCPIf           *pIpxcpPtr;
    UINT1               SubOptVal;

    pIpxcpPtr = pGSEM->pIf->pIpxcpPtr;
    PPP_UNUSED (pInPkt);

    if (pGSEM->pNegFlagsPerIf[IPX_COMP_IDX].FlagMask & ACKED_BY_PEER_MASK)
    {
        if (OptVal.ShortVal == TELEBIT_COMP_PROTO)
        {
            EXTRACT1BYTE (pInPkt, SubOptVal);
            pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXCPCompProt.MaxSlotId =
                SubOptVal;
            EXTRACT1BYTE (pInPkt, SubOptVal);
            pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXCPCompProt.CompSlotId =
                SubOptVal;
        }
        else
        {
            return (NOT_OK);
        }
    }
    else
    {
        if ((OptVal.ShortVal == TELEBIT_COMP_PROTO)
            && (TelebitCompAvailable == PPP_YES))
        {
            EXTRACT1BYTE (pInPkt, SubOptVal);
            pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXCPCompProt.MaxSlotId =
                SubOptVal;
            EXTRACT1BYTE (pInPkt, SubOptVal);
            pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXCPCompProt.CompSlotId =
                SubOptVal;
        }
        else
        {
            return (NOT_OK);
        }
    }
    return (OK);
}

/*********************************************************************
*  Function Name : IPXCPProcessRoutingProtoConfNak 
*  Description   :
*        This function is invoked to process the IPX Routing Protocol option 
*    present in the CONF_NAK and to construct appropriate response .
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*   NegOptFlag   -  indicates whether the option is included in the request
*  Return Value  :  OK/NOT_OK/CLOSE 
*********************************************************************/
INT1
IPXCPProcessRoutingProtoConfNak (tGSEM * pGSEM, t_MSG_DESC * pInPkt,
                                 tOptVal OptVal)
{
    tIPXCPIf           *pIpxcpPtr;

    pIpxcpPtr = pGSEM->pIf->pIpxcpPtr;
    pIpxcpPtr->RoutingProtoNaked = SET;

    /* Makefile changes - unused */

    PPP_UNUSED (pInPkt);
    if ((OptVal.ShortVal == NO_ROUTE_PROTO)
        ||
        ((pGSEM->
          pNegFlagsPerIf[IPX_ROUTING_PROTO_IDX].FlagMask & ACKED_BY_PEER_MASK)
         &&
         ((pIpxcpPtr->
           IpxcpOptionsAckedByPeer.IPXRoutingProtocol == RIP_SAP_NLSP)
          && ((OptVal.ShortVal == NOVELL_RIP_SAP)
              || (OptVal.ShortVal == NLSP_PROTOCOL)))))
    {
        pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXRoutingProtocol = OptVal.ShortVal;
    }
    return (OK);
}

/*********************************************************************
*  Function Name : IPXCPProcessRouterNameConfNak 
*  Description   :
*        This function is invoked to process the IPX Router Name option 
*    present in the CONF_NAK and to construct appropriate response .
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*   NegOptFlag   -  indicates whether the option is included in the request
*  Return Value  :  OK/NOT_OK/CLOSE 
*********************************************************************/
INT1
IPXCPProcessRouterNameConfNak (tGSEM * pGSEM, t_MSG_DESC * pInPkt,
                               tOptVal OptVal)
{
    PPP_UNUSED (pInPkt);
    PPP_UNUSED (pGSEM);
    PPP_UNUSED (OptVal);
    /* Makefile changes - unused */

    return (DISCARD);
}

/***********************************************************************
    OPTION SPECIFIC FUNCTIONS FOR ADDING SUB_OPTIONS IN THE CONF_REQ
************************************************************************/
/*********************************************************************
*  Function Name : IPXCPAddCompSubOpt
*  Description   :
*     This function is used to add Compression suboption(s).
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     tOptVal    -  Option Value received  to be processed
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer
*********************************************************************/
INT1
IPXCPAddCompSubOpt (tGSEM * pGSEM, tOptVal OptVal, t_MSG_DESC * pOutBuf)
{
    tIPXCPIf           *pIpxcpIf;

    /* Makefile changes - unused */
    PPP_TRC1 (CONTROL_PLANE, "OptVal=%d", OptVal.ShortVal);

    pIpxcpIf = (tIPXCPIf *) pGSEM->pIf->pIpxcpPtr;
    APPEND1BYTE (pOutBuf,
                 pIpxcpIf->IpxcpOptionsAckedByPeer.IPXCPCompProt.MaxSlotId);
    APPEND1BYTE (pOutBuf,
                 pIpxcpIf->IpxcpOptionsAckedByPeer.IPXCPCompProt.CompSlotId);
    return (BYTE_LEN_2);
}

/*********************************************************************
*  Function Name : IPXCPAddRoutingProtoSubOpt
*  Description   :
*     This function is used to add Routing Protocol suboption(s).
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     tOptVal    -  Option Value received  to be processed
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer
*********************************************************************/
INT1
IPXCPAddRoutingProtoSubOpt (tGSEM * pGSEM, tOptVal OptVal, t_MSG_DESC * pOutBuf)
{
    tIPXCPIf           *pIpxcpPtr;

    /* Makefile changes - unused */
    PPP_TRC1 (CONTROL_PLANE, "OptVal=%d", OptVal.ShortVal);

    pIpxcpPtr = (tIPXCPIf *) pGSEM->pIf->pIpxcpPtr;

    if ((pIpxcpPtr->RoutingProtoNaked == NOT_SET)
        && (pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXRoutingProtocol ==
            RIP_SAP_NLSP))
    {
        APPEND1BYTE (pOutBuf, IPX_ROUTING_PROTO_OPTION);
        APPEND1BYTE (pOutBuf, 4);
        APPEND2BYTE (pOutBuf, NOVELL_RIP_SAP);
        return (BYTE_LEN_4);
    }
    return (0);
}

/***********************************************************************
    OPTION SPECIFIC FUNCTIONS FOR RETURNING THE ADDRESS OF OPTION DATA
************************************************************************/
/*********************************************************************
*  Function Name : IPXCPReturnNetNumPtr 
*  Description   :
*     This function returns the address of the location where the IPX  
*     Network Number value to be included in the CONF_REQ, is stored.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*     OptLen     -  used to return the length of the option value (in
*                   case the option is of 'STRING' type)
*  Return Value  :  returns the option data address
*********************************************************************/
VOID               *
IPXCPReturnNetNumPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    tIPXCPIf           *pIpxcpIf;

    /* Makefile changes - unused */
    PPP_TRC1 (CONTROL_PLANE, "OptLen=%d", *OptLen);

    pIpxcpIf = (tIPXCPIf *) pGSEM->pIf->pIpxcpPtr;
    return (&pIpxcpIf->IpxcpOptionsAckedByPeer.IPXNetworkNumber);
}

/*********************************************************************
*  Function Name : IPXCPReturnNodeNumPtr 
*  Description   :
*     This function returns the address of the location where the IPX  
*     Node Number value to be included in the CONF_REQ, is stored.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*     OptLen     -  used to return the length of the option value (in
*                   case the option is of 'STRING' type)
*  Return Value  :  returns the option data address
*********************************************************************/
VOID               *
IPXCPReturnNodeNumPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    tIPXCPIf           *pIpxcpIf;

    /* Makefile changes - unused */
    PPP_TRC1 (CONTROL_PLANE, "OptLen=%d", *OptLen);

    pIpxcpIf = (tIPXCPIf *) pGSEM->pIf->pIpxcpPtr;

    *OptLen = NODE_NUMBER_SIZE;
    return (&pIpxcpIf->IpxcpOptionsAckedByPeer.IPXNodeNumber);
}

/*********************************************************************
*  Function Name : IPXCPReturnCompPtr 
*  Description   :
*     This function returns the address of the location where the TELEBIT  Comp 
*  value to be included in the CONF_REQ, is stored.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*     OptLen     -  used to return the length of the option value (in
*                   case the option is of 'STRING' type)
*  Return Value  :  returns the option data address
*********************************************************************/
VOID               *
IPXCPReturnCompPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    tIPXCPIf           *pIpxcpIf;
    /* Makefile changes - unused */
    PPP_TRC1 (CONTROL_PLANE, "OptLen=%d", *OptLen);

    pIpxcpIf = (tIPXCPIf *) pGSEM->pIf->pIpxcpPtr;
    return (&pIpxcpIf->IpxcpOptionsAckedByPeer.IPXCPCompProt.Protocol);
}

/*********************************************************************
*  Function Name : IPXCPReturnRouterNamePtr 
*  Description   :
*     This function returns the address of the location where the IPX  
*     Router Name value to be included in the CONF_REQ, is stored.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*     OptLen     -  used to return the length of the option value (in
*                   case the option is of 'STRING' type)
*  Return Value  :  returns the option data address
*********************************************************************/
VOID               *
IPXCPReturnRouterNamePtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    tIPXCPIf           *pIpxcpIf;

    pIpxcpIf = (tIPXCPIf *) pGSEM->pIf->pIpxcpPtr;
    PPP_TRC1 (CONTROL_PLANE, "IPX Router Name is: %s",
              pIpxcpIf->IpxcpOptionsAckedByPeer.IPXRouterName);
    *OptLen =
        (UINT1) (STRLEN (pIpxcpIf->IpxcpOptionsAckedByPeer.IPXRouterName));
    return (&pIpxcpIf->IpxcpOptionsAckedByPeer.IPXRouterName);
}

/*********************************************************************
*  Function Name : IPXCPReturnRoutingProtoPtr 
*  Description   :
*     This function returns the address of the location where the IPX  
*     Routing Protocol value to be included in the CONF_REQ, is stored.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*     OptLen     -  used to return the length of the option value (in
*                   case the option is of 'STRING' type)
*  Return Value  :  returns the option data address
*********************************************************************/
VOID               *
IPXCPReturnRoutingProtoPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    tIPXCPIf           *pIpxcpIf;

    /* Makefile changes - unused */
    PPP_TRC1 (CONTROL_PLANE, "OptLen=%d", *OptLen);

    pIpxcpIf = (tIPXCPIf *) pGSEM->pIf->pIpxcpPtr;
    if (pIpxcpIf->IpxcpOptionsAckedByPeer.IPXRoutingProtocol == RIP_SAP_NLSP)
    {
        GenReturnData = NLSP_PROTOCOL;
        return ((UINT2 *) &GenReturnData);
    }
    return (&pIpxcpIf->IpxcpOptionsAckedByPeer.IPXRoutingProtocol);
}

/***********************************************************************
    OPTION SPECIFIC FUNCTIONS FOR PROCESSING CONF_REJ
************************************************************************/
/*********************************************************************
*  Function Name : IPXCPProcessNetNumConfRej
*  Description   :
*    This function sets the default value of the rejected option.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*  Return Value  : OK
*********************************************************************/
INT1
IPXCPProcessNetNumConfRej (tGSEM * pGSEM)
{
    tIPXCPIf           *pIpxcpIf;

    pIpxcpIf = (tIPXCPIf *) pGSEM->pIf->pIpxcpPtr;
    pIpxcpIf->IpxcpOptionsAckedByPeer.IPXNetworkNumber = 0;
    return (OK);
}

/*********************************************************************
*  Function Name : IPXCPProcessNodeNumConfRej
*  Description   :
*    This function sets the default value of the rejected option.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*  Return Value  : OK
*********************************************************************/
INT1
IPXCPProcessNodeNumConfRej (tGSEM * pGSEM)
{
    tIPXCPIf           *pIpxcpPtr;

    pIpxcpPtr = (tIPXCPIf *) pGSEM->pIf->pIpxcpPtr;
    MEMCPY (pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXNodeNumber, pNullAddr,
            NODE_NUMBER_SIZE);
    return (OK);
}

/*********************************************************************
*  Function Name : IPXCPProcessCompConfRej
*  Description   :
*   This function sets the default value of the rejected option.
*  INput         :
*     pGSEM      -  points to the GSEM structure
*  Return Value  : OK
*********************************************************************/

INT1
IPXCPProcessCompConfRej (tGSEM * pGSEM)
{
    tIPXCPIf           *pIpxcpPtr;

    pIpxcpPtr = (tIPXCPIf *) pGSEM->pIf->pIpxcpPtr;
    pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXCPCompProt.MaxSlotId = 0;
    pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXCPCompProt.CompSlotId = 0;
    return (OK);
}

/*********************************************************************
*  Function Name : IPXCPProcessRoutingProtoConfRej
*  Description   :
*    This function sets the default value of the rejected option.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*  Return Value  : OK
*********************************************************************/

INT1
IPXCPProcessRoutingProtoConfRej (tGSEM * pGSEM)
{
    tIPXCPIf           *pIpxcpPtr;

    pIpxcpPtr = (tIPXCPIf *) pGSEM->pIf->pIpxcpPtr;
    pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXRoutingProtocol = NOVELL_RIP_SAP;
    return (OK);
}

/*********************************************************************
*  Function Name : IPXCPProcessRouterNameConfRej
*  Description   :
*    This function sets the default value of the rejected option.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*  Return Value  : OK
*********************************************************************/

INT1
IPXCPProcessRouterNameConfRej (tGSEM * pGSEM)
{
    tIPXCPIf           *pIpxcpPtr;

    pIpxcpPtr = (tIPXCPIf *) pGSEM->pIf->pIpxcpPtr;
    BZERO (&pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXRouterName, ROUTER_NAME_SIZE);
    return (OK);
}

/*********************************************************************
*  Function Name : IPXCPCopyOptions 
*  Description   :
*    This function is used to copy the IPXCP options.
*  Input         :
*    pGSEM        -     Points to the GSEM structure
*    Flag        -     Specifies whether to alloc or free the temporary 
*                    variable.
*    PktType        -    Specifies whether the function is called from request
*                    processing or Nak processing.                  
*  Return Value        :
*                    DISCARD on allocation error, OK otherwise.
*********************************************************************/
INT1
IPXCPCopyOptions (tGSEM * pGSEM, UINT1 Flag, UINT1 PktType)
{
    UINT1               Size;
    tIPXCPIf           *pIpxcpPtr;
    tIPXCPOptions      *pTemp;

    pIpxcpPtr = (tIPXCPIf *) pGSEM->pIf->pIpxcpPtr;
    Size = sizeof (tIPXCPOptions);

    if (PktType == CONF_REQ_CODE)
    {
        pTemp = &pIpxcpPtr->IpxcpOptionsAckedByLocal;
    }
    else
    {
        pTemp = &pIpxcpPtr->IpxcpOptionsAckedByPeer;
    }

    if (Flag == NOT_SET)
    {
        if ((pCopyPtr = (UINT1 *) ALLOC_STR (Size)) == NULL)
        {
            PRINT_MEM_ALLOC_FAILURE;
            return (DISCARD);
        }
        MEMCPY (pCopyPtr, (UINT1 *) pTemp, Size);
        if (PktType == CONF_REQ_CODE)
        {
            IPXCPOptionInit (&pIpxcpPtr->IpxcpOptionsAckedByLocal);
        }
    }
    else
    {
        if (Flag == SET)
        {
            MEMCPY (pTemp, (tIPXCPOptions *) pCopyPtr, Size);
        }
        FREE_STR (pCopyPtr);
    }
    return (OK);
}

/*********************************************************************
*  Function Name : IPXCPGetSEMPtr
*  Description   :
*    This function returns the pointer to the IPXCP GSEM taking the tPPPIf
*   structure as a parameter.
*  Input         :
*       pIf -   Points to the PPP Interface structure.
*  Return Value  :
*           Returns a pointer to the IPXCP GSEM.
*********************************************************************/
tGSEM              *
IPXCPGetSEMPtr (tPPPIf * pIf)
{
    tIPXCPIf           *pIpxcpPtr;

    if (pIf->pIpxcpPtr != NULL)
    {
        pIpxcpPtr = (tIPXCPIf *) pIf->pIpxcpPtr;
        PPP_TRC (CONTROL_PLANE, "IPXCP:");
        return (&pIpxcpPtr->IpxcpGSEM);
    }
    return (NULL);
}
