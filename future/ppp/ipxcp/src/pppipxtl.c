/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppipxtl.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description: Low Level Routines for IPXCP module.
 *
 *******************************************************************/
#include "pppsnmpm.h"
#include "ipxcpcom.h"
#include "snmccons.h"
#include "ipxexts.h"
#include "llproto.h"
#include "globexts.h"
#include "ppteslow.h"
/* LOW LEVEL Routines for Table : PppTestIpxTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppTestIpxTable
 Input       :  The Indices
                PppTestIpxIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppTestIpxTable (INT4 i4PppTestIpxIfIndex)
{
    if (SNMPGetIPXCPIfPtr (i4PppTestIpxIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppTestIpxTable
 Input       :  The Indices
                PppTestIpxIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppTestIpxTable (INT4 *pi4PppTestIpxIfIndex)
{
    if (nmhGetFirstIndex (pi4PppTestIpxIfIndex, IpxInstance, FIRST_INST) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppTestIpxTable
 Input       :  The Indices
                PppTestIpxIfIndex
                nextPppTestIpxIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppTestIpxTable (INT4 i4PppTestIpxIfIndex,
                                INT4 *pi4NextPppTestIpxIfIndex)
{
    if (nmhGetNextIndex (&i4PppTestIpxIfIndex, IpxInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextPppTestIpxIfIndex = i4PppTestIpxIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppTestIpxLocalNetNumber
 Input       :  The Indices
                PppTestIpxIfIndex

                The Object 
                retValPppTestIpxLocalNetNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestIpxLocalNetNumber (INT4 i4PppTestIpxIfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValPppTestIpxLocalNetNumber)
{
    tIPXCPIf           *pIpxcpPtr;

    if ((pIpxcpPtr = SNMPGetIPXCPIfPtr (i4PppTestIpxIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    /* RFC007 *Str =(UINT1 *) &pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXNetworkNumber; */
    pRetValPppTestIpxLocalNetNumber->i4_Length = 4;
    MEMCPY (pRetValPppTestIpxLocalNetNumber->pu1_OctetList,
            (UINT1 *) &pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXNetworkNumber,
            pRetValPppTestIpxLocalNetNumber->i4_Length);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppTestIpxLocalNodeNumber
 Input       :  The Indices
                PppTestIpxIfIndex

                The Object 
                retValPppTestIpxLocalNodeNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestIpxLocalNodeNumber (INT4 i4PppTestIpxIfIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValPppTestIpxLocalNodeNumber)
{
    tIPXCPIf           *pIpxcpPtr;

    if ((pIpxcpPtr = SNMPGetIPXCPIfPtr (i4PppTestIpxIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    /* RFC007 *Str      =   pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXNodeNumber;   */
    pRetValPppTestIpxLocalNodeNumber->i4_Length = NODE_NUMBER_SIZE;
    MEMCPY (pRetValPppTestIpxLocalNodeNumber->pu1_OctetList,
            pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXNodeNumber,
            pRetValPppTestIpxLocalNodeNumber->i4_Length);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppTestIpxRemoteNodeNumber
 Input       :  The Indices
                PppTestIpxIfIndex

                The Object 
                retValPppTestIpxRemoteNodeNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestIpxRemoteNodeNumber (INT4 i4PppTestIpxIfIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValPppTestIpxRemoteNodeNumber)
{
    tIPXCPIf           *pIpxcpPtr;

    if ((pIpxcpPtr = SNMPGetIPXCPIfPtr (i4PppTestIpxIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    /* RFC007 *Str      =   pIpxcpPtr->IpxcpOptionsAllowedForPeer.IPXNodeNumber;   */
    pRetValPppTestIpxRemoteNodeNumber->i4_Length = NODE_NUMBER_SIZE;
    MEMCPY (pRetValPppTestIpxRemoteNodeNumber->pu1_OctetList,
            pIpxcpPtr->IpxcpOptionsAllowedForPeer.IPXNodeNumber,
            pRetValPppTestIpxRemoteNodeNumber->i4_Length);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppTestIpxRoutingProtocol
 Input       :  The Indices
                PppTestIpxIfIndex

                The Object 
                retValPppTestIpxRoutingProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestIpxRoutingProtocol (INT4 i4PppTestIpxIfIndex,
                                 INT4 *pi4RetValPppTestIpxRoutingProtocol)
{
    tIPXCPIf           *pIpxcpPtr;

    if ((pIpxcpPtr = SNMPGetIPXCPIfPtr (i4PppTestIpxIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    switch (pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXRoutingProtocol)
    {
        case NO_ROUTE_PROTO:
            *pi4RetValPppTestIpxRoutingProtocol = 1;
            break;
        case NOVELL_RIP_SAP:
            *pi4RetValPppTestIpxRoutingProtocol = 2;
            break;
        case NLSP_PROTOCOL:
            *pi4RetValPppTestIpxRoutingProtocol = 3;
            break;
        case RIP_SAP_NLSP:
            *pi4RetValPppTestIpxRoutingProtocol = 4;
            break;
        default:
            return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppTestIpxRoutingProtoSupport
 Input       :  The Indices
                PppTestIpxIfIndex

                The Object 
                retValPppTestIpxRoutingProtoSupport
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestIpxRoutingProtoSupport (INT4 i4PppTestIpxIfIndex,
                                     INT4
                                     *pi4RetValPppTestIpxRoutingProtoSupport)
{
    tIPXCPIf           *pIpxcpPtr;

    if ((pIpxcpPtr = SNMPGetIPXCPIfPtr (i4PppTestIpxIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    switch (pIpxcpPtr->IpxcpOptionsAllowedForPeer.IPXRoutingProtocol)
    {
        case NO_ROUTE_PROTO:
            *pi4RetValPppTestIpxRoutingProtoSupport = 1;
            break;
        case NOVELL_RIP_SAP:
            *pi4RetValPppTestIpxRoutingProtoSupport = 2;
            break;
        case NLSP_PROTOCOL:
            *pi4RetValPppTestIpxRoutingProtoSupport = 3;
            break;
        case RIP_SAP_NLSP:
            *pi4RetValPppTestIpxRoutingProtoSupport = 4;
            break;
        default:
            return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppTestIpxRouterName
 Input       :  The Indices
                PppTestIpxIfIndex

                The Object 
                retValPppTestIpxRouterName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestIpxRouterName (INT4 i4PppTestIpxIfIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValPppTestIpxRouterName)
{
    tIPXCPIf           *pIpxcpPtr;

    if ((pIpxcpPtr = SNMPGetIPXCPIfPtr (i4PppTestIpxIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    /* RFC007 *Str      =   pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXRouterName;   */
    pRetValPppTestIpxRouterName->i4_Length =
        STRLEN (pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXRouterName);
    MEMCPY (pRetValPppTestIpxRouterName->pu1_OctetList,
            pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXRouterName,
            pRetValPppTestIpxRouterName->i4_Length);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppTestIpxNetNumNegFlag
 Input       :  The Indices
                PppTestIpxIfIndex

                The Object 
                retValPppTestIpxNetNumNegFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestIpxNetNumNegFlag (INT4 i4PppTestIpxIfIndex,
                               INT4 *pi4RetValPppTestIpxNetNumNegFlag)
{
    tIPXCPIf           *pIpxcpPtr;

    if ((pIpxcpPtr = SNMPGetIPXCPIfPtr (i4PppTestIpxIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppTestIpxNetNumNegFlag =
        (pIpxcpPtr->IpxcpGSEM.pNegFlagsPerIf[IPX_NET_NUM_IDX].
         FlagMask & DESIRED_SET) ? 2 : 1;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppTestIpxNodeNumNegFlag
 Input       :  The Indices
                PppTestIpxIfIndex

                The Object 
                retValPppTestIpxNodeNumNegFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestIpxNodeNumNegFlag (INT4 i4PppTestIpxIfIndex,
                                INT4 *pi4RetValPppTestIpxNodeNumNegFlag)
{
    tIPXCPIf           *pIpxcpPtr;

    if ((pIpxcpPtr = SNMPGetIPXCPIfPtr (i4PppTestIpxIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppTestIpxNodeNumNegFlag =
        (pIpxcpPtr->IpxcpGSEM.pNegFlagsPerIf[IPX_NODE_NUM_IDX].
         FlagMask & DESIRED_SET) ? 2 : 1;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppTestIpxTelebitCompAvailable
 Input       :  The Indices
                PppTestIpxIfIndex

                The Object 
                retValPppTestIpxTelebitCompAvailable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestIpxTelebitCompAvailable (INT4 i4PppTestIpxIfIndex,
                                      INT4
                                      *pi4RetValPppTestIpxTelebitCompAvailable)
{
    tIPXCPIf           *pIpxcpPtr;

    if ((pIpxcpPtr = SNMPGetIPXCPIfPtr (i4PppTestIpxIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppTestIpxTelebitCompAvailable =
        (TelebitCompAvailable == PPP_YES) ? 2 : 1;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppTestIpxWanSupport
 Input       :  The Indices
                PppTestIpxIfIndex

                The Object 
                retValPppTestIpxWanSupport
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestIpxWanSupport (INT4 i4PppTestIpxIfIndex,
                            INT4 *pi4RetValPppTestIpxWanSupport)
{
    tIPXCPIf           *pIpxcpPtr;

    if ((pIpxcpPtr = SNMPGetIPXCPIfPtr (i4PppTestIpxIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppTestIpxWanSupport = pIpxcpPtr->WanSupport;
    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppTestIpxLocalNetNumber
 Input       :  The Indices
                PppTestIpxIfIndex

                The Object 
                setValPppTestIpxLocalNetNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestIpxLocalNetNumber (INT4 i4PppTestIpxIfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValPppTestIpxLocalNetNumber)
{
    tIPXCPIf           *pIpxcpPtr;
    if ((pIpxcpPtr = SNMPGetOrCreateIPXCPIfPtr (i4PppTestIpxIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    MEMCPY (&IPXCPCtrlBlk.LocalIpxNetNumber,
            pSetValPppTestIpxLocalNetNumber->pu1_OctetList,
            pSetValPppTestIpxLocalNetNumber->i4_Length);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppTestIpxLocalNodeNumber
 Input       :  The Indices
                PppTestIpxIfIndex

                The Object 
                setValPppTestIpxLocalNodeNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestIpxLocalNodeNumber (INT4 i4PppTestIpxIfIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValPppTestIpxLocalNodeNumber)
{
    tIPXCPIf           *pIpxcpPtr;
    if ((pIpxcpPtr = SNMPGetOrCreateIPXCPIfPtr (i4PppTestIpxIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    MEMCPY (&IPXCPCtrlBlk.LoctoRemIpxNodeNumber,
            pSetValPppTestIpxLocalNodeNumber->pu1_OctetList,
            pSetValPppTestIpxLocalNodeNumber->i4_Length);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppTestIpxRemoteNodeNumber
 Input       :  The Indices
                PppTestIpxIfIndex

                The Object 
                setValPppTestIpxRemoteNodeNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestIpxRemoteNodeNumber (INT4 i4PppTestIpxIfIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pSetValPppTestIpxRemoteNodeNumber)
{
    tIPXCPIf           *pIpxcpPtr;
    if ((pIpxcpPtr = SNMPGetOrCreateIPXCPIfPtr (i4PppTestIpxIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    MEMCPY (&IPXCPCtrlBlk.RemtoLocIpxNodeNumber,
            pSetValPppTestIpxRemoteNodeNumber->pu1_OctetList,
            pSetValPppTestIpxRemoteNodeNumber->i4_Length);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppTestIpxRoutingProtocol
 Input       :  The Indices
                PppTestIpxIfIndex

                The Object 
                setValPppTestIpxRoutingProtocol
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestIpxRoutingProtocol (INT4 i4PppTestIpxIfIndex,
                                 INT4 i4SetValPppTestIpxRoutingProtocol)
{
    tIPXCPIf           *pIpxcpPtr;
    if ((pIpxcpPtr = SNMPGetOrCreateIPXCPIfPtr (i4PppTestIpxIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    switch (i4SetValPppTestIpxRoutingProtocol)
    {
        case 1:
            IPXCPCtrlBlk.RoutingProtocol = NO_ROUTE_PROTO;
            break;
        case 2:
            IPXCPCtrlBlk.RoutingProtocol = NOVELL_RIP_SAP;
            break;
        case 3:
            IPXCPCtrlBlk.RoutingProtocol = NLSP_PROTOCOL;
            break;
        case 4:
            IPXCPCtrlBlk.RoutingProtocol = RIP_SAP_NLSP;
            break;
        default:
            return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppTestIpxRoutingProtoSupport
 Input       :  The Indices
                PppTestIpxIfIndex

                The Object 
                setValPppTestIpxRoutingProtoSupport
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestIpxRoutingProtoSupport (INT4 i4PppTestIpxIfIndex,
                                     INT4 i4SetValPppTestIpxRoutingProtoSupport)
{
    tIPXCPIf           *pIpxcpPtr;
    if ((pIpxcpPtr = SNMPGetOrCreateIPXCPIfPtr (i4PppTestIpxIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    switch (i4SetValPppTestIpxRoutingProtoSupport)
    {
        case 1:
            IPXCPCtrlBlk.RoutingProtoSupport = NO_ROUTE_PROTO;
            break;
        case 2:
            IPXCPCtrlBlk.RoutingProtoSupport = NOVELL_RIP_SAP;
            break;
        case 3:
            IPXCPCtrlBlk.RoutingProtoSupport = NLSP_PROTOCOL;
            break;
        case 4:
            IPXCPCtrlBlk.RoutingProtoSupport = RIP_SAP_NLSP;
            break;
        default:
            return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppTestIpxRouterName
 Input       :  The Indices
                PppTestIpxIfIndex

                The Object 
                setValPppTestIpxRouterName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestIpxRouterName (INT4 i4PppTestIpxIfIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValPppTestIpxRouterName)
{
    tIPXCPIf           *pIpxcpPtr;
    if ((pIpxcpPtr = SNMPGetOrCreateIPXCPIfPtr (i4PppTestIpxIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    MEMCPY (&IPXCPCtrlBlk.RouterName,
            pSetValPppTestIpxRouterName->pu1_OctetList,
            pSetValPppTestIpxRouterName->i4_Length);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppTestIpxNetNumNegFlag
 Input       :  The Indices
                PppTestIpxIfIndex

                The Object 
                setValPppTestIpxNetNumNegFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestIpxNetNumNegFlag (INT4 i4PppTestIpxIfIndex,
                               INT4 i4SetValPppTestIpxNetNumNegFlag)
{
    tIPXCPIf           *pIpxcpPtr;
    if ((pIpxcpPtr = SNMPGetOrCreateIPXCPIfPtr (i4PppTestIpxIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    IPXCPCtrlBlk.LocNetNumNegFlag =
        (i4SetValPppTestIpxNetNumNegFlag == DISABLE) ? 0 : 1;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppTestIpxNodeNumNegFlag
 Input       :  The Indices
                PppTestIpxIfIndex

                The Object 
                setValPppTestIpxNodeNumNegFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestIpxNodeNumNegFlag (INT4 i4PppTestIpxIfIndex,
                                INT4 i4SetValPppTestIpxNodeNumNegFlag)
{
    tIPXCPIf           *pIpxcpPtr;
    if ((pIpxcpPtr = SNMPGetOrCreateIPXCPIfPtr (i4PppTestIpxIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    IPXCPCtrlBlk.LocNodeNumNegFlag =
        (i4SetValPppTestIpxNodeNumNegFlag == DISABLE) ? 0 : 1;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppTestIpxTelebitCompAvailable
 Input       :  The Indices
                PppTestIpxIfIndex

                The Object 
                setValPppTestIpxTelebitCompAvailable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestIpxTelebitCompAvailable (INT4 i4PppTestIpxIfIndex,
                                      INT4
                                      i4SetValPppTestIpxTelebitCompAvailable)
{
    tIPXCPIf           *pIpxcpPtr;
    if ((pIpxcpPtr = SNMPGetOrCreateIPXCPIfPtr (i4PppTestIpxIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (i4SetValPppTestIpxTelebitCompAvailable == ENABLE)
    {
        TelebitCompAvailable = PPP_YES;
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppTestIpxWanSupport
 Input       :  The Indices
                PppTestIpxIfIndex

                The Object 
                setValPppTestIpxWanSupport
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestIpxWanSupport (INT4 i4PppTestIpxIfIndex,
                            INT4 i4SetValPppTestIpxWanSupport)
{
    tIPXCPIf           *pIpxcpPtr;

    if ((pIpxcpPtr = SNMPGetOrCreateIPXCPIfPtr (i4PppTestIpxIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    IPXCPCtrlBlk.WanSupport = (UINT1) i4SetValPppTestIpxWanSupport;
    return (SNMP_SUCCESS);

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppTestIpxLocalNetNumber
 Input       :  The Indices
                PppTestIpxIfIndex

                The Object 
                testValPppTestIpxLocalNetNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestIpxLocalNetNumber (UINT4 *pu4ErrorCode,
                                   INT4 i4PppTestIpxIfIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValPppTestIpxLocalNetNumber)
{
    if (PppGetPppIfPtr (i4PppTestIpxIfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (SNMP_FAILURE);
    }
    if (pTestValPppTestIpxLocalNetNumber->i4_Length > 4)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppTestIpxLocalNodeNumber
 Input       :  The Indices
                PppTestIpxIfIndex

                The Object 
                testValPppTestIpxLocalNodeNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestIpxLocalNodeNumber (UINT4 *pu4ErrorCode,
                                    INT4 i4PppTestIpxIfIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValPppTestIpxLocalNodeNumber)
{
    if (PppGetPppIfPtr (i4PppTestIpxIfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (SNMP_FAILURE);
    }
    if (pTestValPppTestIpxLocalNodeNumber->i4_Length > 6)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppTestIpxRemoteNodeNumber
 Input       :  The Indices
                PppTestIpxIfIndex

                The Object 
                testValPppTestIpxRemoteNodeNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestIpxRemoteNodeNumber (UINT4 *pu4ErrorCode,
                                     INT4 i4PppTestIpxIfIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pTestValPppTestIpxRemoteNodeNumber)
{
    if (PppGetPppIfPtr (i4PppTestIpxIfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (SNMP_FAILURE);
    }
    if (pTestValPppTestIpxRemoteNodeNumber->i4_Length > 6)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppTestIpxRoutingProtocol
 Input       :  The Indices
                PppTestIpxIfIndex

                The Object 
                testValPppTestIpxRoutingProtocol
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestIpxRoutingProtocol (UINT4 *pu4ErrorCode,
                                    INT4 i4PppTestIpxIfIndex,
                                    INT4 i4TestValPppTestIpxRoutingProtocol)
{
    if (PppGetPppIfPtr (i4PppTestIpxIfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (SNMP_FAILURE);
    }
    if (i4TestValPppTestIpxRoutingProtocol != 1
        && i4TestValPppTestIpxRoutingProtocol != 2
        && i4TestValPppTestIpxRoutingProtocol != 3
        && i4TestValPppTestIpxRoutingProtocol != 4)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppTestIpxRoutingProtoSupport
 Input       :  The Indices
                PppTestIpxIfIndex

                The Object 
                testValPppTestIpxRoutingProtoSupport
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestIpxRoutingProtoSupport (UINT4 *pu4ErrorCode,
                                        INT4 i4PppTestIpxIfIndex,
                                        INT4
                                        i4TestValPppTestIpxRoutingProtoSupport)
{
    if (PppGetPppIfPtr (i4PppTestIpxIfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (SNMP_FAILURE);
    }
    if (i4TestValPppTestIpxRoutingProtoSupport != 1
        && i4TestValPppTestIpxRoutingProtoSupport != 2
        && i4TestValPppTestIpxRoutingProtoSupport != 3
        && i4TestValPppTestIpxRoutingProtoSupport != 4)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppTestIpxRouterName
 Input       :  The Indices
                PppTestIpxIfIndex

                The Object 
                testValPppTestIpxRouterName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestIpxRouterName (UINT4 *pu4ErrorCode, INT4 i4PppTestIpxIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValPppTestIpxRouterName)
{
    if (PppGetPppIfPtr (i4PppTestIpxIfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (SNMP_FAILURE);
    }
    if (pTestValPppTestIpxRouterName->i4_Length > ROUTER_NAME_SIZE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppTestIpxNetNumNegFlag
 Input       :  The Indices
                PppTestIpxIfIndex

                The Object 
                testValPppTestIpxNetNumNegFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestIpxNetNumNegFlag (UINT4 *pu4ErrorCode, INT4 i4PppTestIpxIfIndex,
                                  INT4 i4TestValPppTestIpxNetNumNegFlag)
{
    if (PppGetPppIfPtr (i4PppTestIpxIfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (SNMP_FAILURE);
    }
    if (i4TestValPppTestIpxNetNumNegFlag != ENABLE
        && i4TestValPppTestIpxNetNumNegFlag != DISABLE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppTestIpxNodeNumNegFlag
 Input       :  The Indices
                PppTestIpxIfIndex

                The Object 
                testValPppTestIpxNodeNumNegFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestIpxNodeNumNegFlag (UINT4 *pu4ErrorCode,
                                   INT4 i4PppTestIpxIfIndex,
                                   INT4 i4TestValPppTestIpxNodeNumNegFlag)
{
    if (PppGetPppIfPtr (i4PppTestIpxIfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (SNMP_FAILURE);
    }
    if (i4TestValPppTestIpxNodeNumNegFlag != ENABLE
        && i4TestValPppTestIpxNodeNumNegFlag != DISABLE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppTestIpxTelebitCompAvailable
 Input       :  The Indices
                PppTestIpxIfIndex

                The Object 
                testValPppTestIpxTelebitCompAvailable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestIpxTelebitCompAvailable (UINT4 *pu4ErrorCode,
                                         INT4 i4PppTestIpxIfIndex,
                                         INT4
                                         i4TestValPppTestIpxTelebitCompAvailable)
{
    if (PppGetPppIfPtr (i4PppTestIpxIfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (SNMP_FAILURE);
    }
    if (i4TestValPppTestIpxTelebitCompAvailable != ENABLE
        && i4TestValPppTestIpxTelebitCompAvailable != DISABLE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppTestIpxWanSupport
 Input       :  The Indices
                PppTestIpxIfIndex

                The Object 
                testValPppTestIpxWanSupport
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestIpxWanSupport (UINT4 *pu4ErrorCode, INT4 i4PppTestIpxIfIndex,
                               INT4 i4TestValPppTestIpxWanSupport)
{
    PPP_UNUSED (i4PppTestIpxIfIndex);
    if (i4TestValPppTestIpxWanSupport != IPX_WAN
        && i4TestValPppTestIpxWanSupport != IPX_NO_WAN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}
