/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppipxll.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description: Low Level Routines for IPXCP module.
 *
 *******************************************************************/
#include "pppsnmpm.h"
#include "ipxcpcom.h"
#include "snmccons.h"
#include "ipxexts.h"
#include "llproto.h"
#include "globexts.h"
#include "ppipxlow.h"

/*********************************************************************
*  Function Name : SNMPGetIPXCPIfPtr
*  Description   :
*        This function is invoked to get the pointer to tIPXCPIf 
*        structure present in the tPPPIf Interface structure.
*  Parameter(s)  :
*        Index
*  Return Value  : 
*        pointer to tIPXCPIf
*
*********************************************************************/
INT1
CheckAndGetIpxPtr (tPPPIf * pIf)
{
    if (pIf->pIpxcpPtr != NULL)
    {
        return (PPP_SNMP_OK);
    }
    return (PPP_SNMP_ERR);
}

tIPXCPIf           *
SNMPGetIPXCPIfPtr (Index)
     UINT4               Index;
{
    tPPPIf             *pIf;

    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if ((pIf != NULL) && (pIf->LinkInfo.IfIndex == Index))
            return (pIf->pIpxcpPtr);
    }
    return ((tIPXCPIf *) NULL);
}

/*******************************************************************/
tIPXCPIf           *
SNMPGetOrCreateIPXCPIfPtr (Index)
     UINT4               Index;
{
    tPPPIf             *pIf;

    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if ((pIf != NULL) && (pIf->LinkInfo.IfIndex == Index))
        {
            if (pIf->BundleFlag != BUNDLE
                && pIf->MPInfo.MemberInfo.pBundlePtr != NULL)
            {
                return (NULL);
            }

            if (pIf->pIpxcpPtr == NULL)
            {
                if ((pIf->pIpxcpPtr = IPXCPCreateIf (pIf)) == NULL)
                {
                    return (NULL);
                }
            }
            return (pIf->pIpxcpPtr);
        }
    }
    return (NULL);
}

/* LOW LEVEL Routines for Table : PppIpxTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppIpxTable
 Input       :  The Indices
                PppIpxIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhValidateIndexInstancePppIpxTable (INT4 i4PppIpxIfIndex)
{
    if (SNMPGetIPXCPIfPtr (i4PppIpxIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppIpxTable
 Input       :  The Indices
                PppIpxIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexPppIpxTable (INT4 *pi4PppIpxIfIndex)
{
    if (nmhGetFirstIndex (pi4PppIpxIfIndex, IpxInstance, FIRST_INST) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppIpxTable
 Input       :  The Indices
                PppIpxIfIndex
                nextPppIpxIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexPppIpxTable (INT4 i4PppIpxIfIndex, INT4 *pi4NextPppIpxIfIndex)
{
    if (nmhGetNextIndex (&i4PppIpxIfIndex, IpxInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextPppIpxIfIndex = i4PppIpxIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppIpxOperStatus
 Input       :  The Indices
                PppIpxIfIndex

                The Object 
                retValPppIpxOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppIpxOperStatus (INT4 i4PppIpxIfIndex, INT4 *pi4RetValPppIpxOperStatus)
{
    tIPXCPIf           *pIpxcpPtr;
    if ((pIpxcpPtr = SNMPGetIPXCPIfPtr (i4PppIpxIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppIpxOperStatus = pIpxcpPtr->OperStatus;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPppIpxLoctoRemCompProtocol
 Input       :  The Indices
                PppIpxIfIndex

                The Object 
                retValPppIpxLoctoRemCompProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppIpxLoctoRemCompProtocol (INT4 i4PppIpxIfIndex,
                                  INT4 *pi4RetValPppIpxLoctoRemCompProtocol)
{
    tIPXCPIf           *pIpxcpPtr;
    INT4                compProt;

    if ((pIpxcpPtr = SNMPGetIPXCPIfPtr (i4PppIpxIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    compProt = pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXCPCompProt.Protocol;
    if (pIpxcpPtr->OperStatus == STATUS_UP)
    {
        switch (compProt)
        {
            case NO_COMP_PROTO:
                *pi4RetValPppIpxLoctoRemCompProtocol = 1;
                break;
            case TELEBIT_COMP_PROTO:
                *pi4RetValPppIpxLoctoRemCompProtocol = 2;
                break;
            case SHIVA_COMP_PROTO:
                *pi4RetValPppIpxLoctoRemCompProtocol = 3;
                break;
            default:
                return (SNMP_FAILURE);
                break;
        }
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/******n*********************************************************************
 Function    :  nmhGetPppIpxRemtoLocCompProtocol
 Input       :  The Indices
                PppIpxIfIndex

                The Object 
                retValPppIpxRemtoLocCompProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppIpxRemtoLocCompProtocol (INT4 i4PppIpxIfIndex,
                                  INT4 *pi4RetValPppIpxRemtoLocCompProtocol)
{
    tIPXCPIf           *pIpxcpPtr;
    INT4                compProt;

    if ((pIpxcpPtr = SNMPGetIPXCPIfPtr (i4PppIpxIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    compProt = pIpxcpPtr->IpxcpOptionsAckedByLocal.IPXCPCompProt.Protocol;
    if (pIpxcpPtr->OperStatus == STATUS_UP)
    {
        switch (compProt)
        {
            case NO_COMP_PROTO:
                *pi4RetValPppIpxRemtoLocCompProtocol = 1;
                break;
            case TELEBIT_COMP_PROTO:
                *pi4RetValPppIpxRemtoLocCompProtocol = 2;
                break;
            case SHIVA_COMP_PROTO:
                *pi4RetValPppIpxRemtoLocCompProtocol = 3;
                break;
            default:
                return (SNMP_FAILURE);
                break;
        }
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetPppIpxLocalMaxSlotId
 Input       :  The Indices
                PppIpxIfIndex

                The Object 
                retValPppIpxLocalMaxSlotId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppIpxLocalMaxSlotId (INT4 i4PppIpxIfIndex,
                            INT4 *pi4RetValPppIpxLocalMaxSlotId)
{
    tIPXCPIf           *pIpxcpPtr;

    if ((pIpxcpPtr = SNMPGetIPXCPIfPtr (i4PppIpxIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pIpxcpPtr->OperStatus == STATUS_UP)
    {
        *pi4RetValPppIpxLocalMaxSlotId =
            pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXCPCompProt.MaxSlotId;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetPppIpxRemoteMaxSlotId
 Input       :  The Indices
                PppIpxIfIndex

                The Object 
                retValPppIpxRemoteMaxSlotId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppIpxRemoteMaxSlotId (INT4 i4PppIpxIfIndex,
                             INT4 *pi4RetValPppIpxRemoteMaxSlotId)
{
    tIPXCPIf           *pIpxcpPtr;

    if ((pIpxcpPtr = SNMPGetIPXCPIfPtr (i4PppIpxIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pIpxcpPtr->OperStatus == STATUS_UP)
    {
        *pi4RetValPppIpxRemoteMaxSlotId =
            pIpxcpPtr->IpxcpOptionsAckedByLocal.IPXCPCompProt.CompSlotId;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);

}

/* LOW LEVEL Routines for Table : PppIpxConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppIpxConfigTable
 Input       :  The Indices
                PppIpxConfigIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppIpxConfigTable (INT4 i4PppIpxConfigIfIndex)
{
    if (SNMPGetIPXCPIfPtr (i4PppIpxConfigIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppIpxConfigTable
 Input       :  The Indices
                PppIpxConfigIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppIpxConfigTable (INT4 *pi4PppIpxConfigIfIndex)
{
    if (nmhGetFirstIndex (pi4PppIpxConfigIfIndex, IpxInstance, FIRST_INST) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppIpxConfigTable
 Input       :  The Indices
                PppIpxConfigIfIndex
                nextPppIpxConfigIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppIpxConfigTable (INT4 i4PppIpxConfigIfIndex,
                                  INT4 *pi4NextPppIpxConfigIfIndex)
{
    if (nmhGetNextIndex (&i4PppIpxConfigIfIndex, IpxInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextPppIpxConfigIfIndex = i4PppIpxConfigIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppIpxConfigAdminStatus
 Input       :  The Indices
                PppIpxConfigIfIndex

                The Object 
                retValPppIpxConfigAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppIpxConfigAdminStatus (INT4 i4PppIpxConfigIfIndex,
                               INT4 *pi4RetValPppIpxConfigAdminStatus)
{
    tIPXCPIf           *pIpxcpPtr;

    if ((pIpxcpPtr = SNMPGetIPXCPIfPtr (i4PppIpxConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValPppIpxConfigAdminStatus = pIpxcpPtr->AdminStatus;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppIpxConfigCompression
 Input       :  The Indices
                PppIpxConfigIfIndex

                The Object 
                retValPppIpxConfigCompression
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppIpxConfigCompression (INT4 i4PppIpxConfigIfIndex,
                               INT4 *pi4RetValPppIpxConfigCompression)
{
    tIPXCPIf           *pIpxcpPtr;

    if ((pIpxcpPtr = SNMPGetIPXCPIfPtr (i4PppIpxConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXCPCompProt.Protocol == 0)
    {
        *pi4RetValPppIpxConfigCompression = 1;
    }
    else
    {
        if (pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXCPCompProt.Protocol ==
            TELEBIT_COMP_PROTO)
        {
            *pi4RetValPppIpxConfigCompression = 2;
        }
        else
        {
            if (pIpxcpPtr->IpxcpOptionsAckedByPeer.IPXCPCompProt.Protocol ==
                SHIVA_COMP_PROTO)
            {
                *pi4RetValPppIpxConfigCompression = 3;
            }
            else
            {
                return (SNMP_FAILURE);
            }
        }
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppIpxConfigConfComplete
 Input       :  The Indices
                PppIpxConfigIfIndex

                The Object 
                retValPppIpxConfigConfComplete
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppIpxConfigConfComplete (INT4 i4PppIpxConfigIfIndex,
                                INT4 *pi4RetValPppIpxConfigConfComplete)
{
    tIPXCPIf           *pIpxcpPtr;

    if ((pIpxcpPtr = SNMPGetIPXCPIfPtr (i4PppIpxConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pIpxcpPtr->OperStatus == STATUS_UP)
    {
        *pi4RetValPppIpxConfigConfComplete =
            (pIpxcpPtr->IpxcpGSEM.pNegFlagsPerIf[IPX_CONFIG_COMP_IDX].
             FlagMask & ACKED_BY_LOCAL_MASK) ? 1 : 2;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppIpxConfigAdminStatus
 Input       :  The Indices
                PppIpxConfigIfIndex

                The Object 
                testValPppIpxConfigAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppIpxConfigAdminStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4PppIpxConfigIfIndex,
                                  INT4 i4TestValPppIpxConfigAdminStatus)
{
    PPP_UNUSED (i4PppIpxConfigIfIndex);
    if (i4TestValPppIpxConfigAdminStatus != ADMIN_OPEN
        && i4TestValPppIpxConfigAdminStatus != ADMIN_CLOSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppIpxConfigCompression
 Input       :  The Indices
                PppIpxConfigIfIndex

                The Object 
                testValPppIpxConfigCompression
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppIpxConfigCompression (UINT4 *pu4ErrorCode,
                                  INT4 i4PppIpxConfigIfIndex,
                                  INT4 i4TestValPppIpxConfigCompression)
{
    PPP_UNUSED (i4PppIpxConfigIfIndex);
    if (i4TestValPppIpxConfigCompression != 1
        && i4TestValPppIpxConfigCompression != 2
        && i4TestValPppIpxConfigCompression != 3)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppIpxConfigConfComplete
 Input       :  The Indices
                PppIpxConfigIfIndex

                The Object 
                testValPppIpxConfigConfComplete
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppIpxConfigConfComplete (UINT4 *pu4ErrorCode,
                                   INT4 i4PppIpxConfigIfIndex,
                                   INT4 i4TestValPppIpxConfigConfComplete)
{
    PPP_UNUSED (i4PppIpxConfigIfIndex);
    if (i4TestValPppIpxConfigConfComplete != 1
        && i4TestValPppIpxConfigConfComplete != 2)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppIpxConfigAdminStatus
 Input       :  The Indices
                PppIpxConfigIfIndex

                The Object 
                setValPppIpxConfigAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppIpxConfigAdminStatus (INT4 i4PppIpxConfigIfIndex,
                               INT4 i4SetValPppIpxConfigAdminStatus)
{
    tIPXCPIf           *pIpxcpPtr;

    if ((pIpxcpPtr = SNMPGetOrCreateIPXCPIfPtr (i4PppIpxConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (i4SetValPppIpxConfigAdminStatus == ADMIN_OPEN)
    {
        pIpxcpPtr->AdminStatus = (UINT1) i4SetValPppIpxConfigAdminStatus;
        IPXCPEnableIf (pIpxcpPtr, &IPXCPCtrlBlk);
    }
    else
    {
        pIpxcpPtr->AdminStatus = (UINT1) i4SetValPppIpxConfigAdminStatus;
        IPXCPDisableIf (pIpxcpPtr);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppIpxConfigCompression
 Input       :  The Indices
                PppIpxConfigIfIndex

                The Object 
                setValPppIpxConfigCompression
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppIpxConfigCompression (INT4 i4PppIpxConfigIfIndex,
                               INT4 i4SetValPppIpxConfigCompression)
{
    tIPXCPIf           *pIpxcpPtr;

    if ((pIpxcpPtr = SNMPGetOrCreateIPXCPIfPtr (i4PppIpxConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (i4SetValPppIpxConfigCompression == 1)
    {
        pIpxcpPtr->IpxcpGSEM.pNegFlagsPerIf[IPX_CONFIG_COMP_IDX].FlagMask &=
            DESIRED_NOT_SET;
    }
    else
    {
        if (i4SetValPppIpxConfigCompression == 2)
        {
            pIpxcpPtr->IpxcpGSEM.pNegFlagsPerIf[IPX_COMP_IDX].FlagMask |=
                DESIRED_SET;
            pIpxcpPtr->IpxcpOptionsDesired.IPXCPCompProt.Protocol =
                TELEBIT_COMP_PROTO;

        }
    }
    if (TelebitCompAvailable == PPP_YES)
    {
        pIpxcpPtr->IpxcpGSEM.pNegFlagsPerIf[IPX_COMP_IDX].FlagMask |=
            ALLOWED_FOR_PEER_SET;
    }
    else
    {
        pIpxcpPtr->IpxcpGSEM.pNegFlagsPerIf[IPX_COMP_IDX].FlagMask &=
            ALLOWED_FOR_PEER_NOT_SET;
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppIpxConfigConfComplete
 Input       :  The Indices
                PppIpxConfigIfIndex

                The Object 
                setValPppIpxConfigConfComplete
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppIpxConfigConfComplete (INT4 i4PppIpxConfigIfIndex,
                                INT4 i4SetValPppIpxConfigConfComplete)
{
    tIPXCPIf           *pIpxcpPtr;

    if ((pIpxcpPtr = SNMPGetOrCreateIPXCPIfPtr (i4PppIpxConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (i4SetValPppIpxConfigConfComplete == 1)
    {
        pIpxcpPtr->IpxcpGSEM.pNegFlagsPerIf[IPX_CONFIG_COMP_IDX].FlagMask |=
            DESIRED_SET;
    }
    else
    {
        pIpxcpPtr->IpxcpGSEM.pNegFlagsPerIf[IPX_CONFIG_COMP_IDX].FlagMask &=
            DESIRED_NOT_SET;
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppIpxConfigLocalEntityStatus
 Input       :  The Indices

                The Object 
                retValPppIpxConfigLocalEntityStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppIpxConfigLocalEntityStatus (INT4 i4PppIpxConfigIfIndex,
                                     INT4
                                     *pi4RetValPppIpxConfigLocalEntityStatus)
{
    PPP_UNUSED (i4PppIpxConfigIfIndex);
    PPP_UNUSED (pi4RetValPppIpxConfigLocalEntityStatus);
    return (SNMP_FAILURE);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppIpxConfigLocalEntityStatus
 Input       :  The Indices

                The Object 
                setValPppIpxConfigLocalEntityStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppIpxConfigLocalEntityStatus (INT4 i4PppIpxConfigIfIndex,
                                     INT4 i4SetValPppIpxConfigLocalEntityStatus)
{

    PPP_UNUSED (i4PppIpxConfigIfIndex);
    PPP_UNUSED (i4SetValPppIpxConfigLocalEntityStatus);
    return (SNMP_FAILURE);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppIpxConfigLocalEntityStatus
 Input       :  The Indices

                The Object 
                testValPppIpxConfigLocalEntityStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppIpxConfigLocalEntityStatus (UINT4 *pu4ErrorCode,
                                        INT4 i4PppIpxConfigIfIndex,
                                        INT4
                                        i4TestValPppIpxConfigLocalEntityStatus)
{
    PPP_UNUSED (i4TestValPppIpxConfigLocalEntityStatus);
    PPP_UNUSED (i4PppIpxConfigIfIndex);
    PPP_UNUSED (pu4ErrorCode);
    return (SNMP_SUCCESS);
}
