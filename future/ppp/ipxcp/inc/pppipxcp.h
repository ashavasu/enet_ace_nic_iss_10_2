/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppipxcp.h,v 1.2 2014/03/11 14:02:50 siva Exp $
 *
 * Description:This file contains the typedefs, #defines and  
 *             data structures used by the IPXCP Module.      
 *
 *******************************************************************/
#ifndef __PPP_PPPIPXCP_H__
#define __PPP_PPPIPXCP_H__


/*********************************************************************/
/*                              CONSTANTS                           */
/*********************************************************************/

/* CONFIGURATION OPTION VALUES  for IPXCP packets */

#define  IPX_ENDSYSTEM                      1      
#define  IPX_ROUTER                         2      

#define  IPX_NO_WAN                         1      
#define  IPX_WAN                            2      

#define  NO_ROUTE_PROTO                     0      
#define  NOVELL_RIP_SAP                     2      
#define  NLSP_PROTOCOL                      4      
#define  RIP_SAP_NLSP                       5      

#define  NO_COMP_PROTO                      0      
#define  TELEBIT_COMP_PROTO                 0x0002 
#define  SHIVA_COMP_PROTO                   0x0235 
#define  IPX_DATAGRAM                       0x002b 

#define  DISAB_COMP_ID                      0      
#define  ENAB_COMP_ID                       1      
#define  REDEFCOMPBITS                      2      

#define  ACCEPT_ANY_COMP_SLOT_ID            0      
#define  ACCEPT_ANY_MAX_SLOT_ID             0      

#define  ROUTER_NAME_SIZE                   48     
#define  NODE_NUMBER_SIZE                   6      

#define  MAX_IPXCP_OPT_TYPES                6      
#define  TELEBIT_OPT_LEN                    6      

#define  IPX_NET_NUM_IDX                    0      
#define  IPX_NODE_NUM_IDX                   1      
#define  IPX_COMP_IDX                       2      
#define  IPX_ROUTING_PROTO_IDX              3      
#define  IPX_ROUTER_NAME_IDX                4      
#define  IPX_CONFIG_COMP_IDX                5      

#define  IPX_NET_NUM_OPTION                 1      
#define  IPX_NODE_NUM_OPTION                2      
#define  IPX_COMP_OPTION                    3      
#define  IPX_ROUTING_PROTO_OPTION           4      
#define  IPX_ROUTER_NAME_OPTION             5      
#define  IPX_CONFIG_COMP_OPTION             6      

#define  MAX_IPXCOMP_DISCRETE_VALUES        2      
#define  MAX_ROUTING_PROTO_DISCRETE_VALUES  3      


/********************************************************************/
/*          TYPEDEFS    USED BY THE IPXCP MODULE                     */
/*********************************************************************/

typedef struct {
    UINT4       DiscHeader;
    UINT2       Value[MAX_IPXCOMP_DISCRETE_VALUES];
} tIPXCompDiscVal;

typedef struct {
    UINT4       DiscHeader;
    UINT2       Value[MAX_ROUTING_PROTO_DISCRETE_VALUES];
    UINT2       u2Rsvd2;
} tRoutingProtoDiscVal;

typedef struct {
 UINT2 Protocol;
 UINT1 MaxSlotId;
 UINT1 CompSlotId;
} tIPXCPCompProt;


typedef struct {
 tIPXCPCompProt IPXCPCompProt;
 UINT1   IPXNodeNumber[NODE_NUMBER_SIZE];
 UINT1    IPXRouterName[ROUTER_NAME_SIZE];
 UINT2    IPXRoutingProtocol; 
 UINT4    IPXNetworkNumber;
} tIPXCPOptions;

typedef struct ipxcpif {
 VOID            *HlHandle;            /* This is used to store HL handle.
           so that if ppp has any data to 
           send to HL then this handle can be
              used to reach the HL  */
 UINT1    OperStatus;
 UINT1    AdminStatus;
 UINT2   u2Rsvd1;
 tIPXCPOptions  IpxcpOptionsDesired;
 tIPXCPOptions  IpxcpOptionsAllowedForPeer;
 tIPXCPOptions  IpxcpOptionsAckedByPeer;
 tIPXCPOptions  IpxcpOptionsAckedByLocal;
 tGSEM      IpxcpGSEM;
 UINT1   RoutingProtoNaked;
 UINT1   WanSupport;
 UINT2   u2Rsvd2;
} tIPXCPIf;

typedef struct {
    VOID   *HlHandle;                /* this is used to store the HL handle.so
          that this can be used to reach Hl 
          whenever ppp has any data to send */
 UINT2 RoutingProtocol;
 UINT2 RoutingProtoSupport;
 UINT1 RouterName[ROUTER_NAME_SIZE];
 UINT1 LoctoRemIpxNodeNumber[NODE_NUMBER_SIZE];
 UINT1 RemtoLocIpxNodeNumber[NODE_NUMBER_SIZE];
 UINT4 LocalIpxNetNumber;
 UINT1 LocNetNumNegFlag;
 UINT1 LocNodeNumNegFlag;
 UINT1 WanSupport;
 UINT1 u1Rsvd;
} tIPXCPCtrlBlk;


#endif  /* __PPP_PPPIPXCP_H__ */
