/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipxproto.h,v 1.2 2014/03/11 14:02:50 siva Exp $
 *
 * Description:This file contains Prototypes for Functions of 
   IPXCP   Module
 *
 *******************************************************************/
#ifndef __PPP_IPXPROTO_H__
#define __PPP_IPXPROTO_H__


/*   Prototypes for Functions of IPXCP   Module   */

INT1 IPXCPProcessNetNumConfReq ( tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal, UINT2 Offset, UINT1 PresenceFlag );
INT1 IPXCPProcessNodeNumConfReq ( tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal, UINT2 Offset, UINT1 PresenceFlag );
INT1 IPXCPProcessCompConfReq ( tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal, UINT2 Offset, UINT1 PresenceFlag );
INT1 IPXCPProcessRoutingProtoConfReq ( tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal, UINT2 Offset, UINT1 PresenceFlag );
INT1 IPXCPProcessRouterNameConfReq ( tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal, UINT2 Offset, UINT1 PresenceFlag );
INT1 IPXCPProcessNetNumConfNak ( tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal );
INT1 IPXCPProcessNodeNumConfNak ( tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal );
INT1 IPXCPProcessCompConfNak (tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal );
INT1 IPXCPProcessRoutingProtoConfNak (tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal);
INT1 IPXCPProcessRouterNameConfNak (tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal);
INT1 IPXCPAddCompSubOpt ( tGSEM *pGSEM, tOptVal OptVal, t_MSG_DESC *pOutBuf );
INT1 IPXCPAddRoutingProtoSubOpt (tGSEM *pGSEM, tOptVal OptVal, t_MSG_DESC *pOutBuf );
VOID *IPXCPReturnNetNumPtr(tGSEM  *pGSEM, UINT1 *OptLen);
VOID *IPXCPReturnNodeNumPtr ( tGSEM  *pGSEM, UINT1 *OptLen );
VOID *IPXCPReturnCompPtr ( tGSEM  *pGSEM, UINT1 *OptLen );
VOID *IPXCPReturnRouterNamePtr(tGSEM  *pGSEM, UINT1 *OptLen);
VOID *IPXCPReturnRoutingProtoPtr(tGSEM  *pGSEM, UINT1 *OptLen);
INT1 IPXCPProcessNetNumConfRej ( tGSEM *pGSEM );
INT1 IPXCPProcessNodeNumConfRej ( tGSEM *pGSEM );
INT1 IPXCPProcessCompConfRej ( tGSEM *pGSEM );
INT1 IPXCPProcessRoutingProtoConfRej ( tGSEM *pGSEM );
INT1 IPXCPProcessRouterNameConfRej ( tGSEM *pGSEM );

INT1  IPXCPInit(tIPXCPIf *pIf);
tIPXCPIf *IPXCPCreateIf(tPPPIf *pIf);
VOID     IPXCPOptionInit(tIPXCPOptions *pIPXCPOpt);
UINT2    IPXCPConfigInfoLen(tGSEM *pIpxcpGSEM);
VOID     IPXCPUp(tGSEM *pIpxcpGSEM);
VOID     IPXCPDown(tGSEM *pIpxcpGSEM);
VOID     IPXCPFinished(tGSEM *pGSEM);
UINT1    IPXCPRecdConfigAck(tIPXCPIf *, t_MSG_DESC *pInpacket, UINT2     length, UINT1);
UINT1    IPXCPRecdConfigNak(tIPXCPIf *, t_MSG_DESC *pInpacket, UINT2     Length, UINT1);
UINT1    IPXCPRecdConfigRej(tIPXCPIf *, t_MSG_DESC *pInpacket, UINT2     Length, UINT1);
UINT1    IPXCPRecdConfigReq(tIPXCPIf *, t_MSG_DESC *pInpacket, UINT2     Length, UINT1);
UINT1    IPXCPRecdTermReq(tIPXCPIf *, t_MSG_DESC *pInpacket, UINT2     Length, UINT1);
UINT1    IPXCPRecdTermAck(tIPXCPIf *, t_MSG_DESC *pInpacket, UINT2     Length, UINT1);
UINT1    IPXCPRecdCodeRej(tIPXCPIf *, t_MSG_DESC *pInpacket, UINT2     Length, UINT1);

VOID ConfigureIPXCP (tIPXCPIf *pIpxcpPtr);
tIPXCPIf * SNMPGetIPXCPIfPtr(UINT4 Index);
tIPXCPIf * SNMPGetOrCreateIPXCPIfPtr(UINT4 Index);



#endif  /* __PPP_IPXPROTO_H__ */
