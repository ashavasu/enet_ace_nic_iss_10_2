/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipxmidpr.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains Prototypes of Middle Level Routines
 *
 *******************************************************************/
#ifndef __PPP_IPXMIDPR_H__
#define __PPP_IPXMIDPR_H__

/* Prototypes of Middle Level Routines */

VarBind * SNMPIpxEntryGet( OIDType *InDataBasePtr, OIDType *InNamePtr, UINT1 Arg, UINT1 SearchType);
VarBind * SNMPIpxConfigEntryGet( OIDType *InDataBasePtr, OIDType *InNamePtr, UINT1 Arg, UINT1 SearchType);
INT4 SNMPIpxConfigEntryTest( OIDType *InDataBasePtr, OIDType *InNamePtr, UINT1 Arg, MultiDataType *Value);
INT4  SNMPIpxConfigEntrySet( OIDType *InDataBasePtr, OIDType *InNamePtr, UINT1 Arg, MultiDataType *Value);


#endif  /* __PPP_IPXMIDPR_H__ */
