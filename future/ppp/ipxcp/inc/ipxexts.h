/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipxexts.h,v 1.2 2011/10/13 10:31:29 siva Exp $
 *
 * Description:This file contains definitions used by external 
 * modules
 *
 *******************************************************************/
#ifndef __PPP_IPXEXTS_H__
#define __PPP_IPXEXTS_H__


extern   UINT1  TelebitCompAvailable;
extern   UINT1  LocalEntityStatus;
extern   UINT2   IpxcpAllocCounter;
extern   UINT2   IpxcpFreeCounter;
extern   UINT2   IpxcpIfFreeCounter;

extern  tGenOptionInfo          IPXCPGenOptionInfo[];
extern  tOptNegFlagsPerIf       IPXCPOptPerIntf[];
extern  tGSEMCallbacks          IpxcpGSEMCallback;
extern  tIPXCPCtrlBlk           IPXCPCtrlBlk;

INT1 IPXCPCopyOptions ( tGSEM *pGSEM, UINT1 Flag, UINT1 PktType );
tGSEM *IPXCPGetSEMPtr ( tPPPIf *pIf );
INT1    CheckAndGetIpxPtr(tPPPIf *pIf);



#endif  /* __PPP_IPXEXTS_H__ */
