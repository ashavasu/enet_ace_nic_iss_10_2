/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: hdlcprot.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the prototypes for the HDLC functions.
 *
 *******************************************************************/
#ifndef __PPP_HDLCPROT_H__
#define __PPP_HDLCPROT_H__


t_MSG_DESC * FrmAsyncHDLCAppendFCSAndEncode(tAsyncParams *pAsyncParams, t_MSG_DESC *pData, UINT2 *pLength);
t_MSG_DESC * FrmAsyncHDLCDecodeAndVerifyFCS(tAsyncParams *pAsyncParams, t_MSG_DESC *pData, UINT2 *pLength);

#endif  /* __PPP_HDLCPROT_H__ */

UINT4 PPPoSScramble(UINT1*, UINT2*, UINT1*,UINT4,tPPPIf*);
UINT1* PPPoSFCSCalAndAppend(UINT1*,UINT2*,UINT4);
UINT1* PPPoSFCSDecodeAndVerify(UINT1*,UINT2*,UINT4);
UINT1* PPPoSByteStuffing(UINT1*,UINT2 *,tAsyncParams*);
UINT1* PPPoSDebyteStuffing(UINT1*,UINT2 *,tAsyncParams*);
INT4 PPPoSProcessTxPkt(t_MSG_DESC **,UINT2 *,tAsyncParams *,tPPPIf*);
INT4 PPPHandlePPPoSPkt(t_MSG_DESC **,UINT2 *,tAsyncParams *,tPPPIf*);
void PPPSendDataToPPPoS(tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2IfIndex,
                     UINT1 u1Direction, UINT2 u2Protocol);
