/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: frmprots.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the functions prototypes of the framing
 * module
 *
 *******************************************************************/
#ifndef __PPP_FRMPROTS_H__
#define __PPP_FRMPROTS_H__


INT1  FrmGetEncapIndex(UINT1 IfType);

INT1 FrmRxHDLC (BOOLEAN bIsACFCNeg, t_MSG_DESC *pInPDU);
INT1 FrmRxX25(BOOLEAN bIsACFCNeg, t_MSG_DESC *pInPDU);


VOID  FrmTxHDLC(t_MSG_DESC *pOutPDU);

INT1 FrmLLRemoveFramingReq (UINT1 IfType, tAsyncParams *pAsyncParams, BOOLEAN bIsACFCNeg, t_MSG_DESC *pInPkt, UINT2 *pLength);
INT1 FrmAddFramingReq (UINT1 IfType, tAsyncParams *pAsyncParams, BOOLEAN bIsACFC, t_MSG_DESC **pOutPkt, UINT2 *pLength);

VOID  FrmTxVc(t_MSG_DESC *pOutPDU);
INT1 FrmRxVc (BOOLEAN bIsACFCNeg, t_MSG_DESC *pInPDU);
VOID  FrmTxPPPoE(t_MSG_DESC *pOutPDU);
INT1 FrmRxPPPoE (BOOLEAN bIsACFCNeg, t_MSG_DESC *pInPDU);



#endif  /* __PPP_FRMPROTS_H__ */
