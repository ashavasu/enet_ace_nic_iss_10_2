/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: frmdefs.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the constants and macros defined
 *                         by the Framing module.
 *
 *******************************************************************/
#ifndef __PPP_FRMDEFS_H__
#define __PPP_FRMDEFS_H__

#define  ASYNC_IF                  1
#define  SERIAL_IF_TYPE          118
#define  FR_IF_TYPE3               2
#define  X25_IF_TYPE               5
#define  ISDN_IF_TYPE             20

#define  ATM_IF_TYPE             134
#define  LLC_HDR_LEN               4

/* LLC Header fields */
#define  LLC_ROUTED_SAP         0xfe
#define  LLC_CONTROL_UI         0x03
#define  NLPID_PPP              0xcf

/* Error Codes */
#define  BAD_DEST_SAP              4
#define  BAD_SRC_SAP               5
#define  BAD_CONTROL_UI            6
#define  BAD_NLPID_PPP             7

#define  LLC_NLPID_ENCAP           2
#define  ATM_PVC                   1
#define  ATM_SVC                   2


#define  HDLC_ADDR_LEN             2
#define  ALL_STATIONS           0xff
#define  UI                     0x03

#define  BAD_CTRL_FIELD            0
#define  BAD_ADDR_FIELD            1
#define  AC_COMPRESSED             2
#define  AC_NOT_COMPRESSED         3


#define  FRAM_INVALID_IF_TYPE     -2
#define  INVALID_FCS_VALUE        -3
#define  PPP_TEST_PKT              4




#endif  /* __PPP_FRMDEFS_H__ */
