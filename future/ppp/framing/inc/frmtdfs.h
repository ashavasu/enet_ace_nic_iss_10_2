/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: frmtdfs.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the data structures/typedefs 
 *                         defined by the Framing module.
 *
 *******************************************************************/
#ifndef __PPP_FRMTDFS_H__
#define __PPP_FRMTDFS_H__

typedef struct {
        UINT1   IfType;
	UINT1	u1Rsvd1;
	UINT2	u2Rsvd2;
        INT1    (*pDeEncapFnPtr)(BOOLEAN bIsACFCNeg, t_MSG_DESC *pInPDU);
        VOID    (*pEncapFnPtr)(t_MSG_DESC *pOutPDU);
} tEncapTable;

typedef struct AsyncParams {
        UINT1   TxFCSSize;
        UINT1   RxFCSSize;
    UINT2   u2Reserved;
        UINT1   *pTxACCMap;
        UINT1   *pRxACCMap;
} tAsyncParams;


#endif  /* __PPP_FRMTDFS_H__ */
