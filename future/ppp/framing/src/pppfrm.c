/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppfrm.c,v 1.2 2014/03/11 14:02:47 siva Exp $
 *
 * Description:This files contains the interface procedures of the 
 *             Framing module.
 *
 *******************************************************************/

#include "lcpcom.h"
#include "pppdebug.h"
#include "frmdefs.h"
#include "hdlcprot.h"
#include "frmprots.h"
#include "globexts.h"

#define  MAX_IF_TYPES              10
/* alert !!!. changet the value of MAX_IF_TYPES whenevr you are 
 * updating the following array
 * */
static tEncapTable  gEncapTable[] = {
    {SERIAL_IF_TYPE, 0, 0, &FrmRxHDLC, &FrmTxHDLC},
    {SONET_IF_TYPE, 0, 0, &FrmRxHDLC, &FrmTxHDLC},
    {ASYNC_IF_TYPE, 0, 0, &FrmRxHDLC, &FrmTxHDLC},
    {PPTP_IF_TYPE, 0, 0, &FrmRxHDLC, &FrmTxHDLC},
    {ISDN_IF_TYPE, 0, 0, &FrmRxHDLC, &FrmTxHDLC},
    {X25_IF_TYPE, 0, 0, &FrmRxX25, NULL},
    {ATM_IF_TYPE, 0, 0, &FrmRxVc, &FrmTxVc},
    {PPP_OE_IFTYPE, 0, 0, &FrmRxPPPoE, &FrmTxPPPoE},
    {PPP_OE_OUSB_IFTYPE, 0, 0, &FrmRxPPPoE, &FrmTxPPPoE},
    {CFA_PPP, 0, 0, &FrmRxHDLC, &FrmTxHDLC},    /* In case of MP lower layer
                                                   will be PPP */
    {CFA_NONE, 0, 0, &FrmRxHDLC, &FrmTxHDLC}
};

/*********************************************************************
*    Function Name    :    FrmGetEncapIndex
*    Description        :
*                This function determines framing encapsulation table index
*    for a given lower layer interface type.
*    Parameter(s)    :
*            IfType    -    The type lower layer interface.
*    Return Values    :    Framing encapsulation table index for given interface type.
*                        Returns FAILURE for unknown interface types.
*********************************************************************/
INT1
FrmGetEncapIndex (UINT1 IfType)
{
    UINT1               Index;

    for (Index = 0; Index < MAX_IF_TYPES; Index++)
    {
        if (IfType == gEncapTable[Index].IfType)
        {
            return ((INT1) Index);
        }
    }
    return (FAILURE);
}

/*********************************************************************
*    Function Name    :    FrmRxHDLC
*    Description        :
*            This function is used to de-encapsulate the incoming packets
*    when the lower layer interface is a serial/ISDN link.
*    Parameter(s)    :
*        bIsACFCNeg    -    Indicates whether ACFC (Address and Control Field
*                        Compression) has been negotiated succesfully (on rx side).
*            pInPDU    -    Points to the PDU just received. 
*    Return Values    : 
*                    AC_COMPRESSED if address and control fields are compressed,
*                    AC_NOT_COMPRESSED if address and control fields are not compressed,
*                    BAD_ADDR_FIELD if invalid address field (not 0xff),
*                    BAD_CTRL_FIELD if invalid control field (not 0x03).
*********************************************************************/
INT1
FrmRxHDLC (BOOLEAN bIsACFCNeg, t_MSG_DESC * pInPDU)
{
    UINT1               Offset, CharInfo;
    UINT1               ReturnCode;
    Offset = 0;
    /* 
       First byte extracted should be ADDRESS. If it is not a proper address,
       then, check whether  ACFC has been negotiated successfully. If so, 
       then, accept it. Else treate it as BAD.
     */

    GET1BYTE (pInPDU, Offset, CharInfo);

    if (CharInfo != ALL_STATIONS)
    {
        if (bIsACFCNeg == FALSE)
        {
            ReturnCode = BAD_ADDR_FIELD;
        }
        else
        {
            ReturnCode = AC_COMPRESSED;
        }
    }
    else
    {
        Offset++;
        GET1BYTE (pInPDU, Offset, CharInfo);
        if (CharInfo != UI)
        {
            PPP_TRC (ALL_FAILURE, "Error : Invalid control. field");
            ReturnCode = BAD_CTRL_FIELD;
        }
        else
        {
            ReturnCode = AC_NOT_COMPRESSED;
        }
    }
    return ((INT1) ReturnCode);
}

/*********************************************************************
*    Function Name    :    FrmRxX25
*    Description        :
*            This function is used for de-encapsulation when the link is x25.
*    Parameter(s)    :
*        bIsACFCNeg    -    Indicates whether ACFC (Address and Control Field
*                        Compression) has been negotiated succesfully (on rx side).
*            pInPDU    -    Points to the PDU just received. 
*    Return Values    :    AC_COMPRESSED
*********************************************************************/
INT1
FrmRxX25 (BOOLEAN bIsACFCNeg, t_MSG_DESC * pInPDU)
{
    PPP_UNUSED (bIsACFCNeg);
    PPP_UNUSED (pInPDU);
    return (AC_COMPRESSED);
}

/*********************************************************************
*    Function Name    :    FrmTxHDLC
*    Description        :
*    Parameter(s)    :
*            pOutPDU    -    Points to the PDU to be transmitted.
*    Return Values    :    VOID
*********************************************************************/
VOID
FrmTxHDLC (t_MSG_DESC * pOutPDU)
{
    MOVE_BACK_PTR (pOutPDU, HDLC_ADDR_LEN);
    APPEND1BYTE (pOutPDU, ALL_STATIONS);
    APPEND1BYTE (pOutPDU, UI);
    return;
}

/*********************************************************************
*    Function Name    :    FrmTxVc
*    Description        :    This function is used for encapsulation 
*                        when the Lower Layer is ATM.
*    Parameter(s)    :
*            pOutPDU    -    Points to the PDU to be transmitted.
*    Return Values    :    VOID
*********************************************************************/
VOID
FrmTxVc (t_MSG_DESC * pOutPDU)
{
    PPP_UNUSED (pOutPDU);
    return;
}

/*********************************************************************
*    Function Name    :    FrmTxPPPoE
*    Description        :    This function is used for encapsulation 
*                        when the Lower Layer is PPPoE.
*    Parameter(s)    :
*            pOutPDU    -    Points to the PDU to be transmitted.
*    Return Values    :    VOID
*********************************************************************/
VOID
FrmTxPPPoE (t_MSG_DESC * pOutPDU)
{
    PPP_UNUSED (pOutPDU);
    return;
}

/*********************************************************************
*    Function Name    :    FrmRxVc
*    Description        :
*            This function is used to de-encapsulate the incoming packets
*    when the lower layer interface is ATM.
*    Parameter(s)    :
*        bIsACFCNeg    -    Indicates whether ACFC (Address and Control Field
*                        Compression) has been negotiated succesfully (on rx side).
*            pInPDU    -    Points to the PDU just received. 
*    Return Values    : 
*                    AC_COMPRESSED if address and control fields are compressed,
*                    AC_NOT_COMPRESSED if address and control fields are not compressed,
*                    BAD_ADDR_FIELD if invalid address field (not 0xff),
*                    BAD_CTRL_FIELD if invalid control field (not 0x03).
*********************************************************************/
INT1
FrmRxVc (BOOLEAN bIsACFCNeg, t_MSG_DESC * pInPDU)
{
    PPP_UNUSED (bIsACFCNeg);
    PPP_UNUSED (pInPDU);
    return (AC_NOT_COMPRESSED);
}

/*********************************************************************
*    Function Name    :    FrmRxPPPoE
*    Description        :
*            This function is used to de-encapsulate the incoming packets
*    when the lower layer interface is PPPoE.
*    Parameter(s)    :
*        bIsACFCNeg    -    Indicates whether ACFC (Address and Control Field
*                        Compression) has been negotiated succesfully (on rx side).
*            pInPDU    -    Points to the PDU just received. 
*    Return Values    : 
*                    AC_NOT_COMPRESSED 
*********************************************************************/
INT1
FrmRxPPPoE (BOOLEAN bIsACFCNeg, t_MSG_DESC * pInPDU)
{
    PPP_UNUSED (bIsACFCNeg);
    PPP_UNUSED (pInPDU);
    return (AC_NOT_COMPRESSED);
}

/*********************************************************************
*    Function Name    :    FrmLLRemoveFramingReq
*    Description:
*                This function is called by the  Lower Layer Modules  for
*    passing the PPP packet  received. The Framing module forwards the
*    PPP packet after removing CRC, transparency bytes and lower layer
*    framing.
*    Parameter(s):
*        IfType    -    Indicates the lower layer interface type.
*    pAsyncParams-    Points to the structure storing asynchronous link
*                    related variables.
*    bIsACFCNeg    -    Indicates whether ACFC (Address and Control Field
*                    Compression) has been negotiated succesfully (on rx side).
*        pInPkt    -    Points to the received packet.
*        pLength    -    Points to the length of the packet.
*
*    Return Values    :
*        FRAM_INVALID_IF_TYPE        if the given IfType is not supported,
*        PPP_TEST_PKT        if it is a PPP test packet (from protocol simulator),
*        INVALID_FCS_VALUE    if invalid FCS encountered,
*        AC_COMPRESSED        if address and control fields are compressed,
*        AC_NOT_COMPRESSED    if address and control fields are not compressed,
*        BAD_ADDR_FIELD        if invalid address field (not 0xff),
*        BAD_CTRL_FIELD        if invalid control field (not 0x03).
*********************************************************************/
INT1
FrmLLRemoveFramingReq (UINT1 IfType, tAsyncParams * pAsyncParams,
                       BOOLEAN bIsACFCNeg, t_MSG_DESC * pInPkt, UINT2 *pLength)
{
    INT1                DeEncapResult;
    /* Makefile changes - <array subscript has char> */
    INT2                Index;

    PPP_UNUSED (pAsyncParams);
    PPP_UNUSED (pLength);
    /* Makefile changes - <unused parameter> */

    /* De-encapsulate the packet ( remove lower layer headers/footers if any */
    if ((Index = FrmGetEncapIndex (IfType)) == FAILURE)
    {
        return (FRAM_INVALID_IF_TYPE);
    }

    DeEncapResult = (*gEncapTable[Index].pDeEncapFnPtr) (bIsACFCNeg, pInPkt);
    return (DeEncapResult);
}

/*********************************************************************
*    Function Name    :    FrmAddFramingReq
*    Description:
*                This function is invoked by PPP (L2TP) module to forward
*    an outgoing PPP packet to the Framing module. The Framing module
*    inserts CRC, lower layer framing and transparency bytes to the packet.
*    Parameter(s):
*        IfType    -    Indicates the lower layer interface type.
*    pAsyncParams-    Points to the structure storing asynchronous link
*                    related variables.
*        bIsACFC    -    Indicates whether ACFC (Address and Control Field
*                    Compression) has been negotiated succesfully (on tx side).
*    ppOutPkt    -    Points to pointer to the received packet.
*        pLength    -    Points to the length of the packet.
*
*    Return Values    :
*        FRAM_INVALID_IF_TYPE        if the given IfType is not supported,
*        NOT_OK                if any internal erro encountered (like memory failure),
*        OK                    otherwise.
*********************************************************************/
INT1
FrmAddFramingReq (UINT1 IfType, tAsyncParams * pAsyncParams, BOOLEAN bIsACFC,
                  t_MSG_DESC ** pOutPkt, UINT2 *pLength)
{
    INT2                Index;

    PPP_UNUSED (pAsyncParams);
    if ((Index = FrmGetEncapIndex (IfType)) == FAILURE)
    {
        return (FRAM_INVALID_IF_TYPE);
    }

    if ((gEncapTable[Index].pEncapFnPtr != NULL) && (bIsACFC == FALSE))
    {
        (*gEncapTable[Index].pEncapFnPtr) (*pOutPkt);
    }
    *pLength = (UINT2) VALID_BYTES (*pOutPkt);
    return (OK);
}
