/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppphdlc.c,v 1.2 2014/03/11 14:02:47 siva Exp $
 *
 * Description:This file contains the routines for asynchronous 
 *             support to ppp.
 *
 *******************************************************************/

#include "lcpcom.h"
#include "pppdebug.h"
#include "globexts.h"
#include "ppphdlc.h"
#include "hdlcprot.h"

UINT1               gDefaultTxACCM[] = {
    0xff, 0xff, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x06,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};
UINT1               gDefaultRxACCM[] = { 0xff, 0xff, 0xff, 0xff };

/*    
*    This macro checks whether given Octet is flagged in the ACC map or not.
*/
#define MAPPED_IN_ACCM(Octet, pACCMap)        ((pACCMap [Octet/8] & (0x01 << Octet % 8)) ? PPP_YES : PPP_NO)

/*********************************************************************
*    Function Name    :    FrmAsyncHDLCAppendFCSAndEncode
*    Description        :
*                    Function called by LLI to calculate FCS for the incomming
*                    data packet and then append FCS to the incomming packet.
*                    Finally this function escapes the data (including FCS) and
*                    forwards it to Lower Layer.
*    Parameter(s)    :
*        pAsyncParams-    Points to the asynchronous interface related variables.
*            pData    -    Pointer to the incoming packet.
*            pLength    -    Pointer to length of the incoming packet excluding FCS 
*                        size.
*    Return Value    :    VOID    
*********************************************************************/
t_MSG_DESC         *
FrmAsyncHDLCAppendFCSAndEncode (tAsyncParams * pAsyncParams, t_MSG_DESC * pData,
                                UINT2 *pLength)
{
    UINT1               Octet;
    UINT2               Offset;
    UINT4               FCS;
    UINT4               FCSBytes;
    t_MSG_DESC         *pNewData;
    UINT2               NewLength;
    UINT4               BuffSize;

    BuffSize = *pLength;
    Offset = 0;
    NewLength = 0;

    FCSBytes = (UINT4) (pAsyncParams->TxFCSSize / 8);

    BuffSize += FCSBytes;

    BuffSize *= 2;

    if ((pNewData =
         (t_MSG_DESC *) (VOID *) ALLOCATE_BUFFER (BuffSize, 0)) == NULL)
    {
        return (NULL);
    }

    if (pAsyncParams->TxFCSSize == 16)
    {
        FCS = PPPINITFCS16;
        FCS &= 0x0000ffff;
    }
    else
    {
        if (pAsyncParams->TxFCSSize == 32)
        {
            FCS = PPPINITFCS32;
        }
        else
        {
            FCS = 0;
        }
    }
    *pLength = (UINT2) (*pLength + (UINT2) FCSBytes);

    while (*pLength != 0)
    {
        if (*pLength > FCSBytes)
        {
            GET1BYTE (pData, Offset, Octet);
            Offset++;
            if (pAsyncParams->TxFCSSize == 16)
            {
                FCS = (FCS >> 8) ^ FCSTab16[(FCS ^ Octet) & 0xff];
            }
            else
            {
                if (pAsyncParams->TxFCSSize == 32)
                {
                    FCS = (FCS >> 8) ^ FCSTab32[(FCS ^ Octet) & 0xff];
                }
            }
        }
        else
        {
            FCS = (*pLength == FCSBytes) ? (FCS ^ 0xffffffff) : (FCS >> 8);
            Octet = (UINT1) (FCS & 0x00ff);
        }
        if ((Octet == FLAG_SEQ) || (Octet == CTRL_ESC)
            || ((MAPPED_IN_ACCM (Octet, pAsyncParams->pTxACCMap) == PPP_YES)
                && (Octet != 0x5e)))
        {
            ASSIGN1BYTE (pNewData, NewLength, CTRL_ESC);
            NewLength++;
            ASSIGN1BYTE (pNewData, NewLength, Octet ^ 0x20);
            NewLength++;
        }
        else
        {
            ASSIGN1BYTE (pNewData, NewLength, Octet);
            NewLength++;
        }
        (*pLength)--;
    }

    RELEASE_BUFFER (pData);
    *pLength = NewLength;
    return (pNewData);
}

/*********************************************************************
*    Function Name    :    FrmAsyncHDLCDecodeAndVerifyFCS
*    Description        :
*                    Function called by LLI to unescape the incomming
*                    data packet (including FCS). Then this function verifies 
*                    the FCS value. Finally this function detaches FCS and 
*                    forwards the packet to LLI.
*    Parameter(s)    :
*        pAsyncParams-    Points to the asynchronous interface related variables.
*            pData    -    Pointer to the incoming data packet. It also points 
*                        to the final unescaped data.
*            pLength    -    Pointer to length of the incoming packet. It also points
*                        to the length of the final unescaped data. 
*    Return Value    :   returns a pointer to processed incoming data packet
*                       after removing FCS and Async values. 
*********************************************************************/
t_MSG_DESC         *
FrmAsyncHDLCDecodeAndVerifyFCS (tAsyncParams * pAsyncParams, t_MSG_DESC * pData,
                                UINT2 *pLength)
{
    UINT1               Octet;
    UINT2               NewLength;
    UINT2               Offset;
    UINT4               FCS;
    UINT1               Status;

    Offset = 0;
    NewLength = 0;

    if (pAsyncParams->RxFCSSize == 16)
    {
        FCS = PPPINITFCS16;
        FCS &= 0x0000ffff;
    }
    else
    {
        if (pAsyncParams->RxFCSSize == 32)
        {
            FCS = PPPINITFCS32;
        }
        else
        {
            FCS = 0;
        }
    }

    while (*pLength != 0)
    {
        GET1BYTE (pData, Offset, Octet);
        Offset++;
        (*pLength)--;
        if (((Octet < 0x20)
             && (MAPPED_IN_ACCM (Octet, pAsyncParams->pRxACCMap) == PPP_YES)) ==
            NOT_SET)
        {
            if (Octet == FLAG_SEQ)
            {
                break;
            }
            else
            {
                if (Octet == CTRL_ESC)
                {
                    GET1BYTE (pData, Offset, Octet);
                    Offset++;
                    (*pLength)--;
                    if (Octet != FLAG_SEQ)
                    {
                        Octet = Octet ^ 0x20;
                        ASSIGN1BYTE (pData, NewLength, Octet);
                        NewLength++;
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    ASSIGN1BYTE (pData, NewLength, Octet);
                    NewLength++;
                }
            }
        }
        if (pAsyncParams->RxFCSSize == 16)
        {
            FCS = (FCS >> 8) ^ FCSTab16[(FCS ^ Octet) & 0xff];
        }
        else
        {
            if (pAsyncParams->RxFCSSize == 32)
            {
                FCS = (FCS >> 8) ^ FCSTab32[(FCS ^ Octet) & 0xff];
            }
        }
    }

    if (pAsyncParams->RxFCSSize == 16)
    {
        Status = ((FCS == PPPGOODFCS16) ? GOODFCS : BADFCS);
    }
    else
    {
        if (pAsyncParams->RxFCSSize == 32)
        {
            Status = ((FCS == PPPGOODFCS32) ? GOODFCS : BADFCS);
        }
        else
        {
            Status = (pAsyncParams->RxFCSSize == 0) ? GOODFCS : BADFCS;
        }
    }

    if (Status != GOODFCS)
    {
        return (NULL);
    }
    NewLength = (UINT2) (NewLength - (UINT2) (pAsyncParams->RxFCSSize / 8));
    DELETE_BYTES_AT_END (pData, (VALID_BYTES (pData) - NewLength));
    *pLength = NewLength;
    PrintBuffer (pData, NewLength);
    return (pData);
}
