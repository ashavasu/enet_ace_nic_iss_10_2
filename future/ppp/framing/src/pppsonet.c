/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppsonet.c,v 1.5 2014/10/28 11:12:50 siva Exp $
 *
 * Description:This file contains routines of the sonet module.
 *
 *******************************************************************/
#include "lcpcom.h"
#include "ppphdlc.h"
#include "hdlcprot.h"
#include "globexts.h"

#define MAPPED_IN_ACCM(Octet, pACCMap)        ((pACCMap [Octet/8] & (0x01 << Octet % 8)) ? PPP_YES : PPP_NO)

UINT1               BitMapArray[8] =
    { 0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01 };

/***********************************************************************
*  Function Name : PPPoSProcessTxPkt
*  Description   : This procedure processes the ppp packet that is to
*           be sent over sonet interface. 
*  Parameter(s)  :
*         pData        -  pointer to the Buffer containing the data to 
*               be processed.
*      pLength     -  pointer to the Length of the buffer.
*      pAsyncParams  -  pointer to the structure containing the Async
*               parameters.
*         pIf         -  pointer to the PPP interface structure
*  Return Values :     SUCCESS/FAILURE.
*********************************************************************/

INT4
PPPoSProcessTxPkt (t_MSG_DESC ** pData, UINT2 *pLength,
                   tAsyncParams * pAsyncParams, tPPPIf * pIf)
{
    UINT1              *pTempData;
    UINT1              *pOutData;

    pTempData = MEM_MALLOC (*pLength, UINT1);

    COPY_FROM_BUFFER (*pData, pTempData, 0, *pLength);
    if ((pTempData =
         PPPoSFCSCalAndAppend (pTempData, pLength, PPPFCSSIZE32)) != NULL)
    {
        if ((pTempData =
             PPPoSByteStuffing (pTempData, pLength, pAsyncParams)) != NULL)
        {
            if (pIf->pSonetIf->TxScramble == TRUE)
            {
                pOutData = MEM_MALLOC (*pLength, UINT1);
                if (PPPoSScramble (pTempData, pLength, pOutData, SCRAMBLE, pIf)
                    == SUCCESS)
                {
                    RELEASE_BUFFER (*pData);
                    MEM_FREE (pTempData);
                    if ((*pData = PPPAllocateBuffer (*pLength, 0)) != NULL)
                    {
                        COPY_TO_BUFFER (*pData, pOutData, 0, *pLength);
                        MEM_FREE (pOutData);
                        return (SUCCESS);
                    }
                }
                MEM_FREE (pOutData);
            }
            else
            {
                if ((*pData = PPPAllocateBuffer (*pLength, 0)) != NULL)
                {
                    COPY_TO_BUFFER (*pData, pTempData, 0, *pLength);
                    MEM_FREE (pTempData);
                    return (SUCCESS);
                }
                MEM_FREE (pTempData);
            }
        }
    }
    return (FAILURE);
}

/***********************************************************************
*  Function Name : PPPHandlePPPoSPkt
*  Description   : This procedure processes the packet coming from the 
*           sonet interface.
*
*  Parameter(s)  :
*         pData        -  pointer to the Buffer containing the data to 
*               be processed.
*      pLength     -  pointer to the Length of the buffer.
*      pAsyncParams  -  pointer to the structure containing the Async
*               parameters.
*         pIf         -  pointer to the PPP interface structure
*  Return Values :     SUCCESS/FAILURE.
*********************************************************************/

INT4
PPPHandlePPPoSPkt (t_MSG_DESC ** pData, UINT2 *pLength,
                   tAsyncParams * pAsyncParams, tPPPIf * pIf)
{
    UINT1              *pTempData;
    UINT1              *pOutData;

    if ((pTempData = MEM_MALLOC (*pLength, UINT1)) == NULL)
    {
        PPP_TRC (ALL_FAILURE, "Memory Allocation Failed...\n");
        return (FAILURE);
    }

    COPY_FROM_BUFFER (*pData, pTempData, 0, *pLength);
    if (pIf->pSonetIf->RxScramble == TRUE)
    {
        if ((pOutData = MEM_MALLOC (*pLength, UINT1)) == NULL)
        {
            PPP_TRC (ALL_FAILURE, "Memory Allocation Failed...\n");
            MEM_FREE (pTempData);
            return (FAILURE);
        }
        if (PPPoSScramble (pTempData, pLength, pOutData, DESCRAMBLE, pIf) ==
            SUCCESS)
            MEM_FREE (pTempData);
    }
    else
        pOutData = pTempData;

    if ((pTempData =
         PPPoSDebyteStuffing (pOutData, pLength, pAsyncParams)) != NULL)
    {
        if ((pTempData =
             PPPoSFCSDecodeAndVerify (pTempData, pLength,
                                      PPPFCSSIZE32)) != NULL)
        {
            RELEASE_BUFFER (*pData);
            if ((*pData = PPPAllocateBuffer (*pLength, 0)) != NULL)
            {
                COPY_TO_BUFFER (*pData, pTempData, 0, *pLength);
                MEM_FREE (pTempData);
                return (SUCCESS);
            }
        }
    }
    MEM_FREE (pTempData);
    return (FAILURE);
}

/***********************************************************************
*  Function Name : PPPoSFCSCalAndAppend
*  Description   : This procedure calculates the FCS value and appends 
*           it to the existing data.
*  Parameter(s)  :
*    pInData       -    pointer to the array containing the Input Data.
*    pLength       -    pointer to the length of the data.
*    FCSSize       -    parameter indicating the FCS value 
*            size( 16 bit/ 32 bit ) to be calculated.
*  Return Values : 
*          pointer to the newly created array containing the Output Data, 
*    if the memory allocation is success
*      NULL if it memory allocation fails.
*********************************************************************/

UINT1              *
PPPoSFCSCalAndAppend (UINT1 *pInData, UINT2 *pLength, UINT4 FCSSize)
{
    UINT1               Octet;
    UINT2               Offset;
    UINT4               FCS;
    UINT1               FCSBytes;
    UINT1              *pOutData;
    UINT2               NewLength;
    UINT4               BuffSize;

    BuffSize = *pLength;
    Offset = 0;
    NewLength = 0;
    FCSBytes = (UINT1) (FCSSize / 8);
    BuffSize += FCSBytes;

    if ((pOutData = MEM_MALLOC (BuffSize, UINT1)) == NULL)
    {
        PPP_TRC (ALL_FAILURE, "Memory Allocation Failed...\n");
        MEM_FREE (pInData);
        return (NULL);
    }

    if (FCSSize == 16)
    {
        FCS = PPPINITFCS16;
        FCS &= 0x0000ffff;
    }
    else
    {
        FCS = PPPINITFCS32;
    }
    *pLength = (UINT2) (*pLength + (UINT2) FCSBytes);

    while (*pLength != 0)
    {
        if (*pLength > FCSBytes)
        {
            Octet = pInData[Offset++];
            if (FCSSize == 16)
            {
                FCS = (FCS >> 8) ^ FCSTab16[(FCS ^ Octet) & 0xff];
            }
            else
            {
                FCS = (FCS >> 8) ^ FCSTab32[(FCS ^ Octet) & 0xff];
            }
        }
        else
        {
            FCS = (*pLength == FCSBytes) ? (FCS ^ 0xffffffff) : (FCS >> 8);
            Octet = (UINT1) (FCS & 0x00ff);
        }
        pOutData[NewLength] = Octet;
        NewLength++;
        (*pLength)--;
    }
    *pLength = NewLength;
    MEM_FREE (pInData);
    return (pOutData);
}

/***********************************************************************
*  Function Name : PPPoSFCSDecodeAndVerify
*  Description   : This procedure checks if the FCS value is correct and 
*           removes the FCS value from the existing data. 
*  Parameter(s)  :
*    pInData       -    pointer to the array containing the Input Data.
*    pLength       -    pointer to the length of the data.
*    FCSSize       -    parameter indicating the FCS value 
*            size( 16 bit/ 32 bit ) to be checked for.
*  Return Values : 
*          pointer to the newly created array containing the Output Data, 
*    if the memory allocation/FCS check is success
*      NULL if it memory allocation/FCS check fails.

*********************************************************************/

UINT1              *
PPPoSFCSDecodeAndVerify (UINT1 *pInData, UINT2 *pLength, UINT4 FCSSize)
{
    UINT1               Octet;
    UINT2               NewLength;
    UINT2               Offset;
    UINT4               FCS;
    UINT1               Status;
    UINT1              *pOutData;

    Offset = 0;
    NewLength = 0;

    if (FCSSize == 16)
    {
        FCS = PPPINITFCS16;
        FCS &= 0x0000ffff;
    }
    else
    {
        FCS = PPPINITFCS32;
    }

    while (*pLength != 0)
    {
        Octet = pInData[Offset];
        Offset++;
        (*pLength)--;
        NewLength++;

        if (FCSSize == 16)
        {
            FCS = (FCS >> 8) ^ FCSTab16[(FCS ^ Octet) & 0xff];
        }
        else
        {
            FCS = (FCS >> 8) ^ FCSTab32[(FCS ^ Octet) & 0xff];
        }
    }

    if (FCSSize == 16)
    {
        Status = ((FCS == PPPGOODFCS16) ? GOODFCS : BADFCS);
    }
    else
    {
        Status = ((FCS == PPPGOODFCS32) ? GOODFCS : BADFCS);
    }

    if (Status != GOODFCS)
    {
        PPP_TRC (ALL_FAILURE, "Bad FCS Value !!!\n");
        MEM_FREE (pInData);
        return (NULL);
    }

    NewLength = (UINT2) (NewLength - (UINT2) (FCSSize / 8));
    *pLength = NewLength;
    if ((pOutData = MEM_MALLOC ((UINT2) (NewLength + 1), UINT1)) == NULL)
    {
        PPP_TRC (ALL_FAILURE, "Memory Allocation Failed...\n");
        MEM_FREE (pInData);
        return (NULL);
    }
    memcpy (pOutData, pInData, NewLength);
    MEM_FREE (pInData);
    return (pOutData);
}

/***********************************************************************
*  Function Name : PPPoSByteStuffing
*  Description   : This procedure does the byte stuffing on the input data.
*  Parameter(s)  :
*    pInData     -    pointer to the array containing the input data.
*    pLength  -    pointer to the Length of the input data.
*      pAsyncParams  -  pointer to the structure containing the Async
*               parameters.
*  Return Values : 
*          pointer to the newly created array containing the Output Data, 
*    if the memory allocation is success
*      NULL if it memory allocation fails.
*********************************************************************/

UINT1              *
PPPoSByteStuffing (UINT1 *pInData, UINT2 *pLength, tAsyncParams * pAsyncParams)
{
    UINT1               Octet;
    UINT2               Offset;
    UINT1              *pOutData;
    UINT2               NewLength;
    UINT4               BuffSize;

    BuffSize = *pLength;
    Offset = 0;
    NewLength = 1;

    BuffSize *= 2;

    if ((pOutData = MEM_MALLOC ((BuffSize + 2), UINT1)) == NULL)
    {
        PPP_TRC (ALL_FAILURE, "Memory Allocation Failed...\n");
        MEM_FREE (pInData);
        return (NULL);
    }
    pOutData[0] = FLAG_SEQ;

    while (*pLength != 0)
    {
        Octet = pInData[Offset];
        Offset++;
        if ((Octet == FLAG_SEQ) || (Octet == CTRL_ESC)
            || ((MAPPED_IN_ACCM (Octet, pAsyncParams->pTxACCMap) == PPP_YES)
                && (Octet != 0x5e)))
        {
            pOutData[NewLength] = CTRL_ESC;
            NewLength++;
            pOutData[NewLength] = Octet ^ 0x20;
            NewLength++;
        }
        else
        {
            pOutData[NewLength] = Octet;
            NewLength++;
        }
        (*pLength)--;
    }
    pOutData[NewLength] = FLAG_SEQ;
    NewLength++;
    *pLength = NewLength;
    MEM_FREE (pInData);
    return (pOutData);
}

/***********************************************************************
*  Function Name : PPPoSDebyteStuffing
*  Description   : This procedure does the Debyte stuffing on the input data.
*  Parameter(s)  :
*    pInData     -    pointer to the array containing the input data.
*    pLength  -    pointer to the Length of the input data.
*      pAsyncParams  -  pointer to the structure containing the Async
*               parameters.
*  Return Values : 
*          pointer to the newly created array containing the Output Data, 
*    if the memory allocation is success
*      NULL if it memory allocation fails.
*********************************************************************/

UINT1              *
PPPoSDebyteStuffing (UINT1 *pInData, UINT2 *pLength,
                     tAsyncParams * pAsyncParams)
{
    UINT1               Octet;
    UINT2               NewLength;
    UINT2               Offset;
    UINT1              *pOutData;

    Offset = 1;
    NewLength = 0;

    if ((pInData[0] != FLAG_SEQ) || (pInData[*pLength - 1] != FLAG_SEQ))
    {
        MEM_FREE (pInData);
        return (NULL);
    }

    while ((*pLength - 1) != 0)
    {
        Octet = pInData[Offset];
        Offset++;
        (*pLength)--;

        if (((Octet < 0x20)
             && (MAPPED_IN_ACCM (Octet, pAsyncParams->pRxACCMap) == PPP_YES)) ==
            NOT_SET)
        {
            if (Octet == FLAG_SEQ)
            {
                break;
            }
            else
            {
                if (Octet == CTRL_ESC)
                {
                    Octet = pInData[Offset];
                    Offset++;
                    (*pLength)--;
                    if (Octet != FLAG_SEQ)
                    {
                        Octet = Octet ^ 0x20;
                        pInData[NewLength] = Octet;
                        NewLength++;
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    pInData[NewLength] = Octet;
                    NewLength++;
                }
            }
        }
    }
    if ((pOutData = MEM_MALLOC ((UINT2) (NewLength + 1), UINT1)) == NULL)
    {
        PPP_TRC (ALL_FAILURE, "Memory Allocation Failed...\n");
        MEM_FREE (pInData);
        return (NULL);
    }
    memcpy (pOutData, pInData, NewLength);
    MEM_FREE (pInData);
    *pLength = NewLength;
    return (pOutData);
}

/***********************************************************************
*  Function Name : PPPoSScramble
*  Description   : This procedure does the SCRAMBLING/DESCRAMBLING of 
*           the input data.
*  Parameter(s)  :
*    pInData     -    pointer to the array containing the input data.
*    u2Length -    contains the length of the input data.
*    pOutData -    pointer to the array containing the output data.
*    u4Option -    contains the SCRAMBLING/DESCRAMBLING option to be 
*            performed on the input data.
*  Return Values :     SUCCESS/FAILURE.
*********************************************************************/

UINT4
PPPoSScramble (UINT1 *pInData, UINT2 *u2Length, UINT1 *pOutData, UINT4 u4Option,
               tPPPIf * pIf)
{
    UINT4               u4ListHead;
    UINT4               u4ListTail;
    UINT1               u1TempData = 0x00;
    UINT1               u1InData;
    UINT1               u1Bit;
    UINT1              *BitRegister;
    UINT2               i, j;

    u4ListTail = 0;
    u4ListHead = 42;

    if (u4Option == SCRAMBLE)
    {
        BitRegister = pIf->pSonetIf->au1ScrambleRegister;
    }
    else
    {
        BitRegister = pIf->pSonetIf->au1DescrambleRegister;
    }

    for (i = 0; i < *u2Length; i++)
    {
        u1TempData = 0x00;
        u1InData = 0x00;
        u1InData = pInData[i] | u1InData;
        for (j = 1; j <= 8; j++)
        {
            if (u1InData & 0x80)
            {
                u1Bit = 1 ^ BitRegister[u4ListHead];
            }
            else
            {
                u1Bit = 0 ^ BitRegister[u4ListHead];
            }
            u4ListTail = u4ListHead;
            u4ListHead = (u4ListHead + SCRAMBLE_SIZE - 1) % SCRAMBLE_SIZE;
            if (u4Option == SCRAMBLE)
            {
                BitRegister[u4ListTail] = u1Bit;
            }
            if (u4Option == DESCRAMBLE)
            {
                if (u1InData & 0x80)
                {
                    BitRegister[u4ListTail] = 1;
                }
                else
                {
                    BitRegister[u4ListTail] = 0;
                }
            }
            u1InData = (UINT1) (u1InData << 1);
            if (u1Bit == 1)
            {
                u1TempData = u1TempData | BitMapArray[j - 1];
            }
        }
        pOutData[i] = u1TempData;
    }
    *u2Length = i;

    return (SUCCESS);
}

/***********************************************************************
*  Function Name : PPPSendDataToPPPoS
*  Description   :
*          This procedure is used to send the data to the sonet interface.
*  Parameter(s)  :
*    pBuf         -   the pointer to the data buffer to be transmitted
*       u2IfIndex     -   the MIB-2 ifIndex assigned to the interface.
*       u1Direction     -   the direction of the packet transfer 
*       u2Protocol     -   the protocol (the standard PPP protocol ID) 
*                          of the PPP PDU in the buffer.
*  Return Values :  None.
*********************************************************************/

VOID
PPPSendDataToPPPoS (t_MSG_DESC * pBuf, UINT2 u2IfIndex,
                    UINT1 u1Direction, UINT2 u2Protocol)
{
#ifdef CFA_WANTED
    CfaHandlePktFromPpp (pBuf, (UINT4) u2IfIndex, u1Direction, u2Protocol);
#else
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u2IfIndex);
    UNUSED_PARAM (u1Direction);
    UNUSED_PARAM (u2Protocol);
#endif
    return;
}
