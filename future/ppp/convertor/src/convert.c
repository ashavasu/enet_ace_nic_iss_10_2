/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: convert.c,v 1.2 2014/03/11 14:02:46 siva Exp $
 *
 * Description:This file contains the routines of PPP convertor module.
 *
 *******************************************************************/

#include "cvcom.h"
#include "cfa.h"
#include "ppp.h"

/* ACCM default array */
UINT1               CvDefaultTxACCM[] = {
    0xff, 0xff, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};
UINT1               CvDefaultRxACCM[] = { 0xff, 0xff, 0xff, 0xff };
t_SLL               CvIfList;

/*********************************************************************n
*  Function Name : CvCreateIf
*  Description   :
*          This procedure creates an entry in the CV interface data 
*  base . It also initializes the created entry to the 
*  default values  by calling the CVinit() function.
*  Parameter(s)  :
*         pCvIf -    pointer to the CV interface structure
*  Return Values : 
*      pointer to the newly created  interface structure , if the creation is 
*        success
*      NULL if it fails to create the interface.
*********************************************************************/
tCvIf              *
CvCreateIf (INT4 i4SyncIfIndex, INT4 i4AsyncIfIndex)
{
    tCvIf              *pCvIf;

    if ((pCvIf = (tCvIf *) ALLOC_CV_IF ()) != NULL)
    {

        if (CfaIfmSAConvRegisterInterface
            ((UINT2) (i4SyncIfIndex), (UINT2) (i4AsyncIfIndex)) != CFA_SUCCESS)
        {
            FREE_CV_IF (pCvIf);
            return (NULL);
        }
        CvInit (pCvIf);
        pCvIf->u1ConvertorAdminStatus = ADMIN_UP;
        pCvIf->u2SyncIfIndex = (UINT2) i4SyncIfIndex;
        pCvIf->u2AsyncIfIndex = (UINT2) i4AsyncIfIndex;
        SLL_ADD (&CvIfList, (t_SLL_NODE *) pCvIf);
        return (pCvIf);
    }
    else
    {
        return (NULL);
    }
}

/*********************************************************************
*    Function Name    :    CvInit
*    Description        :
*        This function initializes the Cv structure.
*    Parameter(s)    :  
*                pCvIf    -    Points to the Cv structure to be initialized.
*    Return Value    :    VOID    
*********************************************************************/
VOID
CvInit (tCvIf * pCvIf)
{
    MEMCPY (pCvIf->u1ExtTxACCM, CvDefaultTxACCM, TX_ACCM_SIZE);
    MEMCPY (pCvIf->u1AckedRxACCM, CvDefaultRxACCM, RX_ACCM_SIZE);
    CvIndicateAsyncParamsToLL (pCvIf, DEFAULT_ASYNC_VALS);
    pCvIf->u1Sync_Conf_Ack = NOT_SET;
    pCvIf->u1Async_Term_Req = NOT_SET;
    SLL_INIT (&CvIfList);
    return;
}

/*********************************************************************n
*  Function Name : CvDeRegisterInterface
*  Description   :
*          This procedure deletes the entry in the CV interface data 
*  base .
*  Parameter(s)  :
*         pCvIf -    pointer to the CV interface structure
*  Return Values : 
*      Returns SUCCES if deleted, else FAILURE
*********************************************************************/
INT4
CvDeRegisterInterface (tCvIf * pCvIf)
{
    if (CfaIfmSAConvDeRegisterInterface
        (pCvIf->u2SyncIfIndex, pCvIf->u2AsyncIfIndex) != CFA_SUCCESS)
    {
        return (FAILURE);
    }
    else
    {
        return (SUCCESS);
    }
}

/*****************************************************************************
 *                                   
 *    Function Name             : CvHandleIncomingPkt
 *
 *    Description               : This function processes the pkt 
 *                                for CFA and performs the required mapping
 *                                based on the Protocol it receives. 
 *
 *    Input(s)                  : None.
 *
 *    Returns                   : SUCCESS/FAILURE.
 *
 *****************************************************************************/

INT4
CvHandleIncomingPkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                     UINT2 u2IfIndex, UINT4 u4PktSize, UINT2 u2Protocol,
                     UINT1 u1EncapType)
{
    tCvIf              *pCvIf;
    UINT1               u1Code, u1Id;
    UINT2               u2Offset, u2ProtField, u2Len;

    UINT4               u4Length;

    u1EncapType = u1EncapType;
    /* Initialisations */
    u2Offset = 0;
    u2ProtField = 0;
    u4Length = u4PktSize;

/* Get the corresponding CvIf Srtucture */
    if ((pCvIf = GetIfPtr (u2IfIndex, u2Protocol)) == NULL)
    {
        return (FAILURE);
    }

    if ((VALID_BYTES (pBuf)) != u4PktSize)
    {
        /* Discard pkt and return failure */
        return (FAILURE);
    }

    u2Offset = (UINT2) (u2Offset + ((UINT2) HDLC_ADDR_LEN));
    u4Length -= u2Offset;
    CvGetProtocolField (pBuf, &u2ProtField, u2Offset);
    if (u2ProtField == LCP_PROTOCOL)
    {
        u2Offset = (UINT2) (u2Offset + ((UINT2) PROT_LEN));    /* Protocol Field */
        u4Length -= u2Offset;
        if ((CvGetCPHdr (pBuf, u4Length, u2Offset, &u1Code, &u1Id, &u2Len) ==
             NOT_OK))
        {
            /* Invalid Control pkt */
            return (FAILURE);
        }

        u2Offset = (UINT2) (u2Offset + (UINT2) 4);    /* This is for Code,Id and Len */
        switch (u1Code)
        {

            case CONF_REQ_CODE:
                break;

            case CONF_ACK_CODE:
                if (u2Protocol == CFA_HDLC)
                {
                    pCvIf->u1Sync_Conf_Ack = SET;
                    CvUpdateAsyncParams (pCvIf, pBuf, u2Offset);
                }
                break;

            case TERM_REQ_CODE:
                if (u2Protocol == CFA_ASYNC)
                    pCvIf->u1Async_Term_Req = SET;

            case TERM_ACK_CODE:
                break;

            default:
                /* Do nothing and return */
                break;
        }
    }

    else
    {
        if (pCvIf->u1Sync_Conf_Ack == SET)
            CvIndicateAsyncParamsToLL (pCvIf, NEGOTIATED_ASYNC_VALS);
        else
            CvIndicateAsyncParamsToLL (pCvIf, DEFAULT_ASYNC_VALS);
    }
    if (u2Protocol == CFA_HDLC)
    {
        if (CfaGddWrite (pBuf, pCvIf->u2AsyncIfIndex, u4PktSize) != CFA_SUCCESS)
        {
            return (FAILURE);
        }
    }
    else
    {
        if (CfaGddWrite (pBuf, pCvIf->u2SyncIfIndex, u4PktSize) != CFA_SUCCESS)
        {
            return (FAILURE);
        }
    }

    if (pCvIf->u1Async_Term_Req == SET)
    {
        pCvIf->u1Async_Term_Req = NOT_SET;
        CvIndicateAsyncParamsToLL (pCvIf, DEFAULT_ASYNC_VALS);
    }
    return (SUCCESS);

}

/*********************************************************************
*    Function Name        :    CvGetProtocolField
*    Description        :
*        This function finds protocol in the given pkt from Cfa 
*    Parameter(s)    :
*        Protocol    :    The protocol whose index is to be to be found.
*    Return Value        :    Protocol Value 
*********************************************************************/
VOID
CvGetProtocolField (tCRU_BUF_CHAIN_HEADER * pBuf,
                    UINT2 *u2ProtField, UINT2 u2Offset)
{
    UINT1               u1CharInfo;

    /* Extract 1B of the Protocol Field. */
    GET1BYTE (pBuf, u2Offset, u1CharInfo);

    /* If the extracted value is even, then it is a Control Packet */
    if ((u1CharInfo % 2) == 0)
    {
        /* Extract the entire protocol field */
        GET2BYTE (pBuf, u2Offset, *u2ProtField);
        return;
    }
    else
    {
        *u2ProtField = u1CharInfo;
        return;
    }
}

/*********************************************************************
*    Function Name        :    CvGetCPHdr
*    Description        :
*        This function finds Code,Id and Len of thei given pkt from Cfa
*    Parameter(s)    :
*        pBuf    :    The pkt from Cfa from which Code,Id and Len is
*                to be to be found.
*    Return Value    :    OK/NOT_OK    
*********************************************************************/
INT4
CvGetCPHdr (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Length, UINT2 u2Offset,
            UINT1 *u1Code, UINT1 *u1Id, UINT2 *u2Len)
{

    if (u4Length < CP_HDR_LEN)
    {
        /* Rcvd too short header */
        return (NOT_OK);
    }

    GET1BYTE (pBuf, u2Offset, *u1Code);
    GET1BYTE (pBuf, u2Offset + 1, *u1Id);
    GET2BYTE (pBuf, u2Offset + 1, *u2Len);

    *u2Len = (UINT2) (*u2Len - ((UINT2) CP_HDR_LEN));
    return (OK);
}

/***************************************************************************
  Function Name     :          CvIndicateAsyncParamsToLL 
  Description       :  This routine is used to set information on
                       async params based on the negotiated FCS and 
                       asynchronous control character map information.
  Parameters        :
  pCvIf           -  This is the interface(sync/async) , whose  asynchronous 
                       information has to be set.
  Flag          - This indicates whether the Asynchronous parameters
                 are negotiated or not over the pIf.
  return values     : VOID.
***************************************************************************/
VOID
CvIndicateAsyncParamsToLL (tCvIf * pCvIf, UINT1 u1Flag)
{

    if (u1Flag == NEGOTIATED_ASYNC_VALS)
    {
        pCvIf->AsyncParams.pu1TxACCMap = pCvIf->u1AckedTxACCM;
        pCvIf->AsyncParams.pu1RxACCMap = pCvIf->u1AckedRxACCM;
    }
    else
    {
        pCvIf->AsyncParams.pu1TxACCMap = CvDefaultTxACCM;
        pCvIf->AsyncParams.pu1RxACCMap = CvDefaultRxACCM;
    }
    return;
}

/***************************************************************************
  Function Name     :          CvUpdateAsyncParams
  Description       :  This routine is used to update the information on
                       async params based on the negotiated FCS and 
                       asynchronous control character map information.
  Parameters        :
  pCvIf           -  This is the interface(sync/async) , whose  asynchronous 
                       information has to be set.
  pBuf          - The pkt from which ACCM valure needs to be obtained
  return values     : VOID.
***************************************************************************/
VOID
CvUpdateAsyncParams (tCvIf * pCvIf, tCRU_BUF_CHAIN_HEADER * pBuf,
                     UINT2 u2Offset)
{
    UINT1               CharInfo;
    GET1BYTE (pBuf, u2Offset, CharInfo);

    if (CharInfo == ACCM_OPT_VAL)
    {
        u2Offset = (UINT2) (u2Offset + ((UINT2) 2));    /* This is for Type and Length */
        GET1BYTE (pBuf, u2Offset, pCvIf->u1AckedTxACCM[0]);
        GET1BYTE (pBuf, u2Offset + 1, pCvIf->u1AckedTxACCM[1]);
        GET1BYTE (pBuf, u2Offset + 2, pCvIf->u1AckedTxACCM[2]);
        GET1BYTE (pBuf, u2Offset + 3, pCvIf->u1AckedTxACCM[3]);
        MEMCPY (pCvIf->u1ExtTxACCM, pCvIf->u1AckedTxACCM, 4);
    }
    else
    {
        MEMCPY (pCvIf->u1ExtTxACCM, CvDefaultTxACCM, TX_ACCM_SIZE);
    }
    return;
}

/***************************************************************************
  Function Name     :          GetIfPtr
  Description       :  This routine is to get the If ptr from the pCvIfList
  Parameters        :
  i4IfIndex       -  The IfIndex for which the corresponding ptr is to be found
  u2Protocol      -  Sync/Async Protocol
  return values     :  pCvIf ptr.
***************************************************************************/
tCvIf              *
GetIfPtr (INT4 i4IfIndex, UINT2 u2Protocol)
{
    tCvIf              *pCvIf;
    SLL_SCAN (&CvIfList, pCvIf, tCvIf *)
    {
        if (u2Protocol == CFA_HDLC)
        {
            if (pCvIf->u2SyncIfIndex == i4IfIndex)
                return (pCvIf);
        }
        else
        {
            if (pCvIf->u2AsyncIfIndex == i4IfIndex)
                return (pCvIf);
        }
    }
    return (NULL);
}
