/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cvtdfs.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This files contains the type definitions for 
 *			   PPP Convertor
 *
 *******************************************************************/

#ifndef __PPP_CVTDFS_H__
#define __PPP_CVTDFS_H__

#ifdef GLOBAL_VAR
t_SLL	CvIfList; 
UINT1	*pAllocPtr;
#else
extern t_SLL	CvIfList; 
extern UINT1	*pAllocPtr;
#endif

typedef struct {
	UINT1	u1TxFCSSize;
	UINT1	u1RxFCSSize;
	UINT2	u2Reserved1;
	UINT1	*pu1TxACCMap;
	UINT1	*pu1RxACCMap;
}tAsyncParams;

typedef struct {
	t_SLL_NODE NextCvNode;
	UINT2	u2SyncIfIndex;
	UINT2	u2AsyncIfIndex;
	UINT1	u1Sync_Conf_Ack;
	UINT1	u1Async_Term_Req;
	UINT1	u1ConvertorAdminStatus;
	UINT1	u1Rsvd;
	UINT1	u1AckedTxACCM[4];
	UINT1	u1AckedRxACCM[4];
	UINT1	u1ExtTxACCM[32];
	tAsyncParams AsyncParams;
}tCvIf;

#endif
