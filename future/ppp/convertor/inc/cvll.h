/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cvll.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This files contains prototype definitions for low 
 *			   level routines in PPP Convertor
 *
 *******************************************************************/

#ifndef __PPP_CVLL_H__
#define __PPP_CVLL_H__

INT1 nmhValidateIndexInstanceCvTable PROTOTYPE((INT4 i4CvSyncIfIndex , INT4 i4CvAsyncIfIndex));

INT1 nmhGetFirstIndexCvTable PROTOTYPE((INT4 *pi4CvSyncIfIndex , INT4 *pi4CvAsyncIfIndex));

INT1 nmhGetNextIndexCvTable PROTOTYPE((INT4 i4CvSyncIfIndex ,INT4 *pi4NextCvSyncIfIndex , INT4 i4CvAsyncIfIndex ,INT4 *pi4NextCvAsyncIfIndex));

INT1 nmhGetCvRxACCM PROTOTYPE((INT4 i4CvSyncIfIndex, INT4 i4CvAsyncIfIndex,
tSNMP_OCTET_STRING_TYPE *pRetValCvRxACCM));

INT1 nmhGetCvTxACCM PROTOTYPE((INT4 i4CvSyncIfIndex , INT4 i4CvAsyncIfIndex , tSNMP_OCTET_STRING_TYPE *pRetValCvTxACCM));

INT1 nmhGetCvIfAdminStatus PROTOTYPE((INT4 i4CvSyncIfIndex , INT4 i4CvAsyncIfIndex , INT4 *pi4RetValCvIfAdminStatus));

INT1 nmhSetCvIfAdminStatus PROTOTYPE((INT4 i4CvSyncIfIndex , INT4 i4CvAsyncIfIndex , INT4 i4SetValCvIfAdminStatus));

INT1 nmhTestCvIfAdminStatus PROTOTYPE((INT4 i4CvSyncIfIndex , INT4 i4CvAsyncIfIndex , INT4 i4TestValCvIfAdminStatus));
#endif

