/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cvdefs.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the definitions used
 *                         in PPP Convertor
 *
 *******************************************************************/

#ifndef __PPP_CVDEFS_H__        
#define __PPP_CVDEFS_H__
#ifdef __STDC__
#define  PROTOTYPE(x)           x      
#else
#define  PROTOTYPE(x)           ()     
#endif /* __STDC__ */


/* Define lengths for protocol fields */

#define  HDLC_ADDR_LEN          2      

#define  PROT_LEN               2      

#define  CP_HDR_LEN             4      

 
/* #defines for protocol field of different packets */

#define  TX_ACCM_SIZE           32     
#define  RX_ACCM_SIZE           4      

#define  LCP_PROTOCOL           0xc021 

#define  ACCM_OPT_VAL           0x02   

#define  CONF_REQ_CODE          0x01   
#define  CONF_ACK_CODE          0x02   
#define  TERM_REQ_CODE          0x05   
#define  TERM_ACK_CODE          0x06   

#define  DEFAULT_ASYNC_VALS     0      
#define  NEGOTIATED_ASYNC_VALS  1      

#define  ADMIN_UP               1      
#define  ADMIN_DOWN             2      

#define  NOT_SET                0      
#define  SET                    1      
#endif  
