/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cvproto.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the prototype of all the functions
 *			   in PPP Convertor
 *
 *******************************************************************/

#ifndef __PPP_CVPROTO_H__
#define __PPP_CVPROTO_H__


tCvIf *CvCreateIf PROTOTYPE((INT4 i4SyncIfIndex, INT4 i4AsyncIfIndex));

VOID CvInit PROTOTYPE((tCvIf *pCvIf));

INT4 CvDeRegisterInterface PROTOTYPE((tCvIf *pCvIf));

INT4 CvHandleIncomingPacket PROTOTYPE((tCRU_BUF_CHAIN_HEADER  *pBuf,UINT2 u2IfIndex, UINT4 u4PktSize, UINT2 u2Protocol, UINT1 u1EncapType));

VOID CvGetProtocolField PROTOTYPE((tCRU_BUF_CHAIN_HEADER  *pBuf, UINT2 *u2ProtField, UINT2 u2Offset));

INT4 CvGetCPHdr PROTOTYPE((tCRU_BUF_CHAIN_HEADER  *pBuf, UINT4 u4Length, UINT2 u2Offset, UINT1 *u1Code, UINT1 *u1Id, UINT2 *u2Len));

VOID CvIndicateAsyncParamsToLL PROTOTYPE((tCvIf *pCvIf, UINT1 u1Flag));       

VOID CvUpdateAsyncParams PROTOTYPE((tCvIf *pCvIf, tCRU_BUF_CHAIN_HEADER *pBuf, UINT2 u2Offset)); 

tCvIf *GetIfPtr PROTOTYPE((INT4 i4IfIndex, UINT2 u2Protocol));

tCvIf *SNMPGetCvIfPtr PROTOTYPE((INT4 i4SyncIfIndex, INT4 i4AsyncIfIndex));

tCvIf *SNMPGetOrCreateCvIfPtr PROTOTYPE((INT4 i4SyncIfIndex, INT4 i4AsyncIfIndex));

tCvIf *CreateNewCvIf PROTOTYPE((INT4 i4SyncIfIndex, INT4 i4AsyncIfIndex));

#endif

