/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cvmacro.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This files contains the macro definitions for 
 *			   PPP Convertor
 *
 *******************************************************************/

#ifndef __PPP_CVMACRO_H__
#define __PPP_CVMACRO_H__


#define  CALLOC(n, size)      \
          MEM_CALLOC(n, size,void)
#define  FREE(ptr)       MEM_FREE(ptr)

#define   ALLOC_CV_IF()      \
		 (((pAllocPtr = (UINT1 *)CALLOC(1,sizeof(tCvIf)))==NULL)  \
			?  (NULL)  \
			:  (pAllocPtr))

#define FREE_CV_IF(ptr)	FREE(ptr)















#endif
