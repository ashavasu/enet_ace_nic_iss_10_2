/********************************************************************
 * Copyright (C) Future Software Limited, 1997-98,2001
 *
 * $Id: ppplqm.c,v 1.2 2010/12/23 12:21:32 siva Exp $
 *
 * Description:This file contains the routines of LQM module. 
 *
 *******************************************************************/
#include    "lqmcom.h"
#include    "lcpdefs.h"
#include    "globexts.h"

UINT2               LqmIfFreeCounter;
UINT2               LqmFreeCounter;
UINT2               LqmAllocCounter;

/*********************************************************************
*  Function Name :    LQMRecdProtRej
*  Description   :
*        This function is called when the protocol reject is received
*  for the sent LQM packet.
*  Parameter(s)  :  
*        pLqmPtr    -  points to the LQM Interface structure       
*  Return Value :    NULL 
*********************************************************************/
VOID
LQMRecdProtRej (tLQMIf * pLqmPtr)
{
    PPPStopTimer (&pLqmPtr->LqmTimerToSend);
    pLqmPtr->ProtRejRecd = PPP_YES;
    return;
}

/*********************************************************************
*  Function Name :LQMCreate  
*  Description   :
*        This function creates the LQM interface structure and initializes it.
*  It is called by the SNMP interface module when the LQM configuration is SET. 
*  Parameter(s)  :  
*       pIf - points to the PPP interface structure
*  Return Value :
*       NULL - if failed,
*                pointer to the structure - if successful 
*********************************************************************/
tLQMIf             *
LQMCreate (tPPPIf * pIf)
{
    tLQMIf             *pLqmPtr;

    if ((pLqmPtr = (tLQMIf *) (VOID *) ALLOC_LQM_IF ()) == NULL)
    {
        PRINT_MEM_ALLOC_FAILURE;
        return (NULL);
    }

    LQMInit (pLqmPtr);
    pLqmPtr->pIf = pIf;
    return (pLqmPtr);
}

/*********************************************************************
*  Function Name :LQMInit
*  Description   :
*     This function initializes the LQM interface data structure 
*  Parameter(s)  :
*       pIf - points to the PPP interface structure
*  Return Value :
*       NULL - if failed,
*       pointer to the structure - if successful
*********************************************************************/
VOID
LQMInit (tLQMIf * pLqmPtr)
{
    pLqmPtr->LQMStatus.SuccessCounter = 0;
    pLqmPtr->LQMStatus.FailCounter = 0;
    pLqmPtr->LQMStatus.NumReTrials = 0;
    pLqmPtr->LQMStatus.ReportPeriod = 0;
    pLqmPtr->LQMStatus.PeerReportPeriod = 0;
    pLqmPtr->LQMStatus.Status = VALID;

    pLqmPtr->LQMCONFIGS (QualityRate) = QUALITY_RATE;
    pLqmPtr->LQMCONFIGS (MaxFailsAllowed) = MAX_FAILS_ALLOWED;
    pLqmPtr->LQMCONFIGS (LQRsToExamine) = LQRS_TO_EXAMINE;
    pLqmPtr->LQMCONFIGS (MaxReTrials) = MAX_RETRIALS;
    pLqmPtr->LQMCONFIGS (ConfigReportPeriod) = 0;

    pLqmPtr->ProtRejRecd = PPP_NO;
    pLqmPtr->Protocol = LQM_PROTOCOL;

    PPP_INIT_TIMER (pLqmPtr->LqmTimerToSend);
    PPP_INIT_TIMER (pLqmPtr->LqmTimerToExpect);

    return;
}

/*********************************************************************
*  Function Name : LQMSendTimeOut
*  Description   :
*      This function is called when the LQM timers LQM_TO_SEND_TIMER
*  expire.
*  Parameter(s)  :
*     pLqmPtr   -  pointer to the LQM Interface structure    
*  Return Value : VOID
*********************************************************************/
VOID
LQMSendTimeOut (VOID *pLqm)
{
    tLQMIf             *pLqmPtr;
    pLqmPtr = (tLQMIf *) pLqm;

    LQMSend (pLqmPtr);            /*  Send the LQM Statistics  */

    pLqmPtr->LqmTimerToSend.Param1 = PTR_TO_U4 (pLqmPtr);
    PPPStartTimer (&pLqmPtr->LqmTimerToSend, LQM_TO_SEND_TIMER,
                   (UINT4) ((pLqmPtr->LQMStatus.PeerReportPeriod) / 100));
    return;

}

/*********************************************************************
*  Function Name : LQMExpectTimeOut
*  Description   :
*      This function is called when the LQM timers LQM_TO_EXPECT_TIMER expire.
*  Parameter(s)  :
*     pLqmPtr   -  pointer to the LQM Interface structure    
*  Return Value : VOID
*********************************************************************/
VOID
LQMExpectTimeOut (VOID *pLqm)
{
    tLQMIf             *pLqmPtr;
    pLqmPtr = (tLQMIf *) pLqm;
    pLqmPtr->LQMStatus.FailCounter++;

    /* 99070001 -- Return value is checked against BAD  */

    if ((LQMDetermineQuality (pLqmPtr)) != BAD)
    {
        if (pLqmPtr->pIf->LcpGSEM.pNegFlagsPerIf[LQR_IDX].
            FlagMask & ACKED_BY_LOCAL_MASK)
        {
            LQMSend (pLqmPtr);    /*  Send the LQM Statistics  */
        }

        LQMStartExpectTimer (pLqmPtr);
    };
    return;
}

/*********************************************************************
*  Function Name :LQMInput  
*  Description   :
*                    This procedure is called by the LLI when a LQM report is 
*  received from the Peer.It checks the LQR timer value and if it is zero, 
*    then calls the LQMsend function to send back the statistics maintained by 
*  the local entity.If the LQR timer value is non zero, then it passes the 
*  statistics to the Link Manager Function by storing them in the RcvdLqmStat 
*  structure of pLQMIf.
*  Parameter(s)  :  
*        pLqmPtr    - points to the LQM i/f structure
*       pInpacket  - points to the incoming packet
*        Length     - Length of the buffer
*  Return Value : VOID
*********************************************************************/
VOID
LQMInput (tLQMIf * pLqmPtr, t_MSG_DESC * pInpacket, UINT2 Length)
{
    /* Makefile changes - <unused var pRcvdData> */

    UINT4               RcvdMagic;

    if ((pLqmPtr->pIf->LcpGSEM.pNegFlagsPerIf[LQR_IDX].
         FlagMask & ACKED_BY_PEER_MASK) == NOT_SET)
    {
        PPPSendRejPkt (pLqmPtr->pIf, pInpacket, Length, LCP_PROTOCOL,
                       PROT_REJ_CODE);
        return;
    }

    /* Checking for the correct length of the pkt.. */
    if (Length != LQM_PKT_LEN)
    {
        DISCARD_PKT (pLqmPtr->pIf, "LQMInput: rcvd short pkt.");
        return;
    }

    LQMStartExpectTimer (pLqmPtr);

    pLqmPtr->pIf->LLHCounters.InLQRs++;

    MEMCPY (&pLqmPtr->PrevLqmStat, &pLqmPtr->CurrLqmStat, sizeof (tLQMData));

    EXTRACT4BYTE (pInpacket, RcvdMagic);    /* Get the Magic field */

    /*  Checking for the Loop Back */
    if (RcvdMagic == pLqmPtr->pIf->LcpOptionsAckedByPeer.MagicNumber)
    {
        if (RcvdMagic != 0)
        {
            DISCARD_PKT (pLqmPtr->pIf,
                         " Check!! Chances are there for a Loop back...");
            return;
        }
    }
    else
    {
        if (RcvdMagic != pLqmPtr->pIf->LcpOptionsAckedByLocal.MagicNumber)
        {
            DISCARD_PKT (pLqmPtr->pIf,
                         "pkt is discarded : Magic number Problem");
            return;
        }
    }

    /*  Extract the LQR Statistics information from the packet */

    EXTRACT4BYTE (pInpacket, pLqmPtr->CurrLqmStat.LastOutLQRs);
    EXTRACT4BYTE (pInpacket, pLqmPtr->CurrLqmStat.LastOutPackets);
    EXTRACT4BYTE (pInpacket, pLqmPtr->CurrLqmStat.LastOutOctets);

    EXTRACT4BYTE (pInpacket, pLqmPtr->CurrLqmStat.PeerInLQRs);
    EXTRACT4BYTE (pInpacket, pLqmPtr->CurrLqmStat.PeerInPackets);
    EXTRACT4BYTE (pInpacket, pLqmPtr->CurrLqmStat.PeerInDiscards);
    EXTRACT4BYTE (pInpacket, pLqmPtr->CurrLqmStat.PeerInErrors);
    EXTRACT4BYTE (pInpacket, pLqmPtr->CurrLqmStat.PeerInOctets);

    EXTRACT4BYTE (pInpacket, pLqmPtr->CurrLqmStat.PeerOutLQRs);
    EXTRACT4BYTE (pInpacket, pLqmPtr->CurrLqmStat.PeerOutPackets);
    EXTRACT4BYTE (pInpacket, pLqmPtr->CurrLqmStat.PeerOutOctets);

    pLqmPtr->CurrLqmStat.SaveInLQRs = pLqmPtr->pIf->LLHCounters.InLQRs;
    pLqmPtr->CurrLqmStat.SaveInUniPackets =
        pLqmPtr->pIf->LLHCounters.InNUniPackets;
    pLqmPtr->CurrLqmStat.SaveInDiscards = pLqmPtr->pIf->LLHCounters.InDiscards;
    pLqmPtr->CurrLqmStat.SaveInErrors = pLqmPtr->pIf->LLHCounters.InErrors;
    pLqmPtr->CurrLqmStat.SaveInOctets = pLqmPtr->pIf->LLHCounters.InGoodOctets;

    /*  
       Sending the LQM report,if the local entity negotiated to send the same
       and the timer value is zero  PeerInLQR value of current and previous 
       LQR are same
     */

    if (((pLqmPtr->
          pIf->LcpGSEM.pNegFlagsPerIf[LQR_IDX].FlagMask & ACKED_BY_LOCAL_MASK)
         && (PERIOD_ZERO (pLqmPtr->LQMStatus.PeerReportPeriod)))
        || ((pLqmPtr->CurrLqmStat.PeerInLQRs == pLqmPtr->PrevLqmStat.PeerInLQRs)
            && (pLqmPtr->CurrLqmStat.PeerInLQRs != 0)))
    {
        LQMSend (pLqmPtr);
    }

    /* 
       Invoking the Mgr. to study the statistics 
     */

    LQMLinkManager (pLqmPtr);

    return;
}

/*********************************************************************
*  Function Name : LQMSend
*  Description   :
*                This function constructs the LQM packet from the available 
*  statistics for this interface and  forwards it to LLI module.
*  Parameter(s)  :  
*        pLqmPtr -  LQM interface structure for which the LQM packet need to 
*                   be sent
*  Return Value : VOID
*********************************************************************/
VOID
LQMSend (tLQMIf * pLqmPtr)
{
    t_MSG_DESC         *pOutBuf;

    if (pLqmPtr->ProtRejRecd == PPP_YES)
    {
        return;
    }

    if ((pOutBuf =
         (t_MSG_DESC *) (VOID *) ALLOCATE_BUFFER (PPP_INFO_OFFSET + LQM_PKT_LEN,
                                                  PPP_INFO_OFFSET)) == NULL)
    {
        PRINT_MEM_ALLOC_FAILURE;
        return;
    }

    pLqmPtr->pIf->LLHCounters.OutLQRs++;

    /* Changed by BALA : 
       Reason :
       " ....an implementation SHOULD act as if the negotiation had been 
       successful (as if it had instead received a Configure-Ack)......"
     */
    APPEND4BYTE (pOutBuf, pLqmPtr->pIf->LcpOptionsAckedByPeer.MagicNumber);

    APPEND4BYTE (pOutBuf, pLqmPtr->CurrLqmStat.PeerOutLQRs);
    APPEND4BYTE (pOutBuf, pLqmPtr->CurrLqmStat.PeerOutPackets);
    APPEND4BYTE (pOutBuf, pLqmPtr->CurrLqmStat.PeerOutOctets);

    APPEND4BYTE (pOutBuf, pLqmPtr->CurrLqmStat.SaveInLQRs);
    APPEND4BYTE (pOutBuf, pLqmPtr->CurrLqmStat.SaveInUniPackets);
    APPEND4BYTE (pOutBuf, pLqmPtr->CurrLqmStat.SaveInDiscards);
    APPEND4BYTE (pOutBuf, pLqmPtr->CurrLqmStat.SaveInErrors);
    APPEND4BYTE (pOutBuf, pLqmPtr->CurrLqmStat.SaveInOctets);

    APPEND4BYTE (pOutBuf, pLqmPtr->pIf->LLHCounters.OutLQRs);
    APPEND4BYTE (pOutBuf, pLqmPtr->pIf->LLHCounters.OutNUniPackets + 1);
    APPEND4BYTE (pOutBuf,
                 pLqmPtr->pIf->LLHCounters.OutOctets + LQM_PKT_LEN + 4);

    PPPLLITxPkt (pLqmPtr->pIf, pOutBuf, (UINT2) (VALID_BYTES (pOutBuf)),
                 LQM_PROTOCOL);

    return;
}

/*********************************************************************
*  Function Name : LQMLinkManager
*  Description   :
*                This function checks the Link Quality data sent by the peer and 
*      calculates the DELTA value.  It finds the difference between  PeerInpackets 
*      of CurrLqmStat and PrevLqmStat and compares it with the difference between  
*   LastOutPackets of CurrLqmStat and PrevLqmStat to check the quality of the 
*   outgoing link.  It also checks the quality of the incoming link by 
*   comparing the difference between the SaveInUniPacket field of the 
*   CurrLqmStat and PrevLqrSat with the difference between the PeerOutPacket 
*   field of the CurrLqmStat and PrevLqmStat.
*                  If packet loss in both the incoming and  outgoing traffic are 
*   within the acceptable value configured by the SNMP, then the quality of the 
*   link is determined as GOOD.  The link is declared as BAD, if the  
*   statistics of (K) number of LQR packets out of (N) LQR packets indicate 
*   that the quality rate(R)  of the link is not acceptable , where K,N,R are 
*   configurable from the SNMP.
*                  If the quality of the link is found to be BAD, then it 
*   temporiarly  terminates NCP protocols and sends only LQM packets till the 
*   link is restored.Once the link is determined as GOOD, NCP is negotiated 
*   again and the process of transporting  network layer datagrams over the 
*   link continues. It also sends the LQR packet if the PeerInLQR value of 
*   packet recevied and the PeerInLQR field of PrevLqmStat are equal. 
*  Parameter(s)  :  
*            pLqmPtr -  Interface  structure corressponding to the received 
*                       LQM Report
*  Return Value : VOID
*********************************************************************/
void
LQMLinkManager (tLQMIf * pLqmPtr)
{
    UINT4               OutPktLossInPercent = 0;
    UINT4               InPktLossInPercent = 0;

    /* 
       Calculate the Outbound link quality if the LQR negotiated is Full-duplex 
     */

    if (pLqmPtr->CurrLqmStat.PeerInLQRs != 0)
    {

        OutPktLossInPercent =
            LQMFindLossInPercent (pLqmPtr->PrevLqmStat.PeerInPackets,
                                  pLqmPtr->CurrLqmStat.PeerInPackets,
                                  pLqmPtr->PrevLqmStat.LastOutPackets,
                                  pLqmPtr->CurrLqmStat.LastOutPackets);

    }

    if (pLqmPtr->CurrLqmStat.PeerOutLQRs != 1)
    {
        InPktLossInPercent =
            LQMFindLossInPercent (pLqmPtr->PrevLqmStat.SaveInUniPackets,
                                  pLqmPtr->CurrLqmStat.SaveInUniPackets,
                                  pLqmPtr->PrevLqmStat.PeerOutPackets,
                                  pLqmPtr->CurrLqmStat.PeerOutPackets);
    }

    /* Check if the Pkt loss exceeds the threshold configured from SNMP */

    if ((OutPktLossInPercent >
         (UINT4) (100 - pLqmPtr->LQMCONFIGS (QualityRate)))
        || (InPktLossInPercent >
            (UINT4) (100 - pLqmPtr->LQMCONFIGS (QualityRate))))
    {
        pLqmPtr->LQMStatus.FailCounter++;
        PPP_DBG1 ("Fail Counter : %d", pLqmPtr->LQMStatus.FailCounter);
    }
    else
    {
        /*  Increment the No. of LQRs to watch before declaring the Status */
        pLqmPtr->LQMStatus.SuccessCounter++;
        PPP_DBG1 ("FailCounter  : %d", pLqmPtr->LQMStatus.FailCounter);
        PPP_DBG1 ("SuccessCounter  : %d", pLqmPtr->LQMStatus.SuccessCounter);
    }
    LQMDetermineQuality (pLqmPtr);
}

/*********************************************************************
*  Function Name : LQMDetermineQuality
*  Description   :
*     This function determines the quality of the link based on the 
*  LQM Counters and indicates the same to the LCP to take appropriate event
*  Parameter(s)  :
*       pLqmPtr -  LQM interface structure for which the LQM packet need to
*                   be sent
*  Return Value : VOID
*********************************************************************/
/* 99070001 -- Return value is changed from VOID to INT4    */
INT4
LQMDetermineQuality (tLQMIf * pLqmPtr)
{
    int                 status = 0;
    /*
       Check if the No. of failures reaches the Max. allowed value.
       If it reaches the max. vlaue, then trigger DOWN event to 
       all the active NCPs and reset the fail counter . Set the 
       Status as BAD.
     */

    if (pLqmPtr->LQMStatus.FailCounter == pLqmPtr->LQMCONFIGS (MaxFailsAllowed))
    {
        status = LCPIndicateQuality (pLqmPtr, BAD);
    }
    else
    {
        /*  
           If the Fail counter doesn't reach the max. value, then 
           Check whether the LQRsToCheck counter reaches the max. value.
           If so, means that the Link seems to be fine and counters are 
           set to their initial values and the Status is set to GOOD.
         */
        if (pLqmPtr->LQMStatus.SuccessCounter ==
            pLqmPtr->LQMCONFIGS (LQRsToExamine))
        {
            status = LCPIndicateQuality (pLqmPtr, GOOD);
        }
        else
        {
            if (pLqmPtr->LQMStatus.SuccessCounter +
                pLqmPtr->LQMStatus.FailCounter ==
                pLqmPtr->LQMCONFIGS (LQRsToExamine))
            {
                pLqmPtr->LQMStatus.FailCounter = 0;
                pLqmPtr->LQMStatus.SuccessCounter = 0;
            }
        }
    }
    return (status);
}

/*********************************************************************
*  Function Name : LQMStartMonitoring
*  Description   :
*        This function starts the LQM process over a PPP interface
*  Parameter(s)  :
*        pIf    -    pointer to the PPP i/f over which LQM is negotiated
*  Return Value : VOID
*********************************************************************/
VOID
LQMStartMonitoring (tPPPIf * pIf)
{
    UINT4               Period;
    tLQMIf             *pLqmPtr;

    if ((pLqmPtr = pIf->pLqmPtr) != NULL)
    {
        pLqmPtr->ProtRejRecd = PPP_NO;

        if (pIf->LcpGSEM.pNegFlagsPerIf[LQR_IDX].FlagMask & ACKED_BY_PEER_MASK)
        {
            pLqmPtr->LQMStatus.ReportPeriod =
                pLqmPtr->LQMCONFIGS (ConfigReportPeriod);
            if (PERIOD_NON_ZERO (pLqmPtr->LQMStatus.ReportPeriod))
            {
                Period = pLqmPtr->LQMStatus.ReportPeriod;
            }
            else
            {
                Period = pLqmPtr->LQMStatus.PeerReportPeriod;
            }
            /* two round trip intervals */
            Period = 2 * Period;
            pLqmPtr->LqmTimerToExpect.Param1 = PTR_TO_U4 (pLqmPtr);
            PPPStartTimer (&pLqmPtr->LqmTimerToExpect, LQM_TO_EXPECT_TIMER,
                           (UINT4) (Period / 100));
        }

        /* Start the LQM_SEND timer if the LQR Timer is not zero */
        if ((pIf->LcpGSEM.pNegFlagsPerIf[LQR_IDX].
             FlagMask & ACKED_BY_LOCAL_MASK)
            && (PERIOD_NON_ZERO (pLqmPtr->LQMStatus.PeerReportPeriod)))
        {
            pLqmPtr->LqmTimerToSend.Param1 = PTR_TO_U4 (pLqmPtr);
            PPPStartTimer (&pLqmPtr->LqmTimerToSend, LQM_TO_SEND_TIMER,
                           (UINT4) ((pLqmPtr->LQMStatus.PeerReportPeriod) /
                                    100));
        }
    }

    return;
}

/*********************************************************************
*  Function Name : LQMStopMonitoring
*  Description   :
*        This function stop the LQM process over a PPP interface
*  Parameter(s)  :
*        pIf    -    pointer to the PPP i/f over which LQM is negotiated
*  Return Value : VOID
*********************************************************************/
VOID
LQMStopMonitoring (tPPPIf * pIf)
{
    if (pIf->LcpGSEM.pNegFlagsPerIf[LQR_IDX].FlagMask & ACKED_BY_PEER_MASK)
    {
        PPPStopTimer (&pIf->pLqmPtr->LqmTimerToExpect);
    }
    if (pIf->LcpGSEM.pNegFlagsPerIf[LQR_IDX].FlagMask & ACKED_BY_LOCAL_MASK)
    {
        PPPStopTimer (&pIf->pLqmPtr->LqmTimerToSend);
    }
    return;
}

/*********************************************************************
*  Function Name : LQMStartExpectTimer
*  Description   :
*        This function stop the LQM process over a PPP interface
*  Parameter(s)  :
*        pIf    -    pointer to the PPP i/f over which LQM is negotiated
*  Return Value : VOID
*********************************************************************/
VOID
LQMStartExpectTimer (tLQMIf * pLqmPtr)
{
    UINT4               Period;

    pLqmPtr->LqmTimerToExpect.Param1 = PTR_TO_U4 (pLqmPtr);

    if (PERIOD_NON_ZERO (pLqmPtr->LQMStatus.ReportPeriod))
    {
        Period = pLqmPtr->LQMStatus.ReportPeriod;
    }
    else
    {
        Period = pLqmPtr->LQMStatus.PeerReportPeriod;
    }

    Period = 2 * Period;
    PPPRestartTimer (&pLqmPtr->LqmTimerToExpect, LQM_TO_EXPECT_TIMER,
                     (UINT4) (Period / 100));
    return;
}

UINT4
LQMCalculateDiff (UINT4 PrevCounter, UINT4 CurrentCounter)
{
    UINT4               RetVal;

    if (PrevCounter > CurrentCounter)
    {
        RetVal = CurrentCounter + (MAX_COUNTER_VALUE - PrevCounter);
    }
    else
    {
        RetVal = CurrentCounter - PrevCounter;
    }
    return (RetVal);
}

UINT4
LQMFindLossInPercent (UINT4 PrevRecdCounter, UINT4 CurrRecdCounter,
                      UINT4 PrevSentCounter, UINT4 CurrSentCounter)
{
    UINT4               PacketSent,
        PacketRecd, PktLossInPercent = 0, PacketLoss;

    PacketRecd = LQMCalculateDiff (PrevRecdCounter, CurrRecdCounter);

    PacketSent = LQMCalculateDiff (PrevSentCounter, CurrSentCounter);

    /* Modified by Sandeep/Shenoy 27/8/97 */
    if (((PacketLoss = PacketSent - PacketRecd) > 0) && (PacketSent != 0))
    {
        PktLossInPercent = (PacketLoss * 100) / PacketSent;
    }

    return (PktLossInPercent);
}

VOID
LQMDeleteInvalidEntry (tPPPIf * pIf)
{
    /* Free the LQM interface if found invalid */
    if ((pIf->pLqmPtr != NULL) && (pIf->pLqmPtr->LQMStatus.Status == INVALID))
    {
        FREE_LQM_IF (pIf->pLqmPtr);
        pIf->pLqmPtr = NULL;
        pIf->LcpGSEM.pNegFlagsPerIf[LQR_IDX].FlagMask &= DESIRED_NOT_SET;
    }
    return;
}
