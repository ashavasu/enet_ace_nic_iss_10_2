/********************************************************************
 * Copyright (C) Future Software Limited, 1997-98,2001
 *
 * $Id: ppplqrll.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description: Low Level Routines for LQR module.
 *
 *******************************************************************/
#include "lqmcom.h"
#include "snmphdrs.h"
#include "pppsnmpm.h"
#include "llproto.h"
#include "globexts.h"

#ifdef PPP_STACK_WANTED
#include "pppstlow.h"
#include "pplqrlow.h"
#endif

#define LQR_CONFIG_STATUS_DISABLED 1
#define LQR_CONFIG_STATUS_ENABLED 2
extern UINT1        LqrInstance;

INT1
CheckAndGetLqrPtr (tPPPIf * pIf)
{
    if (pIf->pLqmPtr != NULL)
    {
        return (PPP_SNMP_OK);
    }
    return (PPP_SNMP_ERR);
}

#ifdef PPP_STACK_WANTED            /* DSL_ADD */
/****************************************************************************/
tLQMIf             *
SNMPGetOrCreateLQMIf (Index)
     UINT4               Index;
{
    tPPPIf             *pIf;
    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if (pIf->LinkInfo.IfIndex == Index)
        {
            if (pIf->pLqmPtr == NULL)
            {
                if ((pIf->pLqmPtr = LQMCreate (pIf)) == NULL)
                {
                    return (NULL);
                }
            }
            return (pIf->pLqmPtr);
        }
    }
    return (NULL);
}

/* LOW LEVEL Routines for Table : PppLqrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppLqrTable
 Input       :  The Indices
                PppLqrIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppLqrTable (INT4 i4PppLqrIfIndex)
{
    if (PppGetPppIfPtr (i4PppLqrIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppLqrTable
 Input       :  The Indices
                PppLqrIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppLqrTable (INT4 *pi4PppLqrIfIndex)
{
    if (nmhGetFirstIndex (pi4PppLqrIfIndex, LqrInstance, FIRST_INST) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppLqrTable
 Input       :  The Indices
                PppLqrIfIndex
                nextPppLqrIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppLqrTable (INT4 i4PppLqrIfIndex, INT4 *pi4NextPppLqrIfIndex)
{
    if (nmhGetNextIndex (&i4PppLqrIfIndex, LqrInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextPppLqrIfIndex = i4PppLqrIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppLqrQuality
 Input       :  The Indices
                PppLqrIfIndex

                The Object 
                retValPppLqrQuality
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppLqrQuality (INT4 i4PppLqrIfIndex, INT4 *pi4RetValPppLqrQuality)
{

    tPPPIf             *pIf;

    if ((pIf = PppGetPppIfPtr (i4PppLqrIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pIf->LcpGSEM.pNegFlagsPerIf[LQR_IDX].FlagMask & ACKED_BY_PEER_MASK)
    {
        *pi4RetValPppLqrQuality = pIf->LcpStatus.LinkQualityStatus;
        return (SNMP_SUCCESS);
    }

    return (SNMP_FAILURE);

}

/****************************************************************************
 Function    :  nmhGetPppLqrInGoodOctets
 Input       :  The Indices
                PppLqrIfIndex

                The Object 
                retValPppLqrInGoodOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppLqrInGoodOctets (INT4 i4PppLqrIfIndex,
                          UINT4 *pu4RetValPppLqrInGoodOctets)
{

    tPPPIf             *pIf;
    if ((pIf = PppGetPppIfPtr (i4PppLqrIfIndex)) == NULL)
    {
        return ((UINT4) NULL);
    }

    if (pIf->LcpGSEM.pNegFlagsPerIf[LQR_IDX].FlagMask & ACKED_BY_PEER_MASK)
    {
        *pu4RetValPppLqrInGoodOctets = pIf->LLHCounters.InGoodOctets;
        return (SNMP_SUCCESS);
    }

    return (SNMP_FAILURE);

}

/****************************************************************************
 Function    :  nmhGetPppExtLqrInGoodOctets
 Input       :  The Indices
                PppExtLqrIfIndex

                The Object 
                retValPppExtLqrInGoodOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtLqrInGoodOctets (INT4 i4PppExtLqrIfIndex,
                             tSNMP_COUNTER64_TYPE
                             * pu8RetValPppExtLqrInGoodOctets)
{

    tPPPIf             *pIf;
    if ((pIf = PppGetPppIfPtr (i4PppExtLqrIfIndex)) == NULL)
    {
        return ((UINT4) NULL);
    }

    if (pIf->LcpGSEM.pNegFlagsPerIf[LQR_IDX].FlagMask & ACKED_BY_PEER_MASK)
    {
        BYTE8_TO_TWO_BYTE4_CONV (pu8RetValPppExtLqrInGoodOctets,
                                 pIf->LLHCounters.InGoodOctets);
        return (SNMP_SUCCESS);
    }

    return (SNMP_FAILURE);

}

/****************************************************************************
 Function    :  nmhGetPppLqrLocalPeriod
 Input       :  The Indices
                PppLqrIfIndex

                The Object 
                retValPppLqrLocalPeriod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppLqrLocalPeriod (INT4 i4PppLqrIfIndex, INT4 *pi4RetValPppLqrLocalPeriod)
{

    tPPPIf             *pIf;
    tLQMIf             *pLqmPtr;

    if ((pIf = PppGetPppIfPtr (i4PppLqrIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pIf->LcpGSEM.pNegFlagsPerIf[LQR_IDX].FlagMask & ACKED_BY_PEER_MASK)
    {
        pLqmPtr = pIf->pLqmPtr;
        *pi4RetValPppLqrLocalPeriod = pLqmPtr->LQMStatus.ReportPeriod;
        return (SNMP_SUCCESS);
    }

    return (SNMP_FAILURE);

}

/****************************************************************************
 Function    :  nmhGetPppLqrRemotePeriod
 Input       :  The Indices
                PppLqrIfIndex

                The Object 
                retValPppLqrRemotePeriod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppLqrRemotePeriod (INT4 i4PppLqrIfIndex,
                          INT4 *pi4RetValPppLqrRemotePeriod)
{

    tPPPIf             *pIf;
    tLQMIf             *pLqmPtr;

    if ((pIf = PppGetPppIfPtr (i4PppLqrIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pIf->LcpGSEM.pNegFlagsPerIf[LQR_IDX].FlagMask & ACKED_BY_LOCAL_MASK)
    {
        pLqmPtr = pIf->pLqmPtr;
        *pi4RetValPppLqrRemotePeriod = pLqmPtr->LQMStatus.PeerReportPeriod;
        return (SNMP_SUCCESS);
    }

    return (SNMP_FAILURE);

}

/****************************************************************************
 Function    :  nmhGetPppLqrOutLQRs
 Input       :  The Indices
                PppLqrIfIndex

                The Object 
                retValPppLqrOutLQRs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppLqrOutLQRs (INT4 i4PppLqrIfIndex, UINT4 *pu4RetValPppLqrOutLQRs)
{

    tPPPIf             *pIf;

    if ((pIf = PppGetPppIfPtr (i4PppLqrIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pIf->LcpGSEM.pNegFlagsPerIf[LQR_IDX].FlagMask & ACKED_BY_PEER_MASK)
    {
        *pu4RetValPppLqrOutLQRs = pIf->LLHCounters.OutLQRs;
        return (SNMP_SUCCESS);
    }

    return (SNMP_FAILURE);

}

/****************************************************************************
 Function    :  nmhGetPppLqrInLQRs
 Input       :  The Indices
                PppLqrIfIndex

                The Object 
                retValPppLqrInLQRs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppLqrInLQRs (INT4 i4PppLqrIfIndex, UINT4 *pu4RetValPppLqrInLQRs)
{

    tPPPIf             *pIf;

    if ((pIf = PppGetPppIfPtr (i4PppLqrIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pIf->LcpGSEM.pNegFlagsPerIf[LQR_IDX].FlagMask & ACKED_BY_PEER_MASK)
    {
        *pu4RetValPppLqrInLQRs = pIf->LLHCounters.InLQRs;
        return (SNMP_SUCCESS);
    }

    return (SNMP_FAILURE);

}

/****************************************************************************
 Function    :  nmhGetPppExtLqrOutLQRs
 Input       :  The Indices
                PppExtLqrIfIndex

                The Object 
                retValPppExtLqrOutLQRs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtLqrOutLQRs (INT4 i4PppExtLqrIfIndex, tSNMP_COUNTER64_TYPE
                        * pu8RetValPppExtLqrOutLQRs)
{

    tPPPIf             *pIf;

    if ((pIf = PppGetPppIfPtr (i4PppExtLqrIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pIf->LcpGSEM.pNegFlagsPerIf[LQR_IDX].FlagMask & ACKED_BY_PEER_MASK)
    {
        BYTE8_TO_TWO_BYTE4_CONV (pu8RetValPppExtLqrOutLQRs,
                                 pIf->LLHCounters.OutLQRs);
        return (SNMP_SUCCESS);
    }

    return (SNMP_FAILURE);

}

/****************************************************************************
 Function    :  nmhGetPppExtLqrInLQRs
 Input       :  The Indices
                PppExtLqrIfIndex

                The Object 
                retValPppExtLqrInLQRs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtLqrInLQRs (INT4 i4PppExtLqrIfIndex, tSNMP_COUNTER64_TYPE
                       * pu8RetValPppExtLqrInLQRs)
{

    tPPPIf             *pIf;

    if ((pIf = PppGetPppIfPtr (i4PppExtLqrIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pIf->LcpGSEM.pNegFlagsPerIf[LQR_IDX].FlagMask & ACKED_BY_PEER_MASK)
    {
        BYTE8_TO_TWO_BYTE4_CONV (pu8RetValPppExtLqrInLQRs,
                                 pIf->LLHCounters.InLQRs);
        return (SNMP_SUCCESS);
    }

    return (SNMP_FAILURE);

}

/* LOW LEVEL Routines for Table : PppLqrConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppLqrConfigTable
 Input       :  The Indices
                PppLqrConfigIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppLqrConfigTable (INT4 i4PppLqrConfigIfIndex)
{
    if (PppGetPppIfPtr (i4PppLqrConfigIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppLqrConfigTable
 Input       :  The Indices
                PppLqrConfigIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppLqrConfigTable (INT4 *pi4PppLqrConfigIfIndex)
{
    if (nmhGetFirstIndex (pi4PppLqrConfigIfIndex, LqrInstance, FIRST_INST) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppLqrConfigTable
 Input       :  The Indices
                PppLqrConfigIfIndex
                nextPppLqrConfigIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppLqrConfigTable (INT4 i4PppLqrConfigIfIndex,
                                  INT4 *pi4NextPppLqrConfigIfIndex)
{
    if (nmhGetNextIndex (&i4PppLqrConfigIfIndex, LqrInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextPppLqrConfigIfIndex = i4PppLqrConfigIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppLqrConfigPeriod
 Input       :  The Indices
                PppLqrConfigIfIndex

                The Object 
                retValPppLqrConfigPeriod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppLqrConfigPeriod (INT4 i4PppLqrConfigIfIndex,
                          INT4 *pi4RetValPppLqrConfigPeriod)
{

    tPPPIf             *pIf;
    tLQMIf             *pLqmPtr;

    if ((pIf = PppGetPppIfPtr (i4PppLqrConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if ((pLqmPtr = pIf->pLqmPtr) == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValPppLqrConfigPeriod = pLqmPtr->LQMCONFIGS (ConfigReportPeriod);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppLqrConfigStatus
 Input       :  The Indices
                PppLqrConfigIfIndex

                The Object 
                retValPppLqrConfigStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppLqrConfigStatus (INT4 i4PppLqrConfigIfIndex,
                          INT4 *pi4RetValPppLqrConfigStatus)
{

    tPPPIf             *pIf;
    tLQMIf             *pLqmPtr;

    if ((pIf = PppGetPppIfPtr (i4PppLqrConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if ((pLqmPtr = pIf->pLqmPtr) == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValPppLqrConfigStatus = pLqmPtr->LQMStatus.Status;
    return (SNMP_SUCCESS);

}

/* LOW LEVEL Routines for Table : PppLqrExtnsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppLqrExtnsTable
 Input       :  The Indices
                PppLqrExtnsIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppLqrExtnsTable (INT4 i4PppLqrExtnsIfIndex)
{
    if (PppGetPppIfPtr (i4PppLqrExtnsIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppLqrExtnsTable
 Input       :  The Indices
                PppLqrExtnsIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppLqrExtnsTable (INT4 *pi4PppLqrExtnsIfIndex)
{
    if (nmhGetFirstIndex (pi4PppLqrExtnsIfIndex, LqrInstance, FIRST_INST) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppLqrExtnsTable
 Input       :  The Indices
                PppLqrExtnsIfIndex
                nextPppLqrExtnsIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppLqrExtnsTable (INT4 i4PppLqrExtnsIfIndex,
                                 INT4 *pi4NextPppLqrExtnsIfIndex)
{
    if (nmhGetNextIndex (&i4PppLqrExtnsIfIndex, LqrInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextPppLqrExtnsIfIndex = i4PppLqrExtnsIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppLqrExtnsLastReceivedLqrPacket
 Input       :  The Indices
                PppLqrExtnsIfIndex

                The Object 
                retValPppLqrExtnsLastReceivedLqrPacket
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppLqrExtnsLastReceivedLqrPacket (INT4 i4PppLqrExtnsIfIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pRetValPppLqrExtnsLastReceivedLqrPacket)
{

    tLQMIf             *pLqmIf;
    tPPPIf             *pIf;
    UINT1               packet[100];

    if ((pIf = PppGetPppIfPtr (i4PppLqrExtnsIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pLqmIf = pIf->pLqmPtr;
    MEMCPY (packet, (char *) &(pIf->LcpOptionsAckedByLocal.MagicNumber), 4);
    MEMCPY (&(packet[4]), (char *) &(pLqmIf->CurrLqmStat), sizeof (tLQMData));

    /* RFC007 *Str = packet; */
    pRetValPppLqrExtnsLastReceivedLqrPacket->i4_Length = 4 + sizeof (tLQMData);
    MEMCPY (pRetValPppLqrExtnsLastReceivedLqrPacket->pu1_OctetList, packet,
            pRetValPppLqrExtnsLastReceivedLqrPacket->i4_Length);
    return (SNMP_SUCCESS);

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppLqrConfigPeriod
 Input       :  The Indices
                PppLqrConfigIfIndex

                The Object 
                testValPppLqrConfigPeriod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppLqrConfigPeriod (UINT4 *pu4ErrorCode, INT4 i4PppLqrConfigIfIndex,
                             INT4 i4TestValPppLqrConfigPeriod)
{

    PPP_UNUSED (i4PppLqrConfigIfIndex);
    if ((i4TestValPppLqrConfigPeriod < MIN_INTEGER)
        || (i4TestValPppLqrConfigPeriod > MAX_INTEGER))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppLqrConfigStatus
 Input       :  The Indices
                PppLqrConfigIfIndex

                The Object 
                testValPppLqrConfigStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppLqrConfigStatus (UINT4 *pu4ErrorCode, INT4 i4PppLqrConfigIfIndex,
                             INT4 i4TestValPppLqrConfigStatus)
{

    PPP_UNUSED (i4PppLqrConfigIfIndex);
    if ((i4TestValPppLqrConfigStatus != LQR_CONFIG_STATUS_DISABLED)
        && (i4TestValPppLqrConfigStatus != LQR_CONFIG_STATUS_ENABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppLqrConfigPeriod
 Input       :  The Indices
                PppLqrConfigIfIndex

                The Object 
                setValPppLqrConfigPeriod
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppLqrConfigPeriod (INT4 i4PppLqrConfigIfIndex,
                          INT4 i4SetValPppLqrConfigPeriod)
{

    tLQMIf             *pLqmPtr;

    PPP_UNUSED (i4PppLqrConfigIfIndex);
    if ((pLqmPtr = SNMPGetOrCreateLQMIf (i4PppLqrConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pLqmPtr->LQMCONFIGS (ConfigReportPeriod) = i4SetValPppLqrConfigPeriod;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetPppLqrConfigStatus
 Input       :  The Indices
                PppLqrConfigIfIndex

                The Object 
                setValPppLqrConfigStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppLqrConfigStatus (INT4 i4PppLqrConfigIfIndex,
                          INT4 i4SetValPppLqrConfigStatus)
{

    tPPPIf             *pIf;
    tLQMIf             *pLqmPtr;

    PPP_UNUSED (i4PppLqrConfigIfIndex);
    if ((pLqmPtr = SNMPGetOrCreateLQMIf (i4PppLqrConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pLqmPtr->LQMStatus.Status = (UINT1) i4SetValPppLqrConfigStatus;
    pIf = PppGetPppIfPtr (i4PppLqrConfigIfIndex);
    if (i4SetValPppLqrConfigStatus == 2)
    {
        pIf->LcpGSEM.pNegFlagsPerIf[LQR_IDX].FlagMask |= DESIRED_SET;
    }
    else
    {
        pIf->LcpGSEM.pNegFlagsPerIf[LQR_IDX].FlagMask &= DESIRED_NOT_SET;
    }
    return (SNMP_SUCCESS);

}
#endif /* DSL_ADD */
