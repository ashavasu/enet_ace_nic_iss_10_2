/********************************************************************
 * Copyright (C) Future Software Limited, 1997-98,2001
 *
 * $Id: ppplqrel.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This File Contains Low Level Routines for PPPExtLqr GROUP 
 *
 *******************************************************************/

#include "pppsnmpm.h"
#include "lqmcom.h"
#include "snmccons.h"            /* Mani */
#include "llproto.h"
#include "globexts.h"

#ifdef PPP_STACK_WANTED
#include "pplqrlow.h"
#endif
#define MIN_PPP_EXT_LQR_CFG_QUALITY_RATE 0
#define MAX_PPP_EXT_LQR_CFG_QUALITY_RATE 100

extern UINT1        LqrInstance;

#ifdef PPP_STACK_WANTED            /* DSL_ADD */
/* LOW LEVEL Routines for Table : PppExtLqrConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppExtLqrConfigTable
 Input       :  The Indices
                PppExtLqrConfigIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppExtLqrConfigTable (INT4 i4PppExtLqrConfigIfIndex)
{
    if (PppGetPppIfPtr (i4PppExtLqrConfigIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppExtLqrConfigTable
 Input       :  The Indices
                PppExtLqrConfigIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppExtLqrConfigTable (INT4 *pi4PppExtLqrConfigIfIndex)
{
    if (nmhGetFirstIndex (pi4PppExtLqrConfigIfIndex, LqrInstance, FIRST_INST) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppExtLqrConfigTable
 Input       :  The Indices
                PppExtLqrConfigIfIndex
                nextPppExtLqrConfigIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppExtLqrConfigTable (INT4 i4PppExtLqrConfigIfIndex,
                                     INT4 *pi4NextPppExtLqrConfigIfIndex)
{
    if (nmhGetNextIndex (&i4PppExtLqrConfigIfIndex, LqrInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextPppExtLqrConfigIfIndex = i4PppExtLqrConfigIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppExtLqrConfigQualityRate
 Input       :  The Indices
                PppExtLqrConfigIfIndex

                The Object 
                retValPppExtLqrConfigQualityRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtLqrConfigQualityRate (INT4 i4PppExtLqrConfigIfIndex,
                                  INT4 *pi4RetValPppExtLqrConfigQualityRate)
{
    tPPPIf             *pIf;
    tLQMIf             *pLqmPtr;

    if ((pIf = PppGetPppIfPtr (i4PppExtLqrConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pLqmPtr = pIf->pLqmPtr;
    *pi4RetValPppExtLqrConfigQualityRate = pLqmPtr->LQMCONFIGS (QualityRate);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppExtLqrConfigLQRsToExamine
 Input       :  The Indices
                PppExtLqrConfigIfIndex

                The Object 
                retValPppExtLqrConfigLQRsToExamine
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtLqrConfigLQRsToExamine (INT4 i4PppExtLqrConfigIfIndex,
                                    INT4 *pi4RetValPppExtLqrConfigLQRsToExamine)
{
    tPPPIf             *pIf;
    tLQMIf             *pLqmPtr;

    if ((pIf = PppGetPppIfPtr (i4PppExtLqrConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pLqmPtr = pIf->pLqmPtr;
    *pi4RetValPppExtLqrConfigLQRsToExamine =
        pLqmPtr->LQMCONFIGS (LQRsToExamine);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppExtLqrConfigMaxFailsAllowed
 Input       :  The Indices
                PppExtLqrConfigIfIndex

                The Object 
                retValPppExtLqrConfigMaxFailsAllowed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtLqrConfigMaxFailsAllowed (INT4 i4PppExtLqrConfigIfIndex,
                                      INT4
                                      *pi4RetValPppExtLqrConfigMaxFailsAllowed)
{
    tPPPIf             *pIf;
    tLQMIf             *pLqmPtr;

    if ((pIf = PppGetPppIfPtr (i4PppExtLqrConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pLqmPtr = pIf->pLqmPtr;
    *pi4RetValPppExtLqrConfigMaxFailsAllowed =
        pLqmPtr->LQMCONFIGS (MaxFailsAllowed);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppExtLqrConfigMaxReTrials
 Input       :  The Indices
                PppExtLqrConfigIfIndex

                The Object 
                retValPppExtLqrConfigMaxReTrials
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtLqrConfigMaxReTrials (INT4 i4PppExtLqrConfigIfIndex,
                                  INT4 *pi4RetValPppExtLqrConfigMaxReTrials)
{
    tPPPIf             *pIf;
    tLQMIf             *pLqmPtr;

    if ((pIf = PppGetPppIfPtr (i4PppExtLqrConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pLqmPtr = pIf->pLqmPtr;
    *pi4RetValPppExtLqrConfigMaxReTrials = pLqmPtr->LQMCONFIGS (MaxReTrials);
    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppExtLqrConfigQualityRate
 Input       :  The Indices
                PppExtLqrConfigIfIndex

                The Object 
                setValPppExtLqrConfigQualityRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtLqrConfigQualityRate (INT4 i4PppExtLqrConfigIfIndex,
                                  INT4 i4SetValPppExtLqrConfigQualityRate)
{
    tLQMIf             *pLqmPtr;

    if ((pLqmPtr = SNMPGetOrCreateLQMIf (i4PppExtLqrConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pLqmPtr->LQMCONFIGS (QualityRate) =
        (UINT1) i4SetValPppExtLqrConfigQualityRate;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppExtLqrConfigLQRsToExamine
 Input       :  The Indices
                PppExtLqrConfigIfIndex

                The Object 
                setValPppExtLqrConfigLQRsToExamine
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtLqrConfigLQRsToExamine (INT4 i4PppExtLqrConfigIfIndex,
                                    INT4 i4SetValPppExtLqrConfigLQRsToExamine)
{
    tLQMIf             *pLqmPtr;

    if ((pLqmPtr = SNMPGetOrCreateLQMIf (i4PppExtLqrConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pLqmPtr->LQMCONFIGS (LQRsToExamine) =
        (UINT1) i4SetValPppExtLqrConfigLQRsToExamine;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppExtLqrConfigMaxFailsAllowed
 Input       :  The Indices
                PppExtLqrConfigIfIndex

                The Object 
                setValPppExtLqrConfigMaxFailsAllowed
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtLqrConfigMaxFailsAllowed (INT4 i4PppExtLqrConfigIfIndex,
                                      INT4
                                      i4SetValPppExtLqrConfigMaxFailsAllowed)
{
    tLQMIf             *pLqmPtr;

    if ((pLqmPtr = SNMPGetOrCreateLQMIf (i4PppExtLqrConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pLqmPtr->LQMCONFIGS (MaxFailsAllowed) =
        (UINT1) i4SetValPppExtLqrConfigMaxFailsAllowed;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppExtLqrConfigMaxReTrials
 Input       :  The Indices
                PppExtLqrConfigIfIndex

                The Object 
                setValPppExtLqrConfigMaxReTrials
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtLqrConfigMaxReTrials (INT4 i4PppExtLqrConfigIfIndex,
                                  INT4 i4SetValPppExtLqrConfigMaxReTrials)
{
    tLQMIf             *pLqmPtr;

    if ((pLqmPtr = SNMPGetOrCreateLQMIf (i4PppExtLqrConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pLqmPtr->LQMCONFIGS (MaxReTrials) =
        (UINT1) i4SetValPppExtLqrConfigMaxReTrials;
    return (SNMP_SUCCESS);

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppExtLqrConfigQualityRate
 Input       :  The Indices
                PppExtLqrConfigIfIndex

                The Object 
                testValPppExtLqrConfigQualityRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtLqrConfigQualityRate (UINT4 *pu4ErrorCode,
                                     INT4 i4PppExtLqrConfigIfIndex,
                                     INT4 i4TestValPppExtLqrConfigQualityRate)
{
    PPP_UNUSED (i4PppExtLqrConfigIfIndex);
    if (i4TestValPppExtLqrConfigQualityRate < MIN_PPP_EXT_LQR_CFG_QUALITY_RATE
        || i4TestValPppExtLqrConfigQualityRate >
        MAX_PPP_EXT_LQR_CFG_QUALITY_RATE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppExtLqrConfigLQRsToExamine
 Input       :  The Indices
                PppExtLqrConfigIfIndex

                The Object 
                testValPppExtLqrConfigLQRsToExamine
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtLqrConfigLQRsToExamine (UINT4 *pu4ErrorCode,
                                       INT4 i4PppExtLqrConfigIfIndex,
                                       INT4
                                       i4TestValPppExtLqrConfigLQRsToExamine)
{
    PPP_UNUSED (i4PppExtLqrConfigIfIndex);
    if (i4TestValPppExtLqrConfigLQRsToExamine < MIN_INTEGER + 1
        || i4TestValPppExtLqrConfigLQRsToExamine > MAX_INTEGER)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppExtLqrConfigMaxFailsAllowed
 Input       :  The Indices
                PppExtLqrConfigIfIndex

                The Object 
                testValPppExtLqrConfigMaxFailsAllowed
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtLqrConfigMaxFailsAllowed (UINT4 *pu4ErrorCode,
                                         INT4 i4PppExtLqrConfigIfIndex,
                                         INT4
                                         i4TestValPppExtLqrConfigMaxFailsAllowed)
{
    PPP_UNUSED (i4PppExtLqrConfigIfIndex);
    if (i4TestValPppExtLqrConfigMaxFailsAllowed < MIN_INTEGER
        || i4TestValPppExtLqrConfigMaxFailsAllowed > MAX_INTEGER)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppExtLqrConfigMaxReTrials
 Input       :  The Indices
                PppExtLqrConfigIfIndex

                The Object 
                testValPppExtLqrConfigMaxReTrials
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtLqrConfigMaxReTrials (UINT4 *pu4ErrorCode,
                                     INT4 i4PppExtLqrConfigIfIndex,
                                     INT4 i4TestValPppExtLqrConfigMaxReTrials)
{
    PPP_UNUSED (i4PppExtLqrConfigIfIndex);
    if (i4TestValPppExtLqrConfigMaxReTrials < MIN_INTEGER + 1
        || i4TestValPppExtLqrConfigMaxReTrials > MAX_INTEGER)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppExtLqrTable
 Input       :  The Indices
                PppExtLqrIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhValidateIndexInstancePppExtLqrTable (INT4 i4PppExtLqrIfIndex)
{
    PPP_UNUSED (i4PppExtLqrIfIndex);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppExtLqrTable
 Input       :  The Indices
                PppExtLqrIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexPppExtLqrTable (INT4 *pi4PppExtLqrIfIndex)
{
    PPP_UNUSED (*pi4PppExtLqrIfIndex);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppExtLqrTable
 Input       :  The Indices
                PppExtLqrIfIndex
                nextPppExtLqrIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexPppExtLqrTable (INT4 i4PppExtLqrIfIndex,
                               INT4 *pi4NextPppExtLqrIfIndex)
{
    PPP_UNUSED (i4PppExtLqrIfIndex);
    PPP_UNUSED (*pi4NextPppExtLqrIfIndex);
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppExtLqrQualityRate
 Input       :  The Indices
                PppExtLqrIfIndex

                The Object 
                retValPppExtLqrQualityRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtLqrQualityRate (INT4 i4PppExtLqrIfIndex,
                            INT4 *pi4RetValPppExtLqrQualityRate)
{
    PPP_UNUSED (i4PppExtLqrIfIndex);
    PPP_UNUSED (*pi4RetValPppExtLqrQualityRate);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetPppExtLqrLQRsToExamine
 Input       :  The Indices
                PppExtLqrIfIndex

                The Object 
                retValPppExtLqrLQRsToExamine
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtLqrLQRsToExamine (INT4 i4PppExtLqrIfIndex,
                              INT4 *pi4RetValPppExtLqrLQRsToExamine)
{
    PPP_UNUSED (i4PppExtLqrIfIndex);
    PPP_UNUSED (*pi4RetValPppExtLqrLQRsToExamine);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetPppExtLqrMaxFailsAllowed
 Input       :  The Indices
                PppExtLqrIfIndex

                The Object 
                retValPppExtLqrMaxFailsAllowed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtLqrMaxFailsAllowed (INT4 i4PppExtLqrIfIndex,
                                INT4 *pi4RetValPppExtLqrMaxFailsAllowed)
{
    PPP_UNUSED (i4PppExtLqrIfIndex);
    PPP_UNUSED (*pi4RetValPppExtLqrMaxFailsAllowed);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetPppExtLqrMaxReTrials
 Input       :  The Indices
                PppExtLqrIfIndex

                The Object 
                retValPppExtLqrMaxReTrials
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtLqrMaxReTrials (INT4 i4PppExtLqrIfIndex,
                            INT4 *pi4RetValPppExtLqrMaxReTrials)
{
    PPP_UNUSED (i4PppExtLqrIfIndex);
    PPP_UNUSED (*pi4RetValPppExtLqrMaxReTrials);
    return (SNMP_FAILURE);
}
#endif /* DSL_ADD */
