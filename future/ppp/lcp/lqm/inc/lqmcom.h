/********************************************************************
 * Copyright (C) Future Software Limited, 1997-98,2001
 *
 * $Id: lqmcom.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description: This file contains the common LQM include files
 *
 *******************************************************************/
#ifndef __PPP_LQMCOM_H__
#define __PPP_LQMCOM_H__



#include "genhdrs.h"

/* Global PPP includes */
#include "pppmacro.h"
#include "pppdefs.h"
#include "pppdebug.h"
#include "ppptimer.h"
#include "pppdbgex.h"

/* Includes from other modules */
#include "pppgsem.h"
#include "pppgcp.h"
#include "frmtdfs.h"
#include "ppplcp.h"

#include "pppexts.h"

/* Local includes */
#include "ppplqm.h"
#include "lqmprot.h"


#endif  /* __PPP_LQMCOM_H__ */
