/********************************************************************
 * Copyright (C) Future Software Limited, 1997-98,2001
 *
 * $Id: ppplqm.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the typedefs,constants and 
 *             data structures of LQM Module.
 *
 *******************************************************************/
#ifndef __PPP_PPPLQM_H__
#define __PPP_PPPLQM_H__


#define  MAX_COUNTER_VALUE  0xFFFFFFFF 

#define  LQM_PKT_LEN        48         

#define  GOOD               1          
#define  BAD                2          


#define  QUALITY_RATE       100        
#define  MAX_FAILS_ALLOWED  2          
#define  LQRS_TO_EXAMINE    5          
#define  MAX_RETRIALS       5          
#define		LQMCONFIGS(Param)	\
			LQMConfigs.Param 

/*********************************************************************/
/*				  Typedefs used by the LQM Module				  */
/*********************************************************************/

typedef  struct LqmTag{
	UINT4		LastOutLQRs;
	UINT4		LastOutPackets;
	UINT4		LastOutOctets;
	UINT4		PeerInLQRs;
	UINT4		PeerInPackets;
	UINT4		PeerInDiscards;
	UINT4		PeerInErrors;
	UINT4		PeerInOctets;
	UINT4		PeerOutLQRs;
	UINT4		PeerOutPackets;
	UINT4		PeerOutOctets;
	UINT4		SaveInLQRs;
	UINT4		SaveInUniPackets;
	UINT4		SaveInDiscards;
	UINT4		SaveInErrors;
	UINT4		SaveInOctets;
}tLQMData;

typedef		struct   {
	UINT1		Status;
	UINT1		NumReTrials;
	UINT1		FailCounter;
	UINT1		SuccessCounter;
	UINT4		ReportPeriod;	   /* Negotiated Timer to expect LQM   */
	UINT4		PeerReportPeriod;   /* Negotiated Timer to send LQM	 */
}tLQMStatus;

typedef		struct   {
	UINT1		QualityRate;
	UINT1		MaxFailsAllowed;
	UINT1		LQRsToExamine;
	UINT1		MaxReTrials;
	UINT4		ConfigReportPeriod;
}tLQMConfigs;

typedef		struct	lqmif   {
	tPPPIf			*pIf;
	UINT2			Protocol;
	UINT1			ProtRejRecd;
	UINT1			u1Rsvd;
	tLQMData		CurrLqmStat;   /* statistics  of the received LQR packet  */
	tLQMData		PrevLqmStat;   /* statistics  of the previous LQR packet  */
	tLQMStatus		LQMStatus;
	tLQMConfigs		LQMConfigs;
	tPPPTimer		LqmTimerToSend;
	tPPPTimer		LqmTimerToExpect;
}tLQMIf;

/*********************************************************************/


#endif  /* __PPP_PPPLQM_H__ */
