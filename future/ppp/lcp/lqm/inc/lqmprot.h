/********************************************************************
 * Copyright (C) Future Software Limited, 1997-98,2001
 *
 * $Id: lqmprot.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the prototypes for the LQM
 * functions used internally by the LQM module
 *
 *******************************************************************/
#ifndef __PPP_LQMPROT_H__
#define __PPP_LQMPROT_H__


/*   Functions of LQM module   */

VOID     LQMInit(tLQMIf *pLqmPtr);
VOID     GetLQMParameters(tLQMIf *pLqmPtr);
VOID     PrintLQMOptions(tLQMIf *pLqmPtr);
VOID     LQMRecdProtRej(tLQMIf *pLqmPtr);
VOID     LQMInput(tLQMIf *pLqmPtr, t_MSG_DESC *pInpacket, UINT2     Length);
VOID     LQMSend(tLQMIf *pLqmPtr);
VOID     LQMLinkManager(tLQMIf *pLqmPtr);
/* 99070001 -- Return value is changed from VOID to INT4    */
INT4     LQMDetermineQuality(tLQMIf *pLqmPtr);

VOID  LQMDeleteInvalidEntry(tPPPIf  *pIf);
VOID  	LQMStopMonitoring(tPPPIf *pIf);
VOID  	LQMStartMonitoring(tPPPIf *pIf);
VOID  	LQMStartExpectTimer(tLQMIf *pLqmPtr);
UINT4  LQMCalculateDiff(UINT4  PrevCounter, UINT4 CurrentCounter);
UINT4  LQMFindLossInPercent (UINT4 PrevRecdCounter, UINT4 CurrRecdCounter, UINT4 PrevSentCounter, UINT4 CurrSentCounter);

tLQMIf * SNMPGetOrCreateLQMIf(UINT4 Index);
tLQMIf * LQMCreate(tPPPIf *pIf);



#endif  /* __PPP_LQMPROT_H__ */
