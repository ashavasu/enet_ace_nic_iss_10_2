/********************************************************************
 * Copyright (C) Future Software Limited, 1997-98,2001
 *
 * $Id: lqmmidpr.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains prototypes for the MID-LEVEL routines
 *
 *******************************************************************/
#ifndef __PPP_LQMMIDPR_H__
#define __PPP_LQMMIDPR_H__


VarBind * SNMPLqrEntryGet( OIDType *InDataBasePtr, OIDType *InNamePtr, UINT1 Arg, UINT1 SearchType); 
INT4 SNMPLqrConfigEntryTest( OIDType *InDataBasePtr, OIDType *InNamePtr, UINT1 Arg, MultiDataType *Value);
INT4 SNMPLqrConfigEntrySet( OIDType *InDataBasePtr, OIDType *InNamePtr, UINT1 Arg, MultiDataType *Value);
VarBind *SNMPLqrConfigEntryGet( OIDType *InDataBasePtr, OIDType *InNamePtr, UINT1 Arg, UINT1 SearchType);
VarBind * SNMPLinkExtnsEntrySet( OIDType *InDataBasePtr, OIDType *InNamePtr, UINT1 Arg, MultiDataType *Value);
OctetStringType * SNMPFormLQRPacket(UINT4 Index);


#endif  /* __PPP_LQMMIDPR_H__ */
