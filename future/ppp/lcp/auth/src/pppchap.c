/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppchap.c,v 1.7 2014/10/30 12:31:54 siva Exp $
 *
 * Description:This file contains interface and internal 
 *             procedures of CHAP module.
 *
 *******************************************************************/

#include "authcom.h"
#include "pppmd5.h"
#include "lcpdefs.h"
#include "lcpexts.h"
#include "gcpdefs.h"
#include "gsemdefs.h"
#include "globexts.h"
#include "pppl2tp.h"
#include "pppcom.h"
#include "genproto.h"

/* #ifdef L2TP_WANTED */
#include "l2tp.h"
/* #endif */

extern tGSEM       *pTermGSEM;
/*********************************************************************
*  Function Name    :    CHAPClientInit
*  Description        :
*                This function initialises the ClientChap structure and
*    is invoked by the AuthInitServices() function. It sets the time out 
*    value and maximam retransmission to their default values and  sets 
*    the client state to INITIAL .
*    Parameter(s)    :
*        pAuthPtr    -    Points to the authentication interface structure 
*                        which has the ClientChap structure to be initialized.
*    Return Values    :    VOID
*********************************************************************/
VOID
CHAPClientInit (tAUTHIf * pAuthPtr)
{
    UINT1               RespLen;

    pAuthPtr->ClientInfo.ClientChap.pCHAPClientSecret = NULL;

    pAuthPtr->ClientInfo.ClientChap.pChallenge = NULL;    /* Required for MSCHAP */

    RespLen =
        (pAuthPtr->ClientProt ==
         CHAP_PROTOCOL) ? MD5_SIGNATURE_SIZE : MSCHAP_RESPONSE_LENGTH;

    if ((pAuthPtr->ClientInfo.ClientChap.pResponse = ALLOC_STR (RespLen)) ==
        NULL)
    {
        PRINT_MEM_ALLOC_FAILURE;
        return;
    }
    pAuthPtr->ClientInfo.ClientChap.pMSCHAPChgPwdPkt = NULL;

    pAuthPtr->ClientInfo.ClientChap.State = CHAP_CLIENT_STATE_INITIAL;
    pAuthPtr->ClientInfo.ClientChap.TimeoutTime =
        pAuthPtr->pIf->LinkInfo.TimeOutTime;
    pAuthPtr->ClientInfo.ClientChap.MaxTransmits =
        pAuthPtr->pIf->LinkInfo.MaxReTransmits;

    return;
}

/*********************************************************************
*    Function Name    :    CHAPServerInit
*    Description        :
*                This function initialises the ServerChap structure and
*    is invoked by the AuthInitServices() function. It sets the time out 
*    value and maximam retransmission to their default values and  sets 
*    the client state to INITIAL .
*    Parameter(s)    :
*        pAuthPtr    -    Points to the authentication interface structure 
*                        which has the ServerChap structure to be initialized.
*    Return Values    :    VOID
*********************************************************************/
VOID
CHAPServerInit (tAUTHIf * pAuthPtr)
{

    pAuthPtr->ServerInfo.ServerChap.pCHAPCurrentSecret = NULL;
    pAuthPtr->ServerInfo.ServerChap.State = CHAP_SERVER_STATE_INITIAL;
    pAuthPtr->ServerInfo.ServerChap.TimeoutTime =
        pAuthPtr->pIf->LinkInfo.TimeOutTime;
    pAuthPtr->ServerInfo.ServerChap.MaxTransmits =
        pAuthPtr->pIf->LinkInfo.MaxReTransmits;
    pAuthPtr->ServerInfo.ServerChap.ChallengeInterval =
        pAuthPtr->pIf->LinkInfo.ReChalTimeOut;
    pAuthPtr->ChapLastSecretIndex = INIT_VAL;
    pAuthPtr->CHAPSearchList = IF_SPECIFIC_LIST;
    pAuthPtr->ServerInfo.ServerChap.ChallengeTransmits = 0;

    return;
}

/*********************************************************************
*    Function Name    :    CHAPAuthWithPeer
*    Description        :
*                This function is called by the PPP Authentication Module for
*    authenticating the local entity with the remote. It sets the client state 
*    of CHAPState structure to LISTEN and waits for a Challenge from  Peer. This
*    function is called only if the authentication protocol negotiated is CHAP.
*    Parameter(s)    :
*        pAuthPtr    -    Pointer to the CHAP interface structure for which the
*                       authentication  need to be done
*    Return Values    :    VOID
*********************************************************************/
VOID
CHAPAuthWithPeer (tAUTHIf * pAuthPtr)
{

    if (pAuthPtr->ClientInfo.ClientChap.State == CHAP_CLIENT_STATE_INITIAL
        || pAuthPtr->ClientInfo.ClientChap.State == CHAP_CLIENT_STATE_PENDING)
    {
        pAuthPtr->ClientInfo.ClientChap.State = CHAP_CLIENT_STATE_PENDING;
        /* wait until the lower layer is up */
    }
    else
    {
        pAuthPtr->ClientInfo.ClientChap.State = CHAP_CLIENT_STATE_LISTEN;
    }
    return;
}

/*********************************************************************
*    Function Name    :    CHAPAuthPeer
*    Description        :
*                    This function is called by the PPP Authentication Module 
*    for authenticating the Peer. The CHAP server state is set to PENDING if 
*    it is in the INITIAL or PENDING state. Otherwise, a challenge is generated 
*    and sent to the peer. This function is called only if the authentication 
*    protocol negotiated is CHAP.
*    Parameter(s)    :
*        pAuthPtr    -    Points to the interface structure for which the CHAP
*                        authentication  need to be done.
*    Return Values    :    VOID
*********************************************************************/
VOID
CHAPAuthPeer (tAUTHIf * pAuthPtr)
{

    if (pAuthPtr->ServerInfo.ServerChap.State == CHAP_SERVER_STATE_INITIAL
        || pAuthPtr->ServerInfo.ServerChap.State == CHAP_SERVER_STATE_PENDING)
    {
        pAuthPtr->ServerInfo.ServerChap.State = CHAP_SERVER_STATE_PENDING;
    }
    else
    {
        CHAPGenChallenge (pAuthPtr);
        CHAPSendChallenge (pAuthPtr);
        pAuthPtr->ServerInfo.ServerChap.State = CHAP_SERVER_STATE_INITIAL_CHAL;
    }
    return;
}

/*********************************************************************
*    Function Name    :    CHAPChalTimeOut
*    Description        :
*                    This function is called when the retransmission counter
*    expires after sending a CHAP request packet. It calls the appropriate
*    function to retransmit the packet.
*    Parameter(s)    :
*        pAuthPtr    -  Pointer to the authentication interface structure.
*    Return Values    :    VOID
*********************************************************************/
VOID
CHAPChalTimeOut (VOID *pAuth)
{

    tAUTHIf            *pAuthPtr;
    pAuthPtr = (tAUTHIf *) pAuth;
    /*  Check whether the time out occurs after sending a challenge/
       re-challenge request packet 
     */

    if ((pAuthPtr->ServerInfo.ServerChap.State !=
         CHAP_SERVER_STATE_INITIAL_CHAL)
        && (pAuthPtr->ServerInfo.ServerChap.State !=
            CHAP_SERVER_STATE_RECHALLENGE))
    {
        return;
    }

    if (pAuthPtr->ServerInfo.ServerChap.ChallengeTransmits >=
        pAuthPtr->ServerInfo.ServerChap.MaxTransmits)
    {

        PPP_TRC (ALL_FAILURE, "Peer failed to respond to CHAP ChallengeValue");

        pAuthPtr->ServerInfo.ServerChap.State = CHAP_SERVER_STATE_BADAUTH;
        LCPAuthProcessFailed (pAuthPtr->pIf);
        if (pTermGSEM != NULL)
        {
            GSEMRun (pTermGSEM, RXJ_MINUS);
            pTermGSEM = NULL;
        }
    }
    else
    {

        CHAPGenChallenge (pAuthPtr);

        /* Re-send ChallengeValue */
        if (pAuthPtr->ServerProt == CHAP_PROTOCOL)
            CHAPSendChallenge (pAuthPtr);
        else
            MSCHAPSendChallenge (pAuthPtr);
    }
    return;
}

/*********************************************************************
*    Function Name    :    CHAPRespTimeOut
*    Description        :
*            This procedure is used  to retransmit the Challenge response 
*    packet when a CHAP response time out occurs.
*    Parameter(s)    :
*        pAuthPtr    -    Pointer to the authentication interface structure.
*    Return Values    :    VOID
*********************************************************************/
VOID
CHAPRespTimeOut (VOID *pAuth)
{
    tAUTHIf            *pAuthPtr;
    pAuthPtr = (tAUTHIf *) pAuth;
    /*  Check whether the time out occurs after sending a response packet */
    if (pAuthPtr->ClientInfo.ClientChap.State == CHAP_CLIENT_STATE_RESPONSE)
    {

        if (pAuthPtr->ClientInfo.ClientChap.ResponseTransmits >=
            pAuthPtr->ClientInfo.ClientChap.MaxTransmits)
        {

            PPP_TRC (ALL_FAILURE,
                     "Peer failed to respond to CHAP Challenge Response");
            pAuthPtr->ClientInfo.ClientChap.State = CHAP_CLIENT_STATE_BADAUTH;
#ifdef RADIUS_WANTED
            pAuthPtr->pIf->RadInfo.RadReqId = INIT_VAL;
#endif
        }
        else
        {
            /* re-send Response */
            CHAPSendResponse (pAuthPtr);
        }
    }
    return;
}

/*********************************************************************
*    Function Name    :    CHAPReChalTimeOut
*    Description        :
*                This procedure is used to send the Challenge after every
*    ChallengeInterval time.
*    Parameter(s)    :
*        pAuthPtr     -    Pointer to the authentication interface structure.
*    Return Values    :    VOID
*********************************************************************/
VOID
CHAPReChalTimeOut (VOID *pAuth)
{
    tAUTHIf            *pAuthPtr;
    pAuthPtr = (tAUTHIf *) pAuth;
    /* Check if time out has occured in the server open state */
    if (pAuthPtr->ServerInfo.ServerChap.State == CHAP_SERVER_STATE_OPEN)
    {
        pAuthPtr->ServerInfo.ServerChap.State = CHAP_SERVER_STATE_RECHALLENGE;
        if (pAuthPtr->ServerProt == CHAP_PROTOCOL)
        {
            pAuthPtr->ServerInfo.ServerChap.pCHAPCurrentSecret = NULL;
            CHAPGenChallenge (pAuthPtr);
            CHAPSendChallenge (pAuthPtr);
        }
        else
        {
            /* Stuff related to MSCHAP to be done */
            pAuthPtr->ServerInfo.ServerChap.pCHAPCurrentSecret = NULL;
            MSCHAPGenChallenge (pAuthPtr);
            MSCHAPSendChallenge (pAuthPtr);
        }
    }
    return;
}

/*********************************************************************
*    Function Name    :    CHAPRecdProtRej
*    Description        :
*        This procedure is used  to process a protocol reject received for
*    the CHAP protocol.
*    Parameter(s)    :
*        pAuthPtr    -    Pointer to the CHAP interface structure.
*    Return Values    :    VOID
*********************************************************************/
VOID
CHAPRecdProtRej (tAUTHIf * pAuthPtr)
{

    if ((pAuthPtr->ServerProt == CHAP_PROTOCOL)
        || (pAuthPtr->ServerProt == MSCHAP_PROTOCOL))
    {
        if ((pAuthPtr->ServerInfo.ServerChap.State != CHAP_SERVER_STATE_INITIAL)
            && (pAuthPtr->ServerInfo.ServerChap.State !=
                CHAP_SERVER_STATE_CLOSED))
            LCPAuthProcessFailed (pAuthPtr->pIf);
    }

    AUTHCloseServer (pAuthPtr);
    AUTHCloseClient (pAuthPtr);

    return;
}

/*********************************************************************
*    Function Name    :    CHAPInput
*    Description        :
*                This procedure is called by the LLI whenever there is an
*    incoming packet for CHAP module. It processes the CHAP packet and calls 
*    the internal functions  to take appropriate actions.
*    Parameter(s)    :
*        pAuthPtr    -    Points to the CHAP interface structure corresponding 
                        to the incoming packet
*        pInpacket    -    Points to the incoming packet
*            Length    -    Length of the incoming packet.
*    Return Values    :    VOID
*********************************************************************/
VOID
CHAPInput (tAUTHIf * pAuthPtr, t_MSG_DESC * pInpacket, UINT2 Length)
{
    UINT1               Code = 0;
    UINT1               Id = 0;
    UINT2               Len;

    if (UtilExtractAndValidateCPHdr
        (pAuthPtr->pIf, pInpacket, Length, &Code, &Id, &Len) == DISCARD)
    {
        return;
    }

    switch (Code)
    {

        case CHAP_CHALLENGE_REQ:

            if ((pAuthPtr->pIf->LcpGSEM.pNegFlagsPerIf[AUTH_IDX].FlagMask
                 & ACKED_BY_LOCAL_MASK) == 0)
                return;

            if (pAuthPtr->ClientProt == CHAP_PROTOCOL)
            {
                CHAPRcvdChallenge (pAuthPtr, pInpacket, Len, Id);

            }
            else
            {
                if (pAuthPtr->ClientProt == MSCHAP_PROTOCOL)
                {
                    MSCHAPRcvdChallenge (pAuthPtr, pInpacket, Len, Id);
                }
                else
                {
                    PPPSendRejPkt (pAuthPtr->pIf, pInpacket, Length,
                                   LCP_PROTOCOL, PROT_REJ_CODE);
                }
            }
            break;

        case CHAP_CHALLENGE_RESP:

            if ((pAuthPtr->pIf->LcpGSEM.pNegFlagsPerIf[AUTH_IDX].FlagMask
                 & ACKED_BY_PEER_MASK) == 0)
                return;

            if (pAuthPtr->ServerProt == CHAP_PROTOCOL)
            {
                CHAPRcvdAuthResponse (pAuthPtr, pInpacket, Len, Id);
            }
            else
            {
                if (pAuthPtr->ServerProt == MSCHAP_PROTOCOL)
                {
                    MSCHAPRcvdAuthResponse (pAuthPtr, pInpacket, Len, Id);
                }
                else
                {
                    PPPSendRejPkt (pAuthPtr->pIf, pInpacket, Length,
                                   LCP_PROTOCOL, PROT_REJ_CODE);
                }
            }
            break;

        case CHAP_FALSE:

            if ((pAuthPtr->pIf->LcpGSEM.pNegFlagsPerIf[AUTH_IDX].FlagMask
                 & ACKED_BY_LOCAL_MASK) == 0)
                return;

            if (pAuthPtr->ClientProt == CHAP_PROTOCOL)
            {
                CHAPRcvdAuthFailure (pAuthPtr, pInpacket, Len, Id);
            }
            else
            {
                if (pAuthPtr->ClientProt == MSCHAP_PROTOCOL)
                {
                    MSCHAPRcvdAuthFailure (pAuthPtr, pInpacket, Len, Id);
                }
                else
                {
                    PPPSendRejPkt (pAuthPtr->pIf, pInpacket, Length,
                                   LCP_PROTOCOL, PROT_REJ_CODE);
                }
            }
            break;

        case CHAP_TRUE:

            if ((pAuthPtr->pIf->LcpGSEM.pNegFlagsPerIf[AUTH_IDX].FlagMask
                 & ACKED_BY_LOCAL_MASK) == 0)
                return;

            if (pAuthPtr->ClientProt == CHAP_PROTOCOL)
            {
                CHAPRcvdAuthSuccess (pAuthPtr, pInpacket, Len, Id);
            }
            else
            {
                if (pAuthPtr->ClientProt == MSCHAP_PROTOCOL)
                {
                    MSCHAPRcvdAuthSuccess (pAuthPtr, pInpacket, Len, Id);
                }
                else
                {
                    PPPSendRejPkt (pAuthPtr->pIf, pInpacket, Length,
                                   LCP_PROTOCOL, PROT_REJ_CODE);
                }
            }
            break;

        default:
            DISCARD_PKT (pAuthPtr->pIf, "Unknown CHAP Code received.");
    }
    return;
}

/*********************************************************************
*    Function Name    :    CHAPRcvdChallenge
*    Description        :
*                    This function  is called by the CHAPInput when the
*    Authentication Challenge is received from the Peer. It checks the 
*    Challenge, prepares the response for the same and sends the response 
*    back to the Peer.
*    Parameter(s)    :
*        pAuthPtr    -    Pointer to authentication interface which contains 
*                        all the CHAP information.
*        pInPacket    -    Pointer to  Input Packet String.
*        Length        -    Length of the Input Packet.
*        Identifier    -    Identifier received along with the packet and to
*                        be used in response
*    Return Values    :    VOID
*********************************************************************/
VOID
CHAPRcvdChallenge (tAUTHIf * pAuthPtr, t_MSG_DESC * pInpacket, UINT2 Length,
                   UINT1 Identifier)
{
    UINT1               RcvdChalValLen;
    UINT1               RcvdChalValue[MAX_CHALLENGE_LENGTH + 1];
    UINT1               RcvdName[PPP_MAX_NAME_LENGTH];
    UINT1               au1Password[MAX_SECRET_SIZE];
    tMD5_CTX            MDContext;
    tSecret            *pClientSecret;

    BZERO (au1Password, MAX_SECRET_SIZE);

    /* Extract and validate the received challenge */
    BZERO (RcvdName, PPP_MAX_NAME_LENGTH);
    BZERO (RcvdChalValue, (MAX_CHALLENGE_LENGTH + 1));
    PPP_TRC (CONTROL_PLANE, "CHAP_CHALLNGE_REQ\t IN :");

    if (Length < 1)
    {
        DISCARD_PKT (pAuthPtr->pIf, "Pkt discarded due to Invalid length");
        return;
    }
    EXTRACT1BYTE (pInpacket, RcvdChalValLen);
    Length--;
    if (RcvdChalValLen > MAX_CHALLENGE_LENGTH)
    {
        DISCARD_PKT (pAuthPtr->pIf, "Too Long Challenge !!");
        return;
    }

    if (Length < RcvdChalValLen)
    {
        DISCARD_PKT (pAuthPtr->pIf, "Pkt discarded due to Invalid length");
        return;
    }
    pAuthPtr->pIf->LLHCounters.InGoodOctets += Length;

    EXTRACTSTR (pInpacket, RcvdChalValue, RcvdChalValLen);
    Length = (UINT2) (Length - RcvdChalValLen);

    if (Length >= PPP_MAX_NAME_LENGTH)
    {
        DISCARD_PKT (pAuthPtr->pIf, "Rcvd Name is too long !!!");
        return;
    }

    EXTRACTSTR (pInpacket, RcvdName, Length);

    /* Get the secrect from secrets table */
    if ((pAuthPtr->ClientInfo.ClientChap.pCHAPClientSecret =
         MSCHAPAUTHGetSecret (pAuthPtr, RcvdName, CHAP_PROTOCOL, CLIENT))
        == NULL)
    {
        PPP_TRC (ALL_FAILURE,
                 "Unable to send response due to insufficient secret info. ");
        return;
    }
    pClientSecret = pAuthPtr->ClientInfo.ClientChap.pCHAPClientSecret;

    /* Cancel response send timeout if necessary */
    if (pAuthPtr->ClientInfo.ClientChap.State == CHAP_CLIENT_STATE_RESPONSE)
    {
        PPPStopTimer (&pAuthPtr->ClientInfo.ClientChap.ChapRespTimer);
    }
    pAuthPtr->ClientInfo.ClientChap.ResponseId = Identifier;
    pAuthPtr->ClientInfo.ClientChap.ResponseTransmits = 0;

    STRCPY (au1Password, pClientSecret->Secret);
    FsUtlDecryptPasswd ((CHR1 *) au1Password);

    /* Calculate the hash value using the MD5 algorithm */
    PppMD5Init (&MDContext);
    PppMD5Update (&MDContext, &pAuthPtr->ClientInfo.ClientChap.ResponseId, 1);
    PppMD5Update (&MDContext, au1Password, pClientSecret->SecretLen);
    PppMD5Update (&MDContext, RcvdChalValue, RcvdChalValLen);
    PppMD5Final (&MDContext);
    MEMCPY (pAuthPtr->ClientInfo.ClientChap.pResponse, MDContext.Digest,
            MD5_SIGNATURE_SIZE);
    pAuthPtr->ClientInfo.ClientChap.ResponseLength = MD5_SIGNATURE_SIZE;

    /* Send the response to peer */
    CHAPSendResponse (pAuthPtr);

    return;
}

/*********************************************************************
*    Function Name    :    CHAPRcvdAuthResponse
*    Description        :
*                    This function  is called by the CHAPInput when the 
*    CHAP response is received from the Peer. It checks the response and 
*    sends TRUE/FALSE  based on the response.
*    Parameter(s)    :
*        pAuthPtr    -    Pointer to CHAPState structure which contains all 
*                        the CHAP information.
*        pInPacket    -    Pointer to  Input Packet String.
*        Length        -    Length of the Input Packet
*        Identifier    -    Identifier received along with the packet and
*                        to be used in response.
*    Return Values    :    VOID
*********************************************************************/
VOID
CHAPRcvdAuthResponse (tAUTHIf * pAuthPtr, t_MSG_DESC * pInpacket, UINT2 Length,
                      UINT1 Identifier)
{
    UINT1               RcvdMMD[MAX_CHALLENGE_LENGTH + 1], RcvdMMDLen;
    UINT1               au1Password[MAX_SECRET_SIZE];
    INT2                OldState;
    INT2                Code;
    UINT1               HostName[MAX_SECRET_SIZE + 1];
    tMD5_CTX            MDContext;
    tSecret            *pServerSecret;

    UINT1               HostNameLen;

    BZERO (au1Password, MAX_SECRET_SIZE);

#ifdef RADIUS_WANTED
    BZERO (RcvdMMD, MAX_CHALLENGE_LENGTH + 1);
    BZERO (HostName, MAX_SECRET_SIZE + 1);

#endif /* RADIUS_WANTED */

    /*  Check if identifier of last sent challenge matches with that of the incoming response */
    PPP_TRC (CONTROL_PLANE, "CHAP_AUTH_RESPONSE \t IN :");
    if ((Identifier != pAuthPtr->ServerInfo.ServerChap.ChallengeId)
        || (Length < MIN_CHAP_PKT_LEN))
    {
        DISCARD_PKT (pAuthPtr->pIf, "Invalid Id/Length in/of CHAP response.");
        return;
    }

#ifdef L2TP_WANTED
    if (PPPIsL2TPTunnelModeLAC (pAuthPtr->pIf) == FALSE)
    {
#endif
        /* If we have received a duplicate or bogus Response, we have to send 
           the same answer (Success/Failure) as we did for the first Response 
           we saw.
         */

    if (pAuthPtr->ServerInfo.ServerChap.State == CHAP_SERVER_STATE_OPEN)
    {
        CHAPSendStatus (pAuthPtr, CHAP_TRUE);
        return;
    }

    if (pAuthPtr->ServerInfo.ServerChap.State == CHAP_SERVER_STATE_BADAUTH)
    {
        CHAPSendStatus (pAuthPtr, CHAP_FALSE);
        return;
    }
#ifdef RADIUS_WANTED
    if (pAuthPtr->ServerInfo.ServerChap.State == CHAP_SERVER_STATE_PENDING)
    {
        PPP_TRC (CONTROL_PLANE, "Response already send to RADIUS Server");
        return;
    }
#endif

#ifdef L2TP_WANTED
    }
#endif

    /* Extract and validate the incoming response packet */
    EXTRACT1BYTE (pInpacket, RcvdMMDLen);

    if ((Length < RcvdMMDLen) || (RcvdMMDLen > MAX_CHALLENGE_LENGTH))
    {
        DISCARD_PKT (pAuthPtr->pIf, "Pkt discarded due to Invalid length");
        return;
    }

    pAuthPtr->pIf->LLHCounters.InGoodOctets += Length;

    EXTRACTSTR (pInpacket, RcvdMMD, RcvdMMDLen);

    HostNameLen = (UINT1) (Length - RcvdMMDLen - 1);
    EXTRACTSTR (pInpacket, HostName, HostNameLen);
    PPPStopTimer (&pAuthPtr->ServerInfo.ServerChap.ChapChalTimer);

    /*
     * Get secret for authenticating them with us,
     * do the hash ourselves, and compare the result.
     */

#ifdef RADIUS_WANTED
    if (gu1aaaMethod == AAA_RADIUS)
    {
        PPP_TRC (CONTROL_PLANE, "Authenticating using RADIUS Server...");

        Code = (INT2) PppRadCHAPAuthenticate (HostName, HostNameLen, RcvdMMD,
                                              RcvdMMDLen, Identifier,
                                              pAuthPtr->ServerInfo.ServerChap.
                                              ChallengeValue,
                                              pAuthPtr->ServerInfo.ServerChap.
                                              ChallengeValueLength,
                                              pAuthPtr->pIf->LinkInfo.IfIndex);

        pAuthPtr->pIf->RadInfo.RadReqId = (INT1) Code;

        if (Code != NOT_OK)
        {
            pAuthPtr->pIf->RadInfo.ChapState =
                pAuthPtr->ServerInfo.ServerChap.State;
            pAuthPtr->ServerInfo.ServerChap.State = CHAP_SERVER_STATE_PENDING;
            pAuthPtr->pIf->RadInfo.PppAuthId = Identifier;
            if ((pAuthPtr->pIf->RadInfo.Username =
                 (UINT1 *) CALLOC (PPP_MAX_NAME_LENGTH, 1)) == NULL)
            {
                PPP_TRC (ALL_FAILURE, "Memory Alloc For UserName Failed");
                return;
            }
            MEMCPY (pAuthPtr->pIf->RadInfo.Username, HostName, HostNameLen);
            return;
        }

        PPP_TRC (ALL_FAILURE,
                 "Err in contacting RAD Server!.Locally authenticating");
    }

#endif /* RADIUS_WANTED */

    Code = CHAP_FALSE;

    if ((pServerSecret = AUTHGetSecret (pAuthPtr, HostName, HostNameLen,
                                        CHAP_PROTOCOL, SERVER)) == NULL)
    {
        PPP_TRC (ALL_FAILURE,
                 "Unable to send reply due to insufficient secret info. ");
    }
    else
    {
        STRCPY (au1Password, pServerSecret->Secret);
        FsUtlDecryptPasswd ((CHR1 *) au1Password);

        if (RcvdMMDLen == MD5_SIGNATURE_SIZE)
        {
            PppMD5Init (&MDContext);
            PppMD5Update (&MDContext,
                          &pAuthPtr->ServerInfo.ServerChap.ChallengeId, 1);
            PppMD5Update (&MDContext, au1Password, pServerSecret->SecretLen);
            PppMD5Update (&MDContext,
                          pAuthPtr->ServerInfo.ServerChap.ChallengeValue,
                          pAuthPtr->ServerInfo.ServerChap.ChallengeValueLength);
            PppMD5Final (&MDContext);

            /* Compare local and remote MDs and send the appropriate status */
            if (MEMCMP (MDContext.Digest, RcvdMMD, MD5_SIGNATURE_SIZE) == 0)
            {
                Code = CHAP_TRUE;
            }
        }
    }

    CHAPSendStatus (pAuthPtr, (UINT1) (Code));

    if (Code == CHAP_TRUE)
    {
        OldState = pAuthPtr->ServerInfo.ServerChap.State;
        pAuthPtr->ServerInfo.ServerChap.State = CHAP_SERVER_STATE_OPEN;
        if (OldState == CHAP_SERVER_STATE_INITIAL_CHAL)
        {
            LCPUpdateAuthStatus (pAuthPtr->pIf, CHAP_PEER_TRUE);
        }
        if (pAuthPtr->ServerInfo.ServerChap.ChallengeInterval != 0)
        {
            pAuthPtr->ServerInfo.ServerChap.ChapReChalTimer.Param1 =
                PTR_TO_U4 (pAuthPtr);
            PPPStartTimer (&pAuthPtr->ServerInfo.ServerChap.ChapReChalTimer,
                           CHAP_RECHAL_TIMER,
                           pAuthPtr->ServerInfo.ServerChap.ChallengeInterval);
        }

#ifdef L2TP_WANTED
        if (PPPIsL2TPTunnelModeLAC (pAuthPtr->pIf) == TRUE)
        {
            PPPIndicateAuthInfoToLAC (pAuthPtr->pIf->LinkInfo.IfIndex,
                                      L2TP_CHAP_AUTH,
                                      Identifier,
                                      HostName, HostNameLen,
                                      RcvdMMD, RcvdMMDLen,
                                      pAuthPtr->ServerInfo.ServerChap.
                                      ChallengeValue,
                                      pAuthPtr->ServerInfo.ServerChap.
                                      ChallengeValueLength);
        }
#endif

    }
    else
    {

        PPP_TRC (ALL_FAILURE, "CHAP Authentication failed....");
        pAuthPtr->ServerInfo.ServerChap.State = CHAP_SERVER_STATE_BADAUTH;
        LCPAuthProcessFailed (pAuthPtr->pIf);
    }

    return;
}

/*********************************************************************
*  Function Name : CHAPRcvdAuthSuccess
*  Description   :
*                       This function  is called by  CHAPInput when the
*  Authentication Success Reply is received from the peer. This function
*  indicates the  LCP that authentication if success, if there is no other
*  authentication pending.
*  Parameter(s)    :
*        pAuthPtr  -  pointer to CHAPState structure which contains
*                                    all the CHAP information.
*        pInPacket -  pointer to  Input Packet String.
*        Length    -  Length of the Input Packet
*        Identifier-  Identifier received along with the packet and to
*                                   be used in response
*  Return Values : VOID
*********************************************************************/
VOID
CHAPRcvdAuthSuccess (tAUTHIf * pAuthPtr, t_MSG_DESC * pInpacket, UINT2 Length,
                     UINT1 Identifier)
{

    PPP_UNUSED (pInpacket);
    PPP_TRC (CONTROL_PLANE, "CHAP_AUTH_SUCCESS \t IN :");
    if (Identifier != pAuthPtr->ClientInfo.ClientChap.ResponseId)
    {
        DISCARD_PKT (pAuthPtr->pIf, "Invalid Id in CHAP response.");
        return;
    }

    pAuthPtr->pIf->LLHCounters.InGoodOctets += Length;

    if ((pAuthPtr->ClientInfo.ClientChap.State == CHAP_CLIENT_STATE_OPEN)
        || (pAuthPtr->ClientInfo.ClientChap.State !=
            CHAP_CLIENT_STATE_RESPONSE))
        return;

    PPPStopTimer (&pAuthPtr->ClientInfo.ClientChap.ChapRespTimer);

    pAuthPtr->ClientInfo.ClientChap.State = CHAP_CLIENT_STATE_OPEN;

    LCPUpdateAuthStatus (pAuthPtr->pIf, CHAP_WITH_PEER_TRUE);

    return;
}

/*********************************************************************
*  Function Name : CHAPRcvdAuthFailure
*  Description   :
*                       This function  is called by CHAPInput when there is an
*   authentication failure packet sent by peer. It  validates the  input packet
*   and  informs LCP that the authentication has failed.
*  Parameter(s)  :
*           pAuthPtr  -  pointer to CHAPState structure which contains all
*                                    the CHAP information.
*           pInPacket -  pointer to  Input Packet String.
*           Length    -  Length of the Input Packet
*           Identifier-  Identifier received along with the packet and to
*                                    be used in response
*  Return Values : VOID
*********************************************************************/
VOID
CHAPRcvdAuthFailure (tAUTHIf * pAuthPtr, t_MSG_DESC * pInpacket, UINT2 Length,
                     UINT1 Identifier)
{

    PPP_UNUSED (pInpacket);
    PPP_UNUSED (Identifier);
    PPP_TRC (CONTROL_PLANE, "CHAP_AUTH_FAILURE \t IN :");
    if (Identifier != pAuthPtr->ClientInfo.ClientChap.ResponseId)
    {
        DISCARD_PKT (pAuthPtr->pIf, "Invalid Id in CHAP response.");
        return;
    }

    pAuthPtr->pIf->LLHCounters.InGoodOctets += Length;

    if (pAuthPtr->ClientInfo.ClientChap.State != CHAP_CLIENT_STATE_RESPONSE)
        return;

    pAuthPtr->ClientInfo.ClientChap.State = CHAP_CLIENT_STATE_BADAUTH;

    PPPStopTimer (&pAuthPtr->ClientInfo.ClientChap.ChapRespTimer);

    PPP_TRC (ALL_FAILURE, "CHAP authentication failed");

    /* Wait for  the Peer to terminate the link  */
    return;
}

/*********************************************************************
*  Function Name : CHAPGenChallenge
*  Description   :
*                   This function is used by the  CHAPAuthWithPeer()
*   function to generate the pseudo-random Challenge string that is to be sent
*   in the Challenge Request packet.
*  Parameter(s)  :
*           pAuthPtr  -  pointer to CHAPState structure which contains
*                                    all the CHAP information.
*  Return Values : VOID
*********************************************************************/
VOID
CHAPGenChallenge (tAUTHIf * pAuthPtr)
{

    /*pick a random ChallengeValue length between MIN_CHALLENGE_LENGTH and 
       MAX_CHALLENGE_LENGTH */

    pAuthPtr->ServerInfo.ServerChap.ChallengeValueLength =
        GET_RANDOM_CHALLENGE_LENGTH ();

    pAuthPtr->ServerInfo.ServerChap.ChallengeId =
        pAuthPtr->ServerInfo.ServerChap.CurrentId++;

    PPPGetRandom (pAuthPtr->ServerInfo.ServerChap.ChallengeValue,
                  (UINT4) pAuthPtr->ServerInfo.ServerChap.ChallengeValueLength);
    return;
}

/*********************************************************************
*  Function Name : CHAPSendChallenge
*  Description   :
*                       This function is used to transmitt the Challenge to the
*  and is used by the AuthPeer() function, after calling CHAPGenChallenge(). It
*  is also used by the CHAPChalTimeOut() to resend the Challenge to the peer.
*  Parameter(s)  :
*       pAuthPtr -  pointer to CHAPState structure which contains
*                                       all the CHAP information.
*  Return Values : VOID
*********************************************************************/
VOID
CHAPSendChallenge (tAUTHIf * pAuthPtr)
{
    UINT2               Length;
    t_MSG_DESC         *pOutBuf;
    UINT2               u2IdLen;

    if (pAuthPtr->ServerInfo.ServerChap.pCHAPCurrentSecret == NULL)
    {
        if ((pAuthPtr->ServerInfo.ServerChap.pCHAPCurrentSecret =
             AUTHGetSecretName (pAuthPtr, CHAP_PROTOCOL, SERVER)) == NULL)
        {
            /* Wrap around the entries if it is due to re-challenging */
            if (pAuthPtr->ServerInfo.ServerChap.State ==
                CHAP_SERVER_STATE_RECHALLENGE)
            {
                pAuthPtr->CHAPSearchList = IF_SPECIFIC_LIST;
                pAuthPtr->ChapLastSecretIndex = INIT_VAL;
            }
            if ((gu1aaaMethod != AAA_RADIUS)
                && ((pAuthPtr->ServerInfo.ServerChap.pCHAPCurrentSecret =
                     AUTHGetSecretName (pAuthPtr, CHAP_PROTOCOL,
                                        SERVER)) == NULL))
            {
                PPP_TRC (ALL_FAILURE, "CHAP information not available");
                LCPAuthProcessFailed (pAuthPtr->pIf);
                return;
            }
        }
    }

    u2IdLen = (UINT2) (STRLEN (pAuthPtr->pIf->au1HostName));

    Length =
        (UINT2) (pAuthPtr->ServerInfo.ServerChap.ChallengeValueLength +
                 u2IdLen + VALUE_SIZE_FIELD_LEN + CP_HDR_LEN);

    if ((pOutBuf =
         (t_MSG_DESC *) (VOID *) ALLOCATE_BUFFER (PPP_INFO_OFFSET + Length,
                                                  PPP_INFO_OFFSET)) != NULL)
    {

        FILL_PKT_HEADER (pOutBuf, CHAP_CHALLENGE_REQ,
                         pAuthPtr->ServerInfo.ServerChap.ChallengeId, Length);
        APPEND1BYTE (pOutBuf,
                     pAuthPtr->ServerInfo.ServerChap.ChallengeValueLength);
        APPENDSTR (pOutBuf, pAuthPtr->ServerInfo.ServerChap.ChallengeValue,
                   pAuthPtr->ServerInfo.ServerChap.ChallengeValueLength);
        APPENDSTR (pOutBuf, pAuthPtr->pIf->au1HostName, u2IdLen);

        PPPLLITxPkt (pAuthPtr->pIf, pOutBuf, Length, CHAP_PROTOCOL);

        pAuthPtr->ServerInfo.ServerChap.ChapChalTimer.Param1 =
            PTR_TO_U4 (pAuthPtr);
        PPPStartTimer (&pAuthPtr->ServerInfo.ServerChap.ChapChalTimer,
                       CHAP_CHAL_TIMER,
                       pAuthPtr->ServerInfo.ServerChap.TimeoutTime);

        /* Increment number of retransmits */
        ++pAuthPtr->ServerInfo.ServerChap.ChallengeTransmits;

    }
    else
    {
        PRINT_MEM_ALLOC_FAILURE;
    }
    return;
}

/*********************************************************************
*  Function Name :CHAPSendStatus
*  Description   :
*                   This procedure is used to send back the status of the
*   received CHAP response. It  is used by the  CHAPRcvdResponse() function
*   after validating the response value with the calculated one.
*  Parameter(s)  :
*           pAuthPtr -  pointer to CHAPState structure which contains all
*                                    the CHAP information.
*           Code     -  Indicates whether the Status is  TRUE /FALSE
*  Return Values : VOID
*********************************************************************/
VOID
CHAPSendStatus (tAUTHIf * pAuthPtr, UINT1 Code)
{
    t_MSG_DESC         *pOutBuf;
    UINT1               Msg[MAX_MSG_SIZE];
    UINT2               Length = 0;

    if (Code == CHAP_TRUE)
        STRCPY (Msg, "You have successfully logged in....");
    else
        STRCPY (Msg, "You have failed to log in....");

    Length = (UINT2) (STRLEN (Msg) + CP_HDR_LEN);

    if ((pOutBuf =
         (t_MSG_DESC *) (VOID *) ALLOCATE_BUFFER (PPP_INFO_OFFSET + Length,
                                                  PPP_INFO_OFFSET)) != NULL)
    {

        FILL_PKT_HEADER (pOutBuf, Code,
                         pAuthPtr->ServerInfo.ServerChap.ChallengeId, Length);
        APPENDSTR (pOutBuf, Msg, STRLEN (Msg));

        PPPLLITxPkt (pAuthPtr->pIf, pOutBuf, Length, CHAP_PROTOCOL);
    }
    else
    {

        PRINT_MEM_ALLOC_FAILURE;
    }
    return;
}

/*********************************************************************
*  Function Name :CHAPSendResponse
*  Description   :
*                   This function is used by the  CHAPRcvdChallenge() to send
*  back the reply for the received request.It  constructs the response packet
*  and send the same to the lower layer modules by calling LLHandlerSend(). It
*  also starts the restart timer after sending the packet.
*  Parameter(s)  :
*               pAuthPtr  -  pointer to CHAPState structure which contains*                                       all the CHAP information.
*  Return Values : VOID
*********************************************************************/
VOID
CHAPSendResponse (tAUTHIf * pAuthPtr)
{
    t_MSG_DESC         *pOutBuf;
    UINT1               Length;
    tSecret            *pSecret;
    UINT2               u2IdLen;

    if (pAuthPtr->ClientInfo.ClientChap.pCHAPClientSecret == NULL)
    {

        PPP_TRC (ALL_FAILURE, "No Secret available");
        PPPStopTimer (&pAuthPtr->ClientInfo.ClientChap.ChapRespTimer);
        return;
    }
    pSecret = pAuthPtr->ClientInfo.ClientChap.pCHAPClientSecret;

    u2IdLen = pSecret->IdentityLen;

    Length =
        (UINT1) (pAuthPtr->ClientInfo.ClientChap.ResponseLength + u2IdLen +
                 CP_HDR_LEN + VALUE_SIZE_FIELD_LEN);

    if ((pOutBuf =
         (t_MSG_DESC *) (VOID *) ALLOCATE_BUFFER (PPP_INFO_OFFSET + Length,
                                                  PPP_INFO_OFFSET)) != NULL)
    {

        FILL_PKT_HEADER (pOutBuf, CHAP_CHALLENGE_RESP,
                         pAuthPtr->ClientInfo.ClientChap.ResponseId, Length);
        APPEND1BYTE (pOutBuf, pAuthPtr->ClientInfo.ClientChap.ResponseLength);
        APPENDSTR (pOutBuf, pAuthPtr->ClientInfo.ClientChap.pResponse,
                   pAuthPtr->ClientInfo.ClientChap.ResponseLength);
        APPENDSTR (pOutBuf, pSecret->Identity, u2IdLen);

        PPPLLITxPkt (pAuthPtr->pIf, pOutBuf, Length, CHAP_PROTOCOL);

        pAuthPtr->ClientInfo.ClientChap.State = CHAP_CLIENT_STATE_RESPONSE;

        pAuthPtr->ClientInfo.ClientChap.ChapRespTimer.Param1 =
            PTR_TO_U4 (pAuthPtr);
        PPPStartTimer (&pAuthPtr->ClientInfo.ClientChap.ChapRespTimer,
                       CHAP_RESP_TIMER,
                       pAuthPtr->ClientInfo.ClientChap.TimeoutTime);

        ++pAuthPtr->ClientInfo.ClientChap.ResponseTransmits;
    }
    else
    {

        PRINT_MEM_ALLOC_FAILURE;
    }
    return;
}
