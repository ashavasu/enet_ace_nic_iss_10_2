/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppautel.c,v 1.2 2011/10/13 10:31:31 siva Exp $
 *
 * Description: Low Level Routines for Authentication module.
 *
 *******************************************************************/
#include "snmphdrs.h"
#include "authcom.h"
#include "pppsnmpm.h"
#include "llproto.h"
#include "globexts.h"
#ifdef PPP_STACK_WANTED
#include "ppautlow.h"
#endif
extern tSNMP_OID_TYPE pppSecurityEapMD5ProtocolOID;

extern tLangTable   gLangTable;
extern tSecurityInfo IndexZeroSecurityInfo;

#ifdef PPP_STACK_WANTED            /* DSL_ADD */
/* LOW LEVEL Routines for Table : PppExtSecurityConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppExtSecurityConfigTable
 Input       :  The Indices
                PppExtSecurityConfigIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppExtSecurityConfigTable (INT4
                                                   i4PppExtSecurityConfigIfIndex)
{
    if (PppGetPppIfPtr (i4PppExtSecurityConfigIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppExtSecurityConfigTable
 Input       :  The Indices
                PppExtSecurityConfigIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppExtSecurityConfigTable (INT4 *pi4PppExtSecurityConfigIfIndex)
{
    return (nmhGetFirstIndex
            (pi4PppExtSecurityConfigIfIndex, LinkInstance, FIRST_INST));

}

/****************************************************************************
 Function    :  nmhGetNextIndexPppExtSecurityConfigTable
 Input       :  The Indices
                PppExtSecurityConfigIfIndex
                nextPppExtSecurityConfigIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppExtSecurityConfigTable (INT4 i4PppExtSecurityConfigIfIndex,
                                          INT4
                                          *pi4NextPppExtSecurityConfigIfIndex)
{
    if (nmhGetNextIndex
        (&i4PppExtSecurityConfigIfIndex, LinkInstance,
         NEXT_INST) == SNMP_SUCCESS)
    {
        *pi4NextPppExtSecurityConfigIfIndex = i4PppExtSecurityConfigIfIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppExtSecurityConfigReChallingInterval
 Input       :  The Indices
                PppExtSecurityConfigIfIndex

                The Object 
                retValPppExtSecurityConfigReChallingInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtSecurityConfigReChallingInterval (INT4
                                              i4PppExtSecurityConfigIfIndex,
                                              INT4
                                              *pi4RetValPppExtSecurityConfigReChallingInterval)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr (i4PppExtSecurityConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValPppExtSecurityConfigReChallingInterval =
        pIf->LinkInfo.ReChalTimeOut;
    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppExtSecurityConfigReChallingInterval
 Input       :  The Indices
                PppExtSecurityConfigIfIndex

                The Object 
                setValPppExtSecurityConfigReChallingInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtSecurityConfigReChallingInterval (INT4
                                              i4PppExtSecurityConfigIfIndex,
                                              INT4
                                              i4SetValPppExtSecurityConfigReChallingInterval)
{
    tPPPIf             *pIf = NULL;

    pIf = PppGetPppIfPtr (i4PppExtSecurityConfigIfIndex);
    pIf->LinkInfo.ReChalTimeOut =
        (UINT2) i4SetValPppExtSecurityConfigReChallingInterval;

    return (SNMP_SUCCESS);

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppExtSecurityConfigReChallingInterval
 Input       :  The Indices
                PppExtSecurityConfigIfIndex

                The Object 
                testValPppExtSecurityConfigReChallingInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtSecurityConfigReChallingInterval (UINT4 *pu4ErrorCode,
                                                 INT4
                                                 i4PppExtSecurityConfigIfIndex,
                                                 INT4
                                                 i4TestValPppExtSecurityConfigReChallingInterval)
{
    if (PppGetPppIfPtr (i4PppExtSecurityConfigIfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (SNMP_FAILURE);
    }
    if (i4TestValPppExtSecurityConfigReChallingInterval < MIN_INTEGER
        || i4TestValPppExtSecurityConfigReChallingInterval > MAX_INTEGER)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}
#endif /* DSL_ADD */

#ifdef PPP_STACK_WANTED            /* DSL_ADD */
/* LOW LEVEL Routines for Table : PppExtSecuritySecretsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppExtSecuritySecretsTable
 Input       :  The Indices
                PppExtSecuritySecretsLink
                PppExtSecuritySecretsIdIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppExtSecuritySecretsTable (INT4
                                                    i4PppExtSecuritySecretsLink,
                                                    INT4
                                                    i4PppExtSecuritySecretsIdIndex)
{
    PPP_UNUSED (i4PppExtSecuritySecretsLink);
    PPP_UNUSED (i4PppExtSecuritySecretsIdIndex);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppExtSecuritySecretsTable
 Input       :  The Indices
                PppExtSecuritySecretsLink
                PppExtSecuritySecretsIdIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppExtSecuritySecretsTable (INT4 *pi4PppExtSecuritySecretsLink,
                                            INT4
                                            *pi4PppExtSecuritySecretsIdIndex)
{
    /* this table is currently not used */
    PPP_UNUSED (pi4PppExtSecuritySecretsLink);
    PPP_UNUSED (pi4PppExtSecuritySecretsIdIndex);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppExtSecuritySecretsTable
 Input       :  The Indices
                PppExtSecuritySecretsLink
                nextPppExtSecuritySecretsLink
                PppExtSecuritySecretsIdIndex
                nextPppExtSecuritySecretsIdIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppExtSecuritySecretsTable (INT4 i4PppExtSecuritySecretsLink,
                                           INT4
                                           *pi4NextPppExtSecuritySecretsLink,
                                           INT4 i4PppExtSecuritySecretsIdIndex,
                                           INT4
                                           *pi4NextPppExtSecuritySecretsIdIndex)
{
    PPP_UNUSED (i4PppExtSecuritySecretsLink);
    PPP_UNUSED (pi4NextPppExtSecuritySecretsLink);
    PPP_UNUSED (i4PppExtSecuritySecretsIdIndex);
    PPP_UNUSED (pi4NextPppExtSecuritySecretsIdIndex);
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppExtSecuritySecretsCharSet
 Input       :  The Indices
                PppExtSecuritySecretsLink
                PppExtSecuritySecretsIdIndex

                The Object 
                retValPppExtSecuritySecretsCharSet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtSecuritySecretsCharSet (INT4 i4PppExtSecuritySecretsLink,
                                    INT4 i4PppExtSecuritySecretsIdIndex,
                                    INT4 *pi4RetValPppExtSecuritySecretsCharSet)
{
    PPP_UNUSED (i4PppExtSecuritySecretsLink);
    PPP_UNUSED (i4PppExtSecuritySecretsIdIndex);
    PPP_UNUSED (pi4RetValPppExtSecuritySecretsCharSet);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetPppExtSecuritySecretsLangTag
 Input       :  The Indices
                PppExtSecuritySecretsLink
                PppExtSecuritySecretsIdIndex

                The Object 
                retValPppExtSecuritySecretsLangTag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtSecuritySecretsLangTag (INT4 i4PppExtSecuritySecretsLink,
                                    INT4 i4PppExtSecuritySecretsIdIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValPppExtSecuritySecretsLangTag)
{
    PPP_UNUSED (i4PppExtSecuritySecretsLink);
    PPP_UNUSED (i4PppExtSecuritySecretsIdIndex);
    PPP_UNUSED (pRetValPppExtSecuritySecretsLangTag);
    return (SNMP_FAILURE);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppExtSecuritySecretsCharSet
 Input       :  The Indices
                PppExtSecuritySecretsLink
                PppExtSecuritySecretsIdIndex

                The Object 
                setValPppExtSecuritySecretsCharSet
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtSecuritySecretsCharSet (INT4 i4PppExtSecuritySecretsLink,
                                    INT4 i4PppExtSecuritySecretsIdIndex,
                                    INT4 i4SetValPppExtSecuritySecretsCharSet)
{
    PPP_UNUSED (i4PppExtSecuritySecretsLink);
    PPP_UNUSED (i4PppExtSecuritySecretsIdIndex);
    PPP_UNUSED (i4SetValPppExtSecuritySecretsCharSet);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhSetPppExtSecuritySecretsLangTag
 Input       :  The Indices
                PppExtSecuritySecretsLink
                PppExtSecuritySecretsIdIndex

                The Object 
                setValPppExtSecuritySecretsLangTag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtSecuritySecretsLangTag (INT4 i4PppExtSecuritySecretsLink,
                                    INT4 i4PppExtSecuritySecretsIdIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pSetValPppExtSecuritySecretsLangTag)
{
    PPP_UNUSED (i4PppExtSecuritySecretsLink);
    PPP_UNUSED (i4PppExtSecuritySecretsIdIndex);
    PPP_UNUSED (pSetValPppExtSecuritySecretsLangTag);
    return (SNMP_FAILURE);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppExtSecuritySecretsCharSet
 Input       :  The Indices
                PppExtSecuritySecretsLink
                PppExtSecuritySecretsIdIndex

                The Object 
                testValPppExtSecuritySecretsCharSet
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtSecuritySecretsCharSet (UINT4 *pu4ErrorCode,
                                       INT4 i4PppExtSecuritySecretsLink,
                                       INT4 i4PppExtSecuritySecretsIdIndex,
                                       INT4
                                       i4TestValPppExtSecuritySecretsCharSet)
{
    PPP_UNUSED (pu4ErrorCode);
    PPP_UNUSED (i4PppExtSecuritySecretsLink);
    PPP_UNUSED (i4PppExtSecuritySecretsIdIndex);
    PPP_UNUSED (i4TestValPppExtSecuritySecretsCharSet);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppExtSecuritySecretsLangTag
 Input       :  The Indices
                PppExtSecuritySecretsLink
                PppExtSecuritySecretsIdIndex

                The Object 
                testValPppExtSecuritySecretsLangTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtSecuritySecretsLangTag (UINT4 *pu4ErrorCode,
                                       INT4 i4PppExtSecuritySecretsLink,
                                       INT4 i4PppExtSecuritySecretsIdIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pTestValPppExtSecuritySecretsLangTag)
{
    PPP_UNUSED (pu4ErrorCode);
    PPP_UNUSED (i4PppExtSecuritySecretsLink);
    PPP_UNUSED (i4PppExtSecuritySecretsIdIndex);
    PPP_UNUSED (pTestValPppExtSecuritySecretsLangTag);
    return (SNMP_SUCCESS);
}
#endif /* DSL_ADD */

#ifdef PPP_STACK_WANTED            /* DSL_ADD */
/* LOW LEVEL Routines for Table : PppExtSecurityEapConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppExtSecurityEapConfigTable
 Input       :  The Indices
                PppExtSecurityEapConfigIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppExtSecurityEapConfigTable (INT4
                                                      i4PppExtSecurityEapConfigIfIndex)
{
    if (PppGetPppIfPtr (i4PppExtSecurityEapConfigIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppExtSecurityEapConfigTable
 Input       :  The Indices
                PppExtSecurityEapConfigIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexPppExtSecurityEapConfigTable (INT4
                                              *pi4PppExtSecurityEapConfigIfIndex)
{

    return (nmhGetFirstIndex
            (pi4PppExtSecurityEapConfigIfIndex, LinkInstance, FIRST_INST));

}

/****************************************************************************
 Function    :  nmhGetNextIndexPppExtSecurityEapConfigTable
 Input       :  The Indices
                PppExtSecurityEapConfigIfIndex
                nextPppExtSecurityEapConfigIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppExtSecurityEapConfigTable (INT4
                                             i4PppExtSecurityEapConfigIfIndex,
                                             INT4
                                             *pi4NextPppExtSecurityEapConfigIfIndex)
{
    INT1                i1RetVal;
    i1RetVal = nmhGetNextIndex (&i4PppExtSecurityEapConfigIfIndex,
                                LinkInstance, NEXT_INST);
    *pi4NextPppExtSecurityEapConfigIfIndex = i4PppExtSecurityEapConfigIfIndex;

    return i1RetVal;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppExtSecurityEapConfigAuthProtocol
 Input       :  The Indices
                PppExtSecurityEapConfigIfIndex

                The Object 
                retValPppExtSecurityEapConfigAuthProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtSecurityEapConfigAuthProtocol (INT4
                                           i4PppExtSecurityEapConfigIfIndex,
                                           tSNMP_OID_TYPE *
                                           pRetValPppExtSecurityEapConfigAuthProtocol)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr (i4PppExtSecurityEapConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pIf->LinkConfigs.EapDefaultAuthType == EAP_TYPE_MD5)
    {
        return (PPPCopyOid (pRetValPppExtSecurityEapConfigAuthProtocol,
                            &pppSecurityEapMD5ProtocolOID));
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetPppExtSecurityEapConfigMaxIdentityRetries
 Input       :  The Indices
                PppExtSecurityEapConfigIfIndex

                The Object 
                retValPppExtSecurityEapConfigMaxIdentityRetries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtSecurityEapConfigMaxIdentityRetries (INT4
                                                 i4PppExtSecurityEapConfigIfIndex,
                                                 INT4
                                                 *pi4RetValPppExtSecurityEapConfigMaxIdentityRetries)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr (i4PppExtSecurityEapConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppExtSecurityEapConfigMaxIdentityRetries =
        pIf->LinkConfigs.EapMaxIdentityRetries;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppExtSecurityEapConfigFailedIdentityRequests
 Input       :  The Indices
                PppExtSecurityEapConfigIfIndex

                The Object 
                retValPppExtSecurityEapConfigFailedIdentityRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtSecurityEapConfigFailedIdentityRequests (INT4
                                                     i4PppExtSecurityEapConfigIfIndex,
                                                     INT4
                                                     *pi4RetValPppExtSecurityEapConfigFailedIdentityRequests)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr (i4PppExtSecurityEapConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppExtSecurityEapConfigFailedIdentityRequests =
        pIf->pAuthPtr->ServerInfo.ServerEap.FailedIdentityRequests;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppExtSecurityEapConfigSendIdentityRequest
 Input       :  The Indices
                PppExtSecurityEapConfigIfIndex

                The Object 
                retValPppExtSecurityEapConfigSendIdentityRequest
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtSecurityEapConfigSendIdentityRequest (INT4
                                                  i4PppExtSecurityEapConfigIfIndex,
                                                  INT4
                                                  *pi4RetValPppExtSecurityEapConfigSendIdentityRequest)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr (i4PppExtSecurityEapConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppExtSecurityEapConfigSendIdentityRequest =
        pIf->LinkConfigs.EapIdentityNeeded;
    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppExtSecurityEapConfigAuthProtocol
 Input       :  The Indices
                PppExtSecurityEapConfigIfIndex

                The Object 
                setValPppExtSecurityEapConfigAuthProtocol
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtSecurityEapConfigAuthProtocol (INT4
                                           i4PppExtSecurityEapConfigIfIndex,
                                           tSNMP_OID_TYPE *
                                           pSetValPppExtSecurityEapConfigAuthProtocol)
{
    tPPPIf             *pIf = NULL;

    pIf = PppGetPppIfPtr (i4PppExtSecurityEapConfigIfIndex);

    if ((PPP_compare_oids
         (pSetValPppExtSecurityEapConfigAuthProtocol,
          &pppSecurityEapMD5ProtocolOID)) == EQUAL)
    {
        pIf->LinkConfigs.EapDefaultAuthType = EAP_TYPE_MD5;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetPppExtSecurityEapConfigMaxIdentityRetries
 Input       :  The Indices
                PppExtSecurityEapConfigIfIndex

                The Object 
                setValPppExtSecurityEapConfigMaxIdentityRetries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtSecurityEapConfigMaxIdentityRetries (INT4
                                                 i4PppExtSecurityEapConfigIfIndex,
                                                 INT4
                                                 i4SetValPppExtSecurityEapConfigMaxIdentityRetries)
{
    tPPPIf             *pIf = NULL;

    pIf = PppGetPppIfPtr (i4PppExtSecurityEapConfigIfIndex);

    pIf->LinkConfigs.EapMaxIdentityRetries =
        (UINT2) i4SetValPppExtSecurityEapConfigMaxIdentityRetries;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetPppExtSecurityEapConfigSendIdentityRequest
 Input       :  The Indices
                PppExtSecurityEapConfigIfIndex

                The Object 
                setValPppExtSecurityEapConfigSendIdentityRequest
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtSecurityEapConfigSendIdentityRequest (INT4
                                                  i4PppExtSecurityEapConfigIfIndex,
                                                  INT4
                                                  i4SetValPppExtSecurityEapConfigSendIdentityRequest)
{
    tPPPIf             *pIf = NULL;

    pIf = PppGetPppIfPtr (i4PppExtSecurityEapConfigIfIndex);

    pIf->LinkConfigs.EapIdentityNeeded =
        (UINT1) i4SetValPppExtSecurityEapConfigSendIdentityRequest;
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppExtSecurityEapConfigAuthProtocol
 Input       :  The Indices
                PppExtSecurityEapConfigIfIndex

                The Object 
                testValPppExtSecurityEapConfigAuthProtocol
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtSecurityEapConfigAuthProtocol (UINT4 *pu4ErrorCode,
                                              INT4
                                              i4PppExtSecurityEapConfigIfIndex,
                                              tSNMP_OID_TYPE *
                                              pTestValPppExtSecurityEapConfigAuthProtocol)
{
    if (PppGetPppIfPtr (i4PppExtSecurityEapConfigIfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
    if (PPP_compare_oids
        (pTestValPppExtSecurityEapConfigAuthProtocol,
         &pppSecurityEapMD5ProtocolOID) == EQUAL)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2PppExtSecurityEapConfigMaxIdentityRetries
 Input       :  The Indices
                PppExtSecurityEapConfigIfIndex

                The Object 
                testValPppExtSecurityEapConfigMaxIdentityRetries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtSecurityEapConfigMaxIdentityRetries (UINT4 *pu4ErrorCode,
                                                    INT4
                                                    i4PppExtSecurityEapConfigIfIndex,
                                                    INT4
                                                    i4TestValPppExtSecurityEapConfigMaxIdentityRetries)
{
    PPP_UNUSED (i4TestValPppExtSecurityEapConfigMaxIdentityRetries);
    if (PppGetPppIfPtr (i4PppExtSecurityEapConfigIfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppExtSecurityEapConfigSendIdentityRequest
 Input       :  The Indices
                PppExtSecurityEapConfigIfIndex

                The Object 
                testValPppExtSecurityEapConfigSendIdentityRequest
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtSecurityEapConfigSendIdentityRequest (UINT4 *pu4ErrorCode,
                                                     INT4
                                                     i4PppExtSecurityEapConfigIfIndex,
                                                     INT4
                                                     i4TestValPppExtSecurityEapConfigSendIdentityRequest)
{
    PPP_UNUSED (i4TestValPppExtSecurityEapConfigSendIdentityRequest);
    if (PppGetPppIfPtr (i4PppExtSecurityEapConfigIfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}
#endif /* DSL_ADD */
