/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppauth.c,v 1.6 2014/12/16 11:47:25 siva Exp $
 *
 * Description:This file contains interface procedures of 
 *             AUTHENTICATION Module
 *
 *******************************************************************/

#include "pppsnmpm.h"
#include "snmctdfs.h"
#include "authcom.h"
#include "gsemdefs.h"
#include "globexts.h"
#include "utilrand.h"
tSecurityInfo       IndexZeroSecurityInfo;
UINT2               AuthAllocCounter;
UINT2               AuthFreeCounter;

/*********************************************************************
*    Function Name    :  AUTHCreate
*    Description        :
*                This procedure creates an entry corresponding to the 
*    IfIndex in the PAP/CHAP interface data base. It is invoked by the SNMP
*    interface Module when the PAP/CHAP configuration is SET by the SNMP.
*    Parameter(s)    :
*                pIf - points to the PPP Interface structure
*    Return Values     :    Pointer to the entry if successfully created /
*                        NULL, if the creation failed
*********************************************************************/
tAUTHIf            *
AUTHCreate (tPPPIf * pIf)
{
    tAUTHIf            *pAuthPtr;

    if ((pAuthPtr = (tAUTHIf *) (VOID *) ALLOC_AUTH_IF ()) != NULL)
    {
        AUTHInit (pAuthPtr);
        pAuthPtr->pIf = pIf;
        return (pAuthPtr);
    }
    PRINT_MEM_ALLOC_FAILURE;
    return (NULL);
}

/*********************************************************************
*  Function Name    :  AUTHInit
*  Description        :
*                    This function initializes an authentication entry.
*  Parameter(s)        :
*        pAuthPtr    -    Points to the  newly allocated AUTH entry
*    Return Value    :    VOID
*********************************************************************/
VOID
AUTHInit (tAUTHIf * pAuthPtr)
{

    pAuthPtr->LastTriedIndex = 0;
    pAuthPtr->PapLastSecretIndex = INIT_VAL;
    pAuthPtr->ChapLastSecretIndex = INIT_VAL;
    pAuthPtr->MSChapLastSecretIndex = INIT_VAL;
    pAuthPtr->EapMD5LastSecretIndex = INIT_VAL;

    pAuthPtr->PAPSearchList = IF_SPECIFIC_LIST;
    pAuthPtr->CHAPSearchList = IF_SPECIFIC_LIST;
    pAuthPtr->MSCHAPSearchList = IF_SPECIFIC_LIST;

    pAuthPtr->EAPMD5SearchList = IF_SPECIFIC_LIST;

    pAuthPtr->AuthTriggeredNCPs = PPP_NO;

    BZERO (&pAuthPtr->ClientInfo, sizeof (tClientState));
    BZERO (&pAuthPtr->ServerInfo, sizeof (tServerState));

    pAuthPtr->ClientProt = 0;
    pAuthPtr->ServerProt = 0;

    AUTHInitSecurityInfo (&pAuthPtr->SecurityInfo);

    return;
}

/*********************************************************************
*  Function Name    :  AUTHInitSecurityInfo
*  Description        :
*                    This function initializes an authentication Security.
*  Information
*  Parameter(s)        :
*        pAuthPtr    -    Points to the  newly allocated AUTH entry
*    Return Value    :    VOID
*********************************************************************/
VOID
AUTHInitSecurityInfo (tSecurityInfo * pSecurityInfo)
{
    UINT1               Index;

    pSecurityInfo->CHAPLocToRemSecrets = 0;
    pSecurityInfo->MSCHAPLocToRemSecrets = 0;
    pSecurityInfo->EAPMD5LocToRemSecrets = 0;

    pSecurityInfo->PAPLocToRemSecrets = 0;
    pSecurityInfo->CHAPRemToLocSecrets = 0;
    pSecurityInfo->MSCHAPRemToLocSecrets = 0;
    pSecurityInfo->EAPMD5RemToLocSecrets = 0;
    pSecurityInfo->PAPRemToLocSecrets = 0;
    pSecurityInfo->NumActiveProt = 0;

    for (Index = 0; Index < MAX_AUTH_PROT; Index++)
    {
        BZERO (&pSecurityInfo->PrefList[Index], sizeof (tAuthPref));
    }

    SLL_INIT (&pSecurityInfo->SecretList);

    return;
}

/*********************************************************************
*    Function Name        : AuthInitServices 
*    Description            :
*                    If authentication has been negotiated, initialize the 
*    authentication clients and servers.    
*    Parameter(s)        :
*                    pIf    -    Pointer to the PPP Interface structure.    
*    Return Value        :    VOID
*********************************************************************/
VOID
AuthInitServices (tPPPIf * pIf)
{
    tAUTHIf            *pAuthPtr;

    pAuthPtr = pIf->pAuthPtr;
    /*  SKR007 **** AuthTriggered Flag  used for to avoid events to NCPs after periodic Rechallenges */

    pAuthPtr->AuthTriggeredNCPs = PPP_NO;
    if (pIf->LcpGSEM.pNegFlagsPerIf[AUTH_IDX].FlagMask & ACKED_BY_PEER_MASK)
    {
        if (pAuthPtr->LastTriedIndex < pAuthPtr->SecurityInfo.NumActiveProt)
        {
            if (pAuthPtr->SecurityInfo.PrefList[pAuthPtr->LastTriedIndex].
                ChapAlgo == MSCHAP_ALGORITHM)
                pAuthPtr->ServerProt = MSCHAP_PROTOCOL;
            else
                pAuthPtr->ServerProt =
                    pAuthPtr->SecurityInfo.PrefList[pAuthPtr->LastTriedIndex].
                    Protocol;
        }
        else
        {
            if (IndexZeroSecurityInfo.
                PrefList[pAuthPtr->LastTriedIndex -
                         pAuthPtr->SecurityInfo.NumActiveProt].ChapAlgo ==
                MSCHAP_ALGORITHM)
                pAuthPtr->ServerProt = MSCHAP_PROTOCOL;
            else
                pAuthPtr->ServerProt =
                    IndexZeroSecurityInfo.PrefList[pAuthPtr->LastTriedIndex -
                                                   pAuthPtr->SecurityInfo.
                                                   NumActiveProt].Protocol;
        }

        pIf->LcpStatus.AuthStatus =
            (UINT1) (pIf->LcpStatus.AuthStatus | ((pAuthPtr->
                                                   ServerProt ==
                                                   CHAP_PROTOCOL) ? CHAP_PEER
                                                  : ((pAuthPtr->ServerProt ==
                                                      MSCHAP_PROTOCOL) ?
                                                     MSCHAP_PEER
                                                     : ((pAuthPtr->ServerProt ==
                                                         EAP_PROTOCOL) ?
                                                        EAP_PEER : PAP_PEER))));
        if ((pAuthPtr->ServerProt == CHAP_PROTOCOL)
            || (pAuthPtr->ServerProt == MSCHAP_PROTOCOL))
        {
            CHAPServerInit (pAuthPtr);
        }
        else
        {

            if (pAuthPtr->ServerProt == EAP_PROTOCOL)
            {
                pIf->LcpStatus.AuthStatus |= EAP_PEER;
                EAPServerInit (pAuthPtr);
            }
            else
            {
                PAPServerInit (pAuthPtr);

            }
        }
    }

    if (pIf->LcpGSEM.pNegFlagsPerIf[AUTH_IDX].FlagMask & ACKED_BY_LOCAL_MASK)
    {
        if (pIf->LcpStatus.AuthStatus & CHAP_WITH_PEER)
        {
            pAuthPtr->ClientProt = CHAP_PROTOCOL;
            CHAPClientInit (pAuthPtr);
        }
        else
        {
            if (pIf->LcpStatus.AuthStatus & PAP_WITH_PEER)
            {
                pAuthPtr->ClientProt = PAP_PROTOCOL;
                PAPClientInit (pAuthPtr);
            }
            else
            {
                if (pIf->LcpStatus.AuthStatus & MSCHAP_WITH_PEER)
                {
                    pAuthPtr->ClientProt = MSCHAP_PROTOCOL;
                    CHAPClientInit (pAuthPtr);
                }
                else
                {
                    if (pIf->LcpStatus.AuthStatus & EAP_WITH_PEER)
                    {
                        pAuthPtr->ClientProt = EAP_PROTOCOL;
                        EAPClientInit (pAuthPtr);
                    }
                }
            }
        }
    }
#ifdef RADIUS_WANTED
    pIf->RadInfo.RadReqId = INIT_VAL;
#endif
    return;
}

/*********************************************************************
*    Function Name    :    AUTHStartServer 
*    Description        :
*                    This function starts the authentication SERVER task after 
*    the link establishment.
*    Parameter(s)      :
*        pAuthPtr    -    Points to the authentication interface entry.
*    Return Value    :    VOID
*********************************************************************/
VOID
AUTHStartServer (tAUTHIf * pAuthPtr)
{

    if (pAuthPtr->ServerProt == PAP_PROTOCOL)
    {
        if (pAuthPtr->ServerInfo.ServerPap.State == PAP_SERVER_STATE_INITIAL)
        {
            pAuthPtr->ServerInfo.ServerPap.State = PAP_SERVER_STATE_CLOSED;
        }
        else
        {
            if (pAuthPtr->ServerInfo.ServerPap.State ==
                PAP_SERVER_STATE_PENDING)
            {
                pAuthPtr->ServerInfo.ServerPap.State = PAP_SERVER_STATE_LISTEN;
            }
        }
        PAPAuthPeer (pAuthPtr);
    }
    if ((pAuthPtr->ServerProt == CHAP_PROTOCOL)
        || (pAuthPtr->ServerProt == MSCHAP_PROTOCOL))
    {
        if ((pAuthPtr->ServerProt == CHAP_PROTOCOL)
            && (pAuthPtr->SecurityInfo.CHAPRemToLocSecrets == 0))
            pAuthPtr->CHAPSearchList = GLOBAL_LIST;
        else if ((pAuthPtr->ServerProt == MSCHAP_PROTOCOL)
                 && (pAuthPtr->SecurityInfo.MSCHAPRemToLocSecrets == 0))
            pAuthPtr->MSCHAPSearchList = GLOBAL_LIST;
        if (pAuthPtr->ServerInfo.ServerChap.State == CHAP_SERVER_STATE_INITIAL)
        {
            pAuthPtr->ServerInfo.ServerChap.State = CHAP_SERVER_STATE_CLOSED;
        }
        else
        {
            if (pAuthPtr->ServerInfo.ServerChap.State ==
                CHAP_SERVER_STATE_PENDING)
            {
                if (pAuthPtr->ServerProt == CHAP_PROTOCOL)
                {
                    CHAPGenChallenge (pAuthPtr);
                    CHAPSendChallenge (pAuthPtr);
                }
                else
                {
                    MSCHAPGenChallenge (pAuthPtr);
                    MSCHAPSendChallenge (pAuthPtr);
                }
                pAuthPtr->ServerInfo.ServerChap.State =
                    CHAP_SERVER_STATE_INITIAL_CHAL;
            }
        }
        if (pAuthPtr->ServerProt == CHAP_PROTOCOL)
        {
            CHAPAuthPeer (pAuthPtr);
        }
        else
        {

            /* Startup Function for MSCHAP will be different as it involves
               Generating Challenge and Sending it across the peer  */
            MSCHAPAuthPeer (pAuthPtr);
        }
    }

    if (pAuthPtr->ServerProt == EAP_PROTOCOL)
    {

        if (pAuthPtr->SecurityInfo.EAPMD5RemToLocSecrets == 0)
        {
            pAuthPtr->EAPMD5SearchList = GLOBAL_LIST;
        }

        if (pAuthPtr->ServerInfo.ServerEap.State == EAP_SERVER_STATE_INITIAL)
        {
            EAPAuthPeer (pAuthPtr);
        }
    }
    return;

}

/*********************************************************************
*    Function Name    :    AUTHStartClient
*    Description        :
*                    This function starts the authentication CLIENT task.
*    Parameter(s)    :
*        pAuthPtr    -  Points to the Authentication interface structure.
*    Return Value     :    VOID
*********************************************************************/
VOID
AUTHStartClient (tAUTHIf * pAuthPtr)
{

    if (pAuthPtr->ClientProt == PAP_PROTOCOL)
    {
        if (pAuthPtr->ClientInfo.ClientPap.State == PAP_CLIENT_STATE_INITIAL)
        {
            pAuthPtr->ClientInfo.ClientPap.State = PAP_CLIENT_STATE_CLOSED;
        }
        else
        {
            if (pAuthPtr->ClientInfo.ClientPap.State ==
                PAP_CLIENT_STATE_PENDING)
            {
                PAPSendAuthReq (pAuthPtr, NORMAL_TRANSMISSION);
            }
            /* send an auth-request */
        }

        /*  if the number of entries in the i/f specific list is zero, 
           then use the global list. NOTE : By default, i/f specific
           list will be used 
         */
        if (pAuthPtr->SecurityInfo.PAPLocToRemSecrets == 0)
        {
            pAuthPtr->PAPSearchList = GLOBAL_LIST;
        }
        PAPAuthWithPeer (pAuthPtr);
    }

    if ((pAuthPtr->ClientProt == MSCHAP_PROTOCOL)
        || (pAuthPtr->ClientProt == CHAP_PROTOCOL))
    {
        if (pAuthPtr->ClientInfo.ClientChap.State == CHAP_CLIENT_STATE_INITIAL)
        {
            pAuthPtr->ClientInfo.ClientChap.State = CHAP_CLIENT_STATE_CLOSED;
        }
        else
        {
            if (pAuthPtr->ClientInfo.ClientChap.State ==
                CHAP_CLIENT_STATE_PENDING)
                pAuthPtr->ClientInfo.ClientChap.State =
                    CHAP_CLIENT_STATE_LISTEN;
        }

        /* Use the same startup Function for CHAP and MSCHAP Clients */
        CHAPAuthWithPeer (pAuthPtr);
    }
    if (pAuthPtr->ClientProt == EAP_PROTOCOL)
    {
        /*    if the number of entries in the i/f specific list is zero, 
           then use the global list. NOTE : By default, i/f specific
           list will be used 
         */
        if (pAuthPtr->SecurityInfo.EAPMD5LocToRemSecrets == 0)
        {
            pAuthPtr->EAPMD5SearchList = GLOBAL_LIST;
        }
        EAPAuthWithPeer (pAuthPtr);
    }
    return;
}

/*********************************************************************
*    Function Name    :    AUTHCloseServer
*    Description        :
*                    This function stops the authentication SERVER task.
*    Parameter(s)    :
*        pAuthPtr    -    Points to the authentication i/f structure.
*    Return Value     :     VOID
*********************************************************************/
VOID
AUTHCloseServer (tAUTHIf * pAuthPtr)
{

    if (pAuthPtr->ServerProt == PAP_PROTOCOL)
    {
        pAuthPtr->ServerInfo.ServerPap.State = PAP_SERVER_STATE_INITIAL;
        PPPStopTimer (&pAuthPtr->ServerInfo.ServerPap.PapResponseTimer);
    }

    if ((pAuthPtr->ServerProt == CHAP_PROTOCOL)
        || (pAuthPtr->ServerProt == MSCHAP_PROTOCOL))
    {
        PPPStopTimer (&pAuthPtr->ServerInfo.ServerChap.ChapChalTimer);
        PPPStopTimer (&pAuthPtr->ServerInfo.ServerChap.ChapReChalTimer);
    }

    if (pAuthPtr->ServerProt == EAP_PROTOCOL)
    {
        PPPStopTimer (&pAuthPtr->ServerInfo.ServerEap.AuthTypeOptions.
                      ServerEAPMD5.ReChalTimer);
        PPPStopTimer (&pAuthPtr->ServerInfo.ServerEap.AuthTypeOptions.
                      ServerEAPMD5.ChalTimer);

        PPPStopTimer (&pAuthPtr->ServerInfo.ServerEap.EapIdentityTimer);
    }
    pAuthPtr->ServerProt = 0;
    return;
}

/*********************************************************************
*    Function Name    :    AUTHCloseClient
*    Description        :
*                    This function is used to stop the CLIENT task.
*    Parameter(s)    :
*        pAuthPtr    -    Points to the authentication interface structure.
*    Return Value    :    VOID
*********************************************************************/
VOID
AUTHCloseClient (tAUTHIf * pAuthPtr)
{

    if (pAuthPtr->ClientProt == PAP_PROTOCOL)
    {
        PPPStopTimer (&pAuthPtr->ClientInfo.ClientPap.PapResendTimer);
        pAuthPtr->ClientInfo.ClientPap.State = PAP_CLIENT_STATE_INITIAL;
    }

    if ((pAuthPtr->ClientProt == CHAP_PROTOCOL)
        || (pAuthPtr->ClientProt == MSCHAP_PROTOCOL))
    {
        PPPStopTimer (&pAuthPtr->ClientInfo.ClientChap.ChapRespTimer);
        pAuthPtr->ClientInfo.ClientChap.State = CHAP_CLIENT_STATE_INITIAL;
        if (pAuthPtr->ClientInfo.ClientChap.pChallenge != NULL)
        {
            FREE_STR (pAuthPtr->ClientInfo.ClientChap.pChallenge);
            pAuthPtr->ClientInfo.ClientChap.pChallenge = NULL;
        }
        if (pAuthPtr->ClientInfo.ClientChap.pResponse != NULL)
        {
            FREE_STR (pAuthPtr->ClientInfo.ClientChap.pResponse);
            pAuthPtr->ClientInfo.ClientChap.pResponse = NULL;
        }
        if (pAuthPtr->ClientInfo.ClientChap.pMSCHAPChgPwdPkt != NULL)
        {
            FREE_STR (pAuthPtr->ClientInfo.ClientChap.pMSCHAPChgPwdPkt);
            pAuthPtr->ClientInfo.ClientChap.pMSCHAPChgPwdPkt = NULL;
        }
    }

    if (pAuthPtr->ClientProt == EAP_PROTOCOL)
    {
        pAuthPtr->ClientInfo.ClientEap.State = EAP_CLIENT_STATE_INITIAL;
    }

    pAuthPtr->ClientProt = 0;
    return;
}

/*********************************************************************
*    Function Name    :    AUTHGetSecret
*    Description        :
*                    This function returns the pointer to the entry in the 
*    Secret table that corressponds to the  input parameters. It is used by 
*    the CHAP CLIENT and PAP SERVER.
*    Parameter(s)    :
*        pAuthPtr    -    Points to the authentication interface structure. 
*        pName        -    Points to the Identity/SecretName.
*        Length        -    Length of the pName parameter.
*        Protocol    -    CHAP/PAP.
*        Type        -    SERVER/CLIENT.
*    Return Value    :    Pointer to the entry in the secret table.
*********************************************************************/
tSecret            *
AUTHGetSecret (tAUTHIf * pAuthPtr, UINT1 *pName, UINT2 Length, UINT2 Protocol,
               UINT1 Type)
{
    tSecret            *pScanList;

    if (Length == 0)
    {
        return NULL;
    }
    if (pAuthPtr != NULL)
    {
        SLL_SCAN (&pAuthPtr->SecurityInfo.SecretList, pScanList, tSecret *)
        {
            if ((pScanList->Protocol == Protocol) &&
                (pScanList->Direction == Type) &&
                (MEMCMP (pScanList->Identity, pName, Length) == 0) &&
                (pScanList->IdentityLen == Length) &&
                (pScanList->Status == SECRETS_STATUS_VALID))
            {                    /*02032001 */
                return (pScanList);
            }
        }
    }

    SLL_SCAN (&IndexZeroSecurityInfo.SecretList, pScanList, tSecret *)
    {
        if ((pScanList->Protocol == Protocol) &&
            (pScanList->Direction == Type) &&
            (MEMCMP (pScanList->Identity, pName, Length) == 0) &&
            (pScanList->IdentityLen == Length) &&
            (pScanList->Status == SECRETS_STATUS_VALID))
        {                        /*02032001 */
            return (pScanList);
        }
    }

    return (NULL);

}

/*********************************************************************
*  Function Name : MSCHAPAUTHGetSecret
*  Description   :
*                   This procedure is used to get the secret corresponding to
*    MSCHAP Protocol. The first secret corresponding to the MSCHAP Protocol
*    is returned
*    The change password packet is sent to the peer.
*  Parameter(s)  :
*           pAuthPtr -  pointer to Auth Infostructure which contains all
*                                    the MSCHAP information.
*           pUserName  - pointer to the user name
*           pChallenge - pointer to the challenge
*            Identifier - Id of the packet to be sent to the peer
*  Return Values : 
*            pointer to the secret retrieved.
*********************************************************************/
tSecret            *
MSCHAPAUTHGetSecret (tAUTHIf * pAuthPtr, UINT1 *pUserName, UINT2 Protocol,
                     UINT1 Type)
{
    tSecret            *pScanList;

    SLL_SCAN (&pAuthPtr->SecurityInfo.SecretList, pScanList, tSecret *)
    {
        if ((pScanList->Protocol == Protocol) && (pScanList->Direction == Type)
            && (pScanList->Status == SECRETS_STATUS_VALID))
        {
            if ((Type == SERVER) &&
                ((pScanList->IdentityLen != STRLEN (pUserName)) ||
                 (MEMCMP (pScanList->Identity, pUserName,
                          pScanList->IdentityLen) != 0)))
                continue;
            return (pScanList);
        }
    }

    SLL_SCAN (&IndexZeroSecurityInfo.SecretList, pScanList, tSecret *)
    {
        if ((pScanList->Protocol == Protocol) && (pScanList->Direction == Type)
            && (pScanList->Status == SECRETS_STATUS_VALID))
        {
            if ((Type == SERVER) &&
                ((pScanList->IdentityLen != STRLEN (pUserName)) ||
                 (MEMCMP (pScanList->Identity, pUserName,
                          pScanList->IdentityLen) != 0)))
                continue;
            return (pScanList);
        }
    }

    return (NULL);
}

/*********************************************************************
*    Function Name    :    AUTHGetSecretName
*    Description        :
*        This function returns the pointer to the entry in the Secret table
*  that corresponds to the  input parameters. It is used by the CHAP SERVER and 
*  PAP CLIENT.
*  Parameter(s)        :
*        pAuthPtr    -    Points to the authentication interface structure.
*        Protocol    -    CHAP/PAP.
*        Type        -    SERVER/CLIENT.
*  Return Value     :    Pointer to the entry in the secret table.
*********************************************************************/
tSecret            *
AUTHGetSecretName (tAUTHIf * pAuthPtr, UINT2 Protocol, UINT1 Type)
{
    tSecret            *pScanPtr = NULL;
    UINT1              *pSearchList;
    INT4               *pLastIdIndex;

    if (Protocol == CHAP_PROTOCOL)
    {
        pSearchList = &pAuthPtr->CHAPSearchList;
        pLastIdIndex = &pAuthPtr->ChapLastSecretIndex;
    }
    else if (Protocol == MSCHAP_PROTOCOL)
    {
        pSearchList = &pAuthPtr->MSCHAPSearchList;
        pLastIdIndex = &pAuthPtr->MSChapLastSecretIndex;
    }
    else if (Protocol == EAPMD5_PROTOCOL)
    {
        pSearchList = &pAuthPtr->EAPMD5SearchList;
        pLastIdIndex = &pAuthPtr->EapMD5LastSecretIndex;
    }
    else
    {
        pSearchList = &pAuthPtr->PAPSearchList;
        pLastIdIndex = &pAuthPtr->PapLastSecretIndex;
    }

    if (*pSearchList == IF_SPECIFIC_LIST)
    {
        if ((pScanPtr =
             AUTHSearchSecretName (pAuthPtr, &pAuthPtr->SecurityInfo, Protocol,
                                   Type, pLastIdIndex)) == NULL)
        {
            *pSearchList = GLOBAL_LIST;
        }
    }

    if (*pSearchList == GLOBAL_LIST)
    {
        pScanPtr =
            AUTHSearchSecretName (pAuthPtr, &IndexZeroSecurityInfo, Protocol,
                                  Type, pLastIdIndex);
    }

    return (pScanPtr);
}

/*********************************************************************
*    Function Name    :    AUTHSearchSecretName
*    Description        :
*                This function is used to get the next available entry
*   in the given list, for the given protocol and type.
*   protocol, type, and    
*    Parameter(s)    :
*        pAuthPtr    -    Points to the authentication interface structure.
*    pSecurityInfo  -   points to the security information list
*       Protocol    -   CHAP/PAP.
*       Type        -   SERVER/CLIENT.
*       pLastIdIndex -  Last Index value 
*  Return Value     :   Pointer to the entry in the secret table.
*********************************************************************/
tSecret            *
AUTHSearchSecretName (tAUTHIf * pAuthPtr, tSecurityInfo * pSecurityInfo,
                      UINT2 Protocol, UINT1 Type, INT4 *pLastIdIndex)
{
    tSecret            *pScanPtr;

    PPP_UNUSED (pAuthPtr);
    SLL_SCAN (&pSecurityInfo->SecretList, pScanPtr, tSecret *)
    {
        if ((pScanPtr->Protocol == EAPMD5_PROTOCOL) &&
            (pScanPtr->Protocol == Protocol) && (pScanPtr->Direction == Type)
            && (pScanPtr->Status == SECRETS_STATUS_VALID))
        {
            *pLastIdIndex = (INT4) pScanPtr->IdIndex;
            return (pScanPtr);
        }
        else
        {
            if ((pScanPtr->Protocol == Protocol) &&
                (pScanPtr->Direction == Type) &&
                ((INT4) pScanPtr->IdIndex > *pLastIdIndex) &&
                (pScanPtr->Status == SECRETS_STATUS_VALID))
            {
                *pLastIdIndex = (INT4) pScanPtr->IdIndex;
                return (pScanPtr);
            }
        }
    }
    return (NULL);
}

/*********************************************************************
*    Function Name    :    AUTHInsertPrefEntry
*    Description        :
*                    This function inserts the Preference/Protocol entry in the 
*    preference list in the sorted order.
*    Parameter(s)    :
*        pAuthPtr    -    Points to the authentication interface structure.
*        NewEntry    -    Preference structure to be inserted.
*    Return Value     :    Index of the Table at which the new entry is inserted. 
*********************************************************************/
INT1
AUTHInsertPrefEntry (tSecurityInfo * pSecurityInfo, tAuthPref NewEntry)
{
    INT1                Index, RevIndex;

    if (pSecurityInfo->NumActiveProt >= MAX_AUTH_PROT)
    {
        PPP_TRC (MGMT, "No of protocols reached maximum..");
        return -1;
    }

    for (Index = 0; Index < pSecurityInfo->NumActiveProt; Index++)
    {
        if (pSecurityInfo->PrefList[Index].Preference > NewEntry.Preference)
            break;
        if (NewEntry.Protocol == CHAP_PROTOCOL)
        {
            if ((pSecurityInfo->PrefList[Index].Protocol == NewEntry.Protocol)
                && (pSecurityInfo->PrefList[Index].ChapAlgo ==
                    NewEntry.ChapAlgo))
                return (Index);
        }
        else
        {
            if (pSecurityInfo->PrefList[Index].Protocol == NewEntry.Protocol)
                return (Index);
        }
        if (pSecurityInfo->PrefList[Index].Preference == NewEntry.Preference)
        {
            pSecurityInfo->PrefList[Index].Protocol = NewEntry.Protocol;
            pSecurityInfo->PrefList[Index].ChapAlgo = NewEntry.ChapAlgo;
            return (Index);
        }
    }

    pSecurityInfo->NumActiveProt++;

    /* 

       In the following code  The RevIndex var should start from NumActiveProt - 1 
       Previously it was starting from NumActiveProt , because of this we were not 
       able to configure 3 Authentication protocols , this has been found out 
       and fixed during CONF_NAK problem handling. (99050011)

     */

    for (RevIndex = (INT1) (pSecurityInfo->NumActiveProt - 1); RevIndex > Index;
         RevIndex--)
    {
        pSecurityInfo->PrefList[RevIndex] =
            pSecurityInfo->PrefList[RevIndex - 1];
    }

    pSecurityInfo->PrefList[RevIndex] = NewEntry;

    return (RevIndex);
}

/*********************************************************************
*    Function Name    :    AUTHInsertSecret
*    Description        :
*                    This function  is used to insert an entry in the secrets 
*    table in the sorted order.
*    Parameter(s)    :
*        pAuthPtr    -    Points to the authentication interface structure
*        pNew        -    Points to the structure to be inserted.
*    Return Values    :    VOID
*********************************************************************/
VOID
AUTHInsertSecret (tSecurityInfo * pSecurityInfo, tSecret * pNew)
{
    tSecret            *pInPtr = NULL;
    tSecret            *pPrevPtr = NULL;

    SLL_SCAN (&pSecurityInfo->SecretList, pInPtr, tSecret *)
    {
        if (pInPtr->IdIndex > pNew->IdIndex)
        {
            break;
        }
        else
        {
            pPrevPtr = pInPtr;
        }
    }

    SLL_INSERT (&pSecurityInfo->SecretList, (t_SLL_NODE *) pPrevPtr,
                (t_SLL_NODE *) pNew);

    if (pNew->Status == VALID)
    {
        AUTHNewSecretUpdateCounters (pSecurityInfo, pNew);
    }

    return;
}

VOID
AUTHNewSecretUpdateCounters (tSecurityInfo * pSecurityInfo, tSecret * pNew)
{
    if (pNew->Direction == LOC_TO_REMOTE)
    {
        if (pNew->Protocol == CHAP_PROTOCOL)
        {
            pSecurityInfo->CHAPLocToRemSecrets++;
        }
        if (pNew->Protocol == PAP_PROTOCOL)
        {
            pSecurityInfo->PAPLocToRemSecrets++;
        }
        if (pNew->Protocol == MSCHAP_PROTOCOL)
        {
            pSecurityInfo->MSCHAPLocToRemSecrets++;
        }

        if (pNew->Protocol == EAPMD5_PROTOCOL)
        {
            pSecurityInfo->EAPMD5LocToRemSecrets++;
        }

    }
    else
    {
        if (pNew->Protocol == CHAP_PROTOCOL)
        {
            pSecurityInfo->CHAPRemToLocSecrets++;
        }
        if (pNew->Protocol == PAP_PROTOCOL)
        {
            pSecurityInfo->PAPRemToLocSecrets++;
        }
        if (pNew->Protocol == MSCHAP_PROTOCOL)
        {
            pSecurityInfo->MSCHAPRemToLocSecrets++;
        }
        if (pNew->Protocol == EAPMD5_PROTOCOL)
        {
            pSecurityInfo->EAPMD5RemToLocSecrets++;
        }
    }
}

/*********************************************************************
*    Function Name    :    AUTHDeletePrefEntry
*    Description        :
*            This function is used to delete a preference entry from the 
*    preference list.
*    Parameter(s)    :
*        pAuthPtr    - Points to the authentication interface structure.
*        Protocol    - CHAP/PAP
*        ChapAlgo    - MD5/MSCHAP_ALGORITHM/0 (for PAP it is 0)
*    Return Values    : VOID
*********************************************************************/
VOID
AUTHDeletePrefEntry (tSecurityInfo * pSecurityInfo, UINT2 Protocol,
                     UINT1 ChapAlgo)
{
    UINT1               Index;

    for (Index = 0; Index < pSecurityInfo->NumActiveProt; Index++)
    {
        if ((pSecurityInfo->PrefList[Index].Protocol == Protocol)
            && (pSecurityInfo->PrefList[Index].ChapAlgo == ChapAlgo))
        {
            break;
        }
    }

    while (Index < pSecurityInfo->NumActiveProt - 1)
    {
        if (Index >= (UINT1) (MAX_AUTH_PROT - 1))
        {
            break;
        }
        pSecurityInfo->PrefList[Index] = pSecurityInfo->PrefList[Index + 1];
        Index++;
    }

    pSecurityInfo->NumActiveProt--;

    return;
}

/*********************************************************************
*    Function Name    :    AUTHDeleteSecretEntry
*    Description        :
*            This function is used to delete an entry from the secret table.
*    Parameter(s)    :
*        pAuthPtr    -     Points to the authentication interface structure.
*        pToDel        -    Points to the Secret to be deleted.
*  Return Values    :    VOID
*********************************************************************/
VOID
AUTHDeleteSecretEntry (tSecurityInfo * pSecurityInfo, tSecret * pToDel)
{
    if (pToDel->Direction == LOC_TO_REMOTE)
    {
        if (pToDel->Protocol == CHAP_PROTOCOL)
        {
            pSecurityInfo->CHAPLocToRemSecrets--;
        }
        if (pToDel->Protocol == PAP_PROTOCOL)
        {
            pSecurityInfo->PAPLocToRemSecrets--;
        }
        if (pToDel->Protocol == MSCHAP_PROTOCOL)
        {
            pSecurityInfo->MSCHAPLocToRemSecrets--;
        }
        if (pToDel->Protocol == EAPMD5_PROTOCOL)
        {
            pSecurityInfo->EAPMD5LocToRemSecrets--;
        }

    }
    else
    {
        if (pToDel->Protocol == CHAP_PROTOCOL)
        {
            pSecurityInfo->CHAPRemToLocSecrets--;
        }
        if (pToDel->Protocol == PAP_PROTOCOL)
        {
            pSecurityInfo->PAPRemToLocSecrets--;
        }
        if (pToDel->Protocol == MSCHAP_PROTOCOL)
        {
            pSecurityInfo->MSCHAPRemToLocSecrets--;
        }
        if (pToDel->Protocol == EAPMD5_PROTOCOL)
        {
            pSecurityInfo->EAPMD5RemToLocSecrets--;
        }

    }

    SLL_DELETE (&pSecurityInfo->SecretList, (t_SLL_NODE *) pToDel);

    FREE_SECRET_ENTRY (pToDel);

    return;
}

/*********************************************************************
*    Function Name    :    AUTHStartAuthentication
*    Description        :
*                This function is used to start authentication process
*  when successfully negotiated.
*    Parameter(s)    :
*        pAuthPtr    -    Points to the authentication interface structure
*        pToDel        -    Points to the Secret to be deleted.
*    Return Values    :    VOID
*********************************************************************/
VOID
AUTHStartAuthentication (tAUTHIf * pAuthPtr)
{
    AUTHStartClient (pAuthPtr);
    AUTHStartServer (pAuthPtr);

    return;
}

/*********************************************************************
*    Function Name    :    AUTHStopAuthentication
*    Description        :
*                This function is used to stop authentication process
*    when closing the link.
*    Parameter(s)    :
*        pAuthPtr    -    Points to the authentication interface structure
*        pToDel        -    Points to the Secret to be deleted.
*    Return Values    :    VOID
*********************************************************************/
VOID
AUTHStopAuthentication (tAUTHIf * pAuthPtr)
{
    AUTHCloseServer (pAuthPtr);
    AUTHCloseClient (pAuthPtr);
    return;
}

INT1
PPP_compare_oids (oid1, oid2)
     tSNMP_OID_TYPE     *oid1;
     tSNMP_OID_TYPE     *oid2;
{
    UINT4               i, u4_min_length;

    if (!oid1 && !oid2)
    {
        return EQUAL;
    }
    if (oid1 == NULL)
    {
        if (oid2->u4_Length == 0)
        {
            return EQUAL;
        }
        else
        {
            return OK;
        }
    }
    if (oid2 == NULL)
    {
        if (oid1->u4_Length == 0)
        {
            return EQUAL;
        }
        else
        {
            return OK;
        }
    }
    u4_min_length = (oid1->u4_Length < oid2->u4_Length) ?
        oid1->u4_Length : oid2->u4_Length;
    for (i = 0; i < u4_min_length; i++)
    {
        if (oid1->pu4_OidList[i] != oid2->pu4_OidList[i])
            return (NOTOK);
    }
    if (oid1->u4_Length == oid2->u4_Length)
    {
        return (EQUAL);
    }
    return (OK);
}

/*********************************************************************
*    Function Name    :   PPPGetRandom 
*    Description      :   This function is used to generate a random 
*                         string of the specified length
*    Parameter(s)     : 
*        pu1Ptr - Pointer where the random number has to be stored.
*         u4Len - Required length of random number
*    Return Values    :    VOID
*********************************************************************/
VOID
PPPGetRandom (UINT1 *pu1Ptr, UINT4 u4Len)
{
    UINT4               u4Temp,i4RetVal=0;
    UINT4               u4Seed;
    UINT1              *pu1Temp = pu1Ptr;

    i4RetVal = OSIX_SEED (u4Seed);
    UNUSED_PARAM (i4RetVal);
    OSIX_SRAND (u4Seed);
    while (u4Len > 4)
    {
        u4Temp = (UINT4) OSIX_RAND (0ul, OSIX_RAND_MAX);
        MEMCPY (pu1Temp, &u4Temp, 4);
        pu1Temp += 4;
        u4Len -= 4;
    }
    if (u4Len > 0)
    {
        u4Temp = (UINT4) OSIX_RAND (0ul, OSIX_RAND_MAX);
        MEMCPY (pu1Temp, &u4Temp, u4Len);
    }
}
