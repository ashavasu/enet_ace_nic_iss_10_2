/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppeap.c,v 1.5 2014/09/01 11:00:55 siva Exp $
 *
 * Description:This file contains the interface and internal 
 *             procedures of EAP
 *
 *******************************************************************/

#include "authcom.h"
#include "pppmd5.h"
#include "lcpdefs.h"
#include "lcpexts.h"
#include "gcpdefs.h"
#include "gsemdefs.h"
#include "globexts.h"
#include "genexts.h"
#include "genproto.h"

/* #ifdef L2TP_WANTED */
#include "pppl2tp.h"
#include "l2tp.h"
/* #endif */
/*********************************************************************
*  Function Name    :   EAPClientInit
*  Description        :   This function initialises the ClientEap structure
*                     and is invoked by the AuthInitServices() function. 
*                     It sets the AuthType value and sets the client 
*                     state to INITIAL .
*    Parameter(s)    :
*        pAuthPtr -    Points to the authentication interface 
*                               structure which has the ClientEap structure 
*                               to be initialized.
*    Return Values    :    VOID
*********************************************************************/
VOID
EAPClientInit (tAUTHIf * pAuthPtr)
{

    pAuthPtr->ClientInfo.ClientEap.AuthType = EAP_TYPE_MD5;
    pAuthPtr->ClientInfo.ClientEap.State = EAP_CLIENT_STATE_INITIAL;

    switch (pAuthPtr->ClientInfo.ClientEap.AuthType)
    {
        case EAP_TYPE_MD5:
            EAPMD5ClientInit (pAuthPtr);
            break;
        default:
            break;
    }
    return;
}

/*********************************************************************
*    Function Name    :   EAPServerInit
*    Description    :   This function initialises the ServerEap 
*                           structure and is invoked by the AuthInitServices()
*                           function. It sets the time out value and maximam 
*                           retransmission to their default values and  sets 
*                        the client state to INITIAL .
*    Parameter(s)    :
*        pAuthPtr-   Points to the authentication interface structure
*                      which has the ServerEap structure to be 
*                           initialized.
*    Return Values    :    VOID
*********************************************************************/
VOID
EAPServerInit (tAUTHIf * pAuthPtr)
{

    pAuthPtr->ServerInfo.ServerEap.State = EAP_SERVER_STATE_INITIAL;

    pAuthPtr->ServerInfo.ServerEap.TimeoutTime =
        pAuthPtr->pIf->LinkInfo.TimeOutTime;

    pAuthPtr->ServerInfo.ServerEap.MaxTransmits =
        pAuthPtr->pIf->LinkInfo.MaxReTransmits;

    pAuthPtr->ServerInfo.ServerEap.FailedIdentityRequests = 0;

    pAuthPtr->ServerInfo.ServerEap.MaxIdentityRetries =
        pAuthPtr->pIf->LinkConfigs.EapMaxIdentityRetries;
    pAuthPtr->ServerInfo.ServerEap.IdentityNeeded =
        pAuthPtr->pIf->LinkConfigs.EapIdentityNeeded;

    pAuthPtr->ServerInfo.ServerEap.DefaultAuthType =
        (UINT1) pAuthPtr->pIf->LinkConfigs.EapDefaultAuthType;

    pAuthPtr->ServerInfo.ServerEap.AuthType = EAP_TYPE_MD5;

    switch (pAuthPtr->ServerInfo.ServerEap.AuthType)
    {
        case EAP_TYPE_MD5:
            EAPMD5ServerInit (pAuthPtr);
            break;
        default:
            break;
    }
    return;
}

/*********************************************************************
*    Function Name    :  EAPAuthWithPeer
*    Description    :  This function is called by the PPP Authentication 
*                          Module for authenticating the local entity with 
*                          the remote using EAP. It sets the client state to 
*                          LISTEN and waits for a request from Peer. 
*    Parameter(s)    :
*        pAuthPtr-  Pointer to the EAP interface structure for which the
*                          authentication  need to be done
*
*    Return Values    :    VOID
*********************************************************************/
VOID
EAPAuthWithPeer (tAUTHIf * pAuthPtr)
{

    pAuthPtr->ClientInfo.ClientEap.State = EAP_CLIENT_STATE_LISTEN;

    return;
}

/*********************************************************************
*    Function Name    :  EAPAuthPeer
*    Description    :  This function is called by the PPP Authentication 
*                          Module for authenticating the Peer using EAP. 
*    Parameter(s)    :
*        pAuthPtr    -  Points to the interface structure for which the EAP
*               authentication  need to be done.
*
*    Return Values    :  VOID
*********************************************************************/
VOID
EAPAuthPeer (tAUTHIf * pAuthPtr)
{

    if (pAuthPtr->ServerInfo.ServerEap.IdentityNeeded == EAP_SEND_IDENTITY)
    {
        EAPGenIdentityRequest (pAuthPtr);
        EAPSendIdentityRequest (pAuthPtr);
        pAuthPtr->ServerInfo.ServerEap.State = EAP_SERVER_STATE_IDENTITY;
    }
    else
    {
        switch (pAuthPtr->ServerInfo.ServerEap.AuthType)
        {

            case EAP_TYPE_MD5:
                EAPMD5GenChallenge (pAuthPtr);
                EAPMD5SendChallenge (pAuthPtr);
                pAuthPtr->ServerInfo.ServerEap.AuthTypeOptions.ServerEAPMD5.
                    State = EAPMD5_SERVER_STATE_INITIAL_CHAL;
                break;
            default:
                break;
        }
    }
    return;
}

/*********************************************************************
*    Function Name    : EAPIdentityTimeOut
*
*    Description    : This function is called when the retransmission 
*                         timer expires after sending an EAP Identity request 
*                         packet. It calls the appropriate function to 
*                         retransmit the packet.
*    Parameter(s)    :
*        pAuthPtr  -  Pointer to the authentication interface structure.
*
*    Return Values    :    VOID
*********************************************************************/
VOID
EAPIdentityTimeOut (VOID *pAuth)
{
    tAUTHIf            *pAuthPtr;
    pAuthPtr = (tAUTHIf *) pAuth;
    if (pAuthPtr->ServerInfo.ServerEap.State != EAP_SERVER_STATE_IDENTITY)
        return;

    if (pAuthPtr->ServerInfo.ServerEap.RequestTransmits >=
        pAuthPtr->ServerInfo.ServerEap.MaxTransmits)
    {

        PPP_TRC (ALL_FAILURE, "Peer failed to respond to EAP Identity Request");

        pAuthPtr->ServerInfo.ServerEap.State = EAP_SERVER_STATE_BADAUTH;
        LCPAuthProcessFailed (pAuthPtr->pIf);

        if (pTermGSEM != NULL)
        {
            GSEMRun (pTermGSEM, RXJ_MINUS);
        }
    }
    else
    {
        EAPSendIdentityRequest (pAuthPtr);
    }
    return;
}

/*********************************************************************
*    Function Name    :  EAPRecdProtRej
*    Description    :  This procedure is used  to process a protocol 
*                          reject received for the EAP protocol.
*    Parameter(s)    :
*        pAuthPtr-  Pointer to the EAP interface structure.
*    Return Values    :  VOID
*********************************************************************/
VOID
EAPRecdProtRej (tAUTHIf * pAuthPtr)
{

    if (pAuthPtr->ServerProt == EAP_PROTOCOL)
    {
        if ((pAuthPtr->ServerInfo.ServerEap.State != EAP_SERVER_STATE_INITIAL)
            && (pAuthPtr->ServerInfo.ServerEap.State !=
                EAP_SERVER_STATE_CLOSED))
            LCPAuthProcessFailed (pAuthPtr->pIf);
    }

    AUTHCloseServer (pAuthPtr);
    AUTHCloseClient (pAuthPtr);

    return;
}

/*********************************************************************
*    Function Name    :  EAPInput
*    Description    :  This procedure is called by the LLI whenever 
*                          there is an incoming packet for EAP module. 
*                          It processes the EAP packet and calls 
*                       the internal functions  to take appropriate actions.
*    Parameter(s)    :
*            pAuthPtr -  Points to the EAP interface structure 
*                           corresponding to the incoming packet
*        pInpacket-  Points to the incoming packet
*        Length     -  Length of the incoming packet.
*    Return Values    :    VOID
*********************************************************************/
VOID
EAPInput (tAUTHIf * pAuthPtr, t_MSG_DESC * pInpacket, UINT2 Length)
{
    UINT1               Code = 0;
    UINT1               Id = 0;
    UINT2               Len;

    if (UtilExtractAndValidateCPHdr
        (pAuthPtr->pIf, pInpacket, Length, &Code, &Id, &Len) == DISCARD)
    {
        return;
    }

    switch (Code)
    {

        case EAP_REQUEST:
            if ((pAuthPtr->pIf->LcpGSEM.pNegFlagsPerIf[AUTH_IDX].FlagMask
                 & ACKED_BY_LOCAL_MASK) == 0)
                return;

            if (pAuthPtr->ClientProt == EAP_PROTOCOL)
                EAPRcvdAuthRequest (pAuthPtr, pInpacket, Len, Id);
            else
                PPPSendRejPkt (pAuthPtr->pIf, pInpacket, Length, LCP_PROTOCOL,
                               PROT_REJ_CODE);

            break;

        case EAP_RESPONSE:
            if ((pAuthPtr->pIf->LcpGSEM.pNegFlagsPerIf[AUTH_IDX].FlagMask
                 & ACKED_BY_PEER_MASK) == 0)
                return;

            if (pAuthPtr->ServerProt == EAP_PROTOCOL)
                EAPRcvdAuthResponse (pAuthPtr, pInpacket, Len, Id);
            else
                PPPSendRejPkt (pAuthPtr->pIf, pInpacket, Length, LCP_PROTOCOL,
                               PROT_REJ_CODE);

            break;

        case EAP_SUCCESS:
            if ((pAuthPtr->pIf->LcpGSEM.pNegFlagsPerIf[AUTH_IDX].FlagMask
                 & ACKED_BY_LOCAL_MASK) == 0)
                return;

            if (pAuthPtr->ClientProt == EAP_PROTOCOL)
                EAPRcvdAuthSuccess (pAuthPtr, pInpacket, Len, Id);
            else
                PPPSendRejPkt (pAuthPtr->pIf, pInpacket, Length, LCP_PROTOCOL,
                               PROT_REJ_CODE);
            break;

        case EAP_FAILURE:
            if ((pAuthPtr->pIf->LcpGSEM.pNegFlagsPerIf[AUTH_IDX].FlagMask
                 & ACKED_BY_LOCAL_MASK) == 0)
                return;

            if (pAuthPtr->ClientProt == EAP_PROTOCOL)
                EAPRcvdAuthFailure (pAuthPtr, pInpacket, Len, Id);
            else
                PPPSendRejPkt (pAuthPtr->pIf, pInpacket, Length, LCP_PROTOCOL,
                               PROT_REJ_CODE);
            break;

        default:
            DISCARD_PKT (pAuthPtr->pIf, "Unknown EAP Code received.");
    }
    return;
}

/*********************************************************************
*    Function Name    :    EAPRcvdAuthRequest
*    Description        :
*                    This function  is called by the 
* EAPInput when the EAP Request packet is received from the Peer. It checks 
* the Type of the request packet and calls the appropriate fucntions 
* to handle the request from the peer
*    Parameter(s)    :
*        pAuthPtr    -    Pointer to authentication interface which contains 
*                        all the EAP information.
*        pInpacket    -    Pointer to  Input Packet String.
*        Length        -    Length of the Input Packet.
*        Identifier    -    Identifier received along with the packet and to
*                        be used in response
*    Return Values    :    VOID
*********************************************************************/
VOID
EAPRcvdAuthRequest (tAUTHIf * pAuthPtr, t_MSG_DESC * pInpacket, UINT2 Length,
                    UINT1 Identifier)
{

    UINT1               Type;

    PPP_TRC (CONTROL_PLANE, "EAP_AUTH_REQ \t IN :");
    if (Length < 1)
    {
        DISCARD_PKT (pAuthPtr->pIf, "Pkt discarded due to Invalid length");
    }
    pAuthPtr->pIf->LLHCounters.InGoodOctets += Length;
    /* get the request type */
    EXTRACT1BYTE (pInpacket, Type);
    Length--;
    switch (Type)
    {

        case EAP_TYPE_IDENTITY:
            EAPProcessIdentityRequest (pAuthPtr, pInpacket, Length, Identifier);
            break;
        case EAP_TYPE_NOTIFICATION:
            EAPProcessNotificationRequest (pAuthPtr, pInpacket, Length,
                                           Identifier);
            break;

            /* Note : NAK Type EAP packet is valid only in Response Packet */
        case EAP_TYPE_MD5:
            EAPMD5RcvdChallenge (pAuthPtr, pInpacket, Length, Identifier);
            break;
        default:
            DISCARD_PKT (pAuthPtr->pIf, "Unknown Type of EAP packet received.");
            break;
    }
    return;
}

/*********************************************************************
*    Function Name    :    EAPRcvdAuthResponse
*    Description        :
*                    This function  is called by the EAPInput when the 
*    EAP response is received from the Peer. It checks the response and 
*    sends TRUE/FALSE  based on the response.
*    Parameter(s)    :
*        pAuthPtr    -    Pointer to EAPState structure which contains all 
*                        the EAP information.
*        pInpacket    -    Pointer to  Input Packet String.
*        Length        -    Length of the Input Packet
*        Identifier    -    Identifier received along with the packet and
*                        to be used in response.
*    Return Values    :    VOID
*********************************************************************/

VOID
EAPRcvdAuthResponse (tAUTHIf * pAuthPtr, t_MSG_DESC * pInpacket, UINT2 Length,
                     UINT1 Identifier)
{

    UINT1               Type;

    PPP_TRC (CONTROL_PLANE, "EAP_AUTH_RESPONSE \t IN :");
    if (Length < 1)
    {
        DISCARD_PKT (pAuthPtr->pIf, "Pkt discarded due to Invalid length");
    }
    pAuthPtr->pIf->LLHCounters.InGoodOctets += Length;

    EXTRACT1BYTE (pInpacket, Type);

    Length--;
    switch (Type)
    {

        case EAP_TYPE_IDENTITY:
            EAPProcessIdentityResponse (pAuthPtr, pInpacket, Length,
                                        Identifier);
            break;
        case EAP_TYPE_NOTIFICATION:
            EAPProcessNotificationResponse (pAuthPtr, pInpacket, Length,
                                            Identifier);
            break;
        case EAP_TYPE_NAK_RESPONSE:
            EAPProcessNakResponse (pAuthPtr, pInpacket, Length, Identifier);
            break;
        case EAP_TYPE_MD5:
            EAPMD5RcvdResponse (pAuthPtr, pInpacket, Length, Identifier);
            break;
        default:
            DISCARD_PKT (pAuthPtr->pIf, "Unknown Type of EAP packet received.");

    }
    return;
}

/*********************************************************************
*  Function Name : EAPRcvdAuthSuccess
*  Description   :
*                       This function  is called by  EAPInput when the
*  Authentication Success Reply is received from the peer. This function
*  indicates the  LCP that authentication if success, if there is no other
*  authentication pending.
*  Parameter(s)    :
*        pAuthPtr  -  pointer to EAPState structure which contains
*                                    all the EAP information.
*        pInpacket -  pointer to  Input Packet String.
*        Length    -  Length of the Input Packet
*        Identifier-  Identifier received along with the packet and to
*                                   be used in response
*  Return Values : VOID
*********************************************************************/
VOID
EAPRcvdAuthSuccess (tAUTHIf * pAuthPtr, t_MSG_DESC * pInpacket, UINT2 Length,
                    UINT1 Identifier)
{
    tEAPMD5ClientInfo  *pClientEAPMD5;

    PPP_UNUSED (pInpacket);
    PPP_TRC (CONTROL_PLANE, "EAP_AUTH_SUCCESS \t IN :");
    pAuthPtr->pIf->LLHCounters.InGoodOctets += Length;

    switch (pAuthPtr->ClientInfo.ClientEap.AuthType)
    {

        case EAP_TYPE_MD5:

            pClientEAPMD5 =
                &pAuthPtr->ClientInfo.ClientEap.AuthTypeOptions.ClientEAPMD5;

            if (Identifier != pClientEAPMD5->ResponseId)
            {
                DISCARD_PKT (pAuthPtr->pIf, "Invalid Id in  response.");
                return;
            }

            pAuthPtr->ClientInfo.ClientEap.State = EAP_CLIENT_STATE_OPEN;

            if ((pClientEAPMD5->State == EAPMD5_CLIENT_STATE_OPEN) ||
                (pClientEAPMD5->State != EAPMD5_CLIENT_STATE_RESPONSE))
                return;

            pClientEAPMD5->State = EAPMD5_CLIENT_STATE_OPEN;

            LCPUpdateAuthStatus (pAuthPtr->pIf, EAP_WITH_PEER_TRUE);
            break;
        default:
            break;
    }

    return;
}

/*********************************************************************
*  Function Name : EAPRcvdAuthFailure
*  Description   :
*                       This function  is called by EAPInput when there is an
*   authentication failure packet sent by peer. It  validates the  input packet
*   and  informs LCP that the authentication has failed.
*  Parameter(s)  :
*           pAuthPtr  -  pointer to EAPState structure which contains all
*                                    the EAP information.
*           pInpacket -  pointer to  Input Packet String.
*           Length    -  Length of the Input Packet
*           Identifier-  Identifier received along with the packet and to
*                                    be used in response
*  Return Values : VOID
*********************************************************************/
VOID
EAPRcvdAuthFailure (tAUTHIf * pAuthPtr, t_MSG_DESC * pInpacket, UINT2 Length,
                    UINT1 Identifier)
{

    tEAPMD5ClientInfo  *pClientEAPMD5;

    pAuthPtr->pIf->LLHCounters.InGoodOctets += Length;

    PPP_UNUSED (pInpacket);
    PPP_TRC (CONTROL_PLANE, "EAP_AUTH_FAILURE \t IN :");
    switch (pAuthPtr->ClientInfo.ClientEap.AuthType)
    {

        case EAP_TYPE_MD5:

            pClientEAPMD5 =
                &pAuthPtr->ClientInfo.ClientEap.AuthTypeOptions.ClientEAPMD5;

            if (Identifier != pClientEAPMD5->ResponseId)
            {
                DISCARD_PKT (pAuthPtr->pIf, "Invalid Id in  response.");
                return;
            }

            pAuthPtr->ClientInfo.ClientEap.State = EAP_CLIENT_STATE_BADAUTH;

            if (pClientEAPMD5->State != EAPMD5_CLIENT_STATE_RESPONSE)
                return;

            pClientEAPMD5->State = EAPMD5_CLIENT_STATE_BADAUTH;

            PPP_TRC (ALL_FAILURE, " authentication failed");
            break;
        default:
            break;

    }

    /* Wait for  the Peer to terminate the link  */
    return;
}

/*********************************************************************
 *  Function Name : EAPGenIdentityRequest
 *  Description   : This function is used by the  EAPAuthPeer()
 *                  function to generate the pseudo-random identity string 
 *                  that is to be sent in the initial Identity Request packet.
 *  Parameter(s)  :
 *           pAuthPtr  -  pointer to authentication structure which contains
 *                                    all the EAP information.
 *  Return Values : VOID
 *********************************************************************/
VOID
EAPGenIdentityRequest (tAUTHIf * pAuthPtr)
{
    UINT1               Identity[EAP_MAX_IDENTITY_STRING_LENGTH];

    STRCPY (Identity, "Sample Identity Message");

    pAuthPtr->ServerInfo.ServerEap.IdentityStringLength =
        (UINT1) STRLEN (Identity);

    pAuthPtr->ServerInfo.ServerEap.IdentityId =
        (INT1) pAuthPtr->ServerInfo.ServerEap.CurrentId++;
    pAuthPtr->ServerInfo.ServerEap.RequestTransmits = 0;

    STRCPY (pAuthPtr->ServerInfo.ServerEap.IdentityString, Identity);

    return;
}

/*********************************************************************
*  Function Name : EAPSendIdentityRequest
*  Description   :
*                       This function is used to transmit the initial Identity
*                       to the peer and is used by the EAPAuthPeer() function, 
*                       after calling EAPGenIdentity(). Its also used by the 
*                       EAPIdentityTimeOut() to resend the Identity to the 
*                       peer.
*  Parameter(s)  :
*       pAuthPtr -  pointer to authentication structure which contains
 *                                       all the EAP information.
 *  Return Values : VOID
*********************************************************************/
VOID
EAPSendIdentityRequest (tAUTHIf * pAuthPtr)
{
    UINT2               Length;
    t_MSG_DESC         *pOutBuf;

    /* (Code + Identifier + Length )+ Type + IdenityString */

    Length = (UINT2) (CP_HDR_LEN + EAP_TYPE_FIELD_LEN +
                      pAuthPtr->ServerInfo.ServerEap.IdentityStringLength);

    if ((pOutBuf =
         (t_MSG_DESC *) (VOID *) ALLOCATE_BUFFER (PPP_INFO_OFFSET + Length,
                                                  PPP_INFO_OFFSET)) != NULL)
    {

        FILL_PKT_HEADER (pOutBuf, EAP_REQUEST,
                         (UINT1) pAuthPtr->ServerInfo.ServerEap.IdentityId,
                         Length);
        APPEND1BYTE (pOutBuf, EAP_TYPE_IDENTITY);
        APPENDSTR (pOutBuf, pAuthPtr->ServerInfo.ServerEap.IdentityString,
                   pAuthPtr->ServerInfo.ServerEap.IdentityStringLength);

        PPPLLITxPkt (pAuthPtr->pIf, pOutBuf, Length, EAP_PROTOCOL);

        pAuthPtr->ServerInfo.ServerEap.EapIdentityTimer.Param1 =
            PTR_TO_U4 (pAuthPtr);
        PPPStartTimer (&pAuthPtr->ServerInfo.ServerEap.EapIdentityTimer,
                       EAP_IDENTITY_TIMER,
                       pAuthPtr->ServerInfo.ServerEap.TimeoutTime);

        /* Increment number of retransmits */
        ++pAuthPtr->ServerInfo.ServerEap.RequestTransmits;

    }
    else
    {
        PRINT_MEM_ALLOC_FAILURE;
    }
    return;
}

/*********************************************************************
*  Function Name :EAPSendStatus
*
*  Description   :This procedure is used to send the authentication status
*                 (SUCCESS/Failure) message to the peer.
*  Parameter(s)  :
*           pAuthPtr -  pointer to authentication structure which contains all
*                       the EAP information.
*           Code     -  Indicates whether the status is  SUCCESS /FAILURE
*  Return Values : VOID
*********************************************************************/
VOID
EAPSendStatus (tAUTHIf * pAuthPtr, UINT1 Code)
{
    t_MSG_DESC         *pOutBuf;
    UINT2               Length;
    UINT1               Identifier;

    Length = CP_HDR_LEN;

    switch (pAuthPtr->ServerInfo.ServerEap.AuthType)
    {
        case EAP_TYPE_MD5:
            Identifier =
                pAuthPtr->ServerInfo.ServerEap.AuthTypeOptions.ServerEAPMD5.
                ChallengeId;
            break;
        default:
            return;
    }

    if ((pOutBuf =
         (t_MSG_DESC *) (VOID *) ALLOCATE_BUFFER (PPP_INFO_OFFSET + Length,
                                                  PPP_INFO_OFFSET)) != NULL)
    {
        FILL_PKT_HEADER (pOutBuf, Code, Identifier, Length);
        PPPLLITxPkt (pAuthPtr->pIf, pOutBuf, Length, EAP_PROTOCOL);
    }
    else
    {
        PRINT_MEM_ALLOC_FAILURE;
    }
    return;
}

/*********************************************************************
*  Function Name :EAPProcessIdentityRequest
*
*  Description   :This procedure is used to process the identity request
*                 and send a response identity in reply to the request
*  Parameter(s)  :
*           pAuthPtr -  pointer to authentication structure which contains all
*                       the EAP information.
*           pInpacket -  pointer to  Input Packet String.
*           Length    -  Length of the Input Packet
*           Identifier-  Identifier received along with the packet and to
*                                    be used in response
*  Return Values : VOID
*********************************************************************/

VOID
EAPProcessIdentityRequest (tAUTHIf * pAuthPtr, t_MSG_DESC * pInpacket,
                           UINT2 Length, UINT1 Identifier)
{

    t_MSG_DESC         *pOutBuf;
    UINT2               RespPktLen;
    UINT2               IdLen;
    UINT1               Identity[EAP_MAX_IDENTITY_STRING_LENGTH];
    /* 
     * The usage of Identity packet is not clearly explained in RFC 2284. 
     * Here, the same identity string(Type-Data field of Idenity Request)
     * is used as an identity response in reply to the request
     */

      /*****************************************************************
      * The following code portion should be modified for any          *
      * specific requirements in handling identity Request packets     * 
      ******************************************************************/

    /* At this point Length contains size of the identity string */

    IdLen = (UINT2) (((Length > EAP_MAX_IDENTITY_STRING_LENGTH)
                      ? EAP_MAX_IDENTITY_STRING_LENGTH : Length));

    EXTRACTSTR (pInpacket, Identity, IdLen);
    IdLen = (UINT2) STRLEN (pAuthPtr->pIf->au1HostName);

    RespPktLen = (UINT2) (CP_HDR_LEN + EAP_TYPE_FIELD_LEN + IdLen);

    if ((pOutBuf =
         (t_MSG_DESC *) (VOID *) ALLOCATE_BUFFER (PPP_INFO_OFFSET + RespPktLen,
                                                  PPP_INFO_OFFSET)) != NULL)
    {

        FILL_PKT_HEADER (pOutBuf, EAP_RESPONSE, Identifier, RespPktLen);
        APPEND1BYTE (pOutBuf, EAP_TYPE_IDENTITY);

        APPENDSTR (pOutBuf, pAuthPtr->pIf->au1HostName, IdLen);

        PPPLLITxPkt (pAuthPtr->pIf, pOutBuf, RespPktLen, EAP_PROTOCOL);

    }
    else
    {
        PRINT_MEM_ALLOC_FAILURE;
    }
    return;
}

/*********************************************************************
*  Function Name :EAPProcessNotificationRequest
*
*  Description   :This procedure is used to process the notification request
*                 and send ack reply in reply to the request
*  Parameter(s)  :
*           pAuthPtr -  pointer to authentication structure which contains all
*                       the EAP information.
*           pInpacket -  pointer to  Input Packet String.
*           Length    -  Length of the Input Packet
*           Identifier-  Identifier received along with the packet and to
*                                    be used in response
*  Return Values : VOID
********************************************************************/

VOID
EAPProcessNotificationRequest (tAUTHIf * pAuthPtr, t_MSG_DESC * pInpacket,
                               UINT2 Length, UINT1 Identifier)
{

    t_MSG_DESC         *pOutBuf;
    UINT2               RespPktLen;
    UINT1               Type;
    /* 
     *  Response is sent with the Type-Data field of zero bytes long
     *  in reply to the notification request */

    EXTRACT1BYTE (pInpacket, Type);
    Length--;

    /* Log the notification message */

    /* 
     * The code for logging the notification request should come here
     */

    RespPktLen = CP_HDR_LEN + EAP_TYPE_FIELD_LEN;

    if ((pOutBuf =
         (t_MSG_DESC *) (VOID *) ALLOCATE_BUFFER (PPP_INFO_OFFSET + RespPktLen,
                                                  PPP_INFO_OFFSET)) != NULL)
    {

        FILL_PKT_HEADER (pOutBuf, EAP_RESPONSE, Identifier, RespPktLen);
        APPEND1BYTE (pOutBuf, EAP_TYPE_NOTIFICATION);
        PPPLLITxPkt (pAuthPtr->pIf, pOutBuf, RespPktLen, EAP_PROTOCOL);

    }
    else
    {
        PRINT_MEM_ALLOC_FAILURE;
    }
    return;
}

/*********************************************************************
*  Function Name :EAPProcessIdentityResponse
*
*  Description   :This procedure is used to process the identity response
*                 and starts the MD5 authentication
*  Parameter(s)  :
*           pAuthPtr -  pointer to authentication structure which contains all
*                       the EAP information.
*           pInpacket -  pointer to  Input Packet String.
*           Length    -  Length of the Input Packet
*           Identifier-  Identifier received along with the packet and to
*                                    be used in response
*  Return Values : VOID
*********************************************************************/

VOID
EAPProcessIdentityResponse (tAUTHIf * pAuthPtr, t_MSG_DESC * pInpacket,
                            UINT2 Length, UINT1 Identifier)
{

    tEAPMD5ServerInfo  *pServerEAPMD5;

#ifdef RADIUS_WANTED
    UINT1               UserName[MAX_SECRET_SIZE + 1];
    UINT1               au1EapPkt[EAP_PKT_MAX_LEN];
    UINT1               NameLen;
    UINT2               u2EapPktLen = 0;
    INT2                Code;

    BZERO (&UserName, MAX_SECRET_LENGTH + 1);
    BZERO (au1EapPkt, EAP_PKT_MAX_LEN);

#endif /* RADIUS_WANTED */

    PPP_UNUSED (pInpacket);
    PPP_UNUSED (Length);
      /*****************************************************************
      * The following code portion should be modified for any            *
      * specific requirements in handling identity Response epackets     * 
      ******************************************************************/

    if (pAuthPtr->ServerInfo.ServerEap.IdentityId != Identifier)
    {
        DISCARD_PKT (pAuthPtr->pIf, "Invalid Id in EAP Identity response.");
        return;
    }

    PPPStopTimer (&pAuthPtr->ServerInfo.ServerEap.EapIdentityTimer);

#ifdef RADIUS_WANTED
#ifdef L2TP_WANTED
    if (PPPIsL2TPTunnelModeLAC (pAuthPtr->pIf) == FALSE)
    {
#endif

        /*
           If we have received a duplicate or bogus Response, we have to send 
           the same answer (Success/Failure) as we did for the first Response 
           we saw.
         */

        if (pAuthPtr->ServerInfo.ServerEap.State == EAP_SERVER_STATE_OPEN)
        {
            EAPSendStatus (pAuthPtr, EAP_SUCCESS);
            return;
        }

        if (pAuthPtr->ServerInfo.ServerEap.State == EAP_SERVER_STATE_BADAUTH)
        {
            EAPSendStatus (pAuthPtr, EAP_FAILURE);
            return;
        }
        if (pAuthPtr->ServerInfo.ServerChap.State == EAP_SERVER_STATE_PENDING)
        {
            PPP_TRC (CONTROL_PLANE, "Response already send to RADIUS Server");
            return;
        }
#ifdef L2TP_WANTED
    }
#endif

    pAuthPtr->pIf->LLHCounters.InGoodOctets += Length;
#endif

#ifdef RADIUS_WANTED
    NameLen = (UINT1) Length;

    EXTRACTSTR (pInpacket, UserName, NameLen);
    /*
     * Get secret for authenticating them with us,
     * do the hash ourselves, and compare the result.
     */

    if (gu1aaaMethod == AAA_RADIUS)
    {
        PPP_TRC (CONTROL_PLANE, "Authenticating using RADIUS Server...");
        GETSTR (pInpacket, (UINT1 *) &u2EapPktLen, 4, 2);
        u2EapPktLen = OSIX_NTOHS (u2EapPktLen);
        GETSTR (pInpacket, au1EapPkt, 2, u2EapPktLen);
        Code =
            (INT2) (PppRadEapAuthenticate
                    (UserName, NameLen, au1EapPkt, u2EapPktLen,
                     pAuthPtr->pIf->LinkInfo.IfIndex, NULL, 0));
        pAuthPtr->pIf->RadInfo.RadReqId = (INT1) Code;

        if (Code != NOT_OK)
        {
            pAuthPtr->ServerInfo.ServerEap.State = EAP_SERVER_STATE_PENDING;
            pAuthPtr->pIf->RadInfo.PppAuthId = Identifier;
            pAuthPtr->pIf->RadInfo.Username =
                (UINT1 *) CALLOC (PPP_MAX_NAME_LENGTH, 1);
            MEMCPY (pAuthPtr->pIf->RadInfo.Username, UserName, NameLen);
            return;
        }

        PPP_TRC (ALL_FAILURE,
                 "Err in contacting RAD Server!.Locally authenticating");
    }

#endif /* RADIUS_WANTED */

    switch (pAuthPtr->ServerInfo.ServerEap.AuthType)
    {

        case EAP_TYPE_MD5:
            EAPMD5GenChallenge (pAuthPtr);
            EAPMD5SendChallenge (pAuthPtr);
            pServerEAPMD5 =
                &pAuthPtr->ServerInfo.ServerEap.AuthTypeOptions.ServerEAPMD5;
            pServerEAPMD5->State = EAPMD5_SERVER_STATE_INITIAL_CHAL;
            break;
        default:
            break;
    }

}

/*********************************************************************
*  Function Name :EAPProcessNotificationResponse
*
*  Description   :This procedure is used to process the notification response
*                 received from the peer                
*  Parameter(s)  :
*           pAuthPtr -  pointer to authentication structure which contains all
*                       the EAP information.
*           pInpacket -  pointer to  Input Packet String.
*           Length    -  Length of the Input Packet
*           Identifier-  Identifier received along with the packet and to
*                                    be used in response
*  Return Values : VOID
********************************************************************/

VOID
EAPProcessNotificationResponse (tAUTHIf * pAuthPtr, t_MSG_DESC * pInpacket,
                                UINT2 Length, UINT1 Identifier)
{

    /* Makefile changes - unused */
    PPP_UNUSED (pInpacket);
    PPP_UNUSED (Length);

    if (pAuthPtr->ServerInfo.ServerEap.NotifId != Identifier)
    {
        DISCARD_PKT (pAuthPtr->pIf, "Invalid Id in EAP Notification response.");
        return;
    }

}

/*********************************************************************
*  Function Name :EAPProcessNakResponse
*
*  Description   :This procedure is used to process the nak response. If the
*                 MD5 authentication is naked by the peer then it calls the
*                 LCPAuthFailed() to inform the LCP about the authentication
*                 failure
*                                 
*  Parameter(s)  :
*           pAuthPtr -  pointer to authentication structure which contains all
*                       the EAP information.
*           pInpacket -  pointer to  Input Packet String.
*           Length    -  Length of the Input Packet
*           Identifier-  Identifier received along with the packet and to
*                                    be used in response
*  Return Values : VOID
********************************************************************/

VOID
EAPProcessNakResponse (tAUTHIf * pAuthPtr, t_MSG_DESC * pInpacket, UINT2 Length,
                       UINT1 Identifier)
{

    UINT1               TypeData;

    tEAPMD5ServerInfo  *pServerEAPMD5;

    PPP_UNUSED (pInpacket);
    PPP_UNUSED (Identifier);
    if (Length != 2)
    {
        DISCARD_PKT (pAuthPtr->pIf, "Invalid Id in EAP NAK response.");
        return;
    }
    EXTRACT1BYTE (pInpacket, TypeData);

    switch (pAuthPtr->ServerInfo.ServerEap.AuthType)
    {
        case EAP_TYPE_MD5:

            pServerEAPMD5 =
                &pAuthPtr->ServerInfo.ServerEap.AuthTypeOptions.ServerEAPMD5;
            if (pServerEAPMD5->ChallengeId != Identifier)
            {
                DISCARD_PKT (pAuthPtr->pIf, "Invalid Id in EAP NAK response.");
                return;
            }

            PPPStopTimer (&pServerEAPMD5->ChalTimer);

            if (TypeData == EAP_TYPE_MD5)
            {

                EAPMD5GenChallenge (pAuthPtr);
                EAPMD5SendChallenge (pAuthPtr);
                pAuthPtr->ServerInfo.ServerEap.AuthTypeOptions.ServerEAPMD5.
                    State = EAPMD5_SERVER_STATE_INITIAL_CHAL;
            }
            else
            {

                /* All EAP implmentation should support the MD5 authentication
                 * type. If the peer Nak's for MD5 Request Type, inform the
                 * Authentication failure and give up..
                 */
                LCPAuthProcessFailed (pAuthPtr->pIf);
                if (pTermGSEM != NULL)
                {
                    GSEMRun (pTermGSEM, RXJ_MINUS);
                }

            }
            break;

        default:
            break;
    }
}
