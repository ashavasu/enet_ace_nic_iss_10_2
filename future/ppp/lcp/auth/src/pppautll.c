/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppautll.c,v 1.9 2014/12/16 10:49:12 siva Exp $
 *
 * Description:This file contains the SNMP low level routines of
 *             pppSecurity group.
 *
 *******************************************************************/
#include "pppsnmpm.h"
#include "authcom.h"
#include "snmccons.h"
#include "gsemdefs.h"
#include "mpdefs.h"
#include "llproto.h"
#include "globexts.h"
#include "authsnmp.h"
#include "pppstlow.h"
#include "pppcom.h"
#include "stdautcli.h"
extern tSNMP_OID_TYPE pppSecurityChapMD5ProtocolOID;
extern tSNMP_OID_TYPE pppSecurityMSCHAPProtocolOID;
extern tSNMP_OID_TYPE pppSecurityPapProtocolOID;
extern tSNMP_OID_TYPE pppSecurityEapProtocolOID;
extern tSNMP_OID_TYPE pppSecurityEapMD5ProtocolOID;
extern tSecurityInfo IndexZeroSecurityInfo;
t_SECRET_DEPENDENCY gSecrets_dependency;

INT1
CheckAndGetSecurityPtr (tPPPIf * pIf, UINT4 *n1, UINT4 *n2, UINT4 SecondIndex)
{
    INT2                j;

    if (pIf->pAuthPtr != NULL)
    {
        for (j = 0; j < pIf->pAuthPtr->SecurityInfo.NumActiveProt; j++)
        {
            if ((pIf->pAuthPtr->SecurityInfo.PrefList[j].Preference >=
                 SecondIndex)
                && (pIf->pAuthPtr->SecurityInfo.PrefList[j].Preference < *n2))
            {
                *n2 = pIf->pAuthPtr->SecurityInfo.PrefList[j].Preference;
                *n1 = pIf->LinkInfo.IfIndex;
                return (PPP_SNMP_OK);
            }
        }
    }
    return (PPP_SNMP_ERR);
}

INT1
CheckAndGetSecretsPtr (tPPPIf * pIf, UINT4 *n1, UINT4 *n2, UINT4 SecondIndex)
{
    tSecret            *secret;
    if (pIf->pAuthPtr != NULL)
    {
        SLL_SCAN (&pIf->pAuthPtr->SecurityInfo.SecretList, secret, tSecret *)
        {
            if ((secret != NULL) && (secret->IdIndex >= SecondIndex)
                && (secret->StatusValid != SECRETS_STATUS_VALID))
            {
                *n2 = secret->IdIndex;
                *n1 = pIf->LinkInfo.IfIndex;

                return (PPP_SNMP_OK);
            }
        }
    }
    return (PPP_SNMP_ERR);
}

/*********************************************************************/
tSecret            *
SNMPGetSecret (Index1, Index2)
     UINT4               Index1;
     UINT4               Index2;
{
    tSecret            *secret;
    tPPPIf             *pIf;
    if (((pIf = PppGetIfPtr (Index1)) != NULL) && (pIf->pAuthPtr != NULL))
    {
        SLL_SCAN (&pIf->pAuthPtr->SecurityInfo.SecretList, secret, tSecret *)
        {
            if (Index2 == secret->IdIndex)
                return (secret);
        }
    }
    else
    {
        if (Index1 == 0)
        {
            SLL_SCAN (&IndexZeroSecurityInfo.SecretList, secret, tSecret *)
            {
                if (Index2 == secret->IdIndex)
                    return (secret);
            }
        }
    }
    return (NULL);
}

/*****************************************************************************/
VOID
SNMPDeleteSecretEntry (UINT4 Link, UINT4 IdIndex)
{
    tSecret            *pSecret = NULL;
    tPPPIf             *pIf;

    if (((pIf = PppGetIfPtr (Link)) != NULL) && (pIf->pAuthPtr != NULL))
    {
        SLL_SCAN (&pIf->pAuthPtr->SecurityInfo.SecretList, pSecret, tSecret *)
        {
            if (IdIndex == pSecret->IdIndex)
                break;
        }
        if (pSecret)
            AUTHDeleteSecretEntry (&pIf->pAuthPtr->SecurityInfo, pSecret);

        return;
    }
}

/*****************************************************/
INT1
DetectSecretsDependency (UINT4 IfIndex, UINT4 IdIndex, UINT2 proto)
{
    tPPPIf             *pIf;
    INT1                FoundEntry;

    PPP_UNUSED (IdIndex);

    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if (pIf->LinkInfo.IfIndex == IfIndex)
        {
            if (pIf->BundleFlag == MEMBER_LINK)
            {
                if (proto == CHAP_PROTOCOL)
                {
                    if (pIf->pAuthPtr->SecurityInfo.CHAPRemToLocSecrets > 0)
                    {
                        return (OK);
                    }
                }
                else
                {
                    if (proto == PAP_PROTOCOL)
                    {
                        if (pIf->pAuthPtr->SecurityInfo.PAPRemToLocSecrets > 0)
                        {
                            return (OK);
                        }
                    }
                    else
                    {
                        if (proto == MSCHAP_PROTOCOL)
                        {
                            if (pIf->pAuthPtr->
                                SecurityInfo.MSCHAPRemToLocSecrets > 0)
                            {
                                return (OK);
                            }
                        }
                        else
                        {
                            if (proto == EAP_PROTOCOL)
                            {
                                if (pIf->pAuthPtr->
                                    SecurityInfo.EAPMD5RemToLocSecrets > 0)
                                {
                                    return (OK);
                                }
                            }
                        }
                    }
                }
                break;
            }
        }
    }
    if (IfIndex == 0 || PppGetIfPtr (IfIndex) != NULL)
    {
        if (proto == CHAP_PROTOCOL)
        {
            if (IndexZeroSecurityInfo.CHAPRemToLocSecrets > 0)
            {
                return (OK);
            }
        }
        else
        {
            if (proto == PAP_PROTOCOL)
            {
                if (IndexZeroSecurityInfo.PAPRemToLocSecrets > 0)
                {
                    return (OK);
                }
            }
            else
            {
                if (proto == MSCHAP_PROTOCOL)
                {
                    if (IndexZeroSecurityInfo.MSCHAPRemToLocSecrets > 0)
                    {
                        return (OK);
                    }
                }
                else
                {
                    if (proto == EAP_PROTOCOL)
                    {
                        if (IndexZeroSecurityInfo.EAPMD5RemToLocSecrets > 0)
                        {
                            return (OK);
                        }
                    }
                }
            }
        }
    }
    if (IfIndex == 0)
    {
        FoundEntry = NOT_OK;
        SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
        {
            if (pIf->BundleFlag == MEMBER_LINK)
            {
                if (proto == CHAP_PROTOCOL)
                {
                    if (pIf->pAuthPtr->SecurityInfo.CHAPRemToLocSecrets > 0)
                    {
                        FoundEntry = OK;
                    }
                }
                else
                {
                    if (proto == PAP_PROTOCOL)
                    {
                        if (pIf->pAuthPtr->SecurityInfo.PAPRemToLocSecrets > 0)
                        {
                            FoundEntry = OK;
                        }
                    }
                    else
                    {
                        if (proto == MSCHAP_PROTOCOL)
                        {
                            if (pIf->pAuthPtr->
                                SecurityInfo.MSCHAPRemToLocSecrets > 0)
                            {
                                FoundEntry = OK;
                            }
                        }
                        else
                        {
                            if (proto == EAP_PROTOCOL)
                            {
                                if (pIf->pAuthPtr->
                                    SecurityInfo.EAPMD5RemToLocSecrets > 0)
                                {
                                    FoundEntry = OK;
                                }
                            }
                        }
                    }
                }
            }
            if (FoundEntry != OK)
            {
                return (NOT_OK);
            }
            else
            {
                FoundEntry = NOT_OK;
            }
        }
        if (FoundEntry != OK)
        {
            return (NOT_OK);
        }
    }
    return (NOT_OK);
}

/*********************************************************************
*  Function Name : SNMPGetAUTHPrefPtr
*  Description   :
*  Parameter(s)  :
*   Index1 -
*   Index2 -
*  Return Value  :
*
*********************************************************************/
tAuthPref          *
SNMPGetAUTHPrefPtr (Index1, Index2)
     UINT4               Index1;
     UINT4               Index2;
{
    INT2                i;
    tPPPIf             *pIf;
    if (((pIf = PppGetIfPtr (Index1)) != NULL) && (pIf->pAuthPtr != NULL))
    {
        for (i = 0; i < pIf->pAuthPtr->SecurityInfo.NumActiveProt; i++)
        {
            if (pIf->pAuthPtr->SecurityInfo.PrefList[i].Preference == Index2)
                return (&(pIf->pAuthPtr->SecurityInfo.PrefList[i]));
        }
    }
    else
    {
        if (Index1 == 0)
        {
            for (i = 0; i < IndexZeroSecurityInfo.NumActiveProt; i++)
            {
                if (IndexZeroSecurityInfo.PrefList[i].Preference == Index2)
                    return (&IndexZeroSecurityInfo.PrefList[i]);
            }
        }
    }
    return (NULL);
}

/*********************************************************************
*  Function Name : SNMPInsertAUTHPrefEntry
*  Description   :
*  Parameter(s)  :
*   Index1 -
*    pref   -
*   proto  -
*  Return Value  :
*
*********************************************************************/
INT4
SNMPInsertAUTHPrefEntry (IfIndex, pref, proto)
     UINT4               IfIndex;
     UINT4               pref;
     UINT4               proto;
{
    tPPPIf             *pIf;
    tAuthPref           temp;
    MEMSET (&temp, 0, sizeof (tAuthPref));
    temp.Protocol = (UINT2) proto;
    temp.Preference = pref;

    if (proto == MSCHAP_PROTOCOL)
    {
        temp.Protocol = CHAP_PROTOCOL;
        temp.ChapAlgo = MSCHAP_ALGORITHM;
    }
    else
    {
        if (proto == CHAP_PROTOCOL)
            temp.ChapAlgo = MD5;
        else
            temp.ChapAlgo = 0;
    }

    if (IfIndex == 0)
    {
        if (AUTHInsertPrefEntry (&IndexZeroSecurityInfo, temp) == -1)
            return (GEN_ERROR);
    }
    else
    {
        SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
        {
            if ((pIf->LinkInfo.IfIndex == IfIndex))
            {
                if (pIf->pAuthPtr == NULL)
                {
                    if ((pIf->pAuthPtr = AUTHCreate (pIf)) == NULL)
                    {
                        return (GEN_ERROR);
                    }
                }
                if (AUTHInsertPrefEntry (&pIf->pAuthPtr->SecurityInfo, temp) ==
                    -1)
                    return (GEN_ERROR);
                break;
            }
        }
    }
    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if (pIf->LinkInfo.IfIndex == IfIndex || IfIndex == 0)
        {
            pIf->LcpGSEM.pNegFlagsPerIf[AUTH_IDX].FlagMask |= DESIRED_SET;
            if (IfIndex)
            {
                break;
            }
        }

    }

    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : SNMPDetectAUTHPrefEntry
*  Description   :
*  Parameter(s)  :
*    IfIndex - 
*    pref    -
*   proto   -
*   ChapAlgo   -
*  Return Value  :
*
*********************************************************************/
INT4
SNMPDetectAUTHPrefEntry (UINT4 IfIndex, UINT4 pref, UINT4 proto, UINT1 ChapAlgo)
{
    tPPPIf             *pIf;
    UINT1               Index;

    PPP_UNUSED (pref);

    if (IfIndex != 0)
    {
        if ((pIf = PppGetIfPtr (IfIndex)) == NULL)
            return (NOT_OK);
        if (pIf->pAuthPtr == NULL)
            return (OK);
        for (Index = 0; Index < pIf->pAuthPtr->SecurityInfo.NumActiveProt;
             Index++)
        {
            if ((pIf->pAuthPtr->SecurityInfo.PrefList[Index].Protocol == proto)
                && (pIf->pAuthPtr->SecurityInfo.PrefList[Index].ChapAlgo ==
                    ChapAlgo))
            {
                return (NOT_OK);
            }
        }
    }
    else
    {
        for (Index = 0; Index < IndexZeroSecurityInfo.NumActiveProt; Index++)
        {
            if (IndexZeroSecurityInfo.PrefList[Index].Protocol == proto)
            {
                return (NOT_OK);
            }
        }
    }
    return (OK);
}

/*********************************************************************
*  Function Name :SNMPDeleteAuthPref 
*  Description   :
*  Parameter(s)  :
*   IfIndex -
*   pref    -
*  Return Value  :
*
*********************************************************************/
VOID
SNMPDeleteAuthPref (IfIndex, pref)
     UINT4               IfIndex;
     UINT4               pref;
{
    tAuthPref          *pAuthPref = NULL;
    tPPPIf             *pIf;
    INT4                i;

    if (IfIndex == 0)
    {
        for (i = 0; i < IndexZeroSecurityInfo.NumActiveProt; i++)
        {
            if (IndexZeroSecurityInfo.PrefList[i].Preference == pref)
            {
                pAuthPref = &IndexZeroSecurityInfo.PrefList[i];
                break;
            }
        }
        if (pAuthPref != NULL)
        {
            AUTHDeletePrefEntry (&IndexZeroSecurityInfo, pAuthPref->Protocol,
                                 pAuthPref->ChapAlgo);
        }
    }
    else
    {
        SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
        {
            if (pIf->LinkInfo.IfIndex == IfIndex)
            {
                if (pIf->pAuthPtr != NULL)
                {
                    for (i = 0; i < pIf->pAuthPtr->SecurityInfo.NumActiveProt;
                         i++)
                    {
                        if (pIf->pAuthPtr->SecurityInfo.
                            PrefList[i].Preference == pref)
                        {
                            pAuthPref =
                                &(pIf->pAuthPtr->SecurityInfo.PrefList[i]);
                            break;
                        }
                    }
                    if (pAuthPref != NULL)
                    {
                        AUTHDeletePrefEntry (&pIf->pAuthPtr->SecurityInfo,
                                             pAuthPref->Protocol,
                                             pAuthPref->ChapAlgo);
                    }
                }
            }
        }
    }

    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if (pIf->pAuthPtr != NULL)
        {
            if ((pIf->pAuthPtr->SecurityInfo.NumActiveProt == 0)
                && (IndexZeroSecurityInfo.NumActiveProt == 0))
            {
                pIf->LcpGSEM.pNegFlagsPerIf[AUTH_IDX].FlagMask &=
                    DESIRED_NOT_SET;
            }
        }
        else
        {
            if (IndexZeroSecurityInfo.NumActiveProt == 0)
            {
                pIf->LcpGSEM.pNegFlagsPerIf[AUTH_IDX].FlagMask &=
                    DESIRED_NOT_SET;
            }
            else
            {
                pIf->LcpGSEM.pNegFlagsPerIf[AUTH_IDX].FlagMask |= DESIRED_SET;
            }
        }
    }
    return;
}

#ifdef PPP_STACK_WANTED            /* DSL_ADD */
/* LOW LEVEL Routines for Table : PppSecurityConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppSecurityConfigTable
 Input       :  The Indices
                PppSecurityConfigLink
                PppSecurityConfigPreference
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppSecurityConfigTable (INT4 i4PppSecurityConfigLink,
                                                INT4
                                                i4PppSecurityConfigPreference)
{
    if (SNMPGetAUTHPrefPtr
        (i4PppSecurityConfigLink, i4PppSecurityConfigPreference) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppSecurityConfigTable
 Input       :  The Indices
                PppSecurityConfigLink
                PppSecurityConfigPreference
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppSecurityConfigTable (INT4 *pi4PppSecurityConfigLink,
                                        INT4 *pi4PppSecurityConfigPreference)
{
    if (nmhGetFirstDoubleIndex
        (pi4PppSecurityConfigLink, pi4PppSecurityConfigPreference,
         SecurityInstance, FIRST_INST) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppSecurityConfigTable
 Input       :  The Indices
                PppSecurityConfigLink
                nextPppSecurityConfigLink
                PppSecurityConfigPreference
                nextPppSecurityConfigPreference
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppSecurityConfigTable (INT4 i4PppSecurityConfigLink,
                                       INT4 *pi4NextPppSecurityConfigLink,
                                       INT4 i4PppSecurityConfigPreference,
                                       INT4 *pi4NextPppSecurityConfigPreference)
{
    if (nmhGetNextDoubleIndex
        (&i4PppSecurityConfigLink, &i4PppSecurityConfigPreference,
         SecurityInstance, NEXT_INST) == SNMP_SUCCESS)
    {
        *pi4NextPppSecurityConfigLink = i4PppSecurityConfigLink;
        *pi4NextPppSecurityConfigPreference = i4PppSecurityConfigPreference;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppSecurityConfigProtocol
 Input       :  The Indices
                PppSecurityConfigLink
                PppSecurityConfigPreference

                The Object 
                retValPppSecurityConfigProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppSecurityConfigProtocol (INT4 i4PppSecurityConfigLink,
                                 INT4 i4PppSecurityConfigPreference,
                                 tSNMP_OID_TYPE *
                                 pRetValPppSecurityConfigProtocol)
{
    tAuthPref          *pAuthPref;

    if ((pAuthPref =
         SNMPGetAUTHPrefPtr (i4PppSecurityConfigLink,
                             i4PppSecurityConfigPreference)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pAuthPref->Protocol == CHAP_PROTOCOL)
    {
        if (pAuthPref->ChapAlgo == MSCHAP_ALGORITHM)
        {
            return (PPPCopyOid (pRetValPppSecurityConfigProtocol,
                                &pppSecurityMSCHAPProtocolOID));
        }
        else
        {
            return (PPPCopyOid (pRetValPppSecurityConfigProtocol,
                                &pppSecurityChapMD5ProtocolOID));
        }
    }

    if (pAuthPref->Protocol == PAP_PROTOCOL)
    {
        return (PPPCopyOid (pRetValPppSecurityConfigProtocol,
                            &pppSecurityPapProtocolOID));
    }
    if (pAuthPref->Protocol == EAP_PROTOCOL)
    {
        return (PPPCopyOid (pRetValPppSecurityConfigProtocol,
                            &pppSecurityEapProtocolOID));
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetPppSecurityConfigStatus
 Input       :  The Indices
                PppSecurityConfigLink
                PppSecurityConfigPreference

                The Object 
                retValPppSecurityConfigStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppSecurityConfigStatus (INT4 i4PppSecurityConfigLink,
                               INT4 i4PppSecurityConfigPreference,
                               INT4 *pi4RetValPppSecurityConfigStatus)
{

    tAuthPref          *pAuthPref;

    if ((pAuthPref =
         SNMPGetAUTHPrefPtr (i4PppSecurityConfigLink,
                             i4PppSecurityConfigPreference)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppSecurityConfigStatus = 2;
    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppSecurityConfigLink
 Input       :  The Indices
                PppSecurityConfigLink
                PppSecurityConfigPreference

                The Object 
                setValPppSecurityConfigLink
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppSecurityConfigLink (INT4 i4PppSecurityConfigLink,
                             INT4 i4PppSecurityConfigPreference,
                             INT4 i4SetValPppSecurityConfigLink)
{
    PPP_UNUSED (i4PppSecurityConfigLink);
    PPP_UNUSED (i4PppSecurityConfigPreference);
    PPP_UNUSED (i4SetValPppSecurityConfigLink);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetPppSecurityConfigPreference
 Input       :  The Indices
                PppSecurityConfigLink
                PppSecurityConfigPreference

                The Object 
                setValPppSecurityConfigPreference
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppSecurityConfigPreference (INT4 i4PppSecurityConfigLink,
                                   INT4 i4PppSecurityConfigPreference,
                                   INT4 i4SetValPppSecurityConfigPreference)
{
    PPP_UNUSED (i4PppSecurityConfigLink);
    PPP_UNUSED (i4PppSecurityConfigPreference);
    PPP_UNUSED (i4SetValPppSecurityConfigPreference);
    return SNMP_SUCCESS;
}
#endif /* DSL_ADD */

/* LOW LEVEL Routines for Table : PppSecuritySecretsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppSecuritySecretsTable
 Input       :  The Indices
                PppSecuritySecretsLink
                PppSecuritySecretsIdIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppSecuritySecretsTable (INT4 i4PppSecuritySecretsLink,
                                                 INT4
                                                 i4PppSecuritySecretsIdIndex)
{
    tPPPIf             *pIf;
    tSecret            *secret;

    if ((pIf = PppGetIfPtr ((UINT4) i4PppSecuritySecretsLink)) == NULL)
        return SNMP_FAILURE;

    if (pIf->pAuthPtr == NULL)
        return SNMP_FAILURE;

    SLL_SCAN (&pIf->pAuthPtr->SecurityInfo.SecretList, secret, tSecret *)
    {
        if (secret->IdIndex == (UINT4) i4PppSecuritySecretsIdIndex)
            return SNMP_SUCCESS;
    }

    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppSecuritySecretsTable
 Input       :  The Indices
                PppSecuritySecretsLink
                PppSecuritySecretsIdIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppSecuritySecretsTable (INT4 *pi4PppSecuritySecretsLink,
                                         INT4 *pi4PppSecuritySecretsIdIndex)
{
    return (nmhGetFirstDoubleIndex
            (pi4PppSecuritySecretsLink, pi4PppSecuritySecretsIdIndex,
             (INT1) SecretsInstance, FIRST_INST));
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppSecuritySecretsTable
 Input       :  The Indices
                PppSecuritySecretsLink
                nextPppSecuritySecretsLink
                PppSecuritySecretsIdIndex
                nextPppSecuritySecretsIdIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppSecuritySecretsTable (INT4 i4PppSecuritySecretsLink,
                                        INT4 *pi4NextPppSecuritySecretsLink,
                                        INT4 i4PppSecuritySecretsIdIndex,
                                        INT4 *pi4NextPppSecuritySecretsIdIndex)
{
    if (nmhGetNextDoubleIndex
        (&i4PppSecuritySecretsLink, &i4PppSecuritySecretsIdIndex,
         (INT1) SecretsInstance, NEXT_INST) == SNMP_SUCCESS)
    {
        *pi4NextPppSecuritySecretsLink = i4PppSecuritySecretsLink;
        *pi4NextPppSecuritySecretsIdIndex = i4PppSecuritySecretsIdIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppSecuritySecretsDirection
 Input       :  The Indices
                PppSecuritySecretsLink
                PppSecuritySecretsIdIndex

                The Object 
                retValPppSecuritySecretsDirection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppSecuritySecretsDirection (INT4 i4PppSecuritySecretsLink,
                                   INT4 i4PppSecuritySecretsIdIndex,
                                   INT4 *pi4RetValPppSecuritySecretsDirection)
{

    tSecret            *pSecret;

    if ((pSecret =
         SNMPGetSecret ((UINT4) i4PppSecuritySecretsLink,
                        (UINT4) i4PppSecuritySecretsIdIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValPppSecuritySecretsDirection = pSecret->Direction;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPppSecuritySecretsProtocol
 Input       :  The Indices
                PppSecuritySecretsLink
                PppSecuritySecretsIdIndex

                The Object 
                retValPppSecuritySecretsProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppSecuritySecretsProtocol (INT4 i4PppSecuritySecretsLink,
                                  INT4 i4PppSecuritySecretsIdIndex,
                                  tSNMP_OID_TYPE *
                                  pRetValPppSecuritySecretsProtocol)
{

    tSecret            *pSecret;

    if ((pSecret =
         SNMPGetSecret ((UINT4) i4PppSecuritySecretsLink,
                        (UINT4) i4PppSecuritySecretsIdIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    switch (pSecret->Protocol)
    {
        case CHAP_PROTOCOL:
            return (PPPCopyOid (pRetValPppSecuritySecretsProtocol,
                                &pppSecurityChapMD5ProtocolOID));
        case MSCHAP_PROTOCOL:
            return (PPPCopyOid (pRetValPppSecuritySecretsProtocol,
                                &pppSecurityMSCHAPProtocolOID));
        case PAP_PROTOCOL:
            return (PPPCopyOid (pRetValPppSecuritySecretsProtocol,
                                &pppSecurityPapProtocolOID));
        case EAPMD5_PROTOCOL:
            return (PPPCopyOid (pRetValPppSecuritySecretsProtocol,
                                &pppSecurityEapProtocolOID));
        default:
            break;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetPppSecuritySecretsIdentity
 Input       :  The Indices
                PppSecuritySecretsLink
                PppSecuritySecretsIdIndex

                The Object 
                retValPppSecuritySecretsIdentity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppSecuritySecretsIdentity (INT4 i4PppSecuritySecretsLink,
                                  INT4 i4PppSecuritySecretsIdIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValPppSecuritySecretsIdentity)
{

    tSecret            *pSecret;

    if ((pSecret =
         SNMPGetSecret ((UINT4) i4PppSecuritySecretsLink,
                        (UINT4) i4PppSecuritySecretsIdIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pRetValPppSecuritySecretsIdentity->i4_Length = pSecret->IdentityLen;
    MEMCPY (pRetValPppSecuritySecretsIdentity->pu1_OctetList, pSecret->Identity,
            pRetValPppSecuritySecretsIdentity->i4_Length);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPppSecuritySecretsSecret
 Input       :  The Indices
                PppSecuritySecretsLink
                PppSecuritySecretsIdIndex

                The Object 
                retValPppSecuritySecretsSecret
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppSecuritySecretsSecret (INT4 i4PppSecuritySecretsLink,
                                INT4 i4PppSecuritySecretsIdIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValPppSecuritySecretsSecret)
{

    tSecret            *pSecret;

    if ((pSecret =
         SNMPGetSecret ((UINT4) i4PppSecuritySecretsLink,
                        (UINT4) i4PppSecuritySecretsIdIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pRetValPppSecuritySecretsSecret->i4_Length = pSecret->SecretLen;
    MEMCPY (pRetValPppSecuritySecretsSecret->pu1_OctetList, pSecret->Secret,
            pRetValPppSecuritySecretsSecret->i4_Length);

    FsUtlDecryptPasswd ((CHR1 *) pRetValPppSecuritySecretsSecret->
                        pu1_OctetList);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPppSecuritySecretsStatus
 Input       :  The Indices
                PppSecuritySecretsLink
                PppSecuritySecretsIdIndex

                The Object 
                retValPppSecuritySecretsStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppSecuritySecretsStatus (INT4 i4PppSecuritySecretsLink,
                                INT4 i4PppSecuritySecretsIdIndex,
                                INT4 *pi4RetValPppSecuritySecretsStatus)
{

    tSecret            *pSecret;

    if ((pSecret =
         SNMPGetSecret ((UINT4) i4PppSecuritySecretsLink,
                        (UINT4) i4PppSecuritySecretsIdIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValPppSecuritySecretsStatus = pSecret->Status;
    return (SNMP_SUCCESS);

}

#ifdef PPP_STACK_WANTED            /* DSL_ADD */
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppSecurityConfigLink
 Input       :  The Indices
                PppSecurityConfigLink
                PppSecurityConfigPreference

                The Object 
                testValPppSecurityConfigLink
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppSecurityConfigLink (UINT4 *pu4ErrorCode,
                                INT4 i4PppSecurityConfigLink,
                                INT4 i4PppSecurityConfigPreference,
                                INT4 i4TestValPppSecurityConfigLink)
{

    tPPPIf             *pIf;

    PPP_UNUSED (i4PppSecurityConfigPreference);

    if ((i4PppSecurityConfigLink != 0)
        && (((pIf = PppGetPppIfPtr ((UINT4) i4PppSecurityConfigLink)) == NULL)
            || pIf->pAuthPtr == NULL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppSecurityConfigLink < MIN_INTEGER
        || i4TestValPppSecurityConfigLink > MAX_INTEGER)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppSecurityConfigPreference
 Input       :  The Indices
                PppSecurityConfigLink
                PppSecurityConfigPreference

                The Object 
                testValPppSecurityConfigPreference
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppSecurityConfigPreference (UINT4 *pu4ErrorCode,
                                      INT4 i4PppSecurityConfigLink,
                                      INT4 i4PppSecurityConfigPreference,
                                      INT4 i4TestValPppSecurityConfigPreference)
{

    tPPPIf             *pIf;

    PPP_UNUSED (i4PppSecurityConfigPreference);
    if ((i4PppSecurityConfigLink != 0)
        && (((pIf = PppGetPppIfPtr ((UINT4) i4PppSecurityConfigLink)) == NULL)
            || pIf->pAuthPtr == NULL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppSecurityConfigPreference < MIN_INTEGER
        || i4TestValPppSecurityConfigPreference > MAX_INTEGER)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppSecurityConfigProtocol
 Input       :  The Indices
                PppSecurityConfigLink
                PppSecurityConfigPreference

                The Object 
                testValPppSecurityConfigProtocol
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppSecurityConfigProtocol (UINT4 *pu4ErrorCode,
                                    INT4 i4PppSecurityConfigLink,
                                    INT4 i4PppSecurityConfigPreference,
                                    tSNMP_OID_TYPE *
                                    pTestValPppSecurityConfigProtocol)
{

    tPPPIf             *pIf = NULL;
    UINT1               ChapAlgo = 0;
    INT2                u2_Protocol = 0;

    if ((i4PppSecurityConfigLink != 0)
        && (((pIf = PppGetPppIfPtr ((UINT4) i4PppSecurityConfigLink)) == NULL)
            || pIf->pAuthPtr == NULL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (SNMP_FAILURE);
    }

    if (PPP_compare_oids (pTestValPppSecurityConfigProtocol,
                          &pppSecurityChapMD5ProtocolOID) == EQUAL)
    {
        u2_Protocol = (UINT2) CHAP_PROTOCOL;
    }
    if (PPP_compare_oids (pTestValPppSecurityConfigProtocol,
                          &pppSecurityMSCHAPProtocolOID) == EQUAL)
    {
        u2_Protocol = (UINT2) MSCHAP_PROTOCOL;
    }
    if (PPP_compare_oids (pTestValPppSecurityConfigProtocol,
                          &pppSecurityPapProtocolOID) == EQUAL)
    {
        u2_Protocol = (UINT2) PAP_PROTOCOL;
    }
    if (PPP_compare_oids (pTestValPppSecurityConfigProtocol,
                          &pppSecurityEapProtocolOID) == EQUAL)
    {
        u2_Protocol = (UINT2) EAP_PROTOCOL;
    }
    if (u2_Protocol == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    ChapAlgo = ((u2_Protocol == MSCHAP_PROTOCOL) ? MSCHAP_ALGORITHM : MD5);

    if (SNMPDetectAUTHPrefEntry
        (i4PppSecurityConfigLink, i4PppSecurityConfigPreference, u2_Protocol,
         (UINT1) ChapAlgo) != OK)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppSecurityConfigStatus
 Input       :  The Indices
                PppSecurityConfigLink
                PppSecurityConfigPreference

                The Object 
                testValPppSecurityConfigStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppSecurityConfigStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4PppSecurityConfigLink,
                                  INT4 i4PppSecurityConfigPreference,
                                  INT4 i4TestValPppSecurityConfigStatus)
{

    tPPPIf             *pIf = NULL;

    if ((i4PppSecurityConfigLink != 0)
        && (((pIf = PppGetPppIfPtr (i4PppSecurityConfigLink)) == NULL)
            || pIf->pAuthPtr == NULL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (SNMP_FAILURE);
    }

    if ((i4TestValPppSecurityConfigStatus < 1)
        || (i4TestValPppSecurityConfigStatus > 2))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppSecurityConfigStatus == 1)
    {
        if (SNMPGetAUTHPrefPtr
            (i4PppSecurityConfigLink, i4PppSecurityConfigPreference) == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return (SNMP_FAILURE);
        }
    }
    return (SNMP_SUCCESS);

}
#endif /* DSL_ADD */

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppSecuritySecretsDirection
 Input       :  The Indices
                PppSecuritySecretsLink
                PppSecuritySecretsIdIndex

                The Object 
                testValPppSecuritySecretsDirection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppSecuritySecretsDirection (UINT4 *pu4ErrorCode,
                                      INT4 i4PppSecuritySecretsLink,
                                      INT4 i4PppSecuritySecretsIdIndex,
                                      INT4 i4TestValPppSecuritySecretsDirection)
{
    tPPPIf             *pIf = NULL;

    PPP_UNUSED (i4PppSecuritySecretsIdIndex);

    if ((i4PppSecuritySecretsLink != 0)
        || ((pIf = PppGetIfPtr ((UINT4) i4PppSecuritySecretsLink)) == NULL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppSecuritySecretsDirection != LOC_TO_REMOTE_DIRECTION
        && i4TestValPppSecuritySecretsDirection != REMOTE_TO_LOC_DIRECTION)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (pIf->MPInfo.MemberInfo.pBundlePtr != NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppSecuritySecretsProtocol
 Input       :  The Indices
                PppSecuritySecretsLink
                PppSecuritySecretsIdIndex

                The Object 
                testValPppSecuritySecretsProtocol
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppSecuritySecretsProtocol (UINT4 *pu4ErrorCode,
                                     INT4 i4PppSecuritySecretsLink,
                                     INT4 i4PppSecuritySecretsIdIndex,
                                     tSNMP_OID_TYPE *
                                     pTestValPppSecuritySecretsProtocol)
{
    tPPPIf             *pIf = NULL;

    PPP_UNUSED (i4PppSecuritySecretsIdIndex);

    if ((i4PppSecuritySecretsLink != 0)
        || ((pIf = PppGetIfPtr ((UINT4) i4PppSecuritySecretsLink)) == NULL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (SNMP_FAILURE);
    }

    if ((PPP_compare_oids
         (pTestValPppSecuritySecretsProtocol,
          &pppSecurityChapMD5ProtocolOID) != EQUAL)
        &&
        (PPP_compare_oids
         (pTestValPppSecuritySecretsProtocol,
          &pppSecurityMSCHAPProtocolOID) != EQUAL)
        &&
        (PPP_compare_oids
         (pTestValPppSecuritySecretsProtocol,
          &pppSecurityPapProtocolOID) != EQUAL)
        &&
        (PPP_compare_oids
         (pTestValPppSecuritySecretsProtocol,
          &pppSecurityEapMD5ProtocolOID) != EQUAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (pIf->MPInfo.MemberInfo.pBundlePtr != NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppSecuritySecretsIdentity
 Input       :  The Indices
                PppSecuritySecretsLink
                PppSecuritySecretsIdIndex

                The Object 
                testValPppSecuritySecretsIdentity
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppSecuritySecretsIdentity (UINT4 *pu4ErrorCode,
                                     INT4 i4PppSecuritySecretsLink,
                                     INT4 i4PppSecuritySecretsIdIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pTestValPppSecuritySecretsIdentity)
{
    tPPPIf             *pIf = NULL;

    PPP_UNUSED (i4PppSecuritySecretsIdIndex);

    if ((i4PppSecuritySecretsLink != 0)
        || ((pIf = PppGetIfPtr ((UINT4) i4PppSecuritySecretsLink)) == NULL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (SNMP_FAILURE);
    }
#ifdef SNMP_2_WANTED
    if (OSIX_FAILURE ==
        SNMPCheckForNVTChars (pTestValPppSecuritySecretsIdentity->pu1_OctetList,
                              pTestValPppSecuritySecretsIdentity->i4_Length))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
#endif
    if (pTestValPppSecuritySecretsIdentity->i4_Length <=
        MIN_SECRETS_IDENTITY_LENGTH
        || pTestValPppSecuritySecretsIdentity->i4_Length >=
        MAX_SECRETS_IDENTITY_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return (SNMP_FAILURE);
    }

    if (pIf->MPInfo.MemberInfo.pBundlePtr != NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppSecuritySecretsSecret
 Input       :  The Indices
                PppSecuritySecretsLink
                PppSecuritySecretsIdIndex

                The Object 
                testValPppSecuritySecretsSecret
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppSecuritySecretsSecret (UINT4 *pu4ErrorCode,
                                   INT4 i4PppSecuritySecretsLink,
                                   INT4 i4PppSecuritySecretsIdIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValPppSecuritySecretsSecret)
{
    tPPPIf             *pIf = NULL;

    PPP_UNUSED (i4PppSecuritySecretsIdIndex);

    if ((i4PppSecuritySecretsLink != 0)
        || ((pIf = PppGetIfPtr ((UINT4) i4PppSecuritySecretsLink)) == NULL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (SNMP_FAILURE);
    }

#ifdef SNMP_2_WANTED
    if (OSIX_FAILURE ==
        SNMPCheckForNVTChars (pTestValPppSecuritySecretsSecret->pu1_OctetList,
                              pTestValPppSecuritySecretsSecret->i4_Length))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
#endif
    if (pTestValPppSecuritySecretsSecret->i4_Length <=
        MIN_SECRETS_IDENTITY_LENGTH
        || pTestValPppSecuritySecretsSecret->i4_Length >=
        MAX_SECRETS_IDENTITY_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return (SNMP_FAILURE);
    }

    if (pIf->MPInfo.MemberInfo.pBundlePtr != NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppSecuritySecretsStatus
 Input       :  The Indices
                PppSecuritySecretsLink
                PppSecuritySecretsIdIndex

                The Object 
                testValPppSecuritySecretsStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppSecuritySecretsStatus (UINT4 *pu4ErrorCode,
                                   INT4 i4PppSecuritySecretsLink,
                                   INT4 i4PppSecuritySecretsIdIndex,
                                   INT4 i4TestValPppSecuritySecretsStatus)
{
    tPPPIf             *pIf = NULL;

    PPP_UNUSED (i4PppSecuritySecretsIdIndex);
    if ((i4PppSecuritySecretsLink != 0)
        || ((pIf = PppGetIfPtr ((UINT4) i4PppSecuritySecretsLink)) == NULL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (SNMP_FAILURE);
    }
    if ((i4TestValPppSecuritySecretsStatus != SECRETS_STATUS_INVALID)
        && (i4TestValPppSecuritySecretsStatus != SECRETS_STATUS_VALID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if (pIf->MPInfo.MemberInfo.pBundlePtr != NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (SNMP_FAILURE);
    }
    AuthFillSecretDependency ((UINT4) i4PppSecuritySecretsLink,
                              (UINT4) i4PppSecuritySecretsIdIndex);

    if (i4TestValPppSecuritySecretsStatus == SECRETS_STATUS_INVALID)
    {
        if ((gSecrets_dependency.StatusValid & AUTH_ALL_SET) == AUTH_ALL_SET)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (SNMP_FAILURE);
        }
    }
    return (SNMP_SUCCESS);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2PppSecuritySecretsTable
 Input       :  The Indices
                PppSecuritySecretsLink
                PppSecuritySecretsIdIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PppSecuritySecretsTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetPppSecurityConfigProtocol
 Input       :  The Indices
                PppSecurityConfigLink
                PppSecurityConfigPreference

                The Object 
                setValPppSecurityConfigProtocol
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppSecurityConfigProtocol (INT4 i4PppSecurityConfigLink,
                                 INT4 i4PppSecurityConfigPreference,
                                 tSNMP_OID_TYPE *
                                 pSetValPppSecurityConfigProtocol)
{
    UINT2               u2_Protocol = 0;

    if (PPP_compare_oids (pSetValPppSecurityConfigProtocol,
                          &pppSecurityChapMD5ProtocolOID) == EQUAL)
    {
        u2_Protocol = CHAP_PROTOCOL;
    }
    if (PPP_compare_oids (pSetValPppSecurityConfigProtocol,
                          &pppSecurityMSCHAPProtocolOID) == EQUAL)
    {
        u2_Protocol = MSCHAP_PROTOCOL;
    }
    if (PPP_compare_oids (pSetValPppSecurityConfigProtocol,
                          &pppSecurityPapProtocolOID) == EQUAL)
    {
        u2_Protocol = PAP_PROTOCOL;
    }
    if (PPP_compare_oids (pSetValPppSecurityConfigProtocol,
                          &pppSecurityEapProtocolOID) == EQUAL)
    {
        u2_Protocol = EAP_PROTOCOL;
    }

    return ((INT1) SNMPInsertAUTHPrefEntry
            ((UINT4) i4PppSecurityConfigLink,
             (UINT4) i4PppSecurityConfigPreference, (UINT4) u2_Protocol));
}

/****************************************************************************
 Function    :  nmhSetPppSecurityConfigStatus
 Input       :  The Indices
                PppSecurityConfigLink
                PppSecurityConfigPreference

                The Object 
                setValPppSecurityConfigStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppSecurityConfigStatus (INT4 i4PppSecurityConfigLink,
                               INT4 i4PppSecurityConfigPreference,
                               INT4 i4SetValPppSecurityConfigStatus)
{

    if (i4SetValPppSecurityConfigStatus == SECRETS_STATUS_INVALID)
    {
        SNMPDeleteAuthPref ((UINT4) i4PppSecurityConfigLink,
                            (UINT4) i4PppSecurityConfigPreference);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetPppSecuritySecretsDirection
 Input       :  The Indices
                PppSecuritySecretsLink
                PppSecuritySecretsIdIndex

                The Object 
                setValPppSecuritySecretsDirection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppSecuritySecretsDirection (INT4 i4PppSecuritySecretsLink,
                                   INT4 i4PppSecuritySecretsIdIndex,
                                   INT4 i4SetValPppSecuritySecretsDirection)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    AuthFillSecretDependency ((UINT4) i4PppSecuritySecretsLink,
                              (UINT4) i4PppSecuritySecretsIdIndex);

    gSecrets_dependency.Direction = (UINT1) i4SetValPppSecuritySecretsDirection;
    gSecrets_dependency.StatusValid |= AUTH_DIRECTION_MASK;
    if ((gSecrets_dependency.StatusValid & AUTH_ALL_SET) == AUTH_ALL_SET)
    {
        gSecrets_dependency.Status = SECRETS_STATUS_VALID;
    }
    SNMPSetSecuritySecret ((UINT4) i4PppSecuritySecretsLink,
                           (UINT4) i4PppSecuritySecretsIdIndex,
                           &gSecrets_dependency);
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppSecuritySecretsDirection, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 2, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                  i4PppSecuritySecretsLink,
	          i4PppSecuritySecretsIdIndex,
		  i4SetValPppSecuritySecretsDirection));

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetPppSecuritySecretsProtocol
Input       :  The Indices
PppSecuritySecretsLink
PppSecuritySecretsIdIndex

                The Object 
                setValPppSecuritySecretsProtocol
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppSecuritySecretsProtocol (INT4 i4PppSecuritySecretsLink,
                                  INT4 i4PppSecuritySecretsIdIndex,
                                  tSNMP_OID_TYPE *
                                  pSetValPppSecuritySecretsProtocol)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    AuthFillSecretDependency ((UINT4) i4PppSecuritySecretsLink,
                              (UINT4) i4PppSecuritySecretsIdIndex);

    if (PPP_compare_oids (pSetValPppSecuritySecretsProtocol,
                          &pppSecurityChapMD5ProtocolOID) == EQUAL)
    {
        gSecrets_dependency.Protocol = CHAP_PROTOCOL;
        gSecrets_dependency.StatusValid |= AUTH_PROTOCOL_MASK;
    }
    if (PPP_compare_oids (pSetValPppSecuritySecretsProtocol,
                          &pppSecurityMSCHAPProtocolOID) == EQUAL)
    {
        gSecrets_dependency.Protocol = MSCHAP_PROTOCOL;
        gSecrets_dependency.StatusValid |= AUTH_PROTOCOL_MASK;
    }
    if (PPP_compare_oids (pSetValPppSecuritySecretsProtocol,
                          &pppSecurityPapProtocolOID) == EQUAL)
    {
        gSecrets_dependency.Protocol = PAP_PROTOCOL;
        gSecrets_dependency.StatusValid |= AUTH_PROTOCOL_MASK;
    }
    if (PPP_compare_oids (pSetValPppSecuritySecretsProtocol,
                          &pppSecurityEapMD5ProtocolOID) == EQUAL)
    {
        gSecrets_dependency.Protocol = EAPMD5_PROTOCOL;
        gSecrets_dependency.StatusValid |= AUTH_PROTOCOL_MASK;
    }
    if ((gSecrets_dependency.StatusValid & AUTH_ALL_SET) == AUTH_ALL_SET)
    {
        gSecrets_dependency.Status = SECRETS_STATUS_VALID;
    }
    SNMPSetSecuritySecret ((UINT4) i4PppSecuritySecretsLink,
                           (UINT4) i4PppSecuritySecretsIdIndex,
                           &gSecrets_dependency);
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppSecuritySecretsProtocol, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 2, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s",
                  i4PppSecuritySecretsLink,
	          i4PppSecuritySecretsIdIndex,
		  pSetValPppSecuritySecretsProtocol));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetPppSecuritySecretsIdentity
 Input       :  The Indices
                PppSecuritySecretsLink
                PppSecuritySecretsIdIndex

                The Object 
                setValPppSecuritySecretsIdentity
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppSecuritySecretsIdentity (INT4 i4PppSecuritySecretsLink,
                                  INT4 i4PppSecuritySecretsIdIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pSetValPppSecuritySecretsIdentity)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    AuthFillSecretDependency ((UINT4) i4PppSecuritySecretsLink,
                              (UINT4) i4PppSecuritySecretsIdIndex);

    MEMCPY (gSecrets_dependency.Identity,
            pSetValPppSecuritySecretsIdentity->pu1_OctetList,
            pSetValPppSecuritySecretsIdentity->i4_Length);
    gSecrets_dependency.IdentityLen =
        (UINT1) pSetValPppSecuritySecretsIdentity->i4_Length;

    gSecrets_dependency.StatusValid |= AUTH_IDENTITY_MASK;

    if ((gSecrets_dependency.StatusValid & AUTH_ALL_SET) == AUTH_ALL_SET)
    {
        gSecrets_dependency.Status = SECRETS_STATUS_VALID;
    }
    SNMPSetSecuritySecret ((UINT4) i4PppSecuritySecretsLink,
                           (UINT4) i4PppSecuritySecretsIdIndex,
                           &gSecrets_dependency);
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppSecuritySecretsIdentity, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 2, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s",
                  i4PppSecuritySecretsLink,
	          i4PppSecuritySecretsIdIndex,
		  pSetValPppSecuritySecretsIdentity));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetPppSecuritySecretsSecret
 Input       :  The Indices
                PppSecuritySecretsLink
                PppSecuritySecretsIdIndex

                The Object 
                setValPppSecuritySecretsSecret
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppSecuritySecretsSecret (INT4 i4PppSecuritySecretsLink,
                                INT4 i4PppSecuritySecretsIdIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValPppSecuritySecretsSecret)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    AuthFillSecretDependency ((UINT4) i4PppSecuritySecretsLink,
                              (UINT4) i4PppSecuritySecretsIdIndex);

    MEMCPY (gSecrets_dependency.Secret,
            pSetValPppSecuritySecretsSecret->pu1_OctetList,
            pSetValPppSecuritySecretsSecret->i4_Length);

    gSecrets_dependency.SecretLen =
        (UINT1) pSetValPppSecuritySecretsSecret->i4_Length;

    gSecrets_dependency.StatusValid |= AUTH_SECRET_MASK;

    if ((gSecrets_dependency.StatusValid & AUTH_ALL_SET) == AUTH_ALL_SET)
    {
        gSecrets_dependency.Status = SECRETS_STATUS_VALID;
    }
    SNMPSetSecuritySecret ((UINT4) i4PppSecuritySecretsLink,
                           (UINT4) i4PppSecuritySecretsIdIndex,
                           &gSecrets_dependency);
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppSecuritySecretsSecret, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 2, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s",
                  i4PppSecuritySecretsLink,
	          i4PppSecuritySecretsIdIndex,
		  pSetValPppSecuritySecretsSecret));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetPppSecuritySecretsStatus
 Input       :  The Indices
                PppSecuritySecretsLink
                PppSecuritySecretsIdIndex

                The Object 
                setValPppSecuritySecretsStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppSecuritySecretsStatus (INT4 i4PppSecuritySecretsLink,
                                INT4 i4PppSecuritySecretsIdIndex,
                                INT4 i4SetValPppSecuritySecretsStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tPPPIf             *pIf = NULL;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (i4SetValPppSecuritySecretsStatus == SECRETS_STATUS_VALID)
        return SNMP_SUCCESS;

    AuthFillSecretDependency ((UINT4) i4PppSecuritySecretsLink,
                              (UINT4) i4PppSecuritySecretsIdIndex);
    gSecrets_dependency.Status = SECRETS_STATUS_INVALID;

    /* If this link is a bundle then 
     * Scan and remove the secret entry from all the member links */
    pIf = PppGetIfPtr ((UINT4) i4PppSecuritySecretsLink);

    if ((pIf != NULL) && (pIf->BundleFlag == BUNDLE))
    {
        MpUpdateSecretToAllMembers (i4PppSecuritySecretsLink,
                                    i4PppSecuritySecretsIdIndex,
                                    SECRETS_STATUS_INVALID);
    }
    SNMPSetSecuritySecret ((UINT4) i4PppSecuritySecretsLink,
                           (UINT4) i4PppSecuritySecretsIdIndex,
                           &gSecrets_dependency);
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppSecuritySecretsStatus, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 2, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                  i4PppSecuritySecretsLink,
	          i4PppSecuritySecretsIdIndex,
		  i4SetValPppSecuritySecretsStatus));
    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name :SNMPModifySecret
*  Description   :
*  Parameter(s)  :
*    secret - 
*  Return Value  :
*
*********************************************************************/
VOID
SNMPModifySecret (secret, pSecurityInfo, secrets_dependency)
     tSecret            *secret;
     tSecurityInfo      *pSecurityInfo;
     t_SECRET_DEPENDENCY *secrets_dependency;
{
    UINT2               PrevProtocol, CurrentProtocol;
    UINT1               PrevDirection, CurrentDirection;
    UINT1               PrevStatus;

    secret->IdIndex = secrets_dependency->IdIndex;
    PrevDirection = secret->Direction;

    secret->Direction = secrets_dependency->Direction;
    CurrentDirection = secret->Direction;
    PrevProtocol = secret->Protocol;
    secret->Protocol = secrets_dependency->Protocol;
    CurrentProtocol = secret->Protocol;
    PrevStatus = secret->Status;
    secret->Status = secrets_dependency->Status;
    secret->StatusValid = secrets_dependency->StatusValid;
    secret->IdentityLen = secrets_dependency->IdentityLen;
    secret->SecretLen = secrets_dependency->SecretLen;
    MEMCPY (secret->Secret, &secrets_dependency->Secret, secret->SecretLen);
    FsUtlEncryptPasswd ((CHR1 *) secret->Secret);
    MEMCPY (secret->Identity, &secrets_dependency->Identity,
            secret->IdentityLen);
    secret->Identity[secret->IdentityLen] = '\0';
    secret->Secret[secret->SecretLen] = '\0';

    if ((PrevStatus != SECRETS_STATUS_VALID) &&
        (secret->Status == SECRETS_STATUS_VALID))
    {
        AUTHNewSecretUpdateCounters (pSecurityInfo, secret);
    }
    else if (PrevStatus == SECRETS_STATUS_VALID)
    {
        if ((PrevDirection != CurrentDirection))
        {
            /* change in direction */
            if (PrevProtocol == CurrentProtocol)
            {
                if (CurrentDirection == LOC_TO_REMOTE)
                {
                    if (CurrentProtocol == PAP_PROTOCOL)
                    {
                        pSecurityInfo->PAPLocToRemSecrets++;
                        pSecurityInfo->PAPRemToLocSecrets--;
                    }
                    else
                    {
                        if (CurrentProtocol == MSCHAP_PROTOCOL)
                        {
                            pSecurityInfo->MSCHAPRemToLocSecrets++;
                            pSecurityInfo->MSCHAPLocToRemSecrets--;
                        }
                        else
                        {
                            pSecurityInfo->CHAPLocToRemSecrets++;
                            pSecurityInfo->CHAPRemToLocSecrets--;
                        }
                    }
                }
                else
                {
                    if (CurrentProtocol == PAP_PROTOCOL)
                    {
                        pSecurityInfo->PAPRemToLocSecrets++;
                        pSecurityInfo->PAPLocToRemSecrets--;
                    }
                    else
                    {
                        if (CurrentProtocol == MSCHAP_PROTOCOL)
                        {
                            pSecurityInfo->MSCHAPRemToLocSecrets++;
                            pSecurityInfo->MSCHAPLocToRemSecrets--;
                        }
                        else
                        {
                            pSecurityInfo->CHAPRemToLocSecrets++;
                            pSecurityInfo->CHAPLocToRemSecrets--;
                        }
                    }
                }
            }
            else
            {
                if (CurrentDirection == LOC_TO_REMOTE)
                {
                    if (CurrentProtocol == PAP_PROTOCOL)
                    {
                        pSecurityInfo->PAPLocToRemSecrets++;
                        if (PrevProtocol == CHAP_PROTOCOL)
                            pSecurityInfo->CHAPRemToLocSecrets--;
                        else
                            pSecurityInfo->MSCHAPRemToLocSecrets--;
                    }
                    else
                    {
                        if (CurrentProtocol == CHAP_PROTOCOL)
                        {
                            pSecurityInfo->CHAPLocToRemSecrets++;
                            if (PrevProtocol == PAP_PROTOCOL)
                                pSecurityInfo->PAPRemToLocSecrets--;
                            else
                                pSecurityInfo->MSCHAPRemToLocSecrets--;
                        }
                        else
                        {
                            pSecurityInfo->MSCHAPLocToRemSecrets++;
                            if (PrevProtocol == PAP_PROTOCOL)
                                pSecurityInfo->PAPRemToLocSecrets--;
                            else
                                pSecurityInfo->CHAPRemToLocSecrets--;
                        }
                    }
                }
                else
                {
                    if (CurrentProtocol == PAP_PROTOCOL)
                    {
                        pSecurityInfo->PAPRemToLocSecrets++;
                        if (PrevProtocol == CHAP_PROTOCOL)
                            pSecurityInfo->CHAPLocToRemSecrets--;
                        else
                            pSecurityInfo->MSCHAPLocToRemSecrets--;
                    }
                    else
                    {
                        if (CurrentProtocol == CHAP_PROTOCOL)
                        {
                            pSecurityInfo->CHAPRemToLocSecrets++;
                            if (PrevProtocol == PAP_PROTOCOL)
                                pSecurityInfo->PAPLocToRemSecrets--;
                            else
                                pSecurityInfo->MSCHAPLocToRemSecrets--;
                        }
                        else
                        {
                            pSecurityInfo->MSCHAPRemToLocSecrets++;
                            if (PrevProtocol == PAP_PROTOCOL)
                                pSecurityInfo->PAPLocToRemSecrets--;
                            else
                                pSecurityInfo->CHAPLocToRemSecrets--;
                        }
                    }
                }
            }
        }
        else
        {
            if (CurrentProtocol != PrevProtocol)
            {
                if (CurrentDirection == LOC_TO_REMOTE)
                {
                    if (CurrentProtocol == PAP_PROTOCOL)
                    {
                        pSecurityInfo->PAPLocToRemSecrets++;
                        if (PrevProtocol == CHAP_PROTOCOL)
                            pSecurityInfo->CHAPLocToRemSecrets--;
                        else
                            pSecurityInfo->MSCHAPLocToRemSecrets--;
                    }
                    else
                    {
                        if (CurrentProtocol == CHAP_PROTOCOL)
                        {
                            pSecurityInfo->CHAPLocToRemSecrets++;
                            if (PrevProtocol == PAP_PROTOCOL)
                                pSecurityInfo->PAPLocToRemSecrets--;
                            else
                                pSecurityInfo->MSCHAPLocToRemSecrets--;
                        }
                        else
                        {
                            pSecurityInfo->MSCHAPLocToRemSecrets++;
                            if (PrevProtocol == PAP_PROTOCOL)
                                pSecurityInfo->PAPLocToRemSecrets--;
                            else
                                pSecurityInfo->CHAPLocToRemSecrets--;
                        }
                    }
                }
                else
                {
                    if (CurrentProtocol == PAP_PROTOCOL)
                    {
                        pSecurityInfo->PAPRemToLocSecrets++;
                        if (PrevProtocol == CHAP_PROTOCOL)
                            pSecurityInfo->CHAPRemToLocSecrets--;
                        else
                            pSecurityInfo->MSCHAPRemToLocSecrets--;
                    }
                    else
                    {
                        if (CurrentProtocol == CHAP_PROTOCOL)
                        {
                            pSecurityInfo->CHAPRemToLocSecrets++;
                            if (PrevProtocol == PAP_PROTOCOL)
                                pSecurityInfo->PAPRemToLocSecrets--;
                            else
                                pSecurityInfo->MSCHAPRemToLocSecrets--;
                        }
                        else
                        {
                            pSecurityInfo->MSCHAPRemToLocSecrets++;
                            if (PrevProtocol == PAP_PROTOCOL)
                                pSecurityInfo->PAPRemToLocSecrets--;
                            else
                                pSecurityInfo->CHAPRemToLocSecrets--;
                        }
                    }
                }
            }
        }
    }
    return;
}

/*********************************************************************/
VOID
SNMPInsertSecret (pSecurityInfo, secrets_dependency)
        tSecurityInfo      *pSecurityInfo;
        t_SECRET_DEPENDENCY *secrets_dependency;

{
    tSecret            *secret;
    if (secrets_dependency->Status == SECRETS_STATUS_INVALID)
        return;

    if ((secret = (tSecret *) PPP_MALLOC (sizeof (tSecret))) == NULL)
    {
        PPP_TRC (ALL_FAILURE, "Error: tSecret Alloc failure .\n");
        return;
    }
    secret->IdIndex = secrets_dependency->IdIndex;
    secret->Direction = secrets_dependency->Direction;
    secret->Protocol = secrets_dependency->Protocol;
    secret->Status = secrets_dependency->Status;
    secret->StatusValid = secrets_dependency->StatusValid;
    secret->IdentityLen = secrets_dependency->IdentityLen;
    secret->SecretLen = secrets_dependency->SecretLen;
    MEMCPY (secret->Secret, &secrets_dependency->Secret, secret->SecretLen);
    FsUtlEncryptPasswd ((CHR1 *) secret->Secret);
    MEMCPY (secret->SecretLmHash, &secrets_dependency->SecretLmHash,
            secret->SecretLen);
    MEMCPY (secret->Identity, &secrets_dependency->Identity,
            secret->IdentityLen);
    secret->Identity[secret->IdentityLen] = '\0';
    secret->Secret[secret->SecretLen] = '\0';
    AUTHInsertSecret (pSecurityInfo, secret);

    return;
}

/*********************************************************************
*  Function Name : SNMPSetSecuritySecret
*  Description   :
*  Parameter(s)  :
*        IfIndex - 
*        IdIndex -
*  Return Value  :
*
*********************************************************************/
VOID
SNMPSetSecuritySecret (IfIndex, IdIndex, secrets_dependency)
     UINT4               IfIndex;
     UINT4               IdIndex;
     t_SECRET_DEPENDENCY *secrets_dependency;
{
    tSecret            *secret = NULL;
    tPPPIf             *pIf = NULL;
    INT1                DeleteStatus = PPP_YES;
    UINT1               Index;
    UINT2               u2Count = 0;
    UINT1               PasswordLmHash[MAX_SECRET_SIZE];
    UINT2               Password[MAX_SECRET_SIZE * 2];

    if ((secrets_dependency->Protocol == MSCHAP_PROTOCOL)
        && (secrets_dependency->Direction == 2))
    {
        for (Index = 0; Index < secrets_dependency->SecretLen; Index++)
        {
            PasswordLmHash[Index] = secrets_dependency->Secret[Index];
            Password[u2Count++] = secrets_dependency->Secret[Index];
            Password[u2Count++] = 0x00;
        }
        MSChapLmPasswordHash (PasswordLmHash, secrets_dependency->SecretLmHash,
                              secrets_dependency->SecretLen);
        MSChapNtPasswordHash (Password, secrets_dependency->Secret,
                              secrets_dependency->SecretLen);
        secrets_dependency->SecretLen = 16;
    }

    if (IfIndex == 0)
    {
        SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
        {
            if (secrets_dependency->Status == SECRETS_STATUS_VALID)
            {
                DeleteStatus = PPP_NO;
                if (pIf->pAuthPtr == NULL)
                {
                    pIf->pAuthPtr = AUTHCreate (pIf);
                }
                if (secrets_dependency->Direction == LOCAL_TO_REMOTE)
                {
                    pIf->LcpGSEM.pNegFlagsPerIf[AUTH_IDX].FlagMask |=
                        ALLOWED_FOR_PEER_SET;
                }
            }
            else
            {
                if ((pIf->LcpGSEM.CurrentState == ACKSENT)
                    || (pIf->LcpGSEM.CurrentState == OPENED))
                {
                    DeleteStatus = PPP_NO;
                }
            }
        }

        SLL_SCAN (&IndexZeroSecurityInfo.SecretList, secret, tSecret *)
        {
            if (secret->IdIndex == IdIndex)
                break;
        }
        if (secret != NULL)
        {
            SNMPModifySecret (secret, &IndexZeroSecurityInfo,
                              secrets_dependency);
        }
        else
        {
            SNMPInsertSecret (&IndexZeroSecurityInfo, secrets_dependency);
        }

        if (secrets_dependency->Status != SECRETS_STATUS_NOTINSERVICE)
        {
            SNMPInsertAUTHPrefEntry (0, 1, CHAP_PROTOCOL);
            SNMPInsertAUTHPrefEntry (0, 2, PAP_PROTOCOL);

            SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
            {
                if ((pIf->pAuthPtr->SecurityInfo.CHAPLocToRemSecrets +
                     pIf->pAuthPtr->SecurityInfo.PAPLocToRemSecrets +
                     pIf->pAuthPtr->SecurityInfo.MSCHAPLocToRemSecrets +
                     pIf->pAuthPtr->SecurityInfo.EAPMD5LocToRemSecrets == 0)
                    && (IndexZeroSecurityInfo.CHAPLocToRemSecrets +
                        IndexZeroSecurityInfo.PAPLocToRemSecrets +
                        IndexZeroSecurityInfo.MSCHAPLocToRemSecrets +
                        IndexZeroSecurityInfo.EAPMD5LocToRemSecrets == 0))
                {

                    pIf->LcpGSEM.pNegFlagsPerIf[AUTH_IDX].FlagMask &=
                        ALLOWED_FOR_PEER_NOT_SET;
                }
                else
                {
                    pIf->LcpGSEM.pNegFlagsPerIf[AUTH_IDX].FlagMask |=
                        ALLOWED_FOR_PEER_SET;
                }
            }
        }
    }
    else
    {
        if ((pIf = PppGetIfPtr (IfIndex)) == NULL)
            return;
        if (pIf->pAuthPtr == NULL)
        {
            pIf->pAuthPtr = AUTHCreate (pIf);
        }
        if (pIf->pAuthPtr == NULL)
        {
            return;
        }
        SLL_SCAN (&pIf->pAuthPtr->SecurityInfo.SecretList, secret, tSecret *)
        {
            if (secret->IdIndex == IdIndex)
                break;
        }
        if (secret != NULL)
        {
            if (secrets_dependency->Status == SECRETS_STATUS_INVALID)
            {
                if ((pIf->LcpGSEM.CurrentState == ACKSENT)
                    || (pIf->LcpGSEM.CurrentState == OPENED))
                {
                    secret->Status = SECRETS_STATUS_INVALID;
                }
                else
                {
                    AUTHDeleteSecretEntry (&pIf->pAuthPtr->SecurityInfo,
                                           secret);
                }
            }
            else
            {

                SNMPModifySecret (secret, &pIf->pAuthPtr->SecurityInfo,
                                  secrets_dependency);
            }
        }
        else
        {
            if (secrets_dependency->Status != SECRETS_STATUS_INVALID)
                SNMPInsertSecret (&pIf->pAuthPtr->SecurityInfo,
                                  secrets_dependency);
        }

        if (secrets_dependency->Status != SECRETS_STATUS_NOTINSERVICE)
        {
            if ((pIf->pAuthPtr->SecurityInfo.CHAPLocToRemSecrets +
                 pIf->pAuthPtr->SecurityInfo.PAPLocToRemSecrets +
                 pIf->pAuthPtr->SecurityInfo.MSCHAPLocToRemSecrets +
                 pIf->pAuthPtr->SecurityInfo.EAPMD5LocToRemSecrets == 0)
                && (IndexZeroSecurityInfo.CHAPLocToRemSecrets +
                    IndexZeroSecurityInfo.PAPLocToRemSecrets +
                    IndexZeroSecurityInfo.MSCHAPLocToRemSecrets +
                    IndexZeroSecurityInfo.EAPMD5LocToRemSecrets == 0))
            {
                pIf->LcpGSEM.pNegFlagsPerIf[AUTH_IDX].FlagMask &=
                    ALLOWED_FOR_PEER_NOT_SET;
            }
            else
            {
                pIf->LcpGSEM.pNegFlagsPerIf[AUTH_IDX].FlagMask |=
                    ALLOWED_FOR_PEER_SET;
            }

            if (pIf->pAuthPtr->SecurityInfo.CHAPRemToLocSecrets +
                pIf->pAuthPtr->SecurityInfo.PAPRemToLocSecrets +
                pIf->pAuthPtr->SecurityInfo.MSCHAPRemToLocSecrets +
                pIf->pAuthPtr->SecurityInfo.EAPMD5RemToLocSecrets == 0)
            {
                /* Secret entry removed at authenticator.
                 * Disable initiation of Auth protocol negotiation */
                if (pIf->pAuthPtr->SecurityInfo.NumActiveProt > 0)
                {
                    SNMPDeleteAuthPref (IfIndex, 1);
                    SNMPDeleteAuthPref (IfIndex, 2);
                }
            }
            else
            {
                /* Secret entry added at authenticator.
                 * If no preferred auth protocol has already been set
                 * initialize CHAP as the default preferred protocol
                 * and mark that authentication is desired */
                if (pIf->pAuthPtr->SecurityInfo.NumActiveProt == 0)
                {
                    SNMPInsertAUTHPrefEntry (IfIndex, 1, CHAP_PROTOCOL);
                    SNMPInsertAUTHPrefEntry (IfIndex, 2, PAP_PROTOCOL);
                }
            }
        }
        /* If this link is a bundle then 
         * Scan and copy the secret entry to all the member links */
        if ((secrets_dependency->Status == SECRETS_STATUS_VALID) &&
            (pIf->BundleFlag == BUNDLE))
        {
            MpUpdateSecretToAllMembers ((INT4) IfIndex, (INT4) IdIndex,
                                        SECRETS_STATUS_VALID);
        }
    }
    PPP_UNUSED (DeleteStatus);
    return;
}

/*****************************************************/
VOID
AuthFillSecretDependency (UINT4 LinkIndex, UINT4 IdIndex)
{
    tSecret            *pSecret = NULL;

    if ((pSecret = SNMPGetSecret (LinkIndex, IdIndex)) == NULL)
    {
        BZERO (&gSecrets_dependency, sizeof (t_SECRET_DEPENDENCY));
        gSecrets_dependency.Status = SECRETS_STATUS_NOTINSERVICE;
        gSecrets_dependency.StatusValid = 0;
        gSecrets_dependency.IfIndex = LinkIndex;
        gSecrets_dependency.IdIndex = IdIndex;
    }
    else
    {
        BZERO (&gSecrets_dependency, sizeof (t_SECRET_DEPENDENCY));
        gSecrets_dependency.IfIndex = LinkIndex;
        gSecrets_dependency.IdIndex = IdIndex;
        gSecrets_dependency.Status = pSecret->Status;
        gSecrets_dependency.StatusValid = pSecret->StatusValid;
        gSecrets_dependency.Direction = pSecret->Direction;
        gSecrets_dependency.Protocol = pSecret->Protocol;
        gSecrets_dependency.Status = pSecret->Status;
        gSecrets_dependency.IdentityLen = pSecret->IdentityLen;
        gSecrets_dependency.SecretLen = pSecret->SecretLen;
        MEMCPY (gSecrets_dependency.Secret, &pSecret->Secret,
                gSecrets_dependency.SecretLen);
        FsUtlDecryptPasswd ((CHR1 *) gSecrets_dependency.Secret);
        MEMCPY (gSecrets_dependency.SecretLmHash, &pSecret->SecretLmHash,
                gSecrets_dependency.SecretLen);
        MEMCPY (gSecrets_dependency.Identity, &pSecret->Identity,
                gSecrets_dependency.IdentityLen);
        gSecrets_dependency.Identity[gSecrets_dependency.IdentityLen] = '\0';
        gSecrets_dependency.Secret[gSecrets_dependency.SecretLen] = '\0';
    }
}

/* This funtion copies the oid from dest to src */
INT1
PPPCopyOid (tSNMP_OID_TYPE * pDestOid, tSNMP_OID_TYPE * pSrcOid)
{
    UINT4               i, u4_oid_len;
    if ((pSrcOid == NULL) || (pSrcOid->u4_Length == 0))
    {
        return (SNMP_SUCCESS);
    }

    u4_oid_len = pSrcOid->u4_Length;

    if (pDestOid == NULL)
    {
        if ((pDestOid =
             MEM_CALLOC (sizeof (tSNMP_OID_TYPE), 1, tSNMP_OID_TYPE)) == NULL)
        {
            return (SNMP_FAILURE);
        }

        if ((pDestOid->pu4_OidList =
             MEM_CALLOC (sizeof (UINT4), u4_oid_len, UINT4)) == NULL)
        {
            FREE (pDestOid);
            return (SNMP_FAILURE);
        }
    }

    pDestOid->u4_Length = u4_oid_len;

    for (i = 0; i < u4_oid_len; i++)
    {
        pDestOid->pu4_OidList[i] = pSrcOid->pu4_OidList[i];
    }
    FREE (pDestOid);
    return (SNMP_SUCCESS);
}

/* Copy newly added secret or delete latest removed secret
 * to/from all multilink bundle members */
INT4
MpUpdateSecretToAllMembers (INT4 i4BundleIfIndex, INT4 i4SecretIdIndex,
                            INT4 i4SecretStatus)
{
    tPPPIf             *pBundleIf = NULL;
    tSecret            *pSecret = NULL;
    tPPPIf             *pMemberIf = NULL;
    t_SECRET_DEPENDENCY tSecretsDependency;

    pBundleIf = PppGetIfPtr ((UINT4) i4BundleIfIndex);

    if ((pBundleIf != NULL) && (pBundleIf->pAuthPtr != NULL))
    {
        SLL_SCAN (&pBundleIf->pAuthPtr->SecurityInfo.SecretList, pSecret,
                  tSecret *)
        {
            if (pSecret->IdIndex == (UINT4) i4SecretIdIndex)
                break;
        }
        if (pSecret == NULL)
            return PPP_FAILURE;

        SLL_SCAN (&PPPIfList, pMemberIf, tPPPIf *)
        {
            if ((pMemberIf->BundleFlag != MEMBER_LINK) ||
                (pMemberIf->MPInfo.MemberInfo.pBundlePtr != pBundleIf))
            {
                continue;
            }

            MEMSET (&tSecretsDependency, 0, sizeof (t_SECRET_DEPENDENCY));

            tSecretsDependency.IfIndex = pMemberIf->LinkInfo.IfIndex;
            tSecretsDependency.IdIndex = (UINT4) i4SecretIdIndex;
            tSecretsDependency.Status = (UINT1) i4SecretStatus;

            if (i4SecretStatus == SECRETS_STATUS_VALID)
            {
                STRCPY (tSecretsDependency.Identity, pSecret->Identity);
                STRCPY (tSecretsDependency.Secret, pSecret->Secret);
                FsUtlDecryptPasswd ((CHR1 *) tSecretsDependency.Secret);
                tSecretsDependency.Direction = pSecret->Direction;
                tSecretsDependency.Protocol = pSecret->Protocol;
                tSecretsDependency.IdentityLen = pSecret->IdentityLen;
                tSecretsDependency.SecretLen = pSecret->SecretLen;
                tSecretsDependency.StatusValid = pSecret->StatusValid;
            }

            SNMPSetSecuritySecret (pMemberIf->LinkInfo.IfIndex,
                                   (UINT4) i4SecretIdIndex, &tSecretsDependency);
        }
    }
    return PPP_SUCCESS;
}

/* Copy all secret entries in the bundle to the newly added member */
VOID
MpAddAllSecretsToNewMember (tPPPIf * pBundleIf, tPPPIf * pMemberIf)
{
    t_SECRET_DEPENDENCY tSecretsDependency;
    tSecret            *pSecret = NULL;

    if (pBundleIf->pAuthPtr == NULL)
    {
        return;
    }

    SLL_SCAN (&pBundleIf->pAuthPtr->SecurityInfo.SecretList, pSecret, tSecret *)
    {
        MEMSET (&tSecretsDependency, 0, sizeof (t_SECRET_DEPENDENCY));

        tSecretsDependency.IfIndex = pMemberIf->LinkInfo.IfIndex;
        tSecretsDependency.IdIndex = pSecret->IdIndex;
        tSecretsDependency.Status = pSecret->Status;

        STRCPY (tSecretsDependency.Identity, pSecret->Identity);
        STRCPY (tSecretsDependency.Secret, pSecret->Secret);
        FsUtlDecryptPasswd ((CHR1 *) tSecretsDependency.Secret);
        tSecretsDependency.Direction = pSecret->Direction;
        tSecretsDependency.Protocol = pSecret->Protocol;
        tSecretsDependency.IdentityLen = pSecret->IdentityLen;
        tSecretsDependency.SecretLen = pSecret->SecretLen;
        tSecretsDependency.StatusValid = pSecret->StatusValid;

        SNMPSetSecuritySecret (pMemberIf->LinkInfo.IfIndex,
                               pSecret->IdIndex, &tSecretsDependency);
    }

    return;
}

/* Remove all secret entries from the bundle member */
VOID
MpRemoveSecretsFromMember (tPPPIf * pMemberIf)
{
    tSecret            *pSecret = NULL;

    pSecret = (tSecret *)
        TMO_SLL_First (&pMemberIf->pAuthPtr->SecurityInfo.SecretList);

    while (pSecret != NULL)
    {
        AUTHDeleteSecretEntry (&pMemberIf->pAuthPtr->SecurityInfo, pSecret);

        pSecret = (tSecret *)
            TMO_SLL_First (&pMemberIf->pAuthPtr->SecurityInfo.SecretList);
    }

    if (pMemberIf->pAuthPtr->SecurityInfo.CHAPRemToLocSecrets +
        pMemberIf->pAuthPtr->SecurityInfo.PAPRemToLocSecrets +
        pMemberIf->pAuthPtr->SecurityInfo.MSCHAPRemToLocSecrets +
        pMemberIf->pAuthPtr->SecurityInfo.EAPMD5RemToLocSecrets == 0)
    {
        /* Secret entry removed at authenticator.
         * Disable initiation of Auth protocol negotiation */
        if (pMemberIf->pAuthPtr->SecurityInfo.NumActiveProt > 0)
        {
            SNMPDeleteAuthPref (pMemberIf->LinkInfo.IfIndex, 1);
            SNMPDeleteAuthPref (pMemberIf->LinkInfo.IfIndex, 2);
        }
    }
}
