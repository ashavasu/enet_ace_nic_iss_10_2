/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppppap.c,v 1.5 2014/03/11 14:02:51 siva Exp $
 *
 * Description:This file contains interface and internal
 *               procedures of PAP module.
 *
 *******************************************************************/
#include "authcom.h"
#include "lcpdefs.h"
#include "lcpexts.h"
#include "gcpdefs.h"
#include "gsemdefs.h"
#include "globexts.h"
#include "pppl2tp.h"
#include "pppcom.h"
#include "genexts.h"
#include "genproto.h"

/* #ifdef L2TP_WANTED */
#include "l2tp.h"
/* #endif */
/*********************************************************************
*  Function Name : PAPClientInit
*  Description   :
*                       This function initialises the tAUTHIf and
*  is invoked by the ProceedAuth() function. It sets the time out value and
*  maximam retransmission to their default values and  sets the state to
*  INITIAL .
*  Parameter(s)  :
*         pAuthPtr - points to the interface structure to be initialized.
*  Return Values : VOID
*********************************************************************/
VOID
PAPClientInit (tAUTHIf * pAuthPtr)
{

    pAuthPtr->ClientInfo.ClientPap.pPAPCurrentSecret = NULL;
    pAuthPtr->ClientInfo.ClientPap.State = PAP_CLIENT_STATE_INITIAL;
    pAuthPtr->ClientInfo.ClientPap.TimeoutTime =
        pAuthPtr->pIf->LinkInfo.TimeOutTime;
    pAuthPtr->ClientInfo.ClientPap.MaxTransmits =
        pAuthPtr->pIf->LinkInfo.MaxReTransmits;
    /** ANVL-FIX-2000 **/
    pAuthPtr->PapLastSecretIndex = INIT_VAL;
    pAuthPtr->PAPSearchList = IF_SPECIFIC_LIST;
    pAuthPtr->ClientInfo.ClientPap.TransmitCount = 0;
    /** ANVL-FIX-2000 **/
    return;
}

/*********************************************************************
*  Function Name : PAPServerInit
*  Description   :
*                       This function initialises the tAUTHIf and
*  is invoked by the ProceedAuth() function. It sets the time out value and
*  maximam retransmission to their default values and  sets the state to
*  INITIAL .
*  Parameter(s)  :
*         pAuthPtr - points to the interface structure to be initialized.
*  Return Values : VOID
*********************************************************************/
VOID
PAPServerInit (tAUTHIf * pAuthPtr)
{

    pAuthPtr->ServerInfo.ServerPap.State = PAP_SERVER_STATE_INITIAL;

    return;
}

/*********************************************************************
*  Function Name : PAPAuthWithPeer
*  Description   :
*                       This function is called by the ProceedAuth Module for
*  authenticating  this entity  with the Peer. It request for authentication
*  to the peer by invoking PAPSendAuthReq().This function is called only if
*  the authentication protocol negotiated is PAP.
*  Parameter(s)  :
*               pAuthPtr - points to the PAP interface structure
*  Return Values :
*
*********************************************************************/
VOID
PAPAuthWithPeer (tAUTHIf * pAuthPtr)
{

/* Check if the Link is Up. Only then Protocol is activated... */

    if (pAuthPtr->ClientInfo.ClientPap.State == PAP_CLIENT_STATE_INITIAL ||
        pAuthPtr->ClientInfo.ClientPap.State == PAP_CLIENT_STATE_PENDING)
    {
        pAuthPtr->ClientInfo.ClientPap.State = PAP_CLIENT_STATE_PENDING;
    }
    else
    {
        PAPSendAuthReq (pAuthPtr, NORMAL_TRANSMISSION);
        /* Start protocol */
    }
    return;
}

/*********************************************************************
*  Function Name : PAPAuthPeer
*  Description   :
*                       This function is called by the ProceedAuth Module for
*  authenticating the Peer. It sets the server state of PAP structure to the
*  LISTEN state  and waits for the peer to send his  userid / password.This
*  function is called only if the authentication protocol negotiated is PAP.
*  Parameter(s)  :
*               pAuthPtr - points to the PAP interface structure
*  Return Values : VOID
*********************************************************************/
VOID
PAPAuthPeer (tAUTHIf * pAuthPtr)
{

    /*  check for the Link Up */
    if (pAuthPtr->ServerInfo.ServerPap.State == PAP_SERVER_STATE_INITIAL ||
        pAuthPtr->ServerInfo.ServerPap.State == PAP_SERVER_STATE_PENDING)
        pAuthPtr->ServerInfo.ServerPap.State = PAP_SERVER_STATE_PENDING;
    else
        pAuthPtr->ServerInfo.ServerPap.State = PAP_SERVER_STATE_LISTEN;

    pAuthPtr->ServerInfo.ServerPap.PapResponseTimer.Param1 =
        PTR_TO_U4 (pAuthPtr);
    PPPRestartTimer (&pAuthPtr->ServerInfo.ServerPap.PapResponseTimer,
                     PAP_RESP_TIMER,
                     (UINT4) (10 * pAuthPtr->pIf->LinkInfo.TimeOutTime));
    return;
}

/*********************************************************************
*  Function Name : PAPTimeOut
*  Description   :
*                       This function is invoked when the retransmission timer
*  expires. It invokes the PAPSendReq() function to resend the PAP
*  authentication request packet.
*  Parameter(s)  :
*           pAuthPtr -  pointer to the interface structure of PAP
*
*  Return Values :VOID
*********************************************************************/
VOID
PAPTimeOut (VOID *pAuth)
{

    tAUTHIf            *pAuthPtr;
    pAuthPtr = (tAUTHIf *) pAuth;
    if (pAuthPtr->ClientInfo.ClientPap.State == PAP_CLIENT_STATE_AUTHREQ)
    {
        if (pAuthPtr->ClientInfo.ClientPap.TransmitCount >=
            pAuthPtr->ClientInfo.ClientPap.MaxTransmits)
        {
            PPP_DBG ("No response to PAP authenticate-requests");
            pAuthPtr->ClientInfo.ClientPap.State = PAP_CLIENT_STATE_BADAUTH;
            LCPAuthProcessFailed (pAuthPtr->pIf);
            if (pTermGSEM != NULL)
            {
                GSEMRun (pTermGSEM, RXJ_MINUS);
                pTermGSEM = NULL;    /* FIX-2000 */
            }
        }
        else
        {
            PAPSendAuthReq (pAuthPtr, RETRANSMISSION);
            /* Send Authenticate-Request */
        }
    }
    return;
}

/*********************************************************************
*  Function Name : PAPResponseTimeOut
*  Description   :
*                       This function is invoked when no PAP auth Req is
*                       received from the peer after 10*restart timeout 
*                       value.
*  Parameter(s)  :
*           pAuthPtr -  pointer to the interface structure of PAP
*
*  Return Values :VOID
*********************************************************************/
VOID
PAPResponseTimeOut (VOID *pAuth)
{
    tAUTHIf            *pAuthPtr;
    pAuthPtr = (tAUTHIf *) pAuth;
    PPP_DBG ("No  PAP authenticate-requests from the peer");
    pAuthPtr->ServerInfo.ServerPap.State = PAP_SERVER_STATE_INITIAL;
    GSEMRun (&(pAuthPtr->pIf->LcpGSEM), RXJ_MINUS);
#ifdef RADIUS_WANTED
    pAuthPtr->pIf->RadInfo.RadReqId = INIT_VAL;
#endif
    return;
}

/*********************************************************************
*  Function Name :  PAPRecdProtRej
*  Description   :
*                This function is called by the LLIRx() function when a protocol
*  reject packet is received for the PAP protocol.    
*  Parameter(s)  :
*           pAuthPtr   -    points to the PAP interface structure
*  Return Values : VOID
*********************************************************************/
VOID
PAPRecdProtRej (tAUTHIf * pAuthPtr)
{

    if (pAuthPtr->ClientInfo.ClientPap.State == PAP_CLIENT_STATE_AUTHREQ)
    {
        PPP_DBG ("PAP authentication failed due to protocol-reject");
    }
    if (pAuthPtr->ServerInfo.ServerPap.State == PAP_SERVER_STATE_LISTEN)
    {
        PPP_DBG ("PAP authentication of peer failed (protocol-reject)");
        LCPAuthProcessFailed (pAuthPtr->pIf);
    }

    AUTHCloseServer (pAuthPtr);
    AUTHCloseClient (pAuthPtr);

    return;
}

/***************************************************************************
*  Function Name :  PAPInput
*  Description   :
*                       This procedure is called by the LLI whenever there is an
*  incoming packet for PAP module. Like PAPInput(), this function processes
*  the PAP packet and invokes appropriate internal functions for further
*  actions.
*  Parameter(s)  :
*        pAuthPtr       -    points to the PAP interface structure for which
*                                   this incoming packet corresponds to
*        pInpacket      -    points to the incoming packet
*        Length         -    Length of the incoming packet.
*  Return Values : VOID
*********************************************************************/
VOID
PAPInput (tAUTHIf * pAuthPtr, t_MSG_DESC * pInpacket, UINT2 Length)
{
    UINT1               Code;
    UINT1               Id;
    UINT2               Len;

    if (UtilExtractAndValidateCPHdr
        (pAuthPtr->pIf, pInpacket, Length, &Code, &Id, &Len) == DISCARD)
    {
        return;
    }

    switch (Code)
    {
        case PAP_AUTH_REQ:
            if ((pAuthPtr->pIf->LcpGSEM.pNegFlagsPerIf[AUTH_IDX].FlagMask
                 & ACKED_BY_PEER_MASK) == 0)
                return;

            if (pAuthPtr->ServerProt == PAP_PROTOCOL)
                PAPRcvdAuthReq (pAuthPtr, pInpacket, Len, Id);
            else
                PPPSendRejPkt (pAuthPtr->pIf, pInpacket, Length, LCP_PROTOCOL,
                               PROT_REJ_CODE);
            break;

        case PAP_AUTH_ACK:
            if ((pAuthPtr->pIf->LcpGSEM.pNegFlagsPerIf[AUTH_IDX].FlagMask
                 & ACKED_BY_LOCAL_MASK) == 0)
                return;

            if (pAuthPtr->ClientProt == PAP_PROTOCOL)
                PAPRcvdAuthAck (pAuthPtr, pInpacket, Len, Id);
            else
                PPPSendRejPkt (pAuthPtr->pIf, pInpacket, Length, LCP_PROTOCOL,
                               PROT_REJ_CODE);
            break;

        case PAP_AUTH_NAK:
            if ((pAuthPtr->pIf->LcpGSEM.pNegFlagsPerIf[AUTH_IDX].FlagMask
                 & ACKED_BY_LOCAL_MASK) == 0)
                return;

            if (pAuthPtr->ClientProt == PAP_PROTOCOL)
                PAPRcvdAuthNak (pAuthPtr, pInpacket, Len, Id);
            else
                PPPSendRejPkt (pAuthPtr->pIf, pInpacket, Length, LCP_PROTOCOL,
                               PROT_REJ_CODE);
            break;

        default:
            DISCARD_PKT (pAuthPtr->pIf,
                         "Error : Invalid PAP packet received..");
    }
    return;
}

/*********************************************************************
*  Function Name : PAPRcvdAuthReq
*  Description   :
*                       This function  is called by the PAPInput when the
*  incoming  packet is identified as Authentication Request. It validates the
*  request and sends back either ACK or NAK to the Peer. It also passes the
*  result to LCP module by calling LCPPassAuthResult function,if no other
*  authentication is pending.
*  Parameter(s)    :
*     pAuthPtr     -  pointer to tAUTHIf which contains all
*                                   the PAP information.
*     pInPacket    -  pointer to  Input Packet String.
*     Length       -  Length of the Input Packet
*     Id           -  Identifier  received along with the packet and to
*                          be used in response
*  Return Values : VOID
*********************************************************************/
VOID
PAPRcvdAuthReq (tAUTHIf * pAuthPtr, t_MSG_DESC * pInpacket, UINT2 Length,
                UINT1 Id)
{
    UINT1               IdLen;
    UINT1               PeerId[PPP_MAX_NAME_LENGTH];
    UINT1               PasswdLen;
    UINT1               Passwd[MAX_SECRET_LENGTH];
    INT2                ReturnCode;
    UINT1               MsgLen = 0;
    UINT1               Msg[MAX_MSG_SIZE];
    t_MSG_DESC         *pOutBuf = NULL;
    UINT2               Len = 0;
#ifdef L2TP_WANTED
    UINT1               IsLAC;
#endif

    PPP_TRC (CONTROL_PLANE, "PAP_AUTH_REQ\t IN :");
    if ((pOutBuf =
         (t_MSG_DESC *) (VOID *) ALLOCATE_BUFFER (PPP_INFO_OFFSET +
                                                  MAX_MSG_SIZE +
                                                  MSG_LEN_FIELD_LEN,
                                                  PPP_INFO_OFFSET)) == NULL)
    {
        PRINT_MEM_ALLOC_FAILURE;
        return;
    }

#ifdef L2TP_WANTED
    IsLAC = PPPIsL2TPTunnelModeLAC (pAuthPtr->pIf);
#endif
    BZERO (Passwd, MAX_SECRET_LENGTH);
    BZERO (PeerId, PPP_MAX_NAME_LENGTH);

    /* RADIUS_WANTED:This case is valid for RADIUS auth. also! */
    if (pAuthPtr->ServerInfo.ServerPap.State < PAP_SERVER_STATE_LISTEN)
        return;

    /*
     * If we receive a duplicate authenticate-request, we are
     * supposed to return the same status as for the first request.
     */

    Len = CP_HDR_LEN + MSG_LEN_FIELD_LEN;

#ifdef L2TP_WANTED
    if (IsLAC == FALSE)
    {
#endif
        if (pAuthPtr->ServerInfo.ServerPap.State == PAP_SERVER_STATE_OPEN)
        {
            STRCPY (Msg, "U have already logged in....");
            MsgLen = (UINT1) STRLEN (Msg);
            Len = (UINT2) (Len + MsgLen);
            PPP_TRC1 (CONTROL_PLANE, "\t\t\t : [%s] ", Msg);
            FILL_PKT_HEADER (pOutBuf, PAP_AUTH_ACK, Id, Len);
            APPEND1BYTE (pOutBuf, MsgLen);
            APPENDSTR (pOutBuf, Msg, MsgLen);

            PPPLLITxPkt (pAuthPtr->pIf, pOutBuf, Len, PAP_PROTOCOL);

            return;
        }

        if (pAuthPtr->ServerInfo.ServerPap.State == PAP_SERVER_STATE_BADAUTH)
        {

            STRCPY (Msg, "U have failed the authentication....");
            PPP_TRC1 (CONTROL_PLANE, "\t\t\t : [%s] ", Msg);
            Len = (UINT2) (Len + STRLEN (Msg));

            FILL_PKT_HEADER (pOutBuf, PAP_AUTH_NAK, Id, Len);
            APPEND1BYTE (pOutBuf, MsgLen);
            APPENDSTR (pOutBuf, Msg, MsgLen);

            PPPLLITxPkt (pAuthPtr->pIf, pOutBuf, Len, PAP_PROTOCOL);

            return;
        }
#ifdef L2TP_WANTED
    }
#endif

#ifdef L2TP_WANTED
#define RELEASE_PAP_BUFFER(x)        if (IsLAC == FALSE) {\
                    RELEASE_BUFFER(x)\
                    }
#else
#define RELEASE_PAP_BUFFER(x)    RELEASE_BUFFER(x)
#endif

    if (Length < 1)
    {
        DISCARD_PKT (pAuthPtr->pIf, "PAPRcvdAuthReq: rcvd short packet.");
        RELEASE_PAP_BUFFER (pOutBuf);
        return;
    }

    EXTRACT1BYTE (pInpacket, IdLen);
    Length--;

    if (Length < IdLen)
    {
        DISCARD_PKT (pAuthPtr->pIf, "PAPRcvdAuthReq: rcvd short packet.");
        RELEASE_PAP_BUFFER (pOutBuf);
        return;
    }

    if (IdLen >= PPP_MAX_NAME_LENGTH)
    {
        DISCARD_PKT (pAuthPtr->pIf, "Id Len Too Long !!!");
        RELEASE_PAP_BUFFER (pOutBuf);
        return;
    }

    EXTRACTSTR (pInpacket, PeerId, IdLen);

    Length = (UINT2) (Length - IdLen);

    if (Length < 1)
    {
        DISCARD_PKT (pAuthPtr->pIf, "PAPRcvdAuthReq: rcvd short packet.");
        RELEASE_PAP_BUFFER (pOutBuf);
        return;
    }

    EXTRACT1BYTE (pInpacket, PasswdLen);

    Length--;

    if ((Length < PasswdLen) || (PasswdLen >= MAX_SECRET_LENGTH))
    {
        DISCARD_PKT (pAuthPtr->pIf, "Invalid password Length");
        RELEASE_PAP_BUFFER (pOutBuf);
        return;
    }

    EXTRACTSTR (pInpacket, Passwd, PasswdLen);

    pAuthPtr->pIf->LLHCounters.InGoodOctets += Len;

#ifdef L2TP_WANTED
    if (IsLAC == TRUE)
    {
        PPPIndicateAuthInfoToLAC (pAuthPtr->pIf->LinkInfo.IfIndex,
                                  L2TP_PAP_AUTH, Id, PeerId, IdLen, Passwd,
                                  PasswdLen, NULL, 0);
    }
#endif
    PPPStopTimer (&pAuthPtr->ServerInfo.ServerPap.PapResponseTimer);

    /*
     * Check the username and password given.
     */

    /* if Radius authentication is used pass the request to 
     * radius client,make the auth state pending, and return !
     * */
#ifdef RADIUS_WANTED
    if (gu1aaaMethod == AAA_RADIUS)
    {

        ReturnCode =
            (INT2) (PppRadPapAuthenticate
                    (PeerId, IdLen, Passwd, PasswdLen,
                     pAuthPtr->pIf->LinkInfo.IfIndex));
        pAuthPtr->pIf->RadInfo.RadReqId = (INT1) ReturnCode;

        if (ReturnCode != NOT_OK)
        {
            pAuthPtr->ServerInfo.ServerPap.State = PAP_SERVER_STATE_PENDING;
            pAuthPtr->pIf->RadInfo.PppAuthId = Id;
            if ((pAuthPtr->pIf->RadInfo.Username =
                 (UINT1 *) CALLOC (PPP_MAX_NAME_LENGTH, 1)) == NULL)
            {
                PPP_TRC (ALL_FAILURE, "Memory Alloc For UserName Failed");
                return;
            }
            MEMCPY (pAuthPtr->pIf->RadInfo.Username, PeerId, IdLen);
            return;
        }

    }                            /* == AAA_RADIUS */

#endif /* RADIUS_WANTED */

    ReturnCode = CheckPasswd (pAuthPtr, PeerId, IdLen, Passwd, PasswdLen);
    if (ReturnCode == PAP_AUTH_ACK)
    {
        STRCPY (Msg, "U have already logged in....");
        MsgLen = (UINT1) STRLEN (Msg);
        PPP_TRC1 (CONTROL_PLANE, "\t\t\t :[%s]", Msg);
        Len = (UINT2) (Len + MsgLen);

        FILL_PKT_HEADER (pOutBuf, PAP_AUTH_ACK, Id, Len);
        APPEND1BYTE (pOutBuf, MsgLen);
        APPENDSTR (pOutBuf, Msg, MsgLen);

        PPPLLITxPkt (pAuthPtr->pIf, pOutBuf, Len, PAP_PROTOCOL);

        pAuthPtr->ServerInfo.ServerPap.State = PAP_SERVER_STATE_OPEN;
        LCPUpdateAuthStatus (pAuthPtr->pIf, PAP_PEER_TRUE);

    }
    else
    {

        STRCPY (Msg, "Sorry!!! U have failed to login ....");
        PPP_TRC1 (CONTROL_PLANE, "\t\t\t : [%s] ", Msg);
        MsgLen = (UINT1) STRLEN (Msg);

        Len = (UINT2) (Len + MsgLen);

        FILL_PKT_HEADER (pOutBuf, PAP_AUTH_NAK, Id, Len);
        APPEND1BYTE (pOutBuf, MsgLen);
        APPENDSTR (pOutBuf, Msg, MsgLen);

        PPPLLITxPkt (pAuthPtr->pIf, pOutBuf, Len, PAP_PROTOCOL);

        pAuthPtr->ServerInfo.ServerPap.State = PAP_SERVER_STATE_BADAUTH;
        LCPAuthProcessFailed (pAuthPtr->pIf);

    }
    return;
}

/*********************************************************************
*  Function Name : CheckPasswd 
*  Description   :
*                This function validates the Passwd/Id pair and returns app-
*  propriate code.
*  Parameter(s)  :
*     pAuthPtr   -  pointer to which contains all
*                                   the PAP information.
*        pPeerId       -  PeerId
*        PeerIdLen     -  Length of the Id
*     Passwd       -  Passwd
*     PasswdLen  -  Length of Passwd
*    Return Value : AUTH_ACK / AUTH_NAK
*********************************************************************/
UINT1
CheckPasswd (tAUTHIf * pAuthPtr, UINT1 *pPeerId, UINT1 PeerIdLen,
             UINT1 *pPasswd, UINT1 PasswdLen)
{
    tSecret            *pSecret;

    /* Makefile changes - unused */
    PPP_DBG1 ("PasswdLen = %d", PasswdLen);

    if ((pSecret =
         AUTHGetSecret (pAuthPtr, pPeerId, PeerIdLen, PAP_PROTOCOL,
                        SERVER)) == NULL)
    {
        PPP_DBG ("Check failed : Passwd/id not available");
        return (PAP_AUTH_NAK);
    }

    FsUtlEncryptPasswd ((CHR1 *) pPasswd);

    if (MEMCMP (pSecret->Secret, pPasswd, pSecret->SecretLen) ||
        (pSecret->SecretLen != PasswdLen))
    {                            /*02032001 */
        PPP_DBG ("Check failed : Passwd/Id not matched");
        return (PAP_AUTH_NAK);
    }
    return (PAP_AUTH_ACK);
}

/*********************************************************************
*  Function Name :  PAPRcvdAuthAck
*  Description   :
*                       This function  is called by the PAPInput when the
*  incoming packet is an  Acknowledgement to the sent Request. It checks ifit is
*  valid and no other authentication need to be done , then it informs the LCP
*  that the authentication is over.
*  Parameter(s)  :
*           pAuthPtr   -  pointer to tAUTHIf which contains all the PAP info.
*           pInPacket  -  pointer to  Input Packet String.
*           Length     -  Length of the Input Packet
*           Identifier -  Identifier received along with the packet and to 
*                             be used in response
*  Return Values : VOID
*********************************************************************/
VOID
PAPRcvdAuthAck (tAUTHIf * pAuthPtr, t_MSG_DESC * pInpacket, UINT2 Length,
                UINT1 Id)
{
    UINT1               Msg[MAX_MSG_SIZE];
    UINT1               MsgLen;
    UINT2               Len;

    BZERO (Msg, sizeof (Msg));
    Len = Length;
    PPP_TRC (CONTROL_PLANE, "PAP_AUTH_ACK\t : IN");
    if (pAuthPtr->ClientInfo.ClientPap.State == PAP_CLIENT_STATE_AUTHREQ)
    {

        if (Id != pAuthPtr->ClientInfo.ClientPap.UserId)
        {
            DISCARD_PKT (pAuthPtr->pIf, "Error : Id doesnot match....");
            PPP_TRC (CONTROL_PLANE, "ERROR : ID DOESNOT MATCH... ");
            return;
        }

        if (Length < sizeof (UINT1))
        {
            DISCARD_PKT (pAuthPtr->pIf, "PAPRcvdAuthAck: rcvd short packet.");
            return;
        }

        EXTRACT1BYTE (pInpacket, MsgLen);

        Length--;

        if ((Length != MsgLen) || (MsgLen >= MAX_MSG_SIZE))
        {
                        /** ANVL-FIX-2000 **/
            DISCARD_PKT (pAuthPtr->pIf, "Invalid Msg length");
            return;
        }

        EXTRACTSTR (pInpacket, Msg, MsgLen);

        pAuthPtr->pIf->LLHCounters.InGoodOctets += Len;

        pAuthPtr->ClientInfo.ClientPap.State = PAP_CLIENT_STATE_OPEN;

        PPPStopTimer (&pAuthPtr->ClientInfo.ClientPap.PapResendTimer);

        LCPUpdateAuthStatus (pAuthPtr->pIf, PAP_WITH_PEER_TRUE);
    }
    return;
}

/*********************************************************************
*  Function Name : PAPRcvdAuthNak
*  Description   :
*                       This function  is called by the PAPInput when there is
*   an Negative Ack packet received . It validates the  input packet and
*  informs LCP that the authentication has failed.
*  Parameter(s)  :
*     pAuthPtr   -  pointer to tAUTHIf which contains all the PAP information.
*     pInPacket  -  pointer to  Input Packet String.
*     Length     -  Length of the Input Packet
*     Identifier -  Identifier received along with the packet and
*                               to be used in response
*  Return Values : VOID
*********************************************************************/
VOID
PAPRcvdAuthNak (tAUTHIf * pAuthPtr, t_MSG_DESC * pInpacket, UINT2 Length,
                UINT1 Identifier)
{
    UINT1               Msg[50];
    UINT1               MsgLen = 0;
    UINT2               Len = 0;

    PPP_TRC (CONTROL_PLANE, "PAP_AUTH_NAK\t IN:");
    if (pAuthPtr->ClientInfo.ClientPap.State == PAP_CLIENT_STATE_AUTHREQ)
    {
        if (Identifier != pAuthPtr->ClientInfo.ClientPap.UserId)
        {
            DISCARD_PKT (pAuthPtr->pIf, "Error : Id doesnot match....");
            PPP_TRC (CONTROL_PLANE, "ERROR : ID DOESNOT MATCH ");
            return;
        }

        if (Length < 1)
        {
            DISCARD_PKT (pAuthPtr->pIf, "PAPRcvdAuthNak: rcvd short packet.");
            return;
        }

        EXTRACT1BYTE (pInpacket, MsgLen);
        Length--;

        if (Length != MsgLen)
        {
            DISCARD_PKT (pAuthPtr->pIf, "PAPRcvdAuthNak: rcvd short packet.");
            return;
        }

        EXTRACTSTR (pInpacket, Msg, MsgLen);

        pAuthPtr->pIf->LLHCounters.InGoodOctets += Len;

        PPPStopTimer (&pAuthPtr->ClientInfo.ClientPap.PapResendTimer);

        if ((pAuthPtr->ClientInfo.ClientPap.pPAPCurrentSecret =
             AUTHGetSecretName (pAuthPtr, PAP_PROTOCOL, CLIENT)) == NULL)
        {

            PPP_TRC (ALL_FAILURE,
                     "No secret available to authenticate with the remote");
            pAuthPtr->ClientInfo.ClientPap.State = PAP_CLIENT_STATE_BADAUTH;
            /*  Just wait here.. Let the Peer initiate the LINK down process */
            return;
        }
        else
            PAPSendAuthReq (pAuthPtr, NORMAL_TRANSMISSION);
        return;
    }
    return;
}

/*********************************************************************
*  Function Name : PAPSendAuthReq
*  Description   :
*               This function is called by the PAPAuthWithPeer() function
*  and is used to sending the PAP authentication request packet. It is also
*  used by the PAPTimeout() function when the retransmission timer expires.
*  Parameter(s)  :
*     pAuthPtr  -  points to the PAP interface structure
*     ReTxFlag  -  Retransmission Flag
*  Return Values : VOID
*********************************************************************/
VOID
PAPSendAuthReq (tAUTHIf * pAuthPtr, UINT1 ReTxFlag)
{
    UINT1               au1Password[MAX_SECRET_SIZE];
    t_MSG_DESC         *pOutBuf;
    tSecret            *pId;
    UINT2               Length = 0;

    BZERO (au1Password, MAX_SECRET_SIZE);
    PPP_UNUSED (ReTxFlag);
    PPP_TRC (CONTROL_PLANE, "PAP_AUTH_REQ : \t OUT");
    if (pAuthPtr->ClientInfo.ClientPap.pPAPCurrentSecret == NULL)
    {
        if ((pAuthPtr->ClientInfo.ClientPap.pPAPCurrentSecret =
             AUTHGetSecretName (pAuthPtr, PAP_PROTOCOL, CLIENT)) == NULL)
        {
            return;
        }
    }

    pId = pAuthPtr->ClientInfo.ClientPap.pPAPCurrentSecret;

    /* 2 : one for id_len and the other for secret_len */
    Length =
        (UINT2) (pId->IdentityLen + pId->SecretLen + PEER_ID_FIELD_LEN +
                 PASSWD_FIELD_LEN + CP_HDR_LEN);

    if ((pOutBuf =
         (t_MSG_DESC *) (VOID *) ALLOCATE_BUFFER (PPP_INFO_OFFSET + Length,
                                                  PPP_INFO_OFFSET)) != NULL)
    {

        pAuthPtr->ClientInfo.ClientPap.UserId++;

        FILL_PKT_HEADER (pOutBuf, PAP_AUTH_REQ,
                         pAuthPtr->ClientInfo.ClientPap.UserId, Length);

        APPEND1BYTE (pOutBuf, pId->IdentityLen);
        APPENDSTR (pOutBuf, pId->Identity, pId->IdentityLen);
        APPEND1BYTE (pOutBuf, pId->SecretLen);
        MEMCPY (au1Password, pId->Secret, pId->SecretLen);
        FsUtlDecryptPasswd ((CHR1 *) au1Password);
        APPENDSTR (pOutBuf, au1Password, pId->SecretLen);

        PPPLLITxPkt (pAuthPtr->pIf, pOutBuf, Length, PAP_PROTOCOL);

        pAuthPtr->ClientInfo.ClientPap.PapResendTimer.Param1 =
            PTR_TO_U4 (pAuthPtr);
        PPPStartTimer (&pAuthPtr->ClientInfo.ClientPap.PapResendTimer,
                       PAP_REQ_TIMER,
                       pAuthPtr->ClientInfo.ClientPap.TimeoutTime);

        pAuthPtr->ClientInfo.ClientPap.TransmitCount++;
        pAuthPtr->ClientInfo.ClientPap.State = PAP_CLIENT_STATE_AUTHREQ;

    }
    else
    {
        PRINT_MEM_ALLOC_FAILURE;
    }
    return;
}
