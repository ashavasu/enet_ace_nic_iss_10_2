/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppemd5.c,v 1.7 2014/10/30 12:31:54 siva Exp $
 *
 * Description:This file contains the interface and internal 
 *             procedures of MD5 authentication mechanism in EAP
 *
 *******************************************************************/

#include "authcom.h"
#include "pppmd5.h"
#include "lcpdefs.h"
#include "lcpexts.h"
#include "gcpdefs.h"
#include "gsemdefs.h"
#include "genexts.h"
#include "genproto.h"

#include "globexts.h"
#include "pppcom.h"

/* #ifdef L2TP_WANTED */
#include "pppl2tp.h"
#include "l2tp.h"
/* #endif */
/*********************************************************************
*  Function Name    :    EAPMD5ClientInit
*  Description        :
*                This function initialises the ClientEAPMD5 
*                structure of EAP client.
*                            It sets the time out value and maximam 
*                            retransmission to their default values and 
*                            sets the client state to INITIAL .
*    Parameter(s)    :
*        pAuthPtr    -    Points to the authentication interface
*                                structure 
*    Return Values    :    VOID
*********************************************************************/
VOID
EAPMD5ClientInit (tAUTHIf * pAuthPtr)
{
    tEAPMD5ClientInfo  *pClientEAPMD5;

    pClientEAPMD5 =
        &pAuthPtr->ClientInfo.ClientEap.AuthTypeOptions.ClientEAPMD5;
    pClientEAPMD5->pEAPMD5ClientSecret = NULL;

    pClientEAPMD5->State = EAPMD5_CLIENT_STATE_INITIAL;

    return;
}

/*********************************************************************
*  Function Name    :    EAPMD5ServerInit
*  Description        :
*                This function initialises the ServerEAPMD5 
*                structure of EAP Server
*                            It sets the time out value and maximam 
*                            retransmission to their default values and 
*                            sets the server tate to INITIAL .
*    Parameter(s)    :
*        pAuthPtr    -    Points to the authentication 
*                                structure. 
*    Return Values    :    VOID
*********************************************************************/
VOID
EAPMD5ServerInit (tAUTHIf * pAuthPtr)
{
    tEAPMD5ServerInfo  *pServerEAPMD5;

    pServerEAPMD5 =
        &pAuthPtr->ServerInfo.ServerEap.AuthTypeOptions.ServerEAPMD5;

    pServerEAPMD5->pEAPMD5CurrentSecret = NULL;
    pServerEAPMD5->State = EAPMD5_SERVER_STATE_INITIAL;
    pServerEAPMD5->TimeoutTime = pAuthPtr->pIf->LinkInfo.TimeOutTime;
    pServerEAPMD5->MaxTransmits = pAuthPtr->pIf->LinkInfo.MaxReTransmits;

    pServerEAPMD5->ChallengeInterval = pAuthPtr->pIf->LinkInfo.ReChalTimeOut;
    return;
}

/*********************************************************************
*    Function Name    : EAPMD5ChalTimeOut
*    Description    : This function is called when the retransmission 
*                         counter expires after sending a EAPMD5 challenge 
*                         request packet. It calls the appropriate
*                      function to retransmit the packet.
*    Parameter(s)    :
*    pAuthPtr    -  Pointer to the authentication structure.
*    Return Values    :  VOID
*********************************************************************/
VOID
EAPMD5ChalTimeOut (VOID *pAuth)
{
    tEAPMD5ServerInfo  *pServerEAPMD5;
    tAUTHIf            *pAuthPtr;
    pAuthPtr = (tAUTHIf *) pAuth;
    pServerEAPMD5 =
        &pAuthPtr->ServerInfo.ServerEap.AuthTypeOptions.ServerEAPMD5;

    /*Check whether the time out occurs after sending a challenge/
     * re-challenge request packet  */

    if ((pServerEAPMD5->State != EAPMD5_SERVER_STATE_INITIAL_CHAL) &&
        (pServerEAPMD5->State != EAPMD5_SERVER_STATE_RECHALLENGE))
    {
        return;
    }

    if (pServerEAPMD5->ChallengeTransmits >= pServerEAPMD5->MaxTransmits)
    {

        PPP_TRC (ALL_FAILURE,
                 "\nPeer failed to respond to EAPMD5 ChallengeValue");

        pServerEAPMD5->State = EAPMD5_SERVER_STATE_BADAUTH;
        pAuthPtr->ServerInfo.ServerEap.State = EAP_SERVER_STATE_BADAUTH;
        LCPAuthProcessFailed (pAuthPtr->pIf);
        if (pTermGSEM != NULL)
        {
            GSEMRun (pTermGSEM, RXJ_MINUS);
        }
    }
    else
    {
        /* Re-send ChallengeValue */
        EAPMD5SendChallenge (pAuthPtr);
    }
    return;
}

/*********************************************************************
*    Function Name    : EAPMD5ReChalTimeOut
*    Description    : This procedure is used to send the Challenge 
*                         after every ReChallengeInterval time.
*    Parameter(s)    :
*        pAuthPtr- Pointer to the authentication structure.
*    Return Values    :    VOID
*********************************************************************/
VOID
EAPMD5ReChalTimeOut (VOID *pAuth)
{
    tEAPMD5ServerInfo  *pServerEAPMD5;
    tAUTHIf            *pAuthPtr;
    pAuthPtr = (tAUTHIf *) pAuth;

    pServerEAPMD5 =
        &pAuthPtr->ServerInfo.ServerEap.AuthTypeOptions.ServerEAPMD5;
    /* Check if time out has occured in the server open state */
    if (pServerEAPMD5->State == EAPMD5_SERVER_STATE_OPEN)
    {
        pServerEAPMD5->State = EAPMD5_SERVER_STATE_RECHALLENGE;
        pServerEAPMD5->pEAPMD5CurrentSecret = NULL;
        EAPMD5GenChallenge (pAuthPtr);
        EAPMD5SendChallenge (pAuthPtr);
    }
    return;
}

/*********************************************************************
*    Function Name    : EAPMD5RcvdChallenge
*    Description    : This function  is called by the EAPRcvdAuthRequest
*                         when the MD5 Challenge is received from the Peer. 
*                         It checks the Challenge, prepares the response 
*                         for the same and sends the response back to the Peer.
*    Parameter(s)    :
*        pAuthPtr    -    Pointer to authentication interface 
*        pInPacket    -    Pointer to  Input Packet String.
*        Length        -    Length of the Input Packet.
*        Identifier    -    Identifier received along with the 
*                                       packet and to be used in response
*    Return Values    :    VOID
*********************************************************************/
VOID
EAPMD5RcvdChallenge (tAUTHIf * pAuthPtr, t_MSG_DESC * pInpacket, UINT2 Length,
                     UINT1 Identifier)
{
    UINT1               RcvdChalValLen;
    UINT1               RcvdChalValue[MAX_CHALLENGE_LENGTH];
    UINT1               RcvdName[PPP_MAX_NAME_LENGTH];
    UINT1               au1Password[MAX_SECRET_SIZE];
    tMD5_CTX            MDContext;
    tSecret            *pClientSecret;

    t_MSG_DESC         *pOutBuf;

    tEAPMD5ClientInfo  *pClientEAPMD5;
    UINT2               ResPktLen;

    BZERO (au1Password, MAX_SECRET_SIZE);

    pClientEAPMD5 =
        &pAuthPtr->ClientInfo.ClientEap.AuthTypeOptions.ClientEAPMD5;
    /* Extract and validate the received challenge */
    BZERO (RcvdName, PPP_MAX_NAME_LENGTH);
    BZERO (RcvdChalValue, MAX_CHALLENGE_LENGTH);

    if (Length < 1)
    {
        DISCARD_PKT (pAuthPtr->pIf, "Pkt discarded due to Invalid length");
        return;
    }
    pAuthPtr->pIf->LLHCounters.InGoodOctets += Length;

    EXTRACT1BYTE (pInpacket, RcvdChalValLen);
    Length--;

    if (Length < RcvdChalValLen)
    {
        DISCARD_PKT (pAuthPtr->pIf, "Pkt discarded due to Invalid length");
        return;
    }
    EXTRACTSTR (pInpacket, RcvdChalValue, RcvdChalValLen);
    Length = (UINT2) (Length - RcvdChalValLen);

    EXTRACTSTR (pInpacket, RcvdName, Length);

    /* Get the secrect from secrets table */
    if ((pClientEAPMD5->pEAPMD5ClientSecret =
         AUTHGetSecret (pAuthPtr, RcvdName, Length, EAPMD5_PROTOCOL,
                        CLIENT)) == NULL)
    {
        PPP_TRC (ALL_FAILURE,
                 "\nUnable to send response due to insufficient secret info. ");
        return;
    }
    pClientSecret = pClientEAPMD5->pEAPMD5ClientSecret;

    pClientEAPMD5->ResponseId = Identifier;

    STRCPY (au1Password, pClientSecret->Secret);
    FsUtlDecryptPasswd ((CHR1 *) au1Password);

    /* Calculate the hash value using the MD5 algorithm */
    PppMD5Init (&MDContext);
    PppMD5Update (&MDContext, &pClientEAPMD5->ResponseId, 1);
    PppMD5Update (&MDContext, au1Password, pClientSecret->SecretLen);
    PppMD5Update (&MDContext, RcvdChalValue, RcvdChalValLen);
    PppMD5Final (&MDContext);
    MEMCPY (pClientEAPMD5->Response, MDContext.Digest, MD5_SIGNATURE_SIZE);

    pClientEAPMD5->ResponseLength = MD5_SIGNATURE_SIZE;

    pClientSecret->IdentityLen = (UINT1) STRLEN (pAuthPtr->pIf->au1HostName);

    /* Construct and Send the response to peer */
    ResPktLen =
        (UINT2) (CP_HDR_LEN + EAP_TYPE_FIELD_LEN + VALUE_SIZE_FIELD_LEN +
                 pClientEAPMD5->ResponseLength + pClientSecret->IdentityLen);

    if ((pOutBuf =
         (t_MSG_DESC *) (VOID *) ALLOCATE_BUFFER (PPP_INFO_OFFSET + ResPktLen,
                                                  PPP_INFO_OFFSET)) != NULL)
    {

        FILL_PKT_HEADER (pOutBuf, EAP_RESPONSE,
                         pClientEAPMD5->ResponseId, ResPktLen);
        APPEND1BYTE (pOutBuf, EAP_TYPE_MD5);
        APPEND1BYTE (pOutBuf, pClientEAPMD5->ResponseLength);
        APPENDSTR (pOutBuf, pClientEAPMD5->Response,
                   pClientEAPMD5->ResponseLength);

        APPENDSTR (pOutBuf, pAuthPtr->pIf->au1HostName,
                   pClientSecret->IdentityLen);

        PPPLLITxPkt (pAuthPtr->pIf, pOutBuf, Length, EAP_PROTOCOL);

        pClientEAPMD5->State = EAPMD5_CLIENT_STATE_RESPONSE;

        /*   In EAP, the response packets aren't retransmitted on timer
         *   expiry. So, no need to have a retranmission timer for response
         *   packets. (Refer Sec. 2.2.1 in RFC 2284)
         */
    }
    else
    {
        PRINT_MEM_ALLOC_FAILURE;
    }
    return;
}

/*********************************************************************
*    Function Name    :  EAPRcvdAuthResponse
*    Description    :  This function  is called by the EAPRcvdAuthResponse
*                          when the MD5 response is received from the Peer. 
*                          It checks the response and sends SUCCESS/FAILURE 
*                          based on the response.
*    Parameter(s)    :
*        pAuthPtr       -    Pointer to the authentication structure
*        pInPacket    -    Pointer to  Input Packet String.
*        Length        -    Length of the Input Packet
*        Identifier    -    Identifier received along with the 
*                                        packet and to be used in response.
*    Return Values    :    VOID
*********************************************************************/
VOID
EAPMD5RcvdResponse (tAUTHIf * pAuthPtr, t_MSG_DESC * pInpacket, UINT2 Length,
                    UINT1 Identifier)
{
    UINT1               RcvdMMD[MAX_CHALLENGE_LENGTH], RcvdMMDLen;
#ifdef RADIUS_WANTED
    UINT1               au1EapPkt[EAP_PKT_MAX_LEN];
#endif
    UINT1               UserName[MAX_SECRET_SIZE];
    UINT1               au1Password[MAX_SECRET_SIZE];
    UINT1               NameLen;
    UINT2               OldState;
#ifdef RADIUS_WANTED
    UINT2               u2EapPktLen;
#endif
    INT2                Code;
    tMD5_CTX            MDContext;
    tSecret            *pServerSecret;
    tEAPMD5ServerInfo  *pServerEAPMD5;

    BZERO (au1Password, MAX_SECRET_SIZE);
    BZERO (&UserName, MAX_SECRET_LENGTH + 1);
    BZERO (RcvdMMD, MAX_CHALLENGE_LENGTH);

    pServerEAPMD5 =
        &pAuthPtr->ServerInfo.ServerEap.AuthTypeOptions.ServerEAPMD5;
    /*  
       Check if identifier of last sent challenge matches with that of the 
       incoming response 
     */
    if (Identifier != pServerEAPMD5->ChallengeId)
    {
        DISCARD_PKT (pAuthPtr->pIf, "Invalid Id/Length in/of CHAP response.");
        return;
    }

    PPPStopTimer (&pServerEAPMD5->ChalTimer);
#ifdef RADIUS_WANTED
    if (pServerEAPMD5->State == EAPMD5_SERVER_STATE_PENDING)
    {
        PPP_TRC (CONTROL_PLANE, "Response already send to RADIUS Server");
        return;
    }
    if (pAuthPtr->ServerInfo.ServerEap.State == EAP_SERVER_STATE_PENDING)
    {
        PPP_TRC (CONTROL_PLANE, "Response already send to RADIUS Server");
        return;
    }
#endif

#ifdef L2TP_WANTED
    if (PPPIsL2TPTunnelModeLAC (pAuthPtr->pIf) == FALSE)
    {
#endif
        /*
           If we have received a duplicate or bogus Response, we have to send 
           the same answer (Success/Failure) as we did for the first Response 
           we saw.
         */

    if (pServerEAPMD5->State == EAPMD5_SERVER_STATE_OPEN)
    {
        EAPSendStatus (pAuthPtr, EAP_SUCCESS);
        return;
    }

    if (pServerEAPMD5->State == EAPMD5_SERVER_STATE_BADAUTH)
    {
        EAPSendStatus (pAuthPtr, EAP_FAILURE);
        return;
    }
#ifdef L2TP_WANTED
    }
#endif

    /* Extract and validate the incoming response packet */
    EXTRACT1BYTE (pInpacket, RcvdMMDLen);

    if (Length < RcvdMMDLen)
    {
        DISCARD_PKT (pAuthPtr->pIf, "Pkt discarded due to Invalid length");
        return;
    }
    pAuthPtr->pIf->LLHCounters.InGoodOctets += Length;

    EXTRACTSTR (pInpacket, RcvdMMD, RcvdMMDLen);

    NameLen = (UINT1) (Length - RcvdMMDLen - VALUE_SIZE_FIELD_LEN);

    EXTRACTSTR (pInpacket, UserName, NameLen);

#ifdef RADIUS_WANTED
    if (gu1aaaMethod == AAA_RADIUS)
    {
        PPP_TRC (CONTROL_PLANE, "Authenticating using RADIUS Server...");
        GETSTR (pInpacket, (UINT1 *) &u2EapPktLen, 4, 2);
        u2EapPktLen = OSIX_NTOHS (u2EapPktLen);
        GETSTR (pInpacket, au1EapPkt, 2, u2EapPktLen);

        Code =
            (INT2) (PppRadEapAuthenticate
                    (UserName, NameLen, au1EapPkt, u2EapPktLen,
                     pAuthPtr->pIf->LinkInfo.IfIndex,
                     pAuthPtr->pIf->RadInfo.EapState,
                     pAuthPtr->pIf->RadInfo.u1EapStateLen));
        pAuthPtr->pIf->RadInfo.RadReqId = (INT1) Code;

        if (Code != NOT_OK)
        {
            pAuthPtr->ServerInfo.ServerEap.State = EAP_SERVER_STATE_PENDING;
            pAuthPtr->pIf->RadInfo.PppAuthId = Identifier;
            pAuthPtr->pIf->RadInfo.Username =
                (UINT1 *) CALLOC (PPP_MAX_NAME_LENGTH, 1);
            MEMCPY (pAuthPtr->pIf->RadInfo.Username, UserName, NameLen);
            return;
        }

        PPP_TRC (ALL_FAILURE,
                 "Err in contacting RAD Server!.Locally authenticating");
    }

#endif /* RADIUS_WANTED */

    /*
     * Get secret for authenticating them with us,
     * do the hash ourselves, and compare the result.
     */
    Code = EAP_FAILURE;

    pServerSecret =
        AUTHGetSecret (pAuthPtr, UserName, NameLen, EAPMD5_PROTOCOL, SERVER);

    if (pServerSecret == NULL)
    {
        PPP_TRC (ALL_FAILURE,
                 "\nUnable 111 to send reply due to insufficient secret info. ");
        return;
    }
    else
    {
        if (RcvdMMDLen == MD5_SIGNATURE_SIZE)
        {
            STRCPY (au1Password, pServerSecret->Secret);
            FsUtlDecryptPasswd ((CHR1 *) au1Password);

            PppMD5Init (&MDContext);
            PppMD5Update (&MDContext, &pServerEAPMD5->ChallengeId, 1);
            PppMD5Update (&MDContext, au1Password, pServerSecret->SecretLen);
            PppMD5Update (&MDContext, pServerEAPMD5->ChallengeValue,
                          pServerEAPMD5->ChallengeValueLength);
            PppMD5Final (&MDContext);

            /* Compare local and remote MDs and send the appropriate status */
            if (MEMCMP (MDContext.Digest, RcvdMMD, MD5_SIGNATURE_SIZE) == 0)
            {
                Code = EAP_SUCCESS;
            }
        }
    }

    EAPSendStatus (pAuthPtr, (UINT1) (Code));

    if (Code == EAP_SUCCESS)
    {
        pAuthPtr->ServerInfo.ServerEap.State = EAP_SERVER_STATE_OPEN;
        OldState = pServerEAPMD5->State;
        pServerEAPMD5->State = EAPMD5_SERVER_STATE_OPEN;
        if (OldState == EAPMD5_SERVER_STATE_INITIAL_CHAL)
        {
            LCPUpdateAuthStatus (pAuthPtr->pIf, EAP_PEER_TRUE);
        }
        if (pServerEAPMD5->ChallengeInterval != 0)
        {
            pServerEAPMD5->ReChalTimer.Param1 = PTR_TO_U4 (pAuthPtr);
            PPPStartTimer (&pServerEAPMD5->ReChalTimer, EAP_MD5_RECHAL_TIMER,
                           pServerEAPMD5->ChallengeInterval);
        }
    }
    else
    {
        PPP_TRC (ALL_FAILURE, "\nEAP Authentication failed....");
        pAuthPtr->ServerInfo.ServerEap.State = EAP_SERVER_STATE_BADAUTH;
        pServerEAPMD5->State = EAPMD5_SERVER_STATE_BADAUTH;
        LCPAuthProcessFailed (pAuthPtr->pIf);
        if (pTermGSEM != NULL)
        {
            GSEMRun (pTermGSEM, RXJ_MINUS);
        }

    }

    return;
}

/*********************************************************************
*  Function Name : EAPMD5GenChallenge
*  Description   :
*           This function is called to generate the pseudo-random 
*                  Challenge string that is to be sent in the Challenge 
*                  Request packet.
*  Parameter(s)  :
*           pAuthPtr  -  Pointer to the authentication structure
*  Return Values : VOID
*********************************************************************/
VOID
EAPMD5GenChallenge (tAUTHIf * pAuthPtr)
{
    UINT2               Index;

    tEAPMD5ServerInfo  *pServerEAPMD5;

    pServerEAPMD5 =
        &pAuthPtr->ServerInfo.ServerEap.AuthTypeOptions.ServerEAPMD5;
    /*pick a random ChallengeValue length between MIN_CHALLENGE_LENGTH and 
       MAX_CHALLENGE_LENGTH */

    pServerEAPMD5->ChallengeValueLength = GET_RANDOM_CHALLENGE_LENGTH ();

    pServerEAPMD5->ChallengeId = pAuthPtr->ServerInfo.ServerEap.CurrentId++;
    pServerEAPMD5->ChallengeTransmits = 0;

    /* generate a random string */
    for (Index = 0; Index < pServerEAPMD5->ChallengeValueLength; Index++)
        pServerEAPMD5->ChallengeValue[Index] =
            (UINT1) (GET_RANDOM_CHALLENGE_VALUE ());

    return;
}

/*********************************************************************
*  Function Name : EAPMD5SendChallenge
*  Description   :
*                       This function is used to transmit the Challenge to the
*                       peer. It s also used by the EAPMD5ChalTimeOut() 
*                       to resend the Challenge to the peer.
*  Parameter(s)  :
*       pAuthPtr -  pointer to authentication structure
*  Return Values : VOID
*********************************************************************/
VOID
EAPMD5SendChallenge (tAUTHIf * pAuthPtr)
{
    UINT2               Length;
    t_MSG_DESC         *pOutBuf;
    UINT2               u2IdLen;

    tEAPMD5ServerInfo  *pServerEAPMD5;

    pServerEAPMD5 =
        &pAuthPtr->ServerInfo.ServerEap.AuthTypeOptions.ServerEAPMD5;

    pServerEAPMD5->pEAPMD5CurrentSecret = AUTHGetSecretName (pAuthPtr,
                                                             EAPMD5_PROTOCOL,
                                                             SERVER);

    if (pServerEAPMD5->pEAPMD5CurrentSecret == NULL)
    {
        LCPAuthProcessFailed (pAuthPtr->pIf);
        if (pTermGSEM != NULL)
        {
            GSEMRun (pTermGSEM, RXJ_MINUS);
        }
        return;
    }

    u2IdLen = (UINT2) STRLEN (pAuthPtr->pIf->au1HostName);

    /* Code + Identifier + Length + Type + challenge length + challenge
     * value + Name */

    Length = (UINT2) (CP_HDR_LEN + EAP_TYPE_FIELD_LEN + VALUE_SIZE_FIELD_LEN +
                      pServerEAPMD5->ChallengeValueLength + u2IdLen);

    if ((pOutBuf =
         (t_MSG_DESC *) (VOID *) ALLOCATE_BUFFER (PPP_INFO_OFFSET + Length,
                                                  PPP_INFO_OFFSET)) != NULL)
    {

        FILL_PKT_HEADER (pOutBuf, EAP_REQUEST, pServerEAPMD5->ChallengeId,
                         Length);
        APPEND1BYTE (pOutBuf, EAP_TYPE_MD5);

        APPEND1BYTE (pOutBuf, pServerEAPMD5->ChallengeValueLength);
        APPENDSTR (pOutBuf, pServerEAPMD5->ChallengeValue,
                   pServerEAPMD5->ChallengeValueLength);

        APPENDSTR (pOutBuf, pAuthPtr->pIf->au1HostName, u2IdLen);

        PPPLLITxPkt (pAuthPtr->pIf, pOutBuf, Length, EAP_PROTOCOL);

        pServerEAPMD5->ChalTimer.Param1 = PTR_TO_U4 (pAuthPtr);

        PPPStartTimer (&pServerEAPMD5->ChalTimer, EAP_MD5_CHAL_TIMER,
                       pServerEAPMD5->TimeoutTime);

        /* Increment number of retransmits */
        ++pServerEAPMD5->ChallengeTransmits;

    }
    else
    {
        PRINT_MEM_ALLOC_FAILURE;
    }
    return;
}
