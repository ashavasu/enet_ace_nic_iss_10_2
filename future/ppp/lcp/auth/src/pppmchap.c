/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppmchap.c,v 1.7 2014/03/11 14:02:51 siva Exp $
 *
 * Description:This file contains interface and internal 
 *             procedures of MS CHAP Module
 *
 *******************************************************************/

/* Makefile changes - <implicit declaration atoi,ctype>*/
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "authcom.h"
#include "pppmd4.h"
#include "lcpdefs.h"
#include "lcpexts.h"
#include "gcpdefs.h"
#include "gsemdefs.h"
#include "desecb.h"

#include "globexts.h"
#include "pppcom.h"

/****************************************************************************
*                    PROTOTYPES FOR MSCHAP INTERNAL ROUTINES                    *
*****************************************************************************/

VOID                DesHash (UINT1 *pPasswd, UINT1 *pPwdHash);

VOID                MSChapNewPasswdEncryptedWithOldNTPasswdHash (UINT2
                                                                 *pNewPasswd,
                                                                 UINT2
                                                                 *pOldPasswd,
                                                                 UINT1 NewLen,
                                                                 UINT1 OldLen,
                                                                 tPWBlock *
                                                                 pEncrytedPwBlock);

VOID                MSChapEncryptPwBlockWithPasswdHash (UINT2 *pNewPasswd,
                                                        UINT1 PasswdLen,
                                                        UINT1 *pPasswdHash,
                                                        tPWBlock *
                                                        pEncryptedPwBlock);

VOID                MSChapOldNtPasswdHashEncryptedWithNewNtPasswdHash (UINT2
                                                                       *pNewPasswd,
                                                                       UINT2
                                                                       *pOldPasswd,
                                                                       UINT1
                                                                       NewLen,
                                                                       UINT1
                                                                       OldLen,
                                                                       UINT1
                                                                       *pEncryptedPasswdHash);

VOID                MSChapNtPasswdHashEncryptedWithBlock (UINT1 *pPasswdHash,
                                                          UINT1 *pBlock,
                                                          UINT1 *pCypher);

VOID                MSChapProcessAuthFailurePacket (tAUTHIf * pAuthPtr,
                                                    t_MSG_DESC * pInpacket,
                                                    UINT1 Identitier,
                                                    UINT2 Length);

VOID                MSChapPrepareAndSendResponse (tAUTHIf * pAuthPtr,
                                                  UINT1 *pChallenge,
                                                  UINT1 Identifier);

VOID                MSChapPrepareAndSendChangePasswdPacket (tAUTHIf * pAuthPtr,
                                                            UINT1 *pChallenge,
                                                            UINT1 Identifier);

tPWBlock           *RC4Encrypt (tPWBlock * pClearPwBlock, UINT2 BlockLen,
                                UINT1 *pPasswdHash, UINT2 PasswdHashLen);
/*
tPWBlock *RC4Encrypt (tPWBlock *pClearPwBlock, UINT2 BlockLen, UINT1 *pPasswdHash, UINT2 PasswdHashLen);
*/
VOID                MSChapChallengeResponse (UINT1 *pChallenge,
                                             UINT1 *pPasswordHash,
                                             UINT1 *pNtChalResponse);

/****************************************************************************
*                    MSCHAP INTERFACE ROUTINES                                *
*****************************************************************************/

/*********************************************************************
*    Function Name    :    MSCHAPAuthPeer
*    Description        :
*                    This function is called by the PPP Authentication Module 
*    for authenticating the Peer. The MS-CHAP server state is set to PENDING if 
*    it is in the INITIAL or PENDING state. Otherwise, a challenge is generated 
*    and sent to the peer. This function is called only if the authentication 
*    protocol negotiated is CHAP.
*    Parameter(s)    :
*        pAuthPtr    -    Points to the interface structure for which the MS-CHAP
*                        authentication  need to be done.
*    Return Values    :    VOID
*********************************************************************/
VOID
MSCHAPAuthPeer (tAUTHIf * pAuthPtr)
{

    if (pAuthPtr->ServerInfo.ServerChap.State == CHAP_SERVER_STATE_INITIAL
        || pAuthPtr->ServerInfo.ServerChap.State == CHAP_SERVER_STATE_PENDING)
    {
        pAuthPtr->ServerInfo.ServerChap.State = CHAP_SERVER_STATE_PENDING;
    }
    else
    {
        MSCHAPGenChallenge (pAuthPtr);
        MSCHAPSendChallenge (pAuthPtr);
        pAuthPtr->ServerInfo.ServerChap.State = CHAP_SERVER_STATE_INITIAL_CHAL;
    }
    return;
}

/*********************************************************************
*    Function Name    :    MSCHAPRcvdChallenge
*    Description        :
*                    This function  is called by the CHAPInput when the
*    Authentication Challenge is received from the Peer and the CHAP Algorithm 
*    is 0x80. It checks the Challenge, prepares the response for the same and 
*    sends the response back to the Peer.
*    Parameter(s)    :
*        pAuthPtr    -    Pointer to authentication interface which contains 
*                        all the MS-CHAP information.
*        pInPacket    -    Pointer to  Input Packet String.
*        Length        -    Length of the Input Packet.
*        Identifier    -    Identifier received along with the packet and to
*                        be used in response
*    Return Values    :    VOID
*********************************************************************/
VOID
MSCHAPRcvdChallenge (tAUTHIf * pAuthPtr, t_MSG_DESC * pInpacket, UINT2 Length,
                     UINT1 Identifier)
{
    UINT1               RcvdChalValLen;
    UINT1               Index;
    UINT2               u2Count = 0;
    UINT1               PasswdLen;
    UINT1               au1Password[MAX_SECRET_SIZE];
    /* UINT1           RcvdName[Length - MSCHAP_CHALLENGE_LENGTH - 1]; */
    UINT1               RcvdChalValue[MSCHAP_CHALLENGE_LENGTH];
    UINT1               PasswordHash[MSCHAP_PASSWD_HASH_LENGTH];
    UINT1               ChalResponse[MSCHAP_NT_CHAL_RESP_LENGTH];
#ifdef LM_PASSWD_HASH
    UINT1              *Password;
#else
    UINT2              *Password;
#endif
    tSecret            *pClientSecret;

    BZERO (au1Password, MAX_SECRET_SIZE);
    BZERO (RcvdChalValue, MSCHAP_CHALLENGE_LENGTH);
    /* BZERO(RcvdName, Length - MSCHAP_CHALLENGE_LENGTH -1); */
    BZERO (PasswordHash, MSCHAP_PASSWD_HASH_LENGTH);
    BZERO (ChalResponse, MSCHAP_NT_CHAL_RESP_LENGTH);

    /* Extract and validate the received challenge */
    PPP_TRC (CONTROL_PLANE, "MSCHAP_CHALLENGE_REQ \t IN :");
    if (Length < 1)
    {
        DISCARD_PKT (pAuthPtr->pIf, "Pkt discarded due to Invalid length");
        return;
    }
    EXTRACT1BYTE (pInpacket, RcvdChalValLen);
    Length--;

    if (Length < RcvdChalValLen)
    {
        DISCARD_PKT (pAuthPtr->pIf, "Pkt discarded due to Invalid length");
        return;
    }
    pAuthPtr->pIf->LLHCounters.InGoodOctets += Length;

    EXTRACTSTR (pInpacket, RcvdChalValue, RcvdChalValLen);
    Length = (UINT2) (Length - RcvdChalValLen);

    if (pAuthPtr->ClientInfo.ClientChap.pChallenge == NULL)
    {
        if ((pAuthPtr->ClientInfo.ClientChap.pChallenge =
             ALLOC_STR (MSCHAP_CHALLENGE_LENGTH)) == NULL)
        {
            PRINT_MEM_ALLOC_FAILURE;
            return;
        }
        MEMCPY (pAuthPtr->ClientInfo.ClientChap.pChallenge, RcvdChalValue,
                MSCHAP_CHALLENGE_LENGTH);
    }

    /* For MSCHAP the Name will not be present in the recd packet */

    /* if ( Length != 0)  */
    /* EXTRACTSTR(pInpacket, RcvdName, Length); */

    /* What to do if a name is recd in the Challenge Packet ????????? */
    /* Microsoft Authenticators do not normally send the Name field in the
       Challenge Packet ....... So Ignoring the Name. 
     */

    /* Get the secrect from secrets table */

    if ((pAuthPtr->ClientInfo.ClientChap.pCHAPClientSecret =
         MSCHAPAUTHGetSecret (pAuthPtr, NULL, MSCHAP_PROTOCOL, CLIENT)) == NULL)
    {
        PPP_TRC (ALL_FAILURE,
                 "Unable to send response due to insufficient secret info. ");
        return;
    }
    pClientSecret = pAuthPtr->ClientInfo.ClientChap.pCHAPClientSecret;

    /* Cancel response send timeout if necessary */
    if (pAuthPtr->ClientInfo.ClientChap.State == CHAP_CLIENT_STATE_RESPONSE)
    {
        PPPStopTimer (&pAuthPtr->ClientInfo.ClientChap.ChapRespTimer);
    }
    pAuthPtr->ClientInfo.ClientChap.ResponseId = Identifier;
    pAuthPtr->ClientInfo.ClientChap.ResponseTransmits = 0;

    /* Generate the MSCHAP Response */

#ifdef LM_PASSWD_HASH
    PasswdLen = pClientSecret->SecretLen * 2;
    if ((Password = (UINT1 *) ALLOC_STR (PasswdLen)) == NULL)
    {
        PRINT_MEM_ALLOC_FAILURE;
        return;
    }
#else
    PasswdLen = (UINT1) (pClientSecret->SecretLen * 2);
    if ((Password = (UINT2 *) (VOID *) ALLOC_STR (PasswdLen)) == NULL)
    {
        PRINT_MEM_ALLOC_FAILURE;
        return;
    }
#endif

    STRCPY (au1Password, pClientSecret->Secret);
    FsUtlDecryptPasswd ((CHR1 *) au1Password);

    for (Index = 0; Index < pClientSecret->SecretLen; Index++)
    {
#ifdef LM_PASSWD_HASH
        Password[Index] = au1Password[Index];
#else
        Password[u2Count++] = au1Password[Index];
        Password[u2Count++] = 0x00;
#endif
    }

#ifdef LM_PASSWD_HASH
    MSChapLmPasswordHash (Password, PasswordHash, pClientSecret->SecretLen);
#else
    MSChapNtPasswordHash (Password, PasswordHash, pClientSecret->SecretLen);
#endif

    MSChapChallengeResponse (RcvdChalValue, PasswordHash, ChalResponse);

#ifdef LM_PASSWD_HASH
    MEMCPY (pAuthPtr->ClientInfo.ClientChap.pResponse, ChalResponse,
            MSCHAP_LM_CHAL_RESP_LENGTH);
    pAuthPtr->ClientInfo.ClientChap.pResponse[MSCHAP_RESPONSE_LENGTH - 1] = 0;
#else
    MEMCPY (pAuthPtr->ClientInfo.ClientChap.pResponse +
            MSCHAP_LM_CHAL_RESP_LENGTH, ChalResponse,
            MSCHAP_NT_CHAL_RESP_LENGTH);
    pAuthPtr->ClientInfo.ClientChap.pResponse[MSCHAP_RESPONSE_LENGTH - 1] = 1;
#endif
/* commented by rams for the bug reported by Lucent -- 99060001 */
    pAuthPtr->ClientInfo.ClientChap.ResponseLength = MSCHAP_RESPONSE_LENGTH;

    /* Send the response to peer */
    CHAPSendResponse (pAuthPtr);

    FREE_STR (Password);
    return;
}

/*********************************************************************
*    Function Name    :    MSCHAPRcvdAuthResponse
*    Description        :
*                    This function  is called by the CHAPInput when the 
*    MSCHAP response is received from the Peer. It checks the response and 
*    sends TRUE/FALSE  based on the response.
*    Parameter(s)    :
*        pAuthPtr    -    Pointer to Auth Interface structure which contains all 
*                        the CHAP information.
*        pInPacket    -    Pointer to  Input Packet String.
*        Length        -    Length of the Input Packet
*        Identifier    -    Identifier received along with the packet and
*                        to be used in response.
*    Return Values    :    VOID
*********************************************************************/
VOID
MSCHAPRcvdAuthResponse (tAUTHIf * pAuthPtr, t_MSG_DESC * pInpacket,
                        UINT2 Length, UINT1 Identifier)
{
    UINT1               OldState,
        RcvdLength,
        RespCode,
        NameLength,
        UserName[MSCHAP_MAX_NAME_LENGTH + 1],
        RcvdLMChalResponse[MSCHAP_LM_CHAL_RESP_LENGTH + 1],
        RcvdNTChalResponse[MSCHAP_NT_CHAL_RESP_LENGTH + 1];

#ifdef LM_PASSWD_HASH
    UINT1               LMChalRespVal[MSCHAP_LM_CHAL_RESP_LENGTH];
#else
    UINT1               NTChalRespVal[MSCHAP_NT_CHAL_RESP_LENGTH];
#endif

    INT2                Code;

    tSecret            *pServerSecret;

    PPP_TRC (CONTROL_PLANE, "MSCHAP_AUTH_RESPONSE \t IN :");
#ifdef RADIUS_WANTED
    BZERO (RcvdLMChalResponse, MSCHAP_LM_CHAL_RESP_LENGTH + 1);
    BZERO (RcvdNTChalResponse, MSCHAP_NT_CHAL_RESP_LENGTH + 1);
    BZERO (UserName, MSCHAP_MAX_NAME_LENGTH + 1);
#endif /* RADIUS_WANTED */

    /*  
       Check if identifier of last sent challenge matches with that of the 
       incoming response 
     */
    if ((Identifier != pAuthPtr->ServerInfo.ServerChap.ChallengeId)
        || (Length < MIN_CHAP_PKT_LEN))
    {
        DISCARD_PKT (pAuthPtr->pIf, "Invalid Id/Length in/of MSCHAP response.");
        return;
    }
    if (pAuthPtr->ServerInfo.ServerChap.State == CHAP_SERVER_STATE_OPEN)
    {
        MSCHAPSendStatus (pAuthPtr, CHAP_TRUE, (const UINT1 *) NULL);
        return;
    }
    if (pAuthPtr->ServerInfo.ServerChap.State == CHAP_SERVER_STATE_BADAUTH)
    {
        MSCHAPSendStatus (pAuthPtr, CHAP_FALSE, (const UINT1 *) "691");
        return;
    }

#ifdef RADIUS_WANTED
    if (pAuthPtr->ServerInfo.ServerChap.State == CHAP_SERVER_STATE_PENDING)
    {
        PPP_TRC (CONTROL_PLANE, "Response already send to RADIUS Server");
        return;
    }
#endif

    /* Extract and validate the incoming response packet */
    EXTRACT1BYTE (pInpacket, RcvdLength);

    if (Length < RcvdLength)
    {
        DISCARD_PKT (pAuthPtr->pIf, "Pkt discarded due to Invalid length");
        return;
    }

    pAuthPtr->pIf->LLHCounters.InGoodOctets += Length;
    PPPStopTimer (&pAuthPtr->ServerInfo.ServerChap.ChapChalTimer);

    EXTRACTSTR (pInpacket, RcvdLMChalResponse, MSCHAP_LM_CHAL_RESP_LENGTH);
    EXTRACTSTR (pInpacket, RcvdNTChalResponse, MSCHAP_NT_CHAL_RESP_LENGTH);
    EXTRACT1BYTE (pInpacket, RespCode);
    NameLength = (UINT1) (Length - RcvdLength - 1);
    EXTRACTSTR (pInpacket, UserName, NameLength);

#ifdef RADIUS_WANTED
    if (gu1aaaMethod == AAA_RADIUS)
    {
        PPP_TRC (CONTROL_PLANE, "Authenticating using RADIUS Server...");

        Code =
            (INT2) (PppRadMSCHAPAuthenticate
                    (UserName, NameLength, Identifier,
                     pAuthPtr->ServerInfo.ServerChap.ChallengeValue,
                     pAuthPtr->ServerInfo.ServerChap.ChallengeValueLength,
                     RcvdLMChalResponse, RcvdNTChalResponse,
                     pAuthPtr->pIf->LinkInfo.IfIndex));

        pAuthPtr->pIf->RadInfo.RadReqId = (INT1) Code;

        if (Code != NOT_OK)
        {
            pAuthPtr->pIf->RadInfo.ChapState =
                pAuthPtr->ServerInfo.ServerChap.State;
            pAuthPtr->ServerInfo.ServerChap.State = CHAP_SERVER_STATE_PENDING;
            pAuthPtr->pIf->RadInfo.PppAuthId = Identifier;
            if ((pAuthPtr->pIf->RadInfo.Username =
                 (UINT1 *) CALLOC (PPP_MAX_NAME_LENGTH, 1)) == NULL)
            {
                PPP_TRC (ALL_FAILURE, "Memory Alloc For UserName Failed");
                return;
            }
            MEMCPY (pAuthPtr->pIf->RadInfo.Username, UserName, NameLength);
            return;
        }

        PPP_TRC (ALL_FAILURE,
                 "Err in contacting RAD Server!.Locally authenticating");
    }                            /* == AAA_RADIUS */

#endif /* RADIUS_WANTED */

    Code = CHAP_FALSE;

    if ((pServerSecret =
         (tSecret *) MSCHAPAUTHGetSecret (pAuthPtr, UserName, MSCHAP_PROTOCOL,
                                          SERVER)) == NULL)
    {
/* Makefile changes - <too few aruguments> */
        PPP_TRC1 (ALL_FAILURE,
                  "Authentication Failed : User %s does not have login on this server",
                  UserName);
        MSCHAPSendStatus (pAuthPtr, (UINT1) (Code), (const UINT1 *) "691");
        /* Authentication retry is not allowd. Once authentication is
         * failed, the LCP connection is brought down */
        pAuthPtr->ServerInfo.ServerChap.State = CHAP_SERVER_STATE_BADAUTH;
        LCPAuthProcessFailed (pAuthPtr->pIf);
        return;
    }
    else
    {

#ifdef LM_PASSWD_HASH
        MSChapChallengeResponse (pAuthPtr->ServerInfo.ServerChap.ChallengeValue,
                                 pServerSecret->Secret, LMChalRespVal);
        if ((RespCode == 0)
            &&
            (MEMCMP
             (LMChalRespVal, RcvdLMChalResponse,
              MSCHAP_LM_CHAL_RESP_LENGTH) == 0))
            Code = CHAP_TRUE;
#else
        MSChapChallengeResponse (pAuthPtr->ServerInfo.ServerChap.ChallengeValue,
                                 pServerSecret->Secret, NTChalRespVal);
        if ((RespCode == 1)
            &&
            (MEMCMP
             (NTChalRespVal, RcvdNTChalResponse,
              MSCHAP_NT_CHAL_RESP_LENGTH) == 0))
            Code = CHAP_TRUE;
#endif
    }

    if (Code == CHAP_TRUE)
        MSCHAPSendStatus (pAuthPtr, (UINT1) (Code), (const UINT1 *) NULL);
    else
    {
        PPP_TRC (ALL_FAILURE, "Authentication Failed : Password Mismatch");
        MSCHAPSendStatus (pAuthPtr, (UINT1) (Code), (const UINT1 *) "691");
    }

    if (Code == CHAP_TRUE)
    {
        OldState = pAuthPtr->ServerInfo.ServerChap.State;
        pAuthPtr->ServerInfo.ServerChap.State = CHAP_SERVER_STATE_OPEN;
        if (OldState == CHAP_SERVER_STATE_INITIAL_CHAL)
        {
            LCPUpdateAuthStatus (pAuthPtr->pIf, MSCHAP_PEER_TRUE);
        }
        if (pAuthPtr->ServerInfo.ServerChap.ChallengeInterval != 0)
        {
            pAuthPtr->ServerInfo.ServerChap.ChapReChalTimer.Param1 =
                PTR_TO_U4 (pAuthPtr);
            PPPStartTimer (&pAuthPtr->ServerInfo.ServerChap.ChapReChalTimer,
                           CHAP_RECHAL_TIMER,
                           pAuthPtr->ServerInfo.ServerChap.ChallengeInterval);
        }
        pAuthPtr->ServerInfo.ServerChap.pCHAPCurrentSecret =
            MSCHAPAUTHGetSecret (pAuthPtr, UserName, MSCHAP_PROTOCOL, SERVER);
    }
    else
    {
        /* Authentication retry is not allowd. Once authentication is
         * failed, the LCP connection is brought down */
        pAuthPtr->ServerInfo.ServerChap.State = CHAP_SERVER_STATE_BADAUTH;
        LCPAuthProcessFailed (pAuthPtr->pIf);
    }
    return;
}

/*********************************************************************
*  Function Name : MSCHAPRcvdAuthSuccess
*  Description   :
*                       This function  is called by  CHAPInput when the
*  Authentication Success Reply is received from the peer. This function
*  indicates the  LCP that authentication if success, if there is no other
*  authentication pending.
*  Parameter(s)    :
*        pAuthPtr  -  pointer to Auth Info structure which contains
*                                    all the MS-CHAP information.
*        pInPacket -  pointer to  Input Packet String.
*        Length    -  Length of the Input Packet
*        Identifier-  Identifier received along with the packet and to
*                                   be used in response
*  Return Values : VOID
*********************************************************************/
VOID
MSCHAPRcvdAuthSuccess (tAUTHIf * pAuthPtr, t_MSG_DESC * pInpacket, UINT2 Length,
                       UINT1 Identifier)
{
    /* Makefile Changes - <unused parameter> */

    PPP_UNUSED (pInpacket);
    PPP_TRC (CONTROL_PLANE, "MSCHAP_AUTH_SUCCESS \t IN :");
    if (Identifier != pAuthPtr->ClientInfo.ClientChap.ResponseId)
    {
        DISCARD_PKT (pAuthPtr->pIf, "Invalid Id in MS-CHAP response.");
        return;
    }

    pAuthPtr->pIf->LLHCounters.InGoodOctets += Length;

    if ((pAuthPtr->ClientInfo.ClientChap.State == CHAP_CLIENT_STATE_OPEN)
        || (pAuthPtr->ClientInfo.ClientChap.State !=
            CHAP_CLIENT_STATE_RESPONSE))
        return;

    PPPStopTimer (&pAuthPtr->ClientInfo.ClientChap.ChapRespTimer);

    pAuthPtr->ClientInfo.ClientChap.State = CHAP_CLIENT_STATE_OPEN;

    LCPUpdateAuthStatus (pAuthPtr->pIf, MSCHAP_WITH_PEER_TRUE);

    return;
}

/*********************************************************************
*  Function Name : MSCHAPRcvdAuthFailure
*  Description   :
*                       This function  is called by CHAPInput when there is an
*   authentication failure packet sent by peer. It  validates the  input packet
*   and  informs LCP that the authentication has failed.
*  Parameter(s)  :
*           pAuthPtr  -  pointer to Auth Info structure which contains all
*                                    the MSCHAP information.
*           pInPacket -  pointer to  Input Packet String.
*           Length    -  Length of the Input Packet
*           Identifier-  Identifier received along with the packet and to
*                                    be used in response
*  Return Values : VOID
*********************************************************************/
VOID
MSCHAPRcvdAuthFailure (tAUTHIf * pAuthPtr, t_MSG_DESC * pInpacket, UINT2 Length,
                       UINT1 Identifier)
{

    PPP_TRC (CONTROL_PLANE, "MSCHAP_AUTH_FAILURE \t IN :");
    if (Identifier != pAuthPtr->ClientInfo.ClientChap.ResponseId)
    {
        DISCARD_PKT (pAuthPtr->pIf, "Invalid Id in MS-CHAP Failure Packet.");
        return;
    }

    pAuthPtr->pIf->LLHCounters.InGoodOctets += Length;

    if (pAuthPtr->ClientInfo.ClientChap.State != CHAP_CLIENT_STATE_RESPONSE)
        return;
    pAuthPtr->ClientInfo.ClientChap.State = CHAP_CLIENT_STATE_BADAUTH;
    PPPStopTimer (&pAuthPtr->ClientInfo.ClientChap.ChapRespTimer);

    PPP_TRC (ALL_FAILURE, "MSCHAP authentication failed");

    MSChapProcessAuthFailurePacket (pAuthPtr, pInpacket, Identifier, Length);
    return;
}

/*********************************************************************
*  Function Name : MSCHAPGenChallenge
*  Description   :
*                   This function is used by the  MSCHAPAuthPeer()
*   function to generate the pseudo-random Challenge string that is to be sent
*   in the Challenge Request packet.
*  Parameter(s)  :
*           pAuthPtr  -  pointer to CHAPState structure which contains
*                                    all the CHAP information.
*  Return Values : VOID
*********************************************************************/
VOID
MSCHAPGenChallenge (tAUTHIf * pAuthPtr)
{

    /*pick a random ChallengeValue length of 8 bytes */

    pAuthPtr->ServerInfo.ServerChap.ChallengeValueLength =
        MSCHAP_CHALLENGE_LENGTH;
    pAuthPtr->ServerInfo.ServerChap.ChallengeId =
        pAuthPtr->ServerInfo.ServerChap.CurrentId++;
    pAuthPtr->ServerInfo.ServerChap.ChallengeTransmits = 0;

    PPPGetRandom (pAuthPtr->ServerInfo.ServerChap.ChallengeValue,
                  (UINT4) pAuthPtr->ServerInfo.ServerChap.ChallengeValueLength);
    return;
}

/*********************************************************************
*  Function Name : MSCHAPSendChallenge
*  Description   :
*                       This function is used to transmitt the Challenge 
*  to the and is used by the MSCHAPAuthPeer() function, after calling 
*  MSCHAPGenChallenge(). It is also used by the CHAPChalTimeOut() to 
*  resend the Challenge to the peer.
*  Parameter(s)  :
*       pAuthPtr -  pointer to AuthIf structure which contains
*                   all the MS-CHAP information.
*  Return Values : VOID
*********************************************************************/
VOID
MSCHAPSendChallenge (tAUTHIf * pAuthPtr)
{
    UINT2               Length;
    t_MSG_DESC         *pOutBuf;
    /* Makefile Changes - <unused variable pSecret> */

    /*
       Length is calculated on the assumption that Name will not be send 
       in the challenge packet
     */

    Length =
        (UINT2) (pAuthPtr->ServerInfo.ServerChap.ChallengeValueLength +
                 VALUE_SIZE_FIELD_LEN + CP_HDR_LEN);

    if ((pOutBuf =
         (t_MSG_DESC *) (VOID *) ALLOCATE_BUFFER (PPP_INFO_OFFSET + Length,
                                                  PPP_INFO_OFFSET)) != NULL)
    {

        FILL_PKT_HEADER (pOutBuf, CHAP_CHALLENGE_REQ,
                         pAuthPtr->ServerInfo.ServerChap.ChallengeId, Length);
        APPEND1BYTE (pOutBuf,
                     pAuthPtr->ServerInfo.ServerChap.ChallengeValueLength);
        APPENDSTR (pOutBuf, pAuthPtr->ServerInfo.ServerChap.ChallengeValue,
                   pAuthPtr->ServerInfo.ServerChap.ChallengeValueLength);

        PPPLLITxPkt (pAuthPtr->pIf, pOutBuf, Length, CHAP_PROTOCOL);

        pAuthPtr->ServerInfo.ServerChap.ChapChalTimer.Param1 =
            PTR_TO_U4 (pAuthPtr);
        PPPStartTimer (&pAuthPtr->ServerInfo.ServerChap.ChapChalTimer,
                       CHAP_CHAL_TIMER,
                       pAuthPtr->ServerInfo.ServerChap.TimeoutTime);

        /* Increment number of retransmits */
        ++pAuthPtr->ServerInfo.ServerChap.ChallengeTransmits;

    }
    else
    {
        PRINT_MEM_ALLOC_FAILURE;
    }
    return;
}

/*********************************************************************
*  Function Name : MSCHAPSendStatus
*  Description   :
*                   This procedure is used to send back the status of the
*   received MSCHAP response. It  is used by the  MSCHAPRcvdResponse() function
*   after validating the response value with the calculated one.
*  Parameter(s)  :
*           pAuthPtr -  pointer to Auth Infostructure which contains all
*                                    the MSCHAP information.
*           Code     -  Indicates whether the Status is  TRUE /FALSE
*            ErrorCode - Error Code on Failure
*  Return Values : VOID
*********************************************************************/
VOID
MSCHAPSendStatus (tAUTHIf * pAuthPtr, UINT1 Code, const UINT1 *ErrorCode)
{
    t_MSG_DESC         *pOutBuf;
    UINT1               Msg[MAX_MSG_SIZE];
    UINT2               Length;
    UINT1               Version = '1', RetryFlag = '0';

    if (Code == CHAP_TRUE)
    {
        STRCPY (Msg, "You have successfully logged in....");
        Length = (UINT2) (STRLEN (Msg) + CP_HDR_LEN);
    }
    else
    {
        /* 3 octets for decimal error code, 1 octet for retry flag,
           1 octet for version and 2 spaces */
        Length = CP_HDR_LEN + 7;
    }

    if ((pOutBuf =
         (t_MSG_DESC *) (VOID *) ALLOCATE_BUFFER (PPP_INFO_OFFSET + Length,
                                                  PPP_INFO_OFFSET)) != NULL)
    {

        FILL_PKT_HEADER (pOutBuf, Code,
                         pAuthPtr->ServerInfo.ServerChap.ChallengeId, Length);
        if (Code == CHAP_TRUE)
        {
            APPENDSTR (pOutBuf, Msg, STRLEN (Msg));
        }
        else
        {
            UINT1               au1Err[3];
            MEMCPY (au1Err, ErrorCode, 3);
            APPENDSTR (pOutBuf, au1Err, 3);
            APPEND1BYTE (pOutBuf, ' ');
            APPEND1BYTE (pOutBuf, RetryFlag);
            APPEND1BYTE (pOutBuf, ' ');
            APPEND1BYTE (pOutBuf, Version);
        }

        PPPLLITxPkt (pAuthPtr->pIf, pOutBuf, Length, CHAP_PROTOCOL);
    }
    else
    {

        PRINT_MEM_ALLOC_FAILURE;
    }
    return;
}

/*********************************************************************
*  Function Name : MSChapProcessAuthFailurePacket
*  Description   :
*                   This procedure is used to process the received Auth
*   failure packet. The challenge value, if received, will be used in
*    calculating the response or the first challenge value will be used.
*  Parameter(s)  :
*           pAuthPtr -  pointer to Auth Infostructure which contains all
*                                    the MSCHAP information.
*           pInpacket - pointer to the received failure packet
*            Identifier - Id of the packet received
*            Length - Length of the failure packet
*  Return Values : VOID
*********************************************************************/
VOID
MSChapProcessAuthFailurePacket (tAUTHIf * pAuthPtr, t_MSG_DESC * pInpacket,
                                UINT1 Identifier, UINT2 Length)
{
    INT4                FailureCode, RetryCode, VersionCode = -1;
    UINT1               FailureStr[MSCHAP_MAXFAIL_PACKET_LENGTH],
        *pChallenge, *pTmp;

    pChallenge = NULL;
    EXTRACTSTR (pInpacket, FailureStr, Length);
    pTmp = (UINT1 *) STRTOK (FailureStr, " ");
    if (pTmp != NULL)
    {
        Length = (UINT2) (Length - STRLEN (pTmp));
    }
    FailureCode = ATOI (pTmp);

    switch (FailureCode)
    {
        case ERROR_RESTRICTED_LOGON_HOURS:
            break;
        case ERROR_ACCT_DISABLED:
            break;
        case ERROR_PASSWD_EXPIRED:
            break;
        case ERROR_NO_DIALIN_PERMISSION:
            break;
        case ERROR_AUTHENTICATION_FAILURE:
            break;
        case ERROR_CHANGING_PASSWORD:
            break;
        default:
            break;
    }
    pTmp = (UINT1 *) STRTOK (NULL, " ");
    RetryCode = ATOI (pTmp);
    Length--;

    if (Length + 1 >= MSCHAP_CHALLENGE_LENGTH)
    {
        pTmp = (UINT1 *) STRTOK (NULL, " ");
        if ((pTmp != NULL) && (STRLEN (pTmp) == MSCHAP_CHALLENGE_LENGTH))
        {
            if ((pChallenge = ALLOC_STR (MSCHAP_CHALLENGE_LENGTH)) == NULL)
            {
                PRINT_MEM_ALLOC_FAILURE;
                return;
            }
        }
        else
        {
            PPP_DBG ("Invalid Challenge Value (Not equal to 8 octets");
            return;
        }
        MEMCPY (pChallenge, pTmp, MSCHAP_CHALLENGE_LENGTH);
        Length = (UINT2) (Length - MSCHAP_CHALLENGE_LENGTH);
    }

    if (Length != 0)
    {
        pTmp = (UINT1 *) STRTOK (NULL, " ");
        VersionCode = ATOI (pTmp);
        if (pTmp != NULL)
        {
            Length = (UINT2) (Length - STRLEN (pTmp));
        }
    }

    if (FailureCode == ERROR_AUTHENTICATION_FAILURE)
    {
        if (RetryCode == 1)
        {
            if (pChallenge == NULL)
            {
                pAuthPtr->ClientInfo.ClientChap.pChallenge[0] =
                    (UINT1) (pAuthPtr->ClientInfo.ClientChap.pChallenge[0] +
                             23);
                pChallenge = pAuthPtr->ClientInfo.ClientChap.pChallenge;
            }
            MSChapPrepareAndSendResponse (pAuthPtr, pChallenge, ++Identifier);
        }
        else
        {
            PPP_TRC (ALL_FAILURE, " Authentication Failed : No Retry Allowed");
        }
    }
    else
    {
        if (FailureCode == ERROR_PASSWD_EXPIRED)
        {
            if ((RetryCode == 0) && (VersionCode == 2))
            {
                if (pChallenge == NULL)
                {
                    pAuthPtr->ClientInfo.ClientChap.pChallenge[0] =
                        (UINT1) (pAuthPtr->ClientInfo.ClientChap.pChallenge[0] +
                                 23);
                    pChallenge = pAuthPtr->ClientInfo.ClientChap.pChallenge;
                }
                MSChapPrepareAndSendChangePasswdPacket (pAuthPtr, pChallenge,
                                                        ++Identifier);
            }
            else
            {
                PPP_DBG
                    ("Authentication Failed : Invalid R bit/Unknown Version Code Received");
            }
        }
    }
    return;
}

/*********************************************************************
*  Function Name : MSChapPrepareAndSendResponse
*  Description   :
*                   This procedure is used to construct the MSCHAP Response
*   packet for the received failure packet, if a retry is allowed. The 
*    constucted response packet is sent to the peer
*  Parameter(s)  :
*           pAuthPtr -  pointer to Auth Infostructure which contains all
*                                    the MSCHAP information.
*           pChallenge - pointer to the challenge
*            Identifier - Id of the packet to be sent to the peer
*  Return Values : VOID
*********************************************************************/
VOID
MSChapPrepareAndSendResponse (tAUTHIf * pAuthPtr, UINT1 *pChallenge,
                              UINT1 Identifier)
{
    UINT1               NTChalResponse[MSCHAP_NT_CHAL_RESP_LENGTH],
        PasswdLen, Index, PasswordHash[MSCHAP_PASSWD_HASH_LENGTH];
    UINT1               au1Password[MAX_SECRET_SIZE];

#ifdef LM_PASSWD_HASH
    UINT1              *Password;
#else
    UINT2              *Password;
#endif
    tSecret            *pClientSecret;

    BZERO (au1Password, MAX_SECRET_SIZE);
    BZERO (PasswordHash, MSCHAP_PASSWD_HASH_LENGTH);

    /* The AUTHGetSecret function needs to rewritten or a new function is
       required to get the secret for MSCHAP */

    if ((pAuthPtr->ClientInfo.ClientChap.pCHAPClientSecret =
         MSCHAPAUTHGetSecret (pAuthPtr, NULL, MSCHAP_PROTOCOL, CLIENT)) == NULL)
    {
        PPP_DBG ("Unable to send response due to insufficient secret info. ");
        return;
    }
    pClientSecret = pAuthPtr->ClientInfo.ClientChap.pCHAPClientSecret;

    /* Cancel response send timeout if necessary */
    if (pAuthPtr->ClientInfo.ClientChap.State == CHAP_CLIENT_STATE_RESPONSE)
    {
        PPPStopTimer (&pAuthPtr->ClientInfo.ClientChap.ChapRespTimer);
    }
    pAuthPtr->ClientInfo.ClientChap.ResponseId = Identifier;

    /* Generate the MSCHAP Response */

#ifdef LM_PASSWD_HASH
    PasswdLen = pClientSecret->SecretLen;
    if ((Password = (UINT1 *) ALLOC_STR (PasswdLen)) == NULL)
    {
        PRINT_MEM_ALLOC_FAILURE;
        return;
    }
#else
    PasswdLen = (UINT1) (pClientSecret->SecretLen * 2);
    if ((Password = (UINT2 *) (VOID *) ALLOC_STR (PasswdLen)) == NULL)
    {
        PRINT_MEM_ALLOC_FAILURE;
        return;
    }
#endif

    STRCPY (au1Password, pClientSecret->Secret);
    FsUtlDecryptPasswd ((CHR1 *) au1Password);

    for (Index = 0; Index < pClientSecret->SecretLen; Index++)
        Password[Index] = au1Password[Index];

#ifdef LM_PASSWD_HASH
    MSChapLmPasswordHash (Password, PasswordHash, pClientSecret->SecretLen);
#else
    MSChapNtPasswordHash (Password, PasswordHash, pClientSecret->SecretLen);
#endif
    MSChapChallengeResponse (pChallenge, PasswordHash, NTChalResponse);

    MEMCPY (&
            (pAuthPtr->ClientInfo.ClientChap.
             pResponse[MSCHAP_NT_CHAL_RESP_LENGTH]), NTChalResponse,
            MSCHAP_NT_CHAL_RESP_LENGTH);

    pAuthPtr->ClientInfo.ClientChap.pResponse[MSCHAP_RESPONSE_LENGTH - 1] = 1;
    pAuthPtr->ClientInfo.ClientChap.ResponseLength = MSCHAP_RESPONSE_LENGTH;

    if (pAuthPtr->ClientInfo.ClientChap.ResponseTransmits >=
        pAuthPtr->ClientInfo.ClientChap.MaxTransmits)
    {
        PPP_TRC (ALL_FAILURE, "Peer failed to respond to MSCHAP Challenge\r\n");
        pAuthPtr->ClientInfo.ClientChap.State = CHAP_CLIENT_STATE_BADAUTH;
#ifdef RADIUS_WANTED
        pAuthPtr->pIf->RadInfo.RadReqId = INIT_VAL;
#endif
    }
    else
    {
        /* re-send Response */
        CHAPSendResponse (pAuthPtr);
    }

    FREE_STR (Password);
    return;
}

/*********************************************************************
*  Function Name : MSChapPrepareAndSendChangePasswdPacket
*  Description   :
*                   This procedure is used to constuct the MSCHAP change
*    Password packet if the received failure code is ERROR_PASSWD_EXPIRED.
*    The change password packet is sent to the peer.
*  Parameter(s)  :
*           pAuthPtr -  pointer to Auth Infostructure which contains all
*                                    the MSCHAP information.
*           pChallenge - pointer to the challenge
*            Identifier - Id of the packet to be sent to the peer
*  Return Values : VOID
*********************************************************************/
VOID
MSChapPrepareAndSendChangePasswdPacket (tAUTHIf * pAuthPtr, UINT1 *pChallenge,
                                        UINT1 Identifier)
{
    UINT1               NTChalResponse[MSCHAP_NT_CHAL_RESP_LENGTH],
        TmpChalResponse[MSCHAP_NT_CHAL_RESP_LENGTH],
        Index,
        PasswordHash[MSCHAP_PASSWD_HASH_LENGTH],
        TmpPasswdHash[MSCHAP_PASSWD_HASH_LENGTH],
        EncryptedPasswdHash[MSCHAP_PASSWD_HASH_LENGTH], PasswdLen;
    UINT2               TmpLen, Flags,
#ifndef LM_PASSWD_HASH
                        NewPasswd[MSCHAP_PASSWD_HASH_LENGTH],
#endif
                       *Password, Length;
    tSecret            *pClientSecret;
    tPWBlock            PasswdBlk, TmpPasswdBlk;
    t_MSG_DESC         *pOutBuf;
    MEMSET (&PasswdBlk, 0, sizeof (tPWBlock));
    BZERO (PasswordHash, MSCHAP_PASSWD_HASH_LENGTH);
    BZERO (TmpPasswdHash, MSCHAP_PASSWD_HASH_LENGTH);
    BZERO (&TmpPasswdBlk, MSCHAP_ENCRYPTED_HASH_LENGTH);
    BZERO (TmpChalResponse, MSCHAP_NT_CHAL_RESP_LENGTH);

    /* The AUTHGetSecret function needs to rewritten or a new function is
       required to get the secret for MSCHAP. For Change Password Packet,
       the expired password(secret) should be deleted from the secrets
       list and then a search has to be made for a new secret */

    if ((pAuthPtr->ClientInfo.ClientChap.pCHAPClientSecret =
         MSCHAPAUTHGetSecret (pAuthPtr, NULL, MSCHAP_PROTOCOL, CLIENT)) == NULL)
    {
        PPP_TRC (ALL_FAILURE,
                 "Unable to send response due to insufficient secret info. ");
        return;
    }
    pClientSecret = pAuthPtr->ClientInfo.ClientChap.pCHAPClientSecret;

    /* Cancel response send timeout if necessary */
    if (pAuthPtr->ClientInfo.ClientChap.State == CHAP_CLIENT_STATE_RESPONSE)
    {
        PPPStopTimer (&pAuthPtr->ClientInfo.ClientChap.ChapRespTimer);
    }
    pAuthPtr->ClientInfo.ClientChap.ResponseId = Identifier;
    pAuthPtr->ClientInfo.ClientChap.ResponseTransmits = 0;

    PasswdLen = (UINT1) (pClientSecret->SecretLen * 2);

    if ((Password = (UINT2 *) (VOID *) ALLOC_STR (PasswdLen)) == NULL)
    {
        PRINT_MEM_ALLOC_FAILURE;
        return;
    }

    if ((pAuthPtr->ClientInfo.ClientChap.pMSCHAPChgPwdPkt =
         ALLOC_STR (MSCHAP_CHANGE_PASSWD_PACKET_LENGTH)) == NULL)
    {
        PRINT_MEM_ALLOC_FAILURE;
        return;
    }

    for (Index = 0; Index < pClientSecret->SecretLen; Index++)
        Password[Index] = pClientSecret->Secret[Index];

    /* No idea how to get the New Password */

#ifndef LM_PASSWD_HASH

    MSChapNewPasswdEncryptedWithOldNTPasswdHash (NewPasswd, Password,
                                                 PasswdLen,
                                                 pClientSecret->SecretLen,
                                                 &PasswdBlk);
    MSChapOldNtPasswdHashEncryptedWithNewNtPasswdHash (NewPasswd, Password,
                                                       PasswdLen,
                                                       pClientSecret->SecretLen,
                                                       EncryptedPasswdHash);
    MSChapNtPasswordHash (NewPasswd, PasswordHash, sizeof (NewPasswd));
#endif

    MSChapChallengeResponse (pChallenge, PasswordHash, NTChalResponse);

    /* Send the response to peer */

    Length = MSCHAP_CHANGE_PASSWD_PACKET_LENGTH;

    if ((pOutBuf =
         (t_MSG_DESC *) (VOID *) ALLOCATE_BUFFER (PPP_INFO_OFFSET + Length,
                                                  PPP_INFO_OFFSET)) != NULL)
    {
        TmpLen = 0;
        Flags = 1;
        MEMCPY (&(pAuthPtr->ClientInfo.ClientChap.pMSCHAPChgPwdPkt[TmpLen]),
                &PasswdBlk, MSCHAP_ENCRYPTED_HASH_LENGTH);
        TmpLen = (UINT2) (TmpLen + MSCHAP_ENCRYPTED_HASH_LENGTH);
        MEMCPY (&(pAuthPtr->ClientInfo.ClientChap.pMSCHAPChgPwdPkt[TmpLen]),
                EncryptedPasswdHash, MSCHAP_PASSWD_HASH_LENGTH);
        TmpLen = (UINT2) (TmpLen + MSCHAP_PASSWD_HASH_LENGTH);
        MEMCPY (&(pAuthPtr->ClientInfo.ClientChap.pMSCHAPChgPwdPkt[TmpLen]),
                &TmpPasswdBlk, MSCHAP_ENCRYPTED_HASH_LENGTH);
        TmpLen = (UINT2) (TmpLen + MSCHAP_ENCRYPTED_HASH_LENGTH);
        MEMCPY (&(pAuthPtr->ClientInfo.ClientChap.pMSCHAPChgPwdPkt[TmpLen]),
                TmpPasswdHash, MSCHAP_PASSWD_HASH_LENGTH);
        TmpLen = (UINT2) (TmpLen + MSCHAP_PASSWD_HASH_LENGTH);
        MEMCPY (&(pAuthPtr->ClientInfo.ClientChap.pMSCHAPChgPwdPkt[TmpLen]),
                TmpChalResponse, MSCHAP_NT_CHAL_RESP_LENGTH);
        TmpLen = (UINT2) (TmpLen + MSCHAP_NT_CHAL_RESP_LENGTH);
        MEMCPY (&(pAuthPtr->ClientInfo.ClientChap.pMSCHAPChgPwdPkt[TmpLen]),
                NTChalResponse, MSCHAP_NT_CHAL_RESP_LENGTH);
        TmpLen = (UINT2) (TmpLen + MSCHAP_NT_CHAL_RESP_LENGTH);
        MEMCPY (&(pAuthPtr->ClientInfo.ClientChap.pMSCHAPChgPwdPkt[TmpLen]),
                &Flags, 2);

        FILL_PKT_HEADER (pOutBuf, MSCHAP_CHANGE_PASSWD_CODE, Identifier,
                         Length);
        APPENDSTR (pOutBuf, pAuthPtr->ClientInfo.ClientChap.pMSCHAPChgPwdPkt,
                   MSCHAP_ENCRYPTED_HASH_LENGTH);

        PPPLLITxPkt (pAuthPtr->pIf, pOutBuf, Length, CHAP_PROTOCOL);

        pAuthPtr->ClientInfo.ClientChap.State = CHAP_CLIENT_STATE_RESPONSE;
        pAuthPtr->ClientInfo.ClientChap.ChapRespTimer.Param1 =
            PTR_TO_U4 (pAuthPtr);
        PPPStartTimer (&pAuthPtr->ClientInfo.ClientChap.ChapRespTimer,
                       CHAP_RESP_TIMER,
                       pAuthPtr->ClientInfo.ClientChap.TimeoutTime);

        pAuthPtr->ClientInfo.ClientChap.ResponseId = Identifier;
        ++pAuthPtr->ClientInfo.ClientChap.ResponseTransmits;
    }
    else
    {

        PRINT_MEM_ALLOC_FAILURE;
    }

    FREE_STR (Password);
    return;
}

/****************************************************************************
*                    MSCHAP INTERNAL    FUNCTIONS
*****************************************************************************/

/* All the length arguments passed to the functions represent the No. of UNICODE
Characters only. The exact no. of octets has to be calculated inside the 
function body depending on the size of the unicode char */

    /* Common Function for both LM and NT type of responses */

VOID
MSChapChallengeResponse (UINT1 *pChallenge, UINT1 *pPasswordHash,
                         UINT1 *pNtChalResponse)
{
    UINT1               ZPasswordHash[MSCHAP_PADDED_PASSWD_LENGTH];
    des_cblock          passwd[1], DesOutput[1], TmpChal[1];
    des_key_schedule    schedule;

    BZERO (passwd[0], 8);
    MEMCPY (&ZPasswordHash[0], pPasswordHash, MSCHAP_PASSWD_HASH_LENGTH);
    MEMCPY (TmpChal[0], pChallenge, MSCHAP_CHALLENGE_LENGTH);

    /* Zero Pad ZPasswordHash to 21 octets */

    BZERO (&ZPasswordHash[MSCHAP_PASSWD_HASH_LENGTH], 5);

    MEMCPY (passwd[0], &ZPasswordHash[0], 7);
    des_7_To_8_Byte (passwd);
    des_key_sched (passwd, schedule);
    des_ecb_encrypt (TmpChal, DesOutput, schedule, 1);
    MEMCPY (pNtChalResponse, DesOutput[0], MSCHAP_CHALLENGE_LENGTH);

    MEMCPY (passwd[0], &ZPasswordHash[7], 7);
    des_7_To_8_Byte (passwd);
    des_key_sched (passwd, schedule);
    des_ecb_encrypt (TmpChal, DesOutput, schedule, 1);
    MEMCPY (&pNtChalResponse[8], DesOutput[0], MSCHAP_CHALLENGE_LENGTH);

    MEMCPY (passwd[0], &ZPasswordHash[14], 7);
    des_7_To_8_Byte (passwd);
    des_key_sched (passwd, schedule);
    des_ecb_encrypt (TmpChal, DesOutput, schedule, 1);
    MEMCPY (&pNtChalResponse[16], DesOutput[0], MSCHAP_CHALLENGE_LENGTH);

    return;
}

VOID
MSChapLmPasswordHash (UINT1 *pPassword, UINT1 *pPasswordHash, UINT1 Length)
{
    UINT1               UcasePasswd[14];
    UINT1               Index;

    BZERO (UcasePasswd, 14);

    for (Index = 0; Index < Length; Index++)
        UcasePasswd[Index] = (UINT1) (toupper (pPassword[Index]));

    DesHash (UcasePasswd, pPasswordHash);
    DesHash (&UcasePasswd[7], pPasswordHash + 8);

    return;
}

VOID
DesHash (UINT1 *pPasswd, UINT1 *pPwdHash)
{
    des_cblock          passwd[1], DesOutput[1], TmpChal[1];
    des_key_schedule    schedule;

    BZERO (passwd[0], 8);
    MEMCPY (passwd[0], pPasswd, 7);
    MEMCPY (TmpChal[0], "KGS!@#$%", MSCHAP_CHALLENGE_LENGTH);

    des_7_To_8_Byte (passwd);
    des_key_sched (passwd, schedule);
    des_ecb_encrypt (TmpChal, DesOutput, schedule, 1);
    MEMCPY (pPwdHash, DesOutput[0], 8);
    return;
}

VOID
MSChapNtPasswordHash (UINT2 *pPassword, UINT1 *pPasswordHash, UINT1 Length)
{
    MD4_CTX             Context;

    MD4Init (&Context);
    /* casted pPassword to UINT1 * for removing the compilation warn */
    MD4Update (&Context, (UINT1 *) pPassword,
               (unsigned int) (Length * MSCHAP_SIZEOF_UNICODE_CHAR));
    MD4Final (pPasswordHash, &Context);

    return;
}

VOID
MSChapNewPasswdEncryptedWithOldNTPasswdHash (UINT2 *pNewPasswd,
                                             UINT2 *pOldPasswd, UINT1 NewLen,
                                             UINT1 OldLen,
                                             tPWBlock * pEncryptedPwBlock)
{
    UINT1               PasswdHash[MSCHAP_PASSWD_HASH_LENGTH];

    MSChapNtPasswordHash (pOldPasswd, PasswdHash, OldLen);
    MSChapEncryptPwBlockWithPasswdHash (pNewPasswd, NewLen, PasswdHash,
                                        pEncryptedPwBlock);
}

VOID
MSChapEncryptPwBlockWithPasswdHash (UINT2 *pNewPasswd, UINT1 PasswdLen,
                                    UINT1 *pPasswdHash,
                                    tPWBlock * pEncryptedPwBlock)
{
    tPWBlock            ClearPwBlock;
    INT4                Index, PasswdSize, PasswdOffset;

    for (Index = 0; Index < MSCHAP_MAX_PASSWD_LENGTH; Index++)
        ClearPwBlock.Password[Index] = (UINT2) (GET_RANDOM_NUMBER ());

    ClearPwBlock.PasswdLength = MSCHAP_MAX_PASSWD_LENGTH;

    PasswdSize = PasswdLen * MSCHAP_SIZEOF_UNICODE_CHAR;
    PasswdOffset = (INT4) sizeof (ClearPwBlock.Password) - PasswdSize;
    MEMCPY ((UINT1 *) (ClearPwBlock.Password) + PasswdOffset, pNewPasswd,
            PasswdSize);
    ClearPwBlock.PasswdLength = PasswdSize;

    pEncryptedPwBlock =
        (tPWBlock *) RC4Encrypt (&ClearPwBlock, sizeof (ClearPwBlock),
                                 pPasswdHash, sizeof (pPasswdHash));
    PPP_UNUSED (pEncryptedPwBlock);
}

VOID
MSChapOldNtPasswdHashEncryptedWithNewNtPasswdHash (UINT2 *pNewPasswd,
                                                   UINT2 *pOldPasswd,
                                                   UINT1 NewLen, UINT1 OldLen,
                                                   UINT1 *pEncryptedPasswdHash)
{
    UINT1               OldPasswdHash[MSCHAP_PASSWD_HASH_LENGTH],
        NewPasswdHash[MSCHAP_PASSWD_HASH_LENGTH];

    MSChapNtPasswordHash (pOldPasswd, OldPasswdHash, OldLen);
    MSChapNtPasswordHash (pNewPasswd, NewPasswdHash, NewLen);
    MSChapNtPasswdHashEncryptedWithBlock (OldPasswdHash, NewPasswdHash,
                                          pEncryptedPasswdHash);

    return;
}

VOID
MSChapNtPasswdHashEncryptedWithBlock (UINT1 *pPasswdHash, UINT1 *pBlock,
                                      UINT1 *pCypher)
{
    des_cblock          TmpBlock[1], DesOutput[1], TmpPwdHash[1];
    des_key_schedule    schedule;

    BZERO (TmpBlock[0], 8);
    MEMCPY (TmpPwdHash[0], pPasswdHash, 8);

    MEMCPY (TmpBlock[0], pBlock, 7);
    des_7_To_8_Byte (TmpBlock);
    des_key_sched (TmpBlock, schedule);
    des_ecb_encrypt (TmpPwdHash, DesOutput, schedule, 1);
    MEMCPY (pCypher, DesOutput[0], 8);

    BZERO (TmpBlock[0], 8);
    MEMCPY (TmpPwdHash[0], pPasswdHash + 8, 8);

    MEMCPY (TmpBlock[0], pBlock + 7, 7);
    des_7_To_8_Byte (TmpBlock);
    des_key_sched (TmpBlock, schedule);
    des_ecb_encrypt (TmpPwdHash, DesOutput, schedule, 1);
    MEMCPY (pCypher + 8, DesOutput[0], 8);

    return;
}

tPWBlock           *
RC4Encrypt (tPWBlock * pClearPwBlock, UINT2 BlockLen, UINT1 *pPasswdHash,
            UINT2 PasswdHashLen)
{

    PPP_UNUSED (pClearPwBlock);
    PPP_UNUSED (BlockLen);
    PPP_UNUSED (pPasswdHash);
    PPP_UNUSED (PasswdHashLen);

    return (pClearPwBlock);
}
