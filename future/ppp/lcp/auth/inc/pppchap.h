/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppchap.h,v 1.3 2011/07/12 11:04:27 siva Exp $
 *
 * Description: This file contains the CHAP data structures
 *
 *******************************************************************/
#ifndef __PPP_PPPCHAP_H__
#define __PPP_PPPCHAP_H__

/************************************************************************/
/*                  CONSTANTS USED IN THE CHAP SUBMODULE                */
/************************************************************************/

#define  CHAP_CLIENT_STATE_INITIAL       0   
#define  CHAP_CLIENT_STATE_CLOSED        1   
#define  CHAP_CLIENT_STATE_PENDING       2   
#define  CHAP_CLIENT_STATE_LISTEN        3   
#define  CHAP_CLIENT_STATE_RESPONSE      4   
#define  CHAP_CLIENT_STATE_OPEN          5   
#define  CHAP_CLIENT_STATE_BADAUTH       6   

#define  CHAP_SERVER_STATE_INITIAL       0   
#define  CHAP_SERVER_STATE_CLOSED        1   
#define  CHAP_SERVER_STATE_PENDING       2   
#define  CHAP_SERVER_STATE_INITIAL_CHAL  3   
#define  CHAP_SERVER_STATE_OPEN          4   
#define  CHAP_SERVER_STATE_RECHALLENGE   5   
#define  CHAP_SERVER_STATE_BADAUTH       6   

#define  CHAP_CHALLENGE_REQ              1   
#define  CHAP_CHALLENGE_RESP             2   
#define  CHAP_TRUE                       3   
#define  CHAP_FALSE                      4   
#define  CHAP_DEFTIMEOUT                 30  
#define  CHAP_DEFTRANSMITS               2   
#define  CHAP_DEF_RECHAL_TIMEOUT         5   

#define  MAX_CHALLENGE_LENGTH            64  

#define  MIN_CHALLENGE_LENGTH            16  
#define  MAX_RESPONSE_LENGTH             16  
#define  MD5                             5   
#define  MD5_SIGNATURE_SIZE              16  
#define  MAX_SECRET_LENGTH               16  
#define  PPP_MAX_NAME_LENGTH             32
#define  MAX_MSG_SIZE                    55  
#define  MIN_CHAP_PKT_LEN                1   
#define  VALUE_SIZE_FIELD_LEN            1   
#define  MAX_SECRET_SIZE                 255 

#define  MIN_SECRET_HOSTNAME_SIZE        0   
#define  MAX_SECRET_HOSTNAME_SIZE        255 

typedef struct {
    t_SLL_NODE NextSecretNode;
    UINT4       IdIndex;
    UINT4      CharSet;
    UINT2      Protocol;
    UINT1      Direction;
    UINT1      IdentityLen;
    UINT1      Identity[MAX_SECRET_SIZE];
    UINT1      Secret[MAX_SECRET_SIZE];
    UINT1      SecretLmHash[MAX_SECRET_SIZE];
    UINT1      LangTag[MAX_LANGUAGE_NAME];
    UINT1      SecretLen;
    UINT1      Status;
    UINT1      StatusValid;
    UINT1      LangTagLen;
    UINT1      u1Rsvd;
} tSecret;

typedef struct {
    UINT1     State;              /* Client state */
    UINT1     u1Rsvd1;
    UINT2     u2Rsvd2;
    UINT1     *pResponse;    /* Response to send */
    UINT1     *pMSCHAPChgPwdPkt;    /* Response to send */
    UINT1     *pChallenge;  /* First MSCHAP Chal Val*/
    UINT1     ResponseLength;     /* length of response */
    UINT1     ResponseId;         /* ID for response messages */
    UINT2     TimeoutTime;        /* Timeout time in seconds */
    UINT2     MaxTransmits;       /* Maximum # of challenge transmissions */
    UINT2     ResponseTransmits;  /* Number of transmissions of response */
    tPPPTimer ChapRespTimer;
    tSecret  *pCHAPClientSecret;
} tCHAPClientInfo;

typedef struct {
    UINT1     State;                /* Server state */
    UINT1     ChallengeValue[MAX_CHALLENGE_LENGTH+2];
    UINT1     ChallengeValueLength; /* challenge length */
    UINT1     ChallengeId;          /* ID of last challenge */
    UINT1     CurrentId;            /* Current id */
    UINT2     ChallengeInterval;    /* Time for challenging the peer again */
    UINT2     ChallengeTransmits;   /* Number of transmissions of challenge */
    UINT2     TimeoutTime;          /* Timeout time in seconds */
    UINT2     MaxTransmits;         /* Maximum # of challenge transmissions */
    UINT2     u2Rsvd;
    tPPPTimer ChapChalTimer;
    tPPPTimer ChapReChalTimer;
    tSecret  *pCHAPCurrentSecret;
} tCHAPServerInfo;



#endif  /* __PPP_PPPCHAP_H__ */
