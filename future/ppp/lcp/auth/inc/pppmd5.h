/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppmd5.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 *******************************************************************/
#ifndef __PPP_PPPMD5_H__
#define __PPP_PPPMD5_H__

typedef struct {
	unsigned long i[2];					/* number of _bits_ handled mod */
	unsigned long Buf[4];				/* scratch buffer */
	unsigned char In[64];				/* input buffer */
	unsigned char Digest[16];			/* actual digest after MD5Final call */
} tMD5_CTX;


#endif  /* __PPP_PPPMD5_H__ */
