/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppmd4.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This files contains the interface procedures of the
 *             Framing module.
 *
 *******************************************************************/
#ifndef __PPP_PPPMD4_H__
#define __PPP_PPPMD4_H__

/* POINTER defines a generic pointer type */ 
typedef unsigned char *POINTER; 

typedef struct {
	unsigned long	state[4];  /* state (ABCD) */
	unsigned long	count[2];  /* number of bits, modulo 2^64 (lsb first) */
	unsigned char	buffer[64]; /* input buffer */
} MD4_CTX;




#endif  /* __PPP_PPPMD4_H__ */
