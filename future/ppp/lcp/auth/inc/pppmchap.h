/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppmchap.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the CHAP Data Structures.
 *
 *******************************************************************/

#ifndef __PPP_PPPMCHAP_H__
#define __PPP_PPPMCHAP_H__


/************************************************************************/
/*                  CONSTANTS USED IN THE MS-CHAP SUBMODULE                */
/************************************************************************/


#define  MSCHAP_CHALLENGE_LENGTH             8                    
#define  MSCHAP_MAX_PASSWD_LENGTH            256                  
#define  MSCHAP_MAX_NAME_LENGTH              256                  
#define  MSCHAP_RESPONSE_LENGTH              49                   
#define  MSCHAP_CHANGE_PASSWD_PACKET_LENGTH  1118                 
#define  MSCHAP_NT_CHAL_RESP_LENGTH          24                   
#define  MSCHAP_LM_CHAL_RESP_LENGTH          24                   
#define  MSCHAP_PASSWD_HASH_LENGTH           16                   
#define  MSCHAP_ENCRYPTED_HASH_LENGTH        516                  
#define  MSCHAP_CHANGE_PASSWD_CODE           6                    
#define  MSCHAP_SIZEOF_UNICODE_CHAR          2                    
#define  MSCHAP_PADDED_PASSWD_LENGTH         21                   
#define  MSCHAP_MAXFAIL_PACKET_LENGTH        37                   

#define  MSCHAP_ALGORITHM                    0x80                 

/* MSCHAP Failure Error Codes */

#define  ERROR_RESTRICTED_LOGON_HOURS        646                  
#define  ERROR_ACCT_DISABLED                 647                  
#define  ERROR_PASSWD_EXPIRED                648                  
#define  ERROR_NO_DIALIN_PERMISSION          649                  
#define  ERROR_AUTHENTICATION_FAILURE        691                  
#define  ERROR_CHANGING_PASSWORD             709                  

typedef struct {
        UINT2   Password[MSCHAP_MAX_PASSWD_LENGTH];
        INT4    PasswdLength;
} tPWBlock;

typedef UINT1 tDesBlock[8];


#endif  /* __PPP_PPPMCHAP_H__ */
