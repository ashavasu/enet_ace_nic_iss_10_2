/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppauth.h,v 1.3 2011/10/13 10:31:30 siva Exp $
 *
 * Description:This file contains the typedefs common to 
               the CHAP, MSCHAP & PAP authentication protocol.
 *  
 *******************************************************************/
#ifndef __PPP_PPPAUTH_H__
#define __PPP_PPPAUTH_H__

#define  CLIENT         1 
#define  SERVER         2 
#define  LOC_TO_REMOTE  1 
#define  REMOTE_TO_LOC  2 

#define  MAX_AUTH_PROT  4 

#define SECRETS_STATUS_NOTINSERVICE 3
#define SECRETS_STATUS_VALID 2
#define SECRETS_STATUS_INVALID 1

#define MAX_SECRETS_IDENTITY_LENGTH 255
#define MIN_SECRETS_IDENTITY_LENGTH 0
/********************************************************************/
/*          TYPEDEFS    USED BY THE AUTH MODULE                     */
/*********************************************************************/

/*  This data structure contains information need for authenticating
   the local entity with the remote over the established link
 */
typedef union {
 tCHAPClientInfo ClientChap;
 tPAPClientInfo  ClientPap;
        tEAPClientInfo  ClientEap;
} tClientState;

/*  This data structure contains information need for authenticating
   the peer entity with the local over the established link
 */
typedef union {
 tCHAPServerInfo ServerChap;
 tPAPServerInfo  ServerPap;
        tEAPServerInfo  ServerEap;
} tServerState;

typedef struct {
 UINT2     Protocol;
 UINT1     ChapAlgo;
 UINT1   u1Rsvd;
 UINT4     Preference;
} tAuthPref;

typedef struct {
 t_SLL          SecretList;
 tAuthPref      PrefList[MAX_AUTH_PROT];
 INT1           NumActiveProt;
 UINT1          CHAPLocToRemSecrets;
 UINT1          MSCHAPLocToRemSecrets;
        UINT1          EAPMD5LocToRemSecrets;  
 UINT1          PAPLocToRemSecrets;
 UINT1          CHAPRemToLocSecrets;
 UINT1          MSCHAPRemToLocSecrets;
 UINT1          PAPRemToLocSecrets;
        UINT1          EAPMD5RemToLocSecrets;
 UINT1        u1Rsvd1;
 UINT2        u2Rsvd2;
}tSecurityInfo;

typedef struct authif {
 tPPPIf        *pIf;
 tSecurityInfo  SecurityInfo;
 INT2           LastTriedIndex;
 UINT2          ClientProt;
 UINT2          ServerProt;
 UINT2        u2Rsvd1;
 tClientState   ClientInfo;
 tServerState   ServerInfo;
 INT4           PapLastSecretIndex;
 INT4           ChapLastSecretIndex;
 INT4           MSChapLastSecretIndex;
        INT4           EapMD5LastSecretIndex; 
 UINT1          PAPSearchList; 
 UINT1          CHAPSearchList;
 UINT1          MSCHAPSearchList;
        UINT1          EAPMD5SearchList; 
/*** SKR007 ***/
 UINT1          AuthTriggeredNCPs; /****** used not to trigger NCPs again after rechallenge **/
 UINT1        u1Rsvd2;
 UINT2        u2Rsvd3;
 
} tAUTHIf;

extern UINT2    AuthAllocCounter;
extern UINT2    AuthFreeCounter;

INT4
MpUpdateSecretToAllMembers (INT4 i4BundleIfIndex, INT4 i4SecretIdIndex,
                            INT4 i4SecretStatus);
VOID
MpAddAllSecretsToNewMember (tPPPIf *pBundleIf, tPPPIf *pMemberIf);
VOID
MpRemoveSecretsFromMember (tPPPIf *pMemberIf);
#endif  /* __PPP_PPPAUTH_H__ */
