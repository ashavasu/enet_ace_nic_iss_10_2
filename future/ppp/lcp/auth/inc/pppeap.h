/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppeap.h,v 1.2 2011/01/06 13:52:38 siva Exp $
 *
 * Description:This file contains the EAP(Extensible Authentication 
 *             Protocol) consants and data structures
 *
 *******************************************************************/
#ifndef __PPP_PPPEAP_H__
#define __PPP_PPPEAP_H__

/************************************************************************/
/*                  CONSTANTS USED IN THE EAP SUBMODULE                */
/************************************************************************/


/*  
 * Constants for the EAP packets 
 */

#define  EAP_REQUEST                       1  
#define  EAP_RESPONSE                      2  
#define  EAP_SUCCESS                       3  
#define  EAP_FAILURE                       4  

/*   
 * Constants for the different types used in the Request/Response
 * packets of EAP  
 */

#define  EAP_TYPE_IDENTITY                 1  
#define  EAP_TYPE_NOTIFICATION             2  
#define  EAP_TYPE_NAK_RESPONSE             3  
#define  EAP_TYPE_MD5                      4  
#define  EAP_TYPE_OTP                      5  


#define  EAP_SEND_IDENTITY                 1  
#define  EAP_DONOT_SEND_IDENITY            2  

#define  EAP_DEF_MAX_IDENTITY_RETRIES      3  
#define  EAP_MAX_IDENTITY_STRING_LENGTH   50 
#define  EAP_PKT_MAX_LEN                  1496 

#define  EAP_TYPE_FIELD_LEN                1  
#define  EAP_CODE_FIELD_LEN                1 
#define  EAP_ID_FIELD_LEN                  1
#define  EAP_LEN_FIELD_LEN                 2
#define  EAP_HDR_LEN                       4



#define  EAP_SERVER_STATE_INITIAL          0  
#define  EAP_SERVER_STATE_IDENTITY         1  
#define  EAP_SERVER_STATE_AUTH_TYPE        2  
#define  EAP_SERVER_STATE_OPEN             3  
#define  EAP_SERVER_STATE_CLOSED           4  
#define  EAP_SERVER_STATE_BADAUTH          5  
#define  EAP_SERVER_STATE_PENDING          6

#define  EAP_CLIENT_STATE_INITIAL          0  
#define  EAP_CLIENT_STATE_LISTEN           1  
#define  EAP_CLIENT_STATE_OPEN             2  
#define  EAP_CLIENT_STATE_BADAUTH          3  

#define  EAPMD5_CLIENT_STATE_INITIAL       0  
#define  EAPMD5_CLIENT_STATE_CLOSED        1  
#define  EAPMD5_CLIENT_STATE_PENDING       2  
#define  EAPMD5_CLIENT_STATE_LISTEN        3  
#define  EAPMD5_CLIENT_STATE_RESPONSE      4  
#define  EAPMD5_CLIENT_STATE_OPEN          5  
#define  EAPMD5_CLIENT_STATE_BADAUTH       6  

#define  EAPMD5_SERVER_STATE_INITIAL       0  
#define  EAPMD5_SERVER_STATE_CLOSED        1  
#define  EAPMD5_SERVER_STATE_PENDING       2  
#define  EAPMD5_SERVER_STATE_INITIAL_CHAL  3  
#define  EAPMD5_SERVER_STATE_OPEN          4  
#define  EAPMD5_SERVER_STATE_RECHALLENGE   5  
#define  EAPMD5_SERVER_STATE_BADAUTH       6  

#define  EAP_MD5_MAX_RESPONSE_LENGTH       16 
#define  EAP_MD5_MAX_CHALLENGE_LENGTH      64 
typedef struct {

    UINT1     State;              /* Client state */
    UINT1     Response[EAP_MD5_MAX_RESPONSE_LENGTH]; /* Response to send */
    UINT1     ResponseLength;     /* length of response */
    UINT1     ResponseId;         /* ID for response messages */
    UINT1     u1Rsvd;
    tSecret  *pEAPMD5ClientSecret;

} tEAPMD5ClientInfo;

typedef struct {

    UINT1     State;                /* Server state */
    UINT1     ChallengeValueLength; /* challenge length */
    UINT1     ChallengeId;          /* ID of last challenge */
    UINT1     CurrentId;            /* Current id */
    UINT1     ChallengeValue[EAP_MD5_MAX_CHALLENGE_LENGTH];
    UINT2     ChallengeInterval;    /* Time for challenging the peer again */
    UINT2     ChallengeTransmits;   /* Number of transmissions of challenge */
    UINT2     TimeoutTime;          /* Timeout time in seconds */
    UINT2     MaxTransmits;         /* Maximum # of challenge transmissions */
    tPPPTimer ChalTimer;
    tPPPTimer ReChalTimer;
    tSecret  *pEAPMD5CurrentSecret;

} tEAPMD5ServerInfo;

typedef struct {

   union {
      tEAPMD5ClientInfo    ClientEAPMD5;
  }AuthTypeOptions;
   INT1     AuthType;              /* client authentication type MD5/OTP */
   UINT1    State;                 /* client state */
   UINT1    au1Rsvd[2];

} tEAPClientInfo;


typedef struct {

   union {
      tEAPMD5ServerInfo    ServerEAPMD5;
   } AuthTypeOptions; 
 
   tPPPTimer EapIdentityTimer;    

   tPPPTimer EapNotifTimer;

   UINT2    TimeoutTime;         /* Time out value for idenity requests */
   UINT2    RequestTransmits;    /* no of times request is retransmitted */ 
   UINT2    MaxTransmits;        /* Max no of transmissions */
   UINT2    MaxIdentityRetries; /* Max no of identity transmissions */
   UINT2    FailedIdentityRequests; /* No of Idenity failures */
   UINT2     NotifReqTxs;  /* no of times notification req is retransmitted */

   /* array to store the identity string */
   UINT1    IdentityString[EAP_MAX_IDENTITY_STRING_LENGTH];
   INT1     AuthType;            /* server authentication type MD5/OTP */
   UINT1    State;               /* client state */  
   UINT1    CurrentId;           /* current identifier */
   
   UINT1    IdentityStringLength;  /* Identity length */
   INT1     IdentityId;           /* Id of the last transmitted request */
   
   /* flag decides whether to send the inital idenity request or not */
   UINT1    IdentityNeeded;   
   
  /* authentication type which will be tried first as a desired authentication
     type in EAP */
   UINT1    DefaultAuthType;  
   UINT1     NotifId;    /* last transmitted notification req */ 
   UINT1    au1Rsvd[2];
                                      
} tEAPServerInfo;

#endif  /* __PPP_PPPEAP_H__ */
