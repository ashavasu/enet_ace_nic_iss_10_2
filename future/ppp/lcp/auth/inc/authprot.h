/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: authprot.h,v 1.3 2014/10/30 12:31:52 siva Exp $
 *
 * Description:This file contains prototypes for the authentication
 * functions used internally by the authentication module
 *
 *******************************************************************/
#ifndef __PPP_AUTHPROT_H__
#define __PPP_AUTHPROT_H__

#include "authsnmp.h"

#ifdef RADIUS_WANTED

INT4 PppRadPapAuthenticate( UINT1 *, UINT1 , UINT1 *,UINT1 , UINT4 );
INT4 PppRadCHAPAuthenticate(UINT1 *,UINT1 ,UINT1 *,UINT1 ,UINT1 , UINT1 *,UINT1,UINT4 );
INT4 PppRadMSCHAPAuthenticate(UINT1 *,UINT1 ,UINT1,UINT1 *,UINT1,UINT1 *,UINT1 *,UINT4);
INT4 PppRadEapAuthenticate (UINT1 *, UINT1, UINT1 *, UINT2, UINT4, UINT1 *, UINT1);

#endif /* RADIUS_WANTED */
/* Functions of AUTHENTICATION Module */
VOID     PPPGetRandom (UINT1 *pu1Ptr, UINT4 u4Len);
VOID     GetAUTHParameters(tAUTHIf *pAuthPtr);
VOID     PrintAUTHOptions(tAUTHIf *pAuthPtr);

VOID     AUTHInit(tAUTHIf *pAuthPtr);
VOID     AuthInitServices ( tPPPIf *pIf );
VOID     AUTHStartServer(tAUTHIf *pAuthPtr);
VOID     AUTHStartClient(tAUTHIf *pAuthPtr);
VOID     AUTHCloseServer(tAUTHIf *pAuthPtr);
VOID     AUTHCloseClient(tAUTHIf *pAuthPtr);
tSecret *AUTHGetSecret(tAUTHIf *pAuthPtr, UINT1    *pName, UINT2     Length, UINT2     Protocol, UINT1    Type);
tSecret *MSCHAPAUTHGetSecret(tAUTHIf *pAuthPtr, UINT1 *pName, UINT2     Protocol, UINT1    Type);
tSecret *AUTHGetSecretName(tAUTHIf *pAuthPtr, UINT2     Protocol, UINT1    Type);
INT1     AUTHInsertPrefEntry(tSecurityInfo *, tAuthPref NewEntry);
VOID     AUTHInsertSecret(tSecurityInfo *, tSecret *pNew);
VOID     AUTHDeletePrefEntry(tSecurityInfo *, UINT2     Protocol, UINT1 ChapAlgo);
VOID     AUTHDeleteSecretEntry(tSecurityInfo *, tSecret *pToDel);
VOID AUTHNewSecretUpdateCounters (tSecurityInfo *pSecurityInfo, tSecret *pNew);

/* Functions of PAP sub module  */
VOID PAPClientInit(tAUTHIf *pAuthPtr);
VOID PAPServerInit(tAUTHIf *pAuthPtr);
VOID PAPTimeOut(VOID *pAuthPtr);
VOID PAPInput(tAUTHIf *pAuthPtr, t_MSG_DESC *pInpacket, UINT2 Length);
VOID PAPRcvdAuthAck(tAUTHIf *pAuthPtr, t_MSG_DESC *pInpacket, UINT2 Length, 
                    UINT1 Id);
VOID PAPRcvdAuthNak(tAUTHIf *pAuthPtr, t_MSG_DESC *pInpacket, UINT2 Length, 
                    UINT1 Identifier);
VOID PAPSendAuthReq(tAUTHIf *pAuthPtr, UINT1 ReTxFlag);
VOID     PAPAuthwithPeer(tAUTHIf *pPapPtr);
VOID     PAPAuthPeer(tAUTHIf *pPapPtr);
VOID     PAPRecdProtRej(tAUTHIf *);
VOID     PAPRcvdAuthReq(tAUTHIf *pPapPtr, t_MSG_DESC *pInpacket, UINT2     Length, UINT1    Id);
UINT1    CheckPasswd(tAUTHIf *pPapPtr, UINT1    *pPeerId, UINT1    PeerIdLen, UINT1    *pPasswd, UINT1    PasswdLen);





/*  Functions of CHAP sub module */
VOID CHAPClientInit(tAUTHIf *pAuthPtr);
VOID CHAPServerInit(tAUTHIf *pAuthPtr);
VOID CHAPChalTimeOut(VOID *pAuthPtr);
VOID CHAPRespTimeOut(VOID *pAuthPtr);
VOID CHAPReChalTimeOut(VOID *pAuthPtr);


VOID     CHAPAuthwithPeer(tAUTHIf *pChapPtr);
VOID     CHAPAuthPeer(tAUTHIf *pChapPtr);
VOID     CHAPReChallenge(tAUTHIf *pChapPtr);
VOID     CHAPRecdProtRej(tAUTHIf *);
VOID     CHAPInput(tAUTHIf *pChapPtr, t_MSG_DESC *pInpacket, UINT2     Length);
VOID     CHAPRcvdChallenge(tAUTHIf *pChapPtr, t_MSG_DESC *pInpacket, UINT2     Length, UINT1    Identifier);
VOID     CHAPRcvdAuthResponse(tAUTHIf *pChapPtr, t_MSG_DESC *pInpacket, UINT2     Length, UINT1    Identifier);
VOID     CHAPRcvdAuthSuccess(tAUTHIf *pChapPtr, t_MSG_DESC *pInpacket, UINT2     Length, UINT1    Identifier);
VOID     CHAPRcvdAuthFailure(tAUTHIf *pChapPtr, t_MSG_DESC *pInpacket, UINT2     Length, UINT1    Identifer);
VOID     CHAPGenChallenge(tAUTHIf *pChapPtr);
VOID     CHAPSendResponse(tAUTHIf *pChapPtr);
VOID     CHAPSendStatus(tAUTHIf *pChapPtr, UINT1    Code);
VOID     CHAPSendChallenge(tAUTHIf *pChapPtr);

/*  Functions of MSCHAP sub module */

VOID     MSCHAPAuthPeer(tAUTHIf *pMsChapPtr);
VOID     MSCHAPGenChallenge(tAUTHIf *pMsChapPtr);
VOID     MSCHAPSendResponse(tAUTHIf *pMsChapPtr);
VOID     MSCHAPSendStatus(tAUTHIf *pMsChapPtr, UINT1 Code,const UINT1 *ErrorCode);
VOID     MSCHAPSendChallenge(tAUTHIf *pMsChapPtr);

VOID     MSCHAPRcvdChallenge(tAUTHIf *pMsChapPtr, t_MSG_DESC *pInpacket, UINT2 Length, UINT1    Identifier);
VOID     MSCHAPRcvdAuthResponse(tAUTHIf *pMsChapPtr, t_MSG_DESC *pInpacket, UINT2    Length, UINT1  Identifier);
VOID     MSCHAPRcvdAuthSuccess(tAUTHIf *pMsChapPtr, t_MSG_DESC *pInpacket, UINT2     Length, UINT1  Identifier);
VOID     MSCHAPRcvdAuthFailure(tAUTHIf *pMsChapPtr, t_MSG_DESC *pInpacket, UINT2     Length, UINT1  Identifer);


VOID EAPClientInit(tAUTHIf *pAuthPtr);
VOID EAPServerInit(tAUTHIf *pAuthPtr);
VOID EAPAuthWithPeer(tAUTHIf *pAuthPtr);
VOID EAPAuthPeer(tAUTHIf *pAuthPtr);
VOID EAPIdentityTimeOut(VOID *pAuthPtr);
VOID EAPRecdProtRej(tAUTHIf *pAuthPtr);

VOID EAPInput(tAUTHIf *pAuthPtr, t_MSG_DESC *pInpacket, UINT2 Length);
VOID EAPRcvdAuthRequest(tAUTHIf *pAuthPtr, t_MSG_DESC *pInpacket, UINT2 Length, UINT1 Identifier);
VOID EAPRcvdAuthResponse(tAUTHIf *pAuthPtr, t_MSG_DESC *pInpacket, UINT2 Length, UINT1 Identifier);
VOID EAPRcvdAuthSuccess(tAUTHIf *pAuthPtr, t_MSG_DESC *pInpacket, UINT2 Length, UINT1 Identifier);
VOID EAPRcvdAuthFailure(tAUTHIf *pAuthPtr, t_MSG_DESC *pInpacket, UINT2 Length, UINT1 Identifier);

VOID EAPGenIdentityRequest(tAUTHIf *pAuthPtr);
VOID EAPSendIdentityRequest(tAUTHIf *pAuthPtr);
VOID EAPSendStatus(tAUTHIf *pAuthPtr, UINT1 Code);

VOID EAPProcessIdentityRequest(tAUTHIf *pAuthPtr, t_MSG_DESC *pInpacket, UINT2 Length, UINT1 Identifier);
VOID EAPProcessNotificationRequest(tAUTHIf *pAuthPtr, t_MSG_DESC *pInpacket, UINT2 Length, UINT1 Identifier);
VOID EAPProcessIdentityResponse(tAUTHIf *pAuthPtr, t_MSG_DESC *pInpacket, UINT2 Length, UINT1 Identifier);
VOID EAPProcessNotificationResponse(tAUTHIf *pAuthPtr, t_MSG_DESC *pInpacket, UINT2 Length, UINT1 Identifier);
VOID EAPProcessNakResponse(tAUTHIf *pAuthPtr, t_MSG_DESC *pInpacket, UINT2 Length, UINT1 Identifier);



VOID     EAPMD5RcvdAuthResponse(tAUTHIf *pAuthPtr, t_MSG_DESC *pInpacket, UINT2     Length, UINT1    Identifier);


VOID EAPMD5ClientInit(tAUTHIf *pAuthPtr);
VOID EAPMD5ServerInit(tAUTHIf *pAuthPtr);
VOID EAPMD5ChalTimeOut(VOID *pAuthPtr);
VOID EAPMD5ReChalTimeOut(VOID *pAuthPtr);
VOID EAPMD5RcvdChallenge(tAUTHIf *pAuthPtr, t_MSG_DESC *pInpacket, UINT2 Length,
       UINT1 Identifier);
VOID EAPMD5RcvdResponse(tAUTHIf *pAuthPtr, t_MSG_DESC *pInpacket, UINT2 Length,
      UINT1 Identifier);
VOID EAPMD5GenChallenge(tAUTHIf *pAuthPtr);
VOID EAPMD5SendChallenge(tAUTHIf *pAuthPtr);




VOID    AUTHStopAuthentication(tAUTHIf *pAuthPtr);
VOID    AUTHStartAuthentication(tAUTHIf *pAuthPtr);

VOID   AUTHDeleteInvalidEntries(tPPPIf *p);

tSecret *AUTHSearchSecretName(tAUTHIf *pAuthPtr, tSecurityInfo *pSecurityInfo, UINT2 Protocol, UINT1 Type,  INT4  *pLastIdIndex);

VOID AUTHInitSecurityInfo(tSecurityInfo     *pSecurityInfo);


tSecret * SNMPGetSecret(UINT4 Index1, UINT4 pref);
tAuthPref* SNMPGetAUTHPrefPtr(UINT4 Index, UINT4 Pref);
INT4 SNMPInsertAUTHPrefEntry(UINT4 IfIndex, UINT4 pref, UINT4 proto);
INT4 SNMPDetectAUTHPrefEntry(UINT4 IfIndex, UINT4 pref, UINT4 proto, UINT1 ChapAlgo);
VOID SNMPDeleteAuthPref(UINT4 IfIndex, UINT4 pref);
VOID SNMPModifySecret( tSecret *secret, tSecurityInfo  *pSecurityInfo, t_SECRET_DEPENDENCY *secrets_dependency);
VOID SNMPSetSecuritySecret( UINT4 IfIndex, UINT4 IdIndex, t_SECRET_DEPENDENCY *secrets_dependency);
VOID SNMPInsertSecret( tSecurityInfo *pSecurityInfo, t_SECRET_DEPENDENCY *secrets_dependency);
VOID AuthFillSecretDependency (UINT4 LinkIndex, UINT4 IdIndex);
VOID ConfigForAuthPeer (UINT2 u2IfIndex);


VOID SNMPDeleteSecretEntry (UINT4 Link, UINT4 IdIndex);
INT1 DetectSecretsDependency(UINT4 IfIndex, UINT4 IdIndex, UINT2 proto);

VOID MSChapLmPasswordHash (UINT1 * pPassword, UINT1 * pPasswordHash, UINT1 Length);
VOID MSChapNtPasswordHash (UINT2 * pPassword, UINT1 * pPasswordHash, UINT1 Length);

#endif  /* __PPP_AUTHPROT_H__ */
