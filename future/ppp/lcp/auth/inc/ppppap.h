/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppppap.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the data structures declarations 
 *             for the PAP sub module.
 *
 *******************************************************************/
#ifndef __PPP_PPPPAP_H__
#define __PPP_PPPPAP_H__

/*****************  CONSTANTS USED IN THE PAP SUBMODULE **************/

#define   PAP_CLIENT_STATE_INITIAL  0
#define   PAP_CLIENT_STATE_CLOSED   1
#define   PAP_CLIENT_STATE_PENDING  2
#define   PAP_CLIENT_STATE_AUTHREQ  3
#define   PAP_CLIENT_STATE_OPEN     4
#define   PAP_CLIENT_STATE_BADAUTH  5

#define   PAP_SERVER_STATE_INITIAL  0
#define   PAP_SERVER_STATE_CLOSED   1
#define   PAP_SERVER_STATE_PENDING  2
#define   PAP_SERVER_STATE_LISTEN   3
#define   PAP_SERVER_STATE_OPEN     4
#define   PAP_SERVER_STATE_BADAUTH  5

#define    PAP_AUTH_REQ             1
#define    PAP_AUTH_ACK             2
#define    PAP_AUTH_NAK             3
#define    PAP_DEFTIMEOUT          10
#define    PAP_DEF_TRANSMITS       10
#define    PEER_ID_FIELD_LEN        1
#define    PASSWD_FIELD_LEN         1
#define    MSG_LEN_FIELD_LEN        1
/**********************************************************************/

typedef struct {
	UINT1       State;          /* Client state */
	UINT1       UserId;         /* Current id */
	UINT2       TimeoutTime;    /* Timeout time in milliseconds */
	UINT2       TransmitCount;  /* Number of auth-reqs sent */
	UINT2       MaxTransmits;   /* Maximum number of auth-reqs to send */
	tSecret    *pPAPCurrentSecret;
	tPPPTimer   PapResendTimer;
} tPAPClientInfo;

typedef struct {
	tPPPTimer   PapResponseTimer;
	UINT1       State;          /* Server state */
	UINT1	    u1Rsvd1;
	UINT2	    u2Rsvd2;
} tPAPServerInfo;


#endif  /* __PPP_PPPPAP_H__ */
