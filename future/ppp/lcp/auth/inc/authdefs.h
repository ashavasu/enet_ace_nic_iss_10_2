/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: authdefs.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains macros of AUTHENTICATION Module.
 *
 *******************************************************************/
#ifndef __PPP_AUTHDEFS_H__
#define __PPP_AUTHDEFS_H__


#define  CHAP_PEER_TRUE           0xFE 
#define  CHAP_WITH_PEER_TRUE      0xFD 
#define  PAP_PEER_TRUE            0xFB 
#define  PAP_WITH_PEER_TRUE       0xF7 
#define  MSCHAP_PEER_TRUE         0xEF 
#define  MSCHAP_WITH_PEER_TRUE    0xDF 

#define  EAP_PEER_TRUE            0xBF 
#define  EAP_WITH_PEER_TRUE       0x7F 
#define AUTH_CHAP_OPT_LEN    5
#define AUTH_PAP_OPT_LEN    4

#define AUTH_EAP_OPT_LEN        4


#define  CHAP_PEER                1    
#define  CHAP_WITH_PEER           2    
#define  PAP_PEER                 4    
#define  PAP_WITH_PEER            8    
#define  MSCHAP_PEER              0x10
#define  MSCHAP_WITH_PEER         0x20

#define  EAP_PEER                 0x40
#define  EAP_WITH_PEER            0x80

          
#define  CHAP_SECRET_AVAILABLE    1    
#define  PAP_SECRET_AVAILABLE     2    
#define  MSCHAP_SECRET_AVAILABLE  4    

#define  IF_SPECIFIC_LIST         1    
#define  GLOBAL_LIST              2    

#endif  /* __PPP_AUTHDEFS_H__ */
