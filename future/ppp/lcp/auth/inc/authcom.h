/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: authcom.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains common authentication files
 *
 *******************************************************************/
#ifndef __PPP_AUTHCOM_H__
#define __PPP_AUTHCOM_H__

#include "genhdrs.h"

/* Global PPP includes */
#include "pppmacro.h"
#include "pppdefs.h"
#include "pppdebug.h"
#include "ppptimer.h"
#include "pppdbgex.h"


/* Includes from other modules */
#include "frmtdfs.h"
#include "pppgsem.h"
#include "pppgcp.h"
#include "ppplcp.h"

#include "pppexts.h"


/* Local includes */
#include "authdefs.h"
#include "pppchap.h"
#include "ppppap.h"
#include "pppmchap.h"
#include "pppeap.h"
#include "pppmd5.h"
#include "pppmd4.h"
#include "pppauth.h"
#include "authprot.h"




#endif  /* __PPP_AUTHCOM_H__ */
