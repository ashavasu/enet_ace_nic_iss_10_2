/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lcpdefs.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the definitions used by the LCP
 * module
 *
 *******************************************************************/
#ifndef __PPP_LCPDEFS_H__
#define __PPP_LCPDEFS_H__

#define   PPP_LCP_DOWN_LOCK()   PppLcpDownLock()
#define   PPP_LCP_DOWN_UNLOCK() PppLcpDownUnlock()
/*  code field values applicable for LCP,IPCP and IPXCP packets  */
#define  CONF_REQ_CODE                 0x01  
#define  CONF_ACK_CODE                 0x02  
#define  CONF_NAK_CODE                 0x03  
#define  CONF_REJ_CODE                 0x04  
#define  TERM_REQ_CODE                 0x05  
#define  TERM_ACK_CODE                 0x06  
#define  CODE_REJ_CODE                 0x07  
#define  PROT_REJ_CODE                 0x08  
#define  ECHO_REQ_CODE                 0x09  
#define  ECHO_REPLY_CODE               0x0a  
#define  DISCARD_REQ_CODE              0x0b  
#define  IDENTIFICATION_CODE           0x0c  
#define  TIME_REM_CODE                 0x0d  

#define  ALL_STATIONS                  0xff  

#define  LINK_NOT_AVAILABLE            1     
#define  LINK_SUSPENDED                2     
#define  LINK_OPENED                   3     
#define  LINK_AVAILABLE                4     

#define  QUALITY_NOT_DETERMINED        3     

#define  PPP_LOCAL                 1
#define  PPP_RADIUS                2




#define  NO_FCS_REQ                    1     
#define  FCS_16_REQ                    2     
#define  FCS_32_REQ                    4     
#define  FCS_48_REQ                    6     
#define  FCS_16_AVAILABLE              1     
#define  FCS_32_AVAILABLE              2     
#define  FCS_48_AVAILABLE              4     
#define  ASYNC_IF                      1     

#define  TX_ACCM_SIZE                  32    
#define  RX_ACCM_SIZE                  4     

#define  AC_FIELD_COMPRESSED           1     
#define  PF_FIELD_COMPRESSED           2     


#define  PPP_TYPE                      1     
#define  MP_TYPE                       2     

#define  MIN_SPEED_FOR_COMP            19200 

#define  HDLC_ADDR_LEN                 2     
#define  AC_COMPRESSED                 2     
#define  AC_NOT_COMPRESSED             3     
#define  UNCOMPRESSED_PID_LEN          2     
#define  COMPRESSED_PID_LEN            1     


#define  CALLED_FROM_LLI               1     
#define  CALLED_FROM_MP                2     

#define  PROCESSED                     1     
#define  NOT_PROCESSED                 2     

#define  MUST                          1     
#define  NOT_MUST                      2     

#define  ENCAPSULATED                  0     
#define  SINGLE_LINK                   1     

/* IPv6CP Changes -- start */
#define MAX_NCP                        6 /* Incremented by one 
                                            for IPv6CP*/
/* IPv6CP Changes -- End */

#define  ALL_AUTH_OVER                 0     

#define  IMMEDIATE                     0     
#define  DELAYED                       1     

#define  LCP_INTERNATION_OPT_ENABLED   1     
#define  LCP_INTERNATION_OPT_DISABLED  2     

#define  PPP_STATUS_FREE_MASK          0x01 /* 00000001 */
#define  PPP_STATUS_NOT_FREE_MASK      0xFE /* 11111110 */
#define  PPP_FLOATING_MASK             0x02 /* 00000010 */

#define  PPP_DEF_ECHO_PKT_LEN          32

#define  KEEPALIVE_ECHO_PKTSIZE        200

#define  DATA_PKT_MASK                 0xff00
#define  OPT_LEN_OFFSET                1

#endif  /* __PPP_LCPDEFS_H__ */
