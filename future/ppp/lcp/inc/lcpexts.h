/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lcpexts.h,v 1.2 2011/10/13 10:31:32 siva Exp $
 *
 * Description:This file contains the definitions of the LCP used
 * by the external modules
 *
 *******************************************************************/
#ifndef __PPP_LCPEXTS_H__
#define __PPP_LCPEXTS_H__

extern UINT1       FCSAvailability;

extern tOptNegFlagsPerIf    LCPOptPerIntf[];
extern tGSEMCallbacks     LcpGSEMCallback;

extern UINT2 LqmFreeCounter;
#endif  /* __PPP_LCPEXTS_H__ */
