/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lcpprot.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description: This file contains Prototypes for LCP functions 
 *  used internally by LCP module
 *
 *******************************************************************/
#ifndef __PPP_LCPPROT_H__
#define __PPP_LCPPROT_H__

/* Prototypes for LCP functions used internally by LCP module. */

INT1 LCPInit(tPPPIf *pIf);
INT1 LCPReInit(tPPPIf *pIf);
VOID LCPOptionInit(tLCPOptions *pLCPOpt);
VOID LCPEnableCompressions(tPPPIf *pIf);
INT4 LCPInitForIfType(tPPPIf *);
INT4 PppLcpDownLock(VOID);
INT4 PppLcpDownUnlock(VOID);
VOID LCPUp(tGSEM *pGSEM);
VOID LCPDown(tGSEM *pGSEM);
VOID LCPStarting(tGSEM *pGSEM);
VOID LCPFinished(tGSEM *pGSEM);
INT1 LCPCopyOptions(tGSEM *pGSEM, UINT1 Flag, UINT1 PktType);

INT1 LCPRecdProtRej(tGSEM *pGSEM, t_MSG_DESC *pInPkt, UINT2 Length, UINT1 Identifier);
INT1 LCPRecdRxrReqPkt(tGSEM *pGSEM, t_MSG_DESC *pInPkt, UINT2 Length, UINT1 Identifier);
INT1 LCPRecdEchoReply(tGSEM *pGSEM, t_MSG_DESC *pInPkt, UINT2 Length, UINT1 Identifier);

INT1 LCPProcessMRUConfReq( tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal, UINT2 Offset, UINT1 PresenceFlag);
INT1 LCPProcessPaddingConfReq( tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal, UINT2 Offset, UINT1 PresenceFlag);
INT1 LCPProcessACCMConfReq( tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal, UINT2 Offset, UINT1 PresenceFlag);
INT1 LCPProcessAUTHConfReq( tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal, UINT2 Offset, UINT1 PresenceFlag);
INT1 LCPProcessMagicConfReq( tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal, UINT2 Offset, UINT1 PresenceFlag);
INT1 LCPProcessLQRConfReq( tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal, UINT2 Offset, UINT1 PresenceFlag);
INT1 LCPProcessFCSConfReq( tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal, UINT2 Offset, UINT1 PresenceFlag);
INT1 LCPProcessCallBackConfReq( tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal, UINT2 Offset, UINT1 PresenceFlag);
INT1 LCPProcessLinkDiscrConfReq( tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal, UINT2 Offset, UINT1 PresenceFlag);

INT1 LCPProcessMRUConfNak( tGSEM *pGSEM, t_MSG_DESC *pInPkt,tOptVal OptVal);
INT1 LCPProcessACCMConfNak( tGSEM *pGSEM, t_MSG_DESC *pInPkt,tOptVal OptVal);
INT1 LCPProcessAUTHConfNak( tGSEM *pGSEM, t_MSG_DESC *pInPkt,tOptVal OptVal);
INT1 LCPProcessLQRConfNak( tGSEM *pGSEM, t_MSG_DESC *pInPkt,tOptVal OptVal);
INT1 LCPProcessMagicConfNak( tGSEM *pGSEM, t_MSG_DESC *pInPkt,tOptVal OptVal);
INT1 LCPProcessFCSConfNak( tGSEM *pGSEM, t_MSG_DESC *pInPkt,tOptVal OptVal);
INT1 LCPProcessPaddingConfNak( tGSEM *pGSEM, t_MSG_DESC *pInPkt,tOptVal OptVal);
INT1 LCPProcessCallBackConfNak(tGSEM *pGSEM, t_MSG_DESC *pInPkt,tOptVal OptVal);
INT1 LCPProcessLinkDiscrConfNak(tGSEM *pGSEM, t_MSG_DESC *pInPkt,tOptVal OptVal);
INT1 LCPAddAuthSubOpt(tGSEM *pGSEM, tOptVal OptVal, t_MSG_DESC *pOutBuf );
INT1 LCPAddLQRSubOpt(tGSEM *pGSEM, tOptVal OptVal, t_MSG_DESC *pOutBuf );
INT1 LCPAddCallBackSubOpt( tGSEM *pGSEM, tOptVal OptVal, t_MSG_DESC *pOutBuf );

VOID *LCPReturnMRUPtr(tGSEM  *pGSEM, UINT1 *OptLen);
VOID *LCPReturnPaddingPtr(tGSEM  *pGSEM, UINT1 *OptLen);
VOID *LCPReturnACCMPtr(tGSEM  *pGSEM, UINT1 *OptLen);
VOID *LCPReturnMagicPtr(tGSEM  *pGSEM, UINT1 *OptLen);
VOID *LCPReturnAUTHPtr(tGSEM  *pGSEM, UINT1 *OptLen);
VOID *LCPReturnLQRPtr(tGSEM  *pGSEM, UINT1 *OptLen);
VOID *LCPReturnFCSPtr(tGSEM  *pGSEM, UINT1 *OptLen);
VOID *LCPReturnCallBackPtr(tGSEM  *pGSEM, UINT1 *OptLen);
VOID *LCPReturnLinkDiscrPtr(tGSEM  *pGSEM, UINT1 *OptLen);

INT1 LCPProcessMRUConfRej ( tGSEM * pGSEM );
INT1 LCPProcessPaddingConfRej ( tGSEM * pGSEM );
INT1 LCPProcessACCMConfRej ( tGSEM * pGSEM );
INT1 LCPProcessFCSConfRej ( tGSEM * pGSEM );


VOID PPPResetVariables(tPPPIf *pIf);
VOID LCPSendDiscardReq(tPPPIf *pIf);
VOID LCPSendIdPkt(tPPPIf *pIf);
VOID LCPSendRXRPkt(tPPPIf *pIf,UINT1 Code, UINT1 Identifier);

UINT1 BundleSuspendStatus(tPPPIf *pBundleIf, UINT1 LinkAvailStatus);

INT1 LCPProcessInternationConfReq( tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal, UINT2 Offset, UINT1 PresenceFlag);
INT1 LCPProcessInternationConfNak( tGSEM *pGSEM, t_MSG_DESC *pInPkt,tOptVal OptVal);
VOID *LCPReturnInternationPtr(tGSEM  *pGSEM, UINT1 *OptLen);
INT1 LCPAddInternationSubOpt(tGSEM *pGSEM, tOptVal OptVal, t_MSG_DESC *pOutBuf);
#ifdef RADIUS_WANTED

INT4  PppRadAccounting(tPPPIf   *, UINT1   );
#endif

/* NAETRA_ADD for AutoRedial */
extern VOID CliRedial(VOID);
extern INT4 CfaCheckIfDialInterfaceEnabled(UINT4);
extern INT1 nmhGetIfWanAutoReconnect(INT4 ,INT4 *);

#endif  /* __PPP_LCPPROT_H__ */
