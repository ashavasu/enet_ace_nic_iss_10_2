/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lcpcom.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the common LCP files
 *
 *******************************************************************/
#ifndef __PPP_LCPCOM_H__
#define __PPP_LCPCOM_H__

#include "genhdrs.h"

/* Global PPP includes */
#include "pppmacro.h"
#include "pppdefs.h"
#include "pppdebug.h"
#include "ppptimer.h"
#include "pppdbgex.h"

/* Includes from other modules */
#include "frmtdfs.h"
#include "pppgsem.h"
#include "pppgcp.h"

/* Local includes */
#include "lcpdefs.h"
#include "ppplcp.h"

#include "pppexts.h"

/* Includes from other modules which depend on LCP */
#include "ppplqm.h"
#include "pppchap.h"
#include "pppmchap.h"
#include "ppppap.h"
#include "pppeap.h"
#include "pppauth.h"

/* LCP Prototypes */
#include "lcpprot.h"

#endif  /* __PPP_LCPCOM_H__ */
