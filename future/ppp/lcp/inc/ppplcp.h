/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppplcp.h,v 1.7 2014/12/16 10:49:13 siva Exp $
 *
 * Description:This files contains constants and variables used only
 *             by the LCP module
 *
 *******************************************************************/
#ifndef __PPP_PPPLCP_H__
#define __PPP_PPPLCP_H__

/*********************************************************************/
/*          Constants Used only by the LCP Module                    */
/*********************************************************************/


#define  PLAIN_LINK                0x0
#define  DATA_OPEN                 1
#define  DATA_CLOSE                0


#define  DEF_MRU_SIZE              1500
/* This is the max. MTU required for tagged ethernet packets
 * plus the PPP Bridge header */ 
#define  PPP_BCP_DEF_MRU_SIZE       1522 

#define  AAA_LOCAL                 1
#define  AAA_RADIUS                2
#define  AAA_TACCAS                3
#define  AAA_OTHERS                4
#define  PPP_ACCT_START            0
#define  PPP_ACCT_STOP             1

#ifdef SIZING_WANTED
#define  DEF_MAX_NO_OF_INTERFACES  gSystemSize.CfaSystemSize.u4SystemMaxIface
#else
#define  DEF_MAX_NO_OF_INTERFACES         100
#endif
#define  MIN_MRU_SIZE                     128

#define  MIN_EPD_OPT_LEN                  3
#define  DEF_FCS_SIZE                     16
#define  ALT_LQR_PERIOD                   1000
#define  DEF_MAX_ECHO_RETRANSMITS         2
#define  DEF_MAX_RETXMISSION              2
#define  DEF_PAD_VALUE                    255
#define  DEF_ASYNC_MAP                    0xFFFFFFFF
#define  PPP_MAX_NAME_LENGTH                  32

/*   CONFIGURATION   OPTIONS   TYPE   VALUES      for   LCP   packets   */
#define  MRU_OPTION                       1
#define  ACCM_OPTION                      2
#define  AUTH_OPTION                      3
#define  LQR_OPTION                       4
#define  MAGIC_OPTION                     5
#define  PFC_OPTION                       7
#define  ACFC_OPTION                      8
#define  FCS_OPTION                       9
#define  PADDING_OPTION                   10
#define  CALL_BACK_OPTION                 13
#ifdef BAP
#define  LINK_DISCR_OPTION                23
#endif

#define  INTERNATION_OPTION               28


#define  MAGIC_FIELD_LEN                  4

#define  NO_AUTH_PROTOCOL                 0
#define  DEF_IDLE_TIMEOUT                 30
#define  PPP_DEF_KEEPALIVE_TIME           10
#define  DEF_TIMEOUT_TIME                 3
#define  DEF_MAX_RETX_PKT                 10
#define  DEF_MAX_MAGIC_NAK_LOOPS          5
#define  DEF_ECHO_CHAR                    0x41

#define  MAX_LQM_DISCRETE_VALUES          1

#define  MAX_AUTH_DISCRETE_VALUES         3

#define  MAX_FCS_DISCRETE_VALUES          4
#define  MAX_CALL_BACK_DISCRETE_VALUES    7


#define  MAX_EPD_DISCRETE_VALUES          6
#define  MAX_ADDR_SIZE                    20

#define  MAX_INTERNATION_DISCRETE_VALUES  1



#ifdef BAP
#define  MAX_LCP_OPT_TYPES                15
#else
#define  MAX_LCP_OPT_TYPES                14
#endif

#define  MAX_BUNDLE_OPTS                  3


#define  MRRU_IDX                         0
#define  SEQ_IDX                          1
#define  EPD_IDX                          2
#define  MRU_IDX                          3

#define  ACCM_IDX                         MRU_IDX + 1
#define  AUTH_IDX                         MRU_IDX + 2
#define  LQR_IDX                          MRU_IDX + 3
#define  MAGIC_IDX                        MRU_IDX + 4
#define  PFC_IDX                          MRU_IDX + 5
#define  ACFC_IDX                         MRU_IDX + 6
#define  FCS_IDX                          MRU_IDX + 7
#define  PADDING_IDX                      MRU_IDX + 8
#define  CALL_BACK_IDX                    MRU_IDX + 9

#ifdef BAP
#define  LINK_DISCR_IDX                   MRU_IDX + 10
#endif
#define  INTERNATION_IDX                  MRU_IDX + 11


#define  RESET_AUTH_ACKED_BY_PEER         0xAA
#define  RESET_AUTH_ACKED_BY_LOCAL        0x55


#define  CALL_BACK_ACKED                  0x01
#define  CALL_BACK_NOT_ACKED              0xfe
#define  CALL_BACK_REQUESTED              0x02
#define  CALL_BACK_NOT_REQUESTED          0xfd

#define MAX_STATE_LEN                      255

#define  PPP_MAX_CLASS_NUMBERS                16
#define  PPP_MP_PREFIX_MAX_LEN                12
/********************************************************************/
/*TYPEDEFS  USED   BY   THE   LLI   MODULE    */
/*********************************************************************/
#ifdef RADIUS_WANTED
typedef struct pppradinfo{
    UINT1    *Username;          /* UserName of the peer               */
    UINT1    *Class;             /* Class attribute reyrned by server  */
    UINT1    *pu1SessionId;
    INT1    RadReqId;           /* id returned by the radius API      */
    UINT1    PppAuthId;          /* authentication req. id of the peer */
    UINT1    ChapState;          /* Iinital or rechallenging           */
    UINT1    u1EapStateLen;
    UINT1    EapState[MAX_STATE_LEN];  /* Radius Server State          */
    UINT1    AcctNeeded;         /* ????                               */
    UINT4    AcctTerminateCause; /* ????                               */
}tPppRadInfo;
/* definitions of Terminate cause constants */

#define  PPP_RAD_USER_REQ      1
#define  PPP_RAD_LOST_CARRIER  2
#define  PPP_RAD_IDLE_TIMEOUT  4
#define  PPP_RAD_ADMIN_RESET   6
#define  PPP_RAD_CALLBACK      16

#endif /* RADIUS_WANTED */

typedef struct counter {
   UINT4             LastChangeTicks;
#ifdef RADIUS_WANTED
   UINT4             SessionStartTicks;
#endif
   UINT4             InOctets;
   UINT4             InNUniPackets;
   UINT4             InDiscards;
   UINT4             InErrors;
   UINT4             InUnknownProtos;
   UINT4             InLQRs;
   UINT4             InGoodOctets;
   UINT4             OutLQRs;
   UINT4             OutOctets;
   UINT4             OutNUniPackets;
   UINT4             OutDiscards;
   UINT4             BadAddressCounter;
   UINT4             BadControlCounter;
   UINT4             PacketTooLongCounter;
   UINT4             BadFcsCounter;
}tLLHCounters;

typedef struct {
    UINT1          CompSlotId;
    UINT1          MaxSlotId;
    UINT2          u2Rsvd;
} tVJInfo;

/* For IPHC */

#define  DEFAULT_IPHC_TCP_SPACE        15  
#define  DEFAULT_IPHC_NON_TCP_SPACE    15  
#define  DEFAULT_IPHC_FULL_MAX_PERIOD  256 
#define  DEFAULT_IPHC_FULL_MAX_TIME    5   
#define  DEFAULT_IPHC_MAX_HEADER       168 

#define  MAX_IPHC_TCP_SPACE            255 
#define  MAX_IPHC_NON_TCP_SPACE        65535 
#define  RTP_SUBOPT_TYPE               1
#define  RTP_SUBOPT_TYPE_LEN           2

#define  MIN_IPHC_OPT_LEN              14  
#define  MIN_IPHC_SUBOPT_LEN           10  



typedef struct {
    UINT2          u2TcpSpace;
    UINT2          u2NonTcpSpace;
    UINT2          u2FMaxPeriod;
    UINT2          u2FMaxTime;
    UINT2          u2MaxHdr;
    UINT2          u2RtpSubOptFlag;
} tIPHCInfo;




/*********************************************************************/
/* Typedefs   Used   by   the   LCP   Module   */
/*********************************************************************/

typedef struct {
    UINT1    CallBackOperation;
    UINT1    u1Rsvd;
    UINT2    CallBackMsgLen;
    UINT1    *pCallBackMsg;
} tLCPCallBackInfo;

typedef struct {
    UINT1        CallBackNegFlags;
    UINT1        u1Rsvd1;
    UINT2        u2Rsvd2;
    UINT2        TimeBeforeDisconnect;
    UINT2        TimeToDialAfterDisconnect;
    tPPPTimer    CallBackDisconnectTimer;
    tPPPTimer    CallBackDialTimer;
} tLCPCallBackTimeInfo;

#define MIN_LANGUAGE_NAME          1
#define MAX_LANGUAGE_NAME         50

#define MAX_SUPPORTED_LANGUAGES    1

#define  CHAR_SET_UTF8           106

typedef struct {
    UINT4  CharSet;
    UINT1  LangTag[MAX_LANGUAGE_NAME];
    UINT1  LangTagLen;
    UINT1  u1Rsvd;
}tLCPInterOptionInfo;

typedef struct {
   tLCPInterOptionInfo LangList[MAX_SUPPORTED_LANGUAGES];
   UINT1 NumActiveLang;
   UINT1 u1Rsvd1;
   UINT2 u2Rsvd2;
}tLangTable;

typedef struct {
    UINT1                AsyncMap[4];       /* Value   of   async map       */
    UINT4                MagicNumber;       /* Value   of   Magic Number    */
    UINT2                MRU;               /* Value   of   MRU             */
    UINT1                FCSSize;           /* size    of   FCS             */
    UINT1                MaxPadVal;         /* The Maximum padding Value    */
    tLCPCallBackInfo    LCPCallBackInfo;
#ifdef BAP
    UINT2                LinkDiscriminator; /* Value  of  LinkDiscriminator */
    UINT2   u2Rsvd;
#endif
tLCPInterOptionInfo                     LCPInterOptionInfo;
}tLCPOptions;



typedef struct {
   UINT1             EchoId;          /*   Id   for   Echo   */
   UINT1             EchoTransmits;   /*   Counter           */
   UINT1             TestResult;
   UINT1             EchoChar;
   tPPPTimer         EchoTimer;
   tPPPTimer         KeepAliveTimer;
   UINT2             KeepAliveTimeOutVal;
   UINT2             u2Rsvd;
}tECHOInfo;

typedef struct {
   UINT1            AdminStatus;
   UINT1            OperStatus;
   UINT1            LinkQualityStatus;
   UINT1            AuthStatus;
   UINT1            ECPStatus;
   UINT1            CCPEnableFlag;
   UINT1            ECPEnableFlag;
   UINT1            LinkAvailability;

   /* status of the  LCP internationalization option negotiation */
   UINT1             InterOptStatusFlag;
   UINT1             u1Rsvd1;
   UINT2             u2Rsvd2;

}tLCPStatus;

typedef struct {
   UINT1             Length;
   UINT1             Class;
   UINT1             Address[MAX_ADDR_SIZE];
   UINT2             u2Rsvd;
}tBundleId;

#ifdef PPP_STACK_WANTED
typedef struct {
   UINT1  u1PrefixVal[PPP_MP_PREFIX_MAX_LEN]; /* Prefix Elision Value */
   UINT1  u1PrefixLen; /* Prefix length */
}tPrefixElisionInfo;
#endif
typedef struct {
   UINT2             MRRU;
#ifdef PPP_STACK_WANTED
   UINT1             u1MCMLCodeVal;  /*Code value in Multi link header
                                               format option.*/
   UINT1             u1MCMLClassVal;/*No Of Suspendabel classes */

   tPrefixElisionInfo aPrefixElisionInfo[PPP_MAX_CLASS_NUMBERS];
                          /*Prefix Elision info.*/
#else
   UINT2             u2Rsvd;
#endif
   tBundleId         BundleId;
}tBundleOptions;

#ifdef BAP
typedef struct BWMProtocolInfo {
    UINT2            Protocol;
    UINT2            u2Rsvd;
    VOID            *pProtocolInfo;
} tBWMProtocolInfo;
#endif

typedef struct {
   tBundleOptions    BundleOptDesired;
   tBundleOptions    BundleOptAllowedForPeer;
   tBundleOptions    BundleOptAckedByPeer;
   tBundleOptions    BundleOptAckedByLocal;
   tOptNegFlagsPerIf MPFlagsPerIf[MAX_BUNDLE_OPTS];
   UINT1             ECPBundleFlag;
   UINT1             CCPBundleFlag;
   UINT2             u2Rsvd1;

#ifdef BAP
/* Bandwidth Management Protocol(s) related information */
   tBWMProtocolInfo     BWMProtocolInfo;
   UINT1                IsInitiator;
   UINT1                u1Rsvd2;
   UINT2                u2Rsvd3;
/* Bandwidth Management Protocol(s) related information ends here */
#endif

   t_SLL             MemberList;
   UINT4             BundlePktLen;
   INT4              TxSeqNum;
   INT4              Begin1SeqNum;
   INT4              EndSeqNum;
   INT4              Begin2SeqNum;
   t_SLL             RcvdBuffer;
   UINT4             ReassemblyTimeOutVal;
   UINT4             NullFragTimeOutVal;
   UINT4             PeriodicEchoTimeOut;
   tPPPTimer         ReassemblyTimer;
   tPPPTimer         NullFragTimer;
   UINT4             MinThruput;
   UINT4             MaxThruput;
   INT4              LastSeqFwd;
   INT4              LastM;
   UINT4             ReassembleStartTicks;
   UINT4             LastMChangeTicks;
   UINT4             NoOfFragRcvd;
   UINT4             NoOfFragDiscarded;
   UINT1             BundleDirection;
   UINT1             RespRecd;
   UINT1             RespSent;
   UINT1             MPHeaderInitFlag;
#ifdef MP_ALGORITHM_INTERLEAVING
   UINT4             TotalFrag; /* Added by KK for MP - Contains total no. of Fragments to be sent on the bundle */
#endif
}tBundleInfo;

typedef struct {
   t_SLL_NODE        NextMPIf;
   struct   pppif   *pBundlePtr;
   struct   pppif   *pEPDMatchBundleIf;
   struct   pppif   *pAUTHMatchBundleIf;
   tPPPTimer         PeriodicEchoTimer;
   INT4              LastSeqNumRcvd;
   UINT4             TransitDelay;
   UINT4             LastTransitDelay;
   UINT4             EchoTransmitCount;
   UINT4             LastTransitTicks;
   UINT4             CumulativeDelay;
   UINT1             PartOfBundle;
   UINT1      u1Rsvd1;
   UINT2      u2Rsvd2;
}tMemberInfo;

typedef struct {
    t_SLL_NODE       NextFragPtr;
    t_MSG_DESC      *pFragments;
    INT4             SeqNum;
    UINT1            BEFlag;
    UINT1      u1Rsvd1;
    UINT2      u2Rsvd2;
}tMPFrag;

typedef struct {
    UINT4             porttype;
    UINT2             VCNumber;
    UINT1             IfType;
    UINT1             u1Reserved; 
}tPPPIfId;

typedef struct {
   tPPPIfId          IfID;
  tPPPTimer          IdleTimer;
   VOID              *pPppoeSession;
   UINT4             IfIndex;
   UINT4             PhysIfIndex;
   UINT4             StartingTime;
   UINT4             LastPktArrivalTime;
   UINT4             MaxNumLoops;
   UINT4             IfSpeed;
   UINT2             NumLoops;
   UINT2             TimeOutTime;
   UINT2             MaxReTransmits;
   UINT2             ReChalTimeOut;
   UINT1             RxAddrCtrlLen;
   UINT1             RxProtLen;
   UINT1             u1EncapsType;
   UINT1             u1VcType;
   UINT1             Mask;
   UINT1             u1FrmFlag;
   BOOL1             bMRUConfigured;
   UINT1             au1Reserved[1];
#ifdef MP_ALGORITHM_INTERLEAVING
   UINT4             NoOfFrag;
   UINT4             TempNoOfFrag;   /* Temporary frag count on each links     */
   UINT4             ConstFragCount; /* No.Of Frag to be sent on each link     */
   UINT2             LinkIndex;     /* An index given to each of the link for
                                     identification */
   UINT1             au1Rsvd1[2];
#endif /* ~MP_ALGORITHM_INTERLEAVING */
#ifdef PPP_STACK_WANTED
   UINT4             u4TRSessionActiveTime;
   UINT2             IdleTimeOutValue;
   BOOL1             bTROption;
   UINT1             au1Rsvd2[1];
#endif
}tLINKInfo;

typedef struct {
UINT2  RestartTimeOutVal,         /* Timeout time in milliseconds         */
       MaxConfigReqTransmits,     /* Max. Config_Req transmissions        */
       MaxNakLoops,
       MaxTermtransmits;          /* Max Terminate_Req transmissions      */
UINT1  PassiveOption;             /* Passive Option Flag                  */
UINT1  InternationOption;         /* LCP Internationalization Option Flag */
UINT2  EapDefaultAuthType;        /* prefered auth type in EAP            */
UINT2  EapMaxIdentityRetries;
UINT1  EapIdentityNeeded;         /* Identity Req enable/disable flag     */
UINT1  u1Rsvd;
}tLinkConfigs;

#define    MAX_STR_SIZE    20

typedef    struct PPPTestParams {
    tPPPTimer        RecdTimer;
    INT4             SockFD;
    /* PPP communicates through BSD socket interface */

    UINT4            LocIPAddr, RemIPAddr;
    /* Local and remote IP addr for socket interface */

    UINT2    LocUDPPort, RemUDPPort;
    /* Local and remote UDP ports for socket interface */

    UINT1    DNIS [MAX_STR_SIZE], SubAddr [MAX_STR_SIZE], CLID [MAX_STR_SIZE];
} tPPPTestParams;


#define SCRAMBLE_SIZE 43
typedef  struct { 
    UINT1  au1ScrambleRegister[SCRAMBLE_SIZE]; 
    UINT1  au1DescrambleRegister[SCRAMBLE_SIZE];
    UINT1  TxScramble;
    UINT1  RxScramble;
}tSonetIf;

typedef struct pppif {
    t_SLL_NODE       NextPPPIf;
    union   {
          tBundleInfo   BundleInfo;
          tMemberInfo   MemberInfo;
    }MPInfo;
    UINT1             u1RemovedLinkInDP;
    UINT1            BundleFlag;
    UINT1      u1IsCreatedForL2tp;
    UINT1      u1Rsvd1;
    tLinkConfigs     LinkConfigs;
    tLinkConfigs     CurrentLinkConfigs;
    tLCPOptions      LcpOptionsDesired;
    tLCPOptions      LcpOptionsAllowedForPeer;
    tLCPOptions      LcpOptionsAckedByPeer;
    tLCPOptions      LcpOptionsAckedByLocal;
    tLCPStatus       LcpStatus;
    tGSEM            LcpGSEM;
    tLLHCounters     LLHCounters;
    tECHOInfo        EchoInfo;
    tLINKInfo        LinkInfo;
    struct authif   *pAuthPtr;
    struct lqmif    *pLqmPtr;
    UINT1            ExtTxACCM[32];
    UINT1            TermReqFlag;
    UINT1      u1Rsvd3;
    UINT2      u2Rsvd4;
    tAsyncParams     AsyncParams;
    VOID            *pIpcpPtr;
    VOID            *pIp6cpPtr;
    VOID            *pMuxcpPtr;
    VOID            *pMplscpPtr;
    VOID            *pIpxcpPtr;
    VOID            *pBcpPtr;
    VOID            *pCCPIf;
    VOID            *pECPIf;
    tPPPTimer        RecdTimer;
    tLCPCallBackTimeInfo    CallBackTimeInfo;

#ifdef RADIUS_WANTED
tPppRadInfo          RadInfo;
#endif

    tPPPTestParams   Test;
    VOID            *pVjComp;
    UINT1           au1HostName[PPP_MAX_NAME_LENGTH];
    tSonetIf        *pSonetIf;
    UINT1            LastTriedLangIndex;
    UINT1           RowStatus;
    UINT2           u2VlanTci;
}tPPPIf;

typedef  struct {
    UINT4        DiscHeader;
    UINT2        Value[MAX_AUTH_DISCRETE_VALUES];
    UINT2        u2Rsvd;
}tAuthDiscVal;

typedef struct {
    UINT4        DiscHeader;
    UINT2        Value[MAX_LQM_DISCRETE_VALUES];
    UINT2        u2Rsvd;
}tLQMDiscVal;

typedef struct {
    UINT4        DiscHeader;
    UINT1        Value[MAX_FCS_DISCRETE_VALUES];
}tFCSDiscVal;

typedef struct {
    UINT4        DiscHeader;
    UINT1        Value[MAX_CALL_BACK_DISCRETE_VALUES];
    UINT1  u1Rsvd1;
}tCallBackDiscVal;

typedef struct {
    UINT4        DiscHeader;
    UINT1        Value[MAX_EPD_DISCRETE_VALUES];
    UINT2        u2Rsvd;
}tEPDDiscVal;
typedef struct {
    UINT4        DiscHeader;
    UINT4        Value[MAX_INTERNATION_DISCRETE_VALUES];
}tInternationDiscVal;


#define  PPP_THRESH_FOR_TR                300
#endif  /* __PPP_PPPLCP_H__ */
