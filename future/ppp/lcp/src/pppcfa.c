/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppcfa.c,v 1.6 2014/11/23 09:33:38 siva Exp $
 *
 * Description:Contains Interface with FutureCFA .
 *
 *******************************************************************/

#include "lcpcom.h"
#include "gcpdefs.h"
#include "gsemdefs.h"
#include "genexts.h"
#include "mpdefs.h"
#include "pppmp.h"
#include "mpexts.h"

#include "pppipcp.h"
#include "pppbcp.h"

#ifdef BAP
#include "pppbacp.h"
#include "pppbap.h"
#endif
#include "globexts.h"
#include "ipexts.h"
#include "snmpexts.h"
#include "snmccons.h"
#include "cfa.h"
#include  "ppptask.h"
#include "pppdpproto.h"
#include "ipproto.h"
#include "genproto.h"
#include "bcpproto.h"
#include "mpproto.h"

INT4
PppCreatePppInterface (UINT4 u4IfIndex, tIfLayerInfo * pHighInterface,
                       tIfLayerInfo * pLowInterface, UINT4 u4LocalIpAddr,
                       UINT4 u4PeerIpAddr, UINT4 u4IfMtu)
{
    tPPPIfId            IfId;
    tPPPIf             *pIf;

    pIf = NULL;

    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if (pIf->LinkInfo.IfIndex == u4IfIndex)
        {
            PPP_DBG ("Duplicate PPP IfIndex");
            return (PPP_FAILURE);
        }
    }

    IfId.porttype = (UINT1) u4IfIndex;

    IfId.IfType = pLowInterface->u1IfType;

    if ((pIf = LCPCreateIf (&IfId, u4IfIndex, pLowInterface->u4IfIndex)) ==
        NULL)
    {
        PRINT_MEM_ALLOC_FAILURE;
        return (PPP_FAILURE);
    }

    pIf->LinkInfo.PhysIfIndex = pLowInterface->u4IfIndex;

    if (u4IfMtu != 0)
    {
        pIf->LinkInfo.bMRUConfigured = OSIX_TRUE;
        pIf->LcpOptionsDesired.MRU = (UINT2) u4IfMtu;
        pIf->LcpGSEM.pNegFlagsPerIf[MRU_IDX].FlagMask |= DESIRED_SET;
        pIf->LcpOptionsAllowedForPeer.MRU = (UINT2) u4IfMtu;
        pIf->LcpGSEM.pNegFlagsPerIf[MRU_IDX].FlagMask |= ALLOWED_FOR_PEER_SET;
    }

    pIf->LinkInfo.u1EncapsType = pLowInterface->u1EncapsType;
    pIf->LinkInfo.u1VcType = pLowInterface->u1VcType;

#ifdef DPIF
    if (PppCreatePppInterfaceInDp (pIf) == PPP_FAILURE)
    {
        return PPP_FAILURE;
    }
#endif
    switch (pHighInterface->u4IfIndex)
    {
        case 0:                /* for IP */
            return (PppCreateAndEnableIPCPIf
                    (pIf, u4LocalIpAddr, u4PeerIpAddr));
            break;
        default:
            break;
    }                            /* switch */

    return (PPP_SUCCESS);
}

extern tBCPCtrlBlk  BCPCtrlBlk;

INT4
PppEnableBridging (UINT4 u4IfIndex)
{
    tPPPIf             *pIf;

    pIf = NULL;

    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if (pIf->LinkInfo.IfIndex == u4IfIndex)
        {
            break;
        }
    }

    if (pIf == NULL)
    {
        return PPP_FAILURE;
    }

    /* Delete the IPCP node if it exists.
     * Currently either IPCP or BCP can run but not BOTH */
    if (pIf->pIpcpPtr)
        IPCPDeleteIf (pIf->pIpcpPtr);

    /* There is no concept of fragmentation and reassembly
     * at the L2 level.
     * Hence when BCP is enabled the Default MRU should be capable
     * of handling a full ethernet frame plus the BCP bridge header
     * if the MTU has NOT already been configured.
     * 
     * But this is not possible in case of PPPoE since 
     * the maximum MRU possible in that case is only 1492 */
    if ((pIf->LinkInfo.bMRUConfigured == OSIX_FALSE) &&
        (pIf->LinkInfo.IfID.IfType != PPP_OE_IFTYPE))
    {
        pIf->LcpOptionsDesired.MRU = PPP_BCP_DEF_MRU_SIZE;
        pIf->LcpOptionsAllowedForPeer.MRU = PPP_BCP_DEF_MRU_SIZE;
        pIf->LcpGSEM.pNegFlagsPerIf[MRU_IDX].FlagMask |= DESIRED_SET;
    }

#ifdef BCP
    /* Create and Enable BCP node */
    pIf->pBcpPtr = BCPCreateIf (pIf);
    if (pIf->pBcpPtr != NULL)
    {
        BCPEnable (pIf->pBcpPtr);
    }
#endif

    return (PPP_SUCCESS);
}

INT4
PppEnableRouting (UINT4 u4IfIndex, UINT4 u4LocalIpAddr, UINT4 u4PeerIpAddr)
{
    tPPPIf             *pIf;

    pIf = NULL;

    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if (pIf->LinkInfo.IfIndex == u4IfIndex)
        {
            break;
        }
    }

    if (pIf == NULL)
    {
        return PPP_FAILURE;
    }

    /* Delete the BCP node if it exists.
     * Currently either IPCP or BCP can run but not BOTH */
#ifdef BCP
    if (pIf->pBcpPtr)
        BCPDeleteIf (pIf->pBcpPtr);
#endif

    if ((pIf->LinkInfo.bMRUConfigured == OSIX_FALSE) &&
        (pIf->LinkInfo.IfID.IfType != PPP_OE_IFTYPE))
    {
        pIf->LcpOptionsDesired.MRU = DEF_MRU_SIZE;
        pIf->LcpOptionsAllowedForPeer.MRU = DEF_MRU_SIZE;
    }

    PppCreateAndEnableIPCPIf (pIf, u4LocalIpAddr, u4PeerIpAddr);

    return (PPP_SUCCESS);
}

/************************************/

INT4
PppCreateMpInterface (UINT2 u2IfIndex, tIfLayerInfo * pHighInterface,
                      UINT4 u4LocalIpAddr, UINT4 u4PeerIpAddr, UINT4 u4IfMtu)
{
    tPPPIfId            IfId;
    tPPPIf             *pIf;

    IfId.porttype = (UINT1) u2IfIndex;
    IfId.IfType = CFA_PPP;        /* Lower layer link can only be PPP */

#ifdef MP
    if ((pIf = MPCreate (&IfId, u2IfIndex)) == NULL)
    {
        PRINT_MEM_ALLOC_FAILURE;
        return (PPP_FAILURE);
    }
#endif

    pIf->MPInfo.BundleInfo.MPFlagsPerIf[MRRU_IDX].FlagMask |=
        ALLOWED_FOR_PEER_SET;

    if (u4IfMtu != 0)
    {
        pIf->LinkInfo.bMRUConfigured = OSIX_TRUE;
        pIf->MPInfo.BundleInfo.BundleOptDesired.MRRU = (UINT2) u4IfMtu;
        pIf->MPInfo.BundleInfo.BundleOptAllowedForPeer.MRRU = (UINT2) u4IfMtu;
    }

    switch (pHighInterface->u4IfIndex)
    {
        case 0:                /* for IP */
            return (PppCreateAndEnableIPCPIf
                    (pIf, u4LocalIpAddr, u4PeerIpAddr));
            break;
        default:
            break;
    }                            /* switch */

    return (PPP_SUCCESS);
}

/*************************************/
INT4
PppUpdatePppInterface (UINT4 u4IfIndex, tIfLayerInfo * pHighInterface,
                       tIfLayerInfo * pLowInterface, UINT4 u4LocalIpAddr,
                       UINT4 u4PeerIpAddr, UINT4 u4IfMtu)
{
    tPPPIf             *pIf;
    tCfaIfInfo          IfInfo;

    pIf = NULL;
    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if (pIf->LinkInfo.IfIndex == u4IfIndex)
        {
            pIf->LinkInfo.PhysIfIndex = pLowInterface->u4IfIndex;
            pIf->LinkInfo.u1EncapsType = pLowInterface->u1EncapsType;

            if (u4IfMtu != 0)
            {
                /* A new MTU has been explicitly configured for this session */
                pIf->LinkInfo.bMRUConfigured = OSIX_TRUE;
                pIf->LcpOptionsDesired.MRU = (UINT2) u4IfMtu;
                pIf->LcpGSEM.pNegFlagsPerIf[MRU_IDX].FlagMask |= DESIRED_SET;
                pIf->LcpOptionsAllowedForPeer.MRU = (UINT2) u4IfMtu;
                pIf->LcpGSEM.pNegFlagsPerIf[MRU_IDX].FlagMask |=
                    ALLOWED_FOR_PEER_SET;
            }

            CfaGetIfInfo (u4IfIndex, &IfInfo);
            if ((IfInfo.u1BridgedIface == CFA_DISABLED) &&
                (pHighInterface->u4IfIndex == 0))
            {
                PppCreateAndEnableIPCPIf (pIf, u4LocalIpAddr, u4PeerIpAddr);
            }

            break;
        }                        /* If */
    }                            /* SLL_SCAN */

    return (PPP_SUCCESS);
}

/*******************************************/

INT4
PppUpdateMpInterface (UINT2 u2IfIndex, tIfLayerInfo * pHighInterface,
                      UINT4 u4LocalIpAddr, UINT4 u4PeerIpAddr, UINT4 u4IfMtu)
{
    tPPPIf             *pIf;

    pIf = NULL;
    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if (pIf->LinkInfo.IfIndex == u2IfIndex)
        {
            if (pHighInterface->u4IfIndex == 0)
            {
                PppCreateAndEnableIPCPIf (pIf, u4LocalIpAddr, u4PeerIpAddr);
            }

            if (u4IfMtu != 0)
            {
                pIf->LinkInfo.bMRUConfigured = OSIX_TRUE;
                pIf->MPInfo.BundleInfo.BundleOptDesired.MRRU = (UINT2) u4IfMtu;
                pIf->MPInfo.BundleInfo.BundleOptAllowedForPeer.MRRU
                    = (UINT2) u4IfMtu;
                pIf->MPInfo.BundleInfo.MPFlagsPerIf[MRRU_IDX].FlagMask |=
                    ALLOWED_FOR_PEER_SET;
            }

            break;
        }
    }
    return (PPP_SUCCESS);
}

/*************************************/

INT4
PppComposeMpBundle (UINT2 u2MpIfIndex, UINT4 u4PppifIndex, UINT1 u1Command)
{
#ifdef MP
    tPPPIf             *pIf;
    tPPPIf             *pBundleIf;

    pIf = PppGetPppIfPtr (u4PppifIndex);
    pBundleIf = (tPPPIf *) SNMPGetBundleIfptr (u2MpIfIndex);

    if ((pIf != NULL) && (pBundleIf != NULL))
    {
        switch (u1Command)
        {
            case PPP_ADD_LINK:
                MPAddLinkToBundle (pBundleIf, pIf);
                break;

            case PPP_DELETE_LINK:
                MPDeleteLinkFromBundle (pBundleIf, pIf);
                break;
            default:
                return (PPP_FAILURE);
                break;
        }                        /* switch */
    }                            /* if */
    else
    {
        return (PPP_FAILURE);
    }                            /* if */
#else

   UNUSED_PARAM (u2MpIfIndex);
   UNUSED_PARAM (u4PppifIndex);
   UNUSED_PARAM (u1Command);
#endif

    return (PPP_SUCCESS);
}

/****************************/
INT4
PppDeRegisterInterface (UINT4 u4IfIndex)
{
    tPPPIf             *pIf = NULL;
    PPP_LOCK ();
    pIf = PppGetIfPtr (u4IfIndex);
    if (pIf != NULL)
    {
        PPPDeleteIf (pIf);
    }
    PPP_UNLOCK ();
    return (PPP_SUCCESS);
}

/****************************/
INT4
PppUpdateInterfaceStatus (UINT4 u4IfIndex, UINT1 u1AdminStatus,
                          UINT1 u1OperStatus)
{

    t_MSG_DESC         *pBuf = NULL;

    if ((pBuf = (t_MSG_DESC *) (VOID *) ALLOCATE_BUFFER (16, 0)) == NULL)
    {
        return (PPP_FAILURE);
    }
    ASSIGN4BYTE (pBuf, 0, u4IfIndex);
    ASSIGN1BYTE (pBuf, 4, u1AdminStatus);
    ASSIGN1BYTE (pBuf, 5, u1OperStatus);
    PPP_GET_SRC_MODID (pBuf) = PPPMSG_UPDATE_INTF_STATUS;

    if (OsixSendToQ (SELF, PPP_QUEUE_NAME, pBuf, OSIX_MSG_NORMAL) !=
        OSIX_SUCCESS)
    {
        PPP_TRC (ALL_FAILURE, "Send to Ppp Queue Failure.");
        RELEASE_BUFFER (pBuf);
        return (PPP_FAILURE);
    }
    if (OsixSendEvent (SELF, PPP_TASK_NAME, PPP_QUEUE_EVENT) != OSIX_SUCCESS)
    {
        PPP_TRC (ALL_FAILURE, "Ppp Queue Event Failure.");
        return (PPP_FAILURE);
    }
    return (PPP_SUCCESS);
}

/*****************************/
INT4
PppUpdateAdminStatus (UINT4 u4IfIndex, UINT1 u1AdminStatus)
{
    tPPPIf             *pIf;

    if ((pIf = PppGetIfPtr (u4IfIndex)) != NULL)
    {
        switch (u1AdminStatus)
        {
            case STATUS_UP:
                if (pIf->LcpStatus.AdminStatus != STATUS_UP)
                {
                    /* NAETRA_ADD If the Dialup interface is layered below this PPP
                     * interface then do not Enable this PPP interface, because 
                     * we need to DIAL first. 
                     */
#ifdef REVISIT
                    if (GddCheckIfDialInterfaceEnabled (pIf->LinkInfo.IfIndex)
                        == OSIX_SUCCESS)
                    {
                        if (CfaCheckIfDialInterfaceEnabled
                            (pIf->LinkInfo.IfIndex) == OSIX_SUCCESS)
                        {
                            UINT4               u4Reconnect = 0;
                            /* NAETRA_ADD When AutoReconnect is Enabled ,
                             * redial if already dialed and connected to
                             * ISP Successfully.
                             */
                            nmhGetIfWanAutoReconnect (pIf->LinkInfo.IfIndex,
                                                      &u4Reconnect);
                            if (u4Reconnect == STATUS_UP)
                            {
                                CliRedial ();
                                break;
                            }
                        }
                    }
#endif
                    PPPIfEnable (pIf);
                    pIf->LcpStatus.AdminStatus = STATUS_UP;
                }
                break;
            case STATUS_DOWN:
                if (pIf->LcpStatus.AdminStatus != STATUS_DOWN)
                {

                    PPPIfdisable (pIf);
                    pIf->LcpStatus.AdminStatus = STATUS_DOWN;
                }
                break;
            default:
                break;
        }                        /* switch */
        return PPP_SUCCESS;
    }                            /* if */
    return PPP_FAILURE;
}                                /* PppUpdateAdminStatus */

/*****************************/
INT4
PppUpdateLLOperStatus (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    tPPPIf             *pIf;
    tGSEM              *pGSEM = NULL;

    if ((pIf = PppGetIfPtr (u4IfIndex)) != NULL)
    {
        switch (u1OperStatus)
        {
            case PPP_UP:
                pGSEM = &pIf->LcpGSEM;
                GSEMRun (pGSEM, u1OperStatus);
                break;
            case PPP_DOWN:
                pGSEM = &pIf->LcpGSEM;
                GSEMRun (pGSEM, u1OperStatus);
                break;
            default:
                break;
        }                        /* switch */
        return PPP_SUCCESS;
    }                            /* if */
    return PPP_FAILURE;
}                                /* PppUpdateLLOperStatus */

/**********************************/
INT4
PppCreateAndEnableIPCPIf (tPPPIf * pIf, UINT4 u4LocalIpAddr, UINT4 u4PeerIpAddr)
{

    tIPCPIf            *pIpcpPtr;

    if (pIf->pIpcpPtr == NULL)
    {
        if ((pIf->pIpcpPtr = IPCPCreateIf (pIf)) == NULL)
        {
            PRINT_MEM_ALLOC_FAILURE;
            return (PPP_FAILURE);
        }
    }
    else
    {
        IPCPDisableIf (pIf->pIpcpPtr);
    }

    pIpcpPtr = (tIPCPIf *) (pIf->pIpcpPtr);

    pIpcpPtr->IpcpOptionsDesired.IPAddress = u4LocalIpAddr;

    if (u4PeerIpAddr != 0)
    {
        pIpcpPtr->IpcpGSEM.pNegFlagsPerIf[IP_ADDR_IDX].FlagMask |=
            FORCED_TO_REQUEST_SET;
        pIpcpPtr->IpcpOptionsAllowedForPeer.IPAddress = u4PeerIpAddr;
    }

    pIpcpPtr->IpcpGSEM.pNegFlagsPerIf[IP_ADDR_IDX].FlagMask |=
        ALLOWED_FOR_PEER_SET;
    pIpcpPtr->AdminStatus = ADMIN_OPEN;
    IPCPEnableIf (pIpcpPtr, &IPCPCtrlBlk);
    return PPP_SUCCESS;

}                                /* PppCreateAndEnableIPCPIf */

/********************************/
INT4
PppIsIfaceLCPStatusOpen (UINT4 u4PppIfNo)
{
    tPPPIf             *pIf = NULL;

    pIf = PppGetPppIfPtr (u4PppIfNo);

    if (pIf == NULL)
    {
        return FALSE;
    }

    if ((pIf->LcpStatus.LinkAvailability == LINK_AVAILABLE) ||
        (pIf->LcpStatus.LinkAvailability == LINK_OPENED))
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

/************************************************/
INT4
PppIsIfaceIPCPStatusOpen (UINT4 u4PppIfNo)
{
    tPPPIf             *pIf = NULL;
    tIPCPIf            *pIpcpPtr = NULL;

    pIf = PppGetIfPtr (u4PppIfNo);

    if (pIf == NULL)
    {
        return FALSE;
    }

    pIpcpPtr = pIf->pIpcpPtr;

    if (pIpcpPtr == NULL)
    {
        return FALSE;
    }

    if (pIpcpPtr->OperStatus == STATUS_UP)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

/**********************************************/
INT4
PppIsIfaceBCPStatusOpen (UINT4 u4PppIfNo)
{
    tPPPIf             *pIf = NULL;
    tBCPIf             *pBcpPtr = NULL;

    pIf = PppGetIfPtr (u4PppIfNo);
    if (pIf == NULL)
    {
        return FALSE;
    }

    pBcpPtr = pIf->pBcpPtr;

    if (pBcpPtr == NULL)
    {
        return FALSE;
    }

    if (pBcpPtr->OperStatus == STATUS_UP)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

/*********************************************/
INT4
PppGetNegAuthProtocol (UINT4 u4PppIfNo, INT4 *i4Auth, INT4 *i4PeerAuth)
{
    tPPPIf             *pIf = NULL;
    tAUTHIf            *pAuthPtr = NULL;

    pIf = PppGetIfPtr (u4PppIfNo);

    if (pIf == NULL)
    {
        return FAILURE;
    }

    pAuthPtr = pIf->pAuthPtr;

    if (pAuthPtr != NULL)
    {
        *i4Auth = pAuthPtr->ServerProt;
        *i4PeerAuth = pAuthPtr->ClientProt;

        return SUCCESS;
    }

    return FAILURE;
}

/*********************************************/

INT4
PppInitForIfType (UINT4 u4IfIndex)
{
    tPPPIf             *pMemberIf = NULL;
    SLL_SCAN (&PPPIfList, pMemberIf, tPPPIf *)
    {
        if (pMemberIf->LinkInfo.IfIndex == u4IfIndex)
        {
            pMemberIf->LinkInfo.IfID.IfType = NONE_IF_TYPE;
            if (LCPInitForIfType (pMemberIf) == NOT_OK)
            {
                return FAILURE;
            }
        }
    }
    return SUCCESS;
}
