/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppplcpif.c,v 1.6 2014/12/16 11:47:25 siva Exp $
 *
 * Description:This file contains the interface routines of the
 *             LCP module.                    
 *
 *******************************************************************/

#include  "pppcom.h"
#include  "lcpcom.h"
#include  "gsemdefs.h"
#include  "gcpdefs.h"
#include  "lcpexts.h"

#include "mpdefs.h"
#include "pppmp.h"

#ifdef BAP
#include "pppbacp.h"
#include "pppbap.h"
#endif

#include "pppipcp.h"
#include "pppipv6cp.h"
#include "pppmuxcp.h"
#include "pppmplscp.h"
#include "pppipxcp.h"
#include "ipxproto.h"
#include "pppeccmn.h"
#include "pppccp.h"
#include "pppeccmn.h"
#include "pppecp.h"
#include "pppbcp.h"
#include "pppoeport.h"

#include "globexts.h"
/* #ifdef L2TP_WANTED */
#include "pppl2tp.h"
#include "l2tp.h"
/* #endif */
#include "genproto.h"
#include "lqmprot.h"
#include "genexts.h"
#include "ipproto.h"
#include "ecpproto.h"
#include "ipv6proto.h"
#include "ccpproto.h"
#include "bcpproto.h"
#include "mpproto.h"

extern UINT1        gu1AccessType;

/***********************************************************************/
/*                      LQM INTERFACE  FUNCTIONS                       */
/***********************************************************************/
/***********************************************************************
*  Function Name :LCPIndicateQuality
*  Description   :
*        This function is called by the LQM module to pass the quality of
*    the link and to take appropriate action.
*  Parameter(s)  :
*        pLqmPtr - points to the LQM i/f data structure
*        Status  - indicates the quality of the link (GOOD/BAD)
*  Return Values : VOID
*********************************************************************/
/* 99070001 -- Return value is changed from VOID to INT4    */
INT4
LCPIndicateQuality (tLQMIf * pLqmPtr, UINT1 Status)
{
    tAUTHIf            *pAuthPtr;
    tPPPIf             *pIf;

    pIf = pLqmPtr->pIf;
    pAuthPtr = pIf->pAuthPtr;

    pLqmPtr->LQMStatus.FailCounter = 0;
    pLqmPtr->LQMStatus.SuccessCounter = 0;

    if (Status == BAD)
    {

        /* If the link is still BAD, after configured Retries,
           the link should be terminated
         */
        if (pLqmPtr->LQMStatus.NumReTrials == pLqmPtr->LQMCONFIGS (MaxReTrials))
        {
            LCPTriggerEventToNCP (pIf, RXJ_MINUS, LINK_SUSPENDED);
            /* 99070001 -- Return value is changed from VOID to BAD  */
            return (BAD);
        }
        if (pAuthPtr->ServerProt == CHAP_PROTOCOL)
        {
            PPPStopTimer (&pAuthPtr->ServerInfo.ServerChap.ChapReChalTimer);
            PPPStopTimer (&pAuthPtr->ServerInfo.ServerChap.ChapChalTimer);
        }
        if (pAuthPtr->ClientProt == CHAP_PROTOCOL)
            PPPStopTimer (&pAuthPtr->ClientInfo.ClientChap.ChapRespTimer);

        if (pAuthPtr->ServerProt == EAP_PROTOCOL)
        {
            PPPStopTimer (&pAuthPtr->ServerInfo.ServerEap.AuthTypeOptions.
                          ServerEAPMD5.ReChalTimer);
            PPPStopTimer (&pAuthPtr->ServerInfo.ServerEap.AuthTypeOptions.
                          ServerEAPMD5.ChalTimer);
        }

        if (pAuthPtr->ClientProt == PAP_PROTOCOL)
            PPPStopTimer (&pAuthPtr->ClientInfo.ClientPap.PapResendTimer);

        pIf->LcpStatus.OperStatus = STATUS_DOWN;
        LCPTriggerEventToNCP (pIf, RXJ_MINUS, LINK_SUSPENDED);
        pLqmPtr->LQMStatus.NumReTrials++;
    }
    else
    {
        if (pIf->LcpStatus.LinkAvailability == LINK_SUSPENDED)
        {
            /* Enable the CHAP timers */
            if (pAuthPtr->ServerProt == CHAP_PROTOCOL)
            {
                PPPStartTimer (&pAuthPtr->ServerInfo.ServerChap.ChapReChalTimer,
                               CHAP_RECHAL_TIMER,
                               pAuthPtr->ServerInfo.
                               ServerChap.ChallengeInterval);
                PPPStartTimer (&pAuthPtr->ServerInfo.ServerChap.ChapChalTimer,
                               CHAP_CHAL_TIMER,
                               pAuthPtr->ServerInfo.ServerChap.TimeoutTime);
            }
            if (pAuthPtr->ClientProt == CHAP_PROTOCOL)
                PPPStartTimer (&pAuthPtr->ClientInfo.ClientChap.ChapRespTimer,
                               CHAP_RESP_TIMER,
                               pAuthPtr->ClientInfo.ClientChap.TimeoutTime);

            /* Enable the PAP timers */
            if (pAuthPtr->ClientProt == PAP_PROTOCOL)
                PPPStartTimer (&pAuthPtr->ClientInfo.ClientPap.PapResendTimer,
                               PAP_REQ_TIMER,
                               pAuthPtr->ClientInfo.ClientPap.TimeoutTime);

            /* Enable the EAP timers */
            if (pAuthPtr->ServerProt == EAP_PROTOCOL)
            {
                PPPStartTimer (&pAuthPtr->ServerInfo.ServerEap.AuthTypeOptions.
                               ServerEAPMD5.ReChalTimer, EAP_MD5_RECHAL_TIMER,
                               pAuthPtr->ServerInfo.ServerEap.AuthTypeOptions.
                               ServerEAPMD5.ChallengeInterval);
                PPPStartTimer (&pAuthPtr->ServerInfo.ServerEap.AuthTypeOptions.
                               ServerEAPMD5.ChalTimer, EAP_MD5_CHAL_TIMER,
                               pAuthPtr->ServerInfo.ServerEap.TimeoutTime);
            }

            pIf->LcpStatus.OperStatus = STATUS_UP;

            /* Restart the NCPs */
            LCPTriggerEventToNCP (pIf, PPP_DOWN, LINK_AVAILABLE);
            LCPTriggerEventToNCP (pIf, PPP_UP, LINK_AVAILABLE);
        }
        else
            pIf->LcpStatus.LinkQualityStatus = Status;
    }
    /* 99070001 -- Return value is changed from VOID to GOOD  */
    return (GOOD);
}

/***********************************************************************/
/*                      AUTH INTERFACE  FUNCTIONS                      */
/***********************************************************************
*  Function Name :LCPAuthProcessFailed 
*  Description   :
*                This function is called when the peer has failed in the 
*     authentication. It indicates the LCP to terminate the link through 
*   appropriately events.  If the local entity fails in the authentication, 
*   then no processing is done ; it is left to the peer to initiate the Link 
*   down process.
*  Parameter(s)  :
*          pIf -  points to the PPP interface data structure 
*  Return Values : VOID
*********************************************************************/
VOID
LCPAuthProcessFailed (tPPPIf * pIf)
{
    PPPSendEventToPPPTask (pIf->LinkInfo.IfIndex, RXJ_MINUS);
    return;
}

/*********************************************************************
*  Function Name : LCPUpdateAuthStatus
*  Description   :
*            This function updates the authentication Status  bits and 
*  indicates the NCP , if the authentication is over.
*  Parameter(s)  :
*        pIf     -  points to the PPP interface data structure 
*        Status     -  Authentication Status.  
*  Return Values : VOID
*********************************************************************/
VOID
LCPUpdateAuthStatus (tPPPIf * pIf, UINT1 Status)
{

    pIf->LcpStatus.AuthStatus &= Status;    /* Update the mask bits */
/*  SKR007 *** The Flag check is made not enter NetworkLayer phase for periodic authentication ***/
    if (pIf->LcpStatus.AuthStatus == ALL_AUTH_OVER
        && (pIf->pAuthPtr->AuthTriggeredNCPs != PPP_YES))
    {
        PPP_DBG ("Authentication Successful...");
        LCPEnterNetworkLayerPhase (pIf);
        pIf->pAuthPtr->AuthTriggeredNCPs = PPP_YES;

        /* Free Up the memory, if any, that has been allocated to send
           MS-CHAP Change Password Packet */

        desdone ();
        if ((pIf->pAuthPtr->ClientProt == CHAP_PROTOCOL)
            && (pIf->pAuthPtr->ClientInfo.ClientChap.pMSCHAPChgPwdPkt != NULL))
        {
            FREE_STR (pIf->pAuthPtr->ClientInfo.ClientChap.pMSCHAPChgPwdPkt);
            pIf->pAuthPtr->ClientInfo.ClientChap.pMSCHAPChgPwdPkt = NULL;
        }
    }
    return;
}

/***********************************************************************/
/*                      LLI INTERFACE  FUNCTIONS                       */
/***********************************************************************/
/*********************************************************************
*  Function Name : PPPSendRejPkt 
*  Description   :
*      This function constructs and transmits Protocol/Code reject packet.
*  Parameter(s)  :
*      pIf       -  pointer to the Interface structure
*      pInpacket -  points to the buffer containing the packet to be rejected
*      Length    -  Length of the rejected packet
*      Code      -  Indicates whether the reject is for Protocol Rej/
*                    Code Rej           
*  Return Value  : TRUE/FALSE
*********************************************************************/
INT1
PPPSendRejPkt (tPPPIf * pIf, t_MSG_DESC * pErrpacket, UINT2 Length,
               UINT2 Protocol, UINT1 Code)
{
    t_MSG_DESC         *pOutBuf;
    UINT2               Len = CP_HDR_LEN, RejectedProt = 0;
    /* Makefile changes - unused */
    UINT4               ReadOffset = 0;
    UINT1               CharVal = 0;

    ReadOffset = pIf->LinkInfo.RxAddrCtrlLen;

    if (Code == CODE_REJ_CODE)
    {
        ReadOffset += pIf->LinkInfo.RxProtLen;
    }
    else
    {
        Len = (UINT2) (Len + UNCOMPRESSED_PID_LEN);
        if (pIf->LinkInfo.RxProtLen == COMPRESSED_PID_LEN)
        {
            GET1BYTE (pErrpacket, ReadOffset, CharVal);
            RejectedProt = CharVal;
            ReadOffset += COMPRESSED_PID_LEN;
        }
        else
        {
            GET2BYTE (pErrpacket, ReadOffset, RejectedProt);
            ReadOffset += UNCOMPRESSED_PID_LEN;
        }
    }

    if ((Length + Len) > pIf->LcpOptionsAckedByLocal.MRU)
    {
        Length = (UINT2) (pIf->LcpOptionsAckedByLocal.MRU - Len);
    }

    Len = (UINT2) (Len + Length);

    if ((pOutBuf =
         (t_MSG_DESC *) (VOID *) ALLOCATE_BUFFER (PPP_INFO_OFFSET + Len,
                                                  PPP_INFO_OFFSET)) != NULL)
    {

        FILL_PKT_HEADER (pOutBuf, Code, pIf->LcpGSEM.CurrentId, Len);
        if (Code == PROT_REJ_CODE)
            APPEND2BYTE (pOutBuf, RejectedProt);

        pIf->LcpGSEM.CurrentId++;

        CRU_BUF_COPY_MSGS (pOutBuf, pErrpacket, VALID_BYTES (pOutBuf),
                           ReadOffset, Length);
        PPPLLITxPkt (pIf, pOutBuf, Len, Protocol);

        return (TRUE);
    }
    else
    {
        PRINT_MEM_ALLOC_FAILURE;
    }
    return (FALSE);
}

/*********************************************************************
*  Function Name : LCPGetSEMPtr 
*  Description   :
*    This function returns the pointer to the LCP GSEM taking the tPPPIf 
*    structure as a parameter. 
*  Input         :
*        pIf    -    Points to the PPP Interface structure.
*  Return Value  :
*            Returns a pointer to the LCP GSEM. 
*********************************************************************/
tGSEM              *
LCPGetSEMPtr (tPPPIf * pIf)
{
    return (&pIf->LcpGSEM);
}

/***********************************************************************/
/*                      SNMP INTERFACE  FUNCTIONS                      */
/***********************************************************************
*  Function Name : LCPSendEchoReq
*  Description   :
*       This function is called to send a Echo request packet 
*  Parameter(s)  :
*       pIf            - pointer to the interface structure
*       Retransmission - indicates whether the pkt is retransmitted
*  Return Value  : VOID
*********************************************************************/
VOID
LCPSendEchoReq (tPPPIf * pIf, UINT1 Flag)
{

    if (Flag == NORMAL_TRANSMISSION)
    {
        pIf->EchoInfo.EchoId++;
        pIf->EchoInfo.EchoTransmits = 0;
        if (OsixGetSysTime (&pIf->MPInfo.MemberInfo.LastTransitTicks) ==
            OSIX_FAILURE)
        {
            PPP_TRC (OS_RESOURCE, "Failure in  OsixGetSysTime()");
        }
    }

    if (pIf->LcpStatus.LinkAvailability != LINK_NOT_AVAILABLE)
    {
        LCPSendRXRPkt (pIf, ECHO_REQ_CODE, pIf->EchoInfo.EchoId);
        /* Start timer  again */
        pIf->EchoInfo.EchoTimer.Param1 = PTR_TO_U4 (pIf);
        PPPRestartTimer (&pIf->EchoInfo.EchoTimer, ECHO_REQ_TIMER,
                         pIf->LinkInfo.TimeOutTime);
    }
    return;
}

/***********************************************************************
*  Function Name : LCPSendRXRPkt
*  Description   :
*       This function is called to send a RXR packet 
*  Parameter(s)  :
*       pIf      - pointer to the interface structure
*     Code       - DISCARD/ECHO/ID
*   Identifier   - Id. field of the pkt.
*  Return Value  : VOID
*********************************************************************/
VOID
LCPSendRXRPkt (tPPPIf * pIf, UINT1 Code, UINT1 Identifier)
{
    t_MSG_DESC         *pOutBuf;
    UINT2               Index, Length = 0;

    Length = (UINT2) (pIf->LcpOptionsAckedByLocal.MRU / 10);

    if ((pOutBuf =
         (t_MSG_DESC *) (VOID *) ALLOCATE_BUFFER (PPP_INFO_OFFSET + Length,
                                                  PPP_INFO_OFFSET)) == NULL)
    {
        PRINT_MEM_ALLOC_FAILURE;
        return;
    }

    FILL_PKT_HEADER (pOutBuf, Code, Identifier, (UINT2) (Length - 4));

    /* Include Magic number if successfully negotiated */

    /*** SKR007 *** allow it for Link in suspended state also ***/
    if (pIf->LcpStatus.LinkAvailability == LINK_AVAILABLE
        || pIf->LcpStatus.LinkAvailability == LINK_SUSPENDED)
    {
        APPEND4BYTE (pOutBuf, pIf->LcpOptionsAckedByPeer.MagicNumber);
    }

    /* Subtract length for Magic Number and code */

    for (Index = 4; Index < Length - CP_HDR_LEN - MAGIC_FIELD_LEN; Index++)
    {
        APPEND1BYTE (pOutBuf, pIf->EchoInfo.EchoChar);
    }

    PPPLLITxPkt (pIf, pOutBuf, Length, LCP_PROTOCOL);

    return;
}

/***********************************************************************
*  Function Name : LCPSendDiscardReq
*  Description   :
*       This function is called to send a discard request packet 
*  Parameter(s)  :
*       pIf         - pointer to the interface structure
*  Return Value  : VOID
*********************************************************************/
VOID
LCPSendDiscardReq (tPPPIf * pIf)
{
    /* Makefile changes - unused variables */

    if (pIf->LcpStatus.LinkAvailability != LINK_NOT_AVAILABLE)
    {
        ++pIf->LcpGSEM.CurrentId;
        LCPSendRXRPkt (pIf, DISCARD_REQ_CODE, pIf->LcpGSEM.CurrentId);
    }
    return;
}

/***********************************************************************
*  Function Name : LCPSendIdPkt
*  Description   :
*       This function is called to send a Identification packet 
*  Parameter(s)  :
*       pIf         - pointer to the interface structure
*  Return Value  : VOID
*********************************************************************/
VOID
LCPSendIdPkt (tPPPIf * pIf)
{
    ++pIf->LcpGSEM.CurrentId;
    LCPSendRXRPkt (pIf, IDENTIFICATION_CODE, pIf->LcpGSEM.CurrentId);
    return;
}

/***********************************************************************/
/*                      GSEM INTERFACE  FUNCTIONS                      */
/***********************************************************************
*  Function Name : LCPSendEchoResponse
*  Description   :
*          This function is used to transmit the Echo Response packet  for the
*  received Echo request packet.
*  Parameter(s)  :
*        pIf        -     pointer to the interface structure
*        Id        -     Identifier that is received along with Echo Req
*        pMsg    -      points to the Msg that is received along with Echo req
*        Length    -      Length of the Msg part  of Echo req
*  Return Value  : VOID
*********************************************************************/
VOID
LCPSendEchoResponse (tPPPIf * pIf, UINT1 Id, UINT1 *pMsg, UINT2 MsgLen)
{
    t_MSG_DESC         *pOutBuf;
    UINT2               Len = 0;

    Len = CP_HDR_LEN + MAGIC_FIELD_LEN;

    if ((Len + MsgLen) > pIf->LcpOptionsAckedByLocal.MRU)
    {
        MsgLen = (UINT2) (pIf->LcpOptionsAckedByLocal.MRU - Len);
    }

    Len = (UINT2) (Len + MsgLen);

    if ((pOutBuf =
         (t_MSG_DESC *) (VOID *) ALLOCATE_BUFFER (PPP_INFO_OFFSET + Len,
                                                  PPP_INFO_OFFSET)) == NULL)
    {
        PRINT_MEM_ALLOC_FAILURE;
        return;
    }

    FILL_PKT_HEADER (pOutBuf, ECHO_REPLY_CODE, Id, Len);

    APPEND4BYTE (pOutBuf, pIf->LcpOptionsAckedByPeer.MagicNumber);

    if (MsgLen != 0)
    {
        APPENDSTR (pOutBuf, pMsg, MsgLen);
    }

    PPPLLITxPkt (pIf, pOutBuf, Len, LCP_PROTOCOL);

    return;
}

/***********************************************************************/
/*                      HLI INTERFACE  FUNCTIONS                       */
/***********************************************************************
*  Function Name : LCPCreateIf
*  Description   :
*                      This procedure when called to create an entry in the LCP
*  interface data base corresponding to IfId. It initializes the created entry 
*  to the default values by calling LCPInit() function..
*  Parameter(s)  :  
*        pIfId      - pointer to the Identifier of the  Interface to be created
*        IfIndex    - Indicates the entry of this PPP interface in the IfTable 
*        PhyIfIndex - Indicates the Physical Interface index 
*  Return Value  : 
*        pointer to the newly created interface entry , if the creation is 
*       successful
*       NULL  if it  fails to create the interface 
*********************************************************************/
tPPPIf             *
LCPCreateIf (tPPPIfId * pIfId, UINT4 IfIndex, UINT4 PhyIfIndex)
{
    tPPPIf             *pLcpPtr;

    /* Allocate memory for a new interface structure   */

    if ((pLcpPtr = (tPPPIf *) (VOID *) ALLOC_PPP_IF ()) != NULL)
    {
        PPP_DBG ("Before invoking INIT.");
        pLcpPtr->LinkInfo.IfIndex = IfIndex;
        pLcpPtr->LinkInfo.PhysIfIndex = PhyIfIndex;
        MEMCPY (&pLcpPtr->LinkInfo.IfID, pIfId, sizeof (tPPPIfId));
        if (LCPInit (pLcpPtr) != NOT_OK)
        {
#ifdef LQM
            if ((pLcpPtr->pLqmPtr = LQMCreate (pLcpPtr)) == NULL)
            {
                PRINT_MEM_ALLOC_FAILURE;
                FREE_PPP_IF (pLcpPtr);
                return (NULL);
            }
#endif
            if ((pLcpPtr->pAuthPtr = AUTHCreate (pLcpPtr)) == NULL)
            {
                PRINT_MEM_ALLOC_FAILURE;
#ifdef LQM
                FREE_LQM_IF (pLcpPtr->pLqmPtr);
#endif
                FREE_PPP_IF (pLcpPtr);
                return (NULL);
            }

            SLL_ADD (&PPPIfList, (t_SLL_NODE *) pLcpPtr);
            PPP_DBG ("After inserting .");
            return (pLcpPtr);
        }
        else
        {
            PPP_TRC (ALL_FAILURE, "LcpInit Failed \n");
            FREE_PPP_IF (pLcpPtr);
            return (NULL);
        }

    }

    PPP_DBG ("Error: Unable to Alloc/Init PPPIf entry .");
    return (NULL);
}

/*********************************************************************
*  Function Name : LCPDeleteIf
*  Description   :
*            This function is used to delete the PPP link entry from the 
*  interface table.
*  Parameter(s)  :
*       pIf   -   points to the interface to be enabled
*  Return Value  : VOID
*********************************************************************/
VOID
LCPDeleteIf (tPPPIf * pIf)
{

    UINT1               DeleteCPPtrs,i4RetVal=0;
    tIPCPIf            *pIpcpIf;

#ifdef IPv6CP
    tIP6CPIf           *pIp6cpIf;
#endif
    tMuxCPIf           *pMuxcpIf;
    tMPLSCPIf          *pMplscpIf;
    tIPXCPIf           *pIpxcpIf;
#ifdef BCP
    tBCPIf             *pBcpIf;
#endif
#ifdef CCP
    tCCPIf             *pCCPIf;
#endif
    tECPIf             *pECPIf;
    tSecret            *pDel = NULL;

    DeleteCPPtrs = PPP_YES;

    if ((pIf->LinkInfo.IfID.IfType == PPP_OE_IFTYPE)
        && (pIf->LinkInfo.pPppoeSession != NULL))
    {
        PPPDeletePPPoESession (pIf);
    }
    else if (pIf->LcpGSEM.CurrentState == OPENED)
    {
        /* Send a Terminate request packet so that
         * the peer gets a chance to close the connection */
        i4RetVal = UTILSendReqPkt (&(pIf->LcpGSEM), NORMAL_TRANSMISSION, TERM_REQ_CODE);
        UNUSED_PARAM (i4RetVal);
    }

    /*  Free all timers active over this interface */
    PPPStopTimer (&pIf->LinkInfo.IdleTimer);
    PPPStopTimer (&pIf->EchoInfo.EchoTimer);

    PPPStopTimer (&(pIf->RecdTimer));
    PPPStopTimer (&(pIf->CallBackTimeInfo.CallBackDisconnectTimer));
    PPPStopTimer (&(pIf->CallBackTimeInfo.CallBackDialTimer));
    PPPStopTimer (&pIf->EchoInfo.KeepAliveTimer);

    if (pIf->LcpOptionsAckedByLocal.LCPCallBackInfo.pCallBackMsg != NULL)
    {
        FREE_STR (pIf->LcpOptionsAckedByLocal.LCPCallBackInfo.pCallBackMsg);
    }
    if (pIf->LcpOptionsAckedByPeer.LCPCallBackInfo.pCallBackMsg != NULL)
    {
        FREE_STR (pIf->LcpOptionsAckedByPeer.LCPCallBackInfo.pCallBackMsg);
    }

    if (pIf->pAuthPtr != NULL)
    {
        if (pIf->pAuthPtr->ClientProt == CHAP_PROTOCOL)
            PPPStopTimer (&pIf->pAuthPtr->ClientInfo.ClientChap.ChapRespTimer);

        if (pIf->pAuthPtr->ClientProt == PAP_PROTOCOL)
            PPPStopTimer (&pIf->pAuthPtr->ClientInfo.ClientPap.PapResendTimer);

        if (pIf->pAuthPtr->ServerProt == CHAP_PROTOCOL)
        {
            PPPStopTimer (&pIf->pAuthPtr->ServerInfo.ServerChap.ChapChalTimer);
            PPPStopTimer (&pIf->pAuthPtr->ServerInfo.
                          ServerChap.ChapReChalTimer);
        }
        if (pIf->pAuthPtr->ServerProt == EAP_PROTOCOL)
        {
            PPPStopTimer (&pIf->pAuthPtr->ServerInfo.ServerEap.
                          AuthTypeOptions.ServerEAPMD5.ReChalTimer);
            PPPStopTimer (&pIf->pAuthPtr->ServerInfo.ServerEap.
                          AuthTypeOptions.ServerEAPMD5.ChalTimer);
        }

        /* Delete the secret entry */
        while (SLL_COUNT (&pIf->pAuthPtr->SecurityInfo.SecretList) != 0)
        {
            pDel =
                (tSecret *) SLL_FIRST (&pIf->pAuthPtr->SecurityInfo.SecretList);
            if (pDel != NULL)
            {
                SLL_DELETE (&pIf->pAuthPtr->SecurityInfo.SecretList,
                            (t_SLL_NODE *) pDel);
                FREE_SECRET_ENTRY (pDel);
                pDel = NULL;
            }
        }

        FREE_AUTH_IF (pIf->pAuthPtr);
        pIf->pAuthPtr = NULL;
    }

    if (pIf->pLqmPtr != NULL)
    {

        if (pIf->LcpGSEM.pNegFlagsPerIf[LQR_IDX].FlagMask & ACKED_BY_PEER_MASK)
        {
            PPPStopTimer (&pIf->pLqmPtr->LqmTimerToExpect);
        }

        if (pIf->LcpGSEM.pNegFlagsPerIf[LQR_IDX].FlagMask & ACKED_BY_LOCAL_MASK)
        {
            PPPStopTimer (&pIf->pLqmPtr->LqmTimerToSend);
        }
#ifdef LQM
        FREE_LQM_IF (pIf->pLqmPtr);
        pIf->pLqmPtr = NULL;
#endif
    }

    GSEMDelete (&(pIf->LcpGSEM));
    if ((pIf->BundleFlag == MEMBER_LINK)
        && (pIf->MPInfo.MemberInfo.pBundlePtr != NULL))
    {
        DeleteCPPtrs = PPP_NO;
    }
    pIpcpIf = (tIPCPIf *) pIf->pIpcpPtr;
    if ((DeleteCPPtrs == PPP_YES) && (pIpcpIf != NULL))
    {
        IPCPDeleteIf (pIpcpIf);
    }

#ifdef IPv6CP
    pIp6cpIf = (tIP6CPIf *) pIf->pIp6cpPtr;
    if ((DeleteCPPtrs == PPP_YES) && (pIp6cpIf != NULL))
    {
        IP6CPDeleteIf (pIp6cpIf);
    }
#endif

    pMuxcpIf = (tMuxCPIf *) pIf->pMuxcpPtr;
    if ((DeleteCPPtrs == PPP_YES) && (pMuxcpIf != NULL))
    {
        MuxCPDeleteIf (pMuxcpIf);
    }

    pMplscpIf = (tMPLSCPIf *) pIf->pMplscpPtr;
    if ((DeleteCPPtrs == PPP_YES) && (pMplscpIf != NULL))
    {
        MPLSCPDeleteIf (pMplscpIf);
    }

    pIpxcpIf = (tIPXCPIf *) pIf->pIpxcpPtr;
    if ((DeleteCPPtrs == PPP_YES) && (pIpxcpIf != NULL))
    {
#ifdef REVISIT
        IPXCPDeleteIf (pIpxcpIf);    /* MEM_EST */
#endif
    }
#ifdef BCP
    pBcpIf = (tBCPIf *) pIf->pBcpPtr;
    if ((DeleteCPPtrs == PPP_YES) && (pBcpIf != NULL))
    {
        BCPDeleteIf (pBcpIf);
    }
#endif
#ifdef CCP
    pCCPIf = (tCCPIf *) pIf->pCCPIf;
    if ((DeleteCPPtrs == PPP_YES) && (pCCPIf != NULL))
    {
        CCPDeleteIf (pCCPIf);
    }
#endif
    pECPIf = (tECPIf *) pIf->pECPIf;
    if ((DeleteCPPtrs == PPP_YES) && (pECPIf != NULL))
    {
        ECPDeleteIf (pECPIf);
    }
#ifdef MP
    if (pIf->BundleFlag == BUNDLE)
    {
        MPDelete (pIf);
        /* All bundle related cleaning up is done.
         * Hence return here itself.
         */
        return;
    }
#endif
    if (pIf->BundleFlag == MEMBER_LINK)
    {
        if ((pIf->MPInfo.MemberInfo.pBundlePtr != NULL)
            && (pIf->MPInfo.MemberInfo.PartOfBundle == PPP_YES))
        {
#ifdef MP
            MPDeleteLinkFromBundle (pIf->MPInfo.MemberInfo.pBundlePtr, pIf);
#endif
        }
    }

    SLL_DELETE (&PPPIfList, (t_SLL_NODE *) pIf);
    FREE_PPP_IF (pIf);

    return;
}

/*********************************************************************
*  Function Name : LCPEnableIf
*  Description   :
*        This function is called when there is an Admin Open for an 
*  interface from the SNMP Module.It triggers an  OPEN event to GSEM and 
*  passes  LCP tGSEM corresponds to this interface as parameter.
*  Parameter(s)  :  
*        pIf   -   points to the interface to be enabled
*  Return Value  : VOID 
*********************************************************************/
VOID
LCPEnableIf (tPPPIf * pIf)
{
    UINT1               Index;
    tPPPIf             *pMemberIf;

    pIf->LcpStatus.AdminStatus = ADMIN_OPEN;

    if ((pIf->BundleFlag == BUNDLE)
        && (pIf->LcpStatus.OperStatus == STATUS_DOWN))
    {
        MEMCPY (&pIf->MPInfo.BundleInfo.BundleOptAckedByPeer,
                &pIf->MPInfo.BundleInfo.BundleOptDesired,
                sizeof (tBundleOptions));
        for (Index = 0; Index < MAX_BUNDLE_OPTS; Index++)
        {
            if (pIf->MPInfo.BundleInfo.
                MPFlagsPerIf[Index].FlagMask & DESIRED_SET)
            {
                pIf->MPInfo.BundleInfo.MPFlagsPerIf[Index].FlagMask |=
                    ACKED_BY_PEER_SET;
            }
        }
#ifdef BAP
        if (pIf->MPInfo.BundleInfo.BWMProtocolInfo.Protocol == BAP_PROTOCOL)
        {

            ((tBAPInfo
              *) (pIf->MPInfo.BundleInfo.BWMProtocolInfo.
                  pProtocolInfo))->LocalNextLinkDiscrValue = 0;
            ((tBAPInfo
              *) (pIf->MPInfo.BundleInfo.BWMProtocolInfo.
                  pProtocolInfo))->NextNakLinkDiscrValue =
       BAP_MAX_LINK_DISCR_VAL;
        }
#endif
        MEMCPY (&pIf->CurrentLinkConfigs, &pIf->LinkConfigs,
                sizeof (tLinkConfigs));

        SLL_SCAN (&PPPIfList, pMemberIf, tPPPIf *)
        {
            if (pMemberIf->MPInfo.MemberInfo.pBundlePtr == pIf)
            {
                for (Index = 0; Index < MAX_BUNDLE_OPTS; Index++)
                {
                    if (pIf->MPInfo.BundleInfo.
                        MPFlagsPerIf[Index].FlagMask & DESIRED_SET)
                    {
                        pMemberIf->LcpGSEM.pNegFlagsPerIf[Index].FlagMask |=
                            ACKED_BY_PEER_SET;
                        pMemberIf->LcpGSEM.pNegFlagsPerIf[Index].FlagMask |=
                            DESIRED_SET;
                    }
                }                /* for loop */

                if (pMemberIf->LcpStatus.AdminStatus == ADMIN_OPEN)
                    GSEMRun (&(pMemberIf->LcpGSEM), OPEN);
            }
        }                        /* SLL_SCAN */

        return;
    }

    if (pIf->LcpGSEM.CurrentState != (REQSENT || ACKRCVD || ACKSENT))
    {
        pIf->CallBackTimeInfo.CallBackNegFlags &= CALL_BACK_NOT_REQUESTED;
        MEMCPY (&pIf->LcpOptionsAckedByPeer, &pIf->LcpOptionsDesired,
                sizeof (tLCPOptions));
        MEMCPY (&pIf->CurrentLinkConfigs, &pIf->LinkConfigs,
                sizeof (tLinkConfigs));
        for (Index = 0; Index < MAX_LCP_OPT_TYPES; Index++)
        {
            if (pIf->LcpGSEM.pNegFlagsPerIf[Index].FlagMask & DESIRED_SET)
            {
                pIf->LcpGSEM.pNegFlagsPerIf[Index].FlagMask |=
                    ACKED_BY_PEER_SET;
            }
            else
            {
                pIf->LcpGSEM.pNegFlagsPerIf[Index].FlagMask &=
                    ACKED_BY_PEER_NOT_SET;
            }
        }
    }

    PrintFlags (&pIf->LcpGSEM);

    if (pIf->MPInfo.MemberInfo.pBundlePtr == NULL)
        GSEMRun (&pIf->LcpGSEM, OPEN);
    else
    {
        if (pIf->MPInfo.MemberInfo.pBundlePtr->LcpStatus.AdminStatus ==
            ADMIN_OPEN)
            GSEMRun (&pIf->LcpGSEM, OPEN);
    }

    GSEMRestartIfNeeded (&pIf->LcpGSEM);

    return;
}

/*********************************************************************
*  Function Name : LCPDisableIf
*  Description   :
*                This function is called when there is an Admin Close for an 
*  interface from the SNMP Module.It triggers a DOWN event to the GSEM and 
*  passes the LCP tGSEM corresponds to this interface as parameter. 
*  Parameter(s)  :  
*        pIf    -  points to the  LCP interface structure to be disabled
*  Return Value  : 
*********************************************************************/

VOID
LCPDisableIf (tPPPIf * pIf)
{
    tPPPIf             *pMemberIf;

    tIPCPIf            *pIpcpIf;
#ifdef IPv6CP
    tIP6CPIf           *pIp6cpIf;
#endif
    tMuxCPIf           *pMuxcpIf;
    tMPLSCPIf          *pMplscpIf;
    tIPXCPIf           *pIpxcpIf;
    tCCPIf             *pCCPIf;
    tECPIf             *pECPIf;
#ifdef BAP
    tBACPIf            *pBacpIf;
    tBAPInfo           *pBAPInfo;
#endif

#ifdef RADIUS_WANTED
    pIf->RadInfo.AcctTerminateCause = PPP_RAD_ADMIN_RESET;
#endif
    pIf->LcpStatus.AdminStatus = ADMIN_CLOSE;

    if (pIf->BundleFlag == BUNDLE)
    {

        SLL_SCAN (&PPPIfList, pMemberIf, tPPPIf *)
        {
            if (pMemberIf->MPInfo.MemberInfo.pBundlePtr == pIf)
            {
                GSEMRun (&(pMemberIf->LcpGSEM), CLOSE);
            }
        }

        pIpcpIf = (tIPCPIf *) pIf->pIpcpPtr;
        if (pIpcpIf != NULL)
        {
            pIpcpIf->OperStatus = STATUS_DOWN;
        }

#ifdef IPv6CP
        pIp6cpIf = (tIP6CPIf *) pIf->pIp6cpPtr;
        if (pIp6cpIf != NULL)
        {
            pIp6cpIf->u1OperStatus = STATUS_DOWN;
        }
#endif
        pMuxcpIf = (tMuxCPIf *) pIf->pMuxcpPtr;
        if (pMuxcpIf != NULL)
        {
            pMuxcpIf->OperStatus = STATUS_DOWN;
        }

        pMplscpIf = (tMPLSCPIf *) pIf->pMplscpPtr;
        if (pMplscpIf != NULL)
        {
            pMplscpIf->OperStatus = STATUS_DOWN;
        }

        pIpxcpIf = (tIPXCPIf *) pIf->pIpxcpPtr;
        if (pIpxcpIf != NULL)
        {
            pIpxcpIf->OperStatus = STATUS_DOWN;
        }
        pCCPIf = (tCCPIf *) pIf->pCCPIf;
        if (pCCPIf != NULL)
        {
            pCCPIf->OperStatus = STATUS_DOWN;
        }
        pECPIf = (tECPIf *) pIf->pECPIf;
        if (pECPIf != NULL)
        {
            pECPIf->OperStatus = STATUS_DOWN;
        }
#ifdef BAP
        if (pIf->MPInfo.BundleInfo.BWMProtocolInfo.Protocol == BAP_PROTOCOL)
        {
            pBAPInfo =
                (tBAPInfo *) pIf->MPInfo.BundleInfo.
                BWMProtocolInfo.pProtocolInfo;
            pBacpIf = &pBAPInfo->BACPIf;
            pBacpIf->OperStatus = STATUS_DOWN;
        }
#endif
        return;
    }

    GSEMRun (&pIf->LcpGSEM, CLOSE);

    return;
}

/***********************************************************************/
/*              TIMER INTERFACE FUNCTIONS                              */
/***********************************************************************
*  Function Name : LCPEchoReqTimeOut
*  Description   :
*        This function is called when the retransmission timer expires after
*  the transmission of Echo request packet.
*  Parameter(s)  :
*       pIf         - pointer to the interface structure
*  Return Value  : VOID
*********************************************************************/
VOID
LCPEchoReqTimeOut (VOID *pPppIf)
{
    tPPPIf             *pIf;
    pIf = (tPPPIf *) pPppIf;

    LCPSendEchoReq (pIf, RETRANSMISSION);    /* Retransmit the Echo Req  */

    pIf->EchoInfo.EchoTransmits++;

    return;
}

/***********************************************************************
*  Function Name : LCPIdleTimeOut
*  Description   :
*       This function is called when the Idle timer expires. It generates
*   RXJ- event to make the link down.
*  Parameter(s)  :
*       pIf         - pointer to the PPP interface structure
*  Return Value  : VOID
*********************************************************************/
VOID
LCPIdleTimeOut (VOID *pPppIf)
{
    UINT4               DiffTime;
    tPPPIf             *pIf;
    pIf = (tPPPIf *) pPppIf;

    DiffTime =
        (pIf->LinkInfo.LastPktArrivalTime / SYS_NUM_OF_TIME_UNITS_IN_A_SEC) -
        (pIf->LinkInfo.StartingTime / SYS_NUM_OF_TIME_UNITS_IN_A_SEC);

    if (DiffTime == 0)
    {
#ifdef RADIUS_WANTED
        pIf->RadInfo.AcctTerminateCause = PPP_RAD_IDLE_TIMEOUT;
#endif

        GSEMRun (&pIf->LcpGSEM, RXJ_MINUS);

    }
    else
    {
        pIf->LinkInfo.StartingTime = pIf->LinkInfo.LastPktArrivalTime;
        PPPStartTimer (&pIf->LinkInfo.IdleTimer, LCP_IDLE_TIMER,
                       (UINT4) (pIf->EchoInfo.KeepAliveTimeOutVal * 3));
    }
    return;
}

/********************************************************************************
* Function Name BundleSuspendStatus :
* Checks whether to treigger event when any memberlink of bundle is moving from
Avail state to
* suspended state or coming form suspended state to avail state **
************************************************************************************/

UINT1
BundleSuspendStatus (tPPPIf * pBundleIf, UINT1 LinkAvailStatus)
{

    PPP_UNUSED (pBundleIf);
    PPP_UNUSED (LinkAvailStatus);
    return (FALSE);
}

/*********************************************************************
*  Function Name :LCPTriggerEventToNCP
*  Description   :
*                This function is used to trigger event to NCP protocols.
*  It is used by LCPIndicateQuality() and LCPUpdateAuthStatus() function to
*  to trigger events to NCPs.
*  Parameter(s)  :
*        pIf             - points to the PPP Interface data structure
*        Event           - Event to be triggered to NCP SEM
*        LinkAvailStatus - indicates about the Link Availability 
*  Return Values : VOID
*********************************************************************/
VOID
LCPTriggerEventToNCP (tPPPIf * pIf, UINT1 Event, UINT1 LinkAvailStatus)
{

    tIPCPIf            *pIpcpIf;
#ifdef IPv6CP
    tIP6CPIf           *pIp6cpIf;
#endif
    tMuxCPIf           *pMuxcpIf;
    tMPLSCPIf          *pMplscpIf;
    tIPXCPIf           *pIpxcpIf;
    tBCPIf             *pBCPIf;
    tCCPIf             *pCCPIf;
    tECPIf             *pECPIf;
#ifdef BAP
    tBAPInfo           *pBAPInfo;
    tBACPIf            *pBACPIf;
#endif

    pIf->LcpStatus.LinkAvailability = LinkAvailStatus;

    if (LinkAvailStatus == LINK_NOT_AVAILABLE)
    {
        if ((pIf->BundleFlag != BUNDLE)
            && (pIf->MPInfo.MemberInfo.pBundlePtr != NULL)
            &&
            (SLL_COUNT
             (&
              (pIf->MPInfo.MemberInfo.pBundlePtr->MPInfo.
               BundleInfo.MemberList)) == SINGLE_LINK))
        {

            if (pIf->MPInfo.MemberInfo.pBundlePtr->pCCPIf != NULL)
            {
                pIf->pCCPIf = NULL;
            }
            else
            {
                pCCPIf = (tCCPIf *) pIf->pCCPIf;
                if (pCCPIf != NULL)
                {
                    GSEMRun (&pCCPIf->CCPGSEM, Event);
                }
            }
            if (pIf->MPInfo.MemberInfo.pBundlePtr->pECPIf != NULL)
            {
                pIf->pECPIf = NULL;
            }
            else
            {
                pECPIf = (tECPIf *) pIf->pECPIf;
                if (pECPIf != NULL)
                {
                    GSEMRun (&pECPIf->ECPGSEM, Event);
                }
            }
        }
    }
    if ((pIf->BundleFlag == BUNDLE)
        || (pIf->MPInfo.MemberInfo.pBundlePtr == NULL)
        || ((pIf->MPInfo.MemberInfo.pBundlePtr != NULL)
            &&
            ((SLL_COUNT
              (&pIf->MPInfo.MemberInfo.pBundlePtr->MPInfo.
               BundleInfo.MemberList) == SINGLE_LINK)
             ||
             (BundleSuspendStatus
              (pIf->MPInfo.MemberInfo.pBundlePtr, LinkAvailStatus) == TRUE))))
    {

        /* Start IPCP Protocol */
        pIpcpIf = (tIPCPIf *) pIf->pIpcpPtr;
        if (pIpcpIf != NULL)
        {
            GSEMRun (&pIpcpIf->IpcpGSEM, Event);
        }

#ifdef IPv6CP
        /* Start IP6CP Protocol */
        pIp6cpIf = (tIP6CPIf *) pIf->pIp6cpPtr;
        if (pIp6cpIf != NULL)
        {
            GSEMRun (&pIp6cpIf->Ip6cpGSEM, Event);
        }
#endif

        /* Start MUXCP Protocol */
        pMuxcpIf = (tMuxCPIf *) pIf->pMuxcpPtr;
        if (pMuxcpIf != NULL)
        {
            GSEMRun (&pMuxcpIf->MuxCPGSEM, Event);
        }

        /* Start MPLSCP Protocol */
        pMplscpIf = (tMPLSCPIf *) pIf->pMplscpPtr;
        if (pMplscpIf != NULL)
        {
            GSEMRun (&pMplscpIf->MplscpGSEM, Event);
        }

        /* Start IPXCP Protocol */
        pIpxcpIf = (tIPXCPIf *) pIf->pIpxcpPtr;
        if (pIpxcpIf != NULL)
        {
            GSEMRun (&pIpxcpIf->IpxcpGSEM, Event);
        }

        /* Start BCP Protocol */
        pBCPIf = (tBCPIf *) pIf->pBcpPtr;
        if (pBCPIf != NULL)
        {
            GSEMRun (&pBCPIf->BcpGSEM, Event);
        }

#ifdef BAP
        /* Start BACP Protocol */
        if ((pIf->MPInfo.MemberInfo.pBundlePtr != NULL)
            && (pIf->MPInfo.MemberInfo.pBundlePtr->MPInfo.
                BundleInfo.BWMProtocolInfo.Protocol == BAP_PROTOCOL))
        {
            pBAPInfo =
                (tBAPInfo *) pIf->MPInfo.MemberInfo.pBundlePtr->
                MPInfo.BundleInfo.BWMProtocolInfo.pProtocolInfo;
            pBACPIf = &pBAPInfo->BACPIf;
            GSEMRun (&pBACPIf->BACPGSEM, Event);
        }
#endif
    }
    if ((pIf->BundleFlag == BUNDLE)
        || (pIf->MPInfo.MemberInfo.pBundlePtr == NULL)
        || ((pIf->MPInfo.MemberInfo.pBundlePtr != NULL)
            && SLL_COUNT (&pIf->MPInfo.MemberInfo.pBundlePtr->MPInfo.
                          BundleInfo.MemberList) == SINGLE_LINK)
        || ((pIf->MPInfo.MemberInfo.pBundlePtr != NULL)
            && (LinkAvailStatus == LINK_AVAILABLE)
            && (pIf->MPInfo.MemberInfo.pBundlePtr->pCCPIf != pIf->pCCPIf)))
    {
        /* Start CCP Protocol */
        pCCPIf = (tCCPIf *) pIf->pCCPIf;
        if (pCCPIf != NULL)
        {
            GSEMRun (&pCCPIf->CCPGSEM, Event);
        }
    }
    if ((pIf->BundleFlag == BUNDLE)
        || (pIf->MPInfo.MemberInfo.pBundlePtr == NULL)
        || ((pIf->MPInfo.MemberInfo.pBundlePtr != NULL)
            && SLL_COUNT (&pIf->MPInfo.MemberInfo.pBundlePtr->MPInfo.
                          BundleInfo.MemberList) == SINGLE_LINK)
        || ((pIf->MPInfo.MemberInfo.pBundlePtr != NULL)
            && (LinkAvailStatus == LINK_AVAILABLE)
            && (pIf->MPInfo.MemberInfo.pBundlePtr->pECPIf != pIf->pECPIf)))
    {
        /* Start ECP Protocol */
        pECPIf = (tECPIf *) pIf->pECPIf;
        if (pECPIf != NULL)
        {
            GSEMRun (&pECPIf->ECPGSEM, Event);
        }
    }
    return;
}

/*********************************************************************
*  Function Name :LCPEnterNetworkLayerPhase
*  Description   :
*     This function is used to trigger NCPs and insert the Link in the 
*  Member List , if MP is negotiated.  It also sets the OperStatus of 
*  the Link to UP.
*  Parameter(s)  :
*        pIf             - points to the PPP Interface data structure
*  Return Values : VOID
*********************************************************************/
VOID
LCPEnterNetworkLayerPhase (tPPPIf * pIf)
{

#ifdef L2TP_WANTED
    if ((PPPIsL2TPTunnelVoluntary (pIf) == FALSE) &&
        (PPPIsL2TPTunnelModeLAC (pIf) == TRUE))
        return;
#endif

    if (pIf->LcpGSEM.
        pNegFlagsPerIf[CALL_BACK_IDX].FlagMask & ACKED_BY_LOCAL_MASK)
    {
        PPP_DBG
            ("Call back successfully negotiated - Terminating (if necessary ) LCP without entering network layer phase");
        if (TakeLCPCallBackAction (pIf) == IMMEDIATE)
        {
            PPP_DBG
                ("Network Layer phase not entered due to callback negotiation and immediate configuration.");
            return;
        }
    }

    pIf->LcpStatus.LinkAvailability = LINK_AVAILABLE;
    PPP_DBG ("Negotiation NCPs....");

    /* Update the bundle information if necessary.... */
    if (pIf->MPInfo.MemberInfo.pBundlePtr != NULL)
    {
        MEMCPY (&pIf->MPInfo.BundleInfo.BundleOptAckedByPeer,
                &pIf->MPInfo.MemberInfo.pBundlePtr->MPInfo.
                BundleInfo.BundleOptAckedByPeer, sizeof (tBundleOptions));
        MEMCPY (&pIf->MPInfo.BundleInfo.BundleOptAckedByLocal,
                &pIf->MPInfo.MemberInfo.pBundlePtr->MPInfo.
                BundleInfo.BundleOptAckedByLocal, sizeof (tBundleOptions));
        if ((pIf->LcpGSEM.
             pNegFlagsPerIf[MRRU_IDX].FlagMask & ACKED_BY_PEER_MASK)
            || (pIf->LcpGSEM.
                pNegFlagsPerIf[MRRU_IDX].FlagMask & ACKED_BY_LOCAL_MASK))
        {
#ifdef MP
            MPInsertMemberIntoProperBundle (pIf,
                                            pIf->MPInfo.MemberInfo.pBundlePtr);
#endif
        }
    }

#ifdef RADIUS_WANTED
    if ((gu1aaaMethod == AAA_RADIUS) && (pIf->RadInfo.AcctNeeded == PPP_YES))
        PppRadAccounting (pIf, PPP_ACCT_START);
#endif
    pIf->LcpStatus.OperStatus = STATUS_UP;
    PPPHLINotifyIfStatusToHL (pIf, PPP_UP);
    LCPTriggerEventToNCP (pIf, PPP_UP, LINK_AVAILABLE);

    return;
}

/*********************************************************************
*  Function Name :LCPLeaveNetworkLayerPhase
*  Description   :
*     This function is used to trigger NCPs and insert the Link in the 
*  Member List , if MP is negotiated.  It also sets the OperStatus of 
*  the Link to UP.
*  Parameter(s)  :
*        pIf             - points to the PPP Interface data structure
*  Return Values : VOID
*********************************************************************/
VOID
LCPLeaveNetworkLayerPhase (tPPPIf * pIf)
{
    pIf->LcpStatus.OperStatus = STATUS_DOWN;

    PPPHLINotifyIfStatusToHL (pIf, PPP_DOWN);

    /*  Trigger DOWN event to all NCP modules that are active */
    LCPTriggerEventToNCP (pIf, PPP_DOWN, LINK_NOT_AVAILABLE);

#ifdef RADIUS_WANTED
    if ((gu1aaaMethod == AAA_RADIUS) && (pIf->RadInfo.AcctNeeded == PPP_YES))
        PppRadAccounting (pIf, PPP_ACCT_STOP);
    pIf->RadInfo.AcctNeeded = PPP_NO;
#endif

    /* Update the bundle information if necessary.... */
    if (pIf->MPInfo.MemberInfo.PartOfBundle == PPP_YES)
    {
        if ((pIf->LcpGSEM.
             pNegFlagsPerIf[MRRU_IDX].FlagMask & ACKED_BY_PEER_MASK)
            || (pIf->LcpGSEM.
                pNegFlagsPerIf[MRRU_IDX].FlagMask & ACKED_BY_LOCAL_MASK))
        {
#ifdef MP
            MPDeleteMemberFromBundle (pIf->MPInfo.MemberInfo.pBundlePtr, pIf);
#endif
        }
    }
    return;
}
