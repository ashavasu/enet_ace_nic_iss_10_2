/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppplnkll.c,v 1.6 2014/11/25 13:03:35 siva Exp $
 *
 * Description:This file contains low level routines of PPP module.
 *
 *******************************************************************/
#include "pppsnmpm.h"
#include "lcpcom.h"
#include "snmccons.h"
#include "lcpexts.h"
#include "pppmp.h"
#include "mpexts.h"
#include "gsemdefs.h"

#include "mpdefs.h"
#include "pppeccmn.h"
#include "pppccp.h"
#include "pppeccmn.h"
#include "pppecp.h"
#include "ecpexts.h"
#include "pppipcp.h"
#include "ipexts.h"
#include "pppipv6cp.h"
#include "ipv6exts.h"
#include "pppmuxcp.h"
#include "pppmplscp.h"
#include "mplsexts.h"
#include "pppipxcp.h"
#include "ipxexts.h"
#include "pppbcp.h"
#include "bcpexts.h"
#ifdef BAP
#include "pppbacp.h"
#include "bacpexts.h"
#endif

#include "snmpexts.h"
#include "globexts.h"
#include "llproto.h"
#include "pppstlow.h"
#include "mpproto.h"
#include "ccpproto.h"

extern UINT1        gDefaultRxACCM[];

#define MAGIC_NUMBER_SET  2
#define MAGIC_NUMBER_NOT_SET  1
#define MIN_LINK_CONFIG_FCS_SIZE 0
#define MAX_LINK_CONFIG_FCS_SIZE 128
#define MIN_LINK_CONFIG_RECEIVE_ACCMAP 0
#define MAX_LINK_CONFIG_RECEIVE_ACCMAP 4
#define MIN_LINK_CONFIG_TRANSMIT_ACCMAP 0
#define MAX_LINK_CONFIG_TRANSMIT_ACCMAP 4

INT1                (*CheckAndGetInstancePtr1[]) (tPPPIf *) =
{

    CheckAndGetIpPtr,
        CheckAndGetIp6Ptr,
        CheckAndGetMuxCPPtr,
        CheckAndGetMplsPtr,
        CheckAndGetIpxPtr,
#ifdef MP
        CheckAndGetMpPtr,
#endif
#ifdef CCP
        CheckAndGetCCPPtr, CheckAndGetECPPtr, 
#endif
#ifdef BCP
        CheckAndGetBCPPtr,
#endif
#ifdef BAP
        CheckAndGetBACPPtr, CheckAndGetRMInfo,
#endif
#ifdef LQM
	CheckAndGetLqrPtr, 
#endif
	CheckAndGetLinkPtr};

INT1                (*CheckAndGetInstancePtr2[]) (tPPPIf *, UINT4 *, UINT4 *,
                                                  UINT4) =
{
#ifdef CCP
	CheckAndGetCCPTxPtr,
        CheckAndGetCCPRxPtr,
        CheckAndGetECPTxPtr,
        CheckAndGetECPRxPtr,
#endif
#ifdef BCP
        CheckAndGetBCPMacPtr, 
#endif
	CheckAndGetSecurityPtr, 
	CheckAndGetSecretsPtr,};

INT1
CheckAndGetLinkPtr (tPPPIf * pIf)
{
    PPP_UNUSED (pIf);
    return (PPP_SNMP_OK);
}

INT1
getInstanceForTwoIndices (INT4 *Index1, INT4 *Index2, UINT1 InstanceType,
                          INT1 InstCode)
{
    UINT4               n1, n2;
    tPPPIf             *pIf = NULL;
    UINT2               u2Temp;
    UINT2               InputIndex2 = (UINT2) *Index2;

    *Index2 = 0;
    if (InstCode == FIRST_INST)
    {
        *Index1 = 1;
    }

    n1 = MAX_INTEGER;
    n2 = MAX_INTEGER;
    u2Temp = (UINT2) (*Index2);

    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if ((pIf != NULL) && (pIf->LinkInfo.IfIndex >= (UINT4) *Index1)
            && (pIf->LinkInfo.IfIndex < n1))
        {
            if (pIf->LinkInfo.IfIndex == (UINT4) (*Index1))
            {
                (*Index2) = InputIndex2 + 1;
            }

            (*CheckAndGetInstancePtr2[InstanceType]) (pIf, &n1, &n2,
                                                      (UINT4) *Index2);

            if (InstCode == NEXT_INST)
                *Index2 = 0;
        }
    }
    *Index2 = u2Temp;

    if ((n1 == MAX_INTEGER) || (n2 == MAX_INTEGER))
    {
        return (SNMP_FAILURE);
    }
    *Index1 = (INT4) n1;
    *Index2 = (INT4) n2;
    return (SNMP_SUCCESS);
}

INT1
getInstanceForOneIndex (INT4 *Index, UINT1 InstanceType, INT1 InstCode)
{
    UINT4               next;
    tPPPIf             *pIf = NULL;
    if (InstCode == NEXT_INST)
    {
        *Index = *Index + 1;
    }
    else
    {
        *Index = 1;
    }

    next = MAX_INTEGER;
    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if ((pIf != NULL) && (pIf->LinkInfo.IfIndex >= (UINT4) *Index)
            && (pIf->LinkInfo.IfIndex < next))
        {

            if ((InstanceType == MpInstance) && (pIf->BundleFlag != BUNDLE))
            {
                continue;
            }
#ifdef BAP
            if ((InstanceType == BACPInstance) && (pIf->BundleFlag != BUNDLE))
            {
                continue;
            }
#endif
            if ((*CheckAndGetInstancePtr1[InstanceType]) (pIf) == PPP_SNMP_OK)
            {
                next = pIf->LinkInfo.IfIndex;
            }
        }

    }

    if (next == MAX_INTEGER)
    {
        return (SNMP_FAILURE);
    }
    *Index = (INT4) next;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  getPhysicalIndexOfPPP
 Input       :  pppIndex - PppInterfaceIndex
                PhyIndex - Physical Interface of the PPP Index
 Output      :  This Routine take the PPP Index & Sets the physical interface
                accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/

INT1
getPhysicalIndexOfPPP (INT4 pppIndex, INT4 *PhyIndex)
{
    tPPPIf             *pIf = NULL;

    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if ((pIf != NULL) && (pIf->LinkInfo.IfIndex == (UINT4) pppIndex))
        {

            *PhyIndex = (INT4) pIf->LinkInfo.PhysIfIndex;
            return (SNMP_SUCCESS);
        }

    }

    return (SNMP_FAILURE);
}

/*********************************************************************/
tPPPIf             *
PppGetPppIfPtr (Index)
     UINT4               Index;
{
    tPPPIf             *pIf = NULL;
    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if ((pIf->LinkInfo.IfIndex == Index)
            && (pIf->BundleFlag == MEMBER_LINK))
        {
            return (pIf);
        }
    }
    return (NULL);
}

/***********************************************************************/
tPPPIf             *
PppGetIfPtr (UINT4 Index)
{
    tPPPIf             *pIf = NULL;
    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if (pIf->LinkInfo.IfIndex == Index)
            return (pIf);
    }
    return (NULL);
}

#ifdef PPP_STACK_WANTED            /* DSL_ADD */
/******************************************************************
* low level GET routines for   PPP LINK STATUS Table
******************************************************************/
/****************************************************************************
 Function    :  nmhValidateIndexInstancePppLinkStatusTable
 Input       :  The Indices
                PppLinkStatusIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppLinkStatusTable (INT4 i4PppLinkStatusIfIndex)
{
    if (PppGetPppIfPtr (i4PppLinkStatusIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppLinkStatusTable
 Input       :  The Indices
                PppLinkStatusIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppLinkStatusTable (INT4 *pi4PppLinkStatusIfIndex)
{
    if (nmhGetFirstIndex (pi4PppLinkStatusIfIndex, LinkInstance, FIRST_INST) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppLinkStatusTable
 Input       :  The Indices
                PppLinkStatusIfIndex
                nextPppLinkStatusIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppLinkStatusTable (INT4 i4PppLinkStatusIfIndex,
                                   INT4 *pi4NextPppLinkStatusIfIndex)
{
    if (nmhGetNextIndex (&i4PppLinkStatusIfIndex, LinkInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextPppLinkStatusIfIndex = i4PppLinkStatusIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppLinkStatusPhysicalIndex
 Input       :  The Indices
                PppLinkStatusIfIndex

                The Object 
                retValPppLinkStatusPhysicalIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppLinkStatusPhysicalIndex (INT4 i4PppLinkStatusIfIndex,
                                  INT4 *pi4RetValPppLinkStatusPhysicalIndex)
{

    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr (i4PppLinkStatusIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pIf->LcpStatus.OperStatus == STATUS_DOWN)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValPppLinkStatusPhysicalIndex = pIf->LinkInfo.PhysIfIndex;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppLinkStatusBadAddresses
 Input       :  The Indices
                PppLinkStatusIfIndex

                The Object 
                retValPppLinkStatusBadAddresses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppLinkStatusBadAddresses (INT4 i4PppLinkStatusIfIndex,
                                 UINT4 *pu4RetValPppLinkStatusBadAddresses)
{

    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr (i4PppLinkStatusIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pIf->LcpStatus.OperStatus == STATUS_DOWN)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValPppLinkStatusBadAddresses = pIf->LLHCounters.BadAddressCounter;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppLinkStatusBadControls
 Input       :  The Indices
                PppLinkStatusIfIndex

                The Object 
                retValPppLinkStatusBadControls
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppLinkStatusBadControls (INT4 i4PppLinkStatusIfIndex,
                                UINT4 *pu4RetValPppLinkStatusBadControls)
{

    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr (i4PppLinkStatusIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pIf->LcpStatus.OperStatus == STATUS_DOWN)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValPppLinkStatusBadControls = pIf->LLHCounters.BadControlCounter;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPppLinkStatusPacketTooLongs
 Input       :  The Indices
                PppLinkStatusIfIndex

                The Object 
                retValPppLinkStatusPacketTooLongs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppLinkStatusPacketTooLongs (INT4 i4PppLinkStatusIfIndex,
                                   UINT4 *pu4RetValPppLinkStatusPacketTooLongs)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr (i4PppLinkStatusIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pIf->LcpStatus.OperStatus == STATUS_DOWN)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValPppLinkStatusPacketTooLongs =
        pIf->LLHCounters.PacketTooLongCounter;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppLinkStatusBadFCSs
 Input       :  The Indices
                PppLinkStatusIfIndex

                The Object 
                retValPppLinkStatusBadFCSs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppLinkStatusBadFCSs (INT4 i4PppLinkStatusIfIndex,
                            UINT4 *pu4RetValPppLinkStatusBadFCSs)
{

    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr (i4PppLinkStatusIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pIf->LcpStatus.OperStatus == STATUS_DOWN)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValPppLinkStatusBadFCSs = pIf->LLHCounters.BadFcsCounter;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppLinkStatusLocalMRU
 Input       :  The Indices
                PppLinkStatusIfIndex

                The Object 
                retValPppLinkStatusLocalMRU
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppLinkStatusLocalMRU (INT4 i4PppLinkStatusIfIndex,
                             INT4 *pi4RetValPppLinkStatusLocalMRU)
{

    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr (i4PppLinkStatusIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pIf->LcpStatus.OperStatus == STATUS_DOWN)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppLinkStatusLocalMRU = pIf->LcpOptionsAckedByPeer.MRU;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppLinkStatusRemoteMRU
 Input       :  The Indices
                PppLinkStatusIfIndex

                The Object 
                retValPppLinkStatusRemoteMRU
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppLinkStatusRemoteMRU (INT4 i4PppLinkStatusIfIndex,
                              INT4 *pi4RetValPppLinkStatusRemoteMRU)
{

    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr (i4PppLinkStatusIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pIf->LcpStatus.OperStatus == STATUS_DOWN)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppLinkStatusRemoteMRU = pIf->LcpOptionsAckedByLocal.MRU;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppLinkStatusLocalToRemoteProtocolCompression
 Input       :  The Indices
                PppLinkStatusIfIndex

                The Object 
                retValPppLinkStatusLocalToRemoteProtocolCompression
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppLinkStatusLocalToRemoteProtocolCompression (INT4
                                                     i4PppLinkStatusIfIndex,
                                                     INT4
                                                     *pi4RetValPppLinkStatusLocalToRemoteProtocolCompression)
{

    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr (i4PppLinkStatusIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pIf->LcpStatus.OperStatus == STATUS_DOWN)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppLinkStatusLocalToRemoteProtocolCompression =
        (pIf->LcpGSEM.pNegFlagsPerIf[PFC_IDX].
         FlagMask & ACKED_BY_LOCAL_MASK) ? 1 : 2;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppLinkStatusRemoteToLocalProtocolCompression
 Input       :  The Indices
                PppLinkStatusIfIndex

                The Object 
                retValPppLinkStatusRemoteToLocalProtocolCompression
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppLinkStatusRemoteToLocalProtocolCompression (INT4
                                                     i4PppLinkStatusIfIndex,
                                                     INT4
                                                     *pi4RetValPppLinkStatusRemoteToLocalProtocolCompression)
{

    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr (i4PppLinkStatusIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pIf->LcpStatus.OperStatus == STATUS_DOWN)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValPppLinkStatusRemoteToLocalProtocolCompression =
        (pIf->LcpGSEM.pNegFlagsPerIf[PFC_IDX].
         FlagMask & ACKED_BY_PEER_MASK) ? 1 : 2;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppLinkStatusLocalToRemoteACCompression
 Input       :  The Indices
                PppLinkStatusIfIndex

                The Object 
                retValPppLinkStatusLocalToRemoteACCompression
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppLinkStatusLocalToRemoteACCompression (INT4 i4PppLinkStatusIfIndex,
                                               INT4
                                               *pi4RetValPppLinkStatusLocalToRemoteACCompression)
{

    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr (i4PppLinkStatusIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pIf->LcpStatus.OperStatus == STATUS_DOWN)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValPppLinkStatusLocalToRemoteACCompression =
        (pIf->LcpGSEM.pNegFlagsPerIf[ACFC_IDX].
         FlagMask & ACKED_BY_LOCAL_MASK) ? 1 : 2;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppLinkStatusRemoteToLocalACCompression
 Input       :  The Indices
                PppLinkStatusIfIndex

                The Object 
                retValPppLinkStatusRemoteToLocalACCompression
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppLinkStatusRemoteToLocalACCompression (INT4 i4PppLinkStatusIfIndex,
                                               INT4
                                               *pi4RetValPppLinkStatusRemoteToLocalACCompression)
{

    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr (i4PppLinkStatusIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pIf->LcpStatus.OperStatus == STATUS_DOWN)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValPppLinkStatusRemoteToLocalACCompression =
        (pIf->LcpGSEM.pNegFlagsPerIf[ACFC_IDX].
         FlagMask & ACKED_BY_PEER_MASK) ? 1 : 2;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppLinkStatusTransmitFcsSize
 Input       :  The Indices
                PppLinkStatusIfIndex

                The Object 
                retValPppLinkStatusTransmitFcsSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppLinkStatusTransmitFcsSize (INT4 i4PppLinkStatusIfIndex,
                                    INT4 *pi4RetValPppLinkStatusTransmitFcsSize)
{

    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr (i4PppLinkStatusIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pIf->LcpStatus.OperStatus == STATUS_DOWN)
    {
        return (SNMP_FAILURE);
    }

    switch (pIf->LcpOptionsAckedByLocal.FCSSize)
    {
        case NO_FCS_REQ:
            *pi4RetValPppLinkStatusTransmitFcsSize = 0;
            break;
        case FCS_16_REQ:
            *pi4RetValPppLinkStatusTransmitFcsSize = 16;
            break;
        case FCS_32_REQ:
            *pi4RetValPppLinkStatusTransmitFcsSize = 32;
            break;
        case FCS_48_REQ:
            *pi4RetValPppLinkStatusTransmitFcsSize = 48;
            break;
        default:
            return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppLinkStatusReceiveFcsSize
 Input       :  The Indices
                PppLinkStatusIfIndex

                The Object 
                retValPppLinkStatusReceiveFcsSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppLinkStatusReceiveFcsSize (INT4 i4PppLinkStatusIfIndex,
                                   INT4 *pi4RetValPppLinkStatusReceiveFcsSize)
{

    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr (i4PppLinkStatusIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pIf->LcpStatus.OperStatus == STATUS_DOWN)
    {
        return (SNMP_FAILURE);
    }

    switch (pIf->LcpOptionsAckedByPeer.FCSSize)
    {
        case NO_FCS_REQ:
            *pi4RetValPppLinkStatusReceiveFcsSize = 0;
            break;
        case FCS_16_REQ:
            *pi4RetValPppLinkStatusReceiveFcsSize = 16;
            break;
        case FCS_32_REQ:
            *pi4RetValPppLinkStatusReceiveFcsSize = 32;
            break;
        case FCS_48_REQ:
            *pi4RetValPppLinkStatusReceiveFcsSize = 48;
            break;
        default:
            return (SNMP_FAILURE);
            break;
    }
    return (SNMP_SUCCESS);

}
#endif /* DSL_ADD */

/* LOW LEVEL Routines for Table : PppLinkConfigTable. */
/****************************************************************************
 Function    :  nmhGetPppLinkStatusLocalToPeerACCMap
 Input       :  The Indices
                PppLinkStatusIfIndex

                The Object 
                retValPppLinkStatusLocalToPeerACCMap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppLinkStatusLocalToPeerACCMap (INT4 i4PppLinkStatusIfIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValPppLinkStatusLocalToPeerACCMap)
{

    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppLinkStatusIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pIf->LcpStatus.OperStatus == STATUS_DOWN)
    {
        return (SNMP_FAILURE);
    }

    pRetValPppLinkStatusLocalToPeerACCMap->i4_Length = 4;
    MEMCPY ((char *) pRetValPppLinkStatusLocalToPeerACCMap->pu1_OctetList,
            (const char *) pIf->LcpOptionsAckedByLocal.AsyncMap,
            pRetValPppLinkStatusLocalToPeerACCMap->i4_Length);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppLinkStatusPeerToLocalACCMap
 Input       :  The Indices
                PppLinkStatusIfIndex

                The Object 
                retValPppLinkStatusPeerToLocalACCMap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppLinkStatusPeerToLocalACCMap (INT4 i4PppLinkStatusIfIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValPppLinkStatusPeerToLocalACCMap)
{

    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppLinkStatusIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pIf->LcpStatus.OperStatus == STATUS_DOWN)
    {
        return (SNMP_FAILURE);
    }

    pRetValPppLinkStatusPeerToLocalACCMap->i4_Length = 4;
    MEMCPY (pRetValPppLinkStatusPeerToLocalACCMap->pu1_OctetList,
            (const char *) pIf->LcpOptionsAckedByPeer.AsyncMap,
            pRetValPppLinkStatusPeerToLocalACCMap->i4_Length);
    return (SNMP_SUCCESS);

}

/* LOW LEVEL Routines for Table : PppLinkStatusTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppLinkConfigTable
 Input       :  The Indices
                PppLinkConfigIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppLinkConfigTable (INT4 i4PppLinkConfigIfIndex)
{
    if (PppGetPppIfPtr ((UINT4) i4PppLinkConfigIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppLinkConfigTable
 Input       :  The Indices
                PppLinkConfigIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppLinkConfigTable (INT4 *pi4PppLinkConfigIfIndex)
{
    if (nmhGetFirstIndex (pi4PppLinkConfigIfIndex, LinkInstance, FIRST_INST) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppLinkConfigTable
 Input       :  The Indices
                PppLinkConfigIfIndex
                nextPppLinkConfigIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppLinkConfigTable (INT4 i4PppLinkConfigIfIndex,
                                   INT4 *pi4NextPppLinkConfigIfIndex)
{
    if (nmhGetNextIndex (&i4PppLinkConfigIfIndex, LinkInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextPppLinkConfigIfIndex = i4PppLinkConfigIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppLinkConfigInitialMRU
 Input       :  The Indices
                PppLinkConfigIfIndex

                The Object 
                retValPppLinkConfigInitialMRU
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppLinkConfigInitialMRU (INT4 i4PppLinkConfigIfIndex,
                               INT4 *pi4RetValPppLinkConfigInitialMRU)
{

    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppLinkConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValPppLinkConfigInitialMRU = pIf->LcpOptionsDesired.MRU;
    return (SNMP_SUCCESS);

}

#ifdef PPP_STACK_WANTED            /* DSL_ADD */
/****************************************************************************
 Function    :  nmhGetPppLinkConfigReceiveACCMap
 Input       :  The Indices
                PppLinkConfigIfIndex

                The Object 
                retValPppLinkConfigReceiveACCMap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppLinkConfigReceiveACCMap (INT4 i4PppLinkConfigIfIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValPppLinkConfigReceiveACCMap)
{

    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr (i4PppLinkConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pRetValPppLinkConfigReceiveACCMap->i4_Length = 4;
    MEMCPY (pRetValPppLinkConfigReceiveACCMap->pu1_OctetList,
            (const char *) pIf->LcpOptionsAllowedForPeer.AsyncMap,
            pRetValPppLinkConfigReceiveACCMap->i4_Length);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppLinkConfigTransmitACCMap
 Input       :  The Indices
                PppLinkConfigIfIndex

                The Object 
                retValPppLinkConfigTransmitACCMap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppLinkConfigTransmitACCMap (INT4 i4PppLinkConfigIfIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValPppLinkConfigTransmitACCMap)
{

    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr (i4PppLinkConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pRetValPppLinkConfigTransmitACCMap->i4_Length = 4;
    MEMCPY (pRetValPppLinkConfigTransmitACCMap->pu1_OctetList,
            (const char *) pIf->LcpOptionsAllowedForPeer.AsyncMap,
            pRetValPppLinkConfigTransmitACCMap->i4_Length);
    return (SNMP_SUCCESS);

}
#endif /* DSL_ADD */

/****************************************************************************
 Function    :  nmhGetPppLinkConfigMagicNumber
 Input       :  The Indices
                PppLinkConfigIfIndex

                The Object 
                retValPppLinkConfigMagicNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppLinkConfigMagicNumber (INT4 i4PppLinkConfigIfIndex,
                                INT4 *pi4RetValPppLinkConfigMagicNumber)
{

    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppLinkConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValPppLinkConfigMagicNumber =
        (pIf->LcpGSEM.pNegFlagsPerIf[MAGIC_IDX].
         FlagMask & DESIRED_MASK) ? 2 : 1;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppLinkConfigFcsSize
 Input       :  The Indices
                PppLinkConfigIfIndex

                The Object 
                retValPppLinkConfigFcsSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppLinkConfigFcsSize (INT4 i4PppLinkConfigIfIndex,
                            INT4 *pi4RetValPppLinkConfigFcsSize)
{

    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppLinkConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    switch (pIf->LcpOptionsDesired.FCSSize)
    {
        case NO_FCS_REQ:
            *pi4RetValPppLinkConfigFcsSize = 0;
            break;
        case FCS_16_REQ:
            *pi4RetValPppLinkConfigFcsSize = 16;
            break;
        case FCS_32_REQ:
            *pi4RetValPppLinkConfigFcsSize = 32;
            break;
        case FCS_48_REQ:
            *pi4RetValPppLinkConfigFcsSize = 48;
            break;
        default:
            return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppLinkConfigInitialMRU
 Input       :  The Indices
                PppLinkConfigIfIndex

                The Object 
                testValPppLinkConfigInitialMRU
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppLinkConfigInitialMRU (UINT4 *pu4ErrorCode,
                                  INT4 i4PppLinkConfigIfIndex,
                                  INT4 i4TestValPppLinkConfigInitialMRU)
{

    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetIfPtr ((UINT4) i4PppLinkConfigIfIndex)) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        PPP_UNUSED (pIf);
        return (SNMP_FAILURE);
    }

    if ((i4TestValPppLinkConfigInitialMRU > MIN_MRU_SIZE)
        && (i4TestValPppLinkConfigInitialMRU < MAX_INTEGER))
    {
        return (SNMP_SUCCESS);
    }

    else
    {
        if (i4TestValPppLinkConfigInitialMRU != 0)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (SNMP_FAILURE);
        }
    }
    return (SNMP_SUCCESS);
}

#ifdef PPP_STACK_WANTED            /* DSL_ADD */
/****************************************************************************
 Function    :  nmhTestv2PppLinkConfigReceiveACCMap
 Input       :  The Indices
                PppLinkConfigIfIndex

                The Object 
                testValPppLinkConfigReceiveACCMap
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppLinkConfigReceiveACCMap (UINT4 *pu4ErrorCode,
                                     INT4 i4PppLinkConfigIfIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pTestValPppLinkConfigReceiveACCMap)
{

    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetIfPtr (i4PppLinkConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pTestValPppLinkConfigReceiveACCMap->i4_Length <
        MIN_LINK_CONFIG_RECEIVE_ACCMAP
        || pTestValPppLinkConfigReceiveACCMap->i4_Length >
        MAX_LINK_CONFIG_RECEIVE_ACCMAP)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppLinkConfigTransmitACCMap
 Input       :  The Indices
                PppLinkConfigIfIndex

                The Object 
                testValPppLinkConfigTransmitACCMap
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppLinkConfigTransmitACCMap (UINT4 *pu4ErrorCode,
                                      INT4 i4PppLinkConfigIfIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pTestValPppLinkConfigTransmitACCMap)
{

    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetIfPtr (i4PppLinkConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pTestValPppLinkConfigTransmitACCMap->i4_Length <
        MIN_LINK_CONFIG_TRANSMIT_ACCMAP
        || pTestValPppLinkConfigTransmitACCMap->i4_Length >
        MAX_LINK_CONFIG_TRANSMIT_ACCMAP)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}
#endif /* DSL_ADD */

/****************************************************************************
 Function    :  nmhTestv2PppLinkConfigMagicNumber
 Input       :  The Indices
                PppLinkConfigIfIndex

                The Object 
                testValPppLinkConfigMagicNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppLinkConfigMagicNumber (UINT4 *pu4ErrorCode,
                                   INT4 i4PppLinkConfigIfIndex,
                                   INT4 i4TestValPppLinkConfigMagicNumber)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetIfPtr ((UINT4) i4PppLinkConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        PPP_UNUSED (pIf);
        return (SNMP_FAILURE);
    }

    if (i4TestValPppLinkConfigMagicNumber < MAGIC_NUMBER_NOT_SET
        || i4TestValPppLinkConfigMagicNumber > MAGIC_NUMBER_SET)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppLinkConfigFcsSize
 Input       :  The Indices
                PppLinkConfigIfIndex

                The Object 
                testValPppLinkConfigFcsSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppLinkConfigFcsSize (UINT4 *pu4ErrorCode, INT4 i4PppLinkConfigIfIndex,
                               INT4 i4TestValPppLinkConfigFcsSize)
{

    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetIfPtr ((UINT4) i4PppLinkConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        PPP_UNUSED (pIf);
        return (SNMP_FAILURE);
    }

    if (i4TestValPppLinkConfigFcsSize < MIN_LINK_CONFIG_FCS_SIZE
        || i4TestValPppLinkConfigFcsSize > MAX_LINK_CONFIG_FCS_SIZE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    switch (i4TestValPppLinkConfigFcsSize)
    {
        case 0:
        case 16:
        case 32:
        case 48:
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (SNMP_FAILURE);
            break;
    }
    return (SNMP_SUCCESS);

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2PppLinkConfigTable
 Input       :  The Indices
                IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PppLinkConfigTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppLinkConfigInitialMRU
 Input       :  The Indices
                PppLinkConfigIfIndex

                The Object 
                setValPppLinkConfigInitialMRU
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppLinkConfigInitialMRU (INT4 i4PppLinkConfigIfIndex,
                               INT4 i4SetValPppLinkConfigInitialMRU)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppLinkConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (i4SetValPppLinkConfigInitialMRU == 0
        || i4SetValPppLinkConfigInitialMRU == DEF_MRU_SIZE)
    {
        pIf->LcpOptionsDesired.MRU = DEF_MRU_SIZE;
        pIf->LcpGSEM.pNegFlagsPerIf[MRU_IDX].FlagMask &= DESIRED_NOT_SET;
    }
    else
    {
        pIf->LcpGSEM.pNegFlagsPerIf[MRU_IDX].FlagMask |= DESIRED_SET;
        pIf->LcpOptionsDesired.MRU = (UINT2) i4SetValPppLinkConfigInitialMRU;
    }
    return (SNMP_SUCCESS);
}

#ifdef PPP_STACK_WANTED            /* DSL_ADD */
/****************************************************************************
 Function    :  nmhSetPppLinkConfigReceiveACCMap
 Input       :  The Indices
                PppLinkConfigIfIndex

                The Object 
                setValPppLinkConfigReceiveACCMap
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppLinkConfigReceiveACCMap (INT4 i4PppLinkConfigIfIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pSetValPppLinkConfigReceiveACCMap)
{

    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr (i4PppLinkConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if ((pSetValPppLinkConfigReceiveACCMap->i4_Length ==
         MAX_LINK_CONFIG_RECEIVE_ACCMAP)
        &&
        (STRNCMP
         (gDefaultRxACCM, pSetValPppLinkConfigReceiveACCMap->pu1_OctetList,
          pSetValPppLinkConfigReceiveACCMap->i4_Length) == 0))
    {

        pIf->LcpGSEM.pNegFlagsPerIf[ACCM_IDX].FlagMask &= DESIRED_NOT_SET;
        return (SNMP_SUCCESS);
    }

    pIf->LcpGSEM.pNegFlagsPerIf[ACCM_IDX].FlagMask |= DESIRED_SET;
    MEMCPY (pIf->LcpOptionsDesired.AsyncMap,
            pSetValPppLinkConfigReceiveACCMap->pu1_OctetList,
            pSetValPppLinkConfigReceiveACCMap->i4_Length);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppLinkConfigTransmitACCMap
 Input       :  The Indices
                PppLinkConfigIfIndex

                The Object 
                setValPppLinkConfigTransmitACCMap
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppLinkConfigTransmitACCMap (INT4 i4PppLinkConfigIfIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pSetValPppLinkConfigTransmitACCMap)
{

    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr (i4PppLinkConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIf->LcpGSEM.pNegFlagsPerIf[ACCM_IDX].FlagMask |= ALLOWED_FOR_PEER_SET;
    MEMCPY (pIf->LcpOptionsAllowedForPeer.AsyncMap,
            pSetValPppLinkConfigTransmitACCMap->pu1_OctetList,
            pSetValPppLinkConfigTransmitACCMap->i4_Length);
    return (SNMP_SUCCESS);

}
#endif /* DSL_ADD */

/****************************************************************************
 Function    :  nmhSetPppLinkConfigMagicNumber
 Input       :  The Indices
                PppLinkConfigIfIndex

                The Object 
                setValPppLinkConfigMagicNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppLinkConfigMagicNumber (INT4 i4PppLinkConfigIfIndex,
                                INT4 i4SetValPppLinkConfigMagicNumber)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppLinkConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (i4SetValPppLinkConfigMagicNumber == MAGIC_NUMBER_NOT_SET)
    {
        pIf->LcpGSEM.pNegFlagsPerIf[MAGIC_IDX].FlagMask &= DESIRED_NOT_SET;
        pIf->LcpOptionsDesired.MagicNumber = 0;
    }

    if (i4SetValPppLinkConfigMagicNumber == MAGIC_NUMBER_SET)
    {
        pIf->LcpGSEM.pNegFlagsPerIf[MAGIC_IDX].FlagMask |= DESIRED_SET;
        pIf->LcpOptionsDesired.MagicNumber = GENERATE_MAGIC_NUMBER ();
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetPppLinkConfigFcsSize
 Input       :  The Indices
                PppLinkConfigIfIndex

                The Object 
                setValPppLinkConfigFcsSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppLinkConfigFcsSize (INT4 i4PppLinkConfigIfIndex,
                            INT4 i4SetValPppLinkConfigFcsSize)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppLinkConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIf->LcpGSEM.pNegFlagsPerIf[FCS_IDX].FlagMask |= DESIRED_SET;

    switch (i4SetValPppLinkConfigFcsSize)
    {
        case 0:
            pIf->LcpOptionsDesired.FCSSize = NO_FCS_REQ;
            break;
        case 16:
            pIf->LcpOptionsDesired.FCSSize = FCS_16_REQ;
            break;
        case 32:
            pIf->LcpOptionsDesired.FCSSize = FCS_32_REQ;
            break;
        case 48:
            pIf->LcpOptionsDesired.FCSSize = FCS_48_REQ;
            break;
        default:
            break;
    }
    return (SNMP_SUCCESS);
}

#ifndef IPv6CP
INT1
CheckAndGetIp6Ptr (tPPPIf * pIf)
{
    UNUSED_PARAM (pIf);
    return OSIX_SUCCESS;
}
#endif
