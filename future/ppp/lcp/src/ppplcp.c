/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppplcp.c,v 1.8 2014/11/23 09:33:38 siva Exp $
 *
 * Description:This file contains the routines of LCP module.
 *
 *******************************************************************/

#include "lcpcom.h"
#include "gcpdefs.h"
#include "gsemdefs.h"
#include "genexts.h"
#include "authdefs.h"
#include "authprot.h"
#include "mpdefs.h"
#include "pppmp.h"
#include "mpexts.h"
#include "pppipcp.h"
#include "pppipv6cp.h"
#include "pppmplscp.h"
#include "pppipxcp.h"
#include "pppeccmn.h"
#include "pppccp.h"
#include "pppeccmn.h"
#include "pppecp.h"
#ifdef BAP
#include "pppbacp.h"
#include "pppbap.h"
#endif
#include "globexts.h"
#include "pppoeport.h"

#include "pppl2tp.h"
#include "pppdpproto.h"
#include "genproto.h"
#include "lqmprot.h"
#include "pppoelow.h"

UINT1               FCSAvailability;
UINT1               gu1aaaMethod;
UINT1               gu1AuthMode = PPP_AUTH_PAP;
extern tLangTable   gLangTable;

extern tOsixSemId   gPppLcpDownSemId;
extern tSecurityInfo IndexZeroSecurityInfo;
tOptNegFlagsPerIf   LCPOptPerIntf[] = {

    /* For Naetra we will not have the option handler functions for
     * MP related options. So make the flags 0 for these options.
     * same for the case of LQM options
     */
    {MRRU_OPTION, 0, 0},
    {SEQ_OPTION, 0, 0},
    {ENDPT_OPTION, 0, 0},
    {MRU_OPTION,
     DESIRED_SET | ALLOWED_FOR_PEER_SET | ALLOW_FORCED_NAK_SET |
     ALLOW_REJECT_SET, 0},
    {ACCM_OPTION,
     ALLOWED_FOR_PEER_SET | ALLOW_FORCED_NAK_SET | ALLOW_REJECT_SET, 0},
    {AUTH_OPTION, 0, 0},
    {LQR_OPTION, 0, 0},
    {MAGIC_OPTION,
     ALLOWED_FOR_PEER_SET | ALLOW_FORCED_NAK_SET | ALLOW_REJECT_SET, 0},
    {PFC_OPTION,
     ALLOWED_FOR_PEER_SET | ALLOW_FORCED_NAK_SET | ALLOW_REJECT_SET, 0},
    {ACFC_OPTION,
     ALLOWED_FOR_PEER_SET | ALLOW_FORCED_NAK_SET | ALLOW_REJECT_SET, 0},
    {FCS_OPTION,
     ALLOWED_FOR_PEER_SET | ALLOW_FORCED_NAK_SET | ALLOW_REJECT_SET, 0},

    {PADDING_OPTION,
     ALLOWED_FOR_PEER_SET | ALLOW_FORCED_NAK_SET | ALLOW_REJECT_SET, 0},

    {CALL_BACK_OPTION, ALLOW_REJECT_SET, 0}
#ifdef BAP
    ,
    {LINK_DISCR_OPTION, ALLOWED_FOR_PEER_SET, 0}
#endif

    ,

    {INTERNATION_OPTION,
     ALLOWED_FOR_PEER_SET | ALLOW_FORCED_NAK_SET | ALLOW_REJECT_SET, 0}
};

tGSEMCallbacks      LcpGSEMCallback = {
    LCPUp,
    LCPDown,
    LCPStarting,
    LCPFinished,
    NULL,
    NULL,
    NULL,
    NULL
};

tAuthDiscVal        AuthDiscVal = { MAX_AUTH_DISCRETE_VALUES,
    {0xc023, 0xc223, 0xc227}, 0
};

tLQMDiscVal         LQMDiscVal = { MAX_LQM_DISCRETE_VALUES, {0xc025}, 0 };
tFCSDiscVal         FCSDiscVal = { MAX_FCS_DISCRETE_VALUES, {1, 2, 4, 6} };

tCallBackDiscVal    CallBackDiscVal =
    { MAX_CALL_BACK_DISCRETE_VALUES, {0, 1, 2, 3, 4, 5, 6}, 0 };

/* 
 * This strcuture contains all the allowable character-set values
 * to validate the MIBenum field of the ConfReq.
 */
tInternationDiscVal InternationDiscVal = { MAX_INTERNATION_DISCRETE_VALUES,
    {CHAR_SET_UTF8}
};

tRangeInfo          MRURange = { {0}, {0xffff} };
tRangeInfo          MagicRange = { {0}, {0xffffffff} };
tRangeInfo          PaddingRange = { {1}, {0xff} };

#ifdef BAP
tRangeInfo          LinkDiscrRange = { {0}, {0xffff} };
#endif

tRangeInfo          MRRURange = { {0}, {0xffff} };
tEPDDiscVal         EPDDiscVal =
    { MAX_EPD_DISCRETE_VALUES, {0, 1, 2, 3, 4, 5}, 0 };

tOptHandlerFuns     MRUHandlingFuns = {
    LCPProcessMRUConfReq,
    LCPProcessMRUConfNak,
    LCPProcessMRUConfRej,
    LCPReturnMRUPtr,
    NULL
};

tOptHandlerFuns     ACCMHandlingFuns = {
    LCPProcessACCMConfReq,
    LCPProcessACCMConfNak,
    LCPProcessACCMConfRej,
    LCPReturnACCMPtr,
    NULL
};
tOptHandlerFuns     AuthHandlingFuns = {
    LCPProcessAUTHConfReq,
    LCPProcessAUTHConfNak,
    NULL,
    LCPReturnAUTHPtr,
    LCPAddAuthSubOpt
};
tOptHandlerFuns     MagicHandlingFuns = {
    LCPProcessMagicConfReq,
    LCPProcessMagicConfNak,
    NULL,
    LCPReturnMagicPtr,
    NULL
};
tOptHandlerFuns     LQRHandlingFuns = {
    LCPProcessLQRConfReq,
    LCPProcessLQRConfNak,
    NULL,
    LCPReturnLQRPtr,
    LCPAddLQRSubOpt
};

tOptHandlerFuns     FCSHandlingFuns = {
    LCPProcessFCSConfReq,
    LCPProcessFCSConfNak,
    LCPProcessFCSConfRej,
    LCPReturnFCSPtr,
    NULL
};

tOptHandlerFuns     PaddingHandlingFuns = {
    LCPProcessPaddingConfReq,
    LCPProcessPaddingConfNak,
    LCPProcessPaddingConfRej,
    LCPReturnPaddingPtr,
    NULL
};

tOptHandlerFuns     CallBackHandlingFuns = {
    LCPProcessCallBackConfReq,
    LCPProcessCallBackConfNak,
    NULL,
    LCPReturnCallBackPtr,
    LCPAddCallBackSubOpt
};

#ifdef BAP
tOptHandlerFuns     LinkDiscrHandlingFns = {
    LCPProcessLinkDiscrConfReq,
    LCPProcessLinkDiscrConfNak,
    NULL,
    LCPReturnLinkDiscrPtr,
    NULL
};
#endif

tOptHandlerFuns     InternationHandlingFuns = {
    LCPProcessInternationConfReq,
    LCPProcessInternationConfNak,
    NULL,
    LCPReturnInternationPtr,
    LCPAddInternationSubOpt
};
tGenOptionInfo      LCPGenOptionInfo[] = {

#ifdef MP
    {MRRU_OPTION, RANGE, BYTE_LEN_2, 4, &MRRUHandlingFuns, {&MRRURange},
     NOT_SET, 0, 0},
    {SEQ_OPTION, BOOLEAN_CATEGORY, BOOL_TYPE, 2, &SeqOptHandlingFuns,
     {NULL}, NOT_SET, 0, 0},
    {ENDPT_OPTION, DISCRETE, BYTE_LEN_1, 3, &EPDHandlingFuns,
     {(VOID *) &EPDDiscVal}, NOT_SET, 0, 0},
#endif
    {MRU_OPTION, RANGE, BYTE_LEN_2, 4, &MRUHandlingFuns, {&MRURange}, NOT_SET,
     0, 0},

    {ACCM_OPTION, STRING_TYPE, STRING_TYPE, 6, &ACCMHandlingFuns, {NULL},
     NOT_SET, 0, 0},
    {AUTH_OPTION, DISCRETE, BYTE_LEN_2, 4, &AuthHandlingFuns,
     {(VOID *) &AuthDiscVal}, NOT_SET, 0, 0},
    {LQR_OPTION, DISCRETE, BYTE_LEN_2, 4, &LQRHandlingFuns,
     {(VOID *) &LQMDiscVal}, NOT_SET, 0, 0},
    {MAGIC_OPTION, RANGE, BYTE_LEN_4, 6, &MagicHandlingFuns, {&MagicRange},
     NOT_SET, 0, 0},
    {PFC_OPTION, BOOLEAN_CATEGORY, BOOL_TYPE, 2, &BooleanHandlingFuns, {NULL},
     NOT_SET, 0, 0},
    {ACFC_OPTION, BOOLEAN_CATEGORY, BOOL_TYPE, 2, &BooleanHandlingFuns, {NULL},
     NOT_SET, 0, 0},
    {FCS_OPTION, DISCRETE, BYTE_LEN_1, 3, &FCSHandlingFuns,
     {(VOID *) &FCSDiscVal}, NOT_SET, 0, 0},

    {PADDING_OPTION, RANGE, BYTE_LEN_1, 3, &PaddingHandlingFuns,
     {&PaddingRange}, NOT_SET, 0, 0},

    {CALL_BACK_OPTION, DISCRETE, BYTE_LEN_1, 3, &CallBackHandlingFuns,
     {(VOID *) &CallBackDiscVal}, NOT_SET, 0, 0}

#ifdef BAP
    ,

    {LINK_DISCR_OPTION, RANGE, BYTE_LEN_2, 4, &LinkDiscrHandlingFns,
     {&LinkDiscrRange}, NOT_SET, 0, 0}
#endif
    ,

    {INTERNATION_OPTION, DISCRETE, BYTE_LEN_4, 5, &InternationHandlingFuns,
     {(VOID *) &InternationDiscVal}, NOT_SET, 0, 0}
};

extern UINT1        gDefaultTxACCM[];

/*********************************************************************
        Initialization and utility functions of LCP Module
*********************************************************************/

/*********************************************************************
*    Function Name    :    LCPInit
*    Description        :
*        This function initializes the LCP interface structure.
*    Parameter(s)    :  
*                pIf    -    Points to the LCP interface structure to be initialized.
*    Return Value    :    OK/NOT_OK
*********************************************************************/
INT1
LCPInit (tPPPIf * pIf)
{

    INT4                i4RetValPPPoEMode = 0;
    /* Added Initialization of tRangeInfo explicitly to avoid problems 
     * in Big Endian Machines
     * */
    MRURange.MinVal.ShortVal = 0;
    MRURange.MaxVal.ShortVal = 0xffff;

    MagicRange.MinVal.LongVal = 0;
    MagicRange.MaxVal.LongVal = 0xffffffff;

    PaddingRange.MinVal.CharVal = 1;
    PaddingRange.MaxVal.CharVal = 0xff;

    MRRURange.MinVal.ShortVal = 0;
    MRRURange.MaxVal.ShortVal = 0xffff;

#ifdef BAP
    LinkDiscrRange.MinVal.ShortVal = 0;
    LinkDiscrRange.MaxVal.ShortVal = 0xffff;
#endif

#ifdef RADIUS_WANTED
    pIf->RadInfo.AcctNeeded = PPP_NO;
    pIf->RadInfo.AcctTerminateCause = PPP_RAD_USER_REQ;
#endif
    /* Initialize the Option fields */
    LCPOptionInit (&pIf->LcpOptionsDesired);
    LCPOptionInit (&pIf->LcpOptionsAllowedForPeer);
    LCPOptionInit (&pIf->LcpOptionsAckedByPeer);
    LCPOptionInit (&pIf->LcpOptionsAckedByLocal);
    pIf->CurrentLinkConfigs.RestartTimeOutVal = DEF_RESTART_TIMEOUT;
    pIf->CurrentLinkConfigs.MaxConfigReqTransmits = DEF_MAX_CONF_REQ;
    pIf->CurrentLinkConfigs.MaxNakLoops = DEF_MAXNAK_LOOPS;
    pIf->CurrentLinkConfigs.MaxTermtransmits = DEF_MAX_TERM_REQ;
    pIf->CurrentLinkConfigs.PassiveOption = NOT_SET;

    pIf->CurrentLinkConfigs.EapDefaultAuthType = EAP_TYPE_MD5;
    pIf->CurrentLinkConfigs.EapMaxIdentityRetries =
        EAP_DEF_MAX_IDENTITY_RETRIES;
    pIf->CurrentLinkConfigs.EapIdentityNeeded = EAP_SEND_IDENTITY;

    pIf->LinkConfigs.EapDefaultAuthType = EAP_TYPE_MD5;
    pIf->LinkConfigs.EapMaxIdentityRetries = EAP_DEF_MAX_IDENTITY_RETRIES;
    pIf->LinkConfigs.EapIdentityNeeded = EAP_SEND_IDENTITY;

    pIf->LinkConfigs.RestartTimeOutVal = DEF_RESTART_TIMEOUT;
    pIf->LinkConfigs.MaxConfigReqTransmits = DEF_MAX_CONF_REQ;
    pIf->LinkConfigs.MaxNakLoops = DEF_MAXNAK_LOOPS;
    pIf->LinkConfigs.MaxTermtransmits = DEF_MAX_TERM_REQ;
    pIf->LinkConfigs.PassiveOption = NOT_SET;
#ifdef FRAMING
    MEMCPY (pIf->ExtTxACCM, gDefaultTxACCM, TX_ACCM_SIZE);
#endif
    PPPIndicateAsyncParamsToLL (pIf, DEFAULT_ASYNC_VALS);
    pIf->TermReqFlag = NOT_SET;

    pIf->CallBackTimeInfo.CallBackNegFlags = 0;

    /*  By default, allow all FCS sizes supported  */
    pIf->LcpOptionsAllowedForPeer.FCSSize = FCSAvailability;

    /* Initialize the LCP Status variables */
    pIf->LcpStatus.AuthStatus = NO_AUTH_PROTOCOL;
    pIf->LcpStatus.AdminStatus = STATUS_DOWN;
    pIf->LcpStatus.OperStatus = STATUS_DOWN;
    pIf->LcpStatus.LinkQualityStatus = QUALITY_NOT_DETERMINED;
    pIf->LcpStatus.LinkAvailability = LINK_NOT_AVAILABLE;

    /* Initialize the link information */
    pIf->LinkInfo.TimeOutTime = DEF_TIMEOUT_TIME;
    pIf->LinkInfo.MaxNumLoops = DEF_MAX_MAGIC_NAK_LOOPS;
    pIf->LinkInfo.MaxReTransmits = DEF_MAX_RETXMISSION;
    pIf->LinkInfo.RxAddrCtrlLen = 0;
    pIf->LinkInfo.RxProtLen = 0;
    pIf->LinkInfo.u1FrmFlag = PPP_TRUE;
    pIf->LinkInfo.bMRUConfigured = OSIX_FALSE;
    pIf->EchoInfo.KeepAliveTimeOutVal = PPP_DEF_KEEPALIVE_TIME;
    PPP_INIT_TIMER (pIf->LinkInfo.IdleTimer);
    PPP_INIT_TIMER (pIf->CallBackTimeInfo.CallBackDisconnectTimer);
    PPP_INIT_TIMER (pIf->CallBackTimeInfo.CallBackDialTimer);

    /* Initialize LCP SEM and determine link speed */
    if ((GSEMInit (&pIf->LcpGSEM, MAX_LCP_OPT_TYPES) == NOT_OK)
        || ((pIf->LinkInfo.IfSpeed = GetIfSpeed (pIf)) == 0))
    {
        return (NOT_OK);
    }

    /* Initialize the LCP SEM variables */
    pIf->LcpGSEM.Protocol = LCP_PROTOCOL;
    pIf->LcpGSEM.pCallbacks = &LcpGSEMCallback;
    pIf->LcpGSEM.pIf = pIf;
    pIf->LcpGSEM.pGenOptInfo = LCPGenOptionInfo;

    MEMCPY (pIf->LcpGSEM.pNegFlagsPerIf, &LCPOptPerIntf, MEM_MAX_BYTES
            (sizeof (LCPOptPerIntf),
             (sizeof (tOptNegFlagsPerIf) * pIf->LcpGSEM.MaxOptTypes)));
    /* Initialize the MP option flags */
    BZERO (pIf->LcpGSEM.pNegFlagsPerIf,
           sizeof (tOptNegFlagsPerIf) * MAX_BUNDLE_OPTS);

    /* Initialize statistics counter structure */
    BZERO (&pIf->LLHCounters, sizeof (tLLHCounters));

    /* Initialize the Echo information */
    PPP_INIT_TIMER (pIf->EchoInfo.EchoTimer);
    PPP_INIT_TIMER (pIf->EchoInfo.KeepAliveTimer);
    pIf->EchoInfo.EchoTransmits = 0;
    pIf->EchoInfo.EchoChar = DEF_ECHO_CHAR;

    /* Initialize the handles to other modules - For every new NCP added, an
       initialization of the CPs IF structure will have to be made here. */
    pIf->pAuthPtr = NULL;
    pIf->pLqmPtr = NULL;
    pIf->pIpcpPtr = NULL;

#ifdef Ipv6CP
    pIf->pIp6cpPtr = NULL;
#endif

    pIf->pMplscpPtr = NULL;
    pIf->pIpxcpPtr = NULL;
    pIf->pCCPIf = NULL;
    pIf->pECPIf = NULL;

    /* Enable ACFC and PFC negotiation flags based on link speed */
    LCPEnableCompressions (pIf);

    pIf->LcpOptionsAllowedForPeer.MRU = DEF_MRU_SIZE;
    pIf->LcpOptionsDesired.MRU = DEF_MRU_SIZE;
    nmhGetPPPoEMode (&i4RetValPPPoEMode);
    if (PPPOE_SERVER == i4RetValPPPoEMode)
    {
        pIf->LinkInfo.IfID.IfType = NONE_IF_TYPE;
    }
    /* initialize for diff. lower ifTypes */
    if (LCPInitForIfType (pIf) == NOT_OK)
    {
        return NOT_OK;
    }

    pIf->u1IsCreatedForL2tp = PPP_FALSE;
    /* Initialize the MP member information */
    pIf->BundleFlag = MEMBER_LINK;
    SLL_INIT_NODE (&pIf->MPInfo.MemberInfo.NextMPIf);
    pIf->MPInfo.MemberInfo.LastSeqNumRcvd = INIT_SEQ_VALUE;
    pIf->MPInfo.MemberInfo.pBundlePtr = NULL;
    pIf->MPInfo.MemberInfo.PartOfBundle = PPP_NO;
    pIf->u1RemovedLinkInDP = PPP_TRUE;
    PPP_INIT_TIMER (pIf->MPInfo.MemberInfo.PeriodicEchoTimer);
    /*  Multiply the ratio 9600/ifSpeed with the worst-case value (2mb/9600 
       is  approx. 214
     */
    pIf->MPInfo.MemberInfo.TransitDelay =
        (UINT4) (MIN_SPEED * MAX_DIVISIBLE_VALUE) / pIf->LinkInfo.IfSpeed;
    pIf->MPInfo.MemberInfo.CumulativeDelay = 0;

    PPP_INIT_TIMER (pIf->RecdTimer);

    return (OK);
}

/*********************************************************************
*    Function Name    :    LCPReInit
*    Description        :
*        This function Re-Initializes the LCP interface structure.
*    Parameter(s)    :  
*                pIf    -    Points to the LCP interface structure to be initialized.
*    Return Value    :    OK/NOT_OK
*********************************************************************/
INT1
LCPReInit (tPPPIf * pIf)
{
#ifdef RADIUS_WANTED
    pIf->RadInfo.AcctNeeded = PPP_NO;
    pIf->RadInfo.AcctTerminateCause = PPP_RAD_USER_REQ;
#endif
    /* Initialize the Option fields */
    LCPOptionInit (&pIf->LcpOptionsAckedByPeer);
    LCPOptionInit (&pIf->LcpOptionsAckedByLocal);
#ifdef FRAMING
    MEMCPY (pIf->ExtTxACCM, gDefaultTxACCM, TX_ACCM_SIZE);
#endif
    PPPIndicateAsyncParamsToLL (pIf, DEFAULT_ASYNC_VALS);
    pIf->TermReqFlag = NOT_SET;

    pIf->CallBackTimeInfo.CallBackNegFlags = 0;

    /*  By default, allow all FCS sizes supported  */
    pIf->LcpOptionsAllowedForPeer.FCSSize = FCSAvailability;

    /* Initialize the LCP Status variables */
    pIf->LcpStatus.OperStatus = STATUS_DOWN;
    pIf->LcpStatus.LinkQualityStatus = QUALITY_NOT_DETERMINED;
    pIf->LcpStatus.LinkAvailability = LINK_NOT_AVAILABLE;

    /* Initialize the link information */
    pIf->LinkInfo.RxAddrCtrlLen = 0;
    pIf->LinkInfo.RxProtLen = 0;
    PPP_INIT_TIMER (pIf->LinkInfo.IdleTimer);
    PPP_INIT_TIMER (pIf->CallBackTimeInfo.CallBackDisconnectTimer);
    PPP_INIT_TIMER (pIf->CallBackTimeInfo.CallBackDialTimer);

    /* Initialize statistics counter structure */
    BZERO (&pIf->LLHCounters, sizeof (tLLHCounters));

    /* Initialize the Echo information */
    PPP_INIT_TIMER (pIf->EchoInfo.EchoTimer);
    PPP_INIT_TIMER (pIf->EchoInfo.KeepAliveTimer);
    pIf->EchoInfo.EchoTransmits = 0;

    /* Enable ACFC and PFC negotiation flags based on link speed */
    LCPEnableCompressions (pIf);

    /* initialize for diff. lower ifTypes */
    if (LCPInitForIfType (pIf) == NOT_OK)
    {
        return NOT_OK;
    };

    PPP_INIT_TIMER (pIf->RecdTimer);

    return (OK);
}

/****************************************************************************
*    Function Name    :    LCPOptionInit
*    Description        :
*            This function initializes the LCPOption structure pointed to by 
*    pLCPOpt and is called by the LCPInit function.
*    Parameter(s)    :
*            pLCPOpt    -    Points to the LCPOption structure to be initialized.
*  Return Value        :    VOID
*********************************************************************/
VOID
LCPOptionInit (tLCPOptions * pLCPOpt)
{
    pLCPOpt->MRU = DEF_MRU_SIZE;
    pLCPOpt->FCSSize = FCS_16_REQ;
    pLCPOpt->MagicNumber = 0;
    pLCPOpt->AsyncMap[0] = 0xff;
    pLCPOpt->AsyncMap[1] = 0xff;
    pLCPOpt->AsyncMap[2] = 0xff;
    pLCPOpt->AsyncMap[3] = 0xff;
    pLCPOpt->MaxPadVal = DEF_PAD_VALUE;
    pLCPOpt->LCPCallBackInfo.CallBackOperation = 0;
    pLCPOpt->LCPCallBackInfo.pCallBackMsg = NULL;
    pLCPOpt->LCPCallBackInfo.CallBackMsgLen = 0;
#ifdef BAP
    pLCPOpt->LinkDiscriminator = 0;
#endif

    return;
}

/*********************************************************************
*    Function Name    :    PPPResetVariables
*    Description        :
*                This functions resets the counter variables to zero.It also
*    deletes the INVALID entries in the AuthSecret Table. 
*    Parameter(s)    :
*                pIf    -    Pointer to the PPP interface data structure.
*  Return Value      :    VOID
*********************************************************************/
VOID
PPPResetVariables (tPPPIf * pIf)
{
    tAUTHIf            *pAuthPtr;
    tSecret            *pSecret;
    UINT1               u1Index;

    BZERO (&pIf->LLHCounters, sizeof (tLLHCounters));

    /*Copy the SNMP set values from the Desired structure to the AckedByPeer    structure */
    if (pIf->BundleFlag == BUNDLE)
    {
        MEMCPY (&pIf->MPInfo.BundleInfo.BundleOptAckedByPeer,
                &pIf->MPInfo.BundleInfo.BundleOptDesired,
                sizeof (tBundleOptions));
        for (u1Index = 0; u1Index < MAX_BUNDLE_OPTS; u1Index++)
        {
            if (pIf->MPInfo.BundleInfo.MPFlagsPerIf[u1Index].
                FlagMask & DESIRED_SET)
            {
                pIf->MPInfo.BundleInfo.MPFlagsPerIf[u1Index].FlagMask |=
                    ACKED_BY_PEER_SET;
            }
        }
    }

    MEMCPY (&pIf->LcpOptionsAckedByPeer, &pIf->LcpOptionsDesired,
            sizeof (tLCPOptions));

    /*02032001 */
    for (u1Index = 0; u1Index < MAX_LCP_OPT_TYPES; u1Index++)
    {
        if (pIf->LcpGSEM.pNegFlagsPerIf[u1Index].FlagMask & DESIRED_SET)
        {
            pIf->LcpGSEM.pNegFlagsPerIf[u1Index].FlagMask |= ACKED_BY_PEER_SET;
        }
        else
        {
            pIf->LcpGSEM.pNegFlagsPerIf[u1Index].FlagMask &=
                ACKED_BY_PEER_NOT_SET;
        }
    }

    if ((pAuthPtr = pIf->pAuthPtr) != NULL)
    {
        tSecret            *pTempSecret = NULL;    /* this points to the secret 
                                                   node to be freed */

        /* deleting the nodes  ............. */
        SLL_SCAN (&pAuthPtr->SecurityInfo.SecretList, pSecret, tSecret *)
        {
            if (pTempSecret)
                AUTHDeleteSecretEntry (&pAuthPtr->SecurityInfo, pTempSecret);
            pTempSecret = NULL;

            if (pSecret->Status == INVALID)
            {
                pTempSecret = pSecret;
            }
        }
        if (pTempSecret)
            AUTHDeleteSecretEntry (&pAuthPtr->SecurityInfo, pTempSecret);
        /* ...................  form the list */

        if ((pAuthPtr->SecurityInfo.CHAPLocToRemSecrets +
             pAuthPtr->SecurityInfo.PAPLocToRemSecrets +
             pAuthPtr->SecurityInfo.MSCHAPLocToRemSecrets +
             pAuthPtr->SecurityInfo.EAPMD5LocToRemSecrets == 0)
            && (IndexZeroSecurityInfo.CHAPLocToRemSecrets +
                IndexZeroSecurityInfo.PAPLocToRemSecrets +
                IndexZeroSecurityInfo.MSCHAPLocToRemSecrets +
                IndexZeroSecurityInfo.EAPMD5LocToRemSecrets == 0))
        {
            pIf->LcpGSEM.pNegFlagsPerIf[AUTH_IDX].FlagMask &=
                ALLOWED_FOR_PEER_NOT_SET;
        }
        pIf->pAuthPtr->LastTriedIndex = 0;
        pIf->pAuthPtr->PapLastSecretIndex = INIT_VAL;
        pIf->pAuthPtr->ChapLastSecretIndex = INIT_VAL;
        pIf->pAuthPtr->MSChapLastSecretIndex = INIT_VAL;
        pIf->pAuthPtr->EapMD5LastSecretIndex = INIT_VAL;
    }

/* RESETTING 4 BITS OF AUTH FLAG only that are ack'd by peer */
    pIf->LcpStatus.AuthStatus &= RESET_AUTH_ACKED_BY_PEER;
    pIf->LcpStatus.LinkQualityStatus = QUALITY_NOT_DETERMINED;
    pIf->LinkInfo.NumLoops = 0;
    pIf->EchoInfo.EchoTransmits = 0;

    return;
}

/*********************************************************************
*    Function Name    :    LCPEnableCompressions
*    Description        :
*                This function enables address and control field and 
*    protocol field compressions depending on the interface type. 
*    Input            :
*                pIf    -    Points to the PPP Interface structure.
*    Return Value    :
*                    VOID        
*********************************************************************/
VOID
LCPEnableCompressions (tPPPIf * pIf)
{
    /* By default no compressions are set */
    UINT1               u1EnablePFC = NOT_SET;
    UINT1               u1EnableMagic = NOT_SET;

    switch (pIf->LinkInfo.IfID.IfType)
    {
        case ASYNC_IF_TYPE:
            u1EnableMagic = SET;
            pIf->LcpGSEM.pNegFlagsPerIf[PFC_IDX].FlagMask &= DESIRED_NOT_SET;
            pIf->LcpGSEM.pNegFlagsPerIf[FCS_IDX].FlagMask = ALLOW_REJECT_SET;
            pIf->LcpGSEM.pNegFlagsPerIf[ACFC_IDX].FlagMask = ALLOW_REJECT_SET;
            pIf->LcpGSEM.pNegFlagsPerIf[ACCM_IDX].FlagMask = ALLOW_REJECT_SET;
            break;

        case PPTP_IF_TYPE:
            u1EnableMagic = SET;
            pIf->LcpGSEM.pNegFlagsPerIf[ACCM_IDX].FlagMask =
                DESIRED_SET | ALLOWED_FOR_PEER_SET;
            pIf->LcpGSEM.pNegFlagsPerIf[PFC_IDX].FlagMask =
                DESIRED_SET | ALLOWED_FOR_PEER_SET;
            pIf->LcpGSEM.pNegFlagsPerIf[ACFC_IDX].FlagMask =
                DESIRED_SET | ALLOWED_FOR_PEER_SET;
            break;
        case SERIAL_IF_TYPE:
        case ISDN_IF_TYPE:
            break;
        case SONET_IF_TYPE:
            u1EnableMagic = SET;
            break;
        case FR_IF_TYPE:
            u1EnablePFC = SET;
            break;
        case X25_IF_TYPE:
            u1EnablePFC = SET;
            break;
        case PPP_OE_IFTYPE:
            u1EnableMagic = SET;
            pIf->LcpGSEM.pNegFlagsPerIf[PFC_IDX].FlagMask &= DESIRED_NOT_SET;
            pIf->LcpGSEM.pNegFlagsPerIf[FCS_IDX].FlagMask = ALLOW_REJECT_SET;
            pIf->LcpGSEM.pNegFlagsPerIf[ACFC_IDX].FlagMask = ALLOW_REJECT_SET;
            pIf->LcpGSEM.pNegFlagsPerIf[ACCM_IDX].FlagMask = ALLOW_REJECT_SET;
            break;
        case ATM_IF_TYPE:
            u1EnableMagic = SET;
            pIf->LcpGSEM.pNegFlagsPerIf[PFC_IDX].FlagMask &= DESIRED_NOT_SET;
            pIf->LcpGSEM.pNegFlagsPerIf[FCS_IDX].FlagMask = ALLOW_REJECT_SET;
            pIf->LcpGSEM.pNegFlagsPerIf[ACFC_IDX].FlagMask = ALLOW_REJECT_SET;
            pIf->LcpGSEM.pNegFlagsPerIf[ACCM_IDX].FlagMask = ALLOW_REJECT_SET;
            break;
        default:
            break;

    }

    pIf->LcpGSEM.pNegFlagsPerIf[ACFC_IDX].FlagMask &= DESIRED_NOT_SET;

    if (u1EnablePFC == SET)
    {
        pIf->LcpGSEM.pNegFlagsPerIf[PFC_IDX].FlagMask |= DESIRED_SET;
    }
    else
    {
        pIf->LcpGSEM.pNegFlagsPerIf[PFC_IDX].FlagMask &= DESIRED_NOT_SET;
    }

    if (u1EnableMagic == SET)
    {
        pIf->LcpGSEM.pNegFlagsPerIf[MAGIC_IDX].FlagMask |= DESIRED_SET;
        pIf->LcpOptionsDesired.MagicNumber = GENERATE_MAGIC_NUMBER ();
    }
    else
    {
        pIf->LcpGSEM.pNegFlagsPerIf[MAGIC_IDX].FlagMask &= DESIRED_NOT_SET;
    }
    return;
}

/*********************************************************************
*    Function Name    :    LCPInitForIfType()
*    Description        :
*                This function initializes pIf strucutre for different 
*                ifTypes
*    Input            :
*                pIf    -    Points to the PPP Interface structure.
*    Return Value    :
*                    VOID        
*********************************************************************/
INT4
LCPInitForIfType (tPPPIf * pIf)
{
    UINT1               u1Temp = 0;

    pIf->LinkInfo.Mask = 0;

    switch (pIf->LinkInfo.IfID.IfType)
    {
        case NONE_IF_TYPE:
            pIf->LinkInfo.Mask |= PPP_STATUS_FREE_MASK;
            pIf->LinkInfo.Mask |= PPP_FLOATING_MASK;
            break;

        case SONET_IF_TYPE:

            if ((pIf->pSonetIf =
                 MEM_MALLOC (sizeof (tSonetIf), tSonetIf)) == NULL)
            {
                return NOT_OK;
            }
            else
            {

                for (u1Temp = 0; u1Temp < SCRAMBLE_SIZE; u1Temp++)
                    pIf->pSonetIf->au1ScrambleRegister[u1Temp] =
                        (UINT1) (rand () % 2);
                for (u1Temp = 0; u1Temp < SCRAMBLE_SIZE; u1Temp++)
                    pIf->pSonetIf->au1DescrambleRegister[u1Temp] =
                        (UINT1) (rand () % 2);
                pIf->pSonetIf->RxScramble = TRUE;
            }

            break;

        case PPP_OE_IFTYPE:
            /* Negotiate MRU for PPPoE sessions : 1492 */
            pIf->LcpOptionsAllowedForPeer.MRU = PPPOE_DEF_MRU;
            pIf->LcpOptionsDesired.MRU = PPPOE_DEF_MRU;
            pIf->LcpGSEM.pNegFlagsPerIf[MRU_IDX].FlagMask |= DESIRED_SET;
            break;
        case ATM_IF_TYPE:
            /* Negotiate the MRU for PPPOA session  : 1500 */
            pIf->LcpOptionsAllowedForPeer.MRU = PPPOA_DEF_MRU;
            pIf->LcpOptionsDesired.MRU = PPPOA_DEF_MRU;
            pIf->LcpGSEM.pNegFlagsPerIf[MRU_IDX].FlagMask |= DESIRED_SET;
            break;

        default:
            break;
    }
    return OK;
}

/*********************************************************************
     Callback functions of LCP Module - Called from GSEM module
*********************************************************************/

/*********************************************************************
*    Function Name    :    LCPUp
*    Description        :
*                This function is called once the link level configuration  
*    options are negotiated  successfully. This function indicates to other 
*    modules in the subsystem that the LCP is up and further action like 
*    authentication, link quality monitoring can proceed. It also informs the 
*    higher layers about its operational status.
*                The pUp  field of  the LcpGSEMCallback points to this function 
*    and is called from the GSEM Module.
*    Parameter(s)    :  
*        pLcpGSEM    -    Pointer to LCP tGSEM corresponding to current interface
*    Return Value      :    VOID
*********************************************************************/
VOID
LCPUp (tGSEM * pGSEM)
{
    UINT1               u1TempFlag = DESIRED_SET;
    tPPPIf             *pIf = NULL;

    pIf = pGSEM->pIf;

    if (pIf->MPInfo.MemberInfo.pBundlePtr != NULL)
        pIf->MPInfo.MemberInfo.pBundlePtr->LcpStatus.LinkAvailability =
            LINK_AVAILABLE;

    pIf->LcpStatus.LinkAvailability = LINK_OPENED;

/* Inform the lower layer to start using the negotiated FCS and/or ACCM */
    MEMCPY (pIf->ExtTxACCM, pIf->LcpOptionsAckedByLocal.AsyncMap, 4);

    u1TempFlag = pIf->LcpGSEM.pNegFlagsPerIf[ACFC_IDX].FlagMask;

    if ((u1TempFlag & ACKED_BY_PEER_SET) || (u1TempFlag & ACKED_BY_LOCAL_SET))
        PPPIndicateAsyncParamsToLL (pIf, NEGOTIATED_ASYNC_VALS);
    else
        PPPIndicateAsyncParamsToLL (pIf, DEFAULT_ASYNC_VALS);

    /*If Call Back has need acked by peer we set the CALL_BACK_REQUESTED flag 
       so that it does not appear in the new config request.  */
    if (pIf->LcpGSEM.pNegFlagsPerIf[CALL_BACK_IDX].FlagMask & ACKED_BY_PEER_SET)
        pIf->CallBackTimeInfo.CallBackNegFlags |= CALL_BACK_REQUESTED;

#ifdef LQM
/* If LQM is negotiated, start the LQM timers */
    LQMStartMonitoring (pIf);
#endif

    AuthInitServices (pIf);

/*Trigger UP event to the NCP Modules, if authentication has not been 
negotiated. Otherwise, start the authentication process. */

    if (pIf->LcpStatus.AuthStatus == ALL_AUTH_OVER)
    {
        LCPEnterNetworkLayerPhase (pIf);
    }
    else
    {
        AUTHStartAuthentication (pIf->pAuthPtr);
    }

    /* Start the LCP Idle Timer  */
    pIf->LinkInfo.IdleTimer.Param1 = PTR_TO_U4 (pIf);
    if (OsixGetSysTime (&pIf->LLHCounters.LastChangeTicks) == OSIX_FAILURE)
    {
        PPP_TRC (OS_RESOURCE, "Failure in  OsixGetSysTime()");
    }
    pIf->LinkInfo.StartingTime = pIf->LLHCounters.LastChangeTicks;

#ifdef RADIUS_WANTED
    pIf->LLHCounters.SessionStartTicks = pIf->LLHCounters.LastChangeTicks;
#endif

    /* start the keep alive timer if the timeout val != 0 */
    pIf->EchoInfo.KeepAliveTimer.Param1 = PTR_TO_U4 (pIf);
    if (pIf->EchoInfo.KeepAliveTimeOutVal != 0)
    {
        PPPStartTimer (&pIf->LinkInfo.IdleTimer, LCP_IDLE_TIMER,
                       (UINT4) (3 * pIf->EchoInfo.KeepAliveTimeOutVal));
        PPPRestartTimer (&pIf->EchoInfo.KeepAliveTimer, KEEP_ALIVE_TIMER,
                         pIf->EchoInfo.KeepAliveTimeOutVal);
    }

#ifdef L2TP_WANTED
    if (pIf->LinkInfo.IfID.IfType == ASYNC_IF_TYPE)
    {
        PPPIndicateLCPUpToLNS (pIf);
    }
#endif

    PPPHLINotifyProtStatusToHL (pIf, LCP_PROTOCOL, PPP_UP);
    return;
}

INT4
PppLcpDownLock (VOID)
{
    OsixSemTake (gPppLcpDownSemId);
    return SNMP_SUCCESS;
}

INT4
PppLcpDownUnlock (VOID)
{
    OsixSemGive (gPppLcpDownSemId);
    return SNMP_SUCCESS;
}

/*********************************************************************
*    Function Name        :    LCPDown
*    Description            :
*            This function is invoked  when the link goes down. This 
*    function indicates the NCP, AUTHENTICATION and LQM modules about the 
*    termination of  link It also indicates the higher layers that the link 
*    is down.
*    The pDown  field of  the LcpGSEM callback structure points to 
*    this function and is called from the GSEM Module.
*    Parameter(s)        :  
*        pGSEM    -    Pointer to LCP GSEMStructure corresponding 
*                to current interface
*  Return Value            :    VOID
*********************************************************************/
VOID
LCPDown (tGSEM * pGSEM)
{
    tPPPIf             *pIf = NULL;
    PPP_LCP_DOWN_LOCK ();

    if ((pGSEM == NULL) || (pGSEM->pIf == NULL))
    {
        PPP_LCP_DOWN_UNLOCK ();
        return;
    }

    pIf = pGSEM->pIf;

    /* Inform Authentication module about the LCP leaving OPENED state */
    if (pIf->pAuthPtr != NULL)
    {
        AUTHStopAuthentication (pIf->pAuthPtr);
    }

#ifdef LQM
    /* Inform LQM module about LCP leaving OPENED state */
    LQMStopMonitoring (pIf);
#endif

    /* Stop LCP idle timer */
    PPPStopTimer (&pIf->LinkInfo.IdleTimer);
    /* stop keep alive timer */
    PPPStopTimer (&pIf->EchoInfo.KeepAliveTimer);

    if (OsixGetSysTime (&pIf->LLHCounters.LastChangeTicks) == OSIX_FAILURE)
    {
        PPP_TRC (OS_RESOURCE, "Failure in  OsixGetSysTime()");
    }

    if ((pIf->MPInfo.MemberInfo.pBundlePtr != NULL)
        &&
        (SLL_COUNT
         (&(pIf->MPInfo.MemberInfo.pBundlePtr->MPInfo.BundleInfo.MemberList)) ==
         SINGLE_LINK))
        pIf->MPInfo.MemberInfo.pBundlePtr->LcpStatus.LinkAvailability =
            LINK_NOT_AVAILABLE;

    LCPLeaveNetworkLayerPhase (pIf);

    PPPResetVariables (pIf);

    if (pIf->CallBackTimeInfo.CallBackNegFlags & CALL_BACK_REQUESTED)
        pIf->LcpGSEM.pNegFlagsPerIf[CALL_BACK_IDX].FlagMask &=
            ACKED_BY_PEER_NOT_SET;

    /*Inform the lower layer to stop using the negotiated FCS and/or ACCM */
    PPPIndicateAsyncParamsToLL (pIf, DEFAULT_ASYNC_VALS);

    PPPHLINotifyProtStatusToHL (pIf, LCP_PROTOCOL, PPP_DOWN);
    PPP_LCP_DOWN_UNLOCK ();
    return;
}

/*********************************************************************
*    Function Name         :    LCPStarting
*    Description            :
*        This function is called when the administrative open for the 
*    link is received by the PPP subsystem,but the link is not yet available 
*    This function  requests the appropriate module to establish the link.  
*        The pStarting field of the LcpGSEMCallback structure points 
*    to this function and is called from the GSEM Module.
*    Parameter(s)        :  
*    pLcpGSEM    -    Pointer to LCP GSEMStructure corresponding 
*                    to current interface.
*  Return Value            :    VOID
*********************************************************************/
VOID
LCPStarting (tGSEM * pGSEM)
{

    if (((pGSEM->pNegFlagsPerIf[CALL_BACK_IDX].FlagMask & ACKED_BY_LOCAL_MASK)
         && (pGSEM->pIf->CallBackTimeInfo.TimeToDialAfterDisconnect != 0))
        && (pGSEM->pIf->CallBackTimeInfo.CallBackNegFlags & CALL_BACK_ACKED))
    {
        return;
    }

    PPPLLICallSetUp (pGSEM->pIf);

    return;
}

/*********************************************************************
*  Function Name : LCPFinished
*  Description   :
*    This function is invoked when the higher layer is done with 
*  the link and the link is no longer needed. This invokes  procedures for 
*  terminating the link.
*         The pFinished  field of the LcpGSEMCallback structure points 
*  to this function and is called from the GSEM Module.
*  Parameter(s)  :  
*            pLcpGSEM  - pointer to LCP tGSEM corresponding 
*  to current interface
*  Return Value  :    VOID
*********************************************************************/
VOID
LCPFinished (tGSEM * pGSEM)
{
    if (pGSEM->pIf != NULL)
    {
        PPPResetVariables (pGSEM->pIf);    /*02032001 */

        if (pGSEM->pIf->MPInfo.MemberInfo.pBundlePtr != NULL)
        {
#ifdef DPIF
            if (pGSEM->pIf->u1RemovedLinkInDP == PPP_FALSE)
            {
                if (MPRemoveLinkFromBundleToDP (pGSEM->pIf,
                                                pGSEM->pIf->MPInfo.MemberInfo.
                                                pBundlePtr) == PPP_SUCCESS)
                {
                    pGSEM->pIf->u1RemovedLinkInDP = PPP_TRUE;
                }
            }
#endif
        }

        PPPLLICallTerminate (pGSEM->pIf);
    }
    return;
}

/*********************************************************************
                Functions provided by LCP for GCP
*********************************************************************/

/*********************************************************************
*    Function Name        :    LCPCopyOptions 
*    Description            :
*        This function is used to process the configuration request 
*    for LCP Options.
*    Parameter(s)        :
*            pGSEM        -    Points to the GSEM structure
*    Flag        -    Specifies whether to allocate and copy into 
*            temporary variable, free the temporary variable or
*            copy back from the temporary variable. 
*            ( Values : SET/ NOT_SET/ FREE_ALL )
*    PktType        -    Specifies whether the function is called from 
*            GCP's Request processing or Nak processing routine. 
*    Return Value    :    DISCARD on allocation error, OK otherwise.
*********************************************************************/
INT1
LCPCopyOptions (tGSEM * pGSEM, UINT1 Flag, UINT1 PktType)
{
    UINT1               u1Size = 0;
    tLCPOptions        *pTemp = NULL;
    static UINT1        u1AuthFlags = NO_AUTH_PROTOCOL;
    static tPPPIf      *pEPDMatchIf = NULL;
    static tPPPIf      *pAUTHMatchIf = NULL;
    static UINT1       *pCallBackSaver = NULL;

    u1Size = sizeof (tLCPOptions);
    if (PktType == CONF_REQ_CODE)
    {
        pTemp = &pGSEM->pIf->LcpOptionsAckedByLocal;
    }
    else
    {
        pTemp = &pGSEM->pIf->LcpOptionsAckedByPeer;
    }

    if (Flag == NOT_SET)
    {

        /* Function has been called for allocation */
        if ((pCopyPtr = (UINT1 *) ALLOC_STR (u1Size)) == NULL)
        {
            PRINT_MEM_ALLOC_FAILURE;
            return (DISCARD);
        }
        if (pTemp->LCPCallBackInfo.pCallBackMsg != NULL)
        {
            pCallBackSaver = pTemp->LCPCallBackInfo.pCallBackMsg;
        }
        MEMCPY (pCopyPtr, (UINT1 *) pTemp, u1Size);

        u1AuthFlags = pGSEM->pIf->LcpStatus.AuthStatus;

        if (PktType == CONF_REQ_CODE)
        {
            pGSEM->pIf->LcpStatus.AuthStatus &= RESET_AUTH_ACKED_BY_LOCAL;
            LCPOptionInit (&pGSEM->pIf->LcpOptionsAckedByLocal);
            pEPDMatchIf = pGSEM->pIf->MPInfo.MemberInfo.pEPDMatchBundleIf;
            pAUTHMatchIf = pGSEM->pIf->MPInfo.MemberInfo.pAUTHMatchBundleIf;
            pGSEM->pIf->MPInfo.MemberInfo.pEPDMatchBundleIf = NULL;
            pGSEM->pIf->MPInfo.MemberInfo.pAUTHMatchBundleIf = NULL;
        }
    }
    else
    {
        if (Flag == SET)
        {
            /* Function has been called for recopying- Option Discarded!! */
            MEMCPY (pTemp, (tLCPOptions *) (VOID *) pCopyPtr, u1Size);
            pGSEM->pIf->LcpStatus.AuthStatus = u1AuthFlags;
            pGSEM->pIf->MPInfo.MemberInfo.pEPDMatchBundleIf = pEPDMatchIf;
            pGSEM->pIf->MPInfo.MemberInfo.pAUTHMatchBundleIf = pAUTHMatchIf;
            pTemp->LCPCallBackInfo.pCallBackMsg = pCallBackSaver;
            pCallBackSaver = NULL;
        }
        /* Function has been called for freeing memory ( and/or recopying ) */
        if (pCallBackSaver != NULL)
        {
            FREE_STR (pCallBackSaver);
        }
        FREE_STR (pCopyPtr);
    }
    return (OK);
}

/*********************************************************************
        FUNCTIONS TO PROCESS LCP SPECIFIC SPECIAL PACKETS
*********************************************************************/

/*********************************************************************
*    Function Name    :    LCPRecdProtRej
*    Description        :
*    This function is called when there is a protocol reject packet 
*received from the peer. It generates a RXJ+ event if the rejected value is 
*    acceptable, such as protocol reject for NCP. If the reject value is 
*    unacceptable such as protocol reject of LCP, then it generates RXJ- event.
*    Parameter(s)    :  
*            pGSEM    -     Pointer to the LCP tGSEM.
*            pInPkt    -     Pointer to the incoming packet.
*            Length    -     Length of the Configuration Information.
*        Identifier    -    Identifier field of the incoming packet.
*    Return Value    :    RXJ-/RXJ+/DISCARD
*********************************************************************/
INT1
LCPRecdProtRej (tGSEM * pGSEM, t_MSG_DESC * pInPkt, UINT2 Length,
                UINT1 Identifier)
{
    UINT2               u2ProtField;
    tPPPIf             *pIf = NULL;

    tIPCPIf            *pIpcpIf = NULL;

#ifdef Ipv6CP
    tIP6CPIf           *pIp6cpIf = NULL;
#endif

    tMPLSCPIf          *pMplscpIf = NULL;
    tIPXCPIf           *pIpxcpIf = NULL;
    tCCPIf             *pCCPIf = NULL;
    tECPIf             *pECPIf = NULL;
#ifdef BAP
    tBAPInfo           *pBAPInfo = NULL;
    tBACPIf            *pBACPIf = NULL;
#endif
    PPP_UNUSED (Length);
    PPP_TRC1 (CONTROL_PLANE, "\t\t\t[PROT REJ]\tId: [%d]", Identifier);

    pIf = pGSEM->pIf;

    /* Protocol reject received for any NCP over any member link SHOULD 
       be simulated for entire Bundle 
     */
    if (pIf->MPInfo.MemberInfo.PartOfBundle == PPP_YES)
    {
        pIf = pIf->MPInfo.MemberInfo.pBundlePtr;
    }

    EXTRACT2BYTE (pInPkt, u2ProtField);

    switch (u2ProtField)
    {
        case LCP_PROTOCOL:
        case MP_DATAGRAM:
            return (RXJ_MINUS);
        case IP_DATAGRAM:
        case IP_COMP_TCP_DATA:
        case IP_UNCOMP_TCP_DATA:
        case IPCP_PROTOCOL:
            pIpcpIf = (tIPCPIf *) pIf->pIpcpPtr;
            if (pIpcpIf != NULL)
            {
                GSEMRun (&pIpcpIf->IpcpGSEM, PPP_DOWN);
            }
            break;

#ifdef Ipv6CP
        case IPV6CP_PROTOCOL:
        case IP6_DATAGRAM:
            pIp6cpIf = (tIP6CPIf *) pIf->pIp6cpIf;
            if (pIp6cpIf != NULL)
            {
                GSEMRun (&pIp6cpIf->Ip6cpGSEM, PPP_DOWN);
            }
            break;
#endif

        case MPLS_UNICAST_DATAGRAM:
        case MPLS_MULTICAST_DATAGRAM:
        case MPLSCP_PROTOCOL:
            pMplscpIf = (tMPLSCPIf *) pIf->pMplscpPtr;
            if (pMplscpIf != NULL)
            {
                GSEMRun (&pMplscpIf->MplscpGSEM, PPP_DOWN);
            }
            break;

        case IPX_DATAGRAM:
        case TELEBIT_COMP_PROTO:
        case SHIVA_COMP_PROTO:
        case IPXCP_PROTOCOL:
            pIpxcpIf = (tIPXCPIf *) pIf->pIpxcpPtr;
            if (pIpxcpIf != NULL)
            {
                GSEMRun (&pIpxcpIf->IpxcpGSEM, PPP_DOWN);
            }
            break;

        case CCP_DATA_OVER_MEMBER_LINK:
        case CCP_DATA_OVER_INDIVIDUAL_LINK:
        case CCP_OVER_MEMBER_LINK:
        case CCP_OVER_INDIVIDUAL_LINK:
            pCCPIf = (tCCPIf *) pIf->pCCPIf;
            if (pCCPIf != NULL)
            {
                GSEMRun (&pCCPIf->CCPGSEM, PPP_DOWN);
            }
            break;

        case ECP_DATA_OVER_MEMBER_LINK:
        case ECP_DATA_OVER_INDIVIDUAL_LINK:
        case ECP_OVER_MEMBER_LINK:
        case ECP_OVER_INDIVIDUAL_LINK:
            pECPIf = (tECPIf *) pIf->pECPIf;
            if (pECPIf != NULL)
            {
                GSEMRun (&pECPIf->ECPGSEM, PPP_DOWN);
            }
            break;

#ifdef BAP
        case BAP_PROTOCOL:
        case BACP_PROTOCOL:
            if (pIf->MPInfo.BundleInfo.BWMProtocolInfo.Protocol == BAP_PROTOCOL)
            {
                pBAPInfo =
                    (tBAPInfo *) pIf->MPInfo.BundleInfo.BWMProtocolInfo.
                    pProtocolInfo;
                pBACPIf = &pBAPInfo->BACPIf;
                GSEMRun (&pBACPIf->BACPGSEM, PPP_DOWN);
            }
            break;
#endif

        case CHAP_PROTOCOL:
            if (pIf->pAuthPtr != NULL)
            {
                CHAPRecdProtRej (pIf->pAuthPtr);
            }
            break;

        case PAP_PROTOCOL:
            if (pIf->pAuthPtr != NULL)
            {
                PAPRecdProtRej (pIf->pAuthPtr);
            }
            break;
        case EAP_PROTOCOL:
            if (pIf->pAuthPtr != NULL)
            {
                EAPRecdProtRej (pIf->pAuthPtr);
            }
            break;

        case LQM_PROTOCOL:
            if (pIf->pLqmPtr != NULL)
            {
#ifdef LQM
                LQMRecdProtRej (pIf->pLqmPtr);
#endif
            }
            break;

        default:
            return (DISCARD);
    }
    return (RXJ_PLUS);
}

/*********************************************************************
*    Function Name        :    LCPRecdRxrReqPkt
*    Description            :
*                Whenever there is an echo/discard/id request  packet  received 
*    from the peer, the LCP module uses this function to trigger a RXR event 
*    for the GSEM.
*    Parameter(s)        :  
*                pGSEM    -    Pointer to  the LCP tGSEM.
*                pInPkt    -    Pointer to  the incoming packet.
*                Length    -    Length of the Configuration Information.
*            Identifier    -    Identifier field of the incoming packet.
*    Return Value          :     RXR 
*********************************************************************/
INT1
LCPRecdRxrReqPkt (tGSEM * pGSEM, t_MSG_DESC * pInPkt, UINT2 Length,
                  UINT1 Identifier)
{

    /* Makefile changes - unused */
    PPP_UNUSED (Length);
    PPP_TRC1 (CONTROL_PLANE, "\t\t\t[ECHO REQ]\tId: [%d]", Identifier);
    pGSEM->pInParam = pInPkt;
    return (RXR);
}

/*********************************************************************
*    Function Name        :    LCPRecdEchoReply
*    Description            :
*                This function generates a RXR event to the GSEM on finding 
*    that the frame received is valid Echo Response from peer.
*    Parameter(s)            :  
*                pGSEM    -    Pointer to  the LCP tGSEM.
*                pInPkt    -    Pointer to  the incoming packet.
*                Length    -    Length of the Configuration Information.
*            Identifier    -    Identifier field of the incoming packet.
*    Return Value  : RXR /DISCARD
*********************************************************************/
INT1
LCPRecdEchoReply (tGSEM * pGSEM, t_MSG_DESC * pInPkt, UINT2 Length,
                  UINT1 Identifier)
{

    PPP_TRC (CONTROL_PLANE, "\t\t\t[ECHO REPLY]");
    PPP_TRC2 (CONTROL_PLANE, "\nIncoming ECHO Id: [%d], Sent ECHO Id : [%d]",
              Identifier, pGSEM->pIf->EchoInfo.EchoId);
    PPP_UNUSED (Length);

    pGSEM->pInParam = pInPkt;

    /* Check the id field ... */
    if (Identifier != pGSEM->pIf->EchoInfo.EchoId)
    {
        PPP_TRC (ALL_FAILURE, "Invalid Echo packet rcvd. but discarded....");
        return (DISCARD);
    }
    return (RXR);
}

/***********************************************************************
    OPTION SPECIFIC FUNCTIONS FOR PROCESSING CONF_REQ 
************************************************************************/
/*********************************************************************
*    Function Name        :    LCPProcessMRUConfReq 
*    Description            :
*        This function is invoked to process the MRU option present
*    in the CONF_REQ and to construct appropriate response 
*    Parameter(s)        :  
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*     Offset     -  indicates the place in the outgoing buffer at which 
*                   the option value is appended
*  PresenceFlag  -  indicates whether the option is present in the recd.
*                   CONF_REQ packet
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer 
*********************************************************************/
INT1
LCPProcessMRUConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal,
                      UINT2 Offset, UINT1 PresenceFlag)
{

    PPP_UNUSED (pInPkt);
    PPP_TRC1 (CONTROL_PLANE, "\t\t\tMRU     : %d", OptVal.ShortVal);
    if ((PresenceFlag == SET) && (OptVal.ShortVal < 4))
    {
        ASSIGN2BYTE (pGSEM->pOutParam, Offset,
                     pGSEM->pIf->LcpOptionsAllowedForPeer.MRU);
        return BYTE_LEN_2;
    }

    if (PresenceFlag == SET)
    {
        pGSEM->pIf->LcpOptionsAckedByLocal.MRU = OptVal.ShortVal;
        return (ACK);
    }
    else
    {
        ASSIGN2BYTE (pGSEM->pOutParam, Offset,
                     pGSEM->pIf->LcpOptionsAllowedForPeer.MRU);
        return (BYTE_LEN_2);
    }
}

/*********************************************************************
*    Function Name        :    LCPProcessPaddingConfReq 
*    Description            :
*            This function is invoked to process the Padding option present
*    in the CONF_REQ and to construct appropriate response 
*    Parameter(s)        :  
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*     Offset     -  indicates the place in the outgoing buffer at which 
*                   the option value is appended
*  PresenceFlag  -  indicates whether the option is present in the recd.
*                   CONF_REQ packet
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer 
*********************************************************************/
INT1
LCPProcessPaddingConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal,
                          UINT2 Offset, UINT1 PresenceFlag)
{
    PPP_UNUSED (pInPkt);
    PPP_TRC1 (CONTROL_PLANE, "\t\t\tPADDING : %d", OptVal.CharVal);
    if (PresenceFlag == SET)
    {
        pGSEM->pIf->LcpOptionsAckedByLocal.MaxPadVal = OptVal.CharVal;
        pGSEM->pNegFlagsPerIf[PADDING_IDX].FlagMask |= ACKED_BY_LOCAL_SET;
        return (ACK);
    }
    else
    {
        ASSIGN1BYTE (pGSEM->pOutParam, Offset,
                     pGSEM->pIf->LcpOptionsAllowedForPeer.MaxPadVal);
        return (BYTE_LEN_1);
    }
}

/********************************************************************
*  Function Name : LCPProcessACCMConfReq 
*  Description   :
*        This function is invoked to process the ACCM option present
*  in the CONF_REQ and to construct appropriate response 
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*     Offset     -  indicates the place in the outgoing buffer at which 
*                   the option value is appended
*  PresenceFlag  -  indicates whether the option is present in the recd.
*                   CONF_REQ packet
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer 
*********************************************************************/
INT1
LCPProcessACCMConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal,
                       UINT2 Offset, UINT1 PresenceFlag)
{
    tPPPIf             *pIf = NULL;

    pIf = pGSEM->pIf;

    PPP_UNUSED (pInPkt);
    if (PresenceFlag == SET)
    {

        PPP_TRC4 (CONTROL_PLANE, "\t\t\tACCM    : %x %x %x %x",
                  OptVal.StrVal[0], OptVal.StrVal[1], OptVal.StrVal[2],
                  OptVal.StrVal[3]);
        if ((pIf->LcpOptionsAllowedForPeer.AsyncMap[0] & ~OptVal.StrVal[0])
            || (pIf->LcpOptionsAllowedForPeer.AsyncMap[1] & ~OptVal.StrVal[1])
            || (pIf->LcpOptionsAllowedForPeer.AsyncMap[2] & ~OptVal.StrVal[2])
            || (pIf->LcpOptionsAllowedForPeer.AsyncMap[3] & ~OptVal.StrVal[3]))
        {
            ASSIGN1BYTE (pGSEM->pOutParam, Offset,
                         pIf->LcpOptionsAllowedForPeer.AsyncMap[0] | OptVal.
                         StrVal[0]);
            ASSIGN1BYTE (pGSEM->pOutParam, Offset + 1,
                         pIf->LcpOptionsAllowedForPeer.AsyncMap[1] | OptVal.
                         StrVal[1]);
            ASSIGN1BYTE (pGSEM->pOutParam, Offset + 2,
                         pIf->LcpOptionsAllowedForPeer.AsyncMap[2] | OptVal.
                         StrVal[2]);
            ASSIGN1BYTE (pGSEM->pOutParam, Offset + 3,
                         pIf->LcpOptionsAllowedForPeer.AsyncMap[3] | OptVal.
                         StrVal[3]);
            return (BYTE_LEN_4);
        }
        MEMCPY (pIf->LcpOptionsAckedByLocal.AsyncMap, OptVal.StrVal, 4);
        return (ACK);
    }
    ASSIGNSTR (pGSEM->pOutParam, pIf->LcpOptionsAllowedForPeer.AsyncMap, Offset,
               BYTE_LEN_4);
    return (BYTE_LEN_4);
}

/*********************************************************************
*  Function Name : LCPProcessAUTHConfReq 
*  Description   :
*        This function is invoked to process the AUTH option present
*  in the CONF_REQ and to construct appropriate response 
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*     Offset     -  indicates the place in the outgoing buffer at which 
*                   the option value is appended
*  PresenceFlag  -  indicates whether the option is present in the recd.
*                   CONF_REQ packet
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer 
*********************************************************************/
INT1
LCPProcessAUTHConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal,
                       UINT2 Offset, UINT1 PresenceFlag)
{
    tAUTHIf            *pAuthPtr = NULL;
    tPPPIf             *pIf = NULL;
    UINT1               u1SubOptVal = 0;
    INT1                u1RetVal;

    u1RetVal = ACK;
    pIf = pGSEM->pIf;
    pAuthPtr = pIf->pAuthPtr;

    PPP_UNUSED (pInPkt);
    PPP_UNUSED (PresenceFlag);

    switch (OptVal.ShortVal)
    {
        case CHAP_PROTOCOL:
            PPP_TRC1 (CONTROL_PLANE, "\t\t\tAUTH    : %s", "CHAP");
            if (ExtractedLength != AUTH_CHAP_OPT_LEN)
            {
                return (DISCARD);
            }
            EXTRACT1BYTE (pInPkt, u1SubOptVal);
            if ((u1SubOptVal != MD5) && (u1SubOptVal != MSCHAP_ALGORITHM))
            {
                if ((pAuthPtr->SecurityInfo.CHAPLocToRemSecrets)
                    || (IndexZeroSecurityInfo.CHAPLocToRemSecrets))
                {
                    ASSIGN2BYTE (pGSEM->pOutParam, Offset, OptVal.ShortVal);
                    ASSIGN1BYTE (pGSEM->pOutParam, Offset + 2, MD5);
                    u1RetVal = BYTE_LEN_3;
                }
                else
                {
                    if ((pAuthPtr->SecurityInfo.MSCHAPLocToRemSecrets)
                        || (IndexZeroSecurityInfo.MSCHAPLocToRemSecrets))
                    {
                        ASSIGN2BYTE (pGSEM->pOutParam, Offset, OptVal.ShortVal);
                        ASSIGN1BYTE (pGSEM->pOutParam, Offset + 2,
                                     MSCHAP_ALGORITHM);
                        u1RetVal = BYTE_LEN_3;
                    }
                    else
                    {
                        if ((pAuthPtr->SecurityInfo.PAPLocToRemSecrets)
                            || (IndexZeroSecurityInfo.PAPLocToRemSecrets))
                        {
                            ASSIGN2BYTE (pGSEM->pOutParam, Offset,
                                         PAP_PROTOCOL);
                            u1RetVal = BYTE_LEN_2;
                        }
                        else
                        {
                            if ((pAuthPtr->SecurityInfo.EAPMD5LocToRemSecrets)
                                || (IndexZeroSecurityInfo.
                                    EAPMD5LocToRemSecrets))
                            {
                                ASSIGN2BYTE (pGSEM->pOutParam, Offset,
                                             EAP_PROTOCOL);
                                u1RetVal = BYTE_LEN_2;
                            }
                        }
                    }
                }
            }
            else
            {
                if (((pAuthPtr->SecurityInfo.CHAPLocToRemSecrets)
                     || (IndexZeroSecurityInfo.CHAPLocToRemSecrets))
                    && (u1SubOptVal == MD5))
                {
                    pIf->LcpStatus.AuthStatus |= CHAP_WITH_PEER;
                }
                else
                {
                    if (((pAuthPtr->SecurityInfo.MSCHAPLocToRemSecrets)
                         || (IndexZeroSecurityInfo.MSCHAPLocToRemSecrets))
                        && (u1SubOptVal == MSCHAP_ALGORITHM))
                    {

                        pIf->LcpStatus.AuthStatus |= MSCHAP_WITH_PEER;
                    }
                    else
                    {
                        if (((pAuthPtr->SecurityInfo.CHAPLocToRemSecrets) ||
                             (IndexZeroSecurityInfo.CHAPLocToRemSecrets)) &&
                            (u1SubOptVal == MSCHAP_ALGORITHM))
                        {
                            ASSIGN2BYTE (pGSEM->pOutParam, Offset,
                                         OptVal.ShortVal);
                            ASSIGN1BYTE (pGSEM->pOutParam, Offset + 2, MD5);
                            u1RetVal = BYTE_LEN_3;
                        }
                        else if (((pAuthPtr->SecurityInfo.MSCHAPLocToRemSecrets)
                                  || (IndexZeroSecurityInfo.
                                      MSCHAPLocToRemSecrets))
                                 && (u1SubOptVal == MD5))
                        {
                            ASSIGN2BYTE (pGSEM->pOutParam, Offset,
                                         OptVal.ShortVal);
                            ASSIGN1BYTE (pGSEM->pOutParam, Offset + 2,
                                         MSCHAP_ALGORITHM);
                            u1RetVal = BYTE_LEN_3;
                        }
                        else if ((pAuthPtr->SecurityInfo.PAPLocToRemSecrets)
                                 || (IndexZeroSecurityInfo.PAPLocToRemSecrets))
                        {
                            ASSIGN2BYTE (pGSEM->pOutParam, Offset,
                                         PAP_PROTOCOL);
                            u1RetVal = BYTE_LEN_2;
                        }
                        else
                        {
                            if ((pAuthPtr->SecurityInfo.EAPMD5LocToRemSecrets)
                                || (IndexZeroSecurityInfo.
                                    EAPMD5LocToRemSecrets))
                            {
                                ASSIGN2BYTE (pGSEM->pOutParam, Offset,
                                             EAP_PROTOCOL);
                                u1RetVal = BYTE_LEN_2;
                            }
                        }
                    }
                }
            }
            break;

        case PAP_PROTOCOL:
            PPP_TRC1 (CONTROL_PLANE, "\t\t\tAUTH    : %s", "PAP");
            if (ExtractedLength != AUTH_PAP_OPT_LEN)
            {
                return (DISCARD);
            }
            if ((pAuthPtr->SecurityInfo.PAPLocToRemSecrets)
                || (IndexZeroSecurityInfo.PAPLocToRemSecrets))
            {
                pIf->LcpStatus.AuthStatus |= PAP_WITH_PEER;
            }
            else
            {
                if ((pAuthPtr->SecurityInfo.CHAPLocToRemSecrets)
                    || (IndexZeroSecurityInfo.CHAPLocToRemSecrets))
                {
                    ASSIGN2BYTE (pGSEM->pOutParam, Offset, CHAP_PROTOCOL);
                    ASSIGN1BYTE (pGSEM->pOutParam, Offset + 2, MD5);
                    u1RetVal = BYTE_LEN_3;
                }
                else
                {
                    if ((pAuthPtr->SecurityInfo.MSCHAPLocToRemSecrets)
                        || (IndexZeroSecurityInfo.MSCHAPLocToRemSecrets))
                    {
                        ASSIGN2BYTE (pGSEM->pOutParam, Offset, CHAP_PROTOCOL);
                        ASSIGN1BYTE (pGSEM->pOutParam, Offset + 2,
                                     MSCHAP_ALGORITHM);
                        u1RetVal = BYTE_LEN_3;
                    }
                    else
                    {
                        if ((pAuthPtr->SecurityInfo.EAPMD5LocToRemSecrets)
                            || (IndexZeroSecurityInfo.EAPMD5LocToRemSecrets))
                        {
                            ASSIGN2BYTE (pGSEM->pOutParam, Offset,
                                         EAP_PROTOCOL);
                            u1RetVal = BYTE_LEN_2;
                        }
                    }
                }
            }
            break;
        case EAP_PROTOCOL:
            PPP_TRC1 (CONTROL_PLANE, "\t\t\tAUTH    : %s", "EAP");
            if (ExtractedLength != AUTH_EAP_OPT_LEN)
            {
                return (DISCARD);
            }
            if ((pAuthPtr->SecurityInfo.EAPMD5LocToRemSecrets)
                || (IndexZeroSecurityInfo.EAPMD5LocToRemSecrets))
            {
                pIf->LcpStatus.AuthStatus |= EAP_WITH_PEER;
            }
            else
            {
                if ((pAuthPtr->SecurityInfo.CHAPLocToRemSecrets)
                    || (IndexZeroSecurityInfo.CHAPLocToRemSecrets))
                {
                    ASSIGN2BYTE (pGSEM->pOutParam, Offset, CHAP_PROTOCOL);
                    ASSIGN1BYTE (pGSEM->pOutParam, Offset + 2, MD5);
                    u1RetVal = BYTE_LEN_3;
                }
                else
                {
                    if ((pAuthPtr->SecurityInfo.MSCHAPLocToRemSecrets)
                        || (IndexZeroSecurityInfo.MSCHAPLocToRemSecrets))
                    {
                        ASSIGN2BYTE (pGSEM->pOutParam, Offset, CHAP_PROTOCOL);
                        ASSIGN1BYTE (pGSEM->pOutParam, Offset + 2,
                                     MSCHAP_ALGORITHM);
                        u1RetVal = BYTE_LEN_3;
                    }
                    else
                    {
                        if ((pAuthPtr->SecurityInfo.PAPLocToRemSecrets)
                            || (IndexZeroSecurityInfo.PAPLocToRemSecrets))
                        {
                            ASSIGN2BYTE (pGSEM->pOutParam, Offset,
                                         PAP_PROTOCOL);
                            u1RetVal = BYTE_LEN_2;
                        }
                    }
                }
            }
            break;
        default:
            if ((pAuthPtr->SecurityInfo.CHAPLocToRemSecrets)
                || (IndexZeroSecurityInfo.CHAPLocToRemSecrets))
            {
                ASSIGN2BYTE (pGSEM->pOutParam, Offset, CHAP_PROTOCOL);
                ASSIGN1BYTE (pGSEM->pOutParam, Offset + 2, MD5);
                u1RetVal = BYTE_LEN_3;
            }
            else
            {
                if ((pAuthPtr->SecurityInfo.MSCHAPLocToRemSecrets)
                    || (IndexZeroSecurityInfo.MSCHAPLocToRemSecrets))
                {
                    ASSIGN2BYTE (pGSEM->pOutParam, Offset, CHAP_PROTOCOL);
                    ASSIGN1BYTE (pGSEM->pOutParam, Offset + 2,
                                 MSCHAP_ALGORITHM);
                    u1RetVal = BYTE_LEN_3;
                }
                else
                {
                    if ((pAuthPtr->SecurityInfo.PAPLocToRemSecrets)
                        || (IndexZeroSecurityInfo.PAPLocToRemSecrets))
                    {
                        ASSIGN2BYTE (pGSEM->pOutParam, Offset, PAP_PROTOCOL);
                        u1RetVal = BYTE_LEN_2;
                    }
                    else
                    {
                        if ((pAuthPtr->SecurityInfo.EAPMD5LocToRemSecrets)
                            || (IndexZeroSecurityInfo.EAPMD5LocToRemSecrets))
                        {
                            ASSIGN2BYTE (pGSEM->pOutParam, Offset,
                                         EAP_PROTOCOL);
                            u1RetVal = BYTE_LEN_2;
                        }
                    }
                }
            }

    }
    return (u1RetVal);
}

/*********************************************************************
*  Function Name : LCPProcessMagicConfReq 
*  Description   :
*        This function is invoked to process the Magic option present
*  in the CONF_REQ and to construct appropriate response 
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*     Offset     -  indicates the place in the outgoing buffer at which 
*                   the option value is appended
*  PresenceFlag  -  indicates whether the option is present in the recd.
*                   CONF_REQ packet
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer 
*********************************************************************/
INT1
LCPProcessMagicConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal,
                        UINT2 Offset, UINT1 PresenceFlag)
{
    tPPPIf             *pIf;
    UINT4               MagicNum;

    PPP_UNUSED (PresenceFlag);
    pIf = pGSEM->pIf;
    PPP_UNUSED (pInPkt);
    PPP_TRC1 (CONTROL_PLANE, "\t\t\tMAGIC PPP_NO: %x", OptVal.LongVal);
    if (PresenceFlag == NOT_SET)
    {
        ASSIGN4BYTE (pGSEM->pOutParam, Offset, OptVal.LongVal);
        return (BYTE_LEN_4);
    }

    if ((pIf->LcpOptionsAckedByPeer.MagicNumber == OptVal.LongVal)
        || (OptVal.LongVal == 0))
    {

        PPP_DBG ("Generating new magic number for nak");
        MagicNum = GENERATE_MAGIC_NUMBER ();
        pIf->LcpOptionsDesired.MagicNumber =
            pIf->LcpOptionsAckedByPeer.MagicNumber = MagicNum;
        ASSIGN4BYTE (pGSEM->pOutParam, Offset, MagicNum);
        return (BYTE_LEN_4);
    }
    pIf->LcpOptionsAckedByLocal.MagicNumber = OptVal.LongVal;
    return (ACK);
}

/*********************************************************************
*  Function Name : LCPProcessLQRConfReq 
*  Description   :
*        This function is invoked to process the LQR option present
*  in the CONF_REQ and to construct appropriate response 
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*     Offset     -  indicates the place in the outgoing buffer at which 
*                   the option value is appended
*  PresenceFlag  -  indicates whether the option is present in the recd.
*                   CONF_REQ packet
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer 
*********************************************************************/
INT1
LCPProcessLQRConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal,
                      UINT2 Offset, UINT1 PresenceFlag)
{
    tPPPIf             *pIf;
    INT1                i1RetVal;
    UINT4               u4RecdPeriod = 0;

    PPP_UNUSED (pInPkt);
    PPP_UNUSED (PresenceFlag);
    PPP_TRC1 (CONTROL_PLANE, "\t\t\tLQM     : %x", OptVal.ShortVal);
    pIf = pGSEM->pIf;
    i1RetVal = ACK;

    if (OptVal.ShortVal == pIf->pLqmPtr->Protocol)
    {
        EXTRACT4BYTE (pInPkt, u4RecdPeriod);

        if (PresenceFlag == NOT_SET)
        {
            ASSIGN2BYTE (pGSEM->pOutParam, Offset, LQM_PROTOCOL);
            ASSIGN4BYTE (pGSEM->pOutParam, Offset + 2, u4RecdPeriod);
            return BYTE_LEN_6;
        }
        if (PERIOD_ZERO (u4RecdPeriod))
        {

            if (((pGSEM->
                  pNegFlagsPerIf[LQR_IDX].FlagMask & ACKED_BY_PEER_MASK) ==
                 NOT_SET)
                ||
                ((pGSEM->pNegFlagsPerIf[LQR_IDX].FlagMask & ACKED_BY_PEER_MASK)
                 &&
                 (PERIOD_ZERO (pIf->pLqmPtr->LQMCONFIGS (ConfigReportPeriod)))))
            {
                ASSIGN2BYTE (pGSEM->pOutParam, Offset, pIf->pLqmPtr->Protocol);
                ASSIGN4BYTE (pGSEM->pOutParam, Offset + 2,
                             (UINT4) ALT_LQR_PERIOD);
                i1RetVal = BYTE_LEN_6;
            }
        }
        else
        {
            pIf->pLqmPtr->LQMStatus.PeerReportPeriod = u4RecdPeriod;
        }
    }
    else
    {

        ASSIGN2BYTE (pGSEM->pOutParam, Offset, LQM_PROTOCOL);
        ASSIGN4BYTE (pGSEM->pOutParam, (UINT4) (Offset + 2), u4RecdPeriod);
        i1RetVal = BYTE_LEN_6;
    }
    return (i1RetVal);
}

/*********************************************************************
*  Function Name : LCPProcessFCSConfReq 
*  Description   :
*        This function is invoked to process the FCS option present
*  in the CONF_REQ and to construct appropriate response 
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*     Offset     -  indicates the place in the outgoing buffer at which 
*                   the option value is appended
*  PresenceFlag  -  indicates whether the option is present in the recd.
*                   CONF_REQ packet
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer 
*********************************************************************/
INT1
LCPProcessFCSConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal,
                      UINT2 Offset, UINT1 PresenceFlag)
{
    tPPPIf             *pIf = NULL;

    PPP_UNUSED (pInPkt);
    PPP_TRC1 (CONTROL_PLANE, "\t\t\tFCS     : %x", OptVal.CharVal);
    pIf = pGSEM->pIf;
    if (PresenceFlag == SET)
    {
        if ((OptVal.CharVal == NO_FCS_REQ)
            || ((OptVal.CharVal == FCS_16_REQ)
                && (pIf->LcpOptionsAllowedForPeer.FCSSize & FCS_16_AVAILABLE))
            || ((OptVal.CharVal == FCS_32_REQ)
                && (pIf->LcpOptionsAllowedForPeer.FCSSize & FCS_32_AVAILABLE))
            || ((OptVal.CharVal == FCS_48_REQ)
                && (pIf->LcpOptionsAllowedForPeer.FCSSize & FCS_48_AVAILABLE)))
        {
            pIf->LcpOptionsAckedByLocal.FCSSize = OptVal.CharVal;
            return (ACK);
        }
        else
        {
            ASSIGN1BYTE (pGSEM->pOutParam, Offset, FCS_16_REQ);
        }
    }
    else
    {
        ASSIGN1BYTE (pGSEM->pOutParam, Offset,
                     pIf->LcpOptionsAllowedForPeer.FCSSize);
    }
    return (BYTE_LEN_1);
}

/*********************************************************************
*    Function Name    :    LCPProcessCallBackConfReq 
*    Description        :
*        This function is invoked to process the CallBack option present
*    in the CONF_REQ and to construct appropriate response 
*    Input            :
*            pGSEM    -    Pointer to the GSEM structure corresponding to the
*                        packet.
*            pInPkt    -    Points to the incoming packet buffer. 
*            tOptVal    -    Option Value received  to be processed
*            Offset    -    Indicates the place in the outgoing buffer at which 
*                        the option value is appended.
*    PresenceFlag    -    Indicates whether the option is present in the recd.
*                       CONF_REQ packet.
*    Return Value    :    Returns the number of bytes appended in the outgoing
*                        buffer. 
*********************************************************************/
INT1
LCPProcessCallBackConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal,
                           UINT2 Offset, UINT1 PresenceFlag)
{
    tPPPIf             *pIf = NULL;
    UINT1               u1MsgLength = 0;

    PPP_TRC1 (CONTROL_PLANE, "\t\t\tCALLBACK: %x", OptVal.CharVal);

    PPP_TRC2 (CONTROL_PLANE, "Offset=%d, PresenceFlag=%d", Offset,
              PresenceFlag);
    PPP_UNUSED (pInPkt);
    pIf = pGSEM->pIf;
    u1MsgLength = (UINT1) (ExtractedLength - (OPT_HDR_LEN + 1));

    if ((OptVal.CharVal == 0) && (u1MsgLength != 0))
    {
        return (DISCARD);
    }
    pIf->LcpOptionsAckedByLocal.LCPCallBackInfo.CallBackOperation =
        OptVal.CharVal;

    pIf->LcpOptionsAckedByLocal.LCPCallBackInfo.CallBackMsgLen = u1MsgLength;
    if (pIf->LcpOptionsAckedByLocal.LCPCallBackInfo.CallBackMsgLen != 0)
    {
        if ((pIf->LcpOptionsAckedByLocal.LCPCallBackInfo.pCallBackMsg =
             ALLOC_STR (u1MsgLength)) == NULL)
        {
            return (DISCARD);
        }
        EXTRACTSTR (pInPkt,
                    pIf->LcpOptionsAckedByLocal.LCPCallBackInfo.pCallBackMsg,
                    u1MsgLength);
    }

    return (ACK);
}

/***********************************************************************
    OPTION SPECIFIC FUNCTIONS FOR PROCESSING CONF_NAK
************************************************************************/
/*********************************************************************
*  Function Name : LCPProcessMRUConfNak 
*  Description   :
*        This function is invoked to process the MRU option present
*  in the CONF_NAK and to construct appropriate response 
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*  Return Value  :  OK/NOT_OK 
*********************************************************************/
INT1
LCPProcessMRUConfNak (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal)
{

    PPP_UNUSED (pInPkt);
    PPP_TRC1 (CONTROL_PLANE, "\t\t\tMRU     : %d", OptVal.ShortVal);
    if (OptVal.ShortVal == DEF_MRU_SIZE)
    {
        pGSEM->pIf->LcpOptionsAckedByPeer.MRU = DEF_MRU_SIZE;
        pGSEM->pIf->LcpGSEM.pNegFlagsPerIf[MRU_IDX].FlagMask &=
            ACKED_BY_PEER_NOT_SET;
        pRecdOptFlags[MRU_IDX] = NOT_SET;
        return (OK);
    }
    else if ((OptVal.ShortVal <= pGSEM->pIf->LcpOptionsAckedByPeer.MRU) ||
             (OptVal.ShortVal <= DEF_MRU_SIZE))
    {

        pGSEM->pIf->LcpOptionsAckedByPeer.MRU = OptVal.ShortVal;
        return (OK);

    }
    return (CLOSE);
}

/*********************************************************************
*  Function Name : LCPProcessACCMConfNak 
*  Description   :
*        This function is invoked to process the ACCM option present
*  in the CONF_NAK and to construct appropriate response 
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*  Return Value  :  OK/NOT_OK 
*********************************************************************/
INT1
LCPProcessACCMConfNak (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal)
{
    PPP_UNUSED (pInPkt);
    PPP_TRC4 (CONTROL_PLANE, "\t\t\tACCM    : %x %x %x %x", OptVal.StrVal[0],
              OptVal.StrVal[1], OptVal.StrVal[2], OptVal.StrVal[3]);
    MEMCPY (pGSEM->pIf->LcpOptionsAckedByPeer.AsyncMap, OptVal.StrVal, 4);
    /* To be modified (if necessary) */
    return (OK);
}

/*********************************************************************
*  Function Name : LCPProcessAUTHConfNak 
*  Description   :
*        This function is invoked to process the AUTH option present
*  in the CONF_NAK and to construct appropriate response 
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*  Return Value  :  OK/NOT_OK 
*********************************************************************/
INT1
LCPProcessAUTHConfNak (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal)
{
    tAUTHIf            *pAuthPtr = NULL;
    UINT1               u1SubOptVal = 0;

    INT2                i2Index = 0;

    PPP_TRC (CONTROL_PLANE, "\t\t\tAUTH   ");
    pAuthPtr = pGSEM->pIf->pAuthPtr;

    PPP_UNUSED (pInPkt);
    pAuthPtr->LastTriedIndex++;
    if (pGSEM->pNegFlagsPerIf[AUTH_IDX].FlagMask & ACKED_BY_PEER_MASK)
    {
        if ((pAuthPtr->LastTriedIndex >= pAuthPtr->SecurityInfo.NumActiveProt)
            && (pAuthPtr->LastTriedIndex -
                pAuthPtr->SecurityInfo.NumActiveProt >=
                IndexZeroSecurityInfo.NumActiveProt))
        {
            return (CLOSE);
        }
        if (OptVal.ShortVal == CHAP_PROTOCOL)
        {
            EXTRACT1BYTE (pInPkt, u1SubOptVal);
            if ((u1SubOptVal != MD5) && (u1SubOptVal != MSCHAP_ALGORITHM))
            {
                return (NOT_OK);
            }
        }

        for (i2Index = 0; i2Index < pAuthPtr->SecurityInfo.NumActiveProt;
             i2Index++)
        {
            if (OptVal.ShortVal ==
                pAuthPtr->SecurityInfo.PrefList[i2Index].Protocol)
            {
                if (OptVal.ShortVal == CHAP_PROTOCOL)
                {
                    if (u1SubOptVal == pAuthPtr->SecurityInfo.
                        PrefList[i2Index].ChapAlgo)
                        pAuthPtr->LastTriedIndex = i2Index;
                }
                else
                {
                    pAuthPtr->LastTriedIndex = i2Index;
                }
            }
        }
        return (OK);
    }
    return (CLOSE);
}

/*********************************************************************
*  Function Name : LCPProcessLQRConfNak 
*  Description   :
*        This function is invoked to process the LQR option present
*  in the CONF_NAK and to construct appropriate response 
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*  Return Value  :  
*********************************************************************/
INT1
LCPProcessLQRConfNak (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal)
{
    UINT4               u4SubOptVal;

    PPP_UNUSED (pInPkt);
    PPP_UNUSED (OptVal);
    PPP_TRC (CONTROL_PLANE, "\t\t\tLQM   ");
    if (pGSEM->pNegFlagsPerIf[LQR_IDX].FlagMask & ACKED_BY_PEER_MASK)
    {
        EXTRACT4BYTE (pInPkt, u4SubOptVal);
        if (PERIOD_NON_ZERO (u4SubOptVal))
        {
            pGSEM->pIf->pLqmPtr->LQMCONFIGS (ConfigReportPeriod) = u4SubOptVal;
        }
        return (OK);
    }
    return (CLOSE);
}

/*********************************************************************
*  Function Name : LCPProcessMagicConfNak 
*  Description   :
*        This function is invoked to process the Magic option present
*  in the CONF_NAK and to construct appropriate response 
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*  Return Value  :  
*********************************************************************/
INT1
LCPProcessMagicConfNak (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal)
{
    tPPPIf             *pIf;

    PPP_UNUSED (pInPkt);
    PPP_TRC1 (CONTROL_PLANE, "\t\t\tMAGIC PPP_NO : %x", OptVal.LongVal);
    pIf = pGSEM->pIf;
    if (pGSEM->pNegFlagsPerIf[MAGIC_IDX].FlagMask & ACKED_BY_PEER_MASK)
    {
        ++pIf->LinkInfo.NumLoops;
        if (pIf->LinkInfo.NumLoops == pIf->LinkInfo.MaxNumLoops)
        {
            return (CLOSE);
        }
    }
    pIf->LcpOptionsDesired.MagicNumber =
        pIf->LcpOptionsAckedByPeer.MagicNumber = GENERATE_MAGIC_NUMBER ();
    return (OK);
}

/*********************************************************************
*  Function Name : LCPProcessFCSConfNak 
*  Description   :
*        This function is invoked to process the FCS option present
*  in the CONF_NAK and to construct appropriate response 
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*  Return Value  :  
*********************************************************************/
INT1
LCPProcessFCSConfNak (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal)
{
    PPP_UNUSED (pInPkt);
    PPP_TRC1 (CONTROL_PLANE, "\t\t\tFCS      : %x", OptVal.CharVal);
    if (OptVal.CharVal <= 2 * FCSAvailability)
    {
        pGSEM->pIf->LcpOptionsAckedByPeer.FCSSize = OptVal.CharVal;
    }
    return (OK);
}

/*********************************************************************
*  Function Name : LCPProcessPaddingConfNak 
*  Description   :
*        This function is invoked to process the Padding option present
*  in the CONF_NAK and to construct appropriate response 
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*  Return Value  :  OK/NOT_OK 
*********************************************************************/
INT1
LCPProcessPaddingConfNak (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal)
{
    PPP_UNUSED (pInPkt);
    PPP_TRC1 (CONTROL_PLANE, "\t\t\tPADDING  : %x", OptVal.CharVal);
    if (pGSEM->pNegFlagsPerIf[PADDING_IDX].FlagMask & ACKED_BY_PEER_MASK)
    {
        if (OptVal.CharVal <= pGSEM->pIf->LcpOptionsAckedByPeer.MaxPadVal)
        {
            pGSEM->pIf->LcpOptionsAckedByPeer.MaxPadVal = OptVal.CharVal;
        }
    }
    return (OK);
}

/*********************************************************************
*  Function Name : LCPProcessCallBackConfNak 
*  Description   :
*        This function is invoked to process the CallBack option present
*  in the CONF_NAK and to construct appropriate response 
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*  Return Value  :  
*********************************************************************/
INT1
LCPProcessCallBackConfNak (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal)
{

    PPP_UNUSED (pInPkt);
    PPP_TRC (CONTROL_PLANE, "\t\t\tCALLBACK : %x");
    if ((OptVal.CharVal == 0)
        && ((pGSEM->pNegFlagsPerIf[AUTH_IDX].FlagMask & ALLOWED_FOR_PEER_MASK)
            == ALLOWED_FOR_PEER_MASK))
    {
        pGSEM->pIf->LcpOptionsAckedByPeer.LCPCallBackInfo.CallBackOperation =
            OptVal.CharVal;
        return (OK);
    }
/* Nak with values other than 0 is discarded */
    return (DISCARD);
}

/***********************************************************************
    OPTION SPECIFIC FUNCTIONS FOR ADDING SUB_OPTIONS IN THE CONF_REQ
************************************************************************/
/*********************************************************************
*  Function Name : LCPAddAuthSubOpt
*  Description   :
*     This function is used to add authentication suboption (if any)
*  based on the primary option value (CHAP/PAP)
*  Input         :
*     pGSEM        -  pointer to the GSEM structure corresponding to the
*                   packet.
*     tOptVal    -  Option Value received  to be processed
*        pOutBuf    -    The outgoing buffer into which the sub-option has to 
*                    be added.    
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer 
*********************************************************************/
INT1
LCPAddAuthSubOpt (tGSEM * pGSEM, tOptVal OptVal, t_MSG_DESC * pOutBuf)
{

    PPP_UNUSED (OptVal);
    if (OptVal.ShortVal == CHAP_PROTOCOL)
    {

        if (pGSEM->pIf->pAuthPtr->LastTriedIndex <
            pGSEM->pIf->pAuthPtr->SecurityInfo.NumActiveProt)
        {
            if (pGSEM->pIf->pAuthPtr->SecurityInfo.
                PrefList[pGSEM->pIf->pAuthPtr->LastTriedIndex].ChapAlgo ==
                MSCHAP_ALGORITHM)
            {
                APPEND1BYTE (pOutBuf, MSCHAP_ALGORITHM);
            }
            else
            {
                if (pGSEM->pIf->pAuthPtr->SecurityInfo.
                    PrefList[pGSEM->pIf->pAuthPtr->LastTriedIndex].ChapAlgo ==
                    MD5)
                {
                    APPEND1BYTE (pOutBuf, MD5);
                }
            }
        }
        else
        {

            if (IndexZeroSecurityInfo.PrefList[pGSEM->pIf->pAuthPtr->
                                               LastTriedIndex -
                                               pGSEM->pIf->pAuthPtr->
                                               SecurityInfo.NumActiveProt].
                ChapAlgo == MSCHAP_ALGORITHM)
            {

                APPEND1BYTE (pOutBuf, MSCHAP_ALGORITHM);
            }

            else
            {

                if (IndexZeroSecurityInfo.PrefList[pGSEM->pIf->pAuthPtr->
                                                   LastTriedIndex -
                                                   pGSEM->pIf->pAuthPtr->
                                                   SecurityInfo.NumActiveProt].
                    ChapAlgo == MD5)
                {

                    APPEND1BYTE (pOutBuf, MD5);
                }
            }
        }
        return (BYTE_LEN_1);
    }
    return (BYTE_LEN_0);
}

/*********************************************************************
*  Function Name : LCPAddLQRSubOpt
*  Description   :
*     This function is used to add LQR suboption.
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     tOptVal    -  Option Value received  to be processed
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer 
*********************************************************************/
INT1
LCPAddLQRSubOpt (tGSEM * pGSEM, tOptVal OptVal, t_MSG_DESC * pOutBuf)
{

    PPP_UNUSED (OptVal);
    APPEND4BYTE (pOutBuf, pGSEM->pIf->pLqmPtr->LQMCONFIGS (ConfigReportPeriod));
    return (BYTE_LEN_4);
}

/*********************************************************************
*    Function Name    :    LCPAddCallBackSubOpt
*    Description        :
*                This function is used to add the LCPCallBack suboption.
*    Input            :
*            pGSEM    -    Pointer to the GSEM structure corresponding to the
*                        packet.
*            tOptVal    -    Option Value received  to be processed.
*    Return Value    :    Returns the number of bytes appended in the 
*                        outgoing buffer. 
*********************************************************************/
INT1
LCPAddCallBackSubOpt (tGSEM * pGSEM, tOptVal OptVal, t_MSG_DESC * pOutBuf)
{

    PPP_UNUSED (OptVal);
    if (pGSEM->pIf->LcpOptionsAckedByPeer.LCPCallBackInfo.pCallBackMsg != NULL)
    {
        APPENDSTR (pOutBuf,
                   pGSEM->pIf->LcpOptionsAckedByPeer.LCPCallBackInfo.
                   pCallBackMsg,
                   pGSEM->pIf->LcpOptionsAckedByPeer.LCPCallBackInfo.
                   CallBackMsgLen);
        return ((INT1) (pGSEM->pIf->LcpOptionsAckedByPeer.LCPCallBackInfo.
                        CallBackMsgLen));
    }
    return (0);
}

/***********************************************************************
    OPTION SPECIFIC FUNCTIONS FOR RETURNING THE ADDRESS OF OPTION DATA 
************************************************************************/
/*********************************************************************
*  function name : LCPReturnMRUPtr
*  description   :
*     this function returns the address of the location where the mru
*  value to be included in the conf_req, is stored
*  input         :
*     pgsem      -  points to the gsem structure
*     optlen     -  used to return the length of the option value (in
*                   case the option is of 'string' type)
*  return value  :  returns the option data address
*********************************************************************/
VOID               *
LCPReturnMRUPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    PPP_UNUSED (OptLen);
    PPP_TRC1 (CONTROL_PLANE, "\t\t\tMRU     : %d",
              pGSEM->pIf->LcpOptionsAckedByPeer.MRU);
    return (&pGSEM->pIf->LcpOptionsAckedByPeer.MRU);
}

/*********************************************************************
*  function name : LcpReturnPaddingPtr 
*  description   :
*     this function returns the address of the location where the padding
*  value to be included in the conf_req, is stored
*  input         :
*     pgsem      -  points to the gsem structure
*     optlen     -  used to return the length of the option value (in
*                   case the option is of 'string' type)
*  return value  :  returns the option data address
*********************************************************************/
VOID               *
LCPReturnPaddingPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    PPP_UNUSED (OptLen);
    PPP_TRC1 (CONTROL_PLANE, "\t\t\tPADDING : %d",
              pGSEM->pIf->LcpOptionsAckedByPeer.MaxPadVal);
    return (&pGSEM->pIf->LcpOptionsAckedByPeer.MaxPadVal);
}

/*********************************************************************
*  Function Name : LCPReturnACCMPtr
*  Description   :
*     This function returns the address of the location where the ACCM
*  value to be included in the CONF_REQ, is stored
*  Input         :
*     pGSEM      -  points to the GSEM structure
*     OptLen     -  used to return the length of the option value (in
*                   case the option is of 'STRING' type)
*  Return Value  :  returns the option data address
*********************************************************************/
VOID               *
LCPReturnACCMPtr (tGSEM * pGSEM, UINT1 *OptLen)
{

    *OptLen = 4;
    PPP_TRC1 (CONTROL_PLANE, "\t\t\tACCM    : %x",
              pGSEM->pIf->LcpOptionsAckedByPeer.AsyncMap);
    return (&pGSEM->pIf->LcpOptionsAckedByPeer.AsyncMap);
}

/*********************************************************************
*  Function Name : LCPReturnMagicPtr
*  Description   :
*     This function returns the address of the location where the Magic
*  value to be included in the CONF_REQ, is stored
*  Input         :
*     pGSEM      -  points to the GSEM structure
*     OptLen     -  used to return the length of the option value (in
*                   case the option is of 'STRING' type)
*  Return Value  :  returns the option data address
*********************************************************************/
VOID               *
LCPReturnMagicPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    PPP_UNUSED (OptLen);
    PPP_TRC1 (CONTROL_PLANE, "\t\t\tMAGIC PPP_NO: %x",
              pGSEM->pIf->LcpOptionsAckedByPeer.MagicNumber);
    return (&pGSEM->pIf->LcpOptionsAckedByPeer.MagicNumber);
}

/*********************************************************************
*  Function Name : LCPReturnAUTHPtr
*  Description   :
*     This function returns the address of the location where the AUTH 
*  value to be included in the CONF_REQ, is stored
*  Input         :
*     pGSEM      -  points to the GSEM structure
*     OptLen     -  used to return the length of the option value (in
*                   case the option is of 'STRING' type)
*  Return Value  :  returns the option data address
*********************************************************************/
VOID               *
LCPReturnAUTHPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    tAUTHIf            *pAuthPtr = NULL;

    PPP_UNUSED (OptLen);
    pAuthPtr = pGSEM->pIf->pAuthPtr;

    if (pAuthPtr != NULL)
    {

        if (pAuthPtr->LastTriedIndex < pAuthPtr->SecurityInfo.NumActiveProt)
        {
            PPP_TRC1 (CONTROL_PLANE, "\t\t\tAUTH    : %x",
                      pAuthPtr->SecurityInfo.PrefList[pAuthPtr->LastTriedIndex].
                      Protocol);
            return (&pAuthPtr->SecurityInfo.PrefList[pAuthPtr->LastTriedIndex].
                    Protocol);
        }

        PPP_TRC1 (CONTROL_PLANE, "\t\t\tAUTH    : %x",
                  IndexZeroSecurityInfo.PrefList[pAuthPtr->LastTriedIndex -
                                                 pAuthPtr->SecurityInfo.
                                                 NumActiveProt].Protocol);

        return (&IndexZeroSecurityInfo.
                PrefList[pAuthPtr->LastTriedIndex -
                         pAuthPtr->SecurityInfo.NumActiveProt].Protocol);
    }
    return (VOID *) NULL;
}

/*********************************************************************
*  Function Name : LCPReturnLQRPtr
*  Description   :
*     This function returns the address of the location where the LQR
*  value to be included in the CONF_REQ, is stored
*  Input         :
*     pGSEM      -  points to the GSEM structure
*     OptLen     -  used to return the length of the option value (in
*                   case the option is of 'STRING' type)
*  Return Value  :  returns the option data address
*********************************************************************/
VOID               *
LCPReturnLQRPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    PPP_UNUSED (OptLen);
    PPP_TRC1 (CONTROL_PLANE, "\t\t\tLQR     : %x",
              pGSEM->pIf->pLqmPtr->Protocol);
    return (&pGSEM->pIf->pLqmPtr->Protocol);
}

/*********************************************************************
*  Function Name : LCPReturnFCSPtr
*  Description   :
*     This function returns the address of the location where the FCS 
*  value to be included in the CONF_REQ, is stored
*  Input         :
*     pGSEM      -  points to the GSEM structure
*     OptLen     -  used to return the length of the option value (in
*                   case the option is of 'STRING' type)
*  Return Value  :  returns the option data address
*********************************************************************/
VOID               *
LCPReturnFCSPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    PPP_UNUSED (OptLen);
    PPP_TRC1 (CONTROL_PLANE, "\t\t\tFCS     : %x",
              pGSEM->pIf->LcpOptionsAckedByPeer.FCSSize);
    return (&pGSEM->pIf->LcpOptionsAckedByPeer.FCSSize);
}

/*********************************************************************
*  Function Name : LCPReturnCallBackPtr
*  Description   :
*     This function returns the address of the location where the CallBack 
*  value to be included in the CONF_REQ, is stored
*  Input         :
*     pGSEM      -  points to the GSEM structure
*     OptLen     -  used to return the length of the option value (in
*                   case the option is of 'STRING' type)
*  Return Value  :  returns the option data address
*********************************************************************/
VOID               *
LCPReturnCallBackPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    PPP_UNUSED (OptLen);
    PPP_TRC (CONTROL_PLANE, "\t\t\tCALLBACK");
    return (&pGSEM->pIf->LcpOptionsAckedByPeer.LCPCallBackInfo.
            CallBackOperation);
}

/***********************************************************************
    OPTION SPECIFIC FUNCTIONS FOR HANDLING CONF_REJ
************************************************************************/
/*********************************************************************
*  Function Name : LCPProcessMRUConfRej
*  Description   :
*  This function sets the value of the rejected option to the default value.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*  Return Value  :  OK
*********************************************************************/
INT1
LCPProcessMRUConfRej (tGSEM * pGSEM)
{
    PPP_TRC (CONTROL_PLANE, "\t\t\tMRU");
    pGSEM->pIf->LcpOptionsAckedByPeer.MRU = DEF_MRU_SIZE;
    return (OK);
}

/*********************************************************************
*  Function Name : LCPProcessPaddingConfRej
*  Description   :
*  This function sets the value of the rejected option to the default value.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*  Return Value  :  OK
*********************************************************************/
INT1
LCPProcessPaddingConfRej (tGSEM * pGSEM)
{
    PPP_TRC (CONTROL_PLANE, "\t\t\tPADDING");
    pGSEM->pIf->LcpOptionsAckedByPeer.MaxPadVal = DEF_PAD_VALUE;
    return (OK);
}

/*********************************************************************
*  Function Name : LCPProcessACCMConfRej
*  Description   :
*  This function sets the value of the rejected option to the default value.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*  Return Value  :  OK
*********************************************************************/
INT1
LCPProcessACCMConfRej (tGSEM * pGSEM)
{
    PPP_TRC (CONTROL_PLANE, "\t\t\tACCM");
    pGSEM->pIf->LcpOptionsAckedByPeer.AsyncMap[0] = 0xff;
    pGSEM->pIf->LcpOptionsAckedByPeer.AsyncMap[1] = 0xff;
    pGSEM->pIf->LcpOptionsAckedByPeer.AsyncMap[2] = 0xff;
    pGSEM->pIf->LcpOptionsAckedByPeer.AsyncMap[3] = 0xff;
    return (OK);
}

/*********************************************************************
*  Function Name : LCPProcessFCSConfRej
*  Description   :
*  This function sets the value of the rejected option to the default value.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*  Return Value  :  OK
*********************************************************************/
INT1
LCPProcessFCSConfRej (tGSEM * pGSEM)
{
    PPP_TRC (CONTROL_PLANE, "\t\t\tFCS");
    pGSEM->pIf->LcpOptionsAckedByPeer.FCSSize = FCS_16_REQ;
    return (OK);
}

#ifdef BAP
/*********************************************************************
*    Function Name        :    LCPProcessLinkDiscrConfReq 
*    Description            :
*            This function is invoked to process the LinkDiscriminator option 
*    present in the CONF_REQ and to construct appropriate response. 
*    Parameter(s)        :
*                 pGSEM    -    Pointer to the GSEM structure corresponding to the
*                           packet.
*                 pInPkt    -    Points to the incoming packet buffer.
*                tOptVal    -    Option Value received  to be processed.
*                Offset    -    Indicates the place in the outgoing buffer at which
*                            the option value is appended.
*        PresenceFlag    -    Indicates whether the option is present in the recd.
*                            CONF_REQ packet.
*        Return Value    :    Returns ACK if value is agreeably / the number of 
*                            bytes appended in the outgoing NAK buffer otherwise.
*********************************************************************/
INT1
LCPProcessLinkDiscrConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal,
                            UINT2 Offset, UINT1 PresenceFlag)
{
    tPPPIf             *pBundleIf;

    PPP_TRC1 (CONTROL_PLANE, "\t\t\tLINK DISCR: %x", OptVal.ShortVal);
    pBundleIf = pGSEM->pIf->MPInfo.MemberInfo.pBundlePtr;
    PPP_UNUSED (pInPkt);
    PPP_UNUSED (PresenceFlag);
    if (PresenceFlag == NOT_SET)
    {
        OptVal.ShortVal =
            ((tBAPInfo
              *) (pBundleIf->MPInfo.BundleInfo.BWMProtocolInfo.pProtocolInfo))->
            NextNakLinkDiscrValue--;
        ASSIGN2BYTE (pGSEM->pOutParam, Offset, OptVal.ShortVal);
        return (BYTE_LEN_2);
    }

    pGSEM->pIf->LcpOptionsAckedByLocal.LinkDiscriminator = OptVal.ShortVal;

    return (ACK);
}

/*********************************************************************
*  Function Name : LCPProcessLinkDiscrConfNak 
*  Description   :
*        This function is invoked to process the LinkDiscriminator option 
*    present in the CONF_NAK and to construct appropriate response. 
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*  Return Value  :  OK/NOT_OK 
*********************************************************************/
INT1
LCPProcessLinkDiscrConfNak (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal)
{

    PPP_UNUSED (pGSEM);
    PPP_UNUSED (pInPkt);
    PPP_TRC1 (CONTROL_PLANE, "\t\t\tLINK DISCR: %x", OptVal.ShortVal);
    return (OK);
}

/*********************************************************************
*  Function Name : LCPReturnLinkDiscrPtr
*  Description   :
*     This function returns the address of the location where the 
*  LinkDiscriminator value to be included in the CONF_REQ, is stored
*  Input         :
*     pGSEM      -  points to the GSEM structure
*     OptLen     -  used to return the length of the option value (in
*                   case the option is of 'STRING' type)
*  Return Value  :  returns the option data address
*********************************************************************/
VOID               *
LCPReturnLinkDiscrPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    tBAPInfo           *pBAPInfo;

    PPP_UNUSED (OptLen);
    PPP_TRC1 (CONTROL_PLANE, "\t\t\tLINK DISCR: %x",
              pGSEM->pIf->LcpOptionsAckedByPeer.LinkDiscriminator);
    pBAPInfo =
        (tBAPInfo *) pGSEM->pIf->MPInfo.MemberInfo.pBundlePtr->MPInfo.
        BundleInfo.BWMProtocolInfo.pProtocolInfo;

    pGSEM->pIf->LcpOptionsAckedByPeer.LinkDiscriminator =
        pBAPInfo->LocalNextLinkDiscrValue++;

    return (&pGSEM->pIf->LcpOptionsAckedByPeer.LinkDiscriminator);
}
#endif

/*********************************************************************
*  Function Name : LCPProcessInternationConfReq 
*  Description   :
*        This function is invoked to process the AUTH option present
*  in the CONF_REQ and to construct appropriate response 
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*     Offset     -  indicates the place in the outgoing buffer at which 
*                   the option value is appended
*  PresenceFlag  -  indicates whether the option is present in the recd.
*                   CONF_REQ packet
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer 
*********************************************************************/
INT1
LCPProcessInternationConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt,
                              tOptVal OptVal, UINT2 Offset, UINT1 PresenceFlag)
{
    tPPPIf             *pIf = NULL;
    INT1                i1RetVal = 0;
    UINT1               u1Index;
    i1RetVal = ACK;
    pIf = pGSEM->pIf;

    PPP_UNUSED (pInPkt);
    PPP_UNUSED (PresenceFlag);
    for (u1Index = 0; u1Index < gLangTable.NumActiveLang; u1Index++)
    {
        if (OptVal.LongVal == gLangTable.LangList[u1Index].CharSet)
        {
            pIf->LcpOptionsAckedByLocal.LCPInterOptionInfo.CharSet =
                gLangTable.LangList[u1Index].CharSet;
            MEMCPY (pIf->LcpOptionsAckedByLocal.LCPInterOptionInfo.LangTag,
                    gLangTable.LangList[u1Index].LangTag,
                    gLangTable.LangList[u1Index].LangTagLen);

            pIf->LcpOptionsAckedByLocal.LCPInterOptionInfo.LangTagLen =
                gLangTable.LangList[u1Index].LangTagLen;

            return (i1RetVal);
        }
    }

    ASSIGN4BYTE (pGSEM->pOutParam, Offset, CHAR_SET_UTF8);
    ASSIGNSTR (pGSEM->pOutParam, gLangTable.LangList[u1Index - 1].LangTag,
               Offset + 4, gLangTable.LangList[u1Index - 1].LangTagLen);
    i1RetVal =
        (INT1) (BYTE_LEN_4 + gLangTable.LangList[u1Index - 1].LangTagLen);
    return (i1RetVal);
}

/*********************************************************************
*  Function Name : LCPProcessAUTHConfNak 
*  Description   :
*        This function is invoked to process the AUTH option present
*  in the CONF_NAK and to construct appropriate response 
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*  Return Value  :  OK/NOT_OK 
*********************************************************************/
INT1
LCPProcessInternationConfNak (tGSEM * pGSEM, t_MSG_DESC * pInPkt,
                              tOptVal OptVal)
{

    tPPPIf             *pIf = NULL;
    UINT1               u1Index = 0;

    PPP_UNUSED (pInPkt);
    pIf = pGSEM->pIf;
    PPP_UNUSED (pInPkt);
    PPP_UNUSED (OptVal);
    if (OptVal.LongVal == CHAR_SET_UTF8)
    {
        pIf->LcpOptionsAckedByPeer.LCPInterOptionInfo.CharSet = CHAR_SET_UTF8;
        MEMCPY (pIf->LcpOptionsAckedByPeer.LCPInterOptionInfo.LangTag,
                "i-default", 9);
        pIf->LcpOptionsAckedByPeer.LCPInterOptionInfo.LangTagLen = 9;
        return (OK);
    }

    pIf->LastTriedLangIndex++;

    if (pGSEM->pNegFlagsPerIf[INTERNATION_OPTION].FlagMask & ACKED_BY_PEER_MASK)
    {
        if (pIf->LastTriedLangIndex >= gLangTable.NumActiveLang)
        {
            return (CLOSE);
        }

        u1Index = pIf->LastTriedLangIndex;

        pIf->LcpOptionsAckedByPeer.LCPInterOptionInfo.CharSet =
            gLangTable.LangList[u1Index].CharSet;
        MEMCPY (pIf->LcpOptionsAckedByPeer.LCPInterOptionInfo.LangTag,
                gLangTable.LangList[u1Index].LangTag,
                gLangTable.LangList[u1Index].LangTagLen);

        pIf->LcpOptionsAckedByPeer.LCPInterOptionInfo.LangTagLen =
            gLangTable.LangList[u1Index].LangTagLen;

        return (OK);
    }
    return (CLOSE);
}

/*********************************************************************
*  Function Name : LCPReturnAUTHPtr
*  Description   :
*     This function returns the address of the location where the AUTH 
*  value to be included in the CONF_REQ, is stored
*  Input         :
*     pGSEM      -  points to the GSEM structure
*     OptLen     -  used to return the length of the option value (in
*                   case the option is of 'STRING' type)
*  Return Value  :  returns the option data address
*********************************************************************/
VOID               *
LCPReturnInternationPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    UINT1               u1LastTriedIndex;

    PPP_UNUSED (OptLen);
    u1LastTriedIndex = pGSEM->pIf->LastTriedLangIndex;

    if (u1LastTriedIndex < gLangTable.NumActiveLang)
    {

        return (&gLangTable.LangList[u1LastTriedIndex].CharSet);
    }
    return (VOID *) NULL;
}

INT1
LCPAddInternationSubOpt (tGSEM * pGSEM, tOptVal OptVal, t_MSG_DESC * pOutBuf)
{
    UINT1               u1Index;

    PPP_UNUSED (pGSEM);
    for (u1Index = 0; u1Index < gLangTable.NumActiveLang; u1Index++)
    {

        if (OptVal.LongVal == gLangTable.LangList[u1Index].CharSet)
        {
            APPENDSTR (pOutBuf, gLangTable.LangList[u1Index].LangTag,
                       gLangTable.LangList[u1Index].LangTagLen);
            return ((INT1) gLangTable.LangList[u1Index].LangTagLen);
        }

    }

    return (BYTE_LEN_0);
}

/* Keepalive time out processing functions */
VOID
LCPProcessKeepAliveTimeOut (VOID *pPppIf)
{
    tPPPIf             *pIf = NULL;
    UINT1               u1val;
    UINT4               u4TempVal;

    pIf = (tPPPIf *) pPppIf;

    if (pIf->LcpStatus.LinkAvailability != LINK_NOT_AVAILABLE)
    {

        pIf->EchoInfo.EchoId++;

        u4TempVal = pIf->LcpOptionsAckedByLocal.MRU;
        pIf->LcpOptionsAckedByLocal.MRU = KEEPALIVE_ECHO_PKTSIZE;

        LCPSendRXRPkt (pIf, ECHO_REQ_CODE, pIf->EchoInfo.EchoId);
        pIf->LcpOptionsAckedByLocal.MRU = (UINT2) u4TempVal;

        /* Start timer  again */
        pIf->EchoInfo.EchoTimer.Param1 = PTR_TO_U4 (pIf);

        (pIf->LinkInfo.TimeOutTime >= pIf->EchoInfo.KeepAliveTimeOutVal) ?
            (u1val = (UINT1) (pIf->EchoInfo.KeepAliveTimeOutVal - 1)) :
            (u1val = (UINT1) (pIf->LinkInfo.TimeOutTime));

        pIf->EchoInfo.KeepAliveTimer.Param1 = PTR_TO_U4 (pIf);

        if (!u1val)
        {
            PPPRestartTimer (&pIf->EchoInfo.EchoTimer, ECHO_REQ_TIMER, 1);
            PPPRestartTimer (&pIf->EchoInfo.KeepAliveTimer, KEEP_ALIVE_TIMER,
                             (UINT4) (pIf->EchoInfo.KeepAliveTimeOutVal + 1));
        }
        else
        {
            PPPRestartTimer (&pIf->EchoInfo.EchoTimer, ECHO_REQ_TIMER, u1val);
            PPPRestartTimer (&pIf->EchoInfo.KeepAliveTimer, KEEP_ALIVE_TIMER,
                             pIf->EchoInfo.KeepAliveTimeOutVal);
        }
    }
}
