/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppll.c,v 1.5 2014/12/16 10:49:12 siva Exp $
 *
 * Description: Low Level Routines for IfTable.
 *
 *******************************************************************/
#include "pppsnmpm.h"
#include "lcpcom.h"
#include "snmphdrs.h"
#include "llproto.h"
#include "pppexlow.h"
#include "ppipcpcli.h"

UINT1               IpInstance;
UINT1               Ip6Instance;
UINT1               MuxcpInstance;
UINT1               MplsInstance;
UINT1               IpxInstance;
UINT1               MpInstance;
UINT1               CCPInstance;
UINT1               CCPTxInstance;
UINT1               CCPRxInstance;
UINT1               ECPInstance;
UINT1               ECPTxInstance;
UINT1               ECPRxInstance;
UINT1               BcpInstance;
UINT1               BcpMediaInstance;
UINT1               BACPInstance;
UINT1               RMInstance;
UINT1               LqrInstance;
UINT1               SecurityInstance;
UINT1               SecretsInstance;
UINT1               LinkInstance;

/****************************************************************************
 Function    :  nmhGetFirstIndex
 Input       :  The Indices
                pIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_GET_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndex (INT4 *pIndex, UINT1 InstanceType, INT1 InstCode)
{

    return (getInstanceForOneIndex (pIndex, InstanceType, InstCode));

}

/****************************************************************************
 Function    :  nmhGetNextIndex
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_GET_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndex (INT4 *pIndex, UINT1 InstanceType, INT1 InstCode)
{
    return (getInstanceForOneIndex (pIndex, InstanceType, InstCode));

}

/****************************************************************************
 Function    :  nmhGetFirstDoubleIndex
 Input       :  The Indices
                PppSecurityConfigLink
                PppSecurityConfigPreference
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_GET_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstDoubleIndex (INT4 *pIndex1, INT4 *pIndex2, INT1 InstanceType,
                        INT1 InstCode)
{
    return (getInstanceForTwoIndices
            (pIndex1, pIndex2, (UINT1) InstanceType, InstCode));
}

INT1
nmhGetNextDoubleIndex (INT4 *pIndex1, INT4 *pIndex2, INT1 InstanceType,
                       INT1 InstCode)
{
    return (getInstanceForTwoIndices
            (pIndex1, pIndex2, (UINT1) InstanceType, InstCode));
}

VOID
SNMPInitInstance ()
{
    UINT1               Instance;

    Instance = 0;
    IpInstance = Instance++;
    Ip6Instance = Instance++;
    MuxcpInstance = Instance++;
    MplsInstance = Instance++;
    IpxInstance = Instance++;
#ifdef MP
    MpInstance = Instance++;
#endif
#ifdef CCP
    CCPInstance = Instance++;
    ECPInstance = Instance++;
#endif
#ifdef BCP
    BcpInstance = Instance++;
#endif 

#ifdef BAP
    BACPInstance = Instance++;
    RMInstance = Instance++;
#endif

#ifdef LQM
    LqrInstance = Instance++;
#endif

    LinkInstance = Instance++;
    /* index intialization for 2 indices 
     *
     */
    Instance = 0;

#ifdef CCP
    CCPTxInstance = Instance++;
    CCPRxInstance = Instance++;
    ECPTxInstance = Instance++;
    ECPRxInstance = Instance++;
#endif

#ifdef BCP
    BcpMediaInstance = Instance++;
#endif
    SecurityInstance = Instance++;
    SecretsInstance = Instance++;

}

/*
 * Low level routines for pppgen.mib
 */

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppDebugLevelMask
 Input       :  The Indices

                The Object 
                retValPppDebugLevelMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppDebugLevelMask (INT4 *pi4RetValPppDebugLevelMask)
{
    *pi4RetValPppDebugLevelMask = (INT4) DesiredLevel;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetPppDebugLevelMask
 Input       :  The Indices

                The Object 
                setValPppDebugLevelMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppDebugLevelMask (INT4 i4SetValPppDebugLevelMask)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    DesiredLevel = (UINT4) i4SetValPppDebugLevelMask;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppDebugLevelMask, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 0, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                  i4SetValPppDebugLevelMask));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PppDebugLevelMask
 Input       :  The Indices

                The Object 
                testValPppDebugLevelMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppDebugLevelMask (UINT4 *pu4ErrorCode,
                            INT4 i4TestValPppDebugLevelMask)
{
#ifdef DEBUG_WANTED
    if (DEBUG_TRC == i4TestValPppDebugLevelMask)
    {
        return SNMP_SUCCESS;
    }
#endif
    if ((i4TestValPppDebugLevelMask < 0) || (i4TestValPppDebugLevelMask > 2047))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2PppExtIPAddressPoolSelector
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PppExtIPAddressPoolSelector (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2PppDebugLevelMask
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PppDebugLevelMask (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

#if 1                            /* DSL_ADD */
/****************************************************************************
 Function    :  nmhGetPppaaaMethod
 Input       :  The Indices

                The Object 
                retValPppaaaMethod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppaaaMethod (INT4 *pi4RetValPppaaaMethod)
{
    *pi4RetValPppaaaMethod = gu1aaaMethod;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetPppaaaMethod
 Input       :  The Indices

                The Object 
                setValPppaaaMethod
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppaaaMethod (INT4 i4SetValPppaaaMethod)
{
    gu1aaaMethod = (UINT1) i4SetValPppaaaMethod;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2PppaaaMethod
 Input       :  The Indices

                The Object 
                testValPppaaaMethod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppaaaMethod (UINT4 *pu4ErrorCode, INT4 i4TestValPppaaaMethod)
{
    if ((i4TestValPppaaaMethod != PPP_LOCAL)
        && (i4TestValPppaaaMethod != PPP_RADIUS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

INT1
nmhGetPPPAuthMode (INT4 *pi4RetValAuthMode)
{
    *pi4RetValAuthMode = gu1AuthMode;
    return (SNMP_SUCCESS);
}

INT1
nmhTestv2PPPAuthMode (UINT4 *pu4ErrorCode, INT4 i4TestValAuthMode)
{
    if ((i4TestValAuthMode == 1) || (i4TestValAuthMode == 2))
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        PPP_TRC (MGMT, "Wrong Value");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

}

INT1
nmhSetPPPAuthMode (INT4 u1SetValAuthMode)
{
    gu1AuthMode = (UINT1) u1SetValAuthMode;
    return (SNMP_SUCCESS);
}

#endif /* DSL_ADD */
