/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ppplnktl.c,v 1.5 2014/10/17 15:02:46 siva Exp $
 *
 * Description:This file contains low level routines of PPP module.
 *
 *******************************************************************/
#include "pppsnmpm.h"
#include "lcpcom.h"
#include "snmccons.h"
#include "gsemdefs.h"
#include "mpdefs.h"
#include "llproto.h"
#include "globexts.h"
#include "ppteslow.h"

INT4                PacketType;
INT4                PacketLength;

/* LOW LEVEL Routines for Table : PppTestConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppTestConfigTable
 Input       :  The Indices
                PppTestConfigIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppTestConfigTable (INT4 i4PppTestConfigIfIndex)
{
    if (PppGetIfPtr ((UINT4) i4PppTestConfigIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppTestConfigTable
 Input       :  The Indices
                PppTestConfigIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexPppTestConfigTable (INT4 *pi4PppTestConfigIfIndex)
{
    if (nmhGetFirstIndex (pi4PppTestConfigIfIndex, LinkInstance, FIRST_INST) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppTestConfigTable
 Input       :  The Indices
                PppTestConfigIfIndex
                nextPppTestConfigIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppTestConfigTable (INT4 i4PppTestConfigIfIndex,
                                   INT4 *pi4NextPppTestConfigIfIndex)
{
    if (nmhGetNextIndex (&i4PppTestConfigIfIndex, LinkInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextPppTestConfigIfIndex = i4PppTestConfigIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppTestConfigMTU
 Input       :  The Indices
                PppTestConfigIfIndex

                The Object 
                retValPppTestConfigMTU
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestConfigMTU (INT4 i4PppTestConfigIfIndex,
                        INT4 *pi4RetValPppTestConfigMTU)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetIfPtr ((UINT4) i4PppTestConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValPppTestConfigMTU = pIf->LcpOptionsAllowedForPeer.MRU;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppTestConfigSpeed
 Input       :  The Indices
                PppTestConfigIfIndex

                The Object 
                retValPppTestConfigSpeed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestConfigSpeed (INT4 i4PppTestConfigIfIndex,
                          INT4 *pi4RetValPppTestConfigSpeed)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppTestConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValPppTestConfigSpeed = (INT4) pIf->LinkInfo.IfSpeed;

    return (SNMP_SUCCESS);

}

#ifdef PPP_STACK_WANTED
/****************************************************************************
 Function    :  nmhGetPppExtLinkConfigIdleTimeOut
 Input       :  The Indices
                PppTestConfigIfIndex

                The Object 
                retValPppTestConfigIdleTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtLinkConfigIdleTimeOut (INT4 i4PppTestConfigIfIndex,
                                   INT4 *pi4RetValPppTestConfigIdleTimer)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppTestConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValPppTestConfigIdleTimer = pIf->LinkInfo.IdleTimeOutValue;

    return (SNMP_SUCCESS);

}
#endif /* PPP_STACK_WANTED */
/****************************************************************************
 Function    :  nmhSetPppTestConfigMTU
 Input       :  The Indices
                PppTestConfigIfIndex

                The Object 
                setValPppTestConfigMTU
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestConfigMTU (INT4 i4PppTestConfigIfIndex,
                        INT4 i4SetValPppTestConfigMTU)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetIfPtr ((UINT4) i4PppTestConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pIf->BundleFlag == BUNDLE)
    {
        pIf->MPInfo.BundleInfo.BundleOptAllowedForPeer.MRRU =
            (UINT2) i4SetValPppTestConfigMTU;
        pIf->MPInfo.BundleInfo.MPFlagsPerIf[MRRU_IDX].FlagMask |=
            ALLOWED_FOR_PEER_SET;
    }
    else
    {
        pIf->LcpOptionsAllowedForPeer.MRU = (UINT2) i4SetValPppTestConfigMTU;
        pIf->LcpGSEM.pNegFlagsPerIf[MRU_IDX].FlagMask |= ALLOWED_FOR_PEER_SET;

    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppTestConfigSpeed
 Input       :  The Indices
                PppTestConfigIfIndex

                The Object 
                setValPppTestConfigSpeed
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestConfigSpeed (INT4 i4PppTestConfigIfIndex,
                          INT4 i4SetValPppTestConfigSpeed)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppTestConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIf->LinkInfo.IfSpeed = (UINT4) i4SetValPppTestConfigSpeed;

    LCPEnableCompressions (pIf);

    return (SNMP_SUCCESS);

}

#ifdef PPP_STACK_WANTED
/****************************************************************************
 Function    :  nmhSetPppExtLinkConfigIdleTimeOut
 Input       :  The Indices
                PppTestConfigIfIndex

                The Object 
                setValPppTestConfigIdleTimer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtLinkConfigIdleTimeOut (INT4 i4PppTestConfigIfIndex,
                                   INT4 i4SetValPppTestConfigIdleTimer)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppTestConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIf->LinkInfo.IdleTimeOutValue = (UINT2) i4SetValPppTestConfigIdleTimer;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppKeepAliveTimeOutVal
 Input       :  The Indices
                PppTestConfigIfIndex

                The Object 
                retValPppKeepAliveTimeOutVal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppKeepAliveTimeOutVal (INT4 i4PppTestConfigIfIndex,
                              INT4 *pi4RetValPppKeepAliveTimeOutVal)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppTestConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppKeepAliveTimeOutVal = pIf->EchoInfo.KeepAliveTimeOutVal;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppKeepAliveTimeOutVal
 Input       :  The Indices
                PppTestConfigIfIndex

                The Object 
                setValPppKeepAliveTimeOutVal
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppKeepAliveTimeOutVal (INT4 i4PppTestConfigIfIndex,
                              INT4 i4SetValPppKeepAliveTimeOutVal)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppTestConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pIf->LcpStatus.OperStatus == STATUS_DOWN)
    {
        pIf->EchoInfo.KeepAliveTimeOutVal =
            (UINT2) i4SetValPppKeepAliveTimeOutVal;
        return (SNMP_SUCCESS);
    }

    if (i4SetValPppKeepAliveTimeOutVal != 0)
    {
        pIf->EchoInfo.KeepAliveTimeOutVal =
            (UINT2) i4SetValPppKeepAliveTimeOutVal;
        LCPProcessKeepAliveTimeOut (pIf);
    }
    else
    {
        pIf->EchoInfo.KeepAliveTimeOutVal = 0;
        PPPStopTimer (&pIf->EchoInfo.KeepAliveTimer);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetPppExtLinkConfigIdentification
 Input       :  The Indices
                IfIndex

                The Object 
                setValPppExtLinkConfigIdentification
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtLinkConfigIdentification (INT4 i4IfIndex,
                                      INT4
                                      i4SetValPppExtLinkConfigIdentification)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (i4SetValPppExtLinkConfigIdentification == ENABLE)
    {
        LCPSendIdPkt (pIf);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppExtLinkConfigTRVal
 Input       :  The Indices
                IfIndex

                The Object 
                setValPppExtLinkConfigTRVal
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtLinkConfigTRVal (INT4 i4IfIndex, INT4 i4SetValPppExtLinkConfigTRVal)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIf->LinkInfo.u4TRSessionActiveTime = (UINT4) i4SetValPppExtLinkConfigTRVal;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetPppExtLinkConfigTROption
 Input       :  The Indices
                IfIndex

                The Object 
                setValPppExtLinkConfigTROption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtLinkConfigTROption (INT4 i4IfIndex, INT4
                                i4SetValPppExtLinkConfigTROption)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIf->LinkInfo.bTROption = (BOOLEAN) i4SetValPppExtLinkConfigTROption;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPppExtLinkConfigTROption
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppExtLinkConfigTROption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtLinkConfigTROption (INT4 i4IfIndex, INT4
                                *pi4RetValPppExtLinkConfigTROption)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppExtLinkConfigTROption = (INT4) pIf->LinkInfo.bTROption;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPppExtLinkConfigTRVal
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppExtLinkConfigTRVal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtLinkConfigTRVal (INT4 i4IfIndex, INT4
                             *pi4RetValPppExtLinkConfigTRVal)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppExtLinkConfigTRVal = (INT4) pIf->LinkInfo.
        u4TRSessionActiveTime;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppExtLinkConfigTROption
 Input       :  The Indices
                IfIndex

                The Object 
                testValPppExtLinkConfigTROption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtLinkConfigTROption (UINT4 *pu4ErrorCode,
                                   INT4 i4IfIndex,
                                   INT4 i4TestValPppExtLinkConfigTROption)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
    if (i4TestValPppExtLinkConfigTROption != ENABLE
        && i4TestValPppExtLinkConfigTROption != DISABLE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppExtLinkConfigTRVal
 Input       :  The Indices
                IfIndex

                The Object 
                testValPppExtLinkConfigTRVal
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtLinkConfigTRVal (UINT4 *pu4ErrorCode,
                                INT4 i4IfIndex, INT4
                                i4TestValPppExtLinkConfigTRVal)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
    if (i4TestValPppExtLinkConfigTRVal <= PPP_THRESH_FOR_TR)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}
#endif /* PPP_STACK_WANTED */

/****************************************************************************
 Function    :  nmhGetPppExtKeepAliveTimeOut
 Input       :  The Indices
                PppTestConfigIfIndex

                The Object 
                retValPppExtKeepAliveTimeOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtKeepAliveTimeOut (INT4 i4PppTestConfigIfIndex,
                              INT4 *pi4RetValPppExtKeepAliveTimeOut)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppTestConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppExtKeepAliveTimeOut = pIf->EchoInfo.KeepAliveTimeOutVal;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppExtKeepAliveTimeOut
 Input       :  The Indices
                PppTestConfigIfIndex

                The Object 
                setValPppExtKeepAliveTimeOut
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtKeepAliveTimeOut (INT4 i4PppTestConfigIfIndex,
                              INT4 i4SetValPppExtKeepAliveTimeOut)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppTestConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pIf->LcpStatus.OperStatus == STATUS_DOWN)
    {
        pIf->EchoInfo.KeepAliveTimeOutVal =
            (UINT2) i4SetValPppExtKeepAliveTimeOut;
        return (SNMP_SUCCESS);
    }

    if (i4SetValPppExtKeepAliveTimeOut != 0)
    {
        pIf->EchoInfo.KeepAliveTimeOutVal =
            (UINT2) i4SetValPppExtKeepAliveTimeOut;
        LCPProcessKeepAliveTimeOut (pIf);
        PPPRestartTimer (&pIf->LinkInfo.IdleTimer, LCP_IDLE_TIMER,
                         (UINT4) (3 * pIf->EchoInfo.KeepAliveTimeOutVal));
    }
    else
    {
        pIf->EchoInfo.KeepAliveTimeOutVal = PPP_DEF_KEEPALIVE_TIME;
        LCPProcessKeepAliveTimeOut (pIf);
        PPPRestartTimer (&pIf->LinkInfo.IdleTimer, LCP_IDLE_TIMER,
                         (UINT4) (3 * pIf->EchoInfo.KeepAliveTimeOutVal));
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetPppTestConfigDiscardTest
 Input       :  The Indices
                PppTestConfigIfIndex

                The Object 
                setValPppTestConfigDiscardTest
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestConfigDiscardTest (INT4 i4PppTestConfigIfIndex,
                                INT4 i4SetValPppTestConfigDiscardTest)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppTestConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (i4SetValPppTestConfigDiscardTest == ENABLE)
    {
        LCPSendDiscardReq (pIf);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppTestConfigIdentification
 Input       :  The Indices
                PppTestConfigIfIndex

                The Object 
                setValPppTestConfigIdentification
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppTestConfigIdentification (INT4 i4PppTestConfigIfIndex,
                                   INT4 i4SetValPppTestConfigIdentification)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppTestConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (i4SetValPppTestConfigIdentification == ENABLE)
    {
        LCPSendIdPkt (pIf);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppExtLinkConfigTxACFC
 Input       :  The Indices
                PppTestLinkConfigIfIndex

                The Object 
                retValPppTestLinkConfigTxACFC
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtLinkConfigTxACFC (INT4 i4PppTestLinkConfigIfIndex,
                              INT4 *pi4RetValPppTestLinkConfigTxACFC)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppTestLinkConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValPppTestLinkConfigTxACFC =
        (pIf->LcpGSEM.pNegFlagsPerIf[ACFC_IDX].
         FlagMask & ALLOWED_FOR_PEER_MASK) ? 2 : 1;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppExtLinkConfigRxACFC
 Input       :  The Indices
                PppTestLinkConfigIfIndex

                The Object 
                retValPppTestLinkConfigRxACFC
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtLinkConfigRxACFC (INT4 i4PppTestLinkConfigIfIndex,
                              INT4 *pi4RetValPppTestLinkConfigRxACFC)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppTestLinkConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValPppTestLinkConfigRxACFC =
        (pIf->LcpGSEM.pNegFlagsPerIf[ACFC_IDX].FlagMask & DESIRED_MASK) ? 2 : 1;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppExtLinkConfigTxPFC
 Input       :  The Indices
                PppTestLinkConfigIfIndex

                The Object 
                retValPppTestLinkConfigTxPFC
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtLinkConfigTxPFC (INT4 i4PppTestLinkConfigIfIndex,
                             INT4 *pi4RetValPppTestLinkConfigTxPFC)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppTestLinkConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValPppTestLinkConfigTxPFC =
        (pIf->LcpGSEM.pNegFlagsPerIf[PFC_IDX].
         FlagMask & ALLOWED_FOR_PEER_MASK) ? 2 : 1;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppExtLinkConfigRxPFC
 Input       :  The Indices
                PppTestLinkConfigIfIndex

                The Object 
                retValPppTestLinkConfigRxPFC
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtLinkConfigRxPFC (INT4 i4PppTestLinkConfigIfIndex,
                             INT4 *pi4RetValPppTestLinkConfigRxPFC)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppTestLinkConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValPppTestLinkConfigRxPFC =
        (pIf->LcpGSEM.pNegFlagsPerIf[PFC_IDX].FlagMask & DESIRED_MASK) ? 2 : 1;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppExtLinkConfigTxACFC
 Input       :  The Indices
                PppTestLinkConfigIfIndex

                The Object 
                setValPppTestLinkConfigTxACFC
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtLinkConfigTxACFC (INT4 i4PppTestLinkConfigIfIndex,
                              INT4 i4SetValPppTestLinkConfigTxACFC)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppTestLinkConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (i4SetValPppTestLinkConfigTxACFC == ENABLE)
    {
        pIf->LcpGSEM.pNegFlagsPerIf[ACFC_IDX].FlagMask |= ALLOWED_FOR_PEER_SET;
    }
    else
    {
        pIf->LcpGSEM.pNegFlagsPerIf[ACFC_IDX].FlagMask &=
            ALLOWED_FOR_PEER_NOT_SET;
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppExtLinkConfigRxACFC
 Input       :  The Indices
                PppTestLinkConfigIfIndex

                The Object 
                setValPppTestLinkConfigRxACFC
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtLinkConfigRxACFC (INT4 i4PppTestLinkConfigIfIndex,
                              INT4 i4SetValPppTestLinkConfigRxACFC)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppTestLinkConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (i4SetValPppTestLinkConfigRxACFC == ENABLE)
    {
        pIf->LcpGSEM.pNegFlagsPerIf[ACFC_IDX].FlagMask |= DESIRED_SET;
    }
    else
    {
        pIf->LcpGSEM.pNegFlagsPerIf[ACFC_IDX].FlagMask &= DESIRED_NOT_SET;
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppExtLinkConfigTxPFC
 Input       :  The Indices
                PppTestLinkConfigIfIndex

                The Object 
                setValPppTestLinkConfigTxPFC
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtLinkConfigTxPFC (INT4 i4PppTestLinkConfigIfIndex,
                             INT4 i4SetValPppTestLinkConfigTxPFC)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppTestLinkConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (i4SetValPppTestLinkConfigTxPFC == ENABLE)
    {
        pIf->LcpGSEM.pNegFlagsPerIf[PFC_IDX].FlagMask |= ALLOWED_FOR_PEER_SET;
    }
    else
    {
        pIf->LcpGSEM.pNegFlagsPerIf[PFC_IDX].FlagMask &=
            ALLOWED_FOR_PEER_NOT_SET;
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppExtLinkConfigRxPFC
 Input       :  The Indices
                PppTestLinkConfigIfIndex

                The Object 
                setValPppTestLinkConfigRxPFC
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtLinkConfigRxPFC (INT4 i4PppTestLinkConfigIfIndex,
                             INT4 i4SetValPppTestLinkConfigRxPFC)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppTestLinkConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (i4SetValPppTestLinkConfigRxPFC == ENABLE)
    {
        pIf->LcpGSEM.pNegFlagsPerIf[PFC_IDX].FlagMask |= DESIRED_SET;
    }
    else
    {
        pIf->LcpGSEM.pNegFlagsPerIf[PFC_IDX].FlagMask &= DESIRED_NOT_SET;
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppTestConfigMTU
 Input       :  The Indices
                PppTestConfigIfIndex

                The Object 
                testValPppTestConfigMTU
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestConfigMTU (UINT4 *pu4ErrorCode, INT4 i4PppTestConfigIfIndex,
                           INT4 i4TestValPppTestConfigMTU)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetIfPtr ((UINT4) i4PppTestConfigIfIndex)) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        PPP_UNUSED (pIf);
        return (SNMP_FAILURE);
    }

    if ((i4TestValPppTestConfigMTU > 1500)
        && (i4TestValPppTestConfigMTU < MAX_INTEGER))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2PppTestConfigSpeed
 Input       :  The Indices
                PppTestConfigIfIndex

                The Object 
                testValPppTestConfigSpeed
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestConfigSpeed (UINT4 *pu4ErrorCode, INT4 i4PppTestConfigIfIndex,
                             INT4 i4TestValPppTestConfigSpeed)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetIfPtr ((UINT4) i4PppTestConfigIfIndex)) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        PPP_UNUSED (pIf);
        return (SNMP_FAILURE);
    }

    if ((i4TestValPppTestConfigSpeed > 1
         && i4TestValPppTestConfigSpeed < MAX_INTEGER))
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

#ifdef PPP_STACK_WANTED
/****************************************************************************
 Function    :  nmhTestv2PppExtLinkConfigIdleTimeOut
 Input       :  The Indices
                PppTestConfigIfIndex

                The Object 
                testValPppTestConfigIdleTimer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtLinkConfigIdleTimeOut (UINT4 *pu4ErrorCode,
                                      INT4 i4PppTestConfigIfIndex,
                                      INT4 i4TestValPppTestConfigIdleTimer)
{

    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetIfPtr ((UINT4) i4PppTestConfigIfIndex)) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppTestConfigIdleTimer < 1
        || i4TestValPppTestConfigIdleTimer > MAX_INTEGER)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppKeepAliveTimeOutVal
 Input       :  The Indices
                PppTestConfigIfIndex

                The Object 
                testValPppKeepAliveTimeOutVal
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppKeepAliveTimeOutVal (UINT4 *pu4ErrorCode,
                                 INT4 i4PppTestConfigIfIndex,
                                 INT4 i4TestValPppKeepAliveTimeOutVal)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetIfPtr ((UINT4) i4PppTestConfigIfIndex)) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppKeepAliveTimeOutVal < 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}
#endif /* PPP_STACK_WANTED */
/****************************************************************************
 Function    :  nmhTestv2PppExtKeepAliveTimeOut
 Input       :  The Indices
                PppTestConfigIfIndex

                The Object 
                testValPppExtKeepAliveTimeOut
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtKeepAliveTimeOut (UINT4 *pu4ErrorCode,
                                 INT4 i4PppTestConfigIfIndex,
                                 INT4 i4TestValPppExtKeepAliveTimeOut)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetIfPtr ((UINT4) i4PppTestConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        PPP_UNUSED (pIf);
        return (SNMP_FAILURE);
    }

    if ((i4TestValPppExtKeepAliveTimeOut < 0)
        || (i4TestValPppExtKeepAliveTimeOut > 600))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppTestConfigDiscardTest
 Input       :  The Indices
                PppTestConfigIfIndex

                The Object 
                testValPppTestConfigDiscardTest
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestConfigDiscardTest (UINT4 *pu4ErrorCode,
                                   INT4 i4PppTestConfigIfIndex,
                                   INT4 i4TestValPppTestConfigDiscardTest)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetIfPtr ((UINT4) i4PppTestConfigIfIndex)) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        PPP_UNUSED (pIf);
        return (SNMP_FAILURE);
    }

    if (i4TestValPppTestConfigDiscardTest != ENABLE
        && i4TestValPppTestConfigDiscardTest != DISABLE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

#ifdef PPP_STACK_WANTED
/****************************************************************************
 Function    :  nmhTestv2PppExtLinkConfigIdentification
 Input       :  The Indices
                IfIndex

                The Object 
                testValPppExtLinkConfigIdentification
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtLinkConfigIdentification (UINT4 *pu4ErrorCode,
                                         INT4 i4IfIndex,
                                         INT4
                                         i4TestValPppExtLinkConfigIdentification)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetIfPtr ((UINT4) i4IfIndex)) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppExtLinkConfigIdentification != ENABLE
        && i4TestValPppExtLinkConfigIdentification != DISABLE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}
#endif /* PPP_STACK_WANTED */

/****************************************************************************
 Function    :  nmhTestv2PppTestConfigIdentification
 Input       :  The Indices
                PppTestConfigIfIndex

                The Object 
                testValPppTestConfigIdentification
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppTestConfigIdentification (UINT4 *pu4ErrorCode,
                                      INT4 i4PppTestConfigIfIndex,
                                      INT4 i4TestValPppTestConfigIdentification)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetIfPtr ((UINT4) i4PppTestConfigIfIndex)) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        PPP_UNUSED (pIf);
        return (SNMP_FAILURE);
    }

    if (i4TestValPppTestConfigIdentification != ENABLE
        && i4TestValPppTestConfigIdentification != DISABLE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppExtLinkConfigTxACFC
 Input       :  The Indices
                PppTestLinkConfigIfIndex

                The Object 
                testValPppTestLinkConfigTxACFC
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtLinkConfigTxACFC (UINT4 *pu4ErrorCode,
                                 INT4 i4PppTestLinkConfigIfIndex,
                                 INT4 i4TestValPppTestLinkConfigTxACFC)
{
    tPPPIf             *pIf = NULL;
    pIf = PppGetPppIfPtr ((UINT4) i4PppTestLinkConfigIfIndex);
    if (pIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }
    if ((1 == i4TestValPppTestLinkConfigTxACFC)
        || (2 == i4TestValPppTestLinkConfigTxACFC))
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2PppExtLinkConfigRxACFC
 Input       :  The Indices
                PppTestLinkConfigIfIndex

                The Object 
                testValPppTestLinkConfigRxACFC
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtLinkConfigRxACFC (UINT4 *pu4ErrorCode,
                                 INT4 i4PppTestLinkConfigIfIndex,
                                 INT4 i4TestValPppTestLinkConfigRxACFC)
{
    UNUSED_PARAM (i4PppTestLinkConfigIfIndex);
    if ((1 == i4TestValPppTestLinkConfigRxACFC)
        || (2 == i4TestValPppTestLinkConfigRxACFC))
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2PppExtLinkConfigTxPFC
 Input       :  The Indices
                PppTestLinkConfigIfIndex

                The Object 
                testValPppTestLinkConfigTxPFC
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtLinkConfigTxPFC (UINT4 *pu4ErrorCode,
                                INT4 i4PppTestLinkConfigIfIndex,
                                INT4 i4TestValPppTestLinkConfigTxPFC)
{
    UNUSED_PARAM (i4PppTestLinkConfigIfIndex);
    if ((ENABLE == i4TestValPppTestLinkConfigTxPFC)
        || (DISABLE == i4TestValPppTestLinkConfigTxPFC))
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2PppExtLinkConfigRxPFC
 Input       :  The Indices
                PppTestLinkConfigIfIndex

                The Object 
                testValPppTestLinkConfigRxPFC
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtLinkConfigRxPFC (UINT4 *pu4ErrorCode,
                                INT4 i4PppTestLinkConfigIfIndex,
                                INT4 i4TestValPppTestLinkConfigRxPFC)
{
    UNUSED_PARAM (i4PppTestLinkConfigIfIndex);
    if ((ENABLE == i4TestValPppTestLinkConfigRxPFC)
        || (DISABLE == i4TestValPppTestLinkConfigRxPFC))
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetPppTestConfigDiscardTest
 Input       :  The Indices
                PppTestConfigIfIndex

                The Object 
                retValPppTestConfigDiscardTest
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestConfigDiscardTest (INT4 i4PppTestConfigIfIndex,
                                INT4 *pi4RetValPppTestConfigDiscardTest)
{
    PPP_UNUSED (i4PppTestConfigIfIndex);
    PPP_UNUSED (*pi4RetValPppTestConfigDiscardTest);
    return (SNMP_FAILURE);
}

#ifdef PPP_STACK_WANTED
/****************************************************************************
 Function    :  nmhGetPppExtLinkConfigIdentification
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppExtLinkConfigIdentification
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtLinkConfigIdentification (INT4 i4IfIndex,
                                      INT4
                                      *pi4RetValPppExtLinkConfigIdentification)
{
    PPP_UNUSED (i4IfIndex);
    PPP_UNUSED (*pi4RetValPppExtLinkConfigIdentification);
    return (SNMP_FAILURE);
}
#endif /* PPP_STACK_WANTED */

/****************************************************************************
 Function    :  nmhGetPppTestConfigIdentification
 Input       :  The Indices
                PppTestConfigIfIndex

                The Object 
                retValPppTestConfigIdentification
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppTestConfigIdentification (INT4 i4PppTestConfigIfIndex,
                                   INT4 *pi4RetValPppTestConfigIdentification)
{
    PPP_UNUSED (i4PppTestConfigIfIndex);
    PPP_UNUSED (*pi4RetValPppTestConfigIdentification);
    return (SNMP_FAILURE);
}
