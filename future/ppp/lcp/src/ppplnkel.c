/******************************************************************** 
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 * 
 * $Id: ppplnkel.c,v 1.10 2014/12/16 10:49:12 siva Exp $
 * 
 * Description: Low Level Routines for Link Configuration. 
 * 
 *******************************************************************/
#ifndef __PPP_PPPLNKEL_C__
#define __PPP_PPPLNKEL_C__

#include "pppsnmpm.h"
#include "lcpcom.h"
#include "snmphdrs.h"
#include "gsemdefs.h"
#include "pppipcp.h"
#include "pppipv6cp.h"
#include "pppmuxcp.h"
#include "pppipxcp.h"
#include "pppbcp.h"
#include "pppeccmn.h"
#include "pppccp.h"
#include "pppeccmn.h"
#include "pppecp.h"
#include "ecpexts.h"
#include "mpdefs.h"
#include "llproto.h"
#include "globexts.h"
#include "pppdpproto.h"
#include "pppexlow.h"
#include "ppipcpcli.h"
#define MIN_CHAR_VALUE    1
#define MAX_CHAR_VALUE 0x000000ff

#define EXT_LINK_CFG_PASSIVE_ENABLED 1
#define EXT_LINK_CFG_PASSIVE_DISABLED  2

#define    FLAG_SEQ                    0x7e
#define    CTRL_ESC                    0x7d
#define    MIN_EXT_LINK_CFG_TX_ACCM    16
#define    MAX_EXT_LINK_CFG_TX_ACCM    32

#define MAPPED_IN_ACCM(Octet, pACCMap)    ((pACCMap [Octet/8] & (0x01 << Octet % 8)) ? PPP_YES : PPP_NO)

/* LOW LEVEL Routines for Table : PppExtLinkConfigTable. */

/**************************************************************************** 
 Function    :  nmhValidateIndexInstancePppExtLinkConfigTable 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 Output      :  The Routines Validates the Given Indices. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppExtLinkConfigTable (INT4 i4PppExtLinkConfigIfIndex)
{
    if (PppGetPppIfPtr ((UINT4) i4PppExtLinkConfigIfIndex) == NULL)

    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhGetFirstIndexPppExtLinkConfigTable 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 Output      :  The Get First Routines gets the Lexicographicaly 
                First Entry from the Table. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppExtLinkConfigTable (INT4 *pi4PppExtLinkConfigIfIndex)
{
    if (nmhGetFirstIndex (pi4PppExtLinkConfigIfIndex, LinkInstance, FIRST_INST)
        == SNMP_SUCCESS)

    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/**************************************************************************** 
 Function    :  nmhGetNextIndexPppExtLinkConfigTable 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
                nextPppExtLinkConfigIfIndex 
 Output      :  The Get Next function gets the Next Index for 
                the Index Value given in the Index Values. The 
                Indices are stored in the next_varname variables. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppExtLinkConfigTable (INT4 i4PppExtLinkConfigIfIndex,
                                      INT4 *pi4NextPppExtLinkConfigIfIndex)
{
    if (nmhGetNextIndex (&i4PppExtLinkConfigIfIndex, LinkInstance, NEXT_INST)
        == SNMP_SUCCESS)

    {
        *pi4NextPppExtLinkConfigIfIndex = i4PppExtLinkConfigIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

#ifdef PPP_STACK_WANTED
/* Low Level GET Routine for All Objects  */

/**************************************************************************** 
 Function    :  nmhGetPppExtLinkConfigRestartTimeOutValue 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                retValPppExtLinkConfigRestartTimeOutValue 
 Output      :  The Get Low Lev Routine Take the Indices & 
                store the Value requested in the Return val. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhGetPppExtLinkConfigRestartTimeOutValue (INT4 i4PppExtLinkConfigIfIndex,
                                           INT4
                                           *pi4RetValPppExtLinkConfigRestartTimeOutValue)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr (i4PppExtLinkConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }

    *pi4RetValPppExtLinkConfigRestartTimeOutValue =
        pIf->CurrentLinkConfigs.RestartTimeOutVal;

    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhGetPppExtLinkConfigRetransmitTimeOutValue 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                retValPppExtLinkConfigRetransmitTimeOutValue 
 Output      :  The Get Low Lev Routine Take the Indices & 
                store the Value requested in the Return val. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhGetPppExtLinkConfigRetransmitTimeOutValue (INT4 i4PppExtLinkConfigIfIndex,
                                              INT4
                                              *pi4RetValPppExtLinkConfigRetransmitTimeOutValue)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr (i4PppExtLinkConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }

    *pi4RetValPppExtLinkConfigRetransmitTimeOutValue =
        pIf->LinkInfo.TimeOutTime;

    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhGetPppExtLinkConfigMaxReTransmits 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                retValPppExtLinkConfigMaxReTransmits 
 Output      :  The Get Low Lev Routine Take the Indices & 
                store the Value requested in the Return val. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhGetPppExtLinkConfigMaxReTransmits (INT4 i4PppExtLinkConfigIfIndex,
                                      INT4
                                      *pi4RetValPppExtLinkConfigMaxReTransmits)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr (i4PppExtLinkConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }

    *pi4RetValPppExtLinkConfigMaxReTransmits = pIf->LinkInfo.MaxReTransmits;
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhGetPppExtLinkConfigMaxConfTransmits 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                retValPppExtLinkConfigMaxConfTransmits 
 Output      :  The Get Low Lev Routine Take the Indices & 
                store the Value requested in the Return val. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhGetPppExtLinkConfigMaxConfTransmits (INT4 i4PppExtLinkConfigIfIndex,
                                        INT4
                                        *pi4RetValPppExtLinkConfigMaxConfTransmits)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr (i4PppExtLinkConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }

    *pi4RetValPppExtLinkConfigMaxConfTransmits =
        pIf->CurrentLinkConfigs.MaxConfigReqTransmits;

    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhGetPppExtLinkConfigMaxTermTransmits 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                retValPppExtLinkConfigMaxTermTransmits 
 Output      :  The Get Low Lev Routine Take the Indices & 
                store the Value requested in the Return val. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhGetPppExtLinkConfigMaxTermTransmits (INT4 i4PppExtLinkConfigIfIndex,
                                        INT4
                                        *pi4RetValPppExtLinkConfigMaxTermTransmits)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr (i4PppExtLinkConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppExtLinkConfigMaxTermTransmits =
        pIf->CurrentLinkConfigs.MaxTermtransmits;
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhGetPppExtLinkConfigMaxNakAllowed 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                retValPppExtLinkConfigMaxNakAllowed 
 Output      :  The Get Low Lev Routine Take the Indices & 
                store the Value requested in the Return val. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhGetPppExtLinkConfigMaxNakAllowed (INT4 i4PppExtLinkConfigIfIndex,
                                     INT4
                                     *pi4RetValPppExtLinkConfigMaxNakAllowed)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr (i4PppExtLinkConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppExtLinkConfigMaxNakAllowed =
        pIf->CurrentLinkConfigs.MaxNakLoops;
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhGetPppExtLinkConfigMaxMagicLoop 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                retValPppExtLinkConfigMaxMagicLoop 
 Output      :  The Get Low Lev Routine Take the Indices & 
                store the Value requested in the Return val. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhGetPppExtLinkConfigMaxMagicLoop (INT4 i4PppExtLinkConfigIfIndex,
                                    INT4 *pi4RetValPppExtLinkConfigMaxMagicLoop)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr (i4PppExtLinkConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppExtLinkConfigMaxMagicLoop = pIf->LinkInfo.MaxNumLoops;
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhGetPppExtLinkConfigEchoCharacter 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                retValPppExtLinkConfigEchoCharacter 
 Output      :  The Get Low Lev Routine Take the Indices & 
                store the Value requested in the Return val. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhGetPppExtLinkConfigEchoCharacter (INT4 i4PppExtLinkConfigIfIndex,
                                     INT4
                                     *pi4RetValPppExtLinkConfigEchoCharacter)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr (i4PppExtLinkConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppExtLinkConfigEchoCharacter = pIf->EchoInfo.EchoChar;
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhGetPppExtLinkConfigPassiveOption 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                retValPppExtLinkConfigPassiveOption 
 Output      :  The Get Low Lev Routine Take the Indices & 
                store the Value requested in the Return val. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhGetPppExtLinkConfigPassiveOption (INT4 i4PppExtLinkConfigIfIndex,
                                     INT4
                                     *pi4RetValPppExtLinkConfigPassiveOption)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr (i4PppExtLinkConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    if (pIf->CurrentLinkConfigs.PassiveOption == SET)

    {
        *pi4RetValPppExtLinkConfigPassiveOption = EXT_LINK_CFG_PASSIVE_ENABLED;
    }

    else

    {
        *pi4RetValPppExtLinkConfigPassiveOption = EXT_LINK_CFG_PASSIVE_DISABLED;
    }
    return (SNMP_SUCCESS);
}

 /****************************************************************************
 Function    :  nmhGetPppExtLinkConfigTxACCM
 Input       :  The Indices
                PppExtLinkConfigIfIndex

                The Object 
                retValPppExtLinkConfigTxACCM
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtLinkConfigTxACCM (INT4 i4PppExtLinkConfigIfIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValPppExtLinkConfigTxACCM)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetIfPtr (i4PppExtLinkConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pRetValPppExtLinkConfigTxACCM->i4_Length = MAX_EXT_LINK_CFG_TX_ACCM;
    MEMCPY (pRetValPppExtLinkConfigTxACCM->pu1_OctetList, pIf->ExtTxACCM,
            pRetValPppExtLinkConfigTxACCM->i4_Length);

    return (SNMP_SUCCESS);

}

/* Low Level TEST Routines for All Objects  */

/**************************************************************************** 
 Function    :  nmhTestv2PppExtLinkConfigRestartTimeOutValue 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                testValPppExtLinkConfigRestartTimeOutValue 
 Output      :  The Test Low Lev Routine Take the Indices & 
                Test whether that Value is Valid Input for Set. 
                Stores the value of error code in the Return val 
 Error Codes :  The following error codes are to be returned 
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905) 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhTestv2PppExtLinkConfigRestartTimeOutValue (UINT4 *pu4ErrorCode,
                                              INT4 i4PppExtLinkConfigIfIndex,
                                              INT4
                                              i4TestValPppExtLinkConfigRestartTimeOutValue)
{

    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetIfPtr (i4PppExtLinkConfigIfIndex)) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppExtLinkConfigRestartTimeOutValue < MIN_INTEGER
        || i4TestValPppExtLinkConfigRestartTimeOutValue > MAX_INTEGER)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhTestv2PppExtLinkConfigRetransmitTimeOutValue 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                testValPppExtLinkConfigRetransmitTimeOutValue 
 Output      :  The Test Low Lev Routine Take the Indices & 
                Test whether that Value is Valid Input for Set. 
                Stores the value of error code in the Return val 
 Error Codes :  The following error codes are to be returned 
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905) 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhTestv2PppExtLinkConfigRetransmitTimeOutValue (UINT4 *pu4ErrorCode,
                                                 INT4
                                                 i4PppExtLinkConfigIfIndex,
                                                 INT4
                                                 i4TestValPppExtLinkConfigRetransmitTimeOutValue)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetIfPtr (i4PppExtLinkConfigIfIndex)) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppExtLinkConfigRetransmitTimeOutValue < MIN_INTEGER
        || i4TestValPppExtLinkConfigRetransmitTimeOutValue > MAX_INTEGER)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhTestv2PppExtLinkConfigMaxReTransmits 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                testValPppExtLinkConfigMaxReTransmits 
 Output      :  The Test Low Lev Routine Take the Indices & 
                Test whether that Value is Valid Input for Set. 
                Stores the value of error code in the Return val 
 Error Codes :  The following error codes are to be returned 
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905) 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhTestv2PppExtLinkConfigMaxReTransmits (UINT4 *pu4ErrorCode,
                                         INT4 i4PppExtLinkConfigIfIndex,
                                         INT4
                                         i4TestValPppExtLinkConfigMaxReTransmits)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetIfPtr (i4PppExtLinkConfigIfIndex)) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppExtLinkConfigMaxReTransmits < MIN_INTEGER
        || i4TestValPppExtLinkConfigMaxReTransmits > MAX_INTEGER)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhTestv2PppExtLinkConfigMaxConfTransmits 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                testValPppExtLinkConfigMaxConfTransmits 
 Output      :  The Test Low Lev Routine Take the Indices & 
                Test whether that Value is Valid Input for Set. 
                Stores the value of error code in the Return val 
 Error Codes :  The following error codes are to be returned 
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905) 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhTestv2PppExtLinkConfigMaxConfTransmits (UINT4 *pu4ErrorCode,
                                           INT4 i4PppExtLinkConfigIfIndex,
                                           INT4
                                           i4TestValPppExtLinkConfigMaxConfTransmits)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetIfPtr (i4PppExtLinkConfigIfIndex)) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppExtLinkConfigMaxConfTransmits < MIN_INTEGER
        || i4TestValPppExtLinkConfigMaxConfTransmits > MAX_INTEGER)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhTestv2PppExtLinkConfigMaxTermTransmits 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                testValPppExtLinkConfigMaxTermTransmits 
 Output      :  The Test Low Lev Routine Take the Indices & 
                Test whether that Value is Valid Input for Set. 
                Stores the value of error code in the Return val 
 Error Codes :  The following error codes are to be returned 
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905) 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhTestv2PppExtLinkConfigMaxTermTransmits (UINT4 *pu4ErrorCode,
                                           INT4 i4PppExtLinkConfigIfIndex,
                                           INT4
                                           i4TestValPppExtLinkConfigMaxTermTransmits)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetIfPtr (i4PppExtLinkConfigIfIndex)) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppExtLinkConfigMaxTermTransmits < MIN_INTEGER
        || i4TestValPppExtLinkConfigMaxTermTransmits > MAX_INTEGER)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhTestv2PppExtLinkConfigMaxNakAllowed 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                testValPppExtLinkConfigMaxNakAllowed 
 Output      :  The Test Low Lev Routine Take the Indices & 
                Test whether that Value is Valid Input for Set. 
                Stores the value of error code in the Return val 
 Error Codes :  The following error codes are to be returned 
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905) 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhTestv2PppExtLinkConfigMaxNakAllowed (UINT4 *pu4ErrorCode,
                                        INT4 i4PppExtLinkConfigIfIndex,
                                        INT4
                                        i4TestValPppExtLinkConfigMaxNakAllowed)
{

    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetIfPtr (i4PppExtLinkConfigIfIndex)) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppExtLinkConfigMaxNakAllowed < MIN_INTEGER
        || i4TestValPppExtLinkConfigMaxNakAllowed > MAX_INTEGER)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhTestv2PppExtLinkConfigMaxMagicLoop 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                testValPppExtLinkConfigMaxMagicLoop 
 Output      :  The Test Low Lev Routine Take the Indices & 
                Test whether that Value is Valid Input for Set. 
                Stores the value of error code in the Return val 
 Error Codes :  The following error codes are to be returned 
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905) 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhTestv2PppExtLinkConfigMaxMagicLoop (UINT4 *pu4ErrorCode,
                                       INT4 i4PppExtLinkConfigIfIndex,
                                       INT4
                                       i4TestValPppExtLinkConfigMaxMagicLoop)
{

    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetIfPtr (i4PppExtLinkConfigIfIndex)) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppExtLinkConfigMaxMagicLoop < MIN_INTEGER
        || i4TestValPppExtLinkConfigMaxMagicLoop > MAX_INTEGER)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhTestv2PppExtLinkConfigEchoCharacter 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                testValPppExtLinkConfigEchoCharacter 
 Output      :  The Test Low Lev Routine Take the Indices & 
                Test whether that Value is Valid Input for Set. 
                Stores the value of error code in the Return val 
 Error Codes :  The following error codes are to be returned 
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905) 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhTestv2PppExtLinkConfigEchoCharacter (UINT4 *pu4ErrorCode,
                                        INT4 i4PppExtLinkConfigIfIndex,
                                        INT4
                                        i4TestValPppExtLinkConfigEchoCharacter)
{

    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetIfPtr (i4PppExtLinkConfigIfIndex)) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppExtLinkConfigEchoCharacter < MIN_CHAR_VALUE
        || i4TestValPppExtLinkConfigEchoCharacter > MAX_CHAR_VALUE)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhTestv2PppExtLinkConfigPassiveOption 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                testValPppExtLinkConfigPassiveOption 
 Output      :  The Test Low Lev Routine Take the Indices & 
                Test whether that Value is Valid Input for Set. 
                Stores the value of error code in the Return val 
 Error Codes :  The following error codes are to be returned 
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905) 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhTestv2PppExtLinkConfigPassiveOption (UINT4 *pu4ErrorCode,
                                        INT4 i4PppExtLinkConfigIfIndex,
                                        INT4
                                        i4TestValPppExtLinkConfigPassiveOption)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetIfPtr (i4PppExtLinkConfigIfIndex)) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppExtLinkConfigPassiveOption !=
        EXT_LINK_CFG_PASSIVE_ENABLED && i4TestValPppExtLinkConfigPassiveOption
        != EXT_LINK_CFG_PASSIVE_DISABLED)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhGetPppExtLinkConfigInternationOption 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                retValPppExtLinkConfigInternationOption 
 Output      :  The Get Low Lev Routine Take the Indices & 
                store the Value requested in the Return val. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhGetPppExtLinkConfigInternationOption (INT4 i4PppExtLinkConfigIfIndex,
                                         INT4
                                         *pi4RetValPppExtLinkConfigInternationOption)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr (i4PppExtLinkConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }

    *pi4RetValPppExtLinkConfigInternationOption =
        pIf->LinkConfigs.InternationOption;
    return SNMP_SUCCESS;
}
#endif /* PPP_STACK_WANTED */
/**************************************************************************** 
 Function    :  nmhGetPppExtLinkConfigLowerIfType 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                retValPppExtLinkConfigLowerIfType 
 Output      :  The Get Low Lev Routine Take the Indices & 
                store the Value requested in the Return val. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhGetPppExtLinkConfigLowerIfType (INT4 i4PppExtLinkConfigIfIndex,
                                   INT4 *pi4RetValPppExtLinkConfigLowerIfType)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppExtLinkConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppExtLinkConfigLowerIfType = pIf->LinkInfo.IfID.IfType;
    return SNMP_SUCCESS;
}

#ifdef PPP_STACK_WANTED
/**************************************************************************** 
 Function    :  nmhGetPppExtLinkConfigLowerIfDlci 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                retValPppExtLinkConfigLowerIfDlci 
 Output      :  The Get Low Lev Routine Take the Indices & 
                store the Value requested in the Return val. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhGetPppExtLinkConfigLowerIfDlci (INT4 i4PppExtLinkConfigIfIndex,
                                   INT4 *pi4RetValPppExtLinkConfigLowerIfDlci)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr (i4PppExtLinkConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppExtLinkConfigLowerIfDlci = pIf->LinkInfo.IfID.VCNumber;
    return SNMP_SUCCESS;
}
#endif /* PPP_STACK_WANTED */
/**************************************************************************** 
 Function    :  nmhGetPppExtLinkConfigHostName 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                retValPppExtLinkConfigHostName 
 Output      :  The Get Low Lev Routine Take the Indices & 
                store the Value requested in the Return val. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhGetPppExtLinkConfigHostName (INT4 i4PppExtLinkConfigIfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValPppExtLinkConfigHostName)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppExtLinkConfigIfIndex)) == NULL)

    {
        return SNMP_FAILURE;
    }
    STRCPY (pRetValPppExtLinkConfigHostName->pu1_OctetList, pIf->au1HostName);
    pRetValPppExtLinkConfigHostName->i4_Length =
        (INT4) (STRLEN (pIf->au1HostName));
    return SNMP_SUCCESS;
}

#ifdef PPP_STACK_WANTED
/**************************************************************************** 
 Function    :  nmhGetPppExtLinkConfigScrambleEnable 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                retValPppExtLinkConfigScrambleEnable 
 Output      :  The Get Low Lev Routine Take the Indices & 
                store the Value requested in the Return val. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhGetPppExtLinkConfigScrambleEnable (INT4 i4PppExtLinkConfigIfIndex,
                                      INT4
                                      *pi4RetValPppExtLinkConfigScrambleEnable)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr (i4PppExtLinkConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    if (pIf->LinkInfo.IfID.IfType != SONET_IF_TYPE)
    {

        *pi4RetValPppExtLinkConfigScrambleEnable = FALSE;
    }
    else
    {
        *pi4RetValPppExtLinkConfigScrambleEnable = pIf->pSonetIf->TxScramble;
    }
    return (SNMP_SUCCESS);
}
#endif /* PPP_STACK_WANTED */
/* Low Level SET Routine for All Objects  */

/**************************************************************************** 
 Function    :  nmhSetPppExtLinkConfigRestartTimeOutValue 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                setValPppExtLinkConfigRestartTimeOutValue 
 Output      :  The Set Low Lev Routine Take the Indices & 
                Sets the Value accordingly. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhSetPppExtLinkConfigRestartTimeOutValue (INT4 i4PppExtLinkConfigIfIndex,
                                           INT4
                                           i4SetValPppExtLinkConfigRestartTimeOutValue)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetIfPtr ((UINT4) i4PppExtLinkConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    pIf->LinkConfigs.RestartTimeOutVal =
        (UINT2) i4SetValPppExtLinkConfigRestartTimeOutValue;
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhSetPppExtLinkConfigRetransmitTimeOutValue 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                setValPppExtLinkConfigRetransmitTimeOutValue 
 Output      :  The Set Low Lev Routine Take the Indices & 
                Sets the Value accordingly. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhSetPppExtLinkConfigRetransmitTimeOutValue (INT4 i4PppExtLinkConfigIfIndex,
                                              INT4
                                              i4SetValPppExtLinkConfigRetransmitTimeOutValue)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetIfPtr ((UINT4) i4PppExtLinkConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    pIf->LinkInfo.TimeOutTime =
        (UINT2) i4SetValPppExtLinkConfigRetransmitTimeOutValue;
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhSetPppExtLinkConfigMaxReTransmits 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                setValPppExtLinkConfigMaxReTransmits 
 Output      :  The Set Low Lev Routine Take the Indices & 
                Sets the Value accordingly. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhSetPppExtLinkConfigMaxReTransmits (INT4 i4PppExtLinkConfigIfIndex,
                                      INT4
                                      i4SetValPppExtLinkConfigMaxReTransmits)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppExtLinkConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    pIf->LinkInfo.MaxReTransmits =
        (UINT2) i4SetValPppExtLinkConfigMaxReTransmits;

    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhSetPppExtLinkConfigMaxConfTransmits 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                setValPppExtLinkConfigMaxConfTransmits 
 Output      :  The Set Low Lev Routine Take the Indices & 
                Sets the Value accordingly. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhSetPppExtLinkConfigMaxConfTransmits (INT4 i4PppExtLinkConfigIfIndex,
                                        INT4
                                        i4SetValPppExtLinkConfigMaxConfTransmits)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppExtLinkConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    pIf->LinkConfigs.MaxConfigReqTransmits =
        (UINT2) i4SetValPppExtLinkConfigMaxConfTransmits;

    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhSetPppExtLinkConfigMaxTermTransmits 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                setValPppExtLinkConfigMaxTermTransmits 
 Output      :  The Set Low Lev Routine Take the Indices & 
                Sets the Value accordingly. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhSetPppExtLinkConfigMaxTermTransmits (INT4 i4PppExtLinkConfigIfIndex,
                                        INT4
                                        i4SetValPppExtLinkConfigMaxTermTransmits)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppExtLinkConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    pIf->LinkConfigs.MaxTermtransmits =
        (UINT2) i4SetValPppExtLinkConfigMaxTermTransmits;
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhSetPppExtLinkConfigMaxNakAllowed 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                setValPppExtLinkConfigMaxNakAllowed 
 Output      :  The Set Low Lev Routine Take the Indices & 
                Sets the Value accordingly. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhSetPppExtLinkConfigMaxNakAllowed (INT4 i4PppExtLinkConfigIfIndex,
                                     INT4 i4SetValPppExtLinkConfigMaxNakAllowed)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppExtLinkConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    pIf->LinkConfigs.MaxNakLoops =
        (UINT2) i4SetValPppExtLinkConfigMaxNakAllowed;
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhSetPppExtLinkConfigMaxMagicLoop 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                setValPppExtLinkConfigMaxMagicLoop 
 Output      :  The Set Low Lev Routine Take the Indices & 
                Sets the Value accordingly. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhSetPppExtLinkConfigMaxMagicLoop (INT4 i4PppExtLinkConfigIfIndex,
                                    INT4 i4SetValPppExtLinkConfigMaxMagicLoop)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppExtLinkConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    pIf->LinkInfo.MaxNumLoops = (UINT4) i4SetValPppExtLinkConfigMaxMagicLoop;
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhSetPppExtLinkConfigEchoCharacter 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                setValPppExtLinkConfigEchoCharacter 
 Output      :  The Set Low Lev Routine Take the Indices & 
                Sets the Value accordingly. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhSetPppExtLinkConfigEchoCharacter (INT4 i4PppExtLinkConfigIfIndex,
                                     INT4 i4SetValPppExtLinkConfigEchoCharacter)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppExtLinkConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    pIf->EchoInfo.EchoChar = (UINT1) i4SetValPppExtLinkConfigEchoCharacter;
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhSetPppExtLinkConfigPassiveOption 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                setValPppExtLinkConfigPassiveOption 
 Output      :  The Set Low Lev Routine Take the Indices & 
                Sets the Value accordingly. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhSetPppExtLinkConfigPassiveOption (INT4 i4PppExtLinkConfigIfIndex,
                                     INT4 i4SetValPppExtLinkConfigPassiveOption)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppExtLinkConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    if (i4SetValPppExtLinkConfigPassiveOption == EXT_LINK_CFG_PASSIVE_ENABLED)

    {
        pIf->LinkConfigs.PassiveOption = SET;
    }

    else

    {
        pIf->LinkConfigs.PassiveOption = NOT_SET;
    }
    return (SNMP_SUCCESS);
}

#ifdef PPP_STACK_WANTED
/* Low Level GET Routine for All Objects  */

/**************************************************************************** 
 Function    :  nmhGetPppExtLinkCallBackConfigLocToRemOperation 
 Input       :  The Indices 
                PppExtLinkCallBackConfigIfIndex 
 
                The Object  
                retValPppExtLinkCallBackConfigLocToRemOperation 
 Output      :  The Get Low Lev Routine Take the Indices & 
                store the Value requested in the Return val. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhGetPppExtLinkCallBackConfigLocToRemOperation (INT4
                                                 i4PppExtLinkCallBackConfigIfIndex,
                                                 INT4
                                                 *pi4RetValPppExtLinkCallBackConfigLocToRemOperation)
{
    tPPPIf             *pIf = NULL;
    if ((pIf =
         PppGetPppIfPtr ((UINT4) i4PppExtLinkCallBackConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppExtLinkCallBackConfigLocToRemOperation =
        pIf->LcpOptionsAckedByPeer.LCPCallBackInfo.CallBackOperation;
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhGetPppExtLinkCallBackConfigRemToLocOperation 
 Input       :  The Indices 
                PppExtLinkCallBackConfigIfIndex 
 
                The Object  
                retValPppExtLinkCallBackConfigRemToLocOperation 
 Output      :  The Get Low Lev Routine Take the Indices & 
                store the Value requested in the Return val. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhGetPppExtLinkCallBackConfigRemToLocOperation (INT4
                                                 i4PppExtLinkCallBackConfigIfIndex,
                                                 INT4
                                                 *pi4RetValPppExtLinkCallBackConfigRemToLocOperation)
{
    tPPPIf             *pIf = NULL;
    if ((pIf =
         PppGetPppIfPtr ((UINT4) i4PppExtLinkCallBackConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppExtLinkCallBackConfigRemToLocOperation =
        pIf->LcpOptionsAckedByLocal.LCPCallBackInfo.CallBackOperation;
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhGetPppExtLinkCallBackConfigLocToRemMessage 
 Input       :  The Indices 
                PppExtLinkCallBackConfigIfIndex 
 
                The Object  
                retValPppExtLinkCallBackConfigLocToRemMessage 
 Output      :  The Get Low Lev Routine Take the Indices & 
                store the Value requested in the Return val. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhGetPppExtLinkCallBackConfigLocToRemMessage (INT4
                                               i4PppExtLinkCallBackConfigIfIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pRetValPppExtLinkCallBackConfigLocToRemMessage)
{
    tPPPIf             *pIf = NULL;
    if ((pIf =
         PppGetPppIfPtr ((UINT4) i4PppExtLinkCallBackConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    pRetValPppExtLinkCallBackConfigLocToRemMessage->i4_Length =
        pIf->LcpOptionsAckedByPeer.LCPCallBackInfo.CallBackMsgLen;
    MEMCPY (pRetValPppExtLinkCallBackConfigLocToRemMessage->pu1_OctetList,
            pIf->LcpOptionsAckedByPeer.LCPCallBackInfo.pCallBackMsg,
            pRetValPppExtLinkCallBackConfigLocToRemMessage->i4_Length);
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhGetPppExtLinkCallBackConfigLocToRemNeg 
 Input       :  The Indices 
                PppExtLinkCallBackConfigIfIndex 
 
                The Object  
                retValPppExtLinkCallBackConfigLocToRemNeg 
 Output      :  The Get Low Lev Routine Take the Indices & 
                store the Value requested in the Return val. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhGetPppExtLinkCallBackConfigLocToRemNeg (INT4
                                           i4PppExtLinkCallBackConfigIfIndex,
                                           INT4
                                           *pi4RetValPppExtLinkCallBackConfigLocToRemNeg)
{
    tPPPIf             *pIf = NULL;
    if ((pIf =
         PppGetPppIfPtr ((UINT4) i4PppExtLinkCallBackConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    if (pIf->LcpGSEM.pNegFlagsPerIf[CALL_BACK_IDX].
        FlagMask & ACKED_BY_PEER_MASK)

    {
        *pi4RetValPppExtLinkCallBackConfigLocToRemNeg = ENABLE;
    }

    else

    {
        *pi4RetValPppExtLinkCallBackConfigLocToRemNeg = DISABLE;
    }
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhGetPppExtLinkCallBackConfigRemToLocNeg 
 Input       :  The Indices 
                PppExtLinkCallBackConfigIfIndex 
 
                The Object  
                retValPppExtLinkCallBackConfigRemToLocNeg 
 Output      :  The Get Low Lev Routine Take the Indices & 
                store the Value requested in the Return val. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhGetPppExtLinkCallBackConfigRemToLocNeg (INT4
                                           i4PppExtLinkCallBackConfigIfIndex,
                                           INT4
                                           *pi4RetValPppExtLinkCallBackConfigRemToLocNeg)
{
    tPPPIf             *pIf = NULL;
    if ((pIf =
         PppGetPppIfPtr ((UINT4) i4PppExtLinkCallBackConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    if (pIf->LcpGSEM.pNegFlagsPerIf[CALL_BACK_IDX].
        FlagMask & ACKED_BY_LOCAL_MASK)

    {
        *pi4RetValPppExtLinkCallBackConfigRemToLocNeg = ENABLE;
    }

    else

    {
        *pi4RetValPppExtLinkCallBackConfigRemToLocNeg = DISABLE;
    }
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhGetPppExtLinkCallBackConfigTimeBeforeDisconnect 
 Input       :  The Indices 
                PppExtLinkCallBackConfigIfIndex 
 
                The Object  
                retValPppExtLinkCallBackConfigTimeBeforeDisconnect 
 Output      :  The Get Low Lev Routine Take the Indices & 
                store the Value requested in the Return val. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhGetPppExtLinkCallBackConfigTimeBeforeDisconnect (INT4
                                                    i4PppExtLinkCallBackConfigIfIndex,
                                                    INT4
                                                    *pi4RetValPppExtLinkCallBackConfigTimeBeforeDisconnect)
{
    tPPPIf             *pIf = NULL;
    if ((pIf =
         PppGetPppIfPtr ((UINT4) i4PppExtLinkCallBackConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppExtLinkCallBackConfigTimeBeforeDisconnect =
        pIf->CallBackTimeInfo.TimeBeforeDisconnect;
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhGetPppExtLinkCallBackConfigReDialTime 
 Input       :  The Indices 
                PppExtLinkCallBackConfigIfIndex 
 
                The Object  
                retValPppExtLinkCallBackConfigReDialTime 
 Output      :  The Get Low Lev Routine Take the Indices & 
                store the Value requested in the Return val. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhGetPppExtLinkCallBackConfigReDialTime (INT4
                                          i4PppExtLinkCallBackConfigIfIndex,
                                          INT4
                                          *pi4RetValPppExtLinkCallBackConfigReDialTime)
{
    tPPPIf             *pIf = NULL;
    if ((pIf =
         PppGetPppIfPtr ((UINT4) i4PppExtLinkCallBackConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppExtLinkCallBackConfigReDialTime =
        pIf->CallBackTimeInfo.TimeToDialAfterDisconnect;
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhSetPppExtLinkConfigInternationOption 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                setValPppExtLinkConfigInternationOption 
 Output      :  The Set Low Lev Routine Take the Indices & 
                Sets the Value accordingly. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhSetPppExtLinkConfigInternationOption (INT4 i4PppExtLinkConfigIfIndex,
                                         INT4
                                         i4SetValPppExtLinkConfigInternationOption)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppExtLinkConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    pIf->LinkConfigs.InternationOption =
        (UINT1) i4SetValPppExtLinkConfigInternationOption;
    if (i4SetValPppExtLinkConfigInternationOption ==
        LCP_INTERNATION_OPT_ENABLED)

    {
        pIf->LcpGSEM.pNegFlagsPerIf[INTERNATION_IDX].FlagMask |= DESIRED_SET;
    }

    else

    {
        pIf->LcpGSEM.pNegFlagsPerIf[INTERNATION_IDX].FlagMask &=
            DESIRED_NOT_SET;
    }
    return SNMP_SUCCESS;
}
#endif /* PPP_STACK_WANTED */
/**************************************************************************** 
 Function    :  nmhSetPppExtLinkConfigLowerIfType 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                setValPppExtLinkConfigLowerIfType 
 Output      :  The Set Low Lev Routine Take the Indices & 
                Sets the Value accordingly. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhSetPppExtLinkConfigLowerIfType (INT4 i4PppExtLinkConfigIfIndex,
                                   INT4 i4SetValPppExtLinkConfigLowerIfType)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tPPPIf             *pIf = NULL;
    tPPPIfId            IfId;
    UINT4               u4SeqNum = 0;

    MEMSET (&IfId, 0, sizeof (tPPPIfId));
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppExtLinkConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pIf->LcpStatus.AdminStatus != STATUS_DOWN)
    {
        return (SNMP_FAILURE);
    }
#ifdef PPTP_WANTED
    if (i4SetValPppExtLinkConfigLowerIfType == PPTP_IF_TYPE)
    {
        IfId.IfType = PPTP_IF_TYPE;
        IfId.porttype = (UINT1) i4PppExtLinkConfigIfIndex;
        MEMCPY (&pIf->LinkInfo.IfID, &IfId, sizeof (tPPPIfId));
        LCPReInit (pIf);
    }
#endif

    if (i4SetValPppExtLinkConfigLowerIfType == ASYNC_IF_TYPE)
    {
        /* Update the LL Operstatus as DOWN */
        IfId.IfType = ASYNC_IF_TYPE;
        IfId.porttype = (UINT1) i4PppExtLinkConfigIfIndex;
        MEMCPY (&pIf->LinkInfo.IfID, &IfId, sizeof (tPPPIfId));
        pIf->LinkInfo.u1FrmFlag = TRUE;
        LCPReInit (pIf);
    }

    if ((pIf->LinkInfo.IfID.IfType == ATM_IF_TYPE)
        && (i4SetValPppExtLinkConfigLowerIfType == PPP_OE_IFTYPE))
    {

        /* Update the LL Operstatus as DOWN */
        PppUpdateLLOperStatus ((UINT4) i4PppExtLinkConfigIfIndex, PPP_DOWN);

        IfId.IfType = PPP_OE_IFTYPE;
        IfId.porttype = (UINT1) i4PppExtLinkConfigIfIndex;
        MEMCPY (&pIf->LinkInfo.IfID, &IfId, sizeof (tPPPIfId));
        LCPReInit (pIf);
    }

    if ((pIf->LinkInfo.IfID.IfType == PPP_OE_IFTYPE)
        && (i4SetValPppExtLinkConfigLowerIfType == ATM_IF_TYPE))
    {
        PppUpdateLLOperStatus ((UINT4) i4PppExtLinkConfigIfIndex, PPP_DOWN);

        IfId.IfType = ATM_IF_TYPE;
        IfId.porttype = (UINT1) i4PppExtLinkConfigIfIndex;
        MEMCPY (&pIf->LinkInfo.IfID, &IfId, sizeof (tPPPIfId));
        LCPReInit (pIf);

        PppUpdateLLOperStatus ((UINT4) i4PppExtLinkConfigIfIndex, PPP_UP);
    }

    if (i4SetValPppExtLinkConfigLowerIfType == L2TP_IF_TYPE)
    {
        PppUpdateLLOperStatus ((UINT4) i4PppExtLinkConfigIfIndex, PPP_DOWN);

        IfId.IfType = L2TP_IF_TYPE;
        IfId.porttype = (UINT1) i4PppExtLinkConfigIfIndex;
        MEMCPY (&pIf->LinkInfo.IfID, &IfId, sizeof (tPPPIfId));
        LCPReInit (pIf);

        PppUpdateLLOperStatus ((UINT4) i4PppExtLinkConfigIfIndex, PPP_UP);
    }

#ifdef DPIF
    PppUpdatePppInterfaceInDp (pIf);
#endif
    /* BALA Doubts about the info in the above code */

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtLinkConfigLowerIfType, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                  i4PppExtLinkConfigIfIndex,
                  i4SetValPppExtLinkConfigLowerIfType));
    return SNMP_SUCCESS;

}

#ifdef PPP_STACK_WANTED
/**************************************************************************** 
 Function    :  nmhSetPppExtLinkConfigLowerIfDlci 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                setValPppExtLinkConfigLowerIfDlci 
 Output      :  The Set Low Lev Routine Take the Indices & 
                Sets the Value accordingly. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhSetPppExtLinkConfigLowerIfDlci (INT4 i4PppExtLinkConfigIfIndex,
                                   INT4 i4SetValPppExtLinkConfigLowerIfDlci)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetIfPtr ((UINT4) (i4PppExtLinkConfigIfIndex))) == NULL)

    {
        return (SNMP_FAILURE);
    }
    pIf->LinkInfo.IfID.VCNumber = (UINT2) i4SetValPppExtLinkConfigLowerIfDlci;
    return SNMP_SUCCESS;
}
#endif /* PPP_STACK_WANTED */
/**************************************************************************** 
 Function    :  nmhSetPppExtLinkConfigHostName 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                setValPppExtLinkConfigHostName 
 Output      :  The Set Low Lev Routine Take the Indices & 
                Sets the Value accordingly. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhSetPppExtLinkConfigHostName (INT4 i4PppExtLinkConfigIfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValPppExtLinkConfigHostName)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tPPPIf             *pIf = NULL;
    UINT2               u2Len;
     MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if ((u2Len = (UINT2) (pSetValPppExtLinkConfigHostName->i4_Length)))

    {
        RM_GET_SEQ_NUM (&u4SeqNum);
        pIf = PppGetPppIfPtr ((UINT4) i4PppExtLinkConfigIfIndex);
        if (pIf == NULL)
        {
            return SNMP_FAILURE;
        }
        MEMCPY (pIf->au1HostName,
                pSetValPppExtLinkConfigHostName->pu1_OctetList,
                MEM_MAX_BYTES (u2Len, PPP_MAX_NAME_LENGTH));
 SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtLinkConfigHostName, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s",
                  i4PppExtLinkConfigIfIndex,
                 pSetValPppExtLinkConfigHostName));



        return SNMP_SUCCESS;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtLinkConfigHostName, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s",
                  i4PppExtLinkConfigIfIndex,
                  pSetValPppExtLinkConfigHostName));


    return SNMP_SUCCESS;
}

#ifdef PPP_STACK_WANTED
/**************************************************************************** 
 Function    :  nmhSetPppExtLinkConfigScrambleEnable 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                setValPppExtLinkConfigScrambleEnable 
 Output      :  The Set Low Lev Routine Take the Indices & 
                Sets the Value accordingly. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhSetPppExtLinkConfigScrambleEnable (INT4 i4PppExtLinkConfigIfIndex,
                                      INT4
                                      i4SetValPppExtLinkConfigScrambleEnable)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr (i4PppExtLinkConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    pIf->pSonetIf->TxScramble = (UINT1) i4SetValPppExtLinkConfigScrambleEnable;
    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/**************************************************************************** 
 Function    :  nmhTestv2PppExtLinkCallBackConfigLocToRemOperation 
 Input       :  The Indices 
                PppExtLinkCallBackConfigIfIndex 
 
                The Object  
                testValPppExtLinkCallBackConfigLocToRemOperation 
 Output      :  The Test Low Lev Routine Take the Indices & 
                Test whether that Value is Valid Input for Set. 
                Stores the value of error code in the Return val 
 Error Codes :  The following error codes are to be returned 
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905) 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhTestv2PppExtLinkCallBackConfigLocToRemOperation (UINT4 *pu4ErrorCode,
                                                    INT4
                                                    i4PppExtLinkCallBackConfigIfIndex,
                                                    INT4
                                                    i4TestValPppExtLinkCallBackConfigLocToRemOperation)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetIfPtr (i4PppExtLinkCallBackConfigIfIndex)) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((i4TestValPppExtLinkCallBackConfigLocToRemOperation < 0)
        || (i4TestValPppExtLinkCallBackConfigLocToRemOperation >
            MAX_CALL_BACK_DISCRETE_VALUES))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhTestv2PppExtLinkCallBackConfigRemToLocOperation 
 Input       :  The Indices 
                PppExtLinkCallBackConfigIfIndex 
 
                The Object  
                testValPppExtLinkCallBackConfigRemToLocOperation 
 Output      :  The Test Low Lev Routine Take the Indices & 
                Test whether that Value is Valid Input for Set. 
                Stores the value of error code in the Return val 
 Error Codes :  The following error codes are to be returned 
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905) 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhTestv2PppExtLinkCallBackConfigRemToLocOperation (UINT4 *pu4ErrorCode,
                                                    INT4
                                                    i4PppExtLinkCallBackConfigIfIndex,
                                                    INT4
                                                    i4TestValPppExtLinkCallBackConfigRemToLocOperation)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetIfPtr (i4PppExtLinkCallBackConfigIfIndex)) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((i4TestValPppExtLinkCallBackConfigRemToLocOperation < 0)
        || (i4TestValPppExtLinkCallBackConfigRemToLocOperation >
            MAX_CALL_BACK_DISCRETE_VALUES))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhTestv2PppExtLinkCallBackConfigLocToRemMessage 
 Input       :  The Indices 
                PppExtLinkCallBackConfigIfIndex 
 
                The Object  
                testValPppExtLinkCallBackConfigLocToRemMessage 
 Output      :  The Test Low Lev Routine Take the Indices & 
                Test whether that Value is Valid Input for Set. 
                Stores the value of error code in the Return val 
 Error Codes :  The following error codes are to be returned 
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905) 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhTestv2PppExtLinkCallBackConfigLocToRemMessage (UINT4 *pu4ErrorCode,
                                                  INT4
                                                  i4PppExtLinkCallBackConfigIfIndex,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pTestValPppExtLinkCallBackConfigLocToRemMessage)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetIfPtr (i4PppExtLinkCallBackConfigIfIndex)) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pTestValPppExtLinkCallBackConfigLocToRemMessage->i4_Length > 255)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhTestv2PppExtLinkCallBackConfigLocToRemNeg 
 Input       :  The Indices 
                PppExtLinkCallBackConfigIfIndex 
 
                The Object  
                testValPppExtLinkCallBackConfigLocToRemNeg 
 Output      :  The Test Low Lev Routine Take the Indices & 
                Test whether that Value is Valid Input for Set. 
                Stores the value of error code in the Return val 
 Error Codes :  The following error codes are to be returned 
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905) 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhTestv2PppExtLinkCallBackConfigLocToRemNeg (UINT4 *pu4ErrorCode,
                                              INT4
                                              i4PppExtLinkCallBackConfigIfIndex,
                                              INT4
                                              i4TestValPppExtLinkCallBackConfigLocToRemNeg)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetIfPtr (i4PppExtLinkCallBackConfigIfIndex)) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((i4TestValPppExtLinkCallBackConfigLocToRemNeg != ENABLE)
        && (i4TestValPppExtLinkCallBackConfigLocToRemNeg != DISABLE))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhTestv2PppExtLinkCallBackConfigRemToLocNeg 
 Input       :  The Indices 
                PppExtLinkCallBackConfigIfIndex 
 
                The Object  
                testValPppExtLinkCallBackConfigRemToLocNeg 
 Output      :  The Test Low Lev Routine Take the Indices & 
                Test whether that Value is Valid Input for Set. 
                Stores the value of error code in the Return val 
 Error Codes :  The following error codes are to be returned 
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905) 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhTestv2PppExtLinkCallBackConfigRemToLocNeg (UINT4 *pu4ErrorCode,
                                              INT4
                                              i4PppExtLinkCallBackConfigIfIndex,
                                              INT4
                                              i4TestValPppExtLinkCallBackConfigRemToLocNeg)
{

    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetIfPtr (i4PppExtLinkCallBackConfigIfIndex)) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((i4TestValPppExtLinkCallBackConfigRemToLocNeg != ENABLE)
        && (i4TestValPppExtLinkCallBackConfigRemToLocNeg != DISABLE))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhTestv2PppExtLinkCallBackConfigTimeBeforeDisconnect 
 Input       :  The Indices 
                PppExtLinkCallBackConfigIfIndex 
 
                The Object  
                testValPppExtLinkCallBackConfigTimeBeforeDisconnect 
 Output      :  The Test Low Lev Routine Take the Indices & 
                Test whether that Value is Valid Input for Set. 
                Stores the value of error code in the Return val 
 Error Codes :  The following error codes are to be returned 
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905) 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhTestv2PppExtLinkCallBackConfigTimeBeforeDisconnect (UINT4 *pu4ErrorCode,
                                                       INT4
                                                       i4PppExtLinkCallBackConfigIfIndex,
                                                       INT4
                                                       i4TestValPppExtLinkCallBackConfigTimeBeforeDisconnect)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetIfPtr (i4PppExtLinkCallBackConfigIfIndex)) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((i4TestValPppExtLinkCallBackConfigTimeBeforeDisconnect < 0)
        && (i4TestValPppExtLinkCallBackConfigTimeBeforeDisconnect >
            MAX_INTEGER))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhTestv2PppExtLinkCallBackConfigReDialTime 
 Input       :  The Indices 
                PppExtLinkCallBackConfigIfIndex 
 
                The Object  
                testValPppExtLinkCallBackConfigReDialTime 
 Output      :  The Test Low Lev Routine Take the Indices & 
                Test whether that Value is Valid Input for Set. 
                Stores the value of error code in the Return val 
 Error Codes :  The following error codes are to be returned 
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905) 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhTestv2PppExtLinkCallBackConfigReDialTime (UINT4 *pu4ErrorCode,
                                             INT4
                                             i4PppExtLinkCallBackConfigIfIndex,
                                             INT4
                                             i4TestValPppExtLinkCallBackConfigReDialTime)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetIfPtr (i4PppExtLinkCallBackConfigIfIndex)) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((i4TestValPppExtLinkCallBackConfigReDialTime < 0)
        && (i4TestValPppExtLinkCallBackConfigReDialTime > MAX_INTEGER))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */

/**************************************************************************** 
 Function    :  nmhSetPppExtLinkCallBackConfigLocToRemOperation 
 Input       :  The Indices 
                PppExtLinkCallBackConfigIfIndex 
 
                The Object  
                setValPppExtLinkCallBackConfigLocToRemOperation 
 Output      :  The Set Low Lev Routine Take the Indices & 
                Sets the Value accordingly. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhSetPppExtLinkCallBackConfigLocToRemOperation (INT4
                                                 i4PppExtLinkCallBackConfigIfIndex,
                                                 INT4
                                                 i4SetValPppExtLinkCallBackConfigLocToRemOperation)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr (i4PppExtLinkCallBackConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    pIf->LcpOptionsDesired.LCPCallBackInfo.CallBackOperation =
        (UINT1) i4SetValPppExtLinkCallBackConfigLocToRemOperation;
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhSetPppExtLinkCallBackConfigRemToLocOperation 
 Input       :  The Indices 
                PppExtLinkCallBackConfigIfIndex 
 
                The Object  
                setValPppExtLinkCallBackConfigRemToLocOperation 
 Output      :  The Set Low Lev Routine Take the Indices & 
                Sets the Value accordingly. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhSetPppExtLinkCallBackConfigRemToLocOperation (INT4
                                                 i4PppExtLinkCallBackConfigIfIndex,
                                                 INT4
                                                 i4SetValPppExtLinkCallBackConfigRemToLocOperation)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr (i4PppExtLinkCallBackConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    pIf->LcpOptionsAllowedForPeer.LCPCallBackInfo.CallBackOperation =
        (UINT1) i4SetValPppExtLinkCallBackConfigRemToLocOperation;
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhSetPppExtLinkCallBackConfigLocToRemMessage 
 Input       :  The Indices 
                PppExtLinkCallBackConfigIfIndex 
 
                The Object  
                setValPppExtLinkCallBackConfigLocToRemMessage 
 Output      :  The Set Low Lev Routine Take the Indices & 
                Sets the Value accordingly. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhSetPppExtLinkCallBackConfigLocToRemMessage (INT4
                                               i4PppExtLinkCallBackConfigIfIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pSetValPppExtLinkCallBackConfigLocToRemMessage)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr (i4PppExtLinkCallBackConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    if ((pIf->LcpOptionsDesired.LCPCallBackInfo.pCallBackMsg =
         ALLOC_STR (pSetValPppExtLinkCallBackConfigLocToRemMessage->
                    i4_Length)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    MEMCPY (pIf->LcpOptionsDesired.LCPCallBackInfo.pCallBackMsg,
            pSetValPppExtLinkCallBackConfigLocToRemMessage->pu1_OctetList,
            pSetValPppExtLinkCallBackConfigLocToRemMessage->i4_Length);
    pIf->LcpOptionsDesired.LCPCallBackInfo.CallBackMsgLen =
        (UINT2) (pSetValPppExtLinkCallBackConfigLocToRemMessage->i4_Length);
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhSetPppExtLinkCallBackConfigLocToRemNeg 
 Input       :  The Indices 
                PppExtLinkCallBackConfigIfIndex 
 
                The Object  
                setValPppExtLinkCallBackConfigLocToRemNeg 
 Output      :  The Set Low Lev Routine Take the Indices & 
                Sets the Value accordingly. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhSetPppExtLinkCallBackConfigLocToRemNeg (INT4
                                           i4PppExtLinkCallBackConfigIfIndex,
                                           INT4
                                           i4SetValPppExtLinkCallBackConfigLocToRemNeg)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr (i4PppExtLinkCallBackConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    if (i4SetValPppExtLinkCallBackConfigLocToRemNeg == ENABLE)

    {
        pIf->LcpGSEM.pNegFlagsPerIf[CALL_BACK_IDX].FlagMask |= DESIRED_SET;
    }

    else

    {
        pIf->LcpGSEM.pNegFlagsPerIf[CALL_BACK_IDX].FlagMask &= DESIRED_NOT_SET;
    }
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhSetPppExtLinkCallBackConfigRemToLocNeg 
 Input       :  The Indices 
                PppExtLinkCallBackConfigIfIndex 
 
                The Object  
                setValPppExtLinkCallBackConfigRemToLocNeg 
 Output      :  The Set Low Lev Routine Take the Indices & 
                Sets the Value accordingly. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhSetPppExtLinkCallBackConfigRemToLocNeg (INT4
                                           i4PppExtLinkCallBackConfigIfIndex,
                                           INT4
                                           i4SetValPppExtLinkCallBackConfigRemToLocNeg)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr (i4PppExtLinkCallBackConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    if (i4SetValPppExtLinkCallBackConfigRemToLocNeg == ENABLE)

    {
        pIf->LcpGSEM.pNegFlagsPerIf[CALL_BACK_IDX].FlagMask |=
            ALLOWED_FOR_PEER_SET;
    }

    else

    {
        pIf->LcpGSEM.pNegFlagsPerIf[CALL_BACK_IDX].FlagMask &=
            ALLOWED_FOR_PEER_NOT_SET;
    }
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhSetPppExtLinkCallBackConfigTimeBeforeDisconnect 
 Input       :  The Indices 
                PppExtLinkCallBackConfigIfIndex 
 
                The Object  
                setValPppExtLinkCallBackConfigTimeBeforeDisconnect 
 Output      :  The Set Low Lev Routine Take the Indices & 
                Sets the Value accordingly. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhSetPppExtLinkCallBackConfigTimeBeforeDisconnect (INT4
                                                    i4PppExtLinkCallBackConfigIfIndex,
                                                    INT4
                                                    i4SetValPppExtLinkCallBackConfigTimeBeforeDisconnect)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr (i4PppExtLinkCallBackConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    pIf->CallBackTimeInfo.TimeBeforeDisconnect =
        (UINT2) i4SetValPppExtLinkCallBackConfigTimeBeforeDisconnect;
    return SNMP_SUCCESS;
}

/**************************************************************************** 
 Function    :  nmhSetPppExtLinkCallBackConfigReDialTime 
 Input       :  The Indices 
                PppExtLinkCallBackConfigIfIndex 
 
                The Object  
                setValPppExtLinkCallBackConfigReDialTime 
 Output      :  The Set Low Lev Routine Take the Indices & 
                Sets the Value accordingly. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhSetPppExtLinkCallBackConfigReDialTime (INT4
                                          i4PppExtLinkCallBackConfigIfIndex,
                                          INT4
                                          i4SetValPppExtLinkCallBackConfigReDialTime)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr (i4PppExtLinkCallBackConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    pIf->CallBackTimeInfo.TimeToDialAfterDisconnect =
        (UINT2) i4SetValPppExtLinkCallBackConfigReDialTime;
    return SNMP_SUCCESS;
}

/**************************************************************************** 
 Function    :  nmhTestv2PppExtLinkConfigInternationOption 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                testValPppExtLinkConfigInternationOption 
 Output      :  The Test Low Lev Routine Take the Indices & 
                Test whether that Value is Valid Input for Set. 
                Stores the value of error code in the Return val 
 Error Codes :  The following error codes are to be returned 
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905) 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhTestv2PppExtLinkConfigInternationOption (UINT4 *pu4ErrorCode,
                                            INT4 i4PppExtLinkConfigIfIndex,
                                            INT4
                                            i4TestValPppExtLinkConfigInternationOption)
{

    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetIfPtr (i4PppExtLinkConfigIfIndex)) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    switch (i4TestValPppExtLinkConfigInternationOption)

    {
        case LCP_INTERNATION_OPT_ENABLED:
        case LCP_INTERNATION_OPT_DISABLED:
            return SNMP_SUCCESS;
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
}
#endif /* PPP_STACK_WANTED */
/**************************************************************************** 
 Function    :  nmhTestv2PppExtLinkConfigLowerIfType 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                testValPppExtLinkConfigLowerIfType 
 Output      :  The Test Low Lev Routine Take the Indices & 
                Test whether that Value is Valid Input for Set. 
                Stores the value of error code in the Return val 
 Error Codes :  The following error codes are to be returned 
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905) 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhTestv2PppExtLinkConfigLowerIfType (UINT4 *pu4ErrorCode,
                                      INT4 i4PppExtLinkConfigIfIndex,
                                      INT4 i4TestValPppExtLinkConfigLowerIfType)
{
    tPPPIf             *pIf = NULL;

    if (NULL == (pIf = PppGetPppIfPtr ((UINT4) i4PppExtLinkConfigIfIndex)))

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
    if (pIf->LcpStatus.AdminStatus != STATUS_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }
    switch (i4TestValPppExtLinkConfigLowerIfType)

    {
        case SERIAL_IF_TYPE:
        case FR_IF_TYPE:
        case X25_IF_TYPE:
        case ISDN_IF_TYPE:
        case ATM_IF_TYPE:
        case PPP_OE_IFTYPE:
        case ASYNC_IF_TYPE:
            return SNMP_SUCCESS;
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
}

#ifdef PPP_STACK_WANTED
/**************************************************************************** 
 Function    :  nmhTestv2PppExtLinkConfigLowerIfDlci 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                testValPppExtLinkConfigLowerIfDlci 
 Output      :  The Test Low Lev Routine Take the Indices & 
                Test whether that Value is Valid Input for Set. 
                Stores the value of error code in the Return val 
 Error Codes :  The following error codes are to be returned 
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905) 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhTestv2PppExtLinkConfigLowerIfDlci (UINT4 *pu4ErrorCode,
                                      INT4 i4PppExtLinkConfigIfIndex,
                                      INT4 i4TestValPppExtLinkConfigLowerIfDlci)
{

    PPP_UNUSED (*pu4ErrorCode);
    PPP_UNUSED (i4PppExtLinkConfigIfIndex);
    PPP_UNUSED (i4TestValPppExtLinkConfigLowerIfDlci);

    return SNMP_SUCCESS;
}
#endif /* PPP_STACK_WANTED */
/**************************************************************************** 
 Function    :  nmhTestv2PppExtLinkConfigHostName 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                testValPppExtLinkConfigHostName 
 Output      :  The Test Low Lev Routine Take the Indices & 
                Test whether that Value is Valid Input for Set. 
                Stores the value of error code in the Return val 
 Error Codes :  The following error codes are to be returned 
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905) 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhTestv2PppExtLinkConfigHostName (UINT4 *pu4ErrorCode,
                                   INT4 i4PppExtLinkConfigIfIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValPppExtLinkConfigHostName)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr ((UINT4) i4PppExtLinkConfigIfIndex)) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        PPP_UNUSED (pIf);
        return SNMP_FAILURE;
    }
#ifdef SNMP_2_WANTED
    if (OSIX_FAILURE ==
        SNMPCheckForNVTChars (pTestValPppExtLinkConfigHostName->pu1_OctetList,
                              pTestValPppExtLinkConfigHostName->i4_Length))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif
    if (pTestValPppExtLinkConfigHostName->i4_Length >=
        (PPP_MAX_NAME_LENGTH - 1))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

#ifdef PPP_STACK_WANTED
/**************************************************************************** 
 Function    :  nmhTestv2PppExtLinkConfigScrambleEnable 
 Input       :  The Indices 
                PppExtLinkConfigIfIndex 
 
                The Object  
                testValPppExtLinkConfigScrambleEnable 
 Output      :  The Test Low Lev Routine Take the Indices & 
                Test whether that Value is Valid Input for Set. 
                Stores the value of error code in the Return val 
 Error Codes :  The following error codes are to be returned 
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905) 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhTestv2PppExtLinkConfigScrambleEnable (UINT4 *pu4ErrorCode,
                                         INT4 i4PppExtLinkConfigIfIndex,
                                         INT4
                                         i4TestValPppExtLinkConfigScrambleEnable)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr (i4PppExtLinkConfigIfIndex)) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pIf->LinkInfo.IfID.IfType != SONET_IF_TYPE)
    {

        *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
        return SNMP_FAILURE;
    }
    if ((i4TestValPppExtLinkConfigScrambleEnable != 1)
        && (i4TestValPppExtLinkConfigScrambleEnable != 2))

    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhGetPppExtLinkCallBackStatusRemToLocMessage 
 Input       :  The Indices 
                PppExtLinkCallBackStatusIfIndex 
 
                The Object  
                retValPppExtLinkCallBackStatusRemToLocMessage 
 Output      :  The Get Low Lev Routine Take the Indices & 
                store the Value requested in the Return val. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhGetPppExtLinkCallBackStatusRemToLocMessage (INT4
                                               i4PppExtLinkCallBackStatusIfIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pRetValPppExtLinkCallBackStatusRemToLocMessage)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr (i4PppExtLinkCallBackStatusIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    pRetValPppExtLinkCallBackStatusRemToLocMessage->i4_Length =
        pIf->LcpOptionsAckedByLocal.LCPCallBackInfo.CallBackMsgLen;
    MEMCPY (pRetValPppExtLinkCallBackStatusRemToLocMessage->pu1_OctetList,
            pIf->LcpOptionsAckedByLocal.LCPCallBackInfo.pCallBackMsg,
            pRetValPppExtLinkCallBackStatusRemToLocMessage->i4_Length);
    return (SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */

/**************************************************************************** 
 Function    :  nmhGetPppExtLinkPadConfigPadOption 
 Input       :  The Indices 
                PppExtLinkPadConfigIfIndex 
 
                The Object  
                retValPppExtLinkPadConfigPadOption 
 Output      :  The Get Low Lev Routine Take the Indices & 
                store the Value requested in the Return val. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhGetPppExtLinkPadConfigPadOption (INT4 i4PppExtLinkPadConfigIfIndex,
                                    INT4 *pi4RetValPppExtLinkPadConfigPadOption)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr (i4PppExtLinkPadConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    if (pIf->LcpGSEM.pNegFlagsPerIf[PADDING_IDX].FlagMask & DESIRED_MASK)

    {
        *pi4RetValPppExtLinkPadConfigPadOption = ENABLE;
    }

    else

    {
        *pi4RetValPppExtLinkPadConfigPadOption = DISABLE;
    }
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhGetPppExtLinkPadConfigMaxPadValue 
 Input       :  The Indices 
                PppExtLinkPadConfigIfIndex 
 
                The Object  
                retValPppExtLinkPadConfigMaxPadValue 
 Output      :  The Get Low Lev Routine Take the Indices & 
                store the Value requested in the Return val. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhGetPppExtLinkPadConfigMaxPadValue (INT4 i4PppExtLinkPadConfigIfIndex,
                                      INT4
                                      *pi4RetValPppExtLinkPadConfigMaxPadValue)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr (i4PppExtLinkPadConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppExtLinkPadConfigMaxPadValue = pIf->LcpOptionsDesired.MaxPadVal;
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhGetPppExtLinkPadConfigForceReqFlag 
 Input       :  The Indices 
                PppExtLinkPadConfigIfIndex 
 
                The Object  
                retValPppExtLinkPadConfigForceReqFlag 
 Output      :  The Get Low Lev Routine Take the Indices & 
                store the Value requested in the Return val. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhGetPppExtLinkPadConfigForceReqFlag (INT4 i4PppExtLinkPadConfigIfIndex,
                                       INT4
                                       *pi4RetValPppExtLinkPadConfigForceReqFlag)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr (i4PppExtLinkPadConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    if (pIf->LcpGSEM.pNegFlagsPerIf[PADDING_IDX].
        FlagMask & FORCED_TO_REQUEST_MASK)

    {
        *pi4RetValPppExtLinkPadConfigForceReqFlag = ENABLE;
    }

    else

    {
        *pi4RetValPppExtLinkPadConfigForceReqFlag = DISABLE;
    }
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhGetPppExtLinkPadConfigAllowPeerToSend 
 Input       :  The Indices 
                PppExtLinkPadConfigIfIndex 
 
                The Object  
                retValPppExtLinkPadConfigAllowPeerToSend 
 Output      :  The Get Low Lev Routine Take the Indices & 
                store the Value requested in the Return val. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhGetPppExtLinkPadConfigAllowPeerToSend (INT4 i4PppExtLinkPadConfigIfIndex,
                                          INT4
                                          *pi4RetValPppExtLinkPadConfigAllowPeerToSend)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr (i4PppExtLinkPadConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    if (pIf->LcpGSEM.pNegFlagsPerIf[PADDING_IDX].
        FlagMask & ALLOWED_FOR_PEER_MASK)

    {
        *pi4RetValPppExtLinkPadConfigAllowPeerToSend = ENABLE;
    }

    else

    {
        *pi4RetValPppExtLinkPadConfigAllowPeerToSend = DISABLE;
    }
    return (SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */

/**************************************************************************** 
 Function    :  nmhSetPppExtLinkPadConfigPadOption 
 Input       :  The Indices 
                PppExtLinkPadConfigIfIndex 
 
                The Object  
                setValPppExtLinkPadConfigPadOption 
 Output      :  The Set Low Lev Routine Take the Indices & 
                Sets the Value accordingly. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhSetPppExtLinkPadConfigPadOption (INT4 i4PppExtLinkPadConfigIfIndex,
                                    INT4 i4SetValPppExtLinkPadConfigPadOption)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr (i4PppExtLinkPadConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    if (i4SetValPppExtLinkPadConfigPadOption == ENABLE)

    {
        pIf->LcpGSEM.pNegFlagsPerIf[PADDING_IDX].FlagMask |= DESIRED_SET;
    }

    else

    {
        pIf->LcpGSEM.pNegFlagsPerIf[PADDING_IDX].FlagMask &= DESIRED_NOT_SET;
    }
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhSetPppExtLinkPadConfigMaxPadValue 
 Input       :  The Indices 
                PppExtLinkPadConfigIfIndex 
 
                The Object  
                setValPppExtLinkPadConfigMaxPadValue 
 Output      :  The Set Low Lev Routine Take the Indices & 
                Sets the Value accordingly. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhSetPppExtLinkPadConfigMaxPadValue (INT4 i4PppExtLinkPadConfigIfIndex,
                                      INT4
                                      i4SetValPppExtLinkPadConfigMaxPadValue)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr (i4PppExtLinkPadConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    pIf->LcpOptionsDesired.MaxPadVal =
        (UINT1) i4SetValPppExtLinkPadConfigMaxPadValue;
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhSetPppExtLinkPadConfigForceReqFlag 
 Input       :  The Indices 
                PppExtLinkPadConfigIfIndex 
 
                The Object  
                setValPppExtLinkPadConfigForceReqFlag 
 Output      :  The Set Low Lev Routine Take the Indices & 
                Sets the Value accordingly. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhSetPppExtLinkPadConfigForceReqFlag (INT4 i4PppExtLinkPadConfigIfIndex,
                                       INT4
                                       i4SetValPppExtLinkPadConfigForceReqFlag)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr (i4PppExtLinkPadConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    if (i4SetValPppExtLinkPadConfigForceReqFlag == ENABLE)

    {
        pIf->LcpGSEM.pNegFlagsPerIf[PADDING_IDX].FlagMask |=
            FORCED_TO_REQUEST_SET;
    }

    else

    {
        pIf->LcpGSEM.pNegFlagsPerIf[PADDING_IDX].FlagMask &=
            FORCED_TO_REQUEST_NOT_SET;
    }
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhSetPppExtLinkPadConfigAllowPeerToSend 
 Input       :  The Indices 
                PppExtLinkPadConfigIfIndex 
 
                The Object  
                setValPppExtLinkPadConfigAllowPeerToSend 
 Output      :  The Set Low Lev Routine Take the Indices & 
                Sets the Value accordingly. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhSetPppExtLinkPadConfigAllowPeerToSend (INT4 i4PppExtLinkPadConfigIfIndex,
                                          INT4
                                          i4SetValPppExtLinkPadConfigAllowPeerToSend)
{
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr (i4PppExtLinkPadConfigIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    if (i4SetValPppExtLinkPadConfigAllowPeerToSend == ENABLE)

    {
        pIf->LcpGSEM.pNegFlagsPerIf[PADDING_IDX].FlagMask |=
            ALLOWED_FOR_PEER_SET;
        pIf->LcpGSEM.pNegFlagsPerIf[PADDING_IDX].FlagMask |=
            ALLOW_FORCED_NAK_SET;
    }

    else

    {
        pIf->LcpGSEM.pNegFlagsPerIf[PADDING_IDX].FlagMask &=
            ALLOWED_FOR_PEER_NOT_SET;
        pIf->LcpGSEM.pNegFlagsPerIf[PADDING_IDX].FlagMask &=
            ALLOW_FORCED_NAK_NOT_SET;
    }
    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/**************************************************************************** 
 Function    :  nmhTestv2PppExtLinkPadConfigPadOption 
 Input       :  The Indices 
                PppExtLinkPadConfigIfIndex 
 
                The Object  
                testValPppExtLinkPadConfigPadOption 
 Output      :  The Test Low Lev Routine Take the Indices & 
                Test whether that Value is Valid Input for Set. 
                Stores the value of error code in the Return val 
 Error Codes :  The following error codes are to be returned 
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905) 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhTestv2PppExtLinkPadConfigPadOption (UINT4 *pu4ErrorCode,
                                       INT4 i4PppExtLinkPadConfigIfIndex,
                                       INT4
                                       i4TestValPppExtLinkPadConfigPadOption)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetIfPtr (i4PppExtLinkPadConfigIfIndex)) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((i4TestValPppExtLinkPadConfigPadOption != ENABLE)
        && (i4TestValPppExtLinkPadConfigPadOption != DISABLE))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhTestv2PppExtLinkPadConfigMaxPadValue 
 Input       :  The Indices 
                PppExtLinkPadConfigIfIndex 
 
                The Object  
                testValPppExtLinkPadConfigMaxPadValue 
 Output      :  The Test Low Lev Routine Take the Indices & 
                Test whether that Value is Valid Input for Set. 
                Stores the value of error code in the Return val 
 Error Codes :  The following error codes are to be returned 
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905) 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhTestv2PppExtLinkPadConfigMaxPadValue (UINT4 *pu4ErrorCode,
                                         INT4 i4PppExtLinkPadConfigIfIndex,
                                         INT4
                                         i4TestValPppExtLinkPadConfigMaxPadValue)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetIfPtr (i4PppExtLinkPadConfigIfIndex)) == NULL)

    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((i4TestValPppExtLinkPadConfigMaxPadValue <= 0)
        || (i4TestValPppExtLinkPadConfigMaxPadValue >= 255))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : PppExtLinkStatus. */
/**************************************************************************** 
 Function    :  nmhGetPppExtLinkStatusLocalLinkDiscrValue 
 Input       :  The Indices 
                PppExtLinkStatusIfIndex 
 
                The Object  
                retValPppExtLinkStatusLocalLinkDiscrValue 
 Output      :  The Get Low Lev Routine Take the Indices & 
                store the Value requested in the Return val. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhGetPppExtLinkStatusLocalLinkDiscrValue (INT4 i4PppExtLinkStatusIfIndex,
                                           INT4
                                           *pi4RetValPppExtLinkStatusLocalLinkDiscrValue)
{

#ifdef BAP
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr (i4PppExtLinkStatusIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppExtLinkStatusLocalLinkDiscrValue =
        pIf->LcpOptionsAckedByPeer.LinkDiscriminator;
#else
    UNUSED_PARAM (i4PppExtLinkStatusIfIndex);
    UNUSED_PARAM (pi4RetValPppExtLinkStatusLocalLinkDiscrValue);

#endif /*   */
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhGetPppExtLinkStatusRemoteLinkDiscrValue 
 Input       :  The Indices 
                PppExtLinkStatusIfIndex 
 
                The Object  
                retValPppExtLinkStatusRemoteLinkDiscrValue 
 Output      :  The Get Low Lev Routine Take the Indices & 
                store the Value requested in the Return val. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhGetPppExtLinkStatusRemoteLinkDiscrValue (INT4 i4PppExtLinkStatusIfIndex,
                                            INT4
                                            *pi4RetValPppExtLinkStatusRemoteLinkDiscrValue)
{

#ifdef BAP
    tPPPIf             *pIf = NULL;
    if ((pIf = PppGetPppIfPtr (i4PppExtLinkStatusIfIndex)) == NULL)

    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppExtLinkStatusRemoteLinkDiscrValue =
        pIf->LcpOptionsAckedByLocal.LinkDiscriminator;
#else
    UNUSED_PARAM (i4PppExtLinkStatusIfIndex);
    UNUSED_PARAM (pi4RetValPppExtLinkStatusRemoteLinkDiscrValue);

#endif /*   */
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPppExtLinkStatusBadAddresses
 Input       :  The Indices
                i4IfIndex

                The Object 
                retValPppExtLinkStatusBadAddresses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtLinkStatusBadAddresses (INT4 i4IfIndex, tSNMP_COUNTER64_TYPE
                                    * pu8RetValPppExtLinkStatusBadAddresses)
{

    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr (i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pIf->LcpStatus.OperStatus == STATUS_DOWN)
    {
        return (SNMP_FAILURE);
    }

    BYTE8_TO_TWO_BYTE4_CONV (pu8RetValPppExtLinkStatusBadAddresses,
                             pIf->LLHCounters.BadAddressCounter);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppExtLinkStatusBadControls
 Input       :  The Indices
                i4IfIndex

                The Object 
                retValPppExtLinkStatusBadControls
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtLinkStatusBadControls (INT4 i4IfIndex, tSNMP_COUNTER64_TYPE
                                   * pu8RetValPppExtLinkStatusBadControls)
{

    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr (i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pIf->LcpStatus.OperStatus == STATUS_DOWN)
    {
        return (SNMP_FAILURE);
    }

    BYTE8_TO_TWO_BYTE4_CONV (pu8RetValPppExtLinkStatusBadControls,
                             pIf->LLHCounters.BadControlCounter);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPppExtLinkStatusPacketTooLongs
 Input       :  The Indices
                i4IfIndex

                The Object 
                retValPppExtLinkStatusPacketTooLongs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtLinkStatusPacketTooLongs (INT4 i4IfIndex, tSNMP_COUNTER64_TYPE
                                      * pu8RetValPppExtLinkStatusPacketTooLongs)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr (i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pIf->LcpStatus.OperStatus == STATUS_DOWN)
    {
        return (SNMP_FAILURE);
    }

    BYTE8_TO_TWO_BYTE4_CONV (pu8RetValPppExtLinkStatusPacketTooLongs,
                             pIf->LLHCounters.PacketTooLongCounter);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppExtLinkStatusBadFCSs
 Input       :  The Indices
                i4IfIndex

                The Object 
                retValPppExtLinkStatusBadFCSs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtLinkStatusBadFCSs (INT4 i4IfIndex, tSNMP_COUNTER64_TYPE
                               * pu8RetValPppExtLinkStatusBadFCSs)
{

    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetPppIfPtr (i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pIf->LcpStatus.OperStatus == STATUS_DOWN)
    {
        return (SNMP_FAILURE);
    }
    BYTE8_TO_TWO_BYTE4_CONV (pu8RetValPppExtLinkStatusBadFCSs,
                             pIf->LLHCounters.BadFcsCounter);
    return (SNMP_SUCCESS);

}

/**************************************************************************** 
 Function    :  nmhValidateIndexInstancePppExtLinkStatusTable 
 Input       :  The Indices 
                PppExtLinkStatusIfIndex 
 Output      :  The Routines Validates the Given Indices. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppExtLinkStatusTable (INT4 i4PppExtLinkStatusIfIndex)
{
    if (PppGetPppIfPtr (i4PppExtLinkStatusIfIndex) == NULL)

    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhGetFirstIndexPppExtLinkStatusTable 
 Input       :  The Indices 
                PppExtLinkStatusIfIndex 
 Output      :  The Get First Routines gets the Lexicographicaly 
                First Entry from the Table. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppExtLinkStatusTable (INT4 *pi4PppExtLinkStatusIfIndex)
{
    if (nmhGetFirstIndex (pi4PppExtLinkStatusIfIndex, LinkInstance, FIRST_INST)
        == SNMP_SUCCESS)

    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/**************************************************************************** 
 Function    :  nmhGetNextIndexPppExtLinkStatusTable 
 Input       :  The Indices 
                PppExtLinkStatusIfIndex 
                nextPppExtLinkStatusIfIndex 
 Output      :  The Get Next function gets the Next Index for 
                the Index Value given in the Index Values. The 
                Indices are stored in the next_varname variables. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppExtLinkStatusTable (INT4 i4PppExtLinkStatusIfIndex,
                                      INT4 *pi4NextPppExtLinkStatusIfIndex)
{
    if (nmhGetNextIndex (&i4PppExtLinkStatusIfIndex, LinkInstance, NEXT_INST)
        == SNMP_SUCCESS)

    {
        *pi4NextPppExtLinkStatusIfIndex = i4PppExtLinkStatusIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/**************************************************************************** 
 Function    :  nmhValidateIndexInstancePppExtLinkCallBackConfigTable 
 Input       :  The Indices 
                PppExtLinkCallBackConfigIfIndex 
 Output      :  The Routines Validates the Given Indices. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppExtLinkCallBackConfigTable (INT4
                                                       i4PppExtLinkCallBackConfigIfIndex)
{
    if (PppGetPppIfPtr (i4PppExtLinkCallBackConfigIfIndex) == NULL)

    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhGetFirstIndexPppExtLinkCallBackConfigTable 
 Input       :  The Indices 
                PppExtLinkCallBackConfigIfIndex 
 Output      :  The Get First Routines gets the Lexicographicaly 
                First Entry from the Table. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppExtLinkCallBackConfigTable (INT4
                                               *pi4PppExtLinkCallBackConfigIfIndex)
{
    if (nmhGetFirstIndex
        (pi4PppExtLinkCallBackConfigIfIndex, LinkInstance,
         FIRST_INST) == SNMP_SUCCESS)

    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/**************************************************************************** 
 Function    :  nmhGetNextIndexPppExtLinkCallBackConfigTable 
 Input       :  The Indices 
                PppExtLinkCallBackConfigIfIndex 
                nextPppExtLinkCallBackConfigIfIndex 
 Output      :  The Get Next function gets the Next Index for 
                the Index Value given in the Index Values. The 
                Indices are stored in the next_varname variables. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppExtLinkCallBackConfigTable (INT4
                                              i4PppExtLinkCallBackConfigIfIndex,
                                              INT4
                                              *pi4NextPppExtLinkCallBackConfigIfIndex)
{
    if (nmhGetNextIndex
        (&i4PppExtLinkCallBackConfigIfIndex, LinkInstance,
         NEXT_INST) == SNMP_SUCCESS)

    {
        *pi4NextPppExtLinkCallBackConfigIfIndex =
            i4PppExtLinkCallBackConfigIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* LOW LEVEL Routines for Table : PppExtLinkCallBackStatusTable. */

/**************************************************************************** 
 Function    :  nmhValidateIndexInstancePppExtLinkCallBackStatusTable 
 Input       :  The Indices 
                PppExtLinkCallBackStatusIfIndex 
 Output      :  The Routines Validates the Given Indices. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppExtLinkCallBackStatusTable (INT4
                                                       i4PppExtLinkCallBackStatusIfIndex)
{
    if (PppGetPppIfPtr (i4PppExtLinkCallBackStatusIfIndex) == NULL)

    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhGetFirstIndexPppExtLinkCallBackStatusTable 
 Input       :  The Indices 
                PppExtLinkCallBackStatusIfIndex 
 Output      :  The Get First Routines gets the Lexicographicaly 
                First Entry from the Table. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppExtLinkCallBackStatusTable (INT4
                                               *pi4PppExtLinkCallBackStatusIfIndex)
{
    if (nmhGetFirstIndex
        (pi4PppExtLinkCallBackStatusIfIndex, LinkInstance,
         FIRST_INST) == SNMP_SUCCESS)

    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/**************************************************************************** 
 Function    :  nmhGetNextIndexPppExtLinkCallBackStatusTable 
 Input       :  The Indices 
                PppExtLinkCallBackStatusIfIndex 
                nextPppExtLinkCallBackStatusIfIndex 
 Output      :  The Get Next function gets the Next Index for 
                the Index Value given in the Index Values. The 
                Indices are stored in the next_varname variables. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppExtLinkCallBackStatusTable (INT4
                                              i4PppExtLinkCallBackStatusIfIndex,
                                              INT4
                                              *pi4NextPppExtLinkCallBackStatusIfIndex)
{
    if (nmhGetNextIndex
        (&i4PppExtLinkCallBackStatusIfIndex, LinkInstance,
         NEXT_INST) == SNMP_SUCCESS)

    {
        *pi4NextPppExtLinkCallBackStatusIfIndex =
            i4PppExtLinkCallBackStatusIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* LOW LEVEL Routines for Table : PppExtLinkPadConfigTable. */

/****************************************************************************
 Function    :  nmhGetPppExtLinkCallBackStatusLocToRemMessage
 Input       :  The Indices
                PppExtLinkCallBackStatusIfIndex

                The Object 
                retValPppExtLinkCallBackStatusLocToRemMessage
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtLinkCallBackStatusLocToRemMessage (INT4
                                               i4PppExtLinkCallBackStatusIfIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pRetValPppExtLinkCallBackStatusLocToRemMessage)
{
    return (nmhGetPppExtLinkCallBackConfigLocToRemMessage
            (i4PppExtLinkCallBackStatusIfIndex,
             pRetValPppExtLinkCallBackStatusLocToRemMessage));
}

/**************************************************************************** 
 Function    :  nmhValidateIndexInstancePppExtLinkPadConfigTable 
 Input       :  The Indices 
                PppExtLinkPadConfigIfIndex 
 Output      :  The Routines Validates the Given Indices. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppExtLinkPadConfigTable (INT4
                                                  i4PppExtLinkPadConfigIfIndex)
{
    if (PppGetPppIfPtr (i4PppExtLinkPadConfigIfIndex) == NULL)

    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhGetFirstIndexPppExtLinkPadConfigTable 
 Input       :  The Indices 
                PppExtLinkPadConfigIfIndex 
 Output      :  The Get First Routines gets the Lexicographicaly 
                First Entry from the Table. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppExtLinkPadConfigTable (INT4 *pi4PppExtLinkPadConfigIfIndex)
{
    if (nmhGetFirstIndex
        (pi4PppExtLinkPadConfigIfIndex, LinkInstance,
         FIRST_INST) == SNMP_SUCCESS)

    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/**************************************************************************** 
 Function    :  nmhGetNextIndexPppExtLinkPadConfigTable 
 Input       :  The Indices 
                PppExtLinkPadConfigIfIndex 
                nextPppExtLinkPadConfigIfIndex 
 Output      :  The Get Next function gets the Next Index for 
                the Index Value given in the Index Values. The 
                Indices are stored in the next_varname variables. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppExtLinkPadConfigTable (INT4 i4PppExtLinkPadConfigIfIndex,
                                         INT4
                                         *pi4NextPppExtLinkPadConfigIfIndex)
{
    if (nmhGetNextIndex
        (&i4PppExtLinkPadConfigIfIndex, LinkInstance,
         NEXT_INST) == SNMP_SUCCESS)

    {
        *pi4NextPppExtLinkPadConfigIfIndex = i4PppExtLinkPadConfigIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/**************************************************************************** 
 Function    :  nmhTestv2PppExtLinkPadConfigForceReqFlag 
 Input       :  The Indices 
                PppExtLinkPadConfigIfIndex 
 
                The Object  
                testValPppExtLinkPadConfigForceReqFlag 
 Output      :  The Test Low Lev Routine Take the Indices & 
                Test whether that Value is Valid Input for Set. 
                Stores the value of error code in the Return val 
 Error Codes :  The following error codes are to be returned 
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905) 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhTestv2PppExtLinkPadConfigForceReqFlag (UINT4 *pu4ErrorCode,
                                          INT4 i4PppExtLinkPadConfigIfIndex,
                                          INT4
                                          i4TestValPppExtLinkPadConfigForceReqFlag)
{
    PPP_UNUSED (i4PppExtLinkPadConfigIfIndex);
    PPP_UNUSED (i4TestValPppExtLinkPadConfigForceReqFlag);
    PPP_UNUSED (pu4ErrorCode);
    return (SNMP_SUCCESS);
}

/**************************************************************************** 
 Function    :  nmhTestv2PppExtLinkPadConfigAllowPeerToSend 
 Input       :  The Indices 
                PppExtLinkPadConfigIfIndex 
 
                The Object  
                testValPppExtLinkPadConfigAllowPeerToSend 
 Output      :  The Test Low Lev Routine Take the Indices & 
                Test whether that Value is Valid Input for Set. 
                Stores the value of error code in the Return val 
 Error Codes :  The following error codes are to be returned 
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905) 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
nmhTestv2PppExtLinkPadConfigAllowPeerToSend (UINT4 *pu4ErrorCode,
                                             INT4 i4PppExtLinkPadConfigIfIndex,
                                             INT4
                                             i4TestValPppExtLinkPadConfigAllowPeerToSend)
{
    PPP_UNUSED (pu4ErrorCode);
    PPP_UNUSED (i4PppExtLinkPadConfigIfIndex);
    PPP_UNUSED (i4TestValPppExtLinkPadConfigAllowPeerToSend);
    return (SNMP_SUCCESS);
}

 /****************************************************************************
 Function    :  nmhGetPppExtLinkConfigLLFraming
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppExtLinkConfigLLFraming
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtLinkConfigLLFraming (INT4 i4IfIndex,
                                 INT4 *pi4RetValPppExtLinkConfigLLFraming)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetIfPtr (i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValPppExtLinkConfigLLFraming = pIf->LinkInfo.u1FrmFlag;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetPppExtLinkConfigLLFraming
 Input       :  The Indices
                IfIndex

                The Object 
                setValPppExtLinkConfigLLFraming
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtLinkConfigLLFraming (INT4 i4IfIndex,
                                 INT4 i4SetValPppExtLinkConfigLLFraming)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetIfPtr (i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIf->LinkInfo.u1FrmFlag = (UINT1) i4SetValPppExtLinkConfigLLFraming;
#ifdef HARDWARE_ENABLED
    PppLinkConfigFramingToDP (pIf);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PppExtLinkConfigLLFraming
 Input       :  The Indices
                IfIndex

                The Object 
                testValPppExtLinkConfigLLFraming
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtLinkConfigLLFraming (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                    INT4 i4TestValPppExtLinkConfigLLFraming)
{
    if (PppGetIfPtr (i4IfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValPppExtLinkConfigLLFraming != PPP_TRUE)
        && (i4TestValPppExtLinkConfigLLFraming != PPP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
#endif /* PPP_STACK_WANTED */

/****************************************************************************
  Function    :  nmhDepv2PppExtLinkConfigTable
  Input       :  The Indices
                 IfIndex
  Output      :  The Dependency Low Lev Routine Take the Indices &
                 check whether dependency is met or not.
                 Stores the value of error code in the Return val
  Error Codes :  The following error codes are to be returned
                 SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                 SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
                 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PppExtLinkConfigTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
#endif /*__PPP_PPPLNKEL_C__*/
