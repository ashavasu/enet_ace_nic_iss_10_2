#ifdef PPP_KEEP_ALIVE
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppkpal.c,v 1.2 2018/07/04 06:33:13 vijay Exp $
 *
 * Description:Contains Keep Alive part for PPP .
 *
 *******************************************************************/
#include "osxstd.h"
#include "srmbuf.h"
#include "osxsys.h"
#include "lr.h"
#include "pppdefs.h"
#include "pppsnmpm.h"
#include "lcpcom.h"
#include "snmccons.h"
#include "gsemdefs.h"
#include "llproto.h"
#include "globexts.h"
#include "ppplcp.h"
#include "pppkpal.h"
#include "ppteslow.h"
#include "ppptimer.h"
#include "pppdbgex.h"
#include "pppstubs.h"
#include "lcpdefs.h"
#include "lcpprot.h"
#include "genproto.h"
#include "ppptask.h"
#include "iss.h"

tOsixTaskId         gu4PppKpAlTaskId;	    /* Task ID */
tOsixSemId          gPppKpAlSemId;	    /* Sem ID */
tOsixQId            gPppKpAlInputQId;       /* Queue ID */


/***************************************************************************
 * Function Name    :  PppKpAlMain
 *
 * Description      :  This is the start-up function for PPP Keep Alive Handling.
 * 			Creates a Sem and waits in a loop for event
 *
 * Global Variables
 * Referred         :  gPppKpAlSemId
 *
 * Global Variables
 * Modified         :  None
 *
 * Input (s)        :  None
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

VOID
PppKpAlMain ()
{
	UINT4 u4Event = 0;

	if (OsixCreateSem (PPP_KPAL_SEM_NAME, 1, 0, &gPppKpAlSemId) != OSIX_SUCCESS)
	{
		PPP_TRC (OS_RESOURCE, "PppKpAlMain():- PPP Keep Alive Semaphore creation failed");
		PPP_INIT_COMPLETE (OSIX_FAILURE);
		return;
	}

	while (1)
	{
		if (OsixReceiveEvent ((PPP_SET_KPAL_EVENT | PPP_KPAL_LCP_UP_EVENT),
					OSIX_WAIT, (UINT4) 0,
					&u4Event) != OSIX_SUCCESS)
		{
			PPP_TRC (ALL_FAILURE, "OsixReceive Event Failure");
			continue;
		}

		PPP_KPAL_LOCK ();

		if ((u4Event & PPP_SET_KPAL_EVENT) != 0)
		{
			PppSetKpAlHandler ();
		}
		else if ((u4Event & PPP_KPAL_LCP_UP_EVENT) != 0)
		{
			PPPKpAlLCPUpHandler();
		}

		PPP_KPAL_UNLOCK ();

	}  
}

/***************************************************************************
 * Function Name    :  PPPKeepAliveInit
 *
 * Description      :  This is the main Function for PPP kepp alive handling.
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Input (s)        :  None
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/
VOID PPPKeepAliveInit (INT1 *pi1Param)
{
	UNUSED_PARAM (pi1Param);
	if (OsixTskIdSelf (&gu4PppKpAlTaskId) == OSIX_FAILURE)
	{
		PPP_TRC (OS_RESOURCE, "PPPKeepAliveInit:- OsixTskIdSelf failed");
		/* Indicate the status of initialization to the main routine */
		PPP_INIT_COMPLETE (OSIX_FAILURE);
		return;
	}

	if (PpppAlInitGlobals () == PPP_FAILURE)
	{
		PPP_TRC (OS_RESOURCE, "PPPKeepAliveInit:- Allocation of Global items failed");
		PPP_INIT_COMPLETE (OSIX_FAILURE);
		return;
	}

	PPP_INIT_COMPLETE (OSIX_SUCCESS);

	PppKpAlMain();
}

/***************************************************************************
 * Function Name    :  PpppAlInitGlobals
 *
 * Description      :  Function to init and initialize global variables
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Input (s)        :  None
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/
INT4 PpppAlInitGlobals ()
{

	if (OsixCreateQ (PPP_KPAL_QUEUE_NAME,
				PPP_QUEUE_DEPTH, OSIX_LOCAL, &gPppKpAlInputQId) != OSIX_SUCCESS)
	{
		PPP_TRC (ALL_FAILURE, "PpppAlInitGlobals:- Queue Creation Failure ");
		return PPP_FAILURE;
	}
	return PPP_SUCCESS;
}

/***************************************************************************
 * Function Name    :  PppSetKpAlHandler
 *
 * Description      :  Handles incoming events raised after 
 * 			setting Keep Alive timeout value
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Input (s)        :  None
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/
VOID PppSetKpAlHandler ()
{
    tPPPIf * pIf = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    INT4 i4SetValPppExtKeepAliveTimeOut=0;
    tPppKeepAliveMsg PppKpAlMsg;

    MEMSET (&PppKpAlMsg, 0, sizeof (tPppKeepAliveMsg));
    while (OsixReceiveFromQ (SELF, PPP_KPAL_QUEUE_NAME, OSIX_NO_WAIT, 0,
                             &pBuf) == OSIX_SUCCESS)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &PppKpAlMsg,
                                   0, sizeof (tPppKeepAliveMsg));

        pIf = PppKpAlMsg.pIf;
        i4SetValPppExtKeepAliveTimeOut = PppKpAlMsg.PppKpAlSetVal;

        if (pIf->LcpStatus.OperStatus == STATUS_DOWN)
	{
		pIf->EchoInfo.KeepAliveTimeOutVal =
			(UINT2) i4SetValPppExtKeepAliveTimeOut;
                RELEASE_BUFFER (pBuf);
		return;
	}

        if (i4SetValPppExtKeepAliveTimeOut != 0)
	{
		pIf->EchoInfo.KeepAliveTimeOutVal =
			(UINT2) i4SetValPppExtKeepAliveTimeOut;
		LCPProcessKeepAliveTimeOut (pIf);
		PPPRestartTimer (&pIf->LinkInfo.IdleTimer, LCP_IDLE_TIMER,
				(UINT4) (3 * pIf->EchoInfo.KeepAliveTimeOutVal));
	}
        else
	{
		pIf->EchoInfo.KeepAliveTimeOutVal = PPP_DEF_KEEPALIVE_TIME;
		LCPProcessKeepAliveTimeOut (pIf);
		PPPRestartTimer (&pIf->LinkInfo.IdleTimer, LCP_IDLE_TIMER,
				(UINT4) (3 * pIf->EchoInfo.KeepAliveTimeOutVal));
	}
        RELEASE_BUFFER (pBuf);
    }
}

/***************************************************************************
 * Function Name    :  PppKpAlLCPUpStartTimer
 *
 * Description      :  Handles incoming events raised after 
 * 			setting Keep Alive timeout value
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Input (s)        :  None
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/
VOID
PppKpAlLCPUpStartTimer (tPPPIf *pIf)
{
    tCRU_BUF_CHAIN_HEADER *pChainBuf = NULL;
    tPppKeepAliveMsg PppKpAlMsg;

    MEMSET (&PppKpAlMsg, 0, sizeof(tPppKeepAliveMsg));
    PppKpAlMsg.pIf = pIf;
    pChainBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tPppKeepAliveMsg), 0);
    if (pChainBuf == NULL)
    {
        PPP_TRC (ALL_FAILURE, "Can not allocate CRU buffer.");
        return;
    }

    CRU_BUF_Copy_OverBufChain (pChainBuf, (UINT1 *) &PppKpAlMsg,
            0, sizeof (tPppKeepAliveMsg));

    if (OsixSendToQ (SELF, PPP_KPAL_QUEUE_NAME, pChainBuf, OSIX_MSG_NORMAL) !=
            OSIX_SUCCESS)
    {
        PPP_TRC (ALL_FAILURE, "Send to Ppp Keep Alive Queue Failure.");
        RELEASE_BUFFER (pChainBuf);
        return;
    }

    if (OsixSendEvent (SELF, PPP_KPAL_TASK_NAME, PPP_KPAL_LCP_UP_EVENT) != OSIX_SUCCESS)
    {
        PPP_TRC (ALL_FAILURE, "Ppp Queue Event Failure.");
        return;
    }
}

/***************************************************************************
 * Function Name    :  PPPKpAlLCPUpHandler
 *
 * Description      :  Handles incoming events raised after 
 * 			LCP is UP.
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Input (s)        :  None
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/
VOID
PPPKpAlLCPUpHandler ()
{
    tPPPIf  *pIf = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tPppKeepAliveMsg PppKpAlMsg;

    MEMSET (&PppKpAlMsg, 0, sizeof (tPppKeepAliveMsg));
    while (OsixReceiveFromQ (SELF, PPP_KPAL_QUEUE_NAME, OSIX_NO_WAIT, 0,
                &pBuf) == OSIX_SUCCESS)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &PppKpAlMsg,
                0, sizeof (tPppKeepAliveMsg));
        pIf = PppKpAlMsg.pIf;

        PPPStartTimer (&pIf->LinkInfo.IdleTimer, LCP_IDLE_TIMER,
                (UINT4) (3 * pIf->EchoInfo.KeepAliveTimeOutVal));
        PPPRestartTimer (&pIf->EchoInfo.KeepAliveTimer, KEEP_ALIVE_TIMER,
                pIf->EchoInfo.KeepAliveTimeOutVal);
        RELEASE_BUFFER (pBuf);
    }
}

/* Keepalive time out processing functions */
/***************************************************************************
 * Function Name    :  LCPProcessKeepAliveTimeOut
 *
 * Description      :  LCP Process Keep Alive Handling
 *
 * Global Variables
 * Referred         :  None
 *
 * Global Variables
 * Modified         :  None
 *
 * Input (s)        :  None
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/
VOID
LCPProcessKeepAliveTimeOut (VOID *pPppIf)
{
    tPPPIf             *pIf = NULL;
    UINT1               u1val;
    UINT4               u4TempVal;

    pIf = (tPPPIf *) pPppIf;

    if (pIf->LcpStatus.LinkAvailability != LINK_NOT_AVAILABLE)
    {

        pIf->EchoInfo.EchoId++;

        u4TempVal = pIf->LcpOptionsAckedByLocal.MRU;
        pIf->LcpOptionsAckedByLocal.MRU = KEEPALIVE_ECHO_PKTSIZE;

        LCPSendRXRPkt (pIf, ECHO_REQ_CODE, pIf->EchoInfo.EchoId);
        pIf->LcpOptionsAckedByLocal.MRU = (UINT2) u4TempVal;

        /* Start timer  again */
        pIf->EchoInfo.EchoTimer.Param1 = (VOID *) (pIf);

        (pIf->LinkInfo.TimeOutTime >= pIf->EchoInfo.KeepAliveTimeOutVal) ?
            (u1val = (UINT1) (pIf->EchoInfo.KeepAliveTimeOutVal - 1)) :
            (u1val = (UINT1) (pIf->LinkInfo.TimeOutTime));

        pIf->EchoInfo.KeepAliveTimer.Param1 = (VOID *) (pIf);

        if (!u1val)
        {
            PPPRestartTimer (&pIf->EchoInfo.EchoTimer, ECHO_REQ_TIMER, 1);
            PPPRestartTimer (&pIf->EchoInfo.KeepAliveTimer, KEEP_ALIVE_TIMER,
                             (UINT4) (pIf->EchoInfo.KeepAliveTimeOutVal + 1));
        }
        else
        {
            PPPRestartTimer (&pIf->EchoInfo.EchoTimer, ECHO_REQ_TIMER, u1val);
            PPPRestartTimer (&pIf->EchoInfo.KeepAliveTimer, KEEP_ALIVE_TIMER,
                             pIf->EchoInfo.KeepAliveTimeOutVal);
        }
    }
}

/***********************************************************************
*  Function Name : LCPIdleTimeOut
*  Description   :
*       This function is called when the Idle timer expires. It generates
*   RXJ- event to make the link down.
*  Parameter(s)  :
*       pIf         - pointer to the PPP interface structure
*  Return Value  : VOID
*********************************************************************/
VOID
LCPIdleTimeOut (VOID *pPppIf)
{
    UINT4               DiffTime;
    tPPPIf             *pIf;
    pIf = (tPPPIf *) pPppIf;

    DiffTime =
        (pIf->LinkInfo.LastPktArrivalTime / SYS_NUM_OF_TIME_UNITS_IN_A_SEC) -
        (pIf->LinkInfo.StartingTime / SYS_NUM_OF_TIME_UNITS_IN_A_SEC);

    if (DiffTime == 0)
    {
#ifdef RADIUS_WANTED
        pIf->RadInfo.AcctTerminateCause = PPP_RAD_IDLE_TIMEOUT;
#endif

        GSEMRun (&pIf->LcpGSEM, RXJ_MINUS);

    }
    else
    {
        pIf->LinkInfo.StartingTime = pIf->LinkInfo.LastPktArrivalTime;
        PPPStartTimer (&pIf->LinkInfo.IdleTimer, LCP_IDLE_TIMER,
                       (UINT4) (pIf->EchoInfo.KeepAliveTimeOutVal * 3));
    }
    return;
}

/****************************************************************************
 Function    :  PPPChangeDefaultKeepAlive
 Parametes   :
		pIf - pointer to the interface structure
		setValPppExtKeepAliveTimeOut - Time to set Keep Alive Timer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
PPPChangeDefaultKeepAlive (tPPPIf *pIf, INT4 i4SetValPppExtKeepAliveTimeOut)   
{
    tCRU_BUF_CHAIN_HEADER *pChainBuf;
    tPppKeepAliveMsg PppKpAlMsg;
    
    MEMSET (&PppKpAlMsg, 0, sizeof(tPppKeepAliveMsg));
    
    PppKpAlMsg.pIf = pIf;
    PppKpAlMsg.PppKpAlSetVal = i4SetValPppExtKeepAliveTimeOut;
    pChainBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tPppKeepAliveMsg), 0);
    if (pChainBuf == NULL)
	    return (SNMP_FAILURE);
    MEMSET (pChainBuf->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_Copy_OverBufChain (pChainBuf, (UINT1 *) &PppKpAlMsg,
				0, sizeof (tPppKeepAliveMsg));

    if (OsixSendToQ (SELF, PPP_KPAL_QUEUE_NAME, pChainBuf, OSIX_MSG_NORMAL) !=
        OSIX_SUCCESS)
    {
	    PPP_TRC (ALL_FAILURE, "Send to Ppp Keep Alive Queue Failure.");
	    RELEASE_BUFFER (pChainBuf);
	    return (SNMP_FAILURE);
    }

    if (OsixSendEvent (SELF, PPP_KPAL_TASK_NAME, PPP_SET_KPAL_EVENT) != OSIX_SUCCESS)
    {
	    PPP_TRC (ALL_FAILURE, "Ppp Queue Event Failure.");
	    return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/***********************************************************************/
/*              TIMER INTERFACE FUNCTIONS                              */
/***********************************************************************
*  Function Name : LCPEchoReqTimeOut
*  Description   :
*        This function is called when the retransmission timer expires after
*  the transmission of Echo request packet.
*  Parameter(s)  :
*       pIf         - pointer to the interface structure
*  Return Value  : VOID
*********************************************************************/
VOID
LCPEchoReqTimeOut (VOID *pPppIf)
{
    tPPPIf             *pIf;
    pIf = (tPPPIf *) pPppIf;

    LCPSendEchoReq (pIf, RETRANSMISSION);    /* Retransmit the Echo Req  */

    pIf->EchoInfo.EchoTransmits++;
    return;
}

/***********************************************************************
*  Function Name : LCPSendEchoReq
*  Description   :
*       This function is called to send a Echo request packet
*  Parameter(s)  :
*       pIf            - pointer to the interface structure
*       Retransmission - indicates whether the pkt is retransmitted
*  Return Value  : VOID
*********************************************************************/
VOID
LCPSendEchoReq (tPPPIf * pIf, UINT1 Flag)
{

    if (Flag == NORMAL_TRANSMISSION)
    {
        pIf->EchoInfo.EchoId++;
        pIf->EchoInfo.EchoTransmits = 0;
        if (OsixGetSysTime (&pIf->MPInfo.MemberInfo.LastTransitTicks) ==
            OSIX_FAILURE)
        {
            PPP_TRC (OS_RESOURCE, "Failure in  OsixGetSysTime()");
        }
    }

    if (pIf->LcpStatus.LinkAvailability != LINK_NOT_AVAILABLE)
    {
        LCPSendRXRPkt (pIf, ECHO_REQ_CODE, pIf->EchoInfo.EchoId);
        /* Start timer  again */
        pIf->EchoInfo.EchoTimer.Param1 = (VOID *) (pIf);
        PPPRestartTimer (&pIf->EchoInfo.EchoTimer, ECHO_REQ_TIMER,
                         pIf->LinkInfo.TimeOutTime);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : PppKpAlLock                                          */
/*                                                                           */                                                     
/* Description        : This function is used to take the PPP  protocol      */
/*                      SEMa4 to avoid simultaneous access to                */                                                               
/*                      PPP database                                         */
/* Input(s)           : None                                                 */
/*                                                                           */ 
/* Output(s)          : None                                                 */
/*                                                                           */                                                                       
/* Global Variables                                                          */
/* Referred           : gCfaSemId                                            */                                                                        
/*                                                                           */
/* Global Variables                                                          */                                                                 
/* Modified           : gCfaSemId                                            */
/*                                                                           */                                                                       
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */                                                                       
/*****************************************************************************/

INT4 PppKpAlLock (VOID)
{
	if (OsixSemTake (gPppKpAlSemId) != OSIX_SUCCESS)
	{
		return SNMP_FAILURE;
	}
	return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PppKpUnLock                                          */
/*                                                                           */
/* Description        : This function is used to give the PPP                */
/*                      SEMa4 to avoid simultaneous access to                */
/*                      PPP database                                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS                                         */
/*                                                                           */
/*****************************************************************************/
INT4
PppKpAlUnlock (VOID)
{
    OsixSemGive (gPppKpAlSemId);
    return SNMP_SUCCESS;
}


#endif /* PPP_KEEP_ALIVE */
