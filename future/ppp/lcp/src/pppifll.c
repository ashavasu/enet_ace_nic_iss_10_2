/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppifll.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the SNMP low level routines of
 *             pppMp group.
 *
 *******************************************************************/
#define __IF_LOW_H__
#include "lcpcom.h"
#include "snmphdrs.h"
#include "pppsnmpm.h"
#include "mpdefs.h"
#include "llproto.h"
#include "globexts.h"
#include "pppdpport.h"
#include "pppdpproto.h"

/* LOW LEVEL Routines for Table : PppIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppIfTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppIfTable (INT4 i4IfIndex)
{
    if (PppGetIfPtr (i4IfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppIfTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppIfTable (INT4 *pi4IfIndex)
{
    if (nmhGetFirstIndex (pi4IfIndex, LinkInstance, FIRST_INST) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppIfTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppIfTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    if (nmhGetNextIndex (&i4IfIndex, LinkInstance, NEXT_INST) == SNMP_SUCCESS)
    {
        *pi4NextIfIndex = i4IfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppIfRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppIfRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppIfRowStatus (INT4 i4IfIndex, INT4 *pi4RetValPppIfRowStatus)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetIfPtr (i4IfIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Index Found");
        return (SNMP_FAILURE);
    }
    *pi4RetValPppIfRowStatus = pIf->RowStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppIfRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                setValPppIfRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppIfRowStatus (INT4 i4IfIndex, INT4 i4SetValPppIfRowStatus)
{
    tPPPIf             *pIf;

    switch (i4SetValPppIfRowStatus)
    {
        case CREATE_AND_GO:
            if (PPPCreateIf ((UINT4) i4IfIndex) == PPP_FAILURE)
            {
                PPP_TRC (MGMT, "Creation of Interface Failed");
                return SNMP_FAILURE;
            }
            pIf = PppGetIfPtr (i4IfIndex);
            pIf->RowStatus = ACTIVE;
            break;
        case CREATE_AND_WAIT:
            if (PPPCreateIf ((UINT4) i4IfIndex) == PPP_FAILURE)
            {
                PPP_TRC (MGMT, "Creation of Interface Failed");
                return SNMP_FAILURE;
            }
            pIf = PppGetIfPtr (i4IfIndex);
            pIf->RowStatus = NOT_READY;
            break;

        case ACTIVE:
            pIf = PppGetIfPtr (i4IfIndex);
#ifdef HARDWARE_ENABLED
            if (PppCreatePppInterfaceToDP (pIf) == PPP_FAILURE)
            {
                return PPP_FAILURE;
            }
#endif
            pIf->RowStatus = ACTIVE;
            break;

        case DESTROY:
            if ((pIf = PppGetIfPtr (i4IfIndex)) == NULL)
            {
                return SNMP_FAILURE;
            }
            PPPDeleteIf (pIf);
            PPP_TRC1 (PPP_MUST, " \tPPP Interface [%d] Deleted\n", i4IfIndex);
            break;
        default:
            return SNMP_FAILURE;
            break;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppIfRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                testValPppIfRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppIfRowStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                         INT4 i4TestValPppIfRowStatus)
{
    switch (i4TestValPppIfRowStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            if (PppGetIfPtr (i4IfIndex) != NULL)
            {
                PPP_TRC (MGMT, "Interface Already Exists ");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;

        case ACTIVE:
        case DESTROY:
            if (PppGetIfPtr (i4IfIndex) == NULL)
            {
                PPP_TRC (MGMT, "Interface does not exists ");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
            break;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MpIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMpIfTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMpIfTable (INT4 i4IfIndex)
{
    if (PppGetIfPtr (i4IfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMpIfTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMpIfTable (INT4 *pi4IfIndex)
{
    if (nmhGetFirstIndex (pi4IfIndex, MpInstance, NEXT_INST) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMpIfTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMpIfTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    if (nmhGetNextIndex (&i4IfIndex, MpInstance, NEXT_INST) == SNMP_SUCCESS)
    {
        *pi4NextIfIndex = i4IfIndex;
        return (SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMpIfRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                retValMpIfRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMpIfRowStatus (INT4 i4IfIndex, INT4 *pi4RetValMpIfRowStatus)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetIfPtr (i4IfIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Index Found");
        return (SNMP_FAILURE);
    }
    *pi4RetValMpIfRowStatus = pIf->RowStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMpIfRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                setValMpIfRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMpIfRowStatus (INT4 i4IfIndex, INT4 i4SetValMpIfRowStatus)
{
    tPPPIf             *pIf;
    tPPPIfId            IfId;

    IfId.porttype = (UINT1) i4IfIndex;

    switch (i4SetValMpIfRowStatus)
    {
        case CREATE_AND_GO:
            if ((pIf = MPCreate (&IfId, (UINT2) i4IfIndex)) == NULL)
            {
                PPP_TRC (MGMT, "Creation of Interface Failed");
                return SNMP_FAILURE;
            }
            break;

        case DESTROY:
            if ((pIf = PppGetIfPtr (i4IfIndex)) == NULL)
            {
                return SNMP_FAILURE;
            }
            PPPDeleteIf (pIf);
            PPP_TRC1 (PPP_MUST, " \tMP Interface [%d] Deleted\n", i4IfIndex);
            break;
        default:
            return SNMP_FAILURE;
            break;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MpIfRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                testValMpIfRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MpIfRowStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                        INT4 i4TestValMpIfRowStatus)
{
    switch (i4TestValMpIfRowStatus)
    {
        case CREATE_AND_GO:
            if (PppGetIfPtr (i4IfIndex) != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                PPP_TRC (MGMT, "Interface Already Exists ");
                return SNMP_FAILURE;
            }
            break;

        case DESTROY:
            if (PppGetIfPtr (i4IfIndex) == NULL)
            {
                PPP_TRC (MGMT, "Interface does not exists ");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
            break;
    }
    return SNMP_SUCCESS;
}
