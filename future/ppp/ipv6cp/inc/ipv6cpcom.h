/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipv6cpcom.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the common PPP files required for
 * the IPV6CP module
 *
 *******************************************************************/
#ifndef __PPP_IPV6CPCOM_H__
#define __PPP_IPV6CPCOM_H__

#include "genhdrs.h"

/* Global PPP includes */
#include "pppmacro.h"
#include "pppdefs.h"
#include "pppdebug.h"
#include "ppptimer.h"
#include "pppdbgex.h"


/* Includes from other modules */
#include "frmtdfs.h"
#include "pppgsem.h"
#include "pppgcp.h"
#include "ppplcp.h"
#include "pppexts.h"

#include "pppipv6cp.h"

#include "ipv6proto.h"
#include "cfa.h"
#include "ppp.h"


#endif  /* __PPP_IPV6CPCOM_H__ */
