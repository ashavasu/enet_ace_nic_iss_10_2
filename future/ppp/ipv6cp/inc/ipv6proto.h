/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipv6proto.h,v 1.2 2011/10/13 10:31:27 siva Exp $
 *
 * Description:This file contains Prototypes for the IPV6CP functions 
 * used internally by IPV6CP module
 *
 *******************************************************************/
#ifndef __PPP_IPV6PROTO_H__
#define __PPP_IPV6PROTO_H__

/* Prototypes for IPV6CP functions used internally by IPV6CP module */
tIP6CPIf *ALLOC_IP6CP_IF(VOID);
tIP6CPIf *IP6CPCreateIf(tPPPIf *pIf);
VOID IP6CPDeleteIf(tIP6CPIf *pIp6cpPtr);
VOID IP6CPEnableIf(tIP6CPIf *pIp6cpPtr);
VOID IP6CPDisableIf(tIP6CPIf *pIp6cpPtr);
INT1 IP6CPInit(tIP6CPIf *pIp6cpPtr);
VOID IP6CPOptionInit(tIP6CPOptions *pIP6CPOpt);
VOID IP6CPUp(tGSEM *pIp6cpGSEM);
VOID IP6CPDown(tGSEM *pIp6cpGSEM);
VOID IP6CPFinished(tGSEM *pIp6cpGSEM);

INT1 IP6CPProcessIntfIdConfReq(tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal Optval, UINT2 Offset, UINT1 PresenceFlag);
INT1 IP6CPProcessCompProtoConfReq(tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal Optval, UINT2 Offset, UINT1 PresenceFlag);

INT1 IP6CPProcessIntfIdConfNak(tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal);
INT1 IP6CPProcessCompProtoConfNak(tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal);

INT1 IP6CPProcessIntfIdConfRej(tGSEM *pGSEM );
INT1 IP6CPProcessCompProtoConfRej(tGSEM *pGSEM );

VOID *IP6CPReturnIntfIdPtr(tGSEM  *pGSEM, UINT1 *OptLen);
VOID *IP6CPReturnCompProtoPtr(tGSEM  *pGSEM, UINT1 *OptLen);

/* SNMP related */
tIP6CPIf *PppGetIP6CPIfPtr(UINT4 Index);
tIP6CPIf *PppCreateIP6CPIfPtr(UINT4 Index);

INT1 IP6CPAddCompSubOpt (tGSEM * pGSEM, tOptVal OptVal, t_MSG_DESC * pOutBuf);
UINT4   IP6CPCompareIntfId(UINT1 * IntfId1, UINT1 * IntfId2);
VOID    IP6GenInterfaceId(UINT1*);
UINT4   IP6IsIntfIdZero(UINT1*);
#endif  /* __PPP_IPV6PROTO_H__ */
