/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipv6exts.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains definitions and prototypes for 
 * IPV6CP functions used by external modules
 *
 *******************************************************************/
#ifndef __PPP_IPV6EXTS_H__
#define __PPP_IPV6EXTS_H__

extern	tGenOptionInfo			gIp6cpGenOptionInfo[];
extern	tOptNegFlagsPerIf		gIp6cpOptPerIntf[];
extern	tGSEMCallbacks			gIp6cpGSEMCallback;
extern  tIP6Counters			gIP6Counters;

#endif  /* __PPP_IPV6EXTS_H__ */
