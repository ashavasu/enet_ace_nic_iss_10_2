/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: pppipv6cp.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
*
* Description:This file contains the typedefs, #defines and
*             data structures used by the IPV6CP Module.
*
*******************************************************************/

#ifndef __PPP_PPPIPV6CP_H__
#define __PPP_PPPIPV6CP_H__

/*********************************************************************/
/*                              CONSTANTS                           */
/*********************************************************************/







/* CONFIGURATION OPTION VALUES  for IPCP packets */


#define MAX_IP6CP_OPT_TYPES     2
#define IP6_COMPPROTO_DISCRETE_VALUES 1 
#define IP6_INTFID_OPTION       1
#define IP6_COMP_OPTION     2
#define IP6_INTFID_LEN      10
#define IP6_COMP_LEN        4
#define INTF_ID_LEN     8

#define IP6_COMPRESSION_NONE      1
#define  IP6_HEADER_COMPRESSION   3

#define IP6_INTFID_IDX      0
#define IP6_COMP_IDX        1

#define IPv6_DATAGRAM       0x0057

#define IP6_UP          1
#define IP6_DOWN        2
#define IP6_DELETE      3

#define  IPHC_PROTOCOL                  0x0061

#define LOC_INTFID_ZERO     0x0001
#define REM_INTFID_ZERO     0x0010
#define INTFID_EQUAL        0x0100
#define INTFID_DIFF     0x1000

#define IEEE_GLOBAL_ID      1
#define LOCAL_MAC_ADDRESS   2
#define RANDOM_NUMBER       3
#define MANUALLY        4

/********************************************************************/
/*          TYPEDEFS    USED BY THE IPCP MODULE                     */
/*********************************************************************/
typedef struct {
    UINT4   DiscHeader;
    UINT2   Value[IP6_COMPPROTO_DISCRETE_VALUES];
    UINT2   u2Rsvd;
} tIp6CompProtoDiscVal; 

typedef struct {
    UINT1   InterfaceId[INTF_ID_LEN];
    UINT2   Ip6CompProtocol;
    UINT2   u2Rsvd;
    tIPHCInfo IPHCInfo;
} tIP6CPOptions;


typedef struct {
    tIP6CPOptions   Ip6cpOptionsDesired;
    tIP6CPOptions   Ip6cpOptionsAllowedForPeer;
    tIP6CPOptions   Ip6cpOptionsAckedByPeer;
    tIP6CPOptions   Ip6cpOptionsAckedByLocal;
    tGSEM           Ip6cpGSEM;
    UINT1           u1OperStatus;
    UINT1           u1AdminStatus;
    UINT1           u1RowStatus;
    UINT1           u1MethodToGenIntfId;
    VOID            *HLHandle;
} tIP6CPIf;



typedef struct IP6Counters {
UINT2               u2AllocCounter;
UINT2               u2FreeCounter;
UINT2               u2IfFreeCounter;
UINT2		    u2Rsvd;
}tIP6Counters;

#endif /* __PPP_PPPIPV6CP_H__ */
