/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppipv6cp.c,v 1.5 2014/03/11 14:02:50 siva Exp $
 *
 * Description:This file contains callback routines of IPV6CP module.
 *             It also contains routines for the input IPV6CP packet
 *             processing.
 *
 *******************************************************************/

#include "ipv6cpcom.h"
#include "gsemdefs.h"
#include "gcpdefs.h"
#include "genexts.h"
#include "lcpdefs.h"
#include "ipv6exts.h"
#include "mpdefs.h"
#include "globexts.h"
#include "ipv6proto.h"
#include "genproto.h"
tIP6Counters        gIP6Counters;

/******************** GLOBALS ************************************/

tOptHandlerFuns     gIP6IntfIdHandlingFuns = {
    IP6CPProcessIntfIdConfReq,
    IP6CPProcessIntfIdConfNak,
    IP6CPProcessIntfIdConfRej,
    IP6CPReturnIntfIdPtr,
    NULL
};

tOptHandlerFuns     gIP6CompProtoHandlingFuns = {
    IP6CPProcessCompProtoConfReq,
    IP6CPProcessCompProtoConfNak,
    IP6CPProcessCompProtoConfRej,
    IP6CPReturnCompProtoPtr,
    IP6CPAddCompSubOpt
};

tGSEMCallbacks      gIp6cpGSEMCallback = {
    IP6CPUp,
    IP6CPDown,
    NULL,
    IP6CPFinished,
    NCPRxData,
    NCPTxData,
    NULL,
    NULL
};

tIp6CompProtoDiscVal gIp6CompProtoDiscVal = { IP6_COMPPROTO_DISCRETE_VALUES,
    {IPHC_PROTOCOL}
    , 0
};

tGenOptionInfo      gIP6CPGenOptionInfo[] = {

    {IP6_INTFID_OPTION, STRING_TYPE, STRING_TYPE, 10,
     &gIP6IntfIdHandlingFuns, {NULL}
     , NOT_SET, 0, 0}
    ,

    {IP6_COMP_OPTION, DISCRETE, BYTE_LEN_2, 4,
     &gIP6CompProtoHandlingFuns, {(VOID *) &gIp6CompProtoDiscVal},
     NOT_SET, 0, 0}

};

tOptNegFlagsPerIf   gIP6CPOptPerIntf[] = {
    {IP6_INTFID_OPTION,
     ALLOWED_FOR_PEER_SET | ALLOW_REJECT_SET | DESIRED_SET, 0},
    {IP6_COMP_OPTION, ALLOW_REJECT_SET | ALLOWED_FOR_PEER_SET, 0}
};

/***********************************************************************/
/*                      INTERFACE  FUNCTIONS                           */
/************************************************************************/

/***********************************************************************
*  Function Name : IP6CPCreateIf
*  Description   :
*          This procedure creates an entry in the IPV6CP interface data
*  base corresponding to IfId. It also initializes the created entry to the
*  default values  by calling the IP6CPinit() function.
*  Parameter(s)  :
*         pIf -    pointer to the PPP interface structure
*  Return Values :
*      pointer to the newly created  interface structure , if the creation is
*        success
*      NULL if it fails to create the interface.
*********************************************************************/
tIP6CPIf           *
IP6CPCreateIf (tPPPIf * pIf)
{
    tIP6CPIf           *pIp6cpPtr = NULL;

    if ((pIp6cpPtr = (tIP6CPIf *) ALLOC_IP6CP_IF ()) == NULL)
    {
        PPP_TRC (ALL_FAILURE, "Error: IPV6CP Alloc/Init failure .\n");
        return NULL;
    }
    if (IP6CPInit (pIp6cpPtr) == OK)
    {
        pIp6cpPtr->Ip6cpGSEM.pIf = pIf;
    }
    else
    {
        FREE_IP6CP_IF (pIp6cpPtr);
        pIp6cpPtr = NULL;
        PPP_TRC (ALL_FAILURE, "Error: IPV6CP Alloc/Init failure .\n");
    }

    return (pIp6cpPtr);
}

/*********************************************************************
*  Function Name : IP6CPDeleteIf
*  Description   :
*               This procedure deletes an IP6CP interface  entry from the
*  database and deallocates memory corresponding to the interface.
*  Parameter(s)  :
*         pIp6cpPtr -  points to  the IP6CP interface structure to be deleted.
*  Return Values : VOID
*********************************************************************/
VOID
IP6CPDeleteIf (tIP6CPIf * pIp6cpPtr)
{
    pIp6cpPtr->Ip6cpGSEM.pIf->pIp6cpPtr = NULL;
    GSEMDelete (&(pIp6cpPtr->Ip6cpGSEM));
    FREE_IP6CP_IF (pIp6cpPtr);
    return;
}

/*********************************************************************
*  Function Name : IP6CPEnableIf
*  Description   :
*              This function is called when there is an  IP6CP Admin Open
*  for an interface from the SNMP It  triggers an OPEN event to the GSEM and
*  passes the IP6CP tGSEM corresponds to this interface as parameter.
*  Parameter(s)    :
*    pIp6cpPtr    -    Points to the IP6CP interface structure to be enabled
*  Return Values : VOID
*********************************************************************/
VOID
IP6CPEnableIf (tIP6CPIf * pIp6cpPtr)
{
    UINT1               u1Index = 0;
    tPPPIf             *pIf = NULL;

    if (pIp6cpPtr->Ip6cpGSEM.pIf->BundleFlag == BUNDLE)
    {
        SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
        {
            if (pIf->MPInfo.MemberInfo.pBundlePtr != NULL
                && pIf->MPInfo.MemberInfo.pBundlePtr ==
                pIp6cpPtr->Ip6cpGSEM.pIf)
            {
                pIf->pIp6cpPtr = pIp6cpPtr;
            }
        }
    }

    pIp6cpPtr->u1AdminStatus = ADMIN_OPEN;

    /* Check whether the link is already in the Operational state.
       If so, then indicate the GSEM about the  link UP
     */

    if ((pIp6cpPtr->Ip6cpGSEM.CurrentState != REQSENT)
        || (pIp6cpPtr->Ip6cpGSEM.CurrentState != ACKRCVD)
        || (pIp6cpPtr->Ip6cpGSEM.CurrentState != ACKSENT))
    {
        MEMCPY (&pIp6cpPtr->Ip6cpOptionsAckedByPeer,
                &pIp6cpPtr->Ip6cpOptionsDesired, sizeof (tIP6CPOptions));
        for (u1Index = 0; u1Index < MAX_IP6CP_OPT_TYPES; u1Index++)
        {
            if (pIp6cpPtr->Ip6cpGSEM.pNegFlagsPerIf[u1Index].
                FlagMask & DESIRED_SET)
            {
                pIp6cpPtr->Ip6cpGSEM.pNegFlagsPerIf[u1Index].FlagMask |=
                    ACKED_BY_PEER_SET;
            }
            else
            {
                pIp6cpPtr->Ip6cpGSEM.pNegFlagsPerIf[u1Index].FlagMask &=
                    ACKED_BY_PEER_NOT_SET;
            }
        }
    }
    GSEMRun (&pIp6cpPtr->Ip6cpGSEM, OPEN);

    if ((pIp6cpPtr->Ip6cpGSEM.pIf->LcpStatus.LinkAvailability == LINK_AVAILABLE)
        && (pIp6cpPtr->Ip6cpGSEM.CurrentState != CLOSED))
    {
        GSEMRun (&pIp6cpPtr->Ip6cpGSEM, PPP_UP);
    }

    GSEMRestartIfNeeded (&pIp6cpPtr->Ip6cpGSEM);

    return;
}

/*********************************************************************
*  Function Name : IP6CPDisableIf
*  Description   :
*          This function is called when there is an Admin Close for an
*  interface from the SNMP It triggers a CLOSE event to the GSEM and passes
*  the IP6CP GSEMStructure corresponds to this interface as a parameter.
*  Parameter(s)  :
*            pIp6cpPtr - points to  the IP6CP interface structure to be  
*                        disabled
*  Return Values : VOID
*********************************************************************/
VOID
IP6CPDisableIf (tIP6CPIf * pIp6cpPtr)
{
    GSEMRun (&pIp6cpPtr->Ip6cpGSEM, CLOSE);
    return;
}

/*********************************************************************
*  Function Name : IP6CPFinished
*  Description   :
*    This function is invoked when the higher layer is done with
*  the link and the link is no longer needed. This invokes  procedures for
*  terminating the link.
*         The pFinished  field of the GSEMCallback structure points
*  to this function and is called from the GSEM Module.
*  Parameter(s)  :
*            pGSEM  - pointer to IP6CP tGSEM corresponding
*  to current interface
*  Return Value  :    VOID
*********************************************************************/
VOID
IP6CPFinished (tGSEM * pGSEM)
{
    tIP6CPIf           *pIp6cpPtr = NULL;
    UINT1               u1Index = 0;

    pIp6cpPtr = pGSEM->pIf->pIp6cpPtr;
    MEMCPY (&pIp6cpPtr->Ip6cpOptionsAckedByPeer,
            &pIp6cpPtr->Ip6cpOptionsDesired, sizeof (tIP6CPOptions));
    for (u1Index = 0; u1Index < MAX_IP6CP_OPT_TYPES; u1Index++)
    {
        if (pGSEM->pNegFlagsPerIf[u1Index].FlagMask & DESIRED_SET)
        {
            pGSEM->pNegFlagsPerIf[u1Index].FlagMask |= ACKED_BY_PEER_SET;
        }
        else
        {
            pGSEM->pNegFlagsPerIf[u1Index].FlagMask &= ACKED_BY_PEER_NOT_SET;
        }
    }
    return;
}

/*********************************************************************
*  Function Name : IP6CPInit
*  Description   :
*              This function initializes the IP6CP GSEM and  sets
*  the  IP6CP configuration option variables to their default values. It is
*  invoked by the IP6CPCreateIf() function.
*  Parameter(s)  :
*        pIp6cpPtr -   points to the IP6CP interface structure
*  Return Values : OK/NOT_OK
*********************************************************************/
INT1
IP6CPInit (tIP6CPIf * pIp6cpPtr)
{

    /* Initialize the Option fields */
    IP6CPOptionInit (&pIp6cpPtr->Ip6cpOptionsDesired);
    IP6CPOptionInit (&pIp6cpPtr->Ip6cpOptionsAllowedForPeer);
    IP6CPOptionInit (&pIp6cpPtr->Ip6cpOptionsAckedByPeer);
    IP6CPOptionInit (&pIp6cpPtr->Ip6cpOptionsAckedByLocal);

    pIp6cpPtr->u1AdminStatus = STATUS_DOWN;
    pIp6cpPtr->u1OperStatus = STATUS_DOWN;

    if (GSEMInit (&pIp6cpPtr->Ip6cpGSEM, MAX_IP6CP_OPT_TYPES) == NOT_OK)
    {
        return (NOT_OK);
    }
    pIp6cpPtr->Ip6cpGSEM.Protocol = IPV6CP_PROTOCOL;
    pIp6cpPtr->Ip6cpGSEM.pCallbacks = &gIp6cpGSEMCallback;
    pIp6cpPtr->Ip6cpGSEM.pGenOptInfo = gIP6CPGenOptionInfo;
    MEMCPY (pIp6cpPtr->Ip6cpGSEM.pNegFlagsPerIf, &gIP6CPOptPerIntf,
            (sizeof (tOptNegFlagsPerIf) * MAX_IP6CP_OPT_TYPES));
    return (OK);
}

/***********************************************************************/
/*                      INTERNAL  FUNCTIONS                            */
/***********************************************************************/

/***********************************************************************
*  Function Name : IP6CPOptionInit
*  Description   :
*               This function initializes the IP6CPOption structure and is
*  called by the IP6CPInit function.
*  Parameter(s)  :
*       pIP6CPOpt   -  points to the IP6CPOption structure  to be initialized.
*  Return Value  :
*********************************************************************/
VOID
IP6CPOptionInit (tIP6CPOptions * pIP6CPOpt)
{
    MEMSET (pIP6CPOpt->InterfaceId, 0x0, INTF_ID_LEN);
    pIP6CPOpt->Ip6CompProtocol = IP6_COMPRESSION_NONE;
    pIP6CPOpt->IPHCInfo.u2TcpSpace = DEFAULT_IPHC_TCP_SPACE;
    pIP6CPOpt->IPHCInfo.u2NonTcpSpace = DEFAULT_IPHC_NON_TCP_SPACE;
    pIP6CPOpt->IPHCInfo.u2FMaxPeriod = DEFAULT_IPHC_FULL_MAX_PERIOD;
    pIP6CPOpt->IPHCInfo.u2FMaxTime = DEFAULT_IPHC_FULL_MAX_TIME;
    pIP6CPOpt->IPHCInfo.u2MaxHdr = DEFAULT_IPHC_MAX_HEADER;
    pIP6CPOpt->IPHCInfo.u2RtpSubOptFlag = PPP_FALSE;

    return;

}

/*********************************************************************
*  Function Name : IP6CPUp
*  Description   :
*              This function is called once the network level configuration
*  option is negotiated  successfully. This function indicates  to the higher
*  layers  that the IPCP is up.  The pUp  field of  the IP6CPGSEMCallback points
*  to this function and is called from the GSEM Module.
*  Parameter(s)  :
*            pIp6cpGSEM  - pointer to the IP6CP tGSEM
*                                   corresponding to the current interface.
*  Return Values : VOID
*********************************************************************/
VOID
IP6CPUp (tGSEM * pIp6cpGSEM)
{
    tIP6CPIf           *pIp6cpIf = NULL;

    pIp6cpIf = (tIP6CPIf *) pIp6cpGSEM->pIf->pIp6cpPtr;
    pIp6cpIf->u1OperStatus = STATUS_UP;

    PPPHLINotifyProtStatusToHL (pIp6cpGSEM->pIf, IPV6CP_PROTOCOL, PPP_UP);
    return;
}

/*********************************************************************
*  Function Name : IP6CPDown
*  Description   :
*          This function is invoked  when the IP6CP protocol is leaving the
*  OPENED state. It  indicates the higher layers  about the operational status
*  of the IP6CP.  The pDown  field of  the IP6CPGSEMCallback structure points to
*  this function and is called from the GSEM Module.
*  Parameter(s)  :
*            pIp6cpGSEM  - pointer to the IP6CP tGSEM
*                       corresponding to the current interface.
*  Return Values : VOID
*********************************************************************/
VOID
IP6CPDown (tGSEM * pIp6cpGSEM)
{
    tPPPIf             *pIf = NULL;
    tIP6CPIf           *pIp6cpIf = NULL;

    pIf = pIp6cpGSEM->pIf;
    pIp6cpIf = (tIP6CPIf *) pIp6cpGSEM->pIf->pIp6cpPtr;

    /*  If we are maintaining any Interface Identifier Pool 
       sort of thing then we should delete the interface 
       identifier of the current interface and make the 
       deleted interface identifier available for any further 
       use. */

    PPPHLINotifyProtStatusToHL (pIf, IPV6CP_PROTOCOL, PPP_DOWN);
    IP6CPFinished (pIp6cpGSEM);
    pIp6cpIf->u1OperStatus = STATUS_DOWN;

    MEMCPY (&pIp6cpIf->Ip6cpOptionsAckedByPeer, &pIp6cpIf->Ip6cpOptionsDesired,
            sizeof (tIP6CPOptions));
    return;
}

/***********************************************************************
    OPTION SPECIFIC FUNCTIONS FOR PROCESSING CONF_REQ
************************************************************************/
/*********************************************************************
 * *  Function Name : IP6CPAddCompSubOpt
 * *  Description   :
 * *     This function is used to add VJ Compression suboption(s).
 * *  Input         :
 * *     pGSEM      -  pointer to the GSEM structure corresponding to the
 * *                   packet.
 * *     tOptVal    -  Option Value received  to be processed
 * *  Return Value  :  returns the number of bytes appended in the outgoing
 * *                   buffer
 * *********************************************************************/
INT1
IP6CPAddCompSubOpt (tGSEM * pGSEM, tOptVal OptVal, t_MSG_DESC * pOutBuf)
{
    tIP6CPIf           *pIp6cpIf;

    pIp6cpIf = (tIP6CPIf *) pGSEM->pIf->pIp6cpPtr;
    PPP_UNUSED (pOutBuf);
    PPP_UNUSED (OptVal);
    PPP_TRC (CONTROL_PLANE, "\t\t\tIPCP    :");
    if (OptVal.ShortVal == IPHC_PROTOCOL)
    {
        tIPHCInfo          *pIPHCInfo;
        pIPHCInfo = &pIp6cpIf->Ip6cpOptionsAckedByPeer.IPHCInfo;

        APPEND2BYTE (pOutBuf, pIPHCInfo->u2TcpSpace);
        APPEND2BYTE (pOutBuf, pIPHCInfo->u2NonTcpSpace);
        APPEND2BYTE (pOutBuf, pIPHCInfo->u2FMaxPeriod);
        APPEND2BYTE (pOutBuf, pIPHCInfo->u2FMaxTime);
        APPEND2BYTE (pOutBuf, pIPHCInfo->u2MaxHdr);

        if (pIPHCInfo->u2RtpSubOptFlag == PPP_TRUE)
        {
            APPEND1BYTE (pOutBuf, RTP_SUBOPT_TYPE);
            APPEND1BYTE (pOutBuf, RTP_SUBOPT_TYPE_LEN);
            return (MIN_IPHC_SUBOPT_LEN + RTP_SUBOPT_TYPE_LEN);
        }
        return (MIN_IPHC_SUBOPT_LEN);
    }

    return (0);
}

/*********************************************************************
*  Function Name : IP6CPProcessIntfIdConfReq
*  Description   :
*        This function is invoked to process the Interface Identifier
*        option present in the CONF_REQ and to construct appropriate 
*        response.
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer
*     tOptVal    -  Option Value received  to be processed
*     Offset     -  indicates the place in the outgoing buffer at which
*                   the option value is appended
*  PresenceFlag  -  indicates whether the option is present in the recd.
*                   CONF_REQ packet
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer
*********************************************************************/
INT1
IP6CPProcessIntfIdConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal,
                           UINT2 Offset, UINT1 PresenceFlag)
{
    tIP6CPIf           *pIp6cpPtr = NULL;
    UINT4               u4Value = 0x0000;

    /* UINT1               au1InterfaceId[INTF_ID_LEN]; */
    PPP_UNUSED (pInPkt);

    pIp6cpPtr = pGSEM->pIf->pIp6cpPtr;

    if (PresenceFlag == SET)
    {
        u4Value =
            IP6CPCompareIntfId (pIp6cpPtr->Ip6cpOptionsDesired.InterfaceId,
                                OptVal.StrVal);

        /* Case 1 & Case 3 of rfc2472 */

        /*if ((u4Value == (INTFID_DIFF | REM_INTFID_ZERO))
           || (u4Value == INTFID_EQUAL)) */

        if ((((u4Value & INTFID_DIFF) != 0)
             && ((u4Value & REM_INTFID_ZERO) != 0))
            || ((u4Value & INTFID_EQUAL) != 0))
        {
            /* Currently we are supporting only interface Identifier
               to be set MANUALLY only. So when other optins are also
               available, the following code should be removed from
               comments */

            /* IP6GenInterfaceId (au1InterfaceId);
               MEMCPY (pIp6cpPtr->Ip6cpOptionsAllowedForPeer.InterfaceId,
               au1InterfaceId, INTF_ID_LEN); */

            ASSIGNSTR (pGSEM->pOutParam,
                       pIp6cpPtr->Ip6cpOptionsAllowedForPeer.InterfaceId,
                       Offset, BYTE_LEN_8);
            return (BYTE_LEN_8);
        }

        /* Case 2 of rfc2472 */

        /*if ((u4Value == INTFID_DIFF)
           || (u4Value == (LOC_INTFID_ZERO | INTFID_DIFF))) */

        if ((u4Value & INTFID_DIFF) != 0)
        {
            MEMCPY (pIp6cpPtr->Ip6cpOptionsAckedByLocal.InterfaceId,
                    OptVal.StrVal, 8);
            return (ACK);
        }

        /* Case 4 of rfc2472 */

        /* if (u4Value == (LOC_INTFID_ZERO | REM_INTFID_ZERO | INTFID_EQUAL)) */

        if (((u4Value & LOC_INTFID_ZERO) != 0)
            && ((u4Value & REM_INTFID_ZERO) != 0)
            && ((u4Value & INTFID_EQUAL) != 0))
        {
            return (CONF_REJ_CODE);
        }

    }

    if (PresenceFlag == NOT_SET)
    {
        /* Currently we are supporting only interface Identifier
           to be set MANUALLY only. So when other optins are also
           available, the following code should be removed from
           comments */

        /* IP6GenInterfaceId (au1InterfaceId);
           MEMCPY (pIp6cpPtr->Ip6cpOptionsAllowedForPeer.InterfaceId, 
           au1InterfaceId,INTF_ID_LEN); */

        ASSIGNSTR (pGSEM->pOutParam,
                   pIp6cpPtr->Ip6cpOptionsAllowedForPeer.InterfaceId, Offset,
                   BYTE_LEN_8);
        return (BYTE_LEN_8);
    }
    return (BYTE_LEN_8);        /* Doesn't Reach Here At Any Time But Put To Remove
                                   Warning */
}

/*********************************************************************
*  Function Name : IP6CPProcessCompProtoConfReq
*  Description   :
*          This function processes the Compression Option in the
*  CONF_REQ packet and builds appropriate response.
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer
*     tOptVal    -  Option Value received  to be processed
*     Offset     -  indicates the place in the outgoing buffer at which
*                   the option value is appended
*  PresenceFlag  -  indicates whether the option is present in the recd.
*                   CONF_REQ packet
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer
*********************************************************************/
INT1
IP6CPProcessCompProtoConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt,
                              tOptVal OptVal, UINT2 Offset, UINT1 PresenceFlag)
{

    tIP6CPIf           *pIp6cpPtr;
    UINT1               u1RecdOptLen;
    tIPHCInfo          *pIPHCInfo;

    pIp6cpPtr = pGSEM->pIf->pIp6cpPtr;
    u1RecdOptLen = ExtractedLength;

    if (PresenceFlag == SET)
    {

        if (OptVal.ShortVal == IPHC_PROTOCOL)
        {
            tIPHCInfo           IPHCInfo;

            PPP_TRC (CONTROL_PLANE, "\t\t\tCOMPRESSION : IP6HC");
            if (u1RecdOptLen < MIN_IPHC_OPT_LEN)
            {
                return (DISCARD);
            }

            EXTRACT2BYTE (pInPkt, IPHCInfo.u2TcpSpace);
            EXTRACT2BYTE (pInPkt, IPHCInfo.u2NonTcpSpace);
            EXTRACT2BYTE (pInPkt, IPHCInfo.u2FMaxPeriod);
            EXTRACT2BYTE (pInPkt, IPHCInfo.u2FMaxTime);
            EXTRACT2BYTE (pInPkt, IPHCInfo.u2MaxHdr);
            IPHCInfo.u2RtpSubOptFlag = PPP_FALSE;

            if (u1RecdOptLen > MIN_IPHC_OPT_LEN)
            {
                UINT1               u1RemLen, u1Type, u1SubOptLen;

                u1RemLen = (UINT1) (u1RecdOptLen - MIN_IPHC_OPT_LEN);
                while (u1RemLen)
                {
                    EXTRACT1BYTE (pInPkt, u1Type);
                    EXTRACT1BYTE (pInPkt, u1SubOptLen);
                    if ((u1Type == RTP_SUBOPT_TYPE)
                        && (u1SubOptLen == RTP_SUBOPT_TYPE_LEN))
                    {
                        IPHCInfo.u2RtpSubOptFlag = PPP_TRUE;
                    }
                    u1RemLen = (UINT1) (u1RemLen - u1SubOptLen);
                }
            }

            if ((IPHCInfo.u2TcpSpace < MAX_IPHC_TCP_SPACE) &&
                (IPHCInfo.u2NonTcpSpace < MAX_IPHC_NON_TCP_SPACE))
            {
                pIp6cpPtr->Ip6cpOptionsAckedByLocal.Ip6CompProtocol =
                    IPHC_PROTOCOL;
                MEMCPY (&pIp6cpPtr->Ip6cpOptionsAckedByLocal.IPHCInfo,
                        &IPHCInfo, sizeof (tIPHCInfo));
                return (ACK);
            }

        }

    }

    pIPHCInfo = &pIp6cpPtr->Ip6cpOptionsAllowedForPeer.IPHCInfo;

    ASSIGN2BYTE (pGSEM->pOutParam, Offset, IPHC_PROTOCOL);
    Offset = (UINT2) (Offset + 2);

    ASSIGN2BYTE (pGSEM->pOutParam, Offset, pIPHCInfo->u2TcpSpace);
    Offset = (UINT2) (Offset + 2);

    ASSIGN2BYTE (pGSEM->pOutParam, Offset, pIPHCInfo->u2NonTcpSpace);
    Offset = (UINT2) (Offset + 2);

    ASSIGN2BYTE (pGSEM->pOutParam, Offset, pIPHCInfo->u2FMaxPeriod);
    Offset = (UINT2) (Offset + 2);

    ASSIGN2BYTE (pGSEM->pOutParam, Offset, pIPHCInfo->u2FMaxTime);
    Offset = (UINT2) (Offset + 2);

    ASSIGN2BYTE (pGSEM->pOutParam, Offset, pIPHCInfo->u2MaxHdr);
    Offset = (UINT2) (Offset + 2);

    if (pIPHCInfo->u2RtpSubOptFlag == PPP_TRUE)
    {
        ASSIGN1BYTE (pGSEM->pOutParam, Offset, RTP_SUBOPT_TYPE);
        Offset = (UINT2) (Offset + 1);
        ASSIGN1BYTE (pGSEM->pOutParam, Offset, RTP_SUBOPT_TYPE_LEN);
        return (MIN_IPHC_OPT_LEN + RTP_SUBOPT_TYPE_LEN);
    }

    return (MIN_IPHC_OPT_LEN);

}

/***********************************************************************
    OPTION SPECIFIC FUNCTIONS FOR PROCESSING CONF_NAK
************************************************************************/

/*********************************************************************
*  Function Name : IP6CPProcessIntfIdConfNak
*  Description   :
*        This function is invoked to process the Interface Identifier option 
*        present in the CONF_NAK and to construct appropriate response.
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer
*     tOptVal    -  Option Value received  to be processed
*  Return Value  :  OK/NOT_OK/CLOSE
*********************************************************************/
INT1
IP6CPProcessIntfIdConfNak (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal)
{
    tIP6CPIf           *pIp6cpPtr = NULL;

    /* UINT1               au1IntfId[INTF_ID_LEN]; */

    pIp6cpPtr = pGSEM->pIf->pIp6cpPtr;
    PPP_UNUSED (pInPkt);
    /* Check this if statement.This statement is to check if the Interface
       Identifier received in CONF_NAK is equal to the one sent in the 
       previous CONF_REQ or CONF_NAK */

    if ((IP6CPCompareIntfId
         (OptVal.StrVal,
          pIp6cpPtr->Ip6cpOptionsAllowedForPeer.InterfaceId) == INTFID_EQUAL)
        ||
        (IP6CPCompareIntfId
         (OptVal.StrVal,
          pIp6cpPtr->Ip6cpOptionsAckedByLocal.InterfaceId) == INTFID_EQUAL))
    {
        /* Currently we are supporting only interface Identifier
           to be set MANUALLY only. So when other optins are also
           available, the following code should be removed from
           comments */

        /* IP6GenInterfaceId (au1IntfId);
           MEMCPY (pIp6cpPtr->Ip6cpOptionsAllowedForPeer.InterfaceId, au1IntfId,
           INTF_ID_LEN); */

        return (OK);
    }

    if (pGSEM->pNegFlagsPerIf[IP6_INTFID_IDX].FlagMask & ACKED_BY_PEER_MASK)
    {
        if (IP6IsIntfIdZero (pIp6cpPtr->Ip6cpOptionsAckedByPeer.InterfaceId) ==
            TRUE)
        {
            MEMCPY (pIp6cpPtr->Ip6cpOptionsAckedByPeer.InterfaceId,
                    OptVal.StrVal, INTF_ID_LEN);
        }
        else
        {
            return (CLOSE);
        }
    }
    else
    {
        if (IP6CPCompareIntfId
            (OptVal.StrVal,
             pIp6cpPtr->Ip6cpOptionsAckedByPeer.InterfaceId) == INTFID_DIFF)
        {
            if (IP6IsIntfIdZero (pIp6cpPtr->Ip6cpOptionsAckedByPeer.InterfaceId)
                == TRUE)
            {
                MEMCPY (pIp6cpPtr->Ip6cpOptionsAckedByPeer.InterfaceId,
                        OptVal.StrVal, INTF_ID_LEN);
                return (OK);
            }
            if (IP6IsIntfIdZero (OptVal.StrVal) == TRUE)
                return (OK);
            else
                return (DISCARD);
        }
    }
    return (OK);
}

/*********************************************************************
*  Function Name : IP6CPProcessCompProtoConfNak
*  Description   :
*        This function is invoked to process the Compression option present
*  in the CONF_NAK and to construct appropriate response
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer
*     tOptVal    -  Option Value received  to be processed
*   NegOptFlag   -  indicates whether the option is included in the request
*  Return Value  :  OK/NOT_OK/CLOSE
*********************************************************************/
INT1
IP6CPProcessCompProtoConfNak (tGSEM * pGSEM, t_MSG_DESC * pInPkt,
                              tOptVal OptVal)
{

    tIP6CPIf           *pIp6cpPtr = NULL;
    UINT1               u1RecdOptLen;

    pIp6cpPtr = pGSEM->pIf->pIp6cpPtr;

    u1RecdOptLen = ExtractedLength;

    if (OptVal.ShortVal == IPHC_PROTOCOL)
    {
        tIPHCInfo           IPHCInfo;

        MEMSET (&IPHCInfo, 0, sizeof (tIPHCInfo));
        EXTRACT2BYTE (pInPkt, IPHCInfo.u2TcpSpace);
        EXTRACT2BYTE (pInPkt, IPHCInfo.u2NonTcpSpace);
        EXTRACT2BYTE (pInPkt, IPHCInfo.u2FMaxPeriod);
        EXTRACT2BYTE (pInPkt, IPHCInfo.u2FMaxTime);
        EXTRACT2BYTE (pInPkt, IPHCInfo.u2MaxHdr);

        if (u1RecdOptLen > MIN_IPHC_OPT_LEN)
        {
            UINT1               u1Type, u1SubOptLen;

            if ((u1RecdOptLen - MIN_IPHC_OPT_LEN) != RTP_SUBOPT_TYPE_LEN)
            {
                EXTRACT1BYTE (pInPkt, u1Type);
                EXTRACT1BYTE (pInPkt, u1SubOptLen);
                if ((u1Type != RTP_SUBOPT_TYPE)
                    && (u1SubOptLen != RTP_SUBOPT_TYPE_LEN))
                {
                    return NOT_OK;
                }
            }
        }

        if ((IPHCInfo.u2TcpSpace > MAX_IPHC_TCP_SPACE))
        {
            return DISCARD;
        }
        MEMCPY (&pIp6cpPtr->Ip6cpOptionsAckedByPeer.IPHCInfo, &IPHCInfo,
                sizeof (tIPHCInfo));

        return (OK);
    }
    return NOT_OK;

}

/***********************************************************************
    OPTION SPECIFIC FUNCTIONS FOR RETURNING THE ADDRESS OF OPTION DATA
************************************************************************/
/*********************************************************************
*  Function Name : IP6CPReturnIntfIdPtr
*  Description   :
*     This function returns the address of the location where the Interface
*  Identifier value to be included in the CONF_REQ, is stored.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*     OptLen     -  used to return the length of the option value (in
*                   case the option is of 'STRING' type)
*  Return Value  :  returns the option data address
*********************************************************************/

VOID               *
IP6CPReturnIntfIdPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    tIP6CPIf           *pIp6cpIf = NULL;

    *OptLen = INTF_ID_LEN;
    pIp6cpIf = (tIP6CPIf *) pGSEM->pIf->pIp6cpPtr;

    return (&pIp6cpIf->Ip6cpOptionsAckedByPeer.InterfaceId);
}

/*********************************************************************
*  Function Name : IP6CPReturnCompProtoPtr
*  Description   :
*     This function returns the address of the location where the 
*     Compression Option value to be included in the CONF_REQ, is stored.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*     OptLen     -  used to return the length of the option value (in
*                   case the option is of 'STRING' type)
*  Return Value  :  returns the option data address
*********************************************************************/
VOID               *
IP6CPReturnCompProtoPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    tIP6CPIf           *pIp6cpIf = NULL;
    PPP_UNUSED (OptLen);
    pIp6cpIf = (tIP6CPIf *) pGSEM->pIf->pIp6cpPtr;

    return (&pIp6cpIf->Ip6cpOptionsAckedByPeer.Ip6CompProtocol);
}

/*********************************************************************
*  Function Name : IP6CPProcessIntfIdConfRej
*  Description   :
*    This function sets the default value of the rejected option.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*  Return Value  : OK
*********************************************************************/
INT1
IP6CPProcessIntfIdConfRej (tGSEM * pGSEM)
{
    tIP6CPIf           *pIp6cpIf = NULL;

    pIp6cpIf = (tIP6CPIf *) pGSEM->pIf->pIp6cpPtr;
    MEMSET (pIp6cpIf->Ip6cpOptionsAckedByPeer.InterfaceId, 0x0, INTF_ID_LEN);
    return (OK);
}

/*********************************************************************
*  Function Name : IP6CPProcessCompProtoConfRej
*  Description   :
*   This function sets the default value of the rejected option.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*  Return Value  : OK
*********************************************************************/
INT1
IP6CPProcessCompProtoConfRej (tGSEM * pGSEM)
{
    tIP6CPIf           *pIp6cpIf = NULL;
    tIPHCInfo          *pIPHCInfo;

    pIp6cpIf = (tIP6CPIf *) pGSEM->pIf->pIp6cpPtr;

    PPP_TRC (CONTROL_PLANE, "\t\t\tIP6CP    :");
    pIp6cpIf = (tIP6CPIf *) pGSEM->pIf->pIp6cpPtr;
    pIPHCInfo = &pIp6cpIf->Ip6cpOptionsAckedByPeer.IPHCInfo;

    pIPHCInfo->u2TcpSpace = DEFAULT_IPHC_TCP_SPACE;
    pIPHCInfo->u2NonTcpSpace = DEFAULT_IPHC_NON_TCP_SPACE;
    pIPHCInfo->u2FMaxPeriod = DEFAULT_IPHC_FULL_MAX_PERIOD;
    pIPHCInfo->u2FMaxTime = DEFAULT_IPHC_FULL_MAX_TIME;
    pIPHCInfo->u2MaxHdr = DEFAULT_IPHC_MAX_HEADER;
    pIPHCInfo->u2RtpSubOptFlag = PPP_FALSE;
    return (OK);

}

/*********************************************************************
*  Function Name : IP6CPCopyOptions
*  Description   :
*    This function is used to copy the IP6CP options.
*  Input         :
*    pGSEM        -     Points to the GSEM structure
*    Flag        -     Specifies whether to alloc or free the temporary
*                    variable.
*    PktType        -    Specifies whether the function is called from request
*                    processing or Nak processing.
*  Return Value        :
*                    DISCARD on allocation error, OK otherwise.
*********************************************************************/
INT1
IP6CPCopyOptions (tGSEM * pGSEM, UINT1 Flag, UINT1 PktType)
{
    UINT1               Size = 0;
    tIP6CPIf           *pIp6cpIf = NULL;
    tIP6CPOptions      *pTemp = NULL;

    pIp6cpIf = (tIP6CPIf *) pGSEM->pIf->pIp6cpPtr;
    Size = sizeof (tIP6CPOptions);

    if (PktType == CONF_REQ_CODE)
    {
        pTemp = &pIp6cpIf->Ip6cpOptionsAckedByLocal;
    }
    else
    {
        pTemp = &pIp6cpIf->Ip6cpOptionsAckedByPeer;
    }

    if (Flag == NOT_SET)
    {
        if ((pCopyPtr = (UINT1 *) ALLOC_STR (Size)) == NULL)
        {
            PRINT_MEM_ALLOC_FAILURE;
            return (DISCARD);
        }
        MEMCPY (pCopyPtr, (UINT1 *) pTemp, Size);
        if (PktType == CONF_REQ_CODE)
        {
            IP6CPOptionInit (&pIp6cpIf->Ip6cpOptionsAckedByLocal);
        }
    }
    else
    {
        if (Flag == SET)
        {
            MEMCPY (pTemp, (tIP6CPOptions *) pCopyPtr, Size);
        }
        if (pCopyPtr != NULL)
        {
            FREE_STR (pCopyPtr);
            pCopyPtr = NULL;
        }
    }
    return (OK);
}

/*********************************************************************
*  Function Name : IP6CPGetSEMPtr
*  Description   :
*    This function returns the pointer to the IP6CP GSEM taking the tPPPIf
*   structure as a parameter.
*  Input         :
*       pIf -   Points to the PPP Interface structure.
*  Return Value  :
*           Returns a pointer to the IP6CP GSEM.
*********************************************************************/
tGSEM              *
IP6CPGetSEMPtr (tPPPIf * pIf)
{
    tIP6CPIf           *pIp6cpIf = NULL;

    if (pIf->pIp6cpPtr != NULL)
    {
        pIp6cpIf = (tIP6CPIf *) pIf->pIp6cpPtr;
        return (&pIp6cpIf->Ip6cpGSEM);
    }
    return (NULL);
}

/*********************************************************************
*  Function Name : IP6CPCompareIntfId
*  Description   :
*    This function compares the two interfaces.
*  Input         :
*         pLocInterfaceId - Pointer to the first interface identifier.
*         pRemInterfaceId - Pointer to the second interface identifier.
*  Return Value  :
*           Returns 
*               LOC_INTFID_ZERO  if Local Interface Identifier is zero.
*               REM_INTFID_ZERO  if Remote Interface Identifier is zero.
*               INTFID_EQUAL     if the two Interface Identifiers are
*                                equal.
*               INTFID_DIFF      if the two Iterface Identifiers are
*                                different.
*********************************************************************/

UINT4
IP6CPCompareIntfId (UINT1 *pLocInterfaceId, UINT1 *pRemInterfaceId)
{
    UINT4               u4RetVal = 0x0000;
    UINT4               u4Cnt = 0;

    if (IP6IsIntfIdZero (pLocInterfaceId) == TRUE)
        u4RetVal |= LOC_INTFID_ZERO;
    if (IP6IsIntfIdZero (pRemInterfaceId) == TRUE)
        u4RetVal |= REM_INTFID_ZERO;
    for (u4Cnt = 0; u4Cnt < INTF_ID_LEN; u4Cnt++)
        if (pLocInterfaceId[u4Cnt] != pRemInterfaceId[u4Cnt])
            break;

    if (u4Cnt < INTF_ID_LEN)
        u4RetVal |= INTFID_DIFF;
    else
        u4RetVal |= INTFID_EQUAL;

    return (u4RetVal);
}

/*********************************************************************
*  Function Name : IP6GenInterfaceId
*  Description   :
*    This function Generates a new Interface Identifier.
*  Input         : 
*                  Array to store the Interface Identifier.
*  Return Value  :
*                  None
*********************************************************************/
VOID
IP6GenInterfaceId (UINT1 *pInterfaceId)
{
    /* We are not using this function.This function is to be 
       updated if the MethodToGenInterfaceId options are changed. */
    /* Currently we assume that this functions returns a valid 
       Interface Identifier */
    UINT4               u4Cnt = 0;

    for (u4Cnt = 0; u4Cnt < INTF_ID_LEN; u4Cnt++)
        pInterfaceId[u4Cnt] = 0xff;
}

/*********************************************************************
*  Function Name : IP6IsIntfIdZero
*  Description   :
*    This function checks if the Interface Identifier is Zero. 
*  Input         :
*       pInterfaceId - Pointer to the Interface Identifier.
*  Return Value  :
*           Returns TRUE/FALSE
*********************************************************************/
UINT4
IP6IsIntfIdZero (UINT1 *pInterfaceId)
{
    UINT1               au1IntfId[INTF_ID_LEN];

    MEMSET (au1IntfId, 0, INTF_ID_LEN);

    if (MEMCMP (pInterfaceId, au1IntfId, INTF_ID_LEN) == 0)
        return TRUE;
    else
        return FALSE;
}

/*********************************************************************
*  Function Name : ALLOC_IP6CP_IF
*  Description   :
*    This function allocates memory to tIP6CPIf pointer and initialises 
*    it with zero.    
*  Input         : VOID
*  Return Value  :
*           Returns the pointer to tIP6CPIf structure
*********************************************************************/
tIP6CPIf           *
ALLOC_IP6CP_IF ()
{
    tIP6CPIf           *pIp6cpPtr = NULL;

    if ((pIp6cpPtr = (tIP6CPIf *) CALLOC (1, sizeof (tIP6CPIf))) != NULL)
    {
        gIP6Counters.u2AllocCounter++;
        return pIp6cpPtr;
    }
    return NULL;
}
