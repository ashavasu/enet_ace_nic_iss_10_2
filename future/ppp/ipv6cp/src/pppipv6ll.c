/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppipv6ll.c,v 1.2 2014/03/11 14:02:50 siva Exp $
 *
 * Description: Low Level Routines for IPv6CP module.
 *
 *******************************************************************/
#define __IPv6_LOW_H__
#include "ipv6cpcom.h"
#include "snmphdrs.h"
#include "pppsnmpm.h"

#include "ipv6exts.h"
#include "globexts.h"
#include "llproto.h"
#include "ppip6low.h"

/******************************************************************************/
INT1
CheckAndGetIp6Ptr (tPPPIf * pIf)
{
    if (pIf->pIp6cpPtr != NULL)
    {
        return (PPP_SNMP_OK);
    }
    return (PPP_SNMP_ERR);
}

/*********************************************************************
*  Function Name : PppGetIP6CPIfPtr
*  Description   :
*  Parameter(s)  :
*   InDataBasePtr -
*   InNamePtr     -
*   Arg           -
*   Value         -
*  Return Value  :
*
*********************************************************************/
tIP6CPIf           *
PppGetIP6CPIfPtr (Index)
     UINT4               Index;
{
    tPPPIf             *pIf = NULL;

    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if ((pIf != NULL) && (pIf->LinkInfo.IfIndex == Index))
            return (pIf->pIp6cpPtr);
    }
    return ((tIP6CPIf *) NULL);
}

/*******************************************************************/
tIP6CPIf           *
PppCreateIP6CPIfPtr (Index)
     UINT4               Index;
{
    tPPPIf             *pIf = NULL;

    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if ((pIf != NULL) && (pIf->LinkInfo.IfIndex == Index))
        {
            if (pIf->BundleFlag != BUNDLE
                && pIf->MPInfo.MemberInfo.pBundlePtr != NULL)
            {
                return (NULL);
            }
            if (pIf->pIp6cpPtr == NULL)
            {
                if ((pIf->pIp6cpPtr = IP6CPCreateIf (pIf)) == NULL)
                {
                    return (NULL);
                }
            }
            return (pIf->pIp6cpPtr);
        }
    }
    return (NULL);
}

/* LOW LEVEL Routines for Table : PppIp6Table. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppIp6Table
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstancePppIp6Table (INT4 i4IfIndex)
{
    if (PppGetIP6CPIfPtr ((UINT4) i4IfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppIp6Table
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexPppIp6Table (INT4 *pi4IfIndex)
{
    if (nmhGetFirstIndex (pi4IfIndex, Ip6Instance, FIRST_INST) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppIp6Table
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppIp6Table (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    if (nmhGetNextIndex (&i4IfIndex, Ip6Instance, NEXT_INST) == SNMP_SUCCESS)
    {
        *pi4NextIfIndex = i4IfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppIp6OperStatus
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppIp6OperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppIp6OperStatus (INT4 i4IfIndex, INT4 *pi4RetValPppIp6OperStatus)
{
    tIP6CPIf           *pIp6If = NULL;

    if ((pIp6If = PppGetIP6CPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValPppIp6OperStatus = pIp6If->u1OperStatus;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPppIp6LocToRemIfId
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppIp6LocToRemIfId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppIp6LocToRemIfId (INT4 i4IfIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValPppIp6LocToRemIfId)
{
    tIP6CPIf           *pIp6If = NULL;

    if ((pIp6If = PppGetIP6CPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pIp6If->u1OperStatus == STATUS_UP)
    {
        pRetValPppIp6LocToRemIfId->i4_Length = 8;
        if (pIp6If->Ip6cpGSEM.pNegFlagsPerIf[IP6_INTFID_IDX].
            FlagMask & ACKED_BY_LOCAL_MASK)
        {
            MEMCPY (pRetValPppIp6LocToRemIfId->pu1_OctetList,
                    (const char *) pIp6If->Ip6cpOptionsAckedByPeer.InterfaceId,
                    pRetValPppIp6LocToRemIfId->i4_Length);
        }
        else
        {
            MEMCPY (pRetValPppIp6LocToRemIfId->pu1_OctetList,
                    (const char *) pIp6If->Ip6cpOptionsDesired.InterfaceId,
                    pRetValPppIp6LocToRemIfId->i4_Length);

        }
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetPppIp6RemToLocIfId
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppIp6RemToLocIfId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppIp6RemToLocIfId (INT4 i4IfIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValPppIp6RemToLocIfId)
{
    tIP6CPIf           *pIp6If = NULL;

    if ((pIp6If = PppGetIP6CPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pIp6If->u1OperStatus == STATUS_UP)
    {
        pRetValPppIp6RemToLocIfId->i4_Length = 8;
        if (pIp6If->Ip6cpGSEM.pNegFlagsPerIf[IP6_INTFID_IDX].
            FlagMask & ACKED_BY_PEER_MASK)
        {
            MEMCPY (pRetValPppIp6RemToLocIfId->pu1_OctetList,
                    (const char *) pIp6If->Ip6cpOptionsAckedByLocal.InterfaceId,
                    pRetValPppIp6RemToLocIfId->i4_Length);
        }
        else
        {
            MEMCPY (pRetValPppIp6RemToLocIfId->pu1_OctetList,
                    (const char *) pIp6If->Ip6cpOptionsAllowedForPeer.
                    InterfaceId, pRetValPppIp6RemToLocIfId->i4_Length);

        }
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetPppIp6LocToRemCompProt
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppIp6LocToRemCompProt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppIp6LocToRemCompProt (INT4 i4IfIndex,
                              INT4 *pi4RetValPppIp6LocToRemCompProt)
{

    tIP6CPIf           *pIp6If = NULL;

    if ((pIp6If = PppGetIP6CPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pIp6If->Ip6cpGSEM.pNegFlagsPerIf[IP6_COMP_IDX].
        FlagMask & ACKED_BY_LOCAL_MASK)
    {
        if (pIp6If->Ip6cpOptionsAckedByLocal.Ip6CompProtocol == IPHC_PROTOCOL)
        {
            *pi4RetValPppIp6LocToRemCompProt = IP6_HEADER_COMPRESSION;
        }
    }
    else
    {
        *pi4RetValPppIp6LocToRemCompProt = IP6_COMPRESSION_NONE;
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppIp6RemToLocCompProt
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppIp6RemToLocCompProt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppIp6RemToLocCompProt (INT4 i4IfIndex,
                              INT4 *pi4RetValPppIp6RemToLocCompProt)
{
    tIP6CPIf           *pIp6If = NULL;

    if ((pIp6If = PppGetIP6CPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pIp6If->u1OperStatus == STATUS_UP)
    {
        if (pIp6If->Ip6cpGSEM.pNegFlagsPerIf[IP6_COMP_IDX].
            FlagMask & ACKED_BY_LOCAL_MASK)
        {
            *pi4RetValPppIp6RemToLocCompProt = 2;
        }
        else
        {
            *pi4RetValPppIp6RemToLocCompProt = 1;
        }
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);

}

/* LOW LEVEL Routines for Table : PppIp6ConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppIp6ConfigTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstancePppIp6ConfigTable (INT4 i4IfIndex)
{
    if (PppGetIP6CPIfPtr ((UINT4) i4IfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppIp6ConfigTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexPppIp6ConfigTable (INT4 *pi4IfIndex)
{
    if (nmhGetFirstIndex (pi4IfIndex, Ip6Instance, FIRST_INST) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppIp6ConfigTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppIp6ConfigTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    if (nmhGetNextIndex (&i4IfIndex, Ip6Instance, NEXT_INST) == SNMP_SUCCESS)
    {
        *pi4NextIfIndex = i4IfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppIp6ConfigAdminStatus
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppIp6ConfigAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppIp6ConfigAdminStatus (INT4 i4IfIndex,
                               INT4 *pi4RetValPppIp6ConfigAdminStatus)
{
    tIP6CPIf           *pIp6If = NULL;

    if ((pIp6If = PppGetIP6CPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValPppIp6ConfigAdminStatus = pIp6If->u1AdminStatus;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPppIp6ConfigLocToRemIfId
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppIp6ConfigLocToRemIfId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppIp6ConfigLocToRemIfId (INT4 i4IfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValPppIp6ConfigLocToRemIfId)
{
    tIP6CPIf           *pIp6If = NULL;

    if ((pIp6If = PppGetIP6CPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pRetValPppIp6ConfigLocToRemIfId->i4_Length = 8;
    MEMCPY (pRetValPppIp6ConfigLocToRemIfId->pu1_OctetList,
            (const char *) pIp6If->Ip6cpOptionsDesired.InterfaceId,
            pRetValPppIp6ConfigLocToRemIfId->i4_Length);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPppIp6ConfigRemToLocIfId
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppIp6ConfigRemToLocIfId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppIp6ConfigRemToLocIfId (INT4 i4IfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValPppIp6ConfigRemToLocIfId)
{
    tIP6CPIf           *pIp6If = NULL;

    if ((pIp6If = PppGetIP6CPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pRetValPppIp6ConfigRemToLocIfId->i4_Length = 8;
    MEMCPY (pRetValPppIp6ConfigRemToLocIfId->pu1_OctetList,
            (const char *) pIp6If->Ip6cpOptionsAllowedForPeer.InterfaceId,
            pRetValPppIp6ConfigRemToLocIfId->i4_Length);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPppIp6ConfigMethodToGenIfId
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppIp6ConfigMethodToGenIfId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppIp6ConfigMethodToGenIfId (INT4 i4IfIndex,
                                   INT4 *pi4RetValPppIp6ConfigMethodToGenIfId)
{
    tIP6CPIf           *pIp6If = NULL;

    if ((pIp6If = PppGetIP6CPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppIp6ConfigMethodToGenIfId = pIp6If->u1MethodToGenIntfId;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPppIp6ConfigCompression
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppIp6ConfigCompression
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppIp6ConfigCompression (INT4 i4IfIndex,
                               INT4 *pi4RetValPppIp6ConfigCompression)
{

    tIP6CPIf           *pIp6If = NULL;

    if ((pIp6If = PppGetIP6CPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if ((pIp6If->Ip6cpGSEM.pNegFlagsPerIf[IP6_COMP_IDX].
         FlagMask & DESIRED_MASK) == 0)
    {
        *pi4RetValPppIp6ConfigCompression = IP6_COMPRESSION_NONE;
    }
    else
    {
        *pi4RetValPppIp6ConfigCompression = IP6_HEADER_COMPRESSION;
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppIp6ConfigRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppIp6ConfigRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppIp6ConfigRowStatus (INT4 i4IfIndex,
                             INT4 *pi4RetValPppIp6ConfigRowStatus)
{
    tIP6CPIf           *pIp6If = NULL;

    if ((pIp6If = PppGetIP6CPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Index Found");
        return (SNMP_FAILURE);
    }

    *pi4RetValPppIp6ConfigRowStatus = ACTIVE;
    return (SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppIp6ConfigAdminStatus
 Input       :  The Indices
                IfIndex

                The Object 
                setValPppIp6ConfigAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppIp6ConfigAdminStatus (INT4 i4IfIndex,
                               INT4 i4SetValPppIp6ConfigAdminStatus)
{
    tIP6CPIf           *pIp6If = NULL;

    if ((pIp6If = PppCreateIP6CPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    switch (i4SetValPppIp6ConfigAdminStatus)
    {
        case ADMIN_OPEN:
            pIp6If->u1AdminStatus = (UINT1) i4SetValPppIp6ConfigAdminStatus;
            IP6CPEnableIf (pIp6If);
            break;
        case ADMIN_CLOSE:
            pIp6If->u1AdminStatus = (UINT1) i4SetValPppIp6ConfigAdminStatus;
            IP6CPDisableIf (pIp6If);
            break;
        default:
            break;
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetPppIp6ConfigLocToRemIfId
 Input       :  The Indices
                IfIndex

                The Object 
                setValPppIp6ConfigLocToRemIfId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppIp6ConfigLocToRemIfId (INT4 i4IfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValPppIp6ConfigLocToRemIfId)
{
    tIP6CPIf           *pIp6If = NULL;

    if ((pIp6If = PppCreateIP6CPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIp6If->Ip6cpGSEM.pNegFlagsPerIf[IP6_INTFID_IDX].FlagMask |= DESIRED_SET;
    pSetValPppIp6ConfigLocToRemIfId->i4_Length = 8;
    MEMCPY (pIp6If->Ip6cpOptionsDesired.InterfaceId,
            pSetValPppIp6ConfigLocToRemIfId->pu1_OctetList,
            pSetValPppIp6ConfigLocToRemIfId->i4_Length);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetPppIp6ConfigRemToLocIfId
 Input       :  The Indices
                IfIndex

                The Object 
                setValPppIp6ConfigRemToLocIfId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppIp6ConfigRemToLocIfId (INT4 i4IfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValPppIp6ConfigRemToLocIfId)
{
    tIP6CPIf           *pIp6If = NULL;

    if ((pIp6If = PppCreateIP6CPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIp6If->Ip6cpGSEM.pNegFlagsPerIf[IP6_INTFID_IDX].FlagMask |=
        ALLOWED_FOR_PEER_SET;
    pSetValPppIp6ConfigRemToLocIfId->i4_Length = 8;
    MEMCPY (pIp6If->Ip6cpOptionsAllowedForPeer.InterfaceId,
            pSetValPppIp6ConfigRemToLocIfId->pu1_OctetList,
            pSetValPppIp6ConfigRemToLocIfId->i4_Length);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetPppIp6ConfigMethodToGenIfId
 Input       :  The Indices
                IfIndex

                The Object 
                setValPppIp6ConfigMethodToGenIfId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppIp6ConfigMethodToGenIfId (INT4 i4IfIndex,
                                   INT4 i4SetValPppIp6ConfigMethodToGenIfId)
{
    tIP6CPIf           *pIp6If = NULL;

    if ((pIp6If = PppCreateIP6CPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    switch (i4SetValPppIp6ConfigMethodToGenIfId)
    {
        case IEEE_GLOBAL_ID:
        case LOCAL_MAC_ADDRESS:
        case RANDOM_NUMBER:
            return (SNMP_FAILURE);
        case MANUALLY:
            pIp6If->u1MethodToGenIntfId =
                (UINT1) i4SetValPppIp6ConfigMethodToGenIfId;
            break;
        default:
            break;

            /* write appropriate functions to generate 
               Interface Identifier by various methods 
               and call nmhSetPppIp6ConfigLocToRemIfId */
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetPppIp6ConfigCompression
 Input       :  The Indices
                IfIndex

                The Object 
                setValPppIp6ConfigCompression
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppIp6ConfigCompression (INT4 i4IfIndex,
                               INT4 i4SetValPppIp6ConfigCompression)
{

    tIP6CPIf           *pIp6If = NULL;

    if ((pIp6If = PppGetIP6CPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (i4SetValPppIp6ConfigCompression == IP6_COMPRESSION_NONE)
    {

        pIp6If->Ip6cpGSEM.pNegFlagsPerIf[IP6_COMP_IDX].FlagMask &=
            DESIRED_NOT_SET;
    }

    if (i4SetValPppIp6ConfigCompression == IP6_HEADER_COMPRESSION)
    {

        pIp6If->Ip6cpOptionsDesired.Ip6CompProtocol = IPHC_PROTOCOL;

        pIp6If->Ip6cpGSEM.pNegFlagsPerIf[IP6_COMP_IDX].FlagMask |= DESIRED_SET;

        if (pIp6If->u1OperStatus == STATUS_DOWN)
        {
            pIp6If->Ip6cpGSEM.pNegFlagsPerIf[IP6_COMP_IDX].FlagMask |=
                ACKED_BY_PEER_SET;
        }

        return (SNMP_SUCCESS);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppIp6ConfigRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                setValPppIp6ConfigRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppIp6ConfigRowStatus (INT4 i4IfIndex, INT4 i4SetValPppIp6ConfigRowStatus)
{
    tIP6CPIf           *pIp6If = NULL;

    switch (i4SetValPppIp6ConfigRowStatus)
    {
        case CREATE_AND_GO:
            if ((pIp6If = PppCreateIP6CPIfPtr ((UINT4) i4IfIndex)) == NULL)
            {
                PPP_TRC (MGMT, "Creation of Interface Failed");
                return (SNMP_FAILURE);
            }
            pIp6If->u1RowStatus = ACTIVE;
            break;

        case DESTROY:
            if ((pIp6If = PppCreateIP6CPIfPtr ((UINT4) i4IfIndex)) == NULL)
            {
                PPP_TRC (MGMT, "Interface not created");
                return (SNMP_FAILURE);
            }
            PPP_TRC (PPP_MUST, " \t\tIPv6CP Interface Deleted\n");
            IP6CPDisableIf (pIp6If);
            IP6CPDeleteIf (pIp6If);
            break;
        default:
            break;
    }
    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppIp6ConfigAdminStatus
 Input       :  The Indices
                IfIndex

                The Object 
                testValPppIp6ConfigAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppIp6ConfigAdminStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                  INT4 i4TestValPppIp6ConfigAdminStatus)
{
    tIP6CPIf           *pIp6If = NULL;

    if ((pIp6If = PppGetIP6CPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((i4TestValPppIp6ConfigAdminStatus != ADMIN_OPEN) &&
        (i4TestValPppIp6ConfigAdminStatus != ADMIN_CLOSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppIp6ConfigLocToRemIfId
 Input       :  The Indices
                IfIndex

                The Object 
                testValPppIp6ConfigLocToRemIfId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppIp6ConfigLocToRemIfId (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValPppIp6ConfigLocToRemIfId)
{

    if (PppGetIP6CPIfPtr ((UINT4) i4IfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pTestValPppIp6ConfigLocToRemIfId->i4_Length != INTF_ID_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppIp6ConfigRemToLocIfId
 Input       :  The Indices
                IfIndex

                The Object 
                testValPppIp6ConfigRemToLocIfId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppIp6ConfigRemToLocIfId (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValPppIp6ConfigRemToLocIfId)
{
    if (PppGetIP6CPIfPtr ((UINT4) i4IfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pTestValPppIp6ConfigRemToLocIfId->i4_Length != INTF_ID_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppIp6ConfigMethodToGenIfId
 Input       :  The Indices
                IfIndex

                The Object 
                testValPppIp6ConfigMethodToGenIfId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppIp6ConfigMethodToGenIfId (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                      INT4 i4TestValPppIp6ConfigMethodToGenIfId)
{
    if (PppGetIP6CPIfPtr ((UINT4) i4IfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (i4TestValPppIp6ConfigMethodToGenIfId != MANUALLY)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppIp6ConfigCompression
 Input       :  The Indices
                IfIndex

                The Object 
                testValPppIp6ConfigCompression
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppIp6ConfigCompression (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                  INT4 i4TestValPppIp6ConfigCompression)
{

    tIP6CPIf           *pIp6If = NULL;

    if ((pIp6If = PppGetIP6CPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((i4TestValPppIp6ConfigCompression != IP6_COMPRESSION_NONE)
        && (i4TestValPppIp6ConfigCompression != IP6_HEADER_COMPRESSION))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppIp6ConfigRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                testValPppIp6ConfigRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppIp6ConfigRowStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                INT4 i4TestValPppIp6ConfigRowStatus)
{
    tIP6CPIf           *pIp6If = NULL;

    switch (i4TestValPppIp6ConfigRowStatus)
    {
        case CREATE_AND_GO:
            if ((pIp6If = PppGetIP6CPIfPtr ((UINT4) i4IfIndex)) != NULL)
            {
                PPP_TRC (MGMT, "Interface Already Created");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return (SNMP_FAILURE);
            }
            break;
        case DESTROY:
            if ((pIp6If = PppGetIP6CPIfPtr ((UINT4) i4IfIndex)) == NULL)
            {
                PPP_TRC (MGMT, "Interface Not Created");
                return (SNMP_FAILURE);
            }
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (SNMP_FAILURE);
            break;
    }
    return (SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : PppExtIp6CompConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppExtIp6CompConfigTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppExtIp6CompConfigTable (INT4 i4IfIndex)
{
    if (PppGetIP6CPIfPtr ((UINT4) i4IfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppExtIp6CompConfigTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppExtIp6CompConfigTable (INT4 *pi4IfIndex)
{
    if (nmhGetFirstIndex (pi4IfIndex, Ip6Instance, FIRST_INST) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppExtIp6CompConfigTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppExtIp6CompConfigTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    if (nmhGetNextIndex (&i4IfIndex, Ip6Instance, NEXT_INST) == SNMP_SUCCESS)
    {
        *pi4NextIfIndex = i4IfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppExtIp6ConfigTcpSpace
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppExtIp6ConfigTcpSpace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIp6ConfigTcpSpace (INT4 i4IfIndex,
                               INT4 *pi4RetValPppExtIp6ConfigTcpSpace)
{
    tIP6CPIf           *pIp6cpIf;
    tIPHCInfo          *pIPHCInfo;

    if ((pIp6cpIf = PppGetIP6CPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIPHCInfo = &pIp6cpIf->Ip6cpOptionsDesired.IPHCInfo;

    *pi4RetValPppExtIp6ConfigTcpSpace = pIPHCInfo->u2TcpSpace;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetPppExtIp6ConfigNonTcpSpace
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppExtIp6ConfigNonTcpSpace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIp6ConfigNonTcpSpace (INT4 i4IfIndex,
                                  INT4 *pi4RetValPppExtIp6ConfigNonTcpSpace)
{
    tIP6CPIf           *pIp6cpIf;
    tIPHCInfo          *pIPHCInfo;

    if ((pIp6cpIf = PppGetIP6CPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIPHCInfo = &pIp6cpIf->Ip6cpOptionsDesired.IPHCInfo;

    *pi4RetValPppExtIp6ConfigNonTcpSpace = pIPHCInfo->u2NonTcpSpace;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetPppExtIp6ConfigFMaxPeriod
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppExtIp6ConfigFMaxPeriod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIp6ConfigFMaxPeriod (INT4 i4IfIndex,
                                 INT4 *pi4RetValPppExtIp6ConfigFMaxPeriod)
{
    tIP6CPIf           *pIp6cpIf;
    tIPHCInfo          *pIPHCInfo;

    if ((pIp6cpIf = PppGetIP6CPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIPHCInfo = &pIp6cpIf->Ip6cpOptionsDesired.IPHCInfo;

    *pi4RetValPppExtIp6ConfigFMaxPeriod = pIPHCInfo->u2FMaxPeriod;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetPppExtIp6ConfigFMaxTime
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppExtIp6ConfigFMaxTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIp6ConfigFMaxTime (INT4 i4IfIndex,
                               INT4 *pi4RetValPppExtIp6ConfigFMaxTime)
{
    tIP6CPIf           *pIp6cpIf;
    tIPHCInfo          *pIPHCInfo;

    if ((pIp6cpIf = PppGetIP6CPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIPHCInfo = &pIp6cpIf->Ip6cpOptionsDesired.IPHCInfo;

    *pi4RetValPppExtIp6ConfigFMaxTime = pIPHCInfo->u2FMaxTime;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetPppExtIp6ConfigMaxHeader
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppExtIp6ConfigMaxHeader
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIp6ConfigMaxHeader (INT4 i4IfIndex,
                                INT4 *pi4RetValPppExtIp6ConfigMaxHeader)
{
    tIP6CPIf           *pIp6cpIf;
    tIPHCInfo          *pIPHCInfo;

    if ((pIp6cpIf = PppGetIP6CPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIPHCInfo = &pIp6cpIf->Ip6cpOptionsDesired.IPHCInfo;

    *pi4RetValPppExtIp6ConfigMaxHeader = pIPHCInfo->u2MaxHdr;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetPppExtIp6ConfigRtpCompression
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppExtIp6ConfigRtpCompression
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIp6ConfigRtpCompression (INT4 i4IfIndex,
                                     INT4
                                     *pi4RetValPppExtIp6ConfigRtpCompression)
{
    tIP6CPIf           *pIp6cpIf;
    tIPHCInfo          *pIPHCInfo;

    if ((pIp6cpIf = PppGetIP6CPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIPHCInfo = &pIp6cpIf->Ip6cpOptionsDesired.IPHCInfo;

    *pi4RetValPppExtIp6ConfigRtpCompression = pIPHCInfo->u2RtpSubOptFlag;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppExtIp6ConfigTcpSpace
 Input       :  The Indices
                IfIndex

                The Object 
                setValPppExtIp6ConfigTcpSpace
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIp6ConfigTcpSpace (INT4 i4IfIndex,
                               INT4 i4SetValPppExtIp6ConfigTcpSpace)
{
    tIP6CPIf           *pIp6cpIf;
    tIPHCInfo          *pIPHCInfo;

    if ((pIp6cpIf = PppGetIP6CPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIPHCInfo = &pIp6cpIf->Ip6cpOptionsDesired.IPHCInfo;

    pIPHCInfo->u2TcpSpace = (UINT2) i4SetValPppExtIp6ConfigTcpSpace;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetPppExtIp6ConfigNonTcpSpace
 Input       :  The Indices
                IfIndex

                The Object 
                setValPppExtIp6ConfigNonTcpSpace
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIp6ConfigNonTcpSpace (INT4 i4IfIndex,
                                  INT4 i4SetValPppExtIp6ConfigNonTcpSpace)
{
    tIP6CPIf           *pIp6cpIf;
    tIPHCInfo          *pIPHCInfo;

    if ((pIp6cpIf = PppGetIP6CPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIPHCInfo = &pIp6cpIf->Ip6cpOptionsDesired.IPHCInfo;

    pIPHCInfo->u2NonTcpSpace = (UINT2) i4SetValPppExtIp6ConfigNonTcpSpace;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetPppExtIp6ConfigFMaxPeriod
 Input       :  The Indices
                IfIndex

                The Object 
                setValPppExtIp6ConfigFMaxPeriod
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIp6ConfigFMaxPeriod (INT4 i4IfIndex,
                                 INT4 i4SetValPppExtIp6ConfigFMaxPeriod)
{
    tIP6CPIf           *pIp6cpIf;
    tIPHCInfo          *pIPHCInfo;

    if ((pIp6cpIf = PppGetIP6CPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIPHCInfo = &pIp6cpIf->Ip6cpOptionsDesired.IPHCInfo;

    pIPHCInfo->u2FMaxPeriod = (UINT2) i4SetValPppExtIp6ConfigFMaxPeriod;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetPppExtIp6ConfigFMaxTime
 Input       :  The Indices
                IfIndex

                The Object 
                setValPppExtIp6ConfigFMaxTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIp6ConfigFMaxTime (INT4 i4IfIndex,
                               INT4 i4SetValPppExtIp6ConfigFMaxTime)
{
    tIP6CPIf           *pIp6cpIf;
    tIPHCInfo          *pIPHCInfo;

    if ((pIp6cpIf = PppGetIP6CPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIPHCInfo = &pIp6cpIf->Ip6cpOptionsDesired.IPHCInfo;

    pIPHCInfo->u2FMaxTime = (UINT2) i4SetValPppExtIp6ConfigFMaxTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetPppExtIp6ConfigMaxHeader
 Input       :  The Indices
                IfIndex

                The Object 
                setValPppExtIp6ConfigMaxHeader
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIp6ConfigMaxHeader (INT4 i4IfIndex,
                                INT4 i4SetValPppExtIp6ConfigMaxHeader)
{
    tIP6CPIf           *pIp6cpIf;
    tIPHCInfo          *pIPHCInfo;

    if ((pIp6cpIf = PppGetIP6CPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIPHCInfo = &pIp6cpIf->Ip6cpOptionsDesired.IPHCInfo;

    pIPHCInfo->u2MaxHdr = (UINT2) i4SetValPppExtIp6ConfigMaxHeader;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetPppExtIp6ConfigRtpCompression
 Input       :  The Indices
                IfIndex

                The Object 
                setValPppExtIp6ConfigRtpCompression
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIp6ConfigRtpCompression (INT4 i4IfIndex,
                                     INT4 i4SetValPppExtIp6ConfigRtpCompression)
{
    tIP6CPIf           *pIp6cpIf;
    tIPHCInfo          *pIPHCInfo;

    if ((pIp6cpIf = PppGetIP6CPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    pIPHCInfo = &pIp6cpIf->Ip6cpOptionsDesired.IPHCInfo;

    pIPHCInfo->u2RtpSubOptFlag = (UINT2) i4SetValPppExtIp6ConfigRtpCompression;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppExtIp6ConfigTcpSpace
 Input       :  The Indices
                IfIndex

                The Object 
                testValPppExtIp6ConfigTcpSpace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIp6ConfigTcpSpace (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                  INT4 i4TestValPppExtIp6ConfigTcpSpace)
{
    if (PppGetIP6CPIfPtr ((UINT4) i4IfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValPppExtIp6ConfigTcpSpace < 0) ||
        (i4TestValPppExtIp6ConfigTcpSpace > MAX_IPHC_TCP_SPACE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PppExtIp6ConfigNonTcpSpace
 Input       :  The Indices
                IfIndex

                The Object 
                testValPppExtIp6ConfigNonTcpSpace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIp6ConfigNonTcpSpace (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                     INT4 i4TestValPppExtIp6ConfigNonTcpSpace)
{
    if (PppGetIP6CPIfPtr ((UINT4) i4IfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValPppExtIp6ConfigNonTcpSpace < 0) ||
        (i4TestValPppExtIp6ConfigNonTcpSpace > MAX_IPHC_NON_TCP_SPACE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PppExtIp6ConfigFMaxPeriod
 Input       :  The Indices
                IfIndex

                The Object 
                testValPppExtIp6ConfigFMaxPeriod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIp6ConfigFMaxPeriod (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                    INT4 i4TestValPppExtIp6ConfigFMaxPeriod)
{
    PPP_UNUSED (i4TestValPppExtIp6ConfigFMaxPeriod);
    if (PppGetIP6CPIfPtr ((UINT4) i4IfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PppExtIp6ConfigFMaxTime
 Input       :  The Indices
                IfIndex

                The Object 
                testValPppExtIp6ConfigFMaxTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIp6ConfigFMaxTime (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                  INT4 i4TestValPppExtIp6ConfigFMaxTime)
{
    PPP_UNUSED (i4TestValPppExtIp6ConfigFMaxTime);
    if (PppGetIP6CPIfPtr ((UINT4) i4IfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PppExtIp6ConfigMaxHeader
 Input       :  The Indices
                IfIndex

                The Object 
                testValPppExtIp6ConfigMaxHeader
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIp6ConfigMaxHeader (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                   INT4 i4TestValPppExtIp6ConfigMaxHeader)
{
    PPP_UNUSED (i4TestValPppExtIp6ConfigMaxHeader);
    if (PppGetIP6CPIfPtr ((UINT4) i4IfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PppExtIp6ConfigRtpCompression
 Input       :  The Indices
                IfIndex

                The Object 
                testValPppExtIp6ConfigRtpCompression
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIp6ConfigRtpCompression (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                        INT4
                                        i4TestValPppExtIp6ConfigRtpCompression)
{
    if (PppGetIP6CPIfPtr ((UINT4) i4IfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValPppExtIp6ConfigRtpCompression != PPP_TRUE)
        && (i4TestValPppExtIp6ConfigRtpCompression != PPP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
