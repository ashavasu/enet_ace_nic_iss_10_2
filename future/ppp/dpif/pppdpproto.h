/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppdpproto.h,v 1.2 2014/03/11 14:02:47 siva Exp $
 *
 * Description:This header file contains prototypes for CP interface 
 *             routines 
 *
 *******************************************************************/

#ifndef  _PPPDP_PROTO_H_
#define _PPPDP_PROTO_H_

INT4 PppLCPUpToDp(tPPPIf * pIf);
INT4 PppIPCPUpToDp(tPPPIf * pIf);
INT4 PppCPDownToDp (tPPPIf * pIf, UINT2 u2Proto);
INT4 PppCreatePppInterfaceInDp (tPPPIf * pIf);
INT4 PppUpdatePppInterfaceInDp(tPPPIf * pIf);
INT4 PppCreateMpInterfaceToDP (tPPPIf * pIf);
INT4 MPInsertMemberIntoBundleToDP (tPPPIf * pMemberIf, tPPPIf * pBundleIf);
INT4 MPAddLinkToBundleToDP (tPPPIf * pMemberIf, tPPPIf * pBundleIf);
INT4 MPDeleteMemberFromBundleToDP (tPPPIf * pMemberIf, tPPPIf * pBundleIf);
INT4 MPRemoveLinkFromBundleToDP (tPPPIf * pMemberIf, tPPPIf * pBundleIf);
INT4 PPPDeleteIfToDP (tPPPIf * pIf);
INT4 IPv6CPUpToDP (tPPPIf * pIf);
INT4 MPLSCPUpToDP (tPPPIf * pIf);
INT4 MuxCPUpToDP (tPPPIf * pIf);
INT4 PppLinkConfigFramingToDP (tPPPIf * pIf);
INT4 PPPLLICallSetUpToDP (tPPPIf * pIf);
INT4 PPPLLICallTerminateToDP (tPPPIf * pIf);
#include "pppoecom.h"
INT4 PPPoEAddNewSessionToDp(tPPPoESessionNode * pSession);
INT4 PPPoEDeleteSessionFromDp (tPPPoESessionNode * pSession);
INT4 PPPoESessionDp (ePPPoESession ePPPoEType, tPPPoESessionNode * pSession);
#endif
