/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppdpport.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This is the header file of CP contains data structures
 *             and other data
 *******************************************************************/

#ifndef _PPPDP_PORT_H_
#define _PPPDP_PORT_H_

#define  PPP_MODULE         1   
#define PPP_DATA_PKT        3


    /* Message Types */

#define  PPP_CTRL_PKT       1   
#define  PPP_INTF_CREATE    2   
#define  MP_INTF_CREATE     3   
#define  PPP_INTF_DELETE    4   
#define  MP_COMP_BDL        5   
#define  MP_INTF_UPDATE     6   
#define  PPP_CFG_REQ        7   
#define  PPP_LL_STATUS      8   

#define  PPP_LCP_UP         10  
#define  PPP_IPCP_UP        11  
#define  PPP_IPV6CP_UP      12  
#define  PPP_MPLSCP_UP      13  
#define  PPP_MUXCP_UP       14  
#define  PPP_CP_DOWN        15  
#define  PPP_DIALUP_REQ     16  


    /* subtypes  for PPP_CFG_REQ */
#define  PPP_CFG_FRAMING    1   


#define  PPP_BRING_UP_LL    1   
#define  PPP_BRING_DOWN_LL  2   

#define  PPP_MP             23  
#define  PPP_PPP            108 
#define  MAX_EPD_LEN        20  
#define   PPP_YES           1
#define   PPP_NO            0

    /* Sub Types for PPP_DIALUP_REQ */

#define  PPP_BRINGUP_LL     1   
#define  PPP_BRINGDN_LL     2   

    /* Misc */
#define  MAX_PDU_LEN        256 




    /* Data Structures */

typedef struct {
    UINT4           u4SrcMod;
    UINT4           u4DestMod;
    UINT4           u4MsgType;
    UINT4           u4Len;
}tPppMsgHdr;

typedef struct{
    UINT4           u4IfIdx;
    UINT4           u4Status;
}tPppLlStat;


    /* For LCP Options */


typedef struct {
    UINT4           u4NegMask;
    UINT4           MagicNumber;
    UINT2           u2MRU;
    UINT1           u1FCSSize;
    UINT1           u1MaxPadVal;
    UINT1           au1ACCM[4];
} tPppLcpOptions;

typedef struct {
    UINT4           u4IfIdx;
    tPppLcpOptions  LocToRemote;
    tPppLcpOptions  RemoteToLoc;
}tPppLcpUp;


    /* for IPHC */

typedef struct {
   UINT1 CompSlotId;
   UINT1 MaxSlotId;
    UINT2          u2TcpSpace;
    UINT2          u2NonTcpSpace;
    UINT2          u2FMaxPeriod;
    UINT2          u2FMaxTime;
    UINT2          u2RtpSubOptFlag;
    UINT2          u2MaxHdr;

}tPppIpHdrComp;


typedef struct {
    UINT2          u2TcpSpace;
    UINT2          u2NonTcpSpace;
    UINT2          u2FMaxPeriod;
    UINT2          u2FMaxTime;
    UINT2          u2RtpSubOptFlag;
    UINT2          u2MaxHdr;
}tPppIp6HdrComp;





    /* For IPCP */

typedef struct {
    UINT4           u4NegMask;
    UINT4           u4IpAddress;
    tPppIpHdrComp  IpHdrComp;
    UINT2           u2CompProto;
    UINT1           au1WordAlignment[2];
} tPppIPCPOpt;

typedef struct{
    UINT4           u4IfIdx; 
    tPppIPCPOpt     LocToRemote;
    tPppIPCPOpt     RemoteToLoc;
}tPppIPCPUp;

typedef struct{
    UINT4           u4IfIdx;
    UINT2           u2Proto;
    UINT1           au1WordAlignment[2];
} tPppCpDown;


    /* For IPv6CP */

typedef struct {
    UINT4           u4NegMask;
    UINT1           au1IntfToken[8];
    tPppIp6HdrComp   CompInfo;
    UINT2           u2CompProto;
    UINT1           au1WordAlignment[2];
} tPppIPv6CPOpt;

typedef struct {
    UINT4           u4IfIdx; 
    tPppIPv6CPOpt   LocToRemote;
    tPppIPv6CPOpt   RemoteToLoc;
}tPppIPv6CPUp;


    /* For PPPMuxCP */

typedef struct {
    UINT4           u4IfIdx; 
    UINT2           u2LocToRemDefPID;
    UINT2           u2RemToLocDefPID;
    UINT2           u2MaxSubFrmLen;
    UINT1           au1WordAlignment[2];
}tPppMuxCPUp;


    /* For MPLS CP */

typedef struct{
    UINT4           u4IfIdx;
} tPppMplsCPUp;


    /* Misc data */
#define PPP_MAX_MSG_SIZE    1500

/* Config Request */
typedef struct {
    UINT4           u4IfIdx;
    UINT1           u1LinkFraming;
    UINT1           au1WordAlignment[3];
}tPppCfgReq;

typedef struct {
    UINT4           u4IfIdx; 
    UINT4           u4Type;
    UINT4           u4Len;
    UINT1           au1Value[1];
    UINT1           au1WordAlignment[3];
}tPppREQandResp;


typedef struct{
    UINT4           u4IfIdx; 
    UINT4           u4ErrType;
}tPppErr;

typedef struct{
    UINT4           u4IfIdx;
    UINT4           u4PktSize;
}tPppCtrlPkt;


typedef struct{
    UINT4           u4IfIdx; 
    UINT4           u4TimerType;
}tPppTimerExpiry;

typedef struct{
   UINT4            u4IfIdx;
   UINT4            u4PhysPortNo;
   UINT2            u2IfType;
   UINT1            u1FrmFlg;
   UINT1            u1WordAlignment;
}tPppIntfCreate;


typedef struct{
   UINT4            u4IfIdx;
   UINT1            u1IfType;
   UINT1            au1WordAlignment[3];
}tPppIntfDelete;


typedef struct{
    UINT4           u4BdlIdx;
} tPppMPIntfCreate;


typedef struct{
    UINT4           u4BdlIdx;
    UINT4           u4MbrIdx;
    UINT1           u1Cmd;
    UINT1           au1WordAlignment[3];
}tPppCompBdl;

typedef struct {
    INT1           i1Class;
    INT1           i1PrefixLen;
    INT1           i1Align [2];
    UINT1          u1Prefix[PPP_MP_PREFIX_MAX_LEN];
}tPppMCMLPrefix;

typedef struct {
    UINT1           u1Code;
    UINT1           u1SuspClass;
    UINT1           au1WordAlignment[2];
} tPppMLHdrFmt;

typedef struct {
    UINT1           u1Len;
    UINT1           u1Class;
    UINT1           au1WordAlignment[2];
    UINT1           au1Address[MAX_EPD_LEN];
} tPppBundleId;

typedef struct {
    UINT4             u4NegMask; 
    UINT2             u2MRRU;
    UINT1             au1WordAlignment[2];
    tPppBundleId      BundleId;
    tPppMLHdrFmt      MPHdrFormat;
    tPppMCMLPrefix    aPrefixInfo[PPP_MAX_CLASS_NUMBERS];
} tPppBundleOpt;

typedef struct {
    UINT4           u4MbrIdx;
    UINT4           u4BdlIdx;
    tPppBundleOpt   LocToRemote;
    tPppBundleOpt   RemoteToLoc;
} tPppBundleUpdate;

typedef struct {
    UINT4           u4BadAddressCounter;
    UINT4           u4BadControlCounter;
    UINT4           u4BadFcsCounter;
    UINT4           u4InDiscards;
    UINT4           u4InErrors; 
    UINT4           u4InGoodOctets;
    UINT4           InNUniPackets;
    UINT4           u4InOctets; 
    UINT4           u4InUnknownProtos; 
    UINT4           u4OutDiscards; 
    UINT4           u4OutNUniPackets; 
    UINT4           u4OutOctets; 
    UINT4           u4PacketTooLongCounter; 
} tPppDpCounters;

typedef struct {
    UINT4           u4IfIdx; 
    UINT4           u4CfgType ;
    UINT4           u4LinkFraming;
} tPppDpConf;


typedef struct {
    UINT4           u4IfIdx;
    UINT4           u4BadAddressCounter;        
    UINT4           u4BadControlCounter;         
    UINT4           u4BadFcsCounter;             
    UINT4           u4InDiscards;                   
    UINT4           u4InErrors;                      
    UINT4           u4InUniPackets;                
    UINT4           u4InOctets;                      
    UINT4           u4OutDiscards;                 
    UINT4           u4OutUniPackets;            
    UINT4           u4OutOctets;                      
    UINT4           u4PacketTooLongCounter;   
} tPppStats;

typedef struct {
    UINT4           u4IfIdx;
    UINT1           u1Cmd;
    UINT1           au1WordAlignment[3];
} tPppDialUpReq;



/* Function prototypes for the API's */

INT4    PppSendMessageToCP(UINT4 u4MsgType, UINT1 *pMsg, UINT4 u4Len);
INT4    PppInformCpLlOperstatus (UINT4 u4IfIndex, UINT4 u4Status);
INT4    PppHandlePacketFromLl (t_MSG_DESC * , UINT4 , UINT4 ,UINT1);
INT4    PppTxCtrlPktToCP(UINT4 , UINT1 * , UINT4 );

/* Mask Value for Options */

/* The following values are got from the array
 *  * LCPGenOptionInfo in ppplcp.c. It should
 *   * be in sync with the array defined there
 *  
 */

#define MP_MRRU_MASK            0x0001
#define MP_SEQ_MASK             0x0002
#define MP_EPD_MASK             0x0004
#define MP_MLHDR_MASK           0x0008


#define LCP_MRU_MASK            0x0010
#define LCP_ACCM_MASK           0x0020
#define LCP_AUTH_MASK           0x0040
#define LCP_LQR_MASK            0x0080
#define LCP_MAGIC_MASK          0x0100
#define LCP_PFC_MASK            0x0200
#define LCP_ACFC_MASK           0x0400
#define LCP_FCS_MASK            0x0800
#define LCP_PAD_MASK            0x1000
#define LCP_CALLBACK_MASK       0x2000
#define LCP_INTL_MASK           0x4000


/* The following values are got from the array
 *  * IPCPGenOptionInfo in pppipcp.c. It should
 *   * be in sync with the array defined there
 *    
 */

#define IPCP_COMP_MASK          0x0001
#define IPCP_ADDR_MASK          0x0002
#define IPCP_PRI_DNS_MASK       0x0004
#define IPCP_PRI_NBNS_MASK      0x0008
#define IPCP_SEC_DNS_MASK       0x0010
#define IPCP_SEC_NBNS_MASK      0x0020


/* The following values are got from the array
 * IP6CPGenOptionInfo in pppipv6cp.c. It should
 * be in sync with the array defined there
 */

#define IPV6_INTF_MASK          0x0001
#define IPV6_COMP_MASK          0x0002


/* The following values are got from the array
 * MuxCPGenOptionInfo in pppmuxcp.c. It should
 * be in sync with the array defined there
 * 
 */

#define MUXCP_PID_MASK          0x0001

#endif
