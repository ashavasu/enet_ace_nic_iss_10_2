/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppdpcom.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

#ifndef _DPCP_COM_H_
#define _DPCP_COM_H_

#include    "lr.h"
#include    "sllmap.h"
#include    "pppcom.h"
#include    "lcpdefs.h"
#include    "gsemdefs.h"
#include    "genexts.h"
#include    "ppptask.h"
#include    "gcpdefs.h"
#include    "lcpprot.h"
#include    "pppproto.h"
#include    "globexts.h"
#include    "pppipcp.h"
#include    "pppipv6cp.h"
#include    "pppmuxcp.h"
#include    "pppmplscp.h"
#include    "cfa.h"
#include    "ppp.h"
#include    "pppnp.h"
#include    "npapi.h"
#include    "ipnp.h"
#include    "pppdpproto.h"

#endif
