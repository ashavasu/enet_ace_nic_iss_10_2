/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppdpif.c,v 1.5 2014/08/28 11:17:48 siva Exp $
 *
 * Description:This file contains PPP Control plane interface 
 *              related procedures.
 *
 *******************************************************************/

#include "pppdpcom.h"
#ifdef FLOWMGR_WANTED
#include "flowmgr.h"
#endif
#include "secmod.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : PppLCPUpToDp                                               */
/*                                                                           */
/* Description  : This function is called to inform the DP, when LCP         */
/*                    negotiations successfully completed and the LCP has    */
/*                    reached to Open state                                  */
/*                                                                           */
/* Input        : pIf  : pointer to negotiated LCP parameters for particular */
/*                          interface                                        */
/* Output       : none                                                       */
/*                                                                           */
/* Returns      : PPP_SUCCESS / PPP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
PppLCPUpToDp (tPPPIf * pIf)
{
    INT4                i4Status = PPP_SUCCESS;
    UINT1               u1LocToRemPFC = NOT_SET;
    UINT1               u1RemToLocPFC = NOT_SET;
    UINT1               u1LocToRemACFC = NOT_SET;
    UINT1               u1RemToLocACFC = NOT_SET;

    if (pIf->LcpGSEM.pNegFlagsPerIf[PFC_IDX].FlagMask & ACKED_BY_LOCAL_MASK)
    {
        u1LocToRemPFC = SET;
    }
    if (pIf->LcpGSEM.pNegFlagsPerIf[PFC_IDX].FlagMask & ACKED_BY_PEER_MASK)
    {
        u1RemToLocPFC = SET;
    }
    if (pIf->LcpGSEM.pNegFlagsPerIf[ACFC_IDX].FlagMask & ACKED_BY_LOCAL_MASK)
    {
        u1LocToRemACFC = SET;
    }
    if (pIf->LcpGSEM.pNegFlagsPerIf[ACFC_IDX].FlagMask & ACKED_BY_PEER_MASK)
    {
        u1RemToLocACFC = SET;
    }
#ifdef NPAPI_WANTED
    if (FNP_FAILURE == FsNpPppLCPUp (pIf->LinkInfo.IfIndex,
                                     pIf->LcpOptionsAckedByPeer.MRU,
                                     pIf->LcpOptionsAckedByLocal.MRU,
                                     u1LocToRemPFC, u1RemToLocPFC,
                                     u1LocToRemACFC, u1RemToLocACFC))
    {
        i4Status = PPP_FAILURE;
    }
#endif

    PPP_UNUSED (u1LocToRemPFC);
    PPP_UNUSED (u1RemToLocPFC);
    PPP_UNUSED (u1LocToRemACFC);
    PPP_UNUSED (u1RemToLocACFC);

    return (i4Status);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : PppUpdatePppInterfaceInDp                                  */
/*                                                                           */
/* Description  : This function is called update the ppp interface in Data   */
/*                plane                                                      */
/*                                                                           */
/* Input        : pIf   : pointer to particular interface                    */
/* Output       : none                                                       */
/*                                                                           */
/* Returns      : PPP_SUCCESS / PPP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT4
PppUpdatePppInterfaceInDp (tPPPIf * pIf)
{
    INT4                i4Status = PPP_SUCCESS;
#ifdef NPAPI_WANTED
    if (FNP_FAILURE == FsNpUpdPppIf (pIf->LinkInfo.IfIndex,
                                     pIf->LinkInfo.IfID.IfType,
                                     pIf->LinkInfo.PhysIfIndex))
    {
        i4Status = PPP_FAILURE;
    }
#else
    PPP_UNUSED (pIf);
#endif
    return i4Status;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : PppIPCPUpToDp                                              */
/*                                                                           */
/* Description  : This function is called to inform the DP, when IPCP        */
/*                negotiations successfully completed and the LCP has        */
/*                reached to Open state                                      */
/*                                                                           */
/* Input        : pIf            : pointer to negotiated IPCP parameters for */
/*                                    particular interface                   */
/* Output       : none                                                       */
/*                                                                           */
/* Returns      : PPP_SUCCESS / PPP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
PppIPCPUpToDp (tPPPIf * pIf)
{
    INT4                i4Status = PPP_SUCCESS;
#ifdef NPAPI_WANTED
    tIPCPIf            *pIpcpIf = NULL;

    pIpcpIf = (tIPCPIf *) pIf->pIpcpPtr;
    if (FNP_FAILURE == FsNpPppIPCPUp (pIf->LinkInfo.IfIndex,
                                      pIpcpIf->IpcpOptionsAckedByPeer.IPAddress,
                                      pIpcpIf->IpcpOptionsAckedByLocal.
                                      IPAddress))
    {
        return PPP_FAILURE;
    }
    /*Inform pppoe interface up to nat */
    if (NULL != pIf->LinkInfo.pPppoeSession)
    {
        unPppNpwFsPPPoESession FsSessionInfo;
        ePppNpwFsPPPoESession pNpType;

        pNpType = PPPoE_UPDATE_NAT;
        FsSessionInfo.FsNATEntry.IfIndex = pIf->LinkInfo.IfIndex;
        FsSessionInfo.FsNATEntry.u4AckPeerIPAddr =
            pIpcpIf->IpcpOptionsAckedByPeer.IPAddress;

        if (FNP_FAILURE == FsNpPPPoESession (pNpType, &FsSessionInfo))
        {
            i4Status = PPP_FAILURE;
        }

        FsNpPPPoEUpdateNat (pIf->LinkInfo.IfIndex,
                            pIpcpIf->IpcpOptionsAckedByPeer.IPAddress);
    }

#else
    PPP_UNUSED (pIf);
#endif
    return (i4Status);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : PppCPDownToDp                                              */
/*                                                                           */
/* Description  : This function is called to inform the DP, when LCP/IPCP    */
/*                /IPv6CP/MPLSCP/MuXCP etc goes down                         */
/*                                                                           */
/* Input        : pIf     : points to Interface structure                    */
/*              : u2Proto : Indicates the protocol typeas LCP/IPXCP/IPCP etc */
/*                                                                           */
/* Output       : none                                                       */
/*                                                                           */
/* Returns      : PPP_SUCCESS / PPP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
PppCPDownToDp (tPPPIf * pIf, UINT2 u2Proto)
{
#ifdef NPAPI_WANTED
    tPPPoESessionNode  *pSession;
#endif
    INT4                i4Status = PPP_SUCCESS;
    INT4                u4CP = -1;

    if (u2Proto == LCP_PROTOCOL)
    {
        u4CP = NP_PPP_LCP;
    }
    else if (u2Proto == IPCP_PROTOCOL)
    {
        u4CP = NP_PPP_IPCP;
    }
#ifdef NPAPI_WANTED
    if (FNP_FAILURE == FsNpPppCPDown (pIf->LinkInfo.IfIndex, u4CP))
    {
        i4Status = PPP_FAILURE;
    }

    if (NULL != pIf->LinkInfo.pPppoeSession)
    {
        /* down the PPPoE sessions */
        pSession = pIf->LinkInfo.pPppoeSession;
        if (FNP_FAILURE == FsNpPPPoEDelSession (pSession->pIf->LinkInfo.IfIndex,
                                                pSession->u2Port,
                                                pSession->u2SessionId))
        {
            i4Status = PPP_FAILURE;
        }

    }

#else
    PPP_UNUSED (pIf);
#endif

    PPP_UNUSED (u4CP);

    return i4Status;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : PppCreatePppInterfaceInDp                                  */
/*                                                                           */
/* Description  : This function is called to inform the DP, about the PPP    */
/*                interface creation in CP                                   */
/*                                                                           */
/* Input        : pIf  : points to Interface structure containing the new    */
/*                       interface parameters                                */
/*                                                                           */
/* Output       : none                                                       */
/*                                                                           */
/* Returns      : PPP_SUCCESS / PPP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
PppCreatePppInterfaceInDp (tPPPIf * pIf)
{
#ifdef NPAPI_WANTED

    if (FNP_FAILURE == FsNpCreatePppIf (pIf->LinkInfo.IfIndex,
                                        pIf->LinkInfo.PhysIfIndex,
                                        pIf->LinkInfo.IfID.IfType))
    {
        return PPP_FAILURE;

    }
#else
    PPP_UNUSED (pIf);
#endif
    return PPP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : PppCreateMpInterfaceToDP                                   */
/*                                                                           */
/* Description  : This function is called to inform the DP, about the PPP    */
/*                Multilink interface creation in CP                         */
/* Input        : pIf    : points to Interface structure containing the MP   */
/*                         bundle parameters                                 */
/*                                                                           */
/* Output       : none                                                       */
/*                                                                           */
/* Returns      : PPP_SUCCESS / PPP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
PppCreateMpInterfaceToDP (tPPPIf * pIf)
{
#ifdef NPAPI_WANTED
    if (FNP_FAILURE == FsNpCreateMPBundle (pIf->LinkInfo.IfIndex))
    {
        return PPP_FAILURE;

    }
#endif
    PPP_UNUSED (pIf);
    return PPP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : MPInsertMemberIntoBundleToDP                               */
/*                                                                           */
/* Description  : This function is called to inform the DP, that the         */
/*                PPP member link is active and operational in the           */
/*                Multilink PPP interface (bundle)                           */
/*                                                                           */
/* Input        : pMemberIf  : points to Interface structure containing      */
/*                             PPP (member link) parameters                  */
/*                pBundleIf  : points to Interface structure containing      */
/*                             MP bundle (link) parameters                   */
/*                                                                           */
/* Output       : none                                                       */
/*                                                                           */
/* Returns      : PPP_SUCCESS / PPP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
MPInsertMemberIntoBundleToDP (tPPPIf * pMemberIf, tPPPIf * pBundleIf)
{
#ifdef NPAPI_WANTED
    if (FNP_FAILURE == FsNpEnableLinkInMPBundle (pMemberIf->LinkInfo.IfIndex,
                                                 pBundleIf->LinkInfo.IfIndex))
    {
        return PPP_FAILURE;
    }
#else
    UNUSED_PARAM (pMemberIf);
    UNUSED_PARAM (pBundleIf);
#endif
    return PPP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : MPAddLinkToBundleToDP                                      */
/*                                                                           */
/* Description  : This function is called to inform the DP, about            */
/*                administrative addition of a PPP interface (member link)   */
/*                to a Multilink PPP interface (bundle)                      */
/*                                                                           */
/* Input        : pMemberIf  : points to Interface structure containing PPP  */
/*                             (member link) parameters                      */
/*                pBundleIf  : points to Interface structure containing MP   */
/*                             bundle (link) parameters                      */
/*                                                                           */
/* Output       : none                                                       */
/*                                                                           */
/* Returns      : PPP_SUCCESS / PPP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
MPAddLinkToBundleToDP (tPPPIf * pMemberIf, tPPPIf * pBundleIf)
{
#ifdef NPAPI_WANTED
    if (FNP_FAILURE == FsNpAddLinkToMPBundle (pMemberIf->LinkInfo.IfIndex,
                                              pBundleIf->LinkInfo.IfIndex))
    {
        return PPP_FAILURE;
    }
#else
    UNUSED_PARAM (pMemberIf);
    UNUSED_PARAM (pBundleIf);
#endif
    return PPP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : MPRemoveLinkFromBundleToDP                                 */
/*                                                                           */
/* Description  : This function is called to inform the DP, about            */
/*                administrative removal of a PPP interface (member link)    */
/*                from a Multilink PPP interface (bundle) and related        */
/*                parameters                                                 */
/*                                                                           */
/* Input        : pMemberIf  : points to Interface structure containing      */
/*                             PPP (member link) parameters                  */
/*                pBundleIf  : points to Interface structure containing      */
/*                             MP bundle (link) parameters                   */
/*                                                                           */
/* Output       : none                                                       */
/*                                                                           */
/* Returns      : PPP_SUCCESS / PPP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
MPRemoveLinkFromBundleToDP (tPPPIf * pMemberIf, tPPPIf * pBundleIf)
{
#ifdef NPAPI_WANTED
    if (FNP_FAILURE == FsNpRemoveLinkFromMPBundle (pMemberIf->LinkInfo.IfIndex,
                                                   pBundleIf->LinkInfo.IfIndex))
    {
        return PPP_FAILURE;
    }
#else
    UNUSED_PARAM (pMemberIf);
    UNUSED_PARAM (pBundleIf);
#endif
    return PPP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : MPDeleteMemberFromBundleToDP                               */
/*                                                                           */
/* Description  : This function is called to inform the DP, about deletion   */
/*                of a PPP interface (member link) from Multilink PPP        */
/*                interface (bundle)                                         */
/*                                                                           */
/* Input        : pMemberIf  : points to Interface structure containing PPP  */
/*                             (member link) parameters                      */
/*                pBundleIf  : points to Interface structure containing      */
/*                               MP bundle (link) parameters                 */
/*                                                                           */
/* Output       : none                                                       */
/*                                                                           */
/* Returns      : PPP_SUCCESS / PPP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
MPDeleteMemberFromBundleToDP (tPPPIf * pMemberIf, tPPPIf * pBundleIf)
{
#ifdef NPAPI_WANTED
    /* This is a member link that has become LCP down.
     * Disable multilink on this PPP link */
    if (FNP_FAILURE ==
        FsNpDisableLinkInMPBundle (pMemberIf->LinkInfo.IfIndex,
                                   pBundleIf->LinkInfo.IfIndex))
    {
        return PPP_FAILURE;
    }
#else
    PPP_UNUSED (pMemberIf);
    PPP_UNUSED (pBundleIf);
#endif
    return PPP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : PPPDeleteIfToDP                                            */
/*                                                                           */
/* Description  : This function is called to inform the DP, about the        */
/*                deletion of a PPP/MP interface in control plane            */
/*                                                                           */
/* Input        : pIf  : points to Interface structure containing PPP link   */
/*                       parameters                                          */
/*                                                                           */
/* Output       : none                                                       */
/*                                                                           */
/* Returns      : PPP_SUCCESS / PPP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
PPPDeleteIfToDP (tPPPIf * pIf)
{
    INT4                i4Status = PPP_SUCCESS;
    UINT1               u1IfType;

    u1IfType = (pIf->BundleFlag == BUNDLE) ? PPP_MP : PPP_PPP;
#ifdef NPAPI_WANTED
    if (u1IfType == PPP_MP)
    {
        if (FNP_FAILURE == FsNpDeleteMPBundle (pIf->LinkInfo.IfIndex))
        {
            return PPP_FAILURE;

        }
    }
    else
    {
        if (FNP_FAILURE == FsNpDelPppIf (pIf->LinkInfo.IfIndex,
                                         pIf->LinkInfo.PhysIfIndex,
                                         pIf->LinkInfo.IfID.IfType))
        {
            i4Status = PPP_FAILURE;
        }
    }
#else
    PPP_UNUSED (pIf);
#endif

    PPP_UNUSED (u1IfType);
    return i4Status;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IPv6CPUpToDP                                               */
/*                                                                           */
/* Description  : This function is called to inform the DP, when IPv6CP      */
/*                    negotiations successfully completed and the IPv6Cp has */
/*                    reached to Open state                                  */
/*                                                                           */
/* Input        : pIf            : pointer to negotiated IPv6CP parameters   */
/*                                 for particular interface                  */
/* Output       : none                                                       */
/*                                                                           */
/* Returns      : PPP_SUCCESS / PPP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
IPv6CPUpToDP (tPPPIf * pIf)
{
    PPP_UNUSED (pIf);
    return (PPP_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/* Function     : MPLSCPUpToDP                                               */
/*                                                                           */
/* Description  : This function is called to inform the DP, when MPLSCP      */
/*                    negotiations successfully completed and the MPLSCP has */
/*                    reached to Open state                                  */
/*                                                                           */
/* Input        : pIf            : pointer to negotiated MPLSCP parameters   */
/*                                 for particular interface                  */
/* Output       : none                                                       */
/*                                                                           */
/* Returns      : PPP_SUCCESS / PPP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
MPLSCPUpToDP (tPPPIf * pIf)
{
    PPP_UNUSED (pIf);
    return (PPP_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : MuxCPUpToDP                                                */
/*                                                                           */
/* Description  : This function is called to inform the DP, when MuxCP       */
/*                    negotiations successfully completed and the MuxCP has  */
/*                    reached to Open state                                  */
/*                                                                           */
/* Input        : pIf            : pointer to negotiated MuxCP parameters    */
/*                                 for particular interface                  */
/* Output       : none                                                       */
/*                                                                           */
/* Returns      : PPP_SUCCESS / PPP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
MuxCPUpToDP (tPPPIf * pIf)
{
    PPP_UNUSED (pIf);
    return (PPP_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : PppLinkConfigFramingToDP                                   */
/*                                                                           */
/* Description  : This function is called to inform the DP,configuration     */
/*                made on the framing processing.                            */
/* Input        : pIf            : pointer to the ppp interface              */
/* Output       : none                                                       */
/*                                                                           */
/* Returns      : PPP_SUCCESS / PPP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
PppLinkConfigFramingToDP (tPPPIf * pIf)
{
    PPP_UNUSED (pIf);
    return (PPP_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : PPPLLICallSetUpToDP                                        */
/*                                                                           */
/* Description  : This function is called to give a dialup request for       */
/*                made on the framing processing.                            */
/* Input        : pIf            : pointer to the ppp interface              */
/* Output       : none                                                       */
/*                                                                           */
/* Returns      : PPP_SUCCESS / PPP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
PPPLLICallSetUpToDP (tPPPIf * pIf)
{
    PPP_UNUSED (pIf);
    return (PPP_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : PPPLLICallTerminateToDP                                    */
/*                                                                           */
/* Description  : This routine is used to  inform the lower layer to         */
/*                terminate the call.                                        */
/* Input        : pIf            : pointer to the ppp interface              */
/* Output       : none                                                       */
/*                                                                           */
/* Returns      : PPP_SUCCESS / PPP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
PPPLLICallTerminateToDP (tPPPIf * pIf)
{
    PPP_UNUSED (pIf);
    return (PPP_SUCCESS);
}

/*****************************************************************************/
/* Function     : PPPoESessionDp                                             */
/* Description  : This function is called to inform the DP, about the        */
/*              : addition/deletion of a PPP/MP interface in Data Plane               */
/* Input        : pIf points to Interface structure containing PPP link      */
/*              : parameters                                                 */
/* Output       : none                                                       */
/* Returns      : PPP_SUCCESS / PPP_FAILURE                                  */
/*****************************************************************************/
INT4
PPPoESessionDp (ePPPoESession ePPPoEType, tPPPoESessionNode * pSession)
{

    INT4                i4Status = PPP_SUCCESS;

#ifdef NPAPI_WANTED
    unPppNpwFsPPPoESession FsSessionInfo;
    ePppNpwFsPPPoESession pNpType;

    if (ePPPoEType == PPPOE_ADD_SESSION)
    {
        pNpType = PPPoE_ADD_SESSION;
        FsSessionInfo.FsAddNewSession.u4IfIndex =
            pSession->pIf->LinkInfo.IfIndex;
        memcpy (FsSessionInfo.FsAddNewSession.DestMACAddr,
                pSession->DestMACAddr, sizeof (pSession->DestMACAddr));
        memcpy (FsSessionInfo.FsAddNewSession.SrcMACAddr, pSession->SrcMACAddr,
                sizeof (pSession->SrcMACAddr));
        FsSessionInfo.FsAddNewSession.u2Port = pSession->u2Port;
        FsSessionInfo.FsAddNewSession.u2SessionId = pSession->u2SessionId;
        FsSessionInfo.FsAddNewSession.ACCookieId = pSession->ACCookieId;
        if (FNP_FAILURE == FsNpPPPoESession (pNpType, &FsSessionInfo))
        {
            i4Status = PPP_FAILURE;
        }
    }
    else if (ePPPoEType == PPPOE_DELETE_SESSION)
    {
        pNpType = PPPoE_DELETE_SESSION;
        FsSessionInfo.FsDeleteSession.u4IfIndex =
            pSession->pIf->LinkInfo.IfIndex;
        FsSessionInfo.FsDeleteSession.u2Port = pSession->u2Port;
        FsSessionInfo.FsDeleteSession.u2SessionId = pSession->u2SessionId;

        if (FNP_FAILURE == FsNpPPPoESession (pNpType, &FsSessionInfo))
        {
            i4Status = PPP_FAILURE;
        }
    }
#endif
    PPP_UNUSED (ePPPoEType);
    PPP_UNUSED (pSession);
    return i4Status;
}

/*****************************************************************************/
/* Function     : PPPoEAddNewSessionToDp                                     */
/* Description  : This function is called to inform the DP, about the        */
/*              : addition of a PPP/MP interface in Data Plane               */
/* Input        : pIf points to Interface structure containing PPP link      */
/*              : parameters                                                 */
/* Output       : none                                                       */
/* Returns      : PPP_SUCCESS / PPP_FAILURE                                  */
/*****************************************************************************/
INT4
PPPoEAddNewSessionToDp (tPPPoESessionNode * pSession)
{

    INT4                i4Status = PPP_SUCCESS;
#ifdef NPAPI_WANTED
    if (FNP_FAILURE == FsNpPPPoEAddNewSession (pSession->pIf->LinkInfo.IfIndex,
                                               pSession->DestMACAddr,
                                               pSession->SrcMACAddr,
                                               pSession->u2Port,
                                               pSession->u2SessionId,
                                               pSession->ACCookieId))
    {
        i4Status = PPP_FAILURE;
    }
#endif
#ifdef SMOD_WANTED
    if (OSIX_FAILURE ==
        SecApiPPPoEAddNewSession (pSession->pIf->LinkInfo.IfIndex,
                                  pSession->pIf->LinkInfo.PhysIfIndex,
                                  pSession->u2SessionId))
    {
        i4Status = PPP_FAILURE;
    }
#endif
    PPP_UNUSED (pSession);
    return i4Status;
}

/*****************************************************************************/
/* Function     : PPPoEDeleteSessionFromDp                                   */
/* Description  : This function is called to inform the DP, about the        */
/*              : deletion of a PPP/MP interface in Data Plane               */
/* Input        : pIf points to Interface structure containing PPP link      */
/*              : parameters                                                 */
/* Output       : none                                                       */
/* Returns      : PPP_SUCCESS / PPP_FAILURE                                  */
/*****************************************************************************/
INT4
PPPoEDeleteSessionFromDp (tPPPoESessionNode * pSession)
{

    INT4                i4Status = PPP_SUCCESS;
#ifdef NPAPI_WANTED
    if (FNP_FAILURE == FsNpPPPoEDelSession (pSession->pIf->LinkInfo.IfIndex,
                                            pSession->u2Port,
                                            pSession->u2SessionId))
    {
        i4Status = PPP_FAILURE;
    }
#endif
#ifdef SMOD_WANTED
    if (OSIX_FAILURE == SecApiPPPoEDelSession (pSession->pIf->LinkInfo.IfIndex,
                                               pSession->pIf->LinkInfo.
                                               PhysIfIndex,
                                               pSession->u2SessionId))
    {
        i4Status = PPP_FAILURE;
    }
#endif
    PPP_UNUSED (pSession);
    return i4Status;
}
