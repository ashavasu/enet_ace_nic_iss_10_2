##########################################################################
# Copyright (C) 2010 Aricent Inc . All Rights Reserved                   #
# ------------------------------------------                             #
# $Id: make.h,v 1.2 2014/11/23 09:33:37 siva Exp $
# DESCRIPTION  : The make.h for PPP Module                               #
##########################################################################

TOTAL_OPNS =  $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

GLOBAL_FLAGS =      -DUNIX  \
            -DMP_ALGORITHM_INTERLEAVING \
            -UBAP \
            -UBACP  \
            -ULM_PASSWD_HASH   \
            -UDEBUG_WANTED \
            -UHARDWARE_ENABLED\
     -DFS_NPAPI\
            -UIPv6CP\
            -UCCP\
            -UBCP\
     -UDPIF\
            -ULQM\
     -DFRAMING\
     -DMP
#-UL2TP_VOLUNTARY 

ifeq (${TARGET_PKG}, STACK)
GLOBAL_FLAGS += -DPPP_STACK_WANTED
endif 
     
GLOBAL_FLAGS += $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)
CC+=$(CC_FLAGS) 

# UNIX 
#       This is to enable the portion of the code which are dependent on
# the UNIX OS, when it is compiled in UNIX Operating System.


# BAP
#       This should be enabled to include BAP in our PPP stack.

# BACP
#       This should be enabled to include BACP in our PPP stack.

                        
PPPWORK      = ${BASE_DIR}/ppp
#L2TPWORK     = ${BASE_DIR}/l2tp
 
# To integrate FuturePPP with FutureSNMP the SNMPCOMINC and SNMOAGENTINC 
# should be set to the Future SNMP Agent common and agent directory 
# path respectively.


EXE            =  ${PPPWORK}/exe/
OBJ            =  ${PPPWORK}/obj/

CFAINC         =    $(CFAWORK)/inc/
#L2TPINC        =    $(L2TPWORK)/inc/
PPPINC         =    $(PPPWORK)/inc/
GENINC         =    $(PPPWORK)/geninc/
FRAMINC        =    $(PPPWORK)/framing/inc/
GSEM-GCPINC    =    $(PPPWORK)/gsem-gcp/inc/
LCPINC         =    $(PPPWORK)/lcp/inc/
AUTHINC        =    $(PPPWORK)/lcp/auth/inc/
LQMINC         =    $(PPPWORK)/lcp/lqm/inc/
MPINC          =    $(PPPWORK)/mp/inc/
IPCPINC        =    $(PPPWORK)/ipcp/inc/
IPv6CPINC      =    $(PPPWORK)/ipv6cp/inc/
IPXCPINC       =    $(PPPWORK)/ipxcp/inc/
MUXCPINC       =    $(PPPWORK)/muxcp/inc/
MPLSCPINC      =    $(PPPWORK)/mplscp/inc/
BCPINC         =    $(PPPWORK)/bcp/inc/
BACPINC        =    $(PPPWORK)/bacp/inc/
BAPINC         =    $(PPPWORK)/bap/inc/
IPXCPINC       =    $(PPPWORK)/ipxcp/inc/
CCPINC         =    $(PPPWORK)/ccp/inc/
CVINC          =    $(PPPWORK)/convertor/inc/
GZIPINC        =    $(PPPWORK)/ccp/gzip/inc/
MPPEINC        =    $(PPPWORK)/ccp/mppe/inc/
ECPINC         =    $(PPPWORK)/ecp/inc/
ECCMNINC       =    $(PPPWORK)/ecp-ccp/inc/
ECCINC         =    $(PPPWORK)/ecp-ccp/inc/
DESEINC        =    $(PPPWORK)/ecp/des/inc/
PPPOEINC       =    $(PPPWORK)/pppoe/inc/
PPPDPCPINC     =    $(PPPWORK)/dpif/


RADINC      =${BASE_DIR}/radius/inc/    


NCPINCS  =  -I$(IPCPINC) -I$(IPv6CPINC) -I$(MUXCPINC) -I$(MPLSCPINC) 

LOWINCFILES    =  $(PPPINC)pppsnmpm.h \
                  $(PPPINC)snmpexts.h

FINALPPPOBJ    =  $(PPPWORK)/obj/FuturePPP.o
                  
PPPOBJ         =    $(PPPWORK)/obj/
GENOBJ         =    $(PPPWORK)/genobj/
FRAMOBJ        =    $(PPPWORK)/framing/obj/
GSEM-GCPOBJ    =    $(PPPWORK)/gsem-gcp/obj/
LCPOBJ         =    $(PPPWORK)/lcp/obj/
AUTHOBJ        =    $(PPPWORK)/lcp/auth/obj/
LQMOBJ         =    $(PPPWORK)/lcp/lqm/obj/
MPOBJ          =    $(PPPWORK)/mp/obj/
IPCPOBJ        =    $(PPPWORK)/ipcp/obj/
IPv6CPOBJ      =    $(PPPWORK)/ipv6cp/obj/
MUXCPOBJ       =    $(PPPWORK)/muxcp/obj/
MPLSCPOBJ      =    $(PPPWORK)/mplscp/obj/
IPXCPOBJ       =    $(PPPWORK)/ipxcp/obj/
BCPOBJ         =    $(PPPWORK)/bcp/obj/
BACPOBJ        =    $(PPPWORK)/bacp/obj/
BAPOBJ         =    $(PPPWORK)/bap/obj/
IPXCPOBJ       =    $(PPPWORK)/ipxcp/obj/
CCPOBJ         =    $(PPPWORK)/ccp/obj/
CVOBJ          =    $(PPPWORK)/convertor/obj/
GZIPOBJ        =    $(PPPWORK)/ccp/gzip/obj/
MPPEOBJ        =    $(PPPWORK)/ccp/mppe/obj/
ECPOBJ         =    $(PPPWORK)/ecp/obj/
ECCMNOBJ       =    $(PPPWORK)/ecp-ccp/obj/
ECCOBJ         =    $(PPPWORK)/ecp-ccp/obj/
MPPLUSOBJ      =    $(PPPWORK)/mpp/obj/
DESEOBJ        =    $(PPPWORK)/ecp/des/obj/
PPPOEOBJ       =    $(PPPWORK)/pppoe/obj/
PPPDPIFOBJ     =    $(PPPWORK)/dpif/obj/
MDLOBJ         =    $(PPPWORK)/mdl/obj/

###############################################
PROJECT_DEPENDENCIES = $(COMMON_DEPENDENCIES) \
            $(PPPWORK)/Makefile \
            $(PPPWORK)/make.h
###############################################
COMMON_INCLUDE_DIRS += -I$(PPPWORK)/mdl/inc \
                       -I$(PPPWORK)/mdl \
                       -I$(PPPOEINC)
