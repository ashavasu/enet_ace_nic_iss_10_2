/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppoeport.c,v 1.6 2014/10/21 11:17:25 siva Exp $
 *
 * Description:This file contains pppoe functions which calls external 
 *             module routines.
 *
 *******************************************************************/
#include "pppoecom.h"

/************************************************************************/
/*  Function Name   :PPPoESendDiscoveryPkt                              */
/*  Description     :This Function is invoked by the client to send     */
/*                  :the Discovery Packet                               */
/*  Input(s)        :u2Port:The Interface over which the packet is      */
/*                  :Recieved                                           */
/*                  :pDestMac:Points to the Destination Mac Address     */
/*                  :Code:Represents the type of the Discovery Packet   */
/*                  :u2SessionId:Represents the Id of hter session      */
/*                  :pTags:Points to the Tag                            */
/*                  :TagLen:Represents the Length of the Tag Fields     */
/*  Output(s)       : None                                              */
/*  Returns         :                                                   */
/************************************************************************/
VOID
PPPoESendDiscoveryPkt (UINT2 u2PPPIfIndex, UINT1 *pDestMac, UINT1 Code,
                       UINT2 u2SessionId, UINT1 *pTags, UINT2 TagLen)
{
    tCfaIfInfo          IfInfo;
    t_MSG_DESC         *pBuf;
    tPPPoEBinding      *pPPPoEIf;
    tPPPIf             *pPPPIf;
    UINT1               Offset = 0;

    pPPPIf = PppGetIfPtr (u2PPPIfIndex);
    if (pPPPIf == NULL)
    {
        return;
    }
    if ((pPPPoEIf = PPPoEBindingGetNode (pPPPIf->LinkInfo.PhysIfIndex)) == NULL)
    {
        PPP_TRC (ALL_FAILURE, "Binding Node NOT enabled over this interface\n");
        return;
    }

    if (pPPPIf->u2VlanTci == 0)
    {
        pBuf = ALLOCATE_PPPOE_BUFFER ((UINT4) (TagLen + PPPOE_FULL_HDR_LEN));
    }
    else
    {
        /* If vlan encapsulation is needed, allocate extra 4 bytes 
           for the TPID header */
        pBuf = ALLOCATE_PPPOE_BUFFER ((UINT4) ((TagLen + PPPOE_FULL_HDR_LEN) +
                                               PPPOE_TPID_VLAN_BYTE_LENGTH));
    }

    if (pBuf == NULL)
    {
        return;
    }

    /* dest mac addr */
    ASSIGNSTR (pBuf, pDestMac, Offset, HOST_ADDR_LEN);
    /* ASSIGN_N_BYTE (pBuf, pDestMac, Offset, HOST_ADDR_LEN); */
    Offset = (UINT1) (Offset + HOST_ADDR_LEN);
    PPP_TRC (CONTROL_PLANE, "----------------------------");
    PPP_TRC (CONTROL_PLANE, "Sending PPPoE Discovery Pkt:\n");
    PPP_TRC6 (CONTROL_PLANE, "Dest MAC Addr  - %02x:%02x:%02x:%02x:%02x:%02x",
              pDestMac[0], pDestMac[1], pDestMac[2],
              pDestMac[3], pDestMac[4], pDestMac[5]);
    /* src mac addr */

    if (CfaGetIfInfo (pPPPIf->LinkInfo.PhysIfIndex, &IfInfo) == CFA_FAILURE)
    {
        PPP_TRC (ALL_FAILURE, "Invalid port");
        return;
    }

    pDestMac = IfInfo.au1MacAddr;

    ASSIGNSTR (pBuf, pDestMac, Offset, HOST_ADDR_LEN);
    /* ASSIGN_N_BYTE (pBuf, pDestMac, Offset, HOST_ADDR_LEN); */
    Offset = (UINT1) (Offset + HOST_ADDR_LEN);
    PPP_TRC6 (CONTROL_PLANE, "Src  MAC Addr  - %02x:%02x:%02x:%02x:%02x:%02x",
              pDestMac[0], pDestMac[1], pDestMac[2],
              pDestMac[3], pDestMac[4], pDestMac[5]);

    /* Check for vlan id, if not zero add TPID header */
    if (pPPPIf->u2VlanTci != 0)
    {
        ASSIGN2BYTE (pBuf, Offset, VLAN_PROTOCOL_ID);
        Offset = (UINT1) (Offset + 2);
        ASSIGN2BYTE (pBuf, Offset, pPPPIf->u2VlanTci);
        Offset = (UINT1) (Offset + 2);
        PPP_TRC1 (CONTROL_PLANE, "Vlan TCI  - 0x%x", pPPPIf->u2VlanTci);
    }
    /* discovery type */
    ASSIGN2BYTE (pBuf, Offset, PPPOE_DISCOVERY_TYPE);
    Offset = (UINT1) (Offset + 2);
    PPP_TRC1 (CONTROL_PLANE, "Ethernet Type  - 0x%x", PPPOE_DISCOVERY_TYPE);
    /* v&type */
    ASSIGN1BYTE (pBuf, Offset, PPPOE_VER_AND_TYPE);
    Offset++;
    PPP_TRC1 (CONTROL_PLANE, "Ver&Type       - 0x%x", PPPOE_VER_AND_TYPE);
    /* code */
    ASSIGN1BYTE (pBuf, Offset, Code);
    Offset++;
    switch (Code)
    {
        case PADI:
            PPP_TRC1 (CONTROL_PLANE, "Code           - 0x%x [ PADI ]", Code);
            pPPPoEIf->NoOfPADITimeOuts++;
	    break;
        case PADO:
            PPP_TRC1 (CONTROL_PLANE, "Code           - 0x%x [ PADO ]", Code);
            pPPPoEIf->NoOfPADOTrans++;
            break;
        case PADR:
            PPP_TRC1 (CONTROL_PLANE, "Code           - 0x%x [ PADR ]", Code);
            break;
        case PADS:
            PPP_TRC1 (CONTROL_PLANE, "Code           - 0x%x [ PADS ]", Code);
            pPPPoEIf->NoOfPADSTrans++;
            break;
        case PADT:
            PPP_TRC1 (CONTROL_PLANE, "Code           - 0x%x [ PADT ]", Code);
            pPPPoEIf->NoOfPADTTrans++;
            break;
        default:
            break;
    }
    /* session id */
    ASSIGN2BYTE (pBuf, Offset, u2SessionId);
    Offset = (UINT1) (Offset + 2);
    PPP_TRC1 (CONTROL_PLANE, "Session Id     - %x", u2SessionId);
    /* length */
    ASSIGN2BYTE (pBuf, Offset, TagLen);
    Offset = (UINT1) (Offset + 2);
    PPP_TRC1 (CONTROL_PLANE, "Length         - %d", TagLen);
    PPP_TRC (CONTROL_PLANE, "----------------------------");
    /* append the tags */
    if (TagLen)
        ASSIGNSTR (pBuf, pTags, Offset, TagLen);
    /* ASSIGN_N_BYTE (pBuf, pTags, Offset, TagLen); */

    /*send the pkt */
    if (pPPPIf->u2VlanTci == 0)
    {
        PPPoETxPktToLL (pBuf, (UINT2) pPPPIf->LinkInfo.PhysIfIndex,
                        (UINT2) (PPPOE_FULL_HDR_LEN + TagLen));
    }
    else
    {
        PPPoETxPktToLL (pBuf, (UINT2) pPPPIf->LinkInfo.PhysIfIndex,
                        (UINT2) (PPPOE_FULL_HDR_LEN + TagLen +
                                 PPPOE_TPID_VLAN_BYTE_LENGTH));
    }
    return;
}

/************************************************************************/
/*  Function Name   :PPPoEExtractHeader                                 */
/*  Description     : This Function Extracts the Ethernet Header from   */
/*                  :the Recieved Packet                                */
/*  Input(s)        :pBuf:Pointer to the Incoming Buffer                */
/*                  :pHdr:Pointer to the Ethernet Structure             */
/*  Output(s)       : None                                              */
/*  Returns         :                                                   */
/************************************************************************/
VOID
PPPoEExtractHeader (t_MSG_DESC * pBuf, tPPPoEHeader * pHdr)
{
    UINT1              *pLinearPkt;
    UINT2               u2Val = 0;
    UINT2               u2Offset = 0;

    pLinearPkt = (UINT1 *) CRU_BMC_Get_DataPointer (pBuf);
    /* Extracting Destination MAC Address */
    COPY_MAC_ADDR (pHdr->DestMACAddr, pLinearPkt);
    /* Extracting Source     MAC Address */
    COPY_MAC_ADDR (pHdr->SrcMACAddr, pLinearPkt + HOST_ADDR_LEN);

    /* Check whether it is VLAN encapsulated packet */
    u2Val = *(UINT2 *) (VOID *) (pLinearPkt + PPPOE_ETHER_TYPE_INDEX);
    u2Val = OSIX_NTOHS (u2Val);
    if (u2Val == VLAN_PROTOCOL_ID)
    {
        /* Extracting vlan id if it is a TPID header */
        u2Val = *(UINT2 *) (VOID *) (pLinearPkt + (PPPOE_ETHER_TYPE_INDEX + 2));
        pHdr->u2VlanId = OSIX_NTOHS (u2Val);
        pHdr->u2VlanId = (pHdr->u2VlanId & PPPOE_TPID_VLANID_MASK);
        /* The offset for other fields will now be extra 4 bytes */
        u2Offset = PPPOE_TPID_VLAN_BYTE_LENGTH;
    }

    /* Extracting Ethernet Type */
    u2Val =
        *(UINT2 *) (VOID *) (pLinearPkt + PPPOE_ETHER_TYPE_INDEX + u2Offset);
    pHdr->u2EthType = OSIX_NTOHS (u2Val);
    /* Extracting Ver&Type */
    pHdr->u1VerAndType = *(pLinearPkt + PPPOE_VTYPE_INDEX + u2Offset);
    /* Extracting Code */
    pHdr->u1Code = *(pLinearPkt + PPPOE_CODE_INDEX + u2Offset);
    /* Extracting Session Id */
    u2Val = *(UINT2 *) (VOID *) (pLinearPkt + PPPOE_SESSIONID_INDEX + u2Offset);
    pHdr->u2SessionId = OSIX_NTOHS (u2Val);
    /* Extracting Length of the Payload */
    u2Val = *(UINT2 *) (VOID *) (pLinearPkt + PPPOE_LENGTH_INDEX + u2Offset);
    pHdr->u2Length = OSIX_NTOHS (u2Val);
}

/************************************************************************/
/*  Function Name   :PPPSendDataToPPPoE                                 */
/*  Description     :This Function is invoked When PPP wants To send    */
/*                  :Data To PPPoE Module                               */
/*  Input(s)        :pNode:Pointer to the Session Node which contains   */
/*                  :Session over which PPP Packet can be Transmitted   */
/*                  :pOutPDU:Pointer to the Outgoining Packet           */
/*                  :Length:Length of the Packet                        */
/*  Output(s)       : None                                              */
/*  Returns         :                                                   */
/************************************************************************/
VOID
PPPSendDataToPPPoE (VOID *pNode, t_MSG_DESC * pOutPDU, UINT2 Length)
{
    tCfaIfInfo          IfInfo;
    UINT1               aHdrWithVlan[PPPOE_FULL_HDR_LEN +
                                     PPPOE_TPID_VLAN_BYTE_LENGTH];
    UINT1               aHdr[PPPOE_FULL_HDR_LEN];
    tPPPoESessionNode  *pSession;
    UINT2               u2Offset = 0;

    UINT1              *pLinearPkt;

    pSession = (tPPPoESessionNode *) pNode;

    COPY_MAC_ADDR (aHdr, pSession->DestMACAddr);
    COPY_MAC_ADDR (aHdrWithVlan, pSession->DestMACAddr);

    if (CfaGetIfInfo (pSession->u2Port, &IfInfo) == CFA_FAILURE)
    {
        PPP_TRC (ALL_FAILURE, "Invalid port");
        return;
    }

    COPY_MAC_ADDR (aHdr + PPPOE_SRC_ADDR_INDEX, IfInfo.au1MacAddr);
    COPY_MAC_ADDR (aHdrWithVlan + PPPOE_SRC_ADDR_INDEX, IfInfo.au1MacAddr);

    if (pSession->pIf->u2VlanTci != 0)
    {
        /* Insert TPID and vlan id */
        *(UINT2 *) (VOID *) (aHdrWithVlan + PPPOE_ETHER_TYPE_INDEX) =
            OSIX_HTONS (VLAN_PROTOCOL_ID);

        *(UINT2 *) (VOID *) (aHdrWithVlan + PPPOE_ETHER_TYPE_INDEX + 2) =
            OSIX_HTONS (pSession->pIf->u2VlanTci);
        u2Offset = PPPOE_TPID_VLAN_BYTE_LENGTH;

        *(UINT2 *) (VOID *) (aHdrWithVlan + PPPOE_ETHER_TYPE_INDEX + u2Offset) =
            OSIX_HTONS (PPPOE_SESSION_TYPE);
        aHdrWithVlan[PPPOE_VTYPE_INDEX + u2Offset] = PPPOE_VER_AND_TYPE;
        aHdrWithVlan[PPPOE_CODE_INDEX + u2Offset] = PPPOE_SESSION_CODE;
        *(UINT2 *) (VOID *) (aHdrWithVlan + PPPOE_SESSIONID_INDEX + u2Offset) =
            OSIX_HTONS (pSession->u2SessionId);
        *(UINT2 *) (VOID *) (aHdrWithVlan + PPPOE_LENGTH_INDEX + u2Offset) =
            OSIX_HTONS (Length);
        pLinearPkt = (UINT1 *) CRU_BMC_Get_DataPointer (pOutPDU);

        PPPOE_PREPEND_HEADER (pOutPDU, aHdrWithVlan, PPPOE_FULL_HDR_LEN +
                              PPPOE_TPID_VLAN_BYTE_LENGTH);
    }
    else
    {
        *(UINT2 *) (VOID *) (aHdr + PPPOE_ETHER_TYPE_INDEX + u2Offset) =
            OSIX_HTONS (PPPOE_SESSION_TYPE);
        aHdr[PPPOE_VTYPE_INDEX + u2Offset] = PPPOE_VER_AND_TYPE;
        aHdr[PPPOE_CODE_INDEX + u2Offset] = PPPOE_SESSION_CODE;
        *(UINT2 *) (VOID *) (aHdr + PPPOE_SESSIONID_INDEX + u2Offset) =
            OSIX_HTONS (pSession->u2SessionId);
        *(UINT2 *) (VOID *) (aHdr + PPPOE_LENGTH_INDEX + u2Offset) =
            OSIX_HTONS (Length);
        pLinearPkt = (UINT1 *) CRU_BMC_Get_DataPointer (pOutPDU);

        PPPOE_PREPEND_HEADER (pOutPDU, aHdr, PPPOE_FULL_HDR_LEN);
    }

    pLinearPkt = (UINT1 *) CRU_BMC_Get_DataPointer (pOutPDU);
    if (pLinearPkt == NULL)
    {
        PPP_TRC (ALL_FAILURE, "Packet is NULL");
    }
    PPPoETxPktToLL (pOutPDU, pSession->u2Port,
                    (UINT2) (Length + PPPOE_FULL_HDR_LEN + u2Offset));

}

/************************************************************************/
/*  Function Name   :PPPoETlsEventFromPPP                               */
/*  Description     :This Function Gives the TLS event from PPP to      */
/*                  :PPPoE which starts the Discovery Stage             */
/*  Input(s)        :pIf:Pointer to the Interface Structure             */
/*  Output(s)       : None                                              */
/*  Returns         :                                                   */
/************************************************************************/
UINT4
PPPoETlsEventFromPPP (tPPPIf * pIf)
{
    tClientDiscoveryNode *pDiscNode;
    tPPPoEBinding      *pPPPoEIf;
    if (PPPoEMode == PPPOE_SERVER)
        return OSIX_SUCCESS;

    if ((pDiscNode =
         MEM_CALLOC (1, sizeof (tClientDiscoveryNode),
                     tClientDiscoveryNode)) == NULL)
    {
        PPP_TRC (ALL_FAILURE, "MemAlloc for pDiscNode failed \n");
        return OSIX_FAILURE;
    }
    pDiscNode->u2Port = (UINT2) (pIf->LinkInfo.PhysIfIndex);
    pDiscNode->StoredBuf = NULL;
    SLL_INIT (&pDiscNode->pPADOList);
    pDiscNode->pIf = pIf;
    PPP_INIT_TIMER (pDiscNode->PADTimer);
    pDiscNode->ReTxCount = 0;
    pDiscNode->State = PADI_SENT;
    pIf->LinkInfo.pPppoeSession = pDiscNode;
    pPPPoEIf = PPPoEBindingGetNode (pDiscNode->u2Port);
    PPPoECSendPADI (pDiscNode->u2Port, pDiscNode, PPPOE_NORMAL);
    if(pPPPoEIf != NULL)
    {
    	pPPPoEIf->NoOfPADITrans++;
    }
    SLL_ADD (&PPPoEClientDiscList, (t_SLL_NODE *) pDiscNode);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   :PPPoETlfEventFromPPP                               */
/*  Description     :This Function gives PPP-TLF even to PPPoE whcih    */
/*                  :causes the Termination of PPPoE Session            */
/*  Input(s)        :pIf:Pointer to the Interface Structure             */
/*  Output(s)       : None                                              */
/*  Returns         :                                                   */
/************************************************************************/
VOID
PPPoETlfEventFromPPP (tPPPIf * pIf)
{
    tPPPoESessionNode  *pSession;

    if (PPPoEMode == PPPOE_CLIENT)
    {
        tClientDiscoveryNode *pDiscNode = NULL;
        SLL_SCAN (&PPPoEClientDiscList, pDiscNode, tClientDiscoveryNode *)
        {
            if (pDiscNode->pIf == pIf)
                break;
        }
        if (pDiscNode)
        {
            PPPSendEventToPPPTask (pDiscNode->pIf->LinkInfo.IfIndex, PPP_DOWN);
            pDiscNode->pIf->LinkInfo.pPppoeSession = NULL;
            PPPOE_REMOVE_DISCOVERY_NODE (pDiscNode);
            return;
        }
    }                            /* for client in the discovery stage */

    pSession = pIf->LinkInfo.pPppoeSession;

    PPPoESendDiscoveryPkt ((UINT2) pSession->pIf->LinkInfo.IfIndex,
                           pSession->DestMACAddr, PADT, pSession->u2SessionId,
                           NULL, 0);
    PPPoEDeleteSession (pSession);

}

/************************************************************************/
/*  Function Name   :PPPoETxPktToLL                                     */
/*  Description     :This Function is Invoked When PPPoE wants to       */
/*                  :Transmit Packet to the Lower Layer                 */
/*  Input(s)        :pBuf:Points to the Packet                          */
/*                  :u2Port:The Interface over which the Packet is      */
/*                  :Recieved                                           */
/*                  :BufLen:Length of the Packet                        */
/*  Output(s)       : None                                              */
/*  Returns         :                                                   */
/************************************************************************/
VOID
PPPoETxPktToLL (t_MSG_DESC * pBuf, UINT2 u2Port, UINT2 BufLen)
{
    /*
     * This is a *porting* function. Driver related function should 
     * be called to Tx the packet to ethernet interface. "pBuf "
     * contains the complete PPPoE packet including Ethernet V2 
     * header.
     */

    /* FutureCFA expects destination MAC address and Ethernet Protocol 
     * as parameters. Buffer length is ignored.
     */

    tPPPoEBinding      *pPPPoEIf = NULL;
    UINT1               au1DestAddr[HOST_ADDR_LEN];
    UINT2               u2Protocol;

    PPP_UNUSED (BufLen);

    if ((pPPPoEIf = PPPoEBindingGetNode (u2Port)) == NULL)
    {
        PPP_UNUSED (pPPPoEIf);
        return;
    }
    /* Extract the destination MAC address */
    CRU_BUF_Copy_FromBufChain (pBuf, au1DestAddr, 0, HOST_ADDR_LEN);

    /* Extract the Ethernet V2 protocol field */
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2Protocol,
                               ((2 * HOST_ADDR_LEN)), 2);
    u2Protocol = OSIX_NTOHS (u2Protocol);
    if (u2Protocol == CFA_VLAN_PROTOCOL_ID)
    {
        /* Extract the Ethernet V2 protocol field */
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2Protocol,
                                   ((2 * HOST_ADDR_LEN)) + 4, 2);
        u2Protocol = OSIX_NTOHS (u2Protocol);
    }

#ifdef CFA_WANTED
    if (CfaTxPPPoEPkt (pBuf, u2Port, au1DestAddr, u2Protocol) == FAILURE)
    {
        RELEASE_BUFFER (pBuf);
    }
#else
    UNUSED_PARAM (u2Port);
#endif

}

/************************************************************************/
/*  Function Name   :GetFreePPPIfIndex                                  */
/*  Description     :This Function is invoked to get free PPP Interface */
/*  Input(s)        :pIfIndex:Pointer to the Interface Index            */
/*  Output(s)       : None                                              */
/*  Returns         :Returns the Free PPP Interface Index               */
/************************************************************************/
INT4
GetFreePPPIfIndex (UINT4 *pIfIndex, tPPPIf ** pRetIf, UINT2 u2Port,
                   UINT1 IfType)
{
    tPPPIf             *pIf;
    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if ((pIf->LinkInfo.Mask & PPP_FLOATING_MASK /*1 */ )
            && (pIf->LinkInfo.Mask & PPP_STATUS_FREE_MASK /*1 */ ))
        {
            if ((pIf->LinkInfo.PhysIfIndex != u2Port))
            {
                continue;
            }

            tPPPIfId            IfId;
            MEMSET (&IfId, 0, sizeof (tPPPIfId));
            IfId.IfType = IfType;
            IfId.porttype = (UINT1) u2Port;
            MEMCPY (&pIf->LinkInfo.IfID, &IfId, sizeof (tPPPIfId));

            pIf->LinkInfo.PhysIfIndex = u2Port;
            *pIfIndex = pIf->LinkInfo.IfIndex;
            pIf->LinkInfo.Mask &= PPP_STATUS_NOT_FREE_MASK;    /*0 */
            LCPReInit (pIf);
            LCPEnableIf (pIf);
            *pRetIf = pIf;
            return SUCCESS;

        }
    }
    *pRetIf = NULL;
    return FAILURE;
}

/************************************************************************/
/*  Function Name   :ReleasePPPInterface                                */
/*  Description     :This Function is used to Release the PPP Interface */
/*  Input(s)        :pIf:Pointer to the Interface Structure             */
/*  Output(s)       : None                                              */
/*  Returns         :                                                   */
/************************************************************************/
VOID
ReleasePPPInterface (tPPPIf * pIf)
{
    PPP_TRC1 (CONTROL_PLANE, "Releasing the PPP interface :[ %d ]",
              pIf->LinkInfo.IfIndex);
    pIf->LinkInfo.Mask |= PPP_STATUS_FREE_MASK;
    pIf->LinkInfo.IfID.IfType = NONE_IF_TYPE;
    pIf->LinkInfo.pPppoeSession = NULL;
    PPPSendEventToPPPTask (pIf->LinkInfo.IfIndex, PPP_DOWN);
}

/************************************************************************/
/*  Function Name   :PPPHandlePPPoEPkt                                  */
/*  Description     :This Function is invoked when there is PPP Packet  */
/*                  :To PPPoE                                           */
/*  Input(s)        :pBuf:Points To the incoming Buffer                 */
/*                  :Length:Length of the Packet                        */
/*                  :The Interface over which the Packet is Recieved    */
/*  Output(s)       : None                                              */
/*  Returns         :SUCCESS or FAILURE                                 */
/************************************************************************/
INT4
PPPHandlePPPoEPkt (t_MSG_DESC * pBuf, UINT2 Length, UINT2 u2Port)
{
    tPPPoEBinding      *pBinding;

    PPP_UNUSED (Length);

    if ((pBinding = PPPoEBindingGetNode (u2Port)) == NULL)
    {
        PPP_UNUSED (pBinding);
        RELEASE_BUFFER (pBuf);
        PPP_TRC (CONTROL_PLANE, "PPPoE not enabled on this interface");
        return SUCCESS;
    }
    PPP_GET_PORT_NUMBER (pBuf) = u2Port;
    PPP_GET_SRC_MODID (pBuf) = PPPMSG_FROM_PPPoE;

    if (OsixSendToQ (SELF, PPP_QUEUE_NAME, pBuf, OSIX_MSG_NORMAL)
        != OSIX_SUCCESS)
    {
        PPP_TRC (OS_RESOURCE, "Send to OE Queue Failure.");
        return FAILURE;
    }

    if (OsixSendEvent (SELF, PPP_TASK_NAME, PPP_QUEUE_EVENT) != OSIX_SUCCESS)
    {
        return FAILURE;
    }
    return SUCCESS;

}

/************************************************************************/
/*  Function Name   : PPPDeletePPPoESession                             */
/*  Description     : This function deletes the pppoe session,          */
/*                  : when ppp interfcae is deleted                     */
/*  Input(s)        :pIf:Pointer to the Interface Structure             */
/*  Output(s)       : None                                              */
/*  Returns         :                                                   */
/************************************************************************/
VOID
PPPDeletePPPoESession (tPPPIf * pIf)
{
    tPPPoESessionNode  *pSession;
    tPPPoEBinding      *pBinding;

    if (PPPoEMode == PPPOE_CLIENT)
    {
        tClientDiscoveryNode *pDiscNode = NULL;
        SLL_SCAN (&PPPoEClientDiscList, pDiscNode, tClientDiscoveryNode *)
        {
            if (pDiscNode->pIf == pIf)
                break;
        }
        if (pDiscNode)
        {
            PPPOE_REMOVE_DISCOVERY_NODE (pDiscNode);
            return;
        }
    }
    /* for client in the discovery stage */

    pSession = pIf->LinkInfo.pPppoeSession;

    if (pSession == NULL)
        return;

    PPPoESendDiscoveryPkt ((UINT2) pSession->pIf->LinkInfo.IfIndex,
                           pSession->DestMACAddr, PADT, pSession->u2SessionId,
                           NULL, 0);

    if ((pBinding = PPPoEBindingGetNode (pSession->u2Port)) != NULL)
    {
        PPP_TRC1 (CONTROL_PLANE, "Deleting Session on eth%d", pSession->u2Port);
        PPP_TRC1 (CONTROL_PLANE, "Session Id :[ %d ]", pSession->u2SessionId);
        pBinding->NoOfActiveSessions--;
        gu4PPPoEActiveSessions--;
    }

    pSession->pIf->LinkInfo.pPppoeSession = NULL;
    SLL_DELETE (&PPPoESessionList, (t_SLL_NODE *) pSession);
    MEM_FREE (pSession);
}
