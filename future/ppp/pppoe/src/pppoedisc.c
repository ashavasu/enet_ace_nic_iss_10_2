/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppoedisc.c,v 1.18 2014/12/16 11:47:25 siva Exp $
 *
 * Description:This file contains functions for pppoe discovery stage.
 *
 *******************************************************************/
#include "pppoecom.h"
#include "pppdpproto.h"
#include "genproto.h"
/************************************************************************/
/*  Function Name   :PPPoERcvdPADI                                      */
/*  Description     :This Function is invoked when a PADI packet is     */
/*                  :is Recieved                                        */
/*  Input(s)        :pBuf:Points To the incoming Buffer                 */
/*                  :pHdr:Points To the Ethernet Header                 */
/*                  :u2Port:The Interface over which the Packet is      */
/*                  :is Recieved                                        */
/*  Output(s)       : Invokes the function PPPoESendPADO                */
/*  Returns         :Returns FAILURE or SUCCESS                         */
/*                                                                      */
/************************************************************************/
INT4
PPPoERcvdPADI (t_MSG_DESC * pBuf, tPPPoEHeader * pHdr, UINT2 u2Port)
{

    if (PPPoEVerifyPADIorR (pBuf, pHdr, u2Port) == FAILURE)
        return FAILURE;

    if ((MEMCMP (pHdr->DestMACAddr, gau1PPPoeBcastAddr, HOST_ADDR_LEN))
        || (VALID_BYTES (pBuf) + PPPOE_HDR_LEN > MAX_PADI_LEN))
    {
        PPP_TRC (ALL_FAILURE, "Invalid PADI Packet");
        return FAILURE;
    }

    return (PPPoESendPADO (pBuf, pHdr, u2Port));

}

/************************************************************************/
/*  Function Name   :PPPoERcvdPADR                                      */
/*  Description     :This Function is invoked when a PADR Packet is     */
/*                  : Recieved                                          */
/*  Input(s)        : pBuf:Points To the Incoming Packet                */
/*                  : pHdr:Points To the Ethernet Header                */
/*                  :pBinding:Points To the Binding Node                */
/*  Output(s)       :Invokes the Function PPPoESendPADS                 */
/*  Returns         : Returns SUCCESS or FAILURE                        */
/************************************************************************/
INT4
PPPoERcvdPADR (t_MSG_DESC * pBuf, tPPPoEHeader * pHdr, tPPPoEBinding * pBinding)
{
    tCfaIfInfo          IfInfo;
    UINT2               u2Port;

    u2Port = (UINT2) pBinding->u4Index;

    if (PPPoEVerifyPADIorR (pBuf, pHdr, u2Port) == FAILURE)
        return FAILURE;

    if (CfaGetIfInfo (u2Port, &IfInfo) == CFA_FAILURE)
    {
        PPP_TRC (ALL_FAILURE, "Invalid port");
        return FAILURE;
    }

    if (!PPPOE_SAME_ADDR (pHdr->DestMACAddr, IfInfo.au1MacAddr))
    {
        PPP_TRC (ALL_FAILURE, "Invalid PADR Packet");
        return FAILURE;
    }
    else
    {
        if (pBinding->ACCookieMethod != PPPOE_NONE)
        {
            if (CheckTagPresent (pBuf, pHdr->u2Length, AC_COOKIE) == FAILURE)
            {
                return FAILURE;
            }
        }

        return (PPPoESendPADS (pBuf, pHdr, u2Port));
    }
}

/************************************************************************/
/*  Function Name   :PPPoERcvdPADO                                      */
/*  Description     :This Function is invoked when a PADO is Recieved   */
/*  Input(s)        :pBuf:Points To The Incoming Packet                 */
/*                  :pHdr:Points to the Ethernet Header                 */
/*                  :u2Port:Interface over which the Packet was Recieved*/
/*  Output(s)       : None                                              */
/*  Returns         :Returns SUCCESS or FAILURE                         */
/************************************************************************/
INT4
PPPoERcvdPADO (t_MSG_DESC * pBuf, tPPPoEHeader * pHdr, UINT2 u2Port,
               UINT1 u1IsVlanPresent)
{
    tClientDiscoveryNode *pNode;
    tPADOListNode      *pPADONode;
    if (pHdr->u2SessionId != 0)
    {
        PPP_TRC (ALL_FAILURE, "Session Id!= 0 in PADO");
        return FAILURE;
    }
    if (PPPoEVerifyPADO (pBuf, pHdr) == FAILURE)
    {
        PPP_TRC (ALL_FAILURE, "PADO verification failed");
        return FAILURE;
    }

    if (PPPoEGetClientDiscoveryNode (pBuf, pHdr, u2Port, PADO, &pNode) ==
        FAILURE)
    {
        PPP_TRC (ALL_FAILURE, "Unsolicited PADO");
        return FAILURE;
    }

    /* Check for vlan id if encapsulation is present */
    if (u1IsVlanPresent == TRUE)
    {
        if ((pNode->pIf->u2VlanTci & PPPOE_TPID_VLANID_MASK) != pHdr->u2VlanId)
        {
            PPP_TRC (ALL_FAILURE, "Vlan does not match in the PADO");
            return FAILURE;
        }
    }
    else
    {
        /* Received untagged packet, confirm that VLAN is not configured on that interface */
        if (pNode->pIf->u2VlanTci != 0)
        {
            PPP_TRC (ALL_FAILURE,
                     "Received untagged packet for a VLAN interface");
            return FAILURE;
        }
    }

    if ((pPADONode = MEM_MALLOC (sizeof (tPADOListNode), tPADOListNode))
        == NULL)
    {
        PPP_TRC (ALL_FAILURE, "Memory Allocation Failed");
        return FAILURE;
    }
    if ((pPADONode->pHdr = MEM_MALLOC (sizeof (tPPPoEHeader), tPPPoEHeader))
        == NULL)
    {
        PPP_TRC (ALL_FAILURE, "Memory Allocation Failed");
        MEM_FREE (pPADONode);
        return FAILURE;
    }
    MEMCPY (pPADONode->pHdr, pHdr, sizeof (tPPPoEHeader));
    pPADONode->pRcvdBuf = pBuf;
    PPPoERelFlag = PPP_NO;

    switch (pNode->State)
    {
        case PADI_SENT:
            SLL_INIT (&(pNode->pPADOList));
            SLL_ADD (&(pNode->pPADOList), (t_SLL_NODE *) pPADONode);
            pNode->ReTxCount = 0;
            pNode->State = PADO_RCVD;
            break;
        case PADO_RCVD:
            SLL_ADD (&(pNode->pPADOList), (t_SLL_NODE *) pPADONode);
            break;
        default:
            MEM_FREE (pPADONode->pHdr);
            pPADONode->pHdr = NULL;
            MEM_FREE (pPADONode);
    }
    /* switch */
    return SUCCESS;

}

/************************************************************************/
/*  Function Name   :PPPoERcvdPADS                                      */
/*  Description     :This Function is Invoked when a PADS Packet is     */
/*                  : Recieved                                          */
/*  Input(s)        : pBuf:Pointer to the incoming Packet               */
/*                  :pHdr:Pointer to the Ethernet Header                */
/*                  :u2Port:The interface over which the packet is      */
/*                  :is Recieved                                        */
/*  Output(s)       : None                                              */
/*  Returns         :Returns SUCCESS or FAILURE                         */
/************************************************************************/
INT4
PPPoERcvdPADS (t_MSG_DESC * pBuf, tPPPoEHeader * pHdr, UINT2 u2Port,
               UINT1 u1IsVlanPresent)
{
    tClientDiscoveryNode *pNode;
    tPPPoESessionNode  *pSession;
    tPPPIf             *pIf = NULL;

    if (PPPoEGetClientDiscoveryNode (pBuf, pHdr, u2Port, PADS, &pNode) ==
        FAILURE)
    {
        PPP_TRC (ALL_FAILURE, "Unsolicited PADS");
        return FAILURE;
    }

    /* check whether session is already present */
    SLL_SCAN (&PPPoESessionList, pSession, tPPPoESessionNode *)
    {
        if ((pHdr->u2SessionId == pSession->u2SessionId)
            && PPPOE_SAME_ADDR (pSession->DestMACAddr, pHdr->DestMACAddr)
            && (pSession->u2Port == u2Port))
        {
            PPP_TRC (CONTROL_PLANE, "Session already present. Discarding PADS");
            return FAILURE;
        }
    }

    /* Check for vlan id if encapsulation is present */
    if (u1IsVlanPresent == TRUE)
    {
        if ((pNode->pIf->u2VlanTci & PPPOE_TPID_VLANID_MASK) != pHdr->u2VlanId)
        {
            PPP_TRC (ALL_FAILURE, "Vlan does not match in the PADS");
            return FAILURE;
        }
    }
    else
    {
        /* Received untagged packet, confirm that VLAN is not configured on that interface */
        if (pNode->pIf->u2VlanTci != 0)
        {
            PPP_TRC (ALL_FAILURE,
                     "Received untagged packet for a VLAN interface");
            return FAILURE;
        }
    }

    if (PPPoECProcessPADS (pBuf, pHdr, u2Port) == RPADS_PLUS)
    {
        PPPoECAddNewSession (pNode, pHdr);
    }
    else
    {
        pIf = pNode->pIf;
        PPPOE_REMOVE_DISCOVERY_NODE (pNode);

        if (PPPoEMode != PPPOE_SERVER)
        {
            /* PADS received. Server has terminated connection.
             * Try to initiate session again */
            PPPoERestart (pIf);
        }
    }
    return SUCCESS;
}

/************************************************************************/
/*  Function Name   :PPPoERcvdPADT                                      */
/*  Description     :This Function is invoked when a PADT packet is     */
/*                  : is Recieved                                       */
/*  Input(s)        :pBuf:Points to the incoming Buffer                 */
/*                  :pHdr:Points to the Incoming Ethernet Header        */
/*                  :u2Port:The interface over which Packet is Recieved */
/*  Output(s)       : None                                              */
/*  Returns         : SUCCESS or FAILURE                                */
/************************************************************************/
INT4
PPPoERcvdPADT (t_MSG_DESC * pBuf, tPPPoEHeader * pHdr, UINT2 u2Port,
               UINT1 u1IsVlanPresent)
{
    tPPPIf             *pIf = NULL;
    tPPPoESessionNode  *pSession;

    if (PPPoEGetSessionNode (pHdr, u2Port, &pSession) == FAILURE)
        return FAILURE;
    else
    {
        pIf = pSession->pIf;

        /* Check for vlan id if encapsulation is present */
        if (u1IsVlanPresent == TRUE)
        {
            if ((pSession->pIf->u2VlanTci & PPPOE_TPID_VLANID_MASK) !=
                pHdr->u2VlanId)
            {
                PPP_TRC (ALL_FAILURE, "Vlan does not match in the PADT");
                return FAILURE;
            }
        }
        else
        {
            /* Received untagged packet, confirm that VLAN is not configured on that 
               interface */
            if (pSession->pIf->u2VlanTci != 0)
            {
                PPP_TRC (ALL_FAILURE,
                         "Received untagged packet for a VLAN interface");
                return FAILURE;
            }
        }

        PPPoESendDiscoveryPkt ((UINT2) pIf->LinkInfo.IfIndex,
                               pSession->DestMACAddr, PADT,
                               pSession->u2SessionId, NULL, 0);
        PPPoEDeleteSession (pSession);
        if (PPPoEMode != PPPOE_SERVER)
        {
            /* PADT received. Server has terminated connection.
             * Try to initiate session again */
            PPPoERestart (pIf);
        }
    }

    PPP_UNUSED (pBuf);
    return SUCCESS;
}

INT4
PPPoERestart (tPPPIf * pIf)
{
    tClientDiscoveryNode *pDiscNode;
    tPPPoEBinding      *pPPPoEIf;
    if ((pDiscNode =
         MEM_CALLOC (1, sizeof (tClientDiscoveryNode),
                     tClientDiscoveryNode)) == NULL)
    {
        PPP_TRC (ALL_FAILURE, "MemAlloc for pDiscNode failed \n");
        return FAILURE;
    }
    pDiscNode->u2Port = (UINT2) (pIf->LinkInfo.PhysIfIndex);
    pDiscNode->StoredBuf = NULL;
    SLL_INIT (&pDiscNode->pPADOList);
    pDiscNode->pIf = pIf;
    PPP_INIT_TIMER (pDiscNode->PADTimer);
    pDiscNode->ReTxCount = 0;
    pDiscNode->State = PADI_SENT;
    pIf->LinkInfo.pPppoeSession = pDiscNode;
    pPPPoEIf = PPPoEBindingGetNode (pIf->LinkInfo.PhysIfIndex);
    PPPoECSendPADI ((UINT2) pDiscNode->pIf->LinkInfo.IfIndex, pDiscNode,
                    PPPOE_NORMAL);
    if(pPPPoEIf != NULL)
    {
    	pPPPoEIf->NoOfPADITrans++; 
    }
    SLL_ADD (&PPPoEClientDiscList, (t_SLL_NODE *) pDiscNode);
    return SUCCESS;
}

/************************************************************************/
/*  Function Name   :PPPoEVerifyPADIorR                                 */
/*  Description     :This Function is invoked when a PADT packet is     */
/*                  :when a PADI or PADR is Recieved                    */
/*  Input(s)        :pBuf:Points to the incoming Buffer                 */
/*                  :pHdr:Points to the Incoming Ethernet Header        */
/*                  :u2Port:The interface over which Packet is Recieved */
/*  Output(s)       : None                                              */
/*  Returns         : SUCCESS or FAILURE                                */
/************************************************************************/
INT4
PPPoEVerifyPADIorR (t_MSG_DESC * pBuf, tPPPoEHeader * pHdr, UINT2 u2Port)
{
    UINT2               u2Count;
    tPPPoEBinding      *pPppoeBinding;

    if (gu4PPPoEActiveSessions >= gu4PPPoEMaxSession)
    {
        PPP_TRC (ALL_FAILURE, "Can not support more PPPoE sessions");
        return FAILURE;
    }

    if (CheckTagPresent (pBuf, pHdr->u2Length, SERVICE_NAME) == FAILURE)
    {
        PPP_TRC (CONTROL_PLANE, "No Service Name Tag.Discarding PADI/PADR");
        return FAILURE;
    }

    u2Count = PPPoEActiveSessionPerHost (pHdr);

    if (u2Count >= gu4PPPoEMaxSessionPerHost)
    {
        PPP_TRC (CONTROL_PLANE,
                 "Can't support any more sessions for this Host");
        return FAILURE;
    }

    pPppoeBinding = PPPoEBindingGetNode (u2Port);
    if (pPppoeBinding == NULL)
    {
        return FAILURE;
    }

    if (pPppoeBinding->NoOfActiveSessions >= pPppoeBinding->MaxSessions)
    {
        PPP_TRC (CONTROL_PLANE, "Can't support any more sessions in this line");
        return FAILURE;
    }

    if (pHdr->u2SessionId != 0)
    {
        PPP_TRC (CONTROL_PLANE, "Session Id!= 0. Dropping PADI/PADR");
        return FAILURE;
    }

    return SUCCESS;

}

/************************************************************************/
/*  Function Name   :PPPoESendPADO                                      */
/*  Description     :This function is called when a PADO packet is to be*/
/*                  : sent                                              */
/*  Input(s)        :pBuf:Points to the incoming Buffer                 */
/*                  :pHdr:Points to the Incoming Ethernet Header        */
/*                  :u2Port:The interface over which Packet is Recieved */
/*  Output(s)       : None                                              */
/*  Returns         :SUCCESS or FAILURE                                 */
/************************************************************************/
INT4
PPPoESendPADO (t_MSG_DESC * pBuf, tPPPoEHeader * pHdr, UINT2 u2Port)
{
    tPPPoETag           Tag;
    UINT1              *pRespTags;
    UINT2               RespOffset = 0;
    UINT2               Offset = 0;
    UINT2               BufLen;
    UINT1               FoundFlag = PPP_NO;
    UINT1               TagFreeFlag = PPP_NO;
    UINT1               aTagValue[DEF_TAG_VALUE_LEN];
    tPPPIf             *pIf = NULL;

    if ((pRespTags = MEM_CALLOC (DEF_MAX_PADO_LEN, 1, UINT1)) == NULL)
    {
        PPP_TRC (ALL_FAILURE, "Error:MemAlloc for Tags Failed:");
        return FAILURE;
    }
    BufLen = DEF_MAX_PADO_LEN;
    PPP_TRC (CONTROL_PLANE, " Tags :");

    for (Offset = 0; Offset < pHdr->u2Length;
         Offset = (UINT2) (Offset + Tag.Len + TAG_HDR_LEN))
    {
        GET2BYTE (pBuf, Offset, Tag.Type);
        GET2BYTE (pBuf, Offset + TAG_TYPE_LEN, Tag.Len);

        TagFreeFlag = PPP_NO;

        if (Tag.Len)
        {
            if (Tag.Len > DEF_TAG_VALUE_LEN)
            {
                if ((Tag.pValue = MEM_CALLOC (1, Tag.Len, UINT1)) == NULL)
                {
                    PPP_TRC (ALL_FAILURE, "Error:MemAlloc for Tags Failed:");
                    MEM_FREE (pRespTags);
                    return FAILURE;
                }
                GETSTR (pBuf, Tag.pValue, Offset + TAG_HDR_LEN, Tag.Len);
                TagFreeFlag = PPP_YES;
            }
            else
            {
                GETSTR (pBuf, aTagValue, Offset + TAG_HDR_LEN, Tag.Len);
                Tag.pValue = aTagValue;
            }

        }

        switch (Tag.Type)
        {
            case END_OF_LIST:
                PPP_TRC (CONTROL_PLANE, "\tEND_OF_LIST");
                Offset = pHdr->u2Length;
                break;
            case SERVICE_NAME:
                PPP_TRC (CONTROL_PLANE, "\tSERVICE_NAME");
                if (FoundFlag == PPP_NO)
                    FoundFlag = PPP_YES;
                else
                {
                    MEM_FREE (pRespTags);
                    TAG_FREE (Tag);
                    return FAILURE;
                }

                if (PPPoESProcessServiceName
                    (&pRespTags, &RespOffset, &Tag, PADI, &BufLen) == FAILURE)
                {
                    MEM_FREE (pRespTags);
                    TAG_FREE (Tag);
                    PPP_TRC (CONTROL_PLANE, "\t SERVICE NAME SELECTION FAILED");
                    return FAILURE;
                }
                break;

            case HOST_UNIQUE:
                PPP_TRC (CONTROL_PLANE, "\tHOST_UNIQUE");
                AddTags (&pRespTags, &RespOffset, &Tag, &BufLen);
                break;
            case GENERIC_ERROR:
                PPP_TRC (CONTROL_PLANE, "\tGENERIC_ERROR");
                MEM_FREE (pRespTags);
                TAG_FREE (Tag);
                return SUCCESS;
            default:
                break;
        }

        TAG_FREE (Tag);

    }

    /* adding AC-Name Tag */
    Tag.Type = AC_NAME;
    Tag.Len = (UINT2) (STRLEN (pPPPoEACName));
    Tag.pValue = pPPPoEACName;
    AddTags (&pRespTags, &RespOffset, &Tag, &BufLen);
    /* adding AC Cookie Tag if enabled */
    PPPoEAddCookieTag (&pRespTags, pHdr, u2Port, &RespOffset, &BufLen);

    /* send PADO */

    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if ((pIf->LinkInfo.PhysIfIndex == u2Port))
        {
            break;
        }
    }

    if (pIf != NULL)
    {
        if (pIf->LcpStatus.AdminStatus == STATUS_DOWN)
        {
            PPP_TRC (ALL_FAILURE,
                     "PPP Interface Down - Failed to Send PADO \n");
            return FAILURE;
        }
        PPPoESendDiscoveryPkt ((UINT2) pIf->LinkInfo.IfIndex, pHdr->SrcMACAddr,
                               PADO, 0, pRespTags, RespOffset);
        MEM_FREE (pRespTags);
    }
    else
    {

        PPP_TRC (ALL_FAILURE, "Failed to Send PADO \n");
    }

    return SUCCESS;

}

/************************************************************************/
/*  Function Name   :PPPoESendPADS                                      */
/*  Description     :This function is invoked when a PADS packet is to  */
/*                  :sent                                               */
/*  Input(s)        :pBuf:Points to the incoming Buffer                 */
/*                  :pHdr:Points to the Incoming Ethernet Header        */
/*                  :u2Port:The interface over which Packet is Recieved */
/*  Output(s)       : None                                              */
/*  Returns         :SUCCESS or FAILURE                                 */
/************************************************************************/
INT4
PPPoESendPADS (t_MSG_DESC * pBuf, tPPPoEHeader * pHdr, UINT2 u2Port)
{
    tPPPoETag           Tag;
    UINT1              *pRespTags;
    UINT2               RespOffset = 0;
    UINT2               Offset = 0;
    UINT2               BufLen;
    UINT1               FoundFlag = PPP_NO;
    UINT1               TagFreeFlag = PPP_NO;
    UINT1               DupFlag = PPP_NO;
    INT4                ServiceRetVal = FAILURE;
    UINT2               Id = 0;
    UINT1               aTagValue[DEF_TAG_VALUE_LEN];
    UINT4               PPPIfIndex = 0;
    tPPPIf             *pIf;
    MEMSET (&Tag, 0, sizeof (tPPPoETag));

    if ((pRespTags = MEM_CALLOC (1, DEF_MAX_PADO_LEN, UINT1)) == NULL)
    {
        PPP_TRC (ALL_FAILURE, "MemAlloc For pResptags Failed");
        return FAILURE;
    }
    BufLen = DEF_MAX_PADO_LEN;

    PPP_TRC (CONTROL_PLANE, " Tags :");
    for (Offset = 0; Offset < pHdr->u2Length;
         Offset = (UINT2) (Offset + Tag.Len + TAG_HDR_LEN))
    {
        GET2BYTE (pBuf, Offset, Tag.Type);
        GET2BYTE (pBuf, Offset + TAG_TYPE_LEN, Tag.Len);

        TagFreeFlag = PPP_NO;

        if (Tag.Len)
        {
            if (Tag.Len > DEF_TAG_VALUE_LEN)
            {
                if ((Tag.pValue = MEM_CALLOC (1, Tag.Len, UINT1)) == NULL)
                {
                    PPP_TRC (ALL_FAILURE, "MemAlloc For pResptags Failed");
                    MEM_FREE (pRespTags);
                    return FAILURE;
                }
                GETSTR (pBuf, Tag.pValue, Offset + TAG_HDR_LEN, Tag.Len);
                TagFreeFlag = PPP_YES;
            }
            else
            {
                GETSTR (pBuf, aTagValue, Offset + TAG_HDR_LEN, Tag.Len);
                Tag.pValue = aTagValue;

            }

        }

        switch (Tag.Type)
        {
            case END_OF_LIST:
                PPP_TRC (CONTROL_PLANE, "\tEND_OF_LIST");
                Offset = pHdr->u2Length;
                break;
            case SERVICE_NAME:
                PPP_TRC (CONTROL_PLANE, "\tSERVICE_NAME");
                if (FoundFlag == PPP_NO)
                    FoundFlag = PPP_YES;
                else
                {
                    MEM_FREE (pRespTags);
                    TAG_FREE (Tag);
                    return FAILURE;
                }

                if ((ServiceRetVal = PPPoESProcessServiceName (&pRespTags,
                                                               &RespOffset,
                                                               &Tag, PADR,
                                                               &BufLen)) ==
                    FAILURE)
                {
                    MEM_FREE (pRespTags);
                    TAG_FREE (Tag);
                    return FAILURE;
                }
                break;

            case HOST_UNIQUE:
                PPP_TRC (CONTROL_PLANE, "\tHOST_UNIQUE");
                AddTags (&pRespTags, &RespOffset, &Tag, &BufLen);
                break;

            case AC_COOKIE:
                PPP_TRC (CONTROL_PLANE, "\tAC_COOKIE");
                if (PPPoESVerifyCookie (&Tag, pHdr, u2Port, &DupFlag, &Id) ==
                    FAILURE)
                {
                    MEM_FREE (pRespTags);
                    TAG_FREE (Tag);
                    return FAILURE;
                }
                break;

            case GENERIC_ERROR:
                PPP_TRC (CONTROL_PLANE, "\tGENERIC_ERROR");
                MEM_FREE (pRespTags);
                TAG_FREE (Tag);
                return SUCCESS;
            default:
                break;
        }
      TAG_FREE(Tag);
    }

    if (ServiceRetVal != SERVICE_NAME_ERROR)
    {
        if (DupFlag == PPP_YES)
        {
            pHdr->u2SessionId = Id;
        }
        else
        {
            if (GetFreePPPIfIndex (&PPPIfIndex, &pIf, u2Port, PPP_OE_IFTYPE) ==
                FAILURE)
            {
                PPP_TRC (CONTROL_PLANE,
                         "No Free PPP Interface available.Sending AC_SYSTEM_ERROR");
                pHdr->u2SessionId = 0;
                Tag.Type = AC_SYSTEM_ERROR;
                Tag.Len = 0;
                AddTags (&pRespTags, &RespOffset, &Tag, &BufLen);
            }
            else
            {
                tPPPoESessionNode  *pSession;
                if ((pSession = MEM_MALLOC (sizeof (tPPPoESessionNode),
                                            tPPPoESessionNode)) == NULL)
                {
                    PPP_TRC (CONTROL_PLANE, "Memory Allocation Failed\n");
                    MEM_FREE (pRespTags);
                    return FAILURE;
                }
                pSession->pIf = pIf;
                MEMCPY (pSession->DestMACAddr, pHdr->SrcMACAddr, HOST_ADDR_LEN);
                MEMCPY (pSession->SrcMACAddr, pHdr->DestMACAddr, HOST_ADDR_LEN);
                pSession->u2Port = u2Port;
                pSession->u2SessionId = pHdr->u2SessionId = (UINT2) PPPIfIndex;
                pSession->ACCookieId = Id;
                PPPoEAddNewSession (pSession);
                PPP_TRC (CONTROL_PLANE, "Added New PPPoE Session.");
                PPP_TRC1 (CONTROL_PLANE, "PPP Interface : [%d]", PPPIfIndex);
                pIf->EchoInfo.KeepAliveTimeOutVal = PPP_DEF_KEEPALIVE_TIME;
            }
        }
    }
    else
    {
        PPP_TRC (CONTROL_PLANE, "Service name error");
        pHdr->u2SessionId = 0;
    }

    /* sending PADS */
    PPPoESendDiscoveryPkt ((UINT2)PPPIfIndex, pHdr->SrcMACAddr, PADS,
                           pHdr->u2SessionId, pRespTags, RespOffset);
    MEM_FREE (pRespTags);
    return SUCCESS;

}

/************************************************************************/
/*  Function Name   :CheckTagPresent                                    */
/*  Description     :This Function is invoked to verify the tags present*/
/*                  :incoming packet                                    */
/*  Input(s)        :pBuf:Points to the incoming Buffer                 */
/*                  :Len:Length of the incoming packet                  */
/*                  :Type:Indicates the Tag                             */
/*  Output(s)       : None                                              */
/*  Returns         :SUCCESS or FAILURE                                 */
/************************************************************************/
INT4
CheckTagPresent (t_MSG_DESC * pBuf, UINT2 Len, UINT2 Type)
{
    tPPPoETag           Tag;
    UINT2               Offset;

    for (Offset = 0; Offset < Len;
         Offset = (UINT2) (Offset + Tag.Len + TAG_HDR_LEN))
    {
        GET2BYTE (pBuf, Offset, Tag.Type);
        if (Tag.Type == Type)
            return SUCCESS;
        if (Tag.Type == END_OF_LIST)
            break;
        GET2BYTE (pBuf, Offset + TAG_TYPE_LEN, Tag.Len);
    }
    return FAILURE;
}

/************************************************************************/
/*  Function Name   :AddTags                                            */
/*  Description     :This function is invoked to add tags               */
/*  Input(s)        :pRespTags:Points to the buffer where tags  are     */
/*                  :added                                              */
/*                  :Offset:Refers to the displacement                  */
/*                  :Tag:Points to the Tag structure                    */
/*                  :BufLen:Length of the Buffer                        */
/*  Output(s)       : None                                              */
/*  Returns         :                                                   */
/************************************************************************/
VOID
AddTags (UINT1 **pRespTags, UINT2 *Offset, tPPPoETag * pTag, UINT2 *BufLen)
{
    if (*Offset + pTag->Len + TAG_HDR_LEN > *BufLen)
    {
        UINT2               u2NewLen;
        u2NewLen = (UINT2) ((*BufLen) + pTag->Len + TAG_HDR_LEN);
        *pRespTags = MEM_REALLOC (*pRespTags, u2NewLen, UINT1);
        if (*pRespTags == NULL)
            return;
        *BufLen = u2NewLen;
    }

    *(UINT2 *) (VOID *) ((UINT1 *) (*pRespTags + *Offset)) =
        OSIX_HTONS (pTag->Type);
    *(UINT2 *) (VOID *) ((UINT1 *) (*pRespTags + *Offset + TAG_TYPE_LEN)) =
        OSIX_HTONS (pTag->Len);
    if (pTag->Len)
        MEMCPY ((*pRespTags + *Offset + TAG_HDR_LEN), pTag->pValue, pTag->Len);

    *Offset = (UINT2) (*Offset + pTag->Len + TAG_HDR_LEN);
}

/************************************************************************/
/*  Function Name   :PPPoESProcessServiceName                           */
/*  Description     :This function is called to Process the service name*/
/*  Input(s)        :pRespTags:Points to the buffer where tags  are     */
/*                  :added                                              */
/*                  :Offset:Refers to the displacement                  */
/*                  :Tag:Points to the Tag structure                    */
/*                  :BufLen:Length of the Buffer                        */
/*                  :Code:Identifies the type of Discovery Packet       */
/*  Output(s)       : None                                              */
/*  Returns         : SUCCESS or FAILURE                                */
/************************************************************************/
INT4
PPPoESProcessServiceName (UINT1 **pRespTags, UINT2 *Offset, tPPPoETag * pTag,
                          UINT1 Code, UINT2 *BufLen)
{
    tPPPoEService      *pService;
    tPPPoETag           Tag;
    Tag.Type = SERVICE_NAME;

    /* If Service name list is empty send serice name tag with zero 
     * Length. if PADR is received with zero service name length send
     * zero length Tag.
     * */
    if ((SLL_COUNT (&PPPoEServiceList) == 0)
        || ((Code == PADR) && (pTag->Len == 0)))
    {
        Tag.Len = 0;
        AddTags (pRespTags, Offset, &Tag, BufLen);
        return SUCCESS;
    }

    /* when a PADI is received with zero length service name tag
     * All the services availabe in the list is sent to the Host
     * */
    if ((Code == PADI) && (pTag->Len == 0))
    {
        SLL_SCAN (&PPPoEServiceList, pService, tPPPoEService *)
        {
            Tag.Len = (UINT2) (STRLEN (pService->ServiceName));
            Tag.pValue = pService->ServiceName;
            AddTags (pRespTags, Offset, &Tag, BufLen);
        }                        /* SLL_SCAN */
        return SUCCESS;
    }

    /* pTag->Len > 0  ... */

    /* when a PADI/PADR received with *non-zero* length service
     * name tag rcvd tag is searched in the servicename list for
     * a match.If a match found that value is put as the Tag.
     * */
    SLL_SCAN (&PPPoEServiceList, pService, tPPPoEService *)
    {
        if ((pTag->Len == STRLEN (pService->ServiceName)) &&
            (!MEMCMP (pTag->pValue, pService->ServiceName, pTag->Len)))
        {
            AddTags (pRespTags, Offset, pTag, BufLen);
            return SUCCESS;
        }
    }                            /* SLL_SCAN */

    if (Code == PADI)
    {
        return FAILURE;
    }
    /* sending service-name-error tag */
    if (Code == PADR)
    {
        Tag.Type = SERVICE_NAME_ERROR;
        Tag.Len = 0;
        AddTags (pRespTags, Offset, &Tag, BufLen);
        return SERVICE_NAME_ERROR;
    }

    return FAILURE;

}

/************************************************************************/
/*  Function Name   :PPPoEAddCookieTag                                  */
/*  Description     :This function is invoked to add tags to the        */
/*                  :incoming Buffer                                    */
/*  Input(s)        :pRespTags:Points to the buffer where tags  are     */
/*                  :added                                              */
/*                  :Offset:Refers to the displacement                  */
/*                  :pHdr:Points to the Ethernet structure              */
/*                  :Len:Length of the Buffer                           */
/*                  :u2Port:The interface over which the packet is      */
/*                  :Recieved                                           */
/*  Output(s)       : None                                              */
/*  Returns         :                                                   */
/************************************************************************/
VOID
PPPoEAddCookieTag (UINT1 **pRespTags, tPPPoEHeader * pHdr,
                   UINT2 u2Port, UINT2 *Offset, UINT2 *Len)
{
    tPPPoEBinding      *pEthIf;
    tPPPoETag           Tag;
    UINT2               NewId;

    pEthIf = PPPoEBindingGetNode (u2Port);
    if (pEthIf != NULL)
    {
        switch (pEthIf->ACCookieMethod)
        {
            case HMAC_MD5:
                Tag.Type = AC_COOKIE;
                Tag.Len = AC_COOKIE_TAG_LEN;
                Tag.pValue = MEM_MALLOC (AC_COOKIE_TAG_LEN, UINT1);
                if (Tag.pValue != NULL)
                {
                    hmac_md5 (pHdr->SrcMACAddr, HOST_ADDR_LEN,
                              pEthIf->Key, (INT2) STRLEN (pEthIf->Key),
                              Tag.pValue);
                    NewId = PPPoESGetNewCookieId (pHdr, u2Port);
                    NewId = OSIX_HTONS (NewId);
                    MEMCPY ((Tag.pValue + HMAC_DIGEST_LEN), (UINT1 *) &NewId,
                            COOKIE_ID_LEN);
                    AddTags (pRespTags, Offset, &Tag, Len);
                    MEM_FREE (Tag.pValue);
                }
                break;
            default:
                break;
        }
    }
}

/************************************************************************/
/*  Function Name   :PPPoESVerifyCookie                                 */
/*  Description     :This function is called by the Server to verify    */
/*                  : AC-Cookie Tag                                     */
/*  Input(s)        :pTag:Points to the Tag structure                   */
/*                  :DupFlag:Indicates Whether it is a duplicate        */
/*                  :Id:Id of the Packet                                */
/*                  :u2Port:The interface over which the packet is      */
/*                  :Recieved                                           */
/*  Output(s)       : None                                              */
/*  Returns         :                      */
/*                                        */
/************************************************************************/
INT4
PPPoESVerifyCookie (tPPPoETag * pTag, tPPPoEHeader * pHdr, UINT2 u2Port,
                    UINT1 *DupFlag, UINT2 *Id)
{
    UINT1               Digest[16] = { 0 };
    tPPPoEBinding      *pEthIf;
    tPPPoESessionNode  *pSession;
    UINT2               RcvdId;

    pEthIf = PPPoEBindingGetNode (u2Port);
    if (pEthIf != NULL)
    {
        switch (pEthIf->ACCookieMethod)
        {
            case HMAC_MD5:
                hmac_md5 (pHdr->SrcMACAddr, HOST_ADDR_LEN,
                          pEthIf->Key, (INT2) STRLEN (pEthIf->Key), Digest);
                if (MEMCMP (Digest, pTag->pValue, HMAC_DIGEST_LEN) != 0)
                {
                    PPP_TRC (ALL_FAILURE, "Cookie Mismatch ..");
                    return FAILURE;
                }
                RcvdId =
                    *(UINT2 *) (VOID *) ((UINT1 *) pTag->pValue +
                                         HMAC_DIGEST_LEN);
                RcvdId = OSIX_NTOHS (RcvdId);

                SLL_SCAN (&PPPoESessionList, pSession, tPPPoESessionNode *)
                {
                    if ((RcvdId == pSession->ACCookieId)
                        && PPPOE_SAME_ADDR (pHdr->SrcMACAddr,
                                            pSession->DestMACAddr)
                        && (u2Port == pSession->u2Port))
                    {
                        PPP_TRC (CONTROL_PLANE,
                                 "Duplicate PADR ..Resending PADS");
                        *DupFlag = PPP_YES;
                        *Id = pSession->u2SessionId;
                        return SUCCESS;
                    }
                }
                *Id = RcvdId;
                break;
            default:
                break;
        }
    }
    return SUCCESS;
}

/************************************************************************/
/*  Function Name   :PPPoESGetNewCookieId                               */
/*  Description     :This function is invoked to get Cookie-Id          */
/*  Input(s)        :pHdr:Points to the Ethernet structure              */
/*                  :u2Port:The interface over which the packet is      */
/*  Output(s)       : None                                              */
/*  Returns         :                                                   */
/************************************************************************/
UINT2
PPPoESGetNewCookieId (tPPPoEHeader * pHdr, UINT2 u2Port)
{
    UINT2               i = 0;
    tPPPoESessionNode  *pSession;
    SLL_SCAN (&PPPoESessionList, pSession, tPPPoESessionNode *)
    {
        if (PPPOE_SAME_ADDR (pHdr->SrcMACAddr, pSession->DestMACAddr)
            && (u2Port == pSession->u2Port))
        {

            if (pSession->ACCookieId > i)
                i = pSession->ACCookieId;

        }
    }
    PPP_TRC1 (CONTROL_PLANE, "Cookie ID : %d ", i + 1);
    return ((UINT2) (i + 1));
}

/************************************************************************/
/*  Function Name   :PPPoEGetClientDiscoveryNode                        */
/*  Description     :This Function is invoked to get a DiscoveryNode    */
/*  Input(s)        :pBuf:Points to the Buffer                          */
/*                  :pDiscNode:Pointer to Discovery Node                */
/*                  :Code:Identifies the type ofdiscovery Packet        */
/*                  :u2Port:The interface over which the packet is      */
/*  Output(s)       :Pointer to Discovery Node                          */
/*  Returns         :SUCCESS or FAILURE                                 */
/************************************************************************/
INT4
PPPoEGetClientDiscoveryNode (t_MSG_DESC * pBuf, tPPPoEHeader * pHdr,
                             UINT2 u2Port, UINT1 Code,
                             tClientDiscoveryNode ** pDiscNode)
{
    tClientDiscoveryNode *pNode;
    tPPPoETag           Tag;
    UINT1              *pTagVal;

    if (gHostUniqueFlag != PPPOE_ENABLED)
    {
        SLL_SCAN (&PPPoEClientDiscList, pNode, tClientDiscoveryNode *)
        {
            if (pNode->u2Port == u2Port)
            {
                if (Code == PADO)
                {
                    if ((pNode->State == PADI_SENT) ||
                        (pNode->State == PADO_RCVD))
                    {
                        *pDiscNode = pNode;
                        return SUCCESS;
                    }
                }
                 /*PADO*/
                if ((Code == PADS)
                   &&
                   PPPOE_SAME_ADDR (pNode->DestMACAddr, pHdr->SrcMACAddr)
                   && (pNode->State == PADR_SENT))
                {
                    *pDiscNode = pNode;
                    return SUCCESS;
                }
             }
             /*PADS*/
        }                        /* SLL_SACN */
        return FAILURE;
    }                            /* disabled */
    else
    {                            /* enabled */
        /* host unique is enabled ! */
        if (CheckTagPresent (pBuf, pHdr->u2Length, HOST_UNIQUE) == SUCCESS)
        {
            if (GetPPPoETag (pBuf, &Tag, pHdr->u2Length,
                             HOST_UNIQUE) == FAILURE)
                return FAILURE;
            if (Tag.Len != HOST_UNIQUE_TAG_LEN)
                return FAILURE;
        }
        else
        {
            PPP_TRC (CONTROL_PLANE,
                     "Host unique tag enabled..\n\tbut not present in the response");
            return FAILURE;
        }

        pTagVal = Tag.pValue;

        SLL_SCAN (&PPPoEClientDiscList, pNode, tClientDiscoveryNode *)
        {
            if ((pNode->u2Port == u2Port) &&
                (!MEMCMP (pTagVal, pNode->aHostUnique, HOST_UNIQUE_TAG_LEN)))
            {

                if ((Code == PADO) &&
                    ((pNode->State == PADI_SENT)
                     || (pNode->State == PADO_RCVD)))
                {
                    *pDiscNode = pNode;
                    MEM_FREE (Tag.pValue);
                    return SUCCESS;
                }
                 /*PADI*/
                if ((Code == PADS)
                    &&
                    (MEMCMP
                    (pNode->DestMACAddr, pHdr->SrcMACAddr,
                     HOST_ADDR_LEN) == 0) && (pNode->State == PADR_SENT))
                {
                    *pDiscNode = pNode;
                    MEM_FREE (Tag.pValue);
                    return SUCCESS;
                }
             }
             /*PADS*/           /* if */
        }                        /* SLL_SCAN */
        PPP_TRC (CONTROL_PLANE, "No match found for the rcvd PADO/PADS");
        MEM_FREE (Tag.pValue);
        return FAILURE;
    }                            /* enabled */
}

/************************************************************************/
/*  Function Name   :GetPPPoETag                                        */
/*  Description     :This function is used to extract the Tag from the  */
/*                  :packet                                             */
/*  Input(s)        :pBuf:Points to incoming Buffer                     */
/*                  :pTag:Pointerto the Tag Structure                   */
/*                  :TgaType:Indicates the Tag Type                     */
/*                  :Len:Length of the Packet                           */
/*  Output(s)       : None                                              */
/*  Returns         : SUCCESS or FAILURE                                */
/************************************************************************/
INT4
GetPPPoETag (t_MSG_DESC * pBuf, tPPPoETag * pTag, UINT2 Len, UINT2 TagType)
{
    UINT2               Offset = 0;

    for (; Offset < Len; Offset = (UINT2) (Offset + pTag->Len + TAG_HDR_LEN))
    {
        GET2BYTE (pBuf, Offset, pTag->Type);
        GET2BYTE (pBuf, Offset + TAG_TYPE_LEN, pTag->Len);

        if (pTag->Type == TagType)
        {
            if (pTag->Len)
            {
                pTag->pValue = MEM_MALLOC (pTag->Len, UINT1);
                GETSTR (pBuf, pTag->pValue, Offset + TAG_HDR_LEN, pTag->Len);
            }
            else
            {
                pTag->pValue = NULL;
            }
            return SUCCESS;
        }
    }
    return FAILURE;
}

/************************************************************************/
/*  Function Name   :PPPoECSendPADI                                     */
/*  Description     :This Function is invoked by the client to send PADI*/
/*  Input(s)        :u2Port:The interface over which the packet is      */
/*                  :recieved                                           */
/*                  :pDiscNode:Pointer to DiscoveryNode                 */
/*                  :Mode:Indicates Whether it is normal or             */
/*                  :Retransmission                                     */
/*  Output(s)       : None                                              */
/*  Returns         :                                                   */
/************************************************************************/
VOID
PPPoECSendPADI (UINT2 u2Port, tClientDiscoveryNode * pDiscNode, UINT1 Mode)
{

    tPPPoETag           Tag;
    UINT1              *pRespTags;
    UINT2               RespOffset = 0;
    UINT2               BufLen;
    UINT2               u2Val;
    u2Val = (UINT2) (GET_RANDOM_NUMBER ());

    MEMSET (&Tag, 0, sizeof (tPPPoETag));
    /* Check whether PPP Interface is ADMIN UP. If not, dont send PADI packet */
    if (pDiscNode->pIf->LcpStatus.AdminStatus == STATUS_DOWN)
    {
        PPP_TRC (ALL_FAILURE, "PPP Interface Down - Failed to Send PADI \n");
        return;
    }

    BufLen = DEF_MAX_PADI_LEN;

    if ((Mode != PPPOE_NORMAL) && (Mode != PPPOE_RETRANS))
        return;

    if (Mode == PPPOE_NORMAL)
    {
        tPPPoEService      *pService;

        if ((pRespTags = MEM_CALLOC (1, DEF_MAX_PADI_LEN, UINT1)) == NULL)
        {
            PPP_TRC (ALL_FAILURE, "MemAlloc for pRespTags Failed \n");
            return;
        }
        if (gHostUniqueFlag == PPPOE_ENABLED)
        {
            Tag.Type = HOST_UNIQUE;
            Tag.Len = HOST_UNIQUE_TAG_LEN;
            Tag.pValue = (UINT1 *) &u2Val;
            AddTags (&pRespTags, &RespOffset, &Tag, &BufLen);

            /* storing host unique tag */
            MEMCPY (pDiscNode->aHostUnique, (UINT1 *) &u2Val,
                    HOST_UNIQUE_TAG_LEN);
        }

        Tag.Type = SERVICE_NAME;
        if (SLL_COUNT (&PPPoEServiceList) == 0)
        {
            Tag.Len = 0;
        }
        pService = (tPPPoEService *) SLL_FIRST (&PPPoEServiceList);
        if ((pService) && (pService->ServiceName))
        {
            Tag.Len = (UINT2) (STRLEN (pService->ServiceName));
            Tag.pValue = pService->ServiceName;
        }
        AddTags (&pRespTags, &RespOffset, &Tag, &BufLen);
        pDiscNode->StoredBuf = MEM_MALLOC (RespOffset, UINT1);
        if (pDiscNode->StoredBuf != NULL)
        {
            MEMCPY (pDiscNode->StoredBuf, pRespTags, RespOffset);
        }
        pDiscNode->StoredBufLen = RespOffset;
        PPP_INIT_TIMER (pDiscNode->PADTimer);

        PPPoESendDiscoveryPkt ((UINT2) pDiscNode->pIf->LinkInfo.IfIndex,
                               gau1PPPoeBcastAddr, PADI, 0, pRespTags,
                               RespOffset);
        MEM_FREE (pRespTags);
    }
    else
    {
        PPP_TRC1 (CONTROL_PLANE, "PADI ReTx Count :[%d] ",
                  pDiscNode->ReTxCount);
        /* RETRANSMISSION */
        PPPoESendDiscoveryPkt ((UINT2) pDiscNode->pIf->LinkInfo.IfIndex,
                               gau1PPPoeBcastAddr, PADI, 0,
                               pDiscNode->StoredBuf, pDiscNode->StoredBufLen);
    }
    PPPoERestartTimer (pDiscNode);
    PPP_UNUSED (u2Port);
    return;
}

/************************************************************************/
/*  Function Name   :PPPoEPADTimeOut                                    */
/*  Description     :This Function is invoked when PADT timer expires   */
/*  Input(s)        :pDiscNode:Pointer to DiscoveryNode                 */
/*  Output(s)       : None                                              */
/*  Returns         :                                                   */
/************************************************************************/
VOID
PPPoEPADTimeOut (VOID *pNode)
{
    tPADOListNode      *pAC;
    tClientDiscoveryNode *pDiscNode;
    pDiscNode = (tClientDiscoveryNode *) pNode;
    tPPPoEBinding      *pPPPoEIf;
    if (pDiscNode == NULL)
        return;


    pPPPoEIf = PPPoEBindingGetNode (pDiscNode->u2Port);   
    switch (pDiscNode->State)
    {
        case PADI_SENT:
            PPP_TRC (CONTROL_PLANE, "Time out in PADI_SENT state.");
            PPPoECSendPADI (pDiscNode->u2Port, pDiscNode, PPPOE_RETRANS);
            if(pPPPoEIf != NULL)
            {
	    	pPPPoEIf->NoOfPADITrans++;
            }
            break;
        case PADO_RCVD:
            PPP_TRC (CONTROL_PLANE, "Time out in PADO_RCVD state.");
            pAC = PPPoESelectAC (pDiscNode);
            if (pAC == NULL)
                PPPoEGiveEventToPPP (pDiscNode, CLOSE);
            else
            {
                PPPoESendPADR (pAC, pDiscNode);
                pDiscNode->State = PADR_SENT;
                if(pPPPoEIf != NULL)
                { 
                	pPPPoEIf->NoOfPADRTrans++;
                }
            }
            SLL_DELETE_LIST (&pDiscNode->pPADOList, PPPoEDeletePADOListNodes);
            break;

        case PADR_SENT:
            PPP_TRC (CONTROL_PLANE, "Time out in PADR_SENT state.");

            if (pDiscNode->ReTxCount < gu1PPPoEMaxNoOfRetries)
            {
                PPP_TRC (CONTROL_PLANE, "Retransmitting PADR ... ");
                PPPoESendDiscoveryPkt ((UINT2) pDiscNode->pIf->LinkInfo.IfIndex,
                                       pDiscNode->DestMACAddr, PADR,
                                       0, pDiscNode->StoredBuf,
                                       pDiscNode->StoredBufLen);
                PPPoERestartTimer (pDiscNode);
            }
            else
            {
                pDiscNode->ReTxCount = 0;
                FREE_DISC_STORED_BUF (pDiscNode);
                pDiscNode->State = PADI_SENT;
                PPP_TRC (CONTROL_PLANE, "PADR Retransmission over.");
                PPP_TRC (CONTROL_PLANE, "Resending PADI");
                PPPoECSendPADI (pDiscNode->u2Port, pDiscNode, PPPOE_NORMAL);
                if(pPPPoEIf != NULL)
                {
                    pPPPoEIf->NoOfPADITrans++;
                }
            }
            break;
        default:
            break;
    }

}

/************************************************************************/
/*  Function Name   :PPPoESelectAC                                      */
/*  Description     :This Function is invoked by the Client to select   */
/*                  :the Server                                         */
/*  Input(s)        :pDiscNode:Pointer to DiscoveryNode                 */
/*  Output(s)       : None                                              */
/*  Returns         :Pointer to the PADO Node                           */
/************************************************************************/
tPADOListNode      *
PPPoESelectAC (tClientDiscoveryNode * pDiscNode)
{
    tPADOListNode      *pPADO, *pBestMatch;
    tPPPoEService      *pService = NULL;
    UINT1               Count;
    INT1                grade, oldgrade;

    pClientServiceName = NULL;
    pBestMatch = NULL;

    if ((Count = (UINT1) SLL_COUNT (&pDiscNode->pPADOList)) == 1)
    {
        pPADO = (tPADOListNode *) SLL_FIRST (&pDiscNode->pPADOList);
        PPP_UNUSED (Count);
        return pPADO;
    }

    if ((pService = (tPPPoEService *) SLL_FIRST (&PPPoEServiceList)) != NULL)
    {
        pClientServiceName = pService->ServiceName;
    }

    grade = oldgrade = -1;
    SLL_SCAN (&pDiscNode->pPADOList, pPADO, tPADOListNode *)
    {
        grade = (INT1) PPPoEGradePADO (pPADO);
        if (grade > oldgrade)
        {
            pBestMatch = pPADO;
            oldgrade = grade;
        }
    }
    return pBestMatch;
}

/************************************************************************/
/*  Function Name   :PPPoEGradePADO                                     */
/*  Description     :This Functions allocates grades to PADO packets    */
/*  Input(s)        :pPADO:Pointer to the PADO Node                     */
/*  Output(s)       : None                                              */
/*  Returns         :Grade od the PADO packet                           */
/************************************************************************/
INT4
PPPoEGradePADO (tPADOListNode * pPADO)
{
    INT1                grade;
    tPPPoEHeader       *pHdr;
    t_MSG_DESC         *pBuf;
    tPPPoETag           Tag;
    UINT2               Offset;
    UINT1               aTagValue[DEF_TAG_VALUE_LEN];
    grade = 0;
    pBuf = pPADO->pRcvdBuf;
    pHdr = pPADO->pHdr;

    for (Offset = 0; Offset < pHdr->u2Length;
         Offset = (UINT2) (Offset + Tag.Len + TAG_HDR_LEN))
    {
        GET2BYTE (pBuf, Offset, Tag.Type);
        GET2BYTE (pBuf, Offset + TAG_TYPE_LEN, Tag.Len);

        if (Tag.Len > DEF_TAG_VALUE_LEN)
        {
            continue;
        }
        else
        {
            GETSTR (pBuf, aTagValue, Offset + TAG_HDR_LEN, Tag.Len);
            Tag.pValue = aTagValue;
        }

        switch (Tag.Type)
        {
            case END_OF_LIST:
                Offset = pHdr->u2Length;
                break;

            case SERVICE_NAME:
                if ((pClientServiceName != NULL)
                    && (MEMCMP (pClientServiceName, Tag.pValue, Tag.Len) == 0))
                {
                    grade++;
                }

                break;
            case AC_NAME:
                if (MEMCMP (pPPPoEACName, Tag.pValue, Tag.Len) == 0)
                {
                    grade = (INT1) (grade + AC_NAME_GRADE);
                }
                break;
            default:
                break;
        }
    }                            /* for */
    return grade;
}

/************************************************************************/
/*  Function Name   :PPPoEVerifyPADO                                    */
/*  Description     :This Function is used to Verify the PADO PAcket    */
/*  Input(s)        :pBuf:Pointer to incoming Buffer                    */
/*                  : pHdr:Pointer to Ethernet Structure                */
/*  Output(s)       :None                                               */
/*  Returns         :SUCCESS or FAILURE                                 */
/************************************************************************/
INT4
PPPoEVerifyPADO (t_MSG_DESC * pBuf, tPPPoEHeader * pHdr)
{
    if (CheckTagPresent (pBuf, pHdr->u2Length, SERVICE_NAME) == FAILURE)
    {
        PPP_TRC (CONTROL_PLANE, "No Service Name Tag.Discarding PADI");
        return FAILURE;
    }
    if (CheckTagPresent (pBuf, pHdr->u2Length, AC_NAME) == FAILURE)
    {
        PPP_TRC (CONTROL_PLANE, "No Service Name Tag.Discarding PADI");
        return FAILURE;
    }
    if (pHdr->u2SessionId != 0)
    {
        PPP_TRC (CONTROL_PLANE, "Session ID!=0");
        return FAILURE;
    }
    return SUCCESS;

}

/************************************************************************/
/*  Function Name   :PPPoESendPADR                                      */
/*  Description     :This Function is invoked When PADR PAcket is to    */
/*                  :sent                                               */
/*  Input(s)        :pAC:Pointer to the PADO Node                       */
/*                  :pDiscNode:Pointer to the Discovery Node            */
/*  Output(s)       : None                                              */
/*  Returns         :                                                   */
/************************************************************************/
VOID
PPPoESendPADR (tPADOListNode * pAC, tClientDiscoveryNode * pDiscNode)
{
    tPPPoETag           Tag;
    UINT1              *pRespTags;
    UINT2               RespOffset = 0;
    UINT2               Offset = 0;
    UINT2               BufLen;
    UINT1               TagFreeFlag = PPP_NO;
    UINT1               aTagValue[DEF_TAG_VALUE_LEN];
    tPPPoEHeader       *pHdr;
    t_MSG_DESC         *pBuf;

    pHdr = pAC->pHdr;
    pBuf = pAC->pRcvdBuf;

    if ((pRespTags = MEM_CALLOC (1, DEF_MAX_PADR_LEN, UINT1)) == NULL)
    {
        PPP_TRC (ALL_FAILURE, "MemAlloc for pResptags failed \n");
        return;
    }
    BufLen = DEF_MAX_PADR_LEN;

    PPP_TRC (CONTROL_PLANE, " Tags :");

    for (Offset = 0; Offset < pHdr->u2Length;
         Offset = (UINT2) (Offset + Tag.Len + TAG_HDR_LEN))
    {
        GET2BYTE (pBuf, Offset, Tag.Type);
        GET2BYTE (pBuf, Offset + TAG_TYPE_LEN, Tag.Len);

        TagFreeFlag = PPP_NO;

        if (Tag.Len)
        {
            if (Tag.Len > DEF_TAG_VALUE_LEN)
            {
                Tag.pValue = MEM_CALLOC (1, Tag.Len, UINT1);
                if (Tag.pValue != NULL)
                {
                    GETSTR (pBuf, Tag.pValue, Offset + TAG_HDR_LEN, Tag.Len);
                }
                TagFreeFlag = PPP_YES;
            }
            else
            {
                GETSTR (pBuf, aTagValue, Offset + TAG_HDR_LEN, Tag.Len);
                Tag.pValue = aTagValue;
            }

        }

        switch (Tag.Type)
        {
            case END_OF_LIST:
                PPP_TRC (CONTROL_PLANE, "\t\tEND_OF_LIST");
                Offset = pHdr->u2Length;
                break;
            case SERVICE_NAME:
                PPP_TRC (CONTROL_PLANE, "\t\tSERVICE_NAME");
                break;
            case AC_COOKIE:
                PPP_TRC (CONTROL_PLANE, "\t\tAC_COOKIE");
                AddTags (&pRespTags, &RespOffset, &Tag, &BufLen);
                break;
            case GENERIC_ERROR:
                PPP_TRC (CONTROL_PLANE, "\t\tGENERIC_ERROR");
                MEM_FREE (pRespTags);
                TAG_FREE (Tag);
                return;
            default:
                break;
        }

        TAG_FREE (Tag);

    }
    if (GetPPPoETag (pBuf, &Tag, pHdr->u2Length, SERVICE_NAME) == SUCCESS)
    {
        AddTags (&pRespTags, &RespOffset, &Tag, &BufLen);
        TAG_FREE (Tag);
    }
    else
    {
        PPP_TRC (CONTROL_PLANE, "Service Name Tag not present");
        MEM_FREE (pRespTags);
        return;

    }

    /* adding host unique tag , if enabled */
    if (gHostUniqueFlag == PPPOE_ENABLED)
    {
        UINT2               u2Val;
        u2Val = (UINT2) (GET_RANDOM_NUMBER ());
        Tag.Type = HOST_UNIQUE;
        Tag.Len = HOST_UNIQUE_TAG_LEN;
        Tag.pValue = (UINT1 *) &u2Val;
        AddTags (&pRespTags, &RespOffset, &Tag, &BufLen);
        /* storing host unique tag */
        MEMCPY (pDiscNode->aHostUnique, (UINT1 *) &u2Val, HOST_UNIQUE_TAG_LEN);
    }

    pDiscNode->StoredBuf =
        MEM_REALLOC (pDiscNode->StoredBuf, RespOffset, UINT1);
    MEMCPY (pDiscNode->StoredBuf, pRespTags, RespOffset);
    pDiscNode->StoredBufLen = RespOffset;
    MEMCPY (pDiscNode->DestMACAddr, pHdr->SrcMACAddr, HOST_ADDR_LEN);
    PPP_INIT_TIMER (pDiscNode->PADTimer);
    PPPoESendDiscoveryPkt ((UINT2) pDiscNode->pIf->LinkInfo.IfIndex,
                           pHdr->SrcMACAddr, PADR, 0, pRespTags, RespOffset);
    MEM_FREE (pRespTags);

    PPPoERestartTimer (pDiscNode);

    return;

}

/************************************************************************/
/*  Function Name   :PPPoECProcessPADS                                  */
/*  Description     :This Function is used to Process PADS PAcket       */
/*  Input(s)        :pBuf:Pointer to incoming Buffer                    */
/*                  :pHdr:Pointer to the Ethernet Structure             */
/*                  :u2Port:Points to the interface over which the      */
/*                  :packet is Recieved                                 */
/*  Output(s)       : None                                              */
/*  Returns         :                                                   */
/************************************************************************/
INT1
PPPoECProcessPADS (t_MSG_DESC * pBuf, tPPPoEHeader * pHdr, UINT2 u2Port)
{
    tPPPoETag           Tag;
    UINT2               Offset = 0;
    UINT1               FoundFlag = PPP_NO;
    UINT1               TagFreeFlag = PPP_NO;
    UINT1               aTagValue[DEF_TAG_VALUE_LEN];

    Tag.pValue = NULL;
    if (CheckTagPresent (pBuf, pHdr->u2Length, SERVICE_NAME) == FAILURE)
    {
        PPP_TRC (CONTROL_PLANE, "No Service Name Tag.Discarding PADS");
        return FAILURE;
    }

    if (pHdr->u2SessionId == 0)
    {
        PPP_TRC (CONTROL_PLANE, "Session ID ==0");
        return RPADS_MINUS;
    }

    PPP_TRC (CONTROL_PLANE, " Tags :");
    for (Offset = 0; Offset < pHdr->u2Length;
         Offset = (UINT2) (Offset + Tag.Len + TAG_HDR_LEN))
    {
        GET2BYTE (pBuf, Offset, Tag.Type);
        GET2BYTE (pBuf, Offset + TAG_TYPE_LEN, Tag.Len);

        TagFreeFlag = PPP_NO;

        if (Tag.Len)
        {
            if (Tag.Len > DEF_TAG_VALUE_LEN)
            {
                Tag.pValue = MEM_CALLOC (1, Tag.Len, UINT1);
                GETSTR (pBuf, Tag.pValue, Offset + TAG_HDR_LEN, Tag.Len);
                TagFreeFlag = PPP_YES;
            }
            else
            {
                GETSTR (pBuf, aTagValue, Offset + TAG_HDR_LEN, Tag.Len);
                Tag.pValue = aTagValue;
            }

        }

        switch (Tag.Type)
        {
            case END_OF_LIST:
                PPP_TRC (CONTROL_PLANE, "\t\tEND_OF_LIST");
                Offset = pHdr->u2Length;
                break;
            case SERVICE_NAME:
                PPP_TRC (CONTROL_PLANE, "\t\tSERVICE_NAME");
                if (FoundFlag == PPP_NO)
                    FoundFlag = PPP_YES;
                else
                {
                    TAG_FREE (Tag);
                    return RPADS_MINUS;
                }
                if (PPPoECPADSServiceName (&Tag, u2Port) != OK)
                {
                    TAG_FREE (Tag);
                    return RPADS_MINUS;
                }
                break;

            case GENERIC_ERROR:
            case SERVICE_NAME_ERROR:
            case AC_SYSTEM_ERROR:
                PPP_TRC (CONTROL_PLANE, "\t\t... ERROR");
                TAG_FREE (Tag);
                return RPADS_MINUS;

            case AC_COOKIE:
                PPP_TRC (CONTROL_PLANE, "\t\tAC_COOKIE");
            default:
                break;
        }

        TAG_FREE (Tag);
    }                            /* for */
    return RPADS_PLUS;
}

/************************************************************************/
/*  Function Name   :PPPoEInitDiscNode                                  */
/*  Description     :This function is used to initialise Discovery Node */
/*  Input(s)        :pNode:Pointer to DiscoveryNode                     */
/*  Output(s)       : None                                              */
/*  Returns         :                                                   */
/************************************************************************/
VOID
PPPoEInitDiscNode (tClientDiscoveryNode * pNode)
{
    SLL_INIT (&pNode->pPADOList);
    PPP_INIT_TIMER (pNode->PADTimer);
    FREE_DISC_STORED_BUF (pNode);
    pNode->StoredBufLen = 0;
    pNode->ReTxCount = 0;
    pNode->State = INITIAL;

}

/************************************************************************/
/*  Function Name   :PPPoECAddNewSession                                */
/*  Description     :This Function is invoked to add new Session by     */
/*                  :client                                             */
/*  Input(s)        :pNode:Pointer to Discovery Node                    */
/*                  :pHdr:Pointer to Ethernet Structure                 */
/*  Output(s)       : None                                              */
/*  Returns         :                                                   */
/************************************************************************/
VOID
PPPoECAddNewSession (tClientDiscoveryNode * pNode, tPPPoEHeader * pHdr)
{

    tPPPoESessionNode  *pSession;

    pSession = MEM_MALLOC (sizeof (tPPPoESessionNode), tPPPoESessionNode);
    if (pSession != NULL)
    {
        pSession->pIf = pNode->pIf;
        MEMCPY (pSession->DestMACAddr, pHdr->SrcMACAddr, HOST_ADDR_LEN);
        pSession->u2Port = pNode->u2Port;
        pSession->u2SessionId = pHdr->u2SessionId;
        pSession->ACCookieId = 0;

        PPPOE_REMOVE_DISCOVERY_NODE (pNode);

        PPPoEAddNewSession (pSession);
    }
}

/************************************************************************/
/*  Function Name   :PPPoEAddNewSession                                 */
/*  Description     :This Function s invoked to add new session by      */
/*                  :Server                                             */
/*  Input(s)        :pSession:Pointer To session node                   */
/*  Output(s)       : None                                              */
/*  Returns         :                                                   */
/************************************************************************/
VOID
PPPoEAddNewSession (tPPPoESessionNode * pSession)
{
    tPPPoEBinding      *pBinding;
#ifdef DPIF
    ePPPoESession       ePPPoEType;
#endif
    if ((pBinding = PPPoEBindingGetNode (pSession->u2Port)) != NULL)
    {
        PPP_TRC1 (CONTROL_PLANE, "Adding New Session on eth%d",
                  pSession->u2Port);
        PPP_TRC1 (CONTROL_PLANE, "Session Id :[ %d ]", pSession->u2SessionId);
        pBinding->NoOfActiveSessions++;
        pBinding->u4TotalNofSessions++;
        gu4PPPoEActiveSessions++;
    }

    pSession->pIf->LinkInfo.pPppoeSession = pSession;
    SLL_ADD (&PPPoESessionList, (t_SLL_NODE *) pSession);
#ifdef DPIF
    PPPoEAddNewSessionToDp (pSession);
    ePPPoEType = PPPOE_ADD_SESSION;
    PPPoESessionDp (ePPPoEType, pSession);
#endif
    PPPoEGiveEventToPPP (pSession, PPP_UP);
}

/************************************************************************/
/*  Function Name   :PPPoEDeleteSession                                 */
/*  Description     :This Function is invoked to delete a Session       */
/*  Input(s)        :pSession:Pointer to Session Node                   */
/*  Output(s)       : None                                              */
/*  Returns         :                                                   */
/************************************************************************/
VOID
PPPoEDeleteSession (tPPPoESessionNode * pSession)
{
    tPPPoEBinding      *pBinding;
#ifdef DPIF
    ePPPoESession       ePPPoEType;
#endif
    if ((pBinding = PPPoEBindingGetNode (pSession->u2Port)) != NULL)
    {
        PPP_TRC1 (CONTROL_PLANE, "Deleting Session on eth%d", pSession->u2Port);
        PPP_TRC1 (CONTROL_PLANE, "Session Id :[ %d ]", pSession->u2SessionId);
        pBinding->NoOfActiveSessions--;
        gu4PPPoEActiveSessions--;
    }

#ifdef DPIF
    PPPoEDeleteSessionFromDp (pSession);
    ePPPoEType = PPPOE_DELETE_SESSION;
    PPPoESessionDp (ePPPoEType, pSession);
#endif
    pSession->pIf->LinkInfo.pPppoeSession = NULL;
    SLL_DELETE (&PPPoESessionList, (t_SLL_NODE *) pSession);

    if (PPPoEMode == PPPOE_SERVER)
        ReleasePPPInterface (pSession->pIf);
    else
    {
        PPPoEGiveEventToPPP (pSession, PPP_DOWN);
    }

    MEM_FREE (pSession);

}

/************************************************************************/
/*  Function Name   :PPPoEGiveEventToPPP                                */
/*  Description     :This Function is invoked By PPPoE to Give Event to */
/*                  :PPP                                                */
/*  Input(s)        :pNode:Pointer to the Node                          */
/*                  :Event:Represents the Event                         */
/*  Output(s)       : None                                              */
/*  Returns         :                                                   */
/************************************************************************/
VOID
PPPoEGiveEventToPPP (VOID *pNode, UINT1 Event)
{
    tPPPIf             *pIf;
    pIf = ((tPPPoESessionNode *) pNode)->pIf;
    PPPSendEventToPPPTask (pIf->LinkInfo.IfIndex, Event);
}

/************************************************************************/
/*  Function Name   :PPPoECPADSServiceName                              */
/*  Description     :                                                   */
/*  Input(s)        :                                                   */
/*  Output(s)       : None                                              */
/*  Returns         :                                                   */
/************************************************************************/
INT4
PPPoECPADSServiceName (tPPPoETag * pTag, UINT2 u2Port)
{
    PPP_UNUSED (pTag);
    PPP_UNUSED (u2Port);
    return OK;
}

/************************************************************************/
/*  Function Name   :PPPoEDeletePADOListNodes                           */
/*  Description     :This Function used to Delete PADO Nodes            */
/*  Input(s)        :pNode:Pointer to the Node                          */
/*  Output(s)       : None                                              */
/*  Returns         :                                                   */
/************************************************************************/
VOID
PPPoEDeletePADOListNodes (VOID *pNode)
{
    tPADOListNode      *pPADONode;
    pPADONode = pNode;
    MEM_FREE (pPADONode->pHdr);
    RELEASE_BUFFER (pPADONode->pRcvdBuf);
    MEM_FREE (pPADONode);

}

/************************************************************************/
/*  Function Name   :PPPoEActiveSessionPerHost                          */
/*  Description     :This Function is used to get no of active sessions */
/*                  : per host                                          */
/*  Input(s)        :pHdr:Pointer to Ethernet Header                    */
/*  Output(s)       : None                                              */
/*  Returns         :Returns Count                                      */
/************************************************************************/
UINT2
PPPoEActiveSessionPerHost (tPPPoEHeader * pHdr)
{
    UINT2               Count = 0;
    tPPPoESessionNode  *pSession;
    UINT1              *pSrcMac;
    pSrcMac = pHdr->SrcMACAddr;
    SLL_SCAN (&PPPoESessionList, pSession, tPPPoESessionNode *)
    {
        if (PPPOE_SAME_ADDR (pSession->DestMACAddr, pSrcMac))
            Count++;
    }
    return Count;
}

INT4
PPPoEHandleLinkDown (tPPPIf * pIf)
{
/*    tPPPoESessionNode  *pNode, pSessionNode = NULL;*/
    tClientDiscoveryNode *pNode = NULL;
    tPPPoEBinding      *pPPPoEIf;

    SLL_SCAN (&PPPoEClientDiscList, pNode, tClientDiscoveryNode *)
    {
        if (pNode->u2Port == pIf->LinkInfo.PhysIfIndex)
        {
            pPPPoEIf = PPPoEBindingGetNode (pIf->LinkInfo.PhysIfIndex);            
            if ((pNode->State == PADR_SENT) || (pNode->State == PADO_RCVD))
            {
                FREE_DISC_STORED_BUF (pNode);
                pNode->State = PADI_SENT;
                PPP_TRC (CONTROL_PLANE, "Link down.");
                PPP_TRC (CONTROL_PLANE, "Resending PADI");
                PPPoECSendPADI (pNode->u2Port, pNode, PPPOE_NORMAL);
                if( pPPPoEIf!= NULL)
                {
                	pPPPoEIf->NoOfPADITrans++;
                }
                return SUCCESS;
            }
        }
    }                            /* SLL_SCAN */

    /* Consider timed out so that PPPoE discovery is restarted */
    PPP_TRC (CONTROL_PLANE, "Link down. Sending timeout event to LCP");
    GSEMRun (&(pIf->LcpGSEM), RXJ_MINUS);

    return SUCCESS;
}
