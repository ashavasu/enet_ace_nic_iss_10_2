/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppoelow.c,v 1.11 2014/12/16 11:47:25 siva Exp $
 *
 * Description:This file contains low level routines for pppoe module.
 *
 *******************************************************************/
#ifndef __PPP_PPPOELOW_C__
#define __PPP_PPPOELOW_C__
#include "snmctdfs.h"
#include "snmccons.h"
#include "pppoecom.h"
#include "pppoelp.h"
#include "pppoelow.h"
#include "pppoecli.h"
/* LOW LEVEL Routines for Table : PPPoEAcCookieTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePPPoEAcCookieTable
 Input       :  The Indices
                PPPoEAcCookieIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePPPoEAcCookieTable (INT4 i4PPPoEAcCookieIndex)
{
    tPPPoEBinding      *pPPPoEIf;
    if (PPPoEMode == PPPOE_CLIENT)
        return SNMP_FAILURE;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEAcCookieIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Index Found");
        PPP_UNUSED (pPPPoEIf);
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPPPoEAcCookieTable
 Input       :  The Indices
                PPPoEAcCookieIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPPPoEAcCookieTable (INT4 *pi4PPPoEAcCookieIndex)
{
    tPPPoEBinding      *pPPPoEIf;
    UINT4               u4FirstIndex = MAX_INTEGER;
    INT4                Flag = 0;
    if (PPPoEMode == PPPOE_CLIENT)
        return SNMP_FAILURE;

    SLL_SCAN (&PPPoEBindingList, pPPPoEIf, tPPPoEBinding *)
    {
        Flag = 1;
        if (pPPPoEIf->u4Index < (UINT4) u4FirstIndex)
        {
            u4FirstIndex = pPPPoEIf->u4Index;
        }
    }
    if (!Flag)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return SNMP_FAILURE;
    }
    *pi4PPPoEAcCookieIndex = (INT4) u4FirstIndex;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetNextIndexPPPoEAcCookieTable
 Input       :  The Indices
                PPPoEAcCookieIndex
                nextPPPoEAcCookieIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPPPoEAcCookieTable (INT4 i4PPPoEAcCookieIndex,
                                   INT4 *pi4NextPPPoEAcCookieIndex)
{
    tPPPoEBinding      *pPPPoEIf;
    UINT4               u4NextIndex = MAX_INTEGER;
    INT4                Flag = 0;
    if (PPPoEMode == PPPOE_CLIENT)
        return SNMP_FAILURE;

    SLL_SCAN (&PPPoEBindingList, pPPPoEIf, tPPPoEBinding *)
    {
        if (pPPPoEIf->u4Index > (UINT4) i4PPPoEAcCookieIndex)
        {
            Flag = 1;
            if (pPPPoEIf->u4Index < (UINT4) u4NextIndex)
            {
                u4NextIndex = pPPPoEIf->u4Index;
            }
        }
    }
    if (!Flag)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return (SNMP_FAILURE);
    }
    *pi4NextPPPoEAcCookieIndex = (INT4) u4NextIndex;
    return (SNMP_SUCCESS);

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPPPoEAcCookieMethod
 Input       :  The Indices
                PPPoEAcCookieIndex

                The Object 
                retValPPPoEAcCookieMethod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEAcCookieMethod (INT4 i4PPPoEAcCookieIndex,
                           INT4 *pi4RetValPPPoEAcCookieMethod)
{

    tPPPoEBinding      *pPPPoEIf;
    if (PPPoEMode == PPPOE_CLIENT)
        return SNMP_FAILURE;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEAcCookieIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return (SNMP_FAILURE);
    }
    else
    {
        *pi4RetValPPPoEAcCookieMethod = pPPPoEIf->ACCookieMethod;

    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPPPoEConfigAcCookieKey
 Input       :  The Indices
                PPPoEAcCookieIndex

                The Object 
                retValPPPoEConfigAcCookieKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEConfigAcCookieKey (INT4 i4PPPoEAcCookieIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValPPPoEConfigAcCookieKey)
{
    tPPPoEBinding      *pPPPoEIf;
    if (PPPoEMode == PPPOE_CLIENT)
        return SNMP_FAILURE;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEAcCookieIndex)) == NULL)
    {
        PPP_TRC (MGMT, "set pPPoEBindingEnabled first");
        return (SNMP_FAILURE);
    }

    if (pPPPoEIf->Key != NULL)
    {
        STRCPY (pRetValPPPoEConfigAcCookieKey->pu1_OctetList, pPPPoEIf->Key);
        pRetValPPPoEConfigAcCookieKey->i4_Length =
            (INT4) STRLEN (pPPPoEIf->Key);
    }
    return (SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPPPoEAcCookieMethod
 Input       :  The Indices
                PPPoEAcCookieIndex

                The Object 
                setValPPPoEAcCookieMethod
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPPPoEAcCookieMethod (INT4 i4PPPoEAcCookieIndex,
                           INT4 i4SetValPPPoEAcCookieMethod)
{
    tPPPoEBinding      *pPPPoEIf;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEAcCookieIndex)) == NULL)
    {
        PPP_TRC (MGMT, "set pPPoEBindingEnabled first");
        return (SNMP_FAILURE);
    }
    pPPPoEIf->ACCookieMethod = (UINT1) i4SetValPPPoEAcCookieMethod;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPPPoEConfigAcCookieKey
 Input       :  The Indices
                PPPoEAcCookieIndex

                The Object 
                setValPPPoEConfigAcCookieKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPPPoEConfigAcCookieKey (INT4 i4PPPoEAcCookieIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pSetValPPPoEConfigAcCookieKey)
{
    tPPPoEBinding      *pPPPoEIf;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEAcCookieIndex)) == NULL)
    {
        PPP_TRC (MGMT, "set pPPoEBindingEnabled first");
        return (SNMP_FAILURE);
    }
    if (pPPPoEIf->Key != NULL)
    {
        MEM_FREE (pPPPoEIf->Key);
    }
    if ((pPPPoEIf->Key =
         MEM_CALLOC ((size_t) (pSetValPPPoEConfigAcCookieKey->i4_Length + 1), 1,
                     UINT1)) == NULL)
    {
        PPP_TRC (MGMT, "Memory Allocation for key failed");
        return (SNMP_FAILURE);
    }
    MEMCPY (pPPPoEIf->Key, pSetValPPPoEConfigAcCookieKey->pu1_OctetList,
            pSetValPPPoEConfigAcCookieKey->i4_Length);
    return (SNMP_SUCCESS);

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PPPoEAcCookieMethod
 Input       :  The Indices
                PPPoEAcCookieIndex

                The Object 
                testValPPPoEAcCookieMethod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PPPoEAcCookieMethod (UINT4 *pu4ErrorCode, INT4 i4PPPoEAcCookieIndex,
                              INT4 i4TestValPPPoEAcCookieMethod)
{
    PPP_UNUSED (i4PPPoEAcCookieIndex);
    if (PPPoEMode == PPPOE_CLIENT)
    {
        PPP_TRC (MGMT, "AC Cookie configuration is not Valid for PPPoE Client");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValPPPoEAcCookieMethod != PPPOE_NONE)
        && (i4TestValPPPoEAcCookieMethod != HMAC_MD5))
    {
        PPP_TRC (MGMT, "Wrong Value. Only <none> or <hmacMd5> are supported");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PPPoEConfigAcCookieKey
 Input       :  The Indices
                PPPoEAcCookieIndex

                The Object 
                testValPPPoEConfigAcCookieKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PPPoEConfigAcCookieKey (UINT4 *pu4ErrorCode,
                                 INT4 i4PPPoEAcCookieIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pTestValPPPoEConfigAcCookieKey)
{
    PPP_UNUSED (i4PPPoEAcCookieIndex);
    if (PPPoEMode == PPPOE_CLIENT)
    {
        PPP_TRC (MGMT, "AC Cookie configuration is not Valid for PPPoE Client");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((pTestValPPPoEConfigAcCookieKey->i4_Length <= 0)
        || (pTestValPPPoEConfigAcCookieKey->i4_Length > MAX_ACCOOKIE_KEY_LEN))
    {
        PPP_TRC (MGMT, "Wrong Value");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : PPPoEConfigServiceNameTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePPPoEConfigServiceNameTable
 Input       :  The Indices
                PPPoEConfigServiceName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePPPoEConfigServiceNameTable (tSNMP_OCTET_STRING_TYPE *
                                                     pPPPoEConfigServiceName)
{
    tPPPoEService      *pPPPoEService;

    if ((pPPPoEService =
         PPPoEGetServiceNode (pPPPoEConfigServiceName->pu1_OctetList,
                              (UINT1) (pPPPoEConfigServiceName->i4_Length))) ==
        NULL)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        PPP_UNUSED (pPPPoEService);
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFirstIndexPPPoEConfigServiceNameTable
 Input       :  The Indices
                PPPoEConfigServiceName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPPPoEConfigServiceNameTable (tSNMP_OCTET_STRING_TYPE *
                                             pPPPoEConfigServiceName)
{
    tPPPoEService      *pPPPoEService;
    UINT1              *FirstName;

    pPPPoEService = (tPPPoEService *) SLL_FIRST (&PPPoEServiceList);
    if (pPPPoEService == NULL)
        return SNMP_FAILURE;
    FirstName = pPPPoEService->ServiceName;

    SLL_SCAN (&PPPoEServiceList, pPPPoEService, tPPPoEService *)
    {
        if (STRCMP (pPPPoEService->ServiceName, FirstName) < 0)
        {
            FirstName = pPPPoEService->ServiceName;
        }
    }
    MEMCPY (pPPPoEConfigServiceName->pu1_OctetList, FirstName,
            STRLEN (FirstName));
    pPPPoEConfigServiceName->i4_Length = (INT4) STRLEN (FirstName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexPPPoEConfigServiceNameTable
 Input       :  The Indices
                PPPoEConfigServiceName
                nextPPPoEConfigServiceName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPPPoEConfigServiceNameTable (tSNMP_OCTET_STRING_TYPE *
                                            pPPPoEConfigServiceName,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pNextPPPoEConfigServiceName)
{
    tPPPoEService      *pPPPoEService;
    INT4                Flag = 0;
    UINT1              *NextName;
    pPPPoEService = (tPPPoEService *) SLL_FIRST (&PPPoEServiceList);
    if (pPPPoEService == NULL)
        return SNMP_FAILURE;
    NextName = pPPPoEService->ServiceName;

    SLL_SCAN (&PPPoEServiceList, pPPPoEService, tPPPoEService *)
    {
        if (MEMCMP
            (pPPPoEService->ServiceName, pPPPoEConfigServiceName->pu1_OctetList,
             pPPPoEConfigServiceName->i4_Length) > 0)
        {
            Flag = 1;
            if (STRCMP (pPPPoEService->ServiceName, NextName) < 0)
            {
                NextName = pPPPoEService->ServiceName;
            }
        }
    }
    if (!Flag)
        return SNMP_FAILURE;

    MEMCPY (pNextPPPoEConfigServiceName->pu1_OctetList, NextName,
            STRLEN (NextName));
    pNextPPPoEConfigServiceName->i4_Length = (INT4) STRLEN (NextName);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPPPoEConfigServiceRowStatus
 Input       :  The Indices
                PPPoEConfigServiceName

                The Object 
                retValPPPoEConfigServiceRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEConfigServiceRowStatus (tSNMP_OCTET_STRING_TYPE *
                                   pPPPoEConfigServiceName,
                                   INT4 *pi4RetValPPPoEConfigServiceRowStatus)
{
    tPPPoEService      *pPPPoEService;

    if ((pPPPoEService =
         PPPoEGetServiceNode (pPPPoEConfigServiceName->pu1_OctetList,
                              (UINT1) (pPPPoEConfigServiceName->i4_Length))) ==
        NULL)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        PPP_UNUSED (pPPPoEService);
        return (SNMP_FAILURE);
    }
    else
    {
        *pi4RetValPPPoEConfigServiceRowStatus = ACTIVE;

    }
    return (SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPPPoEConfigServiceName
 Input       :  The Indices
                PPPoEConfigServiceName

                The Object 
                setValPPPoEConfigServiceName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPPPoEConfigServiceName (tSNMP_OCTET_STRING_TYPE * pPPPoEConfigServiceName,
                              tSNMP_OCTET_STRING_TYPE *
                              pSetValPPPoEConfigServiceName)
{
    PPP_UNUSED (pPPPoEConfigServiceName);
    PPP_UNUSED (pSetValPPPoEConfigServiceName);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetPPPoEConfigServiceRowStatus
 Input       :  The Indices
                PPPoEConfigServiceName

                The Object 
                setValPPPoEConfigServiceRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPPPoEConfigServiceRowStatus (tSNMP_OCTET_STRING_TYPE *
                                   pPPPoEConfigServiceName,
                                   INT4 i4SetValPPPoEConfigServiceRowStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tPPPoEService      *pPPPoEService;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    pPPPoEService = PPPoEGetServiceNode (pPPPoEConfigServiceName->pu1_OctetList,
                                         (UINT1) (pPPPoEConfigServiceName->
                                                  i4_Length));

    if ((i4SetValPPPoEConfigServiceRowStatus == CREATE_AND_GO) ||
        (i4SetValPPPoEConfigServiceRowStatus == CREATE_AND_WAIT))
    {
        if (pPPPoEService != NULL)
	{
            PPP_TRC (MGMT, "Service name is already configured \n");
	    CLI_SET_ERR (CLI_PPP_SERVICE_NAME_CONFIGURED);
            return SNMP_FAILURE;
	}
        if ((pPPPoEService = MEM_MALLOC (sizeof (tPPPoEService),
                                         tPPPoEService)) == NULL)
        {
            PPP_TRC (MGMT, "MemAlloc Failed \n");
	    CLI_SET_ERR (CLI_PPP_SERVICE_NAME_MEMALLOC);
            return (SNMP_FAILURE);
        }
        if ((pPPPoEService->ServiceName =
             MEM_CALLOC ((size_t) (pPPPoEConfigServiceName->i4_Length + 1), 1,
                         UINT1)) == NULL)
        {
            PPP_TRC (MGMT, "MemAlloc for Service Name Failed \n");
	    CLI_SET_ERR (CLI_PPP_SERVICE_NAME_MEMALLOC);
            MEM_FREE (pPPPoEService);
            return (SNMP_FAILURE);
        }
        MEMCPY (pPPPoEService->ServiceName,
                pPPPoEConfigServiceName->pu1_OctetList,
                pPPPoEConfigServiceName->i4_Length);
        SLL_ADD (&PPPoEServiceList, (t_SLL_NODE *) pPPPoEService);
        RM_GET_SEQ_NUM (&u4SeqNum);
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PPPoEConfigServiceRowStatus, u4SeqNum,
                          FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i",
		      pPPPoEConfigServiceName,
                      i4SetValPPPoEConfigServiceRowStatus));

        return (SNMP_SUCCESS);
    }

    if (i4SetValPPPoEConfigServiceRowStatus == DESTROY)
    {
        if (pPPPoEService == NULL)
        {
            PPP_TRC (MGMT, "Service name not configured! \n");
	    CLI_SET_ERR (CLI_PPP_SERVICE_NAME_NOT_CONFIGURED);
            return SNMP_FAILURE;
        }
        MEM_FREE (pPPPoEService->ServiceName);
        SLL_DELETE (&PPPoEServiceList, (t_SLL_NODE *) pPPPoEService);
        MEM_FREE (pPPPoEService);
        RM_GET_SEQ_NUM (&u4SeqNum);
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PPPoEConfigServiceRowStatus, u4SeqNum,
                          FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i",
		      pPPPoEConfigServiceName,
                      i4SetValPPPoEConfigServiceRowStatus));
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PPPoEConfigServiceName
 Input       :  The Indices
                PPPoEConfigServiceName

                The Object 
                testValPPPoEConfigServiceName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PPPoEConfigServiceName (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pPPPoEConfigServiceName,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pTestValPPPoEConfigServiceName)
{
    if (pPPPoEConfigServiceName->i4_Length !=
        pTestValPPPoEConfigServiceName->i4_Length)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    if (!MEMCMP
        (pPPPoEConfigServiceName->pu1_OctetList,
         pTestValPPPoEConfigServiceName->pu1_OctetList,
         pPPPoEConfigServiceName->i4_Length))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PPPoEConfigServiceRowStatus
 Input       :  The Indices
                PPPoEConfigServiceName

                The Object 
                testValPPPoEConfigServiceRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PPPoEConfigServiceRowStatus (UINT4 *pu4ErrorCode,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pPPPoEConfigServiceName,
                                      INT4 i4TestValPPPoEConfigServiceRowStatus)
{

    if ((i4TestValPPPoEConfigServiceRowStatus != CREATE_AND_GO)
        && (i4TestValPPPoEConfigServiceRowStatus != DESTROY)
        && (i4TestValPPPoEConfigServiceRowStatus != CREATE_AND_WAIT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (i4TestValPPPoEConfigServiceRowStatus == DESTROY)
    {
        if (PPPoEGetServiceNode (pPPPoEConfigServiceName->pu1_OctetList,
                                 (UINT1) (pPPPoEConfigServiceName->
                                          i4_Length)) == NULL)
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    else
    {
        if (pPPPoEConfigServiceName->i4_Length > PPPOE_MAX_SERVICE_NAME_LEN)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2PPPoEConfigServiceNameTable
 Input       :  The Indices
                PPPoEConfigServiceName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PPPoEConfigServiceNameTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : PPPoEBindingsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePPPoEBindingsTable
 Input       :  The Indices
                PPPoEBindingsIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePPPoEBindingsTable (INT4 i4PPPoEBindingsIndex)
{
    tPPPoEBinding      *pPPPoEIf;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEBindingsIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        PPP_UNUSED (pPPPoEIf);
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPPPoEBindingsTable
 Input       :  The Indices
                PPPoEBindingsIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPPPoEBindingsTable (INT4 *pi4PPPoEBindingsIndex)
{

    tPPPoEBinding      *pPPPoEIf;
    INT4                i4FirstIndex = (INT4) MAX_INTEGER;
    INT4                Flag;
    Flag = 0;
    SLL_SCAN (&PPPoEBindingList, pPPPoEIf, tPPPoEBinding *)
    {
        Flag = 1;
        if (pPPPoEIf->u4Index < (UINT4) i4FirstIndex)
        {
            i4FirstIndex = (INT4) pPPPoEIf->u4Index;
        }
    }
    if (!Flag)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return (SNMP_FAILURE);
    }
    *pi4PPPoEBindingsIndex = i4FirstIndex;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPPPoEBindingsTable
 Input       :  The Indices
                PPPoEBindingsIndex
                nextPPPoEBindingsIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPPPoEBindingsTable (INT4 i4PPPoEBindingsIndex,
                                   INT4 *pi4NextPPPoEBindingsIndex)
{
    tPPPoEBinding      *pPPPoEIf;
    INT4                i4NextIndex = (INT4) MAX_INTEGER;
    INT4                Flag;

    Flag = 0;
    SLL_SCAN (&PPPoEBindingList, pPPPoEIf, tPPPoEBinding *)
    {
        if (pPPPoEIf->u4Index > (UINT4) i4PPPoEBindingsIndex)
        {
            Flag = 1;
            if (pPPPoEIf->u4Index < (UINT4) i4NextIndex)
            {
                i4NextIndex = (INT4) pPPPoEIf->u4Index;
            }
        }
    }
    if (!Flag)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return (SNMP_FAILURE);
    }
    *pi4NextPPPoEBindingsIndex = i4NextIndex;
    return (SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPPPoEBindingsEnabled
 Input       :  The Indices
                PPPoEBindingsIndex

                The Object 
                retValPPPoEBindingsEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEBindingsEnabled (INT4 i4PPPoEBindingsIndex,
                            INT4 *pi4RetValPPPoEBindingsEnabled)
{
    tPPPoEBinding      *pPPPoEIf;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEBindingsIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return (SNMP_FAILURE);
    }
    else
    {
        *pi4RetValPPPoEBindingsEnabled = pPPPoEIf->Enable;
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPPPoEBindingsMaxSessions
 Input       :  The Indices
                PPPoEBindingsIndex

                The Object 
                retValPPPoEBindingsMaxSessions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEBindingsMaxSessions (INT4 i4PPPoEBindingsIndex,
                                INT4 *pi4RetValPPPoEBindingsMaxSessions)
{
    tPPPoEBinding      *pPPPoEIf;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEBindingsIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return (SNMP_FAILURE);
    }
    else
    {
        *pi4RetValPPPoEBindingsMaxSessions = pPPPoEIf->MaxSessions;
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPPPoEBindingsNumberActiveSessions
 Input       :  The Indices
                PPPoEBindingsIndex

                The Object 
                retValPPPoEBindingsNumberActiveSessions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEBindingsNumberActiveSessions (INT4 i4PPPoEBindingsIndex,
                                         UINT4
                                         *pu4RetValPPPoEBindingsNumberActiveSessions)
{
    tPPPoEBinding      *pPPPoEIf;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEBindingsIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return (SNMP_FAILURE);
    }
    else
    {
        *pu4RetValPPPoEBindingsNumberActiveSessions =
            pPPPoEIf->NoOfActiveSessions;
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPPPoEBindingsTotalNumberSessions
 Input       :  The Indices
                PPPoEBindingsIndex

                The Object 
                retValPPPoEBindingsTotalNumberSessions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEBindingsTotalNumberSessions (INT4 i4PPPoEBindingsIndex,
                                        UINT4
                                        *pu4RetValPPPoEBindingsTotalNumberSessions)
{
    tPPPoEBinding      *pPPPoEIf;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEBindingsIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return (SNMP_FAILURE);
    }
    else
    {
        *pu4RetValPPPoEBindingsTotalNumberSessions =
            pPPPoEIf->u4TotalNofSessions;
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPPPoEBindingsNumberPADIRejected
 Input       :  The Indices
                PPPoEBindingsIndex

                The Object 
                retValPPPoEBindingsNumberPADIRejected
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEBindingsNumberPADIRejected (INT4 i4PPPoEBindingsIndex,
                                       UINT4
                                       *pu4RetValPPPoEBindingsNumberPADIRejected)
{
    tPPPoEBinding      *pPPPoEIf;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEBindingsIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return (SNMP_FAILURE);
    }
    else
    {
        *pu4RetValPPPoEBindingsNumberPADIRejected = pPPPoEIf->NoOfPADIRej;
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPPPoEBindingsNumberPADIReceived
 Input       :  The Indices
                PPPoEBindingsIndex

                The Object 
                retValPPPoEBindingsNumberPADIReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEBindingsNumberPADIReceived (INT4 i4PPPoEBindingsIndex,
                                       UINT4
                                       *pu4RetValPPPoEBindingsNumberPADIReceived)
{
    tPPPoEBinding      *pPPPoEIf;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEBindingsIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return (SNMP_FAILURE);
    }
    else
    {
        *pu4RetValPPPoEBindingsNumberPADIReceived = pPPPoEIf->NoOfPADIRecv;
    }
    return (SNMP_SUCCESS);

}
/****************************************************************************
Function    :  nmhGetPPPoEBindingsNumberPADIReceived
Input       :  The Indices
PPPoEBindingsIndex The ObjectetValPPPoEBindingsNumberPADIReceived
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEBindingsNumberPADITransmitted (INT4 i4PPPoEBindingsIndex,
                                         UINT4 *pu4RetValPPPoEBindingsNumberPADITransmitted)
{
    tPPPoEBinding      *pPPPoEIf;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEBindingsIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return (SNMP_FAILURE);
    }
    else
    {
        *pu4RetValPPPoEBindingsNumberPADITransmitted = pPPPoEIf->NoOfPADITrans;
    }
    return (SNMP_SUCCESS);

}



/****************************************************************************
Function    :  nmhGetPPPoEBindingsNumberPADRRejected
Input       :  The Indices
PPPoEBindingsIndex
The Object
retValPPPoEBindingsNumberPADRRejected
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEBindingsNumberPADRTransmitted (INT4 i4PPPoEBindingsIndex,
                                          UINT4
                                          *pu4RetValPPPoEBindingsNumberPADRTransmitted)
{
    tPPPoEBinding      *pPPPoEIf;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEBindingsIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return (SNMP_FAILURE);
    }
    else
    {
        *pu4RetValPPPoEBindingsNumberPADRTransmitted = pPPPoEIf->NoOfPADRTrans;
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPPPoEBindingsNumberPADRRejected
 Input       :  The Indices
                PPPoEBindingsIndex

                The Object 
                retValPPPoEBindingsNumberPADRRejected
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEBindingsNumberPADRRejected (INT4 i4PPPoEBindingsIndex,
                                       UINT4
                                       *pu4RetValPPPoEBindingsNumberPADRRejected)
{
    tPPPoEBinding      *pPPPoEIf;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEBindingsIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return (SNMP_FAILURE);
    }
    else
    {
        *pu4RetValPPPoEBindingsNumberPADRRejected = pPPPoEIf->NoOfPADRRej;
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPPPoEBindingsNumberPADRReceived
 Input       :  The Indices
                PPPoEBindingsIndex

                The Object 
                retValPPPoEBindingsNumberPADRReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEBindingsNumberPADRReceived (INT4 i4PPPoEBindingsIndex,
                                       UINT4
                                       *pu4RetValPPPoEBindingsNumberPADRReceived)
{
    tPPPoEBinding      *pPPPoEIf;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEBindingsIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return (SNMP_FAILURE);
    }
    else
    {
        *pu4RetValPPPoEBindingsNumberPADRReceived = pPPPoEIf->NoOfPADRRecv;
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPPPoEBindingsNumberPADOTransmitted
 Input       :  The Indices
                PPPoEBindingsIndex

                The Object 
                retValPPPoEBindingsNumberPADOTransmitted
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEBindingsNumberPADOTransmitted (INT4 i4PPPoEBindingsIndex,
                                          UINT4
                                          *pu4RetValPPPoEBindingsNumberPADOTransmitted)
{
    tPPPoEBinding      *pPPPoEIf;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEBindingsIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return (SNMP_FAILURE);
    }
    else
    {
        *pu4RetValPPPoEBindingsNumberPADOTransmitted = pPPPoEIf->NoOfPADOTrans;
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
Function    :  nmhGetPPPoEBindingsNumberPADOTransmitted
Input       :  The Indices
PPPoEBindingsIndex The Object retValPPPoEBindingsNumberPADOTransmitted
Output      :  The Get Low Lev Routine Take the Indices &
ore the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEBindingsNumberPADORejected (INT4 i4PPPoEBindingsIndex,
                                          UINT4
                                          *pu4RetValPPPoEBindingsNumberPADORejected)
{
    tPPPoEBinding      *pPPPoEIf;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEBindingsIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return (SNMP_FAILURE);
    }
    else
    {
        *pu4RetValPPPoEBindingsNumberPADORejected = pPPPoEIf->NoOfPADORej;
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 * Function    :  nmhGetPPPoEBindingsNumberPADOTransmitted
 * Input       :  The Indices
 * PPPoEBindingsIndex The Object retValPPPoEBindingsNumberPADOTransmitted
 * Output      :  The Get Low Lev Routine Take the Indices &
 * ore the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ****************************************************************************/
INT1
nmhGetPPPoEBindingsNumberPADOReceived (INT4 i4PPPoEBindingsIndex,
                                          UINT4
                                          *pu4RetValPPPoEBindingsNumberPADOReceived)
{
    tPPPoEBinding      *pPPPoEIf;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEBindingsIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return (SNMP_FAILURE);
    }
    else
    {
        *pu4RetValPPPoEBindingsNumberPADOReceived = pPPPoEIf->NoOfPADORecv;
    }
    return (SNMP_SUCCESS);

}


/****************************************************************************
 Function    :  nmhGetPPPoEBindingsNumberPADSTransmitted
 Input       :  The Indices
                PPPoEBindingsIndex

                The Object 
                retValPPPoEBindingsNumberPADSTransmitted
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEBindingsNumberPADSTransmitted (INT4 i4PPPoEBindingsIndex,
                                          UINT4
                                          *pu4RetValPPPoEBindingsNumberPADSTransmitted)
{
    tPPPoEBinding      *pPPPoEIf;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEBindingsIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return (SNMP_FAILURE);
    }
    else
    {
        *pu4RetValPPPoEBindingsNumberPADSTransmitted = pPPPoEIf->NoOfPADSTrans;
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
Function    :  nmhGetPPPoEBindingsNumberPADSTransmitted
Input       :  The Indices
PPPoEBindingsIndex
The Object
retValPPPoEBindingsNumberPADSTransmitted
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
***************************************************************************/
INT1
nmhGetPPPoEBindingsNumberPADSReceived (INT4 i4PPPoEBindingsIndex,
                                          UINT4
                                          *pu4RetValPPPoEBindingsNumberPADSReceived)
{
    tPPPoEBinding      *pPPPoEIf;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEBindingsIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return (SNMP_FAILURE);
    }
    else
    {
        *pu4RetValPPPoEBindingsNumberPADSReceived = pPPPoEIf->NoOfPADSRecv;
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 * Function    :  nmhGetPPPoEBindingsNumberPADSTransmitted
 * Input       :  The Indices
 * PPPoEBindingsIndex
 * The Object
 * retValPPPoEBindingsNumberPADSTransmitted
 * Output      :  The Get Low Lev Routine Take the Indices &
 * store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ***************************************************************************/
INT1
nmhGetPPPoEBindingsNumberPADSRejected (INT4 i4PPPoEBindingsIndex,
                                          UINT4
                                          *pu4RetValPPPoEBindingsNumberPADSRejected)
{
    tPPPoEBinding      *pPPPoEIf;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEBindingsIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return (SNMP_FAILURE);
    }
    else
    {
        *pu4RetValPPPoEBindingsNumberPADSRejected = pPPPoEIf->NoOfPADSRej;
    }
    return (SNMP_SUCCESS);
}




/****************************************************************************
 Function    :  nmhGetPPPoEBindingsNumberPADTReceived
 Input       :  The Indices
                PPPoEBindingsIndex

                The Object 
                retValPPPoEBindingsNumberPADTReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEBindingsNumberPADTReceived (INT4 i4PPPoEBindingsIndex,
                                       UINT4
                                       *pu4RetValPPPoEBindingsNumberPADTReceived)
{
    tPPPoEBinding      *pPPPoEIf;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEBindingsIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return (SNMP_FAILURE);
    }
    else
    {
        *pu4RetValPPPoEBindingsNumberPADTReceived = pPPPoEIf->NoOfPADTRecv;
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPPPoEBindingsNumberPADTTransmitted
 Input       :  The Indices
                PPPoEBindingsIndex

                The Object 
                retValPPPoEBindingsNumberPADTTransmitted
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEBindingsNumberPADTTransmitted (INT4 i4PPPoEBindingsIndex,
                                          UINT4
                                          *pu4RetValPPPoEBindingsNumberPADTTransmitted)
{
    tPPPoEBinding      *pPPPoEIf;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEBindingsIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return (SNMP_FAILURE);
    }
    else
    {
        *pu4RetValPPPoEBindingsNumberPADTTransmitted = pPPPoEIf->NoOfPADTTrans;
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
Function    :  nmhGetPPPoEBindingsNumberPADSTransmitted
Input       :  The Indices
PPoEBindingsIndex
The Object
retValPPPoEBindingsNumberPADSTransmitted
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
***************************************************************************/
INT1
nmhGetPPPoEBindingsNumberPADTRejected (INT4 i4PPPoEBindingsIndex,
                                          UINT4
                                          *pu4RetValPPPoEBindingsNumberPADTRejected)
{
    tPPPoEBinding      *pPPPoEIf;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEBindingsIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return (SNMP_FAILURE);
    }
    else
    {
        *pu4RetValPPPoEBindingsNumberPADTRejected = pPPPoEIf->NoOfPADTRej;
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPPPoEBindingsNumberServiceNameErrors
 Input       :  The Indices
                PPPoEBindingsIndex

                The Object 
                retValPPPoEBindingsNumberServiceNameErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEBindingsNumberServiceNameErrors (INT4 i4PPPoEBindingsIndex,
                                            UINT4
                                            *pu4RetValPPPoEBindingsNumberServiceNameErrors)
{
    tPPPoEBinding      *pPPPoEIf;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEBindingsIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return (SNMP_FAILURE);
    }
    else
    {
        *pu4RetValPPPoEBindingsNumberServiceNameErrors =
            pPPPoEIf->NoOfSerErrors;
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPPPoEBindingsNumberACSystemErrors
 Input       :  The Indices
                PPPoEBindingsIndex

                The Object 
                retValPPPoEBindingsNumberACSystemErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEBindingsNumberACSystemErrors (INT4 i4PPPoEBindingsIndex,
                                         UINT4
                                         *pu4RetValPPPoEBindingsNumberACSystemErrors)
{
    tPPPoEBinding      *pPPPoEIf;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEBindingsIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return (SNMP_FAILURE);
    }
    else
    {
        *pu4RetValPPPoEBindingsNumberACSystemErrors = pPPPoEIf->NoOfAcSysErrors;
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPPPoEBindingsNumberGenericErrorsReceived
 Input       :  The Indices
                PPPoEBindingsIndex

                The Object 
                retValPPPoEBindingsNumberGenericErrorsReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEBindingsNumberGenericErrorsReceived (INT4 i4PPPoEBindingsIndex,
                                                UINT4
                                                *pu4RetValPPPoEBindingsNumberGenericErrorsReceived)
{
    tPPPoEBinding      *pPPPoEIf;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEBindingsIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return (SNMP_FAILURE);
    }
    else
    {
        *pu4RetValPPPoEBindingsNumberGenericErrorsReceived =
            pPPPoEIf->NoOfGenErrorsRecv;
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPPPoEBindingsNumberGenericErrorsTransmitted
 Input       :  The Indices
                PPPoEBindingsIndex

                The Object 
                retValPPPoEBindingsNumberGenericErrorsTransmitted
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEBindingsNumberGenericErrorsTransmitted (INT4 i4PPPoEBindingsIndex,
                                                   UINT4
                                                   *pu4RetValPPPoEBindingsNumberGenericErrorsTransmitted)
{
    tPPPoEBinding      *pPPPoEIf;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEBindingsIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return (SNMP_FAILURE);
    }
    else
    {
        *pu4RetValPPPoEBindingsNumberGenericErrorsTransmitted =
            pPPPoEIf->NoOfGenErrorsTrans;
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPPPoEBindingsNumberMalformedPackets
 Input       :  The Indices
                PPPoEBindingsIndex

                The Object 
                retValPPPoEBindingsNumberMalformedPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEBindingsNumberMalformedPackets (INT4 i4PPPoEBindingsIndex,
                                           UINT4
                                           *pu4RetValPPPoEBindingsNumberMalformedPackets)
{
    tPPPoEBinding      *pPPPoEIf;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEBindingsIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return (SNMP_FAILURE);
    }
    else
    {
        *pu4RetValPPPoEBindingsNumberMalformedPackets =
            pPPPoEIf->NoOfMalFormPkts;
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPPPoEBindingsNumberPADITimeouts
 Input       :  The Indices
                PPPoEBindingsIndex

                The Object 
                retValPPPoEBindingsNumberPADITimeouts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEBindingsNumberPADITimeouts (INT4 i4PPPoEBindingsIndex,
                                       UINT4
                                       *pu4RetValPPPoEBindingsNumberPADITimeouts)
{
    tPPPoEBinding      *pPPPoEIf;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEBindingsIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return (SNMP_FAILURE);
    }
    else
    {
        *pu4RetValPPPoEBindingsNumberPADITimeouts = pPPPoEIf->NoOfPADITimeOuts;
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPPPoEBindingsNumberMultiplePADOReceived
 Input       :  The Indices
                PPPoEBindingsIndex

                The Object 
                retValPPPoEBindingsNumberMultiplePADOReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEBindingsNumberMultiplePADOReceived (INT4 i4PPPoEBindingsIndex,
                                               UINT4
                                               *pu4RetValPPPoEBindingsNumberMultiplePADOReceived)
{
    tPPPoEBinding      *pPPPoEIf;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEBindingsIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return (SNMP_FAILURE);
    }
    else
    {
        *pu4RetValPPPoEBindingsNumberMultiplePADOReceived =
            pPPPoEIf->NoOfMultPADORecv;
    }
    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPPPoEBindingsEnabled
 Input       :  The Indices
                PPPoEBindingsIndex

                The Object 
                setValPPPoEBindingsEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPPPoEBindingsEnabled (INT4 i4PPPoEBindingsIndex,
                            INT4 i4SetValPPPoEBindingsEnabled)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tPPPoEBinding      *pNode;
    
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    pNode = PPPoEBindingGetNode ((UINT4) i4PPPoEBindingsIndex);

    if (i4SetValPPPoEBindingsEnabled == PPPOE_DISABLED)
    {
        if (pNode == NULL)
        {
            return SNMP_FAILURE;
        }
        else
        {
            if (pNode->Key != NULL)
                MEM_FREE (pNode->Key);
            PPPoEInterfaceDown ((UINT2) (pNode->u4Index));
            SLL_DELETE (&PPPoEBindingList, (t_SLL_NODE *) pNode);
            MEM_FREE (pNode);
        }
    }
    else
    {
        if (pNode != NULL)
        {
            return SNMP_SUCCESS;
        }
        if ((pNode = MEM_CALLOC (1, sizeof (tPPPoEBinding), tPPPoEBinding)) ==
            NULL)
        {
            PPP_TRC (MGMT, "MemAlloc For pNode Failed\n");
            return SNMP_FAILURE;
        }
        pNode->u4Index = (UINT4) i4PPPoEBindingsIndex;
        pNode->Enable = PPPOE_ENABLED;
        pNode->ACCookieMethod = PPPOE_NONE;
        pNode->MaxSessions = PPPOE_DEF_MAX_SESSION;
        SLL_ADD (&PPPoEBindingList, (t_SLL_NODE *) pNode);

    }
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PPPoEBindingsEnabled, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                  i4PPPoEBindingsIndex,
                  i4SetValPPPoEBindingsEnabled));


    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPPPoEBindingsMaxSessions
 Input       :  The Indices
                PPPoEBindingsIndex

                The Object 
                setValPPPoEBindingsMaxSessions
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPPPoEBindingsMaxSessions (INT4 i4PPPoEBindingsIndex,
                                INT4 i4SetValPPPoEBindingsMaxSessions)
{
    tPPPoEBinding      *pPPPoEIf;

    if ((pPPPoEIf = PPPoEBindingGetNode ((UINT4) i4PPPoEBindingsIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return (SNMP_FAILURE);
    }
    else
    {
        pPPPoEIf->MaxSessions = (UINT2) i4SetValPPPoEBindingsMaxSessions;
    }
    return (SNMP_SUCCESS);

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PPPoEBindingsEnabled
 Input       :  The Indices
                PPPoEBindingsIndex

                The Object 
                testValPPPoEBindingsEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PPPoEBindingsEnabled (UINT4 *pu4ErrorCode, INT4 i4PPPoEBindingsIndex,
                               INT4 i4TestValPPPoEBindingsEnabled)
{
    if (!PPPOE_IF_EXISTS (i4PPPoEBindingsIndex))
    {
        PPP_TRC (MGMT, "Eth port does not exist");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValPPPoEBindingsEnabled != PPPOE_ENABLED)
        && (i4TestValPPPoEBindingsEnabled != PPPOE_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return (SNMP_SUCCESS);

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2PPPoEBindingsTable
 Input       :  The Indices
                PPPoEBindingsIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PPPoEBindingsTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PPPoEBindingsMaxSessions
 Input       :  The Indices
                PPPoEBindingsIndex

                The Object 
                testValPPPoEBindingsMaxSessions
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PPPoEBindingsMaxSessions (UINT4 *pu4ErrorCode,
                                   INT4 i4PPPoEBindingsIndex,
                                   INT4 i4TestValPPPoEBindingsMaxSessions)
{

    if (PPPoEBindingGetNode ((UINT4) i4PPPoEBindingsIndex) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (SNMP_FAILURE);
    }
    if (i4TestValPPPoEBindingsMaxSessions < 0)
    {
        PPP_TRC (MGMT, "Wrong Value");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPPPoEMode
 Input       :  The Indices

                The Object 
                retValPPPoEMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEMode (INT4 *pi4RetValPPPoEMode)
{
    *pi4RetValPPPoEMode = PPPoEMode;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPPPoEConfigMaxTotalSessions
 Input       :  The Indices

                The Object 
                retValPPPoEConfigMaxTotalSessions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEConfigMaxTotalSessions (INT4 *pi4RetValPPPoEConfigMaxTotalSessions)
{
    *pi4RetValPPPoEConfigMaxTotalSessions = (INT4) gu4PPPoEMaxSession;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPPPoEConfigMaxSessionsPerHost
 Input       :  The Indices

                The Object 
                retValPPPoEConfigMaxSessionsPerHost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEConfigMaxSessionsPerHost (INT4
                                     *pi4RetValPPPoEConfigMaxSessionsPerHost)
{
    *pi4RetValPPPoEConfigMaxSessionsPerHost = (INT4) gu4PPPoEMaxSessionPerHost;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPPPoEConfigPADITxInterval
 Input       :  The Indices

                The Object 
                retValPPPoEConfigPADITxInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEConfigPADITxInterval (INT4 *pi4RetValPPPoEConfigPADITxInterval)
{
    *pi4RetValPPPoEConfigPADITxInterval = gu1PPPoEReTxInterval;
    return (SNMP_SUCCESS);

}

#ifdef PPP_STACK_WANTED
/****************************************************************************
 Function    :  nmhGetPPPoEConfigPADIMaxNumberOfRetries
 Input       :  The Indices

                The Object 
                retValPPPoEConfigPADIMaxNumberOfRetries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEConfigPADIMaxNumberOfRetries (INT4
                                         *pi4RetValPPPoEConfigPADIMaxNumberOfRetries)
{
    *pi4RetValPPPoEConfigPADIMaxNumberOfRetries = gu1PPPoEMaxNoOfRetries;
    return (SNMP_SUCCESS);

}
#endif /* PPP_STACK_WANTED */

/****************************************************************************
 Function    :  nmhGetPPPoEConfigPADRMaxNumberOfRetries
 Input       :  The Indices

                The Object 
                retValPPPoEConfigPADRMaxNumberOfRetries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEConfigPADRMaxNumberOfRetries (INT4
                                         *pi4RetValPPPoEConfigPADRMaxNumberOfRetries)
{
    *pi4RetValPPPoEConfigPADRMaxNumberOfRetries = gu1PPPoEMaxNoOfRetries;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPPPoEConfigPADRWaitTime
 Input       :  The Indices

                The Object 
                retValPPPoEConfigPADRWaitTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEConfigPADRWaitTime (INT4 *pi4RetValPPPoEConfigPADRWaitTime)
{
    *pi4RetValPPPoEConfigPADRWaitTime = gu1PPPoEReTxInterval;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPPPoEHostUniqueEnabled
 Input       :  The Indices

                The Object 
                retValPPPoEHostUniqueEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEHostUniqueEnabled (INT4 *pi4RetValPPPoEHostUniqueEnabled)
{
    *pi4RetValPPPoEHostUniqueEnabled = gHostUniqueFlag;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPPPoEACName
 Input       :  The Indices

                The Object 
                retValPPPoEACName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEACName (tSNMP_OCTET_STRING_TYPE * pRetValPPPoEACName)
{
    pRetValPPPoEACName->i4_Length = (INT4) STRLEN (pPPPoEACName);
    MEMCPY (pRetValPPPoEACName->pu1_OctetList, pPPPoEACName,
            pRetValPPPoEACName->i4_Length);
    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPPPoEMode
 Input       :  The Indices

                The Object 
                setValPPPoEMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPPPoEMode (INT4 i4SetValPPPoEMode)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    PPPoEMode = (UINT1) i4SetValPPPoEMode;

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PPPoEModeOid, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 0, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                  i4SetValPPPoEMode));


    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetPPPoEConfigMaxTotalSessions
 Input       :  The Indices

                The Object 
                setValPPPoEConfigMaxTotalSessions
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPPPoEConfigMaxTotalSessions (INT4 i4SetValPPPoEConfigMaxTotalSessions)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    gu4PPPoEMaxSession = (UINT4) i4SetValPPPoEConfigMaxTotalSessions;

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PPPoEConfigMaxTotalSessions, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 0, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                  i4SetValPPPoEConfigMaxTotalSessions));

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPPPoEConfigMaxSessionsPerHost
 Input       :  The Indices

                The Object 
                setValPPPoEConfigMaxSessionsPerHost
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPPPoEConfigMaxSessionsPerHost (INT4 i4SetValPPPoEConfigMaxSessionsPerHost)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    gu4PPPoEMaxSessionPerHost = (UINT4) i4SetValPPPoEConfigMaxSessionsPerHost;

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PPPoEConfigMaxSessionsPerHost, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 0, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                  i4SetValPPPoEConfigMaxSessionsPerHost));

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPPPoEConfigPADITxInterval
 Input       :  The Indices

                The Object 
                setValPPPoEConfigPADITxInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPPPoEConfigPADITxInterval (INT4 i4SetValPPPoEConfigPADITxInterval)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    gu1PPPoEReTxInterval = (UINT1) i4SetValPPPoEConfigPADITxInterval;

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PPPoEConfigPADITxInterval, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 0, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                  i4SetValPPPoEConfigPADITxInterval));

    return (SNMP_SUCCESS);

}

#ifdef PPP_STACK_WANTED
/****************************************************************************
 Function    :  nmhSetPPPoEConfigPADIMaxNumberOfRetries
 Input       :  The Indices

                The Object 
                setValPPPoEConfigPADIMaxNumberOfRetries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPPPoEConfigPADIMaxNumberOfRetries (INT4
                                         i4SetValPPPoEConfigPADIMaxNumberOfRetries)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    gu1PPPoEMaxNoOfRetries = (UINT1) i4SetValPPPoEConfigPADIMaxNumberOfRetries;

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PPPoEConfigPADRMaxNumberOfRetries, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 0, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                  i4SetValPPPoEConfigPADIMaxNumberOfRetries));

    return (SNMP_SUCCESS);

}
#endif /* PPP_STACK_WANTED */

/****************************************************************************
 Function    :  nmhSetPPPoEConfigPADRMaxNumberOfRetries
 Input       :  The Indices

                The Object 
                setValPPPoEConfigPADRMaxNumberOfRetries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPPPoEConfigPADRMaxNumberOfRetries (INT4
                                         i4SetValPPPoEConfigPADRMaxNumberOfRetries)
{
    gu1PPPoEMaxNoOfRetries = (UINT1) i4SetValPPPoEConfigPADRMaxNumberOfRetries;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPPPoEConfigPADRWaitTime
 Input       :  The Indices

                The Object 
                setValPPPoEConfigPADRWaitTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPPPoEConfigPADRWaitTime (INT4 i4SetValPPPoEConfigPADRWaitTime)
{
    gu1PPPoEReTxInterval = (UINT1) i4SetValPPPoEConfigPADRWaitTime;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPPPoEHostUniqueEnabled
 Input       :  The Indices

                The Object 
                setValPPPoEHostUniqueEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPPPoEHostUniqueEnabled (INT4 i4SetValPPPoEHostUniqueEnabled)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    gHostUniqueFlag = (UINT1) i4SetValPPPoEHostUniqueEnabled;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PPPoEHostUniqueEnabled, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 0, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                  i4SetValPPPoEHostUniqueEnabled));


    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPPPoEACName
 Input       :  The Indices

                The Object 
                setValPPPoEACName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPPPoEACName (tSNMP_OCTET_STRING_TYPE * pSetValPPPoEACName)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    pPPPoEACName =
        MEM_REALLOC (pPPPoEACName, (size_t) (pSetValPPPoEACName->i4_Length + 1),
                     UINT1);
    MEMCPY (pPPPoEACName, pSetValPPPoEACName->pu1_OctetList,
            pSetValPPPoEACName->i4_Length);

    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PPPoEACName, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 0, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s",
                  pSetValPPPoEACName));

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PPPoEMode
 Input       :  The Indices

                The Object 
                testValPPPoEMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PPPoEMode (UINT4 *pu4ErrorCode, INT4 i4TestValPPPoEMode)
{
    if ((i4TestValPPPoEMode == 1) || (i4TestValPPPoEMode == 2))
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        PPP_TRC (MGMT, "Wrong Value");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

}

/****************************************************************************
 Function    :  nmhTestv2PPPoEConfigMaxTotalSessions
 Input       :  The Indices

                The Object 
                testValPPPoEConfigMaxTotalSessions
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PPPoEConfigMaxTotalSessions (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValPPPoEConfigMaxTotalSessions)
{

    if ((i4TestValPPPoEConfigMaxTotalSessions > 0)
        /* 230501 */  && (i4TestValPPPoEConfigMaxTotalSessions < (INT4) 0xffff))
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        PPP_TRC (MGMT, "Wrong value");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

}

/****************************************************************************
 Function    :  nmhTestv2PPPoEConfigMaxSessionsPerHost
 Input       :  The Indices

                The Object 
                testValPPPoEConfigMaxSessionsPerHost
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PPPoEConfigMaxSessionsPerHost (UINT4 *pu4ErrorCode,
                                        INT4
                                        i4TestValPPPoEConfigMaxSessionsPerHost)
{
    if (i4TestValPPPoEConfigMaxSessionsPerHost > 0)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        PPP_TRC (MGMT, "Wrong Value");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2PPPoEConfigPADITxInterval
 Input       :  The Indices

                The Object 
                testValPPPoEConfigPADITxInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PPPoEConfigPADITxInterval (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValPPPoEConfigPADITxInterval)
{
    if ((i4TestValPPPoEConfigPADITxInterval > 0)
        && (i4TestValPPPoEConfigPADITxInterval < 60))
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        PPP_TRC (MGMT, "Wrong Value");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
}

#ifdef PPP_STACK_WANTED
/****************************************************************************
 Function    :  nmhTestv2PPPoEConfigPADIMaxNumberOfRetries
 Input       :  The Indices

                The Object 
                testValPPPoEConfigPADIMaxNumberOfRetries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PPPoEConfigPADIMaxNumberOfRetries (UINT4 *pu4ErrorCode,
                                            INT4
                                            i4TestValPPPoEConfigPADIMaxNumberOfRetries)
{
    if ((i4TestValPPPoEConfigPADIMaxNumberOfRetries > 0)
        && (i4TestValPPPoEConfigPADIMaxNumberOfRetries < 30))
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        PPP_TRC (MGMT, "Wrong Value");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

}
#endif /* PPP_STACK_WANTED */
/****************************************************************************
 Function    :  nmhTestv2PPPoEConfigPADRMaxNumberOfRetries
 Input       :  The Indices

                The Object 
                testValPPPoEConfigPADRMaxNumberOfRetries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PPPoEConfigPADRMaxNumberOfRetries (UINT4 *pu4ErrorCode,
                                            INT4
                                            i4TestValPPPoEConfigPADRMaxNumberOfRetries)
{
    if ((i4TestValPPPoEConfigPADRMaxNumberOfRetries > 0)
        && (i4TestValPPPoEConfigPADRMaxNumberOfRetries < 30))
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        PPP_TRC (MGMT, "Wrong Value");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

}

/****************************************************************************
 Function    :  nmhTestv2PPPoEConfigPADRWaitTime
 Input       :  The Indices

                The Object 
                testValPPPoEConfigPADRWaitTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PPPoEConfigPADRWaitTime (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValPPPoEConfigPADRWaitTime)
{
    if ((i4TestValPPPoEConfigPADRWaitTime < 60)
        && (i4TestValPPPoEConfigPADRWaitTime >= 1))
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        PPP_TRC (MGMT, "Wrong Value");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

}

/****************************************************************************
 Function    :  nmhTestv2PPPoEHostUniqueEnabled
 Input       :  The Indices

                The Object 
                testValPPPoEHostUniqueEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PPPoEHostUniqueEnabled (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValPPPoEHostUniqueEnabled)
{
    if ((i4TestValPPPoEHostUniqueEnabled == 1)
        || (i4TestValPPPoEHostUniqueEnabled == 2))
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        PPP_TRC (MGMT, "Wrong Value");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2PPPoEACName
 Input       :  The Indices

                The Object 
                testValPPPoEACName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PPPoEACName (UINT4 *pu4ErrorCode,
                      tSNMP_OCTET_STRING_TYPE * pTestValPPPoEACName)
{
#ifdef SNMP_2_WANTED
    if (OSIX_FAILURE ==
        SNMPCheckForNVTChars (pTestValPPPoEACName->pu1_OctetList,
                              pTestValPPPoEACName->i4_Length))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif
    if ((pTestValPPPoEACName->i4_Length >= 32)
        || (pTestValPPPoEACName->i4_Length <= 0))
    {
        PPP_TRC (MGMT, "Name length out of range");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return (SNMP_FAILURE);
    }
    else
    {
        return (SNMP_SUCCESS);
    }
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2PPPoEMode
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PPPoEMode (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2PPPoEConfigMaxTotalSessions
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PPPoEConfigMaxTotalSessions (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2PPPoEConfigMaxSessionsPerHost
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PPPoEConfigMaxSessionsPerHost (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2PPPoEConfigPADITxInterval
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PPPoEConfigPADITxInterval (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2PPPoEConfigPADRMaxNumberOfRetries
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PPPoEConfigPADRMaxNumberOfRetries (UINT4 *pu4ErrorCode,
                                           tSnmpIndexList * pSnmpIndexList,
                                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2PPPoEConfigPADRWaitTime
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PPPoEConfigPADRWaitTime (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2PPPoEHostUniqueEnabled
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PPPoEHostUniqueEnabled (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2PPPoEACName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PPPoEACName (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/*********************************************/
tPPPoEBinding      *
PPPoEBindingGetNode (UINT4 Index)
{
    tPPPoEBinding      *pNode;
    SLL_SCAN (&PPPoEBindingList, pNode, tPPPoEBinding *)
    {
        if (pNode->u4Index == Index)
            return pNode;
    }
    return NULL;
}

VOID
PPPoEBindingGetIndex (UINT2 u2Port, UINT4 *pIndex)
{
    tPPPIf             *pIf;
    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if ((pIf->LinkInfo.PhysIfIndex == u2Port))
        {
            *pIndex = pIf->LinkInfo.IfIndex;
            break;
        }
    }
}

tPPPoEService      *
PPPoEGetServiceNode (UINT1 *Name, UINT1 Len)
{
    tPPPoEService      *pNode;
    SLL_SCAN (&PPPoEServiceList, pNode, tPPPoEService *)
    {
        if (!MEMCMP (Name, pNode->ServiceName, Len) &&
            (Len == STRLEN (pNode->ServiceName)))
        {
            return pNode;
        }
    }
    return NULL;
}

tPPPoEVlan         *
PPPoEGetVlanNode (UINT2 u2VlanId)
{
    tPPPoEVlan         *pNode = NULL;
    SLL_SCAN (&PPPoEVLANList, pNode, tPPPoEVlan *)
    {
        if (u2VlanId == pNode->u2VlanId)
        {
            return pNode;
        }
    }
    return NULL;
}

/**************************************************/
VOID
PPPoEInterfaceDown (UINT2 u2Index)
{
    tPPPoESessionNode  *pSession;
    tPPPoESessionNode  *pFreeSession = NULL;
    tClientDiscoveryNode *pDiscNode;
    tClientDiscoveryNode *pFreeDiscNode = NULL;

    /* delete all the pppoe sessions over this port */
    SLL_SCAN (&PPPoESessionList, pSession, tPPPoESessionNode *)
    {
        if (pFreeSession)
        {
            PPPoEDeleteSession (pFreeSession);
        }
        pFreeSession = NULL;
        if (pSession->u2Port == u2Index)
        {
            PPPoESendDiscoveryPkt ((UINT2) pSession->pIf->LinkInfo.IfIndex,
                                   pSession->DestMACAddr, PADT,
                                   pSession->u2SessionId, NULL, 0);
            pFreeSession = pSession;
        }
    }

    if (pFreeSession)
    {
        PPPoEDeleteSession (pFreeSession);
    }
    if (PPPoEMode == PPPOE_SERVER)
        return;
    /* for PPPoE Client we have to delete active discovery nodes */

    SLL_SCAN (&PPPoEClientDiscList, pDiscNode, tClientDiscoveryNode *)
    {
        if (pFreeDiscNode)
        {
            PPPOE_REMOVE_DISCOVERY_NODE (pFreeDiscNode);
        }
        pFreeDiscNode = NULL;
        if (pDiscNode->u2Port == u2Index)
        {
            pDiscNode->pIf->LinkInfo.pPppoeSession = NULL;
            PPPoEGiveEventToPPP (pDiscNode, CLOSE);
            pFreeDiscNode = pDiscNode;
        }
    }
    if (pFreeDiscNode)
    {
        PPPOE_REMOVE_DISCOVERY_NODE (pFreeDiscNode);
    }
    return;
}

/* LOW LEVEL Routines for Table : PPPoEVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePPPoEVlanTable
 Input       :  The Indices
                PPPoEVlanPPPIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePPPoEVlanTable (INT4 i4PPPoEVlanPPPIndex)
{
    tPPPoEVlan         *pPPPoEVlan;

    pPPPoEVlan = PPPoEGetVlanNode ((UINT2) i4PPPoEVlanPPPIndex);
    if (pPPPoEVlan == NULL)
    {
        PPP_TRC (MGMT, "No Matching Index Found");
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPPPoEVlanTable
 Input       :  The Indices
                PPPoEVlanPPPIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPPPoEVlanTable (INT4 *pi4PPPoEVlanPPPIndex)
{
    tPPPoEVlan         *pPPPoEVlan;
    UINT4               u4FirstIndex = MAX_INTEGER;
    INT4                i4Flag = 0;
    /*UINT4               u4NextIndex; */

    pPPPoEVlan = (tPPPoEVlan *) SLL_FIRST (&PPPoEVLANList);
    if (pPPPoEVlan == NULL)
        return SNMP_FAILURE;
    /*u4NextIndex = pPPPoEVlan->u2VlanId; */
    SLL_SCAN (&PPPoEVLANList, pPPPoEVlan, tPPPoEVlan *)
    {
        i4Flag = 1;
        if (pPPPoEVlan->u2VlanId < u4FirstIndex)
        {
            u4FirstIndex = pPPPoEVlan->u2VlanId;
        }
    }
    if (!i4Flag)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return SNMP_FAILURE;
    }
    *pi4PPPoEVlanPPPIndex = (INT4) u4FirstIndex;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPPPoEVlanTable
 Input       :  The Indices
                PPPoEVlanPPPIndex
                nextPPPoEVlanPPPIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPPPoEVlanTable (INT4 i4PPPoEVlanPPPIndex,
                               INT4 *pi4NextPPPoEVlanPPPIndex)
{
    tPPPoEVlan         *pPPPoEVlan;
    UINT4               u4NextIndex = MAX_INTEGER;
    INT4                i4Flag = 0;

    SLL_SCAN (&PPPoEVLANList, pPPPoEVlan, tPPPoEVlan *)
    {
        if (pPPPoEVlan->u2VlanId > i4PPPoEVlanPPPIndex)
        {
            i4Flag = 1;
            if (pPPPoEVlan->u2VlanId < u4NextIndex)
            {
                u4NextIndex = pPPPoEVlan->u2VlanId;
            }
        }
    }
    if (!i4Flag)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return (SNMP_FAILURE);
    }
    *pi4NextPPPoEVlanPPPIndex = (INT4) u4NextIndex;
    return (SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPPPoEVlanID
 Input       :  The Indices
                PPPoEVlanPPPIndex
                pi4RetValPPPoEVlanID
                The Object 
                retValPPPoEVlanID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEVlanID (INT4 i4PPPoEVlanPPPIndex, INT4 *pi4RetValPPPoEVlanID)
{

    tPPPIf             *pIf;

    pIf = (tPPPIf *) PppGetIfPtr ((UINT4) i4PPPoEVlanPPPIndex);
    if (pIf != NULL)
    {
        *pi4RetValPPPoEVlanID = (pIf->u2VlanTci & PPPOE_TPID_VLANID_MASK);
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetPPPoECFI
 Input       :  The Indices
                PPPoEVlanPPPIndex
                pi4RetValPPPoECFI
                The Object 
                retValPPPoECFI
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoECFI (INT4 i4PPPoEVlanPPPIndex, INT4 *pi4RetValPPPoECFI)
{

    tPPPIf             *pIf;

    pIf = (tPPPIf *) PppGetIfPtr ((UINT4) i4PPPoEVlanPPPIndex);
    if (pIf != NULL)
    {
        *pi4RetValPPPoECFI =
            (pIf->
             u2VlanTci & PPPOE_TPID_CFI_MASK) >> PPPOE_CFI_VLANTCI_BIT_POSITION;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetPPPoECoS
 Input       :  The Indices
                PPPoEVlanPPPIndex
                pi4RetValPPPoECoS
                The Object 
                retValPPPoECoS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoECoS (INT4 i4PPPoEVlanPPPIndex, INT4 *pi4RetValPPPoECoS)
{
    tPPPIf             *pIf;

    pIf = (tPPPIf *) PppGetIfPtr ((UINT4) i4PPPoEVlanPPPIndex);
    if (pIf != NULL)
    {
        *pi4RetValPPPoECoS =
            (pIf->
             u2VlanTci & PPPOE_TPID_COS_MASK) >> PPPOE_COS_VLANTCI_BIT_POSITION;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetPPPoEVlanRowStatus
 Input       :  The Indices
                PPPoEVlanPPPIndex
                pi4RetValPPPoEVlanRowStatus
                The Object 
                retValPPPoEVlanRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPPPoEVlanRowStatus (INT4 i4PPPoEVlanPPPIndex,
                          INT4 *pi4RetValPPPoEVlanRowStatus)
{
    tPPPoEVlan         *pVlan;

    pVlan = PPPoEGetVlanNode ((UINT2) i4PPPoEVlanPPPIndex);
    if (pVlan == NULL)
    {
        PPP_TRC (MGMT, "No Matching Entry Found");
        return (SNMP_FAILURE);
    }
    else
    {
        *pi4RetValPPPoEVlanRowStatus = ACTIVE;
    }

    return (SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPPPoEVlanID
 Input       :  The Indices
                PPPoEVlanPPPIndex
                i4SetValPPPoEVlanID
                The Object 
                setValPPPoEVlanID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPPPoEVlanID (INT4 i4PPPoEVlanPPPIndex, INT4 i4SetValPPPoEVlanID)
{
    tPPPIf             *pIf;

    pIf = (tPPPIf *) PppGetIfPtr ((UINT4) i4PPPoEVlanPPPIndex);
    if (pIf != NULL)
    {
        pIf->u2VlanTci = 0;
        pIf->u2VlanTci =
            (UINT2) (pIf->
                     u2VlanTci | (PPPOE_TPID_VLANID_MASK &
                                  i4SetValPPPoEVlanID));
    }
    else
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetPPPoECFI
 Input       :  The Indices
                PPPoEVlanPPPIndex
                i4SetValPPPoECFI
                The Object 
                setValPPPoECFI
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPPPoECFI (INT4 i4PPPoEVlanPPPIndex, INT4 i4SetValPPPoECFI)
{
    tPPPIf             *pIf;
    INT4                pi4RetValPPPoEVlanID = 0;

    pIf = (tPPPIf *) PppGetIfPtr ((UINT4) i4PPPoEVlanPPPIndex);
    if (pIf != NULL)
    {
        pi4RetValPPPoEVlanID = (pIf->u2VlanTci & PPPOE_TPID_VLANID_MASK);

        if (pi4RetValPPPoEVlanID >= PPPOE_MIN_ENCAPSULATION_VLAN_ID &&
            pi4RetValPPPoEVlanID <= PPPOE_MAX_ENCAPSULATION_VLAN_ID)
        {
            pIf->u2VlanTci =
                (UINT2) (pIf->u2VlanTci & (~(PPPOE_TPID_CFI_MASK)));
            pIf->u2VlanTci =
                (UINT2) (pIf->
                         u2VlanTci | (i4SetValPPPoECFI <<
                                      PPPOE_CFI_VLANTCI_BIT_POSITION));
        }
        else
        {
            PPP_TRC (MGMT, "VLAN ID not Configured");
            return (SNMP_FAILURE);
        }
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetPPPoECoS
 Input       :  The Indices
                PPPoEVlanPPPIndex
                i4SetValPPPoECoS
                The Object 
                setValPPPoECoS
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPPPoECoS (INT4 i4PPPoEVlanPPPIndex, INT4 i4SetValPPPoECoS)
{
    tPPPIf             *pIf;
    INT4                pi4RetValPPPoEVlanID = 0;

    pIf = (tPPPIf *) PppGetIfPtr ((UINT4) i4PPPoEVlanPPPIndex);
    if (pIf != NULL)
    {
        pi4RetValPPPoEVlanID = (pIf->u2VlanTci & PPPOE_TPID_VLANID_MASK);

        if (pi4RetValPPPoEVlanID >= PPPOE_MIN_ENCAPSULATION_VLAN_ID &&
            pi4RetValPPPoEVlanID <= PPPOE_MAX_ENCAPSULATION_VLAN_ID)
        {
            pIf->u2VlanTci =
                (UINT2) (pIf->u2VlanTci & (~(PPPOE_TPID_COS_MASK)));
            pIf->u2VlanTci =
                (UINT2) (pIf->
                         u2VlanTci | (i4SetValPPPoECoS <<
                                      PPPOE_COS_VLANTCI_BIT_POSITION));
        }
        else
        {
            PPP_TRC (MGMT, "VLAN ID not Configured");
            return (SNMP_FAILURE);
        }
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetPPPoEVlanRowStatus
 Input       :  The Indices
                PPPoEVlanPPPIndex
                i4SetValPPPoEVlanRowStatus
                The Object 
                setValPPPoEVlanRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPPPoEVlanRowStatus (INT4 i4PPPoEVlanPPPIndex,
                          INT4 i4SetValPPPoEVlanRowStatus)
{
    tPPPoEVlan         *pVlan;

    pVlan = PPPoEGetVlanNode ((UINT2) i4PPPoEVlanPPPIndex);

    if (i4SetValPPPoEVlanRowStatus == CREATE_AND_GO ||
        i4SetValPPPoEVlanRowStatus == CREATE_AND_WAIT)
    {
        if (pVlan != NULL)
            return SNMP_FAILURE;
        if ((pVlan = MEM_MALLOC (sizeof (tPPPoEVlan), tPPPoEVlan)) == NULL)
        {
            PPP_TRC (MGMT, "MemAlloc Failed \n");
            return (SNMP_FAILURE);
        }
        pVlan->u2VlanId = (UINT2) i4PPPoEVlanPPPIndex;
        SLL_ADD (&PPPoEVLANList, (t_SLL_NODE *) pVlan);
        return (SNMP_SUCCESS);
    }

    if (i4SetValPPPoEVlanRowStatus == ACTIVE)
    {
        if (pVlan != NULL)
            pVlan->u2VlanId = (UINT2) i4PPPoEVlanPPPIndex;
        return SNMP_SUCCESS;
    }

    if (i4SetValPPPoEVlanRowStatus == DESTROY)
    {
        if (pVlan == NULL)
            return SNMP_FAILURE;
        SLL_DELETE (&PPPoEVLANList, (t_SLL_NODE *) pVlan);
        MEM_FREE (pVlan);
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PPPoEVlanID
 Input       :  The Indices
                PPPoEVlanPPPIndex
                i4TestValPPPoEVlanID
                The Object 
                testValPPPoEVlanID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PPPoEVlanID (UINT4 *pu4ErrorCode, INT4 i4PPPoEVlanPPPIndex,
                      INT4 i4TestValPPPoEVlanID)
{
    PPP_UNUSED (i4PPPoEVlanPPPIndex);
    if ((i4TestValPPPoEVlanID >= PPPOE_MIN_ENCAPSULATION_VLAN_ID) &&
        (i4TestValPPPoEVlanID <= PPPOE_MAX_ENCAPSULATION_VLAN_ID))
    {
        return (SNMP_SUCCESS);
    }
    else if (i4TestValPPPoEVlanID == 0)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        PPP_TRC (MGMT, "Wrong Value");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2PPPoECFI
 Input       :  The Indices
                PPPoEVlanPPPIndex
                i4TestValPPPoECFI
                The Object 
                testValPPPoECFI
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PPPoECFI (UINT4 *pu4ErrorCode, INT4 i4PPPoEVlanPPPIndex,
                   INT4 i4TestValPPPoECFI)
{
    PPP_UNUSED (i4PPPoEVlanPPPIndex);
    if ((i4TestValPPPoECFI == PPPOE_ENABLE_ENCAPSULATION_CFI) ||
        (i4TestValPPPoECFI == PPPOE_DISABLE_ENCAPSULATION_CFI))
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        PPP_TRC (MGMT, "Wrong Value");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2PPPoECoS
 Input       :  The Indices
                PPPoEVlanPPPIndex
                i4TestValPPPoECoS
                The Object 
                testValPPPoECoS
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2PPPoECoS (UINT4 *pu4ErrorCode, INT4 i4PPPoEVlanPPPIndex,
                   INT4 i4TestValPPPoECoS)
{
    PPP_UNUSED (i4PPPoEVlanPPPIndex);
    if ((i4TestValPPPoECoS >= PPPOE_MIN_ENCAPSULATION_COS) &&
        (i4TestValPPPoECoS <= PPPOE_MAX_ENCAPSULATION_COS))
    {
        return (SNMP_SUCCESS);
    }
    else if (i4TestValPPPoECoS == 0)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        PPP_TRC (MGMT, "Wrong Value");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2PPPoEVlanRowStatus
 Input       :  The Indices
                PPPoEVlanPPPIndex
                i4TestValPPPoEVlanRowStatus
                The Object 
                testValPPPoEVlanRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_VALUE ref:(4 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PPPoEVlanRowStatus (UINT4 *pu4ErrorCode, INT4 i4PPPoEVlanPPPIndex,
                             INT4 i4TestValPPPoEVlanRowStatus)
{
    if ((i4TestValPPPoEVlanRowStatus == CREATE_AND_WAIT)
        || (i4TestValPPPoEVlanRowStatus == CREATE_AND_GO)
        || (i4TestValPPPoEVlanRowStatus == ACTIVE)
        || (i4TestValPPPoEVlanRowStatus == DESTROY))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    PPP_UNUSED (i4PPPoEVlanPPPIndex);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
 Function    :  nmhDepv2PPPoEVlanTable
 Input       :  The Indices
                PPPoEVlanPPPIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PPPoEVlanTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhSetPppPadrMaxRetryCount
 *  Input       :  The Indices
 *                 PppTestConfigIfIndex
 *   
 *                 The Object
 *                 setValPppPadrMaxRertyCount
 *  Output      :  The Set Low Lev Routine Take the Indices &
 *                 Sets the Value accordingly.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ******************************************************************************/
VOID
nmhSetPppPadrMaxRetryCount (INT4 i4SetValPppPadrMaxRertyCount)
{
   gu1PPPoEMaxNoOfRetries = (UINT1) i4SetValPppPadrMaxRertyCount;
}
/*****************************************************************************
 *  Function    :  nmhSetPppPadrMaxRetryCount
 *  Input       :  The Indices
 *                 PppTestConfigIfIndex
 *                 The Object
 *                 setValPppPadrMaxRertyCount
 *  Output      :  The Set Low Lev Routine Take the Indices &
 *                 Sets the Value accordingly.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *******************************************************************************/
INT1
nmhTestv2PadrMaxRetryCount (UINT4 *pu4ErrorCode, INT4 i4TestValPppPadrRetryVal)
{
    if ((i4TestValPppPadrRetryVal < 2)
        || (i4TestValPppPadrRetryVal > 10))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}
#endif  /*__PPP_PPPOELOW_C__*/
