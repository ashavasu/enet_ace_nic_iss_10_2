/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: hmac.c,v 1.2 2014/08/27 10:07:19 siva Exp $
 *
 * Description:This file contains hmac-md5 routines.
 *
 *******************************************************************/
#include "lr.h"
#include "arMD5_inc.h"
#include "arMD5_api.h"
VOID                hmac_md5 (unsigned char *, int, unsigned char *, int,
                              unsigned char *);

/************************************************************************/
/*  Function Name   :hmac_md5                                           */
/*  Description     :This function returns the Digest Value which is    */
/*                  : used in the AC-COOKIE tag by the AC-Concentrator  */
/*  Input(s)        : text:Refers to the MAC_ ADDRESS                   */
/*                  : text_len:Length of hte MAC_ADDRESS                */
/*                  : Key:Key used for getting Digest                   */
/*                  : Key:Length of the Key                             */
/*                  : Digest:The final Digest Value                     */
/*  Output(s)       : Digest Value                                      */
/*  Returns         :                                                   */
/*                                                                      */
/************************************************************************/
VOID
hmac_md5 (text, text_len, key, key_len, digest)
     unsigned char      *text;    /* pointer to data stream */
     int                 text_len;    /* length of data stream */
     unsigned char      *key;    /* pointer to authentication key */
     int                 key_len;    /* length of authentication key */
     unsigned char      *digest;    /* caller digest to be filled in */

{
    unArCryptoHash      context;
    unsigned char       k_ipad[65];    /* inner padding -
                                     * key XORd with ipad
                                     */
    unsigned char       k_opad[65];    /* outer padding -
                                     * key XORd with opad
                                     */
    unsigned char       tk[16];
    int                 i;
    /* if key is longer than 64 bytes reset it to key=MD5(key) */
    if (key_len > 64)
    {
        unArCryptoHash      tctx;

        MEMSET (&tctx, 0, sizeof (unArCryptoHash));
        arMD5_start (&tctx);
        arMD5_update (&tctx, key, (UINT2) (key_len));
        arMD5_finish (&tctx, digest);
        key = tk;
        key_len = 16;
    }

    /*
     * the HMAC_MD5 transform looks like:
     *
     * MD5(K XOR opad, MD5(K XOR ipad, text))
     *
     * where K is an n byte key
     * ipad is the byte 0x36 repeated 64 times
     * opad is the byte 0x5c repeated 64 times
     * and text is the data being protected
     */

    /* start out by storing key in pads */
    MEMSET (k_ipad, 0, sizeof k_ipad);
    MEMSET (k_opad, 0, sizeof k_opad);
    MEMCPY (k_ipad, key, key_len);
    MEMCPY (k_opad, key, key_len);

    /* XOR key with ipad and opad values */
    for (i = 0; i < 64; i++)
    {
        k_ipad[i] ^= 0x36;
        k_opad[i] ^= 0x5c;
    }
    /*
     * perform inner MD5
     */
    MEMSET (&context, 0, sizeof (unArCryptoHash));
    arMD5_start (&context);        /* init context for 1st
                                 * pass */
    arMD5_update (&context, k_ipad, 64);    /* start with inner pad */
    arMD5_update (&context, text, (UINT2) (text_len));    /* then text of datagram */
    arMD5_finish (&context, digest);    /* finish up 1st pass */
    /*
     * perform outer MD5
     */
    arMD5_start (&context);        /* init context for 2nd * pass */
    arMD5_update (&context, k_opad, 64);    /* start with outer pad */
    arMD5_update (&context, digest, 16);    /* then results of 1st hash */
    arMD5_finish (&context, digest);    /* finish up 2nd pass */
}
