/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppoe.c,v 1.11 2014/11/25 13:03:37 siva Exp $
 *
 * Description:This file contains the main routines for PPPoE module.
 *
 *******************************************************************/
#define _PPPOE_GLOABL_
#include "pppoecom.h"

UINT1               gau1PPPoeBcastAddr[6] =
    { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };

/************************************************************************/
/*  Function Name   : PPPoEInitialize                                   */
/*  Description     : Init routiones for PPPoE module                   */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
PPPoEInitialize (VOID)
{
    SLL_INIT (&PPPoEClientDiscList);
    SLL_INIT (&PPPoESessionList);
    SLL_INIT (&PPPoEServiceList);
    SLL_INIT (&PPPoEBindingList);
    SLL_INIT (&PPPoEVLANList);
    PPPoEMode = PPPOE_CLIENT;    /* DSL_ADD */
    gu4PPPoEActiveSessions = 0;
    gHostUniqueFlag = PPPOE_DISABLED;

    gu4PPPoEMaxSession = PPPOE_DEF_MAX_SESSION;
    gu4PPPoEMaxSessionPerHost = PPPOE_DEF_MAX_SESS_PER_HOST;
    gu1PPPoEReTxInterval = PPPOE_DEF_RETX_INTERVAL;
    gu1PPPoEMaxNoOfRetries = PPPOE_DEF_RETRIES_NO;
    /* NAETRA_ADD */
    pPPPoEACName = MEM_MALLOC (STRLEN (PPPOE_DEF_AC_NAME) + 1, UINT1);
    if (pPPPoEACName != NULL)
    {
        STRCPY (pPPPoEACName, PPPOE_DEF_AC_NAME);
    }
}

/************************************************************************/
/*  Function Name   : PPPoERxPktFromLL                                  */
/*  Description     : This Function is called by the Lower Layer module */
/*                  : Length:Length of the Buffer                       */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
PPPoERxPktFromLL (t_MSG_DESC ** ppBuf, UINT2 Length, UINT2 u2Port)
{
    tCfaIfInfo          IfInfo;
    UINT1              *pu1EthHdr;
    UINT1              *pu1SrcMACAddr;
    tPPPoESessionNode  *pSession;
    tPPPoEHeader        PppoeHdr;
    t_MSG_DESC         *pBuf;

    /* DSL_ADD */
    pBuf = *ppBuf;

    pu1EthHdr = (UINT1 *) CRU_BMC_Get_DataPointer (pBuf);

    if (CfaGetIfInfo (u2Port, &IfInfo) == CFA_FAILURE)
    {
        PPP_TRC (ALL_FAILURE, "Invalid port");
        return;
    }

    pu1SrcMACAddr = IfInfo.au1MacAddr;

    if (!(VALID_PPPOE_ETHERNET_HDR (pu1EthHdr, pu1SrcMACAddr)))
    {
        return;
    }

    PPPoEExtractHeader (pBuf, &PppoeHdr);

    /* DSL_MODIFY */
    if (PPPoERemoveEthPadding (ppBuf, PppoeHdr.u2Length, Length) == FAILURE)
        return;

    /* DSL_ADD */
    pBuf = *ppBuf;

    if ((PppoeHdr.u1Code != PADI)
        && (MEMCMP (pu1SrcMACAddr, PppoeHdr.DestMACAddr, HOST_ADDR_LEN)))
        return;

    switch (PppoeHdr.u2EthType)
    {
        case PPPOE_SESSION:
            if (PppoeHdr.u1Code != 0)
            {
                PPP_TRC (ALL_FAILURE, "Session Pkt Code must be zero ...");
                return;
            }
            if (PPPoEGetSessionNode (&PppoeHdr, u2Port, &pSession) == FAILURE)
            {
                PPP_TRC (ALL_FAILURE, "Invalid Session Pkt ...");
                return;
            }
            if (PPPoEIsDot1QTagPresent (pBuf) == SUCCESS)
            {
                MOVE_OFFSET (pBuf,
                             PPPOE_FULL_HDR_LEN + PPPOE_TPID_VLAN_BYTE_LENGTH);
            }
            else
            {
                MOVE_OFFSET (pBuf, PPPOE_FULL_HDR_LEN);
            }

            PPPoERelFlag = PPP_NO;
            ReleaseFlag = PPP_YES;
            Length = (UINT2) VALID_BYTES (pBuf);
            PPPRxPktFromLL (pSession->pIf, pBuf, Length);
            if (ReleaseFlag == PPP_YES)
            {
                RELEASE_INC_BUFFER (pBuf);
            }
            break;

        case PPPOE_DISCOVERY:
            PPPoERcvdDiscPkt (pBuf, &PppoeHdr, Length, u2Port);
            break;
        default:
            return;
    }

}

/************************************************************************/
/*  Function Name   : PPPoERcvdDiscPkt                                  */
/*  Description     : This function is invoked when ever a Discovery    */
/*                  : Packet is Recieved                                */
/*  Input(s)        : pBuf:Points To the incoming Buffer                */
/*                  : pHdr:Points to the Ethernet Header                */
/*                  : Length: Length of the Incoming Buffer             */
/*                  : u2Port:Points to the interface over which this    */
/*                  : packet was recieved                               */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
PPPoERcvdDiscPkt (t_MSG_DESC * pBuf, tPPPoEHeader * pHdr, UINT2 Length,
                  UINT2 u2Port)
{
    tPPPoEBinding      *pPppoeBinding;
    UINT1               u1IsVlanPresent = FALSE;

    pPppoeBinding = PPPoEBindingGetNode (u2Port);
    /* we assume pPppoeBinding != NULL. This check was already done
     * before queuing the packet into ppp.
     * */
    if (pPppoeBinding == NULL)
    {
        return;
    }
    PPPoEDumpDiscoveryPkt (pHdr);

    if (PPPoEVerifyDiscoveryPkt (pHdr, Length, u2Port) != OK)
    {
        pPppoeBinding->NoOfMalFormPkts++;
        PPP_TRC (ALL_FAILURE, "Dropping PPPoE Discovery Pkt");
        return;
    }

    if (PPPoEIsDot1QTagPresent (pBuf) == SUCCESS)
    {
        MOVE_OFFSET (pBuf, PPPOE_FULL_HDR_LEN + PPPOE_TPID_VLAN_BYTE_LENGTH);
        u1IsVlanPresent = TRUE;
    }
    else
    {
        MOVE_OFFSET (pBuf, PPPOE_FULL_HDR_LEN);
    }

    if (PPPoEMode == PPPOE_SERVER)
    {
        switch (pHdr->u1Code)
        {
            case PADI:
                if (PPPoERcvdPADI (pBuf, pHdr, u2Port) == FAILURE)
                 {
                    pPppoeBinding->NoOfPADIRej++;
                 }
		 else
                 {
                pPppoeBinding->NoOfPADIRecv++;
                 }
                break;
            case PADR:
                if (PPPoERcvdPADR (pBuf, pHdr, pPppoeBinding) == FAILURE)
                {
                    pPppoeBinding->NoOfPADRRej++;
                }
                else
                {
                pPppoeBinding->NoOfPADRRecv++;
                }
                break;
            case PADT:
                if (PPPoERcvdPADT (pBuf, pHdr, u2Port, u1IsVlanPresent) ==
                    FAILURE)
                 {
                 	PPP_TRC (ALL_FAILURE, "Dropping PPPoEPPPoERcvdPADT Pkt");   
                 	pPppoeBinding->NoOfMalFormPkts++;
                        pPppoeBinding->NoOfPADTRej++;
                 }
                 else
                {
                pPppoeBinding->NoOfPADTRecv++;
                }
                break;
            default:
                PPP_TRC (ALL_FAILURE, "Invalid Discovery Packet");
                break;
        }
    }
    else
    {
        switch (pHdr->u1Code)
        {
            case PADO:
                if (PPPoERcvdPADO (pBuf, pHdr, u2Port, u1IsVlanPresent) ==
                    FAILURE)
	         {
                  	pPppoeBinding->NoOfMalFormPkts++;
                  	pPppoeBinding->NoOfPADORej++;
		 }
                 else
                 {
                 	pPppoeBinding->NoOfPADORecv++;
                 }
                pPppoeBinding->NoOfMultPADORecv++;
                break;
            case PADS:
                if (PPPoERcvdPADS (pBuf, pHdr, u2Port, u1IsVlanPresent) ==
                    FAILURE)
		   {
                    	pPppoeBinding->NoOfMalFormPkts++;
		    	pPppoeBinding->NoOfPADSRej++;
		   }
 		else
 		{
        		pPppoeBinding->NoOfPADSRecv++;
 		}
                break;
            case PADT:
                if (PPPoERcvdPADT (pBuf, pHdr, u2Port, u1IsVlanPresent) ==
                    FAILURE)
                 {
			PPP_TRC (ALL_FAILURE, "Dropping PPPoEPPPoERcvdPADT Pkt @ client side");
                 	pPppoeBinding->NoOfMalFormPkts++;
                  	pPppoeBinding->NoOfPADTRej++;
                 }
                 else
                 {
                 pPppoeBinding->NoOfPADTRecv++;
                 }
                break;
            default:
                PPP_TRC (ALL_FAILURE, "Invalid Discovery Packet");
                break;
        }
    }

}

/************************************************************************/
/*  Function Name   : PPPoEVerifyDiscoveryPkt                           */
/*  Description     : This Function validates the header of the         */
/*                  : Discovery Packet                                  */
/*  Input(s)        : pHdr:Points to the Ethernet Header                */
/*                  : u2Len:Length of the Header                        */
/*                  : u2Port: The interface over which the packet was   */
/*                  : Recieved                                          */
/*  Output(s)       : None                                              */
/*  Returns         :Returns OK or NOT_OK                               */
/************************************************************************/
INT4
PPPoEVerifyDiscoveryPkt (tPPPoEHeader * pHdr, UINT2 u2Len, UINT2 u2Port)
{
    if (pHdr->u1VerAndType != PPPOE_THIS_VTYPE)
    {
        if ((pHdr->u1VerAndType & PPPOE_THIS_VER) != PPPOE_THIS_VER)
        {
            PPP_TRC (ALL_FAILURE, "VERSION MISMATCH");
        }
        if ((pHdr->u1VerAndType & PPPOE_THIS_TYPE) != PPPOE_THIS_TYPE)
        {
            PPP_TRC (ALL_FAILURE, "TYPE MISMATCH");
        }
        return NOT_OK;
    }
    PPP_UNUSED (u2Len);
    PPP_UNUSED (u2Port);
    return OK;

}

/************************************************************************/
/*  Function Name   : PPPoEGetSessionNode                               */
/*  Description     : This Function is used to get Session Node from    */
/*                  : the Session List                                  */
/*  Input(s)        : pHdr:Points to the Ethernet Header                */
/*                  : u2Port:The Interface over which the packet was    */
/*                  : Recieved                                          */
/*                  : pSessionNode: Points To the Session Node in the   */
/*                  : Session List                                      */
/*  Output(s)       : None                                              */
/*  Returns         : Returns  SUCCESS or FAILURE                       */
/************************************************************************/
INT4
PPPoEGetSessionNode (tPPPoEHeader * pHdr, UINT2 u2Port,
                     tPPPoESessionNode ** pSessionNode)
{
    tPPPoESessionNode  *pNode;
    UINT1              *pDestMac;
    UINT2               u2SessionId;
    u2SessionId = pHdr->u2SessionId;
    pDestMac = pHdr->SrcMACAddr;

    SLL_SCAN (&PPPoESessionList, pNode, tPPPoESessionNode *)
    {
        if ((pNode->u2SessionId == u2SessionId)
            && PPPOE_SAME_ADDR (pDestMac, pNode->DestMACAddr)
            && (u2Port == pNode->u2Port))
        {
            *pSessionNode = pNode;
            return SUCCESS;
        }
    }                            /* SLL_SCAN */
    return FAILURE;
}

/************************************************************************/
/*  Function Name   : PPPoERestartTimer                                 */
/*  Description     : Restarts the PPPoE Timer                          */
/*  Input(s)        : pDiscNode: Points to the Discovery Node           */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
PPPoERestartTimer (tClientDiscoveryNode * pDiscNode)
{
    pDiscNode->ReTxCount++;
    pDiscNode->PADTimer.Param1 = PTR_TO_U4 (pDiscNode);

    PPPStartTimer (&pDiscNode->PADTimer, PAD_EXPECT_TIMER,
                   (UINT4) ((pDiscNode->ReTxCount % 2 +
                             1) * gu1PPPoEReTxInterval));

}

/************************************************************************/
/*  Function Name   : PPPoERemoveEthPAdding                             */
/*  Description     : This Function Removes the Padding added by        */
/*                  : the ETHERNET                                      */
/*  Input(s)        : pInBuf:Points to the incoming Buffer              */
/*                  : PayLoadLen: Length of the Ethernet PayLoad        */
/*                  : BufLen:Length of the Incoming Packet              */
/*  Output(s)       : None                                              */
/*  Returns         : Returns SUCCESS or FAILURE                        */
/************************************************************************/
INT4
PPPoERemoveEthPadding (t_MSG_DESC ** pInBuf, UINT2 PayLoadLen, UINT2 BufLen)
{
    /* Ethernet Header may contain padding bytes at the end. Minimum Ethernet 
     * header Length is 60 bytes. We remove the padding bytes 
     * here */

    UINT2               u2CorrectLen = 0;;

    if (PPPoEIsDot1QTagPresent (*pInBuf) == SUCCESS)
    {
        u2CorrectLen =
            (UINT2) (PayLoadLen + PPPOE_FULL_HDR_LEN +
                     PPPOE_TPID_VLAN_BYTE_LENGTH);
    }
    else
    {
        u2CorrectLen = (UINT2) (PayLoadLen + PPPOE_FULL_HDR_LEN);
    }

    if (u2CorrectLen < BufLen)
    {
        UINT1              *pTempLinear;
        t_MSG_DESC         *pBuf;
        pBuf = *pInBuf;
        if ((pTempLinear = MEM_MALLOC (u2CorrectLen, UINT1)) == NULL)
            return FAILURE;
        CRU_BUF_Copy_FromBufChain (pBuf, pTempLinear, 0, u2CorrectLen);
        RELEASE_BUFFER (pBuf);
        *pInBuf = ALLOCATE_PPPOE_BUFFER (u2CorrectLen);
        CRU_BUF_Copy_OverBufChain (*pInBuf, pTempLinear, 0, u2CorrectLen);
        MEM_FREE (pTempLinear);

    }

    return SUCCESS;
}

/************************************************************************/
/*  Function Name   : PPPoEDumpDiscoveryPkt                             */
/*  Description     : This Function prints the fields of received       */
/*                  : discovery pkt                                     */
/*  Input(s)        : pHdr : Extracted PPPoE Header                     */
/*  Output(s)       : None                                              */
/************************************************************************/
VOID
PPPoEDumpDiscoveryPkt (tPPPoEHeader * pHdr)
{
    PPP_TRC (CONTROL_PLANE, "----------------------------");
    PPP_TRC (CONTROL_PLANE, "Received PPPoE Discovery Pkt:\n");
    PPP_TRC6 (CONTROL_PLANE, "Dest MAC Addr  - %02x:%02x:%02x:%02x:%02x:%02x",
              pHdr->DestMACAddr[0], pHdr->DestMACAddr[1], pHdr->DestMACAddr[2],
              pHdr->DestMACAddr[3], pHdr->DestMACAddr[4], pHdr->DestMACAddr[5]);
    PPP_TRC6 (CONTROL_PLANE, "Src  MAC Addr  - %02x:%02x:%02x:%02x:%02x:%02x",
              pHdr->SrcMACAddr[0], pHdr->SrcMACAddr[1], pHdr->SrcMACAddr[2],
              pHdr->SrcMACAddr[3], pHdr->SrcMACAddr[4], pHdr->SrcMACAddr[5]);
    PPP_TRC1 (CONTROL_PLANE, "Ethernet Type  - 0x%x", pHdr->u2EthType);
    PPP_TRC1 (CONTROL_PLANE, "Ver&Type       - 0x%x", pHdr->u1VerAndType);
    switch (pHdr->u1Code)
    {
        case PADI:
            PPP_TRC1 (CONTROL_PLANE, "Code           - 0x%x [ PADI ]",
                      pHdr->u1Code);
            break;
        case PADO:
            PPP_TRC1 (CONTROL_PLANE, "Code           - 0x%x [ PADO ]",
                      pHdr->u1Code);
            break;
        case PADR:
            PPP_TRC1 (CONTROL_PLANE, "Code           - 0x%x [ PADR ]",
                      pHdr->u1Code);
            break;
        case PADS:
            PPP_TRC1 (CONTROL_PLANE, "Code           - 0x%x [ PADS ]",
                      pHdr->u1Code);
            break;
        case PADT:
            PPP_TRC1 (CONTROL_PLANE, "Code           - 0x%x [ PADT ]",
                      pHdr->u1Code);
            break;
        default:
            PPP_TRC1 (CONTROL_PLANE, "Code           - 0x%x [UNKNOWN]",
                      pHdr->u1Code);
            break;
    }
    PPP_TRC1 (CONTROL_PLANE, "Session Id     - %x", pHdr->u2SessionId);
    PPP_TRC1 (CONTROL_PLANE, "Length         - %d", pHdr->u2Length);
    PPP_TRC (CONTROL_PLANE, "----------------------------");
}

#ifdef FS_KERNEL_WANTED
/************************************************************************/
/*  Function Name   : PPPoEUpdateNatToKernel                            */
/*  Description     : This Function notifies kernel to enable NAT       */
/*                    on PPP interface                                  */
/*  Input(s)        : u4IfIndex : PPP interface index                   */
/*                    u4IpAddr : IP address                             */
/*  Output(s)       : None                                              */
/************************************************************************/

VOID
PPPoEUpdateNatToKernel (UINT4 u4IfIndex, UINT4 u4IpAddr)
{
    tPPPoESessionNode  *pNode;

    SLL_SCAN (&PPPoESessionList, pNode, tPPPoESessionNode *)
    {
        if (pNode->pIf->LinkInfo.IfIndex == u4IfIndex)
        {
            FsNpAutoNat (u4IfIndex, u4IpAddr, CREATE_AND_GO);
            return;
        }
    }
    return;
}
#endif

/************************************************************************/
/*  Function Name   : PPPoEGetDestMacFromPPPIfIdx                       */
/*  Description     : This Function gives Mac address of the peer       */
/*                    for the given PPP index.                          */
/*  Input(s)        : u4IfIndex : PPP interface index                   */
/*                    au1DestMac : Mac Address of the peer.             */
/*  Output(s)       : returns SUCCESS/FAILURE                           */
/************************************************************************/
INT4
PPPoEGetDestMacFromPPPIfIdx (UINT4 u4IfIndex, UINT1 *au1DestMac)
{
    tPPPoESessionNode  *pNode;

    SLL_SCAN (&PPPoESessionList, pNode, tPPPoESessionNode *)
    {
        if (pNode->pIf->LinkInfo.IfIndex == u4IfIndex)
        {
            MEMCPY (au1DestMac, pNode->DestMACAddr, HOST_ADDR_LEN);
            return SUCCESS;
        }
    }
    return FAILURE;
}

/************************************************************************/
/*  Function Name   : PPPoEIsDot1QTagPresent                            */
/*  Description     : This Function checks whether VLAN encapsulation   */
/*                    is present in the message buffer                  */
/*  Input(s)        : pBuf : Message buffer                             */
/*  Output(s)       : None                                              */
/*  Returns         : SUCCESS/FAILURE                                   */
/************************************************************************/
INT4
PPPoEIsDot1QTagPresent (t_MSG_DESC * pBuf)
{
    UINT1              *pLinearPkt;
    UINT2               u2Val = 0;

    pLinearPkt = (UINT1 *) CRU_BMC_Get_DataPointer (pBuf);

    /* Extracting ether type */
    u2Val = *(UINT2 *) (VOID *) (pLinearPkt + PPPOE_ETHER_TYPE_INDEX);
    u2Val = OSIX_NTOHS (u2Val);
    if (VLAN_PROTOCOL_ID == u2Val)
    {
        return SUCCESS;
    }
    return FAILURE;
}

VOID
PppGetPPPoEMode (INT4 *pi4RetValPPPoEMode)
{
    PppLock ();
    *pi4RetValPPPoEMode = PPPoEMode;
    PppUnlock ();
}
