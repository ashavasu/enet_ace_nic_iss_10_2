/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppoelp.h,v 1.3 2014/10/15 13:16:03 siva Exp $
 *
 * Description:
 *
 ********************************************************************/

/* Proto Validate Index Instance for PPPoEAcCookieTable. */
INT1
nmhValidateIndexInstancePPPoEAcCookieTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for PPPoEAcCookieTable  */

INT1
nmhGetFirstIndexPPPoEAcCookieTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexPPPoEAcCookieTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetPPPoEAcCookieMethod ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPPPoEConfigAcCookieKey ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetPPPoEAcCookieMethod ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetPPPoEConfigAcCookieKey ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2PPPoEAcCookieMethod ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2PPPoEConfigAcCookieKey ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));



/* Proto type for Low Level GET Routine All Objects.  */


INT1
nmhGetPPPoEBindingsMaxSessions ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetPPPoEBindingsNumberActiveSessions ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPPPoEBindingsTotalNumberSessions ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPPPoEBindingsNumberPADIRejected ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPPPoEBindingsNumberPADIReceived ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPPPoEBindingsNumberPADITransmitted ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPPPoEBindingsNumberPADORejected ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPPPoEBindingsNumberPADOReceived ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPPPoEBindingsNumberPADSReceived ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPPPoEBindingsNumberPADSRejected ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPPPoEBindingsNumberPADTRejected ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPPPoEBindingsNumberPADRRejected ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPPPoEBindingsNumberPADRReceived ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPPPoEBindingsNumberPADRTransmitted ARG_LIST((INT4 ,UINT4 *));


INT1
nmhGetPPPoEBindingsNumberPADOTransmitted ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPPPoEBindingsNumberPADSTransmitted ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPPPoEBindingsNumberPADTReceived ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPPPoEBindingsNumberPADTTransmitted ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPPPoEBindingsNumberServiceNameErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPPPoEBindingsNumberACSystemErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPPPoEBindingsNumberGenericErrorsReceived ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPPPoEBindingsNumberGenericErrorsTransmitted ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPPPoEBindingsNumberMalformedPackets ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPPPoEBindingsNumberPADITimeouts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetPPPoEBindingsNumberMultiplePADOReceived ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */


INT1
nmhSetPPPoEBindingsMaxSessions ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */


INT1
nmhTestv2PPPoEBindingsMaxSessions ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

