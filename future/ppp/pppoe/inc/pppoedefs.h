/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppoedefs.h,v 1.2 2014/03/11 14:02:54 siva Exp $
 *
 * Description:This file contains the  #defines for pppoe module
 *
 *******************************************************************/
#ifndef __PPPOE_DEFS_H__
#define __PPPOE_DEFS_H__

#define  PPPOE_ENABLE              1
#define  PPPOE_DISABLE             2
#define  PPPOE_SERVER              1
#define  PPPOE_CLIENT              2
#define  PPPOE_ENABLED             1
#define  PPPOE_DISABLED            2
#define  PPPOE_NORMAL              1
#define  PPPOE_RETRANS             2
#define  PPPOE_NONE                1
#define  HMAC_MD5                  2

#define  PPPOE_SESSION             0x8864
#define  PPPOE_DISCOVERY           0x8863

#define  END_OF_LIST               0x0000 
#define  SERVICE_NAME              0x0101 
#define  AC_NAME                   0x0102 
#define  HOST_UNIQUE               0x0103 
#define  AC_COOKIE                 0x0104 
#define  VENDOR_SPEC               0x0105 
#define  RELLAY_SESS_ID            0x0110 
#define  SERVICE_NAME_ERROR        0x0201 
#define  AC_SYSTEM_ERROR           0x0202 
#define  GENERIC_ERROR             0x0203 

#define  PADI                      0x09
#define  PADO                      0x07
#define  PADR                      0x19
#define  PADS                      0x65
#define  PADT                      0xa7

#define  PADI_SENT                 1
#define  PADO_RCVD                 2
#define  PADR_SENT                 3
#define  RPADS_PLUS                5
#define  RPADS_MINUS               6
#define  AC_NAME_GRADE             100


#define  PPPOE_SRC_ADDR_INDEX      6      
#define  PPPOE_ETHER_TYPE_INDEX    12     
#define  PPPOE_VTYPE_INDEX         14     
#define  PPPOE_CODE_INDEX          15     
#define  PPPOE_SESSIONID_INDEX     16     
#define  PPPOE_LENGTH_INDEX        18     

#define  MAC_ADDRESS_SIZE          6      
#define  HOST_ADDR_LEN             6       
#define  ENET_HDR_LEN              14     
#define  PPPOE_ETHER_TYPE_LEN      2      
#define  PPPOE_VTYPE_LEN           1      
#define  PPPOE_CODE_LEN            1      
#define  PPPOE_SESSIONID_LEN       2      
#define  PPPOE_LENGTH_LEN          2      
#define  AC_COOKIE_TAG_LEN         18     
#define  HMAC_DIGEST_LEN           16     
#define  COOKIE_ID_LEN             2      
#define  TAG_HDR_LEN               4      
#define  PPPOE_FULL_HDR_LEN        20     
#define  PPPOE_HDR_LEN             6      
#define  MAX_PADI_LEN              1484   
#define  MAX_TAG_LEN               1480   
#define  TAG_TYPE_LEN              2      
#define  DEF_TAG_VALUE_LEN         32     
#define  HOST_UNIQUE_TAG_LEN       2     
#define  DEF_MAX_PADO_LEN          32     
#define  DEF_MAX_PADI_LEN          32     
#define  DEF_MAX_PADR_LEN          32     
#define  MAX_ACCOOKIE_KEY_LEN      32     
#define  PPPOE_MAX_SERVICE_NAME_LEN 32   
#define  PPPOE_TPID_VLAN_BYTE_LENGTH 4  

        
#define  PPPOE_SESSION_TYPE        0x8864 
#define  PPPOE_DISCOVERY_TYPE      0x8863 
#define  PPPOE_VER_AND_TYPE        0x11   
#define  PPPOE_SESSION_CODE        0x00   
#define  PPPOE_SESS_HDR_LENGTH     20     
#define  PPPOE_TYPE                6      

#define  PPPOE_THIS_VTYPE          0x11
#define  PPPOE_THIS_VER            0x10
#define  PPPOE_THIS_TYPE           0x01

#define  PPPOE_TPID_COS_MASK       0xE000
#define  PPPOE_TPID_CFI_MASK       0x1000
#define  PPPOE_TPID_VLANID_MASK    0x0FFF

#define  PPPOE_MIN_ENCAPSULATION_VLAN_ID 1
#define  PPPOE_MAX_ENCAPSULATION_VLAN_ID 4094
#define  PPPOE_MIN_ENCAPSULATION_COS     1 
#define  PPPOE_MAX_ENCAPSULATION_COS     7 
#define  PPPOE_ENABLE_ENCAPSULATION_CFI  1
#define  PPPOE_DISABLE_ENCAPSULATION_CFI 0
#define  PPPOE_COS_VLANTCI_BIT_POSITION  13
#define  PPPOE_CFI_VLANTCI_BIT_POSITION  12 

#ifndef  MAX_INTEGER               
#define  MAX_INTEGER               0xffffffff
#endif

#define PPPOE_SAME_ADDR(pAddr1,pAddr2)    (!(MEMCMP(pAddr1,pAddr2,6)))

#define COPY_MAC_ADDR(pDest,pSrc)   MEMCPY(pDest,pSrc,HOST_ADDR_LEN)

#define FREE_DISC_STORED_BUF(pNode)     if(pNode->StoredBuf != NULL){\
                                                FREE(pNode->StoredBuf);\
                                                pNode->StoredBuf = NULL;\
                                        }

#define PPPOE_REMOVE_DISCOVERY_NODE(pDiscNode)  \
                      PPPStopTimer (&pDiscNode->PADTimer);\
                      FREE_DISC_STORED_BUF(pDiscNode);\
                      SLL_DELETE(&PPPoEClientDiscList,(t_SLL_NODE *)pDiscNode);\
                      MEM_FREE(pDiscNode);



#define TAG_FREE(Tag)   if(TagFreeFlag == PPP_YES)\
                    MEM_FREE(Tag.pValue)

#define  PPPOE_PREPEND_HEADER(pBuf,pLinear,Size) \
                            CRU_BUF_Prepend_BufChain(pBuf,pLinear,Size)

#define  ALLOCATE_PPPOE_BUFFER(Size)   \
                            CRU_BUF_Allocate_MsgBufChain (Size, 0)

#define VALID_PPPOE_ETHERNET_HDR(pRcvdEhtHdr , SrcAddr) \
    (\
     (MEMCMP(pRcvdEhtHdr+MAC_ADDRESS_SIZE, SrcAddr, MAC_ADDRESS_SIZE))\
     && (\
         !((MEMCMP(pRcvdEhtHdr,SrcAddr,MAC_ADDRESS_SIZE))\
         && ( MEMCMP(pRcvdEhtHdr,gau1PPPoeBcastAddr,MAC_ADDRESS_SIZE)\
             || (PPPoEMode != PPPOE_SERVER )\
            )\
         )\
         )\
     )

#endif /* __PPPOE_DEFS_H__ */
