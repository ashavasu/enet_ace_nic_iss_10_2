/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppoeport.h,v 1.6 2014/10/15 13:16:03 siva Exp $
 *
 * Description:definitions used by external modules.
 *
 *******************************************************************/
#ifndef __PPPOE_PORT_H__
#define __PPPOE_PORT_H__

#define  PPPOE_DEF_MAX_SESSION        128                                       
#define  PPPOE_DEF_MAX_SESS_PER_HOST  16                                         

#define  PPPOE_DEF_RETX_INTERVAL      2                                         
#define  PPPOE_DEF_RETRIES_NO         5                                         
#define  PPPOE_DEF_AC_NAME            "Future"                                  
#define  PPPOE_KEEP_ALIVE_VALUE       100
#define  PPPOE_DEF_AC_NAME_LENGTH     32 /* NAETRA_ADD */

#define  PPPOE_IF_EXISTS(ifIndex)     (CfaApiCheckIfEntryExist((UINT4)ifIndex) != CFA_FAILURE)


typedef struct pppoebinding {
    t_SLL_NODE       NextPPPoEIf;
    UINT4      u4Index;
    UINT4      NoOfActiveSessions;
    UINT4      u4TotalNofSessions;
    UINT4      NoOfPADIRej;
    UINT4      NoOfPADITrans;
    UINT4      NoOfPADIRecv;
    UINT4      NoOfPADRRej;
    UINT4      NoOfPADRTrans;
    UINT4      NoOfPADRRecv;
    UINT4      NoOfPADORecv;    
    UINT4      NoOfPADOTrans;
    UINT4      NoOfPADORej;
    UINT4      NoOfPADSRecv;
    UINT4      NoOfPADSTrans;
    UINT4      NoOfPADSRej;
    UINT4      NoOfPADTRecv;
    UINT4      NoOfPADTTrans;
    UINT4      NoOfPADTRej;
    UINT4      NoOfSerErrors;
    UINT4      NoOfAcSysErrors;
    UINT4      NoOfGenErrorsRecv;
    UINT4      NoOfGenErrorsTrans;
    UINT4      NoOfMalFormPkts;
    UINT4      NoOfPADITimeOuts;
    UINT4      NoOfMultPADORecv;
    UINT1      *Key;  
    UINT2      MaxSessions;
    UINT1      Enable;
    UINT1      ACCookieMethod;
    UINT2      ACCookieId;
    UINT2      u2Rsvd;
 }tPPPoEBinding;   


UINT4 PPPoETlsEventFromPPP ARG_LIST( (tPPPIf *));
VOID PPPoETlfEventFromPPP ARG_LIST((tPPPIf *));
VOID PPPSendDataToPPPoE ARG_LIST((VOID *,t_MSG_DESC *, UINT2 ));
VOID PPPoEInitialize ARG_LIST((VOID));
VOID PPPoERxPktFromLL  ARG_LIST( (t_MSG_DESC **,UINT2 ,UINT2 ) );
VOID PPPDeletePPPoESession  ARG_LIST( (tPPPIf * ) );
#endif
