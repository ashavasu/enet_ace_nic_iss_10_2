/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppoecom.h,v 1.4 2014/03/11 14:02:54 siva Exp $
 *
 * Description:This file contains the common pppoe  inc files
 *
 *******************************************************************/
#ifndef __PPPOE_COM_H__
#define __PPPOE_COM_H__

#include    "lr.h"
#include    "sllmap.h"
#include    "pppcom.h"
#include    "lcpdefs.h"
#include    "gsemdefs.h"
#include    "genexts.h"
#include    "ppptask.h"
#include    "gcpdefs.h"
#include    "lcpprot.h"
#include    "pppproto.h"
#include    "globexts.h"

#include    "pppoeport.h"
#include    "pppoedefs.h"
#include    "pppoetdfs.h"
#include    "pppoeprot.h"
#include    "cfa.h"
#include     "ppp.h"
#ifdef _PPPOE_GLOABL_
#define PPPOE_EXTERN   
#else
#define PPPOE_EXTERN   extern  
#endif

PPPOE_EXTERN    t_SLL           PPPoEClientDiscList;
PPPOE_EXTERN    t_SLL           PPPoESessionList;
PPPOE_EXTERN    t_SLL           PPPoEServiceList;
PPPOE_EXTERN    t_SLL           PPPoEBindingList;
PPPOE_EXTERN    t_SLL           PPPoEVLANList;
PPPOE_EXTERN    UINT1           PPPoEMode;
PPPOE_EXTERN    UINT4           gu4PPPoEActiveSessions;
PPPOE_EXTERN    UINT4           gu4PPPoEMaxSession;
PPPOE_EXTERN    UINT4           gu4PPPoEMaxSessionPerHost;
PPPOE_EXTERN    UINT1           gu1PPPoEReTxInterval;
PPPOE_EXTERN    UINT1           gu1PPPoEMaxNoOfRetries;
PPPOE_EXTERN    UINT1           gHostUniqueFlag;
PPPOE_EXTERN    UINT1           *pPPPoEACName;
PPPOE_EXTERN    UINT1           *pClientServiceName;
PPPOE_EXTERN    UINT1           PPPoERelFlag;
extern          UINT1           gau1PPPoeBcastAddr[];
#undef PPP_TRC_NAME
#define PPP_TRC_NAME "\nPPPoE"

#endif  /*  __PPPOE_COM_H__ */
