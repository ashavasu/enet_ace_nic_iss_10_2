/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppoetdfs.h,v 1.3 2014/03/11 14:02:54 siva Exp $
 *
 * Description:This file contains the common LCP files
 *
 *******************************************************************/
#ifndef __PPPOE_TDFS_H__
#define __PPPOE_TDFS_H__
#include "pppoe.h"
typedef struct PPPoESessionNode {
 t_SLL_NODE NextNode;
 tPPPIf  *pIf;
 UINT2  u2Port;
 UINT2  u2SessionId;
 UINT2  ACCookieId;
 UINT1  DestMACAddr[HOST_ADDR_LEN];
 UINT1  SrcMACAddr[HOST_ADDR_LEN];
 UINT1  au1Rsvd[2];
} tPPPoESessionNode;

typedef struct ClientDiscoveryNode {
 t_SLL_NODE NextNode;
 tPPPIf  *pIf;
 UINT1  DestMACAddr[HOST_ADDR_LEN];
 UINT2  u2Port;
 t_SLL  pPADOList;
 tPPPTimer PADTimer;
    UINT1       *StoredBuf;
    UINT2       StoredBufLen;
    UINT1       aHostUnique[HOST_UNIQUE_TAG_LEN];
 UINT1  ReTxCount;
 UINT1  State;
 UINT2  u2Rsvd;
} tClientDiscoveryNode;

typedef struct PPPoETag {
 UINT1 *pValue;
 UINT2 Type;
 UINT2 Len;
} tPPPoETag;


typedef struct pppoeservice {
   t_SLL_NODE       NextPPPoEIf;
   UINT1            *ServiceName;
}tPPPoEService;

typedef struct pppoevlan {
   t_SLL_NODE       NextPPPoEVlan;
   UINT2            u2VlanId;
   UINT2            u2Rsvd;
}tPPPoEVlan;
    
typedef struct PADOList{
        t_SLL_NODE      NextNode;
        tPPPoEHeader    *pHdr;
        t_MSG_DESC      *pRcvdBuf;
} tPADOListNode;

#endif /* __PPPOE_TDFS_H__ */
