/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppoeprot.h,v 1.2 2014/03/11 14:02:54 siva Exp $
 *
 * Description:Function prototypes.
 *
 *******************************************************************/
#ifndef __PPPOE_PROT_H__
#define __PPPOE_PROT_H__

VOID PPPoERestartTimer  ARG_LIST( (tClientDiscoveryNode *) );
INT4 PPPoERcvdPADI  ARG_LIST( (t_MSG_DESC *,tPPPoEHeader *,UINT2 ) );
INT4 PPPoERcvdPADR  ARG_LIST( (t_MSG_DESC *,tPPPoEHeader *,tPPPoEBinding * ) );
INT4 PPPoERcvdPADO  ARG_LIST( (t_MSG_DESC *,tPPPoEHeader *,UINT2, UINT1 ) );
INT4 PPPoERcvdPADS  ARG_LIST( (t_MSG_DESC *,tPPPoEHeader *,UINT2, UINT1 ) );
INT4 PPPoERcvdPADT  ARG_LIST( (t_MSG_DESC *,tPPPoEHeader *,UINT2, UINT1 ) );
INT4 PPPoESendPADO  ARG_LIST( (t_MSG_DESC *,tPPPoEHeader *,UINT2 ) );
INT4 PPPoESendPADS  ARG_LIST( (t_MSG_DESC *,tPPPoEHeader *,UINT2 ) );
VOID AddTags  ARG_LIST( (UINT1 **,UINT2 *,tPPPoETag *pTag,UINT2 *) );
VOID PPPoEAddCookieTag  ARG_LIST( (UINT1 **,tPPPoEHeader *, UINT2 ,UINT2 *,UINT2 *) );
VOID PPPoECSendPADI  ARG_LIST( (UINT2 ,tClientDiscoveryNode *,UINT1 ) );
VOID PPPoEAddHostUniqueTag  ARG_LIST( (UINT1 **,UINT2 *,UINT1 *) );
VOID PPPoEPADTimeOut  ARG_LIST( (VOID *) );
VOID PPPoESendPADR  ARG_LIST( (tPADOListNode *,tClientDiscoveryNode *) );
VOID PPPoEInitDiscNode  ARG_LIST( (tClientDiscoveryNode *) );
VOID PPPoECAddNewSession  ARG_LIST( (tClientDiscoveryNode *,tPPPoEHeader *) );
VOID PPPoEAddNewSession  ARG_LIST( (tPPPoESessionNode *) );
VOID PPPoEDeleteSession  ARG_LIST( (tPPPoESessionNode   *) );
VOID PPPoEGiveEventToPPP  ARG_LIST( (VOID *,UINT1 ) );
VOID ReleasePPPInterface  ARG_LIST( (tPPPIf *) );
VOID PPPoEDeletePADOListNodes  ARG_LIST( (VOID *) );
VOID PPPoEInterfaceDown  ARG_LIST( (UINT2 ) );
VOID PPPoESendDiscoveryPkt  ARG_LIST( (UINT2 ,UINT1 *,UINT1 , UINT2 ,UINT1 *,UINT2 ) );
VOID PPPoETxPktToLL  ARG_LIST( (t_MSG_DESC *,UINT2 ,UINT2 ) );
INT4 PPPoEGetSessionNode  ARG_LIST( (tPPPoEHeader *pHdr, UINT2 u2Port,tPPPoESessionNode **pSessionNode) );
VOID PPPoERcvdDiscPkt  ARG_LIST( (t_MSG_DESC *pBuf,tPPPoEHeader *pHdr,UINT2 Length,UINT2 u2Port) );
INT4 PPPoEVerifyDiscoveryPkt  ARG_LIST( (tPPPoEHeader *pHdr,UINT2 u2Len,UINT2   u2Port) );
INT4 PPPoEVerifyPADIorR  ARG_LIST( (t_MSG_DESC *pBuf,tPPPoEHeader *pHdr,UINT2 u2Port) );
INT4 PPPoEVerifyPADO  ARG_LIST( (t_MSG_DESC *,tPPPoEHeader *) );
INT4 PPPoEGetClientDiscoveryNode  ARG_LIST( (t_MSG_DESC *pBuf,tPPPoEHeader *pHdr, UINT2 u2Port,UINT1 Code,tClientDiscoveryNode **pDiscNode) );
INT1 PPPoECProcessPADS  ARG_LIST( (t_MSG_DESC *pBuf,tPPPoEHeader *pHdr,UINT2 u2Port) );
INT4 CheckTagPresent  ARG_LIST( (t_MSG_DESC *pBuf,UINT2 Len,UINT2 Type) );
UINT2 PPPoEActiveSessionPerHost  ARG_LIST( (tPPPoEHeader *pHdr) );
tPPPoEBinding *PPPoEBindingGetNode  ARG_LIST( (UINT4 Index) );
INT4 PPPoESProcessServiceName  ARG_LIST( (UINT1 **pRespTags,UINT2 *Offset,tPPPoETag *pTag, UINT1 Code,UINT2 *BufLen) );
INT4 PPPoESVerifyCookie  ARG_LIST( (tPPPoETag *pTag,tPPPoEHeader *pHdr,UINT2 u2Port,UINT1 *DupFlag,UINT2 *Id) );
INT4 GetFreePPPIfIndex  ARG_LIST( (UINT4 *pIfIndex,tPPPIf **pRetIf,UINT2 u2Port,UINT1 IfTypte) );
void hmac_md5  ARG_LIST( (UINT1 *,INT2,UINT1 *,INT2,UINT1 *) );
UINT2 PPPoESGetNewCookieId  ARG_LIST( (tPPPoEHeader *pHdr,UINT2 u2Port) );
INT4 GetPPPoETag  ARG_LIST( (t_MSG_DESC *pBuf,tPPPoETag *pTag,UINT2 Len,UINT2 TagType) );
tPADOListNode *PPPoESelectAC  ARG_LIST( (tClientDiscoveryNode *pDiscNode) );
INT4 PPPoEGradePADO  ARG_LIST( (tPADOListNode *) );
INT4 PPPoECPADSServiceName  ARG_LIST( (tPPPoETag *pTag,UINT2 u2Port) );
tPPPoEService *PPPoEGetServiceNode  ARG_LIST( (UINT1 *Name,UINT1 Len) );
tPPPoEVlan *PPPoEGetVlanNode  ARG_LIST( (UINT2 u2VlanId) );
INT4 PPPoERemoveEthPadding  ARG_LIST( (t_MSG_DESC **,UINT2 , UINT2 ) );
VOID PPPoEDumpDiscoveryPkt ARG_LIST( (tPPPoEHeader *pHdr) );
INT4 PPPoERestart (tPPPIf *pIf);
INT4 PPPoEHandleLinkDown (tPPPIf *pIf);
INT4 PPPoEIsDot1QTagPresent (t_MSG_DESC *pBuf);

#endif /* __PPPOE_PROT_H__ */
