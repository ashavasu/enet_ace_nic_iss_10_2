/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mplscpcom.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the common PPP files required for
 * the MPLSCP module
 *
 *******************************************************************/
#ifndef __PPP_MPLSCPCOM_H__
#define __PPP_MPLSCPCOM_H__

#include "genhdrs.h"

/* Global PPP includes */
#include "pppmacro.h"
#include "pppdefs.h"
#include "pppdebug.h"
#include "ppptimer.h"
#include "pppdbgex.h"


/* Includes from other modules */
#include "frmtdfs.h"
#include "pppgsem.h"
#include "pppgcp.h"
#include "ppplcp.h"
#include "pppexts.h"

#include "pppmplscp.h"

#include "mplsproto.h"


#endif  /* __PPP_MPLSCPCOM_H__ */
