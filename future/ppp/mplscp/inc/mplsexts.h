/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mplsexts.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains definitions and prototypes for 
 * MPLSCP functions used by external modules
 *
 *******************************************************************/
#ifndef __PPP_MPLSEXTS_H__
#define __PPP_MPLSEXTS_H__


extern   UINT2   MplscpIfFreeCounter;
extern   UINT2   MplscpAllocCounter;
extern   UINT2   MplscpFreeCounter;
extern  tGenOptionInfo          MPLSCPGenOptionInfo[];
extern  tOptNegFlagsPerIf       MPLSCPOptPerIntf[];
extern  tGSEMCallbacks          MplscpGSEMCallback;

#endif  /* __PPP_MPLSEXTS_H__ */
