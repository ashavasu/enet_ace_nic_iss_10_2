/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mplsproto.h,v 1.2 2011/10/13 10:31:37 siva Exp $
 *
 * Description:This file contains Prototypes for the MPLSCP functions 
 * used internally by MPLSCP module
 *
 *******************************************************************/
#ifndef __PPP_MPLSPROTO_H__
#define __PPP_MPLSPROTO_H__

/* Prototypes for MPLSCP functions used internally by MPLSCP module */

tMPLSCPIf *MPLSCPCreateIf(tPPPIf *pIf);
VOID MPLSCPEnableIf(tMPLSCPIf *pMplscpPtr);
VOID MPLSCPDisableIf(tMPLSCPIf *pMplscpPtr);
INT1 MPLSCPInit(tMPLSCPIf *pMplscpPtr);
VOID MPLSCPOptionInit(tMPLSCPOptions *pMPLSCPOpt);
VOID MPLSCPUp(tGSEM *pMplscpGSEM);
VOID MPLSCPDown(tGSEM *pMplscpGSEM);
VOID MPLSCPFinished(tGSEM *pMplscpGSEM);
VOID *MPLSCPReturnDummyPtr(tGSEM  *pGSEM, UINT1 *OptLen);

/* SNMP related */
INT1 CheckAndGetMplsPtr(tPPPIf *pIf);
tMPLSCPIf *PppGetMPLSCPIfPtr(UINT4 Index);
tMPLSCPIf *PppCreateMPLSCPIfPtr (UINT4 Index);
VOID MPLSCPDeleteIf (tMPLSCPIf * pMplscpPtr);
#endif  /* __PPP_MPLSPROTO_H__ */
