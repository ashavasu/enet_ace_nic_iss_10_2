/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppmplscp.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the typedefs, #defines and 
 *             data structures used by the MPLSCP Module.						
 *
 *******************************************************************/
#ifndef __PPP_PPPMPLSCP_H__
#define __PPP_PPPMPLSCP_H__

/*********************************************************************/
/*                              CONSTANTS                           */
/*********************************************************************/

/* CONFIGURATION OPTION VALUES  for MPLSCP packets */

#define  MAX_MPLSCP_OPT_TYPES           1    
#define  MPLS_ADDR_IDX                  1    
#define  MPLS_OPTION_IDX                0
#define  MPLS_DUMMY_OPTION              1
#define  MPLS_UNICAST_DATAGRAM          0x0281 
#define  MPLS_MULTICAST_DATAGRAM        0x0283 

#define  MPLSCP_UP                      1    
#define  MPLSCP_DOWN                    2    



/********************************************************************/
/*          TYPEDEFS    USED BY THE MPLSCP MODULE                     */
/*********************************************************************/

typedef   struct   {
	UINT1          DummyOption;
	UINT1	       u1Rsvd1;
	UINT2	       u2Rsvd2;
}   tMPLSCPOptions;


typedef   struct   mplscpif   {
	
	VOID           *HLHandle;  
	UINT1          OperStatus;
	UINT1          AdminStatus;
    UINT1          RowStatus;
        UINT1		 u1Rsvd;
	tMPLSCPOptions   MplscpOptionsDesired;
	tMPLSCPOptions   MplscpOptionsAllowedForPeer;
	tMPLSCPOptions   MplscpOptionsAckedByPeer;
	tMPLSCPOptions   MplscpOptionsAckedByLocal;
	tGSEM            MplscpGSEM;
}   tMPLSCPIf;


#endif  /* __PPP_PPPMPLSCP_H__ */
