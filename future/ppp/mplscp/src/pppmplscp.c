/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppmplscp.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains callback routines of MPLSCP module.
 *             It also contains routines for the input MPLSCP packet 
 *             processing.
 *
 *******************************************************************/

#include "mplscpcom.h"
#include "gsemdefs.h"
#include "gcpdefs.h"
#include "genexts.h"
#include "lcpdefs.h"
#include "mplsexts.h"
#include "mpdefs.h"
#include "globexts.h"

/******************** GLOBALS ************************************/
UINT2               MplscpAllocCounter;
UINT2               MplscpFreeCounter;
UINT2               MplscpIfFreeCounter;

tOptHandlerFuns     MPLSOptionsHandlingFuns = {
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
};

tGSEMCallbacks      MplscpGSEMCallback = {
    MPLSCPUp,
    MPLSCPDown,
    NULL,
    MPLSCPFinished,
    NCPRxData,
    NCPTxData,
    NULL,
    NULL
};

tGenOptionInfo      MPLSCPGenOptionInfo[] = {

    {MPLS_DUMMY_OPTION, RANGE, BYTE_LEN_4, 6, &MPLSOptionsHandlingFuns, {NULL}
     , NOT_SET, 0, 0}
};

tOptNegFlagsPerIf   MPLSCPOptPerIntf[] = {

    {MPLS_DUMMY_OPTION, 0, 0}
};

/***********************************************************************/
/*                      INTERFACE  FUNCTIONS                           */
/***********************************************************************
*  Function Name : MPLSCPCreateIf
*  Description   :
*          This procedure creates an entry in the MPLSCP interface data 
*  base corresponding to IfId. It also initializes the created entry to the 
*  default values  by calling the MPLSCPinit() function.
*  Parameter(s)  :
*         pIf -    pointer to the PPP interface structure
*  Return Values : 
*      pointer to the newly created  interface structure , if the creation is 
*        success
*      NULL if it fails to create the interface.
*********************************************************************/
tMPLSCPIf          *
MPLSCPCreateIf (tPPPIf * pIf)
{
    tMPLSCPIf          *pMplscpPtr;
    if ((pMplscpPtr = (tMPLSCPIf *) ALLOC_MPLSCP_IF ()) != NULL)
    {
        if (MPLSCPInit (pMplscpPtr) != NOT_OK)
        {
            pMplscpPtr->MplscpGSEM.pIf = pIf;
            return (pMplscpPtr);
        }
        else
        {
            PPP_TRC (ALL_FAILURE, "Error: MPLSCP Init Failure .\n");
            FREE_MPLSCP_IF (pMplscpPtr);
            return (NULL);
        }
    }
    PPP_TRC (ALL_FAILURE, "Error: MPLSCP Alloc/Init failure .\n");
    return (NULL);
}

/*********************************************************************
*  Function Name : MPLSCPDeleteIf
*  Description   :
*               This procedure deletes an MPCP interface  entry from the 
*  database and deallocates memory corresponding to the interface.
*  Parameter(s)  :
*         pMplscpPtr -  points to  the MPLSCP interface structure to be deleted.
*  Return Values : VOID
*********************************************************************/
VOID
MPLSCPDeleteIf (tMPLSCPIf * pMplscpPtr)
{
    pMplscpPtr->MplscpGSEM.pIf->pMplscpPtr = NULL;
    GSEMDelete (&(pMplscpPtr->MplscpGSEM));
    FREE_MPLSCP_IF (pMplscpPtr);
    return;
}

/*********************************************************************
*  Function Name : MPLSCPEnableIf
*  Description   :
*              This function is called when there is an  MPLSCP Admin Open 
*  for an interface from the SNMP It  triggers an OPEN event to the GSEM and 
*  passes the MPLSCP tGSEM corresponds to this interface as parameter.
*  Parameter(s)    :
*    pMplscpPtr    -    Points to the MPLSCP interface structure to be enabled
*  Return Values : VOID
*********************************************************************/
VOID
MPLSCPEnableIf (tMPLSCPIf * pMplscpPtr)
{
    UINT1               Index;
    tPPPIf             *pIf;

    if (pMplscpPtr->MplscpGSEM.pIf->BundleFlag == BUNDLE)
    {
        SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
        {
            if (pIf->MPInfo.MemberInfo.pBundlePtr != NULL
                && pIf->MPInfo.MemberInfo.pBundlePtr ==
                pMplscpPtr->MplscpGSEM.pIf)
            {
                pIf->pMplscpPtr = pMplscpPtr;
            }
        }
    }

    pMplscpPtr->MplscpGSEM.pNegFlagsPerIf[MPLS_OPTION_IDX].FlagMask |=
        ALLOWED_FOR_PEER_SET;
    pMplscpPtr->AdminStatus = ADMIN_OPEN;

    /* Check whether the link is already in the Operational state. 
       If so, then indicate the GSEM about the  link UP 
     */

    if ((pMplscpPtr->MplscpGSEM.CurrentState != REQSENT)
        || (pMplscpPtr->MplscpGSEM.CurrentState != ACKRCVD)
        || (pMplscpPtr->MplscpGSEM.CurrentState != ACKSENT))
    {
        MEMCPY (&pMplscpPtr->MplscpOptionsAckedByPeer,
                &pMplscpPtr->MplscpOptionsDesired, sizeof (tMPLSCPOptions));
        for (Index = 0; Index < MAX_MPLSCP_OPT_TYPES; Index++)
        {
            if (pMplscpPtr->MplscpGSEM.pNegFlagsPerIf[Index].
                FlagMask & DESIRED_SET)
            {
                pMplscpPtr->MplscpGSEM.pNegFlagsPerIf[Index].FlagMask |=
                    ACKED_BY_PEER_SET;
            }
            else
            {
                pMplscpPtr->MplscpGSEM.pNegFlagsPerIf[Index].FlagMask &=
                    ACKED_BY_PEER_NOT_SET;
            }
        }
    }

    GSEMRun (&pMplscpPtr->MplscpGSEM, OPEN);

    if ((pMplscpPtr->MplscpGSEM.pIf->LcpStatus.LinkAvailability ==
         LINK_AVAILABLE) && (pMplscpPtr->MplscpGSEM.CurrentState != CLOSED))
    {
        GSEMRun (&pMplscpPtr->MplscpGSEM, UP);
    }

    GSEMRestartIfNeeded (&pMplscpPtr->MplscpGSEM);

    return;
}

/*********************************************************************
*  Function Name : MPLSCPDisableIf
*  Description   :
*          This function is called when there is an Admin Close for an 
*  interface from the SNMP It triggers a CLOSE event to the GSEM and passes 
*  the MPLSCP GSEMStructure corresponds to this interface as a parameter. 
*  Parameter(s)  :
*            pMplscpPtr - points to  the MPLSCP interface structure to be  disabled
*  Return Values : VOID
*********************************************************************/
VOID
MPLSCPDisableIf (tMPLSCPIf * pMplscpPtr)
{
    GSEMRun (&pMplscpPtr->MplscpGSEM, CLOSE);
    return;
}

/*********************************************************************
*  Function Name : MPLSCPFinished
*  Description   :
*    This function is invoked when the higher layer is done with 
*  the link and the link is no longer needed. This invokes  procedures for 
*  terminating the link.
*         The pFinished  field of the GSEMCallback structure points 
*  to this function and is called from the GSEM Module.
*  Parameter(s)  :  
*            pGSEM  - pointer to MPLSCP tGSEM corresponding 
*  to current interface
*  Return Value  :    VOID
*********************************************************************/
VOID
MPLSCPFinished (tGSEM * pGSEM)
{
    tMPLSCPIf          *pMplscpPtr;
    UINT1               Index;
    pMplscpPtr = pGSEM->pIf->pMplscpPtr;
    MEMCPY (&pMplscpPtr->MplscpOptionsAckedByPeer,
            &pMplscpPtr->MplscpOptionsDesired, sizeof (tMPLSCPOptions));
    for (Index = 0; Index < MAX_MPLSCP_OPT_TYPES; Index++)
    {
        if (pGSEM->pNegFlagsPerIf[Index].FlagMask & DESIRED_SET)
        {
            pGSEM->pNegFlagsPerIf[Index].FlagMask |= ACKED_BY_PEER_SET;
        }
        else
        {
            pGSEM->pNegFlagsPerIf[Index].FlagMask &= ACKED_BY_PEER_NOT_SET;
        }
    }
    return;
}

/*********************************************************************
*  Function Name : MPLSCPInit
*  Description   :
*              This function initializes the MPLSCP GSEM and  sets 
*  the  MPLSCP configuration option variables to their default values. It is 
*  invoked by the MPLSCPCreateIf() function.
*  Parameter(s)  :
*        pMplscpPtr -   points to the MPLSCP interface structure
*  Return Values : OK/NOT_OK 
*********************************************************************/
INT1
MPLSCPInit (tMPLSCPIf * pMplscpPtr)
{
    /* Initialize the Option fields */
    MPLSCPOptionInit (&pMplscpPtr->MplscpOptionsDesired);
    MPLSCPOptionInit (&pMplscpPtr->MplscpOptionsAllowedForPeer);
    MPLSCPOptionInit (&pMplscpPtr->MplscpOptionsAckedByPeer);
    MPLSCPOptionInit (&pMplscpPtr->MplscpOptionsAckedByLocal);

    pMplscpPtr->AdminStatus = STATUS_DOWN;
    pMplscpPtr->OperStatus = STATUS_DOWN;

    if (GSEMInit (&pMplscpPtr->MplscpGSEM, MAX_MPLSCP_OPT_TYPES) == NOT_OK)
    {
        return (NOT_OK);
    }
    pMplscpPtr->MplscpGSEM.Protocol = MPLSCP_PROTOCOL;
    pMplscpPtr->MplscpGSEM.pCallbacks = &MplscpGSEMCallback;
    pMplscpPtr->MplscpGSEM.pGenOptInfo = MPLSCPGenOptionInfo;
    MEMCPY (pMplscpPtr->MplscpGSEM.pNegFlagsPerIf, &MPLSCPOptPerIntf,
            (sizeof (tOptNegFlagsPerIf) * MAX_MPLSCP_OPT_TYPES));
    return (OK);
}

/***********************************************************************/
/*                      INTERNAL  FUNCTIONS                            */
/***********************************************************************
*  Function Name : MPLSCPOptionInit
*  Description   :
*               This function initializes the MPLSCPOption structure and is
*  called by the MPLSCPInit function.
*  Parameter(s)  :
*       pMPLSCPOpt   -  points to the MPLSCPOption structure  to be initialized.
*  Return Value  :
*********************************************************************/
VOID
MPLSCPOptionInit (tMPLSCPOptions * pMPLSCPOpt)
{
    pMPLSCPOpt->DummyOption = 0;
    return;
}

/*********************************************************************
*  Function Name : MPLSCPUp
*  Description   :
*              This function is called once the network level configuration  
*  option is negotiated  successfully. This function indicates  to the higher 
*  layers  that the MPLSCP is up.  The pUp  field of  the MPLSCPGSEMCallback points *  to this function and is called from the GSEM Module.
*  Parameter(s)  :
*            pMplscpGSEM  - pointer to the MPLSCP tGSEM 
*                                   corresponding to the current interface.
*  Return Values : VOID 
*********************************************************************/
VOID
MPLSCPUp (tGSEM * pMplscpGSEM)
{
    tMPLSCPIf          *pMplscpIf;

    pMplscpIf = (tMPLSCPIf *) pMplscpGSEM->pIf->pMplscpPtr;
    pMplscpIf->OperStatus = STATUS_UP;
    PPPHLINotifyProtStatusToHL (pMplscpGSEM->pIf, MPLSCP_PROTOCOL, UP);
    return;
}

/*********************************************************************
*  Function Name : MPLSCPDown
*  Description   :
*          This function is invoked  when the MPLSCP protocol is leaving the 
*  OPENED state. It  indicates the higher layers  about the operational status 
*  of the MPLSCP.  The pDown  field of  the MPLSCPGSEMCallback structure  points to 
*  this function and is called from the GSEM Module.
*  Parameter(s)  :
*            pMplscpGSEM  - pointer to the MPLSCP tGSEM 
*                       corresponding to the current interface.
*  Return Values : VOID 
*********************************************************************/
VOID
MPLSCPDown (tGSEM * pMplscpGSEM)
{
    tPPPIf             *pIf;
    tMPLSCPIf          *pMplscpIf;

    pIf = pMplscpGSEM->pIf;
    pMplscpIf = (tMPLSCPIf *) pMplscpGSEM->pIf->pMplscpPtr;
    PPPHLINotifyProtStatusToHL (pIf, MPLSCP_PROTOCOL, DOWN);
    MPLSCPFinished (pMplscpGSEM);
    pMplscpIf->OperStatus = STATUS_DOWN;

    MEMCPY (&pMplscpIf->MplscpOptionsAckedByPeer,
            &pMplscpIf->MplscpOptionsDesired, sizeof (tMPLSCPOptions));
    return;
}

/*********************************************************************
*  Function Name : MPLSCPCopyOptions 
*  Description   :
*    This function is used to copy the MPLSCP options.
*  Input         :
*    pGSEM        -     Points to the GSEM structure
*    Flag        -     Specifies whether to alloc or free the temporary 
*                    variable.
*    PktType        -    Specifies whether the function is called from request
*                    processing or Nak processing.                  
*  Return Value        :
*                    DISCARD on allocation error, OK otherwise.
*********************************************************************/
INT1
MPLSCPCopyOptions (tGSEM * pGSEM, UINT1 Flag, UINT1 PktType)
{
    PPP_UNUSED (pGSEM);
    PPP_UNUSED (Flag);
    PPP_UNUSED (PktType);
    return (OK);
}

/*********************************************************************
*  Function Name : MPLSCPGetSEMPtr
*  Description   :
*    This function returns the pointer to the MPLSCP GSEM taking the tPPPIf
*   structure as a parameter.
*  Input         :
*       pIf -   Points to the PPP Interface structure.
*  Return Value  :
*           Returns a pointer to the MPLSCP GSEM.
*********************************************************************/
tGSEM              *
MPLSCPGetSEMPtr (tPPPIf * pIf)
{
    tMPLSCPIf          *pMplscpIf;

    if (pIf->pMplscpPtr != NULL)
    {
        pMplscpIf = (tMPLSCPIf *) pIf->pMplscpPtr;
        return (&pMplscpIf->MplscpGSEM);
    }
    return (NULL);
}
