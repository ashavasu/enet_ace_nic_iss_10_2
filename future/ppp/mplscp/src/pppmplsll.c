/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppmplsll.c,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

#include "mplscpcom.h"
#include "snmphdrs.h"
#include "pppsnmpm.h"

#include "mplsexts.h"
#include "globexts.h"
#include "llproto.h"
#include "ppmpllow.h"

/******************************************************************************/ INT1
CheckAndGetMplsPtr (tPPPIf * pIf)
{
    if (pIf->pMplscpPtr != NULL)
    {
        return (PPP_SNMP_OK);
    }
    return (PPP_SNMP_ERR);
}

/*********************************************************************
*  Function Name : PppGetMPLSCPIfPtr
*  Description   :
*  Parameter(s)  :
*   InDataBasePtr -
*   InNamePtr     -
*   Arg           -
*   Value         -
*  Return Value  :
*
*********************************************************************/
tMPLSCPIf          *
PppGetMPLSCPIfPtr (Index)
     UINT4               Index;
{
    tPPPIf             *pIf;

    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if ((pIf != NULL) && (pIf->LinkInfo.IfIndex == Index))
            return (pIf->pMplscpPtr);
    }
    PPP_TRC (MGMT, "MPLSCP INTERFACE NOT  CREATED ");
    return ((tMPLSCPIf *) NULL);
}

/*******************************************************************/
tMPLSCPIf          *
PppCreateMPLSCPIfPtr (Index)
     UINT4               Index;
{
    tPPPIf             *pIf;

    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if ((pIf != NULL) && (pIf->LinkInfo.IfIndex == Index))
        {
            if (pIf->BundleFlag != BUNDLE
                && pIf->MPInfo.MemberInfo.pBundlePtr != NULL)
            {
                return (NULL);
            }

            if (pIf->pMplscpPtr == NULL)
            {
                if ((pIf->pMplscpPtr = MPLSCPCreateIf (pIf)) == NULL)
                {
                    return (NULL);
                }
            }
            PPP_TRC (MGMT, "MPLSCP INTERFACE  CREATED ");
            return (pIf->pMplscpPtr);
        }
    }
    PPP_TRC (MGMT, "MPLSCP INTERFACE NOT  CREATED ");
    return (NULL);
}

/* LOW LEVEL Routines for Table : PppMplsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppMplsTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppMplsTable (INT4 i4IfIndex)
{
    if (PppGetMPLSCPIfPtr (i4IfIndex) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Index Found");
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppMplsTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppMplsTable (INT4 *pi4IfIndex)
{
    if (nmhGetFirstIndex (pi4IfIndex, MplsInstance, FIRST_INST) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    PPP_TRC (MGMT, "No Matching Index Found");
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppMplsTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppMplsTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    if (nmhGetNextIndex (&i4IfIndex, MplsInstance, NEXT_INST) == SNMP_SUCCESS)
    {
        *pi4NextIfIndex = i4IfIndex;
        return (SNMP_SUCCESS);
    }
    PPP_TRC (MGMT, "No Matching Index Found");
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppMplsOperStatus
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppMplsOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppMplsOperStatus (INT4 i4IfIndex, INT4 *pi4RetValPppMplsOperStatus)
{

    tMPLSCPIf          *pMplsIf;

    if ((pMplsIf = PppGetMPLSCPIfPtr (i4IfIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Index Found");
        return (SNMP_FAILURE);
    }

    *pi4RetValPppMplsOperStatus = pMplsIf->OperStatus;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppMplsAdminStatus
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppMplsAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppMplsAdminStatus (INT4 i4IfIndex, INT4 *pi4RetValPppMplsAdminStatus)
{

    tMPLSCPIf          *pMplsIf;

    if ((pMplsIf = PppGetMPLSCPIfPtr (i4IfIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Index Found");
        return (SNMP_FAILURE);
    }

    *pi4RetValPppMplsAdminStatus = pMplsIf->AdminStatus;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppMplsRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppMplsRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppMplsRowStatus (INT4 i4IfIndex, INT4 *pi4RetValPppMplsRowStatus)
{
    tMPLSCPIf          *pMplsIf;

    if ((pMplsIf = PppGetMPLSCPIfPtr (i4IfIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Index Found");
        return (SNMP_FAILURE);
    }
    *pi4RetValPppMplsRowStatus = ACTIVE;
    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppMplsAdminStatus
 Input       :  The Indices
                IfIndex

                The Object 
                setValPppMplsAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppMplsAdminStatus (INT4 i4IfIndex, INT4 i4SetValPppMplsAdminStatus)
{

    tMPLSCPIf          *pMplsIf;
    if ((pMplsIf = PppGetMPLSCPIfPtr (i4IfIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Index Found");
        return (SNMP_FAILURE);
    }
    switch (i4SetValPppMplsAdminStatus)
    {
        case ADMIN_OPEN:
            pMplsIf->AdminStatus = (UINT1) i4SetValPppMplsAdminStatus;
            MPLSCPEnableIf (pMplsIf);
            break;

        case ADMIN_CLOSE:
            pMplsIf->AdminStatus = (UINT1) i4SetValPppMplsAdminStatus;
            MPLSCPDisableIf (pMplsIf);
            break;
        default:
            break;
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppMplsRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                setValPppMplsRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppMplsRowStatus (INT4 i4IfIndex, INT4 i4SetValPppMplsRowStatus)
{
    tMPLSCPIf          *pMplsIf;

    switch (i4SetValPppMplsRowStatus)
    {
        case CREATE_AND_GO:
            if ((pMplsIf = PppCreateMPLSCPIfPtr (i4IfIndex)) == NULL)
            {
                PPP_TRC (MGMT, "Creation of Interface Failed");
                return (SNMP_FAILURE);
            }
            pMplsIf->AdminStatus = ADMIN_OPEN;
            pMplsIf->RowStatus = ACTIVE;
            break;
        case DESTROY:
            if ((pMplsIf = PppGetMPLSCPIfPtr (i4IfIndex)) == NULL)
            {
                PPP_TRC (MGMT, "Interface not created");
                return (SNMP_FAILURE);
            }
            PPP_TRC (PPP_MUST, " \t\tMPLSCP Interface Deleted\n");
            MPLSCPDisableIf (pMplsIf);
            MPLSCPDeleteIf (pMplsIf);
            break;
        default:
            break;
    }
    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppMplsAdminStatus
 Input       :  The Indices
                IfIndex

                The Object 
                testValPppMplsAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppMplsAdminStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                             INT4 i4TestValPppMplsAdminStatus)
{

    tMPLSCPIf          *pMplsIf;

    if ((pMplsIf = PppGetMPLSCPIfPtr (i4IfIndex)) == NULL)
    {
        PPP_TRC (MGMT, "No Matching Index Found");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((i4TestValPppMplsAdminStatus != ADMIN_OPEN)
        && (i4TestValPppMplsAdminStatus != ADMIN_CLOSE))
    {
        PPP_TRC (MGMT, "Wrong Value");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppMplsRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                testValPppMplsRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
        }
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppMplsRowStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                           INT4 i4TestValPppMplsRowStatus)
{

    tMPLSCPIf          *pMplsIf;

    switch (i4TestValPppMplsRowStatus)
    {
        case CREATE_AND_GO:
            if ((pMplsIf = PppGetMPLSCPIfPtr (i4IfIndex)) != NULL)
            {
                PPP_TRC (MGMT, "Interface Already Created");
                return (SNMP_FAILURE);
            }
            break;
        case DESTROY:
            if ((pMplsIf = PppGetMPLSCPIfPtr (i4IfIndex)) == NULL)
            {
                PPP_TRC (MGMT, "Interface not created");
                return (SNMP_FAILURE);
            }
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (SNMP_FAILURE);

            break;
    }
    return (SNMP_SUCCESS);

}
