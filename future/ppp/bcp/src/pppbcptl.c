/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppbcptl.c,v 1.2 2014/03/11 14:02:45 siva Exp $
 *
 * Description: Low Level Routines for BCP module.
 *
 *******************************************************************/
#include "pppsnmpm.h"
#include "bcpcom.h"
#include "snmccons.h"
#include "bcpexts.h"
#include "globexts.h"
#include "llproto.h"
#include "ppbcplow.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppExtBcpConfigLoctoRemMACAddress
 Input       :  The Indices
                PppExtBcpConfigIfIndex

                The Object 
                retValPppExtBcpConfigLoctoRemMACAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtBcpConfigLoctoRemMACAddress (INT4 i4PppExtBcpConfigIfIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pRetValPppExtBcpConfigLoctoRemMACAddress)
{
    tBCPIf             *pBcpPtr;

    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppExtBcpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    /* RFC007 *Str      =   pBcpPtr->BcpOptionsAckedByPeer.MACAddress;   */
    pRetValPppExtBcpConfigLoctoRemMACAddress->i4_Length = MAC_ADDR_LEN;
    MEMCPY (pRetValPppExtBcpConfigLoctoRemMACAddress->pu1_OctetList,
            pBcpPtr->BcpOptionsAckedByPeer.MACAddress,
            pRetValPppExtBcpConfigLoctoRemMACAddress->i4_Length);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppExtBcpConfigRemtoLocMACAddress
 Input       :  The Indices
                PppExtBcpConfigIfIndex

                The Object 
                retValPppExtBcpConfigRemtoLocMACAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtBcpConfigRemtoLocMACAddress (INT4 i4PppExtBcpConfigIfIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pRetValPppExtBcpConfigRemtoLocMACAddress)
{
    tBCPIf             *pBcpPtr;

    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppExtBcpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    /* RFC007 *Str      =   pBcpPtr->BcpOptionsAllowedForPeer.MACAddress;   */
    pRetValPppExtBcpConfigRemtoLocMACAddress->i4_Length = MAC_ADDR_LEN;
    MEMCPY (pRetValPppExtBcpConfigRemtoLocMACAddress->pu1_OctetList,
            pBcpPtr->BcpOptionsAllowedForPeer.MACAddress,
            pRetValPppExtBcpConfigRemtoLocMACAddress->i4_Length);
    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppExtBcpConfigLoctoRemMACAddress
 Input       :  The Indices
                PppExtBcpConfigIfIndex

                The Object 
                setValPppExtBcpConfigLoctoRemMACAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtBcpConfigLoctoRemMACAddress (INT4 i4PppExtBcpConfigIfIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pSetValPppExtBcpConfigLoctoRemMACAddress)
{
    tBCPIf             *pBcpPtr;
    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppExtBcpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    MEMCPY (&BCPCtrlBlk.LoctoRemMACAddress,
            pSetValPppExtBcpConfigLoctoRemMACAddress->pu1_OctetList,
            pSetValPppExtBcpConfigLoctoRemMACAddress->i4_Length);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppExtBcpConfigRemtoLocMACAddress
 Input       :  The Indices
                PppExtBcpConfigIfIndex

                The Object 
                setValPppExtBcpConfigRemtoLocMACAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtBcpConfigRemtoLocMACAddress (INT4 i4PppExtBcpConfigIfIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pSetValPppExtBcpConfigRemtoLocMACAddress)
{
    tBCPIf             *pBcpPtr;
    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppExtBcpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    MEMCPY (&BCPCtrlBlk.RemtoLocMACAddress,
            pSetValPppExtBcpConfigRemtoLocMACAddress->pu1_OctetList,
            pSetValPppExtBcpConfigRemtoLocMACAddress->i4_Length);
    return (SNMP_SUCCESS);

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppExtBcpConfigLoctoRemMACAddress
 Input       :  The Indices
                PppExtBcpConfigIfIndex

                The Object 
                testValPppExtBcpConfigLoctoRemMACAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtBcpConfigLoctoRemMACAddress (UINT4 *pu4ErrorCode,
                                            INT4 i4PppExtBcpConfigIfIndex,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pTestValPppExtBcpConfigLoctoRemMACAddress)
{
    if (PppGetIfPtr ((UINT4) i4PppExtBcpConfigIfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (SNMP_FAILURE);
    }
    if (pTestValPppExtBcpConfigLoctoRemMACAddress->i4_Length > 6)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppExtBcpConfigRemtoLocMACAddress
 Input       :  The Indices
                PppExtBcpConfigIfIndex

                The Object 
                testValPppExtBcpConfigRemtoLocMACAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtBcpConfigRemtoLocMACAddress (UINT4 *pu4ErrorCode,
                                            INT4 i4PppExtBcpConfigIfIndex,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pTestValPppExtBcpConfigRemtoLocMACAddress)
{
    if (PppGetIfPtr ((UINT4) i4PppExtBcpConfigIfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (SNMP_FAILURE);
    }
    if (pTestValPppExtBcpConfigRemtoLocMACAddress->i4_Length > 6)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}
