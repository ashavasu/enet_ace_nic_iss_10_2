/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppbcpel.c,v 1.4 2014/09/24 13:21:53 siva Exp $
 *
 * Description: Low Level Routines for BCP module.
 *
 *******************************************************************/
#include "pppsnmpm.h"
#include "bcpcom.h"
#include "snmccons.h"
#include "llproto.h"
#include "ppbcplow.h"
/* LOW LEVEL Routines for Table : PppExtBcpTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppExtBcpTable
 Input       :  The Indices
                PppExtBcpIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppExtBcpTable (INT4 i4PppExtBcpIfIndex)
{
    if (PppGetBCPIfPtr ((UINT4) i4PppExtBcpIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppExtBcpTable
 Input       :  The Indices
                PppExtBcpIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppExtBcpTable (INT4 *pi4PppExtBcpIfIndex)
{
    if (nmhGetFirstIndex (pi4PppExtBcpIfIndex, BcpInstance, FIRST_INST) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppExtBcpTable
 Input       :  The Indices
                PppExtBcpIfIndex
                nextPppExtBcpIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppExtBcpTable (INT4 i4PppExtBcpIfIndex,
                               INT4 *pi4NextPppExtBcpIfIndex)
{
    if (nmhGetNextIndex (&i4PppExtBcpIfIndex, BcpInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextPppExtBcpIfIndex = i4PppExtBcpIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppExtBcpSpanningTreeProtocol
 Input       :  The Indices
                PppExtBcpIfIndex

                The Object 
                retValPppExtBcpSpanningTreeProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtBcpSpanningTreeProtocol (INT4 i4PppExtBcpIfIndex,
                                     INT4
                                     *pi4RetValPppExtBcpSpanningTreeProtocol)
{
    tBCPIf             *pBcpPtr;
    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppExtBcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pBcpPtr->OperStatus == STATUS_UP)
    {
        *pi4RetValPppExtBcpSpanningTreeProtocol =
            pBcpPtr->BcpOptionsAckedByLocal.SpanningTreeProto;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }

}

/****************************************************************************
 Function    :  nmhGetPppExtBcpRemtoLocLanSegmentNumber
 Input       :  The Indices
                PppExtBcpIfIndex

                The Object 
                retValPppExtBcpRemtoLocLanSegmentNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtBcpRemtoLocLanSegmentNumber (INT4 i4PppExtBcpIfIndex,
                                         INT4
                                         *pi4RetValPppExtBcpRemtoLocLanSegmentNumber)
{
    tBCPIf             *pBcpPtr;
    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppExtBcpIfIndex)) == NULL)    /* dvk fix */
    {
        return (SNMP_FAILURE);
    }
    if (pBcpPtr->OperStatus == STATUS_UP)
    {
        *pi4RetValPppExtBcpRemtoLocLanSegmentNumber =
            (pBcpPtr->BcpOptionsAckedByLocal.BridgeLineId & LINE_ID_MASK) >> 4;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }

}

/****************************************************************************
 Function    :  nmhGetPppExtBcpRemtoLocBridgeNumber
 Input       :  The Indices
                PppExtBcpIfIndex

                The Object 
                retValPppExtBcpRemtoLocBridgeNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtBcpRemtoLocBridgeNumber (INT4 i4PppExtBcpIfIndex,
                                     INT4
                                     *pi4RetValPppExtBcpRemtoLocBridgeNumber)
{
    tBCPIf             *pBcpPtr;
    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppExtBcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pBcpPtr->OperStatus == STATUS_UP)
    {
        *pi4RetValPppExtBcpRemtoLocBridgeNumber =
            (pBcpPtr->BcpOptionsAckedByLocal.BridgeLineId & BRIDGE_ID_MASK);
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }

}

/* LOW LEVEL Routines for Table : PppExtBcpConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppExtBcpConfigTable
 Input       :  The Indices
                PppExtBcpConfigIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppExtBcpConfigTable (INT4 i4PppExtBcpConfigIfIndex)
{
    if (PppGetBCPIfPtr ((UINT4) i4PppExtBcpConfigIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppExtBcpConfigTable
 Input       :  The Indices
                PppExtBcpConfigIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppExtBcpConfigTable (INT4 *pi4PppExtBcpConfigIfIndex)
{
    if (nmhGetFirstIndex (pi4PppExtBcpConfigIfIndex, BcpInstance, FIRST_INST) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppExtBcpConfigTable
 Input       :  The Indices
                PppExtBcpConfigIfIndex
                nextPppExtBcpConfigIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppExtBcpConfigTable (INT4 i4PppExtBcpConfigIfIndex,
                                     INT4 *pi4NextPppExtBcpConfigIfIndex)
{
    if (nmhGetNextIndex (&i4PppExtBcpConfigIfIndex, BcpInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextPppExtBcpConfigIfIndex = i4PppExtBcpConfigIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppExtBcpConfigSpanningTreeProtocol
 Input       :  The Indices
                PppExtBcpConfigIfIndex

                The Object 
                retValPppExtBcpConfigSpanningTreeProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtBcpConfigSpanningTreeProtocol (INT4 i4PppExtBcpConfigIfIndex,
                                           INT4
                                           *pi4RetValPppExtBcpConfigSpanningTreeProtocol)
{
    tBCPIf             *pBcpPtr;
    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppExtBcpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppExtBcpConfigSpanningTreeProtocol =
        pBcpPtr->BcpOptionsDesired.SpanningTreeProto;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppExtBcpConfigLoctoRemLanSegmentNumber
 Input       :  The Indices
                PppExtBcpConfigIfIndex

                The Object 
                retValPppExtBcpConfigLoctoRemLanSegmentNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtBcpConfigLoctoRemLanSegmentNumber (INT4 i4PppExtBcpConfigIfIndex,
                                               INT4
                                               *pi4RetValPppExtBcpConfigLoctoRemLanSegmentNumber)
{
    tBCPIf             *pBcpPtr;
    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppExtBcpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppExtBcpConfigLoctoRemLanSegmentNumber =
        (pBcpPtr->BcpOptionsDesired.BridgeLineId & LINE_ID_MASK) >> 4;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppExtBcpConfigLoctoRemBridgeNumber
 Input       :  The Indices
                PppExtBcpConfigIfIndex

                The Object 
                retValPppExtBcpConfigLoctoRemBridgeNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtBcpConfigLoctoRemBridgeNumber (INT4 i4PppExtBcpConfigIfIndex,
                                           INT4
                                           *pi4RetValPppExtBcpConfigLoctoRemBridgeNumber)
{
    tBCPIf             *pBcpPtr;
    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppExtBcpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppExtBcpConfigLoctoRemBridgeNumber =
        (pBcpPtr->BcpOptionsDesired.BridgeLineId & BRIDGE_ID_MASK);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppExtBcpConfigBridgeId
 Input       :  The Indices
                PppExtBcpConfigIfIndex

                The Object 
                retValPppExtBcpConfigBridgeId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtBcpConfigBridgeId (INT4 i4PppExtBcpConfigIfIndex,
                               INT4 *pi4RetValPppExtBcpConfigBridgeId)
{
    tBCPIf             *pBcpPtr;
    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppExtBcpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppExtBcpConfigBridgeId =
        (pBcpPtr->BcpGSEM.pNegFlagsPerIf[BRIDGE_ID_IDX].
         FlagMask & ACKED_BY_PEER_MASK) ? BRIDGE_ID_TRUE : BRIDGE_ID_FALSE;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppExtBcpConfigIEEE802TagFrameSupport
 Input       :  The Indices
                PppExtBcpConfigIfIndex

                The Object 
                retValPppExtBcpConfigIEEE802TagFrameSupport
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtBcpConfigIEEE802TagFrameSupport (INT4 i4PppExtBcpConfigIfIndex,
                                             INT4
                                             *pi4RetValPppExtBcpConfigIEEE802TagFrameSupport)
{
    tBCPIf             *pBcpPtr;

    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppExtBcpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if ((pBcpPtr->BcpGSEM.pNegFlagsPerIf[IEEE_802_TAGGED_FRAME_IDX].FlagMask
         & FORCED_TO_REQUEST_NOT_SET) ||
        (pBcpPtr->BcpGSEM.pNegFlagsPerIf[IEEE_802_TAGGED_FRAME_IDX].FlagMask
         & ACKED_BY_PEER_NOT_SET))
    {
        *pi4RetValPppExtBcpConfigIEEE802TagFrameSupport = IEEE802_TAG_DISABLE;
    }
    else
    {
        *pi4RetValPppExtBcpConfigIEEE802TagFrameSupport = IEEE802_TAG_ENABLE;
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPppExtBcpConfigRowStatus
 Input       :  The Indices
                PppExtBcpConfigIfIndex

                The Object 
                retValPppExtBcpConfigRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtBcpConfigRowStatus (INT4 i4PppExtBcpConfigIfIndex,
                                INT4 *pi4RetValPppExtBcpConfigRowStatus)
{

    if (PppGetBCPIfPtr ((UINT4) i4PppExtBcpConfigIfIndex) != NULL)
    {
        *pi4RetValPppExtBcpConfigRowStatus = ACTIVE;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppExtBcpConfigSpanningTreeProtocol
 Input       :  The Indices
                PppExtBcpConfigIfIndex

                The Object 
                setValPppExtBcpConfigSpanningTreeProtocol
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtBcpConfigSpanningTreeProtocol (INT4 i4PppExtBcpConfigIfIndex,
                                           INT4
                                           i4SetValPppExtBcpConfigSpanningTreeProtocol)
{
    tBCPIf             *pBcpPtr;

    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppExtBcpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pBcpPtr->BcpOptionsDesired.SpanningTreeProto =
        (UINT1) (pBcpPtr->BcpOptionsDesired.
                 SpanningTreeProto |
                 BCPGetSpanTreeBitMask ((UINT1)
                                        (i4SetValPppExtBcpConfigSpanningTreeProtocol)));
    if (i4SetValPppExtBcpConfigSpanningTreeProtocol != 0)
    {
        /* 2878 changes : DESIRED_SET was being ORed before. Now, we will
         * negotitate with Spanning Tree option only if the Mgmt Inline
         * option is rejected
         */
        pBcpPtr->BcpGSEM.pNegFlagsPerIf[SPAN_TREE_IDX].FlagMask &=
            DESIRED_NOT_SET;
    }
    else
    {
        pBcpPtr->BcpGSEM.pNegFlagsPerIf[SPAN_TREE_IDX].FlagMask &=
            DESIRED_NOT_SET;
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppExtBcpConfigLoctoRemLanSegmentNumber
 Input       :  The Indices
                PppExtBcpConfigIfIndex

                The Object 
                setValPppExtBcpConfigLoctoRemLanSegmentNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtBcpConfigLoctoRemLanSegmentNumber (INT4 i4PppExtBcpConfigIfIndex,
                                               INT4
                                               i4SetValPppExtBcpConfigLoctoRemLanSegmentNumber)
{
    tBCPIf             *pBcpPtr;
    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppExtBcpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pBcpPtr->BcpOptionsDesired.BridgeLineId &= BRIDGE_ID_MASK;

    i4SetValPppExtBcpConfigLoctoRemLanSegmentNumber <<= 4;
    pBcpPtr->BcpOptionsDesired.BridgeLineId =
        (UINT1) (pBcpPtr->BcpOptionsDesired.
                 BridgeLineId |
                 ((UINT1) (i4SetValPppExtBcpConfigLoctoRemLanSegmentNumber)));

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppExtBcpConfigLoctoRemBridgeNumber
 Input       :  The Indices
                PppExtBcpConfigIfIndex

                The Object 
                setValPppExtBcpConfigLoctoRemBridgeNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtBcpConfigLoctoRemBridgeNumber (INT4 i4PppExtBcpConfigIfIndex,
                                           INT4
                                           i4SetValPppExtBcpConfigLoctoRemBridgeNumber)
{
    tBCPIf             *pBcpPtr;
    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppExtBcpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pBcpPtr->BcpOptionsDesired.BridgeLineId &= LINE_ID_MASK;
    pBcpPtr->BcpOptionsDesired.BridgeLineId =
        (UINT1) (pBcpPtr->BcpOptionsDesired.
                 BridgeLineId | ((UINT1)
                                 i4SetValPppExtBcpConfigLoctoRemBridgeNumber));

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppExtBcpConfigBridgeId
 Input       :  The Indices
                PppExtBcpConfigIfIndex

                The Object 
                setValPppExtBcpConfigBridgeId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtBcpConfigBridgeId (INT4 i4PppExtBcpConfigIfIndex,
                               INT4 i4SetValPppExtBcpConfigBridgeId)
{
    tBCPIf             *pBcpPtr;
    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppExtBcpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (i4SetValPppExtBcpConfigBridgeId == BRIDGE_ID_TRUE)
    {
        if (pBcpPtr->BcpGSEM.pNegFlagsPerIf[LINE_ID_IDX].FlagMask & DESIRED_SET)
        {
            return (SNMP_FAILURE);
        }
        else
        {
            pBcpPtr->BcpGSEM.pNegFlagsPerIf[BRIDGE_ID_IDX].FlagMask |=
                DESIRED_SET;
        }
    }
    else
    {
        pBcpPtr->BcpGSEM.pNegFlagsPerIf[BRIDGE_ID_IDX].FlagMask &=
            DESIRED_NOT_SET;
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppExtBcpConfigIEEE802TagFrameSupport
 Input       :  The Indices
                PppExtBcpConfigIfIndex

                The Object 
                setValPppExtBcpConfigIEEE802TagFrameSupport
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtBcpConfigIEEE802TagFrameSupport (INT4 i4PppExtBcpConfigIfIndex,
                                             INT4
                                             i4SetValPppExtBcpConfigIEEE802TagFrameSupport)
{
    tBCPIf             *pBcpPtr;

    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppExtBcpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (i4SetValPppExtBcpConfigIEEE802TagFrameSupport == IEEE802_TAG_ENABLE)
    {

        pBcpPtr->BcpGSEM.pNegFlagsPerIf[IEEE_802_TAGGED_FRAME_IDX].FlagMask |=
            DESIRED_SET | ALLOWED_FOR_PEER_SET;
    }
    else
    {
        pBcpPtr->BcpGSEM.pNegFlagsPerIf[IEEE_802_TAGGED_FRAME_IDX].FlagMask &=
            DESIRED_NOT_SET;
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppExtBcpConfigRowStatus
 Input       :  The Indices
                PppExtBcpConfigIfIndex

                The Object 
                setValPppExtBcpConfigRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtBcpConfigRowStatus (INT4 i4PppExtBcpConfigIfIndex,
                                INT4 i4SetValPppExtBcpConfigRowStatus)
{

    tBCPIf             *pBcpIf = NULL;

    switch (i4SetValPppExtBcpConfigRowStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            if ((pBcpIf =
                 PppCreateBCPIfPtr ((UINT4) i4PppExtBcpConfigIfIndex)) == NULL)
            {
                PPP_TRC1 (MGMT, "Creation of BCP Entry Failed. Index : [%d]",
                          i4PppExtBcpConfigIfIndex);
                return SNMP_FAILURE;
            }
            break;

        case DESTROY:
            if ((pBcpIf =
                 PppGetBCPIfPtr ((UINT4) i4PppExtBcpConfigIfIndex)) == NULL)
            {
                return SNMP_FAILURE;
            }
            PPP_TRC1 (MGMT, "Deleting BCP Entry. Index : [%d]",
                      i4PppExtBcpConfigIfIndex);
            BCPDisableIf (pBcpIf);
            BCPDeleteIf (pBcpIf);
            break;
        default:
            break;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppExtBcpConfigSpanningTreeProtocol
 Input       :  The Indices
                PppExtBcpConfigIfIndex

                The Object 
                testValPppExtBcpConfigSpanningTreeProtocol
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtBcpConfigSpanningTreeProtocol (UINT4 *pu4ErrorCode,
                                              INT4 i4PppExtBcpConfigIfIndex,
                                              INT4
                                              i4TestValPppExtBcpConfigSpanningTreeProtocol)
{
    tBCPIf             *pBcpPtr;

    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppExtBcpConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppExtBcpConfigSpanningTreeProtocol < 0
        || i4TestValPppExtBcpConfigSpanningTreeProtocol > 15)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppExtBcpConfigLoctoRemLanSegmentNumber
 Input       :  The Indices
                PppExtBcpConfigIfIndex

                The Object 
                testValPppExtBcpConfigLoctoRemLanSegmentNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtBcpConfigLoctoRemLanSegmentNumber (UINT4 *pu4ErrorCode,
                                                  INT4 i4PppExtBcpConfigIfIndex,
                                                  INT4
                                                  i4TestValPppExtBcpConfigLoctoRemLanSegmentNumber)
{
    tBCPIf             *pBcpPtr;

    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppExtBcpConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppExtBcpConfigLoctoRemLanSegmentNumber < 0
        || i4TestValPppExtBcpConfigLoctoRemLanSegmentNumber > 4065)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppExtBcpConfigLoctoRemBridgeNumber
 Input       :  The Indices
                PppExtBcpConfigIfIndex

                The Object 
                testValPppExtBcpConfigLoctoRemBridgeNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtBcpConfigLoctoRemBridgeNumber (UINT4 *pu4ErrorCode,
                                              INT4 i4PppExtBcpConfigIfIndex,
                                              INT4
                                              i4TestValPppExtBcpConfigLoctoRemBridgeNumber)
{

    tBCPIf             *pBcpPtr;

    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppExtBcpConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppExtBcpConfigLoctoRemBridgeNumber < 0
        || i4TestValPppExtBcpConfigLoctoRemBridgeNumber > 15)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppExtBcpConfigBridgeId
 Input       :  The Indices
                PppExtBcpConfigIfIndex

                The Object 
                testValPppExtBcpConfigBridgeId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtBcpConfigBridgeId (UINT4 *pu4ErrorCode,
                                  INT4 i4PppExtBcpConfigIfIndex,
                                  INT4 i4TestValPppExtBcpConfigBridgeId)
{

    tBCPIf             *pBcpPtr;

    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppExtBcpConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppExtBcpConfigBridgeId != 1
        && i4TestValPppExtBcpConfigBridgeId != 2)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppExtBcpConfigIEEE802TagFrameSupport
 Input       :  The Indices
                PppExtBcpConfigIfIndex

                The Object 
                testValPppExtBcpConfigIEEE802TagFrameSupport
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtBcpConfigIEEE802TagFrameSupport (UINT4 *pu4ErrorCode,
                                                INT4 i4PppExtBcpConfigIfIndex,
                                                INT4
                                                i4TestValPppExtBcpConfigIEEE802TagFrameSupport)
{

    tBCPIf             *pBcpPtr;

    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppExtBcpConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((i4TestValPppExtBcpConfigIEEE802TagFrameSupport != 1) &&
        (i4TestValPppExtBcpConfigIEEE802TagFrameSupport != 2))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppExtBcpConfigRowStatus
 Input       :  The Indices
                PppExtBcpConfigIfIndex

                The Object 
                testValPppExtBcpConfigRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtBcpConfigRowStatus (UINT4 *pu4ErrorCode,
                                   INT4 i4PppExtBcpConfigIfIndex,
                                   INT4 i4TestValPppExtBcpConfigRowStatus)
{

    tBCPIf             *pBcpIf = NULL;

    switch (i4TestValPppExtBcpConfigRowStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            if ((pBcpIf =
                 PppGetBCPIfPtr ((UINT4) i4PppExtBcpConfigIfIndex)) != NULL)
            {
                PPP_TRC (MGMT, "Interface Already Created");
                return SNMP_FAILURE;
            }
            break;
        case DESTROY:
            if ((pBcpIf =
                 PppGetBCPIfPtr ((UINT4) i4PppExtBcpConfigIfIndex)) == NULL)
            {
                PPP_TRC (MGMT, "Interface Not Created");
                return SNMP_FAILURE;
            }
            break;
        case ACTIVE:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
            break;
    }
    return SNMP_SUCCESS;

}
