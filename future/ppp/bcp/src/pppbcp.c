/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppbcp.c,v 1.3 2014/03/11 14:02:45 siva Exp $
 *
 * Description:This file contains callback routines of BCP module.
 *             It also contains routines for the input BCP packet 
 *             processing.
 *
 *******************************************************************/

#include "bcpcom.h"
#include "genexts.h"
#include "gsemdefs.h"
#include "gcpdefs.h"
#include "lcpdefs.h"
#include "globexts.h"
#include "genproto.h"
/*********************************************************************/
/* Variables  Used  by  the BCP Module */
/*********************************************************************/

UINT1               MACAddressAssignFlag;
UINT1               TinygramCompAvailable;
UINT1               LanIdAvailable;

UINT2               BcpAllocCounter;
UINT2               BcpFreeCounter;
UINT2               BcpIfFreeCounter;

tBCPCtrlBlk         BCPCtrlBlk;

tGSEMCallbacks      BcpGSEMCallback = {
    BCPUp,
    BCPDown,
    NULL,
    BCPFinished,
    NCPRxData,
    NCPTxData,
    NULL,
    NULL
};

typedef struct
{
    UINT4               DiscHeader;
    UINT1               Value[MAX_MAC_SUPPORT_DISC_VALUES];
    UINT1               u1Rsvd1;
}
tMACSupportDiscVal;

typedef struct
{
    UINT4               DiscHeader;
    UINT1               Value[MAX_TINYGRAM_DISC_VALUES];
    UINT2               u2Rsvd;
}
tTinygramDiscVal;

typedef struct
{
    UINT4               DiscHeader;
    UINT1               Value[MAX_SPAN_PROT_DISC_VALUES];
    UINT1               u1Rsvd;
    UINT2               u2Rsvd;
}
tSpanTreeDiscVal;

typedef struct
{
    UINT4               DiscHeader;
    UINT1               Value[MAX_IEEE_802_TAG_FRAME_DISC_VALUES];
    UINT2               u2Rsvd;
}
tIEEE802TagFrameDiscVal;

tMACSupportDiscVal  MACSupportDiscVal =
    { MAX_MAC_SUPPORT_DISC_VALUES, {0, 1, 2, 3, 4, 11, 12}, 0 };
tTinygramDiscVal    TinygramDiscVal = { MAX_TINYGRAM_DISC_VALUES, {1, 2}, 0 };
tSpanTreeDiscVal    SpanTreeDiscVal =
    { MAX_SPAN_PROT_DISC_VALUES, {0, 1, 2, 3, 4}, 0, 0 };

tIEEE802TagFrameDiscVal IEEE802TagFrameDiscVal =
    { MAX_IEEE_802_TAG_FRAME_DISC_VALUES, {1, 2}, 0 };

/* CAS supports only 802.3/Ethernet */
tMACTable           MACTypeTable[] = {
    {1, MAC_TYPE_ACKED_BY_PEER_FLAG, 0},
    {2, 0, 0},
    {3, 0, 0},
    {4, 0, 0},
    {11, 0, 0},
    {12, 0, 0}
};

/* Makefile changes - missing braces around initialiser */
tRangeInfo          BridgeIdRange = { {0}, {0xffff} };
tRangeInfo          LineIdRange = { {0}, {0xffff} };

tOptHandlerFuns     BCPBridgeIdHandlingFuns = {
    BCPProcessBridgeIdConfReq,
    BCPProcessBridgeIdConfNak,
    NULL,
    BCPReturnBridgeIdPtr,
    NULL
};

tOptHandlerFuns     BCPLineIdHandlingFuns = {
    BCPProcessLineIdConfReq,
    BCPProcessLineIdConfNak,
    NULL,
    BCPReturnLineIdPtr,
    NULL
};

tOptHandlerFuns     BCPMACSupportHandlingFuns = {
    BCPProcessMACSupportConfReq,
    BCPProcessMACSupportConfNak,
    BCPProcessMACSupportConfRej,
    BCPReturnMACSupportPtr,
    BCPAddMACSupportSubOpt
};

tOptHandlerFuns     BCPTinygramHandlingFuns = {
    BCPProcessTinygramConfReq,
    BCPProcessTinygramConfNak,
    NULL,
    BCPReturnTinygramPtr,
    NULL
};

tOptHandlerFuns     BCPMACAddressHandlingFuns = {
    BCPProcessMACAddressConfReq,
    BCPProcessMACAddressConfNak,
    NULL,
    BCPReturnMACAddressPtr,
    NULL
};

tOptHandlerFuns     BCPSpanningTreeProtoHandlingFuns = {
    BCPProcessSpanningTreeProtoConfReq,
    BCPProcessSpanningTreeProtoConfNak,
    BCPProcessSpanningTreeProtoConfRej,
    BCPReturnSpanningTreeProtoPtr,
    BCPAddSpanningTreeProtoSubOpt
};

tOptHandlerFuns     BCPIEEE802TaggedFrameHandlingFuns = {
    BCPProcessIEEE802TagFrameConfReq,
    NULL,
    NULL,
    BCPReturnIEEE802TagFramePtr,
    NULL
};

tOptHandlerFuns     BCPMgmtInlineHandlingFuns = {
    NULL,
    NULL,
    BCPProcessMgmtInlineConfRej,
    NULL,
    NULL
};

/* Makefile changes - missing braces around initializer */
tGenOptionInfo      BCPGenOptionInfo[] = {

    {BRIDGE_ID_OPTION, RANGE, BYTE_LEN_2, 4, &BCPBridgeIdHandlingFuns,
     {&BridgeIdRange}, NOT_SET, 0, 0},
    {LINE_ID_OPTION, RANGE, BYTE_LEN_2, 4, &BCPLineIdHandlingFuns,
     {&LineIdRange}, NOT_SET, 0, 0},
    {MAC_SUPPORT_OPTION, DISCRETE, BYTE_LEN_1, 3, &BCPMACSupportHandlingFuns,
     {(VOID *) &MACSupportDiscVal}, SET, 0, 0},
    {TINYGRAM_OPTION, DISCRETE, BYTE_LEN_1, 3, &BCPTinygramHandlingFuns,
     {(VOID *) &TinygramDiscVal}, NOT_SET, 0, 0},
    {DUMMY_OPTION, BOOLEAN_CATEGORY, BOOL_TYPE, 2, NULL, {NULL}, NOT_SET, 0, 0},
    {MAC_ADDR_OPTION, STRING_TYPE, STRING_TYPE, 8, &BCPMACAddressHandlingFuns,
     {NULL}, NOT_SET, 0, 0},
    {SPAN_TREE_OPTION, DISCRETE, BYTE_LEN_1, 2 /* Was 6 - a bug */ ,
     &BCPSpanningTreeProtoHandlingFuns, {(VOID *) &SpanTreeDiscVal}, NOT_SET,
     0, 0},
    {IEEE_802_TAGGED_FRAME_OPTION, DISCRETE, BYTE_LEN_1, 3,
     &BCPIEEE802TaggedFrameHandlingFuns, {(VOID *) &IEEE802TagFrameDiscVal},
     NOT_SET, 0, 0},
    {MANAGEMENT_INLINE_OPTION, BOOLEAN_CATEGORY, BOOL_TYPE, 2,
     &BCPMgmtInlineHandlingFuns,
     {NULL}, NOT_SET, 0, 0}

};

/* The BridgeID and Line ID options are for Source Routing bridges. 
 * CAS supports only Transparent Bridging and hence does not support
 * these options */
tOptNegFlagsPerIf   BCPOptPerIntf[] = {
    {BRIDGE_ID_OPTION, 0, 0},
    {LINE_ID_OPTION, 0, 0},
    {MAC_SUPPORT_OPTION, DESIRED_SET | ALLOWED_FOR_PEER_SET | ALLOW_REJECT_SET,
     0},
    {TINYGRAM_OPTION,
     ALLOWED_FOR_PEER_SET | ALLOW_REJECT_SET | ALLOW_FORCED_NAK_SET, 0},
    {DUMMY_OPTION, 0, 0},
    {MAC_ADDR_OPTION,
     ALLOW_REJECT_SET | ALLOW_FORCED_NAK_SET, 0},
    {SPAN_TREE_OPTION,
     ALLOWED_FOR_PEER_SET | ALLOW_REJECT_SET | ALLOW_FORCED_NAK_SET, 0},
    /*DESIRED_SET and ALLOWED_FOR_PEER_SET will be ORed through SNMP ll */
    {IEEE_802_TAGGED_FRAME_OPTION,
     DESIRED_SET | ALLOWED_FOR_PEER_SET | ALLOW_REJECT_SET, 0},
    {MANAGEMENT_INLINE_OPTION,
     ALLOWED_FOR_PEER_SET | DESIRED_SET | ALLOW_REJECT_SET, 0}
    /*  {MANAGEMENT_INLINE_OPTION,
       ALLOW_REJECT_SET, 0} */

};

/***********************************************************************/
/*                      INTERFACE  FUNCTIONS                           */
/***********************************************************************
*  Function Name : BCPCreateIf
*  Description   :
*          This procedure creates an entry in the BCP interface data 
*  base corresponding to IfId. It also initializes the created entry to the 
*  default values  by calling the BCPinit() function.
*  Parameter(s)  :
*         pIf -    pointer to the PPP interface structure
*  Return Values : 
*      pointer to the newly created  interface structure , if the creation is 
*        success
*      NULL if it fails to create the interface.
*********************************************************************/
tBCPIf             *
BCPCreateIf (tPPPIf * pIf)
{
    tBCPIf             *pBcpPtr;

    if ((pBcpPtr = (tBCPIf *) ALLOC_BCP_IF ()) != NULL)
    {
        if (BCPInit (pBcpPtr) != NOT_OK)
        {
            pBcpPtr->BcpGSEM.pIf = pIf;
            return (pBcpPtr);
        }
        else
        {

            PPP_TRC (ALL_FAILURE, "Error: BCP Init failure .\n");
            FREE_BCP_IF (pBcpPtr);
            return (NULL);
        }
    }
    return (NULL);
}

/*********************************************************************
*  Function Name : BCPDeleteIf
*  Description   :
*               This procedure deletes an BCP interface  entry from the 
*  database and deallocates memory corresponding to the interface.
*  Parameter(s)  :
*         pBcpPtr -  points to  the BCP interface structure to be deleted.
*  Return Values : VOID
*********************************************************************/
VOID
BCPDeleteIf (tBCPIf * pBcpPtr)
{
    pBcpPtr->BcpGSEM.pIf->pBcpPtr = NULL;
    GSEMDelete (&(pBcpPtr->BcpGSEM));
    FREE_BCP_IF (pBcpPtr);
    return;
}

/*********************************************************************
*  Function Name : BCPEnable
*  Description   :
*              This function is called when there is an  BCP Admin Open 
*  for an interface from the SNMP It  triggers an OPEN event to the GSEM and 
*  passes the BCP tGSEM corresponds to this interface as parameter.
*  Parameter(s)  :
*            pBcpPtr    -  points to the BCP interface structure to be enabled
*  Return Values : VOID
*********************************************************************/
VOID               *
BCPEnable (tBCPIf * pBcpPtr)
{
    BCPEnableIf (pBcpPtr, &BCPCtrlBlk);
    return (VOID *) &BCPCtrlBlk;
}

/*********************************************************************
*  Function Name : BCPEnableIf
*  Description   :
*              This function is called when there is an  BCP Admin Open 
*  for an interface from the SNMP It  triggers an OPEN event to the GSEM and 
*  passes the BCP tGSEM corresponds to this interface as parameter.
*  Parameter(s)  :
*            pBcpPtr    -  points to the BCP interface structure to be enabled
*             pBcpPtrBlk -  points to the BCPCtrlBlk 
*  Return Values : VOID
*********************************************************************/
VOID
BCPEnableIf (tBCPIf * pBcpPtr, tBCPCtrlBlk * pBcpCtrlBlk)
{
    UINT1               Index;
    tPPPIf             *pIf;

    if (pBcpPtr->BcpGSEM.pIf->BundleFlag == BUNDLE)
    {
        SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
        {
            if (pIf->MPInfo.MemberInfo.pBundlePtr != NULL
                && pIf->MPInfo.MemberInfo.pBundlePtr == pBcpPtr->BcpGSEM.pIf)
            {
                pIf->pBcpPtr = pBcpPtr;
            }
        }
    }

    MEMCPY (pBcpPtr->BcpOptionsDesired.MACAddress,
            pBcpCtrlBlk->LoctoRemMACAddress, MAC_ADDR_LEN);
    MEMCPY (pBcpPtr->BcpOptionsAllowedForPeer.MACAddress,
            pBcpCtrlBlk->RemtoLocMACAddress, MAC_ADDR_LEN);

    pBcpPtr->AdminStatus = ADMIN_OPEN;

    /* 
       Check whether the link is already in the Operational state. If so, 
       then indicate the GSEM about the link UP 
     */

    if ((pBcpPtr->BcpGSEM.CurrentState != REQSENT)
        || (pBcpPtr->BcpGSEM.CurrentState != ACKRCVD)
        || (pBcpPtr->BcpGSEM.CurrentState != ACKSENT))
    {
        MEMCPY (&pBcpPtr->BcpOptionsAckedByPeer, &pBcpPtr->BcpOptionsDesired,
                sizeof (tBCPOptions));
        for (Index = 0; Index < MAX_BCP_OPT_TYPES; Index++)
        {
            if (pBcpPtr->BcpGSEM.pNegFlagsPerIf[Index].FlagMask & DESIRED_SET)
            {
                pBcpPtr->BcpGSEM.pNegFlagsPerIf[Index].FlagMask |=
                    ACKED_BY_PEER_SET;
            }
            else
            {
                pBcpPtr->BcpGSEM.pNegFlagsPerIf[Index].FlagMask &=
                    ACKED_BY_PEER_NOT_SET;
            }
        }
    }

    GSEMRun (&pBcpPtr->BcpGSEM, OPEN);

    if ((pBcpPtr->BcpGSEM.pIf->LcpStatus.LinkAvailability == LINK_AVAILABLE)
        && (pBcpPtr->BcpGSEM.CurrentState != CLOSED))
    {
        GSEMRun (&pBcpPtr->BcpGSEM, PPP_UP);
    }

    GSEMRestartIfNeeded (&pBcpPtr->BcpGSEM);

    return;
}

/*********************************************************************
*  Function Name : BCPDisableIf
*  Description   :
*          This function is called when there is an Admin Close for an 
*  interface from the SNMP It triggers a CLOSE event to the GSEM and passes 
*  the BCP GSEMStructure corresponds to this interface as a parameter. 
*  Parameter(s)  :
*            pBcpPtr - points to  the BCP interface structure to be  disabled
*  Return Values : VOID
*********************************************************************/
VOID
BCPDisableIf (tBCPIf * pBcpPtr)
{
    pBcpPtr->AdminStatus = ADMIN_CLOSE;
    GSEMRun (&pBcpPtr->BcpGSEM, CLOSE);
    return;
}

/*********************************************************************
*  Function Name : BCPInit
*  Description   :
*              This function initializes the BCP GSEM and  sets 
*  the  BCP configuration option variables to their default values. It is 
*  invoked by the BCPCreateIf() function.
*  Parameter(s)  :
*        pBcpPtr -   points to the BCP interface structure
*  Return Values : OK/NOT_OK 
*********************************************************************/
INT1
BCPInit (tBCPIf * pBcpPtr)
{

    BridgeIdRange.MinVal.ShortVal = 0;
    BridgeIdRange.MaxVal.ShortVal = 0xffff;
    LineIdRange.MinVal.ShortVal = 0;
    LineIdRange.MaxVal.ShortVal = 0xffff;

    BCPOptionInit (&pBcpPtr->BcpOptionsDesired);
    BCPOptionInit (&pBcpPtr->BcpOptionsAllowedForPeer);
    BCPOptionInit (&pBcpPtr->BcpOptionsAckedByPeer);
    BCPOptionInit (&pBcpPtr->BcpOptionsAckedByLocal);

    /* CAS supports only 802 1D spanning tree */
    pBcpPtr->BcpOptionsDesired.SpanningTreeProto = IEEE_8021D_SPAN_TREE_PROTO;
    MEMCPY (pBcpPtr->MACTableEntry, MACTypeTable,
            MAX_MAC_TYPES * sizeof (tMACTable));

    pBcpPtr->AdminStatus = STATUS_DOWN;
    pBcpPtr->OperStatus = STATUS_DOWN;

    if (GSEMInit (&pBcpPtr->BcpGSEM, MAX_BCP_OPT_TYPES) == NOT_OK)
    {
        return (NOT_OK);
    }
    pBcpPtr->BcpGSEM.Protocol = BCP_PROTOCOL;
    pBcpPtr->BcpGSEM.pCallbacks = &BcpGSEMCallback;
    pBcpPtr->BcpGSEM.pGenOptInfo = BCPGenOptionInfo;

    MEMCPY (pBcpPtr->BcpGSEM.pNegFlagsPerIf, &BCPOptPerIntf,
            (sizeof (tOptNegFlagsPerIf) * MAX_BCP_OPT_TYPES));

    return (OK);
}

/***********************************************************************/
/*                      INTERNAL  FUNCTIONS                            */
/***********************************************************************
*  Function Name : BCPOptionInit
*  Description   :
*               This function initializes the BCPOptions structure and is
*  called by the BCPInit function.
*  Parameter(s)  :
*       pBCPOpt   -  points to the BCPOption structure  to be initialized.
*  Return Value  :
*********************************************************************/
VOID
BCPOptionInit (tBCPOptions * pBCPOpt)
{
    pBCPOpt->BridgeLineId = BRIDGE_ID_DISABLE;
    pBCPOpt->SpanningTreeProto = NO_SPAN_TREE_PROTO;
    BZERO (pBCPOpt->MACAddress, MAC_ADDR_LEN);
}

/*********************************************************************
*  Function Name : BCPUp
*  Description   :
*              This function is called once the network level configuration  
*  option is negotiated  successfully. This function indicates  to the higher 
*  layers  that the BCP is up.  The pUp  field of  the BCPGSEMCallback points 
*  to this function and is called from the GSEM Module.
*  Parameter(s)  :
*            pBcpGSEM  - pointer to the BCP tGSEM 
*                                   corresponding to the current interface.
*  Return Values : VOID 
*********************************************************************/
VOID
BCPUp (tGSEM * pBcpGSEM)
{

    tBCPIf             *pBCPIf;
    pBCPIf = (tBCPIf *) pBcpGSEM->pIf->pBcpPtr;

    pBCPIf->OperStatus = STATUS_UP;

    PPPHLINotifyProtStatusToHL (pBcpGSEM->pIf, BCP_PROTOCOL, PPP_UP);
    return;
}

/*********************************************************************
*  Function Name : BCPDown
*  Description   :
*          This function is invoked  when the BCP protocol is leaving the 
*  OPENED state. It  indicates the higher layers  about the operational status 
*  of the BCP.  The pDown  field of  the BCPGSEMCallback structure  points to 
*  this function and is called from the GSEM Module.
*  Parameter(s)  :
*            pBcpGSEM  - pointer to the BCP tGSEM 
*                       corresponding to the current interface.
*  Return Values : VOID 
*********************************************************************/
VOID
BCPDown (tGSEM * pBcpGSEM)
{
    tPPPIf             *pIf;
    tBCPIf             *pBCPIf;
    pIf = pBcpGSEM->pIf;
    pBCPIf = (tBCPIf *) pBcpGSEM->pIf->pBcpPtr;

    PPPHLINotifyProtStatusToHL (pIf, BCP_PROTOCOL, PPP_DOWN);
    pBCPIf->OperStatus = STATUS_DOWN;
    BCPFinished (pBcpGSEM);

    MEMCPY (&pBCPIf->BcpOptionsAckedByPeer, &pBCPIf->BcpOptionsDesired,
            sizeof (tBCPOptions));

    return;
}

/*********************************************************************
*  Function Name :  BCPFinished
*  Description   :
*    This function is invoked when the higher layer is done with 
*  the link and the link is no longer needed. This invokes  procedures for 
*  terminating the link.
*         The pFinished  field of the GSEMCallback structure points 
*  to this function and is called from the GSEM Module.
*  Parameter(s)  :  
*            pGSEM  - pointer to BCP tGSEM corresponding 
*  to current interface
*  Return Value  :    VOID
*********************************************************************/
VOID
BCPFinished (tGSEM * pGSEM)
{
    tBCPIf             *pBcpPtr;
    UINT1               Index;
    pBcpPtr = pGSEM->pIf->pBcpPtr;
    MEMCPY (&pBcpPtr->BcpOptionsAckedByPeer, &pBcpPtr->BcpOptionsDesired,
            sizeof (tBCPOptions));
    for (Index = 0; Index < MAX_BCP_OPT_TYPES; Index++)
    {
        if (pGSEM->pNegFlagsPerIf[Index].FlagMask & DESIRED_SET)
        {
            pGSEM->pNegFlagsPerIf[Index].FlagMask |= ACKED_BY_PEER_SET;
        }
        else
        {
            pGSEM->pNegFlagsPerIf[Index].FlagMask &= ACKED_BY_PEER_NOT_SET;
        }
    }
    return;
}

/***********************************************************************
    OPTION SPECIFIC FUNCTIONS FOR PROCESSING CONF_REQ
************************************************************************/
/*********************************************************************
*  Function Name : BCPProcessBridgeIdConfReq
*  Description   :
*        This function is invoked to process the Bridge Identification 
*    option present in the CONF_REQ and to construct appropriate response
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer
*     tOptVal    -  Option Value received  to be processed
*     Offset     -  indicates the place in the outgoing buffer at which 
*                   the option value is appended
*  PresenceFlag  -  No need to check PresenceFlag.
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer
*********************************************************************/

INT1
BCPProcessBridgeIdConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal,
                           UINT2 Offset, UINT1 PresenceFlag)
{
    tBCPIf             *pBcpPtr;

    PPP_UNUSED (pInPkt);
    PPP_UNUSED (PresenceFlag);
    pBcpPtr = (tBCPIf *) pGSEM->pIf->pBcpPtr;

    if (pRecdOptFlags[LINE_ID_IDX] == SET)
    {
        PPP_TRC (CONTROL_PLANE, "Line Id option included !! Pkt Discarded ");
        return (DISCARD);
    }

    pBcpPtr->BcpOptionsAckedByLocal.BridgeLineId = OptVal.ShortVal;

    if ((pBcpPtr->BcpOptionsAckedByPeer.BridgeLineId & BRIDGE_ID_MASK) >
        (OptVal.ShortVal & BRIDGE_ID_MASK))
    {
        pBcpPtr->BcpOptionsAckedByLocal.BridgeLineId &= LINE_ID_MASK;
        pBcpPtr->BcpOptionsAckedByLocal.BridgeLineId =
            (UINT2) ((pBcpPtr->BcpOptionsAckedByLocal.BridgeLineId) | (pBcpPtr->
                                                                       BcpOptionsAckedByPeer.
                                                                       BridgeLineId
                                                                       &
                                                                       BRIDGE_ID_MASK));
        ASSIGN2BYTE (pGSEM->pOutParam, Offset,
                     pBcpPtr->BcpOptionsAckedByLocal.BridgeLineId);
        return (BYTE_LEN_2);
    }

    pBcpPtr->BcpOptionsAckedByPeer.BridgeLineId &= LINE_ID_MASK;
    pBcpPtr->BcpOptionsAckedByPeer.BridgeLineId =
        (UINT2) ((pBcpPtr->BcpOptionsAckedByPeer.BridgeLineId) | (OptVal.
                                                                  ShortVal &
                                                                  BRIDGE_ID_MASK));
    return (ACK);
}

/*********************************************************************
*  Function Name : BCPProcessLineIdConfReq
*  Description   :
*        This function is invoked to process the Line Identification
*    option present in the CONF_REQ and to construct appropriate response
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer
*     tOptVal    -  Option Value received  to be processed
*     Offset     -  indicates the place in the outgoing buffer at which 
*                   the option value is appended
*  PresenceFlag  -  indicates whether the option is present in the recd.
*                   CONF_REQ packet
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer
************************************************************************/

INT1
BCPProcessLineIdConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal,
                         UINT2 Offset, UINT1 PresenceFlag)
{
    tBCPIf             *pBcpPtr;

    PPP_UNUSED (pInPkt);
    PPP_UNUSED (PresenceFlag);
    pBcpPtr = (tBCPIf *) pGSEM->pIf->pBcpPtr;

    if (pRecdOptFlags[BRIDGE_ID_IDX] == SET)
    {
        return (DISCARD);
    }

    pBcpPtr->BcpOptionsAckedByLocal.BridgeLineId = OptVal.ShortVal;
    if ((pBcpPtr->BcpOptionsAckedByPeer.BridgeLineId & LINE_ID_MASK) >
        (OptVal.ShortVal & LINE_ID_MASK))
    {
        pBcpPtr->BcpOptionsAckedByLocal.BridgeLineId &= BRIDGE_ID_MASK;
        pBcpPtr->BcpOptionsAckedByLocal.BridgeLineId |=
            (UINT2) ((pBcpPtr->BcpOptionsAckedByLocal.BridgeLineId) | (pBcpPtr->
                                                                       BcpOptionsAckedByPeer.
                                                                       BridgeLineId
                                                                       &
                                                                       LINE_ID_MASK));
        ASSIGN2BYTE (pGSEM->pOutParam, Offset,
                     pBcpPtr->BcpOptionsAckedByPeer.BridgeLineId);
        return (BYTE_LEN_2);
    }

    pBcpPtr->BcpOptionsAckedByPeer.BridgeLineId &= BRIDGE_ID_MASK;
    pBcpPtr->BcpOptionsAckedByPeer.BridgeLineId |=
        (UINT2) ((pBcpPtr->BcpOptionsAckedByPeer.BridgeLineId) | (OptVal.
                                                                  ShortVal &
                                                                  LINE_ID_MASK));
    return (ACK);
}

/*********************************************************************
*  Function Name : BCPProcessMACSupportConfReq
*  Description   :
*          This function processes the MAC Support option in the 
*  CONF_REQ packet and builds appropriate response.
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer
*     tOptVal    -  Option Value received  to be processed
*     Offset     -  indicates the place in the outgoing buffer at which 
*                   the option value is appended
*  PresenceFlag  -  indicates whether the option is present in the recd.
*                   CONF_REQ packet
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer
*********************************************************************/
INT1
BCPProcessMACSupportConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal,
                             UINT2 Offset, UINT1 PresenceFlag)
{
    tBCPIf             *pBcpPtr;
    UINT1               Index;

    PPP_UNUSED (pInPkt);
    PPP_UNUSED (PresenceFlag);
    PPP_UNUSED (Offset);
    pBcpPtr = (tBCPIf *) pGSEM->pIf->pBcpPtr;

    for (Index = 0; Index < 6; Index++)
    {
        if (OptVal.CharVal == pBcpPtr->MACTableEntry[Index].MACType)
        {
            pBcpPtr->MACTableEntry[Index].NegStatusFlags |=
                MAC_TYPE_ACKED_BY_LOCAL_FLAG;
            return (ACK);
        }
    }
    return (DISCARD);
}

/*********************************************************************
*  Function Name : BCPProcessTinygramConfReq
*  Description   :
*        This function is invoked to process the Tinygram Compression  
*    option present in the CONF_REQ and to construct appropriate response
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer
*     tOptVal    -  Option Value received  to be processed
*     Offset     -  indicates the place in the outgoing buffer at which 
*                   the option value is appended
*  PresenceFlag  -  indicates whether the option is present in the recd.
*                   CONF_REQ packet
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer
************************************************************************/

INT1
BCPProcessTinygramConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal,
                           UINT2 Offset, UINT1 PresenceFlag)
{
    tBCPIf             *pBcpPtr;

    PPP_UNUSED (pInPkt);
    pBcpPtr = (tBCPIf *) pGSEM->pIf->pBcpPtr;

    if (PresenceFlag == NOT_SET)
    {
        if (pBcpPtr->BcpGSEM.pNegFlagsPerIf[TINYGRAM_IDX].
            FlagMask & ALLOWED_FOR_PEER_MASK)
        {
            ASSIGN1BYTE (pGSEM->pOutParam, Offset, TINYGRAM_ENABLE);
            return (BYTE_LEN_1);
        }
        return (0);
    }
    if (OptVal.CharVal == TINYGRAM_DISABLE)
    {
        pRecdOptFlags[TINYGRAM_IDX] = NOT_SET;
        pBcpPtr->BcpGSEM.pNegFlagsPerIf[TINYGRAM_IDX].FlagMask &=
            FORCED_TO_REQUEST_NOT_SET;
    }
    return (ACK);
}

/*********************************************************************
*  Function Name : BCPProcessMACAddressConfReq
*  Description   :
*        This function is invoked to process the MAC Address
*    option present in the CONF_REQ and to construct appropriate response
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer
*     tOptVal    -  Option Value received  to be processed
*     Offset     -  indicates the place in the outgoing buffer at which 
*                   the option value is appended
*  PresenceFlag  -  indicates whether the option is present in the recd.
*                   CONF_REQ packet
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer
************************************************************************/

INT1
BCPProcessMACAddressConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal,
                             UINT2 Offset, UINT1 PresenceFlag)
{
    tBCPIf             *pBcpPtr;

    PPP_UNUSED (pInPkt);
    PPP_UNUSED (PresenceFlag);
    pBcpPtr = (tBCPIf *) pGSEM->pIf->pBcpPtr;

    if (MEMCMP (OptVal.StrVal, pNullAddr, MAC_ADDR_LEN) == 0)
    {
        ASSIGNSTR (pGSEM->pOutParam,
                   pBcpPtr->BcpOptionsAllowedForPeer.MACAddress, Offset,
                   MAC_ADDR_LEN);
        return (MAC_ADDR_LEN);
    }

    MEMCPY (pBcpPtr->BcpOptionsAckedByLocal.MACAddress, OptVal.StrVal,
            MAC_ADDR_LEN);
    return (ACK);
}

/*********************************************************************
*  Function Name : BCPProcessSpanningTreeProtoConfReq
*  Description   :
*        This function is invoked to process the Spanning Tree Protocol
*    option present in the CONF_REQ and to construct appropriate response
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer
*     tOptVal    -  Option Value received  to be processed
*     Offset     -  indicates the place in the outgoing buffer at which 
*                   the option value is appended
*  PresenceFlag  -  indicates whether the option is present in the recd.
*                   CONF_REQ packet
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer
************************************************************************/

INT1
BCPProcessSpanningTreeProtoConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt,
                                    tOptVal OptVal, UINT2 Offset,
                                    UINT1 PresenceFlag)
{
    tBCPIf             *pBcpPtr;
    UINT1               SpanTreeMask,
        AckedByPeerValues, RequestedBitMask, OptionVal;
    INT1                retLen, ExtLen, Index;

    PPP_TRC2 (CONTROL_PLANE, "Offset=%d, PresenceFlag=%d", Offset,
              PresenceFlag);

    pBcpPtr = (tBCPIf *) pGSEM->pIf->pBcpPtr;
    ExtLen = (INT1) ExtractedLength;
    retLen = 0;

    if (OptVal.CharVal == 0)
    {
        if (ExtLen == 3)
        {
            pBcpPtr->BcpOptionsAckedByLocal.SpanningTreeProto = 0;
        }
        else
        {
            return (DISCARD);
        }
    }
    else
    {
        SpanTreeMask = (UINT1) BCPGetSpanTreeBitMask (OptVal.CharVal);
        AckedByPeerValues = pBcpPtr->BcpOptionsAckedByPeer.SpanningTreeProto;
        RequestedBitMask = 0;
        Index = 1;
        if (AckedByPeerValues == 0)
        {
            return (DISCARD);
        }
        else
        {
            if (ExtLen == 3)
            {
                if (AckedByPeerValues & SpanTreeMask)
                {
                    pBcpPtr->BcpOptionsAckedByLocal.SpanningTreeProto |=
                        SpanTreeMask;
                }
                else
                {
                    pGSEM->OptionFlags |= CLOSE_OPTION;
                }
            }
            else
            {
                OptionVal = OptVal.CharVal;
                while (ExtLen >= 3)
                {
                    RequestedBitMask |= SpanTreeMask;
                    EXTRACT1BYTE (pInPkt, OptionVal);
                    SpanTreeMask = (UINT1) BCPGetSpanTreeBitMask (OptionVal);
                    ExtLen--;
                }

                if ((AckedByPeerValues & RequestedBitMask) != RequestedBitMask)
                {
                    while (Index <= 4)
                    {
                        if ((AckedByPeerValues & 0x01)
                            && (RequestedBitMask & 0x01))
                        {
                            ASSIGN1BYTE (pGSEM->pOutParam, Offset,
                                         (UINT1) Index);
                            retLen++;
                            Offset++;
                        }
                        Index++;
                        AckedByPeerValues >>= 1;
                        RequestedBitMask >>= 1;
                    }
                    if (retLen == 0)
                    {
                        pGSEM->OptionFlags |= CLOSE_OPTION;
                    }
                    else
                    {
                        return (retLen);
                    }
                }
            }
        }
    }
    return (ACK);
}

INT1
BCPProcessIEEE802TagFrameConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt,
                                  tOptVal OptVal, UINT2 Offset,
                                  UINT1 PresenceFlag)
{
    tBCPIf             *pBcpPtr;

    PPP_UNUSED (pInPkt);
    PPP_UNUSED (OptVal);
    PPP_UNUSED (PresenceFlag);
    PPP_UNUSED (Offset);
    pBcpPtr = (tBCPIf *) pGSEM->pIf->pBcpPtr;

    if (OptVal.CharVal == IEEE802_TAG_ENABLE)
    {
        pRecdOptFlags[IEEE_802_TAGGED_FRAME_IDX] = SET;
    }

    if (OptVal.CharVal == IEEE802_TAG_DISABLE)
    {
        pRecdOptFlags[IEEE_802_TAGGED_FRAME_IDX] = NOT_SET;
        pBcpPtr->BcpGSEM.pNegFlagsPerIf[IEEE_802_TAGGED_FRAME_IDX].FlagMask &=
            FORCED_TO_REQUEST_NOT_SET;
    }
    return (ACK);
}

/***********************************************************************
    OPTION SPECIFIC FUNCTIONS FOR PROCESSING CONF_NAK
************************************************************************/
/*********************************************************************
*  Function Name : BCPProcessBridgeIdConfNak 
*  Description   :
*        This function is invoked to process the Bridge Identification
*  option present in the CONF_NAK and to construct appropriate response 
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*  Return Value  :  OK/NOT_OK/CLOSE 
*********************************************************************/
INT1
BCPProcessBridgeIdConfNak (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal)
{
    tBCPIf             *pBcpPtr;

    PPP_UNUSED (pInPkt);
    pBcpPtr = (tBCPIf *) pGSEM->pIf->pBcpPtr;

    if ((pGSEM->pNegFlagsPerIf[BRIDGE_ID_IDX].FlagMask & ACKED_BY_PEER_MASK)
        && ((pBcpPtr->BcpOptionsAckedByPeer.BridgeLineId & BRIDGE_ID_MASK) <
            (OptVal.ShortVal & BRIDGE_ID_MASK)))
    {
        pBcpPtr->BcpOptionsAckedByPeer.BridgeLineId &= LINE_ID_MASK;
        pBcpPtr->BcpOptionsAckedByPeer.BridgeLineId =
            (UINT2) ((pBcpPtr->BcpOptionsAckedByPeer.BridgeLineId) | (OptVal.
                                                                      ShortVal &
                                                                      BRIDGE_ID_MASK));
    }
    return (OK);
}

/*********************************************************************
*  Function Name : BCPProcessLineIdConfNak 
*  Description   :
*        This function is invoked to process the Line Identification
*  option present in the CONF_NAK and to construct appropriate response 
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*  Return Value  :  OK/NOT_OK/CLOSE 
*********************************************************************/
INT1
BCPProcessLineIdConfNak (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal)
{
    tBCPIf             *pBcpPtr;

    PPP_UNUSED (pInPkt);
    pBcpPtr = (tBCPIf *) pGSEM->pIf->pBcpPtr;

    if ((pGSEM->pNegFlagsPerIf[LINE_ID_IDX].FlagMask & ACKED_BY_PEER_MASK)
        && ((pBcpPtr->BcpOptionsAckedByPeer.BridgeLineId & LINE_ID_MASK) <
            (OptVal.ShortVal & LINE_ID_MASK)))
    {
        pBcpPtr->BcpOptionsAckedByPeer.BridgeLineId &= BRIDGE_ID_MASK;
        pBcpPtr->BcpOptionsAckedByPeer.BridgeLineId |=
            (UINT2) ((pBcpPtr->BcpOptionsAckedByPeer.BridgeLineId) | (OptVal.
                                                                      ShortVal &
                                                                      LINE_ID_MASK));
    }
    return (OK);
}

/*********************************************************************
*  Function Name : BCPProcessMACSupportConfNak 
*  Description   :
*        This function is invoked to process the MAC Support
*  option present in the CONF_NAK and to construct appropriate response 
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*  Return Value  :  OK/NOT_OK/CLOSE 
*********************************************************************/
INT1
BCPProcessMACSupportConfNak (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal)
{
    PPP_UNUSED (pInPkt);
    PPP_UNUSED (OptVal);
    PPP_UNUSED (pGSEM);
    return (DISCARD);
}

/*********************************************************************
*  Function Name : BCPProcessTinygramConfNak 
*  Description   :
*        This function is invoked to process the Tinygram Compression
*  option present in the CONF_NAK and to construct appropriate response 
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*  Return Value  :  OK/NOT_OK/CLOSE 
*********************************************************************/
INT1
BCPProcessTinygramConfNak (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal)
{

    PPP_UNUSED (pInPkt);
    PPP_UNUSED (OptVal);
    if (pGSEM->pNegFlagsPerIf[TINYGRAM_IDX].FlagMask & ACKED_BY_PEER_MASK)
    {
        return (DISCARD);
    }
    return (OK);
}

/*********************************************************************
*  Function Name : BCPProcessMACAddressConfNak 
*  Description   :
*        This function is invoked to process the MAC Address
*  option present in the CONF_NAK and to construct appropriate response 
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*  Return Value  :  OK/NOT_OK/CLOSE 
*********************************************************************/
INT1
BCPProcessMACAddressConfNak (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal)
{
    tBCPIf             *pBcpPtr;

    PPP_UNUSED (pInPkt);
    pBcpPtr = (tBCPIf *) pGSEM->pIf->pBcpPtr;

    if ((MEMCMP
         (pBcpPtr->BcpOptionsAckedByPeer.MACAddress, pNullAddr,
          MAC_ADDR_LEN) == 0)
        && ((OptVal.StrVal[0] | MAC_ADDR_MULTICAST_FLAG) == NOT_SET))
    {
        MEMCPY (pBcpPtr->BcpOptionsAckedByPeer.MACAddress, OptVal.StrVal,
                MAC_ADDR_LEN);
        return (OK);
    }
    return (DISCARD);
}

/*********************************************************************
*  Function Name : BCPProcessSpanningTreeProtoConfNak 
*  Description   :
*        This function is invoked to process the Spanning Tree Protocol
*  option present in the CONF_NAK and to construct appropriate response 
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*  Return Value  :  OK/NOT_OK/CLOSE 
*********************************************************************/
INT1
BCPProcessSpanningTreeProtoConfNak (tGSEM * pGSEM, t_MSG_DESC * pInPkt,
                                    tOptVal OptVal)
{
    tBCPIf             *pBcpPtr;
    UINT1               SpanTreeMask, AckedByPeerValues, OptionVal;
    INT1                ExtLen;

    pBcpPtr = (tBCPIf *) pGSEM->pIf->pBcpPtr;
    ExtLen = (INT1) ExtractedLength;
    SpanTreeMask = (UINT1) BCPGetSpanTreeBitMask (OptVal.CharVal);
    AckedByPeerValues = pBcpPtr->BcpOptionsAckedByPeer.SpanningTreeProto;
    pBcpPtr->BcpOptionsAckedByPeer.SpanningTreeProto = 0;

    while (ExtLen >= 3)
    {
        if (AckedByPeerValues & SpanTreeMask)
        {
            pBcpPtr->BcpOptionsAckedByPeer.SpanningTreeProto |= SpanTreeMask;
        }
        if (ExtLen == 3)
        {
            break;
        }
        EXTRACT1BYTE (pInPkt, OptionVal);
        SpanTreeMask = (UINT1) BCPGetSpanTreeBitMask (OptionVal);
        ExtLen--;
    };
    return (OK);
}

/***********************************************************************
    OPTION SPECIFIC FUNCTIONS FOR ADDING SUB_OPTIONS IN THE CONF_REQ
************************************************************************/
/*********************************************************************
*  Function Name : BCPAddMACSupportSubOpt
*  Description   :
*     This function is used to add MAC Support suboption(s).
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     tOptVal    -  Option Value received  to be processed
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer
*********************************************************************/
INT1
BCPAddMACSupportSubOpt (tGSEM * pGSEM, tOptVal OptVal, t_MSG_DESC * pOutBuf)
{
    tBCPIf             *pBcpPtr;
    INT1                Count, Length;
    INT2                Index;

    PPP_UNUSED (OptVal);
    Count = 0;
    Length = 0;
    pBcpPtr = (tBCPIf *) pGSEM->pIf->pBcpPtr;

    for (Index = 0; Index < 6; Index++)
    {
        if (pBcpPtr->MACTableEntry[Index].
            NegStatusFlags & MAC_TYPE_ACKED_BY_PEER_FLAG)
        {
            Count++;
            if (Count != 1)
            {
                APPEND1BYTE (pOutBuf, MAC_SUPPORT_OPTION);
                APPEND1BYTE (pOutBuf, 3);
                APPEND1BYTE (pOutBuf, pBcpPtr->MACTableEntry[Index].MACType);
                Length = (INT1) (Length + 3);
            }
        }
    }
    return (Length);
}

/*********************************************************************
*  Function Name : BCPAddSpanningTreeProtoSubOpt
*  Description   :
*     This function is used to add Spanning Tree Protocol suboption(s).
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     tOptVal    -  Option Value received  to be processed
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer
*********************************************************************/
INT1
BCPAddSpanningTreeProtoSubOpt (tGSEM * pGSEM, tOptVal OptVal,
                               t_MSG_DESC * pOutBuf)
{
    tBCPIf             *pBcpPtr;
    INT1                Length;
    UINT1               Index;
    UINT1               SpanProtoVal;

    Index = (UINT1) (OptVal.CharVal + 1);
    Length = 0;
    pBcpPtr = (tBCPIf *) pGSEM->pIf->pBcpPtr;
    SpanProtoVal =
        (UINT1) ((pBcpPtr->BcpOptionsAckedByPeer.SpanningTreeProto) >> OptVal.
                 CharVal);

    while (Index <= 4)
    {
        if (SpanProtoVal & CHECK_BIT_MASK)
        {
            APPEND1BYTE (pOutBuf, Index);
            Length++;
        }
        SpanProtoVal >>= 1;
        Index++;
    }
    return (Length);
}

/***********************************************************************
    OPTION SPECIFIC FUNCTIONS FOR RETURNING THE ADDRESS OF OPTION DATA
************************************************************************/
/*********************************************************************
*  Function Name : BCPReturnBridgeIdPtr 
*  Description   :
*     This function returns the address of the location where the   
*     Bridge Id value to be included in the CONF_REQ, is stored.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*     OptLen     -  used to return the length of the option value (in
*                   case the option is of 'STRING' type)
*  Return Value  :  returns the option data address
*********************************************************************/
VOID               *
BCPReturnBridgeIdPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    tBCPIf             *pBCPIf;

    PPP_UNUSED (OptLen);
    pBCPIf = (tBCPIf *) pGSEM->pIf->pBcpPtr;
    return (&pBCPIf->BcpOptionsAckedByPeer.BridgeLineId);
}

/*********************************************************************
*  Function Name : BCPReturnLineIdPtr 
*  Description   :
*     This function returns the address of the location where the   
*     Line Id value to be included in the CONF_REQ, is stored.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*     OptLen     -  used to return the length of the option value (in
*                   case the option is of 'STRING' type)
*  Return Value  :  returns the option data address
*********************************************************************/
VOID               *
BCPReturnLineIdPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    tBCPIf             *pBCPIf;

    PPP_UNUSED (OptLen);
    pBCPIf = (tBCPIf *) pGSEM->pIf->pBcpPtr;
    return (&pBCPIf->BcpOptionsAckedByPeer.BridgeLineId);
}

/*********************************************************************
*  Function Name : BCPReturnMACSupportPtr 
*  Description   :
*     This function returns the address of the location where the MAC
*  Support value to be included in the CONF_REQ, is stored.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*     OptLen     -  used to return the length of the option value (in
*                   case the option is of 'STRING' type)
*  Return Value  :  returns the option data address
*********************************************************************/
VOID               *
BCPReturnMACSupportPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    /* Makefile changes - array subscript used as char */
    INT2                Index;
    tBCPIf             *pBCPIf;

    PPP_UNUSED (OptLen);
    pBCPIf = (tBCPIf *) pGSEM->pIf->pBcpPtr;
    for (Index = 0; Index < 6; Index++)
    {
        if (pBCPIf->MACTableEntry[Index].
            NegStatusFlags & MAC_TYPE_ACKED_BY_PEER_FLAG)
        {
            GenReturnData = pBCPIf->MACTableEntry[Index].MACType;
            return ((UINT1 *) &GenReturnData);
        }
    }
    return NULL;
}

/*********************************************************************
*  Function Name : BCPReturnTinygramPtr 
*  Description   :
*     This function returns the address of the location where the  
*     Tinygram Option value to be included in the CONF_REQ, is stored.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*     OptLen     -  used to return the length of the option value (in
*                   case the option is of 'STRING' type)
*  Return Value  :  returns the option data address
*********************************************************************/
VOID               *
BCPReturnTinygramPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    PPP_UNUSED (OptLen);
    PPP_UNUSED (pGSEM);
    GenReturnData = TINYGRAM_ENABLE;
    return ((UINT1 *) &GenReturnData);
}

/*********************************************************************
*  Function Name : BCPReturnMACAddressPtr 
*  Description   :
*     This function returns the address of the location where the MAC  
*     Address value to be included in the CONF_REQ, is stored.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*     OptLen     -  used to return the length of the option value (in
*                   case the option is of 'STRING' type)
*  Return Value  :  returns the option data address
*********************************************************************/
VOID               *
BCPReturnMACAddressPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    tBCPIf             *pBCPIf;

    *OptLen = MAC_ADDR_LEN;
    pBCPIf = (tBCPIf *) pGSEM->pIf->pBcpPtr;
    return (pBCPIf->BcpOptionsAckedByPeer.MACAddress);
}

/*********************************************************************
*  Function Name : BCPReturnSpanningTreeProtoPtr 
*  Description   :
*     This function returns the address of the location where the   
*     Spanning Tree Protocol value to be included in the CONF_REQ, is stored.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*     OptLen     -  used to return the length of the option value (in
*                   case the option is of 'STRING' type)
*  Return Value  :  returns the option data address
*********************************************************************/
VOID               *
BCPReturnSpanningTreeProtoPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    INT1                Index;
    UINT1               SpanProtoVal;
    tBCPIf             *pBCPIf;

    PPP_UNUSED (OptLen);
    pBCPIf = (tBCPIf *) pGSEM->pIf->pBcpPtr;
    Index = 1;
    SpanProtoVal = pBCPIf->BcpOptionsAckedByPeer.SpanningTreeProto;

    while (Index <= 4)
    {
        if (SpanProtoVal & CHECK_BIT_MASK)
        {
            GenReturnData = (UINT4) Index;
            return ((UINT1 *) &GenReturnData);
        }
        SpanProtoVal >>= 1;
        Index++;
    }
    return NULL;
}

VOID               *
BCPReturnIEEE802TagFramePtr (tGSEM * pGSEM, UINT1 *OptLen)
{

    PPP_UNUSED (OptLen);
    if (pGSEM->pNegFlagsPerIf[IEEE_802_TAGGED_FRAME_IDX].
        FlagMask & ACKED_BY_PEER_MASK)
    {
        GenReturnData = IEEE802_TAG_ENABLE;
    }
    else
    {
        GenReturnData = IEEE802_TAG_DISABLE;
    }
    return ((VOID *) &GenReturnData);

}

/***********************************************************************
    OPTION SPECIFIC FUNCTIONS FOR PROCESSING CONF_REJ
************************************************************************/

/*********************************************************************
*  Function Name : BCPProcessMACSupportConfRej
*  Description   :
*    This function sets the default value of the rejected option.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*  Return Value  : OK
*********************************************************************/
INT1
BCPProcessMACSupportConfRej (tGSEM * pGSEM)
{
    /* Makefile change - array subscript has type char */
    INT2                Index;
    tBCPIf             *pBCPIf;

    pBCPIf = (tBCPIf *) pGSEM->pIf->pBcpPtr;
    for (Index = 0; Index < 6; Index++)
    {
        pBCPIf->MACTableEntry[Index].NegStatusFlags &=
            MAC_TYPE_ACKED_BY_LOCAL_FLAG;
    }
    return (OK);
}

/*********************************************************************
*  Function Name : BCPProcessSpanningTreeProtoConfRej
*  Description   :
*    This function sets the default value of the rejected option.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*  Return Value  : OK
*********************************************************************/

INT1
BCPProcessSpanningTreeProtoConfRej (tGSEM * pGSEM)
{
    tBCPIf             *pBCPIf;

    pBCPIf = (tBCPIf *) pGSEM->pIf->pBcpPtr;
    pBCPIf->BcpOptionsAckedByPeer.SpanningTreeProto = 0x00;
    return (OK);
}

INT1
BCPProcessMgmtInlineConfRej (tGSEM * pGSEM)
{

    pGSEM->pNegFlagsPerIf[MANAGEMENT_INLINE_IDX].FlagMask &=
        ACKED_BY_PEER_NOT_SET;

    /* Try to negotiate the Spanning Tree Protocol Option */
    pGSEM->pNegFlagsPerIf[SPAN_TREE_IDX].FlagMask |= ACKED_BY_PEER_SET;
    GSEMRun (pGSEM, RCN);
    return (OK);
}

/*********************************************************************
*  Function Name : BCPCopyOptions 
*  Description   :
*    This function is used to copy the BCP options.
*  Input         :
*    pGSEM        -     Points to the GSEM structure
*    Flag        -     Specifies whether to alloc or free the temporary 
*                    variable.
*    PktType        -    Specifies whether the function is called from request
*                    processing or Nak processing.                  
*  Return Value        :
*                    DISCARD on allocation error, OK otherwise.
*********************************************************************/
INT1
BCPCopyOptions (tGSEM * pGSEM, UINT1 Flag, UINT1 PktType)
{
    UINT1               Size;
    tBCPOptions        *pTemp;
    tBCPIf             *pBCPIf;

    Size = sizeof (tBCPOptions);
    pBCPIf = (tBCPIf *) pGSEM->pIf->pBcpPtr;

    if (PktType == CONF_REQ_CODE)
    {
        pTemp = &pBCPIf->BcpOptionsAckedByLocal;
    }
    else
    {
        pTemp = &pBCPIf->BcpOptionsAckedByPeer;
    }

    if (Flag == NOT_SET)
    {
        if ((pCopyPtr = (UINT1 *) ALLOC_STR (Size)) == NULL)
        {
            PRINT_MEM_ALLOC_FAILURE;
            return (DISCARD);
        }
        MEMCPY (pCopyPtr, (UINT1 *) pTemp, Size);
        if (PktType == CONF_REQ_CODE)
        {
            BCPOptionInit (&pBCPIf->BcpOptionsAckedByLocal);
        }
    }
    else
    {
        if (Flag == SET)
        {
            MEMCPY (pTemp, (tBCPOptions *) pCopyPtr, Size);
        }
        FREE_STR (pCopyPtr);
    }
    return (OK);
}

/*********************************************************************
*  Function Name : BCPGetSEMPtr
*  Description   :
*    This function returns the pointer to the BCP GSEM taking the tPPPIf
*   structure as a parameter.
*  Input         :
*       pIf -   Points to the PPP Interface structure.
*  Return Value  :
*           Returns a pointer to the BCP GSEM.
*********************************************************************/
tGSEM              *
BCPGetSEMPtr (tPPPIf * pIf)
{
    tBCPIf             *pBCPIf;

    pBCPIf = (tBCPIf *) pIf->pBcpPtr;
    if (pBCPIf != NULL)
    {
        PPP_TRC (CONTROL_PLANE, "BCP:");
        return (&pBCPIf->BcpGSEM);
    }
    return (NULL);
}

/*********************************************************************
*  Function Name : BCPSpanIeeeGetSEMPtr
*  Description   :
*    This function returns the pointer to the BCP GSEM taking the tPPPIf
*   structure as a parameter.
*  Input         :
*       pIf -   Points to the PPP Interface structure.
*  Return Value  :
*           Returns a pointer to the BCP GSEM.
*********************************************************************/
tGSEM              *
BCPSpanIeeeGetSEMPtr (tPPPIf * pIf)
{
    tBCPIf             *pBCPIf;

    pBCPIf = (tBCPIf *) pIf->pBcpPtr;

    if (pBCPIf != NULL)
    {
        if (((pBCPIf->BcpGSEM.pNegFlagsPerIf[MANAGEMENT_INLINE_IDX].FlagMask &
              ACKED_BY_PEER_MASK) == NOT_SET))
        {
            /* Commented in order to interop with the Alcatel Omni switch.
             * The Omni access rejects the Spanning tree Proto option
             * but still sends the spanning tree BPDUs in the old format
             *
             if ((pBCPIf->BcpOptionsAckedByLocal.SpanningTreeProto !=
             IEEE_8021D_SPAN_TREE_PROTO))
             {
             return (NULL);
             }
             */
        }

        PPP_TRC (CONTROL_PLANE, "BCP:");
        return (&pBCPIf->BcpGSEM);
    }
    return (NULL);
}

/*********************************************************************
*  Function Name : BCPIbmSrcRtGetSEMPtr
*  Description   :
*    This function returns the pointer to the BCP GSEM taking the tPPPIf
*   structure as a parameter.
*  Input         :
*       pIf -   Points to the PPP Interface structure.
*  Return Value  :
*           Returns a pointer to the BCP GSEM.
*********************************************************************/
tGSEM              *
BCPIbmSrcRtGetSEMPtr (tPPPIf * pIf)
{
    tBCPIf             *pBCPIf;

    pBCPIf = (tBCPIf *) pIf->pBcpPtr;
    if (pBCPIf != NULL)
    {
        if (((pBCPIf->BcpGSEM.pNegFlagsPerIf[MANAGEMENT_INLINE_IDX].FlagMask &
              ACKED_BY_PEER_MASK) == NOT_SET))
        {
            if (pBCPIf->BcpOptionsAckedByLocal.SpanningTreeProto !=
                IBM_SRC_ROUTE_SPAN_TREE_PROTO)
            {
                return (NULL);
            }
        }

        PPP_TRC (CONTROL_PLANE, "BCP:");
        return (&pBCPIf->BcpGSEM);
    }
    return (NULL);
}

/*********************************************************************
*  Function Name : BCPDecLanGetSEMPtr
*  Description   :
*    This function returns the pointer to the BCP GSEM taking the tPPPIf
*   structure as a parameter.
*  Input         :
*       pIf -   Points to the PPP Interface structure.
*  Return Value  :
*           Returns a pointer to the BCP GSEM.
*********************************************************************/
tGSEM              *
BCPDecLanGetSEMPtr (tPPPIf * pIf)
{
    tBCPIf             *pBCPIf;

    pBCPIf = (tBCPIf *) pIf->pBcpPtr;
    if (pBCPIf != NULL)

    {
        if (((pBCPIf->BcpGSEM.pNegFlagsPerIf[MANAGEMENT_INLINE_IDX].FlagMask &
              ACKED_BY_PEER_MASK) == NOT_SET))
        {
            if (pBCPIf->BcpOptionsAckedByLocal.SpanningTreeProto !=
                DEC_LAN_BRIDGE_SPAN_TREE_PROTO)
            {
                return (NULL);
            }
        }

        PPP_TRC (CONTROL_PLANE, "BCP:");
        return (&pBCPIf->BcpGSEM);
    }
    return (NULL);
}

/*********************************************************************
*  Function Name : BCPGetSpanTreeBitMask
*  Description   :
*    This function returns the Span Tree Bit Mask for the given 
*    Option value.
*  Input         :
*    MACOptVal - Option Value for which Bit Mask has to be found
*  Return Value  :
*           Returns the Spanning Tree Bit Mask.
*********************************************************************/

UINT2
BCPGetSpanTreeBitMask (UINT1 Index)
{
    UINT2               retval;
    /* Makefile change - retval may be uninitialised */
    retval = 0;

    switch (Index)
    {
        case IEEE_8021D_SPAN_TREE_PROTO:
            retval = 0x01;
            break;
        case IEEE_8021G_SPAN_TREE_PROTO:
            retval = 0x02;
            break;
        case IBM_SRC_ROUTE_SPAN_TREE_PROTO:
            retval = 0x04;
            break;
        case DEC_LAN_BRIDGE_SPAN_TREE_PROTO:
            retval = 0x08;
            break;
        default:
            break;

    }
    return (retval);
}

/*********************************************************************
*  Function Name : BCPInit
*  Description   :
*    This function allocates memory for the tBCPCtrlBlk data structure. 
*  Input         : VOID
*  Return Value  :
*           Returns the memory location allocated to BCPCtrlBlk
*********************************************************************/

VOID
BcpInit (VOID)
{
    BZERO (&BCPCtrlBlk, sizeof (tBCPCtrlBlk));
}

/*********************************************************************
*  Function Name : ALLOC_BCP_IF
*  Description   :
*    This function allocates memory to BCPIf pointer and initialises 
*    it with zero.    
*  Input         : VOID
*  Return Value  :
*           Returns the pointer to tBCPIf structure
*********************************************************************/
tBCPIf             *
ALLOC_BCP_IF ()
{
    tBCPIf             *pBcpPtr = NULL;

    if ((pBcpPtr = (tBCPIf *) CALLOC (1, sizeof (tBCPIf))) != NULL)
    {
        BcpAllocCounter++;
        return pBcpPtr;
    }
    return NULL;
}
