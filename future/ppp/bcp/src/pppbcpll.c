/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppbcpll.c,v 1.5 2014/12/16 10:49:11 siva Exp $
 *
 * Description: Low Level Routines for BCP module.
 *
 *******************************************************************/
#include "pppsnmpm.h"
#include "bcpcom.h"
#include "snmccons.h"
#include "bcpexts.h"
#include "globexts.h"
#include "llproto.h"
#include "npapi.h"
#include "pppnp.h"
#include "stdbcpcli.h"
INT1
CheckAndGetBCPMacPtr (tPPPIf * pIf, UINT4 *n1, UINT4 *n2, UINT4 SecondIndex)
{

    UINT4               Count;
    tBCPIf             *pBcpPtr;

    pBcpPtr = (tBCPIf *) pIf->pBcpPtr;

    if (pBcpPtr != NULL)
    {
        for (Count = 0; Count < 6; Count++)
        {
            if ((pBcpPtr->MACTableEntry[Count].MACType >= SecondIndex)
                && (pBcpPtr->MACTableEntry[Count].MACType < *n2))
            {
                *n2 = pBcpPtr->MACTableEntry[Count].MACType;
                *n1 = pIf->LinkInfo.IfIndex;
            }
        }
    }
    return (PPP_SNMP_OK);
}

INT1
CheckAndGetBCPPtr (tPPPIf * pIf)
{

    if (pIf->pBcpPtr != NULL)
    {
        return (PPP_SNMP_OK);
    }
    return (PPP_SNMP_ERR);
}

/*********************************************************************
*  Function Name : PppGetBCPIfPtr
Description 
ThiS FUNction is invoked to get the pointer to tBCP
structure present in the tPPPIf Interface structu
*  Parameter(s)  :
*        Index
*  Return Value  : 
*        pointer to tBCPIf
*
*********************************************************************/
tBCPIf             *
PppGetBCPIfPtr (Index)
     UINT4               Index;
{
    tPPPIf             *pIf;

    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if ((pIf != NULL) && (pIf->LinkInfo.IfIndex == Index))
            return (pIf->pBcpPtr);
    }
    return ((tBCPIf *) NULL);
}

/*******************************************************************/
tBCPIf             *
PppCreateBCPIfPtr (Index)
     UINT4               Index;
{
    tPPPIf             *pIf;

    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if ((pIf != NULL) && (pIf->LinkInfo.IfIndex == Index))
        {
            if (pIf->BundleFlag != BUNDLE
                && pIf->MPInfo.MemberInfo.pBundlePtr != NULL)
            {
                return (NULL);
            }
            if (pIf->pBcpPtr == NULL)
            {
                if ((pIf->pBcpPtr = BCPCreateIf (pIf)) == NULL)
                {
                    return (NULL);
                }
            }
            return (pIf->pBcpPtr);
        }
    }
    return (NULL);
}

/*********************************************************************
*  Function Name : SNMPGetBCPMACTablePtr
*  Description   :
*        This function is invoked to get the pointer to tMACTable 
*        structure present in the tPPPIf Interface structure.
*  Parameter(s)  :
*        Index, MacType
*  Return Value  : 
*        pointer to tMACTable
*
*********************************************************************/
tMACTable          *
SNMPGetBCPMACTablePtr (Index1, Index2)
     UINT4               Index1;
     UINT4               Index2;
{
    INT2                Count;
    tPPPIf             *pIf;
    tBCPIf             *pBcpPtr;

/***** SKR001 in the following If PppGetPppIfPtr is modified to PppGetIfPtr */
    if (((pIf = PppGetIfPtr (Index1)) != NULL) && (pIf->pBcpPtr != NULL))
    {
        pBcpPtr = (tBCPIf *) pIf->pBcpPtr;
        for (Count = 0; Count < 6; Count++)
        {
            if (pBcpPtr->MACTableEntry[Count].MACType == (UINT1) Index2)
            {
                return (&pBcpPtr->MACTableEntry[Count]);
            }
        }
    }
    return ((tMACTable *) NULL);
}

/*******************************************************************/
tMACTable          *
SNMPGetOrCreateBCPMACTablePtr (Index1, Index2)
     UINT4               Index1;
     UINT4               Index2;
{
    tPPPIf             *pIf;
    tBCPIf             *pBcpPtr = NULL;
    INT2                Count;

/***** SKR001 in the following If PppGetPppIfPtr is modified to PppGetIfPtr */
#ifdef NPAPI_WANTED
    if (Index2 != 1)
    {
        if (PppNpNonEthernetMacTypesSupported () == FNP_FALSE)
        {
            return ((tMACTable *) NULL);
        }
    }
#endif

    if (((pIf = PppGetIfPtr (Index1)) != NULL) && (pIf->pBcpPtr != NULL))
    {
        pBcpPtr = (tBCPIf *) pIf->pBcpPtr;
        for (Count = 0; Count < 6; Count++)
        {
            if (pBcpPtr->MACTableEntry[Count].MACType == (UINT1) Index2)
            {
                return (&pBcpPtr->MACTableEntry[Count]);
            }
        }
    }
    if (pIf != NULL)
    {
        if ((pIf->pBcpPtr == NULL) &&
            ((pIf->pBcpPtr = BCPCreateIf (pIf)) == NULL))
        {
            return ((tMACTable *) NULL);
        }
        else
        {
            pBcpPtr = (tBCPIf *) pIf->pBcpPtr;
            return (pBcpPtr->MACTableEntry);
        }
    }
    return ((tMACTable *) NULL);
}

/* LOW LEVEL Routines for Table : PppBridgeTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppBridgeTable
 Input       :  The Indices
                PppBridgeIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppBridgeTable (INT4 i4PppBridgeIfIndex)
{
    if (PppGetBCPIfPtr ((UINT4) i4PppBridgeIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppBridgeTable
 Input       :  The Indices
                PppBridgeIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppBridgeTable (INT4 *pi4PppBridgeIfIndex)
{
    if (nmhGetFirstIndex (pi4PppBridgeIfIndex, BcpInstance, FIRST_INST) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppBridgeTable
 Input       :  The Indices
                PppBridgeIfIndex
                nextPppBridgeIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppBridgeTable (INT4 i4PppBridgeIfIndex,
                               INT4 *pi4NextPppBridgeIfIndex)
{
    if (nmhGetNextIndex (&i4PppBridgeIfIndex, BcpInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextPppBridgeIfIndex = i4PppBridgeIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppBridgeOperStatus
 Input       :  The Indices
                PppBridgeIfIndex

                The Object 
                retValPppBridgeOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppBridgeOperStatus (INT4 i4PppBridgeIfIndex,
                           INT4 *pi4RetValPppBridgeOperStatus)
{

    tBCPIf             *pBcpPtr;

    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppBridgeIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValPppBridgeOperStatus = pBcpPtr->OperStatus;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPppBridgeLocalToRemoteTinygramCompression
 Input       :  The Indices
                PppBridgeIfIndex

                The Object 
                retValPppBridgeLocalToRemoteTinygramCompression
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppBridgeLocalToRemoteTinygramCompression (INT4 i4PppBridgeIfIndex,
                                                 INT4
                                                 *pi4RetValPppBridgeLocalToRemoteTinygramCompression)
{

    tBCPIf             *pBcpPtr;

    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppBridgeIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pBcpPtr->OperStatus == STATUS_UP)
    {
        *pi4RetValPppBridgeLocalToRemoteTinygramCompression =
            (pBcpPtr->BcpGSEM.pNegFlagsPerIf[TINYGRAM_IDX].
             FlagMask & ACKED_BY_PEER_MASK) ? TINYGRAM_TRUE : TINYGRAM_FALSE;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetPppBridgeRemoteToLocalTinygramCompression
 Input       :  The Indices
                PppBridgeIfIndex

                The Object 
                retValPppBridgeRemoteToLocalTinygramCompression
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppBridgeRemoteToLocalTinygramCompression (INT4 i4PppBridgeIfIndex,
                                                 INT4
                                                 *pi4RetValPppBridgeRemoteToLocalTinygramCompression)
{

    tBCPIf             *pBcpPtr;

    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppBridgeIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pBcpPtr->OperStatus == STATUS_UP)
    {
        *pi4RetValPppBridgeRemoteToLocalTinygramCompression =
            (pBcpPtr->BcpGSEM.pNegFlagsPerIf[TINYGRAM_IDX].
             FlagMask & ALLOWED_FOR_PEER_MASK) ? TINYGRAM_TRUE : TINYGRAM_FALSE;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetPppBridgeLocalToRemoteLanId
 Input       :  The Indices
                PppBridgeIfIndex

                The Object 
                retValPppBridgeLocalToRemoteLanId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppBridgeLocalToRemoteLanId (INT4 i4PppBridgeIfIndex,
                                   INT4 *pi4RetValPppBridgeLocalToRemoteLanId)
{

    tBCPIf             *pBcpPtr;

    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppBridgeIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pBcpPtr->OperStatus == STATUS_UP)
    {
        *pi4RetValPppBridgeLocalToRemoteLanId =
            (pBcpPtr->BcpGSEM.pNegFlagsPerIf[LAN_ID_IDX].
             FlagMask & ACKED_BY_PEER_MASK) ? LAN_ID_TRUE : LAN_ID_FALSE;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhGetPppBridgeRemoteToLocalLanId
 Input       :  The Indices
                PppBridgeIfIndex

                The Object 
                retValPppBridgeRemoteToLocalLanId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppBridgeRemoteToLocalLanId (INT4 i4PppBridgeIfIndex,
                                   INT4 *pi4RetValPppBridgeRemoteToLocalLanId)
{

    tBCPIf             *pBcpPtr;

    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppBridgeIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pBcpPtr->OperStatus == STATUS_UP)
    {
        *pi4RetValPppBridgeRemoteToLocalLanId =
            (pBcpPtr->BcpGSEM.pNegFlagsPerIf[LAN_ID_IDX].
             FlagMask & ALLOWED_FOR_PEER_MASK) ? LAN_ID_TRUE : LAN_ID_FALSE;
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/* LOW LEVEL Routines for Table : PppBridgeConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppBridgeConfigTable
 Input       :  The Indices
                PppBridgeConfigIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppBridgeConfigTable (INT4 i4PppBridgeConfigIfIndex)
{
    if (PppGetBCPIfPtr ((UINT4) i4PppBridgeConfigIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppBridgeConfigTable
 Input       :  The Indices
                PppBridgeConfigIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppBridgeConfigTable (INT4 *pi4PppBridgeConfigIfIndex)
{
    if (nmhGetFirstIndex (pi4PppBridgeConfigIfIndex, BcpInstance, FIRST_INST) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppBridgeConfigTable
 Input       :  The Indices
                PppBridgeConfigIfIndex
                nextPppBridgeConfigIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppBridgeConfigTable (INT4 i4PppBridgeConfigIfIndex,
                                     INT4 *pi4NextPppBridgeConfigIfIndex)
{
    if (nmhGetNextIndex (&i4PppBridgeConfigIfIndex, BcpInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextPppBridgeConfigIfIndex = i4PppBridgeConfigIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppBridgeConfigAdminStatus
 Input       :  The Indices
                PppBridgeConfigIfIndex

                The Object 
                retValPppBridgeConfigAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppBridgeConfigAdminStatus (INT4 i4PppBridgeConfigIfIndex,
                                  INT4 *pi4RetValPppBridgeConfigAdminStatus)
{

    tBCPIf             *pBcpPtr;

    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppBridgeConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppBridgeConfigAdminStatus = pBcpPtr->AdminStatus;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPppBridgeConfigTinygram
 Input       :  The Indices
                PppBridgeConfigIfIndex

                The Object 
                retValPppBridgeConfigTinygram
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppBridgeConfigTinygram (INT4 i4PppBridgeConfigIfIndex,
                               INT4 *pi4RetValPppBridgeConfigTinygram)
{

    tBCPIf             *pBcpPtr;

    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppBridgeConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppBridgeConfigTinygram =
        (pBcpPtr->BcpGSEM.pNegFlagsPerIf[TINYGRAM_IDX].
         FlagMask & ACKED_BY_PEER_MASK) ? TINYGRAM_TRUE : TINYGRAM_FALSE;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPppBridgeConfigRingId
 Input       :  The Indices
                PppBridgeConfigIfIndex

                The Object 
                retValPppBridgeConfigRingId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppBridgeConfigRingId (INT4 i4PppBridgeConfigIfIndex,
                             INT4 *pi4RetValPppBridgeConfigRingId)
{

    tBCPIf             *pBcpPtr;

    PPP_UNUSED (pi4RetValPppBridgeConfigRingId);
    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppBridgeConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    /* Makefile changes - control reaches end of non-void function */
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPppBridgeConfigLineId
 Input       :  The Indices
                PppBridgeConfigIfIndex

                The Object 
                retValPppBridgeConfigLineId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppBridgeConfigLineId (INT4 i4PppBridgeConfigIfIndex,
                             INT4 *pi4RetValPppBridgeConfigLineId)
{

    tBCPIf             *pBcpPtr;

    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppBridgeConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppBridgeConfigLineId =
        (pBcpPtr->BcpGSEM.pNegFlagsPerIf[LINE_ID_IDX].
         FlagMask & ACKED_BY_PEER_MASK) ? LINE_ID_TRUE : LINE_ID_FALSE;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPppBridgeConfigLanId
 Input       :  The Indices
                PppBridgeConfigIfIndex

                The Object 
                retValPppBridgeConfigLanId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppBridgeConfigLanId (INT4 i4PppBridgeConfigIfIndex,
                            INT4 *pi4RetValPppBridgeConfigLanId)
{

    tBCPIf             *pBcpPtr;

    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppBridgeConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppBridgeConfigLanId = (pBcpPtr->BcpGSEM.pNegFlagsPerIf[TINYGRAM_IDX].    /* Huge error */
                                      FlagMask & ACKED_BY_PEER_MASK) ?
        LAN_ID_TRUE : LAN_ID_FALSE;
    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppBridgeConfigAdminStatus
 Input       :  The Indices
                PppBridgeConfigIfIndex

                The Object 
                testValPppBridgeConfigAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppBridgeConfigAdminStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4PppBridgeConfigIfIndex,
                                     INT4 i4TestValPppBridgeConfigAdminStatus)
{
    tBCPIf             *pBcpPtr;

    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppBridgeConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppBridgeConfigAdminStatus != ADMIN_OPEN
        && i4TestValPppBridgeConfigAdminStatus != ADMIN_CLOSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppBridgeConfigTinygram
 Input       :  The Indices
                PppBridgeConfigIfIndex

                The Object 
                testValPppBridgeConfigTinygram
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppBridgeConfigTinygram (UINT4 *pu4ErrorCode,
                                  INT4 i4PppBridgeConfigIfIndex,
                                  INT4 i4TestValPppBridgeConfigTinygram)
{

    tBCPIf             *pBcpPtr;

    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppBridgeConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppBridgeConfigTinygram != 1
        && i4TestValPppBridgeConfigTinygram != 2)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

#ifdef NPAPI_WANTED
    if (PppNpTinygramCompressSupported () == FNP_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
#endif

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppBridgeConfigRingId
 Input       :  The Indices
                PppBridgeConfigIfIndex

                The Object 
                testValPppBridgeConfigRingId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppBridgeConfigRingId (UINT4 *pu4ErrorCode,
                                INT4 i4PppBridgeConfigIfIndex,
                                INT4 i4TestValPppBridgeConfigRingId)
{
    PPP_UNUSED (i4PppBridgeConfigIfIndex);
    PPP_UNUSED (i4TestValPppBridgeConfigRingId);
    PPP_UNUSED (pu4ErrorCode);
#ifdef NPAPI_WANTED
    if (PppNpNonEthernetMacTypesSupported () == FNP_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
#endif

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppBridgeConfigLineId
 Input       :  The Indices
                PppBridgeConfigIfIndex

                The Object 
                testValPppBridgeConfigLineId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppBridgeConfigLineId (UINT4 *pu4ErrorCode,
                                INT4 i4PppBridgeConfigIfIndex,
                                INT4 i4TestValPppBridgeConfigLineId)
{

    tBCPIf             *pBcpPtr;

    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppBridgeConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppBridgeConfigLineId != 1
        && i4TestValPppBridgeConfigLineId != 2)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
#ifdef NPAPI_WANTED
    if (PppNpSRouteBridgeSupported () == FNP_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
#endif

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppBridgeConfigLanId
 Input       :  The Indices
                PppBridgeConfigIfIndex

                The Object 
                testValPppBridgeConfigLanId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppBridgeConfigLanId (UINT4 *pu4ErrorCode,
                               INT4 i4PppBridgeConfigIfIndex,
                               INT4 i4TestValPppBridgeConfigLanId)
{
    tBCPIf             *pBcpPtr;

    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppBridgeConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppBridgeConfigLanId != 1
        && i4TestValPppBridgeConfigLanId != 2)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
#ifdef NPAPI_WANTED
    if (PppNpSRouteBridgeSupported () == FNP_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
#endif

    return (SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppBridgeConfigAdminStatus
 Input       :  The Indices
                PppBridgeConfigIfIndex

                The Object 
                setValPppBridgeConfigAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppBridgeConfigAdminStatus (INT4 i4PppBridgeConfigIfIndex,
                                  INT4 i4SetValPppBridgeConfigAdminStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tBCPIf             *pBcpPtr;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppBridgeConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (i4SetValPppBridgeConfigAdminStatus == ADMIN_OPEN)
    {
        pBcpPtr->AdminStatus = (UINT1) i4SetValPppBridgeConfigAdminStatus;
        BCPEnableIf (pBcpPtr, &BCPCtrlBlk);
    }
    else
    {
        pBcpPtr->AdminStatus = (UINT1) i4SetValPppBridgeConfigAdminStatus;
        BCPDisableIf (pBcpPtr);
    }
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppBridgeConfigAdminStatus, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                  i4PppBridgeConfigIfIndex,
		  i4SetValPppBridgeConfigAdminStatus));

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetPppBridgeConfigTinygram
 Input       :  The Indices
                PppBridgeConfigIfIndex

                The Object 
                setValPppBridgeConfigTinygram
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppBridgeConfigTinygram (INT4 i4PppBridgeConfigIfIndex,
                               INT4 i4SetValPppBridgeConfigTinygram)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tBCPIf             *pBcpPtr;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppBridgeConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (i4SetValPppBridgeConfigTinygram == TINYGRAM_TRUE)
    {
        pBcpPtr->BcpGSEM.pNegFlagsPerIf[TINYGRAM_IDX].FlagMask |= DESIRED_SET;
        pBcpPtr->BcpGSEM.pNegFlagsPerIf[TINYGRAM_IDX].FlagMask |=
            FORCED_TO_REQUEST_SET;
    }
    else
    {
        pBcpPtr->BcpGSEM.pNegFlagsPerIf[TINYGRAM_IDX].FlagMask &=
            DESIRED_NOT_SET;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppBridgeConfigTinygram, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                  i4PppBridgeConfigIfIndex,
		  i4SetValPppBridgeConfigTinygram));

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetPppBridgeConfigRingId
 Input       :  The Indices
                PppBridgeConfigIfIndex

                The Object 
                setValPppBridgeConfigRingId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppBridgeConfigRingId (INT4 i4PppBridgeConfigIfIndex,
                             INT4 i4SetValPppBridgeConfigRingId)
{
    PPP_UNUSED (i4PppBridgeConfigIfIndex);
    PPP_UNUSED (i4SetValPppBridgeConfigRingId);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhSetPppBridgeConfigLineId
 Input       :  The Indices
                PppBridgeConfigIfIndex

                The Object 
                setValPppBridgeConfigLineId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppBridgeConfigLineId (INT4 i4PppBridgeConfigIfIndex,
                             INT4 i4SetValPppBridgeConfigLineId)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tBCPIf             *pBcpPtr;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppBridgeConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (i4SetValPppBridgeConfigLineId == LINE_ID_TRUE)
    {
        if (pBcpPtr->BcpGSEM.pNegFlagsPerIf[BRIDGE_ID_IDX].
            FlagMask & DESIRED_SET)
        {
            return (SNMP_FAILURE);
        }
        else
        {
            pBcpPtr->BcpGSEM.pNegFlagsPerIf[LINE_ID_IDX].FlagMask |=
                DESIRED_SET;
        }
    }
    else
    {
        pBcpPtr->BcpGSEM.pNegFlagsPerIf[LINE_ID_IDX].FlagMask &=
            DESIRED_NOT_SET;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppBridgeConfigLineId, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                  i4PppBridgeConfigIfIndex,
		  i4SetValPppBridgeConfigLineId));

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetPppBridgeConfigLanId
 Input       :  The Indices
                PppBridgeConfigIfIndex

                The Object 
                setValPppBridgeConfigLanId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppBridgeConfigLanId (INT4 i4PppBridgeConfigIfIndex,
                            INT4 i4SetValPppBridgeConfigLanId)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tBCPIf             *pBcpPtr;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppBridgeConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (i4SetValPppBridgeConfigLanId == LAN_ID_TRUE)
    {
        pBcpPtr->BcpGSEM.pNegFlagsPerIf[LAN_ID_IDX].FlagMask |= DESIRED_SET;
    }
    else
    {
        pBcpPtr->BcpGSEM.pNegFlagsPerIf[LAN_ID_IDX].FlagMask &= DESIRED_NOT_SET;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppBridgeConfigLanId, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                  i4PppBridgeConfigIfIndex,
		  i4SetValPppBridgeConfigLanId));
    return (SNMP_SUCCESS);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2PppBridgeConfigTable
 Input       :  The Indices
                IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PppBridgeConfigTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : PppBridgeMediaTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppBridgeMediaTable
 Input       :  The Indices
                PppBridgeMediaIfIndex
                PppBridgeMediaMacType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppBridgeMediaTable (INT4 i4PppBridgeMediaIfIndex,
                                             INT4 i4PppBridgeMediaMacType)
{
    if (PppGetBCPIfPtr ((UINT4) i4PppBridgeMediaIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (SNMPGetBCPMACTablePtr
        ((UINT4) i4PppBridgeMediaIfIndex,
         (UINT4) i4PppBridgeMediaMacType) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppBridgeMediaTable
 Input       :  The Indices
                PppBridgeMediaIfIndex
                PppBridgeMediaMacType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppBridgeMediaTable (INT4 *pi4PppBridgeMediaIfIndex,
                                     INT4 *pi4PppBridgeMediaMacType)
{
    if (nmhGetFirstDoubleIndex
        (pi4PppBridgeMediaIfIndex, pi4PppBridgeMediaMacType,
         (INT1) BcpMediaInstance, FIRST_INST) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppBridgeMediaTable
 Input       :  The Indices
                PppBridgeMediaIfIndex
                nextPppBridgeMediaIfIndex
                PppBridgeMediaMacType
                nextPppBridgeMediaMacType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppBridgeMediaTable (INT4 i4PppBridgeMediaIfIndex,
                                    INT4 *pi4NextPppBridgeMediaIfIndex,
                                    INT4 i4PppBridgeMediaMacType,
                                    INT4 *pi4NextPppBridgeMediaMacType)
{
    if (nmhGetNextDoubleIndex
        (&i4PppBridgeMediaIfIndex, &i4PppBridgeMediaMacType,
         (INT1) BcpMediaInstance, NEXT_INST) == SNMP_SUCCESS)
    {
        *pi4NextPppBridgeMediaIfIndex = i4PppBridgeMediaIfIndex;
        *pi4NextPppBridgeMediaMacType = i4PppBridgeMediaMacType;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppBridgeMediaLocalStatus
 Input       :  The Indices
                PppBridgeMediaIfIndex
                PppBridgeMediaMacType

                The Object 
                retValPppBridgeMediaLocalStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppBridgeMediaLocalStatus (INT4 i4PppBridgeMediaIfIndex,
                                 INT4 i4PppBridgeMediaMacType,
                                 INT4 *pi4RetValPppBridgeMediaLocalStatus)
{

    tBCPIf             *pBcpPtr;
    tMACTable          *pMACTablePtr;

    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppBridgeMediaIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pBcpPtr->OperStatus == STATUS_UP)
    {
        if ((pMACTablePtr =
             SNMPGetBCPMACTablePtr ((UINT4) i4PppBridgeMediaIfIndex,
                                    (UINT4) i4PppBridgeMediaMacType)) == NULL)
        {
            return (SNMP_FAILURE);
        }
    }
    else
    {
        return (SNMP_FAILURE);
    }
    if (pMACTablePtr->NegStatusFlags & MAC_TYPE_ACKED_BY_PEER_FLAG)
    {
        *pi4RetValPppBridgeMediaLocalStatus = 1;
    }
    else
    {
        *pi4RetValPppBridgeMediaLocalStatus = 2;
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPppBridgeMediaRemoteStatus
 Input       :  The Indices
                PppBridgeMediaIfIndex
                PppBridgeMediaMacType

                The Object 
                retValPppBridgeMediaRemoteStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppBridgeMediaRemoteStatus (INT4 i4PppBridgeMediaIfIndex,
                                  INT4 i4PppBridgeMediaMacType,
                                  INT4 *pi4RetValPppBridgeMediaRemoteStatus)
{

    tBCPIf             *pBcpPtr;
    tMACTable          *pMACTablePtr;

    if ((pBcpPtr = PppGetBCPIfPtr ((UINT4) i4PppBridgeMediaIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pBcpPtr->OperStatus == STATUS_UP)
    {
        if ((pMACTablePtr =
             SNMPGetBCPMACTablePtr ((UINT4) i4PppBridgeMediaIfIndex,
                                    (UINT4) i4PppBridgeMediaMacType)) == NULL)
        {
            return (SNMP_FAILURE);
        }
    }
    else
    {
        return (SNMP_FAILURE);
    }
    if (pMACTablePtr->NegStatusFlags & MAC_TYPE_ACKED_BY_LOCAL_FLAG)
    {
        *pi4RetValPppBridgeMediaRemoteStatus = 1;
    }
    else
    {
        *pi4RetValPppBridgeMediaRemoteStatus = 2;
    }
    return (SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : PppBridgeMediaConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppBridgeMediaConfigTable
 Input       :  The Indices
                PppBridgeMediaConfigIfIndex
                PppBridgeMediaConfigMacType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppBridgeMediaConfigTable (INT4
                                                   i4PppBridgeMediaConfigIfIndex,
                                                   INT4
                                                   i4PppBridgeMediaConfigMacType)
{
    if (PppGetBCPIfPtr ((UINT4) i4PppBridgeMediaConfigIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (SNMPGetBCPMACTablePtr
        ((UINT4) i4PppBridgeMediaConfigIfIndex,
         (UINT4) i4PppBridgeMediaConfigMacType) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppBridgeMediaConfigTable
 Input       :  The Indices
                PppBridgeMediaConfigIfIndex
                PppBridgeMediaConfigMacType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppBridgeMediaConfigTable (INT4 *pi4PppBridgeMediaConfigIfIndex,
                                           INT4 *pi4PppBridgeMediaConfigMacType)
{
    if (nmhGetFirstDoubleIndex
        (pi4PppBridgeMediaConfigIfIndex, pi4PppBridgeMediaConfigMacType,
         (INT1) BcpMediaInstance, FIRST_INST) == SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppBridgeMediaConfigTable
 Input       :  The Indices
                PppBridgeMediaConfigIfIndex
                nextPppBridgeMediaConfigIfIndex
                PppBridgeMediaConfigMacType
                nextPppBridgeMediaConfigMacType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppBridgeMediaConfigTable (INT4 i4PppBridgeMediaConfigIfIndex,
                                          INT4
                                          *pi4NextPppBridgeMediaConfigIfIndex,
                                          INT4 i4PppBridgeMediaConfigMacType,
                                          INT4
                                          *pi4NextPppBridgeMediaConfigMacType)
{
    if (nmhGetNextDoubleIndex
        (&i4PppBridgeMediaConfigIfIndex, &i4PppBridgeMediaConfigMacType,
         (INT1) BcpMediaInstance, NEXT_INST) == SNMP_SUCCESS)
    {
        *pi4NextPppBridgeMediaConfigIfIndex = i4PppBridgeMediaConfigIfIndex;
        *pi4NextPppBridgeMediaConfigMacType = i4PppBridgeMediaConfigMacType;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppBridgeMediaConfigLocalStatus
 Input       :  The Indices
                PppBridgeMediaConfigIfIndex
                PppBridgeMediaConfigMacType

                The Object 
                retValPppBridgeMediaConfigLocalStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppBridgeMediaConfigLocalStatus (INT4 i4PppBridgeMediaConfigIfIndex,
                                       INT4 i4PppBridgeMediaConfigMacType,
                                       INT4
                                       *pi4RetValPppBridgeMediaConfigLocalStatus)
{
    tBCPIf             *pBcpPtr;
    tMACTable          *pMACTablePtr;

    if ((pBcpPtr =
         PppGetBCPIfPtr ((UINT4) i4PppBridgeMediaConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if ((pMACTablePtr =
         SNMPGetBCPMACTablePtr ((UINT4) i4PppBridgeMediaConfigIfIndex,
                                (UINT4) i4PppBridgeMediaConfigMacType)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pBcpPtr->BcpGSEM.pNegFlagsPerIf[MAC_SUPPORT_IDX].
        FlagMask & DESIRED_MASK)
    {
        *pi4RetValPppBridgeMediaConfigLocalStatus = 1;
    }
    else
    {
        *pi4RetValPppBridgeMediaConfigLocalStatus = 2;
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppBridgeMediaConfigLocalStatus
 Input       :  The Indices
                PppBridgeMediaConfigIfIndex
                PppBridgeMediaConfigMacType

                The Object 
                testValPppBridgeMediaConfigLocalStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppBridgeMediaConfigLocalStatus (UINT4 *pu4ErrorCode,
                                          INT4 i4PppBridgeMediaConfigIfIndex,
                                          INT4 i4PppBridgeMediaConfigMacType,
                                          INT4
                                          i4TestValPppBridgeMediaConfigLocalStatus)
{

    tBCPIf             *pBcpPtr;

    PPP_UNUSED (i4PppBridgeMediaConfigMacType);

    if ((pBcpPtr =
         PppGetBCPIfPtr ((UINT4) i4PppBridgeMediaConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValPppBridgeMediaConfigLocalStatus != 1
        && i4TestValPppBridgeMediaConfigLocalStatus != 2)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
#ifdef NPAPI_WANTED
    if (PppNpNonEthernetMacTypesSupported () == FNP_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
#endif

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetPppBridgeMediaConfigLocalStatus
 Input       :  The Indices
                PppBridgeMediaConfigIfIndex
                PppBridgeMediaConfigMacType

                The Object 
                setValPppBridgeMediaConfigLocalStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppBridgeMediaConfigLocalStatus (INT4 i4PppBridgeMediaConfigIfIndex,
                                       INT4 i4PppBridgeMediaConfigMacType,
                                       INT4
                                       i4SetValPppBridgeMediaConfigLocalStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tBCPIf             *pBcpPtr;
    tMACTable          *pMACTablePtr;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if ((pBcpPtr =
         PppGetBCPIfPtr ((UINT4) i4PppBridgeMediaConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if ((pMACTablePtr =
         SNMPGetOrCreateBCPMACTablePtr ((UINT4) i4PppBridgeMediaConfigIfIndex,
                                        (UINT4) i4PppBridgeMediaConfigMacType))
        == NULL)
    {

        return (SNMP_FAILURE);
    }
    if (i4SetValPppBridgeMediaConfigLocalStatus == 1)
    {
        pMACTablePtr->NegStatusFlags |= 0x01;
    }
    else
    {
        pMACTablePtr->NegStatusFlags &= 0xfe;
    }
    if (i4SetValPppBridgeMediaConfigLocalStatus != 2)
    {
        pBcpPtr->BcpGSEM.pNegFlagsPerIf[MAC_SUPPORT_IDX].FlagMask |=
            DESIRED_SET;
    }
    else
    {
        pBcpPtr->BcpGSEM.pNegFlagsPerIf[MAC_SUPPORT_IDX].FlagMask &=
            DESIRED_NOT_SET;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppBridgeMediaConfigLocalStatus, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 2, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                  i4PppBridgeMediaConfigIfIndex,
		  i4PppBridgeMediaConfigMacType,		  
		  i4SetValPppBridgeMediaConfigLocalStatus));
    return (SNMP_SUCCESS);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2PppBridgeMediaConfigTable
 Input       :  The Indices
                IfIndex
                PppBridgeMediaConfigMacType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PppBridgeMediaConfigTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
