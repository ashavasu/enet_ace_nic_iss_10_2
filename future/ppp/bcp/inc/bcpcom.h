/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bcpcom.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the common PPP files required for the
 * BCP module
 *
 *******************************************************************/
#ifndef __PPP_BCPCOM_H__
#define __PPP_BCPCOM_H__

#include "genhdrs.h"

/* Global PPP includes */
#include "pppmacro.h"
#include "pppdefs.h"
#include "pppdebug.h"
#include "ppptimer.h"
#include "pppdbgex.h"

/* Includes from other modules */
#include "frmtdfs.h"
#include "pppgsem.h"
#include "pppgcp.h"
#include "ppplcp.h"
#include "pppexts.h"

#include "pppbcp.h"

#include "bcpproto.h"


#endif  /* __PPP_BCPCOM_H__ */
