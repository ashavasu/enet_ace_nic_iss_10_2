/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppbcp.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the typedefs, #defines and  
 *             data structures used by the BCP Module.						
 *
 *******************************************************************/
#ifndef __PPP_PPPBCP_H__
#define __PPP_PPPBCP_H__

/*********************************************************************/
/*                              CONSTANTS                           */
/*********************************************************************/

/* CONFIGURATION OPTION VALUES  for BCP packets */

#define  BRIDGE_ID_OPTION                1      
#define  LINE_ID_OPTION                  2      
#define  MAC_SUPPORT_OPTION              3      
#define  TINYGRAM_OPTION                 4      
/* 2878 changes - instead of LAN_ID_OPTION */
#define  DUMMY_OPTION                    0      
#define  MAC_ADDR_OPTION                 6      
#define  SPAN_TREE_OPTION                7      
#define  IEEE_802_TAGGED_FRAME_OPTION    8
#define  MANAGEMENT_INLINE_OPTION        9

#define  BRIDGE_ID_IDX                   0      
#define  LINE_ID_IDX                     1      
#define  MAC_SUPPORT_IDX                 2      
#define  TINYGRAM_IDX                    3      
#define  LAN_ID_IDX                      4      
#define  MAC_ADDR_IDX                    5      
#define  SPAN_TREE_IDX                   6      
#define  IEEE_802_TAGGED_FRAME_IDX       7
#define  MANAGEMENT_INLINE_IDX           8

#define  BCP_SPAN_IEEE_DATA              0x0201 
#define  BCP_IBM_SRC_RT_DATA             0x0203 
#define  BCP_DEC_LAN_DATA                0x0205 
#define  BCP_BRIDGED_LAN_DATA            0x0031 

#define  MAX_MAC_SUPPORT_DISC_VALUES        7      
#define  MAX_TINYGRAM_DISC_VALUES           2      
#define  MAX_SPAN_PROT_DISC_VALUES          5      
#define  MAX_IEEE_802_TAG_FRAME_DISC_VALUES 2

#define  IEEE_8023_CAN                   1      
#define  IEEE_8024_CAN                   2      
#define  IEEE_8025_CAN                   3      
#define  FDDI_CAN                        4      
#define  IEEE_8025_NON_CAN               11     
#define  FDDI_NON_CAN                    12     

#define  IEEE802_TAG_TRUE                2      
#define  IEEE802_TAG_FALSE               1      
#define  IEEE802_TAG_ENABLE              1      
#define  IEEE802_TAG_DISABLE             2      

#define  MGMT_INLINE_TRUE                2      
#define  MGMT_INLINE_FALSE               1      
#define  MGMT_INLINE_ENABLE              1      
#define  MGMT_INLINE_DISABLE             2      

#define  TINYGRAM_TRUE                   2      
#define  TINYGRAM_FALSE                  1      
#define  TINYGRAM_ENABLE                 1      
#define  TINYGRAM_DISABLE                2      

#define  LAN_ID_TRUE                     2      
#define  LAN_ID_FALSE                    1      
#define  LAN_ID_ENABLE                   1      
#define  LAN_ID_DISABLE                  2      

#define  LINE_ID_TRUE                    2      
#define  LINE_ID_FALSE                   1      
#define  LINE_ID_ENABLE                  1      
#define  LINE_ID_DISABLE                 2      

#define  BRIDGE_ID_TRUE                  2      
#define  BRIDGE_ID_FALSE                 1      
#define  BRIDGE_ID_ENABLE                1      
#define  BRIDGE_ID_DISABLE               2      

#define  NO_SPAN_TREE_PROTO              0      
#define  IEEE_8021D_SPAN_TREE_PROTO      1      
#define  IEEE_8021G_SPAN_TREE_PROTO      2      
#define  IBM_SRC_ROUTE_SPAN_TREE_PROTO   3      
#define  DEC_LAN_BRIDGE_SPAN_TREE_PROTO  4      

#define  LINE_ID_MASK                    0xfff0 
#define  BRIDGE_ID_MASK                  0x000f 

#define  MAC_TYPE_ACKED_BY_PEER_FLAG     0x01   
#define  MAC_TYPE_ACKED_BY_LOCAL_FLAG    0x02   
#define  CHECK_BIT_MASK                  0x01   
#define  MAC_ADDR_MULTICAST_FLAG         0x00   

#define  MAX_BCP_OPT_TYPES               9 /* was 7, 2878 changes */      
#define  MAC_ADDR_LEN                    6      
#define  MAX_MAC_TYPES                   6      

/********************************************************************/
/*          TYPEDEFS    USED BY THE BCP MODULE                     */
/*********************************************************************/

typedef   struct   {
	UINT2	BridgeLineId;
	UINT1	MACAddress[6];
	UINT1	SpanningTreeProto;
	UINT1	u1Rsvd;
	UINT2	u2Rsvd;
}   tBCPOptions;

typedef   struct   {
	UINT1	LoctoRemMACAddress[MAC_ADDR_LEN];
	UINT1	RemtoLocMACAddress[MAC_ADDR_LEN];
}   tBCPCtrlBlk;

typedef   struct   {
	UINT1	MACType;
	UINT1	NegStatusFlags;
	UINT2	u2Rsvd;
} tMACTable;

typedef   struct   bcpif  {
	UINT1			OperStatus;
	UINT1			AdminStatus;
	UINT2			u2Rsvd;
	tBCPOptions		BcpOptionsDesired;
	tBCPOptions		BcpOptionsAllowedForPeer;
	tBCPOptions		BcpOptionsAckedByPeer;
	tBCPOptions		BcpOptionsAckedByLocal;
	tMACTable		MACTableEntry[6];
	tGSEM			BcpGSEM;
} tBCPIf;


#endif  /* __PPP_PPPBCP_H__ */
