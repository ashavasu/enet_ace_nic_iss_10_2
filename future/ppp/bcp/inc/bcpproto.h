/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bcpproto.h,v 1.4 2011/10/13 10:31:17 siva Exp $
 *
 * Description:This file contains the prototypes for BCP functions used
 * internally by the BCP module
 *
 *******************************************************************/
#ifndef __PPP_BCPPROTO_H__
#define __PPP_BCPPROTO_H__

tBCPIf *ALLOC_BCP_IF(VOID);
INT1 BCPProcessBridgeIdConfReq ( tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal, UINT2 Offset, UINT1 PresenceFlag );
INT1 BCPProcessLineIdConfReq ( tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal, UINT2 Offset, UINT1 PresenceFlag );
INT1 BCPProcessMACSupportConfReq ( tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal, UINT2 Offset, UINT1 PresenceFlag );
INT1 BCPProcessTinygramConfReq ( tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal, UINT2 Offset, UINT1 PresenceFlag );
INT1 BCPProcessLANIdConfReq ( tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal,
UINT2 Offset, UINT1 PresenceFlag );
INT1 BCPProcessMACAddressConfReq ( tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal, UINT2 Offset, UINT1 PresenceFlag );
INT1 BCPProcessSpanningTreeProtoConfReq ( tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal, UINT2 Offset, UINT1 PresenceFlag );
INT1 BCPProcessBridgeIdConfNak ( tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal );
INT1 BCPProcessLineIdConfNak ( tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal );
INT1 BCPProcessMACSupportConfNak ( tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal );
INT1 BCPProcessTinygramConfNak ( tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal );
INT1 BCPProcessLANIdConfNak ( tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal );
INT1 BCPProcessMACAddressConfNak ( tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal );
INT1 BCPProcessSpanningTreeProtoConfNak ( tGSEM *pGSEM, t_MSG_DESC *pInPkt, tOptVal OptVal );
INT1 BCPAddMACSupportSubOpt ( tGSEM *pGSEM, tOptVal OptVal, t_MSG_DESC *pOutBuf
);
INT1 BCPAddSpanningTreeProtoSubOpt (tGSEM *pGSEM, tOptVal OptVal, t_MSG_DESC *pOutBuf );
VOID *BCPReturnBridgeIdPtr(tGSEM  *pGSEM, UINT1 *OptLen);
VOID *BCPReturnLineIdPtr(tGSEM  *pGSEM, UINT1 *OptLen);
VOID *BCPReturnMACSupportPtr ( tGSEM  *pGSEM, UINT1 *OptLen );
VOID *BCPReturnTinygramPtr ( tGSEM  *pGSEM, UINT1 *OptLen );
VOID *BCPReturnLANIdPtr ( tGSEM  *pGSEM, UINT1 *OptLen );
VOID *BCPReturnMACAddressPtr ( tGSEM  *pGSEM, UINT1 *OptLen );
VOID *BCPReturnSpanningTreeProtoPtr(tGSEM  *pGSEM, UINT1 *OptLen);
INT1 BCPProcessMACSupportConfRej ( tGSEM *pGSEM );
INT1 BCPProcessSpanningTreeProtoConfRej ( tGSEM *pGSEM );
INT1 BCPCopyOptions ( tGSEM *pGSEM, UINT1 Flag, UINT1 PktType );
tGSEM *BCPGetSEMPtr ( tPPPIf *pIf );
tGSEM *BCPSpanIeeeGetSEMPtr ( tPPPIf *pIf );
tGSEM *BCPIbmSrcRtGetSEMPtr ( tPPPIf *pIf );
tGSEM *BCPDecLanGetSEMPtr ( tPPPIf *pIf );
UINT2 BCPGetSpanTreeBitMask ( UINT1 Index );

INT1  BCPInit(tBCPIf *pIf);
tBCPIf *BCPCreateIf(tPPPIf *pIf);
VOID     BCPDeleteIf(tBCPIf *pIf);
VOID     BCPEnableIf(tBCPIf *pIf, tBCPCtrlBlk *pBcpCtrlBlk);
VOID     BCPDisableIf(tBCPIf *pIf);
VOID     BCPOptionInit(tBCPOptions *pBCPOpt);
UINT2    BCPConfigInfoLen(tGSEM *pBcpGSEM);
VOID     BCPUp(tGSEM *pBcpGSEM);
VOID     BCPDown(tGSEM *pBcpGSEM);
VOID     BCPFinished(tGSEM *pGSEM);
UINT1    BCPRecdConfigAck(tBCPIf *, t_MSG_DESC *pInpacket, UINT2     length, UINT1);
UINT1    BCPRecdConfigNak(tBCPIf *, t_MSG_DESC *pInpacket, UINT2     Length, UINT1);
UINT1    BCPRecdConfigRej(tBCPIf *, t_MSG_DESC *pInpacket, UINT2     Length, UINT1);
UINT1    BCPRecdConfigReq(tBCPIf *, t_MSG_DESC *pInpacket, UINT2     Length, UINT1);
UINT1    BCPRecdTermReq(tBCPIf *, t_MSG_DESC *pInpacket, UINT2     Length, UINT1);
UINT1    BCPRecdTermAck(tBCPIf *, t_MSG_DESC *pInpacket, UINT2     Length, UINT1);
UINT1    BCPRecdCodeRej(tBCPIf *, t_MSG_DESC *pInpacket, UINT2     Length, UINT1);
VOID  ConfigureBCP (tBCPIf *pBcpPtr);
tBCPIf  *PppGetBCPIfPtr(UINT4 Index);
tBCPIf  *PppCreateBCPIfPtr(UINT4 Index);
tMACTable *SNMPGetBCPMACTablePtr(UINT4 Index1,UINT4 Index2);
tMACTable *SNMPGetOrCreateBCPMACTablePtr(UINT4 Index1,UINT4 Index2);

#ifdef BRIDGE_WANTED
INT4 BridgeCreatePort(UINT2 u2IfIndex);
INT4 BridgeDeletePort(UINT2 u2IfIndex);  
#endif

INT1 BCPProcessIEEE802TagFrameConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt,
                                    tOptVal OptVal, UINT2 Offset,
                                    UINT1 PresenceFlag);
VOID *BCPReturnIEEE802TagFramePtr(tGSEM * pGSEM, UINT1 *OptLen);
INT1 BCPProcessMgmtInlineConfRej (tGSEM *pGSEM);



/* LOW LEVEL Routines for Table : PppExtBcpTable. */



VOID BcpInit (VOID);
#endif  /* __PPP_BCPPROTO_H__ */
