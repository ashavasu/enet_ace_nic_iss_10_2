/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bcpexts.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains definitions and prototypes required for
 * the external modules
 *
 *******************************************************************/
#ifndef __PPP_BCPEXTS_H__
#define __PPP_BCPEXTS_H__


extern   UINT2   BcpAllocCounter;
extern   UINT2   BcpFreeCounter;
extern   UINT2   BcpIfFreeCounter;
extern   tMACTable      MACTypeTable[];
extern   UINT1   MACAddressAssignFlag;

extern  tGenOptionInfo          BCPGenOptionInfo[];
extern  tOptNegFlagsPerIf       BCPOptPerIntf[];
extern  tGSEMCallbacks          BcpGSEMCallback;
extern  tBCPCtrlBlk             BCPCtrlBlk;


#endif  /* __PPP_BCPEXTS_H__ */
