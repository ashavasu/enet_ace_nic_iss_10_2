/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppvj.c,v 1.4 2014/09/11 09:44:53 siva Exp $
 *
 * Description: Contains functions for Van-Jacobson TCP/IP header
 *              compression.
 *
 *******************************************************************/

#include <string.h>
#include "pppcom.h"
#include "pppipcp.h"
#include "pppvj.h"
#include "vjproto.h"
#include "gcpdefs.h"
#include "globexts.h"

struct slcompress  *
VJCOMPCreate (pIf)
     tPPPIf             *pIf;
{
    struct slcompress  *VjComp;

    if ((VjComp = (struct slcompress *) CALLOC (1, sizeof (struct slcompress)))
        != NULL)
    {

        pIf->pVjComp = VjComp;
        sl_compress_init (VjComp);
        pIf->pVjComp = VjComp;
        return (VjComp);
    }
    else
    {
        PPP_TRC (ALL_FAILURE, " VJ COMP ALLOC FAILED ");
        return (NULL);
    }
}

/*************************************************************/
UINT2
VJCompressPkt (pBuffer, pIf)
     t_MSG_DESC        **pBuffer;
     tPPPIf             *pIf;
{
    UINT1               Type;
    UINT4               compress_cid;
    struct vjmbuf       m;
    UINT2               u2Len;
    static UINT1        TempLinear[DEF_MRU_SIZE];

    u2Len = (UINT2) VALID_BYTES (*pBuffer);

    if (u2Len > DEF_MRU_SIZE)
    {
        /* Packet size greater than DEF_MRU; No point in
         * compressing 40 bytes
         */
        return TYPE_IP;
    }

    CRU_BUF_Copy_FromBufChain (*pBuffer, TempLinear, 0, u2Len);
    m.m_off = TempLinear;
    m.m_len = u2Len;

    compress_cid =
        ((tIPCPIf *) pIf->pIpcpPtr)->IpcpOptionsAckedByLocal.CompInfo.VJInfo.
        CompSlotId;
    Type = sl_compress_tcp (&m, pIf->pVjComp, (int) compress_cid);
    if (Type != TYPE_IP)
    {

        RELEASE_BUFFER (*pBuffer);
        if ((*pBuffer =
             (t_MSG_DESC *) (VOID *) ALLOCATE_BUFFER ((m.m_len + DATA_OFFSET),
                                                      DATA_OFFSET)) != NULL)
        {
            CRU_BUF_Copy_OverBufChain (*pBuffer, m.m_off, 0, m.m_len);
        }
    }
    return (Type);
}

/*************************************************************/
t_MSG_DESC         *
VJUncompressPkt (pBuffer, pIf, Type, Length)
     t_MSG_DESC         *pBuffer;
     tPPPIf             *pIf;
     UINT2              *Type;
     UINT4               Length;
{
    struct slcompress  *pComp;
    if (*Type == TYPE_IP)
    {
        return pBuffer;
    }
    else
    {
        UINT1              *TempLinear;
        UINT1              *bufp;

        if ((pComp = pIf->pVjComp) == NULL)
        {
            PPP_TRC (ALL_FAILURE, "Compression Entry not found  ");
            return (NULL);
        }

        switch (*Type)
        {
            case IP_UNCOMP_TCP_DATA:
            case IP_COMP_TCP_DATA:
                TempLinear = MEM_MALLOC ((Length + 128), UINT1);
                bufp = TempLinear + 128;
                CRU_BUF_Copy_FromBufChain (pBuffer, bufp, 0, Length);;

                if ((bufp =
                     sl_uncompress_tcp (bufp, &Length, *Type, pComp)) == NULL)
                {
                    PPP_TRC (ALL_FAILURE, "Decompression Error ");
                    break;
                }
                RELEASE_BUFFER (pBuffer);
                if ((pBuffer =
                     (t_MSG_DESC *) (VOID *)
                     ALLOCATE_BUFFER ((Length + DATA_OFFSET),
                                      DATA_OFFSET)) != NULL)
                {
                    CRU_BUF_Copy_OverBufChain (pBuffer, bufp, 0, Length);
                }
                *Type = TYPE_IP;
                FREE (TempLinear);
            default:
                break;
        }
        return pBuffer;
    }                            /* else */

}

/*************************************************************/

UINT1
sl_compress_tcp (m, comp, compress_cid)
     struct vjmbuf      *m;
     struct slcompress  *comp;
     int                 compress_cid;
{
    register struct cstate *cs = comp->last_cs->cs_next;
    register struct ip *ip = mtod (m, struct ip *);
    register UINT4      hlen = (ip->u1VerHdrLen & 0xf0);
    register struct tcphdr *oth;    /* last TCP header */
    register struct tcphdr *th;    /* current TCP header */

    register UINT4      deltaS, deltaA;    /* general purpose temporaries */
    register UINT4      changes = 0;    /* change mask */
    UINT1               new_seq[16];    /* changes from last to current */
    register UINT1     *cp = new_seq;

    /* 
     * Bail if this is an IP fragment or if the TCP packet isn't 
     * `compressible' (i.e., ACK isn't set or some other control bit is 
     * set).  (We assume that the caller has already made sure the packet 
     * is IP proto TCP). 
     */
    MEMSET (new_seq, 0, 16);
    if ((ip->ip_off & OSIX_HTONS (0x3fff)) || m->m_len < 40)
        return (TYPE_IP);

    th = (struct tcphdr *) &((int *) ip)[hlen];
    if ((th->th_flags & (TH_SYN | TH_FIN | TH_RST | TH_ACK)) != TH_ACK)
        return (TYPE_IP);

    /* 
     * Packet is compressible -- we're going to send either a 
     * COMPRESSED_TCP or UNCOMPRESSED_TCP packet.  Either way we need to 
     * locate (or create) the connection state.  Special case the most 
     * recently used connection since it's most likely to be used again & 
     * we don't have to do any reordering if it's used. 
     */
    if (ip->ip_src.s_addr != cs->cs_ip.ip_src.s_addr ||
        ip->ip_dst.s_addr != cs->cs_ip.ip_dst.s_addr ||
        *(int *) th != ((int *) &cs->cs_ip)[(cs->cs_ip.u1VerHdrLen & 0xf0)])
    {

        /* 
         * Wasn't the first -- search for it. 
         * 
         * States are kept in a circularly linked list with last_cs 
         * pointing to the end of the list.  The list is kept in lru 
         * order by moving a state to the head of the list whenever 
         * it is referenced.  Since the list is short and, 
         * empirically, the connection we want is almost always near 
         * the front, we locate states via linear search.  If we 
         * don't find a state for the datagram, the oldest state is 
         * (re-)used. 
         */
        register struct cstate *lcs;
        register struct cstate *lastcs = comp->last_cs;

        do
        {
            lcs = cs;
            cs = cs->cs_next;
            if (ip->ip_src.s_addr == cs->cs_ip.ip_src.s_addr
                && ip->ip_dst.s_addr == cs->cs_ip.ip_dst.s_addr
                && *(int *) th ==
                ((int *) &cs->cs_ip)[(cs->cs_ip.u1VerHdrLen & 0xf0)])
                goto found;

        }
        while (cs != lastcs);

        /* 
         * Didn't find it -- re-use oldest cstate.  Send an 
         * uncompressed packet that tells the other side what 
         * connection number we're using for this conversation. Note 
         * that since the state list is circular, the oldest state 
         * points to the newest and we only need to set last_cs to 
         * update the lru linkage. 
         */
        comp->last_cs = lcs;
        hlen += (th->u1thx2off & 0xf0);
        hlen <<= 2;
        goto uncompressed;

      found:
        /* Found it -- move to the front on the connection list. */
        if (lastcs == cs)
            comp->last_cs = lcs;
        else
        {
            lcs->cs_next = cs->cs_next;
            cs->cs_next = lastcs->cs_next;
            lastcs->cs_next = cs;
        }
    }
    /* 
     * Make sure that only what we expect to change changed. The first 
     * line of the `if' checks the IP protocol version, header length & 
     * type of service.  The 2nd line checks the "Don't fragment" bit. 
     * The 3rd line checks the time-to-live and protocol (the protocol 
     * check is unnecessary but costless).  The 4th line checks the TCP 
     * header length.  The 5th line checks IP options, if any.  The 6th 
     * line checks TCP options, if any.  If any of these things are 
     * different between the previous & current datagram, we send the 
     * current datagram `uncompressed'. 
     */
    oth = (struct tcphdr *) &((int *) &cs->cs_ip)[hlen];
    deltaS = hlen;
    hlen += (th->u1thx2off & 0xf0);
    hlen <<= 2;

    if ((((UINT2 *) ip)[0] != ((UINT2 *) &cs->cs_ip)[0]) ||
        (((UINT2 *) ip)[3] != ((UINT2 *) &cs->cs_ip)[3]) ||
        (((UINT2 *) ip)[4] != ((UINT2 *) &cs->cs_ip)[4]) ||
        ((th->u1thx2off & 0xf0) != (oth->u1thx2off & 0xf0)) ||
        ((deltaS > 5 && MEMCMP ((ip + 1), (&cs->cs_ip + 1), (deltaS - 5) << 2)))
        || (((th->u1thx2off & 0xf0) > 5
             && MEMCMP ((th + 1), (oth + 1),
                        ((th->u1thx2off & 0xf0) - 5) << 2))))
        goto uncompressed;

    /* 
     * Figure out which of the changing fields changed.  The receiver 
     * expects changes in the order: urgent, window, ack, seq. 
     */
    if (th->th_flags & TH_URG)
    {
        deltaS = OSIX_NTOHS (th->th_urp);
        ENCODEZ (deltaS);
        changes |= NEW_U;
    }
    else if (th->th_urp != oth->th_urp)
        /* 
         * argh! URG not set but urp changed -- a sensible 
         * implementation should never do this but RFC793 doesn't 
         * prohibit the change so we have to deal with it. 
         */
        goto uncompressed;

    if ((deltaS = (UINT2) (OSIX_NTOHS (th->th_win) - OSIX_NTOHS (oth->th_win))))
    {
        ENCODE (deltaS);
        changes |= NEW_W;
    }
    if ((deltaA = (OSIX_NTOHL (th->th_ack)) - (OSIX_NTOHL (oth->th_ack))))
    {
        if (deltaA > 0xffff)
            goto uncompressed;
        ENCODE (deltaA);
        changes |= NEW_A;
    }
    if ((deltaS = OSIX_NTOHL (th->th_seq) - OSIX_NTOHL (oth->th_seq)))
    {
        if (deltaS > 0xffff)
            goto uncompressed;
        ENCODE (deltaS);
        changes |= NEW_S;
    }
    /* 
     * Look for the special-case encodings. 
     */
    switch (changes)
    {

        case 0:
            /* 
             * Nothing changed. If this packet contains data and the last 
             * one didn't, this is probably a data packet following an 
             * ack (normal on an interactive connection) and we send it 
             * compressed.  Otherwise it's probably a retransmit, 
             * retransmitted ack or window probe.  Send it uncompressed 
             * in case the other side missed the compressed version. 
             */
            if (ip->ip_len != cs->cs_ip.ip_len &&
                ((UINT2) OSIX_NTOHS (cs->cs_ip.ip_len) == hlen))
                break;

            /* (fall through) */

        case SPECIAL_I:
        case SPECIAL_D:
            /* 
             * Actual changes match one of our special case encodings -- 
             * send packet uncompressed. 
             */
            goto uncompressed;

        case NEW_S | NEW_A:
            if (deltaS == deltaA
                && deltaS == OSIX_NTOHS (cs->cs_ip.ip_len) - hlen)
            {
                /* special case for echoed terminal traffic */
                changes = SPECIAL_I;
                cp = new_seq;
            }
            break;

        case NEW_S:
            if (deltaS == OSIX_NTOHS (cs->cs_ip.ip_len) - hlen)
            {
                /* special case for data xfer */
                changes = SPECIAL_D;
                cp = new_seq;
            }
            break;
        default:
            break;
    }
    deltaS = (UINT4) (OSIX_NTOHS (ip->ip_id) - OSIX_NTOHS (cs->cs_ip.ip_id));
    if (deltaS != 1)
    {
        ENCODEZ (deltaS);
        changes |= NEW_I;
    }
    if (th->th_flags & TH_PUSH)
        changes |= TCP_PUSH_BIT;
    /* 
     * Grab the cksum before we overwrite it below.  Then update our 
     * state with this packet's header. 
     */
    deltaA = OSIX_NTOHS (th->th_sum);
    BCOPY (ip, &cs->cs_ip, hlen);

    /* 
     * We want to use the original packet as our compressed packet. (cp - 
     * new_seq) is the number of bytes we need for compressed sequence 
     * numbers.  In addition we need one byte for the change mask, one 
     * for the connection id and two for the tcp checksum. So, (cp - 
     * new_seq) + 4 bytes of header are needed.  hlen is how many bytes 
     * of the original packet to toss so subtract the two to get the new 
     * packet size. 
     */
    deltaS = (UINT4) (cp - new_seq);
    cp = (UINT1 *) ip;
    if (compress_cid == 0 || comp->last_xmit != cs->cs_id)
    {
        comp->last_xmit = cs->cs_id;
        hlen -= deltaS + 4;
        cp += hlen;
        *cp++ = (UINT1) (changes | NEW_C);
        *cp++ = cs->cs_id;
    }
    else
    {
        hlen -= deltaS + 3;
        cp += hlen;
        *cp++ = (UINT1) changes;
    }
    m->m_len -= hlen;
    m->m_off += hlen;
    *cp++ = (UINT1) (deltaA >> 8);
    *cp++ = (UINT1) deltaA;
    BCOPY (new_seq, cp, deltaS);
    return (TYPE_COMPRESSED_TCP);

  uncompressed:
    /* 
     * Update connection state cs & send uncompressed packet 
     * ('uncompressed' means a regular ip/tcp packet but with the 
     * 'conversation id' we hope to use on future compressed packets in 
     * the protocol field). 
     */
    BCOPY (ip, &cs->cs_ip, hlen);
    ip->ip_p = cs->cs_id;
    comp->last_xmit = cs->cs_id;
    return (TYPE_UNCOMPRESSED_TCP);
}

/******************************************************/

UINT1              *
sl_uncompress_tcp (bufp, Length, type, comp)
     UINT1              *bufp;
     UINT4              *Length;
     UINT4               type;
     struct slcompress  *comp;
{
    int                 len;
    register UINT1     *cp;
    register UINT4      hlen, changes;
    register struct tcphdr *th;
    register struct cstate *cs;
    register struct ip *ip;
    len = (int) *Length;

    switch (type)
    {

        case TYPE_ERROR:
        default:
            goto bad;

        case TYPE_IP:
            return (bufp);

        case TYPE_UNCOMPRESSED_TCP:
            /* 
             * Locate the saved state for this connection.  If the state 
             * index is legal, clear the 'discard' flag. 
             */
            ip = (struct ip *) (VOID *) bufp;
            if (ip->ip_p >= MAX_STATES)
                goto bad;

            cs = &comp->rstate[comp->last_recv = ip->ip_p];
            comp->flags = (UINT2) (comp->flags & (~SLF_TOSS));
            /* 
             * Restore the IP protocol field then save a copy of this 
             * packet header.  (The checksum is zeroed in the copy so we 
             * don't have to zero it each time we process a compressed 
             * packet. 
             */
            ip->ip_p = IPPROTO_TCP;
            hlen = (ip->u1VerHdrLen & 0xf0);
            hlen += (((struct tcphdr *) &((int *) ip)[hlen])->u1thx2off & 0xf0);
            hlen <<= 2;
            BCOPY (ip, &cs->cs_ip, hlen);
            cs->cs_ip.ip_sum = 0;
            cs->cs_hlen = (UINT2) hlen;
            return (bufp);

        case TYPE_COMPRESSED_TCP:
            break;
    }
    /* We've got a compressed packet. */
    cp = bufp;
    changes = *cp++;
    if (changes & NEW_C)
    {
        /* 
         * Make sure the state index is in range, then grab the 
         * state. If we have a good state index, clear the 'discard' 
         * flag. 
         */
        if (*cp >= MAX_STATES)
            goto bad;

        comp->flags = (UINT2) (comp->flags & (~SLF_TOSS));
        comp->last_recv = *cp++;
    }
    else
    {
        /* 
         * This packet has an implicit state index.  If we've had a 
         * line error since the last time we got an explicit state 
         * index, we have to toss the packet. 
         */
        if (comp->flags & SLF_TOSS)
            return ((UINT1 *) 0);
    }
    /* 
     * Find the state then fill in the TCP checksum and PUSH bit. 
     */
    cs = &comp->rstate[comp->last_recv];
    hlen = (UINT4) ((cs->cs_ip.u1VerHdrLen & 0xf0) << 2);
    th = (struct tcphdr *) (VOID *) &((UINT1 *) &cs->cs_ip)[hlen];
    th->th_sum = OSIX_HTONS (((*cp << 8) | cp[1]));
    cp += 2;
    if (changes & TCP_PUSH_BIT)
        th->th_flags |= TH_PUSH;
    else
        th->th_flags = (UINT1) (th->th_flags & (~TH_PUSH));

    /* 
     * Fix up the state's ack, seq, urg and win fields based on the 
     * changemask. 
     */
    switch (changes & SPECIALS_MASK)
    {
        case SPECIAL_I:
        {
            register UINT4      i =
                (UINT4) (OSIX_NTOHS (cs->cs_ip.ip_len) - cs->cs_hlen);
            th->th_ack = OSIX_HTONL ((OSIX_NTOHL (th->th_ack) + i));
            th->th_seq = OSIX_HTONL ((OSIX_NTOHL (th->th_seq) + i));
        }
            break;

        case SPECIAL_D:
            th->th_seq =
                OSIX_HTONL ((OSIX_NTOHL (th->th_seq) +
                             OSIX_NTOHS (cs->cs_ip.ip_len) - cs->cs_hlen));
            break;

        default:
            if (changes & NEW_U)
            {
                th->th_flags |= TH_URG;
                DECODEU (th->th_urp);
            }
            else
                th->th_flags = (UINT1) (th->th_flags & (~TH_URG));
            if (changes & NEW_W)
                DECODES (th->th_win) if (changes & NEW_A)
                    DECODEL (th->th_ack) if (changes & NEW_S)
                        DECODEL (th->th_seq) break;
    }
    /* Update the IP ID */
    if (changes & NEW_I)
    {
    DECODES (cs->cs_ip.ip_id)}
    else
        cs->cs_ip.ip_id = OSIX_HTONS ((OSIX_NTOHS (cs->cs_ip.ip_id) + 1));

    /* 
     * At this point, cp points to the first byte of data in the packet. 
     * If we're not aligned on a 4-byte boundary, copy the data down so 
     * the IP & TCP headers will be aligned.  Then back up cp by the 
     * TCP/IP header length to make room for the reconstructed header (we 
     * assume the packet we were handed has enough space to prepend 128 
     * bytes of header).  Adjust the lenth to account for the new header 
     * & fill in the IP total length. 
     */
    len -= (cp - bufp);
    if (len < 0)
        /* 
         * we must have dropped some characters (crc should detect 
         * this but the old slip framing won't) 
         */
        goto bad;

 /***********
        if ((int) cp & 3) { 
             if (len > 0) 
                  OVBCOPY(cp, (int) cp & ~3, len); 
             cp = (UINT1 *) ((int) cp & ~3); 
        } 
************/
    cp -= cs->cs_hlen;
    len += cs->cs_hlen;
    *Length = (UINT4) len;        /*length of the total packet - */
    cs->cs_ip.ip_len = OSIX_HTONS (len);
    BCOPY (&cs->cs_ip, cp, cs->cs_hlen);

    /* recompute the ip header checksum */
    {
        register UINT2     *bp = (UINT2 *) (VOID *) cp;
        for (changes = 0; hlen > 0; hlen -= 2)
            changes += *bp++;
        changes = (changes & 0xffff) + (changes >> 16);
        changes = (changes & 0xffff) + (changes >> 16);
        ((struct ip *) (VOID *) cp)->ip_sum = (UINT2) (~changes);
    }
    return (cp);

  bad:
    *Length = (UINT4) len;        /*length of the total packet - */
    comp->flags |= SLF_TOSS;
    return ((UINT1 *) 0);
}

/************************************************/

void
sl_compress_init (comp)
     struct slcompress  *comp;
{
    register UINT4      i;
    register struct cstate *tstate = comp->tstate;

    /* 
     * Clean out any junk left from the last time line was used. 
     */
    MEMSET ((char *) comp, 0, sizeof (*comp));
    /* 
     * Link the transmit states into a circular list. 
     */
    for (i = MAX_STATES - 1; i > 0; --i)
    {
        tstate[i].cs_id = (UINT1) i;
        tstate[i].cs_next = &tstate[i - 1];
    }
    tstate[0].cs_next = &tstate[MAX_STATES - 1];
    tstate[0].cs_id = 0;
    comp->last_cs = &tstate[0];
    /* 
     * Make sure we don't accidentally do CID compression 
     * (assumes MAX_STATES < 255). 
     */
    comp->last_recv = 255;
    comp->last_xmit = 255;
    comp->flags = SLF_TOSS;

}
