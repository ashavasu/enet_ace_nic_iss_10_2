/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppipcp.c,v 1.12 2014/12/16 11:47:24 siva Exp $
 *
 * Description:This file contains callback routines of IPCP module.
 *             It also contains routines for the input IPCP packet 
 *             processing.
 *
 *******************************************************************/
#define VJ_OPT_LEN 6

#include "ipcpcom.h"
#include "gsemdefs.h"
#include "gcpdefs.h"
#include "genexts.h"
#include "lcpdefs.h"
#include "ipexts.h"
#include "mpdefs.h"
#include "pppvj.h"
#include "vjproto.h"
#include "globexts.h"
#include "genproto.h"

/******************** GLOBALS ************************************/
UINT1               VJCompAvailable = PPP_YES;
t_SLL               AddrPoolList;
UINT1               gu1IPCPAddrModeSelector;
UINT2               IpcpAllocCounter;
UINT2               IpcpFreeCounter;
UINT2               IpcpIfFreeCounter;

tIPCPCtrlBlk        IPCPCtrlBlk;

tOptHandlerFuns     IPAddrHandlingFuns = {
    IPCPProcessIPAddrConfReq,
    IPCPProcessIPAddrConfNak,
    IPCPProcessIPAddrConfRej,
    IPCPReturnIPAddrPtr,
    NULL
};

tOptHandlerFuns     IPCompHandlingFuns = {
    IPCPProcessCompressionConfReq,
    IPCPProcessCompressionConfNak,
    IPCPProcessCompressionConfRej,
    IPCPReturnCompPtr,
    IPCPAddCompSubOpt
};

tOptHandlerFuns     PrimaryDNSAddrHandlingFuns = {
    IPCPProcessPrimaryDNSAddrConfReq,
    IPCPProcessPrimaryDNSAddrConfNak,
    IPCPProcessPrimaryDNSAddrConfRej,
    IPCPReturnPrimaryDNSAddrPtr,
    NULL
};

tOptHandlerFuns     PrimaryNBNSAddrHandlingFuns = {
    IPCPProcessPrimaryNBNSAddrConfReq,
    IPCPProcessPrimaryNBNSAddrConfNak,
    IPCPProcessPrimaryNBNSAddrConfRej,
    IPCPReturnPrimaryNBNSAddrPtr,
    NULL
};

tOptHandlerFuns     SecondaryDNSAddrHandlingFuns = {
    IPCPProcessSecondaryDNSAddrConfReq,
    IPCPProcessSecondaryDNSAddrConfNak,
    IPCPProcessSecondaryDNSAddrConfRej,
    IPCPReturnSecondaryDNSAddrPtr,
    NULL
};

tOptHandlerFuns     SecondaryNBNSAddrHandlingFuns = {
    IPCPProcessSecondaryNBNSAddrConfReq,
    IPCPProcessSecondaryNBNSAddrConfNak,
    IPCPProcessSecondaryNBNSAddrConfRej,
    IPCPReturnSecondaryNBNSAddrPtr,
    NULL
};

tGSEMCallbacks      IpcpGSEMCallback = {
    IPCPUp,
    IPCPDown,
    NULL,
    IPCPFinished,
    NCPRxData,
    NCPTxData,
    NULL,
    NULL
};

tIPCPCompDiscVal    IPCPCompDiscVal =
    { MAX_IPCP_COMP_DISCR_VALUES, {0x002d, 0x0061}
};
tRangeInfo          IPAddrRange = { {0}
, {0xffffffff}
};
tGenOptionInfo      IPCPGenOptionInfo[] = {

    {IP_COMP_OPTION, DISCRETE, BYTE_LEN_2, 4, &IPCompHandlingFuns,
     {(VOID *) &IPCPCompDiscVal}, NOT_SET, 0, 0}
    ,
    {IP_ADDR_OPTION, RANGE, BYTE_LEN_4, 6, &IPAddrHandlingFuns, {&IPAddrRange}
     , NOT_SET, 0, 0}
    ,

    {PRIMARY_DNS_ADDR_OPTION, RANGE, BYTE_LEN_4, 6,
     &PrimaryDNSAddrHandlingFuns, {&IPAddrRange}, NOT_SET, 0, 0}
    ,

    {PRIMARY_NBNS_ADDR_OPTION, RANGE, BYTE_LEN_4, 6,
     &PrimaryNBNSAddrHandlingFuns, {&IPAddrRange}
     , NOT_SET, 0, 0}
    ,

    {SECONDARY_DNS_ADDR_OPTION, RANGE, BYTE_LEN_4, 6,
     &SecondaryDNSAddrHandlingFuns, {&IPAddrRange}
     , NOT_SET, 0, 0}
    ,

    {SECONDARY_NBNS_ADDR_OPTION, RANGE, BYTE_LEN_4, 6,
     &SecondaryNBNSAddrHandlingFuns, {&IPAddrRange}
     , NOT_SET, 0, 0}
};

tOptNegFlagsPerIf   IPCPOptPerIntf[] = {
    {IP_COMP_OPTION, ALLOW_FORCED_NAK_SET | ALLOW_REJECT_SET, 0}
    ,
    {IP_ADDR_OPTION, ALLOWED_FOR_PEER_SET | ALLOW_REJECT_SET | DESIRED_SET, 0}
    ,
    {PRIMARY_DNS_ADDR_OPTION, ALLOW_REJECT_SET | DESIRED_SET | ALLOWED_FOR_PEER_SET, 0}    /* DSL_ADD */
    ,
    {PRIMARY_NBNS_ADDR_OPTION, ALLOW_FORCED_NAK_SET | ALLOW_REJECT_SET, 0}
    ,
    {SECONDARY_DNS_ADDR_OPTION, ALLOW_REJECT_SET | DESIRED_SET | ALLOWED_FOR_PEER_SET, 0}    /* DSL_ADD */
    ,
    {SECONDARY_NBNS_ADDR_OPTION, ALLOW_FORCED_NAK_SET | ALLOW_REJECT_SET, 0}
};

/****************************************************************************
 Function    :  PPPIPCPRegisterOptions
 Description :  This function returns the IPCP Options to register.
 Input       :  u4Option - Option ID of DNS server. 
                pPPPIPCPOptionCallBack - CallBack function for DNS option ID
                which parses the corresponding input for DNS option ID. 
 Output      :  This Routine stores the DNS option ID and related parameters 
                in structure tPPPIPCPRegOptionStruct.
 Returns     :  SUCCESS or FAILURE
*****************************************************************************/

INT1
PPPIPCPRegisterOptions (UINT4 u4Option,
                        VOID (*pPPPIPCPOptionCallBack)
                        (UINT4, UINT4, UINT4, VOID *))
{
    tTMO_SLL_NODE      *pNode = NULL;
    tPPPIPCPRegOptionStruct *pPPPIPCPRegOption = NULL;

    /* First check if valid option and valid address */
    if (u4Option < 1 || pPPPIPCPOptionCallBack == NULL)
    {
        return FAILURE;
    }

    /* Check if the option is already registered */
    TMO_SLL_Scan (&gPPPIPCPOptionRegList, pNode, tTMO_SLL_NODE *)
    {
        pPPPIPCPRegOption = (tPPPIPCPRegOptionStruct *) pNode;

        /* If option exists, then reset with new CallBack */
        if (pPPPIPCPRegOption->u4PPPIPCPOptionId == u4Option)
        {
            PPP_TRC (DEBUG_TRC, "PPPIPCPoptionsRegister function :"
                     "Reseting the Registration function \n");
            pPPPIPCPRegOption->pPPPIPCPOptionCallBack = pPPPIPCPOptionCallBack;
            return SUCCESS;
        }
    }

    /* if option is not registered, create a new entry in the List */
    if ((pPPPIPCPRegOption =
         (tPPPIPCPRegOptionStruct *) IPCP_ALLOC_REGISTEROPTION_POOL ()) == NULL)
    {
        return FAILURE;
    }

    pPPPIPCPRegOption->u4PPPIPCPOptionId = u4Option;
    pPPPIPCPRegOption->pPPPIPCPOptionCallBack = pPPPIPCPOptionCallBack;

    TMO_SLL_Add (&gPPPIPCPOptionRegList, (tTMO_SLL_NODE *) pPPPIPCPRegOption);
    return SUCCESS;
}

/****************************************************************************
 Function    :  PPPIPCPUnRegisterOptions
 Description :  This function returns the IPCP Options to de-register.
 Input       :  u4Option - Option ID of DNS server.
 Output      :  This Routine deletes the DNS option ID and related parameters
                in structure tPPPIPCPRegOptionStruct.
 Returns     :  VOID
*****************************************************************************/

VOID
PPPIPCPUnRegisterOptions (UINT4 u4PPPIPCPOption)
{
    tTMO_SLL_NODE      *pNode;
    tPPPIPCPRegOptionStruct *pPPPIPCPRegOption = NULL;

    TMO_SLL_Scan (&gPPPIPCPOptionRegList, pNode, tTMO_SLL_NODE *)
    {
        pPPPIPCPRegOption = (tPPPIPCPRegOptionStruct *) pNode;
        if (pPPPIPCPRegOption->u4PPPIPCPOptionId == u4PPPIPCPOption)
        {
            TMO_SLL_Delete (&gPPPIPCPOptionRegList, pNode);
            IPCP_FREE_REGISTEROPTION_POOL (pNode);
            return;
        }
    }
    PPP_TRC (DEBUG_TRC, "Exiting PPPIPCPoptionsUnRegister "
             "function for unregistered Option input \n");
}

/****************************************************************************
 Function    :  PPPIPCPParseRegisterOptions
 Description :  This function calls the DNS options value, type, length.
 Input       :  u4PPPIPCPOption - Option ID for IPCP option.
                u4OptionType - Option Type for Primary or secondary.
                u4Length - Length of the IP address.
                pValue - IP address Value.
 Output      :  This Routine provides the IPCP option, u4OptionType, u4Length 
                and pValue in Callback function for register options and 
                stores the value in gPPPIPCPOptionRegList.
 Returns     :  VOID
*****************************************************************************/

VOID
PPPIPCPParseRegisterOptions (UINT4 u4PPPIPCPOption, UINT4 u4OptionType,
                             UINT4 u4Length, VOID *pValue)
{
    tTMO_SLL_NODE      *pNode;
    tPPPIPCPRegOptionStruct *pPPPIPCPRegOption = NULL;

    TMO_SLL_Scan (&gPPPIPCPOptionRegList, pNode, tTMO_SLL_NODE *)
    {
        pPPPIPCPRegOption = (tPPPIPCPRegOptionStruct *) pNode;
        if (pPPPIPCPRegOption->u4PPPIPCPOptionId == u4PPPIPCPOption)
        {
            pPPPIPCPRegOption->pPPPIPCPOptionCallBack (u4PPPIPCPOption,
                                                       u4OptionType,
                                                       u4Length, pValue);
        }
    }
}

/***********************************************************************/
/*                      INTERFACE  FUNCTIONS                           */
/***********************************************************************
*  Function Name : IPCPCreateIf
*  Description   :
*          This procedure creates an entry in the IPCP interface data 
*  base corresponding to IfId. It also initializes the created entry to the 
*  default values  by calling the IPCPinit() function.
*  Parameter(s)  :
*         pIf -    pointer to the PPP interface structure
*  Return Values : 
*      pointer to the newly created  interface structure , if the creation is 
*        success
*      NULL if it fails to create the interface.
*********************************************************************/
tIPCPIf            *
IPCPCreateIf (tPPPIf * pIf)
{
    tIPCPIf            *pIpcpPtr;
    if ((pIpcpPtr = (tIPCPIf *) (VOID *) ALLOC_IPCP_IF ()) != NULL)
    {
        if (IPCPInit (pIpcpPtr) != NOT_OK)
        {
            pIpcpPtr->IpcpGSEM.pIf = pIf;
            return (pIpcpPtr);
        }
        else
        {

            PPP_TRC (ALL_FAILURE, "Error: IPCP Init failure .\n");
            FREE_IPCP_IF (pIpcpPtr);
            return (NULL);
        }
    }
    PPP_TRC (ALL_FAILURE, "Error: IPCP Alloc/Init failure .\n");
    return (NULL);
}

/*********************************************************************
*  Function Name : IPCPDeleteIf
*  Description   :
*               This procedure deletes an IPCP interface  entry from the 
*  database and deallocates memory corresponding to the interface.
*  Parameter(s)  :
*         pIpcpPtr -  points to  the IPCP interface structure to be deleted.
*  Return Values : VOID
*********************************************************************/
VOID
IPCPDeleteIf (tIPCPIf * pIpcpPtr)
{
    pIpcpPtr->IpcpGSEM.pIf->pIpcpPtr = NULL;
    GSEMDelete (&(pIpcpPtr->IpcpGSEM));
    FREE_IPCP_IF (pIpcpPtr);
    return;
}

/*********************************************************************
*  Function Name : IPCPEnableIf
*  Description   :
*              This function is called when there is an  IPCP Admin Open 
*  for an interface from the SNMP It  triggers an OPEN event to the GSEM and 
*  passes the IPCP tGSEM corresponds to this interface as parameter.
*  Parameter(s)    :
*    pIpcpPtr    -    Points to the IPCP interface structure to be enabled
*    pIpcpPtrBlk    -    Points to the IPCPCtrlBlk 
*  Return Values : VOID
*********************************************************************/
VOID
IPCPEnableIf (tIPCPIf * pIpcpPtr, tIPCPCtrlBlk * pIpcpCtrlBlk)
{
    UINT1               Index;
    tPPPIf             *pIf;

    UNUSED_PARAM (pIpcpCtrlBlk);
    if (pIpcpPtr->IpcpGSEM.pIf->BundleFlag == BUNDLE)
    {
        SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
        {
            if (pIf->MPInfo.MemberInfo.pBundlePtr != NULL
                && pIf->MPInfo.MemberInfo.pBundlePtr == pIpcpPtr->IpcpGSEM.pIf)
            {
                pIf->pIpcpPtr = pIpcpPtr;
            }
        }
    }

    pIpcpPtr->IpcpGSEM.pNegFlagsPerIf[IP_ADDR_IDX].FlagMask |=
        ALLOWED_FOR_PEER_SET;
    pIpcpPtr->AdminStatus = ADMIN_OPEN;

    /* Check whether the link is already in the Operational state. 
       If so, then indicate the GSEM about the  link UP 
     */

    if (pIpcpPtr->IpcpGSEM.CurrentState != (REQSENT || ACKRCVD || ACKSENT))
    {
        MEMCPY (&pIpcpPtr->IpcpOptionsAckedByPeer,
                &pIpcpPtr->IpcpOptionsDesired, sizeof (tIPCPOptions));
        for (Index = 0; Index < MAX_IPCP_OPT_TYPES; Index++)
        {
            if (pIpcpPtr->IpcpGSEM.pNegFlagsPerIf[Index].FlagMask & DESIRED_SET)
            {
                pIpcpPtr->IpcpGSEM.pNegFlagsPerIf[Index].FlagMask |=
                    ACKED_BY_PEER_SET;
            }
            else
            {
                pIpcpPtr->IpcpGSEM.pNegFlagsPerIf[Index].FlagMask &=
                    ACKED_BY_PEER_NOT_SET;
            }
        }
    }

    GSEMRun (&pIpcpPtr->IpcpGSEM, OPEN);

    if ((pIpcpPtr->IpcpGSEM.pIf->LcpStatus.LinkAvailability == LINK_AVAILABLE)
        && (pIpcpPtr->IpcpGSEM.CurrentState != CLOSED))
    {
        GSEMRun (&pIpcpPtr->IpcpGSEM, PPP_UP);
    }

    GSEMRestartIfNeeded (&pIpcpPtr->IpcpGSEM);

    BZERO (&IPCPCtrlBlk, sizeof (tIPCPCtrlBlk));

    return;
}

/*********************************************************************
*  Function Name : IPCPDisableIf
*  Description   :
*          This function is called when there is an Admin Close for an 
*  interface from the SNMP It triggers a CLOSE event to the GSEM and passes 
*  the IPCP GSEMStructure corresponds to this interface as a parameter. 
*  Parameter(s)  :
*            pIpcpPtr - points to  the IPCP interface structure to be  disabled
*  Return Values : VOID
*********************************************************************/
VOID
IPCPDisableIf (tIPCPIf * pIpcpPtr)
{
    GSEMRun (&pIpcpPtr->IpcpGSEM, CLOSE);
    return;
}

/*********************************************************************
*  Function Name : IPCPFinished
*  Description   :
*    This function is invoked when the higher layer is done with 
*  the link and the link is no longer needed. This invokes  procedures for 
*  terminating the link.
*         The pFinished  field of the GSEMCallback structure points 
*  to this function and is called from the GSEM Module.
*  Parameter(s)  :  
*            pGSEM  - pointer to IPCP tGSEM corresponding 
*  to current interface
*  Return Value  :    VOID
*********************************************************************/
VOID
IPCPFinished (tGSEM * pGSEM)
{
    tIPCPIf            *pIpcpPtr;
    UINT1               Index;
    pIpcpPtr = pGSEM->pIf->pIpcpPtr;
    MEMCPY (&pIpcpPtr->IpcpOptionsAckedByPeer, &pIpcpPtr->IpcpOptionsDesired,
            sizeof (tIPCPOptions));
    for (Index = 0; Index < MAX_IPCP_OPT_TYPES; Index++)
    {
        if (pGSEM->pNegFlagsPerIf[Index].FlagMask & DESIRED_SET)
        {
            pGSEM->pNegFlagsPerIf[Index].FlagMask |= ACKED_BY_PEER_SET;
        }
        else
        {
            pGSEM->pNegFlagsPerIf[Index].FlagMask &= ACKED_BY_PEER_NOT_SET;
        }
    }
    return;
}

/*********************************************************************
*  Function Name : IPCPInit
*  Description   :
*              This function initializes the IPCP GSEM and  sets 
*  the  IPCP configuration option variables to their default values. It is 
*  invoked by the IPCPCreateIf() function.
*  Parameter(s)  :
*        pIpcpPtr -   points to the IPCP interface structure
*  Return Values : OK/NOT_OK 
*********************************************************************/
INT1
IPCPInit (tIPCPIf * pIpcpPtr)
{

    IPAddrRange.MinVal.LongVal = 0;
    IPAddrRange.MaxVal.LongVal = 0xffffffff;
    /* Initialize the Option fields */
    IPCPOptionInit (&pIpcpPtr->IpcpOptionsDesired);
    IPCPOptionInit (&pIpcpPtr->IpcpOptionsAllowedForPeer);
    IPCPOptionInit (&pIpcpPtr->IpcpOptionsAckedByPeer);
    IPCPOptionInit (&pIpcpPtr->IpcpOptionsAckedByLocal);

    pIpcpPtr->AdminStatus = STATUS_DOWN;
    pIpcpPtr->OperStatus = STATUS_DOWN;

    if (GSEMInit (&pIpcpPtr->IpcpGSEM, MAX_IPCP_OPT_TYPES) == NOT_OK)
    {
        return (NOT_OK);
    }
    pIpcpPtr->IpcpGSEM.Protocol = IPCP_PROTOCOL;
    pIpcpPtr->IpcpGSEM.pCallbacks = &IpcpGSEMCallback;
    pIpcpPtr->IpcpGSEM.pGenOptInfo = IPCPGenOptionInfo;
    MEMCPY (pIpcpPtr->IpcpGSEM.pNegFlagsPerIf, &IPCPOptPerIntf,
            (sizeof (tOptNegFlagsPerIf) * MAX_IPCP_OPT_TYPES));
    return (OK);
}

/***********************************************************************/
/*                      INTERNAL  FUNCTIONS                            */
/***********************************************************************
*  Function Name : IPCPOptionInit
*  Description   :
*               This function initializes the IPCPOption structure and is
*  called by the IPCPInit function.
*  Parameter(s)  :
*       pIPCPOpt   -  points to the IPCPOption structure  to be initialized.
*  Return Value  :
*********************************************************************/
VOID
IPCPOptionInit (tIPCPOptions * pIPCPOpt)
{
    pIPCPOpt->CompInfo.VJInfo.CompSlotId = DEFAULT_COMP_SLOT_ID;
    pIPCPOpt->CompInfo.VJInfo.MaxSlotId = DEFAULT_MAX_SLOT_ID;
    pIPCPOpt->IPAddress = 0;
    pIPCPOpt->PrimaryDNSAddress = 0;
    pIPCPOpt->PrimaryNBNSAddress = 0;
    pIPCPOpt->SecondaryDNSAddress = 0;
    pIPCPOpt->SecondaryNBNSAddress = 0;
    return;
}

/*********************************************************************
*  Function Name : IPCPUp
*  Description   :
*              This function is called once the network level configuration  
*  option is negotiated  successfully. This function indicates  to the higher 
*  layers  that the IPCP is up.  The pUp  field of  the IPCPGSEMCallback points *  to this function and is called from the GSEM Module.
*  Parameter(s)  :
*            pIpcpGSEM  - pointer to the IPCP tGSEM 
*                                   corresponding to the current interface.
*  Return Values : VOID 
*********************************************************************/
VOID
IPCPUp (tGSEM * pIpcpGSEM)
{
    tIPCPIf            *pIpcpIf;
#ifdef NPAPI_WANTED
    UINT2               u2VlanId = 0;
    UINT1               au1DestMac[HOST_ADDR_LEN];
    UINT4               u4TblFull = FNP_FALSE;
    UINT1               u1PhyIfType = 0;
    INT4                i4PhyIndex = 0;
#endif

    pIpcpIf = (tIPCPIf *) pIpcpGSEM->pIf->pIpcpPtr;
    pIpcpIf->OperStatus = STATUS_UP;

    if ((pIpcpGSEM->pNegFlagsPerIf[IP_COMP_IDX].FlagMask & ACKED_BY_LOCAL_MASK)
        || (pIpcpGSEM->pNegFlagsPerIf[IP_COMP_IDX].
            FlagMask & ACKED_BY_PEER_MASK))
    {
        pIpcpGSEM->pIf->pVjComp =
            (struct slcompress *) VJCOMPCreate (pIpcpGSEM->pIf);
    }
#ifdef NPAPI_WANTED
    PPPoEGetDestMacFromPPPIfIdx (pIpcpGSEM->pIf->LinkInfo.IfIndex, au1DestMac);
#ifdef BCMX_WANTED
    u2VlanId =
        (UINT2) FsNpGetDummyVlanId (pIpcpGSEM->pIf->LinkInfo.PhysIfIndex);
#endif
    getPhysicalIndexOfPPP (pIpcpGSEM->pIf->LinkInfo.IfIndex, &i4PhyIndex);
    CfaGetIfType ((UINT4) i4PhyIndex, &u1PhyIfType);
    if (u1PhyIfType != CFA_HDLC)
    {
        if (FNP_FAILURE == PppFsNpIpv4ArpAdd (u2VlanId,
                                              pIpcpGSEM->pIf->LinkInfo.PhysIfIndex,
                                              pIpcpIf->IpcpOptionsAckedByLocal.
                                              IPAddress, au1DestMac, NULL,
                                              ARP_DYNAMIC, &u4TblFull))
        {
            if (FNP_TRUE == u4TblFull)
            {
                PPP_TRC (ALL_FAILURE, "Arp table is full.\n");
            }
        }
    }
#endif
    PPPHLINotifyProtStatusToHL (pIpcpGSEM->pIf, IPCP_PROTOCOL, PPP_UP);
    return;
}

/*********************************************************************
*  Function Name : IPCPDown
*  Description   :
*          This function is invoked  when the IPCP protocol is leaving the 
*  OPENED state. It  indicates the higher layers  about the operational status 
*  of the IPCP.  The pDown  field of  the IPCPGSEMCallback structure  points to 
*  this function and is called from the GSEM Module.
*  Parameter(s)  :
*            pIpcpGSEM  - pointer to the IPCP tGSEM 
*                       corresponding to the current interface.
*  Return Values : VOID 
*********************************************************************/
VOID
IPCPDown (tGSEM * pIpcpGSEM)
{
    tPPPIf             *pIf;
    tIPCPIf            *pIpcpIf;
    tIPCPAddrPool      *pAddrPool;
    tIPCPAllocAddrNode *pNode;
#ifdef NPAPI_WANTED
    UINT1               u1PhyIfType = 0;
    INT4                i4PhyIndex = 0;
#endif

    pIf = pIpcpGSEM->pIf;
    pIpcpIf = (tIPCPIf *) pIpcpGSEM->pIf->pIpcpPtr;

    SLL_SCAN (&AddrPoolList, pAddrPool, tIPCPAddrPool *)
    {
        if (pIpcpIf->IpcpOptionsAckedByLocal.IPAddress >= pAddrPool->u4LowerAddr
            && pIpcpIf->IpcpOptionsAckedByLocal.IPAddress <=
            pAddrPool->u4UpperAddr)
        {
            SLL_SCAN (&pAddrPool->AllocAddrList, pNode, tIPCPAllocAddrNode *)
            {
                if (pNode->u4Port == pIf->LinkInfo.IfIndex &&
                    pNode->u4Addr == pIpcpIf->IpcpOptionsAckedByLocal.IPAddress)
                {
                    break;
                }                /* If */
            }                    /* SLL_SCAN */
            if (pNode != NULL)
            {
                SLL_DELETE (&pAddrPool->AllocAddrList, (t_SLL_NODE *) pNode);
                IPCP_FREE_NODE (pNode);
                pAddrPool->u4NoOfFreeAddr++;
                break;
            }
        }                        /* if */
    }                            /* SLL_SCAN */

#ifdef NPAPI_WANTED
    getPhysicalIndexOfPPP (pIf->LinkInfo.IfIndex, &i4PhyIndex);
    CfaGetIfType ((UINT4) i4PhyIndex, &u1PhyIfType);
    if (u1PhyIfType != CFA_HDLC)
    {
        PppFsNpIpv4ArpDel (pIpcpIf->IpcpOptionsAckedByLocal.IPAddress,
                           NULL, ARP_DYNAMIC);
    }
#endif
    PPPHLINotifyProtStatusToHL (pIf, IPCP_PROTOCOL, PPP_DOWN);
    IPCPFinished (pIpcpGSEM);
    pIpcpIf->OperStatus = STATUS_DOWN;

    MEMCPY (&pIpcpIf->IpcpOptionsAckedByPeer, &pIpcpIf->IpcpOptionsDesired,
            sizeof (tIPCPOptions));
    return;
}

/***********************************************************************
    OPTION SPECIFIC FUNCTIONS FOR PROCESSING CONF_REQ
************************************************************************/
/*********************************************************************
*  Function Name : IPCPProcessIPAddrConfReq
*  Description   :
*        This function is invoked to process the IP Address option present
*  in the CONF_REQ and to construct appropriate response
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer
*     tOptVal    -  Option Value received  to be processed
*     Offset     -  indicates the place in the outgoing buffer at which 
*                   the option value is appended
*  PresenceFlag  -  indicates whether the option is present in the recd.
*                   CONF_REQ packet
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer
*********************************************************************/
INT1
IPCPProcessIPAddrConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal,
                          UINT2 Offset, UINT1 PresenceFlag)
{
    tIPCPIf            *pIpcpPtr;

    UINT4               u4AddrFromPool;

    PPP_UNUSED (pInPkt);

    pIpcpPtr = pGSEM->pIf->pIpcpPtr;
    PPP_TRC1 (CONTROL_PLANE, "\t\t\tIP ADDR: %x", OptVal.LongVal);
/****** SKR002 *******/
/* the condition pIpcpPtr->IpcpOptionsAllowedForPeer.IpAddress!= 0 is added 
for IPCP remote address negotiation *********/

/* If there are free IP addresses, one of them is picked and assigned
 * to the peer. If there are no free IP addresses, then the original
 * logic is retained- i.e to take from options allowed for peer structure */
    if (PresenceFlag == SET && OptVal.LongVal == 0)
    {
        if (IPCPGetFreeAddrFromPool (pGSEM->pIf, &u4AddrFromPool) == SUCCESS)
        {
            u4AddrFromPool = OSIX_HTONL (u4AddrFromPool);
            CRU_BUF_Copy_OverBufChain (pGSEM->pOutParam,
                                       (UINT1 *) &u4AddrFromPool, Offset, 4);
            /* RFC007 ASSIGN4BYTE(pGSEM->pOutParam, Offset, *u4AddrFromPool); */
            return (BYTE_LEN_4);
        }
        else
        {
            if (pIpcpPtr->IpcpOptionsAllowedForPeer.IPAddress == 0)
            {
                return DISCARD;
            }
        }
    }

    if ((PresenceFlag == NOT_SET) && (ExtractedLength != IP_ADDR_LEN))
        return DISCARD;

    if ((PresenceFlag == NOT_SET)
        || ((PresenceFlag == SET)
            && (pIpcpPtr->IpcpOptionsAllowedForPeer.IPAddress != 0)
            && (pIpcpPtr->IpcpOptionsAllowedForPeer.IPAddress !=
                OptVal.LongVal)))
    {
        ASSIGN4BYTE (pGSEM->pOutParam, Offset,
                     pIpcpPtr->IpcpOptionsAllowedForPeer.IPAddress);
        return (BYTE_LEN_4);
    }

    pIpcpPtr->IpcpOptionsAckedByLocal.IPAddress = OptVal.LongVal;
    return (0);
}

/*********************************************************************
*  Function Name : IPCPProcessCompressionConfReq
*  Description   :
*          This function processes the VJ Compression Option in the 
*  CONF_REQ packet and builds appropriate response.
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer
*     tOptVal    -  Option Value received  to be processed
*     Offset     -  indicates the place in the outgoing buffer at which 
*                   the option value is appended
*  PresenceFlag  -  indicates whether the option is present in the recd.
*                   CONF_REQ packet
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer
*********************************************************************/
INT1
IPCPProcessCompressionConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt,
                               tOptVal OptVal, UINT2 Offset, UINT1 PresenceFlag)
{
    tIPCPIf            *pIpcpPtr;
    tVJInfo             VJInfo;
    UINT1               u1RecdOptLen;
    tIPHCInfo           IPHCInfo;

    MEMSET (&IPHCInfo, 0, sizeof (tIPHCInfo));
    MEMSET (&VJInfo, 0, sizeof (tVJInfo));
    pIpcpPtr = pGSEM->pIf->pIpcpPtr;

    u1RecdOptLen = ExtractedLength;

    if ((PresenceFlag == SET) && (OptVal.ShortVal == VJ_COMP_PROTOCOL))
    {

        PPP_TRC (CONTROL_PLANE, "\t\t\tCOMPRESSION : VJ");
        if (ExtractedLength != VJ_OPT_LEN)
        {
            return (DISCARD);
        }

        EXTRACT1BYTE (pInPkt, VJInfo.MaxSlotId);
        EXTRACT1BYTE (pInPkt, VJInfo.CompSlotId);

        if (((VJInfo.MaxSlotId >= MIN_MAX_SLOT_ID)
             && (VJInfo.MaxSlotId <= MAX_MAX_SLOT_ID))
            && ((VJInfo.CompSlotId == COMP_SLOT_ID_COMPRESSED)
                || (VJInfo.CompSlotId == COMP_SLOT_ID_NOT_COMPRESSED)))
        {
            MEMCPY (&pIpcpPtr->IpcpOptionsAckedByLocal.CompInfo.VJInfo,
                    &VJInfo, sizeof (tVJInfo));
            return (ACK);
        }

    }

    if (OptVal.ShortVal == VJ_COMP_PROTOCOL)
    {
        MEMCPY (&VJInfo, &pIpcpPtr->IpcpOptionsAllowedForPeer.CompInfo.VJInfo,
                sizeof (tVJInfo));
        ASSIGN2BYTE (pGSEM->pOutParam, Offset, IP_COMP_TCP_DATA);
        ASSIGN1BYTE (pGSEM->pOutParam, Offset + 2, VJInfo.MaxSlotId);
        ASSIGN1BYTE (pGSEM->pOutParam, Offset + 3, VJInfo.CompSlotId);
        return (BYTE_LEN_4);
    }

    if ((PresenceFlag == SET) && (OptVal.ShortVal == IPHC_PROTOCOL))
    {

        PPP_TRC (CONTROL_PLANE, "\t\t\tCOMPRESSION : IPHC");
        if (u1RecdOptLen < MIN_IPHC_OPT_LEN)
        {
            return (DISCARD);
        }

        EXTRACT2BYTE (pInPkt, IPHCInfo.u2TcpSpace);
        EXTRACT2BYTE (pInPkt, IPHCInfo.u2NonTcpSpace);
        EXTRACT2BYTE (pInPkt, IPHCInfo.u2FMaxPeriod);
        EXTRACT2BYTE (pInPkt, IPHCInfo.u2FMaxTime);
        EXTRACT2BYTE (pInPkt, IPHCInfo.u2MaxHdr);
        IPHCInfo.u2RtpSubOptFlag = PPP_FALSE;

        if (u1RecdOptLen > MIN_IPHC_OPT_LEN)
        {
            UINT1               u1RemLen, u1Type, u1SubOptLen;

            u1RemLen = (UINT1) (u1RecdOptLen - MIN_IPHC_OPT_LEN);
            while (u1RemLen)
            {
                EXTRACT1BYTE (pInPkt, u1Type);
                EXTRACT1BYTE (pInPkt, u1SubOptLen);
                if ((u1Type == RTP_SUBOPT_TYPE)
                    && (u1SubOptLen == RTP_SUBOPT_TYPE_LEN))
                {
                    IPHCInfo.u2RtpSubOptFlag = PPP_TRUE;
                }
                u1RemLen = (UINT1) (u1RemLen - u1SubOptLen);
            }
        }

        if ((IPHCInfo.u2TcpSpace < MAX_IPHC_TCP_SPACE)
            && (IPHCInfo.u2NonTcpSpace < MAX_IPHC_NON_TCP_SPACE))
        {
            pIpcpPtr->IpcpOptionsAckedByLocal.u2CompProto = IPHC_PROTOCOL;
            MEMCPY (&pIpcpPtr->IpcpOptionsAckedByLocal.CompInfo.IPHCInfo,
                    &IPHCInfo, sizeof (tIPHCInfo));
            return (ACK);
        }

    }

    if (OptVal.ShortVal == IPHC_PROTOCOL)
    {
        memcpy (&(pIpcpPtr->IpcpOptionsAllowedForPeer.CompInfo.IPHCInfo),
                &IPHCInfo, sizeof (tIPHCInfo));

        ASSIGN2BYTE (pGSEM->pOutParam, Offset, IPHC_PROTOCOL);
        Offset = (UINT2) (Offset + 2);

        ASSIGN2BYTE (pGSEM->pOutParam, Offset, IPHCInfo.u2TcpSpace);
        Offset = (UINT2) (Offset + 2);

        ASSIGN2BYTE (pGSEM->pOutParam, Offset, IPHCInfo.u2NonTcpSpace);
        Offset = (UINT2) (Offset + 2);

        ASSIGN2BYTE (pGSEM->pOutParam, Offset, IPHCInfo.u2FMaxPeriod);
        Offset = (UINT2) (Offset + 2);

        ASSIGN2BYTE (pGSEM->pOutParam, Offset, IPHCInfo.u2FMaxTime);
        Offset = (UINT2) (Offset + 2);

        ASSIGN2BYTE (pGSEM->pOutParam, Offset, IPHCInfo.u2MaxHdr);
        Offset = (UINT2) (Offset + 2);

        if (IPHCInfo.u2RtpSubOptFlag == PPP_TRUE)
        {
            ASSIGN1BYTE (pGSEM->pOutParam, Offset, RTP_SUBOPT_TYPE);
            Offset = (UINT2) (Offset + 1);
            ASSIGN1BYTE (pGSEM->pOutParam, Offset, RTP_SUBOPT_TYPE_LEN);
            return (MIN_IPHC_OPT_LEN + RTP_SUBOPT_TYPE_LEN);
        }

        return (MIN_IPHC_OPT_LEN);

    }
    return DISCARD;
}

/*********************************************************************
*  Function Name : IPCPProcessPrimaryDNSAddrConfReq
*  Description   :
*        This function is invoked to process the Primary DNS Address option present
*  in the CONF_REQ and to construct appropriate response
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer
*     tOptVal    -  Option Value received  to be processed
*     Offset     -  indicates the place in the outgoing buffer at which 
*                   the option value is appended
*  PresenceFlag  -  indicates whether the option is present in the recd.
*                   CONF_REQ packet
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer
*********************************************************************/
INT1
IPCPProcessPrimaryDNSAddrConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt,
                                  tOptVal OptVal, UINT2 Offset,
                                  UINT1 PresenceFlag)
{
    tIPCPIf            *pIpcpPtr;
    pIpcpPtr = pGSEM->pIf->pIpcpPtr;
    PPP_UNUSED (pInPkt);
    if ((PresenceFlag == NOT_SET) || ((PresenceFlag == SET) && (pIpcpPtr->IpcpOptionsAllowedForPeer.PrimaryDNSAddress != OptVal.LongVal)    /* DSL_ADD */
                                      && (pIpcpPtr->IpcpOptionsAllowedForPeer.
                                          PrimaryDNSAddress != 0)))
    {
        ASSIGN4BYTE (pGSEM->pOutParam, Offset,
                     pIpcpPtr->IpcpOptionsAllowedForPeer.PrimaryDNSAddress);
        return (BYTE_LEN_4);
    }

    pIpcpPtr->IpcpOptionsAckedByLocal.PrimaryDNSAddress = OptVal.LongVal;
    return (0);
}

/*********************************************************************
*  Function Name : IPCPProcessPrimaryNBNSAddrConfReq
*  Description   :
*        This function is invoked to process the Primary NBNS Address option present
*  in the CONF_REQ and to construct appropriate response
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer
*     tOptVal    -  Option Value received  to be processed
*     Offset     -  indicates the place in the outgoing buffer at which 
*                   the option value is appended
*  PresenceFlag  -  indicates whether the option is present in the recd.
*                   CONF_REQ packet
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer
*********************************************************************/
INT1
IPCPProcessPrimaryNBNSAddrConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt,
                                   tOptVal OptVal, UINT2 Offset,
                                   UINT1 PresenceFlag)
{
    tIPCPIf            *pIpcpPtr;

    pIpcpPtr = pGSEM->pIf->pIpcpPtr;
    PPP_UNUSED (pInPkt);
    if ((PresenceFlag == NOT_SET)
        || ((PresenceFlag == SET)
            && (pIpcpPtr->IpcpOptionsAllowedForPeer.PrimaryNBNSAddress !=
                OptVal.LongVal)))
    {
        ASSIGN4BYTE (pGSEM->pOutParam, Offset,
                     pIpcpPtr->IpcpOptionsAllowedForPeer.PrimaryNBNSAddress);
        return (BYTE_LEN_4);
    }

    pIpcpPtr->IpcpOptionsAckedByLocal.PrimaryNBNSAddress = OptVal.LongVal;
    return (0);
}

/*********************************************************************
*  Function Name : IPCPProcessSecondaryDNSAddrConfReq
*  Description   :
*        This function is invoked to process the Secondary DNS Address option present
*  in the CONF_REQ and to construct appropriate response
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer
*     tOptVal    -  Option Value received  to be processed
*     Offset     -  indicates the place in the outgoing buffer at which 
*                   the option value is appended
*  PresenceFlag  -  indicates whether the option is present in the recd.
*                   CONF_REQ packet
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer
*********************************************************************/
INT1
IPCPProcessSecondaryDNSAddrConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt,
                                    tOptVal OptVal, UINT2 Offset,
                                    UINT1 PresenceFlag)
{
    tIPCPIf            *pIpcpPtr;
    pIpcpPtr = pGSEM->pIf->pIpcpPtr;
    PPP_UNUSED (pInPkt);
    if ((PresenceFlag == NOT_SET) || ((PresenceFlag == SET) && (pIpcpPtr->IpcpOptionsAllowedForPeer.SecondaryDNSAddress != OptVal.LongVal)    /* DSL_ADD */
                                      && (pIpcpPtr->IpcpOptionsAllowedForPeer.
                                          PrimaryDNSAddress != 0)))
    {
        ASSIGN4BYTE (pGSEM->pOutParam, Offset,
                     pIpcpPtr->IpcpOptionsAllowedForPeer.SecondaryDNSAddress);
        return (BYTE_LEN_4);
    }

    pIpcpPtr->IpcpOptionsAckedByLocal.SecondaryDNSAddress = OptVal.LongVal;
    return (0);
}

/*********************************************************************
*  Function Name : IPCPProcessSecondaryNBNSAddrConfReq
*  Description   :
*        This function is invoked to process the Secondary NBNS Address option present
*  in the CONF_REQ and to construct appropriate response
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer
*     tOptVal    -  Option Value received  to be processed
*     Offset     -  indicates the place in the outgoing buffer at which 
*                   the option value is appended
*  PresenceFlag  -  indicates whether the option is present in the recd.
*                   CONF_REQ packet
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer
*********************************************************************/
INT1
IPCPProcessSecondaryNBNSAddrConfReq (tGSEM * pGSEM, t_MSG_DESC * pInPkt,
                                     tOptVal OptVal, UINT2 Offset,
                                     UINT1 PresenceFlag)
{
    tIPCPIf            *pIpcpPtr;

    pIpcpPtr = pGSEM->pIf->pIpcpPtr;
    PPP_UNUSED (pInPkt);
    if ((PresenceFlag == NOT_SET)
        || ((PresenceFlag == SET)
            && (pIpcpPtr->IpcpOptionsAllowedForPeer.SecondaryNBNSAddress !=
                OptVal.LongVal)))
    {
        ASSIGN4BYTE (pGSEM->pOutParam, Offset,
                     pIpcpPtr->IpcpOptionsAllowedForPeer.SecondaryNBNSAddress);
        return (BYTE_LEN_4);
    }

    pIpcpPtr->IpcpOptionsAckedByLocal.SecondaryNBNSAddress = OptVal.LongVal;
    return (0);
}

/***********************************************************************
    OPTION SPECIFIC FUNCTIONS FOR PROCESSING CONF_NAK
************************************************************************/
/*********************************************************************
*  Function Name : IPCPProcessIPAddrConfNak 
*  Description   :
*        This function is invoked to process the IP address option present
*  in the CONF_NAK and to construct appropriate response 
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*  Return Value  :  OK/NOT_OK/CLOSE 
*********************************************************************/
INT1
IPCPProcessIPAddrConfNak (tGSEM * pGSEM, t_MSG_DESC * pInPkt, tOptVal OptVal)
{
    tIPCPIf            *pIpcpPtr;

    pIpcpPtr = pGSEM->pIf->pIpcpPtr;
    PPP_TRC1 (CONTROL_PLANE, "\t\t\tIP ADDR     : %x", OptVal.LongVal);
    PPP_UNUSED (pInPkt);
    if (pGSEM->pNegFlagsPerIf[IP_ADDR_IDX].FlagMask & ACKED_BY_PEER_MASK)
    {
        if (pIpcpPtr->IpcpOptionsAckedByPeer.IPAddress == 0)
        {
            pIpcpPtr->IpcpOptionsAckedByPeer.IPAddress = OptVal.LongVal;
        }
        else
        {
            return (CLOSE);
        }
    }
    else
    {
    /******** SKR002 ******** The if condition is added for the assignment ****/
        if (OptVal.LongVal != pIpcpPtr->IpcpOptionsAckedByPeer.IPAddress)
        {
            if (pIpcpPtr->IpcpOptionsAckedByPeer.IPAddress == 0)
            {
                pIpcpPtr->IpcpOptionsAckedByPeer.IPAddress = OptVal.LongVal;
                return (OK);
            }
            return (OptVal.LongVal == 0) ? OK : DISCARD;
        }
    }
    return (OK);
}

/*********************************************************************
*  Function Name : IPCPProcessCompressionConfNak 
*  Description   :
*        This function is invoked to process the IP address option present
*  in the CONF_NAK and to construct appropriate response 
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*   NegOptFlag   -  indicates whether the option is included in the request
*  Return Value  :  OK/NOT_OK/CLOSE 
*********************************************************************/
INT1
IPCPProcessCompressionConfNak (tGSEM * pGSEM, t_MSG_DESC * pInPkt,
                               tOptVal OptVal)
{
    tIPCPIf            *pIpcpPtr;
    tVJInfo             VJInfo;
    tIPHCInfo           IPHCInfo;
    UINT1               u1RecdOptLen;

    MEMSET (&IPHCInfo, 0, sizeof (tIPHCInfo));
    MEMSET (&VJInfo, 0, sizeof (tVJInfo));
    pIpcpPtr = pGSEM->pIf->pIpcpPtr;
    u1RecdOptLen = ExtractedLength;
    PPP_TRC (CONTROL_PLANE, "\t\t\tIPCP     :");

    if (pIpcpPtr->IpcpOptionsDesired.u2CompProto != OptVal.ShortVal)
    {
        return NOT_OK;
    }

    if (OptVal.ShortVal == VJ_COMP_PROTOCOL)
    {
        EXTRACT1BYTE (pInPkt, VJInfo.MaxSlotId);
        EXTRACT1BYTE (pInPkt, VJInfo.CompSlotId);

        if ((VJInfo.MaxSlotId < MIN_MAX_SLOT_ID)
            || (VJInfo.MaxSlotId > MAX_MAX_SLOT_ID)
            || (VJInfo.CompSlotId == COMP_SLOT_ID_COMPRESSED))
        {
            return (DISCARD);
        }

        pIpcpPtr->IpcpOptionsAckedByPeer.CompInfo.VJInfo.MaxSlotId =
            VJInfo.MaxSlotId;
        pIpcpPtr->IpcpOptionsAckedByPeer.CompInfo.VJInfo.CompSlotId =
            VJInfo.CompSlotId;

        return (OK);
    }

    if (OptVal.ShortVal == IPHC_PROTOCOL)
    {

        EXTRACT2BYTE (pInPkt, IPHCInfo.u2TcpSpace);
        EXTRACT2BYTE (pInPkt, IPHCInfo.u2NonTcpSpace);
        EXTRACT2BYTE (pInPkt, IPHCInfo.u2FMaxPeriod);
        EXTRACT2BYTE (pInPkt, IPHCInfo.u2FMaxTime);
        EXTRACT2BYTE (pInPkt, IPHCInfo.u2MaxHdr);

        if (u1RecdOptLen > MIN_IPHC_OPT_LEN)
        {
            UINT1               u1Type, u1SubOptLen;

            if ((u1RecdOptLen - MIN_IPHC_OPT_LEN) != RTP_SUBOPT_TYPE_LEN)
            {
                EXTRACT1BYTE (pInPkt, u1Type);
                EXTRACT1BYTE (pInPkt, u1SubOptLen);
                if ((u1Type != RTP_SUBOPT_TYPE)
                    && (u1SubOptLen != RTP_SUBOPT_TYPE_LEN))
                {
                    return NOT_OK;
                }
            }
        }

        if ((IPHCInfo.u2TcpSpace > 255) || (IPHCInfo.u2NonTcpSpace > 255))
        {
            return DISCARD;
        }
        MEMCPY (&pIpcpPtr->IpcpOptionsAckedByPeer.CompInfo.IPHCInfo, &IPHCInfo,
                sizeof (tIPHCInfo));

        return (OK);
    }

    return (NOT_OK);
}

/*********************************************************************
*  Function Name : IPCPProcessPrimaryDNSAddrConfNak 
*  Description   :
*        This function is invoked to process the Primary DNS address option present
*  in the CONF_NAK and to construct appropriate response 
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*  Return Value  :  OK/NOT_OK/CLOSE 
*********************************************************************/
INT1
IPCPProcessPrimaryDNSAddrConfNak (tGSEM * pGSEM, t_MSG_DESC * pInPkt,
                                  tOptVal OptVal)
{
    tIPCPIf            *pIpcpPtr;

    pIpcpPtr = pGSEM->pIf->pIpcpPtr;
    PPP_UNUSED (pInPkt);
    if (pGSEM->pNegFlagsPerIf[PRIMARY_DNS_ADDR_IDX].
        FlagMask & ACKED_BY_PEER_MASK)
    {
        if (pIpcpPtr->IpcpOptionsAckedByPeer.PrimaryDNSAddress == 0)
        {
            pIpcpPtr->IpcpOptionsAckedByPeer.PrimaryDNSAddress = OptVal.LongVal;
        }
        else
        {
            return (CLOSE);
        }
    }
    else
    {
    /****** DSL_ADD ******* The if condition is added for the assignment ****/
        if (OptVal.LongVal != pIpcpPtr->IpcpOptionsAckedByPeer.
            PrimaryDNSAddress)
        {
            if (pIpcpPtr->IpcpOptionsAckedByPeer.PrimaryDNSAddress == 0)
            {
                pIpcpPtr->IpcpOptionsAckedByPeer.PrimaryDNSAddress =
                    OptVal.LongVal;
                return (OK);
            }
            return (OptVal.LongVal == 0) ? OK : DISCARD;
        }
    }
    return (OK);
}

/*********************************************************************
*  Function Name : IPCPProcessPrimaryNBNSAddrConfNak 
*  Description   :
*        This function is invoked to process the Primary NBNS address option present
*  in the CONF_NAK and to construct appropriate response 
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*  Return Value  :  OK/NOT_OK/CLOSE 
*********************************************************************/
INT1
IPCPProcessPrimaryNBNSAddrConfNak (tGSEM * pGSEM, t_MSG_DESC * pInPkt,
                                   tOptVal OptVal)
{
    tIPCPIf            *pIpcpPtr;

    pIpcpPtr = pGSEM->pIf->pIpcpPtr;
    PPP_UNUSED (pInPkt);
    if (pGSEM->pNegFlagsPerIf[PRIMARY_NBNS_ADDR_IDX].
        FlagMask & ACKED_BY_PEER_MASK)
    {
        if (pIpcpPtr->IpcpOptionsAckedByPeer.PrimaryNBNSAddress == 0)
        {
            pIpcpPtr->IpcpOptionsAckedByPeer.PrimaryNBNSAddress =
                OptVal.LongVal;
        }
        else
        {
            return (CLOSE);
        }
    }
    else
    {
        pIpcpPtr->IpcpOptionsAckedByPeer.PrimaryNBNSAddress = OptVal.LongVal;
    }
    return (OK);
}

/*********************************************************************
*  Function Name : IPCPProcessSecondaryDNSAddrConfNak 
*  Description   :
*        This function is invoked to process the Secondary DNS address option present
*  in the CONF_NAK and to construct appropriate response 
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*  Return Value  :  OK/NOT_OK/CLOSE 
*********************************************************************/
INT1
IPCPProcessSecondaryDNSAddrConfNak (tGSEM * pGSEM, t_MSG_DESC * pInPkt,
                                    tOptVal OptVal)
{
    tIPCPIf            *pIpcpPtr;

    pIpcpPtr = pGSEM->pIf->pIpcpPtr;
    PPP_UNUSED (pInPkt);
    if (pGSEM->pNegFlagsPerIf[SECONDARY_DNS_ADDR_IDX].
        FlagMask & ACKED_BY_PEER_MASK)
    {
        if (pIpcpPtr->IpcpOptionsAckedByPeer.SecondaryDNSAddress == 0)
        {
            pIpcpPtr->IpcpOptionsAckedByPeer.SecondaryDNSAddress =
                OptVal.LongVal;
        }
        else
        {
            return (CLOSE);
        }
    }
    else
    {
    /****** DSL_ADD ******* The if condition is added for the assignment ****/
        if (OptVal.LongVal != pIpcpPtr->IpcpOptionsAckedByPeer.
            SecondaryDNSAddress)
        {
            if (pIpcpPtr->IpcpOptionsAckedByPeer.SecondaryDNSAddress == 0)
            {
                pIpcpPtr->IpcpOptionsAckedByPeer.SecondaryDNSAddress =
                    OptVal.LongVal;
                return (OK);
            }
            return (OptVal.LongVal == 0) ? OK : DISCARD;
        }
    }
    return (OK);
}

/*********************************************************************
*  Function Name : IPCPProcessSecondaryNBNSAddrConfNak 
*  Description   :
*        This function is invoked to process the Secondary NBNS address option present
*  in the CONF_NAK and to construct appropriate response 
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     pInPkt     -  points to the incoming packet buffer 
*     tOptVal    -  Option Value received  to be processed
*  Return Value  :  OK/NOT_OK/CLOSE 
*********************************************************************/
INT1
IPCPProcessSecondaryNBNSAddrConfNak (tGSEM * pGSEM, t_MSG_DESC * pInPkt,
                                     tOptVal OptVal)
{
    tIPCPIf            *pIpcpPtr;

    pIpcpPtr = pGSEM->pIf->pIpcpPtr;
    PPP_UNUSED (pInPkt);
    if (pGSEM->pNegFlagsPerIf[SECONDARY_NBNS_ADDR_IDX].
        FlagMask & ACKED_BY_PEER_MASK)
    {
        if (pIpcpPtr->IpcpOptionsAckedByPeer.SecondaryNBNSAddress == 0)
        {
            pIpcpPtr->IpcpOptionsAckedByPeer.SecondaryNBNSAddress =
                OptVal.LongVal;
        }
        else
        {
            return (CLOSE);
        }
    }
    else
    {
        pIpcpPtr->IpcpOptionsAckedByPeer.SecondaryNBNSAddress = OptVal.LongVal;
    }
    return (OK);
}

/***********************************************************************
    OPTION SPECIFIC FUNCTIONS FOR ADDING SUB_OPTIONS IN THE CONF_REQ
************************************************************************/
/*********************************************************************
*  Function Name : IPCPAddCompSubOpt
*  Description   :
*     This function is used to add VJ Compression suboption(s).
*  Input         :
*     pGSEM      -  pointer to the GSEM structure corresponding to the
*                   packet.
*     tOptVal    -  Option Value received  to be processed
*  Return Value  :  returns the number of bytes appended in the outgoing
*                   buffer
*********************************************************************/
INT1
IPCPAddCompSubOpt (tGSEM * pGSEM, tOptVal OptVal, t_MSG_DESC * pOutBuf)
{
    tIPCPIf            *pIpcpIf;

    pIpcpIf = (tIPCPIf *) pGSEM->pIf->pIpcpPtr;
    PPP_UNUSED (pOutBuf);
    PPP_UNUSED (OptVal);
    PPP_TRC (CONTROL_PLANE, "\t\t\tIPCP    :");
    if (OptVal.ShortVal == VJ_COMP_PROTOCOL)
    {
        APPEND1BYTE (pOutBuf,
                     pIpcpIf->IpcpOptionsAckedByPeer.CompInfo.VJInfo.MaxSlotId);
        APPEND1BYTE (pOutBuf,
                     pIpcpIf->IpcpOptionsAckedByPeer.CompInfo.VJInfo.
                     CompSlotId);
        return (BYTE_LEN_2);
    }

    if (OptVal.ShortVal == IPHC_PROTOCOL)
    {
        tIPHCInfo          *pIPHCInfo;
        pIPHCInfo = &pIpcpIf->IpcpOptionsAckedByPeer.CompInfo.IPHCInfo;

        APPEND2BYTE (pOutBuf, pIPHCInfo->u2TcpSpace);
        APPEND2BYTE (pOutBuf, pIPHCInfo->u2NonTcpSpace);
        APPEND2BYTE (pOutBuf, pIPHCInfo->u2FMaxPeriod);
        APPEND2BYTE (pOutBuf, pIPHCInfo->u2FMaxTime);
        APPEND2BYTE (pOutBuf, pIPHCInfo->u2MaxHdr);

        if (pIPHCInfo->u2RtpSubOptFlag == PPP_TRUE)
        {
            APPEND1BYTE (pOutBuf, RTP_SUBOPT_TYPE);
            APPEND1BYTE (pOutBuf, RTP_SUBOPT_TYPE_LEN);
            return (MIN_IPHC_OPT_LEN + RTP_SUBOPT_TYPE_LEN);
        }
        return (MIN_IPHC_OPT_LEN);
    }

    return (0);
}

/***********************************************************************
    OPTION SPECIFIC FUNCTIONS FOR RETURNING THE ADDRESS OF OPTION DATA
************************************************************************/
/*********************************************************************
*  Function Name : IPCPReturnIPAddrPtr 
*  Description   :
*     This function returns the address of the location where the IP  
*  Address value to be included in the CONF_REQ, is stored.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*     OptLen     -  used to return the length of the option value (in
*                   case the option is of 'STRING' type)
*  Return Value  :  returns the option data address
*********************************************************************/
VOID               *
IPCPReturnIPAddrPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    tIPCPIf            *pIpcpIf;
    PPP_UNUSED (OptLen);
    pIpcpIf = (tIPCPIf *) pGSEM->pIf->pIpcpPtr;
    PPP_TRC1 (CONTROL_PLANE, "\t\t\tIP ADDR   : %x",
              pIpcpIf->IpcpOptionsAckedByPeer.IPAddress);

    return (&pIpcpIf->IpcpOptionsAckedByPeer.IPAddress);
}

/*********************************************************************
*  Function Name : IPCPReturnCompPtr 
*  Description   :
*     This function returns the address of the location where the VJ Comp 
*  value to be included in the CONF_REQ, is stored.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*     OptLen     -  used to return the length of the option value (in
*                   case the option is of 'STRING' type)
*  Return Value  :  returns the option data address
*********************************************************************/
VOID               *
IPCPReturnCompPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    tIPCPIf            *pIpcpIf;
    PPP_UNUSED (OptLen);
    pIpcpIf = (tIPCPIf *) pGSEM->pIf->pIpcpPtr;
    PPP_TRC1 (CONTROL_PLANE, "\t\t\tCOMPRESSION       : VJ[%d]",
              pIpcpIf->IpcpOptionsDesired.u2CompProto);

    return (&pIpcpIf->IpcpOptionsDesired.u2CompProto);
}

/*********************************************************************
*  Function Name : IPCPReturnPrimaryDNSAddrPtr 
*  Description   :
*     This function returns the address of the location where the Primary DNS  
*  Address value to be included in the CONF_REQ, is stored.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*     OptLen     -  used to return the length of the option value (in
*                   case the option is of 'STRING' type)
*  Return Value  :  returns the option data address
*********************************************************************/
VOID               *
IPCPReturnPrimaryDNSAddrPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    tIPCPIf            *pIpcpIf;

    PPP_UNUSED (OptLen);
    pIpcpIf = (tIPCPIf *) pGSEM->pIf->pIpcpPtr;
    return (&pIpcpIf->IpcpOptionsAckedByPeer.PrimaryDNSAddress);
}

/*********************************************************************
*  Function Name : IPCPReturnPrimaryNBNSAddrPtr 
*  Description   :
*     This function returns the address of the location where the Primary NBNS  
*  Address value to be included in the CONF_REQ, is stored.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*     OptLen     -  used to return the length of the option value (in
*                   case the option is of 'STRING' type)
*  Return Value  :  returns the option data address
*********************************************************************/
VOID               *
IPCPReturnPrimaryNBNSAddrPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    tIPCPIf            *pIpcpIf;

    PPP_TRC1 (CONTROL_PLANE, "NBNS ADDR: [%d]", *OptLen);

    pIpcpIf = (tIPCPIf *) pGSEM->pIf->pIpcpPtr;
    return (&pIpcpIf->IpcpOptionsAckedByPeer.PrimaryNBNSAddress);
}

/*********************************************************************
*  Function Name : IPCPReturnSecondaryDNSAddrPtr 
*  Description   :
*     This function returns the address of the location where the Secondary DNS  
*  Address value to be included in the CONF_REQ, is stored.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*     OptLen     -  used to return the length of the option value (in
*                   case the option is of 'STRING' type)
*  Return Value  :  returns the option data address
*********************************************************************/
VOID               *
IPCPReturnSecondaryDNSAddrPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    tIPCPIf            *pIpcpIf;

    PPP_UNUSED (OptLen);
    pIpcpIf = (tIPCPIf *) pGSEM->pIf->pIpcpPtr;

    return (&pIpcpIf->IpcpOptionsAckedByPeer.SecondaryDNSAddress);
}

/*********************************************************************
*  Function Name : IPCPReturnSecondaryNBNSAddrPtr 
*  Description   :
*     This function returns the address of the location where the Secondary NBNS  
*  Address value to be included in the CONF_REQ, is stored.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*     OptLen     -  used to return the length of the option value (in
*                   case the option is of 'STRING' type)
*  Return Value  :  returns the option data address
*********************************************************************/
VOID               *
IPCPReturnSecondaryNBNSAddrPtr (tGSEM * pGSEM, UINT1 *OptLen)
{
    tIPCPIf            *pIpcpIf;

    PPP_UNUSED (OptLen);
    pIpcpIf = (tIPCPIf *) pGSEM->pIf->pIpcpPtr;

    return (&pIpcpIf->IpcpOptionsAckedByPeer.SecondaryNBNSAddress);
}

/*********************************************************************
*  Function Name : IPCPProcessIPAddrConfRej
*  Description   :
*    This function sets the default value of the rejected option.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*  Return Value  : OK
*********************************************************************/
INT1
IPCPProcessIPAddrConfRej (tGSEM * pGSEM)
{
    tIPCPIf            *pIpcpIf;
    pIpcpIf = (tIPCPIf *) pGSEM->pIf->pIpcpPtr;
    PPP_TRC1 (CONTROL_PLANE, "\t\t\tIPCP    : %ld",
              pIpcpIf->IpcpOptionsAckedByPeer.IPAddress);
    pIpcpIf->IpcpOptionsAckedByPeer.IPAddress = 0;
    return (OK);
}

/*********************************************************************
*  Function Name : IPCPProcessCompressionConfRej
*  Description   :
*   This function sets the default value of the rejected option.
*  INput         :
*     pGSEM      -  points to the GSEM structure
*  Return Value  : OK
*********************************************************************/
INT1
IPCPProcessCompressionConfRej (tGSEM * pGSEM)
{
    tIPCPIf            *pIpcpIf;
    tIPHCInfo          *pIPHCInfo;

    PPP_TRC (CONTROL_PLANE, "\t\t\tIPCP    :");
    pIpcpIf = (tIPCPIf *) pGSEM->pIf->pIpcpPtr;

    if (pIpcpIf->IpcpOptionsDesired.u2CompProto == VJ_COMP_PROTOCOL)
    {
        pIpcpIf->IpcpOptionsAckedByPeer.CompInfo.VJInfo.MaxSlotId = 0;
        pIpcpIf->IpcpOptionsAckedByPeer.CompInfo.VJInfo.CompSlotId = 0;
    }

    if (pIpcpIf->IpcpOptionsDesired.u2CompProto == IPHC_PROTOCOL)
    {

        pIPHCInfo = &pIpcpIf->IpcpOptionsAckedByPeer.CompInfo.IPHCInfo;
        pIPHCInfo->u2TcpSpace = DEFAULT_IPHC_TCP_SPACE;
        pIPHCInfo->u2NonTcpSpace = DEFAULT_IPHC_NON_TCP_SPACE;
        pIPHCInfo->u2FMaxPeriod = DEFAULT_IPHC_FULL_MAX_PERIOD;
        pIPHCInfo->u2FMaxTime = DEFAULT_IPHC_FULL_MAX_TIME;
        pIPHCInfo->u2MaxHdr = DEFAULT_IPHC_MAX_HEADER;
        pIPHCInfo->u2RtpSubOptFlag = PPP_FALSE;
    }

    return (OK);
}

/*********************************************************************
*  Function Name : IPCPProcessPrimaryDNSAddrConfRej
*  Description   :
*    This function sets the default value of the rejected option.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*  Return Value  : OK
*********************************************************************/
INT1
IPCPProcessPrimaryDNSAddrConfRej (tGSEM * pGSEM)
{
    tIPCPIf            *pIpcpIf;

    pIpcpIf = (tIPCPIf *) pGSEM->pIf->pIpcpPtr;

    pIpcpIf->IpcpOptionsAckedByPeer.PrimaryDNSAddress = 0;
    /* Makefile changes - control reaches end of non-void function */
    return (OK);
}

/*********************************************************************
*  Function Name : IPCPProcessPrimaryNBNSAddrConfRej
*  Description   :
*    This function sets the default value of the rejected option.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*  Return Value  : OK
*********************************************************************/
INT1
IPCPProcessPrimaryNBNSAddrConfRej (tGSEM * pGSEM)
{
    tIPCPIf            *pIpcpIf;

    pIpcpIf = (tIPCPIf *) pGSEM->pIf->pIpcpPtr;
    pIpcpIf->IpcpOptionsAckedByPeer.PrimaryNBNSAddress = 0;
    /* Makefile changes - control reaches end of non-void function */
    return (OK);
}

/*********************************************************************
*  Function Name : IPCPProcessSecondaryDNSAddrConfRej
*  Description   :
*    This function sets the default value of the rejected option.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*  Return Value  : OK
*********************************************************************/
INT1
IPCPProcessSecondaryDNSAddrConfRej (tGSEM * pGSEM)
{
    tIPCPIf            *pIpcpIf;

    pIpcpIf = (tIPCPIf *) pGSEM->pIf->pIpcpPtr;
    pIpcpIf->IpcpOptionsAckedByPeer.SecondaryDNSAddress = 0;
    /* Makefile changes - control reaches end of non-void function */
    return (OK);
}

/*********************************************************************
*  Function Name : IPCPProcessSecondaryNBNSAddrConfRej
*  Description   :
*    This function sets the default value of the rejected option.
*  Input         :
*     pGSEM      -  points to the GSEM structure
*  Return Value  : OK
*********************************************************************/
INT1
IPCPProcessSecondaryNBNSAddrConfRej (tGSEM * pGSEM)
{
    tIPCPIf            *pIpcpIf;

    pIpcpIf = (tIPCPIf *) pGSEM->pIf->pIpcpPtr;
    pIpcpIf->IpcpOptionsAckedByPeer.SecondaryNBNSAddress = 0;
    /* Makefile changes - control reaches end of non-void function */
    return (OK);
}

/*********************************************************************
*  Function Name : IPCPCopyOptions 
*  Description   :
*    This function is used to copy the IPCP options.
*  Input         :
*    pGSEM        -     Points to the GSEM structure
*    Flag        -     Specifies whether to alloc or free the temporary 
*                    variable.
*    PktType        -    Specifies whether the function is called from request
*                    processing or Nak processing.                  
*  Return Value        :
*                    DISCARD on allocation error, OK otherwise.
*********************************************************************/
INT1
IPCPCopyOptions (tGSEM * pGSEM, UINT1 Flag, UINT1 PktType)
{
    UINT1               Size;
    tIPCPIf            *pIpcpIf;
    tIPCPOptions       *pTemp;

    pIpcpIf = (tIPCPIf *) pGSEM->pIf->pIpcpPtr;
    Size = sizeof (tIPCPOptions);

    if (PktType == CONF_REQ_CODE)
    {
        pTemp = &pIpcpIf->IpcpOptionsAckedByLocal;
    }
    else
    {
        pTemp = &pIpcpIf->IpcpOptionsAckedByPeer;
    }

    if (Flag == NOT_SET)
    {
        if ((pCopyPtr = (UINT1 *) ALLOC_STR (Size)) == NULL)
        {
            PRINT_MEM_ALLOC_FAILURE;
            return (DISCARD);
        }
        MEMCPY (pCopyPtr, (UINT1 *) pTemp, Size);
        if (PktType == CONF_REQ_CODE)
        {
            IPCPOptionInit (&pIpcpIf->IpcpOptionsAckedByLocal);
        }
    }
    else
    {
        if (Flag == SET)
        {
            MEMCPY (pTemp, (tIPCPOptions *) (VOID *) pCopyPtr, Size);
        }
        if (pCopyPtr != NULL)
        {
            FREE_STR (pCopyPtr);
            pCopyPtr = NULL;
        }
    }
    return (OK);
}

/*********************************************************************
*  Function Name : IPCPGetSEMPtr
*  Description   :
*    This function returns the pointer to the IPCP GSEM taking the tPPPIf
*   structure as a parameter.
*  Input         :
*       pIf -   Points to the PPP Interface structure.
*  Return Value  :
*           Returns a pointer to the IPCP GSEM.
*********************************************************************/
tGSEM              *
IPCPGetSEMPtr (tPPPIf * pIf)
{
    tIPCPIf            *pIpcpIf;

    if (pIf->pIpcpPtr != NULL)
    {
        pIpcpIf = (tIPCPIf *) pIf->pIpcpPtr;
        return (&pIpcpIf->IpcpGSEM);
    }
    return (NULL);
}

INT4
IPCPCheckAssignedAddrForInterface (tPPPIf * pIf, UINT4 *pu4Addr)
{
    tIPCPAddrPool      *pPool = NULL;
    tIPCPAllocAddrNode *pNode = NULL;
    SLL_SCAN (&AddrPoolList, pPool, tIPCPAddrPool *)
    {
        SLL_SCAN (&pPool->AllocAddrList, pNode, tIPCPAllocAddrNode *)
        {
            if (pNode->u4Port == pIf->LinkInfo.IfIndex)
            {
                *pu4Addr = pNode->u4Addr;
                return SUCCESS;
            }
        }
    }
    return FAILURE;
}

INT4
IPCPGetPeerIpAddr (UINT4 *pu4Addr, UINT4 u4IfIndex)
{
    tPPPIf      *pIf = NULL;
    tIPCPIf     *pIpcpPtr = NULL;

    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if (pIf->LinkInfo.IfIndex == u4IfIndex)
        {
            pIpcpPtr = pIf->pIpcpPtr;
            *pu4Addr = pIpcpPtr->IpcpOptionsAckedByLocal.IPAddress;
            return SUCCESS;
        }
    }
    return FAILURE;
}

INT4
pppCheckFreeIPAddresses (VOID)
{
    tIPCPAddrPool      *pAddrPool;
    SLL_SCAN (&AddrPoolList, pAddrPool, tIPCPAddrPool *)
    {
        if (pAddrPool->u4NoOfFreeAddr != 0)
        {
            return SUCCESS;
        }
    }
    return FAILURE;
}

INT4
IPCPGetFreeAddrFromPool (tPPPIf * pIf, UINT4 *pu4Addr)
{

    UINT4               u4AddrIndex;
    tIPCPAddrPool      *pPool;
    tIpConfigInfo       pIpInfo;
    UINT4               u4PoolSubnet;
    UINT4               u4Subnet;

    switch (gu1IPCPAddrModeSelector)
    {

        case IPCP_LOCAL_POOL:
            if (IPCPCheckAssignedAddrForInterface (pIf, pu4Addr) == SUCCESS)
            {
                return SUCCESS;
            }
            SLL_SCAN (&AddrPoolList, pPool, tIPCPAddrPool *)
            {

                CfaIpIfGetIfInfo (pIf->LinkInfo.IfIndex, &pIpInfo);
                u4Subnet = pIpInfo.u4Addr & pIpInfo.u4NetMask;
                u4PoolSubnet = pPool->u4LowerAddr & pIpInfo.u4NetMask;

                if (u4Subnet == u4PoolSubnet)
                {
                    if (pPool->u1PoolStatus == IPCP_UP
                        && pPool->u4NoOfFreeAddr != 0)
                    {
                        for (u4AddrIndex = pPool->u4LowerAddr;
                             u4AddrIndex <= pPool->u4UpperAddr; u4AddrIndex++)
                        {
                            if (IPCPCheckIfAddrFree
                                (&pPool->AllocAddrList, u4AddrIndex) == SUCCESS)
                            {
                                if (IPCPAddAddrToAllocList
                                    (&pPool->AllocAddrList, u4AddrIndex,
                                     pIf->LinkInfo.IfIndex) == SUCCESS)
                                {
                                    *pu4Addr = u4AddrIndex;
                                    pPool->u4NoOfFreeAddr--;
                                    return SUCCESS;
                                }    /* Addr marked as allocated */
                            }    /* Addr is free */
                        }        /* for loop */
                    }            /* Pool status is up and there are free addresses */
                }
            }
            break;

        default:
            break;
    }

    return (FAILURE);

}

INT4
IPCPCheckIfAddrFree (t_SLL * pAllocAddrList, UINT4 u4Addr)
{

    tIPCPAllocAddrNode *pNode;

    SLL_SCAN (pAllocAddrList, pNode, tIPCPAllocAddrNode *)
    {
        if (pNode->u4Addr == u4Addr)
            return FAILURE;
    }

    return SUCCESS;                /* Yes, the addr is free */

}

INT4
IPCPAddAddrToAllocList (t_SLL * pAllocAddrList, UINT4 u4Addr, UINT4 u4Port)
{

    tIPCPAllocAddrNode *pNode;

    if ((pNode = (tIPCPAllocAddrNode *) IPCP_ALLOC_NODE ()) != NULL)
    {

        SLL_INIT_NODE ((t_SLL_NODE *) pNode);
        pNode->u4Addr = u4Addr;
        pNode->u4Port = u4Port;
        pNode->pNextAllocAddrNode = NULL;

        SLL_ADD ((t_SLL *) pAllocAddrList, (t_SLL_NODE *) pNode);
        return (SUCCESS);
    }

    return (FAILURE);
}

/************************************************************************
 # FUNCTION    : IPCPValidateServerAddress                              #
 # DESCRIPTION : If server address falls in the range of any PPP address# 
 #     pool, that is configured,this function returns failure otherwise #
 #     returns success.This is to ensure that,pptp or l2tp server should#
 #     not use any pool address for its interface                       #
 # INPUT       : pool StartAddress,EndAddress                           #
 # RETURN      : PPTP_SUCCESS or L2TP_FAILURE                           #
 ************************************************************************/
INT4
IPCPValidateServerAddress (INT4 i4ServAddress)
{
    tIPCPAddrPool      *pAddrPool = NULL;
    SLL_SCAN (&AddrPoolList, pAddrPool, tIPCPAddrPool *)
    {
        if (((UINT4) i4ServAddress >= pAddrPool->u4LowerAddr)
            && ((UINT4) i4ServAddress <= pAddrPool->u4UpperAddr))
            return FAILURE;
    }
    return SUCCESS;
}
