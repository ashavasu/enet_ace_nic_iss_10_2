/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppipll.c,v 1.7 2014/12/16 11:47:24 siva Exp $
 *
 * Description: Low Level Routines for IPCP module.
 *
 *******************************************************************/
#ifndef __PPP_PPPIPLL_C__
#define __PPP_PPPIPLL_C__
#define __IP_LOW_H__
#include "ipcpcom.h"
#include "snmphdrs.h"
#include "pppsnmpm.h"

#include "ipexts.h"
#include "globexts.h"
#include "llproto.h"
#include "ppipclow.h"
#include "pppexlow.h"
#include "ppipcpcli.h"
/******************************************************************************/
INT1
CheckAndGetIpPtr (tPPPIf * pIf)
{
    if (pIf->pIpcpPtr != NULL)
    {
        return (PPP_SNMP_OK);
    }
    return (PPP_SNMP_ERR);
}

/*********************************************************************
*  Function Name : PppGetIPCPIfPtr
*  Description   :
*  Parameter(s)  :
*   InDataBasePtr -
*   InNamePtr     -
*   Arg           -
*   Value         -
*  Return Value  :
*
*********************************************************************/
tIPCPIf            *
PppGetIPCPIfPtr (Index)
     UINT4               Index;
{
    tPPPIf             *pIf;

    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if ((pIf != NULL) && (pIf->LinkInfo.IfIndex == Index))
            return (pIf->pIpcpPtr);
    }
    return ((tIPCPIf *) NULL);
}

/*******************************************************************/
tIPCPIf            *
PppCreateIPCPIfPtr (Index)
     UINT4               Index;
{
    tPPPIf             *pIf;

    SLL_SCAN (&PPPIfList, pIf, tPPPIf *)
    {
        if ((pIf != NULL) && (pIf->LinkInfo.IfIndex == Index))
        {
            if (pIf->BundleFlag != BUNDLE
                && pIf->MPInfo.MemberInfo.pBundlePtr != NULL)
            {
                return (NULL);
            }
            if (pIf->pIpcpPtr == NULL)
            {
                if ((pIf->pIpcpPtr = IPCPCreateIf (pIf)) == NULL)
                {
                    return (NULL);
                }
            }
            return (pIf->pIpcpPtr);
        }
    }
    return (NULL);
}

#ifdef PPP_STACK_WANTED
/* LOW LEVEL Routines for Table : PppIpTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppIpTable
 Input       :  The Indices
                PppIpIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppIpTable (INT4 i4PppIpIfIndex)
{
    if (PppGetIPCPIfPtr (i4PppIpIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppIpTable
 Input       :  The Indices
                PppIpIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppIpTable (INT4 *pi4PppIpIfIndex)
{
    if (nmhGetFirstIndex (pi4PppIpIfIndex, IpInstance, FIRST_INST) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppIpTable
 Input       :  The Indices
                PppIpIfIndex
                nextPppIpIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppIpTable (INT4 i4PppIpIfIndex, INT4 *pi4NextPppIpIfIndex)
{
    if (nmhGetNextIndex (&i4PppIpIfIndex, IpInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextPppIpIfIndex = i4PppIpIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppIpOperStatus
 Input       :  The Indices
                PppIpIfIndex

                The Object 
                retValPppIpOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppIpOperStatus (INT4 i4PppIpIfIndex, INT4 *pi4RetValPppIpOperStatus)
{
    tIPCPIf            *pIpIf;

    if ((pIpIf = PppGetIPCPIfPtr (i4PppIpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValPppIpOperStatus = pIpIf->OperStatus;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPppIpLocalToRemoteCompressionProtocol
 Input       :  The Indices
                PppIpIfIndex

                The Object 
                retValPppIpLocalToRemoteCompressionProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppIpLocalToRemoteCompressionProtocol (INT4 i4PppIpIfIndex,
                                             INT4
                                             *pi4RetValPppIpLocalToRemoteCompressionProtocol)
{

    tIPCPIf            *pIpIf;

    if ((pIpIf = PppGetIPCPIfPtr (i4PppIpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pIpIf->OperStatus == STATUS_UP)
    {
        if (pIpIf->IpcpGSEM.pNegFlagsPerIf[IP_COMP_IDX].
            FlagMask & ACKED_BY_LOCAL_MASK)
        {
            *pi4RetValPppIpLocalToRemoteCompressionProtocol = 2;
        }
        else
        {
            *pi4RetValPppIpLocalToRemoteCompressionProtocol = 1;
        }
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetPppIpRemoteToLocalCompressionProtocol
 Input       :  The Indices
                PppIpIfIndex

                The Object 
                retValPppIpRemoteToLocalCompressionProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppIpRemoteToLocalCompressionProtocol (INT4 i4PppIpIfIndex,
                                             INT4
                                             *pi4RetValPppIpRemoteToLocalCompressionProtocol)
{

    tIPCPIf            *pIpIf;

    if ((pIpIf = PppGetIPCPIfPtr (i4PppIpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pIpIf->OperStatus == STATUS_UP)
    {
        if (pIpIf->IpcpGSEM.pNegFlagsPerIf[IP_COMP_IDX].
            FlagMask & ACKED_BY_PEER_MASK)
        {
            *pi4RetValPppIpRemoteToLocalCompressionProtocol = 2;
        }
        else
        {
            *pi4RetValPppIpRemoteToLocalCompressionProtocol = 1;
        }
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetPppIpRemoteMaxSlotId
 Input       :  The Indices
                PppIpIfIndex

                The Object 
                retValPppIpRemoteMaxSlotId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppIpRemoteMaxSlotId (INT4 i4PppIpIfIndex,
                            INT4 *pi4RetValPppIpRemoteMaxSlotId)
{

    tIPCPIf            *pIpIf;

    if ((pIpIf = PppGetIPCPIfPtr (i4PppIpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pIpIf->OperStatus == STATUS_UP)
    {
        *pi4RetValPppIpRemoteMaxSlotId =
            pIpIf->IpcpOptionsAckedByLocal.CompInfo.VJInfo.MaxSlotId;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetPppIpLocalMaxSlotId
 Input       :  The Indices
                PppIpIfIndex

                The Object 
                retValPppIpLocalMaxSlotId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppIpLocalMaxSlotId (INT4 i4PppIpIfIndex,
                           INT4 *pi4RetValPppIpLocalMaxSlotId)
{

    tIPCPIf            *pIpIf;

    if ((pIpIf = PppGetIPCPIfPtr (i4PppIpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pIpIf->OperStatus == STATUS_UP)
    {
        *pi4RetValPppIpLocalMaxSlotId =
            pIpIf->IpcpOptionsAckedByPeer.CompInfo.VJInfo.MaxSlotId;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}
#endif /* PPP_STACK_WANTED */

/* LOW LEVEL Routines for Table : PppIpConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppIpConfigTable
 Input       :  The Indices
                PppIpConfigIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppIpConfigTable (INT4 i4PppIpConfigIfIndex)
{
    if (PppGetIPCPIfPtr ((UINT4) i4PppIpConfigIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppIpConfigTable
 Input       :  The Indices
                PppIpConfigIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppIpConfigTable (INT4 *pi4PppIpConfigIfIndex)
{
    if (nmhGetFirstIndex (pi4PppIpConfigIfIndex, IpInstance, FIRST_INST) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppIpConfigTable
 Input       :  The Indices
                PppIpConfigIfIndex
                nextPppIpConfigIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppIpConfigTable (INT4 i4PppIpConfigIfIndex,
                                 INT4 *pi4NextPppIpConfigIfIndex)
{
    if (nmhGetNextIndex (&i4PppIpConfigIfIndex, IpInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextPppIpConfigIfIndex = i4PppIpConfigIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppIpConfigAdminStatus
 Input       :  The Indices
                PppIpConfigIfIndex

                The Object 
                retValPppIpConfigAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppIpConfigAdminStatus (INT4 i4PppIpConfigIfIndex,
                              INT4 *pi4RetValPppIpConfigAdminStatus)
{

    tIPCPIf            *pIpIf;

    if ((pIpIf = PppGetIPCPIfPtr ((UINT4) i4PppIpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValPppIpConfigAdminStatus = pIpIf->AdminStatus;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPppIpConfigCompression
 Input       :  The Indices
                PppIpConfigIfIndex

                The Object 
                retValPppIpConfigCompression
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppIpConfigCompression (INT4 i4PppIpConfigIfIndex,
                              INT4 *pi4RetValPppIpConfigCompression)
{

    tIPCPIf            *pIpIf;

    if ((pIpIf = PppGetIPCPIfPtr ((UINT4) i4PppIpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if ((pIpIf->IpcpGSEM.pNegFlagsPerIf[IP_COMP_IDX].FlagMask & DESIRED_MASK) ==
        0)
    {
        *pi4RetValPppIpConfigCompression = IP_COMPRESSION_NONE;
    }
    else
    {
        *pi4RetValPppIpConfigCompression =
            pIpIf->IpcpOptionsAckedByPeer.u2CompProto;
    }
    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppIpConfigAdminStatus
 Input       :  The Indices
                PppIpConfigIfIndex

                The Object 
                testValPppIpConfigAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppIpConfigAdminStatus (UINT4 *pu4ErrorCode, INT4 i4PppIpConfigIfIndex,
                                 INT4 i4TestValPppIpConfigAdminStatus)
{

    tIPCPIf            *pIpIf;

    if ((pIpIf = PppGetIPCPIfPtr ((UINT4) i4PppIpConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        PPP_UNUSED (pIpIf);
        return (SNMP_FAILURE);
    }

    if ((i4TestValPppIpConfigAdminStatus != ADMIN_OPEN)
        && (i4TestValPppIpConfigAdminStatus != ADMIN_CLOSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppIpConfigCompression
 Input       :  The Indices
                PppIpConfigIfIndex

                The Object 
                testValPppIpConfigCompression
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppIpConfigCompression (UINT4 *pu4ErrorCode, INT4 i4PppIpConfigIfIndex,
                                 INT4 i4TestValPppIpConfigCompression)
{

    tIPCPIf            *pIpIf;

    if ((pIpIf = PppGetIPCPIfPtr ((UINT4) i4PppIpConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        PPP_UNUSED (pIpIf);
        return (SNMP_FAILURE);
    }

    if ((i4TestValPppIpConfigCompression != IP_COMPRESSION_NONE)
        && (i4TestValPppIpConfigCompression != IP_VJ_TCP_COMPRESSION)
        && (i4TestValPppIpConfigCompression != IP_HEADER_COMPRESSION))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2PppIpConfigTable
 Input       :  The Indices
                IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PppIpConfigTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    PPP_UNUSED (pu4ErrorCode);
    PPP_UNUSED (pSnmpIndexList);
    PPP_UNUSED (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppIpConfigAdminStatus
 Input       :  The Indices
                PppIpConfigIfIndex

                The Object 
                setValPppIpConfigAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppIpConfigAdminStatus (INT4 i4PppIpConfigIfIndex,
                              INT4 i4SetValPppIpConfigAdminStatus)
{

    tIPCPIf            *pIpIf;

    if ((pIpIf = PppGetIPCPIfPtr ((UINT4) i4PppIpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    switch (i4SetValPppIpConfigAdminStatus)
    {
        case ADMIN_OPEN:
            pIpIf->AdminStatus = (UINT1) i4SetValPppIpConfigAdminStatus;
            IPCPEnableIf (pIpIf, &IPCPCtrlBlk);
            break;
        case ADMIN_CLOSE:
            pIpIf->AdminStatus = (UINT1) i4SetValPppIpConfigAdminStatus;
            IPCPDisableIf (pIpIf);
            break;
        default:
            break;
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetPppIpConfigCompression
 Input       :  The Indices
                PppIpConfigIfIndex

                The Object 
                setValPppIpConfigCompression
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppIpConfigCompression (INT4 i4PppIpConfigIfIndex,
                              INT4 i4SetValPppIpConfigCompression)
{

    tIPCPIf            *pIpIf;

    if ((pIpIf = PppGetIPCPIfPtr ((UINT4) i4PppIpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    switch (i4SetValPppIpConfigCompression)
    {
        case IP_COMPRESSION_NONE:
            pIpIf->IpcpGSEM.pNegFlagsPerIf[IP_COMP_IDX].FlagMask &=
                DESIRED_NOT_SET;
            if (pIpIf->OperStatus == STATUS_DOWN)
            {
                pIpIf->IpcpGSEM.pNegFlagsPerIf[IP_COMP_IDX].FlagMask &=
                    ACKED_BY_PEER_NOT_SET;
            }
            break;
        case IP_VJ_TCP_COMPRESSION:
            if (VJCompAvailable == PPP_YES)
            {

                pIpIf->IpcpOptionsDesired.u2CompProto = VJ_COMP_PROTOCOL;
                pIpIf->IpcpGSEM.pNegFlagsPerIf[IP_COMP_IDX].FlagMask |=
                    DESIRED_SET | ALLOWED_FOR_PEER_SET;
            }
            else
            {
                return (SNMP_FAILURE);
            }
            if (pIpIf->OperStatus == STATUS_DOWN)
            {
                pIpIf->IpcpGSEM.pNegFlagsPerIf[IP_COMP_IDX].FlagMask |=
                    ACKED_BY_PEER_SET;
            }
            break;
        case IP_HEADER_COMPRESSION:
            pIpIf->IpcpOptionsDesired.u2CompProto = IPHC_PROTOCOL;
            pIpIf->IpcpGSEM.pNegFlagsPerIf[IP_COMP_IDX].FlagMask |= DESIRED_SET;
            if (pIpIf->OperStatus == STATUS_DOWN)
            {
                pIpIf->IpcpGSEM.pNegFlagsPerIf[IP_COMP_IDX].FlagMask |=
                    ACKED_BY_PEER_SET;
            }
            break;
        default:
            break;
    }
    return (SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : PppExtIPAddressConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppExtIPAddressConfigTable
 Input       :  The Indices
                PppExtAddressPoolIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppExtIPAddressConfigTable (INT4
                                                    i4PppExtAddressPoolIndex)
{
    if (IPCPGetAddrPool ((UINT4) i4PppExtAddressPoolIndex) == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppExtIPAddressConfigTable
 Input       :  The Indices
                PppExtAddressPoolIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppExtIPAddressConfigTable (INT4 *pi4PppExtAddressPoolIndex)
{

    UINT4               FirstIndex = MAX_INTEGER;
    tIPCPAddrPool      *pAddrPool;

    SLL_SCAN (&AddrPoolList, pAddrPool, tIPCPAddrPool *)
    {
        if (pAddrPool->u4PoolIndex < FirstIndex)
            FirstIndex = pAddrPool->u4PoolIndex;
    }

    if (FirstIndex != MAX_INTEGER)
    {
        *pi4PppExtAddressPoolIndex = (INT4) FirstIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexPppExtIPAddressConfigTable
 Input       :  The Indices
                PppExtAddressPoolIndex
                nextPppExtAddressPoolIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppExtIPAddressConfigTable (INT4 i4PppExtAddressPoolIndex,
                                           INT4 *pi4NextPppExtAddressPoolIndex)
{

    UINT4               NextIndex = MAX_INTEGER;
    tIPCPAddrPool      *pAddrPool;

    SLL_SCAN (&AddrPoolList, pAddrPool, tIPCPAddrPool *)
    {
        if ((pAddrPool->u4PoolIndex > (UINT4) i4PppExtAddressPoolIndex)
            && (pAddrPool->u4PoolIndex < NextIndex))
        {
            NextIndex = pAddrPool->u4PoolIndex;
        }

    }

    if (NextIndex != MAX_INTEGER)
    {
        *pi4NextPppExtAddressPoolIndex = (INT4) NextIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppExtIPAddressPoolLowerRange
 Input       :  The Indices
                PppExtAddressPoolIndex

                The Object 
                retValPppExtIPAddressPoolLowerRange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIPAddressPoolLowerRange (INT4 i4PppExtAddressPoolIndex,
                                     UINT4
                                     *pu4RetValPppExtIPAddressPoolLowerRange)
{
    tIPCPAddrPool      *pAddrPool;

    if ((pAddrPool =
         IPCPGetAddrPool ((UINT4) i4PppExtAddressPoolIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValPppExtIPAddressPoolLowerRange = pAddrPool->u4LowerAddr;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetPppExtIPAddressPoolUpperRange
 Input       :  The Indices
                PppExtAddressPoolIndex

                The Object 
                retValPppExtIPAddressPoolUpperRange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIPAddressPoolUpperRange (INT4 i4PppExtAddressPoolIndex,
                                     UINT4
                                     *pu4RetValPppExtIPAddressPoolUpperRange)
{
    tIPCPAddrPool      *pAddrPool;

    if ((pAddrPool =
         IPCPGetAddrPool ((UINT4) i4PppExtAddressPoolIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValPppExtIPAddressPoolUpperRange = pAddrPool->u4UpperAddr;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetPppExtIPAddressPoolStatus
 Input       :  The Indices
                PppExtAddressPoolIndex

                The Object 
                retValPppExtIPAddressPoolStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIPAddressPoolStatus (INT4 i4PppExtAddressPoolIndex,
                                 INT4 *pi4RetValPppExtIPAddressPoolStatus)
{
    tIPCPAddrPool      *pAddrPool;

    if ((pAddrPool =
         IPCPGetAddrPool ((UINT4) i4PppExtAddressPoolIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValPppExtIPAddressPoolStatus = pAddrPool->u1PoolStatus;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppExtIPAddressPoolLowerRange
 Input       :  The Indices
                PppExtAddressPoolIndex

                The Object 
                setValPppExtIPAddressPoolLowerRange
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIPAddressPoolLowerRange (INT4 i4PppExtAddressPoolIndex,
                                     UINT4
                                     u4SetValPppExtIPAddressPoolLowerRange)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tIPCPAddrPool      *pAddrPool;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    pAddrPool = IPCPGetOrCreateAddrPool ((UINT4) i4PppExtAddressPoolIndex);
    if (pAddrPool == NULL)
        return SNMP_FAILURE;
    if ((pAddrPool->u4LowerAddr != 0) &&
        (pAddrPool->u4LowerAddr != u4SetValPppExtIPAddressPoolLowerRange) &&
        (SLL_COUNT (&(pAddrPool->AllocAddrList)) > 0))
    {
        return (SNMP_FAILURE);
    }
    pAddrPool->u4LowerAddr = u4SetValPppExtIPAddressPoolLowerRange;
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtIPAddressPoolLowerRange, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
		  i4PppExtAddressPoolIndex,
                  u4SetValPppExtIPAddressPoolLowerRange));
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetPppExtIPAddressPoolUpperRange
 Input       :  The Indices
                PppExtAddressPoolIndex

                The Object 
                setValPppExtIPAddressPoolUpperRange
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIPAddressPoolUpperRange (INT4 i4PppExtAddressPoolIndex,
                                     UINT4
                                     u4SetValPppExtIPAddressPoolUpperRange)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tIPCPAddrPool      *pAddrPool;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    pAddrPool = IPCPGetOrCreateAddrPool ((UINT4) i4PppExtAddressPoolIndex);
    if (pAddrPool == NULL)
        return SNMP_FAILURE;

    if ((pAddrPool->u4UpperAddr != 0) &&
        (pAddrPool->u4UpperAddr != u4SetValPppExtIPAddressPoolUpperRange) &&
        (SLL_COUNT (&(pAddrPool->AllocAddrList)) > 0))
    {
        return (SNMP_FAILURE);
    }
    pAddrPool->u4UpperAddr = u4SetValPppExtIPAddressPoolUpperRange;
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtIPAddressPoolUpperRange, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
		  i4PppExtAddressPoolIndex,
                  u4SetValPppExtIPAddressPoolUpperRange));
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetPppExtIPAddressPoolStatus
 Input       :  The Indices
                PppExtAddressPoolIndex

                The Object 
                setValPppExtIPAddressPoolStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIPAddressPoolStatus (INT4 i4PppExtAddressPoolIndex,
                                 INT4 i4SetValPppExtIPAddressPoolStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tIPCPAddrPool      *pAddrPool;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    pAddrPool = IPCPGetAddrPool ((UINT4) i4PppExtAddressPoolIndex);
    if (pAddrPool == NULL)
        return SNMP_FAILURE;
    if (i4SetValPppExtIPAddressPoolStatus == IPCP_DELETE)
    {
/* Deletion is allowed only if there are no allocated addresses in this pool*/
        if (SLL_COUNT (&(pAddrPool->AllocAddrList)) > 0)
        {
            return (SNMP_FAILURE);
        }
    }
    pAddrPool->u1PoolStatus = (UINT1) i4SetValPppExtIPAddressPoolStatus;

    if (i4SetValPppExtIPAddressPoolStatus == IPCP_DELETE)
    {
/* Deletion is allowed only if there are no allocated addresses in this
 * pool - Hence no need to free the IP address allocated nodes */
        SLL_DELETE (&AddrPoolList, (t_SLL_NODE *) pAddrPool);
        IPCP_FREE_POOL (pAddrPool);
    }
    else if (i4SetValPppExtIPAddressPoolStatus == IPCP_UP)
        pAddrPool->u4NoOfFreeAddr = pAddrPool->u4UpperAddr -
            pAddrPool->u4LowerAddr + 1;
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtIPAddressPoolStatus, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
		  i4PppExtAddressPoolIndex,
                  i4SetValPppExtIPAddressPoolStatus));

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppExtIPAddressPoolLowerRange
 Input       :  The Indices
                PppExtAddressPoolIndex

                The Object 
                testValPppExtIPAddressPoolLowerRange
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIPAddressPoolLowerRange (UINT4 *pu4ErrorCode,
                                        INT4 i4PppExtAddressPoolIndex,
                                        UINT4
                                        u4TestValPppExtIPAddressPoolLowerRange)
{

    PPP_UNUSED (pu4ErrorCode);
    PPP_UNUSED (i4PppExtAddressPoolIndex);
    PPP_UNUSED (u4TestValPppExtIPAddressPoolLowerRange);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2PppExtIPAddressPoolUpperRange
 Input       :  The Indices
                PppExtAddressPoolIndex

                The Object 
                testValPppExtIPAddressPoolUpperRange
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIPAddressPoolUpperRange (UINT4 *pu4ErrorCode,
                                        INT4 i4PppExtAddressPoolIndex,
                                        UINT4
                                        u4TestValPppExtIPAddressPoolUpperRange)
{
    PPP_UNUSED (pu4ErrorCode);
    PPP_UNUSED (i4PppExtAddressPoolIndex);
    PPP_UNUSED (u4TestValPppExtIPAddressPoolUpperRange);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2PppExtIPAddressPoolStatus
 Input       :  The Indices
                PppExtAddressPoolIndex

                The Object 
                testValPppExtIPAddressPoolStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIPAddressPoolStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4PppExtAddressPoolIndex,
                                    INT4 i4TestValPppExtIPAddressPoolStatus)
{
    tIPCPAddrPool      *pAddrPool;

    if ((pAddrPool =
         IPCPGetAddrPool ((UINT4) i4PppExtAddressPoolIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValPppExtIPAddressPoolStatus != IPCP_UP &&
        i4TestValPppExtIPAddressPoolStatus != IPCP_DOWN &&
        i4TestValPppExtIPAddressPoolStatus != IPCP_DELETE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* If there are any allocated IP addresses outstanding, then 
     * this pool cannot be DELETED / MADE DOWN
     */
    if (i4TestValPppExtIPAddressPoolStatus == IPCP_DOWN ||
        i4TestValPppExtIPAddressPoolStatus == IPCP_DELETE)
    {
        if (SLL_COUNT (&pAddrPool->AllocAddrList) != 0)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2PppExtIPAddressConfigTable
 Input       :  The Indices
                PppExtAddressPoolIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PppExtIPAddressConfigTable (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    PPP_UNUSED (pu4ErrorCode);
    PPP_UNUSED (pSnmpIndexList);
    PPP_UNUSED (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : PppExtIPAddressStatusTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppExtIPAddressStatusTable
 Input       :  The Indices
                PppExtAddressPoolStatusIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppExtIPAddressStatusTable (INT4
                                                    i4PppExtAddressPoolStatusIndex)
{
    if (IPCPGetAddrPool ((UINT4) i4PppExtAddressPoolStatusIndex) == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppExtIPAddressStatusTable
 Input       :  The Indices
                PppExtAddressPoolStatusIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppExtIPAddressStatusTable (INT4
                                            *pi4PppExtAddressPoolStatusIndex)
{
    PPP_UNUSED (pi4PppExtAddressPoolStatusIndex);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppExtIPAddressStatusTable
 Input       :  The Indices
                PppExtAddressPoolStatusIndex
                nextPppExtAddressPoolStatusIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppExtIPAddressStatusTable (INT4 i4PppExtAddressPoolStatusIndex,
                                           INT4
                                           *pi4NextPppExtAddressPoolStatusIndex)
{
    PPP_UNUSED (i4PppExtAddressPoolStatusIndex);
    PPP_UNUSED (pi4NextPppExtAddressPoolStatusIndex);
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppExtNoOfFreeIPAddresses
 Input       :  The Indices
                PppExtAddressPoolStatusIndex

                The Object 
                retValPppExtNoOfFreeIPAddresses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtNoOfFreeIPAddresses (INT4 i4PppExtAddressPoolStatusIndex,
                                 INT4 *pi4RetValPppExtNoOfFreeIPAddresses)
{
    tIPCPAddrPool      *pAddrPool;

    if ((pAddrPool =
         IPCPGetAddrPool ((UINT4) i4PppExtAddressPoolStatusIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValPppExtNoOfFreeIPAddresses = (INT4) pAddrPool->u4NoOfFreeAddr;
    return SNMP_SUCCESS;

}

/********* snmp get addresspool if **********/
tIPCPAddrPool      *
IPCPGetAddrPool (UINT4 Index)
{
    tIPCPAddrPool      *pAddrPool;

    SLL_SCAN (&AddrPoolList, pAddrPool, tIPCPAddrPool *)
    {
        if (pAddrPool->u4PoolIndex == Index)
            return (pAddrPool);
    }
    return NULL;
}

/******************* snmp get or create    ******************************/
tIPCPAddrPool      *
IPCPGetOrCreateAddrPool (UINT4 Index)
{
    tIPCPAddrPool      *pAddrPool;

    pAddrPool = NULL;
    SLL_SCAN (&AddrPoolList, pAddrPool, tIPCPAddrPool *)
    {
        if (pAddrPool->u4PoolIndex == Index)
            return (pAddrPool);

    }
    /*Maximum number of pools ,that canbe configured are five */
    if((pAddrPool == NULL) && (SLL_COUNT (&AddrPoolList) < PPP_MAX_IP_POOL))
    {
        return (IPCPCreateNewAddrPool (Index));
    }
    return NULL;
}

/***************    create new addresspool    **********/

tIPCPAddrPool      *
IPCPCreateNewAddrPool (UINT4 Index)
{
    tIPCPAddrPool      *pAddrPool;

    PPP_UNUSED (Index);
    if ((pAddrPool = (tIPCPAddrPool *) IPCP_ALLOC_POOL ()) != NULL)
    {
        pAddrPool->u4LowerAddr = 0;
        pAddrPool->u4UpperAddr = 0;
        pAddrPool->u4PoolIndex = Index;
        pAddrPool->u4NoOfFreeAddr = 0;
        pAddrPool->u1PoolStatus = IPCP_DELETE;

        SLL_ADD ((t_SLL *) & AddrPoolList, (t_SLL_NODE *) pAddrPool);
        SLL_INIT ((t_SLL *) & pAddrPool->AllocAddrList);
        return pAddrPool;
    }
    else
        return NULL;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppExtIPAddressPoolSelector
 Input       :  The Indices

                The Object 
                retValPppExtIPAddressPoolSelector
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIPAddressPoolSelector (INT4 *pi4RetValPppExtIPAddressPoolSelector)
{
    *pi4RetValPppExtIPAddressPoolSelector = gu1IPCPAddrModeSelector;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppExtIPAddressPoolSelector
 Input       :  The Indices

                The Object 
                setValPppExtIPAddressPoolSelector
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIPAddressPoolSelector (INT4 i4SetValPppExtIPAddressPoolSelector)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    gu1IPCPAddrModeSelector = (UINT1) i4SetValPppExtIPAddressPoolSelector;

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtIPAddressPoolSelector, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 0, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                  i4SetValPppExtIPAddressPoolSelector));

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppExtIPAddressPoolSelector
 Input       :  The Indices

                The Object 
                testValPppExtIPAddressPoolSelector
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIPAddressPoolSelector (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValPppExtIPAddressPoolSelector)
{
    if ((IPCP_LOCAL_POOL != i4TestValPppExtIPAddressPoolSelector)
        && (IPCP_ADDRESS_MODE_RADIUS != i4TestValPppExtIPAddressPoolSelector)
        && (IPCP_ADDRESS_MODE_NONE != i4TestValPppExtIPAddressPoolSelector))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (i4TestValPppExtIPAddressPoolSelector == IPCP_ADDRESS_MODE_RADIUS &&
        gu1aaaMethod != AAA_RADIUS)
    {
        PPP_TRC (ALL_FAILURE, "Configure RADIUS As AAA method !\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}
#endif /*__PPP_PPPIPLL_C__*/
