/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppipel.c,v 1.6 2014/12/16 10:49:11 siva Exp $
 *
 * Description: Low Level Routines for IPCP module.
 *
 *******************************************************************/
/* This File contains  Low level Routines for pppExtIp Group *****/

#define __IP_EL_LOW_H__
#include "ipcpcom.h"
#include "snmphdrs.h"
#include "pppsnmpm.h"
#include "llproto.h"
#include "ppipclow.h"
#include "globexts.h"
#include "ppipcpcli.h"
/* Added by Sandeep 28/8/97 */
#define COMP_SLOT_ID_ENABLED    1
#define COMP_SLOT_ID_DISABLED    2

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppExtIpConfigLocalMaxSlotId
 Input       :  The Indices
                PppExtIpConfigIfIndex

                The Object 
                retValPppExtIpConfigLocalMaxSlotId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIpConfigLocalMaxSlotId (INT4 i4PppExtIpConfigIfIndex,
                                    INT4 *pi4RetValPppExtIpConfigLocalMaxSlotId)
{
    tIPCPIf            *pIpPtr;

    if ((pIpPtr = PppGetIPCPIfPtr ((UINT4) i4PppExtIpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppExtIpConfigLocalMaxSlotId =
        pIpPtr->IpcpOptionsDesired.CompInfo.VJInfo.MaxSlotId;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppExtIpConfigLocalCompSlotId
 Input       :  The Indices
                PppExtIpConfigIfIndex

                The Object 
                retValPppExtIpConfigLocalCompSlotId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIpConfigLocalCompSlotId (INT4 i4PppExtIpConfigIfIndex,
                                     INT4
                                     *pi4RetValPppExtIpConfigLocalCompSlotId)
{
    tIPCPIf            *pIpPtr;

    if ((pIpPtr = PppGetIPCPIfPtr ((UINT4) i4PppExtIpConfigIfIndex)) == NULL)    /* dvk fix */
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValPppExtIpConfigLocalCompSlotId =
        (pIpPtr->IpcpOptionsDesired.CompInfo.VJInfo.CompSlotId ==
         COMP_SLOT_ID_COMPRESSED) ? COMP_SLOT_ID_ENABLED :
        COMP_SLOT_ID_DISABLED;
    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppExtIpConfigLocalMaxSlotId
 Input       :  The Indices
                PppExtIpConfigIfIndex

                The Object 
                setValPppExtIpConfigLocalMaxSlotId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIpConfigLocalMaxSlotId (INT4 i4PppExtIpConfigIfIndex,
                                    INT4 i4SetValPppExtIpConfigLocalMaxSlotId)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tIPCPIf            *pIpPtr;
    UINT4		u4SeqNum = 0;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if ((pIpPtr = PppGetIPCPIfPtr ((UINT4) i4PppExtIpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIpPtr->IpcpOptionsDesired.CompInfo.VJInfo.MaxSlotId =
        (UINT1) i4SetValPppExtIpConfigLocalMaxSlotId;

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtIpConfigLocalMaxSlotId, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
		      i4PppExtIpConfigIfIndex,
                      i4SetValPppExtIpConfigLocalMaxSlotId));
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppExtIpConfigLocalCompSlotId
 Input       :  The Indices
                PppExtIpConfigIfIndex

                The Object 
                setValPppExtIpConfigLocalCompSlotId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIpConfigLocalCompSlotId (INT4 i4PppExtIpConfigIfIndex,
                                     INT4 i4SetValPppExtIpConfigLocalCompSlotId)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tIPCPIf            *pIpPtr;
    UINT4		u4SeqNum = 0;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if ((pIpPtr = PppGetIPCPIfPtr ((UINT4) i4PppExtIpConfigIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIpPtr->IpcpOptionsDesired.CompInfo.VJInfo.CompSlotId =
        (i4SetValPppExtIpConfigLocalCompSlotId ==
         COMP_SLOT_ID_ENABLED) ? COMP_SLOT_ID_COMPRESSED :
        COMP_SLOT_ID_NOT_COMPRESSED;

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtIpConfigLocalCompSlotId, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
		      i4PppExtIpConfigIfIndex,
                      i4SetValPppExtIpConfigLocalCompSlotId));
    return (SNMP_SUCCESS);

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppExtIpConfigLocalMaxSlotId
 Input       :  The Indices
                PppExtIpConfigIfIndex

                The Object 
                testValPppExtIpConfigLocalMaxSlotId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIpConfigLocalMaxSlotId (UINT4 *pu4ErrorCode,
                                       INT4 i4PppExtIpConfigIfIndex,
                                       INT4
                                       i4TestValPppExtIpConfigLocalMaxSlotId)
{
    tIPCPIf            *pIpPtr;

    if ((pIpPtr = PppGetIPCPIfPtr ((UINT4) i4PppExtIpConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        PPP_UNUSED (pIpPtr);
        return (SNMP_FAILURE);
    }

    if (i4TestValPppExtIpConfigLocalMaxSlotId < MIN_MAX_SLOT_ID
        || i4TestValPppExtIpConfigLocalMaxSlotId > MAX_MAX_SLOT_ID)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppExtIpConfigLocalCompSlotId
 Input       :  The Indices
                PppExtIpConfigIfIndex

                The Object 
                testValPppExtIpConfigLocalCompSlotId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIpConfigLocalCompSlotId (UINT4 *pu4ErrorCode,
                                        INT4 i4PppExtIpConfigIfIndex,
                                        INT4
                                        i4TestValPppExtIpConfigLocalCompSlotId)
{
    tIPCPIf            *pIpPtr;

    if ((pIpPtr = PppGetIPCPIfPtr ((UINT4) i4PppExtIpConfigIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        PPP_UNUSED (pIpPtr);
        return (SNMP_FAILURE);
    }

    if ((i4TestValPppExtIpConfigLocalCompSlotId != COMP_SLOT_ID_DISABLED)
        && (i4TestValPppExtIpConfigLocalCompSlotId != COMP_SLOT_ID_ENABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/* LOW LEVEL Routines for Table : PppExtIpTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppExtIpTable
 Input       :  The Indices
                PppExtIpIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppExtIpTable (INT4 i4PppExtIpIfIndex)
{
    if (PppGetIPCPIfPtr ((UINT4) i4PppExtIpIfIndex) == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppExtIpTable
 Input       :  The Indices
                PppExtIpIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppExtIpTable (INT4 *pi4PppExtIpIfIndex)
{
    if (nmhGetFirstIndex (pi4PppExtIpIfIndex, IpInstance, FIRST_INST) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppExtIpTable
 Input       :  The Indices
                PppExtIpIfIndex
                nextPppExtIpIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppExtIpTable (INT4 i4PppExtIpIfIndex,
                              INT4 *pi4NextPppExtIpIfIndex)
{
    if (nmhGetNextIndex (&i4PppExtIpIfIndex, IpInstance, NEXT_INST) ==
        SNMP_SUCCESS)
    {
        *pi4NextPppExtIpIfIndex = i4PppExtIpIfIndex;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppExtIpRemoteCompSlotId
 Input       :  The Indices
                PppExtIpIfIndex

                The Object 
                retValPppExtIpRemoteCompSlotId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIpRemoteCompSlotId (INT4 i4PppExtIpIfIndex,
                                INT4 *pi4RetValPppExtIpRemoteCompSlotId)
{
    tIPCPIf            *pIpPtr;

    if ((pIpPtr = PppGetIPCPIfPtr ((UINT4) i4PppExtIpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pIpPtr->OperStatus == STATUS_DOWN)
    {
        *pi4RetValPppExtIpRemoteCompSlotId = COMP_SLOT_ID_DISABLED;
        return (SNMP_SUCCESS);
    }

    *pi4RetValPppExtIpRemoteCompSlotId =
        (pIpPtr->IpcpOptionsAckedByLocal.CompInfo.VJInfo.CompSlotId ==
         COMP_SLOT_ID_COMPRESSED) ? COMP_SLOT_ID_ENABLED :
        COMP_SLOT_ID_DISABLED;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppExtIpLocalCompSlotId
 Input       :  The Indices
                PppExtIpIfIndex

                The Object 
                retValPppExtIpLocalCompSlotId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIpLocalCompSlotId (INT4 i4PppExtIpIfIndex,
                               INT4 *pi4RetValPppExtIpLocalCompSlotId)
{
    tIPCPIf            *pIpPtr;

    if ((pIpPtr = PppGetIPCPIfPtr ((UINT4) i4PppExtIpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pIpPtr->OperStatus == STATUS_DOWN)
    {
        *pi4RetValPppExtIpLocalCompSlotId = COMP_SLOT_ID_DISABLED;
        return (SNMP_SUCCESS);
    }

    *pi4RetValPppExtIpLocalCompSlotId =
        (pIpPtr->IpcpOptionsAckedByPeer.CompInfo.VJInfo.CompSlotId ==
         COMP_SLOT_ID_COMPRESSED) ? COMP_SLOT_ID_ENABLED :
        COMP_SLOT_ID_DISABLED;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppExtIpRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppExtIpRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIpRowStatus (INT4 i4IfIndex, INT4 *pi4RetValPppExtIpRowStatus)
{
    if (PppGetIPCPIfPtr ((UINT4) i4IfIndex) != NULL)
    {
        *pi4RetValPppExtIpRowStatus = ACTIVE;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetPppExtIpRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                setValPppExtIpRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIpRowStatus (INT4 i4IfIndex, INT4 i4SetValPppExtIpRowStatus)
{
    tIPCPIf            *pIpcpIf = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    switch (i4SetValPppExtIpRowStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            if ((pIpcpIf = PppCreateIPCPIfPtr ((UINT4) i4IfIndex)) == NULL)
            {
                PPP_TRC1 (MGMT, "Creation of IPCP Entry Failed. Index : [%d]",
                          i4IfIndex);
                PPP_UNUSED (pIpcpIf);
                return SNMP_FAILURE;
            }
            break;

        case DESTROY:
            if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4IfIndex)) == NULL)
            {
                return SNMP_FAILURE;
            }
            PPP_TRC1 (MGMT, "Deleting IPCP Entry. Index : [%d]", i4IfIndex);
            IPCPDisableIf (pIpcpIf);
            IPCPDeleteIf (pIpcpIf);
            break;
        default:
            break;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtIpRowStatus, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
		      i4IfIndex,
                      i4SetValPppExtIpRowStatus));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2PppExtIpRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                testValPppExtIpRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIpRowStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                            INT4 i4TestValPppExtIpRowStatus)
{
    tIPCPIf            *pIpcpIf = NULL;

    switch (i4TestValPppExtIpRowStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4IfIndex)) != NULL)
            {
                PPP_TRC (MGMT, "Interface Already Created");
                PPP_UNUSED (pIpcpIf);
                return SNMP_FAILURE;
            }
            break;
        case DESTROY:
            if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4IfIndex)) == NULL)
            {
                PPP_TRC (MGMT, "Interface Not Created");
                PPP_UNUSED (pIpcpIf);
                return SNMP_FAILURE;
            }
            break;
        case ACTIVE:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
            break;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2PppExtIpTable
 Input       :  The Indices
                IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PppExtIpTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetPppExtIpAllowIPHCForPeer
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppExtIpAllowIPHCForPeer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIpAllowIPHCForPeer (INT4 i4IfIndex,
                                INT4 *pi4RetValPppExtIpAllowIPHCForPeer)
{

    tIPCPIf            *pIpcpIf;
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pIpcpIf->IpcpGSEM.pNegFlagsPerIf[IP_COMP_IDX].FlagMask &
        ALLOWED_FOR_PEER_MASK)
    {
        *pi4RetValPppExtIpAllowIPHCForPeer = PPP_FALSE;
    }
    else
    {
        *pi4RetValPppExtIpAllowIPHCForPeer = PPP_TRUE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppExtIpAllowIPHCForPeer
 Input       :  The Indices
                IfIndex

                The Object 
                setValPppExtIpAllowIPHCForPeer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIpAllowIPHCForPeer (INT4 i4IfIndex,
                                INT4 i4SetValPppExtIpAllowIPHCForPeer)
{
    tIPCPIf            *pIpcpIf = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4SetValPppExtIpAllowIPHCForPeer == PPP_ENABLE)
    {
        pIpcpIf->IpcpGSEM.pNegFlagsPerIf[IP_COMP_IDX].FlagMask |=
            ALLOWED_FOR_PEER_SET;
    }
    else
    {
        pIpcpIf->IpcpGSEM.pNegFlagsPerIf[IP_COMP_IDX].FlagMask &=
            ALLOWED_FOR_PEER_NOT_SET;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtIpAllowIPHCForPeer, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
		      i4IfIndex,
                      i4SetValPppExtIpAllowIPHCForPeer));
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2PppExtIpAllowIPHCForPeer
 Input       :  The Indices
                IfIndex

                The Object 
                testValPppExtIpAllowIPHCForPeer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIpAllowIPHCForPeer (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                   INT4 i4TestValPppExtIpAllowIPHCForPeer)
{
    tIPCPIf            *pIpcpIf;

    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        PPP_UNUSED (pIpcpIf);
        return SNMP_FAILURE;
    }

    if ((i4TestValPppExtIpAllowIPHCForPeer != PPP_TRUE)
        && (i4TestValPppExtIpAllowIPHCForPeer != PPP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : PppExtIpCompConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppExtIpCompConfigTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppExtIpCompConfigTable (INT4 i4IfIndex)
{
    if (PppGetIPCPIfPtr ((UINT4) i4IfIndex) == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppExtIpCompConfigTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppExtIpCompConfigTable (INT4 *pi4IfIndex)
{
    if (nmhGetFirstIndex (pi4IfIndex, IpInstance, FIRST_INST) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppExtIpCompConfigTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppExtIpCompConfigTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    if (nmhGetNextIndex (&i4IfIndex, IpInstance, NEXT_INST) == SNMP_SUCCESS)
    {
        *pi4NextIfIndex = i4IfIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppExtIpConfigTcpSpace
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppExtIpConfigTcpSpace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIpConfigTcpSpace (INT4 i4IfIndex,
                              INT4 *pi4RetValPppExtIpConfigTcpSpace)
{
    tIPCPIf            *pIpcpIf;
    tIPHCInfo          *pIPHCInfo;
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIPHCInfo = &pIpcpIf->IpcpOptionsDesired.CompInfo.IPHCInfo;

    *pi4RetValPppExtIpConfigTcpSpace = pIPHCInfo->u2TcpSpace;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetPppExtIpConfigNonTcpSpace
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppExtIpConfigNonTcpSpace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIpConfigNonTcpSpace (INT4 i4IfIndex,
                                 INT4 *pi4RetValPppExtIpConfigNonTcpSpace)
{
    tIPCPIf            *pIpcpIf;
    tIPHCInfo          *pIPHCInfo;
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIPHCInfo = &pIpcpIf->IpcpOptionsDesired.CompInfo.IPHCInfo;

    *pi4RetValPppExtIpConfigNonTcpSpace = pIPHCInfo->u2NonTcpSpace;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetPppExtIpConfigFMaxPeriod
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppExtIpConfigFMaxPeriod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIpConfigFMaxPeriod (INT4 i4IfIndex,
                                INT4 *pi4RetValPppExtIpConfigFMaxPeriod)
{

    tIPCPIf            *pIpcpIf;
    tIPHCInfo          *pIPHCInfo;
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIPHCInfo = &pIpcpIf->IpcpOptionsDesired.CompInfo.IPHCInfo;

    *pi4RetValPppExtIpConfigFMaxPeriod = pIPHCInfo->u2FMaxPeriod;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetPppExtIpConfigFMaxTime
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppExtIpConfigFMaxTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIpConfigFMaxTime (INT4 i4IfIndex,
                              INT4 *pi4RetValPppExtIpConfigFMaxTime)
{
    tIPCPIf            *pIpcpIf;
    tIPHCInfo          *pIPHCInfo;
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIPHCInfo = &pIpcpIf->IpcpOptionsDesired.CompInfo.IPHCInfo;

    *pi4RetValPppExtIpConfigFMaxTime = pIPHCInfo->u2FMaxTime;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetPppExtIpConfigMaxHeader
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppExtIpConfigMaxHeader
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIpConfigMaxHeader (INT4 i4IfIndex,
                               INT4 *pi4RetValPppExtIpConfigMaxHeader)
{
    tIPCPIf            *pIpcpIf;
    tIPHCInfo          *pIPHCInfo;
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIPHCInfo = &pIpcpIf->IpcpOptionsDesired.CompInfo.IPHCInfo;

    *pi4RetValPppExtIpConfigMaxHeader = pIPHCInfo->u2MaxHdr;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetPppExtIpConfigRtpCompression
 Input       :  The Indices
                IfIndex

                The Object 
                retValPppExtIpConfigRtpCompression
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIpConfigRtpCompression (INT4 i4IfIndex,
                                    INT4 *pi4RetValPppExtIpConfigRtpCompression)
{
    tIPCPIf            *pIpcpIf;
    tIPHCInfo          *pIPHCInfo;
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIPHCInfo = &pIpcpIf->IpcpOptionsDesired.CompInfo.IPHCInfo;

    *pi4RetValPppExtIpConfigRtpCompression = pIPHCInfo->u2RtpSubOptFlag;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppExtIpConfigTcpSpace
 Input       :  The Indices
                IfIndex

                The Object 
                setValPppExtIpConfigTcpSpace
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIpConfigTcpSpace (INT4 i4IfIndex,
                              INT4 i4SetValPppExtIpConfigTcpSpace)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tIPCPIf            *pIpcpIf;
    tIPHCInfo          *pIPHCInfo;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIPHCInfo = &pIpcpIf->IpcpOptionsDesired.CompInfo.IPHCInfo;

    pIPHCInfo->u2TcpSpace = (UINT2) i4SetValPppExtIpConfigTcpSpace;
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtIpConfigTcpSpace, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
		      i4IfIndex,
                      i4SetValPppExtIpConfigTcpSpace));
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetPppExtIpConfigNonTcpSpace
 Input       :  The Indices
                IfIndex

                The Object 
                setValPppExtIpConfigNonTcpSpace
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIpConfigNonTcpSpace (INT4 i4IfIndex,
                                 INT4 i4SetValPppExtIpConfigNonTcpSpace)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tIPCPIf            *pIpcpIf;
    tIPHCInfo          *pIPHCInfo;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIPHCInfo = &pIpcpIf->IpcpOptionsDesired.CompInfo.IPHCInfo;

    pIPHCInfo->u2NonTcpSpace = (UINT2) i4SetValPppExtIpConfigNonTcpSpace;
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtIpConfigNonTcpSpace, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
		      i4IfIndex,
                      i4SetValPppExtIpConfigNonTcpSpace));
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetPppExtIpConfigFMaxPeriod
 Input       :  The Indices
                IfIndex

                The Object 
                setValPppExtIpConfigFMaxPeriod
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIpConfigFMaxPeriod (INT4 i4IfIndex,
                                INT4 i4SetValPppExtIpConfigFMaxPeriod)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tIPCPIf            *pIpcpIf;
    tIPHCInfo          *pIPHCInfo;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIPHCInfo = &pIpcpIf->IpcpOptionsDesired.CompInfo.IPHCInfo;

    pIPHCInfo->u2FMaxPeriod = (UINT2) i4SetValPppExtIpConfigFMaxPeriod;
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtIpConfigFMaxPeriod, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
		      i4IfIndex,
                      i4SetValPppExtIpConfigFMaxPeriod));
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetPppExtIpConfigFMaxTime
 Input       :  The Indices
                IfIndex

                The Object 
                setValPppExtIpConfigFMaxTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIpConfigFMaxTime (INT4 i4IfIndex,
                              INT4 i4SetValPppExtIpConfigFMaxTime)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tIPCPIf            *pIpcpIf;
    tIPHCInfo          *pIPHCInfo;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIPHCInfo = &pIpcpIf->IpcpOptionsDesired.CompInfo.IPHCInfo;

    pIPHCInfo->u2FMaxTime = (UINT2) i4SetValPppExtIpConfigFMaxTime;
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtIpConfigFMaxTime, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
		      i4IfIndex,
                      i4SetValPppExtIpConfigFMaxTime));
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetPppExtIpConfigMaxHeader
 Input       :  The Indices
                IfIndex

                The Object 
                setValPppExtIpConfigMaxHeader
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIpConfigMaxHeader (INT4 i4IfIndex,
                               INT4 i4SetValPppExtIpConfigMaxHeader)
{

    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tIPCPIf            *pIpcpIf;
    tIPHCInfo          *pIPHCInfo;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIPHCInfo = &pIpcpIf->IpcpOptionsDesired.CompInfo.IPHCInfo;

    pIPHCInfo->u2MaxHdr = (UINT2) i4SetValPppExtIpConfigMaxHeader;
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtIpConfigMaxHeader, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
		      i4IfIndex,
                      i4SetValPppExtIpConfigMaxHeader));
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetPppExtIpConfigRtpCompression
 Input       :  The Indices
                IfIndex

                The Object 
                setValPppExtIpConfigRtpCompression
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIpConfigRtpCompression (INT4 i4IfIndex,
                                    INT4 i4SetValPppExtIpConfigRtpCompression)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tIPCPIf            *pIpcpIf;
    tIPHCInfo          *pIPHCInfo;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pIPHCInfo = &pIpcpIf->IpcpOptionsDesired.CompInfo.IPHCInfo;

    pIPHCInfo->u2RtpSubOptFlag = (UINT2) i4SetValPppExtIpConfigRtpCompression;
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtIpConfigRtpCompression, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
	              i4IfIndex,
                      i4SetValPppExtIpConfigRtpCompression));
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2PppExtIpCompConfigTable
 Input       :  The Indices
                IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2PppExtIpCompConfigTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2PppExtIpConfigTcpSpace
 Input       :  The Indices
                IfIndex

                The Object 
                testValPppExtIpConfigTcpSpace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIpConfigTcpSpace (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                 INT4 i4TestValPppExtIpConfigTcpSpace)
{
    if (PppGetIPCPIfPtr ((UINT4) i4IfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValPppExtIpConfigTcpSpace < 0)
        || (i4TestValPppExtIpConfigTcpSpace > MAX_IPHC_TCP_SPACE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2PppExtIpConfigNonTcpSpace
 Input       :  The Indices
                IfIndex

                The Object 
                testValPppExtIpConfigNonTcpSpace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIpConfigNonTcpSpace (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                    INT4 i4TestValPppExtIpConfigNonTcpSpace)
{
    if (PppGetIPCPIfPtr ((UINT4) i4IfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValPppExtIpConfigNonTcpSpace < 0)
        || (i4TestValPppExtIpConfigNonTcpSpace > MAX_IPHC_NON_TCP_SPACE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2PppExtIpConfigFMaxPeriod
 Input       :  The Indices
                IfIndex

                The Object 
                testValPppExtIpConfigFMaxPeriod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIpConfigFMaxPeriod (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                   INT4 i4TestValPppExtIpConfigFMaxPeriod)
{
    PPP_UNUSED (i4TestValPppExtIpConfigFMaxPeriod);
    if (PppGetIPCPIfPtr ((UINT4) i4IfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2PppExtIpConfigFMaxTime
 Input       :  The Indices
                IfIndex

                The Object 
                testValPppExtIpConfigFMaxTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREAT:ION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIpConfigFMaxTime (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                 INT4 i4TestValPppExtIpConfigFMaxTime)
{
    PPP_UNUSED (i4TestValPppExtIpConfigFMaxTime);
    if (PppGetIPCPIfPtr ((UINT4) i4IfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2PppExtIpConfigMaxHeader
 Input       :  The Indices
                IfIndex

                The Object 
                testValPppExtIpConfigMaxHeader
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIpConfigMaxHeader (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                  INT4 i4TestValPppExtIpConfigMaxHeader)
{
    PPP_UNUSED (i4TestValPppExtIpConfigMaxHeader);
    if (PppGetIPCPIfPtr ((UINT4) i4IfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2PppExtIpConfigRtpCompression
 Input       :  The Indices
                IfIndex

                The Object 
                testValPppExtIpConfigRtpCompression
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIpConfigRtpCompression (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                       INT4
                                       i4TestValPppExtIpConfigRtpCompression)
{
    if (PppGetIPCPIfPtr ((UINT4) i4IfIndex) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValPppExtIpConfigRtpCompression != PPP_TRUE)
        && (i4TestValPppExtIpConfigRtpCompression != PPP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : PppExtIpCompStatusTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstancePppExtIpCompStatusTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstancePppExtIpCompStatusTable (INT4 i4IfIndex)
{
    if (PppGetIPCPIfPtr ((UINT4) i4IfIndex) == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexPppExtIpCompStatusTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexPppExtIpCompStatusTable (INT4 *pi4IfIndex)
{
    if (nmhGetFirstIndex (pi4IfIndex, IpInstance, FIRST_INST) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexPppExtIpCompStatusTable
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexPppExtIpCompStatusTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    if (nmhGetNextIndex (&i4IfIndex, IpInstance, NEXT_INST) == SNMP_SUCCESS)
    {
        *pi4NextIfIndex = i4IfIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */
