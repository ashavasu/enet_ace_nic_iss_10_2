/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppiptl.c,v 1.4 2014/12/16 10:49:11 siva Exp $
 *
 * Description: Low Level Routines for IPCP module.
 *
 *******************************************************************/

#define __IP_TL_LOW_H__
#include "ipcpcom.h"
#include "snmphdrs.h"
#include "pppsnmpm.h"
#include "ipexts.h"
#include "llproto.h"
#include "ppipclow.h"
#include "pppcli.h"
#include "pppdpproto.h"
#include "ppipcpcli.h"
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetPppExtIpLocToRemoteAddress
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                retValPppTestNcpIpLocToRemoteAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIpLocToRemoteAddress (INT4 i4PppTestNcpIfIndex,
                                  UINT4
                                  *pu4RetValPppTestNcpIpLocToRemoteAddress)
{
    tIPCPIf            *pIpcpIf;
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValPppTestNcpIpLocToRemoteAddress =
        pIpcpIf->IpcpOptionsDesired.IPAddress;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppExtIpRemoteToLocAddress
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                retValPppTestNcpIpRemoteToLocAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIpRemoteToLocAddress (INT4 i4PppTestNcpIfIndex,
                                  UINT4
                                  *pu4RetValPppTestNcpIpRemoteToLocAddress)
{
    tIPCPIf            *pIpcpIf;
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValPppTestNcpIpRemoteToLocAddress =
        pIpcpIf->IpcpOptionsAllowedForPeer.IPAddress;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppExtIpAllowVJForPeer
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                retValPppTestNcpAllowVJForPeer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIpAllowVJForPeer (INT4 i4PppTestNcpIfIndex,
                              INT4 *pi4RetValPppTestNcpAllowVJForPeer)
{
    tIPCPIf            *pIpcpIf;
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pIpcpIf->IpcpGSEM.pNegFlagsPerIf[IP_COMP_IDX].FlagMask &
        ALLOWED_FOR_PEER_MASK)
    {
        *pi4RetValPppTestNcpAllowVJForPeer = 2;
    }
    else
    {
        *pi4RetValPppTestNcpAllowVJForPeer = 1;
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPppExtIpLocAddressNegFlag
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                retValPppTestNcpLocAddressNegFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIpLocAddressNegFlag (INT4 i4PppTestNcpIfIndex,
                                 INT4 *pi4RetValPppTestNcpLocAddressNegFlag)
{
    tIPCPIf            *pIpcpIf;
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pIpcpIf->IpcpGSEM.pNegFlagsPerIf[IP_ADDR_IDX].FlagMask & DESIRED_MASK)
    {
        *pi4RetValPppTestNcpLocAddressNegFlag = 2;
    }
    else
    {
        *pi4RetValPppTestNcpLocAddressNegFlag = 1;
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppExtIpAllowVJForPeer
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                testValPppTestNcpAllowVJForPeer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIpAllowVJForPeer (UINT4 *pu4ErrorCode,
                                 INT4 i4PppTestNcpIfIndex,
                                 INT4 i4TestValPppTestNcpAllowVJForPeer)
{
    tIPCPIf            *pIpcpIf;
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        PPP_UNUSED (pIpcpIf);
        return (SNMP_FAILURE);
    }

    if (i4TestValPppTestNcpAllowVJForPeer != PPP_ENABLE
        && i4TestValPppTestNcpAllowVJForPeer != PPP_DISABLE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppExtIpAllowVJForPeer
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                setValPppTestNcpAllowVJForPeer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIpAllowVJForPeer (INT4 i4PppTestNcpIfIndex,
                              INT4 i4SetValPppTestNcpAllowVJForPeer)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tIPCPIf            *pIpcpIf;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (i4SetValPppTestNcpAllowVJForPeer == PPP_ENABLE)
    {
        pIpcpIf->IpcpGSEM.pNegFlagsPerIf[IP_COMP_IDX].FlagMask |=
            ALLOWED_FOR_PEER_SET;
    }
    else
    {
        pIpcpIf->IpcpGSEM.pNegFlagsPerIf[IP_COMP_IDX].FlagMask &=
            ALLOWED_FOR_PEER_NOT_SET;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtIpAllowVJForPeer, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
		      i4PppTestNcpIfIndex,
                      i4SetValPppTestNcpAllowVJForPeer));
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppExtIpLocAddressNegFlag
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                setValPppTestNcpLocAddressNegFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIpLocAddressNegFlag (INT4 i4PppTestNcpIfIndex,
                                 INT4 i4SetValPppTestNcpLocAddressNegFlag)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tIPCPIf            *pIpcpIf;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (i4SetValPppTestNcpLocAddressNegFlag == PPP_ENABLE)
    {
        pIpcpIf->IpcpGSEM.pNegFlagsPerIf[IP_ADDR_IDX].FlagMask |= DESIRED_SET;
    }
    else
    {
        pIpcpIf->IpcpGSEM.pNegFlagsPerIf[IP_ADDR_IDX].FlagMask &=
            DESIRED_NOT_SET;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtIpLocAddressNegFlag, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
		      i4PppTestNcpIfIndex,
                      i4SetValPppTestNcpLocAddressNegFlag));
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppExtIpLocToRemoteAddress
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                setValPppTestNcpIpLocToRemoteAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIpLocToRemoteAddress (INT4 i4PppTestNcpIfIndex,
                                  UINT4 u4SetValPppTestNcpIpLocToRemoteAddress)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tIPCPIf            *pIpcpIf;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIpcpIf->IpcpOptionsDesired.IPAddress =
        u4SetValPppTestNcpIpLocToRemoteAddress;
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtIpLocToRemoteAddress, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
		      i4PppTestNcpIfIndex,
                      u4SetValPppTestNcpIpLocToRemoteAddress));
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppExtIpRemoteToLocAddress
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                setValPppTestNcpIpRemoteToLocAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIpRemoteToLocAddress (INT4 i4PppTestNcpIfIndex,
                                  UINT4 u4SetValPppTestNcpIpRemoteToLocAddress)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tIPCPIf            *pIpcpIf;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIpcpIf->IpcpOptionsAllowedForPeer.IPAddress =
        u4SetValPppTestNcpIpRemoteToLocAddress;
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtIpRemoteToLocAddress, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
		      i4PppTestNcpIfIndex,
                      u4SetValPppTestNcpIpRemoteToLocAddress));
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPppExtIpRemoteAddressNegFlag
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                retValPppTestNcpRemoteAddressNegFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIpRemoteAddressNegFlag (INT4 i4PppTestNcpIfIndex,
                                    INT4
                                    *pi4RetValPppTestNcpRemoteAddressNegFlag)
{
    tIPCPIf            *pIpcpIf;
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pIpcpIf->IpcpGSEM.pNegFlagsPerIf[IP_ADDR_IDX].FlagMask &
        ALLOWED_FOR_PEER_MASK)
    {
        *pi4RetValPppTestNcpRemoteAddressNegFlag = 2;
    }
    else
    {
        *pi4RetValPppTestNcpRemoteAddressNegFlag = 1;
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppExtIpLocToRemotePrimaryDNSAddress
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                retValPppTestNcpIpLocToRemotePrimaryDNSAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIpLocToRemotePrimaryDNSAddress (INT4 i4PppTestNcpIfIndex,
                                            UINT4
                                            *pu4RetValPppTestNcpIpLocToRemotePrimaryDNSAddress)
{
    tIPCPIf            *pIpcpIf;
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pu4RetValPppTestNcpIpLocToRemotePrimaryDNSAddress =
        pIpcpIf->IpcpOptionsDesired.PrimaryDNSAddress;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppExtIpLocToRemoteSecondaryDNSAddress
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                retValPppTestNcpIpLocToRemoteSecondaryDNSAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIpLocToRemoteSecondaryDNSAddress (INT4 i4PppTestNcpIfIndex,
                                              UINT4
                                              *pu4RetValPppTestNcpIpLocToRemoteSecondaryDNSAddress)
{
    tIPCPIf            *pIpcpIf;
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pu4RetValPppTestNcpIpLocToRemoteSecondaryDNSAddress =
        pIpcpIf->IpcpOptionsDesired.SecondaryDNSAddress;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppExtIpLocToRemotePrimaryNBNSAddress
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                retValPppTestNcpIpLocToRemotePrimaryNBNSAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIpLocToRemotePrimaryNBNSAddress (INT4 i4PppTestNcpIfIndex,
                                             UINT4
                                             *pu4RetValPppTestNcpIpLocToRemotePrimaryNBNSAddress)
{
    tIPCPIf            *pIpcpIf;
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValPppTestNcpIpLocToRemotePrimaryNBNSAddress =
        pIpcpIf->IpcpOptionsDesired.PrimaryNBNSAddress;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPppExtIpLocToRemoteSecondaryNBNSAddress
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                retValPppTestNcpIpLocToRemoteSecondaryNBNSAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetPppExtIpLocToRemoteSecondaryNBNSAddress (INT4 i4PppTestNcpIfIndex,
                                               UINT4
                                               *pu4RetValPppTestNcpIpLocToRemoteSecondaryNBNSAddress)
{
    tIPCPIf            *pIpcpIf;
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValPppTestNcpIpLocToRemoteSecondaryNBNSAddress =
        pIpcpIf->IpcpOptionsDesired.SecondaryNBNSAddress;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPppExtIpRemoteToLocPrimaryDNSAddress
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                retValPppTestNcpIpRemoteToLocPrimaryDNSAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIpRemoteToLocPrimaryDNSAddress (INT4 i4PppTestNcpIfIndex,
                                            UINT4
                                            *pu4RetValPppTestNcpIpRemoteToLocPrimaryDNSAddress)
{
    tIPCPIf            *pIpcpIf;
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValPppTestNcpIpRemoteToLocPrimaryDNSAddress =
        pIpcpIf->IpcpOptionsAllowedForPeer.PrimaryDNSAddress;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppExtIpRemoteToLocSecondaryDNSAddress
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                retValPppTestNcpIpRemoteToLocSecondaryDNSAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIpRemoteToLocSecondaryDNSAddress (INT4 i4PppTestNcpIfIndex,
                                              UINT4
                                              *pu4RetValPppTestNcpIpRemoteToLocSecondaryDNSAddress)
{
    tIPCPIf            *pIpcpIf;
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValPppTestNcpIpRemoteToLocSecondaryDNSAddress =
        pIpcpIf->IpcpOptionsAllowedForPeer.SecondaryDNSAddress;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppExtIpRemoteToLocPrimaryNBNSAddress
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                retValPppTestNcpIpRemoteToLocPrimaryNBNSAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIpRemoteToLocPrimaryNBNSAddress (INT4 i4PppTestNcpIfIndex,
                                             UINT4
                                             *pu4RetValPppTestNcpIpRemoteToLocPrimaryNBNSAddress)
{
    tIPCPIf            *pIpcpIf;
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValPppTestNcpIpRemoteToLocPrimaryNBNSAddress =
        pIpcpIf->IpcpOptionsAllowedForPeer.PrimaryNBNSAddress;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppExtIpRemoteToLocSecondaryNBNSAddress
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                retValPppTestNcpIpRemoteToLocSecondaryNBNSAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIpRemoteToLocSecondaryNBNSAddress (INT4 i4PppTestNcpIfIndex,
                                               UINT4
                                               *pu4RetValPppTestNcpIpRemoteToLocSecondaryNBNSAddress)
{
    tIPCPIf            *pIpcpIf;
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pu4RetValPppTestNcpIpRemoteToLocSecondaryNBNSAddress =
        pIpcpIf->IpcpOptionsAllowedForPeer.SecondaryNBNSAddress;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetPppExtIpLocPrimaryDNSAddressNegFlag
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                retValPppTestNcpLocPrimaryDNSAddressNegFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIpLocPrimaryDNSAddressNegFlag (INT4 i4PppTestNcpIfIndex,
                                           INT4
                                           *pi4RetValPppTestNcpLocPrimaryDNSAddressNegFlag)
{
    tIPCPIf            *pIpcpIf;
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pIpcpIf->IpcpGSEM.pNegFlagsPerIf[PRIMARY_DNS_ADDR_IDX].
        FlagMask & DESIRED_MASK)
    {
        *pi4RetValPppTestNcpLocPrimaryDNSAddressNegFlag = 2;
    }
    else
    {
        *pi4RetValPppTestNcpLocPrimaryDNSAddressNegFlag = 1;
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppExtIpLocSecondaryDNSAddressNegFlag
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                retValPppTestNcpLocSecondaryDNSAddressNegFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIpLocSecondaryDNSAddressNegFlag (INT4 i4PppTestNcpIfIndex,
                                             INT4
                                             *pi4RetValPppTestNcpLocSecondaryDNSAddressNegFlag)
{
    tIPCPIf            *pIpcpIf;
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pIpcpIf->IpcpGSEM.pNegFlagsPerIf[SECONDARY_DNS_ADDR_IDX].
        FlagMask & DESIRED_MASK)
    {
        *pi4RetValPppTestNcpLocSecondaryDNSAddressNegFlag = 2;
    }
    else
    {
        *pi4RetValPppTestNcpLocSecondaryDNSAddressNegFlag = 1;
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppExtIpLocPrimaryNBNSAddressNegFlag
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                retValPppTestNcpLocPrimaryNBNSAddressNegFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIpLocPrimaryNBNSAddressNegFlag (INT4 i4PppTestNcpIfIndex,
                                            INT4
                                            *pi4RetValPppTestNcpLocPrimaryNBNSAddressNegFlag)
{
    tIPCPIf            *pIpcpIf;
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pIpcpIf->IpcpGSEM.pNegFlagsPerIf[PRIMARY_NBNS_ADDR_IDX].
        FlagMask & DESIRED_MASK)
    {
        *pi4RetValPppTestNcpLocPrimaryNBNSAddressNegFlag = 2;
    }
    else
    {
        *pi4RetValPppTestNcpLocPrimaryNBNSAddressNegFlag = 1;
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetPppExtIpLocSecondaryNBNSAddressNegFlag
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                retValPppTestNcpLocSecondaryNBNSAddressNegFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetPppExtIpLocSecondaryNBNSAddressNegFlag (INT4 i4PppTestNcpIfIndex,
                                              INT4
                                              *pi4RetValPppTestNcpLocSecondaryNBNSAddressNegFlag)
{
    tIPCPIf            *pIpcpIf;
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (pIpcpIf->IpcpGSEM.pNegFlagsPerIf[SECONDARY_NBNS_ADDR_IDX].
        FlagMask & DESIRED_MASK)
    {
        *pi4RetValPppTestNcpLocSecondaryNBNSAddressNegFlag = 2;
    }
    else
    {
        *pi4RetValPppTestNcpLocSecondaryNBNSAddressNegFlag = 1;
    }
    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetPppExtIpRemoteAddressNegFlag
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                setValPppTestNcpRemoteAddressNegFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIpRemoteAddressNegFlag (INT4 i4PppTestNcpIfIndex,
                                    INT4 i4SetValPppTestNcpRemoteAddressNegFlag)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tIPCPIf            *pIpcpIf;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (i4SetValPppTestNcpRemoteAddressNegFlag == PPP_ENABLE)
    {
        pIpcpIf->IpcpGSEM.pNegFlagsPerIf[IP_ADDR_IDX].FlagMask |=
            FORCED_TO_REQUEST_SET;
    }
    else
    {
        pIpcpIf->IpcpGSEM.pNegFlagsPerIf[IP_ADDR_IDX].FlagMask &=
            ALLOWED_FOR_PEER_NOT_SET;
/*            FORCED_TO_REQUEST_NOT_SET;*/
    }
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtIpRemoteAddressNegFlag, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
		      i4PppTestNcpIfIndex,
                      i4SetValPppTestNcpRemoteAddressNegFlag));
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppExtIpLocToRemotePrimaryDNSAddress
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                setValPppTestNcpIpLocToRemotePrimaryDNSAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIpLocToRemotePrimaryDNSAddress (INT4 i4PppTestNcpIfIndex,
                                            UINT4
                                            u4SetValPppTestNcpIpLocToRemotePrimaryDNSAddress)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tIPCPIf            *pIpcpIf;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIpcpIf->IpcpOptionsDesired.PrimaryDNSAddress =
        u4SetValPppTestNcpIpLocToRemotePrimaryDNSAddress;
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtIpLocToRemotePrimaryDNSAddress, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
		      i4PppTestNcpIfIndex,
                      u4SetValPppTestNcpIpLocToRemotePrimaryDNSAddress));
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppExtIpLocToRemoteSecondaryDNSAddress
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                setValPppTestNcpIpLocToRemoteSecondaryDNSAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIpLocToRemoteSecondaryDNSAddress (INT4 i4PppTestNcpIfIndex,
                                              UINT4
                                              u4SetValPppTestNcpIpLocToRemoteSecondaryDNSAddress)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tIPCPIf            *pIpcpIf;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIpcpIf->IpcpOptionsDesired.SecondaryDNSAddress =
        u4SetValPppTestNcpIpLocToRemoteSecondaryDNSAddress;
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtIpLocToRemoteSecondaryDNSAddress, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
		      i4PppTestNcpIfIndex,
                      u4SetValPppTestNcpIpLocToRemoteSecondaryDNSAddress));
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetPppExtIpLocToRemotePrimaryNBNSAddress
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                setValPppTestNcpIpLocToRemotePrimaryNBNSAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIpLocToRemotePrimaryNBNSAddress (INT4 i4PppTestNcpIfIndex,
                                             UINT4
                                             u4SetValPppTestNcpIpLocToRemotePrimaryNBNSAddress)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tIPCPIf            *pIpcpIf;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIpcpIf->IpcpOptionsDesired.PrimaryNBNSAddress =
        u4SetValPppTestNcpIpLocToRemotePrimaryNBNSAddress;
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtIpLocToRemotePrimaryNBNSAddress, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
		      i4PppTestNcpIfIndex,
		      u4SetValPppTestNcpIpLocToRemotePrimaryNBNSAddress));
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppExtIpLocToRemoteSecondaryNBNSAddress
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                setValPppTestNcpIpLocToRemoteSecondaryNBNSAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIpLocToRemoteSecondaryNBNSAddress (INT4 i4PppTestNcpIfIndex,
                                               UINT4
                                               u4SetValPppTestNcpIpLocToRemoteSecondaryNBNSAddress)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tIPCPIf            *pIpcpIf;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIpcpIf->IpcpOptionsDesired.SecondaryNBNSAddress =
        u4SetValPppTestNcpIpLocToRemoteSecondaryNBNSAddress;
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtIpLocToRemoteSecondaryNBNSAddress, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
		      i4PppTestNcpIfIndex,
                      u4SetValPppTestNcpIpLocToRemoteSecondaryNBNSAddress));
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppExtIpRemoteToLocPrimaryDNSAddress
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                setValPppTestNcpIpRemoteToLocPrimaryDNSAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIpRemoteToLocPrimaryDNSAddress (INT4 i4PppTestNcpIfIndex,
                                            UINT4
                                            u4SetValPppTestNcpIpRemoteToLocPrimaryDNSAddress)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tIPCPIf            *pIpcpIf;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIpcpIf->IpcpOptionsAllowedForPeer.PrimaryDNSAddress =
        u4SetValPppTestNcpIpRemoteToLocPrimaryDNSAddress;
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtIpRemoteToLocPrimaryDNSAddress, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
		      i4PppTestNcpIfIndex,
                      u4SetValPppTestNcpIpRemoteToLocPrimaryDNSAddress));
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetPppExtIpRemoteToLocSecondaryDNSAddress
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                setValPppTestNcpIpRemoteToLocSecondaryDNSAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIpRemoteToLocSecondaryDNSAddress (INT4 i4PppTestNcpIfIndex,
                                              UINT4
                                              u4SetValPppTestNcpIpRemoteToLocSecondaryDNSAddress)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tIPCPIf            *pIpcpIf;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIpcpIf->IpcpOptionsAllowedForPeer.SecondaryDNSAddress =
        u4SetValPppTestNcpIpRemoteToLocSecondaryDNSAddress;
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtIpRemoteToLocSecondaryDNSAddress, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
		      i4PppTestNcpIfIndex,
                      u4SetValPppTestNcpIpRemoteToLocSecondaryDNSAddress));
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetPppExtIpRemoteToLocPrimaryNBNSAddress
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                setValPppTestNcpIpRemoteToLocPrimaryNBNSAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIpRemoteToLocPrimaryNBNSAddress (INT4 i4PppTestNcpIfIndex,
                                             UINT4
                                             u4SetValPppTestNcpIpRemoteToLocPrimaryNBNSAddress)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tIPCPIf            *pIpcpIf;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIpcpIf->IpcpOptionsAllowedForPeer.PrimaryNBNSAddress =
        u4SetValPppTestNcpIpRemoteToLocPrimaryNBNSAddress;
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtIpRemoteToLocPrimaryNBNSAddress, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
		      i4PppTestNcpIfIndex,
                      u4SetValPppTestNcpIpRemoteToLocPrimaryNBNSAddress));
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetPppExtIpRemoteToLocSecondaryNBNSAddress
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                setValPppTestNcpIpRemoteToLocSecondaryNBNSAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIpRemoteToLocSecondaryNBNSAddress (INT4 i4PppTestNcpIfIndex,
                                               UINT4
                                               u4SetValPppTestNcpIpRemoteToLocSecondaryNBNSAddress)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tIPCPIf            *pIpcpIf;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    pIpcpIf->IpcpOptionsAllowedForPeer.SecondaryNBNSAddress =
        u4SetValPppTestNcpIpRemoteToLocSecondaryNBNSAddress;
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtIpRemoteToLocSecondaryNBNSAddress, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u",
		      i4PppTestNcpIfIndex,
                      u4SetValPppTestNcpIpRemoteToLocSecondaryNBNSAddress));
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetPppExtIpLocPrimaryDNSAddressNegFlag
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                setValPppTestNcpLocPrimaryDNSAddressNegFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIpLocPrimaryDNSAddressNegFlag (INT4 i4PppTestNcpIfIndex,
                                           INT4
                                           i4SetValPppTestNcpLocPrimaryDNSAddressNegFlag)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tIPCPIf            *pIpcpIf;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (i4SetValPppTestNcpLocPrimaryDNSAddressNegFlag == PPP_ENABLE)
    {
        pIpcpIf->IpcpGSEM.pNegFlagsPerIf[PRIMARY_DNS_ADDR_IDX].FlagMask |=
            DESIRED_SET;
    }
    else
    {
        pIpcpIf->IpcpGSEM.pNegFlagsPerIf[PRIMARY_DNS_ADDR_IDX].FlagMask &=
            DESIRED_NOT_SET;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtIpLocPrimaryDNSAddressNegFlag, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
		      i4PppTestNcpIfIndex,
                      i4SetValPppTestNcpLocPrimaryDNSAddressNegFlag));
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppExtIpLocSecondaryDNSAddressNegFlag
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                setValPppTestNcpLocSecondaryDNSAddressNegFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIpLocSecondaryDNSAddressNegFlag (INT4 i4PppTestNcpIfIndex,
                                             INT4
                                             i4SetValPppTestNcpLocSecondaryDNSAddressNegFlag)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tIPCPIf            *pIpcpIf;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (i4SetValPppTestNcpLocSecondaryDNSAddressNegFlag == PPP_ENABLE)
    {
        pIpcpIf->IpcpGSEM.pNegFlagsPerIf[SECONDARY_DNS_ADDR_IDX].FlagMask |=
            DESIRED_SET;
    }
    else
    {
        pIpcpIf->IpcpGSEM.pNegFlagsPerIf[SECONDARY_DNS_ADDR_IDX].FlagMask &=
            DESIRED_NOT_SET;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtIpLocSecondaryDNSAddressNegFlag, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
		      i4PppTestNcpIfIndex,
                      i4SetValPppTestNcpLocSecondaryDNSAddressNegFlag));
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppExtIpLocPrimaryNBNSAddressNegFlag
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                setValPppTestNcpLocPrimaryNBNSAddressNegFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIpLocPrimaryNBNSAddressNegFlag (INT4 i4PppTestNcpIfIndex,
                                            INT4
                                            i4SetValPppTestNcpLocPrimaryNBNSAddressNegFlag)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tIPCPIf            *pIpcpIf;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (i4SetValPppTestNcpLocPrimaryNBNSAddressNegFlag == PPP_ENABLE)
    {
        pIpcpIf->IpcpGSEM.pNegFlagsPerIf[PRIMARY_NBNS_ADDR_IDX].FlagMask |=
            DESIRED_SET;
    }
    else
    {
        pIpcpIf->IpcpGSEM.pNegFlagsPerIf[PRIMARY_NBNS_ADDR_IDX].FlagMask &=
            DESIRED_NOT_SET;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtIpLocPrimaryNBNSAddressNegFlag, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
		      i4PppTestNcpIfIndex,
                      i4SetValPppTestNcpLocPrimaryNBNSAddressNegFlag));
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetPppExtIpLocSecondaryNBNSAddressNegFlag
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                setValPppTestNcpLocSecondaryNBNSAddressNegFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetPppExtIpLocSecondaryNBNSAddressNegFlag (INT4 i4PppTestNcpIfIndex,
                                              INT4
                                              i4SetValPppTestNcpLocSecondaryNBNSAddressNegFlag)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    tIPCPIf            *pIpcpIf;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (i4SetValPppTestNcpLocSecondaryNBNSAddressNegFlag == PPP_ENABLE)
    {
        pIpcpIf->IpcpGSEM.pNegFlagsPerIf[SECONDARY_NBNS_ADDR_IDX].FlagMask |=
            DESIRED_SET;
    }
    else
    {
        pIpcpIf->IpcpGSEM.pNegFlagsPerIf[SECONDARY_NBNS_ADDR_IDX].FlagMask &=
            DESIRED_NOT_SET;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, PppExtIpLocSecondaryNBNSAddressNegFlag, u4SeqNum,
                      FALSE, PppLock, PppUnlock, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
		      i4PppTestNcpIfIndex,
                      i4SetValPppTestNcpLocSecondaryNBNSAddressNegFlag));
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2PppExtIpLocAddressNegFlag
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                testValPppTestNcpLocAddressNegFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIpLocAddressNegFlag (UINT4 *pu4ErrorCode,
                                    INT4 i4PppTestNcpIfIndex,
                                    INT4 i4TestValPppTestNcpLocAddressNegFlag)
{
    tIPCPIf            *pIpcpIf;

    if ((pIpcpIf = PppGetIPCPIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        PPP_UNUSED (pIpcpIf);
        return (SNMP_FAILURE);
    }

    if (i4TestValPppTestNcpLocAddressNegFlag != PPP_ENABLE
        && i4TestValPppTestNcpLocAddressNegFlag != PPP_DISABLE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppExtIpLocToRemoteAddress
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                testValPppTestNcpIpLocToRemoteAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIpLocToRemoteAddress (UINT4 *pu4ErrorCode,
                                     INT4 i4PppTestNcpIfIndex,
                                     UINT4
                                     u4TestValPppTestNcpIpLocToRemoteAddress)
{
    PPP_UNUSED (i4PppTestNcpIfIndex);
    PPP_UNUSED (u4TestValPppTestNcpIpLocToRemoteAddress);
    PPP_UNUSED (pu4ErrorCode);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppExtIpRemoteToLocAddress
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                testValPppTestNcpIpRemoteToLocAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIpRemoteToLocAddress (UINT4 *pu4ErrorCode,
                                     INT4 i4PppTestNcpIfIndex,
                                     UINT4
                                     u4TestValPppTestNcpIpRemoteToLocAddress)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = (tPPPIf *) PppGetIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pIf->LcpStatus.AdminStatus == ADMIN_OPEN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_PPP_ADMIN_STAT_OPEN);
        return SNMP_FAILURE;
    }

    if (!((PPP_IS_ADDR_CLASS_A (u4TestValPppTestNcpIpRemoteToLocAddress)) ||
          (PPP_IS_ADDR_CLASS_B (u4TestValPppTestNcpIpRemoteToLocAddress)) ||
          (PPP_IS_ADDR_CLASS_C (u4TestValPppTestNcpIpRemoteToLocAddress)) ||
          (u4TestValPppTestNcpIpRemoteToLocAddress == 0)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_PPP_INVALID_IP);
        return SNMP_FAILURE;
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppExtIpRemoteAddressNegFlag
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                testValPppTestNcpRemoteAddressNegFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIpRemoteAddressNegFlag (UINT4 *pu4ErrorCode,
                                       INT4 i4PppTestNcpIfIndex,
                                       INT4
                                       i4TestValPppTestNcpRemoteAddressNegFlag)
{
    PPP_UNUSED (i4PppTestNcpIfIndex);
    if ((PPP_DISABLE == i4TestValPppTestNcpRemoteAddressNegFlag)
        || (PPP_ENABLE == i4TestValPppTestNcpRemoteAddressNegFlag))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2PppExtIpLocToRemotePrimaryDNSAddress
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                testValPppTestNcpIpLocToRemotePrimaryDNSAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIpLocToRemotePrimaryDNSAddress (UINT4 *pu4ErrorCode,
                                               INT4 i4PppTestNcpIfIndex,
                                               UINT4
                                               u4TestValPppTestNcpIpLocToRemotePrimaryDNSAddress)
{
    PPP_UNUSED (i4PppTestNcpIfIndex);
    PPP_UNUSED (u4TestValPppTestNcpIpLocToRemotePrimaryDNSAddress);
    PPP_UNUSED (pu4ErrorCode);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppExtIpLocToRemoteSecondaryDNSAddress
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                testValPppTestNcpIpLocToRemoteSecondaryDNSAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIpLocToRemoteSecondaryDNSAddress (UINT4 *pu4ErrorCode,
                                                 INT4 i4PppTestNcpIfIndex,
                                                 UINT4
                                                 u4TestValPppTestNcpIpLocToRemoteSecondaryDNSAddress)
{
    PPP_UNUSED (i4PppTestNcpIfIndex);
    PPP_UNUSED (u4TestValPppTestNcpIpLocToRemoteSecondaryDNSAddress);
    PPP_UNUSED (pu4ErrorCode);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppExtIpLocToRemotePrimaryNBNSAddress
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                testValPppTestNcpIpLocToRemotePrimaryNBNSAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIpLocToRemotePrimaryNBNSAddress (UINT4 *pu4ErrorCode,
                                                INT4 i4PppTestNcpIfIndex,
                                                UINT4
                                                u4TestValPppTestNcpIpLocToRemotePrimaryNBNSAddress)
{
    PPP_UNUSED (i4PppTestNcpIfIndex);
    PPP_UNUSED (u4TestValPppTestNcpIpLocToRemotePrimaryNBNSAddress);
    PPP_UNUSED (pu4ErrorCode);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppExtIpLocToRemoteSecondaryNBNSAddress
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                testValPppTestNcpIpLocToRemoteSecondaryNBNSAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIpLocToRemoteSecondaryNBNSAddress (UINT4 *pu4ErrorCode,
                                                  INT4 i4PppTestNcpIfIndex,
                                                  UINT4
                                                  u4TestValPppTestNcpIpLocToRemoteSecondaryNBNSAddress)
{
    PPP_UNUSED (i4PppTestNcpIfIndex);
    PPP_UNUSED (u4TestValPppTestNcpIpLocToRemoteSecondaryNBNSAddress);
    PPP_UNUSED (pu4ErrorCode);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppExtIpRemoteToLocPrimaryDNSAddress
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                testValPppTestNcpIpRemoteToLocPrimaryDNSAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIpRemoteToLocPrimaryDNSAddress (UINT4 *pu4ErrorCode,
                                               INT4 i4PppTestNcpIfIndex,
                                               UINT4
                                               u4TestValPppTestNcpIpRemoteToLocPrimaryDNSAddress)
{
    tPPPIf             *pIf = NULL;

    if ((pIf = PppGetIfPtr ((UINT4) i4PppTestNcpIfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pIf->LcpStatus.AdminStatus == ADMIN_OPEN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_PPP_ADMIN_STAT_OPEN);
        return SNMP_FAILURE;
    }

    if (!
        ((PPP_IS_ADDR_CLASS_A
          (u4TestValPppTestNcpIpRemoteToLocPrimaryDNSAddress))
         ||
         (PPP_IS_ADDR_CLASS_B
          (u4TestValPppTestNcpIpRemoteToLocPrimaryDNSAddress))
         ||
         (PPP_IS_ADDR_CLASS_C
          (u4TestValPppTestNcpIpRemoteToLocPrimaryDNSAddress))
         || (u4TestValPppTestNcpIpRemoteToLocPrimaryDNSAddress == 0)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_PPP_INVALID_IP);
        return SNMP_FAILURE;
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppExtIpRemoteToLocSecondaryDNSAddress
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                testValPppTestNcpIpRemoteToLocSecondaryDNSAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIpRemoteToLocSecondaryDNSAddress (UINT4 *pu4ErrorCode,
                                                 INT4 i4PppTestNcpIfIndex,
                                                 UINT4
                                                 u4TestValPppTestNcpIpRemoteToLocSecondaryDNSAddress)
{
    PPP_UNUSED (i4PppTestNcpIfIndex);
    PPP_UNUSED (u4TestValPppTestNcpIpRemoteToLocSecondaryDNSAddress);
    PPP_UNUSED (pu4ErrorCode);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppExtIpRemoteToLocPrimaryNBNSAddress
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                testValPppTestNcpIpRemoteToLocPrimaryNBNSAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIpRemoteToLocPrimaryNBNSAddress (UINT4 *pu4ErrorCode,
                                                INT4 i4PppTestNcpIfIndex,
                                                UINT4
                                                u4TestValPppTestNcpIpRemoteToLocPrimaryNBNSAddress)
{
    PPP_UNUSED (i4PppTestNcpIfIndex);
    PPP_UNUSED (u4TestValPppTestNcpIpRemoteToLocPrimaryNBNSAddress);
    PPP_UNUSED (pu4ErrorCode);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppExtIpRemoteToLocSecondaryNBNSAddress
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                testValPppTestNcpIpRemoteToLocSecondaryNBNSAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIpRemoteToLocSecondaryNBNSAddress (UINT4 *pu4ErrorCode,
                                                  INT4 i4PppTestNcpIfIndex,
                                                  UINT4
                                                  u4TestValPppTestNcpIpRemoteToLocSecondaryNBNSAddress)
{
    PPP_UNUSED (i4PppTestNcpIfIndex);
    PPP_UNUSED (u4TestValPppTestNcpIpRemoteToLocSecondaryNBNSAddress);
    PPP_UNUSED (pu4ErrorCode);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2PppExtIpLocPrimaryDNSAddressNegFlag
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                testValPppTestNcpLocPrimaryDNSAddressNegFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIpLocPrimaryDNSAddressNegFlag (UINT4 *pu4ErrorCode,
                                              INT4 i4PppTestNcpIfIndex,
                                              INT4
                                              i4TestValPppTestNcpLocPrimaryDNSAddressNegFlag)
{
    PPP_UNUSED (i4PppTestNcpIfIndex);
    if ((PPP_DISABLE == i4TestValPppTestNcpLocPrimaryDNSAddressNegFlag)
        || (PPP_ENABLE == i4TestValPppTestNcpLocPrimaryDNSAddressNegFlag))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2PppExtIpLocSecondaryDNSAddressNegFlag
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                testValPppTestNcpLocSecondaryDNSAddressNegFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIpLocSecondaryDNSAddressNegFlag (UINT4 *pu4ErrorCode,
                                                INT4 i4PppTestNcpIfIndex,
                                                INT4
                                                i4TestValPppTestNcpLocSecondaryDNSAddressNegFlag)
{
    PPP_UNUSED (i4PppTestNcpIfIndex);
    if ((PPP_DISABLE == i4TestValPppTestNcpLocSecondaryDNSAddressNegFlag)
        || (PPP_ENABLE == i4TestValPppTestNcpLocSecondaryDNSAddressNegFlag))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2PppExtIpLocPrimaryNBNSAddressNegFlag
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                testValPppTestNcpLocPrimaryNBNSAddressNegFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIpLocPrimaryNBNSAddressNegFlag (UINT4 *pu4ErrorCode,
                                               INT4 i4PppTestNcpIfIndex,
                                               INT4
                                               i4TestValPppTestNcpLocPrimaryNBNSAddressNegFlag)
{
    PPP_UNUSED (i4PppTestNcpIfIndex);
    if ((PPP_DISABLE == i4TestValPppTestNcpLocPrimaryNBNSAddressNegFlag)
        || (PPP_ENABLE == i4TestValPppTestNcpLocPrimaryNBNSAddressNegFlag))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2PppExtIpLocSecondaryNBNSAddressNegFlag
 Input       :  The Indices
                PppTestNcpIfIndex

                The Object 
                testValPppTestNcpLocSecondaryNBNSAddressNegFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2PppExtIpLocSecondaryNBNSAddressNegFlag (UINT4 *pu4ErrorCode,
                                                 INT4 i4PppTestNcpIfIndex,
                                                 INT4
                                                 i4TestValPppTestNcpLocSecondaryNBNSAddressNegFlag)
{
    PPP_UNUSED (i4PppTestNcpIfIndex);
    if ((PPP_ENABLE == i4TestValPppTestNcpLocSecondaryNBNSAddressNegFlag)
        || (PPP_DISABLE == i4TestValPppTestNcpLocSecondaryNBNSAddressNegFlag))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}
