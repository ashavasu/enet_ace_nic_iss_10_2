/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipexts.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains definitions and prototypes for 
 * IPCP functions used by external modules
 *
 *******************************************************************/
#ifndef __PPP_IPEXTS_H__
#define __PPP_IPEXTS_H__


extern   UINT2   IpcpIfFreeCounter;
extern   UINT2   IpcpAllocCounter;
extern   UINT2   IpcpFreeCounter;
extern   UINT1   VJCompAvailable;
extern  tGenOptionInfo          IPCPGenOptionInfo[];
extern  tOptNegFlagsPerIf       IPCPOptPerIntf[];
extern  tGSEMCallbacks          IpcpGSEMCallback;
extern   tIPCPCtrlBlk           IPCPCtrlBlk;

extern UINT1 gu1IPCPAddrModeSelector;
#endif  /* __PPP_IPEXTS_H__ */
