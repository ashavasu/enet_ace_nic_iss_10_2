/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipmidpr.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains prototypes of Middle Level Routines
 *
 *******************************************************************/
#ifndef __PPP_IPMIDPR_H__
#define __PPP_IPMIDPR_H__

/* Prototypes of Middle Level Routines */

VarBind * SNMPIpEntryGet( OIDType *InDataBasePtr, OIDType *InNamePtr, UINT1 Arg, UINT1 SearchType);
VarBind * SNMPIpConfigEntryGet( OIDType *InDataBasePtr, OIDType *InNamePtr, UINT1 Arg, UINT1 SearchType);
INT4 SNMPIpConfigEntryTest( OIDType *InDataBasePtr, OIDType *InNamePtr, UINT1 Arg, MultiDataType *Value);
INT4  SNMPIpConfigEntrySet( OIDType *InDataBasePtr, OIDType *InNamePtr, UINT1 Arg, MultiDataType *Value);


#endif  /* __PPP_IPMIDPR_H__ */
