/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipcpcom.h,v 1.3 2013/03/28 12:12:07 siva Exp $
 *
 * Description:This file contains the common PPP files required for
 * the IPCP module
 *
 *******************************************************************/
#ifndef __PPP_IPCPCOM_H__
#define __PPP_IPCPCOM_H__

#include "genhdrs.h"

/* Global PPP includes */
#include "pppmacro.h"
#include "pppdefs.h"
#include "pppdebug.h"
#include "ppptimer.h"
#include "pppdbgex.h"


/* Includes from other modules */
#include "frmtdfs.h"
#include "pppgsem.h"
#include "pppgcp.h"
#include "ppplcp.h"
#include "pppexts.h"

#include "pppipcp.h"

#include "ipproto.h"
#include "cfa.h"
#include "ppp.h"
#include "pppoe.h"
#include "arp.h"

#ifdef NPAPI_WANTED
#include "npapi.h"
#include "pppnpwr.h"
#endif

#endif  /* __PPP_IPCPCOM_H__ */
