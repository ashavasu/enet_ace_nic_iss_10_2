/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipproto.h,v 1.4 2014/11/14 12:04:44 siva Exp $
 *
 * Description:This file contains Prototypes for the IPCP functions 
 * used internally by IPCP module
 *
 *******************************************************************/
#ifndef __PPP_IPPROTO_H__
#define __PPP_IPPROTO_H__

/* Prototypes for IPCP functions used internally by IPCP module */

tIPCPIf *IPCPCreateIf (tPPPIf *pIf);
VOID    IPCPDeleteIf  (tIPCPIf *pIpcpPtr);
INT1    IPCPInit      (tIPCPIf *pIpcpPtr);
VOID    IPCPOptionInit(tIPCPOptions *pIPCPOpt);
VOID    IPCPUp        (tGSEM *pIpcpGSEM);
VOID    IPCPDown      (tGSEM *pIpcpGSEM);
VOID    IPCPFinished  (tGSEM *pIpcpGSEM);
INT4 IPCPCheckAssignedAddrForInterface (tPPPIf *pIf,UINT4 *pu4Addr);
INT1 IPCPProcessIPAddrConfReq          (tGSEM *pGSEM, t_MSG_DESC *pInPkt, 
                                        tOptVal Optval, UINT2 Offset, 
                                        UINT1 PresenceFlag);
INT1 IPCPProcessCompressionConfReq     (tGSEM *pGSEM, t_MSG_DESC *pInPkt,
                                        tOptVal Optval, UINT2 Offset,
                                        UINT1 PresenceFlag);
INT1 IPCPProcessPrimaryDNSAddrConfReq  (tGSEM *pGSEM, t_MSG_DESC *pInPkt,
                                        tOptVal Optval, UINT2 Offset,
                                        UINT1 PresenceFlag);
INT1 IPCPProcessPrimaryNBNSAddrConfReq (tGSEM *pGSEM, t_MSG_DESC *pInPkt,
                                        tOptVal Optval, UINT2 Offset,
                                        UINT1 PresenceFlag);
INT1 IPCPProcessSecondaryDNSAddrConfReq (tGSEM *pGSEM, t_MSG_DESC *pInPkt,
                                         tOptVal Optval, UINT2 Offset, 
                                         UINT1 PresenceFlag);
INT1 IPCPProcessSecondaryNBNSAddrConfReq(tGSEM *pGSEM, t_MSG_DESC *pInPkt,
                                         tOptVal Optval, UINT2 Offset,
                                         UINT1 PresenceFlag);

INT1 IPCPProcessIPAddrConfNak           (tGSEM *pGSEM, t_MSG_DESC *pInPkt,
                                         tOptVal OptVal);
INT1 IPCPProcessCompressionConfNak      (tGSEM *pGSEM, t_MSG_DESC *pInPkt,
                                         tOptVal OptVal);
INT1 IPCPProcessPrimaryDNSAddrConfNak   (tGSEM *pGSEM, t_MSG_DESC *pInPkt,
                                         tOptVal OptVal);
INT1 IPCPProcessPrimaryNBNSAddrConfNak  (tGSEM *pGSEM, t_MSG_DESC *pInPkt,
                                         tOptVal OptVal);
INT1 IPCPProcessSecondaryDNSAddrConfNak (tGSEM *pGSEM, t_MSG_DESC *pInPkt,
                                         tOptVal OptVal);
INT1 IPCPProcessSecondaryNBNSAddrConfNak(tGSEM *pGSEM, t_MSG_DESC *pInPkt,
                                         tOptVal OptVal);

INT1 IPCPProcessIPAddrConfRej           (tGSEM *pGSEM );
INT1 IPCPProcessCompressionConfRej      (tGSEM *pGSEM );
INT1 IPCPProcessPrimaryDNSAddrConfRej   (tGSEM *pGSEM );
INT1 IPCPProcessPrimaryNBNSAddrConfRej  (tGSEM *pGSEM );
INT1 IPCPProcessSecondaryDNSAddrConfRej (tGSEM *pGSEM );
INT1 IPCPProcessSecondaryNBNSAddrConfRej(tGSEM *pGSEM );

INT1 IPCPAddCompSubOpt(tGSEM *pGSEM, tOptVal OptVal, t_MSG_DESC *);

VOID *IPCPReturnIPAddrPtr           (tGSEM  *pGSEM, UINT1 *OptLen);
VOID *IPCPReturnCompPtr             (tGSEM  *pGSEM, UINT1 *OptLen);
VOID *IPCPReturnPrimaryDNSAddrPtr   (tGSEM  *pGSEM, UINT1 *OptLen);
VOID *IPCPReturnPrimaryNBNSAddrPtr  (tGSEM  *pGSEM, UINT1 *OptLen);
VOID *IPCPReturnSecondaryDNSAddrPtr (tGSEM  *pGSEM, UINT1 *OptLen);
VOID *IPCPReturnSecondaryNBNSAddrPtr(tGSEM  *pGSEM, UINT1 *OptLen);

INT4 IPCPAddAddrToAllocList (t_SLL  *pAllocAddrList, UINT4 u4Addr,UINT4 u4Port);
INT4 IPCPCheckIfAddrFree    (t_SLL *pAllocAddrList ,UINT4 u4Addr);
INT4 IPCPGetFreeAddrFromPool(tPPPIf *pIf , UINT4 *pu4Addr);

tIPCPAddrPool *IPCPGetOrCreateAddrPool(UINT4 Index);
tIPCPAddrPool *IPCPGetAddrPool        (UINT4 Index);
tIPCPAddrPool *IPCPCreateNewAddrPool  (UINT4 Index);

/* SNMP related */
tIPCPIf *PppGetIPCPIfPtr   (UINT4 Index);
tIPCPIf *PppCreateIPCPIfPtr(UINT4 Index);
INT4
IPCPGetPeerIpAddr (UINT4 *pu4Addr, UINT4 u4IfIndex);
#endif  /* __PPP_IPPROTO_H__ */
