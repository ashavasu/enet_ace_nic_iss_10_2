/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vjproto.h,v 1.1.1.1 2010/11/24 11:42:01 siva Exp $
 *
 * Description:This file contains the prototypes for the VJ functions 
 * used internally by the VJ module
 *
 *******************************************************************/

struct slcompress *VJCOMPCreate (tPPPIf *pIf);
UINT2 VJCompressPkt ( t_MSG_DESC  **pBuffer, tPPPIf *pIf);
t_MSG_DESC *VJUncompressPkt(t_MSG_DESC *pBuffer,tPPPIf *pIf,UINT2 *Type,UINT4 );
UINT1   sl_compress_tcp( struct vjmbuf *m,struct slcompress *comp,int ); 
UINT1 * sl_uncompress_tcp( UINT1 *bufp, UINT4 *, UINT4 , struct slcompress *comp); 
void sl_compress_init(struct slcompress *comp); 
