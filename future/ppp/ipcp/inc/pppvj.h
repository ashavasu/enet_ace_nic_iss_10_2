/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pppvj.h,v 1.2 2014/09/01 11:02:09 siva Exp $
 *
 * Description:This file contains the definitions used by the VJ module
 *
 *******************************************************************/
struct in_addr1
  {
     unsigned int s_addr;
  };

struct ip
  {
    UINT1 u1VerHdrLen;      /* version and header length */
    UINT1 ip_tos;           /* type of service */
    UINT2 ip_len;           /* total length */
    UINT2 ip_id;            /* identification */
    UINT2 ip_off;           /* fragment offset field */
    UINT1 ip_ttl;           /* time to live */
    UINT1 ip_p;             /* protocol */
    UINT2 ip_sum;           /* checksum */
    struct in_addr1 ip_src,ip_dst;  /* source and dest address */
  };

/*****************************/

typedef UINT4 tcp_seq;

struct tcphdr
  {
    UINT2 th_sport;     /* source port */
    UINT2 th_dport;     /* destination port */
    tcp_seq th_seq;     /* sequence number */
    tcp_seq th_ack;     /* acknowledgement number */
    UINT1 u1thx2off;      /* (4:bits unused) and (4:bits offset)*/
    UINT1 th_flags;
    UINT2 th_win;       /* window */
    UINT2 th_sum;       /* checksum */
    UINT2 th_urp;       /* urgent pointer */
};




/******************************************************/ 
            struct vjmbuf { 
                    UINT1  *m_off; /* pointer to start of data */ 
                    UINT4   m_len;  /* length of data */ 
            }; 
 
#define  mtod(m, t)  ((t)(VOID *)(m->m_off)) 
#define  MAX_STATES  16            /* must be >2 and <255 */
#define  MAX_HDR     128           /* max TCP+IP hdr length (by protocol def) */
 
   /* packet types */ 
#define  TYPE_IP                0x21 
#define  TYPE_UNCOMPRESSED_TCP  0x2f 
#define  TYPE_COMPRESSED_TCP    0x2d 
#define  TYPE_ERROR             0x00 
 
#define  NEW_C                  0x40 
#define  NEW_I                  0x20 
#define  TCP_PUSH_BIT           0x10 
 
#define  NEW_S                  0x08 
#define  NEW_A                  0x04 
#define  NEW_W                  0x02 
#define  NEW_U                  0x01 
 
   /* reserved, special-case values of above */ 
   #define SPECIAL_I (NEW_S|NEW_W|NEW_U)        /* echoed interactive traffic */ 
   #define SPECIAL_D (NEW_S|NEW_A|NEW_W|NEW_U)  /* unidirectional data */ 
   #define SPECIALS_MASK (NEW_S|NEW_A|NEW_W|NEW_U) 
 
 
   /* 
    * "state" data for each active tcp conversation on the wire.  This is 
    * basically a copy of the entire IP/TCP header from the last packet together 
    * with a small identifier the transmit & receive ends of the line use to 
    * locate saved header. 
    */ 
   struct cstate { 
        struct cstate *cs_next;  /*next most recently used cstate (xmit only) */ 
        UINT2 cs_hlen;         /* size of hdr (receive only) */ 
        UINT1 cs_id;            /* connection # associated with this state */ 
        UINT1 cs_filler; 
        union { 
             char hdr[MAX_HDR]; 
             struct ip csu_ip;   /* ip/tcp hdr from most recent packet */ 
        } slcs_u; 
   }; 
#define cs_ip slcs_u.csu_ip 
#define cs_hdr slcs_u.csu_hdr 
 
   /* 
    * all the state data for one serial line (we need one of these per line). 
    */ 
   struct slcompress { 
        struct cstate *last_cs;            /* most recently used tstate */ 
        UINT1 last_recv;                  /* last rcvd conn. id */ 
        UINT1 last_xmit;                  /* last sent conn. id */ 
        UINT2 flags; 
        struct cstate tstate[MAX_STATES];  /* xmit connection states */ 
        struct cstate rstate[MAX_STATES];  /* receive connection states */ 
   }; 
 
   /* flag values */ 
#define SLF_TOSS 1       /* tossing rcvd frames because of input err */ 
 
   /* 
    * The following macros are used to encode and decode numbers.  They all 
    * assume that `cp' points to a buffer where the next byte encoded (decoded) 
    * is to be stored (retrieved).  Since the decode routines do arithmetic, 
    * they have to convert from and to network byte order. 
    */ 
 
   /* 
    * ENCODE encodes a number that is known to be non-zero.  ENCODEZ checks for 
    * zero (zero has to be encoded in the long, 3 byte form). 
    */ 
#define ENCODE(n) {\
        if(((UINT2)(n)) >= 256) {\
             *cp++ = 0; \
             cp[1] = (UINT1)(n);\
             cp[0] = (UINT1)((n) >> 8);\
             cp += 2;\
        } else {\
             *cp++ = (UINT1)(n);\
        }\
   } 
#define ENCODEZ(n) { \
        if ((UINT2)(n) >= 256 || (UINT2)(n) == 0) { \
             *cp++ = 0; \
             cp[1] = (UINT1)(n); \
             cp[0] = (UINT1)((n) >> 8); \
             cp += 2; \
        } else { \
             *cp++ = (UINT1)(n); \
        } \
   } 
 
   /* 
    * DECODEL takes the (compressed) change at byte cp and adds it to the 
 
    * current value of packet field 'f' (which must be a 4-byte (long) integer 
    * in network byte order).  DECODES does the same for a 2-byte (short) field. 
    * DECODEU takes the change at cp and stuffs it into the (short) field f. 
    * 'cp' is updated to point to the next field in the compressed header. 
    */ 

#define DECODEL(f) { \
        if (*cp == 0) {\
             (f) = OSIX_HTONL((UINT4)((OSIX_NTOHL(f) + (UINT4)((cp[1] << 8) | cp[2])))); \
             cp += 3; \
        } else { \
             (f) = OSIX_HTONL((OSIX_NTOHL(f) + (UINT4)*cp)); \
             cp++; \
        } \
   }

#define DECODES(f) { \
        if (*cp == 0) {\
             (f) = (UINT2)OSIX_HTONS((OSIX_NTOHS(f) + ((cp[1] << 8) | cp[2]))); \
             cp += 3; \
        } else { \
             (f) = (UINT2)OSIX_HTONS((OSIX_NTOHS(f) + (UINT4)*cp)); \
             cp++; \
        } \
   } 
#define DECODEU(f) { \
        if (*cp == 0) {\
             (f) = (UINT2)OSIX_HTONS(((cp[1] << 8) | cp[2])); \
             cp += 3; \
        } else { \
             (f) = (UINT2)OSIX_HTONS((UINT4)*cp); \
             cp++; \
        } \
   } 
 

#define TH_FIN  0x01
#define TH_SYN  0x02
#define TH_RST  0x04
#define TH_PUSH 0x08
#define TH_ACK  0x10
#define TH_URG  0x20

#ifndef IPPROTO_TCP
#define IPPROTO_TCP 0x06
#endif
