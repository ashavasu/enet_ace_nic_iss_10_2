/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: vacmsec.h,v 1.4 2015/04/28 12:35:03 siva Exp $
 *
 * Description: prototypes and macros for VACM Secuiryt Group 
 * Table Database Model 
 *******************************************************************/

#ifndef _VACMSEC_H
#define _VACMSEC_H

UINT1       au1SecName[SNMP_MAX_SEC_GROUP][SNMP_MAX_OCTETSTRING_SIZE];
UINT1       au1Group[SNMP_MAX_SEC_GROUP][SNMP_MAX_OCTETSTRING_SIZE];
tSecGrp     gaSecGrp[SNMP_MAX_SEC_GROUP];
tTMO_SLL    gSnmpSecGrp;

VOID VACMAddSecGrpSll(tTMO_SLL_NODE *);
VOID SNMPDelVacmSecGrpSll(tTMO_SLL_NODE *);
#endif /* _VACMSEC_H */

