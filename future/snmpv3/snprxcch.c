/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved 
 *
 * $Id: snprxcch.c,v 1.1 2015/04/28 12:35:03 siva Exp $
 *
 * Description: routines for SNMP Proxy Caching 
 *******************************************************************/

#include "snmpcmn.h"
#include "snprxtrn.h"

tSNMP_PROXY_CACHE   snmpCacheTable[SNMP_PROXY_CACHE_ELEMENT_SIZE];
tTMO_HASH_TABLE    *pSnmpProxyCacheTable = NULL;
static UINT2        cache_free_index;

/*********************************************************************
*  Function Name : SnmpInitProxyCache
*  Description   : This function shall be called at init time
*                  to initialize the cache data.
*  Parameter(s)  : None
*  Return Values : SNMP_SUCCESS or SNMP_FAILURE
*********************************************************************/
INT1
SnmpInitProxyCache ()
{
    UINT2               u2counter = 0;

    /* Initialize the hash table */
    pSnmpProxyCacheTable = TMO_HASH_Create_Table (SNMP_PROXY_CACHE_HASH_SIZE,
                                                  NULL, SNMP_ZERO);

    if (NULL == pSnmpProxyCacheTable)
    {
        return SNMP_FAILURE;
    }

    /* Initialize the cache nodes */
    for (u2counter = 0; u2counter < (SNMP_PROXY_CACHE_ELEMENT_SIZE - 1);
         u2counter++)
    {
        snmpCacheTable[u2counter].cacheTableIndex = u2counter;
        snmpCacheTable[u2counter].cacheTableNextIndex = (UINT2) (u2counter + 1);
    }
    snmpCacheTable[u2counter].cacheTableIndex = u2counter;
    snmpCacheTable[u2counter].cacheTableNextIndex =
        SNMP_PROXY_CACHE_ELEMENT_EXHAUSTED;

    /* Set the first free index */
    cache_free_index = 0;

    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : SNMPProxyCacheGetHashIndex
*  Description   : This function is used to get the hash index.
*  Parameter(s)  : u4RequestID - Input for generating key.
*  Return Values : Hash Index
*********************************************************************/

UINT4
SNMPProxyCacheGetHashIndex (UINT4 u4RequestID)
{
    /* adutta */
    UINT4               u4HashIndex = 0;

    u4HashIndex = u4RequestID % SNMP_PROXY_CACHE_HASH_SIZE;

    return u4HashIndex;
}

/*********************************************************************
*  Function Name : SNMPProxyCacheCheckFreeNode
*  Description   : This function shall be called to checks the 
*                  free node. 
*  Parameter(s)  : None.
*  Return Values : SNMP_SUCCESS or SNMP_FAILURE
*********************************************************************/
INT1
SNMPProxyCacheCheckFreeNode (VOID)
{

    /* Failure if there is no free node */
    if (cache_free_index == SNMP_PROXY_CACHE_ELEMENT_EXHAUSTED)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : SNMPProxyCacheGetFreeNode
*  Description   : This function shall be called to get the free node. 
*                  Calling function shall fill the data
*                  in the node returned and then call the 
*                  SNMPProxyCacheAddNode function to add 
*                  the node in the hash table.
*  Parameter(s)  : None.
*  Return Values : Cache node
*********************************************************************/
tSNMP_PROXY_CACHE  *
SNMPProxyCacheGetFreeNode ()
{
    tSNMP_PROXY_CACHE  *pNode = NULL;

    /* Failure if there is no free node */
    if (cache_free_index == SNMP_PROXY_CACHE_ELEMENT_EXHAUSTED)
    {
        return NULL;
    }

    /* Get the free node and set the next free node index */
    pNode = &snmpCacheTable[cache_free_index];
    cache_free_index = pNode->cacheTableNextIndex;

    pNode->ProxyCmnCache.u1_ReqResendFlag = SNMP_FALSE;

    return pNode;
}

/*********************************************************************
*  Function Name : SNMPProxyCacheAddNode
*  Description   : This function shall be called after calling 
*                  SNMPProxyCacheGetFreeNode and the node
*                  with the data should be passed in the function
*  Parameter(s)  : Cache node.
*  Return Values : SNMP_SUCCESS or SNMP_FAILURE
*********************************************************************/
INT1
SNMPProxyCacheAddNode (tSNMP_PROXY_CACHE * pNode)
{
    UINT4               u4HashIndex = SNMP_ZERO;

    /* Get the hash index */
    u4HashIndex =
        SNMPProxyCacheGetHashIndex (pNode->ProxyCmnCache.u4_NewRequestID);

    /* Add the node at the index */
    TMO_HASH_Add_Node (pSnmpProxyCacheTable, &pNode->Link, u4HashIndex, NULL);

    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : SNMPProxyCacheDeleteNode
*  Description   : The calling function shall pass the request id.
*                  This function shall search for node with
*                  the given request id and will delete the node 
*                  from the cache.
*  Parameter(s)  : u4_RequestId - Request ID.
*  Return Values : SNMP_SUCCESS or SNMP_FAILURE
*********************************************************************/
INT1
SNMPProxyCacheDeleteNode (UINT4 u4_RequestId)
{
    UINT4               u4HashIndex = 0;
    tSNMP_PROXY_CACHE  *pNode = NULL;

    /* Get the hash index on the basis of  request id */
    u4HashIndex = SNMPProxyCacheGetHashIndex (u4_RequestId);

    pNode = SNMPProxySearchNode (u4_RequestId);
    if (NULL == pNode)
        return SNMP_FAILURE;

    TMO_HASH_Delete_Node (pSnmpProxyCacheTable, &pNode->Link, u4HashIndex);

    /* Reset the free index */
    SNMPProxyCacheSetFreeNode (pNode);

    /* Free the memory of the node data */
    SNMPProxyCacheFreeVarbind (pNode);

    SNMPTrace ("[PROXY]:Cache Node Deleted \n");

    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : SNMPProxyCacheFreeVarbind
*  Description   : This function shall free the memory allocated 
*                  to the data in the cache node.
*  Parameter(s)  : pNode - Cache node.
*  Return Values : None
*********************************************************************/
VOID
SNMPProxyCacheFreeVarbind (tSNMP_PROXY_CACHE * pNode)
{
    if (pNode->ProxyCmnCache.ProxyV1GbulkCache.pVarBindList)
    {
        FREE_SNMP_VARBIND_LIST (pNode->ProxyCmnCache.ProxyV1GbulkCache.
                                pVarBindList);
        pNode->ProxyCmnCache.ProxyV1GbulkCache.pVarBindList = NULL;
    }
}

/*********************************************************************
*  Function Name : SNMPProxyCacheCopyData
*  Description   : This function copies cache data from one node to
*                  other.
*  Parameter(s)  : pDest - Destination Cache node.
*                  pSrc - Source Cache node.
*  Return Values : SNMP_SUCCESS or SNMP_FAILURE
*********************************************************************/
INT1
SNMPProxyCacheCopyData (tSNMP_PROXY_CACHE * pDest, tSNMP_PROXY_CACHE * pSrc)
{
    INT4                len = 0;

    if ((NULL == pDest) || (NULL == pSrc))
        return SNMP_FAILURE;

    /* Copy the ProxyCmnCache params excluding the timer and get bulk data */
    len = sizeof (tSNMP_PROXY_CMN_CACHE) - (sizeof (tTmrAppTimer) +
                                            sizeof (tSNMP_PROXY_V1GBULK_CACHE));
    MEMCPY ((VOID *) &pDest->ProxyCmnCache, (const VOID *) &pSrc->ProxyCmnCache,
            len);

    /* Copy the OutCommunityStr of get bulk data */
    memcpy ((VOID *) &pDest->ProxyCmnCache.ProxyV1GbulkCache.OutCommunityStr,
            (const VOID *) &pSrc->ProxyCmnCache.ProxyV1GbulkCache.
            OutCommunityStr, sizeof (tSNMP_OCTET_STRING_ARR));
    /* Var bind list shall be copied by the calling function itself */

    /* Copy the union data as per the version */
    if (VERSION3 == pDest->ProxyCmnCache.u4_RecVersion)
    {
        memcpy ((VOID *) &pDest->cacheInfo.ProxyV3SpCache,
                (const VOID *) &pSrc->cacheInfo.ProxyV3SpCache,
                sizeof (tSNMP_PROXY_V3SP_CACHE));
    }
    else
    {
        memcpy ((VOID *) &pDest->cacheInfo.ProxyV1V2SpCache,
                (const VOID *) &pSrc->cacheInfo.ProxyV1V2SpCache,
                sizeof (tSNMP_PROXY_V1V2SP_CACHE));
    }

    /* Cache table index and next free index will not be copied */

    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : SNMPProxyCacheSetFreeNode
*  Description   : This f/n can only be called for the cases when get 
*                  free node is done but add could not be done due
*                  to some failure. The latest allocated node shall be
*                  added back to the free nodes list.
*  Parameter(s)  : pNode - Cache node.
*  Return Values : None
*********************************************************************/
VOID
SNMPProxyCacheSetFreeNode (tSNMP_PROXY_CACHE * pNode)
{

    snmpCacheTable[pNode->cacheTableIndex].cacheTableNextIndex =
        cache_free_index;
    cache_free_index = pNode->cacheTableIndex;

}

/*********************************************************************
*  Function Name : SNMPProxySearchNode
*  Description   : This f/n searches the cache node on the basis of 
*                  request id and returns it.
*  Parameter(s)  : u4_RequestId - Request ID.
*  Return Values : Cache Node in success and NULL in failure.
*********************************************************************/

tSNMP_PROXY_CACHE  *
SNMPProxySearchNode (UINT4 u4_RequestId)
{
    UINT4               u4HashIndex = 0;
    tSNMP_PROXY_CACHE  *pNode = NULL;

    /* Get the hash index on the basis of  request id */
    u4HashIndex = SNMPProxyCacheGetHashIndex (u4_RequestId);

    TMO_HASH_Scan_Bucket (pSnmpProxyCacheTable,
                          u4HashIndex, pNode, tSNMP_PROXY_CACHE *)
    {
        if ((pNode) && (pNode->ProxyCmnCache.u4_NewRequestID == u4_RequestId))
        {
            return pNode;
        }
    }

    return NULL;
}

/*********************************************************************
*  Function Name : SNMPProxyFillCacheData
*  Description   : This f/n is used to populate cache node data in the  
*                  Cache on the of the input parameters.
*  Parameter(s)  : *pPdu - Input pdu
*                  pduVersion - Input pdu version
*                  PduType - Input pdu type
*                  *pOutputPdu - Output pdu after translation
*                  TargetVersion - Version of pOutputPdu
*                  TargetPduType - Pdu type of pOutputPdu
*                  u4_TargetInAddr - Target In address
*                  u4_TargetInPort - Target In port
*                  u4InformRequestId - Request ID for Inform request
*                                      to be used as key
*                  *pSingleTargetOut - Single target out entry
*                  **pNewCacheNode - Cache node is returned in this.
*  Return Values : SNMP_SUCCESS or SNMP_FAILURE
*********************************************************************/

INT1
SNMPProxyFillCacheData (VOID *pPdu,
                        UINT4 pduVersion,
                        INT2 PduType,
                        VOID *pOutputPdu,
                        UINT4 TargetVersion,
                        INT2 TargetPduType,
                        UINT4 u4_TargetInAddr,
                        UINT4 u4_TargetInPort,
                        UINT4 u4InformRequestId,
                        tSNMP_PROXY_CACHE ** pNewCacheNode)
{
    tSNMP_NORMAL_PDU   *pNormalPdu = NULL;
    tSNMP_V3PDU        *pV3Pdu = NULL;
    tSNMP_NORMAL_PDU   *pOutNormalPdu = NULL;
    tSNMP_VAR_BIND     *pSrcVarBind = NULL;
    tSNMP_VAR_BIND     *pDestVarBind = NULL;
    tSNMP_PROXY_V1GBULK_CACHE *pV1GetBulk = NULL;
    tSNMP_PROXY_V3SP_CACHE *pV3Data = NULL;
    tSNMP_PROXY_CACHE  *pCacheNode = NULL;

    if ((NULL == pPdu) || (NULL == pOutputPdu) || (NULL == pNewCacheNode))
        return SNMP_FAILURE;

    /* To remove warning */
    TargetPduType = TargetPduType;
    PduType = PduType;

    pCacheNode = SNMPProxyCacheGetFreeNode ();
    /* Cache exhausted */
    if (NULL == pCacheNode)
    {
        return SNMP_FAILURE;
    }

    if ((VERSION1 == pduVersion) || (VERSION2 == pduVersion))
    {
        pNormalPdu = (tSNMP_NORMAL_PDU *) pPdu;
        pCacheNode->ProxyCmnCache.u4_RecRequestID = pNormalPdu->u4_RequestID;
        pCacheNode->ProxyCmnCache.i2_RecPduType = pNormalPdu->i2_PduType;
        pCacheNode->ProxyCmnCache.u4_RecVersion = pNormalPdu->u4_Version;

        SNMPCopyArrFromOctetStr (&pCacheNode->cacheInfo.ProxyV1V2SpCache.
                                 RecCommunityStr, pNormalPdu->pCommunityStr);
    }
    else if (VERSION3 == pduVersion)
    {
        pV3Pdu = (tSNMP_V3PDU *) pPdu;
        pCacheNode->ProxyCmnCache.u4_RecRequestID =
            pV3Pdu->Normalpdu.u4_RequestID;
        pCacheNode->ProxyCmnCache.i2_RecPduType = pV3Pdu->Normalpdu.i2_PduType;
        pCacheNode->ProxyCmnCache.u4_RecVersion = pV3Pdu->Normalpdu.u4_Version;

        pV3Data = &pCacheNode->cacheInfo.ProxyV3SpCache;
        pV3Data->u4RecMsgMaxSize = pV3Pdu->u4MsgMaxSize;
        pV3Data->u4RecMsgID = pV3Pdu->u4MsgID;
        pV3Data->u4RecMsgSecModel = pV3Pdu->u4MsgSecModel;

        SNMPCopyArrFromOctetStr (&pV3Data->MsgUsrName,
                                 &pV3Pdu->MsgSecParam.MsgUsrName);

        SNMPCopyArrFromOctetStr (&pV3Data->RecMsgFlag, &pV3Pdu->MsgFlag);

        SNMPCopyArrFromOctetStr (&pV3Data->RecContextID, &pV3Pdu->ContextID);

        SNMPCopyArrFromOctetStr (&pV3Data->RecContextName,
                                 &pV3Pdu->ContextName);

    }

    if ((VERSION1 == TargetVersion) || (VERSION2 == TargetVersion))
    {
        pOutNormalPdu = (tSNMP_NORMAL_PDU *) pOutputPdu;

        if (TargetPduType == SNMP_PDU_TYPE_V2_INFORM_REQUEST)
        {
            pCacheNode->ProxyCmnCache.u4_NewRequestID = u4InformRequestId;
        }
        else
        {
            pCacheNode->ProxyCmnCache.u4_NewRequestID =
                pOutNormalPdu->u4_RequestID;
        }

        if ((VERSION1 == TargetVersion) &&
            (SNMP_PDU_TYPE_GET_BULK_REQUEST ==
             pCacheNode->ProxyCmnCache.i2_RecPduType))
        {
            pV1GetBulk = &pCacheNode->ProxyCmnCache.ProxyV1GbulkCache;

            pV1GetBulk->OutCommunityStr.length =
                pOutNormalPdu->pCommunityStr->i4_Length;
            MEMCPY ((VOID *) pV1GetBulk->OutCommunityStr.buffer,
                    (const VOID *) pOutNormalPdu->pCommunityStr->pu1_OctetList,
                    pOutNormalPdu->pCommunityStr->i4_Length);

            pCacheNode->ProxyCmnCache.u1_ReqResendFlag = SNMP_FALSE;

            /* Copy var bind list */
            pSrcVarBind = pOutNormalPdu->pVarBindList;
            while (NULL != pSrcVarBind)
            {
                pDestVarBind = SNMPCopyVarBind (pSrcVarBind);
                if (NULL == pDestVarBind)
                {
                    FREE_SNMP_VARBIND_LIST (pV1GetBulk->pVarBindList);
                    SNMPProxyCacheSetFreeNode (pCacheNode);
                    return SNMP_FAILURE;
                }
                if (pV1GetBulk->pVarBindList == NULL)
                {
                    pV1GetBulk->pVarBindList = pDestVarBind;
                    pV1GetBulk->pVarBindEnd = pDestVarBind;
                }
                else
                {
                    pV1GetBulk->pVarBindEnd->pNextVarBind = pDestVarBind;
                    pV1GetBulk->pVarBindEnd = pDestVarBind;
                }
                pDestVarBind = NULL;
                pSrcVarBind = pSrcVarBind->pNextVarBind;
            }
        }
    }
    else if (VERSION3 == TargetVersion)
    {
        if (TargetPduType == SNMP_PDU_TYPE_V2_INFORM_REQUEST)
        {
            pCacheNode->ProxyCmnCache.u4_NewRequestID = u4InformRequestId;
        }
        else
        {
            pCacheNode->ProxyCmnCache.u4_NewRequestID =
                ((tSNMP_V3PDU *) pOutputPdu)->Normalpdu.u4_RequestID;
        }
    }

    pCacheNode->ProxyCmnCache.u4_TargetInAddr = u4_TargetInAddr;
    pCacheNode->ProxyCmnCache.u4_TargetInPort = u4_TargetInPort;

    if (SNMP_FAILURE == SNMPProxyCacheAddNode (pCacheNode))
    {
        /* Add the node to the free list and free its memory */
        SNMPProxyCacheSetFreeNode (pCacheNode);
        SNMPProxyCacheFreeVarbind (pCacheNode);
        return SNMP_FAILURE;
    }

    *pNewCacheNode = pCacheNode;
    return SNMP_SUCCESS;
}
