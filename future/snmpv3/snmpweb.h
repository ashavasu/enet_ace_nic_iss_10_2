/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: snmpweb.h,v 1.1 2015/04/28 12:35:03 siva Exp $
 *
 * Description: This file contains prototypes for SNMP routines web 
 *              interface and Macro definitions
 *
 *******************************************************************/

#ifndef _SNMPWEB_H
#define _SNMPWEB_H

#define  ISS_WEB_SNMP_PROTO_OID_LEN 9
#define ISS_WEB_SNMP_DES_CBC 2
#define ISS_WEB_SNMP_NONVOLATILE    3
#define ISS_WEB_DEF_RO_SECURITY  "readonly"
#define ISS_WEB_DEF_RW_SECURITY  "none"

extern UINT4  au4StdPrivOid[];
extern UINT4  au4StdAuthOid[];

VOID IssProcessSnmpBasicConfPageSet (tHttp * pHttp);
VOID IssProcessSnmpBasicConfPageGet (tHttp * pHttp);
VOID IssProcessSnmpUserConfPageGet (tHttp * pHttp);
VOID IssProcessSnmpUserConfPageSet (tHttp * pHttp);
VOID IssProcessSnmpGroupConfPageGet (tHttp * pHttp);
VOID IssProcessSnmpGroupConfPageSet (tHttp * pHttp);
VOID IssProcessSnmpAccessConfPageGet (tHttp * pHttp);
VOID IssProcessSnmpAccessConfPageSet (tHttp * pHttp);
VOID IssProcessSnmpViewConfPageGet (tHttp * pHttp);
VOID IssProcessSnmpViewConfPageSet (tHttp * pHttp);
VOID IssProcessSnmpCommunityPageGet (tHttp * pHttp);
VOID IssProcessSnmpCommunityPageSet (tHttp * pHttp);
VOID IssProcessSnmpManagerPageSet (tHttp * pHttp);
VOID IssProcessSnmpManagerPageGet (tHttp * pHttp);
VOID IssPrintAllSnmpUsers(tHttp * pHttp);
VOID IssPrintAllSnmpGroups(tHttp * pHttp);
VOID IssPrintAllSnmpViews(tHttp * pHttp);
VOID IssPrintAllSnmpCommunity (tHttp * pHttp);
VOID IssConvertOctetToDottedDecimal (UINT1 *, UINT4 , UINT1 *);
INT4 IssGetSnmpNotificationName(tSNMP_OCTET_STRING_TYPE *, UINT1 *);
INT1 IssSNMPManagersVerifyNotifyTable (tSNMP_OCTET_STRING_TYPE *);
#endif /* __SNMPWEB_H__ */
