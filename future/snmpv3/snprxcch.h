/************************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved 
 *
 * $Id: snprxcch.h,v 1.2 2015/04/28 12:35:03 siva Exp $
 *
 * Description: Datastructures for SNMP Proxy Caching 
 ************************************************************************/ 
#ifndef _SNPRXCCH_H
#define _SNPRXCCH_H

typedef struct SNMP_OCTET_STRING_ARR {
        INT4       length;
        UINT1       buffer[SNMP_MAX_OCTETSTRING_SIZE];
}tSNMP_OCTET_STRING_ARR;

typedef struct SNMP_PROXY_V1GBULK_CACHE{
        tSNMP_OCTET_STRING_ARR  OutCommunityStr;
        tSNMP_VAR_BIND          *pVarBindList;
        tSNMP_VAR_BIND          *pVarBindEnd;
}tSNMP_PROXY_V1GBULK_CACHE;

typedef struct SNMP_PROXY_CMN_CACHE{
  UINT4   u4_TargetInAddr;
  UINT4   u4_TargetInPort;
        UINT4           u4_RecRequestID;
        UINT4           u4_NewRequestID; 
        UINT4           u4_RecVersion;
        INT2            i2_RecPduType;
        UINT1           u1_ReqResendFlag; 
        UINT1           u1_Padding; 
        tTmrAppTimer    CacheTmr;  
        tSNMP_PROXY_V1GBULK_CACHE       ProxyV1GbulkCache;
}tSNMP_PROXY_CMN_CACHE;


typedef struct SNMP_PROXY_V1V2SP_CACHE{
        tSNMP_OCTET_STRING_ARR  RecCommunityStr;
}tSNMP_PROXY_V1V2SP_CACHE;

typedef struct SNMP_PROXY_V3SP_CACHE{
        UINT4                           u4RecMsgMaxSize;
        UINT4                           u4RecMsgID;
        UINT4                           u4RecMsgSecModel; 
        tSNMP_OCTET_STRING_ARR          MsgUsrName;
        tSNMP_OCTET_STRING_ARR          RecMsgFlag;
        tSNMP_OCTET_STRING_ARR          RecContextID;
        tSNMP_OCTET_STRING_ARR          RecContextName;
}tSNMP_PROXY_V3SP_CACHE;

typedef struct SNMP_PROXY_CACHE{
 tTMO_HASH_NODE   Link;
 tSNMP_PROXY_CMN_CACHE  ProxyCmnCache;
    union{
           tSNMP_PROXY_V1V2SP_CACHE ProxyV1V2SpCache;
              tSNMP_PROXY_V3SP_CACHE       ProxyV3SpCache;     
         }cacheInfo;
 UINT2     cacheTableIndex;
    UINT2                   cacheTableNextIndex;
}tSNMP_PROXY_CACHE;


INT1 SNMPProxyFillCacheData(
   VOID     *pPdu,
   UINT4     pduVersion,
   INT2     PduType,
   VOID     *pOutputPdu, 
   UINT4     TargetVersion, 
   INT2     TargetPduType,
            UINT4       u4_TargetInAddr,
      UINT4       u4_TargetInPort,
            UINT4               u4InformRequestId,
      tSNMP_PROXY_CACHE **pNewCacheNode);
INT1 SNMPProxyCacheCheckFreeNode(VOID);
UINT4 SNMPProxyCacheGetHashIndex(UINT4  RequestID);
tSNMP_PROXY_CACHE*  SNMPProxyCacheGetFreeNode(void);
INT1  SNMPProxyCacheAddNode(tSNMP_PROXY_CACHE *pNode);
INT1 SNMPProxyCacheDeleteNode(UINT4  RequestId);
void SNMPProxyCacheFreeVarbind(tSNMP_PROXY_CACHE *pNode);
tSNMP_PROXY_CACHE* SNMPProxySearchNode(UINT4  RequestId);
INT1 SNMPProxyCacheCopyData(tSNMP_PROXY_CACHE *pDest, tSNMP_PROXY_CACHE *pSrc);
VOID SNMPProxyCacheSetFreeNode(tSNMP_PROXY_CACHE *pNode);

#endif /* _SNPRXCCH_H */
