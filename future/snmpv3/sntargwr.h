/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: sntargwr.h,v 1.5 2015/04/28 12:35:03 siva Exp $
*
* Description: Proto types for snmp target table wrapper  Routines
*********************************************************************/

#ifndef _SNTARGWR_H
#define _SNTARGWR_H

VOID RegisterSNTARG(VOID);

VOID UnRegisterSNTARG(VOID);
INT4 SnmpTargetSpinLockGet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetSpinLockSet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetSpinLockTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpTargetSpinLockDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

INT4 GetNextIndexSnmpTargetAddrTable(tSnmpIndex *, tSnmpIndex *);
INT4 SnmpTargetAddrNameGet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetAddrTDomainGet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetAddrTAddressGet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetAddrTimeoutGet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetAddrRetryCountGet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetAddrTagListGet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetAddrParamsGet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetAddrStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetAddrRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetAddrTDomainSet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetAddrTAddressSet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetAddrTimeoutSet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetAddrRetryCountSet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetAddrTagListSet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetAddrParamsSet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetAddrStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetAddrRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetAddrTDomainTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpTargetAddrTAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpTargetAddrTimeoutTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpTargetAddrRetryCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpTargetAddrTagListTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpTargetAddrParamsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpTargetAddrStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpTargetAddrRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpTargetAddrTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);








INT4 GetNextIndexSnmpTargetParamsTable(tSnmpIndex *, tSnmpIndex *);
INT4 SnmpTargetParamsNameGet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetParamsMPModelGet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetParamsSecurityModelGet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetParamsSecurityNameGet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetParamsSecurityLevelGet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetParamsStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetParamsRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetParamsMPModelSet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetParamsSecurityModelSet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetParamsSecurityNameSet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetParamsSecurityLevelSet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetParamsStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetParamsRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetParamsMPModelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpTargetParamsSecurityModelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpTargetParamsSecurityNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpTargetParamsSecurityLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpTargetParamsStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpTargetParamsRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpTargetParamsTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);






INT4 SnmpUnavailableContextsGet(tSnmpIndex *, tRetVal *);
INT4 SnmpUnknownContextsGet(tSnmpIndex *, tRetVal *);
#endif /* _SNTARGWR_H */
