/*****************************************************************************
 * $Id: snxtrans.c,v 1.4 2015/04/28 12:35:03 siva Exp $
 *
 * Description: This file contains routines for Agentx transport mechanism. 
 ****************************************************************************/
#include "snxinc.h"
#include "fssocket.h"

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxTransInit
 *                                                                          
 *    DESCRIPTION      :  Initialize the trasport mechanism to be used for
 *                        communication with master agent.
 *
 *    INPUT            : None
 * 
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 ****************************************************************************/
INT4
SnxTransInit ()
{
    INT4                i4RetVal = SNMP_FAILURE;

    /* Based on transport mechanism , Do the initialization */
    switch (gAgtxGlobalInfo.u1TDomain)
    {
        case SNX_TDOMAIN_TCP:
            /* Initiate TCP connectin */
            i4RetVal = SnxTransOpenTcpConnection ();
            break;
        default:
            break;
    }
    return i4RetVal;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    :  SnxTransDeInit
 *                                                                          
 *    DESCRIPTION      : Shutdown the connection establised with master agent.
 *
 *    INPUT            : None
 * 
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 ****************************************************************************/
INT4
SnxTransDeInit ()
{
    INT4                i4RetVal = SNMP_SUCCESS;

    /* Based on transport mechanism , Do the de-initialization */
    switch (gAgtxGlobalInfo.u1TDomain)
    {
        case SNX_TDOMAIN_TCP:
            SelRemoveFd (gAgtxGlobalInfo.i4SockId);
            SelRemoveWrFd (gAgtxGlobalInfo.i4SockId);
            close (gAgtxGlobalInfo.i4SockId);
            gAgtxGlobalInfo.i4SockId = -1;
            break;

        default:
            i4RetVal = SNMP_FAILURE;
            break;
    }
    return i4RetVal;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    :  SnxTransOpenTcpConnection
 *                                                                          
 *    DESCRIPTION      :  This function establishes  the TCP connection with 
 *                        the master agent
 *
 *    INPUT            : None
 * 
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 ****************************************************************************/
INT4
SnxTransOpenTcpConnection (VOID)
{
    INT4                i4RetVal = SNMP_FAILURE;

    if (gAgtxGlobalInfo.MasterIpAddr.i4_Length == SNX_IP4_LEN)
    {
        i4RetVal = SnxTransOpenTcpV4Connection ();
    }
    if (gAgtxGlobalInfo.MasterIpAddr.i4_Length == SNX_IP6_LEN)
    {
        i4RetVal = SnxTransOpenTcpV6Connection ();
    }
    return i4RetVal;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    :  SnxTransInitTcpConnection
 *                                                                          
 *    DESCRIPTION      :  This function initiates  the TCP connection with 
 *                        the maste agent
 *
 *    INPUT            : i4SockId - Socket to be connected
 * 
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 ****************************************************************************/
INT4
SnxTransInitTcpConnection (INT4 i4SockId)
{
    INT4                i4RetVal = SNMP_FAILURE;

    if (gAgtxGlobalInfo.MasterIpAddr.i4_Length == SNX_IP4_LEN)
    {
        i4RetVal = SnxTransInitIpv4TcpConnection (i4SockId);
    }
    if (gAgtxGlobalInfo.MasterIpAddr.i4_Length == SNX_IP6_LEN)
    {
        i4RetVal = SnxTransInitIpv6TcpConnection (i4SockId);
    }
    return i4RetVal;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxTransOpenTcpV4Connection
 *                                                                          
 *    DESCRIPTION      : Establishes TCP connection with the IPV4 master agent 
 *
 *    INPUT            : None
 * 
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 ****************************************************************************/
INT4
SnxTransOpenTcpV4Connection ()
{
    INT4                i4SockId;

    i4SockId = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);

    if (i4SockId < 0)
    {
        SNMPTrace ("Opening the socket failed\n");
        return SNMP_FAILURE;
    }

    if (fcntl (i4SockId, F_SETFL, O_NONBLOCK) < 0)
    {
        close (i4SockId);
        return SNMP_FAILURE;
    }
    if (SnxTransInitIpv4TcpConnection (i4SockId) == SNMP_FAILURE)
    {
        if ((errno == EINPROGRESS) || (errno == EALREADY))
        {
            SNMPTrace ("Connection in Progress\n");
            gAgtxGlobalInfo.i4SockId = i4SockId;
            SelAddWrFd (i4SockId, SnxTransTcpOutSocket);
            return SNMP_SUCCESS;
        }
        if (errno == EISCONN)
        {
            gAgtxGlobalInfo.i4SockId = i4SockId;
            SnxTransTcpOutSocket (i4SockId);
            return SNMP_SUCCESS;
        }
        close (i4SockId);
        return SNMP_FAILURE;
    }
    else
    {
        gAgtxGlobalInfo.i4SockId = i4SockId;
        SnxTransTcpOutSocket (i4SockId);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    :  SnxTransInitIpv4TcpConnection
 *                                                                          
 *    DESCRIPTION      :  Initiates the connection
 *
 *    INPUT            : i4SockFd - Socket  
 * 
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 ****************************************************************************/
INT4
SnxTransInitIpv4TcpConnection (INT4 i4SockId)
{
    struct sockaddr_in  Destaddr;
    UINT4               u4MasterIp;

    /* Get the configured Master IP address */
    MEMCPY (&u4MasterIp, gAgtxGlobalInfo.MasterIpAddr.pu1_OctetList, SNMP_FOUR);

    MEMSET (&Destaddr, 0, sizeof (struct sockaddr_in));
    Destaddr.sin_family = AF_INET;
    Destaddr.sin_port = OSIX_HTONS (gAgtxGlobalInfo.u2PortNo);
    Destaddr.sin_addr.s_addr = OSIX_HTONL (u4MasterIp);

    if (connect (i4SockId, (struct sockaddr *) &Destaddr,
                 sizeof (struct sockaddr_in)) < 0)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxTransOpenTcpV6Connection
 *                                                                          
 *    DESCRIPTION      : Establishes TCP connection with the IPV6 master agent 
 *
 *    INPUT            : None
 * 
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 ****************************************************************************/
INT4
SnxTransOpenTcpV6Connection ()
{
    INT4                i4SockId;

    i4SockId = socket (AF_INET6, SOCK_STREAM, IPPROTO_TCP);

    if (i4SockId < 0)
    {
        SNMPTrace ("Opening the socket failed\n");
        return SNMP_FAILURE;
    }

    if (fcntl (i4SockId, F_SETFL, O_NONBLOCK) < 0)
    {
        close (i4SockId);
        return SNMP_FAILURE;
    }
    if (SnxTransInitIpv6TcpConnection (i4SockId) == SNMP_FAILURE)
    {
        if ((errno == EINPROGRESS) || (errno == EALREADY))
        {
            SNMPTrace ("Connection in Progress\n");
            SelAddWrFd (i4SockId, SnxTransTcpOutSocket);
            gAgtxGlobalInfo.i4SockId = i4SockId;
            return SNMP_SUCCESS;
        }
        if (errno == EISCONN)
        {
            SnxTransTcpOutSocket (i4SockId);
            gAgtxGlobalInfo.i4SockId = i4SockId;
            return SNMP_SUCCESS;
        }
        close (i4SockId);
        return SNMP_FAILURE;
    }
    else
    {
        gAgtxGlobalInfo.i4SockId = i4SockId;
        SnxTransTcpOutSocket (i4SockId);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    :  SnxTransInitIpv6TcpConnection
 *                                                                          
 *    DESCRIPTION      :  Initiates the IPV6 tcp connection
 *
 *    INPUT            : i4SockFd - Socket  
 * 
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 ****************************************************************************/
INT4
SnxTransInitIpv6TcpConnection (INT4 i4SockId)
{
    struct sockaddr_in6 ServerAddr;

    MEMSET (&ServerAddr, 0, sizeof (struct sockaddr_in));
    ServerAddr.sin6_family = AF_INET6;
    ServerAddr.sin6_port = OSIX_HTONS (gAgtxGlobalInfo.u2PortNo);

    /* Get the configured Master IP address */
    MEMCPY (&(ServerAddr.sin6_addr.s6_addr),
            gAgtxGlobalInfo.MasterIpAddr.pu1_OctetList, SNX_IP6_LEN);

    if (connect (i4SockId, (struct sockaddr *) &ServerAddr,
                 sizeof (ServerAddr)) < 0)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    :  SnxTransTcpOutSocket
 *                                                                          
 *    DESCRIPTION      :  Indicates the socket is ready for write operation
 *
 *    INPUT            : i4SockFd - Socket  
 * 
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 ****************************************************************************/
VOID
SnxTransTcpOutSocket (INT4 i4SockFd)
{
    if (OsixSendEvent (SELF, (const UINT1 *) SNMP_TASK_NAME,
                       SNX_TRANS_RDY_EVT) != OSIX_SUCCESS)
    {
        SNMPTrace ("SNMP: Agentx Event send Failed\r\n");
    }
    UNUSED_PARAM (i4SockFd);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    :  SnxTransPktCallBack
 *                                                                          
 *    DESCRIPTION      :  Indicates the socket is ready for read operation
 *
 *    INPUT            : i4SockFd - Socket
 * 
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 ****************************************************************************/
VOID
SnxTransPktCallBack (INT4 i4SockFd)
{
    if (OsixSendEvent (SELF, (const UINT1 *) SNMP_TASK_NAME,
                       SNX_AGTX_PKT_EVT) != OSIX_SUCCESS)
    {
        SNMPTrace ("SNMP: Agentx Event send Failed\r\n");
    }
    UNUSED_PARAM (i4SockFd);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxTransRcvPacket
 *                                                                          
 *    DESCRIPTION      :  Receives the agentx packet based on the trasport
 *                        mechanisnm.
 *
 *    INPUT            : None
 * 
 *    OUTPUT           : pu1Packet - Received Packet
 *                       pu4PktLen - Received packet length
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 ****************************************************************************/
INT4
SnxTransRcvPacket (UINT1 *pu1Packet, UINT4 *pu4PktLen)
{
    INT4                i4RetVal = SNMP_FAILURE;

    switch (gAgtxGlobalInfo.u1TDomain)
    {
        case SNX_TDOMAIN_TCP:
            i4RetVal = SnxTransTcpRcvPacket (pu1Packet, pu4PktLen);
            if (i4RetVal == SNMP_FAILURE)
            {
                /* Session is closed by the peer, so reset the subagent
                 * control  */
                gAgtxGlobalInfo.i4SessionId = 0;
                TmrStop (gAgtxGlobalInfo.SnxTmrListId, &gAgtxGlobalInfo.SnxTmr);
                SnxTransDeInit ();
                MEMSET (&gAgtxStats, 0, sizeof (tSnmpAgentxStats));
                return SNMP_FAILURE;
            }
            /* Wait on the descriptor for packet reception */
            SelAddFd (gAgtxGlobalInfo.i4SockId, SnxTransPktCallBack);
            break;
        default:
            break;
    }
    return i4RetVal;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxTransTcpRcvPacket
 *                                                                          
 *    DESCRIPTION      :  Receives the packet received on the TCP socket
 *
 *    INPUT            : None
 * 
 *    OUTPUT           : pu1Packet - Received Packet
 *                       pu4PktLen - Received packet length
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 ****************************************************************************/
INT4
SnxTransTcpRcvPacket (UINT1 *pu1Packet, UINT4 *pu4PktLen)
{
    INT4                i4Msglen = SNX_MAX_MTU;

    i4Msglen = recv (gAgtxGlobalInfo.i4SockId, pu1Packet, i4Msglen, 0);

    if (i4Msglen <= 0)
    {
        return SNMP_FAILURE;
    }
    *pu4PktLen = (UINT4) i4Msglen;
    return SNMP_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxTransTxPkt 
 *                                                                          
 *    DESCRIPTION      :  Transmits the SNMP Agentx packet based on the
 *                        transport mechanism
 *
 *    INPUT            : pu1Packet - Packet to be transmitted
 *                       u4Len     - Length of the packet
 * 
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 ****************************************************************************/
INT4
SnxTransTxPkt (UINT1 *pu1Packet, UINT4 u4Len)
{
    INT4                i4RetVal = SNMP_FAILURE;

    switch (gAgtxGlobalInfo.u1TDomain)
    {
        case SNX_TDOMAIN_TCP:
            i4RetVal = SnxTransTcpTxPacket (pu1Packet, u4Len);
            break;
        default:
            break;
    }
    return i4RetVal;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxMainAgentxEnable
 *                                                                          
 *    DESCRIPTION      :  Transmits the SNMP Agentx packet over the
 *                        TCP connection
 *
 *    INPUT            : pu1Packet - Packet to be transmitted
 *                       u4Len     - Length of the packet
 * 
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 ****************************************************************************/
INT4
SnxTransTcpTxPacket (UINT1 *pu1Packet, UINT4 u4Len)
{
    INT4                i4RetVal = SNMP_FAILURE;

    i4RetVal =
        send (gAgtxGlobalInfo.i4SockId, pu1Packet, (INT4) u4Len, MSG_NOSIGNAL);

    if (i4RetVal < 0)
    {
        SelAddWrFd (gAgtxGlobalInfo.i4SockId, SnxTransTcpOutSocket);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxTransByteOrderFlag
 *                                                                          
 *    DESCRIPTION      :  Provides the byte-ordering to be used in the 
 *                        transport mechanism.
 *
 *    INPUT            : None
 * 
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : u1Flag - Byte Order flag information 
 ****************************************************************************/
UINT1
SnxTransByteOrderFlag ()
{
    UINT1               u1Flag = SNX_NETWORK_BYTE_ORDER;

    switch (gAgtxGlobalInfo.u1TDomain)
    {
        case SNX_TDOMAIN_TCP:
            break;
        default:
            break;
    }
    return u1Flag;
}

/**************************  End of snxtrans.c *******************************/
