
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: snmpcmn.h,v 1.44.2.1 2018/05/21 13:15:33 siva Exp $
 *
 *
 * Description: common macros and prototypes for snmp agent
 *******************************************************************/
#ifndef _SNMPCMN_H
#define _SNMPCMN_H

#include "lr.h"
#include "cfa.h"
#include "fssnmp.h"
#include "snmcport.h"
#include "ip.h" /* ip.h is included for trap */
#include "ipv6.h"
#include "cust.h"
#include "snprxcch.h"
#include "rmgr.h"
#include "msr.h"
#include "utilipvx.h"
#include "snmptcp.h"
#include "snmputil.h"
#include "snmpred.h"
#include "dns.h"
#include "issu.h"


#ifdef ISS_WANTED
#include "iss.h"
#endif

#ifdef MSR_WANTED
#include "msr.h"
#endif

#define SNMP_HEXA_DECIMAL   16
#define SNMP_HEXA_STRLEN    2

#define SNMP_NOT_OK -1
#define SNMP_OK      0
#define SNMP_INVALID 0 
#define SNMP_LESSER     1
#define SNMP_GREATER     2 
#define SNMP_EQUAL  0 
#define SNMP_NOT_EQUAL  3 
#define SNMP_RB_CMP_LESSER -1
#define SNMP_RB_CMP_GREATER 1
#define SNMP_INIT_COMPLETE(u4Status)    lrInitComplete(u4Status)

#define SNMP_FALSE                1
#define SNMP_TRUE                 2

/* sizing parameters */
#define MAX_PKT_LENGTH            1472 
#define MAX_OID_LENGTH            SNMP_MAX_OID_LENGTH 
#define MAX_COMM_NAME_LENGTH      256
#define MAX_SYSOR_TABLE_ENTRY     256
#define MAX_TRAP_TABLE_ENTRY      10
#define MAX_COMMUNITY_TABLE_ENTRY (10 + MAX_SNMP_CONTEXT)
#define MAX_VARBIND               50    
/* Sizing Parameters for Proxy Table */
#define MAX_PROXY_TABLE_ENTRY      48
#define MAX_PROX_TGT_PARAM_ENTRY  48
#define MAX_PROXY_PDU_ENTRY          MAX_PROXY_TABLE_ENTRY
#define PROXY_TYPE_READ              1
#define PROXY_TYPE_WRITE          2
#define PROXY_TYPE_TRAP              3
#define PROXY_TYPE_INFORM          4
#define MIN_PROXY_ENGINE_ID_LEN   5
#define MAX_PROXY_ENGINE_ID_LEN   32


#define MAX_COMM_SEC_NAME_LEN     32

#define SNMP_MAX_CONTEXT_NAME     32

#define SNMP_MAX_TGT_PARAM_LEN    32

/* SNMP V3 Related Sizing Params */
#define SNMP_MAX_USM_ENTRY        (50 + MAX_SNMP_CONTEXT)
#define SNMP_MAX_ACCESS_ENTRY     (50 + MAX_SNMP_CONTEXT)
#define SNMP_MAX_SEC_GROUP        50
#define SNMP_MAX_VIEW_TREE        50
#define SNMP_MAX_FILTER_TREE      20

#if ((defined MI_WANTED) || (defined VRF_WANTED))
#define MAX_SNMP_CONTEXT          SYS_DEF_MAX_NUM_CONTEXTS 
#else
#define MAX_SNMP_CONTEXT          1
#endif
#define MAX_TARGET_ADDR_ENTRY     10
#define SNMP_MAX_TGT_PARAM_ENTRY  10
#define MAX_STR_OCTET_VAL         256 

/* Task Related macros */
#define SNMP_TASK_NAME "SAT"
#define SNMP_TASK_PRIORITY 190
#define SNMP_STACK_SIZE OSIX_DEFAULT_STACK_SIZE 

#define SNMP_SYS_OID_LEN           6
#define SNMP_OID_LEN               7
    
#define SNMP_ACTIVE    1
#define CREATE_REQUEST 2
#define UNDER_CREATION 3
#define SNMP_INACTIVE  4

#define MGR_V1                     0
#define MGR_V2                     1
#define MGR_V1_AND_V2              2
#define SNMP_MANAGER_PORT          162
#define SNMP_MIN_PORT_NUM          1
#define SNMP_MAX_PORT_NUM          65535

#define SNMP_TRAP_TYPE             1
#define SNMP_INFORM_TYPE           2


extern UINT4  gu4SnmpTrcEvent; /* Global variable to enable/disable 
                                * the debug for SNMP module */
/* Trace Options */
#define SNMP_INFO_TRC               INIT_SHUT_TRC
#define SNMP_DEBUG_TRC              MGMT_TRC
#define SNMP_FAILURE_TRC            ALL_FAILURE_TRC
#define SNMP_CRITICAL_TRC           BUFFER_TRC
#define SNMP_ALL_TRC                0xffffffff

#define  SNMP_GLB_TRC() gu4SnmpTrcEvent

#ifdef TRACE_WANTED

#define SNMP_MODULE_NAME        ((const char *)"[SNMP]")

#define   SNMP_TRC(Flag, Fmt) \
          UtlTrcLog(SNMP_GLB_TRC(), Flag, SNMP_MODULE_NAME, Fmt)

#define   SNMP_TRC1(Flag, Fmt, Arg) \
          UtlTrcLog(SNMP_GLB_TRC(), Flag, SNMP_MODULE_NAME, Fmt, Arg)

#define   SNMP_TRC2(Flag, Fmt, Arg1, Arg2) \
          UtlTrcLog(SNMP_GLB_TRC(), Flag, SNMP_MODULE_NAME, Fmt, Arg1, Arg2)

#define   SNMP_TRC3(Flag, Fmt, Arg1, Arg2, Arg3) \
          UtlTrcLog(SNMP_GLB_TRC(), Flag, SNMP_MODULE_NAME, Fmt, Arg1, Arg2, Arg3)
#else

#define   SNMP_TRC(Flag, Fmt)

#define   SNMP_TRC1(Flag, Fmt, Arg) UNUSED_PARAM(Arg)

#define   SNMP_TRC2(Flag, Fmt, Arg1, Arg2) \
{\
    UNUSED_PARAM(Arg1); \
    UNUSED_PARAM(Arg2); \
}

#define   SNMP_TRC3(Flag, Fmt, Arg1, Arg2, Arg3) \
{\
    UNUSED_PARAM(Arg1); \
    UNUSED_PARAM(Arg2); \
    UNUSED_PARAM(Arg3); \
}
#endif /*TRACE_WANTED*/



#ifdef TRACE_WANTED
#define SNMPTrace(x) MOD_TRC(0,0,"",x) 
#define SNMPARGTrace(x,y) MOD_TRC_ARG1(0,0,"",x,y) 
#define SNMPARG2Trace(x,y,z) MOD_TRC_ARG2(0,0,"",x,y,z)
#else
#define SNMPTrace(x)\
{ \
  UNUSED_PARAM (x); \
}
#define SNMPARGTrace(x,y)\
{ \
  UNUSED_PARAM (y); \
}
#define SNMPARG2Trace(x,y,z) \
{ \
  UNUSED_PARAM (y); \
  UNUSED_PARAM (z); \
}
#endif 

#define ENTERPRISE_SPECIFIC 6

#define VERSION1 0
#define VERSION2 1
#define VERSION3 3

#define COLD_START          0
#define WARM_START          1
#define LINK_DOWN           2
#define LINK_UP             3
#define AUTH_FAILURE        4
#define EGP_NEIGHBOR_LOSS   5

#define AUTHTRAP_ENABLE  1
#define AUTHTRAP_DISABLE 2
#define COLDTRAP_ENABLE     1
#define COLDTRAP_DISABLE    2

#define SNMP_V3_PDU          1
#define SNMP_V1V2_PDU        2
#define SNMP_V1V2V3_PDU      3
#define SNMP_MIN_SEC_NONE    1
#define SNMP_MIN_SEC_AUTH    2
#define SNMP_MIN_SEC_PRIV    3

#define SNMP_PDU_TYPE_GET_REQUEST            0xA0
#define SNMP_PDU_TYPE_GET_NEXT_REQUEST       0xA1
#define SNMP_PDU_TYPE_GET_RESPONSE           0xA2
#define SNMP_PDU_TYPE_SET_REQUEST            0xA3
#define SNMP_PDU_TYPE_TRAP                   0xA4
#define SNMP_PDU_TYPE_GET_BULK_REQUEST       0xA5
#define SNMP_PDU_TYPE_V2_INFORM_REQUEST      0xA6
#define SNMP_PDU_TYPE_SNMPV2_TRAP            0xA7
#define SNMP_PDU_TYPE_GET_REPORT             0xA8
#define SNMP_PDU_TYPE_V2_INFORM_RESPONSE     0xA2

#define SNMP_ERR_NO_ERROR                       0
#define SNMP_ERR_TOO_BIG                        1
#define SNMP_ERR_NO_SUCH_NAME                   2
#define SNMP_ERR_BAD_VALUE                      3
#define SNMP_ERR_READ_ONLY                      4
#define SNMP_ERR_GEN_ERR                        5

 /* SNMPv2 errors */
#define SNMP_ERR_NO_ACCESS                      6
#define SNMP_ERR_WRONG_TYPE                     7
#define SNMP_ERR_WRONG_LENGTH                   8
#define SNMP_ERR_WRONG_ENCODING                 9
#define SNMP_ERR_WRONG_VALUE                   10
#define SNMP_ERR_NO_CREATION                   11
#define SNMP_ERR_INCONSISTENT_VALUE            12
#define SNMP_ERR_RESOURCE_UNAVAILABLE          13
#define SNMP_ERR_COMMIT_FAILED                 14
#define SNMP_ERR_UNDO_FAILED                   15
#define SNMP_ERR_AUTHORIZATION_ERROR           16
#define SNMP_ERR_NOT_WRITABLE                  17
#define SNMP_ERR_INCONSISTENT_NAME             18

/* This a proprietary error code return by ISS SNMP Agent for the
 * SNMPSet request, when HA bulk sync and MBSM operation are in progress */
#define SNMP_ERR_PROP_BULK_INPROGRESS          30

/* VACM  Errors This Error Values */
#define SNMP_ERR_NO_CONTEXT                    0xf1
#define SNMP_ERR_NO_GROUP_NAME                 0xf2
#define SNMP_ERR_NO_ACCESS_ENTRY               0xf3 
#define SNMP_NO_SUCH_VIEW                      0xf4
#define SNMP_NOT_IN_VIEW                       0xf5 

#define SNMP_ERROR_WRTIE_ACCESS_DENIED         0xf6
#define SNMP_ALLOC_ERR                         0xf8
#define SNMP_TEST_PORT_VALUE                   0xffff 

#define SNMP_ENCRYPT_BYTE_ALIGN 8
#define SNMP_AUTH_DIGEST_LEN    12
#define SNMP_AUTH_DIGEST_LEN_SHA256    24
#define SNMP_AUTH_DIGEST_LEN_SHA384    32
#define SNMP_AUTH_DIGEST_LEN_SHA512    48

#define SNMPDROP                     0xf0
#define SNMP_AGENTDISCOVERY          0xf1
#define SNMP_NOT_TIMEWINDOW          0xf2  

/* Added For UNKNOWNUSERNAME AND WRONGDIGESTS */
#define SNMP_UNKNOWNUSERNAME         0xf3
#define SNMP_WRONGDIGESTS            0xf4  
#define SNMP_UNKNOWNENGINEID         0xf5
#define SNMP_UNSUPPORTEDSECLEVEL    0xf6
#define SNMP_UNKNOWN_CONTEXT_ENGINEID  0xf7
#define SNMP_UNKNOWN_CONTEXT_NAME  0xf8
#define SNMP_DECRYPTIONERRORS     0xf9

#define SNMP_VIEW_ALONE_MATCHED 0xfa
#define SNMP_PDU_GET_RESPONSE_MASK  0x0000ffff 

#define SNMPPduFree(x) SNMPMemInit()

#define SNMP_NO_AUTH 1
#define SNMP_MD5     2
#define SNMP_SHA     3
#define SNMP_SHA224     4
#define SNMP_SHA256     5
#define SNMP_SHA384     6
#define SNMP_SHA512     7

#define SNMP_NO_PRIV    1
#define SNMP_DES_CBC    2
#define SNMP_TDES_CBC   3
#define SNMP_AESCFB128  4 
#define SNMP_AESCFB192  5
#define SNMP_AESCFB256  6
#define SNMP_AESCTR     7
#define SNMP_AESCTR192  8
#define SNMP_AESCTR256  9

#define SNMP_AUTH_MD5       "1.3.6.1.6.3.10.1.1.2"
#define SNMP_AUTH_SHA       "1.3.6.1.6.3.10.1.1.3"
#define SNMP_AUTH_SHA256    "1.3.6.1.6.3.10.1.1.5"
#define SNMP_AUTH_SHA384    "1.3.6.1.6.3.10.1.1.6"
#define SNMP_AUTH_SHA512    "1.3.6.1.6.3.10.1.1.7"

#define SNMP_PRIV_DES       "1.3.6.1.6.3.10.1.2.2"
#define SNMP_PRIV_TDES      "1.3.6.1.6.3.10.1.2.3"    
#define SNMP_PRIV_AESCFB128 "1.3.6.1.6.3.10.1.2.4"
#define SNMP_PRIV_AESCFB192 "1.3.6.1.6.3.10.1.2.5"
#define SNMP_PRIV_AESCFB256 "1.3.6.1.6.3.10.1.2.6"
#define SNMP_PRIV_AESCTR    "1.3.6.1.6.3.10.1.2.7"
#define SNMP_PRIV_AESCTR192 "1.3.6.1.6.3.10.1.2.8"
#define SNMP_PRIV_AESCTR256 "1.3.6.1.6.3.10.1.2.9"
/* SNMP Storage Types */
#define SNMP_VOLATILE    2

#define SNMP3_STORAGE_TYPE_OTHER          1
#define SNMP3_STORAGE_TYPE_VOLATILE       2
#define SNMP3_STORAGE_TYPE_NONVOLATILE    3
#define SNMP3_STORAGE_TYPE_PERMANAENT     4
#define SNMP3_STORAGE_TYPE_READONLY       5

#define SNMP3_NOTIFY_TYPE_TRAP       1
#define SNMP3_NOTIFY_TYPE_INFORM     2

/* SNMP USM Model ID */
#define SNMP_USM            SNMP_AUTH_PRIV 

/* SNMP Row Status */
#define SNMP_ROWSTATUS_ACTIVE        1
#define SNMP_ROWSTATUS_NOTINSERVICE  2
#define SNMP_ROWSTATUS_NOTREADY      3
#define SNMP_ROWSTATUS_CREATEANDGO   4
#define SNMP_ROWSTATUS_CREATEANDWAIT 5
#define SNMP_ROWSTATUS_DESTROY       6

/* This macro is based on the v3 header possible 
 * maximum length */
#define SNMP_V3_HEADER_LENGTH 330

#define SNMP_MIN_ENGINE_ID_LEN 5
#define SNMP_MAX_ENGINE_ID_LEN 32
#define SNMP_DEFAULT_ENGINEID_LEN 7

#define SNMP_MAX_ADMIN_STR_LENGTH 40

#define SNMP_VIEW_TREE_MASK_MAX_LENGTH 16
#define SNMP_VIEW_MASK_MAX_STRING_LENGTH 32

#define SNMP_FILTER_MASK_MAX_STRING_LENGTH 255

#define SNMP_SECMODEL_V1   1
#define SNMP_SECMODEL_V2C  2
#define SNMP_SECMODEL_V3   3

#define SNMP3_MAX_ARGS 11

#define SNMP3_OVER_TCP_ENABLE 1
#define SNMP3_OVER_TCP_DISABLE 2
#define SNMP3_TRAP_OVER_TCP_ENABLE 1
#define SNMP3_TRAP_OVER_TCP_DISABLE 2

#define SNMP_STD_OCTETSTRING_SIZE 33
#define SNMP_PASSWORD_SIZE        41

#define SNMP_VIEW_MASK_INCLDUE  1
#define SNMP_VIEW_MASK_EXCLUDE  2


#define SNMP_FILTER_MASK_INCLDUE  1
#define SNMP_FILTER_MASK_EXCLUDE  2

#define SNMP_MIN_USM_KEY_LENGTH      8
#define SNMP_MAX_USM_KEY_SHA_LENGTH 40

#define SNMP_IPADDR_LEN   4

#define SNMP_NO_LOGGING_ERROR 1
#define SNMP_LOGGING_ERROR 0

#define SNMP_MSG_BUF_LEN 100
#define SNMP_SECURITY_LEVEL_AUTH    1

#define SNMP_NOAUTH_NOPRIV 1
#define SNMP_AUTH_NOPRIV   2
#define SNMP_AUTH_PRIV     3

#define SNMP_CLI_DUMMY_HANDLE_VALUE 0xffff

#define SNMP_AUTH_TYPE_MD5  2
#define SNMP_AUTH_TYPE_SHA  3
#define SNMP_AUTH_TYPE_SHA_224  4
#define SNMP_AUTH_TYPE_SHA_256  5
#define SNMP_AUTH_TYPE_SHA_384  6
#define SNMP_AUTH_TYPE_SHA_512  7

#define SNMP_MPMODEL_V1    0
#define SNMP_MPMODEL_V2C   1
#define SNMP_MPMODEL_V3    3

#define SNMP_IP6ADDR_LEN  16 

#define SNMP_NOTIFY_FILTER_TYPE_INCLUDED    1
#define SNMP_NOTIFY_FILTER_TYPE_EXCLUDED    2

#define SNMP_WRITE_COMMUNITY_NAME "NETMAN"
#define SNMP_READ_COMMUNITY_NAME  "PUBLIC"

#define SNMP_UNSIGNED    0
#define SNMP_SIGNED      1

#define SNMP_AUTH_BIT           0x01
#define SNMP_ENCY_BIT           0x02
#define SNMP_RESET_REPEAT_BIT   0xfb
#define SNMP_SEC_BITS           0x03

#define AES_128_KEY_LENGTH 128
#define SNMP_AES_128_KEY_LENGTH 128
#define SNMP_AES_192_KEY_LENGTH 192
#define SNMP_AES_256_KEY_LENGTH 256
#define SNMP_AES_IV_LEN    16
#define SNMP_SALT_LEN      8
#define SNMP_TEMP_BUFF_LEN 256
#define SNMP_AES_OUT_BUFF_LEN 1024

/* Union of V4 and V6 Address */ 
typedef struct 
{ 
   union 
   { 
       UINT4 u4Ip4Addr; 
       UINT1 au1Ip6Addr[SNMP_IP6ADDR_LEN]; 
   } uIpAddr; 

   UINT4             u4AddrType;  /* Address family   */
#define Ip4MgrAddr   uIpAddr.u4Ip4Addr 
#define Ip6MgrAddr   uIpAddr.au1Ip6Addr 
}tMgrIpAddr; 


typedef struct SNMP_NORMAL_PDU{
        UINT4                   u4_Version;
        UINT4                   u4_RequestID;
        INT4                    i4_ErrorStatus;
        INT4                    i4_ErrorIndex;
        INT4                    i4_NonRepeaters;
        INT4                    i4_MaxRepetitions;
        tSNMP_OCTET_STRING_TYPE *pCommunityStr;
        tSNMP_VAR_BIND          *pVarBindList;
        tSNMP_VAR_BIND          *pVarBindEnd;
        INT2                    i2_PduType;
        INT2                    i2Padding;
}tSNMP_NORMAL_PDU;

typedef struct SNMP_TRAP_PDU{
        UINT4                   u4_Version;
        UINT4                   u4_TimeStamp;
        INT4                    i4_GenericTrap;
        INT4                    i4_SpecificTrap;
        tSNMP_OCTET_STRING_TYPE *pCommunityStr;
        tSNMP_OID_TYPE          *pEnterprise;
        tSNMP_OCTET_STRING_TYPE *pAgentAddr;
        tSNMP_VAR_BIND          *pVarBindList;
        tSNMP_VAR_BIND          *pVarBindEnd;
        INT2                    i2_PduType;
        INT2                    i2Padding;
}tSNMP_TRAP_PDU;

typedef struct {
    UINT4                   au4SysObjectId[MAX_OID_LENGTH];
    UINT4                   u4OidLen;
    UINT4                   u4SysUpTime;
    UINT4                   u4SnmpBootCount;
    UINT4                   u4SysServices;
    UINT1                   au1SysDescr[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1                   au1SysContact[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1                   au1SysName[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1                   au1SysLocation[SNMP_MAX_OCTETSTRING_SIZE];
    UINT2                   u2ListenTrapPort;
    UINT2                   u2ProxyListenTrapPort;
    UINT2                   u2ListenAgentPort;
    UINT1                   au1Pad[2];
}tSNMP_SYSTEM;


typedef struct {
    UINT4   au4SysOrId[MAX_OID_LENGTH];
    UINT4   u4OidLen;
    UINT4   u4SysOrUpTime;
    INT4    i4Status;
    UINT1   au1SysOrDescr[SNMP_MAX_OCTETSTRING_SIZE];
} tSNMP_SYSORTABLE;

typedef struct  {
    UINT4 u4SnmpInPkts;
    UINT4 u4SnmpOutPkts;
    UINT4 u4SnmpInBadVersions;
    UINT4 u4SnmpInBadCommunityNames;
    UINT4 u4SnmpInBadCommunityUses;
    UINT4 u4SnmpInASNParseErrs;
    UINT4 u4SnmpInBadTypes;
    UINT4 u4SnmpInTooBigs;
    UINT4 u4SnmpInNoSuchNames;
    UINT4 u4SnmpInBadValues;
    UINT4 u4SnmpInReadOnlys;
    UINT4 u4SnmpInGenErrs;
    UINT4 u4SnmpInTotalReqVars;
    UINT4 u4SnmpInTotalSetVars;
    UINT4 u4SnmpInGetRequests;
    UINT4 u4SnmpInGetNexts;
    UINT4 u4SnmpInSetRequests;
    UINT4 u4SnmpInGetResponses;
    UINT4 u4SnmpInTraps;
    UINT4 u4SnmpInInformResponses; 
    UINT4 u4SnmpOutTooBigs;
    UINT4 u4SnmpOutNoSuchNames;
    UINT4 u4SnmpOutBadValues;
    UINT4 u4SnmpOutReadOnlys;
    UINT4 u4SnmpOutGenErrs;
    UINT4 u4SnmpOutGetRequests;
    UINT4 u4SnmpOutGetNexts;
    UINT4 u4SnmpOutSetRequests;
    UINT4 u4SnmpOutGetResponses;
    UINT4 u4SnmpOutTraps;
    UINT4 u4SnmpOutInformRequests; 
    INT4  i4SnmpEnableAuthTraps;
    INT4  i4SnmpColdStartTraps;
    UINT4 u4SnmpSilentDrops;
    UINT4 u4SnmpProxyDrops;
    UINT4 u4SnmpInformDrops;    
    UINT4 u4SnmpInformTimeOut;  
    UINT4 u4SnmpInformAwaitAck; 
    UINT4 u4SnmpUnknownContexts;
    UINT4 u4SnmpUnavailableContexts;
    UINT4 u4SnmpUnkownSecModel;
    UINT4 u4SnmpInvalidMsg;
    UINT4 u4SnmpUnknownPduHandler;
    UINT4 u4SnmpRollbackErrs;
} tSNMP_STAT;

/* Inform Statistics Structure */
typedef struct {
    UINT4   u4InformSent;
    UINT4   u4InformAwAck;
    UINT4   u4InformDrop;
    UINT4   u4InformFail;
    UINT4   u4InformRetried;
    UINT4   u4InformResp; 
} tSNMP_INFORM_STAT;


/* SNMPV3 */
typedef struct
{
 tSNMP_OCTET_STRING_TYPE MsgEngineID;
 UINT4 u4MsgEngineBoot;
 UINT4 u4MsgEngineTime;
 tSNMP_OCTET_STRING_TYPE MsgUsrName;
 tSNMP_OCTET_STRING_TYPE MsgAuthParam;
 tSNMP_OCTET_STRING_TYPE MsgPrivParam;
}tSNMP_SEC_PARAMS;

typedef struct
{
 UINT4 u4MsgID;
 UINT4 u4MsgMaxSize;
 UINT4 u4MsgSecModel; 
 tSNMP_OCTET_STRING_TYPE MsgFlag;
 tSNMP_SEC_PARAMS MsgSecParam;
 tSNMP_OCTET_STRING_TYPE ContextID;
 tSNMP_OCTET_STRING_TYPE ContextName;
 tSNMP_NORMAL_PDU Normalpdu;
}tSNMP_V3PDU;

/* USM */
typedef struct {
    tTMO_SLL_NODE Link;
    tSNMP_OCTET_STRING_TYPE UsmUserEngineID;
    tSNMP_OCTET_STRING_TYPE UsmUserName;
    tSNMP_OCTET_STRING_TYPE UsmUserSecName;
    UINT4                   u4UsmUserCloneFrom;
    UINT4                   u4UsmUserAuthProtocol;
    tSNMP_OCTET_STRING_TYPE UsmUserAuthKeyChange;
    tSNMP_OCTET_STRING_TYPE UsmUserOwnAuthKeyChange;
    UINT4                   u4UsmUserPrivProtocol;
    tSNMP_OCTET_STRING_TYPE UsmUserPrivKeyChange;
    tSNMP_OCTET_STRING_TYPE UsmUserOwnPrivKeyChange;
    tSNMP_OCTET_STRING_TYPE UsmUserPublic;
    tSNMP_OCTET_STRING_TYPE UsmUserAuthPassword;
    tSNMP_OCTET_STRING_TYPE UsmUserPrivPassword;
    UINT4  u4UsmUserStorageType;
    UINT4  u4UsmUserStatus;
}tSnmpUsmEntry;

typedef struct
{
  UINT4 u4UsmStatsUnsupportedSecLevels;
  UINT4 u4UsmStatsNotInTimeWindows;
  UINT4 u4UsmStatsUnknownUserNames;
  UINT4 u4UsmStatsUnknownEngineIDs;
  UINT4 u4UsmStatsWrongDigests;
  UINT4 u4UsmStatsDecryptionErrors;
  INT4  i4UsmUserSpinLock;
}tSnmpUsmStats;

/* VACM  */
typedef struct
{
 tSNMP_OCTET_STRING_TYPE *pUserName;
 tSNMP_OCTET_STRING_TYPE *pContextName;
 UINT4                   u4SecModel;
 UINT4                   u4SecLevel;
}tSnmpVacmInfo;

typedef struct ViewTree
{
  tTMO_SLL_NODE            link;
  tSNMP_OCTET_STRING_TYPE *pName; 
   /* Points to the corresponding entry at Access Table */
  tSNMP_OID_TYPE      SubTree;
  tSNMP_OCTET_STRING_TYPE Mask;
  INT4      i4Type;
  INT4      i4Storage;
  INT4      i4Status;
}tViewTree;

typedef struct AccessEntry
{
    tTMO_SLL_NODE   link;
    tSNMP_OCTET_STRING_TYPE *pGroup;   
    /* Points to the corresponding entry at SecGrp */
    tSNMP_OCTET_STRING_TYPE    Context;
    INT4    i4SecModel;
    INT4    i4SecLevel;
    INT4    i4ContextMatch;
    tSNMP_OCTET_STRING_TYPE ReadName;
    tSNMP_OCTET_STRING_TYPE    WriteName;
    tSNMP_OCTET_STRING_TYPE    NotifyName;
    INT4    i4Storage;
    INT4    i4Status;
}tAccessEntry;

typedef struct
{
    tTMO_SLL_NODE           link;
    INT4                      i4SecModel;
    tSNMP_OCTET_STRING_TYPE SecName;
    tSNMP_OCTET_STRING_TYPE    Group;
    INT4                      i4Storage;
    INT4                      i4Status;
}tSecGrp;

typedef struct
{
 tTMO_SLL_NODE           link;
 tSNMP_OCTET_STRING_TYPE ContextName;
 UINT4        u4VcNum;            /* virtual context number */
}tContext;

/* Community Mapping */
/* V3 Trap */
typedef struct
{
 tTMO_SLL_NODE            Link;
 tSNMP_OCTET_STRING_TYPE  NotifyName;
 tSNMP_OCTET_STRING_TYPE  NotifyTag;
 INT4                     i4NotifyType;
 INT4                     i4NotifyStorageType;
 INT4                     i4Status;
}tSnmpNotifyEntry;

typedef struct 
{
    tSNMP_OCTET_STRING_TYPE TargetParmasName;
    tSNMP_OCTET_STRING_TYPE ProfileName;
    INT4      i4Storage;
    INT4      i4Status;
} tSnmpNotifyFilterProfileTable;

typedef struct 
{
    tTMO_SLL_NODE            link;
    tSNMP_OCTET_STRING_TYPE *pFilterProfileName;
    tSNMP_OID_TYPE      FilterSubTree;
    tSNMP_OCTET_STRING_TYPE FilterMask;
    INT4      i4FilterType;
    INT4      i4Storage;
    INT4      i4RowStatus;
}tSnmpNotifyFilterTable;
/* V3 Target Tables */
typedef struct 
{
 tTMO_SLL_NODE           Link;
 tSNMP_OCTET_STRING_TYPE AddrName;
 tSNMP_OID_TYPE          Domain;
 tSNMP_OCTET_STRING_TYPE Address; /*The Ipaddress 
                                    is stored in network byte order*/
 INT4                    i4TimeOut;
 INT4                    i4RetryCount;
 tSNMP_OCTET_STRING_TYPE TagList;
 tSNMP_OCTET_STRING_TYPE AddrParams;
 tTMO_SLL                pInformList;    /* Target INFORM Msglist */
 tSNMP_INFORM_STAT       *pInformStat;   /* INFOMR Msg Statistics */
 INT4                    i4StorageType;
 INT4                    i4Status;
 UINT2                   u2Port;  
 BOOL1                   b1HostName;
 UINT1                   au1Padding[1]; /* Padding */ 
 UINT1                   au1Hostname[DNS_MAX_QUERY_LEN];
 UINT1                   au1Reserved[1]; /* Padding */ 
}tSnmpTgtAddrEntry;

typedef struct
{
 tTMO_SLL_NODE           Link;
 tSNMP_OCTET_STRING_TYPE ParamName;
 INT2                    i2ParamMPModel;
 INT2                    i2ParamSecModel;
 tSNMP_OCTET_STRING_TYPE ParamSecName;
 INT4                    i4ParamSecLevel;
 INT2                    i2ParamStorageType;
 INT2                    i2Status; 
 tSnmpNotifyFilterProfileTable *pFilterProfileTable;
}tSnmpTgtParamEntry;

/* Structure for INFORM Msg List */
typedef struct {
    tTMO_SLL_NODE    NextEntry;       /* Next Inform PDU */
    UINT4            u4MgrCnt;        /* No. of Mgrs sent this INFORM PDU */
    tSNMP_NORMAL_PDU *pInformPduSent; /* Sent INFORM PDU */
} tSnmpInformMsgNode;

typedef struct InformTimer {
/*Timer should be the first entry in the structure*/
    tTmrAppTimer        TmrInform;      /* Inform Application Timer */
    tSnmpNotifyEntry   *pNotifyEntry;   /*Pointer to notification Entry*/
    UINT4               u4TimerId;      /* INFORM Timer ID */
    VOID               *pManagerAddr;  /* SNMP Manager Address */
} tInformTimer;

typedef struct {
    tTMO_SLL_NODE  NextEntry;          /* Next Inform PDU */
    tSnmpInformMsgNode *pInformNode;   /* Sent INFORM PDU */
    tInformTimer        InformTmr;     /* Timer */ 
    SnmpAppNotifCb      AppCallback;   /* Application given callback */
    INT4                i4RetryCount;  /* The retransmission count */
    UINT4               u4InformReqId; /* Sent Inform PDU Req Id */
    VOID               *pCookieAddr;  /* Address of Cookie given by Appl */
    UINT1               u1UsrNotify;   /* User application Notification */ 
    UINT1               u1Reserved[3]; /* Reserved */ 
} tInformNode;

typedef tTMO_SLL tInformMsgList; 

/* Structure for SNMP PROXY TABLE  */

typedef struct
{
    tTMO_SLL_NODE                link;
    tSNMP_OCTET_STRING_TYPE     ProxyName;
    INT4                        ProxyType;
    tSNMP_OCTET_STRING_TYPE     PrxContextEngineID;
    tSNMP_OCTET_STRING_TYPE     PrxContextName;
    tSNMP_OCTET_STRING_TYPE     PrxTargetParamsIn;
    tSNMP_OCTET_STRING_TYPE     PrxSingleTargetOut;
    tSNMP_OCTET_STRING_TYPE     PrxMultipleTargetOut;
    INT4                        i4Storage;
    INT4                        i4Status; 
}tProxyTableEntry;

/* Structure for SNMP PRPPROXY TABLE  */

typedef struct
{
    tTMO_SLL_NODE                link;
    tSNMP_OCTET_STRING_TYPE     PrpPrxMibName;
    INT4                        i4PrpPrxMibType;
    tSNMP_OID_TYPE        PrpPrxMibID;
    tSNMP_OCTET_STRING_TYPE     PrpPrxTargetParamsIn;
    tSNMP_OCTET_STRING_TYPE     PrpPrxSingleTargetOut;
    tSNMP_OCTET_STRING_TYPE     PrpPrxMultipleTargetOut;
    INT4                        i4Storage;
    INT4                        i4Status; 
    UINT4                        u4Index;
}tPrpProxyTableEntry;

/* Structure for SNMP Trap Filter Table */

typedef struct 
{
    tRBNodeEmbd                RbNode;
    tSNMP_OID_TYPE             TrapFilterOID;
    INT4                       i4Status;
}tSnmpTrapFilterEntry;

typedef struct
{
    tTMO_SLL_NODE                link;
    tPrpProxyTableEntry            PrpPrxEntry;
}tPrpPrxMibIdEntry;

typedef struct
{
    VOID                *pEntry[MAX_PROXY_PDU_ENTRY];
    INT4                 i4Count;
}tProxEntryArray; 

typedef struct
{
    tSNMP_NORMAL_PDU        *pRespPdu;
    tSNMP_PROXY_CACHE        *pProxyCache;
    tSnmpInformMsgNode        *pInformNode;
    tInformNode                *pInformHostNode;
}tProxyCookie;


typedef struct _tSnmpGetVarBindBlock
{
    tSNMP_GET_VAR_BIND    SnmpGetVarBindBlock[MAX_VARBIND];
}tSnmpGetVarBindBlock;

typedef struct _tSnmpPktBlock
{
    UINT1   au1SnmpPktBlock[MAX_PKT_LENGTH];
}tSnmpPktBlock;

typedef struct _tSnmpV3PduBlock
{
    tSNMP_V3PDU  SnmpV3PduBlock;
    UINT1        SnmpOctetString[7 * SNMP_MAX_OCTETSTRING_SIZE];
}tSnmpV3PduBlock;

typedef struct _tSnmpTcpRcvBufBlock
{
    UINT1    SnmpTcpRcvBufBlock[SNMPTCP_RCV_BUF_LEN];
}tSnmpTcpRcvBufBlock;



extern INT1  gi1SnmpUserName[MAX_AUDIT_USER_NAME_LEN];
extern INT4 gi4Socket;
extern INT4 gi4ProxySocket;

VOID SnmpFreeAllMgrInformList (VOID);
VOID SnmpFreeMgrInformList(tSnmpTgtAddrEntry *pSnmpTgtAddrEntry);
VOID SnmpRemoveInformNode (tSnmpTgtAddrEntry *pSnmpTgtAddrEntry, UINT4 u4ReqId,
                           UINT4 u4IsCallBack, UINT4 u4CbStatus);
VOID SnmpRemoveInformRequestMsgNode (tSnmpInformMsgNode *pInformNode);
UINT4 SNMPSendInformToManager(UINT1 *, INT4, tSnmpTgtAddrEntry *,tSnmpNotifyEntry *, 
                             UINT4, UINT4, UINT4, tSNMP_NORMAL_PDU *,
                             SnmpAppNotifCb, VOID *); 
tSNMP_NORMAL_PDU * SNMPCopyInformPdu (tSNMP_NORMAL_PDU *);
INT4 SnmpAddInformNode (tSNMP_NORMAL_PDU * pInformPdu, UINT4 u4ReqId,
                        tSnmpTgtAddrEntry *pSnmpTgtAddrEntry,
                        SnmpAppNotifCb, VOID *);
INT4 SnmpStartInformReTxTimer (UINT4, UINT4, tSnmpTgtAddrEntry *,tSnmpNotifyEntry *);
INT4 SnmpResendInformReqOnTimeOut(tInformNode *, tSnmpTgtAddrEntry *,tSnmpNotifyEntry *); 

tSNMP_VAR_BIND * SNMPGetSysUpTime (VOID);

INT4 SNMPEncodeV2NotificationPacket(tSNMP_NORMAL_PDU *, UINT1 ** , 
                    INT4 *, INT4);

tSNMP_VAR_BIND * SNMPFormVarBind (tSNMP_OID_TYPE *, INT2, UINT4, 
                               INT4, tSNMP_OCTET_STRING_TYPE *, 
                               tSNMP_OID_TYPE *, 
                                       tSNMP_COUNTER64_TYPE);

tSnmpInformMsgNode * SnmpAddInformRequestMsgNode (tSNMP_NORMAL_PDU * pInformPdu);

tSnmpInformMsgNode * SnmpGetInformRequestMsgNode (UINT4 u4ReqId);

tInformNode * SnmpGetInformNode (UINT4 u4ReqId,
                                 tSnmpTgtAddrEntry *pSnmpTgtAddrEntry);

VOID SnmpProcessTmrExpiry (VOID);

INT4 SnmpInformTimerExpiry (tInformTimer * pTimer);
INT4 SnmpStartTimer (tTimerListId TimerList, tTmrAppTimer * pTimer, UINT4 u4Sec);
VOID SnmpStopTimers (VOID);
PUBLIC INT4 SNMPAgentInit PROTO ((VOID));
PUBLIC INT4 SNMPAgentDeInit PROTO ((VOID));
PUBLIC INT4 SNMPIndexMangerListInit PROTO ((VOID));

/* SNMP Agentx Status */
PUBLIC VOID SnxProcessTmrExpiry PROTO ((VOID));
PUBLIC INT4 SnxMainProtoInit    PROTO ((VOID));
PUBLIC VOID SnxMainRcvdAgtxPacketEvent PROTO ((VOID));
PUBLIC INT4 SnxMainOpenSession PROTO ((VOID));
PUBLIC INT4 SnxMainGenerateTrap(tSNMP_VAR_BIND * pVbList, tSNMP_OID_TYPE * pEnterpriseOid,
                          UINT4 u4GenTrapType, UINT4 u4SpecTrapType);

INT4 SnmpIVAesCfb128(UINT1 *pu1IV, 
                     UINT4 u4EngineBoots, 
                     UINT4 u4EngineTime,
                     UINT1 *pu1Salt);
         


INT4 SNMPInformResponseProcess (tSNMP_NORMAL_PDU * pPdu);

tSnmpTgtAddrEntry * SnmpGetManagerTargetAddr (UINT4 u4IpAddr);

#define INFORM_TIMER_EXPIRY                    1
#define ONE_SEC_TIMER_EXPIRY                   2

#define SNMP_FIRST_TRANSMIT                    0 
#define SNMP_RETRANSMIT                        1

#define INFORM_TIMER_EXPIRY                    1

#define SNMP_INR_INFORM_REQTS          gSnmpStat.u4SnmpOutInformRequests++
#define SNMP_INR_INFORM_RESPS          gSnmpStat.u4SnmpInInformResponses++
#define SNMP_INFORM_DROPS              gSnmpStat.u4SnmpInformDrops 

#define SNMP_INR_GET_RESPONSE          gSnmpStat.u4SnmpInGetResponses++ 

#define SNMP_INR_INFORM_TIMEOUT        gSnmpStat.u4SnmpInformTimeOut++
#define SNMP_INR_INFORM_AWAIT_ACK      gSnmpStat.u4SnmpInformAwaitAck++
#define SNMP_DEC_INFORM_AWAIT_ACK      gSnmpStat.u4SnmpInformAwaitAck--
#define SNMP_INFORM_AWAIT_ACK          gSnmpStat.u4SnmpInformAwaitAck

#define SNMP_INR_ROLLBACK_ERRS         gSnmpStat.u4SnmpRollbackErrs++
/* For Inform Statistics Counter updations */
#define SNMP_MGR_INR_INFORM_REQTS(x)    \
             (x)->pInformStat->u4InformSent++

#define SNMP_MGR_INR_INFORM_AWAIT_ACK(x)          \
             (x)->pInformStat->u4InformAwAck++

#define SNMP_MGR_DEC_INFORM_AWAIT_ACK(x)          \
             (x)->pInformStat->u4InformAwAck--

#define SNMP_MGR_INR_INFORM_ACK_FAILED(x)          \
             (x)->pInformStat->u4InformFail++

#define SNMP_MGR_INFORM_DROPS(x)          \
             (x)->pInformStat->u4InformDrop

#define SNMP_MGR_INR_INFORM_REQ_RETRY(x)          \
             (x)->pInformStat->u4InformRetried++

#define SNMP_MGR_INFORM_REQ_RETRY(x)          \
             (x)->pInformStat->u4InformRetried

#define SNMP_MGR_INFORM_AWAIT_ACK(x)          \
             (x)->pInformStat->u4InformAwAck

#define SNMP_MGR_INFORM_ACK_FAILED(x)          \
             (x)->pInformStat->u4InformFail

#define SNMP_MGR_INFORM_ACK(x)          \
             (x)->pInformStat->u4InformResp

#define SNMP_MGR_INR_INFORM_ACK(x)          \
             (x)->pInformStat->u4InformResp++
                          
#define SNMPFreeV2Inform(pPdu) if(pPdu != NULL) {\
                               FREE_OCTET_STRING(pPdu->pCommunityStr); \
                               FREE_SNMP_VARBIND_LIST(pPdu->pVarBindList);\
                               MemReleaseMemBlock(gSnmpNormalPduPoolId, (UINT1 *)pPdu);}

#define FREE_OCTET_STRING(octet_str) free_octetstring(octet_str)

#define FREE_OID(oid)  free_oid(oid)

#define FREE_SNMP_VARBIND(vb)\
        if (vb != NULL)\
        {\
           FREE_OID(vb->pObjName);\
           if(vb->ObjValue.pOctetStrValue != NULL)\
              FREE_OCTET_STRING(vb->ObjValue.pOctetStrValue);\
           if(vb->ObjValue.pOidValue != NULL)\
              FREE_OID(vb->ObjValue.pOidValue);\
           vb->ObjValue.pOidValue = NULL;\
           vb->ObjValue.pOctetStrValue = NULL;\
           MemReleaseMemBlock (gSnmpVarBindPoolId, (UINT1 *)(vb));\
        }

#define FREE_SNMP_VARBIND_LIST SNMP_free_snmp_vb_list

#define SNMPCopyVarBind(pVarBind) SNMPFormVarBind (pVarBind->pObjName,\
                  pVarBind->ObjValue.i2_DataType,\
                  pVarBind->ObjValue.u4_ULongValue,\
                  pVarBind->ObjValue.i4_SLongValue,\
                  pVarBind->ObjValue.pOctetStrValue,\
                  pVarBind->ObjValue.pOidValue,\
                  pVarBind->ObjValue.u8_Counter64Value)

#define SNMPFreeV2Trap(pPdu) if(pPdu != NULL) {\
                               FREE_OCTET_STRING(pPdu->pCommunityStr); \
                               FREE_SNMP_VARBIND_LIST(pPdu->pVarBindList);\
                               MemReleaseMemBlock(gSnmpNormalPduPoolId, (UINT1 *)pPdu);}


#define SNMPFreeV1Trap(pPdu) if(pPdu != NULL) {\
                               FREE_OCTET_STRING(pPdu->pCommunityStr); \
                               FREE_OID(pPdu->pEnterprise);\
                               FREE_OCTET_STRING(pPdu->pAgentAddr);\
                               FREE_SNMP_VARBIND_LIST(pPdu->pVarBindList);\
                               MemReleaseMemBlock (gSnmpTrapPduPoolId, (UINT1 *) pPdu);}

#define SNMP_PROTO_SEMAPHORE                   ((const UINT1*)"SNMA")
#define SNX_PROTO_SEMAPHORE                   ((const UINT1*)"SNMX")

#define SNMP_CREATE_SEMAPHORE() \
        OsixCreateSem (SNMP_PROTO_SEMAPHORE, 1, OSIX_DEFAULT_SEM_MODE, \
                       (&gu4InformSemId))
 
#define SNMP_DELETE_SEMAPHORE() \
             OsixDeleteSem(SELF, SNMP_PROTO_SEMAPHORE)
 
#define   SNMP_INFORM_LOCK()      \
          {                         \
            OsixTakeSem(SELF, SNMP_PROTO_SEMAPHORE, OSIX_WAIT, 0); \
          } 
 
#define   SNMP_INFORM_UNLOCK()      \
          {  \
             OsixGiveSem(SELF, SNMP_PROTO_SEMAPHORE);  \
          }
#define   SNX_PROTO_LOCK()      \
          {                         \
            OsixTakeSem(SELF, SNX_PROTO_SEMAPHORE, OSIX_WAIT, 0); \
          } 
 
#define   SNX_PROTO_UNLOCK()      \
          {  \
             OsixGiveSem(SELF, SNX_PROTO_SEMAPHORE);  \
          }
#define SNMP_CREATE_DB_SEMAPHORE() \
        OsixCreateSem (((const UINT1*)"SNDB"), 1, OSIX_DEFAULT_SEM_MODE, \
                       (&gu4SnmpDbSemId))

#define SNMP_DELETE_DB_SEMAPHORE() \
    OsixSemDel (gu4SnmpDbSemId)

#define SNMP_DB_LOCK()    \
    OsixSemTake (gu4SnmpDbSemId)

#define SNMP_DB_UNLOCK()  \
    OsixSemGive (gu4SnmpDbSemId)

/* TimerList for INFORM Timeouts */
extern tTimerListId     SnmpAgtTimerListId; /* SNMP Agent Timer List Id */
extern tTimerListId     SnmpPxyTimerListId; /* Proxy timer id list */

extern tTMO_SLL         gSnmpSecGrp;  
extern tTMO_SLL         gSnmpViewTree;  
extern tTMO_SLL         gSnmpAccessEntry;  

extern tSNMP_SYSTEM gSnmpSystem;
extern tSNMP_SYSORTABLE  gaSnmpSysorTable[MAX_SYSOR_TABLE_ENTRY];
extern tSNMP_STAT gSnmpStat;
extern tSnmpUsmStats gSnmpUsmStats;
extern tOsixSemId  gu4InformSemId;
extern tSNMP_OCTET_STRING_TYPE gSnmpEngineID;
extern UINT1 gau1SnmpEngineID[];
extern UINT1 gau1LocalSnmpEngineID[];
extern UINT4 gu4SnmpEngineBootCount;
extern UINT4 gu4SnmpAgentControl;
extern UINT4 gu4SnmpAllowedVersion;
extern UINT4 gu4SnmpMinimumSecurity;
extern INT4  gi4SnmpAgentxSubAgtStatus;
extern INT4  gi4SnmpLaunch;
extern tOsixSemId gu4SnmpDbSemId;
extern tOsixSemId  gu4SnxSemId;

extern tMemPoolId gSnmpAgentxOidTypePoolId;
extern tMemPoolId gSnmpSearchRngPoolId;
extern tMemPoolId gSnmpAgentxVarBindPoolId;



#define SNMP_MAX_UTIL_BLOCKS 50

#define SNMP_COM_DEFAULT_ENGINEID_LEN 7

#define SNMP_ONELONG 1L
#define SNMP_MINUS_ONE   -1
#define SNMP_ZERO   0
#define SNMP_ONE    1
#define SNMP_TWO    2
#define SNMP_THREE  3
#define SNMP_FOUR   4
#define SNMP_EIGHT  8

#define SNMP_ENGINE_MAX            2147483647

#define SNMP_AGT_TMR_EXP_EVT       0x00000001
#define SNMP_AGT_IPV4PKT_EVT       0x00000002
#define SNMP_AGT_IPV6PKT_EVT       0x00000004
#define SNX_TMR_EXP_EVT            0x00000008
#define SNX_AGTX_PKT_EVT           0x00000010
#define SNX_TRANS_RDY_EVT          0x00000020
#define SNMP_PXY_TMR_EXP_EVT       0x00000040
#define SNMP_PXY_IPV4PKT_EVT       0x00000080
#define SNMP_PXY_IPV6PKT_EVT       0x00000100
#define SNMPTCP_ENABLE_EVT         0x00000200
#define SNMPTCP_DISABLE_EVT        0x00000400
#define SNMPTCP_V4_NEWCONN_EVT     0x00000800
#define SNMPTCP_V6_NEWCONN_EVT     0x00001000
#define SNMPTCP_PKT_READ_EVT       0x00002000
#define SNMPTCP_PKT_WRITE_EVT      0x00004000
#define SNMP_THROTTLE_TMR_EXP_EVT   0x00008000
#define SNMP_ALL_EVENTS            0xffffffff

#define SNMP_GET_LAST_BYTE(x) (UINT1)(0xff & x);
#define SNMP_INR_IN_PKTS               gSnmpStat.u4SnmpInPkts++
#define SNMP_INR_OUT_PKTS              gSnmpStat.u4SnmpOutPkts++
#define SNMP_INR_BAD_VERSION           gSnmpStat.u4SnmpInBadVersions++
#define SNMP_INR_BAD_COMMUNITY         gSnmpStat.u4SnmpInBadCommunityNames++
#define SNMP_INR_BAD_COMM_USER         gSnmpStat.u4SnmpInBadCommunityUses++
#define SNMP_INR_BAD_ASN_PARSE         gSnmpStat.u4SnmpInASNParseErrs++
#define SNMP_INR_BAD_TYPE              gSnmpStat.u4SnmpInBadTypes++
#define SNMP_INR_IN_TOO_BIG            gSnmpStat.u4SnmpInTooBigs++
#define SNMP_INR_IN_NO_SUCH_NAME       gSnmpStat.u4SnmpInNoSuchNames++
#define SNMP_INR_IN_BAD_VALUE          gSnmpStat.u4SnmpInBadValues++
#define SNMP_INR_IN_READ_ONLY          gSnmpStat.u4SnmpInReadOnlys++
#define SNMP_INR_IN_GEN_ERROR          gSnmpStat.u4SnmpInGenErrs++
#define SNMP_INR_TOT_GET_SUCCESS       gSnmpStat.u4SnmpInTotalReqVars++
#define SNMP_INR_TOT_SET_SUCCESS       gSnmpStat.u4SnmpInTotalSetVars++
#define SNMP_INR_IN_GET_SUCCESS        gSnmpStat.u4SnmpInGetRequests++
#define SNMP_INR_IN_GETNEXT_SUCCESS    gSnmpStat.u4SnmpInGetNexts++
#define SNMP_INR_IN_SET_SUCCESS        gSnmpStat.u4SnmpInSetRequests++
#define SNMP_INR_IN_GET_RES_SUCCESS    gSnmpStat.u4SnmpInGetResponses++
#define SNMP_INR_IN_TRAP_SUCCESS       gSnmpStat.u4SnmpInTraps++
#define SNMP_INR_OUT_TOO_BIG           gSnmpStat.u4SnmpOutTooBigs++
#define SNMP_INR_OUT_NO_SUCH_NAME      gSnmpStat.u4SnmpOutNoSuchNames++
#define SNMP_INR_OUT_BAD_VALUE         gSnmpStat.u4SnmpOutBadValues++
#define SNMP_INR_OUT_READ_ONLY         gSnmpStat.u4SnmpOutReadOnlys++
#define SNMP_INR_OUT_GEN_ERROR         gSnmpStat.u4SnmpOutGenErrs++
#define SNMP_INR_OUT_GET_SUCCESS       gSnmpStat.u4SnmpOutGetRequests++
#define SNMP_INR_OUT_GETNEXT_SUCCESS   gSnmpStat.u4SnmpOutGetNexts++
#define SNMP_INR_OUT_SET_SUCCESS       gSnmpStat.u4SnmpOutSetRequests++
#define SNMP_INR_OUT_GET_RES_SUCCESS   gSnmpStat.u4SnmpOutGetResponses++
#define SNMP_INR_OUT_TRAP_SUCCESS      gSnmpStat.u4SnmpOutTraps++
#define SNMP_INR_SILENT_DROPS          gSnmpStat.u4SnmpSilentDrops++
#define SNMP_INR_PROXY_DROPS           gSnmpStat.u4SnmpProxyDrops++
#define SNMP_INR_UNKNOWN_CONTEXTS      gSnmpStat.u4SnmpUnknownContexts++
#define SNMP_INR_UNAVIAL_CONTEXTS      gSnmpStat.u4SnmpUnavailableContexts++
#define SNMP_INR_UNKNOWN_SECMODEL      gSnmpStat.u4SnmpUnkownSecModel++
#define SNMP_INR_INVALID_MSG           gSnmpStat.u4SnmpInvalidMsg++
#define SNMP_INR_UNKNOWN_PDUHANDLER    gSnmpStat.u4SnmpUnknownPduHandler++
#define SNMP_DCR_OUT_NO_SUCH_NAME \
                         ((gSnmpStat.u4SnmpOutNoSuchNames) > 0) ? (gSnmpStat.u4SnmpOutNoSuchNames--) : (gSnmpStat.u4SnmpOutNoSuchNames = 0 )


#define SNMP_GET_USM_UNSUPPORTSECLEVELS \
        gSnmpUsmStats.u4UsmStatsUnsupportedSecLevels
#define SNMP_GET_USM_NOTINTIMEWINDOWS   \
       gSnmpUsmStats.u4UsmStatsNotInTimeWindows
#define SNMP_GET_USM_UNKNOWNUSERNAMES   \
       gSnmpUsmStats.u4UsmStatsUnknownUserNames
#define SNMP_GET_USM_UNKNOWNENGINEIDS   \
       gSnmpUsmStats.u4UsmStatsUnknownEngineIDs
#define SNMP_GET_USM_WRONGDIGESTS       \
       gSnmpUsmStats.u4UsmStatsWrongDigests
#define SNMP_GET_USM_DECRYPTIONERRORS   \
       gSnmpUsmStats.u4UsmStatsDecryptionErrors
#define SNMP_GET_USM_USERSPINLOCK       \
       gSnmpUsmStats.i4UsmUserSpinLock
#define SNMP_SET_USM_USERSPINLOCK       \
       gSnmpUsmStats.i4UsmUserSpinLock
#define SNMP_GET_USM_UNKNOWNCONTEXTS   \
       gSnmpStat.u4SnmpUnknownContexts

#define SNMP_INC_USM_UNSUPPORTSECLEVELS \
        gSnmpUsmStats.u4UsmStatsUnsupportedSecLevels++
#define SNMP_INC_USM_NOTINTIMEWINDOWS   \
       gSnmpUsmStats.u4UsmStatsNotInTimeWindows++
#define SNMP_INC_USM_UNKNOWNUSERNAMES   \
       gSnmpUsmStats.u4UsmStatsUnknownUserNames++
#define SNMP_INC_USM_UNKNOWNENGINEIDS   \
       gSnmpUsmStats.u4UsmStatsUnknownEngineIDs++
#define SNMP_INC_USM_WRONGDIGESTS       \
       gSnmpUsmStats.u4UsmStatsWrongDigests++
#define SNMP_INC_USM_DECRYPTIONERRORS   \
       gSnmpUsmStats.u4UsmStatsDecryptionErrors++

#define SNMP_EXCEPTION_NO_SUCH_OBJECT       0x80
#define SNMP_EXCEPTION_NO_SUCH_INSTANCE     0x81
#define SNMP_EXCEPTION_END_OF_MIB_VIEW      0x82
#define SNMP_EXCEPTION_NO_DEF_VAL         0x83

#define GETNEXT_END_OF_TBL     0xA5
#define FMAS_SUBRQ_GET_FAIL    0xA6
#define FMAS_SUBRQ_SET_FAIL    0xA7

#define GET_TIME_TICKS(x) SNMPGetTime(x)

#define SNMP_SLL_DYN_Scan(pList, pNode, pTempNode, TypeCast) \
        for(((pNode) = (TypeCast)(TMO_SLL_First ((pList)))),   \
            ((pTempNode) = ((pNode == NULL) ? NULL : \
                           ((TypeCast)    \
                            (TMO_SLL_Next((pList),  \
                                          ((tTMO_SLL_NODE *)(VOID *)(pNode))))))); \
            (pNode) != NULL;  \
            (pNode) = (pTempNode),  \
            ((pTempNode) = ((pNode == NULL) ? NULL :  \
                           ((TypeCast)  \
                            (TMO_SLL_Next((pList),  \
                                          ((tTMO_SLL_NODE *)(VOID *)(pNode))))))))

#define IS_SCALAR(x) ((x != NULL) && (x->u4NoIndices == 0))
#define IS_TABLE(x) ((x != NULL) && (x->u4NoIndices > 0))

/* Export proto types from snmpmain.c */
UINT4 SNMPGetManagerAddr(VOID);
UINT4 SNMPGetManagerPort(VOID);
VOID SNMPSetManagerAddr( UINT4);
VOID SNMPSetManagerPort( UINT4);
VOID  SNMPSendTrapToManager(UINT1 *, INT4, UINT4, UINT2);
INT4  SNMPSendInformResp(UINT1 *, INT4, UINT4, UINT4);
#ifdef IP6_WANTED
VOID  SNMPSendTrap6ToManager(UINT1 *, INT4, tSNMP_OCTET_STRING_TYPE *); 
#endif 
VOID SNMPGetRemoteManagerIpAddr (tMgrIpAddr *);
INT4  SNMPChangeListenPort(INT4 i4Port);
INT4  SNMPChangeProxyListenPort(INT4 i4Port);
VOID  SNMPGetTime(UINT4 *pu4Time);
VOID  SNMPShutdown(VOID);

INT1 SNMPIsMibRestoreInProgress PROTO ((VOID));

/* Export Proto Types from snmpmem.c */
INT1            SNMPMemInit(VOID);
tSNMP_VAR_BIND* SNMPAllocVarBind(VOID);
VOID            SNMPFreeVarBind(tSNMP_VAR_BIND *pPtr);
tSnmpIndex *    SNMPGetFirstIndexPool(VOID);
tSnmpIndex *    SNMPGetSecondIndexPool(VOID);


/* Export from snmagent.c */
INT1  SNMPCommuChkIfEntryExists(INT4 i4Index);
INT1  SNMPTrapChkIfEntryExists(INT4 i4Index);
INT4  SNMPPDUProcess(tSNMP_NORMAL_PDU *, tSnmpVacmInfo *);
INT4  SNMPCommunityCheck(tSNMP_OCTET_STRING_TYPE *, INT2);

/* Export from snmpcode.c */
INT4 SNMPEncodeGetPacket (tSNMP_NORMAL_PDU *, UINT1 **, INT4 *, INT4);
INT1 *SNMPDecodePacket (UINT1 *pu1_packet_ptr, INT4 i4_pack_length,
        INT2    *pPduType);
INT4 SNMPEncodeV1TrapPacket(tSNMP_TRAP_PDU * , UINT1 **, INT4 *);
INT4 SNMPEncodeV2TrapPacket(tSNMP_NORMAL_PDU *, UINT1 ** , INT4 *, INT4);
INT4 SNMPEstimateNormalPacketLength (tSNMP_NORMAL_PDU *msg_ptr);
INT4 SNMPEstimateVarbindLength (tSNMP_VAR_BIND *vb_ptr);
INT4 SNMPDecodeV3Request (UINT1 *, INT4, tSNMP_V3PDU *);
INT4 SNMPEncodeV3Message (tSNMP_V3PDU *, UINT1 **, INT4 *, INT4);
INT4 SNMPEncodeV3ReportPdu (tSNMP_V3PDU *, UINT1 **, INT4 *, INT4 );
INT4 SNMPVersionCheck(UINT1 *pu1Pkt, INT4 i4PktSize);

/* Export from snmpcfg.c */
VOID SNMPInitSystemObjects(VOID);
VOID SnmpSendColdStartTrap (VOID);
UINT4 SnmpGetNumOctets (INT1 *);
INT1
SnmpConvertStringToOctet (INT1 *, tSNMP_OCTET_STRING_TYPE *);
INT1
SnmpConvertOctetToString (tSNMP_OCTET_STRING_TYPE *, INT1 *);

/* Export from snmptrap.c */
/* VOID SnmpSendAuthFailureTrap (VOID); */
VOID SnmpSendAuthFailureTrap (tSNMP_NORMAL_PDU *);
INT4 SNMPInitNotifyTable(VOID);
tSnmpNotifyEntry *SNMPGetNotifyEntry(tSNMP_OCTET_STRING_TYPE *);
tSnmpNotifyEntry *SNMPCreateNotifyEntry(tSNMP_OCTET_STRING_TYPE *);
tSnmpNotifyEntry *SNMPGetNextNotifyEntry(tSNMP_OCTET_STRING_TYPE*); 
tSnmpNotifyEntry *SNMPGetFirstNotifyEntry(VOID); 
INT4 SNMPDeleteNotifyEntry(tSNMP_OCTET_STRING_TYPE *pNotifyName);
tSNMP_V3PDU *SNMPFillV3Params(tSnmpUsmEntry *pSnmpUsmEntry,tSNMP_OCTET_STRING_TYPE *pContextName);
VOID SNMPGenRandomIV(UINT4 *pu4Data);
tSNMP_NORMAL_PDU * SNMPV1TrapToV2Trap (tSNMP_TRAP_PDU *,tSNMP_OCTET_STRING_TYPE *,UINT4,UINT1);


INT4 SNMPInitNotifyFilterProfileTable(VOID);
tSnmpNotifyFilterProfileTable *SNMPGetNotifyFilterProfileEntry(tSNMP_OCTET_STRING_TYPE *);
tSnmpNotifyFilterProfileTable *SNMPCreateNotifyFilterProfileEntry(tSNMP_OCTET_STRING_TYPE *);
tSnmpNotifyFilterProfileTable *SNMPGetNextNotifyFilterProfileEntry(tSNMP_OCTET_STRING_TYPE*); 
tSnmpNotifyFilterProfileTable *SNMPGetFirstNotifyFilterProfileEntry(VOID); 
INT4 SNMPDeleteNotifyFilterProfileEntry(tSNMP_OCTET_STRING_TYPE *pNotifyFilterPrfName);


INT4 SNMPInitNotifyFilterTable (VOID);
INT4 SNMPCreateNotifyFilterEntry (tSNMP_OCTET_STRING_TYPE * pName, 
                                  tSNMP_OID_TYPE * pSubTree);
tSnmpNotifyFilterTable  *SNMPGetFirstNotifyFilterEntry (VOID);
tSnmpNotifyFilterTable  *SNMPGetNextNotifyFilterEntry (tSNMP_OCTET_STRING_TYPE * 
                                                       pName, tSNMP_OID_TYPE * pSubTree);
VOID   SNMPAddNotifyFilterTableSll (tTMO_SLL_NODE * pNode);
tSnmpNotifyFilterTable  *
SNMPGetNotifyFilterEntry (tSNMP_OCTET_STRING_TYPE * pName, tSNMP_OID_TYPE * pSubTree);
INT4
SNMPDeleteNotifyFilterEntry (tSNMP_OCTET_STRING_TYPE * pName, tSNMP_OID_TYPE * pSubTree);
VOID
SNMPDelNotifyFilterEntrySll (tTMO_SLL_NODE * pNode);
INT4 SNMPNotifyFilterCheckProfileName (tSNMP_OCTET_STRING_TYPE * pName);
INT4 SNMPNotifyFilterAccessAllowed (tSnmpNotifyFilterProfileTable *pSnmpNotifyFilterProfileEntry,tSNMP_OID_TYPE * pOID);
tSnmpNotifyFilterTable  *
snmpNotifyFilterGetLongestMatch (tSNMP_OCTET_STRING_TYPE * pName,
                                 tSNMP_OID_TYPE * pInObject);
INT4 SNMPInitTrapFilterTable (VOID);
INT4 SnmpTrapFilterTblCmpFn PROTO ((tRBElem * pRBElem1, tRBElem * pRBElem2));

/* Export from snmplock.c */
INT4 SNMPAccessGet(tSnmpDbEntry*,tSNMP_MULTI_DATA_TYPE*,tSnmpIndex*, UINT4 *,UINT4);
INT4 SNMPAccessGetNext(tSnmpDbEntry*,tSnmpIndex*,tSnmpIndex *, UINT4 );
INT4 SNMPAccessSet(tSnmpDbEntry*, tSNMP_MULTI_DATA_TYPE*,tSnmpIndex *,
               UINT4 );
INT4 SNMPAccessTest(tSnmpDbEntry*,tSNMP_MULTI_DATA_TYPE*,tSnmpIndex*, UINT4*,
                UINT4 );
INT1 SnmpCopyMultiIndexAndData (tSnmpIndex *pMultiIndex, tSnmpIndex *pIndex,
                                tSNMP_MULTI_DATA_TYPE *pMultiData,
                                tSNMP_MULTI_DATA_TYPE *pData);

/* Export from snmplock.c Dependency Check API*/
INT4 SNMPAccessDep(tSnmpDbEntry * pSnmpDbEntry, UINT4 *pError ,tSnmpIndexList *IndexList, 
           tSNMP_VAR_BIND * VarBindList, UINT4 u4VcNum);


/* Export from snmpsmg.c */
INT4  SNMPProcess(UINT1 *, UINT4, UINT1 **, UINT4 *);
VOID  SNMPPacketOnSocket(INT4);
UINT4 SNMPGetRequestID(VOID);
UINT4 SNMPGetMsgID(VOID);
INT4  SNMPV3Process (UINT1 *, UINT4, UINT1 **, UINT4 *);

/* Export from snmpusm.c */
INT4 SNMPKeyGenerator(UINT1 *pu1Passwd,INT4 i4PasswdLen,UINT4 u4Auth,
                      tSNMP_OCTET_STRING_TYPE *pEngineID,
                      tSNMP_OCTET_STRING_TYPE *pOctetString);
VOID SNMPInitUsmTable(VOID);
tSnmpUsmEntry *SNMPCreateUsmEntry(tSNMP_OCTET_STRING_TYPE *pSnmpEngineID,
                               tSNMP_OCTET_STRING_TYPE *pSnmpUserName);
tSnmpUsmEntry *SNMPGetUsmEntry(tSNMP_OCTET_STRING_TYPE *pSnmpEngineID,
                               tSNMP_OCTET_STRING_TYPE *pSnmpUserName);
INT4 SNMPDeleteUsmEntry(tSNMP_OCTET_STRING_TYPE *pSnmpEngineID,
                               tSNMP_OCTET_STRING_TYPE *pSnmpUserName);
tSnmpUsmEntry *SNMPGetFirstUsmEntry(VOID);
tSnmpUsmEntry *SNMPGetNextUsmEntry(tSNMP_OCTET_STRING_TYPE *pSnmpEngineID,
                                   tSNMP_OCTET_STRING_TYPE *pSnmpUserName);
VOID  SNMPCopyOctetString(tSNMP_OCTET_STRING_TYPE *pFirstOctetStr,
                          tSNMP_OCTET_STRING_TYPE *pSecondOctetStr);
INT4 SNMPCompareOctetString(tSNMP_OCTET_STRING_TYPE *pFirstOctetStr,
                            tSNMP_OCTET_STRING_TYPE *pSecondOctetStr);
INT4 SNMPCompareImpliedOctetString(tSNMP_OCTET_STRING_TYPE *pFirstOctetStr,
                            tSNMP_OCTET_STRING_TYPE *pSecondOctetStr);
INT4 SNMPGenerateAuthSecurity(UINT1 *, INT4, UINT1*, tSnmpUsmEntry *);
INT4 SNMPVerifyAuthSecurity(UINT1 *, INT4,tSNMP_V3PDU *,tSnmpUsmEntry *);
INT4 SNMPDecryptePDU(UINT1 *,INT4, tSNMP_V3PDU *,tSnmpUsmEntry *);
INT4 SNMPEncryptePDU(UINT1 *,INT4,tSNMP_V3PDU *, tSnmpUsmEntry *);
INT4 SNMPValidateEngineId(tSNMP_OCTET_STRING_TYPE *);
INT4 SNMPDeleteAllUsmEntry(tSNMP_OCTET_STRING_TYPE *pSnmpEngineID);
INT1 SnmpUpdateUserEntries (INT1 *pPrevEngineID); 
INT1 SnmpUpdateCommunity (INT1 *pPrevEngineID); 
INT4 SnmpV3DefSecureUsers (VOID);
INT4 SnmpIsPrivSecAlgoSupported (INT4 u4AlgoType);
INT4 SnmpIsAuthSecAlgoSupported (INT4 u4AlgoType);

/* Export form wrap files */
VOID RegisterFUTURESNMP(VOID);
VOID SNMPRegisterSNMPV3MIBS(VOID);

/* Export from vacmview.c */
INT4    VACMInitViewTree(VOID);
INT4    VACMViewTreeAdd(tSNMP_OCTET_STRING_TYPE *, tSNMP_OID_TYPE*);
INT4    VACMCheckAccessTree(tSNMP_OCTET_STRING_TYPE *);

tViewTree*    VACMViewTreeGetFirst(VOID);
tViewTree*    VACMViewTreeGet(tSNMP_OCTET_STRING_TYPE*,tSNMP_OID_TYPE*);
tViewTree*    VACMViewTreeGetNext(tSNMP_OCTET_STRING_TYPE*,tSNMP_OID_TYPE*);
tViewTree*    VACMViewTreeGetLongestMatch(tSNMP_OCTET_STRING_TYPE*,
                                        tSNMP_OID_TYPE*,UINT4 *);
INT4    VACMDeleteViewTree(tSNMP_OCTET_STRING_TYPE *, tSNMP_OID_TYPE*);
INT4 SNMPVACMIsAccessAllowed(tSnmpVacmInfo *, INT4, tSNMP_OID_TYPE *,
                             UINT4 *);


/* Export from vacmacc.c */
INT4    VACMInitAccess(VOID);
INT4    VACMAddAccess(tSNMP_OCTET_STRING_TYPE *,
                    tSNMP_OCTET_STRING_TYPE *,INT4,INT4);
tAccessEntry*  VACMGetAccess    (tSNMP_OCTET_STRING_TYPE *,
                               tSNMP_OCTET_STRING_TYPE *, 
                               INT4, INT4);
UINT4    VACMDeleteAccess(tSNMP_OCTET_STRING_TYPE *,
                       tSNMP_OCTET_STRING_TYPE *,INT4,INT4);
tAccessEntry*  VACMGetNextAccess(tSNMP_OCTET_STRING_TYPE *,
                                 tSNMP_OCTET_STRING_TYPE *,
                                 INT4, INT4);
tAccessEntry*    VACMGetFirstAccess(VOID);

/* Export from vacmsec.c */
INT4    VACMInitSecGrp(VOID);
INT4    VACMAddSecGrp(INT4, tSNMP_OCTET_STRING_TYPE *);
INT4    VACMDeleteSecGrp(INT4, tSNMP_OCTET_STRING_TYPE*);
tSecGrp*    VACMGetFirstSecGrp(VOID);
tSecGrp*    VACMGetSecGrp(INT4,tSNMP_OCTET_STRING_TYPE *);
tSecGrp*    VACMGetNextSecGrp(INT4, tSNMP_OCTET_STRING_TYPE *);

/* Export from vacmcxt.c */
tContext* VACMGetContext(tSNMP_OCTET_STRING_TYPE *pName);
INT4      VACMGetContextNum(tSNMP_OCTET_STRING_TYPE *pName, UINT4 *pu4VcNum);
INT4      VACMAddContext(tSNMP_OCTET_STRING_TYPE *pName);
VOID      VACMContextInit(VOID);
tContext *VACMGetFirstContext(VOID);
tContext *VACMGetNextContext(tSNMP_OCTET_STRING_TYPE *pContext);

/* Export from snmcom.c */
INT4   SnmpInitCommunityTable(VOID);
INT4   SNMPAddCommunityEntry(tSNMP_OCTET_STRING_TYPE *);
tCommunityMappingEntry *SNMPGetCommunityEntry(tSNMP_OCTET_STRING_TYPE *);

tCommunityMappingEntry *
SNMPGetCommunityEntryFromSecNameContextID(
         tSNMP_OCTET_STRING_TYPE * pSecurityName,
         tSNMP_OCTET_STRING_TYPE * pSnmpEngineID);
tCommunityMappingEntry * 
SNMPGetCommunityEntryFromSecName (tSNMP_OCTET_STRING_TYPE * pSecurityName);

tCommunityMappingEntry *SNMPGetImpliedCommunityEntry(tSNMP_OCTET_STRING_TYPE *);
tCommunityMappingEntry *SNMPGetFirstCommunity(VOID);
tCommunityMappingEntry *SNMPGetNextCommunityEntry(tSNMP_OCTET_STRING_TYPE*); 
INT4 SNMPDeleteCommunityEntry(tSNMP_OCTET_STRING_TYPE *);
INT4 SNMPGetVacmInfoFromCommMappingTable(tSNMP_OCTET_STRING_TYPE *pCommName,
                                         UINT4 u4Ver, tSnmpVacmInfo *pVacmInfo);
INT4 SnmpIsModeSecure (VOID);
/* Export from sntarget.c */
extern tSnmpTgtParamEntry  gSnmpTgtParamTable[SNMP_MAX_TGT_PARAM_ENTRY];
extern tTMO_SLL            gSnmpTgtParamSll;
extern UINT1 gau1ParamName[SNMP_MAX_TGT_PARAM_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
INT4 SNMPTgtAddrTableInit(VOID);
tSnmpTgtAddrEntry *SNMPGetTgtAddrEntry(tSNMP_OCTET_STRING_TYPE *);
tSnmpTgtAddrEntry *SNMPCreateTgtAddrEntry(tSNMP_OCTET_STRING_TYPE *); 
INT4 SNMPDeleteTgtAddrEntry(tSNMP_OCTET_STRING_TYPE *);
tSnmpTgtAddrEntry *SNMPGetFirstTgtAddrEntry(VOID);
tSnmpTgtAddrEntry *SNMPGetNextTgtAddrEntry(tSNMP_OCTET_STRING_TYPE *); 
tSnmpTgtAddrEntry * SNMPGetTgtAddrTagEntry(tSNMP_OCTET_STRING_TYPE *,
             tSnmpTgtAddrEntry *);

INT4 SNMPTgtParamTableInit(VOID);
tSnmpTgtParamEntry *SNMPGetTgtParamEntry(tSNMP_OCTET_STRING_TYPE *);
tSnmpTgtParamEntry *SNMPCreateTgtParamEntry(tSNMP_OCTET_STRING_TYPE *); 
INT4 SNMPDeleteTgtParamEntry(tSNMP_OCTET_STRING_TYPE *);
tSnmpTgtParamEntry *SNMPGetFirstTgtParamEntry(VOID);
tSnmpTgtParamEntry *SNMPGetNextTgtParamEntry(tSNMP_OCTET_STRING_TYPE *); 

/* Exports from Wrapper Files */
VOID RegisterSNMPMIB (VOID); 
VOID RegisterSNMCOM(VOID); 

/* Export sntarglw.h */ 

INT1 SnmpCheckString (UINT1 *);
INT1 SnmpCheckDefConfig(tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE *,
  tSNMP_OID_TYPE *, tSNMP_OID_TYPE *, INT4);

INT4 Snmp3GroupConfig (UINT1 *, UINT1 *, INT4, UINT4, UINT4*);
VOID Snmp3DefGroupConfig (VOID);
VOID Snmp3Def1GroupConfig (VOID);
INT1 Snmp3GroupModifyError (tSecGrp *, UINT4 **);
INT1 Snmp3GroupCreateError (tSNMP_OCTET_STRING_TYPE *, INT4, UINT4 **);

INT4 Snmp3AccessConfig (UINT1 *, UINT1 *, INT4, INT4, UINT1 *, UINT1 *,
                        UINT1 *, UINT4, UINT4 *);
VOID Snmp3DefAccessConfig (VOID);
VOID Snmp3Def1AccessConfig (VOID);
INT1 Snmp3AccessModifyError (tSNMP_OCTET_STRING_TYPE *,
                             tSNMP_OCTET_STRING_TYPE *,
                             INT4, INT4, UINT4 **);
INT1 Snmp3AccessCreateError (tSNMP_OCTET_STRING_TYPE *,
                             tSNMP_OCTET_STRING_TYPE *,
                             INT4, INT4, UINT4 **);

INT4 Snmp3CommunityConfig (UINT1 *, UINT1 *, UINT1 *, UINT1 *, 
                           UINT1 *, UINT4, UINT1*, UINT4*);
VOID Snmp3DefCommunityConfig (VOID);
VOID Snmp3Def1CommunityConfig (VOID);
INT1 Snmp3CommunityModifyError (tCommunityMappingEntry *, UINT4**);
INT1 Snmp3CommunityCreateError (tSNMP_OCTET_STRING_TYPE *, UINT4**);

INT4 Snmp3NotifyConfig (UINT1 *, UINT1 *, UINT4, UINT4, UINT4*);
VOID Snmp3DefNotifyConfig (VOID);
INT1 Snmp3NotifModifyError (tSNMP_OCTET_STRING_TYPE *, UINT4**);
INT1 Snmp3NotifCreateError (tSNMP_OCTET_STRING_TYPE *, UINT4**);

INT4 Snmp3TargetParamConfig (UINT1 *, INT4, INT4, UINT1 *, INT4,
                             UINT4, UINT4*);
VOID Snmp3DefTargetParamConfig (VOID);
VOID Snmp3Def1TargetParamConfig (VOID);
INT1 Snmp3TargetParamModifyError (tSNMP_OCTET_STRING_TYPE *, UINT4**);
INT1 Snmp3TargetParamCreateError (tSNMP_OCTET_STRING_TYPE *, UINT4**);
INT4 Snmp3PropProxyConfig (UINT1 *,UINT4 , UINT1 *,
                       UINT1 *, UINT1 *,
                       UINT4 ,UINT4 *);
INT1 Snmp3PropProxyCreateError (tSNMP_OCTET_STRING_TYPE *, UINT4 **);


/* Default configuration prototypes */
tCommunityMappingEntry * 
SNMPGetNextImpliedCommunityEntry (tSNMP_OCTET_STRING_TYPE * );


INT4 SNMPCompareExactMatchOctetString (tSNMP_OCTET_STRING_TYPE * ,
                                       tSNMP_OCTET_STRING_TYPE * );

INT4 SnmpGetUsmUserPassword (tSNMP_OCTET_STRING_TYPE *,
                             tSNMP_OCTET_STRING_TYPE *, UINT1 *, UINT1 *);

INT4 SnmpGetUsmUserProtocol (tSNMP_OCTET_STRING_TYPE *,
                             tSNMP_OCTET_STRING_TYPE *, UINT4 *, UINT4 *);

INT4 SnmpGetUsmUserAuthPrivKeyChange (tSNMP_OCTET_STRING_TYPE *,
                                      tSNMP_OCTET_STRING_TYPE *,
                                      tSNMP_OCTET_STRING_TYPE *,
                                      tSNMP_OCTET_STRING_TYPE *);

/* Exported fot MSR */


INT1
nmhGetVacmSecurityToGroupStorageType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));




/* Export from snmprox.c */
INT4 SnmpInitProxyTable (VOID);
INT1 SnmpInitProxyCache(VOID);
INT4 SNMPAddProxyEntry (tSNMP_OCTET_STRING_TYPE * pProxyName);
VOID SNMPProxyEntryAddSll (tTMO_SLL_NODE * pNode);
tProxyTableEntry * SNMPGetProxyEntryFromName (tSNMP_OCTET_STRING_TYPE * );
tProxyTableEntry * SNMPGetProxyEntryFromCtxtId (tSNMP_OCTET_STRING_TYPE * );
INT4 SNMPGetProxyEntry ( tSNMP_OCTET_STRING_TYPE  *pContextEngineID, 
                         tSNMP_OCTET_STRING_TYPE  *pContextName,
                         INT4                     i4PduType,
                         tProxEntryArray          *pProxyEntry);
INT4 SNMPIsValidProxyEntry (VOID);
tProxyTableEntry * SNMPGetFirstProxy (VOID);
tProxyTableEntry * SNMPGetNextProxyEntry (tSNMP_OCTET_STRING_TYPE * pProxyName);
INT4 SNMPDeleteProxyEntry (tSNMP_OCTET_STRING_TYPE * pProxyName);
VOID SNMPDelProxyEntrySll (tTMO_SLL_NODE * pNode);
INT4 SNMPIsProxyOrAgent ( VOID                      *pPdu,
                          INT4                      i4pduVersion,
                          INT2                    i2_PduType,
                          tSNMP_OCTET_STRING_TYPE **pSingleTargetOut,
                          tSNMP_OCTET_STRING_TYPE **pMulTargetOut,
                          INT4                      *pIsProxy,
                          INT4                      *pSendFailResp);
VOID
SNMPMatchProxyEntry (VOID                    *pPdu,
                     INT4                    i4pduVersion,
                     INT2                   i2_PduType,
                     INT4                    *pIsMatchFound,
                     tCommunityMappingEntry *pCommEntry,
                     tSnmpTgtParamEntry        *pTgtParamEntry);
UINT4 SNMPGetSecLevel (tSNMP_V3PDU *);
INT4 SNMPGetEntryCountFromPrxType (INT4);
INT4 SNMPProxyNotifyHandle (VOID *, INT4 , tSNMP_OCTET_STRING_TYPE    *,
                            SnmpAppNotifCb);
INT4 SNMPFreeTrapPdu (VOID **, INT4);
INT4 SnmpProxyResendInformOnTimeOut (tInformNode *, tSnmpTgtAddrEntry *);
INT4 SNMPProxyInformRespProcess (VOID *, INT4);
INT4 SnmpProxyRemoveInformNode (tSnmpTgtAddrEntry *, UINT4, tSNMP_NORMAL_PDU *,
                           UINT4 u4IsCallBack, UINT4 u4CbStatus);
INT1 Snmp3ProxyModifyError (tProxyTableEntry *, UINT4 **);
INT1
Snmp3ProxyCreateError (tSNMP_OCTET_STRING_TYPE *,
                           UINT4 **);
INT4 Snmp3ProxyConfig (UINT1 *,UINT4 , UINT1 *, 
                       UINT1 *, UINT1 *, 
                       UINT1 *, UINT4 ,UINT4 *);

VOID SNMPProxyInformCallback (tSnmpInfmStatusInfo    *);
VOID SNMPProxyLogMessage(VOID *, INT4 , INT2);
VOID SNMPPrintTrapPdu(tSNMP_TRAP_PDU    *);
VOID SNMPPrintV3Pdu(tSNMP_V3PDU    *);
VOID SNMPPrintV2Pdu(tSNMP_NORMAL_PDU    *);
VOID SNMPPrintOctetString(tSNMP_OCTET_STRING_TYPE    *);
VOID SNMPPrintOIDType(tSNMP_OID_TYPE    *);
VOID SNMPPrintVarbindList(tSNMP_VAR_BIND *,tSNMP_VAR_BIND *);
VOID SNMPPrintMultiDataType(tSNMP_MULTI_DATA_TYPE    *);

/***************** Prop Proxy Table ****************/
INT4 SnmpInitPrpProxyTable (VOID);
INT4 SNMPAddPrpProxyEntry (tSNMP_OCTET_STRING_TYPE * );
VOID SNMPAddPrpProxyEntrySll (tTMO_SLL_NODE * );
VOID SNMPAddPrpPrxMibIdEntrySll (tPrpProxyTableEntry * );
tPrpProxyTableEntry * SNMPGetPrpProxyEntryFromName (tSNMP_OCTET_STRING_TYPE * );
tPrpProxyTableEntry * SNMPGetPrpProxyEntryFromMibId (tSNMP_OID_TYPE * );
INT4 SNMPGetPrpProxyEntry ( tSNMP_OID_TYPE  *pPrpPrxMibID, 
                         INT4                     i4ProxyType,
                         tProxEntryArray          *pProxyEntry);
tPrpProxyTableEntry * SNMPGetFirstPrpProxy (VOID);
tPrpProxyTableEntry * SNMPGetNextPrpProxyEntry (tSNMP_OCTET_STRING_TYPE * );
INT4 SNMPDeletePrpProxyEntry (tSNMP_OCTET_STRING_TYPE * );
INT4 SNMPDelPrpPrxMibIdEntry (tPrpProxyTableEntry * );
VOID SNMPDelPrpProxyEntrySll (tTMO_SLL_NODE * pNode);
VOID SNMPDelPrpPrxMibIdEntrySll (tTMO_SLL_NODE * pNode);
INT4 SNMPCompareRootOID (tSNMP_OID_TYPE *, tSNMP_OID_TYPE * );
INT4 SNMPIsValidPrpProxyEntry (VOID);


/****************************************************/

/* snmpinfm.c */
INT4 SnmpCheckForConnection (VOID);

#define SNMP_DEF_COMMUNITY_1     "NETMAN"
#define SNMP_DEF_COMMUNITY_2     "PUBLIC"
#define SNMP_DEF_GROUP_1         "iso"
#define SNMP_DEF_GROUP_2         "noAuthUser"
#define SNMP_DEF_ACCESS_1        "iso"
#define SNMP_DEF_ACCESS_2        "noAuthUser"
#define SNMP_DEF_VIEW_1          "iso"
#define SNMP_DEF_VIEW_2          "restricted"
#define SNMP_DEF_TGT_PARAM_1     "internet"
#define SNMP_DEF_TGT_PARAM_2     "test1"
#define SNMP_DEF_USER_1          "noAuthUser"
#define SNMP_DEF_USER_2          "templateMD5"
#define SNMP_DEF_USER_3          "templateSHA"
#define SNMP_DEF_NOTIFY_1        "iss"
#define SNMP_DEF_NOTIFY_2        "iss1"
#define SNMP_DEF_READ_NAME       "iso"
#define SNMP_DEF_WRITE_NAME      "iso"
#define SNMP_DEF_NOTIFY_NAME     "iso"

#define SNMP_USER1_AUTH_OID    "1.3.6.1.6.3.10.1.1.1" 
#define SNMP_USER2_AUTH_OID    "1.3.6.1.6.3.10.1.1.2" 
#define SNMP_USER3_AUTH_OID    "1.3.6.1.6.3.10.1.1.3" 

#define SNMP_USER1_PRIV_OID    "1.3.6.1.6.3.10.1.2.1" 
#define SNMP_USER2_PRIV_OID    "1.3.6.1.6.3.10.1.2.1" 
#define SNMP_USER3_PRIV_OID    "1.3.6.1.6.3.10.1.2.2" 
#define SNMP_USER4_PRIV_OID    "1.3.6.1.6.3.10.1.2.4"

typedef struct SNMP_DEF {
   const char          *pu1VarName;
} tSNMP_DEFAULT_TABLE;

extern tSNMP_DEFAULT_TABLE          gaSnmpCommunityDefault[];
extern tSNMP_DEFAULT_TABLE          gaSnmpNotifyDefault[];
extern tSNMP_DEFAULT_TABLE          gaSnmpTgtParamDefault[];
extern tSNMP_DEFAULT_TABLE          gaSnmpViewDefault[];
extern tSNMP_DEFAULT_TABLE          gaSnmpGrpDefault[];
extern tSNMP_DEFAULT_TABLE          gaSnmpUserDefault[];
extern tSNMP_OCTET_STRING_TYPE      gSnmpDefaultEngineID;

INT1
SNMPConvertSubOid (UINT1 **ppu1String, UINT4 *pu4Value);
INT1
SNMPHexConvertSubOid (UINT1 **ppu1String, UINT4 *pu4Value);
VOID
SNMPConvertcolonstrToOidString (UINT1 *pCStr, UINT1 *pDStr);


extern UINT4 CasGetSnmpEngineID (tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetFwlGlobalTrap (INT4 *);
extern INT1 nmhGetDhcpSrvPoolUtlTrapControl (INT4 *);
extern INT1 nmhTestv2FwlGlobalTrap (UINT4 *, INT4 );
extern INT1 nmhSetFwlGlobalTrap (INT4 );
extern INT1 nmhTestv2DhcpSrvPoolUtlTrapControl (UINT4 *, INT4 );
extern INT1 nmhSetDhcpSrvPoolUtlTrapControl (INT4 );
extern INT1 nmhGetFirstIndexIfTable (INT4 *);
extern INT1 nmhGetIfType (INT4 , INT4 *);
extern INT1 nmhGetIfLinkUpDownTrapEnable (INT4 , INT4 *);
extern INT1 nmhGetNextIndexIfTable (INT4 , INT4 *);
extern INT4 Snmp3GetT1E1LinkTrapStatus (VOID);

extern INT1 nmhGetMplsTunnelNotificationEnable ARG_LIST((INT4 *));
/* Entity MIB*/

typedef struct {
    UINT4            u4LastChangeTime;
    tTimerListId     ThrottlingTmrListId;
    tTmrAppTimer     ThrottlingTmr;
    BOOL1            b1TimeChangeFlag;
    UINT1            au1Padding[3];
} tSnmpEntGlobalInfo;


extern tSnmpEntGlobalInfo gSnmpEntGlobalInfo;

#define SNMP_MAX_LAST_TIME_CHANGE_TRAPS    9 

extern UINT4 gaau4TrapOidList[SNMP_MAX_LAST_TIME_CHANGE_TRAPS][SNMP_MAX_OID_LENGTH];
extern UINT1 gau1TrapType[SNMP_MAX_LAST_TIME_CHANGE_TRAPS];


#define SNMP_THROTTLING_DEFAULT_INTERVAL     5 /*sec*/

#define SNMP_ARRAY_SIZE_256                           256
#define SNMP_ARRAY_SIZE_257                           257

#define SNMP_ENT_STD_TRAP_OID    "1.3.6.1.2.1.47.2"

#define SNMP_ENT_MIB_LAST_CHANGE_TIME_OID     "1.3.6.1.2.1.47.1.4.1"
#define SNMP_ENT_MIB_OBJ_LAST_CHANGE_TIME     "entLastChangeTime"
#define SNMP_LAST_CHANGE_TIME_VAL          1
#define SNMP_VAL_10                10
#define SNMP_4BIT_MAX             0xf

/* the following macto should be used only in checks and loops
 * it should not be used for memory creation purpose */
#define SNMP_IFPORT_LIST_SIZE  BRG_PORT_LIST_SIZE_EXT
#define SNMP_PORTS_PER_BYTE         8

/* ENT 8 bit */
#define SNMP_BIT8 0x80


#define SNMP_FS_TRAP_OID_LEN 9
#define SNMP_FS_TRAP_OID    "1.3.6.1.4.1.2076.112.20" 

#define SNMP_MIB_REG_TRAP 2
#define SNMP_MIB_DEREG_TRAP 3

PUBLIC VOID SnmpThrottleTmrExpiry PROTO ((VOID));

PUBLIC INT1
SnmpMatchTrapForLastChangeTime PROTO ((tSNMP_OID_TYPE *, UINT4));


PUBLIC VOID SnmpLastChangeNotify PROTO ((VOID));

VOID
SnmpCreateVirtualAccessConfig PROTO ((UINT1 *));
VOID
SnmpCreateVirtualCommunityConfig PROTO ((UINT1 *));
VOID
SnmpDeleteVirtualAccessConfig PROTO ((tSNMP_OCTET_STRING_TYPE *));
VOID SnmpSendTrap PROTO ((UINT1, INT1 *, UINT1, VOID *));
INT1 SnmpGetMIBNameForBaseOid PROTO ((tSNMP_OID_TYPE * , UINT1 *));

/*Exports from srmmem.c */
extern tMemPoolId gSnmpGetVarBindPoolId;
extern tMemPoolId gSnmpPktPoolId;
extern tMemPoolId gSnmpNormalPduPoolId;
extern tMemPoolId gSnmpTrapPduPoolId;
extern tMemPoolId gSnmpInformNodePoolId;
extern tMemPoolId gSnmpInformMsgNodePoolId;
extern tMemPoolId gSnmpTcpRcvBufPoolId;
extern tMemPoolId gSnmpTcpTxNodePoolId;
extern tMemPoolId gSnmpV3PduPoolId;
#ifndef SNAGTXINC_H
#include "snxinc.h"
#endif
#include "snmpsz.h"

/* For SNMP community controlling */
#ifdef __SNMPMAIN_C_
INT4  gi4SnmpCommunityCtrl = SNMP_FALSE;
UINT1 gu1SNMPUpdateDefault = OSIX_TRUE;
#else
PUBLIC INT4 gi4SnmpCommunityCtrl;
PUBLIC UINT1 gu1SNMPUpdateDefault;
#endif /* __SNMPMAIN_C_ */

#ifdef __SNMPMAIN_C_
INT4 gi4SnmpSysLogId;
#else
PUBLIC INT4 gi4SnmpSysLogId;
#endif

#endif /* _SNMPCMN_H */
