/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: snmpmbdb.c,v 1.11 2015/08/20 12:36:44 siva Exp $
 *
 * Description: Routines for mib db access 
 *******************************************************************/

#include "snmpcmn.h"
#include "snmpmbdb.h"
#include "webnmutl.h"
tMibReg            *gpMibReg = NULL;
tMibReg            *gpLastAccessMibPtr = NULL;
tMibReg             gaSnmpMibReg[SNMP_MAX_NUM_OF_MIBS];
tMibData            gaSnmpMibData[SNMP_MAX_NUM_OF_MIBS];
UINT1               gau1SnmpOidType[SNMP_MAX_NUM_OF_MIBS]
    [sizeof (tSNMP_OID_TYPE) + MAX_OID_LENGTH];

UINT4               gSnmpMibDataCurPos = 0;
UINT4               gSnmpOidTypeCurPos = 0;
extern tIndexMgrId  gMibRegIndexMgrId;

/************************************************************************
 *  Function Name   : OIDPrint 
 *  Description     : Function to print oid
 *  Input           : ptr - Oid Pointer
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
OIDPrint (tSNMP_OID_TYPE * ptr)
{
    UINT4               u4Count = 0;
    if (ptr == NULL)
    {
        SNMPTrace ("\n OID is a NULL Pointer");
        return;
    }
    SNMPARGTrace ("OID Length = %d, OID Value =", ptr->u4_Length);
    for (u4Count = SNMP_ZERO;
         u4Count < (UINT4) (ptr->u4_Length - SNMP_ONE); u4Count++)
    {
        SNMPARGTrace ("%d.", ptr->pu4_OidList[u4Count]);
    }
    SNMPARGTrace ("%d\n", ptr->pu4_OidList[u4Count]);
}

/************************************************************************
 *  Function Name   : OctetListPrint 
 *  Description     : Function to print octet string
 *  Input           : ptr - octet string pointer
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
OctetListPrint (tSNMP_OCTET_STRING_TYPE * ptr)
{
    UINT4               u4Count = 0;
    if (ptr == NULL)
    {
        SNMPTrace ("\n OID is a NULL Pointer");
        return;
    }
    SNMPARGTrace ("OID Length = %d, OID Value =", ptr->i4_Length);
    for (u4Count = SNMP_ZERO;
         u4Count < (UINT4) (ptr->i4_Length - SNMP_ONE); u4Count++)
    {
        SNMPARGTrace ("%d.", ptr->pu1_OctetList[u4Count]);
    }
    SNMPARGTrace ("%d\n", ptr->pu1_OctetList[u4Count]);
}

/************************************************************************
 *  Function Name   : VarBindListPrint 
 *  Description     : Function to print the given varbind
 *  Input           : ptr - Variable bind pointer
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
VarBindListPrint (tSNMP_VAR_BIND * ptr)
{
    if (ptr == NULL)
    {
        return;
    }

    OIDPrint (ptr->pObjName);
    switch (ptr->ObjValue.i2_DataType)
    {
        case SNMP_DATA_TYPE_COUNTER32:
            SNMPARGTrace ("Data Type -- COUNTER32 Value -- %d\n",
                          ptr->ObjValue.u4_ULongValue);
            break;
        case SNMP_DATA_TYPE_GAUGE32:
            SNMPARGTrace ("Data Type -- GAUGE32 Value--- %d\n",
                          ptr->ObjValue.u4_ULongValue);
            break;
            /*this is moved here since TimeTicks is an unsigned value */
        case SNMP_DATA_TYPE_TIME_TICKS:
            SNMPARGTrace ("Data Type -- TimeTicks Value-->%d\n",
                          ptr->ObjValue.u4_ULongValue);
            break;
        case SNMP_DATA_TYPE_INTEGER32:
            SNMPARGTrace ("Data Type -- INTEGER32 Value -- %d\n",
                          ptr->ObjValue.i4_SLongValue);
            break;
        case SNMP_DATA_TYPE_OBJECT_ID:
            SNMPTrace ("Data Type -- OBECT_IDENTIFIER\n");
            OIDPrint (ptr->ObjValue.pOidValue);
            break;
        case SNMP_DATA_TYPE_OCTET_PRIM:
            SNMPTrace ("Data Type -- OCTET STRING\n");
            OctetListPrint (ptr->ObjValue.pOctetStrValue);
            break;
        case SNMP_DATA_TYPE_OPAQUE:
            SNMPTrace ("Data Type -- OPAQUE\n");
            OctetListPrint (ptr->ObjValue.pOctetStrValue);
            break;
        case SNMP_DATA_TYPE_IP_ADDR_PRIM:
            SNMPTrace ("Data Type -- IPADDRESS\n");
            OctetListPrint (ptr->ObjValue.pOctetStrValue);
            break;
        case SNMP_DATA_TYPE_NULL:
        case SNMP_EXCEPTION_NO_SUCH_OBJECT:
        case SNMP_EXCEPTION_NO_SUCH_INSTANCE:
        case SNMP_EXCEPTION_END_OF_MIB_VIEW:
            SNMPTrace ("SNMP Exception\n");
            break;
        case SNMP_DATA_TYPE_COUNTER64:
            SNMPARG2Trace ("Data Type -- COUNTER64 Value -- %d,%d\n",
                           ptr->ObjValue.u8_Counter64Value.msn,
                           ptr->ObjValue.u8_Counter64Value.lsn);
            break;
        case GETNEXT_END_OF_TBL:
        case FMAS_SUBRQ_GET_FAIL:
        case FMAS_SUBRQ_SET_FAIL:
            SNMPTrace ("SNMP FAILURE\n");
            break;
        default:
            return;
    }
}

/************************************************************************
 *  Function Name   : SNMPPduPrint
 *  Description     : Function to print the Protocol Data Unit
 *  Input           : pPtr- Pdu Pointer
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
SNMPPduPrint (tSNMP_NORMAL_PDU * pPtr)
{
    tSNMP_VAR_BIND     *pCurr = pPtr->pVarBindList;
    SNMPARGTrace ("\n PDU Version is :%d", pPtr->u4_Version);
    SNMPARGTrace ("\n PDU Request Id is :%d", pPtr->u4_RequestID);
    SNMPARGTrace ("\n PDU ErrorStatus is :%d", pPtr->i4_ErrorStatus);
    SNMPARGTrace ("\n PDU ErrorIndex is :%d", pPtr->i4_ErrorIndex);
    OctetListPrint (pPtr->pCommunityStr);
    while (pCurr != NULL)
    {
        VarBindListPrint (pCurr);
        pCurr = pCurr->pNextVarBind;
    }
}

/************************************************************************
 *  Function Name   : OIDCompare 
 *  Description     : This function compares OID1 with OID2
 *                    if OID1 is lesser than OID2 it returns SNMP_LESSER
 *                    if OID1 is greater than OID2 it returns SNMP_GREATER
 *                    if Both are and lengths are also  equal it returns
 *                    ABS_EQUAL
 *  Input           : OID1 - First Oid, OID2 - Second Oid 
 *  Output          : u4Count - Length upto which they are matching
 *  Returns         : GEATER/SNMP_LESSER/EQUAL
 ************************************************************************/
UINT4
OIDCompare (tSNMP_OID_TYPE OID1, tSNMP_OID_TYPE OID2, UINT4 *u4Count)
{
    UINT4               u4Max = OID1.u4_Length;
    UINT4               u4RetVal = SNMP_EQUAL;

    *u4Count = SNMP_ZERO;

    if (OID1.u4_Length < OID2.u4_Length)
    {
        u4Max = OID1.u4_Length;
        u4RetVal = SNMP_LESSER;
    }
    else if (OID1.u4_Length > OID2.u4_Length)
    {
        u4Max = OID2.u4_Length;
        u4RetVal = SNMP_GREATER;
    }

    for (*u4Count = SNMP_ZERO; *u4Count < u4Max; (*u4Count)++)
    {
        if (OID1.pu4_OidList[*u4Count] != OID2.pu4_OidList[*u4Count])
        {
            if (OID1.pu4_OidList[*u4Count] < OID2.pu4_OidList[*u4Count])
            {
                return SNMP_LESSER;
            }
            return SNMP_GREATER;
        }
    }
    return u4RetVal;
}

/************************************************************************
 *  Function Name   : OIDSearch 
 *  Description     : This functions Searches th MBDB table to get 
 *                    the matching entry
 *                    return EQUAL if it finds an entry
 *                    returs GREATER_OUT if the entry is not within the range
 *                    and greater than the Last Entry.
 *                    returns LESSER_OUT if the entry is not within 
 *                    the range and
 *                    lesser than the First Entry.
 *                    returns NEXT SNMP_GREATER entry if not found but
 *                    within the range
 *  Input           : gMib - Mib Entry Pointer 
 *                    CurOID - Current Oid Pointer
 *  Output          : pNext - Next DB entry Pointer 
 *                    u4Result  - Greater/Lesser/Equal 
 *                    u4Match - Matching length                    
 *  Returns         : Cur DB entry pointer or null
 ************************************************************************/
tMbDbEntry         *
OIDSearch (tMibData * gMib, tSNMP_OID_TYPE CurOID, UINT4 *u4Result,
           tMbDbEntry ** pNext, UINT4 *u4Match)
{
    UINT4               u4Count = 0, u4Min = 0, u4Max = 0;
    UINT4               u4Status = 0;

    u4Min = SNMP_ZERO;
    u4Max = gMib->u4Total;

    u4Count = u4Max - SNMP_ONE;
    *u4Match = SNMP_ZERO;

    u4Status = OIDCompare (CurOID, gMib->pMibEntry[u4Count].ObjectID, u4Match);
/*--------------Checking Boundry values -----------*/
    if ((u4Status == SNMP_EQUAL) || ((u4Status == SNMP_GREATER) &&
                                     (*u4Match ==
                                      gMib->pMibEntry[u4Count].ObjectID.
                                      u4_Length)))
    {
        *u4Result = u4Status;
        *pNext = NULL;
        return &(gMib->pMibEntry[u4Count]);
    }
    else if (u4Status == SNMP_GREATER)
    {
        *u4Result = GREATER_OUT;
        *pNext = NULL;
        return NULL;
    }

    u4Status = OIDCompare (CurOID, gMib->pMibEntry[u4Min].ObjectID, u4Match);
    if ((u4Status == SNMP_EQUAL) || ((u4Status == SNMP_GREATER) &&
                                     (*u4Match ==
                                      gMib->pMibEntry[u4Min].ObjectID.
                                      u4_Length)))
    {
        *u4Result = u4Status;
        if (u4Max > u4Min)
        {
            *pNext = &(gMib->pMibEntry[u4Min + SNMP_ONE]);
        }
        else
        {
            *pNext = NULL;
        }
        return &(gMib->pMibEntry[u4Min]);
    }
    else if (u4Status == SNMP_LESSER)
    {
        *u4Result = LESSER_OUT;
        *pNext = &(gMib->pMibEntry[u4Min]);
        return NULL;
    }
/*-----------If within the range -------------------*/
    while (u4Max > u4Min)
    {
        u4Count = u4Min + (u4Max - u4Min) / SNMP_TWO;
        u4Status =
            OIDCompare (CurOID, gMib->pMibEntry[u4Count].ObjectID, u4Match);
        if ((u4Status == SNMP_EQUAL) ||
            ((u4Status == SNMP_GREATER) &&
             (*u4Match == gMib->pMibEntry[u4Count].ObjectID.u4_Length)))
        {
            *u4Result = u4Status;
            *pNext = &(gMib->pMibEntry[u4Count + SNMP_ONE]);
            return &gMib->pMibEntry[u4Count];
        }

        else if ((u4Count == u4Min) || (u4Count == u4Max))
        {
            *u4Result = u4Status;
            if (u4Status == SNMP_LESSER)
            {
                *pNext = &gMib->pMibEntry[u4Count];
            }
            else
            {
                *pNext = &gMib->pMibEntry[u4Count + SNMP_ONE];
            }
            return NULL;
        }

        else if (u4Status == SNMP_GREATER)
        {
            u4Min = u4Count;
        }
        else
            u4Max = u4Count;

    }
    return NULL;
}

/************************************************************************
 *  Function Name   : MibRegAllocate 
 *  Description     : Allocate memory for protocol register
 *  Input           : u4Index - Index allocated by index manager
 *  Output          : None
 *  Returns         : Pointer for protocol register or null
 ************************************************************************/
tMibReg            *
MibRegAllocate (UINT4 u4Index)
{
    return (&gaSnmpMibReg[u4Index]);
}

/************************************************************************
 *  Function Name   : AttachMib 
 *  Description     : Function to attach a mib to mib database
 *  Input           : MibID - Oid to be attached
 *                    pMibData - DB data pointer    
 *                    pLockPointer - Lock pointer    
 *                    pUnlockPointer - UnLock pointer    
 *                    pSetContextPointer - Context pointer    
 *                    pReleaseContextPointer - Release Context pointer    
 *                    SnmpMsrTgr - Flag to indicate MSR trigger
 *                                 is required or not   
 *                    u4Index - Index to retrieve mib entry    
 *  Output          : None
 *  Returns         : Success or Failure
 ************************************************************************/
INT4
AttachMib (tSNMP_OID_TYPE * MibID, tMibData * pMibData,
           INT4 (*pLockPointer) (VOID), INT4 (*pUnlockPointer) (VOID),
           INT4 (*pSetContextPointer) (UINT4),
           VOID (*pReleaseContextPointer) (VOID), tSnmpMsrBool SnmpMsrTgr,
           UINT4 u4Index)
{
    tMibReg            *pMibPtr = gpMibReg, *pNew = NULL, *pPre = NULL;
    UINT4               u4Result = 0, u4Count = 0;
    UINT4               u4Last;
    UINT1               au1Oid[SNMP_MAX_OID_LENGTH];
    UINT1               au1POid[SNMP_MAX_OID_LENGTH];

/* ------Validation Check ----------------*/
    if ((MibID == NULL) || (pMibData == NULL) ||
        (pMibData->u4Total == SNMP_ZERO) || (pMibData->pMibEntry == NULL))
    {
        return SNMP_FAILURE;
    }

/*------ First Entry Condition--------*/
    if (gpMibReg == NULL)
    {
        gpMibReg = (tMibReg *) MibRegAllocate (u4Index);
        if (gpMibReg == NULL)
        {
            return SNMP_FAILURE;
        }
        else
        {
            gpMibReg->pMibID = MibID;
            gpMibReg->pCurMib = pMibData;
            gpMibReg->pLockPointer = pLockPointer;
            gpMibReg->pUnlockPointer = pUnlockPointer;
            gpMibReg->pSetContextPointer = pSetContextPointer;
            gpMibReg->pReleaseContextPointer = pReleaseContextPointer;
            gpMibReg->SnmpMsrTgr = SnmpMsrTgr;
            gpMibReg->pNextMib = NULL;
            return SNMP_SUCCESS;
        }
    }
/*---------------------------------------*/
    while (pMibPtr != NULL)
    {
        u4Result = OIDCompare (*MibID, *pMibPtr->pMibID, &u4Count);
        if (u4Result == SNMP_LESSER)
        {
            if (u4Count == MibID->u4_Length)
            {
                u4Count = 0;
                u4Last = pMibData->u4Total - 1;
                u4Result = OIDCompare (pMibData->pMibEntry[u4Last].ObjectID,
                                       *pMibPtr->pMibID, &u4Count);
                if (u4Result == SNMP_GREATER)
                {
                    WebnmConvertOidToString (MibID, au1Oid);
                    WebnmConvertOidToString (pMibPtr->pMibID, au1POid);
                    UtlTrcLog (SNMP_TRUE, SNMP_TRUE, "SNMP",
                               "Warning Registering OID %s is an superset of "
                               "registered OID %s\n", au1Oid, au1POid);
                }
            }

            break;
        }
        else if (u4Result == SNMP_GREATER)
        {
            if (u4Count == pMibPtr->pMibID->u4_Length)
            {
                u4Count = 0;
                u4Last = pMibPtr->pCurMib->u4Total - 1;
                u4Result = OIDCompare (*MibID,
                                       pMibPtr->pCurMib->pMibEntry[u4Last].
                                       ObjectID, &u4Count);
                if (u4Result == SNMP_LESSER)
                {
                    WebnmConvertOidToString (MibID, au1Oid);
                    WebnmConvertOidToString (pMibPtr->pMibID, au1POid);
                    UtlTrcLog (SNMP_TRUE, SNMP_TRUE, "SNMP",
                               "Warning Registering OID %s is an subset of "
                               "registered OID %s\n", au1Oid, au1POid);
                }
            }
            pPre = pMibPtr;
            pMibPtr = pMibPtr->pNextMib;
        }
        else if (u4Result == SNMP_EQUAL)
        {
            return SNMP_FAILURE;
        }
    }
    pNew = MibRegAllocate (u4Index);
    if (pNew == NULL)
    {
        return SNMP_FAILURE;
    }
    pNew->pMibID = MibID;
    pNew->pCurMib = pMibData;
    pNew->pNextMib = pMibPtr;
    pNew->pLockPointer = pLockPointer;
    pNew->pUnlockPointer = pUnlockPointer;
    pNew->pSetContextPointer = pSetContextPointer;
    pNew->pReleaseContextPointer = pReleaseContextPointer;
    pNew->SnmpMsrTgr = SnmpMsrTgr;
    if (pPre != NULL)
    {
        pPre->pNextMib = pNew;
    }
    else
    {
        gpMibReg = pNew;
    }
    return SNMP_SUCCESS;
}

INT4
SNMPRegisterMib (tSNMP_OID_TYPE * MibID, tMibData * pMibData,
                 tSnmpMsrBool SnmpMsrTgr)
{
    return (SNMPRegisterMibWithContextIdAndLock (MibID, pMibData, NULL,
                                                 NULL, NULL, NULL, SnmpMsrTgr));
}

/************************************************************************
 *  Function Name   : SNMPRegisterMib 
 *  Description     : Function to register a protocol to agent db
 *  Input           : MibID - Protocol oid
 *                    pMibData - protocol db pointer  
 *  Output          : None
 *  Returns         : Success or Failure
 ************************************************************************/
INT4
SNMPRegisterMibWithLock (tSNMP_OID_TYPE * MibID, tMibData * pMibData,
                         INT4 (*pLockPointer) (VOID),
                         INT4 (*pUnlockPointer) (VOID), tSnmpMsrBool SnmpMsrTgr)
{
    return (SNMPRegisterMibWithContextIdAndLock (MibID, pMibData, pLockPointer,
                                                 pUnlockPointer, NULL, NULL,
                                                 SnmpMsrTgr));
}

/************************************************************************
 *  Function Name   : SNMPRegisterMibWithContextIdAndLock 
 *  Description     : Function to register a protocol to agent db
 *  Input           : MibID - Protocol oid
 *                    pMibData - protocol db pointer 
 *                    pLockPointer - fucntion pointer to take the protocol lock.
 *                    pUnLockPointer - fucntion pointer to release
 *                                     the protocol lock. 
 *                    pSetContextPointer - function pointer to configure 
 *                                         the context Id                 
 *                    pSetContextPointer - function pointer to release the 
 *                                         configured context Id                
 *  Output          : None
 *  Returns         : Success or Failure
 ************************************************************************/
INT4
SNMPRegisterMibWithContextIdAndLock (tSNMP_OID_TYPE * MibID,
                                     tMibData * pMibData,
                                     INT4 (*pLockPointer) (VOID),
                                     INT4 (*pUnlockPointer) (VOID),
                                     INT4 (*pSetContextPointer) (UINT4),
                                     VOID (*pReleaseContextPointer) (VOID),
                                     tSnmpMsrBool SnmpMsrTgr)
{
    UINT4               u4Count = 0, u4Total = 0;
    UINT4               u4NewCount = 0, u4Match = 0;
    UINT4               u4Index = 0;
    INT4                i4Result = SNMP_FAILURE;
    tSNMP_OID_TYPE     *pNewOid = NULL;
    UINT4               u4MibLen = 0;
    tMibData           *pNew = NULL, *pCur = pMibData;
    UINT1               au1Oid[SNMP_MAX_OID_LENGTH];

    if ((MibID == NULL) || (pCur == NULL) ||
        (pCur->u4Total == SNMP_ZERO) || (pCur->pMibEntry == NULL))
    {
        return SNMP_FAILURE;
    }
    u4MibLen = MibID->u4_Length;
    u4Total = pMibData->u4Total;
    pNew = MibDataAlloc (&u4Index);
    if (pNew == NULL)
    {
        UtlTrcLog (1, 1, "SNMP", "Mib Data Allocation Failed \n");
        return SNMP_FAILURE;
    }
    pNewOid = OidAlloc (u4Index);
    if (pNewOid == NULL)
    {
        SNMPTrace ("OID Allocation Fails\n");
        return SNMP_FAILURE;
    }
    SNMPOIDCopy (pNewOid, MibID);
    pNew->pMibEntry = pCur->pMibEntry;
    pNew->u4Total = SNMP_ZERO;
    while (u4Count < u4Total)
    {
        i4Result = (INT4) OIDCompare (*pNewOid,
                                      (pCur->pMibEntry[u4NewCount].ObjectID),
                                      &u4Match);
        if (u4Match == pNewOid->u4_Length)
        {
            u4NewCount++;
        }
        else
        {
            if (u4NewCount == 0)
            {
                WebnmConvertOidToString (pNewOid, au1Oid);
                UtlTrcLog (SNMP_TRUE, SNMP_TRUE, "SNMP",
                           "Error Registration of OID %s failed "
                           "as the MibEntry differs with baseoid registered\n",
                           au1Oid);
            }
            pNew->u4Total = u4NewCount;
            i4Result = AttachMib (pNewOid, pNew, pLockPointer, pUnlockPointer,
                                  pSetContextPointer, pReleaseContextPointer,
                                  SnmpMsrTgr, u4Index);
            if (i4Result == SNMP_FAILURE)
            {
                SNMPTrace ("Error in registering\n");
                return SNMP_FAILURE;
            }
            pNew = MibDataAlloc (&u4Index);
            if (pNew == NULL)
            {
                SNMPTrace ("Allocation failed for Mib Data\r\n");
                return SNMP_FAILURE;
            }

            pNewOid = OidAlloc (u4Index);

            if (pNewOid == NULL)
            {
                SNMPTrace ("Allocation failed for OID\r\n");
                return SNMP_FAILURE;
            }
            pNew->u4Total = SNMP_ZERO;
            pNew->pMibEntry = &(pCur->pMibEntry[u4NewCount]);
            if (u4Total == u4Count + SNMP_ONE)
            {
                pNew->u4Total = SNMP_ONE;
                SNMPOIDCopy (pNewOid, &(pCur->pMibEntry[u4NewCount].ObjectID));
                i4Result = AttachMib (pNewOid, pNew, pLockPointer,
                                      pUnlockPointer, pSetContextPointer,
                                      pReleaseContextPointer, SnmpMsrTgr,
                                      u4Index);
                if (i4Result == SNMP_FAILURE)
                {
                    SNMPTrace ("Error in registering\n");
                    return SNMP_FAILURE;
                }
                return SNMP_SUCCESS;
            }
            OIDCompare (pCur->pMibEntry[u4NewCount].ObjectID,
                        pCur->pMibEntry[u4NewCount + SNMP_ONE].ObjectID,
                        &u4Match);
            SNMPOIDCopy (pNewOid, &(pCur->pMibEntry[u4NewCount].ObjectID));
            if (u4Match == SNMP_ZERO)
            {
                SNMPTrace ("Error in DB\n");
                return SNMP_FAILURE;
            }
            if (u4Match < u4MibLen)
            {

                u4MibLen = pNewOid->u4_Length = u4Match;
            }
            else
            {
                pNewOid->u4_Length = u4MibLen;
            }
            u4NewCount = SNMP_ONE;
            pCur = pNew;
        }
        u4Count++;
    }
    pNew->u4Total = u4NewCount;
    i4Result = AttachMib (pNewOid, pNew, pLockPointer, pUnlockPointer,
                          pSetContextPointer, pReleaseContextPointer,
                          SnmpMsrTgr, u4Index);
    if (i4Result == SNMP_FAILURE)
    {
        SNMPTrace ("Error in registering\n");
        return SNMP_FAILURE;
    }
    SnmpSendTrap (SNMP_MIB_REG_TRAP, (INT1 *) SNMP_FS_TRAP_OID,
                  SNMP_FS_TRAP_OID_LEN, &gSnmpSystem.u2ListenAgentPort);
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : MibDataAlloc 
 *  Description     : Function to allocate data pointer for db register
 *  Input           : pu4Index - Index to notify mib entry
 *  Output          : None
 *  Returns         : Data pointer or null
 ************************************************************************/
tMibData           *
MibDataAlloc (UINT4 *pu4Index)
{

    if (IndexManagerSetNextFreeIndex (gMibRegIndexMgrId,
                                      pu4Index) == INDEX_FAILURE)
    {
        return NULL;
    }
    return (&gaSnmpMibData[*pu4Index]);
}

/************************************************************************
 *  Function Name   : OidAlloc 
 *  Description     : Function to allocate oid
 *  Input           : u4Index - Index to notify Mib Entry
 *  Output          : None 
 *  Returns         : Oid pointer or null 
 ************************************************************************/
tSNMP_OID_TYPE     *
OidAlloc (UINT4 u4Index)
{
    tSNMP_OID_TYPE     *pOid = NULL;

    pOid = (tSNMP_OID_TYPE *) (VOID *) gau1SnmpOidType[u4Index];
    pOid->pu4_OidList = (UINT4 *) (VOID *) ((UINT1 *) pOid +
                                            sizeof (tSNMP_OID_TYPE));
    return pOid;
}

/************************************************************************
 *  Function Name   : GetMbDbEntry 
 *  Description     : This function searches for the given OID in the given
 *                    Mib database and if avalaible returns that entry along
 *                    with the next entry
 *  Input           : ObjectID - Oid Pointer
 *                    u4Count - match length                        
 *  Output          : pNext - Next Mbdb Pointer
 *  Returns         : Mbdb pointer or null 
 ************************************************************************/
VOID
GetMbDbEntry (tSNMP_OID_TYPE ObjectID, tMbDbEntry ** pNext, UINT4 *u4Count,
              tSnmpDbEntry * pSnmpDbEntry)
{
    tMibReg            *pMibPtr = gpMibReg, *pNew = NULL;
    INT4                i4Result = 0;
    UINT4               u4PreCount = 0, u4Status;
    tMbDbEntry         *pCur = NULL;

    *u4Count = 0;

    MEMSET (pSnmpDbEntry, 0, sizeof (tSnmpDbEntry));

    SNMP_DB_LOCK ();
    if ((gpLastAccessMibPtr != NULL) &&
        (MEMCMP (ObjectID.pu4_OidList,
                 gpLastAccessMibPtr->pMibID->pu4_OidList,
                 (sizeof (UINT4) * gpLastAccessMibPtr->pMibID->u4_Length)) ==
         0))
    {
        pNew = gpLastAccessMibPtr;
        SNMP_DB_UNLOCK ();
    }
    else
    {
        SNMP_DB_UNLOCK ();
/*------Get the Best MIB Match----------------*/
        while (pMibPtr != NULL)
        {
            i4Result =
                (INT4) (OIDCompare (ObjectID, *(pMibPtr->pMibID), u4Count));
            if ((i4Result == SNMP_EQUAL)
                || ((i4Result == SNMP_LESSER)
                    && (*u4Count == ObjectID.u4_Length)))
            {
                pNew = pMibPtr;
                break;
            }
            if ((i4Result == SNMP_LESSER)
                && (*u4Count < pMibPtr->pMibID->u4_Length))
            {
                break;
            }
            if ((*u4Count == pMibPtr->pMibID->u4_Length)
                && (*u4Count > u4PreCount))
            {
                pNew = pMibPtr;
                u4PreCount = *u4Count;
            }
            pMibPtr = pMibPtr->pNextMib;
            *u4Count = SNMP_ZERO;
        }
    }
/*--------------No Matching entry --------------------
    pNew is NULL as there is no matching entry.
    if pMibPtr is Null it has reached the last entry
    in the search..
    Else it has stopped in between because the Mib OID 
    is greater than the given OID.
    So, the fucntion should return NULL
    and pNext entry would the position where 
    it has stopped.
    If it has searched completely the r *pNext = NULL
    else current ptr -> First Entry
---------------------------------------------------------*/
    if (pNew == NULL)
    {
        /*--The OID is greater than all MIB Elements registered---*/
        if (pMibPtr == NULL)
        {
            pSnmpDbEntry->pMibEntry = NULL;
            return;
        }
    /*--The OID is does not match but within range------------*/
        else
        {
            *pNext = pMibPtr->pCurMib->pMibEntry;

            pSnmpDbEntry->pMibEntry = NULL;
            pSnmpDbEntry->SnmpMsrTgr = pMibPtr->SnmpMsrTgr;
            pSnmpDbEntry->pNextLockPointer = pMibPtr->pLockPointer;
            pSnmpDbEntry->pNextUnlockPointer = pMibPtr->pUnlockPointer;
            pSnmpDbEntry->pNextSetContextPointer = pMibPtr->pSetContextPointer;
            pSnmpDbEntry->pNextReleaseContextPointer =
                pMibPtr->pReleaseContextPointer;
            return;
        }
    }
       /*----Atleast one/Best  Matching Mib-----------------------
    if there is Match, Search the Mib Table for the OID
    and return the reult of Search function
    if the OID matches with the last the value returned
    will be NULL;
    Hence in this case the first entry of the next Mib Table
    is assigned to pNext
    ----------------------------------------------------*/
    else
    {
        pMibPtr = pNew;

        pSnmpDbEntry->pNextLockPointer = pMibPtr->pLockPointer;
        pSnmpDbEntry->pNextUnlockPointer = pMibPtr->pUnlockPointer;
        pSnmpDbEntry->pNextSetContextPointer = pMibPtr->pSetContextPointer;
        pSnmpDbEntry->pNextReleaseContextPointer =
            pMibPtr->pReleaseContextPointer;
        pCur = OIDSearch (pMibPtr->pCurMib, ObjectID, &u4Status, pNext,
                          u4Count);
        if (*pNext == NULL)
        {
            while (pMibPtr->pNextMib != NULL)
            {
                i4Result = (INT4) OIDCompare (ObjectID,
                                              *(pMibPtr->pNextMib->pMibID),
                                              u4Count);
                if (i4Result == SNMP_LESSER)
                {
                    pCur = OIDSearch (pMibPtr->pCurMib, ObjectID, &u4Status,
                                      pNext, u4Count);
                    if (*pNext == NULL)
                    {
                        *pNext = pMibPtr->pNextMib->pCurMib->pMibEntry;
                    }
                    pSnmpDbEntry->pNextLockPointer =
                        pMibPtr->pNextMib->pLockPointer;
                    pSnmpDbEntry->pNextUnlockPointer =
                        pMibPtr->pNextMib->pUnlockPointer;
                    pSnmpDbEntry->pNextSetContextPointer =
                        pMibPtr->pNextMib->pSetContextPointer;
                    pSnmpDbEntry->pNextReleaseContextPointer =
                        pMibPtr->pNextMib->pReleaseContextPointer;
                    break;
                }
                pMibPtr = pMibPtr->pNextMib;
            }
        }
        pSnmpDbEntry->pLockPointer = pMibPtr->pLockPointer;
        pSnmpDbEntry->pUnlockPointer = pMibPtr->pUnlockPointer;
        pSnmpDbEntry->pSetContextPointer = pMibPtr->pSetContextPointer;
        pSnmpDbEntry->pReleaseContextPointer = pMibPtr->pReleaseContextPointer;
        pSnmpDbEntry->SnmpMsrTgr = pMibPtr->SnmpMsrTgr;
        pSnmpDbEntry->pMibEntry = pCur;
        SNMP_DB_LOCK ();
        gpLastAccessMibPtr = pMibPtr;
        SNMP_DB_UNLOCK ();
        return;
    }
}

/************************************************************************
 *  Function Name   : SNMPUnRegisterMib 
 *  Description     : Function to unregister a protocol from agent db
 *  Input           : MibID - Protocol oid
 *                    pMibData - protocol db pointer  
 *  Output          : None
 *  Returns         : Success or Failure
 ************************************************************************/

INT4
SNMPUnRegisterMib (tSNMP_OID_TYPE * MibID, tMibData * pMibData)
{
    tMbDbEntry         *pStartPtr = NULL, *pEndPtr = NULL;
    tMibReg            *pMibPtr = gpMibReg;
    tMibReg            *pPrevMibPtr = NULL;
    tMibReg            *pTempMibPtr = NULL;
    INT4                i4Return = SNMP_FAILURE;
    UINT4               u4MibRegIndexFreed;

    pStartPtr = pMibData->pMibEntry;
    pEndPtr = pMibData->pMibEntry + pMibData->u4Total;
    UNUSED_PARAM (MibID);
    while (pMibPtr != NULL)
    {
        if (pMibPtr->pCurMib->pMibEntry >= pStartPtr &&
            pMibPtr->pCurMib->pMibEntry <= pEndPtr)
        {

            if (pPrevMibPtr == NULL)
            {
                gpMibReg = pMibPtr->pNextMib;
            }
            else
            {
                pPrevMibPtr->pNextMib = pMibPtr->pNextMib;
            }
            /* gpLastAccessMibPtr is linked with pMibPtr.
             * So when the mib is unregistered , gpLastAccessMibPtr  
             * must be reset to avoid dangling effect
             */
            SNMP_DB_LOCK ();
            if (gpLastAccessMibPtr == pMibPtr)
            {
                gpLastAccessMibPtr = NULL;
            }
            SNMP_DB_UNLOCK ();

            pTempMibPtr = pMibPtr;
            pMibPtr = pMibPtr->pNextMib;

            u4MibRegIndexFreed = (UINT4) (pTempMibPtr - gaSnmpMibReg);

            if (IndexManagerFreeIndex (gMibRegIndexMgrId, u4MibRegIndexFreed)
                == INDEX_FAILURE)
            {
                SNMPTrace ("Unable to successfully indicate to index manager "
                           "while deregistering\n");
            }

            i4Return = SNMP_SUCCESS;
        }
        else
        {
            pPrevMibPtr = pMibPtr;
            pMibPtr = pMibPtr->pNextMib;
        }
    }

    SnmpSendTrap (SNMP_MIB_DEREG_TRAP, (INT1 *) SNMP_FS_TRAP_OID,
                  SNMP_FS_TRAP_OID_LEN, &gSnmpSystem.u2ListenAgentPort);
    return i4Return;
}

/************************************************************************
 *  Function Name   : SNMPGetMbDbEntry 
 *  Description     : This function is called by other modules which 
 *                    searches for the given OID in the given
 *                    Mib database and if avalaible returns that entry along
 *                    with the next entry
 *  Input           : ObjectID - Oid Pointer
 *                    u4Count - match length                        
 *  Output          : pNext - Next Mbdb Pointer
 *  Returns         : Mbdb pointer or null 
 ************************************************************************/
VOID
SNMPGetMbDbEntry (tSNMP_OID_TYPE ObjectID, tMbDbEntry ** pNext, UINT4 *pu4Count,
                  tMbDbEntry ** pMbDbEntry)
{
    tSnmpDbEntry        SnmpDbEntry;

    GetMbDbEntry (ObjectID, pNext, pu4Count, &SnmpDbEntry);

    *pMbDbEntry = SnmpDbEntry.pMibEntry;
}

/************************************************************************
 *  Function Name   : SNMPGetMbDbTgrValue
 *  Description     : This function searches for the given OID in the given
 *                    Mib database and if available returns the snmp trigger
 *                    registered for that OID.
 *  Input           : ObjectID - Oid Pointer
 *  Output          : pSnmpTgr - Trigger registered for the given oid.
 *  Returns         : SNMP_SUCCESS / SNMP_FAILURE
 ************************************************************************/
INT4
SNMPGetMbDbTgrValue (tSNMP_OID_TYPE ObjectID, BOOL1 * pbSnmpTgr)
{
    tMibReg            *pMibPtr = gpMibReg;
    tMibReg            *pBkpMibPtr = NULL; /* Backup ptr to hold the
                                              matching MIB OID from the 
                                              global MIB OID tree */
    UINT4               u4Count = 0;
    INT4                i4Result = 0;

    /* Get the parent entry registered in the snmp mib database for 
     * the given oid */

    while (pMibPtr != NULL)
    {
        i4Result = (INT4) OIDCompare (ObjectID, *(pMibPtr->pMibID), &u4Count);
        if (i4Result == SNMP_EQUAL)
        {
          /* set the obtained OID pattern in the bkp ptr */
          pBkpMibPtr = pMibPtr;
          break;
        }
        /* check till the reference OID length */
        else if ((i4Result == SNMP_GREATER) && 
                 (u4Count == pMibPtr->pMibID->u4_Length))
        {
          /* Compare until the utmost matching OID is found 
             & update bkp ptr with the obtained OID */
          if ((pBkpMibPtr == NULL)  || 
              (pBkpMibPtr->pMibID->u4_Length 
               < pMibPtr->pMibID->u4_Length))
          {
            pBkpMibPtr = pMibPtr;
          }
        }
        pMibPtr = pMibPtr->pNextMib;
    }
    pMibPtr = pBkpMibPtr;

    if (pMibPtr != NULL)
    {
        if (pMibPtr->SnmpMsrTgr == SNMP_MSR_TGR_TRUE)
        {
            *pbSnmpTgr = OSIX_TRUE;
        }
        else
        {
            *pbSnmpTgr = OSIX_FALSE;
        }
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/********************  END OF FILE   ***************************************/
