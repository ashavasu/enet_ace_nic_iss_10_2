/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: snxutl.c,v 1.4 2015/04/28 12:35:03 siva Exp $
 *
 * Declaration of private functions used in this file */
/*******************************************************************/
#include "snxinc.h"

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxUtlAllocOctetString  
 *                                                                          
 *    DESCRIPTION      : Allocates memory for an octet string
 *                        
 *    INPUT            : pOctetStr - pointer to an octetstring for which 
 *                                   memory is to be allocated
 *                         
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : pOctetStr or NULL
 ****************************************************************************/
tSNMP_OCTET_STRING_TYPE *
SnxUtlAllocOctetString (tSNMP_OCTET_STRING_TYPE * pOctetStr)
{

    UINT1               u1AllocFlag = OSIX_FALSE;
    if (pOctetStr == NULL)
    {
        pOctetStr = MemAllocMemBlk (gSnmpOctetStrPoolId);
        if (pOctetStr == NULL)
        {
            SNMPTrace ("Alloc for OctetString failed\n");
            return NULL;
        }
        MEMSET (pOctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        u1AllocFlag = OSIX_TRUE;
    }
    (pOctetStr)->pu1_OctetList = MemAllocMemBlk (gSnmpOctetListPoolId);
    if ((pOctetStr)->pu1_OctetList == NULL)
    {
        SNMPTrace ("Alloc for OctetString failed\n");
        if (u1AllocFlag == OSIX_TRUE)
        {
            MemReleaseMemBlock (gSnmpOctetStrPoolId, (UINT1 *) pOctetStr);
        }
        return NULL;
    }
    MEMSET ((pOctetStr)->pu1_OctetList, 0,
            sizeof (UINT1) * SNMP_MAX_OCTETSTRING_SIZE);
    return pOctetStr;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxUtlFreeOctetString  
 *                                                                          
 *    DESCRIPTION      : Releases the memory allocated to the octetstring 
 *                        
 *    INPUT            : pOctetStr - pointer to an octetstring 
 *                       u1RelFlag - indicates whether memory can be
 *                                   freed or not
 *                        
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 ****************************************************************************/
VOID
SnxUtlFreeOctetString (tSNMP_OCTET_STRING_TYPE * pOctetStr, UINT1 u1RelFlag)
{
    if (pOctetStr->pu1_OctetList != NULL)
    {
        MemReleaseMemBlock (gSnmpOctetListPoolId, (UINT1 *)
                            pOctetStr->pu1_OctetList);
        pOctetStr->pu1_OctetList = NULL;
    }
    if (u1RelFlag == OSIX_TRUE)
    {
        MemReleaseMemBlock (gSnmpOctetStrPoolId, (UINT1 *) pOctetStr);
        pOctetStr = NULL;
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxUtlAllocOid  
 *                                                                          
 *    DESCRIPTION      : Allocates memory for OID 
 *                        
 *    INPUT            : pOid - pointer to the OID 
 *                        
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : pOid or NULL 
 ****************************************************************************/
tSNMP_OID_TYPE     *
SnxUtlAllocOid (tSNMP_OID_TYPE * pOid)
{
    UINT1               u1AllocFlag = OSIX_FALSE;

    if (pOid == NULL)
    {
        pOid = (tSNMP_OID_TYPE *) MemAllocMemBlk (gSnmpOidTypePoolId);

        if (pOid == NULL)
        {
            SNMPTrace ("Alloc for OID failed\n");
            return NULL;
        }
        MEMSET (pOid, 0, sizeof (tSNMP_OID_TYPE));
        u1AllocFlag = OSIX_TRUE;
    }
    pOid->pu4_OidList = (UINT4 *) MemAllocMemBlk (gSnmpOidListPoolId);

    if (pOid->pu4_OidList == NULL)
    {
        SNMPTrace ("Alloc for OID List failed\n");
        if (u1AllocFlag == OSIX_TRUE)
        {
            MemReleaseMemBlock (gSnmpOidTypePoolId, (UINT1 *) pOid);
        }
        return NULL;
    }
    MEMSET (pOid->pu4_OidList, 0, sizeof (UINT4) * MAX_OID_LENGTH);
    return pOid;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxUtlFreeOid  
 *                                                                          
 *    DESCRIPTION      : Releases the memory allocated to the OID 
 *                        
 *    INPUT            : pOid - points to the OID 
 *                       u1RelFlag - indicates whether memory can be freed or not 
 *                       
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : None 
 ****************************************************************************/
VOID
SnxUtlFreeOid (tSNMP_OID_TYPE * pOid, UINT1 u1RelFlag)
{
    if (pOid->pu4_OidList != NULL)
    {
        MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *) pOid->pu4_OidList);
        pOid->pu4_OidList = NULL;
    }
    if (u1RelFlag == OSIX_TRUE)
    {
        MemReleaseMemBlock (gSnmpOidTypePoolId, (UINT1 *) pOid);
        pOid = NULL;
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxUtlAllocAgtxOid 
 *                                                                          
 *    DESCRIPTION      : Allocates the memory to the Agentx OID 
 *                        
 *    INPUT            : pAgxOid - pointer to the Agentx OID
 *                        
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : pAgxOid or NULL
 ****************************************************************************/
tSnmpAgentxOidType *
SnxUtlAllocAgtxOid (tSnmpAgentxOidType * pAgxOid)
{
    UINT1               u1AllocFlag = OSIX_FALSE;
    if (pAgxOid == NULL)
    {
        pAgxOid =
            (tSnmpAgentxOidType *) MemAllocMemBlk (gSnmpAgentxOidTypePoolId);
        if (pAgxOid == NULL)
        {
            SNMPTrace ("Alloc for VarBind failed\n");
            return NULL;
        }
        MEMSET (pAgxOid, 0, sizeof (tSnmpAgentxOidType));
        u1AllocFlag = OSIX_TRUE;
    }
    pAgxOid->pu4OidLst = (UINT4 *) MemAllocMemBlk (gSnmpOidListPoolId);
    if (pAgxOid->pu4OidLst == NULL)
    {
        SNMPTrace ("Alloc for VarBind failed\n");
        if (u1AllocFlag == OSIX_TRUE)
        {
            MemReleaseMemBlock (gSnmpAgentxOidTypePoolId, (UINT1 *) pAgxOid);

        }
        return NULL;
    }
    MEMSET (pAgxOid->pu4OidLst, 0, sizeof (UINT4) * MAX_OID_LENGTH);
    return pAgxOid;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxUtlFreeAgtxOid 
 *                                                                          
 *    DESCRIPTION      : Releases the memory allocated to Agentx OID
 *                        
 *    INPUT            : pAgxOid - pointer to the Agentx OID
 *                       u1RelFlag - indicates whether memory can be freed or not  
 *                         
 *                        
 *    OUTPUT           :  None
 *                                                                          
 *    RETURNS          :  None
 ****************************************************************************/

VOID
SnxUtlFreeAgtxOid (tSnmpAgentxOidType * pAgxOid, UINT1 u1RelFlag)
{
    if (pAgxOid->pu4OidLst != NULL)
    {
        MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *) pAgxOid->pu4OidLst);
        pAgxOid->pu4OidLst = NULL;
    }
    if (u1RelFlag == OSIX_TRUE)
    {
        MemReleaseMemBlock (gSnmpOidTypePoolId, (UINT1 *) pAgxOid);
        pAgxOid = NULL;
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxUtlAllocSrchRng 
 *                                                                          
 *    DESCRIPTION      : Allocates memory for search range 
 *                        
 *    INPUT            : pSearchRangeLst - points to the searchrange
 *                        
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : pSearchRangeLst  or NULL
 ****************************************************************************/
tSnmpSearchRng     *
SnxUtlAllocSrchRng (tSnmpSearchRng * pSearchRangeLst)
{
    UINT1               u1AllocFlag = OSIX_FALSE;

    if (pSearchRangeLst == NULL)
    {
        pSearchRangeLst = (tSnmpSearchRng *)
            MemAllocMemBlk (gSnmpSearchRngPoolId);

        if (pSearchRangeLst == NULL)
        {
            SNMPTrace ("Alloc for SearchRange List failed\n");
            return NULL;
        }
        MEMSET (pSearchRangeLst, 0, sizeof (tSnmpSearchRng));
        u1AllocFlag = OSIX_TRUE;
    }

    if (SnxUtlAllocAgtxOid (&pSearchRangeLst->StartId) == SNMP_FAILURE)
    {
        SNMPTrace ("Alloc for SearchRangeList.StartId failed\n");
        if (u1AllocFlag == OSIX_TRUE)
        {
            MemReleaseMemBlock (gSnmpSearchRngPoolId,
                                (UINT1 *) pSearchRangeLst);
        }
        return NULL;
    }
    if (SnxUtlAllocAgtxOid (&pSearchRangeLst->EndId) == SNMP_FAILURE)
    {
        SNMPTrace ("Alloc for SearchRangeList.EndId failed\n");
        SnxUtlFreeAgtxOid (&pSearchRangeLst->StartId, OSIX_FALSE);
        if (u1AllocFlag == OSIX_TRUE)
        {
            MemReleaseMemBlock (gSnmpSearchRngPoolId,
                                (UINT1 *) pSearchRangeLst);
        }
        return NULL;
    }
    return pSearchRangeLst;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxUtlFreeSrchRng 
 *                                                                          
 *    DESCRIPTION      : Releases the memory allocated to Search Range 
 *                        
 *    INPUT            : pSearchRange - points to the searchrange 
 *                       u1RelFlag - indicates whether memory can be freed or not                    
 *                        
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE 
 ****************************************************************************/
VOID
SnxUtlFreeSrchRng (tSnmpSearchRng * pSearchRange, UINT1 u1RelFlag)
{
    SnxUtlFreeAgtxOid (&pSearchRange->StartId, OSIX_FALSE);
    SnxUtlFreeAgtxOid (&pSearchRange->EndId, OSIX_FALSE);
    if (u1RelFlag == OSIX_TRUE)
    {
        MemReleaseMemBlock (gSnmpSearchRngPoolId, (UINT1 *) pSearchRange);
        pSearchRange = NULL;
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxUtlAllocAgtxVbLst 
 *                                                                          
 *    DESCRIPTION      : Allocates the memory allocated to VarBind list
 *                        
 *    INPUT            : pVarBindLst - points to the carbind 
 *                        
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : pVarBindLst or NULL
 ****************************************************************************/
tSnmpAgentxVarBind *
SnxUtlAllocVarbind (tSnmpAgentxVarBind * pAgxVb)
{
    UINT1               u1AllocFlag = OSIX_FALSE;

    if (pAgxVb == NULL)
    {
        pAgxVb = (tSnmpAgentxVarBind *)
            MemAllocMemBlk (gSnmpAgentxVarBindPoolId);
        if (pAgxVb == NULL)
        {
            SNMPTrace ("Alloc for VarBind List failed\n");
            return NULL;
        }
        MEMSET (pAgxVb, 0, sizeof (tSnmpAgentxVarBind));
        u1AllocFlag = OSIX_TRUE;
    }
    if (SnxUtlAllocAgtxOid (&pAgxVb->ObjName) == NULL)
    {
        SNMPTrace ("Alloc for pVarBindLst->ObjName  failed\n");
        if (u1AllocFlag == OSIX_TRUE)
        {
            MemReleaseMemBlock (gSnmpAgentxVarBindPoolId, (UINT1 *) pAgxVb);
        }
        return NULL;
    }
    pAgxVb->ObjValue.pOctetStrValue =
        SnxUtlAllocOctetString (pAgxVb->ObjValue.pOctetStrValue);
    if (pAgxVb->ObjValue.pOctetStrValue == NULL)

    {
        SNMPTrace
            ("Alloc for pVarBindLst->ObjValue.OctetString Value failed\n");
        SnxUtlFreeAgtxOid (&pAgxVb->ObjName, OSIX_FALSE);
        if (u1AllocFlag == OSIX_TRUE)
        {
            MemReleaseMemBlock (gSnmpAgentxVarBindPoolId, (UINT1 *) pAgxVb);
        }
        return NULL;
    }
    pAgxVb->ObjValue.pOidValue = SnxUtlAllocOid (pAgxVb->ObjValue.pOidValue);
    if (pAgxVb->ObjValue.pOidValue == NULL)
    {
        SNMPTrace ("Alloc for pVarBindLst->ObjValue.pOidValue failed\n");
        SnxUtlFreeAgtxOid (&pAgxVb->ObjName, OSIX_FALSE);
        if (pAgxVb->ObjValue.pOctetStrValue != NULL)
        {
            SnxUtlFreeOctetString (pAgxVb->ObjValue.pOctetStrValue, OSIX_TRUE);
            pAgxVb->ObjValue.pOctetStrValue = NULL;
        }
        if (u1AllocFlag == OSIX_TRUE)
        {
            MemReleaseMemBlock (gSnmpAgentxVarBindPoolId, (UINT1 *) pAgxVb);
        }
        return NULL;
    }
    return pAgxVb;

}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxUtlFreeVarbind 
 *                                                                          
 *    DESCRIPTION      : Releases the memory allocated to varbind 
 *                        
 *    INPUT            : pVarBindLst - points to the varbind 
 *                       u1RelFlag - indicates whether memory can be freed or not                    
 *                        
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : pVarBindLst or NULL
 ****************************************************************************/

VOID
SnxUtlFreeVarbind (tSnmpAgentxVarBind * pVarBindLst, UINT1 u1RelFlag)
{
    SnxUtlFreeAgtxOid (&pVarBindLst->ObjName, OSIX_FALSE);
    if (pVarBindLst->ObjValue.pOidValue != NULL)
    {
        SnxUtlFreeOid (pVarBindLst->ObjValue.pOidValue, OSIX_TRUE);
        pVarBindLst->ObjValue.pOidValue = NULL;
    }
    if (pVarBindLst->ObjValue.pOctetStrValue != NULL)
    {
        SnxUtlFreeOctetString (pVarBindLst->ObjValue.pOctetStrValue, OSIX_TRUE);
        pVarBindLst->ObjValue.pOctetStrValue = NULL;
    }
    if (u1RelFlag == OSIX_TRUE)
    {
        MemReleaseMemBlock (gSnmpAgentxVarBindPoolId, (UINT1 *) pVarBindLst);
        pVarBindLst = NULL;
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxUtlFreeVbList
 *                                                                          
 *    DESCRIPTION      : This function frees the variable bindings list.  
 *
 *    INPUT            : pVarBindLst  - Pointer to the Variable Bindings list
 *                                                                          
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE 
 *    
 ****************************************************************************/
VOID
SnxUtlFreeVbList (tSnmpAgentxVarBind * pVarBindLst)
{
    tSnmpAgentxVarBind *pVbList = pVarBindLst;
    tSnmpAgentxVarBind *pNextVbList = NULL;

    while (pVbList != NULL)
    {
        pNextVbList = pVbList->pNextVarBind;
        SnxUtlFreeVarbind (pVbList, OSIX_TRUE);
        pVbList = pNextVbList;
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxUtlFreeSrchRngLst
 *                                                                          
 *    DESCRIPTION      : This function frees the search Range list.  
 *
 *    INPUT            : pSearchRangeLst  - Pointer to the Search Range list
 *                                                                          
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE 
 *    
 ****************************************************************************/
VOID
SnxUtlFreeSrchRngLst (tSnmpSearchRng * pSearchRangeLst)
{
    tSnmpSearchRng     *pSearchRange = pSearchRangeLst;
    tSnmpSearchRng     *pNxtSearchRange = NULL;

    while (pSearchRange != NULL)
    {
        pNxtSearchRange = pSearchRange->pNextSearchRng;
        SnxUtlFreeSrchRng (pSearchRange, OSIX_TRUE);
        pSearchRange = pNxtSearchRange;
    }
}
