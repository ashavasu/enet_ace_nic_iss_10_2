/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved 
 *
 * $Id: snprxtrn.c,v 1.2 2017/11/20 13:11:26 siva Exp $ 
 *
 * Description: routines for SNMP Proxy Translations 
 *******************************************************************/
#include "snmpcmn.h"
#include "snprxtrn.h"

extern tTMO_SLL     gSnmpUsmSll;

/*********************************************************************
 *  Function Name : SNMPValidateTranslation
 *  Description   : This function validates the translation of a 
 *                  request/response.
 *  Parameter(s)  : i2PduType - Type of pdu.
 *                  u4PduVersion - Version of pdu. 
 *                  u4TargetVersion - Target version of pdu.
 *  Return Values : SNMP_SUCCESS or SNMP_FAILURE
 *********************************************************************/
INT1
SNMPValidateTranslation (INT2 i2PduType,
                         UINT4 u4PduVersion, UINT4 u4TargetVersion)
{
    INT1                i1_Return = SNMP_FAILURE;

    /* Request */
    if (SNMP_PROXY_IS_PDU_REQ (i2PduType))
    {
        if (u4PduVersion >= u4TargetVersion)
            i1_Return = SNMP_SUCCESS;
    }
    /* Response */
    else if (SNMP_PDU_TYPE_GET_RESPONSE == i2PduType)
    {
        if (u4PduVersion <= u4TargetVersion)
            i1_Return = SNMP_SUCCESS;
    }

    return i1_Return;
}

/*********************************************************************
 *  Function Name : SNMPGetCommunityString
 *  Description   : This function returns the community string using 
 *                  security name in the community table.
 *  Parameter(s)  : pSingleTargetOut - Target Parameter to which the 
 *                  request is to be forwarded.
 *  Return Values : Community Name or NULL
 *********************************************************************/
tSNMP_OCTET_STRING_TYPE *
SNMPGetCommunityString (tSNMP_OCTET_STRING_TYPE * pSingleTargetOut,
                        tSNMP_OCTET_STRING_TYPE * pContextEngineID)
{
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    tSnmpTgtParamEntry *pSnmpTgtParamsEntry = NULL;
    tCommunityMappingEntry *pCoummnityEntry = NULL;

    pSnmpTgtAddrEntry = SNMPGetTgtAddrEntry (pSingleTargetOut);
    if (pSnmpTgtAddrEntry == NULL)
    {
        return NULL;
    }

    pSnmpTgtParamsEntry =
        SNMPGetTgtParamEntry (&(pSnmpTgtAddrEntry->AddrParams));
    if (NULL == pSnmpTgtParamsEntry)
    {
        return NULL;
    }

    pCoummnityEntry =
        SNMPGetCommunityEntryFromSecNameContextID (&
                                                   (pSnmpTgtParamsEntry->
                                                    ParamSecName),
                                                   pContextEngineID);
    if (NULL == pCoummnityEntry)
    {
        return NULL;
    }

    return &(pCoummnityEntry->CommunityName);

}

/*********************************************************************
*  Function Name : SNMPCopyArrFromOctetStr
*  Description   : This f/n copies the tSNMP_OCTET_STRING_ARR from 
*                    tSNMP_OCTET_STRING_TYPE.
*  Parameter(s)  : pDestArr - Destination 
*                pSrcOct - Source
*  Return Values : None
*********************************************************************/
VOID
SNMPCopyArrFromOctetStr (tSNMP_OCTET_STRING_ARR * pDestArr,
                         tSNMP_OCTET_STRING_TYPE * pSrcOct)
{
    pDestArr->length = pSrcOct->i4_Length;

    MEMCPY ((VOID *) pDestArr->buffer,
            (const VOID *) pSrcOct->pu1_OctetList, pSrcOct->i4_Length);
}

/*********************************************************************
 *  Function Name : SNMPGetTargetVersion
 *  Description   : This function returns the target version using 
 *                        pSingleTargetOut .
 *  Parameter(s)  : pSingleTargetOut - Target Parameter to which the 
 *                  request is to be forwarded.
 *                pVersion - Target Version is returned in this poniter.
 *  Return Values : SNMP_SUCCESS or SNMP_FAILURE
 *********************************************************************/

INT1
SNMPGetTargetVersion (tSNMP_OCTET_STRING_TYPE * pSingleTargetOut,
                      UINT4 *pVersion)
{
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    tSnmpTgtParamEntry *pSnmpTgtParamsEntry = NULL;

    pSnmpTgtAddrEntry = SNMPGetTgtAddrEntry (pSingleTargetOut);
    if (pSnmpTgtAddrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pSnmpTgtParamsEntry =
        SNMPGetTgtParamEntry (&(pSnmpTgtAddrEntry->AddrParams));
    if (NULL == pSnmpTgtParamsEntry)
    {
        return SNMP_FAILURE;
    }

    if (SNMP_MPMODEL_V1 == pSnmpTgtParamsEntry->i2ParamMPModel)
    {
        *pVersion = VERSION1;
    }
    else if (SNMP_MPMODEL_V2C == pSnmpTgtParamsEntry->i2ParamMPModel)
    {
        *pVersion = VERSION2;
    }
    else if (SNMP_MPMODEL_V3 == pSnmpTgtParamsEntry->i2ParamMPModel)
    {
        *pVersion = VERSION3;
    }
    else
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*********************************************************************
 *  Function Name : SNMPProxyCopyOctetStringFromArr
 *  Description   : This function octet string from array stored 
 *                  in cache data.
 *  Parameter(s)  : pDestOct -  Destination octet string
 *                  pSrcArr -  Source array.
 *  Return Values : VOID
 *********************************************************************/

VOID
SNMPProxyCopyOctetStringFromArr (tSNMP_OCTET_STRING_TYPE * pDestOct,
                                 tSNMP_OCTET_STRING_ARR * pSrcArr)
{
    pDestOct->i4_Length = pSrcArr->length;

    MEMCPY ((VOID *) pDestOct->pu1_OctetList,
            (const VOID *) pSrcArr->buffer, pSrcArr->length);
}

/*********************************************************************
 *  Function Name : SNMPProxyHandler
 *  Description   : This function handles the messages which are to be
 *                  forwarded between SNMP Manager and Agent as Proxy.
 *  Parameter(s)  : pPdu - PDU Pointer
 *                  u4pduVersion - PDU Version
 *                  pSingleTargetOut - Target Parameter to which the 
 *                  request is to be forwarded.
 *                    pCacheData - Cache node in case of responses
 *                    pEncodedMsg - Pointer in which final encoded packet 
 *                  is returned.
 *                    pEncodedMsgLen - Length of the encoded packet
 *  Return Values : SNMP_SUCCESS or SNMP_FAILURE
 *********************************************************************/
INT1
SNMPProxyHandler (VOID *pPdu,
                  UINT4 u4pduVersion,
                  INT2 i2PduType,
                  tSNMP_OCTET_STRING_TYPE * pSingleTargetOut,
                  tSNMP_PROXY_CACHE * pCacheData,
                  UINT1 **pEncodedMsg, UINT4 *pEncodedMsgLen)
{
    UINT4               u4NewRequestID = SNMP_ZERO;
    UINT4               u4TargetVersion = SNMP_ZERO;
    VOID               *pOutputPdu = NULL;
    INT2                i2_TargetPduType = SNMP_ZERO;
    tSNMP_PROXY_CACHE  *pNewCacheNode = NULL;
    tSNMP_VAR_BIND     *pSrcVarBind = NULL;
    tSNMP_VAR_BIND     *pDestVarBind = NULL;
    tSNMP_PROXY_V1GBULK_CACHE *pGetBulkData = NULL;
    UINT4               u4TargetAddr = SNMP_ZERO;
    UINT4               u4TargetPort = SNMP_ZERO;
    UINT4               u4ResendToTargetAddr = SNMP_FALSE;
    tSNMP_NORMAL_PDU   *pNormalPdu = NULL;
    tSNMP_V3PDU        *pV3Pdu = NULL;

    if ((NULL == pPdu) || (NULL == pEncodedMsg) || (NULL == pEncodedMsgLen))
    {
        SNMP_TRC (SNMP_FAILURE_TRC, "SNMPProxyHandler: "
                  "[PROXY]: NULL data passed \r\n");
        return SNMP_FAILURE;
    }

    /*Get the target version in case of request */
    if (SNMP_PROXY_IS_PDU_REQ (i2PduType) && (pSingleTargetOut))
    {
        if (SNMP_FAILURE == SNMPGetTargetVersion (pSingleTargetOut,
                                                  &u4TargetVersion))
        {
            SNMP_TRC (SNMP_FAILURE_TRC, "SNMPProxyHandler: "
                      "[PROXY]: TgtAddrEntry or TgtParamEntry not found \r\n");
            if ((VERSION1 == u4pduVersion) || (VERSION2 == u4pduVersion))
            {
                pNormalPdu = (tSNMP_NORMAL_PDU *) pPdu;
                pNormalPdu->i4_ErrorStatus = SNMP_ERR_INCONSISTENT_VALUE;
            }
            else
            {
                pV3Pdu = (tSNMP_V3PDU *) pPdu;
                pV3Pdu->Normalpdu.i4_ErrorStatus = SNMP_ERR_INCONSISTENT_VALUE;
            }

            if (SNMP_FAILURE == SNMPProxyFormFailResp (pPdu, u4pduVersion,
                                                       pEncodedMsg,
                                                       pEncodedMsgLen))
            {
                return SNMP_FAILURE;
            }
            else
            {
                return SNMP_SUCCESS;
            }
        }
    }
    /* Response */
    else if (pCacheData)
    {
        u4TargetVersion = pCacheData->ProxyCmnCache.u4_RecVersion;
    }
    else
    {
        /* pSingleTargetOut or pCacheData is null */
        return SNMP_FAILURE;
    }

    /* Validate the translation from the source version to the
     * target version 
     */
    if (SNMP_SUCCESS !=
        SNMPValidateTranslation (i2PduType, u4pduVersion, u4TargetVersion))
    {
        /* In case of a response if the translation can not be done
         * then the data from the cache shall be removed and 
         * the response shall be dropped. This scenario should actually
         * never happen.
         */
        if (pCacheData)
            SNMPProxyCacheDeleteNode (pCacheData->ProxyCmnCache.
                                      u4_NewRequestID);
        SNMPTrace ("[PROXY]:Invalid translation \n");

        SNMP_TRC (SNMP_FAILURE_TRC, "SNMPProxyHandler: "
                  "[PROXY]: Invalid translation \r\n");
        if (SNMP_PROXY_IS_PDU_REQ (i2PduType))
        {
            if (SNMP_FAILURE == SNMPProxyFormFailResp (pPdu, u4pduVersion,
                                                       pEncodedMsg,
                                                       pEncodedMsgLen))
            {
                return SNMP_FAILURE;
            }
            else
            {
                return SNMP_SUCCESS;
            }
        }
        return SNMP_FAILURE;
    }

    if (SNMP_PROXY_IS_PDU_REQ (i2PduType))
    {
        /*Check for free node in cache for request message caching */
        if (SNMP_SUCCESS != SNMPProxyCacheCheckFreeNode ())
        {
            SNMP_TRC (SNMP_FAILURE_TRC, "SNMPProxyHandler: "
                      "[PROXY]: Cache Exhausted \r\n");
            if (SNMP_FAILURE == SNMPProxyFormFailResp (pPdu, u4pduVersion,
                                                       pEncodedMsg,
                                                       pEncodedMsgLen))
            {
                return SNMP_FAILURE;
            }
            else
            {
                return SNMP_SUCCESS;
            }

        }

        u4NewRequestID = SNMPGetRequestID ();
    }

    /* Call the translation handler */
    if ((SNMP_SUCCESS !=
         SNMPProxyTranslation (pPdu, &pOutputPdu, u4pduVersion, u4TargetVersion,
                               i2PduType, &i2_TargetPduType, pSingleTargetOut,
                               u4NewRequestID, pCacheData)
         || (NULL == pOutputPdu)))
    {
        /* Translation failure - Remove the data from the cache if any */
        if (pCacheData)
            SNMPProxyCacheDeleteNode (pCacheData->ProxyCmnCache.
                                      u4_NewRequestID);
        if (pOutputPdu)
            SNMPProxyFreePdu (pOutputPdu, (INT4) u4TargetVersion,
                              i2_TargetPduType);

        SNMP_TRC (SNMP_FAILURE_TRC, "SNMPProxyHandler: "
                  "[PROXY]:  Translation failure\r\n");
        return SNMP_FAILURE;
    }

    /* Cases when data is to be added in cache -
     * 1) Fresh Request is received and is translated to a request -
     *      Request shall be cached with the new request id as the key
     * 2) Response is received and is translated to a new request -
     *      Old cache entry shall be deleted and the data with the new 
     *      request id and new var bind list shall be saved in a new entry 
     *       in the cache.
     */

    /* Case 1 */
    if ((SNMP_PROXY_IS_PDU_REQ (i2PduType)) &&
        (SNMP_PROXY_IS_PDU_REQ (i2_TargetPduType)))
    {
        if (SNMP_FAILURE ==
            SNMPProxyFillCacheData (pPdu, u4pduVersion, i2PduType, pOutputPdu,
                                    u4TargetVersion, i2_TargetPduType,
                                    SNMPGetManagerAddr (),
                                    SNMPGetManagerPort (), SNMP_ZERO,
                                    &pNewCacheNode))
        {
            /* Free the translated PDU */
            SNMPProxyFreePdu (pOutputPdu, (INT4) u4TargetVersion,
                              i2_TargetPduType);
            return SNMP_FAILURE;
        }

    }
    /* Case 2 */
    else if ((SNMP_PDU_TYPE_GET_RESPONSE == i2PduType) &&
             (SNMP_PROXY_IS_PDU_REQ (i2_TargetPduType)))
    {
        pNewCacheNode = SNMPProxyCacheGetFreeNode ();
        /* Cache exhausted */
        if (NULL == pNewCacheNode)
        {
            if (pCacheData)
            {
                SNMPProxyCacheDeleteNode (pCacheData->ProxyCmnCache.
                                          u4_RecRequestID);
            }

            SNMPProxyFreePdu (pOutputPdu, (INT4) u4TargetVersion,
                              i2_TargetPduType);
            return SNMP_FAILURE;
        }

        if (SNMP_FAILURE == SNMPProxyCacheCopyData (pNewCacheNode, pCacheData))
        {
            /* Free the node again as this could not be 
               added in the cache */
            SNMPProxyCacheSetFreeNode (pNewCacheNode);
            if (pCacheData)
                SNMPProxyCacheDeleteNode (pNewCacheNode->ProxyCmnCache.
                                          u4_RecRequestID);
            SNMPProxyFreePdu (pOutputPdu, (INT4) u4TargetVersion,
                              i2_TargetPduType);
            return SNMP_FAILURE;
        }

        /* Fill var bind list in pNewCacheNode from pOutputPdu */
        /* Copy var bind list */
        pSrcVarBind = ((tSNMP_NORMAL_PDU *) pOutputPdu)->pVarBindList;
        pGetBulkData = &pNewCacheNode->ProxyCmnCache.ProxyV1GbulkCache;
        while (NULL != pSrcVarBind)
        {
            pDestVarBind = SNMPCopyVarBind (pSrcVarBind);
            if (NULL == pDestVarBind)
            {
                SNMPProxyCacheDeleteNode (pNewCacheNode->ProxyCmnCache.
                                          u4_NewRequestID);
                SNMPProxyFreePdu (pOutputPdu, (INT4) u4TargetVersion,
                                  i2_TargetPduType);
                return SNMP_FAILURE;
            }
            if (pGetBulkData->pVarBindList == NULL)
            {
                pGetBulkData->pVarBindList = pDestVarBind;
                pGetBulkData->pVarBindEnd = pDestVarBind;
            }
            else
            {
                pGetBulkData->pVarBindEnd->pNextVarBind = pDestVarBind;
                pGetBulkData->pVarBindEnd = pDestVarBind;
            }
            pDestVarBind = NULL;
            pSrcVarBind = pSrcVarBind->pNextVarBind;
        }
        pNewCacheNode->ProxyCmnCache.u1_ReqResendFlag = SNMP_TRUE;
        u4TargetVersion = VERSION1;
        pNewCacheNode->ProxyCmnCache.u4_NewRequestID =
            ((tSNMP_NORMAL_PDU *) pOutputPdu)->u4_RequestID;

        /* Delete the previous node */
        SNMPProxyCacheDeleteNode (pCacheData->ProxyCmnCache.u4_NewRequestID);

        /* Add the new node in the cache */
        if (SNMP_FAILURE == SNMPProxyCacheAddNode (pNewCacheNode))
        {
            /* Add the node to the free list and free its memory */
            SNMPProxyCacheSetFreeNode (pNewCacheNode);
            SNMPProxyCacheFreeVarbind (pNewCacheNode);

            SNMPProxyFreePdu (pOutputPdu, (INT4) u4TargetVersion,
                              i2_TargetPduType);
            return SNMP_FAILURE;
        }
        /*Set the agent address to send the request again */

        u4TargetAddr = SNMPGetManagerAddr ();
        SNMPSetManagerAddr (u4TargetAddr);
        SNMPSetManagerPort (SNMP_PORT);

        u4ResendToTargetAddr = SNMP_TRUE;

    }

    /* Encode the packet */
    if ((VERSION1 == u4TargetVersion) || (VERSION2 == u4TargetVersion))
    {
        if (SNMP_NOT_OK == SNMPEncodeGetPacket ((tSNMP_NORMAL_PDU *) pOutputPdu,
                                                pEncodedMsg,
                                                (INT4 *) pEncodedMsgLen,
                                                (UINT4)
                                                (SNMP_PDU_GET_RESPONSE_MASK &
                                                 ((tSNMP_NORMAL_PDU *)
                                                  pOutputPdu)->i2_PduType)))
        {
            if (pNewCacheNode)
                SNMPProxyCacheDeleteNode (pNewCacheNode->ProxyCmnCache.
                                          u4_NewRequestID);
            else if (pCacheData)
                SNMPProxyCacheDeleteNode (pCacheData->ProxyCmnCache.
                                          u4_NewRequestID);

            SNMPProxyFreePdu (pOutputPdu, (INT4) u4TargetVersion,
                              i2_TargetPduType);
            return SNMP_FAILURE;
        }
    }
    else
    {
        /* Same function can be used for encoding request also */
        if (SNMP_NOT_OK == SNMPEncodeV3Message ((tSNMP_V3PDU *) pOutputPdu,
                                                pEncodedMsg,
                                                (INT4 *) pEncodedMsgLen,
                                                (UINT4)
                                                (SNMP_PDU_GET_RESPONSE_MASK &
                                                 ((tSNMP_V3PDU *) pOutputPdu)->
                                                 Normalpdu.i2_PduType)))
        {
            if (pNewCacheNode)
                SNMPProxyCacheDeleteNode (pNewCacheNode->ProxyCmnCache.
                                          u4_NewRequestID);
            else if (pCacheData)
                SNMPProxyCacheDeleteNode (pCacheData->ProxyCmnCache.
                                          u4_NewRequestID);

            SNMPProxyFreePdu (pOutputPdu, (INT4) u4TargetVersion,
                              i2_TargetPduType);
            return SNMP_FAILURE;
        }
    }
    /* Free the translated PDU memory after encoding */
    SNMPProxyFreePdu (pOutputPdu, (INT4) u4TargetVersion, i2_TargetPduType);

    /* Response */
    if (SNMP_PDU_TYPE_GET_RESPONSE == i2_TargetPduType)
    {
        u4TargetAddr = pCacheData->ProxyCmnCache.u4_TargetInAddr;
        u4TargetPort = pCacheData->ProxyCmnCache.u4_TargetInPort;

        SNMPSetManagerAddr (u4TargetAddr);
        SNMPSetManagerPort (u4TargetPort);

        /*Delete the Cache node */
        SNMPProxyCacheDeleteNode (pCacheData->ProxyCmnCache.u4_NewRequestID);
    }
    /* Request */
    else
    {
        tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;

        if (SNMP_FALSE == u4ResendToTargetAddr)
        {
            if (NULL == pSingleTargetOut)
                return SNMP_FAILURE;

            pSnmpTgtAddrEntry = SNMPGetTgtAddrEntry (pSingleTargetOut);
            if (pSnmpTgtAddrEntry == NULL)
            {
                if (pNewCacheNode)
                    SNMPProxyCacheDeleteNode (pNewCacheNode->ProxyCmnCache.
                                              u4_NewRequestID);
                return SNMP_FAILURE;
            }

            MEMCPY (&u4TargetAddr,
                    pSnmpTgtAddrEntry->Address.pu1_OctetList, sizeof (UINT4));

            u4TargetAddr = OSIX_NTOHL (u4TargetAddr);

            SNMPSetManagerAddr (u4TargetAddr);
            SNMPSetManagerPort (SNMP_PORT);
        }
        else
        {
            pSnmpTgtAddrEntry = SnmpGetManagerTargetAddr (u4TargetAddr);

        }
        /*If i2_TargetPduType Request then Start timer */
        if (SNMPStartProxyRequestTimer
            (pNewCacheNode, pSnmpTgtAddrEntry) != SNMP_SUCCESS)
        {
            SNMP_TRC (SNMP_FAILURE_TRC, "SNMPProxyHandler: "
                      "[PROXY]: Unable to start prnoxy timer\r\n");
            if (pNewCacheNode)
                SNMPProxyCacheDeleteNode (pNewCacheNode->ProxyCmnCache.
                                          u4_NewRequestID);

        }
    }

    return SNMP_SUCCESS;
}

/*********************************************************************
 *  Function Name : SNMPProxyTranslation
 *  Description   : This function handles the translations of messages
 *                  based on different SNMP versions.
 *  Parameter(s)  : pPdu - PDU Pointer
 *                  u4pduVersion - PDU Version
 *                  pSingleTargetOut - Target Parameter to which the 
 *                     request is to be forwarded.
 *                  ppOutputPdu - Output pdu request is to be forwarded.
 *                  pTargetPduType - Pdu type of target which has to 
 *                     be sent after translation.
 *                  i4_TargetVersion - Target version after translations.
 *                    u4RequestID - New Request ID passed in case of request
 *                    pCacheData - Cache node passed in case of responses
 *  Return Values : SNMP_SUCCESS or SNMP_FAILURE
 *********************************************************************/
INT1
SNMPProxyTranslation (VOID *pPdu,
                      VOID **ppOutputPdu,
                      UINT4 u4PduVersion,
                      UINT4 u4TargetVersion,
                      INT2 i2PduType,
                      INT2 *pTargetPduType,
                      tSNMP_OCTET_STRING_TYPE * pSingleTargetOut,
                      UINT4 u4RequestID, tSNMP_PROXY_CACHE * pCacheData)
{
    tSNMP_TRAP_PDU     *pv1TrapPdu = NULL;
    tSNMP_NORMAL_PDU   *pOutNormalPdu = NULL;
    tSNMP_V3PDU        *pOutV3Pdu = NULL;
    VOID               *pOutPdu = NULL;

    if (NULL == pPdu)
    {
        return SNMP_FAILURE;
    }

    switch (i2PduType)
    {
            /* request translation */
        case SNMP_PDU_TYPE_GET_REQUEST:
        case SNMP_PDU_TYPE_GET_NEXT_REQUEST:
        case SNMP_PDU_TYPE_SET_REQUEST:
        case SNMP_PDU_TYPE_GET_BULK_REQUEST:
        {
            if ((VERSION3 == u4PduVersion) && (VERSION2 == u4TargetVersion))
            {
                pOutNormalPdu = SNMPV3RequesttoV2Request ((tSNMP_V3PDU *) pPdu,
                                                          pSingleTargetOut,
                                                          u4RequestID);
            }
            else if ((VERSION2 == u4PduVersion) &&
                     (VERSION1 == u4TargetVersion))
            {
                pOutNormalPdu = SNMPV2RequesttoV1Request ((tSNMP_NORMAL_PDU *)
                                                          pPdu,
                                                          pSingleTargetOut,
                                                          u4RequestID);
            }
            else if ((VERSION3 == u4PduVersion) &&
                     (VERSION1 == u4TargetVersion))
            {
                pOutNormalPdu = SNMPV3RequesttoV1Request ((tSNMP_V3PDU *) pPdu,
                                                          pSingleTargetOut,
                                                          u4RequestID);
            }
            else if (((VERSION1 == u4PduVersion) &&
                      (VERSION1 == u4TargetVersion)) ||
                     ((VERSION2 == u4PduVersion) &&
                      (VERSION2 == u4TargetVersion)))

            {
                pOutNormalPdu = SNMPV1V2RequesttoV1V2Request ((tSNMP_NORMAL_PDU
                                                               *) pPdu,
                                                              u4PduVersion,
                                                              pSingleTargetOut,
                                                              u4RequestID);
            }
            else if ((VERSION3 == u4PduVersion) &&
                     (VERSION3 == u4TargetVersion))
            {
                pOutV3Pdu = SNMPV3RequesttoV3Request ((tSNMP_V3PDU *) pPdu,
                                                      pSingleTargetOut,
                                                      u4RequestID);
            }
            else
            {
                /* Incorrect translation */
                return SNMP_FAILURE;
            }
            break;
        }
            /* response translation */
        case SNMP_PDU_TYPE_GET_RESPONSE:
        {
            if ((VERSION1 == u4PduVersion) && (VERSION2 == u4TargetVersion))
            {
                pOutNormalPdu = SNMPV1ResponsetoV2Response ((tSNMP_NORMAL_PDU *)
                                                            pPdu, pCacheData);

            }
            else if ((VERSION2 == u4PduVersion) &&
                     (VERSION3 == u4TargetVersion))
            {
                pOutV3Pdu = SNMPV2ResponsetoV3Response ((tSNMP_NORMAL_PDU *)
                                                        pPdu, pCacheData);
            }
            else if ((VERSION1 == u4PduVersion) &&
                     (VERSION3 == u4TargetVersion))
            {
                pOutPdu = SNMPV1ResponsetoV3Response ((tSNMP_NORMAL_PDU *) pPdu,
                                                      pTargetPduType,
                                                      pCacheData);
            }
            else if (((VERSION1 == u4PduVersion) &&
                      (VERSION1 == u4TargetVersion)) ||
                     ((VERSION2 == u4PduVersion) &&
                      (VERSION2 == u4TargetVersion)))
            {
                pOutNormalPdu =
                    SNMPV1V2ResponsetoV1V2Response ((tSNMP_NORMAL_PDU *) pPdu,
                                                    u4PduVersion, pCacheData);

            }
            else if ((VERSION3 == u4PduVersion) &&
                     (VERSION3 == u4TargetVersion))
            {
                pOutV3Pdu = SNMPV3ResponsetoV3Response ((tSNMP_V3PDU *) pPdu,
                                                        pCacheData);
                if (NULL != pOutV3Pdu)
                {
                    /*Copy Engine Id from Agent */
                    SNMPCopyOctetString (&(pOutV3Pdu->MsgSecParam.MsgEngineID),
                                         &gSnmpEngineID);
                }
            }
            else
            {
                /* Incorrect translation */
                return SNMP_FAILURE;
            }
            break;
        }

            /* Notification Translation */
        case SNMP_PDU_TYPE_TRAP:
        case SNMP_PDU_TYPE_SNMPV2_TRAP:
        case SNMP_PDU_TYPE_V2_INFORM_REQUEST:
        {
            if ((VERSION1 == u4PduVersion) && (VERSION2 == u4TargetVersion))
            {
                pOutNormalPdu = SNMPV1TrapToV2Trap ((tSNMP_TRAP_PDU *) pPdu,
                                                    pSingleTargetOut,
                                                    u4RequestID, SNMP_TRUE);

            }
            else if ((VERSION1 == u4PduVersion) &&
                     (VERSION3 == u4TargetVersion))
            {
                pOutV3Pdu = SNMPV1TraptoV3Trap ((tSNMP_TRAP_PDU *) pPdu,
                                                pSingleTargetOut, u4RequestID);

            }
            else if ((VERSION2 == u4PduVersion) &&
                     (VERSION3 == u4TargetVersion))
            {
                pOutV3Pdu = SNMPV2TraptoV3Trap ((tSNMP_NORMAL_PDU *) pPdu,
                                                pSingleTargetOut, u4RequestID);

            }
            else if ((VERSION1 == u4PduVersion) &&
                     (VERSION1 == u4TargetVersion))
            {
                pv1TrapPdu = SNMPV1TraptoV1Trap ((tSNMP_TRAP_PDU *) pPdu,
                                                 pSingleTargetOut);
            }
            else if ((VERSION2 == u4PduVersion) &&
                     (VERSION2 == u4TargetVersion))
            {
                pOutNormalPdu = SNMPV2TraptoV2Trap ((tSNMP_NORMAL_PDU *) pPdu,
                                                    pSingleTargetOut,
                                                    u4RequestID);

            }
            else if ((VERSION3 == u4PduVersion) &&
                     (VERSION3 == u4TargetVersion))
            {
                pOutV3Pdu = SNMPV3TraptoV3Trap ((tSNMP_V3PDU *) pPdu,
                                                pSingleTargetOut, u4RequestID);

            }
            else if ((VERSION3 == u4PduVersion) &&
                     (VERSION2 == u4TargetVersion))
            {
                pOutNormalPdu = SNMPV3TraptoV2Trap ((tSNMP_V3PDU *) pPdu,
                                                    pSingleTargetOut,
                                                    u4RequestID);
            }
            else
            {
                /* Incorrect translation */
                return SNMP_FAILURE;
            }
            break;
        }
        default:
        {
            /* Incorrect i2PduType */
            return SNMP_FAILURE;
        }
    }

    if (NULL != pOutV3Pdu)
    {
        *ppOutputPdu = (VOID *) pOutV3Pdu;
        *pTargetPduType = pOutV3Pdu->Normalpdu.i2_PduType;
    }
    else if (NULL != pOutNormalPdu)
    {
        *ppOutputPdu = (VOID *) pOutNormalPdu;
        *pTargetPduType = pOutNormalPdu->i2_PduType;
    }
    else if (NULL != pOutPdu)
    {
        *ppOutputPdu = pOutPdu;
        /* pTargetPduType is already set in this case from the 
         * function SNMPV1ResponsetoV3Response
         */
    }
    else if (NULL != pv1TrapPdu)
    {
        *ppOutputPdu = (VOID *) pv1TrapPdu;
    }
    else
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*********************************************************************
 *  Function Name : SNMPV3RequesttoV2Request
 *  Description   : This function translates v3 request pdu to v2
 *                  request pdu.
 *  Parameter(s)  : pV3Pdu - PDU Pointer
 *                  pSingleTargetOut - Target Parameter to which the 
 *                          request is to be forwarded.
 *            u4RequestID - New Request ID
 *  Return Values : v2 Request PDU or NULL
 *********************************************************************/
tSNMP_NORMAL_PDU   *
SNMPV3RequesttoV2Request (tSNMP_V3PDU * pV3Pdu,
                          tSNMP_OCTET_STRING_TYPE * pSingleTargetOut,
                          UINT4 u4RequestID)
{
    tSNMP_NORMAL_PDU   *pV2Request = NULL;
    tSNMP_VAR_BIND     *pSrcVarBind = NULL;
    tSNMP_VAR_BIND     *pDestVarBind = NULL;
    tSNMP_OCTET_STRING_TYPE *pCommString = NULL;

    if ((NULL == pV3Pdu) || (NULL == pSingleTargetOut))
    {
        return NULL;
    }

    /* Allocate memory for the V2 pdu */
    pV2Request = MemAllocMemBlk (gSnmpNormalPduPoolId);
    if (pV2Request == NULL)
    {
        SNMPTrace ("[PROXY]:Unable to allocate Memory for pV2Request\n");
        return NULL;
    }
    MEMSET (pV2Request, SNMP_ZERO, sizeof (tSNMP_NORMAL_PDU));

    pV2Request->u4_Version = VERSION2;
    pV2Request->u4_RequestID = u4RequestID;

    if (SNMP_PDU_TYPE_GET_BULK_REQUEST != pV3Pdu->Normalpdu.i2_PduType)
    {
        pV2Request->i4_ErrorStatus = pV3Pdu->Normalpdu.i4_ErrorStatus;
        pV2Request->i4_ErrorIndex = pV3Pdu->Normalpdu.i4_ErrorIndex;
    }
    else
    {
        pV2Request->i4_NonRepeaters = pV3Pdu->Normalpdu.i4_NonRepeaters;
        pV2Request->i4_MaxRepetitions = pV3Pdu->Normalpdu.i4_MaxRepetitions;
    }
    pV2Request->i2_PduType = pV3Pdu->Normalpdu.i2_PduType;
    /* allocate community community String */
    pV2Request->pCommunityStr =
        allocmem_octetstring (SNMP_MAX_OCTETSTRING_SIZE);
    if (pV2Request->pCommunityStr == NULL)
    {
        SnmpFreeV2Request (pV2Request);
        SNMPTrace ("[PROXY]:Unable to allocate memory for Community String\n");
        return NULL;
    }

    pCommString = SNMPGetCommunityString (pSingleTargetOut, &pV3Pdu->ContextID);
    if (NULL == pCommString)
    {
        SnmpFreeV2Request (pV2Request);
        return NULL;
    }
    SNMPCopyOctetString (pV2Request->pCommunityStr, pCommString);

    /* Copy var bind list */
    pSrcVarBind = pV3Pdu->Normalpdu.pVarBindList;
    while (NULL != pSrcVarBind)
    {
        /* Copy var bind function calls form var bind f/n which takes memory for
         * each element in the var bind structure and copies the data
         */
        pDestVarBind = SNMPCopyVarBind (pSrcVarBind);
        if (NULL == pDestVarBind)
        {
            SnmpFreeV2Request (pV2Request);
            return NULL;
        }
        if (pV2Request->pVarBindList == NULL)
        {
            pV2Request->pVarBindList = pDestVarBind;
            pV2Request->pVarBindEnd = pDestVarBind;
        }
        else
        {
            if (pV2Request->pVarBindEnd != NULL)
            {
                pV2Request->pVarBindEnd->pNextVarBind = pDestVarBind;
            }
            pV2Request->pVarBindEnd = pDestVarBind;
        }
        pSrcVarBind = pSrcVarBind->pNextVarBind;
    }

    return pV2Request;
}

/*********************************************************************
 *  Function Name : SNMPV1V2RequesttoV1V2Request
 *  Description   : This function translates v1 request pdu to v1
 *                  request pdu as well as v2 request pdu to v2 request
 *                  pdu. It does proxy related translations required
 *                  for translations within same version.
 *  Parameter(s)  : pV2Pdu - PDU Pointer
 *                    u4PduVersion - V1 or V2
 *                  pSingleTargetOut - Target Parameter to which the 
 *                     request is to be forwarded.
 *                    u4RequestID - New Request ID
 *  Return Values : v1/v2 Request PDU or NULL
 *********************************************************************/
tSNMP_NORMAL_PDU   *
SNMPV1V2RequesttoV1V2Request (tSNMP_NORMAL_PDU * pV1V2Pdu,
                              UINT4 u4PduVersion,
                              tSNMP_OCTET_STRING_TYPE * pSingleTargetOut,
                              UINT4 u4RequestID)
{
    tSNMP_NORMAL_PDU   *pV1V2Request = NULL;
    tSNMP_VAR_BIND     *pSrcVarBind = NULL;
    tSNMP_VAR_BIND     *pDestVarBind = NULL;
    tSNMP_OCTET_STRING_TYPE *pCommString = NULL;
    tCommunityMappingEntry *pCommunityEntryIn = NULL;

    if ((NULL == pV1V2Pdu) || (NULL == pSingleTargetOut))
    {
        return NULL;
    }

    /* Allocate memory for the V2 pdu */
    pV1V2Request = MemAllocMemBlk (gSnmpNormalPduPoolId);
    if (pV1V2Request == NULL)
    {
        SNMPTrace ("[PROXY]:Unable to allocate Memory for pV1V2Request\n");
        return NULL;
    }
    MEMSET (pV1V2Request, SNMP_ZERO, sizeof (tSNMP_NORMAL_PDU));

    pV1V2Request->u4_Version = u4PduVersion;
    pV1V2Request->u4_RequestID = u4RequestID;

    /* In case non get bulk request, error status and error index shall be 
     * copied 
     */
    if (SNMP_PDU_TYPE_GET_BULK_REQUEST != pV1V2Pdu->i2_PduType)
    {
        pV1V2Request->i4_ErrorStatus = pV1V2Pdu->i4_ErrorStatus;
        pV1V2Request->i4_ErrorIndex = pV1V2Pdu->i4_ErrorIndex;
    }
    else
    {
        pV1V2Request->i4_NonRepeaters = pV1V2Pdu->i4_NonRepeaters;
        pV1V2Request->i4_MaxRepetitions = pV1V2Pdu->i4_MaxRepetitions;
    }

    pV1V2Request->i2_PduType = pV1V2Pdu->i2_PduType;

    /* allocate community community String */
    pV1V2Request->pCommunityStr =
        allocmem_octetstring (SNMP_MAX_OCTETSTRING_SIZE);
    if (pV1V2Request->pCommunityStr == NULL)
    {
        SnmpFreeV2Request (pV1V2Request);
        SNMPTrace ("[PROXY]:Unable to allocate memory for Community String\n");
        return NULL;
    }

    pCommunityEntryIn = SNMPGetCommunityEntryFromName (pV1V2Pdu->pCommunityStr);
    if (NULL == pCommunityEntryIn)
    {
        SnmpFreeV2Request (pV1V2Request);
        return NULL;
    }

    pCommString = SNMPGetCommunityString (pSingleTargetOut,
                                          &(pCommunityEntryIn->
                                            ContextEngineID));

    if (NULL == pCommString)
    {
        SnmpFreeV2Request (pV1V2Request);
        return NULL;
    }
    SNMPCopyOctetString (pV1V2Request->pCommunityStr, pCommString);

    /* Copy var bind list */
    pSrcVarBind = pV1V2Pdu->pVarBindList;
    while (NULL != pSrcVarBind)
    {
        /* Copy var bind function calls form var bind f/n which takes memory for
         * each element in the var bind structure and copies the data
         */
        pDestVarBind = SNMPCopyVarBind (pSrcVarBind);
        if (NULL == pDestVarBind)
        {
            SnmpFreeV2Request (pV1V2Request);
            return NULL;
        }
        if (pV1V2Request->pVarBindList == NULL)
        {
            pV1V2Request->pVarBindList = pDestVarBind;
            pV1V2Request->pVarBindEnd = pDestVarBind;
        }
        else
        {
            if (pV1V2Request->pVarBindEnd != NULL)
            {
                pV1V2Request->pVarBindEnd->pNextVarBind = pDestVarBind;
            }
            pV1V2Request->pVarBindEnd = pDestVarBind;
        }
        pSrcVarBind = pSrcVarBind->pNextVarBind;
    }

    return pV1V2Request;
}

/*********************************************************************
 *  Function Name : SNMPV3RequesttoV1Request
 *  Description   : This function translates v3 request pdu to v1
 *                  request pdu.
 *  Parameter(s)  : pV2Pdu - PDU Pointer
 *                  pSingleTargetOut - Target Parameter to which the 
 *                  request is to be forwarded.
 *  Return Values : v1 Request PDU or NULL
 *********************************************************************/
tSNMP_NORMAL_PDU   *
SNMPV3RequesttoV1Request (tSNMP_V3PDU * pV3Pdu,
                          tSNMP_OCTET_STRING_TYPE * pSingleTargetOut,
                          UINT4 u4RequestID)
{
    tSNMP_NORMAL_PDU   *pV1Request = NULL;
    if ((NULL == pV3Pdu) || (NULL == pSingleTargetOut))
    {
        return NULL;
    }

    pV1Request = SNMPV3RequesttoV2Request (pV3Pdu, pSingleTargetOut,
                                           u4RequestID);

    if (NULL == pV1Request)
        return NULL;

    pV1Request->u4_Version = VERSION1;

    if (SNMP_PDU_TYPE_GET_BULK_REQUEST == pV1Request->i2_PduType)
    {
        pV1Request->i2_PduType = SNMP_PDU_TYPE_GET_NEXT_REQUEST;
        pV1Request->i4_MaxRepetitions = SNMP_ZERO;
        pV1Request->i4_NonRepeaters = SNMP_ZERO;
    }

    return pV1Request;
}

/*********************************************************************
 *  Function Name : SNMPV2RequesttoV1Request
 *  Description   : This function translates v2 request pdu to v1
 *                  request pdu.
 *  Parameter(s)  : pV2Pdu - PDU Pointer
 *                  pSingleTargetOut - Target Parameter to which the 
 *                  request is to be forwarded.
 *  Return Values : v1 Request PDU or NULL
 *********************************************************************/
tSNMP_NORMAL_PDU   *
SNMPV2RequesttoV1Request (tSNMP_NORMAL_PDU * pV2Pdu,
                          tSNMP_OCTET_STRING_TYPE * pSingleTargetOut,
                          UINT4 u4RequestID)
{
    tSNMP_NORMAL_PDU   *pV1Request = NULL;
    tSNMP_VAR_BIND     *pSrcVarBind = NULL;
    tSNMP_VAR_BIND     *pDestVarBind = NULL;
    tSNMP_OCTET_STRING_TYPE *pCommString = NULL;
    tCommunityMappingEntry *pCommunityEntryIn = NULL;

    if ((NULL == pV2Pdu) || (NULL == pSingleTargetOut))
    {
        return NULL;
    }

    /* Allocate memory for the V2 pdu */
    pV1Request = MemAllocMemBlk (gSnmpNormalPduPoolId);
    if (pV1Request == NULL)
    {
        SNMPTrace ("[PROXY]:Unable to allocate Memory for pV1Request\n");
        return NULL;
    }
    MEMSET (pV1Request, SNMP_ZERO, sizeof (tSNMP_NORMAL_PDU));

    pV1Request->u4_Version = VERSION1;
    pV1Request->u4_RequestID = u4RequestID;

    /* In case non get bulk request, error status and error index shall be 
     * copied 
     */
    if (SNMP_PDU_TYPE_GET_BULK_REQUEST != pV2Pdu->i2_PduType)
    {
        pV1Request->i4_ErrorStatus = pV2Pdu->i4_ErrorStatus;
        pV1Request->i4_ErrorIndex = pV2Pdu->i4_ErrorIndex;
        pV1Request->i2_PduType = pV2Pdu->i2_PduType;
    }
    /* Get Bulk request shall be changed to Get next request and 
     * max-repititions 
     * and non-repeators will be set to zero (aeady zero due to memset)
     */
    else
    {
        pV1Request->i2_PduType = SNMP_PDU_TYPE_GET_NEXT_REQUEST;
    }

    /* allocate community community String */
    pV1Request->pCommunityStr =
        allocmem_octetstring (SNMP_MAX_OCTETSTRING_SIZE);
    if (pV1Request->pCommunityStr == NULL)
    {
        SnmpFreeV2Request (pV1Request);
        SNMPTrace ("[PROXY]:Unable to allocate memory for Community String\n");
        return NULL;
    }

    pCommunityEntryIn = SNMPGetCommunityEntryFromName (pV2Pdu->pCommunityStr);

    if (NULL == pCommunityEntryIn)
    {
        SnmpFreeV2Request (pV1Request);
        return NULL;
    }

    pCommString = SNMPGetCommunityString (pSingleTargetOut,
                                          &(pCommunityEntryIn->
                                            ContextEngineID));

    if (NULL == pCommString)
    {
        SnmpFreeV2Request (pV1Request);
        return NULL;
    }
    SNMPCopyOctetString (pV1Request->pCommunityStr, pCommString);

    /* Copy var bind list */
    pSrcVarBind = pV2Pdu->pVarBindList;
    while (NULL != pSrcVarBind)
    {
        /* Copy var bind function calls form var bind f/n which 
         * takes memory for
         * each element in the var bind structure and copies the data
         */
        pDestVarBind = SNMPCopyVarBind (pSrcVarBind);
        if (NULL == pDestVarBind)
        {
            SnmpFreeV2Request (pV1Request);
            return NULL;
        }
        if (pV1Request->pVarBindList == NULL)
        {
            pV1Request->pVarBindList = pDestVarBind;
            pV1Request->pVarBindEnd = pDestVarBind;
        }
        else
        {
            if (pV1Request->pVarBindEnd != NULL)
            {
                pV1Request->pVarBindEnd->pNextVarBind = pDestVarBind;
            }
            pV1Request->pVarBindEnd = pDestVarBind;
        }
        pSrcVarBind = pSrcVarBind->pNextVarBind;
    }

    return pV1Request;
}

/*********************************************************************
 *  Function Name : SNMPV3RequesttoV3Request
 *  Description   : This function translates v3 request pdu to v3
 *                  request pdu.
 *  Parameter(s)  : pV3Pdu - PDU Pointer
 *                  pSingleTargetOut - Target Parameter to which the 
 *                  request is to be forwarded.
 *  Return Values : v3 Request PDU or NULL
 *********************************************************************/
tSNMP_V3PDU        *
SNMPV3RequesttoV3Request (tSNMP_V3PDU * pV3Pdu,
                          tSNMP_OCTET_STRING_TYPE * pSingleTargetOut,
                          UINT4 u4RequestID)
{
    tSNMP_V3PDU        *pV3Request = NULL;
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    tSnmpTgtParamEntry *pSnmpTgtParamsEntry = NULL;
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    tSNMP_VAR_BIND     *pSrcVarBind = NULL;
    tSNMP_VAR_BIND     *pDestVarBind = NULL;

    if ((NULL == pV3Pdu) || (NULL == pSingleTargetOut))
    {
        return NULL;

    }
    pV3Request = SNMPAllocV3Pdu ();
    if (NULL == pV3Request)
    {
        return NULL;
    }

    pV3Request->Normalpdu.u4_Version = VERSION3;
    pV3Request->Normalpdu.u4_RequestID = u4RequestID;

    if (SNMP_PDU_TYPE_GET_BULK_REQUEST == pV3Pdu->Normalpdu.i2_PduType)
    {
        pV3Request->Normalpdu.i4_MaxRepetitions =
            pV3Pdu->Normalpdu.i4_MaxRepetitions;
        pV3Request->Normalpdu.i4_NonRepeaters =
            pV3Pdu->Normalpdu.i4_NonRepeaters;
    }
    else
    {
        pV3Request->Normalpdu.i4_ErrorStatus = pV3Pdu->Normalpdu.i4_ErrorStatus;
        pV3Request->Normalpdu.i4_ErrorIndex = pV3Pdu->Normalpdu.i4_ErrorIndex;
    }
    pV3Request->Normalpdu.i2_PduType = pV3Pdu->Normalpdu.i2_PduType;

    pV3Request->MsgFlag.pu1_OctetList[SNMP_ZERO] = 0x00;
    pV3Request->MsgFlag.i4_Length = 1;
    pV3Request->u4MsgID = SNMP_ZERO;
    pV3Request->u4MsgSecModel = pV3Pdu->u4MsgSecModel;
    pV3Request->u4MsgMaxSize = pV3Pdu->u4MsgMaxSize;
    SNMPCopyOctetString (&pV3Request->ContextID, &pV3Pdu->ContextID);
    SNMPCopyOctetString (&pV3Request->ContextName, &pV3Pdu->ContextName);

    pSnmpTgtAddrEntry = SNMPGetTgtAddrEntry (pSingleTargetOut);
    if (pSnmpTgtAddrEntry == NULL)
    {
        SnmpFreeV3Pdu (pV3Request);
        return NULL;
    }

    pSnmpTgtParamsEntry =
        SNMPGetTgtParamEntry (&(pSnmpTgtAddrEntry->AddrParams));
    if (NULL == pSnmpTgtParamsEntry)
    {
        SnmpFreeV3Pdu (pV3Request);
        return NULL;
    }

    /*Fill security params in the V3 response PDU */
    pSnmpUsmEntry = SNMPGetUsmEntry
        (&pV3Pdu->ContextID, &pSnmpTgtParamsEntry->ParamSecName);

    if (NULL == pSnmpUsmEntry)
    {
        SnmpFreeV3Pdu (pV3Request);
        return NULL;
    }

    SNMPCopyOctetString (&pV3Request->MsgSecParam.MsgEngineID,
                         &pSnmpUsmEntry->UsmUserEngineID);
    SNMPCopyOctetString (&pV3Request->MsgSecParam.MsgUsrName,
                         &pSnmpUsmEntry->UsmUserName);

    SNMPGenRandomIV ((UINT4 *) (VOID *) pV3Request->MsgSecParam.MsgPrivParam.
                     pu1_OctetList);
    pV3Request->MsgSecParam.MsgPrivParam.i4_Length = 8;

    /* Copy var bind list */
    pSrcVarBind = pV3Pdu->Normalpdu.pVarBindList;
    while (NULL != pSrcVarBind)
    {
        pDestVarBind = SNMPCopyVarBind (pSrcVarBind);
        if (NULL == pDestVarBind)
        {
            SnmpFreeV3Pdu (pV3Request);
            return NULL;
        }
        if (pV3Request->Normalpdu.pVarBindList == NULL)
        {
            pV3Request->Normalpdu.pVarBindList = pDestVarBind;
            pV3Request->Normalpdu.pVarBindEnd = pDestVarBind;
        }
        else
        {
            pV3Request->Normalpdu.pVarBindEnd->pNextVarBind = pDestVarBind;
            pV3Request->Normalpdu.pVarBindEnd = pDestVarBind;
        }
        pDestVarBind = NULL;
        pSrcVarBind = pSrcVarBind->pNextVarBind;
    }

    return pV3Request;
}

/*********************************************************************
 *  Function Name : SNMPV1ResponsetoV2Response
 *  Description   : This function translates v1 response pdu to v2
 *                  response pdu.
 *  Parameter(s)  : pV2Pdu - PDU Pointer
 *                  pSingleTargetOut - Target Parameter to which the 
 *                  request is to be forwarded.
 *  Return Values : v1 Request PDU or NULL
 *********************************************************************/
tSNMP_NORMAL_PDU   *
SNMPV1ResponsetoV2Response (tSNMP_NORMAL_PDU * pV1Pdu,
                            tSNMP_PROXY_CACHE * pCacheData)
{
    tSNMP_NORMAL_PDU   *pV2Response = NULL;
    tSNMP_NORMAL_PDU   *pV1Request = NULL;
    UINT1               resend_flag = SNMP_FALSE;
    tSNMP_VAR_BIND     *pVarBind = NULL;
    tSNMP_VAR_BIND     *pSrcVarBind = NULL;
    tSNMP_VAR_BIND     *pDestVarBind = NULL;
    tSNMP_PROXY_V1GBULK_CACHE *pGetBulkData = NULL;

    if (NULL == pV1Pdu)
        return NULL;

    pGetBulkData = &pCacheData->ProxyCmnCache.ProxyV1GbulkCache;

    /* resend flag shall be saved in the cache data of the request that is 
       actually received
       * as the new request will be cached by the dispatch handler and the 
       translation
       * module can not save the flag in the cache before the request is actually 
       cached 
     */
    resend_flag = pCacheData->ProxyCmnCache.u1_ReqResendFlag;

    /* 1) If the actual request was a get bulk request
     * 2) Error status in the response is too big
     * 3) Request has not been re-send and hence can be re-send now
     * 4) There is more than one node in the var bind list
     */
    if ((SNMP_PDU_TYPE_GET_BULK_REQUEST ==
         pCacheData->ProxyCmnCache.i2_RecPduType) &&
        (SNMP_ERR_TOO_BIG == pV1Pdu->i4_ErrorStatus) &&
        (SNMP_FALSE == resend_flag) &&
        (pGetBulkData->pVarBindList != pGetBulkData->pVarBindEnd))
    {
        pV1Request = MemAllocMemBlk (gSnmpNormalPduPoolId);
        if (pV1Request == NULL)
        {
            SNMPTrace ("[PROXY]:Unable to allocate Memory for pV1Request\n");
            return NULL;
        }

        MEMSET (pV1Request, SNMP_ZERO, sizeof (tSNMP_NORMAL_PDU));

        pV1Request->i2_PduType = SNMP_PDU_TYPE_GET_NEXT_REQUEST;
        pV1Request->u4_Version = VERSION1;

        /* Copy var bind from the cache to the new request */
        pSrcVarBind = pGetBulkData->pVarBindList;
        while (NULL != pSrcVarBind)
        {
            /* Copy var bind function calls form var bind f/n which takes 
               memory for
               * each element in the var bind structure and copies the data
             */
            pDestVarBind = SNMPCopyVarBind (pSrcVarBind);
            if (NULL == pDestVarBind)
            {
                SnmpFreeV2Request (pV1Request);
                return NULL;
            }
            if (pV1Request->pVarBindList == NULL)
            {
                pV1Request->pVarBindList = pDestVarBind;
                pV1Request->pVarBindEnd = pDestVarBind;
            }
            else
            {
                if (pV1Request->pVarBindEnd != NULL)
                {
                    pV1Request->pVarBindEnd->pNextVarBind = pDestVarBind;
                }
                pV1Request->pVarBindEnd = pDestVarBind;
            }
            pSrcVarBind = pSrcVarBind->pNextVarBind;
        }

        /* remove the first node from the var bind list */
        pVarBind = pV1Request->pVarBindList;
        if (pV1Request->pVarBindList != NULL)
        {
            pV1Request->pVarBindList = pV1Request->pVarBindList->pNextVarBind;
        }
        MemReleaseMemBlock (gSnmpVarBindPoolId, (UINT1 *) pVarBind);
        pVarBind = NULL;

        /* fill the community string from the cache data of 
           the request send 
           previously */
        pV1Request->pCommunityStr =
            allocmem_octetstring (SNMP_MAX_OCTETSTRING_SIZE);
        if (pV1Request->pCommunityStr == NULL)
        {
            SnmpFreeV2Request (pV1Request);
            SNMPTrace ("[PROXY]:Unable to allocate memory for Community String\
                    n");
            return NULL;
        }

        SNMPProxyCopyOctetStringFromArr (pV1Request->pCommunityStr,
                                         &pGetBulkData->OutCommunityStr);

        /* Generate a new request id */
        pV1Request->u4_RequestID = SNMPGetRequestID ();

        return pV1Request;
    }

    /* This else includes the following cases -
     * 1) Request was a non-Get Bulk request and any response is received
     * 2) Request was a Get-Bulk request, error status in response is too big
     *     and request has already been re-send.
     * 3) Request was a Get-Bulk request, error status in response is too big,
     *     request has not been re-send but there is only one node in the var 
     *     bind list.
     * 4) Request was a Get-Bulk request and error status other than too big is 
     *      is received in the response.
     */
    else
    {
        pV2Response = MemAllocMemBlk (gSnmpNormalPduPoolId);
        if (pV2Response == NULL)
        {
            SNMPTrace ("[PROXY]:Unable to allocate Memory for pV2Response\n");
            return NULL;
        }

        MEMSET (pV2Response, SNMP_ZERO, sizeof (tSNMP_NORMAL_PDU));

        pV2Response->i2_PduType = pV1Pdu->i2_PduType;
        pV2Response->u4_Version = VERSION2;

        /* request ID from the cache */
        pV2Response->u4_RequestID = pCacheData->ProxyCmnCache.u4_RecRequestID;

        /* fill the community string from the cache data */
        pV2Response->pCommunityStr =
            allocmem_octetstring (SNMP_MAX_OCTETSTRING_SIZE);
        if (pV2Response->pCommunityStr == NULL)
        {
            SnmpFreeV2Request (pV2Response);
            SNMPTrace
                ("[PROXY]:Unable to allocate memory for Community String\n");
            return NULL;
        }

        SNMPProxyCopyOctetStringFromArr (pV2Response->pCommunityStr,
                                         &pCacheData->cacheInfo.
                                         ProxyV1V2SpCache.RecCommunityStr);

        /* For non-Get Bulk requets if error status is too big then 
         * set the error status to no error and error index to zero 
         * and set the var bind list to null
         */
        if (SNMP_ERR_TOO_BIG == pV1Pdu->i4_ErrorStatus)
        {
            pV2Response->i4_ErrorStatus = SNMP_ERR_NO_ERROR;
            pV2Response->pVarBindList = NULL;
        }
        /* Copy the var bind list in rest of the cases */
        else
        {
            pV2Response->i4_ErrorIndex = pV1Pdu->i4_ErrorIndex;
            pV2Response->i4_ErrorStatus = pV1Pdu->i4_ErrorStatus;

            pSrcVarBind = pV1Pdu->pVarBindList;
            while (NULL != pSrcVarBind)
            {
                /* Copy var bind function calls form var bind f/n which takes 
                   memory for
                   * each element in the var bind structure and copies the data
                 */
                pDestVarBind = SNMPCopyVarBind (pSrcVarBind);
                if (NULL == pDestVarBind)
                {
                    SnmpFreeV2Request (pV2Response);
                    return NULL;
                }
                if (pV2Response->pVarBindList == NULL)
                {
                    pV2Response->pVarBindList = pDestVarBind;
                    pV2Response->pVarBindEnd = pDestVarBind;
                }
                else
                {
                    if (pV2Response->pVarBindEnd != NULL)
                    {
                        pV2Response->pVarBindEnd->pNextVarBind = pDestVarBind;
                    }
                    pV2Response->pVarBindEnd = pDestVarBind;
                }
                pSrcVarBind = pSrcVarBind->pNextVarBind;
            }
        }

        return pV2Response;
    }
}

/*********************************************************************
 *  Function Name : SNMPV2ResponsetoV3Response
 *  Description   : This function translates v2 response pdu to v3
 *                  response pdu.
 *  Parameter(s)  : pV2Pdu - PDU Pointer
 *                  pSingleTargetOut - Target Parameter to which the 
 *                  request is to be forwarded.
 *  Return Values : v1 Request PDU or NULL
 *********************************************************************/
tSNMP_V3PDU        *
SNMPV2ResponsetoV3Response (tSNMP_NORMAL_PDU * pV2Pdu,
                            tSNMP_PROXY_CACHE * pCacheData)
{
    tSNMP_V3PDU        *pV3Response = NULL;
    tSNMP_VAR_BIND     *pSrcVarBind = NULL;
    tSNMP_VAR_BIND     *pDestVarBind = NULL;

    if ((NULL == pV2Pdu) || (NULL == pCacheData))
    {
        return NULL;
    }

    pV3Response = SNMPAllocV3Pdu ();
    if (NULL == pV3Response)
        return NULL;

    pV3Response->Normalpdu.u4_Version = VERSION3;
    pV3Response->Normalpdu.u4_RequestID =
        pCacheData->ProxyCmnCache.u4_RecRequestID;
    pV3Response->Normalpdu.i2_PduType = pV2Pdu->i2_PduType;

    /* Copy all the data from cache */
    if (SNMP_FAILURE == SNMPCopyV3ParamsFromCache (pV3Response, pCacheData))
    {
        SnmpFreeV3Pdu (pV3Response);
        return NULL;
    }

    /*Copy Engine Id from Agent */
    if (SNMP_PDU_TYPE_V2_INFORM_REQUEST ==
        pCacheData->ProxyCmnCache.i2_RecPduType)
    {
        SNMPCopyOctetString (&(pV3Response->MsgSecParam.MsgEngineID),
                             &pV3Response->ContextID);
    }
    else
    {
        SNMPCopyOctetString (&pV3Response->MsgSecParam.MsgEngineID,
                             &gSnmpEngineID);
    }

    if (SNMP_ERR_TOO_BIG == pV2Pdu->i4_ErrorStatus)
    {
        pV3Response->Normalpdu.i4_ErrorStatus = SNMP_ERR_NO_ERROR;
        pV3Response->Normalpdu.pVarBindList = NULL;
    }
    else
    {
        pV3Response->Normalpdu.i4_ErrorIndex = pV2Pdu->i4_ErrorIndex;
        pV3Response->Normalpdu.i4_ErrorStatus = pV2Pdu->i4_ErrorStatus;

        pSrcVarBind = pV2Pdu->pVarBindList;
        while (NULL != pSrcVarBind)
        {
            pDestVarBind = SNMPCopyVarBind (pSrcVarBind);
            if (NULL == pDestVarBind)
            {
                SnmpFreeV3Pdu (pV3Response);
                return NULL;
            }
            if (pV3Response->Normalpdu.pVarBindList == NULL)
            {
                pV3Response->Normalpdu.pVarBindList = pDestVarBind;
                pV3Response->Normalpdu.pVarBindEnd = pDestVarBind;
            }
            else
            {
                pV3Response->Normalpdu.pVarBindEnd->pNextVarBind = pDestVarBind;
                pV3Response->Normalpdu.pVarBindEnd = pDestVarBind;
            }
            pDestVarBind = NULL;
            pSrcVarBind = pSrcVarBind->pNextVarBind;
        }
    }

    return pV3Response;
}

/*********************************************************************
 *  Function Name : SNMPV1ResponsetoV3Response
 *  Description   : This function translates v1 response pdu to v3
 *                  response pdu.
 *  Parameter(s)  : pV2Pdu - PDU Pointer
 *                  pSingleTargetOut - Target Parameter to which the 
 *                  request is to be forwarded.
 *  Return Values : v1 Request PDU or NULL
 *********************************************************************/
VOID               *
SNMPV1ResponsetoV3Response (tSNMP_NORMAL_PDU * pV1Pdu,
                            INT2 *pPduType, tSNMP_PROXY_CACHE * pCacheData)
{
    tSNMP_V3PDU        *pV3Response = NULL;
    tSNMP_NORMAL_PDU   *pV1Request = NULL;
    tSNMP_VAR_BIND     *pSrcVarBind = NULL;
    tSNMP_VAR_BIND     *pDestVarBind = NULL;
    tSNMP_VAR_BIND     *pVarBind = NULL;
    tSNMP_PROXY_V1GBULK_CACHE *pGetBulkData = NULL;
    INT1                resend_flag = SNMP_FALSE;

    if ((NULL == pV1Pdu) || (NULL == pPduType) || (NULL == pCacheData))
    {
        return NULL;
    }
    pGetBulkData = &(pCacheData->ProxyCmnCache.ProxyV1GbulkCache);

    resend_flag = (INT1) pCacheData->ProxyCmnCache.u1_ReqResendFlag;

    /* 1) If the actual request was a get bulk request
     * 2) Error status in the response is too big
     * 3) Request has not been re-send and hence can be re-send now
     * 4) There is more than one node in the var bind list
     */
    if ((SNMP_PDU_TYPE_GET_BULK_REQUEST ==
         pCacheData->ProxyCmnCache.i2_RecPduType) &&
        (SNMP_ERR_TOO_BIG == pV1Pdu->i4_ErrorStatus) &&
        (SNMP_FALSE == resend_flag) &&
        (pGetBulkData->pVarBindList != pGetBulkData->pVarBindEnd))
    {
        pV1Request = MemAllocMemBlk (gSnmpNormalPduPoolId);
        if (pV1Request == NULL)
        {
            SNMPTrace ("[PROXY]:Unable to allocate Memory for pV1Request\n");
            return NULL;
        }

        MEMSET (pV1Request, SNMP_ZERO, sizeof (tSNMP_NORMAL_PDU));

        pV1Request->i2_PduType = SNMP_PDU_TYPE_GET_NEXT_REQUEST;
        pV1Request->u4_Version = VERSION1;

        *pPduType = SNMP_PDU_TYPE_GET_NEXT_REQUEST;

        /* Copy var bind from the cache to the new request */
        pSrcVarBind = pGetBulkData->pVarBindList;
        while (NULL != pSrcVarBind)
        {
            /* Copy var bind function calls form var bind f/n 
             * which takes memory for
             * each element in the var bind structure and copies the data
             */
            pDestVarBind = SNMPCopyVarBind (pSrcVarBind);
            if (NULL == pDestVarBind)
            {
                SnmpFreeV2Request (pV1Request);
                return NULL;
            }
            if (pV1Request->pVarBindList == NULL)
            {
                pV1Request->pVarBindList = pDestVarBind;
                pV1Request->pVarBindEnd = pDestVarBind;
            }
            else
            {
                if (pV1Request->pVarBindEnd != NULL)
                {
                    pV1Request->pVarBindEnd->pNextVarBind = pDestVarBind;
                }
                pV1Request->pVarBindEnd = pDestVarBind;
            }
            pSrcVarBind = pSrcVarBind->pNextVarBind;
        }

        /* remove the first node from the var bind list */
        pVarBind = pV1Request->pVarBindList;
        if (pV1Request->pVarBindList != NULL)
        {
            pV1Request->pVarBindList = pV1Request->pVarBindList->pNextVarBind;
        }
        MemReleaseMemBlock (gSnmpVarBindPoolId, (UINT1 *) pVarBind);
        pVarBind = NULL;

        /* fill the community string from the cache data of the request send 
           previously */
        pV1Request->pCommunityStr =
            allocmem_octetstring (SNMP_MAX_OCTETSTRING_SIZE);
        if (pV1Request->pCommunityStr == NULL)
        {
            SnmpFreeV2Request (pV1Request);
            SNMPTrace
                ("[PROXY]:Unable to allocate memory for Community String\n");
            return NULL;
        }

        SNMPProxyCopyOctetStringFromArr (pV1Request->pCommunityStr,
                                         &pGetBulkData->OutCommunityStr);

        /* Generate a new request id */
        pV1Request->u4_RequestID = SNMPGetRequestID ();

        return pV1Request;
    }

    /* This else includes the following cases -
     * 1) Request was a non-Get Bulk request and any response is received
     * 2) Request was a Get-Bulk request, error status in response is too big
     *    and request has already been re-send.
     * 3) Request was a Get-Bulk request, error status in response is too big,
     *    request has not been re-send but there is only one node in the var 
     *    bind list.
     * 4) Request was a Get-Bulk request and error status other than too big is 
     *    is received in the response.
     */
    else
    {
        pV3Response = SNMPAllocV3Pdu ();
        if (pV3Response == NULL)
        {
            SNMPTrace ("[PROXY]:Unable to allocate Memory for pV3Response\n");
            return NULL;
        }

        pV3Response->Normalpdu.i2_PduType = pV1Pdu->i2_PduType;
        *pPduType = pV1Pdu->i2_PduType;

        pV3Response->Normalpdu.u4_Version = VERSION3;
        pV3Response->Normalpdu.u4_RequestID =
            pCacheData->ProxyCmnCache.u4_RecRequestID;

        pV3Response->Normalpdu.i4_ErrorStatus = pV1Pdu->i4_ErrorStatus;
        pV3Response->Normalpdu.i4_ErrorIndex = pV1Pdu->i4_ErrorIndex;

        /* Copy all the data from pV3Data in cache */

        if (SNMP_FAILURE == SNMPCopyV3ParamsFromCache (pV3Response, pCacheData))
        {
            SnmpFreeV3Pdu (pV3Response);
            return NULL;
        }
        SNMPCopyOctetString (&pV3Response->MsgSecParam.MsgEngineID,
                             &gSnmpEngineID);

        if (SNMP_ERR_TOO_BIG == pV1Pdu->i4_ErrorStatus)
        {
            pV3Response->Normalpdu.i4_ErrorStatus = SNMP_ERR_NO_ERROR;
            pV3Response->Normalpdu.pVarBindList = NULL;
        }
        else
        {
            pV3Response->Normalpdu.i4_ErrorIndex = pV1Pdu->i4_ErrorIndex;
            pV3Response->Normalpdu.i4_ErrorStatus = pV1Pdu->i4_ErrorStatus;

            pSrcVarBind = pV1Pdu->pVarBindList;
            while (NULL != pSrcVarBind)
            {
                /* Copy var bind function calls form var bind 
                 * f/n which takes memory for
                 * each element in the var bind structure and copies the data
                 */
                pDestVarBind = SNMPCopyVarBind (pSrcVarBind);
                if (NULL == pDestVarBind)
                {
                    SnmpFreeV3Pdu (pV3Response);
                    return NULL;
                }
                if (pV3Response->Normalpdu.pVarBindList == NULL)
                {
                    pV3Response->Normalpdu.pVarBindList = pDestVarBind;
                    pV3Response->Normalpdu.pVarBindEnd = pDestVarBind;
                }
                else
                {
                    pV3Response->Normalpdu.pVarBindEnd->pNextVarBind =
                        pDestVarBind;
                    pV3Response->Normalpdu.pVarBindEnd = pDestVarBind;
                }
                pDestVarBind = NULL;
                pSrcVarBind = pSrcVarBind->pNextVarBind;
            }                    /* while */
        }
    }

    return pV3Response;
}

/*********************************************************************
 *  Function Name : SNMPV1V2ResponsetoV1V2Response
 *  Description   : This function translates v1 response pdu to v1
 *                  response pdu or v2 to v2 response pdu.
 *  Parameter(s)  : pV1V2Pdu - PDU Pointer
 *                  u4PduVersion - PDU Version 
 *                  pCacheData -  Cache Node
 *  Return Values : v1 Request PDU or NULL
 *********************************************************************/
tSNMP_NORMAL_PDU   *
SNMPV1V2ResponsetoV1V2Response (tSNMP_NORMAL_PDU * pV1V2Pdu,
                                UINT4 u4PduVersion,
                                tSNMP_PROXY_CACHE * pCacheData)
{
    tSNMP_NORMAL_PDU   *pV1V2Response = NULL;
    tSNMP_VAR_BIND     *pSrcVarBind = NULL;
    tSNMP_VAR_BIND     *pDestVarBind = NULL;

    if (NULL == pV1V2Pdu)
        return NULL;

    pV1V2Response = MemAllocMemBlk (gSnmpNormalPduPoolId);
    if (pV1V2Response == NULL)
    {
        SNMPTrace ("[PROXY]:Unable to allocate Memory for pV1V2Response\n");
        return NULL;
    }

    MEMSET (pV1V2Response, SNMP_ZERO, sizeof (tSNMP_NORMAL_PDU));

    pV1V2Response->i2_PduType = pV1V2Pdu->i2_PduType;
    pV1V2Response->u4_Version = u4PduVersion;

    /* request ID from the cache */
    pV1V2Response->u4_RequestID = pCacheData->ProxyCmnCache.u4_RecRequestID;

    /* fill the community string from the cache data */
    pV1V2Response->pCommunityStr =
        allocmem_octetstring (SNMP_MAX_OCTETSTRING_SIZE);
    if (pV1V2Response->pCommunityStr == NULL)
    {
        SnmpFreeV2Request (pV1V2Response);
        SNMPTrace ("[PROXY]:Unable to allocate memory for Community String\n");
        return NULL;
    }
    SNMPProxyCopyOctetStringFromArr (pV1V2Response->pCommunityStr,
                                     &pCacheData->cacheInfo.ProxyV1V2SpCache.
                                     RecCommunityStr);

    /* For non-Get Bulk requets if error status is too big then 
     * set the error status to no error and error index to zero 
     * and set the var bind list to null
     */
    if (SNMP_ERR_TOO_BIG == pV1V2Pdu->i4_ErrorStatus)
    {
        pV1V2Response->i4_ErrorStatus = SNMP_ERR_NO_ERROR;
        pV1V2Response->pVarBindList = NULL;
    }
    /* Copy the var bind list in rest of the cases */
    else
    {
        pV1V2Response->i4_ErrorIndex = pV1V2Pdu->i4_ErrorIndex;
        pV1V2Response->i4_ErrorStatus = pV1V2Pdu->i4_ErrorStatus;

        pSrcVarBind = pV1V2Pdu->pVarBindList;
        while (NULL != pSrcVarBind)
        {
            /* Copy var bind function calls form var bind f/n which takes 
               memory for
               * each element in the var bind structure and copies the data
             */
            pDestVarBind = SNMPCopyVarBind (pSrcVarBind);
            if (NULL == pDestVarBind)
            {
                SnmpFreeV2Request (pV1V2Response);
                return NULL;
            }
            if (pV1V2Response->pVarBindList == NULL)
            {
                pV1V2Response->pVarBindList = pDestVarBind;
                pV1V2Response->pVarBindEnd = pDestVarBind;
            }
            else
            {
                if (pV1V2Response->pVarBindEnd != NULL)
                {
                    pV1V2Response->pVarBindEnd->pNextVarBind = pDestVarBind;
                }
                pV1V2Response->pVarBindEnd = pDestVarBind;
            }
            pSrcVarBind = pSrcVarBind->pNextVarBind;
        }
    }
    return pV1V2Response;
}

/************************************************************************
 *  Function Name   : SNMPV3ResponsetoV3Response 
 *  Description     : Function translates v3 response to v3 response. 
 *  Input           : pCacheData - Cache node 
 *                    pV3Pdu - v3 pdu pointer
 *  Output          : None 
 *  Returns         : SNMP_SUCCESS, SNMP_FAILURE 
 ************************************************************************/

tSNMP_V3PDU        *
SNMPV3ResponsetoV3Response (tSNMP_V3PDU * pV3Pdu,
                            tSNMP_PROXY_CACHE * pCacheData)
{
    tSNMP_V3PDU        *pV3Msg = NULL;

    pV3Msg = SNMPAllocV3Pdu ();
    if (NULL == pV3Msg)
    {
        return NULL;
    }

    if (SNMP_FAILURE ==
        SNMPCopyNormalPduParams (&pV3Msg->Normalpdu, &pV3Pdu->Normalpdu))
    {
        SnmpFreeV3Pdu (pV3Msg);
        return NULL;
    }

    if (SNMP_FAILURE == SNMPCopyV3ParamsFromCache (pV3Msg, pCacheData))
    {
        SnmpFreeV3Pdu (pV3Msg);
        return NULL;
    }

    pV3Msg->Normalpdu.u4_RequestID = pCacheData->ProxyCmnCache.u4_RecRequestID;
    pV3Msg->Normalpdu.u4_Version = VERSION3;

    return pV3Msg;
}

/*********************************************************************
 *  Function Name : SNMPV2TraptoV3Trap
 *  Description   : This function translates v1 response pdu to v3
 *                  response pdu.
 *  Parameter(s)  : pV2TrapPdu - PDU Pointer
 *                  pSingleTargetOut - Target Parameter to which the 
 *                  request is to be forwarded.
 *  Return Values : v3 Request PDU or NULL
 *********************************************************************/
tSNMP_V3PDU        *
SNMPV2TraptoV3Trap (tSNMP_NORMAL_PDU * pV2TrapPdu,
                    tSNMP_OCTET_STRING_TYPE * pSingleTargetOut,
                    UINT4 u4RequestID)
{
    tSNMP_V3PDU        *pV3TrapMsg = NULL;
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    tSnmpTgtParamEntry *pSnmpTgtParamsEntry = NULL;
    tCommunityMappingEntry *pCommunityEntryIn = NULL;
    tCommunityMappingEntry *pCommunityEntryOut = NULL;
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;

    if ((NULL == pV2TrapPdu) || (NULL == pSingleTargetOut))
        return NULL;

    pSnmpTgtAddrEntry = SNMPGetTgtAddrEntry (pSingleTargetOut);
    if (pSnmpTgtAddrEntry == NULL)
    {
        return NULL;
    }

    pSnmpTgtParamsEntry =
        SNMPGetTgtParamEntry (&(pSnmpTgtAddrEntry->AddrParams));
    if (NULL == pSnmpTgtParamsEntry)
    {
        return NULL;
    }

    /*Get community entry from v2 message community */
    pCommunityEntryIn =
        SNMPGetCommunityEntryFromName (pV2TrapPdu->pCommunityStr);

    if (NULL == pCommunityEntryIn)
    {
        return NULL;
    }

    pCommunityEntryOut =
        SNMPGetCommunityEntryFromSecNameContextID (&
                                                   (pSnmpTgtParamsEntry->
                                                    ParamSecName),
                                                   &(pCommunityEntryIn->
                                                     ContextEngineID));

    if (NULL == pCommunityEntryOut)
    {
        return NULL;
    }

    pSnmpUsmEntry = SNMPGetUsmEntry
        (&gSnmpEngineID, &pSnmpTgtParamsEntry->ParamSecName);

    if (NULL == pSnmpUsmEntry)
        return NULL;

    pV3TrapMsg = SNMPFillV3Params (pSnmpUsmEntry,
                                   &pCommunityEntryOut->ContextName);

    if (pV3TrapMsg != NULL)
    {
        SNMPCopyOctetString (&(pV3TrapMsg->ContextID),
                             &(pCommunityEntryIn->ContextEngineID));

        pV3TrapMsg->u4MsgSecModel =
            (UINT4) pSnmpTgtParamsEntry->i2ParamSecModel;

        /* Copy other parameters of normal pdu */
        if (SNMP_FAILURE == SNMPCopyNormalPduParams (&pV3TrapMsg->Normalpdu,
                                                     pV2TrapPdu))
        {
            MemReleaseMemBlock (gSnmpV3PduPoolId, (UINT1 *) pV3TrapMsg);
            return NULL;
        }

        pV3TrapMsg->Normalpdu.u4_Version = VERSION3;
        pV3TrapMsg->Normalpdu.u4_RequestID = u4RequestID;
    }
    return pV3TrapMsg;
}

/*********************************************************************
 *  Function Name : SNMPV1TraptoV1Trap
 *  Description   : This function translates v1 response pdu to v1
 *                  response pdu.
 *  Parameter(s)  : pV1TrapPdu - PDU Pointer
 *                  pSingleTargetOut - Target Parameter to which the 
 *                  request is to be forwarded.
 *  Return Values : v1 Request PDU or NULL
 *********************************************************************/
tSNMP_TRAP_PDU     *
SNMPV1TraptoV1Trap (tSNMP_TRAP_PDU * pV1TrapPdu,
                    tSNMP_OCTET_STRING_TYPE * pSingleTargetOut)
{
    tSNMP_TRAP_PDU     *pV1TrapMsg = NULL;
    tSNMP_VAR_BIND     *pSrcVarBind = NULL, *pDestVarBind = NULL;
    tSNMP_OCTET_STRING_TYPE *pCommString = NULL;
    tCommunityMappingEntry *pCommunityEntryIn = NULL;

    if ((NULL == pV1TrapPdu) || (NULL == pSingleTargetOut))
        return NULL;

    pV1TrapMsg = MemAllocMemBlk (gSnmpTrapPduPoolId);

    if (pV1TrapMsg == NULL)
    {
        SNMPTrace ("Unable to allocate memory for pV1TrapMsg\n");
        return NULL;
    }

    MEMSET (pV1TrapMsg, SNMP_ZERO, sizeof (tSNMP_TRAP_PDU));
    pV1TrapMsg->i2_PduType = SNMP_PDU_TYPE_TRAP;
    pV1TrapMsg->u4_Version = VERSION1;
    pV1TrapMsg->u4_TimeStamp = pV1TrapPdu->u4_TimeStamp;

    /* Allocate Community String */
    pV1TrapMsg->pCommunityStr =
        allocmem_octetstring (SNMP_MAX_OCTETSTRING_SIZE);
    if (pV1TrapMsg->pCommunityStr == NULL)
    {
        SNMPFreeV1Trap (pV1TrapMsg);
        SNMPTrace ("Unable to allocate memory for Community String\n");
        return NULL;
    }

    pCommunityEntryIn =
        SNMPGetCommunityEntryFromName (pV1TrapPdu->pCommunityStr);

    if (NULL == pCommunityEntryIn)
    {
        SNMPFreeV1Trap (pV1TrapMsg);
        return NULL;
    }

    pCommString = SNMPGetCommunityString (pSingleTargetOut,
                                          &(pCommunityEntryIn->
                                            ContextEngineID));

    if (NULL == pCommString)
    {
        SNMPFreeV1Trap (pV1TrapMsg);
        return NULL;
    }
    SNMPCopyOctetString (pV1TrapMsg->pCommunityStr, pCommString);

    pV1TrapMsg->pEnterprise = alloc_oid (MAX_OID_LENGTH);
    if (pV1TrapMsg->pEnterprise == NULL)
    {
        SNMPTrace ("Unable to Allocate Enterprise for Trap Pdu\n");
        SNMPFreeV1Trap (pV1TrapMsg);
        return (NULL);
    }

    MEMCPY (pV1TrapMsg->pEnterprise->pu4_OidList,
            pV1TrapPdu->pEnterprise->pu4_OidList,
            SNMP_OID_LEN * sizeof (UINT4));

    pV1TrapMsg->pEnterprise->u4_Length = SNMP_OID_LEN;

    /* Agent IP Address is Updated from System Type */
    pV1TrapMsg->pAgentAddr = allocmem_octetstring (sizeof (UINT4));
    if (pV1TrapMsg->pAgentAddr == NULL)
    {
        SNMPTrace ("Failure to allocate Agent address for v1 trap \n");
        SNMPFreeV1Trap (pV1TrapMsg);
        return NULL;
    }

    SNMPCopyOctetString (pV1TrapMsg->pAgentAddr, pV1TrapPdu->pAgentAddr);

    pV1TrapMsg->i4_GenericTrap = pV1TrapPdu->i4_GenericTrap;
    pV1TrapMsg->i4_SpecificTrap = pV1TrapPdu->i4_SpecificTrap;

    /* Copy var bind list */
    pSrcVarBind = pV1TrapPdu->pVarBindList;
    while (NULL != pSrcVarBind)
    {
        pDestVarBind = SNMPCopyVarBind (pSrcVarBind);
        if (NULL == pDestVarBind)
        {
            SNMPFreeV1Trap (pV1TrapMsg);
            return SNMP_FAILURE;
        }
        if (NULL == pV1TrapMsg->pVarBindList)
        {
            pV1TrapMsg->pVarBindList = pDestVarBind;
            pV1TrapMsg->pVarBindEnd = pDestVarBind;
        }
        else
        {
            if (pV1TrapMsg->pVarBindEnd != NULL)
            {
                pV1TrapMsg->pVarBindEnd->pNextVarBind = pDestVarBind;
            }
            pV1TrapMsg->pVarBindEnd = pDestVarBind;
        }
        pSrcVarBind = pSrcVarBind->pNextVarBind;
    }

    pV1TrapMsg->pVarBindEnd = pDestVarBind;

    return (pV1TrapMsg);
}

/*********************************************************************
 *  Function Name : SNMPV3TraptoV2Trap
 *  Description   : This function translates v3 trap pdu to v2 trap pdu
 *  Parameter(s)  : pV3TrapPdu - PDU Pointer
 *                  pSingleTargetOut - Target Parameter to which the 
 *                  request is to be forwarded.
 *                    Request ID - To be used for the V2 PDU
 *  Return Values : v2 Trap PDU or NULL
 *********************************************************************/
tSNMP_NORMAL_PDU   *
SNMPV3TraptoV2Trap (tSNMP_V3PDU * pV3TrapPdu,
                    tSNMP_OCTET_STRING_TYPE * pSingleTargetOut,
                    UINT4 u4RequestID)
{
    tSNMP_NORMAL_PDU   *pV2Pdu = NULL;
    tSNMP_OCTET_STRING_TYPE *pCommString = NULL;
    tSNMP_VAR_BIND     *pSrcVarBind = NULL;
    tSNMP_VAR_BIND     *pDestVarBind = NULL;

    if ((NULL == pV3TrapPdu) || (NULL == pSingleTargetOut))
        return NULL;

    pV2Pdu = MemAllocMemBlk (gSnmpNormalPduPoolId);
    if (pV2Pdu == NULL)
    {
        SNMPTrace ("Unable to allocate Memory for pV2Pdu\n");
        return NULL;
    }
    MEMSET (pV2Pdu, SNMP_ZERO, sizeof (tSNMP_NORMAL_PDU));
    pV2Pdu->i2_PduType = pV3TrapPdu->Normalpdu.i2_PduType;
    pV2Pdu->u4_Version = VERSION2;
    pV2Pdu->i4_ErrorStatus = SNMP_ERR_NO_ERROR;
    pV2Pdu->i4_ErrorIndex = SNMP_ZERO;
    pV2Pdu->u4_RequestID = u4RequestID;

    /* allocate community community String */
    pV2Pdu->pCommunityStr = allocmem_octetstring (SNMP_MAX_OCTETSTRING_SIZE);
    if (pV2Pdu->pCommunityStr == NULL)
    {
        SNMPFreeV2Trap (pV2Pdu);
        SNMPTrace ("Unable to allocate memory for Community String\n");
        return NULL;
    }

    pCommString = SNMPGetCommunityString (pSingleTargetOut,
                                          &pV3TrapPdu->ContextID);

    if (NULL == pCommString)
    {
        SNMPFreeV2Trap (pV2Pdu);
        return NULL;
    }

    SNMPCopyOctetString (pV2Pdu->pCommunityStr, pCommString);

    /* Copy var bind list */
    pSrcVarBind = pV3TrapPdu->Normalpdu.pVarBindList;
    while (NULL != pSrcVarBind)
    {
        pDestVarBind = SNMPCopyVarBind (pSrcVarBind);
        if (NULL == pDestVarBind)
        {
            SNMPFreeV2Trap (pV2Pdu);
            return SNMP_FAILURE;
        }
        if (NULL == pV2Pdu->pVarBindList)
        {
            pV2Pdu->pVarBindList = pDestVarBind;
            pV2Pdu->pVarBindEnd = pDestVarBind;
        }
        else
        {
            if (pV2Pdu->pVarBindEnd != NULL)
            {
                pV2Pdu->pVarBindEnd->pNextVarBind = pDestVarBind;
            }
            pV2Pdu->pVarBindEnd = pDestVarBind;
        }
        pSrcVarBind = pSrcVarBind->pNextVarBind;
    }

    return pV2Pdu;
}

/*********************************************************************
 *  Function Name : SNMPV2TraptoV2Trap
 *  Description   : This function translates v2 response pdu to v2
 *                  response pdu.
 *  Parameter(s)  : pV2TrapPdu - PDU Pointer
 *                  pSingleTargetOut - Target Parameter to which the 
 *                  request is to be forwarded.
 *  Return Values : v1 Request PDU or NULL
 *********************************************************************/
tSNMP_NORMAL_PDU   *
SNMPV2TraptoV2Trap (tSNMP_NORMAL_PDU * pV2TrapPdu,
                    tSNMP_OCTET_STRING_TYPE * pSingleTargetOut,
                    UINT4 u4RequestID)
{
    tSNMP_NORMAL_PDU   *pV2TrapMsg = NULL;
    tSNMP_VAR_BIND     *pSrcVarBind = NULL, *pDestVarBind = NULL;
    tSNMP_OCTET_STRING_TYPE *pCommString = NULL;
    tCommunityMappingEntry *pCommunityEntryIn = NULL;

    if ((NULL == pV2TrapPdu) || (NULL == pSingleTargetOut))
        return NULL;

    pV2TrapMsg = MemAllocMemBlk (gSnmpNormalPduPoolId);
    if (pV2TrapMsg == NULL)
    {
        SNMPTrace ("Unable to allocate Memory for pV2TrapMsg\n");
        return NULL;
    }
    MEMSET (pV2TrapMsg, SNMP_ZERO, sizeof (tSNMP_NORMAL_PDU));
    pV2TrapMsg->i2_PduType = pV2TrapPdu->i2_PduType;
    pV2TrapMsg->u4_Version = VERSION2;
    pV2TrapMsg->i4_ErrorStatus = SNMP_ERR_NO_ERROR;
    pV2TrapMsg->i4_ErrorIndex = SNMP_ZERO;
    pV2TrapMsg->u4_RequestID = u4RequestID;

    /* allocate community community String */
    pV2TrapMsg->pCommunityStr =
        allocmem_octetstring (SNMP_MAX_OCTETSTRING_SIZE);
    if (pV2TrapMsg->pCommunityStr == NULL)
    {
        SNMPFreeV2Trap (pV2TrapMsg);
        SNMPTrace ("Unable to allocate memory for Community String\n");
        return NULL;
    }

    pCommunityEntryIn =
        SNMPGetCommunityEntryFromName (pV2TrapPdu->pCommunityStr);

    if (NULL == pCommunityEntryIn)
    {
        SNMPFreeV2Trap (pV2TrapMsg);
        return NULL;
    }
    pCommString = SNMPGetCommunityString (pSingleTargetOut,
                                          &(pCommunityEntryIn->
                                            ContextEngineID));

    if (NULL == pCommString)
    {
        SNMPFreeV2Trap (pV2TrapMsg);
        SNMPTrace ("Unable to allocate memory for Community String\n");
        return NULL;
    }
    SNMPCopyOctetString (pV2TrapMsg->pCommunityStr, pCommString);

    /* Copy var bind list */
    pSrcVarBind = pV2TrapPdu->pVarBindList;
    while (NULL != pSrcVarBind)
    {
        pDestVarBind = SNMPCopyVarBind (pSrcVarBind);
        if (NULL == pDestVarBind)
        {
            SNMPFreeV2Trap (pV2TrapMsg);
            return SNMP_FAILURE;
        }
        if (NULL == pV2TrapMsg->pVarBindList)
        {
            pV2TrapMsg->pVarBindList = pDestVarBind;
            pV2TrapMsg->pVarBindEnd = pDestVarBind;
        }
        else
        {
            if (pV2TrapMsg->pVarBindEnd != NULL)
            {
                pV2TrapMsg->pVarBindEnd->pNextVarBind = pDestVarBind;
            }
            pV2TrapMsg->pVarBindEnd = pDestVarBind;
        }
        pSrcVarBind = pSrcVarBind->pNextVarBind;
    }
    return (pV2TrapMsg);
}

/*********************************************************************
 *  Function Name : SNMPV1TraptoV3Trap
 *  Description   : This function translates v2 response pdu to v2
 *                  response pdu.
 *  Parameter(s)  : pV3TrapPdu - PDU Pointer
 *                  pSingleTargetOut - Target Parameter to which the 
 *                  request is to be forwarded.
 *  Return Values : v3 Request PDU or NULL
 *********************************************************************/
tSNMP_V3PDU        *
SNMPV1TraptoV3Trap (tSNMP_TRAP_PDU * pV1TrapPdu,
                    tSNMP_OCTET_STRING_TYPE * pSingleTargetOut,
                    UINT4 u4RequestID)
{
    tSNMP_V3PDU        *pV3TrapMsg = NULL;
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    tSnmpTgtParamEntry *pSnmpTgtParamsEntry = NULL;
    tCommunityMappingEntry *pCommunityEntryIn = NULL;
    tCommunityMappingEntry *pCommunityEntryOut = NULL;
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    tSNMP_VAR_BIND     *pSrcVarBind = NULL;
    tSNMP_VAR_BIND     *pDestVarBind = NULL;
    tSNMP_NORMAL_PDU   *pV2TrapMsg = NULL;

    if ((NULL == pV1TrapPdu) || (NULL == pSingleTargetOut))
        return NULL;

    pV2TrapMsg = SNMPV1TrapToV2Trap (pV1TrapPdu, pSingleTargetOut,
                                     u4RequestID, SNMP_TRUE);
    if (NULL == pV2TrapMsg)
    {
        return NULL;
    }

    pSnmpTgtAddrEntry = SNMPGetTgtAddrEntry (pSingleTargetOut);
    if (pSnmpTgtAddrEntry == NULL)
    {
        SNMPFreeV2Trap (pV2TrapMsg);
        return NULL;
    }

    pSnmpTgtParamsEntry =
        SNMPGetTgtParamEntry (&(pSnmpTgtAddrEntry->AddrParams));
    if (NULL == pSnmpTgtParamsEntry)
    {
        SNMPFreeV2Trap (pV2TrapMsg);
        return NULL;
    }

    /*Get community entry from v1 message community */
    pCommunityEntryIn =
        SNMPGetCommunityEntryFromName (pV1TrapPdu->pCommunityStr);

    if (NULL == pCommunityEntryIn)
    {
        SNMPFreeV2Trap (pV2TrapMsg);
        return NULL;
    }

    pCommunityEntryOut =
        SNMPGetCommunityEntryFromSecNameContextID (&
                                                   (pSnmpTgtParamsEntry->
                                                    ParamSecName),
                                                   &(pCommunityEntryIn->
                                                     ContextEngineID));

    if (NULL == pCommunityEntryOut)
    {
        SNMPFreeV2Trap (pV2TrapMsg);
        return NULL;
    }

    pSnmpUsmEntry = SNMPGetUsmEntry
        (&gSnmpEngineID, &pSnmpTgtParamsEntry->ParamSecName);

    if (NULL == pSnmpUsmEntry)
    {
        SNMPFreeV2Trap (pV2TrapMsg);
        return NULL;
    }

    pV3TrapMsg = SNMPFillV3Params (pSnmpUsmEntry,
                                   &pCommunityEntryOut->ContextName);

    if (pV3TrapMsg != NULL)
    {
        SNMPCopyOctetString (&(pV3TrapMsg->ContextID),
                             &(pCommunityEntryIn->ContextEngineID));

        pV3TrapMsg->u4MsgSecModel =
            (UINT4) pSnmpTgtParamsEntry->i2ParamSecModel;

        /* Copy other parameters of normal pdu */
        pV3TrapMsg->Normalpdu.i2_PduType = SNMP_PDU_TYPE_SNMPV2_TRAP;
        pV3TrapMsg->Normalpdu.u4_Version = VERSION3;
        pV3TrapMsg->Normalpdu.u4_RequestID = u4RequestID;

        /* Copy var bind list */
        pSrcVarBind = pV2TrapMsg->pVarBindList;
        while (NULL != pSrcVarBind)
        {
            pDestVarBind = SNMPCopyVarBind (pSrcVarBind);
            if (NULL == pDestVarBind)
            {
                FREE_SNMP_VARBIND_LIST (pV3TrapMsg->Normalpdu.pVarBindList);
                MemReleaseMemBlock (gSnmpV3PduPoolId, (UINT1 *) pV3TrapMsg);
                SNMPFreeV2Trap (pV2TrapMsg);
                return NULL;
            }
            if (NULL == pV3TrapMsg->Normalpdu.pVarBindList)
            {
                pV3TrapMsg->Normalpdu.pVarBindList = pDestVarBind;
                pV3TrapMsg->Normalpdu.pVarBindEnd = pDestVarBind;
            }
            else
            {
                pV3TrapMsg->Normalpdu.pVarBindEnd->pNextVarBind = pDestVarBind;
                pV3TrapMsg->Normalpdu.pVarBindEnd = pDestVarBind;
            }
            pDestVarBind = NULL;
            pSrcVarBind = pSrcVarBind->pNextVarBind;
        }
    }
    else
    {
        SNMPFreeV2Trap (pV2TrapMsg);
        return NULL;
    }
    SNMPFreeV2Trap (pV2TrapMsg);

    return pV3TrapMsg;
}

/*********************************************************************
 *  Function Name : SNMPV3TraptoV3Trap
 *  Description   : This function translates v2 response pdu to v2
 *                  response pdu.
 *  Parameter(s)  : pV3TrapPdu - PDU Pointer
 *                  pSingleTargetOut - Target Parameter to which the 
 *                  request is to be forwarded.
 *  Return Values : v3 Request PDU or NULL
 *********************************************************************/
tSNMP_V3PDU        *
SNMPV3TraptoV3Trap (tSNMP_V3PDU * pV3TrapPdu,
                    tSNMP_OCTET_STRING_TYPE * pSingleTargetOut,
                    UINT4 u4RequestID)
{
    tSNMP_V3PDU        *pV3TrapMsg = NULL;
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    tSnmpTgtParamEntry *pSnmpTgtParamsEntry = NULL;
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;

    if ((NULL == pV3TrapPdu) || (NULL == pSingleTargetOut))
        return NULL;

    pSnmpTgtAddrEntry = SNMPGetTgtAddrEntry (pSingleTargetOut);
    if (pSnmpTgtAddrEntry == NULL)
        return NULL;

    pSnmpTgtParamsEntry =
        SNMPGetTgtParamEntry (&(pSnmpTgtAddrEntry->AddrParams));
    if (NULL == pSnmpTgtParamsEntry)
    {
        return NULL;
    }

    pSnmpUsmEntry = SNMPGetUsmEntry
        (&pV3TrapPdu->ContextID, &pSnmpTgtParamsEntry->ParamSecName);

    if (NULL == pSnmpUsmEntry)
    {
        return NULL;
    }

    pV3TrapMsg = SNMPFillV3Params (pSnmpUsmEntry, &pV3TrapPdu->ContextName);

    if (pV3TrapMsg != NULL)
    {
        pV3TrapMsg->u4MsgSecModel =
            (UINT4) pSnmpTgtParamsEntry->i2ParamSecModel;

        /* Copy other parameters of normal pdu */
        if (SNMP_FAILURE ==
            SNMPCopyNormalPduParams (&pV3TrapMsg->Normalpdu,
                                     &pV3TrapPdu->Normalpdu))
        {
            MemReleaseMemBlock (gSnmpV3PduPoolId, (UINT1 *) pV3TrapMsg);
            return NULL;
        }
        pV3TrapMsg->Normalpdu.u4_Version = VERSION3;
        pV3TrapMsg->Normalpdu.u4_RequestID = u4RequestID;

        /*Copy Engine Id from Agent */
        SNMPCopyOctetString (&pV3TrapMsg->MsgSecParam.MsgEngineID,
                             &gSnmpEngineID);

        SNMPCopyOctetString (&(pV3TrapMsg->ContextID),
                             &(pV3TrapPdu->ContextID));
    }

    return pV3TrapMsg;
}

/*********************************************************************
 *  Function Name : SNMPCopyNormalPduParams
 *  Description   : This function copies normal pdu from version with
 *                  common feilds.
 *  Parameter(s)  : pDest - Destination pointer
 *                  pSrc - Source pointer
 *  Return Values : v3 Request PDU or NULL
 *********************************************************************/

INT1
SNMPCopyNormalPduParams (tSNMP_NORMAL_PDU * pDest, tSNMP_NORMAL_PDU * pSrc)
{
    tSNMP_VAR_BIND     *pSrcVarBind = NULL;
    tSNMP_VAR_BIND     *pDestVarBind = NULL;

    if ((NULL == pDest) || (NULL == pSrc))
        return SNMP_FAILURE;

    /* Copy other parameters of normal pdu */
    pDest->i2_PduType = pSrc->i2_PduType;
    pDest->i4_ErrorIndex = pSrc->i4_ErrorIndex;
    pDest->i4_ErrorStatus = pSrc->i4_ErrorStatus;
    pDest->i4_MaxRepetitions = pSrc->i4_MaxRepetitions;
    pDest->i4_NonRepeaters = pSrc->i4_NonRepeaters;
    pDest->u4_Version = pSrc->u4_Version;

    /* Copy var bind list */
    pSrcVarBind = pSrc->pVarBindList;
    while (NULL != pSrcVarBind)
    {
        pDestVarBind = SNMPCopyVarBind (pSrcVarBind);
        if (NULL == pDestVarBind)
        {
            FREE_SNMP_VARBIND_LIST (pDest->pVarBindList);
            return SNMP_FAILURE;
        }
        if (NULL == pDest->pVarBindList)
        {
            pDest->pVarBindList = pDestVarBind;
            pDest->pVarBindEnd = pDestVarBind;
        }
        else
        {
            pDest->pVarBindEnd->pNextVarBind = pDestVarBind;
            pDest->pVarBindEnd = pDestVarBind;
        }
        pDestVarBind = NULL;
        pSrcVarBind = pSrcVarBind->pNextVarBind;
    }

    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : SNMPProxyFreePdu 
 *  Description     : Function to start the proxy request timer. 
 *  Input           : pCacheData - Cache node 
 *                    pSnmpTgtAddrEntry - pointer to Target addr entry
 *  Output          : None 
 *  Returns         : SNMP_SUCCESS, SNMP_FAILURE 
 ************************************************************************/

VOID
SNMPProxyFreePdu (VOID *pPdu, INT4 Version, INT2 i2PduType)
{
    tSNMP_TRAP_PDU     *pV1Trap = NULL;
    tSNMP_NORMAL_PDU   *pNormalPdu = NULL;
    tSNMP_V3PDU        *pV3Pdu = NULL;

    if (NULL == pPdu)
        return;

    if ((VERSION1 == Version) && (i2PduType == SNMP_PDU_TYPE_TRAP))
    {
        pV1Trap = (tSNMP_TRAP_PDU *) pPdu;
        SNMPFreeV1Trap (pV1Trap);
    }
    else if ((VERSION1 == Version) || (VERSION2 == Version))
    {
        pNormalPdu = (tSNMP_NORMAL_PDU *) pPdu;
        SnmpFreeV2Request (pNormalPdu);
    }
    else if (VERSION3 == Version)
    {
        pV3Pdu = (tSNMP_V3PDU *) pPdu;
        SnmpFreeV3Pdu (pV3Pdu);
    }
}

/************************************************************************
 *  Function Name   : SNMPProxyFormFailResp 
 *  Description     : Function sends failure response to manager in case 
 *                    translation validation failure.
 *                    inform response. 
 *  Input           : 
 *  Output          : None 
 *  Returns         : SNMP_SUCCESS, SNMP_FAILURE 
 ************************************************************************/
INT1
SNMPProxyFormFailResp (VOID *pPdu,
                       UINT4 u4pduVersion,
                       UINT1 **pEncodedMsg, UINT4 *pEncodedMsgLen)
{
    tSNMP_NORMAL_PDU   *pNormalPdu = NULL;
    tSNMP_V3PDU        *pV3Pdu = NULL;
    tSNMP_NORMAL_PDU   *pOutNormalPdu = NULL;
    tSNMP_V3PDU        *pOutV3Pdu = NULL;
    tSNMP_VAR_BIND     *pVarPtr = NULL;
    tSNMP_OID_TYPE      ProxyDropOid;
    UINT4               ui4SnmpProxyDrops[] = { 1, 3, 6, 1, 2, 1, 11, 32 };

    if (NULL == pPdu)
        return SNMP_FAILURE;

    if ((VERSION1 == u4pduVersion) || (VERSION2 == u4pduVersion))
    {
        pNormalPdu = (tSNMP_NORMAL_PDU *) pPdu;
    }
    else
    {
        pV3Pdu = (tSNMP_V3PDU *) pPdu;
    }

    SNMPTrace ("[PROXY]:Incrementing the Proxy Drops Counter \n");
    SNMP_INR_PROXY_DROPS;

    if (pNormalPdu)
    {
        pOutNormalPdu = MemAllocMemBlk (gSnmpNormalPduPoolId);
        if (pOutNormalPdu == NULL)
        {
            SNMPTrace
                ("[PROXY]:Unable to allocate Memory for pOutNormalPdu \n");
            return SNMP_FAILURE;
        }
        MEMSET (pOutNormalPdu, SNMP_ZERO, sizeof (tSNMP_NORMAL_PDU));

        pOutNormalPdu->i2_PduType = SNMP_PDU_TYPE_GET_RESPONSE;
        pOutNormalPdu->u4_Version = pNormalPdu->u4_Version;
        pOutNormalPdu->u4_RequestID = pNormalPdu->u4_RequestID;
        if (SNMP_ERR_INCONSISTENT_VALUE == pNormalPdu->i4_ErrorStatus)
        {
            pOutNormalPdu->i4_ErrorStatus = SNMP_ERR_INCONSISTENT_VALUE;
        }
        else
        {
            pOutNormalPdu->i4_ErrorStatus = SNMP_ERR_GEN_ERR;
        }
        pOutNormalPdu->i4_ErrorIndex = 1;
        pOutNormalPdu->pCommunityStr =
            allocmem_octetstring (SNMP_MAX_OCTETSTRING_SIZE);
        if (pOutNormalPdu->pCommunityStr == NULL)
        {
            SnmpFreeV2Request (pOutNormalPdu);
            SNMPTrace
                ("[PROXY]:Unable to allocate memory for Community String \n");
            return SNMP_FAILURE;
        }

        SNMPCopyOctetString (pOutNormalPdu->pCommunityStr,
                             pNormalPdu->pCommunityStr);

        /* Filling the ProxyDrop OID and its value */

        pOutNormalPdu->pVarBindList = SNMPAllocVarBind ();
        if (pOutNormalPdu->pVarBindList == NULL)
        {
            SnmpFreeV2Request (pOutNormalPdu);
            SNMPTrace
                ("[PROXY]:Unable to allocate memory for pOutNormalPdu->pVarBindList \n");
            return SNMP_FAILURE;
        }
        pVarPtr = pOutNormalPdu->pVarBindList;

        ProxyDropOid.u4_Length = 8;
        ProxyDropOid.pu4_OidList = ui4SnmpProxyDrops;

        SNMPCopyOid (pVarPtr->pObjName, &ProxyDropOid);

        pVarPtr->ObjValue.i2_DataType = SNMP_DATA_TYPE_COUNTER32;
        pVarPtr->ObjValue.u4_ULongValue = gSnmpStat.u4SnmpProxyDrops;

        if (SNMP_NOT_OK == SNMPEncodeGetPacket (pOutNormalPdu,
                                                pEncodedMsg,
                                                (INT4 *) pEncodedMsgLen,
                                                (UINT4)
                                                (SNMP_PDU_GET_RESPONSE_MASK &
                                                 pOutNormalPdu->i2_PduType)))
        {
            FREE_OCTET_STRING (pOutNormalPdu->pCommunityStr);
            MemReleaseMemBlock (gSnmpNormalPduPoolId, (UINT1 *) pOutNormalPdu);
            SNMPTrace ("[PROXY]:Unable to Encode \n");
            return SNMP_FAILURE;
        }

        FREE_OCTET_STRING (pOutNormalPdu->pCommunityStr);
        MemReleaseMemBlock (gSnmpNormalPduPoolId, (UINT1 *) pOutNormalPdu);
    }
    else
    {
        pOutV3Pdu = SNMPAllocV3Pdu ();
        if (NULL == pOutV3Pdu)
        {
            return SNMP_FAILURE;
        }
        SNMPCopyOctetString (&pOutV3Pdu->ContextID, &pV3Pdu->ContextID);
        SNMPCopyOctetString (&pOutV3Pdu->ContextName, &pV3Pdu->ContextName);
        SNMPCopyOctetString (&pOutV3Pdu->MsgFlag, &pV3Pdu->MsgFlag);
        SNMPCopyOctetString (&pOutV3Pdu->MsgSecParam.MsgAuthParam,
                             &pV3Pdu->MsgSecParam.MsgAuthParam);
        SNMPCopyOctetString (&pOutV3Pdu->MsgSecParam.MsgPrivParam,
                             &pV3Pdu->MsgSecParam.MsgPrivParam);
        SNMPCopyOctetString (&pOutV3Pdu->MsgSecParam.MsgEngineID,
                             &pV3Pdu->MsgSecParam.MsgEngineID);
        SNMPCopyOctetString (&pOutV3Pdu->MsgSecParam.MsgUsrName,
                             &pV3Pdu->MsgSecParam.MsgUsrName);
        pOutV3Pdu->MsgSecParam.u4MsgEngineBoot =
            pV3Pdu->MsgSecParam.u4MsgEngineBoot;
        pOutV3Pdu->MsgSecParam.u4MsgEngineTime =
            pV3Pdu->MsgSecParam.u4MsgEngineTime;
        pOutV3Pdu->u4MsgID = pV3Pdu->u4MsgID;
        pOutV3Pdu->u4MsgMaxSize = pV3Pdu->u4MsgMaxSize;
        pOutV3Pdu->u4MsgSecModel = pV3Pdu->u4MsgSecModel;

        pOutV3Pdu->Normalpdu.i2_PduType = SNMP_PDU_TYPE_GET_RESPONSE;
        pOutV3Pdu->Normalpdu.u4_Version = VERSION3;
        pOutV3Pdu->Normalpdu.u4_RequestID = pV3Pdu->Normalpdu.u4_RequestID;
        if (SNMP_ERR_INCONSISTENT_VALUE == pV3Pdu->Normalpdu.i4_ErrorStatus)
        {
            pOutV3Pdu->Normalpdu.i4_ErrorStatus = SNMP_ERR_INCONSISTENT_VALUE;
        }
        else
        {
            pOutV3Pdu->Normalpdu.i4_ErrorStatus = SNMP_ERR_GEN_ERR;
        }
        pOutV3Pdu->Normalpdu.i4_ErrorIndex = 1;

        /* Filling the ProxyDrop OID and its value */
        pOutV3Pdu->Normalpdu.pVarBindList = SNMPAllocVarBind ();
        if (pOutV3Pdu->Normalpdu.pVarBindList == NULL)
        {
            MemReleaseMemBlock (gSnmpV3PduPoolId, (UINT1 *) pOutV3Pdu);
            SNMPTrace
                ("[PROXY]:Unable to allocate memory to fill ProxyDrop OID\n");
            return SNMP_FAILURE;
        }
        pVarPtr = pOutV3Pdu->Normalpdu.pVarBindList;

        ProxyDropOid.u4_Length = 8;
        ProxyDropOid.pu4_OidList = ui4SnmpProxyDrops;

        SNMPCopyOid (pVarPtr->pObjName, &ProxyDropOid);

        pVarPtr->ObjValue.i2_DataType = SNMP_DATA_TYPE_COUNTER32;
        pVarPtr->ObjValue.u4_ULongValue = gSnmpStat.u4SnmpProxyDrops;

        if (SNMP_NOT_OK == SNMPEncodeV3Message (pOutV3Pdu,
                                                pEncodedMsg,
                                                (INT4 *) pEncodedMsgLen,
                                                (UINT4)
                                                (SNMP_PDU_GET_RESPONSE_MASK &
                                                 pOutV3Pdu->Normalpdu.
                                                 i2_PduType)))
        {
            MemReleaseMemBlock (gSnmpV3PduPoolId, (UINT1 *) pOutV3Pdu);
            SNMPTrace ("[PROXY]:Unable to Encode \n");
            return SNMP_FAILURE;
        }

        MemReleaseMemBlock (gSnmpV3PduPoolId, (UINT1 *) pOutV3Pdu);
    }
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : SNMPCopyV3ParamsFromCache 
 *  Description     : Function copies v3 params from cache 
 *  Input           : pCacheData - Cache node 
 *                    pV3Pdu - v3 pdu pointer
 *  Output          : None 
 *  Returns         : SNMP_SUCCESS, SNMP_FAILURE 
 ************************************************************************/
INT1
SNMPCopyV3ParamsFromCache (tSNMP_V3PDU * pV3Pdu, tSNMP_PROXY_CACHE * pCacheData)
{
    tSNMP_PROXY_V3SP_CACHE *pV3Cache = NULL;

    pV3Cache = &pCacheData->cacheInfo.ProxyV3SpCache;

    SNMPProxyCopyOctetStringFromArr (&pV3Pdu->ContextID,
                                     &pV3Cache->RecContextID);

    SNMPProxyCopyOctetStringFromArr (&pV3Pdu->ContextName,
                                     &pV3Cache->RecContextName);

    SNMPProxyCopyOctetStringFromArr (&pV3Pdu->MsgFlag, &pV3Cache->RecMsgFlag);

    SNMPProxyCopyOctetStringFromArr (&pV3Pdu->MsgSecParam.MsgUsrName,
                                     &pV3Cache->MsgUsrName);

    pV3Pdu->u4MsgID = pV3Cache->u4RecMsgID;
    pV3Pdu->u4MsgMaxSize = pV3Cache->u4RecMsgMaxSize;
    pV3Pdu->u4MsgSecModel = pV3Cache->u4RecMsgSecModel;

    if ((pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO] & SNMP_ENCY_BIT) ==
        SNMP_ENCY_BIT)
    {
        pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO] = 0x03;
        pV3Pdu->MsgFlag.i4_Length = 1;
        SNMPGenRandomIV ((UINT4 *) (VOID *) pV3Pdu->MsgSecParam.MsgPrivParam.
                         pu1_OctetList);
        pV3Pdu->MsgSecParam.MsgPrivParam.i4_Length = 8;
    }

    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : SNMPAllocV3Pdu 
 *  Description     : Function  allocates memory for v3 params
 *  Input           : pCacheData - Cache node 
 *                    pV3Pdu - v3 pdu pointer
 *  Output          : None 
 *  Returns         : SNMP_SUCCESS, SNMP_FAILURE 
 ************************************************************************/

tSNMP_V3PDU        *
SNMPAllocV3Pdu (VOID)
{
    UINT1              *pu1Data = NULL;
    tSNMP_V3PDU        *pV3Pdu = NULL;

    pu1Data = MemAllocMemBlk (gSnmpV3PduPoolId);

    if (pu1Data == NULL)
    {
        SNMPTrace ("Unable to Allocate memory for V3 Request \n");
        return NULL;
    }

    MEMSET (pu1Data, SNMP_ZERO,
            (sizeof (tSNMP_V3PDU) + (7 * SNMP_MAX_OCTETSTRING_SIZE)));
    pV3Pdu = (tSNMP_V3PDU *) (VOID *) pu1Data;
    pu1Data += sizeof (tSNMP_V3PDU);
    pV3Pdu->MsgSecParam.MsgEngineID.pu1_OctetList = pu1Data;
    pu1Data += SNMP_MAX_OCTETSTRING_SIZE;
    pV3Pdu->MsgSecParam.MsgUsrName.pu1_OctetList = pu1Data;
    pu1Data += SNMP_MAX_OCTETSTRING_SIZE;
    pV3Pdu->MsgSecParam.MsgAuthParam.pu1_OctetList = pu1Data;
    pu1Data += SNMP_MAX_OCTETSTRING_SIZE;
    pV3Pdu->MsgSecParam.MsgPrivParam.pu1_OctetList = pu1Data;
    pu1Data += SNMP_MAX_OCTETSTRING_SIZE;
    pV3Pdu->MsgFlag.pu1_OctetList = pu1Data;
    pu1Data += SNMP_MAX_OCTETSTRING_SIZE;
    pV3Pdu->ContextName.pu1_OctetList = pu1Data;
    pu1Data += SNMP_MAX_OCTETSTRING_SIZE;
    pV3Pdu->ContextID.pu1_OctetList = pu1Data;

    return pV3Pdu;
}

/************************************************************************
 *  Function Name   : SNMPProxyCompareOidtoarr 
 *  Description     : Function  compares oid types
 *  Input           : pOID1 - OID1 
 *                    pOID2 - OID2
 *  Output          : None 
 *  Returns         : SNMP_LESSER, SNMP_GREATER, SNMP_EQUAL  
 ************************************************************************/
UINT4
SNMPProxyCompareOidtoarr (tSNMP_OID_TYPE * pOID1, UINT4 *pOID2, UINT4 u4OID2Len)
{
    UINT4               u4Index = SNMP_ZERO;

    if (pOID1->u4_Length < u4OID2Len)
    {
        return SNMP_LESSER;
    }
    else if (pOID1->u4_Length > u4OID2Len)
    {
        return SNMP_GREATER;
    }

    for (u4Index = SNMP_ZERO; u4Index < u4OID2Len; u4Index++)
    {
        if (pOID1->pu4_OidList[u4Index] < pOID2[u4Index])
        {
            return SNMP_LESSER;
        }
        else if (pOID1->pu4_OidList[u4Index] > pOID2[u4Index])
        {
            return SNMP_GREATER;
        }
    }

    return SNMP_EQUAL;
}
