/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: vacmview.h,v 1.6 2016/03/18 13:04:29 siva Exp $
 * 
 *
 * Description: prototypes and macros for VACM View Table 
 *              Database Model 
 *******************************************************************/

#ifndef _VACMVIEW_H
#define _VACMVIEW_H

#define SNMP_VACM_VIEW_TREE_TYPE_INCLUDED    1
#define SNMP_VACM_VIEW_TREE_TYPE_EXCLUDED    2

UINT1       au1Mask[SNMP_MAX_VIEW_TREE][SNMP_MAX_OCTETSTRING_SIZE];
tViewTree   gaViewTree[SNMP_MAX_VIEW_TREE];
UINT4       au4SubOid[SNMP_MAX_VIEW_TREE][MAX_OID_LENGTH];
tTMO_SLL         gSnmpViewTree;  

VOID VACMAddSnmpViewSll(tTMO_SLL_NODE *);
VOID SNMPDelVacmViewEntrySll(tTMO_SLL_NODE *);

#endif /* _VACMVIEW_H */
