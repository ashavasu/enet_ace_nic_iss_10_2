/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: snmpmem.c,v 1.5 2015/04/28 12:35:02 siva Exp $
 *
 * Description:Routines for mempool management 
 *******************************************************************/

#include "snmpcmn.h"
#include "snmpmem.h"
#include "snxinc.h"

/************************************************************************
 *  Function Name   : SNMPMemPoolInit
 *  Description     : Init snmp memory pool pointer to the starting.
 *                    This will happen for each snmp request.
 *  Input           : None
 *  Output          : None
 *  Returns         : None 
 ************************************************************************/

INT1
SNMPMemPoolInit ()
{
    /* Dummy pointers for system sizing during run time */
    tSnmpGetVarBindBlock *pSnmpGetVarBindBlock = NULL;
    tSnmpPktBlock      *pSnmpPktBlock = NULL;
    tSnmpV3PduBlock    *pSnmpV3PduBlock = NULL;
    tSnmpTcpRcvBufBlock *pSnmpTcpRcvBufBlock = NULL;
    tSnmpOidListBlock  *pSnmpOidListBlock = NULL;
    tSnmpOctetListBlock *pSnmpOctetListBlock = NULL;
    tSnmpMultiDataIndexBlock *pSnmpMultiDataIndexBlock = NULL;
    tSnmpMultiOidBlock *pSnmpMultiOidBlock = NULL;

    UNUSED_PARAM (pSnmpGetVarBindBlock);
    UNUSED_PARAM (pSnmpPktBlock);
    UNUSED_PARAM (pSnmpV3PduBlock);
    UNUSED_PARAM (pSnmpTcpRcvBufBlock);
    UNUSED_PARAM (pSnmpOidListBlock);
    UNUSED_PARAM (pSnmpOctetListBlock);
    UNUSED_PARAM (pSnmpMultiDataIndexBlock);
    UNUSED_PARAM (pSnmpMultiOidBlock);

    if (Snmpv3SizingMemCreateMemPools () == OSIX_FAILURE)
    {
        SNMPTrace ("Memory Pool creation Failed \n");
        return SNMP_FAILURE;
    }
    gSnmpGetVarBindPoolId =
        SNMPV3MemPoolIds[MAX_SNMP_GET_VARBIND_BLOCKS_SIZING_ID];
    gSnmpPktPoolId = SNMPV3MemPoolIds[MAX_SNMP_PKT_POOL_BLOCKS_SIZING_ID];
    gSnmpNormalPduPoolId =
        SNMPV3MemPoolIds[MAX_SNMP_NORMAL_PDU_BLOCKS_SIZING_ID];
    gSnmpTrapPduPoolId = SNMPV3MemPoolIds[MAX_SNMP_TRAP_PDU_BLOCKS_SIZING_ID];
    gSnmpV3PduPoolId = SNMPV3MemPoolIds[MAX_SNMP_V3PDU_BLOCKS_SIZING_ID];
    gSnmpInformNodePoolId =
        SNMPV3MemPoolIds[MAX_SNMP_INFORM_NODE_BLOCKS_SIZING_ID];
    gSnmpInformMsgNodePoolId =
        SNMPV3MemPoolIds[MAX_SNMP_INFORM_MSG_NODE_BLOCKS_SIZING_ID];
    gSnmpTcpRcvBufPoolId =
        SNMPV3MemPoolIds[MAX_SNMP_TCP_RCV_BUF_BLOCKS_SIZING_ID];
    gSnmpTcpTxNodePoolId =
        SNMPV3MemPoolIds[MAX_SNMP_TCP_TX_NODE_BLOCKS_SIZING_ID];
    gSnmpVarBindPoolId = SNMPV3MemPoolIds[MAX_SNMP_VARBIND_BLOCKS_SIZING_ID];
    gSnmpOidTypePoolId = SNMPV3MemPoolIds[MAX_SNMP_OID_TYPE_BLOCKS_SIZING_ID];
    gSnmpOidListPoolId = SNMPV3MemPoolIds[MAX_SNMP_OID_LIST_BLOCKS_SIZING_ID];
    gSnmpOctetStrPoolId = SNMPV3MemPoolIds[MAX_SNMP_OCTET_STR_BLOCKS_SIZING_ID];
    gSnmpOctetListPoolId =
        SNMPV3MemPoolIds[MAX_SNMP_OCTET_LIST_BLOCKS_SIZING_ID];
    gSnmpMultiDataPoolId =
        SNMPV3MemPoolIds[MAX_SNMP_MULTI_DATA_BLOCKS_SIZING_ID];
    gSnmpMultiIndexPoolId = SNMPV3MemPoolIds[MAX_SNMP_INDEX_BLOCKS_SIZING_ID];
    gSnmpMultiDataIndexPoolId =
        SNMPV3MemPoolIds[MAX_SNMP_MULTI_DATA_INDEX_BLOCKS_SIZING_ID];
    gSnmpMultiOidPoolId = SNMPV3MemPoolIds[MAX_SNMP_MULTI_OID_BLOCKS_SIZING_ID];
    gSnmpAgentxOidTypePoolId =
        SNMPV3MemPoolIds[MAX_SNMP_AGENTX_OID_BLOCKS_SIZING_ID];
    gSnmpSearchRngPoolId =
        SNMPV3MemPoolIds[MAX_SNMP_SEARCH_RANGE_BLOCKS_SIZING_ID];
    gSnmpAgentxVarBindPoolId =
        SNMPV3MemPoolIds[MAX_SNMP_AGENTX_VARBIND_BLOCKS_SIZING_ID];
    gSnmpTrapFilterPoolId =
        SNMPV3MemPoolIds[MAX_SNMP_TRAP_FILTER_TABLE_ENTRIES_SIZING_ID];
    gSnmpDataPoolId = SNMPV3MemPoolIds[MAX_SNMP_DATA_BLOCKS_SIZING_ID];
    gSnmpAuditInfoPoolId =
        SNMPV3MemPoolIds[MAX_SNMP_AUDIT_INFO_BLOCKS_SIZING_ID];
    gSnmpTagListPoolId = SNMPV3MemPoolIds[MAX_SNMP_TAG_LIST_BLOCKS_SIZING_ID];
    gSnmpAgentParamsPoolId =
        SNMPV3MemPoolIds[MAX_SNMP_AGENT_PARAM_BLOCKS_SIZING_ID];

    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : SNMPMemInit
 *  Description     : Init snmp memory pool pointer to the starting.
 *                    This will happen for each snmp request.
 *  Input           : None
 *  Output          : None
 *  Returns         : None 
 ************************************************************************/

INT1
SNMPMemInit ()
{
    UINT4               u4Count = SNMP_ZERO;

    MEMSET (gaMemPool, SNMP_ZERO, (sizeof (tMemBlock) * MAX_VARBIND));

    for (u4Count = SNMP_ZERO; u4Count < MAX_VARBIND; u4Count++)
        gaMemPool[u4Count].pNext = &(gaMemPool[u4Count + SNMP_ONE]);
    gaMemPool[MAX_VARBIND - SNMP_ONE].pNext = NULL;
    pCur = &(gaMemPool[SNMP_ONE]);
    SNMPIndexInit ();

    if (gi4SnmpMemInit == SNMP_FAILURE)
    {
        if (SNMPMemPoolInit () == SNMP_FAILURE)
        {
            SNMPTrace ("Memory Pool creation Failed \n");

            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : SNMPAllocVarBind 
 *  Description     : Allocate Variable Bind from Variable bind pool
 *  Input           : None
 *  Output          : None
 *  Returns         : Variable Bind pointer or NULL
 ************************************************************************/

tSNMP_VAR_BIND     *
SNMPAllocVarBind ()
{
    tSNMP_VAR_BIND     *pPtr = NULL;

    if (pCur != NULL)
    {
        pCur->VarBind.pObjName = &(pCur->Oid);
        pCur->VarBind.pObjName->pu4_OidList = (pCur->OidList);
        pCur->VarBind.ObjValue.pOidValue = &(pCur->DataOid);
        pCur->VarBind.ObjValue.pOctetStrValue = &(pCur->Octet);
        pCur->VarBind.ObjValue.pOidValue->pu4_OidList
            = (UINT4 *) (VOID *) (pCur->DataList);
        pCur->VarBind.ObjValue.pOctetStrValue->pu1_OctetList = (pCur->DataList);
        pPtr = &(pCur->VarBind);
        pCur = pCur->pNext;
        ((tMemBlock *) pPtr)->pNext = (tMemBlock *) SIGN;
    }
    return pPtr;
}

/************************************************************************
 *  Function Name   : SNMPFreeVarBind 
 *  Description     : Free the given Variable Bind pointer from Variable
 *                    bind pool
 *  Input           : None
 *  Output          : None
 *  Returns         : None 
 ************************************************************************/

VOID
SNMPFreeVarBind (tSNMP_VAR_BIND * pPtr)
{
    tMemBlock          *pNew = NULL;

    if (pPtr == NULL)
    {
        return;
    }
    pPtr->pNextVarBind = NULL;
    pNew = (tMemBlock *) pPtr;
    pNew->pNext = pCur;
    pCur = pNew;
    pPtr = NULL;
}

/************************************************************************
 *  Function Name   : SNMPGetFirstIndexPool 
 *  Description     : Get First Index Pool Pointer
 *  Input           : None
 *  Output          : None
 *  Returns         : Index Pool Pointer 
 ************************************************************************/

tSnmpIndex         *
SNMPGetFirstIndexPool ()
{
    return gaIndexPool1;
}

/************************************************************************
 *  Function Name   : SNMPGetSecondIndexPool 
 *  Description     : Get Second Index Pool Pointer
 *  Input           : None
 *  Output          : None
 *  Returns         : Index Pool Pointer 
 ************************************************************************/

tSnmpIndex         *
SNMPGetSecondIndexPool ()
{
    return gaIndexPool2;
}

/************************************************************************
 *  Function Name   : SNMPIndexInit 
 *  Description     : Function Init Index Pool  
 *  Input           : None
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
PRIVATE VOID
SNMPIndexInit ()
{
    UINT4               u4Count = SNMP_ZERO;
    for (u4Count = SNMP_ZERO; u4Count < SNMP_MAX_INDICES_2; u4Count++)
    {
        gaIndexPool1[u4Count].pIndex = &(gaMultiPool1[u4Count]);
        gaMultiPool1[u4Count].pOctetStrValue = &(gaOctetPool1[u4Count]);
        gaMultiPool1[u4Count].pOidValue = &(gaOIDPool1[u4Count]);
        gaMultiPool1[u4Count].pOctetStrValue->pu1_OctetList
            = (UINT1 *) gau1Data1[u4Count];
        gaMultiPool1[u4Count].pOidValue->pu4_OidList
            = (UINT4 *) (VOID *) gau1Data1[u4Count];

        gaIndexPool2[u4Count].pIndex = &(gaMultiPool2[u4Count]);
        gaMultiPool2[u4Count].pOctetStrValue = &(gaOctetPool2[u4Count]);
        gaMultiPool2[u4Count].pOidValue = &(gaOIDPool2[u4Count]);
        gaMultiPool2[u4Count].pOctetStrValue->pu1_OctetList
            = (UINT1 *) gau1Data2[u4Count];
        gaMultiPool2[u4Count].pOidValue->pu4_OidList
            = (UINT4 *) (VOID *) gau1Data2[u4Count];
    }
}
