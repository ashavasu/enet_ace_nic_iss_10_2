/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: vacmcxt.h,v 1.4 2015/04/28 12:35:03 siva Exp $
 *
 * Description: Global variables for VACM Context Table Database Model 
 *******************************************************************/
#ifndef _VACMCXT_H 
#define _VACMCXT_H

tContext  gaContext[MAX_SNMP_CONTEXT];
UINT1     gau1CxtName[MAX_SNMP_CONTEXT][SNMP_MAX_OCTETSTRING_SIZE];
tTMO_SLL gSnmpContext;
VOID VACMAddContextSll(tTMO_SLL_NODE *pNode);

#endif /* _VACMCXT_H */

