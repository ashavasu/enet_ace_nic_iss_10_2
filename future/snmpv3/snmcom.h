/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: snmcom.h,v 1.4 2015/04/28 12:35:02 siva Exp $
 *
 * Description: snmp community mapping table macros and global vars
 *******************************************************************/
#ifndef _SNMCOM_H
#define _SNMCOM_H
tCommunityMappingEntry gaCommunityEntry[MAX_COMMUNITY_TABLE_ENTRY];
tTMO_SLL            gSnmpCommunityEntry;

UINT1 au1CommIndex[MAX_COMMUNITY_TABLE_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
UINT1 au1CommName[MAX_COMMUNITY_TABLE_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
UINT1 au1ComSecName[MAX_COMMUNITY_TABLE_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
UINT1 au1ContEngine[MAX_COMMUNITY_TABLE_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
UINT1 au1ContName[MAX_COMMUNITY_TABLE_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
UINT1 au1TransTag[MAX_COMMUNITY_TABLE_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];

VOID                SNMPCommunityEntryAddSll (tTMO_SLL_NODE *);
VOID                SNMPDelCommunityEntrySll (tTMO_SLL_NODE *);
#endif

