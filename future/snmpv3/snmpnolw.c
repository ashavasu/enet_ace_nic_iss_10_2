/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: snmpnolw.c,v 1.10 2017/11/20 13:11:26 siva Exp $
*
* Description: snmp notify table Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "snmpcmn.h"
# include  "snmpnolw.h"

extern UINT4        SnmpNotifyTag[11];
extern UINT4        SnmpNotifyType[11];

/* LOW LEVEL Routines for Table : SnmpNotifyTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceSnmpNotifyTable
 Input       :  The Indices
                SnmpNotifyName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceSnmpNotifyTable (tSNMP_OCTET_STRING_TYPE *
                                         pSnmpNotifyName)
{
    if (pSnmpNotifyName->i4_Length == SNMP_ZERO)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexSnmpNotifyTable
 Input       :  The Indices
                SnmpNotifyName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexSnmpNotifyTable (tSNMP_OCTET_STRING_TYPE * pSnmpNotifyName)
{
    tSnmpNotifyEntry   *pSnmpNotifyEntry = NULL;
    pSnmpNotifyEntry = SNMPGetFirstNotifyEntry ();
    if (pSnmpNotifyEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pSnmpNotifyName, &(pSnmpNotifyEntry->NotifyName));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexSnmpNotifyTable
 Input       :  The Indices
                SnmpNotifyName
                nextSnmpNotifyName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexSnmpNotifyTable (tSNMP_OCTET_STRING_TYPE * pSnmpNotifyName,
                                tSNMP_OCTET_STRING_TYPE * pNextSnmpNotifyName)
{
    tSnmpNotifyEntry   *pSnmpNotifyEntry = NULL;
    pSnmpNotifyEntry = SNMPGetNextNotifyEntry (pSnmpNotifyName);
    if (pSnmpNotifyEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pNextSnmpNotifyName, &(pSnmpNotifyEntry->NotifyName));
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetSnmpNotifyTag
 Input       :  The Indices
                SnmpNotifyName

                The Object 
                retValSnmpNotifyTag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpNotifyTag (tSNMP_OCTET_STRING_TYPE * pSnmpNotifyName,
                     tSNMP_OCTET_STRING_TYPE * pRetValSnmpNotifyTag)
{
    tSnmpNotifyEntry   *pSnmpNotifyEntry = NULL;
    pSnmpNotifyEntry = SNMPGetNotifyEntry (pSnmpNotifyName);
    if (pSnmpNotifyEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pRetValSnmpNotifyTag, &(pSnmpNotifyEntry->NotifyTag));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpNotifyType
 Input       :  The Indices
                SnmpNotifyName

                The Object 
                retValSnmpNotifyType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpNotifyType (tSNMP_OCTET_STRING_TYPE * pSnmpNotifyName,
                      INT4 *pi4RetValSnmpNotifyType)
{
    tSnmpNotifyEntry   *pSnmpNotifyEntry = NULL;
    pSnmpNotifyEntry = SNMPGetNotifyEntry (pSnmpNotifyName);
    if (pSnmpNotifyEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValSnmpNotifyType = pSnmpNotifyEntry->i4NotifyType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpNotifyStorageType
 Input       :  The Indices
                SnmpNotifyName

                The Object 
                retValSnmpNotifyStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpNotifyStorageType (tSNMP_OCTET_STRING_TYPE * pSnmpNotifyName,
                             INT4 *pi4RetValSnmpNotifyStorageType)
{
    tSnmpNotifyEntry   *pSnmpNotifyEntry = NULL;
    pSnmpNotifyEntry = SNMPGetNotifyEntry (pSnmpNotifyName);
    if (pSnmpNotifyEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValSnmpNotifyStorageType = pSnmpNotifyEntry->i4NotifyStorageType;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetSnmpNotifyRowStatus
 Input       :  The Indices
                SnmpNotifyName

                The Object 
                retValSnmpNotifyRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpNotifyRowStatus (tSNMP_OCTET_STRING_TYPE * pSnmpNotifyName,
                           INT4 *pi4RetValSnmpNotifyRowStatus)
{
    tSnmpNotifyEntry   *pSnmpNotifyEntry = NULL;
    pSnmpNotifyEntry = SNMPGetNotifyEntry (pSnmpNotifyName);
    if (pSnmpNotifyEntry == NULL)
    {
        SNMP_TRC (SNMP_FAILURE_TRC, "nmhGetSnmpNotifyRowStatus: "
                  "NULL notify entry\r\n");
        return SNMP_FAILURE;
    }
    *pi4RetValSnmpNotifyRowStatus = pSnmpNotifyEntry->i4Status;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetSnmpNotifyTag
 Input       :  The Indices
                SnmpNotifyName

                The Object 
                setValSnmpNotifyTag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpNotifyTag (tSNMP_OCTET_STRING_TYPE * pSnmpNotifyName,
                     tSNMP_OCTET_STRING_TYPE * pSetValSnmpNotifyTag)
{
    tSnmpNotifyEntry   *pSnmpNotifyEntry = NULL;
    pSnmpNotifyEntry = SNMPGetNotifyEntry (pSnmpNotifyName);
    if (pSnmpNotifyEntry == NULL)
    {
        SNMP_TRC (SNMP_FAILURE_TRC, "nmhSetSnmpNotifyTag: "
                  "NULL notify entry\r\n");
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (&(pSnmpNotifyEntry->NotifyTag), pSetValSnmpNotifyTag);
    SNMP_TRC1 (SNMP_DEBUG_TRC, "nmhSetSnmpNotifyTag: "
               "Successfully setting the tag for the notify [%s]\r\n",
               pSnmpNotifyName->pu1_OctetList);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpNotifyType
 Input       :  The Indices
                SnmpNotifyName

                The Object 
                setValSnmpNotifyType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpNotifyType (tSNMP_OCTET_STRING_TYPE * pSnmpNotifyName,
                      INT4 i4SetValSnmpNotifyType)
{
    tSnmpNotifyEntry   *pSnmpNotifyEntry = NULL;
    pSnmpNotifyEntry = SNMPGetNotifyEntry (pSnmpNotifyName);
    if (pSnmpNotifyEntry == NULL)
    {
        SNMP_TRC (SNMP_FAILURE_TRC, "nmhSetSnmpNotifyType: "
                  "NULL notify entry\r\n");
        return SNMP_FAILURE;
    }
    pSnmpNotifyEntry->i4NotifyType = i4SetValSnmpNotifyType;
    SNMP_TRC1 (SNMP_DEBUG_TRC, "nmhSetSnmpNotifyType: "
               "Successfully setting the type for the notify [%s]\r\n",
               pSnmpNotifyName->pu1_OctetList);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpNotifyStorageType
 Input       :  The Indices
                SnmpNotifyName

                The Object 
                setValSnmpNotifyStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpNotifyStorageType (tSNMP_OCTET_STRING_TYPE * pSnmpNotifyName,
                             INT4 i4SetValSnmpNotifyStorageType)
{
    tSnmpNotifyEntry   *pSnmpNotifyEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pSnmpNotifyEntry = SNMPGetNotifyEntry (pSnmpNotifyName);
    if (pSnmpNotifyEntry == NULL)
    {
        SNMP_TRC (SNMP_FAILURE_TRC, "nmhSetSnmpNotifyStorageType: "
                  "NULL notify entry\r\n");
        return SNMP_FAILURE;
    }
    pSnmpNotifyEntry->i4NotifyStorageType = i4SetValSnmpNotifyStorageType;

    /* Send notifications to MSR for all the objects in the table */
    SnmpNotifyInfo.pu4ObjectId = SnmpNotifyTag;
    SnmpNotifyInfo.u4OidLen = sizeof (SnmpNotifyTag) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%s %s", pSnmpNotifyName,
                          &(pSnmpNotifyEntry->NotifyTag)));

    /* As the oidlen, lock and unlock pointers, rowstatus values are the
     * same, the ObjectId is alone filled and notification is sent
     * to MSR */
    SnmpNotifyInfo.pu4ObjectId = SnmpNotifyType;
    SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pSnmpNotifyName,
                          pSnmpNotifyEntry->i4NotifyType));
    SNMP_TRC1 (SNMP_DEBUG_TRC, "nmhSetSnmpNotifyStorageType: "
               "Successfully setting the storage type for the notify [%s]\r\n",
               pSnmpNotifyName->pu1_OctetList);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpNotifyRowStatus
 Input       :  The Indices
                SnmpNotifyName

                The Object 
                setValSnmpNotifyRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpNotifyRowStatus (tSNMP_OCTET_STRING_TYPE * pSnmpNotifyName,
                           INT4 i4SetValSnmpNotifyRowStatus)
{
    tSnmpNotifyEntry   *pSnmpNotifyEntry = NULL;
    pSnmpNotifyEntry = SNMPGetNotifyEntry (pSnmpNotifyName);
    if (i4SetValSnmpNotifyRowStatus == SNMP_ROWSTATUS_CREATEANDGO ||
        i4SetValSnmpNotifyRowStatus == SNMP_ROWSTATUS_CREATEANDWAIT)
    {

        if (pSnmpNotifyEntry != NULL)
        {
            return SNMP_FAILURE;
        }

        if ((pSnmpNotifyEntry = SNMPCreateNotifyEntry (pSnmpNotifyName))
            == NULL)
        {
            return SNMP_FAILURE;
        }
        if (i4SetValSnmpNotifyRowStatus == SNMP_ROWSTATUS_CREATEANDGO)
        {
            pSnmpNotifyEntry->i4Status = SNMP_ACTIVE;
        }
    }
    else if (i4SetValSnmpNotifyRowStatus == SNMP_ROWSTATUS_DESTROY)
    {
        if (pSnmpNotifyEntry != NULL)
        {
            SNMPDeleteNotifyEntry (pSnmpNotifyName);
        }
    }
    else if (i4SetValSnmpNotifyRowStatus == SNMP_ROWSTATUS_NOTINSERVICE)
    {
        if (pSnmpNotifyEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        pSnmpNotifyEntry->i4Status = UNDER_CREATION;
    }
    else                        /* SNMP_ROWSTATUS_ACTIVE */
    {
        if (pSnmpNotifyEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        pSnmpNotifyEntry->i4Status = SNMP_ACTIVE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2SnmpNotifyTag
 Input       :  The Indices
                SnmpNotifyName

                The Object 
                testValSnmpNotifyTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpNotifyTag (UINT4 *pu4ErrorCode,
                        tSNMP_OCTET_STRING_TYPE * pSnmpNotifyName,
                        tSNMP_OCTET_STRING_TYPE * pTestValSnmpNotifyTag)
{
    tSnmpNotifyEntry   *pSnmpNotifyEntry = NULL;
    pSnmpNotifyEntry = SNMPGetNotifyEntry (pSnmpNotifyName);
    if (pSnmpNotifyEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if ((pTestValSnmpNotifyTag->i4_Length == SNMP_ZERO) ||
        (pTestValSnmpNotifyTag->i4_Length >= SNMP_MAX_OCTETSTRING_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpNotifyType
 Input       :  The Indices
                SnmpNotifyName

                The Object 
                testValSnmpNotifyType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpNotifyType (UINT4 *pu4ErrorCode,
                         tSNMP_OCTET_STRING_TYPE * pSnmpNotifyName,
                         INT4 i4TestValSnmpNotifyType)
{
    tSnmpNotifyEntry   *pSnmpNotifyEntry = NULL;
    pSnmpNotifyEntry = SNMPGetNotifyEntry (pSnmpNotifyName);
    if (pSnmpNotifyEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    switch (i4TestValSnmpNotifyType)
    {
        case SNMP_TRAP_TYPE:
        case SNMP_INFORM_TYPE:
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpNotifyStorageType
 Input       :  The Indices
                SnmpNotifyName

                The Object 
                testValSnmpNotifyStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpNotifyStorageType (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE * pSnmpNotifyName,
                                INT4 i4TestValSnmpNotifyStorageType)
{
    tSnmpNotifyEntry   *pSnmpNotifyEntry = NULL;
    pSnmpNotifyEntry = SNMPGetNotifyEntry (pSnmpNotifyName);
    if (pSnmpNotifyEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if ((i4TestValSnmpNotifyStorageType < SNMP3_STORAGE_TYPE_OTHER) ||
        (i4TestValSnmpNotifyStorageType > SNMP3_STORAGE_TYPE_READONLY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValSnmpNotifyStorageType != SNMP3_STORAGE_TYPE_VOLATILE) &&
        (i4TestValSnmpNotifyStorageType != SNMP3_STORAGE_TYPE_NONVOLATILE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpNotifyRowStatus
 Input       :  The Indices
                SnmpNotifyName

                The Object 
                testValSnmpNotifyRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpNotifyRowStatus (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE * pSnmpNotifyName,
                              INT4 i4TestValSnmpNotifyRowStatus)
{
    tSnmpNotifyEntry   *pSnmpNotifyEntry = NULL;
    pSnmpNotifyEntry = SNMPGetNotifyEntry (pSnmpNotifyName);

    if (i4TestValSnmpNotifyRowStatus == SNMP_ROWSTATUS_CREATEANDWAIT ||
        i4TestValSnmpNotifyRowStatus == SNMP_ROWSTATUS_CREATEANDGO)
    {
        if (pSnmpNotifyEntry != NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

    }
    else if (i4TestValSnmpNotifyRowStatus == SNMP_ROWSTATUS_ACTIVE ||
             i4TestValSnmpNotifyRowStatus == SNMP_ROWSTATUS_NOTINSERVICE)
    {
        if (pSnmpNotifyEntry == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else if (i4TestValSnmpNotifyRowStatus == SNMP_ROWSTATUS_DESTROY)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2SnmpNotifyTable
 Input       :  The Indices
                SnmpNotifyName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SnmpNotifyTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : SnmpNotifyFilterProfileTable. */

/****************************************************************************
 Function    :  nmhDepv2SnmpNotifyFilterProfileTable
 Input       :  The Indices
                SnmpTargetParamsName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SnmpNotifyFilterProfileTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceSnmpNotifyFilterProfileTable
 Input       :  The Indices
                SnmpTargetParamsName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceSnmpNotifyFilterProfileTable (tSNMP_OCTET_STRING_TYPE *
                                                      pSnmpTargetParamsName)
{
    if ((pSnmpTargetParamsName->i4_Length <= SNMP_ZERO) ||
        (pSnmpTargetParamsName->i4_Length > SNMP_MAX_OCTETSTRING_SIZE))

    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexSnmpNotifyFilterProfileTable
 Input       :  The Indices
                SnmpTargetParamsName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexSnmpNotifyFilterProfileTable (tSNMP_OCTET_STRING_TYPE *
                                              pSnmpTargetParamsName)
{
    tSnmpNotifyFilterProfileTable *pSnmpNotifyFilterProfileEntry = NULL;
    pSnmpNotifyFilterProfileEntry = SNMPGetFirstNotifyFilterProfileEntry ();
    if (pSnmpNotifyFilterProfileEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pSnmpTargetParamsName,
                         &(pSnmpNotifyFilterProfileEntry->TargetParmasName));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexSnmpNotifyFilterProfileTable
 Input       :  The Indices
                SnmpTargetParamsName
                nextSnmpTargetParamsName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexSnmpNotifyFilterProfileTable (tSNMP_OCTET_STRING_TYPE *
                                             pSnmpTargetParamsName,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pNextSnmpTargetParamsName)
{
    tSnmpNotifyFilterProfileTable *pSnmpNotifyFilterProfileEntry = NULL;
    pSnmpNotifyFilterProfileEntry =
        SNMPGetNextNotifyFilterProfileEntry (pSnmpTargetParamsName);
    if (pSnmpNotifyFilterProfileEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pNextSnmpTargetParamsName,
                         &(pSnmpNotifyFilterProfileEntry->TargetParmasName));
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetSnmpNotifyFilterProfileName
 Input       :  The Indices
                SnmpTargetParamsName

                The Object 
                retValSnmpNotifyFilterProfileName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpNotifyFilterProfileName (tSNMP_OCTET_STRING_TYPE *
                                   pSnmpTargetParamsName,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValSnmpNotifyFilterProfileName)
{
    tSnmpNotifyFilterProfileTable *pSnmpNotifyFilterProfileEntry = NULL;
    pSnmpNotifyFilterProfileEntry =
        SNMPGetNotifyFilterProfileEntry (pSnmpTargetParamsName);
    if (pSnmpNotifyFilterProfileEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pRetValSnmpNotifyFilterProfileName,
                         &(pSnmpNotifyFilterProfileEntry->ProfileName));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpNotifyFilterProfileStorType
 Input       :  The Indices
                SnmpTargetParamsName

                The Object 
                retValSnmpNotifyFilterProfileStorType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpNotifyFilterProfileStorType (tSNMP_OCTET_STRING_TYPE *
                                       pSnmpTargetParamsName,
                                       INT4
                                       *pi4RetValSnmpNotifyFilterProfileStorType)
{
    tSnmpNotifyFilterProfileTable *pSnmpNotifyFilterProfileEntry = NULL;
    pSnmpNotifyFilterProfileEntry =
        SNMPGetNotifyFilterProfileEntry (pSnmpTargetParamsName);
    if (pSnmpNotifyFilterProfileEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValSnmpNotifyFilterProfileStorType =
        pSnmpNotifyFilterProfileEntry->i4Storage;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpNotifyFilterProfileRowStatus
 Input       :  The Indices
                SnmpTargetParamsName

                The Object 
                retValSnmpNotifyFilterProfileRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpNotifyFilterProfileRowStatus (tSNMP_OCTET_STRING_TYPE *
                                        pSnmpTargetParamsName,
                                        INT4
                                        *pi4RetValSnmpNotifyFilterProfileRowStatus)
{
    tSnmpNotifyFilterProfileTable *pSnmpNotifyFilterProfileEntry = NULL;
    pSnmpNotifyFilterProfileEntry =
        SNMPGetNotifyFilterProfileEntry (pSnmpTargetParamsName);
    if (pSnmpNotifyFilterProfileEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValSnmpNotifyFilterProfileRowStatus =
        pSnmpNotifyFilterProfileEntry->i4Status;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetSnmpNotifyFilterProfileName
 Input       :  The Indices
                SnmpTargetParamsName

                The Object 
                setValSnmpNotifyFilterProfileName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpNotifyFilterProfileName (tSNMP_OCTET_STRING_TYPE *
                                   pSnmpTargetParamsName,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pSetValSnmpNotifyFilterProfileName)
{
    tSnmpNotifyFilterProfileTable *pSnmpNotifyFilterProfileEntry = NULL;

    pSnmpNotifyFilterProfileEntry =
        SNMPGetNotifyFilterProfileEntry (pSnmpTargetParamsName);

    if (pSnmpNotifyFilterProfileEntry == NULL)
    {
        SNMP_TRC (SNMP_FAILURE_TRC, "nmhSetSnmpNotifyFilterProfileName: "
                  "NULL profile entry\r\n");
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (&(pSnmpNotifyFilterProfileEntry->ProfileName),
                         pSetValSnmpNotifyFilterProfileName);
    SNMP_TRC1 (SNMP_DEBUG_TRC, "nmhSetSnmpNotifyFilterProfileName: "
               "Successfully setting profile name for target params [%s]\n",
               pSnmpTargetParamsName->pu1_OctetList);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpNotifyFilterProfileStorType
 Input       :  The Indices
                SnmpTargetParamsName

                The Object 
                setValSnmpNotifyFilterProfileStorType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpNotifyFilterProfileStorType (tSNMP_OCTET_STRING_TYPE *
                                       pSnmpTargetParamsName,
                                       INT4
                                       i4SetValSnmpNotifyFilterProfileStorType)
{
    tSnmpNotifyFilterProfileTable *pSnmpNotifyFilterProfileEntry = NULL;
    pSnmpNotifyFilterProfileEntry =
        SNMPGetNotifyFilterProfileEntry (pSnmpTargetParamsName);

    if (pSnmpNotifyFilterProfileEntry == NULL)
    {
        SNMP_TRC (SNMP_FAILURE_TRC, "nmhSetSnmpNotifyFilterProfileStorType: "
                  "NULL profile entry\r\n");
        return SNMP_FAILURE;
    }

    pSnmpNotifyFilterProfileEntry->i4Storage =
        i4SetValSnmpNotifyFilterProfileStorType;
    SNMP_TRC1 (SNMP_DEBUG_TRC, "nmhSetSnmpNotifyFilterProfileStorType: "
               "Successfully setting the storage type for target params [%s]\n",
               pSnmpTargetParamsName->pu1_OctetList);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpNotifyFilterProfileRowStatus
 Input       :  The Indices
                SnmpTargetParamsName

                The Object 
                setValSnmpNotifyFilterProfileRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpNotifyFilterProfileRowStatus (tSNMP_OCTET_STRING_TYPE *
                                        pSnmpTargetParamsName,
                                        INT4
                                        i4SetValSnmpNotifyFilterProfileRowStatus)
{
    tSnmpNotifyFilterProfileTable *pSnmpNotifyFilterProfileEntry = NULL;
    pSnmpNotifyFilterProfileEntry = SNMPGetNotifyFilterProfileEntry
        (pSnmpTargetParamsName);
    switch (i4SetValSnmpNotifyFilterProfileRowStatus)
    {
        case SNMP_ROWSTATUS_CREATEANDWAIT:
            if (pSnmpNotifyFilterProfileEntry != NULL)
            {
                return SNMP_FAILURE;
            }
            pSnmpNotifyFilterProfileEntry =
                SNMPCreateNotifyFilterProfileEntry (pSnmpTargetParamsName);
            if (pSnmpNotifyFilterProfileEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            break;
        case SNMP_ROWSTATUS_ACTIVE:
            if (pSnmpNotifyFilterProfileEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            if (pSnmpNotifyFilterProfileEntry->i4Status != UNDER_CREATION &&
                pSnmpNotifyFilterProfileEntry->ProfileName.i4_Length != 0)
            {
                return SNMP_FAILURE;
            }
            pSnmpNotifyFilterProfileEntry->i4Status = SNMP_ACTIVE;
            break;
        case SNMP_ROWSTATUS_NOTINSERVICE:
            if (pSnmpNotifyFilterProfileEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            pSnmpNotifyFilterProfileEntry->i4Status = UNDER_CREATION;
            break;
        case SNMP_ROWSTATUS_DESTROY:
            if (pSnmpNotifyFilterProfileEntry != NULL)
            {
                if (SNMPDeleteNotifyFilterProfileEntry (pSnmpTargetParamsName)
                    == SNMP_FAILURE)
                {
                    return SNMP_FAILURE;
                }
            }
            break;
        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2SnmpNotifyFilterProfileName
 Input       :  The Indices
                SnmpTargetParamsName

                The Object 
                testValSnmpNotifyFilterProfileName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpNotifyFilterProfileName (UINT4 *pu4ErrorCode,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pSnmpTargetParamsName,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pTestValSnmpNotifyFilterProfileName)
{
    tSnmpNotifyFilterProfileTable *pSnmpNotifyFilterProfileEntry = NULL;

    pSnmpNotifyFilterProfileEntry =
        SNMPGetNotifyFilterProfileEntry (pSnmpTargetParamsName);

    if (pSnmpNotifyFilterProfileEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if ((pTestValSnmpNotifyFilterProfileName->i4_Length <= SNMP_ZERO) ||
        (pTestValSnmpNotifyFilterProfileName->i4_Length >
         SNMP_MAX_OCTETSTRING_SIZE))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    if (SnmpCheckString (pTestValSnmpNotifyFilterProfileName->pu1_OctetList)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpNotifyFilterProfileStorType
 Input       :  The Indices
                SnmpTargetParamsName

                The Object 
                testValSnmpNotifyFilterProfileStorType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpNotifyFilterProfileStorType (UINT4 *pu4ErrorCode,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pSnmpTargetParamsName,
                                          INT4
                                          i4TestValSnmpNotifyFilterProfileStorType)
{
    tSnmpNotifyFilterProfileTable *pSnmpNotifyFilterProfileEntry = NULL;

    pSnmpNotifyFilterProfileEntry =
        SNMPGetNotifyFilterProfileEntry (pSnmpTargetParamsName);

    if (pSnmpNotifyFilterProfileEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if ((i4TestValSnmpNotifyFilterProfileStorType < SNMP3_STORAGE_TYPE_OTHER) ||
        (i4TestValSnmpNotifyFilterProfileStorType >
         SNMP3_STORAGE_TYPE_READONLY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValSnmpNotifyFilterProfileStorType !=
         SNMP3_STORAGE_TYPE_VOLATILE) &&
        (i4TestValSnmpNotifyFilterProfileStorType !=
         SNMP3_STORAGE_TYPE_NONVOLATILE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpNotifyFilterProfileRowStatus
 Input       :  The Indices
                SnmpTargetParamsName

                The Object 
                testValSnmpNotifyFilterProfileRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpNotifyFilterProfileRowStatus (UINT4 *pu4ErrorCode,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pSnmpTargetParamsName,
                                           INT4
                                           i4TestValSnmpNotifyFilterProfileRowStatus)
{
    tSnmpNotifyFilterProfileTable *pSnmpNotifyFilterProfileEntry = NULL;
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;

    pSnmpNotifyFilterProfileEntry =
        SNMPGetNotifyFilterProfileEntry (pSnmpTargetParamsName);

/* Low Level Dependency Routines for All Objects  */

    switch (i4TestValSnmpNotifyFilterProfileRowStatus)
    {
        case SNMP_ROWSTATUS_CREATEANDWAIT:
            if (pSnmpNotifyFilterProfileEntry != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            pSnmpTgtParamEntry = SNMPGetTgtParamEntry (pSnmpTargetParamsName);
            if (pSnmpTgtParamEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
        case SNMP_ROWSTATUS_ACTIVE:
            if (pSnmpNotifyFilterProfileEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            if (pSnmpNotifyFilterProfileEntry->i4Status != UNDER_CREATION ||
                pSnmpNotifyFilterProfileEntry->ProfileName.i4_Length
                == SNMP_ZERO)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
        case SNMP_ROWSTATUS_NOTINSERVICE:
            if (pSnmpNotifyFilterProfileEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            break;
        case SNMP_ROWSTATUS_DESTROY:
            if (pSnmpNotifyFilterProfileEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }

            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : SnmpNotifyFilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceSnmpNotifyFilterTable
 Input       :  The Indices
                SnmpNotifyFilterProfileName
                SnmpNotifyFilterSubtree
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceSnmpNotifyFilterTable (tSNMP_OCTET_STRING_TYPE *
                                               pSnmpNotifyFilterProfileName,
                                               tSNMP_OID_TYPE *
                                               pSnmpNotifyFilterSubtree)
{
    if (SNMPGetNotifyFilterEntry
        (pSnmpNotifyFilterProfileName, pSnmpNotifyFilterSubtree) == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexSnmpNotifyFilterTable
 Input       :  The Indices
                SnmpNotifyFilterProfileName
                SnmpNotifyFilterSubtree
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexSnmpNotifyFilterTable (tSNMP_OCTET_STRING_TYPE *
                                       pSnmpNotifyFilterProfileName,
                                       tSNMP_OID_TYPE *
                                       pSnmpNotifyFilterSubtree)
{
    tSnmpNotifyFilterTable *pFilterNotifyEntry = NULL;
    pFilterNotifyEntry = SNMPGetFirstNotifyFilterEntry ();
    if (pFilterNotifyEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pSnmpNotifyFilterProfileName,
                         pFilterNotifyEntry->pFilterProfileName);
    SNMPOIDCopy (pSnmpNotifyFilterSubtree,
                 &(pFilterNotifyEntry->FilterSubTree));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexSnmpNotifyFilterTable
 Input       :  The Indices
                SnmpNotifyFilterProfileName
                nextSnmpNotifyFilterProfileName
                SnmpNotifyFilterSubtree
                nextSnmpNotifyFilterSubtree
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexSnmpNotifyFilterTable (tSNMP_OCTET_STRING_TYPE *
                                      pSnmpNotifyFilterProfileName,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pNextSnmpNotifyFilterProfileName,
                                      tSNMP_OID_TYPE * pSnmpNotifyFilterSubtree,
                                      tSNMP_OID_TYPE *
                                      pNextSnmpNotifyFilterSubtree)
{
    tSnmpNotifyFilterTable *pFilterNotifyEntry = NULL;

    if (pSnmpNotifyFilterProfileName->i4_Length < SNMP_ZERO)
    {
        return SNMP_FAILURE;
    }

    pFilterNotifyEntry =
        SNMPGetNextNotifyFilterEntry (pSnmpNotifyFilterProfileName,
                                      pSnmpNotifyFilterSubtree);
    if (pFilterNotifyEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pNextSnmpNotifyFilterProfileName,
                         pFilterNotifyEntry->pFilterProfileName);
    SNMPOIDCopy (pNextSnmpNotifyFilterSubtree,
                 &(pFilterNotifyEntry->FilterSubTree));
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetSnmpNotifyFilterMask
 Input       :  The Indices
                SnmpNotifyFilterProfileName
                SnmpNotifyFilterSubtree

                The Object 
                retValSnmpNotifyFilterMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpNotifyFilterMask (tSNMP_OCTET_STRING_TYPE *
                            pSnmpNotifyFilterProfileName,
                            tSNMP_OID_TYPE * pSnmpNotifyFilterSubtree,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValSnmpNotifyFilterMask)
{
    tSnmpNotifyFilterTable *pFilterNotifyEntry = NULL;
    pFilterNotifyEntry = SNMPGetNotifyFilterEntry (pSnmpNotifyFilterProfileName,
                                                   pSnmpNotifyFilterSubtree);
    if (pFilterNotifyEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pRetValSnmpNotifyFilterMask,
                         &(pFilterNotifyEntry->FilterMask));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpNotifyFilterType
 Input       :  The Indices
                SnmpNotifyFilterProfileName
                SnmpNotifyFilterSubtree

                The Object 
                retValSnmpNotifyFilterType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpNotifyFilterType (tSNMP_OCTET_STRING_TYPE *
                            pSnmpNotifyFilterProfileName,
                            tSNMP_OID_TYPE * pSnmpNotifyFilterSubtree,
                            INT4 *pi4RetValSnmpNotifyFilterType)
{
    tSnmpNotifyFilterTable *pFilterNotifyEntry = NULL;
    pFilterNotifyEntry = SNMPGetNotifyFilterEntry (pSnmpNotifyFilterProfileName,
                                                   pSnmpNotifyFilterSubtree);
    if (pFilterNotifyEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValSnmpNotifyFilterType = pFilterNotifyEntry->i4FilterType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpNotifyFilterStorageType
 Input       :  The Indices
                SnmpNotifyFilterProfileName
                SnmpNotifyFilterSubtree

                The Object 
                retValSnmpNotifyFilterStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpNotifyFilterStorageType (tSNMP_OCTET_STRING_TYPE *
                                   pSnmpNotifyFilterProfileName,
                                   tSNMP_OID_TYPE * pSnmpNotifyFilterSubtree,
                                   INT4 *pi4RetValSnmpNotifyFilterStorageType)
{
    tSnmpNotifyFilterTable *pFilterNotifyEntry = NULL;
    pFilterNotifyEntry = SNMPGetNotifyFilterEntry (pSnmpNotifyFilterProfileName,
                                                   pSnmpNotifyFilterSubtree);
    if (pFilterNotifyEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValSnmpNotifyFilterStorageType = pFilterNotifyEntry->i4Storage;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpNotifyFilterRowStatus
 Input       :  The Indices
                SnmpNotifyFilterProfileName
                SnmpNotifyFilterSubtree

                The Object 
                retValSnmpNotifyFilterRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpNotifyFilterRowStatus (tSNMP_OCTET_STRING_TYPE *
                                 pSnmpNotifyFilterProfileName,
                                 tSNMP_OID_TYPE * pSnmpNotifyFilterSubtree,
                                 INT4 *pi4RetValSnmpNotifyFilterRowStatus)
{
    tSnmpNotifyFilterTable *pFilterNotifyEntry = NULL;
    pFilterNotifyEntry = SNMPGetNotifyFilterEntry (pSnmpNotifyFilterProfileName,
                                                   pSnmpNotifyFilterSubtree);
    if (pFilterNotifyEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pFilterNotifyEntry->i4RowStatus == UNDER_CREATION)
    {
        *pi4RetValSnmpNotifyFilterRowStatus = SNMP_ROWSTATUS_NOTINSERVICE;
    }
    if (pFilterNotifyEntry->i4RowStatus == SNMP_ACTIVE)
    {
        *pi4RetValSnmpNotifyFilterRowStatus = SNMP_ROWSTATUS_ACTIVE;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetSnmpNotifyFilterMask
 Input       :  The Indices
                SnmpNotifyFilterProfileName
                SnmpNotifyFilterSubtree

                The Object 
                setValSnmpNotifyFilterMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpNotifyFilterMask (tSNMP_OCTET_STRING_TYPE *
                            pSnmpNotifyFilterProfileName,
                            tSNMP_OID_TYPE * pSnmpNotifyFilterSubtree,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValSnmpNotifyFilterMask)
{
    tSnmpNotifyFilterTable *pFilterNotifyEntry = NULL;

    pFilterNotifyEntry = SNMPGetNotifyFilterEntry (pSnmpNotifyFilterProfileName,
                                                   pSnmpNotifyFilterSubtree);
    if (pFilterNotifyEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pFilterNotifyEntry->FilterMask.pu1_OctetList, 1,
            SNMP_FILTER_MASK_MAX_STRING_LENGTH);
    SNMPCopyOctetString (&(pFilterNotifyEntry->FilterMask),
                         pSetValSnmpNotifyFilterMask);
    pFilterNotifyEntry->FilterMask.i4_Length =
        (INT4) pFilterNotifyEntry->FilterSubTree.u4_Length;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpNotifyFilterType
 Input       :  The Indices
                SnmpNotifyFilterProfileName
                SnmpNotifyFilterSubtree

                The Object 
                setValSnmpNotifyFilterType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpNotifyFilterType (tSNMP_OCTET_STRING_TYPE *
                            pSnmpNotifyFilterProfileName,
                            tSNMP_OID_TYPE * pSnmpNotifyFilterSubtree,
                            INT4 i4SetValSnmpNotifyFilterType)
{
    tSnmpNotifyFilterTable *pFilterNotifyEntry = NULL;
    pFilterNotifyEntry = SNMPGetNotifyFilterEntry (pSnmpNotifyFilterProfileName,
                                                   pSnmpNotifyFilterSubtree);
    if (pFilterNotifyEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pFilterNotifyEntry->i4FilterType = i4SetValSnmpNotifyFilterType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpNotifyFilterStorageType
 Input       :  The Indices
                SnmpNotifyFilterProfileName
                SnmpNotifyFilterSubtree

                The Object 
                setValSnmpNotifyFilterStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpNotifyFilterStorageType (tSNMP_OCTET_STRING_TYPE *
                                   pSnmpNotifyFilterProfileName,
                                   tSNMP_OID_TYPE * pSnmpNotifyFilterSubtree,
                                   INT4 i4SetValSnmpNotifyFilterStorageType)
{
    tSnmpNotifyFilterTable *pFilterNotifyEntry = NULL;
    pFilterNotifyEntry = SNMPGetNotifyFilterEntry (pSnmpNotifyFilterProfileName,
                                                   pSnmpNotifyFilterSubtree);
    if (pFilterNotifyEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pFilterNotifyEntry->i4Storage = i4SetValSnmpNotifyFilterStorageType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpNotifyFilterRowStatus
 Input       :  The Indices
                SnmpNotifyFilterProfileName
                SnmpNotifyFilterSubtree

                The Object 
                setValSnmpNotifyFilterRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpNotifyFilterRowStatus (tSNMP_OCTET_STRING_TYPE *
                                 pSnmpNotifyFilterProfileName,
                                 tSNMP_OID_TYPE * pSnmpNotifyFilterSubtree,
                                 INT4 i4SetValSnmpNotifyFilterRowStatus)
{
    UINT4               u4Return;
    tSnmpNotifyFilterTable *pFilterNotifyEntry = NULL;
    switch (i4SetValSnmpNotifyFilterRowStatus)
    {

        case SNMP_ROWSTATUS_CREATEANDGO:
        case SNMP_ROWSTATUS_CREATEANDWAIT:
        {
            u4Return = (UINT4) SNMPCreateNotifyFilterEntry
                (pSnmpNotifyFilterProfileName, pSnmpNotifyFilterSubtree);
            if (u4Return != SNMP_FAILURE)
            {
                return SNMP_SUCCESS;
            }
            else
            {
                return SNMP_FAILURE;
            }
        }

        case SNMP_ROWSTATUS_ACTIVE:
        {
            pFilterNotifyEntry = SNMPGetNotifyFilterEntry
                (pSnmpNotifyFilterProfileName, pSnmpNotifyFilterSubtree);
            if (pFilterNotifyEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            pFilterNotifyEntry->i4RowStatus = SNMP_ROWSTATUS_ACTIVE;
            return SNMP_SUCCESS;
        }

        case SNMP_ROWSTATUS_DESTROY:
        {
            u4Return = (UINT4) SNMPDeleteNotifyFilterEntry
                (pSnmpNotifyFilterProfileName, pSnmpNotifyFilterSubtree);
            if (u4Return == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }
            else
            {
                return SNMP_SUCCESS;
            }
        }

        case SNMP_ROWSTATUS_NOTINSERVICE:
        {
            pFilterNotifyEntry = SNMPGetNotifyFilterEntry
                (pSnmpNotifyFilterProfileName, pSnmpNotifyFilterSubtree);
            if (pFilterNotifyEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            if (pFilterNotifyEntry->i4RowStatus == SNMP_ROWSTATUS_ACTIVE)
            {
                pFilterNotifyEntry->i4RowStatus = UNDER_CREATION;
                return SNMP_SUCCESS;
            }
        }
        default:
            break;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2SnmpNotifyFilterMask
 Input       :  The Indices
                SnmpNotifyFilterProfileName
                SnmpNotifyFilterSubtree

                The Object 
                testValSnmpNotifyFilterMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpNotifyFilterMask (UINT4 *pu4ErrorCode,
                               tSNMP_OCTET_STRING_TYPE *
                               pSnmpNotifyFilterProfileName,
                               tSNMP_OID_TYPE * pSnmpNotifyFilterSubtree,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValSnmpNotifyFilterMask)
{
    tSnmpNotifyFilterTable *pFilterNotifyEntry = NULL;

    if ((pFilterNotifyEntry = SNMPGetNotifyFilterEntry
         (pSnmpNotifyFilterProfileName, pSnmpNotifyFilterSubtree)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    else if (pTestValSnmpNotifyFilterMask->i4_Length >
             SNMP_FILTER_MASK_MAX_STRING_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    else if ((UINT4) pTestValSnmpNotifyFilterMask->i4_Length >
             pFilterNotifyEntry->FilterSubTree.u4_Length)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpNotifyFilterType
 Input       :  The Indices
                SnmpNotifyFilterProfileName
                SnmpNotifyFilterSubtree

                The Object 
                testValSnmpNotifyFilterType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpNotifyFilterType (UINT4 *pu4ErrorCode,
                               tSNMP_OCTET_STRING_TYPE *
                               pSnmpNotifyFilterProfileName,
                               tSNMP_OID_TYPE * pSnmpNotifyFilterSubtree,
                               INT4 i4TestValSnmpNotifyFilterType)
{
    if (SNMPGetNotifyFilterEntry
        (pSnmpNotifyFilterProfileName, pSnmpNotifyFilterSubtree) != NULL)
    {
        if ((i4TestValSnmpNotifyFilterType != SNMP_NOTIFY_FILTER_TYPE_INCLUDED)
            && (i4TestValSnmpNotifyFilterType !=
                SNMP_NOTIFY_FILTER_TYPE_EXCLUDED))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2SnmpNotifyFilterStorageType
 Input       :  The Indices
                SnmpNotifyFilterProfileName
                SnmpNotifyFilterSubtree

                The Object 
                testValSnmpNotifyFilterStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpNotifyFilterStorageType (UINT4 *pu4ErrorCode,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pSnmpNotifyFilterProfileName,
                                      tSNMP_OID_TYPE * pSnmpNotifyFilterSubtree,
                                      INT4 i4TestValSnmpNotifyFilterStorageType)
{
    if (SNMPGetNotifyFilterEntry
        (pSnmpNotifyFilterProfileName, pSnmpNotifyFilterSubtree) != NULL)
    {
        if ((i4TestValSnmpNotifyFilterStorageType < SNMP3_STORAGE_TYPE_OTHER)
            || (i4TestValSnmpNotifyFilterStorageType >
                SNMP3_STORAGE_TYPE_READONLY))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        if ((i4TestValSnmpNotifyFilterStorageType !=
             SNMP3_STORAGE_TYPE_VOLATILE)
            && (i4TestValSnmpNotifyFilterStorageType !=
                SNMP3_STORAGE_TYPE_NONVOLATILE))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2SnmpNotifyFilterRowStatus
 Input       :  The Indices
                SnmpNotifyFilterProfileName
                SnmpNotifyFilterSubtree

                The Object 
                testValSnmpNotifyFilterRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpNotifyFilterRowStatus (UINT4 *pu4ErrorCode,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pSnmpNotifyFilterProfileName,
                                    tSNMP_OID_TYPE * pSnmpNotifyFilterSubtree,
                                    INT4 i4TestValSnmpNotifyFilterRowStatus)
{
    tSnmpNotifyFilterTable *pFilterNotifyEntry = NULL;
    INT4                i4Return = SNMP_FAILURE;

    pFilterNotifyEntry =
        SNMPGetNotifyFilterEntry (pSnmpNotifyFilterProfileName,
                                  pSnmpNotifyFilterSubtree);

    switch (i4TestValSnmpNotifyFilterRowStatus)
    {
        case SNMP_ROWSTATUS_CREATEANDWAIT:
            if (pFilterNotifyEntry != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            i4Return = SNMPNotifyFilterCheckProfileName
                (pSnmpNotifyFilterProfileName);
            if (i4Return == SNMP_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            break;
        case SNMP_ROWSTATUS_ACTIVE:
            if (pFilterNotifyEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            if (pFilterNotifyEntry->i4RowStatus != UNDER_CREATION ||
                pFilterNotifyEntry->pFilterProfileName->i4_Length == SNMP_ZERO)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
        case SNMP_ROWSTATUS_NOTINSERVICE:
            if (pFilterNotifyEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            break;
        case SNMP_ROWSTATUS_DESTROY:
            if (pFilterNotifyEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2SnmpNotifyFilterTable
 Input       :  The Indices
                SnmpNotifyFilterProfileName
                SnmpNotifyFilterSubtree
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SnmpNotifyFilterTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
