/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: snmusmlw.c,v 1.10 2017/11/20 13:11:26 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "snmpcmn.h"
# include  "snmusmlw.h"

extern UINT1        gau1MsgUsrName[];
extern UINT1        gau1MsgFlag;

UINT4               au4StdAuthOid[SNMP_STD_AUTH_OID_LEN] =
    { 1, 3, 6, 1, 6, 3, 10, 1, 1 };
UINT4               au4StdPrivOid[SNMP_STD_PRIV_OID_LEN] =
    { 1, 3, 6, 1, 6, 3, 10, 1, 2 };
UINT4               au4StdUsmOid[SNMP_STD_USM_OID_LEN] =
    { 1, 3, 6, 1, 6, 3, 15, 1, 2, 2, 1 };

extern UINT4        UsmUserAuthKeyChange[12];
extern UINT4        UsmUserPrivKeyChange[12];

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetUsmStatsUnsupportedSecLevels
 Input       :  The Indices

                The Object 
                retValUsmStatsUnsupportedSecLevels
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUsmStatsUnsupportedSecLevels (UINT4
                                    *pu4RetValUsmStatsUnsupportedSecLevels)
{
    *pu4RetValUsmStatsUnsupportedSecLevels = SNMP_GET_USM_UNSUPPORTSECLEVELS;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetUsmStatsNotInTimeWindows
 Input       :  The Indices

                The Object 
                retValUsmStatsNotInTimeWindows
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUsmStatsNotInTimeWindows (UINT4 *pu4RetValUsmStatsNotInTimeWindows)
{
    *pu4RetValUsmStatsNotInTimeWindows = SNMP_GET_USM_NOTINTIMEWINDOWS;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetUsmStatsUnknownUserNames
 Input       :  The Indices

                The Object 
                retValUsmStatsUnknownUserNames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUsmStatsUnknownUserNames (UINT4 *pu4RetValUsmStatsUnknownUserNames)
{
    *pu4RetValUsmStatsUnknownUserNames = SNMP_GET_USM_UNKNOWNUSERNAMES;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetUsmStatsUnknownEngineIDs
 Input       :  The Indices

                The Object 
                retValUsmStatsUnknownEngineIDs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUsmStatsUnknownEngineIDs (UINT4 *pu4RetValUsmStatsUnknownEngineIDs)
{
    *pu4RetValUsmStatsUnknownEngineIDs = SNMP_GET_USM_UNKNOWNENGINEIDS;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetUsmStatsWrongDigests
 Input       :  The Indices

                The Object 
                retValUsmStatsWrongDigests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUsmStatsWrongDigests (UINT4 *pu4RetValUsmStatsWrongDigests)
{
    *pu4RetValUsmStatsWrongDigests = SNMP_GET_USM_WRONGDIGESTS;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetUsmStatsDecryptionErrors
 Input       :  The Indices

                The Object 
                retValUsmStatsDecryptionErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUsmStatsDecryptionErrors (UINT4 *pu4RetValUsmStatsDecryptionErrors)
{
    *pu4RetValUsmStatsDecryptionErrors = SNMP_GET_USM_DECRYPTIONERRORS;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetUsmUserSpinLock
 Input       :  The Indices

                The Object 
                retValUsmUserSpinLock
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUsmUserSpinLock (INT4 *pi4RetValUsmUserSpinLock)
{
    *pi4RetValUsmUserSpinLock = SNMP_GET_USM_USERSPINLOCK;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetUsmUserSpinLock
 Input       :  The Indices

                The Object 
                setValUsmUserSpinLock
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetUsmUserSpinLock (INT4 i4SetValUsmUserSpinLock)
{
    SNMP_SET_USM_USERSPINLOCK = i4SetValUsmUserSpinLock;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2UsmUserSpinLock
 Input       :  The Indices

                The Object 
                testValUsmUserSpinLock
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2UsmUserSpinLock (UINT4 *pu4ErrorCode, INT4 i4TestValUsmUserSpinLock)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValUsmUserSpinLock);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2UsmUserSpinLock
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2UsmUserSpinLock (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : UsmUserTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceUsmUserTable
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceUsmUserTable (tSNMP_OCTET_STRING_TYPE *
                                      pUsmUserEngineID,
                                      tSNMP_OCTET_STRING_TYPE * pUsmUserName)
{
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;

    pSnmpUsmEntry = SNMPGetUsmEntry (pUsmUserEngineID, pUsmUserName);
    if (pSnmpUsmEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexUsmUserTable
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexUsmUserTable (tSNMP_OCTET_STRING_TYPE * pUsmUserEngineID,
                              tSNMP_OCTET_STRING_TYPE * pUsmUserName)
{
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    pSnmpUsmEntry = SNMPGetFirstUsmEntry ();
    if (pSnmpUsmEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pUsmUserEngineID, &(pSnmpUsmEntry->UsmUserEngineID));
    SNMPCopyOctetString (pUsmUserName, &(pSnmpUsmEntry->UsmUserName));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexUsmUserTable
 Input       :  The Indices
                UsmUserEngineID
                nextUsmUserEngineID
                UsmUserName
                nextUsmUserName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexUsmUserTable (tSNMP_OCTET_STRING_TYPE * pUsmUserEngineID,
                             tSNMP_OCTET_STRING_TYPE * pNextUsmUserEngineID,
                             tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                             tSNMP_OCTET_STRING_TYPE * pNextUsmUserName)
{
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    pSnmpUsmEntry = SNMPGetNextUsmEntry (pUsmUserEngineID, pUsmUserName);
    if (pSnmpUsmEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pNextUsmUserEngineID,
                         &(pSnmpUsmEntry->UsmUserEngineID));
    SNMPCopyOctetString (pNextUsmUserName, &(pSnmpUsmEntry->UsmUserName));
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetUsmUserSecurityName
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName

                The Object 
                retValUsmUserSecurityName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUsmUserSecurityName (tSNMP_OCTET_STRING_TYPE * pUsmUserEngineID,
                           tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                           tSNMP_OCTET_STRING_TYPE * pRetValUsmUserSecurityName)
{
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    pSnmpUsmEntry = SNMPGetUsmEntry (pUsmUserEngineID, pUsmUserName);
    if (pSnmpUsmEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pRetValUsmUserSecurityName,
                         &(pSnmpUsmEntry->UsmUserSecName));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetUsmUserCloneFrom
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName

                The Object 
                retValUsmUserCloneFrom
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUsmUserCloneFrom (tSNMP_OCTET_STRING_TYPE * pUsmUserEngineID,
                        tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                        tSNMP_OID_TYPE * pRetValUsmUserCloneFrom)
{
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    pSnmpUsmEntry = SNMPGetUsmEntry (pUsmUserEngineID, pUsmUserName);
    if (pSnmpUsmEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRetValUsmUserCloneFrom->pu4_OidList[SNMP_ZERO] = SNMP_ZERO;
    pRetValUsmUserCloneFrom->pu4_OidList[SNMP_ONE] = SNMP_ZERO;
    pRetValUsmUserCloneFrom->u4_Length = SNMP_TWO;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetUsmUserAuthProtocol
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName

                The Object 
                retValUsmUserAuthProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUsmUserAuthProtocol (tSNMP_OCTET_STRING_TYPE * pUsmUserEngineID,
                           tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                           tSNMP_OID_TYPE * pRetValUsmUserAuthProtocol)
{
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    pSnmpUsmEntry = SNMPGetUsmEntry (pUsmUserEngineID, pUsmUserName);
    if (pSnmpUsmEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pRetValUsmUserAuthProtocol->pu4_OidList,
            au4StdAuthOid, SNMP_STD_AUTH_OID_LEN * sizeof (UINT4));
    pRetValUsmUserAuthProtocol->u4_Length = SNMP_STD_AUTH_OID_LEN;
    pRetValUsmUserAuthProtocol->pu4_OidList[SNMP_STD_AUTH_OID_LEN] =
        pSnmpUsmEntry->u4UsmUserAuthProtocol;
    pRetValUsmUserAuthProtocol->u4_Length++;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetUsmUserAuthKeyChange
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName

                The Object 
                retValUsmUserAuthKeyChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUsmUserAuthKeyChange (tSNMP_OCTET_STRING_TYPE * pUsmUserEngineID,
                            tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValUsmUserAuthKeyChange)
{
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    pSnmpUsmEntry = SNMPGetUsmEntry (pUsmUserEngineID, pUsmUserName);
    if (pSnmpUsmEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRetValUsmUserAuthKeyChange->i4_Length = SNMP_ZERO;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetUsmUserOwnAuthKeyChange
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName

                The Object 
                retValUsmUserOwnAuthKeyChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUsmUserOwnAuthKeyChange (tSNMP_OCTET_STRING_TYPE * pUsmUserEngineID,
                               tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValUsmUserOwnAuthKeyChange)
{
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    pSnmpUsmEntry = SNMPGetUsmEntry (pUsmUserEngineID, pUsmUserName);
    if (pSnmpUsmEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRetValUsmUserOwnAuthKeyChange->i4_Length = SNMP_ZERO;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetUsmUserPrivProtocol
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName

                The Object 
                retValUsmUserPrivProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUsmUserPrivProtocol (tSNMP_OCTET_STRING_TYPE * pUsmUserEngineID,
                           tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                           tSNMP_OID_TYPE * pRetValUsmUserPrivProtocol)
{
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    pSnmpUsmEntry = SNMPGetUsmEntry (pUsmUserEngineID, pUsmUserName);
    if (pSnmpUsmEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pRetValUsmUserPrivProtocol->pu4_OidList,
            au4StdPrivOid, SNMP_STD_PRIV_OID_LEN * sizeof (UINT4));
    pRetValUsmUserPrivProtocol->u4_Length = SNMP_STD_PRIV_OID_LEN;
    pRetValUsmUserPrivProtocol->pu4_OidList[SNMP_STD_PRIV_OID_LEN] =
        pSnmpUsmEntry->u4UsmUserPrivProtocol;
    pRetValUsmUserPrivProtocol->u4_Length++;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetUsmUserPrivKeyChange
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName

                The Object 
                retValUsmUserPrivKeyChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUsmUserPrivKeyChange (tSNMP_OCTET_STRING_TYPE * pUsmUserEngineID,
                            tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValUsmUserPrivKeyChange)
{
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    pSnmpUsmEntry = SNMPGetUsmEntry (pUsmUserEngineID, pUsmUserName);
    if (pSnmpUsmEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRetValUsmUserPrivKeyChange->i4_Length = SNMP_ZERO;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetUsmUserOwnPrivKeyChange
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName

                The Object 
                retValUsmUserOwnPrivKeyChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUsmUserOwnPrivKeyChange (tSNMP_OCTET_STRING_TYPE * pUsmUserEngineID,
                               tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValUsmUserOwnPrivKeyChange)
{
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    pSnmpUsmEntry = SNMPGetUsmEntry (pUsmUserEngineID, pUsmUserName);
    if (pSnmpUsmEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRetValUsmUserOwnPrivKeyChange->i4_Length = SNMP_ZERO;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetUsmUserPublic
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName

                The Object 
                retValUsmUserPublic
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUsmUserPublic (tSNMP_OCTET_STRING_TYPE * pUsmUserEngineID,
                     tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                     tSNMP_OCTET_STRING_TYPE * pRetValUsmUserPublic)
{
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    pSnmpUsmEntry = SNMPGetUsmEntry (pUsmUserEngineID, pUsmUserName);
    if (pSnmpUsmEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pRetValUsmUserPublic, &(pSnmpUsmEntry->UsmUserPublic));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetUsmUserStorageType
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName

                The Object 
                retValUsmUserStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUsmUserStorageType (tSNMP_OCTET_STRING_TYPE * pUsmUserEngineID,
                          tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                          INT4 *pi4RetValUsmUserStorageType)
{
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    pSnmpUsmEntry = SNMPGetUsmEntry (pUsmUserEngineID, pUsmUserName);
    if (pSnmpUsmEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValUsmUserStorageType = (INT4) pSnmpUsmEntry->u4UsmUserStorageType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetUsmUserStatus
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName

                The Object 
                retValUsmUserStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetUsmUserStatus (tSNMP_OCTET_STRING_TYPE * pUsmUserEngineID,
                     tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                     INT4 *pi4RetValUsmUserStatus)
{
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    pSnmpUsmEntry = SNMPGetUsmEntry (pUsmUserEngineID, pUsmUserName);
    if (pSnmpUsmEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pSnmpUsmEntry->u4UsmUserStatus == SNMP_ACTIVE)
    {
        *pi4RetValUsmUserStatus = SNMP_ROWSTATUS_ACTIVE;
    }
    else if (pSnmpUsmEntry->u4UsmUserStatus == UNDER_CREATION)
    {
        *pi4RetValUsmUserStatus = SNMP_ROWSTATUS_NOTINSERVICE;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetUsmUserCloneFrom
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName

                The Object 
                setValUsmUserCloneFrom
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetUsmUserCloneFrom (tSNMP_OCTET_STRING_TYPE * pUsmUserEngineID,
                        tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                        tSNMP_OID_TYPE * pSetValUsmUserCloneFrom)
{
    tSNMP_OCTET_STRING_TYPE EngineID;
    tSNMP_OCTET_STRING_TYPE UserName;
    UINT4               u4Count = SNMP_ZERO, u4Len = SNMP_ZERO, u4Offset =
        SNMP_ZERO;
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL, *pNewSnmpUsmEntry = NULL;
    UINT1               au1EngineID[SNMP_MAX_STR_ENGINEID_LEN];
    UINT1               au1UserName[SNMP_MAX_ADMIN_STR_LENGTH];

    EngineID.pu1_OctetList = au1EngineID;
    UserName.pu1_OctetList = au1UserName;
    pSnmpUsmEntry = SNMPGetUsmEntry (pUsmUserEngineID, pUsmUserName);
    if (pSnmpUsmEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pSnmpUsmEntry->u4UsmUserCloneFrom != SNMP_ZERO)
    {
        return SNMP_SUCCESS;
    }
    pSnmpUsmEntry->u4UsmUserCloneFrom++;
    /* Validations are already done in Test Routines so, ignore */
    u4Offset = SNMP_STD_USM_OID_LEN + 1;
    u4Len = pSetValUsmUserCloneFrom->pu4_OidList[u4Offset];
    u4Offset++;

    if (MEMCMP (pSetValUsmUserCloneFrom->pu4_OidList,
                au4StdUsmOid, (SNMP_STD_USM_OID_LEN * sizeof (UINT4))) != 0)
    {
        return SNMP_FAILURE;
    }
    for (u4Count = SNMP_ZERO; u4Count < u4Len; u4Count++)
    {
        EngineID.pu1_OctetList[u4Count] = (UINT1) pSetValUsmUserCloneFrom->
            pu4_OidList[u4Offset];
        u4Offset++;
    }
    EngineID.i4_Length = (INT4) u4Len;
    u4Len = pSetValUsmUserCloneFrom->pu4_OidList[u4Offset];
    u4Offset++;

    for (u4Count = SNMP_ZERO; u4Count < u4Len; u4Count++)
    {
        UserName.pu1_OctetList[u4Count] = (UINT1) pSetValUsmUserCloneFrom->
            pu4_OidList[u4Offset];
        u4Offset++;
    }
    UserName.i4_Length = (INT4) u4Len;
    pNewSnmpUsmEntry = SNMPGetUsmEntry (&EngineID, &UserName);
    if ((pNewSnmpUsmEntry == NULL) || (pNewSnmpUsmEntry == pSnmpUsmEntry)
        || (pNewSnmpUsmEntry->u4UsmUserStatus != SNMP_ACTIVE))
    {
        return SNMP_FAILURE;
    }
    /* Cloning Auth Proto, Auth Key, Priv Proto and Priv Key */
    pSnmpUsmEntry->u4UsmUserAuthProtocol =
        pNewSnmpUsmEntry->u4UsmUserAuthProtocol;
    pSnmpUsmEntry->u4UsmUserPrivProtocol =
        pNewSnmpUsmEntry->u4UsmUserPrivProtocol;
    SNMPCopyOctetString (&(pSnmpUsmEntry->UsmUserAuthKeyChange),
                         &(pNewSnmpUsmEntry->UsmUserAuthKeyChange));
    SNMPCopyOctetString (&(pSnmpUsmEntry->UsmUserPrivKeyChange),
                         &(pNewSnmpUsmEntry->UsmUserPrivKeyChange));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetUsmUserAuthProtocol
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName

                The Object 
                setValUsmUserAuthProtocol
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetUsmUserAuthProtocol (tSNMP_OCTET_STRING_TYPE * pUsmUserEngineID,
                           tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                           tSNMP_OID_TYPE * pSetValUsmUserAuthProtocol)
{
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function entry\r\n", __func__);
    pSnmpUsmEntry = SNMPGetUsmEntry (pUsmUserEngineID, pUsmUserName);
    if (pSnmpUsmEntry == NULL)
    {
        SNMP_TRC (SNMP_FAILURE_TRC,
                  "nmhSetUsmUserAuthProtocol: NULL usm Entry\n");
        return SNMP_FAILURE;
    }
    pSnmpUsmEntry->u4UsmUserAuthProtocol = pSetValUsmUserAuthProtocol->
        pu4_OidList[SNMP_STD_AUTH_OID_LEN];

    SNMP_TRC2 (SNMP_CRITICAL_TRC | SNMP_DEBUG_TRC, "nmhSetUsmUserAuthProtocol: "
               "Successfully set the Auth protocol as [%d] for the user [%s]\n",
               pSnmpUsmEntry->u4UsmUserAuthProtocol,
               pUsmUserName->pu1_OctetList);
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function exit\r\n", __func__);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetUsmUserAuthKeyChange
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName

                The Object 
                setValUsmUserAuthKeyChange
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetUsmUserAuthKeyChange (tSNMP_OCTET_STRING_TYPE * pUsmUserEngineID,
                            tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValUsmUserAuthKeyChange)
{
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function entry\r\n", __func__);
    pSnmpUsmEntry = SNMPGetUsmEntry (pUsmUserEngineID, pUsmUserName);
    if (pSnmpUsmEntry == NULL)
    {
        SNMP_TRC (SNMP_FAILURE_TRC,
                  "nmhSetUsmUserAuthKeyChange: NULL usm Entry\n");
        return SNMP_FAILURE;
    }

    if (SNMPIsMibRestoreInProgress () == SNMP_TRUE)
    {
        SNMPCopyOctetString (&(pSnmpUsmEntry->UsmUserAuthKeyChange),
                             pSetValUsmUserAuthKeyChange);
        pSnmpUsmEntry->UsmUserAuthPassword.i4_Length = SNMP_ZERO;
    }
    else
    {
        SNMPCopyOctetString (&(pSnmpUsmEntry->UsmUserAuthPassword),
                             pSetValUsmUserAuthKeyChange);

        SNMPKeyGenerator (pSetValUsmUserAuthKeyChange->pu1_OctetList,
                          pSetValUsmUserAuthKeyChange->i4_Length,
                          pSnmpUsmEntry->u4UsmUserAuthProtocol,
                          &(pSnmpUsmEntry->UsmUserEngineID),
                          &(pSnmpUsmEntry->UsmUserAuthKeyChange));
    }
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function exit\r\n", __func__);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetUsmUserOwnAuthKeyChange
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName

                The Object 
                setValUsmUserOwnAuthKeyChange
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetUsmUserOwnAuthKeyChange (tSNMP_OCTET_STRING_TYPE * pUsmUserEngineID,
                               tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValUsmUserOwnAuthKeyChange)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    INT1                i1Result = SNMP_FAILURE;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if ((MEMCMP
         (pUsmUserName->pu1_OctetList, gau1MsgUsrName,
          pUsmUserName->i4_Length)) == 0)
    {
        if ((gau1MsgFlag & SNMP_AUTH_BIT) == SNMP_AUTH_BIT)
        {
            i1Result =
                nmhSetUsmUserAuthKeyChange (pUsmUserEngineID, pUsmUserName,
                                            pSetValUsmUserOwnAuthKeyChange);
            RM_GET_SEQ_NUM (&u4SeqNum);
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                  UsmUserAuthKeyChange, u4SeqNum,
                                  FALSE, NULL, NULL, 2, SNMP_SUCCESS);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %s %s",
                              pUsmUserEngineID, pUsmUserName,
                              pSetValUsmUserOwnAuthKeyChange));
        }
    }
    return i1Result;
}

/****************************************************************************
 Function    :  nmhSetUsmUserPrivProtocol
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName

                The Object 
                setValUsmUserPrivProtocol
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetUsmUserPrivProtocol (tSNMP_OCTET_STRING_TYPE * pUsmUserEngineID,
                           tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                           tSNMP_OID_TYPE * pSetValUsmUserPrivProtocol)
{
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function entry\r\n", __func__);
    pSnmpUsmEntry = SNMPGetUsmEntry (pUsmUserEngineID, pUsmUserName);
    if (pSnmpUsmEntry == NULL)
    {
        SNMP_TRC (SNMP_FAILURE_TRC,
                  "nmhSetUsmUserPrivProtocol: NULL usm Entry\n");
        return SNMP_FAILURE;
    }
    pSnmpUsmEntry->u4UsmUserPrivProtocol = pSetValUsmUserPrivProtocol->
        pu4_OidList[SNMP_STD_PRIV_OID_LEN];
    SNMP_TRC2 (SNMP_DEBUG_TRC, "nmhSetUsmUserPrivProtocol: "
               "Successfully set the PRIV protocol as [%d] for the user [%s]\n",
               pSnmpUsmEntry->u4UsmUserPrivProtocol,
               pUsmUserName->pu1_OctetList);

    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function exit\r\n", __func__);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetUsmUserPrivKeyChange
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName

                The Object 
                setValUsmUserPrivKeyChange
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetUsmUserPrivKeyChange (tSNMP_OCTET_STRING_TYPE * pUsmUserEngineID,
                            tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValUsmUserPrivKeyChange)
{
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function entry\r\n", __func__);
    pSnmpUsmEntry = SNMPGetUsmEntry (pUsmUserEngineID, pUsmUserName);
    if (pSnmpUsmEntry == NULL)
    {
        SNMP_TRC (SNMP_FAILURE_TRC,
                  "nmhSetUsmUserPrivKeyChange: NULL usm Entry\n");
        return SNMP_FAILURE;
    }

    if (SNMPIsMibRestoreInProgress () == SNMP_TRUE)
    {
        SNMPCopyOctetString (&(pSnmpUsmEntry->UsmUserPrivKeyChange),
                             pSetValUsmUserPrivKeyChange);
        pSnmpUsmEntry->UsmUserPrivPassword.i4_Length = SNMP_ZERO;
    }
    else
    {
        SNMPCopyOctetString (&(pSnmpUsmEntry->UsmUserPrivPassword),
                             pSetValUsmUserPrivKeyChange);
        SNMPKeyGenerator (pSetValUsmUserPrivKeyChange->pu1_OctetList,
                          pSetValUsmUserPrivKeyChange->i4_Length,
                          pSnmpUsmEntry->u4UsmUserAuthProtocol,
                          &(pSnmpUsmEntry->UsmUserEngineID),
                          &(pSnmpUsmEntry->UsmUserPrivKeyChange));
    }
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function exit\r\n", __func__);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetUsmUserOwnPrivKeyChange
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName

                The Object 
                setValUsmUserOwnPrivKeyChange
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetUsmUserOwnPrivKeyChange (tSNMP_OCTET_STRING_TYPE * pUsmUserEngineID,
                               tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValUsmUserOwnPrivKeyChange)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    INT1                i1Result = SNMP_FAILURE;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if ((MEMCMP
         (pUsmUserName->pu1_OctetList, gau1MsgUsrName,
          pUsmUserName->i4_Length)) == 0)
    {
        if ((gau1MsgFlag & SNMP_AUTH_BIT) == SNMP_AUTH_BIT)
        {
            i1Result =
                nmhSetUsmUserPrivKeyChange (pUsmUserEngineID, pUsmUserName,
                                            pSetValUsmUserOwnPrivKeyChange);
            RM_GET_SEQ_NUM (&u4SeqNum);
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                  UsmUserPrivKeyChange, u4SeqNum,
                                  FALSE, NULL, NULL, 2, SNMP_SUCCESS);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %s %s",
                              pUsmUserEngineID, pUsmUserName,
                              pSetValUsmUserOwnPrivKeyChange));
        }
    }
    return i1Result;
}

/****************************************************************************
 Function    :  nmhSetUsmUserPublic
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName

                The Object 
                setValUsmUserPublic
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetUsmUserPublic (tSNMP_OCTET_STRING_TYPE * pUsmUserEngineID,
                     tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                     tSNMP_OCTET_STRING_TYPE * pSetValUsmUserPublic)
{
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function entry\r\n", __func__);
    pSnmpUsmEntry = SNMPGetUsmEntry (pUsmUserEngineID, pUsmUserName);
    if (pSnmpUsmEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (&(pSnmpUsmEntry->UsmUserPublic), pSetValUsmUserPublic);
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function exit\r\n", __func__);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetUsmUserStorageType
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName

                The Object 
                setValUsmUserStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetUsmUserStorageType (tSNMP_OCTET_STRING_TYPE * pUsmUserEngineID,
                          tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                          INT4 i4SetValUsmUserStorageType)
{
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function entry\r\n", __func__);
    pSnmpUsmEntry = SNMPGetUsmEntry (pUsmUserEngineID, pUsmUserName);
    if (pSnmpUsmEntry == NULL)
    {
        SNMP_TRC (SNMP_FAILURE_TRC, "nmhSetUsmUserStorageType: "
                  "NULL usm Entry\n");
        return SNMP_FAILURE;
    }
    pSnmpUsmEntry->u4UsmUserStorageType = (UINT4) i4SetValUsmUserStorageType;
    SNMP_TRC2 (SNMP_DEBUG_TRC, "nmhSetUsmUserStorageType: "
               "Successfully set th storage type as [%d] for the user [%s]\n",
               i4SetValUsmUserStorageType, pUsmUserName->pu1_OctetList);
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function exit\r\n", __func__);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetUsmUserStatus
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName

                The Object 
                setValUsmUserStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetUsmUserStatus (tSNMP_OCTET_STRING_TYPE * pUsmUserEngineID,
                     tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                     INT4 i4SetValUsmUserStatus)
{
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    pSnmpUsmEntry = SNMPGetUsmEntry (pUsmUserEngineID, pUsmUserName);
    if (i4SetValUsmUserStatus == SNMP_ROWSTATUS_CREATEANDGO ||
        i4SetValUsmUserStatus == SNMP_ROWSTATUS_CREATEANDWAIT)
    {
        if (pSnmpUsmEntry != NULL)
        {
            /* Already Created in the Test Routine */
            return SNMP_FAILURE;
        }

        if ((pSnmpUsmEntry =
             SNMPCreateUsmEntry (pUsmUserEngineID, pUsmUserName)) == NULL)
        {
            return SNMP_FAILURE;
        }

        if (i4SetValUsmUserStatus == SNMP_ROWSTATUS_CREATEANDGO)
        {
            pSnmpUsmEntry->u4UsmUserStatus = SNMP_ACTIVE;
        }
    }
    else if (i4SetValUsmUserStatus == SNMP_ROWSTATUS_DESTROY)
    {
        SNMP_TRC1 (SNMP_INFO_TRC, "%s: function entry\r\n", __func__);
        if (pSnmpUsmEntry != NULL)
        {
            SNMPDeleteUsmEntry (pUsmUserEngineID, pUsmUserName);
        }
        else
        {
            SNMP_TRC (SNMP_FAILURE_TRC, "nmhSetUsmUserStatus: "
                      " User Entry does NOT exist\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4SetValUsmUserStatus == SNMP_ROWSTATUS_NOTINSERVICE)
    {
        if (pSnmpUsmEntry == NULL)
        {
            SNMP_TRC (SNMP_FAILURE_TRC, "nmhSetUsmUserStatus: "
                      " User Entry does NOT exist\n");
            return SNMP_FAILURE;
        }
        pSnmpUsmEntry->u4UsmUserStatus = UNDER_CREATION;
    }
    else                        /* SNMP_ROWSTATUS_ACTIVE */
    {
        if (pSnmpUsmEntry == NULL)
        {
            SNMP_TRC (SNMP_FAILURE_TRC, "nmhSetUsmUserStatus: "
                      " User Entry does NOT exist\n");
            return SNMP_FAILURE;
        }
        if ((pSnmpUsmEntry->u4UsmUserAuthProtocol != SNMP_NO_AUTH) &&
            (pSnmpUsmEntry->UsmUserAuthKeyChange.i4_Length == SNMP_ZERO))
        {
            SNMP_TRC (SNMP_FAILURE_TRC, "nmhSetUsmUserStatus: "
                      "Unable to make the user as active\n");
            /* Cannot be made active until the corresponding 
             * usmUserAuthKeyChange have been set. */
            return SNMP_FAILURE;
        }
        pSnmpUsmEntry->u4UsmUserStatus = SNMP_ACTIVE;
    }
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function exit\r\n", __func__);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2UsmUserCloneFrom
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName

                The Object 
                testValUsmUserCloneFrom
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2UsmUserCloneFrom (UINT4 *pu4ErrorCode,
                           tSNMP_OCTET_STRING_TYPE * pUsmUserEngineID,
                           tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                           tSNMP_OID_TYPE * pTestValUsmUserCloneFrom)
{
    tSNMP_OCTET_STRING_TYPE EngineID;
    tSNMP_OCTET_STRING_TYPE UserName;
    UINT4               u4Count = SNMP_ZERO, u4Len = SNMP_ZERO, u4Offset =
        SNMP_ZERO;
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL, *pNewSnmpUsmEntry = NULL;
    UINT1               au1EngineID[SNMP_MAX_STR_ENGINEID_LEN];
    UINT1               au1UserName[SNMP_MAX_ADMIN_STR_LENGTH];

    EngineID.pu1_OctetList = au1EngineID;
    UserName.pu1_OctetList = au1UserName;
    pSnmpUsmEntry = SNMPGetUsmEntry (pUsmUserEngineID, pUsmUserName);
    if (pSnmpUsmEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (pSnmpUsmEntry->u4UsmUserCloneFrom != SNMP_ZERO)
    {
        return SNMP_SUCCESS;
    }
    u4Offset = SNMP_STD_USM_OID_LEN + 1;
    u4Len = pTestValUsmUserCloneFrom->pu4_OidList[u4Offset];
    u4Offset++;
    if (u4Len > SNMP_MAX_STR_ENGINEID_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (MEMCMP (pTestValUsmUserCloneFrom->pu4_OidList,
                au4StdUsmOid, (SNMP_STD_USM_OID_LEN * sizeof (UINT4))) != 0)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    for (u4Count = SNMP_ZERO; u4Count < u4Len; u4Count++)
    {
        if (pTestValUsmUserCloneFrom->pu4_OidList[u4Offset] > MAX_UNIT1_VALUE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        EngineID.pu1_OctetList[u4Count] = (UINT1) pTestValUsmUserCloneFrom->
            pu4_OidList[u4Offset];
        u4Offset++;
    }
    EngineID.i4_Length = (INT4) u4Len;
    u4Len = pTestValUsmUserCloneFrom->pu4_OidList[u4Offset];
    u4Offset++;
    if (u4Len > SNMP_MAX_ADMIN_STR_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    for (u4Count = SNMP_ZERO; u4Count < u4Len; u4Count++)
    {
        if (pTestValUsmUserCloneFrom->pu4_OidList[u4Offset] > MAX_UNIT1_VALUE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        UserName.pu1_OctetList[u4Count] = (UINT1) pTestValUsmUserCloneFrom->
            pu4_OidList[u4Offset];
        u4Offset++;
    }
    UserName.i4_Length = (INT4) u4Len;
    pNewSnmpUsmEntry = SNMPGetUsmEntry (&EngineID, &UserName);
    if ((pNewSnmpUsmEntry == NULL) || (pNewSnmpUsmEntry == pSnmpUsmEntry) ||
        (pNewSnmpUsmEntry->u4UsmUserStatus != SNMP_ACTIVE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2UsmUserAuthProtocol
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName

                The Object 
                testValUsmUserAuthProtocol
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2UsmUserAuthProtocol (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE * pUsmUserEngineID,
                              tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                              tSNMP_OID_TYPE * pTestValUsmUserAuthProtocol)
{
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    UINT4               u4SetValue = SNMP_ZERO;
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function entry\r\n", __func__);
    pSnmpUsmEntry = SNMPGetUsmEntry (pUsmUserEngineID, pUsmUserName);
    if (pSnmpUsmEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        SNMP_TRC (SNMP_FAILURE_TRC,
                  "nmhTestv2UsmUserAuthProtocol: NULL usm Entry\n");
        return SNMP_FAILURE;
    }
    if (pTestValUsmUserAuthProtocol->u4_Length !=
        (SNMP_STD_AUTH_OID_LEN + SNMP_ONE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        SNMP_TRC (SNMP_FAILURE_TRC, "nmhTestv2UsmUserAuthProtocol: "
                  "SNMP Auth protocol length is wrong\n");
        return SNMP_FAILURE;
    }
    u4SetValue = pTestValUsmUserAuthProtocol->
        pu4_OidList[SNMP_STD_AUTH_OID_LEN];
    if (!(u4SetValue == SNMP_MD5 || u4SetValue == SNMP_SHA ||
          u4SetValue == SNMP_SHA256 || u4SetValue == SNMP_SHA384 ||
          u4SetValue == SNMP_NO_AUTH || u4SetValue == SNMP_SHA512))
    {
        SNMP_TRC2 (SNMP_FAILURE_TRC, "nmhTestv2UsmUserAuthProtocol: "
                   "Invalid Auth Protocol [%d] for the user [%s]\n",
                   u4SetValue, pUsmUserName->pu1_OctetList);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#ifdef FIPS_WANTED
    if (SnmpIsAuthSecAlgoSupported (u4SetValue) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        SNMP_TRC2 (SNMP_FAILURE_TRC, "nmhTestv2UsmUserAuthProtocol: "
                   "Unsupported Auth Protocol [%d] for the user [%s]\n",
                   u4SetValue, pUsmUserName->pu1_OctetList);
        return SNMP_FAILURE;
    }
#endif
    if (u4SetValue == SNMP_NO_AUTH &&
        pSnmpUsmEntry->u4UsmUserPrivProtocol != SNMP_NO_PRIV)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        SNMP_TRC2 (SNMP_FAILURE_TRC, "nmhTestv2UsmUserAuthProtocol: "
                   "Inconsistent Auth Protocol [%d] for the user [%s]\n",
                   u4SetValue, pUsmUserName->pu1_OctetList);
        return SNMP_FAILURE;
    }
    SNMP_TRC2 (SNMP_DEBUG_TRC, "nmhTestv2UsmUserAuthProtocol: "
               "Successfullly validated Auth protocol [%d] for the user [%s]\n",
               u4SetValue, pUsmUserName->pu1_OctetList);
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function exit\r\n", __func__);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2UsmUserAuthKeyChange
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName

                The Object 
                testValUsmUserAuthKeyChange
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2UsmUserAuthKeyChange (UINT4 *pu4ErrorCode,
                               tSNMP_OCTET_STRING_TYPE * pUsmUserEngineID,
                               tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValUsmUserAuthKeyChange)
{
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function entry\r\n", __func__);
    pSnmpUsmEntry = SNMPGetUsmEntry (pUsmUserEngineID, pUsmUserName);
    if (pSnmpUsmEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        SNMP_TRC (SNMP_FAILURE_TRC,
                  "nmhTestv2UsmUserAuthKeyChange: NULL usm Entry\n");
        return SNMP_FAILURE;
    }

    if (pTestValUsmUserAuthKeyChange->i4_Length == SNMP_ZERO)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        SNMP_TRC (SNMP_FAILURE_TRC, "nmhTestv2UsmUserAuthKeyChange: "
                  "SNMP Auth protocol key length is wrong\n");
        return SNMP_FAILURE;
    }
    if (pSnmpUsmEntry->u4UsmUserAuthProtocol == SNMP_NO_AUTH)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        SNMP_TRC1 (SNMP_FAILURE_TRC, "nmhTestv2UsmUserAuthKeyChange: "
                   "Auth protocol key change is not applicable for NO AUTH user [%s]\n",
                   pUsmUserName->pu1_OctetList);
        return SNMP_FAILURE;
    }
#ifdef FIPS_WANTED
    if (SnmpIsAuthSecAlgoSupported (pSnmpUsmEntry->u4UsmUserAuthProtocol)
        == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        SNMP_TRC2 (SNMP_FAILURE_TRC, "nmhTestv2UsmUserAuthKeyChange: "
                   "Unsupported Auth Protocol [%d] for the user [%s]\n",
                   pSnmpUsmEntry->u4UsmUserAuthProtocol,
                   pUsmUserName->pu1_OctetList);
        return SNMP_FAILURE;
    }
#endif
    SNMP_TRC1 (SNMP_DEBUG_TRC, "nmhTestv2UsmUserAuthKeyChange: "
               "Successfullly validated Auth protocol key for the user [%s]\n",
               pUsmUserName->pu1_OctetList);
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function exit\r\n", __func__);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2UsmUserOwnAuthKeyChange
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName

                The Object 
                testValUsmUserOwnAuthKeyChange
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2UsmUserOwnAuthKeyChange (UINT4 *pu4ErrorCode,
                                  tSNMP_OCTET_STRING_TYPE * pUsmUserEngineID,
                                  tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValUsmUserOwnAuthKeyChange)
{
    if ((nmhTestv2UsmUserAuthKeyChange (pu4ErrorCode, pUsmUserEngineID,
                                        pUsmUserName,
                                        pTestValUsmUserOwnAuthKeyChange)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (pUsmUserName->i4_Length != (INT4) (STRLEN (gau1MsgUsrName)))
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        return SNMP_FAILURE;
    }
    if ((MEMCMP
         (pUsmUserName->pu1_OctetList, gau1MsgUsrName,
          pUsmUserName->i4_Length)) != 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        return SNMP_FAILURE;
    }
    if (!(gau1MsgFlag & SNMP_AUTH_BIT))
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2UsmUserPrivProtocol
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName

                The Object 
                testValUsmUserPrivProtocol
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2UsmUserPrivProtocol (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE * pUsmUserEngineID,
                              tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                              tSNMP_OID_TYPE * pTestValUsmUserPrivProtocol)
{
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    UINT4               u4SetValue = SNMP_ZERO;
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function entry\r\n", __func__);
    pSnmpUsmEntry = SNMPGetUsmEntry (pUsmUserEngineID, pUsmUserName);
    if (pSnmpUsmEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        SNMP_TRC (SNMP_FAILURE_TRC,
                  "nmhTestv2UsmUserPrivProtocol: NULL usm Entry\n");
        return SNMP_FAILURE;
    }
    if (pTestValUsmUserPrivProtocol->u4_Length !=
        (SNMP_STD_PRIV_OID_LEN + SNMP_ONE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        SNMP_TRC (SNMP_FAILURE_TRC, "nmhTestv2UsmUserPrivProtocol: "
                  "SNMP Auth protocol length is wrong\n");
        return SNMP_FAILURE;
    }
    u4SetValue = pTestValUsmUserPrivProtocol->
        pu4_OidList[SNMP_STD_PRIV_OID_LEN];
    if (!(u4SetValue == SNMP_DES_CBC || u4SetValue == SNMP_AESCFB128 ||
          u4SetValue == SNMP_AESCFB192 || u4SetValue == SNMP_AESCFB256 ||
          u4SetValue == SNMP_AESCTR || u4SetValue == SNMP_AESCTR192
          || u4SetValue == SNMP_AESCTR256 || u4SetValue == SNMP_TDES_CBC
          || u4SetValue == SNMP_NO_PRIV))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        SNMP_TRC2 (SNMP_FAILURE_TRC, "nmhTestv2UsmUserPrivProtocol: "
                   "Invalid PRIV Protocol [%d] for the user [%s]\n",
                   u4SetValue, pUsmUserName->pu1_OctetList);
        return SNMP_FAILURE;
    }
#ifdef FIPS_WANTED
    if (SnmpIsPrivSecAlgoSupported (u4SetValue) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        SNMP_TRC2 (SNMP_FAILURE_TRC, "nmhTestv2UsmUserPrivProtocol: "
                   "Unsupported PRIV Protocol [%d] for the user [%s]\n",
                   u4SetValue, pUsmUserName->pu1_OctetList);
        return SNMP_FAILURE;
    }
#endif
    if ((u4SetValue == SNMP_DES_CBC || u4SetValue == SNMP_AESCFB128) &&
        pSnmpUsmEntry->u4UsmUserAuthProtocol == SNMP_NO_AUTH)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        SNMP_TRC2 (SNMP_FAILURE_TRC, "nmhTestv2UsmUserPrivProtocol: "
                   "Inconsistent PRIV Protocol [%d] for the user [%s]\n",
                   u4SetValue, pUsmUserName->pu1_OctetList);
        return SNMP_FAILURE;
    }
    SNMP_TRC2 (SNMP_DEBUG_TRC, "nmhTestv2UsmUserPrivProtocol: "
               "Successfullly validated PRIV protocol [%d] for the user [%s]\n",
               u4SetValue, pUsmUserName->pu1_OctetList);
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function exit\r\n", __func__);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2UsmUserPrivKeyChange
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName

                The Object 
                testValUsmUserPrivKeyChange
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2UsmUserPrivKeyChange (UINT4 *pu4ErrorCode,
                               tSNMP_OCTET_STRING_TYPE * pUsmUserEngineID,
                               tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValUsmUserPrivKeyChange)
{
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function entry\r\n", __func__);
    pSnmpUsmEntry = SNMPGetUsmEntry (pUsmUserEngineID, pUsmUserName);
    if (pSnmpUsmEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        SNMP_TRC (SNMP_FAILURE_TRC,
                  "nmhTestv2UsmUserPrivKeyChange: NULL usm Entry\n");
        return SNMP_FAILURE;
    }

    if (pTestValUsmUserPrivKeyChange->i4_Length == SNMP_ZERO)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        SNMP_TRC (SNMP_FAILURE_TRC, "nmhTestv2UsmUserPrivKeyChange: "
                  "SNMP Auth protocol length is wrong\n");
        return SNMP_FAILURE;
    }
    if (pSnmpUsmEntry->u4UsmUserAuthProtocol == SNMP_NO_AUTH)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        SNMP_TRC1 (SNMP_FAILURE_TRC, "nmhTestv2UsmUserPrivKeyChange: "
                   "PRIV protocol key change is not applicable for NO AUTH user [%s]\n",
                   pUsmUserName->pu1_OctetList);
        return SNMP_FAILURE;
    }
#ifdef FIPS_WANTED
    if (SnmpIsPrivSecAlgoSupported (pSnmpUsmEntry->u4UsmUserPrivProtocol)
        == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        SNMP_TRC2 (SNMP_FAILURE_TRC, "nmhTestv2UsmUserPrivKeyChange: "
                   "Unsupported Auth Protocol [%d] for the user [%s]\n",
                   pSnmpUsmEntry->u4UsmUserAuthProtocol,
                   pUsmUserName->pu1_OctetList);
        return SNMP_FAILURE;
    }
#endif
    SNMP_TRC1 (SNMP_DEBUG_TRC, "nmhTestv2UsmUserPrivKeyChange: "
               "Successfullly validated PRIV protocol key for the user [%s]\n",
               pUsmUserName->pu1_OctetList);
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function exit\r\n", __func__);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2UsmUserOwnPrivKeyChange
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName

                The Object 
                testValUsmUserOwnPrivKeyChange
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2UsmUserOwnPrivKeyChange (UINT4 *pu4ErrorCode,
                                  tSNMP_OCTET_STRING_TYPE * pUsmUserEngineID,
                                  tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValUsmUserOwnPrivKeyChange)
{
    if ((nmhTestv2UsmUserPrivKeyChange (pu4ErrorCode, pUsmUserEngineID,
                                        pUsmUserName,
                                        pTestValUsmUserOwnPrivKeyChange)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (pUsmUserName->i4_Length != (INT4) (STRLEN (gau1MsgUsrName)))
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        return SNMP_FAILURE;
    }
    if ((MEMCMP
         (pUsmUserName->pu1_OctetList, gau1MsgUsrName,
          pUsmUserName->i4_Length)) != 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        return SNMP_FAILURE;
    }
    if (!(gau1MsgFlag & SNMP_AUTH_BIT))
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2UsmUserPublic
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName

                The Object 
                testValUsmUserPublic
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2UsmUserPublic (UINT4 *pu4ErrorCode,
                        tSNMP_OCTET_STRING_TYPE * pUsmUserEngineID,
                        tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                        tSNMP_OCTET_STRING_TYPE * pTestValUsmUserPublic)
{
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function entry\r\n", __func__);
    pSnmpUsmEntry = SNMPGetUsmEntry (pUsmUserEngineID, pUsmUserName);
    if (pSnmpUsmEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        SNMP_TRC (SNMP_FAILURE_TRC, "nmhTestv2UsmUserPublic: NULL usm Entry\n");
        return SNMP_FAILURE;
    }
    if (pTestValUsmUserPublic->i4_Length > USM_MAX_AUTHN_SIZE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        SNMP_TRC (SNMP_FAILURE_TRC,
                  "nmhTestv2UsmUserPublic: Public Length is wrong\n");
        return SNMP_FAILURE;
    }
    SNMP_TRC1 (SNMP_DEBUG_TRC, "nmhTestv2UsmUserPublic: "
               "Successfullly validated public for the user [%s]\n",
               pUsmUserName->pu1_OctetList);
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function exit\r\n", __func__);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2UsmUserStorageType
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName

                The Object 
                testValUsmUserStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2UsmUserStorageType (UINT4 *pu4ErrorCode,
                             tSNMP_OCTET_STRING_TYPE * pUsmUserEngineID,
                             tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                             INT4 i4TestValUsmUserStorageType)
{
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function entry\r\n", __func__);
    pSnmpUsmEntry = SNMPGetUsmEntry (pUsmUserEngineID, pUsmUserName);
    if (pSnmpUsmEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        SNMP_TRC (SNMP_FAILURE_TRC,
                  "nmhTestv2UsmUserStorageType: NULL usm Entry\n");
        return SNMP_FAILURE;
    }
    if ((i4TestValUsmUserStorageType < SNMP3_STORAGE_TYPE_OTHER) ||
        (i4TestValUsmUserStorageType > SNMP3_STORAGE_TYPE_READONLY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        SNMP_TRC2 (SNMP_FAILURE_TRC, "nmhTestv2UsmUserStorageType: "
                   "Wrong Storage Type [%d] for the user [%s]\n",
                   i4TestValUsmUserStorageType, pUsmUserName->pu1_OctetList);
        return SNMP_FAILURE;
    }
    if ((i4TestValUsmUserStorageType != SNMP3_STORAGE_TYPE_VOLATILE) &&
        (i4TestValUsmUserStorageType != SNMP3_STORAGE_TYPE_NONVOLATILE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        SNMP_TRC2 (SNMP_FAILURE_TRC, "nmhTestv2UsmUserStorageType: "
                   "Invalid Storage Type [%d] for the user [%s]\n",
                   i4TestValUsmUserStorageType, pUsmUserName->pu1_OctetList);
        return SNMP_FAILURE;
    }
    SNMP_TRC1 (SNMP_DEBUG_TRC, "nmhTestv2UsmUserPrivKeyChange: "
               "Successfullly validated  Storage Type for the user [%s]\n",
               pUsmUserName->pu1_OctetList);
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function exit\r\n", __func__);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2UsmUserStatus
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName

                The Object 
                testValUsmUserStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2UsmUserStatus (UINT4 *pu4ErrorCode,
                        tSNMP_OCTET_STRING_TYPE * pUsmUserEngineID,
                        tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                        INT4 i4TestValUsmUserStatus)
{
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;

    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function entry\r\n", __func__);
    pSnmpUsmEntry = SNMPGetUsmEntry (pUsmUserEngineID, pUsmUserName);
    if (i4TestValUsmUserStatus == SNMP_ROWSTATUS_CREATEANDWAIT ||
        i4TestValUsmUserStatus == SNMP_ROWSTATUS_CREATEANDGO)
    {
        if (pSnmpUsmEntry != NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            SNMP_TRC (SNMP_FAILURE_TRC,
                      "nmhTestv2UsmUserStorageType: NULL usm Entry\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4TestValUsmUserStatus == SNMP_ROWSTATUS_ACTIVE ||
             i4TestValUsmUserStatus == SNMP_ROWSTATUS_NOTINSERVICE)
    {
        if (pSnmpUsmEntry == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            SNMP_TRC (SNMP_FAILURE_TRC,
                      "nmhTestv2UsmUserStorageType: NULL usm Entry\n");
            return SNMP_FAILURE;
        }
    }
    else if (i4TestValUsmUserStatus == SNMP_ROWSTATUS_DESTROY)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        SNMP_TRC1 (SNMP_FAILURE_TRC,
                   "nmhTestv2UsmUserStatus: Invalid usm Status [%d]\n",
                   i4TestValUsmUserStatus);
        return SNMP_FAILURE;
    }
    SNMP_TRC1 (SNMP_DEBUG_TRC, "nmhTestv2UsmUserStatus: "
               "Successfullly validated usm row status for the user [%s]\n",
               pUsmUserName->pu1_OctetList);
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function exit\r\n", __func__);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2UsmUserTable
 Input       :  The Indices
                UsmUserEngineID
                UsmUserName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2UsmUserTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
