/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved 
 *
 * $Id: fsproxy.h,v 1.1 2015/04/28 12:35:01 siva Exp $
 *
 * Description: Aricent Snmp PrpProxy Table macros and global vars
 *******************************************************************/
#ifndef _FSPROXY_H
#define _FSPROXY_H
tTMO_SLL              gPrpProxyTableSll;
tPrpPrxMibIdEntry     gaPrpProxyTableEntry[MAX_PROXY_TABLE_ENTRY];
tTMO_SLL              gPrpPrxMibIdSll;

UINT1 au1PrpProxyName[MAX_PROXY_TABLE_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
UINT4 au1PrpProxyMibId[MAX_PROXY_TABLE_ENTRY][MAX_OID_LENGTH];
UINT1 au1PrpProxTgtPrmIn[MAX_PROX_TGT_PARAM_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
UINT1 au1PrpProxSglTgtOut[MAX_PROXY_TABLE_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
UINT1 au1PrpProxMulTgtOut[MAX_PROXY_TABLE_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];

#endif
