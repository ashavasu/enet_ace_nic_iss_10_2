/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: snmcomwr.h,v 1.5 2015/04/28 12:35:02 siva Exp $
 *
 * Description: snmp community mapping table wrapper routines prototypes
 *******************************************************************/

#ifndef _SNMCOMWR_H
#define _SNMCOMWR_H
INT4 GetNextIndexSnmpCommunityTable(tSnmpIndex *, tSnmpIndex *);
INT4 GetNextIndexSnmpTargetAddrExtTable(tSnmpIndex *, tSnmpIndex *);


INT4 SnmpCommunityIndexGet(tSnmpIndex *, tRetVal *);
INT4 SnmpCommunityNameGet(tSnmpIndex *, tRetVal *);
INT4 SnmpCommunitySecurityNameGet(tSnmpIndex *, tRetVal *);
INT4 SnmpCommunityContextEngineIDGet(tSnmpIndex *, tRetVal *);
INT4 SnmpCommunityContextNameGet(tSnmpIndex *, tRetVal *);
INT4 SnmpCommunityTransportTagGet(tSnmpIndex *, tRetVal *);
INT4 SnmpCommunityStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 SnmpCommunityStatusGet(tSnmpIndex *, tRetVal *);
INT4 SnmpCommunityNameSet(tSnmpIndex *, tRetVal *);
INT4 SnmpCommunitySecurityNameSet(tSnmpIndex *, tRetVal *);
INT4 SnmpCommunityContextEngineIDSet(tSnmpIndex *, tRetVal *);
INT4 SnmpCommunityContextNameSet(tSnmpIndex *, tRetVal *);
INT4 SnmpCommunityTransportTagSet(tSnmpIndex *, tRetVal *);
INT4 SnmpCommunityStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 SnmpCommunityStatusSet(tSnmpIndex *, tRetVal *);
INT4 SnmpCommunityNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpCommunitySecurityNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpCommunityContextEngineIDTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpCommunityContextNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpCommunityTransportTagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpCommunityStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpCommunityStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpCommunityTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);







INT4 SnmpTargetAddrTMaskGet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetAddrMMSGet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetAddrTMaskSet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetAddrMMSSet(tSnmpIndex *, tRetVal *);
INT4 SnmpTargetAddrTMaskTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpTargetAddrMMSTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpTargetAddrExtTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


#endif /* _SNMCOMWR_H */
