/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsproxy.c,v 1.1 2015/04/28 12:35:01 siva Exp $
 *
 * Description:Routines for Snmp agent PropProxy table 
 *******************************************************************/

#include "snmpcmn.h"
#include "fsproxy.h"

/*********************************************************************
*  Function Name : SnmpInitPrpProxyTable
*  Description   : Function Init PrpProxy Table 
*  Parameter(s)  : None
*  Return Values : None
*********************************************************************/
INT4
SnmpInitPrpProxyTable ()
{
    UINT4               u4Index = SNMP_ZERO;
    MEMSET (gaPrpProxyTableEntry, SNMP_ZERO, sizeof (gaPrpProxyTableEntry));
    TMO_SLL_Init (&gPrpProxyTableSll);
    TMO_SLL_Init (&gPrpPrxMibIdSll);
    for (u4Index = SNMP_ZERO; u4Index < MAX_PROXY_TABLE_ENTRY; u4Index++)
    {
        gaPrpProxyTableEntry[u4Index].PrpPrxEntry.PrpPrxMibName.pu1_OctetList =
            au1PrpProxyName[u4Index];
        gaPrpProxyTableEntry[u4Index].PrpPrxEntry.PrpPrxMibID.pu4_OidList =
            au1PrpProxyMibId[u4Index];
        gaPrpProxyTableEntry[u4Index].PrpPrxEntry.PrpPrxTargetParamsIn.
            pu1_OctetList = au1PrpProxTgtPrmIn[u4Index];
        gaPrpProxyTableEntry[u4Index].PrpPrxEntry.PrpPrxSingleTargetOut.
            pu1_OctetList = au1PrpProxSglTgtOut[u4Index];
        gaPrpProxyTableEntry[u4Index].PrpPrxEntry.PrpPrxMultipleTargetOut.
            pu1_OctetList = au1PrpProxMulTgtOut[u4Index];
        gaPrpProxyTableEntry[u4Index].PrpPrxEntry.u4Index = u4Index;
        gaPrpProxyTableEntry[u4Index].PrpPrxEntry.i4Status = SNMP_INACTIVE;
    }
    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : SNMPAddPrpProxyEntry 
*  Description   : Function to Add the PrpProxy from the table
*  Parameter(s)  : pPrpProxyName - Prop Proxy Name
*  Return Values : SNMP_SUCCESS/SNMP_FAILURE
*********************************************************************/

INT4
SNMPAddPrpProxyEntry (tSNMP_OCTET_STRING_TYPE * pPrpProxyName)
{
    UINT4               u4Index = SNMP_ZERO;
    tSNMP_OCTET_STRING_TYPE OctetString;

    if (SNMPGetPrpProxyEntryFromName (pPrpProxyName) != NULL)
    {
        return SNMP_FAILURE;
    }

    for (u4Index = SNMP_ZERO; u4Index < MAX_PROXY_TABLE_ENTRY; u4Index++)
    {
        if (gaPrpProxyTableEntry[u4Index].PrpPrxEntry.i4Status == SNMP_INACTIVE)
        {
            gaPrpProxyTableEntry[u4Index].PrpPrxEntry.i4Status = UNDER_CREATION;
            gaPrpProxyTableEntry[u4Index].PrpPrxEntry.i4Storage =
                SNMP3_STORAGE_TYPE_NONVOLATILE;
            OctetString.pu1_OctetList = (UINT1 *) "";
            OctetString.i4_Length = SNMP_ZERO;
            SNMPCopyOctetString (&
                                 (gaPrpProxyTableEntry[u4Index].PrpPrxEntry.
                                  PrpPrxMibName), &OctetString);
            SNMPCopyOctetString (&
                                 (gaPrpProxyTableEntry[u4Index].PrpPrxEntry.
                                  PrpPrxMibName), pPrpProxyName);
            gaPrpProxyTableEntry[u4Index].PrpPrxEntry.i4PrpPrxMibType =
                SNMP_ZERO;
            gaPrpProxyTableEntry[u4Index].PrpPrxEntry.PrpPrxMibID.u4_Length =
                SNMP_ZERO;
            gaPrpProxyTableEntry[u4Index].PrpPrxEntry.PrpPrxTargetParamsIn.
                i4_Length = SNMP_ZERO;
            gaPrpProxyTableEntry[u4Index].PrpPrxEntry.PrpPrxSingleTargetOut.
                i4_Length = SNMP_ZERO;
            gaPrpProxyTableEntry[u4Index].PrpPrxEntry.PrpPrxMultipleTargetOut.
                i4_Length = SNMP_ZERO;

            SNMPAddPrpProxyEntrySll (&
                                     (gaPrpProxyTableEntry[u4Index].PrpPrxEntry.
                                      link));
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/*********************************************************************
*  Function Name : SNMPAddPrpProxyEntrySll 
*  Description   : Function to add the Proxy entry to the list
*  Parameter(s)  : pNode - Node to be added 
*  Return Values : None
*********************************************************************/

VOID
SNMPAddPrpProxyEntrySll (tTMO_SLL_NODE * pNode)
{
    tPrpProxyTableEntry *pCurEntry = NULL;
    tPrpProxyTableEntry *pInEntry = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pPrevNode = NULL;
    pInEntry = (tPrpProxyTableEntry *) pNode;

    TMO_SLL_Scan (&gPrpProxyTableSll, pLstNode, tTMO_SLL_NODE *)
    {
        pCurEntry = (tPrpProxyTableEntry *) pLstNode;

        if (SNMPCompareImpliedOctetString (&(pInEntry->PrpPrxMibName),
                                           &(pCurEntry->PrpPrxMibName)) ==
            SNMP_LESSER)
        {
            break;
        }
        pPrevNode = pLstNode;
    }
    TMO_SLL_Insert (&gPrpProxyTableSll, pPrevNode, pNode);
}

/*********************************************************************
*  Function Name : SNMPAddPrpPrxMibIdEntrySll 
*  Description   : Function to add the PrpPrx Mib entry to the list
*  Parameter(s)  : pNode - Node to be added 
*  Return Values : None
*********************************************************************/

VOID
SNMPAddPrpPrxMibIdEntrySll (tPrpProxyTableEntry * pProxyEntry)
{
    tPrpPrxMibIdEntry  *pCurEntry = NULL;
    tPrpPrxMibIdEntry  *pInEntry = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pPrevNode = NULL;
    UINT4               u4Count = SNMP_ZERO;
    UINT4               u4Status = SNMP_ZERO;

    pInEntry = &(gaPrpProxyTableEntry[pProxyEntry->u4Index]);
    pNode = &(pInEntry->link);

    TMO_SLL_Scan (&gPrpPrxMibIdSll, pLstNode, tTMO_SLL_NODE *)
    {
        pCurEntry = (tPrpPrxMibIdEntry *) pLstNode;

        u4Status = OIDCompare (pInEntry->PrpPrxEntry.PrpPrxMibID,
                               pCurEntry->PrpPrxEntry.PrpPrxMibID, &u4Count);

        if (u4Status == SNMP_LESSER)
        {
            break;
        }

        pPrevNode = pLstNode;
    }
    TMO_SLL_Insert (&gPrpPrxMibIdSll, pPrevNode, pNode);
}

/*********************************************************************
*  Function Name : SNMPGetPrpProxyEntryFromName 
*  Description   : Function to get the corresponding entry from
*                 : Prop Proxy Table
*  Parameter(s)  : pPrpProxyName - Prop Proxy Name
*  Return Values : On Success to return the Entry
*                  on Failure to return NULL
*********************************************************************/
tPrpProxyTableEntry *
SNMPGetPrpProxyEntryFromName (tSNMP_OCTET_STRING_TYPE * pPrpProxyName)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tPrpProxyTableEntry *pPrpProxy = NULL;

    TMO_SLL_Scan (&gPrpProxyTableSll, pListNode, tTMO_SLL_NODE *)
    {
        pPrpProxy = (tPrpProxyTableEntry *) pListNode;
        if (pPrpProxy->i4Status != SNMP_INACTIVE)
        {
            if (SNMPCompareExactMatchOctetString
                (pPrpProxyName, &(pPrpProxy->PrpPrxMibName)) == SNMP_EQUAL)
            {
                return pPrpProxy;
            }
        }
    }
    return NULL;
}

/*********************************************************************
*  Function Name : SNMPGetPrpProxyEntryFromMibId 
*  Description   : Function to get the corresponding entry from PrpProxy Table
*  Parameter(s)  : pMibID - Mib ID
*  Return Values : On Success to return the Entry
*                  on Failure to return NULL
*********************************************************************/
tPrpProxyTableEntry *
SNMPGetPrpProxyEntryFromMibId (tSNMP_OID_TYPE * pMibID)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tPrpProxyTableEntry *pProxy = NULL;
    tPrpPrxMibIdEntry  *pPrpPrxMibEntry = NULL;
    INT4                i4RetVal = 0;

    TMO_SLL_Scan (&gPrpPrxMibIdSll, pListNode, tTMO_SLL_NODE *)
    {
        pPrpPrxMibEntry = (tPrpPrxMibIdEntry *) pListNode;
        pProxy = &(pPrpPrxMibEntry->PrpPrxEntry);
        i4RetVal = SNMPCompareRootOID (pMibID, &(pProxy->PrpPrxMibID));
        if (i4RetVal == SNMP_EQUAL)
        {
            return pProxy;
        }
        else if (i4RetVal == SNMP_LESSER)
        {
            break;
        }
    }
    return NULL;
}

/*********************************************************************
*  Function Name : SNMPGetPrpProxyEntry 
*  Description   : Function to get the corresponding entry from PrpProxy Table
*  Parameter(s)  : pPrpPrxMibID - Mib ID
*                 : i4ProxyType - PDU Type (Read, Write, Trap, Inform)
*                 : pProxyEntry - Array of Entry
*  Return Values : On Success to return the Array of Entry
*                  on Failure to return NULL
*********************************************************************/
INT4
SNMPGetPrpProxyEntry (tSNMP_OID_TYPE * pPrpPrxMibID,
                      INT4 i4ProxyType, tProxEntryArray * pProxyEntry)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tPrpProxyTableEntry *pProxy = NULL;
    tPrpPrxMibIdEntry  *pPrpPrxMibEntry = NULL;
    INT4                count = 0;
    INT4                i4RetVal = 0;

    pProxyEntry->i4Count = 0;

    TMO_SLL_Scan (&gPrpPrxMibIdSll, pListNode, tTMO_SLL_NODE *)
    {
        pPrpPrxMibEntry = (tPrpPrxMibIdEntry *) pListNode;
        pProxy = &(pPrpPrxMibEntry->PrpPrxEntry);
        if (pProxy->i4PrpPrxMibType == i4ProxyType)
        {
            i4RetVal = SNMPCompareRootOID (pPrpPrxMibID,
                                           &(pProxy->PrpPrxMibID));
            if (i4RetVal == SNMP_EQUAL)
            {
                pProxyEntry->pEntry[count] = (tPrpProxyTableEntry *) pProxy;
                count++;
            }
            else if (i4RetVal == SNMP_LESSER)
            {
                /* Break from here as the List is in sorted order and
                   further there will not be any entry which will match */
                break;
            }
        }
        else if (count > SNMP_ZERO)
        {
            i4RetVal = SNMPCompareRootOID (pPrpPrxMibID,
                                           &(pProxy->PrpPrxMibID));
            if (i4RetVal != SNMP_EQUAL)
            {
                /* Break from here */
                break;
            }
        }
    }

    pProxyEntry->i4Count = count;

    if (count == SNMP_ZERO)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : SNMPGetFirstPrpProxy 
*  Description   : Function to get the First Entry from the PrpProxy Table 
*  Parameter(s)  : None 
*  Return Values : Proxy Entry 
*********************************************************************/
tPrpProxyTableEntry *
SNMPGetFirstPrpProxy ()
{
    tPrpProxyTableEntry *pProxyEntry = NULL;
    pProxyEntry = (tPrpProxyTableEntry *) TMO_SLL_First (&gPrpProxyTableSll);
    return pProxyEntry;
}

/*********************************************************************
*  Function Name : SNMPGetNextPrpProxyEntry 
*  Description   : Function to get the next entry form Proxy Table
*  Parameter(s)  : pPrpProxyName - Proxy Name
*  Return Values : Success will return the Proxy Name
*                  Failure will return NULL
*********************************************************************/
tPrpProxyTableEntry *
SNMPGetNextPrpProxyEntry (tSNMP_OCTET_STRING_TYPE * pPrpProxyName)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tPrpProxyTableEntry *pProxyEntry = NULL;

    TMO_SLL_Scan (&gPrpProxyTableSll, pListNode, tTMO_SLL_NODE *)
    {
        pProxyEntry = (tPrpProxyTableEntry *) pListNode;
        if (pProxyEntry->i4Status != SNMP_INACTIVE)
        {
            if (SNMPCompareImpliedOctetString
                (pPrpProxyName, &(pProxyEntry->PrpPrxMibName)) == SNMP_LESSER)
            {
                return pProxyEntry;
            }
        }
    }
    return NULL;
}

/*********************************************************************
*  Function Name : SNMPDeletePrpProxyEntry 
*  Description   : Function to delete the PrpProxy entry 
*  Parameter(s)  : pPrpProxyName - Proxy Name
*  Return Values : SNMP_SUCCESS/SNMP_FAILURE 
*********************************************************************/

INT4
SNMPDeletePrpProxyEntry (tSNMP_OCTET_STRING_TYPE * pPrpProxyName)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tPrpProxyTableEntry *pProxyEntry = NULL;

    TMO_SLL_Scan (&gPrpProxyTableSll, pListNode, tTMO_SLL_NODE *)
    {
        pProxyEntry = (tPrpProxyTableEntry *) pListNode;
        if (SNMPCompareOctetString (pPrpProxyName,
                                    &(pProxyEntry->PrpPrxMibName)) ==
            SNMP_EQUAL)
        {
            SNMPDelPrpPrxMibIdEntry (pProxyEntry);
            pProxyEntry->i4Status = SNMP_INACTIVE;
            SNMPDelPrpProxyEntrySll (&(pProxyEntry->link));
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/*********************************************************************
*  Function Name : SNMPDelPrpPrxMibIdEntry 
*  Description   : Function to delete the PrpProxyMib entry 
*  Parameter(s)  : pPrpProxyName - Proxy Name
*  Return Values : SNMP_SUCCESS/SNMP_FAILURE 
*********************************************************************/

INT4
SNMPDelPrpPrxMibIdEntry (tPrpProxyTableEntry * pProxyEntry)
{
    UINT4               u4Index = SNMP_ZERO;

    u4Index = pProxyEntry->u4Index;
    if (pProxyEntry->i4Status == SNMP_ACTIVE)
    {
        SNMPDelPrpPrxMibIdEntrySll (&(gaPrpProxyTableEntry[u4Index].link));
    }

    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : SNMPDelPrpProxyEntrySll 
*  Description   : Function to delete an entry from the list 
*  Parameter(s)  : pNode - Node to be deleted
*  Return Values : None
*********************************************************************/
VOID
SNMPDelPrpProxyEntrySll (tTMO_SLL_NODE * pNode)
{
    TMO_SLL_Delete (&gPrpProxyTableSll, pNode);
    return;
}

/*********************************************************************
*  Function Name : SNMPDelPrpPrxMibIdEntrySll 
*  Description   : Function to delete an Mib Id entry from the list 
*  Parameter(s)  : pNode - Node to be deleted
*  Return Values : None
*********************************************************************/
VOID
SNMPDelPrpPrxMibIdEntrySll (tTMO_SLL_NODE * pNode)
{
    TMO_SLL_Delete (&gPrpPrxMibIdSll, pNode);
    return;
}

/*********************************************************************
*  Function Name : SNMPCompareRootOID
*  Description   : Function to compare the given root OID with the 
*                 : input OID and returns EQUAL if Best Match found
*  Parameter(s)  : pFirstOID - Pointer to First OID
*                  pSecondOID - Pointer to Root OID
*  Return Values : SNMP_TRUE/SNMP_FALSE
*********************************************************************/
INT4
SNMPCompareRootOID (tSNMP_OID_TYPE * pFirstOID, tSNMP_OID_TYPE * pSecondOID)
{
    UINT4               ui4Count = SNMP_ZERO;

    if (pFirstOID->u4_Length < pSecondOID->u4_Length)
    {
        return SNMP_LESSER;
    }

    for (ui4Count = SNMP_ZERO; ui4Count < pSecondOID->u4_Length; ui4Count++)
    {
        if (pFirstOID->pu4_OidList[ui4Count] <
            pSecondOID->pu4_OidList[ui4Count])
        {
            return SNMP_LESSER;
        }
        if (pFirstOID->pu4_OidList[ui4Count] >
            pSecondOID->pu4_OidList[ui4Count])
        {
            return SNMP_GREATER;
        }
    }

    return SNMP_EQUAL;
}

/*********************************************************************
*  Function Name : SNMPIsValidPrpProxyEntry 
*  Description   : Function to check whether is there any valid ACTIVE
*                 : entry in the Prp Proxy Table or not.
*  Parameter(s)  : 
*  Return Values : Success will return the Proxy Name
*                : Failure will return NULL
*********************************************************************/
INT4
SNMPIsValidPrpProxyEntry ()
{
    UINT4               u4Count = SNMP_ZERO;

    u4Count = TMO_SLL_Count (&gPrpPrxMibIdSll);
    if (u4Count > SNMP_ZERO)
    {
        return SNMP_TRUE;
    }

    return SNMP_FALSE;
}
