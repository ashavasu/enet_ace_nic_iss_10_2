/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: snmptcp.h,v 1.1 2015/04/28 12:35:02 siva Exp $
 *
 * Description: common macros and prototypes for snmp over Tcp
 *******************************************************************/
#ifndef _SNMPTCP_H
#define _SNMPTCP_H
/* Macros */
#define SNMPTCP_CLIENT_Q        "STCP"
#define SNMPTCP_SEM_NAME        ((UINT1*)"STCP_SEM")
#define SNMPTCP_MAX_LISTEN      5 
#define SNMPTCP_INVALID_SOCKFD  -1
#define SNMPTCP_RCV_BUF_LEN     1500
 
#define SNMPTCP_ENABLE          0x01
#define SNMPTCP_DISABLE         0x02

#define SNMPTCP_PKT_WRITE       0x01
#define SNMPTCP_PKT_READ        0x02


/* Typedefs */
typedef struct {
    INT4                    i4Cmd;
    INT4                    i4SockFd;
}tSnmpTcpMsg;

typedef struct {
    INT4                    i4TcpSessions;
    INT4                    i4TcpPktsRcvd;
    INT4                    i4TcpPktsSent;
}tSnmpTcpStats;

typedef struct {
    INT4                    i4SockFd;
    INT4                    i4PartialData;
    INT4                    i4DataLen;
    INT4                    i4LenRcvd;
    tIPvXAddr               MgrAddr;
    UINT1                   *pRcvBuf;
    tTMO_SLL                TxList;
}tSnmpTcpSsn;

typedef struct {
    tTMO_SLL_NODE   TxNode;  /* Tx buffer node information */
    UINT1           *pu1Msg;  /* Pointer of the send buffer */
    UINT4           u4MsgSize; /* Message size of the residual buffer */
    UINT4           u4MsgOffset; /* Residual buffer message offset.*/
} tSnmpTcpTxNode;

typedef struct {
    tSnmpTcpSsn            *apSnmpTcpSsn[MAX_SNMP_TCP_CLIENTS_LIMIT];
    tMemPoolId              TcpSsnPoolId;
    tMemPoolId              TcpCntrlPoolId;
    tMemPoolId              TcpSsnRcvBufPoolId;
    tOsixQId                SnmpTcpQId;
    tSnmpTcpStats           SnmpTcpStats;   
    UINT2                   u2TcpPort;
    UINT2                   u2TcpTrapPort;
    INT4                    i4TcpV4SockId; 
    INT4                    i4TcpV6SockId; 
    INT4                    i4TcpTrapV4SockId; 
    INT4                    i4TcpTrapV6SockId; 
    INT4                    i4TcpStatus;
    INT4                    i4TcpTrapStatus;
    UINT4                   u4TcpTotalSsns;
}tSnmpTcp;

/* Prototypes */

INT4 Snmp3SetTcpStatus PROTO((INT4 i4TcpStatus));
VOID SnmpTcpHandleNewConnEvt PROTO((UINT4 u4Events));
INT4 SnmpAgentTcpInit PROTO((VOID));
VOID SnmpTcpForceCloseAllSsn PROTO((VOID));
VOID SnmpTcpForceCloseSsn PROTO((tSnmpTcpSsn*));
tSnmpTcpSsn* SnmpTcpAllocSsnNode PROTO((VOID));
VOID SnmpTcpFreeSsnNode PROTO((tSnmpTcpSsn *pTcpSsnNode));
INT4 SnmpTcpInitAndAddSsn PROTO((tSnmpTcpSsn *pTcpSsnNode));
tSnmpTcpSsn* SnmpTcpFindSsnNode PROTO((INT4 i4SockFd));
VOID SnmpTcpHandleRdWrEvt PROTO((VOID));
INT4 SnmpTcpProcessReadEvt PROTO((INT4 i4SockFd));
INT4 SnmpTcpProcessWriteEvt PROTO((INT4 i4SockFd));
INT4 SnmpTcpProcessRcvdPkt PROTO((INT4 i4SockId, INT4 i4Version, 
                                  UINT1 *pu1RcvPkt, INT4 i4Len));
INT4 SnmpTcpSockInit PROTO((UINT2 u2Port));
INT4 SnmpTcpSock6Init PROTO((UINT2 u2Port));
INT4 SnmpTcpAcceptNewConnection PROTO((INT4 i4SrvSockFd, INT4 *pi4CliSockFd, 
                                       tIPvXAddr *pClientAddr));
VOID SnmpTcpPktRcvd PROTO((INT4 i4SockFd));
INT4 SnmpTcpSockRcv PROTO((INT4 i4SockFd, UINT1 *pu1Data, UINT2 u2Len, 
                           INT4 *pi4RdBytes));
VOID SnmpTcpV4NewConnCallBk PROTO((INT4 i4SockFd));
VOID SnmpTcpV6NewConnCallBk PROTO((INT4 i4SockFd));
VOID SnmpTcpGetLenAndVersion PROTO((UINT1 *pu1Pkt, INT4 i4PktSize, INT4 *pi4Len,                                   INT4 *pi4Version));
INT4 SnmpTcpSockSend PROTO((INT4 i4SockFd, UINT1 *pu1Data, UINT2 u2PktLen, 
                        INT4 *pi4SentLen));

INT4 SnmpTcpAddPktInTxList PROTO((INT4 i4SockFd, UINT1* pu1Buf, UINT4 u4Size));
VOID SnmpTcpWriteCallBackFn PROTO((INT4 i4SockFd));
INT4 SnmpTcpRemovePktInTxList PROTO((tSnmpTcpTxNode *pNode, INT4 i4SockFd));
INT4 SnmpTcpGetTxListCnt PROTO((INT4 i4SockFd));

/*Export from snmpcode.h*/
extern UINT1 gau1RevDat[];
#endif

