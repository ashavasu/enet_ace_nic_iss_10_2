/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: snmptrap.c,v 1.28 2017/06/09 12:50:08 siva Exp $
 *
 *
 * Description:Routines for SNMP trap handling 
 *******************************************************************/
#ifndef SNMPTRAP_C
#define SNMPTRAP_C
#include "snmpcmn.h"
#include "snmputil.h"
#include "snmptrap.h"
#include "snprxtrn.h"
#include "vcm.h"
#include "utilrand.h"
#include "stdsnmlw.h"
UINT1
     
                gau1NotifyName[MAX_TRAP_TABLE_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
UINT1
     
                gau1NotifyTag[MAX_TRAP_TABLE_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
UINT1
     
     
     
     
     
     
      gau1NotifyFilterName[SNMP_MAX_TGT_PARAM_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
UINT1
     
     
     
     
     
     
     
    gau1FilterTargetParamName[SNMP_MAX_TGT_PARAM_ENTRY]
    [SNMP_MAX_OCTETSTRING_SIZE];
UINT1
     
                gau1FilterMask[SNMP_MAX_FILTER_TREE][SNMP_MAX_OCTETSTRING_SIZE];
UINT4               gau4FilterSubOid[SNMP_MAX_FILTER_TREE][MAX_OID_LENGTH];
extern UINT1       *EoidGetEnterpriseOid (void);

/************************************************************************
 *  Function Name   : SNMP_Context_Notify_Trap 
 *  Description     : Called from protocol for trap with protocool context name
 *  Input           : pEnterpriseOid - Enterprise Oid Pinter
 *                    u4GenTrapType - Generic Trap Type
 *                    u4SpecTrapType - Specific Trap Type
 *                    pVbList - Pointer to Varbind List                   
 *                    pContext - Context Name pointer
 *  Output          : None 
 *  Returns         : None
 ************************************************************************/
VOID
SNMP_Context_Notify_Trap (tSNMP_OID_TYPE * pEnterpriseOid, UINT4 u4GenTrapType,
                          UINT4 u4SpecTrapType, tSNMP_VAR_BIND * pVbList,
                          tSNMP_OCTET_STRING_TYPE * pContext)
{
    /* If the agentx subagent is enabled, send trap to master
     * agent */
    if (gi4SnmpAgentxSubAgtStatus == SNMP_AGENTX_ENABLE)
    {
        SnxMainGenerateTrap (pVbList, pEnterpriseOid, u4GenTrapType,
                             u4SpecTrapType);
        return;
    }
    SNMP_Notify_Format_Send (pEnterpriseOid, u4GenTrapType, u4SpecTrapType,
                             pVbList, NULL, pContext);
}

/************************************************************************
 *  Function Name   : SNMP_AGT_RIF_Notify_Trap 
 *  Description     : Called from protocol for v1 agent
 *  Input           : pEnterpriseOid - Enterprise Oid Pinter
 *                    u4GenTrapType - Generic Trap Type
 *                    u4SpecTrapType - Specific Trap Type
 *                    pVbList - Pointer to Varbind List                    
 *  Output          : None 
 *  Returns         : None
 ************************************************************************/

VOID
SNMP_AGT_RIF_Notify_Trap (tSNMP_OID_TYPE * pEnterpriseOid, UINT4 u4GenTrapType,
                          UINT4 u4SpecTrapType, tSNMP_VAR_BIND * pVbList)
{
    if (gi4SnmpLaunch == SNMP_SUCCESS)
    {
        if (SnmpMatchTrapForLastChangeTime (pEnterpriseOid, u4SpecTrapType) ==
            SNMP_SUCCESS)
        {
            SnmpUpdateLastTimeChange ();
        }
        /* If the agentx subagent is enabled, send trap to master
         * agent */
        if (gi4SnmpAgentxSubAgtStatus == SNMP_AGENTX_ENABLE)
        {
            SnxMainGenerateTrap (pVbList, pEnterpriseOid, u4GenTrapType,
                                 u4SpecTrapType);
            return;
        }
        SNMP_Notify_Format_Send (pEnterpriseOid, u4GenTrapType, u4SpecTrapType,
                                 pVbList, NULL, NULL);
    }
}

/************************************************************************
 *  Function Name   : SNMP_Notify_Trap 
 *  Description     : Called from SNMP_AGT_RIF_Notify_Trap 
 *  Input           : pEnterpriseOid - Enterprise Oid Pinter
 *                    u4GenTrapType - Generic Trap Type
 *                    u4SpecTrapType - Specific Trap Type
 *                    pVbList - Pointer to Varbind List                    
 *                    pEventCommunity - Pointer to Event Community name 
 *  Output          : None 
 *  Returns         : None
 ************************************************************************/
VOID
SNMP_Notify_Trap (tSNMP_OID_TYPE * pEnterpriseOid, UINT4 u4GenTrapType,
                  UINT4 u4SpecTrapType, tSNMP_VAR_BIND * pVbList,
                  tSNMP_OCTET_STRING_TYPE * pEventCommunity)
{

    /* If the agentx subagent is enabled, send trap to master
     * agent */
    if (gi4SnmpAgentxSubAgtStatus == SNMP_AGENTX_ENABLE)
    {
        SnxMainGenerateTrap (pVbList, pEnterpriseOid, u4GenTrapType,
                             u4SpecTrapType);
        return;
    }
    SNMP_Notify_Format_Send (pEnterpriseOid, u4GenTrapType, u4SpecTrapType,
                             pVbList, pEventCommunity, NULL);
}

/************************************************************************
 *  Function Name   : SNMP_Notify_Format_Send 
 *  Description     : Called from SNMP_AGT_RIF_Notify_Trap 
 *  Input           : pEnterpriseOid - Enterprise Oid Pinter
 *                    u4GenTrapType - Generic Trap Type
 *                    u4SpecTrapType - Specific Trap Type
 *                    pVbList - Pointer to Varbind List                    
 *                    pEventCommunity - Pointer to Event Community name
 *                    pContextName - Protocol Context Name Pointer
 *  Output          : None 
 *  Returns         : None
 ************************************************************************/
VOID
SNMP_Notify_Format_Send (tSNMP_OID_TYPE * pEnterpriseOid, UINT4 u4GenTrapType,
                         UINT4 u4SpecTrapType, tSNMP_VAR_BIND * pVbList,
                         tSNMP_OCTET_STRING_TYPE * pEventCommunity,
                         tSNMP_OCTET_STRING_TYPE * pContextName)
{
    tSNMP_TRAP_PDU     *pV1TrapPdu = NULL;
    tSNMP_NORMAL_PDU   *pV2TrapPdu = NULL;
    tSNMP_VAR_BIND     *pTempVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;

    SNMP_INR_IN_TRAP_SUCCESS;
    if (pVbList == NULL)
    {
        SNMPTrace ("The input VbList is NULL\n");
        return;
    }
    pTempVb = pVbList;
    while (pTempVb != NULL)
    {
        if (pTempVb->ObjValue.i2_DataType == SNMP_DATA_TYPE_COUNTER64)
        {
            SNMPTrace ("v1 Trap variable bind List with Couter64 object\n");
            FREE_OID (pEnterpriseOid);
            FREE_SNMP_VARBIND_LIST (pVbList);
            return;
        }
        pTempVb = pTempVb->pNextVarBind;
    }
    pV1TrapPdu = MemAllocMemBlk (gSnmpTrapPduPoolId);
    if (pV1TrapPdu == NULL)
    {
        SNMPTrace ("Unable to Allocate V1 Trap \n");
        FREE_OID (pEnterpriseOid);
        FREE_SNMP_VARBIND_LIST (pVbList);
        return;
    }
    MEMSET (pV1TrapPdu, SNMP_ZERO, sizeof (tSNMP_TRAP_PDU));
    pV1TrapPdu->i4_GenericTrap = (INT4) u4GenTrapType;
    pV1TrapPdu->i4_SpecificTrap = (INT4) u4SpecTrapType;

    /* Enterprise is Updated and input Enterprise OID is freed */
    pV1TrapPdu->pEnterprise = alloc_oid (MAX_OID_LENGTH);
    if (pV1TrapPdu->pEnterprise == NULL)
    {
        SNMPTrace ("Unable to Allocate Enterprise for Trap Pdu\n");
        FREE_OID (pEnterpriseOid);
        FREE_SNMP_VARBIND_LIST (pVbList);
        SNMPFreeV1Trap (pV1TrapPdu);
        return;
    }
    pOid = alloc_oid (MAX_OID_LENGTH);

    if (pOid == NULL)
    {
        SNMPTrace ("Unable to Allocate Enterprise for Trap PDU\n");
        FREE_OID (pEnterpriseOid);
        FREE_SNMP_VARBIND_LIST (pVbList);
        SNMPFreeV1Trap (pV1TrapPdu);
        return;
    }

    if (u4GenTrapType != ENTERPRISE_SPECIFIC)
    {
        MEMCPY (pOid->pu4_OidList, gau4snmpOid, SNMP_OID_LEN * sizeof (UINT4));
        pOid->u4_Length = SNMP_OID_LEN;
    }
    else
    {
        if (pEnterpriseOid == NULL)
        {
            SNMPTrace ("Given Enterprise OID is NULL\n");
            FREE_SNMP_VARBIND_LIST (pVbList);
            SNMPFreeV1Trap (pV1TrapPdu);
            FREE_OID (pOid);
            SNMP_INR_OUT_GEN_ERROR;
            return;
        }
        SNMPOIDCopy (pOid, pEnterpriseOid);
    }
    SNMPRevertEOID (pOid, pV1TrapPdu->pEnterprise);
    FREE_OID (pOid);
    FREE_OID (pEnterpriseOid);

    /* Agent IP Address is Updated from System Type */
    pV1TrapPdu->pAgentAddr = allocmem_octetstring (sizeof (UINT4));
    if (pV1TrapPdu->pAgentAddr == NULL)
    {
        SNMPTrace ("Unable to allocate agent ip address for v1 trap\n");
        FREE_SNMP_VARBIND_LIST (pVbList);
        SNMPFreeV1Trap (pV1TrapPdu);
        return;
    }
    pV1TrapPdu->pAgentAddr->i4_Length = sizeof (UINT4);

    /* Time Stamp is Updated */
    GET_TIME_TICKS (&(pV1TrapPdu->u4_TimeStamp));

    /* Pdu Type is updated as Trap */
    pV1TrapPdu->i2_PduType = SNMP_PDU_TYPE_TRAP;

    /* Varbind list is updated with the input varbind list */
    pV1TrapPdu->pVarBindList = pVbList;
    pV1TrapPdu->pVarBindEnd = pVbList;
    while (pVbList->pNextVarBind != NULL)
    {
        pV1TrapPdu->pVarBindEnd = pVbList;
        pVbList = pVbList->pNextVarBind;
    }
    pV1TrapPdu->pCommunityStr =
        allocmem_octetstring (SNMP_MAX_OCTETSTRING_SIZE);
    if (pV1TrapPdu->pCommunityStr == NULL)
    {
        SNMPTrace ("Unable to allocate Community for v1 trap \n");
        SNMPFreeV1Trap (pV1TrapPdu);
        return;
    }
    if (pEventCommunity != NULL)
    {
        SNMPCopyOctetString (pV1TrapPdu->pCommunityStr, pEventCommunity);
    }
    pV2TrapPdu = SNMPV1TrapToV2Trap (pV1TrapPdu, NULL, 0, SNMP_FALSE);
    if (pV2TrapPdu == NULL)
    {
        SNMPTrace ("Unable to convert v1 to v2 trap \n");
        SNMPFreeV1Trap (pV1TrapPdu);
        return;
    }
    SNMPAgentSendTrap (pV2TrapPdu, pV1TrapPdu, NULL, NULL, pEventCommunity,
                       pContextName);
    SNMPFreeV1Trap (pV1TrapPdu);
    SNMPFreeV2Trap (pV2TrapPdu);
}

/************************************************************************
 *  Function Name   : SNMP_AGT_RIF_Notify_v2Trap 
 *  Description     : Function to send v2 trap from protocol
 *  Input           : pVbList - Variable Bind List Pointer
 *  Output          : None
 *  Returns         : None
 ************************************************************************/

VOID
SNMP_AGT_RIF_Notify_v2Trap (tSNMP_VAR_BIND * pVbList)
{
    tSNMP_NORMAL_PDU   *pV2TrapPdu = NULL;
    tSNMP_TRAP_PDU     *pV1TrapPdu = NULL;
    tSNMP_VAR_BIND     *pV2VbList = NULL;
    tSNMP_VAR_BIND     *pFilterVarPtr = NULL;
    INT4                i4Result = SNMP_SUCCESS;

    SNMP_INR_IN_TRAP_SUCCESS;

    if (pVbList == NULL)
    {
        SNMPTrace ("The input VbList is NULL\n");
        return;
    }
    pV2TrapPdu = MemAllocMemBlk (gSnmpNormalPduPoolId);
    if (pV2TrapPdu == NULL)
    {
        SNMPTrace ("Unable to allocate v2 Trap \n");
        FREE_SNMP_VARBIND_LIST (pVbList);
        return;
    }
    MEMSET (pV2TrapPdu, SNMP_ZERO, sizeof (tSNMP_NORMAL_PDU));
    if (MEMCMP (gau4snmpTrapOid, pVbList->pObjName->pu4_OidList,
                SNMP_TRAP_OID_LEN * 4) != SNMP_ZERO)
    {
        MemReleaseMemBlock (gSnmpNormalPduPoolId, (UINT1 *) pV2TrapPdu);
        FREE_SNMP_VARBIND_LIST (pVbList);
        SNMPTrace
            ("The First OID of the input VbList is not the SnmpTrapOid\n");
        return;
    }
    pV2TrapPdu->u4_RequestID = SNMPGetRequestID ();
    pV2TrapPdu->i4_ErrorStatus = SNMP_ERR_NO_ERROR;
    pV2TrapPdu->i4_ErrorIndex = SNMP_ZERO;
    pV2TrapPdu->i2_PduType = SNMP_PDU_TYPE_SNMPV2_TRAP;
    pV2TrapPdu->u4_Version = VERSION2;

    /* Update System Up Time as a first OID */
    pV2VbList = SNMPGetSysUpTime ();
    if (pV2VbList == NULL)
    {
        MemReleaseMemBlock (gSnmpNormalPduPoolId, (UINT1 *) pV2TrapPdu);
        FREE_SNMP_VARBIND_LIST (pVbList);
        SNMPTrace ("Failure while getting SysUpTime\n");
        return;
    }
    pV2VbList->pNextVarBind = pVbList;

    /* Varbind list is updated with the input varbind list */
    pV2TrapPdu->pVarBindList = pV2VbList;
    pV2TrapPdu->pVarBindEnd = pVbList;
    while (pVbList->pNextVarBind != NULL)
    {
        pV2TrapPdu->pVarBindEnd = pVbList->pNextVarBind;
        pVbList = pVbList->pNextVarBind;
    }
    pV2TrapPdu->pCommunityStr =
        allocmem_octetstring (SNMP_MAX_OCTETSTRING_SIZE);
    if (pV2TrapPdu->pCommunityStr == NULL)
    {
        SNMPTrace ("Unable to allocate Community for v2 trap\n");
        SNMPFreeV2Trap (pV2TrapPdu);
        return;
    }
    pV1TrapPdu = SNMPV2TrapToV1Trap (pV2TrapPdu);
    if (pV1TrapPdu == NULL)
    {
        SNMPTrace ("Unable to convert v2 trap to v1 \n");
        SNMPFreeV2Trap (pV2TrapPdu);
        return;
    }
    if (pV2TrapPdu->pVarBindList != NULL)
    {
        pFilterVarPtr = pV2TrapPdu->pVarBindList->pNextVarBind;
    }
    while (pFilterVarPtr != NULL)
    {

        i4Result = SNMPTrapCheckTrapAllowed (pFilterVarPtr->pObjName);
        if (i4Result == SNMP_FAILURE)
        {
            break;
        }
        pFilterVarPtr = pFilterVarPtr->pNextVarBind;
    }
    if (i4Result == SNMP_SUCCESS)
    {
        SNMPAgentSendTrap (pV2TrapPdu, pV1TrapPdu, NULL, NULL, NULL, NULL);
    }
    SNMPFreeV2Trap (pV2TrapPdu);
    SNMPFreeV1Trap (pV1TrapPdu);
}

/************************************************************************
 *  Function Name   : SNMPAgentSendTrap 
 *  Description     : Function to send v1 or v2 trap depending upon
 *                    Manager Type
 *  Input           : pV2TrapPdu - v2 Trap Pointer
 *                    pV1TrapPdu - v1 Trap Pointer
 *                    pEventCommunity - Pointer to Event Community name 
 *                    AppCallback - Callback function provided by
 *                                  application
 *                    pCookie     - pointer to memory in application
 *                                  that has information related to
 *                                  the notification being generated
 *                    pContextName - Context Information Pointer              
 *  Output          : None
 *  Returns         : None
 ************************************************************************/

VOID
SNMPAgentSendTrap (tSNMP_NORMAL_PDU * pV2TrapPdu, tSNMP_TRAP_PDU * pV1TrapPdu,
                   SnmpAppNotifCb AppCallback, VOID *pCookie,
                   tSNMP_OCTET_STRING_TYPE * pEventCommunity,
                   tSNMP_OCTET_STRING_TYPE * pContextName)
{
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
    UINT4               u4AgentAddr = SNMP_ZERO;
#endif
    UINT4               u4ManagerAddr = SNMP_ZERO, u4Err = SNMP_ZERO;
    UINT1              *pu1Msg = NULL;
    INT4                i4MsgLen = 0, i4Result = SNMP_SUCCESS;
    INT4                i4PrevReqId = 0;
    INT4                i4NotifyType = 0;
    tSNMP_V3PDU        *pV3Pdu = NULL;
    tSnmpNotifyEntry   *pCurrEntry = NULL;
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    tCommunityMappingEntry *pCommunityEntry = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tSnmpVacmInfo       VacmInfo;
    tSNMP_VAR_BIND     *pFilterVarPtr = NULL;
    tSNMP_VAR_BIND     *pFilterV1VarPtr = NULL;
    tSNMP_VAR_BIND     *pVarPtr = NULL;
    tSNMP_OID_TYPE     *pEntOid = NULL;
#ifdef IP6_WANTED
    tSNMP_OCTET_STRING_TYPE *pIp6ManagerAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pIp6AgentAddr = NULL;
#endif
    INT1                i4RetVal = SNMP_FAILURE;
    UINT1               au1TempName[DNS_MAX_QUERY_LEN];
    tDNSResolvedIpInfo  ResolvedIpInfo;

    MEMSET (&ResolvedIpInfo, SNMP_ZERO, sizeof (tDNSResolvedIpInfo));
    MEMSET (au1TempName, SNMP_ZERO, DNS_MAX_QUERY_LEN);
    MEMSET (&VacmInfo, SNMP_ZERO, sizeof (tSnmpVacmInfo));

    if (gu4SnmpAgentControl == SNMP_AGENT_DISABLE)
    {
        return;
    }

    TMO_SLL_Scan (&gSnmpNotifySll, pLstNode, tTMO_SLL_NODE *)
    {
        pCurrEntry = (tSnmpNotifyEntry *) pLstNode;
        if (pCurrEntry->i4Status != ACTIVE)
        {
            continue;
        }
        if (pEventCommunity != NULL)
        {
            if (SNMPCompareOctetString (pEventCommunity,
                                        &pCurrEntry->NotifyName) != SNMP_EQUAL)
            {
                continue;
            }
        }
        pSnmpTgtAddrEntry = NULL;
        while ((pSnmpTgtAddrEntry =
                SNMPGetTgtAddrTagEntry (&(pCurrEntry->NotifyTag),
                                        pSnmpTgtAddrEntry)) != NULL)
        {
            if (pSnmpTgtAddrEntry->i4Status != ACTIVE)
            {
                continue;
            }
            pFilterVarPtr = pV2TrapPdu->pVarBindList->pNextVarBind;
            pFilterV1VarPtr = pV1TrapPdu->pVarBindList->pNextVarBind;

            pSnmpTgtParamEntry =
                SNMPGetTgtParamEntry (&(pSnmpTgtAddrEntry->AddrParams));
            if (pSnmpTgtParamEntry == NULL)
            {
                SNMPTrace (" SNMP Target Param is NULL \n");
                continue;
            }

            if (pSnmpTgtAddrEntry->b1HostName == SNMP_TRUE)
            {
                MEMSET (au1TempName, SNMP_ZERO, DNS_MAX_QUERY_LEN);
                i4RetVal = (INT1) STRLEN (pSnmpTgtAddrEntry->au1Hostname);
                STRNCPY (au1TempName, pSnmpTgtAddrEntry->au1Hostname, i4RetVal);
                i4RetVal = SNMP_FAILURE;
                i4RetVal =
                    (INT1) FsUtlIPvXResolveHostName (au1TempName, DNS_NONBLOCK,
                                                     &ResolvedIpInfo);
                if (i4RetVal == DNS_NOT_RESOLVED)
                {
                    SNMPTrace ("\n Cannot resolve the host name \n");
                    continue;
                    /*not resolved */
                }
                else if (i4RetVal == DNS_IN_PROGRESS)
                {
                    SNMPTrace ("\n Host name resolution in Progress \n");
                    continue;
                    /*in progress */
                }
                else if (i4RetVal == DNS_CACHE_FULL)
                {
                    SNMPTrace
                        ("\nDNS cache full, cannot resolve at the moment");
                    continue;
                    /*cache full */
                }

                /* If the hostname resolves to both IPv4 and IPv6
                 *              * address, IPv6 address is considered */
                if ((ResolvedIpInfo.Resolv6Addr.u1AddrLen != 0) &&
                    (i4RetVal == DNS_RESOLVED))
                {
                    pSnmpTgtAddrEntry->Address.i4_Length = SNMP_IP6ADDR_LEN;
                    MEMCPY (pSnmpTgtAddrEntry->Address.pu1_OctetList,
                            ResolvedIpInfo.Resolv6Addr.au1Addr,
                            IPVX_IPV6_ADDR_LEN);
                }
                else if ((ResolvedIpInfo.Resolv4Addr.u1AddrLen != 0) &&
                         (i4RetVal == DNS_RESOLVED))
                {

                    MEMCPY (pSnmpTgtAddrEntry->Address.pu1_OctetList,
                            ResolvedIpInfo.Resolv4Addr.au1Addr,
                            IPVX_IPV4_ADDR_LEN);
                    pSnmpTgtAddrEntry->Address.i4_Length = SNMP_IPADDR_LEN;
                    pSnmpTgtAddrEntry->u2Port = SNMP_MANAGER_PORT;

                }

            }

            if (pSnmpTgtAddrEntry->Address.i4_Length == SNMP_IPADDR_LEN)
            {
                MEMCPY (&u4ManagerAddr,
                        pSnmpTgtAddrEntry->Address.pu1_OctetList,
                        SNMP_IPADDR_LEN);
                u4ManagerAddr = OSIX_NTOHL (u4ManagerAddr);
            }
            else if (pSnmpTgtAddrEntry->Address.i4_Length == SNMP_IP6ADDR_LEN)
            {
#ifdef IP6_WANTED
                /*Allocate octet string memory during first iteration.
                 * Next iteration onwards re-initialize with 
                 * current pSnmpTgtAddrEntry->Address */

                if (pIp6ManagerAddr == NULL)
                {
                    if ((pIp6ManagerAddr =
                         allocmem_octetstring (SNMP_MAX_OCTETSTRING_SIZE)) ==
                        NULL)
                    {
                        SNMPTrace ("Memory allocation failed\n");
                        continue;
                    }
                }
                MEMSET (pIp6ManagerAddr->pu1_OctetList, 0,
                        SNMP_MAX_OCTETSTRING_SIZE);
                SNMPCopyOctetString (pIp6ManagerAddr,
                                     &(pSnmpTgtAddrEntry->Address));
#endif
            }

            if (pCurrEntry->i4NotifyType == SNMP_TRAP_TYPE)
            {
                i4NotifyType = SNMP_PDU_TYPE_SNMPV2_TRAP;
            }
            else
            {
                i4NotifyType = SNMP_PDU_TYPE_V2_INFORM_REQUEST;
            }

            /* Check any filter is installed for the Target Address */

            if (pSnmpTgtParamEntry->i2ParamMPModel == SNMP_MPMODEL_V1)
            {
                while (pFilterV1VarPtr != NULL)
                {
                    i4Result =
                        SNMPNotifyFilterAccessAllowed
                        ((pSnmpTgtParamEntry->pFilterProfileTable),
                         pFilterV1VarPtr->pObjName);
                    if (i4Result == SNMP_FAILURE)
                    {
                        break;
                    }
                    pFilterV1VarPtr = pFilterV1VarPtr->pNextVarBind;
                }
                if (i4Result == SNMP_FAILURE)
                {

                    /* since filter validation is failed,dont sent the trap
                       to manager */
                    continue;
                }
            }
            else
            {
                while (pFilterVarPtr != NULL)
                {
                    i4Result =
                        SNMPNotifyFilterAccessAllowed
                        ((pSnmpTgtParamEntry->pFilterProfileTable),
                         pFilterVarPtr->pObjName);
                    if (i4Result == SNMP_FAILURE)
                    {
                        break;
                    }
                    pFilterVarPtr = pFilterVarPtr->pNextVarBind;
                }
                if (i4Result == SNMP_FAILURE)
                {

                    /* since filter validation is failed,dont sent the trap
                       to manager */
                    continue;
                }

            }

            if (MEMCMP (EoidGetEnterpriseOid (), "2076", STRLEN ("2076")) != 0)
            {
                if ((pEntOid = alloc_oid (SNMP_MAX_OID_LENGTH)) == NULL)
                {
                    SNMPTrace ("Enterprise OID Memory allocation failed\n");
                    continue;
                }

                if (pSnmpTgtParamEntry->i2ParamMPModel == SNMP_MPMODEL_V1)
                {
                    pVarPtr = pV1TrapPdu->pVarBindList->pNextVarBind;
                }
                else
                {
                    pVarPtr = pV2TrapPdu->pVarBindList->pNextVarBind;
                }
                while (pVarPtr != NULL)
                {
                    SNMPRevertEOID (pVarPtr->pObjName, pEntOid);
                    SNMPCopyOid (pVarPtr->pObjName, pEntOid);
                    pVarPtr = pVarPtr->pNextVarBind;
                }
                free_oid (pEntOid);
            }

            if (pSnmpTgtParamEntry->i2ParamMPModel == SNMP_MPMODEL_V1)
            {
                /* RFC 2576 - Section 5.2.3: The securityName must be equal 
                 * to the snmpCommunitySecurityName value*/
                pCommunityEntry =
                    SNMPGetCommunityEntryFromSecName
                    (&pSnmpTgtParamEntry->ParamSecName);
                if (pCommunityEntry == NULL)
                {
                    continue;
                }
                SNMPCopyOctetString (pV1TrapPdu->pCommunityStr,
                                     &pCommunityEntry->CommunityName);

                if (pSnmpTgtAddrEntry->Address.i4_Length == SNMP_IPADDR_LEN)
                {
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
                    /* Get the source address of the agent entity depending upon
                     * Manager Address. This function to be ported if FutureIP
                     * is not used */
                    if ((NetIpv4GetSrcAddrToUseForDest
                         (u4ManagerAddr, &u4AgentAddr)) == NETIPV4_FAILURE)
                    {
#ifdef IP6_WANTED
                        if (pIp6ManagerAddr != NULL)
                        {
                            free_octetstring (pIp6ManagerAddr);
                            pIp6ManagerAddr = NULL;
                        }
#endif
                        return;
                    }
                    u4AgentAddr = OSIX_HTONL (u4AgentAddr);
                    MEMCPY (pV1TrapPdu->pAgentAddr->pu1_OctetList,
                            (UINT1 *) &u4AgentAddr, sizeof (UINT4));
#endif
                }
                else if (pSnmpTgtAddrEntry->Address.i4_Length ==
                         SNMP_IP6ADDR_LEN)
                {
#ifdef IP6_WANTED
                    if ((pIp6AgentAddr =
                         allocmem_octetstring (SNMP_IP6ADDR_LEN)) == NULL)
                    {
                        SNMPTrace ("Memory allocation failed\n");
                        free_octetstring (pIp6ManagerAddr);
                        pIp6ManagerAddr = NULL;
                        continue;
                    }
                    else
                    {
                        NetIpv6GetSrcAddrForDestAddr (VCM_DEFAULT_CONTEXT,
                                                      (tIp6Addr *) (VOID *)
                                                      pIp6ManagerAddr->
                                                      pu1_OctetList,
                                                      (tIp6Addr *) (VOID *)
                                                      pIp6AgentAddr->
                                                      pu1_OctetList);
                        SNMPCopyOctetString (pV1TrapPdu->pAgentAddr,
                                             pIp6AgentAddr);
                        free_octetstring (pIp6AgentAddr);
                    }
#endif
                }

                SNMPEncodeV1TrapPacket (pV1TrapPdu, &pu1Msg, &i4MsgLen);
            }

            else if (pSnmpTgtParamEntry->i2ParamMPModel == SNMP_MPMODEL_V2C)
            {
                /* RFC 2576 - Section 5.2.3: The securityName must be equal 
                 * to the snmpCommunitySecurityName value*/
                pCommunityEntry =
                    SNMPGetCommunityEntryFromSecName
                    (&pSnmpTgtParamEntry->ParamSecName);
                if (pCommunityEntry == NULL)
                {
                    continue;
                }
                SNMPCopyOctetString (pV2TrapPdu->pCommunityStr,
                                     &pCommunityEntry->CommunityName);
                SNMPEncodeV2TrapPacket (pV2TrapPdu, &pu1Msg, &i4MsgLen,
                                        i4NotifyType);
            }
            else if (pSnmpTgtParamEntry->i2ParamMPModel == SNMP_MPMODEL_V3)
            {
                pSnmpUsmEntry = SNMPGetUsmEntry (&gSnmpEngineID,
                                                 &pSnmpTgtParamEntry->
                                                 ParamSecName);
                if (pSnmpUsmEntry == NULL)
                {
                    continue;
                }
                pV3Pdu = SNMPFillV3Params (pSnmpUsmEntry, pContextName);

                if (pV3Pdu != NULL)
                {
                    pV3Pdu->u4MsgSecModel =
                        (UINT4) pSnmpTgtParamEntry->i2ParamSecModel;
                    MEMCPY (&pV3Pdu->Normalpdu, pV2TrapPdu,
                            sizeof (tSNMP_NORMAL_PDU));
                    VacmInfo.pUserName = &(pV3Pdu->MsgSecParam.MsgUsrName);
                    VacmInfo.pContextName = &(pV3Pdu->ContextName);
                    VacmInfo.u4SecModel = pV3Pdu->u4MsgSecModel;
                    if (pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO] == 0x01)
                    {
                        VacmInfo.u4SecLevel = SNMP_AUTH_NOPRIV;
                    }
                    else if (pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO] == 0x03)
                    {
                        VacmInfo.u4SecLevel = SNMP_AUTH_PRIV;
                    }
                    else
                    {
                        VacmInfo.u4SecLevel = SNMP_NOAUTH_NOPRIV;
                    }
                    pVarPtr = pV3Pdu->Normalpdu.pVarBindList;
                    while (pVarPtr != NULL)
                    {
                        i4Result =
                            SNMPVACMIsAccessAllowed (&VacmInfo,
                                                     SNMP_ACCESSIBLEFORNOTIFY,
                                                     pVarPtr->pObjName, &u4Err);
                        if (i4Result == SNMP_FAILURE)
                        {
                            break;
                        }
                        pVarPtr = pVarPtr->pNextVarBind;
                    }
                    if (i4Result == SNMP_SUCCESS)
                    {
                        SNMPEncodeV3Message (pV3Pdu, &pu1Msg, &i4MsgLen,
                                             i4NotifyType);
                    }
                    SNMPFreeV3Params (pV3Pdu);
                }
            }
            else
            {
                continue;
            }
            if (pu1Msg != NULL)
            {
                if (pSnmpTgtAddrEntry->Address.i4_Length == SNMP_IP6ADDR_LEN)
                {
#ifdef IP6_WANTED
                    SNMPSendTrap6ToManager (pu1Msg, i4MsgLen, pIp6ManagerAddr);
                    free_octetstring (pIp6ManagerAddr);
                    pIp6ManagerAddr = NULL;
#endif
                }
                else if (pSnmpTgtAddrEntry->Address.i4_Length ==
                         SNMP_IPADDR_LEN)
                {
                    if (pCurrEntry->i4NotifyType == SNMP_TRAP_TYPE)
                    {
                        SNMPSendTrapToManager (pu1Msg, i4MsgLen,
                                               u4ManagerAddr,
                                               pSnmpTgtAddrEntry->u2Port);
                    }
                    else
                    {
                        SNMP_INFORM_LOCK ();
                        SNMPSendInformToManager (pu1Msg, i4MsgLen,
                                                 pSnmpTgtAddrEntry,
                                                 pCurrEntry,
                                                 pV2TrapPdu->u4_RequestID,
                                                 (UINT4) i4PrevReqId,
                                                 SNMP_FIRST_TRANSMIT,
                                                 pV2TrapPdu,
                                                 AppCallback, pCookie);
                        SNMP_INFORM_UNLOCK ();
                    }
                }

                MemReleaseMemBlock (gSnmpPktPoolId, (UINT1 *) pu1Msg);
                pu1Msg = NULL;

                continue;
            }
        }
    }
#ifdef IP6_WANTED
    if (pIp6ManagerAddr != NULL)
    {
        free_octetstring (pIp6ManagerAddr);
        pIp6ManagerAddr = NULL;
    }
#endif

}

/************************************************************************
 *  Function Name   : SNMPV1TrapToV2Trap 
 *  Description     : Function to convert v1 trap to v2 trap
 *  Input           : pV1TrapMsg - v1 Trap Pointer
 *  Output          : None
 *  Returns         : Pointer to v2 trap Else NULL
 ************************************************************************/

tSNMP_NORMAL_PDU   *
SNMPV1TrapToV2Trap (tSNMP_TRAP_PDU * pV1TrapMsg,
                    tSNMP_OCTET_STRING_TYPE * pSingleTargetOut,
                    UINT4 RequestID, UINT1 IsProxyFlag)
{
    tSNMP_NORMAL_PDU   *pV2TrapMsg = NULL;
    tSNMP_VAR_BIND     *pV2VbList = NULL, *pSnmpTrapVb = NULL;
    tSNMP_VAR_BIND     *pV1TrapVbList = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOidValue = NULL, *pSnmpStdTrapOid = NULL;
    tSNMP_OID_TYPE     *pEnterpriseTrapOid = NULL, *pEnterpriseOidValue = NULL;
    tSNMP_OID_TYPE     *pTrapAddressOid = NULL;
    tSNMP_OID_TYPE     *pTrapCommunityOid = NULL;
    tSNMP_OCTET_STRING_TYPE *pTrapAddressOidValue = NULL;
    tSNMP_OCTET_STRING_TYPE *pTrapCommunityOidValue = NULL;
    tSNMP_VAR_BIND     *pCopiedV1TrapVbList = NULL;
    tSNMP_COUNTER64_TYPE Counter64Value;
    tSNMP_OCTET_STRING_TYPE *pCommString = NULL;
    UINT4               u4SysUpTimeOidLength = SNMP_ZERO;
    tCommunityMappingEntry *pCommunityEntryIn = NULL;

    tSNMP_VAR_BIND     *pTmpVarBind = NULL;
    tSNMP_OID_TYPE     *pVarBindOidName = NULL;
    UINT1               u1TrapOidPresent = SNMP_FALSE;
    UINT1               u1EnterpriseOidPresent = SNMP_FALSE;
    UINT1               u1TrapAddressOidPresent = SNMP_FALSE;
    UINT1               u1TrapCommunityOidPresent = SNMP_FALSE;

    if ((IsProxyFlag == SNMP_TRUE) && (NULL == pSingleTargetOut))
    {
        SNMPTrace (" Single Target Out is NULL \n");
        return NULL;
    }

    Counter64Value.msn = SNMP_ZERO;
    Counter64Value.lsn = SNMP_ZERO;
    pV2TrapMsg = MemAllocMemBlk (gSnmpNormalPduPoolId);
    if (pV2TrapMsg == NULL)
    {
        SNMPTrace ("Unable to allocate Memory for pV2TrapMsg\n");
        return NULL;
    }
    MEMSET (pV2TrapMsg, SNMP_ZERO, sizeof (tSNMP_NORMAL_PDU));
    pV2TrapMsg->i2_PduType = SNMP_PDU_TYPE_SNMPV2_TRAP;
    pV2TrapMsg->u4_Version = VERSION2;
    pV2TrapMsg->i4_ErrorStatus = SNMP_ERR_NO_ERROR;
    pV2TrapMsg->i4_ErrorIndex = SNMP_ZERO;
    if (SNMP_TRUE == IsProxyFlag)
    {
        pV2TrapMsg->u4_RequestID = RequestID;
    }
    else
    {
        pV2TrapMsg->u4_RequestID = SNMPGetRequestID ();
    }

    /* allocate community community String */
    pV2TrapMsg->pCommunityStr =
        allocmem_octetstring (SNMP_MAX_OCTETSTRING_SIZE);
    if (pV2TrapMsg->pCommunityStr == NULL)
    {
        SNMPFreeV2Trap (pV2TrapMsg);
        SNMPTrace ("Unable to allocate memory for Community String\n");
        return NULL;
    }
    if (pV1TrapMsg->pCommunityStr != NULL)
    {
        if (SNMP_TRUE == IsProxyFlag)
        {
            pCommunityEntryIn =
                SNMPGetCommunityEntryFromName (pV1TrapMsg->pCommunityStr);
            if (NULL == pCommunityEntryIn)
            {
                SNMPFreeV2Trap (pV2TrapMsg);
                return NULL;
            }

            if (NULL == (pCommString =
                         SNMPGetCommunityString (pSingleTargetOut,
                                                 &
                                                 (pCommunityEntryIn->
                                                  ContextEngineID))))
            {
                SNMPFreeV2Trap (pV2TrapMsg);
                return NULL;
            }
            SNMPCopyOctetString (pV2TrapMsg->pCommunityStr, pCommString);
            u4SysUpTimeOidLength = SYSUPTIME_OID_LEN;
            pV2VbList = MemAllocMemBlk (gSnmpVarBindPoolId);
            if (pV2VbList == NULL)
            {
                SNMPFreeV2Trap (pV2TrapMsg);
                return (NULL);
            }
            MEMSET (pV2VbList, SNMP_ZERO, sizeof (tSNMP_VAR_BIND));
            if ((pV2VbList->pObjName =
                 alloc_oid ((INT4) u4SysUpTimeOidLength)) == NULL)
            {
                FREE_SNMP_VARBIND (pV2VbList);
                SNMPFreeV2Trap (pV2TrapMsg);
                return NULL;
            }
            MEMCPY (pV2VbList->pObjName->pu4_OidList,
                    gau4sysUpTimeOid, SYSUPTIME_OID_LEN * sizeof (UINT4));

            pV2VbList->ObjValue.u4_ULongValue = pV1TrapMsg->u4_TimeStamp;
            pV2VbList->ObjValue.i2_DataType = SNMP_DATA_TYPE_TIME_TICKS;

        }
        else
        {
            SNMPCopyOctetString (pV2TrapMsg->pCommunityStr,
                                 pV1TrapMsg->pCommunityStr);

            pV2VbList = SNMPGetSysUpTime ();
            if (pV2VbList == NULL)
            {
                SNMPFreeV2Trap (pV2TrapMsg);
                SNMPTrace ("Unable to Get the SysUpTime Value\n");
                return NULL;
            }
            pV2VbList->ObjValue.u4_ULongValue = pV1TrapMsg->u4_TimeStamp;
        }
    }

    pTmpVarBind = pV1TrapMsg->pVarBindList;
    while (pTmpVarBind != NULL)
    {
        pVarBindOidName = pTmpVarBind->pObjName;
        if (pVarBindOidName)
        {
            if (SNMP_EQUAL ==
                SNMPProxyCompareOidtoarr (pVarBindOidName, gau4snmpTrapOid,
                                          TRAP_OID_LEN))
            {
                u1TrapOidPresent = SNMP_TRUE;
            }
            else if (SNMP_EQUAL ==
                     SNMPProxyCompareOidtoarr (pVarBindOidName,
                                               gau4snmpTrapEnterpriseOid,
                                               ENTERPRISE_OID_LEN))
            {
                u1EnterpriseOidPresent = SNMP_TRUE;
            }
            else if (SNMP_EQUAL ==
                     SNMPProxyCompareOidtoarr (pVarBindOidName,
                                               gau4snmpTrapAddressOid,
                                               TRAP_ADDRESS_OID_LEN))
            {
                u1TrapAddressOidPresent = SNMP_TRUE;
            }
            else if (SNMP_EQUAL ==
                     SNMPProxyCompareOidtoarr (pVarBindOidName,
                                               gau4snmpTrapCommunityOid,
                                               TRAP_COMMUNITY_OID_LEN))
            {
                u1TrapCommunityOidPresent = SNMP_TRUE;
            }
        }
        pTmpVarBind = pTmpVarBind->pNextVarBind;
    }

    if (SNMP_FALSE == u1TrapOidPresent)
    {
        if (pV1TrapMsg->i4_GenericTrap == ENTERPRISE_SPECIFIC)
        {
            pSnmpTrapOidValue =
                alloc_oid ((INT4) pV1TrapMsg->pEnterprise->u4_Length +
                           SNMP_TWO);
            if (pSnmpTrapOidValue == NULL)
            {
                FREE_SNMP_VARBIND (pV2VbList);
                SNMPFreeV2Trap (pV2TrapMsg);
                return NULL;
            }
            MEMCPY (pSnmpTrapOidValue->pu4_OidList,
                    pV1TrapMsg->pEnterprise->pu4_OidList,
                    pV1TrapMsg->pEnterprise->u4_Length * sizeof (UINT4));
            pSnmpTrapOidValue->u4_Length = pV1TrapMsg->pEnterprise->u4_Length;
            pSnmpTrapOidValue->pu4_OidList[pSnmpTrapOidValue->u4_Length++] =
                SNMP_ZERO;
            pSnmpTrapOidValue->pu4_OidList[pSnmpTrapOidValue->u4_Length++] =
                (UINT4) pV1TrapMsg->i4_SpecificTrap;

        }
        else
        {
            pSnmpTrapOidValue = alloc_oid (SNMP_TRAP_OID_LEN);
            if (pSnmpTrapOidValue == NULL)
            {
                FREE_SNMP_VARBIND (pV2VbList);
                SNMPFreeV2Trap (pV2TrapMsg);
                return NULL;
            }
            switch (pV1TrapMsg->i4_GenericTrap)
            {
                case COLD_START:
                case WARM_START:
                case LINK_DOWN:
                case LINK_UP:
                case AUTH_FAILURE:
                case EGP_NEIGHBOR_LOSS:
                    MEMCPY (pSnmpTrapOidValue->pu4_OidList, gau4snmpTrapsOid,
                            (AUTH_FAILURE_OID_LEN - SNMP_ONE) * sizeof (UINT4));
                    pSnmpTrapOidValue->u4_Length =
                        AUTH_FAILURE_OID_LEN - SNMP_ONE;
                    pSnmpTrapOidValue->
                        pu4_OidList[pSnmpTrapOidValue->u4_Length++] =
                        (UINT4) pV1TrapMsg->i4_GenericTrap + SNMP_ONE;
                    break;

                default:
                    FREE_SNMP_VARBIND (pV2VbList);
                    SNMPFreeV2Trap (pV2TrapMsg);
                    free_oid (pSnmpTrapOidValue);
                    return (NULL);
            }
        }
        pSnmpStdTrapOid = alloc_oid (SNMP_TRAP_OID_LEN);
        if (pSnmpStdTrapOid == NULL)
        {
            FREE_SNMP_VARBIND (pV2VbList);
            SNMPFreeV2Trap (pV2TrapMsg);
            free_oid (pSnmpTrapOidValue);
            return NULL;
        }
        MEMCPY (pSnmpStdTrapOid->pu4_OidList, gau4snmpTrapOid,
                SNMP_TRAP_OID_LEN * sizeof (UINT4));

        if ((pSnmpTrapVb = SNMP_AGT_FormVarBind (pSnmpStdTrapOid,
                                                 SNMP_DATA_TYPE_OBJECT_ID,
                                                 SNMP_ZERO, SNMP_ZERO, NULL,
                                                 pSnmpTrapOidValue,
                                                 Counter64Value)) == NULL)
        {
            FREE_SNMP_VARBIND (pV2VbList);
            SNMPFreeV2Trap (pV2TrapMsg);
            free_oid (pSnmpTrapOidValue);
            free_oid (pSnmpStdTrapOid);
            return NULL;
        }
        if (NULL != pV2VbList)
        {
            pV2VbList->pNextVarBind = pSnmpTrapVb;
        }

    }                            /* if (SNMP_FALSE == u1TrapOidPresent) */
    else
    {
        pSnmpTrapVb = pV2VbList;
    }

    if (pSnmpTrapVb == NULL)
    {
        FREE_SNMP_VARBIND_LIST (pV2VbList);
        SNMPFreeV2Trap (pV2TrapMsg);
        return (NULL);
    }

    pV1TrapVbList = pV1TrapMsg->pVarBindList;
    while (pV1TrapVbList != NULL)
    {
        pCopiedV1TrapVbList = SNMPCopyVarBind (pV1TrapVbList);
        /*if ((pCopiedV1TrapVbList == NULL) || (pSnmpTrapVb == NULL)) */
        if (pCopiedV1TrapVbList == NULL)
        {
            /* This free takes care of freeing pSnmpTrapVb  and
             * pCopiedV1TrapVbList as well */
            if (pV2VbList == NULL)
            {
                FREE_SNMP_VARBIND_LIST (pSnmpTrapVb);
            }
            else
            {
                FREE_SNMP_VARBIND_LIST (pV2VbList);
            }
            SNMPFreeV2Trap (pV2TrapMsg);
            return (NULL);
        }
        pSnmpTrapVb->pNextVarBind = pCopiedV1TrapVbList;
        pSnmpTrapVb = pSnmpTrapVb->pNextVarBind;
        pV1TrapVbList = pV1TrapVbList->pNextVarBind;
    }

    if ((SNMP_FALSE == u1TrapAddressOidPresent) && (SNMP_TRUE == IsProxyFlag))
    {
        /* Update Trap Address OID name */
        pTrapAddressOid = alloc_oid (TRAP_ADDRESS_OID_LEN);
        if (pTrapAddressOid == NULL)
        {
            /* This free takes care of freeing pSnmpTrapVb as well */
            if (pV2VbList == NULL)
            {
                FREE_SNMP_VARBIND_LIST (pSnmpTrapVb);
            }
            else
            {
                FREE_SNMP_VARBIND_LIST (pV2VbList);
            }
            SNMPFreeV2Trap (pV2TrapMsg);
            return NULL;
        }
        MEMCPY (pTrapAddressOid->pu4_OidList, gau4snmpTrapAddressOid,
                sizeof (gau4snmpTrapAddressOid));

        pTrapAddressOid->u4_Length = TRAP_ADDRESS_OID_LEN;

        /* OID value */
        pTrapAddressOidValue =
            allocmem_octetstring (pV1TrapMsg->pAgentAddr->i4_Length);
        if (pTrapAddressOidValue == NULL)
        {
            /* This free takes care of freeing pSnmpTrapVb as well */
            if (pV2VbList == NULL)
            {
                FREE_SNMP_VARBIND_LIST (pSnmpTrapVb);
            }
            else
            {
                FREE_SNMP_VARBIND_LIST (pV2VbList);
            }
            SNMPFreeV2Trap (pV2TrapMsg);
            free_oid (pTrapAddressOid);
            return NULL;
        }
        SNMPCopyOctetString (pTrapAddressOidValue, pV1TrapMsg->pAgentAddr);

        if ((pSnmpTrapVb->pNextVarBind =
             SNMP_AGT_FormVarBind (pTrapAddressOid, SNMP_DATA_TYPE_IP_ADDR_PRIM,
                                   SNMP_ZERO, SNMP_ZERO, pTrapAddressOidValue,
                                   NULL, Counter64Value)) == NULL)
        {
            /* This free takes care of freeing pSnmpTrapVb as well */
            if (pV2VbList == NULL)
            {
                FREE_SNMP_VARBIND_LIST (pSnmpTrapVb);
            }
            else
            {
                FREE_SNMP_VARBIND_LIST (pV2VbList);
            }
            SNMPFreeV2Trap (pV2TrapMsg);
            free_oid (pTrapAddressOid);
            FREE_OCTET_STRING (pTrapAddressOidValue);
            return (NULL);
        }

        memcpy (&(pSnmpTrapVb->pNextVarBind->ObjValue.u4_ULongValue),
                pTrapAddressOidValue->pu1_OctetList, SNMP_FOUR);
        pSnmpTrapVb->pNextVarBind->ObjValue.u4_ULongValue =
            OSIX_HTONL (pSnmpTrapVb->pNextVarBind->ObjValue.u4_ULongValue);

        pSnmpTrapVb = pSnmpTrapVb->pNextVarBind;
    }

    if ((SNMP_FALSE == u1TrapCommunityOidPresent) && (SNMP_TRUE == IsProxyFlag))
    {
        /* Community OID name */
        pTrapCommunityOid = alloc_oid (TRAP_COMMUNITY_OID_LEN);
        if ((pTrapCommunityOid == NULL) || (NULL == pV1TrapMsg->pCommunityStr))
        {
            /* This free takes care of freeing pSnmpTrapVb as well */
            FREE_SNMP_VARBIND_LIST (pV2VbList);
            SNMPFreeV2Trap (pV2TrapMsg);
            /* This free takes care of pTrapCommunityOid if it was
             * allocated*/
            free_oid (pTrapCommunityOid);
            return NULL;
        }
        MEMCPY (pTrapCommunityOid->pu4_OidList, gau4snmpTrapCommunityOid,
                sizeof (gau4snmpTrapCommunityOid));
        pTrapCommunityOid->u4_Length = TRAP_COMMUNITY_OID_LEN;

        /* OID Value */
        pTrapCommunityOidValue =
            allocmem_octetstring (pV1TrapMsg->pCommunityStr->i4_Length);
        if (pTrapCommunityOidValue == NULL)
        {
            FREE_SNMP_VARBIND_LIST (pV2VbList);
            SNMPFreeV2Trap (pV2TrapMsg);
            free_oid (pTrapCommunityOid);
            return NULL;
        }
        SNMPCopyOctetString (pTrapCommunityOidValue, pV1TrapMsg->pCommunityStr);

        if ((pSnmpTrapVb->pNextVarBind =
             SNMP_AGT_FormVarBind (pTrapCommunityOid, SNMP_DATA_TYPE_OCTET_PRIM,
                                   SNMP_ZERO, SNMP_ZERO, pTrapCommunityOidValue,
                                   NULL, Counter64Value)) == NULL)
        {
            FREE_SNMP_VARBIND_LIST (pV2VbList);
            SNMPFreeV2Trap (pV2TrapMsg);
            free_oid (pTrapCommunityOid);
            FREE_OCTET_STRING (pTrapCommunityOidValue);
            return (NULL);
        }

        pSnmpTrapVb = pSnmpTrapVb->pNextVarBind;

    }

    if (SNMP_FALSE == u1EnterpriseOidPresent)
    {
        /* Update Enterprise OID at the last */
        pEnterpriseTrapOid = alloc_oid (SNMP_TRAP_OID_LEN);
        if (pEnterpriseTrapOid == NULL)
        {
            /* This free takes care of freeing pSnmpTrapVb as well */
            if (pV2VbList == NULL)
            {
                FREE_SNMP_VARBIND_LIST (pSnmpTrapVb);
            }
            else
            {
                FREE_SNMP_VARBIND_LIST (pV2VbList);
            }
            SNMPFreeV2Trap (pV2TrapMsg);
            return NULL;
        }
        MEMCPY (pEnterpriseTrapOid->pu4_OidList, gau4snmpTrapEnterpriseOid,
                sizeof (gau4snmpTrapEnterpriseOid));
        pEnterpriseTrapOid->u4_Length = SNMP_TRAP_OID_LEN;
        pEnterpriseOidValue =
            alloc_oid ((INT4) pV1TrapMsg->pEnterprise->u4_Length);
        if (pEnterpriseOidValue == NULL)
        {
            /* This free takes care of freeing pSnmpTrapVb as well */
            if (pV2VbList == NULL)
            {
                FREE_SNMP_VARBIND_LIST (pSnmpTrapVb);
            }
            else
            {
                FREE_SNMP_VARBIND_LIST (pV2VbList);
            }
            SNMPFreeV2Trap (pV2TrapMsg);
            free_oid (pEnterpriseTrapOid);
            return NULL;
        }
        MEMCPY (pEnterpriseOidValue->pu4_OidList,
                pV1TrapMsg->pEnterprise->pu4_OidList,
                pV1TrapMsg->pEnterprise->u4_Length * sizeof (UINT4));
        pEnterpriseOidValue->u4_Length = pV1TrapMsg->pEnterprise->u4_Length;

        if ((pSnmpTrapVb->pNextVarBind =
             SNMP_AGT_FormVarBind (pEnterpriseTrapOid, SNMP_DATA_TYPE_OBJECT_ID,
                                   SNMP_ZERO, SNMP_ZERO, NULL,
                                   pEnterpriseOidValue,
                                   Counter64Value)) == NULL)
        {
            /* This free takes care of freeing pSnmpTrapVb as well */
            if (pV2VbList == NULL)
            {
                FREE_SNMP_VARBIND_LIST (pSnmpTrapVb);
            }
            else
            {
                FREE_SNMP_VARBIND_LIST (pV2VbList);
            }
            SNMPFreeV2Trap (pV2TrapMsg);
            free_oid (pEnterpriseTrapOid);
            free_oid (pEnterpriseOidValue);
            return (NULL);
        }
    }

    pV2TrapMsg->pVarBindList = pV2VbList;
    /* Free pSnmpTrapVb if it was allocated but was not assigned to pV2VbList
     * memory leak fix*/
    if (pV2VbList == NULL)
    {
        FREE_SNMP_VARBIND_LIST (pSnmpTrapVb);
    }
    return (pV2TrapMsg);
}

/************************************************************************
 *  Function Name   : SNMPV2TrapToV1Trap 
 *  Description     : Function to convert v2 trap to v1 trap
 *  Input           : pV2TrapMsg - v2 Trap Pointer
 *  Output          : None
 *  Returns         : v1 trap pointer Else NULL
 ************************************************************************/

tSNMP_TRAP_PDU     *
SNMPV2TrapToV1Trap (tSNMP_NORMAL_PDU * pV2TrapMsg)
{
    tSNMP_TRAP_PDU     *pV1TrapMsg = NULL;
    tSNMP_OID_TYPE     *pEnterprise = NULL;
    tSNMP_VAR_BIND     *pTempVb = NULL, *pTempVb1 = NULL;
    tSNMP_VAR_BIND     *pFirstVb = NULL, *pDesVb = NULL;
    INT4                i4GenericTrap = 0;
    INT4                i4SpecificTrap = 0;
    UINT1               u1EnterpriseTrapStatus = 0;
    UINT1               u1FirstVb = 1;

    pV1TrapMsg = MemAllocMemBlk (gSnmpTrapPduPoolId);
    if (pV1TrapMsg == NULL)
    {
        SNMPTrace ("Unable to allocate memory for pV1TrapMsg\n");
        return NULL;
    }
    MEMSET (pV1TrapMsg, SNMP_ZERO, sizeof (tSNMP_TRAP_PDU));
    pV1TrapMsg->i2_PduType = SNMP_PDU_TYPE_TRAP;
    pV1TrapMsg->u4_Version = VERSION1;
    pV1TrapMsg->u4_TimeStamp = pV2TrapMsg->pVarBindList->ObjValue.u4_ULongValue;

    /* Allocate Community String */
    pV1TrapMsg->pCommunityStr =
        allocmem_octetstring (SNMP_MAX_OCTETSTRING_SIZE);
    if (pV1TrapMsg->pCommunityStr == NULL)
    {
        SNMPFreeV1Trap (pV1TrapMsg);
        SNMPTrace ("Unable to allocate memory for Community String\n");
        return NULL;
    }
    pEnterprise = SNMPGetEnterpriseOid (pV2TrapMsg, &i4GenericTrap,
                                        &i4SpecificTrap,
                                        &u1EnterpriseTrapStatus);
    if (pEnterprise == NULL)
    {
        SNMPTrace ("Failure in getting Enterprise OID from V2Trap for v1");
        SNMPFreeV1Trap (pV1TrapMsg);
        return NULL;
    }

    /* Agent IP Address is Updated from System Type */
    pV1TrapMsg->pAgentAddr = allocmem_octetstring (sizeof (UINT4));
    if (pV1TrapMsg->pAgentAddr == NULL)
    {
        SNMPTrace ("Failure to allocate Agent address for v1 trap \n");
        SNMPFreeV1Trap (pV1TrapMsg);
        free_oid (pEnterprise);
        return NULL;
    }
    pV1TrapMsg->pAgentAddr->i4_Length = sizeof (UINT4);

    pV1TrapMsg->pEnterprise = pEnterprise;
    pV1TrapMsg->i4_GenericTrap = i4GenericTrap;
    pV1TrapMsg->i4_SpecificTrap = i4SpecificTrap;
    pTempVb = pV2TrapMsg->pVarBindList;
    pTempVb = pTempVb->pNextVarBind;

    while (pTempVb != NULL)
    {
        if (pTempVb == pV2TrapMsg->pVarBindEnd)
        {
            if (u1EnterpriseTrapStatus == SNMP_ONE)
            {
                break;
            }
        }
        if (pTempVb->ObjValue.i2_DataType == SNMP_DATA_TYPE_COUNTER64)
        {
            pTempVb = pTempVb->pNextVarBind;
            continue;
        }
        pDesVb = SNMPCopyVarBind (pTempVb);
        if (pDesVb == NULL)
        {
            if (u1FirstVb != SNMP_ONE)
            {
                FREE_SNMP_VARBIND_LIST (pFirstVb);
            }
            SNMPFreeV1Trap (pV1TrapMsg);
            return (NULL);
        }
        if (u1FirstVb == SNMP_ONE)
        {
            pTempVb1 = pDesVb;
            pFirstVb = pTempVb1;
            u1FirstVb++;
        }
        else
        {
            pTempVb1->pNextVarBind = pDesVb;
            pTempVb1 = pTempVb1->pNextVarBind;
        }
        pTempVb = pTempVb->pNextVarBind;
    }

    if (pTempVb1 != NULL)
    {
        pTempVb1->pNextVarBind = NULL;
    }

    pV1TrapMsg->pVarBindList = pFirstVb;
    pV1TrapMsg->pVarBindEnd = pDesVb;

    return (pV1TrapMsg);
}

/************************************************************************
 *  Function Name   : SNMPGetSysUpTime 
 *  Description     : Function to generate system up time variable bind
 *  Input           : None
 *  Output          : None
 *  Returns         : variable bind pointer for sys up time Else NULL
 ************************************************************************/

tSNMP_VAR_BIND     *
SNMPGetSysUpTime ()
{
    UINT4               u4SysUpTimeOidLength = SNMP_ZERO;
    UINT4               u4SysUpTimeValue = SNMP_ZERO;
    tSNMP_VAR_BIND     *pSnmpSysUpTimeVb = NULL;

    u4SysUpTimeOidLength = SYSUPTIME_OID_LEN;
    pSnmpSysUpTimeVb = MemAllocMemBlk (gSnmpVarBindPoolId);
    if (pSnmpSysUpTimeVb == NULL)
    {
        return (NULL);
    }
    MEMSET (pSnmpSysUpTimeVb, SNMP_ZERO, sizeof (tSNMP_VAR_BIND));
    if ((pSnmpSysUpTimeVb->pObjName =
         alloc_oid ((INT4) u4SysUpTimeOidLength)) == NULL)
    {
        FREE_SNMP_VARBIND (pSnmpSysUpTimeVb);
        return (NULL);
    }
    MEMCPY (pSnmpSysUpTimeVb->pObjName->pu4_OidList,
            gau4sysUpTimeOid, SYSUPTIME_OID_LEN * sizeof (UINT4));
    /*
     * Low level function to get the sysUpTime value
     */
    nmhGetSysUpTime (&u4SysUpTimeValue);
    pSnmpSysUpTimeVb->ObjValue.u4_ULongValue = u4SysUpTimeValue;
    pSnmpSysUpTimeVb->ObjValue.i2_DataType = SNMP_DATA_TYPE_TIME_TICKS;
    return (pSnmpSysUpTimeVb);
}

/************************************************************************
 *  Function Name   : SNMPGetEnterpriseOid 
 *  Description     : Function to get enterprise OID from v2trap Varbind 
 *  Input           : pV2TrapMsg - Trap Variable Bind pointer
 *  Output          : pi4GenericTrap - Pointer to updated Generic trap type
 *                    pi4SpecificTrap - Pointer to updated Specific trap
 *                    type                                          
 *                    pu1EnterpriseTrapStatus - Pointer to be updated
 *                                              with enterprise trap status
 *  Returns         : Enterprise OID pointer Else NULL on any failure 
 ************************************************************************/

tSNMP_OID_TYPE     *
SNMPGetEnterpriseOid (tSNMP_NORMAL_PDU * pV2TrapMsg,
                      INT4 *pi4GenericTrap,
                      INT4 *pi4SpecificTrap, UINT1 *pu1EnterpriseTrapStatus)
{
    tSNMP_OID_TYPE     *pEnterprise = NULL;
    tSNMP_OID_TYPE     *pLastVbOid = NULL;
    tSNMP_OID_TYPE     *pTempOid = NULL;
    tSNMP_OID_TYPE     *pTempOidValue = NULL;
    tSNMP_VAR_BIND     *pTempVb = NULL;
    UINT4               u4Temp = 0;

    pTempVb = pV2TrapMsg->pVarBindList->pNextVarBind;

    pLastVbOid = pV2TrapMsg->pVarBindEnd->pObjName;
    pTempOid = pTempVb->pObjName;
    *pu1EnterpriseTrapStatus = SNMP_ZERO;
    if ((!(MEMCMP (gau4snmpTrapOid, pTempOid->pu4_OidList,
                   SNMP_TRAP_OID_LEN * sizeof (UINT4)) == SNMP_ZERO) &&
         (pTempOid->u4_Length == SNMP_TRAP_OID_LEN)))
    {
        return (NULL);
    }

    pTempOidValue = pTempVb->ObjValue.pOidValue;
    for (u4Temp = SNMP_ZERO; u4Temp < (SNMP_TRAP_OID_LEN - SNMP_TWO); u4Temp++)
    {
        if (gau4snmpTrapsOid[u4Temp] != pTempOidValue->pu4_OidList[u4Temp])
        {
            break;
        }
    }
    /* Check whether its any one of the standard traps */
    if ((u4Temp == (SNMP_TRAP_OID_LEN - SNMP_TWO)) &&
        (pTempOidValue->pu4_OidList[u4Temp] >= (COLD_START + SNMP_ONE)) &&
        (pTempOidValue->pu4_OidList[u4Temp] <= (EGP_NEIGHBOR_LOSS + SNMP_ONE)))
    {
        *pi4GenericTrap = (INT4) pTempOidValue->pu4_OidList[u4Temp] - SNMP_ONE;
        *pi4SpecificTrap = SNMP_ZERO;
        if ((MEMCMP (gau4snmpTrapEnterpriseOid, pLastVbOid->pu4_OidList,
                     SNMP_TRAP_OID_LEN * sizeof (UINT4)) == SNMP_ZERO) &&
            (pLastVbOid->u4_Length == (SNMP_TRAP_OID_LEN)))
        {
            pEnterprise =
                alloc_oid ((INT4) pV2TrapMsg->pVarBindEnd->ObjValue.pOidValue->
                           u4_Length);
            if (pEnterprise == NULL)
            {
                return (NULL);
            }
            MEMCPY (pEnterprise->pu4_OidList,
                    pV2TrapMsg->pVarBindEnd->ObjValue.pOidValue->pu4_OidList,
                    (pV2TrapMsg->pVarBindEnd->ObjValue.pOidValue->u4_Length *
                     sizeof (UINT4)));
            *pu1EnterpriseTrapStatus = SNMP_ONE;
        }
        else
        {
            pEnterprise = alloc_oid (SNMP_OID_LEN);
            if (pEnterprise == NULL)
            {
                return (NULL);
            }
            MEMCPY (pEnterprise->pu4_OidList, gau4snmpOid,
                    SNMP_OID_LEN * sizeof (UINT4));
            pEnterprise->u4_Length = SNMP_OID_LEN;
        }
    }
    else                        /* Not one of the 6 standard traps */
    {
        if (pTempOidValue->pu4_OidList[pTempOidValue->u4_Length - SNMP_TWO] !=
            SNMP_ZERO)
        {
            pEnterprise =
                alloc_oid (((INT4) pTempOidValue->u4_Length - SNMP_ONE));
            if (pEnterprise == NULL)
            {
                return (NULL);
            }
            MEMCPY (pEnterprise->pu4_OidList, pTempOidValue->pu4_OidList,
                    ((pTempOidValue->u4_Length - SNMP_ONE) * sizeof (UINT4)));
            pEnterprise->u4_Length = (pTempOidValue->u4_Length - 1);
        }
        else
        {
            *pu1EnterpriseTrapStatus = SNMP_ONE;
            pEnterprise =
                alloc_oid (((INT4) pTempOidValue->u4_Length - SNMP_TWO));
            if (pEnterprise == NULL)
            {
                return (NULL);
            }
            MEMCPY (pEnterprise->pu4_OidList, pTempOidValue->pu4_OidList,
                    ((pTempOidValue->u4_Length - SNMP_TWO) * sizeof (UINT4)));
            pEnterprise->u4_Length = (pTempOidValue->u4_Length - 2);
        }
        *pi4GenericTrap = ENTERPRISE_SPECIFIC;
        *pi4SpecificTrap = (INT4) pTempOidValue->pu4_OidList
            [pTempOidValue->u4_Length - SNMP_ONE];
    }
    return (pEnterprise);
}

/************************************************************************
 *  Function Name   : SNMPFormVarBind 
 *  Description     : Function to form variable bind for the give oid and
 *                    value. This will allocate memory for each element.
 *                    This is the difference between this and 
 *                    SNMP_AGT_FormVarBind   
 *  Input           : pOid - Pointer to oid to be assigned in varbind
 *                    i2Type - Data type in the varbind value
 *                    u4Value  - value of unsigned32 if type is unsigned
 *                    i4Value - value of signed32 if the type is signed32
 *                    pOctet - pointer to octet string if the type is
 *                    octet string
 *                    pOidType - Pointer to oid if the type is oid
 *                    u8Value - Conter64 value if the type is conter64 
 *  Output          : None 
 *  Returns         : Variable bind pointer or null
 ************************************************************************/

tSNMP_VAR_BIND     *
SNMPFormVarBind (tSNMP_OID_TYPE * pOid, INT2 i2Type, UINT4 u4Value,
                 INT4 i4Value, tSNMP_OCTET_STRING_TYPE * pOctet,
                 tSNMP_OID_TYPE * pOidType, tSNMP_COUNTER64_TYPE u8Value)
{
    tSNMP_VAR_BIND     *pVarBindPtr = NULL;
    if ((NULL == pOctet)
        && ((i2Type == SNMP_DATA_TYPE_OCTET_PRIM)
            || (i2Type == SNMP_DATA_TYPE_OPAQUE)
            || (i2Type == SNMP_DATA_TYPE_IP_ADDR_PRIM)))
    {
        SNMPTrace ("Octet String is NULL\n");
        return NULL;
    }
    if ((pVarBindPtr = MemAllocMemBlk (gSnmpVarBindPoolId)) == NULL)
    {
        SNMPTrace ("Malloc Failed for Variable Bind List\n");
        return NULL;
    }
    MEMSET (pVarBindPtr, SNMP_ZERO, sizeof (tSNMP_VAR_BIND));

    pVarBindPtr->pObjName = alloc_oid ((INT4) pOid->u4_Length);
    if (pVarBindPtr->pObjName == NULL)
    {
        FREE_SNMP_VARBIND (pVarBindPtr);
        SNMPTrace ("Malloc Failed for object name\n");
        return NULL;
    }

    MEMCPY (pVarBindPtr->pObjName->pu4_OidList, pOid->pu4_OidList,
            pOid->u4_Length * sizeof (UINT4));
    pVarBindPtr->pObjName->u4_Length = pOid->u4_Length;
    pVarBindPtr->ObjValue.i2_DataType = i2Type;
    pVarBindPtr->ObjValue.u4_ULongValue = SNMP_ZERO;
    pVarBindPtr->ObjValue.i4_SLongValue = SNMP_ZERO;
    pVarBindPtr->ObjValue.pOidValue = NULL;
    pVarBindPtr->ObjValue.pOctetStrValue = NULL;
    pVarBindPtr->ObjValue.u8_Counter64Value.lsn = SNMP_ZERO;
    pVarBindPtr->ObjValue.u8_Counter64Value.msn = SNMP_ZERO;
    pVarBindPtr->pNextVarBind = NULL;
    switch (i2Type)
    {
        case SNMP_DATA_TYPE_COUNTER32:
        case SNMP_DATA_TYPE_GAUGE32:
        case SNMP_DATA_TYPE_TIME_TICKS:
            pVarBindPtr->ObjValue.u4_ULongValue = u4Value;
            break;
        case SNMP_DATA_TYPE_INTEGER32:
            pVarBindPtr->ObjValue.i4_SLongValue = i4Value;
            break;
        case SNMP_DATA_TYPE_OBJECT_ID:
            pVarBindPtr->ObjValue.pOidValue =
                alloc_oid ((INT4) pOidType->u4_Length);
            if (pVarBindPtr->ObjValue.pOidValue == NULL)
            {
                FREE_SNMP_VARBIND (pVarBindPtr);
                return NULL;
            }
            MEMCPY (pVarBindPtr->ObjValue.pOidValue->pu4_OidList,
                    pOidType->pu4_OidList,
                    pOidType->u4_Length * sizeof (UINT4));
            break;
        case SNMP_DATA_TYPE_OCTET_PRIM:
        case SNMP_DATA_TYPE_OPAQUE:
            pVarBindPtr->ObjValue.pOctetStrValue =
                allocmem_octetstring (pOctet->i4_Length);
            if (pVarBindPtr->ObjValue.pOctetStrValue == NULL)
            {
                FREE_SNMP_VARBIND (pVarBindPtr);
                return NULL;
            }
            MEMCPY (pVarBindPtr->ObjValue.pOctetStrValue->pu1_OctetList,
                    pOctet->pu1_OctetList, pOctet->i4_Length);
            pVarBindPtr->ObjValue.pOctetStrValue->i4_Length = pOctet->i4_Length;

            break;
        case SNMP_DATA_TYPE_IP_ADDR_PRIM:
            pVarBindPtr->ObjValue.pOctetStrValue =
                allocmem_octetstring (pOctet->i4_Length);
            if (pVarBindPtr->ObjValue.pOctetStrValue == NULL)
            {
                FREE_SNMP_VARBIND (pVarBindPtr);
                return NULL;
            }
            MEMCPY (pVarBindPtr->ObjValue.pOctetStrValue->pu1_OctetList,
                    pOctet->pu1_OctetList, pOctet->i4_Length);
            pVarBindPtr->ObjValue.pOctetStrValue->i4_Length = pOctet->i4_Length;

            /*Also copy the value in the u4_ULongValue */

            MEMCPY (&(pVarBindPtr->ObjValue.u4_ULongValue),
                    pOctet->pu1_OctetList, pOctet->i4_Length);

            pVarBindPtr->ObjValue.u4_ULongValue =
                OSIX_HTONL (pVarBindPtr->ObjValue.u4_ULongValue);

            break;
        case SNMP_DATA_TYPE_COUNTER64:
            pVarBindPtr->ObjValue.u8_Counter64Value.lsn = u8Value.lsn;
            pVarBindPtr->ObjValue.u8_Counter64Value.msn = u8Value.msn;
            break;
        case SNMP_DATA_TYPE_NULL:
        case SNMP_EXCEPTION_NO_SUCH_OBJECT:
        case SNMP_EXCEPTION_NO_SUCH_INSTANCE:
        case SNMP_EXCEPTION_END_OF_MIB_VIEW:
            break;
        default:
            FREE_SNMP_VARBIND (pVarBindPtr);
            return NULL;
    }
    return pVarBindPtr;
}

/************************************************************************
 *  Function Name   : SNMP_free_snmp_vb_list 
 *  Description     : Function to free the varbind List 
 *  Input           : vb_list - pointer to varbind list
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
SNMP_free_snmp_vb_list (tSNMP_VAR_BIND * vb_list)
{
    tSNMP_VAR_BIND     *pcurr_vb = NULL;

    while (vb_list != NULL)
    {
        pcurr_vb = vb_list;
        vb_list = vb_list->pNextVarBind;
        FREE_SNMP_VARBIND (pcurr_vb);
    }
}

/************************************************************************
 *  Function Name   : SnmpSendAuthFailureTrap 
 *  Description     : Function to send auth trap when community string
 *                    check fails for snmp request   
 *  Input           : pPdu - pointer to SNMP pdu
 *  Output          : None
 *  Returns         : None
 ************************************************************************/

VOID
SnmpSendAuthFailureTrap (tSNMP_NORMAL_PDU * pPdu)
{
    tSNMP_OID_TYPE     *pAuthtrapoid = NULL, *pSnmpTrapOid = NULL;
    tSNMP_VAR_BIND     *pSnmpAuthVb = NULL;
    tSNMP_OID_TYPE     *pEnterOid = NULL, *pSnmpEnterOid = NULL;
    tSNMP_COUNTER64_TYPE u8Value;
    tSNMP_VAR_BIND     *pVarPtr = pPdu->pVarBindList;
    INT4                i4_Length = 0;
    UINT4               u4OidLen = 0;
    INT2                i2Type = 0;
    tSNMP_OID_TYPE     *pVarOid, *pValOid = NULL;
    tSNMP_OCTET_STRING_TYPE *pVarOctStr = NULL;
    u8Value.msn = 0;
    u8Value.lsn = 0;
    pAuthtrapoid = alloc_oid (AUTH_FAILURE_OID_LEN);
    if (pAuthtrapoid == NULL)
    {
        SNMPTrace ("Unable to allocate memory for pAuthtrapoid\n");
        return;
    }
    MEMCPY (pAuthtrapoid->pu4_OidList, gau4authFailureOid,
            AUTH_FAILURE_OID_LEN * sizeof (UINT4));
    pAuthtrapoid->u4_Length = AUTH_FAILURE_OID_LEN;

    pSnmpTrapOid = alloc_oid (SNMP_TRAP_OID_LEN);
    if (pSnmpTrapOid == NULL)
    {
        SNMPTrace ("Unable to allocate memory for pSnmpTrapOid\n");
        FREE_OID (pAuthtrapoid);
        return;
    }
    MEMCPY (pSnmpTrapOid->pu4_OidList, gau4snmpTrapOid,
            SNMP_TRAP_OID_LEN * sizeof (UINT4));
    pSnmpTrapOid->u4_Length = SNMP_TRAP_OID_LEN;

    pSnmpAuthVb = SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                        SNMP_DATA_TYPE_OBJECT_ID,
                                        SNMP_ZERO, SNMP_ZERO, NULL,
                                        pAuthtrapoid, u8Value);
    if (pSnmpAuthVb == NULL)
    {
        SNMPTrace ("Failure in FormVarBind fn. for in AuthTrapVb formation\n");
        FREE_OID (pAuthtrapoid);
        FREE_OID (pSnmpTrapOid);
        return;
    }
    pEnterOid = alloc_oid (SNMP_TRAP_OID_LEN);
    if (pEnterOid == NULL)
    {
        SNMPTrace ("Unable to allocate memory for pEnterOid\n");
        FREE_SNMP_VARBIND_LIST (pSnmpAuthVb);
        return;
    }
    MEMCPY (pEnterOid->pu4_OidList, gau4snmpTrapEnterpriseOid,
            SNMP_TRAP_OID_LEN * sizeof (UINT4));
    pEnterOid->u4_Length = SNMP_TRAP_OID_LEN;

    pSnmpEnterOid = alloc_oid (SNMP_OID_LEN);
    if (pSnmpEnterOid == NULL)
    {
        SNMPTrace ("Unable to allocate memory for pSnmpEnterOid\n");
        FREE_SNMP_VARBIND_LIST (pSnmpAuthVb);
        FREE_OID (pEnterOid);
        return;
    }
    MEMCPY (pSnmpEnterOid->pu4_OidList, gau4snmpOid,
            SNMP_OID_LEN * sizeof (UINT4));

    pSnmpAuthVb->pNextVarBind = SNMP_AGT_FormVarBind (pEnterOid,
                                                      SNMP_DATA_TYPE_OBJECT_ID,
                                                      SNMP_ZERO, SNMP_ZERO,
                                                      NULL, pSnmpEnterOid,
                                                      u8Value);
    if (pSnmpAuthVb->pNextVarBind == NULL)
    {
        SNMPTrace ("Failure in FormVarbind fn. for forming pSnmpAuthVb\n");
        FREE_SNMP_VARBIND_LIST (pSnmpAuthVb);
        FREE_OID (pEnterOid);
        FREE_OID (pSnmpEnterOid);
        return;
    }
    /* Add the additional Variable Bindings */
    if (pVarPtr != NULL)
    {
        u4OidLen = pVarPtr->pObjName->u4_Length;
        if ((pVarOid = alloc_oid ((INT4) u4OidLen)) == NULL)
        {
            SNMPTrace ("Unable to allocate memory for pEnterOid\n");
            FREE_SNMP_VARBIND_LIST (pSnmpAuthVb);
            return;
        }
        MEMCPY (pVarOid->pu4_OidList, pVarPtr->pObjName->pu4_OidList,
                u4OidLen * sizeof (UINT4));
        pVarOid->u4_Length = u4OidLen;
        i2Type = (pVarPtr->ObjValue.i2_DataType);
        switch (i2Type)
        {
            case SNMP_DATA_TYPE_COUNTER32:
            case SNMP_DATA_TYPE_GAUGE32:
            case SNMP_DATA_TYPE_TIME_TICKS:
                if ((pSnmpAuthVb->pNextVarBind->pNextVarBind =
                     SNMP_AGT_FormVarBind (pVarOid,
                                           (pVarPtr->ObjValue.i2_DataType),
                                           (pVarPtr->ObjValue.u4_ULongValue), 0,
                                           NULL, pSnmpEnterOid,
                                           u8Value)) == NULL)
                {
                    SNMPTrace
                        ("Failure in FormVarbind fn. for forming pSnmpAuthVb\n");
                    FREE_SNMP_VARBIND_LIST (pSnmpAuthVb);
                    FREE_OID (pVarOid);
                    return;
                }
                break;
            case SNMP_DATA_TYPE_INTEGER32:
                if ((pSnmpAuthVb->pNextVarBind->pNextVarBind =
                     SNMP_AGT_FormVarBind (pVarOid,
                                           (pVarPtr->ObjValue.i2_DataType), 0,
                                           (pVarPtr->ObjValue.i4_SLongValue),
                                           NULL, pSnmpEnterOid,
                                           u8Value)) == NULL)
                {
                    SNMPTrace
                        ("Failure in FormVarbind fn. for forming pSnmpAuthVb\n");
                    FREE_SNMP_VARBIND_LIST (pSnmpAuthVb);
                    FREE_OID (pVarOid);
                    return;
                }
                break;
            case SNMP_DATA_TYPE_OBJECT_ID:
                u4OidLen = pVarPtr->ObjValue.pOidValue->u4_Length;
                if ((pValOid = alloc_oid ((INT4) u4OidLen)) == NULL)
                {
                    SNMPTrace ("Unable to allocate memory for pValOid\n");
                    FREE_SNMP_VARBIND_LIST (pSnmpAuthVb);
                    FREE_OID (pVarOid);
                    return;
                }
                MEMCPY (pValOid->pu4_OidList,
                        pVarPtr->ObjValue.pOidValue->pu4_OidList,
                        u4OidLen * sizeof (UINT4));
                pValOid->u4_Length = u4OidLen;
                if ((pSnmpAuthVb->pNextVarBind->pNextVarBind =
                     SNMP_AGT_FormVarBind (pVarOid,
                                           (pVarPtr->ObjValue.i2_DataType), 0,
                                           0, NULL, pValOid, u8Value)) == NULL)
                {
                    SNMPTrace
                        ("Failure in FormVarbind fn. for forming pSnmpAuthVb\n");
                    FREE_SNMP_VARBIND_LIST (pSnmpAuthVb);
                    FREE_OID (pValOid);
                    FREE_OID (pVarOid);
                    return;
                }
                break;
            case SNMP_DATA_TYPE_OCTET_PRIM:
            case SNMP_DATA_TYPE_OPAQUE:
            case SNMP_DATA_TYPE_IP_ADDR_PRIM:
                if ((pVarOctStr =
                     allocmem_octetstring (SNMP_MAX_OCTETSTRING_SIZE)) == NULL)
                {
                    SNMPTrace ("Failure in allocating OctetString \n");
                    FREE_SNMP_VARBIND_LIST (pSnmpAuthVb);
                    FREE_OID (pVarOid);
                    return;
                }
                i4_Length = pVarPtr->ObjValue.pOctetStrValue->i4_Length;
                MEMCPY (pVarOctStr->pu1_OctetList,
                        pVarPtr->ObjValue.pOctetStrValue->pu1_OctetList,
                        i4_Length);
                pVarOctStr->i4_Length = i4_Length;

                if ((pSnmpAuthVb->pNextVarBind->pNextVarBind =
                     SNMP_AGT_FormVarBind ((pVarOid),
                                           (pVarPtr->ObjValue.i2_DataType), 0,
                                           0, pVarOctStr, 0, u8Value)) == NULL)
                {
                    SNMPTrace
                        ("Failure in FormVarbind fn. for forming pSnmpAuthVb\n");
                    FREE_SNMP_VARBIND_LIST (pSnmpAuthVb);
                    FREE_OCTET_STRING (pVarOctStr);
                    FREE_OID (pVarOid);
                    return;
                }
                break;
            case SNMP_DATA_TYPE_COUNTER64:
                if ((pSnmpAuthVb->pNextVarBind->pNextVarBind =
                     SNMP_AGT_FormVarBind (pVarOid,
                                           (pVarPtr->ObjValue.i2_DataType), 0,
                                           0, NULL, 0,
                                           (pVarPtr->
                                            ObjValue.u8_Counter64Value))) ==
                    NULL)
                {
                    SNMPTrace
                        ("Failure in FormVarbind fn. for forming pSnmpAuthVb\n");
                    FREE_SNMP_VARBIND_LIST (pSnmpAuthVb);
                    FREE_OID (pVarOid);
                    return;
                }
                break;
            default:
                if (i2Type == SNMP_DATA_TYPE_NULL)
                {
                    pSnmpAuthVb->pNextVarBind->pNextVarBind = NULL;
                    SNMP_AGT_RIF_Notify_v2Trap (pSnmpAuthVb);
                    FREE_OID (pVarOid);
                    return;
                }
                if ((pSnmpAuthVb->pNextVarBind->pNextVarBind =
                     SNMP_AGT_FormVarBind (pVarOid,
                                           (pVarPtr->ObjValue.i2_DataType),
                                           0, 0, NULL, 0, u8Value)) == NULL)

                {
                    SNMPTrace ("Failure in AGT FormVarbind fn. for default.\n");
                    FREE_SNMP_VARBIND_LIST (pSnmpAuthVb);
                    FREE_OID (pVarOid);
                    return;
                }
                break;
        }                        /* end of Switch */
        pSnmpAuthVb->pNextVarBind->pNextVarBind->pNextVarBind = NULL;
    }                            /* end of if */
    else
    {
        pSnmpAuthVb->pNextVarBind->pNextVarBind = NULL;
    }
    SNMP_AGT_RIF_Notify_v2Trap (pSnmpAuthVb);
    return;
}

/************************************************************************
 *  Function Name   : SNMPInitNotifyTable
 *  Description     : Initialise the Notify Table 
 *  Input           : None
 *  Output          : None
 *  Returns         : SNMP_SUCCESS/SNMP_FAILURE 
 ************************************************************************/
INT4
SNMPInitNotifyTable ()
{
    UINT4               u4Index = SNMP_ZERO;
    MEMSET (gSnmpNotifyTable, SNMP_ZERO, sizeof (gSnmpNotifyTable));
    MEMSET (gau1NotifyName, SNMP_ZERO, sizeof (gau1NotifyName));
    MEMSET (gau1NotifyTag, SNMP_ZERO, sizeof (gau1NotifyTag));
    TMO_SLL_Init (&gSnmpNotifySll);
    for (u4Index = SNMP_ZERO; u4Index < MAX_TRAP_TABLE_ENTRY; u4Index++)
    {
        gSnmpNotifyTable[u4Index].NotifyName.pu1_OctetList =
            gau1NotifyName[u4Index];
        gSnmpNotifyTable[u4Index].NotifyTag.pu1_OctetList =
            gau1NotifyTag[u4Index];
        gSnmpNotifyTable[u4Index].i4Status = SNMP_INACTIVE;

    }
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : SNMPGetNotifyEntry
 *  Description     : Function to return the corresponding Notify Entry 
 *  Input           : pNotifyName - Notify Name
 *  Output          : None
 *  Returns         : pointer to notification entry Else NULL
 ************************************************************************/

tSnmpNotifyEntry   *
SNMPGetNotifyEntry (tSNMP_OCTET_STRING_TYPE * pNotifyName)
{
    tTMO_SLL_NODE      *pLstNode = NULL;
    tSnmpNotifyEntry   *pSnmpNotifyEntry = NULL;
    TMO_SLL_Scan (&gSnmpNotifySll, pLstNode, tTMO_SLL_NODE *)
    {
        pSnmpNotifyEntry = (tSnmpNotifyEntry *) pLstNode;
        if (SNMPCompareOctetString (pNotifyName,
                                    &(pSnmpNotifyEntry->NotifyName)) ==
            SNMP_EQUAL)
        {
            return pSnmpNotifyEntry;
        }

    }
    return NULL;
}

/************************************************************************
 *  Function Name   : SNMPCreateNotifyEntry
 *  Description     : Function to Create a Notify Entry 
 *  Input           : pNotifyName - Notify Name
 *  Output          : None
 *  Returns         : pointer to notify entry Else NULL
 ************************************************************************/

tSnmpNotifyEntry   *
SNMPCreateNotifyEntry (tSNMP_OCTET_STRING_TYPE * pNotifyName)
{
    UINT4               u4Index = SNMP_ZERO;
    if (SNMPGetNotifyEntry (pNotifyName) != NULL)
    {
        return NULL;
    }
    for (u4Index = SNMP_ZERO; u4Index < MAX_TRAP_TABLE_ENTRY; u4Index++)
    {
        if (gSnmpNotifyTable[u4Index].i4Status == SNMP_INACTIVE)
        {
            gSnmpNotifyTable[u4Index].i4Status = UNDER_CREATION;
            MEMSET (gSnmpNotifyTable[u4Index].NotifyName.pu1_OctetList, 0,
                    SNMP_MAX_OCTETSTRING_SIZE);
            MEMSET (gSnmpNotifyTable[u4Index].NotifyTag.pu1_OctetList, 0,
                    SNMP_MAX_OCTETSTRING_SIZE);
            gSnmpNotifyTable[u4Index].NotifyName.i4_Length = SNMP_ZERO;
            gSnmpNotifyTable[u4Index].NotifyTag.i4_Length = SNMP_ZERO;
            gSnmpNotifyTable[u4Index].i4NotifyStorageType =
                SNMP3_STORAGE_TYPE_NONVOLATILE;
            gSnmpNotifyTable[u4Index].i4NotifyType = SNMP_TRAP_TYPE;
            SNMPCopyOctetString (&(gSnmpNotifyTable[u4Index].NotifyName),
                                 pNotifyName);
            SNMPAddNotifySll (&(gSnmpNotifyTable[u4Index].Link));
            return (&(gSnmpNotifyTable[u4Index]));
        }
    }
    return NULL;
}

/************************************************************************
 *  Function Name   : SNMPDeleteNotifyEntry
 *  Description     : Function to Delete a Notify Entry 
 *  Input           : pNotifyName - Notify Name
 *  Output          : None
 *  Returns         : SNMP_SUCCESS/SNMP_FAILURE 
 ************************************************************************/

INT4
SNMPDeleteNotifyEntry (tSNMP_OCTET_STRING_TYPE * pNotifyName)
{
    tTMO_SLL_NODE      *pLstNode = NULL;
    tSnmpNotifyEntry   *pSnmpNotifyEntry = NULL;
    TMO_SLL_Scan (&gSnmpNotifySll, pLstNode, tTMO_SLL_NODE *)
    {
        pSnmpNotifyEntry = (tSnmpNotifyEntry *) pLstNode;
        if (SNMPCompareOctetString (pNotifyName,
                                    &(pSnmpNotifyEntry->NotifyName)) ==
            SNMP_EQUAL)
        {
            pSnmpNotifyEntry->i4Status = SNMP_INACTIVE;
            SNMPDelNotifySll (&(pSnmpNotifyEntry->Link));
            return SNMP_SUCCESS;
        }

    }
    return SNMP_FAILURE;

}

/************************************************************************
 *  Function Name   : SNMPGetFirstNotifyEntry
 *  Description     : Function to return the first Notify Entry 
 *  Input           : None 
 *  Output          : None
 *  Returns         : pointer to first notification entry
 ************************************************************************/

tSnmpNotifyEntry   *
SNMPGetFirstNotifyEntry ()
{
    tSnmpNotifyEntry   *pSnmpNotifyEntry = NULL;
    pSnmpNotifyEntry = (tSnmpNotifyEntry *) TMO_SLL_First (&gSnmpNotifySll);
    return pSnmpNotifyEntry;

}

/************************************************************************
 *  Function Name   : SNMPGetNextNotifyEntry
 *  Description     : Function to return the Next Notify Entry 
 *  Input           : pNotifyName - Notify Name 
 *  Output          : None
 *  Returns         : pointer to next notification entry Else NULL
 ************************************************************************/

tSnmpNotifyEntry   *
SNMPGetNextNotifyEntry (tSNMP_OCTET_STRING_TYPE * pNotifyName)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tSnmpNotifyEntry   *pSnmpNotifyEntry = NULL;

    TMO_SLL_Scan (&gSnmpNotifySll, pListNode, tTMO_SLL_NODE *)
    {
        pSnmpNotifyEntry = (tSnmpNotifyEntry *) pListNode;

        if (SNMPCompareImpliedOctetString
            (pNotifyName, (&pSnmpNotifyEntry->NotifyName)) == SNMP_LESSER)
        {
            return pSnmpNotifyEntry;
        }
    }
    return NULL;
}

/************************************************************************
 *  Function Name   : SNMPGenRandomIV
 *  Description     : Function to Generate Random Data 
 *  Input           : pu4Data - Data 
 *  Output          : None
 *  Returns         : None 
 ************************************************************************/

VOID
SNMPGenRandomIV (UINT4 *pu4Data)
{
    pu4Data[0] = (UINT4) RAND ();
    pu4Data[1] = (UINT4) RAND ();
}

/************************************************************************
 *  Function Name   : SNMPAddNotifySll
 *  Description     : Function to Add  a node in the list 
 *  Input           : pNode -  Node to be Add
 *  Output          : None
 *  Returns         : None 
 ************************************************************************/

VOID
SNMPAddNotifySll (tTMO_SLL_NODE * pNode)
{
    tSnmpNotifyEntry   *pCurrEntry = NULL;
    tSnmpNotifyEntry   *pInEntry = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pPrevLstNode = NULL;

    pInEntry = (tSnmpNotifyEntry *) pNode;
    TMO_SLL_Scan (&gSnmpNotifySll, pLstNode, tTMO_SLL_NODE *)
    {
        pCurrEntry = (tSnmpNotifyEntry *) pLstNode;
        if (SNMPCompareImpliedOctetString (&(pInEntry->NotifyName),
                                           &(pCurrEntry->NotifyName)) ==
            SNMP_LESSER)
        {
            break;
        }
        pPrevLstNode = pLstNode;
    }
    TMO_SLL_Insert (&gSnmpNotifySll, pPrevLstNode, pNode);

}

/************************************************************************
 *  Function Name   : SNMPDelNotifySll
 *  Description     : Function to Delete  a node in the list 
 *  Input           : pNode -  Node to be Add
 *  Output          : None
 *  Returns         : None 
 ************************************************************************/

VOID
SNMPDelNotifySll (tTMO_SLL_NODE * pNode)
{
    TMO_SLL_Delete (&gSnmpNotifySll, pNode);
    return;
}

/************************************************************************
 *  Function Name   : SNMPFillV3Params
 *  Description     : Function to fill the V3 parameters
 *  Input           : pSnmpUsmEntry - USM Entry
 *                    pContextName - Context Name
 *  Output          : None
 *  Returns         : pointer to v3 pdu Else NULL 
 ************************************************************************/

tSNMP_V3PDU        *
SNMPFillV3Params (tSnmpUsmEntry * pSnmpUsmEntry,
                  tSNMP_OCTET_STRING_TYPE * pContextName)
{
    UINT1              *pu1Data = NULL;
    tSNMP_V3PDU        *pV3Pdu = NULL;
    tContext           *pContext = NULL;

    if (pContextName != NULL)
    {
        pContext = VACMGetContext (pContextName);
        if (pContext == NULL)
        {
            SNMP_INR_UNAVIAL_CONTEXTS;
            SNMPTrace ("Not Available Context \n");
            return NULL;
        }
    }

    pu1Data = MemAllocMemBlk (gSnmpV3PduPoolId);
    if (pu1Data == NULL)
    {
        SNMPTrace ("Unable to Allocate memory for V3 Trap \n");
        return NULL;
    }

    MEMSET (pu1Data, SNMP_ZERO,
            (sizeof (tSNMP_V3PDU) + (7 * SNMP_MAX_OCTETSTRING_SIZE)));
    pV3Pdu = (tSNMP_V3PDU *) (VOID *) pu1Data;
    pu1Data += sizeof (tSNMP_V3PDU);
    pV3Pdu->MsgSecParam.MsgEngineID.pu1_OctetList = pu1Data;
    pu1Data += SNMP_MAX_OCTETSTRING_SIZE;
    pV3Pdu->MsgSecParam.MsgUsrName.pu1_OctetList = pu1Data;
    pu1Data += SNMP_MAX_OCTETSTRING_SIZE;
    pV3Pdu->MsgSecParam.MsgAuthParam.pu1_OctetList = pu1Data;
    pu1Data += SNMP_MAX_OCTETSTRING_SIZE;
    pV3Pdu->MsgSecParam.MsgPrivParam.pu1_OctetList = pu1Data;
    pu1Data += SNMP_MAX_OCTETSTRING_SIZE;
    pV3Pdu->MsgFlag.pu1_OctetList = pu1Data;
    pu1Data += SNMP_MAX_OCTETSTRING_SIZE;
    pV3Pdu->ContextName.pu1_OctetList = pu1Data;
    pu1Data += SNMP_MAX_OCTETSTRING_SIZE;
    pV3Pdu->ContextID.pu1_OctetList = pu1Data;
    if (pContextName != NULL)
    {
        SNMPCopyOctetString (&(pV3Pdu->ContextName), &(pContext->ContextName));
    }
    SNMPCopyOctetString (&(pV3Pdu->ContextID), &gSnmpEngineID);
    pV3Pdu->u4MsgMaxSize = MAX_PKT_LENGTH;
    SNMPCopyOctetString (&pV3Pdu->MsgSecParam.MsgEngineID,
                         &pSnmpUsmEntry->UsmUserEngineID);
    SNMPCopyOctetString (&pV3Pdu->MsgSecParam.MsgUsrName,
                         &pSnmpUsmEntry->UsmUserName);
    pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO] = 0x00;
    pV3Pdu->MsgFlag.i4_Length = 1;
    if (pSnmpUsmEntry->u4UsmUserAuthProtocol != SNMP_NO_AUTH)
    {
        pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO] = 0x01;
        pV3Pdu->MsgFlag.i4_Length = 1;
    }
    if (pSnmpUsmEntry->u4UsmUserPrivProtocol != SNMP_NO_PRIV)
    {
        pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO] = 0x03;
        pV3Pdu->MsgFlag.i4_Length = 1;
        SNMPGenRandomIV ((UINT4 *) (VOID *) pV3Pdu->MsgSecParam.
                         MsgPrivParam.pu1_OctetList);
        pV3Pdu->MsgSecParam.MsgPrivParam.i4_Length = 8;
    }
    pV3Pdu->u4MsgID = SNMPGetMsgID ();
    return pV3Pdu;
}

/************************************************************************
 *  Function Name   : SnmpSendColdStartTrap 
 *  Description     : Function to send ColdStart trap when snmp agent 
 *                    is reinitializing database itself. 
 *  Input           : None
 *  Output          : None
 *  Returns         : None
 ************************************************************************/

VOID
SnmpSendColdStartTrap ()
{
    tSNMP_OID_TYPE     *pColdtrapoid = NULL, *pSnmpTrapOid = NULL;
    tSNMP_VAR_BIND     *pSnmpColdVb = NULL;
    tSNMP_OID_TYPE     *pEnterOid = NULL, *pSnmpEnterOid = NULL;
    tSNMP_COUNTER64_TYPE u8Value;

    if (gSnmpStat.i4SnmpColdStartTraps == COLDTRAP_DISABLE)
    {
        return;
    }
    u8Value.msn = 0;
    u8Value.lsn = 0;
    pColdtrapoid = alloc_oid (AUTH_FAILURE_OID_LEN);
    if (pColdtrapoid == NULL)
    {
        SNMPTrace ("Unable to allocate memory for pColdtrapoid\n");
        return;
    }
    MEMCPY (pColdtrapoid->pu4_OidList, gau4coldStartOid,
            AUTH_FAILURE_OID_LEN * sizeof (UINT4));
    pSnmpTrapOid = alloc_oid (SNMP_TRAP_OID_LEN);
    if (pSnmpTrapOid == NULL)
    {
        SNMPTrace ("Unable to allocate memory for pSnmpTrapOid\n");
        FREE_OID (pColdtrapoid);
        return;
    }
    MEMCPY (pSnmpTrapOid->pu4_OidList, gau4snmpTrapOid,
            SNMP_TRAP_OID_LEN * sizeof (UINT4));

    pSnmpColdVb = SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                        SNMP_DATA_TYPE_OBJECT_ID,
                                        SNMP_ZERO, SNMP_ZERO, NULL,
                                        pColdtrapoid, u8Value);
    if (pSnmpColdVb == NULL)
    {
        SNMPTrace ("Failure in FormVarBind fn. for in pWarmTrapVb formation\n");
        FREE_OID (pColdtrapoid);
        FREE_OID (pSnmpTrapOid);
        return;
    }
    pEnterOid = alloc_oid (SNMP_TRAP_OID_LEN);
    if (pEnterOid == NULL)
    {
        SNMPTrace ("Unable to allocate memory for pEnterOid\n");
        FREE_SNMP_VARBIND_LIST (pSnmpColdVb);
        return;
    }
    MEMCPY (pEnterOid->pu4_OidList, gau4snmpTrapEnterpriseOid,
            SNMP_TRAP_OID_LEN * sizeof (UINT4));

    pSnmpEnterOid = alloc_oid (SNMP_OID_LEN);
    if (pSnmpEnterOid == NULL)
    {
        SNMPTrace ("Unable to allocate memory for pSnmpEnterOid\n");
        FREE_SNMP_VARBIND_LIST (pSnmpColdVb);
        FREE_OID (pEnterOid);
        return;
    }
    MEMCPY (pSnmpEnterOid->pu4_OidList, gau4snmpOid,
            SNMP_OID_LEN * sizeof (UINT4));

    pSnmpColdVb->pNextVarBind = SNMP_AGT_FormVarBind (pEnterOid,
                                                      SNMP_DATA_TYPE_OBJECT_ID,
                                                      SNMP_ZERO, SNMP_ZERO,
                                                      NULL, pSnmpEnterOid,
                                                      u8Value);
    if (pSnmpColdVb->pNextVarBind == NULL)
    {
        SNMPTrace ("Failure in FormVarbind fn. for forming pSnmpColdVb\n");
        FREE_SNMP_VARBIND_LIST (pSnmpColdVb);
        FREE_OID (pEnterOid);
        FREE_OID (pSnmpEnterOid);
        return;
    }
    pSnmpColdVb->pNextVarBind->pNextVarBind = NULL;
    SNMP_AGT_RIF_Notify_v2Trap (pSnmpColdVb);
    return;
}

/************************************************************************
 *  Function Name   : SnmpGetManagerTargetAddr
 *  Description     : Function to get Target address entry for the given
 *                    Manager IP address
 *  Input           : u4IpAddr      - Manager Ip Address
 *  Output          : pTgtAddrEntry - pointer to target addr table entry
 *  Returns         : Address to target address table Else NULL
 ************************************************************************/

tSnmpTgtAddrEntry  *
SnmpGetManagerTargetAddr (UINT4 u4IpAddr)
{
    UINT4               u4ManagerAddr = 0;
    tSnmpTgtAddrEntry  *pTgtAddrEntry = NULL;
#ifdef IP6_WANTED
    tSNMP_OCTET_STRING_TYPE *pIp6ManagerAddr = NULL;
#endif

    /* Scan over all Managers list. */
    if ((pTgtAddrEntry = SNMPGetFirstTgtAddrEntry ()) == NULL)
    {
        return NULL;
    }

    do
    {
        if (pTgtAddrEntry->Address.i4_Length == SNMP_IPADDR_LEN)
        {
            MEMCPY (&u4ManagerAddr, pTgtAddrEntry->Address.pu1_OctetList,
                    sizeof (UINT4));
            u4ManagerAddr = OSIX_NTOHL (u4ManagerAddr);
        }
        else if (pTgtAddrEntry->Address.i4_Length == SNMP_IP6ADDR_LEN)
        {
#ifdef IP6_WANTED
            /*Allocate octet string memory during first iteration.
             * Next iteration onwards re-initialize with
             * current pSnmpTgtAddrEntry->Address */

            if (pIp6ManagerAddr == NULL)
            {
                if ((pIp6ManagerAddr =
                     allocmem_octetstring (SNMP_MAX_OCTETSTRING_SIZE)) == NULL)
                {
                    SNMPTrace ("Memory allocation failed\n");
                    continue;
                }
            }
            MEMSET (pIp6ManagerAddr->pu1_OctetList, 0,
                    SNMP_MAX_OCTETSTRING_SIZE);
            SNMPCopyOctetString (pIp6ManagerAddr, &(pTgtAddrEntry->Address));
#endif
        }

        /* Currently only IPv4 address is handled as the global
         * value of manager address is only an IPv4 address
         */
        if (u4ManagerAddr == u4IpAddr)
        {
#ifdef IP6_WANTED
            free_octetstring (pIp6ManagerAddr);
            pIp6ManagerAddr = NULL;
#endif
            return pTgtAddrEntry;
        }

        pTgtAddrEntry = SNMPGetNextTgtAddrEntry (&(pTgtAddrEntry->AddrName));
    }                            /* end of do...while */
    while (pTgtAddrEntry != NULL);
#ifdef IP6_WANTED
    if (pIp6ManagerAddr != NULL)
    {
        free_octetstring (pIp6ManagerAddr);
        pIp6ManagerAddr = NULL;
    }
#endif
    return NULL;
}

/************************************************************************
 *  Function Name   : SNMP_AGT_RIF_Notify_UsrAppl_v2Trap 
 *  Description     : Function to send v2 trap/inform from protocol
 *  Input           : pVbList     - Variable Bind List Pointer
 *                    AppCallback - Callback function provided by
 *                                  application
 *                    pCookie     - pointer to memory in application
 *                                  that has information related to
 *                                  the notification being generated
 *  Output          : None
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/

INT4
SNMP_AGT_RIF_Notify_UsrAppl_v2Trap (tSNMP_VAR_BIND * pVbList,
                                    SnmpAppNotifCb AppCallback, VOID *pCookie)
{
    tSNMP_NORMAL_PDU   *pV2TrapPdu = NULL;
    tSNMP_TRAP_PDU     *pV1TrapPdu = NULL;
    tSNMP_VAR_BIND     *pV2VbList = NULL;

    if (pVbList == NULL)
    {
        SNMPTrace ("The input VbList is NULL\n");
        return OSIX_FAILURE;
    }
    pV2TrapPdu = MemAllocMemBlk (gSnmpNormalPduPoolId);
    if (pV2TrapPdu == NULL)
    {
        SNMPTrace ("Unable to allocate v2 Trap \n");
        FREE_SNMP_VARBIND_LIST (pVbList);
        return OSIX_FAILURE;
    }
    MEMSET (pV2TrapPdu, SNMP_ZERO, sizeof (tSNMP_NORMAL_PDU));
    if (MEMCMP (gau4snmpTrapOid, pVbList->pObjName->pu4_OidList,
                SNMP_TRAP_OID_LEN * 4) != SNMP_ZERO)
    {
        FREE_SNMP_VARBIND_LIST (pVbList);
        SNMPFreeV2Trap (pV2TrapPdu);
        SNMPTrace
            ("The First OID of the input VbList is not the SnmpTrapOid\n");
        return OSIX_FAILURE;
    }

    pV2TrapPdu->u4_RequestID = SNMPGetRequestID ();
    pV2TrapPdu->i4_ErrorStatus = SNMP_ERR_NO_ERROR;
    pV2TrapPdu->i4_ErrorIndex = SNMP_ZERO;
    pV2TrapPdu->i2_PduType = SNMP_PDU_TYPE_SNMPV2_TRAP;
    pV2TrapPdu->u4_Version = VERSION2;

    /* Update System Up Time as a first OID */
    pV2VbList = SNMPGetSysUpTime ();
    if (pV2VbList == NULL)
    {
        FREE_SNMP_VARBIND_LIST (pVbList);
        SNMPFreeV2Trap (pV2TrapPdu);
        SNMPTrace ("Failure while getting SysUpTime\n");
        return OSIX_FAILURE;
    }
    pV2VbList->pNextVarBind = pVbList;

    /* Varbind list is updated with the input varbind list */
    pV2TrapPdu->pVarBindList = pV2VbList;
    pV2TrapPdu->pVarBindEnd = pVbList;
    while (pVbList->pNextVarBind != NULL)
    {
        pV2TrapPdu->pVarBindEnd = pVbList->pNextVarBind;
        pVbList = pVbList->pNextVarBind;
    }
    pV2TrapPdu->pCommunityStr =
        allocmem_octetstring (SNMP_MAX_OCTETSTRING_SIZE);
    if (pV2TrapPdu->pCommunityStr == NULL)
    {
        SNMPTrace ("Unable to allocate Community for v2 trap\n");
        SNMPFreeV2Trap (pV2TrapPdu);
        return OSIX_FAILURE;
    }
    pV1TrapPdu = SNMPV2TrapToV1Trap (pV2TrapPdu);
    if (pV1TrapPdu == NULL)
    {
        SNMPTrace ("Unable to convert v2 trap to v1 \n");
        SNMPFreeV2Trap (pV2TrapPdu);
        return OSIX_FAILURE;
    }

    SNMPAgentSendTrap (pV2TrapPdu, pV1TrapPdu, AppCallback, pCookie, NULL,
                       NULL);
    SNMPFreeV2Trap (pV2TrapPdu);
    SNMPFreeV1Trap (pV1TrapPdu);

    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : SNMP_AGT_RIF_Notify_Trap_v1Usr
 *  Description     : Called from protocol for v1 agent
 *  Input           : pEnterpriseOid - Enterprise Oid Pinter
 *                    u4GenTrapType  - Generic Trap Type
 *                    u4SpecTrapType - Specific Trap Type
 *                    pVbList        - Pointer to Varbind List                    
 *                    AppCallback    - Callback function provided by
 *                                     application
 *                    pCookie        - pointer to memory in application
 *                                     that has information related to
 *                                     the notification being generated
 *  Output          : None 
 *  Returns         : None
 ************************************************************************/

VOID
SNMP_AGT_RIF_Notify_Trap_v1Usr (tSNMP_OID_TYPE * pEnterpriseOid,
                                UINT4 u4GenTrapType, UINT4 u4SpecTrapType,
                                tSNMP_VAR_BIND * pVbList,
                                SnmpAppNotifCb AppCallback, VOID *pCookie)
{
    tSNMP_TRAP_PDU     *pV1TrapPdu = NULL;
    tSNMP_NORMAL_PDU   *pV2TrapPdu = NULL;
    tSNMP_VAR_BIND     *pTempVb = NULL;

    SNMP_INR_IN_TRAP_SUCCESS;
    if (pVbList == NULL)
    {
        SNMPTrace ("The input VbList is NULL\n");
        return;
    }
    pTempVb = pVbList;
    while (pTempVb != NULL)
    {
        if (pTempVb->ObjValue.i2_DataType == SNMP_DATA_TYPE_COUNTER64)
        {
            SNMPTrace ("v1 Trap variable bind List with Couter64 object\n");
            FREE_OID (pEnterpriseOid);
            FREE_SNMP_VARBIND_LIST (pVbList);
            return;
        }
        pTempVb = pTempVb->pNextVarBind;
    }
    pV1TrapPdu = MemAllocMemBlk (gSnmpTrapPduPoolId);
    if (pV1TrapPdu == NULL)
    {
        SNMPTrace ("Unable to Allocate V1 Trap \n");
        FREE_OID (pEnterpriseOid);
        FREE_SNMP_VARBIND_LIST (pVbList);
        return;
    }
    MEMSET (pV1TrapPdu, SNMP_ZERO, sizeof (tSNMP_TRAP_PDU));
    pV1TrapPdu->i4_GenericTrap = (INT4) u4GenTrapType;
    pV1TrapPdu->i4_SpecificTrap = (INT4) u4SpecTrapType;

    /* Enterprise is Updated and input Enterprise OID is freed */
    pV1TrapPdu->pEnterprise = alloc_oid (MAX_OID_LENGTH);
    if (pV1TrapPdu->pEnterprise == NULL)
    {
        SNMPTrace ("Unable to Allocate Enterprise for Trap PDU\n");
        FREE_OID (pEnterpriseOid);
        FREE_SNMP_VARBIND_LIST (pVbList);
        SNMPFreeV1Trap (pV1TrapPdu);
        return;
    }
    if (u4GenTrapType != ENTERPRISE_SPECIFIC)
    {
        MEMCPY (pV1TrapPdu->pEnterprise->pu4_OidList, gau4snmpOid,
                SNMP_OID_LEN);
        pV1TrapPdu->pEnterprise->u4_Length = SNMP_OID_LEN;
    }
    else
    {
        if (pEnterpriseOid == NULL)
        {
            SNMPTrace ("Given Enterprise OID is NULL\n");
            FREE_SNMP_VARBIND_LIST (pVbList);
            SNMPFreeV1Trap (pV1TrapPdu);
            SNMP_INR_OUT_GEN_ERROR;
            return;
        }
        SNMPOIDCopy (pV1TrapPdu->pEnterprise, pEnterpriseOid);
    }
    FREE_OID (pEnterpriseOid);

    /* Agent IP Address is Updated from System Type */
    pV1TrapPdu->pAgentAddr = allocmem_octetstring (sizeof (UINT4));
    if (pV1TrapPdu->pAgentAddr == NULL)
    {
        SNMPTrace ("Unable to allocate agent ip address for v1 trap\n");
        FREE_SNMP_VARBIND_LIST (pVbList);
        SNMPFreeV1Trap (pV1TrapPdu);
        return;
    }
    pV1TrapPdu->pAgentAddr->i4_Length = sizeof (UINT4);

    /* Time Stamp is Updated */
    GET_TIME_TICKS (&(pV1TrapPdu->u4_TimeStamp));

    /* Pdu Type is updated as Trap */
    pV1TrapPdu->i2_PduType = SNMP_PDU_TYPE_TRAP;

    /* Varbind list is updated with the input varbind list */
    pV1TrapPdu->pVarBindList = pVbList;
    pV1TrapPdu->pVarBindEnd = pVbList;
    while (pVbList->pNextVarBind != NULL)
    {
        pV1TrapPdu->pVarBindEnd = pVbList;
        pVbList = pVbList->pNextVarBind;
    }
    pV1TrapPdu->pCommunityStr =
        allocmem_octetstring (SNMP_MAX_OCTETSTRING_SIZE);
    if (pV1TrapPdu->pCommunityStr == NULL)
    {
        SNMPTrace ("Unable to allocate Community for v1 trap \n");
        SNMPFreeV1Trap (pV1TrapPdu);
        return;
    }
    pV2TrapPdu = SNMPV1TrapToV2Trap (pV1TrapPdu, NULL, 0, SNMP_FALSE);
    if (pV2TrapPdu == NULL)
    {
        SNMPTrace ("Unable to convert v1 to v2 trap \n");
        SNMPFreeV1Trap (pV1TrapPdu);
        return;
    }

    SNMPAgentSendTrap (pV2TrapPdu, pV1TrapPdu, AppCallback, pCookie, NULL,
                       NULL);
    SNMPFreeV1Trap (pV1TrapPdu);
    SNMPFreeV2Trap (pV2TrapPdu);
}

/************************************************************************
 *  Function Name   : SNMPInitNotifyFilterProfileTable
 *  Description     : Initialise the Notify Table 
 *  Input           : None
 *  Output          : None
 *  Returns         : SNMP_SUCCESS/SNMP_FAILURE 
 ************************************************************************/
INT4
SNMPInitNotifyFilterProfileTable ()
{
    UINT4               u4Index = SNMP_ZERO;
    MEMSET (gSnmpFilterProfileTable, SNMP_ZERO,
            sizeof (gSnmpFilterProfileTable));
    MEMSET (gau1NotifyFilterName, SNMP_ZERO, sizeof (gau1NotifyFilterName));
    for (u4Index = SNMP_ZERO; u4Index < SNMP_MAX_TGT_PARAM_ENTRY; u4Index++)
    {
        gSnmpFilterProfileTable[u4Index].TargetParmasName.pu1_OctetList =
            gau1FilterTargetParamName[u4Index];
        gSnmpFilterProfileTable[u4Index].ProfileName.pu1_OctetList =
            gau1NotifyFilterName[u4Index];
        gSnmpFilterProfileTable[u4Index].i4Status = SNMP_INACTIVE;

    }
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : SNMPGetNotifyFilterProfileEntry
 *  Description     : Function to return the corresponding Notify Filter Entry 
 *  Input           : pNotifyFilterPrfName - Notify Filter Profile Name
 *  Output          : None
 *  Returns         : pointer to notification entry Else NULL
 ************************************************************************/

tSnmpNotifyFilterProfileTable *
SNMPGetNotifyFilterProfileEntry (tSNMP_OCTET_STRING_TYPE * pNotifyFilterPrfName)
{
    tTMO_SLL_NODE      *pLstNode = NULL;
    tSnmpNotifyFilterProfileTable *pSnmpNotifyFilterProfileEntry = NULL;
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    TMO_SLL_Scan (&gSnmpTgtParamSll, pLstNode, tTMO_SLL_NODE *)
    {
        pSnmpTgtParamEntry = (tSnmpTgtParamEntry *) pLstNode;
        if (SNMPCompareOctetString (pNotifyFilterPrfName,
                                    &(pSnmpTgtParamEntry->ParamName)) ==
            SNMP_EQUAL)
        {
            if (pSnmpTgtParamEntry->pFilterProfileTable != NULL)
            {
                pSnmpNotifyFilterProfileEntry =
                    (tSnmpNotifyFilterProfileTable *)
                    pSnmpTgtParamEntry->pFilterProfileTable;
                return pSnmpNotifyFilterProfileEntry;
            }
        }
    }
    return NULL;
}

/************************************************************************
 *  Function Name   : SNMPCreateNotifyFilterProfileEntry
 *  Description     : Function to Create a Notify Entry 
 *  Input           : pNotifyFilterName - FilterName
 *  Output          : None
 *  Returns         : pointer to notify entry Else NULL
 ************************************************************************/

tSnmpNotifyFilterProfileTable *
SNMPCreateNotifyFilterProfileEntry (tSNMP_OCTET_STRING_TYPE *
                                    pNotifyFilterPrfName)
{
    UINT4               u4Index = SNMP_ZERO;
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    if (SNMPGetNotifyFilterProfileEntry (pNotifyFilterPrfName) != NULL)
    {
        return NULL;
    }

    pSnmpTgtParamEntry = SNMPGetTgtParamEntry (pNotifyFilterPrfName);
    if (pSnmpTgtParamEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    for (u4Index = SNMP_ZERO; u4Index < SNMP_MAX_TGT_PARAM_ENTRY; u4Index++)
    {
        if (gSnmpFilterProfileTable[u4Index].i4Status == SNMP_INACTIVE)
        {
            gSnmpFilterProfileTable[u4Index].i4Status = UNDER_CREATION;
            gSnmpFilterProfileTable[u4Index].TargetParmasName.i4_Length =
                SNMP_ZERO;
            gSnmpFilterProfileTable[u4Index].ProfileName.i4_Length = SNMP_ZERO;
            gSnmpFilterProfileTable[u4Index].i4Storage =
                SNMP3_STORAGE_TYPE_NONVOLATILE;
            SNMPCopyOctetString
                (&(gSnmpFilterProfileTable[u4Index].TargetParmasName),
                 pNotifyFilterPrfName);

            pSnmpTgtParamEntry->pFilterProfileTable =
                (tSnmpNotifyFilterProfileTable
                 *) (&(gSnmpFilterProfileTable[u4Index]));

            return (&(gSnmpFilterProfileTable[u4Index]));
        }
    }
    return NULL;
}

/************************************************************************
 *  Function Name   : SNMPDeleteNotifyFilterProfileEntry
 *  Description     : Function to Delete a Notify Filter Profile Entry 
 *  Input           : pNotifyFilterName - FilterName
 *  Output          : None
 *  Returns         : SNMP_SUCCESS/SNMP_FAILURE 
 ************************************************************************/

INT4
SNMPDeleteNotifyFilterProfileEntry (tSNMP_OCTET_STRING_TYPE *
                                    pNotifyFilteriPrfName)
{
    tTMO_SLL_NODE      *pLstNode = NULL;
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    TMO_SLL_Scan (&gSnmpTgtParamSll, pLstNode, tTMO_SLL_NODE *)
    {
        pSnmpTgtParamEntry = (tSnmpTgtParamEntry *) pLstNode;
        if (SNMPCompareOctetString (pNotifyFilteriPrfName,
                                    &(pSnmpTgtParamEntry->ParamName)) ==
            SNMP_EQUAL)
        {
            pSnmpTgtParamEntry->pFilterProfileTable->i4Status = SNMP_INACTIVE;
            pSnmpTgtParamEntry->pFilterProfileTable = NULL;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}

/************************************************************************
 *  Function Name   : SNMPGetFirstNotifyFilterProfileEntry
 *  Description     : Function to return the first Notify FilterProfile Entry 
 *  Input           : None 
 *  Output          : None
 *  Returns         : pointer to first notification entry
 ************************************************************************/

tSnmpNotifyFilterProfileTable *
SNMPGetFirstNotifyFilterProfileEntry ()
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tSnmpNotifyFilterProfileTable *pSnmpNotifyFilterProfileEntry = NULL;
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;

    TMO_SLL_Scan (&gSnmpTgtParamSll, pListNode, tTMO_SLL_NODE *)
    {
        pSnmpTgtParamEntry = (tSnmpTgtParamEntry *) pListNode;

        if (pSnmpTgtParamEntry->pFilterProfileTable != NULL)
        {
            pSnmpNotifyFilterProfileEntry =
                (tSnmpNotifyFilterProfileTable *)
                pSnmpTgtParamEntry->pFilterProfileTable;
            return pSnmpNotifyFilterProfileEntry;
        }
    }
    return NULL;

}

/************************************************************************
 *  Function Name   : SNMPGetNextNotifyFilterProfileEntry
 *  Description     : Function to return the Next Notify Filter Profile Entry 
 *  Input           : pNotifyFilterName - FilterName 
 *  Output          : None
 *  Returns         : pointer to next notification entry Else NULL
 ************************************************************************/

tSnmpNotifyFilterProfileTable *
SNMPGetNextNotifyFilterProfileEntry (tSNMP_OCTET_STRING_TYPE *
                                     pNotifyFilterPrfName)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tSnmpNotifyFilterProfileTable *pSnmpNotifyFilterProfileEntry = NULL;
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;

    TMO_SLL_Scan (&gSnmpTgtParamSll, pListNode, tTMO_SLL_NODE *)
    {
        pSnmpTgtParamEntry = (tSnmpTgtParamEntry *) pListNode;

        if (SNMPCompareImpliedOctetString
            (pNotifyFilterPrfName, (&pSnmpTgtParamEntry->ParamName))
            == SNMP_LESSER)
        {
            if (pSnmpTgtParamEntry->pFilterProfileTable != NULL)
            {
                pSnmpNotifyFilterProfileEntry =
                    (tSnmpNotifyFilterProfileTable *)
                    pSnmpTgtParamEntry->pFilterProfileTable;
                return pSnmpNotifyFilterProfileEntry;
            }
        }
    }
    return NULL;
}

/*********************************************************************
*  Function Name : SNMPInitNotifyFilterTable
*  Description   : Initialises the Notify Filter Table 
*  Parameter(s)  : None 
*  Return Values : None 
*********************************************************************/

INT4
SNMPInitNotifyFilterTable ()
{
    UINT4               u4Index = SNMP_ZERO;
    MEMSET (gSnmpFilterTable, SNMP_ZERO, sizeof (gSnmpFilterTable));
    MEMSET (gau1FilterMask, SNMP_ZERO, sizeof (gau1FilterMask));
    MEMSET (gau4FilterSubOid, SNMP_ZERO, sizeof (gau4FilterSubOid));
    TMO_SLL_Init (&gSnmpFilterSll);
    for (u4Index = SNMP_ZERO; u4Index < SNMP_MAX_FILTER_TREE; u4Index++)
    {
        gSnmpFilterTable[u4Index].i4RowStatus = SNMP_INACTIVE;
        gSnmpFilterTable[u4Index].FilterMask.pu1_OctetList =
            gau1FilterMask[u4Index];
        gSnmpFilterTable[u4Index].FilterSubTree.pu4_OidList =
            gau4FilterSubOid[u4Index];
    }
    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : SNMPCreateNotifyFilterEntry 
*  Description   : Function to create an entry  in the Notify Filter Table 
*  Parameter(s)  : 
*  Return Values : 
*********************************************************************/

INT4
SNMPCreateNotifyFilterEntry (tSNMP_OCTET_STRING_TYPE * pName,
                             tSNMP_OID_TYPE * pSubTree)
{
    UINT4               u4Index = SNMP_ZERO;
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    tSnmpNotifyFilterProfileTable *pSnmpNotifyFilterProfileEntry = NULL;
    tTMO_SLL_NODE      *pListNode = NULL;
    if (SNMPGetNotifyFilterEntry (pName, pSubTree) != NULL)
    {
        return SNMP_FAILURE;
    }

    for (u4Index = SNMP_ZERO; u4Index < SNMP_MAX_FILTER_TREE; u4Index++)
    {
        if (gSnmpFilterTable[u4Index].i4RowStatus == SNMP_INACTIVE)
        {
            TMO_SLL_Scan (&gSnmpTgtParamSll, pListNode, tTMO_SLL_NODE *)
            {
                pSnmpTgtParamEntry = (tSnmpTgtParamEntry *) pListNode;

                pSnmpNotifyFilterProfileEntry =
                    (tSnmpNotifyFilterProfileTable *)
                    pSnmpTgtParamEntry->pFilterProfileTable;
                if (pSnmpNotifyFilterProfileEntry != NULL)
                {
                    if (SNMPCompareOctetString (pName,
                                                &
                                                (pSnmpNotifyFilterProfileEntry->
                                                 ProfileName)) == SNMP_EQUAL)
                    {

                        gSnmpFilterTable[u4Index].pFilterProfileName =
                            &(pSnmpNotifyFilterProfileEntry->ProfileName);
                        break;
                    }
                }
            }
            if ((gSnmpFilterTable[u4Index].pFilterProfileName == NULL))
            {
                return SNMP_FAILURE;

            }
            SNMPOIDCopy (&(gSnmpFilterTable[u4Index].FilterSubTree), pSubTree);
            MEMSET (gSnmpFilterTable[u4Index].FilterMask.pu1_OctetList, 1,
                    SNMP_MAX_OCTETSTRING_SIZE);
            gSnmpFilterTable[u4Index].FilterMask.i4_Length =
                (INT4) pSubTree->u4_Length;
            gSnmpFilterTable[u4Index].i4FilterType =
                SNMP_NOTIFY_FILTER_TYPE_INCLUDED;
            gSnmpFilterTable[u4Index].i4Storage =
                SNMP3_STORAGE_TYPE_NONVOLATILE;
            gSnmpFilterTable[u4Index].i4RowStatus = UNDER_CREATION;
            SNMPAddNotifyFilterTableSll (&(gSnmpFilterTable[u4Index].link));
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/*********************************************************************
*  Function Name : SNMPGetFirstNotifyFilterEntry 
*  Description   : Function to get the first entry Notify Filter Profile table 
*  Parameter(s)  : None 
*  Return Values : None 
*********************************************************************/

tSnmpNotifyFilterTable *
SNMPGetFirstNotifyFilterEntry ()
{
    tSnmpNotifyFilterTable *pFirstSnmpNotifyFilterTable = NULL;
    pFirstSnmpNotifyFilterTable =
        (tSnmpNotifyFilterTable *) TMO_SLL_First (&gSnmpFilterSll);
    return pFirstSnmpNotifyFilterTable;
}

/*********************************************************************
*  Function Name : SNMPGetNextNotifyFilterEntry
*  Description   : Function to get the Next entry from Notify Filter table 
*  Parameter(s)  : pName    - Tree Name
                   pSubTree - Subtree Oid   
*  Return Values : SUCCESS/NULL 
*********************************************************************/

tSnmpNotifyFilterTable *
SNMPGetNextNotifyFilterEntry (tSNMP_OCTET_STRING_TYPE * pName,
                              tSNMP_OID_TYPE * pSubTree)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tSnmpNotifyFilterTable *pFilterNotifyEntry = NULL;
    INT4                i4Result = 0;
    UINT4               u4Count = 0;

    TMO_SLL_Scan (&gSnmpFilterSll, pListNode, tTMO_SLL_NODE *)
    {
        pFilterNotifyEntry = (tSnmpNotifyFilterTable *) pListNode;

        i4Result =
            SNMPCompareOctetString (pName,
                                    pFilterNotifyEntry->pFilterProfileName);

        if (i4Result == SNMP_GREATER)
        {
            continue;
        }
        else if (i4Result == SNMP_LESSER)
        {
            return pFilterNotifyEntry;
        }
        if (OIDCompare (*pSubTree, pFilterNotifyEntry->FilterSubTree,
                        &u4Count) == SNMP_LESSER)
        {
            return pFilterNotifyEntry;
        }
    }
    return NULL;
}

/*********************************************************************
*  Function Name : SNMPAddNotifyFilterTableSll 
*  Description   : Function to add node to the  list
*  Parameter(s)  : pNode - Node to be add 
*  Return Values : None 
*********************************************************************/

VOID
SNMPAddNotifyFilterTableSll (tTMO_SLL_NODE * pNode)
{
    tSnmpNotifyFilterTable *pCurProfile = NULL;
    tSnmpNotifyFilterTable *pInProfile = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pPrevNode = NULL;
    UINT4               u4Match = SNMP_ZERO;
    UINT4               u4RetVal = SNMP_ZERO;
    pInProfile = (tSnmpNotifyFilterTable *) pNode;

    TMO_SLL_Scan (&gSnmpFilterSll, pLstNode, tTMO_SLL_NODE *)
    {
        pCurProfile = (tSnmpNotifyFilterTable *) pLstNode;
        u4RetVal =
            (UINT4) SNMPCompareOctetString (pInProfile->pFilterProfileName,
                                            pCurProfile->pFilterProfileName);
        if (u4RetVal == SNMP_LESSER)
        {
            break;
        }
        else if (u4RetVal == SNMP_GREATER)
        {
            pPrevNode = pLstNode;
            continue;
        }

        u4RetVal =
            OIDCompare (pInProfile->FilterSubTree, pCurProfile->FilterSubTree,
                        &u4Match);
        if (u4RetVal == SNMP_LESSER)
        {
            break;
        }
        pPrevNode = pLstNode;
    }
    TMO_SLL_Insert (&gSnmpFilterSll, pPrevNode, pNode);
}

/*********************************************************************
*  Function Name : snmpNotifyFilterGetLongestMatch 
*  Description   : Function to get longest match in snmp filter table 
*  Parameter(s)  : pName      - Profile Name
                   pInObject  - OID to be searched
*  Return Values : SNMP_SUCCESS/SNMP_FAILURE  
*********************************************************************/

tSnmpNotifyFilterTable *
snmpNotifyFilterGetLongestMatch (tSNMP_OCTET_STRING_TYPE * pName,
                                 tSNMP_OID_TYPE * pInObject)
{

    UINT4               u4Match = SNMP_ZERO;
    tSnmpNotifyFilterTable *pFilterNotifyEntry = NULL;
    tSnmpNotifyFilterTable *pLongEntry = NULL;
    tTMO_SLL_NODE      *pListNode = NULL;
    INT4                i4Found = SNMP_ZERO;
    INT4                i4MaskPos = SNMP_ZERO;
    INT4                i4OidPos = SNMP_ZERO;

    TMO_SLL_Scan (&gSnmpFilterSll, pListNode, tTMO_SLL_NODE *)
    {
        pFilterNotifyEntry = (tSnmpNotifyFilterTable *) pListNode;
        if ((SNMPCompareOctetString
             (pFilterNotifyEntry->pFilterProfileName, pName) == SNMP_EQUAL)
            && pInObject->u4_Length >=
            pFilterNotifyEntry->FilterSubTree.u4_Length)
        {
            i4MaskPos = 0;
            i4OidPos = 0;
            i4Found = 1;
            for (i4OidPos = 0; ((UINT4) i4OidPos <
                                pFilterNotifyEntry->FilterSubTree.u4_Length);
                 i4OidPos++)
            {
                if ((pFilterNotifyEntry->FilterMask.pu1_OctetList[i4MaskPos]) !=
                    0)
                {
                    if (pInObject->pu4_OidList[i4OidPos] !=
                        pFilterNotifyEntry->FilterSubTree.pu4_OidList[i4OidPos])
                    {
                        i4Found = 0;
                    }
                }
                i4MaskPos++;
            }
            if (i4Found)
            {
                if ((pLongEntry == NULL
                     || pFilterNotifyEntry->FilterSubTree.u4_Length >
                     pLongEntry->FilterSubTree.u4_Length)
                    || (pFilterNotifyEntry->FilterSubTree.u4_Length ==
                        pLongEntry->FilterSubTree.u4_Length
                        && OIDCompare (pFilterNotifyEntry->FilterSubTree,
                                       pLongEntry->FilterSubTree,
                                       &u4Match) != SNMP_LESSER))
                {
                    pLongEntry = pFilterNotifyEntry;
                }
            }
        }
    }
    return pLongEntry;
}

/*********************************************************************
*  Function Name : SNMPGetNotifyFilterEntry 
*  Description   : Function to get the snmp filter Table entry 
*  Parameter(s)  : pName    - Profile Name
                   pSubTree - OID 
*  Return Values : NULL/pFilterNotifyEntry 
*********************************************************************/

tSnmpNotifyFilterTable *
SNMPGetNotifyFilterEntry (tSNMP_OCTET_STRING_TYPE * pName,
                          tSNMP_OID_TYPE * pSubTree)
{
    tSnmpNotifyFilterTable *pFilterNotifyEntry = NULL;
    tTMO_SLL_NODE      *pListNode = NULL;
    UINT4               u4Match = SNMP_ZERO;

    TMO_SLL_Scan (&gSnmpFilterSll, pListNode, tTMO_SLL_NODE *)
    {
        pFilterNotifyEntry = (tSnmpNotifyFilterTable *) pListNode;
        if (SNMPCompareOctetString
            (pFilterNotifyEntry->pFilterProfileName, pName) == SNMP_EQUAL)
        {
            if (OIDCompare (pFilterNotifyEntry->FilterSubTree, *pSubTree,
                            &u4Match) == SNMP_EQUAL)
            {
                return pFilterNotifyEntry;
            }
        }
    }
    return NULL;
}

/*********************************************************************
*  Function Name : SNMPDeleteNotifyFilterEntry 
*  Description   : Function to delete an entry from snmp filter table
*  Parameter(s)  : pName    - Profile Name
                   pSubTree - OID 
*  Return Values : SNMP_SUCCESS/SNMP_FAILURE 
*********************************************************************/

INT4
SNMPDeleteNotifyFilterEntry (tSNMP_OCTET_STRING_TYPE * pName,
                             tSNMP_OID_TYPE * pSubTree)
{
    UINT4               u4RetVal = 0;
    tTMO_SLL_NODE      *pListNode = NULL;
    tSnmpNotifyFilterTable *pFilterNotifyEntry = NULL;
    UINT4               u4Match = SNMP_ZERO;
    TMO_SLL_Scan (&gSnmpFilterSll, pListNode, tTMO_SLL_NODE *)
    {
        pFilterNotifyEntry = (tSnmpNotifyFilterTable *) pListNode;
        if (SNMPCompareOctetString
            (pFilterNotifyEntry->pFilterProfileName, pName) == SNMP_EQUAL)
        {
            u4RetVal =
                OIDCompare (pFilterNotifyEntry->FilterSubTree, *pSubTree,
                            &u4Match);
            if (pSubTree->u4_Length == u4Match)
            {
                pFilterNotifyEntry->i4RowStatus = SNMP_INACTIVE;
                SNMPDelNotifyFilterEntrySll (&(pFilterNotifyEntry->link));
                return SNMP_SUCCESS;
            }
        }
    }
    UNUSED_PARAM (u4RetVal);
    return SNMP_FAILURE;
}

/*********************************************************************
*  Function Name : SNMPDelNotifyFilterEntrySll 
*  Description   : Delete the node for list
*  Parameter(s)  : pNode - Node to be deleted
*  Return Values : None
*********************************************************************/

VOID
SNMPDelNotifyFilterEntrySll (tTMO_SLL_NODE * pNode)
{
    TMO_SLL_Delete (&gSnmpFilterSll, pNode);
    return;
}

/*********************************************************************
 * *  Function Name : SNMPNotifyFilterCheckProfileName
 * *  Description   : Function to check the entry from Filter ProfileTable
 * *  Parameter(s)  : pName    - Filter Profile Name
 * *  Return Values : SNMP_SUCCESS/SNMP_FAILURE
 * *********************************************************************/
INT4
SNMPNotifyFilterCheckProfileName (tSNMP_OCTET_STRING_TYPE * pName)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    tSnmpNotifyFilterProfileTable *pSnmpNotifyFilterProfileEntry = NULL;
    UINT4               u4MatchFound = SNMP_ZERO;

    TMO_SLL_Scan (&gSnmpTgtParamSll, pListNode, tTMO_SLL_NODE *)
    {
        pSnmpTgtParamEntry = (tSnmpTgtParamEntry *) pListNode;

        pSnmpNotifyFilterProfileEntry =
            (tSnmpNotifyFilterProfileTable *)
            pSnmpTgtParamEntry->pFilterProfileTable;

        if (pSnmpNotifyFilterProfileEntry != NULL)
        {

            if (SNMPCompareOctetString (pName,
                                        &
                                        (pSnmpNotifyFilterProfileEntry->
                                         ProfileName)) == SNMP_EQUAL)
            {
                u4MatchFound = SNMP_ONE;
                break;
            }
        }
    }

    if (u4MatchFound == SNMP_ONE)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/*********************************************************************
*  Function Name : SNMPNotifyFilterAccessAllowed 
*  Description   : Check the Filter access for particular OID 
*  Parameter(s)  : pSnmpNotifyFilterProfileEntry   - Filter Information 
*                  i4FilterType  - Filter Type
*                  pOID        - OID
*  Return Values : SNMP_SUCCESS/SNMP_FAILURE 
*********************************************************************/

INT4
SNMPNotifyFilterAccessAllowed (tSnmpNotifyFilterProfileTable *
                               pSnmpNotifyFilterProfileEntry,
                               tSNMP_OID_TYPE * pOID)
{
    tSNMP_OCTET_STRING_TYPE *pFilterPrfName = NULL;
    tSnmpNotifyFilterTable *pFilterTable = NULL;

    if (pSnmpNotifyFilterProfileEntry == NULL)
    {
        return SNMP_SUCCESS;
    }

    pFilterPrfName = &(pSnmpNotifyFilterProfileEntry->ProfileName);

    if (pFilterPrfName == NULL || pFilterPrfName->i4_Length == SNMP_ZERO)
    {
        return SNMP_FAILURE;
    }

    pFilterTable = snmpNotifyFilterGetLongestMatch (pFilterPrfName, pOID);

    if (MEMCMP (gau4snmpTrapOid, pOID->pu4_OidList,
                SNMP_TRAP_OID_LEN * 4) == SNMP_ZERO)
    {

        /* As per RFC 3414,A notification can be sent
           1 if the NOTIFICATION-TYPE OBJECT IDENTIFIER of the notification (this
           * is the value of the element of the variable bindings whose name is
           * snmpTrapOID.0, i.e., the second variable binding) is specifically
           * included,  
           2 And none of the object instances to be included in the
           * variable-bindings of the notification are specifically excluded by
           the matching entries. */

        if ((pFilterTable == NULL) ||
            (pFilterTable->i4FilterType == SNMP_NOTIFY_FILTER_TYPE_EXCLUDED))
        {
            return SNMP_FAILURE;
        }
        else
        {
            return SNMP_SUCCESS;
        }
    }

    else
    {
        if (pFilterTable == NULL ||
            (pFilterTable->i4FilterType == SNMP_NOTIFY_FILTER_TYPE_INCLUDED))
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }

    }
}

/************************************************************************
 *  Function Name   : SNMPInitTrapFilterTable
 *  Description     : Initialise the Filter Table
 *  Input           : None
 *  Output          : None
 *  Returns         : SNMP_SUCCESS/SNMP_FAILURE
 ************************************************************************/
INT4
SNMPInitTrapFilterTable ()
{

    gSnmpTrapFilterTable =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tSnmpTrapFilterEntry, RbNode),
                              SnmpTrapFilterTblCmpFn);
    if (gSnmpTrapFilterTable == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : SnmpTrapFilterTblCmpFn
 *  Description     : This function is used as a Compare function in the
 *                    RBTree
 *  Input           : e1 , e2 -  Elements of RBTree
 *  Output          : None
 *  Returns         : SNMP_SUCCESS/SNMP_FAILURE
 ************************************************************************/
INT4
SnmpTrapFilterTblCmpFn (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tSnmpTrapFilterEntry *pSnmpTrapFilterEntry1 = NULL;
    tSnmpTrapFilterEntry *pSnmpTrapFilterEntry2 = NULL;
    INT4                i4RetVal = SNMP_EQUAL;

    pSnmpTrapFilterEntry1 = (tSnmpTrapFilterEntry *) pRBElem1;
    pSnmpTrapFilterEntry2 = (tSnmpTrapFilterEntry *) pRBElem2;
    i4RetVal = SNMPCompareRootOID (&(pSnmpTrapFilterEntry1->TrapFilterOID),
                                   &(pSnmpTrapFilterEntry2->TrapFilterOID));
    if (i4RetVal == SNMP_LESSER)
    {
        i4RetVal = SNMP_RB_CMP_LESSER;
    }
    else if (i4RetVal == SNMP_GREATER)
    {
        i4RetVal = SNMP_RB_CMP_GREATER;
    }
    return i4RetVal;
}

/*********************************************************************
*  Function Name : SNMPTrapCheckTrapAllowed 
*  Description   : Check the Filter access for particular OID 
*  Parameter(s)  : pOID        - OID
*  Return Values : SNMP_SUCCESS/SNMP_FAILURE 
*********************************************************************/

INT4
SNMPTrapCheckTrapAllowed (tSNMP_OID_TYPE * pOID)
{

    tSnmpTrapFilterEntry *pSnmpTrapFilterEntry = NULL;
    tSnmpTrapFilterEntry SnmpTrapFilterEntry;

    /* Validate OID */

    MEMSET (&SnmpTrapFilterEntry, SNMP_ZERO, sizeof (tSnmpTrapFilterEntry));
    SnmpTrapFilterEntry.TrapFilterOID.pu4_OidList =
        (UINT4 *) MemAllocMemBlk (gSnmpOidListPoolId);

    if (SnmpTrapFilterEntry.TrapFilterOID.pu4_OidList == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (SnmpTrapFilterEntry.TrapFilterOID.pu4_OidList, SNMP_ZERO,
            sizeof (tSnmpOidListBlock));

    SNMPCopyOid (&(SnmpTrapFilterEntry.TrapFilterOID), pOID);

    pSnmpTrapFilterEntry = (tSnmpTrapFilterEntry *)
        RBTreeGet (gSnmpTrapFilterTable, (tRBElem *) & SnmpTrapFilterEntry);
    if (pSnmpTrapFilterEntry == NULL)
    {
        MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *)
                            SnmpTrapFilterEntry.TrapFilterOID.pu4_OidList);
        return SNMP_SUCCESS;
    }
    if (MEMCMP (pOID->pu4_OidList,
                pSnmpTrapFilterEntry->TrapFilterOID.pu4_OidList,
                pOID->u4_Length) == 0)
    {
        MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *)
                            SnmpTrapFilterEntry.TrapFilterOID.pu4_OidList);
        return SNMP_FAILURE;
    }
    MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *)
                        SnmpTrapFilterEntry.TrapFilterOID.pu4_OidList);
    return SNMP_SUCCESS;
}

#endif /* SNMPTRAP_C */
/********************  END OF FILE   ***************************************/
