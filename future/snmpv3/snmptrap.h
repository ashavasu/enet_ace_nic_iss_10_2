/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: snmptrap.h,v 1.9 2015/04/28 12:35:02 siva Exp $
 *
 * Description: Macros and proto types for trap 
 *******************************************************************/
#ifndef _SNMPTRAP_H
#define _SNMPTRAP_H

#ifdef SNMPTRAP_C
UINT4 gau4sysUpTimeOid[]          = {1, 3, 6, 1, 2, 1, 1, 3, 0};
UINT4 gau4snmpTrapsOid[]          = {1, 3, 6, 1, 6, 3, 1, 1, 5};
UINT4 gau4coldStartOid[]          = {1, 3, 6, 1, 6, 3, 1, 1, 5, 1};
UINT4 gau4warmStartOid[]          = {1, 3, 6, 1, 6, 3, 1, 1, 5, 2};
UINT4 gau4linkDownOid[]           = {1, 3, 6, 1, 6, 3, 1, 1, 5, 3};
UINT4 gau4linkUpOid[]             = {1, 3, 6, 1, 6, 3, 1, 1, 5, 4};
UINT4 gau4authFailureOid[]        = {1, 3, 6, 1, 6, 3, 1, 1, 5, 5};
UINT4 gau4egpNeighborLossOid[]    = {1, 3, 6, 1, 6, 3, 1, 1, 5, 6};
UINT4 gau4snmpTrapOid[]           = {1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0};
UINT4 gau4snmpTrapEnterpriseOid[] = {1, 3, 6, 1, 6, 3, 1, 1, 4, 3, 0};

UINT4 gau4snmpTrapAddressOid[]    = {1, 3, 6, 1, 6, 3, 18, 1, 3, 0};
UINT4 gau4snmpTrapCommunityOid[]  = {1, 3, 6, 1, 6, 3, 18, 1, 4, 0};

UINT4 gau4snmpOid[]               = {1, 3, 6, 1, 2, 1, 11};

tSnmpNotifyEntry gSnmpNotifyTable[MAX_TRAP_TABLE_ENTRY]; 
tTMO_SLL         gSnmpNotifySll;
tRBTree gSnmpTrapFilterTable;

#else

extern UINT4 gau4sysUpTimeOid[];
extern UINT4 gau4snmpTrapsOid[];
extern UINT4 gau4coldStartOid[];
extern UINT4 gau4warmStartOid[];
extern UINT4 gau4linkDownOid[];
extern UINT4 gau4linkUpOid[] ;
extern UINT4 gau4authFailureOid[];
extern UINT4 gau4egpNeighborLossOid[];
extern UINT4 gau4snmpTrapOid[]   ;
extern UINT4 gau4snmpTrapEnterpriseOid[];
extern UINT4 gau4snmpTrapAddressOid[] ;
extern UINT4 gau4snmpTrapCommunityOid[];
extern UINT4 gau4snmpOid[];

#endif /* SNMPTRAP_C */

#define SNMP_OID_LEN               7
#define SYSUPTIME_OID_LEN          9
#define AUTH_FAILURE_OID_LEN       10

#define TRAP_OID_LEN               11
#define ENTERPRISE_OID_LEN         11
#define TRAP_ADDRESS_OID_LEN       10
#define TRAP_COMMUNITY_OID_LEN     10

#define FREE_OCTET_STRING(octet_str) free_octetstring(octet_str)

#define FREE_OID(oid)   free_oid(oid)

#ifndef SNMPTRAP_C 
#define FREE_SNMP_VARBIND(vb)\
        if (vb != NULL)\
        {\
           FREE_OID(vb->pObjName);\
           if(vb->ObjValue.pOctetStrValue != NULL)\
              FREE_OCTET_STRING(vb->ObjValue.pOctetStrValue);\
           if(vb->ObjValue.pOidValue != NULL)\
              FREE_OID(vb->ObjValue.pOidValue);\
           vb->ObjValue.pOidValue = NULL;\
           vb->ObjValue.pOctetStrValue = NULL;\
           MemReleaseMemBlock (gSnmpVarBindPoolId, (UINT1 *)(vb));\
        }
#endif

#define FREE_SNMP_VARBIND_LIST SNMP_free_snmp_vb_list

#define SNMPCopyVarBind(pVarBind) SNMPFormVarBind (pVarBind->pObjName,\
         pVarBind->ObjValue.i2_DataType,\
         pVarBind->ObjValue.u4_ULongValue,\
         pVarBind->ObjValue.i4_SLongValue,\
         pVarBind->ObjValue.pOctetStrValue,\
         pVarBind->ObjValue.pOidValue,\
         pVarBind->ObjValue.u8_Counter64Value)

#define SNMPFreeV2Trap(pPdu) if(pPdu != NULL) {\
                               FREE_OCTET_STRING(pPdu->pCommunityStr); \
                               FREE_SNMP_VARBIND_LIST(pPdu->pVarBindList);\
                               MemReleaseMemBlock(gSnmpNormalPduPoolId, (UINT1 *)pPdu);}


#define SNMPFreeV1Trap(pPdu) if(pPdu != NULL) {\
                               FREE_OCTET_STRING(pPdu->pCommunityStr); \
                               FREE_OID(pPdu->pEnterprise);\
                               FREE_OCTET_STRING(pPdu->pAgentAddr);\
                               FREE_SNMP_VARBIND_LIST(pPdu->pVarBindList);\
                               MemReleaseMemBlock (gSnmpTrapPduPoolId, (UINT1 *) pPdu);}

#define SNMPFreeV3Params(x) MemReleaseMemBlock (gSnmpV3PduPoolId, (UINT1 *)x)

#ifdef SNMPTRAP_C
tSnmpNotifyFilterProfileTable gSnmpFilterProfileTable[SNMP_MAX_TGT_PARAM_ENTRY]; 


tSnmpNotifyFilterTable gSnmpFilterTable[SNMP_MAX_FILTER_TREE]; 
tTMO_SLL     gSnmpFilterSll;
#else
extern tSnmpNotifyFilterTable gSnmpFilterTable[];
extern tSnmpNotifyFilterProfileTable gSnmpFilterProfileTable[];
extern tTMO_SLL     gSnmpFilterSll;
#endif

VOID SNMPAddNotifySll(tTMO_SLL_NODE *pNode);
VOID SNMPDelNotifySll(tTMO_SLL_NODE *pNode);
         

VOID
SNMP_Notify_Format_Send (tSNMP_OID_TYPE *,UINT4,UINT4,tSNMP_VAR_BIND *,
                         tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE *);

VOID SNMPAgentSendTrap(tSNMP_NORMAL_PDU *, tSNMP_TRAP_PDU *,
                       SnmpAppNotifCb, VOID *, tSNMP_OCTET_STRING_TYPE *,
                       tSNMP_OCTET_STRING_TYPE *);

tSNMP_OID_TYPE * SNMPGetEnterpriseOid (tSNMP_NORMAL_PDU *, INT4 *,
                                       INT4 *, UINT1 *);
tSNMP_TRAP_PDU * SNMPV2TrapToV1Trap (tSNMP_NORMAL_PDU *);
INT4 SNMPTrapCheckTrapAllowed (tSNMP_OID_TYPE * pOID);
#endif /* _SNMPTRAP_H */
