/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: snmmpdlw.h,v 1.4 2015/04/28 12:35:02 siva Exp $
*
* Description: Proto types for snmp stats Low Level  Routines
*********************************************************************/
#ifndef _SNMMPDLW_H
#define _SNMMPDLW_H
/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetSnmpUnknownSecurityModels ARG_LIST((UINT4 *));

INT1
nmhGetSnmpInvalidMsgs ARG_LIST((UINT4 *));

INT1
nmhGetSnmpUnknownPDUHandlers ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetSnmpEngineID ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetSnmpEngineBoots ARG_LIST((INT4 *));

INT1
nmhGetSnmpEngineTime ARG_LIST((INT4 *));

INT1
nmhGetSnmpEngineMaxMessageSize ARG_LIST((INT4 *));
#endif
