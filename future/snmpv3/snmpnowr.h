/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: snmpnowr.h,v 1.5 2015/04/28 12:35:02 siva Exp $
*
* Description: Proto types for snmp notify table wrapper  Routines
*********************************************************************/
#ifndef _SNMPNOWR_H
#define _SNMPNOWR_H
INT4 GetNextIndexSnmpNotifyTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterSNMPNO(VOID);

VOID UnRegisterSNMPNO(VOID);
INT4 SnmpNotifyNameGet(tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyTagGet(tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyTypeGet(tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyTagSet(tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyTypeSet(tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyTagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);




INT4 GetNextIndexSnmpNotifyFilterProfileTable(tSnmpIndex *, tSnmpIndex *);
INT4 SnmpNotifyFilterProfileNameGet(tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyFilterProfileStorTypeGet(tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyFilterProfileRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyFilterProfileNameSet(tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyFilterProfileStorTypeSet(tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyFilterProfileRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyFilterProfileNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyFilterProfileStorTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyFilterProfileRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyFilterProfileTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 GetNextIndexSnmpNotifyFilterTable(tSnmpIndex *, tSnmpIndex *);
INT4 SnmpNotifyFilterSubtreeGet(tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyFilterMaskGet(tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyFilterTypeGet(tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyFilterStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyFilterRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyFilterMaskSet(tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyFilterTypeSet(tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyFilterStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyFilterRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyFilterMaskTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyFilterTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyFilterStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyFilterRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpNotifyFilterTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);




#endif /* _SNMPNOWR_H */
