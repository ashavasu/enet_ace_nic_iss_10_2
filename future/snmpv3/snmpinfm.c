/* $Id: snmpinfm.c,v 1.17 2016/03/18 13:04:29 siva Exp $ */

#include "snmpcmn.h"
#include "snmputil.h"
#include "fssocket.h"
#include "trace.h"
#include "snprxtrn.h"

tInformMsgList      gInformMsgList;
extern UINT4        lrmGlobalTrace;

/************************************************************************
 *  Function Name   : SNMPSendInformToManager 
 *  Description     : Function to open a socket and send the inform
 *                    to manager                                     
 *  Input           : pu1Msg            - Data Pointer
 *                    i4MsgLen          - Message length
 *                    u4ManagerAddr     - Trap Manager IP Address
 *                    pSnmpTgtAddrEntry - pointer to Target addr Entry
 *                    pSnmpNotifyEntry  - pointer to Notification Entry
 *                    u4ReqId           - Initial Inform Request Identifier
 *                    u4PrevReqId       - Previous request identifier
 *                    u4Retry           - Flag that indicates if the 
 *                                        message is transmitted for the
 *                                        FIRST time or RE-TRANSMITTED
 *                    pInformPdu        - Pointer to the Inform PDU 
 *                    AppCallback       - Callback function provided by
 *                                        application
 *                    pCookie           - pointer to memory in application
 *                                        that has information related to
 *                                        the notification being generated
 *  Output          : None
 *  Returns         : None
 ************************************************************************/

UINT4
SNMPSendInformToManager (UINT1 *pu1Msg, INT4 i4MsgLen,
                         tSnmpTgtAddrEntry * pSnmpTgtAddrEntry,
                         tSnmpNotifyEntry * pSnmpNotifyEntry,
                         UINT4 u4ReqId, UINT4 u4PrevReqId, UINT4 u4Retry,
                         tSNMP_NORMAL_PDU * pInformPdu,
                         SnmpAppNotifCb AppCallback, VOID *pCookie)
{
    UINT4               u4Status = SNMP_SUCCESS;
    UINT4               u4ManagerAddr = 0;
    struct sockaddr_in  SockAddr;
    tSNMP_NORMAL_PDU   *pInformDupPdu = NULL;
    tSnmpInfmStatusInfo SnmpInfmStatusInfo;

    MEMSET (&SockAddr, 0, sizeof (SockAddr));
    MEMSET (&SnmpInfmStatusInfo, 0, sizeof (tSnmpInfmStatusInfo));
    MEMCPY (&u4ManagerAddr,
            pSnmpTgtAddrEntry->Address.pu1_OctetList, SNMP_IPADDR_LEN);

    /* Get the Target IpAddress */
    u4ManagerAddr = OSIX_NTOHL (u4ManagerAddr);

    SockAddr.sin_family = AF_INET;

    SockAddr.sin_port = OSIX_HTONS (pSnmpTgtAddrEntry->u2Port);

    SockAddr.sin_addr.s_addr = OSIX_HTONL (u4ManagerAddr);

    if (sendto (gi4Socket, pu1Msg, i4MsgLen, 0,
                (struct sockaddr *) &SockAddr, sizeof (SockAddr)) == -1)
    {
        perror (":");
        SNMP_INR_OUT_GEN_ERROR;
        SNMPTrace ("SNMP: Unable to send Notification to manager\n");
        if (AppCallback != NULL)
        {
            SnmpInfmStatusInfo.pAppCookie = pCookie;
            SnmpInfmStatusInfo.u4InfmReqId = u4ReqId;

            MEMCPY (&SnmpInfmStatusInfo.u4MgrAddr,
                    pSnmpTgtAddrEntry->Address.pu1_OctetList, SNMP_IPADDR_LEN);

            SnmpInfmStatusInfo.u2Port = pSnmpTgtAddrEntry->u2Port;

            SnmpInfmStatusInfo.u4AckStatus = SNMP_INFORM_NOT_SENT;
            /* The manager address passed to this function is in network 
             * byte order */
            (*AppCallback) (&SnmpInfmStatusInfo);
            u4Status = SNMP_FAILURE;
        }
    }
    else
    {
        SNMP_INR_INFORM_REQTS;
        SNMP_INR_INFORM_AWAIT_ACK;
        SNMP_MGR_INR_INFORM_REQTS (pSnmpTgtAddrEntry);
        SNMP_MGR_INR_INFORM_AWAIT_ACK (pSnmpTgtAddrEntry);

        /* First Time Transmission */
        if ((u4Retry == SNMP_FIRST_TRANSMIT) && (pInformPdu != NULL))
        {
            /* Copy the INFORM PDU for re-transmission purpose */
            pInformDupPdu = (tSNMP_NORMAL_PDU *) SNMPCopyInformPdu (pInformPdu);

            if (pInformDupPdu == NULL)
            {
                SNMPTrace ("Duplicating  V2 INFORM Failed.\n");
                u4Status = SNMP_FAILURE;
            }
            else
            {
                /* Add the Inform Pdu to the Manager Inform List */
                /* NOTE: shouldn't free pInformDupPdu *** */
                u4Status =
                    (UINT4) SnmpAddInformNode ((tSNMP_NORMAL_PDU *)
                                               pInformDupPdu,
                                               pInformPdu->u4_RequestID,
                                               pSnmpTgtAddrEntry, AppCallback,
                                               pCookie);
                if (u4Status == SNMP_SUCCESS)
                {
                    if (AppCallback != NULL)
                    {
                        SnmpInfmStatusInfo.pAppCookie = pCookie;
                        SnmpInfmStatusInfo.u4InfmReqId = u4ReqId;
                        MEMCPY (&(SnmpInfmStatusInfo.u4MgrAddr),
                                pSnmpTgtAddrEntry->Address.pu1_OctetList,
                                SNMP_IPADDR_LEN);

                        SnmpInfmStatusInfo.u2Port = pSnmpTgtAddrEntry->u2Port;

                        SnmpInfmStatusInfo.u4AckStatus = SNMP_INFORM_MSG_TX;

                        /* The manager address passed to this function is in 
                         * network byte order */
                        (*AppCallback) (&SnmpInfmStatusInfo);
                    }

                    if (SnmpStartInformReTxTimer
                        (u4ReqId, pInformPdu->u4_RequestID,
                         pSnmpTgtAddrEntry, pSnmpNotifyEntry) != SNMP_SUCCESS)
                    {
                        SNMPTrace ("Unable to start re-tx timer. \n");
                    }
                }
                else
                {
                    /* Failure returned by Add Inform node               */
                    /* The Inform message is acutally dropped here       */
                    /* increase the drop count - Global and for Manager  */

                    SNMP_INFORM_DROPS += 1;
                    SNMP_MGR_INFORM_DROPS (pSnmpTgtAddrEntry) += 1;

                    SNMPFreeV2Inform (pInformDupPdu);
                }
            }
        }
        else if (u4Retry == SNMP_RETRANSMIT)    /* Retransmission messages */
        {
            SNMP_MGR_INR_INFORM_REQ_RETRY (pSnmpTgtAddrEntry);

            /* Decrementing the global value of await-ack during retransmission 
             * for the Previous msg. */
            SNMP_DEC_INFORM_AWAIT_ACK;

            /* Execute CALLBACK with SNMP_INFORM_MSG_RE_TX
             * as the Inform message is retransmitted
             */
            if (AppCallback != NULL)
            {
                SnmpInfmStatusInfo.pAppCookie = pCookie;
                SnmpInfmStatusInfo.u4InfmReqId = u4ReqId;
                MEMCPY (&SnmpInfmStatusInfo.u4MgrAddr,
                        pSnmpTgtAddrEntry->Address.pu1_OctetList,
                        SNMP_IPADDR_LEN);

                SnmpInfmStatusInfo.u2Port = pSnmpTgtAddrEntry->u2Port;

                SnmpInfmStatusInfo.u4AckStatus = SNMP_INFORM_MSG_RE_TX;
                /* The manager address passed to this function is in 
                 * network byte order */
                (*AppCallback) (&SnmpInfmStatusInfo);
            }

            if (SnmpStartInformReTxTimer (u4ReqId, u4PrevReqId,
                                          pSnmpTgtAddrEntry,
                                          pSnmpNotifyEntry) != SNMP_SUCCESS)
            {
                SNMPTrace ("Unable to start re-tx timer. \n");
            }
            else
            {
                u4Status = SNMP_FAILURE;
            }

        }

    }

    return u4Status;
}

/* Function for resending the INFORM Request with new Request ID on timeout */
/************************************************************************
 *  Function Name   : SnmpResendInformReqOnTimeOut
 *  Description     : Function to re-transmit INFORM PDU 
 *  Input           : pInformHostNode   - Inform Host Node
 *                  : pSnmpTgtAddrEntry - pointer to target addr entry
 *                    pSnmpNotifyEntry - pointer to notiifcation entry
 *  Output          : None
 *  Returns         : SNMP_SUCCESS/SNMP_FAILURE;
 ************************************************************************/

INT4
SnmpResendInformReqOnTimeOut (tInformNode * pInformHostNode,
                              tSnmpTgtAddrEntry * pSnmpTgtAddrEntry,
                              tSnmpNotifyEntry * pSnmpNotifyEntry)
{

    UINT1              *pu1Msg = NULL;
    INT4                i4MsgLen = 0, i4Result = SNMP_ZERO;
    UINT4               u4RetrReqId = 0;
    UINT4               u4OrigReqId = 0;
    UINT4               u4PrevReqId = 0;
    UINT4               u4Err = SNMP_ZERO;
    tSNMP_V3PDU        *pV3Pdu = NULL;
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    tSNMP_VAR_BIND     *pVarPtr = NULL;
    tSnmpVacmInfo       VacmInfo;

    tSNMP_NORMAL_PDU   *pInformRetPdu = NULL;
    tSNMP_VAR_BIND     *pSysTime = NULL;

    if ((pInformHostNode == NULL) || (pInformHostNode->pInformNode == NULL) ||
        (pInformHostNode->pInformNode->pInformPduSent == NULL))
    {
        SNMPTrace ("There is no INFORM PDU to be re-transmitted \n");
        return SNMP_FAILURE;
    }

    u4PrevReqId = pInformHostNode->u4InformReqId;

    pInformRetPdu = (tSNMP_NORMAL_PDU *)
        pInformHostNode->pInformNode->pInformPduSent;

    /* Since this is re-transmission, generate a new Request ID  and assign 
     * the same to the Re-transmission INFORM PDU */
    u4RetrReqId = SNMPGetRequestID ();

    /* Save the original Req Id as multiple managers use this */
    u4OrigReqId = pInformRetPdu->u4_RequestID;

    /* Assign the New request ID for encoding */
    pInformRetPdu->u4_RequestID = u4RetrReqId;

    /* Assign the current System Time */
    pSysTime = SNMPGetSysUpTime ();
    if (pSysTime == NULL)
    {
        SNMPTrace ("SNMPGetSysUpTime failed. \n");
        return SNMP_FAILURE;
    }
    else
    {
        pInformRetPdu->pVarBindList->ObjValue.u4_ULongValue =
            pSysTime->ObjValue.u4_ULongValue;
        FREE_SNMP_VARBIND (pSysTime);
    }

    pSnmpTgtParamEntry = SNMPGetTgtParamEntry (&(pSnmpTgtAddrEntry->
                                                 AddrParams));
    if (pSnmpTgtParamEntry == NULL)
    {
        SNMPTrace (" SNMP Target Param is NULL Re-transmission FAILED\n");
        return SNMP_FAILURE;
    }

    if (pSnmpTgtParamEntry->i2ParamMPModel == SNMP_MPMODEL_V2C)
    {
        /* Encoding of the INFORM PDU for re-transmission */

        SNMPEncodeV2TrapPacket (pInformRetPdu, &pu1Msg, &i4MsgLen,
                                SNMP_PDU_TYPE_V2_INFORM_REQUEST);
    }
    else if (pSnmpTgtParamEntry->i2ParamMPModel == SNMP_MPMODEL_V3)
    {
        pSnmpUsmEntry = SNMPGetUsmEntry (&gSnmpEngineID,
                                         &pSnmpTgtParamEntry->ParamSecName);
        if (pSnmpUsmEntry == NULL)
        {
            SNMPTrace (" SNMP User Entry is NULL Re-transmission FAILED\n");
            return SNMP_FAILURE;
        }
        pV3Pdu = SNMPFillV3Params (pSnmpUsmEntry, NULL);

        if (pV3Pdu != NULL)
        {
            pV3Pdu->u4MsgSecModel = (UINT4) pSnmpTgtParamEntry->i2ParamSecModel;
            MEMCPY (&pV3Pdu->Normalpdu, pInformRetPdu,
                    sizeof (tSNMP_NORMAL_PDU));
            VacmInfo.pUserName = &(pV3Pdu->MsgSecParam.MsgUsrName);
            VacmInfo.pContextName = &(pV3Pdu->ContextName);
            VacmInfo.u4SecModel = pV3Pdu->u4MsgSecModel;
            if (pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO] == 0x01)
            {
                VacmInfo.u4SecLevel = SNMP_AUTH_NOPRIV;
            }
            else if (pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO] == 0x03)
            {
                VacmInfo.u4SecLevel = SNMP_AUTH_PRIV;
            }
            else
            {
                VacmInfo.u4SecLevel = SNMP_NOAUTH_NOPRIV;
            }
            pVarPtr = pV3Pdu->Normalpdu.pVarBindList;
            while (pVarPtr != NULL)
            {
                i4Result = SNMPVACMIsAccessAllowed (&VacmInfo,
                                                    SNMP_ACCESSIBLEFORNOTIFY,
                                                    pVarPtr->pObjName, &u4Err);
                if (i4Result == SNMP_FAILURE)
                {
                    break;
                }
                pVarPtr = pVarPtr->pNextVarBind;
            }
            if (i4Result == SNMP_SUCCESS)
            {
                SNMPEncodeV3Message (pV3Pdu, &pu1Msg, &i4MsgLen,
                                     SNMP_PDU_TYPE_V2_INFORM_REQUEST);
            }
            MemReleaseMemBlock (gSnmpV3PduPoolId, (UINT1 *) pV3Pdu);
        }

    }

    /* Re-assign the Original Request ID in Inform Msg List */
    /* To be re-assigned after encoding the packet */
    pInformRetPdu->u4_RequestID = u4OrigReqId;

    if (pu1Msg == NULL)
    {
        SNMPTrace ("Encoding of INFORM PDU during re-transmit failed. \n");
        return SNMP_FAILURE;
    }

    /* Function to send the INFORM PDU */
    /* This is the re-transmission of INFORM. Hence 
     * Retransmission is set to SNMP_ONE */

    SNMPSendInformToManager (pu1Msg, i4MsgLen,
                             pSnmpTgtAddrEntry, pSnmpNotifyEntry,
                             u4RetrReqId, u4PrevReqId, SNMP_RETRANSMIT,
                             pInformRetPdu, pInformHostNode->AppCallback,
                             pInformHostNode->pCookieAddr);

    /* Increase the Retransmission count */
    pInformHostNode->i4RetryCount += 1;

    MemReleaseMemBlock (gSnmpPktPoolId, (UINT1 *) pu1Msg);
    pu1Msg = NULL;

    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : SNMPCopyInformPdu 
 *  Description     : Function to copy INFORM PDU used for re-transmission
 *  Input           : pV2TrapMsg - v2 Trap Pointer
 *  Output          : None
 *  Returns         : INFORM PDU pointer Else NULL
 ************************************************************************/

tSNMP_NORMAL_PDU   *
SNMPCopyInformPdu (tSNMP_NORMAL_PDU * pV2TrapMsg)
{
    tSNMP_NORMAL_PDU   *pInformMsg = NULL;
    tSNMP_VAR_BIND     *pTempVb = NULL, *pTempVb1 = NULL;
    tSNMP_VAR_BIND     *pFirstVb = NULL, *pDesVb = NULL;

    UINT1               u1FirstVb = 1;

    pInformMsg = MemAllocMemBlk (gSnmpNormalPduPoolId);
    if (pInformMsg == NULL)
    {
        SNMPTrace ("Unable to allocate memory for INFORM Msg\n");
        return NULL;
    }
    MEMSET (pInformMsg, SNMP_ZERO, sizeof (tSNMP_NORMAL_PDU));
    pInformMsg->u4_RequestID = pV2TrapMsg->u4_RequestID;
    pInformMsg->i2_PduType = SNMP_PDU_TYPE_V2_INFORM_REQUEST;
    pInformMsg->u4_Version = VERSION2;

    /* Allocate Community String */
    pInformMsg->pCommunityStr =
        allocmem_octetstring (SNMP_MAX_OCTETSTRING_SIZE);
    if (pInformMsg->pCommunityStr == NULL)
    {
        SNMPFreeV2Trap (pInformMsg);
        SNMPTrace ("Unable to allocate memory for Community String\n");
        return NULL;
    }
    SNMPCopyOctetString (pInformMsg->pCommunityStr, pV2TrapMsg->pCommunityStr);

    pTempVb = pV2TrapMsg->pVarBindList;

    while (pTempVb != NULL)
    {
        pDesVb = SNMPCopyVarBind (pTempVb);
        if (pDesVb == NULL)
        {
            SNMPFreeV2Trap (pInformMsg);
            return (NULL);
        }
        if (u1FirstVb == SNMP_ONE)
        {
            pTempVb1 = pDesVb;
            pFirstVb = pTempVb1;
            u1FirstVb++;
        }
        else
        {
            pTempVb1->pNextVarBind = pDesVb;
            pTempVb1 = pTempVb1->pNextVarBind;
        }
        pTempVb = pTempVb->pNextVarBind;
    }

    if (pTempVb1 != NULL)
    {
        pTempVb1->pNextVarBind = NULL;
    }

    pInformMsg->pVarBindList = pFirstVb;
    pInformMsg->pVarBindEnd = pDesVb;

    return (pInformMsg);
}

/*************************************************************************/
/*  Function Name   : SnmpFreeAllMgrInformList                           */
/*  Description     : This function checks for Inform Nodes for each     */
/*                  : Manager. Deletes the Inform Reqs for all Managers. */
/*                  : Stops the Retransmission Timer.                    */
/*  Input(s)        : None.                                              */
/*  Output(s)       : None.                                              */
/*  Returns         : None.                                              */
/*************************************************************************/

VOID
SnmpFreeAllMgrInformList ()
{
    tSnmpTgtAddrEntry  *pFirstTarget = NULL;

    /* Scan over all Managers list. */
    if ((pFirstTarget = SNMPGetFirstTgtAddrEntry ()) == NULL)
    {
        return;
    }

    do
    {
        SnmpFreeMgrInformList (pFirstTarget);

        pFirstTarget = SNMPGetNextTgtAddrEntry (&(pFirstTarget->AddrName));
    }                            /* end of do...while */
    while (pFirstTarget != NULL);

    return;
}

/************************************************************************/
/*  Function Name   : SnmpAddInformNode                                 */
/*  Description     : Add a new Inform Request to the List.             */
/*  Input(s)        : pInformPdu        - Request to be added.          */
/*                  : u4ReqId           - Request Id.                   */
/*                  : pSnmpTgtAddrEntry - pointer to Target addr entry  */
/*                    AppCallback       - Callback function provided by */
/*                                        application                   */
/*                    pCookie           - pointer to memory in          */
/*                                        application that has          */
/*                                        information related to the    */
/*                                        notification being generated  */
/*  Output(s)       : None.                                             */
/*  Returns         : SNMP_SUCCESS/SNMP_FAILURE                         */
/************************************************************************/

INT4
SnmpAddInformNode (tSNMP_NORMAL_PDU * pInformPdu, UINT4 u4ReqId,
                   tSnmpTgtAddrEntry * pSnmpTgtAddrEntry,
                   SnmpAppNotifCb AppCallback, VOID *pCookie)
{
    tInformNode        *pInformNode = NULL;
    tTMO_SLL           *pInformList = NULL;

    tSnmpInformMsgNode *pAddMsgNode = NULL;

    /* allocate for the new node and the new stack entry caused by the creation of
     * this node - if unsuccessful then return failure. We always allocate for the
     * indirectly affected layer also since we cannot roll-back the change if we
     * are not able to allocate memory later. */
    if ((pInformNode = MemAllocMemBlk (gSnmpInformNodePoolId)) == NULL)
    {
        SNMPTrace ("Inform Node Alloc Failure \n");
        return SNMP_FAILURE;
    }

    MEMSET (pInformNode, 0, sizeof (tInformNode));
    /* initialise the node with the values provided */
    TMO_SLL_Init_Node (&pInformNode->NextEntry);

    /* Incoming PDU itself is a copy of the Sent Inform PDU.
     * Check the INFORM PDU Node is available in Global INFORM Msg List.
     * If Available, increase the Manager count. Else add the INFORM PDU 
     * Node to the global INFORM Msg List.
     * NOTE: This PDU Node is freed on receipt of ACK or on reaching Max 
     * retransmission  from all Managers.*/

    pAddMsgNode = SnmpAddInformRequestMsgNode (pInformPdu);
    if (pAddMsgNode == NULL)
    {
        SNMPTrace ("INFORM MSG Node is NULL due to memory failure.\n");
        MemReleaseMemBlock (gSnmpInformNodePoolId, (UINT1 *) pInformNode);
        pInformNode = NULL;
        return SNMP_FAILURE;
    }

    /* Assign this as the Inform Msg Pointer tot he Inform req Node */
    pInformNode->pInformNode = pAddMsgNode;

    /* Set the values for this node and also copy the incoming INFORM PDU */
    pInformNode->u4InformReqId = u4ReqId;
    pInformNode->i4RetryCount = 0;

    /* 
     * update the Inform Pdu with application given
     * callback function and the cookie address
     */
    pInformNode->AppCallback = AppCallback;
    pInformNode->pCookieAddr = pCookie;

    /* Add the entry to the Trap table List */
    pInformList = &(pSnmpTgtAddrEntry->pInformList);

    /* call direct add */
    TMO_SLL_Add (pInformList, &pInformNode->NextEntry);
    return SNMP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : SnmpRemoveInformNode                              */
/*  Description     : Removes an Inform Request from the list.          */
/*  Input(s)        : pSnmpTgtAddrEntry - pointer to Target addr Entry  */
/*                    u4ReqId           - Inform Req Id to be removed   */
/*                    u4IsCallBack      - Flag which indicates if       */
/*                                        execution of callback function*/
/*                                        is required                   */
/*                    u4CbStatus        - If Callback function is       */
/*                                        executed, this variable       */
/*                                        contains the status returned  */
/*                                        to application from SNMP      */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/

VOID
SnmpRemoveInformNode (tSnmpTgtAddrEntry * pSnmpTgtAddrEntry, UINT4 u4ReqId,
                      UINT4 u4IsCallBack, UINT4 u4CbStatus)
{
    tInformNode        *pScanNode = NULL;
    tInformNode        *pTmpScanNode = NULL;
    tTMO_SLL           *pInformList = NULL;
    BOOLEAN             bMsgNodeFound = FALSE;
    SnmpAppNotifCb      AppRegCbFn = NULL;
    tSnmpInfmStatusInfo SnmpInfmStatusInfo;

    MEMSET (&SnmpInfmStatusInfo, 0, sizeof (tSnmpInfmStatusInfo));

    /* Add the entry to the Trap table List */
    pInformList = &(pSnmpTgtAddrEntry->pInformList);

    if (pInformList == NULL)
    {
        return;
    }

    /* Find the Node that matches the given ReqId */
    if (TMO_SLL_Count (pInformList) > 0)
    {
        SNMP_SLL_DYN_Scan (pInformList, pScanNode, pTmpScanNode, tInformNode *)
        {

            if (pScanNode->u4InformReqId == u4ReqId)
            {
                bMsgNodeFound = TRUE;
                break;
            }
        }
    }
    else
    {
        return;
    }

    if (bMsgNodeFound == FALSE)
    {
        /* This should not occur */
        return;
    }

    if (u4IsCallBack == SNMP_TRUE)
    {
        /* Execute CALLBACK with SNMP_INFORM_MSG_DROPPED /
         * SNMP_INFORM_ACK_RECEIVED based on u4CbStatus
         */
        AppRegCbFn = pScanNode->AppCallback;
        if (AppRegCbFn != NULL)
        {
            SnmpInfmStatusInfo.pAppCookie = pScanNode->pCookieAddr;
            SnmpInfmStatusInfo.u4InfmReqId = pScanNode->u4InformReqId;
            MEMCPY (&SnmpInfmStatusInfo.u4MgrAddr,
                    pSnmpTgtAddrEntry->Address.pu1_OctetList, SNMP_IPADDR_LEN);

            SnmpInfmStatusInfo.u2Port = pSnmpTgtAddrEntry->u2Port;

            if ((pScanNode->i4RetryCount == 0)
                && (u4CbStatus == SNMP_INFORM_ACK_RECEIVED))
            {
                SnmpInfmStatusInfo.u4InfmResp = 1;
                SnmpInfmStatusInfo.u4InfmSent = 1;
                SnmpInfmStatusInfo.u4AckStatus = SNMP_INFORM_ACK_RECEIVED;
            }
            else if (u4CbStatus == SNMP_INFORM_ACK_RECEIVED)
            {
                SnmpInfmStatusInfo.u4InfmResp = 1;
                SnmpInfmStatusInfo.u4InfmRetried =
                    (UINT4) pScanNode->i4RetryCount;
                SnmpInfmStatusInfo.u4InfmFail = (UINT4) pScanNode->i4RetryCount;
                SnmpInfmStatusInfo.u4InfmSent =
                    (UINT4) (pScanNode->i4RetryCount + 1);
                SnmpInfmStatusInfo.u4AckStatus = u4CbStatus;
            }
            else if (u4CbStatus == SNMP_INFORM_NOT_ACKED)
            {
                SnmpInfmStatusInfo.u4InfmResp = 0;
                SnmpInfmStatusInfo.u4InfmRetried =
                    (UINT4) pScanNode->i4RetryCount;
                SnmpInfmStatusInfo.u4InfmFail =
                    (UINT4) (pScanNode->i4RetryCount + 1);
                SnmpInfmStatusInfo.u4InfmSent =
                    (UINT4) (pScanNode->i4RetryCount + 1);
                SnmpInfmStatusInfo.u4AckStatus = SNMP_INFORM_NOT_ACKED;
            }
            (*AppRegCbFn) (&SnmpInfmStatusInfo);
        }
    }

    /* First delete the Inform Msg Node from Global List */
    SnmpRemoveInformRequestMsgNode (pScanNode->pInformNode);
    pScanNode->pInformNode = NULL;
    TmrStopTimer (SnmpAgtTimerListId, &(pScanNode->InformTmr.TmrInform));
    TMO_SLL_Delete (pInformList, &pScanNode->NextEntry);
    MemReleaseMemBlock (gSnmpInformNodePoolId, (UINT1 *) pScanNode);
    pScanNode = NULL;
    return;
}

/*************************************************************************/
/*  Function Name   : SnmpFreeMgrInformList                              */
/*  Description     : This function checks for Inform Node's for the     */
/*                  : Manager. Deletes the Inform Reqs for the Manager.  */
/*                  : Stops the Retransmission Timer.                    */
/*  Input(s)        : pSnmpTgtAddrEntry - pointer to Target addr Entry   */
/*  Output(s)       : None.                                              */
/*  Returns         : None.                                              */
/*************************************************************************/

VOID
SnmpFreeMgrInformList (tSnmpTgtAddrEntry * pSnmpTgtAddrEntry)
{
    tInformNode        *pScanNode = NULL;
    tInformNode        *pTmpScanNode = NULL;
    tTMO_SLL           *pTempSll = NULL;

    /* Assign the InformList starting address */
    pTempSll = &(pSnmpTgtAddrEntry->pInformList);

    if (pTempSll != NULL)
    {

        /* Scan for each Node and Delete the same */
        SNMP_SLL_DYN_Scan (pTempSll, pScanNode, pTmpScanNode, tInformNode *)
        {
            SnmpRemoveInformRequestMsgNode (pScanNode->pInformNode);
            pScanNode->pInformNode = NULL;
            TmrStopTimer (SnmpAgtTimerListId,
                          &(pScanNode->InformTmr.TmrInform));
            TMO_SLL_Delete (pTempSll, &pScanNode->NextEntry);
            MemReleaseMemBlock (gSnmpInformNodePoolId, (UINT1 *) pScanNode);
            pScanNode = NULL;
        }                        /* End of Scan */

    }                            /* End of if */

}

/************************************************************************/
/*  Function Name   : SnmpGetInformNode                                 */
/*  Description     : Gets the Inform Req from the List.Returns NULL if */
/*                  : it is not present in the list                     */
/*  Input(s)        : u4ReqId           - Inform Msg Request Id         */
/*                  : pSnmpTgtAddrEntry - pointer to Target addr Entry  */
/*  Output(s)       : None.                                             */
/*  Returns         : pointer to the node if present else NULL !        */
/************************************************************************/

tInformNode        *
SnmpGetInformNode (UINT4 u4ReqId, tSnmpTgtAddrEntry * pSnmpTgtAddrEntry)
{
    tInformNode        *pScanNode = NULL;
    tInformNode        *pTmpScanNode = NULL;
    tTMO_SLL           *pInformList = NULL;

    /* Add the entry to the Trap table List */
    if (pSnmpTgtAddrEntry != NULL)
        pInformList = &(pSnmpTgtAddrEntry->pInformList);

    /* Find the Node that matches the given ReqId */
    if (pInformList && (TMO_SLL_Count (pInformList) > 0))
    {
        SNMP_SLL_DYN_Scan (pInformList, pScanNode, pTmpScanNode, tInformNode *)
        {

            if (pScanNode->u4InformReqId == u4ReqId)
            {
                return (pScanNode);
            }
        }
    }

    return (NULL);
}

/************************************************************************/
/*  Function Name   : SnmpGetInformRequestMsgNode                       */
/*  Description     : Get the Inform PDU from the List.Returns NULL if  */
/*                  : it is not present in the list                     */
/*  Input(s)        : u4ReqId - Inform Msg Request Id                   */
/*  Output(s)       : None.                                             */
/*  Returns         : pointer to the node if present else NULL !        */
/************************************************************************/

tSnmpInformMsgNode *
SnmpGetInformRequestMsgNode (UINT4 u4ReqId)
{
    tSnmpInformMsgNode *pScanNode = NULL;
    tSnmpInformMsgNode *pTmpScanNode = NULL;

    /* Scan the Global INFORM Msg List.
     * Find the Node that matches the given ReqId */
    if (TMO_SLL_Count (&gInformMsgList) > 0)
    {
        SNMP_SLL_DYN_Scan (&gInformMsgList, pScanNode, pTmpScanNode,
                           tSnmpInformMsgNode *)
        {
            if (pScanNode->pInformPduSent->u4_RequestID == u4ReqId)
            {
                break;
            }
        }
    }

    return pScanNode;
}

/************************************************************************/
/*  Function Name   : SnmpAddInformRequestMsgNode                       */
/*  Description     : Adds the Inform PDU to the List.                  */
/*  Input(s)        : pInformPdu - Pointer to Inform PDU                */
/*  Output(s)       : None.                                             */
/*  Returns         : Pointer to INFORM Msg Node Else NULL              */
/************************************************************************/

tSnmpInformMsgNode *
SnmpAddInformRequestMsgNode (tSNMP_NORMAL_PDU * pInformPdu)
{
    tSnmpInformMsgNode *pInformNode = NULL;
    UINT4               u4ReqId = pInformPdu->u4_RequestID;

    /* First Check for the existence of this INFORM Message */
    pInformNode = SnmpGetInformRequestMsgNode (u4ReqId);

    if (pInformNode != NULL)
    {
        pInformNode->u4MgrCnt += 1;
        SNMPFreeV2Inform (pInformPdu);
        return pInformNode;
    }

    /* Allocate Memory for the Inform Node */
    if ((pInformNode = MemAllocMemBlk (gSnmpInformMsgNodePoolId)) == NULL)
    {
        SNMPTrace ("Inform Message Node Alloc Failure \n");
        return NULL;
    }

    MEMSET (pInformNode, 0, sizeof (tSnmpInformMsgNode));
    /* initialise the node with the values provided */
    TMO_SLL_Init_Node (&pInformNode->NextEntry);

    /* NOTE: Incoming PDU itself is  a copy of the Sent Inform PDU.
     * This PDU is freed on receipt of ACK or on reaching Max retransmission */

    pInformNode->pInformPduSent = pInformPdu;
    pInformNode->u4MgrCnt = SNMP_ONE;

    /* Add the Node */
    TMO_SLL_Add (&gInformMsgList, &pInformNode->NextEntry);
    return pInformNode;
}

/************************************************************************/
/*  Fuwnction Name   : SnmpRemoveInformRequestMsgNode                   */
/*  Description     : Removes an Inform Request from the list.          */
/*  Input(s)        : pInformMsgNode  - Pointer to Inform Message       */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/

VOID
SnmpRemoveInformRequestMsgNode (tSnmpInformMsgNode * pInformMsgNode)
{

    /* Check whether there are any INFORM Msg list */
    if (pInformMsgNode == NULL)
    {
        return;
    }

    /* Check for the number of managers who has sent this INFORM PDU */
    if (pInformMsgNode->u4MgrCnt > SNMP_ONE)
    {
        --pInformMsgNode->u4MgrCnt;
        return;
    }

    /* Delete the Inform Msg Node if last manager */
    TMO_SLL_Delete (&gInformMsgList, &pInformMsgNode->NextEntry);
    SNMPFreeV2Inform (pInformMsgNode->pInformPduSent);
    pInformMsgNode->pInformPduSent = NULL;
    MemReleaseMemBlock (gSnmpInformMsgNodePoolId, (UINT1 *) (pInformMsgNode));
    pInformMsgNode = NULL;
}

/************************************************************************/
/*  Function Name   : SnmpCheckForConnection                            */
/*  Description     : Returns number of awaiting acknowledgements for   */
/*                    Inform PDU of default target address entry        */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : Number of awaiting acknowledgements               */
/************************************************************************/

INT4
SnmpCheckForConnection ()
{
    return 0;
}

/************************************************************************/
/*  Function Name   : SnmpProxyRemoveInformNode                         */
/*  Description     : Removes an Inform Request from the list.          */
/*  Input(s)        : pSnmpTgtAddrEntry - pointer to Target addr Entry  */
/*                  : pPdu           - Inform PDU                        */
/*                  : u4IsCallBack      - Flag which indicates if       */
/*                  :                     execution of callback function*/
/*                  :                     is required                   */
/*                  : u4CbStatus        - If Callback function is       */
/*                  :                     executed, this variable       */
/*                  :                     contains the status returned  */
/*                  :                     to application from SNMP      */
/*  Output(s)       : None.                                             */
/*  Returns         : None.                                             */
/************************************************************************/

INT4
SnmpProxyRemoveInformNode (tSnmpTgtAddrEntry * pSnmpTgtAddrEntry, UINT4 u4ReqId,
                           tSNMP_NORMAL_PDU * pPdu, UINT4 u4IsCallBack,
                           UINT4 u4CbStatus)
{
    tInformNode        *pScanNode = NULL;
    tInformNode        *pTmpScanNode = NULL;
    tTMO_SLL           *pInformList = NULL;
    INT4                i4Result = SNMP_FAILURE;
    BOOLEAN             bMsgNodeFound = FALSE;
    SnmpAppNotifCb      AppRegCbFn = NULL;
    tSnmpInfmStatusInfo SnmpInfmStatusInfo;

    MEMSET (&SnmpInfmStatusInfo, 0, sizeof (tSnmpInfmStatusInfo));

    pInformList = &(pSnmpTgtAddrEntry->pInformList);

    if (pInformList == NULL)
    {
        return i4Result;
    }

    /* Find the Node that matches the given ReqId */
    if (TMO_SLL_Count (pInformList) > 0)
    {
        SNMP_SLL_DYN_Scan (pInformList, pScanNode, pTmpScanNode, tInformNode *)
        {

            if (pScanNode->u4InformReqId == u4ReqId)
            {
                bMsgNodeFound = TRUE;
                break;
            }
        }
    }
    else
    {
        return i4Result;
    }

    if (bMsgNodeFound == FALSE)
    {
        /* This should not occur */
        return i4Result;
    }

    if (u4IsCallBack == SNMP_TRUE)
    {
        /* Execute CALLBACK with SNMP_INFORM_MSG_DROPPED /
         * SNMP_INFORM_ACK_RECEIVED based on u4CbStatus
         */
        AppRegCbFn = pScanNode->AppCallback;
        if (AppRegCbFn != NULL)
        {
            SnmpInfmStatusInfo.pAppCookie = pScanNode->pCookieAddr;
            SnmpInfmStatusInfo.u4InfmReqId = pScanNode->u4InformReqId;
            MEMCPY (&SnmpInfmStatusInfo.u4MgrAddr,
                    pSnmpTgtAddrEntry->Address.pu1_OctetList,
                    pSnmpTgtAddrEntry->Address.i4_Length);
            if ((pScanNode->i4RetryCount == 0)
                && (u4CbStatus == SNMP_INFORM_ACK_RECEIVED))
            {
                SnmpInfmStatusInfo.u4InfmResp = 1;
                SnmpInfmStatusInfo.u4InfmSent = 1;
                SnmpInfmStatusInfo.u4AckStatus = SNMP_INFORM_ACK_RECEIVED;
            }
            else if (u4CbStatus == SNMP_INFORM_ACK_RECEIVED)
            {
                SnmpInfmStatusInfo.u4InfmResp = 1;
                SnmpInfmStatusInfo.u4InfmRetried =
                    (UINT4) pScanNode->i4RetryCount;
                SnmpInfmStatusInfo.u4InfmFail = (UINT4) pScanNode->i4RetryCount;
                SnmpInfmStatusInfo.u4InfmSent =
                    (UINT4) pScanNode->i4RetryCount + 1;
                SnmpInfmStatusInfo.u4AckStatus = u4CbStatus;
            }
            else if (u4CbStatus == SNMP_INFORM_NOT_ACKED)
            {
                SnmpInfmStatusInfo.u4InfmResp = 0;
                SnmpInfmStatusInfo.u4InfmRetried =
                    (UINT4) pScanNode->i4RetryCount;
                SnmpInfmStatusInfo.u4InfmFail =
                    (UINT4) (pScanNode->i4RetryCount + 1);
                SnmpInfmStatusInfo.u4InfmSent =
                    (UINT4) (pScanNode->i4RetryCount + 1);
                SnmpInfmStatusInfo.u4AckStatus = SNMP_INFORM_NOT_ACKED;
            }

            ((tProxyCookie *) SnmpInfmStatusInfo.pAppCookie)->pRespPdu = pPdu;
            ((tProxyCookie *) SnmpInfmStatusInfo.pAppCookie)->pInformNode =
                pScanNode->pInformNode;

            (*AppRegCbFn) (&SnmpInfmStatusInfo);
            i4Result = SNMP_SUCCESS;
        }
    }

    /* First delete the Inform Msg Node from Global List */
    SnmpRemoveInformRequestMsgNode (pScanNode->pInformNode);
    pScanNode->pInformNode = NULL;
    TmrStopTimer (SnmpAgtTimerListId, &(pScanNode->InformTmr.TmrInform));
    TMO_SLL_Delete (pInformList, &pScanNode->NextEntry);
    MemReleaseMemBlock (gSnmpInformNodePoolId, (UINT1 *) pScanNode);
    pScanNode = NULL;
    return i4Result;
}

/* Function for resending the INFORM Request with new Request ID on timeout */
/************************************************************************
 *  Function Name   : SnmpProxyResendInformOnTimeOut
 *  Description     : Function to re-transmit INFORM PDU 
 *  Input           : pInformHostNode   - Inform Host Node
 *                  : pSnmpTgtAddrEntry - pointer to target addr entry
 *  Output          : None
 *  Returns         : SNMP_SUCCESS/SNMP_FAILURE;
 ************************************************************************/
INT4
SnmpProxyResendInformOnTimeOut (tInformNode * pInformHostNode,
                                tSnmpTgtAddrEntry * pSnmpTgtAddrEntry)
{

    INT4                i4MsgLen = 0, i4Result = SNMP_SUCCESS;
    UINT4               u4RetrReqId = 0;
    UINT4               u4OrigReqId = 0;
    UINT4               u4PrevReqId = 0;
    INT2                i2OutPduType = SNMP_ZERO;
    UINT1              *pu1Msg = NULL;
    VOID               *pOutPtr = NULL;
    tSNMP_V3PDU        *pV3Pdu = NULL;
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    tSNMP_NORMAL_PDU   *pInformRetPdu = NULL;
    tSNMP_VAR_BIND     *pSysTime = NULL;
    tProxyCookie       *pCookie = NULL;

    if ((pInformHostNode == NULL) || (pInformHostNode->pInformNode == NULL) ||
        (pInformHostNode->pInformNode->pInformPduSent == NULL))
    {
        SNMPTrace ("There is no INFORM PDU to be re-transmitted \n");
        return SNMP_FAILURE;
    }

    pCookie = (tProxyCookie *) pInformHostNode->pCookieAddr;

    u4PrevReqId = pInformHostNode->u4InformReqId;

    pInformRetPdu = (tSNMP_NORMAL_PDU *)
        pInformHostNode->pInformNode->pInformPduSent;

    /* Since this is re-transmission, generate a new Request ID  and assign 
     * the same to the Re-transmission INFORM PDU */
    u4RetrReqId = SNMPGetRequestID ();

    /* Save the original Req Id as multiple managers use this */
    u4OrigReqId = pInformRetPdu->u4_RequestID;

    /* Assign the New request ID for encoding */
    pInformRetPdu->u4_RequestID = u4RetrReqId;

    i2OutPduType = SNMP_PDU_TYPE_V2_INFORM_REQUEST;

    /* Assign the current System Time */
    pSysTime = SNMPGetSysUpTime ();
    if (pSysTime == NULL)
    {
        SNMPTrace ("SNMPGetSysUpTime failed. \n");
        return SNMP_FAILURE;
    }
    else
    {
        pInformRetPdu->pVarBindList->ObjValue.u4_ULongValue =
            pSysTime->ObjValue.u4_ULongValue;
        FREE_SNMP_VARBIND (pSysTime);
    }

    pSnmpTgtParamEntry = SNMPGetTgtParamEntry (&(pSnmpTgtAddrEntry->
                                                 AddrParams));
    if (pSnmpTgtParamEntry == NULL)
    {
        SNMPTrace (" SNMP Target Param is NULL Re-transmission FAILED\n");
        return SNMP_FAILURE;
    }

    if (pSnmpTgtParamEntry->i2ParamMPModel == SNMP_MPMODEL_V2C)
    {
        /* Encoding of the INFORM PDU for re-transmission */
        SNMPEncodeV2TrapPacket (pInformRetPdu, &pu1Msg, &i4MsgLen,
                                (INT4) i2OutPduType);
    }
    else if (pSnmpTgtParamEntry->i2ParamMPModel == SNMP_MPMODEL_V3)
    {
        i4Result = SNMPProxyTranslation ((VOID *) pInformRetPdu,
                                         &pOutPtr,
                                         VERSION2,
                                         VERSION3,
                                         pInformRetPdu->i2_PduType,
                                         &i2OutPduType,
                                         &pSnmpTgtAddrEntry->AddrName,
                                         u4RetrReqId, NULL);

        if (i4Result == SNMP_FAILURE)
        {
            SNMPTrace ("[PROXY]: Translation Failed\n");
            if (pOutPtr)
            {
                SNMPProxyFreePdu (pOutPtr, VERSION3, i2OutPduType);
            }

            return i4Result;
        }
        else
        {
            pV3Pdu = (tSNMP_V3PDU *) pOutPtr;
            SNMPEncodeV3Message (pV3Pdu, &pu1Msg, &i4MsgLen, i2OutPduType);
        }

        SNMPFreeTrapPdu (&pOutPtr, VERSION3);
    }

    /* Re-assign the Original Request ID in Inform Msg List */
    /* To be re-assigned after encoding the packet */
    pInformRetPdu->u4_RequestID = u4OrigReqId;

    if (pu1Msg == NULL)
    {
        SNMPTrace ("Encoding of INFORM PDU during re-transmit failed. \n");
        return SNMP_FAILURE;
    }

    if (pCookie)
    {
        /* Set the Inform Host Pointer in the Cookie, which will be used for 
           in case of the sendto Failure in INFORM */
        pCookie->pInformHostNode = pInformHostNode;
    }

    /* Function to send the INFORM PDU */
    /* This is the re-transmission of INFORM. Hence 
     * Retransmission is set to SNMP_ONE */

    SNMPSendInformToManager (pu1Msg, i4MsgLen,
                             pSnmpTgtAddrEntry, NULL,
                             u4RetrReqId, u4PrevReqId, SNMP_RETRANSMIT,
                             pInformRetPdu, pInformHostNode->AppCallback,
                             pInformHostNode->pCookieAddr);

    /* Increase the Retransmission count */
    pInformHostNode->i4RetryCount += 1;
    MemReleaseMemBlock (gSnmpPktPoolId, (UINT1 *) pu1Msg);

    return SNMP_SUCCESS;
}
