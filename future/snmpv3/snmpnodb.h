/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: snmpnodb.h,v 1.5 2015/04/28 12:35:02 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _SNMPNODB_H
#define _SNMPNODB_H

UINT1 SnmpNotifyTableINDEX [] = {SNMP_DATA_TYPE_IMP_OCTET_PRIM};
UINT1 SnmpNotifyFilterProfileTableINDEX [] = {SNMP_DATA_TYPE_IMP_OCTET_PRIM};
UINT1 SnmpNotifyFilterTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_IMP_OBJECT_ID};

UINT4 snmpno [] ={1,3,6,1,6,3,13};
tSNMP_OID_TYPE snmpnoOID = {7, snmpno};


UINT4 SnmpNotifyName [ ] ={1,3,6,1,6,3,13,1,1,1,1};
UINT4 SnmpNotifyTag [ ] ={1,3,6,1,6,3,13,1,1,1,2};
UINT4 SnmpNotifyType [ ] ={1,3,6,1,6,3,13,1,1,1,3};
UINT4 SnmpNotifyStorageType [ ] ={1,3,6,1,6,3,13,1,1,1,4};
UINT4 SnmpNotifyRowStatus [ ] ={1,3,6,1,6,3,13,1,1,1,5};
UINT4 SnmpNotifyFilterProfileName [ ] ={1,3,6,1,6,3,13,1,2,1,1};
UINT4 SnmpNotifyFilterProfileStorType [ ] ={1,3,6,1,6,3,13,1,2,1,2};
UINT4 SnmpNotifyFilterProfileRowStatus [ ] ={1,3,6,1,6,3,13,1,2,1,3};
UINT4 SnmpNotifyFilterSubtree [ ] ={1,3,6,1,6,3,13,1,3,1,1};
UINT4 SnmpNotifyFilterMask [ ] ={1,3,6,1,6,3,13,1,3,1,2};
UINT4 SnmpNotifyFilterType [ ] ={1,3,6,1,6,3,13,1,3,1,3};
UINT4 SnmpNotifyFilterStorageType [ ] ={1,3,6,1,6,3,13,1,3,1,4};
UINT4 SnmpNotifyFilterRowStatus [ ] ={1,3,6,1,6,3,13,1,3,1,5};


tMbDbEntry snmpnoMibEntry[]= {

{{11,SnmpNotifyName}, GetNextIndexSnmpNotifyTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, SnmpNotifyTableINDEX, 1, 0, 0, NULL},

{{11,SnmpNotifyTag}, GetNextIndexSnmpNotifyTable, SnmpNotifyTagGet, SnmpNotifyTagSet, SnmpNotifyTagTest, SnmpNotifyTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, SnmpNotifyTableINDEX, 1, 0, 0, ""},

{{11,SnmpNotifyType}, GetNextIndexSnmpNotifyTable, SnmpNotifyTypeGet, SnmpNotifyTypeSet, SnmpNotifyTypeTest, SnmpNotifyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, SnmpNotifyTableINDEX, 1, 0, 0, "1"},

{{11,SnmpNotifyStorageType}, GetNextIndexSnmpNotifyTable, SnmpNotifyStorageTypeGet, SnmpNotifyStorageTypeSet, SnmpNotifyStorageTypeTest, SnmpNotifyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, SnmpNotifyTableINDEX, 1, 0, 0, "3"},

{{11,SnmpNotifyRowStatus}, GetNextIndexSnmpNotifyTable, SnmpNotifyRowStatusGet, SnmpNotifyRowStatusSet, SnmpNotifyRowStatusTest, SnmpNotifyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, SnmpNotifyTableINDEX, 1, 0, 1, NULL},

{{11,SnmpNotifyFilterProfileName}, GetNextIndexSnmpNotifyFilterProfileTable, SnmpNotifyFilterProfileNameGet, SnmpNotifyFilterProfileNameSet, SnmpNotifyFilterProfileNameTest, SnmpNotifyFilterProfileTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, SnmpNotifyFilterProfileTableINDEX, 1, 0, 0, NULL},

{{11,SnmpNotifyFilterProfileStorType}, GetNextIndexSnmpNotifyFilterProfileTable, SnmpNotifyFilterProfileStorTypeGet, SnmpNotifyFilterProfileStorTypeSet, SnmpNotifyFilterProfileStorTypeTest, SnmpNotifyFilterProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, SnmpNotifyFilterProfileTableINDEX, 1, 0, 0, "3"},

{{11,SnmpNotifyFilterProfileRowStatus}, GetNextIndexSnmpNotifyFilterProfileTable, SnmpNotifyFilterProfileRowStatusGet, SnmpNotifyFilterProfileRowStatusSet, SnmpNotifyFilterProfileRowStatusTest, SnmpNotifyFilterProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, SnmpNotifyFilterProfileTableINDEX, 1, 0, 1, NULL},

{{11,SnmpNotifyFilterSubtree}, GetNextIndexSnmpNotifyFilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_NOACCESS, SnmpNotifyFilterTableINDEX, 2, 0, 0, NULL},

{{11,SnmpNotifyFilterMask}, GetNextIndexSnmpNotifyFilterTable, SnmpNotifyFilterMaskGet, SnmpNotifyFilterMaskSet, SnmpNotifyFilterMaskTest, SnmpNotifyFilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, SnmpNotifyFilterTableINDEX, 2, 0, 0, "0"},

{{11,SnmpNotifyFilterType}, GetNextIndexSnmpNotifyFilterTable, SnmpNotifyFilterTypeGet, SnmpNotifyFilterTypeSet, SnmpNotifyFilterTypeTest, SnmpNotifyFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, SnmpNotifyFilterTableINDEX, 2, 0, 0, "1"},

{{11,SnmpNotifyFilterStorageType}, GetNextIndexSnmpNotifyFilterTable, SnmpNotifyFilterStorageTypeGet, SnmpNotifyFilterStorageTypeSet, SnmpNotifyFilterStorageTypeTest, SnmpNotifyFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, SnmpNotifyFilterTableINDEX, 2, 0, 0, "3"},

{{11,SnmpNotifyFilterRowStatus}, GetNextIndexSnmpNotifyFilterTable, SnmpNotifyFilterRowStatusGet, SnmpNotifyFilterRowStatusSet, SnmpNotifyFilterRowStatusTest, SnmpNotifyFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, SnmpNotifyFilterTableINDEX, 2, 0, 1, NULL},
};
tMibData snmpnoEntry = { 13, snmpnoMibEntry };
#endif /* _SNMPNODB_H */

