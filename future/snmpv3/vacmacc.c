/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: vacmacc.c,v 1.10 2015/04/28 12:35:03 siva Exp $
 *
 * Description: routines for VACM Access Table Database Model 
 *******************************************************************/

#include "snmpcmn.h"
#include "snmp3cli.h"
#include "vacmacc.h"
/*********************************************************************
*  Function Name : VACMInitAccess 
*  Description   : Function to intialise the VACM Access Table
*  Parameter(s)  : None 
*  Return Values : None
*********************************************************************/

INT4
VACMInitAccess ()
{
    UINT4               u4Index = SNMP_ZERO;
    MEMSET (gaAccessEntry, SNMP_ZERO, sizeof (tAccessEntry));
    MEMSET (gaAccessEntry, SNMP_ZERO, sizeof (gaAccessEntry));
    MEMSET (au1ReadName, SNMP_ZERO, sizeof (au1ReadName));
    MEMSET (au1WriteName, SNMP_ZERO, sizeof (au1WriteName));
    MEMSET (au1NotifyName, SNMP_ZERO, sizeof (au1NotifyName));
    MEMSET (au1ContextName, SNMP_ZERO, sizeof (au1ContextName));
    TMO_SLL_Init (&gSnmpAccessEntry);
    for (u4Index = SNMP_ZERO; u4Index < SNMP_MAX_ACCESS_ENTRY; u4Index++)
    {
        gaAccessEntry[u4Index].ReadName.pu1_OctetList = au1ReadName[u4Index];
        gaAccessEntry[u4Index].WriteName.pu1_OctetList = au1WriteName[u4Index];
        gaAccessEntry[u4Index].NotifyName.pu1_OctetList =
            au1NotifyName[u4Index];
        gaAccessEntry[u4Index].Context.pu1_OctetList = au1ContextName[u4Index];
        gaAccessEntry[u4Index].i4Status = SNMP_INACTIVE;
    }
    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : VACMAddAccess 
*  Description   : Function to add an entry to VACM Access table
*  Parameter(s)  : pGroup     - Group Name
*                  pContext   - Context Name
*                  i4SecModel - Security Model
*                  i4SecLevel - Security Level
*  Return Values : SNMP_SUCCESS/SNMP_FAILURE 
*********************************************************************/

INT4
VACMAddAccess (tSNMP_OCTET_STRING_TYPE * pGroup,
               tSNMP_OCTET_STRING_TYPE * pContext, INT4 i4SecModel,
               INT4 i4SecLevel)
{
    UINT4               u4Index = SNMP_ZERO;
    tTMO_SLL_NODE      *pListNode = NULL;
    tSecGrp            *pSecEntry = NULL;
    if (VACMGetAccess (pGroup, pContext, i4SecModel, i4SecLevel) != NULL)
    {
        return SNMP_FAILURE;
    }
    for (u4Index = SNMP_ZERO; u4Index < SNMP_MAX_ACCESS_ENTRY; u4Index++)
    {
        if (gaAccessEntry[u4Index].i4Status == SNMP_INACTIVE)
        {
            gaAccessEntry[u4Index].pGroup = NULL;
            TMO_SLL_Scan (&gSnmpSecGrp, pListNode, tTMO_SLL_NODE *)
            {
                pSecEntry = (tSecGrp *) pListNode;
                if (SNMPCompareOctetString (&(pSecEntry->Group), pGroup) ==
                    SNMP_EQUAL)
                {
                    gaAccessEntry[u4Index].pGroup = &(pSecEntry->Group);
                    break;
                }
            }
            if (gaAccessEntry[u4Index].pGroup == NULL)
            {
                return SNMP_FAILURE;
            }
            gaAccessEntry[u4Index].i4Status = UNDER_CREATION;
            gaAccessEntry[u4Index].i4SecModel = i4SecModel;
            gaAccessEntry[u4Index].i4SecLevel = i4SecLevel;
            gaAccessEntry[u4Index].i4ContextMatch = 1;
            gaAccessEntry[u4Index].i4Storage = SNMP3_STORAGE_TYPE_NONVOLATILE;
            SNMPCopyOctetString (&(gaAccessEntry[u4Index].Context), pContext);
            gaAccessEntry[u4Index].ReadName.i4_Length = SNMP_ZERO;
            gaAccessEntry[u4Index].WriteName.i4_Length = SNMP_ZERO;
            gaAccessEntry[u4Index].NotifyName.i4_Length = SNMP_ZERO;
            VACMAddAccessEntrySll (&(gaAccessEntry[u4Index].link));
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/*********************************************************************
*  Function Name : VACMAddAccessEntrySll 
*  Description   : Function to add the access node  to the list
*  Parameter(s)  : pNode - node to be added
*  Return Values : None 
*********************************************************************/

VOID
VACMAddAccessEntrySll (tTMO_SLL_NODE * pNode)
{
    tAccessEntry       *pCurEntry = NULL;
    tAccessEntry       *pInEntry = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pPrevNode = NULL;
    INT4                i4Return;

    pInEntry = (tAccessEntry *) pNode;
    TMO_SLL_Scan (&gSnmpAccessEntry, pLstNode, tTMO_SLL_NODE *)
    {
        pCurEntry = (tAccessEntry *) pLstNode;

        i4Return = SNMPCompareOctetString (pInEntry->pGroup, pCurEntry->pGroup);

        if (i4Return == SNMP_LESSER)
        {
            break;
        }
        else if (i4Return == SNMP_GREATER)
        {
            pPrevNode = pLstNode;
            continue;
        }

        i4Return =
            SNMPCompareOctetString (&(pInEntry->Context),
                                    &(pCurEntry->Context));

        if (i4Return == SNMP_LESSER)
        {
            break;
        }
        else if (i4Return == SNMP_GREATER)
        {
            pPrevNode = pLstNode;
            continue;
        }

        if (pCurEntry->i4SecModel > pInEntry->i4SecModel)
        {
            break;
        }
        else if (pCurEntry->i4SecModel < pInEntry->i4SecModel)
        {
            pPrevNode = pLstNode;
            continue;
        }

        if (pCurEntry->i4SecLevel > pInEntry->i4SecLevel)
        {
            break;
        }
        pPrevNode = pLstNode;
    }
    TMO_SLL_Insert (&gSnmpAccessEntry, pPrevNode, pNode);
}

/*********************************************************************
*  Function Name : VACMGetAccess 
*  Description   : Function to get an entry from VACM Access Table
*  Parameter(s)  : pGroup     - Group Name
*                  pContext   - Context Name
*                  i4SecModel - Security Model
*                  i4SecLevel - Security Level
*  Return Values : SNMP_SUCCESS/SNMP_FAILURE
*********************************************************************/

tAccessEntry       *
VACMGetAccess (tSNMP_OCTET_STRING_TYPE * pGroup,
               tSNMP_OCTET_STRING_TYPE * pContext,
               INT4 i4SecModel, INT4 i4SecLevel)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tAccessEntry       *pAccessEntry = NULL;
    TMO_SLL_Scan (&gSnmpAccessEntry, pListNode, tTMO_SLL_NODE *)
    {
        pAccessEntry = (tAccessEntry *) pListNode;
        if (pAccessEntry->i4Status != SNMP_INACTIVE)
        {
            if ((pAccessEntry->i4SecModel == i4SecModel) &&
                (i4SecLevel == pAccessEntry->i4SecLevel) &&
                (SNMPCompareOctetString (pGroup, pAccessEntry->pGroup) ==
                 SNMP_EQUAL)
                && (SNMPCompareOctetString (pContext, &(pAccessEntry->Context))
                    == SNMP_EQUAL))
            {
                return pAccessEntry;
            }
        }
    }
    return NULL;
}

/*********************************************************************
*  Function Name : VACMGetFirstAccess 
*  Description   : Function to get the first access entry from the list
*  Parameter(s)  : None
*  Return Values : Access Entry
*********************************************************************/

tAccessEntry       *
VACMGetFirstAccess ()
{
    tAccessEntry       *pFirstAccessEntry = NULL;
    pFirstAccessEntry = (tAccessEntry *) TMO_SLL_First (&gSnmpAccessEntry);
    return pFirstAccessEntry;
}

/*********************************************************************
*  Function Name : VACMGetNextAccess 
*  Description   : Function to get the next access entry from the list
*  Parameter(s)  : pGroup   - Group Name
*                  pContext - Context Name
*                  i4SecModel - Security model
*                  i4SecLevel - Security level
*  Return Values : Access Entry/NULL
*********************************************************************/

tAccessEntry       *
VACMGetNextAccess (tSNMP_OCTET_STRING_TYPE * pGroup,
                   tSNMP_OCTET_STRING_TYPE * pContext, INT4 i4SecModel,
                   INT4 i4SecLevel)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tAccessEntry       *pAccessEntry = NULL;
    INT4                i4Result;

    TMO_SLL_Scan (&gSnmpAccessEntry, pListNode, tTMO_SLL_NODE *)
    {
        pAccessEntry = (tAccessEntry *) pListNode;

        i4Result = SNMPCompareOctetString (pGroup, pAccessEntry->pGroup);
        if (i4Result == SNMP_GREATER)
        {
            continue;
        }
        else if (i4Result == SNMP_LESSER)
        {
            return pAccessEntry;
        }

        i4Result = SNMPCompareOctetString (pContext, &(pAccessEntry->Context));
        if (i4Result == SNMP_GREATER)
        {
            continue;
        }
        else if (i4Result == SNMP_LESSER)
        {
            return pAccessEntry;
        }

        if (i4SecModel > pAccessEntry->i4SecModel)
        {
            continue;
        }
        else if (i4SecModel < pAccessEntry->i4SecModel)
        {
            return pAccessEntry;
        }

        if (i4SecLevel < pAccessEntry->i4SecLevel)
        {
            return pAccessEntry;
        }
    }
    return NULL;
}

/*********************************************************************
*  Function Name : VACMDeleteAccess 
*  Description   : Function to delete the entry from VACM Access table
*  Parameter(s)  : pGroup     -  Group Name
*                  pContext   - Context Name
*                  i4SecModel - Security Model
*                  i4SecLevel - Security Level
*  Return Values : SNMP_SUCCESS/SNMP_FAILURE
*********************************************************************/

UINT4
VACMDeleteAccess (tSNMP_OCTET_STRING_TYPE * pGroup,
                  tSNMP_OCTET_STRING_TYPE * pContext,
                  INT4 i4SecModel, INT4 i4SecLevel)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tAccessEntry       *pAccessEntry = NULL;

    TMO_SLL_Scan (&gSnmpAccessEntry, pListNode, tTMO_SLL_NODE *)
    {
        pAccessEntry = (tAccessEntry *) pListNode;
        if ((pAccessEntry->i4SecModel == i4SecModel) &&
            (pAccessEntry->i4SecLevel == i4SecLevel) &&
            (SNMPCompareOctetString (pContext, &(pAccessEntry->Context)) ==
             SNMP_EQUAL) &&
            (SNMPCompareOctetString (pGroup, pAccessEntry->pGroup) ==
             SNMP_EQUAL))

        {
            pAccessEntry->i4Status = SNMP_INACTIVE;
            SNMPDelVacmAccessEntrySll (&(pAccessEntry->link));
            return SNMP_SUCCESS;
        }
    }

    CLI_SET_ERR (CLI_SNMP3_GRPACCESS_NOT_PRESENT_ERR);
    return SNMP_FAILURE;
}

/*********************************************************************
*  Function Name : SNMPDelVacmAccessEntrySll 
*  Description   : Function to delete an access entry from the list
*  Parameter(s)  : pNode - Node to be deleted 
*  Return Values : None
*********************************************************************/

VOID
SNMPDelVacmAccessEntrySll (tTMO_SLL_NODE * pNode)
{
    TMO_SLL_Delete (&gSnmpAccessEntry, pNode);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : SnmpDeleteVirtualAccessConfig                      */
/*                                                                           */
/*     DESCRIPTION      : Function to delete a  access entry  for teh        */
/*                        context deleted                                    */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
SnmpDeleteVirtualAccessConfig (tSNMP_OCTET_STRING_TYPE * pName)
{
    tSNMP_OCTET_STRING_TYPE Group;
    UINT1               au1Group[SNMP_MAX_ADMIN_STR_LENGTH] = SNMP_DEF_ACCESS_1;
    INT4                i4SecLevel = SNMP_NOAUTH_NOPRIV;
    INT4                i4SecModel = SNMP_SECMODEL_V2C;

    tTMO_SLL_NODE      *pListNode = NULL;
    tAccessEntry       *pAccessEntry = NULL;

    Group.pu1_OctetList = au1Group;
    Group.i4_Length = (INT4) STRLEN (au1Group);

    TMO_SLL_Scan (&gSnmpAccessEntry, pListNode, tTMO_SLL_NODE *)
    {
        pAccessEntry = (tAccessEntry *) pListNode;
        if ((pAccessEntry->i4SecModel == i4SecModel) &&
            (pAccessEntry->i4SecLevel == i4SecLevel) &&
            (SNMPCompareOctetString (pName, &(pAccessEntry->Context)) ==
             SNMP_EQUAL) &&
            (SNMPCompareOctetString (&Group, pAccessEntry->pGroup) ==
             SNMP_EQUAL))

        {
            pAccessEntry->i4Status = SNMP_INACTIVE;
            SNMPDelVacmAccessEntrySll (&(pAccessEntry->link));
            return;
        }
    }
    return;
}

/********************  END OF FILE   ***************************************/
