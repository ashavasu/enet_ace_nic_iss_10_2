/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: stdsnmlw.c,v 1.5 2015/04/28 12:35:03 siva Exp $
 *
 * Description: Routines for Snmp Agent std mib Module 
 *******************************************************************/

#include "snmpcmn.h"
#include "stdsnmwr.h"
#include "stdsnmlw.h"
#include "lldp.h"
UINT4               gu4SnmpSetSerialNo = 0;
/****************************************************************************
 Function    :  nmhValidateIndexInstanceSysORTable
 Input       :  The Indices
                SysORIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhValidateIndexInstanceSysORTable (INT4 i4SysORIndex)
{
    if ((i4SysORIndex < SNMP_ONE) || (i4SysORIndex > MAX_SYSOR_TABLE_ENTRY))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexSysORTable
 Input       :  The Indices
                SysORIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexSysORTable (INT4 *pi4SysORIndex)
{
    INT4                i4Index = 0;
    for (i4Index = SNMP_ZERO; i4Index < MAX_SYSOR_TABLE_ENTRY; i4Index++)
    {
        if (gaSnmpSysorTable[i4Index].i4Status == SNMP_ACTIVE)
        {
            *pi4SysORIndex = i4Index + SNMP_ONE;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexSysORTable
 Input       :  The Indices
                SysORIndex
                nextSysORIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexSysORTable (INT4 i4SysORIndex, INT4 *pi4NextSysORIndex)
{
    INT4                i4Count;

    if (i4SysORIndex <= 0)
    {
        return SNMP_FAILURE;
    }
    while (i4SysORIndex < MAX_SYSOR_TABLE_ENTRY)
    {
        if (gaSnmpSysorTable[i4SysORIndex - SNMP_ONE].i4Status == SNMP_ACTIVE)
        {
            for (i4Count = i4SysORIndex + SNMP_ONE;
                 i4Count < MAX_SYSOR_TABLE_ENTRY; i4Count++)
            {
                if (gaSnmpSysorTable[i4Count - SNMP_ONE].i4Status ==
                    SNMP_ACTIVE)
                {
                    *pi4NextSysORIndex = i4Count;
                    return SNMP_SUCCESS;
                }
            }
            return SNMP_FAILURE;
        }
        i4SysORIndex++;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetSysORID
 Input       :  The Indices
                SysORIndex

                The Object 
                retValSysORID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSysORID (INT4 i4SysORIndex, tSNMP_OID_TYPE * pRetValSysORID)
{
    if (gaSnmpSysorTable[i4SysORIndex - SNMP_ONE].i4Status == SNMP_ACTIVE)
    {
        MEMCPY (pRetValSysORID->pu4_OidList,
                gaSnmpSysorTable[i4SysORIndex - SNMP_ONE].au4SysOrId,
                gaSnmpSysorTable[i4SysORIndex - SNMP_ONE].u4OidLen *
                sizeof (UINT4));
        pRetValSysORID->u4_Length =
            gaSnmpSysorTable[i4SysORIndex - SNMP_ONE].u4OidLen;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetSysORDescr
 Input       :  The Indices
                SysORIndex

                The Object 
                retValSysORDescr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSysORDescr (INT4 i4SysORIndex,
                  tSNMP_OCTET_STRING_TYPE * pRetValSysORDescr)
{
    if (gaSnmpSysorTable[i4SysORIndex - SNMP_ONE].i4Status == SNMP_ACTIVE)
    {
        pRetValSysORDescr->i4_Length =
            (INT4) STRLEN (gaSnmpSysorTable[i4SysORIndex - SNMP_ONE].
                           au1SysOrDescr);
        if (pRetValSysORDescr->i4_Length < SNMP_MAX_OCTETSTRING_SIZE)    /*kloc */
            MEMCPY (pRetValSysORDescr->pu1_OctetList,
                    gaSnmpSysorTable[i4SysORIndex - SNMP_ONE].au1SysOrDescr,
                    pRetValSysORDescr->i4_Length);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetSysORUpTime
 Input       :  The Indices
                SysORIndex

                The Object 
                retValSysORUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSysORUpTime (INT4 i4SysORIndex, UINT4 *pu4RetValSysORUpTime)
{
    if (gaSnmpSysorTable[i4SysORIndex - SNMP_ONE].i4Status == SNMP_ACTIVE)
    {
        *pu4RetValSysORUpTime =
            (gaSnmpSysorTable[i4SysORIndex - SNMP_ONE].u4SysOrUpTime);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetSysDescr
 Input       :  The Indices

                The Object 
                retValSysDescr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSysDescr (tSNMP_OCTET_STRING_TYPE * pRetValSysDescr)
{
    pRetValSysDescr->i4_Length = (INT4) STRLEN (gSnmpSystem.au1SysDescr);
    STRNCPY (pRetValSysDescr->pu1_OctetList,
             gSnmpSystem.au1SysDescr, pRetValSysDescr->i4_Length);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSysObjectID
 Input       :  The Indices

                The Object 
                retValSysObjectID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSysObjectID (tSNMP_OID_TYPE * pRetValSysObjectID)
{
    MEMCPY (pRetValSysObjectID->pu4_OidList,
            gSnmpSystem.au4SysObjectId, gSnmpSystem.u4OidLen * sizeof (UINT4));
    pRetValSysObjectID->u4_Length = gSnmpSystem.u4OidLen;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSysUpTime
 Input       :  The Indices

                The Object 
                retValSysUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSysUpTime (UINT4 *pu4RetValSysUpTime)
{
    UINT4               u4TimeTks = 0;
    GET_TIME_TICKS (&u4TimeTks);
    *pu4RetValSysUpTime = u4TimeTks - gSnmpSystem.u4SysUpTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetSysContact
 Input       :  The Indices

                The Object 
                retValSysContact
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSysContact (tSNMP_OCTET_STRING_TYPE * pRetValSysContact)
{
    pRetValSysContact->i4_Length = (INT4) STRLEN (gSnmpSystem.au1SysContact);
    STRNCPY (pRetValSysContact->pu1_OctetList,
             gSnmpSystem.au1SysContact, pRetValSysContact->i4_Length);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetSysName
Input       :  The Indices

                The Object 
                retValSysName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSysName (tSNMP_OCTET_STRING_TYPE * pRetValSysName)
{
    pRetValSysName->i4_Length = (INT4) STRLEN (gSnmpSystem.au1SysName);
    STRNCPY (pRetValSysName->pu1_OctetList,
             gSnmpSystem.au1SysName, pRetValSysName->i4_Length);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSysLocation
 Input       :  The Indices

                The Object 
                retValSysLocation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSysLocation (tSNMP_OCTET_STRING_TYPE * pRetValSysLocation)
{
    pRetValSysLocation->i4_Length = (INT4) STRLEN (gSnmpSystem.au1SysLocation);
    STRNCPY (pRetValSysLocation->pu1_OctetList,
             gSnmpSystem.au1SysLocation, pRetValSysLocation->i4_Length);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSysServices
 Input       :  The Indices

                The Object 
                retValSysServices
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSysServices (INT4 *pi4RetValSysServices)
{
    *pi4RetValSysServices = (INT4) gSnmpSystem.u4SysServices;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSysORLastChange
 Input       :  The Indices

                The Object 
                retValSysORLastChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSysORLastChange (UINT4 *pu4RetValSysORLastChange)
{
    INT4                i4Index = 0;
    UINT4               u4TimeTks = 0, u4MaxValue = 0;
    *pu4RetValSysORLastChange = SNMP_ZERO;
    for (i4Index = SNMP_ZERO; i4Index < MAX_SYSOR_TABLE_ENTRY; i4Index++)
    {
        if (gaSnmpSysorTable[i4Index].i4Status == SNMP_ACTIVE)
        {
            if (gaSnmpSysorTable[i4Index].u4SysOrUpTime > u4MaxValue)
            {
                u4MaxValue = gaSnmpSysorTable[i4Index].u4SysOrUpTime;
            }
        }
    }
    if (u4MaxValue != SNMP_ZERO)
    {
        GET_TIME_TICKS (&u4TimeTks);
        *pu4RetValSysORLastChange = u4TimeTks - u4MaxValue;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSysContact
 Input       :  The Indices

                The Object 
                setValSysContact
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSysContact (tSNMP_OCTET_STRING_TYPE * pSetValSysContact)
{

    STRNCPY (gSnmpSystem.au1SysContact,
             pSetValSysContact->pu1_OctetList, pSetValSysContact->i4_Length);
    gSnmpSystem.au1SysContact[pSetValSysContact->i4_Length] = '\0';
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSysName
 Input       :  The Indices

                The Object 
                setValSysName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSysName (tSNMP_OCTET_STRING_TYPE * pSetValSysName)
{
    STRNCPY (gSnmpSystem.au1SysName,
             pSetValSysName->pu1_OctetList, pSetValSysName->i4_Length);
    gSnmpSystem.au1SysName[pSetValSysName->i4_Length] = '\0';
    LldpApiNotifySysName (pSetValSysName->pu1_OctetList);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSysLocation
 Input       :  The Indices

                The Object 
                setValSysLocation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSysLocation (tSNMP_OCTET_STRING_TYPE * pSetValSysLocation)
{
    STRNCPY (gSnmpSystem.au1SysLocation,
             pSetValSysLocation->pu1_OctetList, pSetValSysLocation->i4_Length);
    gSnmpSystem.au1SysLocation[pSetValSysLocation->i4_Length] = '\0';
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SysContact
 Input       :  The Indices

                The Object 
                testValSysContact
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SysContact (UINT4 *pu4ErrorCode,
                     tSNMP_OCTET_STRING_TYPE * pTestValSysContact)
{
    if ((pTestValSysContact->i4_Length < SNMP_ZERO)
        || (pTestValSysContact->i4_Length >= MAX_STR_OCTET_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (SNMPCheckForNVTChars (pTestValSysContact->pu1_OctetList,
                              pTestValSysContact->i4_Length) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SysName
 Input       :  The Indices

                The Object 
                testValSysName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SysName (UINT4 *pu4ErrorCode,
                  tSNMP_OCTET_STRING_TYPE * pTestValSysName)
{
    if ((pTestValSysName->i4_Length < SNMP_ZERO) ||
        (pTestValSysName->i4_Length >= MAX_STR_OCTET_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    if (SNMPCheckForNVTChars (pTestValSysName->pu1_OctetList,
                              pTestValSysName->i4_Length) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SysLocation
 Input       :  The Indices

                The Object 
                testValSysLocation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SysLocation (UINT4 *pu4ErrorCode,
                      tSNMP_OCTET_STRING_TYPE * pTestValSysLocation)
{
    if ((pTestValSysLocation->i4_Length < SNMP_ZERO) ||
        (pTestValSysLocation->i4_Length >= MAX_STR_OCTET_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    if (SNMPCheckForNVTChars (pTestValSysLocation->pu1_OctetList,
                              pTestValSysLocation->i4_Length) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2SysContact
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SysContact (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2SysName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SysName (UINT4 *pu4ErrorCode,
                 tSnmpIndexList * pSnmpIndexList, tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2SysLocation
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SysLocation (UINT4 *pu4ErrorCode,
                     tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpInPkts
 Input       :  The Indices

                The Object 
                retValSnmpInPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpInPkts (UINT4 *pu4RetValSnmpInPkts)
{
    *pu4RetValSnmpInPkts = gSnmpStat.u4SnmpInPkts;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpOutPkts
 Input       :  The Indices

                The Object 
                retValSnmpOutPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpOutPkts (UINT4 *pu4RetValSnmpOutPkts)
{
    *pu4RetValSnmpOutPkts = gSnmpStat.u4SnmpOutPkts;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpInBadVersions
 Input       :  The Indices

                The Object 
                retValSnmpInBadVersions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpInBadVersions (UINT4 *pu4RetValSnmpInBadVersions)
{
    *pu4RetValSnmpInBadVersions = gSnmpStat.u4SnmpInBadVersions;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpInBadCommunityNames
 Input       :  The Indices

                The Object 
                retValSnmpInBadCommunityNames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpInBadCommunityNames (UINT4 *pu4RetValSnmpInBadCommunityNames)
{
    *pu4RetValSnmpInBadCommunityNames = gSnmpStat.u4SnmpInBadCommunityNames;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpInBadCommunityUses
 Input       :  The Indices

                The Object 
                retValSnmpInBadCommunityUses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpInBadCommunityUses (UINT4 *pu4RetValSnmpInBadCommunityUses)
{
    *pu4RetValSnmpInBadCommunityUses = gSnmpStat.u4SnmpInBadCommunityUses;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpInASNParseErrs
 Input       :  The Indices

                The Object 
                retValSnmpInASNParseErrs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpInASNParseErrs (UINT4 *pu4RetValSnmpInASNParseErrs)
{
    *pu4RetValSnmpInASNParseErrs = gSnmpStat.u4SnmpInASNParseErrs;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpInBadTypes
 Input       :  The Indices

                The Object
                retValSnmpInBadTypes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpInBadTypes (UINT4 *pu4RetValSnmpInBadTypes)
{
    *pu4RetValSnmpInBadTypes = gSnmpStat.u4SnmpInBadTypes;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpInTooBigs
 Input       :  The Indices

                The Object 
                retValSnmpInTooBigs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpInTooBigs (UINT4 *pu4RetValSnmpInTooBigs)
{
    *pu4RetValSnmpInTooBigs = gSnmpStat.u4SnmpInTooBigs;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpInNoSuchNames
 Input       :  The Indices

                The Object 
                retValSnmpInNoSuchNames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpInNoSuchNames (UINT4 *pu4RetValSnmpInNoSuchNames)
{
    *pu4RetValSnmpInNoSuchNames = gSnmpStat.u4SnmpInNoSuchNames;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpInBadValues
 Input       :  The Indices

                The Object 
                retValSnmpInBadValues
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpInBadValues (UINT4 *pu4RetValSnmpInBadValues)
{
    *pu4RetValSnmpInBadValues = gSnmpStat.u4SnmpInBadValues;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpInReadOnlys
 Input       :  The Indices

                The Object 
                retValSnmpInReadOnlys
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpInReadOnlys (UINT4 *pu4RetValSnmpInReadOnlys)
{
    *pu4RetValSnmpInReadOnlys = gSnmpStat.u4SnmpInReadOnlys;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpInGenErrs
 Input       :  The Indices

                The Object 
                retValSnmpInGenErrs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpInGenErrs (UINT4 *pu4RetValSnmpInGenErrs)
{
    *pu4RetValSnmpInGenErrs = gSnmpStat.u4SnmpInGenErrs;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpInTotalReqVars
 Input       :  The Indices

                The Object 
                retValSnmpInTotalReqVars
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpInTotalReqVars (UINT4 *pu4RetValSnmpInTotalReqVars)
{
    *pu4RetValSnmpInTotalReqVars = gSnmpStat.u4SnmpInTotalReqVars;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpInTotalSetVars
 Input       :  The Indices

                The Object 
                retValSnmpInTotalSetVars
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpInTotalSetVars (UINT4 *pu4RetValSnmpInTotalSetVars)
{
    *pu4RetValSnmpInTotalSetVars = gSnmpStat.u4SnmpInTotalSetVars;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpInGetRequests
 Input       :  The Indices

                The Object 
                retValSnmpInGetRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpInGetRequests (UINT4 *pu4RetValSnmpInGetRequests)
{
    *pu4RetValSnmpInGetRequests = gSnmpStat.u4SnmpInGetRequests;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpInGetNexts
 Input       :  The Indices

                The Object 
                retValSnmpInGetNexts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpInGetNexts (UINT4 *pu4RetValSnmpInGetNexts)
{
    *pu4RetValSnmpInGetNexts = gSnmpStat.u4SnmpInGetNexts;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpInSetRequests
 Input       :  The Indices

                The Object 
                retValSnmpInSetRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpInSetRequests (UINT4 *pu4RetValSnmpInSetRequests)
{
    *pu4RetValSnmpInSetRequests = gSnmpStat.u4SnmpInSetRequests;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpInGetResponses
 Input       :  The Indices

                The Object 
                retValSnmpInGetResponses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpInGetResponses (UINT4 *pu4RetValSnmpInGetResponses)
{
    *pu4RetValSnmpInGetResponses = gSnmpStat.u4SnmpInGetResponses;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpInTraps
 Input       :  The Indices

                The Object 
                retValSnmpInTraps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpInTraps (UINT4 *pu4RetValSnmpInTraps)
{
    *pu4RetValSnmpInTraps = gSnmpStat.u4SnmpInTraps;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpOutTooBigs
 Input       :  The Indices

                The Object 
                retValSnmpOutTooBigs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpOutTooBigs (UINT4 *pu4RetValSnmpOutTooBigs)
{
    *pu4RetValSnmpOutTooBigs = gSnmpStat.u4SnmpOutTooBigs;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpOutNoSuchNames
 Input       :  The Indices

                The Object 
                retValSnmpOutNoSuchNames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpOutNoSuchNames (UINT4 *pu4RetValSnmpOutNoSuchNames)
{
    *pu4RetValSnmpOutNoSuchNames = gSnmpStat.u4SnmpOutNoSuchNames;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpOutBadValues
 Input       :  The Indices

                The Object 
                retValSnmpOutBadValues
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpOutBadValues (UINT4 *pu4RetValSnmpOutBadValues)
{
    *pu4RetValSnmpOutBadValues = gSnmpStat.u4SnmpOutBadValues;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpOutReadOnlys
 Input       :  The Indices

                The Object
                retValSnmpOutReadOnlys
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpOutReadOnlys (UINT4 *pu4RetValSnmpOutReadOnlys)
{
    *pu4RetValSnmpOutReadOnlys = gSnmpStat.u4SnmpOutReadOnlys;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpOutGenErrs
 Input       :  The Indices

                The Object 
                retValSnmpOutGenErrs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpOutGenErrs (UINT4 *pu4RetValSnmpOutGenErrs)
{
    *pu4RetValSnmpOutGenErrs = gSnmpStat.u4SnmpOutGenErrs;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpOutGetRequests
 Input       :  The Indices

                The Object 
                retValSnmpOutGetRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpOutGetRequests (UINT4 *pu4RetValSnmpOutGetRequests)
{
    *pu4RetValSnmpOutGetRequests = gSnmpStat.u4SnmpOutGetRequests;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpOutGetNexts
 Input       :  The Indices

                The Object 
                retValSnmpOutGetNexts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpOutGetNexts (UINT4 *pu4RetValSnmpOutGetNexts)
{
    *pu4RetValSnmpOutGetNexts = gSnmpStat.u4SnmpOutGetNexts;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpOutSetRequests
 Input       :  The Indices

                The Object 
                retValSnmpOutSetRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpOutSetRequests (UINT4 *pu4RetValSnmpOutSetRequests)
{
    *pu4RetValSnmpOutSetRequests = gSnmpStat.u4SnmpOutSetRequests;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpOutGetResponses
 Input       :  The Indices

                The Object 
                retValSnmpOutGetResponses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpOutGetResponses (UINT4 *pu4RetValSnmpOutGetResponses)
{
    *pu4RetValSnmpOutGetResponses = gSnmpStat.u4SnmpOutGetResponses;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpOutTraps
 Input       :  The Indices

                The Object 
                retValSnmpOutTraps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpOutTraps (UINT4 *pu4RetValSnmpOutTraps)
{
    *pu4RetValSnmpOutTraps = gSnmpStat.u4SnmpOutTraps;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpEnableAuthTraps
 Input       :  The Indices

                The Object 
                retValSnmpEnableAuthTraps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpEnableAuthenTraps (INT4 *pi4RetValSnmpEnableAuthTraps)
{
    *pi4RetValSnmpEnableAuthTraps = gSnmpStat.i4SnmpEnableAuthTraps;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetSnmpEnableAuthenTraps
 Input       :  The Indices

                The Object 
                setValSnmpEnableAuthTraps
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpEnableAuthenTraps (INT4 i4SetValSnmpEnableAuthTraps)
{
    gSnmpStat.i4SnmpEnableAuthTraps = i4SetValSnmpEnableAuthTraps;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpEnableAuthenTraps
 Input       :  The Indices

                The Object 
                testValSnmpEnableAuthTraps
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpEnableAuthenTraps (UINT4 *pu4ErrorCode,
                                INT4 i4TestValSnmpEnableAuthTraps)
{
    if ((i4TestValSnmpEnableAuthTraps == AUTHTRAP_ENABLE) ||
        (i4TestValSnmpEnableAuthTraps == AUTHTRAP_DISABLE))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2SnmpEnableAuthenTraps
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SnmpEnableAuthenTraps (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpSilentDrops
 Input       :  The Indices

                The Object 
                retValSnmpSilentDrops
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpSilentDrops (UINT4 *pu4RetValSnmpSilentDrops)
{
    *pu4RetValSnmpSilentDrops = gSnmpStat.u4SnmpSilentDrops;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpProxyDrops
 Input       :  The Indices

                The Object 
                retValSnmpProxyDrops
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpProxyDrops (UINT4 *pu4RetValSnmpProxyDrops)
{
    *pu4RetValSnmpProxyDrops = gSnmpStat.u4SnmpProxyDrops;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpSetSerialNo
 Input       :  The Indices

                The Object 
                retValSnmpSetSerialNo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpSetSerialNo (INT4 *pi4RetValSnmpSetSerialNo)
{
    *pi4RetValSnmpSetSerialNo = (INT4) gu4SnmpSetSerialNo;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpSetSerialNo
 Input       :  The Indices

                The Object 
                setValSnmpSetSerialNo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpSetSerialNo (INT4 i4SetValSnmpSetSerialNo)
{
    gu4SnmpSetSerialNo = (UINT4) i4SetValSnmpSetSerialNo;
    gu4SnmpSetSerialNo++;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpSetSerialNo
 Input       :  The Indices

                The Object 
                testValSnmpSetSerialNo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpSetSerialNo (UINT4 *pu4ErrorCode, INT4 i4TestValSnmpSetSerialNo)
{
    if (gu4SnmpSetSerialNo != (UINT4) i4TestValSnmpSetSerialNo)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2SnmpSetSerialNo
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SnmpSetSerialNo (UINT4 *pu4ErrorCode,
                         tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
