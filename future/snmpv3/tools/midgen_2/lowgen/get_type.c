/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: get_type.c,v 1.4 2015/04/28 14:43:31 siva Exp $
 *
 * Description: This File Contains the function Which     
 *              Parse the apsyntax.def file and store 
 *              Parsed Values in the Global TYPE TABLE
 *******************************************************************/
# include "include.h"
# include "extern.h"

/******************************************************************************
*      function Name        : initialize_type_table                           *
*      Role of the function : This fn searches for the New Type in the        *
*                             file apsyntax.def and stores it in the Type     *
*                             Table Array if the New Type is found.           *
*      Formal Parameters    : pi1_datastring                                  *
*      Global Variables     : type_table, i4_global_newcount                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
initialize_type_table (UINT1 *pi1_datastring)
{
    INT4                i4_flag = FALSE;
    UINT1              *pi1_string;
    UINT1              *pi1_temp;
    FILE               *f1;

    /* This Fn finds whether the the data type has already occured.  */
    i4_flag = find_already_datatype_present (pi1_datastring);
    if (i4_flag == TRUE)
    {
        return;
    }

    f1 = fopen (NEW_SYNTAX_FILE_NAME, READ_MODE);
    if (f1 == NULL)
    {
        printf ("\n\tERROR : Can't find the apsyntax.def file\n");
        clear_directory ();
        exit (NEGATIVE);
    }

    /* This Fn searches for the given data type in the file.  */
    pi1_string = search_dtype (pi1_datastring, f1);
    if (pi1_string == NULL)
    {
        printf ("\n\n\tERROR :Can't find the New Data Type in the file\n");
        printf ("\n\tThe Data Type is  %s\n", pi1_datastring);
        clear_directory ();
        exit (NEGATIVE);
    }
    else
    {
        /*  Storing the Value of the New Type name in the global Type Table. */
        strcpy (type_table[i4_global_newcount].type_name, pi1_datastring);

        do
        {
            if (!check_empty_line (pi1_string))
            {
                continue;
            }
            else
            {
                pi1_string = remove_comment (pi1_string);

                /*  Searches for the presence of INTEGER.  */
                if (strstr (pi1_string, STR_INTEGER_TYPE))
                {

                    /*  Storing the Size & Type of the New Data Type. */
                    type_table[i4_global_newcount].size = ONE;
                    type_table[i4_global_newcount].primitive_type = INTEGER;
                    break;
                }
                else if (strstr (pi1_string, STR_OCTET_STR_TYPE))
                {

                    type_table[i4_global_newcount].primitive_type =
                        OCTET_STRING;
                    if (strstr (pi1_string, STR_CONST_SIZE) != NULL)
                    {
                        pi1_temp = strstr (pi1_string, STR_CONST_SIZE);
                        pi1_temp = strstr (pi1_temp, OPEN_BRACKET);
                        pi1_temp++;
                        pi1_temp = strtok (pi1_temp, CLOSE_BRACKET);
                        if (strstr (pi1_temp, DOT_STR))
                        {
                            pi1_temp = strrchr (pi1_temp, DOT_CHAR);
                            pi1_temp++;
                            /*  Range Of OctetStr Given. so Add One to Len */
                            type_table[i4_global_newcount].size =
                                atoi (pi1_temp) + ONE;
                        }
                        else
                        {
                            type_table[i4_global_newcount].size =
                                atoi (pi1_temp);
                        }
                    }
                    else
                    {
                        type_table[i4_global_newcount].size = ZERO;
                    }

                    break;
                }
                else if (strstr (pi1_string, STR_OID_TYPE))
                {

                    type_table[i4_global_newcount].primitive_type =
                        OBJECT_IDENTIFIER;
                    type_table[i4_global_newcount].size = ZERO;
                    break;
                }
                else if ((strstr (pi1_string, STR_NETWORKADDR_TYPE))
                         || (strstr (pi1_string, STR_IPADDR_TYPE)))
                {

                    type_table[i4_global_newcount].primitive_type = ADDRESS;
                    type_table[i4_global_newcount].size = FOUR;
                    break;
                }
                else if (strstr (pi1_string, STR_MACADDR_TYPE))
                {

                    type_table[i4_global_newcount].primitive_type = MacAddress;
                    type_table[i4_global_newcount].size = SIX;
                    break;
                }
                else if ((strstr (pi1_string, STR_TIMETICKS_TYPE))
                         || (strstr (pi1_string, STR_UNSIGNED32_TYPE))
                         || (strstr (pi1_string, STR_GAUGE32_TYPE))
                         || (strstr (pi1_string, STR_COUNTER32_TYPE))
                         || (strstr (pi1_string, STR_COUNTER64_TYPE)))
                {

                    if (strstr (pi1_string, STR_TIMETICKS_TYPE))
                    {
                        strcpy (type_table[i4_global_newcount].
                                primitive_type_name, STR_TIMETICKS_TYPE);
                    }
                    else if (strstr (pi1_string, STR_GAUGE32_TYPE))
                    {
                        strcpy (type_table[i4_global_newcount].
                                primitive_type_name, STR_GAUGE32_TYPE);
                    }
                    else if (strstr (pi1_string, STR_COUNTER32_TYPE))
                    {
                        strcpy (type_table[i4_global_newcount].
                                primitive_type_name, STR_COUNTER32_TYPE);
                    }
                    else if (strstr (pi1_string, STR_COUNTER64_TYPE))
                    {
                        strcpy (type_table[i4_global_newcount].
                                primitive_type_name, STR_COUNTER64_TYPE);
                    }
                    else if (strstr (pi1_string, STR_UNSIGNED32_TYPE))
                    {

                        strcpy (type_table[i4_global_newcount].
                                primitive_type_name, STR_UNSIGNED32_TYPE);
                    }
                    type_table[i4_global_newcount].primitive_type = POS_INTEGER;
                    type_table[i4_global_newcount].size = ONE;
                    break;
                }
            }
            pi1_string = get_line (f1);
        }
        while (TRUE);

        fclose (f1);
    }
    i4_global_newcount++;

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : find_already_datatype_present                   *
*      Role of the function : This fn searches for the New Type in the        *
*                             global array name_new_datatype whether it has   *
*                             already occured in the MIBand if the New        *
*                             Type is present it returns TRUE else FALSE.     *
*      Formal Parameters    : pi1_datastring                                  *
*      Global Variables     : name_new_datatype , i4_global_newcount          *
*      Use of Recursion     : None                                            *
*      Return Value         : TRUE (if found) else FALSE.                     *
******************************************************************************/
INT4
find_already_datatype_present (UINT1 *pi1_datastring)
{
    INT4                i4_count;

    for (i4_count = FALSE; i4_count < i4_global_newcount; i4_count++)
    {
        if (!strcmp (name_new_datatype[i4_count], pi1_datastring))
        {
            return TRUE;
        }
    }

    return FALSE;

}                                /* End Of Function */

/******************************************************************************
*      function Name        : search_dtype                                    *
*      Role of the function : This fn searches for the New Type in the        *
*                             apsytax.def file if present returns the Line    *
*                             else return NULL.                               *
*      Formal Parameters    : pi1_datastring  , f1                            *
*      Global Variables     : name_new_datatype                               *
*      Use of Recursion     : None                                            *
*      Return Value         : Line (if type found) else NULL.                 *
******************************************************************************/
UINT1              *
search_dtype (UINT1 *pi1_datastring, FILE * f1)
{
    UINT1              *pi1_temp;

    while (!feof (f1))
    {
        pi1_temp = get_line (f1);

        if (!check_empty_line (pi1_temp))
        {
            continue;
        }
        else
        {
            /*
             *   To check whether the New Type is present
             *  in the apsyntax.def file.
             */
            if (!strstr (pi1_temp, pi1_datastring))
            {
                continue;
            }
            else
            {
                /*  The DATATYPE is present.  */
                pi1_temp = remove_comment (pi1_temp);

                /*  
                 *  Store the NEW DATA TYPE in the global string array
                 *  which has all the previous occured data types.
                 */
                strcpy (name_new_datatype[i4_global_newcount], pi1_datastring);
                return pi1_temp;
            }
        }
    }                            /* End of while loop. */

    return NULL;

}                                /* End of the function. */
