/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: include.h,v 1.5 2015/04/28 14:43:31 siva Exp $
 *
 * Description:This File contains all the Header Files       
 *             needed for the Middle Level Code Generator. 
 *******************************************************************/
		 

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <ctype.h>

#include "std_tdfs.h"
#include "defines.h"
#include "type.h"
#include "proto.h"
#include "extdefn.h"

#define MAX_DESC_LEN 128      /* max. length of descriptor */
typedef struct impindices_list {
          INT1 name[MAX_DESC_LEN];
	  struct impindices_list *next;
} tIMPINDICES_LIST;

