 /********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: wrap_code.c,v 1.8 2015/04/28 14:43:31 siva Exp $
 *
 * Description: 
 *              
 *******************************************************************/

#include "include.h"
#include "extern.h"

tIMPINDICES_LIST   *pImpIndx = NULL;
extern INT1         FILENAME;
extern INT1         i1_file_name[MAX_LINE];
extern INT1         i1_src_path[MAX_LINE];
extern INT1         i1_inc_path[MAX_LINE];
extern INT4         i4TotalCount;
extern INT1         gi1TableEnable;
static INT1         gi1RegisterMib = 0;

FILE               *fWrapC = NULL;
FILE               *fWrapH = NULL;
FILE               *fDb = NULL;
FILE               *fISaveH = NULL;
INT4                i4NoOfIndex = 0;
INT1                MibOid[MAX_LINE];
UINT4               Mib4Oid[MAX_LINE];
INT1                au1MibName[MAX_LINE];
INT1                i1_Capfilename[MAX_LINE];
INT1                au1AccessFnName[MAX_LINE];
INT1                MibFlag = 0;
INT4                i4MibOidLen = 0;
void                wrap_Table_GetRoutine (INT4 i4MibPointer);
void                wrap_Table_SetRoutine (INT4 i4MibPointer);
void                wrap_Table_TestRoutine (INT4 i4MibPointer);
void                wrap_validateIndexInstance ();
void                mbdb_MibEntrycode_gen ();
void                mbdb_MibEntrycode_gen_table ();
void                mbdb_formIndexArray ();
void                CreateOidDatabase (INT1 *);
void                CreateDefValTable (INT1 *);
INT4                GetOidLenFromName (INT1 *pu1Name);
void                FormOidList (UINT1 *);
void                FormOidArray (UINT1 *pu1Oid, UINT4 *pu4Oid);
INT4                ConvSubOIDToLong (UINT1 **ppu1TempPtr);
INT1               *GetOidPointer (INT1 *pu1Name);
INT1               *GetDefvalFromName (INT1 *pu1Name);
void                WriteDB (INT1 *, INT1 *, INT1 *);
INT1                i1_tmp_file_name[MAX_LINE];

INT1                GetImpliedIndexMatch (INT1 *pi1Name);
typedef struct OID
{
    INT1                name[MAX_LINE];
    INT1                oid[MAX_LINE];
    INT4                i4Len;
}
tOid;

tOid                OidTable[MAX_OBJECTS * 50];
tOid                TableOidDatabase[MAX_TABLES * 50];

typedef struct DEFVAL
{
    INT1                name[MAX_LINE];
    INT1                def_val[MAX_LINE];
} tDefval;
tDefval             DefvalTable[MAX_OBJECTS * 50];

UINT1               au1IndexArray[MAX_TABLES][MAX_LINE];
UINT1               au1TableArray[MAX_TABLES][MAX_LINE];
UINT1               au1ScalarArray[MAX_TABLES][MAX_LINE];
UINT4               gu4IndexCount = 0;
UINT4               gu4ScalarCount = 0;

typedef struct MBDB
{
    UINT1               au1Oid[MAX_LINE];
    UINT4               u4Len;
    UINT1              *pu1String;
    UINT4               au4Oid[MAX_LINE];
}
tMbdb;
tMbdb               gMbdb[MAX_OBJECTS * 50];
UINT4               gu4ObjectCount = 0;

#define SWAP_MBDB(x,y) {tMbdb t; memcpy(&t,&x,sizeof(tMbdb)); \
                        memcpy(&x,&y,sizeof(tMbdb));\
                        memcpy(&y,&t,sizeof(tMbdb));}

#define MEM_MALLOC(Size,Type)              (Type *)malloc(Size)

INT1                au1MibDesc[MAX_LINE];
void
wrap_file_open (INT1 *p1_in_file, INT1 *pi1_src_path, INT1 *pi1_inc_path)
{
    UINT1               au1Array[MAX_LINE];
    INT1                i1_filename[MAX_LINE];
    INT1               *p1_temp;
    INT4                i4Count = 0;

    bzero (i1_filename, MAX_LINE);
    strcpy (i1_tmp_file_name, p1_in_file);
    strcpy (i1_filename, p1_in_file);
    p1_temp = strrchr (i1_filename, '/');
    if (p1_temp != NULL)
    {
        p1_temp++;
    }
    else
    {
        p1_temp = i1_filename;
    }
    strcpy (au1MibDesc, p1_temp);
    while (au1MibDesc[i4Count] != '\0')
    {
        if (au1MibDesc[i4Count] == '.')
        {
            au1MibDesc[i4Count] = '\0';
            break;
        }
        i4Count++;
    }
    i4Count = 0;
    /*
     *This is added to reduce the length of the file name to five to bring
     *the over all length of the reduced file name to eight
     */
    p1_temp[6] = '\0';

    if (FILENAME == 1)
    {
        p1_temp = i1_file_name;
    }
    while (p1_temp[i4Count] != '\0')
    {
        if (p1_temp[i4Count] == '.')
        {
            p1_temp[i4Count] = '\0';
            break;
        }
        i4Count++;
    }
    i4Count = 0;
    strcpy (au1MibName, p1_temp);
    bzero (i1_Capfilename, MAX_LINE);
    strcpy (i1_Capfilename, p1_temp);
    while (i1_Capfilename[i4Count] != '\0')
    {
        i1_Capfilename[i4Count] = toupper (i1_Capfilename[i4Count]);
        i4Count++;
    }

    i4Count = 1;
    bzero (au1AccessFnName, MAX_LINE);
    au1AccessFnName[0] = i1_Capfilename[0];
    while (i1_Capfilename[i4Count] != '\0')
    {
        au1AccessFnName[i4Count] = tolower (i1_Capfilename[i4Count]);
        i4Count++;
    }

    memset (au1Array, 0, MAX_LINE);
    strcpy (au1Array, pi1_src_path);
    strcat (au1Array, p1_temp);
    strcat (au1Array, "wr.c");
    fWrapC = fopen (au1Array, WRITE_MODE);
    if (fWrapC == NULL)
    {
        printf ("Unable to Open wr.c file in write Mode\n");
        clear_directory ();
        exit (NEGATIVE);
    }

    memset (au1Array, 0, MAX_LINE);
    strcpy (au1Array, pi1_inc_path);
    strcat (au1Array, p1_temp);
    strcat (au1Array, "wr.h");
    fWrapH = fopen (au1Array, WRITE_MODE);
    if (fWrapH == NULL)
    {
        printf ("Unable to Open wr.h file in write Mode\n");
        clear_directory ();
        exit (NEGATIVE);
    }

    fprintf (fWrapH, "#ifndef _%sWR_H\n#define _%sWR_H\n",
             i1_Capfilename, i1_Capfilename);

    fprintf (fWrapC, INCS " \"lr.h\" \n");
    fprintf (fWrapC, INCS " \"fssnmp.h\" \n");
    fprintf (fWrapC, INCS " \"%slw.h\"\n", p1_temp);
    fprintf (fWrapC, INCS " \"%swr.h\"\n", p1_temp);
    fprintf (fWrapC, INCS " \"%sdb.h\"\n", p1_temp);

    if (TRUE == i4_isave_flag)
    {
        memset (au1Array, 0, MAX_LINE);
        strcpy (au1Array, pi1_inc_path);
        strcat (au1Array, p1_temp);
        strcat (au1Array, "cli.h");

        fISaveH = fopen (au1Array, WRITE_MODE);
        if (fISaveH == NULL)
        {
            printf ("Unable to Open cli.h file in write Mode\n");
            clear_directory ();
            exit (NEGATIVE);
        }
        else
        {
            fprintf (fISaveH,
                     "/***************************************************"
                     "*****************\n");
            fprintf (fISaveH,
                     "* Copyright (C) 2006 Aricent Inc . All Rights Reserved\n*\n");
            fprintf (fISaveH,
                     "* $Id: wrap_code.c,v 1.8 2015/04/28 14:43:31 siva Exp $\n*\n",
                     p1_temp);
            fprintf (fISaveH,
                     "* Description: Header file for nmhSet update trigger\n");
            fprintf (fISaveH,
                     "***************************************************"
                     "******************/\n");
        }
    }
    gu4IndexCount = 0;
    gu4ObjectCount = 0;
    CreateOidDatabase (p1_in_file);
    CreateDefValTable (p1_in_file);
}

void
wrap_code_getfirst_next ()
{
    INT4                i4Counter = ZERO, i4Count = ZERO;

    if (p_table_struct->status == STATUS_OBSOLETE)
        return;

    mbdb_formIndexArray ();
    fprintf (fWrapC, "\nINT4 GetNextIndex%s", p_table_struct->table_name);
    fprintf (fWrapC,
             "(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)\n{\n");

    /* ProtoType in Header File */
    fprintf (fWrapH, "INT4 GetNextIndex%s", p_table_struct->table_name);
    fprintf (fWrapH, "(tSnmpIndex *, tSnmpIndex *);\n");
    fprintf (fWrapC, "\tif (pFirstMultiIndex == NULL) \n\t{\n");
    fprintf (fWrapC, "\t\tif (nmhGetFirstIndex%s(\n\t\t\t",
             p_table_struct->table_name);
    /* Arguments to mhGetFirst Index */
    for (i4Counter = ZERO; i4Counter < p_table_struct->no_of_indices;
         i4Counter++)
    {
        for (i4Count = ZERO; i4Count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4Count++)
        {
            if (COMPARE_OBJECT_WITH_INDEX (i4Count, i4Counter) != FAILURE)
            {
                continue;
            }
            switch (p_table_struct->object[i4Count].type)
            {
                case POS_INTEGER:
                case ADDRESS:
                {
                    if ((strcmp
                         (p_table_struct->object[i4Count].type_name,
                          "Counter64")) == 0)
                    {
                        fprintf (fWrapC,
                                 "pNextMultiIndex->pIndex[%d].u8_Counter64Value",
                                 i4Counter);
                    }
                    else
                    {
                        fprintf (fWrapC,
                                 "&(pNextMultiIndex->pIndex[%d].u4_ULongValue)",
                                 i4Counter);
                    }
                    break;
                }
                case INTEGER:
                {
                    fprintf (fWrapC,
                             "&(pNextMultiIndex->pIndex[%d].i4_SLongValue)",
                             i4Counter);
                    break;
                }
                case OCTET_STRING:
                {
                    fprintf (fWrapC,
                             "pNextMultiIndex->pIndex[%d].pOctetStrValue",
                             i4Counter);
                    break;
                }
                case OBJECT_IDENTIFIER:
                {
                    fprintf (fWrapC,
                             "pNextMultiIndex->pIndex[%d].pOidValue",
                             i4Counter);
                    break;
                }
                case MacAddress:
                {
                    fprintf (fWrapC,
                             "(tMacAddr *)pNextMultiIndex->pIndex[%d]."
                             "pOctetStrValue->pu1_OctetList", i4Counter);
                    break;
                }
                default:
                {
                    printf ("Unknown Data Type \n");
                    clear_directory ();
                    exit (NEGATIVE);
                    break;
                }
            }
        }
        if ((i4Counter + 1) < p_table_struct->no_of_indices)
        {
            fprintf (fWrapC, ",\n\t\t\t");
        }
    }
    fprintf (fWrapC, ") == SNMP_FAILURE)\n");
    fprintf (fWrapC, "\t\t{\n");
    fprintf (fWrapC, " \t\t\treturn SNMP_FAILURE;\n\t\t}\n\t}\n");
    fprintf (fWrapC, "\telse\n\t{\n");
    fprintf (fWrapC, "\t\tif (nmhGetNextIndex%s(\n\t\t\t",
             p_table_struct->table_name);

    /* Arguments to mhGetNextFirst Index */
    for (i4Counter = ZERO; i4Counter < p_table_struct->no_of_indices;
         i4Counter++)
    {
        for (i4Count = ZERO; i4Count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4Count++)
        {
            if (COMPARE_OBJECT_WITH_INDEX (i4Count, i4Counter) != FAILURE)
            {
                continue;
            }
            switch (p_table_struct->object[i4Count].type)
            {
                case POS_INTEGER:
                case ADDRESS:
                {
                    if ((strcmp
                         (p_table_struct->object[i4Count].type_name,
                          "Counter64")) == 0)
                    {
                        fprintf (fWrapC,
                                 "pFirstMultiIndex->pIndex[%d].u8_Counter64Value,\n",
                                 i4Counter);
                        fprintf (fWrapC,
                                 "\t\t\tpNextMultiIndex->pIndex[%d].u8_Counter64Value",
                                 i4Counter);
                    }
                    else
                    {
                        fprintf (fWrapC,
                                 "pFirstMultiIndex->pIndex[%d].u4_ULongValue,\n",
                                 i4Counter);
                        fprintf (fWrapC,
                                 "\t\t\t&(pNextMultiIndex->pIndex[%d].u4_ULongValue)",
                                 i4Counter);
                    }
                    break;
                }
                case INTEGER:
                {
                    fprintf (fWrapC,
                             "pFirstMultiIndex->pIndex[%d].i4_SLongValue,\n",
                             i4Counter);
                    fprintf (fWrapC,
                             "\t\t\t&(pNextMultiIndex->pIndex[%d].i4_SLongValue)",
                             i4Counter);
                    break;
                }
                case OCTET_STRING:
                {
                    fprintf (fWrapC,
                             "pFirstMultiIndex->pIndex[%d].pOctetStrValue,\n",
                             i4Counter);
                    fprintf (fWrapC,
                             "\t\t\tpNextMultiIndex->pIndex[%d].pOctetStrValue",
                             i4Counter);
                    break;
                }
                case OBJECT_IDENTIFIER:
                {
                    fprintf (fWrapC,
                             "pFirstMultiIndex->pIndex[%d].pOidValue,\n",
                             i4Counter);
                    fprintf (fWrapC,
                             "\t\t\tpNextMultiIndex->pIndex[%d].pOidValue",
                             i4Counter);
                    break;
                }
                case MacAddress:
                {
                    fprintf (fWrapC,
                             "*(tMacAddr *)pFirstMultiIndex->pIndex[%d]."
                             "pOctetStrValue->pu1_OctetList,\n", i4Counter);
                    fprintf (fWrapC,
                             "\t\t\t(tMacAddr *)pNextMultiIndex->pIndex[%d]."
                             "pOctetStrValue->pu1_OctetList", i4Counter);
                    break;
                }
                default:
                {
                    printf ("Unknown Data Type \n");
                    clear_directory ();
                    exit (NEGATIVE);
                    break;
                }
            }
        }
        if ((i4Counter + 1) < p_table_struct->no_of_indices)
        {
            fprintf (fWrapC, ",\n\t\t\t");
        }
    }
    fprintf (fWrapC, ") == SNMP_FAILURE)\n\t\t{\n");
    fprintf (fWrapC, "\t\t\treturn SNMP_FAILURE;\n");
    fprintf (fWrapC, "\t\t}\n\t}\n\t\n");
    for (i4Counter = ZERO; i4Counter < p_table_struct->no_of_indices;
         i4Counter++)
    {
        for (i4Count = ZERO; i4Count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4Count++)
        {
            if (COMPARE_OBJECT_WITH_INDEX (i4Count, i4Counter) != FAILURE)
            {
                continue;
            }
            if (p_table_struct->object[i4Count].type == MacAddress)
            {
                fprintf (fWrapC,
                         "\t\tpNextMultiIndex->pIndex[%d].pOctetStrValue->i4_Length = 6;\n",
                         i4Counter);
            }
        }
    }
    fprintf (fWrapC, "\treturn SNMP_SUCCESS;\n}\n");
}

void
FormMibOid ()
{
    INT1               *pOid = NULL;
    UINT4               au4Oid[MAX_LINE];

    if (MibFlag == 0)
    {
        pOid = GetOidPointer (i1_module_name);
        if (pOid == NULL)
        {
            printf ("Unable to Form Mib Oid\n");
            clear_directory ();
            exit (NEGATIVE);
        }
        strcpy (MibOid, pOid);
        i4MibOidLen = GetOidLenFromName (i1_module_name);
        FormOidArray (MibOid, Mib4Oid);

        if (gi1TableEnable != 1)
        {
            fprintf (fWrapC, "\n\nVOID Register%s ()\n{\n", i1_Capfilename);
            if ((gi1ContextIdEnable == 1) && (gi1LockEnable == 1))
            {
                fprintf (fWrapC,
                         "\tSNMPRegisterMibWithContextIdAndLock (&%sOID, &%sEntry,"
                         " NULL, NULL, NULL, NULL, SNMP_MSR_TGR_FALSE);\n",
                         au1MibName, au1MibName);
            }
            else if ((gi1ContextIdEnable == 0) && (gi1LockEnable == 1))
            {
                fprintf (fWrapC,
                         "\tSNMPRegisterMibWithLock (&%sOID, &%sEntry,"
                         " NULL, NULL, SNMP_MSR_TGR_FALSE);\n", au1MibName,
                         au1MibName);
            }
            else
            {
                fprintf (fWrapC,
                         "\tSNMPRegisterMib (&%sOID, &%sEntry, SNMP_MSR_TGR_FALSE);\n",
                         au1MibName, au1MibName);
            }
            fprintf (fWrapC,
                     "\tSNMPAddSysorEntry (&%sOID, (const UINT1 *) \"%s\");\n",
                     au1MibName, au1MibDesc);
            fprintf (fWrapC, "}\n\n");
        }
        fprintf (fWrapH, "\nVOID Register%s(VOID);\n", i1_Capfilename);

        fprintf (fWrapH, "\nVOID UnRegister%s(VOID);\n", i1_Capfilename);
        if (gi1TableEnable != 1)
        {
            fprintf (fWrapC, "\n\nVOID UnRegister%s ()\n{\n", i1_Capfilename);
            fprintf (fWrapC,
                     "\tSNMPUnRegisterMib (&%sOID, &%sEntry);\n", au1MibName,
                     au1MibName);
            fprintf (fWrapC,
                     "\tSNMPDelSysorEntry (&%sOID, (const UINT1 *) \"%s\");\n",
                     au1MibName, au1MibDesc);
            fprintf (fWrapC, "}\n\n");
        }
    }
    else
    {
        pOid = GetOidPointer (i1_module_name);
        if (pOid == NULL)
        {
            printf ("Unable to Form Mib Oid\n");
            clear_directory ();
            exit (NEGATIVE);
        }
        FormOidArray (pOid, au4Oid);
        if (memcmp (Mib4Oid, au4Oid, (MAX_LINE * 4)) > 0)
        {
            strcpy (MibOid, pOid);
            i4MibOidLen = GetOidLenFromName (i1_module_name);
            FormOidArray (MibOid, Mib4Oid);
        }
    }
    MibFlag = 1;
}

void
wrap_code_get_all_objects ()
{
    INT4                i4Count = ZERO;
    INT4                i4IndexCount = ZERO;
    if (gi1TableEnable == 1)
    {
        mbdb_MibEntrycode_gen_table ();
    }
    else
    {
        mbdb_MibEntrycode_gen ();
    }
    FormMibOid ();
    for (i4Count = ZERO; i4Count < p_table_struct->no_of_objects; i4Count++)
    {

        /*For Access type access-notify and not-accessible, No need to generate Get routine */
        if ((p_table_struct->object[i4Count].access == NO_ACCESS) ||
            (p_table_struct->object[i4Count].status == STATUS_OBSOLETE))
        {
            continue;
        }
        if (p_table_struct->object[i4Count].access != ACC_FOR_NOTIFY)
        {
            fprintf (fWrapC,
                     "INT4 %sGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)\n{\n",
                     p_table_struct->object[i4Count].object_name);
        }
        /* ProtoType in Header File */
        if (p_table_struct->object[i4Count].access != ACC_FOR_NOTIFY)
        {
            fprintf (fWrapH,
                     "INT4 %sGet(tSnmpIndex *, tRetVal *);\n",
                     p_table_struct->object[i4Count].object_name);
        }
        /* Scalar Objects */
        if (p_table_struct->no_of_indices == 0)
        {
            strcpy (au1ScalarArray[gu4ScalarCount++],
                    p_table_struct->object[i4Count].object_name);
            if (p_table_struct->object[i4Count].type == MacAddress)
            {
                if (p_table_struct->object[i4Count].access != ACC_FOR_NOTIFY)
                {
                    fprintf (fWrapC,
                             "\tpMultiData->pOctetStrValue->i4_Length = 6;\n");
                }
            }
            if (p_table_struct->object[i4Count].access != ACC_FOR_NOTIFY)
            {
                fprintf (fWrapC, "\tUNUSED_PARAM(pMultiIndex);\n");
                fprintf (fWrapC, "\treturn(nmhGet%s(",
                         p_table_struct->object[i4Count].object_name);
            }
            switch (p_table_struct->object[i4Count].type)
            {
                case POS_INTEGER:
                case ADDRESS:
                    if ((strcmp
                         (p_table_struct->object[i4Count].type_name,
                          "Counter64")) == 0)
                    {
                        if (p_table_struct->object[i4Count].access !=
                            ACC_FOR_NOTIFY)
                        {
                            fprintf (fWrapC,
                                     "&(pMultiData->u8_Counter64Value)");
                        }
                    }
                    else
                    {
                        if (p_table_struct->object[i4Count].access !=
                            ACC_FOR_NOTIFY)
                        {
                            fprintf (fWrapC, "&(pMultiData->u4_ULongValue)");
                        }
                    }
                    break;
                case INTEGER:
                    if (p_table_struct->object[i4Count].access !=
                        ACC_FOR_NOTIFY)
                    {
                        fprintf (fWrapC, "&(pMultiData->i4_SLongValue)");
                    }
                    break;
                case OCTET_STRING:
                    if (p_table_struct->object[i4Count].access !=
                        ACC_FOR_NOTIFY)
                    {
                        fprintf (fWrapC, "pMultiData->pOctetStrValue");
                    }
                    break;
                case OBJECT_IDENTIFIER:
                    if (p_table_struct->object[i4Count].access !=
                        ACC_FOR_NOTIFY)
                    {
                        fprintf (fWrapC, "pMultiData->pOidValue");
                    }
                    break;
                case MacAddress:
                    if (p_table_struct->object[i4Count].access !=
                        ACC_FOR_NOTIFY)
                    {
                        fprintf (fWrapC,
                                 "(tMacAddr *)pMultiData->pOctetStrValue->pu1_OctetList");
                    }
                    break;
                default:
                    printf ("Unknown Data Type \n");
                    clear_directory ();
                    exit (NEGATIVE);
                    break;
            }
            if (p_table_struct->object[i4Count].access != ACC_FOR_NOTIFY)
            {
                fprintf (fWrapC, "));\n}\n");
            }
        }
        else                    /* Table Objects */
        {

            /* Get Routine for Index Objects */
            if (find_object_is_index (p_table_struct->object[i4Count].
                                      object_name) == IS_INDEX)
            {
                /* Add Code for nmhValidate Index */
                wrap_validateIndexInstance ();
                for (i4IndexCount = ZERO; i4IndexCount <
                     p_table_struct->no_of_objects +
                     p_table_struct->no_of_imports; i4IndexCount++)
                {
                    if (COMPARE_OBJECT_WITH_INDEX (i4Count, i4IndexCount)
                        != FAILURE)
                    {
                        continue;
                    }

                    switch (p_table_struct->object[i4Count].type)
                    {
                        case POS_INTEGER:
                        case ADDRESS:
                            if ((strcmp
                                 (p_table_struct->object[i4Count].type_name,
                                  "Counter64")) == 0)
                            {
                                fprintf (fWrapC,
                                         "\tMultiData->u8_Counter64Value.msn = pMultiIndex->pIndex[%d]"
                                         ".u8_Counter64Value.msn;\n",
                                         i4IndexCount);
                                fprintf (fWrapC,
                                         "\tMultiData->u8_Counter64Value.lsn = pMultiIndex->pIndex[%d]"
                                         ".u8_Counter64Value.lsn;\n",
                                         i4IndexCount);
                            }
                            else
                            {
                                fprintf (fWrapC, "\tpMultiData->u4_ULongValue ="
                                         "pMultiIndex->pIndex[%d].u4_ULongValue;\n",
                                         i4IndexCount);
                            }
                            break;
                        case INTEGER:
                            fprintf (fWrapC, "\tpMultiData->i4_SLongValue ="
                                     "pMultiIndex->pIndex[%d].i4_SLongValue;\n",
                                     i4IndexCount);
                            break;
                        case OCTET_STRING:
                        case MacAddress:
                            fprintf (fWrapC,
                                     "\tMEMCPY(pMultiData->pOctetStrValue->pu1_OctetList,\n\t"
                                     "pMultiIndex->pIndex[%d].pOctetStrValue->pu1_OctetList,\n\t"
                                     "pMultiIndex->pIndex[%d].pOctetStrValue->i4_Length);\n\t",
                                     i4IndexCount, i4IndexCount);
                            fprintf (fWrapC,
                                     "pMultiData->pOctetStrValue->i4_Length=\n"
                                     "\tpMultiIndex->pIndex[%d].pOctetStrValue->i4_Length;\n",
                                     i4IndexCount);
                            break;
                        case OBJECT_IDENTIFIER:
                            fprintf (fWrapC,
                                     "\tMEMCPY(pMultiData->pOidValue->pu4_OidList,"
                                     "pMultiIndex->pIndex[%d].pOidValue->pu4_OidList,"
                                     "(pMultiIndex->pIndex[%d].pOidValue->u4_Length"
                                     "+sizeof(UINT4)));", i4IndexCount,
                                     i4IndexCount);
                            fprintf (fWrapC,
                                     "pMultiData->pOidValue->u4_Length=\n"
                                     "\tpMultiIndex->pIndex[%d].pOidValue->u4_Length;\n",
                                     i4IndexCount);
                            break;
                        default:
                            printf ("Unknown Data Type \n");
                            clear_directory ();
                            exit (NEGATIVE);
                            break;
                    }
                }
                if (p_table_struct->object[i4Count].access != READ_CREATE)
                {
                    fprintf (fWrapC, "\n\treturn SNMP_SUCCESS;\n");
                }
                /* Get Routine for Index Objects with MAX_ACCESS
                 * as read-create*/
                else
                {
                    wrap_Table_GetRoutine (i4Count);
                }
            }
            else                /* Get Routine for Table Objects */
            {
                /* Add Code for nmhValidate Index */
                wrap_validateIndexInstance ();
                wrap_Table_GetRoutine (i4Count);
            }
            fprintf (fWrapC, "\n}\n");
        }
    }
}

void
wrap_validateIndexInstance ()
{
    INT4                i4Counter = ZERO, i4Count = ZERO;
    fprintf (fWrapC, "\tif (nmhValidateIndexInstance%s(\n",
             p_table_struct->table_name);
    for (i4Counter = ZERO; i4Counter < p_table_struct->no_of_indices;
         i4Counter++)
    {
        for (i4Count = ZERO; i4Count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4Count++)
        {
            if (COMPARE_OBJECT_WITH_INDEX (i4Count, i4Counter) != FAILURE)
            {
                continue;
            }
            switch (p_table_struct->object[i4Count].type)
            {
                case POS_INTEGER:
                case ADDRESS:
                    if ((strcmp
                         (p_table_struct->object[i4Count].type_name,
                          "Counter64")) == 0)
                    {
                        fprintf (fWrapC,
                                 "\t\tpMultiIndex->pIndex[%d].u8_Counter64Value",
                                 i4Counter);
                    }
                    else
                    {
                        fprintf (fWrapC,
                                 "\t\tpMultiIndex->pIndex[%d].u4_ULongValue",
                                 i4Counter);
                    }
                    break;
                case INTEGER:
                    fprintf (fWrapC,
                             "\t\tpMultiIndex->pIndex[%d].i4_SLongValue",
                             i4Counter);
                    break;
                case OCTET_STRING:
                    fprintf (fWrapC,
                             "\t\tpMultiIndex->pIndex[%d].pOctetStrValue",
                             i4Counter);
                    break;
                case OBJECT_IDENTIFIER:
                    fprintf (fWrapC,
                             "\t\tpMultiIndex->pIndex[%d].pOidValue",
                             i4Counter);
                    break;
                case MacAddress:
                    fprintf (fWrapC,
                             "\t\t(*(tMacAddr *)pMultiIndex->pIndex[%d]."
                             "pOctetStrValue->pu1_OctetList)", i4Counter);
                    break;
                default:
                    printf ("Unknown Data Type \n");
                    clear_directory ();
                    exit (NEGATIVE);
                    break;
            }
        }
        if ((i4Counter + 1) < p_table_struct->no_of_indices)
        {
            fprintf (fWrapC, ",\n");
        }
        else
        {
            fprintf (fWrapC, ") == SNMP_FAILURE)\n");
            fprintf (fWrapC, "\t{\n");
            fprintf (fWrapC, "\t\treturn SNMP_FAILURE;\n\t}\n");
        }
    }
}

void
wrap_Table_GetRoutine (INT4 i4MibPointer)
{
    INT4                i4Counter = ZERO, i4Count = ZERO;
    if (p_table_struct->object[i4MibPointer].type == MacAddress)
    {
        fprintf (fWrapC, "\tpMultiData->pOctetStrValue->i4_Length = 6;\n");
    }
    fprintf (fWrapC, "\treturn(nmhGet%s(\n",
             p_table_struct->object[i4MibPointer].object_name);
    for (i4Counter = ZERO; i4Counter < p_table_struct->no_of_indices;
         i4Counter++)
    {
        for (i4Count = ZERO; i4Count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4Count++)
        {
            if (COMPARE_OBJECT_WITH_INDEX (i4Count, i4Counter) != FAILURE)
            {
                continue;
            }
            switch (p_table_struct->object[i4Count].type)
            {
                case POS_INTEGER:
                case ADDRESS:
                    if ((strcmp
                         (p_table_struct->object[i4Count].type_name,
                          "Counter64")) == 0)
                    {
                        fprintf (fWrapC,
                                 "\t\tpMultiIndex->pIndex[%d].u8_Counter64Value",
                                 i4Counter);
                    }
                    else
                    {
                        fprintf (fWrapC,
                                 "\t\tpMultiIndex->pIndex[%d].u4_ULongValue",
                                 i4Counter);
                    }
                    break;
                case INTEGER:
                    fprintf (fWrapC,
                             "\t\tpMultiIndex->pIndex[%d].i4_SLongValue",
                             i4Counter);
                    break;
                case OCTET_STRING:
                    fprintf (fWrapC,
                             "\t\tpMultiIndex->pIndex[%d].pOctetStrValue",
                             i4Counter);
                    break;
                case OBJECT_IDENTIFIER:
                    fprintf (fWrapC,
                             "\t\tpMultiIndex->pIndex[%d].pOidValue",
                             i4Counter);
                    break;
                case MacAddress:
                    fprintf (fWrapC,
                             "\t\t(*(tMacAddr *)pMultiIndex->pIndex[%d]."
                             "pOctetStrValue->pu1_OctetList)", i4Counter);
                    break;
                default:
                    printf ("Unknown Data Type \n");
                    clear_directory ();
                    exit (NEGATIVE);
                    break;
            }
        }
        if ((i4Counter + 1) < p_table_struct->no_of_indices)
        {
            fprintf (fWrapC, ",\n");
        }
        else
        {
            fprintf (fWrapC, ",\n\t\t");
            switch (p_table_struct->object[i4MibPointer].type)
            {
                case POS_INTEGER:
                case ADDRESS:
                    if ((strcmp
                         (p_table_struct->object[i4MibPointer].type_name,
                          "Counter64")) == 0)
                    {
                        fprintf (fWrapC, "&(pMultiData->u8_Counter64Value)");
                    }
                    else
                    {
                        fprintf (fWrapC, "&(pMultiData->u4_ULongValue)");
                    }
                    break;
                case INTEGER:
                    fprintf (fWrapC, "&(pMultiData->i4_SLongValue)");
                    break;
                case OCTET_STRING:
                    fprintf (fWrapC, "pMultiData->pOctetStrValue");
                    break;
                case OBJECT_IDENTIFIER:
                    fprintf (fWrapC, "pMultiData->pOidValue");
                    break;
                case MacAddress:
                    fprintf (fWrapC, "(tMacAddr *)pMultiData->pOctetStrValue"
                             "->pu1_OctetList");
                    break;
                default:
                    printf ("Unknown Data Type \n");
                    clear_directory ();
                    exit (NEGATIVE);
                    break;
            }
            fprintf (fWrapC, "));\n");
        }
    }
}

void
wrap_code_set_all_objects ()
{
    INT4                i4Count = ZERO;
    for (i4Count = ZERO; i4Count < p_table_struct->no_of_objects; i4Count++)
    {

        if (find_object_is_index (p_table_struct->object[i4Count].
                                  object_name) == IS_INDEX &&
            p_table_struct->object[i4Count].access != READ_CREATE)
        {
            continue;
        }
        if ((p_table_struct->object[i4Count].access == READ_ONLY) ||
            (p_table_struct->object[i4Count].status == STATUS_OBSOLETE))
        {
            continue;
        }
        if (p_table_struct->object[i4Count].access != ACC_FOR_NOTIFY)
        {
            fprintf (fWrapC,
                     "INT4 %sSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)\n{\n",
                     p_table_struct->object[i4Count].object_name);
        }
        /* Proto Type in Header File */
        if (p_table_struct->object[i4Count].access != ACC_FOR_NOTIFY)
        {
            fprintf (fWrapH,
                     "INT4 %sSet(tSnmpIndex *, tRetVal *);\n",
                     p_table_struct->object[i4Count].object_name);
        }
        /* Scalar Objects */
        if (p_table_struct->no_of_indices == 0)
        {
            if (p_table_struct->object[i4Count].access != ACC_FOR_NOTIFY)
            {
                fprintf (fWrapC, "\tUNUSED_PARAM(pMultiIndex);\n");
                fprintf (fWrapC, "\treturn(nmhSet%s(",
                         p_table_struct->object[i4Count].object_name);
            }
            switch (p_table_struct->object[i4Count].type)
            {
                case POS_INTEGER:
                case ADDRESS:
                    if ((strcmp
                         (p_table_struct->object[i4Count].type_name,
                          "Counter64")) == 0)
                    {
                        if (p_table_struct->object[i4Count].access !=
                            ACC_FOR_NOTIFY)
                        {
                            fprintf (fWrapC,
                                     "&(pMultiData->u8_Counter64Value)");
                        }
                    }
                    else
                    {
                        if (p_table_struct->object[i4Count].access !=
                            ACC_FOR_NOTIFY)
                        {
                            fprintf (fWrapC, "pMultiData->u4_ULongValue");
                        }
                    }
                    break;
                case INTEGER:
                    if (p_table_struct->object[i4Count].access !=
                        ACC_FOR_NOTIFY)
                    {
                        fprintf (fWrapC, "pMultiData->i4_SLongValue");
                    }
                    break;
                case OCTET_STRING:
                    if (p_table_struct->object[i4Count].access !=
                        ACC_FOR_NOTIFY)
                    {
                        fprintf (fWrapC, "pMultiData->pOctetStrValue");
                    }
                    break;
                case OBJECT_IDENTIFIER:
                    if (p_table_struct->object[i4Count].access !=
                        ACC_FOR_NOTIFY)
                    {
                        fprintf (fWrapC, "pMultiData->pOidValue");
                    }
                    break;
                case MacAddress:
                    if (p_table_struct->object[i4Count].access !=
                        ACC_FOR_NOTIFY)
                    {
                        fprintf (fWrapC,
                                 "(*(tMacAddr *)pMultiData->pOctetStrValue"
                                 "->pu1_OctetList)");
                    }
                    break;
                default:
                    printf ("Unknown Data Type \n");
                    clear_directory ();
                    exit (NEGATIVE);
                    break;
            }
            if (p_table_struct->object[i4Count].access != ACC_FOR_NOTIFY)
            {
                fprintf (fWrapC, "));\n}\n\n\n");
            }
        }
        else                    /* Table Objects */
        {
            wrap_Table_SetRoutine (i4Count);
            fprintf (fWrapC, "\n}\n\n");
        }
    }
}

void
wrap_code_test_all_objects ()
{
    INT4                i4Count = ZERO;
    for (i4Count = ZERO; i4Count < p_table_struct->no_of_objects; i4Count++)
    {

        if (find_object_is_index (p_table_struct->object[i4Count].
                                  object_name) == IS_INDEX &&
            p_table_struct->object[i4Count].access != READ_CREATE)
        {
            continue;
        }
        if ((p_table_struct->object[i4Count].access == READ_ONLY) ||
            (p_table_struct->object[i4Count].status == STATUS_OBSOLETE))
        {
            continue;
        }
        if (p_table_struct->object[i4Count].access != ACC_FOR_NOTIFY)
        {
            fprintf (fWrapC,
                     "INT4 %sTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex,"
                     " tRetVal * pMultiData)\n{\n",
                     p_table_struct->object[i4Count].object_name);
        }
        /* Prototype in Header File */
        if (p_table_struct->object[i4Count].access != ACC_FOR_NOTIFY)
        {
            fprintf (fWrapH,
                     "INT4 %sTest(UINT4 *, tSnmpIndex *, tRetVal *);\n",
                     p_table_struct->object[i4Count].object_name);
        }
        if (p_table_struct->object[i4Count].type == MacAddress)
        {
            if (p_table_struct->object[i4Count].access != ACC_FOR_NOTIFY)
            {
                fprintf (fWrapC, "    if (");
                fprintf (fWrapC,
                         "pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)");
                fprintf (fWrapC, "\n    {\n");
                fprintf (fWrapC,
                         "        *pu4Error = SNMP_ERR_WRONG_LENGTH;\n");
                fprintf (fWrapC, "        return SNMP_FAILURE;");
                fprintf (fWrapC, "\n    }\n");
            }
        }

        /* Scalar Objects */
        if (p_table_struct->no_of_indices == 0)
        {
            if (p_table_struct->object[i4Count].access != ACC_FOR_NOTIFY)
            {
                fprintf (fWrapC, "\tUNUSED_PARAM(pMultiIndex);\n");
                fprintf (fWrapC, "\treturn(nmhTestv2%s(pu4Error, ",
                         p_table_struct->object[i4Count].object_name);
            }
            switch (p_table_struct->object[i4Count].type)
            {
                case POS_INTEGER:
                case ADDRESS:
                    if ((strcmp
                         (p_table_struct->object[i4Count].type_name,
                          "Counter64")) == 0)
                    {
                        if (p_table_struct->object[i4Count].access !=
                            ACC_FOR_NOTIFY)
                        {
                            fprintf (fWrapC,
                                     "&(pMultiData->u8_Counter64Value)");
                        }
                    }
                    else
                    {
                        if (p_table_struct->object[i4Count].access !=
                            ACC_FOR_NOTIFY)
                        {
                            fprintf (fWrapC, "pMultiData->u4_ULongValue");
                        }
                    }
                    break;
                case INTEGER:
                    if (p_table_struct->object[i4Count].access !=
                        ACC_FOR_NOTIFY)
                    {
                        fprintf (fWrapC, "pMultiData->i4_SLongValue");
                    }
                    break;
                case OCTET_STRING:
                    if (p_table_struct->object[i4Count].access !=
                        ACC_FOR_NOTIFY)
                    {
                        fprintf (fWrapC, "pMultiData->pOctetStrValue");
                    }
                    break;
                case OBJECT_IDENTIFIER:
                    if (p_table_struct->object[i4Count].access !=
                        ACC_FOR_NOTIFY)
                    {
                        fprintf (fWrapC, "pMultiData->pOidValue");
                    }
                    break;
                case MacAddress:
                    if (p_table_struct->object[i4Count].access !=
                        ACC_FOR_NOTIFY)
                    {
                        fprintf (fWrapC,
                                 "(*(tMacAddr *)pMultiData->pOctetStrValue->"
                                 "pu1_OctetList)");
                    }
                    break;
                default:
                    printf ("Unknown Data Type \n");
                    clear_directory ();
                    exit (NEGATIVE);
                    break;
            }
            if (p_table_struct->object[i4Count].access != ACC_FOR_NOTIFY)
            {
                fprintf (fWrapC, "));\n}\n\n\n");
            }
        }
        else                    /* Table Objects */
        {
            wrap_Table_TestRoutine (i4Count);
            fprintf (fWrapC, "\n}\n\n");
        }
    }
}

void
wrap_Table_SetRoutine (INT4 i4MibPointer)
{
    INT4                i4Counter = ZERO, i4Count = ZERO;
    fprintf (fWrapC, "\treturn (nmhSet%s(\n",
             p_table_struct->object[i4MibPointer].object_name);
    for (i4Counter = ZERO; i4Counter < p_table_struct->no_of_indices;
         i4Counter++)
    {
        for (i4Count = ZERO; i4Count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4Count++)
        {
            if (COMPARE_OBJECT_WITH_INDEX (i4Count, i4Counter) != FAILURE)
            {
                continue;
            }
            switch (p_table_struct->object[i4Count].type)
            {
                case POS_INTEGER:
                case ADDRESS:
                    if ((strcmp
                         (p_table_struct->object[i4Count].type_name,
                          "Counter64")) == 0)
                    {
                        fprintf (fWrapC,
                                 "\t\tpMultiIndex->pIndex[%d].u8_Counter64Value",
                                 i4Counter);
                    }
                    else
                    {
                        fprintf (fWrapC,
                                 "\t\tpMultiIndex->pIndex[%d].u4_ULongValue",
                                 i4Counter);
                    }
                    break;
                case INTEGER:
                    fprintf (fWrapC,
                             "\t\tpMultiIndex->pIndex[%d].i4_SLongValue",
                             i4Counter);
                    break;
                case OCTET_STRING:
                    fprintf (fWrapC,
                             "\t\tpMultiIndex->pIndex[%d].pOctetStrValue",
                             i4Counter);
                    break;
                case OBJECT_IDENTIFIER:
                    fprintf (fWrapC,
                             "\t\tpMultiIndex->pIndex[%d].pOidValue",
                             i4Counter);
                    break;
                case MacAddress:
                    fprintf (fWrapC,
                             "\t\t(*(tMacAddr *)pMultiIndex->pIndex[%d].pOctetStrValue"
                             "->pu1_OctetList)", i4Counter);
                    break;
                default:
                    printf ("Unknown Data Type \n");
                    clear_directory ();
                    exit (NEGATIVE);
                    break;
            }
        }
        if ((i4Counter + 1) < p_table_struct->no_of_indices)
        {
            fprintf (fWrapC, ",\n");
        }
        else
        {
            fprintf (fWrapC, ",\n\t\t");
            switch (p_table_struct->object[i4MibPointer].type)
            {
                case POS_INTEGER:
                case ADDRESS:
                    if ((strcmp
                         (p_table_struct->object[i4MibPointer].type_name,
                          "Counter64")) == 0)
                    {
                        fprintf (fWrapC, "&(pMultiData->u8_Counter64Value)");
                    }
                    else
                    {
                        fprintf (fWrapC, "pMultiData->u4_ULongValue");
                    }
                    break;
                case INTEGER:
                    fprintf (fWrapC, "pMultiData->i4_SLongValue");
                    break;
                case OCTET_STRING:
                    fprintf (fWrapC, "pMultiData->pOctetStrValue");
                    break;
                case OBJECT_IDENTIFIER:
                    fprintf (fWrapC, "pMultiData->pOidValue");
                    break;
                case MacAddress:
                    fprintf (fWrapC, "(*(tMacAddr *)pMultiData->pOctetStrValue"
                             "->pu1_OctetList)");
                    break;
                default:
                    printf ("Unknown Data Type \n");
                    clear_directory ();
                    exit (NEGATIVE);
                    break;
            }
            fprintf (fWrapC, "));\n");
        }
    }
}

void
wrap_Table_TestRoutine (INT4 i4MibPointer)
{
    INT4                i4Counter = ZERO, i4Count = ZERO;
    fprintf (fWrapC, "\treturn (nmhTestv2%s(pu4Error,\n",
             p_table_struct->object[i4MibPointer].object_name);
    for (i4Counter = ZERO; i4Counter < p_table_struct->no_of_indices;
         i4Counter++)
    {
        for (i4Count = ZERO; i4Count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4Count++)
        {
            if (COMPARE_OBJECT_WITH_INDEX (i4Count, i4Counter) != FAILURE)
            {
                continue;
            }
            switch (p_table_struct->object[i4Count].type)
            {
                case POS_INTEGER:
                case ADDRESS:
                    if ((strcmp
                         (p_table_struct->object[i4Count].type_name,
                          "Counter64")) == 0)
                    {
                        fprintf (fWrapC,
                                 "\t\tpMultiIndex->pIndex[%d].u8_Counter64Value",
                                 i4Counter);
                    }
                    else
                    {
                        fprintf (fWrapC,
                                 "\t\tpMultiIndex->pIndex[%d].u4_ULongValue",
                                 i4Counter);
                    }
                    break;
                case INTEGER:
                    fprintf (fWrapC,
                             "\t\tpMultiIndex->pIndex[%d].i4_SLongValue",
                             i4Counter);
                    break;
                case OCTET_STRING:
                    fprintf (fWrapC,
                             "\t\tpMultiIndex->pIndex[%d].pOctetStrValue",
                             i4Counter);
                    break;
                case OBJECT_IDENTIFIER:
                    fprintf (fWrapC,
                             "\t\tpMultiIndex->pIndex[%d].pOidValue",
                             i4Counter);
                    break;
                case MacAddress:
                    fprintf (fWrapC,
                             "\t\t(*(tMacAddr *)pMultiIndex->pIndex[%d].pOctetStrValue->"
                             "pu1_OctetList)", i4Counter);
                    break;
                default:
                    printf ("Unknown Data Type \n");
                    clear_directory ();
                    exit (NEGATIVE);
                    break;
            }
        }
        if ((i4Counter + 1) < p_table_struct->no_of_indices)
        {
            fprintf (fWrapC, ",\n");
        }
        else
        {
            fprintf (fWrapC, ",\n\t\t");
            switch (p_table_struct->object[i4MibPointer].type)
            {
                case POS_INTEGER:
                case ADDRESS:
                    if ((strcmp
                         (p_table_struct->object[i4MibPointer].type_name,
                          "Counter64")) == 0)
                    {
                        fprintf (fWrapC, "&(pMultiData->u8_Counter64Value)");
                    }
                    else
                    {
                        fprintf (fWrapC, "pMultiData->u4_ULongValue");
                    }
                    break;
                case INTEGER:
                    fprintf (fWrapC, "pMultiData->i4_SLongValue");
                    break;
                case OCTET_STRING:
                    fprintf (fWrapC, "pMultiData->pOctetStrValue");
                    break;
                case OBJECT_IDENTIFIER:
                    fprintf (fWrapC, "pMultiData->pOidValue");
                    break;
                case MacAddress:
                    fprintf (fWrapC,
                             "(*(tMacAddr *)pMultiData->pOctetStrValue->"
                             "pu1_OctetList)");
                    break;
                default:
                    printf ("Unknown Data Type \n");
                    clear_directory ();
                    exit (NEGATIVE);
                    break;
            }
            fprintf (fWrapC, "));\n");
        }
    }
}

void
wrap_lock_routine (FILE * fWrC)
{

}
void
wrap_file_close (INT1 *p1_in_file, INT1 *pi1_src_path, INT1 *pi1_inc_path)
{
    fprintf (fWrapH, "#endif /* _%sWR_H */\n", i1_Capfilename);
    if ((gi1TableEnable == 1) && (gi1RegisterMib == 0))
    {
        RegisterMibTablewise ();
        gi1RegisterMib++;
    }
    fclose (fWrapC);
    fclose (fWrapH);
    if (TRUE == i4_isave_flag)
    {
        fclose (fISaveH);
    }
    WriteDB (p1_in_file, pi1_src_path, pi1_inc_path);
    MibFlag = 0;
}

void
WriteDB (INT1 *p1_in_file, INT1 *pi1_src_path, INT1 *pi1_inc_path)
{
    UINT1               au1Array[MAX_LINE];
    INT1                i1_filename[MAX_LINE];
    INT1               *p1_temp;
    INT4                i4Count = 0;
    UINT4               u4Count = 0;
    INT4                i, j;
    UINT1              *pTableID = NULL;
    INT1                i1TableOidLen;
    UINT1              *pScalarID = NULL;
    INT1                i1ScalarOidLen;

    bzero (i1_filename, MAX_LINE);
    strcpy (i1_filename, p1_in_file);
    p1_temp = strrchr (i1_filename, '/');
    if (p1_temp != NULL)
    {
        p1_temp++;
    }
    else
    {
        p1_temp = i1_filename;
    }
    /*
     *This is added to reduce the length of the file name to five to bring
     *the over all length of the reduced file name to eight
     */
    p1_temp[6] = '\0';

    if (FILENAME == 1)
    {
        p1_temp = i1_file_name;
    }
    while (p1_temp[i4Count] != '\0')
    {
        if (p1_temp[i4Count] == '.')
        {
            p1_temp[i4Count] = '\0';
            break;
        }
        i4Count++;
    }
    i4Count = 0;

    memset (au1Array, 0, MAX_LINE);
    strcpy (au1Array, pi1_inc_path);
    strcat (au1Array, p1_temp);
    strcat (au1Array, "db.h");
    fDb = fopen (au1Array, WRITE_MODE);
    if (fDb == NULL)
    {
        printf ("Unable to Open db.h file in write Mode\n");
        clear_directory ();
        exit (NEGATIVE);
    }
    fprintf (fDb, "/***************************************************"
             "*****************\n");
    fprintf (fDb,
             "* Copyright (C) 2006 Aricent Inc . All Rights Reserved\n*\n");
    fprintf (fDb,
             "* $Id: wrap_code.c,v 1.8 2015/04/28 14:43:31 siva Exp $\n*\n",
             p1_temp);
    fprintf (fDb, "* Description: Protocol Mib Data base\n");
    fprintf (fDb, "***************************************************"
             "******************/\n");

    fprintf (fDb, "#ifndef _%sDB_H\n#define _%sDB_H\n",
             i1_Capfilename, i1_Capfilename);
    for (u4Count = 0; u4Count < gu4IndexCount; u4Count++)
    {
        fprintf (fDb, "%s", au1IndexArray[u4Count]);
    }
    fprintf (fDb, "\n\n");
    fprintf (fDb, "UINT4 %s [] ={%s};\n", p1_temp, MibOid);
    fprintf (fDb, "tSNMP_OID_TYPE %sOID = {%d, %s};\n\n\n", p1_temp,
             i4MibOidLen, p1_temp);
    if (gi1TableEnable == 1)
    {
        fprintf (fDb, "/* Generated OID's for tables */\n");
        for (u4Count = 0; u4Count < gu4IndexCount; u4Count++)
        {
            pTableID = GetOidPointer (au1TableArray[u4Count]);
            i1TableOidLen = GetOidLenFromName (au1TableArray[u4Count]);
            fprintf (fDb, "UINT4 %s [] ={%s};\n", au1TableArray[u4Count],
                     pTableID);
            fprintf (fDb, "tSNMP_OID_TYPE %sOID = {%d, %s};\n\n\n",
                     au1TableArray[u4Count], i1TableOidLen,
                     au1TableArray[u4Count]);
        }
        fprintf (fDb, "\n\n");
    }
    for (u4Count = 0; u4Count < gu4ObjectCount; u4Count++)
    {
        fprintf (fDb, "%s", gMbdb[u4Count].au1Oid);
    }

    fprintf (fDb, "\n\n");
    if (gi1TableEnable == 1)
    {
        for (u4Count = 0; u4Count < gu4ScalarCount; u4Count++)
        {
            pScalarID = GetOidPointer (au1ScalarArray[u4Count]);
            i1ScalarOidLen = GetOidLenFromName (au1ScalarArray[u4Count]);
            fprintf (fDb, "tSNMP_OID_TYPE %sOID = {%d, %s};\n\n\n",
                     au1ScalarArray[u4Count], i1ScalarOidLen,
                     au1ScalarArray[u4Count]);
        }
    }
    fprintf (fDb, "\n\n");
    if (gi1TableEnable != 1)
    {
        fprintf (fDb, "tMbDbEntry %sMibEntry[]= {\n", p1_temp);
    }

    for (i = 0; i < gu4ObjectCount; i++)
    {
        for (j = 0; j < (gu4ObjectCount - 1); j++)
        {
            if (memcmp (gMbdb[j].au4Oid, gMbdb[j + 1].au4Oid, (MAX_LINE * 4)) >
                0)
            {
                SWAP_MBDB (gMbdb[j], gMbdb[j + 1]);
            }
        }
    }
    for (u4Count = 0; u4Count < gu4ObjectCount; u4Count++)
    {
        if (gMbdb[u4Count].pu1String != NULL)
        {
            fprintf (fDb, "%s", gMbdb[u4Count].pu1String);
            free (gMbdb[u4Count].pu1String);
        }
    }

    if (gi1TableEnable != 1)
    {
        fprintf (fDb, "};\n");
        fprintf (fDb, "tMibData %sEntry = { %d, %sMibEntry };\n\n",
                 p1_temp, i4TotalCount, p1_temp);
    }

    fprintf (fDb, "#endif /* _%sDB_H */\n\n", i1_Capfilename);
    fclose (fDb);
}

void
mbdb_MibEntrycode_gen ()
{
    INT4                i4Count = 0;
    INT4                i4Len = 0;
    UINT1              *pData = NULL;
    UINT1              *pDefVal = NULL;
    UINT1              *pObjectID = NULL;
    UINT1               au1Temp[MAX_LINE];
    for (i4Count = ZERO; i4Count < p_table_struct->no_of_objects; i4Count++)
    {
        if (p_table_struct->object[i4Count].status == STATUS_OBSOLETE)
        {
            continue;
        }
        if ((gMbdb[gu4ObjectCount].pu1String =
             (MEM_MALLOC (100000, UINT1))) == NULL)
        {
            printf ("Error in Memory allocation");
        }
        memset (gMbdb[gu4ObjectCount].pu1String, '\0',
                sizeof (gMbdb[gu4ObjectCount].pu1String));
        pData = gMbdb[gu4ObjectCount].pu1String;
        FormOidList (p_table_struct->object[i4Count].object_name);
        i4Len = GetOidLenFromName (p_table_struct->object[i4Count].object_name);
        sprintf (au1Temp, "\n{{%d,%s}, ", i4Len,
                 p_table_struct->object[i4Count].object_name);
        strcat (pData, au1Temp);
        if (!i4_global_scalar_flag)
        {
            sprintf (au1Temp, "GetNextIndex%s, ", p_table_struct->table_name);
            strcat (pData, au1Temp);
        }
        else
        {
            sprintf (au1Temp, "NULL, ");
            strcat (pData, au1Temp);
        }

        /*For read-only Access type: Only Get routine */
        if (p_table_struct->object[i4Count].access == READ_ONLY)
        {
            sprintf (au1Temp, "%sGet, NULL, NULL, NULL, ",
                     p_table_struct->object[i4Count].object_name);
            strcat (pData, au1Temp);
        }

        /*For not-accessable access type: Get, Set and Test routines are not required. 
         *Hence Get, Set and Test routines should be NULL for not-accessable type object*/
        else if ((p_table_struct->object[i4Count].access == NO_ACCESS)
                 || (p_table_struct->object[i4Count].access == ACC_FOR_NOTIFY))
        {
            sprintf (au1Temp, "NULL, NULL, NULL, NULL, ");
            strcat (pData, au1Temp);
        }

        /*For Read/write Access type: Both Get and Set */
        else
        {
            sprintf (au1Temp, "%sGet, %sSet, %sTest, ",
                     p_table_struct->object[i4Count].object_name,
                     p_table_struct->object[i4Count].object_name,
                     p_table_struct->object[i4Count].object_name);
            strcat (pData, au1Temp);
            if (i4_global_scalar_flag)
            {
                sprintf (au1Temp, "%sDep, ",
                         p_table_struct->object[i4Count].object_name);
                strcat (pData, au1Temp);
            }
            else
            {
                sprintf (au1Temp, "%sDep, ", p_table_struct->table_name);
                strcat (pData, au1Temp);
            }
        }

        if (strcmp (p_table_struct->object[i4Count].type_name, "Counter64") ==
            0)
        {
            sprintf (au1Temp, "SNMP_DATA_TYPE_COUNTER64, ");
            strcat (pData, au1Temp);
        }
        else if (strcmp (p_table_struct->object[i4Count].type_name, "Counter32")
                 == 0)
        {
            sprintf (au1Temp, "SNMP_DATA_TYPE_COUNTER32, ");
            strcat (pData, au1Temp);
        }
        else if (strcmp (p_table_struct->object[i4Count].type_name, "INTEGER")
                 == 0)
        {
            sprintf (au1Temp, "SNMP_DATA_TYPE_INTEGER, ");
            strcat (pData, au1Temp);
        }
        else if (strcmp (p_table_struct->object[i4Count].type_name, "Integer32")
                 == 0)
        {
            sprintf (au1Temp, "SNMP_DATA_TYPE_INTEGER32, ");
            strcat (pData, au1Temp);
        }
        else if (strcmp
                 (p_table_struct->object[i4Count].type_name, "Unsigned32") == 0)
        {
            sprintf (au1Temp, "SNMP_DATA_TYPE_UNSIGNED32, ");
            strcat (pData, au1Temp);
        }
        else if (strncmp
                 (p_table_struct->object[i4Count].type_name,
                  "OCTET STRING", strlen ("OCTET STRING")) == 0)
        {
            sprintf (au1Temp, "SNMP_DATA_TYPE_OCTET_PRIM, ");
            strcat (pData, au1Temp);
        }
        else if (strcmp (p_table_struct->object[i4Count].type_name,
                         "OBJECT IDENTIFIER") == 0)
        {
            sprintf (au1Temp, "SNMP_DATA_TYPE_OBJECT_ID, ");
            strcat (pData, au1Temp);
        }
        else if (strcmp (p_table_struct->object[i4Count].type_name,
                         "TimeTicks") == 0)
        {
            sprintf (au1Temp, "SNMP_DATA_TYPE_TIME_TICKS, ");
            strcat (pData, au1Temp);
        }
        else if (strcmp (p_table_struct->object[i4Count].type_name,
                         "Gauge32") == 0)
        {
            sprintf (au1Temp, "SNMP_DATA_TYPE_GAUGE32, ");
            strcat (pData, au1Temp);
        }
        else if (strcmp (p_table_struct->object[i4Count].type_name,
                         "NetworkAddress") == 0)
        {
            sprintf (au1Temp, "SNMP_DATA_TYPE_OCTET_PRIM, ");
            strcat (pData, au1Temp);
        }
        else if (strcmp (p_table_struct->object[i4Count].type_name,
                         "IpAddress") == 0)
        {
            sprintf (au1Temp, "SNMP_DATA_TYPE_IP_ADDR_PRIM, ");
            strcat (pData, au1Temp);
        }
        else if (strcmp (p_table_struct->object[i4Count].type_name,
                         "MacAddress") == 0)
        {
            sprintf (au1Temp, "SNMP_DATA_TYPE_MAC_ADDRESS, ");
            strcat (pData, au1Temp);
        }
        else
        {
            printf ("Unknown Type %s\n",
                    p_table_struct->object[i4Count].type_name);
            clear_directory ();
            exit (NEGATIVE);
        }

        switch (p_table_struct->object[i4Count].access)
        {
            case READ_ONLY:
                sprintf (au1Temp, "SNMP_READONLY, ");
                strcat (pData, au1Temp);
                break;
            case NO_ACCESS:
                sprintf (au1Temp, "SNMP_NOACCESS, ");
                strcat (pData, au1Temp);
                break;
            case READ_WRITE:
            case READ_CREATE:
                sprintf (au1Temp, "SNMP_READWRITE, ");
                strcat (pData, au1Temp);
                break;
            case ACC_FOR_NOTIFY:
                sprintf (au1Temp, "SNMP_ACCESSIBLEFORNOTIFY, ");
                strcat (pData, au1Temp);
                break;
            default:
                printf ("Unknown access %d\n",
                        p_table_struct->object[i4Count].access);
                clear_directory ();
                exit (NEGATIVE);
        }

        if (!i4_global_scalar_flag)
        {
            sprintf (au1Temp, "%sINDEX, %d, ",
                     p_table_struct->table_name, i4NoOfIndex);
            strcat (pData, au1Temp);
        }
        else
        {
            sprintf (au1Temp, "NULL, 0, ");
            strcat (pData, au1Temp);
        }

        /* Updating db.h with deprecated status either 1 or 0 */
        if (STATUS_DEPRECATED == p_table_struct->object[i4Count].status)
        {
            sprintf (au1Temp, "%d, ", 1);
        }
        else
        {
            sprintf (au1Temp, "%d, ", 0);
        }
        strcat (pData, au1Temp);

        /* Updating db.h with rowstatus value either 1 or 0 */
        sprintf (au1Temp, "%d, ", p_table_struct->object[i4Count].isRowStatus);
        strcat (pData, au1Temp);

        if (strcmp (p_table_struct->object[i4Count].type_name,
                    "OBJECT IDENTIFIER") == 0)
        {
            pDefVal =
                GetDefvalFromName (p_table_struct->object[i4Count].object_name);
            if (pDefVal != NULL)
            {
                pDefVal[0] = toupper (pDefVal[0]);
                pObjectID = GetOidPointer (pDefVal);
                if (pObjectID != NULL)
                    sprintf (au1Temp, "\"%s\"},\n", pObjectID);
                else
                    sprintf (au1Temp, "NULL},\n");
            }
            else
            {
                sprintf (au1Temp, "NULL},\n");
            }
        }
        else
        {
            if (GetDefvalFromName (p_table_struct->object[i4Count].object_name)
                != NULL)
                sprintf (au1Temp, "\"%s\"},\n",
                         GetDefvalFromName (p_table_struct->object[i4Count].
                                            object_name));
            else
                sprintf (au1Temp, "NULL},\n");
        }
        strcat (pData, au1Temp);
        i4TotalCount++;
        gu4ObjectCount++;
    }
}

void
mbdb_MibEntrycode_gen_table ()
{
    INT4                i4Count = 0;
    INT4                i4Len = 0;
    UINT1              *pData = NULL;
    UINT1              *pDefVal = NULL;
    UINT1              *pObjectID = NULL;
    UINT1               au1Temp[MAX_LINE];
    INT1                p1_temp[MAX_LINE];
    INT4                i4TableCount = p_table_struct->no_of_objects;

    strcpy (p1_temp, p_table_struct->table_name);
    if ((gMbdb[gu4ObjectCount].pu1String =
         (MEM_MALLOC (100000, UINT1))) == NULL)
    {
        printf ("Error in Memory allocation");
    }

    memset (gMbdb[gu4ObjectCount].pu1String, '\0',
            sizeof (gMbdb[gu4ObjectCount].pu1String));
    pData = gMbdb[gu4ObjectCount].pu1String;
    /* This check is added to prevent generation of tMbDbEntry for Groups 
     * incase of table wise tMbDbEntry generation*/

    if (p_table_struct->no_of_indices > ZERO)
    {
        sprintf (au1Temp, "tMbDbEntry %sMibEntry[]= {\n", p1_temp);
        strcat (pData, au1Temp);
    }
    for (i4Count = ZERO; i4Count < p_table_struct->no_of_objects; i4Count++)
    {
        if (p_table_struct->object[i4Count].status == STATUS_OBSOLETE)
        {
            i4TableCount--;
            continue;
        }
        FormOidList (p_table_struct->object[i4Count].object_name);
        i4Len = GetOidLenFromName (p_table_struct->object[i4Count].object_name);
        if (p_table_struct->no_of_indices == ZERO)
        {
            sprintf (au1Temp, "tMbDbEntry %sMibEntry[]= {\n",
                     p_table_struct->object[i4Count].object_name);
            strcat (pData, au1Temp);
        }
        sprintf (au1Temp, "\n{{%d,%s}, ", i4Len,
                 p_table_struct->object[i4Count].object_name);
        strcat (pData, au1Temp);

        if (!i4_global_scalar_flag)
        {
            sprintf (au1Temp, "GetNextIndex%s, ", p_table_struct->table_name);
            strcat (pData, au1Temp);
        }
        else
        {
            sprintf (au1Temp, "NULL, ");
            strcat (pData, au1Temp);
        }

        /*For read-only Access type: Only Get routine */
        if (p_table_struct->object[i4Count].access == READ_ONLY)
        {
            sprintf (au1Temp, "%sGet, NULL, NULL, NULL, ",
                     p_table_struct->object[i4Count].object_name);
            strcat (pData, au1Temp);
        }

        /*For not-accessable access type: Get, Set and Test routines are not required. 
         *Hence Get, Set and Test routines should be NULL for not-accessable type object*/
        else if ((p_table_struct->object[i4Count].access == NO_ACCESS)
                 || (p_table_struct->object[i4Count].access == ACC_FOR_NOTIFY))
        {
            sprintf (au1Temp, "NULL, NULL, NULL, NULL, ");
            strcat (pData, au1Temp);
        }

        /*For Read/write Access type: Both Get and Set */
        else
        {
            sprintf (au1Temp, "%sGet, %sSet, %sTest, ",
                     p_table_struct->object[i4Count].object_name,
                     p_table_struct->object[i4Count].object_name,
                     p_table_struct->object[i4Count].object_name);
            strcat (pData, au1Temp);
            if (i4_global_scalar_flag)
            {
                sprintf (au1Temp, "%sDep, ",
                         p_table_struct->object[i4Count].object_name);
                strcat (pData, au1Temp);
            }
            else
            {
                sprintf (au1Temp, "%sDep, ", p_table_struct->table_name);
                strcat (pData, au1Temp);
            }
        }

        if (strcmp (p_table_struct->object[i4Count].type_name, "Counter64") ==
            0)
        {
            sprintf (au1Temp, "SNMP_DATA_TYPE_COUNTER64, ");
            strcat (pData, au1Temp);
        }
        else if (strcmp (p_table_struct->object[i4Count].type_name, "Counter32")
                 == 0)
        {
            sprintf (au1Temp, "SNMP_DATA_TYPE_COUNTER32, ");
            strcat (pData, au1Temp);
        }
        else if (strcmp (p_table_struct->object[i4Count].type_name, "INTEGER")
                 == 0)
        {
            sprintf (au1Temp, "SNMP_DATA_TYPE_INTEGER, ");
            strcat (pData, au1Temp);
        }
        else if (strcmp (p_table_struct->object[i4Count].type_name, "Integer32")
                 == 0)
        {
            sprintf (au1Temp, "SNMP_DATA_TYPE_INTEGER32, ");
            strcat (pData, au1Temp);
        }
        else if (strcmp
                 (p_table_struct->object[i4Count].type_name, "Unsigned32") == 0)
        {
            sprintf (au1Temp, "SNMP_DATA_TYPE_UNSIGNED32, ");
            strcat (pData, au1Temp);
        }
        else if (strncmp
                 (p_table_struct->object[i4Count].type_name,
                  "OCTET STRING", strlen ("OCTET STRING")) == 0)
        {
            sprintf (au1Temp, "SNMP_DATA_TYPE_OCTET_PRIM, ");
            strcat (pData, au1Temp);
        }
        else if (strcmp (p_table_struct->object[i4Count].type_name,
                         "OBJECT IDENTIFIER") == 0)
        {
            sprintf (au1Temp, "SNMP_DATA_TYPE_OBJECT_ID, ");
            strcat (pData, au1Temp);
        }
        else if (strcmp (p_table_struct->object[i4Count].type_name,
                         "TimeTicks") == 0)
        {
            sprintf (au1Temp, "SNMP_DATA_TYPE_TIME_TICKS, ");
            strcat (pData, au1Temp);
        }
        else if (strcmp (p_table_struct->object[i4Count].type_name,
                         "Gauge32") == 0)
        {
            sprintf (au1Temp, "SNMP_DATA_TYPE_GAUGE32, ");
            strcat (pData, au1Temp);
        }
        else if (strcmp (p_table_struct->object[i4Count].type_name,
                         "NetworkAddress") == 0)
        {
            sprintf (au1Temp, "SNMP_DATA_TYPE_OCTET_PRIM, ");
            strcat (pData, au1Temp);
        }
        else if (strcmp (p_table_struct->object[i4Count].type_name,
                         "IpAddress") == 0)
        {
            sprintf (au1Temp, "SNMP_DATA_TYPE_IP_ADDR_PRIM, ");
            strcat (pData, au1Temp);
        }
        else if (strcmp (p_table_struct->object[i4Count].type_name,
                         "MacAddress") == 0)
        {
            sprintf (au1Temp, "SNMP_DATA_TYPE_MAC_ADDRESS, ");
            strcat (pData, au1Temp);
        }
        else
        {
            printf ("Unknown Type %s\n",
                    p_table_struct->object[i4Count].type_name);
            clear_directory ();
            exit (NEGATIVE);
        }

        switch (p_table_struct->object[i4Count].access)
        {
            case READ_ONLY:
                sprintf (au1Temp, "SNMP_READONLY, ");
                strcat (pData, au1Temp);
                break;
            case NO_ACCESS:
                sprintf (au1Temp, "SNMP_NOACCESS, ");
                strcat (pData, au1Temp);
                break;
            case READ_WRITE:
            case READ_CREATE:
                sprintf (au1Temp, "SNMP_READWRITE, ");
                strcat (pData, au1Temp);
                break;
            case ACC_FOR_NOTIFY:
                sprintf (au1Temp, "SNMP_ACCESSIBLEFORNOTIFY, ");
                strcat (pData, au1Temp);
                break;
            default:
                printf ("Unknown access %d\n",
                        p_table_struct->object[i4Count].access);
                clear_directory ();
                exit (NEGATIVE);
        }

        if (!i4_global_scalar_flag)
        {
            sprintf (au1Temp, "%sINDEX, %d, ",
                     p_table_struct->table_name, i4NoOfIndex);
            strcat (pData, au1Temp);
        }
        else
        {
            sprintf (au1Temp, "NULL, 0, ");
            strcat (pData, au1Temp);
        }

        /* Updating db.h with deprecated status either 1 or 0 */
        if (STATUS_DEPRECATED == p_table_struct->object[i4Count].status)
        {
            sprintf (au1Temp, "%d, ", 1);
        }
        else
        {
            sprintf (au1Temp, "%d, ", 0);
        }
        strcat (pData, au1Temp);

        /* Updating db.h with rowstatus value either 1 or 0 */
        sprintf (au1Temp, "%d, ", p_table_struct->object[i4Count].isRowStatus);
        strcat (pData, au1Temp);

        if (strcmp (p_table_struct->object[i4Count].type_name,
                    "OBJECT IDENTIFIER") == 0)
        {
            pDefVal =
                GetDefvalFromName (p_table_struct->object[i4Count].object_name);
            if (pDefVal != NULL)
            {
                pDefVal[0] = toupper (pDefVal[0]);
                pObjectID = GetOidPointer (pDefVal);
                if (pObjectID != NULL)
                    sprintf (au1Temp, "\"%s\"},\n", pObjectID);
                else
                    sprintf (au1Temp, "NULL},\n");
            }
            else
            {
                sprintf (au1Temp, "NULL},\n");
            }
        }
        else
        {
            if (GetDefvalFromName (p_table_struct->object[i4Count].object_name)
                != NULL)
                sprintf (au1Temp, "\"%s\"},\n",
                         GetDefvalFromName (p_table_struct->object[i4Count].
                                            object_name));
            else
                sprintf (au1Temp, "NULL},\n");
        }
        strcat (pData, au1Temp);
        i4TotalCount++;
        gu4ObjectCount++;
        if (p_table_struct->no_of_indices == ZERO)
        {
            sprintf (au1Temp, "};\n");
            strcat (pData, au1Temp);
            sprintf (au1Temp, "tMibData %sEntry = { 1, %sMibEntry };\n\n",
                     p_table_struct->object[i4Count].object_name,
                     p_table_struct->object[i4Count].object_name);
            strcat (pData, au1Temp);
        }

    }
    if (p_table_struct->no_of_indices > ZERO)
    {
        sprintf (au1Temp, "};\n");
        strcat (pData, au1Temp);
        sprintf (au1Temp, "tMibData %sEntry = { %d, %sMibEntry };\n\n",
                 p1_temp, i4TableCount, p1_temp);
        strcat (pData, au1Temp);
    }

}

void
FormOidList (UINT1 *pName)
{
    INT1               *pi1Oid = NULL;
    UINT1              *pTemp = NULL;
    pTemp = gMbdb[gu4ObjectCount].au1Oid;
    pi1Oid = GetOidPointer (pName);
    if (pi1Oid == NULL)
    {
        printf ("Unable to Get Oid for %s\n", pName);
        clear_directory ();
        exit (NEGATIVE);
    }
    sprintf (pTemp, "UINT4 %s [ ] ={%s};\n", pName, pi1Oid);
    gMbdb[gu4ObjectCount].u4Len = GetOidLenFromName (pName);
    FormOidArray (pi1Oid, gMbdb[gu4ObjectCount].au4Oid);
}

void
FormOidArray (UINT1 *pu1Src, UINT4 *pu4Oid)
{
    INT4                i4Int = 0, i4DotCount = 0;
    UINT1              *pu1Oid = pu1Src;
    memset (pu4Oid, 0, (MAX_LINE * 4));
    for (i4Int = 0; pu1Oid[i4Int] != '\0'; i4Int++)
    {
        if (pu1Oid[i4Int] == ',')
            i4DotCount++;
    }
    for (i4Int = 0; i4Int < i4DotCount + 1; i4Int++)
    {
        if (isdigit ((INT4) *pu1Oid))
        {
            pu4Oid[i4Int] = ConvSubOIDToLong (&pu1Oid);
        }
        else
        {
            printf ("Unable to convert Oid to Oid of UINT4 \n");
            clear_directory ();
            exit (NEGATIVE);
        }
        if (*pu1Oid == ',')
        {
            pu1Oid++;
        }
        else if (*pu1Oid != '\0')
        {
            printf ("Unable to convert Oid to Oid of UINT4 \n");
            clear_directory ();
            exit (NEGATIVE);
        }
    }
}

INT4
ConvSubOIDToLong (UINT1 **ppu1TempPtr)
{
    INT2                i2Init;
    UINT4               u4Value;
    UINT1               u1TempValue;

    u4Value = 0;
    for (i2Init = 0; ((i2Init < 11) && (**ppu1TempPtr != ',') &&
                      (**ppu1TempPtr != '\0')); i2Init++)
    {
        if (!isdigit (**ppu1TempPtr))
        {
            printf (" Oid with non numeric value\n");
            clear_directory ();
            exit (NEGATIVE);
        }
        if (strncpy (&u1TempValue, *ppu1TempPtr, 1) == NULL)
        {
            printf ("strncpy for Oid fails\n");
            clear_directory ();
            exit (NEGATIVE);
        }
        u4Value = (u4Value * (UINT4) 10) + (0x0f & (UINT4) u1TempValue);
        (*ppu1TempPtr)++;
    }
    return (u4Value);
}

void
mbdb_formIndexArray ()
{
    INT4                i4Counter = 0, i4Count = 0;
    UINT1               au1Temp[MAX_LINE];
    UINT1              *pData = &au1IndexArray[gu4IndexCount][0];
    *pData = '\0';
    i4NoOfIndex = 0;
    sprintf (au1Temp, "\nUINT1 %sINDEX [] = {", p_table_struct->table_name);
    memset (&au1TableArray[gu4IndexCount], 0, MAX_LINE);
    strcpy (au1TableArray[gu4IndexCount], p_table_struct->table_name);
    gu4IndexCount++;
    strcat (pData, au1Temp);
    for (i4Counter = ZERO; i4Counter < p_table_struct->no_of_indices;
         i4Counter++)
    {
        for (i4Count = ZERO; i4Count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4Count++)
        {
            if (COMPARE_OBJECT_WITH_INDEX (i4Count, i4Counter) != FAILURE)
            {
                continue;
            }
            if (i4NoOfIndex > 0)
            {
                sprintf (au1Temp, " ,");
                strcat (pData, au1Temp);
            }
            if (strcmp (p_table_struct->object[i4Count].type_name, "Counter64")
                == 0)
            {
                sprintf (au1Temp, "SNMP_DATA_TYPE_COUNTER64");
                strcat (pData, au1Temp);
            }
            else if (strcmp
                     (p_table_struct->object[i4Count].type_name,
                      "Counter32") == 0)
            {
                sprintf (au1Temp, "SNMP_DATA_TYPE_COUNTER32");
                strcat (pData, au1Temp);
            }
            else if (strcmp
                     (p_table_struct->object[i4Count].type_name,
                      "INTEGER") == 0)
            {
                sprintf (au1Temp, "SNMP_DATA_TYPE_INTEGER");
                strcat (pData, au1Temp);
            }
            else if (strcmp
                     (p_table_struct->object[i4Count].type_name,
                      "Integer32") == 0)
            {
                sprintf (au1Temp, "SNMP_DATA_TYPE_INTEGER32");
                strcat (pData, au1Temp);
            }
            else if (strcmp
                     (p_table_struct->object[i4Count].type_name,
                      "Unsigned32") == 0)
            {
                sprintf (au1Temp, "SNMP_DATA_TYPE_UNSIGNED32");
                strcat (pData, au1Temp);
            }
            else if (strncmp
                     (p_table_struct->object[i4Count].type_name,
                      "OCTET STRING", strlen ("OCTET STRING")) == 0)
            {
                if (GetImpliedIndexMatch
                    (p_table_struct->object[i4Count].object_name) == TRUE)
                {
                    sprintf (au1Temp, "SNMP_DATA_TYPE_IMP_OCTET_PRIM");
                    strcat (pData, au1Temp);
                }
                else
                {
                    /*If the Object is OCTET STRING,Check whether it is a 
                     * of fixed Length.If so,Form Index table with Size */
                    /* Eg..  { SNMP_DATA_TYPE_UNSIGNED32 ,
                     * SNMP_FIXED_LENGTH_OCTET_STRING,6,
                     *                   SNMP_DATA_TYPE_INTEGER}; */
                    if ((p_table_struct->object[i4Count].min_val) ==
                        (p_table_struct->object[i4Count].size))
                    {
                        sprintf (au1Temp, "SNMP_FIXED_LENGTH_OCTET_STRING");
                        strcat (pData, au1Temp);
                        sprintf (au1Temp, " ,%d",
                                 p_table_struct->object[i4Count].size);
                        strcat (pData, au1Temp);
                    }
                    else
                    {
                        sprintf (au1Temp, "SNMP_DATA_TYPE_OCTET_PRIM");
                        strcat (pData, au1Temp);
                    }
                }
            }
            else if (strcmp (p_table_struct->object[i4Count].type_name,
                             "OBJECT IDENTIFIER") == 0)
            {
                if (GetImpliedIndexMatch
                    (p_table_struct->object[i4Count].object_name) == TRUE)
                {
                    sprintf (au1Temp, "SNMP_DATA_TYPE_IMP_OBJECT_ID");
                    strcat (pData, au1Temp);
                }
                else
                {
                    sprintf (au1Temp, "SNMP_DATA_TYPE_OBJECT_ID");
                    strcat (pData, au1Temp);
                }
            }
            else if (strcmp (p_table_struct->object[i4Count].type_name,
                             "TimeTicks") == 0)
            {
                sprintf (au1Temp, "SNMP_DATA_TYPE_TIME_TICKS");
                strcat (pData, au1Temp);
            }
            else if (strcmp (p_table_struct->object[i4Count].type_name,
                             "Gauge32") == 0)
            {
                sprintf (au1Temp, "SNMP_DATA_TYPE_GAUGE32");
                strcat (pData, au1Temp);
            }
            else if (strcmp (p_table_struct->object[i4Count].type_name,
                             "NetworkAddress") == 0)
            {
                sprintf (au1Temp, "SNMP_DATA_TYPE_OCTET_PRIM");
                strcat (pData, au1Temp);
            }
            else if (strcmp (p_table_struct->object[i4Count].type_name,
                             "IpAddress") == 0)
            {
                sprintf (au1Temp, "SNMP_DATA_TYPE_IP_ADDR_PRIM");
                strcat (pData, au1Temp);
            }
            else if (strcmp (p_table_struct->object[i4Count].type_name,
                             "MacAddress") == 0)
            {
                /* To avoid filling Index value for Fixed Octet Sting like
                 * MacAddress */
                sprintf (au1Temp, "SNMP_FIXED_LENGTH_OCTET_STRING");
                strcat (pData, au1Temp);
                sprintf (au1Temp, " ,%d", MAC_STR_LENGTH);
                strcat (pData, au1Temp);
            }
            else
            {
                printf ("Unknown Type %s\n",
                        p_table_struct->object[i4Count].type_name);
                clear_directory ();
                exit (NEGATIVE);
            }
            i4NoOfIndex++;
        }
    }
    sprintf (au1Temp, "};");
    strcat (pData, au1Temp);
}

void
CreateOidDatabase (INT1 *p1_in_file)
{
    FILE               *pTemp = NULL;
    INT1                au1Data[MAX_LINE];
    INT1               *pu1Start = NULL, *pu1End = NULL, *pu1Ptr = NULL;
    INT4                i4Count = 0, i4Len = 0;
    INT1                i1_filename[MAX_LINE];
    INT1               *p1_temp, *p_End;
    UINT1               au1Array[MAX_LINE];

    bzero (i1_filename, MAX_LINE);
    strcpy (i1_filename, p1_in_file);
    p1_temp = strrchr (i1_filename, '/');
    if (p1_temp != NULL)
    {
        p1_temp++;
    }
    else
    {
        p1_temp = i1_filename;
    }
    p_End = strchr (p1_temp, '.');
    if (p_End == NULL)
    {
        printf ("Unable to get file name\n");
        clear_directory ();
        exit (NEGATIVE);
    }
    memset (au1Array, 0, MAX_LINE);
    memcpy (au1Array, p1_temp, (p_End - p1_temp));
    au1Array[p_End - p1_temp] = '\0';
    strcat (au1Array, ".h");

    pTemp = fopen (au1Array, READ_MODE);
    if (pTemp == NULL)
    {
        printf ("Unable to Open %s file in read Mode\n", au1Array);
        clear_directory ();
        exit (NEGATIVE);
    }

    while (fgets (au1Data, MAX_LINE, pTemp) != NULL)
    {
        pu1Start = au1Data;
        pu1Ptr = strstr (au1Data, ",        ");
        if (pu1Ptr != NULL)
        {
            pu1End = pu1Ptr;
            while (*pu1Start != '"')
            {
                pu1Start++;
            }
            pu1Start++;
            while (*pu1End != '"')
            {
                pu1End--;
            }
            memcpy (OidTable[i4Count].name, pu1Start, (pu1End - pu1Start));
            OidTable[i4Count].name[pu1End - pu1Start] = '\0';
            OidTable[i4Count].name[0] = toupper (OidTable[i4Count].name[0]);
            pu1Ptr++;
            while (*pu1Ptr == ' ')
            {
                pu1Ptr++;
            }
            pu1End = strstr (pu1Ptr, "\n");
            if (pu1End != NULL)
            {
                pu1Start = pu1Ptr;
                while (*pu1Start != '"')
                {
                    pu1Start++;
                }
                pu1Start++;
                while (*pu1End != '"')
                {
                    pu1End--;
                }
                memcpy (OidTable[i4Count].oid, pu1Start, (pu1End - pu1Start));
                OidTable[i4Count].oid[pu1End - pu1Start] = '\0';
                pu1Start = OidTable[i4Count].oid;
                i4Len = 1;
                while (*pu1Start != '\0')
                {
                    if (*pu1Start == '.')
                    {
                        *pu1Start = ',';
                        i4Len++;
                    }
                    pu1Start++;
                }
                OidTable[i4Count].i4Len = i4Len;
            }
            i4Count++;
        }
    }
}

INT4
GetOidLenFromName (INT1 *pu1Name)
{
    INT4                i4Count = 0;
    for (i4Count = 0; i4Count < (MAX_OBJECTS * 50); i4Count++)
    {
        if (strcmp (OidTable[i4Count].name, pu1Name) == 0)
        {
            return OidTable[i4Count].i4Len;
        }
    }
    return 0;
}

INT1               *
GetOidPointer (INT1 *pu1Name)
{
    INT4                i4Count = 0;
    for (i4Count = 0; i4Count < (MAX_OBJECTS * 50); i4Count++)
    {
        if (strcmp (OidTable[i4Count].name, pu1Name) == 0)
        {
            return OidTable[i4Count].oid;
        }
    }
    return NULL;
}

INT1
GetImpliedIndexMatch (INT1 *pi1Name)
{
    tIMPINDICES_LIST   *pTemp = pImpIndx;
    if (pTemp == NULL)
    {
        return FALSE;
    }
    while (pTemp)
    {
        if (strcasecmp (pi1Name, pTemp->name) == 0)
        {
            return TRUE;
        }
        pTemp = pTemp->next;
    }
    return FALSE;
}

void
CreateDefValTable (INT1 *p1_in_file)
{
    FILE               *pTemp = NULL;
    INT1                au1Data[MAX_LINE];
    INT1               *pu1Start = NULL, *pu1End = NULL, *pu1Ptr = NULL;
    INT4                i4Count = 0;
    INT1                i1_filename[MAX_LINE];
    INT1               *p1_temp, *p_End;
    UINT1               au1Array[MAX_LINE];

    bzero (i1_filename, MAX_LINE);
    strcpy (i1_filename, p1_in_file);
    p1_temp = strrchr (i1_filename, '/');
    if (p1_temp != NULL)
    {
        p1_temp++;
    }
    else
    {
        p1_temp = i1_filename;
    }
    p_End = strchr (p1_temp, '.');
    if (p_End == NULL)
    {
        printf ("Unable to get file name\n");
        clear_directory ();
        exit (NEGATIVE);
    }
    memset (au1Array, 0, MAX_LINE);
    memcpy (au1Array, p1_temp, (p_End - p1_temp));
    au1Array[p_End - p1_temp] = '\0';
    strcat (au1Array, ".dval");

    pTemp = fopen (au1Array, READ_MODE);
    if (pTemp == NULL)
    {
        printf ("Unable to Open %s file in read Mode\n", au1Array);
        clear_directory ();
        exit (NEGATIVE);
    }

    while (fgets (au1Data, MAX_LINE, pTemp) != NULL)
    {
        pu1Start = au1Data;
        pu1Ptr = strstr (au1Data, ", ");
        if (pu1Ptr != NULL)
        {
            pu1End = pu1Ptr;
            while (*pu1Start != '"')
            {
                pu1Start++;
            }
            pu1Start++;
            while (*pu1End != '"')
            {
                pu1End--;
            }
            memcpy (DefvalTable[i4Count].name, pu1Start, (pu1End - pu1Start));
            DefvalTable[i4Count].name[pu1End - pu1Start] = '\0';
            DefvalTable[i4Count].name[0] =
                toupper (DefvalTable[i4Count].name[0]);
            pu1Ptr++;
            while (*pu1Ptr == ' ')
            {
                pu1Ptr++;
            }
            pu1End = strstr (pu1Ptr, "\n");
            if (pu1End != NULL)
            {
                pu1Start = pu1Ptr;
                while (*pu1Start != '"')
                {
                    pu1Start++;
                }
                pu1Start++;
                while (*pu1End != '"')
                {
                    pu1End--;
                }
                memcpy (DefvalTable[i4Count].def_val, pu1Start,
                        (pu1End - pu1Start));
                DefvalTable[i4Count].def_val[pu1End - pu1Start] = '\0';
            }
            i4Count++;
        }
    }
}

INT1               *
GetDefvalFromName (INT1 *pu1Name)
{
    INT4                i4Count = 0;
    for (i4Count = 0; i4Count < (MAX_OBJECTS * 50); i4Count++)
    {
        if (strcmp (DefvalTable[i4Count].name, pu1Name) == 0)
        {
            return DefvalTable[i4Count].def_val;
        }
    }
    return NULL;
}

void
wrap_code_dep_all_objects ()
{
    INT4                i4Count = ZERO;

    for (i4Count = ZERO; i4Count < p_table_struct->no_of_objects; i4Count++)
    {
        if (find_object_is_index (p_table_struct->object[i4Count].
                                  object_name) == IS_INDEX)
        {
            continue;
        }
        if ((p_table_struct->object[i4Count].access == READ_ONLY) ||
            (p_table_struct->object[i4Count].status == STATUS_OBSOLETE))
        {
            continue;
        }
        /* Scalar Objects */
        if (p_table_struct->no_of_indices == 0)
        {
            if (p_table_struct->object[i4Count].access != ACC_FOR_NOTIFY)
            {
                fprintf (fWrapC,
                         "INT4 %sDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList,"
                         " tSNMP_VAR_BIND *pSnmpvarbinds)\n{\n",
                         p_table_struct->object[i4Count].object_name);
            }

            /* Prototype in Header File */
            if (p_table_struct->object[i4Count].access != ACC_FOR_NOTIFY)
            {
                fprintf (fWrapH,
                         "INT4 %sDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);\n",
                         p_table_struct->object[i4Count].object_name);

                fprintf (fWrapC,
                         "\treturn(nmhDepv2%s(pu4Error, pSnmpIndexList, pSnmpvarbinds));",
                         p_table_struct->object[i4Count].object_name);
                fprintf (fWrapC, "\n}\n\n");
            }
        }
        else
        {
            fprintf (fWrapC,
                     "INT4 %sDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, "
                     "tSNMP_VAR_BIND *pSnmpvarbinds)\n{\n",
                     p_table_struct->table_name);

            /* Prototype in Header File */
            fprintf (fWrapH,
                     "INT4 %sDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);\n",
                     p_table_struct->table_name);

            fprintf (fWrapC,
                     "\treturn(nmhDepv2%s(pu4Error, pSnmpIndexList, pSnmpvarbinds));",
                     p_table_struct->table_name);
            fprintf (fWrapC, "\n}\n\n");
            break;
        }
    }
}

void
mid_code_set_all_objects ()
{
    INT4                i4Count = ZERO;
    INT4                flag = FALSE;
    INT1                i1_first_obj_flag = 0;

    if (p_table_struct->status == STATUS_OBSOLETE)
    {
        return;
    }

    for (i4Count = ZERO; i4Count < p_table_struct->no_of_objects; i4Count++)
    {
        if ((p_table_struct->object[i4Count].access == READ_ONLY) ||
            (p_table_struct->object[i4Count].status == STATUS_OBSOLETE))
        {
            continue;
        }
        if (flag == FALSE)
        {
            fprintf (fISaveH,
                     "\n\n/* extern declaration corresponding to OID variable present in protocol db.h */\n");
            flag = TRUE;
        }
        fprintf (fISaveH, "extern UINT4 %s[%d];\n",
                 p_table_struct->object[i4Count].object_name,
                 GetOidLenFromName (p_table_struct->object[i4Count].
                                    object_name));
    }

    if (i4_extern_flag == FALSE)
    {
        for (i4Count = ZERO; i4Count < p_table_struct->no_of_objects; i4Count++)
        {
            if (find_object_is_index (p_table_struct->object[i4Count].
                                      object_name) == IS_INDEX)
            {
                continue;
            }
            if ((p_table_struct->object[i4Count].access == READ_ONLY) ||
                (p_table_struct->object[i4Count].status == STATUS_OBSOLETE))
            {
                continue;
            }
            if (i1_first_obj_flag == 0)
            {
                fprintf (fISaveH,
                         "\n#if defined(ISS_WANTED) ||  defined(RM_WANTED)\n");
                i1_first_obj_flag = 1;
            }
            fprintf (fISaveH,
                     "#define nmhSet%s(",
                     p_table_struct->object[i4Count].object_name);

            /* If Condition Which Checks the presence of Scalar Group. */
            if (!i4_global_scalar_flag)
            {
                /* This Fn Passes the Types of Objects for Each Index. */
                passing_index_to_midlevelcode (fISaveH);
            }

            /*  Passing the TYPE and Variable for The return Value. */
            if (!i4_global_scalar_flag)
            {
                fprintf (fISaveH, " ,");
            }

            passing_prefix_according_totype (fISaveH, i4Count);
            fprintf (fISaveH, "SetVal%s)\t\\\n",
                     p_table_struct->object[i4Count].object_name);

            if (i4_incr_lock_flag == TRUE)
            {
                fprintf (fISaveH, "\tnmhSetCmnWithLock(");
            }
            else
            {
                fprintf (fISaveH, "\tnmhSetCmn(");
            }
            fprintf (fISaveH, "%s, ",
                     p_table_struct->object[i4Count].object_name);
            fprintf (fISaveH, "%d, ",
                     GetOidLenFromName (p_table_struct->object[i4Count].
                                        object_name));

            fprintf (fISaveH, "%sSet, ",
                     p_table_struct->object[i4Count].object_name);
            fprintf (fISaveH, "%s, ", ai1_lock_string);
            fprintf (fISaveH, "%s, ", ai1_unlock_string);
            fprintf (fISaveH, "%d, ",
                     p_table_struct->object[i4Count].isDeprecated);
            fprintf (fISaveH, "%d, ",
                     p_table_struct->object[i4Count].isRowStatus);
            fprintf (fISaveH, "%d, ", p_table_struct->no_of_indices);
            fprintf (fISaveH, "\"");
            passing_fmtindex_to_midlevelcode (fISaveH);
            passing_fmtdata_to_midlevelcode (fISaveH, i4Count);
            fprintf (fISaveH, "\", ");
            /* If Condition Which Checks the presence of Scalar Group. */
            if (!i4_global_scalar_flag)
            {
                /* This Fn Passes the Types of Objects for Each Index. */
                passing_index_to_midlevelcode (fISaveH);
            }

            /*  Passing the TYPE and Variable for The return Value. */
            if (!i4_global_scalar_flag)
            {
                fprintf (fISaveH, " ,");
            }

            passing_prefix_according_totype (fISaveH, i4Count);
            fprintf (fISaveH, "SetVal%s)\n",
                     p_table_struct->object[i4Count].object_name);
        }
        if (i1_first_obj_flag == 1)
        {
            fprintf (fISaveH, "\n#endif\n");
        }
    }

}

/******************************************************************************
*      function Name        : passing_fmtindex_to_midlevelcode               *
*      Role of the function : This Fn Passes the format string for indices    *
*                             Routines .                                      *
*      Formal Parameters    : f                                               *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
passing_fmtindex_to_midlevelcode (FILE * f)
{
    INT4                i4_counter;
    INT4                i4_count;

    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {
        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            switch (p_table_struct->object[i4_count].type)
            {
                case POS_INTEGER:
                case ADDRESS:
                    if ((strcmp
                         (p_table_struct->object[i4_count].type_name,
                          "Counter64")) == 0)
                    {
                        fprintf (f, "%%C");
                        break;
                    }
                    else if ((strcmp
                              (p_table_struct->object[i4_count].type_name,
                               "TimeTicks")) == 0)
                    {
                        fprintf (f, "%%t");
                        break;
                    }
                    else if ((strcmp
                              (p_table_struct->object[i4_count].type_name,
                               "Counter32")) == 0)
                    {
                        fprintf (f, "%%c");
                        break;
                    }
                    else if ((strcmp
                              (p_table_struct->object[i4_count].type_name,
                               "IpAddress")) == 0)
                    {
                        fprintf (f, "%%p");
                        break;
                    }
                    else
                    {
                        fprintf (f, "%%u");
                        break;
                    }
                case INTEGER:
                    fprintf (f, "%%i");
                    break;
                case OCTET_STRING:
                    fprintf (f, "%%s");
                    break;
                case OBJECT_IDENTIFIER:
                    fprintf (f, "%%o");
                    break;
                case MacAddress:
                    fprintf (f, "%%m");
                    break;
                default:
                    break;

            }

            /* Break When A Index is Found. */
            break;
        }                        /* End Of FOR Loop. */
        fprintf (f, " ");
    }                            /* End of Main FOR Loop For Each Index. */
}                                /* End Of Function. */

/******************************************************************************
*      function Name        : passing_fmtdata_to_midlevelcode                 *
*      Role of the function : This Fn writes fmtstring for pmultidata         *
*      Formal Parameters    : f , i4_count                                    *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
passing_fmtdata_to_midlevelcode (FILE * f, INT4 i4_count)
{

    switch (p_table_struct->object[i4_count].type)
    {
        case POS_INTEGER:
        case ADDRESS:
            if ((strcmp
                 (p_table_struct->object[i4_count].type_name,
                  "Counter64")) == 0)
            {
                fprintf (f, "%%C");
                break;
            }
            else if ((strcmp
                      (p_table_struct->object[i4_count].type_name,
                       "TimeTicks")) == 0)
            {
                fprintf (f, "%%t");
                break;
            }
            else if ((strcmp
                      (p_table_struct->object[i4_count].type_name,
                       "Counter32")) == 0)
            {
                fprintf (f, "%%c");
                break;
            }
            else if ((strcmp
                      (p_table_struct->object[i4_count].type_name,
                       "IpAddress")) == 0)
            {
                fprintf (f, "%%p");
                break;
            }
            else
            {
                fprintf (f, "%%u");
                break;
            }
        case INTEGER:
            fprintf (f, "%%i");
            break;
        case OCTET_STRING:
            fprintf (f, "%%s");
            break;
        case OBJECT_IDENTIFIER:
            fprintf (f, "%%o");
            break;
        case MacAddress:
            fprintf (f, "%%m");
            break;
        default:
            break;

    }                            /*  End of Switch */

}                                /* End of Function. */

/******************************************************************************
*      function Name        : passing_index_to_midlevelcode                   *
*      Role of the function : This Fn Passes the Indices to the mid Level     *
*                             Routines .                                      *
*      Formal Parameters    : f                                               *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
passing_index_to_midlevelcode (FILE * f)
{
    INT4                i4_counter;
    INT4                i4_count;

    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {
        if (i4_counter > ZERO)
        {
            fprintf (f, " , ");
        }
        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            passing_prefix_according_midtype (f, i4_count);
            fprintf (f, "%s", p_table_struct->object[i4_count].object_name);

            /* Break When A Index is Found. */
            break;
        }                        /* End Of FOR Loop. */

    }                            /* End of Main FOR Loop For Each Index. */

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : passing_prefix_according_midtype                 *
*      Role of the function : This Fn Passes the Prefix of the Var (i.ei4 for *
*                             INT4 etc) mid the Fn .                           *
*      Formal Parameters    : f , i4_count                                    *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
passing_prefix_according_midtype (FILE * f, INT4 i4_count)
{

    switch (p_table_struct->object[i4_count].type)
    {
        case ADDRESS:
        case POS_INTEGER:
            if ((strcmp
                 (p_table_struct->object[i4_count].type_name,
                  "Counter64")) == 0)
                fprintf (f, "u8");
            else
                fprintf (f, "u4");
            break;
        case INTEGER:
            fprintf (f, "i4");
            break;
        case OCTET_STRING:
        case OBJECT_IDENTIFIER:
            fprintf (f, "p");
            break;
        case MacAddress:
            fprintf (f, "t");
            break;

    }                            /* End Of Main Switch. */

}
void
RegisterMibTablewise ()
{

    UINT4               u4Count = ZERO;
    /*This piece of code is added here to register mib table wise */
    fprintf (fWrapC, "\n\nVOID Register%s ()\n{\n", i1_Capfilename);
    for (u4Count = 0; u4Count < gu4IndexCount; u4Count++)
    {
        if ((gi1ContextIdEnable == 1) && (gi1LockEnable == 1))
        {
            fprintf (fWrapC,
                     "\tSNMPRegisterMibWithContextIdAndLock (&%sOID, &%sEntry,"
                     " NULL, NULL, NULL, NULL, SNMP_MSR_TGR_FALSE);\n",
                     au1TableArray[u4Count], au1TableArray[u4Count]);
        }
        else if ((gi1ContextIdEnable == 0) && (gi1LockEnable == 1))
        {
            fprintf (fWrapC,
                     "\tSNMPRegisterMibWithLock (&%sOID, &%sEntry,"
                     " NULL, NULL, SNMP_MSR_TGR_FALSE);\n",
                     au1TableArray[u4Count], au1TableArray[u4Count]);
        }
        else
        {
            fprintf (fWrapC,
                     "\tSNMPRegisterMib (&%sOID, &%sEntry, SNMP_MSR_TGR_FALSE);\n",
                     au1TableArray[u4Count], au1TableArray[u4Count]);
        }
    }
    for (u4Count = 0; u4Count < gu4ScalarCount; u4Count++)
    {
        if ((gi1ContextIdEnable == 1) && (gi1LockEnable == 1))
        {
            fprintf (fWrapC,
                     "\tSNMPRegisterMibWithContextIdAndLock (&%sOID, &%sEntry,"
                     " NULL, NULL, NULL, NULL, SNMP_MSR_TGR_FALSE);\n",
                     au1ScalarArray[u4Count], au1ScalarArray[u4Count]);
        }
        else if ((gi1ContextIdEnable == 0) && (gi1LockEnable == 1))
        {
            fprintf (fWrapC,
                     "\tSNMPRegisterMibWithLock (&%sOID, &%sEntry,"
                     " NULL, NULL, SNMP_MSR_TGR_FALSE);\n",
                     au1ScalarArray[u4Count], au1ScalarArray[u4Count]);
        }
        else
        {
            fprintf (fWrapC,
                     "\tSNMPRegisterMib (&%sOID, &%sEntry, SNMP_MSR_TGR_FALSE);\n",
                     au1ScalarArray[u4Count], au1ScalarArray[u4Count]);
        }
    }
    fprintf (fWrapC,
             "\tSNMPAddSysorEntry (&%sOID, (const UINT1 *) \"%s\");\n",
             au1MibName, au1MibDesc);
    fprintf (fWrapC, "}\n\n");
    fprintf (fWrapC, "\n\nVOID UnRegister%s ()\n{\n", i1_Capfilename);
    for (u4Count = 0; u4Count < gu4IndexCount; u4Count++)
    {
        fprintf (fWrapC,
                 "\tSNMPUnRegisterMib (&%sOID, &%sEntry);\n",
                 au1TableArray[u4Count], au1TableArray[u4Count]);
    }
    for (u4Count = 0; u4Count < gu4ScalarCount; u4Count++)
    {
        fprintf (fWrapC,
                 "\tSNMPUnRegisterMib (&%sOID, &%sEntry);\n",
                 au1ScalarArray[u4Count], au1ScalarArray[u4Count]);
    }
    fprintf (fWrapC,
             "\tSNMPDelSysorEntry (&%sOID, (const UINT1 *) \"%s\");\n",
             au1MibName, au1MibDesc);
    fprintf (fWrapC, "}\n\n");
}

/*  End Of Function. */
