/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: generator.c,v 1.4 2015/04/28 14:43:31 siva Exp $
 *
 * Description: This File Contains the function  Which    
 *              call the Middle level Code generator Fns
 *              for the GET,SET & TEST routines.
 *******************************************************************/

#include "include.h"
#include "extern.h"

/******************************************************************************
*      function Name        : generator_module                                *
*      Role of the function : This fn calls all the Fns which are used to     *
*                             Generate the Files needed for the Middle & Low  *
*                             Level routines and the Header Files needed.     *
*      Formal Parameters    : None.                                           *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
generator_module (void)
{
    INT4                i4_index;
    static FILE        *fpTemp = NULL;

    if (fpTemp != f_low_h)
    {
        /* CVS Header Added */

        fprintf (f_low_h, "/***************************************************"
                 "*****************\n");
        fprintf (f_low_h, "* Copyright (C) 2006 Aricent Inc . All Rights Reserved\n*\n");
        fprintf (f_low_h,
                 "* $Id: generator.c,v 1.4 2015/04/28 14:43:31 siva Exp $\n*\n");
        fprintf (f_low_h,
                 "* Description: Proto types for Low Level  Routines\n");
        fprintf (f_low_h,
                 "***************************************************"
                 "******************/\n");
        fpTemp = f_low_h;
    }

    /* 
     * code added to change the names of variables and functions
     * to hungarian style in low level code
     */
    p_table_struct->table_name[0] = toupper (p_table_struct->table_name[0]);
    p_table_struct->entry_name[0] = toupper (p_table_struct->entry_name[0]);
    for (i4_index = 0; i4_index < p_table_struct->no_of_indices; i4_index++)
    {
        p_table_struct->index_name[i4_index][0] =
            toupper (p_table_struct->index_name[i4_index][0]);
    }
    for (i4_index = 0;
         i4_index <
         p_table_struct->no_of_objects + p_table_struct->no_of_imports;
         i4_index++)
    {
        p_table_struct->object[i4_index].object_name[0] =
            toupper (p_table_struct->object[i4_index].object_name[0]);
    }

    /* Generates Skeletol Code for Low Level. */
    low_level_code_generator (f_low_c);

    /* Generates Prototypes for Low Level. */
    low_level_proto_generator (f_low_h);

    /* 
     * code added to restore the changes made to p_table_struct
     * added by soma on 07/03/98
     */
    p_table_struct->table_name[0] = tolower (p_table_struct->table_name[0]);
    p_table_struct->entry_name[0] = tolower (p_table_struct->entry_name[0]);
    for (i4_index = 0; i4_index < p_table_struct->no_of_indices; i4_index++)
    {
        p_table_struct->index_name[i4_index][0] =
            tolower (p_table_struct->index_name[i4_index][0]);
    }
    for (i4_index = 0;
         i4_index <
         p_table_struct->no_of_objects + p_table_struct->no_of_imports;
         i4_index++)
    {
        p_table_struct->object[i4_index].object_name[0] =
            tolower (p_table_struct->object[i4_index].object_name[0]);
    }

    /* Generates the Header files _ocon.h  & _ogp_ind.h  */
    header_ocon (f_ocon);
    header_ogp_index (f_ogp);

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : code_generator_mid_module                       *
*      Role of the function : This fn calls all the Fns which are used to     *
*                             Generate the Files needed for the Middle Level  *
*                             routines.                                       *
*      Formal Parameters    : fp                                              *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
code_generator_mid_module (FILE * fp)
{
    INT4                i4_readonly_flag;

    /* Main routine for finding EXACT or NEXT routine and call the get rtn.  */
    if (!all_no_access ())
    {
        get_routine (fp);
    }

    i4_readonly_flag = check_readonly ();
    if (i4_readonly_flag == TRUE)
    {
        /* The routine for generating the set routines.  */
        set_routine (fp);

        /* The routine for generating the test routines.  */
        test_routine (fp);
    }

}                                /*  End Of Function */

/******************************************************************************
*      function Name        : check_readonly                                  *
*      Role of the function : This fn Check whether atleast one Variable      *
*                             has the Permission For READ WRITE.              *
*      Formal Parameters    : fp                                              *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : TRUE (if a Object with READ WRITE is present).  *
*                             FALSE (if all the Objects are READ ONLY).       *
******************************************************************************/
INT4
check_readonly (void)
{
    INT4                i4_count;

    for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects; i4_count++)
    {
        if ((p_table_struct->object[i4_count].access == READ_WRITE)
            || (p_table_struct->object[i4_count].access == READ_CREATE))
        {
            return TRUE;
        }
    }

    return FALSE;

}                                /* End of Function */

/******************************************************************************
*      function Name        : all_no_access                                   *
*      Role of the function : This fn Check whether all  Variables            *
*                             has the Permission For NO_ACCESS.               *
*      Formal Parameters    : fp                                              *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : FALSE (if a Object, other than NO_ACCESS        *
*                             is present).                                    *
*                             TRUE  (if all the Objects are NO_ACCESS).       *
******************************************************************************/
INT4
all_no_access (void)
{
    INT4                i4_count;
    for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects; i4_count++)
    {
        if ((p_table_struct->object[i4_count].access != NO_ACCESS))
        {
            return FALSE;
        }
    }

    return TRUE;

}                                /* End of Function */
