/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: oid_table1.c,v 1.4 2015/04/28 14:43:31 siva Exp $
 *
 * Description: This File Contains the function Which     
 *              Generates the snmp_mbdb.h File for FS 
 *              version 1.    
 *******************************************************************/

# include "include.h"
# include "extern.h"

#ifdef FUTURE_SNMP_WANTED
extern INT1         UName[50];
#endif

extern INT4         i4_check_repeat_table;
INT4                p_temp_counter = 1;

/******************************************************************************
*      function Name        : oid_table_v1                                    *
*      Role of the function : This fn generates the snmp-mbdb.h file (for v1) *
*      Formal Parameters    : pu1_path                                        *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/
void
oid_table_v1 (UINT1 *pu1_path, INT1 *p1_mibfile)
{
    /*  
     *  This fn Generates the Oid Arrays  
     *  and the The Base Oid Array.
     */
    generate_oidarray_fortable (pu1_path, p1_mibfile);

    /*  This fn Genrates the MIB Object Table. */
    generate_MIBOBJ_table (pu1_path, p1_mibfile);

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : generate_oidarray_fortable                      *  
*      Role of the function : This Fn generates the Oid Array for the         *  
*                             snmp-mbdb.h file (for v1)                       *
*      Formal Parameters    : pu1_path                                        *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/
void
generate_oidarray_fortable (UINT1 *pu1_path, INT1 *p1_mibfile)
{
    INT4                i4_oid_len = ZERO;
    UINT1              *pu1_string;
    UINT1               u1_path[MAX_LINE];
    INT1                i1_temp_mibfile[MAX_LINE];
    INT1               *p1_temp;
    FILE               *t1;
    FILE               *snmp;

    strcpy (u1_path, pu1_path);
    strcat (u1_path, "temp1");
    t1 = fopen (u1_path, APPEND_MODE);
    if (t1 == NULL)
    {
        printf
            ("\n\n\tError : Cannot Open Temporary File  for generating the MIB data base \n");
        /*This part is commented, can be used for testing */
        /*
         * printf("\n\tERROR : Can't Open Temp file 1\n");
         */
        clear_directory ();
        exit (NEGATIVE);
    }
    strcpy (i1_temp_mibfile, p1_mibfile);
    p1_temp = strrchr (i1_temp_mibfile, '.');
    if (p1_temp)
        *p1_temp = '\0';
    strcat (i1_temp_mibfile, ".h");
    snmp = fopen (i1_temp_mibfile, READ_MODE);
    if (snmp == NULL)
    {
        printf ("\n\tERROR :Can't Open %s file\n", i1_temp_mibfile);
        clear_directory ();
        exit (NEGATIVE);
    }

    /*  Searches the snmp-mib.h file for the Oid.  */
    pu1_string = search_table_name (snmp);

    if (i4_check_repeat_table)
    {
        strcat (p_table_struct->table_name, itoa (p_temp_counter));
    }

    /*  Forms the Array.  */
    i4_oid_len = write_oidarray_tofile (t1, pu1_string, p1_mibfile);
    fclose (t1);

    /*  Generates the Oid Table.  */
    generate_OID_table (pu1_path, i4_oid_len, p1_mibfile);
    fclose (snmp);

}                                /*  End of Function . */

/******************************************************************************
*      function Name        : write_oidarray_tofile                           *
*      Role of the function : This fn extracts the OID from the snmp-mbdb.h   *
*                             file & separates the No & writes the OID array  *
*                             to the file for the Table.                      *
*      Formal Parameters    : t1 , pu1_string                                 *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : Returns the len of the Oid i4_oidlen.           *
******************************************************************************/
INT4
write_oidarray_tofile (FILE * t1, UINT1 *pu1_string, INT1 *p1_mibfile)
{
    INT4                i4_count;
    INT4                i4_oid_len = TRUE;
    UINT1              *pu1_temp;
    UINT1               u1_temp[MAX_LINE];
    INT1               *p1_temp, i1_mibfile[MAX_LINE];

    strcpy (i1_mibfile, p1_mibfile);
    p1_temp = strchr (i1_mibfile, '.');
    if (p1_temp != NULL)
        *p1_temp = '\0';

    if (pu1_string)
    {
        strcpy (u1_temp, p_table_struct->table_name);
        pu1_temp = strupper (u1_temp);
        fprintf (t1, "\n UINT4  au4_SNMP_OGP_%s_OID [] =", pu1_temp);
        fprintf (t1, "\n                {");
        if ((pu1_string = strstr (pu1_string, COMMA)) != NULL)
        {
            if ((pu1_string = strstr (pu1_string, QOTES)) != NULL)
            {
                pu1_string++;
                for (i4_count = FALSE; i4_count < strlen (pu1_string) - TRUE;
                     i4_count++)
                {
                    if (pu1_string[i4_count] == QOTES_CHAR)
                    {
                        continue;
                    }
                    if (pu1_string[i4_count] != DOT_CHAR)
                    {
                        fprintf (t1, "%c", pu1_string[i4_count]);
                    }
                    else
                    {
                        fprintf (t1, COMMA);
                        i4_oid_len++;
                    }
                }                /* End of FOR LOOP . */

            }                    /* End of IF Condition. */

        }                        /* End of IF Condition. */

    }                            /*  End of Main IF condition.  */

    fprintf (t1, CLOSE_PARAS ";\n");

    return i4_oid_len;

}                                /*  End of Function */

/******************************************************************************
*      function Name        : search_table_name                               *
*      Role of the function : This fn searches in the snmp-mid.h file for the *
*                             Table Name & returns the Line if it is Present  *
*      Formal Parameters    : FILE * snmp                                     *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : Returns the Line with the Table Name else NULL. *
******************************************************************************/
UINT1              *
search_table_name (FILE * snmp)
{
    /*
     * this variable is used to solve the problem  of incorrect generation of
     * OIDs for tables while getting from snmp-mib.h
     * added by soma on 02/04/98
     */
    UINT1               u1_name[MAX_LINE];
    UINT1              *pu1_temp;

    /*
     *  Moving the File Ptr to a position where the definition
     *  of the array of name's and their Oid's start.
     */
    while (TRUE)
    {
        pu1_temp = get_line (snmp);
        if (strstr (pu1_temp, "ccitt"))
        {
            break;
        }
    }

    /*  Main While Loop to Find the OID. */
    while (!feof (snmp))
    {
        pu1_temp = get_line (snmp);
        if (check_empty_line (pu1_temp))
        {
            pu1_temp = remove_comment (pu1_temp);
            pu1_temp++;

            /*
             * copy line read from snmp-mib.h to a temporary variable to extract
             * the name of the table/entry to compare with present table/entry,
             * stored in p_table_struct.
             * Also the comparision is changed to strcmp from strncmp
             * code added by soma on 02/04/98
             */
            strcpy (u1_name, pu1_temp);
            /* this itself suffices to get table name since 
             * strtok put \0 in place of " 
             */
            strtok (u1_name, QOTES);

            /* Getting the Name of the Object alone in a temp array. */
            if (i4_global_scalar_flag
                && !strcmp (u1_name, p_table_struct->table_name))
            {
                return pu1_temp;
            }
            if (!i4_global_scalar_flag
                && !strcmp (u1_name, p_table_struct->entry_name))
            {
                return pu1_temp;
            }
        }
    }                            /* End Of Main While. */
    return NULL;
}                                /*  End of Function  */

/******************************************************************************
*      function Name        : generate_OID_table                              *
*      Role of the function : This fn writes to the a temp file the OID Table *
*                             for each Table present in the MIB.              *
*      Formal Parameters    : pu1_path , i4_oidlen                            *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
generate_OID_table (UINT1 *pu1_path, INT4 i4_oid_len, INT1 *p1_mibfile)
{
    static INT4         i4_name_array_flag = FALSE;
    UINT1               u1_temp[MAX_LINE];
    UINT1               u1_path[MAX_LINE];
    UINT1              *pu1_temp;
    FILE               *t2;
    INT1               *p1_temp, i1_mibfile[MAX_LINE];

    strcpy (i1_mibfile, p1_mibfile);
    p1_temp = strchr (i1_mibfile, '.');
    if (p1_temp != NULL)
        *p1_temp = '\0';
    strcpy (u1_path, pu1_path);
    strcat (u1_path, "temp2");
    t2 = fopen (u1_path, APPEND_MODE);
    if (t2 == NULL)
    {
        printf
            ("\n\n\tError : Cannot Open Temporary File  for generating the MIB data base \n");
        /*This part is commented, can be used for testing */
        /*
         * printf("\n\tERROR : Can't Open Temp file 2\n");  
         */
        clear_directory ();
        exit (NEGATIVE);
    }

    if (i4_name_array_flag <= FALSE)
    {
        fprintf (t2, "\n tSNMP_OID_TYPE  BaseOIDTable [] =\n {\n");
        fprintf (t2, "\n /*  The OID Table for The MIB  */\n");
        i4_name_array_flag++;
    }
    fprintf (t2, "\n          {");
    strcpy (u1_temp, p_table_struct->table_name);
    pu1_temp = strupper (u1_temp);
    fprintf (t2, "%d,(UINT4  *) au4_SNMP_OGP_%s_OID} ,\n", i4_oid_len,
             pu1_temp);
    fclose (t2);

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : generate_MIBOBJ_table                           *
*      Role of the function : This fn writes to the a temp file the MIBOBJ    *
*                             Table for each object present in the MIB.       * 
*      Formal Parameters    : pu1_path , i4_oidlen                            *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
generate_MIBOBJ_table (UINT1 *pu1_path, INT1 *p1_mibfile)
{
    INT4                i4_count;
    INT4                i4_readonly_flag = TRUE;
    static INT4         i4_MIBOBJ_array_flag = FALSE;
    UINT1               u1_temp[MAX_LINE];
    UINT1               u1_path[MAX_LINE];
    UINT1               u1_table_name[MAX_LINE];
    UINT1              *pu1_temp;
    FILE               *t3;
    FILE               *t4;
    INT1               *p1_temp, i1_mibfile[MAX_LINE];

#ifdef CORRECT_MIDGEN
    UINT1              *pi1_string1;

    (UINT1 *) pi1_string1 =
        (UINT1 *) calloc (1, sizeof (p_table_struct->entry_name));
#endif

    strcpy (i1_mibfile, p1_mibfile);
    p1_temp = strchr (i1_mibfile, '.');
    if (p1_temp != NULL)
        *p1_temp = '\0';
    strcpy (u1_path, pu1_path);
    strcat (u1_path, "temp3");
    t3 = fopen (u1_path, APPEND_MODE);
    if (t3 == NULL)
    {
        printf
            ("\n\n\tError : Cannot Open Temporary File  for generating the MIB data base \n");
        /*This part is commented, can be used for testing */
        /*
         * printf("\n\tERROR : Can't Open Temp file 3\n");
         */
        clear_directory ();
        exit (NEGATIVE);
    }

    if (i4_MIBOBJ_array_flag <= FALSE)
    {
        fprintf (t3, "\n /*  The Mib Object Table for The MIB  */\n");
        fprintf (t3, "\ntSNMP_MIB_OBJ_DESC  MIBObjectTable[] = {\n");
        i4_MIBOBJ_array_flag++;
    }

    /*  Converting the Table Name to Upper Case.  */
    strcpy (u1_temp, p_table_struct->table_name);
    pu1_temp = strupper (u1_temp);
    strcpy (u1_table_name, pu1_temp);

    for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects; i4_count++)
    {
        fprintf (t3, "\n{");
        fprintf (t3, "\n /*  %s  */",
                 p_table_struct->object[i4_count].object_name);
        fprintf (t3, "\n SNMP_OGP_INDEX_%s,", u1_table_name);
        switch (p_table_struct->object[i4_count].type)
        {
            case POS_INTEGER:
            {
                if (strncmp
                    (p_table_struct->object[i4_count].type_name,
                     STR_COUNTER32_TYPE, strlen (STR_COUNTER32_TYPE)) == FALSE)
                {
                    fprintf (t3, "\n SNMP_DATA_TYPE_COUNTER,");
                }
                if (strncmp
                    (p_table_struct->object[i4_count].type_name,
                     STR_COUNTER64_TYPE, strlen (STR_COUNTER64_TYPE)) == FALSE)
                {
                    fprintf (t3, "\n SNMP_DATA_TYPE_COUNTER,");
                }
                if (strncmp
                    (p_table_struct->object[i4_count].type_name,
                     STR_GAUGE32_TYPE, strlen (STR_GAUGE32_TYPE)) == FALSE)
                {
                    fprintf (t3, "\n SNMP_DATA_TYPE_GAUGE,");
                }
                if (strncmp
                    (p_table_struct->object[i4_count].type_name,
                     STR_TIMETICKS_TYPE, strlen (STR_TIMETICKS_TYPE)) == FALSE)
                {
                    fprintf (t3, "\n SNMP_DATA_TYPE_TIME_TICKS,");
                }

                if (strncmp
                    (p_table_struct->object[i4_count].type_name,
                     STR_UNSIGNED32_TYPE,
                     strlen (STR_UNSIGNED32_TYPE)) == FALSE)
                {
                    fprintf (t3, "\n SNMP_DATA_TYPE_UNSIGNED32,");

                }
                break;

            }
            case INTEGER:
                fprintf (t3, "\n SNMP_DATA_TYPE_INTEGER,");
                break;
            case ADDRESS:
                fprintf (t3, "\n SNMP_DATA_TYPE_IP_ADDR_PRIM,");
                break;
            case OCTET_STRING:
            case MacAddress:
                fprintf (t3, "\n SNMP_DATA_TYPE_OCTET_PRIM,");
                break;
            case OBJECT_IDENTIFIER:
                fprintf (t3, "\n SNMP_DATA_TYPE_OBJECT_ID,");
                break;
        }                        /* End of Main Switch. */

        /*  Setting The Flag if There is One Object with Read Write Access. */
        if ((p_table_struct->object[i4_count].access == READ_WRITE)
            || (p_table_struct->object[i4_count].access == READ_CREATE))
        {
            i4_readonly_flag = FALSE;
        }

        /*  Writing the Access to the _MBDB.H file.  */
        if (p_table_struct->object[i4_count].access == READ_ONLY)
        {
            fprintf (t3, "\n READ_ONLY,");
        }
        else if (p_table_struct->object[i4_count].access == READ_CREATE)
        {
            fprintf (t3, "\n READ_CREATE,");
        }
        else if (p_table_struct->object[i4_count].access == NO_ACCESS)
        {
            fprintf (t3, "\n NO_ACCESS,");
        }
        else if (p_table_struct->object[i4_count].access == READ_WRITE)
        {
            fprintf (t3, "\n READ_WRITE,");
        }
        else if (p_table_struct->object[i4_count].access == ACC_FOR_NOTIFY)
        {
            fprintf (t3, "\n ACCESSIBLE_FOR_NOTIFY,");
        }

        /*  This Part of the Code Declares the CONST for the MIB-OBJECT.  */
        strcpy (u1_temp, p_table_struct->object[i4_count].object_name);
        pu1_temp = strupper (u1_temp);
        fprintf (t3, "\n %s,", pu1_temp);

        /*  
         *  This Fn stores the Pointers to the GET SET & TEST
         *  middle level routines in the MIB_OBJ Table.
         */
        fnpointer_to_mid_fns (t3, i4_count);

        fprintf (t3, "\n},");
        fflush (t3);

    }                            /* End of FOR LOOP. */

    if (all_no_access ())
    {
        i4_readonly_flag = 3;
    }
    /*  Only  Used For Version II.  */
    strcpy (u1_path, pu1_path);
    strcat (u1_path, "temp4");
    t4 = fopen (u1_path, APPEND_MODE);

    /* Only For FutureSNMP Version II.  */
    if (t4 == NULL)
    {
        printf
            ("\n\n\tError : Cannot Open Temporary File  for generating the MIB data base \n");
        /*This part is commented, can be used for testing */
        /*
         * printf("\n\tERROR : Can't Open Temp file 4\n");
         */
        clear_directory ();
        exit (NEGATIVE);
    }

#ifdef CORRECT_MIDGEN
    strcpy (pi1_string1, p_table_struct->entry_name);

    if (i4_check_repeat_table)
    {
        strcat (pi1_string1, itoa (p_temp_counter));
        p_temp_counter++;
    }
#endif

    fprintf (t4, "%d", p_table_struct->no_of_objects);
    fprintf (t4, COMMA);
    fprintf (t4, "%d", i4_readonly_flag);
    fprintf (t4, COMMA);
#ifdef CORRECT_MIDGEN
    fprintf (t4, "%s", pi1_string1);
    free (pi1_string1);
#else
    fprintf (t4, "%s", p_table_struct->entry_name);
#endif

    fprintf (t4, NEWLINE_STRING);

    fclose (t3);
    fclose (t4);

}                                /*  End of Function. */

/******************************************************************************
*      function Name        : fnpointer_to_mid_fns                            *
*      Role of the function : This fn writes the function pointer for the GET *
*                             SET & TEST Rtns to the MIB_OBJ Table in the     *
*                             _MBDB.H File.                                   *
*      Formal Parameters    : t3 , i4_count                                   *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/

void
fnpointer_to_mid_fns (FILE * t3, INT4 i4_count)
{

#ifdef CORRECT_MIDGEN
    UINT1              *pi1_string;

    (UINT1 *) pi1_string =
        (UINT1 *) calloc (1, sizeof (p_table_struct->entry_name));
    strcpy (pi1_string, p_table_struct->entry_name);

    if (i4_check_repeat_table)
    {
        strcat (pi1_string, itoa (p_temp_counter));
    }
    switch (p_table_struct->object[i4_count].access)
    {
        case READ_WRITE:
            fprintf (t3, "\n %sGet,", pi1_string);
            fprintf (t3, "\n %sTest,", pi1_string);
            fprintf (t3, "\n %sSet", pi1_string);
            break;
        case READ_CREATE:
            fprintf (t3, "\n %sGet,", pi1_string);
            fprintf (t3, "\n %sTest,", pi1_string);
            fprintf (t3, "\n %sSet", pi1_string);
            break;
        case READ_ONLY:
            fprintf (t3, "\n %sGet,", pi1_string);
            fprintf (t3, "\n NULLF,");
            fprintf (t3, "\n NULLF");
            break;
#else
    switch (p_table_struct->object[i4_count].access)
    {
        case READ_WRITE:
            fprintf (t3, "\n %sGet,", p_table_struct->entry_name);
            fprintf (t3, "\n %sTest,", p_table_struct->entry_name);
            fprintf (t3, "\n %sSet", p_table_struct->entry_name);
            break;
        case READ_CREATE:
            fprintf (t3, "\n %sGet,", p_table_struct->entry_name);
            fprintf (t3, "\n %sTest,", p_table_struct->entry_name);
            fprintf (t3, "\n %sSet", p_table_struct->entry_name);
            break;
        case READ_ONLY:
            fprintf (t3, "\n %sGet,", p_table_struct->entry_name);
            fprintf (t3, "\n NULLF,");
            fprintf (t3, "\n NULLF");
            break;
#endif
        case NO_ACCESS:
            fprintf (t3, "\n NULLVBF,");
            fprintf (t3, "\n NULLF,");
            fprintf (t3, "\n NULLF");
            break;
        case ACC_FOR_NOTIFY:
            fprintf (t3, "\n NULLVBF,");
            fprintf (t3, "\n NULLF,");
            fprintf (t3, "\n NULLF");
            break;
    }                            /*  End of SWITCH. */

#ifdef CORRECT_MIDGEN
    free (pi1_string);
#endif

}                                /*  End of Function. */

/******************************************************************************
*      function Name        : create_mbdbh_file                               *
*      Role of the function : This fn writes the Content of the Temp files to *
*                             the SNMP_MBDM.H File in path passed as paras.   *
*      Formal Parameters    : pu1_path , file_name                            *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
create_mbdbh_file (UINT1 *pu1_path, UINT1 *file_name)
{
    INT4                i4_count;
    UINT1               u1_path[MAX_LINE];
    UINT1              *pu1_string;
    FILE               *mbdb;
    FILE               *temp;

    if ((pu1_string = (UINT1 *) calloc (1, MAX_LINE)) == NULL)
    {
        printf ("\n\tERROR : Malloc Failure.\n");
        clear_directory ();
        exit (NEGATIVE);
    }
    strcpy (u1_path, pu1_path);
    strcat (u1_path, file_name);
    strcat (u1_path, MBDB_EXTN_H);
    mbdb = fopen (u1_path, WRITE_MODE);
    if (mbdb == NULL)
    {
        printf ("\n\tERROR : Can't Open MBDB.H file\n");
        clear_directory ();
        exit (NEGATIVE);
    }
    oid_table_incs (mbdb, file_name);
    for (i4_count = TRUE; i4_count <= THREE; i4_count++)
    {
        strcpy (u1_path, pu1_path);
        strcat (u1_path, "temp");
        if (i4_count == TRUE)
        {
            strcat (u1_path, "1");
        }
        else if (i4_count == TWO)
        {
            strcat (u1_path, "2");
        }
        else
        {
            strcat (u1_path, "3");
        }

        temp = fopen (u1_path, READ_MODE);
        if (temp == NULL)
        {
            printf ("\n\tERROR : Cannot OPEN Temp %d file\n", i4_count);
            clear_directory ();
            exit (NEGATIVE);
        }
        while (!feof (temp))
        {
            if (!fgets (pu1_string, MAX_LINE, temp))
            {
                break;
            }
            fputs (pu1_string, mbdb);
        }
        if (i4_count == TWO)
        {
            fprintf (mbdb, CLOSE_PARAS);
            fprintf (mbdb, SEMI_COLON);
        }
        if (i4_count == THREE)
        {
            fprintf (mbdb, "\n" CLOSE_PARAS);
            fprintf (mbdb, SEMI_COLON);
        }
        fflush (temp);
        fclose (temp);

    }                            /* End of FOR LOOP */

    fprintf (mbdb, "\n/* Finding The Max No Of Objects. */");
    fprintf (mbdb, "\nconst UINT4 MAX_OBJECTS = ");
    fprintf (mbdb, "(sizeof(MIBObjectTable)) / (sizeof(tSNMP_MIB_OBJ_DESC");
    fprintf (mbdb, "));");
    free (pu1_string);
    fclose (mbdb);

}                                /* End Of Function */

/******************************************************************************
*      function Name        : oid_table_incs                                  *
*      Role of the function : This fn writes to the File the definitions and  *
*                             include files in the output file SNMP_MBDB.H.   *
*      Formal Parameters    : snmp , file_name                                *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
oid_table_incs (FILE * snmp, UINT1 *file_name)
{
    fprintf (snmp, "/*****");
    fprintf (snmp, "\n The MBDB File Which Contains the MIB DATA BASE.");
    fprintf (snmp, "\n *****/");

#ifdef FUTURE_SNMP_WANTED

    fprintf (snmp, "\n#ifndef     %s_SNMP_MBDB_H", UName);
    fprintf (snmp, "\n#define     %s_SNMP_MBDB_H\n", UName);
    fprintf (snmp, "\n#endif      %s_SNMP_MBDB_H\n", UName);

#else

    fprintf (snmp, "\n#ifndef     _SNMP_MBDB_H");
    fprintf (snmp, "\n#define     _SNMP_MBDB_H\n");
    fprintf (snmp, "\n#endif      _SNMP_MBDB_H\n");

#endif

    fprintf (snmp, "\n/*  NULL FUNCTION POINTER . */\n");
    fprintf (snmp, DEFS "NULLF  ARG_LIST(((INT4 (*)(tSNMP_OID_TYPE *,\\");
    fprintf (snmp, "\n                             tSNMP_OID_TYPE *,\\");
    fprintf (snmp, "\n                             UINT1,\\");
    fprintf (snmp,
             "\n                             tSNMP_MULTI_DATA_TYPE *)) NULL))\n");

    fprintf (snmp, "\n/*  NULL VARBIND FUNCTION POINTER . */\n");
    fprintf (snmp,
             DEFS "NULLVBF  ARG_LIST(((tSNMP_VARBIND* (*)(tSNMP_OID_TYPE *,\\");
    fprintf (snmp,
             "\n                                         tSNMP_OID_TYPE *,\\");
    fprintf (snmp, "\n                                         UINT1,\\");
    fprintf (snmp,
             "\n                                         UINT1))NULL))\n");

}                                /* End Of Function. */
