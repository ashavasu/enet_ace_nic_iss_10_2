/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: get_routine.c,v 1.4 2015/04/28 14:43:31 siva Exp $
 *
 * Description: This File Contains the function Which     
 *              generate the Middle level GET Routines. 
 *******************************************************************/

#include "include.h"
#include "extern.h"

#ifdef CORRECT_MIDGEN
extern INT4         i4_check_repeat_table;
extern INT4         p_temp_counter;
#endif

/******************************************************************************
*      function Name        : get_routine                                     *
*      Role of the function : This fn is the Main Module of get Routine which *
*                             calls all the other code generating Fns.        *
*      Formal Parameters    : fp                                              *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/
void
get_routine (FILE * fp)
{

#ifdef CORRECT_MIDGEN
    UINT1              *pi1_string;

    (UINT1 *) pi1_string =
        (UINT1 *) calloc (1, sizeof (p_table_struct->entry_name));
    strcpy (pi1_string, p_table_struct->entry_name);

    if (i4_check_repeat_table)
    {
        strcat (pi1_string, itoa (p_temp_counter));
    }
    comments_for_getroutine (pi1_string, fp);
    prototype_for_getroutine (pi1_string, fp);
    free (pi1_string);
#else
    comments_for_getroutine (p_table_struct->entry_name, fp);
    prototype_for_getroutine (p_table_struct->entry_name, fp);
#endif

    if (i4_global_scalar_flag)
    {

        fprintf (fp, "\n\n/*** DECLARATION_BEGIN ***/\n\n");
        declaration_scalar_table_getroutine (fp);
        fprintf (fp, "\n\n/*** DECLARATION_END ***/\n\n");
        get_scalar_exact_routine (fp);

    }
    else
    {
        fprintf (fp, "\n\n/*** DECLARATION_BEGIN ***/\n\n");
        declarations_for_getroutine (fp);
        fprintf (fp, "\n\n/*** DECLARATION_END ***/\n\n");
        get_exact_routine (fp);
        get_next_routine (fp);

    }
    generate_routine_forall_objects (fp);

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : comments_for_get_routine                        *
*      Role of the function : This fn generates the Comments for the Get Fns  *
*      Formal Parameters    : pi1_entry_name , fp                             *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/
void
comments_for_getroutine (UINT1 *pi1_entry_name, FILE * fp)
{
    fprintf (fp, "/" DOT);
    fprintf (fp, "\n Function   : %sGet", pi1_entry_name);
    fprintf (fp,
             "\n Description: This routine returns the value of the requested");
    fprintf (fp, " MIB variable.");
    fprintf (fp,
             "\n Input      : p_in_db       : The OID as formed by the SNMP Agent");
    fprintf (fp,
             "\n                              from the static MIB database.");
    fprintf (fp,
             "\n              p_incoming    : The OID as it is send by the manager");
    fprintf (fp, "\n                              in the SNMP PDU.");
    fprintf (fp,
             "\n              u1_arg        : The position of the variable in the MIB group.");
    fprintf (fp,
             "\n              u1_search_type: Indicates whether this routine is called");
    fprintf (fp, "\n                              as a GET or GETNEXT.");
    fprintf (fp,
             "\n Output     : The actual OID for which the operation is performed is");
    fprintf (fp, "\n              returned in p_in_db.");
    fprintf (fp,
             "\n Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either");
    fprintf (fp,
             "\n              the corresponding value, or NULL if the GET/GETNEXT");
    fprintf (fp,
             "\n              operation could not be performed successfully.");
    fprintf (fp, "\n" DOT "/ \n");
}

/******************************************************************************
*      function Name        : prototype_for_getroutine                        *
*      Role of the function : This fn generates the Prototypes for the        *
*                             Get Routines                                    *
*      Formal Parameters    : pi1_entry_name , fp                             *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/
void
prototype_for_getroutine (UINT1 *i1_entry_name, FILE * fp)
{

    fprintf (fp, "\n/* Prototype declarations for get_routine. */");
    fprintf (fp, "\ntSNMP_VAR_BIND*");
    fprintf (fp,
             "\n%sGet (tSNMP_OID_TYPE* p_in_db , tSNMP_OID_TYPE* p_incoming,",
             i1_entry_name);
    fprintf (fp, "\n       UINT1 u1_arg, UINT1 u1_search_type)");
    fprintf (fp, "\n{");

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : prototype_for_getroutine                        *
*      Role of the function : This fn generates the code for which declares   *
*                             the variables needed for the Mid level Routines.* 
*      Formal Parameters    : fp                                              *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/
void
declarations_for_getroutine (FILE * fp)
{

    fprintf (fp, "\n    /* Declarations for get routine .*/");

    /*  Declaring the Common Variables. */
    declaration_of_common_variables (fp);

    fprintf (fp, "\n    INT4 LEN_%s_INDEX;\n", p_table_struct->table_name);

    /* Declaration of the Variables that are args to the FormVarbind Fn. */
    declaration_of_formvarbind_args (fp);

    fprintf (fp, "\n    /*");
    fprintf (fp, "\n     *  These Variable are declared for being used in the");
    fprintf (fp,
             "\n     *  FOR Loop for extracting Indices from the given OID.");
    fprintf (fp, "\n     */");
    fprintf (fp, "\n    INT4 i4_count = FALSE;");

    /*  
     *  The Fn which Declares all the variables which
     *  are required to extract the Indices.
     */
    declare_all_var_to_extractindices (fp);

    /*  This Fn Declares the return Values for the Table. */
    declaration_of_returnvalue (fp);

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : declaration_of_returnvalue                      *
*      Role of the function : This fn generates the code for which declares   *
*                             the return Vals for indices for the Mid lev fn. *
*      Formal Parameters    : fp                                              *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/
void
declaration_of_returnvalue (FILE * fp)
{
    INT4                i4_count;
    INT4                i4_index;

    for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
         p_table_struct->no_of_imports; i4_count++)
    {

        switch (p_table_struct->object[i4_count].type)
        {
            case INTEGER:
            case POS_INTEGER:
            case OBJECT_IDENTIFIER:
                break;
            case OCTET_STRING:
            {
                fprintf (fp,
                         "\n    tSNMP_OCTET_STRING_TYPE * poctet_retval_%s = NULL;",
                         p_table_struct->object[i4_count].object_name);
                break;
            }
            case ADDRESS:
            {
                fprintf (fp, "\n    UINT4 u4_addr_ret_val_%s;",
                         p_table_struct->object[i4_count].object_name);
                break;
            }
            case MacAddress:
            {
                fprintf (fp, "\n    tMacAddr mac_addr_ret_val_%s;",
                         p_table_struct->object[i4_count].object_name);
                break;
            }
        }                        /*  End Of Switch */

    }                            /* End of FOR loop */

}                                /* End of Function. */

/******************************************************************************
*      function Name        : declaration_of_common_variables                 *
*      Role of the function : This fn generates the code for which declares   *
*                             the common vars used in the Mid lev fn.         *
*      Formal Parameters    : fp                                              *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
declaration_of_common_variables (FILE * fp)
{
    fprintf (fp, "\n    UINT1 i1_ret_val = FALSE;");

    /* These Variable are used to extract the Indices from the Given Oid */
    fprintf (fp, "\n    INT4 i4_offset = FALSE;");
    fprintf (fp, "\n    INT4 i4_partial_index_len = FALSE;");
    fprintf (fp, "\n    INT4 i4_partial_index_flag = TRUE;");
    fprintf (fp, "\n    INT4 i4_size_offset = FALSE;");
    /*
     *This variable is added to check for the errors to be returned from 
     *the low level code. This value is returned from the low level function.
     *This is utilised only in low level test routine.
     */
    fprintf (fp, "\n    UINT4 u4ErrorCode = 0;");

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : declaration_of_formvarbind_args                 *
*      Role of the function : This fn generates the code for which declares   *
*                             the vars which are passed to the Formvarbind fn *
*      Formal Parameters    : fp                                              *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
declaration_of_formvarbind_args (FILE * fp)
{
    fprintf (fp,
             "\n    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */");
    fprintf (fp, "\n    INT4 i4_return_val = FALSE;");
    fprintf (fp, "\n    UINT4 u4_counter_val = FALSE;");
    fprintf (fp, "\n    tSNMP_COUNTER64_TYPE u8_counter_val = {0,0};");
    fprintf (fp, "\n    INT2 i2_type;\n");
    fprintf (fp, "\n    /*");
    fprintf (fp,
             "\n     *  The Declaration of the Octet String Array which is used for");
    fprintf (fp,
             "\n     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.");
    fprintf (fp, "\n     */");
    fprintf (fp, "\n    UINT1 u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;");
    fprintf (fp, "\n    UINT1 * pu1_octet_string = NULL;");
    fprintf (fp, "\n    tSNMP_OCTET_STRING_TYPE * poctet_string = NULL;");
    fprintf (fp, "\n    tSNMP_OID_TYPE  * pOidValue = NULL;\n");

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : declaration_scalar_table_getroutine             *
*      Role of the function : This fn generates the code for which declares   *
*                             the vars which are used in the Midlevel Rtns.   *
*      Formal Parameters    : fp                                              *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
declaration_scalar_table_getroutine (FILE * fp)
{
    fprintf (fp, "\n\n    /* Declarations for Scalar get routine. */\n");

    /*  Declaring the Common Variables. */
    declaration_of_common_variables (fp);
    fprintf (fp, "\n    INT4 LEN_%s_INDEX;\n", p_table_struct->table_name);
    /* This Fn Declares the Vars that are paras to the FormVarbind Fn. */
    declaration_of_formvarbind_args (fp);

    fprintf (fp, "\n\n    /*");
    fprintf (fp, "\n     *  This Variable is declared for being used in the");
    fprintf (fp, "\n     *  FOR Loop for extracting Indices from OID Given.");
    fprintf (fp, "\n     */\n");
    fprintf (fp, "\n    INT4 i4_count = FALSE ;");

    /*  This Fn Declares the Vars needed for the Return Values. */
    declaration_of_returnvalue (fp);

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : declare_all_var_to_extractindices               *
*      Role of the function : This fn generates the code for which declares   *
*                             the vars which are used to extract the Indices. *
*      Formal Parameters    : fp                                              *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
declare_all_var_to_extractindices (FILE * fp)
{
    INT4                i4_counter;
    INT4                i4_count;
    INT4                i4_index;

    /* FOR Loop for the declaration of Vars which store the Extracted Indices. */

    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {

        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {
            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            switch (p_table_struct->object[i4_count].type)
            {
                case POS_INTEGER:
                {
                    if ((strcmp
                         (p_table_struct->object[i4_count].type_name,
                          "Counter64")) == 0)
                    {
                        fprintf (fp,
                                 "\n  tSNMP_COUNTER64_TYPE   u8_%s = FALSE;",
                                 p_table_struct->object[i4_count].object_name);
                        fprintf (fp,
                                 "\n  tSNMP_COUNTER64_TYPE   u8_next_%s = FALSE ;\n",
                                 p_table_struct->object[i4_count].object_name);
                    }
                    else
                    {
                        fprintf (fp, "\n    UINT4 u4_%s = FALSE;",
                                 p_table_struct->object[i4_count].object_name);
                        fprintf (fp, "\n    UINT4 u4_next_%s = FALSE ;\n",
                                 p_table_struct->object[i4_count].object_name);
                    }
                    break;
                }
                case INTEGER:
                {
                    fprintf (fp, "\n    INT4 i4_%s = FALSE;",
                             p_table_struct->object[i4_count].object_name);
                    fprintf (fp, "\n    INT4 i4_next_%s = FALSE;\n",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                case OCTET_STRING:
                {
                    fprintf (fp, "\n    /*");
                    fprintf (fp,
                             "\n     *  The tSNMP_OCTET_STRING_TYPE Which Store");
                    fprintf (fp, "\n     *  the Length and the Octet String.");
                    fprintf (fp, "\n     */");

                    fprintf (fp, "\n    INT4  i4_len_Octet_index_%s = FALSE;",
                             p_table_struct->object[i4_count].object_name);
                    fprintf (fp,
                             "\n    tSNMP_OCTET_STRING_TYPE * poctet_%s = NULL;",
                             p_table_struct->object[i4_count].object_name);
                    fprintf (fp,
                             "\n    tSNMP_OCTET_STRING_TYPE * poctet_next_%s = NULL;\n",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                case OBJECT_IDENTIFIER:
                {
                    fprintf (fp,
                             "\n    /*  The Variable Which Store the Length of the OID. */");
                    fprintf (fp, "\n    INT4 i4_len_OID_index_%s;",
                             p_table_struct->object[i4_count].object_name);

                    fprintf (fp, "\n    tSNMP_OID_TYPE * OID_%s = NULL;",
                             p_table_struct->object[i4_count].object_name);
                    fprintf (fp, "\n    tSNMP_OID_TYPE * OID_next_%s = NULL;\n",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                case MacAddress:
                {
                    /* Declaring the Variables for Mac Addr(6 bytes). */
                    fprintf (fp, "\n    /*");
                    fprintf (fp,
                             "\n     *  The Declaration of the structure which stores");
                    fprintf (fp,
                             "\n     *  the Mac Address of length Six Bytes.");
                    fprintf (fp, "\n     */");
                    fprintf (fp, "\n    tMacAddr  mac_addr_%s;",
                             p_table_struct->object[i4_count].object_name);
                    fprintf (fp, "\n    tMacAddr  mac_addr_next_%s;",
                             p_table_struct->object[i4_count].object_name);

                    break;
                }
                case ADDRESS:
                {
                    /* 
                     *  Declaring the Variables for IP Address , Physical Address.
                     *  Network Address (len 4 bytes).
                     */

                    fprintf (fp, "\n    /*");
                    fprintf (fp,
                             "\n     *  The Declaration of the Structure which stores");
                    fprintf (fp, "\n     *  the Address of length Four Bytes");
                    fprintf (fp, "\n     */");
                    fprintf (fp, "\n    UINT4  u4_addr_%s = FALSE;",
                             p_table_struct->object[i4_count].object_name);
                    fprintf (fp, "\n    UINT4  u4_addr_next_%s = FALSE;",
                             p_table_struct->object[i4_count].object_name);

                    fprintf (fp,
                             "\n    UINT1 u1_addr_%s[ADDR_LEN] = NULL_STRING;",
                             p_table_struct->object[i4_count].object_name);
                    fprintf (fp,
                             "\n    UINT1 u1_addr_next_%s[ADDR_LEN] = NULL_STRING;\n",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
            }                    /* End of Switch. */

            /* Break when One Index is found. */
            break;
        }                        /* End of for loop for all Objects.  */

    }                            /* End of Main FOR Loop For Each Index . */

}                                /* End  Of Function .  */

/******************************************************************************
*      function Name        : allocate_octetstring                            *
*      Role of the function : This Fn generates the Code which allocates the  *
*                             the memory for Octet Strings for all Objects.   *
*      Formal Parameters    : fp                                              *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
allocate_octetstring (FILE * fp)
{
    INT4                i4_count = FALSE;
    INT4                i4_index_flag;
    INT4                i4_index = FALSE;
    /*
     *  This FOR Loop is for generating the code which allocates memory for
     *  all the Objects which are of type Octet String. If the Size is known
     *  then the malloc is done for that else for a Max Size (MAX_OCTETLEN).
     */
    for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
         p_table_struct->no_of_imports; i4_count++)
    {

        /* Finding Whether the Object is an Index or Not. */
        i4_index_flag =
            find_object_is_index (p_table_struct->object[i4_count].object_name);

        /* If the Object is of Type Octet String. */
        if (p_table_struct->object[i4_count].type == OCTET_STRING)
        {

            fprintf (fp,
                     "\n       if((poctet_%s = (tSNMP_OCTET_STRING_TYPE *)malloc",
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp, "(sizeof(tSNMP_OCTET_STRING_TYPE))) == NULL);");
            fprintf (fp, "\n         return((tSNMP_VAR_BIND * )NULL);");

            /* If the Object is an Index. Then the Size is Known. */
            if (i4_index_flag == IS_INDEX)
            {

                /* The Case where the Size of Octet Str is in the p_incoming. */
                if (p_table_struct->object[i4_count].size == ZERO)
                {
                    fprintf (fp,
                             "\n       if((poctet_%s->pu1_OctetList = (UINT1 *) malloc(i4_len_Octet_index_%s)) == NULL)",
                             p_table_struct->object[i4_count].object_name,
                             p_table_struct->object[i4_count].object_name);
                }
                else
                {
                    fprintf (fp,
                             "\n       if((poctet_%s->pu1_OctetList = (UINT1 *) malloc(%d)) == NULL)",
                             p_table_struct->object[i4_count].object_name,
                             p_table_struct->object[i4_count].size);
                }
                fprintf (fp, "\n          return((tSNMP_VAR_BIND * )NULL);");

            }                    /* Else Condition for the Object Not being an Index. */
            else if (i4_index_flag == NOT_INDEX)
            {

                /*  The Case where the Size of Octet String is Not Known. */
                if (p_table_struct->object[i4_count].size == ZERO)
                {
                    fprintf (fp,
                             "\n       if((poctet_%s->pu1_OctetList = (UINT1 *) malloc(MAX_OCTETLEN)) == NULL)",
                             p_table_struct->object[i4_count].object_name);
                }
                else
                {
                    fprintf (fp,
                             "\n       if((poctet_%s->pu1_OctetList = (UINT1 *) malloc(%d)) == NULL)",
                             p_table_struct->object[i4_count].object_name,
                             p_table_struct->object[i4_count].size);
                }

                fprintf (fp, "\n          return((tSNMP_VAR_BIND * )NULL);");

            }

        }                        /* End Of If Case for OCTET STRING. */

    }                            /* End Of FOR Loop. */

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : get_scalar_exact_routine                        *
*      Role of the function : This Fn generates the Get Exact Routines for    *
*                             the Scalar Groups.                              *
*      Formal Parameters    : fp                                              *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
get_scalar_exact_routine (FILE * fp)
{
    /* 
     *  This Fn generates the code for finding the Len of the Indices
     *  according to their type and stores them in the LEN_tablename_INDEX var. 
     */

    fprintf (fp, "\n   LEN_%s_INDEX = p_in_db->u4_Length ;",
             p_table_struct->table_name);
    fprintf (fp,
             "\n\n   /*  Incrementing the Length for the Extract of Scalar Tables. */");

    fprintf (fp, "\n   LEN_%s_INDEX++ ;", p_table_struct->table_name);

    fprintf (fp, "\n   if(u1_search_type == SNMP_SEARCH_TYPE_EXACT) {");
    fprintf (fp, "\n         if((LEN_%s_INDEX != (INT4) p_incoming->u4_Length)",
             p_table_struct->table_name);
    fprintf (fp,
             "\n         || (p_incoming->pu4_OidList[p_incoming->u4_Length - 1] != 0)) {");
    fprintf (fp, "\n            return ((tSNMP_VAR_BIND *)NULL);");
    fprintf (fp, "\n         }");
    fprintf (fp, "\n   }");
    fprintf (fp, "\n   else {");
    fprintf (fp,
             "\n         /*  Get Next Operation on the Scalar Variable.  */");
    fprintf (fp,
             "\n         if((INT4) p_incoming->u4_Length >= LEN_%s_INDEX) {",
             p_table_struct->table_name);
    fprintf (fp, "\n            return ((tSNMP_VAR_BIND *)NULL);");
    fprintf (fp, "\n         }");
    fprintf (fp, "\n   }");
    fprintf (fp, "\n  switch(u1_arg)\n  {");

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : get_exact_routine                               *
*      Role of the function : This Fn generates the Get Exact Routines for    *
*                             the Table.                                      *
*      Formal Parameters    : fp                                              *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
get_exact_routine (FILE * fp)
{
    INT4                i4_count = ZERO;
    INT4                i4_index = ZERO;
    INT4                i4_counter = ZERO;

    fprintf (fp, "i4_offset = FALSE ;\n");
    fprintf (fp, "switch (u1_search_type) {\n\n");
    fprintf (fp, "     case SNMP_SEARCH_TYPE_EXACT :\n");
    fprintf (fp, "     {");

    /* 
     *  This Fn Generates the Code for Assigning Value of the Variable
     *  Len Index to the Variable(which is declared) from the Given OID.
     */
    assign_len_of_variablelen_index (fp);

    fprintf (fp,
             "\n\n        if (p_incoming->u4_Length != (UINT4) i4_size_offset) ");
    fprintf (fp, "\n        {");
    fprintf (fp, "\n            return (NULL);");
    fprintf (fp, "\n        }");

    /*
     *  This Fn generates the code for finding the Len of th Indices
     *  as per their type & stores them in the LEN_entryname_INDEX variable.
     */
    find_len_of_indices (fp);

    fprintf (fp, "\n        if(LEN_%s_INDEX == (INT4) p_incoming->u4_Length) {",
             p_table_struct->table_name);

    /*  
     *  Allocating Memory for Octet Str type Indices which are
     *  to be passed to the Low Level Routines Get Exact Case.
     *  (Only if Octet String is Present as an Index for the Table).
     */
    alloc_mem_for_octetstring_getexact_first (fp, GET_EXACT_FLAG);

    /*  This fn Checks For Malloc Failures for Octet Strings. */
    fn_checking_for_malloc_failure (fp);

    /*  
     *  Allocating Memory for OID type Indices which are
     *  to be passed to the Low Level Routines Get Exact Case.
     *  (Only if Octet String is Present as an Index for the Table).
     */
    alloc_mem_for_oid_getexact_first (fp, GET_EXACT_FLAG);

    /*  This fn Checks For Malloc Failures for OID. */
    fn_checking_for_malloc_failure_oid (fp);

    /*  This Fn Generated the code for extracting Indices. */
    code_for_extracting_indices (fp);

    /*  
     *  Calling the Low Level fn for validating the Indices
     *  which are extracted from the Oid given by the manager
     *  only when the Table not a scalar.
     */
    if (i4_global_scalar_flag <= ZERO)
    {
        fprintf (fp,
                 "\n             /*  Low Level Routine Which Validates the Indices. */");
        /* 
         * this is to conform to the Hungarian styled function names
         * used in low level code
         * added by soma on 07/03/98
         */
        p_table_struct->table_name[0] = toupper (p_table_struct->table_name[0]);
        fprintf (fp,
                 "\n             if((i1_ret_val = nmhValidateIndexInstance%s(",
                 p_table_struct->table_name);
        /*
         * this is to restore the change made above
         * added by soma on 07/03/98
         */
        p_table_struct->table_name[0] = tolower (p_table_struct->table_name[0]);

        /*  Passing the Indices to Low Level Validate Index fn. */
        passing_indices_to_lowlevel_routine (fp);

        fprintf (fp, ")) != SNMP_SUCCESS)");
        fprintf (fp, "\n             {");

        /*  Freeing the Octet String Indices. */
        free_all_octetstrings (fp, GET_EXACT_FLAG);

        /*  Freeing the OID Indices. */
        free_all_oids (fp, GET_EXACT_FLAG);

        fprintf (fp, "\n                    return ((tSNMP_VAR_BIND *) NULL);");
        fprintf (fp, "\n             }");
    }
    fprintf (fp,
             "\n             /*  Storing the Extracted Index in p_in_db. */");

    /*  FOR Loop for Storing the extracted Index in the p_in_db EXACT case. */
    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {
        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            switch (p_table_struct->object[i4_count].type)
            {
                case POS_INTEGER:
                {
                    if ((strcmp
                         (p_table_struct->object[i4_count].type_name,
                          "Counter64")) == 0)
                        fprintf (fp,
                                 "\n             p_in_db->pu4_OidList[p_in_db->u4_Length++] = u8_%s;",
                                 p_table_struct->object[i4_count].object_name);
                    else
                        fprintf (fp,
                                 "\n             p_in_db->pu4_OidList[p_in_db->u4_Length++] = u4_%s;",
                                 p_table_struct->object[i4_count].object_name);
                    break;
                }
                case INTEGER:
                {
                    fprintf (fp,
                             "\n             p_in_db->pu4_OidList[p_in_db->u4_Length++] = (UINT4) i4_%s;",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                case OBJECT_IDENTIFIER:
                {
                    fprintf (fp,
                             "\n             /*  Storing the Lenght of the OID Get Exact.  */");
                    fprintf (fp,
                             "\n             p_in_db->pu4_OidList[p_in_db->u4_Length++] = OID_%s->u4_Length;",
                             p_table_struct->object[i4_count].object_name);
                    fprintf (fp,
                             "\n             for(i4_count = FALSE ; i4_count < OID_%s->u4_Length ; i4_count++)",
                             p_table_struct->object[i4_count].object_name);
                    fprintf (fp,
                             "\n                 p_in_db->pu4_OidList[p_in_db->u4_Length++] = OID_%s->pu4_OidList[i4_count] ;\n",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                case OCTET_STRING:
                {
                    fprintf (fp,
                             "\n             /*  Storing the Length of the Octet String Get Exact.  */");
                    /*
                     * if condition removed to fix incorrect extraction of
                     * OCTET STRING type index
                     * changed by soma on 06/06/98
                     */
                    fprintf (fp,
                             "\n             p_in_db->pu4_OidList[p_in_db->u4_Length++] = poctet_%s->i4_Length;",
                             p_table_struct->object[i4_count].object_name);
                    fprintf (fp,
                             "\n             /*  FOR Loop for storing the value from get first to p_in_db. */");
                    fprintf (fp,
                             "\n             for(i4_count = FALSE ; i4_count < poctet_%s->i4_Length ; i4_count++)",
                             p_table_struct->object[i4_count].object_name);
                    fprintf (fp,
                             "\n                 p_in_db->pu4_OidList[p_in_db->u4_Length++] = (UINT4) poctet_%s->pu1_OctetList[i4_count];\n",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                case ADDRESS:
                {
                    fprintf (fp,
                             "\n             for(i4_count = FALSE ; i4_count < ADDR_LEN ; i4_count++)");
                    fprintf (fp,
                             "\n                 p_in_db->pu4_OidList[p_in_db->u4_Length++] = u1_addr_%s[i4_count];\n",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                case MacAddress:
                {
                    fprintf (fp, "\n             /*");
                    fprintf (fp,
                             "\n              *  This FOR loop is for storing the content of");
                    fprintf (fp,
                             "\n              *  MacAddr struct into the p_in_db.");
                    fprintf (fp, "\n              */");
                    fprintf (fp,
                             "\n             for(i4_count = FALSE; i4_count < MAC_ADDR_LEN ; i4_count++)");
                    fprintf (fp,
                             "\n                 p_in_db->pu4_OidList[p_in_db->u4_Length++] = mac_addr_%s[i4_count];",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                default:
                    break;

            }                    /* End of Switch Case */

            /* Break When One Index is Found. */
            break;
        }                        /* End Of FOR Loop . */

    }                            /* End of Main FOR loop For Each Index . */

    fprintf (fp, "\n             i4_partial_index_flag = FALSE;");
    fprintf (fp, "\n        }");
    fprintf (fp, "\n        else {");
    fprintf (fp, "\n           return ((tSNMP_VAR_BIND *)NULL) ;");
    fprintf (fp, "\n        }");
    fprintf (fp, "\n        break;");
    fprintf (fp, "\n     }");

}                                /*  End Of Function. */

/******************************************************************************
*      function Name        : assign_len_of_variablelen_index                 *
*      Role of the function : This Fn generates the code for assiging the Len *
*                             of the Variable Length Index from the Given OID.*
*      Formal Parameters    : fp                                              *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
assign_len_of_variablelen_index (FILE * fp)
{
    INT4                i4_count;
    INT4                i4_counter;
    INT4                i4_index = ZERO;

    /* 
     * Initializing the i4_size_offset Variable to the Length of the
     * OID of the Object i.e the Length present in the p_in_db.
     */
    fprintf (fp, "\n        /*");
    fprintf (fp,
             "\n         *  Initializing the i4_size_offset Variable to the Length of the");
    fprintf (fp,
             "\n         *  OID of the Object i.e the Length present in the p_in_db.");
    fprintf (fp, "\n         */");
    fprintf (fp, "\n        i4_size_offset = p_in_db->u4_Length;");
    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {

        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            switch (p_table_struct->object[i4_count].type)
            {
                case POS_INTEGER:
                case INTEGER:
                {
                    fprintf (fp, "\n        i4_size_offset += INTEGER_LEN;");
                    break;
                }
                case ADDRESS:
                {
                    fprintf (fp, "\n        i4_size_offset += ADDR_LEN;");
                    break;
                }
                case MacAddress:
                {
                    fprintf (fp, "\n        i4_size_offset += MAC_ADDR_LEN;");
                    break;
                }
                case OCTET_STRING:
                {
                    /*
                     * if condition removed to fix the incorrect
                     * extraction of OCTET STRING type index
                     * changed by soma on 06/06/98
                     */
                    fprintf (fp,
                             "\n        i4_len_Octet_index_%s = p_incoming->pu4_OidList[i4_size_offset++];",
                             p_table_struct->object[i4_count].object_name);
                    fprintf (fp,
                             "\n        i4_size_offset += i4_len_Octet_index_%s;",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                case OBJECT_IDENTIFIER:
                {
                    fprintf (fp,
                             "\n        i4_len_OID_index_%s = p_incoming->pu4_OidList[i4_size_offset++];",
                             p_table_struct->object[i4_count].object_name);
                    fprintf (fp,
                             "\n        i4_size_offset += i4_len_OID_index_%s;",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
            }                    /* End Of SWITCH Case . */

            /*  Break After Finding An Index. */
            break;
        }                        /* End of FOR Loop. */

    }                            /* End of Main FOR Loop for Each Index. */

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : find_len_of_indices                             *
*      Role of the function : This Fn generates the code for finding the Len  *
*                             of the Length from the Given OID.               *
*      Formal Parameters    : fp                                              *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
find_len_of_indices (FILE * fp)
{
    INT4                i4_counter;
    INT4                i4_count;
    INT4                i4_index;

    fprintf (fp, "\n        /*");
    fprintf (fp,
             "\n         *  Finding the length of the Index for extracting");
    fprintf (fp,
             "\n         *  Index from the structure given by the Manager.");
    fprintf (fp, "\n         */");
    fprintf (fp, "\n        /*");
    fprintf (fp,
             "\n         *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File");
    fprintf (fp,
             "\n         *  where it is Defined for Explanation of the Variable.");
    fprintf (fp, "\n         */");

    fprintf (fp, "\n        LEN_%s_INDEX = p_in_db->u4_Length ",
             p_table_struct->table_name);

    /* FOR loop for finding the length of the Index alone.  */
    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {

        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            switch (p_table_struct->object[i4_count].type)
            {
                case POS_INTEGER:
                case INTEGER:
                {
                    fprintf (fp, " + INTEGER_LEN");
                    break;
                }
                case OCTET_STRING:
                {
                    /*  
                     *  The Length is Incremented by One Since
                     *  1 byte is used for storing the Octet Strs
                     *  Length. for eg if a Get Operation on an
                     *  Object (with Variable Len Octet Str or OID as
                     *  Index) is Done the It will be Given as
                     *  <Object_Name>.<Length of Following Index>.
                     *  <OctetString itself>. So the One is Added for
                     *  the <Length of the Following Index>.
                     */
                    /*
                     * if condition removed to fix the incorrect
                     * extraction of OCTET STRING type index
                     * changed by soma on 06/06/98
                     */
                    fprintf (fp,
                             " + i4_len_Octet_index_%s + LEN_OF_VARIABLE_LEN_INDEX",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                case OBJECT_IDENTIFIER:
                {
                    fprintf (fp,
                             " + i4_len_OID_index_%s + LEN_OF_VARIABLE_LEN_INDEX",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                case ADDRESS:
                {
                    fprintf (fp, " + ADDR_LEN");
                    break;
                }
                case MacAddress:
                {
                    fprintf (fp, " + MAC_ADDR_LEN");
                    break;
                }
                default:
                    break;
            }                    /* End of switch. */

            /* Break When A Index is found.  */
            break;

        }                        /* End of For LOOP */

    }                            /* End of Main FOR Loop for Each Index.  */

    fprintf (fp, ";\n");

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : code_for_extracting_indices                     *
*      Role of the function : This Fn generates the code which extracts the   *
*                             Indices from the Given OID.                     *
*      Formal Parameters    : fp                                              *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
code_for_extracting_indices (FILE * fp)
{
    INT4                i4_counter = ZERO;
    INT4                i4_count = ZERO;
    INT4                i4_index_count = ZERO;

    /* 
     * For loop for separating a index from the 
     * incoming Vars given by the Mngr.
     */
    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {

        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            /* 
             *  Incrementing the Index Count by One for every Index
             *  so that a cross check can be made with the No of Indices
             *  processed and the No of Indices actually Present.
             */
            i4_index_count++;

            /*
             *  This Fn Generates the Code for Extracting
             *  an Index According To its Data type. 
             */
            extract_one_index (fp, i4_count);

            /* Break if a Index Found . */
            break;
        }                        /* End of For LOOP.  */

    }                            /* End of Main FOR Loop For Each Index. */

    /* 
     *  Checking if the No of Indices Processed is the Same
     *  as the No of Indices parsed from the MIB file.
     */
    if (i4_index_count != p_table_struct->no_of_indices)
    {

        printf
            ("\n\tERROR : The No Of Indices Present in the Table is lesser than");
        printf ("\n\t        Number actually Processed in %s.\n",
                p_table_struct->table_name);
        clear_directory ();
        exit (NEGATIVE);

    }

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : get_next_routine                                *
*      Role of the function : This Fn generates GET Routine for GET NEXT case *
*      Formal Parameters    : fp                                              *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
get_next_routine (FILE * fp)
{
    INT4                i4_count = ZERO;
    INT4                i4_index = ZERO;
    INT4                i4_counter;

    fprintf (fp, "\n     case SNMP_SEARCH_TYPE_NEXT  :\n     {");
    fprintf (fp, "\n         /*  The Manager Has Not Given the Indices. */");
    fprintf (fp, "\n         if(p_incoming->u4_Length <= p_in_db->u4_Length)");
    fprintf (fp, "\n         {");

    /*
     *  This fn Allocates memory for Octest String type Indices to be
     *  Passed to the Low Level GetNext Routines according to the size.
     */
    alloc_mem_for_octetstring_getexact_first (fp, GET_FIRST_FLAG);

    /*  This fn check for Malloc Failures. */
    fn_checking_for_malloc_failure (fp);

    /*
     *  This fn Allocates memory for the Indices of OID 
     *  type to the Passed to the Low Level GetNext Routines.
     */
    alloc_mem_for_oid_getexact_first (fp, GET_FIRST_FLAG);

    /*  This fn check for Malloc Failures. */
    fn_checking_for_malloc_failure_oid (fp);

    fprintf (fp,
             "\n               /*  Flag is Set to Indicate Low Level Get First Operation. */");
    fprintf (fp, "\n               i4_partial_index_flag = FALSE ;");
    fprintf (fp, "\n               if(( i1_ret_val");
    /* 
     * this is to conform to the Hungarian styled function names
     * used in low level code
     * added by soma on 07/03/98
     */
    p_table_struct->table_name[0] = toupper (p_table_struct->table_name[0]);
    fprintf (fp, "\n                      = nmhGetFirstIndex%s(",
             p_table_struct->table_name);
    /*
     * this is to restore the change made above
     * added by soma on 07/03/98
     */
    p_table_struct->table_name[0] = tolower (p_table_struct->table_name[0]);

    /* 
     *  Fn for passing the indices into the
     *  low level GET FIRST routine.
     */
    passing_indices_to_lowlevel_routine_getfirst (fp);

    fprintf (fp, "))== SNMP_SUCCESS) {");

    /* 
     *  For loop for storing the value of Index got from the low level
     *  get_first routine into the p_in_db structure after converting
     *  the structure got from the low level routine as per their type.  
     */

    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {

        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            switch (p_table_struct->object[i4_count].type)
            {
                case POS_INTEGER:
                {
                    if ((strcmp
                         (p_table_struct->object[i4_count].type_name,
                          "Counter64")) == 0)
                        fprintf (fp,
                                 "\n                    p_in_db->pu4_OidList[p_in_db->u4_Length++] = u8_%s;",
                                 p_table_struct->object[i4_count].object_name);
                    else
                        fprintf (fp,
                                 "\n                    p_in_db->pu4_OidList[p_in_db->u4_Length++] = u4_%s;",
                                 p_table_struct->object[i4_count].object_name);
                    break;
                }
                case INTEGER:
                {
                    fprintf (fp,
                             "\n                    p_in_db->pu4_OidList[p_in_db->u4_Length++] = (UINT4) i4_%s;",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                case OBJECT_IDENTIFIER:
                {
                    fprintf (fp,
                             "\n                    /*  Storing the Length. */");
                    fprintf (fp,
                             "\n                    p_in_db->pu4_OidList[p_in_db->u4_Length++] = OID_%s->u4_Length;",
                             p_table_struct->object[i4_count].object_name);
                    fprintf (fp,
                             "\n                    for(i4_count = FALSE ; i4_count < OID_%s->u4_Length ; i4_count++)",
                             p_table_struct->object[i4_count].object_name);
                    fprintf (fp,
                             "\n                        p_in_db->pu4_OidList[p_in_db->u4_Length++] = OID_%s->pu4_OidList[i4_count] ;\n",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                case OCTET_STRING:
                {
                    fprintf (fp,
                             "\n                    /*  Storing the Length. */");
                    /*
                     * if condition removed to fix incorrect extraction of
                     * OCTET STRING type index
                     * changed by soma on 06/06/98
                     */
                    fprintf (fp,
                             "\n                    p_in_db->pu4_OidList[p_in_db->u4_Length++] = poctet_%s->i4_Length;",
                             p_table_struct->object[i4_count].object_name);
                    fprintf (fp,
                             "\n                    /*  FOR Loop for storing the value from get first to p_in_db. */");
                    fprintf (fp,
                             "\n                    for(i4_count = FALSE ; i4_count < poctet_%s->i4_Length ; i4_count++)",
                             p_table_struct->object[i4_count].object_name);
                    fprintf (fp,
                             "\n                        p_in_db->pu4_OidList[p_in_db->u4_Length++] = (UINT4) poctet_%s->pu1_OctetList[i4_count];\n",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                case ADDRESS:
                {
                    fprintf (fp,
                             "\n                    *((UINT4 *) (u1_addr_%s)) = OSIX_HTONL(u4_addr_%s);\n",
                             p_table_struct->object[i4_count].object_name,
                             p_table_struct->object[i4_count].object_name);

                    fprintf (fp,
                             "\n                    for(i4_count = FALSE ; i4_count < ADDR_LEN ; i4_count++)");
                    fprintf (fp,
                             "\n                        p_in_db->pu4_OidList[p_in_db->u4_Length++] = u1_addr_%s[i4_count];\n",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                case MacAddress:
                {

                    fprintf (fp, "\n                    /*");
                    fprintf (fp,
                             "\n                     *  This FOR loop is for storing the content of");
                    fprintf (fp,
                             "\n                     *  MacAddr struct into the p_in_db.");
                    fprintf (fp, "\n                     */");
                    fprintf (fp,
                             "\n                    for(i4_count = FALSE; i4_count < MAC_ADDR_LEN ; i4_count++)");
                    fprintf (fp,
                             "\n                        p_in_db->pu4_OidList[p_in_db->u4_Length++] = mac_addr_%s[i4_count];",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                default:
                    break;

            }                    /* End of Switch Case */

            /* Break When One Index is Found. */
            break;
        }                        /* End Of FOR Loop . */

    }                            /* End of Main FOR loop For Each Index . */

    fprintf (fp, "\n               }");
    fprintf (fp, "\n               else {");

    /*  
     *  This Fn Frees all the Indices of Octet String
     *  type for the get_first low level routines .
     */
    free_all_octetstrings (fp, GET_FIRST_FLAG);

    /*  
     *  This Fn Frees all the Indices of OID
     *  type for the get_first low level routines .
     */
    free_all_oids (fp, GET_FIRST_FLAG);

    fprintf (fp, "\n                    return (( tSNMP_VAR_BIND*) NULL);");
    fprintf (fp, "\n               }");
    fprintf (fp, "\n        }");

    /*  GET_NEXT LOW LEVEL BEGINS. */

    fprintf (fp,
             "\n        else if(p_incoming->u4_Length > p_in_db->u4_Length) {");
    fprintf (fp,
             "\n           /*  Flag is Set to Indicate Get First Operation. */");
    fprintf (fp, "\n           i4_partial_index_flag = TRUE ;\n");

    /*  This Fn Generates the Code for extracting Partial Indices. */
    code_for_extracting_partial_indices (fp);

    /*  Calling of the Low Level routine GET_NEXT.  */

    fprintf (fp, "\n\n           /*");
    fprintf (fp,
             "\n            *  Get the value of the variable with the new index");
    fprintf (fp, "\n            *  By Calling the Low Level GET_NEXT routine.");
    fprintf (fp, "\n            */");

    fprintf (fp, "\n           if(( i1_ret_val");
    /* 
     * this is to conform to the Hungarian styled function names
     * used in low level code
     * added by soma on 07/03/98
     */
    p_table_struct->table_name[0] = toupper (p_table_struct->table_name[0]);
    fprintf (fp, "\n                = nmhGetNextIndex%s(",
             p_table_struct->table_name);
    /*
     * this is to restore the change made above
     * added by soma on 07/03/98
     */
    p_table_struct->table_name[0] = tolower (p_table_struct->table_name[0]);

    /* FOR Loop for passing the (paras) indices into the low lev get_next fn. */

    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {
        if (i4_counter > ZERO)
        {
            fprintf (fp, " ,");
        }
        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            switch (p_table_struct->object[i4_count].type)
            {
                case POS_INTEGER:
                {
                    if ((strcmp
                         (p_table_struct->object[i4_count].type_name,
                          "Counter64")) == 0)
                        fprintf (fp, "u8_%s , &u4_next_%s",
                                 p_table_struct->object[i4_count].object_name,
                                 p_table_struct->object[i4_count].object_name);
                    else
                        fprintf (fp, "u4_%s , &u4_next_%s",
                                 p_table_struct->object[i4_count].object_name,
                                 p_table_struct->object[i4_count].object_name);
                    break;
                }
                case INTEGER:
                {
                    fprintf (fp, "i4_%s , &i4_next_%s",
                             p_table_struct->object[i4_count].object_name,
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                case OCTET_STRING:
                {
                    fprintf (fp, "poctet_%s , poctet_next_%s",
                             p_table_struct->object[i4_count].object_name,
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                case OBJECT_IDENTIFIER:
                {
                    fprintf (fp, "OID_%s , OID_next_%s",
                             p_table_struct->object[i4_count].object_name,
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                case ADDRESS:
                {
                    fprintf (fp, "u4_addr_%s , &u4_addr_next_%s",
                             p_table_struct->object[i4_count].object_name,
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                case MacAddress:
                {
                    fprintf (fp, "mac_addr_%s , &mac_addr_next_%s",
                             p_table_struct->object[i4_count].object_name,
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                default:
                    break;

            }                    /* End of Switch Case */

            /* Break When A Index is Found. */
            break;
        }                        /* End of FOR loop */

    }                            /* End of Main FOR Loop for Each Index. */

    fprintf (fp, "))== SNMP_SUCCESS) {");

    /* 
     *  The FOR LOOP is for Appending the New OID which is got from
     *  the get_next Low Level fn to the p_in_db and the first Index.
     */

    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {

        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            switch (p_table_struct->object[i4_count].type)
            {
                case POS_INTEGER:
                {
                    if ((strcmp
                         (p_table_struct->object[i4_count].type_name,
                          "Counter64")) == 0)
                    {
                        fprintf (fp, "\n               u8_%s = u4_next_%s ;",
                                 p_table_struct->object[i4_count].object_name,
                                 p_table_struct->object[i4_count].object_name);
                        fprintf (fp,
                                 "\n               p_in_db->pu4_OidList[p_in_db->u4_Length++] = u8_next_%s;",
                                 p_table_struct->object[i4_count].object_name);
                    }
                    else
                    {
                        fprintf (fp, "\n               u4_%s = u4_next_%s ;",
                                 p_table_struct->object[i4_count].object_name,
                                 p_table_struct->object[i4_count].object_name);
                        fprintf (fp,
                                 "\n               p_in_db->pu4_OidList[p_in_db->u4_Length++] = u4_next_%s;",
                                 p_table_struct->object[i4_count].object_name);
                    }
                    break;
                }
                case INTEGER:
                {
                    fprintf (fp, "\n               i4_%s = i4_next_%s ;",
                             p_table_struct->object[i4_count].object_name,
                             p_table_struct->object[i4_count].object_name);
                    fprintf (fp,
                             "\n               p_in_db->pu4_OidList[p_in_db->u4_Length++] = (UINT4) i4_next_%s;",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                case OCTET_STRING:
                {
                    fprintf (fp,
                             "\n                   /*  Storing the Value of the Len of Octet Str in p_in_db. */");
                    /*
                     * if condition removed to fix incorrect extractioon of
                     * OCTET STRING type index
                     * changed by soma on 07/06/98
                     */
                    fprintf (fp,
                             "\n                   p_in_db->pu4_OidList[p_in_db->u4_Length++] = poctet_next_%s->i4_Length;",
                             p_table_struct->object[i4_count].object_name);
                    fprintf (fp,
                             "\n                   for(i4_count = FALSE; i4_count < poctet_next_%s->i4_Length ; i4_count++)",
                             p_table_struct->object[i4_count].object_name);
                    fprintf (fp,
                             "\n                       p_in_db->pu4_OidList[p_in_db->u4_Length++] = (UINT4) poctet_next_%s->pu1_OctetList[i4_count] ;",
                             p_table_struct->object[i4_count].object_name);

                    /*  
                     *  Assigning Next Octet Str Index to First
                     *  Index , After Freeing the First Index.
                     */

                    fprintf (fp,
                             "\n                   free_octetstring(poctet_%s);",
                             p_table_struct->object[i4_count].object_name);
                    fprintf (fp,
                             "\n                   poctet_%s = poctet_next_%s;",
                             p_table_struct->object[i4_count].object_name,
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                case OBJECT_IDENTIFIER:
                {
                    fprintf (fp,
                             "\n                   /*  Storing the Value of the Len of OID in p_in_db.  */");
                    fprintf (fp,
                             "\n                   p_in_db->pu4_OidList[p_in_db->u4_Length++] = OID_next_%s->u4_Length;",
                             p_table_struct->object[i4_count].object_name);

                    fprintf (fp,
                             "\n                   for(i4_count = FALSE ; i4_count < OID_next_%s->u4_Length ; i4_count++)",
                             p_table_struct->object[i4_count].object_name);
                    fprintf (fp,
                             "\n                       p_in_db->pu4_OidList[p_in_db->u4_Length++] = OID_next_%s->pu4_OidList[i4_count] ;\n",
                             p_table_struct->object[i4_count].object_name);

                    fprintf (fp,
                             "\n                   /*  Freeing the First Oid and Storing the Next Oid In it. */");
                    fprintf (fp, "\n                   free_oid(OID_%s);",
                             p_table_struct->object[i4_count].object_name);
                    /*  Assigning the Next Oid to the First One. */
                    fprintf (fp,
                             "\n                   /*  Assigning the Next Oid to the First Oid. */");
                    fprintf (fp, "\n                   OID_%s = OID_next_%s;",
                             p_table_struct->object[i4_count].object_name,
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                case ADDRESS:
                {
                    fprintf (fp,
                             "\n                   u4_addr_%s = u4_addr_next_%s ;",
                             p_table_struct->object[i4_count].object_name,
                             p_table_struct->object[i4_count].object_name);
                    fprintf (fp,
                             "\n                   *((UINT4 *) (u1_addr_next_%s)) =  OSIX_HTONL(u4_addr_%s);\n",
                             p_table_struct->object[i4_count].object_name,
                             p_table_struct->object[i4_count].object_name);

                    fprintf (fp,
                             "\n                   for(i4_count = FALSE; i4_count < ADDR_LEN ; i4_count++)");
                    fprintf (fp,
                             "\n                       p_in_db->pu4_OidList[p_in_db->u4_Length++] = (UINT4) u1_addr_next_%s[i4_count] ;",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                case MacAddress:
                {

                    fprintf (fp, "\n                   /*");
                    fprintf (fp,
                             "\n                    *  This FOR loop is for storing the content");
                    fprintf (fp,
                             "\n                    *  of the MacAddr struct into the p_in_db.");
                    fprintf (fp, "\n                    */\n");
                    fprintf (fp,
                             "\n                   for(i4_count = FALSE ; i4_count < MAC_ADDR_LEN ; i4_count++)");
                    fprintf (fp,
                             "\n                       p_in_db->pu4_OidList[p_in_db->u4_Length++] = (UINT4) mac_addr_next_%s[i4_count];",
                             p_table_struct->object[i4_count].object_name);

                    /*  Assigning the Next Mac Addr to the First One. */

                    fprintf (fp,
                             "\n                    /* Assigning the Next Mac Addr to First One. */");

                    fprintf (fp,
                             "\n                    MEMCPY ( mac_addr_%s , mac_addr_next_%s, MAC_ADDR_LEN );",
                             p_table_struct->object[i4_count].object_name,
                             p_table_struct->object[i4_count].object_name);

                    break;
                }
                default:
                    break;

            }                    /* End of Switch Case */

            /* Break When A Index is Found. */
            break;
        }                        /* End of FOR loop. */

    }                            /* End of Main FOR loop for Each Index. */

    fprintf (fp, "\n           }");
    fprintf (fp, "\n           else {");

    /*  Freeing all the Octet String Indices. */
    free_all_octetstrings (fp, GET_FIRST_FLAG);

    /*  Freeing all the Next Octet String Indices. */
    free_all_octetstrings (fp, GET_NEXT_FLAG);

    /*  Freeing all the OID Indices. */
    free_all_oids (fp, GET_FIRST_FLAG);

    /*  Freeing all the Next OID Indices. */
    free_all_oids (fp, GET_NEXT_FLAG);

    fprintf (fp, "\n                    return ((tSNMP_VAR_BIND*) NULL);");
    fprintf (fp, "\n           }");
    fprintf (fp, "\n        }");
    fprintf (fp, "\n        else {");
    fprintf (fp, "\n                    return ((tSNMP_VAR_BIND*) NULL);");
    fprintf (fp, "\n        }");
    fprintf (fp, "\n        break ;");
    fprintf (fp, "\n    }");
    fprintf (fp, "\n    default :");
    fprintf (fp, "\n        return ((tSNMP_VAR_BIND*) NULL);\n");
    fprintf (fp, "\n } /* End of SWITCH statement. */\n");

    /* GET for all objects starts here.  */

    fprintf (fp, "\n /*");
    fprintf (fp,
             "\n  *  The SWITCH CASE statement for calling the Low Level fn");
    fprintf (fp, "\n  *  for all objects Low Level Routines for all Objects.");
    fprintf (fp, "\n  */");
    fprintf (fp, "\n switch(u1_arg)");
    fprintf (fp, "\n {");

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : generate_routine_forall_objects                 *
*      Role of the function : This Fn generates GET Routine which calls the   *
*                             Low Level routines for each Object according to *
*                             their access permissions.                       *
*      Formal Parameters    : fp                                              *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
generate_routine_forall_objects (FILE * fp)
{
    INT4                i4_count = ZERO;
    INT4                i4_no_access[MAX_OBJECTS];
    INT4                i4_no_access_count = ZERO;
    INT4                i4_index_flag = ZERO;
    INT4                i4_index;
    UINT1               i1_temp[MAX_LINE] = NULL_STR;
    UINT1              *pi1_temp;

    /*  Fn for Generating Code for all the Read-only Indices. */
    if (i4_global_scalar_flag <= ZERO)
    {
        getroutine_for_all_indices ();
    }

    /*  FOR Loop for generating Code for all Objects. */
    for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects; i4_count++)
    {
        /* 
         *  This if check for the access permission and writes
         *  to the file only the objects which can be accessed. 
         */
        if ((p_table_struct->object[i4_count].access == NO_ACCESS)
            || (p_table_struct->object[i4_count].access == ACC_FOR_NOTIFY))
        {
            i4_index_flag =
                find_object_is_index (p_table_struct->object[i4_count].
                                      object_name);
            if (i4_index_flag == NOT_INDEX)
            {
                i4_no_access[i4_no_access_count++] = i4_count;
            }
        }
        else
        {
            i4_index_flag =
                find_object_is_index (p_table_struct->object[i4_count].
                                      object_name);
            if (i4_index_flag == IS_INDEX)
            {
                continue;
            }
            strcpy (i1_temp, p_table_struct->object[i4_count].object_name);
            pi1_temp = strupper (i1_temp);
            fprintf (fp, "\n     case %s:", pi1_temp);
            fprintf (fp, "\n        {");

            /*  
             *  Finding whether the Object is of OCTET STRING TYPE
             *  and allocating memory for the OCTET STRING TYPE.
             */
            allocmem_for_octetstring_retvalue (fp, i4_count);

            /*  
             *  Finding whether the Object is of type OID and
             *  allocating memory for the object of type OID.
             */
            allocmem_for_oid_retvalue (fp, i4_count);

            /* 
             * this is to conform to the Hungarian styled function names
             * used in low level code
             * added by soma on 07/03/98
             */
            p_table_struct->object[i4_count].object_name[0] =
                toupper (p_table_struct->object[i4_count].object_name[0]);
            fprintf (fp, "\n          i1_ret_val = nmhGet%s (",
                     p_table_struct->object[i4_count].object_name);
            /*
             * this is to restore the change made above
             * added by soma on 07/03/98
             */
            p_table_struct->object[i4_count].object_name[0] =
                tolower (p_table_struct->object[i4_count].object_name[0]);

            /* The Fn for sending the Index variable to Low Level Fn. */
            passing_indices_to_lowlevel_routine (fp);

            /*
             *  The SWITCH CASE statement for passing the return value
             *  args to the Low Level function according to their type.
             */
            if (p_table_struct->no_of_indices > ZERO)
            {
                fprintf (fp, ",");
            }

            switch (p_table_struct->object[i4_count].type)
            {
                case POS_INTEGER:
                {
                    if (strcmp
                        (p_table_struct->object[i4_count].type_name,
                         "Counter64") == 0)
                        fprintf (fp, "&u8_counter_val");
                    else
                        fprintf (fp, "&u4_counter_val");
                    break;
                }
                case INTEGER:
                {
                    fprintf (fp, "&i4_return_val");
                    break;
                }
                case OCTET_STRING:
                {
                    fprintf (fp, "poctet_retval_%s",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                case OBJECT_IDENTIFIER:
                {
                    fprintf (fp, "pOidValue");
                    break;
                }
                case ADDRESS:
                {
                    fprintf (fp, "&u4_addr_ret_val_%s",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                case MacAddress:
                {
                    fprintf (fp, "&mac_addr_ret_val_%s",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
            }                    /* End of Switch */

            fprintf (fp, ");");

            /*  Freeing All the Oids and Octet Strings. */
            free_all_octetstrings (fp, GET_FIRST_FLAG);
            free_all_oids (fp, GET_FIRST_FLAG);

            fprintf (fp, "\n          if (i1_ret_val == SNMP_SUCCESS) {");

            /* 
             *  Switch Case statement for storing the Value of the OBJECT
             *  requested in the corresponding variable for passing to 
             *  the FormVarbind function.
             */

            switch (p_table_struct->object[i4_count].type)
            {
                case POS_INTEGER:
                {
                    if (strcmp
                        (p_table_struct->object[i4_count].type_name,
                         "Unsigned32") == 0)
                    {
                        fprintf (fp,
                                 "\n             i2_type = SNMP_DATA_TYPE_UNSIGNED32 ;");
                    }
                    if (strcmp
                        (p_table_struct->object[i4_count].type_name,
                         "Counter32") == 0)
                    {
                        fprintf (fp,
                                 "\n             i2_type = SNMP_DATA_TYPE_COUNTER32 ;");
                    }
                    else if (strcmp
                             (p_table_struct->object[i4_count].type_name,
                              "Counter64") == 0)
                    {
                        fprintf (fp,
                                 "\n             i2_type = SNMP_DATA_TYPE_COUNTER64 ;");
                    }
                    else if (strcmp
                             (p_table_struct->object[i4_count].type_name,
                              "Gauge32") == 0)
                    {
                        fprintf (fp,
                                 "\n             i2_type = SNMP_DATA_TYPE_GAUGE32 ;");
                    }
                    else if (strcmp
                             (p_table_struct->object[i4_count].type_name,
                              "TimeTicks") == 0)
                    {
                        fprintf (fp,
                                 "\n             i2_type = SNMP_DATA_TYPE_TIME_TICKS ;");
                    }
                    break;
                }
                case INTEGER:
                {
                    fprintf (fp,
                             "\n             i2_type = SNMP_DATA_TYPE_INTEGER ;");
                    break;
                }
                case OCTET_STRING:
                {
                    fprintf (fp,
                             "\n             i2_type = SNMP_DATA_TYPE_OCTET_PRIM ;");
                    fprintf (fp,
                             "\n\n             /*  Assigning the Octet Str Ptr Got From Low Level Rtns. */");
                    fprintf (fp,
                             "\n             poctet_string = poctet_retval_%s;",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                case OBJECT_IDENTIFIER:
                {
                    fprintf (fp,
                             "\n             i2_type = SNMP_DATA_TYPE_OBJECT_ID ;");
                    break;
                }
                case ADDRESS:
                {
                    fprintf (fp,
                             "\n             i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;");
                    fprintf (fp,
                             "\n             /* This part of the Code converts the ADDR to Octet String. */");

                    fprintf (fp,
                             "\n             *((UINT4 *) (u1_octet_string)) = OSIX_HTONL(u4_addr_ret_val_%s) ;",
                             p_table_struct->object[i4_count].object_name);

                    fprintf (fp,
                             "\n             poctet_string = (tSNMP_OCTET_STRING_TYPE *) SNMP_AGT_FormOctetString(u1_octet_string , ADDR_LEN);");
                    break;
                }
                case MacAddress:
                {
                    fprintf (fp,
                             "\n             i2_type = SNMP_DATA_TYPE_OCTET_PRIM ;");

                    fprintf (fp,
                             "\n             poctet_string = (tSNMP_OCTET_STRING_TYPE *) SNMP_AGT_FormOctetString(mac_addr_ret_val_%s, MAC_ADDR_LEN);",
                             p_table_struct->object[i4_count].object_name);

                    break;
                }
                default:
                    break;

            }                    /* End of Switch */

            fprintf (fp, "\n          }");
            fprintf (fp, "\n          else {");

            /*  Freeing the Octet String Return Value. */
            free_octetstring_retvalue (fp, i4_count);

            if (p_table_struct->object[i4_count].type == OBJECT_IDENTIFIER)
            {
                fprintf (fp, "\n             free_oid(pOidValue);");
            }
            /* End of IF case for freeing OID. */

            fprintf (fp, "\n             return ((tSNMP_VAR_BIND *) NULL);");
            fprintf (fp, "\n          }");
            fprintf (fp, "\n          break;\n     }");
        }                        /* end of if else condition.  */
    }
    /*  
     *  End of FOR Loop which generates the code for
     *  the MIB Objects which are accessible.
     */

    /* FOR LOOP for the Objects which are not accessible.  */

    for (i4_count = ZERO; i4_count < i4_no_access_count; i4_count++)
    {

        strcpy (i1_temp,
                p_table_struct->object[i4_no_access[i4_count]].object_name);
        pi1_temp = strupper (i1_temp);
        fprintf (fp, "\n     case %s :", pi1_temp);

    }

    fprintf (fp, "\n     default :");

    /*  Freeing All the Oids and Octet Strings. */
    free_all_octetstrings (fp, GET_FIRST_FLAG);
    free_all_oids (fp, GET_FIRST_FLAG);

    fprintf (fp, "\n            return((tSNMP_VAR_BIND * )NULL);");
    fprintf (fp, "\n   }/* End Of SWITCH Case for All Objects. */\n");

    if (i4_global_scalar_flag)
    {

        /* 
         *  Adding the .0 to the Oid of a Scalar 
         *  Object and Incrementing the Length. 
         */
        fprintf (fp, "\n   /* Incrementing the Length of the p_in_db. */");
        fprintf (fp, "\n   p_in_db->u4_Length++;");
        fprintf (fp,
                 "\n   /* Adding the .0 to the p_in_db for scalar Objects. */");
        fprintf (fp, "\n   p_in_db->pu4_OidList[p_in_db->u4_Length-1] = ZERO;");

    }
    /*  Fn for Exit Trace Logs Get Routine. */

    fprintf (fp,
             "\n\n  return(SNMP_AGT_FormVarBind (p_in_db , i2_type , u4_counter_val , i4_return_val, poctet_string , pOidValue,u8_counter_val));\n");
    fprintf (fp, "\n} /*   THE GET FUNCTION GETS OVER . */\n\n");

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : trace_logs_exit                                 *
*      Role of the function : This Fn generates the Exit Trace Logs for the   *
*                             Mid level routines.                             *
*      Formal Parameters    : fp                                              *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
trace_logs_exit (FILE * file)
{
    fprintf (file,
             "\n   /*** $$TRACE_LOG (EXIT,\"  i2_type = %%d\\n\",i2_type); ***/");
    fprintf (file,
             "\n   /*** $$TRACE_LOG (EXIT,\"  u4_counter_val = %%u\\n\",u4_counter_val); ***/");
    fprintf (file,
             "\n   /*** $$TRACE_LOG (EXIT,\"  i2_type = %%d\\n\",i4_return_val); ***/");

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : find_object_is_index                            *
*      Role of the function : This Fn finds whether the Given Object is an    *
*                             Index or not.                                   *
*      Formal Parameters    : pi1_string                                      *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : IS_INDEX (if the object is anIndex) else        * 
*                             NOT_INDEX.                                      *
******************************************************************************/
INT4
find_object_is_index (UINT1 *pi1_string)
{
    INT4                i4_count;

    for (i4_count = ZERO; i4_count < p_table_struct->no_of_indices; i4_count++)
    {

        if (strcmp (pi1_string, p_table_struct->index_name[i4_count]) == ZERO)
        {
            return IS_INDEX;
        }

    }

    return NOT_INDEX;

}                                /*  End Of Function. */

/******************************************************************************
*      function Name        : find_index_type_OIDorOctet                      *
*      Role of the function : This Fn finds whether the Given Index is of     *
*                             type OID or Octet String.                       *
*      Formal Parameters    : INT4 i4_count.                                  *
*      Global Variables     : p_table_struct , type_table                     *
*      Use of Recursion     : None                                            *
*      Return Value         : TRUE if the type of Index is OID or Octet Str   * 
*                             Else FALSE.                                     *  
******************************************************************************/
INT4
find_index_type_OIDorOctet (INT4 i4_count)
{
    INT4                i4_index;

    if (p_table_struct->object[i4_count].type == OCTET_STRING)
    {
        return TRUE;
    }
    else if (p_table_struct->object[i4_count].type == OBJECT_IDENTIFIER)
    {
        return TRUE;
    }
    return FALSE;

}                                /*  End Of Function. */

/******************************************************************************
*      function Name        : passing_indices_to_lowlevel_routine_getfirst    *
*      Role of the function : This Fn generates the Code for all the objects  *
*                             It passes args to the Low Lev Fn according to   *
*                             the type for GET FIRST.                         *
*      Formal Parameters    : fp                                              *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
passing_indices_to_lowlevel_routine_getfirst (FILE * fp)
{
    INT4                i4_count;
    INT4                i4_counter;
    INT4                i4_index;

    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {

        if (i4_counter > ZERO)
        {
            fprintf (fp, " , ");
        }

        /*
         *  For Loop which check's for the Object Name
         *  which Matches with the Index Name.
         */
        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            switch (p_table_struct->object[i4_count].type)
            {
                case POS_INTEGER:
                {
                    if ((strcmp
                         (p_table_struct->object[i4_count].type_name,
                          "Counter64")) == 0)
                        fprintf (fp, "&u8_%s",
                                 p_table_struct->object[i4_count].object_name);
                    else
                        fprintf (fp, "&u4_%s",
                                 p_table_struct->object[i4_count].object_name);
                    break;
                }
                case INTEGER:
                {
                    fprintf (fp, "&i4_%s",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                case OCTET_STRING:
                {
                    fprintf (fp, "poctet_%s",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                case OBJECT_IDENTIFIER:
                {
                    fprintf (fp, "OID_%s",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                case ADDRESS:
                {
                    fprintf (fp, "&u4_addr_%s",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                case MacAddress:
                {
                    fprintf (fp, "&mac_addr_%s",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                default:
                    break;

            }                    /* End of Switch Case */

            /* Break After One Index if found. */
            break;
        }                        /* End of FOR loop */

    }                            /* End Of Main FOR LOOP For Each Index. */

}                                /* End of Function */

/******************************************************************************
*      function Name        : passing_indices_to_lowlevel_routine             *
*      Role of the function : This Fn generates the Code for all the objects  *
*                             It passes args to the Low Lev Fn according to   *
*                             the type .                                      *
*      Formal Parameters    : fp                                              *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
passing_indices_to_lowlevel_routine (FILE * fp)
{
    INT4                i4_count;
    INT4                i4_counter;
    INT4                i4_index;

    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {

        if (i4_counter > ZERO)
        {
            fprintf (fp, " , ");
        }

        /*  
         *  For Loop which check's for the Object Name
         *  which Matches with the Index Name.
         */
        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            switch (p_table_struct->object[i4_count].type)
            {
                case POS_INTEGER:
                {
                    if ((strcmp
                         (p_table_struct->object[i4_count].type_name,
                          "Counter64")) == 0)
                        fprintf (fp, "u8_%s",
                                 p_table_struct->object[i4_count].object_name);

                    else
                        fprintf (fp, "u4_%s",
                                 p_table_struct->object[i4_count].object_name);
                    break;
                }
                case INTEGER:
                {
                    fprintf (fp, "i4_%s",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                case OCTET_STRING:
                {
                    fprintf (fp, "poctet_%s",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                case OBJECT_IDENTIFIER:
                {
                    fprintf (fp, "OID_%s",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                case ADDRESS:
                {
                    fprintf (fp, "u4_addr_%s",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                case MacAddress:
                {
                    fprintf (fp, "mac_addr_%s",
                             p_table_struct->object[i4_count].object_name);
                    break;
                }
                default:
                    break;

            }                    /* End of Switch Case */

            /* Break After One Index if found.  */
            break;
        }                        /* End of FOR loop */

    }                            /* End Of Main FOR LOOP For Each Index. */

}                                /* End of Function */

/******************************************************************************
*      function Name        : code_for_extracting_partial_indices             *
*      Role of the function : This Fn generates the Code which extracts the   *
*                             partial Indices according to their type.        *
*      Formal Parameters    : fp                                              *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
code_for_extracting_partial_indices (FILE * fp)
{
    INT4                i4_counter;
    INT4                i4_count;
    INT4                i4_OID_Octet_flag;

    fprintf (fp, "\n           /*  Initializing the Partial Index Length. */");
    fprintf (fp, "\n           i4_partial_index_len = p_in_db->u4_Length;\n");

    /* 
     *  FOR Loop For Generating the Code Which Extracts the
     *  Indices(partial)from the OID Given by the Manager.
     */
    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {

        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            fprintf (fp,
                     "\n           if((INT4) p_incoming->u4_Length > i4_partial_index_len) {");

            fprintf (fp,
                     "\n               /*  Adding the Len of Index to i4_partial_index_len Var. */");

            /* 
             *  This Fn Adds the Length of the Index to the Variable
             *  i4_partial_index_len so as to Extract the Indices.
             */
            add_len_of_index (i4_count);

            /*
             *  Allocating Memory for the first and the Next Indices of
             *  type Octet String which are passed to GetNext Low Level fns.
             */
            alloc_mem_for_octetstring_getnext (fp, i4_count);

            /*
             *  This Fn check for malloc failures & frees the Octet Strs 
             *  of the current index & also the previous allocations.
             */
            fn_checking_for_malloc_failure_getnext (fp, i4_counter, i4_count);

            /*
             *  Allocating Memory for the first and the Next Indices 
             *  of type OID which are passed to GetNext Low Level fns.
             */
            alloc_mem_for_oid_getnext (fp, i4_count);

            /*
             *  This Fn check for malloc failures & frees the OID 
             *  of the current index & also the previous allocations.
             */
            fn_checking_for_malloc_failure_getnext_oid (fp, i4_counter,
                                                        i4_count);

            /*  This Fn Generates the Code For Extracting Indices.  */
            extract_one_index (fp, i4_count);

            fprintf (fp, "\n           }");

            /* This fn Finds whether the Index is of type OID or Octet Str. */
            i4_OID_Octet_flag = find_index_type_OIDorOctet (i4_count);

            if (i4_OID_Octet_flag == TRUE)
            {
                fprintf (fp, "\n           else {");
                fprintf (fp, "\n               /*");
                fprintf (fp,
                         "\n                *  Memory Allocation of the (Partial) Index of type OID");
                fprintf (fp,
                         "\n                *  and Octet string which is not given by the Manager.");
                fprintf (fp, "\n                */");
                /*  
                 *  Malloc for Partial Index if the Index is 
                 *  not given (in run time) for OID. 
                 */
                allocmem_oid_for_partial_index (fp, i4_count);

                /*  
                 *  Malloc for Partial Index if the Index is 
                 *  not given (in run time) for Octet String.
                 */
                allocmem_octetstring_for_partial_index (fp, i4_count);
                fprintf (fp, "\n           }");
            }                    /* IF Condition for Octet Strings and OIDs. */

            /* Break when One Index is found. */
            break;
        }                        /* End Of FOR Loop. */

    }                            /* End Of Main FOR Loop for Each Index. */

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : add_len_of_index                                *
*      Role of the function : This Fn generates the Code which Finds the Len  *
*                             of the variable len Index & Adds cummulatively  *
*                             to the var used for extracting a Index (i.e.    *
*                             i4_partial_index_len).                          *
*      Formal Parameters    : i4_count                                        *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
add_len_of_index (INT4 i4_count)
{
    int                 i4_index = ZERO;

    switch (p_table_struct->object[i4_count].type)
    {
        case INTEGER:
        case POS_INTEGER:
        {
            fprintf (fp,
                     "\n              i4_partial_index_len += INTEGER_LEN;");
            break;
        }
        case ADDRESS:
        {
            fprintf (fp, "\n              i4_partial_index_len += ADDR_LEN;");
            break;
        }
        case MacAddress:
        {
            fprintf (fp,
                     "\n              i4_partial_index_len += MAC_ADDR_LEN;");
            break;
        }
        case OCTET_STRING:
        {
            /*
             * if condition removed to fix incorrect extraction of
             * OCTET STRING type index
             * changed by soma on 07/06/98
             */
            fprintf (fp,
                     "\n              i4_len_Octet_index_%s = p_incoming->pu4_OidList[i4_partial_index_len++] ;",
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "\n              i4_partial_index_len += i4_len_Octet_index_%s ;",
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case OBJECT_IDENTIFIER:
        {
            fprintf (fp,
                     "\n               i4_len_OID_index_%s = p_incoming->pu4_OidList[i4_partial_index_len++] ;",
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "\n               i4_partial_index_len += i4_len_OID_index_%s ;",
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        default:
            break;
    }                            /* End Of SWITCH Case. */

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : extract_one_index                               *
*      Role of the function : This Fn generates the Code which extracts One   *
*                             index from the given OID.                       *
*      Formal Parameters    : fp , i4_count                                   *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
extract_one_index (FILE * fp, INT4 i4_count)
{
    INT4                i4_index = ZERO;

    /* 
     *  Switch Case Statement For Each Data Type To 
     *  generate the code for Extracting An Index.
     */
    switch (p_table_struct->object[i4_count].type)
    {
        case POS_INTEGER:
        {
            if ((strcmp
                 (p_table_struct->object[i4_count].type_name,
                  "Counter64")) == 0)
            {
                fprintf (fp,
                         "\n             /* Extracting The Integer Index. */");
                fprintf (fp,
                         "\n             u8_%s = (UINT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];",
                         p_table_struct->object[i4_count].object_name);
                fprintf (fp, "\n             i4_offset ++ ;\n");
            }
            else
            {
                fprintf (fp,
                         "\n             /* Extracting The Integer Index. */");
                fprintf (fp,
                         "\n             u4_%s = (UINT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];",
                         p_table_struct->object[i4_count].object_name);
                fprintf (fp, "\n             i4_offset ++ ;\n");
            }
            break;
        }
        case INTEGER:
        {
            fprintf (fp, "\n             /* Extracting The Integer Index. */");
            fprintf (fp,
                     "\n             i4_%s = (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset] ;",
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp, "\n             i4_offset ++ ;\n");
            break;
        }
        case OCTET_STRING:
        {
            /*
             * if condition removed to fix incorrect extraction of
             * OCTET STRING type index
             * changed by soma on 07/06/98
             */
            fprintf (fp, "\n            /*");
            fprintf (fp,
                     "\n             *  This is to Increment the Array Pointer by one Which");
            fprintf (fp, "\n             *  Contains the Length of the Index.");
            fprintf (fp, "\n             */");
            fprintf (fp, "\n            i4_offset++;");
            fprintf (fp,
                     "\n             /*  The FOR LOOP for extracting Octet_str Index from the Given OID. */");
            fprintf (fp,
                     "\n            for(i4_count = FALSE ; i4_count < i4_len_Octet_index_%s ; i4_count++ , i4_offset++)",
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "\n                poctet_%s->pu1_OctetList[i4_count] = (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset] ;\n",
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case OBJECT_IDENTIFIER:
        {
            fprintf (fp, "\n             /*");
            fprintf (fp,
                     "\n              *  This is to Increment the Array Pointer by");
            fprintf (fp,
                     "\n              *  one Which Contains the Length of the Index.");
            fprintf (fp, "\n              */");
            fprintf (fp, "\n             i4_offset++;");

            fprintf (fp,
                     "\n             /*  FOR Loop for extracting the Indices of OID Type from Given OID. */");

            fprintf (fp,
                     "\n             for(i4_count = FALSE ; i4_count < i4_len_OID_index_%s ; i4_count++ , i4_offset++)",
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "\n                 OID_%s->pu4_OidList[i4_count] = (UINT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset] ;\n",
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case ADDRESS:
        {
            fprintf (fp,
                     "\n             /*  FOR Loop for extracting the Index of Type Address From Given OID. */");

            fprintf (fp,
                     "\n             for(i4_count = FALSE ; i4_count < ADDR_LEN ; i4_count++ , i4_offset++)");
            fprintf (fp,
                     "\n                 u1_addr_%s[i4_count] = (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];\n",
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "\n             /*  This Library Fn converts the Char Array to a Word of 4 bytes. */");
            fprintf (fp,
                     "\n             u4_addr_%s = OSIX_NTOHL(*((UINT4 *)(u1_addr_%s)));\n",
                     p_table_struct->object[i4_count].object_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case MacAddress:
        {
            fprintf (fp,
                     "\n             /*  FOR Loop for extracting the Indices of Type MacAddress from Given OID. */");
            fprintf (fp,
                     "\n             for(i4_count = FALSE ; i4_count < MAC_ADDR_LEN ; i4_count++ , i4_offset++)");
            fprintf (fp,
                     "\n                 mac_addr_%s[i4_count] = (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset] ;\n",
                     p_table_struct->object[i4_count].object_name);

            break;
        }
        default:
            break;
    }                            /*  End Of Switch Case. */

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : getroutine_for_all_indices                      *
*      Role of the function : This Fn generates the Code which performs the   *
*                             get operation in the Mid level routines. No low *
*                             level Routines is involved in this operation.   *
*      Formal Parameters    : None                                            *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
getroutine_for_all_indices (void)
{
    INT4                i4_counter;
    INT4                i4_count;
    UINT1               i1_temp[MAX_LINE];
    UINT1              *pi1_temp;

    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {

        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects;
             i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            strcpy (i1_temp, p_table_struct->object[i4_count].object_name);
            pi1_temp = strupper (i1_temp);
            fprintf (fp, "\n     case %s:", pi1_temp);
            fprintf (fp, "\n        {");

            /*  
             *  Fn Which Generates the Code for passing 
             *  the Type to the FormVarbind Function.
             */
            passing_i2_type_to_formvarbind_for_index (i4_count);

            fprintf (fp, "\n       /*");
            fprintf (fp,
                     "\n        *  This is for the Indices Which are not accessible");
            fprintf (fp,
                     "\n        *  They are not Passed to the Low Level Routine but");
            fprintf (fp,
                     "\n        *  Extracted in the Midlevel function and Returned.");
            fprintf (fp, "\n        */");
            fprintf (fp,
                     "\n       if((u1_search_type == SNMP_SEARCH_TYPE_EXACT)");
            fprintf (fp, "\n       || (i4_partial_index_flag == FALSE)) {");

            /* 
             *  This Fn Generates the Code for Converting
             *  the index to the Destination Type.
             */
            passing_args_to_formvarbind_for_index (i4_count, GET_FIRST_FLAG);

            fprintf (fp, "\n       }");
            fprintf (fp, "\n       else {");

            /* 
             *  This Function Generates the Code for Converting
             *  the index to the Destination Type for Get Next.
             */
            passing_args_to_formvarbind_for_index (i4_count, GET_NEXT_FLAG);

            fprintf (fp, "\n       }");
            if (p_table_struct->object[i4_count].type != OCTET_STRING)
            {
                /*  Freeing the Octet String Indices. */
                free_all_octetstrings (fp, GET_FIRST_FLAG);
            }
            if (p_table_struct->object[i4_count].type != OBJECT_IDENTIFIER)
            {
                /*  Freeing the Oid-Type Indices. */
                free_all_oids (fp, GET_FIRST_FLAG);
            }
            fprintf (fp, "\n       break;");
            fprintf (fp, "\n     }");

            /* Break After Generating Code for an Index. */
            break;
        }                        /*  End Of FOR Loop for All Objects . */

    }                            /*  End Of FOR Loop for Each Index. */

}                                /*  End Of Function. */

/******************************************************************************
*      function Name        : passing_i2_type_to_formvarbind_for_index        *
*      Role of the function : This Fn generates the Code for storing the value*
*                             in the var i2_type which is passed to the       *
*                             FormVarbind Fn.                                 *
*                             level Routines is involved in this operation.   *
*      Formal Parameters    : i4_count                                        *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
passing_i2_type_to_formvarbind_for_index (INT4 i4_count)
{
    INT4                i4_index;

    switch (p_table_struct->object[i4_count].type)
    {
        case POS_INTEGER:
        {
            if (strcmp (p_table_struct->object[i4_count].type_name, "Counter32")
                == 0)
            {
                fprintf (fp, "\n       i2_type = SNMP_DATA_TYPE_COUNTER32 ;\n");
            }
            else if (strcmp
                     (p_table_struct->object[i4_count].type_name,
                      "Gauge32") == 0)
            {
                fprintf (fp, "\n       i2_type = SNMP_DATA_TYPE_GAUGE32 ;\n");
            }
            else if (strcmp
                     (p_table_struct->object[i4_count].type_name,
                      "Unsigned32") == 0)
            {
                fprintf (fp,
                         "\n       i2_type = SNMP_DATA_TYPE_UNSIGNED32 ;\n");
            }
            else if (strcmp
                     (p_table_struct->object[i4_count].type_name,
                      "Counter64") == 0)
            {
                fprintf (fp, "\n       i2_type = SNMP_DATA_TYPE_COUNTER64 ;\n");
            }
            else if (strcmp
                     (p_table_struct->object[i4_count].type_name,
                      "TimeTicks") == 0)
            {
                fprintf (fp,
                         "\n       i2_type = SNMP_DATA_TYPE_TIME_TICKS ;\n");
            }
            break;
        }
        case INTEGER:
            fprintf (fp, "\n       i2_type = SNMP_DATA_TYPE_INTEGER;\n");
            break;
        case OCTET_STRING:
            fprintf (fp, "\n       i2_type = SNMP_DATA_TYPE_OCTET_PRIM;\n");
            break;
        case OBJECT_IDENTIFIER:
            fprintf (fp, "\n       i2_type = SNMP_DATA_TYPE_OBJECT_ID;\n");
            break;
        case ADDRESS:
            fprintf (fp, "\n       i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;\n");
            break;
        case MacAddress:
            fprintf (fp, "\n       i2_type = SNMP_DATA_TYPE_OCTET_PRIM;\n");
            break;
/*  End of  Case Extensible. */

    }                            /* End Of Main Switch. */

}                                /*  End Of Function. */

/******************************************************************************
*      function Name        : passing_args_to_formvarbind_for_index           *
*      Role of the function : This Fn which converts the Indices into the     *
*                             destination type & passes to the FormVarbind    *
*                             Fn. This is for get routine for Indices which   *
*                             are not accessible GET NEXT.                    *
*      Formal Parameters    : i4_count                                        *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
passing_args_to_formvarbind_for_index (INT4 i4_count, INT4 i4_get_flag)
{
    INT4                i4_index;

    switch (p_table_struct->object[i4_count].type)
    {
        case POS_INTEGER:
        {
            if ((strcmp
                 (p_table_struct->object[i4_count].type_name, "Countr64")) == 0)
            {
                if (i4_get_flag == GET_FIRST_FLAG)
                {
                    fprintf (fp, "\n             u8_counter_val = u8_%s ;",
                             p_table_struct->object[i4_count].object_name);
                }
                else if (i4_get_flag == GET_NEXT_FLAG)
                {
                    fprintf (fp, "\n             u8_counter_val = u8_next_%s ;",
                             p_table_struct->object[i4_count].object_name);
                }
            }
            else
            {
                if (i4_get_flag == GET_FIRST_FLAG)
                {
                    fprintf (fp, "\n             u4_counter_val = u4_%s ;",
                             p_table_struct->object[i4_count].object_name);
                }
                else if (i4_get_flag == GET_NEXT_FLAG)
                {
                    fprintf (fp, "\n             u4_counter_val = u4_next_%s ;",
                             p_table_struct->object[i4_count].object_name);
                }
            }
            break;
        }
        case INTEGER:
        {
            if (i4_get_flag == GET_FIRST_FLAG)
            {
                fprintf (fp, "\n             i4_return_val = i4_%s ;",
                         p_table_struct->object[i4_count].object_name);
            }
            else if (i4_get_flag == GET_NEXT_FLAG)
            {
                fprintf (fp, "\n             i4_return_val = i4_next_%s ;",
                         p_table_struct->object[i4_count].object_name);
            }
            break;
        }
        case OCTET_STRING:
        {
            fprintf (fp, "\n             poctet_string = ");

            if (i4_get_flag == GET_FIRST_FLAG)
            {
                fprintf (fp, "poctet_%s;",
                         p_table_struct->object[i4_count].object_name);
            }
            else if (i4_get_flag == GET_NEXT_FLAG)
            {
                fprintf (fp, "poctet_next_%s;",
                         p_table_struct->object[i4_count].object_name);
            }
            break;
        }
        case OBJECT_IDENTIFIER:
        {
            if (i4_get_flag == GET_FIRST_FLAG)
            {
                fprintf (fp, "\n             pOidValue = OID_%s;",
                         p_table_struct->object[i4_count].object_name);
            }
            else if (i4_get_flag == GET_NEXT_FLAG)
            {
                fprintf (fp, "\n             pOidValue = OID_next_%s;",
                         p_table_struct->object[i4_count].object_name);
            }
            break;
        }
        case ADDRESS:
        {
            fprintf (fp, "\n             *((UINT4 *)(u1_octet_string))");
            if (i4_get_flag == GET_FIRST_FLAG)
            {
                fprintf (fp, " = OSIX_HTONL (u4_addr_%s);",
                         p_table_struct->object[i4_count].object_name);
            }
            else if (i4_get_flag == GET_NEXT_FLAG)
            {
                fprintf (fp, " = OSIX_HTONL (u4_addr_next_%s);",
                         p_table_struct->object[i4_count].object_name);
            }

            fprintf (fp,
                     "\n             poctet_string = (tSNMP_OCTET_STRING_TYPE *) SNMP_AGT_FormOctetString(u1_octet_string , ADDR_LEN);");
            break;
        }
        case MacAddress:
        {
            fprintf (fp,
                     "\n             poctet_string = (tSNMP_OCTET_STRING_TYPE *)SNMP_AGT_FormOctetString");
            if (i4_get_flag == GET_FIRST_FLAG)
            {
                fprintf (fp, "(mac_addr_%s , MAC_ADDR_LEN) ;",
                         p_table_struct->object[i4_count].object_name);
            }
            else if (i4_get_flag == GET_NEXT_FLAG)
            {
                fprintf (fp, "(mac_addr_next_%s , MAC_ADDR_LEN);",
                         p_table_struct->object[i4_count].object_name);
            }
            break;
        }
    }                            /*  End Of Switch. */

}                                /*  End Of Function. */

/******************************************************************************
*      function Name        : free_all_oids                                   *
*      Role of the function : This Fn Frees all the tSNMP_OID_TYPE            *
*                             variables which were declared to extract the    *
*                             Indices for get exact and get next according to *
*                             the flag (i4_type_flag) which is passed.        *
*      Formal Parameters    : FILE *fp , INT4 i4_type_flag                    *
*      Global Variables     : p_table_struct , type_table                     *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
free_all_oids (FILE * fp, INT4 i4_type_flag)
{
    INT4                i4_count = ZERO;
    INT4                i4_counter = ZERO;
    INT4                i4_index = ZERO;

    /*  Freeing the Indices which are of OID Type.  */
    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {
        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            if (p_table_struct->object[i4_count].type == OBJECT_IDENTIFIER)
            {

                if ((i4_type_flag == GET_FIRST_FLAG)
                    || (i4_type_flag == GET_EXACT_FLAG))
                {
                    fprintf (fp, "\n                 free_oid(OID_%s);",
                             p_table_struct->object[i4_count].object_name);
                }
                if (i4_type_flag == GET_NEXT_FLAG)
                {
                    fprintf (fp, "\n                 free_oid(OID_next_%s);",
                             p_table_struct->object[i4_count].object_name);
                }
            }

            /* Break When A Index is found.  */
            break;
        }                        /* End of FOR Loop for Indices */

    }                            /* End of Main FOR Loop. */

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : free_all_octetstrings                           *
*      Role of the function : This Fn Frees all the tSNMP_OCTET_STRING_TYPE   *
*                             variables which were declared to extract the    *
*                             Indices for get exact and get next according to *
*                             the flag (i4_type_flag) which is passed.        *
*      Formal Parameters    : FILE *fp , INT4 i4_type_flag                    *
*      Global Variables     : p_table_struct , type_table                     *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
free_all_octetstrings (FILE * fp, INT4 i4_type_flag)
{
    INT4                i4_count = ZERO;
    INT4                i4_counter = ZERO;
    INT4                i4_index = ZERO;

    /*  Freeing the Indices which are of Octet String Type.  */
    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {

        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            if (p_table_struct->object[i4_count].type == OCTET_STRING)
            {

                if ((i4_type_flag == GET_FIRST_FLAG)
                    || (i4_type_flag == GET_EXACT_FLAG))
                {
                    fprintf (fp,
                             "\n                 free_octetstring(poctet_%s);",
                             p_table_struct->object[i4_count].object_name);
                }
                if (i4_type_flag == GET_NEXT_FLAG)
                {
                    fprintf (fp,
                             "\n                 free_octetstring(poctet_next_%s);",
                             p_table_struct->object[i4_count].object_name);
                }
            }

            /* Break When A Index is found.  */
            break;
        }                        /* End of FOR Loop for Indices */

    }                            /* End of Main FOR Loop. */

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : alloc_mem_for_oid_getexact_first                *
*      Role of the function : This Fn allocates the memory for the Indices    *
*                             which are of type OID for Getexact.             *
*      Formal Parameters    : FILE *fp                                        *
*      Global Variables     : p_table_struct , type_table                     *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
alloc_mem_for_oid_getexact_first (FILE * fp, INT4 i4_flag)
{
    INT4                i4_counter = ZERO;
    INT4                i4_count = ZERO;
    INT4                i4_index = ZERO;

    /* 
     *  Allocating Memory for the Indices which of OID
     *  Type to the Low Level Get Exact and First case. 
     */
    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {

        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            if (p_table_struct->object[i4_count].type == OBJECT_IDENTIFIER)
            {

                fprintf (fp,
                         "\n             /*  Allocating Memory for the OID Index. */");
                if (i4_flag == GET_EXACT_FLAG)
                {
                    fprintf (fp,
                             "\n             OID_%s = (tSNMP_OID_TYPE *)alloc_oid(i4_len_OID_index_%s);",
                             p_table_struct->object[i4_count].object_name,
                             p_table_struct->object[i4_count].object_name);
                }
                if (i4_flag == GET_FIRST_FLAG)
                {
                    fprintf (fp,
                             "\n             OID_%s = (tSNMP_OID_TYPE *)alloc_oid(MAX_OID_LEN);",
                             p_table_struct->object[i4_count].object_name);
                }
            }
/* End Of Extensible Case. */

            /* Break When A Index is found.  */
            break;
        }                        /* FOR Loop For All Variables. */

    }                            /* Main FOR Loop for Each Index. */

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : alloc_mem_for_octetstring_getexact_first              *
*      Role of the function : This Fn allocates the memory for the Indices    *
*                             which are of type OctetStr for Getexact.        *
*      Formal Parameters    : FILE *fp                                        *
*      Global Variables     : p_table_struct , type_table                     *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
alloc_mem_for_octetstring_getexact_first (FILE * fp, INT4 i4_flag)
{
    INT4                i4_counter = ZERO;
    INT4                i4_count = ZERO;
    INT4                i4_index = ZERO;

    /* 
     *  Allocating Memory for the Indices which are of OctetStr
     *  Type to the Low Level Get Exact and First case. 
     */
    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {

        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            if (p_table_struct->object[i4_count].type == OCTET_STRING)
            {
                if (i4_flag == GET_EXACT_FLAG)
                {
                    fprintf (fp,
                             "\n             /*  Allocating Memory for the Get Exact Octet String Index. */");
                }
                if (i4_flag == GET_FIRST_FLAG)
                {
                    fprintf (fp,
                             "\n             /*  Allocating Memory for the Get First Octet String Index. */");
                }

                /*
                 * code modified to fix incorrect extraction of OCTET STRING
                 * type index - assign correct amount of memory
                 * code changed by soma on 16/06/98
                 */
                if (i4_flag == GET_EXACT_FLAG)
                {
                    fprintf (fp,
                             "\n             poctet_%s = (tSNMP_OCTET_STRING_TYPE *)allocmem_octetstring(i4_len_Octet_index_%s);",
                             p_table_struct->object[i4_count].object_name,
                             p_table_struct->object[i4_count].object_name);
                }
                if (i4_flag == GET_FIRST_FLAG)
                {
                    if (p_table_struct->object[i4_count].size == ZERO)
                    {
                        fprintf (fp,
                                 "\n             poctet_%s = (tSNMP_OCTET_STRING_TYPE *)allocmem_octetstring(MAX_OCTETLEN);",
                                 p_table_struct->object[i4_count].object_name);
                    }
                    else
                    {
                        fprintf (fp,
                                 "\n             poctet_%s = (tSNMP_OCTET_STRING_TYPE *)allocmem_octetstring(%d);",
                                 p_table_struct->object[i4_count].object_name,
                                 p_table_struct->object[i4_count].size);
                    }
                }
            }

            /* Break When A Index is found.  */
            break;
        }                        /* FOR Loop For All Variables. */

    }                            /* Main FOR Loop for Each Index. */

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : fn_checking_for_malloc_failure_oid              *
*      Role of the function : This Fn allocates the memory for the Indices    *
*                             which are of type OctetStr for Getexact.        *
*      Formal Parameters    : FILE *fp                                        *
*      Global Variables     : p_table_struct , type_table                     *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
fn_checking_for_malloc_failure_oid (FILE * fp)
{
    INT4                i4_counter = ZERO;
    INT4                i4_count = ZERO;
    INT4                i4_index = ZERO;

    /* This Var is used to Indicate the Presence of One OID Index. */
    INT4                i4_first_index_flag = ZERO;

    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {

        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            if (p_table_struct->object[i4_count].type == OBJECT_IDENTIFIER)
            {

                if (i4_first_index_flag == ZERO)
                {
                    fprintf (fp, "\n             if(");
                }

                /*  Finding Whether there are more than one Index. */
                if (i4_first_index_flag >= ONE)
                {
                    fprintf (fp, "||");
                }

                i4_first_index_flag++;
                fprintf (fp, "(OID_%s == NULL)",
                         p_table_struct->object[i4_count].object_name);
            }

            /* Break When A Index is found.  */
            break;
        }                        /* FOR Loop For All Variables. */

    }                            /* Main FOR Loop for Each Index. */

    if (i4_first_index_flag != ZERO)
    {
        fprintf (fp, ") {");
        free_all_oids (fp, GET_FIRST_FLAG);
        fprintf (fp, "\n                    return((tSNMP_VAR_BIND *)NULL);");
        fprintf (fp, "\n             }");
    }

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : fn_checking_for_malloc_failure                  *
*      Role of the function : This Fn allocates the memory for the Indices    *
*                             which are of type OctetStr for Getexact.        *
*      Formal Parameters    : FILE *fp                                        *
*      Global Variables     : p_table_struct , type_table                     *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
fn_checking_for_malloc_failure (FILE * fp)
{
    INT4                i4_counter = ZERO;
    INT4                i4_count = ZERO;
    INT4                i4_index = ZERO;

    /* This Var is used to Indicate the Presence of One Octet str Index. */
    INT4                i4_first_index_flag = ZERO;

    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {

        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            if (p_table_struct->object[i4_count].type == OCTET_STRING)
            {

                if (i4_first_index_flag == ZERO)
                {
                    fprintf (fp, "\n             if(");
                }

                /*  Finding Whether there are more than one Index. */
                if (i4_first_index_flag >= ONE)
                {
                    fprintf (fp, "||");
                }

                i4_first_index_flag++;
                fprintf (fp, "(poctet_%s == NULL)",
                         p_table_struct->object[i4_count].object_name);
            }

            /* Break When A Index is found.  */
            break;
        }                        /* FOR Loop For All Variables. */

    }                            /* Main FOR Loop for Each Index. */

    if (i4_first_index_flag != ZERO)
    {
        fprintf (fp, ")");
    }
    if (i4_first_index_flag != ZERO)
    {
        fprintf (fp, " {");
        free_all_octetstrings (fp, GET_FIRST_FLAG);
        fprintf (fp, "\n                    return((tSNMP_VAR_BIND *)NULL);");
        fprintf (fp, "\n             }");
    }

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : fn_checking_for_malloc_failure_getnext_oid      *
*      Role of the function : This Fn allocates the memory for the Indices    *
*                             which are of type OID for Getnext for One       *
*                             Index which is specified by i4_index_count. The *
*                             Var i4_index_count specifies upto which Index   *
*                             has been processed.                             *
*      Formal Parameters    : FILE *fp , INT4 i4_index_count.                 *
*      Global Variables     : p_table_struct , type_table                     *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
fn_checking_for_malloc_failure_getnext_oid (FILE * fp, INT4 i4_index_count,
                                            INT4 i4_count)
{
    INT4                i4_index = ZERO;

    /*  Checking the Validity of the Malloc's Done.  */
    if (p_table_struct->object[i4_count].type == OBJECT_IDENTIFIER)
    {
        fprintf (fp,
                 "\n\n               /*  Checking for the malloc failure.  */");
        fprintf (fp, "\n               if(");
    }
    else
    {
        return;
    }

    if (p_table_struct->object[i4_count].type == OBJECT_IDENTIFIER)
    {
        fprintf (fp, "(OID_%s == NULL)",
                 p_table_struct->object[i4_count].object_name);
        fprintf (fp, " || (OID_next_%s == NULL)",
                 p_table_struct->object[i4_count].object_name);
    }

    fprintf (fp, ")");
    fprintf (fp, "\n               {");

    fprintf (fp, "\n                  /*  Freeing the Current Index. */");
    if (p_table_struct->object[i4_count].type == OBJECT_IDENTIFIER)
    {
        fprintf (fp, "\n                  free_oid(OID_%s);",
                 p_table_struct->object[i4_count].object_name);
        fprintf (fp, "\n                  free_oid(OID_next_%s);",
                 p_table_struct->object[i4_count].object_name);
    }

    /*  
     *  This Fn frees the Indices of type OID that
     *  have been allocated  previous to the current Index. 
     */
    free_previous_oid_indices (fp, i4_index_count);

    fprintf (fp, "\n                  return((tSNMP_VAR_BIND *)NULL);");
    fprintf (fp, "\n               }\n");

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : fn_checking_for_malloc_failure_getnext          *
*      Role of the function : This Fn allocates the memory for the Indices    *
*                             which are of type OctetStr for Getnext for One  *
*                             Index which is specified by i4_index_count. The *
*                             Var i4_index_count specifies upto which Index   *
*                             has been processed.                             *
*      Formal Parameters    : FILE *fp , INT4 i4_index_count.                 *
*      Global Variables     : p_table_struct , type_table                     *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
fn_checking_for_malloc_failure_getnext (FILE * fp, INT4 i4_index_count,
                                        INT4 i4_count)
{
    INT4                i4_index = ZERO;

    /*  Checking the Validity of the Malloc's Done.  */
    if (p_table_struct->object[i4_count].type == OCTET_STRING)
    {
        fprintf (fp,
                 "\n\n               /*  Checking for the malloc failure.  */");
        fprintf (fp, "\n               if(");
    }
    else
    {
        return;
    }

    if (p_table_struct->object[i4_count].type == OCTET_STRING)
    {
        fprintf (fp, "(poctet_%s == NULL)",
                 p_table_struct->object[i4_count].object_name);
        fprintf (fp, " || (poctet_next_%s == NULL)",
                 p_table_struct->object[i4_count].object_name);
    }

    fprintf (fp, ") {");

    fprintf (fp, "\n                  /*  Freeing the Current Index. */");
    if (p_table_struct->object[i4_count].type == OCTET_STRING)
    {
        fprintf (fp, "\n                  free_octetstring(poctet_%s);",
                 p_table_struct->object[i4_count].object_name);
        fprintf (fp, "\n                  free_octetstring(poctet_next_%s);",
                 p_table_struct->object[i4_count].object_name);
    }

    /*  
     *  This Fn frees the Indices of type Octet Strings that
     *  have been allocated  previous to the current Index. 
     */
    free_previous_octet_indices (fp, i4_index_count);

    fprintf (fp, "\n                  return((tSNMP_VAR_BIND *)NULL);");
    fprintf (fp, "\n               }\n");

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : free_previous_oid_indices                       *
*      Role of the function : This Fn Frees all the Previous Indices which    *
*                             were allocated memory of type OID for the       *
*                             GetNext Fn. The i4_index_count has the No of    *
*                             Indices which are processed.                    *
*      Formal Parameters    : FILE *fp  , INT4 i4_index_count.                *
*      Global Variables     : p_table_struct , type_table                     *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
free_previous_oid_indices (FILE * fp, INT4 i4_index_count)
{
    INT4                i4_count;
    INT4                i4_counter;
    INT4                i4_index;

    for (i4_counter = ZERO; i4_counter < i4_index_count; i4_counter++)
    {

        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            if (p_table_struct->object[i4_count].type == OBJECT_IDENTIFIER)
            {
                fprintf (fp, "\n                  free_oid(OID_%s);",
                         p_table_struct->object[i4_count].object_name);
                fprintf (fp, "\n                  free_oid(OID_next_%s);",
                         p_table_struct->object[i4_count].object_name);
            }

            /* Break When an Index is found. */
            break;
        }                        /* End of FOR Loop for each Object. */

    }                            /* End of Main FOR Loop for each Index. */

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : free_previous_octet_indices                     *
*      Role of the function : This Fn Frees all the Previous Indices which    *
*                             were allocated memory of type Octet String      *
*                             for the GetNext Case. The i4_index_count has    *
*                             the No of Indices which are processed.          *
*      Formal Parameters    : FILE *fp  , INT4 i4_index_count.                *
*      Global Variables     : p_table_struct , type_table                     *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
free_previous_octet_indices (FILE * fp, INT4 i4_index_count)
{
    INT4                i4_count;
    INT4                i4_counter;
    INT4                i4_index;

    for (i4_counter = ZERO; i4_counter < i4_index_count; i4_counter++)
    {

        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            if (p_table_struct->object[i4_count].type == OCTET_STRING)
            {

                fprintf (fp, "\n                  free_octetstring(poctet_%s);",
                         p_table_struct->object[i4_count].object_name);
                fprintf (fp,
                         "\n                  free_octetstring(poctet_next_%s);",
                         p_table_struct->object[i4_count].object_name);
            }

            /* Break When an Index is found. */
            break;
        }                        /* End of FOR Loop for each Object. */

    }                            /* End of Main FOR Loop for each Index. */
}

/******************************************************************************
*      function Name        : alloc_mem_for_oid_getnext                       *
*      Role of the function : This Fn allocates the memory for the Indices    *
*                             which are of type OID for Get Next.             *
*      Formal Parameters    : FILE *fp                                        *
*      Global Variables     : p_table_struct , type_table                     *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
alloc_mem_for_oid_getnext (FILE * fp, INT4 i4_count)
{
    INT4                i4_index = ZERO;

    /*  Alloc Mem for Index of OID Type passed to low Level Routines. */
    if (p_table_struct->object[i4_count].type == OBJECT_IDENTIFIER)
    {

        fprintf (fp,
                 "\n               /*  Allocating Memory for The OID Get Next Fn. */");

        fprintf (fp,
                 "\n               OID_%s = (tSNMP_OID_TYPE *)alloc_oid(i4_len_OID_index_%s);",
                 p_table_struct->object[i4_count].object_name,
                 p_table_struct->object[i4_count].object_name);
        fprintf (fp,
                 "\n               OID_next_%s = (tSNMP_OID_TYPE *)alloc_oid(MAX_OID_LEN);",
                 p_table_struct->object[i4_count].object_name);
    }

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : allocmem_oid_for_partial_index                  *
*      Role of the function : This Fn allocates the memory for the Indices    *
*                             which are of type OID for Get Next if the       *
*                             partial Indices are not given.(i.e. the else    *
*                             case of the partial Index extraction)           *
*      Formal Parameters    : FILE *fp  , INT4 i4_count                       *
*      Global Variables     : p_table_struct , type_table                     *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
allocmem_oid_for_partial_index (FILE * fp, INT4 i4_count)
{
    INT4                i4_index = ZERO;

    if (p_table_struct->object[i4_count].type == OBJECT_IDENTIFIER)
    {
        fprintf (fp, "\n               OID_%s = alloc_oid(MAX_OID_LEN);",
                 p_table_struct->object[i4_count].object_name);
        fprintf (fp, "\n               OID_next_%s = alloc_oid(MAX_OID_LEN);",
                 p_table_struct->object[i4_count].object_name);
        fprintf (fp, "\n               if((OID_%s == NULL)",
                 p_table_struct->object[i4_count].object_name);
        fprintf (fp, "\n               || (OID_next_%s == NULL)) {",
                 p_table_struct->object[i4_count].object_name);
        fprintf (fp, "\n                  free_oid(OID_%s);",
                 p_table_struct->object[i4_count].object_name);
        fprintf (fp, "\n                  free_oid(OID_next_%s);",
                 p_table_struct->object[i4_count].object_name);

        /* Free all the Previous Oids and Octet Strings. */
        free_previous_octet_indices (fp, i4_count);
        free_previous_oid_indices (fp, i4_count);

        fprintf (fp, "\n                  return  ((tSNMP_VAR_BIND *)NULL);");
        fprintf (fp, "\n               }");
    }

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : alloc_mem_for_octetstring_getnext               *
*      Role of the function : This Fn allocates the memory for the Indices    *
*                             which are of type OctetStr for Get Next.        *
*      Formal Parameters    : FILE *fp                                        *
*      Global Variables     : p_table_struct , type_table                     *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
alloc_mem_for_octetstring_getnext (FILE * fp, INT4 i4_count)
{
    INT4                i4_index = ZERO;

    /* 
     *  Allocating Memory for One Index of OctetStr
     *  Type , passed to the Low Level Get Next Case. 
     */

    if (p_table_struct->object[i4_count].type == OCTET_STRING)
    {
        fprintf (fp,
                 "\n               /*  Allocating Memory for The Octet Str Get Next Index. */");
        /*  For First Index. */
        /*
         * if condition removed to to allocate correct amount of memory for
         * OCTET STRING type index
         * changed by soma on 07/06/98
         */
        fprintf (fp,
                 "\n               poctet_%s = (tSNMP_OCTET_STRING_TYPE *)allocmem_octetstring(i4_len_Octet_index_%s);",
                 p_table_struct->object[i4_count].object_name,
                 p_table_struct->object[i4_count].object_name);

        /*  For Next Index. */
        if (p_table_struct->object[i4_count].size == ZERO)
        {
            fprintf (fp,
                     "\n               poctet_next_%s = (tSNMP_OCTET_STRING_TYPE *)allocmem_octetstring(MAX_OCTETLEN);",
                     p_table_struct->object[i4_count].object_name);
        }
        else
        {
            fprintf (fp,
                     "\n               poctet_next_%s = (tSNMP_OCTET_STRING_TYPE *)allocmem_octetstring(%d);",
                     p_table_struct->object[i4_count].object_name,
                     p_table_struct->object[i4_count].size);
        }
    }

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : allocmem_for_octetstring_retvalue               *
*      Role of the function : This Fn allocates the memory for the Object     *
*                             which are of type OctetStr for Return Value.    *
*      Formal Parameters    : FILE *fp , INT4 i4_count                        *
*      Global Variables     : p_table_struct , type_table                     *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
allocmem_for_octetstring_retvalue (FILE * fp, INT4 i4_count)
{
    INT4                i4_index;

    if (p_table_struct->object[i4_count].type == OCTET_STRING)
    {
        if (p_table_struct->object[i4_count].size == ZERO)
        {
            fprintf (fp,
                     "\n          poctet_retval_%s = (tSNMP_OCTET_STRING_TYPE *)allocmem_octetstring(MAX_OCTETLEN);",
                     p_table_struct->object[i4_count].object_name);
        }
        else
        {
            fprintf (fp,
                     "\n          poctet_retval_%s = (tSNMP_OCTET_STRING_TYPE *)allocmem_octetstring(%d);",
                     p_table_struct->object[i4_count].object_name,
                     p_table_struct->object[i4_count].size);
        }

        fprintf (fp, "\n          if(poctet_retval_%s == NULL) {",
                 p_table_struct->object[i4_count].object_name);

        /*  Freeing all the Oid Type & Octet String Type Indices. */
        free_all_oids (fp, GET_FIRST_FLAG);
        free_all_octetstrings (fp, GET_FIRST_FLAG);

        fprintf (fp, "\n             return ((tSNMP_VAR_BIND *)NULL);");
        fprintf (fp, "\n          }");
    }

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : free_octetstring_retvalue                       *
*      Role of the function : This Fn allocates the frees the return Value    *
*                             for Octet String type                           *
*      Formal Parameters    : FILE *fp , i4_count                             *
*      Global Variables     : p_table_struct , type_table                     *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
free_octetstring_retvalue (FILE * fp, INT4 i4_count)
{
    INT4                i4_index;

    if (p_table_struct->object[i4_count].type == OCTET_STRING)
    {

        /* Freeing the Octet String Return Value. */
        fprintf (fp, "\n             free_octetstring(poctet_retval_%s);",
                 p_table_struct->object[i4_count].object_name);

    }

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : allocmem_for_oid_retvalue                       *
*      Role of the function : This Fn allocates the memory for the Object     *
*                             which are of type OID for Return Value.         *
*      Formal Parameters    : FILE *fp  INT4 i4_count                         *
*      Global Variables     : p_table_struct , type_table                     *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
allocmem_for_oid_retvalue (FILE * fp, INT4 i4_count)
{
    INT4                i4_index;

    if (p_table_struct->object[i4_count].type == OBJECT_IDENTIFIER)
    {

        /*  Allocating Memory for the OID Type ret Value. */
        fprintf (fp,
                 "\n          pOidValue = (tSNMP_OID_TYPE *) alloc_oid(MAX_OID_LEN);");
        fprintf (fp, "\n          if(pOidValue == NULL) {");

        /*  Freeing all the Oid Type & Octet String Type Indices. */
        free_all_oids (fp, GET_FIRST_FLAG);
        free_all_octetstrings (fp, GET_FIRST_FLAG);

        fprintf (fp, "\n             return ((tSNMP_VAR_BIND *) NULL);");
        fprintf (fp, "\n          }");
    }

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : allocmem_octetstring_for_partial_index          *
*      Role of the function : This Fn allocates the memory for the Indices    *
*                             which are of type Octet String for Get Next if  *
*                             the partial Indiex are not given.(i.e. the else *
*                             case of the partial Index extraction)           *
*      Formal Parameters    : FILE *fp  , INT4 i4_count                       *
*      Global Variables     : p_table_struct , type_table                     *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
allocmem_octetstring_for_partial_index (FILE * fp, INT4 i4_count)
{
    INT4                i4_index = ZERO;

    if (p_table_struct->object[i4_count].type == OCTET_STRING)
    {
        /*
         * if condition to allocate exact amount of memory for
         * OCTET STRING type indices
         * changed by soma on 16/06/98
         */
        if (p_table_struct->object[i4_count].size == ZERO)
        {
            fprintf (fp,
                     "\n               poctet_%s = allocmem_octetstring(MAX_OCTETLEN);",
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "\n               poctet_next_%s = allocmem_octetstring(MAX_OCTETLEN);",
                     p_table_struct->object[i4_count].object_name);
        }
        else
        {
            fprintf (fp,
                     "\n               poctet_%s = allocmem_octetstring(%d);",
                     p_table_struct->object[i4_count].object_name,
                     p_table_struct->object[i4_count].size);
            fprintf (fp,
                     "\n               poctet_next_%s = allocmem_octetstring(%d);",
                     p_table_struct->object[i4_count].object_name,
                     p_table_struct->object[i4_count].size);
        }
        fprintf (fp, "\n               if((poctet_%s == NULL)",
                 p_table_struct->object[i4_count].object_name);
        fprintf (fp, "\n               || (poctet_next_%s == NULL)) {",
                 p_table_struct->object[i4_count].object_name);
        fprintf (fp, "\n                  free_octetstring(poctet_%s);",
                 p_table_struct->object[i4_count].object_name);
        fprintf (fp, "\n                  free_octetstring(poctet_next_%s);",
                 p_table_struct->object[i4_count].object_name);

        /*  Free all the Previous Oids and Octet Strings. */
        free_previous_octet_indices (fp, i4_count);
        free_previous_oid_indices (fp, i4_count);

        fprintf (fp, "\n                  return  ((tSNMP_VAR_BIND *)NULL);");
        fprintf (fp, "\n               }");
    }

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : gencode_for_alloc_octet_fn                      *
*      Role of the function : This Fn genrates the Code which write to the    *
*                             _mid.c file the fn allocmem_octetstring.        *
*      Formal Parameters    : FILE *fp                                        *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
gencode_for_alloc_oid_fn (FILE * fp)
{
    fprintf (fp, "\n/" DOT);
    fprintf (fp, "\n Function    :  alloc_oid");
    fprintf (fp,
             "\n Description :  This Function Allocates the Memory for an Object of OID Type.");
    fprintf (fp, "\n Input       :  i4_size: Size of Oid.");
    fprintf (fp,
             "\n Output      :  temp   : Allocation of Memory for the Oid for Given Size.");
    fprintf (fp, "\n Returns     :  Pointer of tSNMP_OID_TYPE.");
    fprintf (fp, "\n" DOT "/");

    fprintf (fp, "\ntSNMP_OID_TYPE * alloc_oid(INT4 i4_size)");
    fprintf (fp, "\n{");
    fprintf (fp, "\n    tSNMP_OID_TYPE * temp;\n");
    fprintf (fp, "\n    /*  This Fn Allocates Memory for the Oid. */");
    fprintf (fp,
             "\n    temp = (tSNMP_OID_TYPE *) malloc(sizeof(tSNMP_OID_TYPE));");
    fprintf (fp, "\n    if(temp == NULL) {");
    fprintf (fp, "\n        return (NULL);");
    fprintf (fp, "\n    }");
    fprintf (fp,
             "\n    temp->pu4_OidList = (UINT4 *) malloc(sizeof(UINT4) * i4_size);\n");
    fprintf (fp, "\n    if(temp->pu4_OidList == NULL) {");
    fprintf (fp, "\n        free(temp);");
    fprintf (fp, "\n        return (NULL);");
    fprintf (fp, "\n    }");
    fprintf (fp, "\n    temp->u4_Length = (UINT4) i4_size;");
    fprintf (fp, "\n    return temp;");
    fprintf (fp, "\n}");

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : gencode_for_free_oid_fn                         *
*      Role of the function : This Fn genrates the Code which write to the    *
*                             _mid.c file the fn free_octetstring.            *
*      Formal Parameters    : FILE *fp                                        *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
gencode_for_free_oid_fn (FILE * fp)
{

    fprintf (fp, "\n/" DOT);
    fprintf (fp, "\n Function    :  free_oid");
    fprintf (fp,
             "\n Description :  This Function Frees the Memory for an Object of OID Type.");
    fprintf (fp, "\n Input       :  temp : The Ptr which is to be freed.");
    fprintf (fp, "\n Output      :  Frees the Given Oid Pointer.");
    fprintf (fp, "\n Returns     :  None.");
    fprintf (fp, "\n" DOT "/");

    fprintf (fp, "\nvoid free_oid(tSNMP_OID_TYPE * temp)");
    fprintf (fp, "\n{");
    fprintf (fp, "\n    if(temp == NULL) {");
    fprintf (fp, "\n       return;");
    fprintf (fp, "\n    }");
    fprintf (fp, "\n    free(temp->pu4_OidList);");
    fprintf (fp, "\n    free(temp);");
    fprintf (fp, "\n    return;");
    fprintf (fp, "\n}");

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : gencode_for_alloc_octet_fn                      *
*      Role of the function : This Fn genrates the Code which write to the    *
*                             _mid.c file the fn allocmem_octetstring.        *
*      Formal Parameters    : FILE *fp                                        *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
gencode_for_alloc_octet_fn (FILE * fp)
{

    fprintf (fp, "\n/" DOT);
    fprintf (fp, "\n Function    :  allocmem_octetstring");
    fprintf (fp,
             "\n Description :  This Function Allocates Memory for Octet String Type.");
    fprintf (fp, "\n Input       :  i4_size : The Size of Octet String.");
    fprintf (fp, "\n Output      :  Allocated Octet String Pointer.");
    fprintf (fp, "\n Returns     :  Pointer to an Octet String Variable.");
    fprintf (fp, "\n" DOT "/");

    fprintf (fp,
             "\ntSNMP_OCTET_STRING_TYPE * allocmem_octetstring(INT4 i4_size)");
    fprintf (fp, "\n{");
    fprintf (fp, "\n    tSNMP_OCTET_STRING_TYPE * temp;\n");
    fprintf (fp, "\n    /*  This Fn Allocates Memory for the Octet String. */");
    fprintf (fp,
             "\n    temp = (tSNMP_OCTET_STRING_TYPE *) malloc(sizeof(tSNMP_OCTET_STRING_TYPE));");
    fprintf (fp, "\n    if(temp == NULL) {");
    fprintf (fp, "\n        return (NULL);");
    fprintf (fp, "\n    }");
    fprintf (fp, "\n    temp->pu1_OctetList = (UINT1 *) malloc(i4_size);\n");
    fprintf (fp, "\n    if(temp->pu1_OctetList == NULL) {");
    fprintf (fp, "\n        free(temp);");
    fprintf (fp, "\n        return (NULL);");
    fprintf (fp, "\n    }");
    fprintf (fp, "\n    temp->i4_Length = i4_size;");
    fprintf (fp, "\n    return temp;");
    fprintf (fp, "\n}");

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : gencode_for_free_octet_fn                       *
*      Role of the function : This Fn genrates the Code which write to the    *
*                             _mid.c file the fn free_octetstring.            *
*      Formal Parameters    : FILE *fp                                        *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
gencode_for_free_octet_fn (FILE * fp)
{
    fprintf (fp, "\n/" DOT);
    fprintf (fp, "\n Function    :  free_octetstring");
    fprintf (fp,
             "\n Description :  This Function Frees Memory for Octet String Type Var.");
    fprintf (fp, "\n Input       :  temp : The Pointer to the Octet String.");
    fprintf (fp, "\n Output      :  Frees the Octet String Pointer.");
    fprintf (fp, "\n Returns     :  None.");
    fprintf (fp, "\n" DOT "/");

    fprintf (fp, "\nvoid free_octetstring(tSNMP_OCTET_STRING_TYPE * temp)");
    fprintf (fp, "\n{");
    fprintf (fp, "\n    if(temp == NULL) {");
    fprintf (fp, "\n       return;");
    fprintf (fp, "\n    }");
    fprintf (fp, "\n    free(temp->pu1_OctetList);");
    fprintf (fp, "\n    free(temp);");
    fprintf (fp, "\n    return;");
    fprintf (fp, "\n}");

}                                /* End Of Function. */
