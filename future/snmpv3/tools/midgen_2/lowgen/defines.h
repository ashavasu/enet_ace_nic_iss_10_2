/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: defines.h,v 1.5 2015/04/28 14:43:31 siva Exp $
 *
 * Description:This File contains the Constant Definitions   
 *             needed for the Middle Level Code Generator. 
 *******************************************************************/
      
/* General Definitions.   */

#define FALSE         0
#define TRUE          1

#define SCANNER_SUCCESS     1
#define SCANNER_FAILURE     0

#define FAILURE       0

#define NEGATIVE      -1        
#define ZERO          0
#define ONE           1
#define TWO           2
#define THREE         3         
#define FOUR          4          
#define FIVE          5           
#define SIX           6          
#define SEVEN         7          
#define FILE_NAME_LEN 12      
#define MKDIR_FLAG    16877         



/*  File Opening Modes.  */

#define READ_MODE     "r"
#define APPEND_MODE   "a"
#define WRITE_MODE    "w"

/*  Index Flags.  */
     
#define IS_INDEX      1
#define NOT_INDEX     0 

/* Display Flags.  */
     
#define DISPLAY_ON    1
#define DISPLAY_OFF   0

/* Definition of The Scalar Table.  */

#define SCALAR        7   

/* Definition of The Macro.  */

#ifdef __STDC__
#define PROTO(x)   x 
#else
#define PROTO(x)  ()
#endif /* __STDC__ */

#define COMPARE_OBJECT_WITH_INDEX(i4_count , i4_counter) strcmp(p_table_struct->object[i4_count].object_name ,\
                                                   p_table_struct->index_name[i4_counter])

/* Maximum Value Definitions.  */

#define MAX_LINE      1024 
#define MAX_INDICES   25
#define MAX_OBJECTS   1024
#define MAX_TABLES    100
#define NEW_LINE      10
#define MAX_NEW_DATA  20


#define MAXCMDARGS    12
#define MINCMDARGS    2
#define MAXINFILE     25

/* For Ver 2 mbdb.h */

#define MAX_NO_OF_GROUPS    250

#define INCS          "# include "
#define DEFS          "# define "
#define SETENV        "setenv "

/* File Extension Definitions.  */

#define DOT           "****************************************************************************" 
#define MID_FILE_EXTN_C     "mid.c_temp"     
#define MID_FILE_FINAL_C     "mid.c"     
#define MID_FILE_EXTN_H     "mid.h"     
#define LOW_FILE_EXTN_C     "lw.c"  
#define LOW_FILE_EXTN_H     "lw.h"  
#define OCON_EXTN_H   "con.h"            
#define OGP_EXTN_H    "ogi.h"          
#define MBDB_EXTN_H   "mdb.h"          
#define MAKE_FILE_EXTN      "_mk.unx"    

/* Characters and String Definitions.  */

#define COMMENT       "--"
#define PERCENT       "%%"
#define MINUS         "-"
#define END_OF_BLOCK  "::="
#define COMMENT_END_PATTERN "*/"
#define NEWLINE_STRING      "\n"
#define NEWLINE_CHAR  '\n'
#define TAB           '\t'  
#define TAB_STR       "\t"  
#define NULL_STR      "\0"         
#define NULL_CHAR     '\0'         
#define SQR_OPEN_BRACKET    "["   
#define OPEN_PARAS    "{"         
#define CLOSE_PARAS   "}"        
#define COMMA         ","            
#define COMMA_CHAR    ','            
#define QOTES         "\""            
#define QOTES_CHAR    '\"'            
#define OPEN_BRACKET  "("   
#define CLOSE_BRACKET ")"
#define OPEN_BRAC_CHAR      '('
#define DOT_STR       "." 
#define DOT_CHAR      '.' 
#define SPACE         ' '
#define SPACE_STR     " "
#define ASTRIK        "*"
#define SEMI_COLON    ";" 
#define SLASH         "\\"
#define BACK_DIR      "../"

/* Definition For the Access for the OBJECTS.  */

#define READ_ONLY            1
#define READ_WRITE           2 
#define NO_ACCESS            3
#define ACC_FOR_NOTIFY       4
#define READ_CREATE          5
     
/* Definition For the Data Types of the OBJECTS.  */

#define INTEGER       1          
#define POS_INTEGER   2          
#define OCTET_STRING  3        
#define ADDRESS       4 
#define OBJECT_IDENTIFIER   5    
#define MacAddress    6          
#define EXTENSIBLE    7

/*  Definition for OID_TABLE Version II.  */

#define NO_COMMON_GROUP    -1

/*  Definition for Get Exact , Get First and Next Flags.  */

# define GET_EXACT_FLAG      1   
# define GET_FIRST_FLAG      2   
# define GET_NEXT_FLAG 3   

/*  Definition of String Constants.  */

#define STR_SEQ_OF             "SEQUENCE OF" 
#define STR_SEQ                "SEQUENCE"  
#define STR_SCALAR_TABLE_BEGIN "SCALAR_TABLE_BEGIN"
#define STR_SCALAR_TABLE_END   "SCALAR_TABLE_END"
#define STR_INDEX              "INDEX" 
#define STR_OBJ_TYPE           "OBJECT-TYPE"
#define STR_IMPORTS_BEGIN      "IMPORTS BEGIN"
#define STR_IMPORTS_END        "IMPORTS END"
#define STR_INTEGER_TYPE       "INTEGER"
#define STR_INTEGER32_TYPE     "Integer32"
#define STR_UNSIGNED32_TYPE    "Unsigned32"
#define STR_OCTET_STR_TYPE     "OCTET STRING"
#define STR_OID_TYPE           "OBJECT IDENTIFIER"
#define STR_COUNTER32_TYPE     "Counter32"
#define STR_COUNTER64_TYPE     "Counter64"
#define STR_TIMETICKS_TYPE     "TimeTicks"
#define STR_GAUGE32_TYPE         "Gauge32"
#define STR_NETWORKADDR_TYPE   "NetworkAddress"
#define STR_IPADDR_TYPE        "IpAddress"
#define STR_MACADDR_TYPE       "MacAddress"
#define STR_OCTET              "OCTET"
#define STR_OBJECT             "OBJECT"
#define STR_CONST_SIZE         "SIZE"
#define STR_UNSIG_INT_TYPE     "UINT4"
#define STR_INT_TYPE           "INT4"
#define STR_SHORT_UNSIG_INT_TYPE     "UINT2"
#define STR_SHORT_INT_TYPE     "INT2"
#define STR_UINT1              "UINT1"
#define STR_TYPEDEF            "typedef"

/*  File Names.  */

#define NEW_SYNTAX_FILE_NAME   "apsyntax.def"

/*  Shell Command Flag.  */

#define REMOVE                 "rm "   
#define TEMP_FILE              "temp?" 
#define REMOVE_DIR             "rm -rf "   

#define V2COMP                 "./v2comp "

#define SMIDEF                 "smi.def"
#define DOTDEF                 ".def"
#define DOTH                   ".h"

/*
 *  Defining Constant to Remove The Xmosy 
 *  Redirected Temp Output File.
 */

#define RM_PREMOSY_REDIRECT      "rm xm"

/* 
 *  Defining the Const for Redirecting 
 *  the output of the Xmosy Output.
 */
#define REDIRECT_OUTPUT        " >& xm"

/*  Defining the Types for INT and CHAR.  */

#define INT                    "d"
#define POS_INT                "u"
#define STRING                 "s" 

/* Defining the Constants for the Type of Routine.  */

#define GETFIRST_INDEX      0
#define INDEX         1
#define GET           3
#define TEST          4
#define SET           5

/* Defining the Constants for Command Line Processing.  */

#define DIRECTORY_OPTION        'd'
#define INDIVID_OPTION          'i'

#define AGENT_OPTION            's'
#define TABLE_OPTION            't'

#define GOID_OPTION             'g'

#define LOCK_OPTION             'l'
#define CONTEXTID_OPTION        'c' 
#define FILENAME_OPTION         'f'
#define MULTIPLE_INSTANCE       'm'
#define INCREMENTAL_SAVE        'x'
#define LOCK_FUNC               'L'
#define UNLOCK_FUNC             'U'
#define EXTERNAL_DECLARATION    'e'
#define MAPPING_LOCK_UNLOCK     'X'
#define TABLE_OPTION            't'

/* Defining  a MAC String Length */

#define MAC_STR_LENGTH 6

/* Defining the Option String.  */

/* 
 * option string modified to accommadate two new options -s and -t
 * modified by soma on 12/03/98
 */
/* 
 * option string modified to accommadate new option -g
 * modified by soma on 11/04/98
 */
/* 
 * option string modified to accommadate new options -c and -m
 * -c for passing call back functions to get context ID
 * -m for getting the MI string incase the mib is for Multiple Instance
 */
#define OPTION_STRING          "f:d:lcxteXU:L:d:--m:"

/* Defining the Constants for the Oid Table Version II.  */ 

#define MATCH_FOUND           1            
#define NO_MATCH_FOUND        0            

/* Defining the Status of the Elements.  */ 

#define GROUPED               -200 

/* Defining the Subgrp Flag Constants.  */

#define SUBGRP_PRESENT        1            
#define SUBGRP_NOT_PRESENT    2            

/*
 *  Defining the constant which are used to check for 
 *  the Presence of the grp element in the Index array.
 */
#define ELEMENT_FOUND  1
#define NO_ELEMENT_FOUND     2

/* Defining the Constant used in finding the Group Oid Length.  */

#define GROUP_OID_LEN_FOUND   1
#define GROUP_OID_LEN_NOT_FOUND     0

/*
 *  Defining the Constants which are used to specify the 
 *  Priority & Time Out in the SNMP-MBDB.H file for Ver 2.
 */
#define MBDB_VER2_PRIORITY   1
#define MBDB_VER2_TIMEOUT    10 

/* Defining constant for processing SNMP_ERR_WRONG_TYPE*/

#define INTEGER32   1
#define OCTETSTRING 2
#define OBJECTIDENTIFIER  3
#define COUNTER32   4
#define COUNTER64   5
#define TIMETICKS   6
#define GAUGE32     7
#define IPADDRESS   8
#define MACADDRESS  9
#define UNSIGNED32  10

/* Definition For the Status for the OBJECTS.  */

#define STATUS_CURRENT      1
#define STATUS_DEPRECATED   2 
#define STATUS_OBSOLETE     3
