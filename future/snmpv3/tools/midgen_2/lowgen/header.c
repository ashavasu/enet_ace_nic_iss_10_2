/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: header.c,v 1.4 2015/04/28 14:43:31 siva Exp $
 *
 * Description: This File Contains the function Which     
 *              generates the header files _ocon.h  and
 *              _ogp_ind.h.
 *******************************************************************/

# include "include.h"
# include "extern.h"

#ifdef CORRECT_MIDGEN
extern INT4         i4_check_repeat_table;
extern INT4         p_temp_counter;
#endif

/******************************************************************************
*      function Name        : header_ocon                                     *
*      Role of the function : This fn Generates the Const Declarations for    *
*                             each Table or Scalar Grp for each Object in them*
*      Formal Parameters    : fp                                              *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
header_ocon (FILE * fp)
{
    INT4                i4_count;
    UINT1               i1_temp[MAX_LINE];
    UINT1              *pi1_string;
    UINT1               u1Pad, u1;
    fprintf (fp, "\n/*");
    fprintf (fp, "\n *  The Constant Declarations for");
    fprintf (fp, "\n *  %s\n */\n\n", p_table_struct->table_name);

    /*  FOR LOOP for declaring the constants for the OBJECTS in a given table. */

    for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects; i4_count++)
    {
        strcpy (i1_temp, p_table_struct->object[i4_count].object_name);
        pi1_string = strupper (i1_temp);

        /* u1Pad added
         * for Right Alignment of Header Files generated 
         * by Midgen Tool
         */
        u1Pad = (50 - strlen (pi1_string));
        fprintf (fp, DEFS);
        fprintf (fp, "%s", pi1_string);
        for (u1 = 0; u1 < u1Pad; u1++)
            fprintf (fp, "%c", 32);
        fprintf (fp, "(%d)\n", p_table_struct->object[i4_count].position);
    }

}                                /*  End of Function . */

/******************************************************************************
*      function Name        : header_ogp_index                                *
*      Role of the function : This fn Generates the const declarations for    *
*                             the _OGP_IND.H for each Table or Scalar Grp.    *
*      Formal Parameters    : fp                                              *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
header_ogp_index (FILE * fp)
{
    static INT4         i4_ogp_index_count = ZERO;
    UINT1               i1_temp[MAX_LINE];
    UINT1              *pi1_string;
    UINT1               u1Pad, u1;

    strcpy (i1_temp, p_table_struct->table_name);
    pi1_string = strupper (i1_temp);

#ifdef CORRECT_MIDGEN
    if (i4_check_repeat_table)
    {
        strcat (pi1_string, itoa (p_temp_counter));
    }
#endif

    /* u1Pad added
     * for Right Alignment of Header Files generated 
     * by Midgen Tool
     */

    u1Pad = (45 - strlen (pi1_string));
    fprintf (fp, DEFS);
    fprintf (fp, "SNMP_OGP_INDEX_%s", pi1_string);
    for (u1 = 0; u1 < u1Pad; u1++)
        fprintf (fp, "%c", 32);
    fprintf (fp, "(%d)\n", i4_ogp_index_count++);

}                                /*  End of Function . */

/******************************************************************************
*      function Name        : header_constant_declare                         *
*      Role of the function : This fn Generates the constants required for    *
*                             the _Mid level Routines in the OGP header file. *
*      Formal Parameters    : fp, i4_ver_no                                   *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/
void
header_constant_declare (FILE * fp)
{
    fprintf (fp, NEWLINE_STRING);
    fprintf (fp, NEWLINE_STRING);
    fprintf (fp, DEFS);

    /*
     * values of SNMP_SUCCESS are different for version 1 and version 2
     * earlier tool was generating the same value for both the versions
     * modified by soma on 13/03/98
     */
    fprintf (fp, "      SNMP_SUCCESS    1\n");

    fprintf (fp, DEFS);

    /*
     * values of SNMP_FAILURE are different for version 1 and version 2
     * earlier tool was generating the same value for both the versions
     * modified by soma on 13/03/98
     */
    fprintf (fp, "      SNMP_FAILURE    0\n");

    fprintf (fp, DEFS);
    fprintf (fp, "      NULL_STRING     \"\\0\" \n");
    fprintf (fp, DEFS);
    fprintf (fp, "      MAX_OCTETLEN     255 \n");
    fprintf (fp, DEFS);
    fprintf (fp, "      MAX_OID_LEN     255 \n");
    fprintf (fp, DEFS);
    fprintf (fp, "      MAC_ADDR_LEN     6 \n");
    fprintf (fp, DEFS);
    fprintf (fp, "      ADDR_LEN         4 \n");
    fprintf (fp, DEFS);
    fprintf (fp, "      INTEGER_LEN      1 \n");
    fprintf (fp, DEFS);
    fprintf (fp, "      ZERO      0 \n");
    fprintf (fp, "\n/*");
    fprintf (fp, "\n *  This Constant is the Length of the Length of the");
    fprintf (fp, "\n *  Variable Length Index (Octet Strings of Var Length");
    fprintf (fp, "\n *  of OIDs which are Given in the Get Request as shown");
    fprintf (fp,
             "\n *  here get_operation <Object Name>.<Index Len>.<Actual Index>");
    fprintf (fp, "\n *  so this Variable is Declared.");
    fprintf (fp, "\n */\n");
    fprintf (fp, DEFS);
    fprintf (fp, "      LEN_OF_VARIABLE_LEN_INDEX       1 \n");

}                                /* End Of Function. */
