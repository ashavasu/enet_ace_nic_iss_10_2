/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: global.h,v 1.5 2015/04/28 14:43:31 siva Exp $
 *
 * Description:This File contains the Global Variables       
 *             Definitions.       
 *******************************************************************/
	   

INT4 i4_global_count = FALSE;

/*  This Variable stores whether the Table is scalar Table or not.  */
INT4 i4_global_scalar_flag  ;

/* Flag For Dispaly Option.  */
INT4 i4_global_lineno_flag  ;

/*Flag for finding whether Mac Address Object is present or not*/
INT1 i4_global_mac_flag = 0;

INT4 argc;
UINT1 *argv[100];

/* Declaration of the Global Variable which stores the File Name.  */   
INT1 i1_file_name[MAX_LINE] ;
INT1 FILENAME = 0;
/* Declaration of the Global Variable which stores the Directory Name.  */   
INT1 i1_dir_name[MAX_LINE] ;
INT1  i1_src_path[MAX_LINE];
INT1  i1_inc_path[MAX_LINE];

/*Declaratin of final mid.c file*/
INT1 i1_mid_final_file[MAX_LINE]; 
INT1 i1_mid_temp_file[MAX_LINE]; 
 
/* Declarations related to EXTENSIBLE TYPES.  */
INT4 i4_global_newcount = FALSE;
UINT1 name_new_datatype[MAX_NEW_DATA][MAX_LINE];

FILE*          f       ;    /*   File Pointer for Input MIB file     */

FILE*          fp      ;    /*   File Pointer for Output 'C' File    */
FILE*          f_mid_temp ;    /*   File Pointer for File MID TEMP   */
FILE*          f_mid_final ;    /*   File Pointer for  File MID C   */
FILE*          f_mid_h ;    /*   File Pointer for Header File MID    */

FILE*          f_low_c ;    /*   File Pointer for Output File LOW    */
FILE*          f_low_h ;    /*   File Pointer for Header File LOW    */

FILE*          f_ocon  ;    /*   File Pointer for CON.H              */
FILE*          f_ogp   ;    /*   File Pointer for OGP_IND.H          */
FILE*          f_mbdb  ;    /*   File Pointer for MBDB.H             */

FILE*          f_types ;    /*   File Pointer for New Data Types     */

t_MIB_TABLE        *p_table_struct                  ;
t_MIB_TABLE        *p_scalar_struct		    ;
t_TYPE_TABLE        type_table[MAX_NEW_DATA]        ;
/* The following flag is set in case the mib is for Multiple instance */
INT4     i4_mi_flag                        ;
/* Flag to generate new set of nmhSet routines in case of incremental save feature */
INT4     i4_isave_flag                        ;
INT4     i4_extern_flag = FALSE;
INT4     i4_incr_lock_flag = FALSE;
INT4     i4_lock_flag                         ;
INT4     i4_unlock_flag                       ;
INT1     ai1_lock_string[MAX_LINE]           ;
INT1     ai1_unlock_string[MAX_LINE]         ;
/* The following functions have been added to support Multiple Instance */
INT1     ai1_get_context[MAX_LINE]         ;
INT1     ai1_release_context[MAX_LINE]     ;
INT1     ai1_mi_string[MAX_LINE]           ;
