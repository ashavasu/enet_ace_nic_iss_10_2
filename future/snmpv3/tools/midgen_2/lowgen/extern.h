/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: extern.h,v 1.7 2015/04/28 14:43:31 siva Exp $
 *
 * Description:This File contains the Extern Declarations    
 *             needed for the Middle Level Code Generator. 
 *******************************************************************/
		
extern INT1              gi1LockEnable;
extern INT1              gi1ContextIdEnable;
extern INT1              gi1TableEnable;
extern INT4              i4_global_count                    ;

extern INT4              i4_global_scalar_flag              ;  
extern INT4              i4_global_lineno_flag              ;  
extern INT4              i4_global_mac_flag                 ;  

extern INT4              i4_global_newcount                 ;
extern UINT1             name_new_datatype[MAX_NEW_DATA][MAX_LINE];                 
extern INT1             i1_dir_name[MAX_LINE]              ;
extern INT4             argc                               ;
extern UINT1*           argv []                            ;

extern FILE*            f                                  ;
extern FILE*            fp                                 ;
extern FILE*            f_mid_h                            ;
extern FILE*            f_low_c                            ;
extern FILE*            f_low_h                            ;
extern FILE*            f_ocon                             ;
extern FILE*            f_ogp                              ;
extern FILE*            f_mbdb                             ;
extern t_MIB_TABLE*     p_table_struct                     ;
extern t_MIB_TABLE*     p_scalar_struct                     ;
extern t_TYPE_TABLE     type_table[MAX_NEW_DATA]           ;
extern tIMPINDICES_LIST *pImpIndx;
extern INT1 i1_module_name[MAX_TABLES];  /*Holds the name of the module identity*/

extern INT4 
parser_module PROTO ((t_MIB_TABLE * , FILE *));

extern void
chek_presenceof_index PROTO ((void));

extern INT4
find_object_is_index PROTO ((UINT1 *));

extern INT4
find_index_type_OIDorOctet PROTO ((INT4));

extern void
generator_module PROTO((void));

extern void 
code_generator_mid_module PROTO ((FILE *));
 
extern void 
prototype_generator_mid_module PROTO ((FILE *));

extern void 
mem_allocate PROTO ((void));

extern void
validate_command_line PROTO(());

extern INT4
process_command_line PROTO((INT4 , INT1 * [] , INT4 *, INT1 *, INT2 *));

extern void
check_invalid_option PROTO((INT4 , INT4 , INT4));

extern INT4
find_version PROTO((INT4 , UINT1 * []));

extern INT4 
find_dirname PROTO((INT4  , UINT1 * []));

extern INT4
make_dir PROTO((UINT1 *));

extern void
create_directory_structure PROTO((UINT1 * ));

extern UINT1*
get_pathname PROTO((void));

extern UINT1* 
get_line PROTO ((FILE *));

extern INT4 
check_empty_line PROTO ((UINT1 *));

extern UINT1* 
remove_comment PROTO ((UINT1 *));

extern INT4  
go_down PROTO ((FILE *));

extern UINT1*
get_table_name PROTO ((FILE *));

extern INT4 
get_index PROTO ((FILE *));

extern INT4      
separate_indices PROTO ((UINT1 *));

extern UINT1*      
strupper PROTO ((UINT1 *));

extern INT4     
get_no_of_objects PROTO ((FILE *));

extern INT4     
get_objects PROTO ((FILE *));

extern void     
get_import_index PROTO ((FILE *));

extern void     
get_position PROTO ((FILE *));

extern void     
store_object_type PROTO ((UINT1* , INT4));

extern void 
get_entry_name PROTO ((FILE *));

extern void 
initialize_type_table PROTO ((UINT1 *));

extern void  
get_components_of_struct PROTO ((FILE *));

extern void  
find_datatype_of_element PROTO ((UINT1 * , INT4));

extern UINT1 * 
search_dtype PROTO ((UINT1 * , FILE *));

extern void 
comments_for_getroutine PROTO ((UINT1 *,FILE *));

extern void
prototype_for_getroutine PROTO ((UINT1 *,FILE *));

extern void
declaration_of_returnvalue PROTO ((FILE *));

extern void
declaration_of_value PROTO ((FILE *));

extern void
declaration_of_common_variables PROTO ((FILE *));

extern void
declaration_of_formvarbind_args PROTO ((FILE *));

extern void
declaration_for_getroutine PROTO ((FILE *));

extern void
declaration_scalar_table_getroutine PROTO ((FILE *));

extern void
declare_all_var_to_extractindices PROTO ((FILE *));

extern void
find_len_of_indices PROTO ((FILE *));

extern void 
get_exact_routine PROTO ((FILE *));

extern void 
get_next_routine PROTO ((FILE *));

extern void
alloc_mem_for_oid_getexact_first PROTO ((FILE * , INT4));

extern void 
fn_checking_for_malloc_failure_oid PROTO ((FILE *));

extern void 
fn_checking_for_malloc_failure_oid_settest PROTO ((FILE *));

extern void 
alloc_mem_for_oid_getnext PROTO ((FILE *, INT4));

extern void 
fn_checking_for_malloc_failure_getnext_oid PROTO ((FILE * , INT4 , INT4));

extern void
allocmem_oid_for_partial_index PROTO((FILE * , INT4));

extern void
free_all_oids PROTO ((FILE * , INT4));

extern void
free_previous_oid_indices PROTO ((FILE *, INT4));

extern void 
alloc_mem_for_octetstring_getexact_first PROTO ((FILE * , INT4));

extern void 
fn_checking_for_malloc_failure PROTO ((FILE *));

extern void 
fn_checking_for_malloc_failure_gettest PROTO ((FILE *));

extern void 
alloc_mem_for_octetstring_getnext PROTO ((FILE *, INT4));

extern void 
fn_checking_for_malloc_failure_getnext PROTO ((FILE * , INT4 , INT4));

extern void
allocmem_for_octetstring_retvalue PROTO((FILE * , INT4));

extern void
free_octetstring_retvalue PROTO((FILE * , INT4));

extern void
allocmem_for_oid_retvalue PROTO((FILE * , INT4));

extern void
allocmem_octetstring_for_partial_index PROTO((FILE * , INT4));

extern void
free_all_octetstrings PROTO((FILE * , INT4));

extern void
free_previous_octet_indices PROTO((FILE * , INT4));

extern void 
code_for_extracting_partial_indices PROTO((FILE *));

extern void
add_len_of_index PROTO((INT4));

extern void
add_len_of_index_new_datatype PROTO((INT4 , INT4));

extern void 
assign_len_of_variablelen_index PROTO((FILE *));

extern void 
assign_len_of_variablelen_index_new_datatype PROTO((INT4 , INT4));

extern void 
find_len_of_varlen_index_new_datatype PROTO((INT4 , INT4));

extern void 
extract_one_index PROTO((FILE * , INT4));

extern void 
get_routine PROTO ((FILE *));

extern void 
passing_args_to_formvarbind_for_index_new_datatype PROTO((INT4 , INT4 , INT4));

extern void 
passing_indices_to_lowlevel_routine PROTO ((FILE *));

extern void 
passing_indices_to_lowlevel_routine_getfirst PROTO ((FILE *));

extern void 
comments_for_setroutine PROTO ((UINT1 *,FILE *));

extern void
prototype_for_setroutine PROTO ((UINT1 * , FILE *));

extern void 
declarations_for_setroutine PROTO ((FILE *));

extern void 
declaration_scalar_table_setroutine PROTO ((FILE *));

extern void 
set_for_allobjects PROTO ((FILE *));

extern void 
extract_value PROTO ((FILE * , INT4));

extern void 
extract_valfrom_multi_for_new_datatype PROTO ((INT4 , INT4));

extern void 
pass_value_to_the_lowlevel_routine PROTO ((FILE * , INT4));

extern void 
set_routine PROTO ((FILE *));

extern void 
comments_for_testroutine PROTO ((UINT1 *,FILE *));

extern void 
prototype_for_testroutine PROTO ((UINT1 * , FILE *));

extern void 
declarations_for_testroutine PROTO ((FILE *));

extern void 
declaration_scalar_testroutine PROTO ((FILE *));

extern void 
test_for_allobjects PROTO ((FILE *));

extern void 
test_routine PROTO ((FILE *));

extern INT4
check_presenceof_index PROTO ((void));
 
extern void 
get_scalar_exact_routine PROTO ((FILE *));

extern void 
declarations_for_getroutine PROTO ((FILE *));

extern void 
code_for_extracting_indices PROTO ((FILE *));

extern void
proto_midlevel_get_routine PROTO((FILE *));

extern void
proto_midlevel_set_routine PROTO((FILE *));

extern void
proto_midlevel_test_routine PROTO((FILE *));

extern void
passing_typeto_lowlev_skeletolcode PROTO((FILE * , INT4));

extern void
passing_typeto_lowlev_skeletolcode_getfirst PROTO((FILE * , INT4));

extern void
passing_prefix_according_totype PROTO((FILE * , INT4));

extern void
passing_prefix_according_totype_getfirst PROTO((FILE * , INT4));

extern void
defining_varsfor_lowlevel PROTO((FILE *));

extern void
defining_varsfor_lowlevel_getfirst PROTO((FILE *));

extern void
trace_logs_for_index PROTO((FILE *));

extern void
trace_logs_for_index_getfirst PROTO((FILE *));

extern void
trace_logs_for_object PROTO((FILE * , INT4 , INT4));

extern void
trace_logs_exit PROTO((FILE *));

extern void
passing_varsto_lowlevel PROTO((FILE *));

extern void
passing_varsto_lowlevel_getfirst PROTO((FILE *));

extern void
low_level_code_generator PROTO((FILE *));

extern void 
function_for_ifdef PROTO((FILE *));

extern void
low_code_getfirst_next PROTO((FILE *));

extern void
comment_for_lowlevel_validate_index PROTO((FILE *));

extern void
comment_for_lowlevel_getfirst PROTO((FILE *));

extern void
comment_for_lowlevel_getnext PROTO((FILE *));

extern void
low_code_get_all_objects PROTO((FILE *));

extern void
comment_for_lowlevel_get PROTO((FILE * , INT4));

extern void
low_code_set_all_objects PROTO((FILE *));

extern void
comment_for_lowlevel_set PROTO((FILE * , INT4));

extern void
low_code_test_all_objects PROTO((FILE *));

extern void
comment_for_lowlevel_test PROTO((FILE * , INT4));

extern void
comment_for_lowlevel_dep PROTO((FILE * , INT4, INT4));

extern void
passing_index_to_lowlevelcode PROTO((FILE *));

extern void
low_level_proto_generator PROTO((FILE *));

extern void
low_proto_getfirst_next PROTO((FILE *));

extern void
low_proto_get_all_objects PROTO((FILE *));

extern void
low_proto_set_all_objects PROTO((FILE *));

extern void
low_proto_test_all_objects PROTO((FILE *));

extern void
passing_indextype_to_lowlevelproto PROTO((FILE *));

extern void
passing_indextype_to_lowlevelproto_getfirst PROTO((FILE *));

extern INT4
search_new_data_type PROTO((UINT1 *)); 

extern INT4 
find_already_datatype_present PROTO((UINT1 *));

extern void 
declarations_for_new_datatype PROTO((INT4 , INT4));

extern void
declare_retval_for_new_datatype PROTO((INT4 , INT4));

extern void 
extracting_indices_for_new_datatype PROTO((INT4 , INT4));

extern void 
convert_chararrayto_struct PROTO((INT4 , INT4));

extern void 
convert_from_struct_to_string PROTO((INT4 , INT4));

extern void 
convert_from_struct_to_string_getnext PROTO((INT4 , INT4));

extern void 
storeback_OID_for_new_datatype PROTO((INT4 , INT4));

extern void 
storeback_OID_for_new_datatype_exact PROTO((INT4 , INT4));

extern void
passing_retval_for_new_datatype PROTO((INT4 ,INT4)); 

extern void 
generate_code_for_new_datatype PROTO((INT4));

extern void
passargs_to_lowlev_for_new_datatype PROTO((INT4 , INT4)); 

extern void
passargs_to_lowlev_for_new_datatype_getfirst PROTO((INT4 , INT4)); 

extern void
passargs_to_lowlev_getnext_new_datatype PROTO((INT4 , INT4)); 

extern void
header_ocon PROTO((FILE *));

extern void
header_constant_declare PROTO((FILE *));

extern void
header_ogp_index PROTO((FILE *));

extern void
oid_table_v1 PROTO((UINT1 * , INT1 *));

extern void
oid_table_v2 PROTO((void));

extern void
oid_table_incs PROTO((FILE * , UINT1 *));

extern void
generate_oidarray_fortable PROTO((UINT1 * , INT1 *));

extern INT4
write_oidarray_tofile PROTO((FILE * , UINT1 *,INT1 *));

extern void
generate_OID_table PROTO((UINT1 * , INT4,INT1 *));

extern void
generate_MIBOBJ_table PROTO((UINT1 *,INT1 *));

extern UINT1*
search_table_name PROTO((FILE *));

extern void 
fnpointer_to_mid_fns PROTO((FILE * , INT4 ));

void 
create_mbdbh_file PROTO((UINT1* , UINT1 *));

extern void
create_makefile (UINT1 *pu1_pathname, UINT1 *pu1_file_name,
                 INT4 i4_tables_per_file,
                 INT1 **pi1_table_list, INT4 i4_table_len,
				 INT1 p1_in_files[][MAX_LINE], INT4 i4_file_count);

extern void
create_envfile PROTO((UINT1 * , UINT1 * ));

extern void
create_includeh PROTO((UINT1 * , UINT1 * ));

extern void
prototype_FormFncts PROTO((FILE *));

extern void
clear_directory PROTO((void));

extern void
premosy_postmosy (INT4 argc, INT1 argv[][MAX_LINE]);

/* Vers 2 */

extern INT4 
find_nof_comma PROTO ((UINT1 *));

extern INT4 
nof_grouped_oid PROTO ((INT4 []));

extern INT4
get_oid_fromtempfile PROTO ((UINT1 *));

extern INT4 
find_commonoid PROTO ((void));

extern INT4
find_bigoid_index PROTO ((INT4 , INT4 []));

extern INT4 
find_next_nongrouped_index PROTO ((INT4 []));

extern INT4
find_next_nongrouped_ingrp PROTO ((INT4 , INT4 []));

extern void
fill_with_groupno PROTO ((INT4 , INT4 []));

extern void
store_grpno PROTO ((INT4 , INT4 []));

extern INT4
find_before_oidlen PROTO ((UINT1 * , INT4));

extern INT4
find_matching_oid PROTO ((INT4 , INT4 , INT4 , INT4 []));

extern INT4 
form_groups PROTO ((INT4 , INT4 , INT4 []));

extern INT4
find_suboid_len PROTO ((INT4 [] , INT4, INT2));

extern INT4
find_nof_subgrps PROTO ((INT4 [] , INT4));

extern INT4
find_lenof_groupoid PROTO ((INT4 [] , INT4));

extern UINT1 * 
get_tablename_from_tempfile PROTO ((INT4 , UINT1 *));

extern void
fnptr_for_basoidtable PROTO ((FILE * , UINT1 * , INT4));

extern void 
write_oidarray (INT4 i4_indexarray[], UINT1 *pu1_pathname,
                UINT1 *pu1_filename, INT2 i2_goid_len, INT4 i4_argv_count,
				INT1 u1_parse_files[][MAX_LINE], INT1 i1_code_for_agent);

extern void
write_suboidarray PROTO ((INT4 [], UINT1 * ,UINT1 *, INT2));

extern void
write_grpoid_table PROTO ((INT4 [] , UINT1 * , UINT1 *, INT1, INT2));

extern void 
write_baseoid_table PROTO ((INT4 [] , UINT1 * , UINT1 *, INT1, INT2));

extern void 
write_mibobj_table PROTO ((UINT1 * , UINT1 *, INT1));

extern void 
create_mbdbh_file_ver2 (UINT1 *pu1_pathname, UINT1 *pu1_filename,
                        INT1 i1_code_for_agent, INT2 i2_goid_len,
						INT4 i4_argv_count, INT1 u1_parse_files[][MAX_LINE]);

extern void 
gencode_for_alloc_octet_fn PROTO ((FILE *));

extern void 
gencode_for_alloc_oid_fn PROTO ((FILE *));

extern void 
gencode_for_free_octet_fn PROTO ((FILE *));

extern void 
gencode_for_free_oid_fn PROTO ((FILE *));

extern void
prototype_alloc_free_fns PROTO((FILE *));

/*
 * following function are added to help split output into multiple files
 * and avoid subset problem while generating groups
 * added by soma on 06/03/98
 */

extern INT1 *
itoa PROTO ((INT4));

extern UINT1
check_for_subset PROTO ((INT4 *, INT4 *, INT4 []));

extern void
merge_groups PROTO ((INT4, INT4, INT4 []));

extern UINT1 *
get_group_oid PROTO ((INT4, INT4 []));

/*
 * following functions are added to provide the user an option to specify
 * the length of Group OID to be kept in Group OID table in mbdb.h file
 * added by soma on 14/04/98
 */

extern INT4
shortest_oid PROTO ((void));

extern void
form_groups_in_one_shot PROTO ((INT2, INT4 []));

extern INT4
get_group_oid_len PROTO ((INT2, INT4, INT4 []));

/* The following Global Variable are included to generate nmhroutines
 * in case the mib is for Multiplw Instance
 * included on 06/01/2007
 */
extern INT4             i4_mi_flag                         ;
extern INT4             i4_lock_flag                      ;
extern INT4             i4_unlock_flag                      ;
extern INT4             i4_isave_flag                         ;
extern INT1             ai1_get_context[MAX_LINE]          ;
extern INT1             ai1_release_context[MAX_LINE]      ;
extern INT1             ai1_mi_string[MAX_LINE]            ;
extern INT1             ai1_lock_string[MAX_LINE]            ;
extern INT1             ai1_unlock_string[MAX_LINE]            ;
extern INT4             i4_extern_flag;
extern INT4             i4_incr_lock_flag;
