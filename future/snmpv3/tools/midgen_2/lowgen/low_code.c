/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: low_code.c,v 1.6 2015/04/28 14:43:31 siva Exp $
 *
 * Description: This File Contains the function Which     
 *              calls all the routines which generate
 *              the low level routines.  
 *******************************************************************/

# include "include.h"
# include "extern.h"

/******************************************************************************
*      function Name        : low_level_code_generator                        *
*      Role of the function : This fn calls all the fns needed for the Low    *
*                             Level skeletol Code Generation.                 *
*      Formal Parameters    : f                                               *
*      Global Variables     : i4_global_scalar_flag , p_table_struct.         *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
low_level_code_generator (FILE * f)
{
    INT4                i4_readonly_flag;

    if (!i4_global_scalar_flag)
    {
        low_code_getfirst_next (f);
        wrap_code_getfirst_next ();
    }
    low_code_get_all_objects (f);
    wrap_code_get_all_objects ();

    i4_readonly_flag = check_readonly ();
    if (i4_readonly_flag == TRUE)
    {
        low_code_set_all_objects (f);
        low_code_test_all_objects (f);
        low_code_dep_all_objects (f);
        wrap_code_set_all_objects ();
        wrap_code_test_all_objects ();
        wrap_code_dep_all_objects ();
        if (TRUE == i4_isave_flag)
            mid_code_set_all_objects ();
    }

}                                /*  End OF Function */

/******************************************************************************
*      function Name        : comment_for_lowlevel_getfirst                   *
*      Role of the function : This Fn Generates the Comments for the Low Level*
*                             skeletol code for GET FIRST Routines.           *
*      Formal Parameters    : f                                               *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
comment_for_lowlevel_validate_index (FILE * f)
{
    INT4                i4_count;

    fprintf (f, "\n/" DOT);
    fprintf (f, "\n Function    :  nmhValidateIndexInstance%s",
             p_table_struct->table_name);
    fprintf (f, "\n Input       :  The Indices");

    /*  FOR Loop for adding the Index Names to the Comments. */
    for (i4_count = ZERO; i4_count < p_table_struct->no_of_indices; i4_count++)
    {
        fprintf (f, "\n                %s",
                 p_table_struct->index_name[i4_count]);
    }

    fprintf (f, "\n Output      :  The Routines Validates the Given Indices.");
    fprintf (f, "\n Returns     :  SNMP_SUCCESS or SNMP_FAILURE");
    fprintf (f, "\n" DOT "/");

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : comment_for_lowlevel_getfirst                   *
*      Role of the function : This Fn Generates the Comments for the Low Level*
*                             skeletol code for GET FIRST Routines.           *
*      Formal Parameters    : f                                               *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
comment_for_lowlevel_getfirst (FILE * f)
{
    INT4                i4_count;

    fprintf (f, "\n/" DOT);
    fprintf (f, "\n Function    :  nmhGetFirstIndex%s",
             p_table_struct->table_name);
    fprintf (f, "\n Input       :  The Indices");

    /*  FOR Loop for adding the Index Names to the Comments. */
    for (i4_count = ZERO; i4_count < p_table_struct->no_of_indices; i4_count++)
    {
        fprintf (f, "\n                %s",
                 p_table_struct->index_name[i4_count]);
    }

    fprintf (f,
             "\n Output      :  The Get First Routines gets the Lexicographicaly");
    fprintf (f, "\n                First Entry from the Table.");
    fprintf (f, "\n Returns     :  SNMP_SUCCESS or SNMP_FAILURE");
    fprintf (f, "\n" DOT "/");

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : comment_for_lowlevel_getnext                    *
*      Role of the function : This Fn Generates the Comment for the Low Level *
*                             skeletol code for GET NEXT Routines.            *
*      Formal Parameters    : f                                               *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
comment_for_lowlevel_getnext (FILE * f)
{
    INT4                i4_count;

    fprintf (f, "\n/" DOT);
    fprintf (f, "\n Function    :  nmhGetNextIndex%s",
             p_table_struct->table_name);
    fprintf (f, "\n Input       :  The Indices");

    for (i4_count = ZERO; i4_count < p_table_struct->no_of_indices; i4_count++)
    {
        fprintf (f, "\n                %s",
                 p_table_struct->index_name[i4_count]);
        fprintf (f, "\n                next%s",
                 p_table_struct->index_name[i4_count]);
    }
    fprintf (f,
             "\n Output      :  The Get Next function gets the Next Index for");
    fprintf (f,
             "\n                the Index Value given in the Index Values. The");
    fprintf (f,
             "\n                Indices are stored in the next_varname variables.");
    fprintf (f, "\n Returns     :  SNMP_SUCCESS or SNMP_FAILURE");
    fprintf (f, "\n" DOT "/");

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : comment_for_lowlevel_get                        *
*      Role of the function : This Fn Generates the Comment for the Low Level *
*                             skeletol code for GET Routines.                 *
*      Formal Parameters    : f , i4_counter                                  *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
comment_for_lowlevel_get (FILE * f, INT4 i4_counter)
{
    INT4                i4_count;

    fprintf (f, "\n/" DOT);
    fprintf (f, "\n Function    :  nmhGet%s",
             p_table_struct->object[i4_counter].object_name);
    fprintf (f, "\n Input       :  The Indices");

    for (i4_count = ZERO; i4_count < p_table_struct->no_of_indices; i4_count++)
    {
        fprintf (f, "\n                %s",
                 p_table_struct->index_name[i4_count]);
    }

    fprintf (f, "\n\n                The Object ");
    fprintf (f, "\n                retVal%s",
             p_table_struct->object[i4_counter].object_name);
    fprintf (f, "\n Output      :  The Get Low Lev Routine Take the Indices &");
    fprintf (f,
             "\n                store the Value requested in the Return val.");
    fprintf (f, "\n Returns     :  SNMP_SUCCESS or SNMP_FAILURE");
    fprintf (f, "\n" DOT "/");

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : comment_for_lowlevel_set                        *
*      Role of the function : This Fn Generates the Comment for the Low Level *
*                             skeletol code for SET Routines.                 *
*      Formal Parameters    : f , i4_counter                                  *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
comment_for_lowlevel_set (FILE * f, INT4 i4_counter)
{
    INT4                i4_count;

    fprintf (f, "\n/" DOT);
    fprintf (f, "\n Function    :  nmhSet%s",
             p_table_struct->object[i4_counter].object_name);
    fprintf (f, "\n Input       :  The Indices");

    /*  FOR Loop for Adding the Index names in the Comments for the Set Fns.
     */
    for (i4_count = ZERO; i4_count < p_table_struct->no_of_indices; i4_count++)
    {
        fprintf (f, "\n                %s",
                 p_table_struct->index_name[i4_count]);
    }
    fprintf (f, "\n\n                The Object ");
    fprintf (f, "\n                setVal%s",
             p_table_struct->object[i4_counter].object_name);
    fprintf (f, "\n Output      :  The Set Low Lev Routine Take the Indices &");
    fprintf (f, "\n                Sets the Value accordingly.");
    fprintf (f, "\n Returns     :  SNMP_SUCCESS or SNMP_FAILURE");
    fprintf (f, "\n" DOT "/");

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : comment_for_lowlevel_test                       *
*      Role of the function : This Fn Generates the Comment for the Low Level *
*                             skeletol code for TEST Routines.                *
*      Formal Parameters    : f , i4_counter                                  *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
comment_for_lowlevel_test (FILE * f, INT4 i4_counter)
{
    INT4                i4_count;

    fprintf (f, "\n/" DOT);
    fprintf (f, "\n Function    :  nmhTestv2%s",
             p_table_struct->object[i4_counter].object_name);
    fprintf (f, "\n Input       :  The Indices");

    /*  FOR Loop for Adding the Index names in the Comments for the Test Fns.  */
    for (i4_count = ZERO; i4_count < p_table_struct->no_of_indices; i4_count++)
    {
        fprintf (f, "\n                %s",
                 p_table_struct->index_name[i4_count]);
    }

    fprintf (f, "\n\n                The Object ");
    fprintf (f, "\n                testVal%s",
             p_table_struct->object[i4_counter].object_name);
    fprintf (f,
             "\n Output      :  The Test Low Lev Routine Take the Indices &");
    fprintf (f,
             "\n                Test whether that Value is Valid Input for Set.");
    fprintf (f,
             "\n                Stores the value of error code in the Return val");
    fprintf (f,
             "\n Error Codes :  The following error codes are to be returned");
    fprintf (f,
             "\n                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)");
    fprintf (f,
             "\n                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)");
    fprintf (f,
             "\n                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)");
    fprintf (f,
             "\n                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)");
    fprintf (f,
             "\n                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)");
    fprintf (f, "\n Returns     :  SNMP_SUCCESS or SNMP_FAILURE");
    fprintf (f, "\n" DOT "/");

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : low_code_getfirst_next                          *
*      Role of the function : This Fn Generates the Skeletol Code for the     *
*                             Low Level GET_FIRST and GET_NEXT Routines & the *
*                             Validate Index Instance for the GET_EXACT fn    *
*      Formal Parameters    : f                                               *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
low_code_getfirst_next (FILE * f)
{
    INT4                i4_counter = ZERO;
    INT4                i4_count = ZERO;
    INT4                i4_cnt = ZERO;
    UINT1               au1FirstBuf[5];
    UINT1               au1NextBuf[5];
    UINT1               au1NextpBuf[5];
    memset (au1FirstBuf, 0, 5);
    memset (au1NextBuf, 0, 5);
    memset (au1NextpBuf, 0, 5);

    if (p_table_struct->status == STATUS_OBSOLETE)
        return;

    /*  GET_EXACT  Validate Index Instance Routine.  */
    fprintf (f, "\n/* LOW LEVEL Routines for Table : %s. */\n",
             p_table_struct->table_name);

    /* The Fn Generates Comments for the Low Level Validate Index Instance.  */
    comment_for_lowlevel_validate_index (f);

    fprintf (f, "\n/* GET_EXACT Validate Index Instance Routine. */\n");
    fprintf (f, "\nINT1 nmhValidateIndexInstance%s(",
             p_table_struct->table_name);

    /* This Fn passes all the Indices to the Low level routine. */
    passing_index_to_lowlevelcode (f);
    fprintf (f, ")");

    fprintf (f, "\n{");

#ifdef MIDGEN_DEBUG_WANTED

    fprintf (f, "\n\tprintf(\"Function Name : nmhValidateIndexInstance%s\"); ",
             p_table_struct->table_name);
    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {
        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }
            print_variable_value (f, INDEX, i4_count);
            /* Break When A Index is Found. */
            break;
        }                        /* End Of FOR Loop. */

    }                            /* End of Main FOR Loop For Each Index. */

#endif /* MIDGEN_DEBUG_WANTED */

    fprintf (f, "\n}");

    /*  GET_FIRST Routine.  */

    /* This Fn Generates the Code for the Low Level Get First Routines.  */
    comment_for_lowlevel_getfirst (f);

    fprintf (f, "\n/* GET_FIRST Routine. */\n");
    fprintf (f, "\nINT1 nmhGetFirstIndex%s(", p_table_struct->table_name);

    /*  FOR Loop for passing all the indices to the Low level Get first Fn. */
    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {
        if (i4_counter >= TRUE)
        {
            fprintf (f, " , ");
        }
        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            /*
             *  This Fn write to the File the Type of the
             *  Index to be passed to the Low Level Routine.
             */
            passing_typeto_lowlev_skeletolcode_getfirst (f, i4_count);
            passing_prefix_according_totype_getfirst (f, i4_count);
            for (i4_cnt = ZERO; i4_cnt < i4_counter; i4_cnt++)
            {

                if (strcmp
                    (p_table_struct->index_name[i4_counter],
                     p_table_struct->index_name[i4_cnt]) == 0)
                {
                    sprintf (au1FirstBuf, "%d", i4_counter);
                }
            }
            fprintf (f, "%s%s", p_table_struct->object[i4_count].object_name,
                     au1FirstBuf);

            /* Break When A Index is Found. */
            break;

        }                        /* End of For Loop */

    }                            /* End of FOR Loop for Each Index . */
    fprintf (f, ")");

    fprintf (f, "\n{");

#ifdef MIDGEN_DEBUG_WANTED

    fprintf (f, "\n\tprintf(\"Function Name : nmhGetFirstIndex%s\"); ",
             p_table_struct->table_name);

    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {
        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }
            print_variable_value (f, GETFIRST_INDEX, i4_count);

            /* Break When A Index is Found. */
            break;
        }                        /* End Of FOR Loop. */

    }                            /* End of Main FOR Loop For Each Index. */

#endif /* MIDGEN_DEBUG_WANTED */

    fprintf (f, "\n}");

    /*  GET_NEXT Routine.  */

    /* This Fn Generates the Code for the Low Level Get Next Rtns.  */
    comment_for_lowlevel_getnext (f);

    fprintf (f, "\n/* GET_NEXT Routine.  */");
    fprintf (f, "\nINT1 nmhGetNextIndex%s(", p_table_struct->table_name);

    /*
     *  For Loop for passing all the INDICES And the
     *  NEXT INDEX Value to the Low level Get Next Fn.
     */
    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {
        if (i4_counter >= TRUE)
        {
            fprintf (f, " , ");
        }
        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            /*
             *  This Fn writes to the File the TYPE of the INDEX
             *  (passed as Value) to be passed to the Low Level Routine.
             */
            passing_typeto_lowlev_skeletolcode (f, i4_count);
            passing_prefix_according_totype (f, i4_count);
            for (i4_cnt = ZERO; i4_cnt < i4_counter; i4_cnt++)
            {

                if (strcmp
                    (p_table_struct->index_name[i4_counter],
                     p_table_struct->index_name[i4_cnt]) == 0)
                {
                    sprintf (au1NextBuf, "%d", i4_counter);
                }
            }
            fprintf (f, "%s%s ,", p_table_struct->object[i4_count].object_name,
                     au1NextBuf);

            /*
             *  This Fn writes to the File the TYPE of the NEXT INDEX
             *  (passed as Pointer) to be Passed to the Low Level Routine.
             */
            passing_typeto_lowlev_skeletolcode_getfirst (f, i4_count);
            passing_prefix_according_totype_getfirst (f, i4_count);
            for (i4_cnt = ZERO; i4_cnt < i4_counter; i4_cnt++)
            {

                if (strcmp
                    (p_table_struct->index_name[i4_counter],
                     p_table_struct->index_name[i4_cnt]) == 0)
                {
                    sprintf (au1NextpBuf, "%d", i4_counter);
                }
            }
            fprintf (f, "Next%s%s ",
                     p_table_struct->object[i4_count].object_name, au1NextpBuf);

            /* Break When A Index is Found. */
            break;

        }                        /* End OF FOR Loop . */

    }                            /* End Of Main FOR LOOP For Each Index . */

    fprintf (f, CLOSE_BRACKET);

    fprintf (f, "\n{");

#ifdef MIDGEN_DEBUG_WANTED

    fprintf (f, "\n\tprintf(\"Function Name : nmhGetNextIndex%s\"); ",
             p_table_struct->table_name);

    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {
        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }
            print_variable_value (f, INDEX, i4_count);
            print_variable_value_obj (f, i4_count, GETFIRST_INDEX, 1);
            /* Break When A Index is Found. */
            break;
        }                        /* End Of FOR Loop. */

    }                            /* End of Main FOR Loop For Each Index. */

#endif /* MIDGEN_DEBUG_WANTED */

    fprintf (f, "\n}");

}                                /* End Of Function . */

/******************************************************************************
*      function Name        : low_code_get_all_objects                        *
*      Role of the function : This Fn Generates the Skeletol Code for the Low *
*                             Level GET Rtns for all the Objects in a Table.  *
*      Formal Parameters    : f                                               *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
low_code_get_all_objects (FILE * f)
{
    INT4                i4_count;
    INT4                i4_index_flag = NOT_INDEX;
    INT1                i1_first_obj_flag = 0;

#ifdef MIDGEN_DEBUG_WANTED
    INT4                i4_vcount, i4_vcounter;

#endif /* MIDGEN_DEBUG_WANTED */

    if (p_table_struct->status == STATUS_OBSOLETE)
        return;

    /*  Main For Loop for All the Objects.  */

    for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects; i4_count++)
    {
        if ((p_table_struct->object[i4_count].access == READ_ONLY)
            || (p_table_struct->object[i4_count].access == READ_CREATE)
            || (p_table_struct->object[i4_count].access == READ_WRITE))
        {
            /*  Finding Whether the Object is Index or Not. */
            i4_index_flag =
                find_object_is_index (p_table_struct->object[i4_count].
                                      object_name);

            /*
             *  If the Object is an Index then the Low Level
             *   Get Routines are not Needed.
             */
            if ((i4_index_flag == IS_INDEX)
                || (p_table_struct->object[i4_count].status == STATUS_OBSOLETE))
            {
                continue;
            }

            if (i1_first_obj_flag == 0)
            {
                i1_first_obj_flag = 1;
                fprintf (f, "\n/* Low Level GET Routine for All Objects  */\n");
            }

            /*  This Fn Generates the Comments For Low Level GET routines. */
            comment_for_lowlevel_get (f, i4_count);

            fprintf (f, "\nINT1 nmhGet%s(",
                     p_table_struct->object[i4_count].object_name);

            /* This Fn passes all the Indices to the Low level routine. */
            passing_index_to_lowlevelcode (f);

            /*  Passing the TYPE and Variable for The return Value. */
            if (i4_global_scalar_flag == ZERO)
            {
                fprintf (f, " , ");
            }

            passing_typeto_lowlev_skeletolcode_getfirst (f, i4_count);
            passing_prefix_according_totype_getfirst (f, i4_count);
            fprintf (f, "RetVal%s)",
                     p_table_struct->object[i4_count].object_name);

            fprintf (f, "\n{");

            if (i4_mi_flag == TRUE)
            {
                fprintf (f, "\n    INT4 i4Return = SNMP_FAILURE;\n");

                if (i4_global_scalar_flag == ZERO)
                {
                    fprintf (f, "\n    if(%s(", ai1_get_context);

                    /* 
                     * This function passes the Context Id 
                     * to the Get context function 
                     * */
                    passing_context_id (f);

                    fprintf (f, ") == SNMP_FAILURE)");
                    fprintf (f, "\n    {\n        return i4Return;\n    }\n");
                }
                fprintf (f, "\n    i4Return = nmhGet");

                /* 
                 * This function passes the SI nmh routine 
                 * to the corresponding MI routine
                 *  */
                pass_si_nmh_routine (f, i4_count);
                fprintf (f, "(");

                if (p_table_struct->no_of_indices > 1)
                {
                    passing_index_to_si_lowlevelcode (f);

                    /*  
                     *  Passing the TYPE and Variable
                     *   for The return Value. 
                     *   */
                    if (i4_global_scalar_flag == ZERO)
                    {
                        fprintf (f, " , ");
                    }
                }

                passing_prefix_according_totype_getfirst (f, i4_count);

                fprintf (f, "RetVal%s);",
                         p_table_struct->object[i4_count].object_name);
                if (i4_global_scalar_flag == ZERO)
                {
                    fprintf (f, "\n    %s();", ai1_release_context);
                }
                fprintf (f, "\n    return i4Return;");
            }

#ifdef MIDGEN_DEBUG_WANTED

            fprintf (f, "\n\tprintf(\"Function Name : nmhGet%s\");",
                     p_table_struct->object[i4_count].object_name);

            for (i4_vcounter = ZERO;
                 i4_vcounter < p_table_struct->no_of_indices; i4_vcounter++)
            {

                for (i4_vcount = ZERO;
                     i4_vcount <
                     p_table_struct->no_of_objects +
                     p_table_struct->no_of_imports; i4_vcount++)
                {

                    if (COMPARE_OBJECT_WITH_INDEX (i4_vcount, i4_vcounter) !=
                        FAILURE)
                    {
                        continue;
                    }
                    print_variable_value (f, INDEX, i4_vcount);

                    /* Break When A Index is Found. */
                    break;
                }                /* End of For Loop */

            }                    /* End of FOR Loop for Each Index . */

            print_variable_value_obj (f, i4_count, GET, 2);

#endif /* MIDGEN_DEBUG_WANTED */

            fprintf (f, "\n}");

        }                        /* End of IF condition for Checking ACCESS Permissions. */

    }                            /* End of Main For Loop for all Objects.  */

}                                /*  End of Function. */

/******************************************************************************
*      function Name        : low_code_set_all_objects                        *
*      Role of the function : This Fn Generates the Skeletol Code for the Low *
*                             Level SET Rtns for all the Objects in a Table.  *
*      Formal Parameters    : f                                               *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
low_code_set_all_objects (FILE * f)
{
    INT4                i4_count;
    INT1                i1_first_obj_flag = 0;

#ifdef MIDGEN_DEBUG_WANTED
    INT4                i4_vcount, i4_vcounter;
#endif

    if (p_table_struct->status == STATUS_OBSOLETE)
        return;

    /*  Main FOR Loop for All the Objects.  */

    for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects; i4_count++)
    {
        if (p_table_struct->object[i4_count].status == STATUS_OBSOLETE)
        {
            continue;
        }

        if (i1_first_obj_flag == 0)
        {
            i1_first_obj_flag = 1;
            fprintf (f, "\n/* Low Level SET Routine for All Objects  */\n");
        }

        if ((p_table_struct->object[i4_count].access == READ_WRITE)
            || (p_table_struct->object[i4_count].access == READ_CREATE))
        {
            /* This Fn Generates the Comments for low level SET routines.  */

            comment_for_lowlevel_set (f, i4_count);

            fprintf (f, "\nINT1 nmhSet%s(",
                     p_table_struct->object[i4_count].object_name);

            /* This Fn passes all the Indices to the Low level routine. */
            passing_index_to_lowlevelcode (f);

            /*  Passing the TYPE and Variable for The Set Value.  */
            if (i4_global_scalar_flag == ZERO)
            {
                fprintf (f, " , ");
            }

            passing_typeto_lowlev_skeletolcode (f, i4_count);
            passing_prefix_according_totype (f, i4_count);
            fprintf (f, "SetVal%s)",
                     p_table_struct->object[i4_count].object_name);

            fprintf (f, "\n{");

            if (i4_mi_flag == TRUE)
            {
                fprintf (f, "\n    INT4 i4Return = SNMP_FAILURE;\n");
                if (i4_global_scalar_flag == ZERO)
                {
                    fprintf (f, "\n    if(%s(", ai1_get_context);

                    /* 
                     * This function passes the Context Id 
                     * to the Get context function 
                     * */
                    passing_context_id (f);

                    fprintf (f, ") == SNMP_FAILURE)");
                    fprintf (f, "\n    {\n        return i4Return;\n    }\n");
                }
                fprintf (f, "\n    i4Return = nmhSet");

                /* 
                 * This function passes the SI nmh routine 
                 * to the corresponding MI routine 
                 * */
                pass_si_nmh_routine (f, i4_count);

                fprintf (f, "(");

                if (p_table_struct->no_of_indices > 1)
                {
                    passing_index_to_si_lowlevelcode (f);

                    /*  
                     *  Passing the TYPE and Variable fo
                     *  r The return Value. 
                     *  */
                    if (i4_global_scalar_flag == ZERO)
                    {
                        fprintf (f, " , ");
                    }
                }

                passing_prefix_according_totype (f, i4_count);

                fprintf (f, "SetVal%s);",
                         p_table_struct->object[i4_count].object_name);
                if (i4_global_scalar_flag == ZERO)
                {
                    fprintf (f, "\n    %s();", ai1_release_context);
                }
                fprintf (f, "\n    return i4Return;");
            }

#ifdef MIDGEN_DEBUG_WANTED

            fprintf (f, "\n\tprintf(\"Function Name : nmhSet%s\");",
                     p_table_struct->object[i4_count].object_name);

            for (i4_vcounter = ZERO;
                 i4_vcounter < p_table_struct->no_of_indices; i4_vcounter++)
            {

                for (i4_vcount = ZERO;
                     i4_vcount <
                     p_table_struct->no_of_objects +
                     p_table_struct->no_of_imports; i4_vcount++)
                {

                    if (COMPARE_OBJECT_WITH_INDEX (i4_vcount, i4_vcounter) !=
                        FAILURE)
                    {
                        continue;
                    }

                    print_variable_value (f, INDEX, i4_vcount);
                    /* Break When A Index is Found. */
                    break;
                }                /* End of For Loop */

            }                    /* End of FOR Loop for Each Index . */
            print_variable_value_obj (f, i4_count, SET, 0);

#endif /* MIDGEN_DEBUG_WANTED */

            fprintf (f, "\n}");

        }                        /*  End of IF Condition for checking ACESS permission.  */

    }                            /* End of Main For Loop. */

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : low_code_test_all_objects                       *
*      Role of the function : This Fn Generates the Skeletol Code for the Low *
*                             Level TEST Rtns for all the Objects in a Table. *
*      Formal Parameters    : f                                               *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
low_code_test_all_objects (FILE * f)
{
    INT4                i4_count;
    INT1                i1_first_obj_flag = 0;

#ifdef MIDGEN_DEBUG_WANTED
    INT4                i4_vcount, i4_vcounter;
#endif /* MIDGEN_DEBUG_WANTED */

    if (p_table_struct->status == STATUS_OBSOLETE)
        return;

    /*  Main For Loop for All the Objects.  */
    for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects; i4_count++)
    {
        if (p_table_struct->object[i4_count].status == STATUS_OBSOLETE)
        {
            continue;
        }

        if (i1_first_obj_flag == 0)
        {
            i1_first_obj_flag = 1;
            fprintf (f, "\n/* Low Level TEST Routines for All Objects  */\n");
        }

        if ((p_table_struct->object[i4_count].access == READ_WRITE)
            || (p_table_struct->object[i4_count].access == READ_CREATE))
        {
            /*  This Fn Generates Comments for Low Level TEST Routines   */
            comment_for_lowlevel_test (f, i4_count);

            fprintf (f, "\nINT1 nmhTestv2%s(",
                     p_table_struct->object[i4_count].object_name);
            /*
             *This varaiable *pu4ErrorCode is added to support check for errors
             *NO CREATION, WRONG VALUE, INCONSISTENT VALUE, WRONG LENGTH
             *and INCONSISTENT NAME. This variable should be returned from
             *the low level code.
             */
            fprintf (f, "UINT4 *pu4ErrorCode , ");
            /* This Fn passes all the Indices to the Low level routine. */
            passing_index_to_lowlevelcode (f);

            /*  Passing the TYPE and Variable for The return Value. */
            if (i4_global_scalar_flag == ZERO)
            {
                fprintf (f, " , ");
            }

            passing_typeto_lowlev_skeletolcode (f, i4_count);
            passing_prefix_according_totype (f, i4_count);
            fprintf (f, "TestVal%s)",
                     p_table_struct->object[i4_count].object_name);

            fprintf (f, "\n{");

            if (i4_mi_flag == TRUE)
            {
                fprintf (f, "\n    INT4 i4Return = SNMP_FAILURE;\n");
                if (i4_global_scalar_flag == ZERO)
                {
                    fprintf (f, "\n    if(%s(", ai1_get_context);

                    /* 
                     * This function passes the Context Id 
                     * to the Get context function 
                     * */
                    passing_context_id (f);

                    fprintf (f, ") == SNMP_FAILURE)");
                    fprintf (f,
                             "\n    {\n        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;");
                    fprintf (f, "\n        return i4Return;\n    }\n");
                }
                fprintf (f, "\n    i4Return = nmhTestv2");

                /* 
                 * This function passes the SI nmh routine 
                 * to the corresponding MI routine 
                 * */
                pass_si_nmh_routine (f, i4_count);

                fprintf (f, "(");
                fprintf (f, "pu4ErrorCode , ");

                if (p_table_struct->no_of_indices > 1)
                {
                    passing_index_to_si_lowlevelcode (f);

                    /*  
                     *  Passing the TYPE and Variable 
                     *  for The return Value. 
                     *  */
                    if (i4_global_scalar_flag == ZERO)
                    {
                        fprintf (f, " , ");
                    }
                }

                passing_prefix_according_totype (f, i4_count);

                fprintf (f, "TestVal%s);",
                         p_table_struct->object[i4_count].object_name);
                if (i4_global_scalar_flag == ZERO)
                {
                    fprintf (f, "\n    %s();", ai1_release_context);
                }
                fprintf (f, "\n    return i4Return;");
            }

#ifdef MIDGEN_DEBUG_WANTED

            fprintf (f, "\n\tprintf(\"Function Name : nmhTestv2%s\");",
                     p_table_struct->object[i4_count].object_name);

            for (i4_vcounter = ZERO;
                 i4_vcounter < p_table_struct->no_of_indices; i4_vcounter++)
            {

                for (i4_vcount = ZERO;
                     i4_vcount <
                     p_table_struct->no_of_objects +
                     p_table_struct->no_of_imports; i4_vcount++)
                {

                    if (COMPARE_OBJECT_WITH_INDEX (i4_vcount, i4_vcounter) !=
                        FAILURE)
                    {
                        continue;
                    }
                    print_variable_value (f, INDEX, i4_vcount);

                    /* Break When A Index is Found. */
                    break;
                }                /* End of For Loop */

            }                    /* End of FOR Loop for Each Index . */

            print_variable_value_obj (f, i4_count, TEST, 0);

            fprintf (f, "\n    ");
            fprintf (f, " \t printf(\" ");
            fprintf (f, PERCENT);
            fprintf (f, POS_INT);
            fprintf (f, " \", pu4ErrorCode);  ");

#endif /* MIDGEN_DEBUG_WANTED */

            fprintf (f, "\n}");

        }                        /* End of IF condition for checking ACCESS permissions. */

    }                            /* End of Main For Loop.  */

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : passing_index_to_lowlevelcode                   *
*      Role of the function : This Fn Passes the Indices to the Low Level     *
*                             Routines .                                      *
*      Formal Parameters    : f                                               *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
passing_index_to_lowlevelcode (FILE * f)
{
    INT4                i4_counter;
    INT4                i4_count;
    INT4                i4_cnt;
    UINT1               au1Buf[5];
    memset (au1Buf, 0, 5);
    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {
        if (i4_counter > ZERO)
        {
            fprintf (f, " , ");
        }
        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            passing_typeto_lowlev_skeletolcode (f, i4_count);
            passing_prefix_according_totype (f, i4_count);

            for (i4_cnt = ZERO; i4_cnt < i4_counter; i4_cnt++)
            {

                if (strcmp
                    (p_table_struct->index_name[i4_cnt],
                     p_table_struct->object[i4_count].object_name) == 0)
                {
                    sprintf (au1Buf, "%d", i4_counter);
                }
            }

            fprintf (f, "%s%s", p_table_struct->object[i4_count].object_name,
                     au1Buf);

            /* Break When A Index is Found. */
            break;
        }                        /* End Of FOR Loop. */

    }                            /* End of Main FOR Loop For Each Index. */

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : passing_varsto_lowlevel_getfirst                *
*      Role of the function : This Fn Passes the Parameters alone (with out   *
*                             types) to the Low Level Routines GET FIRST.     *
*      Formal Parameters    : f                                               *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
passing_varsto_lowlevel_getfirst (FILE * f)
{
    INT4                i4_counter;
    INT4                i4_count;

    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {
        if (i4_counter > ZERO)
        {
            fprintf (f, " , ");
        }
        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            passing_prefix_according_totype_getfirst (f, i4_count);
            fprintf (f, "%s", p_table_struct->object[i4_count].object_name);

            /* Break When A Index is Found. */
            break;
        }                        /* End of For Loop */

    }                            /* End of FOR Loop for Each Index . */

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : passing_varsto_lowlevel                         *
*      Role of the function : This Fn Passes the Parameters alone (with out   *
*                             types) to the Low Level Routines .              *
*      Formal Parameters    : f                                               *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
passing_varsto_lowlevel (FILE * f)
{
    INT4                i4_counter;
    INT4                i4_count;

    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {
        if (i4_counter > ZERO)
        {
            fprintf (f, " , ");
        }
        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            passing_prefix_according_totype (f, i4_count);
            fprintf (f, "%s", p_table_struct->object[i4_count].object_name);

            /* Break When A Index is Found. */
            break;
        }                        /* End of For Loop */

    }                            /* End of FOR Loop for Each Index . */

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : defining_varsfor_lowlevel_getfirst              *
*      Role of the function : This Fn defines the Parameters alone (with out  *
*                             types) to the Low Level Routines GET FIRST.     *
*      Formal Parameters    : f                                               *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
defining_varsfor_lowlevel_getfirst (FILE * f)
{
    INT4                i4_counter;
    INT4                i4_count;

    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {
        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            fprintf (f, "\n    ");
            passing_typeto_lowlev_skeletolcode_getfirst (f, i4_count);
            passing_prefix_according_totype_getfirst (f, i4_count);
            fprintf (f, "%s ;", p_table_struct->object[i4_count].object_name);

            /* Break When A Index is Found. */
            break;
        }                        /* End of For Loop */

    }                            /* End of FOR Loop for Each Index . */

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : defining_varsfor_lowlevel                       *
*      Role of the function : This Fn defines the Parameters alone (with out  *
*                             types) to the Low Level Routines .              *
*      Formal Parameters    : f                                               *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
defining_varsfor_lowlevel (FILE * f)
{
    INT4                i4_counter;
    INT4                i4_count;

    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {

        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            fprintf (f, "\n    ");
            passing_typeto_lowlev_skeletolcode (f, i4_count);
            passing_prefix_according_totype (f, i4_count);
            fprintf (f, "%s ;", p_table_struct->object[i4_count].object_name);

            /* Break When A Index is Found. */
            break;
        }                        /* End of For Loop */

    }                            /* End of FOR Loop for Each Index . */

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : passing_typeto_lowlev_skeletolcode_getfirst     *
*      Role of the function : This Fn writes to the File the Type of the Index*
*                             be passed Low Level Routines GET FIRST & NEXT.  *
*      Formal Parameters    : f , i4_count                                    *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
passing_typeto_lowlev_skeletolcode_getfirst (FILE * f, INT4 i4_count)
{

    switch (p_table_struct->object[i4_count].type)
    {
        case POS_INTEGER:
        case ADDRESS:
        {
            if (strcmp (p_table_struct->object[i4_count].type_name, "Counter64")
                == 0)
                fprintf (f, "tSNMP_COUNTER64_TYPE *");
            else
                fprintf (f, "UINT4 *");
            break;
        }
        case INTEGER:
        {
            fprintf (f, "INT4 *");
            break;
        }
        case OCTET_STRING:
        {
            fprintf (f, "tSNMP_OCTET_STRING_TYPE * ");
            break;
        }
        case OBJECT_IDENTIFIER:
        {
            fprintf (f, "tSNMP_OID_TYPE * ");
            break;
        }
        case MacAddress:
        {
            fprintf (f, "tMacAddr * ");
            break;
        }
        default:
            break;
    }                            /*  End of Switch */

}                                /* End of Function. */

#ifdef MIDGEN_DEBUG_WANTED

 /* 
  * The functions print_variable_value() and print_variable_value_obj() are
  * added to print in the console the values of the parameters passed to 
  * the functions present in the low level routines.
  */

void
print_variable_value (FILE * f, INT4 i4_type_flag, INT4 i4_count)
{
    fprintf (f, "\n    ");
    fprintf (f, "\tprintf(\" ");
    switch_case1 (f, i4_count, i4_type_flag);
    fprintf (f, ");\n");

}                                /* End Of Function. */

switch_case1 (FILE * f, INT4 i4_count, INT4 i4_type_flag)
{
    switch (p_table_struct->object[i4_count].type)
    {
        case POS_INTEGER:
        case ADDRESS:
        {
            if (i4_type_flag == GETFIRST_INDEX)
            {
                fprintf (f, PERCENT);
                fprintf (f, POS_INT);
                fprintf (f, " \" ");
                if (strcmp
                    (p_table_struct->object[i4_count].type_name,
                     "Counter64") == 0)
                    fprintf (f, ", pu8%s",
                             p_table_struct->object[i4_count].object_name);
                else
                    fprintf (f, ", pu4%s",
                             p_table_struct->object[i4_count].object_name);
            }

            if (i4_type_flag == INDEX)
            {
                fprintf (f, PERCENT);
                fprintf (f, POS_INT);
                fprintf (f, " \" ");
                if (strcmp
                    (p_table_struct->object[i4_count].type_name,
                     "Counter64") == 0)
                    fprintf (f, ", u8%s",
                             p_table_struct->object[i4_count].object_name);
                else
                    fprintf (f, ", u4%s",
                             p_table_struct->object[i4_count].object_name);
            }

            if (i4_type_flag == GET)
            {
                fprintf (f, PERCENT);
                fprintf (f, POS_INT);
                fprintf (f, " \" ");
                if (strcmp
                    (p_table_struct->object[i4_count].type_name,
                     "Counter64") == 0)
                {
                    /*fprintf(f,", u8%s ",p_table_struct->object[i4_count].object_name); */
                    fprintf (f, ", pu8RetVal%s",
                             p_table_struct->object[i4_count].object_name);
                }

                else
                {
                    /*fprintf(f,", u4%s ",p_table_struct->object[i4_count].object_name); */
                    fprintf (f, ", pu4RetVal%s",
                             p_table_struct->object[i4_count].object_name);
                }
            }
            if (i4_type_flag == SET)
            {
                fprintf (f, PERCENT);
                fprintf (f, POS_INT);
                fprintf (f, " \" ");
                if (strcmp
                    (p_table_struct->object[i4_count].type_name,
                     "Counter64") == 0)
                    fprintf (f, ", u8SetVal%s",
                             p_table_struct->object[i4_count].object_name);
                else
                    fprintf (f, ", u4SetVal%s",
                             p_table_struct->object[i4_count].object_name);
            }

            if (i4_type_flag == TEST)
            {
                fprintf (f, PERCENT);
                fprintf (f, POS_INT);
                fprintf (f, " \" ");
                if (strcmp
                    (p_table_struct->object[i4_count].type_name,
                     "Counter64") == 0)
                    fprintf (f, ", u8TestVal%s",
                             p_table_struct->object[i4_count].object_name);
                else
                    fprintf (f, ", u4TestVal%s",
                             p_table_struct->object[i4_count].object_name);
            }

            /*fprintf(f," );"); */
            break;
        }
        case INTEGER:
        {
            if (i4_type_flag == GETFIRST_INDEX)
            {
                fprintf (f, PERCENT);
                fprintf (f, INT);
                fprintf (f, " \" ");
                fprintf (f, ", pi4%s",
                         p_table_struct->object[i4_count].object_name);
            }
            if (i4_type_flag == INDEX)
            {
                fprintf (f, PERCENT);
                fprintf (f, INT);
                fprintf (f, " \" ");
                fprintf (f, ", i4%s",
                         p_table_struct->object[i4_count].object_name);
            }
            if (i4_type_flag == GET)
            {
                fprintf (f, PERCENT);
                fprintf (f, INT);
                fprintf (f, " \" ");
                fprintf (f, ", pi4RetVal%s",
                         p_table_struct->object[i4_count].object_name);
            }
            if (i4_type_flag == SET)
            {
                fprintf (f, PERCENT);
                fprintf (f, INT);
                fprintf (f, " \" ");
                fprintf (f, ", i4SetVal%s",
                         p_table_struct->object[i4_count].object_name);
            }
            if (i4_type_flag == TEST)
            {
                fprintf (f, PERCENT);
                fprintf (f, INT);
                fprintf (f, " \" ");
                fprintf (f, ", i4TestVal%s",
                         p_table_struct->object[i4_count].object_name);
            }
            break;
        }
        case OCTET_STRING:
        case OBJECT_IDENTIFIER:
        case MacAddress:
        {
            fprintf (f, PERCENT);
            fprintf (f, INT);
            fprintf (f, " \" ");
            fprintf (f, ", p%s", p_table_struct->object[i4_count].object_name);
            break;
        }
        default:
            break;
    }

}

void
print_variable_value_obj (FILE * f, INT4 i4_count, INT4 i4_type_flag,
                          INT4 i4Flag)
{
    fprintf (f, "\n    ");
    fprintf (f, "\tprintf(\" ");
    switch_case (f, i4_count, i4_type_flag);
    fprintf (f, ");\n");
}                                /* End Of Function. */

switch_case (FILE * f, INT4 i4_count, INT4 i4_type_flag)
{
    switch (p_table_struct->object[i4_count].type)
    {
        case POS_INTEGER:
        case ADDRESS:
        {
            if (i4_type_flag == GETFIRST_INDEX)
            {
                fprintf (f, PERCENT);
                fprintf (f, POS_INT);
                fprintf (f, " \" ");
                if (strcmp
                    (p_table_struct->object[i4_count].type_name,
                     "Counter64") == 0)
                    fprintf (f, ", pu8Next%s",
                             p_table_struct->object[i4_count].object_name);
                else
                    fprintf (f, ", pu4Next%s",
                             p_table_struct->object[i4_count].object_name);
            }

            if (i4_type_flag == INDEX)
            {
                fprintf (f, PERCENT);
                fprintf (f, "ul");
                fprintf (f, " \" ");
                if (strcmp
                    (p_table_struct->object[i4_count].type_name,
                     "Counter64") == 0)
                    fprintf (f, ", u8%s",
                             p_table_struct->object[i4_count].object_name);
                else
                    fprintf (f, ", u4%s",
                             p_table_struct->object[i4_count].object_name);
            }

            if (i4_type_flag == GET)
            {
                fprintf (f, PERCENT);
                fprintf (f, POS_INT);
                fprintf (f, " \" ");
                if (strcmp
                    (p_table_struct->object[i4_count].type_name,
                     "Counter64") == 0)
                {
                    /*fprintf(f,", u8%s ",p_table_struct->object[i4_count].object_name); */
                    fprintf (f, ", pu8RetVal%s",
                             p_table_struct->object[i4_count].object_name);
                }

                else
                {
                    /*fprintf(f,", u4%s ",p_table_struct->object[i4_count].object_name); */
                    fprintf (f, ", pu4RetVal%s",
                             p_table_struct->object[i4_count].object_name);
                }
            }
            if (i4_type_flag == SET)
            {
                fprintf (f, PERCENT);
                fprintf (f, POS_INT);
                fprintf (f, " \" ");
                if (strcmp
                    (p_table_struct->object[i4_count].type_name,
                     "Counter64") == 0)
                    fprintf (f, ", u8SetVal%s",
                             p_table_struct->object[i4_count].object_name);
                else
                    fprintf (f, ", u4SetVal%s",
                             p_table_struct->object[i4_count].object_name);
            }

            if (i4_type_flag == TEST)
            {
                fprintf (f, PERCENT);
                fprintf (f, POS_INT);
                fprintf (f, " \" ");
                if (strcmp
                    (p_table_struct->object[i4_count].type_name,
                     "Counter64") == 0)
                    fprintf (f, ", u8TestVal%s",
                             p_table_struct->object[i4_count].object_name);
                else
                    fprintf (f, ", u4TestVal%s",
                             p_table_struct->object[i4_count].object_name);
            }

            /* fprintf(f," );");   */
            break;
        }
        case INTEGER:
        {
            if (i4_type_flag == GETFIRST_INDEX)
            {
                fprintf (f, PERCENT);
                fprintf (f, INT);
                fprintf (f, "\" ");
                fprintf (f, ", pi4Next%s",
                         p_table_struct->object[i4_count].object_name);
            }
            if (i4_type_flag == INDEX)
            {
                fprintf (f, PERCENT);
                fprintf (f, INT);
                fprintf (f, "\" ");
                fprintf (f, ", i4%s",
                         p_table_struct->object[i4_count].object_name);
            }
            if (i4_type_flag == GET)
            {
                fprintf (f, PERCENT);
                fprintf (f, INT);
                fprintf (f, "\" ");
                fprintf (f, ", pi4RetVal%s",
                         p_table_struct->object[i4_count].object_name);
            }
            if (i4_type_flag == SET)
            {
                fprintf (f, PERCENT);
                fprintf (f, INT);
                fprintf (f, "\" ");
                fprintf (f, ", i4SetVal%s",
                         p_table_struct->object[i4_count].object_name);
            }
            if (i4_type_flag == TEST)
            {
                fprintf (f, PERCENT);
                fprintf (f, INT);
                fprintf (f, "\" ");
                fprintf (f, ", i4TestVal%s",
                         p_table_struct->object[i4_count].object_name);
            }
            break;
        }
        case OCTET_STRING:
        case OBJECT_IDENTIFIER:
        {
            if (i4_type_flag == GETFIRST_INDEX)
            {
                fprintf (f, PERCENT);
                fprintf (f, INT);
                fprintf (f, "\" ");
                fprintf (f, ", pNext%s",
                         p_table_struct->object[i4_count].object_name);
            }

            if (i4_type_flag == INDEX)
            {
                fprintf (f, PERCENT);
                fprintf (f, INT);
                fprintf (f, "\" ");
                fprintf (f, ", %s",
                         p_table_struct->object[i4_count].object_name);
            }

            if (i4_type_flag == GET)
            {
                fprintf (f, PERCENT);
                fprintf (f, INT);
                fprintf (f, "\" ");
                fprintf (f, ", pRetVal%s",
                         p_table_struct->object[i4_count].object_name);
            }

            if (i4_type_flag == SET)
            {
                fprintf (f, PERCENT);
                fprintf (f, INT);
                fprintf (f, "\" ");
                fprintf (f, ", pSetVal%s",
                         p_table_struct->object[i4_count].object_name);
            }

            if (i4_type_flag == TEST)
            {
                fprintf (f, PERCENT);
                fprintf (f, INT);
                fprintf (f, "\" ");
                fprintf (f, ", pTestVal%s",
                         p_table_struct->object[i4_count].object_name);
            }
            break;
        }
        case MacAddress:
        {
            if (i4_type_flag == GETFIRST_INDEX)
            {
                fprintf (f, PERCENT);
                fprintf (f, INT);
                fprintf (f, "\" ");
                fprintf (f, ", Next%s",
                         p_table_struct->object[i4_count].object_name);
            }

            if (i4_type_flag == INDEX)
            {
                fprintf (f, PERCENT);
                fprintf (f, INT);
                fprintf (f, "\" ");
                fprintf (f, ", %s",
                         p_table_struct->object[i4_count].object_name);
            }

            if (i4_type_flag == GET)
            {
                fprintf (f, PERCENT);
                fprintf (f, INT);
                fprintf (f, "\" ");
                fprintf (f, ", RetVal%s",
                         p_table_struct->object[i4_count].object_name);
            }

            if (i4_type_flag == SET)
            {
                fprintf (f, PERCENT);
                fprintf (f, INT);
                fprintf (f, "\" ");
                fprintf (f, ", SetVal%s",
                         p_table_struct->object[i4_count].object_name);
            }

            if (i4_type_flag == TEST)
            {
                fprintf (f, PERCENT);
                fprintf (f, INT);
                fprintf (f, "\" ");
                fprintf (f, ", TestVal%s",
                         p_table_struct->object[i4_count].object_name);
            }
            break;
        }
        default:
            break;
    }

}

#endif /* MIDGEN_DEBUG_WANTED */

/******************************************************************************
*      function Name        : passing_typeto_lowlev_skeletolcode              *
*      Role of the function : This Fn writes to the File the Type of the Index*
*                             be passed Low Level Routines .                  *
*      Formal Parameters    : f , i4_count                                    *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
passing_typeto_lowlev_skeletolcode (FILE * f, INT4 i4_count)
{

    switch (p_table_struct->object[i4_count].type)
    {
        case POS_INTEGER:
        case ADDRESS:
        {
            if ((strcmp
                 (p_table_struct->object[i4_count].type_name,
                  "Counter64")) == 0)
                fprintf (f, "tSNMP_COUNTER64_TYPE ");
            else
                fprintf (f, "UINT4 ");
            break;
        }
        case INTEGER:
        {
            fprintf (f, "INT4 ");
            break;
        }
        case OCTET_STRING:
        {
            fprintf (f, "tSNMP_OCTET_STRING_TYPE *");
            break;
        }
        case OBJECT_IDENTIFIER:
        {
            fprintf (f, "tSNMP_OID_TYPE *");
            break;
        }
        case MacAddress:
        {
            fprintf (f, "tMacAddr ");
            break;
        }
        default:
            break;
    }                            /*  End of Switch */

}                                /* End of Function. */

/******************************************************************************
*      function Name        : passing_prefix_according_totype_getfirst        *
*      Role of the function : This Fn Passes the Prefix of the Var (i.ei4 for *
*                             INT4 etc) to the Fn for GET FIRST.              *
*      Formal Parameters    : f , i4_count                                    *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
passing_prefix_according_totype_getfirst (FILE * f, INT4 i4_count)
{

    switch (p_table_struct->object[i4_count].type)
    {
        case ADDRESS:
        case POS_INTEGER:

            if (strcmp (p_table_struct->object[i4_count].type_name, "Counter64")
                == 0)
                fprintf (f, "pu8");
            else
                fprintf (f, "pu4");
            break;
        case INTEGER:
            fprintf (f, "pi4");
            break;
        case OCTET_STRING:
        case MacAddress:
        case OBJECT_IDENTIFIER:
            fprintf (f, "p");
            break;

    }                            /* End Of Main Switch. */

}                                /*  End Of Function. */

/******************************************************************************
*      function Name        : passing_prefix_according_totype                 *
*      Role of the function : This Fn Passes the Prefix of the Var (i.ei4 for *
*                             INT4 etc) to the Fn .                           *
*      Formal Parameters    : f , i4_count                                    *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
passing_prefix_according_totype (FILE * f, INT4 i4_count)
{

    switch (p_table_struct->object[i4_count].type)
    {
        case ADDRESS:
        case POS_INTEGER:
            if ((strcmp
                 (p_table_struct->object[i4_count].type_name,
                  "Counter64")) == 0)
                fprintf (f, "u8");
            else
                fprintf (f, "u4");
            break;
        case INTEGER:
            fprintf (f, "i4");
            break;
        case OCTET_STRING:
        case OBJECT_IDENTIFIER:
            fprintf (f, "p");
            break;
        case MacAddress:
            break;

    }                            /* End Of Main Switch. */

}                                /*  End Of Function. */

/******************************************************************************
*      function Name        : trace_logs_for_index_getfirst                   *
*      Role of the function : This Fn Generates the Trace Logs For the Indices*
*                             of a Table For GET FIRST and GET NEXT fns .     *                           *
*      Formal Parameters    : file                                            *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
trace_logs_for_index_getfirst (FILE * file)
{
    INT4                i4_count = ZERO;
    INT4                i4_counter = ZERO;

    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {
        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            /*  This Fn Adds the Trace Logs for Each Object.  */
            trace_logs_for_object (file, i4_count, GETFIRST_INDEX);

            /* Break when an Object is Found. */
            break;
        }                        /*  End Of FOR Loop For All Objects. */

    }                            /* End Of Main FOR Loop For Each Index. */

}                                /*  End Of Function. */

/******************************************************************************
*      function Name        : trace_logs_for_index                            *
*      Role of the function : This Fn Generates the Trace Logs For the Indices*
*                             of a Table .                                    *
*      Formal Parameters    : file                                            *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
trace_logs_for_index (FILE * file)
{
    INT4                i4_count = ZERO;
    INT4                i4_counter = ZERO;

    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {

        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            /*  This Fn Adds the Trace Logs for Each Object.  */
            trace_logs_for_object (file, i4_count, INDEX);

            /* Break when an Object is Found. */
            break;
        }                        /*  End Of FOR Loop For All Objects. */

    }                            /* End Of Main FOR Loop For Each Index. */

}                                /*  End Of Function. */

/******************************************************************************
*      function Name        : trace_logs_for_object                           *
*      Role of the function : This Fn Generates the Trace Logs For an Object  *
*                             according to the Type of the Flag which is given*
*                             as a Parameter to the Fn.                       *
*                             If i4_type_flag = GETFIRST_INDEX then for       *
*                                               getfirst & getnext            *
*                             If i4_type_flag = INDEX then for GET SET & TEST *
*                                               (for Index)                   *
*                             If i4_type_flag = GET then for Get              *
*                             If i4_type_flag = SET then for Set              *
*                             If i4_type_flag = TEST then for Test routines.  *
*      Formal Parameters    : file                                            *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
trace_logs_for_object (FILE * file, INT4 i4_count, INT4 i4_type_flag)
{

    switch (p_table_struct->object[i4_count].type)
    {
        case POS_INTEGER:
        case ADDRESS:
        {
            fprintf (file, QOTES);
            fprintf (file, "%s = ",
                     p_table_struct->object[i4_count].object_name);
            fprintf (file, PERCENT);
            fprintf (file, POS_INT);
            fprintf (file, "\\n");
            fprintf (file, QOTES);
            if (i4_type_flag == GETFIRST_INDEX)
            {
                if (strcmp
                    (p_table_struct->object[i4_count].type_name,
                     "Counter64") == 0)
                    fprintf (file, ", *pu8%s); ***/",
                             p_table_struct->object[i4_count].object_name);
                else
                    fprintf (file, ", *pu4%s); ***/",
                             p_table_struct->object[i4_count].object_name);
            }
            if (i4_type_flag == INDEX)
            {
                if (strcmp
                    (p_table_struct->object[i4_count].type_name,
                     "Counter64") == 0)
                    fprintf (file, ", u8%s); ***/",
                             p_table_struct->object[i4_count].object_name);
                else
                    fprintf (file, ", u4%s); ***/",
                             p_table_struct->object[i4_count].object_name);
            }
            if (i4_type_flag == GET)
            {
                if (strcmp
                    (p_table_struct->object[i4_count].type_name,
                     "Counter64") == 0)
                    fprintf (file, ", *pu8RetVal%s); ***/",
                             p_table_struct->object[i4_count].object_name);
                else
                    fprintf (file, ", *pu4RetVal%s); ***/",
                             p_table_struct->object[i4_count].object_name);
            }
            if (i4_type_flag == SET)
            {
                if (strcmp
                    (p_table_struct->object[i4_count].type_name,
                     "Counter64") == 0)
                    fprintf (file, ", u8SetVal%s); ***/",
                             p_table_struct->object[i4_count].object_name);
                else
                    fprintf (file, ", u4SetVal%s); ***/",
                             p_table_struct->object[i4_count].object_name);
            }
            if (i4_type_flag == TEST)
            {
                if (strcmp
                    (p_table_struct->object[i4_count].type_name,
                     "Counter64") == 0)
                    fprintf (file, ", u8TestVal%s); ***/",
                             p_table_struct->object[i4_count].object_name);
                else
                    fprintf (file, ", u4TestVal%s); ***/",
                             p_table_struct->object[i4_count].object_name);
            }
            break;
        }
        case INTEGER:
        {
            fprintf (file, QOTES);
            fprintf (file, "%s = ",
                     p_table_struct->object[i4_count].object_name);
            fprintf (file, PERCENT);
            fprintf (file, INT);
            fprintf (file, "\\n");
            fprintf (file, QOTES);
            if (i4_type_flag == GETFIRST_INDEX)
            {
                fprintf (file, ", *pi4%s); ***/",
                         p_table_struct->object[i4_count].object_name);
            }
            if (i4_type_flag == INDEX)
            {
                fprintf (file, ", i4%s); ***/",
                         p_table_struct->object[i4_count].object_name);
            }
            if (i4_type_flag == GET)
            {
                fprintf (file, ", *pi4RetVal%s); ***/",
                         p_table_struct->object[i4_count].object_name);
            }
            if (i4_type_flag == SET)
            {
                fprintf (file, ", i4SetVal%s); ***/",
                         p_table_struct->object[i4_count].object_name);
            }
            if (i4_type_flag == TEST)
            {
                fprintf (file, ", i4TestVal%s); ***/",
                         p_table_struct->object[i4_count].object_name);
            }
            break;
        }
    }                            /* End Of Switch Case. */

}                                /*  End Of Function. */

/******************************************************************************
*      function Name        : passing_context_id                              *
*      Role of the function : This Fn passes the context id to GetContext and *
*                             ReleaseContext Routines                         *
*      Formal Parameters    : f                                               *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
passing_context_id (FILE * f)
{
    INT4                i4_count;

    if (p_table_struct->no_of_imports > ZERO)
    {
        i4_count = p_table_struct->no_of_objects +
            p_table_struct->no_of_imports - 1;
        passing_prefix_according_totype (f, i4_count);
        fprintf (f, "%s", p_table_struct->object[i4_count].object_name);
    }

    if (p_table_struct->no_of_imports == ZERO)
    {
        i4_count = ZERO;
        passing_prefix_according_totype (f, i4_count);
        fprintf (f, "%s", p_table_struct->object[i4_count].object_name);
    }
}                                /* End Of Function */

/******************************************************************************
*      function Name        : pass_si_nmh_routine                             *
*      Role of the function : This Fn passes the si nmh routine               *
*                                                                             *
*      Formal Parameters    : f, i4_count                                     *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
pass_si_nmh_routine (FILE * f, INT4 i4_count)
{
    INT4                i4_length1;
    INT4                i4_length2;
    INT4                i4_length3;
    INT1                ai1_si_object_name[MAX_LINE] = { 0 };
    INT1               *pi1_result;

    if (strstr (p_table_struct->object[i4_count].object_name,
                ai1_mi_string) == NULL)
    {
        printf ("\n\n The user defined mi_string does not occur in"
                " the mi_nmh_routine\n");
        validate_command_line ();
        exit (NEGATIVE);
    }

    i4_length1 = strlen (p_table_struct->object[i4_count].object_name);
    i4_length2 = strlen (ai1_mi_string);

    pi1_result = strstr (p_table_struct->object[i4_count].object_name,
                         ai1_mi_string);
    i4_length3 = strlen (pi1_result);

    pi1_result = (pi1_result + i4_length2);

    memcpy (ai1_si_object_name, p_table_struct->object[i4_count].object_name,
            (i4_length1 - i4_length3));
    ai1_si_object_name[i4_length1 - i4_length3] = 0;    /* NULL Termination */
    strcat (ai1_si_object_name, pi1_result);

    fprintf (f, "%s", ai1_si_object_name);
}                                /* End Of Function  */

/******************************************************************************
*      function Name        : passing_index_to_si_lowlevelcode                *
*      Role of the function : This Fn Passes the Indices to the Low Level     *
*                             Routines .                                      *
*      Formal Parameters    : f                                               *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
passing_index_to_si_lowlevelcode (FILE * f)
{
    INT4                i4_counter;
    INT4                i4_count;

    for (i4_counter = 1; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {
        if (i4_counter > 1)
        {
            fprintf (f, " , ");
        }

        for (i4_count = 0; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {
            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            passing_prefix_according_totype (f, i4_count);
            fprintf (f, "%s", p_table_struct->object[i4_count].object_name);

            /* Break When A Index is Found. */
            break;
        }                        /* End Of FOR Loop. */

    }                            /* End of Main FOR Loop For Each Index. */

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : low_code_dep_all_objects                        *
*      Role of the function : This Fn Generates the Skeletol Code for the Low *
*                             Level Dependency Rtns for all the Scalar Objects*
*                             and a single Dep Rtn for all the                *
*                             objects in a Table.                             *
*      Formal Parameters    : f                                               *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
low_code_dep_all_objects (FILE * f)
{
    INT4                i4Count;
    INT1                i1_first_obj_flag = 0;

    if (p_table_struct->status == STATUS_OBSOLETE)
        return;

    for (i4Count = ZERO; i4Count < p_table_struct->no_of_objects; i4Count++)
    {
        if (find_object_is_index (p_table_struct->object[i4Count].
                                  object_name) == IS_INDEX)
        {
            continue;
        }
        if ((p_table_struct->object[i4Count].access) == READ_ONLY
            || (p_table_struct->object[i4Count].status == STATUS_OBSOLETE))
        {
            continue;
        }
        if (i1_first_obj_flag == 0)
        {
            i1_first_obj_flag = 1;
            fprintf (f,
                     "\n/* Low Level Dependency Routines for All Objects  */\n");
        }

        /* Scalar Objects */
        if (p_table_struct->no_of_indices == 0)
        {
            if ((p_table_struct->object[i4Count].access == READ_WRITE)
                || (p_table_struct->object[i4Count].access == READ_CREATE))
            {
                /*  Generates Comments for Low Level DEP Routines   */
                comment_for_lowlevel_dep (f, FALSE, i4Count);
                fprintf (f, "\nINT1 nmhDepv2%s(",
                         p_table_struct->object[i4Count].object_name);
                fprintf (f,
                         "UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)");
                fprintf (f, "\n{");
                fprintf (f, "\n\tUNUSED_PARAM(pu4ErrorCode);\n");
                fprintf (f, "\tUNUSED_PARAM(pSnmpIndexList);\n");
                fprintf (f, "\tUNUSED_PARAM(pSnmpVarBind);\n");
                fprintf (f, "\treturn SNMP_SUCCESS;");
                fprintf (f, "\n}");
            }
        }
        else
        {
            comment_for_lowlevel_dep (f, TRUE, ZERO);
            fprintf (f, "\nINT1 nmhDepv2%s(", p_table_struct->table_name);
            fprintf (f,
                     "UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)");
            fprintf (f, "\n{");
            fprintf (f, "\n\tUNUSED_PARAM(pu4ErrorCode);\n");
            fprintf (f, "\tUNUSED_PARAM(pSnmpIndexList);\n");
            fprintf (f, "\tUNUSED_PARAM(pSnmpVarBind);\n");
            fprintf (f, "\treturn SNMP_SUCCESS;");
            fprintf (f, "\n}");
            break;
        }
    }
}

/******************************************************************************
*      function Name        : comment_for_lowlevel_dep                       *
*      Role of the function : This Fn Generates the Comment for the Low Level *
*                             skeletol code for Dependency Routines.          *
*      Formal Parameters    : f , i4_counter                                  *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
comment_for_lowlevel_dep (FILE * f, INT4 is_tabular_object, INT4 i4_counter)
{
    INT4                i4_count;

    fprintf (f, "\n/" DOT);
    if (is_tabular_object == TRUE)
    {
        fprintf (f, "\n Function    :  nmhDepv2%s", p_table_struct->table_name);
        fprintf (f, "\n Input       :  The Indices");
        /*  FOR Loop for Adding the Index names in the Comments for the Test Fns.  */
        for (i4_count = ZERO; i4_count < p_table_struct->no_of_indices;
             i4_count++)
        {
            fprintf (f, "\n                %s",
                     p_table_struct->index_name[i4_count]);
        }
    }
    else
    {
        fprintf (f, "\n Function    :  nmhDepv2%s",
                 p_table_struct->object[i4_counter].object_name);
    }
    fprintf (f,
             "\n Output      :  The Dependency Low Lev Routine Take the Indices &");
    fprintf (f, "\n                check whether dependency is met or not.");
    fprintf (f,
             "\n                Stores the value of error code in the Return val");
    fprintf (f,
             "\n Error Codes :  The following error codes are to be returned");
    fprintf (f,
             "\n                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)");
    fprintf (f,
             "\n                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)");
    fprintf (f,
             "\n                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)");
    fprintf (f,
             "\n                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)");
    fprintf (f,
             "\n                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)");
    fprintf (f, "\n Returns     :  SNMP_SUCCESS or SNMP_FAILURE");
    fprintf (f, "\n" DOT "/");

}                                /* End Of Function. */
