/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: line_ops.c,v 1.4 2015/04/28 14:43:31 siva Exp $
 *
 * Description: This File Contains the function Which     
 *              manipulate each Line in the File. 
 *******************************************************************/

#include "include.h"
#include "extern.h"

/******************************************************************************
*      function Name        : get_line                                        *
*      Role of the function : This fn gets a Line from the File Stream Given. *
*                             The Fn Display the Mib File Name by Comparing   *
*                             the local File Ptr and the Global ptr "f".      *
*      Formal Parameters    : file                                            *  *
*      Global Variables     : i4_global_lineno_flag , f (the MIB file ptr).   *
*      Use of Recursion     : None                                            *
*      Return Value         : A line From the File.                           *
******************************************************************************/
UINT1              *
get_line (FILE * file)
{
    /* This Variable is to count the No of Lines Processed in the Mib File. */
    static int          i4_lineno = ZERO;
    UINT1              *pi1_string;

    if ((pi1_string = (UINT1 *) calloc (1, MAX_LINE)) == NULL)
    {
        printf ("\n\n\tERROR : Malloc Failure.\n");
        clear_directory ();
        exit (NEGATIVE);
    }
    fgets (pi1_string, MAX_LINE, file);

    /* Moving the Pointer to the First Valid Char.  */
    while ((*pi1_string == SPACE) || (*pi1_string == TAB))
    {
        pi1_string++;
    }

    if (i4_global_lineno_flag == DISPLAY_ON)
    {
        /*  Debug Option.  */
        if (file == f)
        {
            printf ("MIB Line  %d : %s", i4_lineno++, pi1_string);
        }
    }

    return pi1_string;

}                                /*  End Of Function. */

/******************************************************************************
*      function Name        : remove_comment                                  *
*      Role of the function : This fn removes the comment from the given      *
*                             input Line.                                     *
*      Formal Parameters    : file                                            *
*      Global Variables     : i4_global_lineno_flag , f (the MIB file ptr).   *
*      Use of Recursion     : None                                            *
*      Return Value         : The Comment Removed line.                       *
******************************************************************************/
UINT1              *
remove_comment (UINT1 *pi1_string)
{
    UINT1              *pi1_flag;

    pi1_flag = strstr (pi1_string, COMMENT);
    if (pi1_flag != NULL)
    {
        *pi1_flag = NULL_CHAR;
    }
    pi1_flag = strstr (pi1_string, NEWLINE_STRING);
    if (pi1_flag != NULL)
    {
        *pi1_flag = NULL_CHAR;
    }

    return pi1_string;

}                                /*  End Of Function. */

/******************************************************************************
*      function Name        : check_empty_line                                *
*      Role of the function : This fn checks whether the given Line is empty  *
*                             or Not.  It also check for comment if the Line  *
*                             is a comment then it checks for the presence of *
*                             SCALAR_TABLE_BEGIN & if so return SCALAR.       *
*      Formal Parameters    : pi1_string                                      *
*      Global Variables     : None
*      Use of Recursion     : None                                            *
*      Return Value         : TRUE (when the Line is Not empty or not a comm- *
*                             ent) else if the Comment has SCALAR_TABLE_BEGIN *
*                             then return SCALAR else FALSE.                  *
******************************************************************************/
INT4
check_empty_line (UINT1 *pi1_string)
{
    INT4                i4_flag = ZERO;

    i4_flag = strcmp (pi1_string, NEWLINE_STRING);
    if (i4_flag == FALSE)
    {
        return i4_flag;
    }
    i4_flag = strncmp (pi1_string, COMMENT, strlen (COMMENT));
    if (i4_flag == FALSE)
    {
        /*  Finding the Begining of The SCALAR TABLE.  */
        if (strstr (pi1_string, "SCALAR_TABLE_BEGIN") != NULL)
        {
            return SCALAR;
        }
    }
    return i4_flag;

}                                /*  End Of Function. */

/******************************************************************************
*      function Name        : go_down                                         *
*      Role of the function : This fn searches for the pattern "::=" & moves  *
*                             the file ptr to the Line containing the pattern *
*      Formal Parameters    : f                                               *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : TRUE.                                           *
******************************************************************************/
INT4
go_down (FILE * f)
{
    UINT1               u1_temp[MAX_LINE];

    while (!feof (f))
    {

        fgets (u1_temp, MAX_LINE, f);
        if (check_empty_line (u1_temp) == FALSE)
        {
            continue;
        }

        /* Checking for the string "::= " .  */
        if (strstr (u1_temp, END_OF_BLOCK))
        {
            return TRUE;
        }

    }
    return TRUE;

}                                /*  End Of Function. */

/******************************************************************************
*      function Name        : separate_indices                                *
*      Role of the function : This fn searches for the key word "INDEX" & gets*
*                             the Indices from the Line & stores them in the  *
*                             Global Table.                                   *
*      Formal Parameters    : f                                               *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : i4_count (The No of Indices).                   *
******************************************************************************/
INT4
separate_indices (UINT1 *pi1_src_string)
{
    INT4                i4_count = ZERO;
    INT4                i4_temp_count = ZERO;
    UINT1              *pi1_string;
    UINT1               i1_string[MAX_LINE];
    UINT1              *pi1_temp = NULL;
    UINT1              *pi1_dummy = NULL;

    if ((pi1_string = (UINT1 *) calloc (1, MAX_LINE)) == NULL)
    {
        printf ("\n\n\tERROR : Malloc Failure.\n");
        clear_directory ();
        exit (NEGATIVE);
    }

    if ((pi1_temp = (UINT1 *) calloc (1, MAX_LINE)) == NULL)
    {
        printf ("\n\n\tERROR : Malloc Failure.\n");
        clear_directory ();
        exit (NEGATIVE);
    }

    pi1_src_string = strstr (pi1_src_string, OPEN_PARAS);
    pi1_src_string++;

    strcpy (pi1_string, pi1_src_string);
    pi1_temp = strchr (pi1_string, COMMA_CHAR);
    if (pi1_temp == NULL)
    {

        /* This is the Last Index in the Line (Table) */
        pi1_string = strtok (pi1_string, CLOSE_PARAS);

        /* Removing the Space before the Index. */
        while (*pi1_string++ == SPACE);

        pi1_string--;

        /* Finding the Spaces after the Index. */
        pi1_dummy = strchr (pi1_string, SPACE);

        /* Removing the Spaces after the Index. */
        if (pi1_dummy != NULL)
        {
            *pi1_dummy = NULL_CHAR;
        }
        strcpy (p_table_struct->index_name[i4_count], pi1_string);
        return ++i4_count;
    }

    pi1_string = strtok (pi1_string, COMMA);

    /* Removing the Space before the Index. */
    while (*pi1_string++ == SPACE);

    pi1_string--;

    strcpy (p_table_struct->index_name[i4_count], pi1_string);
    i4_count++;
    do
    {
        strcpy (i1_string, pi1_string);
        pi1_string = strtok (NULL, COMMA);
        if (pi1_string == NULL)
        {
            break;
        }

        /* Removing the Space before the Index. */
        while (*pi1_string++ == SPACE);

        pi1_string--;

        strcpy (p_table_struct->index_name[i4_count++], pi1_string);

    }
    while (pi1_string != NULL);

    pi1_string = strtok (i1_string, CLOSE_PARAS);

    /* Removing the Space before the Index. */
    while (*pi1_string++ == SPACE);

    pi1_string--;

    strcpy (p_table_struct->index_name[--i4_count], pi1_string);

    /* Removing the Spaces after the Indices. */
    for (i4_temp_count = ZERO; i4_temp_count < i4_count + ONE; i4_temp_count++)
    {

        pi1_dummy = strchr (p_table_struct->index_name[i4_temp_count], SPACE);
        if (pi1_dummy != NULL)
        {
            *pi1_dummy = NULL_CHAR;
        }

    }
    return ++i4_count;

}                                /*  End Of Function. */

/******************************************************************************
*      function Name        : strupper                                        *
*      Role of the function : This fn converts the Given String to Upper Case *
*      Formal Parameters    : pi1_temp                                        *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : pi1_temp Converted Upper Case String.           *
******************************************************************************/
UINT1              *
strupper (UINT1 *pi1_temp)
{
    INT4                i4_count;

    for (i4_count = FALSE; i4_count < strlen (pi1_temp); i4_count++)
    {
        pi1_temp[i4_count] = toupper (pi1_temp[i4_count]);
    }

    return pi1_temp;

}                                /*  End Of Function. */

/*****************************************************************************
 *      Function Name        : itoa                                          *
 *      Role of the function : Converts integer value to string.             *
 *      Formal Parameters    : i4_value                                      *
 *      Global Variables     : None                                          *
 *      Use of Recursion     : None                                          *
 *      Return Value         : str, if converted                             *
 *                             NULL, if not converted                        *
 *****************************************************************************/

INT1               *
itoa (INT4 i4_value)
{
    INT1                ch, *str;
    INT4                i = 0, j;

    str = (INT1 *) calloc (1, (11 * sizeof (INT1)));
    if (str == NULL)
    {
        fprintf (stderr, "Memory allocation failure\n");
        return NULL;
    }
    if (i4_value < 0)
    {
        str[i++] = '-';
        i4_value *= -1;
    }
    else if (i4_value == 0)
    {
        str[i++] = '0';
    }
    while (i4_value != 0)
    {
        str[i++] = '0' + (i4_value % 10);
        i4_value = i4_value / 10;
    }
    str[i] = '\0';
    for (i = 0, j = strlen (str) - 1; i < j; i++, j--)
    {
        ch = str[i];
        str[i] = str[j];
        str[j] = ch;
    }
    return str;
}
