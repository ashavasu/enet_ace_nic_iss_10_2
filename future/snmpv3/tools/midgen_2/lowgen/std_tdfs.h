/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: std_tdfs.h,v 1.4 2015/04/28 14:43:31 siva Exp $
 *
 * Description:This File contains the Standard Type Defs       
 *             needed for the Mid Level Code Generator.
 *******************************************************************/
		   
  typedef char            INT1;
  typedef unsigned char   UINT1;
  typedef short           INT2;
  typedef unsigned short  UINT2;
  typedef int             INT4;
  typedef unsigned int    UINT4;
