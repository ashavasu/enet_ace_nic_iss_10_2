/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: code_new_type.c,v 1.4 2015/04/28 14:43:31 siva Exp $
 *
 * Description: This File Contains the functions Which            
 *              generate the code for the New Data Type.
 *******************************************************************/

# include "include.h"
# include "extern.h"

/******************************************************************************
*      function Name        : search_new_data_type                            *
*      Role of the function : This fn searches for the new Type in the Type   *
*                             Table Array.                                    *
*      Formal Parameters    : pi1_newtype                                     *
*      Global Variables     : pi1_newtype , i4_global_newcount                *
*      Use of Recursion     : None                                            *
*      Return Value         : Index of the New Type if Present Else -1        *
******************************************************************************/
INT4
search_new_data_type (UINT1 *pi1_newtype)
{
    INT4                i4_count;

    /*  
     *  For loop for finding the Index of the New Data type
     *  in the Global Char Array name_new_datatype.
     */
    for (i4_count = ZERO; i4_count < i4_global_newcount; i4_count++)
    {
        if (!strcmp (pi1_newtype, name_new_datatype[i4_count]))
        {
            return i4_count;
        }
    }
    return NEGATIVE;

}                                /* End of function */

/******************************************************************************
*      function Name        : declarations_for_new_datatype                   *
*      Role of the function : This Fn Generates the DECLARATIONS for New Types*
*      Formal Parameters    : i4_index  , i4_count                            *
*      Global Variables     : type_table                                      *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
declarations_for_new_datatype (INT4 i4_index, INT4 i4_count)
{
    /*  
     *  Generating the Code which is the Declaration  
     *  of the vars needed for New Types.
     */
    switch (type_table[i4_index].primitive_type)
    {
        case POS_INTEGER:
        {
            fprintf (fp, "\n    UINT4 u4_%s_%s = FALSE;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp, "\n    UINT4 u4_%s_next_%s = FALSE;\n",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case INTEGER:
        {
            fprintf (fp, "\n    INT4 i4_%s_%s = FALSE;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp, "\n    INT4 i4_%s_next_%s = FALSE;\n",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case OBJECT_IDENTIFIER:
        {
            fprintf (fp,
                     "\n    /*  The Variable Which Store the Length of the OID. */");
            fprintf (fp, "\n    INT4 i4_len_%s_index_%s;\n",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp, "\n    tSNMP_OID_TYPE * %s_%s = NULL;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp, "\n    tSNMP_OID_TYPE * %s_next_%s = NULL;\n",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case OCTET_STRING:
        {
            fprintf (fp, "\n    /*");
            fprintf (fp, "\n     *  The tSNMP_OCTET_STRING_TYPE Which Store");
            fprintf (fp, "\n     *  the Length and Octet String.");
            fprintf (fp, "\n     */");
            fprintf (fp, "\n    INT4  i4_len_%s_index_%s = FALSE;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp, "\n    tSNMP_OCTET_STRING_TYPE * p%s_%s = NULL;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "\n    tSNMP_OCTET_STRING_TYPE * p%s_next_%s = NULL;\n",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case MacAddress:
        {
            /* Declaring the Variables for Mac Address (len 6 bytes) */
            fprintf (fp, "\n    /*");
            fprintf (fp,
                     "\n     *  The Declaration of the struct which stores ");
            fprintf (fp, "\n     *  the Mac Address of len Six bytes.");
            fprintf (fp, "\n     */");

            fprintf (fp, "\n    tMacAddr  %s_%s;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp, "\n    tMacAddr  %s_next_%s;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);

            break;
        }
        case ADDRESS:
        {
            /* 
             *  Declaring the Variables for IP Address,
             *  Physical Address Network Address (len 4 bytes)
             */
            fprintf (fp, "\n    /*");
            fprintf (fp,
                     "\n     *  The Declaration of the Structure which stores the");
            fprintf (fp, "\n     *  Address of len FOUR bytes.");
            fprintf (fp, "\n     */");
            fprintf (fp, "\n    UINT4  u4_%s_%s = FALSE;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp, "\n    UINT4  u4_%s_next_%s = FALSE;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);

            fprintf (fp, "\n    UINT1 u1_%s_%s[ADDR_LEN] = NULL_STRING;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp, "\n    UINT1 u1_%s_next_%s[ADDR_LEN] = NULL_STRING;\n",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        default:
            break;
    }                            /*  End of Switch */

}                                /* End of Function */

/******************************************************************************
*      function Name        : declare_retval_for_new_datatype                 *
*      Role of the function : This Fn Generates the DECLARATIONSof the Return *
*                             Values for NEW Types.                           *
*      Formal Parameters    : i4_index , i4_count                             *
*      Global Variables     : type_table                                      *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
declare_retval_for_new_datatype (int i4_index, int i4_count)
{
    switch (type_table[i4_index].primitive_type)
    {
        case POS_INTEGER:
        case INTEGER:
        case OBJECT_IDENTIFIER:
            break;
        case OCTET_STRING:
        {
            fprintf (fp,
                     "\n    tSNMP_OCTET_STRING_TYPE * p%s_retval_%s = NULL;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case ADDRESS:
        {
            fprintf (fp, "\n    UINT4 u4_%s_ret_val_%s;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case MacAddress:
        {
            fprintf (fp, "\n    tMacAddr %s_ret_val_%s;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        default:
            break;
    }                            /* End Of Switch. */

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : extracting_indices_for_new_datatype             *
*      Role of the function : This fn Generates the Code which extracts the   *
*                             Indices from the given Oid for New Types & also *
*                             generates the code which converts them to the   *
*                             destination Type which is passed to the Low Lev *
*                             Routines.                                       *
*      Formal Parameters    : i4_index , i4_count                             *
*      Global Variables     : type_table                                      *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
extracting_indices_for_new_datatype (INT4 i4_index, INT4 i4_count)
{
    switch (type_table[i4_index].primitive_type)
    {
        case POS_INTEGER:
        {
            fprintf (fp, "\n             /* Extracting The Integer Index. */");
            fprintf (fp,
                     "\n             u4_%s_%s = (UINT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp, "\n             i4_offset++ ;\n");
            break;
        }
        case INTEGER:
        {
            fprintf (fp, "\n             /* Extracting The Integer Index. */");
            fprintf (fp,
                     "\n             i4_%s_%s = (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp, "\n             i4_offset++ ;\n");
            break;
        }
        case OBJECT_IDENTIFIER:
        {
            fprintf (fp, "\n             /*");
            fprintf (fp,
                     "\n              *  Incrementing the Array Pointer to the location ");
            fprintf (fp,
                     "\n              *  which Contains the Length of the Index.");
            fprintf (fp, "\n              */");
            fprintf (fp, "\n             i4_offset++;");

            fprintf (fp,
                     "\n             for(i4_count = FALSE ; i4_count < i4_len_%s_index_%s ; i4_count++ , i4_offset++)",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "\n                 %s_%s->pu4_OidList[i4_count] = (UINT4) ",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];");
            break;
        }
        case OCTET_STRING:
        {
            /* if condition remove to fix incorrect extraction of
             * OCTET STRING type index from incoming OID
             * changed by soma on 16/06/98
             */
            fprintf (fp, "\n             /*");
            fprintf (fp,
                     "\n              *  Incrementing the Array Pointer to the");
            fprintf (fp,
                     "\n              *  location which Contains the Length of the Index.");
            fprintf (fp, "\n              */");
            fprintf (fp, "\n             i4_offset++;");
            fprintf (fp,
                     "\n             /*  The FOR LOOP for extracting the Indices from the Given OID. */");
            fprintf (fp,
                     "\n             for(i4_count = FALSE ; i4_count < i4_len_%s_index_%s ; i4_count++ , i4_offset++)",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "\n                p%s_%s->pu1_OctetList[i4_count] = (UINT1) ",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset] ;\n");
            break;
        }
        case ADDRESS:
        {
            fprintf (fp,
                     "\n             /*  FOR Loop for extracting the Indices of Type Addr from the Given OID. */");

            fprintf (fp,
                     "\n             for(i4_count = FALSE ; i4_count < ADDR_LEN ; i4_count++ , i4_offset++)");
            fprintf (fp,
                     "\n                 u1_%s_%s[i4_count] = (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "\n             /*  This Library Fn converts the Char Array into a Word of 4 Bytes. */");
            fprintf (fp,
                     "\n             u4_%s_%s = OSIX_NTOHL(*((UINT4 *)(u1_%s_%s)));\n",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name,
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case MacAddress:
        {
            fprintf (fp,
                     "\n             /*  FOR Loop for extracting the Indices of Type MacAddress from the Given OID. */");
            fprintf (fp,
                     "\n             for(i4_count = FALSE ; i4_count < MAC_ADDR_LEN ; i4_count++ , i4_offset++)");
            fprintf (fp,
                     "\n                 %s_%s[i4_count] = (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);

            break;
        }
        default:
            break;
    }                            /*  End of Switch */

}                                /* End of Function */

/******************************************************************************
*      function Name        : passargs_to_lowlev_for_new_datatype             *
*      Role of the function : This fn Generates the Code which passes the     *
*                             Indices to the Low Lev fn for New Types. This fn*
*                             is used only in GET EXACT & get all objects.    *
*      Formal Parameters    : i4_index , i4_count                             *
*      Global Variables     : type_table                                      *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
passargs_to_lowlev_for_new_datatype (INT4 i4_index, INT4 i4_count)
{
    switch (type_table[i4_index].primitive_type)
    {
        case POS_INTEGER:
        {
            fprintf (fp, " u4_%s_%s", type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case INTEGER:
        {
            fprintf (fp, " i4_%s_%s", type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case OCTET_STRING:
        {
            fprintf (fp, " p%s_%s", type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case OBJECT_IDENTIFIER:
        {
            fprintf (fp, " %s_%s", type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case ADDRESS:
        {
            fprintf (fp, " u4_%s_%s", type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case MacAddress:
        {
            fprintf (fp, " %s_%s", type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        default:
            break;
    }                            /*  End of Switch */

}                                /* End of Function */

/******************************************************************************
*      function Name        : passargs_to_lowlev_for_new_datatype_getfirst    *
*      Role of the function : This fn Generates the Code which passes the     *
*                             Indices to the Low Lev fn for New Types for GET *
*                             FIRST Fn. This Fn is used only in GET EXACT     *
*                             & get all objects fn.                           *
*      Formal Parameters    : i4_index , i4_count                             *
*      Global Variables     : type_table                                      *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
passargs_to_lowlev_for_new_datatype_getfirst (INT4 i4_index, INT4 i4_count)
{
    switch (type_table[i4_index].primitive_type)
    {
        case POS_INTEGER:
        {
            fprintf (fp, " &u4_%s_%s", type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case INTEGER:
        {
            fprintf (fp, " &i4_%s_%s", type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case OCTET_STRING:
        {
            fprintf (fp, " p%s_%s", type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case OBJECT_IDENTIFIER:
        {
            fprintf (fp, " %s_%s", type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case ADDRESS:
        {
            fprintf (fp, " &u4_%s_%s", type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case MacAddress:
        {
            fprintf (fp, " &%s_%s", type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        default:
            break;
    }                            /*  End of Switch */

}                                /* End of Function */

/******************************************************************************
*      function Name        : passargs_to_lowlev_getnext_new_datatype         *
*      Role of the function : This fn Generates the Code which passes the     *
*                             Indices to the Low Lev fn for New Types for GET *
*                             NEXT Fn. This Fn is used only in GET NEXT.      *
*      Formal Parameters    : i4_index , i4_count                             *
*      Global Variables     : type_table                                      *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
passargs_to_lowlev_getnext_new_datatype (INT4 i4_index, INT4 i4_count)
{
    switch (type_table[i4_index].primitive_type)
    {
        case POS_INTEGER:
        {
            fprintf (fp, "u4_%s_%s ", type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp, ", &u4_%s_next_%s", type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case INTEGER:
        {
            fprintf (fp, "i4_%s_%s ", type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp, ", &i4_%s_next_%s", type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case OCTET_STRING:
        {
            fprintf (fp, "p%s_%s ", type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp, ", p%s_next_%s", type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case OBJECT_IDENTIFIER:
        {
            fprintf (fp, " %s_%s ", type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp, ", %s_next_%s", type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case ADDRESS:
        {
            fprintf (fp, " u4_%s_%s ", type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp, ", &u4_%s_next_%s", type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case MacAddress:
        {
            fprintf (fp, " %s_%s ", type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp, ", &%s_next_%s", type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        default:
            break;
    }                            /*  End of Switch */

}                                /* End of Function */

/******************************************************************************
*      function Name        : storeback_OID_for_new_datatype                  *
*      Role of the function : This fn Generates the Code which puts back the  *
*                             new Oid got from GET FIRST fn to the p_in_db.   *
*      Formal Parameters    : i4_index , i4_count                             *
*      Global Variables     : type_table                                      *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
storeback_OID_for_new_datatype (INT4 i4_index, INT4 i4_count)
{
    switch (type_table[i4_index].primitive_type)
    {
        case POS_INTEGER:
        {
            fprintf (fp,
                     "\n                    p_in_db->pu4_OidList[p_in_db->u4_Length++] = u4_%s_%s;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case INTEGER:
        {
            fprintf (fp,
                     "\n                    p_in_db->pu4_OidList[p_in_db->u4_Length++] = (UINT4) i4_%s_%s;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case OBJECT_IDENTIFIER:
        {
            fprintf (fp,
                     "\n                     /*  Storing the Length of the OID. */");
            fprintf (fp,
                     "\n                     p_in_db->pu4_OidList[p_in_db->u4_Length++] = %s_%s->u4_Length;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp, "\n                     /*  Storing the OID. */");
            fprintf (fp,
                     "\n                     for(i4_count = FALSE ; i4_count < %s_%s->u4_Length ; i4_count++)",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "\n                         p_in_db->pu4_OidList[p_in_db->u4_Length++] = %s_%s->pu4_OidList[i4_count];",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case OCTET_STRING:
        {
            /*  
             *  FOR loop for storing the octet string which is 
             *  got from the low level routine to the p_in_db.
             */
            fprintf (fp, "\n                     /* Storing the Length. */");
            /*
             * if condition removed to fix incorrect extraction of
             * OCTET STRING type index from incoming OID
             * changed by soma on 16/06/98
             */
            fprintf (fp,
                     "\n                     p_in_db->pu4_OidList[p_in_db->u4_Length++] = p%s_%s->i4_Length;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "\n                     /* The FOR LOOP for storing the Index in p_in_db. */");
            fprintf (fp,
                     "\n                     for(i4_count = FALSE ; i4_count < p%s_%s->i4_Length ; i4_count++)",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "\n                          p_in_db->pu4_OidList[p_in_db->u4_Length++] = (UINT4) p%s_%s->pu1_OctetList[i4_count];",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case ADDRESS:
        {
            fprintf (fp, "\n                     /*");
            fprintf (fp,
                     "\n                      *  This part of the Code Converts the content of the");
            fprintf (fp,
                     "\n                      *  NetWorkAddr struct to the char array.");
            fprintf (fp, "\n                      */");
            fprintf (fp,
                     "\n                     *((UINT4 *)(u1_%s_%s )) = OSIX_HTONL(u4_%s_%s);\n",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name,
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "\n                     for(i4_count = FALSE ; i4_count < ADDR_LEN ; i4_count++)");
            fprintf (fp,
                     "\n                         p_in_db->pu4_OidList[p_in_db->u4_Length++] = u1_%s_%s[i4_count];\n",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case MacAddress:
        {
            fprintf (fp, "\n                    /*");
            fprintf (fp,
                     "\n                     *  This FOR loop is for storing the content of the");
            fprintf (fp,
                     "\n                     *  MacAddress struct into the p_in_db.\n");
            fprintf (fp, "\n                     */");
            fprintf (fp,
                     "\n                    for(i4_count = FALSE; i4_count < MAC_ADDR_LEN ; i4_count++)");
            fprintf (fp,
                     "\n                        p_in_db->pu4_OidList[p_in_db->u4_Length++] = %s_%s[i4_count];",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);

            break;
        }
        default:
            break;
    }                            /*  End of Switch */

}                                /* End of Function */

/******************************************************************************
*      function Name        : storeback_OID_for_new_datatype_exact            *
*      Role of the function : This fn Generates the Code which puts back the  *
*                             new Oid in p_in_db for EXACT case .             *
*      Formal Parameters    : i4_index , i4_count                             *
*      Global Variables     : type_table , p_table_struct                                     *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
storeback_OID_for_new_datatype_exact (INT4 i4_index, INT4 i4_count)
{
    switch (type_table[i4_index].primitive_type)
    {
        case POS_INTEGER:
        {
            fprintf (fp,
                     "\n             p_in_db->pu4_OidList[p_in_db->u4_Length++] = u4_%s_%s;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case INTEGER:
        {
            fprintf (fp,
                     "\n             p_in_db->pu4_OidList[p_in_db->u4_Length++] = (UINT4) i4_%s_%s;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case OBJECT_IDENTIFIER:
        {
            fprintf (fp,
                     "\n              /*  Storing the Length of the OID. Get Exact.  */");
            fprintf (fp,
                     "\n              p_in_db->pu4_OidList[p_in_db->u4_Length++] = %s_%s->u4_Length;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "\n              for(i4_count = FALSE ; i4_count < %s_%s->u4_Length ; i4_count++)",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "\n                  p_in_db->pu4_OidList[p_in_db->u4_Length++] = %s_%s->pu4_OidList[i4_count];",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case OCTET_STRING:
        {
            /*  
             *  FOR loop for storing the octet string which is 
             *  got from the low level routine to the p_in_db.
             */
            fprintf (fp,
                     "\n              /*  Storing the Length of the Octet Str Get Exact.  */");
            /*
             * if condition removed to fix incorrect extraction of
             * OCTET STRING type index from incoming OID
             * changed by soma on 16/06/98
             */
            fprintf (fp,
                     "\n              p_in_db->pu4_OidList[p_in_db->u4_Length++] = p%s_%s->i4_Length;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "\n              /* The FOR LOOP for storing the Index in p_in_db. */");
            fprintf (fp,
                     "\n              for(i4_count = FALSE ; i4_count < p%s_%s->i4_Length ; i4_count++)",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "\n                   p_in_db->pu4_OidList[p_in_db->u4_Length++] = (UINT4) p%s_%s->pu1_OctetList[i4_count];",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case ADDRESS:
        {
            fprintf (fp,
                     "\n              for(i4_count = FALSE ; i4_count < ADDR_LEN ; i4_count++)");
            fprintf (fp,
                     "\n                  p_in_db->pu4_OidList[p_in_db->u4_Length++] = u1_%s_%s[i4_count];\n",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case MacAddress:
        {
            fprintf (fp, "\n             /*");
            fprintf (fp,
                     "\n              *  This FOR loop is for storing the content of the");
            fprintf (fp,
                     "\n              *  MacAddress struct into the p_in_db.\n");
            fprintf (fp, "\n              */");
            fprintf (fp,
                     "\n             for(i4_count = FALSE; i4_count < MAC_ADDR_LEN ; i4_count++)");
            fprintf (fp,
                     "\n                 p_in_db->pu4_OidList[p_in_db->u4_Length++] = %s_%s[i4_count];",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        default:
            break;
    }                            /*  End of Switch */

}                                /* End of Function */

/******************************************************************************
*      function Name        : storeback_OID_getnext_for_new_datatype          *
*      Role of the function : This fn Generates the Code which puts back the  *
*                             new Oid got from GET NEXT fn to the p_in_dbi    *
*                             for New Type.                                   *
*      Formal Parameters    : i4_index , i4_count                             *
*      Global Variables     : type_table                                      *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
storeback_OID_getnext_new_datatype (INT4 i4_index, INT4 i4_count)
{
    switch (type_table[i4_index].primitive_type)
    {
        case POS_INTEGER:
        {
            fprintf (fp, "\n                   u4_%s_%s = u4_%s_next_%s;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name,
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "\n                   p_in_db->pu4_OidList[p_in_db->u4_Length++] = u4_%s_next_%s;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case INTEGER:
        {
            fprintf (fp, "\n                   i4_%s_%s = i4_%s_next_%s;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name,
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "\n                   p_in_db->pu4_OidList[p_in_db->u4_Length++] = (UINT4) i4_%s_next_%s;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case OBJECT_IDENTIFIER:
        {
            fprintf (fp,
                     "\n                   /*  Storing the Length of the Oid in p_in_db.  */");
            fprintf (fp,
                     "\n                   p_in_db->pu4_OidList[p_in_db->u4_Length++] = %s_next_%s->u4_Length;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "\n                   for(i4_count = FALSE ; i4_count < %s_next_%s->u4_Length ; i4_count++)",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "\n                       p_in_db->pu4_OidList[p_in_db->u4_Length++] = %s_next_%s->pu4_OidList[i4_count] ;\n",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);

            /*  
             *  Assigning the Next Oid to the First
             *  Oid after Freeing the First Oid.
             */
            fprintf (fp,
                     "\n                   /*  Freeing the First Oid and Filling with the Next Oid. */");
            fprintf (fp, "\n                   free_oid(%s_%s);",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "\n                   /*  Assigning the Next Oid to First Oid. */");
            fprintf (fp, "\n                   %s_%s = %s_next_%s;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name,
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);

            break;
        }
        case OCTET_STRING:
        {
            fprintf (fp,
                     "\n                   /*  Storing the Length of the Octet String.  */");
            /*
             * if condition removed to fix incorrect extraction of
             * OCTET STRING type index from incoming OID
             * changed by soma on 16/06/98
             */
            fprintf (fp,
                     "\n                   p_in_db->pu4_OidList[p_in_db->u4_Length++] = p%s_next_%s->i4_Length;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "\n                   /*  The FOR LOOP for storing the indices to the p_in_db. */\n");
            fprintf (fp,
                     "\n                   for(i4_count = FALSE ; i4_count < p%s_next_%s->i4_Length ; i4_count++)",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "\n                      p_in_db->pu4_OidList[p_in_db->u4_Length++] = (UINT4) p%s_next_%s->pu1_OctetList[i4_count];",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);

            /*  
             *  Assigning the Next Octet String to the First
             *  after freeing the First Octest String. 
             */
            fprintf (fp, "\n                   free_octetstring(p%s_%s);",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "\n                   /*  Assigning the Next Octet String to the First. */");
            fprintf (fp, "\n                   p%s_%s = p%s_next_%s;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name,
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case ADDRESS:
        {
            fprintf (fp, "\n                   /*");
            fprintf (fp,
                     "\n                    *  This part of the Code Converts the content of");
            fprintf (fp,
                     "\n                    *  the NetWorkAddr struct to Char Array.");
            fprintf (fp, "\n                    */");
            fprintf (fp, "\n                   u4_%s_%s = u4_%s_next_%s;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name,
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "\n                   *((UINT4 *)(u1_%s_next_%s)) = OSIX_HTONL(u4_%s_next_%s);\n ",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name,
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "\n                   for(i4_count = FALSE ; i4_count < ADDR_LEN ; i4_count++)");
            fprintf (fp,
                     "\n                       p_in_db->pu4_OidList[p_in_db->u4_Length++] = u1_%s_next_%s[i4_count];\n",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case MacAddress:
        {

            fprintf (fp, "\n                   /*");
            fprintf (fp,
                     "\n                    *  This FOR loop is for storing the content of");
            fprintf (fp,
                     "\n                    *  the MacAddr struct into the p_in_db.");
            fprintf (fp, "\n                    */");
            fprintf (fp,
                     "\n                   for(i4_count = FALSE; i4_count < MAC_ADDR_LEN ; i4_count++)");
            fprintf (fp,
                     "\n                       p_in_db->pu4_OidList[p_in_db->u4_Length++] = %s_next_%s[i4_count];",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);

            fprintf (fp,
                     "\n                   %s_%s.u4Dword = %s_next_%s.u4Dword;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name,
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "\n                   %s_%s.u2Word = %s_next_%s.u2Word;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name,
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        default:
            break;
    }                            /*  End of Switch */

}                                /* End of Function. */

/******************************************************************************
*      function Name        : fill_args_to_formvarbind                        *
*      Role of the function : This fn Generates the Code for storing the Vals *
*                             got from low lev fns to the Form Varbind Fns    *
*                             for New Type.                                   *
*      Formal Parameters    : i4_index , i4_count                             *
*      Global Variables     : type_table                                      *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
fill_args_to_formvarbind (INT4 i4_index, INT4 i4_count)
{
    switch (type_table[i4_index].primitive_type)
    {
        case POS_INTEGER:
        {
            if (strcmp
                (p_table_struct->object[i4_count].type_name,
                 STR_COUNTER32_TYPE) == 0)
                fprintf (fp,
                         "\n             i2_type = SNMP_DATA_TYPE_COUNTER32;");
            else if (strcmp
                     (p_table_struct->object[i4_count].type_name,
                      STR_COUNTER64_TYPE) == 0)
                fprintf (fp,
                         "\n             i2_type = SNMP_DATA_TYPE_COUNTER64;");
            else if (strcmp
                     (p_table_struct->object[i4_count].type_name,
                      STR_UNSIGNED32_TYPE) == 0)
                fprintf (fp,
                         "\n             i2_type = SNMP_DATA_TYPE_UNSIGNED32;");
            else if (strcmp
                     (p_table_struct->object[i4_count].type_name,
                      STR_GAUGE32_TYPE) == 0)
                fprintf (fp, "\n             i2_type = SNMP_DATA_TYPE_GAUGE;");
            else if (strcmp
                     (p_table_struct->object[i4_count].type_name,
                      STR_TIMETICKS_TYPE) == 0)
                fprintf (fp,
                         "\n             i2_type = SNMP_DATA_TYPE_TIME_TICKS;");
            break;
        }
        case INTEGER:
        {
            fprintf (fp, "\n             i2_type = SNMP_DATA_TYPE_INTEGER;");
            break;
        }
        case OBJECT_IDENTIFIER:
        {
            fprintf (fp, "\n             i2_type = SNMP_DATA_TYPE_OBJECT_ID;");
            break;
        }
        case OCTET_STRING:
        {
            fprintf (fp, "\n             i2_type = SNMP_DATA_TYPE_OCTET_PRIM;");
            fprintf (fp,
                     "\n\n           /*  Assigning the Octet Str Ptr Got >From Low Level Rtns. */\n");
            fprintf (fp, "\n             poctet_string = p%s_retval_%s;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case ADDRESS:
        {
            fprintf (fp,
                     "\n              i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;");
            fprintf (fp,
                     "\n              /* This part of the Code is to convert the Address to Octet String. */");

            fprintf (fp,
                     "\n              *((UINT4 *)(u1_octet_string)) = OSIX_HTONL(u4_%s_ret_val_%s);\n",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "\n              poctet_string = (tSNMP_OCTET_STRING_TYPE *) SNMP_AGT_FormOctetString(u1_octet_string , ADDR_LEN);");
            break;
        }
        case MacAddress:
        {
            fprintf (fp,
                     "\n              i2_type = SNMP_DATA_TYPE_OCTET_PRIM;");

            fprintf (fp,
                     "\n              poctet_string = (tSNMP_OCTET_STRING_TYPE *) SNMP_AGT_FormOctetString(%s_ret_val_%s, MAC_ADDR_LEN);",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);

            break;
        }
        default:
            break;
    }                            /*  End of Switch */

}                                /* End of Function. */

/******************************************************************************
*      function Name        : passing_retval_for_new_datatype                 *
*      Role of the function : This fn Generates the Code for passing the      *
*                             Return Vals to low lev fns accoring to the type *
*                             for New Type.                                   *
*      Formal Parameters    : i4_index , i4_count                             *
*      Global Variables     : type_table                                      *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
passing_retval_for_new_datatype (INT4 i4_index, INT4 i4_count)
{
    switch (type_table[i4_index].primitive_type)
    {
        case POS_INTEGER:
        {
            fprintf (fp, "&u4_counter_val");
            break;
        }
        case INTEGER:
        {
            fprintf (fp, "&i4_return_val");
            break;
        }
        case OCTET_STRING:
        {
            fprintf (fp, "p%s_retval_%s", type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case OBJECT_IDENTIFIER:
        {
            fprintf (fp, "pOidValue");
            break;
        }
        case ADDRESS:
        {
            fprintf (fp, "&u4_%s_ret_val_%s", type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case MacAddress:
        {
            fprintf (fp, "&%s_ret_val_%s", type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        default:
            break;

    }                            /*  End Of Switch  */

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : extract_valfrom_multi_for_new_datatype          *
*      Role of the function : This fn Generates the Code for extracting the   *
*                             Values to be sent to the low lev fns from the   *
*                             Multi Data Type for New Type.                   *
*      Formal Parameters    : i4_index , i4_count                             *
*      Global Variables     : type_table                                      *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
extract_valfrom_multi_for_new_datatype (INT4 i4_index, INT4 i4_count)
{
    switch (type_table[i4_index].primitive_type)
    {

        case POS_INTEGER:
        case INTEGER:
        case OBJECT_IDENTIFIER:
        case OCTET_STRING:
            break;
        case ADDRESS:
        {
            fprintf (fp,
                     "\n                 for(i4_offset = FALSE ; i4_offset < ADDR_LEN ;  i4_offset++)");
            fprintf (fp,
                     "\n                     u1_octet_string[i4_offset] = p_value->pOctetStrValue->pu1_OctetList[i4_offset];");
            fprintf (fp,
                     "\n                 u4_%s_val_%s = OSIX_NTOHL(*((UINT4 *)(u1_octet_string)));",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case MacAddress:
        {
            fprintf (fp,
                     "\n                 for(i4_offset = FALSE ; i4_offset < MAC_ADDR_LEN ;  i4_offset++)");
            fprintf (fp,
                     "\n                     %s_val_%s[i4_offset] = p_value->pOctetStrValue->pu1_OctetList[i4_offset];",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);

            break;
        }

    }                            /*  End of SWITCH  */

}                                /*  End of Function */

/******************************************************************************
*      function Name        : assign_len_of_variablelen_index_new_datatype    *
*      Role of the function : This fn Generates the Code for assigning the    *
*                             length of the Var length Indices from the Given *
*                             Ois for New Types.                              *
*      Formal Parameters    : i4_index , i4_count                             *
*      Global Variables     : type_table                                      *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
assign_len_of_variablelen_index_new_datatype (INT4 i4_index, INT4 i4_count)
{
    switch (type_table[i4_index].primitive_type)
    {
        case POS_INTEGER:
        case INTEGER:
        {
            fprintf (fp, "\n        i4_size_offset += INTEGER_LEN;");
            break;
        }
        case OCTET_STRING:
        {
            /*
             * if condition removed to fix the incorrect
             * extraction of OCTET STRING type index
             * changed by soma on 06/06/98
             */
            fprintf (fp,
                     "\n        i4_len_%s_index_%s = p_incoming->pu4_OidList[i4_size_offset++];",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp, "\n        i4_size_offset += i4_len_%s_index_%s;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case OBJECT_IDENTIFIER:
        {
            fprintf (fp,
                     "\n        i4_len_%s_index_%s = p_incoming->pu4_OidList[i4_size_offset++];",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp, "\n        i4_size_offset += i4_len_%s_index_%s;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case ADDRESS:
        {
            fprintf (fp, "\n        i4_size_offset += ADDR_LEN;");
            break;
        }
        case MacAddress:
        {
            fprintf (fp, "\n        i4_size_offset += MAC_ADDR_LEN;");
            break;
        }
        default:
            break;
    }                            /* End Of SWITCH Case. */

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : add_len_of_index_new_datatype                   *
*      Role of the function : This fn Generates the Code which find the Length*
*                             of the Var length Indices for New Types and Adds*
*                             it cummulatively ot the Var used for extracting *
*                             a Index ( i.e. i4_partial_index_len)            *
*      Formal Parameters    : i4_index , i4_count                             *
*      Global Variables     : type_table                                      *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
add_len_of_index_new_datatype (INT4 i4_index, INT4 i4_count)
{
    switch (type_table[i4_index].primitive_type)
    {
        case INTEGER:
        case POS_INTEGER:
        {
            fprintf (fp,
                     "\n              i4_partial_index_len += INTEGER_LEN ;");
            break;
        }
        case ADDRESS:
        {
            fprintf (fp, "\n              i4_partial_index_len += ADDR_LEN ;");
            break;
        }
        case MacAddress:
        {
            fprintf (fp,
                     "\n              i4_partial_index_len += MAC_ADDR_LEN ;");
            break;
        }
        case OCTET_STRING:
        {
            /*
             * if condiiton remove to fix incorrect extraction of
             * OCTET STRING type index
             * changed by soma on 16/06/98
             */
            fprintf (fp,
                     "\n              i4_len_%s_index_%s = p_incoming->pu4_OidList[i4_partial_index_len++] ;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "\n              i4_partial_index_len += i4_len_%s_index_%s ;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            /* code change ends here -soma */
            break;
        }
        case OBJECT_IDENTIFIER:
        {
            fprintf (fp,
                     "\n              i4_len_%s_index_%s = p_incoming->pu4_OidList[i4_partial_index_len++] ;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "\n              i4_partial_index_len += i4_len_%s_index_%s ;",
                     type_table[i4_index].type_name,
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        default:
            break;
    }                            /* End Of SWITCH Case. */

}                                /* End Of Function. */

/*******************************************************************************
*    function Name        : passing_args_to_formvarbind_for_index_new_datatype *
*    Role of the function : This fn Generates the Code for storing the Val of  *
*                           Extracted into the Vars of the FormVarbind fn      *
*                           According to the Type for New Type.                *
*     Formal Parameters    : i4_index , i4_count , i4_get_flag                 *
*     Global Variables     : type_table                                        *
*     Use of Recursion     : None                                              *
*     Return Value         : None.                                             *
*******************************************************************************/
void
passing_args_to_formvarbind_for_index_new_datatype (INT4 i4_index,
                                                    INT4 i4_count,
                                                    INT4 i4_get_flag)
{
    switch (type_table[i4_index].primitive_type)
    {
        case POS_INTEGER:
        {
            if (i4_get_flag == GET_FIRST_FLAG)
                fprintf (fp, "\n             u4_counter_val = u4_%s_%s ;",
                         type_table[i4_index].type_name,
                         p_table_struct->object[i4_count].object_name);
            else if (i4_get_flag == GET_NEXT_FLAG)
                fprintf (fp, "\n             u4_counter_val = u4_%s_next_%s ;",
                         type_table[i4_index].type_name,
                         p_table_struct->object[i4_count].object_name);
            break;
        }
        case INTEGER:
        {
            if (i4_get_flag == GET_FIRST_FLAG)
                fprintf (fp, "\n             i4_return_val = i4_%s_%s ;",
                         type_table[i4_index].type_name,
                         p_table_struct->object[i4_count].object_name);
            else if (i4_get_flag == GET_NEXT_FLAG)
                fprintf (fp, "\n             i4_return_val = i4_%s_next_%s ;",
                         type_table[i4_index].type_name,
                         p_table_struct->object[i4_count].object_name);
            break;
        }
        case OCTET_STRING:
        {
            fprintf (fp, "\n             poctet_string = ");
            if (i4_get_flag == GET_FIRST_FLAG)
                fprintf (fp, "p%s_%s;", type_table[i4_index].type_name,
                         p_table_struct->object[i4_count].object_name);
            else if (i4_get_flag == GET_NEXT_FLAG)
            {
                fprintf (fp, "p%s_next_%s;", type_table[i4_index].type_name,
                         p_table_struct->object[i4_count].object_name);
            }
            break;
        }
        case OBJECT_IDENTIFIER:
        {
            if (i4_get_flag == GET_FIRST_FLAG)
                fprintf (fp, "\n             pOidValue = %s_%s;",
                         type_table[i4_index].type_name,
                         p_table_struct->object[i4_count].object_name);
            if (i4_get_flag == GET_NEXT_FLAG)
                fprintf (fp, "\n             pOidValue = %s_next_%s;",
                         type_table[i4_index].type_name,
                         p_table_struct->object[i4_count].object_name);
            break;
        }
        case ADDRESS:
        {
            fprintf (fp, "\n             *((UINT4 *)(u1_octet_string)) ");
            if (i4_get_flag == GET_FIRST_FLAG)
                fprintf (fp, "= OSIX_HTONL (u4_%s_%s);",
                         type_table[i4_index].type_name,
                         p_table_struct->object[i4_count].object_name);
            else if (i4_get_flag == GET_NEXT_FLAG)
                fprintf (fp, "= OSIX_HTONL (u4_%s_next_%s);",
                         type_table[i4_index].type_name,
                         p_table_struct->object[i4_count].object_name);
            fprintf (fp,
                     "\n             poctet_string = (tSNMP_OCTET_STRING_TYPE *) SNMP_AGT_FormOctetString(u1_octet_string , ADDR_LEN);");
            break;
        }
        case MacAddress:
        {
            fprintf (fp,
                     "\n                poctet_string = (tSNMP_OCTET_STRING_TYPE *) SNMP_AGT_FormOctetString");
            if (i4_get_flag == GET_FIRST_FLAG)
                fprintf (fp, "(%s_%s , MAC_ADDR_LEN);",
                         type_table[i4_index].type_name,
                         p_table_struct->object[i4_count].object_name);
            else if (i4_get_flag == GET_NEXT_FLAG)
                fprintf (fp, "(%s_next_%s , MAC_ADDR_LEN);",
                         type_table[i4_index].type_name,
                         p_table_struct->object[i4_count].object_name);
            break;
        }

    }                            /*  End Of Switch Case. */

}                                /* End Of Function. */
