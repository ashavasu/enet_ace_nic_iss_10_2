/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: proto.h,v 1.6 2015/04/28 14:43:31 siva Exp $
 *
 * Description:This File contains the Proto Type Declarations  
 *             needed for the Mid Level Code Generator.
 *******************************************************************/
		

INT4
parser_module PROTO ((t_MIB_TABLE * , FILE *));

INT4
check_presenceof_index PROTO ((void));

extern INT4
find_object_is_index PROTO ((UINT1 *));

INT4
find_index_type_OIDorOctet PROTO ((INT4));

void
generator_module PROTO((void));

void 
RegisterMibTablewise PROTO((void));

void 
code_generator_mid_module PROTO ((FILE *));
 
void 
prototype_generator_mid_module PROTO ((FILE *));

void 
mem_allocate PROTO ((void));

void
validate_command_line PROTO(());

INT4
process_command_line PROTO((INT4 , INT1 * [] , INT4 *, INT1 *, INT2 *));

void
check_invalid_option PROTO((INT4 , INT4 , INT4 ));

INT4
find_version PROTO((INT4 , UINT1 * [])); 

INT4
find_dirname PROTO((INT4 , UINT1 * []));

INT4
make_dir PROTO((UINT1 *));

void
create_directory_structure PROTO((UINT1 *));

UINT1*
get_pathname PROTO((void));

UINT1* 
get_line PROTO ((FILE *));

INT4 
check_empty_line PROTO ((UINT1 *));

UINT1* 
remove_comment PROTO ((UINT1 *));

INT4  
go_down PROTO ((FILE *));

UINT1*
get_table_name PROTO ((FILE *));

INT4 
get_index PROTO ((FILE *));

INT4      
separate_indices PROTO ((UINT1 *));

UINT1*      
strupper PROTO ((UINT1 *));

INT4
get_no_of_objects PROTO ((FILE *));

INT4     
get_objects PROTO ((FILE *));

void
get_import_index PROTO ((FILE *));

void
get_position PROTO ((FILE *));

void     
initialize_type_table PROTO ((UINT1 *));

void     
get_components_of_struct PROTO ((FILE *));

void     
find_datatype_of_element PROTO ((UINT1 * , INT4));

UINT1*     
search_dtype PROTO ((UINT1 * , FILE *));

void
store_object_type PROTO ((UINT1* , INT4));

void
get_entry_name PROTO ((FILE *));

void
declaration_of_returnvalue PROTO ((FILE *));

void
declaration_of_value PROTO ((FILE *));

void
declaration_of_common_variables PROTO ((FILE *));

void
declaration_of_form_varbind PROTO ((FILE *));

void
declaration_for_getroutine PROTO ((FILE *));

void
declaration_scalar_table_getroutine PROTO ((FILE *));

void 
alloc_mem_for_oid_getexact_first PROTO ((FILE * , INT4));

void 
fn_checking_for_malloc_failure_oid PROTO ((FILE *));

void 
fn_checking_for_malloc_failure_oid_settest PROTO ((FILE *));

void 
alloc_mem_for_oid_getnext PROTO ((FILE *, INT4));

void 
fn_checking_for_malloc_failure_getnext_oid PROTO ((FILE * , INT4 , INT4));

void
allocmem_oid_for_partial_index PROTO((FILE * , INT4));

void 
free_all_oids PROTO ((FILE * , INT4));

void 
free_previous_oid_indices PROTO ((FILE *, INT4));

void 
alloc_mem_for_octetstring_getexact_first PROTO ((FILE * , INT4));

void 
fn_checking_for_malloc_failure_settest PROTO ((FILE *));

void 
alloc_mem_for_octetstring_getnext PROTO ((FILE *, INT4));

void 
fn_checking_for_malloc_failure_getnext PROTO ((FILE * , INT4 , INT4));

void
allocmem_for_octetstring_retvalue PROTO((FILE * , INT4));

void
free_octetstring_retvalue PROTO((FILE * , INT4));

void
allocmem_for_oid_retvalue PROTO((FILE * , INT4));

void
allocmem_octetstring_for_partial_index PROTO((FILE * , INT4));

void 
free_all_octetstring PROTO ((FILE * , INT4));

void 
free_previous_octet_indices PROTO ((FILE * , INT4));

void
declare_all_var_to_extractindices PROTO ((FILE *));

void
find_len_of_indices PROTO ((FILE *));

void 
get_exact_routine PROTO ((FILE *));

void 
get_next_routine PROTO ((FILE *));

void 
code_for_extracting_partial_indices PROTO((FILE *));

void
add_len_of_index PROTO((INT4));

void 
add_len_of_index_new_datatype PROTO((INT4 , INT4));

void
assign_len_of_variablelen_index PROTO((FILE *));

void
assign_len_of_variablelen_index_new_datatype PROTO((INT4 , INT4));

void
find_len_of_varlen_index_new_datatype PROTO((INT4 , INT4));

void
extract_one_index PROTO((FILE * , INT4));

void 
get_routine PROTO ((FILE *));

void
comments_for_getroutine PROTO ((UINT1 * , FILE *));

void
prototype_for_getroutine PROTO ((UINT1 * , FILE *));

INT4 
check_readonly PROTO ((void));

INT4 
all_no_access PROTO ((void));

void 
storeback_OID_getnext_new_datatype PROTO ((INT4, INT4));

void 
fill_args_to_formvarbind PROTO ((INT4, INT4));

void 
generate_routine_forall_objects PROTO ((FILE *));

void
getroutine_for_all_indices PROTO((void));

void 
passing_i2_type_to_formvarbind_for_index PROTO ((INT4));

void
passing_args_to_formvarbind_for_index PROTO((INT4 , INT4));

void
passing_args_to_formvarbind_for_index_new_datatype PROTO((INT4 , INT4 , INT4));

void 
passing_indices_to_lowlevel_routine PROTO ((FILE *));

void 
comments_for_setroutine PROTO ((UINT1 *,FILE *));

void
prototype_for_setroutine PROTO ((UINT1 *,FILE *));

void 
declaration_for_setroutine PROTO ((FILE *));

void 
declaration_scalar_table_setroutine PROTO ((FILE *));

void 
set_for_allobjects PROTO ((FILE *));

void 
extract_value PROTO ((FILE * , INT4));

void 
extract_valfrom_multi_for_new_datatype PROTO ((INT4 , INT4));

void 
pass_value_to_lowlevel_routine PROTO ((FILE * , INT4));

void 
set_routine PROTO ((FILE *));

void 
prototype_for_testroutine PROTO ((UINT1 *,FILE *));

void 
declaration_for_testroutine PROTO ((FILE *));

void 
declaration_scalar_table_testroutine PROTO ((FILE *));

void
comments_for_testroutine PROTO((UINT1 * , FILE *));

void 
test_for_allobjects PROTO ((FILE *));

void 
test_routine PROTO ((FILE *));

void
get_scalar_exact_routine PROTO ((FILE *));

void
declarations_for_getroutine PROTO ((FILE *));

void 
code_for_extracting_indices PROTO ((FILE *));

void
proto_midlevel_get_routine PROTO ((FILE *));

void
proto_midlevel_set_routine PROTO ((FILE *));

void
proto_midlevel_test_routine PROTO ((FILE *));

void
low_level_code_generator PROTO ((FILE *));

void
function_for_ifdef PROTO((FILE *));

void
passing_typeto_lowlev_skeletolcode PROTO((FILE * , INT4));

void
passing_typeto_lowlev_skeletolcode_getfirst PROTO((FILE * , INT4));

void
passing_prefix_according_totype PROTO((FILE * , INT4));

void
passing_prefix_according_totype_getfirst PROTO((FILE * , INT4));

void
defining_varsfor_lowlevel PROTO((FILE *));

void
defining_varsfor_lowlevel_getfirst PROTO((FILE *));

void
trace_logs_for_index PROTO((FILE *));

void
trace_logs_for_index_getfirst PROTO((FILE *));

void
trace_logs_for_object PROTO((FILE * , INT4 , INT4));

void
trace_logs_exit PROTO((FILE *));

void
passing_varsto_lowlevel PROTO((FILE *));

void
passing_varsto_lowlevel_getfirst PROTO((FILE *));

void
low_code_getfirst_next PROTO ((FILE *));

void
comment_for_lowlevel_getfirst PROTO((FILE *));

void
comment_for_lowlevel_getnext PROTO((FILE *));

void
low_code_get_all_objects PROTO((FILE *));

void
comment_for_lowlevel_get PROTO((FILE * , INT4));

void
low_code_set_all_objects PROTO((FILE *));

void
comment_for_lowlevel_set PROTO((FILE * , INT4));

void
low_code_test_all_objects PROTO((FILE *));

void
low_code_dep_all_objects PROTO((FILE *));

void
low_proto_dep_all_objects PROTO((FILE *));

void
comment_for_lowlevel_test PROTO((FILE * , INT4));

void
comment_for_lowlevel_dep PROTO((FILE * , INT4, INT4));

void
passing_index_to_lowlevelcode PROTO((FILE *));

void
low_level_proto_generator PROTO ((FILE *));

void
low_proto_getfirst_next PROTO ((FILE *));

void
low_proto_get_all_objects PROTO((FILE *));

void
low_proto_set_all_objects PROTO((FILE *));

void
low_proto_test_all_objects PROTO((FILE *));

void
passing_indextype_to_lowlevelproto PROTO((FILE *));

void
passing_indextype_to_lowlevelproto_getfirst PROTO((FILE *));

INT4 
search_new_data_type PROTO((UINT1 *));

INT4 
find_already_datatype_present PROTO((UINT1 *));

void 
declarations_for_new_datatype PROTO((INT4 , INT4)); 

void 
declare_retval_for_new_datatype PROTO((INT4 , INT4)); 

void 
extracting_indices_for_new_datatype PROTO((INT4 , INT4)); 

void 
convert_chararrayto_struct PROTO((INT4 , INT4)); 

void 
convert_from_struct_to_string PROTO((INT4 , INT4)); 

void 
convert_from_struct_to_string_getnext PROTO((INT4 , INT4)); 

void 
storeback_OID_for_new_datatype PROTO((INT4 , INT4)); 

void 
storeback_OID_for_new_datatype_exact PROTO((INT4 , INT4)); 

void 
passing_argsto_lowlev_for_new_datatype PROTO((INT4 , INT4)); 

void 
passing_retval_for_new_datatype PROTO((INT4 , INT4)); 

void 
generate_code_for_new_datatype PROTO((INT4)); 

void 
passargs_to_lowlev_for_new_datatype PROTO((INT4 , INT4));

void 
passargs_to_lowlev_for_new_datatype_getfirst PROTO((INT4 , INT4));

void 
passargs_to_lowlev_getnext_new_datatype PROTO((INT4 , INT4));

void 
head_ocon PROTO((FILE *));

void 
head_constant_declare PROTO((FILE *));

void 
head_ogp_index PROTO((FILE *));
 
void 
oid_table_v1 PROTO((UINT1 * , INT1 *));

void 
oid_table_v2 PROTO((void));

void 
oid_table_incs PROTO((FILE * , UINT1 *));

void 
generate_oidarray_fortable PROTO((UINT1 * , INT1 *));

INT4 
write_oidarray_tofile PROTO((FILE * , UINT1 *,INT1 *));

void 
generate_OID_table PROTO((UINT1 * , INT4 ,INT1 *));

void 
generate_MIBOBJ_table PROTO((UINT1 *,INT1 *));

UINT1*
search_table_name PROTO((FILE *));

void 
fnpointer_to_mid_fns PROTO((FILE * , INT4));

void 
create_mbdbh_file PROTO((UINT1* , UINT1 *));

void
create_makefile (UINT1 *pu1_pathname, UINT1 *pu1_file_name,
                 INT4 i4_tables_per_file,
                 INT1 **pi1_table_list, INT4 i4_table_len,
				 INT1 p1_in_files[][MAX_LINE], INT4 i4_file_count);

void
create_envfile PROTO((UINT1 * , UINT1 * ));

void
create_includeh PROTO((UINT1 * , UINT1 * ));

void 
header_constant_declare PROTO ((FILE *));

void
prototype_FormFncts PROTO((FILE *));

void
extern_prototype_FormFncts PROTO((FILE *));

void
clear_directory PROTO((void));

void
premosy_postmosy (INT4 argc, INT1 argv[][MAX_LINE]);

INT4
find_nof_comma PROTO ((UINT1 *));

INT4
nof_grouped_oid PROTO ((INT4 []));

INT4
get_oid_fromtempfile PROTO((UINT1 *));

INT4
find_commonoid PROTO ((void));

INT4
find_bigoid_index PROTO ((INT4 , INT4 []));

INT4
find_next_nongrouped_index PROTO ((INT4 []));

INT4
find_next_nongrouped_ingrp PROTO ((INT4 , INT4 []));

void
fill_with_groupno PROTO ((INT4 , INT4 []));

void
store_grpno PROTO ((INT4 , INT4 []));

INT4
find_before_oidlen PROTO ((UINT1 * , INT4));

INT4
find_matching_oid PROTO ((INT4 , INT4 , INT4 , INT4 []));

INT4
form_groups PROTO ((INT4 , INT4 , INT4 []));

INT4
find_suboid_len PROTO ((INT4 [] , INT4, INT2));

INT4
find_nof_subgrps PROTO ((INT4 [] , INT4));

INT4
find_lenof_groupoid PROTO ((INT4 [] , INT4));

UINT1 *
get_tablename_from_tempfile PROTO ((INT4 , UINT1 *));

void
fnptr_for_basoidtable PROTO ((FILE * , UINT1 * , INT4));

void
write_oidarray (INT4 i4_indexarray[], UINT1 *pu1_pathname,
                UINT1 *pu1_filename, INT2 i2_goid_len, INT4 i4_argv_count,
				INT1 u1_parse_files[][MAX_LINE], INT1 i1_code_for_agent);
void
write_suboidarray PROTO ((INT4 [], UINT1 * ,UINT1 *, INT2));

void
write_grpoid_table PROTO ((INT4 [] , UINT1 * , UINT1 *, INT1, INT2));

void
write_baseoid_table PROTO ((INT4 [] , UINT1 * , UINT1 *, INT1, INT2));

void
write_mibobj_table PROTO ((UINT1 * , UINT1 *, INT1));

void
create_mbdbh_file_ver2 (UINT1 *pu1_pathname, UINT1 *pu1_filename,
                        INT1 i1_code_for_agent, INT2 i2_goid_len,
						INT4 i4_argv_count, INT1 u1_parse_files[][MAX_LINE]);

void 
gencode_for_alloc_oid_fn PROTO ((FILE *));

void 
gencode_for_alloc_octet_fn PROTO ((FILE *));

void 
gencode_for_free_oid_fn PROTO ((FILE *));

void 
gencode_for_free_octet_fn PROTO ((FILE *));

void 
prototype_alloc_free_fns PROTO ((FILE *));

void 
extern_prototype_alloc_free_fns PROTO ((FILE *));

/*
 * following function are added to help split output into multiple files
 * and avoid subset problem while generating groups
 * added by soma on 06/03/98
 */

INT1 *
itoa PROTO ((INT4));

UINT1
check_for_subset PROTO ((INT4 *, INT4 *, INT4 []));

void
merge_groups PROTO ((INT4, INT4, INT4 []));

UINT1 *
get_group_oid PROTO ((INT4, INT4 []));

/*
 * following functions are added to provide the user an option to specify
 * the length of Group OID to be kept in Group OID table in mbdb.h file
 * added by soma on 14/04/98
 */

INT4
shortest_oid PROTO ((void));

void
form_groups_in_one_shot PROTO ((INT2, INT4 []));

INT4
get_group_oid_len PROTO ((INT2, INT4, INT4 []));

void
open_files PROTO ((INT1 *,INT1 *, INT1 *, INT1 *));

void
close_files PROTO (());

INT4
yyerror PROTO((INT1 *));

INT4
isalpha PROTO((INT4));

INT4 
ValueForType PROTO((INT1 *));

INT1 *
GetStringFromType PROTO(( INT1 *));

void wrap_file_open PROTO((INT1*,INT1*,INT1*));
void wrap_code_getfirst_next PROTO((void));
void wrap_code_getfirst_next PROTO((void));
void wrap_code_get_all_objects PROTO((void));
void wrap_code_set_all_objects PROTO((void));
void wrap_code_test_all_objects PROTO((void));
void wrap_code_dep_all_objects PROTO((void));
void wrap_file_close PROTO((INT1 *,INT1 *,INT1 *));
void mid_code_set_all_objects PROTO((void));
void passing_index_to_midlevelcode PROTO((FILE*));
void passing_prefix_according_midtype PROTO((FILE*, INT4));
void passing_fmtindex_to_midlevelcode PROTO((FILE*));
void passing_fmtdata_to_midlevelcode PROTO((FILE*, INT4));
/* The following functions are used in case the 
 * mib is for Multiple Instance
 * Included on 3/01/2007
 */
void passing_context_id PROTO((FILE*));
void pass_si_nmh_routine PROTO((FILE*, INT4));
void pass_user_strings PROTO((INT4, INT1 * []));
void parse_lock_function PROTO((INT4, INT1 * []));
void parse_unlock_function PROTO((INT4, INT1 * []));
void passing_index_to_si_lowlevelcode PROTO((FILE*));
