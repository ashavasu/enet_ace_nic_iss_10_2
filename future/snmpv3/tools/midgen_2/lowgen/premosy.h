/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: premosy.h,v 1.5 2015/04/28 14:43:31 siva Exp $
 *
 * Description:This file contains the typedefs, other header 
 *             files to be included, definition of MACROs 
 *             and function prototypes for premosy module  
 *******************************************************************/

#ifndef _PREMOSY_H
#define _PREMOSY_H

#include "common.h" 

#define ALLOC_LINE_MEM (INT1 *) calloc (1, MAX_LINE)
#define ALLOC_DESC_MEM (INT1 *) calloc (1, MAX_DESC_LEN)
#define ALLOC_OID_MEM (tOID *) calloc (1, sizeof (tOID))
#define ALLOC_NAME_MEM (tNAME_LIST *) calloc (1, sizeof (tNAME_LIST))
#define ALLOC_NAME_VALUE_MEM (tNAME_VALUE *) calloc (1, sizeof (tNAME_VALUE))
#define ALLOC_TC_MEM (tTC_TABLE *) calloc (1, sizeof (tTC_TABLE))

typedef struct name_list {
         INT1 name[MAX_DESC_LEN];
			struct name_list *next;
} tNAME_LIST;

/* added to store indices of a table *
 * to use, if some one defines a AUGMENTS later           *
 *                                                        */
typedef struct indices_list {
          INT1 name[MAX_DESC_LEN];
	  tNAME_LIST *name_list;
	  struct indices_list *next;
} tINDICES_LIST;

typedef struct impindices_list {
          INT1 name[MAX_DESC_LEN];
	  struct impindices_list *next;
} tIMPINDICES_LIST;


typedef struct name_value {
         INT1 name[MAX_DESC_LEN];
         INT4 value;
			struct name_value *next;
} tNAME_VALUE;

typedef struct oid {
         INT1 name[MAX_DESC_LEN];
			INT4 oid;
         struct oid *next;
} tOID;

typedef struct tc_table {
         INT1 tc_name[MAX_DESC_LEN];
			INT1 tc_base[MAX_DESC_LEN];
			INT1 tc_enum_type;
			tNAME_VALUE *tc_name_value_list;
         struct tc_table *next;
} tTC_TABLE;

typedef struct object_ls {
         INT1 name[MAX_DESC_LEN];
	INT1 syntax[MAX_DESC_LEN];
	INT1 access[MAX_DESC_LEN];
	INT4 range;
	INT4 lowVal;
         struct object_ls *next;
} tOBJECT_LS;
void free_OID_memory (tOID *);
void free_tc_memory (tTC_TABLE *);
void free_names_memory (tNAME_LIST *);
void free_name_values_memory (tNAME_VALUE *);
INT4 check_validity_of_oid (INT1 *);
INT4 check_validity_of_object (INT1 *);
INT4 check_validity_of_syntax (INT1 *);
INT4 check_validity_of_import (INT1 *);
INT4 check_for_duplicate_name (INT1 *, INT1);
INT4 check_validity_of_name (INT1 *, INT1*);
INT4 check_validity_of_value (INT1 *, INT1, INT4);
INT4 store_import (INT1 *);
INT4 store_oid (INT1 *);
INT4 store_object (INT1 *);
INT4 store_value (INT1, INT4);
INT1 *itoa (INT4);
INT4 append_temp_file_to_output_file (INT1 *, INT1 *);
INT1 *compute_primitive(INT1 *);
extern genetor_module();
extern oid_table_v1();
void add_v2_typedefs();
void add_typedef(INT1 *,INT1 *);
void get_syntax_info_fromfile();
INT4 get_syntax_fronfile(INT1 *,INT1 *);
void add_into_object_list(INT1 *);
void check_for_imports();
void store_object_info();
void free_object_memory(tOBJECT_LS *);
INT4 check_indices_defination(INT1 *);
INT4 already_in_table_entry(INT1 *);


#endif /* _PREMOSY_H */
