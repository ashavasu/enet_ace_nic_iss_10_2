/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: incs.c,v 1.4 2015/04/28 14:43:31 siva Exp $
 *
 * Description: This File Contains the function Which     
 *              Generates the INCLUDE.H File and places 
 *              it in the INC directory.  
 *******************************************************************/

# include "include.h"
# include "extern.h"

/******************************************************************************
*      function Name        : create_includeh                                 *
*      Role of the function : This fn generates the Include File which has    *
*                             all the Include file.                           *
*      Formal Parameters    : pi1_string                                      *
*      Global Variables     : i4_global_mac_flag                              *
*      Use of Recursion     : None                                            *
*      Return Value         : i4_count (Nof Commas in the String.             *
******************************************************************************/
void
create_includeh (UINT1 *pi1_incpath, UINT1 *file_name)
{
    UINT1               i1_path[MAX_LINE];
    FILE               *fincs;

    strcpy (i1_path, pi1_incpath);
    strcat (i1_path, "include.h");
    fincs = fopen (i1_path, WRITE_MODE);
    if (fincs == NULL)
    {
        printf ("\n\tERROR : Can't Open INCLUDE.H File\n");
        clear_directory ();
        exit (NEGATIVE);
    }
    fprintf (fincs, "\n /********************************/ ");
    fprintf (fincs, "\n /*         INCLUDE . H          */ ");
    fprintf (fincs, "\n /*                              */ ");
    fprintf (fincs, "\n /*  This File Contains all the  */ ");
    fprintf (fincs, "\n /*  Include  Files.             */ ");

    if (i4_global_mac_flag)
    {
        fprintf (fincs, "\n /*  Also contains definition for*/ ");
        fprintf (fincs, "\n /*  object of type MacAddress   */ ");
    }
    fprintf (fincs, "\n /********************************/ \n\n");

    fprintf (fincs, INCS);
    fprintf (fincs, "<stdio.h>\n");
    fprintf (fincs, INCS);
    fprintf (fincs, "\"snmccons.h\"\n");
    fprintf (fincs, INCS);
    fprintf (fincs, "\"snmcdefn.h\"\n");
    fprintf (fincs, INCS);
    fprintf (fincs, "\"snmctdfs.h\"\n");

    /*
     * If a object of type MacAddress is present (ie i4_global_mac_flag =1)
     *then generate the corresponding data structure of that type
     */
    if (i4_global_mac_flag)
    {
        fprintf (fincs,
                 "\n/*Data Structure for objects of type MacAddress*/\n");
        fprintf (fincs, "\ntypedef struct MacAddress {");
        fprintf (fincs, "\n\tUINT4   u4Dword;");
        fprintf (fincs, "\n\tUINT2   u2Word;");
        fprintf (fincs, "\n} tMacAddr;\n");
    }
    /*
     *This is to avoid the same declaration in all include file in case multiple
     *files are given as i/p.
     */
    i4_global_mac_flag = 0;
    fclose (fincs);
}                                /*  End Of Function. */
