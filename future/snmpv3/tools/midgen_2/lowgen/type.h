/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: type.h,v 1.4 2015/04/28 14:43:31 siva Exp $
 *
 * Description:This File contains the Type Definitions         
 *             needed for the Mid Level Code Generator.
 *******************************************************************/


/* Type Declartion For The Global Data Structure.  */          

typedef struct table{
		char table_name[MAX_LINE];               	/* Holds the Name of A Table. */
		char entry_name[MAX_LINE];                  /* Holds the Entry Name for a Table. */
		char index_name[MAX_INDICES][MAX_LINE];     /* Array Holding the Name of the Indices. */
                int  no_of_indices;                 /* No Of Indices in the Table. */
                int  no_of_imports;                 /* No Of Import Indices in the Table. */
                int  no_of_objects;                 /* No Of Objects in the Table excluding the Import Indices. */
				int  status;                        /* Holds the Status of A Table */
		struct ss{                                  /* Structure Holding the Attributes of Each Object. */
				char object_name[MAX_LINE];
                char type_name[MAX_LINE]; 
				int  access;
				int  type;
		int  min_val; /* To track Lowest Range */		
                int  size;
                int  position;
	                char defval[MAX_LINE];
			char isDeprecated;
			char isRowStatus;
			int  status;
			}object[MAX_OBJECTS];
		struct table  *next;
	}t_MIB_TABLE;	

	
/*  
 *   Type Declaration For The Data Structure
 *   which has the details of the new data types.
 */ 

typedef struct typetable{
                           char type_name[MAX_LINE];            /* Name of the Data Type. */
                           char primitive_type_name[MAX_LINE];  /* Name of the Primitive Data Type. */
                           int  primitive_type;                 /* Name of the Type to which the New Type is Mapped to. */
                           int  size;                           /* Size of the New Type. */
                         }t_TYPE_TABLE;     

/*
 * This data structure is used for returning the integer value for the correspon * ding string
 */
typedef struct ValueTable{
   char * pDataType;
   int    i4_value;
}t_VALUE_TABLE;
