/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: set_routine.c,v 1.4 2015/04/28 14:43:31 siva Exp $
 *
 * Description: This File Contains the function Which     
 *              generate the Middle level SET Routines .
 *******************************************************************/

#include "include.h"
#include "extern.h"

#ifdef CORRECT_MIDGEN
extern INT4         i4_check_repeat_table;
extern INT4         p_temp_counter;
#endif

/******************************************************************************
*      function Name        : set_routine                                     *
*      Role of the function : This fn is the Main Module of Set Routine which *
*                             calls all the other code generating Fns.        *
*      Formal Parameters    : fp                                              *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/
void
set_routine (FILE * fp)
{

#ifdef CORRECT_MIDGEN
    UINT1              *pi1_string;

    (UINT1 *) pi1_string =
        (UINT1 *) calloc (1, sizeof (p_table_struct->entry_name));
    strcpy (pi1_string, p_table_struct->entry_name);

    if (i4_check_repeat_table)
    {
        strcat (pi1_string, itoa (p_temp_counter));
    }
    comments_for_setroutine (pi1_string, fp);
    prototype_for_setroutine (pi1_string, fp);
    free (pi1_string);
#else
    comments_for_setroutine (p_table_struct->entry_name, fp);
    prototype_for_setroutine (p_table_struct->entry_name, fp);
#endif

    fprintf (fp, "\n\n/*** DECLARATION_BEGIN ***/\n\n");
    declarations_for_setroutine (fp);
    fprintf (fp, "\n\n/*** DECLARATION_END ***/\n\n");
    set_for_allobjects (fp);

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : comments_for_set_routine                        *
*      Role of the function : This fn generates the Comments for the Set Fns  *
*      Formal Parameters    : pi1_entry_name , fp                             *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/
void
comments_for_setroutine (UINT1 *pi1_entry_name, FILE * fp)
{
    fprintf (fp, "\n\n/" DOT "");
    fprintf (fp, "\n Function   : %sSet", pi1_entry_name);
    fprintf (fp,
             "\n Description: This routine returns the value of the requested");
    fprintf (fp, "MIB variable.");
    fprintf (fp,
             "\n Input      : p_in_db   : The OID as formed by the SNMP Agent");
    fprintf (fp, "\n                          from the static MIB database.");
    fprintf (fp,
             "\n              p_incoming: The OID as it is send by the manager");
    fprintf (fp, "\n                          in the SNMP PDU.");
    fprintf (fp,
             "\n              u1_arg    : The position of the variable in the MIB group.");
    fprintf (fp,
             "\n              p_value   : Points to MultiDataType of the variable");
    fprintf (fp, "\n                          specified by p_incoming.");
    fprintf (fp, "\n Output     : The actual OID for which the operation is");
    fprintf (fp, "\n              performed is returned in p_in_db.");
    fprintf (fp, "\n Returns    : Returns one of the following error codes");
    fprintf (fp, "\n           SNMP_ERR_INCONSISTNT_NAME");
    fprintf (fp, "\n           SNMP_ERR_GEN_ERR");
    fprintf (fp, "\n" DOT "/ \n");
}

/******************************************************************************
*      function Name        : prototype_for_setroutine                        *
*      Role of the function : This fn generates the Prototypes for the        *
*                             Set Routines                                    *
*      Formal Parameters    : pi1_entry_name , fp                             *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/
void
prototype_for_setroutine (UINT1 *i1_entry_name, FILE * fp)
{
    fprintf (fp, "\n/* Prototype declarations for set_routine. */");
    fprintf (fp,
             "\nINT4 %sSet (tSNMP_OID_TYPE *p_in_db , tSNMP_OID_TYPE *p_incoming,",
             i1_entry_name);
    fprintf (fp, "\n            UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE *p_value)");
    fprintf (fp, "\n{\n");

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : declarations_for_setroutine                     *
*      Role of the function : This fn generates the code for which declares   *
*                             the vars for the SET routines .                 *
*      Formal Parameters    : fp                                              *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
declarations_for_setroutine (FILE * fp)
{
    fprintf (fp, "\n    /* Declarations for set routine */\n");

    /*  Declaration of Common varibles. */
    declaration_of_common_variables (fp);

    /* 
     *  The Declaration of the Octet String Variable Which is used
     *  to extract the Value From the MULTI DATA TYPE Struct.
     */
    fprintf (fp, "\n    /*");
    fprintf (fp, "\n     *  This Variable is used to extract the Value in The");
    fprintf (fp, "\n     *  MULTI DATA TYPE which is sent by the Manager.");
    fprintf (fp, "\n     */");
    fprintf (fp, "\n    UINT1 u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;");
    fprintf (fp, "\n    INT4  LEN_%s_INDEX          ;\n",
             p_table_struct->table_name);
    fprintf (fp, "\n    INT4  i4_count = FALSE;");

    /*  Fn which declares variables for extracting indices as per their type. */
    declare_all_var_to_extractindices (fp);

    /* 
     *  This Fn Declares the Variable for each object that
     *  is passed to the Low Level SET and TEST routines.
     */
    declaration_of_value (fp);

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : declaration_of_value                            *
*      Role of the function : This fn generates the code for which declares   *
*                             the Vals for indices for the Mid lev Set and    *
*                             Test Routines.                                  *
*      Formal Parameters    : fp                                              *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/
void
declaration_of_value (FILE * fp)
{
    INT4                i4_count;

    fprintf (fp, "\n    /*");
    fprintf (fp,
             "\n     *  The Declaration of variables which are used to extract");
    fprintf (fp,
             "\n     *  the Values From the MULTI DATA TYPE sent by the Manager.");
    fprintf (fp, "\n     */ ");

    for (i4_count = FALSE; i4_count < p_table_struct->no_of_objects; i4_count++)
    {
        switch (p_table_struct->object[i4_count].type)
        {
            case INTEGER:
            case POS_INTEGER:
            case OBJECT_IDENTIFIER:
            case OCTET_STRING:
                break;
            case ADDRESS:
            {
                fprintf (fp, "\n    UINT4 u4_addr_val_%s ;",
                         p_table_struct->object[i4_count].object_name);
                break;
            }
            case MacAddress:
            {
                fprintf (fp, "\n    tMacAddr mac_addr_val_%s ;",
                         p_table_struct->object[i4_count].object_name);
                break;
            }

        }                        /*  End Of Switch */

    }                            /* End of FOR loop */

}                                /* End of Function. */

/******************************************************************************
*      function Name        : set_forall_objects                              *
*      Role of the function : This Fn generates the Set routines for all the  *
*                             Object in the Table according to the permission *
*      Formal Parameters    : fp                                              *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
set_for_allobjects (FILE * fp)
{

    INT4                i4_count = FALSE;
    INT4                i4_no_access[MAX_OBJECTS];
    INT4                i4_no_access_count = FALSE;
    UINT1               i1_temp[MAX_LINE] = NULL_STR;
    UINT1              *pi1_temp;

    fprintf (fp, "\n     i4_offset = FALSE;");
    fprintf (fp, "\n     if(p_incoming->u4_Length <= p_in_db->u4_Length) {");
    fprintf (fp, "\n        return(SNMP_ERR_WRONG_LENGTH);");
    fprintf (fp, "\n     }");
    fprintf (fp, "\n     else {");

    /*
     *  This Fn Generates the Code for Assigning Value of the Variable
     *  Len Index to the Variable(which is declared) from the Given OID.
     */
    assign_len_of_variablelen_index (fp);

    /*
     *  Allocating Memory for Octet Str type Indices which are
     *  to be passed to the Low Level Routines Get Exact Case.
     *  (Only if Octet String is Present as an Index for the Table).
     */
    alloc_mem_for_octetstring_getexact_first (fp, GET_EXACT_FLAG);

    /*  This fn Checks For Malloc Failures for Octet Strings. */
    fn_checking_for_malloc_failure_settest (fp);

    /*
     *  Allocating Memory for OID type Indices which are
     *  to be passed to the Low Level Routines Get Exact Case.
     *  (Only if Octet String is Present as an Index for the Table).
     */
    alloc_mem_for_oid_getexact_first (fp, GET_EXACT_FLAG);

    /*  This fn Checks For Malloc Failures for OID. */
    fn_checking_for_malloc_failure_oid_settest (fp);

    /* This Fn generates the code which extracts the indices.  */
    code_for_extracting_indices (fp);

    fprintf (fp, "\n    }/* End Of Else Condition. */");

    /*
     *  For loop for calling low level routines for each 
     *  object according to their access permission.
     */

    fprintf (fp, "\n    switch (u1_arg)   {");
    for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects; i4_count++)
    {
        /*
         *  This if check for the access permission and writes 
         *  to the file only the objects which can be accessed. 
         */
        if ((p_table_struct->object[i4_count].access == NO_ACCESS)
            || (p_table_struct->object[i4_count].access == READ_ONLY))
        {

            i4_no_access[i4_no_access_count++] = i4_count;
            continue;

        }
        else if ((p_table_struct->object[i4_count].access == READ_WRITE)
                 || (p_table_struct->object[i4_count].access == READ_CREATE))
        {

            strcpy (i1_temp, p_table_struct->object[i4_count].object_name);
            pi1_temp = strupper (i1_temp);
            fprintf (fp, "\n       case %s:", pi1_temp);
            fprintf (fp, "\n          {");

            /* 
             *  This Fn check for the Type of the Object and extracts the Value
             *  (on which set is performed) from the Multi Data Type Struct.
             */
            extract_value (fp, i4_count);

            /* 
             * this is to conform to the Hungarian styled function names
             * used in low level code
             * added by soma on 07/03/98
             */
            p_table_struct->object[i4_count].object_name[0] =
                toupper (p_table_struct->object[i4_count].object_name[0]);
            fprintf (fp, "\n             i1_ret_val = nmhSet%s(",
                     p_table_struct->object[i4_count].object_name);
            /*
             * this is to restore the change made above
             * added by soma on 07/03/98
             */
            p_table_struct->object[i4_count].object_name[0] =
                tolower (p_table_struct->object[i4_count].object_name[0]);

            /* This Fn sends the Index variable to the Low Level routine. */
            passing_indices_to_lowlevel_routine (fp);

            /* 
             *  This Fn sends the Value parameter for the
             *  set routine to the Low Level routine.
             */
            pass_value_to_lowlevel_routine (fp, i4_count);

            fprintf (fp, ");\n");

            /*  Freeing All the Oids and Octet Strings. */
            free_all_octetstrings (fp, GET_FIRST_FLAG);
            free_all_oids (fp, GET_FIRST_FLAG);

            fprintf (fp, "\n             if (i1_ret_val == SNMP_SUCCESS) {");
            fprintf (fp, "\n                 return (SNMP_ERR_NO_ERROR);");
            fprintf (fp, "\n             }");
            fprintf (fp, "\n             else {");
            fprintf (fp, "\n                 return (SNMP_ERR_GEN_ERR);");
            fprintf (fp, "\n             }");
            fprintf (fp, "\n             break;");
            fprintf (fp, "\n          }");

        }                        /* End of else loop.  */

    }                            /* End of For loop. */

    /*
     *  For loop for writing to the file the OBJECTS that 
     *  are not accessible and that are READ_ONLY.
     */
    for (i4_count = ZERO; i4_count < i4_no_access_count; i4_count++)
    {

        fprintf (fp, "\n     /*  Read Only Variables. */");
        strcpy (i1_temp,
                p_table_struct->object[i4_no_access[i4_count]].object_name);
        pi1_temp = strupper (i1_temp);

        fprintf (fp, "\n     case %s :", pi1_temp);
    }

    /*  Freeing All the Oids and Octet Strings. */
    free_all_octetstrings (fp, GET_FIRST_FLAG);
    free_all_oids (fp, GET_FIRST_FLAG);

    /* 
     * Only If there are Objects which dont have 
     * permission This statement is to be present.
     */
    if (i4_no_access_count > ZERO)
    {
        fprintf (fp, "\n          return(SNMP_ERR_NOT_WRITABLE);\n");
    }

    fprintf (fp, "\n     default :");

    /*  Freeing All the Oids and Octet Strings. */
    free_all_octetstrings (fp, GET_FIRST_FLAG);
    free_all_oids (fp, GET_FIRST_FLAG);

    fprintf (fp, "\n         return(SNMP_ERR_INCONSISTENT_NAME);\n     }");
    fprintf (fp, "/* End of Switch */");
    fprintf (fp, "\n} /*   THE SET ROUTINES GET OVER. */\n\n");

}                                /*  End Of Function. */

/******************************************************************************
*      function Name        : extract_value                                   *
*      Role of the function : This Fn checks for the Objects Data type and    *
*                             extracts Values from the Multi Data Type struct *
*      Formal Parameters    : fp , i4_count                                   *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
extract_value (FILE * fp, INT4 i4_count)
{

    switch (p_table_struct->object[i4_count].type)
    {
        case POS_INTEGER:
        case INTEGER:
        case OBJECT_IDENTIFIER:
        case OCTET_STRING:
            break;
        case ADDRESS:
        {
            fprintf (fp,
                     "\n                 for(i4_offset = FALSE ; i4_offset < ADDR_LEN ; i4_offset++)");
            fprintf (fp,
                     "\n                     u1_octet_string[i4_offset] = p_value->pOctetStrValue->pu1_OctetList[i4_offset];");
            fprintf (fp,
                     "\n                 u4_addr_val_%s = OSIX_NTOHL(*((UINT4 *)(u1_octet_string)));",
                     p_table_struct->object[i4_count].object_name);

            break;
        }
        case MacAddress:
        {
            fprintf (fp,
                     "\n                 for(i4_offset = FALSE ; i4_offset < MAC_ADDR_LEN ; i4_offset++)");
            fprintf (fp,
                     "\n                     mac_addr_val_%s[i4_offset] = p_value->pOctetStrValue->pu1_OctetList[i4_offset];",
                     p_table_struct->object[i4_count].object_name);

            break;
        }
        default:
            break;

    }                            /* End of SWITCH CASE Statement. */

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : pass_value_to_lowlevel_routine                  *
*      Role of the function : This Fn checks for the Objects Data type and    *
*                             passes the Value from the Multi Data Type to    *
*                             the low level Fns.                              *
*      Formal Parameters    : fp , i4_count                                   *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
pass_value_to_lowlevel_routine (FILE * fp, INT4 i4_count)
{

    if (i4_global_scalar_flag == FALSE)
    {
        fprintf (fp, " ,");
    }

    switch (p_table_struct->object[i4_count].type)
    {
        case POS_INTEGER:
        {
            fprintf (fp, " p_value->u4_ULongValue");
            break;
        }
        case INTEGER:
        {
            fprintf (fp, " p_value->i4_SLongValue");
            break;
        }
        case OCTET_STRING:
        {
            fprintf (fp, " p_value->pOctetStrValue");
            break;
        }
        case OBJECT_IDENTIFIER:
        {
            fprintf (fp, " p_value->pOidValue");
            break;
        }
        case ADDRESS:
        {
            fprintf (fp, " u4_addr_val_%s",
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        case MacAddress:
        {
            fprintf (fp, " mac_addr_val_%s",
                     p_table_struct->object[i4_count].object_name);
            break;
        }
        default:
            break;

    }                            /* End of SWITCH CASE Statement. */

}                                /*  End of Function */

/******************************************************************************
*      function Name        : fn_checking_for_malloc_failure_settest          *
*      Role of the function : This Fn allocates the memory for the Indices    *
*                             which are of type OctetStr for Set and Test fns *
*      Formal Parameters    : FILE *fp                                        *
*      Global Variables     : p_table_struct , type_table                     *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
fn_checking_for_malloc_failure_settest (FILE * fp)
{
    INT4                i4_counter = ZERO;
    INT4                i4_count = ZERO;

    /* This Var is used to Indicate the Presence of One Octet str Index. */
    INT4                i4_first_index_flag = ZERO;

    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {

        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            if (p_table_struct->object[i4_count].type == OCTET_STRING)
            {

                if (i4_first_index_flag == ZERO)
                {
                    fprintf (fp, "\n             if(");
                }

                /*  Finding Whether there are more than one Index. */
                if (i4_first_index_flag >= ONE)
                {
                    fprintf (fp, "||");
                }

                i4_first_index_flag++;
                fprintf (fp, "(poctet_%s == NULL)",
                         p_table_struct->object[i4_count].object_name);
            }

            /* Break when An index is found. */
            break;
        }                        /* FOR Loop For All Variables. */

    }                            /* Main FOR Loop for Each Index. */

    if (i4_first_index_flag != ZERO)
    {
        fprintf (fp, ")");
    }

    if (i4_first_index_flag != ZERO)
    {

        fprintf (fp, "\n             {");
        free_all_octetstrings (fp, GET_FIRST_FLAG);
        fprintf (fp, "\n                    return(SNMP_ERR_GEN_ERR);");
        fprintf (fp, "\n             }");

    }

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : fn_checking_for_malloc_failure_oid_settest      *
*      Role of the function : This Fn allocates the memory for the Indices    *
*                             which are of type OctetStr for Set and Test fn. *
*      Formal Parameters    : FILE *fp                                        *
*      Global Variables     : p_table_struct , type_table                     *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
fn_checking_for_malloc_failure_oid_settest (FILE * fp)
{
    INT4                i4_counter = ZERO;
    INT4                i4_count = ZERO;

    /* This Var is used to Indicate the Presence of One OID Index. */
    INT4                i4_first_index_flag = ZERO;

    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {

        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {
            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            if (p_table_struct->object[i4_count].type == OBJECT_IDENTIFIER)
            {

                if (i4_first_index_flag == ZERO)
                {
                    fprintf (fp, "\n             if(");
                }

                /*  Finding Whether there are more than one Index. */
                if (i4_first_index_flag >= ONE)
                {
                    fprintf (fp, "||");
                }

                i4_first_index_flag++;
                fprintf (fp, "(OID_%s == NULL)",
                         p_table_struct->object[i4_count].object_name);
            }
            /* Break When A Index is found.  */
            break;
        }                        /* FOR Loop For All Variables. */

    }                            /* Main FOR Loop for Each Index. */

    if (i4_first_index_flag != ZERO)
    {

        fprintf (fp, ") {");
        free_all_oids (fp, GET_FIRST_FLAG);
        fprintf (fp, "\n                    return(SNMP_ERR_GEN_ERR);");
        fprintf (fp, "\n             }");

    }

}                                /* End Of Function. */
