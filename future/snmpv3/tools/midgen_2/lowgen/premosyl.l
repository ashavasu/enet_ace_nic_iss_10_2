%{
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: premosyl.l,v 1.4 2015/04/28 14:43:31 siva Exp $
 *
 * Description: This file contains specification for lexer. 
 *              
 *******************************************************************/

/****************************************************************************/
#include "premosy.h"
#include "premosyy.h"

INT4 yylineno;
void read_comment (void);
%}
%%
\n { yylineno++; }
[ \t\r]+ ;
"--" { read_comment (); }
DEFINITIONS {
		  strcpy (yylval.str, yytext);
        return DEFINITIONS;
		  }
BEGIN {
		  strcpy (yylval.str, yytext);
        return BEGIN_DEF;
		  }
END {
		  strcpy (yylval.str, yytext);
        return END;
		  }
IMPORTS {
		  strcpy (yylval.str, yytext);
        return IMPORTS;
		  }
FROM {
		  strcpy (yylval.str, yytext);
        return FROM;
		  }
OBJECT {
		  strcpy (yylval.str, yytext);
        return OBJECT;
		  }
IDENTIFIER {
		  strcpy (yylval.str, yytext);
        return IDENTIFIER;
		  }
"::=" {
       strcpy (yylval.str, yytext);
		 return CCE;
		 } 
MODULE[-]IDENTITY {
		  strcpy (yylval.str, yytext);
        return MODULE_IDENTITY;
		  }
LAST[-]UPDATED {
		  strcpy (yylval.str, yytext);
        return LAST_UPDATED;
		  }
ORGANIZATION {
		  strcpy (yylval.str, yytext);
        return ORGANIZATION;
		  }
CONTACT[-]INFO {
		  strcpy (yylval.str, yytext);
        return CONTACT_INFO;
		  }
OBJECT[-]IDENTITY {
		  strcpy (yylval.str, yytext);
        return OBJECT_IDENTITY;
		  }
REVISION {
		  strcpy (yylval.str, yytext);
        return REVISION;
		  }
OBJECT[-]TYPE {
		  strcpy (yylval.str, yytext);
        return OBJECT_TYPE;
		  }
SYNTAX {
		  strcpy (yylval.str, yytext);
        return SYNTAX;
		  }
UNITS {
		  strcpy (yylval.str, yytext);
        return UNITS;
		  }
MAX[-]ACCESS {
		  strcpy (yylval.str, yytext);
        return MAX_ACCESS;
		  }
STATUS {
		  strcpy (yylval.str, yytext);
        return STATUS;
		  }
DEFVAL {
		  strcpy (yylval.str, yytext);
        return DEFVAL;
		  }
DESCRIPTION {
		  strcpy (yylval.str, yytext);
        return DESCRIPTION;
		  }
read[-]only {
		  strcpy (yylval.str, yytext);
        return _READ_ONLY;
		  }
read[-]write {
		  strcpy (yylval.str, yytext);
        return _READ_WRITE;
		  }
read[-]create {
		  strcpy (yylval.str, yytext);
        return _READ_CREATE;
		  }
not[-]accessible {
		  strcpy (yylval.str, yytext);
        return NOT_ACCESSIBLE;
		  }
accessible[-]for[-]notify {
		  strcpy (yylval.str, yytext);
        return ACCESSIBLE_FOR_NOTIFY;
		  }
not[-]implemented {
		  strcpy (yylval.str, yytext);
        return NOT_IMPLIMENTED;
		  }
current {
		  strcpy (yylval.str, yytext);
        return CURRENT;
		  }
deprecated {
		  strcpy (yylval.str, yytext);
        return DEPRECATED;
		  }
obsolete {
		  strcpy (yylval.str, yytext);
        return OBSOLETE;
		  }
SEQUENCE {
		  strcpy (yylval.str, yytext);
        return SEQUENCE;
		  }
OF {
		  strcpy (yylval.str, yytext);
        return OF;
		  }
INDEX {
		  strcpy (yylval.str, yytext);
        return _INDEX;
		  }
AUGMENTS {
		  strcpy (yylval.str, yytext);
        return AUGMENTS;
		  }
WRITE[-]SYNTAX {
		  strcpy (yylval.str, yytext);
        return WRITE_SYNTAX;
		  }
CREATION[-]REQUIRES {
		  strcpy (yylval.str, yytext);
        return CREATION_REQUIRES;
		  }
MODULE[-]COMPLIANCE {
		  strcpy (yylval.str, yytext);
        return MODULE_COMPLIANCE;
		  }
MIN[-]ACCESS {
		  strcpy (yylval.str, yytext);
        return MIN_ACCESS;
		  }
ACCESS {
		  strcpy (yylval.str, yytext);
        return ACCESS;
		  }
MODULE {
		  strcpy (yylval.str, yytext);
        return MODULE;
		  }
MANDATORY[-]GROUPS {
		  strcpy (yylval.str, yytext);
        return MANDATORY_GROUPS;
		  }
GROUP {
		  strcpy (yylval.str, yytext);
        return GROUP;
		  }
AGENT[-]CAPABILITIES {
		  strcpy (yylval.str, yytext);
        return AGENT_CAPABILITIES;
		  }
PRODUCT[-]RELEASE {
		  strcpy (yylval.str, yytext);
        return PRODUCT_RELEASE;
		  }
SUPPORTS {
		  strcpy (yylval.str, yytext);
        return SUPPORTS;
		  }
INCLUDES {
		  strcpy (yylval.str, yytext);
        return INCLUDES;
		  }
VARIATION {
		  strcpy (yylval.str, yytext);
        return VARIATION;
		  }
OBJECT[-]GROUP {
		  strcpy (yylval.str, yytext);
        return OBJECT_GROUP;
		  }
NOTIFICATION[-]GROUP {
		  strcpy (yylval.str, yytext);
        return NOTIFICATION_GROUP;
		  }
IMPLIED {
		  strcpy (yylval.str, yytext);
        return IMPLIED;
		  }
NOTIFICATION[-]TYPE {
		  strcpy (yylval.str, yytext);
        return NOTIFICATION_TYPE;
		  }
OBJECTS {
		  strcpy (yylval.str, yytext);
        return OBJECTS;
		  }
REFERENCE {
		  strcpy (yylval.str, yytext);
        return REFERENCE;
		  }
TEXTUAL[-]CONVENTION {
		  strcpy (yylval.str, yytext);
        return TEXTUAL_CONVENTION;
		  }
DISPLAY[-]HINT {
		  strcpy (yylval.str, yytext);
        return DISPLAY_HINT;
		  }
NOTIFICATIONS {
		  strcpy (yylval.str, yytext);
        return NOTIFICATIONS;
		  }
OCTET {
		  strcpy (yylval.str, yytext);
        return OCTET;
		  }
STRING {
		  strcpy (yylval.str, yytext);
        return _STRING;
		  }
SIZE {
		  strcpy (yylval.str, yytext);
        return SIZE;
		  }
0 {
		  yylval.intval = atoi (yytext);
        return _ZERO;
		  }
[0-9]+  {
		  yylval.intval = atoi (yytext);
		  return NUM;
		  }
[a-zA-Z0-9]+ {
        if (strlen (yytext) > MAX_DESC_LEN - 1) {
           yyerror ("Descriptor/Name length should not exceed 64");
        }
        strcpy (yylval.str, yytext);
		  return NAME;
		  }
[A-Za-z0-9][a-zA-Z0-9-]+ {
        if (strlen (yytext) > MAX_DESC_LEN - 1) {
           yyerror ("Descriptor/Name length should not exceed 64");
        }
        strcpy (yylval.str, yytext);
		  return HNAME;
		  }
'[01]*'[Bb]  {
        strcpy (yylval.str, yytext);
		  return BINSTRING;
		  }
'[a-fA-F0-9]*'[Hh]  {
        strcpy (yylval.str, yytext);
		  return HEXSTRING;
		  }
\" {
		INT1 i1_char;

      while ((i1_char = input ()) != '"') {
		   if (i1_char == '\n') {
			   yylineno++;
			}
		}
		return QSTRING;
	}
. return yytext[0];
%%

void read_comment (void)
{
	INT1 i1_char, i1_prev_char = ' ';

	while (i1_char = input ())  {
		if ( i1_char == '\n')
		   break;
		i1_prev_char = i1_char;
	}
	if ( i1_char == '\n') {
	   yylineno++;
	}
	return;
}
