/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: oid_table2.c,v 1.4 2015/04/28 14:43:31 siva Exp $
 *
 * Description: This File Contains the function Which     
 *              parse the Temp Files Generated and use 
 *              them to Generate the snmp-mbdb.h file 
 *              for version 2. 
 *******************************************************************/

# include "include.h"
# include "extern.h"

/*  
 *  This is the main character array which holds the
 *  Oid's which are present in the Temp file 1 created
 *  earlier in the generation process in the fns present
 *  in the file oid_table2.c.
 */
UINT1              *pu1_oidarray[MAX_NO_OF_GROUPS];

/*  
 *  This is the Variable which holds the Value of the 
 *  group which is being formed. This is global to this
 *  file and since recursion is used the value of this
 *  varies during the execution of the function.
 */
INT4                i4_grp = ONE;

/* 
 *  This Tells the No of Oids present in the global
 *  String Array which holds the Oids.
 */
INT4                i4_sizeof_array;

#ifdef FUTURE_SNMP_WANTED

extern INT1        *Name;
extern INT1         UName[50];
UINT1               OUT_FILE[MAX_LINE];

#endif

/******************************************************************************
*      function Name        : find_nof_comma                                  *
*      Role of the function : This fn finds the No of Comma's in the given    *
*                             String.                                         *
*      Formal Parameters    : pu1_string                                      *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : i4_count (Nof Commas in the String.             *
******************************************************************************/
INT4
find_nof_comma (UINT1 *pu1_string)
{
    INT4                i4_count = ZERO;

    while (*pu1_string != NULL_CHAR)
    {
        if (*pu1_string++ == COMMA_CHAR)
        {
            i4_count++;
        }
    }
    return i4_count;
}                                /* End Of Function. */

/******************************************************************************
*      function Name        : nof_grouped_oid                                 *
*      Role of the function : This fn finds the No of Oid's which are present *
*                             in a group which is formed.                     *
*      Formal Parameters    : i4_indexarray[]                                 *
*      Global Variables     : i4_sizeof_array                                 *
*      Use of Recursion     : None                                            *
*      Return Value         : i4_nof_oids (Nof oids in a Group).              *
******************************************************************************/
INT4
nof_grouped_oid (INT4 i4_indexarray[])
{

    INT4                i4_count;
    INT4                i4_nof_oids = ZERO;

    for (i4_count = ZERO; i4_count < i4_sizeof_array; i4_count++)
    {
        if (i4_indexarray[i4_count] == GROUPED)
        {
            i4_nof_oids++;
        }
    }
    return i4_nof_oids;
}                                /* End Of Function. */

/******************************************************************************
*      function Name        : get_oid_fromtempfile                            *
*      Role of the function : This fn Gets the Oids from the Temp File 1 &    *
*                             stores them to the Global Array pu1_oidarray    *
*      Formal Parameters    : pu1_pathname , pu1_filename                     *
*      Global Variables     : pu1_oidarray , i4_sizeof_array                  *
*      Use of Recursion     : None                                            *
*      Return Value         : i4_count (Nof oids present in the file)         * 
******************************************************************************/
INT4
get_oid_fromtempfile (UINT1 *pu1_pathname)
{
    INT4                i4_count = ZERO;
    UINT1               u1_file[MAX_LINE];
    FILE               *t1;
    strcpy (u1_file, pu1_pathname);
    strcat (u1_file, "temp1");
    t1 = fopen (u1_file, READ_MODE);
    if (t1 == NULL)
    {
        printf
            ("\n\n\tError : Cannot Open Temporary File  for generating the MIB data base \n");
        /*This part is commented, can be used for testing */
        /*
         *printf("\n\n\tError : Cannot Open Temp1 File  %s \n",u1_file);
         */
        clear_directory ();
        exit (NEGATIVE);
    }

    while (!feof (t1))
    {

        pu1_oidarray[i4_count] = (UINT1 *) calloc (1, MAX_LINE);

        if (pu1_oidarray[i4_count] == NULL)
        {
            printf ("\n\n\tError : Malloc Failure.\n");
            clear_directory ();
            exit (NEGATIVE);
        }
        if (!fgets (pu1_oidarray[i4_count], MAX_LINE, t1))
        {
            break;
        }
        if (!fgets (pu1_oidarray[i4_count], MAX_LINE, t1))
        {
            break;
        }
        if (!fgets (pu1_oidarray[i4_count], MAX_LINE, t1))
        {
            break;
        }
        pu1_oidarray[i4_count] = strstr (pu1_oidarray[i4_count], OPEN_PARAS);
        pu1_oidarray[i4_count]++;
        pu1_oidarray[i4_count] = strtok (pu1_oidarray[i4_count], CLOSE_PARAS);
        i4_count++;
    }
    fclose (t1);
    return i4_count;
}                                /* End Of Function. */

/******************************************************************************
*      function Name        : find_commonoid                                  *
*      Role of the function : This fn finds the Minimun Common Oid in all the * 
*                             Oids which are present in the pu1_oidarray      *
*      Formal Parameters    : None                                            *
*      Global Variables     : pu1_oidarray , i4_sizeof_array                  *
*      Use of Recursion     : None                                            *
*      Return Value         : i4_commonoid_len(The Common oids Length)        *
******************************************************************************/
INT4
find_commonoid (void)
{
    INT4                i4_count;
    INT4                i4_commonoid_len;
    INT4                i4_small = MAX_LINE;
    UINT1               u1_commonoid[MAX_LINE];
    UINT1              *pu1_temp;

    /* Finding the Smallest Oid length.  */
    for (i4_count = ZERO; i4_count < i4_sizeof_array; i4_count++)
    {
        if (strlen (pu1_oidarray[i4_count]) < i4_small)
        {
            i4_small = strlen (pu1_oidarray[i4_count]);
        }
    }
    strncpy (u1_commonoid, pu1_oidarray[ZERO], i4_small);
    u1_commonoid[i4_small] = NULL_CHAR;
    /* Main While Loop For finding the Common Oid.  */
    while (TRUE)
    {

        i4_count = TRUE;
        i4_commonoid_len = strlen (u1_commonoid);

        for (i4_count = ZERO; i4_count < i4_sizeof_array; i4_count++)
        {
            if (strncmp (pu1_oidarray[i4_count], u1_commonoid, i4_commonoid_len)
                != FALSE)
            {
                break;
            }
        }
        if (i4_count == i4_sizeof_array)
        {
            break;
        }
        else
        {
            pu1_temp = strrchr (u1_commonoid, COMMA_CHAR);
            *pu1_temp = NULL_CHAR;
        }
    }                            /* End Of While Loop. */
    return i4_commonoid_len;
}                                /* End Of Function. */

/******************************************************************************
*      function Name        : find_bigoid_index                               *
*      Role of the function : This fn finds the Biggest Oid in a Grp which is *
*                             specified by the i4_no variable, returns Index. *
*      Formal Parameters    : i4_no , i4_indexarray[]                         *
*      Global Variables     : i4_sizeof_array                                 *
*      Use of Recursion     : None                                            *
*      Return Value         : i4_commonoid_len( The Common oids Length)       *
******************************************************************************/
INT4
find_bigoid_index (INT4 i4_no, INT4 i4_indexarray[])
{
    INT4                i4_count;
    INT4                i4_comma_count;
    INT4                i4_bigindex;
    INT4                i4_big = ZERO;

    for (i4_count = ZERO; i4_count < i4_sizeof_array; i4_count++)
    {
        if (i4_indexarray[i4_count] != i4_no)
        {
            continue;
        }
        i4_comma_count = find_nof_comma (pu1_oidarray[i4_count]);
        if (i4_comma_count > i4_big)
        {
            i4_big = i4_comma_count;
            i4_bigindex = i4_count;
        }
    }                            /* End Of For Loop. */
    return i4_bigindex;
}                                /* End of Function. */

/******************************************************************************
*      function Name        : find_next_nongrouped_index                      *
*      Role of the function : This fn finds the Next Non Grouped Oid from the *
*                             i4_indexarray , return the Index.               *
*      Formal Parameters    : i4_indexarray[]                                 *
*      Global Variables     : i4_sizeof_array                                 *
*      Use of Recursion     : None                                            *
*      Return Value         : i4_index( The Index of Next non grouped oids)   *
******************************************************************************/
INT4
find_next_nongrouped_index (INT4 i4_indexarray[])
{
    INT4                i4_count;
    INT4                i4_index = NEGATIVE;

    for (i4_count = ZERO; i4_count < i4_sizeof_array; i4_count++)
    {
        /* searching the elements which are not grouped */
        if (i4_indexarray[i4_count] == ZERO)
        {
            i4_index = i4_count;
            /* Break After an element is found.  */
            break;
        }
    }                            /* End Of For Loop. */
    return i4_index;
}                                /* End of Function. */

/******************************************************************************
*      function Name        : find_next_nongrouped_ingrp                      *
*      Role of the function : This fn finds the Next Non Grouped in the grp   *
*                             specified in the i4_grpno variableOid from the  *
*                             i4_indexarray , return the Index.               *
*      Formal Parameters    : i4_grpno , i4_indexarray[]                      *
*      Global Variables     : i4_sizeof_array                                 *
*      Use of Recursion     : None                                            *
*      Return Value         : i4_index (The Index of Next non grouped         *
*                             oids in a grp specified by i4_grpno)            *
******************************************************************************/
INT4
find_next_nongrouped_ingrp (INT4 i4_grpno, INT4 i4_indexarray[])
{
    INT4                i4_count;

    for (i4_count = ZERO; i4_count < i4_sizeof_array; i4_count++)
    {

        /* Searching the Elements which are Not Grouped.  */
        if (i4_indexarray[i4_count] != i4_grpno)
        {
            continue;
        }
        else
        {
            /* Break After an element is found.  */
            break;
        }
    }                            /* End Of For Loop. */
    if (i4_count == i4_sizeof_array)
    {
        return NO_ELEMENT_FOUND;
    }
    else
    {
        return ELEMENT_FOUND;
    }
}                                /* End of Function. */

/******************************************************************************
*      function Name        : fill_with_groupno                               *
*      Role of the function : This fn fills the i4_indexarray with the        *
*                             i4_grpno.                                       *
*                             i4_indexarray , return the Index.               *
*      Formal Parameters    : i4_grpno , i4_indexarray[]                      *
*      Global Variables     : i4_sizeof_array                                 *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/
void
fill_with_groupno (INT4 i4_grpno, INT4 i4_indexarray[])
{
    INT4                i4_count;

    for (i4_count = ZERO; i4_count < i4_sizeof_array; i4_count++)
    {
        if (i4_indexarray[i4_count] == ZERO)
        {
            i4_indexarray[i4_count] = i4_grpno;
        }
    }
}                                /* End Of Function. */

/******************************************************************************
*     function Name        : store_grpno                                      *
*     Role of the function : This fn fills the i4_indexarray with the i4_grpno*
*                            to the elements which have the Status GROUPED &  *
*                            stores all the other Oids with the same grpno    *
*                            ZERO and doesn't change other Oids.              *
*     Formal Parameters    : i4_grpno , i4_indexarray[]                       *
*     Global Variables     : i4_sizeof_array                                  *
*     Use of Recursion     : None                                             *
*     Return Value         : None                                             *
******************************************************************************/
void
store_grpno (INT4 i4_grp, INT4 i4_indexarray[])
{
    INT4                i4_count;

    /* 
     *  FOR loop for storing the elements which are NOT
     *  grouped in that particular group to ZERO.
     */
    for (i4_count = ZERO; i4_count < i4_sizeof_array; i4_count++)
    {
        if (i4_indexarray[i4_count] == i4_grp)
        {
            i4_indexarray[i4_count] = ZERO;
        }
    }                            /* End Of For Loop. */

    /* 
     *  FOR loop for storing the elements having 
     *  the status GROUPED to the Group Number (i4_grp).
     */
    for (i4_count = ZERO; i4_count < i4_sizeof_array; i4_count++)
    {
        if (i4_indexarray[i4_count] == GROUPED)
        {
            i4_indexarray[i4_count] = i4_grp;
        }
    }                            /* End Of For Loop. */
}                                /* End Of Function. */

/******************************************************************************
*      function Name        : find_before_oidlen                              *
*      Role of the function : This fn given a Oid and a length finds the      *
*                             length of the Before Oid.                       *
*                             i4_indexarray , return the Index.               *
*      Formal Parameters    : pu1_string , i4_oidlen                          *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/
INT4
find_before_oidlen (UINT1 *pu1_string, INT4 i4_oidlen)
{
    INT4                i4_count;

    i4_count = i4_oidlen - ONE;

    while (pu1_string[i4_count] != COMMA_CHAR)
    {
        i4_count--;
    }

    return i4_count;

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : find_matching_oid                               *
*      Role of the function : This Fn Takes the biggest Oid's Index & searches*
*                             for Presence of Matching Oids in the given grp. *
*      Formal Parameters    : i4_grp , i4_oidlen , i4_bigoid_index ,          *
*                             i4_indexarray[]                                 *
*      Global Variables     : i4_grp                                          *
*      Use of Recursion     : None                                            *
*      Return Value         : MATCH_FOUND if grp formed else MATCH_NOT_FOUND  *
******************************************************************************/
INT4
find_matching_oid (INT4 i4_grp, INT4 i4_oidlen, INT4 i4_bigoid_index,
                   INT4 i4_indexarray[])
{
    INT4                i4_count;
    INT4                i4_grpflag = ZERO;
    UINT1               u1_string[MAX_LINE] = NULL_STR;

    /*  Copying the Longest Oid into a char array.  */
    strncpy (u1_string, pu1_oidarray[i4_bigoid_index], i4_oidlen);

    for (i4_count = ZERO; i4_count < i4_sizeof_array; i4_count++)
    {
        if (i4_indexarray[i4_count] == i4_grp)
        {
            /*
             *  Continue when encountered the largest OID
             *  with which the comparison is done.
             */
            if (i4_count == i4_bigoid_index)
            {
                continue;
            }

            /* 
             *  Continue when the Len of the Oid which is
             *  being compared is less than the Biggest Oid.
             */
            if (strlen (pu1_oidarray[i4_count]) < i4_oidlen)
            {
                continue;
            }

            /* Comparing the Biggest Oid with the present Oid.  */
            if (strncmp (u1_string, pu1_oidarray[i4_count],
                         strlen (u1_string)) == ZERO)
            {
                /*
                 * first condition added since it was not grouping an OID,
                 * with last character '\0', into this group
                 * added by soma on 03/04/98
                 */
                if ((pu1_oidarray[i4_count][strlen (u1_string)] != NULL_CHAR) &&
                    (pu1_oidarray[i4_count][strlen (u1_string)] != COMMA_CHAR))
                {
                    continue;
                }
                /*  assigning the status array to GROUPED */
                i4_indexarray[i4_count] = GROUPED;
                i4_grpflag++;
            }
        }                        /* End of if */
    }                            /* End of for loop */

    if (i4_grpflag == ZERO)
    {
        return NO_MATCH_FOUND;
    }
    else
    {
        /*
         *  Storing the Group Number for the Oid with
         *  which the Comparision is being done.
         */
        i4_indexarray[i4_bigoid_index] = GROUPED;
        return MATCH_FOUND;
    }
}                                /* End Of Function. */

/******************************************************************************
*      function Name        : form_groups                                     *
*      Role of the function : This Fn forms the Grps with the elements of the *
*                             indexarray identified by the grp no Var i4_grp. *
*      Formal Parameters    : i4_grp , i4_commonoid_len , i4_indexarray[]     *
*      Global Variables     : i4_grp                                          *
*      Use of Recursion     : Yes                                             *
*      Return Value         : i4_oidlen (common oid len of the grp formed)    *
******************************************************************************/
INT4
form_groups (INT4 i4_grp, INT4 i4_commonoid_len, INT4 i4_indexarray[])
{
    INT4                i4_bigoid_index;
    INT4                i4_oidlen;
    INT4                i4_nof_elements;
    INT4                i4_matchflag;

    /* Filling the Index_array with the i4_grp number */
    fill_with_groupno (i4_grp, i4_indexarray);

    /* Finding the largest OID in that Group */
    i4_bigoid_index = find_bigoid_index (i4_grp, i4_indexarray);

    /* Finding the length of the largest OID in that Group */
    i4_oidlen = strlen (pu1_oidarray[i4_bigoid_index]);

    do
    {
        /*  
         *  This Fn finds whether any oid with common
         *  parts are present and assigns the grp No to it.
         */
        i4_matchflag =
            find_matching_oid (i4_grp, i4_oidlen, i4_bigoid_index,
                               i4_indexarray);
        if (i4_matchflag == NO_MATCH_FOUND)
        {
            if (i4_oidlen != i4_commonoid_len)
            {
                i4_oidlen =
                    find_before_oidlen (pu1_oidarray[i4_bigoid_index],
                                        i4_oidlen);
            }
            if (i4_oidlen == i4_commonoid_len)
            {
                /* 
                 *  Assigning the Negative Value of the Group Number
                 *  if the Biggest Oid doesn't form any Group.  
                 */
                i4_indexarray[i4_bigoid_index] = i4_grp * NEGATIVE;
                break;
            }
        }                        /* End of IF for Match Found. */
    }
    while (i4_matchflag == NO_MATCH_FOUND);

    /*  
     *  Finding the Number of elements which are
     *  grouped (i.e having the status GROUPED).
     */
    i4_nof_elements = nof_grouped_oid (i4_indexarray);

    /* 
     *  Storing the Group Number in the Index Array Respective to
     *  the elements which belong to the Group in the i4_grp .
     */
    if (i4_nof_elements >= ZERO)
    {
        store_grpno (i4_grp, i4_indexarray);
    }
    return i4_oidlen;
}                                /* End Of Function */

/******************************************************************************
*      function Name        : find_suboid_len                                 *
*      Role of the function : This fn finds the SubOid Length of the Table .  * 
*      Formal Parameters    : i4_indexarray, i4_count, i2_goid_len            *
*      Global Variables     : pu1_oidarray                                    *
*      Return Value         : i4_suboid_len(Sub Oid Length)                   *
******************************************************************************/
INT4
find_suboid_len (INT4 i4_indexarray[], INT4 i4_count, INT2 i2_goid_len)
{
    INT4                i4_grpno;
    INT4                i4_grpoid_len;
    INT4                i4_suboid_count = ZERO;

    /*  
     *  This Case is Only when there is a Single Table. 
     *  This is done since The tool generates a group
     *  even if there is only one Table in the MIB.
     */
    /*
     * return value has to be changed from '1' to '0', which gave incorrect
     * base oid length earlier
     * changed by soma on 06/03/98
     */
    if (i4_sizeof_array == ONE)
    {
        if (i2_goid_len == 0)
        {
            return ZERO;
        }
        else
        {
            return (find_nof_comma (pu1_oidarray[i4_count]) + 1 - i2_goid_len);
        }
    }
    i4_grpno = i4_indexarray[i4_count];
    if (i2_goid_len == 0)
    {
        i4_grpoid_len = find_lenof_groupoid (i4_indexarray, i4_grpno);
    }
    else
    {
        i4_grpoid_len =
            get_group_oid_len (i2_goid_len, i4_grpno, i4_indexarray);
    }

    /*  Finding the No of Sub Oids after the Group Oid Length. */
    while (pu1_oidarray[i4_count][i4_grpoid_len] != NULL_CHAR)
    {
        if (pu1_oidarray[i4_count][i4_grpoid_len++] == COMMA_CHAR)
        {
            i4_suboid_count++;
        }
    }

    /* Returning the Nof oid After the Group Oid.  */
    return i4_suboid_count;
}                                /* End Of Function. */

/******************************************************************************
*      function Name        :  find_nof_subgrps                               *
*      Role of the function : This fn finds the No of Subgrps which are       *
*                             formed from a Group.                            *
*      Formal Parameters    : i4_indexarray , i4_grpno                        *
*      Global Variables     : i4_sizeof_array                                 *
*      Use of Recursion     : None                                            *
*      Return Value         : i4_nof_subgrps (Nof Groups if Found Else Zero.) *
******************************************************************************/
INT4
find_nof_subgrps (INT4 i4_indexarray[], INT4 i4_grpno)
{
    INT4                i4_nof_subgrps = ZERO;
    INT4                i4_count;

    /*  If the Number of Oids is one then there will be no sub Groups.  */
    if (i4_sizeof_array == ONE)
    {
        return ZERO;
    }
    for (i4_count = ZERO; i4_count < i4_sizeof_array; i4_count++)
    {
        if (i4_indexarray[i4_count] == i4_grpno)
        {
            i4_nof_subgrps++;
        }
    }
    return i4_nof_subgrps;
}                                /* End Of Function. */

/******************************************************************************
*      function Name        : find_lenof_groupoid                             *
*      Role of the function : This fn finds the Len of the Grp Given by grpno.*
*      Formal Parameters    : i4_indexarray , i4_grpno                        *
*      Global Variables     : i4_sizeof_array, pu1_oidarray                   *
*      Use of Recursion     : None                                            *
*      Return Value         : i4_nof_subgrps (Nof Groups if Found Else Zero.) *
******************************************************************************/
INT4
find_lenof_groupoid (INT4 i4_indexarray[], INT4 i4_grpno)
{
    INT4                i4_count;
    INT4                i4_index;
    INT4                i4_oidlen = ZERO;
    INT4                i4_flag = GROUP_OID_LEN_NOT_FOUND;
    UINT1               u1_string[MAX_LINE] = NULL_STR;

    /* Find an Element in the Group given in the i4_grpno.  */
    for (i4_count = ZERO; i4_count < i4_sizeof_array; i4_count++)
    {
        if (i4_indexarray[i4_count] == i4_grpno)
        {
            break;
        }
    }

    /* If no OID with this Group is found. */
    if (i4_count == i4_sizeof_array)
    {
        return ZERO;
    }

    /*  Storing the oid of that group in the temp string.  */
    strcpy (u1_string, pu1_oidarray[i4_count]);

    i4_index = i4_count;
    i4_oidlen = strlen (u1_string);

    /*  If the Number of Objects is ONE then return the Len of the OID. */
    if (i4_sizeof_array == ONE)
    {
        return i4_oidlen;
    }
    /*  Main While Loop for finding the Common Oid in a Particular Group.  */
    while (i4_flag == GROUP_OID_LEN_NOT_FOUND)
    {
        /*  
         *  FOR Loop for checking whether all the Oid's with a particular
         *  length (given by i4_oidlen) matches if no match is found then
         *  the the Before Oid is found and the process is continued else
         *  the i4_flag is set or GROUP_OID_LEN_FOUND.
         */
        for (i4_count = ZERO; i4_count < i4_sizeof_array; i4_count++)
        {
            if (i4_indexarray[i4_count] == i4_grpno)
            {
                if (strncmp (pu1_oidarray[i4_count], u1_string, i4_oidlen) !=
                    ZERO)
                {
                    break;
                }
            }
        }
        if (i4_count == i4_sizeof_array)
        {
            i4_flag = GROUP_OID_LEN_FOUND;
        }
        else
        {
            /*  
             *  The Before Oid length is found & copied 
             *  into the u1_string variable.
             */
            i4_oidlen = find_before_oidlen (u1_string, i4_oidlen);
            strcpy (u1_string, NULL_STR);
            strncpy (u1_string, pu1_oidarray[i4_index], i4_oidlen);
        }
    }                            /* End Of While Loop. */
    /*  Returning the Oid Length.  */
    return i4_oidlen;
}                                /* End Of Function. */

/******************************************************************************
*      function Name        : get_tablename_from_tempfile                     *
*      Role of the function : This fn Gets the Table Name From the Temp file  *
*                             till the i4_offset No of Lines.                 *
*      Formal Parameters    : i4_offset                                       *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : The Line with the Table Name from Temp File.    *
******************************************************************************/
UINT1              *
get_tablename_from_tempfile (INT4 i4_offset, UINT1 *pu1_pathname)
{
    UINT1               u1_file[MAX_LINE];
    UINT1               u1_temp[MAX_LINE];
    UINT1              *pu1_string;
    INT4                i4_count;
    FILE               *t1;

    strcpy (u1_file, pu1_pathname);
    strcat (u1_file, "temp1");

    /* Opening the Temp File.  */
    t1 = fopen (u1_file, READ_MODE);
    if (t1 == NULL)
    {
        printf
            ("\n\n\tError : Cannot Open Temporary File  for generating the MIB data base \n");
        /*This part is commented, can be used for testing */
        /*
         * printf("\n\n\tERROR : Unable to Open Temp File 1\n");
         */
        clear_directory ();
        exit (NEGATIVE);
    }

    for (i4_count = ZERO; i4_count < i4_offset - ONE; i4_count++)
    {
        fgets (u1_temp, MAX_LINE, t1);
        fgets (u1_temp, MAX_LINE, t1);
        fgets (u1_temp, MAX_LINE, t1);
    }

    fgets (u1_temp, MAX_LINE, t1);
    pu1_string = get_line (t1);
    fgets (u1_temp, MAX_LINE, t1);

    fclose (t1);
    return pu1_string;

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : fnptr_for_basoidtable                           *
*      Role of the function : This fn writes to the snmp-mbdb.h file (for v2) *
*                             the Fn Ptrs in the Base Oid Table according to  *
*                             the Read Only Flag.                             *
*      Formal Parameters    : mbdb , pu1_entryname , i4_readonly_flag         *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/
void
fnptr_for_basoidtable (FILE * mbdb, UINT1 *pu1_entryname, INT4 i4_readonly_flag)
{
    if (i4_readonly_flag != 3)
    {
        fprintf (mbdb, "%s", pu1_entryname);
        fprintf (mbdb, "Get,\n");
    }
    else
    {
        fprintf (mbdb, "NULLVBF,\n");
    }

    if (i4_readonly_flag == FALSE)
    {
        fprintf (mbdb, "%s", pu1_entryname);
        fprintf (mbdb, "Test,\n");
        fprintf (mbdb, "%s", pu1_entryname);
        fprintf (mbdb, "Set,\n");
    }
    else
    {
        fprintf (mbdb, "NULLF,\n");
        fprintf (mbdb, "NULLF,\n");
    }

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : write_oidarray                                  *
*      Role of the function : This fn writes the Oid Array for the Grp which  *
*                             are formed.                                     *
*      Formal Parameters    : i4_indexarray, pu1_filename,                    *
*                             pu1_pathname, i2_goid_len, i1_code_for_agent    *
*      Global Variables     : i4_grp                                          *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/
void
write_oidarray (INT4 i4_indexarray[], UINT1 *pu1_pathname,
                UINT1 *pu1_filename, INT2 i2_goid_len, INT4 i4_argv_count,
                INT1 u1_parse_files[][MAX_LINE], INT1 i1_code_for_agent)
{
    UINT1               u1_file[MAX_LINE];
    INT4                i4_oidlen;
    INT4                i4_index = FALSE;
    INT4                i4_negative_flag = FALSE;
    INT4                i4_count;
    INT4                i4_counter;
    UINT1               u1_string[MAX_LINE];
    FILE               *mbdb;
#ifdef FUTURE_SNMP_WANTED
    INT1                fName[MAXINFILE][MAX_LINE];
    UINT1               i;
    UINT4               j;
#endif

    strcpy (u1_file, pu1_pathname);
    strcat (u1_file, pu1_filename);
    strcat (u1_file, MBDB_EXTN_H);

#ifdef FUTURE_SNMP_WANTED

    for (i = 0; i < MAXINFILE; i++)
    {
        for (j = 0; j < MAX_LINE; j++)
        {
            fName[i][j] = u1_parse_files[i][j];
        }
    }
    u1_parse_files[1][5] = '\0';
    fName[1][5] = '\0';
    strcpy (OUT_FILE, u1_file);

#endif
    /* Opening the mdbd file in Write Mode.  */
    mbdb = fopen (u1_file, WRITE_MODE);
    if (mbdb == NULL)
    {
        printf ("\n\t ERROR: Unable to Open %s_mbdb.h \n", pu1_filename);
        clear_directory ();
        exit (NEGATIVE);
    }

    fprintf (mbdb, "\n/*****");
    fprintf (mbdb, "\n  %smdb.h . This File Contains the Mib DataBase.",
             fName[1]);
    fprintf (mbdb, "\n*****/\n");

#ifdef FUTURE_SNMP_WANTED

    UName[5] = '\0';
    fprintf (mbdb, "\n#ifndef _%sMDB_H", UName);
    fprintf (mbdb, "\n#define _%sMDB_H", UName);

#else

    fprintf (mbdb, "\n#ifndef _SNMP_MBDB_H");
    fprintf (mbdb, "\n#define _SNMP_MBDB_H");

#endif

    fprintf (mbdb, "\n\n/*  The NULL FUNCTION POINTER.  */\n");
    fprintf (mbdb, DEFS "NULLF ARG_LIST(((INT4 (*)(tSNMP_OID_TYPE *,\\");
    fprintf (mbdb,
             "\n                                          tSNMP_OID_TYPE *,\\");
    fprintf (mbdb, "\n                                          UINT1,\\");
    fprintf (mbdb,
             "\n                                          tSNMP_MULTI_DATA_TYPE *)) NULL))");

    fprintf (mbdb, "\n/*  NULL VARBIND FUNCTION POINTER . */\n");
    fprintf (mbdb,
             DEFS
             "NULLVBF  ARG_LIST(((tSNMP_VAR_BIND* (*)(tSNMP_OID_TYPE *,\\");
    fprintf (mbdb,
             "\n                                           tSNMP_OID_TYPE *,\\");
    fprintf (mbdb, "\n                                           UINT1,\\");
    fprintf (mbdb,
             "\n                                           UINT1))NULL))\n");

    for (i4_count = 1; i4_count < i4_argv_count; i4_count++)
    {
        fName[i4_count][5] = '\0';
        fprintf (mbdb, INCS "\"%smid.h\"\n", fName[i4_count]);
        fprintf (mbdb, INCS "\"%scon.h\"\n", fName[i4_count]);
        fprintf (mbdb, INCS "\"%sogi.h\"\n", fName[i4_count]);
    }

    fprintf (mbdb, "\n/*  The Declaration of the Group Arrays. */\n\n");

    i4_count = ONE;
    /* Main DO Loop for storing in the File the Group Oid Array's.  */
    do
    {
        /* Fn Which finds the Length of the Oid's of a Group.  */
        /*
         * if condition added to fix length of group OID
         * added by soma on 14/04/98
         */
        if (i2_goid_len == ZERO)
        {
            i4_oidlen = find_lenof_groupoid (i4_indexarray, i4_count);
        }
        else
        {
            i4_oidlen =
                get_group_oid_len (i2_goid_len, i4_count, i4_indexarray);
        }

        i4_index = i4_count;

        /*
         *  This FOR loop Finds the Oid in a Grp and stores in the
         *  u1_string the string of length equal to groupoid len.
         */
        for (i4_counter = ZERO; i4_counter < i4_sizeof_array; i4_counter++)
        {
            if (i4_indexarray[i4_counter] == i4_index)
            {
                break;
            }
        }
        strncpy (u1_string, pu1_oidarray[i4_counter], i4_oidlen);
        u1_string[i4_oidlen] = NULL_CHAR;

#ifdef FUTURE_SNMP_WANTED
        if (i1_code_for_agent == TRUE)
            fprintf (mbdb, "UINT4 au4_%s_%s_TABLE%d[] = {", Name, pu1_filename,
                     i4_count);
        else
            fprintf (mbdb, "UINT4 au4_%s_%ssa_TABLE%d[] = {", Name,
                     pu1_filename, i4_count);
#else
        if (i1_code_for_agent == TRUE)
            fprintf (mbdb, "UINT4 au4_%s_TABLE%d[] = {", pu1_filename,
                     i4_count);
        else
            fprintf (mbdb, "UINT4 au4_%ssa_TABLE%d[] = {", pu1_filename,
                     i4_count);
#endif

        fprintf (mbdb, "%s", u1_string);
        fprintf (mbdb, "};\n");
        i4_negative_flag = FALSE;
        i4_count++;
    }
    while (i4_count < i4_grp);    /* End Of DO Loop. */
    fclose (mbdb);
}                                /* End Of Function. */

/******************************************************************************
*      function Name        : write_suboidarray                               *
*      Role of the function : This fn writes the Sub Oid Array for the Grp    *
*                             which is formed.                                *
*      Formal Parameters    : i4_indexarray, pu1_filename,                    *
*                             pu1_pathname, i2_goid_len                       *
*      Global Variables     : i4_grp ,i4_sizeof_array                         *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/
void
write_suboidarray (INT4 i4_indexarray[], UINT1 *pu1_pathname,
                   UINT1 *pu1_filename, INT2 i2_goid_len)
{
    UINT1               u1_file[MAX_LINE];
    INT4                i4_count;
    INT4                i4_oidlen = ZERO;
    INT4                i4_grpno;
    UINT1              *pu1_string;
    UINT1               u1_temp[MAX_LINE] = NULL_STR;
    FILE               *mbdb;

    strcpy (u1_file, pu1_pathname);
    strcat (u1_file, pu1_filename);
    strcat (u1_file, MBDB_EXTN_H);

    /* 
     *  Opening the mdbd file in Write (Append) Mode inorder to put the 
     *  Sub Oid's at the End of the Previously written MBDB file. 
     */
    mbdb = fopen (u1_file, APPEND_MODE);
    if (mbdb == NULL)
    {
        printf ("\n\t ERROR: Unable to Open %s_mbdb.h file Ver 2\n",
                pu1_filename);
        clear_directory ();
        exit (NEGATIVE);
    }

    fprintf (mbdb, "\n/*  The Declaration of the SubGroup Arrays. */\n\n");
    for (i4_count = ZERO; i4_count < i4_sizeof_array; i4_count++)
    {

        /* 
         *  This Fn Finds the Table Name which is Present in the
         *  temp file. The No of the Table is given as the Input.
         *  The i4_count is Incremented by ONE because the Tables
         *  Start from ONE.
         */
        pu1_string = get_tablename_from_tempfile (i4_count + ONE, pu1_pathname);

        pu1_string = strtok (pu1_string, NEWLINE_STRING);

        fprintf (mbdb, "%s", pu1_string);
        fprintf (mbdb, "{");

        /*  This Fn Finds the Length of the Common Oid in a Group.  */
        i4_grpno = i4_indexarray[i4_count];

        /*
         * if condition added to fix length of group OID
         * added by soma on 14/04/98
         */
        /* Finding the Length of the Group Oid. */
        if (i2_goid_len == ZERO)
        {
            i4_oidlen = find_lenof_groupoid (i4_indexarray, i4_grpno);
        }
        else
        {
            i4_oidlen =
                get_group_oid_len (i2_goid_len, i4_grpno, i4_indexarray);
        }
        strcpy (u1_temp, pu1_oidarray[i4_count]);

        /* 
         * this condition is added to fix the problem of generating an
         * invalid base oid where it has to be null. If this condition is
         * is not there it prints whatever is there after '\0' in u1_temp
         * in case it has to be null base oid. In case if base oid is not
         * null comma is skipped and remaining oid is printed
         * added by soma on 06/03/98
         */

        if (u1_temp[i4_oidlen] != '\0')
        {
            fprintf (mbdb, "%s", &u1_temp[i4_oidlen + ONE]);
        }
        else
        {
            fprintf (mbdb, "0");
        }
        fprintf (mbdb, "};\n");
        fflush (mbdb);
    }                            /* End Of FOR Loop. */
    fclose (mbdb);
}                                /* End Of Function. */

/******************************************************************************
*      function Name        : write_grpoid_table                              *
*      Role of the function : This fn writes to the snmp-mbdb.h file (for v2) *
*                             the Group Oid Table.                            *
*      Formal Parameters    : i4_indexarray, pu1_filename, pu1_pathname,      *
*                             i1_code_for_agent, i2_goid_len                  *
*      Global Variables     : i4_grp                                          *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/
void
write_grpoid_table (INT4 i4_indexarray[], UINT1 *pu1_pathname,
                    UINT1 *pu1_filename, INT1 i1_code_for_agent,
                    INT2 i2_goid_len)
{
    UINT1               u1_file[MAX_LINE];
    INT4                i4_count;
    INT4                i4_counter;
    INT4                i4_oidlen;
    INT4                i4_len = ZERO;
    INT4                i4_index;
    INT4                i4_nof_subgrp;

    UINT1               u1_string[MAX_LINE] = NULL_STR;
    FILE               *mbdb;

    strcpy (u1_file, pu1_pathname);
    strcat (u1_file, pu1_filename);
    strcat (u1_file, MBDB_EXTN_H);

    /* Opening the mbdb file in Write Mode.  */
    mbdb = fopen (u1_file, APPEND_MODE);
    if (mbdb == NULL)
    {
        printf ("\n\t ERROR: Unable to Open %s_mbdb.h file \n", pu1_filename);
        clear_directory ();
        exit (NEGATIVE);
    }
    fprintf (mbdb, "\n/*  Declaration of Group OID Table. */");

    /*
     * description of the group OID table
     * added by soma on 07/05/98
     */
    fprintf (mbdb,
             "\n/* Each entry contains Length of Group OID, Pointer to the Group OID,");
    fprintf (mbdb,
             "\n   Priority of the registering subagent, Timeout for response,");
    fprintf (mbdb, "\n   and Number of Subgroups in that order */\n");

    /* condition inserted here to support the -s option, which indicates
     * that middle level code is going to be used by Subagent
     * added by soma on 12/03/98
     */

#ifdef FUTURE_SNMP_WANTED

    if (i1_code_for_agent == TRUE)
    {
        fprintf (mbdb,
                 "\n tSNMP_GroupOIDType %s_FMAS_GroupOIDTable[] =\n{\n", Name);
    }
    else
    {
        fprintf (mbdb,
                 "\n tSNMP_GroupOIDType %s_FMASSA_GroupOIDTable[] =\n{\n",
                 Name);
    }

#else

    if (i1_code_for_agent == TRUE)
    {
        fprintf (mbdb, "\n tSNMP_GroupOIDType FMAS_GroupOIDTable[] =\n{\n");
    }
    else
    {
        fprintf (mbdb, "\n tSNMP_GroupOIDType FMASSA_GroupOIDTable[] =\n{\n");
    }

#endif

    i4_count = ONE;
    /*  Main DO Loop for Forming the Group Oid Table.  */
    do
    {
        fprintf (mbdb, "   {");

        /*  Finding the Oid Length of a Group.  */
        if (i2_goid_len == ZERO)
        {
            i4_oidlen = find_lenof_groupoid (i4_indexarray, i4_count);
        }
        else
        {
            i4_oidlen =
                get_group_oid_len (i2_goid_len, i4_count, i4_indexarray);
        }
        i4_index = i4_count;

        /* Finding the Index of an element.(with group as i4_count) .  */
        for (i4_counter = ZERO; i4_counter < i4_sizeof_array; i4_counter++)
        {
            if (i4_indexarray[i4_counter] == i4_index)
            {
                break;
            }
        }

        /*  Copying the Oid into a Temp Array.  */
        strcpy (u1_string, pu1_oidarray[i4_counter]);

        /* 
         *  Finding the Number of Oid's in the Length
         *  (by counting the Comma's).
         */
        for (i4_len = ONE, i4_counter = ZERO; i4_counter < i4_oidlen;
             i4_counter++)
        {
            if (u1_string[i4_counter] == COMMA_CHAR)
            {
                i4_len++;
            }
        }
        fprintf (mbdb, "%d , ", i4_len);

#ifdef FUTURE_SNMP_WANTED
        if (i1_code_for_agent == TRUE)
            fprintf (mbdb, "au4_%s_%s_TABLE%d , ", Name, pu1_filename,
                     i4_count);
        else
            fprintf (mbdb, "au4_%s_%ssa_TABLE%d , ", Name, pu1_filename,
                     i4_count);
#else
        if (i1_code_for_agent == TRUE)
            fprintf (mbdb, "au4_%s_TABLE%d , ", pu1_filename, i4_count);
        else
            fprintf (mbdb, "au4_%ssa_TABLE%d , ", pu1_filename, i4_count);
#endif

        fprintf (mbdb, "%d , ", MBDB_VER2_PRIORITY);
        fprintf (mbdb, "%d , ", MBDB_VER2_TIMEOUT);

        /*  This Fn Finds the No of Subgrp's in a Group. */
        i4_nof_subgrp = find_nof_subgrps (i4_indexarray, i4_count);

        /*  
         *  This Case is Only when there is a Single Table. 
         *  This is done since The tool generates a group
         *  even if there is only one Table in the MIB.
         */
        if (i4_sizeof_array == ONE)
        {
            i4_nof_subgrp = ONE;
        }

        fprintf (mbdb, "%d", i4_nof_subgrp);
        fprintf (mbdb, "}");
        if (i4_count < i4_grp - ONE)
        {
            fprintf (mbdb, ",\n");
        }

        /*  Incrementing the Counter.  */
        i4_count++;
    }
    while (i4_count < i4_grp);
    /* End Of Do while Loop. */

    fprintf (mbdb, "\n};");
    fclose (mbdb);

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : write_baseoid_table                             *
*      Role of the function : This fn writes to the snmp-mbdb.h file (for v2) *
*                             the Base Oid Table.                             *
*      Formal Parameters    : i4_indexarray, pu1_pathname, pu1_filename,      *
*                             i1_code_for_agent, i2_goid_len                  *
*      Global Variables     : i4_sizeof_array, i4_grp                         *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/
void
write_baseoid_table (INT4 i4_indexarray[], UINT1 *pu1_pathname,
                     UINT1 *pu1_filename, INT1 i1_code_for_agent,
                     INT2 i2_goid_len)
{
    UINT1               u1_file[MAX_LINE];
    INT4                i4_count;
    INT4                i4_counter;
    INT4                i4_len_suboid;
    INT4                i4_nof_objects;
    INT4                i4_readonly_flag;
    UINT1              *pu1_string;
    UINT1              *pu1_temp;
    UINT1              *pu1_temp1;
    FILE               *mbdb;
    FILE               *t4;
    /*
     * base oid table, FMAS_BaseOIDTable, was incorrectly generated with
     * commas after last element this variable is used to solve the problem
     * added by soma on 06/03/98
     */
    INT4                i4_tables_written = 0;

    /*
     * middle level function pointers in base oid table were incorrectly 
     * generated, this variable is used to solve the problem
     * added by soma on 11/03/98
     */
    INT4                i4_index;

    /*
     * these variables are used to change the order of the objects while 
     * writing to FMAS_MIBObjectTable
     * added by soma on 12/03/98
     */
    UINT1              *pu1_table_name;
    UINT1               u1_temp[MAX_LINE];
    INT4                i4_objects_written = 0;
    INT4                i4_temp_index;
    FILE               *t3;
    FILE               *t5;

    strcpy (u1_file, pu1_pathname);
    strcat (u1_file, pu1_filename);
    strcat (u1_file, MBDB_EXTN_H);

    /* Opening the mbdb file in Append Mode.  */
    mbdb = fopen (u1_file, APPEND_MODE);
    if (mbdb == NULL)
    {
        printf ("\n\t ERROR: Unable to Open %s_mbdb.h \n", pu1_filename);
        clear_directory ();
        exit (NEGATIVE);
    }

    /* Opening the Temp file 4 in Read Mode.  */
    strcpy (u1_file, pu1_pathname);
    strcat (u1_file, "temp4");

    t4 = fopen (u1_file, READ_MODE);
    if (t4 == NULL)
    {
        printf
            ("\n\n\tError : Cannot Open Temporary File  for generating the MIB data base \n");
        /*This part is commented, can be used for testing */
        /*
         *printf("\n\t ERROR: Unable to Open Temp File 4 for Ver 2\n");
         */
        clear_directory ();
        exit (NEGATIVE);
    }

    /*
     * object details are read from temp3 file and written to temp5
     * file in the correct order
     * code added by soma on 12/03/98
     */
    /* Opening the Temp file 3 in Read Mode.  */
    strcpy (u1_file, pu1_pathname);
    strcat (u1_file, "temp3");

    t3 = fopen (u1_file, READ_MODE);
    if (t3 == NULL)
    {
        printf
            ("\n\n\tError : Cannot Open Temporary File  for generating the MIB data base \n");
        /*This part is commented, can be used for testing */
        /* 
         *printf("\n\t ERROR: Unable to Open Temp File 3 for Ver 2\n");
         */
        clear_directory ();
        exit (NEGATIVE);
    }

    /* Opening the Temp file 5 in Write Mode.  */
    strcpy (u1_file, pu1_pathname);
    strcat (u1_file, "temp5");

    t5 = fopen (u1_file, WRITE_MODE);
    if (t5 == NULL)
    {
        printf
            ("\n\n\tError : Cannot Open Temporary File  for generating the MIB data base \n");
        /*This part is commented, can be used for testing */
        /*
         *printf("\n\t ERROR: Unable to Open Temp File 5 for Ver 2\n");
         */
        clear_directory ();
        exit (NEGATIVE);
    }
    fprintf (t5, "\n/* Declaration of MIB Object Table. */");

    /*
     * description of the MIB object table
     * added by soma on 07/05/98
     */
    fprintf (t5, "\n/* Each entry contains Name of the table, Permissions for");
    fprintf (t5, "\n   the object and Object name in that order */\n");

    fprintf (t5, "\n tSNMP_MIBObjectDescrType");
    /* condition inserted here to support the -s option, which indicates
     * that middle level code is going to be used by Subagent
     * added by soma on 12/03/98
     */
#ifdef FUTURE_SNMP_WANTED

    if (i1_code_for_agent == TRUE)
    {
        fprintf (t5, "  %s_FMAS_MIBObjectTable[] = {\n", Name);
    }
    else
    {
        fprintf (t5, "  %s_FMASSA_MIBObjectTable[] = {\n", Name);
    }

#else

    if (i1_code_for_agent == TRUE)
    {
        fprintf (t5, "  FMAS_MIBObjectTable[] = {\n");
    }
    else
    {
        fprintf (t5, " FMASSA_MIBObjectTable[] = {\n");
    }

#endif

    fprintf (mbdb, "\n/*  Declaration of Base OID Table. */");

    /*
     * description of the Base OID table
     * added by soma on 07/05/98
     */
    fprintf (mbdb,
             "\n/* Each entry contains Length of Base OID, Pointer to the Base OID,");
    fprintf (mbdb,
             "\n   Middle level get function pointer, Middle level test function pointer,");
    fprintf (mbdb,
             "\n   Middle level set function pointer and Number of objects in that table");
    fprintf (mbdb, "\n   in that order */\n");

    /* condition inserted here to support the -s option, which indicates
     * that middle level code is going to be used by Subagent
     * added by soma on 12/03/98
     */

#ifdef FUTURE_SNMP_WANTED

    if (i1_code_for_agent == TRUE)
    {
        fprintf (mbdb,
                 "\n tSNMP_BaseOIDType  %s_FMAS_BaseOIDTable[] = {\n", Name);
    }
    else
    {
        fprintf (mbdb,
                 "\n tSNMP_BaseOIDType  %s_FMASSA_BaseOIDTable[] = {\n", Name);
    }

#else

    if (i1_code_for_agent == TRUE)
    {
        fprintf (mbdb, "\n tSNMP_BaseOIDType  FMAS_BaseOIDTable[] = {\n");
    }
    else
    {
        fprintf (mbdb, "\n tSNMP_BaseOIDType  FMASSA_BaseOIDTable[] = {\n");
    }

#endif

    /*  Main FOR Loop for each Group.  */
    for (i4_counter = ONE; i4_counter <= i4_grp; i4_counter++)
    {

        /*  Main FOR Loop for each Object Identifier or Table.  */
        for (i4_count = ZERO; i4_count < i4_sizeof_array; i4_count++)
        {
            /*
             * Checking whether the Object Id is in the 
             * Group (The Group given by the i4_counter).
             */
            if (i4_indexarray[i4_count] != i4_counter)
            {
                continue;
            }

            /* 
             * if object has been written, print comma after that
             * code added by soma on 06/03/98
             */
            if (i4_tables_written > ZERO)
            {
                fprintf (mbdb, ",\n");
            }

            fprintf (mbdb, "{\n");

            /*  This Fn Finds the Suboid Length for a Table.  */
            i4_len_suboid =
                find_suboid_len (i4_indexarray, i4_count, i2_goid_len);

            fprintf (mbdb, "%d,\n", i4_len_suboid);

            /*  This Fn Finds the Name of the Table.  */
            pu1_string =
                get_tablename_from_tempfile (i4_count + ONE, pu1_pathname);
            pu1_string = strstr (pu1_string, SPACE_STR);
            /*  Moving the Spaces Before the Table Name.  */
            pu1_string += TWO;
            /*pu1_string = strtok(pu1_string , SQR_OPEN_BRACKET); */
            pu1_string = strtok (pu1_string, SPACE_STR);

            fprintf (mbdb, "%s,\n", pu1_string);
            /*  
             *  Reading the Read Only Status and No of Objects &
             *  the Entry Name of the Table from Temp File 4.
             */

            /* 
             * skip the names of unwanted tables since intially they have
             * been eritten to temp4 file in lexicographical order
             * file pointer is to be reset to the beginning of file each
             * time we enter the loop, otherwise we can't get the names
             * of tables we skipped in the previous parse(s)
             * code added by soma on 11/03/98
             */
            rewind (t4);
            for (i4_index = 0; i4_index < i4_count; i4_index++)
            {
                pu1_temp = get_line (t4);
                free (pu1_temp);
            }

            pu1_temp = get_line (t4);

            /*  Finding the Number of Objects in The Table.  */
            pu1_temp1 = strtok (pu1_temp, COMMA);
            i4_nof_objects = atoi (pu1_temp1);

            /*  Getting the Read Only Flag.  */
            pu1_temp1 = strtok (NULL, COMMA);
            i4_readonly_flag = atoi (pu1_temp1);

            /*  Getting the Table Name From the Temp File.  */
            pu1_temp1 = strtok (NULL, NEWLINE_STRING);
            /*  This Fn Passes the Fn Ptrs to the Base Oid Table.  */
            fnptr_for_basoidtable (mbdb, pu1_temp1, i4_readonly_flag);

            /*  
             *  Writing the Nof Objects to mbdb.h file
             *  (got from the temp File).
             */
            fprintf (mbdb, "%d\n", i4_nof_objects);
            fprintf (mbdb, "}");

            /* 
             * one table has been written, incriment i4_tables_written
             * code added by soma on 06/03/98
             */
            i4_tables_written++;

            /*
             * earlier objects were written to FMAS_MIBObjectTable in the order
             * in which they appear in MIB, although the order of the tables
             * changed in the process of grouping. So the objects were
             * incorrectly accessed as actual objects remained in the same order
             * even though tables order changed. To solve this problem the groups
             * of objects have to be shifted to the correct position according to
             * the table's position. To solve this problem all the objects are
             * arranged to be in the correct order in this function and
             * write_mibobj_table() has been deleted
             * added by soma on 12/03/98
             */
            rewind (t3);
            /* extract au4 */
            pu1_table_name = strtok (pu1_string, "_");
            /* extract SNMP */
            pu1_table_name = strtok (NULL, "_");
            /* extract OGP */
            pu1_table_name = strtok (NULL, "_");
            /* extract table name */
            pu1_table_name = strtok (NULL, "_");
            while (fgets (pu1_temp, MAX_LINE, t3))
            {
                /*
                 * Earlier a simple check was done to see whether the present
                 * line contains the table name. In that case it matched to the
                 * wrong input line and written incorrect fields to mib object
                 * table i.e. function pointers in place of access type and object
                 * name. Now the check is keen to match the exact table name.
                 * changed by soma on 24/03/98
                 */
                if (strstr (pu1_temp, "SNMP_OGP_INDEX_"))
                {
                    /* copy table name present after SNMP_OGP_INDEX_ */
                    strcpy (u1_temp, pu1_temp + 16);
                    pu1_temp1 = strstr (u1_temp, ",");
                    *pu1_temp1 = '\0';
                    if (strcmp (u1_temp, pu1_table_name) == 0)
                    {
                        break;
                    }
                }
            }
            for (i4_index = 0; i4_index < i4_nof_objects; i4_index++)
            {
                if (i4_objects_written > ZERO)
                {
                    fprintf (t5, ",\n");
                }
                fprintf (t5, "{\n");
                pu1_temp1 = strtok (pu1_temp, ",");
                fprintf (t5, "%s,\n", pu1_temp1);
                fgets (pu1_temp, MAX_LINE, t3);
                fgets (pu1_temp, MAX_LINE, t3);
                pu1_temp1 = strtok (pu1_temp, ",");
                fprintf (t5, "%s,\n", pu1_temp1);
                fgets (pu1_temp, MAX_LINE, t3);
                pu1_temp1 = strtok (pu1_temp, ",");
                fprintf (t5, "%s\n", pu1_temp1);
                fprintf (t5, "}");
                i4_objects_written++;
                /* skip unwanted lines in temp3 file */
                for (i4_temp_index = 0; i4_temp_index < 7; i4_temp_index++)
                {
                    if (!fgets (pu1_temp, MAX_LINE, t3))
                    {
                        break;
                    }
                }
            }
        }                        /* End Of FOR Loop. */

    }                            /* End of FOR loop for Each Group. */

    fprintf (mbdb, "\n};");
    fprintf (t5, "\n};");
    fclose (t3);
    fclose (t4);
    fclose (t5);
    fclose (mbdb);

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : write_mibobj_table                              *
*      Role of the function : This fn writes to the mbdb.h file (for v2)      *
*                             the Mib Object Table From Temp File 5.          *
*      Formal Parameters    : pu1_filename, i1_code_for_agent                 *
*      Global Variables     : i4_grp                                          *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/
void
write_mibobj_table (UINT1 *pu1_pathname, UINT1 *pu1_filename,
                    INT1 i1_code_for_agent)
{
    /* 
     * earlier object details were taken from temp3 file but to they
     * were not in proper order. In write_baseoid_table() object details
     * were written to temp5 file in correct order. It's enough now to
     * append that file to mbdb.h file and write the remaining things
     * at the end of mbdb.h file
     * code modified by soma on 12/03/98
     */
    UINT1               u1_temp[MAX_LINE];
    FILE               *mbdb;
    FILE               *t5;

    /* Opening the mdbd file in Append Mode.  */
    strcpy (u1_temp, pu1_pathname);
    strcat (u1_temp, pu1_filename);
    strcat (u1_temp, MBDB_EXTN_H);

    mbdb = fopen (u1_temp, APPEND_MODE);
    if (mbdb == NULL)
    {
        printf ("\n\tERROR: Unable to Open %s_mbdb.h file \n", pu1_filename);
        clear_directory ();
        exit (NEGATIVE);
    }

    /* Opening the Temp file 5 in Read Mode.  */
    strcpy (u1_temp, pu1_pathname);
    strcat (u1_temp, "temp5");

    t5 = fopen (u1_temp, READ_MODE);
    if (t5 == NULL)
    {
        printf
            ("\n\n\tError : Cannot Open Temporary File  for generating the MIB data base \n");
        /*This part is commented, can be used for testing */
        /*
         * printf("\n\tERROR: Unable to Open Temp File 5 (for Ver 2)\n");
         */
        clear_directory ();
        exit (NEGATIVE);
    }

    /* append the contents of temp5 file to mbdb.h */
    while (!feof (t5))
    {
        fgets (u1_temp, MAX_LINE, t5);
        fputs (u1_temp, mbdb);
    }
    fputs ("\n", mbdb);

    /*
     * code is changed to write the number of groups in generic form
     * instead of a magic number
     * changed by soma on 06/03/98
     */
    /* condition inserted here to support the -s option, which indicates
     * that middle level code is going to be used by Subagent
     * added by soma on 12/03/98
     */

#ifdef FUTURE_SNMP_WANTED

    if (i1_code_for_agent == TRUE)
    {
        fprintf (mbdb, "\n tSNMP_GLOBAL_STRUCT %s_FMAS_Global_data =\n{", Name);
        fprintf (mbdb,
                 "sizeof (%s_FMAS_GroupOIDTable) / sizeof (tSNMP_GroupOIDType),\n",
                 Name);
        fprintf (mbdb,
                 " sizeof (%s_FMAS_BaseOIDTable) / sizeof (tSNMP_BaseOIDType)};",
                 Name);
    }
    else
    {
        fprintf (mbdb, "\n tSNMP_GLOBAL_STRUCT %s_FMASSA_Global_data =\n{",
                 Name);
        fprintf (mbdb,
                 "sizeof (%s_FMASSA_GroupOIDTable) / sizeof (tSNMP_GroupOIDType),\n",
                 Name);
        fprintf (mbdb,
                 "sizeof (%s_FMASSA_BaseOIDTable) / sizeof (tSNMP_BaseOIDType)};",
                 Name);
    }

#else

    if (i1_code_for_agent == TRUE)
    {
        fprintf (mbdb, "\n tSNMP_GLOBAL_STRUCT FMAS_Global_data =\n{");
        fprintf (mbdb,
                 "sizeof (FMAS_GroupOIDTable) / sizeof (tSNMP_GroupOIDType),\n");
        fprintf (mbdb,
                 " sizeof (FMAS_BaseOIDTable) / sizeof (tSNMP_BaseOIDType)};");
    }
    else
    {
        fprintf (mbdb, "\n tSNMP_GLOBAL_STRUCT FMASSA_Global_data =\n{");
        fprintf (mbdb,
                 "sizeof (FMASSA_GroupOIDTable) / sizeof (tSNMP_GroupOIDType),\n");
        fprintf (mbdb,
                 "sizeof (FMASSA_BaseOIDTable) / sizeof (tSNMP_BaseOIDType)};");
    }

#endif

#ifdef FUTURE_SNMP_WANTED

    if (i1_code_for_agent == TRUE)
        fprintf (mbdb, "\n\n int %s_MAX_OBJECTS =", Name);
    else
        fprintf (mbdb, "\n\n int %s_MAX_OBJECTSSA =", Name);

#else

    if (i1_code_for_agent == TRUE)
        fprintf (mbdb, "\n\n int MAX_OBJECTS =");
    else
        fprintf (mbdb, "\n\n int MAX_OBJECTSSA =");

#endif

    /* condition inserted here to support the -s option, which indicates
     * that middle level code is going to be used by Subagent
     * added by soma on 12/03/98
     */

#ifdef FUTURE_SNMP_WANTED

    if (i1_code_for_agent == TRUE)
    {
        fprintf (mbdb,
                 "\n(sizeof (%s_FMAS_MIBObjectTable) / sizeof (tSNMP_MIBObjectDescrType));",
                 Name);
    }
    else
    {
        fprintf (mbdb,
                 "\n(sizeof (%s_FMASSA_MIBObjectTable) / sizeof (tSNMP_MIBObjectDescrType));",
                 Name);
    }

#else

    if (i1_code_for_agent == TRUE)
    {
        fprintf (mbdb,
                 "\n(sizeof (FMAS_MIBObjectTable) / sizeof (tSNMP_MIBObjectDescrType));");
    }
    else
    {
        fprintf (mbdb,
                 "\n(sizeof (FMASSA_MIBObjectTable) / sizeof (tSNMP_MIBObjectDescrType));");
    }

#endif

#ifdef FUTURE_SNMP_WANTED
    fprintf (mbdb, "\n\n#endif  /* _%sMDB_H */\n", UName);
#else
    fprintf (mbdb, "\n\n#endif  /* _SNMP_MBDB_H */\n");
#endif

    fclose (mbdb);
    fclose (t5);

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : create_mbdb_file_ver2                           *
*      Role of the function : This fn writes to the mbdb.h file (for v2)      *
*      Formal Parameters    : pu1_pathname , pu1_filename                     *
*                             i1_code_for_agent, i2_goid_len                  *
*      Global Variables     : i4_grp , i4_sizeof_array , pu1_oidarray.        *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/
void
create_mbdbh_file_ver2 (UINT1 *pu1_pathname, UINT1 *pu1_filename,
                        INT1 i1_code_for_agent, INT2 i2_goid_len,
                        INT4 i4_argv_count, INT1 u1_parse_files[][MAX_LINE])
{
    /*  
     *  This is the Array which stores the status of the 
     *  Oids (i.e to which this Oid is grouped to) and
     *  all updations are done to this array when the
     *  groups are formed.
     */
    INT4                i4_indexarray[MAX_NO_OF_GROUPS] = { ZERO };
    INT4                i4_oidlen;
    INT4                i4_next_index;
    INT4                i4_common_oidlen;
    INT4                i4_index;
    /*
     * these variables are added to sort out any SUBSET problem
     */
    UINT1               u1_subset_exist;
    INT4                i4_group1;
    INT4                i4_group2;

    /*
     * this variable is used to support -g option
     * this stores the length of, num of sub-identifiers, shortest OID
     */
    INT4                i4_shortest_oid_len;

    /*  FORMING THE GROUPS.  */
    /* Getting the Oid from the Temp File into the pu1_oidarray. */
    i4_sizeof_array = get_oid_fromtempfile (pu1_pathname);

    /* Finding the Common oid of all the Oid's present.  */
    i4_common_oidlen = find_commonoid ();

    /*
     * this peice of code checks whethe the length of group OID specified in
     * command line is greater than shortest OID in the MIB
     * added by soma on 14/04/98
     */
    if (i2_goid_len)
    {
        i4_shortest_oid_len = shortest_oid ();
        if (i2_goid_len > i4_shortest_oid_len)
        {
            printf ("\nERROR: Group OID length is greater than shortest OID\n");
            printf ("Tip: It can take a max value of %d\n",
                    i4_shortest_oid_len);
            clear_directory ();
            exit (NEGATIVE);
        }
        if (i2_goid_len < 6)
        {
            printf
                ("\nWarning: Group OID length is too short! Continuing...\n");
        }
    }

    /*  
     *  The Groups are to be Formed Only when
     *  there are more than One Oid present.
     */
    if (i4_sizeof_array > ONE)
    {
        if (i2_goid_len)
        {
            form_groups_in_one_shot (i2_goid_len, i4_indexarray);
        }
        else
        {
            /*  Main DO WHILE Loop for Forming the Groups.  */
            do
            {
                /*  Assigning the Common oid of all the Oid's present.  */
                i4_oidlen = i4_common_oidlen;

                /* This Fn Forms the Grps */
                i4_oidlen = form_groups (i4_grp, i4_oidlen, i4_indexarray);
                /*
                 *  Finding the Next Non Grouped Item. This Fn
                 *  returns -1 if there is no Oid to be Grouped.
                 */
                i4_next_index = find_next_nongrouped_index (i4_indexarray);
                /* Increment the Grp Count by 1 after a grp has been Processed. */
                i4_grp++;
            }
            while (i4_next_index != NEGATIVE);
        }
    }
    else if (i4_sizeof_array == ONE)
    {
        /* There is Only One OID */
        i4_indexarray[ZERO] = ONE;
    }

    /* 
     * this condition was inserted after giving the control to user to
     * specify the length of Group OID to be kept in Group OID table
     * added by soma on 14/04/98
     */

    if (i2_goid_len == 0)
    {
        /*
         * code added to check whether subsets exist in the formed groups
         * if found those two groups are merged and formed in to a single group
         * this process continues till there are no subsets found
         * added by soma on 06/03/98
         */

        /* make all group numbers positive for easy handling */
        for (i4_index = 0; i4_index < i4_sizeof_array; i4_index++)
        {
            if (i4_indexarray[i4_index] < ZERO)
            {
                i4_indexarray[i4_index] = i4_indexarray[i4_index] * NEGATIVE;
            }
        }
        /* sort out subsets */
        u1_subset_exist =
            check_for_subset (&i4_group1, &i4_group2, i4_indexarray);
        while (u1_subset_exist == TRUE)
        {
            merge_groups (i4_group1, i4_group2, i4_indexarray);
            u1_subset_exist =
                check_for_subset (&i4_group1, &i4_group2, i4_indexarray);
        }
    }

    /*  GENERATION OF THE MBDB STARTS HERE.  */

    /*  
     *  This Fn writes to the file the OID Arrays 
     *  which have been Grouped.
     */
    /*
     *Extra parameter i1_code_for_agent is passed to generate differnet name 
     *when -s option is used
     */

    write_oidarray (i4_indexarray, pu1_pathname, pu1_filename,
                    i2_goid_len, i4_argv_count, u1_parse_files,
                    i1_code_for_agent);

    /*  This Fn writes to the file the arrays for each subgroup.  */
    write_suboidarray (i4_indexarray, pu1_pathname, pu1_filename, i2_goid_len);

    /*  This Fn writes the Group OID Table.  */
    write_grpoid_table (i4_indexarray, pu1_pathname, pu1_filename,
                        i1_code_for_agent, i2_goid_len);

    /*  This Fn writes the Base OID Table.  */
    write_baseoid_table (i4_indexarray, pu1_pathname, pu1_filename,
                         i1_code_for_agent, i2_goid_len);

    /*  This Fn writes the MIB Object Table from temp file 5 */
    write_mibobj_table (pu1_pathname, pu1_filename, i1_code_for_agent);

}                                /* End Of Main Function. */

/*
 * following functions are added to help solve the problem of
 * subsets in forming groups
 * added by soma on 06/03/98
 */

/******************************************************************************
*      Function Name        : check_for_subset                                *
*      Role of the function : Checks for the existence of subset in groups    *
*      Formal Parameters    : pi4_group1, pi4_group2, i4_indexarray           *
*      Global Variables     : i4_grp                                          *
*      Use of Recursion     : None                                            *
*      Return Value         : TRUE, if SUBSET exists                          *
*                             FALSE, otherwise                                *
******************************************************************************/

UINT1
check_for_subset (INT4 *pi4_group1, INT4 *pi4_group2, INT4 i4_indexarray[])
{
    UINT1              *pu1_group_oid1 = NULL;
    UINT1              *pu1_group_oid2 = NULL;
    INT4                i4_group_index1, i4_group_index2;
    INT4                i4_group_oid_len1, i4_group_oid_len2;
    INT4                i4_min_len;

    for (i4_group_index1 = 1; i4_group_index1 < i4_grp; i4_group_index1++)
    {
        *pi4_group1 = i4_group_index1;
        pu1_group_oid1 = get_group_oid (i4_group_index1, i4_indexarray);
        i4_group_oid_len1 = strlen (pu1_group_oid1);
        for (i4_group_index2 = i4_group_index1 + 1; i4_group_index2 < i4_grp;
             i4_group_index2++)
        {
            *pi4_group2 = i4_group_index2;
            pu1_group_oid2 = get_group_oid (i4_group_index2, i4_indexarray);
            i4_min_len = i4_group_oid_len2 = strlen (pu1_group_oid2);
            if (i4_group_oid_len1 < i4_group_oid_len2)
            {
                i4_min_len = i4_group_oid_len1;
            }
            /*
             * last two conditions ensure that X.1 and X.10 are not treated
             * as forming subset
             * added by soma on 03/04/98
             */
            if (!strncmp (pu1_group_oid1, pu1_group_oid2, i4_min_len))
            {
                /*
                 * this is to ensure that X.1 and X.10 are not treated
                 * as forming subset
                 * added by soma on 03/04/98
                 */
                if ((i4_group_oid_len1 < i4_group_oid_len2) &&
                    ((pu1_group_oid2[i4_min_len] == NULL_CHAR) ||
                     (pu1_group_oid2[i4_min_len] == COMMA_CHAR)))
                {
                    return TRUE;
                }
                if ((i4_group_oid_len1 > i4_group_oid_len2) &&
                    ((pu1_group_oid1[i4_min_len] == NULL_CHAR) ||
                     (pu1_group_oid1[i4_min_len] == COMMA_CHAR)))
                {
                    return TRUE;
                }
            }
        }
    }
    return FALSE;
}

/******************************************************************************
*      Function Name        : merge_groups                                    *
*      Role of the function : Combines the groups passed                      *
*      Formal Parameters    : i4_grp1, i4_grp2, i4_indexarray                 *
*      Global Variables     : i4_grp , i4_sizeof_array , pu1_oidarray         *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/

void
merge_groups (INT4 i4_grp1, INT4 i4_grp2, INT4 i4_indexarray[])
{
    INT4                i4_index;

    for (i4_index = 0; i4_index < i4_sizeof_array; i4_index++)
    {
        /* combine i4_grp1 and i4_grp2 by changing values of the i4_indexarray */
        if (i4_indexarray[i4_index] == i4_grp2)
        {
            i4_indexarray[i4_index] = i4_grp1;
        }
        /* adjust group numbers as two groups are merged */
        if (i4_indexarray[i4_index] > i4_grp2)
        {
            --i4_indexarray[i4_index];
        }
    }
    /* now total number of groups is one less than previous */
    --i4_grp;
    return;
}

/******************************************************************************
*      Function Name        : get_group_oid                                   *
*      Role of the function : extracts the group oid of the group passed      *
*      Formal Parameters    : i4_group_no, i4_indexarray                      *
*      Global Variables     : i4_grp , i4_sizeof_array , pu1_oidarray         *
*      Use of Recursion     : None                                            *
*      Return Value         : Pointer to the group oid                        *
******************************************************************************/

UINT1              *
get_group_oid (INT4 i4_group_no, INT4 i4_indexarray[])
{
    UINT1              *pu1_group_oid;
    INT4                i4_index;
    INT4                i4_group_oid_len;

    pu1_group_oid = (UINT1 *) calloc (1, (MAX_LINE * sizeof (UINT1)));
    if (pu1_group_oid == NULL)
    {
        printf ("\n\n\tError : Malloc Failure.\n");
        clear_directory ();
        exit (NEGATIVE);
    }
    i4_group_oid_len = find_lenof_groupoid (i4_indexarray, i4_group_no);
    for (i4_index = 0; i4_index < i4_sizeof_array; i4_index++)
    {
        if (i4_indexarray[i4_index] == i4_group_no)
        {
            strncpy (pu1_group_oid, pu1_oidarray[i4_index], i4_group_oid_len);
            pu1_group_oid[i4_group_oid_len] = '\0';
            break;
        }
    }
    return pu1_group_oid;
}

/*
 * These functions are added to support the new option -g, which permits
 * user to specify the length of group OID to be kept in group OID table
 * in mbdb.h file.
 * added by soma on 14/04/98
 */

/******************************************************************************
*      Function Name        : shortest_oid                                    *
*      Role of the function : finds the length of the shortest OID in the MIB *
*      Formal Parameters    : None                                            *
*      Global Variables     : pu1_oidarray, i4_sizeof_array                   *
*      Use of Recursion     : None                                            *
*      Return Value         : Length of shortest OID in the MIB               *
******************************************************************************/

INT4
shortest_oid (void)
{
    INT4                i4_oid_len, i4_index;
    INT4                i4_soid_len = MAX_LINE;

    for (i4_index = 0; i4_index < i4_sizeof_array; i4_index++)
    {
        i4_oid_len = find_nof_comma (pu1_oidarray[i4_index]);
        if (i4_oid_len < i4_soid_len)
        {
            i4_soid_len = i4_oid_len;
        }
    }
    /* incremented since it did not consider the last sub-identifier */
    return (++i4_soid_len);
}

/******************************************************************************
*      Function Name        : form_groups_in_one_shot                         *
*      Role of the function : form groups based on the length of group OID    *
*      Formal Parameters    : i2_goid_len, i4_indexarray                      *
*      Global Variables     : pu1_oidarray, i4_sizeof_array                   *
*      Use of Recursion     : None                                            *
*      Return Value         : Length of shortest OID in the MIB               *
******************************************************************************/

void
form_groups_in_one_shot (INT2 i2_goid_len, INT4 i4_indexarray[])
{
    UINT1              *pu1_comma_ptr;
    INT4                i4_group_oid_len;
    INT4                i4_index;
    INT4                i4_next_index = 0;
    INT4                i4_commas;

    fill_with_groupno (ZERO, i4_indexarray);
    do
    {
        for (i4_commas = i2_goid_len, pu1_comma_ptr =
             pu1_oidarray[i4_next_index]; i4_commas > 0; i4_commas--)
        {
            pu1_comma_ptr = strchr (pu1_comma_ptr, COMMA_CHAR);
            if (pu1_comma_ptr != NULL)
            {
                pu1_comma_ptr++;
            }
        }
        if (pu1_comma_ptr == NULL)
        {
            i4_group_oid_len = strlen (pu1_oidarray[i4_next_index]);
        }
        else
        {
            i4_group_oid_len = pu1_comma_ptr - pu1_oidarray[i4_next_index] - 1;
        }
        for (i4_index = 0; i4_index < i4_sizeof_array; i4_index++)
        {
            if (i4_indexarray[i4_index] == ZERO)
            {
                if (strncmp
                    (pu1_oidarray[i4_next_index], pu1_oidarray[i4_index],
                     i4_group_oid_len) == 0)
                {
                    if ((pu1_oidarray[i4_index][i4_group_oid_len] == NULL_CHAR)
                        || (pu1_oidarray[i4_index][i4_group_oid_len] ==
                            COMMA_CHAR))
                    {
                        i4_indexarray[i4_index] = i4_grp;
                    }
                }
            }
        }
        i4_grp++;
        i4_next_index = find_next_nongrouped_index (i4_indexarray);
    }
    while (i4_next_index != NEGATIVE);
}

/******************************************************************************
*      Function Name        : get_group_oid_len                               *
*      Role of the function : finds the length of the group OID               *
*      Formal Parameters    : i2_goid_len, i4_groupno, i4_indexarray          *
*      Global Variables     : pu1_oidarray, i4_sizeof_array                   *
*      Use of Recursion     : None                                            *
*      Return Value         : Length of the group OID                         *
******************************************************************************/

INT4
get_group_oid_len (INT2 i2_goid_len, INT4 i4_groupno, INT4 i4_indexarray[])
{
    UINT1              *pu1_comma_ptr;
    INT4                i4_index, i4_commas;
    INT4                i4_oidlen;

    for (i4_index = 0; i4_indexarray[i4_index] != i4_groupno; i4_index++);
    for (i4_commas = i2_goid_len,
         pu1_comma_ptr = pu1_oidarray[i4_index]; i4_commas > 0; i4_commas--)
    {
        pu1_comma_ptr = strchr (pu1_comma_ptr, COMMA_CHAR);
        if (pu1_comma_ptr != NULL)
        {
            pu1_comma_ptr++;
        }
    }
    if (pu1_comma_ptr == NULL)
    {
        i4_oidlen = strlen (pu1_oidarray[i4_index]);
    }
    else
    {
        i4_oidlen = pu1_comma_ptr - pu1_oidarray[i4_index] - 1;
    }
    return i4_oidlen;
}
