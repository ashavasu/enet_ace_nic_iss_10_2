%{
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: premosyy.y,v 1.5 2015/04/28 14:43:31 siva Exp $
 *
 * Description: This file contains specification for parser. 
 *              
 *******************************************************************/
 
#include "premosy.h"
#include "defines.h" 
#include "type.h"    

INT4 i4_range;                     /* for range reprsenataion */
INT4 i4_lowVal;                    /* For tracking Lower Range */
INT1 u1_incl_path[MAX_LINE];       /* for include path reprasentrtion */
INT1 i1_is_oid = FALSE;            /* indicates whether it's a OID assignment */
INT1 i1_oid_wanted = TRUE;         /* some OIDs need not be printed to output file */
INT1 i1_is_row_name = FALSE;       /* indicates whether it's row of table */
INT1 i1_rowdef_exists = FALSE;     /* indicates whether row definition exists */
INT1 i1_is_tc = TRUE;              /* indicatess whether it's textual convention parsed */
INT1 i1_is_defval = FALSE;         /* whether it's a default value being parsed */
INT1 i1_start_check = FALSE;       /* whether objects should be checked for order */
INT1 i1_enum_type = FALSE;         /* indicates whether it's enumerated syntax type */
INT1 i1_access[MAX_DESC_LEN];      /* stores the value of ACCESS clause */
INT1 i1_status[MAX_DESC_LEN];      /* stores the value of STATUS clause */
INT1 i1_syntax[MAX_DESC_LEN];      /* stores the value of SYNTAX clause */
INT1 i1_object_desc[MAX_DESC_LEN]; /* keeps the name of object descriptor */
INT1 i1_object_identifier[MAX_OID_LEN]; /* Complete OID in dotted form */
INT1 i1_row_name[MAX_DESC_LEN];    /* stores the name of row of a table */
INT1 i1_out_file[MAX_FILE_NAME_LEN]; /* holds name of the output file */
INT1 i1_in_file[MAX_FILE_NAME_LEN]; /* holds name of the input file */
INT1 i1_temp_file[MAX_FILE_NAME_LEN]; /* Details of all objects are written to*/
INT1 i1_module_name[MAX_LINE_LEN];  /*Holds the name of the module identity*/ 
tNAME_LIST *p_oid_list; /* Stores OID assignments, table names and rownames */
tNAME_LIST *p_object_list;         /* Stores all OBJECT-TYPE's */
tNAME_LIST *p_import_list;         /* Stores all Imports */
tNAME_LIST *p_table_entries;       /* store all columnar objects to check for order */
tNAME_LIST *p_this_entry;/*points to object name that should be read now */
tNAME_LIST *p_indices;             /* stores indices of the table */ 
tNAME_VALUE *p_name_values;  /* stores size or named-values of syntax */
tOID *p_object_ids;  /* holds name and OID of all object identifiers */
tTC_TABLE *p_type_defs; /* Store name, base type and size/named-values*/
tTC_TABLE *p_this_tc;           /* points to the node beibg filled up */


INT1 *scalar_table_name[255];       /* For registering SCALAR/TABLE names */
INT4 i4_no_of_table_scalar = 0;     /* For holding No of SCALAR/TABLE parsed */
INT4 i4_check_repeat_table = FALSE; /* For checking the reoccurance of table */

tOBJECT_LS *p_object_ls;
tINDICES_LIST *p_indices_ls;


INT2 i2_table_expecting = 0 ;
INT2 i2_table_flag = 0 ;
INT1 i1_table_status_set_flag = 0;
INT4 i4_no_of_table_entries;
INT1 i1_mibfile_name[MAX_LINE]; /* hols the name file that is processing */
FILE *p_out_file;                  /* file pointer for output file */
FILE *p_temp_file;                 /* file pointer for temp file */
extern INT4 yylineno;              /* counts number of lines in input */
extern INT1 *yytext;           /* where Lexer keeps the matched input */
extern FILE *yyin;                /* where input for Lexer comes from */

extern t_MIB_TABLE *p_table_struct;
extern tIMPINDICES_LIST *pImpIndx;
extern t_MIB_TABLE *p_scalar_struct;
extern INT4 i4_global_scalar_flag;
extern INT1 i1_inc_path[];
extern INT1 i4_global_mac_flag;
INT4 i4_check_for_notify_object = 0;
%}
%union {
    INT1 str[MAX_DESC_LEN];
	 INT4 intval;
};

%token <str> MODULE_IDENTITY OBJECT_IDENTITY TEXTUAL_CONVENTION OBJECT_TYPE
%token <str> NOTIFICATION_TYPE MODULE_COMPLIANCE
%token <str> OBJECT_GROUP NOTIFICATION_GROUP AGENT_CAPABILITIES
%token DEFINITIONS BEGIN_DEF CCE IMPORTS FROM END REVISION UNITS
%token LAST_UPDATED ORGANIZATION CONTACT_INFO
%token OBJECT IDENTIFIER SYNTAX DESCRIPTION REFERENCE _INDEX IMPLIED DEFVAL
%token MAX_ACCESS _READ_ONLY _READ_WRITE _READ_CREATE NOT_ACCESSIBLE
%token MIN_ACCESS ACCESS ACCESSIBLE_FOR_NOTIFY NOT_IMPLIMENTED WRITE_ONLY
%token STATUS CURRENT DEPRECATED OBSOLETE DISPLAY_HINT
%token SEQUENCE OF OBJECTS SIZE OCTET _STRING MODULE
%token INCLUDES SUPPORTS VARIATION WRITE_SYNTAX CREATION_REQUIRES AUGMENTS 
%token MANDATORY_GROUPS GROUP PRODUCT_RELEASE NOTIFICATIONS MANDATORY_GROUPS
%token <str> NAME HNAME QSTRING BINSTRING HEXSTRING
%token <intval> NUM _ZERO
%type <str> Modules Module MibName ObjectDescriptor SyntaxType Objects
%type <str> ModuleName NameHName Notifications NotificationName
%type <str> ObjectName Imports TypesSet Types Type Cells Cell Groups GroupName
%type <intval> DefaultValue DefValue Value Ranges Range AnyNum Num

%%
begin      : MibName DEFINITIONS CCE BEGIN_DEF 
             body END                         {
					       t_MIB_TABLE *temp = p_scalar_struct;
					       while(temp)
					       {
					       temp = temp->next;
					       }
					       }
           ;	 

body       : IMPORTS Imports ';'
             ObjectDescriptor MODULE_IDENTITY {
                 char ch;
                 strcpy(i1_module_name,$4);
                 ch = $4[0];
                 if (ch >= 'a' && ch <= 'z')
                 {
                     ch = ch-32;
                 }    
                 $4[0] = ch;
                 strcpy(i1_module_name,$4);
             }  
             ModuleIdentityBody CCE ObjectId         
             Modules
           ;

Imports    : TypesSet
           | Imports TypesSet
	   ;

TypesSet   : Types FROM MibName
           ;

Types      : Type
           | Types ',' Type
	   ;

ModuleIdentityBody: LAST_UPDATED QSTRING
                    ORGANIZATION QSTRING
                    CONTACT_INFO QSTRING
                    DESCRIPTION QSTRING
                    RevisionPart 
                  ;

RevisionPart: Revisions
            |
            ;

Revisions: Revision
         | Revisions Revision
         ;

Revision: REVISION QSTRING
          DESCRIPTION QSTRING
        ;

Modules: Module
       | Modules Module
		 ;

Module:  ObjectDescriptor OBJECT_IDENTITY              
         ObjectIdentityBody CCE ObjectId               
       | ObjectDescriptor OBJECT IDENTIFIER CCE        
	 ObjectId                                     
       | SyntaxType CCE TEXTUAL_CONVENTION TextualConventionBody   {
                 tTC_TABLE *p_temp_tc,*p_tc_temp ;
		 INT1 *p1_temp;
		       p_temp_tc = p_type_defs;
                        p_this_tc = ALLOC_TC_MEM; 
			if(p_this_tc == NULL ){
			yyerror("malloc error ");
			}
                     p_this_tc->tc_name_value_list = NULL;
	            strcpy (p_this_tc->tc_name, $1);
		   strcpy(p_this_tc->tc_base,i1_syntax);
                if( primitive_test(i1_syntax) == 0 )
		{
		p1_temp =   compute_primitive(i1_syntax);
		strcpy(p_this_tc->tc_base,p1_temp);
		}
		   
               p_this_tc->next = NULL;
                  if (p_type_defs == NULL) {
	                p_type_defs = p_this_tc;
		           }
	              else {
                    while (p_temp_tc->next != NULL) {
                     p_temp_tc = p_temp_tc->next;
	                   }
                 p_temp_tc->next = p_this_tc;
            }
	   }
				       
       | ObjectDescriptor OBJECT_TYPE    { strcpy(i1_object_desc,$1);}     
         ObjectTypeBody CCE ObjectId     {
                                          INT4 i4_position;
					  INT1 *p1_position;
					  store_object_info();
                                        if(p_table_struct)
					{
                                        if(strcmp(i1_syntax,p_table_struct->entry_name))
					{
                                             if(i2_table_flag)
					     {
					     if( check_object_in_table_list($1))
					     {
					     strcpy(p_table_struct->object[p_table_struct->no_of_objects].object_name,$1);
					     p1_position = strrchr(i1_object_identifier,'.');
					     if(p1_position == NULL )
					     {
					     fprintf(stderr,"position not claer in identifier %s \n",i1_object_identifier);
					     yyerror("");
					     }
					     i4_position = atoi(p1_position+1);
					     p_table_struct->object[p_table_struct->no_of_objects].position = i4_position;  
					     write_syntax(p_table_struct);
					     write_access(p_table_struct);
					     write_status(p_table_struct);
					     p_table_struct->no_of_objects++;
					   /*  printf("in table object type entered %s  and count %d  \n",$1,p_table_struct->no_of_objects); */
					     if(p_table_struct->no_of_objects == i4_no_of_table_entries ) {
					     check_for_imports();
					     i2_table_flag = 0;
					     i4_no_of_table_entries = 0;
#ifdef DEBUG
					     printdata(p_table_struct); 
					     getchar();  
#endif
					     i4_global_scalar_flag = 0;
					     p_table_struct->entry_name[0] = tolower(p_table_struct->entry_name[0]);
					     p_table_struct->table_name[0] = tolower(p_table_struct->table_name[0]);
					      generator_module();
					         oid_table_v1(u1_incl_path,i1_mibfile_name);
					     free(p_table_struct);
                               	    free_names_memory (p_table_entries);
					p_table_entries = NULL; 
					     p_table_struct = NULL ;
					     }
					     
					     }
					     else
					     {
#if DEBUG					     
					     printf("WARNING : A Scalar object in between, definations of a table objects \n");
#endif					     
					     write_scalar();
					     }

					  } /* if i2_table_flag end */
					  }/* same syntax if loop end */
					  }/* if p_table_struct  end  */
					     else
					     {
					     /* scalar object take care */
					     write_scalar();


					     }
					     i4_range = 0;
					     }
      | SyntaxType CCE SEQUENCE          
       '{' ConceptualTableEntry '}'       

      | ObjectDescriptor NOTIFICATION_TYPE {i4_check_for_notify_object = 1;}
                                                 
	NotificationTypeBody CCE ObjectId  {i4_check_for_notify_object = 0; }
                                                 
      | ObjectDescriptor MODULE_COMPLIANCE      
                                                
	ModuleComplianceBody CCE ObjectId       
                                                
      | ObjectDescriptor OBJECT_GROUP           
                                                 
	ObjectGroupBody CCE ObjectId            
                                                
      | ObjectDescriptor NOTIFICATION_GROUP     
                                               
	NotificationGroupBody CCE ObjectId      
                                                
      | ObjectDescriptor AGENT_CAPABILITIES     
                                                
	AgentCapabilitiesBody CCE ObjectId      
                                                
      ;

ObjectIdentityBody: STATUS Status
                    DESCRIPTION QSTRING
                    ReferPart
                  ;

TextualConventionBody: DisplayPart
                       STATUS Status
                       DESCRIPTION QSTRING
                       ReferPart
                       SYNTAX Syntax
                     ;

DisplayPart: DISPLAY_HINT QSTRING
           |
           ;

ObjectTypeBody: SYNTAX Syntax
                UnitsPart
                MAX_ACCESS Access 
		STATUS Status {
            if (i1_table_status_set_flag)
            {
                 write_table_status ();
                 i1_table_status_set_flag = 0;
            }
        }
		DESCRIPTION QSTRING
		ReferPart
		IndexPart
		DefValPart
	      ;

UnitsPart: UNITS QSTRING
         |
         ;

NotificationTypeBody: ObjectsPart     {/* strcpy(i1_syntax , "INTEGER"); */}
                      STATUS Status   {/* strcpy(i1_access , "read-only"); */}
		      DESCRIPTION QSTRING
		      ReferPart
	 ;

ObjectsPart: OBJECTS '{' Objects '}'
           |
			  ;

Objects: ObjectName
       | Objects ',' ObjectName
		 ;

ModuleComplianceBody: STATUS Status
                      DESCRIPTION QSTRING
							 ReferPart
							 CModulePart
						  ;	

CModulePart: CModules
          |
			 ;

CModules: CModule
       | CModules CModule
		 ;

CModule: MODULE ModuleName 
         MandatoryPart
		   CompliancePart
		 ;

MandatoryPart: MANDATORY_GROUPS '{' Groups '}'
             | 
				 ;

Groups: GroupName
      | Groups ',' GroupName
		;

CompliancePart: Compliances
              |
				  ;

Compliances: Compliance
           | Compliances Compliance
			  ;

Compliance: ComplianceGroup
          | Object
			 ;

ComplianceGroup: GROUP ObjectDescriptor
                 DESCRIPTION QSTRING
					;

Object: OBJECT ObjectDescriptor
        SyntaxPart
		  WriteSyntaxPart
		  AccessPart
		  DESCRIPTION QSTRING
		;

SyntaxPart: SYNTAX Syntax
          |
			 ;

WriteSyntaxPart: WRITE_SYNTAX Syntax
               |
					;

AccessPart: MIN_ACCESS Access
          | 
			 ;

GroupName: ObjectDescriptor
         ;

ObjectGroupBody: ObjectsPart
                 STATUS Status
                 DESCRIPTION QSTRING
                 ReferPart
               ;

NotificationGroupBody: NotificationsPart
                       STATUS Status
                       DESCRIPTION QSTRING
                       ReferPart
                     ;

NotificationsPart: NOTIFICATIONS '{' Notifications '}'
                 ;

Notifications: NotificationName
             | Notifications ',' NotificationName
             ;

AgentCapabilitiesBody: PRODUCT_RELEASE QSTRING
                       STATUS Status
                       DESCRIPTION QSTRING
                       ReferPart
                       AModulePart
                     ;

AModulePart: AModules
           |
           ;

AModules: AModule
        | AModules AModule
        ;

AModule: SUPPORTS ModuleName
         INCLUDES '{' Groups '}'
         VariationPart
       ;

VariationPart: Variations
             |
				 ;

Variations: Variation
			 | Variations Variation
          ;

Variation: VARIATION ObjectName
        	  SyntaxPart
        	  WriteSyntaxPart
        	  AAccessPart
        	  CreationPart
        	  DefValPart
        	  DESCRIPTION QSTRING 
         ;

AAccessPart: ACCESS AAccess
			 |
          ;

AAccess: Access
       | NOT_IMPLIMENTED
       | WRITE_ONLY
       ;

CreationPart: CREATION_REQUIRES '{' Cells '}'
            |
				;

Cells: Cell
	  | Cells ',' Cell
     ;

Cell: ObjectName
    ;

ConceptualTableEntry: Entries
                    ;

Entries: Entry
       | Entries ','  Entry
		 ;

Entry: ObjectDescriptor           { 
                                    tNAME_LIST *temp;
                                    i4_no_of_table_entries++;
                                    if(p_table_entries == NULL ) {
				    p_table_entries = (tNAME_LIST *)calloc(1,sizeof(tNAME_LIST));
				       strcpy(p_table_entries->name,$1);
				       }
				       else { 
				             temp = p_table_entries;
					     while(temp->next != NULL )
					       temp = temp->next;
					       temp->next = (tNAME_LIST *)calloc(1,sizeof(tNAME_LIST));
					    strcpy(temp->next->name,$1); 
					    }

                                    /*   printf("%s %d \n",$1,i4_no_of_table_entries); */
				    }
        Syntax
     ;

DefValPart: DEFVAL 
             DefaultValue 
	 |
          ;

DefaultValue: DefValue
            | '{' DefValue '}' { $$=$2;}
            | '{' '{' DefValue ',' DefValue '}' '}' { $$ = $$; }
            | '{' '{' '}' '}' { $$ = $$; }
            |  '{' '{' DefValue '}' '}' { $$ = $$; }
            ;

DefValue: ObjectDescriptor {$$=$$;}
        | ObjectDescriptor NUM {$$=$2;}
        | Num Num     {$$=$2;}       
        | QSTRING     {$$=$$;}       
        | Value            
        | '{' Enums '}'
		  ;

Syntax  : SyntaxType  SubType {strcpy(i1_syntax,$1);}
	| NAME  '{' Enums '}' {
				   strcpy(i1_syntax,$1);
				     /* to convert BITS to INTEGER  ***/
            /* BITS are mapped to OCTET STRING */
				if(strcmp($1,"BITS") == 0 )
	                            /* strcpy(i1_syntax,"INTEGER"); */
	                            strcpy(i1_syntax,"OCTET STRING");
				   /****** end of change ******/
			      }
	| OCTET _STRING SubType{strcpy(i1_syntax,"OCTET STRING");}
	| OBJECT IDENTIFIER   {strcpy(i1_syntax,"OBJECT IDENTIFIER");}
	| SEQUENCE OF SyntaxType {

           
             p_table_struct = (t_MIB_TABLE *) calloc(1,sizeof(t_MIB_TABLE));
             while( p_scalar_struct != NULL)
             {
                 p_table_struct = p_scalar_struct;
                 reg_table_name();
                 i4_global_scalar_flag = p_scalar_struct->no_of_objects;
                 generator_module();
                 oid_table_v1(u1_incl_path,i1_mibfile_name);
                 p_scalar_struct = p_scalar_struct->next;
             }
             i4_check_repeat_table = FALSE;
             p_scalar_struct = NULL;
             free(p_scalar_struct);
             p_table_struct = NULL;
             free(p_table_struct);

                                 i2_table_flag= 1;       
                                 i1_table_status_set_flag = 1;
	                         strcpy(i1_syntax,$3);
	                         p_table_struct = (t_MIB_TABLE *) calloc(1,sizeof(t_MIB_TABLE));
				 if(p_table_struct == NULL )
				 {
				 printf("memory error \n");
				 exit(0);
				 }
	                         strcpy(p_table_struct->table_name,i1_object_desc);
	                         strcpy(p_table_struct->entry_name,$3);
		/*		 printf("entry name: %s \n",p_table_struct->entry_name);getchar(); */}
		;

SubType: '('           { i4_range = 0; i4_lowVal = 0; }
          Ranges ')'
		 | '('{i4_range = 0; i4_lowVal = 0; } SIZE 
		    '(' Ranges ')' ')'
		 |
		 ;

Ranges: Range
      | Ranges '|' Range { i4_range = $1;}    
		;

Range: Value { i4_lowVal = $1;} 
     | Value '.' '.' Value {i4_lowVal = $1;} 
	  ;

Value:    '-' NUM {$$=$2;}
          | Num       { if(i4_range < $1) i4_range = $1;}
	  | HEXSTRING  {$$=$$;}
	  | BINSTRING {$$=$$;}
	  ;

Enums: Enum
     | Enums ',' Enum
     | '{' Enum '}'
	  ;

Enum: NAME '(' AnyNum ')'     
     | CURRENT '('AnyNum ')'
     | DEPRECATED '('AnyNum ')'
     | HNAME '('AnyNum ')'
     | NAME
    ;

Access      : _READ_ONLY             {strcpy(i1_access,"read-only"); }
            | _READ_WRITE            {strcpy(i1_access,"read-write"); }
	    | _READ_CREATE           {strcpy(i1_access,"read-create"); }
	    | ACCESSIBLE_FOR_NOTIFY {strcpy(i1_access,"accessible-for-notify");}
	    | NOT_ACCESSIBLE        {strcpy(i1_access,"not-accessible"); }
	    ;

Status: CURRENT { strcpy (i1_status, "current"); }
      | DEPRECATED { strcpy (i1_status, "deprecated"); }
		| OBSOLETE { strcpy (i1_status, "obsolete"); }
		;

ReferPart: REFERENCE QSTRING
         |
         ;

IndexPart: _INDEX '{' IndexTypes '}'
         | AUGMENTS '{' NAME '}'   {
	                            INT1 i1_aug_syntax[MAX_DESC_LEN];
	                            tINDICES_LIST *p_indi_list= p_indices_ls;
				    tNAME_LIST *p_name;
				   if( get_syntax_of_augment($3,i1_aug_syntax) == ERROR ) {
               get_elements_of_augment($3);
				   /* fprintf(stderr,"Syntax of AUGMENT %s not found \n",$3);
				      yyerror(""); */
				   }
               else {
				    while ( p_indi_list ) {
				      if ( (strcmp(p_indi_list->name,i1_aug_syntax )) == 0) {
				      p_name = p_indi_list->name_list;
				      while ( p_name ) {
				      strcpy(p_table_struct->index_name[p_table_struct->no_of_indices],p_name->name);
                                       p_table_struct->no_of_indices++;
				       p_name = p_name->next;
				       }
                                     break;
				      }
				      else 
				      p_indi_list= p_indi_list->next;
				      }
                  }
				      }
				      

	|
			;

IndexTypes: IndexType
          | IndexTypes ',' IndexType
			 ;

IndexType: ObjectDescriptor         {
                                      tINDICES_LIST *p_temp=p_indices_ls;
				      tNAME_LIST *p_temp_name;
                                       if(i2_table_flag){
                                       strcpy(p_table_struct->index_name[p_table_struct->no_of_indices],$1);
                                        p_table_struct->no_of_indices++;
					if ( p_indices_ls == NULL ) {
					p_indices_ls = (tINDICES_LIST *)calloc(1,sizeof(tINDICES_LIST));
					if(p_indices_ls == NULL){
					fprintf(stderr,"malloc error \n");
					yyerror("");
					}
					p_indices_ls->name_list = NULL;
					p_indices_ls->next = NULL;
					strcpy(p_indices_ls->name,p_table_struct->entry_name);
					p_indices_ls->name_list = (tNAME_LIST *)calloc(1,sizeof(tNAME_LIST));
					if(p_indices_ls->name_list == NULL) {
					fprintf(stderr,"malloc error \n");
					yyerror("");
					}
					p_indices_ls->name_list->next = NULL;
					strcpy(p_indices_ls->name_list->name,$1);
					}
					else {
					while(p_temp) {
					  if((strcmp(p_table_struct->entry_name,p_temp->name)) == 0) {
                                          p_temp_name =p_temp->name_list;
					  while(p_temp_name->next != NULL)
					  p_temp_name = p_temp_name->next;
					  p_temp_name->next = (tNAME_LIST *)calloc(1,sizeof(tNAME_LIST));
					  if( p_temp_name->next == NULL ) {
					  fprintf(stderr,"malloc error \n");
					  yyerror("");
					  }
					  p_temp_name = p_temp_name->next;
					  if(p_temp_name == NULL) {
					  fprintf(stderr,"malloc error \n");
					  yyerror("");
					  }
					  p_temp_name->next = NULL;
					  strcpy(p_temp_name->name,$1);
					  break ;
					  }
					  else
					   p_temp = p_temp->next;
					       
					   }
					   if ( p_temp == NULL ) {
					   p_temp = p_indices_ls;
					   while(p_temp->next != NULL ){
					   p_temp = p_temp->next;
					   }
					   p_temp->next =(tINDICES_LIST *)calloc(1,sizeof(tINDICES_LIST));	
					   if(p_temp->next == NULL ) {
					   fprintf(stderr,"malloc error \n");
					   yyerror("");
					   }
					   p_temp = p_temp->next ;
					   p_temp->name_list = NULL;
					p_temp->next = NULL;
					strcpy(p_temp->name,p_table_struct->entry_name);
					p_temp->name_list = (tNAME_LIST *)calloc(1,sizeof(tNAME_LIST));
					if ( p_temp->name_list == NULL ) {
					fprintf(stderr,"malloc error \n");
					yyerror("");
					}
					p_temp->name_list->next = NULL;
					strcpy(p_temp->name_list->name,$1);

					}
					}
					}
					}
         | IMPLIED ObjectDescriptor     {
                                      tINDICES_LIST *p_temp=p_indices_ls;
                                      tIMPINDICES_LIST *pImpTemp=pImpIndx;
				      tNAME_LIST *p_temp_name;
           if(i2_table_flag)
           {
             if (pImpIndx == NULL ) 
             {
	       pImpIndx = (tIMPINDICES_LIST *)
                          calloc(1,sizeof(tIMPINDICES_LIST));
	       if(pImpIndx == NULL)
               {
		   yyerror("Import index Malloc Error");
	       }
	       pImpIndx->next = NULL;
	       strcpy(pImpIndx->name,$2);
             }
             else
             {
               while(pImpTemp->next != NULL)
               {
                 pImpTemp = pImpTemp->next;
               }
               pImpTemp->next = (tIMPINDICES_LIST *)
                   calloc(1,sizeof(tIMPINDICES_LIST));
               if(pImpTemp->next == NULL)
               {
                  yyerror("Import index Malloc Error");
               }
               pImpTemp->next->next = NULL;
               strcpy(pImpTemp->next->name,$2);
             }
           }
                                       if(i2_table_flag){
                                       strcpy(p_table_struct->index_name[p_table_struct->no_of_indices],$2);
                                        p_table_struct->no_of_indices++;
					if ( p_indices_ls == NULL ) {
					p_indices_ls = (tINDICES_LIST *)calloc(1,sizeof(tINDICES_LIST));
					if(p_indices_ls == NULL){
					fprintf(stderr,"malloc error \n");
					yyerror("");
					}
					p_indices_ls->name_list = NULL;
					p_indices_ls->next = NULL;
					strcpy(p_indices_ls->name,p_table_struct->entry_name);
					p_indices_ls->name_list = (tNAME_LIST *)calloc(1,sizeof(tNAME_LIST));
					if(p_indices_ls->name_list == NULL) {
					fprintf(stderr,"malloc error \n");
					yyerror("");
					}
					p_indices_ls->name_list->next = NULL;
					strcpy(p_indices_ls->name_list->name,$2);
					}
					else {
					while(p_temp) {
					  if((strcmp(p_table_struct->entry_name,p_temp->name)) == 0) {
                                          p_temp_name =p_temp->name_list;
					  while(p_temp_name->next != NULL)
					  p_temp_name = p_temp_name->next;
					  p_temp_name->next = (tNAME_LIST *)calloc(1,sizeof(tNAME_LIST));
					  if( p_temp_name->next == NULL ) {
					  fprintf(stderr,"malloc error \n");
					  yyerror("");
					  }
					  p_temp_name = p_temp_name->next;
					  if(p_temp_name == NULL) {
					  fprintf(stderr,"malloc error \n");
					  yyerror("");
					  }
					  p_temp_name->next = NULL;
					  strcpy(p_temp_name->name,$2);
					  break ;
					  }
					  else
					   p_temp = p_temp->next;
					       
					   }
					   if ( p_temp == NULL ) {
					   p_temp = p_indices_ls;
					   while(p_temp->next != NULL ){
					   p_temp = p_temp->next;
					   }
					   p_temp->next =(tINDICES_LIST *)calloc(1,sizeof(tINDICES_LIST));	
					   if(p_temp->next == NULL ) {
					   fprintf(stderr,"malloc error \n");
					   yyerror("");
					   }
					   p_temp = p_temp->next ;
					   p_temp->name_list = NULL;
					p_temp->next = NULL;
					strcpy(p_temp->name,p_table_struct->entry_name);
					p_temp->name_list = (tNAME_LIST *)calloc(1,sizeof(tNAME_LIST));
					if ( p_temp->name_list == NULL ) {
					fprintf(stderr,"malloc error \n");
					yyerror("");
					}
					p_temp->name_list->next = NULL;
					strcpy(p_temp->name_list->name,$2);

					}
					}
					}
					}
			;

ObjectId: '{'                 {strcpy (i1_object_identifier, "");}
	  Components '}'      { i1_object_identifier[strlen(i1_object_identifier) - 1] = '\0';}
	;

Components: Component
          | Components Component
          ;

Component: NameHName     {
	           	   strcat (i1_object_identifier, $1);
	                   strcat (i1_object_identifier, ".");
		         }

         | Num          {
	                    INT1 *pi1_temp;

			      pi1_temp = itoa ($1);
			      if (pi1_temp == NULL) { yyerror (""); }
                             strcat (i1_object_identifier, pi1_temp);
                             strcat (i1_object_identifier, ".");
														              }

         | NameHName '(' Num ')' {
	                            INT1 *pi1_temp;
                                
                                    pi1_temp = itoa ($3);
		                     if (pi1_temp == NULL) { yyerror (""); }
		                  strcat (i1_object_identifier, pi1_temp);
	           	          strcat (i1_object_identifier, ".");
																					           }

         ;

MibName: NameHName 
       ;

Type: NameHName 
    | MODULE_IDENTITY
    | OBJECT_IDENTITY
    | TEXTUAL_CONVENTION
    | OBJECT_TYPE
    | NOTIFICATION_TYPE
    | MODULE_COMPLIANCE
    | NOTIFICATION_GROUP
    | OBJECT_GROUP
    | AGENT_CAPABILITIES
    ;

NotificationName: NAME
                ;

ModuleName: NAME
          | { strcpy ($$, ""); /* chumma! to avoid warning */ }
          | HNAME
          ;

ObjectName: NAME {
                   tOBJECT_LS *p_object_ls_temp = p_object_ls;
                   INT4 i4_check_till_last_object = 0;
                   /*Checking whether NAME is notification type*/
                   if (i4_check_for_notify_object == 1)
		   {    
		       /*Check for the objectname in the list*/
		       if (p_object_ls_temp != NULL) 
		       { 

                       while (strcmp(p_object_ls_temp->name,$1))
                       {
                           if (p_object_ls_temp->next != NULL)
                           {    
                               p_object_ls_temp = p_object_ls_temp->next;
                           }
                           else
                           {
                               i4_check_till_last_object = 1;
                               break;
                           }    
                       }
                       /*Check whether the ACCESS clause of objectname is of type
                        not-accessible*/
                       if (i4_check_for_notify_object == 0)
                       {    
                           if (!strcmp(p_object_ls_temp->access,"not-accessible"))
                           {   
                               /*printf ("OBJ : %s,ACCESS : %s\n",p_object_ls_temp->name,
                                 p_object_ls_temp->access);*/
                               printf("\nERROR:Object type cannot have MAX-ACCESS as " 
                                      "not-accessible [Ref:RFC 2578 (Sec:8.1)]\n");
                               yyerror("");
                           }
                       }
                   }
                 }
	    }
          ;

SyntaxType: NAME 
          ;

ObjectDescriptor: NAME 
                ;

NameHName: NAME		 
			| HNAME
         ;

AnyNum: Num
      | '-' NUM { $$ = -$2; }
      ;

Num: NUM
   | _ZERO
   ;

%%

/*****************************************************************************
 *      Function Name        : parse                                         *
 *      Role of the function : Process the command line and calls parser fn. *
 *      Formal Parameters    : argc, pars                                    *
 *      Global Variables     :                                               *
 *      Use of Recursion     : None.                                         *
 *      Return Value         : None.                                         *
 *****************************************************************************/
INT4 parse(INT1 pars[MAX_LINE],INT1 *p1_inc_path)
{
   INT1 i1_response;
   INT1 *pi1_dotptr;
   INT4 i4_index;
   
	struct stat buf;

	strcpy(u1_incl_path,p1_inc_path);
	
		if ((yyin = fopen (pars, "r")) == NULL) {
		   fprintf (stderr, "\nCan't open %s\nPress <Enter> to continue... ", pars);
		   getchar();
		   return ERROR;
		}
		strcpy (i1_mibfile_name, pars);
		strcpy (i1_in_file, pars);
		strcpy (i1_out_file, pars);
      if (pi1_dotptr = strrchr (i1_out_file, '.')) {
         *pi1_dotptr = '\0';
      }
   	strcpy (i1_temp_file, "v2compXXXXXX");
   	if (mkstemp (i1_temp_file) == -1) {
   	   strcpy (i1_temp_file, "v2ctemp.def");
   	} 
   	if ((p_temp_file = fopen (i1_temp_file, "w")) == NULL) {
   		fprintf (stderr, "Can't open %s to write intermediate output\n", i1_temp_file);
			exit(0);   
   	}
      p_oid_list = NULL;
      p_object_list = NULL;
      p_import_list = NULL;
      p_table_entries = NULL;
      p_this_entry = NULL;
      p_indices = NULL;
      p_name_values = NULL;
      p_object_ids = NULL;
      p_type_defs = NULL;
      p_this_tc = NULL;
      p_object_ls = NULL ;
      p_indices_ls = NULL;
      yylineno = 1;
      printf ("\n%s...", i1_in_file);
      fflush (stdout);
      add_v2_typedefs();
      get_syntax_info_fromfile();
      yyparse ();
      {
      t_MIB_TABLE *p_temp,*p_temp_struct = p_scalar_struct;
      while(p_temp_struct != NULL){
      p_table_struct = p_temp_struct;
#ifdef DEBUG
      printf("BEFORE PRINTDATA \n");
      printdata(p_temp_struct); 
#endif
      i4_global_scalar_flag = p_temp_struct->no_of_objects;
      generator_module();
      oid_table_v1(u1_incl_path,i1_mibfile_name);
      p_temp = p_temp_struct;
      p_temp_struct = p_temp_struct->next;
      free(p_temp);
      }
      p_table_struct = NULL ;
      p_scalar_struct = NULL ;
      }
      {
      tTC_TABLE *temp=p_type_defs;
      while(temp)
      {
      temp = temp->next;
      }
      }
   	fclose (p_temp_file);
   	free_names_memory (p_oid_list);
   	free_names_memory (p_object_list);
   	free_names_memory (p_import_list);
   	free_object_memory (p_object_ls);
      free_name_values_memory (p_name_values);
      free_OID_memory (p_object_ids);
      free_tc_memory (p_type_defs);
      unlink(i1_temp_file);

}

/*****************************************************************************
 *      Function Name        : yyerror                                       *
 *      Role of the function : Prints error message passed and exits after   *
 *                             closing all files and unlinking output file.  *
 *      Formal Parameters    : pi1_mesg                                      *
 *      Global Variables     : p_out_file, p_temp_file, i1_out_file,         *
 *                             i1_in_file, i1_temp_file, p_oid_list          *
 *                             p_import_list, p_object_list                  *
 *      Use of Recursion     : None                                          *
 *      Return Value         : None                                          *
 *****************************************************************************/

INT4 yyerror (INT1 *pi1_mesg)
{
#ifdef OPEN_INPUT_FILE
   INT1 i1_cmd_line[MAX_LINE_LEN];
#endif /* OPEN_INPUT_FILE */
   INT1 i1_temp[MAX_LINE_LEN];

   fprintf (stderr, "\n%s(line %d): %s\n", i1_in_file, yylineno, pi1_mesg);
   fprintf (stderr, "Last token read %s\n", yytext);
	fclose (p_temp_file);
	unlink (i1_out_file);
	unlink (i1_temp_file);
	free_names_memory (p_oid_list);
	free_names_memory (p_object_list);
	free_names_memory (p_import_list);
   free_name_values_memory (p_name_values);
   free_OID_memory (p_object_ids);
   free_tc_memory (p_type_defs);
#ifdef OPEN_INPUT_FILE
   sprintf (i1_cmd_line, "vi +%d %s", yylineno, i1_in_file);
   fprintf (stderr, "Locating error in input file...\n");
   sleep (5);
   system (i1_cmd_line);
#endif /* OPEN_INPUT_FILE */
 /* Deleting the Temp Files. */
   strcpy(i1_temp, REMOVE);
   strcat(i1_temp, i1_inc_path);
   strcat(i1_temp, TEMP_FILE);
   system(i1_temp);
	exit (ERROR);
}

/*****************************************************************************
 *      Function Name        : printdata                                    *
 *      Role of the function : printd the data of the structure tMIB_TABLE . *
 *                             for debugging purpose only                    *
 *      Formal Parameters    : p_table_struct ( pointer to structure )       *
 *      Global Variables     :                                               *
 *      Use of Recursion     : None.                                         *
 *      Return Value         : None.                                         *
 *****************************************************************************/
printdata(t_MIB_TABLE *p_table_struct)
{
INT2 i1_temp;

printf("table name :     %s \n",p_table_struct->table_name);
printf("entry name :     %s \n",p_table_struct->entry_name);
printf("no. of obj :     %d \n",p_table_struct->no_of_objects);
printf("no. of imp :     %d \n",p_table_struct->no_of_imports);
for(i1_temp=0;i1_temp<p_table_struct->no_of_objects+p_table_struct->no_of_imports;i1_temp++)
printf("object name:       %s   %d  %s %d %d %d %d \n",p_table_struct->object[i1_temp].object_name,p_table_struct->object[i1_temp].position,p_table_struct->object[i1_temp].type_name,p_table_struct->object[i1_temp].access,p_table_struct->object[i1_temp].type,p_table_struct->object[i1_temp].size,p_table_struct->object[i1_temp].min_val);

for(i1_temp=0;i1_temp<p_table_struct->no_of_indices;i1_temp++)
printf("indice name:       %s   \n",p_table_struct->index_name[i1_temp]);
}

/*****************************************************************************
 *      Function Name        : write_access                                  *
 *      Role of the function : write appropriate access field for the present*
 *                             object in the structure tMIB_TABLE.           *
 *      Formal Parameters    : p_table_struct ( pointer to the structure     *
 *                             tMIB_TABLE                                    *
 *      Global Variables     :                                               *
 *      Use of Recursion     : None.                                         *
 *      Return Value         : None.                                         *
 *****************************************************************************/
write_access(t_MIB_TABLE *p_table_struct)
{
INT4 i4_imports;
INT4 i4_objects;

i4_imports = p_table_struct->no_of_imports;
i4_objects = p_table_struct->no_of_objects;
 if(strcmp(i1_access,"read-only") == 0) 
     p_table_struct->object[i4_imports+i4_objects].access = READ_ONLY ;
 if(strcmp(i1_access,"read-write") == 0) 
     p_table_struct->object[i4_imports+i4_objects].access = READ_WRITE;
 if(strcmp(i1_access,"not-accessible") == 0)
     p_table_struct->object[i4_imports+i4_objects].access = NO_ACCESS ;
 if(strcmp(i1_access,"accessible-for-notify") == 0)
  p_table_struct->object[i4_imports+i4_objects].access = ACC_FOR_NOTIFY;
 if(strcmp(i1_access,"read-create") == 0)
     p_table_struct->object[i4_imports+i4_objects].access = READ_CREATE;

}

/*****************************************************************************
 *      Function Name        : write_status                                  *
 *      Role of the function : write appropriate status field for the present*
 *                             object in the structure tMIB_TABLE.           *
 *      Formal Parameters    : p_table_struct ( pointer to the structure     *
 *                             tMIB_TABLE                                    *
 *      Global Variables     :                                               *
 *      Use of Recursion     : None.                                         *
 *      Return Value         : None.                                         *
 *****************************************************************************/
write_status(t_MIB_TABLE *p_table_struct)
{
 INT4 i4_imports;
 INT4 i4_objects;

 i4_imports = p_table_struct->no_of_imports;
 i4_objects = p_table_struct->no_of_objects;

 if(strcmp(i1_status,"current") == 0) 
     p_table_struct->object[i4_imports+i4_objects].status = STATUS_CURRENT ;
 if(strcmp(i1_status,"deprecated") == 0) {
     p_table_struct->object[i4_imports+i4_objects].status = STATUS_DEPRECATED;
     p_table_struct->object[i4_imports+i4_objects].isDeprecated = TRUE;
 }
 if(strcmp(i1_status,"obsolete") == 0)
     p_table_struct->object[i4_imports+i4_objects].status = STATUS_OBSOLETE ;

}

/*****************************************************************************
 *      Function Name        : write_table_status                            *
 *      Role of the function : writes appropriate status field for the       *
 *                             current tMIB_TABLE structure object.          *
 *      Formal Parameters    : None.                                         *
 *      Global Variables     : p_table_struct                                *
 *      Use of Recursion     : None.                                         *
 *      Return Value         : None.                                         *
 *****************************************************************************/
write_table_status()
{


 if(strcmp(i1_status,"current") == 0) 
     p_table_struct->status = STATUS_CURRENT ;
 if(strcmp(i1_status,"deprecated") == 0) 
     p_table_struct->status = STATUS_DEPRECATED;
 if(strcmp(i1_status,"obsolete") == 0)
     p_table_struct->status = STATUS_OBSOLETE ;

}

/*****************************************************************************
 *      Function Name        : write_syntax                                  *
 *      Role of the function : write appropriate syntax field for the present*
 *                             object in the structure tMIB_TABLE.           *
 *      Formal Parameters    : p_table_struct ( pointer to the structure     *
 *                             tMIB_TABLE                                    *
 *      Global Variables     :                                               *
 *      Use of Recursion     : None.                                         *
 *      Return Value         : None.                                         *
 *****************************************************************************/

write_syntax(t_MIB_TABLE *p_table_struct)
{
INT1 i1_temp_syntax[MAX_LINE];
INT1 i1_temp[MAX_LINE];
tTC_TABLE *p_temp=p_type_defs;
INT4 i4_imports;
INT4 i4_objects;
INT1 i=0 , j=0;

i4_imports = p_table_struct->no_of_imports;
i4_objects = p_table_struct->no_of_objects;
strcpy(i1_temp_syntax,i1_syntax);
if( primitive_test(i1_syntax) == 1 ){
      strcpy(p_table_struct->object[i4_imports+i4_objects].type_name,i1_syntax);
      strcpy(i1_temp_syntax,i1_syntax);
      }
      else {
          while(p_temp)
	  {
	  if(strcmp(p_temp->tc_name,i1_syntax) == 0) {
            if(strcmp(p_temp->tc_name,"RowStatus") == 0) {
                p_table_struct->object[i4_imports+i4_objects].isRowStatus = TRUE;
            }
            if(strcmp(p_temp->tc_name,"EntryStatus") == 0) {
                p_table_struct->object[i4_imports+i4_objects].isRowStatus = TRUE;
            } 
      strcpy(p_table_struct->object[i4_imports+i4_objects].type_name,p_temp->tc_base);
	  strcpy(i1_temp_syntax,p_temp->tc_base);
	  break;
	  }
	  else
	  p_temp = p_temp->next;
      }
      
      if(p_temp == NULL )
      {
      fprintf(stderr,"\nPrimitive type of %s not found .\nPl. Enter the syntax in info.def file \nFor further details use user guide.\nPl <Enter> to cntinue.",i1_syntax);
      getchar();
      yyerror("");
      }
     
      }
 if( strcmp(i1_temp_syntax,"Integer32") == 0 || strcmp(i1_temp_syntax,"INTEGER") == 0)
        p_table_struct->object[i4_imports+i4_objects].type = INTEGER ;

 else if( strcmp(i1_temp_syntax,"Counter32") == 0 || strcmp(i1_temp_syntax,"Counter64") == 0 || strcmp(i1_temp_syntax,"TimeTicks") == 0 || strcmp(i1_temp_syntax,"Gauge32") == 0 || strcmp(i1_temp_syntax,"Unsigned32") == 0 ) {
 p_table_struct->object[i4_imports+i4_objects].type = POS_INTEGER;
         p_table_struct->object[i4_imports+i4_objects].size = INTEGER; }

 else if( strcmp(i1_temp_syntax,"OBJECT IDENTIFIER") == 0 ) {
 p_table_struct->object[i4_imports+i4_objects].type = OBJECT_IDENTIFIER;
         p_table_struct->object[i4_imports+i4_objects].size = 0; }

 else if( strcmp(i1_temp_syntax,"NetworkAddress") == 0 ) {
 p_table_struct->object[i4_imports+i4_objects].type = ADDRESS ;
         p_table_struct->object[i4_imports+i4_objects].size = ADDRESS ; }

 else if( strcmp(i1_temp_syntax,"IpAddress") == 0 ) {
 p_table_struct->object[i4_imports+i4_objects].type = ADDRESS ;
         p_table_struct->object[i4_imports+i4_objects].size = ADDRESS ; }

 else if( strcmp(i1_temp_syntax,"MacAddress") == 0 ) {
 i4_global_mac_flag = 1;

 p_table_struct->object[i4_imports+i4_objects].type = MacAddress;
         p_table_struct->object[i4_imports+i4_objects].size = MacAddress ; }


 else if( strncmp(i1_temp_syntax,"OCTET STRING",strlen("OCTET STRING")) == 0 ) {
 p_table_struct->object[i4_imports+i4_objects].type = OCTET_STRING;
 if(strlen(i1_temp_syntax) != strlen("OCTET STRING"))
 {
    /* If Range is provided like 0..20 or 2|4 its clearly SNMP_DATA_TYPE_OCTET_PRIM type 
     * so skip the value calculation
     */
    if ((strstr (i1_temp_syntax, "..") == NULL) &&
        (strstr (i1_temp_syntax, "|") == NULL))
    {

       for( i = strlen("OCTET STRING") + 1, j=0; i1_temp_syntax[i] != '\0' ; i++,j++)
       {
          i1_temp[j] = i1_temp_syntax[i];
       }

       i4_lowVal = atoi(i1_temp);
       i4_range = i4_lowVal;
    }
 }
      if(i4_range)
      {
       p_table_struct->object[i4_imports+i4_objects].size = i4_range;
       p_table_struct->object[i4_imports+i4_objects].min_val= i4_lowVal;
      }
	 else
         {
       p_table_struct->object[i4_imports+i4_objects].size = 256 ;
       p_table_struct->object[i4_imports+i4_objects].min_val= 0 ;
	 }
	 }
 else {
 	fprintf(stderr,"Syntax not clear for object %s \n",p_table_struct->object[i4_imports+i4_objects].object_name );
	yyerror("");
}
}


/*****************************************************************************
 *      Function Name        : write_scalar                                  *
 *      Role of the function : Writes the scalar Object fields in the        *
 *                             structure tMIB_TABLE                          *
 *      Formal Parameters    : None.                                         *
 *      Global Variables     :                                               *
 *      Use of Recursion     : None.                                         *
 *      Return Value         : None.                                         *
 *****************************************************************************/


write_scalar()
{
t_MIB_TABLE *p_temp,*p_temp_follo;
INT1 i1_entry_name[MAX_LINE];
INT1 i1_table_name[MAX_LINE];
INT1 *p1_temp,*p1_position;

strcpy(i1_table_name,i1_object_identifier);
p1_temp = strrchr(i1_table_name,'.');
if(p1_temp != NULL )
   *p1_temp = '\0';
   p1_position = p1_temp;
   strcpy(i1_entry_name,i1_table_name);
 /*  strcat(i1_entry_name,"ScalarEntry");
getchar();  */
if ( p_scalar_struct == NULL )
{
/*  printf("new objecr\n"); */
   p_scalar_struct = (t_MIB_TABLE *)calloc(1,sizeof(t_MIB_TABLE));
   strcpy(p_scalar_struct->table_name,i1_table_name);
   strcpy(p_scalar_struct->entry_name,i1_entry_name);
}
p_temp = p_scalar_struct;


if (!i4_check_repeat_table)
       i4_check_repeat_table = check_table_name();


  while(p_temp)
  {
   if(strcmp(p_temp->table_name,i1_table_name) == 0 )
   {
 /*    printf("pre and this obj is same \n");
     getchar(); */
     break;
   }
   p_temp_follo = p_temp;
   p_temp = p_temp->next;
  }

  if(p_temp == NULL)
  {
  p_temp_follo->next = (t_MIB_TABLE *)calloc(1,sizeof(t_MIB_TABLE));
  strcpy(p_temp_follo->next->table_name,i1_table_name);
  strcpy(p_temp_follo->next->entry_name,i1_entry_name);
  p_temp = p_temp_follo->next;
  }
  strcpy(p_temp->object[p_temp->no_of_objects].object_name,i1_object_desc);
  if( p1_position == NULL )
  {
    fprintf(stderr,"POSITION NOT CLEAR FOR OBJECT %s \n",i1_object_desc);
    yyerror("");
  }
  p_temp->object[p_temp->no_of_objects].position = atoi(p1_position+1);
  write_syntax(p_temp);
  write_access(p_temp);
  write_status(p_temp);
  p_temp->no_of_objects++;


}



/*****************************************************************************
 *      Function Name        : check_object_in_table_list                    *
 *      Role of the function : Checks for the preticular object in a table   *
 *                             entry. ( place of use: checking, wheather all *
 *                             indices defined in table or not )             *
 *      Formal Parameters    : p1_name ( pointer to the object name)         *
 *      Global Variables     :                                               *
 *      Use of Recursion     : None.                                         *
 *      Return Value         : TRUE/FALSE.                                   *
 *****************************************************************************/


INT4 check_object_in_table_list(INT1 *name)
 {
 tNAME_LIST *temp=p_table_entries;
 if(p_table_entries == NULL ) return FALSE;
 while(temp != NULL )
 {
 if ( strcmp(name,temp->name) == 0 )
    return TRUE;
    else
    temp = temp->next;
 }
 return FALSE;
 }


/*****************************************************************************
 *      Function Name        : compute_primitive                             *
 *      Role of the function : Findout the primitive syntax .                *
 *      Formal Parameters    : p1_syntax . (pointer to syntax, which is not  *
 *                             primitive                                     *
 *      Global Variables     :                                               *
 *      Use of Recursion     : YES.                                          *
 *      Return Value         : pointer to the primitive syntax.(INT1 *)      *
 *****************************************************************************/

INT1 *compute_primitive(INT1 *p1_syntax)
{
tTC_TABLE  *p_temp;
INT1 i1_name[MAX_LINE];

p_temp = p_type_defs;
while(p_temp){
     if ( strcmp(p_temp->tc_name,p1_syntax) == 0)
         if( primitive_test(p_temp->tc_base) == 1 )
	    return p_temp->tc_base;
	 else {
	 return  compute_primitive(p_temp->tc_base);
	 }
       else
	    p_temp = p_temp->next;
	 }
     fprintf(stderr," he Primitive Syntax not found for %s \n",p1_syntax);
     yyerror("");

 }





/*****************************************************************************
 *      Function Name        : primitive_test                                *
 *      Role of the function : Checks, wheather given name is primitive      *
 *                             syntax or not.                                *
 *      Formal Parameters    : p1_name. (pointer to the syntax name)         *
 *      Global Variables     :                                               *
 *      Use of Recursion     : None.                                         *
 *      Return Value         : TRUE/FALSE                                    *
 *****************************************************************************/




 primitive_test(INT1 *p1_name)
 {
 tTC_TABLE *p_temp=p_type_defs;
 
   if((strcmp(p1_name,"INTEGER")==0) ||(strcmp(p1_name,"Unsigned32")==0) ||(strcmp(p1_name,"Integer32")==0) ||(strcmp(p1_name,"Counter32")==0) ||(strcmp(p1_name,"Counter64")==0) ||(strcmp(p1_name,"Gauge32")==0) ||(strcmp(p1_name,"OBJECT IDENTIFIER")==0) ||(strncmp(p1_name,"OCTET STRING",strlen("OCTET STRING"))==0) ||(strcmp(p1_name,"NetworkAddress")==0) ||(strcmp(p1_name,"MacAddress")==0) ||(strcmp(p1_name,"IpAddress")==0) ||(strcmp(p1_name,"TimeTicks")==0) ){
   return 1;
   }
   return 0;
   }



/*****************************************************************************
 *      Function Name        : add_v2_typedefd                               *
 *      Role of the function : Initial addition SMI-V2 typedefs ( TEXTUAL    *
 *                             CONVENTIONs that are defined in rfc1902       *
 *      Formal Parameters    : None.                                         *
 *      Global Variables     :                                               *
 *      Use of Recursion     : None.                                         *
 *      Return Value         : None.                                         *
 *****************************************************************************/



void  add_v2_typedefs()
{

add_typedef("DisplayString","OCTET STRING");
add_typedef("PhysAddress","OCTET STRING");
add_typedef("MacAddress","OCTET STRING");
add_typedef("TruthValue","INTEGER");
add_typedef("TestAndIncr","INTEGER");
add_typedef("AutonomousType","OBJECT IDENTIFIER");
add_typedef("InstancePointer","OBJECT IDENTIFIER");
add_typedef("VariablePointer","OBJECT IDENTIFIER");
add_typedef("RowPointer","OBJECT IDENTIFIER");
add_typedef("RowStatus","INTEGER");
add_typedef("TimeStamp","TimeTicks");
add_typedef("TimeInterval","INTEGER");
add_typedef("DateAndTime","OCTET STRING");
add_typedef("StorageType","INTEGER");
add_typedef("TDomain","OBJECT IDENTIFIER");
add_typedef("TAddress","OCTET STRING");
add_typedef("BITS","OCTET STRING");
add_typedef("EntryStatus","INTEGER");
}

/*****************************************************************************
 *      Function Name        : add_typedefs                                  *
 *      Role of the function : addition of new types of syntax.(TEX. CONVEN) *
 *      Formal Parameters    : p_name1,p_name2 ( p_name1 is new name, whereas*
 *                             p_name2 is primitive type syntax              *
 *      Global Variables     :                                               *
 *      Use of Recursion     : None.                                         *
 *      Return Value         : None.                                         *
 *****************************************************************************/
void add_typedef(INT1 *p_name1,INT1 *p_name2)
{
tTC_TABLE  *p_temp , *p_temp2;

 p_temp = (tTC_TABLE *) malloc(sizeof(tTC_TABLE));
 if(p_temp == NULL) {
 fprintf(stderr,"malloc error \n");
 exit(0);
 }
 p_temp->next = NULL;
 p_temp->tc_name_value_list = NULL;
 strcpy(p_temp->tc_name,p_name1);
 strcpy(p_temp->tc_base,p_name2);
 if(p_type_defs == NULL )
  p_type_defs = p_temp;
  else {
   p_temp2 = p_type_defs;
    while(p_temp2->next)
     p_temp2 = p_temp2->next;
    p_temp2->next = p_temp;
       }
 }

/*****************************************************************************
 *      Function Name        : get_syntax_info_fromfile                      *
 *      Role of the function : get the syntax from info.def file.            *
 *      Formal Parameters    : None.                                         *
 *      Global Variables     : INFOFILE                                      *
 *      Use of Recursion     : None.                                         *
 *      Return Value         : None.                                         *
 *****************************************************************************/
 void  get_syntax_info_fromfile()
 {

 FILE *fp;
 INT1 i1_line[MAX_LINE];
 INT1 i1_old_name[MAX_LINE];
 INT1 i1_new_name[MAX_LINE];
 INT1 i1_new_name1[MAX_LINE];
 INT1 i1_new_name2[MAX_LINE];
 INT1 i1_temp[MAX_LINE];
 INT1 *p1_temp;
 INT1 i = 0;

 if( (fp = fopen(INFOFILE,"r")) != NULL)  {
    bzero(i1_line,MAX_LINE);
   fgets(i1_line,MAX_LINE,fp);
   while(!feof(fp)) {
    if(strstr(i1_line,"::=")!= NULL) {
     bzero(i1_new_name,MAX_LINE);
     bzero(i1_new_name1,MAX_LINE);
     bzero(i1_new_name2,MAX_LINE);
     sscanf(i1_line,"%s %s %s %s %s",i1_old_name,i1_temp,i1_new_name,i1_new_name1,i1_new_name2);
    if( strlen(i1_new_name1) ){
     i1_new_name[strlen(i1_new_name)] = ' ';
        /* Expecting single space between OCTET STRING and (SIZE(x)) for fixed length ocet string
        in info.def file*/
          if((strlen(i1_new_name1) > 6) && (strstr(i1_new_name1,"STRING")!= NULL))
          {
             printf("\n Error : Expecting single space between OCTET STRING and SIZE in info.def for object %s\n",i1_old_name);
             yyerror(""); 
          }
     strncpy(i1_new_name+strlen(i1_new_name),i1_new_name1,strlen(i1_new_name1));
       }
    if( strlen(i1_new_name2) ){
       if(strstr(i1_new_name2,"SIZE")!= NULL){
		    if (strstr(i1_new_name2,")")== NULL)
		    {
			    printf("\n Error : Enter correct syntax in info.def OCTET STRING (SIZE(x)) for object %s\n",i1_old_name);
			    yyerror("");
		    }

     i1_new_name[strlen(i1_new_name)] = ' ';
     for( i = 6; i1_new_name2[i] != ')' ; i++){
     i1_new_name[strlen(i1_new_name)] = i1_new_name2[i];
     }
       }
    }
	if( primitive_test(i1_new_name) == 0 ){
          p1_temp = compute_primitive(i1_new_name);
         add_typedef(i1_old_name,p1_temp);
	 }
	 else
         add_typedef(i1_old_name,i1_new_name);

         }
   fgets(i1_line,MAX_LINE,fp);
        }
	fclose(fp);
       }
       
 }
     
/*****************************************************************************
 *      Function Name        : check_for_imports.                            *
 *      Role of the function : Check wheather any indices imported.          *
 *      Formal Parameters    :                                     *
 *      Global Variables     :                                               *
 *      Use of Recursion     : None.                                         *
 *      Return Value         : None.                                         *
 *****************************************************************************/

void check_for_imports()
{
INT4 i4_indices_count;
INT4 i4_object_count;
INT4 i4_count;
INT4 i4_flag= 0;

i4_object_count = p_table_struct->no_of_objects;
for(i4_indices_count=0;i4_indices_count < p_table_struct->no_of_indices;i4_indices_count++) {
      i4_flag = 0;
      if ( already_in_table_entry(p_table_struct->index_name[i4_indices_count]) == 0 )
      if(check_indices_defination(p_table_struct->index_name[i4_indices_count]) == 0 ) {
      for(i4_count=0;i4_count < i4_object_count;i4_count++)
      {
      if( strcmp(p_table_struct->index_name[i4_indices_count],p_table_struct->object[i4_count].object_name) == 0 ){
      i4_flag = 1;
      break;
      }
      }
      if(i4_flag == 0 ){
        add_into_object_list(p_table_struct->index_name[i4_indices_count]);
      }

     }
     }

}



/*****************************************************************************
 *      Function Name        : add_into_object_list                          *
 *      Role of the function : If Indices is impored, add that object in the *
 *                             structure tMIB_TABLE                          *
 *      Formal Parameters    : p1_name ( name of the indices).               *
 *      Global Variables     :                                               *
 *      Use of Recursion     : None.                                         *
 *      Return Value         : None.                                         *
 *****************************************************************************/


 void   add_into_object_list(INT1 *p1_name)
 {
 INT1 i1_syntax_type[MAX_LINE];
 INT1 *p1_temp;
 INT1 i1_flag = ZERO;
 INT4 i4Counter = ZERO;

for (i4Counter = ZERO; i4Counter < p_table_struct->no_of_objects + p_table_struct->no_of_imports;
     i4Counter++)
{

   if( strcmp(p_table_struct->object[i4Counter].object_name,p1_name) == 0)
      i1_flag = 1;
}

if (i1_flag == ZERO)
{

    strcpy(p_table_struct->object[p_table_struct->no_of_objects+p_table_struct->no_of_imports].object_name,p1_name);

   if( get_syntax_fronfile(p1_name,i1_syntax_type) == ERROR ) {
   fprintf(stderr,"Syntax of Imported object %s not found,\n"
                   "for details read user guide. \n",p1_name);
		   yyerror("");
   }
strcpy(i1_syntax,i1_syntax_type);
write_syntax(p_table_struct);
p_table_struct->object[p_table_struct->no_of_objects+p_table_struct->no_of_imports].access = NO_ACCESS;
p_table_struct->object[p_table_struct->no_of_objects+p_table_struct->no_of_imports].position = 0 ;
p_table_struct->no_of_imports++;
}



    }

/*****************************************************************************
 *      Function Name        : main                                          *
 *      Role of the function : If it is imported  object, get syntaxx from   *
 *                             info.def file                                 *
 *      Formal Parameters    : p1-name,p1_syntax                              *
 *      Global Variables     : INFOFILE.                                     *
 *      Use of Recursion     : None.                                         *
 *      Return Value         : ERROR/SUCCESS                                 *
 *****************************************************************************/

 INT4 get_syntax_fronfile(INT1 *p1_name,INT1 *p1_syntax)
 {
  FILE *fp;
  INT1 i1_line[MAX_LINE];
  INT1 i1_old_name[MAX_LINE];
  INT1 i1_new_name[MAX_LINE];
  INT1 i1_temp1[MAX_LINE];
  INT1 i1_temp2[MAX_LINE];
  INT1 i1_temp3[MAX_LINE];
  INT1 i1_temp4[MAX_LINE];
  INT1 i4_temp4;
  INT1 *p1_temp;
  INT1 i=0;


 
 if( (fp = fopen(INFOFILE,"r")) != NULL)  {
   bzero(i1_line,MAX_LINE);
   fgets(i1_line,MAX_LINE,fp);
    while(!feof(fp)) {
    if(strstr(i1_line,"IMPORT OBJECT")!= NULL) {
    bzero(i1_temp2,MAX_LINE);
    bzero(i1_temp3,MAX_LINE);
    bzero(i1_temp4,MAX_LINE);
    bzero(i1_new_name,MAX_LINE);
    i4_temp4 = 0;
      sscanf(i1_line,"%s %s %s %s %s %s",i1_old_name,i1_temp1,i1_temp2,i1_new_name,i1_temp3,i1_temp4);
      if(strcmp(p1_name,i1_old_name) == 0) {
        if( strlen(i1_temp3) ){
	 i1_new_name[strlen(i1_new_name)] = ' ';
     /* Expecting single space between OCTET STRING and (SIZE(x)) for fixed length ocet string
        in info.def file*/
     if((strlen(i1_temp3) > 6) && (strstr(i1_temp3,"STRING")!= NULL))
     {
        printf("\n Error : Expecting single space between OCTET STRING and SIZE in info.def for object %s\n",i1_old_name);
        yyerror(""); 
     }
	 strncpy(i1_new_name+strlen(i1_new_name),i1_temp3,strlen(i1_temp3));
	}
    if( strlen(i1_temp4) ){
       if(strstr(i1_temp4,"SIZE")!= NULL){
	       if (strstr(i1_temp4,")")== NULL)
	       {
		       printf("\n Error : Enter correct syntax in info.def OCTET STRING (SIZE(x)) for object %s\n",i1_old_name);
		       yyerror("");
	       }

     i1_new_name[strlen(i1_new_name)] = ' ';
     for( i = 6; i1_temp4[i] != ')' ; i++){
     i1_new_name[strlen(i1_new_name)] = i1_temp4[i];
     }
     }
    }
      if( primitive_test(i1_new_name) == 0 ){
        p1_temp = compute_primitive(i1_new_name);
        add_typedef(i1_old_name,p1_temp);
        strcpy(p1_syntax,p1_temp);
         }
      else
        strcpy(p1_syntax,i1_new_name);
	i4_range = i4_temp4;
         return SUCCESS ;
         }
	 }
      fgets(i1_line,MAX_LINE,fp);
     }
    fclose(fp);
   }
   return ERROR;

}

/*****************************************************************************
 *      Function Name        : store_object_info                             *
 *      Role of the function : store the object info in a linked list.       *
 *      Formal Parameters    : None.                                         *
 *      Global Variables     : i1_object_desc,i1_access,i1_syntax            *
 *      Use of Recursion     : None.                                         *
 *      Return Value         : None.                                         *
 *****************************************************************************/

void store_object_info()
{
tOBJECT_LS *p_temp;
tOBJECT_LS *p_temp1;
INT1 *p1_syntax;

p_temp = (tOBJECT_LS *) calloc(1,sizeof(tOBJECT_LS));

 if(p_temp == NULL )
 yyerror(" malloc error ");
p_temp->next = NULL;
 strcpy(p_temp->name,i1_object_desc);
 strcpy(p_temp->access,i1_access);
 p_temp->range = i4_range;
 p_temp->lowVal = i4_lowVal;
 strcpy(p_temp->syntax,i1_syntax);
 /*
if( primitive_test(i1_syntax) == 1 )
 strcpy(p_temp->syntax,i1_syntax);
 else{
   p1_syntax = compute_primitive(i1_syntax);
   strcpy(p_temp->syntax,p1_syntax);
 }
 */
 p_temp1 = p_object_ls;
 if(p_object_ls == NULL )
 p_object_ls = p_temp;
 else {
    while(p_temp1->next != NULL ){
      p_temp1 = p_temp1->next;
      }
      p_temp1->next = p_temp;
      }
      }
   
 
/*****************************************************************************
 *      Function Name        : check_indices_defination                      *
 *      Role of the function : Check, wheather indices are previously defined*
 *                             in the MIB defination file.                   *
 *      Formal Parameters    : p1_name ( name of the indices).               *
 *      Global Variables     : p_object_ls                                   *
 *      Use of Recursion     : None.                                         *
 *      Return Value         : TRUE/FALSE                                    *
 *****************************************************************************/

INT4 check_indices_defination(INT1 *p1_name) 
{
tOBJECT_LS *p_temp = p_object_ls;
INT4 i4_object_no;
INT4 i4_import_no;
INT4 i4Counter = ZERO;
INT1 *p1_temp;
INT1 i1_flag = ZERO;
for (i4Counter = ZERO; i4Counter < p_table_struct->no_of_objects + p_table_struct->no_of_imports;
     i4Counter++)
{

   if( strcmp(p_table_struct->object[i4Counter].object_name,p1_name) == 0)
      i1_flag = 1;
}
if(i1_flag == ZERO)
{

i4_object_no = p_table_struct->no_of_objects;
i4_import_no = p_table_struct->no_of_imports;
while( p_temp != NULL )
{
if ( strcmp(p_temp->name,p1_name ) == 0 ){
 strcpy(p_table_struct->object[i4_object_no+i4_import_no].object_name,p_temp->name);
i4_range = p_temp->range;
i4_lowVal = p_temp->lowVal;
strcpy(i1_access,p_temp->access);
  if( primitive_test(p_temp->syntax) == 0 ) {
  p1_temp = compute_primitive(p_temp->syntax);
  strcpy(i1_syntax,p1_temp);
  }
  else
  strcpy(i1_syntax,p_temp->syntax);
write_syntax(p_table_struct);
write_access(p_table_struct);
p_table_struct->object[i4_object_no+i4_import_no].position = 0;
p_table_struct->no_of_imports++;
return TRUE;
}
    else
    p_temp = p_temp->next;
 }

 return FALSE ;
}
return TRUE;
}


/*****************************************************************************
 *      Function Name        : already_in_table_entry                        *
 *      Role of the function : checks for a perticular object, already       *
 *                              defined in a table.                          *
 *      Formal Parameters    : p1_name, name of the object.                  *
 *      Global Variables     :                                               *
 *      Use of Recursion     : None.                                         *
 *      Return Value         : TRUE/FALSE                                    *
 *****************************************************************************/

INT4 already_in_table_entry(INT1 *p1_name)
{
INT4 i4_count;

  for(i4_count =0 ;i4_count < p_table_struct->no_of_objects ; i4_count++)
     if (( strcmp(p_table_struct->object[i4_count].object_name,p1_name)) == 0 )
        return TRUE;

	return FALSE;

}

#define INFOFILE "info.def"
#define MAX_AUGMENT_ELEMENTS 10 
INT1 i1_temp[MAX_AUGMENT_ELEMENTS+2][MAX_LINE];

void get_fill_syntax()
{
 INT4 i4Count = 0;
 for(i4Count =2; i4Count < (MAX_AUGMENT_ELEMENTS+2); i4Count++)
 {
   if (strlen(i1_temp[i4Count]) == 0)
   {
     break;
   }
   strcpy(p_table_struct->index_name[p_table_struct->no_of_indices],
          i1_temp[i4Count]);
   p_table_struct->no_of_indices++;
 }
}


void get_elements_of_augment(INT1 *p1_name)
{
  FILE *fp;
  int   i,j;
  INT1  i1ExistsFlag =0; 
  INT1 i1_line[MAX_LINE], *pTemp = NULL;
  INT1 i1EntryFound = 0;

  /* Here we are supporting AUGMENT with 10 Elements.
   * If the AUGMENT Elements are more than 10 then
   * this tool will ignore and only takes first 10 Elements
   */
  if((fp = fopen(INFOFILE,"r")) != NULL)  {
   bzero(i1_line,MAX_LINE);
   fgets(i1_line,MAX_LINE,fp);
   while(!feof(fp))
   {
    if(strstr(i1_line,"AUGMENT")!= NULL)
    {
     bzero(i1_temp,sizeof(i1_temp));
     sscanf(i1_line,"%s %s %s %s %s %s %s %s %s %s %s %s", 
            i1_temp[0],i1_temp[1],i1_temp[2],i1_temp[3],i1_temp[4],
            i1_temp[5],i1_temp[6],i1_temp[7],i1_temp[8],i1_temp[9],
            i1_temp[10],i1_temp[11]);
     for(i=0; i <= 10;i++)
     {
       if (strcmp(i1_temp[i],"IMPLIED") == 0)
       {
            tIMPINDICES_LIST *pImpTemp = pImpIndx; 
            i1ExistsFlag = 0;
            if (pImpIndx == NULL ) 
            {
	        pImpIndx = (tIMPINDICES_LIST *)
                          calloc(1,sizeof(tIMPINDICES_LIST));
	        if(pImpIndx == NULL)
                {
                    yyerror("Import index Malloc Error");
	        }
	        pImpIndx->next = NULL;
	        strcpy(pImpIndx->name,i1_temp[i+1]);
            }
            else
            {
                if(strcmp(pImpTemp->name,i1_temp[i+1]) == 0)
                {
                    i1ExistsFlag = 1;
                }
                while(pImpTemp->next != NULL)
                {
                    if(strcmp(pImpTemp->name,i1_temp[i+1]) == 0)
                    {
                       i1ExistsFlag = 1;
                    }
                    pImpTemp = pImpTemp->next;
                }
                if(i1ExistsFlag != 1)
                {
                    pImpTemp->next = (tIMPINDICES_LIST *)
                    calloc(1,sizeof(tIMPINDICES_LIST));
                    if(pImpTemp->next == NULL)
                    {
                        yyerror("Import index Malloc Error");
                    }
                    pImpTemp->next->next = NULL;
                    strcpy(pImpTemp->next->name,i1_temp[i+1]);
                }
            }
            break;
        } 
     } 
     for(j=i;j<=10;j++)
     {
        strcpy(i1_temp[j],i1_temp[j+1]);
     }

     if(strcmp(i1_temp[0], p1_name) == 0)
     {
       i1EntryFound = 1;
       break;
     }
    }
    bzero(i1_line,MAX_LINE);
    fgets(i1_line,MAX_LINE,fp);
   }
   fclose(fp);
 }
 if (i1EntryFound == 1)
 {
   get_fill_syntax();
 }
 else
 { 
   fprintf(stderr,"Syntax of AUGMENT %s not found \n",p1_name);
   yyerror(""); 
 } 
}

/*****************************************************************************
 *      Function Name        : get_syntax_of_augment                         *
 *      Role of the function : Finds the syntax of the object which is       *
 *                              defined in AUGMENT clause                    *
 *      Formal Parameters    : p1_name, name of the object.                  *
 *      Global Variables     :                                               *
 *      Use of Recursion     : None.                                         *
 *      Return Value         : ERROR/SUCCESS                                 *
 *****************************************************************************/
get_syntax_of_augment(INT1 *p1_name,INT1 *p1_syntax )
{
      tOBJECT_LS *p_object_temp = p_object_ls;
        while ( p_object_temp ) {
	if ( strcmp(p1_name,p_object_temp->name) == 0) {
	strcpy(p1_syntax,p_object_temp->syntax);
	   return SUCCESS;
	   }
	   p_object_temp = p_object_temp->next;
	   }
	   return ERROR;
}


INT4 check_table_name()
{
INT4 index;
for ( index = 1; index <= i4_no_of_table_scalar; index++ )
{
    if((strcmp(scalar_table_name[index], p_scalar_struct->table_name))==0)
        return TRUE;
}
return FALSE;
}

reg_table_name()
{
i4_no_of_table_scalar++;
scalar_table_name[ i4_no_of_table_scalar ] = (INT1*)malloc(255);
strcpy(scalar_table_name[i4_no_of_table_scalar],p_table_struct->table_name);
}


