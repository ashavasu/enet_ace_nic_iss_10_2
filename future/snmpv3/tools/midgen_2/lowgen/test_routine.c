/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: test_routine.c,v 1.4 2015/04/28 14:43:31 siva Exp $
 *
 * Description: This File Contains the function Which     
 *              generate the Middle level TEST Routines .
 *******************************************************************/

#include "include.h"
#include "extern.h"

#ifdef CORRECT_MIDGEN
extern INT4         i4_check_repeat_table;
extern INT4         p_temp_counter;
#endif

/******************************************************************************
*      function Name        : test_routine                                    *
*      Role of the function : This fn is the Main Module of Test Routine which* 
*                             calls all the other code generating Fns.        *
*      Formal Parameters    : fp                                              *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/
void
test_routine (FILE * fp)
{

#ifdef CORRECT_MIDGEN
    UINT1              *pi1_string;

    (UINT1 *) pi1_string =
        (UINT1 *) calloc (1, sizeof (p_table_struct->entry_name));
    strcpy (pi1_string, p_table_struct->entry_name);

    if (i4_check_repeat_table)
    {
        strcat (pi1_string, itoa (p_temp_counter));
    }
    comments_for_testroutine (pi1_string, fp);
    prototype_for_testroutine (pi1_string, fp);
    free (pi1_string);
#else
    comments_for_testroutine (p_table_struct->entry_name, fp);
    prototype_for_testroutine (p_table_struct->entry_name, fp);
#endif

    fprintf (fp, "\n\n/*** DECLARATION_BEGIN ***/\n\n");
    declarations_for_testroutine (fp);
    fprintf (fp, "\n\n/*** DECLARATION_END ***/\n\n");
    test_for_allobjects (fp);

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : comments_for_test_routine                       *
*      Role of the function : This fn generates the Comments for the Test Fns *
*      Formal Parameters    : pi1_entry_name , fp                             *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/
void
comments_for_testroutine (UINT1 *pi1_entry_name, FILE * fp)
{
    fprintf (fp, "\n\n/" DOT);
    fprintf (fp, "\n Function   : %sTest", pi1_entry_name);
    fprintf (fp,
             "\n Description: This routine returns the value of the requested");
    fprintf (fp, " MIB variable.");
    fprintf (fp,
             "\n Input      : p_in_db   : The OID as formed by the SNMP Agent");
    fprintf (fp, "\n                          from the static MIB database.");
    fprintf (fp,
             "\n              p_incoming: The OID as it is send by the manager");
    fprintf (fp, "\n                          in the SNMP PDU.");
    fprintf (fp,
             "\n              u1_arg    : The position of the variable in the MIB group.");
    fprintf (fp,
             "\n              p_value   : Points to MultiDataType of the variable");
    fprintf (fp, "\n                          specified by p_incoming.");
    fprintf (fp, "\n Returns    : Returns one of the following error codes\n");
    fprintf (fp, "\n           SNMP_ERR_INCONSISTNT_NAME");
    fprintf (fp, "\n           SNMP_ERR_INCONSISTNT_VALUE");
    fprintf (fp, "\n           SNMP_ERR_WRONG_VALUE");
    fprintf (fp, "\n           SNMP_ERR_WRONG_LENGTH");
    fprintf (fp, "\n           SNMP_ERR_NOT_WRITABLE");
    fprintf (fp, "\n           SNMP_ERR_NO_CREATION");
    fprintf (fp, "\n           SNMP_ERR_WRONG_TYPE");
    fprintf (fp, "\n           SNMP_ERR_GEN_ERR");
    fprintf (fp, "\n" DOT "/ \n");
}

/******************************************************************************
*      function Name        : prototype_for_testroutine                       *
*      Role of the function : This fn generates the Prototypes for the        *
*                             Test Routines                                   *
*      Formal Parameters    : pi1_entry_name , fp                             *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/
void
prototype_for_testroutine (UINT1 *i1_entry_name, FILE * fp)
{
    fprintf (fp, "\n/* Prototype declarations for test_routine */\n");
    fprintf (fp,
             "\nINT4 %sTest (tSNMP_OID_TYPE* p_in_db , tSNMP_OID_TYPE* p_incoming,",
             i1_entry_name);
    fprintf (fp,
             "\n             UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE* p_value)");
    fprintf (fp, "\n{\n");
}                                /* End Of Function. */

/******************************************************************************
*      function Name        : declarations_for_testroutine                    *
*      Role of the function : This fn generates the code for which declares   *
*                             the vars for the TEST routines .                *
*      Formal Parameters    : fp                                              *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
declarations_for_testroutine (FILE * fp)
{
    fprintf (fp, "\n    /* Declarations for Test routine . */\n");

    /* Declaration of the Common Variables.  */
    declaration_of_common_variables (fp);

    /* 
     *  The Declaration of the Octet String Variable Which is
     *  used to extract the Value From the MULTI DATA TYPE Struct.
     */
    fprintf (fp, "\n    /*");
    fprintf (fp, "\n     *  This Variable is used to extract the Value in The");
    fprintf (fp, "\n     *  Multi Data Type which is sent by the Manager");
    fprintf (fp, "\n     */");
    fprintf (fp, "\n    UINT1 u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;");
    fprintf (fp, "\n    INT4 LEN_%s_INDEX         ;\n",
             p_table_struct->table_name);
    fprintf (fp, "\n    INT4 i4_count = FALSE;");

    /*  
     *  Fn which declares the variables  for extracting
     *  indices as per their type.
     */
    declare_all_var_to_extractindices (fp);

    /* 
     *  Declares the Value which is to be passed
     *  to the Low Level SET and TEST routines.
     */
    declaration_of_value (fp);

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : test_forall_objects                             *
*      Role of the function : This Fn generates the Test routines for all the *
*                             Object in the Table according to the permission *
*      Formal Parameters    : fp                                              *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
test_for_allobjects (FILE * fp)
{
    INT4                i4_count = ZERO;
    INT4                i4_no_access[MAX_OBJECTS];
    INT4                i4_no_access_count = FALSE;
    UINT1               i1_temp[MAX_LINE];
    UINT1              *pi1_temp;
    INT1               *pi1_type;

    fprintf (fp, "\n     i4_offset = FALSE;");
    fprintf (fp, "\n     if(p_incoming->u4_Length <= p_in_db->u4_Length) {");
    fprintf (fp, "\n        return (SNMP_ERR_WRONG_LENGTH);");
    fprintf (fp, "\n     }");
    fprintf (fp, "\n     else {");

    /*
     *  This Fn Generates the Code for Assigning Value of the Variable
     *  Len Index to the Variable(which is declared) from the Given OID.
     */
    assign_len_of_variablelen_index (fp);

    /* Added check in middle level Test Routines 
     * to find whether the correct no. of Indices 
     * are passed 
     */

    if (p_table_struct->no_of_indices != 0)
    {
        fprintf (fp,
                 "\n\n        if (p_incoming->u4_Length != (UINT4) i4_size_offset)");
        fprintf (fp, "\n        {");
        fprintf (fp, "\n            return(SNMP_ERR_WRONG_LENGTH);");
        fprintf (fp, "\n        }");
    }

    /*
     *  Allocating Memory for Octet Str type Indices which are
     *  to be passed to the Low Level Routines Get Exact Case.
     *  (Only if Octet String is Present as an Index for the Table).
     */
    alloc_mem_for_octetstring_getexact_first (fp, GET_EXACT_FLAG);

    /*  This fn Checks For Malloc Failures for Octet Strings. */
    fn_checking_for_malloc_failure_settest (fp);

    /*
     *  Allocating Memory for OID type Indices which are
     *  to be passed to the Low Level Routines Get Exact Case.
     *  (Only if Octet String is Present as an Index for the Table).
     */
    alloc_mem_for_oid_getexact_first (fp, GET_EXACT_FLAG);

    /*  This fn Checks For Malloc Failures for OID. */
    fn_checking_for_malloc_failure_oid_settest (fp);

    /* This Fn generates the code which extracts the indices.  */
    code_for_extracting_indices (fp);

    fprintf (fp, "\n     }");

    /*
     *  Calling the Low Level fn for validating the Indices
     *  which are extracted from the Oid given by the manager
     *  Only when the Table is not a Scalar Table.
     */
    if (i4_global_scalar_flag <= ZERO)
    {

        fprintf (fp,
                 "\n     /*  Low Level Routine Which Validates the Indices. */");
        /*  
         *  Commented the Validation Routine in mid.c for 
         *  Initial Row Creation.
         */
        fprintf (fp,
                 "\n     /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION \n");
        /* 
         * this is to conform to the Hungarian styled function names
         * used in low level code
         * added by soma on 07/03/98
         */
        p_table_struct->table_name[0] = toupper (p_table_struct->table_name[0]);
        fprintf (fp, "\n     if((i1_ret_val = nmhValidateIndexInstance%s(",
                 p_table_struct->table_name);
        /*
         * this is to restore the change made above
         * added by soma on 07/03/98
         */
        p_table_struct->table_name[0] = tolower (p_table_struct->table_name[0]);

        /*  Passing the Indices to Low Level Validate Index fn. */
        passing_indices_to_lowlevel_routine (fp);

        fprintf (fp, ")) != SNMP_SUCCESS) {");

        /*  Freeing the Octet String Indices. */
        free_all_octetstrings (fp, GET_EXACT_FLAG);

        /*  Freeing the OID Indices. */
        free_all_oids (fp, GET_EXACT_FLAG);

        fprintf (fp, "\n            return SNMP_ERR_INCONSISTENT_NAME;");
        fprintf (fp, "\n     } */");
    }

    /* 
     *  For loop for calling low level routines for each object
     *  according to their access premission.
     */
    fprintf (fp, "\n     switch (u1_arg)   {");
    /*
     * This variable is used to get appropriate name of the object in the gene-
     * -rated code, as a part of error check for SNMP_ERR_WRONG_TYPE.
     */
    for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects; i4_count++)
    {

        /* 
         *  This if check for the access permission and writes 
         *  to the file only the objects which can be accessed. 
         */
        if ((p_table_struct->object[i4_count].access == NO_ACCESS)
            || (p_table_struct->object[i4_count].access == READ_ONLY))
        {

            i4_no_access[i4_no_access_count++] = i4_count;
            continue;
        }
        else if ((p_table_struct->object[i4_count].access == READ_WRITE)
                 || (p_table_struct->object[i4_count].access == READ_CREATE))
        {
            /*
             *This function returns the type value for Middle level routines
             *as a part of check for SNMP_ERR_WRONG_TYPE. Memory for this is 
             *allocated inside the function.
             */
            pi1_type =
                GetStringFromType (p_table_struct->object[i4_count].type_name);

            strcpy (i1_temp, p_table_struct->object[i4_count].object_name);
            pi1_temp = strupper (i1_temp);
            fprintf (fp, "\n\n       case %s:", pi1_temp);
            fprintf (fp, "\n          {");
            fprintf (fp, "\n               if (p_value->i2_DataType != %s) {",
                     pi1_type);
            fprintf (fp, "\n                   return (SNMP_ERR_WRONG_TYPE);");
            fprintf (fp, "\n               }");
            /*
             *  This fn check for the type of the data and extract the
             *  value(on which test is performed) from MultiData type struct.
             */
            /*
             * This value is now not in use after printing onto file. So
             * removed after use
             */

            if (pi1_type != NULL)
            {
                free (pi1_type);
                pi1_type = NULL;
            }
            extract_value (fp, i4_count);

            p_table_struct->object[i4_count].object_name[0] =
                toupper (p_table_struct->object[i4_count].object_name[0]);
            fprintf (fp, "\n\n               i1_ret_val = nmhTestv2%s(",
                     p_table_struct->object[i4_count].object_name);

            /*
             *This varaiable *pu4ErrorCode is added to support check for errors
             *as NO CREATION, WRONG VALUE, INCONSISTENT VALUE, WRONG LENGTH etc.             *This variable should be returned from the low level code, which
             *reflects the maximum index/instance supported in the low level
             */

            fprintf (fp, "&u4ErrorCode , ");
            /*
             * this is to restore the change made above
             * added by soma on 07/03/98
             */
            p_table_struct->object[i4_count].object_name[0] =
                tolower (p_table_struct->object[i4_count].object_name[0]);

            /* This Fn sends the Index variable to the Low Level fn.  */
            passing_indices_to_lowlevel_routine (fp);

            /* This Fn sends the Val paras for the test rtn to the Low Lev rtns */
            pass_value_to_lowlevel_routine (fp, i4_count);

            fprintf (fp, ");\n");

            /*  Freeing All the Oids and Octet Strings. */
            free_all_octetstrings (fp, GET_FIRST_FLAG);
            free_all_oids (fp, GET_FIRST_FLAG);

            fprintf (fp, "\n               if (i1_ret_val == SNMP_SUCCESS) {");
            fprintf (fp, "\n                   return (SNMP_ERR_NO_ERROR);");
            fprintf (fp, "\n               }");
            fprintf (fp, "\n               else {");
            fprintf (fp, "\n                   switch (u4ErrorCode) {");
            fprintf (fp,
                     "\n                       case SNMP_ERR_WRONG_LENGTH : ");
            fprintf (fp,
                     "\n                       case SNMP_ERR_WRONG_VALUE : ");
            fprintf (fp,
                     "\n                       case SNMP_ERR_NO_CREATION : ");
            fprintf (fp,
                     "\n                       case SNMP_ERR_INCONSISTENT_VALUE : ");
            fprintf (fp,
                     "\n                       case SNMP_ERR_INCONSISTENT_NAME : ");
            fprintf (fp, "\n                           return (u4ErrorCode);");
            fprintf (fp, "\n                       default :  ");
            fprintf (fp,
                     "\n                           return (SNMP_ERR_WRONG_VALUE);");
            fprintf (fp, "\n                   }");
            fprintf (fp, "\n               }");

            fprintf (fp, "\n               break;");
            fprintf (fp, "\n          }");
        }                        /* End of ELSE Condition.  */

    }                            /* End of FOR loop. */

    /* 
     *  For loop for writing to the file the OBJECTS that 
     *  are not accessible & that are READ_ONLY.
     */
    fprintf (fp, "\n     /*  Read Only Variables */\n");

    for (i4_count = ZERO; i4_count < i4_no_access_count; i4_count++)
    {

        strcpy (i1_temp,
                p_table_struct->object[i4_no_access[i4_count]].object_name);
        pi1_temp = strupper (i1_temp);
        fprintf (fp, "\n     case %s :", pi1_temp);

    }

    /*  Freeing All the Oids and Octet Strings. */
    free_all_octetstrings (fp, GET_FIRST_FLAG);
    free_all_oids (fp, GET_FIRST_FLAG);

    fprintf (fp, "\n          return(SNMP_ERR_NOT_WRITABLE);\n");
    fprintf (fp, "\n     default :");

    /*  Freeing All the Oids and Octet Strings. */
    free_all_octetstrings (fp, GET_FIRST_FLAG);
    free_all_oids (fp, GET_FIRST_FLAG);

    fprintf (fp, "\n         return(SNMP_ERR_INCONSISTENT_NAME);");
    fprintf (fp, "\n   }");
    fprintf (fp, "/* End of Switch */");
    fprintf (fp, "\n} /*  THE TEST ROUTINES ARE OVER . */\n\n");

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : GetStringFromType                               *
*      Role of the function : This Fn returns string value understandable to  *
*                             the generated middle level code from the known  *
*                             data type.
*      Formal Parameters    : pDataString                                     *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/

INT1               *
GetStringFromType (INT1 *pDataString)
{
    INT1               *pi1_String;
    INT4                i4_value;

    if ((pi1_String = (char *) calloc (1, MAX_LINE)) == NULL)
    {
        exit (1);
    }
    i4_value = ValueForType (pDataString);

    switch (i4_value)
    {
        case INTEGER32:
            strcpy (pi1_String, "SNMP_DATA_TYPE_INTEGER");
            break;
        case UNSIGNED32:
            strcpy (pi1_String, "SNMP_DATA_TYPE_UNSIGNED32");
            break;
        case OCTETSTRING:
            strcpy (pi1_String, "SNMP_DATA_TYPE_OCTET_PRIM");
            break;
        case OBJECTIDENTIFIER:
            strcpy (pi1_String, "SNMP_DATA_TYPE_OBJECT_ID");
            break;
        case COUNTER32:
            strcpy (pi1_String, "SNMP_DATA_TYPE_COUNTER32");
            break;
        case COUNTER64:
            strcpy (pi1_String, "SNMP_DATA_TYPE_COUNTER64");
            break;
        case TIMETICKS:
            strcpy (pi1_String, "SNMP_DATA_TYPE_TIME_TICKS");
            break;
        case GAUGE32:
            strcpy (pi1_String, "SNMP_DATA_TYPE_GAUGE32");
            break;
        case IPADDRESS:
            strcpy (pi1_String, "SNMP_DATA_TYPE_IP_ADDR_PRIM");
            break;
        case MACADDRESS:
            strcpy (pi1_String, "SNMP_DATA_TYPE_OCTET_PRIM");
            break;
        default:
            break;
    }
    return pi1_String;
}

/******************************************************************************
*      function Name        : ValueForType                                    *
*      Role of the function : This Fn returns an unique value for a string    *
*                             whose value is a standard data type  else it    *
*                             returns -1.                                     *
*      Formal Parameters    : pDataString                                     *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/

int
ValueForType (INT1 *pDataString)
{

    INT4                i4_count = 0;
/*The last value is NULL for termination condition*/

    t_VALUE_TABLE       p_struct_map_value[] = { {"INTEGER", INTEGER32},
    {"Integer32", INTEGER32},
    {"Unsigned32", UNSIGNED32},
    {"OCTET STRING", OCTETSTRING},
    {"OBJECT IDENTIFIER", OBJECTIDENTIFIER},
    {"Counter32", COUNTER32},
    {"Counter64", COUNTER64},
    {"TimeTicks", TIMETICKS},
    {"Gauge32", GAUGE32},
    {"IpAddress", IPADDRESS},
    {"MacAddress", MACADDRESS},
    {"NULL", ZERO}
    };

    while (p_struct_map_value[i4_count].i4_value != ZERO)
    {
        if (strcmp (pDataString, p_struct_map_value[i4_count].pDataType) == 0)
            return p_struct_map_value[i4_count].i4_value;
        i4_count++;
    }
    return NEGATIVE;
}
