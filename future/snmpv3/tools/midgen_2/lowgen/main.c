/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: main.c,v 1.5 2015/04/28 14:43:31 siva Exp $
 *
 * Description: This File is the Main Fn which calls the  
 *              Parser and the Generator Module. 
 *******************************************************************/

#include "include.h"
#include "global.h"
#include <ctype.h>

#ifdef FUTURE_SNMP_WANTED

INT1                gi1LockEnable = 0;
INT1                gi1TableEnable = 0;
INT1                gi1ContextIdEnable = 0;
INT1                ProtoName[50];
INT1               *Name;
INT1                UName[50];
extern UINT1        OUT_FILE[MAX_LINE];
INT4                i4TotalCount = 0;
#endif

void                unused_variables_remover ();
INT4                variable_scanner (UINT4 pos, UINT1 *line);
void                generate_notify_oid (INT1 *input);
void                copy_notify_oid ();

/******************************************************************************
*      function Name        : main                                            *
*      Role of the function : This fn is the MAIN fn which calls the Parser & *
*                             Code Generator Modules.                         *
*      Formal Parameters    : argc , argv[]                                   *
*      Global Variables     : All Global Variable used (../inc/global.h).     *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/
INT4
main (INT4 argc, INT1 *argv[])
{
    INT4                i4_individual = FALSE;
    INT4                i4_index;
    INT4                i4_argv_count;
    INT4                i4_file_count;
    INT1               *pi1_pathname;
    INT1                i1_temp[MAX_LINE];
#ifdef FUTURE_SNMP_WANTED
    INT1                newfilename[MAX_LINE];
#endif
    /*
     * these variables are added to split the generated midlevel
     * and low level code across multiple files 
     * added by soma on 04/03/98
     */
    INT4                i4_no_of_tables = 0;
    INT4                i4_tables_per_file = 0;
    INT1               *pi1_table_list[MAX_NO_OF_GROUPS];

    /*
     * this flag is used to determine whether the middle level
     * code is to be generated for Agent or Subagent
     * added by soma on 12/03/98
     */
    INT1                i1_code_for_agent = TRUE;
    INT1                u1_parse_files[MAXINFILE][MAX_LINE];
    INT1                input_file[MAX_LINE];
    INT1                newfile[MAX_LINE];
    FILE               *outfile = NULL;
    UINT1               au1Buf[MAX_LINE];
    INT1                i1Flag = 0;

    /*
     * this variable is declared to remove dot(.) in MIB file name, if exists,
     * so that there will not be more than one dot in the name of the file
     * which contains middle level get, set and test routines
     * added by soma on 16/06/98
     */
    UINT1              *pu1_dotptr;

    /*
     * this holds the length of the group OID to be written to mbdb.h
     * file, if specified by user in command line
     * added by soma on 11/04/98
     */
    INT2                i2_group_oid_len = 0;

    /*To check whether the length of the file name is exceeding five */
    INT4                i4_len_check;
    INT1               *i1_prim_end;
    INT4                i4_prim_len;
    UINT1               command[MAX_LINE];
#ifdef FUTURE_SNMP_WANTED
    INT4                i4ProtoNameCount;
#endif

    /*  Initialize DISPLAY Flag.  */
    i4_global_lineno_flag = DISPLAY_OFF;

    /* 
     *  This Fn Processes the Options which are used in the Tool. 
     *  It Stores the Version no in the i4_ver_no and Returns the Dir Name.
     */
    i4_individual = process_command_line (argc, argv, &i4_tables_per_file,
                                          &i1_code_for_agent,
                                          &i2_group_oid_len);

    if ((i4_incr_lock_flag == TRUE) && ((i4_lock_flag == FALSE) ||
                                        (i4_unlock_flag == FALSE)))
    {
        printf
            ("\nERROR: Lock and Unlock Routines have to be specified when using -X option\n");
        exit (NEGATIVE);
    }
    if (i4_isave_flag == TRUE && i4_lock_flag == FALSE)
    {
        strcpy (ai1_lock_string, "NULL");
    }
    if (i4_isave_flag == TRUE && i4_unlock_flag == FALSE)
    {
        strcpy (ai1_unlock_string, "NULL");
    }
#ifdef FUTURE_SNMP_WANTED

    strcpy (ProtoName, argv[argc - 1]);
    Name = strtok (ProtoName, ".");
    if (FILENAME == 1)
    {
        Name = i1_file_name;
    }
    for (i4ProtoNameCount = 0; Name[i4ProtoNameCount] != '\0';
         i4ProtoNameCount++)
    {
        if (islower (Name[i4ProtoNameCount]))
        {
            UName[i4ProtoNameCount] = Name[i4ProtoNameCount] - 32;
        }
        else
        {
            UName[i4ProtoNameCount] = Name[i4ProtoNameCount];
        }
    }

#endif

    for (i4_file_count = 0, i4_argv_count = 0;
         i4_argv_count < argc; i4_argv_count++)
    {
        if (strstr (argv[i4_argv_count], ".mib") != NULL)
        {
            strcpy (u1_parse_files[i4_file_count++], argv[i4_argv_count]);
            strcpy (input_file, argv[i4_argv_count]);
        }
    }

    /* to store notification types in a file */
    strcpy (newfile, "notify.h");
    outfile = fopen (newfile, WRITE_MODE);
    printf ("Input: %s, New: %s\n", input_file, newfile);
    fp = fopen (input_file, READ_MODE);
    if (fp == NULL)
    {
        printf ("Can't open mib file");
        exit (NEGATIVE);
    }
    while (!feof (fp))
    {
        fgets (au1Buf, MAX_LINE, fp);
        if ((strstr (au1Buf, "NOTIFICATION-TYPE") != NULL) &&
            (strstr (au1Buf, "::=") == NULL))
        {
            if (i1Flag != 0)
            {
                fprintf (outfile, "%s", au1Buf);
            }
            i1Flag = 1;
        }
    }
    fclose (outfile);
    fclose (fp);

    /* To convert trap objects to ordinary objects before parsing mib, 
     * to generate oid for trap objects also.
     */
    generate_notify_oid (input_file);

    /* moving modified mib to original mib */
    /*strcpy (command, "mv output.mib ");
       strcat (command, input_file);
       system (command); */

    /*Warning to give a file with less than five characters in primary name */
    for (i4_len_check = 0; i4_len_check < i4_file_count; i4_len_check++)
    {
        i1_prim_end = strchr (u1_parse_files[i4_len_check], '.');
        /*
         * Checks whether the length of the primary name of the file exceeds
         * five characters, if so warning is printed on stdout
         */
        if (i1_prim_end != NULL)
            i4_prim_len = i1_prim_end - u1_parse_files[i4_len_check];
        else
            i4_prim_len = strlen (u1_parse_files[i4_len_check]);

        if (i4_prim_len > 5)
        {
            printf
                ("\nWarning: Names of generated files use the first six characters of input file(s)");
            printf
                ("\nIf more than one input files contain the same set of first five characters, ");
            printf ("\ngenerated files may get overwritten.");
            break;
        }
    }                            /*Warning loop ends here */

    /* 
     *  This If Condition Checks for Dir Name being
     *  the same as MIB file Name. 
     */

    if (!strcmp (i1_dir_name, argv[argc - ONE]))
    {
        printf ("\nERROR: Names of the directory and MIB file are same\n");
        exit (NEGATIVE);
    }

    /*
     * code added here to remove the restriction on the command line format
     * so that options can be specified in any order
     * added by soma on 14/04/98
     */

    /*  This Fn generates the snmp-mib.h file from premosy and postmosy. */
    /* name xmosy_postmosy changed to premosy_postmosy, by soma on 02/03/98 */

    premosy_postmosy (i4_file_count, u1_parse_files);

    /*
     *  Fn Gets The Path Name This Fn is OS Dependent When Used
     *  in another environment this Function should be changed. 
     */
    pi1_pathname = get_pathname ();
    if (pi1_pathname == NULL)
    {
        printf ("\n\tERROR : The Shell Variable PWD not Exported\n");
        exit (NEGATIVE);
    }
    strcat (pi1_pathname, "/");

    /*
     * in the following if condition half of the code was repeated in
     * both the conditions viz. TRUE and FALSE
     * common part was put outside and else condition removed
     * as there was nothing to do other than common action
     * changed by soma on 05/03/98
     */

    if (*i1_dir_name != NULL_CHAR)
    {
        make_dir (i1_dir_name);

        /* Copying the Directory Name (Specified in the -d option.) */
        /*
         * code modified to handle the situation in which user specifies
         * the directory name starting from '/' e.g. /home/midgen/temp
         * added by soma on 05/03/98
         */

        if (*i1_dir_name == '/')
        {
            strcpy (pi1_pathname, i1_dir_name);
        }
        else
        {
            strcat (pi1_pathname, i1_dir_name);
        }
        strcat (pi1_pathname, "/");
    }
    /* This Fn Creates the Dir structure COM INC OBJ and SRC.  */
    create_directory_structure (pi1_pathname);

    /* Copying the INCLUDE PATH NAME */
    strcpy (i1_temp, pi1_pathname);
    strcpy (i1_inc_path, i1_temp);

    /* Copying the SOURCE PATH NAME */
    strcpy (i1_temp, pi1_pathname);
    strcpy (i1_src_path, i1_temp);

    /* 
     *  Finding the Length of the File Name and Extracting a 
     *  File Name of 8 characters and Copying the Path.
     */
    if (strlen (argv[argc - ONE]) > FILE_NAME_LEN)
    {
        /*
         * As strncpy() doesn't place a '\0' at the end the
         * following line was added by soma on 03/03/98
         */
        *(argv[argc - ONE] + FILE_NAME_LEN) = '\0';
    }
    /*
     * remove any dots in MIB file name so that output file names will not
     * contain more than one dot.
     * code added by soma on 16/06/98
     */

    if ((pu1_dotptr = strchr (argv[argc - ONE], DOT_CHAR)) != NULL)
    {
        *pu1_dotptr = NULL_CHAR;
    }

    printf ("\n\nMidgen: Generating Middle and Low Level Code...");
    fflush (stdout);

    /* 
     *  This is the Main while Loop for calling Parser
     *  and generator Modules till the end of the file. 
     */
    /*  PARSER MODULE. */

    {
        INT4                i4_temp;
        INT1                i1_filename[MAX_LINE];
        for (i4_temp = 0; i4_temp < i4_file_count; i4_temp++)
        {
            i4TotalCount = 0;
            wrap_file_open (u1_parse_files[i4_temp], i1_inc_path, i1_src_path);
            bzero (i1_filename, MAX_LINE);
            open_files (u1_parse_files[i4_temp], i1_inc_path, i1_src_path,
                        i1_filename);
            parse (u1_parse_files[i4_temp], i1_inc_path);
            strcpy (u1_parse_files[i4_temp], i1_filename);
            if (isalpha ((INT4) u1_parse_files[i4_temp][0]) == 0)
            {
                fprintf (stderr, "File %s does not start with a letter. \n",
                         u1_parse_files[i4_temp]);
                strcpy (i1_temp, REMOVE);
                strcat (i1_temp, i1_inc_path);
                strcat (i1_temp, TEMP_FILE);
                system (i1_temp);
                exit (0);
            }
            fprintf (f_ocon, "\n#endif /*  %sOCON_H  */\n",
                     u1_parse_files[i4_temp]);
            fprintf (f_ogp, "\n#endif /*  %sOGP_H  */\n",
                     u1_parse_files[i4_temp]);
            close_files ();
            unused_variables_remover ();

            wrap_file_close (u1_parse_files[i4_temp], i1_inc_path, i1_src_path);
            strcpy (i1_temp, REMOVE_DIR);
            strcat (i1_temp, i1_src_path);
            strcat (i1_temp, "*mid.c ");
            strcat (i1_temp, i1_inc_path);
            strcat (i1_temp, "*mid.h ");
            strcat (i1_temp, i1_inc_path);
            strcat (i1_temp, "*ogi.h ");
            strcat (i1_temp, i1_inc_path);
            strcat (i1_temp, "*con.h ");
            strcat (i1_temp, i1_inc_path);
            strcat (i1_temp, "*Temp*.h ");
            system (i1_temp);
        }
    }

    /*
     * code added to write private routines in to a seperate file
     * incase output is to be split into multiple files
     * added by soma on 05/03/98
     */

    /* Part of code brought under the switch UNWANTED_CODE to prevent the 
     * generation of the include files extern.h,include.h,midconst.h and
     * midutils.h and  source file midutils.c, which are not used 
     * by the FutureSNMP.
     */
#ifdef UNWANTED_CODE

    strcpy (i1_temp, i1_src_path);
    strcat (i1_temp, "midutils.c");
    fp = fopen (i1_temp, WRITE_MODE);
    if (fp == NULL)
    {
        printf ("\n\nERROR : Can't open midutils.c file \n");
        clear_directory ();
        exit (NEGATIVE);
    }
    fprintf (fp, INCS " \"include.h\"\n\n");
    /* 
     *  Fn's which generate the Code for allocating memory
     *  for Octet String and Object Identifier.
     */
    gencode_for_alloc_oid_fn (fp);
    gencode_for_alloc_octet_fn (fp);

    /* 
     *  Fn's which generate the Code for Freeing memory
     *  for Octet String and Object Identifier.
     */
    gencode_for_free_oid_fn (fp);
    gencode_for_free_octet_fn (fp);
    fclose (fp);

    strcpy (i1_temp, i1_inc_path);
    strcat (i1_temp, "midutils.h");
    fp = fopen (i1_temp, WRITE_MODE);
    if (fp == NULL)
    {
        printf ("\n\nERROR : Can't open midutils.h Header file \n");
        clear_directory ();
        exit (NEGATIVE);
    }
    /*  This Fn Generates the Prototypes for the SNMP Form Functions. */
    prototype_FormFncts (fp);

    /*
     *   This Fn Generates the Prototypes for the Alloc and
     *   Free Fns for Octet Strings and Object Identifiers. 
     */
    prototype_alloc_free_fns (fp);
    fclose (fp);
    strcpy (i1_temp, i1_inc_path);
    strcat (i1_temp, "extern.h");
    fp = fopen (i1_temp, WRITE_MODE);
    if (fp == NULL)
    {
        printf ("\n\nERROR : Can't open extern.h Header file \n");
        clear_directory ();
        exit (NEGATIVE);
    }
    /*  This Fn Generates the extern Prototypes for the SNMP Form Functions. */
    extern_prototype_FormFncts (fp);

    /*
     *   This Fn Generates the extern Prototypes for the Alloc and
     *   Free Fns for Octet Strings and Object Identifiers. 
     */
    extern_prototype_alloc_free_fns (fp);
    fclose (fp);

    strcpy (i1_temp, i1_inc_path);
    strcat (i1_temp, "midconst.h");
    fp = fopen (i1_temp, WRITE_MODE);
    if (fp == NULL)
    {
        printf ("\n\nERROR : Can't open midconst.h Header file \n");
        clear_directory ();
        exit (NEGATIVE);
    }
    /* This Fn Declares the Constants.  */
    header_constant_declare (fp);
    fclose (fp);

#endif
#ifdef UNWANTED_CODE
    /*  Creating the Make File. */
    create_makefile (pi1_pathname, argv[argc - ONE],
                     i4_tables_per_file, pi1_table_list, i4_no_of_tables,
                     u1_parse_files, i4_file_count);

#endif

    /* free memory allocated to pi1_table_list as it's no longer needed */
    for (i4_index = 0; i4_index < i4_no_of_tables; i4_index++)
    {
        free (pi1_table_list[i4_index]);
    }

#ifdef UNWANTED_CODE
    /*  Creating the Environment File. */
    create_envfile (pi1_pathname, argv[argc - ONE]);

    /*  Creating the Include File (include.h). */
    create_includeh (i1_inc_path, argv[argc - ONE]);

#endif
    /* 
     * delete file snmp-mib.h created by FuturePostmosy as
     * it's no longer needed
     * added by soma on 06/03/98
     */

    /* Deleting the Temp Files. */
    strcpy (i1_temp, REMOVE);
    strcat (i1_temp, i1_inc_path);
    strcat (i1_temp, TEMP_FILE);
    system (i1_temp);

    printf ("Done\n");
    fflush (stdout);
#ifdef FUTURE_SNMP_WANTED

    strcpy (newfilename, i1_inc_path);
    if (FILENAME == 1)
    {
        Name = i1_file_name;
    }
    else
    {
        Name[5] = '\0';
    }
    strcat (newfilename, Name);
    strcat (newfilename, "mdb.h");
    rename (OUT_FILE, newfilename);

#endif
    strcpy (i1_temp, REMOVE_DIR);
    pi1_pathname = get_pathname ();
    strcat (i1_temp, pi1_pathname);
    strcat (i1_temp, BACK_DIR);
    strcpy (ProtoName, argv[argc - 1]);
    Name = strtok (ProtoName, ".");
    strcat (i1_temp, Name);
    strcat (i1_temp, DOTDEF);
    system (i1_temp);

    strcpy (i1_temp, REMOVE_DIR);
    pi1_pathname = get_pathname ();
    strcat (i1_temp, pi1_pathname);
    strcat (i1_temp, BACK_DIR);
    strcpy (ProtoName, argv[argc - 1]);
    Name = strtok (ProtoName, ".");
    strcat (i1_temp, Name);
    strcat (i1_temp, DOTH);
    system (i1_temp);

    copy_notify_oid ();
    exit (ZERO);
    return 0;
}                                /*  THE MAIN FUNCTION GETS OVER */

/******************************************************************************
*      function Name        : create_directory_structure                      *
*      Role of the function : This fn creates the Directory Structure in the  *
*                             path given in the string (pi1_name).            *
*      Formal Parameters    : f                                               *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
create_directory_structure (UINT1 *pi1_name)
{
    UINT1               i1_temp[MAX_LINE];

    /* Part of code brought under switch UNWANTED_CODE to prevent the
     * the generation of the unwanted directories com & obj, which 
     * are not used by FutureSNMP.
     */

#ifdef UNWANTED_CODE

    strcpy (i1_temp, pi1_name);
    strcat (i1_temp, "com");
    make_dir (i1_temp);

    strcpy (i1_temp, pi1_name);
    strcat (i1_temp, "obj");
    make_dir (i1_temp);

#endif
    strcpy (i1_temp, pi1_name);
    make_dir (i1_temp);
}                                /* End Of Function. */

/******************************************************************************
*      function Name        : mem_allocate                                    *
*      Role of the function : This fn allocate memory for the Global Data     *
*                             structure (i.e. p_table_struct).                *
*      Formal Parameters    : f                                               *
*      Global Variables     : p_table_struct                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
mem_allocate (void)
{
    p_table_struct = (t_MIB_TABLE *) calloc (1, sizeof (t_MIB_TABLE));

    if (p_table_struct == NULL)
    {
        printf ("\n\n\tERROR :  Malloc Failure.\n");
        clear_directory ();
        exit (NEGATIVE);
    }

}                                /*  End Of Function. */

/******************************************************************************
*      function Name        : validate_command_line                           *
*      Role of the function : This fn Validates the Command Line Arguments    *
*                            the user.                                        *
*      Formal Parameters    : argc , argv[]                                   *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None                                            *
******************************************************************************/
void
validate_command_line ()
{

    /* 
     * Usage message and if condition are modified by soma on 02/03/98 
     */

    printf ("\nUsage:");
    printf ("\n midgen  [options] <mib_files> \n");
    printf ("\nOptions:\n");
    printf ("\n   -m <GetContext String> <ReleaseContext String> <MI String>");
    printf ("\n                usage is optional; In case the mib is for");
    printf ("\n                Multi Instance, it sets i4_mi_flag as True\n");
    printf ("\n     -d<dirname>  specifies the directory in which");
    printf ("\n                  the directory structure is to be created\n");
    printf ("\n     -f<filename> specifies the name in which the files should");
    printf ("\n                  be generated\n");
    printf ("\n      -l generates Registration Function with Lock(NULL)"
            " function Pointers\n");
    printf ("\n   -c generates Registration Function with Lock(NULL)");
    printf ("\n      function Pointers and ContextId (NULL)"
            " function pointers\n");
    printf
        ("\n   -x generates nmhSetCmn routines for sending MSR Trigger from CLI\n");
    printf
        ("\n   -X generates nmhSetCmnWithLock routines for sending MSR Trigger from CLI");
    printf
        ("\n      (Lock and Unlock routines are mandatory for this option)\n");
    printf
        ("\n   -e generates external declarations of the read-write objects");
    printf
        ("\n   -L <cli_lock_fun> generates new nmhSetCmn with lock function\n");
    printf
        ("\n   -U <cli_unlock_fun> generates new nmhSetCmn with Unlock function");
    printf ("\n   -t  registers objects in the mib table wise");
    printf ("\n");
    exit (NEGATIVE);

}                                /* End Of Function . */

/******************************************************************************
*      function Name        : get_pathname                                    *
*      Role of the function : This fn gets the Present Working Directory.     *
*      Formal Parameters    : None                                            *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : pwd.                                            *
******************************************************************************/
UINT1              *
get_pathname (void)
{
    return (UINT1 *) getenv ("PWD");
}

/******************************************************************************
*      function Name        : make_dir                                        *
*      Role of the function : This fn creates the Directory only when it is   *
*                             if it is not Present.                           *
*      Formal Parameters    : i1_name                                         *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : TRUE (when Dir Created) Else NEGATIVE.          *
******************************************************************************/
INT4
make_dir (UINT1 *i1_name)
{
    INT4                i4_flag;

    i4_flag = mkdir (i1_name, MKDIR_FLAG);
    return TRUE;

}                                /*  End Of Function. */

/******************************************************************************
*      function Name        : premosy_postmosy                                *
*      Role of the function : This fn creates the snmp-mib.h file in the      *
*                             current directory.                              * 
*      Formal Parameters    : argc , argv[]                                   *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
premosy_postmosy (INT4 argc, INT1 argv[][MAX_LINE])
{
    INT4                i4_sysflag = ZERO;
    INT1                i1_temp[MAX_LINE];
    INT4                i4_temp;
    /* 
     * V2Comp doesn't write unnecessary details to terminal
     * It displays warnings and errors besides asking user response to
     * overwrite output file, if already exists. It also opens input MIB
     * file in case there is an error and places the cursor on the line which
     * caused error. If user doesn't specify '-l' option in command line,
     * the output from V2Comp is redirected to a file and hence user
     * will not be able to see any thing and may feel that tool entered
     * infinite loop. There are two scenarios to be considered:
     * 1. If .def file already exists then tool asks whether to overwrite.
     * If this has been redirected to a file, user doesn't know it and waits 
     * for completion of the process without entering response.
     * 2.If there is an error in input MIB V2Comp displays an error
     * message and opens the input MIB and places the cursor on the line of
     * error. Again if this has been redirected to a file, user doesn't know
     * it and waits for completion of the process without exiting editor.
     * For all the reasons above mentioned redirection of output of
     * FuturePostmosy is removed irrespective of '-l' option.
     * changed by soma on 07/03/98
     */
    strcpy (i1_temp, V2COMP);
    for (i4_temp = 0; i4_temp < argc; i4_temp++)
    {
        if (strstr (argv[i4_temp], ".mib") != NULL)
        {
            strcat (i1_temp, argv[i4_temp]);
            strcat (i1_temp, " ");
        }
    }
    printf ("\nCompiling mibs ...\n");
    fflush (stdout);
    i4_sysflag = system (i1_temp);
    if (i4_sysflag != ZERO)
    {
        printf ("\nERROR: Unable to create .def File From V2Compiler\n");
        exit (NEGATIVE);
    }

    /*
     * condition is necessary since some times smi.def may not be there
     * and root information is present in input MIB itself. If this condition
     * not checked postmosy will not be able to open smi.def in the above
     * mentioned case
     * added by soma on 07/03/98
     */
    /*
     * .def file and snmp-mib.h are generated by V2Comp and 
     * FuturePostmosy as a part of middle level code generation.
     * So these two file have to be deleted after using them.
     * As snmp-mib.h is to be used at later point it's removed just
     * before exiting middle level generator.
     * added by soma on 03/03/98
     */

    strcpy (i1_temp, argv[argc - ONE]);
    strcat (i1_temp, DOTDEF);
    unlink (i1_temp);
}                                /*  End Of Function. */

/******************************************************************************
*      function Name        : process_command_line                            *
*      Role of the function : This fn Processes the Command  Args and Gives   *
*                             the Error if Present Else it stores the Dir     *
*                             Name in the Variable i1_dir_name & returns      *
*                             the Version Number                              *
*      Formal Parameters    : argc , argv[] , pi4_nof_tables,                 *
*                             pi1_code_for_agent, pi2_goid_len                *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : Version number                                  *
******************************************************************************/
INT4
process_command_line (INT4 argc, INT1 *argv[], INT4 *pi4_nof_tables,
                      INT1 *pi1_code_for_agent, INT2 *pi2_goid_len)
{
    INT1                i1_option;
    INT4                i4_ver_no = ZERO;
    INT4                i4_dircount = ZERO;
    INT4                i4_filecount = ZERO;

    while ((i1_option = getopt (argc, argv, OPTION_STRING)) != NEGATIVE)
    {
        switch (i1_option)
        {
            case DIRECTORY_OPTION:
            {
                if (i4_dircount > ZERO)
                {
                    printf ("\nERROR: -d option specified more than once\n");
                    exit (NEGATIVE);
                }
                i4_dircount++;

                /* 
                 * if condition modified as it did not allow '-' to
                 * exist in a directory name, which is valid in UNIX
                 * However, it doesn't allow the directory name to
                 * start with '-'
                 * changed by soma on 02/03/98
                 */

                if (strstr (optarg, MINUS) == optarg)
                {
                    printf ("\nERROR: Directory name not given.\n");
                    exit (NEGATIVE);
                }

                /* Copying the Directory Name into global variable */
                strcpy (i1_dir_name, optarg);
                break;
            }
            case LOCK_OPTION:
                gi1LockEnable = 1;
                break;

                /* For passing call back functions to get contextid 
                 * from the snmp session.
                 */

            case CONTEXTID_OPTION:
                gi1LockEnable = 1;
                gi1ContextIdEnable = 1;
                break;

            case MULTIPLE_INSTANCE:
            {
                i4_mi_flag = TRUE;
                pass_user_strings (argc, argv);
                break;
            }

                /* Optional flag for incremental save option */
            case INCREMENTAL_SAVE:
            {
                i4_isave_flag = TRUE;
                break;
            }

                /* Optional flag for lock function */
            case LOCK_FUNC:
            {
                i4_lock_flag = TRUE;
                parse_lock_function (argc, argv);
                break;
            }

                /* Optional flag for unlock function */
            case UNLOCK_FUNC:
            {
                i4_unlock_flag = TRUE;
                parse_unlock_function (argc, argv);
                break;
            }

                /* Optional flag for external declaration */
            case EXTERNAL_DECLARATION:
            {
                i4_isave_flag = TRUE;
                i4_extern_flag = TRUE;
                break;
            }

                /* Optional flag for incremental save mapping 
                 * with mandatory lock and unlock routines */
            case MAPPING_LOCK_UNLOCK:
            {
                i4_isave_flag = TRUE;
                i4_incr_lock_flag = TRUE;
                break;
            }

                /* 
                 * added the below case for providing -f option,
                 * with which the user can specify the name in which
                 * the files should be generated.
                 */
            case TABLE_OPTION:
                gi1TableEnable = 1;
                break;

            case FILENAME_OPTION:
            {
                if (i4_filecount > ZERO)
                {
                    printf ("\nERROR: -f option specified more than once\n");
                    validate_command_line ();
                    exit (NEGATIVE);
                }
                i4_filecount++;

                if (strstr (optarg, MINUS) == optarg)
                {
                    printf ("\nERROR: File name not given.\n");
                    validate_command_line ();
                    exit (NEGATIVE);
                }

                /* Copying the FIle Name into global variable */
                strcpy (i1_file_name, optarg);
                if (isdigit (i1_file_name[0]) != 0)
                {
                    printf
                        ("\nERROR: File name should not start with number.\n");
                    validate_command_line ();
                    exit (NEGATIVE);
                }
                FILENAME = 1;
                break;
            }
            default:
                validate_command_line ();
                exit (NEGATIVE);

        }                        /* End Of Switch. */
    }                            /* End Of While Loop */

    /*  Checking For the Presence of the MIB File.  */
    if (argv[optind] == NULL)
    {
        validate_command_line ();
    }

    /*  Returning the Name of the Directory.  */
    return i4_ver_no;

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : clear_directory                                 *
*      Role of the function : This fn Removes the Directory Strucutre when    *
*                             an Exit Due to Failure is attained.             *
*      Formal Parameters    : None                                            *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
clear_directory (void)
{
    UINT1               i1_temp[MAX_LINE];

    printf ("\n Deleting the Directory  %s :\n", i1_dir_name);

    strcpy (i1_temp, REMOVE_DIR);
    strcat (i1_temp, i1_dir_name);
    system (i1_temp);
    system ("rm -rf wrap.c wrap.h");
}                                /*  End Of Function. */

/******************************************************************************
*      function Name        : open_files                                      *
*      Role of the function : This fn open the neccesary files to write       *
*                             Midgen code and header files required.          *
*      Formal Parameters    :                                                 *
*      Global Variables     : FILE pointers.                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
open_files (INT1 *p1_in_file, INT1 *p1_inc_path, INT1 *p1_src_path,
            INT1 *p1_file)
{
    INT1                i1_filename[MAX_LINE], i1_temp[MAX_LINE];
    INT1               *p1_temp, *p1_tmp;
    bzero (i1_filename, MAX_LINE);
    bzero (i1_temp, MAX_LINE);
    strcpy (i1_filename, p1_in_file);
    p1_temp = strrchr (i1_filename, '/');
    if (p1_temp != NULL)
    {
        p1_temp++;
    }
    else
    {
        p1_temp = i1_filename;
    }
    p1_tmp = strchr (p1_temp, '.');
    if (p1_tmp != NULL)
    {
        *p1_tmp = '\0';
    }

    strcpy (p1_file, p1_temp);
    strcpy (i1_temp, p1_src_path);
    /*
     *This is added to reduce the length of the file name to five to bring
     *the over all length of the reduced file name to eight
     */
    p1_temp[6] = '\0';

    if (FILENAME == 1)
    {
        p1_temp = i1_file_name;
    }

    strcat (i1_temp, p1_temp);

    strcpy (i1_mid_temp_file, i1_temp);
    strcpy (i1_mid_final_file, i1_temp);

    strcat (i1_temp, MID_FILE_EXTN_C);
    strcat (i1_mid_temp_file, MID_FILE_EXTN_C);
    strcat (i1_mid_final_file, MID_FILE_FINAL_C);

    fp = fopen (i1_temp, WRITE_MODE);
    if (fp == NULL)
    {
        printf ("\n\nERROR : Can't Open Middle Level 'C' file %s \n", i1_temp);
        clear_directory ();
        exit (NEGATIVE);
    }
    fprintf (fp, INCS " \"include.h\"\n");
    fprintf (fp, INCS " \"%smid.h\"\n", p1_temp);
    fprintf (fp, INCS " \"%slow.h\"\n", p1_temp);
    fprintf (fp, INCS " \"%scon.h\"\n", p1_temp);
    fprintf (fp, INCS " \"%sogi.h\"\n", p1_temp);
    fprintf (fp, INCS " \"extern.h\"\n");
    fprintf (fp, INCS " \"midconst.h\"\n\n");
    strcpy (i1_temp, p1_src_path);
    strcat (i1_temp, p1_temp);
    strcat (i1_temp, LOW_FILE_EXTN_C);

    f_low_c = fopen (i1_temp, WRITE_MODE);
    if (f_low_c == NULL)
    {
        printf ("\n\nERROR : Can't Open Middle Level 'C' file %s \n", i1_temp);
        clear_directory ();
        exit (NEGATIVE);
    }
    fprintf (f_low_c, "/***************************************************"
             "*****************\n");
    fprintf (f_low_c,
             "* Copyright (C) 2006 Aricent Inc . All Rights Reserved\n*\n");
    fprintf (f_low_c,
             "* $Id: main.c,v 1.5 2015/04/28 14:43:31 siva Exp $\n*\n");
    fprintf (f_low_c, "* Description: Protocol Low Level Routines\n");
    fprintf (f_low_c, "***************************************************"
             "******************/\n");
    fprintf (f_low_c, INCS " \"lr.h\" \n");
    fprintf (f_low_c, INCS " \"fssnmp.h\" \n");
    fprintf (f_low_c, INCS " \"%slw.h\"\n", p1_temp);
    strcpy (i1_temp, p1_inc_path);
    strcat (i1_temp, p1_temp);
    strcat (i1_temp, MID_FILE_EXTN_H);

    f_mid_h = fopen (i1_temp, WRITE_MODE);
    if (f_mid_h == NULL)
    {
        printf ("\n\nERROR : Can't Open Middle Level 'H' file %s \n", i1_temp);
        clear_directory ();
        exit (NEGATIVE);
    }
    strcpy (i1_temp, p1_inc_path);
    strcat (i1_temp, p1_temp);
    strcat (i1_temp, LOW_FILE_EXTN_H);

    f_low_h = fopen (i1_temp, WRITE_MODE);
    if (f_low_h == NULL)
    {
        printf ("\n\nERROR : Can't Open Low Level 'H' file %s \n", i1_temp);
        clear_directory ();
        exit (NEGATIVE);
    }

    strcpy (i1_temp, p1_inc_path);
    strcat (i1_temp, p1_temp);
    strcat (i1_temp, OCON_EXTN_H);

    f_ocon = fopen (i1_temp, WRITE_MODE);
    if (f_ocon == NULL)
    {
        printf ("\n\nERROR : Can't open Header file containing constants\n");
        clear_directory ();
        exit (NEGATIVE);
    }
    fprintf (f_ocon, "\n# ifndef %sOCON_H", p1_temp);
    fprintf (f_ocon, "\n# define %sOCON_H", p1_temp);

    strcpy (i1_temp, p1_inc_path);
    strcat (i1_temp, p1_temp);
    strcat (i1_temp, OGP_EXTN_H);

    f_ogp = fopen (i1_temp, WRITE_MODE);
    if (f_ogp == NULL)
    {
        printf ("\n\nERROR : Can't open OCON.H Header file \n");
        clear_directory ();
        exit (NEGATIVE);
    }
    fprintf (f_ogp, "\n# ifndef %sOGP_H", p1_temp);
    fprintf (f_ogp, "\n# define %sOGP_H", p1_temp);
    fprintf (f_ogp,
             "\n\n /* The Definitions of the OGP Index Constants.  */\n\n");
}

/******************************************************************************
*      function Name        : close_files                                     *
*      Role of the function : This fn closes all files, previously            *
*                             opened.                                         *
*      Formal Parameters    :                                                 *
*      Global Variables     : FILE pointers.                                  *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
close_files ()
{
    fclose (fp);
    fclose (f_low_c);
    fclose (f_mid_h);
    fclose (f_low_h);
    fclose (f_ocon);
    fclose (f_ogp);

}

void
unused_variables_remover ()
{

    UINT1               au1_line[MAX_LINE];
    UINT4               u4_pos = 0;
    UINT2               u2_check = 0;

    f_mid_temp = fopen (i1_mid_temp_file, READ_MODE);
    f_mid_final = fopen (i1_mid_final_file, WRITE_MODE);

    while (fgets (au1_line, 255, f_mid_temp) != NULL)
    {

        if (strstr (au1_line, "DECLARATION_END") != NULL)
            u2_check = 0;
        if (strstr (au1_line, "DECLARATION_BEGIN") != NULL)
            u2_check = 1;
        u4_pos = ftell (f_mid_temp);

        if ((strstr (au1_line, "/*") != NULL)
            || (strstr (au1_line, "*/") != NULL)
            || (strstr (au1_line, ";") == NULL))
        {
            fputs (au1_line, f_mid_final);
            continue;
        }

        if ((u2_check == 0)
            || (variable_scanner (u4_pos, au1_line) == SCANNER_SUCCESS))
            fputs (au1_line, f_mid_final);
    }

    fclose (f_mid_temp);
    fclose (f_mid_final);
    unlink (i1_mid_temp_file);
}

INT4
variable_scanner (UINT4 pos, UINT1 *line)
{
    FILE               *f_scan;
    UINT1               au1_variable[MAX_LINE];
    UINT1               au1_line[MAX_LINE];
    UINT1               u1_temp;
    UINT2               u2_var_check = 0;
    UINT2               u2_cnt;
    UINT2               u2_var_cnt = 0;
    UINT2               u2_funt_cnt = 1;
    UINT4               u4_read;
    INT4                i4_retVal;
    UINT1              *pu1_pCheck;
    UINT1              *pu1_temp1;
    UINT1              *pu1_temp2;

    for (u2_cnt = 0; (u1_temp = *(line + u2_cnt)) != '\0'; u2_cnt++)
    {
        switch (u2_var_check)
        {
            case 0:
                if ((u1_temp != ' ') && (u1_temp != '\t'))
                    u2_var_check = 1;
                break;

            case 1:
                if ((u1_temp == ' ') || (u1_temp == '\t'))
                    u2_var_check = 2;
                break;

            case 2:
                if ((u1_temp != ' ') && (u1_temp != '\t') && (u1_temp != '*'))
                {
                    u2_var_check = 3;
                    u2_cnt--;
                }
                break;

            default:
                if ((u1_temp == '=')
                    || (u1_temp == ' ')
                    || (u1_temp == '\t')
                    || (u1_temp == '[') || (u1_temp == ';'))
                {
                    au1_variable[u2_var_cnt] = '\0';
                    u2_var_cnt++;
                    break;
                }
                else
                {
                    au1_variable[u2_var_cnt] = u1_temp;
                    u2_var_cnt++;
                }
        }
        if ((u2_var_cnt > 0) && (au1_variable[u2_var_cnt - 1] == '\0'))
            break;
    }
 /*** End Of For Loop ***/

    f_scan = fopen (i1_mid_temp_file, READ_MODE);
    fseek (f_scan, pos, SEEK_SET);
    i4_retVal = SCANNER_FAILURE;

    while ((u4_read = ((INT4 *) fgets (au1_line, 255, f_scan)) != NULL))
    {
        if (strstr (au1_line, "{") != NULL)
            u2_funt_cnt++;

        if (strstr (au1_line, "}") != NULL)
            u2_funt_cnt--;

        if ((pu1_pCheck = strstr (au1_line, au1_variable)) != NULL)
        {
            pu1_temp1 = pu1_pCheck;
            pu1_pCheck--;
            pu1_temp2 = pu1_temp1 + u2_var_cnt - 1;
            if (((pu1_temp1 == au1_line)
                 || ((!((*pu1_pCheck >= 'a') && (*pu1_pCheck <= 'z')))
                     && (!((*pu1_pCheck >= 'A') && (*pu1_pCheck <= 'Z')))))
                && ((!((*pu1_temp2 >= 'a') && (*pu1_temp2 <= 'z')))
                    && (!((*pu1_temp2 >= 'A') && (*pu1_temp2 <= 'Z')))))
            {
                i4_retVal = SCANNER_SUCCESS;
                break;
            }                    /* Variable found */
        }

        if (u2_funt_cnt == 0)
        {
            i4_retVal = SCANNER_FAILURE;
            break;
        }                        /* Function hits end */

    }                            /* repeat read till end of file */

    fclose (f_scan);            /* Close the File that's opened in Read Mode */

    return (i4_retVal);
}

/******************************************************************************
*      function Name        : pass_user_strings                               *
*      Role of the function : This fn passes the user defined GetContext,     *
*                             ReleaseContext, MIstring form command line      *
*      Formal Parameters    : argc, argv                                      *
*      Global Variables     :                                                 *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
pass_user_strings (INT4 argc, INT1 *argv[])
{
    INT4                i4_arg_count;

    for (i4_arg_count = 0; i4_arg_count < argc; i4_arg_count++)
    {
        if (argv[i4_arg_count][0] == '-' && argv[i4_arg_count][1] == 'm')
        {
            /* Since the function does not start with MINUS */
            i4_arg_count++;
            if (argv[i4_arg_count][0] == '-')
            {
                printf ("\nGetContest String is not given\n");
                validate_command_line ();
                exit (NEGATIVE);
            }
            strcpy (ai1_get_context, argv[i4_arg_count]);

            i4_arg_count++;
            if (argv[i4_arg_count][0] == '-')
            {
                printf ("\nRelease Contest" "String is not given\n");
                validate_command_line ();
                exit (NEGATIVE);
            }
            strcpy (ai1_release_context, argv[i4_arg_count]);

            i4_arg_count++;
            if ((argv[i4_arg_count][0] == '-') ||
                (strstr (argv[i4_arg_count], ".mib") != NULL))
            {
                printf ("\n Multiple Instance" "String is not given\n");
                validate_command_line ();
                exit (NEGATIVE);
            }
            strcpy (ai1_mi_string, argv[i4_arg_count]);
            break;
        }
    }
}

/******************************************************************************
*      function Name        : parse_lock_function                             *
*      Role of the function : This fn passes the user defined lock function   *
*      Formal Parameters    : argc, argv                                      *
*      Global Variables     :                                                 *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
parse_lock_function (INT4 argc, INT1 *argv[])
{
    INT4                i4_arg_count;

    for (i4_arg_count = 0; i4_arg_count < argc; i4_arg_count++)
    {
        if (argv[i4_arg_count][0] == '-' && argv[i4_arg_count][1] == 'L')
        {
            i4_arg_count++;
            if (argv[i4_arg_count][0] == '-')
            {
                printf ("\nLock function is not given\n");
                validate_command_line ();
                exit (NEGATIVE);
            }
            strcpy (ai1_lock_string, argv[i4_arg_count]);
            break;
        }
    }
}

/******************************************************************************
*      function Name        : parse_unlock_function                           *
*      Role of the function : This fn passes the user defined unlock function *
*      Formal Parameters    : argc, argv                                      *
*      Global Variables     :                                                 *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
parse_unlock_function (INT4 argc, INT1 *argv[])
{
    INT4                i4_arg_count;

    for (i4_arg_count = 0; i4_arg_count < argc; i4_arg_count++)
    {
        if (argv[i4_arg_count][0] == '-' && argv[i4_arg_count][1] == 'U')
        {
            i4_arg_count++;
            if (argv[i4_arg_count][0] == '-')
            {
                printf ("\nUnLock function is not given\n");
                validate_command_line ();
                exit (NEGATIVE);
            }
            strcpy (ai1_unlock_string, argv[i4_arg_count]);
            break;
        }
    }
}

/******************************************************************************
 *  Function Name        : generate_notify_oid
 *  Role of the function : This fn reads the mib and replaces all notification
 *                        types to object types and overwrites the newfile with 
 *                        original mib file
 *  Formal Parameters    : input - holds the mib name
 *****************************************************************************/

void
generate_notify_oid (INT1 *input)
{
    UINT1               au1Buffer[256];
    FILE               *fp = NULL;
    FILE               *new = NULL;
    UINT1               au1NewFile[64];
    UINT1               u1Flag = 0;
    UINT1              *result;
    UINT1               c;

    memset (au1Buffer, 0, 256);
    strcpy (au1NewFile, "out.mib");
    result = (UINT1 *) malloc (256);

    fp = fopen (input, READ_MODE);
    if (fp == NULL)
    {
        printf ("\nError in opening mib file\n");
        exit (NEGATIVE);
    }

    new = fopen (au1NewFile, WRITE_MODE);
    if (new == NULL)
    {
        printf ("\nError in opening new file\n");
        exit (NEGATIVE);
    }

    while (!feof (fp))
    {
        fgets (au1Buffer, 256, fp);
        if ((strstr (au1Buffer, "--") != NULL) &&
            (strstr (au1Buffer, "NOTIFICATION-TYPE") != NULL))
        {
            continue;
        }
        result = strstr (au1Buffer, "NOTIFICATION-TYPE");
        if (result != NULL)
        {
            if (u1Flag != 0)
            {
                if (strstr (result, "::=") == NULL)
                {
                    strcpy (result, "OBJECT-TYPE");
                    fprintf (new, "%s", au1Buffer);
                    fprintf (new, "\n    SYNTAX        INTEGER\n");
                    fprintf (new, "     MAX-ACCESS    read-write\n");
                    memset (au1Buffer, '\0', 256);
                    fgets (au1Buffer, 256, fp);
                    if (strstr (au1Buffer, "OBJECTS") != NULL)
                    {
                        while (strstr (au1Buffer, "STATUS") == NULL)
                        {
                            memset (au1Buffer, '\0', 256);
                            fgets (au1Buffer, 256, fp);
                            while (strcmp (au1Buffer, "END") == 0)
                            {
                                break;
                            }
                        }
                    }
                }
            }
            u1Flag = 1;
        }
        fprintf (new, "%s", au1Buffer);
        memset (au1Buffer, 0, 256);
    }
    free (result);
    fclose (fp);
    fclose (new);
    system ("./v2comp out.mib");
}

/*******************************************************************************
 *  Function Name        : copy_notify_oid
 *  Role of the function : This function copies the notification objects and OID 
 *                         type from the temporary file into notif.h
 ********************************************************************************/

void
copy_notify_oid ()
{
    FILE               *in, *out, *noti;
    struct stat         buf;
    UINT1               au1Name[50];
    UINT1               au1Dummy[20];
    UINT1               au1NotifLine[256];
    UINT1               au1WriteName[80];
    UINT1               au1WriteOid[80];

    noti = fopen ("notify.h", READ_MODE);
    if (noti == NULL)
    {
        printf ("\nError in opening  notify.h file\n");
        exit (NEGATIVE);
    }
    if (stat ("notif.h", &buf) == 0)
    {
        out = fopen ("notif.h", APPEND_MODE);
    }
    else
    {
        out = fopen ("notif.h", WRITE_MODE);
        if (out == NULL)
        {
            printf ("\nError in opening notif.h file\n");
            exit (NEGATIVE);
        }
    }
    while (!feof (noti))
    {
        memset (au1Name, 0, 50);
        memset (au1Dummy, 0, 20);
        fscanf (noti, "%s %s", au1Name, au1Dummy);
        in = fopen ("out.h", READ_MODE);
        if (strlen (au1Name) <= 1)
        {
            break;
        }
        if (in == NULL)
        {
            printf ("\nError in opening output.mib\n");
            exit (NEGATIVE);
        }
        while (!feof (in))
        {
            memset (au1NotifLine, 0, 256);
            fgets (au1NotifLine, 256, in);
            if (strstr (au1NotifLine, au1Name) != NULL)
            {
                memset (au1WriteName, 0, 80);
                memset (au1WriteOid, 0, 80);
                memset (au1Dummy, 0, sizeof (au1Dummy));
                fprintf (out, "{");
                fprintf (out, "{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},");
                fprintf (out, "{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},");
                sscanf (au1NotifLine, "%s %s,", au1WriteName, au1WriteOid);
                strcpy (au1Dummy, au1WriteOid);
                memset (au1WriteOid, 0, sizeof (au1WriteOid));
                strncpy (au1WriteOid, au1Dummy, strlen (au1Dummy) - 1);
                fprintf (out, "%s %s},\n\n", au1WriteName, au1WriteOid);
                break;
            }
        }
        memset (au1NotifLine, 0, 256);
        fclose (in);
    }
    fclose (out);
    fclose (noti);
    system ("rm out*");
    system ("rm notify.h");
}
