/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: low_proto.c,v 1.4 2015/04/28 14:43:31 siva Exp $
 *
 * Description: This File Contains the function Which     
 *              calls all the routines which generate
 *              the prototypes for low level routines.
 *******************************************************************/

# include "include.h"
# include "extern.h"

/******************************************************************************
*      function Name        : low_level_proto_generator                       *
*      Role of the function : This fn calls all the fns needed for the Low    *
*                             Level prototypes Generation.                    *
*      Formal Parameters    : f                                               *
*      Global Variables     : i4_global_scalar_flag , p_table_struct          *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
low_level_proto_generator (FILE * f)
{
    INT4                i4_readonly_flag;
    if (i4_global_scalar_flag == FALSE)
    {
        low_proto_getfirst_next (f);
    }
    low_proto_get_all_objects (f);

    /* Checks whether all the Objects are READ_ONLY.  */
    i4_readonly_flag = check_readonly ();

    if (i4_readonly_flag == TRUE)
    {
        low_proto_set_all_objects (f);
        low_proto_test_all_objects (f);
        low_proto_dep_all_objects (f);
    }

}                                /*  End Of Function . */

/******************************************************************************
*      function Name        : low_proto_getfirst_next                         *
*      Role of the function : This fn Generates the ProtoTypes for the Low    *
*                             Level GET_FIRST and GET_NEXT routines .         *
*      Formal Parameters    : f                                               *
*      Global Variables     : p_table_struct.                                 *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
low_proto_getfirst_next (FILE * f)
{
    INT4                i4_counter;
    INT4                i4_count;

    if (p_table_struct->status == STATUS_OBSOLETE)
    {
        return;
    }

    /*  GET_EXACT Validate Index Instance Routine.   */
    fprintf (f, "\n/* Proto Validate Index Instance for %s. */",
             p_table_struct->table_name);
    fprintf (f, "\nINT1");
    fprintf (f, "\nnmhValidateIndexInstance%s ARG_LIST((",
             p_table_struct->table_name);

    /* This Fn passes all the indices types to the Low level Get first fn.  */
    passing_indextype_to_lowlevelproto (f);

    fprintf (f, "));\n");

    /*  GET_FIRST Routine.   */
    fprintf (f, "\n/* Proto Type for Low Level GET FIRST fn for %s  */\n",
             p_table_struct->table_name);
    fprintf (f, "\nINT1");
    fprintf (f, "\nnmhGetFirstIndex%s ARG_LIST((", p_table_struct->table_name);

    /* This Fn passes all the indices types to the Low level Get first fn.  */
    passing_indextype_to_lowlevelproto_getfirst (f);

    fprintf (f, "));\n");

    /*  GET_NEXT Routine.  */
    fprintf (f, "\n/* Proto type for GET_NEXT Routine.  */\n");
    fprintf (f, "\nINT1");
    fprintf (f, "\nnmhGetNextIndex%s ARG_LIST((", p_table_struct->table_name);

    /*
     *  This FOR Loop passes all the INDICES and the NEXT 
     *  Values to the Low level Get Next function. 
     */
    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {

        if (i4_counter > ZERO)
        {
            fprintf (f, " , ");
        }
        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            /* 
             *  This Fn writes to the File the TYPE of the INDEX
             *  (passed as Value) to be passed to the Low Level Routine.
             */
            passing_typeto_lowlev_skeletolcode (f, i4_count);

            /* 
             *  This Fn writes to the File the TYPE of the NEXT INDEX
             *  (passed as Pointer) to be passed to the Low Level Routine.
             */
            fprintf (f, ", ");
            passing_typeto_lowlev_skeletolcode_getfirst (f, i4_count);

            /* Breaking the Loop If Index Found */
            break;
        }                        /* End Of FOR Loop. */

    }                            /* End Of Main FOR Loop For Each Index. */

    fprintf (f, "));\n");

}                                /*  End Of Function. */

/******************************************************************************
*      function Name        : low_proto_get_all_objects                       *
*      Role of the function : This fn Generates the ProtoTypes for the Low    *
*                             Level GET rtns for all the Objects in a Table . *
*      Formal Parameters    : f                                               *
*      Global Variables     : p_table_struct.                                 *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
low_proto_get_all_objects (FILE * f)
{
    INT4                i4_count;
    INT4                i4_index_flag = NOT_INDEX;
    INT1                i1_first_obj_flag = 0;

    if (p_table_struct->status == STATUS_OBSOLETE)
    {
        return;
    }

    /*  Main For Loop for All the Objects.  */
    for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects; i4_count++)
    {
        if ((p_table_struct->object[i4_count].access == READ_ONLY)
            || (p_table_struct->object[i4_count].access == READ_CREATE)
            || (p_table_struct->object[i4_count].access == READ_WRITE))
        {
            /*  Checking whether the Object is an Index.  */
            i4_index_flag =
                find_object_is_index (p_table_struct->object[i4_count].
                                      object_name);

            /*  
             *  If the Object is an Index then the Low Level 
             *  Get Routines are not Needed.
             */
            if ((i4_index_flag == IS_INDEX)
                || (p_table_struct->object[i4_count].status == STATUS_OBSOLETE))
            {
                continue;
            }

            if (i1_first_obj_flag == 0)
            {
                i1_first_obj_flag = 1;
                fprintf (f,
                         "\n/* Proto type for Low Level GET Routine All Objects.  */\n");
            }

            fprintf (f, "\nINT1");
            fprintf (f, "\nnmhGet%s ARG_LIST((",
                     p_table_struct->object[i4_count].object_name);

            /*  Fn passes all the Indices to the Low level routine. */
            passing_indextype_to_lowlevelproto (f);

            if (i4_global_scalar_flag == ZERO)
            {
                fprintf (f, ",");
            }

            /*  Passing the TYPE and Variable for The return Value.  */
            passing_typeto_lowlev_skeletolcode_getfirst (f, i4_count);

            fprintf (f, "));\n");
        }                        /* End of IF condition for Checking ACCESS Permissions.  */

    }                            /* End of Main For Loop. */

}                                /*  End of Function. */

/******************************************************************************
*      function Name        : low_proto_set_all_objects                       *
*      Role of the function : This fn Generates the ProtoTypes for the Low    *
*                             Level SET rtns for all the Objects in a Table . *
*      Formal Parameters    : f                                               *
*      Global Variables     : p_table_struct.                                 *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
low_proto_set_all_objects (FILE * f)
{
    INT4                i4_count;
    INT1                i1_first_obj_flag = 0;

    if (p_table_struct->status == STATUS_OBSOLETE)
    {
        return;
    }

    /*  Main For Loop for All the Objects.  */
    for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects; i4_count++)
    {
        if (p_table_struct->object[i4_count].status == STATUS_OBSOLETE)
        {
            continue;
        }
        if (i1_first_obj_flag == 0)
        {
            i1_first_obj_flag = 1;
            fprintf (f, "\n/* Low Level SET Routine for All Objects.  */\n");
        }

        if ((p_table_struct->object[i4_count].access == READ_WRITE)
            || (p_table_struct->object[i4_count].access == READ_CREATE))
        {
            fprintf (f, "\nINT1");
            fprintf (f, "\nnmhSet%s ARG_LIST((",
                     p_table_struct->object[i4_count].object_name);

            /* If Condition Which Checks the presence of Scalar Group. */
            if (i4_global_scalar_flag == FALSE)
            {
                /* This Fn Passes the Types of Objects for Each Index. */
                passing_indextype_to_lowlevelproto (f);
            }

            /*  Passing the TYPE and Variable for The return Value. */
            if (i4_global_scalar_flag == FALSE)
            {
                fprintf (f, " ,");
            }

            passing_typeto_lowlev_skeletolcode (f, i4_count);

            fprintf (f, "));\n");
        }                        /*  End of IF Condition for checking ACCESS permission.  */

    }                            /* End of Main For Loop.  */

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : low_proto_test_all_objects                      *
*      Role of the function : This fn Generates the ProtoTypes for the Low    *
*                             Level TEST rtns for all the Objects in a Table. *
*      Formal Parameters    : f                                               *
*      Global Variables     : p_table_struct.                                 *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
low_proto_test_all_objects (FILE * f)
{
    INT4                i4_count;
    INT1                i1_first_obj_flag = 0;

    if (p_table_struct->status == STATUS_OBSOLETE)
    {
        return;
    }

    /*  Main For Loop for All the Objects.  */
    for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects; i4_count++)
    {
        if (p_table_struct->object[i4_count].status == STATUS_OBSOLETE)
        {
            continue;
        }
        if (i1_first_obj_flag == 0)
        {
            i1_first_obj_flag = 1;
            fprintf (f, "\n/* Low Level TEST Routines for.  */\n");
        }

        if ((p_table_struct->object[i4_count].access == READ_WRITE)
            || (p_table_struct->object[i4_count].access == READ_CREATE))
        {
            fprintf (f, "\nINT1");
            /*
             *This is for returning errors which can only be returned from low
             *level code
             */
            fprintf (f, "\nnmhTestv2%s ARG_LIST((UINT4 *  ,",
                     p_table_struct->object[i4_count].object_name);

            /* If Condition Which Checks the presence of Scalar Group. */
            if (i4_global_scalar_flag == FALSE)
            {
                /* This Fn Passes the Types of Objects for Each Index.  */
                passing_indextype_to_lowlevelproto (f);
            }

            /*  Passing the TYPE and Variable for The return Value.  */
            if (i4_global_scalar_flag == FALSE)
            {
                fprintf (f, " ,");
            }
            passing_typeto_lowlev_skeletolcode (f, i4_count);

            fprintf (f, "));\n");
        }                        /* End of IF condition for checking ACCESS permissions.  */
    }                            /* End of Main For Loop. */
}                                /* End of Function . */

/******************************************************************************
*      function Name        : passing_indextype_to_lowlevelproto_getfirst     *
*      Role of the function : This fn Passes to the Low Level PROTO the       *
*                             Indices for GET FIRST & NEXT Rtns.              *
*      Formal Parameters    : f                                               *
*      Global Variables     : p_table_struct.                                 *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
passing_indextype_to_lowlevelproto_getfirst (FILE * f)
{
    INT4                i4_counter;
    INT4                i4_count;

    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {

        if (i4_counter >= ONE)
        {
            fprintf (f, " , ");
        }

        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            /* This Fn Passes the Type of the Variable to Low Level Protos. */
            passing_typeto_lowlev_skeletolcode_getfirst (f, i4_count);

            /* Break When A Index is Found. */
            break;
        }                        /* End Of FOR Loop. */

    }                            /* End of Main FOR Loop For Each Index. */

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : passing_indextype_to_lowlevelproto_getfirst     *
*      Role of the function : This fn Passes to the Low Level PROTO the       *
*                             Indices.                                        *
*      Formal Parameters    : f                                               *
*      Global Variables     : p_table_struct.                                 *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
passing_indextype_to_lowlevelproto (FILE * f)
{
    INT4                i4_counter;
    INT4                i4_count;

    for (i4_counter = ZERO; i4_counter < p_table_struct->no_of_indices;
         i4_counter++)
    {

        if (i4_counter >= ONE)
        {
            fprintf (f, " , ");
        }
        for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects +
             p_table_struct->no_of_imports; i4_count++)
        {

            if (COMPARE_OBJECT_WITH_INDEX (i4_count, i4_counter) != FAILURE)
            {
                continue;
            }

            /* This Fn Passes the Type of the Variable to Low Level Protos.  */
            passing_typeto_lowlev_skeletolcode (f, i4_count);

            /* Break When A Index is Found. */
            break;
        }                        /* End Of FOR Loop. */

    }                            /* End of Main FOR Loop For Each Index. */

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : prototype_FormFncts                             *
*      Role of the function : This fn declares the Prototype for the          *
*                             FromVarbind Fn.                                 *
*      Formal Parameters    : f                                               *
*      Global Variables     : None                                            *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
prototype_FormFncts (FILE * f)
{
    fprintf (f, " /*  Prototype for the FORMVARBIND Function. */\n");
    fprintf (f,
             "\ntSNMP_VAR_BIND * SNMP_AGT_FormVarBind ARG_LIST((tSNMP_OID_TYPE * ,");
    fprintf (f, " INT2 , UINT4 , INT4 , tSNMP_OCTET_STRING_TYPE * ,");
    fprintf (f, " tSNMP_OID_TYPE *,tSNMP_COUNTER64_TYPE));\n");

    fprintf (f, "\n/*  Prototype for the FORM OCTET STRING Function.  */\n");
    fprintf (f,
             "\n tSNMP_OCTET_STRING_TYPE * SNMP_AGT_FormOctetString ARG_LIST((");
    fprintf (f, "UINT1 *,INT4));\n\n");

    fprintf (f, "\n/*  Prototype for the FORM OID Function.  */\n");
    fprintf (f,
             "\n tSNMP_OID_TYPE * SNMP_AGT_FormOid ARG_LIST((UINT4 * ,INT2));\n\n");

}                                /* End Of Function. */

/******************************************************************************
*      function Name        : extern_prototype_FormFncts                             *
*      Role of the function : This fn declares the Prototype for the          *
*                             FromVarbind Fn.                                 *
*      Formal Parameters    : f                                               *
*      Global Variables     : i4_global_mac_flag                              *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
extern_prototype_FormFncts (FILE * f)
{
    fprintf (f, " /* Extern Prototype for the FORMVARBIND Function. */\n");
    fprintf (f,
             "\nextern tSNMP_VAR_BIND * SNMP_AGT_FormVarBind ARG_LIST((tSNMP_OID_TYPE * ,");
    fprintf (f, " INT2 , UINT4 , INT4 , tSNMP_OCTET_STRING_TYPE * ,");
    fprintf (f, " tSNMP_OID_TYPE *,tSNMP_COUNTER64_TYPE));\n");

    fprintf (f, "\n/*  Prototype for the FORM OCTET STRING Function.  */\n");
    fprintf (f,
             "\n extern tSNMP_OCTET_STRING_TYPE * SNMP_AGT_FormOctetString ARG_LIST((");
    fprintf (f, "UINT1 *,INT4));\n\n");

    fprintf (f, "\n/*  Prototype for the FORM OID Function.  */\n");
    fprintf (f,
             "\n extern tSNMP_OID_TYPE * SNMP_AGT_FormOid ARG_LIST((UINT4 * ,INT2));\n\n");

}

/******************************************************************************
*      function Name        : low_proto_dep_all_objects                      *
*      Role of the function : This fn Generates the ProtoTypes for the Low    *
*                             Level TEST rtns for all the Objects in a Table. *
*      Formal Parameters    : f                                               *
*      Global Variables     : p_table_struct.                                 *
*      Use of Recursion     : None                                            *
*      Return Value         : None.                                           *
******************************************************************************/
void
low_proto_dep_all_objects (FILE * f)
{
    INT4                i4_count;
    INT1                i1_first_obj_flag = 0;

    if (p_table_struct->status == STATUS_OBSOLETE)
    {
        return;
    }

    /*  Main For Loop for All the Objects.  */
    for (i4_count = ZERO; i4_count < p_table_struct->no_of_objects; i4_count++)
    {
        if (p_table_struct->object[i4_count].status == STATUS_OBSOLETE)
        {
            continue;
        }
        if (i1_first_obj_flag == 0)
        {
            i1_first_obj_flag = 1;
            fprintf (f, "\n/* Low Level DEP Routines for.  */\n");
        }

        if ((p_table_struct->object[i4_count].access == READ_WRITE)
            || (p_table_struct->object[i4_count].access == READ_CREATE))
        {
            if (p_table_struct->no_of_indices == 0)
            {
                fprintf (f, "\nINT1");
                /*
                 *This is for returning errors which can only be returned from low
                 *level code
                 */
                fprintf (f,
                         "\nnmhDepv2%s ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));\n",
                         p_table_struct->object[i4_count].object_name);
            }
            else
            {
                fprintf (f, "\nINT1");
                /*
                 *This is for returning errors which can only be returned from low
                 *level code
                 */
                fprintf (f,
                         "\nnmhDepv2%s ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));\n",
                         p_table_struct->table_name);
                break;
            }
        }
    }
}
