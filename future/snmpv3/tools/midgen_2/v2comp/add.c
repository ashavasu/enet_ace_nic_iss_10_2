/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: add.c,v 1.4 2015/04/28 14:43:33 siva Exp $
 *
 * Description: This file contains a function which adds an OID 
 *              to the linked list and supporting functions.
 *******************************************************************/

#include "postmosy.h"

extern INT1        *pi1_file_being_processed;
extern tOID_TABLE   recent_oid;
extern tOID_TABLE  *p_oid_list;
extern UINT4        u4_lineno;

/*****************************************************************************
 *      Function Name        : add_entry                                     *
 *      Role of the function : Place a node, comprises of Object Descriptor  *
 *                             and its OID, in the linked list pointed by    *
 *                             p_oid_list in the ascending order.            *
 *      Formal Parameters    : pi1_name, pi1_oid - Object name and its OID   *
 *      Global Variables     : p_oid_list, recent_oid                        *
 *      Use of Recursion     : None                                          *
 *      Return Value         : -1, if error occurs                           *
 *                              0, else                                      *
 *****************************************************************************/

INT4
add_entry (INT1 *pi1_name, INT1 *pi1_oid)
{
    INT1               *pi1_token, *pi1_orig_oid = pi1_oid;
    INT4                i4_retval;
    tOID_TABLE         *p_entry, *p_node = p_oid_list;
    tOID_TABLE         *p_temp, *p_prev_node = NULL;

    p_entry = ALLOC_ENTRY_MEM;
    if (p_entry == NULL)
    {
        fprintf (stderr, "\nMemory allocation failure\n");
        return ERROR;
    }
    strcpy (p_entry->object_desc, pi1_name);
    strcpy (p_entry->object_oid, pi1_oid);

    if (p_oid_list == NULL)
    {                            /* First node */
        p_oid_list = p_entry;
        return SUCCESS;
    }

    p_node = p_oid_list;
    p_prev_node = NULL;

    while (p_node->next != NULL)
        p_node = p_node->next;
    p_node->next = p_entry;

    return SUCCESS;
}

/*****************************************************************************
 *      Function Name        : get_next_component                            *
 *      Role of the function : Returns the pattern until the delimeter.      *
 *      Formal Parameters    : pi1_str, i1_delim                             *
 *      Global Variables     : None                                          *
 *      Use of Recursion     : None                                          *
 *      Return Value         : -1, if error occurs                           *
 *                              0, else                                      *
 *****************************************************************************/

INT1               *
get_next_component (INT1 *pi1_str, INT1 i1_delim)
{
    INT1               *pi1_temp, *pi1_pattern;
    UINT1               u1_count;

    pi1_temp = strchr (pi1_str, i1_delim);
    if (pi1_temp == NULL)
    {
        if (strlen (pi1_str) > MAX_DESC_LEN)
        {
            fprintf (stderr, "\n%s(%u): Descriptor length exceeded 64\n",
                     pi1_file_being_processed, u4_lineno);
            return NULL;
        }
        pi1_pattern = (INT1 *) malloc (strlen (pi1_str) + 1);
        if (pi1_pattern == NULL)
        {
            fprintf (stderr, "Memory allocation failure\n");
            return NULL;
        }
        strcpy (pi1_pattern, pi1_str);
    }
    else
    {
        u1_count = pi1_temp - pi1_str;
        if (u1_count > MAX_DESC_LEN)
        {
            fprintf (stderr, "\n%s(%u): Descriptor length exceeded 64\n",
                     pi1_file_being_processed, u4_lineno);
            return NULL;
        }
        pi1_pattern = (INT1 *) malloc (u1_count + 1);
        if (pi1_pattern == NULL)
        {
            fprintf (stderr, "Memory allocation failure\n");
            return NULL;
        }
        strncpy (pi1_pattern, pi1_str, u1_count);
        pi1_pattern[u1_count] = '\0';
    }
    return pi1_pattern;
}
