/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: store.c,v 1.4 2015/04/28 14:43:33 siva Exp $
 *
 * Description: This file contains routenes which store            
 *               passed string in a data structure.           
 *******************************************************************/

#include "premosy.h"

extern tNAME_LIST  *p_oid_list;
extern tNAME_LIST  *p_object_list;
extern tNAME_LIST  *p_import_list;
extern tNAME_VALUE *p_name_values;
extern tTC_TABLE   *p_type_defs;
extern tTC_TABLE   *p_this_tc;

/*****************************************************************************
 *      Function Name        : store_import                                  *
 *      Role of the function : Stores the passed string in p_import_list.    *
 *      Formal Parameters    : pi1_import_name                               *
 *      Global Variables     : p_import_list                                 *
 *      Use of Recursion     : None                                          *
 *      Return Value         : -1, if not stored due to malloc failure       *
 *                             0, if stored successfully                     *
 *****************************************************************************/

INT4
store_import (INT1 *pi1_import_name)
{
    tNAME_LIST         *p_entry, *p_temp = p_import_list;

    p_entry = ALLOC_NAME_MEM;
    if (p_entry == NULL)
    {
        fprintf (stderr, "Memory allocation failure\n");
        return ERROR;
    }
    strcpy (p_entry->name, pi1_import_name);
    p_entry->next = NULL;
    if (p_import_list == NULL)
    {
        p_import_list = p_entry;
    }
    else
    {
        while (p_temp->next != NULL)
        {
            p_temp = p_temp->next;
        }
        p_temp->next = p_entry;
    }
    return SUCCESS;
}

/*****************************************************************************
 *      Function Name        : store_oid                                     *
 *      Role of the function : Stores oid name in p_oid_list.                *
 *      Formal Parameters    : pi1_oid_name                                  *
 *      Global Variables     : p_oid_list                                    *
 *      Use of Recursion     : None                                          *
 *      Return Value         : -1, if not stored due to malloc failure       *
 *                             0, if stored successfully                     *
 *****************************************************************************/

INT4
store_oid (INT1 *pi1_oid_name)
{
    tNAME_LIST         *p_entry, *p_temp = p_oid_list;

    p_entry = ALLOC_NAME_MEM;
    if (p_entry == NULL)
    {
        fprintf (stderr, "Memory allocation failure\n");
        return ERROR;
    }
    strcpy (p_entry->name, pi1_oid_name);
    p_entry->next = NULL;
    if (p_temp == NULL)
    {
        p_oid_list = p_entry;
    }
    else
    {
        while (p_temp->next != NULL)
        {
            p_temp = p_temp->next;
        }
        p_temp->next = p_entry;
    }
    return SUCCESS;
}

/*****************************************************************************
 *      Function Name        : store_object                                  *
 *      Role of the function : Stores object name in p_object_list.          *
 *      Formal Parameters    : pi1_object_name                               *
 *      Global Variables     : p_object_list                                 *
 *      Use of Recursion     : None                                          *
 *      Return Value         : -1, if not stored due to malloc failure       *
 *                             0, if stored successfully                     *
 *****************************************************************************/

INT4
store_object (INT1 *pi1_object_name)
{
    tNAME_LIST         *p_entry, *p_temp = p_object_list;

    p_entry = ALLOC_NAME_MEM;
    if (p_entry == NULL)
    {
        fprintf (stderr, "Memory allocation failure\n");
        return ERROR;
    }
    strcpy (p_entry->name, pi1_object_name);
    p_entry->next = NULL;
    if (p_temp == NULL)
    {
        p_object_list = p_entry;
    }
    else
    {
        while (p_temp->next != NULL)
        {
            p_temp = p_temp->next;
        }
        p_temp->next = p_entry;
    }
    return SUCCESS;
}

/*****************************************************************************
 *      Function Name        : store_value                                   *
 *      Role of the function : Stores value either in p_this_tc              *
 *                             or p_name_values based on the context.        *
 *      Formal Parameters    : i1_is_tc, i4_value                            *
 *      Global Variables     : p_name_values, p_this_tc                      *
 *      Use of Recursion     : None                                          *
 *      Return Value         : -1, if not stored due to malloc failure       *
 *                             0, if stored successfully                     *
 *****************************************************************************/

INT4
store_value (INT1 i1_is_tc, INT4 i4_value)
{
    tNAME_VALUE        *p_entry, *p_temp_name_value;

    p_entry = ALLOC_NAME_VALUE_MEM;
    if (p_entry == NULL)
    {
        fprintf (stderr, "Memory allocation failure\n");
        return ERROR;
    }
    p_entry->value = i4_value;
    p_entry->next = NULL;
    if (i1_is_tc == MID_TRUE)
    {
        p_temp_name_value = p_this_tc->tc_name_value_list;
    }
    else
    {
        p_temp_name_value = p_name_values;
    }
    if (p_temp_name_value == NULL)
    {
        if (i1_is_tc == MID_TRUE)
        {
            p_this_tc->tc_name_value_list = p_entry;
        }
        else
        {
            p_name_values = p_entry;
        }
    }
    else
    {
        while (p_temp_name_value->next != NULL)
        {
            p_temp_name_value = p_temp_name_value->next;
        }
        p_temp_name_value->next = p_entry;
    }
    return SUCCESS;
}

/*****************************************************************************
 *      Function Name        : itoa                                          *
 *      Role of the function : Converts integer value to string.             *
 *      Formal Parameters    : i4_value                                      *
 *      Global Variables     : None                                          *
 *      Use of Recursion     : None                                          *
 *      Return Value         : str, if converted                             *
 *                             NULL, if not converted                        *
 *****************************************************************************/

INT1               *
itoa (INT4 i4_value)
{
    INT1                ch, *str;
    INT4                i = 0, j;

    str = ALLOC_DESC_MEM;
    if (str == NULL)
    {
        fprintf (stderr, "Memory allocation failure\n");
        return NULL;
    }
    if (i4_value < 0)
    {
        str[i++] = '-';
        i4_value *= -1;
    }
    else if (i4_value == 0)
    {
        str[i++] = '0';
    }
    while (i4_value != 0)
    {
        str[i++] = '0' + (i4_value % 10);
        i4_value = i4_value / 10;
    }
    str[i] = '\0';
    for (i = 0, j = strlen (str) - 1; i < j; i++, j--)
    {
        ch = str[i];
        str[i] = str[j];
        str[j] = ch;
    }
    return str;
}

/*****************************************************************************
 *      Function Name        : uitoa                                         *
 *      Role of the function : Converts unsigned integer value to string.    *
 *      Formal Parameters    : i4_value                                      *
 *      Global Variables     : None                                          *
 *      Use of Recursion     : None                                          *
 *      Return Value         : str, if converted                             *
 *                             NULL, if not converted                        *
 *****************************************************************************/

INT1               *
uitoa (UINT4 i4_value)
{
    INT1                ch, *str;
    INT4                i = 0, j;

    str = ALLOC_DESC_MEM;
    if (str == NULL)
    {
        fprintf (stderr, "Memory allocation failure\n");
        return NULL;
    }
    if (i4_value < 0)
    {
        str[i++] = '-';
        i4_value *= -1;
    }
    else if (i4_value == 0)
    {
        str[i++] = '0';
    }
    while (i4_value != 0)
    {
        str[i++] = '0' + (i4_value % 10);
        i4_value = i4_value / 10;
    }
    str[i] = '\0';
    for (i = 0, j = strlen (str) - 1; i < j; i++, j--)
    {
        ch = str[i];
        str[i] = str[j];
        str[j] = ch;
    }
    return str;
}
