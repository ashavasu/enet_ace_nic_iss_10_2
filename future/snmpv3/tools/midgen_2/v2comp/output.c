/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: output.c,v 1.4 2015/04/28 14:43:33 siva Exp $
 *
 * Description: This file contains a routine which dumps contents
 *             of p_oid_list to file.
 *******************************************************************/

#include "postmosy.h"

extern tOID_TABLE  *p_oid_list;

/*****************************************************************************
 *      Function Name        : write_oid_list_to_file                        *
 *      Role of the function : Dumps the contents of p_oid_list to a file.   *
 *      Formal Parameters    : pi1_file_name - output file name              *
 *      Global Variables     : p_oid_list                                    *
 *      Use of Recursion     : None                                          *
 *      Return Value         : -1, if error occurs                           *
 *                              0, else                                      *
 *****************************************************************************/

INT4
write_oid_list_to_file (INT1 *pi1_file_name, INT1 *pi1OnFilename)
{
    tOID_TABLE         *p_temp;
    FILE               *p_out_file;
    FILE               *p_on_file;
    INT4                i4_count = 0;

    if ((p_out_file = fopen (pi1_file_name, "w")) == NULL)
    {
        fprintf (stderr, "\nCan't open %s\n", pi1_file_name);
        return ERROR;
    }
    if ((p_on_file = fopen (pi1OnFilename, "w")) == NULL)
    {
        fprintf (stderr, "\nCan't open %s\n", pi1OnFilename);
        return ERROR;
    }

    fprintf (p_out_file, "/*\n");
    fprintf (p_out_file, " * Automatically generated by FuturePostmosy\n");
    fprintf (p_out_file, " * Do not Edit!!!\n");
    fprintf (p_out_file, " */\n\n");
    fprintf (p_out_file, "#ifndef _SNMP_MIB_H\n");
    fprintf (p_out_file, "#define _SNMP_MIB_H\n\n");
    fprintf (p_out_file, "/* SNMP-MIB translation table. */\n\n");
    fprintf (p_out_file, "static struct MIB_OID orig_mib_oid_table[] = {\n");

    while ((p_temp = p_oid_list) != NULL)
    {
        fprintf (p_out_file, "\"%s\",        \"%s\",\n",
                 p_oid_list->object_desc, p_oid_list->object_oid);
        fprintf (p_on_file, "{");
        fprintf (p_on_file, "{{{0x0, 0x0}, 0x0, 0 ,  {0, 0, 0}}},");
        fprintf (p_on_file, "{{{0x0, 0x0}, 0x0,  0 , {0, 0, 0}}},");
        fprintf (p_on_file, "\"%s\",\"%s\"},\n\n",
                 p_oid_list->object_desc, p_oid_list->object_oid);

        p_oid_list = p_oid_list->next;
        i4_count = i4_count + 1;
        free (p_temp);
    }
    fprintf (p_out_file, "0,0\n");
    fprintf (p_out_file, "};\n\n");
    fprintf (p_out_file, "#endif /* _SNMP_MIB_H */\n");
    fclose (p_out_file);
    fclose (p_on_file);
    return SUCCESS;
}
