/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: process.c,v 1.4 2015/04/28 14:43:33 siva Exp $
 *
 * Description: This file contains routines which process          
 *              input files and place OIDs in linked list.
 *******************************************************************/

#include "postmosy.h"

UINT4               u4_lineno;    /* indicates the line being processed in input file */
extern tOID_TABLE  *p_oid_list;
extern INT1        *pi1_file_being_processed;

/*****************************************************************************
 *      Function Name        : initialise_oid_table                          *
 *      Role of the function : This function initialises the linked list     *
 *                             with standard OIDs.                           *
 *      Formal Parameters    : None                                          *
 *      Global Variables     : None                                          *
 *      Use of Recursion     : None                                          *
 *      Return Value         : -1, if error occurs                           *
 *                              0, else                                      *
 *****************************************************************************/

INT4
initialise_oid_table (void)
{
    if (add_entry ("ccitt", "0") == ERROR)
    {
        return ERROR;
    }
    if (add_entry ("iso", "1") == ERROR)
    {
        return ERROR;
    }
    if (add_entry ("joint-iso-ccitt", "2") == ERROR)
    {
        return ERROR;
    }
    if (add_entry ("org", "iso.3") == ERROR)
    {
        return ERROR;
    }
    if (add_entry ("dod", "org.6") == ERROR)
    {
        return ERROR;
    }
    if (add_entry ("internet", "dod.1") == ERROR)
    {
        return ERROR;
    }
    if (add_entry ("directory", "internet.1") == ERROR)
    {
        return ERROR;
    }
    if (add_entry ("mgmt", "internet.2") == ERROR)
    {
        return ERROR;
    }
    if (add_entry ("mib-2", "mgmt.1") == ERROR)
    {
        return ERROR;
    }
    if (add_entry ("transmission", "mib-2.10") == ERROR)
    {
        return ERROR;
    }
    if (add_entry ("experimental", "internet.3") == ERROR)
    {
        return ERROR;
    }
    if (add_entry ("private", "internet.4") == ERROR)
    {
        return ERROR;
    }
    if (add_entry ("enterprises", "private.1") == ERROR)
    {
        return ERROR;
    }
    if (add_entry ("security", "internet.5") == ERROR)
    {
        return ERROR;
    }
    if (add_entry ("snmpV2", "internet.6") == ERROR)
    {
        return ERROR;
    }
    if (add_entry ("snmpDomains", "snmpV2.1") == ERROR)
    {
        return ERROR;
    }
    if (add_entry ("snmpProxys", "snmpV2.2") == ERROR)
    {
        return ERROR;
    }
    if (add_entry ("snmpModules", "snmpV2.3") == ERROR)
    {
        return ERROR;
    }

    return SUCCESS;
}

/*****************************************************************************
 *      Function Name        : process_input                                 *
 *      Role of the function : Process input file and update the linked list *
 *                             by adding one node for each name and OID pair.*
 *      Formal Parameters    : pi1_in_file                                   *
 *      Global Variables     : p_oid_list                                    *
 *      Use of Recursion     : None                                          *
 *      Return Value         : ERROR, if error occurs                        *
 *                             SUCCESS, else                                 *
 *****************************************************************************/

INT4
process_input (INT1 *pi1_in_file)
{
    INT1                i1_line[MAX_LINE_LEN], *pi1_line;
    INT1                i1_object_desc[MAX_DESC_LEN],
        i1_object_oid[MAX_OID_LEN];
    INT4                i4_retval;
    FILE               *p_in_file;

    if ((p_in_file = fopen (pi1_in_file, "r")) == NULL)
    {
        fprintf (stderr, "\nCan't open %s\n", pi1_in_file);
        return ERROR;
    }
    u4_lineno = 0;
    while (fgets (i1_line, MAX_LINE_LEN, p_in_file) != NULL)
    {
        u4_lineno++;
        i4_retval = check_line (i1_line);
        if (i4_retval == BLANK || i4_retval == COMMENT)
        {
            continue;
        }
        if (i4_retval == INVALID)
        {
            fprintf (stderr, "\n%s(%u): Invalid line\n", pi1_in_file,
                     u4_lineno);
            fprintf (stderr, "%s\n", i1_line);
            fclose (p_in_file);
            return ERROR;
        }
        if (i4_retval == -1)
        {
            return ERROR;
        }
        for (pi1_line = i1_line; *pi1_line == ' ' || *pi1_line == '\t';
             ++pi1_line);
        sscanf (pi1_line, "%s %s", i1_object_desc, i1_object_oid);
        if (add_entry (i1_object_desc, i1_object_oid) == ERROR)
        {
            return ERROR;
        }
    }
    fclose (p_in_file);
    return SUCCESS;
}

/*****************************************************************************
 *      Function Name        : check_line                                    *
 *      Role of the function : Checks line for comment, blank and valid.     *
 *      Formal Parameters    : pi1_line                                      *
 *      Global Variables     : None                                          *
 *      Use of Recursion     : None                                          *
 *      Return Value         : VALID, if contains the required fields        *
 *                             INVALID, if not contains the required fields  *
 *                             COMMENT, if comment line                      *
 *                             BLANK, if blank line                          *
 *****************************************************************************/

INT4
check_line (INT1 *pi1_line)
{
    INT1               *pi1_temp;

    if (strstr (pi1_line, "--") == pi1_line)
    {
        return COMMENT;
    }
    while (*pi1_line == ' ' || *pi1_line == '\t')
    {
        pi1_line++;
    }
    if (*pi1_line == '\n')
    {
        return BLANK;
    }
    else
    {
        if (!isalpha (*pi1_line) || isupper (*pi1_line))
        {
            fprintf (stderr,
                     "\n%s(%u): ObjectName should start with lowercase letter\n",
                     pi1_file_being_processed, u4_lineno);
            return ERROR;
        }
        pi1_temp = strchr (pi1_line, ' ');
        if (pi1_temp == NULL)
        {
            pi1_temp = strchr (pi1_line, '\t');
        }
        if (pi1_temp == NULL)
        {
            return INVALID;
        }
        if (pi1_temp - pi1_line > MAX_DESC_LEN)
        {
            fprintf (stderr, "\n%s(%u): Descriptor length exceeded 64\n",
                     pi1_file_being_processed, u4_lineno);
            return ERROR;
        }
        pi1_line = pi1_temp;
        while (*pi1_line == ' ' || *pi1_line == '\t')
        {
            pi1_line++;
        }
        if (*pi1_line == '\n')
        {
            return INVALID;
        }
        return VALID;
    }
}
