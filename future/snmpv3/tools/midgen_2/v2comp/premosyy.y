%{
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: premosyy.y,v 1.4 2015/04/28 14:43:33 siva Exp $
 *
 * Description: This file contains specification for parser.      
 *              
 *******************************************************************/

#include "premosy.h"
#include "postmosy.h"

INT1 check_agument[MAX_DESC_LEN][MAX_DESC_LEN];/*Stores AUGMENT object*/
INT4 count = 0;/*Counter*/
INT4 i4dummymin = 0;/*Stores the min values of range*/
UINT4 u4dummymax = 0;/*Stores the max values of range*/
INT4 i4Count = 0;/*counter*/
INT4 i4_flag1 = 0,i4_flag2 = 0;/*Flags to represent the range type*/
INT4 i4_range[4] = {0, 0, 0, 0};/*stores the min and max values of range when more than one
                         range is specifeied*/
INT4 i4_dummy=0;/*dummy variable*/
INT4 i4_dupval[MAX_DESC_LEN] = {0};/*Stores the values to check whether the value
                                     is repeated more than once*/
INT4 i4_dupcount = 0;/*Used as the counter*/ 
INT4 i4_dup = 0;/*Used as the counter*/
INT4 i4_dupcounter = 0;/*Used as the counter*/
INT4 i4_check_flag = 0;/*Used as the flag to check whether the syntax type is imported*/
tNAME_LIST *p_temp_import_list;/* Stores Imports */
INT4 check_count=0;/*Stores the number of AUGMENT objects that are not found*/
INT4 check_flag=0;/*Indicates whether all the AUGMENT objects are found or not*/
INT1 i1_is_oid = MID_FALSE; /* indicates whether it's a OID assignment */
INT1 i1_oid_wanted = MID_TRUE; /* some OIDs need not be printed to output file */
INT1 i1_is_row_name = MID_FALSE; /* indicates whether it's row of table */
INT1 i1_rowdef_exists = MID_FALSE; /* indicates whether row definition exists */
INT1 i1_is_tc = MID_TRUE; /* indicatess whether it's textual convention parsed */
INT1 i1_is_defval = MID_FALSE; /* whether it's a default value being parsed */
INT1 i1_start_check = MID_FALSE; /* whether objects should be checked for order */
INT1 i1_enum_type = MID_FALSE; /* indicates whether it's enumerated syntax type */
INT1 i1_access[MAX_DESC_LEN]; /* stores the value of ACCESS clause */
INT1 i1_status[MAX_DESC_LEN]; /* stores the value of STATUS clause */
INT1 i1_syntax[MAX_DESC_LEN]; /* stores the value of SYNTAX clause */
INT1 i1_object_desc[MAX_DESC_LEN]; /* keeps the name of object descriptor */
INT1 i1_object_identifier[MAX_OID_LEN]; /* Complete OID in dotted form */
INT1 i1_row_name[MAX_DESC_LEN]; /* stores the name of row of a table */
INT1 i1_out_file[MAX_FILE_NAME_LEN]; /* holds name of the output file */
INT1 i1_in_file[MAX_FILE_NAME_LEN]; /* holds name of the input file */
INT1 i1_defval_file[MAX_FILE_NAME_LEN]; /* holds name of the defval file */
INT1 i1_temp_file[MAX_FILE_NAME_LEN]; /* Details of all objects are written to*/
tNAME_LIST *p_oid_list; /* Stores OID assignments, table names and rownames */
tNAME_LIST *p_object_list; /* Stores all OBJECT-TYPE's */
tNAME_LIST *p_import_list; /* Stores all Imports */
tNAME_LIST *p_table_entries; /* store all columnar objects to check for order */
tNAME_LIST *p_this_entry; /* points to object name that should be read now */
tNAME_LIST *p_indices; /* stores indices of the table */ 
tNAME_LIST *p_checked_indices; /* stores valid indices */
tNAME_VALUE *p_name_values; /* stores size or named-values of syntax */
tOID *p_object_ids; /* holds name and OID of all object identifiers */
tTC_TABLE *p_type_defs; /* Store name, base type and size/named-values */
tTC_TABLE *p_this_tc; /* points to the node beibg filled up */
FILE *p_out_file; /* file pointer for output file */
FILE *p_defval_file; /* file pointer for default value file */
FILE *p_temp_file; /* file pointer for temp file */
extern INT4 yylineno; /* counts number of lines in input */
extern INT1 *yytext; /* where Lexer keeps the matched input */
extern FILE *yyin; /* where input for Lexer comes from */
INT1 obj_Des[MAX_DESC_LEN][MAX_DESC_LEN]; /*To store the object identifier 
                                            whose type is not know*/
INT4 i4_count_invalid_oid = 0;/*Counter to store the object identifiers count
                               whose type is not know*/
INT4 i4_check_implied = 0;/*To check whether object with IMPLIED clause
                            is the last object in the index*/
INT4 i4_check_implied_present = 0;/*To check whether a object with IMPLIED clause 
                                    is present in the INDEX*/
INT4 i4_len = 0;/*To store the length of the string*/
INT1 p_num_text[15] = {0};/*To store the string*/
INT4 i4_first_num = 0;/*To strore the output of atoi function*/
INT4 i4_check_for_fisrt_enum = 0;/*To check for the fisrt enum*/
INT4 i4_check_for_zero = 0;/*Used as flag to check the subidentifier value*/
INT1 *p1_num_text = NULL;/*Dummy pointers*/
INT1 *p2_num_text = NULL;/*Dummy Pointers*/
INT1 *p3_num_text = "2147483648";/*Stores max value of integer*/
INT1 *p4_num_text = NULL;/*Dummy pointers*/
INT8 i8_check_for_range = 0;/*To store the unsigned value*/
INT8 i8_max_unsigned_range = 4294967296;/*Stores max value of unsigned integer*/
INT1 i1ModuleName[64];
INT1 i1IsInt = 0; /*To check whether range is integer*/
UINT1 u1EnumSum[16] = { 0 }; /* To store the enumerator sum for BITS syntax. */
UINT1 u1EnumList[128] = { 0 }; /* Enumerator BIT List */

%}
%union {
    INT1 str[MAX_DESC_LEN];
	INT4 intval;
};

%token <str> MODULE_IDENTITY OBJECT_IDENTITY TEXTUAL_CONVENTION OBJECT_TYPE 
%token <str> NOTIFICATION_TYPE MODULE_COMPLIANCE 
%token <str> OBJECT_GROUP NOTIFICATION_GROUP AGENT_CAPABILITIES
%token DEFINITIONS BEGIN_DEF CCE IMPORTS FROM END REVISION UNITS
%token LAST_UPDATED ORGANIZATION CONTACT_INFO
%token OBJECT IDENTIFIER SYNTAX DESCRIPTION REFERENCE INDEX IMPLIED DEFVAL
%token MAX_ACCESS READ_ONLY READ_WRITE READ_CREATE NOT_ACCESSIBLE
%token MIN_ACCESS ACCESS ACCESSIBLE_FOR_NOTIFY NOT_IMPLIMENTED WRITE_ONLY
%token STATUS CURRENT DEPRECATED OBSOLETE DISPLAY_HINT
%token SEQUENCE OF OBJECTS SIZE OCTET STRING MODULE TRUE FALSE 
%token ABSENT ANY APPLICATION BIT BOOLEAN BY CHOICE COMPONENT COMPONENTS
%token DEFAULT DEFINED ENUMERATED ENTERPRISE EXPLICIT EXPORTS EXTERNAL 
%token IMPLICIT MAX MIN MINUS_INFINITY OPTIONAL PLUS_INFINITY PRESENT
%token PRIVATE REAL SET TAGS TRAP_TYPE UNIVERSAL VARIABLES WITH 
%token INCLUDES SUPPORTS VARIATION WRITE_SYNTAX CREATION_REQUIRES AUGMENTS 
%token MANDATORY_GROUPS GROUP PRODUCT_RELEASE NOTIFICATIONS MANDATORY_GROUPS
%token <str> NAME HNAME QSTRING BINSTRING HEXSTRING
%token <intval> NUM ZERO
%type <str> Modules Module MibName ObjectDescriptor SyntaxType Objects
%type <str> ModuleName NameHName Notifications NotificationName
%type <str> ObjectName Imports TypesSet Types Type Cells Cell Groups GroupName
%type <intval> DefaultValue DefValue Value DValue Ranges Range AnyNum Num

%%

begin: MibName DEFINITIONS CCE BEGIN_DEF {
			 fprintf (p_out_file, "-- Automatically generated by FutureV2comp\n");
			 fprintf (p_out_file, "-- Do not Edit!!!\n");
			 fprintf (p_out_file, "-- Object definitions compiled from %s\n\n", $1);
       } body END
	  ;	 

body: IMPORTS Imports ';'
      ObjectDescriptor MODULE_IDENTITY {
         i1_is_oid = MID_TRUE;
         strcpy (i1_object_desc, $4);
         fprintf (p_out_file, "%s   ", $4);
      } ModuleIdentityBody CCE ObjectId {
         i1_is_oid = MID_FALSE;
			if (store_oid ($4) == ERROR) {
			   yyerror ("");
			}
      }
      Modules
	 ;

Imports: TypesSet
       | Imports TypesSet
		 ;

TypesSet: Types FROM MibName
        ;

Types: Type
     | Types ',' Type
	  ;

ModuleIdentityBody: LAST_UPDATED QSTRING
                    ORGANIZATION QSTRING
                    CONTACT_INFO QSTRING
                    DESCRIPTION QSTRING
                    RevisionPart
                  ;

RevisionPart: Revisions
            |
            ;

Revisions: Revision
         | Revisions Revision
         ;

Revision: REVISION QSTRING
          DESCRIPTION QSTRING
        ;

Modules: Module
       | Modules Module
		 ;

Module: ObjectDescriptor OBJECT_IDENTITY {
			  i1_is_oid = MID_TRUE;
           if (check_for_duplicate_name ($1, i1_is_oid) == MID_TRUE) {
              fprintf (stderr, "\nERROR: Duplicate name %s exists\n", $1);
              yyerror ("");
           }
           strcpy (i1_object_desc, $1);
		     fprintf (p_out_file, "%s   ", $1);
        } ObjectIdentityBody CCE ObjectId {
			  i1_is_oid = MID_FALSE;
			  if (store_oid ($1) == ERROR) {
			     yyerror ("");
			  }
		  }
      | ObjectDescriptor OBJECT IDENTIFIER CCE {
          /*Check for the flag,inoder to determine whether a invalid Obj-Des is found*/
          if (i4_count_invalid_oid > 0)
          {
              for (count = 0; count < i4_count_invalid_oid; count++)
              {    
                  if(!strcmp(obj_Des[count],$1))    
                  {
                      /*printf ("OBJ : %s,TEMP : %s\n",obj_Des[count],$1);*/
                      i4_count_invalid_oid--;
                  }
              }
           }
			  i1_is_oid = MID_TRUE;
           if (check_for_duplicate_name ($1, i1_is_oid) == MID_TRUE) {
              fprintf (stderr, "\nERROR: Duplicate name %s exists\n", $1);
              yyerror ("");
           }
           strcpy (i1_object_desc, $1);
		     fprintf (p_out_file, "%s   ", $1);
        } ObjectId {
			  i1_is_oid = MID_FALSE;
			  if (store_oid ($1) == ERROR) {
			     yyerror ("");
			  }
		  }
		| SyntaxType CCE TEXTUAL_CONVENTION TextualConventionBody {
           tTC_TABLE *p_temp_tc = p_type_defs;

           strcpy (p_this_tc->tc_name, $1);
           p_this_tc->next = NULL;
           if (p_type_defs == NULL) {
              p_type_defs = p_this_tc;
           }
           else {
              while (p_temp_tc->next != NULL) {
                 p_temp_tc = p_temp_tc->next;
              }
              p_temp_tc->next = p_this_tc;
           }
		  }
      | ObjectDescriptor OBJECT_TYPE {
           if (i1_rowdef_exists == MID_TRUE) {
              yyerror ("Row definition missing");
           }
           /*Checks for the AUGMENT object*/
           if(check_flag > 0) {
              for(count = 0; count < check_count; count++)
              {
                  if(strcmp(check_agument[count],$1)==0)
                  {
                      check_flag=check_flag-1;
                  }
              }
           }

           if ((i1_is_row_name == MID_TRUE)) {
              i1_is_row_name = MID_FALSE;
              i1_rowdef_exists = MID_TRUE;
              if (store_oid ($1) == ERROR) {
                 yyerror ("");
              }
           }
           if (check_for_duplicate_name ($1, i1_is_oid) == MID_TRUE) {
              fprintf (stderr, "\nERROR: Duplicate name %s exists\n", $1);
              yyerror ("");
           }
			  if (store_object ($1) == ERROR) {
			     yyerror ("");
			  }
           if (i1_start_check == MID_TRUE) {
              if (p_this_entry == NULL) {
		 free_names_memory (p_table_entries);
                 p_table_entries = NULL;
                 i1_start_check = MID_FALSE;
              }
              else if (!check_for_table_object($1) /* strcmp (p_this_entry->name, $1) != 0 */) {
          /*       yyerror ("Object not in order or missing");*/
              }  
              else  {
                 p_this_entry = p_this_entry->next;
              }
           }
			  strcpy (i1_object_desc, $1);
		     fprintf (p_temp_file, "%s   ", $1);
			  i1_is_tc = MID_FALSE;
		  } ObjectTypeBody CCE {i4_check_for_zero = 1;}ObjectId {
		     fprintf (p_temp_file, "%s   %s   %s\n", i1_syntax, i1_access, i1_status);
           free_name_values_memory (p_name_values);
           p_name_values = NULL;
			  i1_is_tc = MID_TRUE;
              i4_check_for_zero = 0;
		  }
      | SyntaxType CCE SEQUENCE {
			  if (i1_rowdef_exists == MID_FALSE) {
			     yyerror ("Expecting entry definition after table definition");
			  }
			  if (strcmp ($1, i1_row_name) != 0) {
			     yyerror ("Row name mismatch");
			  }
           i1_rowdef_exists = MID_FALSE;
           i1_start_check = MID_TRUE;
		  } '{' ConceptualTableEntry '}' {
           tNAME_LIST *p_checked, *p_table, *p_entry, *p_index = p_indices;

           while (p_index != NULL) {
              p_table = p_table_entries;
              while (p_table != NULL) {
                 if (strcmp (p_index->name, p_table->name) == 0) {
                    p_entry = ALLOC_NAME_MEM;
                    if (p_entry == NULL) {
                       fprintf (stderr, "Memory allocation failure\n");
                       yyerror ("");
                    }
                    strcpy (p_entry->name, p_index->name);
                    p_entry->next = NULL;
                    if (p_checked_indices == NULL) {
                       p_checked_indices = p_entry;
                    }
                    else {
                       p_checked = p_checked_indices;
                       while (p_checked->next != NULL) {
                          p_checked = p_checked->next;
                       }
                       p_checked->next = p_entry;
                    }
                    p_index = p_index->next;
                    break;
                 }
                 p_table = p_table->next;
              }
              if (p_table == NULL) {
                 p_checked = p_checked_indices;
                 while (p_checked != NULL) {
                    if (!strcmp (p_index->name, p_checked->name)) {
                       p_index = p_index->next;
                       break;
                    }
                    p_checked = p_checked->next;
                 }
                 if (p_checked == NULL) {
                    if (check_validity_of_import (p_index->name) == VALID) {
                       p_index = p_index->next;
                    }
                    else {
                    if (check_validity_of_object (p_index->name) != VALID)
                    {
                      yyerror ("Index missing");
                    }
                    break;
                    /* yyerror ("Index missing"); */
                    }
                 }
              }
           }
           free_names_memory (p_indices);
           p_indices = NULL;
        }
      | ObjectDescriptor NOTIFICATION_TYPE {
           i1_oid_wanted = MID_FALSE;
        } NotificationTypeBody CCE ObjectId {
           i1_oid_wanted = MID_TRUE;
        }
		| ObjectDescriptor MODULE_COMPLIANCE {
           i1_oid_wanted = MID_FALSE;
        } ModuleComplianceBody CCE ObjectId {
           i1_oid_wanted = MID_TRUE;
        }
      | ObjectDescriptor OBJECT_GROUP {
           i1_oid_wanted = MID_FALSE;
        } ObjectGroupBody CCE ObjectId {
           i1_oid_wanted = MID_TRUE;
        }
      | ObjectDescriptor NOTIFICATION_GROUP {
           i1_oid_wanted = MID_FALSE;
        } NotificationGroupBody CCE ObjectId {
           i1_oid_wanted = MID_TRUE;
        }
      | ObjectDescriptor AGENT_CAPABILITIES {
           i1_oid_wanted = MID_FALSE;
        } AgentCapabilitiesBody CCE ObjectId {
           i1_oid_wanted = MID_TRUE;
        }
      ;

ObjectIdentityBody: STATUS Status
                    DESCRIPTION QSTRING
                    ReferPart
                  ;

TextualConventionBody: DisplayPart
                       STATUS Status
                       DESCRIPTION QSTRING
                       ReferPart
                       SYNTAX Syntax
                     ;

DisplayPart: DISPLAY_HINT QSTRING
           |
           ;

ObjectTypeBody: SYNTAX Syntax
                UnitsPart
                MAX_ACCESS Access 
					 STATUS Status
					 DESCRIPTION QSTRING
					 ReferPart
					 IndexPart
					 DefValPart
				  ;

UnitsPart: UNITS QSTRING
         |
         ;

NotificationTypeBody: ObjectsPart
                      STATUS Status
		      DESCRIPTION QSTRING
		      ReferPart
						  ;

ObjectsPart: OBJECTS '{' Objects '}'
           |
			  ;

Objects: ObjectName
       | Objects ',' ObjectName
		 ;

ModuleComplianceBody: STATUS Status
                      DESCRIPTION QSTRING
							 ReferPart
							 CModulePart
						  ;	

CModulePart: CModules
          |
			 ;

CModules: CModule
       | CModules CModule
		 ;

CModule: MODULE ModuleName 
         MandatoryPart
		   CompliancePart
		 ;

MandatoryPart: MANDATORY_GROUPS '{' Groups '}'
             | 
				 ;

Groups: GroupName
      | Groups ',' GroupName
		;

CompliancePart: Compliances
              |
				  ;

Compliances: Compliance
           | Compliances Compliance
			  ;

Compliance: ComplianceGroup
          | Object
			 ;

ComplianceGroup: GROUP ObjectDescriptor
                 DESCRIPTION QSTRING
					;

Object: OBJECT ObjectDescriptor
        SyntaxPart
		  WriteSyntaxPart
		  AccessPart
		  DESCRIPTION QSTRING
		;

SyntaxPart: SYNTAX Syntax
          |
			 ;

WriteSyntaxPart: WRITE_SYNTAX Syntax
               |
					;

AccessPart: MIN_ACCESS Access
          | 
			 ;

GroupName: ObjectDescriptor
         ;

ObjectGroupBody: ObjectsPart
                 STATUS Status
                 DESCRIPTION QSTRING
                 ReferPart
               ;

NotificationGroupBody: NotificationsPart
                       STATUS Status
                       DESCRIPTION QSTRING
                       ReferPart
                     ;

NotificationsPart: NOTIFICATIONS '{' Notifications '}'
                 ;

Notifications: NotificationName
             | Notifications ',' NotificationName
             ;

AgentCapabilitiesBody: PRODUCT_RELEASE QSTRING
                       STATUS Status
                       DESCRIPTION QSTRING
                       ReferPart
                       AModulePart
                     ;

AModulePart: AModules
           |
           ;

AModules: AModule
        | AModules AModule
        ;

AModule: SUPPORTS ModuleName
         INCLUDES '{' Groups '}'
         VariationPart
       ;

VariationPart: Variations
             |
				 ;

Variations: Variation
			 | Variations Variation
          ;

Variation: VARIATION ObjectName
        	  SyntaxPart
        	  WriteSyntaxPart
        	  AAccessPart
        	  CreationPart
        	  DefValPart
        	  DESCRIPTION QSTRING
         ;

AAccessPart: ACCESS AAccess
			 |
          ;

AAccess: Access
       | NOT_IMPLIMENTED
       | WRITE_ONLY
       ;

CreationPart: CREATION_REQUIRES '{' Cells '}'
            |
				;

Cells: Cell
	  | Cells ',' Cell
     ;

Cell: ObjectName
    ;

ConceptualTableEntry: Entries
                    ;

Entries: Entry
       | Entries ','  Entry
		 ;

Entry: ObjectDescriptor {
          tNAME_LIST *p_entry, *p_temp_name = p_table_entries;

          p_entry = ALLOC_NAME_MEM;
          if (p_entry == NULL) {
             fprintf (stderr, "Memory allocation failure\n");
             yyerror ("");
          }
          strcpy (p_entry->name, $1);
          p_entry->next = NULL;
          if (p_table_entries == NULL) { /* first node */
             p_table_entries = p_entry;
             p_this_entry = p_table_entries;
          }
          else {
             while (p_temp_name->next != NULL) {
                p_temp_name = p_temp_name->next;
             }
             p_temp_name->next = p_entry;
          }
       } Syntax
     ;

DefValPart: DEFVAL {
               if (!strcmp (i1_syntax, "Counter32") ||
                   !strcmp (i1_syntax, "Counter64")) {
                  yyerror ("Syntax Error");
               }
               i1_is_defval = MID_TRUE;
            } '{' DefaultValue '}' {
               i1_is_defval = MID_FALSE;
            }  
			 |
          ;

DefaultValue: DefValue
            ;

DefValue: ObjectDescriptor {
             tTC_TABLE *p_temp_tc = p_type_defs;
             if ((strcmp (i1_syntax, "OCTET STRING") == 0) ||
                 (strcmp (i1_syntax, "IpAddress") == 0) ||
                 (strcmp (i1_syntax, "TimeTicks") == 0) ||
                 (strcmp (i1_syntax, "Integer32") == 0) ||
                 (strcmp (i1_syntax, "Unsigned32") == 0) ||
                 (strcmp (i1_syntax, "Gauge32") == 0) ||
                 (strcmp (i1_syntax, "Opaque") == 0)) {
                yyerror ("Syntax Error");
             }
             while (p_temp_tc != NULL) {
                if (strcmp (p_temp_tc->tc_name, i1_syntax) == 0) {
                   break;
                }
                p_temp_tc = p_temp_tc->next;
             }
             if (p_temp_tc != NULL) {
                if ((strcmp (p_temp_tc->tc_base, "OCTET STRING") == 0) ||
                    (strcmp (p_temp_tc->tc_base, "IpAddress") == 0) ||
                    (strcmp (p_temp_tc->tc_base, "TimeTicks") == 0) ||
                    (strcmp (p_temp_tc->tc_base, "Counter32") == 0) ||
                    (strcmp (p_temp_tc->tc_base, "Counter64") == 0) ||
                    (strcmp (p_temp_tc->tc_base, "Integer32") == 0) ||
                    (strcmp (p_temp_tc->tc_base, "Unsigned32") == 0) ||
                    (strcmp (p_temp_tc->tc_base, "Gauge32") == 0) ||
                    (strcmp (p_temp_tc->tc_base, "Opaque") == 0)) {
                   yyerror ("Syntax Error");
                }
             }
		tNAME_VALUE        *p_temp_name_value;
                   tOID_TABLE         *p_temp;
                   if (strcmp (i1_syntax, "INTEGER") == 0)
                   {
                     p_temp_name_value = p_name_values;
                     while (p_temp_name_value != NULL)
                     {
                       if (strcmp ($1, p_temp_name_value->name) == 0) {
                          /*Convert the integer into string*/
                           p1_num_text = itoa(p_temp_name_value->value);
                           fprintf(p_defval_file, "\"%s\", \"%s\"\n", i1_object_desc, p1_num_text); 
                           break;
                        } 
                        p_temp_name_value = p_temp_name_value->next;
                      } 
                   }
                   else if (strcmp (i1_syntax, "OBJECT IDENTIFIER") == 0)
                   {
                     fprintf(p_defval_file, "\"%s\", \"%s\"\n", i1_object_desc, yylval.str);
                   }
                   else
                   {
                     tTC_TABLE          *p_temp_tc;
                     p_temp_tc = p_type_defs;
                     while (p_temp_tc != NULL)
                     {
                      if (strcmp (p_temp_tc->tc_name, i1_syntax) == 0)
                      {
                        break;
                      }
                      p_temp_tc = p_temp_tc->next;
                     }
                     if (p_temp_tc != NULL)
                     {
                      if (strcmp (p_temp_tc->tc_base, "INTEGER") == 0)
                      {
                        p_temp_name_value = p_temp_tc->tc_name_value_list;
                        while (p_temp_name_value != NULL)
                        {
                          if (strcmp ($1, p_temp_name_value->name) == 0) {
                            /*Convert the integer into string*/
                             p1_num_text = itoa(p_temp_name_value->value);
                             fprintf(p_defval_file, "\"%s\", \"%s\"\n", i1_object_desc, p1_num_text); 
                             break;
                          } 
                          p_temp_name_value = p_temp_name_value->next;
                        }
                      }
                      if (strcmp (p_temp_tc->tc_base, "OBJECT IDENTIFIER") == 0)
                      {
                        fprintf(p_defval_file, "\"%s\", \"%s\"\n", i1_object_desc, yylval.str);
                      } 
                     }
             }
		    }
        | ObjectDescriptor NUM {
             tTC_TABLE *p_temp_tc = p_type_defs;
             if ((strcmp (i1_syntax, "OCTET STRING") == 0) ||
                 (strcmp (i1_syntax, "IpAddress") == 0) ||
                 (strcmp (i1_syntax, "TimeTicks") == 0) ||
                 (strcmp (i1_syntax, "INTEGER") == 0) ||
                 (strcmp (i1_syntax, "Integet32") == 0) ||
                 (strcmp (i1_syntax, "Unsigned32") == 0) ||
                 (strcmp (i1_syntax, "Gauge32") == 0) ||
                 (strcmp (i1_syntax, "Opaque") == 0)) {
                yyerror ("Syntax Error");
             }
             if (strcmp (i1_syntax, "OBJECT IDENTIFIER") == 0) {
                if (check_validity_of_oid ($1) == INVALID) {
                   yyerror ("Invalid name");
                }
             }
             while (p_temp_tc != NULL) {
                if (strcmp (p_temp_tc->tc_name, i1_syntax) == 0) {
                   break;
                }
                p_temp_tc = p_temp_tc->next;
             }
             if (p_temp_tc != NULL) {
                if ((strcmp (p_temp_tc->tc_base, "OCTET STRING") == 0) ||
                    (strcmp (p_temp_tc->tc_base, "IpAddress") == 0) ||
                    (strcmp (p_temp_tc->tc_base, "TimeTicks") == 0) ||
                    (strcmp (p_temp_tc->tc_base, "INTEGER") == 0) ||
                    (strcmp (p_temp_tc->tc_base, "Counter32") == 0) ||
                    (strcmp (p_temp_tc->tc_base, "Counter64") == 0) ||
                    (strcmp (p_temp_tc->tc_base, "Integer32") == 0) ||
                    (strcmp (p_temp_tc->tc_base, "Unsigned32") == 0) ||
                    (strcmp (p_temp_tc->tc_base, "Gauge32") == 0) ||
                    (strcmp (p_temp_tc->tc_base, "Opaque") == 0)) {
                   yyerror ("Syntax Error");
                }
             }
             tNAME_VALUE        *p_temp_name_value;
                   tOID_TABLE         *p_temp;
                   if (strcmp (i1_syntax, "INTEGER") == 0)
                   {
                     p_temp_name_value = p_name_values;
                     while (p_temp_name_value != NULL)
                     {
                       if (strcmp ($1, p_temp_name_value->name) == 0) {
                          /*Convert the integer into string*/
                           p1_num_text = itoa(p_temp_name_value->value);
                           p4_num_text = itoa($2);
                           fprintf(p_defval_file, "\"%s\", \"%s.%s\"\n", i1_object_desc, p1_num_text,p4_num_text); 
                           break;
                        } 
                        p_temp_name_value = p_temp_name_value->next;
                      } 
                   }
                   else if (strcmp (i1_syntax, "OBJECT IDENTIFIER") == 0)
                   {
                      p4_num_text = itoa($2);
                     fprintf(p_defval_file, "\"%s\", \"%s.%s\"\n", i1_object_desc, yylval.str,p4_num_text);
                   }
                   else
                   {
                     tTC_TABLE          *p_temp_tc;
                     p_temp_tc = p_type_defs;
                     while (p_temp_tc != NULL)
                     {
                      if (strcmp (p_temp_tc->tc_name, i1_syntax) == 0)
                      {
                        break;
                      }
                      p_temp_tc = p_temp_tc->next;
                     }
                     if (p_temp_tc != NULL)
                     {
                      if (strcmp (p_temp_tc->tc_base, "INTEGER") == 0)
                      {
                        p_temp_name_value = p_temp_tc->tc_name_value_list;
                        while (p_temp_name_value != NULL)
                        {
                          if (strcmp ($1, p_temp_name_value->name) == 0) {
                            /*Convert the integer into string*/
                             p1_num_text = itoa(p_temp_name_value->value);
                             p4_num_text = itoa($2);
                             fprintf(p_defval_file, "\"%s\", \"%s.%s\"\n", i1_object_desc, p1_num_text, p4_num_text); 
                             break;
                          } 
                          p_temp_name_value = p_temp_name_value->next;
                        }
                      }
                      if (strcmp (p_temp_tc->tc_base, "OBJECT IDENTIFIER") == 0)
                      {
                        p4_num_text = itoa($2);
                        fprintf(p_defval_file, "\"%s\", \"%s.%s\"\n", i1_object_desc, yylval.str, p4_num_text);
                      } 
                     }
             }
          } 
        | Num Num {
             tTC_TABLE *p_temp_tc = p_type_defs;
             if ((strcmp (i1_syntax, "OCTET STRING") == 0) ||
                 (strcmp (i1_syntax, "IpAddress") == 0) ||
                 (strcmp (i1_syntax, "TimeTicks") == 0) ||
                 (strcmp (i1_syntax, "INTEGER") == 0) ||
                 (strcmp (i1_syntax, "Integer32") == 0) ||
                 (strcmp (i1_syntax, "Unsigned32") == 0) ||
                 (strcmp (i1_syntax, "Gauge32") == 0) ||
                 (strcmp (i1_syntax, "Opaque") == 0)) {
                yyerror ("Syntax Error");
             }
             while (p_temp_tc != NULL) {
                if (strcmp (p_temp_tc->tc_name, i1_syntax) == 0) {
                   break;
                }
                p_temp_tc = p_temp_tc->next;
             }
             if (p_temp_tc != NULL) {
                if ((strcmp (p_temp_tc->tc_base, "OCTET STRING") == 0) ||
                    (strcmp (p_temp_tc->tc_base, "IpAddress") == 0) ||
                    (strcmp (p_temp_tc->tc_base, "TimeTicks") == 0) ||
                    (strcmp (p_temp_tc->tc_base, "INTEGER") == 0) ||
                    (strcmp (p_temp_tc->tc_base, "Integer32") == 0) ||
                    (strcmp (p_temp_tc->tc_base, "Counter32") == 0) ||
                    (strcmp (p_temp_tc->tc_base, "Counter64") == 0) ||
                    (strcmp (p_temp_tc->tc_base, "Unsigned32") == 0) ||
                    (strcmp (p_temp_tc->tc_base, "Gauge32") == 0) ||
                    (strcmp (p_temp_tc->tc_base, "Opaque") == 0)) {
                   yyerror ("Syntax Error");
                }
             }
               p1_num_text = itoa($1);
                p4_num_text = itoa($2);
             printf("\n NUM1 - %s NUM2 - %s", p1_num_text, p4_num_text);
                fprintf(p_defval_file, "\"%s\", \"%s %s\"\n", i1_object_desc, p1_num_text, p4_num_text);
          } 
        | QSTRING {
             tTC_TABLE *p_temp_tc = p_type_defs;
             if ((strcmp (i1_syntax, "TimeTicks") == 0) ||
                 (strcmp (i1_syntax, "OBJECT IDENTIFIER") == 0) ||
                 (strcmp (i1_syntax, "INTEGER") == 0) ||
                 (strcmp (i1_syntax, "Integer32") == 0) ||
                 (strcmp (i1_syntax, "Unsigned32") == 0) ||
                 (strcmp (i1_syntax, "Gauge32") == 0)) {
                yyerror ("Syntax Error");
             }
             while (p_temp_tc != NULL) {
                if (strcmp (p_temp_tc->tc_name, i1_syntax) == 0) {
                   break;
                }
                p_temp_tc = p_temp_tc->next;
             }
             if (p_temp_tc != NULL) {
                if ((strcmp (p_temp_tc->tc_base, "TimeTicks") == 0) ||
                    (strcmp (p_temp_tc->tc_base, "OBJECT IDENTIFIER") == 0) ||
                    (strcmp (p_temp_tc->tc_base, "INTEGER") == 0) ||
                    (strcmp (p_temp_tc->tc_base, "Integer32") == 0) ||
                    (strcmp (p_temp_tc->tc_base, "Counter32") == 0) ||
                    (strcmp (p_temp_tc->tc_base, "Counter64") == 0) ||
                    (strcmp (p_temp_tc->tc_base, "Unsigned32") == 0) ||
                    (strcmp (p_temp_tc->tc_base, "Gauge32") == 0)) {
                   yyerror ("Syntax Error");
                }
             }
             fprintf(p_defval_file,"\"%s\", %s\n",i1_object_desc, yytext);
          } 
        | DValue  
        | '{' Enums 
        { 
            tNAME_VALUE *p_name = NULL;
            UINT1 u1TempSum[16] = { 0 };
            INT4 i4Temp = 0;

            if (strcmp (i1_syntax, "BITS") != 0) {
               yyerror ("Syntax Error\n");
            }
            
            p_name = i1_is_tc ? p_this_tc->tc_name_value_list : p_name_values;
            
            while (p_name != NULL)
            {
                u1TempSum[(p_name->value / 8)] += (1 << (7 - (p_name->value % 8)));
                p_name = p_name->next;
            }

            for (i4Temp = 0; i4Temp < 16; i4Temp++)
            {
                if (u1EnumSum[i4Temp] > u1TempSum[i4Temp])
                {
                    yyerror ("Default Value not within range");
                }
            }

            p1_num_text = itoa (u1EnumSum[0]);
            fprintf(p_defval_file, "\"%s\", \"%s", i1_object_desc, p1_num_text);

            for (i4Temp = 1; i4Temp < 16; i4Temp++)
            {
                if (u1EnumSum[i4Temp] != 0)
                {
                    p1_num_text = itoa (u1EnumSum[i4Temp]);
                    fprintf(p_defval_file, ":%s", p1_num_text);
                }
            }

            fprintf (p_defval_file, "\"\n");

            for (i4Temp = 0; i4Temp < 16; i4Temp++)
            {
                u1EnumSum[i4Temp] = 0;
            }

            for (i4Temp = 0; i4Temp < 128; i4Temp++)
            {
                u1EnumList[i4Temp] = 0;
            }
         } '}'
		  ;

Syntax: SyntaxType { 
			  if (check_validity_of_syntax ($1) == INVALID) {
			     yyerror ("Invalid Syntax Type");
			  }
           if (strcmp (i1_row_name, $1) != 0) {
              strcpy (i1_syntax, $1);
           } else {
              strcpy (i1_syntax, "Aggregate");
           }
           if (i1_is_tc == MID_TRUE) {
              p_this_tc = ALLOC_TC_MEM;
              if (p_this_tc == NULL) {
                 fprintf (stderr, "Memory allocation failure\n");
                 yyerror ("");
              }
              strcpy (p_this_tc->tc_base, i1_syntax);
              p_this_tc->tc_enum_type = MID_FALSE;
              p_this_tc->tc_name_value_list = NULL;
           }
           else {
              i1_enum_type = MID_FALSE;
           }
		  } SubType

		| NAME {
                if (strcmp ($1, "INTEGER") && strcmp ($1, "BITS") 
                    && strcmp ($1, "RowStatus")) {
                    p_temp_import_list = p_import_list;
                    while(p_temp_import_list != NULL)
                    {
                        if (!(strcmp(p_temp_import_list->name,$1)))
                        {
                            i4_check_flag = 1;
                        }
                        p_temp_import_list = p_temp_import_list->next;
                    }
                    if (i4_check_flag == 0)
                    {
                        yyerror ("Syntax Error");
                    }
                    i4_check_flag = 0;
			  }

/**********
		     strcpy (i1_syntax, $1); *********/
                     strcpy(i1_syntax,"INTEGER");
           if (i1_is_tc == MID_TRUE) {
              p_this_tc = ALLOC_TC_MEM;
              if (p_this_tc == NULL) {
                 fprintf (stderr, "Memory allocation failure\n");
                 yyerror ("");
              }
              strcpy (p_this_tc->tc_base, i1_syntax);
              p_this_tc->tc_enum_type = MID_TRUE;
              p_this_tc->tc_name_value_list = NULL;
           }
           else {
              i1_enum_type = MID_TRUE;
           }
		  } '{' {  if (!strcmp ($1, "BITS"))
                   { 
                       i4_check_for_fisrt_enum = 1;
                       /*printf ("AT NAME : %d,Syntax : %s\n",i4_check_for_fisrt_enum,$1);*/
                   }    
                }Enums '}' {
           tNAME_VALUE *p_temp, *p_prev;

           if (!strcmp (i1_syntax, "BITS")) {
              p_temp = i1_is_tc ? p_this_tc->tc_name_value_list : p_name_values;
              if (p_temp->value != 0) {
                 yyerror ("BITS: Starting value must be 0");
              }
              while (p_temp->next != NULL) {
                 p_prev = p_temp;
                 p_temp = p_temp->next;
                 if (p_temp->value != p_prev->value + 1) {
                    yyerror ("BITS: Enumerated Values must be contiguious");
                 }
              }
           }

           if (strcmp ($1, "BITS") == 0)
           {
               strcpy (i1_syntax, "BITS");
           }
        }

		| OCTET STRING { 
		     strcpy (i1_syntax, "OCTET STRING"); 
           if (i1_is_tc == MID_TRUE) {
              p_this_tc = ALLOC_TC_MEM;
              if (p_this_tc == NULL) {
                 fprintf (stderr, "Memory allocation failure\n");
                 yyerror ("");
              }
              strcpy (p_this_tc->tc_base, i1_syntax);
              p_this_tc->tc_enum_type = MID_FALSE;
              p_this_tc->tc_name_value_list = NULL;
           }
           else {
              i1_enum_type = MID_FALSE;
           }
		  } SubType
		| OBJECT IDENTIFIER {
		     strcpy (i1_syntax, "OBJECT IDENTIFIER");
           if (i1_is_tc == MID_TRUE) {
              p_this_tc = ALLOC_TC_MEM;
              if (p_this_tc == NULL) {
                 fprintf (stderr, "Memory allocation failure\n");
                 yyerror ("");
              }
              strcpy (p_this_tc->tc_base, i1_syntax);
              p_this_tc->tc_enum_type = MID_FALSE;
              p_this_tc->tc_name_value_list = NULL;
           }
           else {
              i1_enum_type = MID_FALSE;
           }
		  }
		| SEQUENCE OF SyntaxType { 
           tTC_TABLE *p_temp_tc = p_type_defs;
           /* code added to give error when another table is defined *
	    * before definig tha all objects of previous table                 *
	   */
	   if ( p_table_entries != NULL )
	   {
	   fprintf(stderr,"ERROR: before defining all objects of previous table, defination of %s Table.\n",$3);
			   yyerror("");
	   }
           if (i1_is_tc == MID_TRUE) {
              yyerror ("Syntax Error");
           }
           i1_is_row_name = MID_TRUE;
			  strcpy (i1_row_name, $3);
		     strcpy (i1_syntax, "Aggregate"); 
			  if (store_oid (i1_object_desc) == ERROR) {
			     yyerror ("");
			  }
           p_this_tc = ALLOC_TC_MEM;
			  if (p_this_tc == NULL) {
              fprintf (stderr, "Memory allocation failure\n");
			     yyerror ("");
			  }
           strcpy (p_this_tc->tc_name, $3);
           strcpy (p_this_tc->tc_base, "Rowname");
           p_this_tc->tc_enum_type = MID_FALSE;
           p_this_tc->tc_name_value_list = NULL;
           p_this_tc->next = NULL;
           if (p_type_defs == NULL) {
              p_type_defs = p_this_tc;
           }
           else {
              p_temp_tc = p_type_defs; 
              while (p_temp_tc->next != NULL) {
                 p_temp_tc = p_temp_tc->next;
              }
              p_temp_tc->next = p_this_tc;
           }
		  }
		;

SubType: '(' { 
            tTC_TABLE *p_temp_tc = p_type_defs;
               /*Set the flag to zero*/
               i4_flag2 = 0;
			   if ( !strcmp (i1_syntax, "OCTET STRING") ||
					  !strcmp (i1_syntax, "IpAddress") ||
					  !strcmp (i1_syntax, "BITS") ||
					  !strcmp (i1_syntax, "Counter32") ||
					  !strcmp (i1_syntax, "Counter64") ||
					  !strcmp (i1_syntax, "TimeTicks") ||
					  !strcmp (i1_syntax, "Opaque") ||
					  !strcmp (i1_syntax, "DisplayString")) {
					yyerror ("Syntax Error");
		      }
            while (p_temp_tc != NULL) {
               if (strcmp (p_temp_tc->tc_name, i1_syntax) == 0) {
                  i1_enum_type = p_temp_tc->tc_enum_type;
                  break;
               }
               p_temp_tc = p_temp_tc->next;
            }
            if (p_temp_tc != NULL) {
               if ((strcmp (p_temp_tc->tc_base, "OCTET STRING") == 0) ||
                   (strcmp (p_temp_tc->tc_base, "IpAddress") == 0) ||
					    (strcmp (p_temp_tc->tc_base, "BITS") == 0) ||
					    (strcmp (p_temp_tc->tc_base, "Counter32") == 0) ||
					    (strcmp (p_temp_tc->tc_base, "Counter64") == 0) ||
					    (strcmp (p_temp_tc->tc_base, "TimeTicks") == 0) ||
                   (strcmp (p_temp_tc->tc_base, "DisplayString") == 0) ||
                   (strcmp (p_temp_tc->tc_base, "Opaque") == 0)) {
                  yyerror ("Syntax Error");
               }
            }
         } Ranges { while(i4_dummy < i4_dupcount)
                    {
                        i4_dupval[i4_dummy] = 0;
                        i4_dummy++;
                    }
                    i4_dummy = 0;
                    i4_dupcount = 0; } ')'
		 | '(' SIZE {
            tTC_TABLE *p_temp_tc = p_type_defs;
               /*Set the flag to zero*/
               i4_flag2 = 0;
			   if ( !strcmp (i1_syntax, "INTEGER") ||
					  !strcmp (i1_syntax, "IpAddress") ||
					  !strcmp (i1_syntax, "BITS") ||
					  !strcmp (i1_syntax, "TimeTicks") ||
					  !strcmp (i1_syntax, "Integer32") ||
					  !strcmp (i1_syntax, "Counter32") ||
					  !strcmp (i1_syntax, "Counter64") ||
					  !strcmp (i1_syntax, "Gauge32")) {
					yyerror ("Syntax Error");
		      }
            while (p_temp_tc != NULL) {
               if (strcmp (p_temp_tc->tc_name, i1_syntax) == 0) {
                  i1_enum_type = p_temp_tc->tc_enum_type;
                  break;
               }
               p_temp_tc = p_temp_tc->next;
            }
            if (p_temp_tc != NULL) {
               if ((strcmp (p_temp_tc->tc_base, "IpAddress") == 0) ||
					    (strcmp (p_temp_tc->tc_base, "BITS") == 0) ||
                   (strcmp (p_temp_tc->tc_base, "INTEGER") == 0) ||
                   (strcmp (p_temp_tc->tc_base, "TimeTicks") == 0) ||
                   (strcmp (p_temp_tc->tc_base, "Integer32") == 0) ||
                   (strcmp (p_temp_tc->tc_base, "Counter32") == 0) ||
                   (strcmp (p_temp_tc->tc_base, "Counter64") == 0) ||
                   (strcmp (p_temp_tc->tc_base, "Gauge32") == 0) ||
                   (strcmp (p_temp_tc->tc_base, "Opaque") == 0)) {
                  yyerror ("Syntax Error");
               }
            }
         }
           '(' Ranges { while(i4_dummy < i4_dupcount)
               {
                    i4_dupval[i4_dummy] = 0;
                    i4_dummy++;
               }

                        i4_dummy=0;
                        i4_dupcount=0;} ')' ')'
		 |
		 ;

Ranges: {i4_range[0]= 0; i4_range[1] = 0; i4_range[2] = 0; i4_range[3] = 0;}Range
      | Ranges {i4_flag1 = 1;}'|' Range {
          /*Checks whether the range is overlapping*/
          if (i4_range[0] <= i4_range[2])
          {   /*printf ("Range1: %d,Range2 : %d\n",i4_range[1],i4_range[2]);
              printf ("Flag : %d,i1_syntax : %s\n",i4_flag2,i1_syntax);*/
              if ((i4_range[1] >= i4_range[2]) 
                  && i4_flag2 == 0 && (strcmp (i1_syntax, "Unsigned32") != 0))
              {
                  printf ("\nRanges Overlap\n");
                  yyerror ("Syntax Error\n");
              }
          }
          else if (i4_range[0] >= i4_range[2])
          {  
              if ((i4_range[0] >= i4_range[2]) && (i4_range[1] >= i4_range[2]))
              {
                  /*printf ("\nwarning: ranges not in ascending order\n");*/
                  if ((i4_range[3] >= i4_range[0]) || (i4_range[3] >= i4_range[1])
                      && i4_flag2 == 0 && (strcmp (i1_syntax, "Unsigned32") != 0))
                  {   
                      printf ("\nRanges Overlap\n");
                      yyerror ("Syntax Error\n");
                  }
              }
              else if ((i4_range[0] <= i4_range[2]) && (i4_range[1] >= i4_range[2])
                        && i4_flag2 == 0 && (strcmp (i1_syntax, "Unsigned32") != 0))
              {
                  printf ("\nRanges Overlap\n");
                  yyerror ("Syntax Error\n");
              }    
          }


          /*Setting the array elements to zero*/
          for (i4_dummy = 0; i4_dummy < 4; i4_dummy++)
          {
              i4_range[i4_dummy] = 0;
          }
          i4_dummy = 0;
          i4_flag2 = 0;
          i4_flag1 = 0;
      }
      ;

Range: Value {i4_flag2 = 1;/*checks whether range is of type (num|min..max)*/
              i4_dupval[i4_dupcount] = $1;
              i4_dupcount++;
              /*checking whether values are repeated*/
              for (i4_dup = 0;i4_dup <= i4_dupcount-1;i4_dup++)
              {
                  /*printf ("count %d: %d\n",i4_dup,i4_dupval[i4_dup]);*/
                  if (i4_dupval[i4_dupcount-1] == i4_dupval[i4_dup])
                  {
                      i4_dupcounter++;
                      if (i4_dupcounter == 2)
                      {
                          printf ("\nDuplicate value found\n");
                          yyerror ("Syntax Error");
                       }
                  }
              }i4_dupcounter=0;}    
     | Value { i4Count++; } '.' '.'  Value {
         /*Values are stored if the flag is not set*/
         if (i4_flag1 == 0 && (strcmp (i1_syntax, "Unsigned32") != 0))
         {    
             i4_range[0] = $1;
             i4_range[1] = $5;

         }
         /*Values are stored if the flag is set*/
         if (i4_flag1 == 1 && (strcmp (i1_syntax, "Unsigned32") != 0))
         {
             i4_range[2] = $1;
             i4_range[3] = $5;
         }
         if (i1IsInt == 1)
         {
             i1IsInt = 0;
             i4dummymin = 0;
             u4dummymax = 0;
         }    
         }   
	  ;

Value: '-' NUM {
          tTC_TABLE *p_temp_tc = p_type_defs;
          i1IsInt = 1;
          if ((strcmp (i1_syntax, "OCTET STRING") == 0) ||
              (strcmp (i1_syntax, "DisplayString") == 0) ||
              (strcmp (i1_syntax, "OBJECT IDENTIFIER") == 0) ||
              (strcmp (i1_syntax, "IpAddress") == 0) ||
              (strcmp (i1_syntax, "TimeTicks") == 0) ||
              (strcmp (i1_syntax, "Unsigned32") == 0) ||
              (strcmp (i1_syntax, "Counter32") == 0) ||
              (strcmp (i1_syntax, "Counter64") == 0) ||
              (strcmp (i1_syntax, "Gauge32") == 0) ||
              (strcmp (i1_syntax, "Opaque") == 0)) {
              yyerror ("Syntax Error");
          }
          i4_len = strlen(yytext);
          /*Checking whether the number is greater than 10 digits*/
          if(i4_len >10)
          {
              yyerror("ERROR:Out of Range [Ref:RFC 2578 (Sec 7.1.1)]\n");
          }
          /*Checking whether the number is equal to 10 digits,
            if yes,preform the range check*/
          if(i4_len == 10)
          {
              /*Copy the digit which is in string format into array*/
              strcpy(p_num_text,yytext);
              /*Convert the string into integer*/
              i4_first_num = atoi(p_num_text);
              /*Convert the integer into string*/
              p1_num_text = itoa(i4_first_num);
              p2_num_text = p_num_text;
              /*Check whether both strings are equal*/
              if (strcmp(p2_num_text,p1_num_text))
              {
                  /*Check whether the string is equal to max negative value 
                   *of integer (i.e -2147483648)*/
                  if (strcmp(p2_num_text,p3_num_text))
                  {    
                      yyerror("ERROR:Out of Range [Ref:RFC 2578 (Sec 7.1.1)]\n");
                  }
              }
          }
          while (p_temp_tc != NULL) {
              if (strcmp (p_temp_tc->tc_name, i1_syntax) == 0) {
                  i1_enum_type = p_temp_tc->tc_enum_type;
                  break;
              }
              p_temp_tc = p_temp_tc->next;
          }
          if (p_temp_tc != NULL) {
              if ((strcmp (p_temp_tc->tc_base, "OCTET STRING") == 0) ||
                  (strcmp (p_temp_tc->tc_base, "DisplayString") == 0) ||
                 (strcmp (p_temp_tc->tc_base, "OBJECT IDENTIFIER") == 0) ||
                 (strcmp (p_temp_tc->tc_base, "IpAddress") == 0) ||
                 (strcmp (p_temp_tc->tc_base, "TimeTicks") == 0) ||
                 (strcmp (p_temp_tc->tc_base, "Unsigned32") == 0) ||
                 (strcmp (p_temp_tc->tc_base, "Counter32") == 0) ||
                 (strcmp (p_temp_tc->tc_base, "Counter64") == 0) ||
                 (strcmp (p_temp_tc->tc_base, "Gauge32") == 0) ||
                 (strcmp (p_temp_tc->tc_base, "Opaque") == 0)) {
                yyerror ("Syntax Error");
             }
          }
          if (i1_is_defval == MID_TRUE) {
             if (check_validity_of_value (i1_syntax, i1_enum_type, -$2) == INVALID) {
                yyerror ("Invalid default value");
             }
          }
          else {
             if (store_value (i1_is_tc, -$2) == ERROR) {
                yyerror ("");
             }
          }
	    }
     | Num {
         tTC_TABLE *p_temp_tc = p_type_defs;
         i4_len = strlen(yytext);
         /*Checking whether the number is greater than 10 digits*/
         if(i4_len >10)
         {
             yyerror("ERROR:Out of Range [Ref:RFC 2578 (Sec 7.1.1)]\n");
         }
         /*Checking whether the number is equal to 10 digits,
           if yes,preform the range check*/
         if((strcmp (i1_syntax, "Integer32") == 0)||(strcmp (i1_syntax, "INTEGER") == 0))
         {    
             if(i4_len==10)
             {
                 /*Copy the digit which is in string format into array*/
                 strcpy(p_num_text,yytext);
                 /*Convert the string into integer*/
                 i4_first_num = atoi(p_num_text);
                 /*Convert the integer into string*/
                 p1_num_text = itoa(i4_first_num);
                 p2_num_text = p_num_text;
                 /*Check whether both strings are equal*/
                 if (strcmp(p1_num_text,p2_num_text))
                 {
                     yyerror("ERROR:Out of Range [Ref:RFC 2578 (Sec 7.1.1)]\n");
                 }
             }
         }
         if(strcmp (i1_syntax, "Unsigned32") == 0)
         {
             if(i4_len==10)
             {
                 strcpy(p_num_text,yytext);
                 /*Convert the string into integer*/
                 i8_check_for_range = atoll(p_num_text);
                 /*Check whether the number is within the range*/
                 if (i8_check_for_range >= i8_max_unsigned_range)
                 {
                     yyerror("ERROR:Out of Range [Ref:RFC 2578 (Sec 7.1.1)]\n");
                 }
             }
         }
         if(strcmp (i1_syntax, "OCTET STRING") == 0)
         {
             if( $1 > 65535)
             {
                 yyerror("ERROR:Out of Range [Ref:RFC 2578 (Sec 7.1.2)]\n");
             }
         }
         /*stores min value of range*/
         if (i4Count == 0)
         {
             i4dummymin = $1;
         }
         /*stores max value of range*/ 
         /*i1IsInt is used to avoid range checking of 1..'ffffffff'h case*/
         if (i4Count == 1 && i1IsInt == 0)
         {
             u4dummymax = $1;
             /*Checks whether min value of range is greater than max value
              *if greater,returns error*/
             if (i4dummymin > u4dummymax)
             {
                 printf("\rMin value of range is greater than Max value\n");
                 yyerror ("Syntax Error");
             }
             i4dummymin = 0;
             u4dummymax = 0;
              i4Count = 0;
          }
          if (strcmp (i1_syntax, "OBJECT IDENTIFIER") == 0) {
             yyerror ("Syntax Error");
          }
          while (p_temp_tc != NULL) {
             if (strcmp (p_temp_tc->tc_name, i1_syntax) == 0) {
                i1_enum_type = p_temp_tc->tc_enum_type;
                break;
             }
             p_temp_tc = p_temp_tc->next;
          }
          if (p_temp_tc != NULL) {
             if (strcmp (p_temp_tc->tc_base, "OBJECT IDENTIFIER") == 0) {
                yyerror ("Syntax Error");
             }
          }
          if (i1_is_defval == MID_TRUE) {
             if (check_validity_of_value (i1_syntax, i1_enum_type, $1) == INVALID) {
                yyerror ("Invalid default value");
             }
          }
          else {
             if (store_value (i1_is_tc, $1) == ERROR) {
                yyerror ("");
             }
          }
	    }
	  | HEXSTRING {
			 INT4 i4_intvalue = 0;
			 INT1 *pi1_hex_string = $1;
          tTC_TABLE *p_temp_tc = p_type_defs;
          i1IsInt = 1;
          if ((strlen($1)-3)%2 != 0)
          {
              yyerror ("\nLength of hexadecimal string is not a multiple of 2\n");
          }    
          if (!strcmp (i1_syntax, "OBJECT IDENTIFIER")) {
             yyerror ("Syntax Error");
          }
          while (p_temp_tc != NULL) {
             if (strcmp (i1_syntax, p_temp_tc->tc_name) == 0) {
                i1_enum_type = p_temp_tc->tc_enum_type;
                break;
             }
             p_temp_tc = p_temp_tc->next;
          }
          if (p_temp_tc != NULL) {
             if (!strcmp (p_temp_tc->tc_base, "OBJECT IDENTIFIER")) {
                yyerror ("Syntax Error");
             }
          }
			 pi1_hex_string[strlen (pi1_hex_string) - 2] = '\0';
			 pi1_hex_string++;
			 if (strlen (pi1_hex_string) > 8) {
			    pi1_hex_string += strlen (pi1_hex_string) - 8;
			 }
			 sscanf (pi1_hex_string, "%x", &i4_intvalue);
          if (i1_is_defval == MID_TRUE) {
             if (check_validity_of_value (i1_syntax, i1_enum_type, i4_intvalue) == INVALID) {
                yyerror ("Invalid Default Value");
             }
          } else {
             if (store_value (i1_is_tc, i4_intvalue) == ERROR) {
                yyerror ("");
             }
          }
	    }
	  | BINSTRING {
			 INT4 i4_intvalue = 0;
			 INT1 *pi1_bin_string = $1;
          tTC_TABLE *p_temp_tc = p_type_defs;
          i1IsInt = 1;
          if ((strlen($1)-3)%8 != 0)
          {
              yyerror ("\nlength of binary string is not a multiple of 8\n");
          } 
          if (!strcmp (i1_syntax, "OBJECT IDENTIFIER")) {
             yyerror ("Syntax Error");
          }
          while (p_temp_tc != NULL) {
             if (strcmp (i1_syntax, p_temp_tc->tc_name) == 0) {
                i1_enum_type = p_temp_tc->tc_enum_type;
                break;
             }
             p_temp_tc = p_temp_tc->next;
          }
          if (p_temp_tc != NULL) {
             if (!strcmp (p_temp_tc->tc_base, "OBJECT IDENTIFIER")) {
                yyerror ("Syntax Error");
             }
          }
			 pi1_bin_string[strlen (pi1_bin_string) - 2] = '\0';
			 pi1_bin_string++;
			 if (strlen (pi1_bin_string) > 32) {
			    pi1_bin_string += strlen (pi1_bin_string) - 32;
			 }
			 for (i4_intvalue = 0; *pi1_bin_string; pi1_bin_string++) {
			    i4_intvalue <<= 1;
				 i4_intvalue += *pi1_bin_string - '0';
			 }
          if (i1_is_defval == MID_TRUE) {
             if (check_validity_of_value (i1_syntax, i1_enum_type, i4_intvalue) == INVALID) {
                yyerror ("Invalid Default Value");
             }
          } else {
             if (store_value (i1_is_tc, i4_intvalue) == ERROR) {
                yyerror ("");
             }
          }
	    }
	  ;

DValue: '-' NUM {
          tTC_TABLE *p_temp_tc = p_type_defs;

          if ((strcmp (i1_syntax, "OCTET STRING") == 0) ||
              (strcmp (i1_syntax, "DisplayString") == 0) ||
              (strcmp (i1_syntax, "OBJECT IDENTIFIER") == 0) ||
              (strcmp (i1_syntax, "IpAddress") == 0) ||
              (strcmp (i1_syntax, "TimeTicks") == 0) ||
              (strcmp (i1_syntax, "Unsigned32") == 0) ||
              (strcmp (i1_syntax, "Counter32") == 0) ||
              (strcmp (i1_syntax, "Counter64") == 0) ||
              (strcmp (i1_syntax, "Gauge32") == 0) ||
              (strcmp (i1_syntax, "Opaque") == 0)) {
              yyerror ("Syntax Error");
          }
          i4_len = strlen(yytext);
          /*Checking whether the number is greater than 10 digits*/
          if(i4_len >10)
          {
              yyerror("ERROR:Out of Range [Ref:RFC 2578 (Sec 7.1.1)]\n");
          }
          /*Checking whether the number is equal to 10 digits,
            if yes,preform the range check*/
          if(i4_len == 10)
          {
              /*Copy the digit which is in string format into array*/
              strcpy(p_num_text,yytext);
              /*Convert the string into integer*/
              i4_first_num = atoi(p_num_text);
              /*Convert the integer into string*/
              p1_num_text = itoa(i4_first_num);
              p2_num_text = p_num_text;
              /*Check whether both strings are equal*/
              if (strcmp(p2_num_text,p1_num_text))
              {
                  /*Check whether the string is equal to max negative value 
                   *of integer (i.e -2147483648)*/
                  if (strcmp(p2_num_text,p3_num_text))
                  {    
                      yyerror("ERROR:Out of Range [Ref:RFC 2578 (Sec 7.1.1)]\n");
                  }
              }
          }
          while (p_temp_tc != NULL) {
              if (strcmp (p_temp_tc->tc_name, i1_syntax) == 0) {
                  i1_enum_type = p_temp_tc->tc_enum_type;
                  break;
              }
              p_temp_tc = p_temp_tc->next;
          }
          if (p_temp_tc != NULL) {
              if ((strcmp (p_temp_tc->tc_base, "OCTET STRING") == 0) ||
                  (strcmp (p_temp_tc->tc_base, "DisplayString") == 0) ||
                 (strcmp (p_temp_tc->tc_base, "OBJECT IDENTIFIER") == 0) ||
                 (strcmp (p_temp_tc->tc_base, "IpAddress") == 0) ||
                 (strcmp (p_temp_tc->tc_base, "TimeTicks") == 0) ||
                 (strcmp (p_temp_tc->tc_base, "Unsigned32") == 0) ||
                 (strcmp (p_temp_tc->tc_base, "Counter32") == 0) ||
                 (strcmp (p_temp_tc->tc_base, "Counter64") == 0) ||
                 (strcmp (p_temp_tc->tc_base, "Gauge32") == 0) ||
                 (strcmp (p_temp_tc->tc_base, "Opaque") == 0)) {
                yyerror ("Syntax Error");
             }
          }
          if (i1_is_defval == MID_TRUE) {
             if (check_validity_of_value (i1_syntax, i1_enum_type, -$2) == INVALID) {
                yyerror ("Invalid default value");
             }
             else {
             /*Convert the integer into string*/
             p1_num_text = itoa($2);
             fprintf(p_defval_file, "\"%s\", \"-%s\"\n", i1_object_desc, p1_num_text); 
             }
          }
          else {
             if (store_value (i1_is_tc, -$2) == ERROR) {
                yyerror ("");
             }
          }
	    }
     | Num {
         tTC_TABLE *p_temp_tc = p_type_defs;
         i4_len = strlen(yytext);
         /*Checking whether the number is greater than 10 digits*/
         if(i4_len >10)
         {
             yyerror("ERROR:Out of Range [Ref:RFC 2578 (Sec 7.1.1)]\n");
         }
         /*Checking whether the number is equal to 10 digits,
           if yes,preform the range check*/
         if((strcmp (i1_syntax, "Integer32") == 0)||(strcmp (i1_syntax, "INTEGER") == 0))
         {    
             if(i4_len==10)
             {
                 /*Copy the digit which is in string format into array*/
                 strcpy(p_num_text,yytext);
                 /*Convert the string into integer*/
                 i4_first_num = atoi(p_num_text);
                 /*Convert the integer into string*/
                 p1_num_text = itoa(i4_first_num);
                 p2_num_text = p_num_text;
                 /*Check whether both strings are equal*/
                 if (strcmp(p1_num_text,p2_num_text))
                 {
                     yyerror("ERROR:Out of Range [Ref:RFC 2578 (Sec 7.1.1)]\n");
                 }
             }
         }
         if(strcmp (i1_syntax, "Unsigned32") == 0)
         {
             if(i4_len==10)
             {
                 strcpy(p_num_text,yytext);
                 /*Convert the string into integer*/
                 i8_check_for_range = atoll(p_num_text);
                 /*Check whether the number is within the range*/
                 if (i8_check_for_range >= i8_max_unsigned_range)
                 {
                     yyerror("ERROR:Out of Range [Ref:RFC 2578 (Sec 7.1.1)]\n");
                 }
             }
         }
         if(strcmp (i1_syntax, "OCTET STRING") == 0)
         {
             if( $1 > 65535)
             {
                 yyerror("ERROR:Out of Range [Ref:RFC 2578 (Sec 7.1.2)]\n");
             }
         }
         /*stores min value of range*/
         if (i4Count == 0)
         {
             i4dummymin = $1;
         }
         /*stores max value of range*/ 
         if (i4Count == 1)
         {
             u4dummymax = $1;
             /*Checks whether min value of range is greater than max value
              *if greater,returns error*/
             if (i4dummymin > u4dummymax)
             {
                 printf("\rMin value of range is greater than Max value\n");
                 yyerror ("Syntax Error");
             }
             i4dummymin = 0;
             u4dummymax = 0;
              i4Count = 0;
          }
          if (strcmp (i1_syntax, "OBJECT IDENTIFIER") == 0) {
             yyerror ("Syntax Error");
          }
          while (p_temp_tc != NULL) {
             if (strcmp (p_temp_tc->tc_name, i1_syntax) == 0) {
                i1_enum_type = p_temp_tc->tc_enum_type;
                break;
             }
             p_temp_tc = p_temp_tc->next;
          }
          if (p_temp_tc != NULL) {
             if (strcmp (p_temp_tc->tc_base, "OBJECT IDENTIFIER") == 0) {
                yyerror ("Syntax Error");
             }
          }
          if (i1_is_defval == MID_TRUE) {
             if (check_validity_of_value (i1_syntax, i1_enum_type, $1) == INVALID) {
                yyerror ("Invalid default value");
             }
          else {
             /*Convert the integer into string*/
             p1_num_text = itoa($1);
             fprintf(p_defval_file, "\"%s\", \"%s\"\n", i1_object_desc, p1_num_text); 
           }
          }
          else {
             if (store_value (i1_is_tc, $1) == ERROR) {
                yyerror ("");
             }
          }
	    }
	  | HEXSTRING {
		  INT4 i4_intvalue = 0;
		  UINT4 i4_uintvalue = 0;
		  INT4 i4Cnt = 0, i4Count = 0;
		  INT1 hex1[3];
		  INT1 *pi1_hex_string = $1;
		  tTC_TABLE *p_temp_tc = p_type_defs;

		  if ((strlen($1)-3)%2 != 0)
		  {
			  yyerror ("\nLength of hexadecimal string is not a multiple of 2\n");
		  }    
		  if (!strcmp (i1_syntax, "OBJECT IDENTIFIER")) {
			  yyerror ("Syntax Error");
		  }
		  while (p_temp_tc != NULL) {
			  if (strcmp (i1_syntax, p_temp_tc->tc_name) == 0) {
				  i1_enum_type = p_temp_tc->tc_enum_type;
				  break;
			  }
			  p_temp_tc = p_temp_tc->next;
		  }
		  if (p_temp_tc != NULL) {
			  if (!strcmp (p_temp_tc->tc_base, "OBJECT IDENTIFIER")) {
				  yyerror ("Syntax Error");
			  }
		  }
		  pi1_hex_string[strlen (pi1_hex_string) - 2] = '\0';
		  pi1_hex_string++;
		  if (i1_is_defval == MID_TRUE) {
			  if(!strcmp(i1_syntax, "MacAddress"))
			  {
				  fprintf(p_defval_file, "\"%s\", \"%s\"\n", i1_object_desc, pi1_hex_string); 
			  }
			  else if(!strcmp(i1_syntax, "IpAddress"))
			  {
				  sscanf (pi1_hex_string, "%x", &i4_uintvalue);
				  /*Convert the integer into string*/
				  p1_num_text = uitoa(i4_uintvalue);
				  fprintf(p_defval_file, "\"%s\", \"%s\"\n", i1_object_desc, p1_num_text); 
			  }
			  else {
				  if (strlen (pi1_hex_string) > 8) {
					  pi1_hex_string += strlen (pi1_hex_string) - 8;
				  }
				  sscanf (pi1_hex_string, "%x", &i4_intvalue);
				  if (check_validity_of_value (i1_syntax, i1_enum_type, i4_intvalue) == INVALID) {
					  yyerror ("Invalid Default Value");
				  }
				  else {
					  /*Convert the integer into string*/
					  p1_num_text = itoa(i4_intvalue);
					  fprintf(p_defval_file, "\"%s\", \"%s\"\n", i1_object_desc, p1_num_text); 
				  }
			  }
		  } else {
			  if(strcmp(i1_syntax, "IpAddress")){
				  if (store_value (i1_is_tc, i4_intvalue) == ERROR) {
					  yyerror ("");
				  }
			  }
		  }
	  }
	  | BINSTRING {
		  INT4 i4_intvalue = 0;
		  UINT4 i4_uintvalue = 0;
		  INT4 i4Cnt = 0, i4Count = 0;
		  INT1 bin_array[5];
		  INT1 *binary;
		  INT1 *pi1_bin_string = $1;
		  tTC_TABLE *p_temp_tc = p_type_defs;
		  if ((strlen($1)-3)%8 != 0)
		  {
			  yyerror ("\nlength of binary string is not a multiple of 8\n");
		  } 
		  if (!strcmp (i1_syntax, "OBJECT IDENTIFIER")) {
			  yyerror ("Syntax Error");
		  }
		  while (p_temp_tc != NULL) {
			  if (strcmp (i1_syntax, p_temp_tc->tc_name) == 0) {
				  i1_enum_type = p_temp_tc->tc_enum_type;
				  break;
			  }
			  p_temp_tc = p_temp_tc->next;
		  }
		  if (p_temp_tc != NULL) {
			  if (!strcmp (p_temp_tc->tc_base, "OBJECT IDENTIFIER")) {
				  yyerror ("Syntax Error");
			  }
		  }
		  pi1_bin_string[strlen (pi1_bin_string) - 2] = '\0';
		  pi1_bin_string++;
		  if (i1_is_defval == MID_TRUE) {
			  if(!strcmp(i1_syntax, "MacAddress"))
			  {
				  fprintf(p_defval_file, "\"%s\", \"", i1_object_desc); 
				  while(i4Cnt<strlen(pi1_bin_string))
				  {
					  i4Count = 0;
					  bin_array[i4Count++] = pi1_bin_string[i4Cnt++];
					  bin_array[i4Count++] = pi1_bin_string[i4Cnt++];
					  bin_array[i4Count++] = pi1_bin_string[i4Cnt++];
					  bin_array[i4Count++] = pi1_bin_string[i4Cnt++];
					  bin_array[i4Count] = '\0';
					  binary = bin_array;
					  for (i4_intvalue = 0; *binary; binary++) {
						  i4_intvalue <<= 1;
						  i4_intvalue += *binary - '0';
					  }
					  fprintf(p_defval_file, "%x", i4_intvalue); 
				  }
				  fprintf(p_defval_file, "\"\n"); 
			  }
			  else if(!strcmp(i1_syntax, "IpAddress"))
			  {
				  for (i4_uintvalue = 0; *pi1_bin_string; pi1_bin_string++) {
					  i4_uintvalue <<= 1;
					  i4_uintvalue += *pi1_bin_string - '0';
				  }
				  /*Convert the integer into string*/
				  p1_num_text = uitoa(i4_uintvalue);
				  fprintf(p_defval_file, "\"%s\", \"%s\"\n", i1_object_desc, p1_num_text); 
			  }
			  else {
				  if (strlen (pi1_bin_string) > 32) {
					  pi1_bin_string += strlen (pi1_bin_string) - 32;
				  }
				  for (i4_intvalue = 0; *pi1_bin_string; pi1_bin_string++) {
					  i4_intvalue <<= 1;
					  i4_intvalue += *pi1_bin_string - '0';
				  }
				  if (check_validity_of_value (i1_syntax, i1_enum_type, i4_intvalue) == INVALID) {
					  yyerror ("Invalid Default Value");
				  }
				  else {
					  /*Convert the integer into string*/
					  p1_num_text = itoa(i4_intvalue);
					  fprintf(p_defval_file, "\"%s\", \"%s\"\n", i1_object_desc, p1_num_text); 
				  }
			  }
		  } else {
			  if(strcmp(i1_syntax, "IpAddress")){
				  if (store_value (i1_is_tc, i4_intvalue) == ERROR) {
					  yyerror ("");
				  }
			  }
		  }
	    }
	  ;

Enums: Enum
     | Enums ',' Enum
     | '{' Enum '}'
	  ;

Enum: NAME '(' AnyNum ')'{ 
         tNAME_VALUE *p_temp_name, *p_this_named_value;
         if((i4_check_for_fisrt_enum == 1) && ($3 != 0))
         {    
             printf ("\nWARNING: First bit (bit zero) has no name assigned "
                     "[Ref : RFC-2578 (sec-7.1.4)]\n");
         }
         i4_check_for_fisrt_enum = 0;
		 if (isupper ($1[0])) {
			   yyerror ("Name should start with lowercase letter");
	     }
         if (!strcmp (i1_syntax, "BITS") && ($3 < 0)) {
            yyerror ("Syntax Error");
         }
         p_this_named_value = ALLOC_NAME_VALUE_MEM;
         if (p_this_named_value == NULL) {
            fprintf (stderr, "Memory allocation failure\n");
            yyerror ("");
         }
         strcpy (p_this_named_value->name, $1);
         p_this_named_value->value = $3;
         p_this_named_value->next = NULL;
         p_temp_name = i1_is_tc ? p_this_tc->tc_name_value_list : p_name_values;
         if (p_temp_name == NULL) {
            if (i1_is_tc == MID_TRUE) {
               p_this_tc->tc_name_value_list = p_this_named_value;
            } else {
               p_name_values = p_this_named_value;
            }
         } else {
            while (p_temp_name->next != NULL) {
               p_temp_name = p_temp_name->next;
            }
            p_temp_name->next = p_this_named_value;
         }
	   }
       | HNAME '(' AnyNum ')'{
         tNAME_VALUE *p_temp_name, *p_this_named_value;
         if((i4_check_for_fisrt_enum == 1) && ($3 != 0))
         {    
             printf ("\nWARNING: First bit (bit zero) has no name assigned "
                     "[Ref : RFC-2578 (sec-7.1.4)]\n");
         }
         i4_check_for_fisrt_enum = 0;
		 if (isupper ($1[0])) {
			   yyerror ("Name should start with lowercase letter");
	     }
         if (!strcmp (i1_syntax, "BITS") && ($3 < 0)) {
            yyerror ("Syntax Error");
         }
         p_this_named_value = ALLOC_NAME_VALUE_MEM;
         if (p_this_named_value == NULL) {
            fprintf (stderr, "Memory allocation failure\n");
            yyerror ("");
         }
         strcpy (p_this_named_value->name, $1);
         p_this_named_value->value = $3;
         p_this_named_value->next = NULL;
         p_temp_name = i1_is_tc ? p_this_tc->tc_name_value_list : p_name_values;
         if (p_temp_name == NULL) {
            if (i1_is_tc == MID_TRUE) {
               p_this_tc->tc_name_value_list = p_this_named_value;
            } else {
               p_name_values = p_this_named_value;
            }
         } else {
            while (p_temp_name->next != NULL) {
               p_temp_name = p_temp_name->next;
            }
            p_temp_name->next = p_this_named_value;
         }
	   }
       | NAME {
           tNAME_VALUE *p_temp_name = NULL;
           INT1 i1ValueFound = MID_FALSE;

           if (i1_is_defval == MID_TRUE)
           {
               p_temp_name = i1_is_tc ? p_this_tc->tc_name_value_list : p_name_values;
               
               while (p_temp_name != NULL)
               {
                   if (strcmp (p_temp_name->name, $1) == 0)
                   {
                       i1ValueFound = MID_TRUE;
                       break;
                   }
                   p_temp_name = p_temp_name->next;
               }
               
               if (i1ValueFound == MID_FALSE)
               {
                   yyerror ("Enumerator not present in SYNTAX\n");
               }

               if (u1EnumList[p_temp_name->value] == 1)
               {
                   yyerror ("Enumerator is duplicated in DEFVAL clause\n");
               }
               
               u1EnumList[p_temp_name->value] = 1;
               u1EnumSum[(p_temp_name->value / 8)] 
                   += (1 << (7 - (p_temp_name->value % 8)));
           }
       }

       | CURRENT '('AnyNum ')'
       | DEPRECATED '('AnyNum ')'
    ;

Access: READ_ONLY { 
           if (!strcmp (i1_syntax, "Aggregate")) {
              yyerror ("Syntax Error");
           }
           strcpy (i1_access, "read-only");
        }
      | READ_WRITE { 
           if (!strcmp (i1_syntax, "Aggregate") ||
               !strcmp (i1_syntax, "Counter64") ||
               !strcmp (i1_syntax, "Counter32")) {
              yyerror ("Syntax Error");
           }
           strcpy (i1_access, "read-write");
        }
		| READ_CREATE {
           if (!strcmp (i1_syntax, "Aggregate") ||
               !strcmp (i1_syntax, "Counter64") ||
               !strcmp (i1_syntax, "Counter32")) {
              yyerror ("Syntax Error");
           }
           strcpy (i1_access, "read-create");
        }
		| ACCESSIBLE_FOR_NOTIFY {
           if (!strcmp (i1_syntax, "Aggregate") ||
               !strcmp (i1_syntax, "Counter64") ||
               !strcmp (i1_syntax, "Counter32")) {
              yyerror ("Syntax Error");
           }
           strcpy (i1_access, "accessible-for-notify");
        }
		| NOT_ACCESSIBLE { strcpy (i1_access, "not-accessible"); }
		;

Status: CURRENT { strcpy (i1_status, "current"); }
      | DEPRECATED { strcpy (i1_status, "deprecated"); }
		| OBSOLETE { strcpy (i1_status, "obsolete"); }
		;

ReferPart: REFERENCE QSTRING
         |
         ;

IndexPart: INDEX '{' IndexTypes {
                     /*Check for the object  with IMPLIED clause in the INDEX*/
                     if (i4_check_implied_present == 1)
                     {
                         /*If this flag is not set,it implies that object with
                           IMPLIED clause is the last indice*/
                         if (i4_check_implied == 0)
                         {
                             printf ("\nWARNING:IMPLIED index should be last" 
                                     "object in the INDEX clause[Ref:RFC2578(sec:7.7)]\n");
                          }
                     }}
                 '}'
         | AUGMENTS '{' NAME '}' {
              if (check_validity_of_object ($3) == INVALID) {
                  /*Stores the AUGMENT in the array and verify whether
                   *it is defined in the next objects*/
                  strcpy(check_agument[check_flag],$3);
                  check_flag=check_flag+1;
                  check_count=check_count+1;
              }
           }
			|
			;

IndexTypes: IndexType
          | IndexTypes ',' IndexType
			 ;

IndexType: ObjectDescriptor {
              tNAME_LIST *p_index, *p_temp_index = p_indices;
              /*Here we are checking for the implied flag 
                if it is set,reset it.Which implies that IMPLIED object is
                not the last indice in the INDEX clause [Ref:RFC2578(sec:7.7)]*/    
              if (i4_check_implied == 1)
              {
                  i4_check_implied = 0;
              }

              p_index = ALLOC_NAME_MEM;
              if (p_index == NULL) {
                 fprintf (stderr, "Memory allocation failure\n");
                 yyerror ("");
              }
              strcpy (p_index->name, $1);
              p_index->next = NULL;
              if (p_indices == NULL) {
                 p_indices = p_index;
              }
              else {
                 while (p_temp_index->next != NULL) {
                    p_temp_index = p_temp_index->next;
                 }
                 p_temp_index->next = p_index;
              }
			  }
         | IMPLIED ObjectDescriptor {
              tNAME_LIST *p_index, *p_temp_index = p_indices;
              /*Set the flags when a Object with IMPLIED clause 
              is present as the indices*/
              i4_check_implied = 1;
              i4_check_implied_present = 1;
              p_index = ALLOC_NAME_MEM;
              if (p_index == NULL) {
                 fprintf (stderr, "Memory allocation failure\n");
                 yyerror ("");
              }
              strcpy (p_index->name, $2);
              p_index->next = NULL;
              if (p_indices == NULL) {
                 p_indices = p_index;
              }
              else {
                 while (p_temp_index->next != NULL) {
                    p_temp_index = p_temp_index->next;
                 }
                 p_temp_index->next = p_index;
              }
			  }
			;

ObjectId: '{' {
             strcpy (i1_object_identifier, "");
          } Components '}' {
             INT1 *pi1_token, i1_orig_oid[MAX_OID_LEN];
             INT1 i1_parent[MAX_OID_LEN], i1_position[MAX_DESC_LEN];
             tOID *p_entry, *p_temp_oid = p_object_ids;

             i1_object_identifier[strlen(i1_object_identifier) - 1] = '\0';

             strcpy (i1_orig_oid, i1_object_identifier);
             pi1_token = strtok (i1_orig_oid, ".");
             while (pi1_token != NULL) {
                if (!isdigit(*pi1_token)) {
				       if (check_validity_of_oid (pi1_token) == INVALID) {
                          /*When the validity of oid fails
                            store it in array inoder to check
                            whether it is defined in the future processing of mib*/
                          strcpy(obj_Des[i4_count_invalid_oid],pi1_token);
                          i4_count_invalid_oid++;
				          /*yyerror ("Invalid Object Identifier");*/
				       }
                }
                pi1_token = strtok (NULL, ".");
             }

             /* get parent and position seperated */
             strcpy (i1_orig_oid, i1_object_identifier);
             pi1_token = strrchr (i1_orig_oid, '.');
             if (pi1_token == NULL) {
                strcpy (i1_parent, "NoParent");
                strcpy (i1_position, i1_orig_oid);
             } else {
                strcpy (i1_position, pi1_token + 1);
                *pi1_token = '\0';
                strcpy (i1_parent, i1_orig_oid);
				    if (isupper (i1_parent[0])) {
				       yyerror ("ObjectDescriptor should start with lowercase letter");
				    }
             }
             if (p_object_ids == NULL) {
                p_object_ids = ALLOC_OID_MEM;
                if (p_object_ids == NULL) {
                   fprintf (stderr, "Memory allocation failure\n");
                   yyerror ("");
                }
                strcpy (p_object_ids->name, i1_parent);
                p_object_ids->oid = atoi (i1_position);
                p_object_ids->next = NULL;
             } else {
                while (p_temp_oid->next != NULL) {
                   if (strcmp (p_temp_oid->name, i1_parent) == 0) {
                      if (p_temp_oid->oid == atoi (i1_position)) {
                         fprintf (stderr, "\nERROR: Duplicate OID for %s\n", i1_object_desc);
                         yyerror ("");
                      }
                      if (++p_temp_oid->oid != atoi (i1_position)) {
                         p_temp_oid->oid = atoi (i1_position);
#if DEBUG			 
                         fprintf (stderr, "\nWARNING: Object %s not in order\n",
                                                                i1_object_desc);
#endif								
                      }
                      break;
                   }
                   p_temp_oid = p_temp_oid->next;
                }
                if (p_temp_oid->next == NULL) {
                   if (strcmp (p_temp_oid->name, i1_parent) == 0) {
                      if (p_temp_oid->oid == atoi (i1_position)) {
                         fprintf (stderr, "\nERROR: Duplicate OID for %s\n", i1_object_desc);
                         yyerror ("");
                      }
                      if (++p_temp_oid->oid != atoi (i1_position)) {
                         p_temp_oid->oid = atoi (i1_position);
#if DEBUG			 
                         fprintf (stderr, "\nWARNING: Object %s not in order\n",
                                                                i1_object_desc);
#endif								
                      }
                   } else {
                      p_entry = ALLOC_OID_MEM;
                      if (p_entry == NULL) {
                         fprintf (stderr, "Memory allocation failure\n");
                         yyerror ("");
                      }
                      strcpy (p_entry->name, i1_parent);
                      if ((p_entry->oid = atoi (i1_position)) != 1) {
                         strcpy (i1_orig_oid, i1_object_identifier);
                         pi1_token = strtok (i1_orig_oid, ".");
                         if (strcmp (pi1_token, "iso") &&
                             strcmp (pi1_token, "org") &&
                             strcmp (pi1_token, "dod") &&
                             strcmp (pi1_token, "internet") &&
                             strcmp (pi1_token, "directory") &&
                             strcmp (pi1_token, "mgmt") &&
                             strcmp (pi1_token, "mib-2") &&
                             strcmp (pi1_token, "transmission") &&
                             strcmp (pi1_token, "experimental") &&
                             strcmp (pi1_token, "private") &&
                             strcmp (pi1_token, "enterprises") &&
                             strcmp (pi1_token, "security") &&
                             strcmp (pi1_token, "snmpV2")) {
			     
#if DEBUG			 
                            fprintf (stderr, "\nWARNING: Object %s", i1_object_desc);
                            fprintf (stderr, " position not starting from 1\n");
#endif			    
                         }
                      }
                      p_entry->next = NULL;
                      p_temp_oid->next = p_entry;
                   }
                }
             }
             if (i1_oid_wanted && i1_is_oid) {
				    fprintf (p_out_file, "%s\n", i1_object_identifier);
			    } else if (i1_oid_wanted && !i1_is_oid) {
				    fprintf (p_temp_file, "%s   ", i1_object_identifier);
				 }
			 }
        ;

Components: Component
          | Components Component
          ;

Component: NameHName {
              if (isupper ($1[0])) {
                 yyerror ("ObjectDescriptor should start with lowercase letter");
              }
              strcat (i1_object_identifier, $1);
              strcat (i1_object_identifier, ".");
           }
         | Num {
              INT1 *pi1_temp;
              /*Checking whether the subidentifier is assigned zero*/
              if (i4_check_for_zero == 1 && $1 == 0)
              {
                  printf ("\nERROR : last subidentifier assigned may not be zero "
                          "[Ref : RFC-2578 (sec-7.7)]\n");
                  yyerror("");
              }
              pi1_temp = itoa ($1);
              if (pi1_temp == NULL) {
                 yyerror ("");
              }
              strcat (i1_object_identifier, pi1_temp);
              strcat (i1_object_identifier, ".");
           }
         | NameHName '(' Num ')' {
              INT1 *pi1_temp;

              if (isupper ($1[0])) {
                 yyerror ("ObjectDescriptor should start with lowercase letter");
              }
              pi1_temp = itoa ($3);
              if (pi1_temp == NULL) {
                 yyerror ("");
              }
              strcat (i1_object_identifier, pi1_temp);
              strcat (i1_object_identifier, ".");
           }
         ;

MibName: NameHName {
		      if (islower ($1[0])) {
			      yyerror ("Mibname should start with uppercase letter");
			   }
         }
       ;

Type: NameHName {
			if(store_import ($1) == ERROR) {
			   yyerror ("");
			}
	   }
    | MODULE_IDENTITY
    | OBJECT_IDENTITY
    | TEXTUAL_CONVENTION
	| OBJECT_TYPE
    | NOTIFICATION_TYPE
    | MODULE_COMPLIANCE
    | NOTIFICATION_GROUP
    | OBJECT_GROUP
    | AGENT_CAPABILITIES
    ;

NotificationName: NAME
                ;

ModuleName: NAME
          | { strcpy ($$, ""); /* chumma! to avoid warning */ }
          | HNAME
          ;

ObjectName: NAME {
			      if (check_validity_of_object ($1) == INVALID) {
				      yyerror ("Invalid object name");
			      }
			   }
          ;

SyntaxType: NAME {
               if (islower ($1[0])) {
                  yyerror ("Syntax Type should start with uppercase letter");
               }
            }
          ;

ObjectDescriptor: NAME {
                     if (isupper ($1[0])) {
                        yyerror ("ObjectDescriptor should start with lowercase letter");
                     }
                  }
                ;

NameHName: NAME		 
			| HNAME
         ;

AnyNum: Num
      | '-' NUM { $$ = -$2; }
      ;

Num: NUM 
   | ZERO
   ;

%%

/***********************************************************************
 *      Function Name        : main                                    *
 *      Role of the function : Process the command line and calls      *
 *                             parser fn.                              *
 *      Formal Parameters    : argc, argv                              *
 *      Global Variables     :                                         *
 *      Use of Recursion     : None.                                   *
 *      Return Value         : None.                                   *
 **********************************************************************/

INT4 main (INT4 argc, INT1 *argv[])
{
	INT1 i1_response[20];
   INT1 *pi1_dotptr;
   INT4 i4_index;
	struct stat buf;

	if (argc < 2) {
	   fprintf (stderr, "Usage: %s <input file> ...\n", argv[0]);
		exit (ERROR);
	}
   for (i4_index = 1; i4_index < argc; i4_index++) {
		if ((yyin = fopen (argv[i4_index], "r")) == NULL) {
		   fprintf (stderr, "\nCan't open %s \nPlease <Enter> to continue...\n", argv[i4_index]);
		   getchar();
			continue;
		}
		strcpy (i1_in_file, argv[i4_index]);
		strcpy (i1_out_file, argv[i4_index]);
      if (pi1_dotptr = strrchr (i1_out_file, '.')) {
         *pi1_dotptr = '\0';
      }
                strcpy (i1_defval_file, i1_out_file);
                strcat (i1_defval_file, ".dval");
		strcat (i1_out_file, PREMOSY_OUTPUT_EXTN);
         fflush (stdin);
		if (stat (i1_out_file, &buf) == 0) {
		   fprintf (stderr, "\nWant to overwrite %s ?(yes/no)(y/n): ", i1_out_file);
         fflush (stdout);
         fflush (stdin);
			scanf ("%s",i1_response);
        /* getchar ();  to read out additional char */
         fflush (stdin);
			if (tolower (i1_response[0]) != 'y') {
			   continue;
			}
		}
		if ((p_out_file = fopen (i1_out_file, "w")) == NULL) {
		   fprintf (stderr, "Can't open %s to write output\n", i1_out_file);
			continue;
		}
		if ((p_defval_file = fopen (i1_defval_file, "w")) == NULL) {
		   fprintf (stderr, "Can't open %s to write output\n", i1_defval_file);
			continue;
		}
   	strcpy (i1_temp_file, "v2compXXXXXX");
   	if (mkstemp (i1_temp_file) == -1) {
   	   strcpy (i1_temp_file, "v2ctemp.def");
   	}
   	if ((p_temp_file = fopen (i1_temp_file, "w")) == NULL) {
   		fprintf (stderr, "Can't open %s to write intermediate output\n", i1_temp_file);
   		continue;
   	}
      p_oid_list = NULL;
      p_object_list = NULL;
      p_import_list = NULL;
      p_table_entries = NULL;
      p_this_entry = NULL;
      p_indices = NULL;
      p_checked_indices = NULL;
      p_name_values = NULL;
      p_object_ids = NULL;
      p_type_defs = NULL;
      p_this_tc = NULL;
      yylineno = 1;
      printf ("\nParsing %s...", i1_in_file);
   	fflush (stdout);
	add_v2_typedefs();
	add_v2_obj_ids();
      yyparse ();
      /*Checks whether AUGMENT is found*/
      if(check_flag > 0) {
          fprintf (stderr, "Can't find the augment");
          yyerror();
      }
      if (i4_count_invalid_oid != 0)
      {
          yyerror ("Invalid Object Identifier");
      }    

     
   	fclose (p_out_file);
   	fclose (p_temp_file);
   	free_names_memory (p_oid_list);
   	free_names_memory (p_object_list);
   	free_names_memory (p_import_list);
      free_name_values_memory (p_name_values);
      free_names_memory (p_checked_indices);
      free_OID_memory (p_object_ids);
      free_tc_memory (p_type_defs);
      if (append_temp_file_to_output_file (i1_out_file, i1_temp_file) == ERROR) {
         continue;
      }
     if( postmosy (i1_in_file) == ERROR)
         exit(ERROR);
	}
	exit (SUCCESS);
}

/**********************************************************************
 *      Function Name        : yyerror                                *
 *      Role of the function : Prints error message passed and exits 
 *                after closing all files and unlinking output file.  *
 *      Formal Parameters    : pi1_mesg                               *
 *      Global Variables     : p_out_file, p_temp_file, i1_out_file,  *
 *                             i1_in_file, i1_temp_file, p_oid_list   *
 *                             p_import_list, p_object_list           *
 *      Use of Recursion     : None                                   *
 *      Return Value         : None                                   *
 **********************************************************************/

INT4 yyerror (INT1 *pi1_mesg)
{
#ifdef OPEN_INPUT_FILE
   INT1 i1_cmd_line[MAX_LINE_LEN];
#endif /* OPEN_INPUT_FILE */

   fprintf (stderr, "\n%s(line %d): %s\n", i1_in_file, yylineno, pi1_mesg);
   fprintf (stderr, "Last token read %s\n", yytext);
	fclose (p_out_file);
	fclose (p_temp_file);
	unlink (i1_out_file);
	unlink (i1_temp_file);
	free_names_memory (p_oid_list);
	free_names_memory (p_object_list);
	free_names_memory (p_import_list);
   free_name_values_memory (p_name_values);
   free_names_memory (p_checked_indices);
   free_OID_memory (p_object_ids);
   free_tc_memory (p_type_defs);
#ifdef OPEN_INPUT_FILE
   sprintf (i1_cmd_line, "vi +%d %s", yylineno, i1_in_file);
   fprintf (stderr, "Locating error in input file...\n");
   sleep (5);
   system (i1_cmd_line);
#endif /* OPEN_INPUT_FILE */
	exit (ERROR);
}



/***********************************************************************
 *      Function Name        : check_for_table_object.                 *
 *      Role of the function : checks wheather this object is table    *
 *                             element or not.                         *
 *      Formal Parameters    : p1_name ( name of the object )          *
 *      Global Variables     : p_table_entries.                        *
 *      Use of Recursion     : None.                                   *
 *      Return Value         : MID_TRUE/MID_FALSE                              *
 **********************************************************************/

check_for_table_object(INT1 *p1_name)
{
tNAME_LIST *p_temp = p_table_entries;
if( p_temp == NULL )
  return MID_FALSE;
  while(p_temp)
  {
  if ( strcmp(p_temp->name,p1_name) == 0 )
           return MID_TRUE;

    p_temp = p_temp->next;
  }
  return MID_FALSE;
  }





/***********************************************************************
 *      Function Name        : add_v2_typedefs.                        *
 *      Role of the function : initialization. adding all TEXTUAL-     *
 *                             CONVENTIONs of SMI-v2.                  *
 *      Formal Parameters    :                                         *
 *      Global Variables     :                                         *
 *      Use of Recursion     : None.                                   *
 *      Return Value         : None.                                   *
 **********************************************************************/


void  add_v2_typedefs()
{

add_typedef("DisplayString","OCTET STRING");
add_typedef("PhysAddress","OCTET STRING");
add_typedef("MacAddress","OCTET STRING");
add_typedef("TestAndIncr","INTEGER");
add_typedef("AutonomousType","OBJECT IDENTIFIER");
add_typedef("InstancePointer","OBJECT IDENTIFIER");
add_typedef("VariablePointer","OBJECT IDENTIFIER");
add_typedef("RowPointer","OBJECT IDENTIFIER");
add_typedef("TimeStamp","TimeTicks");
add_typedef("TimeInterval","INTEGER");
add_typedef("DateAndTime","OCTET STRING");
add_typedef("TDomain","OBJECT IDENTIFIER");
add_typedef("TAddress","OCTET STRING");


/****** addition TruthValue TEXTUAL MACRO of SNMPv2-TC *******/
add_TruthValue();
add_StorageType();
add_RowStatus();
add_EnabledStatus();

}

/***********************************************************************
 *      Function Name        : add_TruthValue                          *
 *      Role of the function : add new typedef TruthValue  to the      *
 *                             structure  p_typedefs                   *
 *      Formal Parameters    : None                                    *
 *      Global Variables     : p_type_defs                             *
 *      Use of Recursion     : None.                                   *
 *      Return Value         : None.                                   *
 **********************************************************************/

add_TruthValue()
{
tTC_TABLE  *p_temp , *p_temp2;
tNAME_VALUE *p_name;

 p_temp = (tTC_TABLE *) malloc(sizeof(tTC_TABLE));
 if(p_temp == NULL) {
 fprintf(stderr,"malloc error \n");
 exit(ERROR);
 }
 p_temp->next = NULL;
 strcpy(p_temp->tc_name,"TruthValue");
 strcpy(p_temp->tc_base,"INTEGER");
 p_temp->tc_name_value_list = (tNAME_VALUE *)malloc(sizeof(tNAME_VALUE));
 p_name = p_temp->tc_name_value_list;
 strcpy(p_name->name,"true");
 p_name->value = 1;
 p_name->next = (tNAME_VALUE *) malloc(sizeof(tNAME_VALUE));
 p_name = p_name->next;
 p_name->next = NULL;
 strcpy(p_name->name,"false");
 p_name->value = 2;
 p_temp->next = p_type_defs;
 p_type_defs = p_temp;

 }

/***********************************************************************
 *      Function Name        : add_StorageType                         *
 *      Role of the function : add new typedef TruthValue  to the      *
 *                             structure  p_typedefs                   *
 *      Formal Parameters    : None                                    *
 *      Global Variables     : p_type_defs                             *
 *      Use of Recursion     : None.                                   *
 *      Return Value         : None.                                   *
 **********************************************************************/

add_StorageType()
{
tTC_TABLE  *p_temp , *p_temp2;
tNAME_VALUE *p_name;

 p_temp = (tTC_TABLE *) malloc(sizeof(tTC_TABLE));
 if(p_temp == NULL) {
 fprintf(stderr,"malloc error \n");
 exit(ERROR);
 }
 p_temp->next = NULL;
 strcpy(p_temp->tc_name,"StorageType");
 strcpy(p_temp->tc_base,"INTEGER");
 p_temp->tc_name_value_list = (tNAME_VALUE *)malloc(sizeof(tNAME_VALUE));
 p_name = p_temp->tc_name_value_list;
 strcpy(p_name->name,"other");
 p_name->value = 1;
 p_name->next = (tNAME_VALUE *) malloc(sizeof(tNAME_VALUE));
 p_name = p_name->next;
 strcpy(p_name->name,"volatile");
 p_name->value = 2;
 p_name->next = (tNAME_VALUE *) malloc(sizeof(tNAME_VALUE));
 p_name = p_name->next;
 strcpy(p_name->name,"nonVolatile");
 p_name->value = 3;
 p_name->next = (tNAME_VALUE *) malloc(sizeof(tNAME_VALUE));
 p_name = p_name->next;
 strcpy(p_name->name,"permanent");
 p_name->value = 4;
 p_name->next = (tNAME_VALUE *) malloc(sizeof(tNAME_VALUE));
 p_name = p_name->next;
 strcpy(p_name->name,"readOnly");
 p_name->value = 5;
p_name->next = NULL;


 p_temp->next = p_type_defs;
 p_type_defs = p_temp;
 }


/***********************************************************************
 *      Function Name        : add_RowStatus                          *
 *      Role of the function : add new typedef TruthValue  to the      *
 *                             structure  p_typedefs                   *
 *      Formal Parameters    : None                                    *
 *      Global Variables     : p_type_defs                             *
 *      Use of Recursion     : None.                                   *
 *      Return Value         : None.                                   *
 **********************************************************************/

add_RowStatus()
{
tTC_TABLE  *p_temp , *p_temp2;
tNAME_VALUE *p_name;

 p_temp = (tTC_TABLE *) malloc(sizeof(tTC_TABLE));
 if(p_temp == NULL) {
 fprintf(stderr,"malloc error \n");
 exit(ERROR);
 }
 p_temp->next = NULL;
 strcpy(p_temp->tc_name,"RowStatus");
 strcpy(p_temp->tc_base,"INTEGER");
 p_temp->tc_name_value_list = (tNAME_VALUE *)malloc(sizeof(tNAME_VALUE));
 p_name = p_temp->tc_name_value_list;
 strcpy(p_name->name,"active");
 p_name->value = 1;
 p_name->next = (tNAME_VALUE *) malloc(sizeof(tNAME_VALUE));
 p_name = p_name->next;
 strcpy(p_name->name,"notInService");
 p_name->value = 2;
 p_name->next = (tNAME_VALUE *) malloc(sizeof(tNAME_VALUE));
 p_name = p_name->next;
 strcpy(p_name->name,"notReady");
 p_name->value = 3;
 p_name->next = (tNAME_VALUE *) malloc(sizeof(tNAME_VALUE));
 p_name = p_name->next;
 strcpy(p_name->name,"createAndGo");
 p_name->value = 4;
 p_name->next = (tNAME_VALUE *) malloc(sizeof(tNAME_VALUE));
 p_name = p_name->next;
 strcpy(p_name->name,"createAndWait");
 p_name->value = 5;
 p_name->next = (tNAME_VALUE *) malloc(sizeof(tNAME_VALUE));
 p_name = p_name->next;
 strcpy(p_name->name,"destroy");
 p_name->value = 6;
 p_name->next = NULL;


 p_temp->next = p_type_defs;
 p_type_defs = p_temp;
 }

 

/***********************************************************************
 *      Function Name        : add_EnabledStatus                       *
 *      Role of the function : add new typedef EnabledStatus to the    *
 *                             structure  p_typedefs                   *
 *      Formal Parameters    : None                                    *
 *      Global Variables     : p_type_defs                             *
 *      Use of Recursion     : None.                                   *
 *      Return Value         : None.                                   *
 **********************************************************************/

add_EnabledStatus()
{
tTC_TABLE  *p_temp ;
tNAME_VALUE *p_name;

 p_temp = (tTC_TABLE *) malloc(sizeof(tTC_TABLE));
 if(p_temp == NULL) {
 fprintf(stderr,"malloc error \n");
 exit(ERROR);
 }
 p_temp->next = NULL;
 strcpy(p_temp->tc_name,"EnabledStatus");
 strcpy(p_temp->tc_base,"INTEGER");
 p_temp->tc_name_value_list = (tNAME_VALUE *)malloc(sizeof(tNAME_VALUE));
 p_name = p_temp->tc_name_value_list;
 strcpy(p_name->name,"enabled");
 p_name->value = 1;
 p_name->next = (tNAME_VALUE *) malloc(sizeof(tNAME_VALUE));
 p_name = p_name->next;
 strcpy(p_name->name,"disabled");
 p_name->value = 2;
 p_name->next = NULL;


 p_temp->next = p_type_defs;
 p_type_defs = p_temp;
 }

/***********************************************************************
 *      Function Name        : add_typedefs                            *
 *      Role of the function : add new typedef to the structure        *
 *                             p_typedefs                              *
 *      Formal Parameters    : p_nam1,p_name2 ( new name and primitive)*
 *      Global Variables     : p_type_defs                             *
 *      Use of Recursion     : None.                                   *
 *      Return Value         : None.                                   *
 **********************************************************************/
void add_typedef(INT1 *p_name1,INT1 *p_name2)
{
tTC_TABLE  *p_temp , *p_temp2;

 p_temp = (tTC_TABLE *) malloc(sizeof(tTC_TABLE));
 if(p_temp == NULL) {
 fprintf(stderr,"malloc error \n");
 exit(ERROR);
 }
 p_temp->next = NULL;
 p_temp->tc_name_value_list = NULL;
 strcpy(p_temp->tc_name,p_name1);
 strcpy(p_temp->tc_base,p_name2);
 if(p_type_defs == NULL )
 p_type_defs = p_temp;
 else {
 p_temp2 = p_type_defs;
 while(p_temp2->next)
 p_temp2 = p_temp2->next;
 p_temp2->next = p_temp;
 }
 }

/***********************************************************************
 *      Function Name        : add_v2_obj_ids                          *
 *      Role of the function : add all objects that are defined in     *
 *                             SMI-v2                                  *
 *      Formal Parameters    : None.                                   *
 *      Global Variables     :                                         *
 *      Use of Recursion     : None.                                   *
 *      Return Value         : None.                                   *
 **********************************************************************/
void add_v2_obj_ids()
{
store_oid("ccitt");
store_oid("iso");
store_oid("joint-iso-ccitt");
store_oid("org");
store_oid("dod");
store_oid("internet");
store_oid("directory");
store_oid("mgmt");
store_oid("mib-2");
store_oid("transmission");
store_oid("experimental");
store_oid("private");
store_oid("enterprises");
store_oid("security");
store_oid("snmpV2");
store_oid("snmpDomains");
store_oid("snmpProxys");
store_oid("snmpModules");
return ;
}
