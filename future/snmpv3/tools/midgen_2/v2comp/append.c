/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: append.c,v 1.4 2015/04/28 14:43:33 siva Exp $
 *
 * Description:This file contains a function which appends 
 *             a file to another. 
 *******************************************************************/

#include "premosy.h"

/*****************************************************************************
 *      Function Name        : append_temp_file_to_output_file               *
 *      Role of the function : Appends the contents of temp file to output   *
 *      Formal Parameters    : pi1_out_file, pi1_temp_file                   *
 *      Global Variables     : None                                          *
 *      Use of Recursion     : None                                          *
 *      Return Value         : None                                          *
 *****************************************************************************/

INT4
append_temp_file_to_output_file (INT1 *pi1_out_file, INT1 *pi1_temp_file)
{
    INT1                i1_line[MAX_LINE_LEN];
    FILE               *p_out_file, *p_temp_file;

    if ((p_out_file = fopen (pi1_out_file, "a")) == NULL)
    {
        fprintf (stderr, "Can't open output file %s\n", pi1_out_file);
        return ERROR;
    }
    if ((p_temp_file = fopen (pi1_temp_file, "r")) == NULL)
    {
        fprintf (stderr, "Can't open temp file %s\n", pi1_temp_file);
        return ERROR;
    }
    fprintf (p_out_file, "\n");
    while (fgets (i1_line, MAX_LINE_LEN, p_temp_file) != NULL)
    {
        fputs (i1_line, p_out_file);
    }
    fclose (p_out_file);
    fclose (p_temp_file);
    unlink (pi1_temp_file);
    return SUCCESS;
}
