/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: postmosy.h,v 1.4 2015/04/28 14:43:33 siva Exp $
 *
 * Description: This file contains the typedefs, other header   
 *              files to be included, definition of MACROs     
 *              and function prototypes for postmosy module    
 *******************************************************************/

#ifndef _POSTMOSY_H
#define _POSTMOSY_H

#include "common.h" 

#define COMMENT 2
#define BLANK 3
#define POSTMOSY_OUTPUT_EXTN ".h"
#define ALLOC_ENTRY_MEM ((tOID_TABLE *) calloc (1, sizeof (tOID_TABLE)))

typedef struct oid_table {
        INT1 object_desc[MAX_DESC_LEN];
	INT1 object_oid[MAX_OID_LEN];
		  struct oid_table *next;
} tOID_TABLE;

INT4 initialise_oid_table (void );
INT4 add_entry (INT1 *, INT1 *);
INT4 compare_oids (INT1 *, INT1 *);
INT4 process_input (INT1 *);
INT4 check_line (INT1 *);
INT1 *get_next_component (INT1 *, INT1);
INT4 write_oid_list_to_file (INT1 *, INT1 *);
void free_memory (tOID_TABLE *);

#endif /* _POSTMOSY_H */
