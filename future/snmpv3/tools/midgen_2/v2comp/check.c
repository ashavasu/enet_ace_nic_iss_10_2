/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: check.c,v 1.5 2015/04/28 14:43:33 siva Exp $
 *
 * Description:This file contains routines which check for the
 *             validity of the passed string in the context. 
 *******************************************************************/

#include "premosy.h"

extern tNAME_LIST  *p_oid_list;
extern tNAME_LIST  *p_object_list;
extern tNAME_LIST  *p_import_list;
extern tNAME_VALUE *p_name_values;
extern tTC_TABLE   *p_type_defs;
extern tTC_TABLE   *p_this_tc;
INT4                get_syntax_fromfile (INT1 *p1_name);

/*****************************************************************************
 *      Function Name        : check_validity_of_oid                         *
 *      Role of the function : Checks whether passed string is a valid OID.  *
 *      Formal Parameters    : pi1_oid_name                                  *
 *      Global Variables     : p_oid_list                                    *
 *      Use of Recursion     : None                                          *
 *      Return Value         : VALID, if passed name is valid                *
 *                             INVALID, if passed name is invalid            *
 *****************************************************************************/

INT4
check_validity_of_oid (INT1 *pi1_oid_name)
{
    tNAME_LIST         *p_temp = p_oid_list;

    while (p_temp != NULL)
    {
        if (strcmp (pi1_oid_name, p_temp->name) == 0)
        {
            return VALID;
        }
        p_temp = p_temp->next;
    }
    return check_validity_of_import (pi1_oid_name);
}

/*****************************************************************************
 *      Function Name        : check_validity_of_object                      *
 *      Role of the function : Checks whether passed name is valid object.   *
 *      Formal Parameters    : pi1_object_name                               *
 *      Global Variables     : p_object_list                                 *
 *      Use of Recursion     : None                                          *
 *      Return Value         : VALID, if passed name is valid                *
 *                             INVALID, if passed name is invalid            *
 *****************************************************************************/

INT4
check_validity_of_object (INT1 *pi1_object_name)
{
    tNAME_LIST         *p_temp = p_object_list;

    while (p_temp != NULL)
    {
        if (strcmp (pi1_object_name, p_temp->name) == 0)
        {
            return VALID;
        }
        p_temp = p_temp->next;
    }
    return check_validity_of_import (pi1_object_name);
}

/*****************************************************************************
 *      Function Name        : check_validity_of_syntax                      *
 *      Role of the function : Checks whether passed name is valid syntax.   *
 *      Formal Parameters    : pi1_syntax_name                               *
 *      Global Variables     : p_type_defs                                   *
 *      Use of Recursion     : None                                          *
 *      Return Value         : VALID, if passed name is valid                *
 *                             INVALID, if passed name is invalid            *
 *****************************************************************************/

INT4
check_validity_of_syntax (INT1 *pi1_syntax_name)
{
    tTC_TABLE          *temp_tc = p_type_defs;

    if ((strcmp (pi1_syntax_name, "INTEGER") == 0) ||
        (strcmp (pi1_syntax_name, "Integer32") == 0) ||
        (strcmp (pi1_syntax_name, "Unsigned32") == 0) ||
        (strcmp (pi1_syntax_name, "Counter32") == 0) ||
        (strcmp (pi1_syntax_name, "Counter64") == 0) ||
        (strcmp (pi1_syntax_name, "Gauge32") == 0) ||
        (strcmp (pi1_syntax_name, "Opaque") == 0) ||
        (strcmp (pi1_syntax_name, "TimeTicks") == 0) ||
        (strcmp (pi1_syntax_name, "BITS") == 0) ||
        (strcmp (pi1_syntax_name, "IpAddress") == 0))
    {
        return VALID;
    }
    for (; temp_tc != NULL; temp_tc = temp_tc->next)
    {
        if (strcmp (pi1_syntax_name, temp_tc->tc_name) == 0)
        {
            return VALID;
        }
    }
    return check_validity_of_import (pi1_syntax_name);
}

/*****************************************************************************
 *      Function Name        : check_validity_of_import                      *
 *      Role of the function : Checks whether passed name is valid import.   *
 *      Formal Parameters    : pi1_import_name                               *
 *      Global Variables     : p_import_list                                 *
 *      Use of Recursion     : None                                          *
 *      Return Value         : VALID, if passed name is valid                *
 *                             INVALID, if passed name is invalid            *
 *****************************************************************************/

INT4
check_validity_of_import (INT1 *pi1_import_name)
{
    tNAME_LIST         *p_temp = p_import_list;

    while (p_temp != NULL)
    {
        if (strcmp (pi1_import_name, p_temp->name) == 0)
        {
            return VALID;
        }
        p_temp = p_temp->next;
    }
    /* Added by Senthilgcs. This will check the info.def */
    return (get_syntax_fromfile (pi1_import_name));
}

/************************************************************************
*  Function Name        : get_syntax_fromfile 
*  Role of the function : Check whether the imported Index is present in 
*                         the info.def file 
*  Formal Parameters    : pi1_import_name - Inde name to be validated 
*  Global Variables     : None 
*  Use of Recursion     : None                    
*  Return Value         : VALID if the index is present in info.def 
*                         otherwise INVALID
*************************************************************************/

INT4
get_syntax_fromfile (INT1 *p1_name)
{
    FILE               *fp;
    INT1                i1_line[MAX_LINE_LEN];
    INT1                i1_name[MAX_LINE_LEN];
    INT1                i1_temp1[MAX_LINE_LEN];
    INT1                i1_temp2[MAX_LINE_LEN];

    if ((fp = fopen ("info.def", "r")) != NULL)
    {
        bzero (i1_line, MAX_LINE_LEN);
        fgets (i1_line, MAX_LINE_LEN, fp);
        while (!feof (fp))
        {
            if (strstr (i1_line, "IMPORT OBJECT") != NULL)
            {
                bzero (i1_name, MAX_LINE_LEN);
                sscanf (i1_line, "%s %s %s", i1_name, i1_temp1, i1_temp2);
                if (strcmp (p1_name, i1_name) == 0 &&
                    strlen (p1_name) == strlen (i1_name))
                {
                    return VALID;
                }
            }
            fgets (i1_line, MAX_LINE_LEN, fp);
        }
        fclose (fp);
    }
    return INVALID;
}

/*****************************************************************************
 *      Function Name        : check_for_duplicate_name                      *
 *      Role of the function : Checks whether passed object already defined. *
 *      Formal Parameters    : pi1_object_name, i1_is_oid                    *
 *      Global Variables     : p_object_list, p_oid_list                     *
 *      Use of Recursion     : None                                          *
 *      Return Value         : MID_TRUE, if passed object alreay defined         *
 *                             MID_FALSE, else if it is the first occurence      *
 *****************************************************************************/

INT4
check_for_duplicate_name (INT1 *pi1_object_name, INT1 i1_is_oid)
{
    tNAME_LIST         *p_temp;

    p_temp = i1_is_oid ? p_oid_list : p_object_list;
    while (p_temp != NULL)
    {
        if (strcmp (p_temp->name, pi1_object_name) == 0)
        {
            if ((strcmp (p_temp->name, "iso") == 0) ||
                (strcmp (p_temp->name, "ccitt") == 0) ||
                (strcmp (p_temp->name, "joint-iso-ccitt") == 0) ||
                (strcmp (p_temp->name, "org") == 0) ||
                (strcmp (p_temp->name, "dod") == 0) ||
                (strcmp (p_temp->name, "internet") == 0) ||
                (strcmp (p_temp->name, "directory") == 0) ||
                (strcmp (p_temp->name, "mgmt") == 0) ||
                (strcmp (p_temp->name, "mib-2") == 0) ||
                (strcmp (p_temp->name, "transmission") == 0) ||
                (strcmp (p_temp->name, "experimental") == 0) ||
                (strcmp (p_temp->name, "private") == 0) ||
                (strcmp (p_temp->name, "enterprises") == 0) ||
                (strcmp (p_temp->name, "security") == 0) ||
                (strcmp (p_temp->name, "snmpV2") == 0) ||
                (strcmp (p_temp->name, "snmpDomains") == 0) ||
                (strcmp (p_temp->name, "snmpProxys") == 0) ||
                (strcmp (p_temp->name, "snmpModules") == 0))
                return MID_FALSE;

            return MID_TRUE;
        }
        p_temp = p_temp->next;
    }
    return MID_FALSE;
}

/*****************************************************************************
 *      Function Name        : check_validity_of_name                        *
 *      Role of the function : Checks whether passed string is valid         *
 *                             default  value.                               *
 *      Formal Parameters    : pi1_syntax_type, pi1_default_value            *
 *      Global Variables     : p_name_values, p_type_defs                    *
 *      Use of Recursion     : None                                          *
 *      Return Value         : VALID, if passed name is valid                *
 *                             INVALID, if passed name is invalid            *
 *****************************************************************************/

INT4
check_validity_of_name (INT1 *pi1_syntax_type, INT1 *pi1_default_value)
{
    tTC_TABLE          *p_temp_tc;
    tNAME_VALUE        *p_temp_name_value;

    if (strcmp (pi1_syntax_type, "INTEGER") == 0)
    {
        p_temp_name_value = p_name_values;
        while (p_temp_name_value != NULL)
        {
            if (strcmp (pi1_default_value, p_temp_name_value->name) == 0)
            {
                return VALID;
            }
            p_temp_name_value = p_temp_name_value->next;
        }
        return INVALID;
    }
    if (strcmp (pi1_syntax_type, "OBJECT IDENTIFIER") == 0)
    {
        return check_validity_of_oid (pi1_default_value);
    }
    p_temp_tc = p_type_defs;
    while (p_temp_tc != NULL)
    {
        if (strcmp (p_temp_tc->tc_name, pi1_syntax_type) == 0)
        {
            break;
        }
        p_temp_tc = p_temp_tc->next;
    }
    if (p_temp_tc != NULL)
    {
        if (strcmp (p_temp_tc->tc_base, "INTEGER") == 0)
        {
            p_temp_name_value = p_temp_tc->tc_name_value_list;
            while (p_temp_name_value != NULL)
            {
                if (strcmp (p_temp_name_value->name, pi1_default_value) == 0)
                {
                    return VALID;
                }
                p_temp_name_value = p_temp_name_value->next;
            }
            return INVALID;
        }
        if (strcmp (p_temp_tc->tc_base, "OBJECT IDENTIFIER") == 0)
        {
            return check_validity_of_oid (pi1_default_value);
        }
    }
    return VALID;
}

/*****************************************************************************
 *      Function Name        : check_validity_of_value                       *
 *      Role of the function : Checks whether passed value is a valid        *
 *                             default  value.                               *
 *      Formal Parameters    : pi1_syntax, i1_enum, i4_defval                *
 *      Global Variables     : p_namd_values, p_type_defs                    *
 *      Use of Recursion     : None                                          *
 *      Return Value         : VALID, if passed value is valid               *
 *                             INVALID, if passed value is invalid           *
 *****************************************************************************/

INT4
check_validity_of_value (INT1 *pi1_syntax, INT1 i1_enum, INT4 i4_defval)
{
    INT4                i4_limlo, i4_limup;
    tTC_TABLE          *p_temp_tc = p_type_defs;
    tNAME_VALUE        *p_temp_name_value;

    if (!strcmp (pi1_syntax, "OCTET STRING") ||
        !strcmp (pi1_syntax, "IpAddress") || !strcmp (pi1_syntax, "Opaque") ||
        !strcmp (pi1_syntax, "BITS"))
    {
        return VALID;
    }
    while (p_temp_tc != NULL)
    {
        if (strcmp (p_temp_tc->tc_name, pi1_syntax) == 0)
        {
            break;
        }
        p_temp_tc = p_temp_tc->next;
    }
    if (p_temp_tc != NULL)
    {
        if (!strcmp (p_temp_tc->tc_base, "OCTET STRING") ||
            !strcmp (p_temp_tc->tc_base, "IpAddress") ||
            !strcmp (p_temp_tc->tc_base, "Opaque"))
        {
            return VALID;
        }
        p_temp_name_value = p_temp_tc->tc_name_value_list;
    }
    else
    {
        p_temp_name_value = p_name_values;
    }

    /* no range may have been given for Gauge, INTEGER etc. */
    if (p_temp_name_value == NULL)
    {
        return VALID;
    }
    while (p_temp_name_value != NULL)
    {
        i4_limlo = p_temp_name_value->value;
        p_temp_name_value = p_temp_name_value->next;
        if (p_temp_name_value != NULL)
        {
            i4_limup = p_temp_name_value->value;
            p_temp_name_value = p_temp_name_value->next;
            if (i4_defval == i4_limlo || i4_defval == i4_limup)
            {
                return VALID;
            }
            if (p_temp_name_value != NULL)
            {
                i4_limup = p_temp_name_value->value;
            }
            if (i4_defval == i4_limlo || i4_defval == i4_limup)
            {
                return VALID;
            }
            if (!i1_enum && (i4_defval > i4_limlo && i4_defval < i4_limup))
            {
                return VALID;
            }
        }
        else
        {
            if (i4_limlo == i4_defval)
            {
                return VALID;
            }
            else
            {
                return INVALID;
            }
        }
    }
    return INVALID;
}
