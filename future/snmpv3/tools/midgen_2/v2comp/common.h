/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: common.h,v 1.4 2015/04/28 14:43:33 siva Exp $
 *
 * Description: This file contains common macros and typedefs   
 *              used by both premosy and postmosy modules      
 *******************************************************************/

#ifndef COMMON_H
#define COMMON_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/stat.h>
#include <unistd.h>

typedef char INT1;
typedef unsigned char UINT1;
typedef short INT2;
typedef unsigned short UINT2;
typedef int INT4;
typedef unsigned int UINT4;
typedef long long int INT8;
#define MID_TRUE 1
#define MID_FALSE 0
#define VALID 1
#define INVALID 0
#define SUCCESS 0
#define ERROR -1
#define MAX_LINE_LEN 256     /* max. length of line */
#define MAX_FILE_NAME_LEN 64 /* max. length of file name */
#define MAX_DESC_LEN 65      /* max. length of descriptor */
/*
 * OID may contain at the most 128 sub identifiers with the last one being
 * a number. Assuming that remaining 127 sub identifiers may be names with
 * a maximum size of 64 the length of OID can be calculated as
 * (127 * max. length of name(64) + max. length of integer (10 digits) +
 *  127 * length of dot(1) + 1 char to store null char)
 */
#define MAX_OID_LEN 8266     /* max. length of OID (127*64+10+127) */
#define PREMOSY_OUTPUT_EXTN ".def"
#define INFOFILE "info.def"

#endif /* COMMOM_H */
