/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: postmosy.c,v 1.4 2015/04/28 14:43:33 siva Exp $
 *
 * Description: main routine which calls the necessary functions.
 *             
 *******************************************************************/

#include "postmosy.h"

INT1               *pi1_file_being_processed;    /* points to name of the file being processed */
tOID_TABLE          recent_oid;    /* stores the recently used object name and it's OID */
tOID_TABLE         *p_oid_list;    /* stores name and OID of all entries */

INT4                compute_sub_oid ();

/*****************************************************************************
 *      Function Name        : postmosy                                      *
 *      Role of the function : Does the file checking and calls other        *
 *                             routines to complete the process.             *
 *      Formal Parameters    : pi1_mib_name                                  *
 *      Global Variables     : None                                          *
 *      Use of Recursion     : None                                          *
 *      Return Value         : SUCCESS on success, ERROR on error            *
 *****************************************************************************/

INT4
postmosy (INT1 *pi1_mib_name)
{
    INT1                i1_response[20];
    INT1               *pi1_dotptr;
    INT1                i1_output_file[MAX_FILE_NAME_LEN];
    INT1                i1_input_file[MAX_FILE_NAME_LEN];
    INT1                i1OnfileName[MAX_FILE_NAME_LEN];
    INT4                i4_index;
    struct stat         buf;

    p_oid_list = NULL;
    memset (i1OnfileName, 0, MAX_FILE_NAME_LEN);
    strcpy (i1_output_file, pi1_mib_name);
    if (pi1_dotptr = strrchr (i1_output_file, '.'))
    {
        *pi1_dotptr = '\0';
    }
    strcpy (i1OnfileName, i1_output_file);
    strcpy (i1_input_file, i1_output_file);
    strcat (i1_input_file, PREMOSY_OUTPUT_EXTN);
    strcat (i1_output_file, POSTMOSY_OUTPUT_EXTN);
    strcat (i1OnfileName, "on.h");
    if (stat (i1_output_file, &buf) == 0)
    {
        fprintf (stderr, "\nWant to overwrite %s ?(yes/no)(y/n): ",
                 i1_output_file);
        fflush (stdout);
        fflush (stdin);
        scanf ("%s", i1_response);
        fflush (stdin);
        if (tolower (i1_response[0]) != 'y')
        {
            return ERROR;
        }
    }
#if DEBUG
    printf ("\nBuilding OID tree for %s...", pi1_mib_name);
    fflush (stdout);
#endif

    /* store standard OID tree till the node "internet" */
    if (initialise_oid_table () == ERROR)
    {
        free_memory (p_oid_list);
        return ERROR;
    }
    if (stat (INFOFILE, &buf) == 0)
    {
        pi1_file_being_processed = INFOFILE;
        if (get_input_fromfile (pi1_file_being_processed) == ERROR)
        {
            free_memory (p_oid_list);
            return ERROR;
        }
    }

    pi1_file_being_processed = i1_input_file;
    if (process_input (pi1_file_being_processed) == ERROR)
    {
        free_memory (p_oid_list);
        return ERROR;
    }

    /* convertion of text form oid into number.number format */
    if (resolve_oids () == ERROR)
    {
        free_memory (p_oid_list);
        return ERROR;
    }

    /* sorting list in order of oids */
    if (sort_oids () == ERROR)
    {
        free_memory (p_oid_list);
        return ERROR;
    }
    /* now dump the contents of linked list to output file */
    if (write_oid_list_to_file (i1_output_file, i1OnfileName) == ERROR)
    {
        free_memory (p_oid_list);
        return ERROR;
    }
    return SUCCESS;
}

/*****************************************************************************
 *      Function Name        : free_memory                                   *
 *      Role of the function : Frees the memory allocated to linked list.    *
 *      Formal Parameters    : p_head                                        *
 *      Global Variables     : None                                          *
 *      Use of Recursion     : None                                          *
 *      Return Value         : None                                          *
 *****************************************************************************/

void
free_memory (tOID_TABLE * p_head)
{
    tOID_TABLE         *p_temp;

    while ((p_temp = p_head) != NULL)
    {
        p_head = p_head->next;
        free (p_temp);
    }
    return;
}

/*****************************************************************************
 *      Function Name        : resolve_oids                                  *
 *      Role of the function : finouts the num.num format oid from text.text *
 *      Formal Parameters    : None                                          *
 *      Global Variables     : p_oid_list                                    *
 *      Use of Recursion     : None                                          *
 *      Return Value         : ERROR/SUCCESS                                 *
 *****************************************************************************/

INT4
resolve_oids ()
{
    tOID_TABLE         *ptr1;
    INT1                i1_oid[MAX_OID_LEN];
    ptr1 = p_oid_list;

    for (; ptr1 != NULL; ptr1 = ptr1->next)
    {
        strcpy (i1_oid, "");
        if (get_oid (ptr1->object_oid, i1_oid) == ERROR)
            return ERROR;
        i1_oid[strlen (i1_oid) - 1] = '\0';
        strcpy (ptr1->object_oid, i1_oid);
    }
    return SUCCESS;
}

/*****************************************************************************
 *      Function Name        : get_oid                                       *
 *      Role of the function : computes the oid in number format.            *
 *      Formal Parameters    : p1_named_oid,p1_oid ( named and numbered oids)*
 *      Global Variables     : None                                          *
 *      Use of Recursion     : None                                          *
 *      Return Value         : ERROR/SUCCESS                                 *
 *****************************************************************************/

INT4
get_oid (INT1 *p1_named_oid, INT1 *p1_oid)
{
    INT1               *p1_oid_follo;

    do
    {
        p1_oid_follo = strchr (p1_named_oid, '.');
        if (p1_oid_follo != NULL)
        {
            *p1_oid_follo = '\0';
            p1_oid_follo++;
        }

        if (isdigit (p1_named_oid[0]))
        {
            strcat (p1_oid, p1_named_oid);
            strcat (p1_oid, ".");
        }
        else if (compute_sub_oid (p1_named_oid, p1_oid) == ERROR)
            return ERROR;
        p1_named_oid = p1_oid_follo;
    }
    while (p1_oid_follo != NULL);
    return SUCCESS;
}

/*****************************************************************************
 *      Function Name        : compute_sub_oid                               *
 *      Role of the function : computes inner part of oid in a textual oid.  *
 *      Formal Parameters    : p1_named_oid,p1_oid(named and numbered oids). *
 *      Global Variables     : p_oid_list                                    *
 *      Use of Recursion     : None                                          *
 *      Return Value         : ERROR/SUCCESS                                 *
 *****************************************************************************/

INT4
compute_sub_oid (INT1 *p1_name_oid, INT1 *p1_oid)
{
    tOID_TABLE         *p_oid_temp = p_oid_list;
    INT1                i1_temp_named_oid[MAX_OID_LEN];

    while (p_oid_temp != NULL)
    {
        if (strcmp (p1_name_oid, p_oid_temp->object_desc) == 0)
        {
            strcpy (i1_temp_named_oid, p_oid_temp->object_oid);
            get_oid (i1_temp_named_oid, p1_oid);
            return SUCCESS;
        }
        p_oid_temp = p_oid_temp->next;
    }

    printf
        ("\n MIB File Does not consist oid of %s object,\n Please enter this object identifier in info.def file \n For further details refer user guide . \nPl. <Enter> to continue. ",
         p1_name_oid);
    getchar ();
    /*    scanf("%s",i1_temp_named_oid);
       add_entry(p1_name_oid,i1_temp_named_oid);
       get_oid(i1_temp_named_oid , p1_oid);  */
    return ERROR;
}

/*****************************************************************************
 *      Function Name        : sort_oids                                     *
 *      Role of the function : sorts all object identifiers in asending order*
 *      Formal Parameters    : None.                                         *
 *      Global Variables     : p_oid_list                                    *
 *      Use of Recursion     : None                                          *
 *      Return Value         : ERROR/SUCCESS                                 *
 *****************************************************************************/

INT4
sort_oids ()
{
    INT4                i4_retval;
    tOID_TABLE         *p_oid_temp1, *p_oid_temp2;
    tOID_TABLE         *p_oid_pre_temp1, *p_oid_pre_temp2;
    p_oid_pre_temp1 = NULL;
    for (p_oid_temp1 = p_oid_list; p_oid_temp1 != NULL;
         p_oid_temp1 = p_oid_temp1->next)
    {
        p_oid_pre_temp2 = p_oid_temp1;
        for (p_oid_temp2 = p_oid_temp1->next; p_oid_temp2 != NULL;
             p_oid_temp2 = p_oid_temp2->next)
        {
            i4_retval =
                compare_oids (p_oid_temp1->object_oid, p_oid_temp2->object_oid);
            if (i4_retval == 0)
            {
                if (strcmp (p_oid_temp1->object_desc, p_oid_temp2->object_desc)
                    == 0)
                {
                    p_oid_pre_temp2->next = p_oid_temp2->next;
                    free (p_oid_temp2);
                    p_oid_temp2 = p_oid_pre_temp2;
                }
                else
                {
                    fprintf (stderr, " ERROR: Duplicate oid assignment \n");
                    fprintf (stderr,
                             "OBJECT 1 \t  : %s \nOBJECT 2 \t  : %s \nOBJECT IDENTIFIER : %s\n",
                             p_oid_temp1->object_desc, p_oid_temp2->object_desc,
                             p_oid_temp1->object_oid);
                    return ERROR;
                }
            }
            else if (i4_retval > 0)
            {
                if (p_oid_pre_temp1 == NULL)
                {
                    p_oid_list = p_oid_temp2;
                    p_oid_pre_temp2->next = p_oid_temp2->next;
                    p_oid_temp2->next = p_oid_pre_temp2;
                    p_oid_temp1 = p_oid_temp2;
                    p_oid_temp2 = p_oid_pre_temp2;
                }
                else
                {
                    p_oid_pre_temp1->next = p_oid_temp2;
                    p_oid_pre_temp2->next = p_oid_temp2->next;
                    p_oid_temp2->next = p_oid_temp1;
                    p_oid_temp1 = p_oid_temp2;
                    p_oid_temp2 = p_oid_pre_temp2;
                }

            }

            p_oid_pre_temp2 = p_oid_temp2;
        }
        p_oid_pre_temp1 = p_oid_temp1;
    }
    return SUCCESS;

}

/*****************************************************************************
 *      Function Name        : get_input_fromfile                            *
 *      Role of the function : Get the OBJECT info from info.def file.       *
 *      Formal Parameters    : p1_file ( file name)                          *
 *      Global Variables     : None                                          *
 *      Use of Recursion     : None                                          *
 *      Return Value         : ERROR/SUCCESS                                 *
 *****************************************************************************/

get_input_fromfile (INT1 *p1_file)
{
    FILE               *fp;
    INT1                i1_line[MAX_LINE_LEN];
    INT1                i1_object_name[MAX_LINE_LEN],
        i1_object_identifier[MAX_LINE_LEN];
    INT1                i1_temp1[MAX_LINE_LEN], i1_temp2[MAX_LINE_LEN];

    if ((fp = fopen (p1_file, "r")) != NULL)
    {
        while (fgets (i1_line, MAX_LINE_LEN, fp) != NULL)
        {
            sscanf (i1_line, "%s %s %s ", i1_object_name, i1_temp1,
                    i1_object_identifier);
            if (strcmp (i1_temp1, "OID") == 0)
            {
                if (add_entry (i1_object_name, i1_object_identifier) == ERROR)
                    return ERROR;
            }
        }
        fclose (fp);
    }
    return SUCCESS;
}
