/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: snmproxdb.h,v 1.1 2015/04/28 12:35:02 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _SNMPROXDB_H
#define _SNMPROXDB_H

UINT1 SnmpProxyTableINDEX [] = {SNMP_DATA_TYPE_IMP_OCTET_PRIM};

UINT4 snmprox [] ={1,3,6,1,6,3,14};
tSNMP_OID_TYPE snmproxOID = {7, snmprox};


UINT4 SnmpProxyName [ ] ={1,3,6,1,6,3,14,1,2,1,1};
UINT4 SnmpProxyType [ ] ={1,3,6,1,6,3,14,1,2,1,2};
UINT4 SnmpProxyContextEngineID [ ] ={1,3,6,1,6,3,14,1,2,1,3};
UINT4 SnmpProxyContextName [ ] ={1,3,6,1,6,3,14,1,2,1,4};
UINT4 SnmpProxyTargetParamsIn [ ] ={1,3,6,1,6,3,14,1,2,1,5};
UINT4 SnmpProxySingleTargetOut [ ] ={1,3,6,1,6,3,14,1,2,1,6};
UINT4 SnmpProxyMultipleTargetOut [ ] ={1,3,6,1,6,3,14,1,2,1,7};
UINT4 SnmpProxyStorageType [ ] ={1,3,6,1,6,3,14,1,2,1,8};
UINT4 SnmpProxyRowStatus [ ] ={1,3,6,1,6,3,14,1,2,1,9};


tMbDbEntry snmproxMibEntry[]= {

{{11,SnmpProxyName}, GetNextIndexSnmpProxyTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, SnmpProxyTableINDEX, 1, 0, 0, NULL},

{{11,SnmpProxyType}, GetNextIndexSnmpProxyTable, SnmpProxyTypeGet, SnmpProxyTypeSet, SnmpProxyTypeTest, SnmpProxyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, SnmpProxyTableINDEX, 1, 0, 0, NULL},

{{11,SnmpProxyContextEngineID}, GetNextIndexSnmpProxyTable, SnmpProxyContextEngineIDGet, SnmpProxyContextEngineIDSet, SnmpProxyContextEngineIDTest, SnmpProxyTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, SnmpProxyTableINDEX, 1, 0, 0, NULL},

{{11,SnmpProxyContextName}, GetNextIndexSnmpProxyTable, SnmpProxyContextNameGet, SnmpProxyContextNameSet, SnmpProxyContextNameTest, SnmpProxyTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, SnmpProxyTableINDEX, 1, 0, 0, NULL},

{{11,SnmpProxyTargetParamsIn}, GetNextIndexSnmpProxyTable, SnmpProxyTargetParamsInGet, SnmpProxyTargetParamsInSet, SnmpProxyTargetParamsInTest, SnmpProxyTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, SnmpProxyTableINDEX, 1, 0, 0, NULL},

{{11,SnmpProxySingleTargetOut}, GetNextIndexSnmpProxyTable, SnmpProxySingleTargetOutGet, SnmpProxySingleTargetOutSet, SnmpProxySingleTargetOutTest, SnmpProxyTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, SnmpProxyTableINDEX, 1, 0, 0, NULL},

{{11,SnmpProxyMultipleTargetOut}, GetNextIndexSnmpProxyTable, SnmpProxyMultipleTargetOutGet, SnmpProxyMultipleTargetOutSet, SnmpProxyMultipleTargetOutTest, SnmpProxyTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, SnmpProxyTableINDEX, 1, 0, 0, NULL},

{{11,SnmpProxyStorageType}, GetNextIndexSnmpProxyTable, SnmpProxyStorageTypeGet, SnmpProxyStorageTypeSet, SnmpProxyStorageTypeTest, SnmpProxyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, SnmpProxyTableINDEX, 1, 0, 0, "3"},

{{11,SnmpProxyRowStatus}, GetNextIndexSnmpProxyTable, SnmpProxyRowStatusGet, SnmpProxyRowStatusSet, SnmpProxyRowStatusTest, SnmpProxyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, SnmpProxyTableINDEX, 1, 0, 1, NULL},
};
tMibData snmproxEntry = { 9, snmproxMibEntry };
#endif /* _SNMPROXDB_H */

