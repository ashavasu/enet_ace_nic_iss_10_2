/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssnmp3wr.c,v 1.5 2015/04/28 12:35:02 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#include  "lr.h"
#include  "fssnmp.h"
#include  "fssnmp3lw.h"
#include  "fssnmp3wr.h"
#include  "fssnmp3db.h"

VOID
RegisterFSSNMP3 ()
{
    SNMPRegisterMib (&fssnmpOID, &fssnmpEntry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fssnmpOID, (const UINT1 *) "fssnmp3");
}

VOID
UnRegisterFSSNMP3 ()
{
    SNMPUnRegisterMib (&fssnmpOID, &fssnmpEntry);
    SNMPDelSysorEntry (&fssnmpOID, (const UINT1 *) "fssnmp3");
}

INT4
SnmpInInformResponsesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpInInformResponses (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpOutInformRequestsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpOutInformRequests (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpInformDropsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpInformDrops (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpInformAwaitingAckGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpInformAwaitingAck (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpListenTrapPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpListenTrapPort (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpOverTcpStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpOverTcpStatus (&(pMultiData->i4_SLongValue)));
}

INT4
SnmpListenTcpPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpListenTcpPort (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpTrapOverTcpStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpTrapOverTcpStatus (&(pMultiData->i4_SLongValue)));
}

INT4
SnmpListenTcpTrapPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpListenTcpTrapPort (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpListenTrapPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSnmpListenTrapPort (pMultiData->u4_ULongValue));
}

INT4
SnmpOverTcpStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSnmpOverTcpStatus (pMultiData->i4_SLongValue));
}

INT4
SnmpListenTcpPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSnmpListenTcpPort (pMultiData->u4_ULongValue));
}

INT4
SnmpTrapOverTcpStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSnmpTrapOverTcpStatus (pMultiData->i4_SLongValue));
}

INT4
SnmpListenTcpTrapPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSnmpListenTcpTrapPort (pMultiData->u4_ULongValue));
}

INT4
SnmpListenTrapPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SnmpListenTrapPort (pu4Error, pMultiData->u4_ULongValue));
}

INT4
SnmpOverTcpStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SnmpOverTcpStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
SnmpListenTcpPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SnmpListenTcpPort (pu4Error, pMultiData->u4_ULongValue));
}

INT4
SnmpTrapOverTcpStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SnmpTrapOverTcpStatus
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
SnmpListenTcpTrapPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SnmpListenTcpTrapPort
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
SnmpListenTrapPortDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SnmpListenTrapPort
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
SnmpOverTcpStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SnmpOverTcpStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
SnmpListenTcpPortDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SnmpListenTcpPort
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
SnmpTrapOverTcpStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SnmpTrapOverTcpStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
SnmpListenTcpTrapPortDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SnmpListenTcpTrapPort
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexSnmpInformCntTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexSnmpInformCntTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexSnmpInformCntTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
SnmpInformSentGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSnmpInformCntTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSnmpInformSent (pMultiIndex->pIndex[0].pOctetStrValue,
                                  &(pMultiData->u4_ULongValue)));

}

INT4
SnmpInformAwaitAckGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSnmpInformCntTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSnmpInformAwaitAck (pMultiIndex->pIndex[0].pOctetStrValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
SnmpInformRetriedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSnmpInformCntTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSnmpInformRetried (pMultiIndex->pIndex[0].pOctetStrValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
SnmpInformDroppedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSnmpInformCntTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSnmpInformDropped (pMultiIndex->pIndex[0].pOctetStrValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
SnmpInformFailedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSnmpInformCntTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSnmpInformFailed (pMultiIndex->pIndex[0].pOctetStrValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
SnmpInformResponsesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSnmpInformCntTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSnmpInformResponses (pMultiIndex->pIndex[0].pOctetStrValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
SnmpColdStartTrapControlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpColdStartTrapControl (&(pMultiData->i4_SLongValue)));
}

INT4
SnmpAgentControlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpAgentControl (&(pMultiData->i4_SLongValue)));
}

INT4
SnmpAllowedPduVersionsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpAllowedPduVersions (&(pMultiData->i4_SLongValue)));
}

INT4
SnmpMinimumSecurityRequiredGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpMinimumSecurityRequired (&(pMultiData->i4_SLongValue)));
}

INT4
SnmpInRollbackErrsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpInRollbackErrs (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpProxyListenTrapPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpProxyListenTrapPort (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpColdStartTrapControlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSnmpColdStartTrapControl (pMultiData->i4_SLongValue));
}

INT4
SnmpAgentControlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSnmpAgentControl (pMultiData->i4_SLongValue));
}

INT4
SnmpAllowedPduVersionsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSnmpAllowedPduVersions (pMultiData->i4_SLongValue));
}

INT4
SnmpMinimumSecurityRequiredSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSnmpMinimumSecurityRequired (pMultiData->i4_SLongValue));
}

INT4
SnmpProxyListenTrapPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSnmpProxyListenTrapPort (pMultiData->u4_ULongValue));
}

INT4
SnmpColdStartTrapControlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SnmpColdStartTrapControl
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
SnmpAgentControlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SnmpAgentControl (pu4Error, pMultiData->i4_SLongValue));
}

INT4
SnmpAllowedPduVersionsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SnmpAllowedPduVersions
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
SnmpMinimumSecurityRequiredTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SnmpMinimumSecurityRequired
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
SnmpProxyListenTrapPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SnmpProxyListenTrapPort
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
SnmpColdStartTrapControlDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SnmpColdStartTrapControl
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
SnmpAgentControlDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SnmpAgentControl (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
SnmpAllowedPduVersionsDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SnmpAllowedPduVersions
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
SnmpMinimumSecurityRequiredDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SnmpMinimumSecurityRequired
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
SnmpProxyListenTrapPortDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SnmpProxyListenTrapPort
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsSnmpProxyTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsSnmpProxyTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsSnmpProxyTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsSnmpProxyMibTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsSnmpProxyTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsSnmpProxyMibType (pMultiIndex->pIndex[0].pOctetStrValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsSnmpProxyMibIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsSnmpProxyTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsSnmpProxyMibId (pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiData->pOidValue));

}

INT4
FsSnmpProxyMibTargetParamsInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsSnmpProxyTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsSnmpProxyMibTargetParamsIn
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsSnmpProxyMibSingleTargetOutGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsSnmpProxyTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsSnmpProxyMibSingleTargetOut
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsSnmpProxyMibMultipleTargetOutGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsSnmpProxyTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsSnmpProxyMibMultipleTargetOut
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsSnmpProxyMibStorageTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsSnmpProxyTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsSnmpProxyMibStorageType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsSnmpProxyMibRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsSnmpProxyTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsSnmpProxyMibRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsSnmpProxyMibTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsSnmpProxyMibType (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsSnmpProxyMibIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsSnmpProxyMibId (pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiData->pOidValue));

}

INT4
FsSnmpProxyMibTargetParamsInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsSnmpProxyMibTargetParamsIn
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsSnmpProxyMibSingleTargetOutSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsSnmpProxyMibSingleTargetOut
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsSnmpProxyMibMultipleTargetOutSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsSnmpProxyMibMultipleTargetOut
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsSnmpProxyMibStorageTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsSnmpProxyMibStorageType
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsSnmpProxyMibRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsSnmpProxyMibRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsSnmpProxyMibTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsSnmpProxyMibType (pu4Error,
                                         pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsSnmpProxyMibIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2FsSnmpProxyMibId (pu4Error,
                                       pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiData->pOidValue));

}

INT4
FsSnmpProxyMibTargetParamsInTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsSnmpProxyMibTargetParamsIn (pu4Error,
                                                   pMultiIndex->
                                                   pIndex[0].pOctetStrValue,
                                                   pMultiData->pOctetStrValue));

}

INT4
FsSnmpProxyMibSingleTargetOutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsSnmpProxyMibSingleTargetOut (pu4Error,
                                                    pMultiIndex->
                                                    pIndex[0].pOctetStrValue,
                                                    pMultiData->pOctetStrValue));

}

INT4
FsSnmpProxyMibMultipleTargetOutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2FsSnmpProxyMibMultipleTargetOut (pu4Error,
                                                      pMultiIndex->
                                                      pIndex[0].pOctetStrValue,
                                                      pMultiData->pOctetStrValue));

}

INT4
FsSnmpProxyMibStorageTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsSnmpProxyMibStorageType (pu4Error,
                                                pMultiIndex->
                                                pIndex[0].pOctetStrValue,
                                                pMultiData->i4_SLongValue));

}

INT4
FsSnmpProxyMibRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsSnmpProxyMibRowStatus (pu4Error,
                                              pMultiIndex->
                                              pIndex[0].pOctetStrValue,
                                              pMultiData->i4_SLongValue));

}

INT4
FsSnmpProxyTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSnmpProxyTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSnmpListenAgentPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSnmpListenAgentPort (&(pMultiData->u4_ULongValue)));
}

INT4
FsSnmpEngineIDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSnmpEngineID (pMultiData->pOctetStrValue));
}

INT4
FsSnmpListenAgentPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSnmpListenAgentPort (pMultiData->u4_ULongValue));
}

INT4
FsSnmpEngineIDSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSnmpEngineID (pMultiData->pOctetStrValue));
}

INT4
FsSnmpListenAgentPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSnmpListenAgentPort
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsSnmpEngineIDTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSnmpEngineID (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsSnmpListenAgentPortDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSnmpListenAgentPort
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSnmpEngineIDDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSnmpEngineID (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
SnmpAgentxTransportDomainGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpAgentxTransportDomain (&(pMultiData->i4_SLongValue)));
}

INT4
SnmpAgentxMasterAgentAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpAgentxMasterAgentAddr (pMultiData->pOctetStrValue));
}

INT4
SnmpAgentxMasterAgentPortNoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpAgentxMasterAgentPortNo (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpAgentxSubAgentInPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpAgentxSubAgentInPkts (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpAgentxSubAgentOutPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpAgentxSubAgentOutPkts (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpAgentxSubAgentPktDropsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpAgentxSubAgentPktDrops (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpAgentxSubAgentParseDropsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpAgentxSubAgentParseDrops (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpAgentxSubAgentInOpenFailGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpAgentxSubAgentInOpenFail (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpAgentxSubAgentOpenPktCntGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpAgentxSubAgentOpenPktCnt (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpAgentxSubAgentInClosePktCntGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpAgentxSubAgentInClosePktCnt
            (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpAgentxSubAgentOutClosePktCntGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpAgentxSubAgentOutClosePktCnt
            (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpAgentxSubAgentIdAllocPktCntGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpAgentxSubAgentIdAllocPktCnt
            (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpAgentxSubAgentIdDllocPktCntGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpAgentxSubAgentIdDllocPktCnt
            (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpAgentxSubAgentRegPktCntGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpAgentxSubAgentRegPktCnt (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpAgentxSubAgentUnRegPktCntGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpAgentxSubAgentUnRegPktCnt (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpAgentxSubAgentAddCapsCntGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpAgentxSubAgentAddCapsCnt (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpAgentxSubAgentRemCapsCntGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpAgentxSubAgentRemCapsCnt (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpAgentxSubAgentNotifyPktCntGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpAgentxSubAgentNotifyPktCnt
            (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpAgentxSubAgentPingCntGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpAgentxSubAgentPingCnt (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpAgentxSubAgentInGetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpAgentxSubAgentInGets (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpAgentxSubAgentInGetNextsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpAgentxSubAgentInGetNexts (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpAgentxSubAgentInGetBulksGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpAgentxSubAgentInGetBulks (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpAgentxSubAgentInTestSetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpAgentxSubAgentInTestSets (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpAgentxSubAgentInCommitsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpAgentxSubAgentInCommits (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpAgentxSubAgentInCleanupsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpAgentxSubAgentInCleanups (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpAgentxSubAgentInUndosGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpAgentxSubAgentInUndos (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpAgentxSubAgentOutResponseGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpAgentxSubAgentOutResponse (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpAgentxSubAgentInResponseGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpAgentxSubAgentInResponse (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpAgentxSubAgentControlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpAgentxSubAgentControl (&(pMultiData->i4_SLongValue)));
}

INT4
SnmpAgentxTransportDomainSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSnmpAgentxTransportDomain (pMultiData->i4_SLongValue));
}

INT4
SnmpAgentxMasterAgentAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSnmpAgentxMasterAgentAddr (pMultiData->pOctetStrValue));
}

INT4
SnmpAgentxMasterAgentPortNoSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSnmpAgentxMasterAgentPortNo (pMultiData->u4_ULongValue));
}

INT4
SnmpAgentxSubAgentControlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSnmpAgentxSubAgentControl (pMultiData->i4_SLongValue));
}

INT4
SnmpAgentxTransportDomainTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SnmpAgentxTransportDomain
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
SnmpAgentxMasterAgentAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SnmpAgentxMasterAgentAddr
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
SnmpAgentxMasterAgentPortNoTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SnmpAgentxMasterAgentPortNo
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
SnmpAgentxSubAgentControlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SnmpAgentxSubAgentControl
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
SnmpAgentxTransportDomainDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SnmpAgentxTransportDomain
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
SnmpAgentxMasterAgentAddrDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SnmpAgentxMasterAgentAddr
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
SnmpAgentxMasterAgentPortNoDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SnmpAgentxMasterAgentPortNo
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
SnmpAgentxSubAgentControlDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SnmpAgentxSubAgentControl
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
SnmpAgentxContextNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpAgentxContextName (pMultiData->pOctetStrValue));
}

INT4
SnmpAgentxContextNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSnmpAgentxContextName (pMultiData->pOctetStrValue));
}

INT4
SnmpAgentxContextNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SnmpAgentxContextName
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
SnmpAgentxContextNameDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SnmpAgentxContextName
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsSnmpTrapFilterTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsSnmpTrapFilterTable
            (pNextMultiIndex->pIndex[0].pOidValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsSnmpTrapFilterTable
            (pFirstMultiIndex->pIndex[0].pOidValue,
             pNextMultiIndex->pIndex[0].pOidValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsSnmpTrapFilterRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsSnmpTrapFilterTable
        (pMultiIndex->pIndex[0].pOidValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsSnmpTrapFilterRowStatus (pMultiIndex->pIndex[0].pOidValue,
                                             &(pMultiData->i4_SLongValue)));

}

INT4
FsSnmpTrapFilterRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsSnmpTrapFilterRowStatus (pMultiIndex->pIndex[0].pOidValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsSnmpTrapFilterRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsSnmpTrapFilterRowStatus (pu4Error,
                                                pMultiIndex->
                                                pIndex[0].pOidValue,
                                                pMultiData->i4_SLongValue));

}

INT4
FsSnmpTrapFilterTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSnmpTrapFilterTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsSnmpTargetAddrTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsSnmpTargetAddrTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsSnmpTargetAddrTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsSnmpTargetHostNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsSnmpTargetAddrTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsSnmpTargetHostName (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FsSnmpTargetHostNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsSnmpTargetHostName (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FsSnmpTargetHostNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsSnmpTargetHostName (pu4Error,
                                           pMultiIndex->pIndex[0].
                                           pOctetStrValue,
                                           pMultiData->pOctetStrValue));

}

INT4
FsSnmpTargetAddrTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSnmpTargetAddrTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
