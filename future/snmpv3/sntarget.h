/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: sntarget.h,v 1.4 2015/04/28 12:35:03 siva Exp $
 *
 * Description: prototypes for Target Address Table 
 *******************************************************************/
#ifndef _SNTARGET_H
#define _SNTARGET_H
tSnmpTgtAddrEntry   gSnmpTgtAddrTable[MAX_TARGET_ADDR_ENTRY];
tTMO_SLL            gSnmpTgtAddrSll;

tSnmpTgtParamEntry  gSnmpTgtParamTable[SNMP_MAX_TGT_PARAM_ENTRY];
tTMO_SLL            gSnmpTgtParamSll;
tSNMP_INFORM_STAT   gSnmpTgtInformStatArr[MAX_TARGET_ADDR_ENTRY];

UINT1 gau1AddrName[MAX_TARGET_ADDR_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
UINT1 gau1Address[MAX_TARGET_ADDR_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
UINT1 gau1TagList[MAX_TARGET_ADDR_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
UINT1 gau1AddrParam[MAX_TARGET_ADDR_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
UINT4 gau4Domain[MAX_TARGET_ADDR_ENTRY][MAX_OID_LENGTH]; 
UINT1 gau1ParamName[SNMP_MAX_TGT_PARAM_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
UINT1 gau1ParamSecName[SNMP_MAX_TGT_PARAM_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
VOID  SNMPAddTgtAddrSll (tTMO_SLL_NODE *);
VOID  SNMPDelTgtAddrSll (tTMO_SLL_NODE *);
VOID  SNMPAddTgtParamSll (tTMO_SLL_NODE *);
VOID  SNMPDelTgtParamSll (tTMO_SLL_NODE *);
#endif
