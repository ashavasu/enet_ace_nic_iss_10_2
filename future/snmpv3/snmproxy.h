/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved 
 *
 * $Id: snmproxy.h,v 1.1 2015/04/28 12:35:02 siva Exp $
 *
 * Description: snmp Proxy Table macros and global vars
 *******************************************************************/
#ifndef _SNMPROX_H
#define _SNMPROX_H
tProxyTableEntry   gaProxyTableEntry[MAX_PROXY_TABLE_ENTRY];
tTMO_SLL           gProxyTableSll;

UINT1 au1ProxyName[MAX_PROXY_TABLE_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
UINT1 au1ProxCntxEngID[MAX_PROXY_TABLE_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
UINT1 au1ProxContName[MAX_PROXY_TABLE_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
UINT1 au1ProxTgtPrmIn[MAX_PROX_TGT_PARAM_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
UINT1 au1ProxSglTgtOut[MAX_PROXY_TABLE_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
UINT1 au1ProxMulTgtOut[MAX_PROXY_TABLE_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];

VOID  SNMPAddProxyEntrySll (tTMO_SLL_NODE *);
#endif
