/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: snmproxlw.h,v 1.1 2015/04/28 12:35:02 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for SnmpProxyTable. */
INT1
nmhValidateIndexInstanceSnmpProxyTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for SnmpProxyTable  */

INT1
nmhGetFirstIndexSnmpProxyTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexSnmpProxyTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetSnmpProxyType ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetSnmpProxyContextEngineID ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetSnmpProxyContextName ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetSnmpProxyTargetParamsIn ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetSnmpProxySingleTargetOut ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetSnmpProxyMultipleTargetOut ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetSnmpProxyStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetSnmpProxyRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetSnmpProxyType ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetSnmpProxyContextEngineID ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetSnmpProxyContextName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetSnmpProxyTargetParamsIn ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetSnmpProxySingleTargetOut ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetSnmpProxyMultipleTargetOut ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetSnmpProxyStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetSnmpProxyRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2SnmpProxyType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2SnmpProxyContextEngineID ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2SnmpProxyContextName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2SnmpProxyTargetParamsIn ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2SnmpProxySingleTargetOut ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2SnmpProxyMultipleTargetOut ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2SnmpProxyStorageType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2SnmpProxyRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));
/* Low Level DEP Routines for.  */

INT1
nmhDepv2SnmpProxyTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
