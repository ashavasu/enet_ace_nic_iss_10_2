
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: stdsnmlw.h,v 1.4 2015/04/28 12:35:03 siva Exp $
 *
 * Description: Prototypes for Snmp Agent std mib Module
 *******************************************************************/
#ifndef _STDSNMLW_H
#define _STDSNMLW_H
/* Proto Validate Index Instance for SysORTable. */
INT1
nmhValidateIndexInstanceSysORTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for SysORTable  */

INT1
nmhGetFirstIndexSysORTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexSysORTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetSysORID ARG_LIST((INT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetSysORDescr ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetSysORUpTime ARG_LIST((INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetSysDescr ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetSysObjectID ARG_LIST((tSNMP_OID_TYPE * ));

INT1
nmhGetSysUpTime ARG_LIST((UINT4 *));

INT1
nmhGetSysContact ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetSysName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetSysLocation ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetSysServices ARG_LIST((INT4 *));

INT1
nmhGetSysORLastChange ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetSysContact ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetSysName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetSysLocation ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2SysContact ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2SysName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2SysLocation ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2SysContact ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2SysName ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2SysLocation ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetSnmpInPkts ARG_LIST((UINT4 *));

INT1
nmhGetSnmpOutPkts ARG_LIST((UINT4 *));

INT1
nmhGetSnmpInBadVersions ARG_LIST((UINT4 *));

INT1
nmhGetSnmpInBadCommunityNames ARG_LIST((UINT4 *));

INT1
nmhGetSnmpInBadCommunityUses ARG_LIST((UINT4 *));

INT1
nmhGetSnmpInASNParseErrs ARG_LIST((UINT4 *));

INT1
nmhGetSnmpInBadTypes ARG_LIST((UINT4 *));

INT1
nmhGetSnmpInTooBigs ARG_LIST((UINT4 *));

INT1
nmhGetSnmpInNoSuchNames ARG_LIST((UINT4 *));

INT1
nmhGetSnmpInBadValues ARG_LIST((UINT4 *));

INT1
nmhGetSnmpInReadOnlys ARG_LIST((UINT4 *));

INT1
nmhGetSnmpInGenErrs ARG_LIST((UINT4 *));

INT1
nmhGetSnmpInTotalReqVars ARG_LIST((UINT4 *));

INT1
nmhGetSnmpInTotalSetVars ARG_LIST((UINT4 *));

INT1
nmhGetSnmpInGetRequests ARG_LIST((UINT4 *));

INT1
nmhGetSnmpInGetNexts ARG_LIST((UINT4 *));

INT1
nmhGetSnmpInSetRequests ARG_LIST((UINT4 *));

INT1
nmhGetSnmpInGetResponses ARG_LIST((UINT4 *));

INT1
nmhGetSnmpInTraps ARG_LIST((UINT4 *));

INT1
nmhGetSnmpOutTooBigs ARG_LIST((UINT4 *));

INT1
nmhGetSnmpOutNoSuchNames ARG_LIST((UINT4 *));

INT1
nmhGetSnmpOutBadValues ARG_LIST((UINT4 *));

INT1
nmhGetSnmpOutReadOnlys ARG_LIST((UINT4 *));

INT1
nmhGetSnmpOutGenErrs ARG_LIST((UINT4 *));

INT1
nmhGetSnmpOutGetRequests ARG_LIST((UINT4 *));

INT1
nmhGetSnmpOutGetNexts ARG_LIST((UINT4 *));

INT1
nmhGetSnmpOutSetRequests ARG_LIST((UINT4 *));

INT1
nmhGetSnmpOutGetResponses ARG_LIST((UINT4 *));

INT1
nmhGetSnmpOutTraps ARG_LIST((UINT4 *));

INT1
nmhGetSnmpEnableAuthenTraps ARG_LIST((INT4 *));

INT1
nmhGetSnmpSilentDrops ARG_LIST((UINT4 *));

INT1
nmhGetSnmpProxyDrops ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetSnmpEnableAuthenTraps ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2SnmpEnableAuthenTraps ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2SnmpEnableAuthenTraps ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetSnmpSetSerialNo ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetSnmpSetSerialNo ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2SnmpSetSerialNo ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2SnmpSetSerialNo ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
#endif /* _STDSNMLW_H */
