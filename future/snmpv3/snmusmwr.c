/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: snmusmwr.c,v 1.4 2015/04/28 12:35:03 siva Exp $
*
* Description: Usm Wrapper  Routines
*********************************************************************/

# include  "lr.h"
# include  "fssnmp.h"
# include  "snmusmlw.h"
# include  "snmusmwr.h"
# include  "snmusmdb.h"

VOID
RegisterSNMUSM ()
{
    SNMPRegisterMib (&snmusmOID, &snmusmEntry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&snmusmOID, (const UINT1 *) "snmusm");
}

INT4
UsmStatsUnsupportedSecLevelsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetUsmStatsUnsupportedSecLevels (&(pMultiData->u4_ULongValue)));
}

INT4
UsmStatsNotInTimeWindowsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetUsmStatsNotInTimeWindows (&(pMultiData->u4_ULongValue)));
}

INT4
UsmStatsUnknownUserNamesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetUsmStatsUnknownUserNames (&(pMultiData->u4_ULongValue)));
}

INT4
UsmStatsUnknownEngineIDsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetUsmStatsUnknownEngineIDs (&(pMultiData->u4_ULongValue)));
}

INT4
UsmStatsWrongDigestsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetUsmStatsWrongDigests (&(pMultiData->u4_ULongValue)));
}

INT4
UsmStatsDecryptionErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetUsmStatsDecryptionErrors (&(pMultiData->u4_ULongValue)));
}

INT4
UsmUserSpinLockGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetUsmUserSpinLock (&(pMultiData->i4_SLongValue)));
}

INT4
UsmUserSpinLockSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetUsmUserSpinLock (pMultiData->i4_SLongValue));
}

INT4
UsmUserSpinLockTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2UsmUserSpinLock (pu4Error, pMultiData->i4_SLongValue));
}

INT4
UsmUserSpinLockDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2UsmUserSpinLock (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexUsmUserTable (tSnmpIndex * pFirstMultiIndex,
                          tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexUsmUserTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexUsmUserTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
UsmUserEngineIDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceUsmUserTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[0].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[0].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[0].pOctetStrValue->i4_Length;

    return SNMP_SUCCESS;

}

INT4
UsmUserNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceUsmUserTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[1].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[1].pOctetStrValue->i4_Length;

    return SNMP_SUCCESS;

}

INT4
UsmUserSecurityNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceUsmUserTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetUsmUserSecurityName (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiData->pOctetStrValue));

}

INT4
UsmUserCloneFromGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceUsmUserTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetUsmUserCloneFrom (pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiData->pOidValue));

}

INT4
UsmUserAuthProtocolGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceUsmUserTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetUsmUserAuthProtocol (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiData->pOidValue));

}

INT4
UsmUserAuthKeyChangeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceUsmUserTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetUsmUserAuthKeyChange (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiData->pOctetStrValue));

}

INT4
UsmUserOwnAuthKeyChangeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceUsmUserTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetUsmUserOwnAuthKeyChange
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
UsmUserPrivProtocolGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceUsmUserTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetUsmUserPrivProtocol (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiData->pOidValue));

}

INT4
UsmUserPrivKeyChangeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceUsmUserTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetUsmUserPrivKeyChange (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiData->pOctetStrValue));

}

INT4
UsmUserOwnPrivKeyChangeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceUsmUserTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetUsmUserOwnPrivKeyChange
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
UsmUserPublicGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceUsmUserTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetUsmUserPublic (pMultiIndex->pIndex[0].pOctetStrValue,
                                 pMultiIndex->pIndex[1].pOctetStrValue,
                                 pMultiData->pOctetStrValue));

}

INT4
UsmUserStorageTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceUsmUserTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetUsmUserStorageType (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
UsmUserStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceUsmUserTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetUsmUserStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                 pMultiIndex->pIndex[1].pOctetStrValue,
                                 &(pMultiData->i4_SLongValue)));

}

INT4
UsmUserCloneFromSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetUsmUserCloneFrom (pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiData->pOidValue));

}

INT4
UsmUserAuthProtocolSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetUsmUserAuthProtocol (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiData->pOidValue));

}

INT4
UsmUserAuthKeyChangeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetUsmUserAuthKeyChange (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiData->pOctetStrValue));

}

INT4
UsmUserOwnAuthKeyChangeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetUsmUserOwnAuthKeyChange
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
UsmUserPrivProtocolSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetUsmUserPrivProtocol (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiData->pOidValue));

}

INT4
UsmUserPrivKeyChangeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetUsmUserPrivKeyChange (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiData->pOctetStrValue));

}

INT4
UsmUserOwnPrivKeyChangeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetUsmUserOwnPrivKeyChange
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
UsmUserPublicSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetUsmUserPublic (pMultiIndex->pIndex[0].pOctetStrValue,
                                 pMultiIndex->pIndex[1].pOctetStrValue,
                                 pMultiData->pOctetStrValue));

}

INT4
UsmUserStorageTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetUsmUserStorageType (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      pMultiData->i4_SLongValue));

}

INT4
UsmUserStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetUsmUserStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                 pMultiIndex->pIndex[1].pOctetStrValue,
                                 pMultiData->i4_SLongValue));

}

INT4
UsmUserCloneFromTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2UsmUserCloneFrom (pu4Error,
                                       pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiData->pOidValue));

}

INT4
UsmUserAuthProtocolTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2UsmUserAuthProtocol (pu4Error,
                                          pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiData->pOidValue));

}

INT4
UsmUserAuthKeyChangeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2UsmUserAuthKeyChange (pu4Error,
                                           pMultiIndex->pIndex[0].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiData->pOctetStrValue));

}

INT4
UsmUserOwnAuthKeyChangeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2UsmUserOwnAuthKeyChange (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              pOctetStrValue,
                                              pMultiIndex->pIndex[1].
                                              pOctetStrValue,
                                              pMultiData->pOctetStrValue));

}

INT4
UsmUserPrivProtocolTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2UsmUserPrivProtocol (pu4Error,
                                          pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiData->pOidValue));

}

INT4
UsmUserPrivKeyChangeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2UsmUserPrivKeyChange (pu4Error,
                                           pMultiIndex->pIndex[0].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiData->pOctetStrValue));

}

INT4
UsmUserOwnPrivKeyChangeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2UsmUserOwnPrivKeyChange (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              pOctetStrValue,
                                              pMultiIndex->pIndex[1].
                                              pOctetStrValue,
                                              pMultiData->pOctetStrValue));

}

INT4
UsmUserPublicTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    return (nmhTestv2UsmUserPublic (pu4Error,
                                    pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiData->pOctetStrValue));

}

INT4
UsmUserStorageTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2UsmUserStorageType (pu4Error,
                                         pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         pMultiData->i4_SLongValue));

}

INT4
UsmUserStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    return (nmhTestv2UsmUserStatus (pu4Error,
                                    pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiData->i4_SLongValue));

}

INT4
UsmUserTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2UsmUserTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
