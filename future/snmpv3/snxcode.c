/*****************************************************************************
 * $Id: snxcode.c,v 1.5 2015/04/28 12:35:03 siva Exp $
 *
 * Description: This file contains routines for AgentX encoding/decoding. 
 ****************************************************************************/
#include "snxinc.h"
tSnmpAgentxOidType  TempEndId;
/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxCodeDecodePacket
 *                                                                          
 *    DESCRIPTION      : This function Decodes the AgentX data from the buffer.  
 *
 *    INPUT            : pu1Buffer  - Pointer to the linear buffer
 *                       i4PktLen    - Total length of the packet
 *                                                                          
 *    OUTPUT           : pAgentxPdu - Pointer to the AgentX PDU
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 *
 ****************************************************************************/
INT4
SnxCodeDecodePacket (UINT1 *pu1Buffer, INT4 i4PktLen,
                     tSnmpAgentxPdu * pAgentxPdu)
{
    UINT4               u4OffSet = 0;
    INT4                i4RetVal = SNMP_FAILURE;

    /* Decoding AgentX Header from the received Buffer */

    if (i4PktLen < SNX_AGENTX_PDU_HDR_LEN)
    {
        gAgtxStats.u4PktDrops++;
        SNMPTrace ("Packet Length mismatch. Packet dropped\n");
        return i4RetVal;
    }

    SNX_GET_1BYTE (pu1Buffer, u4OffSet, pAgentxPdu->u1Version);

    if (pAgentxPdu->u1Version != SNX_AGENTX_VERSION)
    {
        gAgtxStats.u4PktDrops++;
        SNMPTrace ("Incorrect Version. Packet dropped\n");
        return i4RetVal;
    }

    SNX_GET_1BYTE (pu1Buffer, u4OffSet, pAgentxPdu->u1PduType);

    SNX_GET_1BYTE (pu1Buffer, u4OffSet, pAgentxPdu->u1Flags);

    SNX_GET_1BYTE (pu1Buffer, u4OffSet, pAgentxPdu->u1Reserved);

    SnxCodeGet4ByteData (pu1Buffer, &u4OffSet, pAgentxPdu->u1Flags,
                         (UINT4 *) &(pAgentxPdu->i4SessionId));

    SnxCodeGet4ByteData (pu1Buffer, &u4OffSet, pAgentxPdu->u1Flags,
                         &(pAgentxPdu->u4TransactionId));

    SnxCodeGet4ByteData (pu1Buffer, &u4OffSet, pAgentxPdu->u1Flags,
                         &(pAgentxPdu->u4PacketId));

    SnxCodeGet4ByteData (pu1Buffer, &u4OffSet, pAgentxPdu->u1Flags,
                         &(pAgentxPdu->u4PayLoadLen));

    if (i4PktLen < (INT4) (pAgentxPdu->u4PayLoadLen + SNX_AGENTX_PDU_HDR_LEN))
    {
        gAgtxStats.u4ParseDrops++;
        SNMPTrace ("Packet Length mismatch. Packet dropped\n");
        return i4RetVal;
    }

    /* Decoding Pdu specific data (Close, Response, Get, GetNext, GetBulk,
     * Testset, CommitSet, UndoSet or CleanupSet) */
    i4RetVal = SnxCodeDecodePduSpecificData (pu1Buffer, u4OffSet, pAgentxPdu);

    if ((i4RetVal == SNMP_SUCCESS) &&
        (i4PktLen > (INT4) (pAgentxPdu->u4PayLoadLen + SNX_AGENTX_PDU_HDR_LEN)))
    {
        return SNX_MUL_REQ_PKT;
    }

    return i4RetVal;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxCodeGet4ByteData
 *                                                                          
 *    DESCRIPTION      : This function Decodes the four byte AgentX data 
 *                       from the buffer based on the Flag.  
 *
 *    INPUT            : pu1Buffer  - Pointer to the linear buffer
 *                       pu4OffSet  - Pointer to the Offset
 *                       u1Flags     - Flag bit which specifies 
 *                                     NETWORK_BYTE_ORDER_BIT is set or not
 *                                                                          
 *    OUTPUT           : pu4Data - Pointer to the four byte variable
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE 
 *    
 ****************************************************************************/
VOID
SnxCodeGet4ByteData (UINT1 *pu1Buffer, UINT4 *pu4OffSet, UINT1 u1Flags,
                     UINT4 *pu4Data)
{
    UINT1              *pu1Pkt = pu1Buffer;

    pu1Pkt = pu1Pkt + (*pu4OffSet);

    MEMCPY (pu4Data, pu1Pkt, SNX_4BYTE_DATALEN);

    if (u1Flags & SNX_NETWORK_BYTE_ORDER)
    {
        *pu4Data = OSIX_NTOHL (*pu4Data);
    }

    (*pu4OffSet) += SNX_4BYTE_DATALEN;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxCodePut4ByteData
 *                                                                          
 *    DESCRIPTION      : This function encodes the four byte AgentX data 
 *                       from the buffer based on the Flag.  
 *
 *    INPUT            :  pu4Data - Pointer to the four byte variable
 *                        pu4OffSet  - Pointer to the Offset
 *                        u1Flags    - Flag bit which specifies 
 *                                     NETWORK_BYTE_ORDER_BIT is set or not
 *                                                                          
 *    OUTPUT           :  pu1Buffer  - Pointer to the linear buffer
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE 
 *    
 ****************************************************************************/
VOID
SnxCodePut4ByteData (UINT4 u4Data, UINT4 *pu4OffSet, UINT1 u1Flags,
                     UINT1 *pu1Buffer)
{
    if (u1Flags & SNX_NETWORK_BYTE_ORDER)
    {
        u4Data = OSIX_HTONL (u4Data);
    }

    MEMCPY (pu1Buffer + (*pu4OffSet), &u4Data, SNX_4BYTE_DATALEN);

    (*pu4OffSet) += SNX_4BYTE_DATALEN;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxCodeGet8ByteData
 *                                                                          
 *    DESCRIPTION      : This function Decodes the eight byte AgentX data 
 *                       from the buffer based on the Flag.  
 *
 *    INPUT            : pu1Buffer  - Pointer to the linear buffer
 *                       pu4OffSet  - Pointer to the Offset
 *                       u1Flags     - Flag bit which specifies 
 *                                     NETWORK_BYTE_ORDER_BIT is set or not
 *                                                                          
 *    OUTPUT           : pu8Data - Pointer to the eight byte variable
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE 
 *    
 ****************************************************************************/
VOID
SnxCodeGet8ByteData (UINT1 *pu1Buffer, UINT4 *pu4OffSet, UINT1 u1Flags,
                     tSNMP_COUNTER64_TYPE * pu8Data)
{
    UINT1              *pu1Pkt = pu1Buffer;

    pu1Pkt = pu1Pkt + (*pu4OffSet);

    /* Counter64 */

    MEMCPY (&pu8Data->msn, pu1Pkt, SNX_4BYTE_DATALEN);

    if (u1Flags & SNX_NETWORK_BYTE_ORDER)
    {
        pu8Data->msn = OSIX_NTOHL (pu8Data->msn);
    }

    (*pu4OffSet) += SNX_4BYTE_DATALEN;

    MEMCPY (&pu8Data->lsn, (pu1Pkt + (*pu4OffSet)), SNX_4BYTE_DATALEN);

    if (u1Flags & SNX_NETWORK_BYTE_ORDER)
    {
        pu8Data->lsn = OSIX_NTOHL (pu8Data->lsn);
    }

    (*pu4OffSet) += SNX_4BYTE_DATALEN;

}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxCodePut8ByteData
 *                                                                          
 *    DESCRIPTION      : This function encodes the two byte AgentX data 
 *                       from the buffer based on the Flag.  
 *
 *    INPUT            : pu8Data - Pointer to the eight byte variable
 *                       pu4OffSet  - Pointer to the Offset
 *                       u1Flags    - Flag bit which specifies 
 *                                    NETWORK_BYTE_ORDER_BIT is set or not
 *                                                                          
 *    OUTPUT           : pu1Buffer  - Pointer to the linear buffer
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE 
 *    
 ****************************************************************************/
VOID
SnxCodePut8ByteData (tSNMP_COUNTER64_TYPE u8Data, UINT4 *pu4OffSet,
                     UINT1 u1Flags, UINT1 *pu1Buffer)
{
    /* Counter64 */

    if (u1Flags & SNX_NETWORK_BYTE_ORDER)
    {
        u8Data.msn = OSIX_HTONL (u8Data.msn);
    }

    MEMCPY ((pu1Buffer + (*pu4OffSet)), &u8Data.msn, SNX_4BYTE_DATALEN);

    (*pu4OffSet) += SNX_4BYTE_DATALEN;

    if (u1Flags & SNX_NETWORK_BYTE_ORDER)
    {
        u8Data.lsn = OSIX_HTONL (u8Data.lsn);
    }

    MEMCPY ((pu1Buffer + (*pu4OffSet)), &u8Data.lsn, SNX_4BYTE_DATALEN);

    (*pu4OffSet) += SNX_4BYTE_DATALEN;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxCodeGet2ByteData
 *                                                                          
 *    DESCRIPTION      : This function Decodes the two byte AgentX data 
 *                       from the buffer based on the Flag.  
 *
 *    INPUT            : pu1Buffer  - Pointer to the linear buffer
 *                       pu4OffSet  - Pointer to the Offset
 *                       u1Flags     - Flag bit which specifies 
 *                                     NETWORK_BYTE_ORDER_BIT is set or not
 *                                                                          
 *    OUTPUT           : pu2Data - Pointer to the two byte variable
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE 
 *    
 ****************************************************************************/
VOID
SnxCodeGet2ByteData (UINT1 *pu1Buffer, UINT4 *pu4OffSet, UINT1 u1Flags,
                     UINT2 *pu2Data)
{
    UINT1              *pu1Pkt = pu1Buffer;

    pu1Pkt = pu1Pkt + (*pu4OffSet);

    MEMCPY (pu2Data, pu1Pkt, SNX_2BYTE_DATALEN);

    if (u1Flags & SNX_NETWORK_BYTE_ORDER)
    {
        *pu2Data = OSIX_NTOHS (*pu2Data);
    }

    (*pu4OffSet) += SNX_2BYTE_DATALEN;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxCodePut2ByteData
 *                                                                          
 *    DESCRIPTION      : This function encodes the two byte AgentX data 
 *                       based on the Flag.  
 *
 *    INPUT            : pu2Data    - Pointer to the two byte variable
 *                       pu4OffSet  - Pointer to the Offset
 *                       u1Flags    - Flag bit which specifies 
 *                                     NETWORK_BYTE_ORDER_BIT is set or not
 *                                                                          
 *    OUTPUT           : pu1Buffer  - Pointer to the linear buffer
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE 
 *    
 ****************************************************************************/
VOID
SnxCodePut2ByteData (UINT2 u2Data, UINT4 *pu4OffSet, UINT1 u1Flags,
                     UINT1 *pu1Buffer)
{
    if (u1Flags & SNX_NETWORK_BYTE_ORDER)
    {
        u2Data = OSIX_HTONS (u2Data);
    }

    MEMCPY ((pu1Buffer + (*pu4OffSet)), &u2Data, SNX_2BYTE_DATALEN);

    (*pu4OffSet) += SNX_2BYTE_DATALEN;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxCodeGetOctetString
 *                                                                          
 *    DESCRIPTION      : This function gets OctetString from the buffer.  
 *
 *    INPUT            : pu1Buffer  - Pointer to the linear buffer
 *                       pu4OffSet  - Pointer to the Offset
 *                       u1Flag      - Flag bit which specifies 
 *                                      NETWORK_BYTE_ORDER_BIT is set or not
 *                                                                          
 *    OUTPUT           : pSnmpOctetString - Pointer to the OctetString
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE 
 *    
 ****************************************************************************/
INT4
SnxCodeGetOctetString (UINT1 *pu1Buf, UINT4 *pu4OffSet, UINT1 u1Flags,
                       tSNMP_OCTET_STRING_TYPE * pSnmpOctetString)
{
    INT4                i4Len = 0;

    /* Get the Octet String Length */
    SnxCodeGet4ByteData (pu1Buf, pu4OffSet, u1Flags, (UINT4 *) &i4Len);

    if (i4Len > SNMP_MAX_OCTETSTRING_SIZE)
    {
        SNMPTrace ("Length of the Octet String exceeds max value\n");
        return SNMP_FAILURE;
    }

    MEMCPY (pSnmpOctetString->pu1_OctetList, (pu1Buf + (*pu4OffSet)), i4Len);
    pSnmpOctetString->i4_Length = i4Len;
    /* For 4 Byte alignment */
    if (i4Len % SNX_4BYTE_DATALEN != 0)
    {
        i4Len += (SNX_4BYTE_DATALEN - (pSnmpOctetString->i4_Length
                                       % SNX_4BYTE_DATALEN));
    }
    *pu4OffSet += (UINT4) i4Len;

    return SNMP_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxCodePutOctetString
 *                                                                          
 *    DESCRIPTION      : This function gets OctetString from the buffer.  
 *
 *    INPUT            : pu1Buffer  - Pointer to the linear buffer
 *                       pu4OffSet  - Pointer to the Offset
 *                       u1Flag      - Flag bit which specifies 
 *                                      NETWORK_BYTE_ORDER_BIT is set or not
 *                                                                          
 *    OUTPUT           : pSnmpOctetString - Pointer to the OctetString
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE 
 *    
 ****************************************************************************/
INT4
SnxCodePutOctetString (tSNMP_OCTET_STRING_TYPE * pSnmpOctetString,
                       UINT4 *pu4OffSet, UINT1 u1Flags, UINT1 *pu1Buf)
{
    UINT4               u4Len = 0;
    if (pSnmpOctetString->i4_Length == 0)
    {
        SNMPTrace ("Failed to fill octet string\n");
        (*pu4OffSet) += SNX_4BYTE_DATALEN;
        return SNMP_SUCCESS;
    }
    /* For 4 Byte alignment */
    if (pSnmpOctetString->i4_Length % SNX_4BYTE_DATALEN != 0)
    {
        u4Len += (SNX_4BYTE_DATALEN - ((UINT4) pSnmpOctetString->i4_Length
                                       % SNX_4BYTE_DATALEN));
    }
    SnxCodePut4ByteData ((UINT4) pSnmpOctetString->i4_Length, pu4OffSet,
                         u1Flags, pu1Buf);

    MEMCPY (pu1Buf + (*pu4OffSet),
            pSnmpOctetString->pu1_OctetList, pSnmpOctetString->i4_Length);

    (*pu4OffSet) += (u4Len + (UINT4) pSnmpOctetString->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxCodeDecodeOid
 *                                                                          
 *    DESCRIPTION      : This function decodes the OID from the buffer.  
 *
 *    INPUT            : pu1Buf      - Pointer to the linear buffer
 *                       pu4OffSet  - Pointer to the Offset
 *                       u1Flag       - Flag bit which specifies 
 *                                      NETWORK_BYTE_ORDER_BIT is set or not
 *                                                                          
 *    OUTPUT           : pSnxOidType - Pointer to the OID
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 *    
 ****************************************************************************/

INT4
SnxCodeDecodeOid (UINT1 *pu1Buf, UINT4 *pu4OffSet, UINT1 u1Flag,
                  tSnmpAgentxOidType * pSnxOidType)
{
    UINT1               u1SubIdCnt = 0;
    UINT1               u1Prefix = 0;
    UINT2               u2TempNoSubId = 0;

    /* Number of sub-identifiers */

    SNX_GET_1BYTE (pu1Buf, *pu4OffSet, pSnxOidType->u1NoSubId);

    /* Prefix */

    SNX_GET_1BYTE (pu1Buf, *pu4OffSet, pSnxOidType->u1Prefix);
    u1Prefix = pSnxOidType->u1Prefix;

    if (u1Prefix != 0)
    {
        u2TempNoSubId = (UINT2) pSnxOidType->u1NoSubId;
        if (u2TempNoSubId > (SNMP_MAX_OID_LENGTH - SNX_INET_PREFIX_LEN))
        {
            SNMPTrace ("SNMP Oid Len greater than Max OID Len\n");
            return SNMP_FAILURE;
        }
    }

    /* Include */

    SNX_GET_1BYTE (pu1Buf, *pu4OffSet, *(UINT1 *) &(pSnxOidType->b1Include));
    SNX_GET_1BYTE (pu1Buf, *pu4OffSet, pSnxOidType->u1Reserved);

    /* Sub-identifiers */

    for (u1SubIdCnt = 0; u1SubIdCnt < pSnxOidType->u1NoSubId; u1SubIdCnt++)
    {
        SnxCodeGet4ByteData (pu1Buf, pu4OffSet, u1Flag,
                             &(pSnxOidType->pu4OidLst[u1SubIdCnt]));
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxCodeEncodeOid
 *                                                                          
 *    DESCRIPTION      : This function encodes the OID from the buffer.  
 *
 *    INPUT            : pu1Buf      - Pointer to the linear buffer
 *                       pu4OffSet  - Pointer to the Offset
 *                       u1Flag       - Flag bit which specifies 
 *                                      NETWORK_BYTE_ORDER_BIT is set or not
 *                                                                          
 *    OUTPUT           : pSnxOidType - Pointer to the OID
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 *    
 ****************************************************************************/

INT4
SnxCodeEncodeOid (tSnmpAgentxOidType * pSnxOidType, UINT4 *pu4OffSet,
                  UINT1 u1Flag, UINT1 *pu1Buf)
{
    UINT1               u1SubIdCnt = 0;

    /* Number of sub-identifiers */

    SNX_PUT_1BYTE (pSnxOidType->u1NoSubId, *pu4OffSet, pu1Buf);

    /* Prefix */

    SNX_PUT_1BYTE (pSnxOidType->u1Prefix, *pu4OffSet, pu1Buf);

    /* Include */

    SNX_PUT_1BYTE ((UINT1) pSnxOidType->b1Include, *pu4OffSet, pu1Buf);
    SNX_PUT_1BYTE (pSnxOidType->u1Reserved, *pu4OffSet, pu1Buf);

    /* Sub-identifiers */

    for (u1SubIdCnt = 0; u1SubIdCnt < pSnxOidType->u1NoSubId; u1SubIdCnt++)
    {
        SnxCodePut4ByteData (pSnxOidType->pu4OidLst[u1SubIdCnt], pu4OffSet,
                             u1Flag, pu1Buf);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxCodeDecodePduSpecificData
 *                                                                          
 *    DESCRIPTION      : This function decodes the AgentX pdu from the buffer
 *                       based on the PDU type.  
 *
 *    INPUT            : pu1Buf      - Pointer to the linear buffer
 *                       u4OffSet    - Offset
 *                       pAgentxPdu  - Pointer to the AgentX PDU
 *                                                                          
 *    OUTPUT           : pAgentxPdu  - Pointer to the AgentX PDU
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE 
 *
 ****************************************************************************/
INT4
SnxCodeDecodePduSpecificData (UINT1 *pu1Buf, UINT4 u4OffSet,
                              tSnmpAgentxPdu * pAgentxPdu)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    switch (pAgentxPdu->u1PduType)
    {
        case SNX_COMMITSET_PDU:
            /* Fall thro' */
        case SNX_UNDOSET_PDU:
            /* Fall thro' */
        case SNX_CLEANUP_PDU:
            /* These PDUs consist of the AgentX Header Only. */
            break;
        case SNX_CLOSE_PDU:
            SNX_GET_1BYTE (pu1Buf, u4OffSet, pAgentxPdu->u1Reason);
            break;
        case SNX_RESPONSE_PDU:
            SnxCodeGet4ByteData (pu1Buf, &u4OffSet, pAgentxPdu->u1Flags,
                                 &(pAgentxPdu->u4SysUpTime));
            SnxCodeGet2ByteData (pu1Buf, &u4OffSet, pAgentxPdu->u1Flags,
                                 &(pAgentxPdu->u2Error));
            SnxCodeGet2ByteData (pu1Buf, &u4OffSet, pAgentxPdu->u1Flags,
                                 &(pAgentxPdu->u2ErrIndex));

            if (SnxCodeDecodeVbList (pu1Buf, u4OffSet, pAgentxPdu)
                != SNMP_SUCCESS)
            {
                i4RetVal = SNMP_FAILURE;
                break;
            }
            break;

        case SNX_TESTSET_PDU:

            if (SnxCodeDecodeContextInfo (pu1Buf, &u4OffSet, pAgentxPdu)
                != SNMP_SUCCESS)
            {
                i4RetVal = SNX_PARSE_ERR;
                break;
            }

            if (SnxCodeDecodeVbList (pu1Buf, u4OffSet, pAgentxPdu)
                != SNMP_SUCCESS)
            {
                i4RetVal = SNX_PARSE_ERR;
                break;
            }

            break;
        case SNX_GETBULK_PDU:

            if (SnxCodeDecodeContextInfo (pu1Buf, &u4OffSet, pAgentxPdu)
                != SNMP_SUCCESS)
            {
                i4RetVal = SNX_PARSE_ERR;
                break;
            }

            SnxCodeGet2ByteData (pu1Buf, &u4OffSet, pAgentxPdu->u1Flags,
                                 &(pAgentxPdu->u2NonRepeaters));
            SnxCodeGet2ByteData (pu1Buf, &u4OffSet, pAgentxPdu->u1Flags,
                                 &(pAgentxPdu->u2MaxRepetitions));

            if (SnxCodeDecodeSearchRngList (pu1Buf, u4OffSet, pAgentxPdu)
                != SNMP_SUCCESS)
            {
                SnxUtlFreeOctetString (&pAgentxPdu->ContextName, OSIX_FALSE);
                pAgentxPdu->pSearchRngLst = NULL;
                i4RetVal = SNX_PARSE_ERR;
                break;
            }

            break;
        case SNX_GET_PDU:
            /* Fall thro' */
        case SNX_GETNEXT_PDU:

            if (SnxCodeDecodeContextInfo (pu1Buf, &u4OffSet, pAgentxPdu)
                != SNMP_SUCCESS)
            {
                i4RetVal = SNX_PARSE_ERR;
                break;
            }

            if (SnxCodeDecodeSearchRngList (pu1Buf, u4OffSet, pAgentxPdu)
                != SNMP_SUCCESS)
            {
                SnxUtlFreeOctetString (&pAgentxPdu->ContextName, OSIX_FALSE);
                pAgentxPdu->pSearchRngLst = NULL;
                i4RetVal = SNX_PARSE_ERR;
                break;
            }

            break;
        default:
            SNMPTrace ("Not a valid AgentX PDU Type.\n");
            i4RetVal = SNMP_FAILURE;
            break;
    }
    return i4RetVal;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxCodeEncodePduSpecificData
 *                                                                          
 *    DESCRIPTION      : This function encodes the AgentX pdu from the buffer
 *                       based on the PDU type.  
 *
 *    INPUT            : pAgentxPdu  - Pointer to the AgentX PDU
 *                       u4OffSet    - Offset
 *                                                                          
 *    OUTPUT           : pu1Buf      - Pointer to the linear buffer
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE 
 *
 ****************************************************************************/
INT4
SnxCodeEncodePduSpecificData (UINT1 *pu1Buf, UINT4 u4OffSet,
                              tSnmpAgentxPdu * pAgentxPdu)
{
    INT4                i4RetVal = SNMP_SUCCESS;
    UINT1               au1Resvd[SNX_RESERVE_3BYTE_DATALEN];

    MEMSET (au1Resvd, 0, SNX_RESERVE_3BYTE_DATALEN);

    switch (pAgentxPdu->u1PduType)
    {
        case SNX_OPEN_PDU:

            SNX_PUT_1BYTE (pAgentxPdu->u1TimeOut, u4OffSet, pu1Buf);
            MEMCPY (&pu1Buf[u4OffSet], au1Resvd, SNX_RESERVE_3BYTE_DATALEN);
            u4OffSet += SNX_RESERVE_3BYTE_DATALEN;

            if (pAgentxPdu->OidVal.u1NoSubId == 0)
            {
                break;
            }

            if (SnxCodeEncodeOid (&(pAgentxPdu->OidVal),
                                  &u4OffSet, pAgentxPdu->u1Flags,
                                  pu1Buf) != SNMP_SUCCESS)
            {
                i4RetVal = SNMP_FAILURE;
                break;
            }

            if (SnxCodePutOctetString (&(pAgentxPdu->OidDesc), &u4OffSet,
                                       pAgentxPdu->u1Flags,
                                       pu1Buf) != SNMP_SUCCESS)
            {
                i4RetVal = SNMP_FAILURE;
                break;
            }

            gAgtxStats.u4OpenPduSent++;
            break;

        case SNX_CLOSE_PDU:

            SNX_PUT_1BYTE (pAgentxPdu->u1Reason, u4OffSet, pu1Buf);
            MEMCPY (&pu1Buf[u4OffSet], au1Resvd, SNX_RESERVE_3BYTE_DATALEN);
            u4OffSet += SNX_RESERVE_3BYTE_DATALEN;

            gAgtxStats.u4ClosePduSent++;
            break;

        case SNX_REGISTER_PDU:

            if (SnxCodeEncodeContextInfo (pu1Buf, &u4OffSet, pAgentxPdu)
                != SNMP_SUCCESS)
            {
                i4RetVal = SNMP_FAILURE;
                break;
            }

            SNX_PUT_1BYTE (pAgentxPdu->u1TimeOut, u4OffSet, pu1Buf);
            SNX_PUT_1BYTE (pAgentxPdu->u1Priority, u4OffSet, pu1Buf);
            SNX_PUT_1BYTE (pAgentxPdu->u1RangeSubId, u4OffSet, pu1Buf);

            pu1Buf[u4OffSet++] = 0;

            if ((SnxCodeEncodeOid (&(pAgentxPdu->OidVal), &u4OffSet,
                                   pAgentxPdu->u1Flags,
                                   pu1Buf)) != SNMP_SUCCESS)
            {
                i4RetVal = SNMP_FAILURE;
                break;
            }

            SnxCodePut4ByteData (pAgentxPdu->u4UpperBound, &u4OffSet,
                                 pAgentxPdu->u1Flags, pu1Buf);

            gAgtxStats.u4RegPduSent++;
            break;

        case SNX_UNREGISTER_PDU:

            if (SnxCodeEncodeContextInfo (pu1Buf, &u4OffSet, pAgentxPdu)
                != SNMP_SUCCESS)
            {
                i4RetVal = SNMP_FAILURE;
                break;
            }

            pu1Buf[u4OffSet++] = 0;
            SNX_PUT_1BYTE (pAgentxPdu->u1Priority, u4OffSet, pu1Buf);
            SNX_PUT_1BYTE (pAgentxPdu->u1RangeSubId, u4OffSet, pu1Buf);
            pu1Buf[u4OffSet++] = 0;

            if ((SnxCodeEncodeOid (&(pAgentxPdu->OidVal), &u4OffSet,
                                   pAgentxPdu->u1Flags,
                                   pu1Buf)) != SNMP_SUCCESS)
            {
                i4RetVal = SNMP_FAILURE;
                break;
            }

            SnxCodePut4ByteData (pAgentxPdu->u4UpperBound, &u4OffSet,
                                 pAgentxPdu->u1Flags, pu1Buf);

            gAgtxStats.u4UnRegPduSent++;
            break;

        case SNX_INDEX_ALLOC_PDU:
            /* Fall thro' */
        case SNX_INDEX_DEALLOC_PDU:
            /* Fall thro' */
        case SNX_NOTIFY_PDU:
            if (SnxCodeEncodeContextInfo (pu1Buf, &u4OffSet, pAgentxPdu)
                != SNMP_SUCCESS)
            {
                i4RetVal = SNMP_FAILURE;
                break;
            }

            if (SnxCodeEncodeVbList (pAgentxPdu, u4OffSet, pu1Buf)
                != SNMP_SUCCESS)
            {
                i4RetVal = SNMP_FAILURE;
                break;
            }

            if (pAgentxPdu->u1PduType == SNX_INDEX_ALLOC_PDU)
            {
                gAgtxStats.u4IndxAllocPduSent++;
            }

            if (pAgentxPdu->u1PduType == SNX_INDEX_DEALLOC_PDU)
            {
                gAgtxStats.u4IndxDAllocPduSent++;
            }

            if (pAgentxPdu->u1PduType == SNX_NOTIFY_PDU)
            {
                gAgtxStats.u4NotifyPduSent++;
            }

            break;

        case SNX_ADD_AGENT_CAPS_PDU:

            if (SnxCodeEncodeContextInfo (pu1Buf, &u4OffSet, pAgentxPdu)
                != SNMP_SUCCESS)
            {
                i4RetVal = SNMP_FAILURE;
                break;
            }

            if (SnxCodeEncodeOid (&(pAgentxPdu->OidVal),
                                  &u4OffSet, pAgentxPdu->u1Flags,
                                  pu1Buf) != SNMP_SUCCESS)
            {
                i4RetVal = SNMP_FAILURE;
                break;
            }

            if (SnxCodePutOctetString (&(pAgentxPdu->OidDesc), &u4OffSet,
                                       pAgentxPdu->u1Flags,
                                       pu1Buf) != SNMP_SUCCESS)
            {
                i4RetVal = SNMP_FAILURE;
                break;
            }

            gAgtxStats.u4AddCapsPduSent++;
            break;

        case SNX_REMOVE_AGENT_CAPS_PDU:

            if (SnxCodeEncodeContextInfo (pu1Buf, &u4OffSet, pAgentxPdu)
                != SNMP_SUCCESS)
            {
                i4RetVal = SNMP_FAILURE;
                break;
            }

            if (SnxCodeEncodeOid (&(pAgentxPdu->OidVal),
                                  &u4OffSet, pAgentxPdu->u1Flags,
                                  pu1Buf) != SNMP_SUCCESS)
            {
                i4RetVal = SNMP_FAILURE;
                break;
            }

            gAgtxStats.u4RemoveCapsPduSent++;
            break;

        case SNX_PING_PDU:

            if (SnxCodeEncodeContextInfo (pu1Buf, &u4OffSet, pAgentxPdu)
                != SNMP_SUCCESS)
            {
                i4RetVal = SNMP_FAILURE;
                break;
            }

            gAgtxStats.u4PingPduSent++;
            break;

        case SNX_RESPONSE_PDU:
            SnxCodePut4ByteData (pAgentxPdu->u4SysUpTime, &u4OffSet,
                                 pAgentxPdu->u1Flags, pu1Buf);
            SnxCodePut2ByteData (pAgentxPdu->u2Error, &u4OffSet,
                                 pAgentxPdu->u1Flags, pu1Buf);
            SnxCodePut2ByteData (pAgentxPdu->u2ErrIndex, &u4OffSet,
                                 pAgentxPdu->u1Flags, pu1Buf);

            if (SnxCodeEncodeVbList (pAgentxPdu, u4OffSet, pu1Buf)
                != SNMP_SUCCESS)
            {
                i4RetVal = SNMP_FAILURE;
                break;
            }

            gAgtxStats.u4RespPduSent++;
            break;

        default:
            SNMPTrace ("Can`t encode.Not a valid AgentX PDU Type.\n");
            i4RetVal = SNMP_FAILURE;

    }

    return i4RetVal;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxCodeDecodeContextInfo
 *                                                                          
 *    DESCRIPTION      : This function decodes the context information      .  
 *
 *    INPUT            : pu1Buffer   - Pointer to the linear buffer
 *                       pu4OffSet   - Pointer to the offset
 *                       pAgentxPdu  - Pointer to the AgentX PDU
 *                                                                          
 *    OUTPUT           : pAgentxPdu  - Pointer to the AgentX PDU
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE 
 *
 ****************************************************************************/
INT4
SnxCodeDecodeContextInfo (UINT1 *pu1Buffer, UINT4 *pu4OffSet,
                          tSnmpAgentxPdu * pAgentxPdu)
{
    UINT4               u4VcNum = 0;

    /* Context Information */

    if (pAgentxPdu->u1Flags & SNX_NON_DEFAULT_CTXT)
    {
        if (SnxUtlAllocOctetString (&(pAgentxPdu->ContextName)) == NULL)
        {
            return SNMP_FAILURE;
        }
        if (SnxCodeGetOctetString (pu1Buffer, pu4OffSet, pAgentxPdu->u1Flags,
                                   &(pAgentxPdu->ContextName)) != SNMP_SUCCESS)
        {
            SNMPTrace ("Unable to parse context information\n");
            SnxUtlFreeOctetString (&(pAgentxPdu->ContextName), OSIX_FALSE);
            return SNMP_FAILURE;
        }

        if (VACMGetContextNum (&(pAgentxPdu->ContextName), &u4VcNum)
            != SNMP_FAILURE)
        {
            SNMPTrace ("Not a valid context name\n");
            SnxUtlFreeOctetString (&(pAgentxPdu->ContextName), OSIX_FALSE);
            return SNMP_FAILURE;
        }

    }

    else
    {
        /*Default ctxt name */
        pAgentxPdu->ContextName.i4_Length = 0;
        pAgentxPdu->ContextName.pu1_OctetList = NULL;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxCodeEncodeContextInfo
 *                                                                          
 *    DESCRIPTION      : This function decodes the context information      .  
 *
 *    INPUT            : pu1Buffer   - Pointer to the linear buffer
 *                       pu4OffSet   - Pointer to the offset
 *                       pAgentxPdu  - Pointer to the AgentX PDU
 *                                                                          
 *    OUTPUT           : pAgentxPdu  - Pointer to the AgentX PDU
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE 
 *
 ****************************************************************************/
INT4
SnxCodeEncodeContextInfo (UINT1 *pu1Buffer, UINT4 *pu4OffSet,
                          tSnmpAgentxPdu * pAgentxPdu)
{
    /* Context Information */

    if (pAgentxPdu->u1Flags & SNX_NON_DEFAULT_CTXT)
    {

        if (pAgentxPdu->ContextName.pu1_OctetList == NULL)
        {
            SNMPTrace ("Context Information not present\n");
            return SNMP_FAILURE;
        }

        if (SnxCodePutOctetString (&(pAgentxPdu->ContextName), pu4OffSet,
                                   pAgentxPdu->u1Flags,
                                   pu1Buffer) != SNMP_SUCCESS)
        {
            SNMPTrace ("Unable to fill the context information\n");
            return SNMP_FAILURE;
        }

    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxCodeDecodeSearchRngList
 *                                                                          
 *    DESCRIPTION      : This function decodes the search range list      .  
 *
 *    INPUT            : pu1Buffer   - Pointer to the linear buffer
 *                       u4OffSet   - Offset
 *                       pAgentxPdu  - Pointer to the AgentX PDU
 *                                                                          
 *    OUTPUT           : pAgentxPdu  - Pointer to the AgentX PDU
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 *    
 ****************************************************************************/

INT4
SnxCodeDecodeSearchRngList (UINT1 *pu1Buffer, UINT4 u4OffSet,
                            tSnmpAgentxPdu * pAgentxPdu)
{
    tSnmpSearchRng     *pSearchRangeLst = NULL;
    tSnmpSearchRng     *pPrevSrchRngLst = NULL;

    while (u4OffSet < (pAgentxPdu->u4PayLoadLen + SNX_AGENTX_PDU_HDR_LEN))
    {
        pSearchRangeLst = SnxUtlAllocSrchRng (pSearchRangeLst);
        if (pSearchRangeLst == NULL)
        {
            SNMPTrace ("Failed to allocate memory for search range\n");
            SnxUtlFreeSrchRngLst (pAgentxPdu->pSearchRngLst);
            return SNMP_FAILURE;
        }

        if (pAgentxPdu->pSearchRngLst == NULL)
        {
            pAgentxPdu->pSearchRngLst = pSearchRangeLst;
        }

        if (pPrevSrchRngLst != NULL)
        {
            pPrevSrchRngLst->pNextSearchRng = pSearchRangeLst;
        }

        /* Start OID */

        if (SnxCodeDecodeOid (pu1Buffer, &u4OffSet, pAgentxPdu->u1Flags,
                              &(pSearchRangeLst->StartId)) != SNMP_SUCCESS)
        {
            SnxUtlFreeSrchRngLst (pAgentxPdu->pSearchRngLst);
            return SNMP_FAILURE;
        }

        /* End OID */

        if (SnxCodeDecodeOid (pu1Buffer, &u4OffSet, pAgentxPdu->u1Flags,
                              &(pSearchRangeLst->EndId)) != SNMP_SUCCESS)
        {
            SnxUtlFreeSrchRngLst (pAgentxPdu->pSearchRngLst);
            return SNMP_FAILURE;
        }
        TempEndId = pSearchRangeLst->EndId;

        /* End OID must be NULL for Get-Reguest  and Start OID`s include
         * field should be set */
        if ((pAgentxPdu->u1PduType == SNX_GET_PDU) &&
            (pSearchRangeLst->EndId.u1NoSubId != 0) &&
            (pSearchRangeLst->StartId.b1Include != 1))
        {
            SNMPTrace ("Ending OID is not null/Include field might be zero");
            SNMPTrace (" for GetRequest\n");
            SnxUtlFreeSrchRngLst (pAgentxPdu->pSearchRngLst);
            return SNMP_FAILURE;
        }

        /* Check to see if the END OID include field is always zero. */
        if (pSearchRangeLst->EndId.b1Include != 0)
        {
            SNMPTrace
                ("Include field of ending OID`s in SearchRange is non-zero\n");
            SnxUtlFreeSrchRngLst (pAgentxPdu->pSearchRngLst);
            return SNMP_FAILURE;
        }

        pPrevSrchRngLst = pSearchRangeLst;
        pSearchRangeLst = NULL;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxCodeEncodeVbList
 *                                                                          
 *    DESCRIPTION      : This function encodes the variable bindings list      .  
 *
 *    INPUT            : u4OffSet   - Offset
 *                       pAgentxPdu  - Pointer to the AgentX PDU
 *                                                                          
 *    OUTPUT           : pu1Buffer   - Pointer to the linear buffer
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE 
 *    
 ****************************************************************************/
INT4
SnxCodeEncodeVbList (tSnmpAgentxPdu * pAgentxPdu, UINT4 u4OffSet,
                     UINT1 *pu1Buffer)
{
    tSnmpAgentxVarBind *pVbList = pAgentxPdu->pAgxVarBindList;

    while (pVbList != NULL)
    {
        /* Type */

        SnxCodePut2ByteData (pVbList->u2Type, &u4OffSet,
                             pAgentxPdu->u1Flags, pu1Buffer);
        SnxCodePut2ByteData (pVbList->u2Reserved, &u4OffSet,
                             pAgentxPdu->u1Flags, pu1Buffer);
        /* ObjName */

        if (SnxCodeEncodeOid (&(pVbList->ObjName),
                              &u4OffSet, pAgentxPdu->u1Flags,
                              pu1Buffer) != SNMP_SUCCESS)
        {
            SNMPTrace ("Failed to fill Object name in Variable Binding List\n");
            return SNMP_FAILURE;
        }

        /* ObjValue */

        if (SnxCodeEncodeObjValue (pVbList, &u4OffSet,
                                   pAgentxPdu->u1Flags,
                                   pu1Buffer) != SNMP_SUCCESS)
        {
            SNMPTrace
                ("Failed to fill Object value in Variable Binding List\n");
            return SNMP_FAILURE;
        }

        pVbList = pVbList->pNextVarBind;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxCodeDecodeVbList
 *                                                                          
 *    DESCRIPTION      : This function decodes the variable bindings list      .  
 *
 *    INPUT            : pu1Buffer   - Pointer to the linear buffer
 *                       u4OffSet   - Offset
 *                       pAgentxPdu  - Pointer to the AgentX PDU
 *                                                                          
 *    OUTPUT           : pAgentxPdu  - Pointer to the AgentX PDU
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE 
 *    
 ****************************************************************************/
INT4
SnxCodeDecodeVbList (UINT1 *pu1Buffer, UINT4 u4OffSet,
                     tSnmpAgentxPdu * pAgentxPdu)
{
    tSnmpAgentxVarBind *pVbList = NULL;
    tSnmpAgentxVarBind *pPrevVbList = NULL;

    while (u4OffSet < (pAgentxPdu->u4PayLoadLen + SNX_AGENTX_PDU_HDR_LEN))
    {
        pVbList = SnxUtlAllocVarbind (pVbList);
        if (pVbList == NULL)
        {
            SNMPTrace ("Failed to allocate memory for variable binding\n");
            SnxUtlFreeVbList (pAgentxPdu->pAgxVarBindList);
            return SNMP_FAILURE;
        }

        if (pAgentxPdu->pAgxVarBindList == NULL)
        {
            pAgentxPdu->pAgxVarBindList = pVbList;
        }

        if (pPrevVbList != NULL)
        {
            pPrevVbList->pNextVarBind = pVbList;
        }

        /* Type */

        SnxCodeGet2ByteData (pu1Buffer, &u4OffSet, pAgentxPdu->u1Flags,
                             &(pVbList->u2Type));
        SnxCodeGet2ByteData (pu1Buffer, &u4OffSet, pAgentxPdu->u1Flags,
                             &(pVbList->u2Reserved));
        /* ObjName */

        if (SnxCodeDecodeOid (pu1Buffer, &u4OffSet, pAgentxPdu->u1Flags,
                              &(pVbList->ObjName)) != SNMP_SUCCESS)
        {
            SnxUtlFreeVbList (pAgentxPdu->pAgxVarBindList);
            return SNMP_FAILURE;
        }

        /* ObjValue */

        if (SnxCodeDecodeObjValue (pu1Buffer, &u4OffSet, pAgentxPdu->u1Flags,
                                   pVbList) != SNMP_SUCCESS)
        {
            SNMPTrace ("Unable to parse Object value information\n");
            SnxUtlFreeVbList (pAgentxPdu->pAgxVarBindList);
            return SNMP_FAILURE;
        }

        pPrevVbList = pVbList;
        pVbList = NULL;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxCodeDecodeObjValue
 *                                                                          
 *    DESCRIPTION      : This function decodes the AgentX object Value.  
 *
 *    INPUT            : pu1Buffer   - Pointer to the linear buffer
 *                       pu4OffSet   - Pointer to the Offset
 *                       u1Flags     - Flag bit which specifies 
 *                                     NETWORK_BYTE_ORDER_BIT is set or not
 *                                                                          
 *    OUTPUT           : pVbList - Pointer to the Variable Binding List
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE 
 *    
 ****************************************************************************/
INT4
SnxCodeDecodeObjValue (UINT1 *pu1Buffer, UINT4 *pu4OffSet, UINT1 u1Flags,
                       tSnmpAgentxVarBind * pVbList)
{
    INT4                i4RetVal = SNMP_SUCCESS;
    tSnmpAgentxOidType  SnxOidType;

    switch (pVbList->u2Type)
    {
        case SNMP_DATA_TYPE_COUNTER32:
            /* Fall thro' */
        case SNMP_DATA_TYPE_GAUGE32:
            /* Fall thro' */
        case SNMP_DATA_TYPE_TIME_TICKS:
            SnxCodeGet4ByteData (pu1Buffer, pu4OffSet, u1Flags,
                                 &(pVbList->ObjValue.u4_ULongValue));
            break;
        case SNMP_DATA_TYPE_COUNTER64:
            SnxCodeGet8ByteData (pu1Buffer, pu4OffSet, u1Flags,
                                 &(pVbList->ObjValue.u8_Counter64Value));
            break;
        case SNMP_DATA_TYPE_INTEGER32:
            SnxCodeGet4ByteData (pu1Buffer, pu4OffSet, u1Flags,
                                 (UINT4 *) &(pVbList->ObjValue.i4_SLongValue));
            break;
        case SNMP_DATA_TYPE_OBJECT_ID:
            MEMSET (&SnxOidType, 0, sizeof (tSnmpAgentxOidType));
            SnxUtlAllocAgtxOid (&SnxOidType);

            if (SnxCodeDecodeOid (pu1Buffer, pu4OffSet, u1Flags,
                                  &SnxOidType) != SNMP_SUCCESS)
            {
                i4RetVal = SNMP_FAILURE;
                SnxUtlFreeAgtxOid (&SnxOidType, OSIX_FALSE);
                break;
            }

            if (SnxCodeConvertAgtxOidToAgentOid
                (&SnxOidType, pVbList->ObjValue.pOidValue) != SNMP_SUCCESS)
            {
                i4RetVal = SNMP_FAILURE;
                SnxUtlFreeAgtxOid (&SnxOidType, OSIX_FALSE);
                break;
            }

            SnxUtlFreeAgtxOid (&SnxOidType, OSIX_FALSE);
            break;
        case SNMP_DATA_TYPE_OCTET_PRIM:
            /* Fall thro' */
        case SNMP_DATA_TYPE_OPAQUE:

            if (SnxCodeGetOctetString
                (pu1Buffer, pu4OffSet, u1Flags,
                 pVbList->ObjValue.pOctetStrValue) != SNMP_SUCCESS)
            {
                i4RetVal = SNMP_FAILURE;
                break;
            }

            break;
        case SNMP_DATA_TYPE_IP_ADDR_PRIM:

            if (SnxCodeGetOctetString
                (pu1Buffer, pu4OffSet, u1Flags,
                 pVbList->ObjValue.pOctetStrValue) != SNMP_SUCCESS)
            {
                i4RetVal = SNMP_FAILURE;
                break;
            }

            MEMCPY (&(pVbList->ObjValue.u4_ULongValue),
                    pVbList->ObjValue.pOctetStrValue->pu1_OctetList, SNMP_FOUR);
            pVbList->ObjValue.u4_ULongValue =
                OSIX_NTOHL (pVbList->ObjValue.u4_ULongValue);
            break;
        case SNMP_DATA_TYPE_NULL:
            /* Null type - Do nothing */
            break;
        default:
            SNMPTrace ("Not a valid AgentX object type.\n");
            i4RetVal = SNMP_FAILURE;
    }

    return i4RetVal;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxCodeEncodeObjValue
 *                                                                          
 *    DESCRIPTION      : This function encodes the AgentX object Value.  
 *
 *    INPUT            : pVbList - Pointer to the Variable Binding List
 *                       pu4OffSet   - Pointer to the Offset
 *                       u1Flags     - Flag bit which specifies 
 *                                     NETWORK_BYTE_ORDER_BIT is set or not
 *                                                                          
 *    OUTPUT           : pu1Buffer   - Pointer to the linear buffer
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE 
 *    
 ****************************************************************************/
INT4
SnxCodeEncodeObjValue (tSnmpAgentxVarBind * pVbList, UINT4 *pu4OffSet,
                       UINT1 u1Flags, UINT1 *pu1Buffer)
{
    tSnmpAgentxOidType  SnxOidType;

    MEMSET (&SnxOidType, 0, sizeof (SnxOidType));

    switch (pVbList->u2Type)
    {
        case SNMP_DATA_TYPE_COUNTER32:
            /* Fall thro' */
        case SNMP_DATA_TYPE_GAUGE32:
            /* Fall thro' */
        case SNMP_DATA_TYPE_TIME_TICKS:
            SnxCodePut4ByteData (pVbList->ObjValue.u4_ULongValue, pu4OffSet,
                                 u1Flags, pu1Buffer);
            break;
        case SNMP_DATA_TYPE_COUNTER64:
            SnxCodePut8ByteData (pVbList->ObjValue.u8_Counter64Value,
                                 pu4OffSet, u1Flags, pu1Buffer);
            break;
        case SNMP_DATA_TYPE_INTEGER32:
            SnxCodePut4ByteData ((UINT4) pVbList->ObjValue.i4_SLongValue,
                                 pu4OffSet, u1Flags, pu1Buffer);
            break;
        case SNMP_DATA_TYPE_OBJECT_ID:
            SnxUtlAllocAgtxOid (&SnxOidType);
            if (pVbList->ObjValue.pOidValue == NULL)
            {
                SNMPTrace ("Failed to fil OID value\n");
                SnxUtlFreeAgtxOid (&SnxOidType, OSIX_FALSE);
                return SNMP_FAILURE;
            }

            if (SnxCodeConvertAgentOidToAgtxOid
                (pVbList->ObjValue.pOidValue, &SnxOidType,
                 OSIX_FALSE) != SNMP_SUCCESS)
            {
                SnxUtlFreeAgtxOid (&SnxOidType, OSIX_FALSE);
                return SNMP_FAILURE;
            }

            if (SnxCodeEncodeOid (&SnxOidType, pu4OffSet, u1Flags,
                                  pu1Buffer) != SNMP_SUCCESS)
            {
                SnxUtlFreeAgtxOid (&SnxOidType, OSIX_FALSE);
                return SNMP_FAILURE;
            }

            SnxUtlFreeAgtxOid (&SnxOidType, OSIX_FALSE);
            break;
        case SNMP_DATA_TYPE_OCTET_PRIM:
            /* Fall thro' */
        case SNMP_DATA_TYPE_OPAQUE:

            if (pVbList->ObjValue.pOctetStrValue == NULL)
            {
                SNMPTrace ("Failed to fill octet string value\n");
                return SNMP_FAILURE;
            }

            if (SnxCodePutOctetString
                (pVbList->ObjValue.pOctetStrValue, pu4OffSet, u1Flags,
                 pu1Buffer) != SNMP_SUCCESS)
            {
                return SNMP_FAILURE;
            }

            break;
        case SNMP_DATA_TYPE_IP_ADDR_PRIM:

            if (pVbList->ObjValue.pOctetStrValue == NULL)
            {
                pVbList->ObjValue.pOctetStrValue =
                    SnxUtlAllocOctetString (pVbList->ObjValue.pOctetStrValue);
                if (pVbList->ObjValue.pOctetStrValue == NULL)
                {
                    return SNMP_FAILURE;
                }
            }

            pVbList->ObjValue.u4_ULongValue =
                OSIX_HTONL (pVbList->ObjValue.u4_ULongValue);
            MEMCPY (pVbList->ObjValue.pOctetStrValue->pu1_OctetList,
                    &(pVbList->ObjValue.u4_ULongValue), SNX_4BYTE_DATALEN);
            pVbList->ObjValue.pOctetStrValue->i4_Length = SNX_4BYTE_DATALEN;

            if (SnxCodePutOctetString
                (pVbList->ObjValue.pOctetStrValue, pu4OffSet, u1Flags,
                 pu1Buffer) != SNMP_SUCCESS)
            {
                SnxUtlFreeOctetString (pVbList->ObjValue.pOctetStrValue,
                                       OSIX_TRUE);
                pVbList->ObjValue.pOctetStrValue = NULL;
                return SNMP_FAILURE;
            }

            break;
        case SNMP_DATA_TYPE_NULL:
            /* Fall thro` */
        case SNMP_EXCEPTION_NO_SUCH_OBJECT:
            /* Fall thro` */
        case SNMP_EXCEPTION_NO_SUCH_INSTANCE:
            /* Fall thro` */
        case SNMP_EXCEPTION_END_OF_MIB_VIEW:
            /* Null type - Do nothing */
            break;
        default:
            break;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxCodeConvertAgtxOidToAgentOid
 *                                                                          
 *    DESCRIPTION      : This function converts Agentx OID format
 *                       to SNMP Agent OID format
 *
 *    INPUT            : pSnxOidType   - Pointer to the Agentx OID
 *                                                                          
 *    OUTPUT           : pSnmpOid - Pointer to the SNMP OID
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE 
 *    
 ****************************************************************************/
INT4
SnxCodeConvertAgtxOidToAgentOid (tSnmpAgentxOidType * pSnxOidType,
                                 tSNMP_OID_TYPE * pSnmpOid)
{
    pSnmpOid->u4_Length = 0;
    if (pSnxOidType->u1Prefix != 0)
    {
        /* Check to see if internet prefix is present */
        pSnmpOid->u4_Length = SNX_INET_PREFIX_LEN;
        /* Internet prefix will be prepended to 
         * SNMP OID. */
        SNX_CPY_INET_PREFIX (pSnmpOid->pu4_OidList, pSnxOidType->u1Prefix);
    }

    MEMCPY (pSnmpOid->pu4_OidList + pSnmpOid->u4_Length, pSnxOidType->pu4OidLst,
            ((pSnxOidType->u1NoSubId) * SNX_4BYTE_DATALEN));
    pSnmpOid->u4_Length += pSnxOidType->u1NoSubId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxCodeConvertAgentOidToAgtxOid
 *                                                                          
 *    DESCRIPTION      : This function converts Agentx OID format
 *                       to SNMP Agent OID format
 *
 *    INPUT            : pSnmpOid - Pointer to the SNMP OID
 *                                                                          
 *    OUTPUT           : pSnxOidType   - Pointer to the Agentx OID
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE 
 *    
 ****************************************************************************/
INT4
SnxCodeConvertAgentOidToAgtxOid (tSNMP_OID_TYPE * pSnmpOid,
                                 tSnmpAgentxOidType * pSnxOidType,
                                 UINT4 u4PrefixFlag)
{
    UINT4              *pu4Oid = pSnmpOid->pu4_OidList;

    /* Get the number of sub-identifiers from Snmp OID */
    pSnxOidType->u1NoSubId = (UINT1) pSnmpOid->u4_Length;

    /* Include field */
    pSnxOidType->b1Include = 1;

    /* Reserved field */
    pSnxOidType->u1Reserved = 0;
    pSnxOidType->u1Prefix = 0;

    if (u4PrefixFlag == OSIX_TRUE)
    {
        /* Get the prefix from Snmp OID */
        SNX_CMP_INET_PREFIX (pu4Oid, pSnxOidType->u1Prefix);
    }

    /* Check to see if the SNMP OID list has internet prefix attached
     * if so, then subtract that length from the total sub-identifiers 
     * length and copy the remaining sub-ids */
    if (pSnxOidType->u1Prefix != 0)
    {

        pSnxOidType->u1NoSubId =
            (UINT1) (pSnxOidType->u1NoSubId - SNX_INET_PREFIX_LEN);
        MEMCPY (pSnxOidType->pu4OidLst, &pu4Oid[SNX_INET_PREFIX_LEN],
                (pSnxOidType->u1NoSubId * SNX_4BYTE_DATALEN));
    }
    else
    {
        MEMCPY (pSnxOidType->pu4OidLst, pu4Oid,
                (pSnxOidType->u1NoSubId * SNX_4BYTE_DATALEN));
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxCodeConvertAgentPduToAgtxPdu
 *                                                                          
 *    DESCRIPTION      : This function converts Agentx PDU format
 *                       to SNMP Agent PDU format
 *
 *    INPUT            : pSnmpPdu - Pointer to the SNMP PDU
 *                                                                          
 *    OUTPUT           : pAgentxPdu - Pointer to the Agentx PDU
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE 
 *    
 ****************************************************************************/
INT4
SnxCodeConvertAgentPduToAgtxPdu (tSNMP_NORMAL_PDU * pSnmpPdu,
                                 tSnmpAgentxPdu * pAgentxPdu)
{
    switch (pSnmpPdu->i2_PduType)
    {
        case SNMP_PDU_TYPE_GET_RESPONSE:
            pAgentxPdu->u1PduType = SNX_RESPONSE_PDU;
            if (SnxCodeConvertVbLstToAgtxVbList (pSnmpPdu->pVarBindList,
                                                 &(pAgentxPdu->pAgxVarBindList))
                != SNMP_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            if (pAgentxPdu->u2Error != SNX_NO_ERR)
            {
                /* Update the AGENTX Varbind list w.r.t Error code */
                SnxCodeUpdateRespPduWithErrId (pAgentxPdu);
            }
            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxCodeUpdateRespPduWithErrId
 *                                                                          
 *    DESCRIPTION      : This function updates SNMP Agent PDU with 
 *                       appropriate error code 
 *
 *    INPUT            : pAgentxPdu - Pointer to the Agentx PDU
 *                                                                          
 *    OUTPUT           : pAgentxPdu - Pointer to the Agentx PDU with 
 *                       appropriate error code
 *                                                                          
 *    RETURNS          : None 
 *    
 ****************************************************************************/
VOID
SnxCodeUpdateRespPduWithErrId (tSnmpAgentxPdu * pAgentxPdu)
{
    tSnmpAgentxVarBind *pVarBind = pAgentxPdu->pAgxVarBindList;
    UINT2               u2ErrIndx = (UINT2) ((pAgentxPdu->u2ErrIndex) - 1);

    while (u2ErrIndx)
    {
        pVarBind = pVarBind->pNextVarBind;
        u2ErrIndx--;
    }
    switch (pAgentxPdu->u2Error)
    {
        case SNMP_EXCEPTION_NO_SUCH_OBJECT:
            pVarBind->u2Type = SNMP_EXCEPTION_NO_SUCH_OBJECT;
            pAgentxPdu->u2Error = SNX_NO_ERR;
            pAgentxPdu->u2ErrIndex = 0;
            break;
        case SNMP_EXCEPTION_NO_SUCH_INSTANCE:
            pVarBind->u2Type = SNMP_EXCEPTION_NO_SUCH_INSTANCE;
            pAgentxPdu->u2Error = SNX_NO_ERR;
            pAgentxPdu->u2ErrIndex = 0;
            break;
        case SNMP_EXCEPTION_END_OF_MIB_VIEW:
            pVarBind->u2Type = SNMP_EXCEPTION_END_OF_MIB_VIEW;
            pAgentxPdu->u2Error = SNX_NO_ERR;
            pAgentxPdu->u2ErrIndex = 0;
            break;
        default:
            break;
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxCodeConvertAgtxPduToAgentPdu
 *                                                                          
 *    DESCRIPTION      : This function converts SNMP Agent PDU format
 *                       to Agentx PDU format
 *
 *    INPUT            : pAgentxPdu - Pointer to the Agentx PDU
 *                                                                          
 *    OUTPUT           : pSnmpPdu - Pointer to the SNMP PDU
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE 
 *    
 ****************************************************************************/
INT4
SnxCodeConvertAgtxPduToAgentPdu (tSnmpAgentxPdu * pAgentxPdu,
                                 tSNMP_NORMAL_PDU * pSnmpPdu)
{
    INT4                i4RetVal = SNMP_SUCCESS;
    UINT1               u1IsTypeSet = OSIX_FALSE;

    pSnmpPdu->u4_Version = pAgentxPdu->u1Version;

    switch (pAgentxPdu->u1PduType)
    {
        case SNX_GETBULK_PDU:
            pSnmpPdu->i4_NonRepeaters = (INT4) pAgentxPdu->u2NonRepeaters;
            pSnmpPdu->i4_MaxRepetitions = (INT4) pAgentxPdu->u2MaxRepetitions;
            pSnmpPdu->i2_PduType = SNMP_PDU_TYPE_GET_BULK_REQUEST;
            u1IsTypeSet = OSIX_TRUE;
            /* Fall thro' */
        case SNX_GETNEXT_PDU:
            if (u1IsTypeSet == OSIX_FALSE)
            {
                pSnmpPdu->i2_PduType = SNMP_PDU_TYPE_GET_NEXT_REQUEST;
                u1IsTypeSet = OSIX_TRUE;
            }
            /* Fall thro' */
        case SNX_GET_PDU:
            if (u1IsTypeSet == OSIX_FALSE)
            {
                pSnmpPdu->i2_PduType = SNMP_PDU_TYPE_GET_REQUEST;
            }
            if (SnxCodeConvertAgtxSrchRngLstToVbList (pAgentxPdu->pSearchRngLst,
                                                      &(pSnmpPdu->pVarBindList),
                                                      &(pSnmpPdu->pVarBindEnd),
                                                      pAgentxPdu->u1PduType)
                != SNMP_SUCCESS)
            {
                i4RetVal = SNMP_FAILURE;
                break;
            }
            break;
        case SNX_TESTSET_PDU:
            pSnmpPdu->i2_PduType = SNMP_PDU_TYPE_SET_REQUEST;
            if (SnxCodeConvertAgtxVbLstToVbList (pAgentxPdu->pAgxVarBindList,
                                                 &(pSnmpPdu->pVarBindList),
                                                 &(pSnmpPdu->pVarBindEnd))
                != SNMP_SUCCESS)
            {
                i4RetVal = SNMP_FAILURE;
                break;
            }
            break;
        default:
            i4RetVal = SNMP_FAILURE;
    }

    return i4RetVal;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxCodeEncodePacket
 *                                                                          
 *    DESCRIPTION      : Encodes the PDU for the packet to be transimitted.
 *
 *    INPUT            : pAgentxPdu - Pointer to the Agentx PDU
 *                                                                          
 *    OUTPUT           : pu1Buf     -  Encoded Buffer
 *                       pu4OutLen  -  Length of the encoded packet
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE 
 *    
 ****************************************************************************/
INT4
SnxCodeEncodePacket (tSnmpAgentxPdu * pAgentxPdu, UINT1 *pu1Buf,
                     UINT4 *pu4OutLen)
{
    UINT4               u4OffSet = 0;

    SNX_PUT_1BYTE (pAgentxPdu->u1Version, u4OffSet, pu1Buf);

    SNX_PUT_1BYTE (pAgentxPdu->u1PduType, u4OffSet, pu1Buf);

    SNX_PUT_1BYTE (pAgentxPdu->u1Flags, u4OffSet, pu1Buf);

    /* Reserved Field */

    pu1Buf[u4OffSet++] = 0;

    SnxCodePut4ByteData ((UINT4) pAgentxPdu->i4SessionId, &u4OffSet,
                         pAgentxPdu->u1Flags, pu1Buf);

    SnxCodePut4ByteData (pAgentxPdu->u4TransactionId, &u4OffSet,
                         pAgentxPdu->u1Flags, pu1Buf);

    SnxCodePut4ByteData (pAgentxPdu->u4PacketId, &u4OffSet, pAgentxPdu->u1Flags,
                         pu1Buf);

    SnxCodePut4ByteData (pAgentxPdu->u4PayLoadLen, &u4OffSet,
                         pAgentxPdu->u1Flags, pu1Buf);

    /* Encoding Pdu specific data Open,Close,Register,Unregister,
     * Index alloc/dealloc, Add/remove agent capabilities,
     * Trap */

    if (SnxCodeEncodePduSpecificData (pu1Buf, u4OffSet, pAgentxPdu)
        != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pu4OutLen = pAgentxPdu->u4PayLoadLen + SNX_AGENTX_PDU_HDR_LEN;

    return SNMP_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxCodeConvertAgtxVbLstToVbList
 *                                                                          
 *    DESCRIPTION      : This function converts SNMP Agent Variable binding
 *                       to Agentx Variable binding
 *
 *    INPUT            : pAgtxVbList - Pointer to SNMP Agent Varbind List
 *                                                                          
 *    OUTPUT           : ppVarbindList - Pointer to SNMP Varbind List pointer
 *                       ppEndVbPtr - Pointer to SNMP Varbind End pointer
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE 
 *    
 ****************************************************************************/
INT4
SnxCodeConvertAgtxVbLstToVbList (tSnmpAgentxVarBind * pAgtxVbList,
                                 tSNMP_VAR_BIND ** ppVarbindList,
                                 tSNMP_VAR_BIND ** ppEndVbPtr)
{
    tSNMP_VAR_BIND     *pSnmpAgtVbList = NULL;
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pPrevSnmpAgtVbList = NULL;
    tSnmpAgentxVarBind *pSnmpAgtxVbList = pAgtxVbList;

    while (pSnmpAgtxVbList != NULL)
    {
        pSnmpAgtVbList = SNMPAllocVarBind ();
        if (pSnmpAgtVbList == NULL)
        {
            SNMPMemInit ();
            return SNMP_FAILURE;
        }
        pSnmpAgtVbList->pNextVarBind = NULL;
        if (pVbList == NULL)
        {
            pVbList = pSnmpAgtVbList;
        }

        if (pPrevSnmpAgtVbList != NULL)
        {
            pPrevSnmpAgtVbList->pNextVarBind = pSnmpAgtVbList;
        }

        pSnmpAgtVbList->ObjValue.i2_DataType = (INT2) pSnmpAgtxVbList->u2Type;

        if (SnxCodeConvertAgtxOidToAgentOid (&(pSnmpAgtxVbList->ObjName),
                                             pSnmpAgtVbList->pObjName)
            != SNMP_SUCCESS)
        {
            SNMPMemInit ();
            return SNMP_FAILURE;
        }

        switch (pSnmpAgtVbList->ObjValue.i2_DataType)
        {
            case SNMP_DATA_TYPE_COUNTER32:
                /* Fall thro' */
            case SNMP_DATA_TYPE_GAUGE32:
                /* Fall thro' */
            case SNMP_DATA_TYPE_TIME_TICKS:
                pSnmpAgtVbList->ObjValue.u4_ULongValue =
                    pSnmpAgtxVbList->ObjValue.u4_ULongValue;
                break;
            case SNMP_DATA_TYPE_COUNTER64:
                pSnmpAgtVbList->ObjValue.u8_Counter64Value
                    = pSnmpAgtxVbList->ObjValue.u8_Counter64Value;
                break;
            case SNMP_DATA_TYPE_INTEGER32:
                pSnmpAgtVbList->ObjValue.i4_SLongValue
                    = pSnmpAgtxVbList->ObjValue.i4_SLongValue;
                break;
            case SNMP_DATA_TYPE_OBJECT_ID:
                pSnmpAgtVbList->ObjValue.pOidValue->u4_Length =
                    pSnmpAgtxVbList->ObjValue.pOidValue->u4_Length;
                MEMCPY (pSnmpAgtVbList->ObjValue.pOidValue->pu4_OidList,
                        pSnmpAgtxVbList->ObjValue.pOidValue->pu4_OidList,
                        ((pSnmpAgtxVbList->ObjValue.pOidValue->u4_Length)
                         * sizeof (UINT4)));
                break;
            case SNMP_DATA_TYPE_OCTET_PRIM:
                /* Fall thro' */
            case SNMP_DATA_TYPE_OPAQUE:
                pSnmpAgtVbList->ObjValue.pOctetStrValue->i4_Length =
                    pSnmpAgtxVbList->ObjValue.pOctetStrValue->i4_Length;
                MEMCPY (pSnmpAgtVbList->ObjValue.pOctetStrValue->pu1_OctetList,
                        pSnmpAgtxVbList->ObjValue.pOctetStrValue->pu1_OctetList,
                        pSnmpAgtxVbList->ObjValue.pOctetStrValue->i4_Length);
                break;
            case SNMP_DATA_TYPE_IP_ADDR_PRIM:
                pSnmpAgtVbList->ObjValue.u4_ULongValue
                    = pSnmpAgtxVbList->ObjValue.u4_ULongValue;

                pSnmpAgtVbList->ObjValue.pOctetStrValue->i4_Length =
                    pSnmpAgtxVbList->ObjValue.pOctetStrValue->i4_Length;
                MEMCPY (pSnmpAgtVbList->ObjValue.pOctetStrValue->pu1_OctetList,
                        pSnmpAgtxVbList->ObjValue.pOctetStrValue->pu1_OctetList,
                        pSnmpAgtxVbList->ObjValue.pOctetStrValue->i4_Length);
                break;
            case SNMP_DATA_TYPE_NULL:
                /* Null type - Do nothing */
                break;
            default:
                SNMPTrace ("Not a valid AgentX Object Type\n");
                SNMPMemInit ();
                return SNMP_FAILURE;

        }

        pSnmpAgtxVbList = pSnmpAgtxVbList->pNextVarBind;
        pPrevSnmpAgtVbList = pSnmpAgtVbList;
        pSnmpAgtVbList = NULL;
    }

    *ppEndVbPtr = pPrevSnmpAgtVbList;
    *ppVarbindList = pVbList;

    return SNMP_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxCodeConvertVbLstToAgtxVbList
 *                                                                          
 *    DESCRIPTION      : This function converts SNMP Agent Variable binding
 *                       to Agentx Variable binding
 *
 *    INPUT            : pVbList - Pointer to Agent Varbind List
 *                                                                          
 *    OUTPUT           : ppAgentxVbList - Pointer to Agentx Varbind List
 *                       pointer
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE 
 *    
 ****************************************************************************/
INT4
SnxCodeConvertVbLstToAgtxVbList (tSNMP_VAR_BIND * pVbList,
                                 tSnmpAgentxVarBind ** ppAgentxVbList)
{
    tSNMP_VAR_BIND     *pSnmpAgtVbList = pVbList;
    tSnmpAgentxVarBind *pSnmpAgtxVbList = NULL;
    tSnmpAgentxVarBind *pAgtxVbList = NULL;
    tSnmpAgentxVarBind *pPrevSnmpAgtxVbList = NULL;

    while (pSnmpAgtVbList != NULL)
    {
        pSnmpAgtxVbList = SnxUtlAllocVarbind (pSnmpAgtxVbList);
        if (pSnmpAgtxVbList == NULL)
        {
            SnxUtlFreeVbList (pAgtxVbList);
            return SNMP_FAILURE;
        }

        if (pAgtxVbList == NULL)
        {
            pAgtxVbList = pSnmpAgtxVbList;
        }

        if (pPrevSnmpAgtxVbList != NULL)
        {
            pPrevSnmpAgtxVbList->pNextVarBind = pSnmpAgtxVbList;
        }

        pSnmpAgtxVbList->u2Type = (UINT2) pSnmpAgtVbList->ObjValue.i2_DataType;
        pSnmpAgtxVbList->u2Reserved = 0;

        if (pSnmpAgtVbList->pObjName != NULL)
        {
            if (SnxCodeConvertAgentOidToAgtxOid (pSnmpAgtVbList->pObjName,
                                                 &(pSnmpAgtxVbList->ObjName),
                                                 OSIX_TRUE) != SNMP_SUCCESS)
            {
                SnxUtlFreeVbList (pAgtxVbList);
                return SNMP_FAILURE;
            }

        }

        switch (pSnmpAgtxVbList->u2Type)
        {
            case SNMP_DATA_TYPE_COUNTER32:
                /* Fall thro' */
            case SNMP_DATA_TYPE_GAUGE32:
                /* Fall thro' */
            case SNMP_DATA_TYPE_TIME_TICKS:
                pSnmpAgtxVbList->ObjValue.u4_ULongValue =
                    pSnmpAgtVbList->ObjValue.u4_ULongValue;
                break;
            case SNMP_DATA_TYPE_COUNTER64:
                pSnmpAgtxVbList->ObjValue.u8_Counter64Value
                    = pSnmpAgtVbList->ObjValue.u8_Counter64Value;
                break;
            case SNMP_DATA_TYPE_INTEGER32:
                pSnmpAgtxVbList->ObjValue.i4_SLongValue
                    = pSnmpAgtVbList->ObjValue.i4_SLongValue;
                break;
            case SNMP_DATA_TYPE_OBJECT_ID:
                pSnmpAgtxVbList->ObjValue.pOidValue->u4_Length =
                    pSnmpAgtVbList->ObjValue.pOidValue->u4_Length;
                MEMCPY (pSnmpAgtxVbList->ObjValue.pOidValue->pu4_OidList,
                        pSnmpAgtVbList->ObjValue.pOidValue->pu4_OidList,
                        (pSnmpAgtxVbList->ObjValue.pOidValue->u4_Length
                         * SNX_4BYTE_DATALEN));
                break;
            case SNMP_DATA_TYPE_OCTET_PRIM:
                /* Fall thro' */
            case SNMP_DATA_TYPE_OPAQUE:
                pSnmpAgtxVbList->ObjValue.pOctetStrValue->i4_Length =
                    pSnmpAgtVbList->ObjValue.pOctetStrValue->i4_Length;
                MEMCPY (pSnmpAgtxVbList->ObjValue.pOctetStrValue->pu1_OctetList,
                        pSnmpAgtVbList->ObjValue.pOctetStrValue->pu1_OctetList,
                        pSnmpAgtVbList->ObjValue.pOctetStrValue->i4_Length);
                break;
            case SNMP_DATA_TYPE_IP_ADDR_PRIM:
                pSnmpAgtxVbList->ObjValue.u4_ULongValue
                    = pSnmpAgtVbList->ObjValue.u4_ULongValue;
                break;
            case SNMP_DATA_TYPE_NULL:
                /* Null type - Do nothing */
                break;
            default:
                break;

        }

        pPrevSnmpAgtxVbList = pSnmpAgtxVbList;
        pSnmpAgtxVbList = NULL;
        pSnmpAgtVbList = pSnmpAgtVbList->pNextVarBind;

    }

    *ppAgentxVbList = pAgtxVbList;
    return SNMP_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxCodeConvertAgtxSrchRngLstToVbList
 *                                                                          
 *    DESCRIPTION      : This function converts Agentx SearchRange List
 *                       to Agent Variable binding List
 *
 *    INPUT            : pSrchRngLst - Pointer to AgentX Search Range List
 *                       u1PduType - PDU Type
 *                                                                          
 *    OUTPUT           : ppVbList - Pointer to SNMP Agent Varbind list pointer
 *                       ppEndVb - Pointer to SNMP Ending Varbind pointer
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE 
 *    
 ****************************************************************************/
INT4
SnxCodeConvertAgtxSrchRngLstToVbList (tSnmpSearchRng * pSrchRngLst,
                                      tSNMP_VAR_BIND ** ppVbList,
                                      tSNMP_VAR_BIND ** ppEndVb,
                                      UINT1 u1PduType)
{
    tSnmpSearchRng     *pSrchRange = pSrchRngLst;
    tSNMP_VAR_BIND     *pVarBindStart = NULL;
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pPrevVb = NULL;

    UNUSED_PARAM (u1PduType);
    while (pSrchRange != NULL)
    {
        pVarBindStart = SNMPAllocVarBind ();

        if (pVarBindStart == NULL)
        {
            SNMPMemInit ();
            return SNMP_FAILURE;
        }
        pVarBindStart->pNextVarBind = NULL;
        if (pVbList == NULL)
        {
            pVbList = pVarBindStart;
        }
        if (pPrevVb != NULL)
        {
            pPrevVb->pNextVarBind = pVarBindStart;
        }
        /* Gets the start OID of a search range and fill it in
         * the start varbind and end varbind */
        if (SnxCodeConvertAgtxSrchRngToVb (pSrchRange,
                                           pVarBindStart) != SNMP_SUCCESS)
        {
            SNMPMemInit ();
            return SNMP_FAILURE;
        }
        pSrchRange = pSrchRange->pNextSearchRng;
        /* Attaching the varbinds to the original varbind list */
        pPrevVb = pVarBindStart;
        pVarBindStart = NULL;
    }
    *ppEndVb = pPrevVb;
    *ppVbList = pVbList;
    return SNMP_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxCodeConvertAgtxSrchRngToVb
 *                                                                          
 *    DESCRIPTION      : This function converts SNMP Agent Variable binding
 *                       to Agentx Variable binding
 *
 *    INPUT            : pSrchRngLst - Pointer to Search Range List
 *                                                                          
 *    OUTPUT           : pVbList - Pointer to Variable Bindings List
 *                                                                          
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE 
 *    
 ****************************************************************************/
INT4
SnxCodeConvertAgtxSrchRngToVb (tSnmpSearchRng * pSrchRngLst,
                               tSNMP_VAR_BIND * pVbList)
{
    if (pSrchRngLst->StartId.u1NoSubId != 0)
    {
        pVbList->pObjName->u4_Length = pSrchRngLst->StartId.u1NoSubId;

        if (pSrchRngLst->StartId.u1Prefix != 0)
        {
            pVbList->pObjName->u4_Length = pSrchRngLst->StartId.u1NoSubId;
            pVbList->pObjName->u4_Length += SNX_INET_PREFIX_LEN;
        }

        if (SnxCodeConvertAgtxOidToAgentOid (&(pSrchRngLst->StartId),
                                             pVbList->pObjName) != SNMP_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : SnxCodeGetVbLstLen
 *                                                                          
 *    DESCRIPTION      : This function gets the length of Agentx Variable 
 *                       binding
 *
 *    INPUT            : pAgtxVbList - Pointer to AgentX Varbind
 *                                                                          
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : u4VbLen - Variable List Length 
 *    
 ****************************************************************************/
INT4
SnxCodeGetVbLstLen (tSnmpAgentxVarBind * pAgtxVbList)
{
    UINT4               u4VbLen = 0;
    UINT4               u4Pad = 0;
    while (pAgtxVbList != NULL)
    {
        /* 2 Byte respresenting type and 2 Byte reserved */
        u4VbLen += SNX_4BYTE_DATALEN;
        /* Variable name length */
        u4VbLen += (UINT4) ((SNX_OID_INFO_SIZE +
                             (pAgtxVbList->ObjName.u1NoSubId *
                              SNX_4BYTE_DATALEN)));
        switch (pAgtxVbList->u2Type)
        {
            case SNMP_DATA_TYPE_COUNTER32:
                /* Fall thro' */
            case SNMP_DATA_TYPE_GAUGE32:
                /* Fall thro' */
            case SNMP_DATA_TYPE_TIME_TICKS:
                u4VbLen += SNX_4BYTE_DATALEN;
                break;
            case SNMP_DATA_TYPE_COUNTER64:
                u4VbLen += (2 * SNX_4BYTE_DATALEN);
                break;
            case SNMP_DATA_TYPE_INTEGER32:
                u4VbLen += SNX_4BYTE_DATALEN;
                break;
            case SNMP_DATA_TYPE_OBJECT_ID:
                u4VbLen += (SNX_OID_INFO_SIZE +
                            (pAgtxVbList->ObjValue.pOidValue->u4_Length
                             * SNX_4BYTE_DATALEN));
                break;
            case SNMP_DATA_TYPE_OCTET_PRIM:
                /* Fall thro' */
            case SNMP_DATA_TYPE_OPAQUE:
                /* For 4 Byte alignment */
                u4Pad =
                    (UINT4) (pAgtxVbList->ObjValue.pOctetStrValue->i4_Length);
                if ((u4Pad % SNX_4BYTE_DATALEN) != 0)
                {
                    u4Pad += (SNX_4BYTE_DATALEN - (u4Pad % SNX_4BYTE_DATALEN));
                }

                u4VbLen += SNX_OID_INFO_SIZE + u4Pad;
                break;
            case SNMP_DATA_TYPE_IP_ADDR_PRIM:
                u4VbLen += SNX_OID_INFO_SIZE + SNX_4BYTE_DATALEN;
                break;
            case SNMP_DATA_TYPE_NULL:
                /* Fall thro` */
            case SNMP_EXCEPTION_NO_SUCH_OBJECT:
                /* Fall thro` */
            case SNMP_EXCEPTION_NO_SUCH_INSTANCE:
                /* Fall thro` */
            case SNMP_EXCEPTION_END_OF_MIB_VIEW:
                /* Null type - Do nothing */
                break;
            default:
                SNMPTrace ("Not a valid AgentX Object Type.\n");
                return SNMP_FAILURE;
        }
        pAgtxVbList = pAgtxVbList->pNextVarBind;
    }
    return ((INT4) u4VbLen);
}

/***************************  End of snxcode.c *******************************/
