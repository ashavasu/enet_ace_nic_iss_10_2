/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: sntargdb.h,v 1.5 2015/04/28 12:35:03 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _SNTARGDB_H
#define _SNTARGDB_H

UINT1 SnmpTargetAddrTableINDEX [] = {SNMP_DATA_TYPE_IMP_OCTET_PRIM};
UINT1 SnmpTargetParamsTableINDEX [] = {SNMP_DATA_TYPE_IMP_OCTET_PRIM};

UINT4 sntarg [] ={1,3,6,1,6,3,12};
tSNMP_OID_TYPE sntargOID = {7, sntarg};


UINT4 SnmpTargetSpinLock [ ] ={1,3,6,1,6,3,12,1,1};
UINT4 SnmpTargetAddrName [ ] ={1,3,6,1,6,3,12,1,2,1,1};
UINT4 SnmpTargetAddrTDomain [ ] ={1,3,6,1,6,3,12,1,2,1,2};
UINT4 SnmpTargetAddrTAddress [ ] ={1,3,6,1,6,3,12,1,2,1,3};
UINT4 SnmpTargetAddrTimeout [ ] ={1,3,6,1,6,3,12,1,2,1,4};
UINT4 SnmpTargetAddrRetryCount [ ] ={1,3,6,1,6,3,12,1,2,1,5};
UINT4 SnmpTargetAddrTagList [ ] ={1,3,6,1,6,3,12,1,2,1,6};
UINT4 SnmpTargetAddrParams [ ] ={1,3,6,1,6,3,12,1,2,1,7};
UINT4 SnmpTargetAddrStorageType [ ] ={1,3,6,1,6,3,12,1,2,1,8};
UINT4 SnmpTargetAddrRowStatus [ ] ={1,3,6,1,6,3,12,1,2,1,9};
UINT4 SnmpTargetParamsName [ ] ={1,3,6,1,6,3,12,1,3,1,1};
UINT4 SnmpTargetParamsMPModel [ ] ={1,3,6,1,6,3,12,1,3,1,2};
UINT4 SnmpTargetParamsSecurityModel [ ] ={1,3,6,1,6,3,12,1,3,1,3};
UINT4 SnmpTargetParamsSecurityName [ ] ={1,3,6,1,6,3,12,1,3,1,4};
UINT4 SnmpTargetParamsSecurityLevel [ ] ={1,3,6,1,6,3,12,1,3,1,5};
UINT4 SnmpTargetParamsStorageType [ ] ={1,3,6,1,6,3,12,1,3,1,6};
UINT4 SnmpTargetParamsRowStatus [ ] ={1,3,6,1,6,3,12,1,3,1,7};
UINT4 SnmpUnavailableContexts [ ] ={1,3,6,1,6,3,12,1,4};
UINT4 SnmpUnknownContexts [ ] ={1,3,6,1,6,3,12,1,5};


tMbDbEntry sntargMibEntry[]= {

{{9,SnmpTargetSpinLock}, NULL, SnmpTargetSpinLockGet, SnmpTargetSpinLockSet, SnmpTargetSpinLockTest, SnmpTargetSpinLockDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,SnmpTargetAddrName}, GetNextIndexSnmpTargetAddrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, SnmpTargetAddrTableINDEX, 1, 0, 0, NULL},

{{11,SnmpTargetAddrTDomain}, GetNextIndexSnmpTargetAddrTable, SnmpTargetAddrTDomainGet, SnmpTargetAddrTDomainSet, SnmpTargetAddrTDomainTest, SnmpTargetAddrTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, SnmpTargetAddrTableINDEX, 1, 0, 0, NULL},

{{11,SnmpTargetAddrTAddress}, GetNextIndexSnmpTargetAddrTable, SnmpTargetAddrTAddressGet, SnmpTargetAddrTAddressSet, SnmpTargetAddrTAddressTest, SnmpTargetAddrTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, SnmpTargetAddrTableINDEX, 1, 0, 0, NULL},

{{11,SnmpTargetAddrTimeout}, GetNextIndexSnmpTargetAddrTable, SnmpTargetAddrTimeoutGet, SnmpTargetAddrTimeoutSet, SnmpTargetAddrTimeoutTest, SnmpTargetAddrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, SnmpTargetAddrTableINDEX, 1, 0, 0, "1500"},

{{11,SnmpTargetAddrRetryCount}, GetNextIndexSnmpTargetAddrTable, SnmpTargetAddrRetryCountGet, SnmpTargetAddrRetryCountSet, SnmpTargetAddrRetryCountTest, SnmpTargetAddrTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, SnmpTargetAddrTableINDEX, 1, 0, 0, "3"},

{{11,SnmpTargetAddrTagList}, GetNextIndexSnmpTargetAddrTable, SnmpTargetAddrTagListGet, SnmpTargetAddrTagListSet, SnmpTargetAddrTagListTest, SnmpTargetAddrTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, SnmpTargetAddrTableINDEX, 1, 0, 0, ""},

{{11,SnmpTargetAddrParams}, GetNextIndexSnmpTargetAddrTable, SnmpTargetAddrParamsGet, SnmpTargetAddrParamsSet, SnmpTargetAddrParamsTest, SnmpTargetAddrTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, SnmpTargetAddrTableINDEX, 1, 0, 0, NULL},

{{11,SnmpTargetAddrStorageType}, GetNextIndexSnmpTargetAddrTable, SnmpTargetAddrStorageTypeGet, SnmpTargetAddrStorageTypeSet, SnmpTargetAddrStorageTypeTest, SnmpTargetAddrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, SnmpTargetAddrTableINDEX, 1, 0, 0, "3"},

{{11,SnmpTargetAddrRowStatus}, GetNextIndexSnmpTargetAddrTable, SnmpTargetAddrRowStatusGet, SnmpTargetAddrRowStatusSet, SnmpTargetAddrRowStatusTest, SnmpTargetAddrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, SnmpTargetAddrTableINDEX, 1, 0, 1, NULL},

{{11,SnmpTargetParamsName}, GetNextIndexSnmpTargetParamsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, SnmpTargetParamsTableINDEX, 1, 0, 0, NULL},

{{11,SnmpTargetParamsMPModel}, GetNextIndexSnmpTargetParamsTable, SnmpTargetParamsMPModelGet, SnmpTargetParamsMPModelSet, SnmpTargetParamsMPModelTest, SnmpTargetParamsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, SnmpTargetParamsTableINDEX, 1, 0, 0, NULL},

{{11,SnmpTargetParamsSecurityModel}, GetNextIndexSnmpTargetParamsTable, SnmpTargetParamsSecurityModelGet, SnmpTargetParamsSecurityModelSet, SnmpTargetParamsSecurityModelTest, SnmpTargetParamsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, SnmpTargetParamsTableINDEX, 1, 0, 0, NULL},

{{11,SnmpTargetParamsSecurityName}, GetNextIndexSnmpTargetParamsTable, SnmpTargetParamsSecurityNameGet, SnmpTargetParamsSecurityNameSet, SnmpTargetParamsSecurityNameTest, SnmpTargetParamsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, SnmpTargetParamsTableINDEX, 1, 0, 0, NULL},

{{11,SnmpTargetParamsSecurityLevel}, GetNextIndexSnmpTargetParamsTable, SnmpTargetParamsSecurityLevelGet, SnmpTargetParamsSecurityLevelSet, SnmpTargetParamsSecurityLevelTest, SnmpTargetParamsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, SnmpTargetParamsTableINDEX, 1, 0, 0, NULL},

{{11,SnmpTargetParamsStorageType}, GetNextIndexSnmpTargetParamsTable, SnmpTargetParamsStorageTypeGet, SnmpTargetParamsStorageTypeSet, SnmpTargetParamsStorageTypeTest, SnmpTargetParamsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, SnmpTargetParamsTableINDEX, 1, 0, 0, "3"},

{{11,SnmpTargetParamsRowStatus}, GetNextIndexSnmpTargetParamsTable, SnmpTargetParamsRowStatusGet, SnmpTargetParamsRowStatusSet, SnmpTargetParamsRowStatusTest, SnmpTargetParamsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, SnmpTargetParamsTableINDEX, 1, 0, 1, NULL},

{{9,SnmpUnavailableContexts}, NULL, SnmpUnavailableContextsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,SnmpUnknownContexts}, NULL, SnmpUnknownContextsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData sntargEntry = { 19, sntargMibEntry };
#endif /* _SNTARGDB_H */

