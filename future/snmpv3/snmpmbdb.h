
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: snmpmbdb.h,v 1.7 2015/04/28 12:35:02 siva Exp $
 *
 * Description: macros and prototypes for db access module
 *******************************************************************/
#ifndef _SNMPMBDB_H
#define _SNMPMBDB_H
/* #define SNMP_LESSER  1
#define SNMP_GREATER  2 
#define SNMP_EQUAL  0  */
#define NOT_FOUND 3
#define GREATER_OUT 4
#define LESSER_OUT 5


typedef struct MibReg{
 tSNMP_OID_TYPE *pMibID;
 tMibData *pCurMib;
   INT4     (*pLockPointer)(VOID);
   INT4     (*pUnlockPointer)(VOID);
   INT4     (*pSetContextPointer)(UINT4);
   VOID     (*pReleaseContextPointer)(VOID);
   tSnmpMsrBool SnmpMsrTgr;
 struct MibReg *pNextMib;
}tMibReg;

/*----------------------Function Prototypes--------------*/
VOID OIDPrint ( tSNMP_OID_TYPE * ptr);
VOID OctetListPrint (tSNMP_OCTET_STRING_TYPE * ptr);
VOID VarBindListPrint (tSNMP_VAR_BIND * ptr);
VOID SNMPPduPrint (tSNMP_NORMAL_PDU * pPtr);
INT4 AttachMib (tSNMP_OID_TYPE * MibID, tMibData * pMibData,
                INT4 (*)(VOID),INT4(*)(VOID),
                INT4 (*pSetContextPointer) (UINT4),
                VOID (*pReleaseContextPointer) (VOID),
                tSnmpMsrBool ,UINT4 u4Index);
tMbDbEntry* OIDSearch(tMibData *gMib,tSNMP_OID_TYPE CurOID,UINT4 *u4Result,
                        tMbDbEntry** pNext,UINT4 *u4Match);
tMibReg* MibRegAllocate(UINT4 u4Index);
tMibData* MibDataAlloc (UINT4 * u4Index);
tSNMP_OID_TYPE * OidAlloc (UINT4 u4Index);

#define SNMP_MAX_NUM_OF_MIBS  800

#endif /* _SNMPMBDB_H */
