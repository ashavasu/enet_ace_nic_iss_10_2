/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: vacmacc.h,v 1.4 2015/04/28 12:35:03 siva Exp $
 *
 * Description: prototypes and macros for VACM Access 
 *              Table Database Model 
 *******************************************************************/

#ifndef _VACMACC_H 
#define _VACMACC_H

VOID VACMAddAccessEntrySll(tTMO_SLL_NODE *);
VOID SNMPDelVacmAccessEntrySll(tTMO_SLL_NODE *);

tAccessEntry gaAccessEntry[SNMP_MAX_ACCESS_ENTRY];
tTMO_SLL     gSnmpAccessEntry;  
UINT1        au1ReadName[SNMP_MAX_ACCESS_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
UINT1        au1WriteName[SNMP_MAX_ACCESS_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
UINT1        au1NotifyName[SNMP_MAX_ACCESS_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
UINT1        au1ContextName[SNMP_MAX_ACCESS_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
#endif /* _VACMACC_H */
