/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: snmproxlw.c,v 1.1 2015/04/28 12:35:02 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "snmpcmn.h"
# include  "snmproxlw.h"

/* LOW LEVEL Routines for Table : SnmpProxyTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceSnmpProxyTable
 Input       :  The Indices
                SnmpProxyName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceSnmpProxyTable (tSNMP_OCTET_STRING_TYPE *
                                        pSnmpProxyName)
{
    tProxyTableEntry   *pProxyEntry = NULL;

    pProxyEntry = SNMPGetProxyEntryFromName (pSnmpProxyName);
    if (pProxyEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexSnmpProxyTable
 Input       :  The Indices
                SnmpProxyName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexSnmpProxyTable (tSNMP_OCTET_STRING_TYPE * pSnmpProxyName)
{
    tProxyTableEntry   *pFirst = NULL;

    pFirst = SNMPGetFirstProxy ();
    if (pFirst == NULL)
    {
        return SNMP_FAILURE;
    }

    SNMPCopyOctetString (pSnmpProxyName, &(pFirst->ProxyName));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexSnmpProxyTable
 Input       :  The Indices
                SnmpProxyName
                nextSnmpProxyName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexSnmpProxyTable (tSNMP_OCTET_STRING_TYPE * pSnmpProxyName,
                               tSNMP_OCTET_STRING_TYPE * pNextSnmpProxyName)
{
    tProxyTableEntry   *pNext = NULL;

    if (pSnmpProxyName->i4_Length >= MAX_STR_OCTET_VAL)
    {
        return SNMP_FAILURE;
    }

    pNext = SNMPGetNextProxyEntry (pSnmpProxyName);

    if (pNext == NULL)
    {
        return SNMP_FAILURE;
    }

    SNMPCopyOctetString (pNextSnmpProxyName, &(pNext->ProxyName));

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetSnmpProxyType
 Input       :  The Indices
                SnmpProxyName

                The Object 
                retValSnmpProxyType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpProxyType (tSNMP_OCTET_STRING_TYPE * pSnmpProxyName,
                     INT4 *pi4RetValSnmpProxyType)
{
    tProxyTableEntry   *pProxyEntry = NULL;

    pProxyEntry = SNMPGetProxyEntryFromName (pSnmpProxyName);
    if (pProxyEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValSnmpProxyType = pProxyEntry->ProxyType;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetSnmpProxyContextEngineID
 Input       :  The Indices
                SnmpProxyName

                The Object 
                retValSnmpProxyContextEngineID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpProxyContextEngineID (tSNMP_OCTET_STRING_TYPE *
                                pSnmpProxyName,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValSnmpProxyContextEngineID)
{
    tProxyTableEntry   *pProxyEntry = NULL;

    pProxyEntry = SNMPGetProxyEntryFromName (pSnmpProxyName);
    if (pProxyEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    SNMPCopyOctetString (pRetValSnmpProxyContextEngineID,
                         &(pProxyEntry->PrxContextEngineID));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpProxyContextName
 Input       :  The Indices
                SnmpProxyName

                The Object 
                retValSnmpProxyContextName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpProxyContextName (tSNMP_OCTET_STRING_TYPE *
                            pSnmpProxyName,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValSnmpProxyContextName)
{
    tProxyTableEntry   *pProxyEntry = NULL;

    pProxyEntry = SNMPGetProxyEntryFromName (pSnmpProxyName);
    if (pProxyEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pProxyEntry->PrxContextName.i4_Length != SNMP_ZERO)
    {
        SNMPCopyOctetString (pRetValSnmpProxyContextName,
                             &(pProxyEntry->PrxContextName));
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpProxyTargetParamsIn
 Input       :  The Indices
                SnmpProxyName

                The Object 
                retValSnmpProxyTargetParamsIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpProxyTargetParamsIn (tSNMP_OCTET_STRING_TYPE *
                               pSnmpProxyName,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValSnmpProxyTargetParamsIn)
{
    tProxyTableEntry   *pProxyEntry = NULL;

    pProxyEntry = SNMPGetProxyEntryFromName (pSnmpProxyName);
    if (pProxyEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    SNMPCopyOctetString (pRetValSnmpProxyTargetParamsIn,
                         &(pProxyEntry->PrxTargetParamsIn));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpProxySingleTargetOut
 Input       :  The Indices
                SnmpProxyName

                The Object 
                retValSnmpProxySingleTargetOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpProxySingleTargetOut (tSNMP_OCTET_STRING_TYPE *
                                pSnmpProxyName,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValSnmpProxySingleTargetOut)
{
    tProxyTableEntry   *pProxyEntry = NULL;

    pProxyEntry = SNMPGetProxyEntryFromName (pSnmpProxyName);
    if (pProxyEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    SNMPCopyOctetString (pRetValSnmpProxySingleTargetOut,
                         &(pProxyEntry->PrxSingleTargetOut));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpProxyMultipleTargetOut
 Input       :  The Indices
                SnmpProxyName

                The Object 
                retValSnmpProxyMultipleTargetOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpProxyMultipleTargetOut (tSNMP_OCTET_STRING_TYPE *
                                  pSnmpProxyName,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValSnmpProxyMultipleTargetOut)
{
    tProxyTableEntry   *pProxyEntry = NULL;

    pProxyEntry = SNMPGetProxyEntryFromName (pSnmpProxyName);
    if (pProxyEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    SNMPCopyOctetString (pRetValSnmpProxyMultipleTargetOut,
                         &(pProxyEntry->PrxMultipleTargetOut));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpProxyStorageType
 Input       :  The Indices
                SnmpProxyName

                The Object 
                retValSnmpProxyStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpProxyStorageType (tSNMP_OCTET_STRING_TYPE *
                            pSnmpProxyName, INT4 *pi4RetValSnmpProxyStorageType)
{
    tProxyTableEntry   *pProxyEntry = NULL;

    pProxyEntry = SNMPGetProxyEntryFromName (pSnmpProxyName);
    if (pProxyEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValSnmpProxyStorageType = pProxyEntry->i4Storage;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpProxyRowStatus
 Input       :  The Indices
                SnmpProxyName

                The Object 
                retValSnmpProxyRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpProxyRowStatus (tSNMP_OCTET_STRING_TYPE * pSnmpProxyName,
                          INT4 *pi4RetValSnmpProxyRowStatus)
{
    tProxyTableEntry   *pProxyEntry = NULL;

    pProxyEntry = SNMPGetProxyEntryFromName (pSnmpProxyName);
    if (pProxyEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pProxyEntry->i4Status == UNDER_CREATION)
    {
        *pi4RetValSnmpProxyRowStatus = pProxyEntry->i4Status;
    }

    if (pProxyEntry->i4Status == SNMP_ACTIVE)
    {
        *pi4RetValSnmpProxyRowStatus = SNMP_ROWSTATUS_ACTIVE;
    }

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetSnmpProxyType
 Input       :  The Indices
                SnmpProxyName

                The Object 
                setValSnmpProxyType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpProxyType (tSNMP_OCTET_STRING_TYPE * pSnmpProxyName,
                     INT4 i4SetValSnmpProxyType)
{
    tProxyTableEntry   *pProxyEntry = NULL;

    pProxyEntry = SNMPGetProxyEntryFromName (pSnmpProxyName);
    if (pProxyEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pProxyEntry->ProxyType = i4SetValSnmpProxyType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpProxyContextEngineID
 Input       :  The Indices
                SnmpProxyName

                The Object 
                setValSnmpProxyContextEngineID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpProxyContextEngineID (tSNMP_OCTET_STRING_TYPE *
                                pSnmpProxyName,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValSnmpProxyContextEngineID)
{
    tProxyTableEntry   *pProxyEntry = NULL;

    pProxyEntry = SNMPGetProxyEntryFromName (pSnmpProxyName);
    if (pProxyEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    SNMPCopyOctetString (&(pProxyEntry->PrxContextEngineID),
                         pSetValSnmpProxyContextEngineID);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpProxyContextName
 Input       :  The Indices
                SnmpProxyName

                The Object 
                setValSnmpProxyContextName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpProxyContextName (tSNMP_OCTET_STRING_TYPE *
                            pSnmpProxyName,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValSnmpProxyContextName)
{
    tProxyTableEntry   *pProxyEntry = NULL;

    pProxyEntry = SNMPGetProxyEntryFromName (pSnmpProxyName);
    if (pProxyEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    SNMPCopyOctetString (&(pProxyEntry->PrxContextName),
                         pSetValSnmpProxyContextName);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpProxyTargetParamsIn
 Input       :  The Indices
                SnmpProxyName

                The Object 
                setValSnmpProxyTargetParamsIn
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpProxyTargetParamsIn (tSNMP_OCTET_STRING_TYPE *
                               pSnmpProxyName,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValSnmpProxyTargetParamsIn)
{
    tProxyTableEntry   *pProxyEntry = NULL;

    pProxyEntry = SNMPGetProxyEntryFromName (pSnmpProxyName);
    if (pProxyEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    SNMPCopyOctetString (&(pProxyEntry->PrxTargetParamsIn),
                         pSetValSnmpProxyTargetParamsIn);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpProxySingleTargetOut
 Input       :  The Indices
                SnmpProxyName

                The Object 
                setValSnmpProxySingleTargetOut
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpProxySingleTargetOut (tSNMP_OCTET_STRING_TYPE *
                                pSnmpProxyName,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValSnmpProxySingleTargetOut)
{
    tProxyTableEntry   *pProxyEntry = NULL;

    pProxyEntry = SNMPGetProxyEntryFromName (pSnmpProxyName);
    if (pProxyEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    SNMPCopyOctetString (&(pProxyEntry->PrxSingleTargetOut),
                         pSetValSnmpProxySingleTargetOut);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpProxyMultipleTargetOut
 Input       :  The Indices
                SnmpProxyName

                The Object 
                setValSnmpProxyMultipleTargetOut
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpProxyMultipleTargetOut (tSNMP_OCTET_STRING_TYPE *
                                  pSnmpProxyName,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pSetValSnmpProxyMultipleTargetOut)
{
    tProxyTableEntry   *pProxyEntry = NULL;

    pProxyEntry = SNMPGetProxyEntryFromName (pSnmpProxyName);
    if (pProxyEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    SNMPCopyOctetString (&(pProxyEntry->PrxMultipleTargetOut),
                         pSetValSnmpProxyMultipleTargetOut);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpProxyStorageType
 Input       :  The Indices
                SnmpProxyName

                The Object 
                setValSnmpProxyStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpProxyStorageType (tSNMP_OCTET_STRING_TYPE * pSnmpProxyName,
                            INT4 i4SetValSnmpProxyStorageType)
{
    tProxyTableEntry   *pProxyEntry = NULL;

    pProxyEntry = SNMPGetProxyEntryFromName (pSnmpProxyName);
    if (pProxyEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pProxyEntry->i4Storage = i4SetValSnmpProxyStorageType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpProxyRowStatus
 Input       :  The Indices
                SnmpProxyName

                The Object 
                setValSnmpProxyRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpProxyRowStatus (tSNMP_OCTET_STRING_TYPE * pSnmpProxyName,
                          INT4 i4SetValSnmpProxyRowStatus)
{
    UINT4               u4Return = SNMP_FAILURE;
    switch (i4SetValSnmpProxyRowStatus)
    {
        case SNMP_ROWSTATUS_CREATEANDWAIT:
        {
            u4Return = (UINT4) SNMPAddProxyEntry (pSnmpProxyName);
            if (u4Return != SNMP_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        }
        case SNMP_ROWSTATUS_ACTIVE:
        {
            tProxyTableEntry   *pProxyEntry = NULL;

            pProxyEntry = SNMPGetProxyEntryFromName (pSnmpProxyName);
            if (pProxyEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            pProxyEntry->i4Status = SNMP_ROWSTATUS_ACTIVE;
            return SNMP_SUCCESS;

        }
        case SNMP_ROWSTATUS_DESTROY:
        {
            u4Return = (UINT4) SNMPDeleteProxyEntry (pSnmpProxyName);
            if (u4Return != SNMP_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        }

        case SNMP_ROWSTATUS_NOTINSERVICE:
        {
            tProxyTableEntry   *pProxyEntry = NULL;
            pProxyEntry = SNMPGetProxyEntryFromName (pSnmpProxyName);
            if (pProxyEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            if (pProxyEntry->i4Status == SNMP_ROWSTATUS_ACTIVE)
            {
                pProxyEntry->i4Status = SNMP_ROWSTATUS_NOTINSERVICE;
                return SNMP_SUCCESS;
            }
        }
        default:
            break;
    }

    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2SnmpProxyType
 Input       :  The Indices
                SnmpProxyName

                The Object 
                testValSnmpProxyType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpProxyType (UINT4 *pu4ErrorCode,
                        tSNMP_OCTET_STRING_TYPE * pSnmpProxyName,
                        INT4 i4TestValSnmpProxyType)
{
    tProxyTableEntry   *pProxyEntry = NULL;
    pProxyEntry = SNMPGetProxyEntryFromName (pSnmpProxyName);

    if (pProxyEntry != NULL)
    {
        if ((i4TestValSnmpProxyType < PROXY_TYPE_READ) ||
            (i4TestValSnmpProxyType > PROXY_TYPE_INFORM))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        if (pProxyEntry->i4Status == SNMP_ROWSTATUS_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
            return SNMP_FAILURE;
        }

        if (SNMPGetEntryCountFromPrxType (i4TestValSnmpProxyType)
            > MAX_PROXY_PDU_ENTRY)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2SnmpProxyContextEngineID
 Input       :  The Indices
                SnmpProxyName

                The Object 
                testValSnmpProxyContextEngineID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpProxyContextEngineID (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pSnmpProxyName,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValSnmpProxyContextEngineID)
{
    tProxyTableEntry   *pProxyEntry = NULL;
    pProxyEntry = SNMPGetProxyEntryFromName (pSnmpProxyName);

    if (pProxyEntry != NULL)
    {
        if ((pTestValSnmpProxyContextEngineID->i4_Length <
             MIN_PROXY_ENGINE_ID_LEN) ||
            (pTestValSnmpProxyContextEngineID->i4_Length >
             MAX_PROXY_ENGINE_ID_LEN))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
            return SNMP_FAILURE;
        }

        if (SNMPCompareOctetString (pTestValSnmpProxyContextEngineID,
                                    &gSnmpEngineID) == SNMP_EQUAL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        if (pProxyEntry->i4Status == SNMP_ROWSTATUS_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
            return SNMP_FAILURE;
        }

        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2SnmpProxyContextName
 Input       :  The Indices
                SnmpProxyName

                The Object 
                testValSnmpProxyContextName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpProxyContextName (UINT4 *pu4ErrorCode,
                               tSNMP_OCTET_STRING_TYPE *
                               pSnmpProxyName,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValSnmpProxyContextName)
{
    tProxyTableEntry   *pProxyEntry = NULL;
    pProxyEntry = SNMPGetProxyEntryFromName (pSnmpProxyName);

    if (pProxyEntry != NULL)
    {
        if ((pTestValSnmpProxyContextName->i4_Length < SNMP_ZERO) ||
            (pTestValSnmpProxyContextName->i4_Length > SNMP_MAX_CONTEXT_NAME))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
            return SNMP_FAILURE;
        }

        if (pProxyEntry->i4Status == SNMP_ROWSTATUS_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
            return SNMP_FAILURE;
        }

        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2SnmpProxyTargetParamsIn
 Input       :  The Indices
                SnmpProxyName

                The Object 
                testValSnmpProxyTargetParamsIn
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpProxyTargetParamsIn (UINT4 *pu4ErrorCode,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pSnmpProxyName,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValSnmpProxyTargetParamsIn)
{
    tProxyTableEntry   *pProxyEntry = NULL;
    pProxyEntry = SNMPGetProxyEntryFromName (pSnmpProxyName);

    if (pProxyEntry != NULL)
    {
        if ((pTestValSnmpProxyTargetParamsIn->i4_Length <= SNMP_ZERO) ||
            (pTestValSnmpProxyTargetParamsIn->i4_Length >
             SNMP_MAX_TGT_PARAM_LEN))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
            return SNMP_FAILURE;
        }

        if (pProxyEntry->i4Status == SNMP_ROWSTATUS_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
            return SNMP_FAILURE;
        }

        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2SnmpProxySingleTargetOut
 Input       :  The Indices
                SnmpProxyName

                The Object 
                testValSnmpProxySingleTargetOut
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpProxySingleTargetOut (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pSnmpProxyName,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValSnmpProxySingleTargetOut)
{
    tProxyTableEntry   *pProxyEntry = NULL;
    pProxyEntry = SNMPGetProxyEntryFromName (pSnmpProxyName);

    if (pProxyEntry != NULL)
    {
        if ((pTestValSnmpProxySingleTargetOut->i4_Length < SNMP_ZERO) ||
            (pTestValSnmpProxySingleTargetOut->i4_Length >
             SNMP_MAX_OCTETSTRING_SIZE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
            return SNMP_FAILURE;
        }

        if (pProxyEntry->i4Status == SNMP_ROWSTATUS_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
            return SNMP_FAILURE;
        }

        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2SnmpProxyMultipleTargetOut
 Input       :  The Indices
                SnmpProxyName

                The Object 
                testValSnmpProxyMultipleTargetOut
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpProxyMultipleTargetOut (UINT4 *pu4ErrorCode,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pSnmpProxyName,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pTestValSnmpProxyMultipleTargetOut)
{
    tProxyTableEntry   *pProxyEntry = NULL;

    pProxyEntry = SNMPGetProxyEntryFromName (pSnmpProxyName);

    if (pProxyEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pProxyEntry->i4Status == SNMP_ROWSTATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
        return SNMP_FAILURE;
    }

    if ((pTestValSnmpProxyMultipleTargetOut->i4_Length < SNMP_ZERO) ||
        (pTestValSnmpProxyMultipleTargetOut->i4_Length > MAX_STR_OCTET_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpProxyStorageType
 Input       :  The Indices
                SnmpProxyName

                The Object 
                testValSnmpProxyStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpProxyStorageType (UINT4 *pu4ErrorCode,
                               tSNMP_OCTET_STRING_TYPE * pSnmpProxyName,
                               INT4 i4TestValSnmpProxyStorageType)
{
    if (nmhValidateIndexInstanceSnmpProxyTable (pSnmpProxyName) == SNMP_SUCCESS)
    {
        if ((i4TestValSnmpProxyStorageType < SNMP3_STORAGE_TYPE_OTHER) ||
            (i4TestValSnmpProxyStorageType > SNMP3_STORAGE_TYPE_READONLY))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        if ((i4TestValSnmpProxyStorageType != SNMP3_STORAGE_TYPE_VOLATILE)
            && (i4TestValSnmpProxyStorageType !=
                SNMP3_STORAGE_TYPE_NONVOLATILE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;

    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2SnmpProxyRowStatus
 Input       :  The Indices
                SnmpProxyName

                The Object 
                testValSnmpProxyRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpProxyRowStatus (UINT4 *pu4ErrorCode,
                             tSNMP_OCTET_STRING_TYPE *
                             pSnmpProxyName, INT4 i4TestValSnmpProxyRowStatus)
{
    tProxyTableEntry   *pProxyEntry = NULL;
    pProxyEntry = SNMPGetProxyEntryFromName (pSnmpProxyName);

    switch (i4TestValSnmpProxyRowStatus)
    {
        case SNMP_ROWSTATUS_CREATEANDWAIT:
            if (pProxyEntry != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            break;
        case SNMP_ROWSTATUS_ACTIVE:
            if (pProxyEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }

            if ((pProxyEntry->PrxContextEngineID.i4_Length == SNMP_ZERO) ||
                (pProxyEntry->PrxTargetParamsIn.i4_Length == SNMP_ZERO) ||
                (pProxyEntry->ProxyType == SNMP_ZERO))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if (((pProxyEntry->ProxyType == PROXY_TYPE_READ) ||
                 (pProxyEntry->ProxyType == PROXY_TYPE_WRITE)) &&
                (!pProxyEntry->PrxSingleTargetOut.i4_Length))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if (((pProxyEntry->ProxyType == PROXY_TYPE_TRAP) ||
                 (pProxyEntry->ProxyType == PROXY_TYPE_INFORM)) &&
                (!pProxyEntry->PrxMultipleTargetOut.i4_Length))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            break;
        case SNMP_ROWSTATUS_NOTINSERVICE:
            if (pProxyEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            if (pProxyEntry->i4Status != SNMP_ROWSTATUS_ACTIVE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
        case SNMP_ROWSTATUS_DESTROY:
            if (pProxyEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            break;
        case SNMP_ROWSTATUS_CREATEANDGO:
            /* CreateAndGo can never come directly because CreateAndGo
               will be broken into CreateAndWait and then ACTIVE,
               so if CreateAndGo comes to us then we will return failure */
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2SnmpProxyTable
 Input       :  The Indices
                SnmpProxyName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SnmpProxyTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
