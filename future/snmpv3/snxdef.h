/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: snxdef.h,v 1.4 2015/04/28 12:35:03 siva Exp $
*
* Description: Macro definition used by SNMP Agentx module
*********************************************************************/
#ifndef _SNXDEF_H

#define SNX_REG_FLAG                         1
#define SNX_UNREG_FLAG                       2

#define SNX_ADDCAP_FLAG                      1
#define SNX_REMCAP_FLAG                      2

#define SNX_ALLOC_FLAG                       1
#define SNX_DEALLOC_FLAG                     2

#define SNX_MAX_MTU                          1500
#define SNX_DEFAULT_MASTER_PORT               705

/* Macros for using buddy memory*/
#define SNX_MAX_VAR_BLOCK_SIZE               SNX_MAX_MTU 
#define SNX_MIN_VAR_BLOCK_SIZE               4 
#define SNX_MAX_VAR_BLOCKS                   300

#define  SNX_CONNECT_SUCCESS      0x01
#define  CONNECT_INPROGRESS   0x02
#define  CONNECT_FAIL         0x03

/* AgentX Header Informations*/
#define SNX_AGENTX_VERSION                   1
#define SNX_AGENTX_PDU_HDR_LEN               20
#define SNX_NON_DEFAULT_CTXT                 0x08 
#define SNX_NETWORK_BYTE_ORDER               0x10 


#define SNX_SUBAGT_DESC             "Aricent Agentx SubAgent." 

/* Timer related Macros */
#define SNX_OPEN_TIMER                       1
#define SNX_PING_TIMER                       2

#define SNX_OPEN_TIMEOUT                     5
#define SNX_PING_TIMEOUT                     5
#define SNX_MAX_OUTSTAND_PING                3 /* Max No of ping for which we should try to
                                                  till get the response */

/* Various Agentx Messages */
#define SNX_OPEN_PDU                         1
#define SNX_CLOSE_PDU                        2
#define SNX_REGISTER_PDU                     3
#define SNX_UNREGISTER_PDU                   4
#define SNX_GET_PDU                          5
#define SNX_GETNEXT_PDU                      6
#define SNX_GETBULK_PDU                      7
#define SNX_TESTSET_PDU                      8
#define SNX_COMMITSET_PDU                    9
#define SNX_UNDOSET_PDU                     10
#define SNX_CLEANUP_PDU                     11
#define SNX_NOTIFY_PDU                      12
#define SNX_PING_PDU                        13
#define SNX_INDEX_ALLOC_PDU                 14
#define SNX_INDEX_DEALLOC_PDU               15
#define SNX_ADD_AGENT_CAPS_PDU              16
#define SNX_REMOVE_AGENT_CAPS_PDU           17
#define SNX_RESPONSE_PDU                    18

/* Close PDU Error.Sending entity may be shutting down */
#define SNX_REASON_SHUTDOWN                 5

/* Time in seconds, that master agent should allow to elapse after
 * dispatching  a message to sub-agent and regards sub-agent as 
 * not responding */
#define SNX_MASTER_DISPATCH_TIMEOUT          1

/* Default registration priority to be used by sub agent */
#define SNX_DEF_REG_PRIORITY                 127

/* Agentx Error status Values */
#define SNX_NO_ERR                           0
#define SNX_OPEN_FAIL_ERR                    256
#define SNX_NOT_OPEN_ERR                     257
#define SNX_INDEX_WRONG_TYPE_ERR             258
#define SNX_INDEX_ALREADY_ALLOCATED_ERR      259
#define SNX_INDEX_NONE_AVAILABLE_ERR         260
#define SNX_INDEX_NOT_ALLOCATED_ERR          261
#define SNX_UNSUPPORTED_CONTEXT_ERR          262
#define SNX_DUP_REGISTER_ERR                 263
#define SNX_UNKNOWN_REGISTER_ERR             264
#define SNX_UNKNOWN_AGENT_CAPS_ERR           265
#define SNX_PARSE_ERR                        266
#define SNX_REQ_DENIED                       267
#define SNX_PROCESS_ERR                      268

#define SNX_MUL_REQ_PKT                      269  /* Multiple Request are Received 
                                                     with in a Single Packet */

#define SNX_PDU_GEN_INFO_SIZE                4
#define SNX_OID_INFO_SIZE                    4

#define SNX_4BYTE_DATALEN                    4
#define SNX_2BYTE_DATALEN                    2
#define SNX_RESERVE_3BYTE_DATALEN            3
#define SNX_INET_PREFIX_LEN                  5

#define MAX_SNMP_AGTX_CTXT_NAME_LEN          32

/* Change the SNX_PROP_MIBOID_LEN based on SNX_PROP_MIBOID */
#define SNX_PROP_MIBID_1                      { 1, 3, 6, 1, 4, 1, 2076 }
#define SNX_PROP_MIBID_1_LEN                  7

#define SNX_PROP_MIBID_2                      { 1, 3, 6, 1, 4, 1, 29601, 2}
#define SNX_PROP_MIBID_2_LEN                  8

/* SNMPv2 OIDs */
#define SNX_SNMPV2_MIB_ID                        { 1, 3, 6, 1, 6 }
#define SNX_SNMPV2_MIB_ID_LEN                    5

#define SNX_GET_1BYTE(pu1Buffer, u4OffSet, u1Val)\
        { \
            u1Val = *(pu1Buffer + (u4OffSet));\
            (u4OffSet)++; \
        }
#define SNX_CPY_INET_PREFIX(pu4OidVal,u1Prefix)\
        {\
            pu4OidVal[0] = 1;\
            pu4OidVal[1] = 3;\
            pu4OidVal[2] = 6;\
            pu4OidVal[3] = 1;\
            pu4OidVal[4] = u1Prefix;\
        }

#define SNX_CMP_INET_PREFIX(pu4OidVal,u1Prefix)\
        {\
            if((pu4OidVal[0] == 1) && (pu4OidVal[1] == 3) && (pu4OidVal[2] == 6) &&(pu4OidVal[3] == 1))\
            {\
              u1Prefix = (UINT1) pu4OidVal[4];\
            }\
            else{\
                u1Prefix = 0;\
            }\
        }
#define SNX_PUT_1BYTE(u1Val, u4OffSet, pu1Buffer) \
        { \
            *(pu1Buffer + (u4OffSet)) = u1Val;\
            (u4OffSet)++; \
        }

#endif
