# include  "lr.h"
# include  "fssnmp.h"
# include  "snmproxlw.h"
# include  "snmproxwr.h"
# include  "snmproxdb.h"

INT4
GetNextIndexSnmpProxyTable (tSnmpIndex * pFirstMultiIndex,
                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexSnmpProxyTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexSnmpProxyTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

VOID
RegisterSNMPROX ()
{
    SNMPRegisterMib (&snmproxOID, &snmproxEntry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&snmproxOID, (const UINT1 *) "stdsnproxy");
}

VOID
UnRegisterSNMPROX ()
{
    SNMPUnRegisterMib (&snmproxOID, &snmproxEntry);
    SNMPDelSysorEntry (&snmproxOID, (const UINT1 *) "stdsnproxy");
}

INT4
SnmpProxyTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSnmpProxyTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSnmpProxyType (pMultiIndex->pIndex[0].pOctetStrValue,
                                 &(pMultiData->i4_SLongValue)));

}

INT4
SnmpProxyContextEngineIDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSnmpProxyTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSnmpProxyContextEngineID
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
SnmpProxyContextNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSnmpProxyTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSnmpProxyContextName (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->pOctetStrValue));

}

INT4
SnmpProxyTargetParamsInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSnmpProxyTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSnmpProxyTargetParamsIn
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
SnmpProxySingleTargetOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSnmpProxyTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSnmpProxySingleTargetOut
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
SnmpProxyMultipleTargetOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSnmpProxyTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSnmpProxyMultipleTargetOut
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
SnmpProxyStorageTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSnmpProxyTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSnmpProxyStorageType (pMultiIndex->pIndex[0].pOctetStrValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
SnmpProxyRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSnmpProxyTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSnmpProxyRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
SnmpProxyTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetSnmpProxyType (pMultiIndex->pIndex[0].pOctetStrValue,
                                 pMultiData->i4_SLongValue));

}

INT4
SnmpProxyContextEngineIDSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetSnmpProxyContextEngineID
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
SnmpProxyContextNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetSnmpProxyContextName (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->pOctetStrValue));

}

INT4
SnmpProxyTargetParamsInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetSnmpProxyTargetParamsIn
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
SnmpProxySingleTargetOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetSnmpProxySingleTargetOut
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
SnmpProxyMultipleTargetOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetSnmpProxyMultipleTargetOut
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
SnmpProxyStorageTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetSnmpProxyStorageType (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
SnmpProxyRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetSnmpProxyRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiData->i4_SLongValue));

}

INT4
SnmpProxyTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    return (nmhTestv2SnmpProxyType (pu4Error,
                                    pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiData->i4_SLongValue));

}

INT4
SnmpProxyContextEngineIDTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2SnmpProxyContextEngineID (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiData->pOctetStrValue));

}

INT4
SnmpProxyContextNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2SnmpProxyContextName (pu4Error,
                                           pMultiIndex->pIndex[0].
                                           pOctetStrValue,
                                           pMultiData->pOctetStrValue));

}

INT4
SnmpProxyTargetParamsInTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2SnmpProxyTargetParamsIn (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              pOctetStrValue,
                                              pMultiData->pOctetStrValue));

}

INT4
SnmpProxySingleTargetOutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2SnmpProxySingleTargetOut (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiData->pOctetStrValue));

}

INT4
SnmpProxyMultipleTargetOutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2SnmpProxyMultipleTargetOut (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 pOctetStrValue,
                                                 pMultiData->pOctetStrValue));

}

INT4
SnmpProxyStorageTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2SnmpProxyStorageType (pu4Error,
                                           pMultiIndex->pIndex[0].
                                           pOctetStrValue,
                                           pMultiData->i4_SLongValue));

}

INT4
SnmpProxyRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2SnmpProxyRowStatus (pu4Error,
                                         pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiData->i4_SLongValue));

}

INT4
SnmpProxyTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SnmpProxyTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
