/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: snmpnowr.c,v 1.5 2015/04/28 12:35:02 siva Exp $
*
* Description: snmp notify table wrapper Routines
*********************************************************************/

# include  "lr.h"
# include  "snmpcmn.h"
# include  "snmpnolw.h"
# include  "snmpnowr.h"
# include  "snmpnodb.h"

INT4
GetNextIndexSnmpNotifyTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexSnmpNotifyTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexSnmpNotifyTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

VOID
RegisterSNMPNO ()
{
    SNMPRegisterMib (&snmpnoOID, &snmpnoEntry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&snmpnoOID, (const UINT1 *) "snmpnotify");
}

VOID
UnRegisterSNMPNO ()
{
    SNMPUnRegisterMib (&snmpnoOID, &snmpnoEntry);
    SNMPDelSysorEntry (&snmpnoOID, (const UINT1 *) "snmpnotify");
}

INT4
SnmpNotifyNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSnmpNotifyTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[0].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[0].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[0].pOctetStrValue->i4_Length;

    return SNMP_SUCCESS;

}

INT4
SnmpNotifyTagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSnmpNotifyTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSnmpNotifyTag (pMultiIndex->pIndex[0].pOctetStrValue,
                                 pMultiData->pOctetStrValue));

}

INT4
SnmpNotifyTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSnmpNotifyTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSnmpNotifyType (pMultiIndex->pIndex[0].pOctetStrValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4
SnmpNotifyStorageTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSnmpNotifyTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSnmpNotifyStorageType (pMultiIndex->pIndex[0].pOctetStrValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
SnmpNotifyRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSnmpNotifyTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSnmpNotifyRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
SnmpNotifyTagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetSnmpNotifyTag (pMultiIndex->pIndex[0].pOctetStrValue,
                                 pMultiData->pOctetStrValue));

}

INT4
SnmpNotifyTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetSnmpNotifyType (pMultiIndex->pIndex[0].pOctetStrValue,
                                  pMultiData->i4_SLongValue));

}

INT4
SnmpNotifyStorageTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetSnmpNotifyStorageType (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiData->i4_SLongValue));

}

INT4
SnmpNotifyRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetSnmpNotifyRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiData->i4_SLongValue));

}

INT4
SnmpNotifyTagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    return (nmhTestv2SnmpNotifyTag (pu4Error,
                                    pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiData->pOctetStrValue));

}

INT4
SnmpNotifyTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2SnmpNotifyType (pu4Error,
                                     pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiData->i4_SLongValue));

}

INT4
SnmpNotifyStorageTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2SnmpNotifyStorageType (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            pOctetStrValue,
                                            pMultiData->i4_SLongValue));

}

INT4
SnmpNotifyRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2SnmpNotifyRowStatus (pu4Error,
                                          pMultiIndex->pIndex[0].
                                          pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
SnmpNotifyTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SnmpNotifyTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexSnmpNotifyFilterProfileTable (tSnmpIndex * pFirstMultiIndex,
                                          tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexSnmpNotifyFilterProfileTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexSnmpNotifyFilterProfileTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
SnmpNotifyFilterProfileNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSnmpNotifyFilterProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSnmpNotifyFilterProfileName
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
SnmpNotifyFilterProfileStorTypeGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSnmpNotifyFilterProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSnmpNotifyFilterProfileStorType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
SnmpNotifyFilterProfileRowStatusGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSnmpNotifyFilterProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSnmpNotifyFilterProfileRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
SnmpNotifyFilterProfileNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetSnmpNotifyFilterProfileName
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
SnmpNotifyFilterProfileStorTypeSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetSnmpNotifyFilterProfileStorType
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
SnmpNotifyFilterProfileRowStatusSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetSnmpNotifyFilterProfileRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
SnmpNotifyFilterProfileNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2SnmpNotifyFilterProfileName (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  pOctetStrValue,
                                                  pMultiData->pOctetStrValue));

}

INT4
SnmpNotifyFilterProfileStorTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2SnmpNotifyFilterProfileStorType (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      pOctetStrValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
SnmpNotifyFilterProfileRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2SnmpNotifyFilterProfileRowStatus (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       pOctetStrValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
SnmpNotifyFilterProfileTableDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SnmpNotifyFilterProfileTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexSnmpNotifyFilterTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexSnmpNotifyFilterTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOidValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexSnmpNotifyFilterTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].pOidValue,
             pNextMultiIndex->pIndex[1].pOidValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
SnmpNotifyFilterSubtreeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSnmpNotifyFilterTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOidValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOidValue->pu4_OidList,
            pMultiIndex->pIndex[1].pOidValue->pu4_OidList,
            (pMultiIndex->pIndex[1].pOidValue->u4_Length + sizeof (UINT4)));
    pMultiData->pOidValue->u4_Length =
        pMultiIndex->pIndex[1].pOidValue->u4_Length;

    return SNMP_SUCCESS;

}

INT4
SnmpNotifyFilterMaskGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSnmpNotifyFilterTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOidValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSnmpNotifyFilterMask (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiIndex->pIndex[1].pOidValue,
                                        pMultiData->pOctetStrValue));

}

INT4
SnmpNotifyFilterTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSnmpNotifyFilterTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOidValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSnmpNotifyFilterType (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiIndex->pIndex[1].pOidValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
SnmpNotifyFilterStorageTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSnmpNotifyFilterTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOidValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSnmpNotifyFilterStorageType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].pOidValue, &(pMultiData->i4_SLongValue)));

}

INT4
SnmpNotifyFilterRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceSnmpNotifyFilterTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOidValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSnmpNotifyFilterRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].pOidValue, &(pMultiData->i4_SLongValue)));

}

INT4
SnmpNotifyFilterMaskSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetSnmpNotifyFilterMask (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiIndex->pIndex[1].pOidValue,
                                        pMultiData->pOctetStrValue));

}

INT4
SnmpNotifyFilterTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetSnmpNotifyFilterType (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiIndex->pIndex[1].pOidValue,
                                        pMultiData->i4_SLongValue));

}

INT4
SnmpNotifyFilterStorageTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetSnmpNotifyFilterStorageType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].pOidValue, pMultiData->i4_SLongValue));

}

INT4
SnmpNotifyFilterRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetSnmpNotifyFilterRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].pOidValue, pMultiData->i4_SLongValue));

}

INT4
SnmpNotifyFilterMaskTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2SnmpNotifyFilterMask (pu4Error,
                                           pMultiIndex->pIndex[0].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[1].pOidValue,
                                           pMultiData->pOctetStrValue));

}

INT4
SnmpNotifyFilterTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2SnmpNotifyFilterType (pu4Error,
                                           pMultiIndex->pIndex[0].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[1].pOidValue,
                                           pMultiData->i4_SLongValue));

}

INT4
SnmpNotifyFilterStorageTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2SnmpNotifyFilterStorageType (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  pOctetStrValue,
                                                  pMultiIndex->pIndex[1].
                                                  pOidValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
SnmpNotifyFilterRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2SnmpNotifyFilterRowStatus (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                pOctetStrValue,
                                                pMultiIndex->pIndex[1].
                                                pOidValue,
                                                pMultiData->i4_SLongValue));

}

INT4
SnmpNotifyFilterTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SnmpNotifyFilterTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
