
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: snmagent.c,v 1.17 2016/03/18 13:04:28 siva Exp $
 *
 * Description: Routines for snmp agent main module
 *******************************************************************/
#include "snmpcmn.h"
#include "snmagent.h"
#include "iss.h"
#include <time.h>
#include "fssyslog.h"

BOOL1               b1IsRollbackEnabled = SNMP_ROLLBACK_ENABLED;
BOOL1               b1IsInitCalled = SNMP_FALSE;

INT4
issSnmpInitParams (BOOL1 initParams)
{
    if (initParams != SNMP_ROLLBACK_ENABLED &&
        initParams != SNMP_ROLLBACK_DISABLED)
    {
        SNMPTrace ("Rollback flag not correct");
        return SNMP_FAILURE;

    }

    if (b1IsInitCalled == SNMP_FALSE)
    {
        b1IsRollbackEnabled = initParams;
        b1IsInitCalled = SNMP_TRUE;
    }
    else
    {
        SNMPTrace ("Rollback flag already set");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : SNMPPDUProcess 
 *  Description     : Function to process the decoded Pdu
 *  Input           : pPdu - Protocol Data Unit pointer
 *  Output          : None
 *  Returns         : SNMP_SUCCESS or SNMP_FAILURE
 ************************************************************************/
INT4
SNMPPDUProcess (tSNMP_NORMAL_PDU * pPdu, tSnmpVacmInfo * pVacmInfo)
{
    INT4                i4Result = SNMP_SUCCESS;
    UINT1              *pu1Oid = NULL;
    CHR1                au1SnmpBufMsg[SNMP_MSG_BUF_LEN];
    tUtlInAddr          InAddr;
    tUtlIn6Addr         In6Addr;
    tMgrIpAddr          ManagerIpAddr;

    MEMSET (au1SnmpBufMsg, 0, SNMP_MSG_BUF_LEN);
    MEMSET (&InAddr, 0, sizeof (tUtlInAddr));
    MEMSET (&In6Addr, 0, sizeof (tUtlIn6Addr));
    MEMSET (&ManagerIpAddr, 0, sizeof (tMgrIpAddr));

    if (pPdu->pVarBindList == NULL)
    {
        /*Not to update the error status if NULL varbind is received */
        return SNMP_SUCCESS;
    }
    if ((gEntOid1->pu4_OidList[0] != ISS_ENT_OID_1) ||
        (gEntOid2->pu4_OidList[0] != ISS_ENT_OID_2))
    {
        if ((pPdu->pVarBindList->pObjName->pu4_OidList[EOID_OFFSET] ==
             ISS_ENT_OID_1)
            || (pPdu->pVarBindList->pObjName->pu4_OidList[EOID_OFFSET] ==
                ISS_ENT_OID_2))
        {
            pPdu->i4_ErrorStatus = SNMP_ERR_NO_ACCESS;
            return SNMP_FAILURE;
        }
    }
    switch (pPdu->i2_PduType)
    {
        case SNMP_PDU_TYPE_GET_REQUEST:
            SNMP_INR_IN_GET_SUCCESS;
            i4Result = SNMPGetProcess (pPdu, pVacmInfo);
            break;
        case SNMP_PDU_TYPE_GET_NEXT_REQUEST:
            SNMP_INR_IN_GETNEXT_SUCCESS;
            i4Result = SNMPGetNextProcess (pPdu, pVacmInfo);
            break;
/* Commenting out as this type clashes with INFORM_RESPONSE 
 * also we will be generating response for a GET
 */
/*
        case SNMP_PDU_TYPE_GET_RESPONSE:
            SNMP_INR_BAD_TYPE;
            i4Result = SNMP_FAILURE;
            break;
*/
        case SNMP_PDU_TYPE_SET_REQUEST:
            SNMP_INR_IN_SET_SUCCESS;
            i4Result = SNMPSetProcess (pPdu, pVacmInfo);
            break;
        case SNMP_PDU_TYPE_GET_BULK_REQUEST:
            SNMP_INR_IN_GETNEXT_SUCCESS;
            i4Result = SNMPGetBulkProcess (pPdu, pVacmInfo);
            if (i4Result == SNMP_SUCCESS)
            {
                if ((pPdu->i4_NonRepeaters == SNMP_ZERO) &&
                    (pPdu->i4_MaxRepetitions == SNMP_ZERO))
                {
                    break;
                }
                pu1Oid = MemAllocMemBlk (gSnmpMultiOidPoolId);
                if (pu1Oid == NULL)
                {
                    SNMPTrace ("SNMP: Unable to allocate memory for Oids\n");
                    break;
                }

                /* Gathering Manager IP address */
                SNMPGetRemoteManagerIpAddr (&ManagerIpAddr);
                if (ManagerIpAddr.u4AddrType == IPVX_ADDR_FMLY_IPV4)
                {
                    MEMCPY (&InAddr, &(ManagerIpAddr.uIpAddr.u4Ip4Addr),
                            sizeof (tUtlInAddr));
                    SPRINTF (au1SnmpBufMsg, "from %s", UtlInetNtoa (InAddr));
                }
                else if (ManagerIpAddr.u4AddrType == IPVX_ADDR_FMLY_IPV6)
                {
                    MEMCPY (&(In6Addr.u1addr),
                            &(ManagerIpAddr.uIpAddr.au1Ip6Addr),
                            sizeof (tUtlIn6Addr));
                    SPRINTF (au1SnmpBufMsg, "from %s", UtlInetNtoa6 (In6Addr));
                }

                /* Generating OID */
                MEMSET (pu1Oid, 0, sizeof (tSnmpMultiOidBlock));
                SNMPGetOidString (pPdu->pVarBindList->pObjName->pu4_OidList,
                                  pPdu->pVarBindList->pObjName->u4_Length,
                                  pu1Oid);

                SYS_LOG_CMD ((SYSLOG_DEBUG_LEVEL, SYSLOG_DEF_USER_INDEX,
                              "SNMP %s BULK %s %s SUCCESS", gi1SnmpUserName,
                              pu1Oid, au1SnmpBufMsg));

                MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1Oid);
            }
            break;

            /* Added SNMP_PDU_TYPE_GET_REPORT as the net-snmp Manager sends inform response 
               with message type value 0xA8 - i.e SNMP_PDU_TYPE_GET_REPORT */
        case SNMP_PDU_TYPE_V2_INFORM_RESPONSE:
        case SNMP_PDU_TYPE_GET_REPORT:
            SNMP_INR_INFORM_RESPS;
            SNMP_INFORM_LOCK ();
            /* Always this function should return SNMP_FAILURE */
            i4Result = SNMPInformResponseProcess (pPdu);
            SNMP_INFORM_UNLOCK ();
            break;
        default:
            SNMP_INR_BAD_TYPE;
            i4Result = SNMP_FAILURE;
    }
    return i4Result;
}

/************************************************************************
 *  Function Name   : SNMPGetProcess 
 *  Description     : Function to process request type of GET
 *  Input           : pPdu - Protocol Data Unit
 *  Output          : None
 *  Returns         : SNMP_SUCCESS or SNMP_FAILURE
 ************************************************************************/
INT4
SNMPGetProcess (tSNMP_NORMAL_PDU * pPdu, tSnmpVacmInfo * pVacmInfo)
{
    tSNMP_VAR_BIND     *pVarPtr = pPdu->pVarBindList;
    INT4                i4Result = SNMP_FAILURE;
    INT4                i4Err = SNMP_ERR_NO_ERROR;
    INT4                i4Index = 0;
    UINT4               u4VcNum = 0;
    tSnmpIndex         *pIndex = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    UINT1              *pu1Oid = NULL;
    UINT1              *pu1Data = NULL;
    CHR1                au1SnmpBufMsg[SNMP_MSG_BUF_LEN];
    tUtlInAddr          InAddr;
    tUtlIn6Addr         In6Addr;
    tMgrIpAddr          ManagerIpAddr;

    MEMSET (au1SnmpBufMsg, 0, SNMP_MSG_BUF_LEN);
    MEMSET (&InAddr, 0, sizeof (tUtlInAddr));
    MEMSET (&In6Addr, 0, sizeof (tUtlIn6Addr));
    MEMSET (&ManagerIpAddr, 0, sizeof (tMgrIpAddr));

    pIndex = SNMPGetFirstIndexPool ();

    pOid = alloc_oid (MAX_OID_LENGTH);

    if (pOid == NULL)
    {
        pPdu->i4_ErrorStatus = SNMP_ERR_NO_ERROR;
        pPdu->i4_ErrorIndex = 0;
        SNMPTrace ("SNMP: Unable to allocate memory for Oids\n");
        return SNMP_FAILURE;
    }

    pu1Oid = MemAllocMemBlk (gSnmpMultiOidPoolId);
    if (pu1Oid == NULL)
    {
        pPdu->i4_ErrorStatus = SNMP_ERR_NO_ERROR;
        pPdu->i4_ErrorIndex = 0;
        free_oid (pOid);
        SNMPTrace ("SNMP: Unable to allocate memory for Oid\n");
        return SNMP_FAILURE;
    }

    pu1Data = MemAllocMemBlk (gSnmpDataPoolId);
    if (pu1Data == NULL)
    {
        pPdu->i4_ErrorStatus = SNMP_ERR_NO_ERROR;
        pPdu->i4_ErrorIndex = 0;
        free_oid (pOid);
        MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1Oid);
        SNMPTrace ("SNMP: Unable to allocate memory for Data\n");
        return SNMP_FAILURE;
    }

    MEMSET (pu1Oid, 0, sizeof (tSnmpMultiOidBlock));
    MEMSET (pu1Data, 0, MAX_SNMP_DATA_LENGTH);

    while (pVarPtr != NULL)
    {
        if (pVarPtr->pObjName == NULL)
        {
            free_oid (pOid);
            MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1Oid);
            MemReleaseMemBlock (gSnmpDataPoolId, pu1Data);
            return SNMP_FAILURE;
        }

        SNMPUpdateEOID (pVarPtr->pObjName, pOid, FALSE);
        if (pOid->u4_Length > SNMP_MAX_OID_LENGTH)
        {
            SNMP_INR_BAD_ASN_PARSE;
            free_oid (pOid);
            MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1Oid);
            MemReleaseMemBlock (gSnmpDataPoolId, pu1Data);
            return SNMP_FAILURE;
        }

        /* Gathering Manager IP address */
        SNMPGetRemoteManagerIpAddr (&ManagerIpAddr);
        if (ManagerIpAddr.u4AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            MEMCPY (&InAddr, &(ManagerIpAddr.uIpAddr.u4Ip4Addr),
                    sizeof (tUtlInAddr));
            SPRINTF (au1SnmpBufMsg, "from %s", UtlInetNtoa (InAddr));
        }
        else if (ManagerIpAddr.u4AddrType == IPVX_ADDR_FMLY_IPV6)
        {
            MEMCPY (&(In6Addr.u1addr), &(ManagerIpAddr.uIpAddr.au1Ip6Addr),
                    sizeof (tUtlIn6Addr));
            SPRINTF (au1SnmpBufMsg, "from %s", UtlInetNtoa6 (In6Addr));
        }

        i4Index++;
        i4Result = SNMPVACMIsAccessAllowed (pVacmInfo, SNMP_READONLY,
                                            pVarPtr->pObjName,
                                            (UINT4 *) &i4Err);
        if (i4Result == SNMP_SUCCESS)
        {
            VACMGetContextNum (pVacmInfo->pContextName, &u4VcNum);
            i4Result = SNMPGet (*pOid, &pVarPtr->ObjValue,
                                (UINT4 *) &i4Err, pIndex, u4VcNum);

            /* Generating OID and Data for syslog message */
            SNMPGetOidString (pOid->pu4_OidList, pOid->u4_Length, pu1Oid);
            if(pOid == NULL)
            {
                free_oid (pOid);
                MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1Oid);
                MemReleaseMemBlock (gSnmpDataPoolId, pu1Data);
                return SNMP_FAILURE;
            }
            SnmpRevertEoidString (pu1Oid);
            SNMPConvertDataToString (&(pVarPtr->ObjValue), pu1Data,
                                     (UINT2) pVarPtr->ObjValue.i2_DataType);
            if (i4Err == SNMP_ERR_NO_ERROR)
            {
                SYS_LOG_CMD ((SYSLOG_DEBUG_LEVEL, SYSLOG_DEF_USER_INDEX,
                              "SNMP %s GET %s %s SUCCESS", gi1SnmpUserName,
                              pu1Oid, au1SnmpBufMsg));
            }
        }
        else
        {
            if (i4Err == SNMP_NO_SUCH_VIEW ||
                i4Err == SNMP_ERR_NO_ACCESS_ENTRY ||
                i4Err == SNMP_ERR_NO_GROUP_NAME)
            {
                if (pPdu->u4_Version == VERSION2)
                {
                    pPdu->i4_ErrorStatus = SNMP_ERR_AUTHORIZATION_ERROR;
                }
                else
                {
                    SNMP_INR_IN_NO_SUCH_NAME;
                    pPdu->i4_ErrorStatus = SNMP_ERR_NO_SUCH_NAME;
                }
                /* RFC 3584 section 4.4
                 * Whenever the SNMPv2 error-status value of authorizationError is
                 * translated to an SNMPv1 error-status value of noSuchName, 
                 * the value of snmpInBadCommunityUses MUST be incremented.*/
                SNMP_INR_BAD_COMM_USER;
                if (gSnmpStat.i4SnmpEnableAuthTraps == AUTHTRAP_ENABLE)
                {
                    SnmpSendAuthFailureTrap (pPdu);
                }
                pPdu->i4_ErrorIndex = SNMP_ZERO;
            }
            else if (i4Err == SNMP_ERR_NO_CONTEXT)
            {
                free_oid (pOid);
                MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1Oid);
                MemReleaseMemBlock (gSnmpDataPoolId, pu1Data);
                return SNMPDROP;
            }
            else
            {
                SNMP_INR_IN_GEN_ERROR;
                pPdu->i4_ErrorStatus = SNMP_ERR_GEN_ERR;
                pPdu->i4_ErrorIndex = i4Index;
            }
            free_oid (pOid);
            MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1Oid);
            MemReleaseMemBlock (gSnmpDataPoolId, pu1Data);
            return SNMP_FAILURE;
        }
        /*
         * To return NO_SUCH_NAME Error, when a GET request is 
         * received from a V1 Manager.
         */
        if ((pPdu->u4_Version == VERSION1) &&
            (pVarPtr->ObjValue.i2_DataType == SNMP_DATA_TYPE_COUNTER64))
        {
            pPdu->i4_ErrorStatus = SNMP_ERR_NO_SUCH_NAME;
            pPdu->i4_ErrorIndex = i4Index;
            SNMP_INR_OUT_NO_SUCH_NAME;
            free_oid (pOid);
            MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1Oid);
            MemReleaseMemBlock (gSnmpDataPoolId, pu1Data);
            return SNMP_FAILURE;
        }

        if (i4Result == SNMP_SUCCESS)
        {
            SNMP_INR_TOT_GET_SUCCESS;
        }
        if (i4Result == SNMP_FAILURE)
        {
            /* --------------------------------
             * if  Version1 the Agent returns 
             *  either NO_SUCH_OBJECT or GEN ERROR
             *  Error and Error Index is set at Pdu
             *  The processing is terminated
             *  ----------------------------------*/
            if (pPdu->u4_Version == VERSION1)
            {
                if ((i4Err != SNMP_EXCEPTION_NO_SUCH_OBJECT) &&
                    (i4Err != SNMP_EXCEPTION_NO_SUCH_INSTANCE))
                {

                    SNMP_INR_OUT_GEN_ERROR;
                    i4Err = SNMP_ERR_GEN_ERR;
                }
                else
                {
                    i4Err = SNMP_ERR_NO_SUCH_NAME;
                    SNMP_INR_OUT_NO_SUCH_NAME;

                }
                pPdu->i4_ErrorStatus = i4Err;
                pPdu->i4_ErrorIndex = i4Index;
                i4Result = SNMP_FAILURE;
                break;
            }
            else
                     /*------Assumtion V2------------
                 * If Version 2 if the Error is
                 *  NoSuchObject or NoSuchInstance
                 *  the processing can continue...
                 *  if any other Error Occurs
                 *  Error is set to Gen Error
                 *  Error index is set 
                 *  procesiing terminated
                 *  ---------------------------*/
            {
                if ((i4Err != SNMP_EXCEPTION_NO_SUCH_OBJECT)
                    && (i4Err != SNMP_EXCEPTION_NO_SUCH_INSTANCE)
                    && (i4Err != SNMP_ERR_NO_ACCESS))
                {
                    i4Err = SNMP_ERR_GEN_ERR;
                    pPdu->i4_ErrorStatus = i4Err;
                    pPdu->i4_ErrorIndex = i4Index;
                    i4Result = SNMP_FAILURE;
                    break;
                }
                else
                {
                    pVarPtr->ObjValue.i2_DataType = (INT2) i4Err;
                    i4Result = SNMP_SUCCESS;

                    SNMPGetOidString (pOid->pu4_OidList, pOid->u4_Length,
                                      pu1Oid);
                    SnmpRevertEoidString (pu1Oid);
                    SYS_LOG_CMD ((SYSLOG_DEBUG_LEVEL, SYSLOG_DEF_USER_INDEX,
                                  "SNMP %s GET %s %s FAILED. ERROR CODE: %d",
                                  gi1SnmpUserName, pu1Oid, au1SnmpBufMsg,
                                  pPdu->i4_ErrorStatus));
                }

            }
        }
        pVarPtr = pVarPtr->pNextVarBind;
    }

    free_oid (pOid);
    MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1Oid);
    MemReleaseMemBlock (gSnmpDataPoolId, pu1Data);
    if (i4Result == SNMP_SUCCESS)
    {
        pPdu->i4_ErrorStatus = SNMP_ERR_NO_ERROR;
        pPdu->i4_ErrorIndex = 0;
    }
    return i4Result;
}

/************************************************************************
 *  Function Name   : SNMPGetNextProcess 
 *  Description     : Function to process request type of GETNEXT
 *  Input           : pPdu - Protocol Data Unit
 *  Output          : None
 *  Returns         : SNMP_SUCCESS or SNMP_FAILURE
 ************************************************************************/
INT4
SNMPGetNextProcess (tSNMP_NORMAL_PDU * pPdu, tSnmpVacmInfo * pVacmInfo)
{
    tSNMP_VAR_BIND     *pVarPtr = pPdu->pVarBindList, *pNew = NULL;
    tSNMP_VAR_BIND     *pTemp = NULL;
    INT4                i4Result = SNMP_FAILURE;
    INT4                i4Err = SNMP_ERR_NO_ERROR;
    INT4                i4Index = 0;
    UINT4               u4VcNum = 0;
    UINT4               u4Match = 0;
    tSNMP_VAR_BIND     *pLocal = NULL;
    tSnmpIndex         *pCurIndex = NULL, *pNextIndex = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    UINT1              *pu1Oid = NULL;
    UINT1              *pu1NextOid = NULL;
    UINT1              *pu1Data = NULL;
    CHR1                au1SnmpBufMsg[SNMP_MSG_BUF_LEN];
    tUtlInAddr          InAddr;
    tUtlIn6Addr         In6Addr;
    tMgrIpAddr          ManagerIpAddr;

    MEMSET (au1SnmpBufMsg, 0, SNMP_MSG_BUF_LEN);
    MEMSET (&InAddr, 0, sizeof (tUtlInAddr));
    MEMSET (&In6Addr, 0, sizeof (tUtlIn6Addr));
    MEMSET (&ManagerIpAddr, 0, sizeof (tMgrIpAddr));

    pu1Oid = MemAllocMemBlk (gSnmpMultiOidPoolId);
    if (pu1Oid == NULL)
    {
        pPdu->i4_ErrorStatus = SNMP_ERR_NO_ERROR;
        pPdu->i4_ErrorIndex = 0;
        SNMPTrace ("SNMP: Unable to allocate memory for Oids\n");
        return SNMP_FAILURE;
    }

    pu1NextOid = MemAllocMemBlk (gSnmpMultiOidPoolId);
    if (pu1NextOid == NULL)
    {
        pPdu->i4_ErrorStatus = SNMP_ERR_NO_ERROR;
        pPdu->i4_ErrorIndex = 0;
        SNMPTrace ("SNMP: Unable to allocate memory for Oids\n");
        MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1Oid);
        return SNMP_FAILURE;
    }

    pu1Data = MemAllocMemBlk (gSnmpDataPoolId);
    if (pu1Data == NULL)
    {
        pPdu->i4_ErrorStatus = SNMP_ERR_NO_ERROR;
        pPdu->i4_ErrorIndex = 0;
        MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1Oid);
        MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1NextOid);
        SNMPTrace ("SNMP: Unable to allocate memory for Data\n");
        return SNMP_FAILURE;
    }

    pNextIndex = SNMPGetFirstIndexPool ();
    pCurIndex = SNMPGetSecondIndexPool ();

    pTemp = SNMPAllocVarBind ();
    pLocal = SNMPAllocVarBind ();
    if (pTemp == NULL || pLocal == NULL)
    {
        i4Err = SNMP_ERR_GEN_ERR;
        SNMP_INR_IN_GEN_ERROR;
        MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1Oid);
        MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1NextOid);
        MemReleaseMemBlock (gSnmpDataPoolId, pu1Data);
        return SNMP_FAILURE;
    }

    pOid = alloc_oid (MAX_OID_LENGTH);

    if (pOid == NULL)
    {
        pPdu->i4_ErrorStatus = SNMP_ERR_NO_ERROR;
        pPdu->i4_ErrorIndex = 0;
        SNMPTrace ("SNMP: Unable to allocate memory for Oids\n");
        MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1Oid);
        MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1NextOid);
        MemReleaseMemBlock (gSnmpDataPoolId, pu1Data);
        return SNMP_FAILURE;
    }

    if (pVarPtr != NULL)
    {
        SNMPOIDCopy (pLocal->pObjName, pVarPtr->pObjName);
    }

    while (pVarPtr != NULL)
    {
        i4Index++;
        pNew = SNMPAllocVarBind ();
        if (pNew == NULL)
        {
            i4Result = SNMP_FAILURE;
            i4Err = SNMP_ERR_GEN_ERR;
            SNMP_INR_OUT_GEN_ERROR;
        }
        else
        {
            if (pVacmInfo->pContextName->i4_Length != SNMP_ZERO)
            {
                if (VACMGetContextNum (pVacmInfo->pContextName, &u4VcNum)
                    == SNMP_FAILURE)
                {
                    i4Err = SNMP_ERR_NO_CONTEXT;
                    SNMP_INR_UNAVIAL_CONTEXTS;
                    free_oid (pOid);
                    MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1Oid);
                    MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1NextOid);
                    MemReleaseMemBlock (gSnmpDataPoolId, pu1Data);
                    return SNMP_FAILURE;
                }
            }
            if (pVarPtr->pObjName == NULL)
            {
                free_oid (pOid);
                MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1Oid);
                MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1NextOid);
                MemReleaseMemBlock (gSnmpDataPoolId, pu1Data);
                return SNMP_FAILURE;
            }		

            SNMPUpdateEOID (pVarPtr->pObjName, pOid, TRUE);
            if (pOid->u4_Length > SNMP_MAX_OID_LENGTH)
            {
                SNMP_INR_BAD_ASN_PARSE;
                i4Err = SNMP_ERR_TOO_BIG;
                free_oid (pOid);
                MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1Oid);
                MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1NextOid);
                MemReleaseMemBlock (gSnmpDataPoolId, pu1Data);
                return SNMP_FAILURE;
            }

            i4Result = SNMPGetNextOID (*pOid, pNew->pObjName,
                                       &(pVarPtr->ObjValue),
                                       (UINT4 *) &i4Err, pCurIndex,
                                       pNextIndex, u4VcNum);
        }

        if (i4Result == SNMP_SUCCESS)
        {
            /* Generating OID */
            SNMPGetOidString (pOid->pu4_OidList, pOid->u4_Length, pu1Oid);

            /* Generating Next OID */
            SNMPGetOidString (pNew->pObjName->pu4_OidList,
                              pNew->pObjName->u4_Length, pu1NextOid);

            /* Generating next data */
            SNMPConvertDataToString (&(pVarPtr->ObjValue), pu1Data,
                                     (UINT2) pVarPtr->ObjValue.i2_DataType);

            /* Gathering Manager IP address */
            SNMPGetRemoteManagerIpAddr (&ManagerIpAddr);
            if (ManagerIpAddr.u4AddrType == IPVX_ADDR_FMLY_IPV4)
            {
                MEMCPY (&InAddr, &(ManagerIpAddr.uIpAddr.u4Ip4Addr),
                        sizeof (tUtlInAddr));
                SPRINTF (au1SnmpBufMsg, "from %s", UtlInetNtoa (InAddr));
            }
            else if (ManagerIpAddr.u4AddrType == IPVX_ADDR_FMLY_IPV6)
            {
                MEMCPY (&(In6Addr.u1addr), &(ManagerIpAddr.uIpAddr.au1Ip6Addr),
                        sizeof (tUtlIn6Addr));
                SPRINTF (au1SnmpBufMsg, "from %s", UtlInetNtoa6 (In6Addr));
            }

            SYS_LOG_CMD ((SYSLOG_DEBUG_LEVEL, SYSLOG_DEF_USER_INDEX,
                          "SNMP %s GETNEXT %s: next-oid %s %s SUCCESS",
                          gi1SnmpUserName, pu1Oid, pu1NextOid, au1SnmpBufMsg));

            SNMPRevertEOID (pNew->pObjName, pVarPtr->pObjName);
            if ((i4Err == SNMP_ERR_NO_ERROR) &&
                (OIDCompare (*(pLocal->pObjName), *(pVarPtr->pObjName),
                             &u4Match) == SNMP_GREATER))
            {
                SNMPFreeVarBind (pNew);
                continue;
            }
            /*  
             * To Skip Counter64 Object, when a GETNEXT request 
             * is received from a V1 Manager. 
             */
            if ((pPdu->u4_Version == VERSION1) &&
                (pVarPtr->ObjValue.i2_DataType == SNMP_DATA_TYPE_COUNTER64))
            {
                SNMPFreeVarBind (pNew);
                continue;
            }

            if (SNMPVACMIsAccessAllowed (pVacmInfo, SNMP_READONLY,
                                         pVarPtr->pObjName, (UINT4 *) &i4Err)
                == SNMP_FAILURE)
            {
                SNMPFreeVarBind (pNew);
                if (i4Err == SNMP_ERR_NO_ACCESS)
                {
                        SNMPFreeVarBind (pLocal);
			free_oid (pOid);
			pPdu->i4_ErrorStatus = SNMP_ERR_NO_ACCESS;
			pPdu->i4_ErrorIndex = i4Index;
			MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1Oid);
			MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1NextOid);
			MemReleaseMemBlock (gSnmpDataPoolId, pu1Data);
			return SNMP_FAILURE;
		}
                continue;
            }

            SNMPOIDCopy (pTemp->pObjName, pVarPtr->pObjName);
            SNMPFreeVarBind (pNew);
            SNMP_INR_TOT_GET_SUCCESS;

        }
        else
        {
            SNMPFreeVarBind (pNew);
            if (pPdu->u4_Version == VERSION1)
            {
                if (i4Err == SNMP_EXCEPTION_END_OF_MIB_VIEW)
                {
                    i4Err = SNMP_ERR_NO_SUCH_NAME;
                    SNMP_INR_OUT_NO_SUCH_NAME;
                }
                else
                {
                    i4Err = SNMP_ERR_GEN_ERR;
                    SNMP_INR_OUT_GEN_ERROR;
                }
                pPdu->i4_ErrorStatus = i4Err;
                pPdu->i4_ErrorIndex = i4Index;
                i4Result = SNMP_FAILURE;
                break;
            }
            else
            /*------Assumtion V2----------*/
            {
                if (i4Err != SNMP_EXCEPTION_END_OF_MIB_VIEW)
                {
                    SNMP_INR_OUT_GEN_ERROR;
                    pPdu->i4_ErrorStatus = SNMP_ERR_GEN_ERR;
                    pPdu->i4_ErrorIndex = i4Index;
                    free_oid (pOid);
                    MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1Oid);
                    MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1NextOid);
                    MemReleaseMemBlock (gSnmpDataPoolId, pu1Data);
                    return SNMP_FAILURE;
                }
                else
                {
                    if ((pTemp != NULL) && (pTemp->pObjName != NULL))
                    {
                        if (pTemp->pObjName->u4_Length != 0)
                        {
                            SNMPOIDCopy (pVarPtr->pObjName, pTemp->pObjName);
                            SNMPFreeVarBind (pTemp);
                        }
                    }
                    pVarPtr->ObjValue.i2_DataType =
                        SNMP_EXCEPTION_END_OF_MIB_VIEW;
                    i4Result = SNMP_SUCCESS;
                }
            }
        }
        pVarPtr = pVarPtr->pNextVarBind;
        if (pVarPtr != NULL)
        {
            SNMPOIDCopy (pLocal->pObjName, pVarPtr->pObjName);
        }
    }
    if (i4Result == SNMP_SUCCESS)
    {
        pPdu->i4_ErrorStatus = SNMP_ERR_NO_ERROR;
        pPdu->i4_ErrorIndex = 0;
    }
    SNMPFreeVarBind (pLocal);
    free_oid (pOid);
    MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1Oid);
    MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1NextOid);
    MemReleaseMemBlock (gSnmpDataPoolId, pu1Data);
    return i4Result;
}

/************************************************************************
*  Function Name   : SNMPGetNextVarBind 
*  Description     : Function to process varbinds
*  Input           : pVarPtr - Pointer to Varbind list
*                    pu4Err - Error Number pointer
*  Output          : None
*  Returns         : SNMP_SUCCESS or SNMP_FAILURE
************************************************************************/
INT4
SNMPGetNextVarBind (tSNMP_VAR_BIND * pVarPtr, UINT4 *pu4Err,
                    tSnmpVacmInfo * pVacmInfo)
{
    tSNMP_VAR_BIND     *pNew = NULL;
    tSNMP_VAR_BIND     *pTemp = NULL;
    INT4                i4Result = SNMP_FAILURE;
    UINT4               u4VcNum = 0;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OID_TYPE     *pTempOid = NULL;
    tSnmpIndex         *pCurIndex = NULL, *pNextIndex = NULL;
    pNextIndex = SNMPGetFirstIndexPool ();
    pCurIndex = SNMPGetSecondIndexPool ();

    pTemp = SNMPAllocVarBind ();
    if (pTemp == NULL)
    {
        *pu4Err = SNMP_ALLOC_ERR;
        return SNMP_FAILURE;
    }

    pOid = alloc_oid (MAX_OID_LENGTH);

    if (pOid == NULL)
    {
        *pu4Err = SNMP_ALLOC_ERR;
        SNMPTrace ("SNMP: Unable to allocate memory for Oids\n");
        SNMPFreeVarBind (pTemp);
        return SNMP_FAILURE;
    }

    if (pVarPtr != NULL)
    {
        pNew = SNMPAllocVarBind ();
        if (pNew == NULL)
        {
            *pu4Err = SNMP_ALLOC_ERR;
            free_oid (pOid);
            SNMPFreeVarBind (pTemp);
            return SNMP_FAILURE;
        }

        if (pVarPtr->pObjName == NULL)
        {
            *pu4Err = SNMP_ERR_GEN_ERR;  		
            SNMPFreeVarBind (pTemp);
            free_oid (pOid);
            return SNMP_FAILURE;
        }		

        SNMPUpdateEOID (pVarPtr->pObjName, pOid, TRUE);
        if (pOid->u4_Length > SNMP_MAX_OID_LENGTH)
        {
            SNMP_INR_BAD_ASN_PARSE;
            *pu4Err = SNMP_ERR_TOO_BIG;
            SNMPFreeVarBind (pTemp);
            free_oid (pOid);
            return SNMP_FAILURE;
        }

        if (pVacmInfo->pContextName->i4_Length != SNMP_ZERO)
        {
            if (VACMGetContextNum (pVacmInfo->pContextName, &u4VcNum)
                == SNMP_FAILURE)
            {
                free_oid (pOid);
                *pu4Err = SNMP_ERR_NO_CONTEXT;
                SNMP_INR_UNAVIAL_CONTEXTS;
                return SNMP_FAILURE;
            }
        }
        i4Result = SNMPGetNextOID (*pOid, pNew->pObjName,
                                   &(pVarPtr->ObjValue),
                                   pu4Err, pCurIndex, pNextIndex, u4VcNum);
        if (i4Result == SNMP_SUCCESS)
        {
            SNMPRevertEOID (pNew->pObjName, pVarPtr->pObjName);
            if (SNMPVACMIsAccessAllowed (pVacmInfo, SNMP_READONLY,
                                         pVarPtr->pObjName, pu4Err)
                == SNMP_FAILURE)
            {
                SNMPFreeVarBind (pNew);
                i4Result = SNMP_FAILURE;
                *pu4Err = SNMP_NO_SUCH_VIEW;
            }
            else
            {
                SNMPOIDCopy (pTemp->pObjName, pVarPtr->pObjName);
                if (pVarPtr->ObjValue.i2_DataType == SNMP_DATA_TYPE_OBJECT_ID)
                {
                    pTempOid = alloc_oid (MAX_OID_LENGTH);
                    if (pTempOid != NULL)
                    {
                        SNMPRevertEOID (pVarPtr->ObjValue.pOidValue, pTempOid);
                        SNMPOIDCopy (pVarPtr->ObjValue.pOidValue, pTempOid);
                        free_oid (pTempOid);
                    }
                    else
                    {
                        i4Result = SNMP_FAILURE;
                        *pu4Err = SNMP_NO_SUCH_VIEW;
                    }
                }
                SNMPFreeVarBind (pNew);
            }
        }
        else
        {
            SNMPFreeVarBind (pNew);
            if (*pu4Err != SNMP_EXCEPTION_END_OF_MIB_VIEW)

            {
                *pu4Err = SNMP_ERR_GEN_ERR;
                i4Result = SNMP_FAILURE;
            }
            else
            {
                if (pTemp != NULL)
                {
                    if (pTemp->pObjName->u4_Length != 0)
                    {
                        SNMPOIDCopy (pVarPtr->pObjName, pTemp->pObjName);
                    }
                }
                pVarPtr->ObjValue.i2_DataType = SNMP_EXCEPTION_END_OF_MIB_VIEW;
                i4Result = SNMP_SUCCESS;
            }
        }
    }
    free_oid (pOid);
    SNMPFreeVarBind (pTemp);
    return i4Result;
}

/************************************************************************
 **  Function Name   : SNMPGetBindCount
 **  Description     : This will return the no of varbinds
 **  Input           : pVarPtr - VarBind
 **  Output          : no of varbind count
 **  Returns         : SNMP_SUCCESS or SNMP_FAILURE
 *************************************************************************/
INT4
SNMPGetBindCount (tSNMP_VAR_BIND * pVarPtr)
{
    tSNMP_VAR_BIND     *pTemp = pVarPtr;
    INT4                i4Count = SNMP_ZERO;
    while (pTemp != NULL)
    {
        i4Count++;
        pTemp = pTemp->pNextVarBind;
    }
    return i4Count;
}

/************************************************************************
 *  Function Name   : SNMPGetBulkProcess 
 *  Description     : Function is called when request PDU of type GETBULK 
 *  Input           : pPdu - Protocol Data Unit Pointer
 *  Output          : None
 *  Returns         : SNMP_SUCCESS or SNMP_FAILURE
 ************************************************************************/
INT4
SNMPGetBulkProcess (tSNMP_NORMAL_PDU * pPdu, tSnmpVacmInfo * pVacmInfo)
{
    tSNMP_VAR_BIND     *pVarPtr = pPdu->pVarBindList;
    tSNMP_VAR_BIND     *pStart = NULL, *pTemp = NULL, *pDel = NULL,
        *pPrev = pPdu->pVarBindList;
    INT4                i4Repeat = SNMP_ZERO, i4NonRepeat = SNMP_ZERO;
    INT4                i4VarBindCount = SNMP_ZERO, i4Index = SNMP_ZERO;
    INT4                i4Err = SNMP_ERR_NO_ERROR;
    INT4                i4Size = SNMP_ZERO;
    INT4                i4Temp = SNMP_ZERO, i4Val = SNMP_ZERO;
    UINT4               u4Match = 0;
    tSNMP_VAR_BIND     *pLocal = NULL;

    /* Drop the packet if its a GetBulkReq from a V1 Manager */
    if (pPdu->u4_Version == VERSION1)
    {
        SNMPPduFree (pPdu);
        return (SNMPDROP);
    }
    if (pPdu->i4_NonRepeaters < SNMP_ZERO)
    {
        pPdu->i4_NonRepeaters = SNMP_ZERO;
    }
    if (pPdu->i4_MaxRepetitions < SNMP_ZERO)
    {
        pPdu->i4_MaxRepetitions = SNMP_ZERO;
    }

    if (pPdu->i4_NonRepeaters > SNMP_ZERO)
    {
        i4NonRepeat = pPdu->i4_NonRepeaters;
    }
    if (pPdu->i4_MaxRepetitions > SNMP_ZERO)
    {
        i4Repeat = pPdu->i4_MaxRepetitions;
    }

    if ((pPdu->i4_NonRepeaters == SNMP_ZERO) &&
        (pPdu->i4_MaxRepetitions == SNMP_ZERO))
    {
        pPdu->i4_ErrorIndex = SNMP_ZERO;
        pPdu->i4_ErrorStatus = SNMP_ERR_NO_ERROR;
        SNMP_INR_TOT_GET_SUCCESS;
        pPdu->pVarBindList = NULL;
        return SNMP_SUCCESS;
    }

    /* Response Packet Length Estimations */
    pTemp = pVarPtr;
    pPdu->pVarBindList = NULL;
    SNMPEstimateNormalPacketLength (pPdu);
    if (pVacmInfo == NULL)
    {
        pPdu->pVarBindList = pTemp;
        return SNMP_FAILURE;
    }
    if (pVacmInfo->u4SecModel == SNMP_USM)
    {
        i4Size += SNMP_V3_HEADER_LENGTH;
    }

    pPdu->pVarBindList = pTemp;

    pPdu->i4_ErrorIndex = SNMP_ZERO;
    pPdu->i4_ErrorStatus = SNMP_ERR_NO_ERROR;
    pLocal = SNMPAllocVarBind ();
    if (pLocal == NULL)
    {
        pPdu->i4_ErrorStatus = SNMP_ERR_GEN_ERR;
        pPdu->i4_ErrorIndex = SNMP_ZERO;
        pPrev->pNextVarBind = NULL;
        SNMP_INR_OUT_GEN_ERROR;
        return SNMP_FAILURE;
    }
    if (pVarPtr != NULL)
    {
        SNMPOIDCopy (pLocal->pObjName, pVarPtr->pObjName);
    }

    /* Do Single Get Next for Non Repeaters */
    while (pVarPtr != NULL && i4NonRepeat > SNMP_ZERO)
    {
        i4Index++;
        SNMPGetNextVarBind (pVarPtr, (UINT4 *) &i4Err, pVacmInfo);
        if (i4Err == SNMP_ALLOC_ERR)
        {
            pPdu->i4_ErrorStatus = SNMP_ERR_NO_ERROR;
            pPdu->i4_ErrorIndex = SNMP_ZERO;
            pPrev->pNextVarBind = NULL;
            return SNMP_SUCCESS;
        }
        else if (i4Err == SNMP_ERR_GEN_ERR)
        {
            pPdu->i4_ErrorStatus = SNMP_ERR_GEN_ERR;
            pPdu->i4_ErrorIndex = i4Index;
            pPrev->pNextVarBind = NULL;
            SNMP_INR_OUT_GEN_ERROR;
            return SNMP_FAILURE;
        }
        else if (i4Err == SNMP_NO_SUCH_VIEW)
        {
            /* if i4NonRepeat == pPdu->i4_NonRepeaters
             * it means that some varbinds are there,
             * which should be returned to manager*/

            if (i4NonRepeat != pPdu->i4_NonRepeaters)
            {
                pPdu->i4_ErrorStatus = SNMP_ERR_NO_ERROR;
                pPdu->i4_ErrorIndex = 0;
                pPrev->pNextVarBind = NULL;
                return SNMP_SUCCESS;
            }
            else
            {
                pPdu->i4_ErrorStatus = SNMP_ERR_AUTHORIZATION_ERROR;
                pPdu->i4_ErrorIndex = SNMP_ZERO;
                pPrev->pNextVarBind = NULL;
                return SNMP_FAILURE;
            }
        }
        if ((i4Err == SNMP_ERR_NO_ERROR) &&
            (OIDCompare (*(pLocal->pObjName), *(pVarPtr->pObjName), &u4Match) ==
             SNMP_GREATER))
        {
            continue;
        }
        i4Size += SNMPEstimateVarbindLength (pVarPtr);
        if (i4Size > (MAX_PKT_LENGTH - SNMP_ENCODE_LENGTH))
        {
            SNMPTrace ("Exceeds Max Pkt Length \n");
            pPdu->i4_ErrorIndex = SNMP_ZERO;
            pPdu->i4_ErrorStatus = SNMP_ERR_NO_ERROR;
            pPdu->i4_NonRepeaters = i4Index;
            pPrev->pNextVarBind = NULL;
            return SNMP_SUCCESS;
        }
        pPrev = pVarPtr;
        pVarPtr = pVarPtr->pNextVarBind;
        i4NonRepeat--;
        if (pVarPtr != NULL)
        {
            SNMPOIDCopy (pLocal->pObjName, pVarPtr->pObjName);
        }

    }
    if (pPdu->i4_MaxRepetitions == SNMP_ZERO)
    {
        pPdu->i4_ErrorIndex = SNMP_ZERO;
        pPdu->i4_ErrorStatus = SNMP_ERR_NO_ERROR;
        pPdu->i4_NonRepeaters = i4Index;
        pPrev->pNextVarBind = NULL;
    }

    /* Multiple Get Next for Repeaters */
    i4VarBindCount = SNMPGetBindCount (pVarPtr);
    if (i4VarBindCount == 0)
    {
        return SNMP_SUCCESS;
    }
    i4Index = 0;
    while (i4Repeat > SNMP_ZERO)
    {
        i4Repeat--;
        i4Temp = i4VarBindCount;
        pStart = pTemp = SNMPAllocVarBind ();
        if (pStart == NULL)
        {
            pPdu->i4_ErrorIndex = SNMP_ZERO;
            pPdu->i4_ErrorStatus = SNMP_ERR_NO_ERROR;
            pVarPtr->pNextVarBind = NULL;
            return SNMP_SUCCESS;
        }
        if (pVarPtr != NULL)
        {
            SNMPOIDCopy (pLocal->pObjName, pVarPtr->pObjName);
        }
        while (i4Temp > 0)
        {
            i4Index++;
            SNMPGetNextVarBind (pVarPtr, (UINT4 *) &i4Err, pVacmInfo);
            if (i4Err == SNMP_ALLOC_ERR)
            {
                pPdu->i4_ErrorStatus = SNMP_ERR_NO_ERROR;
                pPdu->i4_ErrorIndex = SNMP_ZERO;
                pPrev->pNextVarBind = NULL;
                return SNMP_SUCCESS;
            }
            else if (i4Err == SNMP_ERR_GEN_ERR)
            {
                pPdu->i4_ErrorStatus = SNMP_ERR_GEN_ERR;
                pPdu->i4_ErrorIndex = i4Index;
                pPrev->pNextVarBind = NULL;
                SNMP_INR_OUT_GEN_ERROR;
                return SNMP_FAILURE;
            }
            else if (i4Err == SNMP_NO_SUCH_VIEW)
            {

                /* if i4NonRepeat == pPdu->i4_MaxRepetitions - 1 
                 * it means that some varbinds are there,
                 * which should be returned to manager */
                if (i4Repeat != pPdu->i4_MaxRepetitions - 1)
                {
                    pPdu->i4_ErrorStatus = SNMP_ERR_NO_ERROR;
                    pPdu->i4_ErrorIndex = 0;
                    pPrev->pNextVarBind = NULL;
                    return SNMP_SUCCESS;
                }
                else
                {
                    pPdu->i4_ErrorStatus = SNMP_ERR_AUTHORIZATION_ERROR;
                    pPdu->i4_ErrorIndex = SNMP_ZERO;
                    pPrev->pNextVarBind = NULL;
                    return SNMP_FAILURE;
                }
            }
            if ((i4Temp == i4VarBindCount) &&
                (i4Err == SNMP_ERR_NO_ERROR) &&
                (OIDCompare
                 (*(pLocal->pObjName), *(pVarPtr->pObjName),
                  &u4Match) == SNMP_GREATER))
            {
                i4Index--;
                continue;
            }
            i4Size += SNMPEstimateVarbindLength (pVarPtr);
            if (i4Size > (MAX_PKT_LENGTH - SNMP_ENCODE_LENGTH))
            {
                SNMPTrace ("Exceeds Max Pkt Length \n");
                pPdu->i4_ErrorIndex = SNMP_ZERO;
                pPdu->i4_ErrorStatus = SNMP_ERR_NO_ERROR;
                pPdu->i4_MaxRepetitions = i4Index;
                pPrev->pNextVarBind = NULL;
                return SNMP_SUCCESS;
            }
            pPrev = pVarPtr;
            SNMPOIDCopy (pTemp->pObjName, pVarPtr->pObjName);
            i4Temp--;
            if (i4Repeat != 0)
            {
                if (i4Temp == 0)
                {
                    if (i4Err == SNMP_EXCEPTION_END_OF_MIB_VIEW)
                    {
                        pDel = pStart;
                        for (i4Val = SNMP_ONE;
                             i4Val < (i4VarBindCount - SNMP_ONE); i4Val++)
                        {

                            pDel = pDel->pNextVarBind;
                        }
                        /* Remove the last varbind in the list at the end of the                                                                     mib view */
                        pDel->pNextVarBind = NULL;

                        i4VarBindCount--;

                        if (i4VarBindCount == 0)
                        {
                            return SNMP_SUCCESS;
                        }
                    }
                    pVarPtr->pNextVarBind = pStart;
                    pVarPtr = pStart;
                    pTemp->pNextVarBind = NULL;
                }
                else
                {
                    if (i4Err != SNMP_EXCEPTION_END_OF_MIB_VIEW)
                    {
                        pTemp->pNextVarBind = SNMPAllocVarBind ();
                        if (pTemp->pNextVarBind == NULL)
                        {
                            pPdu->i4_ErrorIndex = SNMP_ZERO;
                            pPdu->i4_ErrorStatus = SNMP_ERR_NO_ERROR;
                            pVarPtr->pNextVarBind = NULL;
                            return SNMP_SUCCESS;
                        }
                        pTemp = pTemp->pNextVarBind;
                    }
                    else
                    {
                        i4VarBindCount--;
                    }
                    pVarPtr = pVarPtr->pNextVarBind;
                }
            }
            else
            {
                pVarPtr = pVarPtr->pNextVarBind;
            }
        }
    }
    SNMP_INR_TOT_GET_SUCCESS;
    return SNMP_SUCCESS;
}

void
freeAllRBMemory (tSNMP_GET_VAR_BIND * pGetVarBind, INT4 numVarbinds)
{

    INT4                i = 0;
    if (pGetVarBind == NULL)
        return;

    for (i = 0; i < numVarbinds; i++)
    {
        if ((pGetVarBind + i) != NULL && pGetVarBind[i].pRollbackValue != NULL)
            SNMPFreeVarBind (pGetVarBind[i].pRollbackValue);
    }
    if (pGetVarBind != NULL)
        MemReleaseMemBlock (gSnmpGetVarBindPoolId, (UINT1 *) pGetVarBind);
}

/************************************************************************
 *  Function Name   : SNMPSetProcess 
 *  Description     : Function is called when request PDU of type SET
 *  Input           : pPdu - Protocol Data Unit Pointer
 *  Output          : None
 *  Returns         : SNMP_SUCCESS or SNMP_FAILURE
 ************************************************************************/
INT4
SNMPSetProcess (tSNMP_NORMAL_PDU * pPdu, tSnmpVacmInfo * pVacmInfo)
{
    tSNMP_VAR_BIND     *pVarPtr = pPdu->pVarBindList;
    tSNMP_OID_TYPE      OID;
    tSNMP_MULTI_DATA_TYPE *pVal;
    tAuditInfo         *pTempAudit = NULL;
    time_t              t;
    INT4                i4Result = SNMP_FAILURE;
    INT4                i4Err = SNMP_ERR_NO_ERROR;
    INT4                i4Index = 0;    /* Error Indexing */
    UINT4               u4VcNum = 0;
    UINT1              *pu1Oid = NULL;
    UINT1              *pu1Data = NULL;
    tSnmpIndex         *pIndex = NULL;

    /*Rollback: declaration of rollback variables */
    tSnmpIndexList      indexList;
    tSnmpDbEntry        SnmpDbEntry;
    UINT4               u4Len = 0;
    INT4                numVarbinds = 0;    /* Error Indexing */
    INT4                i4TempIndex = 0;    /* Error Indexing */
    INT4                u4Match;
    INT1                rowStatusPresent;
    tSNMP_GET_VAR_BIND *pGetVarBind = NULL;
    tSNMP_MULTI_DATA_TYPE RowStatus;
    tMbDbEntry         *pCurr = NULL, *pNext = NULL;
    BOOL1               isRBEnabled;
    BOOL1               isBreakCreateAndGo[MAX_VARBIND];
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OID_TYPE     *pCustOid = NULL;
    struct tm          *pTime = NULL;
    UINT4               u4AscTimeLen = 0;

    pIndex = SNMPGetFirstIndexPool ();
    isRBEnabled = b1IsRollbackEnabled;
    b1IsInitCalled = SNMP_TRUE;

    pOid = alloc_oid (MAX_OID_LENGTH);

    if (pOid == NULL)
    {
        pPdu->i4_ErrorStatus = SNMP_ERR_NO_ERROR;
        pPdu->i4_ErrorIndex = 0;
        SNMPTrace ("SNMP: Unable to allocate memory for Oids\n");
        return SNMP_FAILURE;
    }

    /*STEP 1: TEST operation and 
       construction of index list for tabular objects */
    SNMPTrace ("-------Inside SNMPSetProcess***********\n\n");
    SNMPARGTrace ("isRBEnabled:%d\n", isRBEnabled);
/***********************TEST******************************************/
    SNMPTrace ("Start Test while loop\n ");
    while (pVarPtr != NULL)
    {
        /*To avoid Array out of bounds */
        if (i4Index >= SNMP_MAX_INDICES_2)
        {
            break;
        }
        else
        {
            i4Index++;
        }

        if (pVarPtr->ObjValue.i2_DataType == SNMP_DATA_TYPE_IP_ADDR_PRIM)
        {
            SNMPTrace ("i2_DataType == SNMP_DATA_TYPE_IP_ADDR_PRIM\n ");
            if (pVarPtr->ObjValue.pOctetStrValue->i4_Length != SNMP_FOUR)
            {
                SNMPTrace ("i4_Length!= SNMP_FOUR\n ");
                pPdu->i4_ErrorStatus = SNMP_ERR_WRONG_LENGTH;
                pPdu->i4_ErrorIndex = i4Index;
                free_oid (pOid);
                return SNMP_FAILURE;
            }
        }
        if (pVarPtr->pObjName == NULL)
        {
            pPdu->i4_ErrorStatus = SNMP_ERR_COMMIT_FAILED;
            pPdu->i4_ErrorIndex = i4Index;
            free_oid (pOid);
            return SNMP_FAILURE;
        }
        SNMPUpdateEOID (pVarPtr->pObjName, pOid, FALSE);
        if (pOid->u4_Length > SNMP_MAX_OID_LENGTH)
        {
            SNMP_INR_BAD_ASN_PARSE;
            pPdu->i4_ErrorStatus = SNMP_ERR_TOO_BIG;
            pPdu->i4_ErrorIndex = i4Index;
            free_oid (pOid);
            return SNMP_FAILURE;
        }

        SNMPARGTrace ("Calling SNMPVACMIsAccessAllowed:%d\n ", i4Index);
        i4Result = SNMPVACMIsAccessAllowed (pVacmInfo, SNMP_READWRITE,
                                            pVarPtr->pObjName,
                                            (UINT4 *) &i4Err);
        if (i4Result == SNMP_SUCCESS)
        {
            SNMPARGTrace ("Calling SNMPTest:%d\n ", i4Index);
            VACMGetContextNum (pVacmInfo->pContextName, &u4VcNum);
            /* STEP 1.1: passing (i4Index -1)th element of pIndex array */
            pIndex[i4Index - 1].u4No = 0;

            GetMbDbEntry (*pOid, &pNext, &u4Len, &SnmpDbEntry);
            pCurr = SnmpDbEntry.pMibEntry;
            /*initializing to invalid for other varbinds */
            isBreakCreateAndGo[i4Index] = SNMP_INVALID;

            if (pCurr != NULL &&
                pCurr->u2RowStatus == TRUE &&
                pVarPtr->ObjValue.i4_SLongValue == CREATE_AND_GO)
            {
                SNMPTrace ("Test for createandwait\n ");
                MEMSET (&RowStatus, SNMP_ZERO, sizeof (tSNMP_MULTI_DATA_TYPE));
                RowStatus.i2_DataType = SNMP_DATA_TYPE_INTEGER32;
                RowStatus.i4_SLongValue = CREATE_AND_WAIT;

                /*Test with CreateAndWait */
                i4Result = SNMPTest (*pOid, &RowStatus, (UINT4 *) &i4Err,
                                     pIndex + (i4Index - 1), u4VcNum);

                /*if createAndwait failed with err inconsistent value */
                if (i4Result == SNMP_FAILURE)
                {                /*Test for createAndgo */
                    SNMPTrace ("Creatandwait failed: Test for createandgo\n ");
                    i4Result = SNMPTest (*pOid, &pVarPtr->ObjValue,
                                         (UINT4 *) &i4Err,
                                         pIndex + (i4Index - 1), u4VcNum);

                    if (i4Result == SNMP_SUCCESS)
                    {            /*if successful then dont break createAndgo */
                        SNMPTrace ("Test failed:cant break createandgo\n ");
                        isBreakCreateAndGo[i4Index] = SNMP_FALSE;
                    }
                }
                else
                {                /*if createandwait success then break createAndgo */
                    SNMPTrace ("can break creatandgo\n ");
                    isBreakCreateAndGo[i4Index] = SNMP_TRUE;
                }

            }
            else
            {
                SNMPTrace ("Normal Test \n ");
                i4Result = SNMPTest (*pOid, &pVarPtr->ObjValue,
                                     (UINT4 *) &i4Err,
                                     pIndex + (i4Index - 1), u4VcNum);
            }
        }
        else
        {
            SNMPTrace ("SNMPVACMIsAccessAllowed returned FAILURE\n ");
            if (i4Err == SNMP_NO_SUCH_VIEW ||
                i4Err == SNMP_ERR_NO_ACCESS_ENTRY ||
                i4Err == SNMP_ERR_NO_GROUP_NAME)
            {
                if (pPdu->u4_Version == VERSION2)
                {
                    pPdu->i4_ErrorStatus = SNMP_ERR_AUTHORIZATION_ERROR;
                    pPdu->i4_ErrorIndex = SNMP_ZERO;
                }
                else
                {
                    SNMP_INR_IN_NO_SUCH_NAME;
                    pPdu->i4_ErrorStatus = SNMP_ERR_NO_SUCH_NAME;
                    pPdu->i4_ErrorIndex = i4Index;
                }
                /* RFC 3584 section 4.4
                 * Whenever the SNMPv2 error-status value of authorizationError is
                 * translated to an SNMPv1 error-status value of noSuchName, 
                 * the value of snmpInBadCommunityUses MUST be incremented.*/
                SNMP_INR_BAD_COMM_USER;
                if (gSnmpStat.i4SnmpEnableAuthTraps == AUTHTRAP_ENABLE)
                {
                    SnmpSendAuthFailureTrap (pPdu);
                }
            }
            else if (i4Err == SNMP_ERR_NO_CONTEXT)
            {
                free_oid (pOid);
                return SNMPDROP;
            }
            else
            {
                SNMP_INR_IN_GEN_ERROR;
                pPdu->i4_ErrorStatus = SNMP_ERR_GEN_ERR;
                pPdu->i4_ErrorIndex = i4Index;
            }
            free_oid (pOid);
            return SNMP_FAILURE;
        }

        /*
         * To return GEN ERR, when a SET request is received from a
         * V1 Manager.
         */
        if ((pPdu->u4_Version == VERSION1) &&
            (pVarPtr->ObjValue.i2_DataType == SNMP_DATA_TYPE_COUNTER64))
        {
            SNMPTrace ("SNMPTest returned FAILURE\n");
            pPdu->i4_ErrorStatus = SNMP_ERR_GEN_ERR;
            pPdu->i4_ErrorIndex = i4Index;
            SNMP_INR_BAD_ASN_PARSE;
            free_oid (pOid);
            return SNMP_FAILURE;
        }

        if (i4Result == SNMP_FAILURE)
        {
            SNMPTrace ("SNMPTest returned FAILURE\n");
            if (((pPdu->u4_Version == VERSION1) &&
                 (i4Err != SNMP_ERR_NOT_WRITABLE)) ||
                ((pPdu->u4_Version == VERSION2) &&
                 (i4Err < SNMP_ERR_NO_ERROR ||
                  i4Err > SNMP_ERR_INCONSISTENT_NAME)))
            {
                SNMP_INR_OUT_BAD_VALUE;
                pPdu->i4_ErrorStatus = SNMP_ERR_BAD_VALUE;
            }
            else
            {
                if ((pPdu->u4_Version == VERSION1) &&
                    (i4Err == SNMP_ERR_NOT_WRITABLE))
                {
                    pPdu->i4_ErrorStatus = SNMP_ERR_NO_SUCH_NAME;
                }
                else
                {
                    pPdu->i4_ErrorStatus = i4Err;
                }
            }
            pPdu->i4_ErrorIndex = i4Index;

            if (ISS_IS_AUDIT_ENABLED () == ISS_TRUE)
            {
                pTempAudit = MemAllocMemBlk (gSnmpAuditInfoPoolId);
                if (pTempAudit == NULL)
                {
                    free_oid (pOid);
                    return SNMP_FAILURE;
                }
                MEMSET (pTempAudit, 0, sizeof (tAuditInfo));
                t = time (NULL);
                OID = *(pVarPtr->pObjName);
                pVal = &(pVarPtr->ObjValue);
                pu1Oid = MemAllocMemBlk (gSnmpMultiOidPoolId);
                if (pu1Oid == NULL)
                {
                    free_oid (pOid);
                    MemReleaseMemBlock (gSnmpAuditInfoPoolId,
                                        (UINT1 *) pTempAudit);
                    return SNMP_FAILURE;
                }
                MEMSET (pu1Oid, 0, sizeof (tSnmpMultiOidBlock));
                SNMPGetOidString (OID.pu4_OidList, OID.u4_Length, pu1Oid);
                STRCPY (pTempAudit->TempSnmp.au4_OidList, "Audit:");
                STRNCAT (pTempAudit->TempSnmp.au4_OidList, pu1Oid,
                         STRLEN (pu1Oid));
                MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1Oid);

                pu1Data = MemAllocMemBlk (gSnmpDataPoolId);
                if (pu1Data == NULL)
                {
                    free_oid (pOid);
                    MemReleaseMemBlock (gSnmpAuditInfoPoolId,
                                        (UINT1 *) pTempAudit);
                    return SNMP_FAILURE;
                }
                MEMSET (pu1Data, 0, MAX_SNMP_DATA_LENGTH);
                SNMPConvertDataToString (pVal, pu1Data,
                                         (UINT2) pVal->i2_DataType);
                pTempAudit->TempSnmp.pu1ObjVal = pu1Data;
                STRNCPY (pTempAudit->TempSnmp.ai1SnmpUser,
                         pVacmInfo->pUserName->pu1_OctetList,
                         (UINT4) pVacmInfo->pUserName->i4_Length);
                pTempAudit->TempSnmp.u4MngrIpAddr = SNMPGetManagerAddr ();
                STRCPY (pTempAudit->TempSnmp.ai1Status, "Failed");
                pTime = localtime (&t);
                if (pTime != NULL)
                {
                    u4AscTimeLen = STRLEN (asctime (pTime));
                    u4Len = (u4AscTimeLen < (sizeof (pTempAudit->au1Time) - 1))
                        ? u4AscTimeLen : (sizeof (pTempAudit->au1Time) - 1);
                    STRNCPY (pTempAudit->au1Time, asctime (pTime), u4Len);
                    pTempAudit->au1Time[u4Len] = '\0';
                }
                pTempAudit->i2Flag = AUDIT_SNMP_MSG;
                if (MSRSendAuditInfo (pTempAudit) != MSR_SUCCESS)
                {
                    SNMPTrace ("Error in Audit queue send from SNMP Test\n");
                    free_oid (pOid);
                    MemReleaseMemBlock (gSnmpAuditInfoPoolId,
                                        (UINT1 *) pTempAudit);
                    MemReleaseMemBlock (gSnmpDataPoolId, pu1Data);
                    return SNMP_FAILURE;
                }
                MemReleaseMemBlock (gSnmpAuditInfoPoolId, (UINT1 *) pTempAudit);
            }

            free_oid (pOid);
            return SNMP_FAILURE;
        }
        pVarPtr = pVarPtr->pNextVarBind;
        /* STEP 1.2: Reassigning MULTI_DATA_VALUE pointer of 
           gaIndexPool1[i4Index] to correct location of gaMultiPool1 array */
        pIndex[i4Index].pIndex = pIndex[i4Index - 1].pIndex +
            pIndex[i4Index - 1].u4No;
    }

    SNMPTrace ("end of SNMPTest\n \n");

    pCustOid = alloc_oid (MAX_OID_LENGTH);

    if (pCustOid == NULL)
    {
        pPdu->i4_ErrorStatus = SNMP_ERR_NO_ERROR;
        pPdu->i4_ErrorIndex = 0;
        SNMPTrace ("SNMP: Unable to allocate memory for Oids\n");
        free_oid (pOid);
        return SNMP_FAILURE;
    }
    SNMPOIDCopy (pCustOid, pPdu->pVarBindList->pObjName);

/*********************************************************************/
    /*prepare indexList to be passed in Dependency function call */
    numVarbinds = i4Index;
    indexList.u4NoVarbind = (UINT4) numVarbinds;
    indexList.pIndexList = pIndex;
    /*  allocate memory for pGetVarBind  */
    pGetVarBind = MemAllocMemBlk (gSnmpGetVarBindPoolId);
    if (pGetVarBind == NULL)
    {
        SNMPTrace ("pGetVarBind Memory Allocation failure\n");
        i4Err = SNMP_ERR_GEN_ERR;
        SNMP_INR_IN_GEN_ERROR;
        free_oid (pOid);
        free_oid (pCustOid);
        return SNMP_FAILURE;
    }

/********************DEP/GET********************************************/
    /* STEP 2: Dependency check and Get operation */
    pVarPtr = pPdu->pVarBindList;
    i4Index = 0;
    SNMPTrace ("start DEP/GET while loop \n");
    while (pVarPtr != NULL)
    {

        /*To avoid Array out of bounds */
        if (i4Index >= SNMP_MAX_INDICES_2)
        {
            break;
        }
        else
        {
            i4Index++;
        }

        SNMPARGTrace ("calling DEP for:%d\n", i4Index);

        if (pVarPtr->pObjName == NULL)
        {
            pPdu->i4_ErrorStatus = SNMP_ERR_COMMIT_FAILED;
            pPdu->i4_ErrorIndex = i4Index;
            freeAllRBMemory (pGetVarBind, i4Index - 1);
            free_oid (pCustOid);
            free_oid (pOid);
            return SNMP_FAILURE;
        }
        SNMPUpdateEOID (pVarPtr->pObjName, pOid, FALSE);
        if (pOid->u4_Length > SNMP_MAX_OID_LENGTH)
        {
            SNMP_INR_BAD_ASN_PARSE;
            i4Err = SNMP_ERR_TOO_BIG;
            pPdu->i4_ErrorStatus = SNMP_ERR_TOO_BIG;
            pPdu->i4_ErrorIndex = i4Index;
            freeAllRBMemory (pGetVarBind, i4Index - 1);
            free_oid (pCustOid);
            free_oid (pOid);
            return SNMP_FAILURE;
        }

        if (i4Result == SNMP_FAILURE)
            /* STEP 2.1:call Dep check API */
            i4Result = SNMPDependency (*pOid, pPdu->pVarBindList,
                                       (UINT4 *) &i4Err, &indexList, u4VcNum);

        if (i4Result == SNMP_FAILURE)
        {
            SNMPTrace ("DEP failure\n");
            i4Err = SNMP_ERR_GEN_ERR;
            pPdu->i4_ErrorStatus = SNMP_ERR_GEN_ERR;
            pPdu->i4_ErrorIndex = i4Index;
            i4Result = SNMP_FAILURE;
            freeAllRBMemory (pGetVarBind, i4Index - 1);
            free_oid (pOid);
            free_oid (pCustOid);
            return SNMP_FAILURE;
        }

        /* STEP 2.2:allocate memory for current rollback value of object */
        pGetVarBind[i4Index - 1].isRollBackReqd = SNMP_FALSE;
        pGetVarBind[i4Index - 1].isRowStatus = 0;
        pGetVarBind[i4Index - 1].pSetVarBind = pVarPtr;
        pGetVarBind[i4Index - 1].activateLater = SNMP_FALSE;
        pGetVarBind[i4Index - 1].prevRowStatus = SNMP_INVALID;
        pGetVarBind[i4Index - 1].isValueSet = SNMP_FALSE;
        pGetVarBind[i4Index - 1].pRollbackValue = SNMPAllocVarBind ();

        if (pGetVarBind[i4Index - 1].pRollbackValue == NULL)
        {
            SNMPTrace ("Allocation of VarBind failure\n");
            i4Err = SNMP_ERR_GEN_ERR;
            pPdu->i4_ErrorStatus = SNMP_ERR_GEN_ERR;
            pPdu->i4_ErrorIndex = i4Index;
            i4Result = SNMP_FAILURE;
            freeAllRBMemory (pGetVarBind, i4Index - 1);
            free_oid (pOid);
            free_oid (pCustOid);
            return SNMP_FAILURE;
        }

        /* Update the enterprise oid in the GetVarbind obj and for the
         * rollback value */
        SNMPOIDCopy (pGetVarBind[i4Index - 1].pRollbackValue->pObjName, pOid);
        SNMPOIDCopy (pGetVarBind[i4Index - 1].pSetVarBind->pObjName, pOid);

        /* Rollback: Construct (pGetVarBind + i4Index) parameters */
        GetMbDbEntry (*pOid, &pNext, &u4Len, &SnmpDbEntry);
        pCurr = SnmpDbEntry.pMibEntry;

        if (pCurr != NULL)
            pGetVarBind[i4Index - 1].isRowStatus = (INT1) pCurr->u2RowStatus;

        if ((pCurr != NULL) && (pCurr->u4NoIndices != 0) &&
            (pCurr->u2RowStatus == TRUE) &&
            (pVarPtr->ObjValue.i4_SLongValue == CREATE_AND_GO))
        {
            pGetVarBind[i4TempIndex].activateLater = SNMP_TRUE;
        }

        if (isRBEnabled == SNMP_ROLLBACK_ENABLED)
        {

            SNMPARGTrace ("calling GET for:%d\n", i4Index);
            i4Result = SNMPGet (*pOid,
                                &(pGetVarBind[i4Index - 1].pRollbackValue->
                                  ObjValue), (UINT4 *) &i4Err,
                                pIndex + (i4Index - 1), u4VcNum);

            /*if(IS_SCALAR(pCurr)) - might work */
            if ((pCurr != NULL) && (pCurr->u4NoIndices == 0))
            {
                SNMPTrace ("OBJ is scalar\n");
                if (i4Result == SNMP_FAILURE)
                {
                    /*should never occur because 
                       possible errors should have occurred
                       in Test and Dependency check */

                    SNMPTrace (" and returned failure:ERROR\n");
                    i4Err = SNMP_ERR_GEN_ERR;
                    pPdu->i4_ErrorStatus = SNMP_ERR_GEN_ERR;
                    pPdu->i4_ErrorIndex = i4Index;
                    i4Result = SNMP_FAILURE;
                    freeAllRBMemory (pGetVarBind, i4Index);
                    free_oid (pOid);
                    free_oid (pCustOid);
                    return SNMP_FAILURE;
                }
                else
                {
                    SNMPTrace (" and rollback is reqd\n");
                    pGetVarBind[i4Index - 1].isRollBackReqd = TRUE;
                    pGetVarBind[i4Index - 1].prevRowStatus = SNMP_INVALID;
                }
            }
            else                /*tabular */
            {
                if (pCurr != NULL && pCurr->u2RowStatus == FALSE)    /*non rowstatus */
                {
                    SNMPTrace (" OBJ is non rowstatus\n");
                    pGetVarBind[i4Index - 1].prevRowStatus = SNMP_INVALID;
                    if (i4Result == SNMP_FAILURE)
                    {
                        SNMPTrace (" and rollback is not reqd\n");
                        pGetVarBind[i4Index - 1].isRollBackReqd = FALSE;
                    }
                    else
                    {
                        SNMPTrace (" and rollback is reqd\n");
                        pGetVarBind[i4Index - 1].isRollBackReqd = TRUE;
                    }
                }
                if (pCurr != NULL && pCurr->u2RowStatus == TRUE)
                     /*ROWSTATUS*/
                {
                    SNMPTrace ("OBJ is rowstatus\n");
                    if (pVarPtr->ObjValue.i4_SLongValue == DESTROY ||
                        pVarPtr->ObjValue.i4_SLongValue == CREATE_AND_WAIT ||
                        ((pVarPtr->ObjValue.i4_SLongValue == ACTIVE ||
                          pVarPtr->ObjValue.i4_SLongValue == NOT_IN_SERVICE) &&
                         i4Result == SNMP_FAILURE))
                    {            /*these are the conditions under which entry
                                   into prevRowStatus has to be invalid */
                        SNMPTrace (" prevRowStatus has to be invalid\n");
                        pGetVarBind[i4Index - 1].prevRowStatus = SNMP_INVALID;
                        if (pVarPtr->ObjValue.i4_SLongValue == DESTROY)
                        {
                            /*if rowstatus is destory then search for any 
                               createandgo for same row */
                            i4TempIndex = 0;
                            rowStatusPresent = SNMP_FALSE;
                            while (i4TempIndex < i4Index - 1)
                            {    /*search for same OID with createandgo */
                                if (OIDCompare
                                    (*(pGetVarBind[i4TempIndex].pRollbackValue->
                                       pObjName), *pOid,
                                     (UINT4 *) &u4Match) == SNMP_EQUAL)
                                {
                                    if (pGetVarBind[i4TempIndex].pSetVarBind->
                                        ObjValue.i4_SLongValue == CREATE_AND_GO)
                                    {
                                        rowStatusPresent = SNMP_TRUE;
                                        break;
                                    }
                                    else
                                    {
                                        i4TempIndex++;

                                    }
                                }
                                else
                                {
                                    i4TempIndex++;
                                }
                            }
                            if (rowStatusPresent == SNMP_TRUE)
                            {    /*if destroy for same row then no need to send Active for 
                                   createandgo because row would have been destroyed till then */
                                pGetVarBind[i4TempIndex].activateLater =
                                    SNMP_FALSE;
                            }
                        }

                    }
                    else
                    {
                        i4TempIndex = 0;
                        rowStatusPresent = SNMP_FALSE;
                        while (i4TempIndex < i4Index - 1)
                        {        /*search for same OID inside set pdu */
                            if (OIDCompare
                                (*
                                 (pGetVarBind[i4TempIndex].pRollbackValue->
                                  pObjName), *pOid,
                                 (UINT4 *) &u4Match) == SNMP_EQUAL)
                            {
                                rowStatusPresent = SNMP_TRUE;
                                break;
                            }
                            else
                            {
                                i4TempIndex++;
                            }
                        }
                        if (rowStatusPresent == SNMP_FALSE)
                            /*rowstatus was not present inside set pdu */
                        {
                            pGetVarBind[i4Index - 1].prevRowStatus =
                                SNMP_INVALID;
                            switch (pVarPtr->ObjValue.i4_SLongValue)
                            {
                                    /* assign current row status into i4_prevRowStatus */
                                case CREATE_AND_GO:
                                    /*there would be no row existing before */
                                    /*required in order to send ACTIVE 
                                       in case of successful set */
                                    SNMPTrace
                                        (" prevRowStatus has to be invalid\n");
                                    pGetVarBind[i4Index - 1].prevRowStatus =
                                        SNMP_INVALID;
                                    if (isBreakCreateAndGo[i4Index] ==
                                        SNMP_FALSE)
                                    {
                                        pGetVarBind[i4Index - 1].activateLater =
                                            SNMP_FALSE;
                                    }
                                    else if (isBreakCreateAndGo[i4Index] ==
                                             SNMP_TRUE)
                                        pGetVarBind[i4Index - 1].activateLater =
                                            SNMP_TRUE;
                                    break;
                                case ACTIVE:
                                    SNMPTrace
                                        (" prevRowStatus has to be valid\n");
                                    pGetVarBind[i4Index - 1].prevRowStatus =
                                        pGetVarBind[i4Index -
                                                    1].pRollbackValue->ObjValue.
                                        i4_SLongValue;
                                    break;
                                case NOT_IN_SERVICE:
                                    SNMPTrace
                                        (" prevRowStatus has to be valid\n");
                                    pGetVarBind[i4Index - 1].prevRowStatus =
                                        pGetVarBind[i4Index -
                                                    1].pRollbackValue->ObjValue.
                                        i4_SLongValue;
                                    break;
                                default:
                                    break;
                            }

                        }
                        else
                        {
                            SNMPTrace ("rowstatus exists twice in Set PDU\n");
                        }
                    }            /*pGetVarBind updated */

                    /*update pGetVarBind now */
                    switch (pVarPtr->ObjValue.i4_SLongValue)
                        /*switch on value to be set */
                    {
                        case CREATE_AND_GO:
                            pGetVarBind[i4Index - 1].isRollBackReqd = TRUE;
                            pGetVarBind[i4Index - 1].pRollbackValue->
                                ObjValue.i4_SLongValue = DESTROY;

                            pGetVarBind[i4Index - 1].prevRowStatus =
                                SNMP_INVALID;
                            if (isBreakCreateAndGo[i4Index] == SNMP_FALSE)
                            {
                                pGetVarBind[i4Index - 1].activateLater =
                                    SNMP_FALSE;
                            }
                            else if (isBreakCreateAndGo[i4Index] == SNMP_TRUE)
                                pGetVarBind[i4Index - 1].activateLater =
                                    SNMP_TRUE;
                            break;

                        case CREATE_AND_WAIT:
                            /*destory the newly created rows in case
                               of rollback */
                            pGetVarBind[i4Index - 1].isRollBackReqd = TRUE;
                            pGetVarBind[i4Index - 1].pRollbackValue->
                                ObjValue.i4_SLongValue = DESTROY;
                            break;
                        case ACTIVE:
                        case NOT_IN_SERVICE:
                            /*change to inactive in order to rollback
                               values of column */
                            pGetVarBind[i4Index - 1].isRollBackReqd = TRUE;
                            pGetVarBind[i4Index - 1].pRollbackValue->
                                ObjValue.i4_SLongValue = NOT_IN_SERVICE;
                            break;
                        case DESTROY:    /*rollback will fail */
                            pGetVarBind[i4Index - 1].isRollBackReqd =
                                SNMP_NOT_OK;
                            break;
                        default:
                            break;
                    }            /*end of switch */
                }                /*end of if rowstatus */
            }                    /*end of if tabular */
        }
        pVarPtr = pVarPtr->pNextVarBind;
    }                            /*end of while */
    SNMPTrace ("end DEP/GET while loop\n\n");

/*********************SET ********************************************/

    /* STEP 3.1: To SET values of rowtstaus Creation requests */
    i4Index = 0;
    SNMPTrace ("start SET of create requests first\n");
    while (i4Index < numVarbinds)
    {
        i4Index++;

        if (isBreakCreateAndGo[i4Index] == SNMP_FALSE)
        {                        /*createAndgo */
            MEMSET (&RowStatus, SNMP_ZERO, sizeof (tSNMP_MULTI_DATA_TYPE));
            RowStatus.i2_DataType = SNMP_DATA_TYPE_INTEGER32;
            RowStatus.i4_SLongValue = CREATE_AND_GO;

            SNMPTrace ("SET of createAndGo\n");
            i4Result =
                SNMPSet (*(pGetVarBind[i4Index - 1].pSetVarBind->pObjName),
                         &RowStatus, (UINT4 *) &i4Err, pIndex, u4VcNum);
            if (i4Result == SNMP_SUCCESS)
            {
                pGetVarBind[i4Index - 1].isValueSet = SNMP_TRUE;
                SNMP_INR_TOT_SET_SUCCESS;
            }
        }
        /* Sending Rowstatus as 'CREATE_AND_WAIT if it is CREATE_AND_GO */
        else if (isBreakCreateAndGo[i4Index] == SNMP_TRUE)
        {                        /*createAndwait */
            SNMPTrace ("SET of createAndWait\n");
            MEMSET (&RowStatus, SNMP_ZERO, sizeof (tSNMP_MULTI_DATA_TYPE));
            RowStatus.i2_DataType = SNMP_DATA_TYPE_INTEGER32;
            RowStatus.i4_SLongValue = CREATE_AND_WAIT;

            i4Result =
                SNMPSet (*(pGetVarBind[i4Index - 1].pSetVarBind->pObjName),
                         &RowStatus, (UINT4 *) &i4Err, pIndex, u4VcNum);
            if (i4Result == SNMP_SUCCESS)
            {

                pGetVarBind[i4Index - 1].isValueSet = SNMP_TRUE;
                SNMP_INR_TOT_SET_SUCCESS;
            }
        }
        else                    /*nothing to do */
            i4Result = SNMP_SUCCESS;

        if (i4Result == SNMP_FAILURE)
        {
            SNMPTrace (" ERROR:SET operation failure\n");
            SNMP_INR_OUT_GEN_ERROR;
            if (pPdu->u4_Version == VERSION1
                || isRBEnabled == SNMP_ROLLBACK_DISABLED)
                pPdu->i4_ErrorStatus = SNMP_ERR_GEN_ERR;
            else
                pPdu->i4_ErrorStatus = SNMP_ERR_COMMIT_FAILED;
            pPdu->i4_ErrorIndex = i4Index;
            i4Result = SNMP_FAILURE;

            if (isRBEnabled == SNMP_ROLLBACK_DISABLED)
            {
                SNMPTrace (" SET returning failure\n");
                gSnmpStat.u4SnmpInTotalSetVars += (UINT4) (i4Index - 1);
                freeAllRBMemory (pGetVarBind, numVarbinds);
                free_oid (pOid);
                free_oid (pCustOid);
                return i4Result;
            }
            break;                /*NOW COMES THE ROLLBACK */
        }

    }                            /*end of while */
    SNMPTrace ("end of SET of create requests first\n");

    /* STEP 3.2: To SET values of all other varbinds */
    if (i4Result != SNMP_FAILURE)
    {
        pVarPtr = pPdu->pVarBindList;
        i4Index = 0;
        SNMPTrace ("start SET while loop \n");
        while (pVarPtr != NULL)
        {
            /*To avoid Array out of bounds */
            if (i4Index >= SNMP_MAX_INDICES_2)
            {
                break;
            }
            else
            {
                i4Index++;
            }

            if (pVarPtr->pObjName == NULL)
            {
                pPdu->i4_ErrorIndex = i4Index + 1;
                freeAllRBMemory (pGetVarBind, numVarbinds);
                free_oid (pCustOid);
                free_oid (pOid);
                return SNMP_FAILURE;
            }
            SNMPUpdateEOID (pVarPtr->pObjName, pOid, FALSE);
            if (pOid->u4_Length > SNMP_MAX_OID_LENGTH)
            {
                SNMP_INR_BAD_ASN_PARSE;
                gSnmpStat.u4SnmpInTotalSetVars += (UINT4) i4Index - 1;
                freeAllRBMemory (pGetVarBind, numVarbinds);
                free_oid (pCustOid);
                free_oid (pOid);
                return SNMP_FAILURE;
            }

            SNMPARGTrace ("calling SET for:%d\n", i4Index);
            if (isBreakCreateAndGo[i4Index] == SNMP_INVALID)
            {
                i4Result = SNMPSet (*pOid,
                                    &pVarPtr->ObjValue, (UINT4 *) &i4Err,
                                    pIndex, u4VcNum);
                if (i4Result == SNMP_SUCCESS)
                {
                    SNMPTrace (" this SET operation success\n");
                    pGetVarBind[i4Index - 1].isValueSet = SNMP_TRUE;
                    SNMP_INR_TOT_SET_SUCCESS;
                }
                if (i4Result == SNMP_FAILURE)
                {
                    SNMPTrace (" ERROR:SET operation failure\n");
                    SNMP_INR_OUT_GEN_ERROR;
                    if (pPdu->u4_Version == VERSION1 ||
                        isRBEnabled == SNMP_ROLLBACK_DISABLED)
                        pPdu->i4_ErrorStatus = SNMP_ERR_GEN_ERR;
                    else
                        pPdu->i4_ErrorStatus = SNMP_ERR_COMMIT_FAILED;
                    pPdu->i4_ErrorIndex = i4Index;
                    i4Result = SNMP_FAILURE;

                    if (isRBEnabled == SNMP_ROLLBACK_DISABLED)
                    {
                        SNMPTrace (" SET returning failure\n");
                        gSnmpStat.u4SnmpInTotalSetVars += (UINT4) (i4Index - 1);
                        freeAllRBMemory (pGetVarBind, numVarbinds);
                        free_oid (pOid);
                        free_oid (pCustOid);
                        return i4Result;
                    }
                    break;        /*NOW COMES THE ROLLBACK */
                }
            }
            pVarPtr = pVarPtr->pNextVarBind;
        }                        /*End of while of set */
    }
    SNMPTrace ("end SET while loop \n\n");

/*********************************************************************/
    /* STEP 4: Set operation successful - 
       Send RowStatus as Active for all CreateAndWait */
    if (i4Result == SNMP_SUCCESS)
    {
        SNMPTrace ("All SET operations successful:Send RowStatus as Active\n");
/********************ACTIVE*******************************************/
        SNMPTrace ("start while loop for sending RowStatus as Active\n");
        i4TempIndex = 0;
        pVarPtr = pPdu->pVarBindList;
        while (i4TempIndex < numVarbinds)
        {
            MEMSET (&RowStatus, SNMP_ZERO, sizeof (tSNMP_MULTI_DATA_TYPE));
            RowStatus.i2_DataType = SNMP_DATA_TYPE_INTEGER32;
            RowStatus.i4_SLongValue = ACTIVE;

            /* For CreateAndG0 splitting activateLater would be set to true */
            if (pGetVarBind[i4TempIndex].activateLater == SNMP_TRUE)
            {
                if (pVarPtr->pObjName == NULL)
                {
                    freeAllRBMemory (pGetVarBind, numVarbinds);
                    free_oid (pCustOid);
                    free_oid (pOid);
                    return SNMP_FAILURE;
                }
                SNMPUpdateEOID (pVarPtr->pObjName, pOid, FALSE);
                if (pOid->u4_Length > SNMP_MAX_OID_LENGTH)
                {
                    pPdu->i4_ErrorIndex = i4Index + 1;
                    i4Result = SNMP_FAILURE;
                    freeAllRBMemory (pGetVarBind, numVarbinds);
                    free_oid (pCustOid);
                    SNMP_INR_BAD_ASN_PARSE;
                    free_oid (pOid);
                    return SNMP_FAILURE;
                }

                i4Result = SNMPSet (*pOid, &(RowStatus),
                                    (UINT4 *) &i4Err, pIndex, u4VcNum);
                if (i4Result == SNMP_FAILURE)
                {
                    SNMPTrace ("Send RowStatus as Active:FAILURE\n");
                    /* need to ROLLBACK -in reverse order if this fails tooo */
                    if (pPdu->u4_Version == VERSION1)
                        pPdu->i4_ErrorStatus = SNMP_ERR_GEN_ERR;
                    else
                        pPdu->i4_ErrorStatus = SNMP_ERR_COMMIT_FAILED;
                    pPdu->i4_ErrorIndex = i4TempIndex + 1;
                    i4Result = SNMP_FAILURE;
                    /* this is done so that rollback is
                       now done in reverse order
                       from the end of pGetVarBind- 
                       so that rollback all varbinds */
                    i4Index = numVarbinds + 1;
                    break;
                }                /*end of if-failure */
            }                    /*end of if */
            if (pVarPtr->pNextVarBind != NULL)
                pVarPtr = pVarPtr->pNextVarBind;
            i4TempIndex++;
        }                        /*end of while */
        SNMPTrace ("End RowStatus as Active while\n\n");
    }

    /*Set operation failed :-( */
    if (i4Result == SNMP_FAILURE && isRBEnabled == SNMP_ROLLBACK_ENABLED)
    {
        /* STEP 5:ROLLBACK -in reverse order in case of failure */
/******************ROLLBACK*******************************************/
        pVarPtr = pPdu->pVarBindList;
        i4Index -= 2;            /*adjusting of index */
        i4TempIndex = i4Index;
        i4Index = numVarbinds - 1;
        /*RESET values of all varbinds whose values we have SET so far */
        SNMPTrace ("start while of Rollback\n");
        while (i4Index >= 0)
        {                        /*Rollback only if value was set */
            if (pGetVarBind[i4Index].isValueSet == SNMP_TRUE)
            {
                if (pGetVarBind[i4Index].isRollBackReqd == SNMP_NOT_OK)
                {
                    SNMPTrace
                        ("Rolback required is not_ok if curr val was DESTROY\n");
                    /*rollback is failing coz current val=DESTROY */
                    if (pPdu->u4_Version == VERSION1)
                        pPdu->i4_ErrorStatus = SNMP_ERR_GEN_ERR;
                    else
                        pPdu->i4_ErrorStatus = SNMP_ERR_UNDO_FAILED;
                    SNMP_INR_ROLLBACK_ERRS;
                    pPdu->i4_ErrorIndex = i4Index + 1;
                    i4Result = SNMP_FAILURE;
                    freeAllRBMemory (pGetVarBind, numVarbinds);
                    free_oid (pOid);
                    free_oid (pCustOid);
                    return SNMP_FAILURE;
                }
                if (pGetVarBind[i4Index].isRollBackReqd == TRUE)
                {
                    SNMPTrace ("Rollback is required\n");
                    i4Result =
                        SNMPSet (*
                                 (pGetVarBind[i4Index].pRollbackValue->
                                  pObjName),
                                 &(pGetVarBind[i4Index].pRollbackValue->
                                   ObjValue), (UINT4 *) &i4Err, pIndex,
                                 u4VcNum);
                    if (i4Result == SNMP_FAILURE)
                    {
                        SNMPTrace ("Rollback failure\n");
                        if (pPdu->u4_Version == VERSION1)
                            pPdu->i4_ErrorStatus = SNMP_ERR_GEN_ERR;
                        else
                            pPdu->i4_ErrorStatus = SNMP_ERR_UNDO_FAILED;
                        SNMP_INR_ROLLBACK_ERRS;
                        pPdu->i4_ErrorIndex = i4Index + 1;
                        i4Result = SNMP_FAILURE;
                        freeAllRBMemory (pGetVarBind, numVarbinds);
                        free_oid (pOid);
                        free_oid (pCustOid);
                        return SNMP_FAILURE;
                    }
                }
            }
            if (pVarPtr->pNextVarBind != NULL)
                pVarPtr = pVarPtr->pNextVarBind;
            i4Index--;
        }                        /*End of while */

        SNMPTrace ("end of Rollback while \n\n");
/****************RB rowstatus*****************************************/
        /*STEP 6: now rollback rowstatus saved inside pGetVarBind */
        SNMPTrace ("while of Rollback rowstatus\n");
        pVarPtr = pPdu->pVarBindList;
        while (i4TempIndex >= 0)
        {

            if (pGetVarBind[i4TempIndex].prevRowStatus != SNMP_INVALID)
            {
                MEMSET (&RowStatus, SNMP_ZERO, sizeof (tSNMP_MULTI_DATA_TYPE));
                RowStatus.i2_DataType = SNMP_DATA_TYPE_INTEGER32;
                RowStatus.i4_SLongValue =
                    pGetVarBind[i4TempIndex].prevRowStatus;
                i4Result =
                    SNMPSet (*(pGetVarBind[i4TempIndex].pSetVarBind->pObjName),
                             &(RowStatus), (UINT4 *) &i4Err, pIndex, u4VcNum);
                if (i4Result == SNMP_FAILURE)
                {
                    SNMPTrace ("Alas!Rollback failed\n");
                    if (RowStatus.i4_SLongValue == NOT_READY)
                    {            /*in case of notready, we cant set it ourselves
                                   maybe the system did that itself
                                   if not return error - UNDO_FAILED */
                        SNMPTrace ("not ready set failed\n");
                        i4Result =
                            SNMPGet (*
                                     (pGetVarBind[i4TempIndex].pSetVarBind->
                                      pObjName), &(RowStatus), (UINT4 *) &i4Err,
                                     pIndex, u4VcNum);
                        if (RowStatus.i4_SLongValue != NOT_READY)
                        {
                            SNMPTrace ("not ready set failed\n");
                            if (pPdu->u4_Version == VERSION1)
                                pPdu->i4_ErrorStatus = SNMP_ERR_GEN_ERR;
                            else
                                pPdu->i4_ErrorStatus = SNMP_ERR_UNDO_FAILED;
                            SNMP_INR_ROLLBACK_ERRS;
                            pPdu->i4_ErrorIndex = i4TempIndex + 1;
                            i4Result = SNMP_FAILURE;
                            freeAllRBMemory (pGetVarBind, numVarbinds);
                            free_oid (pOid);
                            free_oid (pCustOid);
                            return SNMP_FAILURE;

                        }
                        else
                        {
                            SNMPTrace (" But value was already not ready\n");
                        }
                    }
                    else
                    {
                        if (pPdu->u4_Version == VERSION1)
                            pPdu->i4_ErrorStatus = SNMP_ERR_GEN_ERR;
                        else
                            pPdu->i4_ErrorStatus = SNMP_ERR_UNDO_FAILED;
                        SNMP_INR_ROLLBACK_ERRS;
                        pPdu->i4_ErrorIndex = i4TempIndex + 1;
                        i4Result = SNMP_FAILURE;
                        freeAllRBMemory (pGetVarBind, numVarbinds);
                        free_oid (pOid);
                        free_oid (pCustOid);
                        return SNMP_FAILURE;
                    }
                }
            }
            i4TempIndex--;
        }
        SNMPTrace ("end of Rollback rollback while \n\n");
    }
    else
    {                            /*SET operation successful finally :-) */
        /*either all will be succful or none will be */
        SNMPTrace ("All SET operations successful\n");
        SNMPRevertEOID (pCustOid, pPdu->pVarBindList->pObjName);
        pPdu->i4_ErrorStatus = SNMP_ERR_NO_ERROR;
        pPdu->i4_ErrorIndex = SNMP_ZERO;
    }
    freeAllRBMemory (pGetVarBind, numVarbinds);
    free_oid (pOid);
    free_oid (pCustOid);
    return i4Result;
}

/********************  END OF FILE   ***************************************/
