/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: snmcomdb.h,v 1.5 2015/04/28 12:35:02 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _SNMCOMDB_H
#define _SNMCOMDB_H

UINT1 SnmpCommunityTableINDEX [] = {SNMP_DATA_TYPE_IMP_OCTET_PRIM};
UINT1 SnmpTargetAddrExtTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};

UINT4 snmcom [] ={1,3,6,1,6,3,18};
tSNMP_OID_TYPE snmcomOID = {7, snmcom};


UINT4 SnmpCommunityIndex [ ] ={1,3,6,1,6,3,18,1,1,1,1};
UINT4 SnmpCommunityName [ ] ={1,3,6,1,6,3,18,1,1,1,2};
UINT4 SnmpCommunitySecurityName [ ] ={1,3,6,1,6,3,18,1,1,1,3};
UINT4 SnmpCommunityContextEngineID [ ] ={1,3,6,1,6,3,18,1,1,1,4};
UINT4 SnmpCommunityContextName [ ] ={1,3,6,1,6,3,18,1,1,1,5};
UINT4 SnmpCommunityTransportTag [ ] ={1,3,6,1,6,3,18,1,1,1,6};
UINT4 SnmpCommunityStorageType [ ] ={1,3,6,1,6,3,18,1,1,1,7};
UINT4 SnmpCommunityStatus [ ] ={1,3,6,1,6,3,18,1,1,1,8};
UINT4 SnmpTargetAddrTMask [ ] ={1,3,6,1,6,3,18,1,2,1,1};
UINT4 SnmpTargetAddrMMS [ ] ={1,3,6,1,6,3,18,1,2,1,2};


tMbDbEntry snmcomMibEntry[]= {

{{11,SnmpCommunityIndex}, GetNextIndexSnmpCommunityTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, SnmpCommunityTableINDEX, 1, 0, 0, NULL},

{{11,SnmpCommunityName}, GetNextIndexSnmpCommunityTable, SnmpCommunityNameGet, SnmpCommunityNameSet, SnmpCommunityNameTest, SnmpCommunityTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, SnmpCommunityTableINDEX, 1, 0, 0, NULL},

{{11,SnmpCommunitySecurityName}, GetNextIndexSnmpCommunityTable, SnmpCommunitySecurityNameGet, SnmpCommunitySecurityNameSet, SnmpCommunitySecurityNameTest, SnmpCommunityTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, SnmpCommunityTableINDEX, 1, 0, 0, NULL},

{{11,SnmpCommunityContextEngineID}, GetNextIndexSnmpCommunityTable, SnmpCommunityContextEngineIDGet, SnmpCommunityContextEngineIDSet, SnmpCommunityContextEngineIDTest, SnmpCommunityTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, SnmpCommunityTableINDEX, 1, 0, 0, NULL},

{{11,SnmpCommunityContextName}, GetNextIndexSnmpCommunityTable, SnmpCommunityContextNameGet, SnmpCommunityContextNameSet, SnmpCommunityContextNameTest, SnmpCommunityTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, SnmpCommunityTableINDEX, 1, 0, 0, "0"},

{{11,SnmpCommunityTransportTag}, GetNextIndexSnmpCommunityTable, SnmpCommunityTransportTagGet, SnmpCommunityTransportTagSet, SnmpCommunityTransportTagTest, SnmpCommunityTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, SnmpCommunityTableINDEX, 1, 0, 0, "0"},

{{11,SnmpCommunityStorageType}, GetNextIndexSnmpCommunityTable, SnmpCommunityStorageTypeGet, SnmpCommunityStorageTypeSet, SnmpCommunityStorageTypeTest, SnmpCommunityTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, SnmpCommunityTableINDEX, 1, 0, 0, NULL},

{{11,SnmpCommunityStatus}, GetNextIndexSnmpCommunityTable, SnmpCommunityStatusGet, SnmpCommunityStatusSet, SnmpCommunityStatusTest, SnmpCommunityTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, SnmpCommunityTableINDEX, 1, 0, 1, NULL},

{{11,SnmpTargetAddrTMask}, GetNextIndexSnmpTargetAddrExtTable, SnmpTargetAddrTMaskGet, SnmpTargetAddrTMaskSet, SnmpTargetAddrTMaskTest, SnmpTargetAddrExtTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, SnmpTargetAddrExtTableINDEX, 1, 0, 0, "0"},

{{11,SnmpTargetAddrMMS}, GetNextIndexSnmpTargetAddrExtTable, SnmpTargetAddrMMSGet, SnmpTargetAddrMMSSet, SnmpTargetAddrMMSTest, SnmpTargetAddrExtTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, SnmpTargetAddrExtTableINDEX, 1, 0, 0, "484"},
};
tMibData snmcomEntry = { 10, snmcomMibEntry };
#endif /* _SNMCOMDB_H */

