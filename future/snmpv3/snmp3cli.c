/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: snmp3cli.c,v 1.56.2.1 2018/03/09 13:26:21 siva Exp $
 *
 * Description: Routines for mib db access 
 *******************************************************************/
#ifndef __SNMP3CLI_C__
#define __SNMP3CLI_C__

/* SOURCE FILE HEADER :
 *
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : snmp3cli.c                                       |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      : ISS TEAM                                         |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : SNMP3                                            |
 * |                                                                           |
 * |  MODULE NAME           : SNMP Interface                                   |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : Action routines for set/get objects in           |
 * |                          stdsncom.mib, stdvacm.mib, stdsmusm.mib,         |
 * |                          stdsntgt.mib, stdsnnot.mib, stdsnmpd.mib         |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 *
 */
#ifdef ECFM_WANTED
#include "ecfm.h"
#endif
#include "snmpcmn.h"
#include "snmputil.h"
#include "snmcomwr.h"
#include "snmcomlw.h"
#include "stdvacwr.h"
#include "stdvaclw.h"
#include "sntargwr.h"
#include "sntarglw.h"
#include "snmpnowr.h"
#include "snmpnolw.h"
#include "snmusmwr.h"
#include "snmusmlw.h"
#include "fssnmp3wr.h"
#include "fssnmp3lw.h"
#include "snmp3cli.h"
#include "fssocket.h"
#include "stdsnmwr.h"
#include "stdsnmlw.h"
#include "fssnmpcli.h"
#include "stdpoecli.h"
#include "stdvaccli.h"
#include "stdsnucli.h"
#include "stdsntcli.h"
#include "stdsnmpcli.h"
#include "snmproxlw.h"
#include "snmproxwr.h"
#include "stdsnpcli.h"
#include "stdsnncli.h"
#include "webnmutl.h"
#include "stdsnccli.h"
#ifdef   POE_WANTED
#include "poe.h"
#endif
#include "dhcp6.h"
#include "fips.h"

extern tMgrIpAddr   gManagerIpAddr;

extern tRBTree      gSnmpTrapFilterTable;

#ifdef EXT_CRYPTO_WANTED
extern UINT4        gu4ExtTrcFlag;
#endif

static INT1         Snmp3TargetAddrModifyError (tSNMP_OCTET_STRING_TYPE *
                                                pSnmpTargetAddrName);

static INT1         Snmp3TargetAddrCreateError (tSNMP_OCTET_STRING_TYPE *
                                                pTargetAddrName);

static INT1         Snmp3ViewModifyError (tSNMP_OCTET_STRING_TYPE * pViewName,
                                          tSNMP_OID_TYPE * pOid,
                                          tSNMP_OID_TYPE * pViewOid);

static INT1         Snmp3ViewCreateError (tSNMP_OCTET_STRING_TYPE * pViewName,
                                          tSNMP_OID_TYPE * pOid,
                                          tSNMP_OID_TYPE * pViewOid);

const CHR1         *au1Storage[] = {
    NULL,
    "Other",
    "Volatile",
    "Nonvolatile",
    "Permanent",
    "Read Only"
};

const CHR1         *au1ProxyType[] = {
    /*NULL, */
    "Read",
    "Write",
    "Trap",
    "Inform"
};

const CHR1         *au1RowStatus[] = {
    "Active",
    "NotInService",
    "NotReady",
    "CreateAndGo",
    "CreateAndWait",
    "Destroy"
};

const CHR1         *au1SecModel[] = {
    "v1",
    "v2c",
    "v3"
};

const CHR1         *au1SecLevel[] = {
    "No Authentication, No Privacy",
    "Authentication, No Privacy",
    "Authentication, Privacy"
};

const CHR1         *au1ContextMatch[] = {
    "Exact",
    "Prefix"
};

const CHR1         *au1NotificationType[] = {
    "trap",
    "inform"
};

const CHR1         *au1ViewType[] = {
    "Included",
    "Excluded"
};

const CHR1         *au1AuthProto[] = {
    "None",
    "MD5",
    "SHA",
    "SHA224",
    "SHA256",
    "SHA384",
    "SHA512"
};

const CHR1         *au1PrivProto[] = {
    "None",
    "DES_CBC",
    "TDES_CBC",
    "AES_CFB128",
    "AES_CFB192",
    "AES_CFB256",
    "AESCTR",
    "AESCTR192",
    "AESCTR256"
};

extern UINT4        au4StdPrivOid[];
extern UINT4        au4StdAuthOid[];

const CHR1         *au1NotifyFilterType[] = {
    "Included",
    "Excluded"
};

#define                   SNMP3_MAX_NAME_LEN     1024
#define                   SNMP3_BYTE_LEN         8
#define                   SNMP3_MAX_RETRY_COUNT  100

#define  SNMP_ALLOC_MULTIDATA(x) if((x=Snmp3CliAllocMultiData()) == NULL){return FAILURE;}

#define  SNMP_ALLOC_OID(x) if((x=Snmp3CliAllocOid()) == NULL){return FAILURE;}

tSNMP_MULTI_DATA_TYPE gSnmpMultiData[SNMP3_MAX_RETRY_COUNT];
UINT4               gu4SnmpMultiDataCount;
tSNMP_OCTET_STRING_TYPE gSnmpOctetString[SNMP3_MAX_RETRY_COUNT];
UINT1
     
                gau1SnmpOctetString[SNMP3_MAX_RETRY_COUNT][SNMP_MAX_OID_LENGTH];
UINT4               gu4SnmpOctetStrCount;
tSNMP_OID_TYPE      gSnmpOidType[SNMP3_MAX_RETRY_COUNT];
UINT4               gau4SnmpOidType[SNMP3_MAX_RETRY_COUNT][SNMP_MAX_OID_LENGTH];
UINT4               gu4SnmpOidCount;
CHR1                gau1NumberIdx[SNMP3_MAX_NAME_LEN];

VOID                Snmp3CliConvertDataToString (tSNMP_MULTI_DATA_TYPE * pData,
                                                 UINT1 *pu1String,
                                                 UINT1 u1Type);

tSNMP_OID_TYPE     *Snmp3CliAllocOid (VOID);

tSNMP_MULTI_DATA_TYPE *Snmp3CliAllocMultiData (VOID);

tSNMP_OCTET_STRING_TYPE *Snmp3CliAllocOctetString (VOID);

INT4
 
 
 
 
 
 
 
 Snmp3CliWalk (tCliHandle CliHandle, CHR1 * pOid, UINT1 u1Count, UINT1 u1Value);

INT4
cli_process_snmp3_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[SNMP3_MAX_ARGS];
    INT1                argno = 0;
    INT4                i4RetStatus = 0;
    UINT1               au1ContextName[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1None[SNMP_STD_OCTETSTRING_SIZE] = "none";
    UINT1               au1Default[SNMP_STD_OCTETSTRING_SIZE] = "";
    UINT1               au1Clear[SNMP_STD_OCTETSTRING_SIZE] = "clear";
    UINT1               au1NoView[SNMP_STD_OCTETSTRING_SIZE] = "noview";
    UINT1               au1ClearView[SNMP_STD_OCTETSTRING_SIZE] = "clearview";
    UINT1               au1ReadView[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1WriteView[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1NotifyView[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1Mask[SNMP_VIEW_MASK_MAX_STRING_LENGTH];
    UINT1               au1FilterMask[SNMP_FILTER_MASK_MAX_STRING_LENGTH];
    UINT1               au1AuthPasswd[SNMP_PASSWORD_SIZE];
    UINT1               au1PrivPasswd[SNMP_PASSWORD_SIZE];
    UINT1              *pu1TagList = NULL;
    UINT1               au1EngineId[SNMP_MAX_STR_ENGINEID_LEN];
    UINT1              *pu1TAddress = NULL;
    UINT4               u4ErrCode = 0;
    UINT4               u4ListenTrapPort = SNMP_MANAGER_PORT;
    UINT4               i4TcpStatus = SNMP3_OVER_TCP_DISABLE;
    UINT4               u4ListenAgentPort = SNMP_PORT;
    UINT2               u2Port = 0;

    va_start (ap, u4Command);

    /* third arguement is always interface name/index */
    va_arg (ap, UINT1 *);

    /* Walk through the rest of the arguements and store in args array.
     * Store 16 arguements at the max. This is because vlan commands do not
     * take more than sixteen inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give
     * second input. In that case first arg will be null and second arg only
     * has value */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == SNMP3_MAX_ARGS)
            break;
    }
    va_end (ap);

    switch (u4Command)
    {
        case CLI_SNMP3_PROXY_CREATE:
            /* args[0] - Proxy Name */
            /* args[1] - Proxy Type  */
            /* args[2] - Context Engine Id   */
            /* args[3] - Target ParamIn    */
            /* args[4] - TargetOut */
            /* args[5] - ContextName */
            /* args[6] - Storage Type */

            i4RetStatus =
                Snmp3CliProxyCreate (CliHandle, (UINT1 *) args[0],
                                     CLI_PTR_TO_U4 (args[1]), (UINT1 *) args[2],
                                     (UINT1 *) args[3], (UINT1 *) args[4],
                                     (UINT1 *) args[5],
                                     CLI_PTR_TO_U4 (args[6]));

            break;

        case CLI_SNMP3_PROP_PROXY_CREATE:
            /* args[0] - Prop Proxy Name */
            /* args[1] - Proxy Type  */
            /* args[2] - Mib Id   */
            /* args[3] - Target ParamIn    */
            /* args[4] - TargetOut */
            /* args[5] - Storage Type */

            i4RetStatus =
                Snmp3CliPropProxyCreate (CliHandle, (UINT1 *) args[0],
                                         CLI_PTR_TO_U4 (args[1]),
                                         (UINT1 *) args[2], (UINT1 *) args[3],
                                         (UINT1 *) args[4],
                                         CLI_PTR_TO_U4 (args[5]));
            break;

        case CLI_SNMP3_PROXY_NO_CREATE:
            i4RetStatus = Snmp3CliProxyRemove (CliHandle, (UINT1 *) args[0]);
            break;

        case CLI_SNMP3_PROP_PROXY_NO_CREATE:
            i4RetStatus =
                Snmp3CliPropProxyRemove (CliHandle, (UINT1 *) args[0]);
            break;

        case CLI_SNMP3_SHOW_PROXY:
            i4RetStatus = Snmp3CliShowProxy (CliHandle);
            break;

        case CLI_SNMP3_SHOW_PROP_PROXY:
            i4RetStatus = Snmp3CliShowPropProxy (CliHandle);
            break;

        case CLI_SNMP3_DEBUG:
            i4RetStatus =
                Snmp3CliSetTraceOption (CLI_PTR_TO_U4 (args[0]),
                                        SNMP3_TRC_ENABLED);
            break;

        case CLI_SNMP3_NO_DEBUG:
            i4RetStatus =
                Snmp3CliSetTraceOption (CLI_PTR_TO_U4 (args[0]),
                                        SNMP3_TRC_DISABLED);
            break;

        case CLI_SNMP3_AGENT_ENABLE:
            i4RetStatus = Snmp3CliAgentEnable (CliHandle);
            break;
        case CLI_SNMP3_AGENT_DISABLE:
            i4RetStatus = Snmp3CliAgentDisable (CliHandle);
            break;

        case CLI_SNMP3_COMMUNITY_CREATE:
            /* args[0] - Community Index */
            /* args[1] - Community Name  */
            /* args[2] - Security Name   */
            /* args[3] - Context Name    */
            /* args[4] - Transport Tag Identifier */
            /* args[5] - Storage Type */
            /* args[6] - Context EngineID */

            MEMSET (au1ContextName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
            pu1TagList = MemAllocMemBlk (gSnmpTagListPoolId);
            if (pu1TagList == NULL)
            {
                return CLI_FAILURE;
            }
            MEMSET (pu1TagList, 0, MAX_STR_OCTET_VAL + 1);
            MEMSET (au1EngineId, 0, SNMP_MAX_STR_ENGINEID_LEN);

            if (args[3] != NULL)
            {
                if (STRCMP (args[3], "none") == 0)
                {
                    MEMCPY (au1ContextName, au1Clear, STRLEN (au1Clear));
                }
                else
                {
                    STRNCPY (au1ContextName, args[3],
                             (sizeof (au1ContextName) - 1));
                }
            }
            else
            {
                MEMCPY (au1ContextName, au1None, STRLEN (au1None));
            }

            if (args[4] != NULL)
            {
                if (STRCMP (args[4], "none") == 0)
                {
                    MEMCPY (pu1TagList, au1Clear, STRLEN (au1Clear));
                }
                else
                {
                    STRNCPY (pu1TagList, args[4], (STRLEN ((UINT1 *) args[4])));
                }
            }
            else
            {
                MEMCPY (pu1TagList, au1None, STRLEN (au1None));
            }
            if (args[6] != NULL)
            {
                STRNCPY (au1EngineId, args[6], (sizeof (au1EngineId) - 1));
            }

            i4RetStatus = Snmp3CliCommunityConfig (CliHandle, (UINT1 *) args[0],
                                                   (UINT1 *) args[1],
                                                   (UINT1 *) args[2],
                                                   au1ContextName,
                                                   pu1TagList,
                                                   CLI_PTR_TO_U4 (args[5]),
                                                   au1EngineId);
            MemReleaseMemBlock (gSnmpTagListPoolId, pu1TagList);
            break;

        case CLI_SNMP3_COMMUNITY_NO_CREATE:
            /* args[0] - Community Index */

            i4RetStatus =
                Snmp3CliRemoveCommunity (CliHandle, (UINT1 *) args[0]);
            break;

        case CLI_SNMP3_GROUP_CREATE:
            /* args[0] - Group Name     */
            /* args[1] - User Name      */
            /* args[2] - Security Model */
            /* args[3] - Storage Type   */

            i4RetStatus = Snmp3CliGroupConfig (CliHandle, (UINT1 *) args[0],
                                               (UINT1 *) args[1],
                                               CLI_PTR_TO_I4 (args[2]),
                                               CLI_PTR_TO_U4 (args[3]));
            break;

        case CLI_SNMP3_GROUP_NO_CREATE:
            /* args[0] - Group Name     */
            /* args[1] - User Name      */
            /* args[2] - Security Model */

            i4RetStatus = Snmp3CliRemoveGroup (CliHandle, (UINT1 *) args[0],
                                               (UINT1 *) args[1],
                                               CLI_PTR_TO_I4 (args[2]));
            break;

        case CLI_SNMP3_ACCESS_CREATE:
            /* args[0] - Group Name      */
            /* args[1] - Security Model  */
            /* args[2] - Security Level  */
            /* args[3] - Read ViewName   */
            /* args[4] - Write ViewName  */
            /* args[5] - Notify ViewName */
            /* args[6] - Storage Type    */
            /* args[7] - Context Name    */

            MEMSET (au1ReadView, 0, SNMP_MAX_ADMIN_STR_LENGTH);
            MEMSET (au1WriteView, 0, SNMP_MAX_ADMIN_STR_LENGTH);
            MEMSET (au1NotifyView, 0, SNMP_MAX_ADMIN_STR_LENGTH);
            MEMSET (au1Default, 0, SNMP_STD_OCTETSTRING_SIZE);

            if (args[3] != NULL)
            {
                if (STRCMP (args[3], "none") == 0)
                {
                    MEMCPY (au1ReadView, au1ClearView, STRLEN (au1ClearView));
                }
                else
                {
                    STRNCPY (au1ReadView, args[3], (sizeof (au1ReadView) - 1));
                }
            }
            else
            {
                MEMCPY (au1ReadView, au1NoView, STRLEN (au1NoView));
            }

            if (args[4] != NULL)
            {
                if (STRCMP (args[4], "none") == 0)
                {
                    MEMCPY (au1WriteView, au1ClearView, STRLEN (au1ClearView));
                }
                else
                {
                    STRNCPY (au1WriteView, args[4],
                             (sizeof (au1WriteView) - 1));
                }
            }
            else
            {
                MEMCPY (au1WriteView, au1NoView, STRLEN (au1NoView));
            }

            if (args[5] != NULL)
            {
                if (STRCMP (args[5], "none") == 0)
                {
                    MEMCPY (au1NotifyView, au1ClearView, STRLEN (au1ClearView));
                }
                else
                {
                    STRNCPY (au1NotifyView, args[5],
                             (sizeof (au1NotifyView) - 1));
                }
            }
            else
            {
                MEMCPY (au1NotifyView, au1NoView, STRLEN (au1NoView));
            }

            if (args[7] != NULL)
            {
                STRNCPY (au1Default, args[7], (sizeof (au1Default) - 1));
            }

            i4RetStatus = Snmp3CliAccessConfig (CliHandle, (UINT1 *) args[0],
                                                au1Default,
                                                CLI_PTR_TO_I4 (args[2]),
                                                CLI_PTR_TO_I4 (args[1]),
                                                au1ReadView, au1WriteView,
                                                au1NotifyView,
                                                CLI_PTR_TO_U4 (args[6]));
            break;

        case CLI_SNMP3_ACCESS_NO_CREATE:
            /* args[0] - Group Name      */
            /* args[1] - Security Model  */
            /* args[2] - Security Level  */
            /* args[3] - Context Name    */

            MEMSET (au1Default, 0, SNMP_STD_OCTETSTRING_SIZE);
            if (args[3] != NULL)
            {
                STRNCPY (au1Default, args[3], (sizeof (au1Default) - 1));
            }
            else
            {
                STRCPY (au1Default, "");
            }
            i4RetStatus = Snmp3CliAccessDelete (CliHandle, (UINT1 *) args[0],
                                                au1Default,
                                                CLI_PTR_TO_I4 (args[2]),
                                                CLI_PTR_TO_I4 (args[1]));
            break;

        case CLI_SNMP3_ENGINE_ID:
            /* args[0] - Engine Identifier */

            i4RetStatus = Snmp3CliSetEngineId (CliHandle, (UINT1 *) args[0]);
            break;

        case CLI_SNMP3_ENGINE_NO_ID:

            i4RetStatus = Snmp3CliReSetEngineId (CliHandle);
            break;

        case CLI_SNMP3_VIEW_CREATE:
            /* args[0] - ViewTree Name */
            /* args[1] - OID Tree      */
            /* args[2] - Mask          */
            /* args[3] - Mask Type     */
            /* args[4] - Storage Type  */

            MEMSET (au1Mask, 0, SNMP_VIEW_MASK_MAX_STRING_LENGTH);

            if (args[2] != NULL)
            {
                STRNCPY (au1Mask, args[2], (sizeof (au1Mask) - 1));
            }
            else
            {
                MEMCPY (au1Mask, au1None, STRLEN (au1None));
            }

            i4RetStatus = Snmp3CliViewConfig (CliHandle, (UINT1 *) args[0],
                                              (UINT1 *) args[1],
                                              au1Mask, CLI_PTR_TO_U4 (args[3]),
                                              CLI_PTR_TO_U4 (args[4]));
            break;

        case CLI_SNMP3_VIEW_NO_CREATE:
            /* args[0] - ViewTree Name */
            /* args[1] - OID Tree      */

            i4RetStatus = Snmp3CliViewDelete (CliHandle, (UINT1 *) args[0],
                                              (UINT1 *) args[1]);
            break;

        case CLI_SNMP3_TRGTADDR_CREATE:
            /* args[0] - Address Name      */
            /* args[1] - Param Name        */
            /* args[2] - Target IP Address */
            /* args[3] - TimeOut Value     */
            /* args[4] - Retry Count       */
            /* args[5] - Tag List          */
            /* args[6] - Storage Type      */
            /* args[7] - Target Port       */
            /* args[8] - IP Address Type   */

            pu1TagList = MemAllocMemBlk (gSnmpTagListPoolId);
            if (pu1TagList == NULL)
            {
                return CLI_FAILURE;
            }
            MEMSET (pu1TagList, 0, MAX_STR_OCTET_VAL + 1);

            if (args[5] != NULL)
            {
                if (STRCMP (args[5], "none") == 0)
                {
                    MEMCPY (pu1TagList, au1Clear, STRLEN (au1Clear));
                }
                else
                {
                    STRNCPY (pu1TagList, args[5], (STRLEN ((UINT1 *) args[5])));
                }
            }
            else
            {
                MEMCPY (pu1TagList, au1None, STRLEN (au1None));
            }

            pu1TAddress = MemAllocMemBlk (gSnmpOctetListPoolId);
            if (pu1TAddress == NULL)
            {
                MemReleaseMemBlock (gSnmpTagListPoolId, pu1TagList);
                return CLI_FAILURE;
            }
            MEMSET (pu1TAddress, 0, sizeof (tSnmpOctetListBlock));

            u2Port = (UINT2) CLI_PTR_TO_U4 (args[7]);

            if ((CLI_PTR_TO_U4 (args[8])) == SNMP_IP_V4)
            {
                /* copy the IpAddress */
                MEMCPY (pu1TAddress, args[2], SNMP_IPADDR_LEN);
                /* copy the port */

                MEMCPY (pu1TAddress + SNMP_IPADDR_LEN, &u2Port, SNMP_TWO);
            }
            else if ((CLI_PTR_TO_U4 (args[8])) == SNMP_IP_V6)
            {
                MEMCPY (pu1TAddress, args[2], SNMP_IP6ADDR_LEN);
            }
            else
            {
                STRNCPY (pu1TAddress, args[2], (STRLEN (args[2]) + 1));
                *pu1TAddress = UtilStrToLower ((UINT1 *) pu1TAddress);
            }

            i4RetStatus =
                Snmp3CliTargetAddrConfig (CliHandle, (UINT1 *) args[0],
                                          pu1TAddress, (UINT1 *) args[1],
                                          pu1TagList, CLI_PTR_TO_U4 (args[3]),
                                          CLI_PTR_TO_U4 (args[4]),
                                          CLI_PTR_TO_U4 (args[6]),
                                          CLI_PTR_TO_U4 (args[8]), u2Port);

            MemReleaseMemBlock (gSnmpTagListPoolId, pu1TagList);
            MemReleaseMemBlock (gSnmpOctetListPoolId, pu1TAddress);
            break;

        case CLI_SNMP3_TRGTADDR_NO_CREATE:
            /* args[0] - Address Name      */

            i4RetStatus = Snmp3CliTargetAddrDelete (CliHandle,
                                                    (UINT1 *) args[0]);
            break;

        case CLI_SNMP3_TRGTPARAMS_CREATE:
            /* args[0] - Param Name     */
            /* args[1] - User Name      */
            /* args[2] - Security Model */
            /* args[3] - Security Level */
            /* args[4] - Message Processing Model */
            /* args[5] - Storage Type   */
            /* args[6] - Filter Profile Name   */
            /* args[7] - Filter Storage Type   */

            i4RetStatus = Snmp3CliTargetParamConfig (CliHandle,
                                                     (UINT1 *) args[0],
                                                     CLI_PTR_TO_I4 (args[4]),
                                                     CLI_PTR_TO_I4 (args[2]),
                                                     (UINT1 *) args[1],
                                                     CLI_PTR_TO_I4 (args[3]),
                                                     CLI_PTR_TO_U4 (args[5]),
                                                     (UINT1 *) args[6],
                                                     CLI_PTR_TO_U4 (args[7]));
            break;

        case CLI_SNMP3_TRGTPARAMS_NO_CREATE:
            /* args[0] - Param Name */

            i4RetStatus = Snmp3CliTargetParamDelete (CliHandle,
                                                     (UINT1 *) args[0]);
            break;

        case CLI_SNMP3_NOTIFY_FILTER_CREATE:
            /* args[0] - Filter Name */
            /* args[1] - OID Tree      */
            /* args[2] - Mask          */
            /* args[3] - Filter Type Type     */
            /* args[4] - Storage Type  */

            MEMSET (au1FilterMask, 0, SNMP_FILTER_MASK_MAX_STRING_LENGTH);

            if (args[2] != NULL)
            {
                STRNCPY (au1FilterMask, args[2], (sizeof (au1FilterMask) - 1));
            }
            else
            {
                MEMCPY (au1FilterMask, au1None, STRLEN (au1None));
            }

            i4RetStatus = Snmp3CliNotifyFilterConfig (CliHandle,
                                                      (UINT1 *) args[0],
                                                      (UINT1 *) args[1],
                                                      au1FilterMask,
                                                      CLI_PTR_TO_U4 (args[3]),
                                                      CLI_PTR_TO_U4 (args[4]));
            break;
        case CLI_SNMP3_NOTIFY_FILTER_NO_CREATE:
            /* args[0] - Filter Name */
            /* args[1] - OID Tree      */

            i4RetStatus = Snmp3CliNotifyFilterDelete (CliHandle,
                                                      (UINT1 *) args[0],
                                                      (UINT1 *) args[1]);
            break;

        case CLI_SNMP3_USER_CREATE:

            /* args[0] - User Name  */
            /* args[1] - Authentication Type */
            /* args[2] - Password      */
            /* args[3] - Priv Protocol Type */
            /* args[4] - Priv Password */
            /* args[5] - Storage Type   */
            /* args[6] - Engine ID */

            MEMSET (au1AuthPasswd, 0, SNMP_PASSWORD_SIZE);
            MEMSET (au1PrivPasswd, 0, SNMP_PASSWORD_SIZE);
            MEMSET (au1EngineId, 0, SNMP_MAX_STR_ENGINEID_LEN);

            if (args[2] != NULL)
            {
                if ((STRLEN ((UINT1 *) args[2])) > (sizeof (au1AuthPasswd) - 1))
                {
                    CLI_SET_ERR (CLI_SNMP3_INVALID_INPUT_ERR);
                }
                else
                {
                    STRNCPY (au1AuthPasswd, args[2],
                             (sizeof (au1AuthPasswd) - 1));
                }
            }
            else
            {
                STRCPY (au1AuthPasswd, "");
            }

            if (args[4] != NULL)
            {
                STRNCPY (au1PrivPasswd, args[4], (sizeof (au1PrivPasswd) - 1));
            }
            else
            {
                STRCPY (au1PrivPasswd, "");
            }
            if (args[6] != NULL)
            {
                STRNCPY (au1EngineId, args[6], (sizeof (au1EngineId) - 1));
            }

            i4RetStatus = Snmp3CliUsmConfig (CliHandle, (UINT1 *) args[0],
                                             CLI_PTR_TO_I4 (args[1]),
                                             au1AuthPasswd,
                                             CLI_PTR_TO_I4 (args[3]),
                                             au1PrivPasswd,
                                             CLI_PTR_TO_U4 (args[5]),
                                             au1EngineId,
                                             CLI_PTR_TO_U4 (args[7]));
            break;

        case CLI_SNMP3_USER_NO_CREATE:
            /* args[0] - User Name  */
            MEMSET (au1EngineId, 0, SNMP_MAX_STR_ENGINEID_LEN);
            if (args[1] != NULL)
            {
                STRNCPY (au1EngineId, args[1], (sizeof (au1EngineId) - 1));
            }

            i4RetStatus = Snmp3CliUsmDelete (CliHandle, (UINT1 *) args[0],
                                             au1EngineId);
            break;

        case CLI_SNMP3_NOTIF_CREATE:
            /* args[0] - Notification Name */
            /* args[1] - Notification Tag  */
            /* args[2] - Notification Type */
            /* args[3] - Storage Type      */

            pu1TagList = MemAllocMemBlk (gSnmpTagListPoolId);
            if (pu1TagList == NULL)
            {
                return CLI_FAILURE;
            }
            MEMSET (pu1TagList, 0, MAX_STR_OCTET_VAL + 1);

            if (STRCMP (args[1], "none") == 0)
            {
                MEMCPY (pu1TagList, au1Clear, STRLEN (au1Clear));
            }
            else
            {
                STRNCPY (pu1TagList, args[1], (STRLEN ((UINT1 *) args[1])));
            }

            i4RetStatus = Snmp3CliNotifyConfig (CliHandle, (UINT1 *) args[0],
                                                pu1TagList,
                                                CLI_PTR_TO_U4 (args[2]),
                                                CLI_PTR_TO_U4 (args[3]));
            MemReleaseMemBlock (gSnmpTagListPoolId, pu1TagList);
            break;

        case CLI_SNMP3_NOTIF_NO_CREATE:
            /* args[0] - Notification Name */

            i4RetStatus = Snmp3CliNotifyDelete (CliHandle, (UINT1 *) args[0]);
            break;

        case CLI_SNMP3_LISTEN_TRAP_PORT:
            u4ListenTrapPort = *args[0];
            i4RetStatus = Snmp3CliSetListenTrapPort (CliHandle,
                                                     (UINT2) u4ListenTrapPort);
            break;

        case CLI_SNMP3_NO_LISTEN_TRAP_PORT:
            i4RetStatus =
                Snmp3CliSetListenTrapPort (CliHandle, SNMP_MANAGER_PORT);
            break;

        case CLI_SNMP3_SHOW_PROXY_LISTEN_TRAP_PORT:
            i4RetStatus = Snmp3CliShowProxyListenTrapPort (CliHandle);
            break;

        case CLI_SNMP3_PROXY_LISTEN_TRAP_PORT:
            u4ListenTrapPort = *args[0];
            i4RetStatus =
                Snmp3CliSetProxyListenTrapPort (CliHandle, u4ListenTrapPort);
            break;
        case CLI_SNMP3_NO_PROXY_LISTEN_TRAP_PORT:
            i4RetStatus =
                Snmp3CliSetProxyListenTrapPort (CliHandle, SNMP_MANAGER_PORT);
            break;

        case CLI_SNMP3_TCP_ENABLE:
            i4TcpStatus = SNMP3_OVER_TCP_ENABLE;
            i4RetStatus = Snmp3CliSetTcpStatus (CliHandle, i4TcpStatus);
            break;

        case CLI_SNMP3_NO_TCP_ENABLE:
            i4TcpStatus = SNMP3_OVER_TCP_DISABLE;
            i4RetStatus = Snmp3CliSetTcpStatus (CliHandle, i4TcpStatus);
            break;

        case CLI_SNMP3_TCP_TRAP_ENABLE:
            i4TcpStatus = SNMP3_TRAP_OVER_TCP_ENABLE;
            i4RetStatus = Snmp3CliSetTcpTrapStatus (CliHandle, i4TcpStatus);
            break;

        case CLI_SNMP3_NO_TCP_TRAP_ENABLE:
            i4TcpStatus = SNMP3_TRAP_OVER_TCP_DISABLE;
            i4RetStatus = Snmp3CliSetTcpTrapStatus (CliHandle, i4TcpStatus);
            break;

        case CLI_SNMP3_LISTEN_TCP_PORT:
            /*u4ListenTrapPort = CLI_PTR_TO_U4(args[0]); */
            i4RetStatus = Snmp3CliSetListenTcpPort (CliHandle,
                                                    *(UINT2 *) args[0]);
            break;

        case CLI_SNMP3_NO_LISTEN_TCP_PORT:
            i4RetStatus = Snmp3CliSetListenTcpPort (CliHandle, SNMP_PORT);
            break;

        case CLI_SNMP3_LISTEN_TCP_TRAP_PORT:
            /*u4ListenTrapPort = CLI_PTR_TO_U4(args[0]); */
            i4RetStatus = Snmp3CliSetListenTcpTrapPort (CliHandle,
                                                        *(UINT2 *) args[0]);
            break;

        case CLI_SNMP3_NO_LISTEN_TCP_TRAP_PORT:
            i4RetStatus =
                Snmp3CliSetListenTcpTrapPort (CliHandle,
                                              (UINT2) SNMP_MANAGER_PORT);
            break;

        case CLI_SNMP3_SHOW_SNMP_TCP:
            i4RetStatus = Snmp3CliShowSnmpTcpDetails (CliHandle);
            break;
        case CLI_SNMP3_SHOW_DETAILS:

            i4RetStatus = Snmp3CliShowDetails (CliHandle);
            break;

        case CLI_SNMP3_SHOW_COMMUNITY:

            i4RetStatus = Snmp3CliShowCommunity (CliHandle);
            break;

        case CLI_SNMP3_SHOW_GROUP:

            i4RetStatus = Snmp3CliShowGroup (CliHandle);
            break;

        case CLI_SNMP3_SHOW_ACCESS:

            i4RetStatus = Snmp3CliShowAccessGroup (CliHandle);
            break;

        case CLI_SNMP3_SHOW_ENGINEID:

            i4RetStatus = Snmp3CliShowEngineId (CliHandle);
            break;

        case CLI_SNMP3_SHOW_VIEWTREE:

            i4RetStatus = Snmp3CliShowViewTree (CliHandle);
            break;

        case CLI_SNMP3_SHOW_TRGTADDR:

            i4RetStatus = Snmp3CliShowTargetAddr (CliHandle);
            break;

        case CLI_SNMP3_SHOW_TRGTPARAM:

            i4RetStatus = Snmp3CliShowTargetParam (CliHandle);
            break;

        case CLI_SNMP3_SHOW_USER:

            i4RetStatus = Snmp3CliShowUser (CliHandle);
            break;

        case CLI_SNMP3_SHOW_NOTIF:

            i4RetStatus = Snmp3CliShowNotification (CliHandle);
            break;

        case CLI_SNMP3_AUTHTRAP_STATUS:

            i4RetStatus = Snmp3CliSetAuthTrapStatus (CliHandle,
                                                     CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_SNMP3_ENABLE_TRAP:
            i4RetStatus = Snmp3CliSetTraps (CliHandle, CLI_PTR_TO_U4 (args[0]),
                                            CLI_TRAP_ENABLE);
            break;

        case CLI_SNMP3_DISABLE_TRAP:
            i4RetStatus = Snmp3CliSetTraps (CliHandle, CLI_PTR_TO_U4 (args[0]),
                                            CLI_TRAP_DISABLE);
            break;

        case CLI_SNMP3_SYSTEM_CONTACT:
            i4RetStatus =
                Snmp3CliSetSystemContact (CliHandle, (UINT1 *) args[0]);
            break;

        case CLI_SNMP3_SYSTEM_LOCATION:
            i4RetStatus = Snmp3CliSetSystemLocation (CliHandle,
                                                     (UINT1 *) args[0]);
            break;

        case CLI_SNMP3_SHOW_INFMSTAT:

            i4RetStatus = Snmp3CliShowInformStats (CliHandle);
            break;

        case CLI_SNMP3_SHOW_TRAPS:
            i4RetStatus = Snmp3CliShowTraps (CliHandle);
            break;

        case CLI_SNMP3_AGENT_PORT:
            u4ListenAgentPort = *args[0];
            i4RetStatus = Snmp3CliSetListenAgentPort (CliHandle,
                                                      u4ListenAgentPort);
            break;
        case CLI_SNMP3_SHOW_NOTIFY_FILTER:

            i4RetStatus = Snmp3CliShowNotifyFilter (CliHandle);
            break;

        case CLI_SNMP3_NAME_TO_OID:
            i4RetStatus = Snmp3CliGetOidFromName (CliHandle, (CHR1 *) args[0]);
            break;

        case CLI_SNMP3_OID_TO_NAME:
            i4RetStatus = Snmp3CliGetNameFromOid (CliHandle, (CHR1 *) args[0]);
            break;

        case CLI_SNMP3_SET_NAME_VALUE:
            i4RetStatus = Snmp3CliSetValueForName (CliHandle,
                                                   (CHR1 *) args[0],
                                                   (UINT1 *) args[1],
                                                   (UINT1)
                                                   CLI_PTR_TO_U4 (args[2]));
            break;

        case CLI_SNMP3_SET_OID_VALUE:
            i4RetStatus = Snmp3CliSetValueForOid (CliHandle, (CHR1 *) args[0],
                                                  (UINT1 *) args[1],
                                                  (UINT1)
                                                  CLI_PTR_TO_U4 (args[2]));
            break;

        case CLI_SNMP3_GET_NAME:
            i4RetStatus =
                Snmp3CliGetName (CliHandle, (CHR1 *) args[0],
                                 (UINT1) CLI_PTR_TO_U4 (args[1]));
            break;

        case CLI_SNMP3_GET_OID:
            i4RetStatus =
                Snmp3CliGetOid (CliHandle, (CHR1 *) args[0],
                                (UINT1) CLI_PTR_TO_U4 (args[1]));
            break;

        case CLI_SNMP3_GETNEXT_NAME:
            i4RetStatus =
                Snmp3CliGetNextName (CliHandle, (CHR1 *) args[0],
                                     (UINT1) CLI_PTR_TO_U4 (args[1]));
            break;

        case CLI_SNMP3_GETNEXT_OID:
            i4RetStatus =
                Snmp3CliGetNextOid (CliHandle, (CHR1 *) args[0],
                                    (UINT1) CLI_PTR_TO_U4 (args[1]));
            break;

        case CLI_SNMP3_NAME_WALK:
            if (args[1] != NULL)
            {
                i4RetStatus =
                    Snmp3CliNameWalk (CliHandle, (CHR1 *) args[0],
                                      (UINT1) *(args[1]),
                                      (UINT1) CLI_PTR_TO_U4 (args[2]));

            }
            else
            {
                i4RetStatus =
                    Snmp3CliNameWalk (CliHandle, (CHR1 *) args[0],
                                      (UINT1) CLI_PTR_TO_U4 (args[1]),
                                      (UINT1) CLI_PTR_TO_U4 (args[2]));
            }

            break;

        case CLI_SNMP3_OID_WALK:
            if (args[1] != NULL)
            {
                i4RetStatus =
                    Snmp3CliOidWalk (CliHandle, (CHR1 *) args[0],
                                     (UINT1) *(args[1]),
                                     (UINT1) CLI_PTR_TO_U4 (args[2]));

            }
            else
            {
                i4RetStatus =
                    Snmp3CliOidWalk (CliHandle, (CHR1 *) args[0],
                                     (UINT1) CLI_PTR_TO_U4 (args[1]),
                                     (UINT1) CLI_PTR_TO_U4 (args[2]));
            }

            break;

        case CLI_SNMP3_SET_TRAP_FILTER_NAME:
            i4RetStatus =
                Snmp3CliSetTrapFilterName (CliHandle, (CHR1 *) args[0],
                                           CLI_TRAP_ENABLE);
            break;

        case CLI_SNMP3_SET_TRAP_FILTER_OID:
            i4RetStatus =
                Snmp3CliSetTrapFilterOid (CliHandle, (CHR1 *) args[0],
                                          CLI_TRAP_ENABLE);
            break;

        case CLI_SNMP3_SET_NO_TRAP_FILTER_NAME:
            i4RetStatus =
                Snmp3CliSetTrapFilterName (CliHandle, (CHR1 *) args[0],
                                           CLI_TRAP_DISABLE);
            break;

        case CLI_SNMP3_SET_NO_TRAP_FILTER_OID:
            i4RetStatus =
                Snmp3CliSetTrapFilterOid (CliHandle, (CHR1 *) args[0],
                                          CLI_TRAP_DISABLE);
            break;

        default:
            CliPrintf (CliHandle, "%%Unknown command \r\n");
            return CLI_FAILURE;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_SNMP3_MAX_ERR))
        {
            CliPrintf (CliHandle, "%s", Snmp3CliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  Snmp3CliSetTraceOption
 * Description :  This function configues the trace option for Snmp module
 *
 * Input       :
 *                i4TraceInput - Trace level
 *                i4Action - Enabled / Disabled Trace
 *
 * Output      :  None
 *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT4
Snmp3CliSetTraceOption (UINT4 u4TraceInput, UINT4 u4Action)
{
    switch (u4Action)
    {
        case SNMP3_TRC_ENABLED:
            if (gu4SnmpTrcEvent == u4TraceInput)
            {
                return CLI_SUCCESS;
            }
            gu4SnmpTrcEvent = gu4SnmpTrcEvent | u4TraceInput;
#ifdef EXT_CRYPTO_WANTED
            gu4ExtTrcFlag = gu4ExtTrcFlag | u4TraceInput;
#endif
            break;
        case SNMP3_TRC_DISABLED:
            gu4SnmpTrcEvent = gu4SnmpTrcEvent & u4TraceInput;
#ifdef EXT_CRYPTO_WANTED
            gu4ExtTrcFlag = gu4ExtTrcFlag & u4TraceInput;
#endif
            break;
        default:
            return CLI_FAILURE;
            break;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliAgentEnable                                */
/*                                                                           */
/*     DESCRIPTION      : Function to enables the Snmp Agent                 */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliAgentEnable (tCliHandle CliHandle)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2SnmpAgentControl (&u4ErrorCode, SNMP_AGENT_ENABLE)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetSnmpAgentControl (SNMP_AGENT_ENABLE) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliAgentDisable                               */
/*                                                                           */
/*     DESCRIPTION      : Function to disables the Snmp Agent                */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliAgentDisable (tCliHandle CliHandle)
{
    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);    /*kloc */

    if (nmhTestv2SnmpAgentControl (&u4ErrorCode, SNMP_AGENT_DISABLE)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetSnmpAgentControl (SNMP_AGENT_DISABLE) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliCommunityConfig                            */
/*                                                                           */
/*     DESCRIPTION      : Function to configure the Community Table          */
/*                                                                           */
/*     INPUT            : pu1CommIndex   - Community Index                   */
/*                        pu1CommName    - Community Name                    */
/*                        pu1SecName     - Security string                   */
/*                        pu1ContextName - View identifier                   */
/*                        pu1Tag         - Transport tag identifier          */
/*                        u4StorageType  - Storage type in memory            */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliCommunityConfig (tCliHandle CliHandle, UINT1 *pu1CommIndex,
                         UINT1 *pu1CommName, UINT1 *pu1SecName,
                         UINT1 *pu1ContextName, UINT1 *pu1Tag,
                         UINT4 u4StorageType, UINT1 *pu1ContextEngineId)
{
    UINT4               u4ErrCode = 0;
    UINT4               u4NumOctets = 0;

    if (STRCMP (pu1ContextEngineId, "") != 0)
    {
        u4NumOctets = SnmpGetNumOctets ((INT1 *) pu1ContextEngineId);
        if ((u4NumOctets < SNMP_MIN_ENGINE_ID_LEN) ||
            (u4NumOctets > SNMP_MAX_ENGINE_ID_LEN))
        {
            CliPrintf (CliHandle,
                       "\r%% Valid ContextEngineID Length is in the range 5-32 octets\r\n");
            return CLI_FAILURE;
        }
    }

    if (Snmp3CommunityConfig (pu1CommIndex, pu1CommName, pu1SecName,
                              pu1ContextName, pu1Tag, u4StorageType,
                              pu1ContextEngineId, &u4ErrCode) == SNMP_FAILURE)
    {
        CLI_SET_ERR (u4ErrCode);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliRemoveCommunity                            */
/*                                                                           */
/*     DESCRIPTION      : Function to remove the Community Entry             */
/*                                                                           */
/*     INPUT            : pu1CommIndex   - Community Index                   */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliRemoveCommunity (tCliHandle CliHandle, UINT1 *pu1CommIndex)
{
    UINT1               au1CommunityIndex[SNMP_MAX_ADMIN_STR_LENGTH];

    tSNMP_OCTET_STRING_TYPE CommIndex;

    UNUSED_PARAM (CliHandle);

    /* verify validity of input */
    if (SnmpCheckString (pu1CommIndex) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_SNMP3_INVALID_INPUT_ERR);
        return CLI_FAILURE;
    }

    MEMSET (au1CommunityIndex, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    STRNCPY (au1CommunityIndex, pu1CommIndex, (sizeof (au1CommunityIndex) - 1));

    CommIndex.pu1_OctetList = au1CommunityIndex;
    CommIndex.i4_Length = (INT4) STRLEN (au1CommunityIndex);

    /* entry found - Delete */
    if (nmhSetSnmpCommunityStatus (&CommIndex, SNMP_ROWSTATUS_DESTROY)
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_SNMP3_COMMUNITY_NOTPRESENT_ERR);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliProxyCreate                                */
/*                                                                           */
/*     DESCRIPTION      : Function to create entry in Proxy Table            */
/*                                                                           */
/*     INPUT            : pu1GroupName  -  Group Name                        */
/*                        pu1SecName    -  Security Name                     */
/*                        i4SecModel    -  Security Model                    */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliProxyCreate (tCliHandle CliHandle, UINT1 *pu1ProxyName,
                     UINT4 u4ProxyType, UINT1 *pu1ContextEngineId,
                     UINT1 *pu1TargetParamIn, UINT1 *pu1TargetOut,
                     UINT1 *pu1ContextName, UINT4 u4StorageType)
{
    UINT4               u4ErrCode = 0;
    UINT4               u4NumOctets = 0;

    u4NumOctets = SnmpGetNumOctets ((INT1 *) pu1ContextEngineId);
    if ((u4NumOctets < SNMP_MIN_ENGINE_ID_LEN) ||
        (u4NumOctets > SNMP_MAX_ENGINE_ID_LEN))
    {
        CliPrintf (CliHandle,
                   "\r%% Valid ContextEngineID Length is in the range 5-32 octets\r\n");
        return CLI_FAILURE;
    }

    if (Snmp3ProxyConfig (pu1ProxyName, u4ProxyType, pu1ContextEngineId,
                          pu1TargetParamIn, pu1TargetOut,
                          pu1ContextName, u4StorageType,
                          &u4ErrCode) == SNMP_FAILURE)
    {
        CLI_SET_ERR (u4ErrCode);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliPropProxyCreate                            */
/*                                                                           */
/*     DESCRIPTION      : Function to create entry in Proxy Table            */
/*                                                                           */
/*     INPUT            : pu1ProxyName   -  Proxy Name                       */
/*                        u4ProxyType    -  Proxy Type                       */
/*                        pu1MibId       -  Mib Id                           */
/*                        pu1TargetParamIn- TargetParamIn                    */
/*                        pu1TargetOut    - TargetOut                        */
/*                        u4StorageType   - StorageType                      */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/******************************************************************************/

INT4
Snmp3CliPropProxyCreate (tCliHandle CliHandle, UINT1 *pu1ProxyName,
                         UINT4 u4ProxyType, UINT1 *pu1MibId,
                         UINT1 *pu1TargetParamIn, UINT1 *pu1TargetOut,
                         UINT4 u4StorageType)
{
    UINT4               u4ErrCode = 0;
    UNUSED_PARAM (CliHandle);
    if (Snmp3PropProxyConfig (pu1ProxyName, u4ProxyType, pu1MibId,
                              pu1TargetParamIn, pu1TargetOut,
                              u4StorageType, &u4ErrCode) == SNMP_FAILURE)
    {
        CLI_SET_ERR (u4ErrCode);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliProxyRemove                                */
/*                                                                           */
/*     DESCRIPTION      : Function to delete Proxy entry from Proxy Table    */
/*                                                                           */
/*     INPUT            : pu1ProxyName -   Proxy Name                        */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
Snmp3CliProxyRemove (tCliHandle CliHandle, UINT1 *pu1ProxyName)
{
    UINT1               au1ProxyName[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT4               u4ErrCode = 0;

    tSNMP_OCTET_STRING_TYPE ProxyName;

    UNUSED_PARAM (CliHandle);

    MEMSET (au1ProxyName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    STRNCPY (au1ProxyName, pu1ProxyName, (sizeof (au1ProxyName) - 1));

    ProxyName.pu1_OctetList = au1ProxyName;
    ProxyName.i4_Length = (INT4) STRLEN (au1ProxyName);

    /* Delete entry */

    if (nmhTestv2SnmpProxyRowStatus
        (&u4ErrCode, &ProxyName, (INT4) SNMP_ROWSTATUS_DESTROY) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_SNMP3_PROXY_NOTPRESENT_ERR);
        return CLI_FAILURE;
    }

    if (nmhSetSnmpProxyRowStatus (&ProxyName, (INT4) SNMP_ROWSTATUS_DESTROY) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_SNMP3_PROXY_NOTPRESENT_ERR);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliPropProxyRemove                                */
/*                                                                           */
/*     DESCRIPTION      : Function to delete Proxy entry from Proxy Table    */
/*                                                                           */
/*     INPUT            : pu1ProxyName -   Proxy Name                        */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
Snmp3CliPropProxyRemove (tCliHandle CliHandle, UINT1 *pu1PropProxyName)
{
    UINT1               au1ProxyName[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT4               u4ErrCode = 0;

    tSNMP_OCTET_STRING_TYPE ProxyName;

    UNUSED_PARAM (CliHandle);

    MEMSET (au1ProxyName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    STRNCPY (au1ProxyName, pu1PropProxyName, (sizeof (au1ProxyName) - 1));

    ProxyName.pu1_OctetList = au1ProxyName;
    ProxyName.i4_Length = (INT4) STRLEN (au1ProxyName);

    /* Delete entry */
    if (nmhTestv2FsSnmpProxyMibRowStatus
        (&u4ErrCode, &ProxyName, (INT4) SNMP_ROWSTATUS_DESTROY) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_SNMP3_MIB_PROXY_NOTPRESENT_ERR);
        return CLI_FAILURE;
    }

    if (nmhSetFsSnmpProxyMibRowStatus
        (&ProxyName, (INT4) SNMP_ROWSTATUS_DESTROY) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_SNMP3_MIB_PROXY_NOTPRESENT_ERR);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliShowProxy                                  */
/*                                                                           */
/*     DESCRIPTION      : Function to show entry from Proxy Table            */
/*                                                                           */
/*     INPUT            : CliHandle     -  CliHandle                         */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliShowProxy (tCliHandle CliHandle)
{

    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT1                ai1Name[SNMP_MAX_OCTETSTRING_SIZE - 1];

    tProxyTableEntry   *pProxyEntry = NULL;

    if ((pProxyEntry = SNMPGetFirstProxy ()) == NULL)
    {
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r\n");
    do
    {
        SNPRINTF ((CHR1 *) ai1Name,
                  ((UINT4) (pProxyEntry->ProxyName.i4_Length + 1)),
                  (const CHR1 *) pProxyEntry->ProxyName.pu1_OctetList);
        CliPrintf (CliHandle, "Proxy Name                : %s \r\n", ai1Name);

        MEMSET (ai1Name, 0, SNMP_MAX_OCTETSTRING_SIZE - 1);
        SnmpConvertOctetToString (&pProxyEntry->PrxContextEngineID, ai1Name);
        CliPrintf (CliHandle, "Proxy ContextEngineID     : %s \r\n", ai1Name);

        SNPRINTF ((CHR1 *) ai1Name,
                  ((UINT4) (pProxyEntry->PrxContextName.i4_Length + 1)),
                  (const CHR1 *) pProxyEntry->PrxContextName.pu1_OctetList);
        CliPrintf (CliHandle, "Proxy ContextName         : %s \r\n", ai1Name);

        SNPRINTF ((CHR1 *) ai1Name,
                  (UINT4) ((pProxyEntry->PrxTargetParamsIn.i4_Length + 1)),
                  (const CHR1 *) pProxyEntry->PrxTargetParamsIn.pu1_OctetList);
        CliPrintf (CliHandle, "Proxy TargetParamIn       : %s \r\n", ai1Name);

        SNPRINTF ((CHR1 *) ai1Name,
                  (UINT4) ((pProxyEntry->PrxSingleTargetOut.i4_Length + 1)),
                  (const CHR1 *) pProxyEntry->PrxSingleTargetOut.pu1_OctetList);
        CliPrintf (CliHandle, "Proxy SingleTargetOut     : %s \r\n", ai1Name);

        SNPRINTF ((CHR1 *) ai1Name,
                  (UINT4) ((pProxyEntry->PrxMultipleTargetOut.i4_Length + 1)),
                  (const CHR1 *) pProxyEntry->
                  PrxMultipleTargetOut.pu1_OctetList);
        CliPrintf (CliHandle, "Proxy MultipleTargetOut   : %s \r\n", ai1Name);

        CliPrintf (CliHandle, "Proxy Type                : %s\r\n",
                   au1ProxyType[pProxyEntry->ProxyType - 1]);

        CliPrintf (CliHandle, "Storage Type              : %s\r\n",
                   au1Storage[pProxyEntry->i4Storage]);

        CliPrintf (CliHandle, "Row Status                : %s\r\n",
                   au1RowStatus[pProxyEntry->i4Status - 1]);

        u4PagingStatus = (UINT4) CliPrintf (CliHandle,
                                            "---------------------------------------------------- \r\n");

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            break;
        }

        pProxyEntry = SNMPGetNextProxyEntry (&(pProxyEntry->ProxyName));
    }
    while (pProxyEntry != NULL);
    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliShowPropProxy                              */
/*                                                                           */
/*     DESCRIPTION      : Function to show entry from Proxy Table            */
/*                                                                           */
/*     INPUT            : CliHandle     -  CliHandle                         */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliShowPropProxy (tCliHandle CliHandle)
{

    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT1                ai1Name[SNMP_MAX_OCTETSTRING_SIZE - 1];
    UINT4               u4MibIdCnt = 0;

    tPrpProxyTableEntry *pProxyEntry = NULL;

    if ((pProxyEntry = SNMPGetFirstPrpProxy ()) == NULL)
    {
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r\n");
    do
    {
        SNPRINTF ((CHR1 *) ai1Name,
                  (UINT4) ((pProxyEntry->PrpPrxMibName.i4_Length + 1)),
                  (const CHR1 *) pProxyEntry->PrpPrxMibName.pu1_OctetList);
        CliPrintf (CliHandle, "Prop Proxy Name                : %s \r\n",
                   ai1Name);

        MEMSET (ai1Name, 0, SNMP_MAX_OCTETSTRING_SIZE - 1);
        if (pProxyEntry->PrpPrxMibID.u4_Length >= SNMP_ONE)
        {
            for (u4MibIdCnt = 0;
                 u4MibIdCnt < (pProxyEntry->PrpPrxMibID.u4_Length - 1);
                 u4MibIdCnt++)
            {
                SPRINTF ((CHR1 *) ai1Name + STRLEN (ai1Name), "%u.",
                         pProxyEntry->PrpPrxMibID.pu4_OidList[u4MibIdCnt]);
            }
            SPRINTF ((CHR1 *) ai1Name + STRLEN (ai1Name), "%u",
                     pProxyEntry->PrpPrxMibID.pu4_OidList[u4MibIdCnt]);
        }

        CliPrintf (CliHandle, "Prop MibID                     : %s \r\n",
                   ai1Name);
        MEMSET (ai1Name, 0, SNMP_MAX_OCTETSTRING_SIZE - 1);

        SNPRINTF ((CHR1 *) ai1Name,
                  (UINT4) ((pProxyEntry->PrpPrxTargetParamsIn.i4_Length + 1)),
                  (const CHR1 *) pProxyEntry->
                  PrpPrxTargetParamsIn.pu1_OctetList);
        CliPrintf (CliHandle, "Prop Proxy TargetParamIn       : %s \r\n",
                   ai1Name);

        SNPRINTF ((CHR1 *) ai1Name,
                  (UINT4) ((pProxyEntry->PrpPrxSingleTargetOut.i4_Length + 1)),
                  (const CHR1 *) pProxyEntry->
                  PrpPrxSingleTargetOut.pu1_OctetList);
        CliPrintf (CliHandle, "Prop Proxy SingleTargetOut     : %s \r\n",
                   ai1Name);

        SNPRINTF ((CHR1 *) ai1Name,
                  (UINT4) ((pProxyEntry->PrpPrxMultipleTargetOut.i4_Length +
                            1)),
                  (const CHR1 *) pProxyEntry->
                  PrpPrxMultipleTargetOut.pu1_OctetList);
        CliPrintf (CliHandle, "Prop Proxy MultipleTargetOut   : %s \r\n",
                   ai1Name);

        CliPrintf (CliHandle, "Prop Proxy Type                : %s\r\n",
                   au1ProxyType[pProxyEntry->i4PrpPrxMibType - 1]);

        CliPrintf (CliHandle, "Prop Storage Type              : %s\r\n",
                   au1Storage[pProxyEntry->i4Storage]);

        CliPrintf (CliHandle, "Prop Row Status                : %s\r\n",
                   au1RowStatus[pProxyEntry->i4Status - 1]);

        u4PagingStatus = (UINT4) CliPrintf (CliHandle,
                                            "---------------------------------------------------- \r\n");

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            break;
        }

        pProxyEntry = SNMPGetNextPrpProxyEntry (&(pProxyEntry->PrpPrxMibName));
    }
    while (pProxyEntry != NULL);
    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliRemoveGroup                                */
/*                                                                           */
/*     DESCRIPTION      : Function to delete group entry from Group Table    */
/*                                                                           */
/*     INPUT            : pu1GroupName  -  Group Name                        */
/*                        pu1SecName    -  Security Name                     */
/*                        i4SecModel    -  Security Model                    */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliRemoveGroup (tCliHandle CliHandle, UINT1 *pu1GroupName,
                     UINT1 *pu1SecName, INT4 i4SecModel)
{
    UINT1               au1SecurityName[SNMP_MAX_ADMIN_STR_LENGTH];

    tSNMP_OCTET_STRING_TYPE SecurityName;

    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (pu1GroupName);

    /* verify validity of inputs */
    if (SnmpCheckString (pu1SecName) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_SNMP3_INVALID_INPUT_ERR);
        return CLI_FAILURE;
    }

    MEMSET (au1SecurityName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    STRNCPY (au1SecurityName, pu1SecName, (sizeof (au1SecurityName) - 1));

    SecurityName.pu1_OctetList = au1SecurityName;
    SecurityName.i4_Length = (INT4) STRLEN (au1SecurityName);

    /* Delete entry */
    if (nmhSetVacmSecurityToGroupStatus (i4SecModel, &SecurityName,
                                         SNMP_ROWSTATUS_DESTROY)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliGroupConfig                                */
/*                                                                           */
/*     DESCRIPTION      : Function to configure the Group Table              */
/*                                                                           */
/*     INPUT            : pu1GroupName  -  Group Name                        */
/*                        pu1SecName    -  Security Name                     */
/*                        i4SecModel    -  Security Model                    */
/*                        u4StorageType - Storage type in memory             */
/*                                                                           */
/*     OUTPUT           : CliHandle                                          */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
Snmp3CliGroupConfig (tCliHandle CliHandle, UINT1 *pu1GroupName,
                     UINT1 *pu1SecName, INT4 i4SecModel, UINT4 u4StorageType)
{
    UINT4               u4ErrCode = 0;

    UNUSED_PARAM (CliHandle);

    if (Snmp3GroupConfig (pu1GroupName, pu1SecName, i4SecModel,
                          u4StorageType, &u4ErrCode) == SNMP_FAILURE)
    {
        CLI_SET_ERR (u4ErrCode);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliAccessConfig                               */
/*                                                                           */
/*     DESCRIPTION      : Function to configure the Access Table             */
/*                                                                           */
/*     INPUT            : pu1Group      - Group Name                         */
/*                        pu1Context    - Context Name                       */
/*                        i4SecLevel    - Security Level                     */
/*                        i4SecModel    - Security Model                     */
/*                        pu1ReadName   - Read Name                          */
/*                        pu1WriteName  - Write Name                         */
/*                        pu1NotifyName - Notify Name                        */
/*                        u4StorageType - Storage type in memory             */
/*                                                                           */
/*     OUTPUT           : CliHandle.                                         */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
Snmp3CliAccessConfig (tCliHandle CliHandle, UINT1 *pu1Group,
                      UINT1 *pu1Context, INT4 i4SecLevel,
                      INT4 i4SecModel, UINT1 *pu1ReadName,
                      UINT1 *pu1WriteName, UINT1 *pu1NotifyName,
                      UINT4 u4StorageType)
{
    UINT4               u4ErrCode = 0;

    UNUSED_PARAM (CliHandle);
    if (Snmp3AccessConfig (pu1Group, pu1Context, i4SecLevel,
                           i4SecModel, pu1ReadName, pu1WriteName,
                           pu1NotifyName, u4StorageType, &u4ErrCode)
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (u4ErrCode);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliAccessDelete                               */
/*                                                                           */
/*     DESCRIPTION      : This function Deletes SNMPv3 Access entry          */
/*                                                                           */
/*     INPUT            : pu1GroupName  - SNMP group identifier              */
/*                        pu1Context    - Context Name                       */
/*                        i4SecLevel    - Security Level                     */
/*                        i4SecModel    - Security Model                     */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliAccessDelete (tCliHandle CliHandle, UINT1 *pu1GroupName,
                      UINT1 *pu1Context, INT4 i4SecLevel, INT4 i4SecModel)
{
    UINT1               au1GroupName[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1ContextName[SNMP_MAX_ADMIN_STR_LENGTH];

    tSNMP_OCTET_STRING_TYPE GroupName;
    tSNMP_OCTET_STRING_TYPE ContextName;

    UNUSED_PARAM (CliHandle);

    /* verify validity of inputs */
    if (SnmpCheckString (pu1GroupName) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_SNMP3_INVALID_INPUT_ERR);
        return CLI_FAILURE;
    }

    MEMSET (au1GroupName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    STRNCPY (au1GroupName, pu1GroupName, (sizeof (au1GroupName) - 1));

    GroupName.pu1_OctetList = au1GroupName;
    GroupName.i4_Length = (INT4) STRLEN (au1GroupName);

    MEMSET (au1ContextName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    MEMCPY (au1ContextName, pu1Context, STRLEN (pu1Context));

    ContextName.pu1_OctetList = au1ContextName;
    ContextName.i4_Length = (INT4) STRLEN (pu1Context);

    /* Delete entry */
    if (nmhSetVacmAccessStatus (&GroupName, &ContextName, i4SecModel,
                                i4SecLevel, SNMP_ROWSTATUS_DESTROY)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliSetEngineId                                */
/*                                                                           */
/*     DESCRIPTION      : This function Sets a new SNMPv3 Engine Identifier  */
/*                                                                           */
/*     INPUT            : pu1EngineId  - SNMP Local engine identifier        */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliSetEngineId (tCliHandle CliHandle, UINT1 *pu1EngineId)
{

    UINT1               au1TempSnmpEngineID[SNMP_MAX_ENGINE_ID_LEN];
    tSNMP_OCTET_STRING_TYPE SnmpEngineID;
    INT1                ai1Name[SNMP_MAX_STR_ENGINEID_LEN + 1];
    UINT4               u4ErrorCode = 0;
    SnmpEngineID.pu1_OctetList = au1TempSnmpEngineID;

    MEMSET (ai1Name, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);
    if (SnmpConvertStringToOctet ((INT1 *) pu1EngineId, &SnmpEngineID) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Error in the EngineID entered\r\n");
        return CLI_FAILURE;
    }
    if (nmhTestv2FsSnmpEngineID (&u4ErrorCode, &SnmpEngineID) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetFsSnmpEngineID (&SnmpEngineID) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle,
               "\r\nWarning: Updating the Engine ID regenerates the localized"
               "        \r\nkeys for Authentication and Privacy of all the user"
               "        \r\nentries present in this Agent\r\n\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliReSetEngineId                              */
/*                                                                           */
/*     DESCRIPTION      : This function Re-sets SNMPv3 Engine Identifier to  */
/*                        its default local identifier                       */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliReSetEngineId (tCliHandle CliHandle)
{

    UNUSED_PARAM (CliHandle);

    /* re-set engine ID to default engine ID */
    if (nmhSetFsSnmpEngineID (&gSnmpDefaultEngineID) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliViewConfig                                 */
/*                                                                           */
/*     DESCRIPTION      : Function to configure the View Tree Table          */
/*                                                                           */
/*     INPUT            : pu1ViewName   - View Name                          */
/*                        pu1OID        - Subtree OID                        */
/*                        pu1Mask       - Mask                               */
/*                        u4MaskType    - Mask type (include/exclude)        */
/*                        u4StorageType - Storage type in memory             */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliViewConfig (tCliHandle CliHandle, UINT1 *pu1ViewName,
                    UINT1 *pu1OID, UINT1 *pu1Mask, UINT4 u4MaskType,
                    UINT4 u4StorageType)
{
    UINT4               u4ErrorCode;
    UINT1               u1MaskFlag = CLI_FALSE;
    UINT1               au1ViewName[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1ViewMask[SNMP_VIEW_MASK_MAX_STRING_LENGTH];
    UINT1               au1TmpViewMask[SNMP_VIEW_MASK_MAX_STRING_LENGTH];
    UINT1               au1NoneMask[SNMP_VIEW_MASK_MAX_STRING_LENGTH];
    UINT1               i1MaskCount = 0;
    INT4                i4Count;
    INT4                i4Int;

    tViewTree          *pView = NULL;
    tSNMP_OCTET_STRING_TYPE ViewName;
    tSNMP_OCTET_STRING_TYPE ViewMask;
    tSNMP_OCTET_STRING_TYPE NoneMask;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OID_TYPE     *pViewOid = NULL;

    UNUSED_PARAM (CliHandle);

    MEMSET (au1TmpViewMask, 0, sizeof (au1TmpViewMask));

    /* verify validity of inputs */
    if (SnmpCheckString (pu1ViewName) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_SNMP3_INVALID_INPUT_ERR);
        return CLI_FAILURE;
    }

    pOid = SNMP_AGT_GetOidFromString ((INT1 *) pu1OID);
    if (pOid == NULL)
    {
        CLI_SET_ERR (CLI_SNMP3_OID_ERR);
        return CLI_FAILURE;
    }

    MEMSET (au1ViewName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    STRNCPY (au1ViewName, pu1ViewName, (sizeof (au1ViewName) - 1));

    ViewName.pu1_OctetList = au1ViewName;
    ViewName.i4_Length = (INT4) STRLEN (au1ViewName);
    ViewMask.i4_Length = 0;        /*kloc */

    if (STRCMP (pu1Mask, "none") != 0)
    {

        pViewOid = SNMP_AGT_GetOidFromString ((INT1 *) pu1Mask);
        if (pViewOid == NULL)
        {
            CLI_SET_ERR (CLI_SNMP3_MASK_ERR);
            free_oid (pOid);
            return CLI_FAILURE;
        }

        for (i4Count = 0; i4Count < (INT4) pViewOid->u4_Length; i4Count++)
        {
            if ((pViewOid->pu4_OidList[i4Count] == SNMP_ONE) ||
                (pViewOid->pu4_OidList[i4Count] == SNMP_ZERO))
            {
                continue;
            }
            else
            {
                CLI_SET_ERR (CLI_SNMP3_MASK_ERR);
                free_oid (pOid);
                free_oid (pViewOid);
                return CLI_FAILURE;
            }
        }

        MEMSET (au1ViewMask, 0, SNMP_VIEW_MASK_MAX_STRING_LENGTH);

        for (i4Int = 0; (i4Int < (INT4) STRLEN (pu1Mask)) &&
             (i1MaskCount < SNMP_VIEW_MASK_MAX_STRING_LENGTH); i4Int++)
        {
            if (isdigit (pu1Mask[i4Int]))
            {
                au1TmpViewMask[i1MaskCount++]
                    = (UINT1) ((pu1Mask[i4Int]) - '0');
            }
        }

        MEMCPY (au1ViewMask, au1TmpViewMask, i4Int);

        ViewMask.pu1_OctetList = au1ViewMask;
        ViewMask.i4_Length = i1MaskCount;

        u1MaskFlag = CLI_TRUE;
    }
    else                        /*kloc */
    {
        ViewMask.i4_Length = 0;
    }

    /* check if an entry exists already */
    if ((pView = VACMViewTreeGet (&ViewName, pOid)) != NULL)
    {
        nmhSetVacmViewTreeFamilyStatus (&ViewName, pOid,
                                        SNMP_ROWSTATUS_NOTINSERVICE);

        /* entry exists - if any value in table is changed, modify */
        if (STRCMP (pu1Mask, "none") == 0)
        {
            /* Mask not given, set to all 1's */
            /* According to the extension rule given in stdvacm.mib 
             * we will set mask to be all 1's */
            MEMSET (au1NoneMask, 1, SNMP_VIEW_MASK_MAX_STRING_LENGTH);

            NoneMask.pu1_OctetList = au1NoneMask;
            NoneMask.i4_Length = ViewName.i4_Length;

            if (nmhSetVacmViewTreeFamilyMask (&ViewName, pOid, &NoneMask)
                == SNMP_FAILURE)
            {
                return (Snmp3ViewModifyError (&ViewName, pOid, pViewOid));
            }

        }
        else
        {
            if (SNMPCompareOctetString (&ViewMask,
                                        &(pView->Mask)) != SNMP_EQUAL)
            {
                /* Mask modified, save new value */
                if (nmhTestv2VacmViewTreeFamilyMask (&u4ErrorCode, &ViewName,
                                                     pOid, &ViewMask)
                    == SNMP_FAILURE)
                {
                    CLI_SET_ERR (CLI_SNMP3_MASK_ERR);
                    Snmp3ViewModifyError (&ViewName, pOid, pViewOid);
                    return CLI_FAILURE;
                }

                /* set new mask value */
                if (nmhSetVacmViewTreeFamilyMask (&ViewName, pOid, &ViewMask)
                    == SNMP_FAILURE)
                {
                    return (Snmp3ViewModifyError (&ViewName, pOid, pViewOid));
                }
            }
        }

        if ((INT4) u4MaskType != pView->i4Type)
        {
            if (nmhSetVacmViewTreeFamilyType
                (&ViewName, pOid, (INT4) u4MaskType) == SNMP_FAILURE)
            {
                return (Snmp3ViewModifyError (&ViewName, pOid, pViewOid));
            }
        }

        if ((u4StorageType != 0) && ((INT4) u4StorageType != pView->i4Storage))
        {
            /* Storage Type modified, save new value */
            if (nmhSetVacmViewTreeFamilyStorageType (&ViewName, pOid,
                                                     (INT4) u4StorageType)
                == SNMP_FAILURE)
            {
                return (Snmp3ViewModifyError (&ViewName, pOid, pViewOid));
            }
        }

        nmhSetVacmViewTreeFamilyStatus (&ViewName, pOid, SNMP_ROWSTATUS_ACTIVE);
        /* Entry modified */

        free_oid (pOid);
        pOid = NULL;

        if (u1MaskFlag == CLI_TRUE)
        {
            free_oid (pViewOid);
            pViewOid = NULL;
        }
    }
    else
    {
        /* entry does not exist - ADD */
        if (nmhSetVacmViewTreeFamilyStatus (&ViewName, pOid,
                                            SNMP_ROWSTATUS_CREATEANDWAIT)
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_SNMP3_NEW_ENTRY_ERR);
            free_oid (pOid);

            if (u1MaskFlag == CLI_TRUE)
            {
                free_oid (pViewOid);
            }

            return CLI_FAILURE;
        }

        /* Test and Set Inputs */
        if (u1MaskFlag == CLI_TRUE)
        {
            if (nmhTestv2VacmViewTreeFamilyMask (&u4ErrorCode, &ViewName,
                                                 pOid, &ViewMask)
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_SNMP3_MASK_ERR);
                Snmp3ViewCreateError (&ViewName, pOid, pViewOid);
                return CLI_FAILURE;
            }

            if (nmhSetVacmViewTreeFamilyMask (&ViewName, pOid, &ViewMask)
                == SNMP_FAILURE)
            {
                return (Snmp3ViewCreateError (&ViewName, pOid, pViewOid));
            }
        }

        if (nmhSetVacmViewTreeFamilyType (&ViewName, pOid, (INT4) u4MaskType)
            == SNMP_FAILURE)
        {
            return (Snmp3ViewCreateError (&ViewName, pOid, pViewOid));
        }

        if (u4StorageType == 0)
        {
            /* Storage type not given  - make default as non-volatile type */
            u4StorageType = SNMP3_STORAGE_TYPE_NONVOLATILE;
        }

        if (nmhSetVacmViewTreeFamilyStorageType (&ViewName, pOid,
                                                 (INT4) u4StorageType)
            == SNMP_FAILURE)
        {
            return (Snmp3ViewCreateError (&ViewName, pOid, pViewOid));
        }

        nmhSetVacmViewTreeFamilyStatus (&ViewName, pOid, SNMP_ROWSTATUS_ACTIVE);

        free_oid (pOid);

        if (u1MaskFlag == CLI_TRUE)
        {
            free_oid (pViewOid);
        }

        /* Entry created */
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliViewDelete                                 */
/*                                                                           */
/*     DESCRIPTION      : Function to delete View from View Tree Table       */
/*                                                                           */
/*     INPUT            : pu1ViewName   - View Name                          */
/*                        pu1OID        - Subtree OID                        */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliViewDelete (tCliHandle CliHandle, UINT1 *pu1ViewName, UINT1 *pu1OID)
{
    UINT1               au1ViewName[SNMP_MAX_ADMIN_STR_LENGTH];

    tSNMP_OCTET_STRING_TYPE ViewName;
    tSNMP_OID_TYPE     *pOid;

    UNUSED_PARAM (CliHandle);

    /* verify validity of inputs */
    if (SnmpCheckString (pu1ViewName) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_SNMP3_INVALID_INPUT_ERR);
        return CLI_FAILURE;
    }

    pOid = SNMP_AGT_GetOidFromString ((INT1 *) pu1OID);
    if (pOid == NULL)
    {
        CLI_SET_ERR (CLI_SNMP3_OID_ERR);
        return CLI_FAILURE;
    }

    MEMSET (au1ViewName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    STRNCPY (au1ViewName, pu1ViewName, (sizeof (au1ViewName) - 1));

    ViewName.pu1_OctetList = au1ViewName;
    ViewName.i4_Length = (INT4) STRLEN (au1ViewName);

    /* Delete view */
    if (nmhSetVacmViewTreeFamilyStatus (&ViewName, pOid,
                                        SNMP_ROWSTATUS_DESTROY) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_SNMP3_VIEW_NOTPRESENT_ERR);
        free_oid (pOid);
        return CLI_FAILURE;
    }

    free_oid (pOid);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3ViewModifyError                               */
/*                                                                           */
/*     DESCRIPTION      : Function to set View Table modification error      */
/*                        and set the row status of the entry to ACTIVE      */
/*                        the entry is set to ACTIVE as modification to the  */
/*                        existing entry failed                              */
/*                                                                           */
/*     INPUT            : pView  - pointer to View table entry               */
/*                        pOid      - pointer to Subtree Oid                 */
/*                        pViewOid  - pointer to Mask Oid                    */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT1
Snmp3ViewModifyError (tSNMP_OCTET_STRING_TYPE * pViewName,
                      tSNMP_OID_TYPE * pOid, tSNMP_OID_TYPE * pViewOid)
{
    CLI_SET_ERR (CLI_SNMP3_ENTRY_MODIFICATION_ERR);

    nmhSetVacmViewTreeFamilyStatus (pViewName, pOid, SNMP_ROWSTATUS_ACTIVE);

    if (pViewOid != NULL)
    {
        free_oid (pViewOid);
    }

    if (pOid != NULL)
    {
        free_oid (pOid);
    }

    return CLI_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3ViewCreateError                               */
/*                                                                           */
/*     DESCRIPTION      : Function to set Access Table entry creation err    */
/*                        and to delete the entry created                    */
/*                                                                           */
/*     INPUT            : pViewName - View Name                              */
/*                        pOid      - pointer to Subtree Oid                 */
/*                        pViewOid  - pointer to Mask Oid                    */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT1
Snmp3ViewCreateError (tSNMP_OCTET_STRING_TYPE * pViewName,
                      tSNMP_OID_TYPE * pOid, tSNMP_OID_TYPE * pViewOid)
{
    CLI_SET_ERR (CLI_SNMP3_ENTRY_CREATION_ERR);

    if (nmhSetVacmViewTreeFamilyStatus (pViewName, pOid,
                                        SNMP_ROWSTATUS_DESTROY) == SNMP_FAILURE)
    {
        ;
    }

    if (pOid != NULL)
    {
        free_oid (pOid);
    }

    if (pViewOid != NULL)
    {
        free_oid (pViewOid);
    }

    return CLI_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliTargetAddrConfig                           */
/*                                                                           */
/*     DESCRIPTION      : Fucntion to configure the Target Address Table     */
/*                                                                           */
/*     INPUT            : pu1AddrName   - Address Name                       */
/*                        u4IpAddr      - Ip Address                         */
/*                        pu1AddrParam  - Address Param Name                 */
/*                        u4TimeOut     - Time Out value                     */
/*                        u4RetryCount  - Maximum retries                    */
/*                        pu1Tag        - Tag value                          */
/*                        u4StorageType - Storage type in memory             */
/*                        u4IpVersion   - Version of the IP                  */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
Snmp3CliTargetAddrConfig (tCliHandle CliHandle, UINT1 *pu1AddrName,
                          UINT1 *pu1IpAddr, UINT1 *pu1AddrParam, UINT1 *pu1Tag,
                          UINT4 u4TimeOut, UINT4 u4RetryCount,
                          UINT4 u4StorageType, UINT4 u4IpVersion, UINT2 u2Port)
{
    UINT1               au1AddrName[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1IpAddrName[DNS_MAX_QUERY_LEN];
    UINT1               au1ParamName[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1Tag[MAX_STR_OCTET_VAL];
    UINT1               au1ClearTag[MAX_STR_OCTET_VAL];
    UINT4               u4ErrorCode;
    UINT1               u1TagFlag = CLI_FALSE;
    UINT1               au1IpAddress[SNMP_IP6ADDR_LEN];
    UINT4               u4IpAddr = 0;
    UINT2               u2Ipv4Port = 0;

    tSNMP_OCTET_STRING_TYPE TargetAddrName;
    tSNMP_OCTET_STRING_TYPE TargetAddr;
    tSNMP_OCTET_STRING_TYPE ParamName;
    tSNMP_OCTET_STRING_TYPE TransportTag;
    tSNMP_OCTET_STRING_TYPE ClearTag;
    tSnmpTgtAddrEntry  *pTargetEntry = NULL;

    MEMSET (au1IpAddress, SNMP_ZERO, SNMP_IP6ADDR_LEN);
    UNUSED_PARAM (CliHandle);

    /* verify validity of inputs */
    if ((SnmpCheckString (pu1AddrName) == SNMP_FAILURE) ||
        (SnmpCheckString (pu1Tag) == SNMP_FAILURE) ||
        (SnmpCheckString (pu1AddrParam) == SNMP_FAILURE))
    {
        CLI_SET_ERR (CLI_SNMP3_INVALID_INPUT_ERR);
        return CLI_FAILURE;
    }

    MEMSET (au1AddrName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    STRNCPY (au1AddrName, pu1AddrName, (sizeof (au1AddrName) - 1));

    TargetAddrName.pu1_OctetList = au1AddrName;
    TargetAddrName.i4_Length = (INT4) STRLEN (au1AddrName);
    TargetAddr.i4_Length = 0;    /*kloc */
    TransportTag.i4_Length = 0;    /*kloc */

    MEMSET (au1IpAddrName, 0, DNS_MAX_QUERY_LEN);

    if (u4IpVersion == SNMP_IP_V4)
    {
        u4IpAddr = *(UINT4 *) (VOID *) pu1IpAddr;
        u4IpAddr = OSIX_NTOHL (u4IpAddr);

        if (((u4IpAddr & 0xff000000) < 0x01000000)
            || ((u4IpAddr & 0xff000000) > 0xfe000000))
        {
            CliPrintf (CliHandle, "\r%% Invalid Ip Address \r\n");
            return CLI_FAILURE;
        }
        if (!((IP_IS_ADDR_CLASS_A (OSIX_HTONL (u4IpAddr)) ?
               (IP_IS_ZERO_NETWORK (OSIX_HTONL (u4IpAddr)) ? 0 : 1) : 0) ||
              (IP_IS_ADDR_CLASS_B (OSIX_HTONL (u4IpAddr))) ||
              (IP_IS_ADDR_CLASS_C (OSIX_HTONL (u4IpAddr)))))
        {
            CliPrintf (CliHandle, "\r%% Invalid Ip Address \r\n");
            return CLI_FAILURE;
        }
        if ((u4IpAddr & 0x0000007f) == 0x0000007f)
        {
            CLI_SET_ERR (CLI_SNMP3_INVALID_INPUT_ERR);
            return CLI_FAILURE;
        }
        /*Move the pointer to fetch the port */
        pu1IpAddr = pu1IpAddr + SNMP_IPADDR_LEN;
        u2Ipv4Port = *(UINT2 *) (VOID *) pu1IpAddr;
        u2Ipv4Port = OSIX_NTOHS (u2Ipv4Port);
        /* Copy the IpAddress and port number to the array */

        MEMCPY (au1IpAddrName, &u4IpAddr, SNMP_IPADDR_LEN);
        MEMCPY (au1IpAddrName + SNMP_IPADDR_LEN, &u2Ipv4Port, SNMP_TWO);

        /* Total lenght of the TargetAddress will be lenght of Ipaddress
           and port */
        TargetAddr.i4_Length = SNMP_IPADDR_LEN + SNMP_TWO;

    }
    else if (u4IpVersion == SNMP_IP_V6)
    {
        INET_PTON (AF_INET6, pu1IpAddr, au1IpAddress);
        MEMCPY (au1IpAddrName, au1IpAddress, SNMP_IP6ADDR_LEN);
        MEMCPY (au1IpAddrName + SNMP_IP6ADDR_LEN, &u2Port, SNMP_TWO);
        TargetAddr.i4_Length = SNMP_IP6ADDR_LEN + SNMP_TWO;
    }
    else if (STRLEN (pu1IpAddr) <= DNS_MAX_QUERY_LEN)    /*kloc */
    {
        MEMCPY (au1IpAddrName, pu1IpAddr, STRLEN (pu1IpAddr));
        MEMCPY (au1IpAddrName + STRLEN (pu1IpAddr), &u2Port, SNMP_TWO);
        au1IpAddrName[(STRLEN (pu1IpAddr) + SNMP_TWO)] = '\0';
        TargetAddr.i4_Length = (INT4) (STRLEN (pu1IpAddr) + SNMP_TWO);
    }
    TargetAddr.pu1_OctetList = au1IpAddrName;

    MEMSET (au1ParamName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    STRNCPY (au1ParamName, pu1AddrParam, (sizeof (au1ParamName) - 1));

    ParamName.pu1_OctetList = au1ParamName;
    ParamName.i4_Length = (INT4) STRLEN (au1ParamName);

    if ((STRCMP (pu1Tag, "none") != 0) && (STRCMP (pu1Tag, "clear") != 0))
    {
        MEMSET (au1Tag, 0, MAX_STR_OCTET_VAL);
        STRNCPY (au1Tag, pu1Tag, (sizeof (au1Tag) - 1));

        TransportTag.pu1_OctetList = au1Tag;
        TransportTag.i4_Length = (INT4) STRLEN (au1Tag);

        u1TagFlag = CLI_TRUE;
    }
    else                        /*kloc */
    {
        TransportTag.i4_Length = 0;
    }

    /* TimeInterval definition specifies that the value is
     * 1/100 of seconds.So Convert Seconds to TimeInterval */
    u4TimeOut = u4TimeOut * 100;

    /* check if an entry exists already */
    if ((pTargetEntry = SNMPGetTgtAddrEntry (&TargetAddrName)) != NULL)
    {
        nmhSetSnmpTargetAddrRowStatus (&TargetAddrName,
                                       SNMP_ROWSTATUS_NOTINSERVICE);

        /* entry exists - if any value in table is changed, modify */

        if ((u4TimeOut != 0) && ((INT4) u4TimeOut != pTargetEntry->i4TimeOut))
        {
            /* Time Out value modified, save new value */
            if (nmhSetSnmpTargetAddrTimeout (&TargetAddrName,
                                             (INT4) u4TimeOut) == SNMP_FAILURE)
            {
                return (Snmp3TargetAddrModifyError (&TargetAddrName));
            }
        }

        if ((u4RetryCount != 0) &&
            ((INT4) u4RetryCount != pTargetEntry->i4RetryCount))
        {
            /* Retry Count value modified, save new value */
            if (nmhSetSnmpTargetAddrRetryCount (&TargetAddrName,
                                                (INT4) u4RetryCount)
                == SNMP_FAILURE)
            {
                return (Snmp3TargetAddrModifyError (&TargetAddrName));
            }
        }

        if (nmhTestv2SnmpTargetAddrParams (&u4ErrorCode, &TargetAddrName,
                                           &ParamName) == SNMP_FAILURE)
        {
            return (Snmp3TargetAddrCreateError (&TargetAddrName));
        }

        if (nmhSetSnmpTargetAddrParams (&TargetAddrName, &ParamName)
            == SNMP_FAILURE)
        {
            return (Snmp3TargetAddrCreateError (&TargetAddrName));
        }

        if (u1TagFlag == CLI_TRUE)
        {
            if (nmhTestv2SnmpTargetAddrTagList (&u4ErrorCode, &TargetAddrName,
                                                &TransportTag) == SNMP_FAILURE)
            {
                return (Snmp3TargetAddrCreateError (&TargetAddrName));
            }
        }

        if (u1TagFlag == CLI_TRUE)
        {
            if (nmhSetSnmpTargetAddrTagList (&TargetAddrName, &TransportTag)
                == SNMP_FAILURE)
            {
                return (Snmp3TargetAddrCreateError (&TargetAddrName));
            }
        }

        if (STRCMP (pu1Tag, "clear") == 0)
        {
            /* Tag cleared, clear existing value */
            MEMSET (au1ClearTag, 0, MAX_STR_OCTET_VAL);

            ClearTag.pu1_OctetList = au1ClearTag;
            ClearTag.i4_Length = MAX_STR_OCTET_VAL;

            if (nmhSetSnmpTargetAddrTagList (&TargetAddrName, &ClearTag)
                == SNMP_FAILURE)
            {
                return (Snmp3TargetAddrModifyError (&TargetAddrName));
            }
        }
        else
        {
            if ((STRCMP (pu1Tag, "none") != 0) &&
                (SNMPCompareOctetString
                 (&TransportTag, &(pTargetEntry->TagList)) != SNMP_EQUAL))
            {
                /* Tag identifier modified, test new value */
                if (nmhTestv2SnmpTargetAddrTagList (&u4ErrorCode,
                                                    &TargetAddrName,
                                                    &TransportTag)
                    == SNMP_FAILURE)
                {
                    CLI_SET_ERR (CLI_SNMP3_TAGLIST_ERR);
                    return (Snmp3TargetAddrModifyError (&TargetAddrName));
                }

                /* clear previous tag */
                MEMSET (au1ClearTag, 0, MAX_STR_OCTET_VAL);

                ClearTag.pu1_OctetList = au1ClearTag;
                ClearTag.i4_Length = MAX_STR_OCTET_VAL;

                if (nmhSetSnmpTargetAddrTagList (&TargetAddrName, &ClearTag)
                    == SNMP_FAILURE)
                {
                    return (Snmp3TargetAddrModifyError (&TargetAddrName));
                }

                /* save new tag */
                if (nmhSetSnmpTargetAddrTagList (&TargetAddrName,
                                                 &TransportTag) == SNMP_FAILURE)
                {
                    return (Snmp3TargetAddrModifyError (&TargetAddrName));
                }
            }
        }

        if ((u4StorageType != 0) &&
            ((INT4) u4StorageType != pTargetEntry->i4StorageType))
        {
            /* Storage Type modified, save new value */
            if (nmhSetSnmpTargetAddrStorageType (&TargetAddrName,
                                                 (INT4) u4StorageType)
                == SNMP_FAILURE)
            {
                return (Snmp3TargetAddrModifyError (&TargetAddrName));
            }
        }

        if ((SNMPCompareOctetString
             (&TargetAddr, &(pTargetEntry->Address)) != SNMP_EQUAL))
        {
            if (u4IpVersion != IPVX_DNS_FAMILY)
            {
                if (nmhTestv2SnmpTargetAddrTAddress (&u4ErrorCode,
                                                     &TargetAddrName,
                                                     &TargetAddr) ==
                    SNMP_FAILURE)
                {
                    return (Snmp3TargetAddrModifyError (&TargetAddrName));
                }

                if (nmhSetSnmpTargetAddrTAddress (&TargetAddrName, &TargetAddr)
                    == SNMP_FAILURE)
                {
                    return (Snmp3TargetAddrModifyError (&TargetAddrName));
                }
            }
            else
            {
                if (nmhTestv2FsSnmpTargetHostName (&u4ErrorCode,
                                                   &TargetAddrName,
                                                   &TargetAddr) == SNMP_FAILURE)
                {
                    return (Snmp3TargetAddrModifyError (&TargetAddrName));
                }

                if (nmhSetFsSnmpTargetHostName (&TargetAddrName, &TargetAddr)
                    == SNMP_FAILURE)
                {
                    return (Snmp3TargetAddrModifyError (&TargetAddrName));
                }
            }
        }

        nmhSetSnmpTargetAddrRowStatus (&TargetAddrName, SNMP_ROWSTATUS_ACTIVE);
        /* Entry modified */
    }
    else
    {
        /* entry does not exist - ADD */
        if (nmhSetSnmpTargetAddrRowStatus (&TargetAddrName,
                                           SNMP_ROWSTATUS_CREATEANDWAIT)
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_SNMP3_NEW_ENTRY_ERR);
            return CLI_FAILURE;
        }

        if (u4StorageType == 0)
        {
            /* Storage type not given  - make default as non-volatile type */
            u4StorageType = SNMP3_STORAGE_TYPE_NONVOLATILE;
        }

        if (nmhSetSnmpTargetAddrStorageType (&TargetAddrName,
                                             (INT4) u4StorageType)
            == SNMP_FAILURE)
        {
            return (Snmp3TargetAddrCreateError (&TargetAddrName));
        }

        /* Test Inputs */
        if (u4IpVersion != IPVX_DNS_FAMILY)
        {
            if (nmhTestv2SnmpTargetAddrTAddress (&u4ErrorCode, &TargetAddrName,
                                                 &TargetAddr) == SNMP_FAILURE)
            {
                return (Snmp3TargetAddrCreateError (&TargetAddrName));
            }
        }
        else
        {
            if (nmhTestv2FsSnmpTargetHostName (&u4ErrorCode,
                                               &TargetAddrName,
                                               &TargetAddr) == SNMP_FAILURE)
            {
                return (Snmp3TargetAddrModifyError (&TargetAddrName));
            }
        }

        if (nmhTestv2SnmpTargetAddrTimeout (&u4ErrorCode, &TargetAddrName,
                                            (INT4) u4TimeOut) == SNMP_FAILURE)
        {
            return (Snmp3TargetAddrCreateError (&TargetAddrName));
        }

        if (nmhTestv2SnmpTargetAddrRetryCount (&u4ErrorCode, &TargetAddrName,
                                               (INT4) u4RetryCount)
            == SNMP_FAILURE)
        {
            return (Snmp3TargetAddrCreateError (&TargetAddrName));
        }

        if (nmhTestv2SnmpTargetAddrParams (&u4ErrorCode, &TargetAddrName,
                                           &ParamName) == SNMP_FAILURE)
        {
            return (Snmp3TargetAddrCreateError (&TargetAddrName));
        }

        if (u1TagFlag == CLI_TRUE)
        {
            if (nmhTestv2SnmpTargetAddrTagList (&u4ErrorCode, &TargetAddrName,
                                                &TransportTag) == SNMP_FAILURE)
            {
                return (Snmp3TargetAddrCreateError (&TargetAddrName));
            }
        }

        /* Set Inputs */
        if (u4IpVersion != IPVX_DNS_FAMILY)
        {
            if (nmhSetSnmpTargetAddrTAddress (&TargetAddrName, &TargetAddr)
                == SNMP_FAILURE)
            {
                return (Snmp3TargetAddrCreateError (&TargetAddrName));
            }
        }
        else
        {
            if (nmhSetFsSnmpTargetHostName (&TargetAddrName, &TargetAddr)
                == SNMP_FAILURE)
            {
                return (Snmp3TargetAddrModifyError (&TargetAddrName));
            }
        }

        if (nmhSetSnmpTargetAddrTimeout (&TargetAddrName, (INT4) u4TimeOut)
            == SNMP_FAILURE)
        {
            return (Snmp3TargetAddrCreateError (&TargetAddrName));
        }

        if (nmhSetSnmpTargetAddrRetryCount (&TargetAddrName,
                                            (INT4) u4RetryCount)
            == SNMP_FAILURE)
        {
            return (Snmp3TargetAddrCreateError (&TargetAddrName));
        }

        if (nmhSetSnmpTargetAddrParams (&TargetAddrName, &ParamName)
            == SNMP_FAILURE)
        {
            return (Snmp3TargetAddrCreateError (&TargetAddrName));
        }

        if (u1TagFlag == CLI_TRUE)
        {
            if (nmhSetSnmpTargetAddrTagList (&TargetAddrName, &TransportTag)
                == SNMP_FAILURE)
            {
                return (Snmp3TargetAddrCreateError (&TargetAddrName));
            }
        }

        if (nmhTestv2SnmpTargetAddrRowStatus (&u4ErrorCode,
                                              &TargetAddrName,
                                              SNMP_ROWSTATUS_ACTIVE)
            == SNMP_FAILURE)
        {
            return (Snmp3TargetAddrCreateError (&TargetAddrName));
        }

        if (nmhSetSnmpTargetAddrRowStatus (&TargetAddrName,
                                           SNMP_ROWSTATUS_ACTIVE)
            == SNMP_FAILURE)
        {
            return (Snmp3TargetAddrCreateError (&TargetAddrName));
        }

        /* Entry created */
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliTargetAddrDelete                           */
/*                                                                           */
/*     DESCRIPTION      : Fucntion to remove Target Address from the Target  */
/*                        Address Table                                      */
/*                                                                           */
/*     INPUT            : pu1AddrName   - Address Name                       */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliTargetAddrDelete (tCliHandle CliHandle, UINT1 *pu1AddrName)
{
    UINT1               au1AddrName[SNMP_MAX_ADMIN_STR_LENGTH];

    tSNMP_OCTET_STRING_TYPE TargetAddrName;

    UNUSED_PARAM (CliHandle);

    /* verify validity of inputs */
    if (SnmpCheckString (pu1AddrName) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_SNMP3_INVALID_INPUT_ERR);
        return CLI_FAILURE;
    }

    MEMSET (au1AddrName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    STRNCPY (au1AddrName, pu1AddrName, (sizeof (au1AddrName) - 1));

    TargetAddrName.pu1_OctetList = au1AddrName;
    TargetAddrName.i4_Length = (INT4) STRLEN (au1AddrName);

    /* Delete Entry */
    if (nmhSetSnmpTargetAddrRowStatus (&TargetAddrName,
                                       SNMP_ROWSTATUS_DESTROY) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_SNMP3_TRGTADDR_NOTPRESENT_ERR);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3TargetAddrModifyError                         */
/*                                                                           */
/*     DESCRIPTION      : Function to set Target Address Table modification  */
/*                        err and set the row status of the entry to ACTIVE  */
/*                        the entry is set to ACTIVE as modification to the  */
/*                        existing entry failed                              */
/*                                                                           */
/*     INPUT            : pTargetEntry  - pointer to Access Group entry      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT1
Snmp3TargetAddrModifyError (tSNMP_OCTET_STRING_TYPE * pSnmpTargetAddrName)
{
    CLI_SET_ERR (CLI_SNMP3_ENTRY_MODIFICATION_ERR);

    nmhSetSnmpTargetAddrRowStatus (pSnmpTargetAddrName, SNMP_ROWSTATUS_ACTIVE);

    return CLI_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3TargetAddrCreateError                         */
/*                                                                           */
/*     DESCRIPTION      : Function to set Target Address Table entry creation*/
/*                        error and to delete the entry created              */
/*                                                                           */
/*     INPUT            : pTargetAddrName - Target Address Name              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT1
Snmp3TargetAddrCreateError (tSNMP_OCTET_STRING_TYPE * pTargetAddrName)
{
    CLI_SET_ERR (CLI_SNMP3_ENTRY_CREATION_ERR);

    nmhSetSnmpTargetAddrRowStatus (pTargetAddrName, SNMP_ROWSTATUS_DESTROY);

    return CLI_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliTargetParamConfig                          */
/*                                                                           */
/*     DESCRIPTION      : Function to configure the Target Param Table       */
/*                                                                           */
/*     INPUT            : pu1ParamName  - Param Name                         */
/*                        i4MpModel     - Message Process Model              */
/*                        i4SecModel    - Security Model                     */
/*                        pu1SecName    - Secutiry Name                      */
/*                        i4SecLevel    - Security Level                     */
/*                        u4StorageType - Storage type in memory             */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliTargetParamConfig (tCliHandle CliHandle, UINT1 *pu1ParamName,
                           INT4 i4MpModel, INT4 i4SecModel,
                           UINT1 *pu1SecName, INT4 i4SecLevel,
                           UINT4 u4StorageType, UINT1 *pu1FilterProfileName,
                           UINT4 u4FilterStorageType)
{

    UINT4               u4ErrCode = 0;
    INT4                i4RetStatus = CLI_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;

    UINT1               au1FilterProfileName[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1ParamName[SNMP_MAX_ADMIN_STR_LENGTH];
    tSNMP_OCTET_STRING_TYPE FilterProfileName;
    tSNMP_OCTET_STRING_TYPE ParamName;

    UNUSED_PARAM (CliHandle);

    if (Snmp3TargetParamConfig (pu1ParamName, i4MpModel, i4SecModel,
                                pu1SecName, i4SecLevel, u4StorageType,
                                &u4ErrCode) == SNMP_FAILURE)
    {
        CLI_SET_ERR (u4ErrCode);
        return CLI_FAILURE;
    }

    /* Create a Filter Profile Table if any filter profile name is configured
       for the target params */
    if (pu1FilterProfileName != NULL)
    {

        MEMSET (au1FilterProfileName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
        STRNCPY (au1FilterProfileName, pu1FilterProfileName,
                 (sizeof (au1FilterProfileName) - 1));
        FilterProfileName.pu1_OctetList = au1FilterProfileName;
        FilterProfileName.i4_Length = (INT4) STRLEN (au1FilterProfileName);

        MEMSET (au1ParamName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
        STRNCPY (au1ParamName, pu1ParamName, (sizeof (au1ParamName) - 1));
        ParamName.pu1_OctetList = au1ParamName;
        ParamName.i4_Length = (INT4) STRLEN (au1ParamName);

        i4RetStatus = nmhGetSnmpNotifyFilterProfileRowStatus
            (&ParamName, &i4RowStatus);
        if (i4RetStatus == SNMP_SUCCESS)
        {
            i4RowStatus = ACTIVE;
            /* Entry Already Exists */
            i4RetStatus = nmhTestv2SnmpNotifyFilterProfileRowStatus
                (&u4ErrCode, &ParamName, NOT_IN_SERVICE);
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }
            i4RetStatus = nmhSetSnmpNotifyFilterProfileRowStatus
                (&ParamName, NOT_IN_SERVICE);
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }
        }
        else
        {
            /* Create a New Entry */
            i4RetStatus = nmhTestv2SnmpNotifyFilterProfileRowStatus
                (&u4ErrCode, &ParamName, CREATE_AND_WAIT);
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }
            i4RetStatus = nmhSetSnmpNotifyFilterProfileRowStatus
                (&ParamName, CREATE_AND_WAIT);
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

        }

        /* Perform test and set opertions for the objects */
        i4RetStatus = nmhTestv2SnmpNotifyFilterProfileName
            (&u4ErrCode, &ParamName, &FilterProfileName);
        if (i4RetStatus == SNMP_FAILURE)
        {
            nmhSetSnmpNotifyFilterProfileRowStatus (&ParamName, i4RowStatus);
            return (CLI_FAILURE);
        }
        i4RetStatus = nmhSetSnmpNotifyFilterProfileName
            (&ParamName, &FilterProfileName);
        if (i4RetStatus == SNMP_FAILURE)
        {
            nmhSetSnmpNotifyFilterProfileRowStatus (&ParamName, i4RowStatus);
            return (CLI_FAILURE);
        }
        if (u4FilterStorageType != SNMP_ZERO)
        {
            i4RetStatus = nmhTestv2SnmpNotifyFilterProfileStorType
                (&u4ErrCode, &ParamName, (INT4) u4FilterStorageType);
            if (i4RetStatus == SNMP_FAILURE)
            {
                nmhSetSnmpNotifyFilterProfileRowStatus (&ParamName,
                                                        i4RowStatus);
                return (CLI_FAILURE);
            }
            i4RetStatus = nmhSetSnmpNotifyFilterProfileStorType
                (&ParamName, (INT4) u4FilterStorageType);
            if (i4RetStatus == SNMP_FAILURE)
            {
                nmhSetSnmpNotifyFilterProfileRowStatus (&ParamName,
                                                        i4RowStatus);
                return (CLI_FAILURE);
            }
        }

        /*Finally make the row status as active */
        i4RetStatus = nmhTestv2SnmpNotifyFilterProfileRowStatus
            (&u4ErrCode, &ParamName, ACTIVE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        i4RetStatus = nmhSetSnmpNotifyFilterProfileRowStatus
            (&ParamName, ACTIVE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);

        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliTargetParamDelete                          */
/*                                                                           */
/*     DESCRIPTION      : Fucntion to remove Target Param from the Target    */
/*                        Params Table                                       */
/*                                                                           */
/*     INPUT            : pu1AddrName   - Address Name                       */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliTargetParamDelete (tCliHandle CliHandle, UINT1 *pu1ParamName)
{
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    UINT1               au1ParamName[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT4               u4ErrorCode = 0;
    INT4                i4RetStatus = 0;

    tSNMP_OCTET_STRING_TYPE TargetParamName;

    UNUSED_PARAM (CliHandle);

    /* verify validity of inputs */
    if (SnmpCheckString (pu1ParamName) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_SNMP3_INVALID_INPUT_ERR);
        return CLI_FAILURE;
    }

    MEMSET (au1ParamName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    STRNCPY (au1ParamName, pu1ParamName, (sizeof (au1ParamName) - 1));

    TargetParamName.pu1_OctetList = au1ParamName;
    TargetParamName.i4_Length = (INT4) STRLEN (au1ParamName);

    pSnmpTgtParamEntry = SNMPGetTgtParamEntry (&TargetParamName);
    if (pSnmpTgtParamEntry == NULL)
    {
        CLI_SET_ERR (CLI_SNMP3_TRGTADDR_NOTPRESENT_ERR);
        return CLI_FAILURE;
    }

    /* Associated filter must be deleted first */
    if (pSnmpTgtParamEntry->pFilterProfileTable != NULL)
    {
        i4RetStatus = nmhTestv2SnmpNotifyFilterProfileRowStatus
            (&u4ErrorCode, &TargetParamName, DESTROY);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_SNMP3_TRGTADDR_NOTPRESENT_ERR);
            return CLI_FAILURE;
        }
        i4RetStatus = nmhSetSnmpNotifyFilterProfileRowStatus (&TargetParamName,
                                                              DESTROY);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_SNMP3_TRGTADDR_NOTPRESENT_ERR);
            return CLI_FAILURE;
        }

    }
    if (nmhTestv2SnmpTargetParamsRowStatus (&u4ErrorCode, &TargetParamName,
                                            SNMP_ROWSTATUS_DESTROY)
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_SNMP3_TRGTPARAM_NOTPRESENT_ERR);
        return CLI_FAILURE;
    }
    /* Delete Entry */
    if (nmhSetSnmpTargetParamsRowStatus (&TargetParamName,
                                         SNMP_ROWSTATUS_DESTROY)
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_SNMP3_TRGTPARAM_NOTPRESENT_ERR);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliNotifyFilterConfig                         */
/*                                                                           */
/*     DESCRIPTION      : Function to configure the Notify Filter Table      */
/*                                                                           */
/*     INPUT            : pu1FilterName   - Filter Profile Name              */
/*                        pu1OID        - Filter Subtree OID                 */
/*                        pu1Mask       - Filter Mask                        */
/*                        u4FilterType    - Filter type (include/exclude)    */
/*                        u4StorageType - Storage type in memory             */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliNotifyFilterConfig (tCliHandle CliHandle, UINT1 *pu1FilterName,
                            UINT1 *pu1OID, UINT1 *pu1Mask, UINT4 u4FilterType,
                            UINT4 u4StorageType)
{
    UINT4               u4ErrorCode = 0;
    UINT1               u1MaskFlag = CLI_FALSE;
    UINT1               au1FilterName[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1FilterMask[SNMP_FILTER_MASK_MAX_STRING_LENGTH];
    UINT1               au1TmpFilterMask[SNMP_FILTER_MASK_MAX_STRING_LENGTH];
    UINT1               au1NoneMask[SNMP_FILTER_MASK_MAX_STRING_LENGTH];
    UINT1               i1MaskCount = 0;
    INT4                i4Count = 0;
    INT4                i4Int = 0;
    INT4                i4RetStatus = 0;
    INT4                i4RowStatus = DESTROY;

    tSNMP_OCTET_STRING_TYPE FilterName;
    tSNMP_OCTET_STRING_TYPE FilterMask;
    tSNMP_OCTET_STRING_TYPE NoneMask;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OID_TYPE     *pFilterOid = NULL;

    UNUSED_PARAM (CliHandle);

    /* verify validity of inputs */
    if (SnmpCheckString (pu1FilterName) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_SNMP3_INVALID_INPUT_ERR);
        return CLI_FAILURE;
    }

    pOid = SNMP_AGT_GetOidFromString ((INT1 *) pu1OID);
    if (pOid == NULL)
    {
        CLI_SET_ERR (CLI_SNMP3_OID_ERR);
        return CLI_FAILURE;
    }

    MEMSET (au1FilterName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    STRNCPY (au1FilterName, pu1FilterName, (sizeof (au1FilterName) - 1));

    MEMSET (au1TmpFilterMask, 0, SNMP_FILTER_MASK_MAX_STRING_LENGTH);

    FilterName.pu1_OctetList = au1FilterName;
    FilterName.i4_Length = (INT4) STRLEN (au1FilterName);

    if (STRCMP (pu1Mask, "none") != 0)
    {

        pFilterOid = SNMP_AGT_GetOidFromString ((INT1 *) pu1Mask);
        if (pFilterOid == NULL)
        {
            CLI_SET_ERR (CLI_SNMP3_MASK_ERR);
            free_oid (pOid);
            return CLI_FAILURE;
        }

        for (i4Count = 0; i4Count < (INT4) pFilterOid->u4_Length; i4Count++)
        {
            if ((pFilterOid->pu4_OidList[i4Count] == SNMP_ONE) ||
                (pFilterOid->pu4_OidList[i4Count] == SNMP_ZERO))
            {
                continue;
            }
            else
            {
                CLI_SET_ERR (CLI_SNMP3_MASK_ERR);
                free_oid (pOid);
                free_oid (pFilterOid);
                return CLI_FAILURE;
            }
        }

        MEMSET (au1FilterMask, 0, SNMP_FILTER_MASK_MAX_STRING_LENGTH);

        for (i4Int = 0; i4Int < (INT4) STRLEN (pu1Mask); i4Int++)
        {
            if (isdigit (pu1Mask[i4Int]))
            {
                au1TmpFilterMask[i1MaskCount++]
                    = (UINT1) ((pu1Mask[i4Int]) - '0');
            }
        }

        MEMCPY (au1FilterMask, au1TmpFilterMask, i4Int);

        FilterMask.pu1_OctetList = au1FilterMask;
        FilterMask.i4_Length = i1MaskCount;

        u1MaskFlag = CLI_TRUE;
    }

    /* check if an entry exists already */
    i4RetStatus = nmhGetSnmpNotifyFilterRowStatus
        (&FilterName, pOid, &i4RowStatus);
    if (i4RetStatus == SNMP_SUCCESS)
    {
        i4RowStatus = ACTIVE;
        i4RetStatus = nmhTestv2SnmpNotifyFilterRowStatus (&u4ErrorCode,
                                                          &FilterName, pOid,
                                                          NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            free_oid (pOid);
            if (u1MaskFlag == CLI_TRUE)
            {
                free_oid (pFilterOid);
            }
            CLI_SET_ERR (CLI_SNMP3_ENTRY_MODIFICATION_ERR);
            return CLI_FAILURE;
        }
        i4RetStatus = nmhSetSnmpNotifyFilterRowStatus (&FilterName,
                                                       pOid, NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            free_oid (pOid);
            if (u1MaskFlag == CLI_TRUE)
            {
                free_oid (pFilterOid);
            }
            CLI_SET_ERR (CLI_SNMP3_ENTRY_MODIFICATION_ERR);
            return CLI_FAILURE;
        }
    }
    else
    {
        i4RetStatus = nmhTestv2SnmpNotifyFilterRowStatus (&u4ErrorCode,
                                                          &FilterName, pOid,
                                                          CREATE_AND_WAIT);
        if (i4RetStatus == SNMP_FAILURE)
        {
            free_oid (pOid);
            if (u1MaskFlag == CLI_TRUE)
            {
                free_oid (pFilterOid);
            }
            CLI_SET_ERR (CLI_SNMP3_ENTRY_CREATION_ERR);
            return CLI_FAILURE;
        }
        i4RetStatus = nmhSetSnmpNotifyFilterRowStatus (&FilterName,
                                                       pOid, CREATE_AND_WAIT);
        if (i4RetStatus == SNMP_FAILURE)
        {
            free_oid (pOid);
            if (u1MaskFlag == CLI_TRUE)
            {
                free_oid (pFilterOid);
            }
            CLI_SET_ERR (CLI_SNMP3_ENTRY_CREATION_ERR);
            return CLI_FAILURE;
        }

        /* Entry created */
    }

    if (STRCMP (pu1Mask, "none") == 0)
    {
        /* Mask not given, set to all 1's */
        /* According to the extension rule given in stdvacm.mib 
         * we will set mask to be all 1's */
        MEMSET (au1NoneMask, 1, SNMP_FILTER_MASK_MAX_STRING_LENGTH);

        NoneMask.pu1_OctetList = au1NoneMask;
        NoneMask.i4_Length = (INT4) pOid->u4_Length;

        if (nmhTestv2SnmpNotifyFilterMask (&u4ErrorCode,
                                           &FilterName, pOid, &NoneMask)
            == SNMP_FAILURE)
        {
            nmhSetSnmpNotifyFilterRowStatus (&FilterName, pOid, i4RowStatus);
            free_oid (pOid);
            if (u1MaskFlag == CLI_TRUE)
            {
                free_oid (pFilterOid);
            }
            CLI_SET_ERR (CLI_SNMP3_MASK_ERR);
            return CLI_FAILURE;
        }
        if (nmhSetSnmpNotifyFilterMask (&FilterName, pOid, &NoneMask)
            == SNMP_FAILURE)
        {
            nmhSetSnmpNotifyFilterRowStatus (&FilterName, pOid, i4RowStatus);
            free_oid (pOid);
            if (u1MaskFlag == CLI_TRUE)
            {
                free_oid (pFilterOid);
            }
            CLI_SET_ERR (CLI_SNMP3_MASK_ERR);
            return CLI_FAILURE;
        }

    }
    else
    {
        if (nmhTestv2SnmpNotifyFilterMask (&u4ErrorCode, &FilterName,
                                           pOid, &FilterMask) == SNMP_FAILURE)
        {
            nmhSetSnmpNotifyFilterRowStatus (&FilterName, pOid, i4RowStatus);
            free_oid (pOid);
            if (u1MaskFlag == CLI_TRUE)
            {
                free_oid (pFilterOid);
            }
            CLI_SET_ERR (CLI_SNMP3_MASK_ERR);
            return CLI_FAILURE;
        }

        if (nmhSetSnmpNotifyFilterMask (&FilterName, pOid, &FilterMask)
            == SNMP_FAILURE)
        {
            nmhSetSnmpNotifyFilterRowStatus (&FilterName, pOid, i4RowStatus);
            free_oid (pOid);
            if (u1MaskFlag == CLI_TRUE)
            {
                free_oid (pFilterOid);
            }
            CLI_SET_ERR (CLI_SNMP3_MASK_ERR);
            return CLI_FAILURE;
        }
    }
    if (u4StorageType != 0)
    {
        if (nmhTestv2SnmpNotifyFilterStorageType
            (&u4ErrorCode, &FilterName, pOid, (INT4) u4StorageType) ==
            SNMP_FAILURE)
        {
            nmhSetSnmpNotifyFilterRowStatus (&FilterName, pOid, i4RowStatus);
            free_oid (pOid);
            if (u1MaskFlag == CLI_TRUE)
            {
                free_oid (pFilterOid);
            }
            CLI_SET_ERR (CLI_SNMP3_INVALID_INPUT_ERR);
            return CLI_FAILURE;
        }

        if (nmhSetSnmpNotifyFilterStorageType
            (&FilterName, pOid, (INT4) u4StorageType) == SNMP_FAILURE)
        {
            nmhSetSnmpNotifyFilterRowStatus (&FilterName, pOid, i4RowStatus);
            free_oid (pOid);
            if (u1MaskFlag == CLI_TRUE)
            {
                free_oid (pFilterOid);
            }
            CLI_SET_ERR (CLI_SNMP3_INVALID_INPUT_ERR);
            return CLI_FAILURE;
        }

    }

    if (u4FilterType != SNMP_ZERO)
    {
        i4RetStatus = nmhTestv2SnmpNotifyFilterType (&u4ErrorCode,
                                                     &FilterName, pOid,
                                                     (INT4) u4FilterType);
        if (i4RetStatus == SNMP_FAILURE)
        {
            nmhSetSnmpNotifyFilterRowStatus (&FilterName, pOid, i4RowStatus);
            free_oid (pOid);
            if (u1MaskFlag == CLI_TRUE)
            {
                free_oid (pFilterOid);
            }
            CLI_SET_ERR (CLI_SNMP3_INVALID_INPUT_ERR);
            return CLI_FAILURE;
        }

        if (nmhSetSnmpNotifyFilterType (&FilterName, pOid,
                                        (INT4) u4FilterType) == SNMP_FAILURE)
        {
            nmhSetSnmpNotifyFilterRowStatus (&FilterName, pOid, i4RowStatus);
            free_oid (pOid);
            if (u1MaskFlag == CLI_TRUE)
            {
                free_oid (pFilterOid);
            }
            CLI_SET_ERR (CLI_SNMP3_INVALID_INPUT_ERR);
            return CLI_FAILURE;
        }
    }
    /* Finally make the row status as active */

    i4RetStatus = nmhTestv2SnmpNotifyFilterRowStatus (&u4ErrorCode,
                                                      &FilterName, pOid,
                                                      ACTIVE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        nmhSetSnmpNotifyFilterRowStatus (&FilterName, pOid, i4RowStatus);
        free_oid (pOid);
        if (u1MaskFlag == CLI_TRUE)
        {
            free_oid (pFilterOid);
        }
        return CLI_FAILURE;
    }
    i4RetStatus = nmhSetSnmpNotifyFilterRowStatus (&FilterName, pOid, ACTIVE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        nmhSetSnmpNotifyFilterRowStatus (&FilterName, pOid, i4RowStatus);
        free_oid (pOid);
        if (u1MaskFlag == CLI_TRUE)
        {
            free_oid (pFilterOid);
        }
        return CLI_FAILURE;
    }

    free_oid (pOid);
    pOid = NULL;

    if (u1MaskFlag == CLI_TRUE)
    {
        free_oid (pFilterOid);
        pFilterOid = NULL;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliNotifyFilterDelete                         */
/*                                                                           */
/*     DESCRIPTION      : Function to delete Filter from Notify Filter Table */
/*                                                                           */
/*     INPUT            : pu1FilterName   - Filter Name                      */
/*                        pu1OID        - Subtree OID                        */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliNotifyFilterDelete (tCliHandle CliHandle, UINT1 *pu1FilterName,
                            UINT1 *pu1OID)
{
    UINT1               au1FilterName[SNMP_MAX_ADMIN_STR_LENGTH];

    tSNMP_OCTET_STRING_TYPE FilterName;
    tSNMP_OID_TYPE     *pOid = NULL;
    UINT4               u4ErrorCode = 0;
    INT4                i4RetStatus = 0;

    UNUSED_PARAM (CliHandle);

    /* verify validity of inputs */
    if (SnmpCheckString (pu1FilterName) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_SNMP3_INVALID_INPUT_ERR);
        return CLI_FAILURE;
    }

    pOid = SNMP_AGT_GetOidFromString ((INT1 *) pu1OID);
    if (pOid == NULL)
    {
        CLI_SET_ERR (CLI_SNMP3_OID_ERR);
        return CLI_FAILURE;
    }

    MEMSET (au1FilterName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    STRNCPY (au1FilterName, pu1FilterName, (sizeof (au1FilterName) - 1));

    FilterName.pu1_OctetList = au1FilterName;
    FilterName.i4_Length = (INT4) STRLEN (au1FilterName);
    i4RetStatus = nmhTestv2SnmpNotifyFilterRowStatus (&u4ErrorCode, &FilterName,
                                                      pOid, DESTROY);
    if (i4RetStatus == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_SNMP3_FILTER_ENTRY_NOTPRESENT_ERR);
        free_oid (pOid);
        return CLI_FAILURE;
    }
    i4RetStatus = nmhSetSnmpNotifyFilterRowStatus (&FilterName, pOid, DESTROY);
    if (i4RetStatus == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_SNMP3_FILTER_ENTRY_NOTPRESENT_ERR);
        free_oid (pOid);
        return CLI_FAILURE;
    }

    free_oid (pOid);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliUsmConfig                                  */
/*                                                                           */
/*     DESCRIPTION      : Function to configure User Security Model Table    */
/*                                                                           */
/*     INPUT            : pu1UserName   - User Name                          */
/*                        i4AuthProto   - Authentication Protocol            */
/*                        pu1AuthPass   - Authentication Passwd              */
/*                        i4PrivProto   - Privacy Protocol                   */
/*                        pu1PrivPass   - Privacy Passwd                     */
/*                        u4StorageType - Storage type in memory             */
/*                        u4PrivSet     - Is Priv option set                 */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliUsmConfig (tCliHandle CliHandle, UINT1 *pu1UserName, INT4 i4AuthProto,
                   UINT1 *pu1AuthPass, INT4 i4PrivProto, UINT1 *pu1PrivPass,
                   UINT4 u4StorageType, UINT1 *pu1EngineId, UINT4 u4PrivSet)
{
    UINT1               au1UserName[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1AuthPassWord[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1PrivPassWord[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1EngineId[SNMP_MAX_STR_ENGINEID_LEN];
    INT4                i4Len = 0;
    UINT4               u4NumOctets = 0;
    UINT4               au4Protocol[SNMP_STD_AUTH_OID_LEN + 1];
    UINT4               u4ErrorCode = 0;

#ifdef FIPS_WANTED
    UINT4               u4SecurityMode = 0;
#endif
    tSnmpUsmEntry      *pUsmEntry = NULL;
    tSNMP_OCTET_STRING_TYPE UserName;
    tSNMP_OCTET_STRING_TYPE AuthPassWord;
    tSNMP_OCTET_STRING_TYPE PrivPassWord;
    tSNMP_OCTET_STRING_TYPE EngineId;
    tSNMP_OID_TYPE      Protocol;

    MEMSET (au1EngineId, 0, SNMP_MAX_STR_ENGINEID_LEN);
    EngineId.pu1_OctetList = au1EngineId;

    MEMSET (au4Protocol, 0, sizeof (au4Protocol));
    Protocol.pu4_OidList = &au4Protocol[0];

    /* verify validity of inputs */
    if ((SnmpCheckString (pu1UserName) == SNMP_FAILURE) ||
        (SnmpCheckString (pu1AuthPass) == SNMP_FAILURE) ||
        (SnmpCheckString (pu1PrivPass) == SNMP_FAILURE))
    {
        CLI_SET_ERR (CLI_SNMP3_INVALID_INPUT_ERR);
        return CLI_FAILURE;
    }

    UNUSED_PARAM (CliHandle);

    if ((STRCMP (pu1AuthPass, "none") == 0) && (i4AuthProto != SNMP_ONE))
    {
        CLI_SET_ERR (CLI_SNMP3_AUTH_PASWD_ERR);
        return CLI_FAILURE;
    }

#ifdef FIPS_WANTED
    u4SecurityMode = FipsGetFipsCurrOperMode ();

    if (u4SecurityMode == LEGACY_MODE)
    {
        if (SnmpIsPrivSecAlgoSupported (i4PrivProto) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_SNMP3_NEW_ENTRY_PRIV_LEGACY_ERR);
            return CLI_FAILURE;
        }
    }
    else if (u4SecurityMode == FIPS_MODE)
    {
        if (SnmpIsAuthSecAlgoSupported (i4AuthProto) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_SNMP3_NEW_ENTRY_AUTH_FIPS_ERR);
            return CLI_FAILURE;
        }
        if (SnmpIsPrivSecAlgoSupported (i4PrivProto) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_SNMP3_NEW_ENTRY_PRIV_FIPS_ERR);
            return CLI_FAILURE;
        }
    }
    else if (u4SecurityMode == CNSA_MODE)
    {
        if (SnmpIsAuthSecAlgoSupported (i4AuthProto) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_SNMP3_NEW_ENTRY_AUTH_CNSA_ERR);
            return CLI_FAILURE;
        }
        if (SnmpIsPrivSecAlgoSupported (i4PrivProto) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_SNMP3_NEW_ENTRY_PRIV_CNSA_ERR);
            return CLI_FAILURE;
        }
    }
#endif

    if ((STRCMP (pu1AuthPass, "none") != 0) ||
        (STRCMP (pu1PrivPass, "none") != 0))
    {
        i4Len = (INT4) STRLEN (pu1AuthPass);
        if ((i4AuthProto == SNMP_MD5) && ((i4Len < SNMP_MIN_USM_KEY_LENGTH) ||
                                          (i4Len > SNMP_MAX_ADMIN_STR_LENGTH)))
        {
            CLI_SET_ERR (CLI_SNMP3_AUTH_PASWD_LEN_ERR);
            return CLI_FAILURE;
        }

        if (((i4AuthProto == SNMP_SHA) || (i4AuthProto == SNMP_SHA256)
             || (i4AuthProto == SNMP_SHA384))
            && ((i4Len < SNMP_MIN_USM_KEY_LENGTH)
                || (i4Len > SNMP_MAX_USM_KEY_SHA_LENGTH)))
        {
            CLI_SET_ERR (CLI_SNMP3_AUTH_PASWD_LEN_ERR);
            return CLI_FAILURE;
        }

        i4Len = (INT4) STRLEN (pu1PrivPass);
        if (((i4PrivProto == SNMP_DES_CBC) || (i4PrivProto == SNMP_AESCFB128)
             || (i4PrivProto == SNMP_AESCFB256))
            && ((i4Len < SNMP_MIN_USM_KEY_LENGTH)
                || (i4Len > SNMP_MAX_ADMIN_STR_LENGTH)))
        {
            CLI_SET_ERR (CLI_SNMP3_PRIV_PASWD_LEN_ERR);
            return CLI_FAILURE;
        }
    }

    MEMSET (au1UserName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    STRNCPY (au1UserName, pu1UserName, (sizeof (au1UserName) - 1));

    UserName.pu1_OctetList = au1UserName;
    UserName.i4_Length = (INT4) STRLEN (au1UserName);

    MEMSET (au1AuthPassWord, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    STRNCPY (au1AuthPassWord, pu1AuthPass, (sizeof (au1AuthPassWord) - 1));

    AuthPassWord.pu1_OctetList = au1AuthPassWord;
    AuthPassWord.i4_Length = (INT4) STRLEN (au1AuthPassWord);

    MEMSET (au1PrivPassWord, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    STRNCPY (au1PrivPassWord, pu1PrivPass, (sizeof (au1PrivPassWord) - 1));

    PrivPassWord.pu1_OctetList = au1PrivPassWord;

    PrivPassWord.i4_Length = (INT4) STRLEN (au1PrivPassWord);

    /* 
     *   Multiple instance support not avialable, So we have to set only 
     *   default engine-id.
     */

    if (STRCMP (pu1EngineId, "") != 0)
    {

        u4NumOctets = SnmpGetNumOctets ((INT1 *) pu1EngineId);
        if ((u4NumOctets < SNMP_MIN_ENGINE_ID_LEN) ||
            (u4NumOctets > SNMP_MAX_ENGINE_ID_LEN))
        {
            CliPrintf (CliHandle,
                       "\r%% Valid EngineID Length is in the range 5-32 octets \r\n");
            return CLI_FAILURE;
        }

        if (SnmpConvertStringToOctet ((INT1 *) pu1EngineId, &EngineId) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Error in the EngineID entered\r\n");
            return CLI_FAILURE;
        }

    }
    else
    {
        SNMPCopyOctetString (&EngineId, &gSnmpEngineID);
    }

    /* check if an entry exists already */
    if ((pUsmEntry = SNMPGetUsmEntry (&EngineId, &UserName)) != NULL)
    {
        nmhSetUsmUserStatus (&EngineId, &UserName, SNMP_ROWSTATUS_NOTINSERVICE);

        /* entry exists - if any value in table is changed, modify */
        if ((u4StorageType != 0) &&
            (u4StorageType != pUsmEntry->u4UsmUserStorageType))
        {
            /* Storage Type modified, save new value */
            if (nmhTestv2UsmUserStorageType (&u4ErrorCode, &EngineId,
                                             &UserName, (INT4) u4StorageType)
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_SNMP3_ENTRY_MODIFICATION_ERR);
                nmhSetUsmUserStatus (&EngineId, &UserName,
                                     SNMP_ROWSTATUS_ACTIVE);
                return CLI_FAILURE;
            }

            if (nmhSetUsmUserStorageType (&EngineId, &UserName,
                                          (INT4) u4StorageType) == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_SNMP3_ENTRY_MODIFICATION_ERR);
                nmhSetUsmUserStatus (&EngineId, &UserName,
                                     SNMP_ROWSTATUS_ACTIVE);
                return CLI_FAILURE;
            }
        }

        nmhSetUsmUserStatus (&EngineId, &UserName, SNMP_ROWSTATUS_ACTIVE);
        /* Entry modified */
    }
    else
    {
        /* entry does not exist - ADD */
        if (nmhSetUsmUserStatus (&EngineId, &UserName,
                                 SNMP_ROWSTATUS_CREATEANDWAIT) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_SNMP3_NEW_ENTRY_ERR);
            return CLI_FAILURE;
        }
        if ((pUsmEntry = SNMPGetUsmEntry (&EngineId, &UserName)) == NULL)
        {
            CLI_SET_ERR (CLI_SNMP3_NEW_ENTRY_ERR);
            return CLI_FAILURE;
        }

        if (u4StorageType == 0)
        {
            /* Storage type not given  - make default as non-volatile type */
            u4StorageType = SNMP3_STORAGE_TYPE_NONVOLATILE;
        }

        if (nmhTestv2UsmUserStorageType (&u4ErrorCode, &EngineId,
                                         &UserName, (INT4) u4StorageType)
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_SNMP3_NEW_ENTRY_ERR);
            nmhSetUsmUserStatus (&EngineId, &UserName, SNMP_ROWSTATUS_DESTROY);
            return CLI_FAILURE;
        }

        if (nmhSetUsmUserStorageType (&EngineId, &UserName,
                                      (INT4) u4StorageType) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_SNMP3_NEW_ENTRY_ERR);
            nmhSetUsmUserStatus (&EngineId, &UserName, SNMP_ROWSTATUS_DESTROY);
            return CLI_FAILURE;
        }

        SNMPCopyOctetString (&(pUsmEntry->UsmUserAuthPassword),
                             &(AuthPassWord));
        SNMPCopyOctetString (&(pUsmEntry->UsmUserPrivPassword),
                             &(PrivPassWord));
    }

    if ((i4AuthProto != SNMP_NO_AUTH) && (i4PrivProto == SNMP_NO_PRIV))
    {
        MEMCPY (Protocol.pu4_OidList, au4StdAuthOid,
                SNMP_STD_AUTH_OID_LEN * sizeof (UINT4));
        Protocol.pu4_OidList[SNMP_STD_AUTH_OID_LEN] = (UINT4) i4AuthProto;
        Protocol.u4_Length = SNMP_STD_AUTH_OID_LEN + 1;

        if (nmhSetUsmUserAuthProtocol (&EngineId, &UserName, &Protocol)
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_SNMP3_NEW_ENTRY_ERR);
            nmhSetUsmUserStatus (&EngineId, &UserName, SNMP_ROWSTATUS_DESTROY);
            return CLI_FAILURE;
        }

        if (u4PrivSet == 1)
        {
            MEMCPY (Protocol.pu4_OidList, au4StdPrivOid,
                    SNMP_STD_PRIV_OID_LEN * sizeof (UINT4));
            Protocol.pu4_OidList[SNMP_STD_PRIV_OID_LEN] = (UINT4) i4PrivProto;
            Protocol.u4_Length = SNMP_STD_PRIV_OID_LEN + 1;

            if (nmhSetUsmUserPrivProtocol (&EngineId, &UserName, &Protocol)
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_SNMP3_NEW_ENTRY_ERR);
                nmhSetUsmUserStatus (&EngineId, &UserName,
                                     SNMP_ROWSTATUS_DESTROY);
                return CLI_FAILURE;
            }
        }

        if (nmhSetUsmUserAuthKeyChange (&EngineId, &UserName,
                                        &AuthPassWord) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_SNMP3_NEW_ENTRY_ERR);
            nmhSetUsmUserStatus (&EngineId, &UserName, SNMP_ROWSTATUS_DESTROY);
            return CLI_FAILURE;
        }
    }
    else if ((i4AuthProto != SNMP_NO_AUTH) && (i4PrivProto != SNMP_NO_PRIV))
    {
        MEMCPY (Protocol.pu4_OidList, au4StdAuthOid,
                SNMP_STD_AUTH_OID_LEN * sizeof (UINT4));
        Protocol.pu4_OidList[SNMP_STD_AUTH_OID_LEN] = (UINT4) i4AuthProto;
        Protocol.u4_Length = SNMP_STD_AUTH_OID_LEN + 1;

        if (nmhSetUsmUserAuthProtocol (&EngineId, &UserName, &Protocol)
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_SNMP3_NEW_ENTRY_ERR);
            nmhSetUsmUserStatus (&EngineId, &UserName, SNMP_ROWSTATUS_DESTROY);
            return CLI_FAILURE;
        }

        MEMCPY (Protocol.pu4_OidList, au4StdPrivOid,
                SNMP_STD_PRIV_OID_LEN * sizeof (UINT4));
        Protocol.pu4_OidList[SNMP_STD_PRIV_OID_LEN] = (UINT4) i4PrivProto;
        Protocol.u4_Length = SNMP_STD_PRIV_OID_LEN + 1;

        if (nmhSetUsmUserPrivProtocol (&EngineId, &UserName, &Protocol)
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_SNMP3_NEW_ENTRY_ERR);
            nmhSetUsmUserStatus (&EngineId, &UserName, SNMP_ROWSTATUS_DESTROY);
            return CLI_FAILURE;
        }

        if (nmhSetUsmUserAuthKeyChange (&EngineId, &UserName,
                                        &AuthPassWord) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_SNMP3_NEW_ENTRY_ERR);
            nmhSetUsmUserStatus (&EngineId, &UserName, SNMP_ROWSTATUS_DESTROY);
            return CLI_FAILURE;
        }

        if (nmhSetUsmUserPrivKeyChange (&EngineId, &UserName,
                                        &PrivPassWord) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_SNMP3_NEW_ENTRY_ERR);
            nmhSetUsmUserStatus (&EngineId, &UserName, SNMP_ROWSTATUS_DESTROY);
            return CLI_FAILURE;
        }
    }

    nmhSetUsmUserStatus (&EngineId, &UserName, SNMP_ROWSTATUS_ACTIVE);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliUsmDelete                                  */
/*                                                                           */
/*     DESCRIPTION      : This function Deletes SNMPv3 User                  */
/*                                                                           */
/*     INPUT            : pu1UserName   - SNMP user                          */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliUsmDelete (tCliHandle CliHandle, UINT1 *pu1UserName, UINT1 *pu1EngineId)
{
    UINT1               au1UserName[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1EngineId[SNMP_MAX_STR_ENGINEID_LEN];
    UINT4               u4NumOctets = 0;

    tSNMP_OCTET_STRING_TYPE UserName;
    tSNMP_OCTET_STRING_TYPE EngineId;

    MEMSET (au1EngineId, 0, SNMP_MAX_STR_ENGINEID_LEN);
    EngineId.pu1_OctetList = au1EngineId;

    MEMSET (au1UserName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    STRNCPY (au1UserName, pu1UserName, (sizeof (au1UserName) - 1));

    if (STRCMP (pu1EngineId, "") != 0)
    {

        u4NumOctets = SnmpGetNumOctets ((INT1 *) pu1EngineId);
        if ((u4NumOctets < SNMP_MIN_ENGINE_ID_LEN) ||
            (u4NumOctets > SNMP_MAX_ENGINE_ID_LEN))
        {
            CliPrintf (CliHandle,
                       "\r%% Valid EngineID Length is in the range 5-32 octets \r\n");
            return CLI_FAILURE;
        }

        if (SnmpConvertStringToOctet ((INT1 *) pu1EngineId, &EngineId) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Error in the EngineID entered\r\n");
            return CLI_FAILURE;
        }

    }
    else
    {
        SNMPCopyOctetString (&EngineId, &gSnmpEngineID);
    }

    UserName.pu1_OctetList = au1UserName;
    UserName.i4_Length = (INT4) STRLEN (au1UserName);

    if (nmhSetUsmUserStatus (&EngineId, &UserName, SNMP_ROWSTATUS_DESTROY)
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_SNMP3_USER_NOTPRESENT_ERR);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliNotifyConfig                               */
/*                                                                           */
/*     DESCRIPTION      : Function to configure the Notify Name              */
/*                                                                           */
/*     INPUT            : pu1NotifyName -  Notify Name                       */
/*                        pu1Tag        -  Notify Tag value                  */
/*                        u4NotifyType  -  Notification type                 */
/*                        u4StorageType - Storage type in memory             */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
Snmp3CliNotifyConfig (tCliHandle CliHandle, UINT1 *pu1NotifyName,
                      UINT1 *pu1Tag, UINT4 u4NotifyType, UINT4 u4StorageType)
{
    UINT4               u4ErrCode = 0;

    UNUSED_PARAM (CliHandle);

    if (Snmp3NotifyConfig (pu1NotifyName, pu1Tag, u4NotifyType, u4StorageType,
                           &u4ErrCode) == SNMP_FAILURE)
    {
        CLI_SET_ERR (u4ErrCode);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliNotifyDelete                               */
/*                                                                           */
/*     DESCRIPTION      : This function Deletes SNMPv3 Host                  */
/*                                                                           */
/*     INPUT            : pu1HostIpAddr       - Host identifer address       */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliNotifyDelete (tCliHandle CliHandle, UINT1 *pu1NotifyName)
{
    UINT1               au1NotifyName[SNMP_MAX_ADMIN_STR_LENGTH];

    tSNMP_OCTET_STRING_TYPE NotifyName;

    UNUSED_PARAM (CliHandle);

    /* verify validity of inputs */
    if (SnmpCheckString (pu1NotifyName) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_SNMP3_INVALID_INPUT_ERR);
        return CLI_FAILURE;
    }

    MEMSET (au1NotifyName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    STRNCPY (au1NotifyName, pu1NotifyName, (sizeof (au1NotifyName) - 1));

    NotifyName.pu1_OctetList = au1NotifyName;
    NotifyName.i4_Length = (INT4) STRLEN (au1NotifyName);

    /* Delete entry */
    if (nmhSetSnmpNotifyRowStatus (&NotifyName, SNMP_ROWSTATUS_DESTROY)
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_SNMP3_NOTIF_NOTPRESENT_ERR);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliSetListenTrapPort                          */
/*                                                                           */
/*     DESCRIPTION      : This function configure the agent's listen port    */
/*                                                                           */
/*     INPUT            : u2ListenTrapPort   - Listen port over which agent  */
/*                                             will listen                   */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
Snmp3CliSetListenTrapPort (tCliHandle CliHandle, UINT2 u2ListenTrapPort)
{

    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);    /*kloc */

    if (nmhTestv2SnmpListenTrapPort (&u4ErrorCode, (UINT4) u2ListenTrapPort)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetSnmpListenTrapPort ((UINT4) u2ListenTrapPort) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliSetProxyListenTrapPort                          */
/*                                                                           */
/*     DESCRIPTION      : This function configure the agent's listen port    */
/*                                                                           */
/*     INPUT            : u2ListenTrapPort   - Listen port over which agent  */
/*                                             will listen                   */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
Snmp3CliSetProxyListenTrapPort (tCliHandle CliHandle, UINT4 u4ListenTrapPort)
{

    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);    /*kloc */

    if (nmhTestv2SnmpProxyListenTrapPort (&u4ErrorCode, u4ListenTrapPort)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetSnmpProxyListenTrapPort (u4ListenTrapPort) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliSetTcpStatus                               */
/*                                                                           */
/*     DESCRIPTION      : This function Enables/Disables sending SNMP        */
/*                        messages over TCP                                  */
/*                                                                           */
/*     INPUT            : i4TcpStatus - Status of SNMP over TCP              */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
Snmp3CliSetTcpStatus (tCliHandle CliHandle, UINT4 i4TcpStatus)
{

    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);    /*kloc */

    if (nmhTestv2SnmpOverTcpStatus (&u4ErrorCode, (INT4) i4TcpStatus) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetSnmpOverTcpStatus (i4TcpStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliSetListenTcpPort                           */
/*                                                                           */
/*     DESCRIPTION      : This function configure the agent's tcp listen port*/
/*                                                                           */
/*     INPUT            : u2ListenTrapPort   - Tcp Listen port over which    */
/*                                             agent will listen for SNMP msg*/
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
Snmp3CliSetListenTcpPort (tCliHandle CliHandle, UINT2 u2ListenTcpPort)
{

    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);    /*kloc */

    if (nmhTestv2SnmpListenTcpPort (&u4ErrorCode, (UINT4) u2ListenTcpPort)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetSnmpListenTcpPort ((UINT4) u2ListenTcpPort) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliSetTcpTrapStatus                           */
/*                                                                           */
/*     DESCRIPTION      : This function Enables/Disables sending SNMP        */
/*                        trap messages over TCP                             */
/*                                                                           */
/*     INPUT            : u4TcpTrapStatus - Status of SNMP Trap over TCP     */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
Snmp3CliSetTcpTrapStatus (tCliHandle CliHandle, UINT4 u4TcpTrapStatus)
{

    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);    /*kloc */

    if (nmhTestv2SnmpTrapOverTcpStatus (&u4ErrorCode, (INT4) u4TcpTrapStatus)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetSnmpTrapOverTcpStatus (u4TcpTrapStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliSetListenTcpTrapPort                       */
/*                                                                           */
/*     DESCRIPTION      : This function configure the agent's tcp listen port*/
/*                                                                           */
/*     INPUT            : u2ListenTrapPort   - Tcp Listen port over which    */
/*                                             agent will listen             */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
Snmp3CliSetListenTcpTrapPort (tCliHandle CliHandle, UINT2 u2ListenTrapPort)
{

    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);    /*kloc */

    if (nmhTestv2SnmpListenTcpTrapPort (&u4ErrorCode, (UINT4) u2ListenTrapPort)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetSnmpListenTcpTrapPort ((UINT4) u2ListenTrapPort) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliSetListenAgentPort                         */
/*                                                                           */
/*     DESCRIPTION      : This function configure the agent's listen port    */
/*                                                                           */
/*     INPUT            : u4ListenAgentPort   - Listen port over which agent  */
/*                                             will listen                   */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
Snmp3CliSetListenAgentPort (tCliHandle CliHandle, UINT4 u4ListenAgentPort)
{

    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsSnmpListenAgentPort (&u4ErrorCode, u4ListenAgentPort)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n Port No is out of Range\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsSnmpListenAgentPort (u4ListenAgentPort) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r Error: Socket creation failed.The port already in use \r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliShowSnmpTcpDetails                         */
/*                                                                           */
/*     DESCRIPTION      : This function Displays SNMP over Tcp Configuration */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains Information to Display       */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliShowSnmpTcpDetails (tCliHandle CliHandle)
{
    INT4                i4SnmpTcpStatus = 0;
    INT4                i4SnmpTrapTcpStatus = 0;
    UINT4               u4SnmpListenTcpPort = 0;
    UINT4               u4SnmpListenTcpTrapPort = 0;

    nmhGetSnmpOverTcpStatus (&i4SnmpTcpStatus);
    if (i4SnmpTcpStatus == SNMP3_OVER_TCP_ENABLE)
    {
        CliPrintf (CliHandle, "\r\nsnmp over tcp enabled \r\n");

    }
    else
    {
        CliPrintf (CliHandle, "\r\nsnmp over tcp disabled \r\n");
    }

    nmhGetSnmpTrapOverTcpStatus (&i4SnmpTrapTcpStatus);
    if (i4SnmpTrapTcpStatus == SNMP3_TRAP_OVER_TCP_ENABLE)
    {
        CliPrintf (CliHandle, "\r\nsnmp trap over tcp enabled \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r\nsnmp trap over tcp disabled \r\n");
    }
    nmhGetSnmpListenTcpPort (&u4SnmpListenTcpPort);
    if (u4SnmpListenTcpPort)
    {
        CliPrintf (CliHandle, "\r\nsnmp listen tcp port %d\r\n",
                   u4SnmpListenTcpPort);
    }
    nmhGetSnmpListenTcpTrapPort (&u4SnmpListenTcpTrapPort);
    if (u4SnmpListenTcpTrapPort)
    {
        CliPrintf (CliHandle, "\r\nsnmp listen tcp trap port %d\r\n",
                   u4SnmpListenTcpTrapPort);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliShowDetails                                */
/*                                                                           */
/*     DESCRIPTION      : This function Displays SNMP communication status   */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains Information to Display       */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliShowDetails (tCliHandle CliHandle)
{
    INT4                i4SnmpAgentStatus = 0;

    nmhGetSnmpAgentControl (&i4SnmpAgentStatus);

    if (i4SnmpAgentStatus == SNMP_AGENT_ENABLE)
    {
        CliPrintf (CliHandle, "\r\n %d SNMP Packets Input\r\n",
                   gSnmpStat.u4SnmpInPkts);
        CliPrintf (CliHandle, "     %d Bad SNMP Version errors\r\n",
                   gSnmpStat.u4SnmpInBadVersions);
        CliPrintf (CliHandle, "     %d Unknown community name\r\n",
                   gSnmpStat.u4SnmpInBadCommunityNames);
        CliPrintf (CliHandle, "     %d Get request PDUs\r\n",
                   gSnmpStat.u4SnmpInGetRequests);
        CliPrintf (CliHandle, "     %d Get Next PDUs\r\n",
                   gSnmpStat.u4SnmpInGetNexts);
        CliPrintf (CliHandle, "     %d Set request PDUs\r\n",
                   gSnmpStat.u4SnmpInSetRequests);

        CliPrintf (CliHandle, "\r\n %d SNMP Packets Output\r\n",
                   gSnmpStat.u4SnmpOutPkts);
        CliPrintf (CliHandle, "     %d Too big errors\r\n",
                   gSnmpStat.u4SnmpOutTooBigs);
        CliPrintf (CliHandle, "     %d No such name errors\r\n",
                   gSnmpStat.u4SnmpOutNoSuchNames);
        CliPrintf (CliHandle, "     %d Bad value errors\r\n",
                   gSnmpStat.u4SnmpOutBadValues);
        CliPrintf (CliHandle, "     %d General errors\r\n",
                   gSnmpStat.u4SnmpOutGenErrs);
        CliPrintf (CliHandle, "     %d Trap PDUs\r\n",
                   gSnmpStat.u4SnmpOutTraps);

        CliPrintf (CliHandle, "\r\n %d SNMP Rollback failures\r\n",
                   gSnmpStat.u4SnmpRollbackErrs);

        CliPrintf (CliHandle, "\r\n SNMP Manager-role output packets \r\n");
        CliPrintf (CliHandle, "     %d Drops\r\n", gSnmpStat.u4SnmpSilentDrops);

        CliPrintf (CliHandle, "\r\n SNMP Informs: \r\n");
        CliPrintf (CliHandle, "     %d Inform Requests generated\r\n",
                   gSnmpStat.u4SnmpOutInformRequests);
        CliPrintf (CliHandle, "     %d Inform Responses received\r\n",
                   gSnmpStat.u4SnmpInInformResponses);
        CliPrintf (CliHandle, "     %d Inform messages Dropped\r\n",
                   gSnmpStat.u4SnmpInformDrops);
        CliPrintf (CliHandle,
                   "     %d Inform Requests awaiting Acknowledgement\r\n",
                   gSnmpStat.u4SnmpInformAwaitAck);
        CliPrintf (CliHandle, "\r\n SNMP Trap Listen Port is %d\r\n",
                   gSnmpSystem.u2ListenTrapPort);
        CliPrintf (CliHandle, "\r\n snmp agent port : %d\r\n ",
                   gSnmpSystem.u2ListenAgentPort);
    }
    else
    {
        CliPrintf (CliHandle, "\r Snmp Agent Disabled\r\n");
    }
    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliShowProxyListenTrapPort                              */
/*                                                                           */
/*     DESCRIPTION      : This function Displays SNMP communication info     */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains Information to Display       */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
Snmp3CliShowProxyListenTrapPort (tCliHandle CliHandle)
{
    UINT4               u4Port = 0;
    if (nmhGetSnmpProxyListenTrapPort (&u4Port) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "\r\n snmp-server proxy-udp-port : %d\r\n ", u4Port);

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliShowCommunity                              */
/*                                                                           */
/*     DESCRIPTION      : This function Displays SNMP communication info     */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains Information to Display       */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliShowCommunity (tCliHandle CliHandle)
{
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT1                ai1Name[SNMP_MAX_OCTETSTRING_SIZE - 1];
    INT1                ai1EngineId[SNMP_MAX_STR_ENGINEID_LEN];

    tCommunityMappingEntry *pCommEntry = NULL;

    if ((pCommEntry = SNMPGetFirstCommunity ()) == NULL)
    {
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r\n");
    do
    {
        MEMSET (ai1EngineId, 0, SNMP_MAX_STR_ENGINEID_LEN);
        SNPRINTF ((CHR1 *) ai1Name,
                  (UINT4) (pCommEntry->CommIndex.i4_Length + 1),
                  (const CHR1 *) pCommEntry->CommIndex.pu1_OctetList);
        CliPrintf (CliHandle, "Community Index : %s \r\n", ai1Name);

        SNPRINTF ((CHR1 *) ai1Name,
                  (UINT4) (pCommEntry->CommunityName.i4_Length + 1),
                  (const CHR1 *) pCommEntry->CommunityName.pu1_OctetList);
        CliPrintf (CliHandle, "Community Name  : %s \r\n", ai1Name);

        SNPRINTF ((CHR1 *) ai1Name,
                  (UINT4) (pCommEntry->SecurityName.i4_Length + 1),
                  (const CHR1 *) pCommEntry->SecurityName.pu1_OctetList);
        CliPrintf (CliHandle, "Security Name   : %s \r\n", ai1Name);

        SNPRINTF ((CHR1 *) ai1Name,
                  (UINT4) (pCommEntry->ContextName.i4_Length + 1),
                  (const CHR1 *) pCommEntry->ContextName.pu1_OctetList);
        CliPrintf (CliHandle, "Context Name    : %s \r\n", ai1Name);

        SnmpConvertOctetToString (&pCommEntry->ContextEngineID, ai1EngineId);
        CliPrintf (CliHandle, "Context EngineID: %s\r\n", ai1EngineId);

        SNPRINTF ((CHR1 *) ai1Name,
                  (UINT4) (pCommEntry->TransTag.i4_Length + 1),
                  (const CHR1 *) pCommEntry->TransTag.pu1_OctetList);
        CliPrintf (CliHandle, "Transport Tag   : %s \r\n", ai1Name);

        CliPrintf (CliHandle, "Storage Type    : %s\r\n",
                   au1Storage[pCommEntry->i4Storage]);
        CliPrintf (CliHandle, "Row Status      : %s\r\n",
                   au1RowStatus[pCommEntry->i4Status - 1]);

        u4PagingStatus = (UINT4) CliPrintf (CliHandle,
                                            "------------------------------ \r\n");

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            break;
        }

        pCommEntry = SNMPGetNextCommunityEntry (&(pCommEntry->CommIndex));
    }
    while (pCommEntry != NULL);
    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliShowGroup                                  */
/*                                                                           */
/*     DESCRIPTION      : This function Displays SNMP Groups                 */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains Information to Display       */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliShowGroup (tCliHandle CliHandle)
{
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT1                ai1Name[SNMP_MAX_OCTETSTRING_SIZE - 1];
    tSecGrp            *pSecGrpEntry = NULL;

    if ((pSecGrpEntry = VACMGetFirstSecGrp ()) == NULL)
    {
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r\n");
    do
    {
        CliPrintf (CliHandle, "Security Model : %s \r\n",
                   au1SecModel[pSecGrpEntry->i4SecModel - 1]);

        SNPRINTF ((CHR1 *) ai1Name,
                  (UINT4) (pSecGrpEntry->SecName.i4_Length + 1),
                  (const CHR1 *) pSecGrpEntry->SecName.pu1_OctetList);
        CliPrintf (CliHandle, "Security Name  : %s \r\n", ai1Name);

        SNPRINTF ((CHR1 *) ai1Name, (UINT4) (pSecGrpEntry->Group.i4_Length + 1),
                  (const CHR1 *) pSecGrpEntry->Group.pu1_OctetList);
        CliPrintf (CliHandle, "Group Name     : %s \r\n", ai1Name);

        CliPrintf (CliHandle, "Storage Type   : %s \r\n",
                   au1Storage[pSecGrpEntry->i4Storage]);
        CliPrintf (CliHandle, "Row Status     : %s \r\n",
                   au1RowStatus[pSecGrpEntry->i4Status - 1]);
        u4PagingStatus = (UINT4) CliPrintf (CliHandle,
                                            "------------------------------ \r\n");

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            break;
        }

        pSecGrpEntry = VACMGetNextSecGrp (pSecGrpEntry->i4SecModel,
                                          &(pSecGrpEntry->SecName));
    }
    while (pSecGrpEntry != NULL);

    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliShowAccessGroup                            */
/*                                                                           */
/*     DESCRIPTION      : This function Displays SNMP Access Groups          */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains Information to Display       */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliShowAccessGroup (tCliHandle CliHandle)
{
    INT1                ai1Name[SNMP_MAX_OCTETSTRING_SIZE - 1];
    UINT4               u4PagingStatus = CLI_SUCCESS;

    tAccessEntry       *pFirstAccess = NULL;

    if ((pFirstAccess = VACMGetFirstAccess ()) == NULL)
    {
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r\n");
    do
    {
        SNPRINTF ((CHR1 *) ai1Name,
                  (UINT4) (pFirstAccess->pGroup->i4_Length + 1),
                  (const CHR1 *) pFirstAccess->pGroup->pu1_OctetList);
        CliPrintf (CliHandle, "Group Name   : %s \r\n", ai1Name);

        if (pFirstAccess->ReadName.i4_Length == 0)
        {
            CliPrintf (CliHandle, "Read View    : \r\n");
        }
        else
        {
            SNPRINTF ((CHR1 *) ai1Name,
                      (UINT4) (pFirstAccess->ReadName.i4_Length + 1),
                      (const CHR1 *) pFirstAccess->ReadName.pu1_OctetList);
            CliPrintf (CliHandle, "Read View    : %s\r\n", ai1Name);
        }
        if (pFirstAccess->WriteName.i4_Length == 0)
        {
            CliPrintf (CliHandle, "Write View   : \r\n");
        }
        else
        {
            SNPRINTF ((CHR1 *) ai1Name,
                      (UINT4) (pFirstAccess->WriteName.i4_Length + 1),
                      (const CHR1 *) pFirstAccess->WriteName.pu1_OctetList);
            CliPrintf (CliHandle, "Write View   : %s\r\n", ai1Name);
        }
        if (pFirstAccess->NotifyName.i4_Length == 0)
        {
            CliPrintf (CliHandle, "Notify View  : \r\n");
        }
        else
        {
            SNPRINTF ((CHR1 *) ai1Name,
                      (UINT4) (pFirstAccess->NotifyName.i4_Length + 1),
                      (const CHR1 *) pFirstAccess->NotifyName.pu1_OctetList);
            CliPrintf (CliHandle, "Notify View  : %s\r\n", ai1Name);
        }
        CliPrintf (CliHandle, "Storage Type : %s\r\n",
                   au1Storage[pFirstAccess->i4Storage]);
        CliPrintf (CliHandle, "Row Status   : %s \r\n",
                   au1RowStatus[pFirstAccess->i4Status - 1]);
        u4PagingStatus = (UINT4) CliPrintf (CliHandle,
                                            "------------------------------ \r\n");

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            break;
        }

        pFirstAccess = VACMGetNextAccess (pFirstAccess->pGroup,
                                          &(pFirstAccess->Context),
                                          pFirstAccess->i4SecModel,
                                          pFirstAccess->i4SecLevel);
    }
    while (pFirstAccess != NULL);

    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliShowEngineId                               */
/*                                                                           */
/*     DESCRIPTION      : This function Displays SNMP Engine Identifier      */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains Information to Display       */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliShowEngineId (tCliHandle CliHandle)
{
    INT1                ai1Name[SNMP_MAX_STR_ENGINEID_LEN + 1];

    MEMSET (ai1Name, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);
    SnmpConvertOctetToString (&gSnmpEngineID, ai1Name);

    CliPrintf (CliHandle, "\r\nEngineId: %s\r\n\r\n", ai1Name);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliShowViewTree                               */
/*                                                                           */
/*     DESCRIPTION      : This function Displays SNMP View Trees             */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains Information to Display       */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliShowViewTree (tCliHandle CliHandle)
{
    INT1                ai1Name[SNMP_MAX_ADMIN_STR_LENGTH - 1];
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4OidCnt = 0;
    INT1                ai1OID[SNMP_MAX_OCTETSTRING_SIZE];

    tViewTree          *pViewTree = NULL;

    if ((pViewTree = VACMViewTreeGetFirst ()) == NULL)
    {
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r\n");
    do
    {
        SNPRINTF ((CHR1 *) ai1Name, (UINT4) (pViewTree->pName->i4_Length + 1),
                  (const CHR1 *) pViewTree->pName->pu1_OctetList);
        CliPrintf (CliHandle, "View Name    : %s \r\n", ai1Name);

        MEMSET (ai1OID, 0, SNMP_MAX_OCTETSTRING_SIZE);
        if (pViewTree->SubTree.u4_Length >= SNMP_ONE)
        {
            for (u4OidCnt = 0; u4OidCnt < (pViewTree->SubTree.u4_Length - 1);
                 u4OidCnt++)
            {
                SPRINTF ((CHR1 *) ai1OID + STRLEN (ai1OID), "%u.",
                         pViewTree->SubTree.pu4_OidList[u4OidCnt]);
            }
            SPRINTF ((CHR1 *) ai1OID + STRLEN (ai1OID), "%u",
                     pViewTree->SubTree.pu4_OidList[u4OidCnt]);
        }

        CliPrintf (CliHandle, "Subtree OID  : %s \r\n", ai1OID);

        MEMSET (ai1OID, 0, SNMP_MAX_OCTETSTRING_SIZE);
        if (pViewTree->Mask.i4_Length >= SNMP_ONE)
        {
            for (u4OidCnt = 0;
                 u4OidCnt < (UINT4) (pViewTree->Mask.i4_Length - 1); u4OidCnt++)
            {
                SPRINTF ((CHR1 *) ai1OID + STRLEN (ai1OID), "%u.",
                         pViewTree->Mask.pu1_OctetList[u4OidCnt]);
            }
            SPRINTF ((CHR1 *) ai1OID + STRLEN (ai1OID), "%u",
                     pViewTree->Mask.pu1_OctetList[u4OidCnt]);
        }

        CliPrintf (CliHandle, "Subtree Mask : %s \r\n", ai1OID);
        CliPrintf (CliHandle, "View Type    : %s \r\n",
                   au1ViewType[pViewTree->i4Type - 1]);
        CliPrintf (CliHandle, "Storage Type : %s\r\n",
                   au1Storage[pViewTree->i4Storage]);
        CliPrintf (CliHandle, "Row Status   : %s \r\n",
                   au1RowStatus[pViewTree->i4Status - 1]);
        u4PagingStatus = (UINT4) CliPrintf (CliHandle,
                                            "------------------------------ \r\n");

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            break;
        }

        pViewTree = VACMViewTreeGetNext (pViewTree->pName,
                                         &(pViewTree->SubTree));
    }
    while (pViewTree != NULL);
    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliShowTargetAddr                             */
/*                                                                           */
/*     DESCRIPTION      : Function to display the Target Address Table       */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains Information to Display       */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliShowTargetAddr (tCliHandle CliHandle)
{
    INT1                ai1Name[SNMP_MAX_OCTETSTRING_SIZE - 1];
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               au1IpAddr[60];
    tSnmpTgtAddrEntry  *pFirstTarget = NULL;
    if ((pFirstTarget = SNMPGetFirstTgtAddrEntry ()) == NULL)
    {
        return CLI_SUCCESS;
    }
    CliPrintf (CliHandle, "\r\n");

    do
    {
        SNPRINTF ((CHR1 *) ai1Name,
                  (UINT4) (pFirstTarget->AddrName.i4_Length + 1),
                  (const CHR1 *) pFirstTarget->AddrName.pu1_OctetList);
        CliPrintf (CliHandle, "Target Address Name : %s \r\n", ai1Name);
        if (pFirstTarget->b1HostName == SNMP_TRUE)
        {
            CliPrintf (CliHandle, "Domain Name         : %s \r\n",
                       pFirstTarget->au1Hostname);
        }

        if (pFirstTarget->Address.i4_Length == SNMP_IPADDR_LEN)
        {
            INET_NTOP (AF_INET,
                       pFirstTarget->Address.pu1_OctetList,
                       au1IpAddr, INET_ADDRSTRLEN);
            CliPrintf (CliHandle, "IP Address          : %s \r\n", au1IpAddr);
            CliPrintf (CliHandle, "Port                : %d \r\n",
                       pFirstTarget->u2Port);
        }
        else if (pFirstTarget->Address.i4_Length == SNMP_IP6ADDR_LEN)
        {
            INET_NTOP (AF_INET6,
                       pFirstTarget->Address.pu1_OctetList,
                       au1IpAddr, INET6_ADDRSTRLEN);
            CliPrintf (CliHandle, "IP Address          : %s \r\n", au1IpAddr);
            CliPrintf (CliHandle, "Port                : %d \r\n",
                       pFirstTarget->u2Port);
        }
        else
        {
            CliPrintf (CliHandle, "Host Name           : %s \r\n",
                       pFirstTarget->au1Hostname);
            CliPrintf (CliHandle, "Port                : %d \r\n",
                       pFirstTarget->u2Port);
        }

        SNPRINTF ((CHR1 *) ai1Name,
                  (UINT4) (pFirstTarget->TagList.i4_Length + 1),
                  (const CHR1 *) pFirstTarget->TagList.pu1_OctetList);
        CliPrintf (CliHandle, "Tag List            : %s \r\n", ai1Name);

        SNPRINTF ((CHR1 *) ai1Name,
                  (UINT4) (pFirstTarget->AddrParams.i4_Length + 1),
                  (const CHR1 *) pFirstTarget->AddrParams.pu1_OctetList);
        CliPrintf (CliHandle, "Parameters          : %s \r\n", ai1Name);

        CliPrintf (CliHandle,
                   "Storage Type        : %s\r\n",
                   au1Storage[pFirstTarget->i4StorageType]);
        CliPrintf (CliHandle,
                   "Row Status          : %s\r\n",
                   au1RowStatus[pFirstTarget->i4Status - 1]);
        u4PagingStatus = (UINT4) CliPrintf (CliHandle,
                                            "------------------------------ \r\n");

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            break;
        }

        pFirstTarget = SNMPGetNextTgtAddrEntry (&(pFirstTarget->AddrName));
    }
    while (pFirstTarget != NULL);
    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliShowTargetParam                            */
/*                                                                           */
/*     DESCRIPTION      : Function to display the Target param entries       */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains Information to Display       */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliShowTargetParam (tCliHandle CliHandle)
{
    INT1                ai1Name[SNMP_MAX_OCTETSTRING_SIZE - 1];
    UINT4               u4PagingStatus = CLI_SUCCESS;

    tSnmpTgtParamEntry *pFirstTgtParam = NULL;

    if ((pFirstTgtParam = SNMPGetFirstTgtParamEntry ()) == NULL)
    {
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r\n");
    do
    {
        SNPRINTF ((CHR1 *) ai1Name,
                  (UINT4) (pFirstTgtParam->ParamName.i4_Length + 1),
                  (const CHR1 *) pFirstTgtParam->ParamName.pu1_OctetList);
        CliPrintf (CliHandle, "Target Parameter Name    : %s \r\n", ai1Name);

        if (pFirstTgtParam->i2ParamMPModel == SNMP_MPMODEL_V1)
        {
            CliPrintf (CliHandle, "Message Processing Model : v1 \r\n");
        }
        else if (pFirstTgtParam->i2ParamMPModel == SNMP_MPMODEL_V2C)
        {
            CliPrintf (CliHandle, "Message Processing Model : v2c \r\n");
        }
        else if (pFirstTgtParam->i2ParamMPModel == SNMP_MPMODEL_V3)
        {
            CliPrintf (CliHandle, "Message Processing Model : v3 \r\n");
        }
        CliPrintf (CliHandle, "Security Model           : %s \r\n",
                   au1SecModel[pFirstTgtParam->i2ParamSecModel - 1]);

        SNPRINTF ((CHR1 *) ai1Name,
                  (UINT4) (pFirstTgtParam->ParamSecName.i4_Length + 1),
                  (const CHR1 *) pFirstTgtParam->ParamSecName.pu1_OctetList);
        CliPrintf (CliHandle, "Security Name            : %s\r\n", ai1Name);

        CliPrintf (CliHandle, "Security Level           : %s \r\n",
                   au1SecLevel[pFirstTgtParam->i4ParamSecLevel - 1]);
        CliPrintf (CliHandle, "Storage Type             : %s\r\n",
                   au1Storage[pFirstTgtParam->i2ParamStorageType]);
        CliPrintf (CliHandle, "Row Status               : %s\r\n",
                   au1RowStatus[pFirstTgtParam->i2Status - 1]);
        MEMSET (ai1Name, 0, sizeof (ai1Name));

        if (pFirstTgtParam->pFilterProfileTable == NULL)
        {
            SPRINTF ((char *) ai1Name, "%s", "None");
        }
        else
        {
            SNPRINTF ((char *) ai1Name,
                      (UINT4) (pFirstTgtParam->pFilterProfileTable->
                               ProfileName.i4_Length + 1),
                      (const CHR1 *) pFirstTgtParam->
                      pFilterProfileTable->ProfileName.pu1_OctetList);
        }
        CliPrintf (CliHandle, "Filter  Profile Name     : %s \r\n", ai1Name);
        CliPrintf (CliHandle, "Row Status               : %s\r\n",
                   au1RowStatus[pFirstTgtParam->i2Status - 1]);

        u4PagingStatus = (UINT4) CliPrintf (CliHandle,
                                            "------------------------------ \r\n");

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            break;
        }

        pFirstTgtParam
            = SNMPGetNextTgtParamEntry (&(pFirstTgtParam->ParamName));
    }
    while (pFirstTgtParam != NULL);

    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliShowUser                                   */
/*                                                                           */
/*     DESCRIPTION      : This function Displays SNMP Users                  */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains Information to Display       */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliShowUser (tCliHandle CliHandle)
{
    UINT4               u4PagingStatus = CLI_SUCCESS;

    tSnmpUsmEntry      *pUserEntry = NULL;
    INT1                ai1Name[SNMP_MAX_OCTETSTRING_SIZE - 1];

    if ((pUserEntry = SNMPGetFirstUsmEntry ()) == NULL)
    {
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r\n");
    do
    {
        MEMSET (ai1Name, 0, SNMP_MAX_OCTETSTRING_SIZE - 1);
        SnmpConvertOctetToString (&pUserEntry->UsmUserEngineID, ai1Name);

        CliPrintf (CliHandle, "Engine ID               : %s \r\n", ai1Name);

        SNPRINTF ((CHR1 *) ai1Name,
                  (UINT4) (pUserEntry->UsmUserName.i4_Length + 1),
                  (const CHR1 *) pUserEntry->UsmUserName.pu1_OctetList);
        CliPrintf (CliHandle, "User                    : %s \r\n", ai1Name);

        CliPrintf (CliHandle, "Authentication Protocol : %s \r\n",
                   au1AuthProto[pUserEntry->u4UsmUserAuthProtocol - 1]);
        CliPrintf (CliHandle, "Privacy Protocol        : %s\r\n",
                   au1PrivProto[pUserEntry->u4UsmUserPrivProtocol - 1]);
        CliPrintf (CliHandle, "Storage Type            : %s\r\n",
                   au1Storage[pUserEntry->u4UsmUserStorageType]);
        CliPrintf (CliHandle, "Row Status              : %s\r\n",
                   au1RowStatus[pUserEntry->u4UsmUserStatus - 1]);
        u4PagingStatus = (UINT4) CliPrintf (CliHandle,
                                            "------------------------------ \r\n");

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            break;
        }

        pUserEntry = SNMPGetNextUsmEntry (&(pUserEntry->UsmUserEngineID),
                                          &(pUserEntry->UsmUserName));
    }
    while (pUserEntry != NULL);
    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliShowNotification                           */
/*                                                                           */
/*     DESCRIPTION      : This function Displays SNMP Notification entries   */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains Information to Display       */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliShowNotification (tCliHandle CliHandle)
{
    INT1                ai1Name[SNMP_MAX_OCTETSTRING_SIZE - 1];
    UINT4               u4PagingStatus = CLI_SUCCESS;

    tSnmpNotifyEntry   *pFirNotify = NULL;

    if ((pFirNotify = SNMPGetFirstNotifyEntry ()) == NULL)
    {
        return CLI_SUCCESS;
    }
    CliPrintf (CliHandle, "\r\n");

    do
    {
        SNPRINTF ((CHR1 *) ai1Name,
                  (UINT4) (pFirNotify->NotifyName.i4_Length + 1),
                  (const CHR1 *) pFirNotify->NotifyName.pu1_OctetList);
        CliPrintf (CliHandle, "Notify Name  : %s \r\n", ai1Name);

        SNPRINTF ((CHR1 *) ai1Name,
                  (UINT4) (pFirNotify->NotifyTag.i4_Length + 1),
                  (const CHR1 *) pFirNotify->NotifyTag.pu1_OctetList);
        CliPrintf (CliHandle, "Notify Tag   : %s \r\n", ai1Name);

        CliPrintf (CliHandle, "Notify Type  : %s \r\n",
                   au1NotificationType[pFirNotify->i4NotifyType - 1]);
        CliPrintf (CliHandle, "Storage Type : %s\r\n",
                   au1Storage[pFirNotify->i4NotifyStorageType]);
        CliPrintf (CliHandle, "Row Status   : %s\r\n",
                   au1RowStatus[pFirNotify->i4Status - 1]);
        u4PagingStatus = (UINT4) CliPrintf (CliHandle,
                                            "------------------------------ \r\n");

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            break;
        }

        pFirNotify = SNMPGetNextNotifyEntry (&(pFirNotify->NotifyName));
    }
    while (pFirNotify != NULL);
    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliShowNotifyFilter                           */
/*                                                                           */
/*     DESCRIPTION      : This function Displays SNMP Filters                */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains Information to Display       */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliShowNotifyFilter (tCliHandle CliHandle)
{

    tSNMP_OCTET_STRING_TYPE FilterName;
    tSNMP_OCTET_STRING_TYPE FilterMask;
    tSNMP_OID_TYPE      FilterOid;
    UINT1               au1FilterName[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1FilterMask[SNMP_MAX_ADMIN_STR_LENGTH];
    INT1                ai1Name[SNMP_MAX_ADMIN_STR_LENGTH + 1];
    INT1                ai1OID[SNMP_MAX_OCTETSTRING_SIZE];
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4RetStatus = 0;
    UINT4               u4OidCnt = 0;
    INT4                i4FilterType = 0;
    INT4                i4RowStatus = 0;
    INT4                i4StorageType = 0;
    INT1               *pi1OID = NULL;

    pi1OID = ai1OID;

    MEMSET (au1FilterName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    MEMSET (au1FilterMask, 0, SNMP_MAX_ADMIN_STR_LENGTH);

    FilterName.pu1_OctetList = au1FilterName;
    FilterMask.pu1_OctetList = au1FilterMask;
    FilterOid.pu4_OidList = (UINT4 *) MemAllocMemBlk (gSnmpOidListPoolId);
    if (FilterOid.pu4_OidList == NULL)
    {
        return CLI_FAILURE;
    }
    MEMSET (FilterOid.pu4_OidList, 0, sizeof (tSnmpOidListBlock));

    if ((nmhGetFirstIndexSnmpNotifyFilterTable (&FilterName,
                                                &FilterOid)) == SNMP_FAILURE)
    {
        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) FilterOid.pu4_OidList);
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r\n");
    do
    {
        SNPRINTF ((char *) ai1Name, (UINT4) (FilterName.i4_Length + 1),
                  (const CHR1 *) FilterName.pu1_OctetList);
        CliPrintf (CliHandle, "Filter Name  : %s \r\n", ai1Name);

        MEMSET (ai1OID, 0, SNMP_MAX_OCTETSTRING_SIZE);
        if (FilterOid.u4_Length >= SNMP_ONE)
        {
            for (u4OidCnt = 0; u4OidCnt < (FilterOid.u4_Length - 1); u4OidCnt++)
            {
                if ((STRLEN (ai1OID)) < SNMP_MAX_OCTETSTRING_SIZE)
                {
                    SPRINTF ((CHR1 *) pi1OID + STRLEN (ai1OID), "%u.",
                             FilterOid.pu4_OidList[u4OidCnt]);
                }
            }
            if ((STRLEN (ai1OID)) < SNMP_MAX_OCTETSTRING_SIZE)
            {
                SPRINTF ((CHR1 *) pi1OID + STRLEN (ai1OID), "%u",
                         FilterOid.pu4_OidList[u4OidCnt]);
            }
        }

        CliPrintf (CliHandle, "Subtree OID  : %s \r\n", ai1OID);

        /* Get the Table Objects value */
        nmhGetSnmpNotifyFilterMask (&FilterName, &FilterOid, &FilterMask);
        nmhGetSnmpNotifyFilterType (&FilterName, &FilterOid, &i4FilterType);
        nmhGetSnmpNotifyFilterStorageType (&FilterName, &FilterOid,
                                           &i4StorageType);
        nmhGetSnmpNotifyFilterRowStatus (&FilterName, &FilterOid, &i4RowStatus);

        MEMSET (ai1OID, 0, SNMP_MAX_OCTETSTRING_SIZE);
        if (FilterMask.i4_Length >= SNMP_ONE)
        {
            for (u4OidCnt = 0;
                 u4OidCnt < (UINT4) (FilterMask.i4_Length - 1); u4OidCnt++)
            {
                if ((STRLEN (ai1OID)) < SNMP_MAX_OCTETSTRING_SIZE)
                {
                    SPRINTF ((CHR1 *) pi1OID + STRLEN (ai1OID), "%u.",
                             FilterMask.pu1_OctetList[u4OidCnt]);
                }
            }
            if ((STRLEN (ai1OID)) < SNMP_MAX_OCTETSTRING_SIZE)
            {
                SPRINTF ((CHR1 *) pi1OID + STRLEN (ai1OID), "%u",
                         FilterMask.pu1_OctetList[u4OidCnt]);
            }
        }

        CliPrintf (CliHandle, "Subtree Mask : %s \r\n", ai1OID);
        CliPrintf (CliHandle, "Filter Type  : %s \r\n",
                   au1NotifyFilterType[i4FilterType - 1]);
        CliPrintf (CliHandle, "Storage Type : %s\r\n",
                   au1Storage[i4StorageType]);
        CliPrintf (CliHandle, "Row Status   : %s \r\n",
                   au1RowStatus[i4RowStatus - 1]);
        u4PagingStatus = (UINT4) CliPrintf (CliHandle,
                                            "------------------------------ \r\n");

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            break;

        }

        i4RetStatus = nmhGetNextIndexSnmpNotifyFilterTable (&FilterName,
                                                            &FilterName,
                                                            &FilterOid,
                                                            &FilterOid);

    }
    while (i4RetStatus == SNMP_SUCCESS);
    CliPrintf (CliHandle, "\r\n");

    MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *) FilterOid.pu4_OidList);
    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmpv3ShowRunningConfig                            */
/*                                                                           */
/*     DESCRIPTION      : This function displays current configuration       */
/*                        commands of  SNMPv3.                               */
/*                                                                           */
/*     INPUT            : CliHandle  - Contains Information to Display       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
Snmpv3ShowRunningConfig (tCliHandle CliHandle)
{
    static UINT1        au1param1[SNMP_MAX_STR_ENGINEID_LEN + 1];
    static UINT1        au1param2[SNMP_MAX_STR_ENGINEID_LEN + 1];
    tSNMP_OCTET_STRING_TYPE SnmpIndex;
    tSNMP_OCTET_STRING_TYPE NextSnmpIndex;
    tSNMP_OCTET_STRING_TYPE SnmpName;
    tSNMP_OCTET_STRING_TYPE SnmpSecurityName;
    tSNMP_OCTET_STRING_TYPE Snmpparam;
    tSNMP_OCTET_STRING_TYPE Snmpparam1;
    tSNMP_OCTET_STRING_TYPE SnmpContextEngineId;
    tSNMP_OCTET_STRING_TYPE VacmReadName;
    tSNMP_OCTET_STRING_TYPE VacmWriteName;
    tSNMP_OCTET_STRING_TYPE VacmNotifyName;
    tSNMP_OCTET_STRING_TYPE SnmpTargetAddrTagLists;
    tSNMP_OCTET_STRING_TYPE FilterName;
    tSNMP_OID_TYPE      VacmFamilySubtree;
    tSNMP_OID_TYPE      NextVacmFamilySubtree;
    tSNMP_OID_TYPE      UsmAuthProtocol;
    tSNMP_OID_TYPE      UsmPrivProtocol;
    tPrpProxyTableEntry *pPropProxyEntry = NULL;
    tProxyTableEntry   *pProxyEntry = NULL;
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;

    INT1               *pi1OID = NULL;
    INT1               *pi1SpacePtr = NULL;
    INT1                ai1Name[SNMP_MAX_STR_ENGINEID_LEN + 1];
    INT1                ai1Name1[SNMP_MAX_STR_ENGINEID_LEN + 1];
    INT1                ai1Name2[SNMP_MAX_STR_ENGINEID_LEN + 1];

    INT4                i4RetNotifyType = 0;
    INT4                i4RetStorageType = 0;
    INT4                i4Timeout = 0;
    INT4                i4RetryCount = 0;
    INT4                i4SecurityModel = 0;
    INT4                i4NextSecurityModel = 0;
    INT4                i4SecurityLevel = 0;
    INT4                i4NextSecurityLevel = 0;
    INT4                i4RetVal = 0;
    INT4                i4RowStatus = 0;
    INT4                i4DefFlag = FALSE;
    INT4                i4Index = 0;
    INT4                i4FilterStorageType = 0;
    UINT4               au4Value[SNMP_PASSWORD_SIZE];
    UINT4               au4Value1[SNMP_PASSWORD_SIZE];
    UINT4               u4Count = 0;
    UINT4               u4Port = 0;
    UINT4               u4MibIdCnt = 0;
    /* Snmp Over TCP */
    INT4                i4SnmpTcpStatus = 0;
    INT4                i4SnmpTrapTcpStatus = 0;
    INT4                i4ResType = 0;
    UINT4               u4SnmpListenTcpPort = 0;
    UINT4               u4SnmpListenTcpTrapPort = 0;
    INT4                i4AuthTrapStatus = 0;
    INT4                i4ColdTrapControl = 0;
    UINT4               u4ListenTrapPort = SNMP_MANAGER_PORT;
    UINT4               u4ListenAgentPort = SNMP_PORT;
    UINT2               u2Port = 0;

    UINT1              *pu1Snmpparam = NULL;
    UINT1              *pu1Snmpparam1 = NULL;
    UINT1               au1param3[SNMP_MAX_ADMIN_STR_LENGTH + 1];
    UINT1               au1param4[SNMP_MAX_ADMIN_STR_LENGTH + 1];
    UINT1               au1param5[SNMP_MAX_STR_ENGINEID_LEN + 1];
    UINT1               au1IpAddr[60];
    UINT1               u1NoneFlag = SNMP_TRUE;
    UINT1              *pu1Oid = NULL;
    UINT1               au1Name[SNMP_MAX_OID_LENGTH];
    tSnmpTrapFilterEntry *pSnmpTrapFilterEntry = NULL;
    tSnmpUsmEntry      *pUsmEntry = NULL;

    UNUSED_PARAM (i4ResType);
    UNUSED_PARAM (pUsmEntry);
    /* SNMP_TRUE = mask is "none" (default field)
     * SNMP_FALSE = mask is not "none" */

    MEMSET (au1param1, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);
    MEMSET (au1param2, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);
    MEMSET (au1param3, 0, SNMP_MAX_ADMIN_STR_LENGTH + 1);
    MEMSET (au1param4, 0, SNMP_MAX_ADMIN_STR_LENGTH + 1);
    MEMSET (au1param5, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);

    pu1Snmpparam = MemAllocMemBlk (gSnmpTagListPoolId);
    if (pu1Snmpparam == NULL)
    {
        return CLI_FAILURE;
    }
    MEMSET (pu1Snmpparam, 0, MAX_STR_OCTET_VAL + 1);

    pu1Snmpparam1 = MemAllocMemBlk (gSnmpTagListPoolId);
    if (pu1Snmpparam1 == NULL)
    {
        MemReleaseMemBlock (gSnmpTagListPoolId, pu1Snmpparam);
        return CLI_FAILURE;
    }
    MEMSET (pu1Snmpparam1, 0, MAX_STR_OCTET_VAL + 1);

    SnmpIndex.pu1_OctetList = au1param1;
    SnmpIndex.i4_Length = 0;
    NextSnmpIndex.pu1_OctetList = au1param2;
    NextSnmpIndex.i4_Length = 0;
    SnmpSecurityName.pu1_OctetList = au1param3;
    SnmpSecurityName.i4_Length = 0;
    SnmpContextEngineId.pu1_OctetList = au1param5;
    SnmpContextEngineId.i4_Length = 0;
    Snmpparam.pu1_OctetList = pu1Snmpparam;
    Snmpparam.i4_Length = 0;
    Snmpparam1.pu1_OctetList = pu1Snmpparam1;
    Snmpparam1.i4_Length = 0;
    VacmWriteName.pu1_OctetList = au1param3;
    VacmWriteName.i4_Length = 0;
    VacmFamilySubtree.pu4_OidList = au4Value;
    VacmFamilySubtree.u4_Length = 0;
    NextVacmFamilySubtree.pu4_OidList = au4Value1;
    NextVacmFamilySubtree.u4_Length = 0;
    UsmAuthProtocol.pu4_OidList = au4Value;
    UsmAuthProtocol.u4_Length = 0;
    UsmPrivProtocol.pu4_OidList = au4Value1;
    UsmPrivProtocol.u4_Length = 0;
    FilterName.pu1_OctetList = au1param4;
    FilterName.i4_Length = 0;

    nmhGetSnmpAgentControl (&i4RetVal);
    if (i4RetVal != SNMP_AGENT_ENABLE)
    {
        CliPrintf (CliHandle, "\r\ndisable snmpagent\r\n");
    }
    CliPrintf (CliHandle, "! \r\n");
    /*Snmp Over TCP */
    nmhGetSnmpOverTcpStatus (&i4SnmpTcpStatus);
    if (i4SnmpTcpStatus != SNMP3_OVER_TCP_DISABLE)
    {
        CliPrintf (CliHandle, "\r\nsnmp tcp enable\r\n");
    }
    nmhGetSnmpTrapOverTcpStatus (&i4SnmpTrapTcpStatus);
    if (i4SnmpTrapTcpStatus != SNMP3_TRAP_OVER_TCP_DISABLE)
    {
        CliPrintf (CliHandle, "\r\nsnmp trap tcp enable\r\n");
    }
    nmhGetSnmpListenTcpPort (&u4SnmpListenTcpPort);
    if (u4SnmpListenTcpPort != SNMP_PORT)
    {
        CliPrintf (CliHandle, "\r\nsnmp-server tcp-port %d\r\n",
                   u4SnmpListenTcpPort);
    }
    nmhGetSnmpListenTcpTrapPort (&u4SnmpListenTcpTrapPort);
    if (u4SnmpListenTcpTrapPort != SNMP_MANAGER_PORT)
    {
        CliPrintf (CliHandle, "\r\nsnmp-server trap tcp-port %d\r\n",
                   u4SnmpListenTcpTrapPort);
    }
    nmhGetSnmpEnableAuthenTraps (&i4AuthTrapStatus);
    if (i4AuthTrapStatus != AUTHTRAP_DISABLE)
    {
        CliPrintf (CliHandle,
                   "\r\nsnmp-server enable traps snmp authentication\r\n");
    }
    nmhGetSnmpColdStartTrapControl (&i4ColdTrapControl);
    if (i4ColdTrapControl != ISS_ENABLE)
    {
        CliPrintf (CliHandle, "\r\nno snmp-server enable traps coldstart\r\n");
    }
    nmhGetSnmpProxyListenTrapPort (&u4Port);
    if (u4Port != SNMP_MANAGER_PORT)
    {
        CliPrintf (CliHandle, "\r\nsnmp-server trap proxy-udp-port %d\r\n ",
                   u4Port);
    }

    /* Get Restoration Type - whether CSR or CSR */
    i4ResType = IssGetRestoreTypeFromNvRam ();

    pProxyEntry = SNMPGetFirstProxy ();

    if (pProxyEntry != NULL)
    {
        CliPrintf (CliHandle, "\r\n");
    }
    while (pProxyEntry != NULL)
    {
        SNPRINTF ((CHR1 *) ai1Name,
                  ((UINT4) (pProxyEntry->ProxyName.i4_Length + 1)),
                  (const CHR1 *) pProxyEntry->ProxyName.pu1_OctetList);

        pi1SpacePtr = (INT1 *) STRCHR ((INT1 *) ai1Name, ' ');

        if (pi1SpacePtr != NULL)
        {
            CliPrintf (CliHandle, "snmp proxy name \"%s\" ", ai1Name);
        }
        else
        {
            CliPrintf (CliHandle, "snmp proxy name %s ", ai1Name);
        }
        CliPrintf (CliHandle, "proxytype %s ",
                   au1ProxyType[pProxyEntry->ProxyType - 1]);
        MEMSET (ai1Name, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);
        SnmpConvertOctetToString (&pProxyEntry->PrxContextEngineID, ai1Name);

        pi1SpacePtr = (INT1 *) STRCHR ((INT1 *) ai1Name, ' ');
        if (pi1SpacePtr != NULL)
        {
            CliPrintf (CliHandle, "contextengineid \"%s\" ", ai1Name);
        }
        else
        {
            CliPrintf (CliHandle, "contextengineid %s ", ai1Name);
        }
        SNPRINTF ((CHR1 *) ai1Name,
                  (UINT4) ((pProxyEntry->PrxTargetParamsIn.i4_Length + 1)),
                  (const CHR1 *) pProxyEntry->PrxTargetParamsIn.pu1_OctetList);

        pi1SpacePtr = (INT1 *) STRCHR ((INT1 *) ai1Name, ' ');

        if (pi1SpacePtr != NULL)
        {
            CliPrintf (CliHandle, "targetparamsin \"%s\" ", ai1Name);
        }
        else
        {
            CliPrintf (CliHandle, "targetparamsin %s ", ai1Name);
        }

        if (((pProxyEntry->ProxyType == PROXY_TYPE_READ) ||
             (pProxyEntry->ProxyType == PROXY_TYPE_WRITE)))
        {
            SNPRINTF ((CHR1 *) ai1Name,
                      (UINT4) ((pProxyEntry->PrxSingleTargetOut.i4_Length + 1)),
                      (const CHR1 *) pProxyEntry->
                      PrxSingleTargetOut.pu1_OctetList);
            pi1SpacePtr = (INT1 *) STRCHR ((INT1 *) ai1Name, ' ');

            if (pi1SpacePtr != NULL)
            {
                CliPrintf (CliHandle, "targetout \"%s\" ", ai1Name);
            }
            else
            {
                CliPrintf (CliHandle, "targetout %s ", ai1Name);
            }
        }
        else if ((pProxyEntry->ProxyType == PROXY_TYPE_TRAP) ||
                 (pProxyEntry->ProxyType == PROXY_TYPE_INFORM))
        {
            SNPRINTF ((CHR1 *) ai1Name,
                      (UINT4) ((pProxyEntry->PrxMultipleTargetOut.i4_Length +
                                1)),
                      (const CHR1 *) pProxyEntry->
                      PrxMultipleTargetOut.pu1_OctetList);

            pi1SpacePtr = (INT1 *) STRCHR ((INT1 *) ai1Name, ' ');

            if (pi1SpacePtr != NULL)
            {
                CliPrintf (CliHandle, "targetout \"%s\" ", ai1Name);
            }
            else
            {
                CliPrintf (CliHandle, "targetout %s ", ai1Name);
            }

        }

        if (STRLEN (pProxyEntry->PrxContextName.pu1_OctetList) != 0)
        {
            SNPRINTF ((CHR1 *) ai1Name,
                      ((UINT4) (pProxyEntry->PrxContextName.i4_Length + 1)),
                      (const CHR1 *) pProxyEntry->PrxContextName.pu1_OctetList);

            pi1SpacePtr = (INT1 *) STRCHR ((INT1 *) ai1Name, ' ');

            if (pi1SpacePtr != NULL)
            {
                CliPrintf (CliHandle, "contextname \"%s\" ", ai1Name);
            }
            else
            {
                CliPrintf (CliHandle, "contextname %s ", ai1Name);
            }
        }
        if (pProxyEntry->i4Storage != SNMP3_STORAGE_TYPE_NONVOLATILE)
        {
            CliPrintf (CliHandle, "storagetype %s\r\n",
                       au1Storage[pProxyEntry->i4Storage]);
        }

        CliPrintf (CliHandle, "\r\n");
        pProxyEntry = SNMPGetNextProxyEntry (&(pProxyEntry->ProxyName));
    }

    pPropProxyEntry = SNMPGetFirstPrpProxy ();

    if (pPropProxyEntry != NULL)
    {
        CliPrintf (CliHandle, "\r\n");
    }

    while (pPropProxyEntry != NULL)
    {
        SNPRINTF ((CHR1 *) ai1Name,
                  (UINT4) ((pPropProxyEntry->PrpPrxMibName.i4_Length + 1)),
                  (const CHR1 *) pPropProxyEntry->PrpPrxMibName.pu1_OctetList);
        pi1SpacePtr = (INT1 *) STRCHR ((INT1 *) ai1Name, ' ');
        if (pi1SpacePtr != NULL)
        {
            CliPrintf (CliHandle, "snmp mibproxy name \"%s\" ", ai1Name);
        }
        else
        {
            CliPrintf (CliHandle, "snmp mibproxy name %s ", ai1Name);
        }

        MEMSET (ai1Name, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);
        if (pPropProxyEntry->PrpPrxMibID.u4_Length >= SNMP_ONE)
        {
            for (u4MibIdCnt = 0;
                 u4MibIdCnt < (pPropProxyEntry->PrpPrxMibID.u4_Length - 1);
                 u4MibIdCnt++)
            {
                SPRINTF ((CHR1 *) ai1Name + STRLEN (ai1Name), "%u.",
                         pPropProxyEntry->PrpPrxMibID.pu4_OidList[u4MibIdCnt]);
            }
            SPRINTF ((CHR1 *) ai1Name + STRLEN (ai1Name), "%u",
                     pPropProxyEntry->PrpPrxMibID.pu4_OidList[u4MibIdCnt]);
        }

        CliPrintf (CliHandle, "proxytype %s ",
                   au1ProxyType[pPropProxyEntry->i4PrpPrxMibType - 1]);
        CliPrintf (CliHandle, "mibid %s ", ai1Name);
        MEMSET (ai1Name, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);

        SNPRINTF ((CHR1 *) ai1Name,
                  (UINT4) ((pPropProxyEntry->PrpPrxTargetParamsIn.i4_Length +
                            1)),
                  (const CHR1 *) pPropProxyEntry->
                  PrpPrxTargetParamsIn.pu1_OctetList);
        pi1SpacePtr = (INT1 *) STRCHR ((INT1 *) ai1Name, ' ');
        if (pi1SpacePtr != NULL)
        {
            CliPrintf (CliHandle, "targetparamsin \"%s\" ", ai1Name);
        }
        else
        {
            CliPrintf (CliHandle, "targetparamsin %s ", ai1Name);
        }

        if ((pPropProxyEntry->i4PrpPrxMibType == PROXY_TYPE_READ) ||
            (pPropProxyEntry->i4PrpPrxMibType == PROXY_TYPE_WRITE))
        {
            SNPRINTF ((CHR1 *) ai1Name,
                      (UINT4) ((pPropProxyEntry->
                                PrpPrxSingleTargetOut.i4_Length + 1)),
                      (const CHR1 *) pPropProxyEntry->
                      PrpPrxSingleTargetOut.pu1_OctetList);

            pi1SpacePtr = (INT1 *) STRCHR ((INT1 *) ai1Name, ' ');
            if (pi1SpacePtr != NULL)
            {
                CliPrintf (CliHandle, "targetout \"%s\" ", ai1Name);
            }
            else
            {
                CliPrintf (CliHandle, "targetout %s ", ai1Name);
            }

        }
        else if ((pPropProxyEntry->i4PrpPrxMibType == PROXY_TYPE_TRAP) ||
                 (pPropProxyEntry->i4PrpPrxMibType == PROXY_TYPE_INFORM))
        {
            SNPRINTF ((CHR1 *) ai1Name,
                      (UINT4) ((pPropProxyEntry->
                                PrpPrxMultipleTargetOut.i4_Length + 1)),
                      (const CHR1 *) pPropProxyEntry->
                      PrpPrxMultipleTargetOut.pu1_OctetList);

            pi1SpacePtr = (INT1 *) STRCHR ((INT1 *) ai1Name, ' ');
            if (pi1SpacePtr != NULL)
            {
                CliPrintf (CliHandle, "targetout \"%s\" ", ai1Name);
            }
            else
            {
                CliPrintf (CliHandle, "targetout %s ", ai1Name);
            }

        }

        if (pPropProxyEntry->i4Storage != SNMP3_STORAGE_TYPE_NONVOLATILE)
        {
            CliPrintf (CliHandle, "storagetype %s\r\n",
                       au1Storage[pPropProxyEntry->i4Storage]);
        }

        CliPrintf (CliHandle, "\r\n");
        pPropProxyEntry =
            SNMPGetNextPrpProxyEntry (&(pPropProxyEntry->PrpPrxMibName));
    }
    /* Displays Filter Trap table */
    pSnmpTrapFilterEntry =
        (tSnmpTrapFilterEntry *) RBTreeGetFirst (gSnmpTrapFilterTable);
    pu1Oid = MemAllocMemBlk (gSnmpMultiOidPoolId);
    while (pSnmpTrapFilterEntry != NULL)
    {
        MEMSET (au1Name, 0, SNMP_MAX_OID_LENGTH);
        CliPrintf (CliHandle, "snmp filter trap ");
        SNMPGetOidString (pSnmpTrapFilterEntry->TrapFilterOID.pu4_OidList,
                          pSnmpTrapFilterEntry->TrapFilterOID.u4_Length,
                          pu1Oid);
        SnmpONGetNameFromOid ((char *) pu1Oid, (char *) au1Name);
        if (STRLEN (au1Name) == 0)
        {

            CliPrintf (CliHandle, "oid %s\r\n", pu1Oid);

        }
        else
        {
            CliPrintf (CliHandle, "name %s\r\n", au1Name);
        }
        pSnmpTrapFilterEntry = (tSnmpTrapFilterEntry *) RBTreeGetNext
            (gSnmpTrapFilterTable,
             pSnmpTrapFilterEntry, SnmpTrapFilterTblCmpFn);
    }
    MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1Oid);
    /*Display Agentx related configurations */
    SnxCliShowRunningConfig (CliHandle);

    nmhGetFsSnmpListenAgentPort (&u4ListenAgentPort);
    if (u4ListenAgentPort != SNMP_PORT)
    {
        CliPrintf (CliHandle, "\r\nsnmp agent port %d\r\n", u4ListenAgentPort);
    }
    nmhGetSnmpListenTrapPort (&u4ListenTrapPort);
    if (u4ListenTrapPort != SNMP_MANAGER_PORT)
    {
        CliPrintf (CliHandle, "\r\nsnmp-server trap udp-port %d\r\n",
                   (UINT2) u4ListenTrapPort);
    }

    MEMSET (ai1Name, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);
    MEMSET (ai1Name1, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);

    if (SNMP_EQUAL != SNMPCompareOctetString (&gSnmpEngineID,
                                              &gSnmpDefaultEngineID))
    {
        if (SnmpConvertOctetToString (&gSnmpEngineID, ai1Name) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Invalid Ip Address Name\r\n");
        }

        pi1SpacePtr = (INT1 *) STRCHR ((INT1 *) ai1Name, ' ');
        if (pi1SpacePtr != NULL)
        {
            CliPrintf (CliHandle, "snmp engineId \"%s\"\r\n", ai1Name);
        }
        else
        {
            CliPrintf (CliHandle, "snmp engineId %s\r\n", ai1Name);
        }
    }

    MEMSET (SnmpIndex.pu1_OctetList, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);
    MEMSET (Snmpparam.pu1_OctetList, 0, MAX_STR_OCTET_VAL + 1);

    if (nmhGetFirstIndexUsmUserTable (&SnmpIndex, &Snmpparam) == SNMP_SUCCESS)
    {
        SNMPCopyOctetString (&NextSnmpIndex, &SnmpIndex);
        SNMPCopyOctetString (&Snmpparam1, &Snmpparam);

        do
        {
            MEMSET (SnmpIndex.pu1_OctetList, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);
            MEMSET (Snmpparam.pu1_OctetList, 0, MAX_STR_OCTET_VAL + 1);
            MEMSET (UsmAuthProtocol.pu4_OidList, 0, SNMP_PASSWORD_SIZE);
            MEMSET (UsmPrivProtocol.pu4_OidList, 0, SNMP_PASSWORD_SIZE);

            SNMPCopyOctetString (&SnmpIndex, &NextSnmpIndex);
            SNMPCopyOctetString (&Snmpparam, &Snmpparam1);
            nmhGetUsmUserStatus (&SnmpIndex, &Snmpparam, &i4RowStatus);

            if (i4RowStatus == SNMP_ROWSTATUS_ACTIVE)
            {
                nmhGetUsmUserAuthProtocol (&SnmpIndex, &Snmpparam,
                                           &UsmAuthProtocol);
                nmhGetUsmUserPrivProtocol (&SnmpIndex, &Snmpparam,
                                           &UsmPrivProtocol);
                nmhGetUsmUserStorageType (&SnmpIndex, &Snmpparam,
                                          &i4RetStorageType);

                pUsmEntry = SNMPGetUsmEntry (&SnmpIndex, &Snmpparam);

                if (SnmpCheckDefConfig
                    (&SnmpIndex, &Snmpparam, &UsmAuthProtocol, &UsmPrivProtocol,
                     i4RetStorageType) == SNMP_FAILURE)
                {
                    pi1SpacePtr =
                        (INT1 *) STRCHR ((INT1 *) Snmpparam.pu1_OctetList, ' ');
                    if (pi1SpacePtr != NULL)
                    {
                        CliPrintf (CliHandle, "snmp user \"%s\"",
                                   Snmpparam.pu1_OctetList);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "snmp user %s",
                                   Snmpparam.pu1_OctetList);
                    }
                    pi1OID = MemAllocMemBlk (gSnmpOctetListPoolId);
                    if (pi1OID == NULL)
                    {
                        MemReleaseMemBlock (gSnmpTagListPoolId, pu1Snmpparam);
                        MemReleaseMemBlock (gSnmpTagListPoolId, pu1Snmpparam1);
                        return CLI_FAILURE;
                    }
                    MEMSET (pi1OID, 0, sizeof (tSnmpOctetListBlock));

                    if (UsmAuthProtocol.u4_Length > 0)
                    {
                        for (u4Count = 0;
                             u4Count < (UsmAuthProtocol.u4_Length - 1);
                             u4Count++)
                        {
                            SPRINTF ((CHR1 *) pi1OID + STRLEN (pi1OID), "%u.",
                                     UsmAuthProtocol.pu4_OidList[u4Count]);
                        }
                        SPRINTF ((CHR1 *) pi1OID + STRLEN (pi1OID), "%u",
                                 UsmAuthProtocol.pu4_OidList[u4Count]);
                    }
                    if (STRCMP (pi1OID, SNMP_AUTH_MD5) == 0)
                        CliPrintf (CliHandle, " auth md5 AUTHPASSWD");
                    else if (STRCMP (pi1OID, SNMP_AUTH_SHA) == 0)
                        CliPrintf (CliHandle, " auth sha AUTHPASSWD");
                    else if (STRCMP (pi1OID, SNMP_AUTH_SHA256) == 0)
                        CliPrintf (CliHandle, " auth sha256 AUTHPASSWD");
                    else if (STRCMP (pi1OID, SNMP_AUTH_SHA384) == 0)
                        CliPrintf (CliHandle, " auth sha384 AUTHPASSWD");
                    else if (STRCMP (pi1OID, SNMP_AUTH_SHA512) == 0)
                        CliPrintf (CliHandle, " auth sha512 AUTHPASSWD");
                    MEMSET (pi1OID, 0, sizeof (tSnmpOctetListBlock));
                    if (UsmPrivProtocol.u4_Length > 0)
                    {
                        for (u4Count = 0;
                             u4Count < (UsmPrivProtocol.u4_Length - 1);
                             u4Count++)
                        {
                            SPRINTF ((CHR1 *) pi1OID + STRLEN (pi1OID), "%u.",
                                     UsmPrivProtocol.pu4_OidList[u4Count]);
                        }
                        SPRINTF ((CHR1 *) pi1OID + STRLEN (pi1OID), "%u",
                                 UsmPrivProtocol.pu4_OidList[u4Count]);
                    }

                    if (STRCMP (pi1OID, SNMP_PRIV_DES) == 0)
                    {
                        CliPrintf (CliHandle, " priv DES PRIVPASSWD");
                    }
                    else if (STRCMP (pi1OID, SNMP_PRIV_AESCFB128) == 0)
                    {
                        CliPrintf (CliHandle, " priv AES_CFB128 PRIVPASSWD");
                    }
                    else if (STRCMP (pi1OID, SNMP_PRIV_AESCTR) == 0)
                    {
                        CliPrintf (CliHandle, " priv AESCTR PRIVPASSWD");
                    }
                    else if (STRCMP (pi1OID, SNMP_PRIV_AESCTR192) == 0)
                    {
                        CliPrintf (CliHandle, " priv AESCTR192 PRIVPASSWD");
                    }
                    else if (STRCMP (pi1OID, SNMP_PRIV_AESCTR256) == 0)
                    {
                        CliPrintf (CliHandle, " priv AESCTR256 PRIVPASSWD");
                    }
                    else if (STRCMP (pi1OID, SNMP_PRIV_AESCFB192) == 0)
                    {
                        CliPrintf (CliHandle, " priv AES_CFB192 PRIVPASSWD");
                    }
                    else if (STRCMP (pi1OID, SNMP_PRIV_AESCFB256) == 0)
                    {
                        CliPrintf (CliHandle, " priv AES_CFB256 PRIVPASSWD");
                    }
                    else if (STRCMP (pi1OID, SNMP_PRIV_TDES) == 0)
                    {
                        CliPrintf (CliHandle, " priv TDES PRIVPASSWD");
                    }

                    if (i4RetStorageType == 2)
                        CliPrintf (CliHandle, " volatile");
                    MEMSET (ai1Name2, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);
                    if (SNMP_EQUAL != SNMPCompareOctetString (&SnmpIndex,
                                                              &gSnmpEngineID))
                    {
                        SnmpConvertOctetToString (&SnmpIndex, ai1Name2);
                        pi1SpacePtr = (INT1 *) STRCHR ((INT1 *) ai1Name2, ' ');
                        if (pi1SpacePtr != NULL)
                        {
                            CliPrintf (CliHandle, " EngineId \"%s\"", ai1Name2);
                        }
                        else
                        {
                            CliPrintf (CliHandle, " EngineId %s", ai1Name2);
                        }

                    }

                    MEMSET (NextSnmpIndex.pu1_OctetList, 0,
                            SNMP_MAX_STR_ENGINEID_LEN + 1);
                    MEMSET (Snmpparam1.pu1_OctetList, 0, MAX_STR_OCTET_VAL + 1);
                    CliPrintf (CliHandle, "\r\n");
                    MemReleaseMemBlock (gSnmpOctetListPoolId, (UINT1 *) pi1OID);
                }
            }
        }
        while (nmhGetNextIndexUsmUserTable (&SnmpIndex,
                                            &NextSnmpIndex,
                                            &Snmpparam,
                                            &Snmpparam1) == SNMP_SUCCESS);
    }

    MEMSET (SnmpIndex.pu1_OctetList, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);

    if (nmhGetFirstIndexSnmpCommunityTable (&SnmpIndex) == SNMP_SUCCESS)
    {
        SNMPCopyOctetString (&NextSnmpIndex, &SnmpIndex);

        do
        {
            i4DefFlag = FALSE;
            MEMSET (SnmpIndex.pu1_OctetList, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);
            MEMSET (SnmpSecurityName.pu1_OctetList, 0,
                    SNMP_MAX_ADMIN_STR_LENGTH + 1);
            MEMSET (SnmpContextEngineId.pu1_OctetList, 0,
                    SNMP_MAX_STR_ENGINEID_LEN + 1);
            MEMSET (Snmpparam.pu1_OctetList, 0, MAX_STR_OCTET_VAL + 1);
            MEMSET (Snmpparam1.pu1_OctetList, 0, MAX_STR_OCTET_VAL + 1);
            MEMSET (ai1Name1, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);

            SNMPCopyOctetString (&SnmpIndex, &NextSnmpIndex);

            nmhGetSnmpCommunityStatus (&SnmpIndex, &i4RowStatus);

            SnmpName.pu1_OctetList = MemAllocMemBlk (gSnmpOctetListPoolId);
            if (SnmpName.pu1_OctetList == NULL)
            {
                MemReleaseMemBlock (gSnmpTagListPoolId, pu1Snmpparam);
                MemReleaseMemBlock (gSnmpTagListPoolId, pu1Snmpparam1);
                return CLI_FAILURE;
            }
            MEMSET (SnmpName.pu1_OctetList, 0, sizeof (tSnmpOctetListBlock));
            SnmpName.i4_Length = 0;

            if (i4RowStatus == SNMP_ROWSTATUS_ACTIVE)
            {
                nmhGetSnmpCommunityName (&SnmpIndex, &SnmpName);
                nmhGetSnmpCommunitySecurityName (&SnmpIndex, &SnmpSecurityName);
                nmhGetSnmpCommunityContextName (&SnmpIndex, &Snmpparam);
                nmhGetSnmpCommunityStorageType (&SnmpIndex, &i4RetStorageType);
                nmhGetSnmpCommunityTransportTag (&SnmpIndex, &Snmpparam1);
                nmhGetSnmpCommunityContextEngineID (&SnmpIndex,
                                                    &SnmpContextEngineId);
                for (i4Index = 0;
                     gaSnmpCommunityDefault[i4Index].pu1VarName != NULL;
                     i4Index++)
                {
                    if ((0 == STRCMP (SnmpIndex.pu1_OctetList,
                                      gaSnmpCommunityDefault
                                      [i4Index].pu1VarName))
                        && (0 ==
                            STRCMP (SnmpName.pu1_OctetList,
                                    gaSnmpCommunityDefault
                                    [i4Index].pu1VarName))
                        && (0 ==
                            STRCMP (SnmpSecurityName.pu1_OctetList, "none")))

                    {
                        i4DefFlag = TRUE;
                        break;
                    }
                }
                if (i4DefFlag == TRUE)
                {
                    MemReleaseMemBlock (gSnmpOctetListPoolId,
                                        SnmpName.pu1_OctetList);
                    continue;
                }
                SnmpIndex.pu1_OctetList[SnmpIndex.i4_Length] = '\0';
                pi1SpacePtr =
                    (INT1 *) STRCHR ((INT1 *) SnmpIndex.pu1_OctetList, ' ');
                if (pi1SpacePtr != NULL)
                {
                    CliPrintf (CliHandle, "snmp community index \"%s\"",
                               SnmpIndex.pu1_OctetList);
                }
                else
                {
                    CliPrintf (CliHandle, "snmp community index %s",
                               SnmpIndex.pu1_OctetList);
                }

                if (SnmpName.i4_Length > 0)
                {
                    SnmpName.pu1_OctetList[SnmpName.i4_Length] = '\0';

                    pi1SpacePtr =
                        (INT1 *) STRCHR ((INT1 *) SnmpName.pu1_OctetList, ' ');
                    if (pi1SpacePtr != NULL)
                    {
                        CliPrintf (CliHandle, " name \"%s\"",
                                   SnmpName.pu1_OctetList);
                    }
                    else
                    {
                        CliPrintf (CliHandle, " name %s",
                                   SnmpName.pu1_OctetList);
                    }
                }
                if (SnmpSecurityName.i4_Length > 0)
                {
                    SnmpSecurityName.pu1_OctetList[SnmpSecurityName.i4_Length] =
                        '\0';
                    pi1SpacePtr =
                        (INT1 *) STRCHR ((INT1 *) SnmpSecurityName.
                                         pu1_OctetList, ' ');
                    if (pi1SpacePtr != NULL)
                    {
                        CliPrintf (CliHandle, " security \"%s\"",
                                   SnmpSecurityName.pu1_OctetList);
                    }
                    else
                    {
                        CliPrintf (CliHandle, " security %s",
                                   SnmpSecurityName.pu1_OctetList);
                    }
                }
                if (Snmpparam.i4_Length > 0)
                {
                    Snmpparam.pu1_OctetList[Snmpparam.i4_Length] = '\0';
                    if (Snmpparam.i4_Length < MAX_STR_OCTET_VAL + 1)    /*kloc */
                    {
                        if (STRCMP (Snmpparam.pu1_OctetList, "") != 0)
                        {
                            pi1SpacePtr =
                                (INT1 *) STRCHR ((INT1 *) Snmpparam.
                                                 pu1_OctetList, ' ');
                            if (pi1SpacePtr != NULL)
                            {
                                CliPrintf (CliHandle, " context \"%s\"",
                                           Snmpparam.pu1_OctetList);
                            }
                            else
                            {
                                CliPrintf (CliHandle, " context %s",
                                           Snmpparam.pu1_OctetList);
                            }
                        }
                    }
                }

                if (i4RetStorageType == 2)
                    CliPrintf (CliHandle, " volatile");

                if (Snmpparam1.i4_Length > 0)
                {
                    Snmpparam1.pu1_OctetList[Snmpparam1.i4_Length] = '\0';
                    CliPrintf (CliHandle, " transporttag %s",
                               Snmpparam1.pu1_OctetList);
                }

                if (SnmpContextEngineId.i4_Length > 0)
                {
                    if (SNMP_EQUAL !=
                        SNMPCompareOctetString (&SnmpContextEngineId,
                                                &gSnmpDefaultEngineID))
                    {
                        if (SNMP_EQUAL !=
                            SNMPCompareOctetString (&SnmpContextEngineId,
                                                    &gSnmpEngineID))
                        {
                            SnmpConvertOctetToString (&SnmpContextEngineId,
                                                      ai1Name1);
                            pi1SpacePtr =
                                (INT1 *) STRCHR ((INT1 *) ai1Name1, ' ');
                            if (pi1SpacePtr != NULL)
                            {
                                CliPrintf (CliHandle, " contextengineid \"%s\"",
                                           ai1Name1);
                            }
                            else
                            {
                                CliPrintf (CliHandle, " contextengineid %s",
                                           ai1Name1);
                            }
                        }
                    }
                }

                MEMSET (NextSnmpIndex.pu1_OctetList, 0,
                        SNMP_MAX_STR_ENGINEID_LEN + 1);
                CliPrintf (CliHandle, "\r\n");

            }
            MemReleaseMemBlock (gSnmpOctetListPoolId, SnmpName.pu1_OctetList);
        }
        while (nmhGetNextIndexSnmpCommunityTable (&SnmpIndex,
                                                  &NextSnmpIndex)
               == SNMP_SUCCESS);

    }

    MEMSET (SnmpIndex.pu1_OctetList, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);

    if (nmhGetFirstIndexVacmSecurityToGroupTable (&i4SecurityModel,
                                                  &SnmpIndex) == SNMP_SUCCESS)
    {
        i4NextSecurityModel = i4SecurityModel;
        SNMPCopyOctetString (&NextSnmpIndex, &SnmpIndex);

        do
        {
            i4DefFlag = FALSE;
            MEMSET (SnmpIndex.pu1_OctetList, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);

            i4SecurityModel = i4NextSecurityModel;
            SNMPCopyOctetString (&SnmpIndex, &NextSnmpIndex);

            nmhGetVacmSecurityToGroupStatus (i4SecurityModel,
                                             &SnmpIndex, &i4RowStatus);
            SnmpName.pu1_OctetList = MemAllocMemBlk (gSnmpOctetListPoolId);
            if (SnmpName.pu1_OctetList == NULL)
            {
                MemReleaseMemBlock (gSnmpTagListPoolId, pu1Snmpparam);
                MemReleaseMemBlock (gSnmpTagListPoolId, pu1Snmpparam1);
                return CLI_FAILURE;
            }
            MEMSET (SnmpName.pu1_OctetList, 0, sizeof (tSnmpOctetListBlock));
            SnmpName.i4_Length = 0;

            if (i4RowStatus == SNMP_ROWSTATUS_ACTIVE)
            {

                nmhGetVacmGroupName (i4SecurityModel, &SnmpIndex, &SnmpName);
                nmhGetVacmSecurityToGroupStorageType (i4SecurityModel,
                                                      &SnmpIndex,
                                                      &i4RetStorageType);

                for (i4Index = 0;
                     gaSnmpGrpDefault[i4Index].pu1VarName != NULL; i4Index++)
                {
                    if (0 == STRCMP (SnmpName.pu1_OctetList,
                                     gaSnmpGrpDefault[i4Index].pu1VarName))
                    {
                        i4DefFlag = TRUE;
                        break;
                    }
                }

                if (i4DefFlag == TRUE)
                {
                    MemReleaseMemBlock (gSnmpOctetListPoolId,
                                        SnmpName.pu1_OctetList);
                    continue;
                }

                CliPrintf (CliHandle, "snmp group");

                if (SnmpName.i4_Length > 0)
                {
                    pi1SpacePtr =
                        (INT1 *) STRCHR ((INT1 *) SnmpName.pu1_OctetList, ' ');
                    if (pi1SpacePtr != NULL)
                    {
                        CliPrintf (CliHandle, " \"%s\"",
                                   SnmpName.pu1_OctetList);
                    }
                    else
                    {
                        CliPrintf (CliHandle, " %s", SnmpName.pu1_OctetList);
                    }
                }
                pi1SpacePtr =
                    (INT1 *) STRCHR ((INT1 *) SnmpIndex.pu1_OctetList, ' ');
                if (pi1SpacePtr != NULL)
                {
                    CliPrintf (CliHandle, " user \"%s\"",
                               SnmpIndex.pu1_OctetList);
                }
                else
                {
                    CliPrintf (CliHandle, " user %s", SnmpIndex.pu1_OctetList);
                }
                CliPrintf (CliHandle, " security-model %s",
                           au1SecModel[i4SecurityModel - 1]);

                if (i4RetStorageType == 2)
                    CliPrintf (CliHandle, " volatile");

                MEMSET (NextSnmpIndex.pu1_OctetList, 0,
                        SNMP_MAX_STR_ENGINEID_LEN + 1);
                CliPrintf (CliHandle, "\r\n");

            }
            MemReleaseMemBlock (gSnmpOctetListPoolId, SnmpName.pu1_OctetList);
        }
        while (nmhGetNextIndexVacmSecurityToGroupTable (i4SecurityModel,
                                                        &i4NextSecurityModel,
                                                        &SnmpIndex,
                                                        &NextSnmpIndex)
               == SNMP_SUCCESS);
    }

    MEMSET (SnmpIndex.pu1_OctetList, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);
    MEMSET (Snmpparam.pu1_OctetList, 0, MAX_STR_OCTET_VAL + 1);

    if (nmhGetFirstIndexVacmAccessTable (&SnmpIndex, &Snmpparam,
                                         &i4SecurityModel, &i4SecurityLevel)
        == SNMP_SUCCESS)
    {
        SNMPCopyOctetString (&NextSnmpIndex, &SnmpIndex);
        SNMPCopyOctetString (&Snmpparam1, &Snmpparam);
        i4NextSecurityModel = i4SecurityModel;
        i4NextSecurityLevel = i4SecurityLevel;

        do
        {
            i4DefFlag = FALSE;
            MEMSET (SnmpIndex.pu1_OctetList, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);
            MEMSET (Snmpparam.pu1_OctetList, 0, MAX_STR_OCTET_VAL + 1);
            MEMSET (VacmWriteName.pu1_OctetList, 0,
                    SNMP_MAX_ADMIN_STR_LENGTH + 1);
            VacmNotifyName.pu1_OctetList = MemAllocMemBlk (gSnmpTagListPoolId);
            if (VacmNotifyName.pu1_OctetList == NULL)
            {
                MemReleaseMemBlock (gSnmpTagListPoolId, pu1Snmpparam);
                MemReleaseMemBlock (gSnmpTagListPoolId, pu1Snmpparam1);
                return CLI_FAILURE;
            }
            VacmNotifyName.i4_Length = 0;
            MEMSET (VacmNotifyName.pu1_OctetList, 0, MAX_STR_OCTET_VAL);

            VacmReadName.pu1_OctetList = MemAllocMemBlk (gSnmpOctetListPoolId);
            if (VacmReadName.pu1_OctetList == NULL)
            {
                MemReleaseMemBlock (gSnmpTagListPoolId, pu1Snmpparam);
                MemReleaseMemBlock (gSnmpTagListPoolId, pu1Snmpparam1);
                MemReleaseMemBlock (gSnmpTagListPoolId,
                                    VacmNotifyName.pu1_OctetList);
                return CLI_FAILURE;
            }
            VacmReadName.i4_Length = 0;
            MEMSET (VacmReadName.pu1_OctetList, 0,
                    sizeof (tSnmpOctetListBlock));

            SNMPCopyOctetString (&SnmpIndex, &NextSnmpIndex);
            SNMPCopyOctetString (&Snmpparam, &Snmpparam1);
            i4SecurityModel = i4NextSecurityModel;
            i4SecurityLevel = i4NextSecurityLevel;

            nmhGetVacmAccessStatus (&SnmpIndex, &Snmpparam, i4SecurityModel,
                                    i4SecurityLevel, &i4RowStatus);

            if (i4RowStatus == SNMP_ROWSTATUS_ACTIVE)
            {
                for (i4Index = 0; gaSnmpGrpDefault[i4Index].pu1VarName != NULL;
                     i4Index++)
                {
                    if (0 == STRCMP (SnmpIndex.pu1_OctetList,
                                     gaSnmpGrpDefault[i4Index].pu1VarName))
                    {
                        i4DefFlag = TRUE;
                        break;
                    }
                }
                if (i4DefFlag == TRUE)
                {
                    MemReleaseMemBlock (gSnmpTagListPoolId,
                                        VacmNotifyName.pu1_OctetList);
                    MemReleaseMemBlock (gSnmpOctetListPoolId,
                                        VacmReadName.pu1_OctetList);
                    continue;
                }
                nmhGetVacmAccessReadViewName (&SnmpIndex,
                                              &Snmpparam,
                                              i4SecurityModel,
                                              i4SecurityLevel, &VacmReadName);
                nmhGetVacmAccessWriteViewName (&SnmpIndex,
                                               &Snmpparam,
                                               i4SecurityModel,
                                               i4SecurityLevel, &VacmWriteName);
                nmhGetVacmAccessNotifyViewName (&SnmpIndex,
                                                &Snmpparam,
                                                i4SecurityModel,
                                                i4SecurityLevel,
                                                &VacmNotifyName);
                nmhGetVacmAccessStorageType (&SnmpIndex,
                                             &Snmpparam,
                                             i4SecurityModel,
                                             i4SecurityLevel,
                                             &i4RetStorageType);

                pi1SpacePtr =
                    (INT1 *) STRCHR ((INT1 *) SnmpIndex.pu1_OctetList, ' ');
                if (pi1SpacePtr != NULL)
                {
                    CliPrintf (CliHandle, "snmp access \"%s\"",
                               SnmpIndex.pu1_OctetList);
                }
                else
                {
                    CliPrintf (CliHandle, "snmp access %s",
                               SnmpIndex.pu1_OctetList);
                }
                CliPrintf (CliHandle, " %s", au1SecModel[i4SecurityModel - 1]);

                if (i4SecurityModel == 3)
                {
                    if (i4SecurityLevel == 1)
                        CliPrintf (CliHandle, " noauth");
                    else if (i4SecurityLevel == 2)
                        CliPrintf (CliHandle, " auth");
                    else if (i4SecurityLevel == 3)
                        CliPrintf (CliHandle, " priv");
                }

                if (VacmReadName.i4_Length > 0)
                {
                    pi1SpacePtr =
                        (INT1 *) STRCHR ((INT1 *) VacmReadName.pu1_OctetList,
                                         ' ');
                    if (pi1SpacePtr != NULL)
                    {
                        CliPrintf (CliHandle, " read \"%s\"",
                                   VacmReadName.pu1_OctetList);
                    }
                    else
                    {
                        CliPrintf (CliHandle, " read %s",
                                   VacmReadName.pu1_OctetList);
                    }
                }
                if (VacmWriteName.i4_Length > 0)
                {
                    pi1SpacePtr =
                        (INT1 *) STRCHR ((INT1 *) VacmWriteName.pu1_OctetList,
                                         ' ');
                    if (pi1SpacePtr != NULL)
                    {
                        CliPrintf (CliHandle, " write \"%s\"",
                                   VacmWriteName.pu1_OctetList);
                    }
                    else
                    {
                        CliPrintf (CliHandle, " write %s",
                                   VacmWriteName.pu1_OctetList);
                    }
                }
                if (VacmNotifyName.i4_Length > 0)
                {
                    pi1SpacePtr =
                        (INT1 *) STRCHR ((INT1 *) VacmNotifyName.pu1_OctetList,
                                         ' ');
                    if (pi1SpacePtr != NULL)
                    {
                        CliPrintf (CliHandle, " notify \"%s\"",
                                   VacmNotifyName.pu1_OctetList);
                    }
                    else
                    {
                        CliPrintf (CliHandle, " notify %s",
                                   VacmNotifyName.pu1_OctetList);
                    }
                }
                if (i4RetStorageType == 2)
                    CliPrintf (CliHandle, " volatile");

                if ((STRCMP (Snmpparam.pu1_OctetList, "default") != 0) &&
                    (Snmpparam.i4_Length > 0))
                {
                    pi1SpacePtr =
                        (INT1 *) STRCHR ((INT1 *) Snmpparam.pu1_OctetList, ' ');
                    if (pi1SpacePtr != NULL)
                    {
                        CliPrintf (CliHandle, " context \"%s\"",
                                   Snmpparam.pu1_OctetList);
                    }
                    else
                    {
                        CliPrintf (CliHandle, " context %s",
                                   Snmpparam.pu1_OctetList);
                    }
                }

                MEMSET (NextSnmpIndex.pu1_OctetList, 0,
                        SNMP_MAX_STR_ENGINEID_LEN + 1);
                MEMSET (Snmpparam1.pu1_OctetList, 0, MAX_STR_OCTET_VAL + 1);
                CliPrintf (CliHandle, "\r\n");

            }
            MemReleaseMemBlock (gSnmpTagListPoolId,
                                VacmNotifyName.pu1_OctetList);
            MemReleaseMemBlock (gSnmpOctetListPoolId,
                                VacmReadName.pu1_OctetList);

        }
        while (nmhGetNextIndexVacmAccessTable (&SnmpIndex,
                                               &NextSnmpIndex,
                                               &Snmpparam,
                                               &Snmpparam1,
                                               i4SecurityModel,
                                               &i4NextSecurityModel,
                                               i4SecurityLevel,
                                               &i4NextSecurityLevel)
               == SNMP_SUCCESS);

    }

    MEMSET (SnmpIndex.pu1_OctetList, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);
    MEMSET (VacmFamilySubtree.pu4_OidList, 0,
            (sizeof (UINT4) * SNMP_PASSWORD_SIZE));

    if (nmhGetFirstIndexVacmViewTreeFamilyTable (&SnmpIndex,
                                                 &VacmFamilySubtree)
        == SNMP_SUCCESS)
    {
        SNMPCopyOctetString (&NextSnmpIndex, &SnmpIndex);
        SNMPOIDCopy (&NextVacmFamilySubtree, &VacmFamilySubtree);

        do
        {
            i4DefFlag = FALSE;
            MEMSET (SnmpIndex.pu1_OctetList, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);
            MEMSET (Snmpparam.pu1_OctetList, 0, MAX_STR_OCTET_VAL + 1);

            SNMPCopyOctetString (&SnmpIndex, &NextSnmpIndex);
            SNMPOIDCopy (&VacmFamilySubtree, &NextVacmFamilySubtree);

            nmhGetVacmViewTreeFamilyStatus (&SnmpIndex,
                                            &VacmFamilySubtree, &i4RowStatus);

            if (i4RowStatus == SNMP_ROWSTATUS_ACTIVE)
            {

                nmhGetVacmViewTreeFamilyMask (&SnmpIndex,
                                              &VacmFamilySubtree, &Snmpparam);
                nmhGetVacmViewTreeFamilyType (&SnmpIndex,
                                              &VacmFamilySubtree, &i4RetVal);
                nmhGetVacmViewTreeFamilyStorageType (&SnmpIndex,
                                                     &VacmFamilySubtree,
                                                     &i4RetStorageType);

                for (i4Index = 0;
                     gaSnmpViewDefault[i4Index].pu1VarName != NULL; i4Index++)
                {
                    if (0 == STRCMP (SnmpIndex.pu1_OctetList,
                                     gaSnmpViewDefault[i4Index].pu1VarName))
                    {
                        if ((Snmpparam.i4_Length == 1) &&
                            (VacmFamilySubtree.pu4_OidList[0] == 1))
                        {
                            i4DefFlag = TRUE;
                            break;
                        }
                    }
                }
                if (i4DefFlag == TRUE)
                {
                    continue;
                }
                pi1SpacePtr =
                    (INT1 *) STRCHR ((INT1 *) SnmpIndex.pu1_OctetList, ' ');
                if (pi1SpacePtr != NULL)
                {
                    CliPrintf (CliHandle, "snmp view \"%s\"",
                               SnmpIndex.pu1_OctetList);
                }
                else
                {
                    CliPrintf (CliHandle, "snmp view %s",
                               SnmpIndex.pu1_OctetList);
                }
                pi1OID = MemAllocMemBlk (gSnmpOctetListPoolId);
                if (pi1OID == NULL)
                {
                    MemReleaseMemBlock (gSnmpTagListPoolId, pu1Snmpparam);
                    MemReleaseMemBlock (gSnmpTagListPoolId, pu1Snmpparam1);
                    return CLI_FAILURE;
                }
                MEMSET (pi1OID, 0, sizeof (tSnmpOctetListBlock));

                if (VacmFamilySubtree.u4_Length > 0)
                {
                    for (u4Count = 0;
                         u4Count < (VacmFamilySubtree.u4_Length - 1); u4Count++)
                    {
                        SPRINTF ((CHR1 *) pi1OID + STRLEN (pi1OID), "%u.",
                                 VacmFamilySubtree.pu4_OidList[u4Count]);
                    }
                    SPRINTF ((CHR1 *) pi1OID + STRLEN (pi1OID), "%u",
                             VacmFamilySubtree.pu4_OidList[u4Count]);

                    CliPrintf (CliHandle, " %s", pi1OID);
                }

                MEMSET (pi1OID, 0, sizeof (tSnmpOctetListBlock));
                if (Snmpparam.i4_Length > 0)
                {
                    for (u4Count = 0;
                         u4Count < (UINT4) (Snmpparam.i4_Length - 1); u4Count++)
                    {
                        SPRINTF ((CHR1 *) pi1OID + STRLEN (pi1OID), "%u.",
                                 Snmpparam.pu1_OctetList[u4Count]);

                        /* Default mask i.e. "none" is stored in the
                         * mib, as all the fields filled with 1 */
                        if (Snmpparam.pu1_OctetList[u4Count] != 1)
                        {
                            u1NoneFlag = SNMP_FALSE;
                        }
                    }
                    SPRINTF ((CHR1 *) pi1OID + STRLEN (pi1OID), "%u",
                             Snmpparam.pu1_OctetList[u4Count]);

                    /* check the final element here */
                    if (Snmpparam.pu1_OctetList[u4Count] != 1)
                    {
                        u1NoneFlag = SNMP_FALSE;
                    }

                    /* Print the mask only if it doesn't have default value */
                    if (u1NoneFlag == SNMP_FALSE)
                    {
                        CliPrintf (CliHandle, " mask %s", pi1OID);
                    }
                    u1NoneFlag = SNMP_TRUE;    /* Resetting the flag to 
                                               its default value */
                }

                if (i4RetVal == 1)
                    CliPrintf (CliHandle, " included");
                else if (i4RetVal == 2)
                    CliPrintf (CliHandle, " excluded");

                if (i4RetStorageType == 2)
                    CliPrintf (CliHandle, " volatile");

                MEMSET (NextSnmpIndex.pu1_OctetList, 0,
                        SNMP_MAX_STR_ENGINEID_LEN + 1);
                MEMSET (NextVacmFamilySubtree.pu4_OidList, 0,
                        SNMP_PASSWORD_SIZE);
                CliPrintf (CliHandle, "\r\n");

                MemReleaseMemBlock (gSnmpOctetListPoolId, (UINT1 *) pi1OID);
            }
        }
        while (nmhGetNextIndexVacmViewTreeFamilyTable (&SnmpIndex,
                                                       &NextSnmpIndex,
                                                       &VacmFamilySubtree,
                                                       &NextVacmFamilySubtree)
               == SNMP_SUCCESS);

    }

    MEMSET (SnmpIndex.pu1_OctetList, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);
    SnmpIndex.i4_Length = 0;
    if (nmhGetFirstIndexSnmpTargetAddrTable (&SnmpIndex) == SNMP_SUCCESS)
    {
        SNMPCopyOctetString (&NextSnmpIndex, &SnmpIndex);

        do
        {
            MEMSET (SnmpIndex.pu1_OctetList, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);
            MEMSET (Snmpparam.pu1_OctetList, 0, MAX_STR_OCTET_VAL + 1);
            MEMSET (Snmpparam1.pu1_OctetList, 0, MAX_STR_OCTET_VAL + 1);

            SnmpTargetAddrTagLists.pu1_OctetList =
                MemAllocMemBlk (gSnmpTagListPoolId);
            if (SnmpTargetAddrTagLists.pu1_OctetList == NULL)
            {
                MemReleaseMemBlock (gSnmpTagListPoolId, pu1Snmpparam);
                MemReleaseMemBlock (gSnmpTagListPoolId, pu1Snmpparam1);
                return CLI_FAILURE;
            }
            SnmpTargetAddrTagLists.i4_Length = 0;
            MEMSET (SnmpTargetAddrTagLists.pu1_OctetList, 0, MAX_STR_OCTET_VAL);

            SNMPCopyOctetString (&SnmpIndex, &NextSnmpIndex);

            nmhGetSnmpTargetAddrRowStatus (&SnmpIndex, &i4RowStatus);

            if (i4RowStatus == SNMP_ROWSTATUS_ACTIVE)
            {

                nmhGetSnmpTargetAddrParams (&SnmpIndex, &Snmpparam);

                pSnmpTgtAddrEntry = SNMPGetTgtAddrEntry (&SnmpIndex);

                if (pSnmpTgtAddrEntry == NULL)
                {
                    return SNMP_FAILURE;
                }

                if (pSnmpTgtAddrEntry->b1HostName == SNMP_TRUE)
                {
                    nmhGetFsSnmpTargetHostName (&SnmpIndex, &Snmpparam1);
                }
                else
                {
                    nmhGetSnmpTargetAddrTAddress (&SnmpIndex, &Snmpparam1);
                }
                nmhGetSnmpTargetAddrTimeout (&SnmpIndex, &i4Timeout);
                /* converting time interval to seconds */
                i4Timeout = i4Timeout / 100;
                nmhGetSnmpTargetAddrRetryCount (&SnmpIndex, &i4RetryCount);
                nmhGetSnmpTargetAddrTagList (&SnmpIndex,
                                             &SnmpTargetAddrTagLists);
                nmhGetSnmpTargetAddrStorageType (&SnmpIndex, &i4RetStorageType);

                pi1SpacePtr =
                    (INT1 *) STRCHR ((INT1 *) SnmpIndex.pu1_OctetList, ' ');
                if (pi1SpacePtr != NULL)
                {
                    CliPrintf (CliHandle, "snmp targetaddr \"%s\"",
                               SnmpIndex.pu1_OctetList);
                }
                else
                {
                    CliPrintf (CliHandle, "snmp targetaddr %s",
                               SnmpIndex.pu1_OctetList);
                }

                if (Snmpparam.i4_Length > 0)
                {
                    pi1SpacePtr =
                        (INT1 *) STRCHR ((INT1 *) Snmpparam.pu1_OctetList, ' ');
                    if (pi1SpacePtr != NULL)
                    {
                        CliPrintf (CliHandle, " param \"%s\"",
                                   Snmpparam.pu1_OctetList);
                    }
                    else
                    {
                        CliPrintf (CliHandle, " param %s",
                                   Snmpparam.pu1_OctetList);
                    }
                }
                if (pSnmpTgtAddrEntry->b1HostName == SNMP_FALSE)
                {

                    if (Snmpparam1.i4_Length == SNMP_IPADDR_LEN + SNMP_TWO)
                    {
                        INET_NTOP (AF_INET,
                                   Snmpparam1.pu1_OctetList,
                                   au1IpAddr, INET_ADDRSTRLEN);

                        CliPrintf (CliHandle, " %s", au1IpAddr);
                    }
                    else if (Snmpparam1.i4_Length == SNMP_IP6ADDR_LEN)
                    {
                        INET_NTOP (AF_INET6,
                                   Snmpparam1.pu1_OctetList,
                                   au1IpAddr, INET6_ADDRSTRLEN);

                        CliPrintf (CliHandle, " %s", au1IpAddr);
                    }
                }
                else if (pSnmpTgtAddrEntry->b1HostName == SNMP_TRUE)
                {
                    CliPrintf (CliHandle, " %s", Snmpparam1.pu1_OctetList);
                }

                if (i4Timeout != SNMP_INFM_DEF_TIMEOUT_IN_SECONDS)
                {
                    CliPrintf (CliHandle, " timeout %d", i4Timeout);
                }
                if (i4RetryCount != SNMP_INFM_DEF_RETRY_VAL)
                {
                    CliPrintf (CliHandle, " retries %d", i4RetryCount);
                }
                if (SnmpTargetAddrTagLists.i4_Length > 0)
                {
                    pi1SpacePtr =
                        (INT1 *) STRCHR ((INT1 *) SnmpTargetAddrTagLists.
                                         pu1_OctetList, ' ');
                    if (pi1SpacePtr != NULL)
                    {
                        CliPrintf (CliHandle, " taglist \"%s\"",
                                   SnmpTargetAddrTagLists.pu1_OctetList);
                    }
                    else
                    {
                        CliPrintf (CliHandle, " taglist %s",
                                   SnmpTargetAddrTagLists.pu1_OctetList);
                    }
                }
                if (i4RetStorageType == 2)
                    CliPrintf (CliHandle, " volatile");
                if (Snmpparam1.i4_Length == SNMP_IPADDR_LEN + SNMP_TWO)
                {
                    u2Port = *(UINT2 *) (VOID *) (Snmpparam1.pu1_OctetList
                                                  + SNMP_IPADDR_LEN);
                    u2Port = OSIX_HTONS (u2Port);
                    if (u2Port != SNMP_MANAGER_PORT)
                    {
                        CliPrintf (CliHandle, " port %d", u2Port);
                    }
                }
                if ((pSnmpTgtAddrEntry->u2Port != SNMP_MANAGER_PORT)
                    && (Snmpparam1.i4_Length != SNMP_IPADDR_LEN + SNMP_TWO))
                {
                    CliPrintf (CliHandle, " port %d",
                               pSnmpTgtAddrEntry->u2Port);
                }

                MEMSET (NextSnmpIndex.pu1_OctetList, 0,
                        SNMP_MAX_STR_ENGINEID_LEN + 1);
                CliPrintf (CliHandle, "\r\n");

            }
            MemReleaseMemBlock (gSnmpTagListPoolId,
                                SnmpTargetAddrTagLists.pu1_OctetList);
        }
        while (nmhGetNextIndexSnmpTargetAddrTable (&SnmpIndex,
                                                   &NextSnmpIndex)
               == SNMP_SUCCESS);
    }

    MEMSET (SnmpIndex.pu1_OctetList, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);

    if (nmhGetFirstIndexSnmpTargetParamsTable (&SnmpIndex) == SNMP_SUCCESS)
    {
        SNMPCopyOctetString (&NextSnmpIndex, &SnmpIndex);

        do
        {
            i4DefFlag = FALSE;
            MEMSET (SnmpIndex.pu1_OctetList, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);
            MEMSET (SnmpSecurityName.pu1_OctetList, 0,
                    SNMP_MAX_ADMIN_STR_LENGTH + 1);

            SNMPCopyOctetString (&SnmpIndex, &NextSnmpIndex);

            nmhGetSnmpTargetParamsRowStatus (&SnmpIndex, &i4RowStatus);

            if (i4RowStatus == SNMP_ROWSTATUS_ACTIVE)
            {
                nmhGetSnmpTargetParamsMPModel (&SnmpIndex, &i4RetVal);
                nmhGetSnmpTargetParamsSecurityModel (&SnmpIndex,
                                                     &i4SecurityModel);
                nmhGetSnmpTargetParamsSecurityName (&SnmpIndex,
                                                    &SnmpSecurityName);
                nmhGetSnmpTargetParamsSecurityLevel (&SnmpIndex,
                                                     &i4SecurityLevel);
                nmhGetSnmpTargetParamsStorageType (&SnmpIndex,
                                                   &i4RetStorageType);

                for (i4Index = 0;
                     gaSnmpTgtParamDefault[i4Index].pu1VarName != NULL;
                     i4Index++)
                {
                    if (0 == STRCMP (SnmpIndex.pu1_OctetList,
                                     gaSnmpTgtParamDefault[i4Index].pu1VarName))
                    {
                        i4DefFlag = TRUE;
                        break;
                    }
                }

                if (i4DefFlag == TRUE)
                {
                    continue;
                }

                pi1SpacePtr =
                    (INT1 *) STRCHR ((INT1 *) SnmpIndex.pu1_OctetList, ' ');
                if (pi1SpacePtr != NULL)
                {
                    CliPrintf (CliHandle, "snmp targetparams \"%s\"",
                               SnmpIndex.pu1_OctetList);
                }
                else
                {
                    CliPrintf (CliHandle, "snmp targetparams %s",
                               SnmpIndex.pu1_OctetList);
                }
                if (SnmpSecurityName.i4_Length > 0)
                {
                    pi1SpacePtr =
                        (INT1 *) STRCHR ((INT1 *) SnmpSecurityName.
                                         pu1_OctetList, ' ');
                    if (pi1SpacePtr != NULL)
                    {
                        CliPrintf (CliHandle, " user \"%s\"",
                                   SnmpSecurityName.pu1_OctetList);
                    }
                    else
                    {
                        CliPrintf (CliHandle, " user %s",
                                   SnmpSecurityName.pu1_OctetList);
                    }
                }

                if (i4SecurityModel > 0)
                {
                    CliPrintf (CliHandle, " security-model %s",
                               au1SecModel[i4SecurityModel - 1]);
                }

                if (i4SecurityModel == 3)
                {
                    if (i4SecurityLevel == 1)
                        CliPrintf (CliHandle, " noauth");
                    else if (i4SecurityLevel == 2)
                        CliPrintf (CliHandle, " auth");
                    else if (i4SecurityLevel == 3)
                        CliPrintf (CliHandle, " priv");
                }

                if (i4RetVal == SNMP_MPMODEL_V2C)
                    CliPrintf (CliHandle, " message-processing v2c");
                else if (i4RetVal == SNMP_MPMODEL_V3)
                    CliPrintf (CliHandle, " message-processing v3");
                else
                    CliPrintf (CliHandle, " message-processing v1");

                if (i4RetStorageType == 2)
                    CliPrintf (CliHandle, " volatile");
                if (nmhGetSnmpNotifyFilterProfileName (&SnmpIndex, &FilterName)
                    != SNMP_FAILURE)
                {

                    if (FilterName.i4_Length > 0)
                    {
                        pi1SpacePtr =
                            (INT1 *) STRCHR ((INT1 *) FilterName.pu1_OctetList,
                                             ' ');
                        if (pi1SpacePtr != NULL)
                        {
                            CliPrintf (CliHandle, " filterprofile-name \"%s\"",
                                       FilterName.pu1_OctetList);
                        }
                        else
                        {
                            CliPrintf (CliHandle, " filterprofile-name %s",
                                       FilterName.pu1_OctetList);
                        }
                    }
                }
                nmhGetSnmpNotifyFilterProfileStorType (&SnmpIndex,
                                                       &i4FilterStorageType);
                if (i4FilterStorageType == 2)
                {
                    CliPrintf (CliHandle, " filter-storagetype volatile");
                }
                MEMSET (NextSnmpIndex.pu1_OctetList, 0,
                        SNMP_MAX_STR_ENGINEID_LEN + 1);
                CliPrintf (CliHandle, "\r\n");

            }

        }
        while (nmhGetNextIndexSnmpTargetParamsTable (&SnmpIndex,
                                                     &NextSnmpIndex)
               == SNMP_SUCCESS);
    }
/* Start:Filter Table*/
    MEMSET (SnmpIndex.pu1_OctetList, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);
    MEMSET (VacmFamilySubtree.pu4_OidList, 0,
            (sizeof (UINT4) * SNMP_PASSWORD_SIZE));

    if (nmhGetFirstIndexSnmpNotifyFilterTable (&SnmpIndex,
                                               &VacmFamilySubtree)
        == SNMP_SUCCESS)
    {
        SNMPCopyOctetString (&NextSnmpIndex, &SnmpIndex);
        SNMPOIDCopy (&NextVacmFamilySubtree, &VacmFamilySubtree);

        do
        {
            i4DefFlag = FALSE;
            MEMSET (SnmpIndex.pu1_OctetList, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);
            MEMSET (Snmpparam.pu1_OctetList, 0, MAX_STR_OCTET_VAL + 1);

            SNMPCopyOctetString (&SnmpIndex, &NextSnmpIndex);
            SNMPOIDCopy (&VacmFamilySubtree, &NextVacmFamilySubtree);

            nmhGetSnmpNotifyFilterMask (&SnmpIndex,
                                        &VacmFamilySubtree, &Snmpparam);
            nmhGetSnmpNotifyFilterType (&SnmpIndex,
                                        &VacmFamilySubtree, &i4RetVal);
            nmhGetSnmpNotifyFilterStorageType (&SnmpIndex,
                                               &VacmFamilySubtree,
                                               &i4RetStorageType);

            pi1SpacePtr =
                (INT1 *) STRCHR ((INT1 *) SnmpIndex.pu1_OctetList, ' ');
            if (pi1SpacePtr != NULL)
            {
                CliPrintf (CliHandle, "snmp filterprofile \"%s\"",
                           SnmpIndex.pu1_OctetList);
            }
            else
            {
                CliPrintf (CliHandle, "snmp filterprofile %s",
                           SnmpIndex.pu1_OctetList);
            }
            pi1OID = MemAllocMemBlk (gSnmpOctetListPoolId);
            if (pi1OID == NULL)
            {
                CliPrintf (CliHandle, "!\r\n");
                MemReleaseMemBlock (gSnmpTagListPoolId, pu1Snmpparam);
                MemReleaseMemBlock (gSnmpTagListPoolId, pu1Snmpparam1);
                return CLI_FAILURE;
            }
            MEMSET (pi1OID, 0, sizeof (tSnmpOctetListBlock));

            if (VacmFamilySubtree.u4_Length > 0)
            {
                for (u4Count = 0;
                     u4Count < (VacmFamilySubtree.u4_Length - 1); u4Count++)
                {
                    SPRINTF ((char *) pi1OID + STRLEN (pi1OID), "%u.",
                             VacmFamilySubtree.pu4_OidList[u4Count]);
                }
                SPRINTF ((char *) pi1OID + STRLEN (pi1OID), "%u",
                         VacmFamilySubtree.pu4_OidList[u4Count]);

                CliPrintf (CliHandle, " %s", pi1OID);
            }

            MEMSET (pi1OID, 0, sizeof (tSnmpOctetListBlock));
            if (Snmpparam.i4_Length > 0)
            {
                for (u4Count = 0;
                     u4Count < (UINT4) (Snmpparam.i4_Length - 1); u4Count++)
                {
                    SPRINTF ((char *) pi1OID + STRLEN (pi1OID), "%u.",
                             Snmpparam.pu1_OctetList[u4Count]);

                    /* Default mask i.e. "none" is stored in the
                     * mib, as all the fields filled with 1 */
                    if (Snmpparam.pu1_OctetList[u4Count] != 1)
                    {
                        u1NoneFlag = SNMP_FALSE;
                    }
                }
                SPRINTF ((char *) pi1OID + STRLEN (pi1OID), "%u",
                         Snmpparam.pu1_OctetList[u4Count]);

                /* check the final element here */
                if (Snmpparam.pu1_OctetList[u4Count] != 1)
                {
                    u1NoneFlag = SNMP_FALSE;
                }

                /* Print the mask only if it doesn't have default value */
                if (u1NoneFlag == SNMP_FALSE)
                {
                    CliPrintf (CliHandle, " mask %s", pi1OID);
                }
                u1NoneFlag = SNMP_TRUE;    /* Resetting the flag to 
                                           its default value */

                if (i4RetVal == 1)
                    CliPrintf (CliHandle, " included");
                else if (i4RetVal == 2)
                    CliPrintf (CliHandle, " excluded");

                if (i4RetStorageType == 2)
                    CliPrintf (CliHandle, " volatile");

                MEMSET (NextSnmpIndex.pu1_OctetList, 0,
                        SNMP_MAX_STR_ENGINEID_LEN + 1);
                MEMSET (NextVacmFamilySubtree.pu4_OidList, 0,
                        SNMP_PASSWORD_SIZE);
                CliPrintf (CliHandle, "\r\n");

            }
            MemReleaseMemBlock (gSnmpOctetListPoolId, (UINT1 *) pi1OID);
        }
        while (nmhGetNextIndexSnmpNotifyFilterTable (&SnmpIndex,
                                                     &NextSnmpIndex,
                                                     &VacmFamilySubtree,
                                                     &NextVacmFamilySubtree)
               == SNMP_SUCCESS);
    }
    /* End: Filter Table */
    MEMSET (SnmpIndex.pu1_OctetList, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);
    MEMSET (Snmpparam.pu1_OctetList, 0, MAX_STR_OCTET_VAL + 1);

    MEMSET (SnmpIndex.pu1_OctetList, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);
    if (nmhGetFirstIndexSnmpNotifyTable (&SnmpIndex) == SNMP_SUCCESS)
    {
        SNMPCopyOctetString (&NextSnmpIndex, &SnmpIndex);

        do
        {
            i4DefFlag = FALSE;
            MEMSET (SnmpIndex.pu1_OctetList, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);
            MEMSET (Snmpparam.pu1_OctetList, 0, MAX_STR_OCTET_VAL + 1);

            SNMPCopyOctetString (&SnmpIndex, &NextSnmpIndex);

            nmhGetSnmpNotifyRowStatus (&SnmpIndex, &i4RowStatus);

            if (i4RowStatus == SNMP_ROWSTATUS_ACTIVE)
            {
                nmhGetSnmpNotifyTag (&SnmpIndex, &Snmpparam);
                nmhGetSnmpNotifyType (&SnmpIndex, &i4RetNotifyType);
                nmhGetSnmpNotifyStorageType (&SnmpIndex, &i4RetStorageType);

                for (i4Index = 0;
                     gaSnmpNotifyDefault[i4Index].pu1VarName != NULL; i4Index++)
                {
                    if (0 == STRCMP (SnmpIndex.pu1_OctetList,
                                     gaSnmpNotifyDefault[i4Index].pu1VarName))
                    {
                        i4DefFlag = TRUE;
                        break;
                    }
                }

                if (i4DefFlag == TRUE)
                {
                    continue;
                }

                pi1SpacePtr =
                    (INT1 *) STRCHR ((INT1 *) SnmpIndex.pu1_OctetList, ' ');
                if (pi1SpacePtr != NULL)
                {
                    CliPrintf (CliHandle, "snmp notify \"%s\"",
                               SnmpIndex.pu1_OctetList);
                }
                else
                {
                    CliPrintf (CliHandle, "snmp notify %s",
                               SnmpIndex.pu1_OctetList);
                }
                if (Snmpparam.i4_Length > 0)
                {
                    pi1SpacePtr =
                        (INT1 *) STRCHR ((INT1 *) Snmpparam.pu1_OctetList, ' ');
                    if (pi1SpacePtr != NULL)
                    {
                        CliPrintf (CliHandle, " tag \"%s\"",
                                   Snmpparam.pu1_OctetList);
                    }
                    else
                    {
                        CliPrintf (CliHandle, " tag %s",
                                   Snmpparam.pu1_OctetList);
                    }
                }
                if (SNMP3_NOTIFY_TYPE_TRAP == i4RetNotifyType)
                {
                    CliPrintf (CliHandle, " type Trap");
                }
                else if (SNMP3_NOTIFY_TYPE_INFORM == i4RetNotifyType)
                {
                    CliPrintf (CliHandle, " type Inform");
                }

                if (i4RetStorageType == 2)
                    CliPrintf (CliHandle, " volatile");

                MEMSET (NextSnmpIndex.pu1_OctetList, 0,
                        SNMP_MAX_STR_ENGINEID_LEN + 1);
                CliPrintf (CliHandle, "\r\n");

            }

        }
        while (nmhGetNextIndexSnmpNotifyTable (&SnmpIndex,
                                               &NextSnmpIndex) == SNMP_SUCCESS);
    }

    MemReleaseMemBlock (gSnmpTagListPoolId, pu1Snmpparam);
    MemReleaseMemBlock (gSnmpTagListPoolId, pu1Snmpparam1);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliSetAuthTrapStatus                          */
/*                                                                           */
/*     DESCRIPTION      : Function to Enable/Disable generation of           */
/*                        authentication traps.                              */
/*                                                                           */
/*     INPUT            : u4AuthTrapStatus - Action to perform enable/disable*/
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT1
Snmp3CliSetAuthTrapStatus (tCliHandle CliHandle, UINT4 u4AuthTrapStatus)
{
    UNUSED_PARAM (CliHandle);

    if (nmhSetSnmpEnableAuthenTraps ((INT4) u4AuthTrapStatus) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CliShowInformStats                            */
/*                                                                           */
/*     DESCRIPTION      : This function Displays SNMP Inform statistics of   */
/*                        every Target address entity                        */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains Information to Display       */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Snmp3CliShowInformStats (tCliHandle CliHandle)
{
    INT1                ai1Name[SNMP_MAX_OCTETSTRING_SIZE - 1];
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               au1IpAddr[60];
    tSnmpTgtAddrEntry  *pFirstTarget = NULL;
    if ((pFirstTarget = SNMPGetFirstTgtAddrEntry ()) == NULL)
    {
        return CLI_SUCCESS;
    }
    CliPrintf (CliHandle, "\r\n");

    do
    {
        SNPRINTF ((CHR1 *) ai1Name,
                  (UINT4) (pFirstTarget->AddrName.i4_Length + 1),
                  (const CHR1 *) pFirstTarget->AddrName.pu1_OctetList);
        CliPrintf (CliHandle, "Target Address Name : %s \r\n", ai1Name);

        if (pFirstTarget->Address.i4_Length == SNMP_IPADDR_LEN)
        {
            INET_NTOP (AF_INET,
                       pFirstTarget->Address.pu1_OctetList,
                       au1IpAddr, INET_ADDRSTRLEN);
            CliPrintf (CliHandle, "IP Address          : %s \r\n", au1IpAddr);
            CliPrintf (CliHandle, "Port                : %d \r\n",
                       pFirstTarget->u2Port);
        }
        else if (pFirstTarget->Address.i4_Length == SNMP_IP6ADDR_LEN)
        {
            INET_NTOP (AF_INET6,
                       pFirstTarget->Address.pu1_OctetList,
                       au1IpAddr, INET6_ADDRSTRLEN);
            CliPrintf (CliHandle, "IP Address          : %s \r\n", au1IpAddr);
        }

        if ((pFirstTarget->pInformStat->u4InformSent != 0) ||
            (pFirstTarget->pInformStat->u4InformAwAck != 0) ||
            (pFirstTarget->pInformStat->u4InformDrop != 0) ||
            (pFirstTarget->pInformStat->u4InformFail != 0) ||
            (pFirstTarget->pInformStat->u4InformRetried != 0) ||
            (pFirstTarget->pInformStat->u4InformResp != 0))
        {
            CliPrintf (CliHandle,
                       "Inform messages Sent        : %d \r\n",
                       pFirstTarget->pInformStat->u4InformSent);
            CliPrintf (CliHandle,
                       "Acknowledgement awaited for : %d Inform messages\r\n",
                       pFirstTarget->pInformStat->u4InformAwAck);
            CliPrintf (CliHandle,
                       "Inform messages Dropped     : %d \r\n",
                       pFirstTarget->pInformStat->u4InformDrop);
            CliPrintf (CliHandle,
                       "Acknowledgement Failed for  : %d Inform messages\r\n",
                       pFirstTarget->pInformStat->u4InformFail);
            CliPrintf (CliHandle,
                       "Informs retransmitted       : %d \r\n",
                       pFirstTarget->pInformStat->u4InformRetried);
            CliPrintf (CliHandle,
                       "Inform Responses received   : %d \r\n",
                       pFirstTarget->pInformStat->u4InformResp);
        }

        u4PagingStatus = (UINT4) CliPrintf (CliHandle,
                                            "------------------------------ \r\n");

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            break;
        }

        pFirstTarget = SNMPGetNextTgtAddrEntry (&(pFirstTarget->AddrName));
    }
    while (pFirstTarget != NULL);
    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/****************************************************************/
/*  Function Name   : Snmp3CliShowTraps                         */
/*  Description     : This function is used to display the      */
/*                    list of traps that are enabled in the     */
/*                    system                                    */
/*  Input(s)        : CliHandle   - CLI Handle                  */
/*  Output(s)       : None                                      */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                   */
/****************************************************************/

INT4
Snmp3CliShowTraps (tCliHandle CliHandle)
{

    UINT4               u4Index;
    INT4                i4ColdTrapControl;
    INT4                i4AuthTrapStatus;
    UINT1              *pu1Oid = NULL;
    UINT1               au1Name[SNMP_MAX_OID_LENGTH];
    tSnmpTrapFilterEntry *pSnmpTrapFilterEntry = NULL;
#ifdef MPLS_WANTED
    INT4                i4MplsTrapStatus;
#endif
    CliPrintf (CliHandle, "%-25s\n\r", "Currently enabled traps:");
    CliPrintf (CliHandle, "------------------------\n\r");
    for (u4Index = 0; u4Index < CLI_SNMP3_NO_OF_TRAPS; u4Index++)
    {
        if (u4Index == CLI_SNMP3_COLD_START_TRAP_ID)
        {
            if (nmhGetSnmpColdStartTrapControl (&i4ColdTrapControl) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (i4ColdTrapControl == ISS_ENABLE)
            {
                CliPrintf (CliHandle, "coldstart,");
            }
        }
        else if (u4Index == CLI_SNMP3_AUTHTRAP_STATUS_ID)
        {
            nmhGetSnmpEnableAuthenTraps (&i4AuthTrapStatus);

            if (i4AuthTrapStatus == AUTHTRAP_ENABLE)
            {
                CliPrintf (CliHandle, "authentication-failure-traps,");
            }
        }
#ifdef DHCP6_SRV_WANTED
        else if (u4Index == CLI_SNMP3_DHCP6_SERVER_TRAP_ID)
        {
            Dhcp6SrvShowTraps (CliHandle);
        }
#endif
#ifdef DHCP6_CLNT_WANTED
        else if (u4Index == CLI_SNMP3_DHCP6_CLIENT_TRAP_ID)
        {
            Dhcp6ClientShowTraps (CliHandle);
        }
#endif
#ifdef DHCP6_RLY_WANTED
        else if (u4Index == CLI_SNMP3_DHCP6_RLY_TRAP_ID)
        {
            Dhcp6RelayShowTraps (CliHandle);
        }
#endif
#ifdef ECFM_WANTED
        else if (u4Index == CLI_SNMP3_ECFM_TRAP_ID)
        {

            /* EcfmShowTraps is called with SNMP_ZERO
             * for displalying the ethernet traps and 
             * with SNMP_ONE for displaying y1731-mplstp
             * traps */

            EcfmShowTraps (CliHandle, SNMP_ZERO);
            EcfmShowTraps (CliHandle, SNMP_ONE);
        }
#endif
#ifdef MPLS_WANTED
        else if (u4Index == CLI_SNMP3_MPLS_TRAP_ID)
        {
            nmhGetMplsTunnelNotificationEnable (&i4MplsTrapStatus);
            if (i4MplsTrapStatus == SNMP_ONE)
            {
                CliPrintf (CliHandle, "\nmpls traffic-eng");
            }
        }
#endif
    }
    /* to display the filter trap table */
    pSnmpTrapFilterEntry =
        (tSnmpTrapFilterEntry *) RBTreeGetFirst (gSnmpTrapFilterTable);
    pu1Oid = MemAllocMemBlk (gSnmpMultiOidPoolId);
    while (pSnmpTrapFilterEntry != NULL)
    {
        MEMSET (au1Name, 0, SNMP_MAX_OID_LENGTH);
        CliPrintf (CliHandle, " snmp filter trap ");
        SNMPGetOidString (pSnmpTrapFilterEntry->TrapFilterOID.pu4_OidList,
                          pSnmpTrapFilterEntry->TrapFilterOID.u4_Length,
                          pu1Oid);
        SnmpONGetNameFromOid ((char *) pu1Oid, (char *) au1Name);
        if (STRLEN (au1Name) == 0)
        {
            CliPrintf (CliHandle, "oid %s\r\n", pu1Oid);
        }
        else
        {
            CliPrintf (CliHandle, "name  %s\r\n", au1Name);
        }
        pSnmpTrapFilterEntry = (tSnmpTrapFilterEntry *) RBTreeGetNext
            (gSnmpTrapFilterTable,
             pSnmpTrapFilterEntry, SnmpTrapFilterTblCmpFn);
    }
    MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1Oid);

    CliPrintf (CliHandle, "\n");
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : Snmp3CliSetTraps                         */
/*  Description     : This function is used to Enable/Disable  */
/*                    a particular Trap in the System          */
/*  Input(s)        : None                                     */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
INT4
Snmp3CliSetTraps (tCliHandle CliHandle, UINT4 u4TrapId, UINT4 u4Flag)
{

    UINT4               u4ErrorCode;
    UNUSED_PARAM (u4TrapId);
    if (u4Flag == CLI_TRAP_ENABLE)
    {
        if (nmhTestv2SnmpColdStartTrapControl (&u4ErrorCode,
                                               ISS_ENABLE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetSnmpColdStartTrapControl (ISS_ENABLE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2SnmpColdStartTrapControl (&u4ErrorCode,
                                               ISS_DISABLE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetSnmpColdStartTrapControl (ISS_DISABLE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : Snmp3CliSetSystemContact                 */
/*  Description     : This function is used to set/edit the    */
/*                    system contact information               */
/*  Input(s)        : pu1SysContact-System Contact information */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
INT4
Snmp3CliSetSystemContact (tCliHandle CliHandle, UINT1 *pu1SysContact)
{
    tSNMP_OCTET_STRING_TYPE SysContacts;
    UINT4               u4ErrorCode = 0;

    SysContacts.pu1_OctetList = pu1SysContact;
    SysContacts.i4_Length = (INT4) STRLEN (pu1SysContact);

    if (SNMP_FAILURE == nmhTestv2SysContact (&u4ErrorCode, &SysContacts))
    {
        switch (u4ErrorCode)
        {
            case SNMP_ERR_WRONG_LENGTH:
                CliPrintf (CliHandle, "%%Invalid length\r\n");
                break;
            case SNMP_ERR_WRONG_VALUE:
                CliPrintf (CliHandle, "%%Invalid input\r\n");
                break;
            default:
                break;
        }
        return SNMP_FAILURE;
    }
    if (SNMP_FAILURE == nmhSetSysContact (&SysContacts))
    {
        CLI_FATAL_ERROR (CliHandle);
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }

}

/***************************************************************/
/*  Function Name   : Snmp3CliSetSystemLocation                */
/*  Description     : This function is used to set/edit the    */
/*                    system location information              */
/*  Input(s)        : pu1SysLocation - System Location         */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
INT4
Snmp3CliSetSystemLocation (tCliHandle CliHandle, UINT1 *pu1SysLocation)
{
    tSNMP_OCTET_STRING_TYPE SysLocations;
    UINT4               u4ErrorCode = 0;

    SysLocations.pu1_OctetList = pu1SysLocation;
    SysLocations.i4_Length = (INT4) STRLEN (pu1SysLocation);

    if (SNMP_FAILURE == nmhTestv2SysLocation (&u4ErrorCode, &SysLocations))
    {
        switch (u4ErrorCode)
        {
            case SNMP_ERR_WRONG_LENGTH:
                CliPrintf (CliHandle, "%%Invalid length\r\n");
                break;
            case SNMP_ERR_WRONG_VALUE:
                CliPrintf (CliHandle, "%%Invalid input\r\n");
                break;
            default:
                break;
        }
        return SNMP_FAILURE;
    }
    if (SNMP_FAILURE == nmhSetSysLocation (&SysLocations))
    {
        CLI_FATAL_ERROR (CliHandle);
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/***************************************************************/
/*  Function Name   : Snmp3GetLinkTrapStatus                   */
/*  Description     : This function is used to get the linkup  */
/*                    linkdown trap status of each interface   */
/*  Input(s)        : None                                     */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
Snmp3GetLinkTrapStatus (VOID)
{
    INT4                i4Index;
    INT4                i4IfaceType;
    INT4                i4PrevIndex;
    INT4                i4LinkUpDownTrap;

    CFA_LOCK ();
    if (nmhGetFirstIndexIfTable (&i4Index) == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return CLI_FAILURE;
    }

    do
    {
        nmhGetIfType (i4Index, &i4IfaceType);

        if (i4IfaceType != CFA_ENET)
        {
            CFA_UNLOCK ();
            return CLI_FAILURE;
        }

        nmhGetIfLinkUpDownTrapEnable (i4Index, &i4LinkUpDownTrap);
        if (i4LinkUpDownTrap == ISS_ENABLE)
        {
            CFA_UNLOCK ();
            return CLI_SUCCESS;
        }

        i4PrevIndex = i4Index;
    }
    while (nmhGetNextIndexIfTable (i4PrevIndex, &i4Index) == SNMP_SUCCESS);

    CFA_UNLOCK ();
    return CLI_FAILURE;
}

/***************************************************************/
/*  Function Name   : Snmp3GetT1E1LinkTrapStatus               */
/*  Description     : This function is used to get the linkup  */
/*                    linkdown trap status of each T1E1        */
/*                    interface                                */
/*  Input(s)        : None                                     */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
Snmp3GetT1E1LinkTrapStatus (VOID)
{

#ifdef  T1E1_WANTED

    INT4                i4Index;
    INT4                i4IfaceType;
    INT4                i4PrevIndex;
    INT4                i4LinkUpDownTrap;

    T1E1_LOCK ();

    if (nmhGetFirstIndexDsx1CurrentTable (&i4Index) == SNMP_FAILURE)
    {
        T1E1_UNLOCK ();
        return CLI_FAILURE;
    }
    do
    {

        nmhGetDsx1LineStatusChangeTrapEnable (i4Index, &i4LinkUpDownTrap);

        if (i4LinkUpDownTrap == ISS_ENABLE)
        {
            T1E1_UNLOCK ();
            return CLI_SUCCESS;
        }

        i4PrevIndex = i4Index;
    }
    while (nmhGetNextIndexDsx1CurrentTable (i4PrevIndex,
                                            &i4Index) == SNMP_SUCCESS);

    T1E1_UNLOCK ();
#endif
    return CLI_FAILURE;
}

/***************************************************************/
/*  Function Name   : Snmp3CliGetOidFromName                   */
/*  Description     : This function is used to convert the     */
/*                    mib name to the corresponding OID.       */
/*  Input(s)        : pName - Mib name                         */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
Snmp3CliGetOidFromName (tCliHandle CliHandle, CHR1 * pName)
{
    CHR1                aIndex[SNMP_MAX_OID_LENGTH];
    UINT4               u4RetVal = 0;

    MEMSET (aIndex, '\0', SNMP_MAX_OID_LENGTH);

    u4RetVal = (UINT4) SnmpONGetOidFromName (pName, (char *) &aIndex);

    if (u4RetVal == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Entry not present\r\n");
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\rMIB OID for %s is %s\r\n", pName, aIndex);

    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : Snmp3CliGetNameFromOid                   */
/*  Description     : This function is used to convert the     */
/*                    OID to the corresponding mib name.       */
/*  Input(s)        : pOid - OID                               */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
Snmp3CliGetNameFromOid (tCliHandle CliHandle, CHR1 * pOid)
{
    CHR1                aIndex[SNMP_MAX_OID_LENGTH];
    UINT4               u4RetVal = 0;

    MEMSET (aIndex, '\0', SNMP_MAX_OID_LENGTH);

    u4RetVal = (UINT4) SnmpONGetNameFromOid (pOid, (char *) &aIndex);

    if (u4RetVal == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Entry not present\r\n");
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\rMIB Name for %s is %s\r\n", pOid, aIndex);

    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : Snmp3CliSetValueForName                  */
/*  Description     : This function is used to set the         */
/*                    value for the specified mib name         */
/*  Input(s)        : pName - mib object name,                 */
/*                    pu1Val - value to be set                 */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
Snmp3CliSetValueForName (tCliHandle CliHandle, CHR1 * pName, UINT1 *pu1Val,
                         UINT1 u1Value)
{
    UINT4               u4Val = 0;

    SNMPGetFirstIndexPool ();

    MEMSET (gau1NumberIdx, '\0', SNMP3_MAX_NAME_LEN);

    u4Val = (UINT4) SnmpONGetOidFromName (pName, (char *) &gau1NumberIdx);

    if (u4Val == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% Invalid mib object name\r\n");
        return CLI_FAILURE;
    }

    Snmp3CliSetValueForOid (CliHandle, (CHR1 *) & gau1NumberIdx, pu1Val,
                            u1Value);

    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : Snmp3CliSetTrapFilterName                */
/*  Description     : This function is used to set the         */
/*                    filter table                             */
/*  Input(s)        : pName - mib object name,                 */
/*                    u4Flag - enable/disable                  */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
Snmp3CliSetTrapFilterName (tCliHandle CliHandle, CHR1 * pName, UINT4 u4Flag)
{

    UINT4               u4Val = 0;
    CHR1                aIndex[SNMP_MAX_OID_LENGTH];
    CHR1                au1Number[SNMP_MAX_OID_LENGTH];
    tSNMP_OID_TYPE     *pNumber = NULL;
    UINT4               u4ErrorCode = SNMP_ZERO;

    MEMSET (aIndex, '\0', SNMP_MAX_OID_LENGTH);
    MEMSET (au1Number, '\0', SNMP_MAX_OID_LENGTH);

    u4Val = (UINT4) SnmpONGetOidFromName (pName, (char *) &aIndex);
    if (u4Val == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n Invalid mib object name \r\n");
        return CLI_FAILURE;
    }

    STRCPY (au1Number, aIndex);
    pNumber = SNMP_AGT_GetOidFromString ((INT1 *) au1Number);
    if (pNumber == NULL)
    {
        return SNMP_FAILURE;
    }

    if (u4Flag == CLI_TRAP_ENABLE)
    {
        if (nmhTestv2FsSnmpTrapFilterRowStatus (&u4ErrorCode, pNumber,
                                                SNMP_ROWSTATUS_CREATEANDGO)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\nCannot create as entry already exists\r\n");
            SNMP_FreeOid (pNumber);
            return CLI_FAILURE;
        }
        if (nmhSetFsSnmpTrapFilterRowStatus
            (pNumber, SNMP_ROWSTATUS_CREATEANDGO) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            SNMP_FreeOid (pNumber);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsSnmpTrapFilterRowStatus (&u4ErrorCode, pNumber,
                                                SNMP_ROWSTATUS_DESTROY)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\nCannot delete as entry doesn't exist\r\n");
            SNMP_FreeOid (pNumber);
            return CLI_FAILURE;
        }
        if (nmhSetFsSnmpTrapFilterRowStatus (pNumber, SNMP_ROWSTATUS_DESTROY)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            SNMP_FreeOid (pNumber);
            return CLI_FAILURE;
        }
    }
    SNMP_FreeOid (pNumber);
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : Snmp3CliSetTrapFilterOid                 */
/*  Description     : This function is used to set the         */
/*                    filter table                             */
/*  Input(s)        : pOid - OID                               */
/*                    u4Flag - enable/disable                  */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
Snmp3CliSetTrapFilterOid (tCliHandle CliHandle, CHR1 * pOid, UINT4 u4Flag)
{

    UINT1               au1Number[SNMP_MAX_OID_LENGTH];
    UINT4               u4ErrorCode = SNMP_ZERO;
    tSNMP_OID_TYPE     *pNumber = NULL;

    MEMSET (au1Number, '\0', SNMP_MAX_OID_LENGTH);

    if (STRLEN (pOid) > SNMP_MAX_OID_LENGTH)
    {
        SNMP_INR_BAD_ASN_PARSE;
        CliPrintf (CliHandle,
                   "\n %% OID is too big to process => %s\r\n", pOid);
        return CLI_FAILURE;
    }
    STRNCPY (au1Number, pOid, (sizeof (au1Number) - 1));
    pNumber = SNMP_AGT_GetOidFromString ((INT1 *) au1Number);

    if (pNumber == NULL)
    {
        return CLI_FAILURE;
    }

    if (u4Flag == CLI_TRAP_ENABLE)
    {
        if (nmhTestv2FsSnmpTrapFilterRowStatus (&u4ErrorCode, pNumber,
                                                SNMP_ROWSTATUS_CREATEANDGO)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\nCannot create as entry already exists\r\n");
            SNMP_FreeOid (pNumber);
            return CLI_FAILURE;
        }
        if (nmhSetFsSnmpTrapFilterRowStatus
            (pNumber, SNMP_ROWSTATUS_CREATEANDGO) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            SNMP_FreeOid (pNumber);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsSnmpTrapFilterRowStatus (&u4ErrorCode, pNumber,
                                                SNMP_ROWSTATUS_DESTROY)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\nCannot delete as entry doesn't exist\r\n");
            SNMP_FreeOid (pNumber);
            return CLI_FAILURE;
        }
        if (nmhSetFsSnmpTrapFilterRowStatus (pNumber, SNMP_ROWSTATUS_DESTROY)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            SNMP_FreeOid (pNumber);
            return CLI_FAILURE;
        }
    }
    SNMP_FreeOid (pNumber);
    return CLI_SUCCESS;
}

/***********************************************************************/
/*  Function Name   : Snmp3CliConvertStringToData                          */
/*  Description     : This function is used to convert the             */
/*                    data in string format to multidata.              */
/*  Input(s)        : pu1String - String contain data in string format.*/
/*                    u1Type - Type of the data                        */
/*  Output(s)       : None                                             */
/*  Returns         : pointer to a multidata or null                   */
/***********************************************************************/

tSNMP_MULTI_DATA_TYPE *
Snmp3CliConvertStringToData (UINT1 *pu1String, UINT1 u1Type)
{
    tSNMP_MULTI_DATA_TYPE *pData = NULL;
    UINT4               u4Temp = 0;

    if ((pData = Snmp3CliAllocMultiData ()) == NULL)
    {
        return NULL;
    }
    switch (u1Type)
    {
        case SNMP_DATA_TYPE_INTEGER32:
            pData->i4_SLongValue = ATOI (pu1String);
            pData->i2_DataType = 0x02;
            break;
        case SNMP_DATA_TYPE_UNSIGNED32:
            pData->i2_DataType = 0x42;
            pData->u4_ULongValue = (UINT4) ATOI (pu1String);
            break;
        case SNMP_DATA_TYPE_COUNTER32:
            pData->u4_ULongValue = (UINT4) ATOI (pu1String);
            pData->i2_DataType = 0x41;
            break;
        case SNMP_DATA_TYPE_COUNTER64:
            u4Temp = (UINT4) ATOI (&(pu1String[0]));
            pData->u8_Counter64Value.msn = u4Temp;
            u4Temp = (UINT4) ATOI (&(pu1String[4]));
            pData->u8_Counter64Value.lsn = u4Temp;
            pData->i2_DataType = 0x46;
            break;
        case SNMP_DATA_TYPE_IP_ADDR_PRIM:
            WebnmConvertStringToOctet (pData->pOctetStrValue, pu1String);
            MEMCPY (&pData->u4_ULongValue,
                    pData->pOctetStrValue->pu1_OctetList, 4);
            pData->u4_ULongValue = OSIX_NTOHL (pData->u4_ULongValue);
            pData->i2_DataType = 0x40;
            break;
        case SNMP_DATA_TYPE_OBJECT_ID:
            WebnmConvertStringToOid (pData->pOidValue, pu1String, 1);
            pData->i2_DataType = 0x06;
            break;
        case SNMP_DATA_TYPE_OCTET_PRIM:
            STRCPY (pData->pOctetStrValue->pu1_OctetList, pu1String);
            pData->pOctetStrValue->i4_Length = (INT4) STRLEN (pu1String);
            pData->i2_DataType = 0x04;
            break;
        case SNMP_DATA_TYPE_TIME_TICKS:
            pData->u4_ULongValue = (UINT4) ATOI (pu1String);
            pData->i2_DataType = 0x43;
            break;

        default:
            return NULL;
    }
    return pData;
}

tSNMP_MULTI_DATA_TYPE *
Snmp3CliAllocMultiData ()
{
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;
    tOsixTaskId         ProcessingTaskId = 0;

    OsixTskIdSelf (&ProcessingTaskId);

    if (gu4SnmpMultiDataCount >= SNMP3_MAX_RETRY_COUNT)
    {
        gu4SnmpMultiDataCount = 0;
    }
    pMultiData = &gSnmpMultiData[gu4SnmpMultiDataCount++];
    pMultiData->pOctetStrValue = Snmp3CliAllocOctetString ();
    pMultiData->pOidValue = Snmp3CliAllocOid ();

    if ((pMultiData->pOctetStrValue == NULL) || (pMultiData->pOidValue == NULL))
    {
        PRINTF ("Unable to Allocate memory for MultiData\n");
        return NULL;
    }

    return pMultiData;
}

tSNMP_OCTET_STRING_TYPE *
Snmp3CliAllocOctetString ()
{
    tSNMP_OCTET_STRING_TYPE *pOctetString = NULL;
    tOsixTaskId         ProcessingTaskId = 0;

    OsixTskIdSelf (&ProcessingTaskId);

    if (gu4SnmpOctetStrCount >= SNMP3_MAX_RETRY_COUNT)
    {
        gu4SnmpOctetStrCount = 0;
    }
    pOctetString = &gSnmpOctetString[gu4SnmpOctetStrCount];
    pOctetString->pu1_OctetList =
        &gau1SnmpOctetString[gu4SnmpOctetStrCount++][0];
    pOctetString->i4_Length = 0;
    return pOctetString;
}

tSNMP_OID_TYPE     *
Snmp3CliAllocOid ()
{
    tSNMP_OID_TYPE     *pOid = NULL;
    tOsixTaskId         ProcessingTaskId = 0;

    OsixTskIdSelf (&ProcessingTaskId);

    if (gu4SnmpOidCount >= SNMP3_MAX_RETRY_COUNT)
    {
        gu4SnmpOidCount = 0;
    }

    pOid = &gSnmpOidType[gu4SnmpOidCount];
    pOid->pu4_OidList = &gau4SnmpOidType[gu4SnmpOidCount++][0];
    pOid->u4_Length = 0;
    return pOid;
}

/***************************************************************/
/*  Function Name   : Snmp3CliSetValueForOid                   */
/*  Description     : This function is used to set the value   */
/*                    for the specified mib object.            */
/*  Input(s)        : pOid - OID                               */
/*                    pu1Val - value to be set                 */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
Snmp3CliSetValueForOid (tCliHandle CliHandle, CHR1 * pOid, UINT1 *pu1Val,
                        UINT1 u1Value)
{
    UINT1               au1Number[SNMP_MAX_OID_LENGTH];
    UINT1               au1Name[SNMP_MAX_OID_LENGTH];
    tSNMP_OID_TYPE     *pNumber = NULL;
    tSnmpIndex         *pIndex;
    UINT4               u4Len = 0;
    UINT4               u4RetVal = 0;
    UINT4               u4Error = 0;
    UINT1               u1OidType = 0;
    tSNMP_MULTI_DATA_TYPE *pData = NULL;
    tSnmpDbEntry        SnmpDbEntry;
    tMbDbEntry         *pCur = NULL, *pNext = NULL;

    pIndex = SNMPGetFirstIndexPool ();

    MEMSET (au1Number, '\0', SNMP_MAX_OID_LENGTH);
    MEMSET (au1Name, '\0', SNMP_MAX_OID_LENGTH);
    MEMSET (&SnmpDbEntry, 0, sizeof (tSnmpDbEntry));

    if (STRLEN (pOid) > SNMP_MAX_OID_LENGTH)
    {
        SNMP_INR_BAD_ASN_PARSE;
        CliPrintf (CliHandle,
                   "\n %% OID is too big to process => %s\r\n", pOid);
        return CLI_FAILURE;
    }
    STRNCPY (au1Number, pOid, (sizeof (au1Number) - 1));
    pNumber = SNMP_AGT_GetOidFromString ((INT1 *) au1Number);

    if (pNumber == NULL)
    {
        return CLI_FAILURE;
    }

    GetMbDbEntry (*pNumber, &pNext, &u4Len, &SnmpDbEntry);
    pCur = SnmpDbEntry.pMibEntry;
    if (pCur == NULL)
    {
        CliPrintf (CliHandle,
                   " %% No Such Object available on this agent::%s\r\n", pOid);
        SNMP_FreeOid (pNumber);
        return CLI_FAILURE;
    }

    u1OidType = (UINT1) pCur->u4OIDType;

    pData = Snmp3CliConvertStringToData (pu1Val, u1OidType);
    if (pData == NULL)
    {
        SNMP_FreeOid (pNumber);
        return CLI_FAILURE;
    }
    if (u1OidType == SNMP_DATA_TYPE_OCTET_PRIM)
    {
        WebnmConvertColonStringToOctet (pData->pOctetStrValue, pu1Val);
        if ((pData->pOctetStrValue->i4_Length) < SNMP_MAX_OID_LENGTH)
        {
            pData->pOctetStrValue->pu1_OctetList[pData->
                                                 pOctetStrValue->i4_Length] =
                '\0';
        }
    }

    u4RetVal = (UINT4) SNMPTest (*pNumber, pData, &u4Error,
                                 pIndex, SNMP_DEFAULT_CONTEXT);
    if (u4RetVal == SNMP_FAILURE)
    {
        if (u4Error == SNMP_ERR_WRONG_VALUE)
        {
            CliPrintf (CliHandle, " %%wrongValue::%s =: %s\r\n", pOid, pu1Val);
        }
        else if (u4Error == SNMP_ERR_WRONG_LENGTH)
        {
            CliPrintf (CliHandle, " %%wrongLength::%s =: %s\r\n", pOid, pu1Val);
        }
        else if (u4Error == SNMP_ERR_NO_CREATION)
        {
            CliPrintf (CliHandle, " %%noCreation::%s =: %s\r\n", pOid, pu1Val);
        }
        else if (u4Error == SNMP_ERR_INCONSISTENT_VALUE)
        {
            CliPrintf (CliHandle, " %%Inconsistent value::%s =: %s\r\n", pOid,
                       pu1Val);
        }
        else if (u4Error == SNMP_ERR_INCONSISTENT_NAME)
        {
            CliPrintf (CliHandle, " %%Inconsistent name::%s =: %s\r\n", pOid,
                       pu1Val);
        }
        else if (u4Error == SNMP_ERR_NOT_WRITABLE)
        {
            CliPrintf (CliHandle, " %%Not Writable::%s =: %s\r\n", pOid,
                       pu1Val);
        }
        else
        {
            if (u4Error != SNMP_ERR_NO_ERROR)
            {
                CliPrintf (CliHandle, " %%Err Code %d::%s =: %s\r\n", u4Error,
                           pOid, pu1Val);
            }
        }

        SNMP_FreeOid (pNumber);
        return CLI_FAILURE;
    }

    /* Memsetting the Manager Ip address purposefully to avoid displaying 
       wrong IP address in syslog message generated when SNMPSet performed from CLI */
    MEMSET (&gManagerIpAddr, 0, sizeof (tMgrIpAddr));
    u4RetVal =
        (UINT4) SNMPSet (*pNumber, pData, &u4Error, pIndex,
                         SNMP_DEFAULT_CONTEXT);

    if (u1OidType == SNMP_DATA_TYPE_OCTET_PRIM)
    {
        Snmp3CliConvertDataToString (pData, (UINT1 *) pu1Val, u1OidType);
    }

    if (u4RetVal == SNMP_FAILURE)
    {
        if (u4Error == SNMP_ERR_NO_ACCESS)
        {
            CliPrintf (CliHandle, " %%noAccess::%s =: %s\r\n", pOid, pu1Val);
        }
        else if (u4Error == SNMP_ERR_NO_CREATION)
        {
            CliPrintf (CliHandle, " %%noCreation::%s =: %s\r\n", pOid, pu1Val);
        }
        else if (u4Error == SNMP_ERR_COMMIT_FAILED)
        {
            CliPrintf (CliHandle, " %%Commit Failed::%s =: %s\r\n", pOid,
                       pu1Val);
        }
        else if (u4Error == SNMP_ERR_GEN_ERR)
        {
            CliPrintf (CliHandle, " %%Gen Error :%s =: %s\r\n", pOid, pu1Val);
        }
        else
        {
            if (u4Error != SNMP_ERR_NO_ERROR)
            {
                CliPrintf (CliHandle, " %%Err Code %d::%s =: %s\r\n", u4Error,
                           pOid, pu1Val);
            }
        }
        SNMP_FreeOid (pNumber);
        return CLI_FAILURE;
    }
    if (u1Value == 1)
    {
        CliPrintf (CliHandle, "\r\n%s\r\n", pu1Val);
        SNMP_FreeOid (pNumber);
        return CLI_SUCCESS;
    }
    SnmpONGetNameFromOid ((char *) pOid, (char *) au1Name);
    CliPrintf (CliHandle, "\r\n%s =: %s\r\n", au1Name, pu1Val);

    SNMP_FreeOid (pNumber);
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : Snmp3CliGetName                          */
/*  Description     : This function is used to get the value   */
/*                    of the mib object                        */
/*  Input(s)        : pName - mib name                         */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
Snmp3CliGetName (tCliHandle CliHandle, CHR1 * pName, UINT1 u1Value)
{
    UINT4               u4RetVal = 0;
    UINT4               u4Val = 0;

    SNMPGetFirstIndexPool ();

    MEMSET (gau1NumberIdx, '\0', SNMP3_MAX_NAME_LEN);

    u4Val = (UINT4) SnmpONGetOidFromName (pName, (char *) gau1NumberIdx);

    if (u4Val == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    u4RetVal =
        (UINT4) Snmp3CliGetOid (CliHandle, (CHR1 *) & gau1NumberIdx, u1Value);

    if (u4RetVal == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : Snmp3CliConvertDataToString              */
/*  Description     : This function is used to convert the     */
/*                    multidata to string.                     */
/*  Input(s)        : pData - Pointer to multidata             */
/*                    u1Type - Type of element in multidata    */
/*  Output(s)       : pu1String - pointer to a string where the*/
/*                    result will be written                   */
/*  Returns         : None                                     */
/***************************************************************/

VOID
Snmp3CliConvertDataToString (tSNMP_MULTI_DATA_TYPE * pData,
                             UINT1 *pu1String, UINT1 u1Type)
{
    UINT4               u4Count = 0, u4Value = 0, u4Temp = 0;
    UINT1               au1Temp[SNMP_MAX_OID_LENGTH];

    pu1String[0] = '\0';
    MEMSET (au1Temp, 0, sizeof (au1Temp));

    switch (u1Type)
    {
        case SNMP_DATA_TYPE_INTEGER32:

            SPRINTF ((CHR1 *) pu1String, "%d", pData->i4_SLongValue);

            break;

        case SNMP_DATA_TYPE_UNSIGNED32:
        case SNMP_DATA_TYPE_COUNTER32:
        case SNMP_DATA_TYPE_TIME_TICKS:

            SPRINTF ((CHR1 *) pu1String, "%u", pData->u4_ULongValue);
            break;

        case SNMP_DATA_TYPE_COUNTER64:

            SPRINTF ((CHR1 *) pu1String, "%u.", pData->u8_Counter64Value.msn);
            SPRINTF ((CHR1 *) au1Temp, "%u", pData->u8_Counter64Value.lsn);
            STRCAT (pu1String, au1Temp);
            break;

        case SNMP_DATA_TYPE_IP_ADDR_PRIM:

            u4Value = pData->u4_ULongValue;

            for (u4Count = 0; u4Count < SNMP_MAX_IPADDR_LEN - 1; u4Count++)
            {
                u4Temp = (u4Value & 0xff000000);
                u4Temp = u4Temp >> 24;
                SPRINTF ((CHR1 *) au1Temp, "%u.", u4Temp);
                STRCAT (pu1String, au1Temp);
                u4Value = u4Value << SNMP3_BYTE_LEN;
            }

            u4Temp = (u4Value & 0xff000000);
            u4Temp = u4Temp >> 24;
            SPRINTF ((CHR1 *) au1Temp, "%u", u4Temp);
            STRCAT (pu1String, au1Temp);
            break;

        case SNMP_DATA_TYPE_OBJECT_ID:

            for (u4Count = 0; u4Count < pData->pOidValue->u4_Length; u4Count++)
            {
                if (u4Count == pData->pOidValue->u4_Length - 1)
                {
                    SPRINTF ((CHR1 *) au1Temp, "%u",
                             pData->pOidValue->pu4_OidList[u4Count]);
                }
                else
                {
                    SPRINTF ((CHR1 *) au1Temp, "%u.",
                             pData->pOidValue->pu4_OidList[u4Count]);
                }
                STRCAT (pu1String, au1Temp);
            }
            break;
        case SNMP_DATA_TYPE_MAC_ADDRESS:
            /*Intentional fall through */
        case SNMP_DATA_TYPE_OCTET_PRIM:

            for (u4Count = 0; u4Count <
                 (UINT4) (pData->pOctetStrValue->i4_Length); u4Count++)
            {
                if (u4Count == (UINT4) (pData->pOctetStrValue->i4_Length - 1))
                {
                    SPRINTF ((CHR1 *) au1Temp, "%02x",
                             pData->pOctetStrValue->pu1_OctetList[u4Count]);
                    if (STRLEN (pu1String) + STRLEN (au1Temp) <
                        SNMP3_MAX_NAME_LEN)
                    {
                        STRCAT (pu1String, au1Temp);
                    }
                }
                else
                {
                    SPRINTF ((CHR1 *) au1Temp, "%02x",
                             pData->pOctetStrValue->pu1_OctetList[u4Count]);
                    if (STRLEN (pu1String) + STRLEN (au1Temp) + 1 <
                        SNMP3_MAX_NAME_LEN)
                    {
                        STRCAT (pu1String, au1Temp);
                        STRCAT (pu1String, ":");
                    }
                }
            }
            break;

        default:
            break;
    }
    return;
}

/***************************************************************/
/*  Function Name   : Snmp3CliGetOid                           */
/*  Description     : This function is used to get the         */
/*                    value of the given mib OID.              */
/*  Input(s)        : pOid - OID                               */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
Snmp3CliGetOid (tCliHandle CliHandle, CHR1 * pOid, UINT1 u1Value)
{
    UINT1               au1Number[SNMP_MAX_OID_LENGTH];
    UINT1               au1Name[SNMP_MAX_OID_LENGTH];
    tSNMP_OID_TYPE     *pNumber = NULL;
    tSnmpIndex         *pIndex = NULL;
    UINT4               u4Len = 0;
    UINT4               u4RetVal = 0;
    UINT4               u4Error = 0;
    UINT1               u1OidType = 0;
    tSNMP_MULTI_DATA_TYPE *pData = NULL;
    tSnmpDbEntry        SnmpDbEntry;
    tMbDbEntry         *pCur = NULL, *pNext = NULL;

    SNMP_ALLOC_MULTIDATA (pData);
    MEMSET (pData->pOctetStrValue->pu1_OctetList, 0, 10);

    pIndex = SNMPGetFirstIndexPool ();

    MEMSET (au1Number, '\0', SNMP_MAX_OID_LENGTH);
    MEMSET (au1Name, '\0', SNMP_MAX_OID_LENGTH);
    MEMSET (&SnmpDbEntry, 0, sizeof (tSnmpDbEntry));

    if (STRLEN (pOid) > SNMP_MAX_OID_LENGTH)
    {
        SNMP_INR_BAD_ASN_PARSE;
        CliPrintf (CliHandle,
                   "\n %% OID is too big to process => %s\r\n", pOid);
        return CLI_FAILURE;
    }
    STRNCPY (au1Number, pOid, (sizeof (au1Number) - 1));
    pNumber = SNMP_AGT_GetOidFromString ((INT1 *) au1Number);

    if (pNumber == NULL)
    {
        return SNMP_FAILURE;
    }

    GetMbDbEntry (*pNumber, &pNext, &u4Len, &SnmpDbEntry);
    pCur = SnmpDbEntry.pMibEntry;

    if (pCur == NULL)
    {
        CliPrintf (CliHandle,
                   " %% No Such Object available on this agent::%s\r\n", pOid);
        SNMP_FreeOid (pNumber);
        return SNMP_FAILURE;
    }

    u1OidType = (UINT1) pCur->u4OIDType;

    u4RetVal = (UINT4) SNMPGet (*pNumber, pData, &u4Error, pIndex,
                                SNMP_DEFAULT_CONTEXT);

    if (u4Error != SNMP_ERR_NO_ERROR)
    {
        CliPrintf (CliHandle, " %%Err Code %d::%s\r\n", u4Error, pOid);
        SNMP_FreeOid (pNumber);
        return CLI_FAILURE;
    }
    if (u4RetVal == SNMP_FAILURE)
    {
        SNMP_FreeOid (pNumber);
        return CLI_FAILURE;
    }

    Snmp3CliConvertDataToString (pData, (UINT1 *) pOid, u1OidType);

    if (u1Value == 1)
    {
        CliPrintf (CliHandle, " %s\r\n", pOid);
        SNMP_FreeOid (pNumber);
        return CLI_SUCCESS;
    }

    SnmpONGetNameFromOid ((char *) au1Number, (char *) au1Name);
    CliPrintf (CliHandle, "%s =: %s\r\n", au1Name, pOid);

    SNMP_FreeOid (pNumber);
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : Snmp3CliGetNextName                      */
/*  Description     : This function is used to get the         */
/*                    value of the next mib object.            */
/*  Input(s)        : pName - mib object name                  */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
Snmp3CliGetNextName (tCliHandle CliHandle, CHR1 * pName, UINT1 u1Value)
{

    UINT4               u4RetVal = 0;
    UINT4               u4Val = 0;

    SNMPGetFirstIndexPool ();
    SNMPGetSecondIndexPool ();

    MEMSET (gau1NumberIdx, '\0', SNMP3_MAX_NAME_LEN);

    u4Val = (UINT4) SnmpONGetOidFromName (pName, gau1NumberIdx);

    if (u4Val == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    u4RetVal =
        (UINT4) Snmp3CliGetNextOid (CliHandle, (CHR1 *) & gau1NumberIdx,
                                    u1Value);

    if (u4RetVal == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : Snmp3CliGetNextOid                       */
/*  Description     : This function is used to get the         */
/*                    value of the next mib OID.               */
/*  Input(s)        : pOid - OID                               */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
Snmp3CliGetNextOid (tCliHandle CliHandle, CHR1 * pOid, UINT1 u1Value)
{
    UINT1               au1Number[SNMP_MAX_OID_LENGTH];
    UINT1               au1Name[SNMP_MAX_OID_LENGTH];
    tSNMP_OID_TYPE     *pNumber = NULL, *pNextOid = NULL;
    tSnmpIndex         *pCurIndex, *pNextIndex = NULL;
    UINT4               u4RetVal = 0;
    UINT4               u4Error = 0;
    UINT1               u1OidType = 0;
    UINT1               aOid[SNMP_MAX_OID_LENGTH];
    tSNMP_MULTI_DATA_TYPE *pTableData = NULL;
    tSnmpDbEntry        SnmpDbEntry;

    pCurIndex = SNMPGetFirstIndexPool ();
    pNextIndex = SNMPGetSecondIndexPool ();

    SNMP_ALLOC_OID (pNextOid);
    SNMP_ALLOC_MULTIDATA (pTableData);

    MEMSET (au1Number, '\0', SNMP_MAX_OID_LENGTH);
    MEMSET (au1Name, '\0', SNMP_MAX_OID_LENGTH);
    MEMSET (aOid, '\0', SNMP_MAX_OID_LENGTH);
    MEMSET (&SnmpDbEntry, 0, sizeof (tSnmpDbEntry));

    if (STRLEN (pOid) > SNMP_MAX_OID_LENGTH)
    {
        SNMP_INR_BAD_ASN_PARSE;
        CliPrintf (CliHandle,
                   "\n %% OID is too big to process => %s\r\n", pOid);
        return CLI_FAILURE;
    }
    STRNCPY (au1Number, pOid, (sizeof (au1Number) - 1));
    pNumber = SNMP_AGT_GetOidFromString ((INT1 *) au1Number);

    if (pNumber == NULL)
    {
        return SNMP_FAILURE;
    }

    u4RetVal = (UINT4) SNMPGetNextOID (*pNumber, pNextOid, pTableData,
                                       &u4Error, pCurIndex, pNextIndex,
                                       SNMP_DEFAULT_CONTEXT);

    if (u4RetVal == SNMP_FAILURE)
    {
        SNMP_FreeOid (pNumber);
        return CLI_FAILURE;
    }

    u1OidType = (UINT1) pTableData->i2_DataType;

    Snmp3CliConvertDataToString (pTableData, (UINT1 *) pOid, u1OidType);
    WebnmConvertOidToString (pNextOid, aOid);
    if (u1Value == 1)
    {
        CliPrintf (CliHandle, "%s \r\n", pOid);
        SNMP_FreeOid (pNumber);
        return CLI_SUCCESS;
    }
    SnmpONGetNameFromOid ((char *) au1Number, (char *) au1Name);
    CliPrintf (CliHandle, "%s =: %s \r\n", aOid, pOid);

    SNMP_FreeOid (pNumber);
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : Snmp3CliNameWalk                         */
/*  Description     : This function is used to display         */
/*                    the values of a given mib object.        */
/*  Input(s)        : pName - pointer to mib name              */
/*                    u1Count - no of entries to be displayed  */
/*                              for corresponding object       */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
Snmp3CliNameWalk (tCliHandle CliHandle, CHR1 * pName, UINT1 u1Count,
                  UINT1 u1Value)
{
    UINT4               u4RetVal = 0;
    UINT4               u4Val = 0;

    SNMPGetFirstIndexPool ();
    SNMPGetSecondIndexPool ();

    MEMSET (gau1NumberIdx, '\0', SNMP3_MAX_NAME_LEN);

    u4Val = (UINT4) SnmpONGetOidFromName (pName, gau1NumberIdx);
    if (u4Val == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    u4RetVal =
        (UINT4) Snmp3CliOidWalk (CliHandle, (CHR1 *) & gau1NumberIdx, u1Count,
                                 u1Value);

    if (u4RetVal == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : Snmp3CliOidWalk                          */
/*  Description     : This function is used to display the     */
/*                    values of a given OID.                   */
/*  Input(s)        : pOid - OID                               */
/*                    u1Count - no of entries to be displayed  */
/*                              for corresponding object       */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
Snmp3CliOidWalk (tCliHandle CliHandle, CHR1 * pOid, UINT1 u1Count,
                 UINT1 u1Value)
{
    if (STRLEN (pOid) > SNMP_MAX_OID_LENGTH)
    {
        SNMP_INR_BAD_ASN_PARSE;
        CliPrintf (CliHandle,
                   "\n %% OID is too big to process => %s\r\n", pOid);
        return CLI_FAILURE;
    }
    if ((STRCMP (pOid, "1")) == 0)
    {
        Snmp3CliWalk (CliHandle, pOid, 0, u1Value);
        return CLI_SUCCESS;
    }

    if (u1Count != SNMP_ZERO)
    {
        Snmp3CliWalk (CliHandle, pOid, u1Count, u1Value);
    }
    else if (u1Count == SNMP_ZERO)
    {
        Snmp3CliWalk (CliHandle, pOid, 0, u1Value);
    }
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : Snmp3CliWalk                             */
/*  Description     : This function is used to display all walk*/
/*                    objects                                  */
/*  Input(s)        : pOid - OID                               */
/*                    u1Count - no of entries to be displayed  */
/*                              for corresponding object       */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
Snmp3CliWalk (tCliHandle CliHandle, CHR1 * pOid, UINT1 u1Count, UINT1 u1Value)
{
    tSNMP_OID_TYPE     *pOidLenCheck = NULL;
    tSNMP_OID_TYPE     *pNumber = NULL;
    tSNMP_OID_TYPE     *pNextOid = NULL;
    tSNMP_MULTI_DATA_TYPE *pTableData = NULL;
    tSnmpIndex         *pCurIndex = NULL;
    tSnmpIndex         *pNextIndex = NULL;
    UINT4               u4Error = 0;
    UINT4               u4RetVal = 0;
    UINT4               u4RetGetVal = 0;
    INT1                i1Index = 0;
    UINT1               au1Number[SNMP_MAX_OID_LENGTH];
    UINT1               au1Oid[SNMP_MAX_OID_LENGTH];
    UINT1               au1Name[SNMP_MAX_OID_LENGTH];
    UINT1               u1OidType = 0;
    UINT4               u4Count = 0;
    UINT4               u4RetStat = 0;

    pCurIndex = SNMPGetFirstIndexPool ();
    pNextIndex = SNMPGetSecondIndexPool ();

    SNMP_ALLOC_OID (pNextOid);
    SNMP_ALLOC_MULTIDATA (pTableData);

    MEMSET (au1Oid, '\0', SNMP_MAX_OID_LENGTH);
    MEMSET (au1Name, '\0', SNMP_MAX_OID_LENGTH);
    MEMSET (au1Number, '\0', SNMP_MAX_OID_LENGTH);

    STRNCPY (au1Number, pOid, (SNMP_MAX_OID_LENGTH - 1));
    pNumber = SNMP_AGT_GetOidFromString ((INT1 *) au1Number);
    pOidLenCheck = pNumber;

    if (pNumber == NULL)
    {
        return CLI_FAILURE;
    }
    u4RetGetVal = (UINT4) SNMPGet (*pNumber, pTableData, &u4Error,
                                   pCurIndex, SNMP_DEFAULT_CONTEXT);
    if (u4RetGetVal == SNMP_SUCCESS)
    {
        u1OidType = (UINT1) pTableData->i2_DataType;
        Snmp3CliConvertDataToString (pTableData, (UINT1 *) pOid, u1OidType);
        WebnmConvertOidToString (pNumber, au1Oid);

        if (u1Value == 1)
        {
            CliPrintf (CliHandle, "%s \r\n", pOid);
        }
        if (u1Value != 1)
        {
            SnmpONGetNameFromOid ((char *) au1Oid, (char *) au1Name);
            CliPrintf (CliHandle, "%s =: %s \r\n", au1Name, pOid);
        }
        SNMP_FreeOid (pNumber);
        return CLI_SUCCESS;
    }
    do
    {
        MEMSET (au1Oid, '\0', SNMP_MAX_OID_LENGTH);
        u4RetVal =
            (UINT4) SNMPGetNextOID (*pNumber, pNextOid, pTableData, &u4Error,
                                    pCurIndex, pNextIndex,
                                    SNMP_DEFAULT_CONTEXT);
        u1OidType = (UINT1) pTableData->i2_DataType;

        if (u4RetVal == SNMP_SUCCESS)
        {
            Snmp3CliConvertDataToString (pTableData, (UINT1 *) pOid, u1OidType);
            WebnmConvertOidToString (pNextOid, au1Oid);

            u4RetStat = OIDCompare (*pOidLenCheck, *pNextOid, &u4Count);
            if (u4Count == pOidLenCheck->u4_Length)
            {
                if (u1Value == 1)
                {
                    CliPrintf (CliHandle, "%s \r\n", pOid);
                }
                if (u1Value != 1)
                {
                    SnmpONGetNameFromOid ((char *) au1Oid, (char *) au1Name);
                    CliPrintf (CliHandle, "%s =: %s \r\n", au1Name, pOid);
                }
            }
            else
            {
                break;
            }
            if ((u4Error == SNMP_ERR_NO_ERROR)
                && (OIDCompare (*pNumber, *pNextOid, &u4Count)) == SNMP_GREATER)
            {
                CliPrintf (CliHandle, "Lexicographical order is breached \r\n");
                break;
            }
            i1Index++;
            if ((u1Count != 0) && (i1Index >= u1Count))
            {
                break;
            }

            pNumber = pNextOid;
        }
        else if (u4RetVal == SNMP_FAILURE)
        {
            break;
        }

    }
    while (1);
    SNMP_FreeOid (pOidLenCheck);
    UNUSED_PARAM (u4RetStat);
    return CLI_SUCCESS;
}

#endif
