
/* $Id: snmptmr.c,v 1.8 2015/04/28 12:35:02 siva Exp $ */

#include "snmpcmn.h"
#include "snprxtrn.h"

/************************************************************************/
/*  Function Name   : SnmpStartTimer                                    */
/*  Description     : This function start a timer for 'u4Sec' duration. */
/*  Input(s)        : TimerList - Timer List Id                         */
/*                  : pTimer    - pointer to timer node                 */
/*                  : u4Sec     - Timer expiry time in seconds.         */
/*  Output(s)       : None.                                             */
/*  Returns         : SNMP_SUCCESS or SNMP_FAILURE                      */
/************************************************************************/

INT4
SnmpStartTimer (tTimerListId TimerList, tTmrAppTimer * pTimer, UINT4 u4Sec)
{
    UINT4               u4Ticks;

    /* This function start a timer for u4Sec Duration .. */

    u4Ticks = u4Sec * SYS_NUM_OF_TIME_UNITS_IN_A_SEC;

    if (TmrStartTimer (TimerList, pTimer, u4Ticks) == TMR_FAILURE)
    {
        SNMPTrace ("Starting SNMP Inform Timer failed.\r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : SnmpProcessTmrExpiry                              */
/*  Description     : Process Timer Expiry, Depending on the timer id,  */
/*                    calls appropriate functions                       */
/*  Input           : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/

VOID
SnmpProcessTmrExpiry (VOID)
{
    tTmrAppTimer       *pTimer = NULL;
    tInformTimer       *pTemp = NULL;
    tInformNode        *pNode = NULL;

    SNMP_INFORM_LOCK ();
    while ((pTimer = TmrGetNextExpiredTimer (SnmpAgtTimerListId)) != NULL)
    {
        /* Get the SNMP Agent defined structure pointer
           which is passed while starting the timer and has the timer id, 
           INFORM Req Id and the Trap Manager index  */

        {
            pNode = (tInformNode *) pTimer->u4Data;
            pTemp = &pNode->InformTmr;

            switch (pTemp->u4TimerId)
            {
                case INFORM_TIMER_EXPIRY:

                    if (SnmpInformTimerExpiry (pTemp) != SNMP_SUCCESS)
                    {
                        SNMPTrace ("Problem in Inform Timer expiry process \n");
                    }
                    break;

                default:
                    break;
            }                    /* End of switch statement */

        }                        /* End of if */
    }                            /* End of while statement */

    SNMP_INFORM_UNLOCK ();
    return;
}

/************************************************************************
 *  Function Name   : SnmpStopTimers
 *  Description     : Function to Stop the running Timers for Inform
 *  Input           : None 
 *  Output          : None 
 *  Returns         : None 
 ************************************************************************/

VOID
SnmpStopTimers ()
{
    tInformNode        *pNode = NULL;
    tTMO_SLL           *pInformList = NULL;
    tSnmpTgtAddrEntry  *pFirstTarget = NULL;

    if ((pFirstTarget = SNMPGetFirstTgtAddrEntry ()) == NULL)
    {
        return;
    }

    do
    {
        /* Scan on all the Trap Manager entries for Inform List */
        pInformList = &(pFirstTarget->pInformList);

        if (pInformList != NULL)
        {
            /* Scan through the InformList  */
            TMO_SLL_Scan (pInformList, pNode, tInformNode *)
            {
                if (pNode != NULL)
                {
                    TmrStopTimer (SnmpAgtTimerListId,
                                  &(pNode->InformTmr.TmrInform));

                }                /* end of if */

            }                    /* scan */

        }                        /* end of if */
        pFirstTarget = SNMPGetNextTgtAddrEntry (&(pFirstTarget->AddrName));
    }                            /* end of do...while */
    while (pFirstTarget != NULL);

    return;
}

/************************************************************************
 *  Function Name   : SNMPGetTime 
 *  Description     : Function to Get Current System Time 
 *  Input           : pu4Time - Time variable to update 
 *  Output          : None 
 *  Returns         : None 
 ************************************************************************/

VOID
SNMPGetTime (UINT4 *pu4Time)
{
    OsixGetSysTime (pu4Time);
}

/************************************************************************
 *  Function Name   : SnmpStartInformReTxTimer 
 *  Description     : Function to start the re-transmission timer. 
 *  Input           : u4NewReqId - INFORM REQ ID, u4Index - Trap Mgr Index 
 *                  : u4PrevReqId - INFORM REQ ID for the previous msg,  
 *                    pSnmpTgtAddrEntry - pointer to Target addr entry
 *                    pSnmpNotifyEntry - pointer to Notification entry
 *  Output          : None 
 *  Returns         : SNMP_SUCCESS, SNMP_FAILURE 
 ************************************************************************/

INT4
SnmpStartInformReTxTimer (UINT4 u4NewReqId, UINT4 u4PrevReqId,
                          tSnmpTgtAddrEntry * pSnmpTgtAddrEntry,
                          tSnmpNotifyEntry * pSnmpNotifyEntry)
{

    tInformNode        *pInformNode = NULL;
    INT4                i4Timeout = SNMP_ZERO;

    if (pSnmpTgtAddrEntry)
    {
        pInformNode = SnmpGetInformNode (u4PrevReqId, pSnmpTgtAddrEntry);
    }

    if (pInformNode == NULL)
    {
        SNMPTrace ("Inform Node not found for the request. \n");
        return SNMP_FAILURE;
    }

    i4Timeout = (INT4) ((pSnmpTgtAddrEntry->i4TimeOut) / 100);

    pInformNode->u4InformReqId = u4NewReqId;
    pInformNode->InformTmr.u4TimerId = INFORM_TIMER_EXPIRY;
    pInformNode->InformTmr.pManagerAddr = (VOID *) pSnmpTgtAddrEntry;
    pInformNode->InformTmr.TmrInform.u4Data = (FS_ULONG) (pInformNode);
    /*Fill the timer node here with the notification type information */
    pInformNode->InformTmr.pNotifyEntry = pSnmpNotifyEntry;

    SnmpStartTimer (SnmpAgtTimerListId,
                    &(pInformNode->InformTmr.TmrInform), (UINT4) i4Timeout);
    return SNMP_SUCCESS;
}

/*************************************************************************/
/*  Function Name   : SnmpInformTimerExpiry                              */
/*  Description     : This function checks which Inform Node's timer has */
/*                  : expired. Regenerates a new Request to Manager.     */
/*                  : Starts the Retransmission Timer.                   */
/*  Input(s)        : pTimer - pointer to timer node.                    */
/*  Output(s)       : None.                                              */
/*  Returns         : SNMP_SUCCESS or SNMP_FAILURE                       */
/************************************************************************/

INT4
SnmpInformTimerExpiry (tInformTimer * pTimer)
{
    UINT4               u4ReqId = 0;
    tInformNode        *pNode = NULL;
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    tSnmpNotifyEntry   *pSnmpNotifyEntry = NULL;
    UINT4               u4CbStatus = SNMP_INFORM_NOT_ACKED;
    UINT4               u4IsCallBack = SNMP_TRUE;

    /* Callback fn must be executed
     * when Inform node is removed as
     * number of retries exceded the
     * max value
     */

    pSnmpTgtAddrEntry = (tSnmpTgtAddrEntry *) pTimer->pManagerAddr;

    pSnmpNotifyEntry = (tSnmpNotifyEntry *) pTimer->pNotifyEntry;

    pNode = (tInformNode *) (pTimer->TmrInform.u4Data);

    if (pNode == NULL)
    {
        return SNMP_FAILURE;
    }

    u4ReqId = pNode->u4InformReqId;

    /* Increase the ACK failed count for the Manager */
    SNMP_MGR_INR_INFORM_ACK_FAILED (pSnmpTgtAddrEntry);
    SNMP_MGR_DEC_INFORM_AWAIT_ACK (pSnmpTgtAddrEntry);

    /* Check the Re-transmission count. If Retransmit is equal to the max value,
     * delete the node and increase the counters. */

    if (pSnmpNotifyEntry)
    {
        if (((pNode != NULL)
             && (pNode->i4RetryCount >= pSnmpTgtAddrEntry->i4RetryCount))
            || (pSnmpNotifyEntry->i4Status == SNMP_INACTIVE)
            || (pSnmpNotifyEntry->i4NotifyType == SNMP_TRAP_TYPE))
        {
            SnmpRemoveInformNode (pSnmpTgtAddrEntry, u4ReqId,
                                  u4IsCallBack, u4CbStatus);
        }
        else
        {
            /* Resend the Inform Request to the Same Manager with a Different 
             * Req Id  on-timeout*/
            if (SnmpResendInformReqOnTimeOut
                (pNode, pSnmpTgtAddrEntry, pSnmpNotifyEntry) != SNMP_SUCCESS)
            {
                SNMPTrace ("Re-transmission of INFORM REQUEST failed. \n");
                return SNMP_FAILURE;
            }

        }
    }
    else
    {
        if ((pNode != NULL)
            && (pNode->i4RetryCount >= pSnmpTgtAddrEntry->i4RetryCount))
        {
            SnmpProxyRemoveInformNode (pSnmpTgtAddrEntry, u4ReqId, NULL,
                                       u4IsCallBack, u4CbStatus);
        }
        else
        {
            /* Resend the Inform Request to the Same Manager with a Different 
             * Req Id  on-timeout*/
            if (SnmpProxyResendInformOnTimeOut
                (pNode, pSnmpTgtAddrEntry) != SNMP_SUCCESS)
            {
                SNMPTrace ("Re-transmission of INFORM REQUEST failed. \n");
                return SNMP_FAILURE;
            }

        }
    }

    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : SNMPStartProxyRequestTimer 
 *  Description     : Function to start the re-transmission timer. 
 *  Input           : pCacheData - Cache node
 *                  : u4PrevReqId - INFORM REQ ID for the previous msg,  
 *                    pSnmpTgtAddrEntry - pointer to Target addr entry
 *  Output          : None 
 *  Returns         : SNMP_SUCCESS, SNMP_FAILURE 
 ************************************************************************/

INT4
SNMPStartProxyRequestTimer (tSNMP_PROXY_CACHE * pCacheData,
                            tSnmpTgtAddrEntry * pSnmpTgtAddrEntry)
{
    tTmrAppTimer       *pTimerData = NULL;
    INT4                i4Timeout = SNMP_ZERO;

    if ((pCacheData == NULL) || (pSnmpTgtAddrEntry == NULL))
    {
        SNMPTrace ("Cache data or pSnmpTgtAddrEntry is NULL \n");
        return SNMP_FAILURE;
    }

    i4Timeout = (INT4) ((pSnmpTgtAddrEntry->i4TimeOut) / 100);

    pTimerData = &pCacheData->ProxyCmnCache.CacheTmr;
    pTimerData->u4Data = (FS_ULONG) pCacheData;

    if (SNMP_FAILURE == SnmpStartTimer (SnmpPxyTimerListId,
                                        pTimerData, (UINT4) i4Timeout))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : SnmpProxyProcessTimerExpiry 
 *  Description     : This function handles proxy request timer expiry. 
 *  Output          : None 
 *  Returns         : VOID 
 ************************************************************************/
VOID
SnmpProxyProcessTimerExpiry ()
{
    tTmrAppTimer       *pTimer = NULL;
    tSNMP_PROXY_CACHE  *pCacheNode = NULL;
    SNMPTrace ("[PROXY]:Proxy timer Expired \n");

    while ((pTimer = TmrGetNextExpiredTimer (SnmpPxyTimerListId)) != NULL)
    {
        pCacheNode = (tSNMP_PROXY_CACHE *) pTimer->u4Data;
        if (pCacheNode)
        {
            SNMPProxyCacheDeleteNode (pCacheNode->ProxyCmnCache.
                                      u4_NewRequestID);
        }
    }

    return;
}
