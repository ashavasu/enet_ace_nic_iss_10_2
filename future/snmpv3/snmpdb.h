
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: snmpdb.h,v 1.6 2015/04/28 12:36:57 siva Exp $
 *
 * Description: macros and prototypes for db access module
 *******************************************************************/
#ifndef _SNMPDB_H
#define _SNMPDB_H

#define NO_INDEX  2
#define NO_INDEX_LEN_MATCH 3 

/* Max allowd Integer */
#define SNMP_MAX_INT 0x7fffffff


#endif /* _SNMPDB_H */

