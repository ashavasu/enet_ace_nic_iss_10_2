/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: snmcom.c,v 1.14 2016/03/18 13:04:29 siva Exp $
 *
 * Description:Routines for Snmp agent community mapping table 
 *******************************************************************/

#include "snmpcmn.h"
#include "snmcom.h"

/*********************************************************************
*  Function Name : SnmpInitCommunityTable
*  Description   : Function Init Community Mapping Table 
*  Parameter(s)  : None
*  Return Values : None
*********************************************************************/

INT4
SnmpInitCommunityTable ()
{
    UINT4               u4Index = SNMP_ZERO;
    MEMSET (gaCommunityEntry, SNMP_ZERO, sizeof (gaCommunityEntry));
    MEMSET (au1CommIndex, SNMP_ZERO, sizeof (au1CommIndex));
    MEMSET (au1CommName, SNMP_ZERO, sizeof (au1CommName));
    MEMSET (au1ComSecName, SNMP_ZERO, sizeof (au1ComSecName));
    MEMSET (au1ContEngine, SNMP_ZERO, sizeof (au1ContEngine));
    MEMSET (au1ContName, SNMP_ZERO, sizeof (au1ContName));
    MEMSET (au1TransTag, SNMP_ZERO, sizeof (au1TransTag));
    TMO_SLL_Init (&gSnmpCommunityEntry);
    for (u4Index = SNMP_ZERO; u4Index < MAX_COMMUNITY_TABLE_ENTRY; u4Index++)
    {
        gaCommunityEntry[u4Index].CommIndex.pu1_OctetList =
            au1CommIndex[u4Index];
        gaCommunityEntry[u4Index].CommunityName.pu1_OctetList =
            au1CommName[u4Index];
        gaCommunityEntry[u4Index].SecurityName.pu1_OctetList =
            au1ComSecName[u4Index];
        gaCommunityEntry[u4Index].ContextEngineID.pu1_OctetList =
            au1ContEngine[u4Index];
        gaCommunityEntry[u4Index].ContextName.pu1_OctetList =
            au1ContName[u4Index];
        gaCommunityEntry[u4Index].TransTag.pu1_OctetList = au1TransTag[u4Index];
        gaCommunityEntry[u4Index].i4Status = SNMP_INACTIVE;
    }
    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : SNMPAddCommunityEntry 
*  Description   : Function to Add the Community Entry from the table
*  Parameter(s)  : pCommunityIndex - Comunity Index 
*  Return Values : SNMP_SUCCESS/SNMP_FAILURE
*********************************************************************/

INT4
SNMPAddCommunityEntry (tSNMP_OCTET_STRING_TYPE * pCommunityIndex)
{
    UINT4               u4Index = SNMP_ZERO;
    tSNMP_OCTET_STRING_TYPE ContextName;

    if (SNMPGetCommunityEntry (pCommunityIndex) != NULL)
    {
        return SNMP_FAILURE;
    }

    for (u4Index = SNMP_ZERO; u4Index < MAX_COMMUNITY_TABLE_ENTRY; u4Index++)
    {
        if (gaCommunityEntry[u4Index].i4Status == SNMP_INACTIVE)
        {
            gaCommunityEntry[u4Index].i4Status = UNDER_CREATION;
            gaCommunityEntry[u4Index].i4Storage =
                SNMP3_STORAGE_TYPE_NONVOLATILE;
            SNMPCopyOctetString (&(gaCommunityEntry[u4Index].CommIndex),
                                 pCommunityIndex);
            ContextName.pu1_OctetList = (UINT1 *) "";
            ContextName.i4_Length = 0;
            SNMPCopyOctetString (&(gaCommunityEntry[u4Index].ContextName),
                                 &ContextName);
            gaCommunityEntry[u4Index].TransTag.i4_Length = 0;
            SNMPCommunityEntryAddSll (&(gaCommunityEntry[u4Index].link));
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/*********************************************************************
*  Function Name : SNMPCommunityEntryAddSll 
*  Description   : Function to add the Community entry to the list
*  Parameter(s)  : pNode - Node to be added 
*  Return Values : None
*********************************************************************/

VOID
SNMPCommunityEntryAddSll (tTMO_SLL_NODE * pNode)
{
    tCommunityMappingEntry *pCurEntry = NULL;
    tCommunityMappingEntry *pInEntry = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pPrevNode = NULL;
    pInEntry = (tCommunityMappingEntry *) pNode;

    TMO_SLL_Scan (&gSnmpCommunityEntry, pLstNode, tTMO_SLL_NODE *)
    {
        pCurEntry = (tCommunityMappingEntry *) pLstNode;

        if (SNMPCompareImpliedOctetString (&(pInEntry->CommIndex),
                                           &(pCurEntry->CommIndex)) ==
            SNMP_LESSER)
        {
            break;
        }
        pPrevNode = pLstNode;
    }
    TMO_SLL_Insert (&gSnmpCommunityEntry, pPrevNode, pNode);
}

/*********************************************************************
*  Function Name : SNMPGetCommunityEntry 
*  Description   : Function to get the corresponding entry form Community Table
*  Parameter(s)  : pCommunityIndex - Community Index 
*  Return Values : On Success to return the Entry
*                  on Failure to return NULL
*********************************************************************/

tCommunityMappingEntry *
SNMPGetCommunityEntry (tSNMP_OCTET_STRING_TYPE * pCommunityIndex)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tCommunityMappingEntry *pCommunity = NULL;

    TMO_SLL_Scan (&gSnmpCommunityEntry, pListNode, tTMO_SLL_NODE *)
    {
        pCommunity = (tCommunityMappingEntry *) pListNode;
        if (pCommunity->i4Status != SNMP_INACTIVE)
        {
            if (SNMPCompareExactMatchOctetString
                (pCommunityIndex, &(pCommunity->CommIndex)) == SNMP_EQUAL)
            {
                return pCommunity;
            }
        }
    }
    return NULL;
}

/*********************************************************************
*  Function Name : SNMPGetCommunityEntryFromName 
*  Description   : Function to get the corresponding entry form Community Table
*  Parameter(s)  : pCommunityName - Community Name
*  Return Values : On Success to return the Entry
*                  on Failure to return NULL
*********************************************************************/

tCommunityMappingEntry *
SNMPGetCommunityEntryFromName (tSNMP_OCTET_STRING_TYPE * pCommunityName)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tCommunityMappingEntry *pCommunity = NULL;

    TMO_SLL_Scan (&gSnmpCommunityEntry, pListNode, tTMO_SLL_NODE *)
    {
        pCommunity = (tCommunityMappingEntry *) pListNode;
        if (pCommunity->i4Status != SNMP_INACTIVE)
        {
            if (SNMPCompareExactMatchOctetString
                (pCommunityName, &(pCommunity->CommunityName)) == SNMP_EQUAL)
            {
                return pCommunity;
            }
        }
    }
    return NULL;
}

/*********************************************************************
*  Function Name : SNMPGetFirstCommunity 
*  Description   : Function to get the First Entry from the Community Table 
*  Parameter(s)  : None 
*  Return Values : Community Entry 
*********************************************************************/

tCommunityMappingEntry *
SNMPGetFirstCommunity ()
{
    tCommunityMappingEntry *pCommunityEntry = NULL;
    pCommunityEntry =
        (tCommunityMappingEntry *) TMO_SLL_First (&gSnmpCommunityEntry);
    return pCommunityEntry;
}

/*********************************************************************
*  Function Name : SNMPGetNextCommunityEntry 
*  Description   : Function to get the next entry form Community Table
*  Parameter(s)  : pCommunityIndex - Community Index
*  Return Values : Success will return the Community Entry
*                  Failure will return NULL
*********************************************************************/
tCommunityMappingEntry *
SNMPGetNextCommunityEntry (tSNMP_OCTET_STRING_TYPE * pCommunityIndex)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tCommunityMappingEntry *pCommunity = NULL;

    TMO_SLL_Scan (&gSnmpCommunityEntry, pListNode, tTMO_SLL_NODE *)
    {
        pCommunity = (tCommunityMappingEntry *) pListNode;
        if (pCommunity->i4Status != SNMP_INACTIVE)
        {
            if (SNMPCompareImpliedOctetString
                (pCommunityIndex, &(pCommunity->CommIndex)) == SNMP_LESSER)
            {
                return pCommunity;
            }
        }
    }
    return NULL;
}

/*********************************************************************
*  Function Name : SNMPDeleteCommunityEntry 
*  Description   : Function to delete the Community entry 
*  Parameter(s)  : pCommunityIndex - Cimmunity Index
*  Return Values : SNMP_SUCCESS/SNMP_FAILURE 
*********************************************************************/

INT4
SNMPDeleteCommunityEntry (tSNMP_OCTET_STRING_TYPE * pCommunityIndex)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tCommunityMappingEntry *pCommunityEntry = NULL;

    TMO_SLL_Scan (&gSnmpCommunityEntry, pListNode, tTMO_SLL_NODE *)
    {
        pCommunityEntry = (tCommunityMappingEntry *) pListNode;
        if (SNMPCompareOctetString (pCommunityIndex,
                                    &(pCommunityEntry->CommIndex)) ==
            SNMP_EQUAL)
        {
            pCommunityEntry->i4Status = SNMP_INACTIVE;
            SNMPDelCommunityEntrySll (&(pCommunityEntry->link));
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/*********************************************************************
*  Function Name : SNMPDelCommunityEntrySll 
*  Description   : Function to delete an entry from the list 
*  Parameter(s)  : pNode - Node to be deleted
*  Return Values : None
*********************************************************************/

VOID
SNMPDelCommunityEntrySll (tTMO_SLL_NODE * pNode)
{
    TMO_SLL_Delete (&gSnmpCommunityEntry, pNode);
    return;
}

/*********************************************************************
*  Function Name : SNMPGetVacmInfoFromCommMappingTable 
*  Description   : Function to return the community name from Vacm Table
*  Parameter(s)  : pCommName   - Community Name
*                  u4Version   - Snmp Version 
*                  pVacmInfo   - VACM Information
*  Return Values : SNMP_SUCCESS/SNMP_FAILURE 
*********************************************************************/

INT4
SNMPGetVacmInfoFromCommMappingTable (tSNMP_OCTET_STRING_TYPE * pCommName,
                                     UINT4 u4Version, tSnmpVacmInfo * pVacmInfo)
{
    tCommunityMappingEntry *pCommunityEntry = NULL;
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    UINT4               u4TgtAddr = 0;

    if (pCommName->i4_Length == SNMP_ZERO)
    {
        SNMPTrace ("Invalid Community Name Given\n");
        return SNMP_FAILURE;
    }

    pCommunityEntry = SNMPGetCommunityEntryFromName (pCommName);
    if (pCommunityEntry == NULL)
    {
        SNMPTrace ("No Matching in the community Mapping Table\n");
        return SNMP_FAILURE;
    }
    pVacmInfo->pUserName = &(pCommunityEntry->SecurityName);
    pVacmInfo->pContextName = &(pCommunityEntry->ContextName);
    if (pCommunityEntry->TransTag.i4_Length == SNMP_ZERO)
    {
        if (u4Version == VERSION1)
        {
            pVacmInfo->u4SecModel = SNMP_SECMODEL_V1;
        }
        else
        {
            pVacmInfo->u4SecModel = SNMP_SECMODEL_V2C;
        }
        pVacmInfo->u4SecLevel = SNMP_NOAUTH_NOPRIV;
    }
    else
    {
        pSnmpTgtAddrEntry = SNMPGetTgtAddrTagEntry (&pCommunityEntry->TransTag,
                                                    NULL);
        if (pSnmpTgtAddrEntry == NULL)
        {
            SNMPTrace ("No TagEntry in the Target Address Table\n");
            return SNMP_FAILURE;
        }

        u4TgtAddr =
            OSIX_NTOHL (*(UINT4 *) (VOID *)
                        (pSnmpTgtAddrEntry->Address.pu1_OctetList));

        if (u4TgtAddr != SNMPGetManagerAddr ())
        {
            SNMPTrace ("Target Address not matching the Entry in"
                       "SNMP Target Address Table\n");
            return SNMP_FAILURE;
        }

        pSnmpTgtParamEntry = SNMPGetTgtParamEntry
            (&pSnmpTgtAddrEntry->AddrParams);
        if (pSnmpTgtParamEntry == NULL)
        {
            SNMPTrace ("No Entry in the Target Param Table\n");
            return SNMP_FAILURE;
        }
        pVacmInfo->u4SecModel = (UINT4) pSnmpTgtParamEntry->i2ParamSecModel;
        pVacmInfo->u4SecLevel = (UINT4) pSnmpTgtParamEntry->i4ParamSecLevel;
    }
    return SNMP_SUCCESS;
}

/*********************************************************************
 *  Function Name : SNMPGetCommunityEntryFromSecNameContextID
 *  Description   : This function returns the community entry using
 *                  security name and context id.
 *  Parameter(s)  : pSecurityName - Security name for matching entry 
 *                  in the community table.
 *  Return Values : Community Entry or NULL.
 *********************************************************************/
tCommunityMappingEntry *
SNMPGetCommunityEntryFromSecNameContextID (tSNMP_OCTET_STRING_TYPE *
                                           pSecurityName,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pSnmpEngineID)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tCommunityMappingEntry *pCommunity = NULL;

    TMO_SLL_Scan (&gSnmpCommunityEntry, pListNode, tTMO_SLL_NODE *)
    {
        pCommunity = (tCommunityMappingEntry *) pListNode;
        if (pCommunity->i4Status != SNMP_INACTIVE)
        {
            if ((SNMPCompareExactMatchOctetString
                 (pSecurityName, &(pCommunity->SecurityName)) == SNMP_EQUAL)
                &&
                (SNMPCompareOctetString (pSnmpEngineID,
                                         &(pCommunity->ContextEngineID)) ==
                 SNMP_EQUAL))
            {
                return pCommunity;
            }
        }
    }
    return NULL;
}

/*********************************************************************
 *  Function Name : SNMPGetCommunityEntryFromSecName
 *  Description   : This function returns the community entry using
 *                  security name.
 *  Parameter(s)  : pSecurityName - Security name for matching entry 
 *                  in the community table.
 *  Return Values : Community Entry or NULL.
 *********************************************************************/
tCommunityMappingEntry *
SNMPGetCommunityEntryFromSecName (tSNMP_OCTET_STRING_TYPE * pSecurityName)
{
    tCommunityMappingEntry *pCommunity = NULL;
    tTMO_SLL_NODE      *pListNode = NULL;

    TMO_SLL_Scan (&gSnmpCommunityEntry, pListNode, tTMO_SLL_NODE *)
    {
        pCommunity = (tCommunityMappingEntry *) pListNode;

        if (pCommunity->i4Status != SNMP_INACTIVE)
        {
            if (SNMPCompareExactMatchOctetString
                (pSecurityName, &(pCommunity->SecurityName)) == SNMP_EQUAL)
            {
                return pCommunity;
            }
        }
    }
    return NULL;
}

/********************  END OF FILE   ***************************************/
