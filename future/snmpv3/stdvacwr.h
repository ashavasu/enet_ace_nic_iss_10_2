/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: stdvacwr.h,v 1.4 2015/04/28 12:35:03 siva Exp $
 *
 * Description: Prototypes for VACM MIB wrapper routines 
 *******************************************************************/
#ifndef _STDVACWR_H
#define _STDVACWR_H
INT4 GetNextIndexVacmContextTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterSTDVAC(VOID);
INT4 VacmContextNameGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexVacmSecurityToGroupTable(tSnmpIndex *, tSnmpIndex *);
INT4 VacmSecurityModelGet(tSnmpIndex *, tRetVal *);
INT4 VacmSecurityNameGet(tSnmpIndex *, tRetVal *);
INT4 VacmGroupNameGet(tSnmpIndex *, tRetVal *);
INT4 VacmSecurityToGroupStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 VacmSecurityToGroupStatusGet(tSnmpIndex *, tRetVal *);
INT4 VacmGroupNameSet(tSnmpIndex *, tRetVal *);
INT4 VacmSecurityToGroupStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 VacmSecurityToGroupStatusSet(tSnmpIndex *, tRetVal *);
INT4 VacmGroupNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 VacmSecurityToGroupStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 VacmSecurityToGroupStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 VacmSecurityToGroupTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 GetNextIndexVacmAccessTable(tSnmpIndex *, tSnmpIndex *);
INT4 VacmAccessContextPrefixGet(tSnmpIndex *, tRetVal *);
INT4 VacmAccessSecurityModelGet(tSnmpIndex *, tRetVal *);
INT4 VacmAccessSecurityLevelGet(tSnmpIndex *, tRetVal *);
INT4 VacmAccessContextMatchGet(tSnmpIndex *, tRetVal *);
INT4 VacmAccessReadViewNameGet(tSnmpIndex *, tRetVal *);
INT4 VacmAccessWriteViewNameGet(tSnmpIndex *, tRetVal *);
INT4 VacmAccessNotifyViewNameGet(tSnmpIndex *, tRetVal *);
INT4 VacmAccessStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 VacmAccessStatusGet(tSnmpIndex *, tRetVal *);
INT4 VacmAccessContextMatchSet(tSnmpIndex *, tRetVal *);
INT4 VacmAccessReadViewNameSet(tSnmpIndex *, tRetVal *);
INT4 VacmAccessWriteViewNameSet(tSnmpIndex *, tRetVal *);
INT4 VacmAccessNotifyViewNameSet(tSnmpIndex *, tRetVal *);
INT4 VacmAccessStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 VacmAccessStatusSet(tSnmpIndex *, tRetVal *);
INT4 VacmAccessContextMatchTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 VacmAccessReadViewNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 VacmAccessWriteViewNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 VacmAccessNotifyViewNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 VacmAccessStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 VacmAccessStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 VacmAccessTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);






INT4 VacmViewSpinLockGet(tSnmpIndex *, tRetVal *);
INT4 VacmViewSpinLockSet(tSnmpIndex *, tRetVal *);
INT4 VacmViewSpinLockTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 VacmViewSpinLockDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

INT4 GetNextIndexVacmViewTreeFamilyTable(tSnmpIndex *, tSnmpIndex *);
INT4 VacmViewTreeFamilyViewNameGet(tSnmpIndex *, tRetVal *);
INT4 VacmViewTreeFamilySubtreeGet(tSnmpIndex *, tRetVal *);
INT4 VacmViewTreeFamilyMaskGet(tSnmpIndex *, tRetVal *);
INT4 VacmViewTreeFamilyTypeGet(tSnmpIndex *, tRetVal *);
INT4 VacmViewTreeFamilyStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 VacmViewTreeFamilyStatusGet(tSnmpIndex *, tRetVal *);
INT4 VacmViewTreeFamilyMaskSet(tSnmpIndex *, tRetVal *);
INT4 VacmViewTreeFamilyTypeSet(tSnmpIndex *, tRetVal *);
INT4 VacmViewTreeFamilyStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 VacmViewTreeFamilyStatusSet(tSnmpIndex *, tRetVal *);
INT4 VacmViewTreeFamilyMaskTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 VacmViewTreeFamilyTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 VacmViewTreeFamilyStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 VacmViewTreeFamilyStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 VacmViewTreeFamilyTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);




#endif /* _STDVACWR_H */
