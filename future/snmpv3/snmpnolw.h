/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: snmpnolw.h,v 1.5 2015/04/28 12:35:02 siva Exp $
*
* Description: Proto types for snmp notify table Low Level  Routines
*********************************************************************/
#ifndef _SNMPNOLW_H
#define _SNMPNOLW_H
/* Proto Validate Index Instance for SnmpNotifyTable. */
INT1
nmhValidateIndexInstanceSnmpNotifyTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for SnmpNotifyTable  */

INT1
nmhGetFirstIndexSnmpNotifyTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexSnmpNotifyTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetSnmpNotifyTag ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetSnmpNotifyType ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetSnmpNotifyStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetSnmpNotifyRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetSnmpNotifyTag ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetSnmpNotifyType ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetSnmpNotifyStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetSnmpNotifyRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2SnmpNotifyTag ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2SnmpNotifyType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2SnmpNotifyStorageType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2SnmpNotifyRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2SnmpNotifyTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for SnmpNotifyFilterProfileTable. */
INT1
nmhValidateIndexInstanceSnmpNotifyFilterProfileTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for SnmpNotifyFilterProfileTable  */

INT1
nmhGetFirstIndexSnmpNotifyFilterProfileTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexSnmpNotifyFilterProfileTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetSnmpNotifyFilterProfileName ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetSnmpNotifyFilterProfileStorType ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetSnmpNotifyFilterProfileRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetSnmpNotifyFilterProfileName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetSnmpNotifyFilterProfileStorType ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetSnmpNotifyFilterProfileRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2SnmpNotifyFilterProfileName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2SnmpNotifyFilterProfileStorType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2SnmpNotifyFilterProfileRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2SnmpNotifyFilterProfileTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for SnmpNotifyFilterTable. */
INT1
nmhValidateIndexInstanceSnmpNotifyFilterTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OID_TYPE *));

/* Proto Type for Low Level GET FIRST fn for SnmpNotifyFilterTable  */

INT1
nmhGetFirstIndexSnmpNotifyFilterTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , tSNMP_OID_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexSnmpNotifyFilterTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OID_TYPE *, tSNMP_OID_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetSnmpNotifyFilterMask ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OID_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetSnmpNotifyFilterType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OID_TYPE *,INT4 *));

INT1
nmhGetSnmpNotifyFilterStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OID_TYPE *,INT4 *));

INT1
nmhGetSnmpNotifyFilterRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OID_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetSnmpNotifyFilterMask ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OID_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetSnmpNotifyFilterType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OID_TYPE * ,INT4 ));

INT1
nmhSetSnmpNotifyFilterStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OID_TYPE * ,INT4 ));

INT1
nmhSetSnmpNotifyFilterRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OID_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2SnmpNotifyFilterMask ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OID_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2SnmpNotifyFilterType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OID_TYPE * ,INT4 ));

INT1
nmhTestv2SnmpNotifyFilterStorageType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OID_TYPE * ,INT4 ));

INT1
nmhTestv2SnmpNotifyFilterRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OID_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2SnmpNotifyFilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
#endif
