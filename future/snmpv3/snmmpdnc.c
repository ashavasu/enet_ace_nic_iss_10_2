
/* $Id: snmmpdnc.c,v 1.1 2015/12/29 11:53:18 siva Exp $
    ISS Wrapper module
    module SNMP-FRAMEWORK-MIB

 */

# include  "snmpcmn.h"
# include  "snmmpdlw.h"
# include  "snmmpdnc.h"

/********************************************************************
* FUNCTION NcSnmpEngineIDGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcSnmpEngineIDGet (UINT1 *psnmpEngineID )
{
    tSNMP_OCTET_STRING_TYPE v_snmpEngineID ;

    memset (&v_snmpEngineID, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
	v_snmpEngineID.pu1_OctetList = psnmpEngineID;

    return (nmhGetSnmpEngineID (
						&v_snmpEngineID));

} /* NcSnmpEngineIDGet */

/********************************************************************
* FUNCTION NcSnmpEngineBootsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcSnmpEngineBootsGet (
                INT4 *pi4snmpEngineBoots )
{
    return(nmhGetSnmpEngineBoots(
                 pi4snmpEngineBoots ));

} /* NcSnmpEngineBootsGet */

/********************************************************************
* FUNCTION NcSnmpEngineTimeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcSnmpEngineTimeGet (
                INT4 *pi4snmpEngineTime )
{

    return(nmhGetSnmpEngineTime(
                 pi4snmpEngineTime ));

} /* NcSnmpEngineTimeGet */

/********************************************************************
* FUNCTION NcSnmpEngineMaxMessageSizeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
INT1 NcSnmpEngineMaxMessageSizeGet (
                INT4 *pi4snmpEngineMaxMessageSize )
{
    return(nmhGetSnmpEngineMaxMessageSize(
                 pi4snmpEngineMaxMessageSize ));

} /* NcSnmpEngineMaxMessageSizeGet */

/* END i_SNMP_FRAMEWORK_MIB.c */
