
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: snmagent.h,v 1.5 2015/04/28 12:35:02 siva Exp $
 *
 * Description:macros and prototypes for snmagent.c
 *******************************************************************/
#ifndef _SNMAGENT_H
#define _SNMAGENT_H

#define SNMP_PDU_GET_RESPONSE_MASK  0x0000ffff 
#define SNMP_ENCODE_LENGTH   100 
INT4 SNMPGetProcess(tSNMP_NORMAL_PDU *, tSnmpVacmInfo *);
INT4 SNMPGetNextProcess(tSNMP_NORMAL_PDU *, tSnmpVacmInfo *);
INT4 SNMPSetProcess(tSNMP_NORMAL_PDU *pPdu, tSnmpVacmInfo *);
INT4 SNMPGetNextVarBind(tSNMP_VAR_BIND *,UINT4 *,tSnmpVacmInfo *);
INT4 SNMPGetBulkProcess(tSNMP_NORMAL_PDU *pPdu,tSnmpVacmInfo *);
INT4 SNMPGetBindCount (tSNMP_VAR_BIND * pVarPtr);
void freeAllRBMemory(tSNMP_GET_VAR_BIND *pGetVarBind,
					 INT4 numVarbinds);

#endif /* _SNMAGENT_H */
