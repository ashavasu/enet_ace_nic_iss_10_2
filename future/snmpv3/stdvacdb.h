/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdvacdb.h,v 1.4 2015/04/28 12:35:03 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDVACDB_H
#define _STDVACDB_H

UINT1 VacmContextTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 VacmSecurityToGroupTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 VacmAccessTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 VacmViewTreeFamilyTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OBJECT_ID};

UINT4 stdvac [] ={1,3,6,1,6,3,16};
tSNMP_OID_TYPE stdvacOID = {7, stdvac};


UINT4 VacmContextName [ ] ={1,3,6,1,6,3,16,1,1,1,1};
UINT4 VacmSecurityModel [ ] ={1,3,6,1,6,3,16,1,2,1,1};
UINT4 VacmSecurityName [ ] ={1,3,6,1,6,3,16,1,2,1,2};
UINT4 VacmGroupName [ ] ={1,3,6,1,6,3,16,1,2,1,3};
UINT4 VacmSecurityToGroupStorageType [ ] ={1,3,6,1,6,3,16,1,2,1,4};
UINT4 VacmSecurityToGroupStatus [ ] ={1,3,6,1,6,3,16,1,2,1,5};
UINT4 VacmAccessContextPrefix [ ] ={1,3,6,1,6,3,16,1,4,1,1};
UINT4 VacmAccessSecurityModel [ ] ={1,3,6,1,6,3,16,1,4,1,2};
UINT4 VacmAccessSecurityLevel [ ] ={1,3,6,1,6,3,16,1,4,1,3};
UINT4 VacmAccessContextMatch [ ] ={1,3,6,1,6,3,16,1,4,1,4};
UINT4 VacmAccessReadViewName [ ] ={1,3,6,1,6,3,16,1,4,1,5};
UINT4 VacmAccessWriteViewName [ ] ={1,3,6,1,6,3,16,1,4,1,6};
UINT4 VacmAccessNotifyViewName [ ] ={1,3,6,1,6,3,16,1,4,1,7};
UINT4 VacmAccessStorageType [ ] ={1,3,6,1,6,3,16,1,4,1,8};
UINT4 VacmAccessStatus [ ] ={1,3,6,1,6,3,16,1,4,1,9};
UINT4 VacmViewSpinLock [ ] ={1,3,6,1,6,3,16,1,5,1};
UINT4 VacmViewTreeFamilyViewName [ ] ={1,3,6,1,6,3,16,1,5,2,1,1};
UINT4 VacmViewTreeFamilySubtree [ ] ={1,3,6,1,6,3,16,1,5,2,1,2};
UINT4 VacmViewTreeFamilyMask [ ] ={1,3,6,1,6,3,16,1,5,2,1,3};
UINT4 VacmViewTreeFamilyType [ ] ={1,3,6,1,6,3,16,1,5,2,1,4};
UINT4 VacmViewTreeFamilyStorageType [ ] ={1,3,6,1,6,3,16,1,5,2,1,5};
UINT4 VacmViewTreeFamilyStatus [ ] ={1,3,6,1,6,3,16,1,5,2,1,6};


tMbDbEntry stdvacMibEntry[]= {

{{11,VacmContextName}, GetNextIndexVacmContextTable, VacmContextNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, VacmContextTableINDEX, 1, 0, 0, NULL},

{{11,VacmSecurityModel}, GetNextIndexVacmSecurityToGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, VacmSecurityToGroupTableINDEX, 2, 0, 0, NULL},

{{11,VacmSecurityName}, GetNextIndexVacmSecurityToGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, VacmSecurityToGroupTableINDEX, 2, 0, 0, NULL},

{{11,VacmGroupName}, GetNextIndexVacmSecurityToGroupTable, VacmGroupNameGet, VacmGroupNameSet, VacmGroupNameTest, VacmSecurityToGroupTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, VacmSecurityToGroupTableINDEX, 2, 0, 0, NULL},

{{11,VacmSecurityToGroupStorageType}, GetNextIndexVacmSecurityToGroupTable, VacmSecurityToGroupStorageTypeGet, VacmSecurityToGroupStorageTypeSet, VacmSecurityToGroupStorageTypeTest, VacmSecurityToGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, VacmSecurityToGroupTableINDEX, 2, 0, 0, "3"},

{{11,VacmSecurityToGroupStatus}, GetNextIndexVacmSecurityToGroupTable, VacmSecurityToGroupStatusGet, VacmSecurityToGroupStatusSet, VacmSecurityToGroupStatusTest, VacmSecurityToGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, VacmSecurityToGroupTableINDEX, 2, 0, 1, NULL},

{{11,VacmAccessContextPrefix}, GetNextIndexVacmAccessTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, VacmAccessTableINDEX, 4, 0, 0, NULL},

{{11,VacmAccessSecurityModel}, GetNextIndexVacmAccessTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, VacmAccessTableINDEX, 4, 0, 0, NULL},

{{11,VacmAccessSecurityLevel}, GetNextIndexVacmAccessTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, VacmAccessTableINDEX, 4, 0, 0, NULL},

{{11,VacmAccessContextMatch}, GetNextIndexVacmAccessTable, VacmAccessContextMatchGet, VacmAccessContextMatchSet, VacmAccessContextMatchTest, VacmAccessTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, VacmAccessTableINDEX, 4, 0, 0, "1"},

{{11,VacmAccessReadViewName}, GetNextIndexVacmAccessTable, VacmAccessReadViewNameGet, VacmAccessReadViewNameSet, VacmAccessReadViewNameTest, VacmAccessTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, VacmAccessTableINDEX, 4, 0, 0, "0"},

{{11,VacmAccessWriteViewName}, GetNextIndexVacmAccessTable, VacmAccessWriteViewNameGet, VacmAccessWriteViewNameSet, VacmAccessWriteViewNameTest, VacmAccessTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, VacmAccessTableINDEX, 4, 0, 0, "0"},

{{11,VacmAccessNotifyViewName}, GetNextIndexVacmAccessTable, VacmAccessNotifyViewNameGet, VacmAccessNotifyViewNameSet, VacmAccessNotifyViewNameTest, VacmAccessTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, VacmAccessTableINDEX, 4, 0, 0, "0"},

{{11,VacmAccessStorageType}, GetNextIndexVacmAccessTable, VacmAccessStorageTypeGet, VacmAccessStorageTypeSet, VacmAccessStorageTypeTest, VacmAccessTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, VacmAccessTableINDEX, 4, 0, 0, "3"},

{{11,VacmAccessStatus}, GetNextIndexVacmAccessTable, VacmAccessStatusGet, VacmAccessStatusSet, VacmAccessStatusTest, VacmAccessTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, VacmAccessTableINDEX, 4, 0, 1, NULL},

{{10,VacmViewSpinLock}, NULL, VacmViewSpinLockGet, VacmViewSpinLockSet, VacmViewSpinLockTest, VacmViewSpinLockDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,VacmViewTreeFamilyViewName}, GetNextIndexVacmViewTreeFamilyTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, VacmViewTreeFamilyTableINDEX, 2, 0, 0, NULL},

{{12,VacmViewTreeFamilySubtree}, GetNextIndexVacmViewTreeFamilyTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_NOACCESS, VacmViewTreeFamilyTableINDEX, 2, 0, 0, NULL},

{{12,VacmViewTreeFamilyMask}, GetNextIndexVacmViewTreeFamilyTable, VacmViewTreeFamilyMaskGet, VacmViewTreeFamilyMaskSet, VacmViewTreeFamilyMaskTest, VacmViewTreeFamilyTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, VacmViewTreeFamilyTableINDEX, 2, 0, 0, "0"},

{{12,VacmViewTreeFamilyType}, GetNextIndexVacmViewTreeFamilyTable, VacmViewTreeFamilyTypeGet, VacmViewTreeFamilyTypeSet, VacmViewTreeFamilyTypeTest, VacmViewTreeFamilyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, VacmViewTreeFamilyTableINDEX, 2, 0, 0, "1"},

{{12,VacmViewTreeFamilyStorageType}, GetNextIndexVacmViewTreeFamilyTable, VacmViewTreeFamilyStorageTypeGet, VacmViewTreeFamilyStorageTypeSet, VacmViewTreeFamilyStorageTypeTest, VacmViewTreeFamilyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, VacmViewTreeFamilyTableINDEX, 2, 0, 0, "3"},

{{12,VacmViewTreeFamilyStatus}, GetNextIndexVacmViewTreeFamilyTable, VacmViewTreeFamilyStatusGet, VacmViewTreeFamilyStatusSet, VacmViewTreeFamilyStatusTest, VacmViewTreeFamilyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, VacmViewTreeFamilyTableINDEX, 2, 0, 1, NULL},
};
tMibData stdvacEntry = { 22, stdvacMibEntry };
#endif /* _STDVACDB_H */

