/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdvaclw.c,v 1.14 2017/11/20 13:11:27 siva Exp $
*
* Description: VACM MIB Low Level Routines
*********************************************************************/
# include  "snmpcmn.h"
# include  "stdvaclw.h"

#define MAX_VACM_CONTEXT_NAMELEN  32

#define VACM_VIEW_TREE_TYPE_EXCLUDED 2
#define VACM_VIEW_TREE_TYPE_INCLUDED 1
#define SNMP_VACM_ACCESS_CMATCH_EXACT   1
#define SNMP_VACM_ACCESS_CMATCH_PREFIX  2

#define SNMP_VACM_MIN_STRING           0
#define SNMP_VACM_MAX_STRING           32

#define SNMP_VIEW_TREE_MASK_MAX_LENGTH 16

INT4                gi4VacmViewSpinLock = 0;
/* LOW LEVEL Routines for Table : VacmContextTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceVacmContextTable
 Input       :  The Indices
                VacmContextName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceVacmContextTable (tSNMP_OCTET_STRING_TYPE *
                                          pVacmContextName)
{
    if (VACMGetContext (pVacmContextName) != NULL)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexVacmContextTable
 Input       :  The Indices
                VacmContextName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexVacmContextTable (tSNMP_OCTET_STRING_TYPE * pVacmContextName)
{
    tContext           *pContext = NULL;
    pContext = VACMGetFirstContext ();
    if (pContext == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pVacmContextName, &(pContext->ContextName));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexVacmContextTable
 Input       :  The Indices
                VacmContextName
                nextVacmContextName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexVacmContextTable (tSNMP_OCTET_STRING_TYPE * pVacmContextName,
                                 tSNMP_OCTET_STRING_TYPE * pNextVacmContextName)
{
    tContext           *pContext = NULL;
    pContext = VACMGetNextContext (pVacmContextName);
    if (pContext == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pNextVacmContextName, &(pContext->ContextName));
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/* LOW LEVEL Routines for Table : VacmSecurityToGroupTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceVacmSecurityToGroupTable
 Input       :  The Indices
                VacmSecurityModel
                VacmSecurityName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceVacmSecurityToGroupTable (INT4 i4VacmSecurityModel,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pVacmSecurityName)
{
    tSecGrp            *pSecGrpEntry = NULL;

    pSecGrpEntry = VACMGetSecGrp (i4VacmSecurityModel, pVacmSecurityName);
    if (pSecGrpEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexVacmSecurityToGroupTable
 Input       :  The Indices
                VacmSecurityModel
                VacmSecurityName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexVacmSecurityToGroupTable (INT4 *pi4VacmSecurityModel,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pVacmSecurityName)
{
    tSecGrp            *pSecGrpEntry = NULL;
    pSecGrpEntry = VACMGetFirstSecGrp ();
    if (pSecGrpEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4VacmSecurityModel = pSecGrpEntry->i4SecModel;
    SNMPCopyOctetString (pVacmSecurityName, &(pSecGrpEntry->SecName));
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexVacmSecurityToGroupTable
 Input       :  The Indices
                VacmSecurityModel
                nextVacmSecurityModel
                VacmSecurityName
                nextVacmSecurityName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexVacmSecurityToGroupTable (INT4 i4VacmSecurityModel,
                                         INT4 *pi4NextVacmSecurityModel,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pVacmSecurityName,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pNextVacmSecurityName)
{
    tSecGrp            *pSecGrpEntry = NULL;
    pSecGrpEntry = VACMGetNextSecGrp (i4VacmSecurityModel, pVacmSecurityName);
    if (pSecGrpEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4NextVacmSecurityModel = pSecGrpEntry->i4SecModel;
    SNMPCopyOctetString (pNextVacmSecurityName, &(pSecGrpEntry->SecName));
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetVacmGroupName
 Input       :  The Indices
                VacmSecurityModel
                VacmSecurityName

                The Object 
                retValVacmGroupName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVacmGroupName (INT4 i4VacmSecurityModel,
                     tSNMP_OCTET_STRING_TYPE * pVacmSecurityName,
                     tSNMP_OCTET_STRING_TYPE * pRetValVacmGroupName)
{
    tSecGrp            *pSecGrpEntry = NULL;
    pSecGrpEntry = VACMGetSecGrp (i4VacmSecurityModel, pVacmSecurityName);
    if (pSecGrpEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pRetValVacmGroupName, &(pSecGrpEntry->Group));
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetVacmSecurityToGroupStorageType
 Input       :  The Indices
                VacmSecurityModel
                VacmSecurityName

                The Object 
                retValVacmSecurityToGroupStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVacmSecurityToGroupStorageType (INT4 i4VacmSecurityModel,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pVacmSecurityName,
                                      INT4
                                      *pi4RetValVacmSecurityToGroupStorageType)
{
    tSecGrp            *pSecGrpEntry = NULL;
    pSecGrpEntry = VACMGetSecGrp (i4VacmSecurityModel, pVacmSecurityName);
    if (pSecGrpEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValVacmSecurityToGroupStorageType = pSecGrpEntry->i4Storage;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetVacmSecurityToGroupStatus
 Input       :  The Indices
                VacmSecurityModel
                VacmSecurityName

                The Object 
                retValVacmSecurityToGroupStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVacmSecurityToGroupStatus (INT4 i4VacmSecurityModel,
                                 tSNMP_OCTET_STRING_TYPE * pVacmSecurityName,
                                 INT4 *pi4RetValVacmSecurityToGroupStatus)
{
    tSecGrp            *pSecGrpEntry = NULL;
    pSecGrpEntry = VACMGetSecGrp (i4VacmSecurityModel, pVacmSecurityName);
    if (pSecGrpEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pSecGrpEntry->i4Status == UNDER_CREATION)
    {
        *pi4RetValVacmSecurityToGroupStatus = SNMP_ROWSTATUS_NOTINSERVICE;
    }
    if (pSecGrpEntry->i4Status == SNMP_ACTIVE)
    {
        *pi4RetValVacmSecurityToGroupStatus = SNMP_ROWSTATUS_ACTIVE;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetVacmGroupName
 Input       :  The Indices
                VacmSecurityModel
                VacmSecurityName

                The Object 
                setValVacmGroupName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVacmGroupName (INT4 i4VacmSecurityModel,
                     tSNMP_OCTET_STRING_TYPE * pVacmSecurityName,
                     tSNMP_OCTET_STRING_TYPE * pSetValVacmGroupName)
{
    tSecGrp            *pSecGrpEntry = NULL;
    pSecGrpEntry = VACMGetSecGrp (i4VacmSecurityModel, pVacmSecurityName);
    if (pSecGrpEntry == NULL)
    {
        SNMP_TRC (SNMP_FAILURE_TRC, "nmhSetVacmGroupName: "
                  "NULL security group entry\r\n");
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (&(pSecGrpEntry->Group), pSetValVacmGroupName);
    SNMP_TRC2 (SNMP_DEBUG_TRC, "nmhSetVacmGroupName: "
               "Successfully setting the group name [%s] for the user [%s]\r\n",
               pSetValVacmGroupName->pu1_OctetList,
               pVacmSecurityName->pu1_OctetList);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetVacmSecurityToGroupStorageType
 Input       :  The Indices
                VacmSecurityModel
                VacmSecurityName

                The Object 
                setValVacmSecurityToGroupStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVacmSecurityToGroupStorageType (INT4 i4VacmSecurityModel,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pVacmSecurityName,
                                      INT4
                                      i4SetValVacmSecurityToGroupStorageType)
{
    tSecGrp            *pSecGrpEntry = NULL;
    pSecGrpEntry = VACMGetSecGrp (i4VacmSecurityModel, pVacmSecurityName);
    if (pSecGrpEntry == NULL)
    {
        SNMP_TRC (SNMP_FAILURE_TRC, "nmhSetVacmSecurityToGroupStorageType: "
                  "NULL security group entry\r\n");
        return SNMP_FAILURE;
    }
    pSecGrpEntry->i4Storage = i4SetValVacmSecurityToGroupStorageType;
    SNMP_TRC1 (SNMP_DEBUG_TRC, "nmhSetVacmSecurityToGroupStorageType: "
               "Successfully setting the storage type for the group [%s]\r\n",
               pVacmSecurityName->pu1_OctetList);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetVacmSecurityToGroupStatus
 Input       :  The Indices
                VacmSecurityModel
                VacmSecurityName

                The Object 
                setValVacmSecurityToGroupStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVacmSecurityToGroupStatus (INT4 i4VacmSecurityModel,
                                 tSNMP_OCTET_STRING_TYPE * pVacmSecurityName,
                                 INT4 i4SetValVacmSecurityToGroupStatus)
{

    switch (i4SetValVacmSecurityToGroupStatus)
    {

        case SNMP_ROWSTATUS_CREATEANDGO:
        case SNMP_ROWSTATUS_CREATEANDWAIT:
        {
            UINT4               u4Return;
            u4Return =
                (UINT4) VACMAddSecGrp (i4VacmSecurityModel, pVacmSecurityName);
            if (u4Return != SNMP_FAILURE)
            {
                return SNMP_SUCCESS;
            }
            else
            {
                return SNMP_FAILURE;
            }
        }

        case SNMP_ROWSTATUS_ACTIVE:
        {
            tSecGrp            *pSecGrpEntry = NULL;
            pSecGrpEntry =
                VACMGetSecGrp (i4VacmSecurityModel, pVacmSecurityName);
            if (pSecGrpEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            pSecGrpEntry->i4Status = SNMP_ROWSTATUS_ACTIVE;
            return SNMP_SUCCESS;
        }

        case SNMP_ROWSTATUS_DESTROY:
        {
            UINT4               u4Return;
            u4Return =
                (UINT4) VACMDeleteSecGrp (i4VacmSecurityModel,
                                          pVacmSecurityName);
            if (u4Return == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }
            else
            {
                return SNMP_SUCCESS;
            }

        }
        case SNMP_ROWSTATUS_NOTINSERVICE:
        {
            tSecGrp            *pSecGrpEntry = NULL;
            pSecGrpEntry =
                VACMGetSecGrp (i4VacmSecurityModel, pVacmSecurityName);
            if (pSecGrpEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            if (pSecGrpEntry->i4Status == SNMP_ROWSTATUS_ACTIVE)
            {
                pSecGrpEntry->i4Status = SNMP_ROWSTATUS_NOTINSERVICE;
                return SNMP_SUCCESS;
            }
        }
        default:
            break;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2VacmGroupName
 Input       :  The Indices
                VacmSecurityModel
                VacmSecurityName

                The Object 
                testValVacmGroupName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VacmGroupName (UINT4 *pu4ErrorCode, INT4 i4VacmSecurityModel,
                        tSNMP_OCTET_STRING_TYPE * pVacmSecurityName,
                        tSNMP_OCTET_STRING_TYPE * pTestValVacmGroupName)
{
    if (SnmpIsModeSecure () == SNMP_SUCCESS)
    {
        if ((i4VacmSecurityModel == SNMP_SECMODEL_V1) ||
            (i4VacmSecurityModel == SNMP_SECMODEL_V2C))
        {
            *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
            return SNMP_FAILURE;
        }
    }
    if (nmhValidateIndexInstanceVacmSecurityToGroupTable (i4VacmSecurityModel,
                                                          pVacmSecurityName) ==
        SNMP_SUCCESS)
    {
        if ((pTestValVacmGroupName->i4_Length < SNMP_VACM_MIN_STRING) ||
            (pTestValVacmGroupName->i4_Length > SNMP_VACM_MAX_STRING))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;

    }
}

/****************************************************************************
 Function    :  nmhTestv2VacmSecurityToGroupStorageType
 Input       :  The Indices
                VacmSecurityModel
                VacmSecurityName

                The Object 
                testValVacmSecurityToGroupStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VacmSecurityToGroupStorageType (UINT4 *pu4ErrorCode,
                                         INT4 i4VacmSecurityModel,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pVacmSecurityName,
                                         INT4
                                         i4TestValVacmSecurityToGroupStorageType)
{
    if (nmhValidateIndexInstanceVacmSecurityToGroupTable (i4VacmSecurityModel,
                                                          pVacmSecurityName) ==
        SNMP_SUCCESS)
    {

        if ((i4TestValVacmSecurityToGroupStorageType < SNMP3_STORAGE_TYPE_OTHER)
            || (i4TestValVacmSecurityToGroupStorageType >
                SNMP3_STORAGE_TYPE_READONLY))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        if ((i4TestValVacmSecurityToGroupStorageType !=
             SNMP3_STORAGE_TYPE_VOLATILE) &&
            (i4TestValVacmSecurityToGroupStorageType !=
             SNMP3_STORAGE_TYPE_NONVOLATILE))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        }
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;

    }
}

/****************************************************************************
 Function    :  nmhTestv2VacmSecurityToGroupStatus
 Input       :  The Indices
                VacmSecurityModel
                VacmSecurityName

                The Object 
                testValVacmSecurityToGroupStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VacmSecurityToGroupStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4VacmSecurityModel,
                                    tSNMP_OCTET_STRING_TYPE * pVacmSecurityName,
                                    INT4 i4TestValVacmSecurityToGroupStatus)
{
    tSecGrp            *pSecGrpEntry = NULL;
    if (i4TestValVacmSecurityToGroupStatus == SNMP_ROWSTATUS_NOTREADY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValVacmSecurityToGroupStatus < SNMP_ROWSTATUS_ACTIVE) ||
        (i4TestValVacmSecurityToGroupStatus > SNMP_ROWSTATUS_DESTROY))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pSecGrpEntry = VACMGetSecGrp (i4VacmSecurityModel, pVacmSecurityName);

    if ((pSecGrpEntry == NULL) && (i4TestValVacmSecurityToGroupStatus ==
                                   SNMP_ROWSTATUS_ACTIVE))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (nmhValidateIndexInstanceVacmSecurityToGroupTable
        (i4VacmSecurityModel, pVacmSecurityName) == SNMP_SUCCESS)
    {
        if (i4TestValVacmSecurityToGroupStatus == SNMP_ROWSTATUS_CREATEANDWAIT)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        }
        else
        {
            return SNMP_SUCCESS;
        }
    }
    else
    {
        if (i4TestValVacmSecurityToGroupStatus != SNMP_ROWSTATUS_CREATEANDWAIT)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        else
        {
            return SNMP_SUCCESS;
        }
    }

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2VacmSecurityToGroupTable
 Input       :  The Indices
                VacmSecurityModel
                VacmSecurityName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2VacmSecurityToGroupTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : VacmAccessTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceVacmAccessTable
 Input       :  The Indices
                VacmGroupName
                VacmAccessContextPrefix
                VacmAccessSecurityModel
                VacmAccessSecurityLevel
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceVacmAccessTable (tSNMP_OCTET_STRING_TYPE *
                                         pVacmGroupName,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pVacmAccessContextPrefix,
                                         INT4 i4VacmAccessSecurityModel,
                                         INT4 i4VacmAccessSecurityLevel)
{
    tAccessEntry       *pAccessEntry = NULL;
    pAccessEntry = VACMGetAccess (pVacmGroupName, pVacmAccessContextPrefix,
                                  i4VacmAccessSecurityModel,
                                  i4VacmAccessSecurityLevel);
    if (pAccessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexVacmAccessTable
 Input       :  The Indices
                VacmGroupName
                VacmAccessContextPrefix
                VacmAccessSecurityModel
                VacmAccessSecurityLevel
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexVacmAccessTable (tSNMP_OCTET_STRING_TYPE * pVacmGroupName,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pVacmAccessContextPrefix,
                                 INT4 *pi4VacmAccessSecurityModel,
                                 INT4 *pi4VacmAccessSecurityLevel)
{
    tAccessEntry       *pAccessEntry = NULL;
    pAccessEntry = VACMGetFirstAccess ();
    if (pAccessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pVacmGroupName, pAccessEntry->pGroup);
    SNMPCopyOctetString (pVacmAccessContextPrefix, &(pAccessEntry->Context));
    *pi4VacmAccessSecurityModel = pAccessEntry->i4SecModel;
    *pi4VacmAccessSecurityLevel = pAccessEntry->i4SecLevel;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexVacmAccessTable
 Input       :  The Indices
                VacmGroupName
                nextVacmGroupName
                VacmAccessContextPrefix
                nextVacmAccessContextPrefix
                VacmAccessSecurityModel
                nextVacmAccessSecurityModel
                VacmAccessSecurityLevel
                nextVacmAccessSecurityLevel
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexVacmAccessTable (tSNMP_OCTET_STRING_TYPE * pVacmGroupName,
                                tSNMP_OCTET_STRING_TYPE * pNextVacmGroupName,
                                tSNMP_OCTET_STRING_TYPE *
                                pVacmAccessContextPrefix,
                                tSNMP_OCTET_STRING_TYPE *
                                pNextVacmAccessContextPrefix,
                                INT4 i4VacmAccessSecurityModel,
                                INT4 *pi4NextVacmAccessSecurityModel,
                                INT4 i4VacmAccessSecurityLevel,
                                INT4 *pi4NextVacmAccessSecurityLevel)
{
    tAccessEntry       *pAccessEntry = NULL;
    pAccessEntry = VACMGetNextAccess (pVacmGroupName, pVacmAccessContextPrefix,
                                      i4VacmAccessSecurityModel,
                                      i4VacmAccessSecurityLevel);
    if (pAccessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pNextVacmGroupName, pAccessEntry->pGroup);
    SNMPCopyOctetString (pNextVacmAccessContextPrefix,
                         &(pAccessEntry->Context));
    *pi4NextVacmAccessSecurityModel = pAccessEntry->i4SecModel;
    *pi4NextVacmAccessSecurityLevel = pAccessEntry->i4SecLevel;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetVacmAccessContextMatch
 Input       :  The Indices
                VacmGroupName
                VacmAccessContextPrefix
                VacmAccessSecurityModel
                VacmAccessSecurityLevel

                The Object 
                retValVacmAccessContextMatch
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVacmAccessContextMatch (tSNMP_OCTET_STRING_TYPE * pVacmGroupName,
                              tSNMP_OCTET_STRING_TYPE *
                              pVacmAccessContextPrefix,
                              INT4 i4VacmAccessSecurityModel,
                              INT4 i4VacmAccessSecurityLevel,
                              INT4 *pi4RetValVacmAccessContextMatch)
{
    tAccessEntry       *pAccessEntry = NULL;
    pAccessEntry = VACMGetAccess (pVacmGroupName, pVacmAccessContextPrefix,
                                  i4VacmAccessSecurityModel,
                                  i4VacmAccessSecurityLevel);
    if (pAccessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValVacmAccessContextMatch = pAccessEntry->i4ContextMatch;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetVacmAccessReadViewName
 Input       :  The Indices
                VacmGroupName
                VacmAccessContextPrefix
                VacmAccessSecurityModel
                VacmAccessSecurityLevel

                The Object 
                retValVacmAccessReadViewName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVacmAccessReadViewName (tSNMP_OCTET_STRING_TYPE * pVacmGroupName,
                              tSNMP_OCTET_STRING_TYPE *
                              pVacmAccessContextPrefix,
                              INT4 i4VacmAccessSecurityModel,
                              INT4 i4VacmAccessSecurityLevel,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValVacmAccessReadViewName)
{
    tAccessEntry       *pAccessEntry = NULL;
    pAccessEntry = VACMGetAccess (pVacmGroupName, pVacmAccessContextPrefix,
                                  i4VacmAccessSecurityModel,
                                  i4VacmAccessSecurityLevel);
    if (pAccessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pRetValVacmAccessReadViewName,
                         &(pAccessEntry->ReadName));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetVacmAccessWriteViewName
 Input       :  The Indices
                VacmGroupName
                VacmAccessContextPrefix
                VacmAccessSecurityModel
                VacmAccessSecurityLevel

                The Object 
                retValVacmAccessWriteViewName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVacmAccessWriteViewName (tSNMP_OCTET_STRING_TYPE * pVacmGroupName,
                               tSNMP_OCTET_STRING_TYPE *
                               pVacmAccessContextPrefix,
                               INT4 i4VacmAccessSecurityModel,
                               INT4 i4VacmAccessSecurityLevel,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValVacmAccessWriteViewName)
{
    tAccessEntry       *pAccessEntry = NULL;
    pAccessEntry = VACMGetAccess (pVacmGroupName, pVacmAccessContextPrefix,
                                  i4VacmAccessSecurityModel,
                                  i4VacmAccessSecurityLevel);
    if (pAccessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pRetValVacmAccessWriteViewName,
                         &(pAccessEntry->WriteName));
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetVacmAccessNotifyViewName
 Input       :  The Indices
                VacmGroupName
                VacmAccessContextPrefix
                VacmAccessSecurityModel
                VacmAccessSecurityLevel

                The Object 
                retValVacmAccessNotifyViewName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVacmAccessNotifyViewName (tSNMP_OCTET_STRING_TYPE * pVacmGroupName,
                                tSNMP_OCTET_STRING_TYPE *
                                pVacmAccessContextPrefix,
                                INT4 i4VacmAccessSecurityModel,
                                INT4 i4VacmAccessSecurityLevel,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValVacmAccessNotifyViewName)
{
    tAccessEntry       *pAccessEntry = NULL;
    pAccessEntry = VACMGetAccess (pVacmGroupName, pVacmAccessContextPrefix,
                                  i4VacmAccessSecurityModel,
                                  i4VacmAccessSecurityLevel);
    if (pAccessEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    SNMPCopyOctetString (pRetValVacmAccessNotifyViewName,
                         &(pAccessEntry->NotifyName));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetVacmAccessStorageType
 Input       :  The Indices
                VacmGroupName
                VacmAccessContextPrefix
                VacmAccessSecurityModel
                VacmAccessSecurityLevel

                The Object 
                retValVacmAccessStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVacmAccessStorageType (tSNMP_OCTET_STRING_TYPE * pVacmGroupName,
                             tSNMP_OCTET_STRING_TYPE * pVacmAccessContextPrefix,
                             INT4 i4VacmAccessSecurityModel,
                             INT4 i4VacmAccessSecurityLevel,
                             INT4 *pi4RetValVacmAccessStorageType)
{
    tAccessEntry       *pAccessEntry = NULL;
    pAccessEntry = VACMGetAccess (pVacmGroupName, pVacmAccessContextPrefix,
                                  i4VacmAccessSecurityModel,
                                  i4VacmAccessSecurityLevel);
    if (pAccessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValVacmAccessStorageType = pAccessEntry->i4Storage;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetVacmAccessStatus
 Input       :  The Indices
                VacmGroupName
                VacmAccessContextPrefix
                VacmAccessSecurityModel
                VacmAccessSecurityLevel

                The Object 
                retValVacmAccessStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVacmAccessStatus (tSNMP_OCTET_STRING_TYPE * pVacmGroupName,
                        tSNMP_OCTET_STRING_TYPE * pVacmAccessContextPrefix,
                        INT4 i4VacmAccessSecurityModel,
                        INT4 i4VacmAccessSecurityLevel,
                        INT4 *pi4RetValVacmAccessStatus)
{
    tAccessEntry       *pAccessEntry = NULL;
    pAccessEntry = VACMGetAccess (pVacmGroupName, pVacmAccessContextPrefix,
                                  i4VacmAccessSecurityModel,
                                  i4VacmAccessSecurityLevel);
    if (pAccessEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pAccessEntry->i4Status == UNDER_CREATION)
    {
        *pi4RetValVacmAccessStatus = SNMP_ROWSTATUS_NOTINSERVICE;
    }
    if (pAccessEntry->i4Status == SNMP_ACTIVE)
    {
        *pi4RetValVacmAccessStatus = SNMP_ROWSTATUS_ACTIVE;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetVacmAccessContextMatch
 Input       :  The Indices
                VacmGroupName
                VacmAccessContextPrefix
                VacmAccessSecurityModel
                VacmAccessSecurityLevel

                The Object 
                setValVacmAccessContextMatch
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVacmAccessContextMatch (tSNMP_OCTET_STRING_TYPE * pVacmGroupName,
                              tSNMP_OCTET_STRING_TYPE *
                              pVacmAccessContextPrefix,
                              INT4 i4VacmAccessSecurityModel,
                              INT4 i4VacmAccessSecurityLevel,
                              INT4 i4SetValVacmAccessContextMatch)
{
    tAccessEntry       *pAccess = NULL;
    pAccess = VACMGetAccess (pVacmGroupName, pVacmAccessContextPrefix,
                             i4VacmAccessSecurityModel,
                             i4VacmAccessSecurityLevel);
    if (pAccess == NULL)
    {
        return SNMP_FAILURE;
    }
    pAccess->i4ContextMatch = i4SetValVacmAccessContextMatch;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetVacmAccessReadViewName
 Input       :  The Indices
                VacmGroupName
                VacmAccessContextPrefix
                VacmAccessSecurityModel
                VacmAccessSecurityLevel

                The Object 
                setValVacmAccessReadViewName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVacmAccessReadViewName (tSNMP_OCTET_STRING_TYPE * pVacmGroupName,
                              tSNMP_OCTET_STRING_TYPE *
                              pVacmAccessContextPrefix,
                              INT4 i4VacmAccessSecurityModel,
                              INT4 i4VacmAccessSecurityLevel,
                              tSNMP_OCTET_STRING_TYPE *
                              pSetValVacmAccessReadViewName)
{
    tAccessEntry       *pAccess = NULL;
    pAccess = VACMGetAccess (pVacmGroupName, pVacmAccessContextPrefix,
                             i4VacmAccessSecurityModel,
                             i4VacmAccessSecurityLevel);
    if (pAccess == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (&(pAccess->ReadName), pSetValVacmAccessReadViewName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetVacmAccessWriteViewName
 Input       :  The Indices
                VacmGroupName
                VacmAccessContextPrefix
                VacmAccessSecurityModel
                VacmAccessSecurityLevel

                The Object 
                setValVacmAccessWriteViewName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVacmAccessWriteViewName (tSNMP_OCTET_STRING_TYPE * pVacmGroupName,
                               tSNMP_OCTET_STRING_TYPE *
                               pVacmAccessContextPrefix,
                               INT4 i4VacmAccessSecurityModel,
                               INT4 i4VacmAccessSecurityLevel,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValVacmAccessWriteViewName)
{
    tAccessEntry       *pAccess = NULL;
    pAccess = VACMGetAccess (pVacmGroupName, pVacmAccessContextPrefix,
                             i4VacmAccessSecurityModel,
                             i4VacmAccessSecurityLevel);
    if (pAccess == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (&(pAccess->WriteName), pSetValVacmAccessWriteViewName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetVacmAccessNotifyViewName
 Input       :  The Indices
                VacmGroupName
                VacmAccessContextPrefix
                VacmAccessSecurityModel
                VacmAccessSecurityLevel

                The Object 
                setValVacmAccessNotifyViewName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVacmAccessNotifyViewName (tSNMP_OCTET_STRING_TYPE * pVacmGroupName,
                                tSNMP_OCTET_STRING_TYPE *
                                pVacmAccessContextPrefix,
                                INT4 i4VacmAccessSecurityModel,
                                INT4 i4VacmAccessSecurityLevel,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValVacmAccessNotifyViewName)
{
    tAccessEntry       *pAccess = NULL;
    pAccess = VACMGetAccess (pVacmGroupName, pVacmAccessContextPrefix,
                             i4VacmAccessSecurityModel,
                             i4VacmAccessSecurityLevel);
    if (pAccess == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (&(pAccess->NotifyName),
                         pSetValVacmAccessNotifyViewName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetVacmAccessStorageType
 Input       :  The Indices
                VacmGroupName
                VacmAccessContextPrefix
                VacmAccessSecurityModel
                VacmAccessSecurityLevel

                The Object 
                setValVacmAccessStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVacmAccessStorageType (tSNMP_OCTET_STRING_TYPE * pVacmGroupName,
                             tSNMP_OCTET_STRING_TYPE *
                             pVacmAccessContextPrefix,
                             INT4 i4VacmAccessSecurityModel,
                             INT4 i4VacmAccessSecurityLevel,
                             INT4 i4SetValVacmAccessStorageType)
{
    tAccessEntry       *pAccess = NULL;
    pAccess = VACMGetAccess (pVacmGroupName, pVacmAccessContextPrefix,
                             i4VacmAccessSecurityModel,
                             i4VacmAccessSecurityLevel);
    if (pAccess == NULL)
    {
        return SNMP_FAILURE;
    }
    pAccess->i4Storage = i4SetValVacmAccessStorageType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetVacmAccessStatus
 Input       :  The Indices
                VacmGroupName
                VacmAccessContextPrefix
                VacmAccessSecurityModel
                VacmAccessSecurityLevel

                The Object 
                setValVacmAccessStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVacmAccessStatus (tSNMP_OCTET_STRING_TYPE * pVacmGroupName,
                        tSNMP_OCTET_STRING_TYPE * pVacmAccessContextPrefix,
                        INT4 i4VacmAccessSecurityModel,
                        INT4 i4VacmAccessSecurityLevel,
                        INT4 i4SetValVacmAccessStatus)
{
    switch (i4SetValVacmAccessStatus)
    {

        case SNMP_ROWSTATUS_CREATEANDGO:
        case SNMP_ROWSTATUS_CREATEANDWAIT:
        {
            UINT4               u4Return;
            u4Return =
                (UINT4) VACMAddAccess (pVacmGroupName, pVacmAccessContextPrefix,
                                       i4VacmAccessSecurityModel,
                                       i4VacmAccessSecurityLevel);
            if (u4Return != SNMP_FAILURE)
            {
                return SNMP_SUCCESS;
            }
            else
            {
                return SNMP_FAILURE;
            }
        }

        case SNMP_ROWSTATUS_ACTIVE:
        {
            tAccessEntry       *pAccess = NULL;
            pAccess = VACMGetAccess (pVacmGroupName, pVacmAccessContextPrefix,
                                     i4VacmAccessSecurityModel,
                                     i4VacmAccessSecurityLevel);
            if (pAccess == NULL)
            {
                return SNMP_FAILURE;
            }
            pAccess->i4Status = SNMP_ROWSTATUS_ACTIVE;
            return SNMP_SUCCESS;
        }
        case SNMP_ROWSTATUS_DESTROY:
        {
            UINT4               u4Return;
            u4Return = VACMDeleteAccess (pVacmGroupName,
                                         pVacmAccessContextPrefix,
                                         i4VacmAccessSecurityModel,
                                         i4VacmAccessSecurityLevel);
            if (u4Return == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }
            else
            {
                return SNMP_SUCCESS;
            }

        }
        case SNMP_ROWSTATUS_NOTINSERVICE:
        {
            tAccessEntry       *pAccess = NULL;
            pAccess = VACMGetAccess (pVacmGroupName, pVacmAccessContextPrefix,
                                     i4VacmAccessSecurityModel,
                                     i4VacmAccessSecurityLevel);
            if (pAccess == NULL)
            {
                return SNMP_FAILURE;
            }
            if (pAccess->i4Status == SNMP_ROWSTATUS_ACTIVE)
            {
                pAccess->i4Status = SNMP_ROWSTATUS_NOTINSERVICE;
                return SNMP_SUCCESS;
            }
        }
        default:
            break;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2VacmAccessContextMatch
 Input       :  The Indices
                VacmGroupName
                VacmAccessContextPrefix
                VacmAccessSecurityModel
                VacmAccessSecurityLevel

                The Object 
                testValVacmAccessContextMatch
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VacmAccessContextMatch (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE * pVacmGroupName,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pVacmAccessContextPrefix,
                                 INT4 i4VacmAccessSecurityModel,
                                 INT4 i4VacmAccessSecurityLevel,
                                 INT4 i4TestValVacmAccessContextMatch)
{
    if (nmhValidateIndexInstanceVacmAccessTable (pVacmGroupName,
                                                 pVacmAccessContextPrefix,
                                                 i4VacmAccessSecurityModel,
                                                 i4VacmAccessSecurityLevel) ==
        SNMP_SUCCESS)
    {

        if ((i4TestValVacmAccessContextMatch != SNMP_VACM_ACCESS_CMATCH_EXACT)
            && (i4TestValVacmAccessContextMatch !=
                SNMP_VACM_ACCESS_CMATCH_PREFIX))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2VacmAccessReadViewName
 Input       :  The Indices
                VacmGroupName
                VacmAccessContextPrefix
                VacmAccessSecurityModel
                VacmAccessSecurityLevel

                The Object 
                testValVacmAccessReadViewName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VacmAccessReadViewName (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE * pVacmGroupName,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pVacmAccessContextPrefix,
                                 INT4 i4VacmAccessSecurityModel,
                                 INT4 i4VacmAccessSecurityLevel,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pTestValVacmAccessReadViewName)
{
    if (nmhValidateIndexInstanceVacmAccessTable (pVacmGroupName,
                                                 pVacmAccessContextPrefix,
                                                 i4VacmAccessSecurityModel,
                                                 i4VacmAccessSecurityLevel) ==
        SNMP_SUCCESS)
    {

        if ((pTestValVacmAccessReadViewName->i4_Length < SNMP_VACM_MIN_STRING)
            || (pTestValVacmAccessReadViewName->i4_Length >
                SNMP_VACM_MAX_STRING))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    else
    {

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2VacmAccessWriteViewName
 Input       :  The Indices
                VacmGroupName
                VacmAccessContextPrefix
                VacmAccessSecurityModel
                VacmAccessSecurityLevel

                The Object 
                testValVacmAccessWriteViewName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VacmAccessWriteViewName (UINT4 *pu4ErrorCode,
                                  tSNMP_OCTET_STRING_TYPE * pVacmGroupName,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pVacmAccessContextPrefix,
                                  INT4 i4VacmAccessSecurityModel,
                                  INT4 i4VacmAccessSecurityLevel,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValVacmAccessWriteViewName)
{
#ifdef FIPS_WANTED
    INT4                i4FipsOperMode = 0;

    IssCustGetFipsCurrOperMode (&i4FipsOperMode);

    if ((ISS_FIPS_MODE == i4FipsOperMode) || (ISS_CNSA_MODE == i4FipsOperMode))
    {
        if ((i4VacmAccessSecurityModel == SNMP_SECMODEL_V1) ||
            (i4VacmAccessSecurityModel == SNMP_SECMODEL_V2C))
        {
            *pu4ErrorCode = SNMP_ERROR_WRTIE_ACCESS_DENIED;
            return SNMP_FAILURE;
        }
    }
#endif
    if (nmhValidateIndexInstanceVacmAccessTable (pVacmGroupName,
                                                 pVacmAccessContextPrefix,
                                                 i4VacmAccessSecurityModel,
                                                 i4VacmAccessSecurityLevel) ==
        SNMP_SUCCESS)
    {
        if ((pTestValVacmAccessWriteViewName->i4_Length < SNMP_VACM_MIN_STRING)
            || (pTestValVacmAccessWriteViewName->i4_Length >
                SNMP_VACM_MAX_STRING))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    else
    {

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2VacmAccessNotifyViewName
 Input       :  The Indices
                VacmGroupName
                VacmAccessContextPrefix
                VacmAccessSecurityModel
                VacmAccessSecurityLevel

                The Object 
                testValVacmAccessNotifyViewName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VacmAccessNotifyViewName (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE * pVacmGroupName,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pVacmAccessContextPrefix,
                                   INT4 i4VacmAccessSecurityModel,
                                   INT4 i4VacmAccessSecurityLevel,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValVacmAccessNotifyViewName)
{
    if (nmhValidateIndexInstanceVacmAccessTable (pVacmGroupName,
                                                 pVacmAccessContextPrefix,
                                                 i4VacmAccessSecurityModel,
                                                 i4VacmAccessSecurityLevel) ==
        SNMP_SUCCESS)
    {
        if ((pTestValVacmAccessNotifyViewName->i4_Length < SNMP_VACM_MIN_STRING)
            || (pTestValVacmAccessNotifyViewName->i4_Length >
                SNMP_VACM_MAX_STRING))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    else
    {

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2VacmAccessStorageType
 Input       :  The Indices
                VacmGroupName
                VacmAccessContextPrefix
                VacmAccessSecurityModel
                VacmAccessSecurityLevel

                The Object 
                testValVacmAccessStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VacmAccessStorageType (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE * pVacmGroupName,
                                tSNMP_OCTET_STRING_TYPE *
                                pVacmAccessContextPrefix,
                                INT4 i4VacmAccessSecurityModel,
                                INT4 i4VacmAccessSecurityLevel,
                                INT4 i4TestValVacmAccessStorageType)
{
    if (nmhValidateIndexInstanceVacmAccessTable (pVacmGroupName,
                                                 pVacmAccessContextPrefix,
                                                 i4VacmAccessSecurityModel,
                                                 i4VacmAccessSecurityLevel) ==
        SNMP_SUCCESS)
    {

        if ((i4TestValVacmAccessStorageType < SNMP3_STORAGE_TYPE_OTHER) ||
            (i4TestValVacmAccessStorageType > SNMP3_STORAGE_TYPE_READONLY))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        if ((i4TestValVacmAccessStorageType != SNMP3_STORAGE_TYPE_VOLATILE) &&
            (i4TestValVacmAccessStorageType != SNMP3_STORAGE_TYPE_NONVOLATILE))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    else
    {

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2VacmAccessStatus
 Input       :  The Indices
                VacmGroupName
                VacmAccessContextPrefix
                VacmAccessSecurityModel
                VacmAccessSecurityLevel

                The Object 
                testValVacmAccessStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VacmAccessStatus (UINT4 *pu4ErrorCode,
                           tSNMP_OCTET_STRING_TYPE * pVacmGroupName,
                           tSNMP_OCTET_STRING_TYPE * pVacmAccessContextPrefix,
                           INT4 i4VacmAccessSecurityModel,
                           INT4 i4VacmAccessSecurityLevel,
                           INT4 i4TestValVacmAccessStatus)
{
    tAccessEntry       *pAccess = NULL;
    if ((i4TestValVacmAccessStatus == SNMP_ROWSTATUS_CREATEANDGO) ||
        (i4TestValVacmAccessStatus == SNMP_ROWSTATUS_NOTREADY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValVacmAccessStatus < SNMP_ROWSTATUS_ACTIVE) ||
        (i4TestValVacmAccessStatus > SNMP_ROWSTATUS_DESTROY))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    pAccess = VACMGetAccess (pVacmGroupName, pVacmAccessContextPrefix,
                             i4VacmAccessSecurityModel,
                             i4VacmAccessSecurityLevel);

    if ((pAccess == NULL)
        && (i4TestValVacmAccessStatus == SNMP_ROWSTATUS_ACTIVE))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (nmhValidateIndexInstanceVacmAccessTable (pVacmGroupName,
                                                 pVacmAccessContextPrefix,
                                                 i4VacmAccessSecurityModel,
                                                 i4VacmAccessSecurityLevel) ==
        SNMP_SUCCESS)
    {
        if (i4TestValVacmAccessStatus == SNMP_ROWSTATUS_CREATEANDWAIT)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        else
        {
            return SNMP_SUCCESS;
        }
    }
    else
    {
        if (i4TestValVacmAccessStatus != SNMP_ROWSTATUS_CREATEANDWAIT)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        else
        {
            return SNMP_SUCCESS;
        }
    }
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2VacmAccessTable
 Input       :  The Indices
                VacmGroupName
                VacmAccessContextPrefix
                VacmAccessSecurityModel
                VacmAccessSecurityLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2VacmAccessTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetVacmViewSpinLock
 Input       :  The Indices

                The Object 
                retValVacmViewSpinLock
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVacmViewSpinLock (INT4 *pi4RetValVacmViewSpinLock)
{
    *pi4RetValVacmViewSpinLock = gi4VacmViewSpinLock;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetVacmViewSpinLock
 Input       :  The Indices

                The Object 
                setValVacmViewSpinLock
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVacmViewSpinLock (INT4 i4SetValVacmViewSpinLock)
{
    gi4VacmViewSpinLock = i4SetValVacmViewSpinLock;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2VacmViewSpinLock
 Input       :  The Indices

                The Object 
                testValVacmViewSpinLock
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VacmViewSpinLock (UINT4 *pu4ErrorCode, INT4 i4TestValVacmViewSpinLock)
{
    if (i4TestValVacmViewSpinLock != gi4VacmViewSpinLock)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2VacmViewSpinLock
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2VacmViewSpinLock (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : VacmViewTreeFamilyTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceVacmViewTreeFamilyTable
 Input       :  The Indices
                VacmViewTreeFamilyViewName
                VacmViewTreeFamilySubtree
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceVacmViewTreeFamilyTable (tSNMP_OCTET_STRING_TYPE *
                                                 pVacmViewTreeFamilyViewName,
                                                 tSNMP_OID_TYPE *
                                                 pVacmViewTreeFamilySubtree)
{
    if (VACMViewTreeGet
        (pVacmViewTreeFamilyViewName, pVacmViewTreeFamilySubtree) == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexVacmViewTreeFamilyTable
 Input       :  The Indices
                VacmViewTreeFamilyViewName
                VacmViewTreeFamilySubtree
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexVacmViewTreeFamilyTable (tSNMP_OCTET_STRING_TYPE *
                                         pVacmViewTreeFamilyViewName,
                                         tSNMP_OID_TYPE *
                                         pVacmViewTreeFamilySubtree)
{
    tViewTree          *pTree = NULL;
    pTree = VACMViewTreeGetFirst ();
    if (pTree == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pVacmViewTreeFamilyViewName, pTree->pName);
    SNMPOIDCopy (pVacmViewTreeFamilySubtree, &(pTree->SubTree));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexVacmViewTreeFamilyTable
 Input       :  The Indices
                VacmViewTreeFamilyViewName
                nextVacmViewTreeFamilyViewName
                VacmViewTreeFamilySubtree
                nextVacmViewTreeFamilySubtree
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexVacmViewTreeFamilyTable (tSNMP_OCTET_STRING_TYPE *
                                        pVacmViewTreeFamilyViewName,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pNextVacmViewTreeFamilyViewName,
                                        tSNMP_OID_TYPE *
                                        pVacmViewTreeFamilySubtree,
                                        tSNMP_OID_TYPE *
                                        pNextVacmViewTreeFamilySubtree)
{
    tViewTree          *pTree = NULL;

    if (pVacmViewTreeFamilyViewName->i4_Length < SNMP_ZERO)
    {
        return SNMP_FAILURE;
    }

    pTree = VACMViewTreeGetNext (pVacmViewTreeFamilyViewName,
                                 pVacmViewTreeFamilySubtree);
    if (pTree == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pNextVacmViewTreeFamilyViewName, pTree->pName);
    SNMPOIDCopy (pNextVacmViewTreeFamilySubtree, &(pTree->SubTree));
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetVacmViewTreeFamilyMask
 Input       :  The Indices
                VacmViewTreeFamilyViewName
                VacmViewTreeFamilySubtree

                The Object 
                retValVacmViewTreeFamilyMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVacmViewTreeFamilyMask (tSNMP_OCTET_STRING_TYPE *
                              pVacmViewTreeFamilyViewName,
                              tSNMP_OID_TYPE * pVacmViewTreeFamilySubtree,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValVacmViewTreeFamilyMask)
{
    tViewTree          *pTree = NULL;
    pTree = VACMViewTreeGet (pVacmViewTreeFamilyViewName,
                             pVacmViewTreeFamilySubtree);
    if (pTree == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pRetValVacmViewTreeFamilyMask, &(pTree->Mask));
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetVacmViewTreeFamilyType
 Input       :  The Indices
                VacmViewTreeFamilyViewName
                VacmViewTreeFamilySubtree

                The Object 
                retValVacmViewTreeFamilyType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVacmViewTreeFamilyType (tSNMP_OCTET_STRING_TYPE *
                              pVacmViewTreeFamilyViewName,
                              tSNMP_OID_TYPE * pVacmViewTreeFamilySubtree,
                              INT4 *pi4RetValVacmViewTreeFamilyType)
{
    tViewTree          *pTree = NULL;
    pTree = VACMViewTreeGet (pVacmViewTreeFamilyViewName,
                             pVacmViewTreeFamilySubtree);
    if (pTree == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValVacmViewTreeFamilyType = pTree->i4Type;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetVacmViewTreeFamilyStorageType
 Input       :  The Indices
                VacmViewTreeFamilyViewName
                VacmViewTreeFamilySubtree

                The Object 
                retValVacmViewTreeFamilyStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVacmViewTreeFamilyStorageType (tSNMP_OCTET_STRING_TYPE *
                                     pVacmViewTreeFamilyViewName,
                                     tSNMP_OID_TYPE *
                                     pVacmViewTreeFamilySubtree,
                                     INT4
                                     *pi4RetValVacmViewTreeFamilyStorageType)
{
    tViewTree          *pTree = NULL;
    pTree = VACMViewTreeGet (pVacmViewTreeFamilyViewName,
                             pVacmViewTreeFamilySubtree);
    if (pTree == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValVacmViewTreeFamilyStorageType = pTree->i4Storage;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetVacmViewTreeFamilyStatus
 Input       :  The Indices
                VacmViewTreeFamilyViewName
                VacmViewTreeFamilySubtree

                The Object 
                retValVacmViewTreeFamilyStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVacmViewTreeFamilyStatus (tSNMP_OCTET_STRING_TYPE *
                                pVacmViewTreeFamilyViewName,
                                tSNMP_OID_TYPE * pVacmViewTreeFamilySubtree,
                                INT4 *pi4RetValVacmViewTreeFamilyStatus)
{
    tViewTree          *pTree = NULL;
    pTree = VACMViewTreeGet (pVacmViewTreeFamilyViewName,
                             pVacmViewTreeFamilySubtree);
    if (pTree == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pTree->i4Status == UNDER_CREATION)
    {
        *pi4RetValVacmViewTreeFamilyStatus = SNMP_ROWSTATUS_NOTINSERVICE;
    }
    if (pTree->i4Status == SNMP_ACTIVE)
    {
        *pi4RetValVacmViewTreeFamilyStatus = SNMP_ROWSTATUS_ACTIVE;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetVacmViewTreeFamilyMask
 Input       :  The Indices
                VacmViewTreeFamilyViewName
                VacmViewTreeFamilySubtree

                The Object 
                setValVacmViewTreeFamilyMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVacmViewTreeFamilyMask (tSNMP_OCTET_STRING_TYPE *
                              pVacmViewTreeFamilyViewName,
                              tSNMP_OID_TYPE * pVacmViewTreeFamilySubtree,
                              tSNMP_OCTET_STRING_TYPE *
                              pSetValVacmViewTreeFamilyMask)
{
    tViewTree          *pTree = NULL;

    pTree = VACMViewTreeGet (pVacmViewTreeFamilyViewName,
                             pVacmViewTreeFamilySubtree);
    if (pTree == NULL)
    {
        SNMP_TRC (SNMP_FAILURE_TRC, "nmhSetVacmViewTreeFamilyMask: "
                  "NULL view entry\r\n");
        return SNMP_FAILURE;
    }
    MEMSET (pTree->Mask.pu1_OctetList, 1, SNMP_VIEW_TREE_MASK_MAX_LENGTH);
    SNMPCopyOctetString (&(pTree->Mask), pSetValVacmViewTreeFamilyMask);
    pTree->Mask.i4_Length = (INT4) pTree->SubTree.u4_Length;
    SNMP_TRC1 (SNMP_DEBUG_TRC, "nmhSetVacmViewTreeFamilyMask: "
               "Suuccessfully setting the family mask for the view [%s]\r\n",
               pVacmViewTreeFamilyViewName->pu1_OctetList);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetVacmViewTreeFamilyType
 Input       :  The Indices
                VacmViewTreeFamilyViewName
                VacmViewTreeFamilySubtree

                The Object 
                setValVacmViewTreeFamilyType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVacmViewTreeFamilyType (tSNMP_OCTET_STRING_TYPE *
                              pVacmViewTreeFamilyViewName,
                              tSNMP_OID_TYPE * pVacmViewTreeFamilySubtree,
                              INT4 i4SetValVacmViewTreeFamilyType)
{
    tViewTree          *pTree = NULL;
    pTree = VACMViewTreeGet (pVacmViewTreeFamilyViewName,
                             pVacmViewTreeFamilySubtree);
    if (pTree == NULL)
    {
        SNMP_TRC (SNMP_FAILURE_TRC, "nmhSetVacmViewTreeFamilyType: "
                  "NULL view entry\r\n");
        return SNMP_FAILURE;
    }
    pTree->i4Type = i4SetValVacmViewTreeFamilyType;
    SNMP_TRC1 (SNMP_DEBUG_TRC, "nmhSetVacmViewTreeFamilyType: "
               "Suuccessfully setting the family type for the view [%s]\r\n",
               pVacmViewTreeFamilyViewName->pu1_OctetList);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetVacmViewTreeFamilyStorageType
 Input       :  The Indices
                VacmViewTreeFamilyViewName
                VacmViewTreeFamilySubtree

                The Object 
                setValVacmViewTreeFamilyStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVacmViewTreeFamilyStorageType (tSNMP_OCTET_STRING_TYPE *
                                     pVacmViewTreeFamilyViewName,
                                     tSNMP_OID_TYPE *
                                     pVacmViewTreeFamilySubtree,
                                     INT4 i4SetValVacmViewTreeFamilyStorageType)
{
    tViewTree          *pTree = NULL;
    pTree = VACMViewTreeGet (pVacmViewTreeFamilyViewName,
                             pVacmViewTreeFamilySubtree);
    if (pTree == NULL)
    {
        SNMP_TRC (SNMP_FAILURE_TRC, "nmhSetVacmViewTreeFamilyStorageType: "
                  "NULL view entry\r\n");
        return SNMP_FAILURE;
    }
    pTree->i4Storage = i4SetValVacmViewTreeFamilyStorageType;
    SNMP_TRC1 (SNMP_DEBUG_TRC, "nmhSetVacmViewTreeFamilyStorageType: "
               "Suuccessfully setting the family storage type for the view [%s]\r\n",
               pVacmViewTreeFamilyViewName->pu1_OctetList);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetVacmViewTreeFamilyStatus
 Input       :  The Indices
                VacmViewTreeFamilyViewName
                VacmViewTreeFamilySubtree

                The Object 
                setValVacmViewTreeFamilyStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVacmViewTreeFamilyStatus (tSNMP_OCTET_STRING_TYPE *
                                pVacmViewTreeFamilyViewName,
                                tSNMP_OID_TYPE * pVacmViewTreeFamilySubtree,
                                INT4 i4SetValVacmViewTreeFamilyStatus)
{

    switch (i4SetValVacmViewTreeFamilyStatus)
    {

        case SNMP_ROWSTATUS_CREATEANDGO:
        case SNMP_ROWSTATUS_CREATEANDWAIT:
        {
            UINT4               u4Return;
            u4Return = (UINT4) VACMViewTreeAdd (pVacmViewTreeFamilyViewName,
                                                pVacmViewTreeFamilySubtree);
            if (u4Return != SNMP_FAILURE)
            {
                return SNMP_SUCCESS;
            }
            else
            {
                return SNMP_FAILURE;
            }
        }

        case SNMP_ROWSTATUS_ACTIVE:
        {
            tViewTree          *pTree = NULL;
            pTree = VACMViewTreeGet (pVacmViewTreeFamilyViewName,
                                     pVacmViewTreeFamilySubtree);
            if (pTree == NULL)
            {
                return SNMP_FAILURE;
            }
            pTree->i4Status = SNMP_ROWSTATUS_ACTIVE;
            return SNMP_SUCCESS;
        }

        case SNMP_ROWSTATUS_DESTROY:
        {
            UINT4               u4Return;
            u4Return = (UINT4) VACMDeleteViewTree (pVacmViewTreeFamilyViewName,
                                                   pVacmViewTreeFamilySubtree);
            if (u4Return == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }
            else
            {
                return SNMP_SUCCESS;
            }
        }

        case SNMP_ROWSTATUS_NOTINSERVICE:
        {
            tViewTree          *pTree = NULL;
            pTree = VACMViewTreeGet (pVacmViewTreeFamilyViewName,
                                     pVacmViewTreeFamilySubtree);
            if (pTree == NULL)
            {
                return SNMP_FAILURE;
            }
            if (pTree->i4Status == SNMP_ROWSTATUS_ACTIVE)
            {
                pTree->i4Status = SNMP_ROWSTATUS_NOTINSERVICE;
                return SNMP_SUCCESS;
            }
        }
        default:
            break;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2VacmViewTreeFamilyMask
 Input       :  The Indices
                VacmViewTreeFamilyViewName
                VacmViewTreeFamilySubtree

                The Object 
                testValVacmViewTreeFamilyMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VacmViewTreeFamilyMask (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pVacmViewTreeFamilyViewName,
                                 tSNMP_OID_TYPE * pVacmViewTreeFamilySubtree,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pTestValVacmViewTreeFamilyMask)
{

    tViewTree          *pViewEntry = NULL;

    if ((pViewEntry = VACMViewTreeGet
         (pVacmViewTreeFamilyViewName, pVacmViewTreeFamilySubtree)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    else if (pTestValVacmViewTreeFamilyMask->i4_Length >
             SNMP_VIEW_TREE_MASK_MAX_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    else if ((UINT4) pTestValVacmViewTreeFamilyMask->i4_Length >
             pViewEntry->SubTree.u4_Length)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2VacmViewTreeFamilyType
 Input       :  The Indices
                VacmViewTreeFamilyViewName
                VacmViewTreeFamilySubtree

                The Object 
                testValVacmViewTreeFamilyType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VacmViewTreeFamilyType (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pVacmViewTreeFamilyViewName,
                                 tSNMP_OID_TYPE * pVacmViewTreeFamilySubtree,
                                 INT4 i4TestValVacmViewTreeFamilyType)
{
    if (nmhValidateIndexInstanceVacmViewTreeFamilyTable
        (pVacmViewTreeFamilyViewName,
         pVacmViewTreeFamilySubtree) == SNMP_SUCCESS)
    {
        if ((i4TestValVacmViewTreeFamilyType != VACM_VIEW_TREE_TYPE_INCLUDED) &&
            (i4TestValVacmViewTreeFamilyType != VACM_VIEW_TREE_TYPE_EXCLUDED))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2VacmViewTreeFamilyStorageType
 Input       :  The Indices
                VacmViewTreeFamilyViewName
                VacmViewTreeFamilySubtree

                The Object 
                testValVacmViewTreeFamilyStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VacmViewTreeFamilyStorageType (UINT4 *pu4ErrorCode,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pVacmViewTreeFamilyViewName,
                                        tSNMP_OID_TYPE *
                                        pVacmViewTreeFamilySubtree,
                                        INT4
                                        i4TestValVacmViewTreeFamilyStorageType)
{

    if (nmhValidateIndexInstanceVacmViewTreeFamilyTable
        (pVacmViewTreeFamilyViewName,
         pVacmViewTreeFamilySubtree) == SNMP_SUCCESS)
    {
        if ((i4TestValVacmViewTreeFamilyStorageType < SNMP3_STORAGE_TYPE_OTHER)
            || (i4TestValVacmViewTreeFamilyStorageType >
                SNMP3_STORAGE_TYPE_READONLY))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        if ((i4TestValVacmViewTreeFamilyStorageType !=
             SNMP3_STORAGE_TYPE_VOLATILE)
            && (i4TestValVacmViewTreeFamilyStorageType !=
                SNMP3_STORAGE_TYPE_NONVOLATILE))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2VacmViewTreeFamilyStatus
 Input       :  The Indices
                VacmViewTreeFamilyViewName
                VacmViewTreeFamilySubtree

                The Object 
                testValVacmViewTreeFamilyStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VacmViewTreeFamilyStatus (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pVacmViewTreeFamilyViewName,
                                   tSNMP_OID_TYPE * pVacmViewTreeFamilySubtree,
                                   INT4 i4TestValVacmViewTreeFamilyStatus)
{
    tViewTree          *pTree = NULL;

    if (i4TestValVacmViewTreeFamilyStatus == SNMP_ROWSTATUS_NOTREADY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValVacmViewTreeFamilyStatus < SNMP_ROWSTATUS_ACTIVE) ||
        (i4TestValVacmViewTreeFamilyStatus > SNMP_ROWSTATUS_DESTROY))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    pTree = VACMViewTreeGet (pVacmViewTreeFamilyViewName,
                             pVacmViewTreeFamilySubtree);
    if ((pTree == NULL) &&
        (i4TestValVacmViewTreeFamilyStatus == SNMP_ROWSTATUS_ACTIVE))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (nmhValidateIndexInstanceVacmViewTreeFamilyTable
        (pVacmViewTreeFamilyViewName,
         pVacmViewTreeFamilySubtree) == SNMP_SUCCESS)
    {
        if (i4TestValVacmViewTreeFamilyStatus == SNMP_ROWSTATUS_CREATEANDWAIT)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        else
        {
            return SNMP_SUCCESS;
        }
    }
    else
    {
        if (VACMCheckAccessTree (pVacmViewTreeFamilyViewName) == SNMP_FAILURE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if (i4TestValVacmViewTreeFamilyStatus != SNMP_ROWSTATUS_CREATEANDWAIT)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        else
        {
            return SNMP_SUCCESS;
        }
    }
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2VacmViewTreeFamilyTable
 Input       :  The Indices
                VacmViewTreeFamilyViewName
                VacmViewTreeFamilySubtree
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2VacmViewTreeFamilyTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
