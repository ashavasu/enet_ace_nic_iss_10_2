
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: snmpmem.h,v 1.7 2015/04/28 12:35:02 siva Exp $
 *
 * Description: typedef, macros and prototypes for memory pool
 *                management
 *******************************************************************/
#ifndef _SNMPMEM_H
#define _SNMPMEM_H

/* MEREG_FIX */
#define SIGN 0x55AAAA55
#define MAX_STR_LEN 256
#define MULTISIZE MAX_STR_LEN + 44

typedef struct MemBlock {
 tSNMP_VAR_BIND VarBind;
 tSNMP_OID_TYPE Oid;
 tSNMP_OID_TYPE DataOid;
 tSNMP_OCTET_STRING_TYPE Octet;
 UINT4 OidList[MAX_OID_LENGTH];
 UINT1 DataList[MAX_OID_LENGTH*SNMP_FOUR];
 struct MemBlock *pNext;
}tMemBlock;

tMemBlock gaMemPool[MAX_VARBIND];
tMemBlock *pCur = &(gaMemPool[0]);

tSnmpIndex gaIndexPool1[SNMP_MAX_INDICES_2];
tSNMP_MULTI_DATA_TYPE gaMultiPool1[SNMP_MAX_INDICES_2];
tSNMP_OCTET_STRING_TYPE gaOctetPool1[SNMP_MAX_INDICES_2];
tSNMP_OID_TYPE  gaOIDPool1[SNMP_MAX_INDICES_2];
UINT1 gau1Data1[SNMP_MAX_INDICES_2][1024];

tSnmpIndex gaIndexPool2[SNMP_MAX_INDICES_2];
tSNMP_MULTI_DATA_TYPE gaMultiPool2[SNMP_MAX_INDICES_2];
tSNMP_OCTET_STRING_TYPE gaOctetPool2[SNMP_MAX_INDICES_2];
tSNMP_OID_TYPE  gaOIDPool2[SNMP_MAX_INDICES_2];
UINT1 gau1Data2[SNMP_MAX_INDICES_2][1024];
INT1 gi1MemInitFlag = SNMP_FAILURE;

PRIVATE VOID SNMPIndexInit(VOID);
PRIVATE INT1 SNMPMemPoolInit (VOID);

tMemPoolId gSnmpVarBindPoolId;
tMemPoolId gSnmpGetVarBindPoolId;
tMemPoolId gSnmpPktPoolId;
tMemPoolId gSnmpNormalPduPoolId;
tMemPoolId gSnmpTrapPduPoolId;
tMemPoolId gSnmpInformNodePoolId;
tMemPoolId gSnmpInformMsgNodePoolId;
tMemPoolId gSnmpTcpRcvBufPoolId;
tMemPoolId gSnmpTcpTxNodePoolId;
tMemPoolId gSnmpV3PduPoolId;
tMemPoolId gSnmpOidTypePoolId;
tMemPoolId gSnmpOidListPoolId;
tMemPoolId gSnmpOctetStrPoolId;
tMemPoolId gSnmpOctetListPoolId;
tMemPoolId gSnmpMultiDataPoolId;
tMemPoolId gSnmpMultiIndexPoolId;
tMemPoolId gSnmpMultiDataIndexPoolId;
tMemPoolId gSnmpMultiOidPoolId;
tMemPoolId gSnmpAgentxOidTypePoolId;
tMemPoolId gSnmpSearchRngPoolId;
tMemPoolId gSnmpAgentxVarBindPoolId;
tMemPoolId gSnmpTrapFilterPoolId;
tMemPoolId gSnmpDataPoolId;
tMemPoolId gSnmpAuditInfoPoolId;
tMemPoolId gSnmpTagListPoolId;
tMemPoolId gSnmpAgentParamsPoolId;

#endif /* _SNMPMEM_H */
