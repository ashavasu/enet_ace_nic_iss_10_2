/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved 
 *
 * $Id: snprxtrn.h,v 1.1 2015/04/28 12:35:03 siva Exp $
 *
 * Description: Prototypes for SNMP Proxy Translations 
 *******************************************************************/
#ifndef _SNPRXTRN_H
#define _SNPRXTRN_H

#include "snmputil.h"

#define SnmpFreeV2Request(pPdu) if(pPdu != NULL) {\
                               FREE_OCTET_STRING(pPdu->pCommunityStr); \
                               FREE_SNMP_VARBIND_LIST(pPdu->pVarBindList);\
                               MemReleaseMemBlock (gSnmpNormalPduPoolId, (UINT1 *)(pPdu));}

#define SnmpFreeV3Pdu(pPdu) if(pPdu != NULL) {\
                               FREE_OCTET_STRING(pPdu->Normalpdu.pCommunityStr); \
                               FREE_SNMP_VARBIND_LIST(pPdu->Normalpdu.pVarBindList);\
                               MemReleaseMemBlock(gSnmpV3PduPoolId, (UINT1 *)pPdu);}

#define SNMP_PROXY_IS_PDU_REQ(PduType) \
                                    (PduType == SNMP_PDU_TYPE_GET_REQUEST ||\
                                    PduType == SNMP_PDU_TYPE_GET_NEXT_REQUEST ||\
                                    PduType == SNMP_PDU_TYPE_SET_REQUEST ||\
                                    PduType == SNMP_PDU_TYPE_GET_BULK_REQUEST)

tSNMP_OCTET_STRING_TYPE* 
SNMPGetCommunityString(
		tSNMP_OCTET_STRING_TYPE	*pSingleTargetOut,
		tSNMP_OCTET_STRING_TYPE	*pContextEngineID);

INT1 SNMPGetTargetVersion(
	tSNMP_OCTET_STRING_TYPE	*pSingleTargetOut,
	UINT4						*pVersion);

INT1 SNMPValidateTranslation(
        INT2 	PduType,
        UINT4 	PduVersion,
        UINT4 	TargetVersion);


INT1 SNMPProxyHandler(
void		*pPdu,
UINT4 	    pduVersion,
INT2		PduType,
tSNMP_OCTET_STRING_TYPE	*pSingleTargetOut,
tSNMP_PROXY_CACHE			*pCacheData,
UINT1							**pEncodedMsg,
UINT4							*pEncodedMsgLen);

INT1 SNMPProxyTranslation(
		void							*pPdu, 
		void							**ppOutputPdu, 
		UINT4							u4PduVersion, 
		UINT4							u4_TargetVersion, 
		INT2							PduType,
		INT2							*pTargetPduType,
		tSNMP_OCTET_STRING_TYPE	        *pSingleTargetOut,
        UINT4				            RequestID,
		tSNMP_PROXY_CACHE			    *pCacheData);

tSNMP_NORMAL_PDU* SNMPV3RequesttoV2Request(
	tSNMP_V3PDU					*pV3Pdu,
	tSNMP_OCTET_STRING_TYPE	*pSingleTargetOut,
    UINT4				RequestID);

tSNMP_NORMAL_PDU* SNMPV1V2RequesttoV1V2Request(
	tSNMP_NORMAL_PDU			*pV1V2Pdu,
	UINT4						PduVersion,
	tSNMP_OCTET_STRING_TYPE	*pSingleTargetOut,
    UINT4				RequestID);

tSNMP_NORMAL_PDU* SNMPV3RequesttoV1Request(
	tSNMP_V3PDU					*pV3Pdu,
	tSNMP_OCTET_STRING_TYPE	*pSingleTargetOut,
    UINT4				RequestID);

tSNMP_NORMAL_PDU* SNMPV2RequesttoV1Request(
	tSNMP_NORMAL_PDU			*pV2Pdu,
	tSNMP_OCTET_STRING_TYPE	*pSingleTargetOut,
    UINT4				RequestID);

tSNMP_V3PDU* SNMPV3RequesttoV3Request(
	tSNMP_V3PDU					*pV3Pdu,
	tSNMP_OCTET_STRING_TYPE	*pSingleTargetOut,
    UINT4				RequestID);

tSNMP_NORMAL_PDU* SNMPV1ResponsetoV2Response(
	tSNMP_NORMAL_PDU			*pV1Pdu,
	tSNMP_PROXY_CACHE			*pCacheData);

tSNMP_V3PDU* SNMPV2ResponsetoV3Response(
	tSNMP_NORMAL_PDU			*pV2Pdu,
	tSNMP_PROXY_CACHE			*pCacheData);

void* SNMPV1ResponsetoV3Response(
	tSNMP_NORMAL_PDU	*pV1Pdu,
	INT2				*pPduType,
	tSNMP_PROXY_CACHE	*pCacheData);

tSNMP_NORMAL_PDU* SNMPV1V2ResponsetoV1V2Response(
	tSNMP_NORMAL_PDU			*pV1V2Pdu,
	UINT4						u4PduVersion,
	tSNMP_PROXY_CACHE			*pCacheData);

tSNMP_V3PDU* SNMPV2TraptoV3Trap(
	tSNMP_NORMAL_PDU					*pV2TrapPdu,
	tSNMP_OCTET_STRING_TYPE	*pSingleTargetOut,
    UINT4				RequestID);

tSNMP_TRAP_PDU* SNMPV1TraptoV1Trap(
tSNMP_TRAP_PDU			*pV1TrapPdu,
tSNMP_OCTET_STRING_TYPE *pSingleTargetOut);

tSNMP_NORMAL_PDU* SNMPV2TraptoV2Trap(
tSNMP_NORMAL_PDU			*pV2TrapPdu,
tSNMP_OCTET_STRING_TYPE *pSingleTargetOut,
UINT4				RequestID);

tSNMP_V3PDU* SNMPV1TraptoV3Trap(
tSNMP_TRAP_PDU			*pV1TrapPdu,
tSNMP_OCTET_STRING_TYPE *pSingleTargetOut,
UINT4				RequestID);

tSNMP_V3PDU* SNMPV3TraptoV3Trap(
tSNMP_V3PDU			*pV3TrapPdu,
tSNMP_OCTET_STRING_TYPE *pSingleTargetOut,
UINT4				RequestID);

tSNMP_NORMAL_PDU* 
SNMPV3TraptoV2Trap(
        tSNMP_V3PDU 			  *pV3TrapPdu,
        tSNMP_OCTET_STRING_TYPE *pSingleTargetOut,
        UINT4					   RequestID);

INT1 SNMPCopyNormalPduParams(tSNMP_NORMAL_PDU *pDest, tSNMP_NORMAL_PDU  *pSrc);

INT4
SNMPStartProxyRequestTimer (tSNMP_PROXY_CACHE *pCacheData, 
                            tSnmpTgtAddrEntry * pSnmpTgtAddrEntry);

VOID SnmpProxyProcessTimerExpiry(VOID);

void SNMPProxyFreePdu(void *pPdu, INT4 Version, INT2 PduType);

tSNMP_V3PDU* SNMPV3ResponsetoV3Response(
        tSNMP_V3PDU *pV3Pdu,tSNMP_PROXY_CACHE *pCacheData);

tSNMP_NORMAL_PDU*  SNMPV2InfRespToV2InfResp(
			tSNMP_NORMAL_PDU		*pPdu,
			tSNMP_PROXY_CACHE		*pCacheData);


tSNMP_V3PDU*  SNMPAllocV3Pdu(VOID);

INT1 SNMPCopyV3ParamsFromCache(
	tSNMP_V3PDU				*pV3Pdu,
	tSNMP_PROXY_CACHE		*pCacheData);

tSNMP_V3PDU* SNMPV3InfRespToV3InfResp(
						tSNMP_V3PDU     			*pV3Pdu,
						tSNMP_PROXY_CACHE		*pCacheData);

VOID SNMPProxyCopyOctetStringFromArr(
	tSNMP_OCTET_STRING_TYPE		*pDestOct,
	tSNMP_OCTET_STRING_ARR			*pSrcArr);

VOID SNMPCopyArrFromOctetStr(
	tSNMP_OCTET_STRING_ARR		*pDestArr,
	tSNMP_OCTET_STRING_TYPE	*pSrcOct);

INT1 SNMPProxyFormFailResp(
			VOID	*pPdu,
			UINT4	pduVersion,
			UINT1	**pEncodedMsg,
			UINT4	*pEncodedMsgLen);

UINT4
SNMPProxyCompareOidtoarr (
        tSNMP_OID_TYPE *pOID1, 
        UINT4 *pOID2, 
        UINT4 u4OID2Len);

#endif /* _SNPRXTRN_H */
