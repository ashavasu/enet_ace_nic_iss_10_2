/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: sntarget.c,v 1.10 2015/04/28 12:35:03 siva Exp $
 *
 * Description: routines for Target Address Table 
 *******************************************************************/
#include "snmpcmn.h"
#include "sntarget.h"

/*********************************************************************
*  Function Name : SNMPTgtAddrTableInit 
*  Description   : Fucntion to Initialise the Taget Address Table 
*  Parameter(s)  : None
*  Return Values : None
*********************************************************************/

INT4
SNMPTgtAddrTableInit ()
{
    UINT4               u4Index = SNMP_ZERO;
    MEMSET (gSnmpTgtAddrTable, SNMP_ZERO, sizeof (gSnmpTgtAddrTable));
    MEMSET (gau1AddrName, SNMP_ZERO, sizeof (gau1AddrName));
    MEMSET (gau4Domain, SNMP_ZERO, sizeof (gau4Domain));
    MEMSET (gau1Address, SNMP_ZERO, sizeof (gau1Address));
    MEMSET (gau1TagList, SNMP_ZERO, sizeof (gau1TagList));
    MEMSET (gau1AddrParam, SNMP_ZERO, sizeof (gau1AddrParam));
    TMO_SLL_Init (&gSnmpTgtAddrSll);
    for (u4Index = SNMP_ZERO; u4Index < MAX_TARGET_ADDR_ENTRY; u4Index++)
    {
        gSnmpTgtAddrTable[u4Index].AddrName.pu1_OctetList =
            gau1AddrName[u4Index];
        gSnmpTgtAddrTable[u4Index].Domain.pu4_OidList = gau4Domain[u4Index];
        gSnmpTgtAddrTable[u4Index].Address.pu1_OctetList = gau1Address[u4Index];
        gSnmpTgtAddrTable[u4Index].i4TimeOut = SNMP_ZERO;
        gSnmpTgtAddrTable[u4Index].i4RetryCount = SNMP_ZERO;
        gSnmpTgtAddrTable[u4Index].TagList.pu1_OctetList = gau1TagList[u4Index];
        gSnmpTgtAddrTable[u4Index].AddrParams.pu1_OctetList =
            gau1AddrParam[u4Index];
        TMO_SLL_Init (&(gSnmpTgtAddrTable[u4Index].pInformList));
        gSnmpTgtAddrTable[u4Index].pInformStat =
            &gSnmpTgtInformStatArr[u4Index];
        gSnmpTgtAddrTable[u4Index].i4Status = SNMP_INACTIVE;
    }
    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : SNMPGetTgtAddrEntry 
*  Description   : Fucntion to get Target Address Entries 
*  Parameter(s)  : pTgtAddrName - Target Address Name 
*  Return Values : On Succes - return Entry
*                  On Failure - return NULL
*********************************************************************/

tSnmpTgtAddrEntry  *
SNMPGetTgtAddrEntry (tSNMP_OCTET_STRING_TYPE * pTgtAddrName)
{
    tTMO_SLL_NODE      *pLstNode = NULL;
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    TMO_SLL_Scan (&gSnmpTgtAddrSll, pLstNode, tTMO_SLL_NODE *)
    {
        pSnmpTgtAddrEntry = (tSnmpTgtAddrEntry *) pLstNode;
        if (SNMPCompareOctetString (pTgtAddrName,
                                    &(pSnmpTgtAddrEntry->AddrName)) ==
            SNMP_EQUAL)
        {
            return pSnmpTgtAddrEntry;
        }
    }
    return NULL;
}

/*********************************************************************
*  Function Name : SNMPCreateTgtAddrEntry 
*  Description   : Function to Create an Target Address Entry  
*  Parameter(s)  : pTgtAddrName - Target Address Name 
*  Return Values : On Succes  - return the Entry
*                  On Failure - return NULL
*********************************************************************/

tSnmpTgtAddrEntry  *
SNMPCreateTgtAddrEntry (tSNMP_OCTET_STRING_TYPE * pTgtAddrName)
{
    UINT4               u4Index = SNMP_ZERO;
    if (SNMPGetTgtAddrEntry (pTgtAddrName) != NULL)
    {
        return NULL;
    }
    for (u4Index = SNMP_ZERO; u4Index < MAX_TARGET_ADDR_ENTRY; u4Index++)
    {
        if (gSnmpTgtAddrTable[u4Index].i4Status == SNMP_INACTIVE)
        {
            gSnmpTgtAddrTable[u4Index].i4Status = UNDER_CREATION;
            MEMSET (gSnmpTgtAddrTable[u4Index].AddrName.pu1_OctetList, 0,
                    SNMP_MAX_OCTETSTRING_SIZE);
            MEMSET (gSnmpTgtAddrTable[u4Index].Address.pu1_OctetList, 0,
                    SNMP_MAX_OCTETSTRING_SIZE);
            MEMSET (gSnmpTgtAddrTable[u4Index].TagList.pu1_OctetList, 0,
                    SNMP_MAX_OCTETSTRING_SIZE);
            MEMSET (gSnmpTgtAddrTable[u4Index].AddrParams.pu1_OctetList, 0,
                    SNMP_MAX_OCTETSTRING_SIZE);
            gSnmpTgtAddrTable[u4Index].Address.i4_Length = SNMP_ZERO;
            gSnmpTgtAddrTable[u4Index].i4TimeOut = SNMP_INFM_DEF_TIMEOUT_VAL;
            gSnmpTgtAddrTable[u4Index].i4RetryCount = SNMP_INFM_DEF_RETRY_VAL;
            gSnmpTgtAddrTable[u4Index].TagList.i4_Length = SNMP_ZERO;
            gSnmpTgtAddrTable[u4Index].AddrParams.i4_Length = SNMP_ZERO;
            gSnmpTgtAddrTable[u4Index].Domain.u4_Length = SNMP_ZERO;
            gSnmpTgtAddrTable[u4Index].i4StorageType =
                SNMP3_STORAGE_TYPE_NONVOLATILE;
            SNMPCopyOctetString (&(gSnmpTgtAddrTable[u4Index].AddrName),
                                 pTgtAddrName);
            MEMSET (gSnmpTgtAddrTable[u4Index].pInformStat, SNMP_ZERO,
                    sizeof (tSNMP_INFORM_STAT));
            SNMPAddTgtAddrSll (&(gSnmpTgtAddrTable[u4Index].Link));
            return (&(gSnmpTgtAddrTable[u4Index]));
        }
    }
    return NULL;
}

/*********************************************************************
*  Function Name : SNMPDeleteTgtAddrEntry 
*  Description   : Function to delete the Target Addr Entries 
*  Parameter(s)  : pTgtAddrName - Target Addr Name
*  Return Values : SNMP_SUCCESS/SNMP_FAILURE 
*********************************************************************/

INT4
SNMPDeleteTgtAddrEntry (tSNMP_OCTET_STRING_TYPE * pTgtAddrName)
{
    tTMO_SLL_NODE      *pLstNode = NULL;
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    tInformNode        *pNode = NULL;
    tTMO_SLL           *pInformList = NULL;

    TMO_SLL_Scan (&gSnmpTgtAddrSll, pLstNode, tTMO_SLL_NODE *)
    {
        pSnmpTgtAddrEntry = (tSnmpTgtAddrEntry *) pLstNode;
        if (SNMPCompareOctetString (pTgtAddrName,
                                    &(pSnmpTgtAddrEntry->AddrName)) ==
            SNMP_EQUAL)
        {
            pSnmpTgtAddrEntry->i4Status = SNMP_INACTIVE;
            /* Scan on all the Trap Manager entries for Inform List */
            pInformList = &(pSnmpTgtAddrEntry->pInformList);

            if (pInformList != NULL)
            {
                /* Scan through the InformList  */
                while ((pNode = (tInformNode *) TMO_SLL_First (pInformList))
                       != NULL)
                {
                    SnmpRemoveInformRequestMsgNode (pNode->pInformNode);
                    pNode->pInformNode = NULL;

                    TmrStopTimer (SnmpAgtTimerListId,
                                  &(pNode->InformTmr.TmrInform));
                    TMO_SLL_Delete (pInformList, &pNode->NextEntry);
                    MemReleaseMemBlock (gSnmpInformNodePoolId, (UINT1 *) pNode);
                    pNode = NULL;
                }
            }
            SNMPDelTgtAddrSll (&(pSnmpTgtAddrEntry->Link));
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/*********************************************************************
*  Function Name : SNMPGetFirstTgtAddrEntry 
*  Description   : Function to get the first entry from Target Address Table 
*  Parameter(s)  : None
*  Return Values : Target Entry 
*********************************************************************/

tSnmpTgtAddrEntry  *
SNMPGetFirstTgtAddrEntry ()
{
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    pSnmpTgtAddrEntry = (tSnmpTgtAddrEntry *) TMO_SLL_First (&gSnmpTgtAddrSll);
    return pSnmpTgtAddrEntry;

}

/*********************************************************************
*  Function Name : SNMPGetNextTgtAddrEntry 
*  Description   : Function to get the next entry from target address table 
*  Parameter(s)  : pTgtAddrName - Target Address Name
*  Return Values : Target Address Entry
*********************************************************************/

tSnmpTgtAddrEntry  *
SNMPGetNextTgtAddrEntry (tSNMP_OCTET_STRING_TYPE * pTgtAddrName)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;

    TMO_SLL_Scan (&gSnmpTgtAddrSll, pListNode, tTMO_SLL_NODE *)
    {
        pSnmpTgtAddrEntry = (tSnmpTgtAddrEntry *) pListNode;

        if (SNMPCompareImpliedOctetString
            (pTgtAddrName, &(pSnmpTgtAddrEntry->AddrName)) == SNMP_LESSER)
        {
            return pSnmpTgtAddrEntry;
        }
    }
    return NULL;
}

/*********************************************************************
*  Function Name : SNMPAddTgtAddrSll 
*  Description   : Function to add the node to the target address entry list 
*  Parameter(s)  : pNode - Node to be added
*  Return Values : None
*********************************************************************/

VOID
SNMPAddTgtAddrSll (tTMO_SLL_NODE * pNode)
{
    tSnmpTgtAddrEntry  *pCurrEntry = NULL;
    tSnmpTgtAddrEntry  *pInEntry = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pPrevLstNode = NULL;

    pInEntry = (tSnmpTgtAddrEntry *) pNode;
    TMO_SLL_Scan (&gSnmpTgtAddrSll, pLstNode, tTMO_SLL_NODE *)
    {
        pCurrEntry = (tSnmpTgtAddrEntry *) pLstNode;

        if (SNMPCompareImpliedOctetString (&(pInEntry->AddrName),
                                           &(pCurrEntry->AddrName)) ==
            SNMP_LESSER)
        {
            break;
        }
        pPrevLstNode = pLstNode;
    }
    TMO_SLL_Insert (&gSnmpTgtAddrSll, pPrevLstNode, pNode);

}

/*********************************************************************
*  Function Name : SNMPDelTgtAddrSll 
*  Description   : Function to delete the target address entry from the list 
*  Parameter(s)  : pNode - Node to be deleted
*  Return Values : None
*********************************************************************/

VOID
SNMPDelTgtAddrSll (tTMO_SLL_NODE * pNode)
{
    TMO_SLL_Delete (&gSnmpTgtAddrSll, pNode);
    return;
}

/*********************************************************************
*  Function Name : SNMPTgtParamTableInit 
*  Description   : Initialise the Target Address Param Table 
*  Parameter(s)  : None
*  Return Values : None
*********************************************************************/

INT4
SNMPTgtParamTableInit ()
{
    UINT4               u4Index = SNMP_ZERO;
    MEMSET (gSnmpTgtParamTable, SNMP_ZERO, sizeof (gSnmpTgtParamTable));
    MEMSET (gau1ParamName, SNMP_ZERO, sizeof (gau1ParamName));
    MEMSET (gau1ParamSecName, SNMP_ZERO, sizeof (gau1ParamSecName));
    TMO_SLL_Init (&gSnmpTgtParamSll);
    for (u4Index = SNMP_ZERO; u4Index < SNMP_MAX_TGT_PARAM_ENTRY; u4Index++)
    {
        gSnmpTgtParamTable[u4Index].ParamName.pu1_OctetList =
            gau1ParamName[u4Index];
        gSnmpTgtParamTable[u4Index].ParamSecName.pu1_OctetList =
            gau1ParamSecName[u4Index];
        gSnmpTgtParamTable[u4Index].i2Status = SNMP_INACTIVE;
    }
    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : SNMPGetTgtParamEntry 
*  Description   : Function to get the target address param entry 
*  Parameter(s)  : pTgtParamName - Target Param Name
*  Return Values : TargetParamEntry 
*********************************************************************/

tSnmpTgtParamEntry *
SNMPGetTgtParamEntry (tSNMP_OCTET_STRING_TYPE * pTgtParamName)
{
    tTMO_SLL_NODE      *pLstNode = NULL;
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    TMO_SLL_Scan (&gSnmpTgtParamSll, pLstNode, tTMO_SLL_NODE *)
    {
        pSnmpTgtParamEntry = (tSnmpTgtParamEntry *) pLstNode;
        if (SNMPCompareOctetString (pTgtParamName,
                                    &(pSnmpTgtParamEntry->ParamName)) ==
            SNMP_EQUAL)
        {
            return pSnmpTgtParamEntry;
        }
    }
    return NULL;
}

/*********************************************************************
*  Function Name : SNMPCreateTgtParamEntry 
*  Description   : Function to create an entry from target param table 
*  Parameter(s)  : pTgtParamName - Target Param Name 
*  Return Values : TargetParamEntry/NULL 
*********************************************************************/

tSnmpTgtParamEntry *
SNMPCreateTgtParamEntry (tSNMP_OCTET_STRING_TYPE * pTgtParamName)
{
    UINT4               u4Index = SNMP_ZERO;
    if (SNMPGetTgtParamEntry (pTgtParamName) != NULL)
    {
        return NULL;
    }
    for (u4Index = SNMP_ZERO; u4Index < SNMP_MAX_TGT_PARAM_ENTRY; u4Index++)
    {
        if (gSnmpTgtParamTable[u4Index].i2Status == SNMP_INACTIVE)
        {
            gSnmpTgtParamTable[u4Index].i2Status = UNDER_CREATION;
            gSnmpTgtParamTable[u4Index].ParamSecName.i4_Length = SNMP_ZERO;
            gSnmpTgtParamTable[u4Index].i2ParamMPModel = SNMP_ZERO;
            gSnmpTgtParamTable[u4Index].i2ParamSecModel = SNMP_ZERO;
            gSnmpTgtParamTable[u4Index].i4ParamSecLevel = SNMP_NOAUTH_NOPRIV;
            gSnmpTgtParamTable[u4Index].i2ParamStorageType =
                SNMP3_STORAGE_TYPE_NONVOLATILE;
            gSnmpTgtParamTable[u4Index].pFilterProfileTable = NULL;
            SNMPCopyOctetString (&(gSnmpTgtParamTable[u4Index].ParamName),
                                 pTgtParamName);
            SNMPAddTgtParamSll (&(gSnmpTgtParamTable[u4Index].Link));
            return (&(gSnmpTgtParamTable[u4Index]));
        }
    }
    return NULL;
}

/*********************************************************************
*  Function Name : SNMPDeleteTgtParamEntry 
*  Description   : Function to delete the target param entry 
*  Parameter(s)  : pTgtParamName - Param Name
*  Return Values : SNMP_SUCCESS/SNMP_FAILURE
*********************************************************************/

INT4
SNMPDeleteTgtParamEntry (tSNMP_OCTET_STRING_TYPE * pTgtParamName)
{
    tTMO_SLL_NODE      *pLstNode = NULL;
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    TMO_SLL_Scan (&gSnmpTgtParamSll, pLstNode, tTMO_SLL_NODE *)
    {
        pSnmpTgtParamEntry = (tSnmpTgtParamEntry *) pLstNode;
        if (SNMPCompareOctetString (pTgtParamName,
                                    &(pSnmpTgtParamEntry->ParamName)) ==
            SNMP_EQUAL)
        {
            pSnmpTgtParamEntry->i2Status = SNMP_INACTIVE;
            SNMPDelTgtParamSll (&(pSnmpTgtParamEntry->Link));
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/*********************************************************************
*  Function Name : SNMPGetFirstTgtParamEntry 
*  Description   : Function to get the first target param entry 
*  Parameter(s)  : None
*  Return Values : Target Param Entry 
*********************************************************************/

tSnmpTgtParamEntry *
SNMPGetFirstTgtParamEntry ()
{
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    pSnmpTgtParamEntry =
        (tSnmpTgtParamEntry *) TMO_SLL_First (&gSnmpTgtParamSll);
    return pSnmpTgtParamEntry;
}

/*********************************************************************
*  Function Name : SNMPGetNextTgtParamEntry
*  Description   : Function to get the next entry from target address param
*                  table 
*  Parameter(s)  : pTgtParamName - Target param Name
*  Return Values : TargetParamEntry/NULL
*********************************************************************/

tSnmpTgtParamEntry *
SNMPGetNextTgtParamEntry (tSNMP_OCTET_STRING_TYPE * pTgtParamName)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;

    TMO_SLL_Scan (&gSnmpTgtParamSll, pListNode, tTMO_SLL_NODE *)
    {
        pSnmpTgtParamEntry = (tSnmpTgtParamEntry *) pListNode;

        if (SNMPCompareImpliedOctetString
            (pTgtParamName, (&pSnmpTgtParamEntry->ParamName)) == SNMP_LESSER)
        {
            return pSnmpTgtParamEntry;
        }
    }
    return NULL;
}

/*********************************************************************
*  Function Name : SNMPAddTgtParamSll
*  Description   : Function to add the target param entry to the list 
*  Parameter(s)  : pNode - Node to be added
*  Return Values : None
*********************************************************************/

VOID
SNMPAddTgtParamSll (tTMO_SLL_NODE * pNode)
{
    tSnmpTgtParamEntry *pCurrEntry = NULL;
    tSnmpTgtParamEntry *pInEntry = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pPrevLstNode = NULL;

    pInEntry = (tSnmpTgtParamEntry *) pNode;
    TMO_SLL_Scan (&gSnmpTgtParamSll, pLstNode, tTMO_SLL_NODE *)
    {
        pCurrEntry = (tSnmpTgtParamEntry *) pLstNode;
        if (SNMPCompareImpliedOctetString (&(pInEntry->ParamName),
                                           &(pCurrEntry->ParamName)) ==
            SNMP_LESSER)
        {
            break;
        }
        pPrevLstNode = pLstNode;
    }
    TMO_SLL_Insert (&gSnmpTgtParamSll, pPrevLstNode, pNode);

}

/*********************************************************************
*  Function Name : SNMPDelTgtParamSll
*  Description   : 
*  Parameter(s)  : None
*  Return Values : None
*********************************************************************/

VOID
SNMPDelTgtParamSll (tTMO_SLL_NODE * pNode)
{
    TMO_SLL_Delete (&gSnmpTgtParamSll, pNode);
    return;
}

/*********************************************************************
*  Function Name : SNMPGetTgtAddrTagEntry
*  Description   : Function to get the  target address tag entry
*  Parameter(s)  : pTag - Tag
*                  pSnmpTgtParamEntry - Target Param Entry 
*  Return Values : Target Tag Entry/NULL 
*********************************************************************/

tSnmpTgtAddrEntry  *
SNMPGetTgtAddrTagEntry (tSNMP_OCTET_STRING_TYPE * pTag,
                        tSnmpTgtAddrEntry * pSnmpTgtAddrEntry)
{
    UINT4               u4Index = SNMP_ZERO, u4StartIndex = SNMP_ZERO;
    if (pSnmpTgtAddrEntry != NULL)
    {
        for (u4StartIndex = SNMP_ZERO; u4StartIndex < MAX_TARGET_ADDR_ENTRY;
             u4StartIndex++)
        {
            if (pSnmpTgtAddrEntry == (&gSnmpTgtAddrTable[u4StartIndex]))
            {
                u4StartIndex++;
                break;
            }
        }
    }
    for (u4Index = u4StartIndex; u4Index < MAX_TARGET_ADDR_ENTRY; u4Index++)
    {
        if (gSnmpTgtAddrTable[u4Index].i4Status == SNMP_ACTIVE)
        {
            if ((gSnmpTgtAddrTable[u4Index].TagList.i4_Length ==
                 pTag->i4_Length)
                &&
                (MEMCMP
                 (gSnmpTgtAddrTable[u4Index].TagList.pu1_OctetList,
                  pTag->pu1_OctetList, pTag->i4_Length) == 0))
            {
                break;
            }
        }
    }
    if (u4Index != MAX_TARGET_ADDR_ENTRY)
    {
        return (&gSnmpTgtAddrTable[u4Index]);
    }
    return NULL;
}

/********************  END OF FILE   ***************************************/
