/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: vacmcxt.c,v 1.6 2015/04/28 12:35:03 siva Exp $
 *
 * Description: routines for VACM Context Table Database Model 
 *******************************************************************/
#include "snmpcmn.h"
#include "vacmcxt.h"
/*********************************************************************
*  Function Name : 
*  Description   : 
*  Parameter(s)  : 
*  Return Values : 
*********************************************************************/

VOID
VACMContextInit (VOID)
{
    INT4                i4Index = SNMP_ZERO;
    TMO_SLL_Init (&gSnmpContext);
    MEMSET (gaContext, SNMP_ZERO, sizeof (gaContext));
    MEMSET (gau1CxtName, SNMP_ZERO, sizeof (gau1CxtName));
    for (i4Index = SNMP_ZERO; i4Index < MAX_SNMP_CONTEXT; i4Index++)
    {
        gaContext[i4Index].ContextName.pu1_OctetList = gau1CxtName[i4Index];
    }
}

/*********************************************************************
*  Function Name : 
*  Description   : 
*  Parameter(s)  : 
*  Return Values : 
*********************************************************************/

INT4
VACMAddContext (tSNMP_OCTET_STRING_TYPE * pName)
{
    UINT4               u4VcId = 0;
    INT4                i4Index = SNMP_ZERO;
    if (VACMGetContextNum (pName, &u4VcId) == SNMP_SUCCESS)
    {
        /* Context is already added to VACM */
        return SNMP_SUCCESS;
    }
    for (i4Index = SNMP_ZERO; i4Index < MAX_SNMP_CONTEXT; i4Index++)
    {
        if (gaContext[i4Index].ContextName.i4_Length == SNMP_ZERO)
        {
            break;
        }
    }

    if (i4Index < MAX_SNMP_CONTEXT)
    {
        SNMPCopyOctetString (&(gaContext[i4Index].ContextName), pName);
        VACMAddContextSll (&(gaContext[i4Index].link));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/*********************************************************************
*  Function Name : 
*  Description   : 
*  Parameter(s)  : 
*  Return Values : 
*********************************************************************/
VOID
VACMAddContextSll (tTMO_SLL_NODE * pNode)
{
    tContext           *pCurContext = NULL;
    tContext           *pInContext = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pPrevNode = NULL;

    pInContext = (tContext *) pNode;
    TMO_SLL_Scan (&gSnmpContext, pLstNode, tTMO_SLL_NODE *)
    {
        pCurContext = (tContext *) pLstNode;
        if (SNMPCompareImpliedOctetString (&(pInContext->ContextName),
                                           &(pCurContext->ContextName)) ==
            SNMP_LESSER)
        {
            break;
        }
        pPrevNode = pLstNode;
    }
    TMO_SLL_Insert (&gSnmpContext, pPrevNode, pNode);
}

/*********************************************************************
*  Function Name : 
*  Description   : 
*  Parameter(s)  : 
*  Return Values : 
*********************************************************************/

tContext           *
VACMGetContext (tSNMP_OCTET_STRING_TYPE * pName)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tContext           *pContext = NULL;
    TMO_SLL_Scan (&gSnmpContext, pListNode, tTMO_SLL_NODE *)
    {
        pContext = (tContext *) pListNode;
        if (SNMPCompareOctetString (pName, &(pContext->ContextName)) ==
            SNMP_EQUAL)
        {
            return pContext;
        }
    }
    return NULL;
}

/*********************************************************************
*  Function Name : VACMGetContextNum 
*  Description   : This function provides the context number 
*                  for the given context name
*  Input         : pName -Context Name  
*  Output        : pu4VcNum - Context Number
*  Return Values : SNMP_SUCCESS/SNMP_FAILURE 
*********************************************************************/

INT4
VACMGetContextNum (tSNMP_OCTET_STRING_TYPE * pName, UINT4 *pu4VcNum)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tContext           *pContext = NULL;
    TMO_SLL_Scan (&gSnmpContext, pListNode, tTMO_SLL_NODE *)
    {
        pContext = (tContext *) pListNode;
        if (SNMPCompareOctetString (pName, &(pContext->ContextName)) ==
            SNMP_EQUAL)
        {
            *pu4VcNum = pContext->u4VcNum;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/*********************************************************************
*  Function Name : 
*  Description   : 
*  Parameter(s)  : 
*  Return Values : 
*********************************************************************/
tContext           *
VACMGetFirstContext ()
{
    tContext           *pContext = NULL;
    pContext = (tContext *) TMO_SLL_First (&gSnmpContext);
    return pContext;
}

/*********************************************************************
*  Function Name : 
*  Description   : 
*  Parameter(s)  : 
*  Return Values : 
*********************************************************************/
tContext           *
VACMGetNextContext (tSNMP_OCTET_STRING_TYPE * pContext)
{
    tTMO_SLL_NODE      *pNode = NULL;
    tContext           *pTempContext = NULL;

    pTempContext = VACMGetContext (pContext);
    if (pTempContext == NULL)
    {
        return NULL;
    }
    pNode = &(pTempContext->link);
    if (pNode != NULL && pNode != gSnmpContext.Tail)
    {
        return ((tContext *) pNode->pNext);
    }
    return NULL;
}

/*********************************************************************
*  Function Name : SnmpCreateVirtualContext 
*  Description   : This function creates virtual context in snmp
*  Parameter(s)  : u4ContextId - Context Id
*                  pName       - Context Name
*  Return Values : SNMP_FAILURE/SNMP_SUCCESS
*********************************************************************/
UINT4
SnmpCreateVirtualContext (UINT4 u4ContextId, tSNMP_OCTET_STRING_TYPE * pName)
{
    if (u4ContextId >= MAX_SNMP_CONTEXT)
    {
        /* Context ID is greater than the Max Context ID supported inSNMPV3 */
        return SNMP_FAILURE;
    }
    if (u4ContextId == SNMP_ZERO)
    {
        /* check whether virtual context id is default context.   
         * default context will be created during snmpv3 initialization */
        return SNMP_SUCCESS;
    }
    if (gaContext[u4ContextId].ContextName.i4_Length != SNMP_ZERO)
    {
        if (SNMPCompareOctetString (pName,
                                    &(gaContext[u4ContextId].ContextName))
            == SNMP_EQUAL)
        {
            /* Context has been already created */
            return SNMP_SUCCESS;
        }
        return SNMP_FAILURE;
    }

    SNMPCopyOctetString (&(gaContext[u4ContextId].ContextName), pName);
    gaContext[u4ContextId].u4VcNum = u4ContextId;
    VACMAddContextSll (&(gaContext[u4ContextId].link));
    return SNMP_SUCCESS;

}

/*********************************************************************
*  Function Name : SnmpUpdateAliasName 
*  Description   : This function creates virtual context in snmp
*  Parameter(s)  : u4ContextId - Context Id
*                  pName       - Context Name
*  Return Values : SNMP_FAILURE/SNMP_SUCCESS
*********************************************************************/
UINT4
SnmpUpdateAliasName (UINT4 u4ContextId, tSNMP_OCTET_STRING_TYPE * pName)
{
    if (u4ContextId >= MAX_SNMP_CONTEXT)
    {
        /* Context ID is greater than the Max Context ID supported inSNMPV3 */
        return SNMP_FAILURE;
    }
    if (u4ContextId == SNMP_DEFAULT_CONTEXT)
    {
        /* No need to create default community. This will be
         * taken care in snmp initialisation itself
         */
        return SNMP_SUCCESS;
    }
    if (SnmpDeleteVirtualContext (u4ContextId) != SNMP_SUCCESS)
    {
        /* Failed to delete the virtual context in SNMPv3 module. */
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (&(gaContext[u4ContextId].ContextName), pName);
    gaContext[u4ContextId].u4VcNum = u4ContextId;
    VACMAddContextSll (&(gaContext[u4ContextId].link));

    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : SnmpDeleteVirtualContext 
*  Description   : This function deletes virtual context in snmp
*  Parameter(s)  : u4ContextId - Context Id
*  Return Values : SNMP_FAILURE/SNMP_SUCCESS
*********************************************************************/
UINT4
SnmpDeleteVirtualContext (UINT4 u4ContextId)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tContext           *pContext = NULL;
    TMO_SLL_Scan (&gSnmpContext, pListNode, tTMO_SLL_NODE *)
    {
        pContext = (tContext *) pListNode;
        if ((pContext->u4VcNum) == u4ContextId)
        {
            break;
        }
    }
    if (pListNode != NULL)
    {

        TMO_SLL_Delete (&gSnmpContext, pListNode);
        gaContext[u4ContextId].u4VcNum = SNMP_ZERO;
        MEMSET (gaContext[u4ContextId].ContextName.pu1_OctetList, 0,
                gaContext[u4ContextId].ContextName.i4_Length);
        gaContext[u4ContextId].ContextName.i4_Length = SNMP_ZERO;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}
