/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssnmp3lw.h,v 1.6 2015/04/28 12:35:02 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetSnmpInInformResponses ARG_LIST((UINT4 *));

INT1
nmhGetSnmpOutInformRequests ARG_LIST((UINT4 *));

INT1
nmhGetSnmpInformDrops ARG_LIST((UINT4 *));

INT1
nmhGetSnmpInformAwaitingAck ARG_LIST((UINT4 *));

INT1
nmhGetSnmpListenTrapPort ARG_LIST((UINT4 *));

INT1
nmhGetSnmpOverTcpStatus ARG_LIST((INT4 *));

INT1
nmhGetSnmpListenTcpPort ARG_LIST((UINT4 *));

INT1
nmhGetSnmpTrapOverTcpStatus ARG_LIST((INT4 *));

INT1
nmhGetSnmpListenTcpTrapPort ARG_LIST((UINT4 *));

INT1
nmhGetFsSnmpEngineID ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */
INT1
nmhSetSnmpListenTrapPort ARG_LIST((UINT4 ));

INT1
nmhSetSnmpOverTcpStatus ARG_LIST((INT4 ));

INT1
nmhSetSnmpListenTcpPort ARG_LIST((UINT4 ));

INT1
nmhSetSnmpTrapOverTcpStatus ARG_LIST((INT4 ));

INT1
nmhSetSnmpListenTcpTrapPort ARG_LIST((UINT4 ));

INT1
nmhSetFsSnmpEngineID ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2SnmpListenTrapPort ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2SnmpOverTcpStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2SnmpListenTcpPort ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2SnmpTrapOverTcpStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2SnmpListenTcpTrapPort ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsSnmpEngineID ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2SnmpListenTrapPort ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2SnmpOverTcpStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2SnmpListenTcpPort ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2SnmpTrapOverTcpStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2SnmpListenTcpTrapPort ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSnmpEngineID ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for SnmpInformCntTable. */
INT1
nmhValidateIndexInstanceSnmpInformCntTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for SnmpInformCntTable  */

INT1
nmhGetFirstIndexSnmpInformCntTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexSnmpInformCntTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetSnmpInformSent ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetSnmpInformAwaitAck ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetSnmpInformRetried ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetSnmpInformDropped ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetSnmpInformFailed ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetSnmpInformResponses ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetSnmpColdStartTrapControl ARG_LIST((INT4 *));

INT1
nmhGetSnmpAgentControl ARG_LIST((INT4 *));

INT1
nmhGetSnmpAllowedPduVersions ARG_LIST((INT4 *));

INT1
nmhGetSnmpMinimumSecurityRequired ARG_LIST((INT4 *));

INT1
nmhGetSnmpInRollbackErrs ARG_LIST((UINT4 *));

INT1
nmhGetSnmpProxyListenTrapPort ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetSnmpColdStartTrapControl ARG_LIST((INT4 ));

INT1
nmhSetSnmpAgentControl ARG_LIST((INT4 ));

INT1
nmhSetSnmpAllowedPduVersions ARG_LIST((INT4 ));

INT1
nmhSetSnmpMinimumSecurityRequired ARG_LIST((INT4 ));

INT1
nmhSetSnmpProxyListenTrapPort ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2SnmpColdStartTrapControl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2SnmpAgentControl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2SnmpAllowedPduVersions ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2SnmpMinimumSecurityRequired ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2SnmpProxyListenTrapPort ARG_LIST((UINT4 *  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2SnmpColdStartTrapControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2SnmpAgentControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2SnmpAllowedPduVersions ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2SnmpMinimumSecurityRequired ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2SnmpProxyListenTrapPort ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsSnmpProxyTable. */
INT1
nmhValidateIndexInstanceFsSnmpProxyTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsSnmpProxyTable  */

INT1
nmhGetFirstIndexFsSnmpProxyTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsSnmpProxyTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSnmpProxyMibType ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsSnmpProxyMibId ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OID_TYPE * ));

INT1
nmhGetFsSnmpProxyMibTargetParamsIn ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsSnmpProxyMibSingleTargetOut ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsSnmpProxyMibMultipleTargetOut ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsSnmpProxyMibStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsSnmpProxyMibRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSnmpProxyMibType ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsSnmpProxyMibId ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OID_TYPE *));

INT1
nmhSetFsSnmpProxyMibTargetParamsIn ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsSnmpProxyMibSingleTargetOut ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsSnmpProxyMibMultipleTargetOut ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsSnmpProxyMibStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsSnmpProxyMibRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSnmpProxyMibType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsSnmpProxyMibId ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OID_TYPE *));

INT1
nmhTestv2FsSnmpProxyMibTargetParamsIn ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsSnmpProxyMibSingleTargetOut ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsSnmpProxyMibMultipleTargetOut ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsSnmpProxyMibStorageType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsSnmpProxyMibRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSnmpProxyTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSnmpListenAgentPort ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSnmpListenAgentPort ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSnmpListenAgentPort ARG_LIST((UINT4 *  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSnmpListenAgentPort ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetSnmpAgentxTransportDomain ARG_LIST((INT4 *));

INT1
nmhGetSnmpAgentxMasterAgentAddr ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetSnmpAgentxMasterAgentPortNo ARG_LIST((UINT4 *));

INT1
nmhGetSnmpAgentxSubAgentInPkts ARG_LIST((UINT4 *));

INT1
nmhGetSnmpAgentxSubAgentOutPkts ARG_LIST((UINT4 *));

INT1
nmhGetSnmpAgentxSubAgentPktDrops ARG_LIST((UINT4 *));

INT1
nmhGetSnmpAgentxSubAgentParseDrops ARG_LIST((UINT4 *));

INT1
nmhGetSnmpAgentxSubAgentInOpenFail ARG_LIST((UINT4 *));

INT1
nmhGetSnmpAgentxSubAgentOpenPktCnt ARG_LIST((UINT4 *));

INT1
nmhGetSnmpAgentxSubAgentInClosePktCnt ARG_LIST((UINT4 *));

INT1
nmhGetSnmpAgentxSubAgentOutClosePktCnt ARG_LIST((UINT4 *));

INT1
nmhGetSnmpAgentxSubAgentIdAllocPktCnt ARG_LIST((UINT4 *));

INT1
nmhGetSnmpAgentxSubAgentIdDllocPktCnt ARG_LIST((UINT4 *));

INT1
nmhGetSnmpAgentxSubAgentRegPktCnt ARG_LIST((UINT4 *));

INT1
nmhGetSnmpAgentxSubAgentUnRegPktCnt ARG_LIST((UINT4 *));

INT1
nmhGetSnmpAgentxSubAgentAddCapsCnt ARG_LIST((UINT4 *));

INT1
nmhGetSnmpAgentxSubAgentRemCapsCnt ARG_LIST((UINT4 *));

INT1
nmhGetSnmpAgentxSubAgentNotifyPktCnt ARG_LIST((UINT4 *));

INT1
nmhGetSnmpAgentxSubAgentPingCnt ARG_LIST((UINT4 *));

INT1
nmhGetSnmpAgentxSubAgentInGets ARG_LIST((UINT4 *));

INT1
nmhGetSnmpAgentxSubAgentInGetNexts ARG_LIST((UINT4 *));

INT1
nmhGetSnmpAgentxSubAgentInGetBulks ARG_LIST((UINT4 *));

INT1
nmhGetSnmpAgentxSubAgentInTestSets ARG_LIST((UINT4 *));

INT1
nmhGetSnmpAgentxSubAgentInCommits ARG_LIST((UINT4 *));

INT1
nmhGetSnmpAgentxSubAgentInCleanups ARG_LIST((UINT4 *));

INT1
nmhGetSnmpAgentxSubAgentInUndos ARG_LIST((UINT4 *));

INT1
nmhGetSnmpAgentxSubAgentOutResponse ARG_LIST((UINT4 *));

INT1
nmhGetSnmpAgentxSubAgentInResponse ARG_LIST((UINT4 *));

INT1
nmhGetSnmpAgentxSubAgentControl ARG_LIST((INT4 *));

INT1
nmhGetSnmpAgentxContextName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetSnmpAgentxTransportDomain ARG_LIST((INT4 ));

INT1
nmhSetSnmpAgentxMasterAgentAddr ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetSnmpAgentxMasterAgentPortNo ARG_LIST((UINT4 ));

INT1
nmhSetSnmpAgentxSubAgentControl ARG_LIST((INT4 ));

INT1
nmhSetSnmpAgentxContextName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2SnmpAgentxTransportDomain ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2SnmpAgentxMasterAgentAddr ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2SnmpAgentxMasterAgentPortNo ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2SnmpAgentxSubAgentControl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2SnmpAgentxContextName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2SnmpAgentxTransportDomain ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2SnmpAgentxMasterAgentAddr ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2SnmpAgentxMasterAgentPortNo ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2SnmpAgentxSubAgentControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2SnmpAgentxContextName ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsSnmpTrapFilterTable. */
INT1
nmhValidateIndexInstanceFsSnmpTrapFilterTable ARG_LIST((tSNMP_OID_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsSnmpTrapFilterTable  */

INT1
nmhGetFirstIndexFsSnmpTrapFilterTable ARG_LIST((tSNMP_OID_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsSnmpTrapFilterTable ARG_LIST((tSNMP_OID_TYPE *, tSNMP_OID_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSnmpTrapFilterRowStatus ARG_LIST((tSNMP_OID_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSnmpTrapFilterRowStatus ARG_LIST((tSNMP_OID_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSnmpTrapFilterRowStatus ARG_LIST((UINT4 *  ,tSNMP_OID_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSnmpTrapFilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsSnmpTargetAddrTable. */
INT1
nmhValidateIndexInstanceFsSnmpTargetAddrTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsSnmpTargetAddrTable  */

INT1
nmhGetFirstIndexFsSnmpTargetAddrTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsSnmpTargetAddrTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSnmpTargetHostName ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSnmpTargetHostName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSnmpTargetHostName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSnmpTargetAddrTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
