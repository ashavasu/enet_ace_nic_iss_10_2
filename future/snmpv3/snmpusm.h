/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: snmpusm.h,v 1.10 2017/11/20 13:11:26 siva Exp $
 *
 * Description:macros and prototypes for snmpusm.c
 *******************************************************************/

#ifndef _SNMPUSM_H
#define _SNMPUSM_H

#define SNMP_MAX_PASSWD_LEN   64
#define SNMP_KEY_MAX_PASSWD_LEN 150
#define SNMP_PASSWD_TO_STRING_LEN 1048576

#define SNMP_AUTH_MD5_KEY_LEN  16
#define SNMP_AUTH_SHA_KEY_LEN  20
#define SNMP_AUTH_SHA256_KEY_LEN  32
#define SNMP_AUTH_SHA384_KEY_LEN  48
#define SNMP_AUTH_SHA512_KEY_LEN  64


UINT1 gau1SnmpEngineID[SNMP_MAX_ENGINE_ID_LEN] = 
              {0x80,0x00,0x08,0x1c,0x04,0x46,0x53};

UINT1 gau1LocalSnmpEngineID[SNMP_MAX_ENGINE_ID_LEN]; 

UINT4 gu4SnmpEngineBootCount = 0;

tSnmpUsmStats gSnmpUsmStats;
tSNMP_OCTET_STRING_TYPE gSnmpEngineID;
tSNMP_OCTET_STRING_TYPE gSnmpDefaultEngineID;

tSnmpUsmEntry gSnmpUsmTable[SNMP_MAX_USM_ENTRY];

UINT1 au1UsmEngineID[SNMP_MAX_USM_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
UINT1 au1UsmName[SNMP_MAX_USM_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
UINT1 au1UsmSecName[SNMP_MAX_USM_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
UINT1 au1UsmAuthKeyChange[SNMP_MAX_USM_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
UINT1 au1UsmOwnAuthKeyChange[SNMP_MAX_USM_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
UINT1 au1UsmPrivKeyChange[SNMP_MAX_USM_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
UINT1 au1UsmOwnPrivKeyChange[SNMP_MAX_USM_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
UINT1 au1UsmPublic[SNMP_MAX_USM_ENTRY][SNMP_MAX_OCTETSTRING_SIZE];
UINT1 au1UsmUserAuthPassword[SNMP_MAX_USM_ENTRY][SNMP_MAX_ADMIN_STR_LENGTH];
UINT1 au1UsmUserPrivPassword[SNMP_MAX_USM_ENTRY][SNMP_MAX_ADMIN_STR_LENGTH];

tTMO_SLL gSnmpUsmSll;

VOID SNMPAddUsmSll(tTMO_SLL_NODE *pNode);
VOID SNMPDelUsmSll(tTMO_SLL_NODE *pNode);
#endif /* _SNMPUSM_H */
