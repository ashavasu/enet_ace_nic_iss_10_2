#ifndef _SNMPROXWR_H
#define _SNMPROXWR_H
INT4 GetNextIndexSnmpProxyTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterSNMPROX(VOID);

VOID UnRegisterSNMPROX(VOID);
INT4 SnmpProxyTypeGet(tSnmpIndex *, tRetVal *);
INT4 SnmpProxyContextEngineIDGet(tSnmpIndex *, tRetVal *);
INT4 SnmpProxyContextNameGet(tSnmpIndex *, tRetVal *);
INT4 SnmpProxyTargetParamsInGet(tSnmpIndex *, tRetVal *);
INT4 SnmpProxySingleTargetOutGet(tSnmpIndex *, tRetVal *);
INT4 SnmpProxyMultipleTargetOutGet(tSnmpIndex *, tRetVal *);
INT4 SnmpProxyStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 SnmpProxyRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 SnmpProxyTypeSet(tSnmpIndex *, tRetVal *);
INT4 SnmpProxyContextEngineIDSet(tSnmpIndex *, tRetVal *);
INT4 SnmpProxyContextNameSet(tSnmpIndex *, tRetVal *);
INT4 SnmpProxyTargetParamsInSet(tSnmpIndex *, tRetVal *);
INT4 SnmpProxySingleTargetOutSet(tSnmpIndex *, tRetVal *);
INT4 SnmpProxyMultipleTargetOutSet(tSnmpIndex *, tRetVal *);
INT4 SnmpProxyStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 SnmpProxyRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 SnmpProxyTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpProxyContextEngineIDTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpProxyContextNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpProxyTargetParamsInTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpProxySingleTargetOutTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpProxyMultipleTargetOutTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpProxyStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpProxyRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpProxyTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);








#endif /* _SNMPROXWR_H */
