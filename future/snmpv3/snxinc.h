/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: snxinc.h,v 1.4 2015/04/28 12:35:03 siva Exp $
 *
 * Description:This file contains the exported definitions and
 *             macros related to redundancy.
 *
 *******************************************************************/
#ifndef SNAGTXINC_H
#define SNAGTXINC_H

#include "lr.h"
#include "fsbuddy.h"
#include "snmctdfs.h"
#include "snmccons.h"
#ifndef _SNMPCMN_H
#include "snmpcmn.h"
#endif
#include "snmpmbdb.h"
#include "snxdef.h"
#include "snxtdfs.h"
#include "snxextn.h"
#include "snxproto.h"
#include "utilcli.h"
#endif
