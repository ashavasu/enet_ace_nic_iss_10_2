/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: vacmsec.c,v 1.10 2017/11/20 13:11:27 siva Exp $
 *
 * Description: routines for VACM Secuiryt Group Table Database Model 
 *******************************************************************/
#include "snmpcmn.h"
#include "snmp3cli.h"
#include "vacmsec.h"

/*********************************************************************
*  Function Name : VACMInitSecGrp 
*  Description   : Initialise the VACM Security Table
*  Parameter(s)  : None 
*  Return Values : None
*********************************************************************/
INT4
VACMInitSecGrp ()
{
    UINT4               u4Index = SNMP_ZERO;
    MEMSET (gaSecGrp, SNMP_ZERO, sizeof (tSecGrp));
    MEMSET (gaSecGrp, SNMP_ZERO, sizeof (gaSecGrp));
    MEMSET (au1SecName, SNMP_ZERO, sizeof (au1SecName));
    MEMSET (au1Group, SNMP_ZERO, sizeof (au1Group));
    TMO_SLL_Init (&gSnmpSecGrp);
    for (u4Index = SNMP_ZERO; u4Index < SNMP_MAX_SEC_GROUP; u4Index++)
    {
        gaSecGrp[u4Index].SecName.pu1_OctetList = au1SecName[u4Index];
        gaSecGrp[u4Index].Group.pu1_OctetList = au1Group[u4Index];
        gaSecGrp[u4Index].i4Status = SNMP_INACTIVE;
    }
    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : VACMAddSecGrp 
*  Description   : Function to add an entry to Security Group Table 
*  Parameter(s)  : i4SecModel - Security Model
*                  pSecName   - Secutiry Name
*  Return Values :  SNMP_SUCCESS/SNMP_FAILURE
*********************************************************************/

INT4
VACMAddSecGrp (INT4 i4SecModel, tSNMP_OCTET_STRING_TYPE * pSecName)
{
    UINT4               u4Index = SNMP_ZERO;
    if (VACMGetSecGrp (i4SecModel, pSecName) != NULL)
    {
        return SNMP_FAILURE;
    }
    for (u4Index = 0; u4Index < SNMP_MAX_SEC_GROUP; u4Index++)
    {
        if (gaSecGrp[u4Index].i4Status == SNMP_INACTIVE)
        {
            gaSecGrp[u4Index].i4Status = UNDER_CREATION;
            gaSecGrp[u4Index].i4SecModel = i4SecModel;
            SNMPCopyOctetString (&(gaSecGrp[u4Index].SecName), pSecName);
            gaSecGrp[u4Index].i4Storage = SNMP3_STORAGE_TYPE_NONVOLATILE;
            VACMAddSecGrpSll (&(gaSecGrp[u4Index].link));
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/*********************************************************************
*  Function Name : VACMAddSecGrpSll 
*  Description   : Function to add the node the list
*  Parameter(s)  : pNode - Node to be added
*  Return Values : None 
*********************************************************************/

VOID
VACMAddSecGrpSll (tTMO_SLL_NODE * pNode)
{
    tSecGrp            *pCurEntry = NULL;
    tSecGrp            *pInEntry = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pPrevNode = NULL;
    pInEntry = (tSecGrp *) pNode;

    TMO_SLL_Scan (&gSnmpSecGrp, pLstNode, tTMO_SLL_NODE *)
    {
        pCurEntry = (tSecGrp *) pLstNode;
        if (pCurEntry->i4SecModel > pInEntry->i4SecModel)
        {
            break;
        }
        else if (pCurEntry->i4SecModel < pInEntry->i4SecModel)
        {
            pPrevNode = pLstNode;
            continue;
        }

        if (SNMPCompareOctetString
            (&(pInEntry->SecName), &(pCurEntry->SecName)) == SNMP_LESSER)
        {
            break;
        }
        pPrevNode = pLstNode;
    }
    TMO_SLL_Insert (&gSnmpSecGrp, pPrevNode, pNode);
}

/*********************************************************************
*  Function Name : VACMDeleteSecGrp 
*  Description   : Function to delete the entry from security group table 
*  Parameter(s)  : i4SecModel - Security Model
*                  pSecName   - Security Name
*  Return Values : SNMP_SUCCESS/SNMP_FAILURE 
*********************************************************************/

INT4
VACMDeleteSecGrp (INT4 i4SecModel, tSNMP_OCTET_STRING_TYPE * pSecName)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tTMO_SLL_NODE      *pNextNode = NULL;
    tSecGrp            *pSecEntry = NULL;
    tAccessEntry       *pAccessEntry = NULL;

    TMO_SLL_Scan (&gSnmpSecGrp, pListNode, tTMO_SLL_NODE *)
    {
        pSecEntry = (tSecGrp *) pListNode;
        if ((pSecEntry->i4SecModel == i4SecModel) &&
            (SNMPCompareOctetString (pSecName, &(pSecEntry->SecName)) ==
             SNMP_EQUAL))
        {
            TMO_SLL_Scan (&gSnmpAccessEntry, pNextNode, tTMO_SLL_NODE *)
            {
                pAccessEntry = (tAccessEntry *) pNextNode;
                if (SNMPCompareOctetString (pAccessEntry->pGroup,
                                            &(pSecEntry->Group)) == SNMP_EQUAL)
                {
                    CLI_SET_ERR (CLI_SNMP3_GRP_PRESENT_INACCESS_ERR);
                    return SNMP_FAILURE;
                }
            }
            pSecEntry->i4Status = SNMP_INACTIVE;
            SNMPDelVacmSecGrpSll (&(pSecEntry->link));
            return SNMP_SUCCESS;
        }
    }

    CLI_SET_ERR (CLI_SNMP3_GRP_NOT_PRESENT_ERR);
    return SNMP_FAILURE;
}

/*********************************************************************
*  Function Name : SNMPDelVacmSecGrpSll
*  Description   : Function to delete the node from the list
*  Parameter(s)  : pNode - Node to be deleted 
*  Return Values : None
*********************************************************************/

VOID
SNMPDelVacmSecGrpSll (tTMO_SLL_NODE * pNode)
{
    TMO_SLL_Delete (&gSnmpSecGrp, pNode);
    return;
}

/*********************************************************************
*  Function Name : VACMGetSecGrp 
*  Description   : Function to get the security group entry from the table
*  Parameter(s)  : i4SecModel - Security Model
*                  pSecName   - Security Name
*  Return Values : Entry/NULL
*********************************************************************/

tSecGrp            *
VACMGetSecGrp (INT4 i4SecModel, tSNMP_OCTET_STRING_TYPE * pSecName)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tSecGrp            *pSecEntry = NULL;
    TMO_SLL_Scan (&gSnmpSecGrp, pListNode, tTMO_SLL_NODE *)
    {
        pSecEntry = (tSecGrp *) pListNode;
        if (pSecEntry->i4Status != SNMP_INACTIVE)
        {
            if ((pSecEntry->i4SecModel == i4SecModel) &&
                (SNMPCompareOctetString (pSecName,
                                         &(pSecEntry->SecName)) == SNMP_EQUAL))
            {
                SNMP_TRC1 (SNMP_DEBUG_TRC, "VACMGetSecGrp: "
                           "Successfully getting the group entry for the user [%s]\r\n",
                           pSecName->pu1_OctetList);
                return pSecEntry;
            }
        }
    }
    SNMP_TRC1 (SNMP_FAILURE_TRC, "VACMGetSecGrp: "
               "Failed to get the entry for the user [%s]\r\n",
               pSecName->pu1_OctetList);
    return NULL;
}

/*********************************************************************
*  Function Name : VACMGetFirstSecGrp 
*  Description   : Function to get the first entry from security group table
*  Parameter(s)  : None
*  Return Values : Security group entry
*********************************************************************/

tSecGrp            *
VACMGetFirstSecGrp ()
{
    tSecGrp            *pFirstSecEntry = NULL;
    pFirstSecEntry = (tSecGrp *) TMO_SLL_First (&gSnmpSecGrp);
    return pFirstSecEntry;
}

/*********************************************************************
*  Function Name : VACMGetNextSecGrp
*  Description   : Function to get the next entry from security group table
*  Parameter(s)  : i4SecModel - Security Model
*                  pSecName   - Security Name
*  Return Values : Entry/NULL
*********************************************************************/

tSecGrp            *
VACMGetNextSecGrp (INT4 i4SecModel, tSNMP_OCTET_STRING_TYPE * pSecName)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tSecGrp            *pSecEntry = NULL;

    TMO_SLL_Scan (&gSnmpSecGrp, pListNode, tTMO_SLL_NODE *)
    {
        pSecEntry = (tSecGrp *) pListNode;
        if (i4SecModel > pSecEntry->i4SecModel)
        {
            continue;
        }
        else if (i4SecModel < pSecEntry->i4SecModel)
        {
            return pSecEntry;
        }
        if (SNMPCompareOctetString (pSecName, &(pSecEntry->SecName))
            == SNMP_LESSER)
        {
            return pSecEntry;
        }
    }
    return NULL;
}

/********************  END OF FILE   ***************************************/
