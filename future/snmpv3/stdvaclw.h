/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdvaclw.h,v 1.4 2015/04/28 12:35:03 siva Exp $
*
* Description: Proto types for VACM MIB Low Level  Routines
*********************************************************************/

#ifndef _STDVACLW_H
#define _STDVACLW_H
/* Proto Validate Index Instance for VacmContextTable. */
INT1
nmhValidateIndexInstanceVacmContextTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for VacmContextTable  */

INT1
nmhGetFirstIndexVacmContextTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexVacmContextTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

/* Proto Validate Index Instance for VacmSecurityToGroupTable. */
INT1
nmhValidateIndexInstanceVacmSecurityToGroupTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for VacmSecurityToGroupTable  */

INT1
nmhGetFirstIndexVacmSecurityToGroupTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexVacmSecurityToGroupTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetVacmGroupName ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));


INT1
nmhGetVacmSecurityToGroupStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetVacmGroupName ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetVacmSecurityToGroupStorageType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetVacmSecurityToGroupStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2VacmGroupName ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2VacmSecurityToGroupStorageType ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2VacmSecurityToGroupStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2VacmSecurityToGroupTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for VacmAccessTable. */
INT1
nmhValidateIndexInstanceVacmAccessTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for VacmAccessTable  */

INT1
nmhGetFirstIndexVacmAccessTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexVacmAccessTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetVacmAccessContextMatch ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetVacmAccessReadViewName ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetVacmAccessWriteViewName ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetVacmAccessNotifyViewName ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetVacmAccessStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetVacmAccessStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetVacmAccessContextMatch ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhSetVacmAccessReadViewName ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetVacmAccessWriteViewName ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetVacmAccessNotifyViewName ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetVacmAccessStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhSetVacmAccessStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2VacmAccessContextMatch ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2VacmAccessReadViewName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2VacmAccessWriteViewName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2VacmAccessNotifyViewName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2VacmAccessStorageType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2VacmAccessStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2VacmAccessTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetVacmViewSpinLock ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetVacmViewSpinLock ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2VacmViewSpinLock ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2VacmViewSpinLock ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for VacmViewTreeFamilyTable. */
INT1
nmhValidateIndexInstanceVacmViewTreeFamilyTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OID_TYPE *));

/* Proto Type for Low Level GET FIRST fn for VacmViewTreeFamilyTable  */

INT1
nmhGetFirstIndexVacmViewTreeFamilyTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , tSNMP_OID_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexVacmViewTreeFamilyTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OID_TYPE *, tSNMP_OID_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetVacmViewTreeFamilyMask ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OID_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetVacmViewTreeFamilyType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OID_TYPE *,INT4 *));

INT1
nmhGetVacmViewTreeFamilyStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OID_TYPE *,INT4 *));

INT1
nmhGetVacmViewTreeFamilyStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OID_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetVacmViewTreeFamilyMask ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OID_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetVacmViewTreeFamilyType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OID_TYPE * ,INT4 ));

INT1
nmhSetVacmViewTreeFamilyStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OID_TYPE * ,INT4 ));

INT1
nmhSetVacmViewTreeFamilyStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OID_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2VacmViewTreeFamilyMask ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OID_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2VacmViewTreeFamilyType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OID_TYPE * ,INT4 ));

INT1
nmhTestv2VacmViewTreeFamilyStorageType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OID_TYPE * ,INT4 ));

INT1
nmhTestv2VacmViewTreeFamilyStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OID_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2VacmViewTreeFamilyTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
#endif /* _STDVACLW_H */
