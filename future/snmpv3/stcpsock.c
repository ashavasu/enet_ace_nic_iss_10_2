/*****************************************************************************
 * $Id: stcpsock.c,v 1.1 2015/04/28 12:35:03 siva Exp $
 *
 * Description: SNMPoTCPsocket interface functions.
 ****************************************************************************/

#include "snmpcmn.h"
#include "fssocket.h"

extern tSnmpTcp     gSnmpTcpGblInfo;
/*************************************************************************** 
 * Function           : SnmpTcpSockInit                                    *
 * Input(s)           : u2Port - to bind the socket with ipv4Addr family *
 * Output(s)          : TCP socket descriptor                              *
 * Returns            : SNMP_SUCCESS/SNMP_FAILURE                          *
 * Action             : Routine to create a TCP server scoket,set socket 
                        options                                            *
 *                      for accepting the connections from peer.           *
****************************************************************************/
INT4
SnmpTcpSockInit (UINT2 u2Port)
{
    INT4                i4SockFd = 0;
    INT4                i4Flags = -1;
    INT4                i4RetVal = SNMP_ZERO;
    struct sockaddr_in  serv_addr;

    SNMPTrace ("SnmpTcpSockInit: Entered SnmpTcpSockInit\r\n");

    /* Open the tcp socket */
    i4SockFd = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (i4SockFd < SNMP_ZERO)
    {
        SNMPTrace ("SnmpTcpSockInit-TCP server socket creation failure !!\r\n");
        return SNMP_FAILURE;
    }
    MEMSET (&serv_addr, SNMP_ZERO, sizeof (serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);
    serv_addr.sin_port = OSIX_HTONS (u2Port);

    i4RetVal = bind (i4SockFd, (struct sockaddr *) &serv_addr,
                     sizeof (serv_addr));
    if (i4RetVal < SNMP_ZERO)
    {
        SNMPTrace ("SnmpTcpSockInit: Unable to bind with TCP server"
                   "address and port for socket\r\n");
        close (i4SockFd);
        return SNMP_FAILURE;
    }
    /* Get current socket flags */
    if ((i4Flags = fcntl (i4SockFd, F_GETFL, SNMP_ZERO)) < SNMP_ZERO)
    {
        SNMPTrace ("SnmpTcpSockInit: TCP server Fcntl GET Failure !!!\n");
        close (i4SockFd);
        return SNMP_FAILURE;
    }

    /* Set the socket in non-blocking mode */
    i4Flags |= O_NONBLOCK;
    if (fcntl (i4SockFd, F_SETFL, i4Flags) < SNMP_ZERO)
    {
        SNMPTrace ("SnmpTcpSockInit: TCP server Fcntl SET Failure !!!\n");
        close (i4SockFd);
        return SNMP_FAILURE;
    }
    /* Initializing the port and Binding the socket */
    i4RetVal = listen (i4SockFd, SNMPTCP_MAX_LISTEN);
    if (i4RetVal < SNMP_ZERO)
    {
        SNMPTrace ("SnmpTcpSockInit: Unable to listen for the socket\r\n");
        close (i4SockFd);
        return SNMP_FAILURE;
    }
    gSnmpTcpGblInfo.i4TcpV4SockId = i4SockFd;

    /* Add this SockFd to SELECT library */
    SelAddFd (i4SockFd, SnmpTcpV4NewConnCallBk);
    SNMPTrace ("SnmpTcpSockInit: Leaving SnmpTcpSockInit\r\n");
    return (SNMP_SUCCESS);
}

/******************************************************************************
 * Function           : SnmpTcpSock6Init
 * Input(s)           : u4Port - Port to bind the socket with ipv6 addr family
 * Output(s)          : TCP server socket descriptor
 * Returns            : SNMP_SUCCESS/SNMP_FAILURE
 * Action             : Routine to create a TCP server scoket,set socket options
 *                      for accepting the connections from peer.
 ******************************************************************************/

INT4
SnmpTcpSock6Init (UINT2 u2Port)
{
    INT4                i4SockFd = SNMPTCP_INVALID_SOCKFD;
    INT4                i4Flags = SNMP_MINUS_ONE;
    INT4                i4RetVal = SNMP_ZERO;
    INT4                i4OptVal = OSIX_FALSE;
    struct sockaddr_in6 serv_addr;

    SNMPTrace ("SnmpTcpSock6Init: function  ENTRY \r\n");

    /* Open the tcp socket with INET6 addr family */
    i4SockFd = socket (AF_INET6, SOCK_STREAM, IPPROTO_TCP);
    if (i4SockFd < SNMP_ZERO)
    {
        SNMPTrace ("SnmpTcpSock6Init:TCP server socket creation failure !!!\n");
        return SNMP_FAILURE;
    }

    i4OptVal = OSIX_TRUE;
    if (setsockopt (i4SockFd, IPPROTO_IPV6, IPV6_V6ONLY, &i4OptVal,
                    sizeof (i4OptVal)))
    {
        SNMPTrace ("SnmpTcpSock6Init:Unable to Set Socket Option "
                   "IPV6_V6ONLY !!!\n");
        close (i4SockFd);
        return SNMP_FAILURE;
    }

    /* Get current socket flags */
    if ((i4Flags = fcntl (i4SockFd, F_GETFL, SNMP_ZERO)) < SNMP_ZERO)
    {
        SNMPTrace ("SnmpTcpSock6Init: TCP server Fcntl GET Failure !!!\n");
        close (i4SockFd);
        return SNMP_FAILURE;
    }

    /* Set the socket in non-blocking mode */
    i4Flags |= O_NONBLOCK;
    if (fcntl (i4SockFd, F_SETFL, i4Flags) < SNMP_ZERO)
    {
        SNMPTrace ("SnmpTcpSock6Init: TCP server Fcntl SET Failure !!!\n");
        close (i4SockFd);
        return SNMP_FAILURE;
    }

    /* Initializing and binding the port with the socket */
    MEMSET (&serv_addr, SNMP_ZERO, sizeof (serv_addr));
    serv_addr.sin6_family = AF_INET6;
    /*  serv_addr.sin6_addr.s6_addr = OSIX_HTONL (INADDR_ANY); */
    inet_pton (AF_INET6, (const CHR1 *) "0::0", &serv_addr.sin6_addr.s6_addr);
    serv_addr.sin6_port = OSIX_HTONS (u2Port);

    i4RetVal = bind (i4SockFd, (struct sockaddr *) &serv_addr,
                     sizeof (serv_addr));

    if (i4RetVal < SNMP_ZERO)
    {
        SNMPTrace ("SNMP: SnmpTcpSock6Init: Unable to bind with TCP server"
                   "address and port for socket\n");
        close (i4SockFd);
        return SNMP_FAILURE;
    }

    i4RetVal = listen (i4SockFd, SNMPTCP_MAX_LISTEN);
    if (i4RetVal < SNMP_ZERO)
    {
        SNMPTrace (" SnmpTcpSock6Init: Unable to listen for the socket\n");
        close (i4SockFd);
        return SNMP_FAILURE;
    }

    gSnmpTcpGblInfo.i4TcpV6SockId = i4SockFd;
    /* Add this SockFd to SELECT library */
    SelAddFd (i4SockFd, SnmpTcpV6NewConnCallBk);

    SNMPTrace ("SNMP: SnmpTcpSock6Init: EXIT \r\n");
    return (SNMP_SUCCESS);
}

/*******************************************************************************
 * Function           : SnmpTcpAcceptNewConnection                             *
 * Input(s)           : i4SrvSockFd - Server socket fd                         *
                        pi4CliSockFd - Pointer to the Client socket
                        pClientAddr - Pointer to the Client Address
 * Output(s)          : New connection identifier                              *
 * Returns            : i4CliSockFd on SUCCESS/ SNMP_FAILURE on failure        *
 * Action             : This routine accepts the connection from the TCP peer. *
*******************************************************************************/
INT4
SnmpTcpAcceptNewConnection (INT4 i4SrvSockFd, INT4 *pi4CliSockFd,
                            tIPvXAddr * pClientAddr)
{
    INT4                i4Val;
    INT4                i4CliSockFd = SNMPTCP_INVALID_SOCKFD;
    INT4                i4Flags = SNMP_ZERO;
    UINT4               u4CliLen = sizeof (struct sockaddr_in);
    UINT4               u4IpAddr = SNMP_ZERO;
    struct sockaddr_in  CliSockAddr;

    SNMPTrace ("SNMP:SnmpTcpAcceptNewConnection: ENTRY \r\n");

    if (i4SrvSockFd == SNMPTCP_INVALID_SOCKFD)
    {
        SNMPTrace ("SNMP:Invalid server socket id \r\n");
        return SNMP_FAILURE;
    }
    MEMSET (&CliSockAddr, SNMP_ZERO, u4CliLen);

    i4CliSockFd = accept (i4SrvSockFd, (struct sockaddr *) &CliSockAddr,
                          &u4CliLen);

    if (i4CliSockFd < SNMP_ZERO)
    {
        SNMPTrace ("SNMP: Connection accept failed !!!\r\n");

        return SNMP_FAILURE;
    }
    /* Set the socket is non-blocking mode */
    i4Flags = O_NONBLOCK;
    i4Val = fcntl (i4CliSockFd, F_SETFL, i4Flags);
    if (i4Val < SNMP_ZERO)
    {
        SNMPTrace ("SNMP: TCP Fcntl SET Failure !!!\r\n");
        close (i4CliSockFd);
        return SNMP_FAILURE;
    }
    /* Adding the accepted socket to the select library */

    *pi4CliSockFd = i4CliSockFd;

    if (CliSockAddr.sin_family == AF_INET)
    {
        u4IpAddr = OSIX_NTOHL (CliSockAddr.sin_addr.s_addr);
        pClientAddr->u1Afi = IPVX_ADDR_FMLY_IPV4;
        pClientAddr->u1AddrLen = IPVX_IPV4_ADDR_LEN;
        MEMCPY (pClientAddr->au1Addr, (UINT1 *) &u4IpAddr, IPVX_IPV4_ADDR_LEN);
    }
    return SNMP_SUCCESS;
}

/******************************************************************************
 * Function           : SnmpTcpPktRcvd
 * Input(s)           : i4SockFd - socket descriptor.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Callback function registered to SelAddFd library.
 *                      This function is invoked when a pakcet arrives on
 *                      TCP socket, it sends SNMPTCP_PKT_READ_EVT to Snmp main
 ******************************************************************************/

VOID
SnmpTcpPktRcvd (INT4 i4SockFd)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;

    tSnmpTcpMsg        *pMsg = NULL;

    SNMPTrace ("SNMP: SnmpTcpPktRcvd: ENTRY \r\n");

    pMsg = (tSnmpTcpMsg *) MemAllocMemBlk (gSnmpTcpGblInfo.TcpCntrlPoolId);

    if (pMsg == NULL)
    {
        SNMPTrace ("\r\n TcpCntrlPoolId exhausted");
        return;
    }

    /* Copying the socket fd to buff */
    pMsg->i4SockFd = i4SockFd;
    pMsg->i4Cmd = SNMPTCP_PKT_READ;

    /* Send the Data to the queue */
    if (OsixQueSend (gSnmpTcpGblInfo.SnmpTcpQId, (UINT1 *) &pMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (gSnmpTcpGblInfo.TcpCntrlPoolId, (UINT1 *) pMsg);
        SNMPTrace ("SNMP: SnmpTcpPktRcvd:Send To SNMPoTCP Q Failed\n");
        return;
    }

    /* Send Event to the task */
    if (OsixSendEvent (SELF, (const UINT1 *) SNMP_TASK_NAME,
                       SNMPTCP_PKT_READ_EVT) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (gSnmpTcpGblInfo.TcpCntrlPoolId, (UINT1 *) pMsg);
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return;
    }
    SNMPTrace ("SNMP: SnmpTcpPktRcvd: EXIT\r\n");
    return;
}

/******************************************************************************
 * Function           : SnmpTcpWriteCallBackFn
 * Input(s)           : i4SockFd - socket descriptor.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Callback function registered to SelAddWrFd library.
 *                      This function is invoked when a Write buffer in 
 *                      TCP socket is free.This function posts an event to Main  *                      Task
 ******************************************************************************/
VOID
SnmpTcpWriteCallBackFn (INT4 i4SockFd)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;

    tSnmpTcpMsg        *pMsg = NULL;

    SNMPTrace ("SNMP: SnmpTcpPktRcvd: ENTRY \r\n");

    pMsg = (tSnmpTcpMsg *) MemAllocMemBlk (gSnmpTcpGblInfo.TcpCntrlPoolId);

    if (pMsg == NULL)
    {
        SNMPTrace ("SNMP: SnmpTcpWriteCallBackFn: Failed to allocate memory\n");
        return;
    }

    /* Copying the socket fd to buff */
    pMsg->i4SockFd = i4SockFd;
    pMsg->i4Cmd = SNMPTCP_PKT_WRITE;

    /* Send the Data to the queue */
    if (OsixQueSend (gSnmpTcpGblInfo.SnmpTcpQId, (UINT1 *) &pMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (gSnmpTcpGblInfo.TcpCntrlPoolId, (UINT1 *) pMsg);
        SNMPTrace ("SNMP: SnmpTcpPktRcvd:Send To SNMPoTCP Q Failed\n");
        return;
    }

    /* Send Event to the task */
    if (OsixSendEvent (SELF, (const UINT1 *) SNMP_TASK_NAME,
                       SNMPTCP_PKT_WRITE_EVT) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (gSnmpTcpGblInfo.TcpCntrlPoolId, (UINT1 *) pMsg);
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return;
    }
    SNMPTrace ("SNMP: SnmpTcpPktRcvd: EXIT\r\n");
    return;
}

/******************************************************************************
 * Function           : SnmpTcpSockRcv
 * Input(s)           : i4SockFd - socket descriptor.
 *                      pu1Data - Buffer to receive the data
 *                      u2PktLen - Length of data to be received from socket
 *                      pi4RdBytes - Length of Data received on the socket
 * Output(s)          : None.
 * Returns            : SNMP_SUCCESS/SNMP_FAILURE
 * Action             : This function receives the data from the given socket
 ******************************************************************************/
INT4
SnmpTcpSockRcv (INT4 i4SockFd, UINT1 *pu1Data, UINT2 u2Len, INT4 *pi4RdBytes)
{
    INT4                i4RdBytes = 0;
    SNMPTrace ("\r\nEntering the function SnmpTcpRcvSock");

    i4RdBytes = recv (i4SockFd, pu1Data, u2Len, 0);

    if (i4RdBytes < 0)
    {
        /* Other than EWOULDBLOCK OR EAGAIN cases, 
         * critical error occured in the socket*/
        if ((errno == EWOULDBLOCK) || (errno == EAGAIN))
        {
            *pi4RdBytes = i4RdBytes;
            return SNMP_SUCCESS;
        }
        perror ("\n SNMPTCP: message receive failed \n");
        return SNMP_FAILURE;
    }

    if (i4RdBytes == 0)
    {
        *pi4RdBytes = i4RdBytes;
        return SNMP_FAILURE;
    }

    *pi4RdBytes = i4RdBytes;
    return SNMP_SUCCESS;
}

/******************************************************************************
 * Function           : SnmpTcpSockSend
 * Input(s)           : i4SockFd - socket descriptor.
 *                      pu1Data - Data to send on the socket
 *                      u2PktLen - Length of data to be send thro the socket
 *                      pi4WrBytes - Length of Data Sent on the socket
 * Output(s)          : None.
 * Returns            : SNMP_SUCCESS/SNMP_FAILURE
 * Action             : This function sends the given data thro the TCP socket
 ******************************************************************************/
INT4
SnmpTcpSockSend (INT4 i4SockFd, UINT1 *pu1Data, UINT2 u2PktLen,
                 INT4 *pi4WrBytes)
{
    INT4                i4WrBytes = 0;

    SNMPTrace ("SnmpTcpSockSend: Function ENTRY \r\n");
    /* Send the file information to request the file */
    i4WrBytes = send (i4SockFd, pu1Data, u2PktLen, MSG_NOSIGNAL);
    if (i4WrBytes < 0)
    {
        /* Other than EWOULDBLOCK OR EAGAIN cases,
         * critical error occured in the socket*/
        if ((errno == EWOULDBLOCK) || (errno == EAGAIN))
        {
            *pi4WrBytes = 0;
            return SNMP_SUCCESS;
        }
        perror ("\n SNMPTCP: Snmp Msg Send over TCP failed \n");
        return SNMP_FAILURE;
    }
    *pi4WrBytes = i4WrBytes;
    SNMPTrace ("SNMPTCP:SnmpTcpSockSend: EXIT \r\n");
    return SNMP_SUCCESS;
}

/******************************************************************************
 * Function           : SnmpTcpV4NewConnCallBk
 * Input(s)           : i4SockFd - socket descriptor.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Callback function registered to SELECT library.
 *                      This function is invoked when a pakcet arrives on
 *                      TCP socket for Ipv4.
 ******************************************************************************/
VOID
SnmpTcpV4NewConnCallBk (INT4 i4SockFd)
{
    UNUSED_PARAM (i4SockFd);
    SNMPTrace ("SNMP: ENTERING SnmpTcpV4NewConnCallBk \r\n");

    if (OsixSendEvent (SELF, (const UINT1 *) SNMP_TASK_NAME,
                       SNMPTCP_V4_NEWCONN_EVT) != OSIX_SUCCESS)
    {
        SNMPTrace ("SNMP: Event Send Failed -SNMPoTCP_NEW_CONN_EVT\r\n");
        return;
    }
    SNMPTrace ("SNMP: Leaving SnmpTcpV4NewConnCallBk\r\n");
}

/******************************************************************************
 * Function           : SnmpTcpNew6ConnCallBk
 * Input(s)           : i4SockFd - socket descriptor.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Callback function registered to SELECT library.
 *                      This function is invoked when a pakcet arrives on
 *                      TCP socket for Ipv4.
 ******************************************************************************/
VOID
SnmpTcpV6NewConnCallBk (INT4 i4SockFd)
{
    UNUSED_PARAM (i4SockFd);
    SNMPTrace ("SNMP: ENTERING SnmpTcpV6NewConnCallBk \r\n");

    if (OsixSendEvent (SELF, (const UINT1 *) SNMP_TASK_NAME,
                       SNMPTCP_V6_NEWCONN_EVT) != OSIX_SUCCESS)
    {
        SNMPTrace ("SNMP: Event Send Failed -SNMPoTCP_NEW_CONN_EVT\r\n");
        return;
    }
    SNMPTrace ("SNMP: Leaving SnmpTcpV6NewConnCallBk\r\n");
}
