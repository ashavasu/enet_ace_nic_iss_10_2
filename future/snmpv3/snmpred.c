/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: snmpred.c,v 1.8 2015/04/28 12:35:02 siva Exp $
 *
 * Description:This file contains redundancy related function.  
 *
 *******************************************************************/
#ifndef _SNMPRED_C_
#define _SNMPRED_C_

#include "lr.h"
#include "rmgr.h"
#include "snmpcmn.h"
#include "snmputil.h"
#include "snmpred.h"

UINT1               gu1SnmpNoOfPeers;
tSnmpNodeStatus     gSnmpNodeStatus = SNMP_NODE_INIT;
tOsixQId            gSnmpRedQId;
tMemPoolId          gSnmpRedRmMsgPoolId;

/*****************************************************************************/
/* Function Name      : SnmpRedTaskMain                                      */
/*                                                                           */
/* Description        : This function is the entry point function of the SNMP*/
/*                      RED Task.                                            */
/*                                                                           */
/* Input(s)           : pi1Param - currently unused.                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SnmpRedTaskMain (INT1 *pi1Param)
{
    UINT4               u4Events = 0;

    UNUSED_PARAM (pi1Param);

    /* Create queue to recieve msgs from RM */
    if (SNMP_RED_CREATE_QUEUE (SNMP_RED_Q_NAME, MAX_SNMP_RED_Q_DEPTH, 0,
                               &(SNMP_RED_Q_ID)) != OSIX_SUCCESS)
    {
        SNMPTrace (" SNMP: RED: SNMP_RED Queue creation failed\n");

        SNMP_RED_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    SNMP_RED_RM_MSG_POOL_ID = SNMPV3MemPoolIds[MAX_SNMP_RED_Q_DEPTH_SIZING_ID];

    if (SnmpRedRegisterWithRM () != SNMP_SUCCESS)
    {
        SNMP_RED_DELETE_QUEUE (SELF, SNMP_RED_Q_NAME);
        SNMP_RED_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Indicate the status of initialization to the main routine */
    SNMP_RED_INIT_COMPLETE (OSIX_SUCCESS);

    /* Waiting in a blocking, continuous call in order to receive events */
    while (1)
    {
        if (SNMP_RED_RECEIVE_EVENT (SNMP_RED_ALL_EVENTS, OSIX_WAIT, 0,
                                    &u4Events) == OSIX_SUCCESS)
        {
            if (u4Events & SNMP_RED_PKT_ENQ_EVENT)
            {
                SnmpRedHandlePktEnqEvent ();
            }
        }
    }

}

/*****************************************************************************/
/* Function Name      : SnmpRedRegisterWithRM                                */
/*                                                                           */
/* Description        : Registers SNMP with RM by providing an application   */
/*                      ID for SNMP and a call back function to be called    */
/*                      whenever RM needs to send an event to SNMP.          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS/SNMP_FAILURE                            */
/*****************************************************************************/
INT4
SnmpRedRegisterWithRM (VOID)
{
    tRmRegParams        RmRegParams;

    RmRegParams.u4EntId = RM_SNMP_APP_ID;
    RmRegParams.pFnRcvPkt = SnmpRedRcvPktFromRm;
    if (RmRegisterProtocols (&RmRegParams) == RM_FAILURE)
    {
        SNMPTrace (" SNMP : RED : Registration with RM FAILED\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnmpRedRcvPktFromRm                                  */
/*                                                                           */
/* Description        : This function constructs a message containing the    */
/*                      given RM event and RM message and post it to the SNMP*/
/*                      RED task queue.                                      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS/SNMP_FAILURE                            */
/*****************************************************************************/
INT4
SnmpRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tSnmpRedQMsg       *pMesg = NULL;

    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        /* Message absent and hence no need to process and no need
         * to send anything to SNMP RED task. */

        return RM_FAILURE;
    }
    else if (u1Event == RM_MESSAGE)
    {
        /* RM message is not valid for SNMP */
        RM_FREE (pData);
        return RM_FAILURE;
    }

    SNMP_RED_ALLOC_MEM_BLOCK (SNMP_RED_RM_MSG_POOL_ID, pMesg, tSnmpRedQMsg);

    if (pMesg == NULL)
    {
        SNMPTrace ("SNMP : RED : RM Message "
                   "MEM_ALLOC_MEM_BLOCK FAILED for RM message \n");

        if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            RmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        return RM_FAILURE;
    }

    MEMSET (pMesg, 0, sizeof (tSnmpRedQMsg));

    pMesg->u4MsgType = SNMP_RED_RM_MSG;

    pMesg->RmData.u1Event = u1Event;
    pMesg->RmData.pFrame = pData;
    pMesg->RmData.u2Length = u2DataLen;

    if (SNMP_RED_ENQUEUE_MESSAGE (SNMP_RED_Q_NAME, pMesg) != OSIX_SUCCESS)
    {
        SNMPTrace (" SNMP : RED : Rm Message enqueue FAILED\n");

        SNMP_RED_RELEASE_MEM_BLOCK (SNMP_RED_RM_MSG_POOL_ID, (UINT1 *) pMesg);

        if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            RmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        return RM_FAILURE;
    }

    /* Sent the event to SNMP RED Task. */
    if (SNMP_RED_SEND_EVENT (SELF, SNMP_RED_TASK_NAME, SNMP_RED_PKT_ENQ_EVENT)
        != OSIX_SUCCESS)
    {
        SNMPTrace (" SNMP : RED : Rm Event send FAILED\n");
        return RM_FAILURE;
    }

    return RM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnmpRedHandlePktEnqEvent                             */
/*                                                                           */
/* Description        : Handles packet received by SNMP RED task             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
SnmpRedHandlePktEnqEvent (VOID)
{
    tOsixMsg           *pOsixMsg = NULL;
    tSnmpRedQMsg       *pMesg = NULL;

    /* Event received, dequeue messages */
    while (SNMP_RED_DEQUEUE_MESSAGES (SNMP_RED_Q_NAME, OSIX_NO_WAIT, &pOsixMsg)
           == OSIX_SUCCESS)
    {
        pMesg = (tSnmpRedQMsg *) pOsixMsg;
        switch (pMesg->u4MsgType)
        {
            case SNMP_RED_RM_MSG:
                SnmpHandleRmEvents (pMesg->RmData);
                break;

            default:
                SNMPTrace (" SNMP : RED: Unknown messages received by"
                           " SNMP RED task\n");
                break;
        }

        /* Now release the buffer to pool */
        SNMP_RED_RELEASE_MEM_BLOCK (SNMP_RED_RM_MSG_POOL_ID, (UINT1 *) pMesg);
    }
}

/*****************************************************************************/
/* Function Name      : SnmpHandleRmEvents                                   */
/*                                                                           */
/* Description        : This function is invoked to process the following    */
/*                      from RM module:-                                     */
/*                           - RM events and                                 */
/*                           - update messages.                              */
/*                      This function interprets the RM events and calls the */
/*                      corresponding module functions to process those      */
/*                      events.                                              */
/*                                                                           */
/* Input(s)           : SnmpRmMsg - Rm Message which contains event          */
/*                                  and buffer                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS / SNMP_FAILURE                          */
/*****************************************************************************/
INT4
SnmpHandleRmEvents (tSnmpRmMsg SnmpRmMsg)
{
    tRmNodeInfo        *pData = NULL;
    tSnmpNodeStatus     SnmpPrevNodeSt = SNMP_NODE_INIT;
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_SNMP_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    switch (SnmpRmMsg.u1Event)
    {
        case GO_ACTIVE:

            SNMPTrace ("SNMP: RED: In SnmpHandleRmEvents "
                       "GO_ACTIVE message received from RM \r\n");
            SnmpPrevNodeSt = SNMP_NODE_STATUS ();
            SNMP_NODE_STATUS () = SNMP_NODE_ACTIVE;

            SNMP_NUM_STANDBY_NODES () = RmGetStandbyNodeCount ();

            if (SnmpPrevNodeSt == SNMP_NODE_INIT)
            {
                ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
            }
            else
            {
                ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
            }

            RmApiHandleProtocolEvent (&ProtoEvt);

            break;

        case GO_STANDBY:

            SNMPTrace ("SNMP: RED: In SnmpHandleRmEvents "
                       "GO_STANDBY message received from RM \r\n");

            SNMP_NODE_STATUS () = SNMP_NODE_STANDBY;

            SNMP_NUM_STANDBY_NODES () = 0;

            ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
            RmApiHandleProtocolEvent (&ProtoEvt);

            break;

        case RM_STANDBY_UP:
            pData = (tRmNodeInfo *) SnmpRmMsg.pFrame;
            SNMP_NUM_STANDBY_NODES () = pData->u1NumStandby;
            RmReleaseMemoryForMsg ((UINT1 *) pData);
            break;

        case RM_STANDBY_DOWN:
            pData = (tRmNodeInfo *) SnmpRmMsg.pFrame;
            SNMP_NUM_STANDBY_NODES () = pData->u1NumStandby;
            RmReleaseMemoryForMsg ((UINT1 *) pData);
            break;

        default:
            SNMPTrace ("SNMP: RED: !! Error !! In SnmpHandleRmEvents "
                       "Invalid Event received from RM\n");

            break;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnmpRedSetNodeStateToInit                               */
/*                                                                           */
/* Description        : It moves the SNMP red status to INIT                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
SnmpRedSetNodeStateToInit (VOID)
{
    SNMP_NODE_STATUS () = SNMP_NODE_INIT;
    SNMP_NUM_STANDBY_NODES () = 0;
}

#endif
