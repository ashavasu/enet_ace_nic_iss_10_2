/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: snxtdfs.h,v 1.4 2015/04/28 12:35:03 siva Exp $
*
* Description: Structure definition which are used by Agentx module
*********************************************************************/
#ifndef _SNAGTXTDFS_H
#define _SNAGTXTDFS_H

typedef struct SnmpAgentxOidType
{
    UINT1        u1NoSubId;
    UINT1        u1Prefix;
    BOOL1        b1Include;
    UINT1        u1Reserved;
    UINT4        *pu4OidLst;
}tSnmpAgentxOidType;

typedef struct SnmpSearchRng 
{
    tSnmpAgentxOidType      StartId;
    tSnmpAgentxOidType      EndId;
    struct SnmpSearchRng   *pNextSearchRng;
}tSnmpSearchRng;

typedef struct SnmpAgentxVarBind {
        UINT2                      u2Type;
        UINT2                      u2Reserved;
        tSnmpAgentxOidType         ObjName;
        tSNMP_MULTI_DATA_TYPE      ObjValue;
        struct SnmpAgentxVarBind  *pNextVarBind;
} tSnmpAgentxVarBind;

typedef struct SnmpOidInfo
{
    tSnmpAgentxOidType      OidVal;
    tSNMP_OCTET_STRING_TYPE OidDesc;
}tSnmpOidInfo;

typedef struct SnmpAgentxPdu
{
    UINT1                   u1Version;
    UINT1                   u1PduType;
    UINT1                   u1Flags;
    UINT1                   u1Reserved;
    INT4                    i4SessionId;
    UINT4                   u4TransactionId;
    UINT4                   u4PacketId;
    UINT4                   u4PayLoadLen;
    UINT1                   u1TimeOut;
    UINT1                   u1Reason;
    UINT1                   u1Priority;
    UINT1                   u1RangeSubId;
    tSNMP_OCTET_STRING_TYPE ContextName;
    union
    {
        tSnmpSearchRng         *pSearchRngLst;
        tSnmpOidInfo            OidInfo;
        tSnmpAgentxVarBind     *pVarBindList; 
    }unPduInfo;
#define pSearchRngLst    unPduInfo.pSearchRngLst
#define OidVal           unPduInfo.OidInfo.OidVal
#define OidDesc          unPduInfo.OidInfo.OidDesc
#define pAgxVarBindList  unPduInfo.pVarBindList
    UINT4                   u4UpperBound;
    UINT2                   u2NonRepeaters;
    UINT2                   u2MaxRepetitions;
    UINT4                   u4SysUpTime;
    UINT2                   u2Error;
    UINT2                   u2ErrIndex;
}tSnmpAgentxPdu;


typedef  struct SnmpAgentxInfo
{
     INT4                    i4SockId;
     INT4                    i4SessionId;
     tSNMP_OCTET_STRING_TYPE MasterIpAddr;   /* Master Agent Ip Address */
     UINT2                   u2PortNo;         /* Port number of the master agent */
     UINT1                   u1TDomain; 
     UINT1                  u1OutStandPing; /* No of pings for which 
                                               we didnt get a response */
     INT4                    i4BuddyId;
     tTimerListId            SnxTmrListId;
     tTmrBlk                 SnxTmr;
     tSNMP_OCTET_STRING_TYPE ContextName;
}tAgentxInfo;

typedef  struct SnmpAgentxStats
{
     UINT4                  u4InPkts;
     UINT4                  u4OutPks;
     UINT4                  u4PktDrops;
     UINT4                  u4ParseDrops;   
     UINT4                  u4OpenFails;
     UINT4                  u4OpenPduSent;
     UINT4                  u4ClosePduRcvd;
     UINT4                  u4ClosePduSent;
     UINT4                  u4IndxAllocPduSent;
     UINT4                  u4IndxDAllocPduSent;
     UINT4                  u4RegPduSent;
     UINT4                  u4UnRegPduSent;
     UINT4                  u4AddCapsPduSent;
     UINT4                  u4RemoveCapsPduSent;
     UINT4                  u4NotifyPduSent;
     UINT4                  u4PingPduSent;   
     UINT4                  u4GetPduRcvd;
     UINT4                  u4GetNextPduRcvd;
     UINT4                  u4GetBulkPduRcvd;
     UINT4                  u4TestSetPduRcvd;
     UINT4                  u4CommitSetPduRcvd;
     UINT4                  u4UndoSetPduRcvd;
     UINT4                  u4CleanupSetPduRcvd;
     UINT4                  u4RespPduSent;
     UINT4                  u4RespPduRcvd;
}tSnmpAgentxStats;
#endif
