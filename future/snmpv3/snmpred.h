/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: snmpred.h,v 1.7 2015/04/28 12:35:02 siva Exp $
 *
 * Description:This file contains the exported definitions and  
 *             macros related to redundancy.                   
 *
 *******************************************************************/

#ifndef _SNMPRED_H
#define _SNMPRED_H

/* To handle message/events given by redundancy manager. */
typedef struct SnmpRmMsg
{
    tRmMsg        *pFrame;     /* Message given by RM module. */
    UINT2          u2Length;   /* Length of message given by RM module. */
    UINT1          u1Event;    /* Event given by RM module. */
    UINT1          u1Reserved;
}tSnmpRmMsg;

typedef struct SnmpRedQMsg
{
   UINT4         u4MsgType;
   tSnmpRmMsg    RmData;
}tSnmpRedQMsg;

typedef enum {
    SNMP_RED_RM_MSG = 1,
}tSnmpRedQMsgType;


#define SNMP_RED_TASK_NAME        ((const UINT1*) "SRED")
#define SNMP_RED_Q_NAME           ((const UINT1 *)"SRDQ")
#define SNMP_RED_Q_ID             gSnmpRedQId         
#define SNMP_RED_RM_MSG_POOL_ID   gSnmpRedRmMsgPoolId

#define SNMP_RED_MEMORY_TYPE      MEM_DEFAULT_MEMORY_TYPE

#define SNMP_RED_PKT_ENQ_EVENT    0x01

#define SNMP_RED_ALL_EVENTS       (SNMP_RED_PKT_ENQ_EVENT)


#define SNMP_RED_INIT_COMPLETE(u4Status)    lrInitComplete(u4Status)

#define SNMP_RED_CREATE_QUEUE     OsixCreateQ
#define SNMP_RED_DELETE_QUEUE     OsixDeleteQ
#define SNMP_RED_SEND_EVENT       OsixSendEvent
#define SNMP_RED_RECEIVE_EVENT    OsixReceiveEvent

#define SNMP_RED_ALLOC_MEM_BLOCK(PoolId,pu1Msg,type)\
         pu1Msg = (type *)(MemAllocMemBlk(PoolId));

#define SNMP_RED_RELEASE_MEM_BLOCK(PoolId, pu1Block)\
         MemReleaseMemBlock ((PoolId), (pu1Block))

#define SNMP_RED_ENQUEUE_MESSAGE(qnam,pmsg)\
         OsixSendToQ(SELF,(qnam),(tOsixMsg *)(VOID *)(pmsg),OSIX_MSG_NORMAL)

#define SNMP_RED_DEQUEUE_MESSAGES(qnam,wait,ppmsg)\
         OsixReceiveFromQ (SELF,(qnam),(wait),0,ppmsg)


/* Macros to write in to RM buffer. */
#define SNMP_RM_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), (u4MesgType)); \
        *(pu4Offset) += 4;\
}while (0)

#define SNMP_RM_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), (u2MesgType)); \
        *(pu4Offset) += 2;\
}while (0)

#define SNMP_RM_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), (u1MesgType)); \
        *(pu4Offset) += 1;\
}while (0)

#define SNMP_RM_PUT_N_BYTE(pdest, psrc, pu4Offset, u4Size) \
do { \
    RM_COPY_TO_OFFSET((pdest), (psrc), *(pu4Offset), u4Size); \
        *(pu4Offset) +=u4Size; \
}while (0)

#define SNMP_RM_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define SNMP_RM_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define SNMP_RM_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define SNMP_RM_GET_N_BYTE(psrc, pdest, pu4Offset, u4Size) \
do { \
    RM_GET_DATA_N_BYTE (psrc, pdest, *(pu4Offset), u4Size); \
        *(pu4Offset) +=u4Size; \
}while (0)

/* Node Status */
typedef UINT4 tSnmpNodeStatus;

#define SNMP_SYNC_SET_MSG       1

#define SNMP_NODE_INIT         RM_INIT
#define SNMP_NODE_ACTIVE       RM_ACTIVE
#define SNMP_NODE_STANDBY      RM_STANDBY

extern tSnmpNodeStatus     gSnmpNodeStatus;
extern UINT1               gu1SnmpNoOfPeers;

#define SNMP_NODE_STATUS()   gSnmpNodeStatus

#define SNMP_NUM_STANDBY_NODES() gu1SnmpNoOfPeers

#define SNMP_IS_STANDBY_UP() \
          ((gu1SnmpNoOfPeers > 0) ? SNMP_TRUE : SNMP_FALSE)


VOID
SnmpRedTaskMain (INT1 *pi1Param);
INT4
SnmpRedRegisterWithRM (VOID);
INT4
SnmpRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen);
VOID
SnmpRedHandlePktEnqEvent (VOID);
INT4
SnmpHandleRmEvents (tSnmpRmMsg SnmpRmMsg);
INT4
SnmpSendMsgToRm (tRmMsg * pMsg, UINT2 u2BufSize);
#endif
