/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdsnmdb.h,v 1.4 2015/04/28 12:35:03 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDSNMDB_H
#define _STDSNMDB_H

UINT1 SysORTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};

UINT4 stdsnm [] ={1,3,6,1,2,1,1};
tSNMP_OID_TYPE stdsnmOID = {7, stdsnm};


UINT4 SysDescr [ ] ={1,3,6,1,2,1,1,1};
UINT4 SysObjectID [ ] ={1,3,6,1,2,1,1,2};
UINT4 SysUpTime [ ] ={1,3,6,1,2,1,1,3};
UINT4 SysContact [ ] ={1,3,6,1,2,1,1,4};
UINT4 SysName [ ] ={1,3,6,1,2,1,1,5};
UINT4 SysLocation [ ] ={1,3,6,1,2,1,1,6};
UINT4 SysServices [ ] ={1,3,6,1,2,1,1,7};
UINT4 SysORLastChange [ ] ={1,3,6,1,2,1,1,8};
UINT4 SysORIndex [ ] ={1,3,6,1,2,1,1,9,1,1};
UINT4 SysORID [ ] ={1,3,6,1,2,1,1,9,1,2};
UINT4 SysORDescr [ ] ={1,3,6,1,2,1,1,9,1,3};
UINT4 SysORUpTime [ ] ={1,3,6,1,2,1,1,9,1,4};
UINT4 SnmpInPkts [ ] ={1,3,6,1,2,1,11,1};
UINT4 SnmpInBadVersions [ ] ={1,3,6,1,2,1,11,3};
UINT4 SnmpInBadCommunityNames [ ] ={1,3,6,1,2,1,11,4};
UINT4 SnmpInBadCommunityUses [ ] ={1,3,6,1,2,1,11,5};
UINT4 SnmpInASNParseErrs [ ] ={1,3,6,1,2,1,11,6};
UINT4 SnmpEnableAuthenTraps [ ] ={1,3,6,1,2,1,11,30};
UINT4 SnmpSilentDrops [ ] ={1,3,6,1,2,1,11,31};
UINT4 SnmpProxyDrops [ ] ={1,3,6,1,2,1,11,32};
UINT4 SnmpOutPkts [ ] ={1,3,6,1,2,1,11,2};
UINT4 SnmpInTooBigs [ ] ={1,3,6,1,2,1,11,8};
UINT4 SnmpInNoSuchNames [ ] ={1,3,6,1,2,1,11,9};
UINT4 SnmpInBadValues [ ] ={1,3,6,1,2,1,11,10};
UINT4 SnmpInReadOnlys [ ] ={1,3,6,1,2,1,11,11};
UINT4 SnmpInGenErrs [ ] ={1,3,6,1,2,1,11,12};
UINT4 SnmpInTotalReqVars [ ] ={1,3,6,1,2,1,11,13};
UINT4 SnmpInTotalSetVars [ ] ={1,3,6,1,2,1,11,14};
UINT4 SnmpInGetRequests [ ] ={1,3,6,1,2,1,11,15};
UINT4 SnmpInGetNexts [ ] ={1,3,6,1,2,1,11,16};
UINT4 SnmpInSetRequests [ ] ={1,3,6,1,2,1,11,17};
UINT4 SnmpInGetResponses [ ] ={1,3,6,1,2,1,11,18};
UINT4 SnmpInTraps [ ] ={1,3,6,1,2,1,11,19};
UINT4 SnmpOutTooBigs [ ] ={1,3,6,1,2,1,11,20};
UINT4 SnmpOutNoSuchNames [ ] ={1,3,6,1,2,1,11,21};
UINT4 SnmpOutBadValues [ ] ={1,3,6,1,2,1,11,22};
UINT4 SnmpOutGenErrs [ ] ={1,3,6,1,2,1,11,24};
UINT4 SnmpOutGetRequests [ ] ={1,3,6,1,2,1,11,25};
UINT4 SnmpOutGetNexts [ ] ={1,3,6,1,2,1,11,26};
UINT4 SnmpOutSetRequests [ ] ={1,3,6,1,2,1,11,27};
UINT4 SnmpOutGetResponses [ ] ={1,3,6,1,2,1,11,28};
UINT4 SnmpOutTraps [ ] ={1,3,6,1,2,1,11,29};
UINT4 SnmpSetSerialNo [ ] ={1,3,6,1,6,3,1,1,6,1};


tMbDbEntry stdsnmMibEntry[]= {

{{8,SysDescr}, NULL, SysDescrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,SysObjectID}, NULL, SysObjectIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,SysUpTime}, NULL, SysUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,SysContact}, NULL, SysContactGet, SysContactSet, SysContactTest, SysContactDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{8,SysName}, NULL, SysNameGet, SysNameSet, SysNameTest, SysNameDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{8,SysLocation}, NULL, SysLocationGet, SysLocationSet, SysLocationTest, SysLocationDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{8,SysServices}, NULL, SysServicesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,SysORLastChange}, NULL, SysORLastChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,SysORIndex}, GetNextIndexSysORTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, SysORTableINDEX, 1, 0, 0, NULL},

{{10,SysORID}, GetNextIndexSysORTable, SysORIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READONLY, SysORTableINDEX, 1, 0, 0, NULL},

{{10,SysORDescr}, GetNextIndexSysORTable, SysORDescrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, SysORTableINDEX, 1, 0, 0, NULL},

{{10,SysORUpTime}, GetNextIndexSysORTable, SysORUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, SysORTableINDEX, 1, 0, 0, NULL},

{{8,SnmpInPkts}, NULL, SnmpInPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,SnmpOutPkts}, NULL, SnmpOutPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,SnmpInBadVersions}, NULL, SnmpInBadVersionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,SnmpInBadCommunityNames}, NULL, SnmpInBadCommunityNamesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,SnmpInBadCommunityUses}, NULL, SnmpInBadCommunityUsesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,SnmpInASNParseErrs}, NULL, SnmpInASNParseErrsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,SnmpInTooBigs}, NULL, SnmpInTooBigsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,SnmpInNoSuchNames}, NULL, SnmpInNoSuchNamesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,SnmpInBadValues}, NULL, SnmpInBadValuesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,SnmpInReadOnlys}, NULL, SnmpInReadOnlysGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,SnmpInGenErrs}, NULL, SnmpInGenErrsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,SnmpInTotalReqVars}, NULL, SnmpInTotalReqVarsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,SnmpInTotalSetVars}, NULL, SnmpInTotalSetVarsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,SnmpInGetRequests}, NULL, SnmpInGetRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,SnmpInGetNexts}, NULL, SnmpInGetNextsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,SnmpInSetRequests}, NULL, SnmpInSetRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,SnmpInGetResponses}, NULL, SnmpInGetResponsesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,SnmpInTraps}, NULL, SnmpInTrapsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,SnmpOutTooBigs}, NULL, SnmpOutTooBigsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,SnmpOutNoSuchNames}, NULL, SnmpOutNoSuchNamesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,SnmpOutBadValues}, NULL, SnmpOutBadValuesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,SnmpOutGenErrs}, NULL, SnmpOutGenErrsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,SnmpOutGetRequests}, NULL, SnmpOutGetRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,SnmpOutGetNexts}, NULL, SnmpOutGetNextsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,SnmpOutSetRequests}, NULL, SnmpOutSetRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,SnmpOutGetResponses}, NULL, SnmpOutGetResponsesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,SnmpOutTraps}, NULL, SnmpOutTrapsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,SnmpEnableAuthenTraps}, NULL, SnmpEnableAuthenTrapsGet, SnmpEnableAuthenTrapsSet, SnmpEnableAuthenTrapsTest, SnmpEnableAuthenTrapsDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{8,SnmpSilentDrops}, NULL, SnmpSilentDropsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,SnmpProxyDrops}, NULL, SnmpProxyDropsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,SnmpSetSerialNo}, NULL, SnmpSetSerialNoGet, SnmpSetSerialNoSet, SnmpSetSerialNoTest, SnmpSetSerialNoDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData stdsnmEntry = { 43, stdsnmMibEntry };
#endif /* _STDSNMDB_H */

