/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssnmp3db.h,v 1.6 2015/04/28 12:35:01 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSSNMPDB_H
#define _FSSNMPDB_H
#ifndef _SNTARGDB_H
#define _SNTARGDB_H

UINT1 SnmpInformCntTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsSnmpProxyTableINDEX [] = {SNMP_DATA_TYPE_IMP_OCTET_PRIM};
UINT1 FsSnmpTrapFilterTableINDEX [] = {SNMP_DATA_TYPE_IMP_OBJECT_ID};
UINT1 FsSnmpTargetAddrTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};

UINT4 fssnmp [] ={1,3,6,1,4,1,2076,112};
tSNMP_OID_TYPE fssnmpOID = {8, fssnmp};


UINT4 SnmpInInformResponses [ ] ={1,3,6,1,4,1,2076,112,1};
UINT4 SnmpOutInformRequests [ ] ={1,3,6,1,4,1,2076,112,2};
UINT4 SnmpInformDrops [ ] ={1,3,6,1,4,1,2076,112,3};
UINT4 SnmpInformAwaitingAck [ ] ={1,3,6,1,4,1,2076,112,4};
UINT4 SnmpListenTrapPort [ ] ={1,3,6,1,4,1,2076,112,5};
UINT4 SnmpOverTcpStatus [ ] ={1,3,6,1,4,1,2076,112,6};
UINT4 SnmpListenTcpPort [ ] ={1,3,6,1,4,1,2076,112,7};
UINT4 SnmpTrapOverTcpStatus [ ] ={1,3,6,1,4,1,2076,112,8};
UINT4 SnmpListenTcpTrapPort [ ] ={1,3,6,1,4,1,2076,112,9};
UINT4 SnmpInformTgtAddrName [ ] ={1,3,6,1,4,1,2076,112,10,1,1};
UINT4 SnmpInformSent [ ] ={1,3,6,1,4,1,2076,112,10,1,2};
UINT4 SnmpInformAwaitAck [ ] ={1,3,6,1,4,1,2076,112,10,1,3};
UINT4 SnmpInformRetried [ ] ={1,3,6,1,4,1,2076,112,10,1,4};
UINT4 SnmpInformDropped [ ] ={1,3,6,1,4,1,2076,112,10,1,5};
UINT4 SnmpInformFailed [ ] ={1,3,6,1,4,1,2076,112,10,1,6};
UINT4 SnmpInformResponses [ ] ={1,3,6,1,4,1,2076,112,10,1,7};
UINT4 SnmpColdStartTrapControl [ ] ={1,3,6,1,4,1,2076,112,11};
UINT4 SnmpAgentControl [ ] ={1,3,6,1,4,1,2076,112,12};
UINT4 SnmpAllowedPduVersions [ ] ={1,3,6,1,4,1,2076,112,13};
UINT4 SnmpMinimumSecurityRequired [ ] ={1,3,6,1,4,1,2076,112,14};
UINT4 SnmpInRollbackErrs [ ] ={1,3,6,1,4,1,2076,112,16};
UINT4 SnmpProxyListenTrapPort [ ] ={1,3,6,1,4,1,2076,112,17};
UINT4 FsSnmpProxyMibName [ ] ={1,3,6,1,4,1,2076,112,18,1,1};
UINT4 FsSnmpProxyMibType [ ] ={1,3,6,1,4,1,2076,112,18,1,2};
UINT4 FsSnmpProxyMibId [ ] ={1,3,6,1,4,1,2076,112,18,1,3};
UINT4 FsSnmpProxyMibTargetParamsIn [ ] ={1,3,6,1,4,1,2076,112,18,1,4};
UINT4 FsSnmpProxyMibSingleTargetOut [ ] ={1,3,6,1,4,1,2076,112,18,1,5};
UINT4 FsSnmpProxyMibMultipleTargetOut [ ] ={1,3,6,1,4,1,2076,112,18,1,6};
UINT4 FsSnmpProxyMibStorageType [ ] ={1,3,6,1,4,1,2076,112,18,1,7};
UINT4 FsSnmpProxyMibRowStatus [ ] ={1,3,6,1,4,1,2076,112,18,1,8};
UINT4 FsSnmpListenAgentPort [ ] ={1,3,6,1,4,1,2076,112,19};
UINT4 FsSnmpEngineID [ ] ={1,3,6,1,4,1,2076,112,21};
UINT4 SnmpAgentxTransportDomain [ ] ={1,3,6,1,4,1,2076,112,15,1};
UINT4 SnmpAgentxMasterAgentAddr [ ] ={1,3,6,1,4,1,2076,112,15,2};
UINT4 SnmpAgentxMasterAgentPortNo [ ] ={1,3,6,1,4,1,2076,112,15,3};
UINT4 SnmpAgentxSubAgentInPkts [ ] ={1,3,6,1,4,1,2076,112,15,4};
UINT4 SnmpAgentxSubAgentOutPkts [ ] ={1,3,6,1,4,1,2076,112,15,5};
UINT4 SnmpAgentxSubAgentPktDrops [ ] ={1,3,6,1,4,1,2076,112,15,6};
UINT4 SnmpAgentxSubAgentParseDrops [ ] ={1,3,6,1,4,1,2076,112,15,7};
UINT4 SnmpAgentxSubAgentInOpenFail [ ] ={1,3,6,1,4,1,2076,112,15,8};
UINT4 SnmpAgentxSubAgentOpenPktCnt [ ] ={1,3,6,1,4,1,2076,112,15,9};
UINT4 SnmpAgentxSubAgentInClosePktCnt [ ] ={1,3,6,1,4,1,2076,112,15,10};
UINT4 SnmpAgentxSubAgentOutClosePktCnt [ ] ={1,3,6,1,4,1,2076,112,15,11};
UINT4 SnmpAgentxSubAgentIdAllocPktCnt [ ] ={1,3,6,1,4,1,2076,112,15,12};
UINT4 SnmpAgentxSubAgentIdDllocPktCnt [ ] ={1,3,6,1,4,1,2076,112,15,13};
UINT4 SnmpAgentxSubAgentRegPktCnt [ ] ={1,3,6,1,4,1,2076,112,15,14};
UINT4 SnmpAgentxSubAgentUnRegPktCnt [ ] ={1,3,6,1,4,1,2076,112,15,15};
UINT4 SnmpAgentxSubAgentAddCapsCnt [ ] ={1,3,6,1,4,1,2076,112,15,16};
UINT4 SnmpAgentxSubAgentRemCapsCnt [ ] ={1,3,6,1,4,1,2076,112,15,17};
UINT4 SnmpAgentxSubAgentNotifyPktCnt [ ] ={1,3,6,1,4,1,2076,112,15,18};
UINT4 SnmpAgentxSubAgentPingCnt [ ] ={1,3,6,1,4,1,2076,112,15,19};
UINT4 SnmpAgentxSubAgentInGets [ ] ={1,3,6,1,4,1,2076,112,15,20};
UINT4 SnmpAgentxSubAgentInGetNexts [ ] ={1,3,6,1,4,1,2076,112,15,21};
UINT4 SnmpAgentxSubAgentInGetBulks [ ] ={1,3,6,1,4,1,2076,112,15,22};
UINT4 SnmpAgentxSubAgentInTestSets [ ] ={1,3,6,1,4,1,2076,112,15,23};
UINT4 SnmpAgentxSubAgentInCommits [ ] ={1,3,6,1,4,1,2076,112,15,24};
UINT4 SnmpAgentxSubAgentInCleanups [ ] ={1,3,6,1,4,1,2076,112,15,25};
UINT4 SnmpAgentxSubAgentInUndos [ ] ={1,3,6,1,4,1,2076,112,15,26};
UINT4 SnmpAgentxSubAgentOutResponse [ ] ={1,3,6,1,4,1,2076,112,15,27};
UINT4 SnmpAgentxSubAgentInResponse [ ] ={1,3,6,1,4,1,2076,112,15,28};
UINT4 SnmpAgentxSubAgentControl [ ] ={1,3,6,1,4,1,2076,112,15,29};
UINT4 SnmpAgentxContextName [ ] ={1,3,6,1,4,1,2076,112,15,30};
UINT4 FsSnmpTrapFilterOID [ ] ={1,3,6,1,4,1,2076,112,20,3,1,1};
UINT4 FsSnmpTrapFilterRowStatus [ ] ={1,3,6,1,4,1,2076,112,20,3,1,2};
UINT4 FsSnmpTargetHostName [ ] ={1,3,6,1,4,1,2076,112,25,1,1,1};




tMbDbEntry fssnmpMibEntry[]= {

{{9,SnmpInInformResponses}, NULL, SnmpInInformResponsesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,SnmpOutInformRequests}, NULL, SnmpOutInformRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,SnmpInformDrops}, NULL, SnmpInformDropsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,SnmpInformAwaitingAck}, NULL, SnmpInformAwaitingAckGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,SnmpListenTrapPort}, NULL, SnmpListenTrapPortGet, SnmpListenTrapPortSet, SnmpListenTrapPortTest, SnmpListenTrapPortDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "162"},

{{9,SnmpOverTcpStatus}, NULL, SnmpOverTcpStatusGet, SnmpOverTcpStatusSet, SnmpOverTcpStatusTest, SnmpOverTcpStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{9,SnmpListenTcpPort}, NULL, SnmpListenTcpPortGet, SnmpListenTcpPortSet, SnmpListenTcpPortTest, SnmpListenTcpPortDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "161"},

{{9,SnmpTrapOverTcpStatus}, NULL, SnmpTrapOverTcpStatusGet, SnmpTrapOverTcpStatusSet, SnmpTrapOverTcpStatusTest, SnmpTrapOverTcpStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{9,SnmpListenTcpTrapPort}, NULL, SnmpListenTcpTrapPortGet, SnmpListenTcpTrapPortSet, SnmpListenTcpTrapPortTest, SnmpListenTcpTrapPortDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "162"},

{{11,SnmpInformTgtAddrName}, GetNextIndexSnmpInformCntTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, SnmpInformCntTableINDEX, 1, 0, 0, NULL},

{{11,SnmpInformSent}, GetNextIndexSnmpInformCntTable, SnmpInformSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, SnmpInformCntTableINDEX, 1, 0, 0, NULL},

{{11,SnmpInformAwaitAck}, GetNextIndexSnmpInformCntTable, SnmpInformAwaitAckGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, SnmpInformCntTableINDEX, 1, 0, 0, NULL},

{{11,SnmpInformRetried}, GetNextIndexSnmpInformCntTable, SnmpInformRetriedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, SnmpInformCntTableINDEX, 1, 0, 0, NULL},

{{11,SnmpInformDropped}, GetNextIndexSnmpInformCntTable, SnmpInformDroppedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, SnmpInformCntTableINDEX, 1, 0, 0, NULL},

{{11,SnmpInformFailed}, GetNextIndexSnmpInformCntTable, SnmpInformFailedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, SnmpInformCntTableINDEX, 1, 0, 0, NULL},

{{11,SnmpInformResponses}, GetNextIndexSnmpInformCntTable, SnmpInformResponsesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, SnmpInformCntTableINDEX, 1, 0, 0, NULL},

{{9,SnmpColdStartTrapControl}, NULL, SnmpColdStartTrapControlGet, SnmpColdStartTrapControlSet, SnmpColdStartTrapControlTest, SnmpColdStartTrapControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{9,SnmpAgentControl}, NULL, SnmpAgentControlGet, SnmpAgentControlSet, SnmpAgentControlTest, SnmpAgentControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{9,SnmpAllowedPduVersions}, NULL, SnmpAllowedPduVersionsGet, SnmpAllowedPduVersionsSet, SnmpAllowedPduVersionsTest, SnmpAllowedPduVersionsDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "3"},

{{9,SnmpMinimumSecurityRequired}, NULL, SnmpMinimumSecurityRequiredGet, SnmpMinimumSecurityRequiredSet, SnmpMinimumSecurityRequiredTest, SnmpMinimumSecurityRequiredDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,SnmpAgentxTransportDomain}, NULL, SnmpAgentxTransportDomainGet, SnmpAgentxTransportDomainSet, SnmpAgentxTransportDomainTest, SnmpAgentxTransportDomainDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,SnmpAgentxMasterAgentAddr}, NULL, SnmpAgentxMasterAgentAddrGet, SnmpAgentxMasterAgentAddrSet, SnmpAgentxMasterAgentAddrTest, SnmpAgentxMasterAgentAddrDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,SnmpAgentxMasterAgentPortNo}, NULL, SnmpAgentxMasterAgentPortNoGet, SnmpAgentxMasterAgentPortNoSet, SnmpAgentxMasterAgentPortNoTest, SnmpAgentxMasterAgentPortNoDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "705"},

{{10,SnmpAgentxSubAgentInPkts}, NULL, SnmpAgentxSubAgentInPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,SnmpAgentxSubAgentOutPkts}, NULL, SnmpAgentxSubAgentOutPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,SnmpAgentxSubAgentPktDrops}, NULL, SnmpAgentxSubAgentPktDropsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,SnmpAgentxSubAgentParseDrops}, NULL, SnmpAgentxSubAgentParseDropsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,SnmpAgentxSubAgentInOpenFail}, NULL, SnmpAgentxSubAgentInOpenFailGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,SnmpAgentxSubAgentOpenPktCnt}, NULL, SnmpAgentxSubAgentOpenPktCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,SnmpAgentxSubAgentInClosePktCnt}, NULL, SnmpAgentxSubAgentInClosePktCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,SnmpAgentxSubAgentOutClosePktCnt}, NULL, SnmpAgentxSubAgentOutClosePktCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,SnmpAgentxSubAgentIdAllocPktCnt}, NULL, SnmpAgentxSubAgentIdAllocPktCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,SnmpAgentxSubAgentIdDllocPktCnt}, NULL, SnmpAgentxSubAgentIdDllocPktCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,SnmpAgentxSubAgentRegPktCnt}, NULL, SnmpAgentxSubAgentRegPktCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,SnmpAgentxSubAgentUnRegPktCnt}, NULL, SnmpAgentxSubAgentUnRegPktCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,SnmpAgentxSubAgentAddCapsCnt}, NULL, SnmpAgentxSubAgentAddCapsCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,SnmpAgentxSubAgentRemCapsCnt}, NULL, SnmpAgentxSubAgentRemCapsCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,SnmpAgentxSubAgentNotifyPktCnt}, NULL, SnmpAgentxSubAgentNotifyPktCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,SnmpAgentxSubAgentPingCnt}, NULL, SnmpAgentxSubAgentPingCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,SnmpAgentxSubAgentInGets}, NULL, SnmpAgentxSubAgentInGetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,SnmpAgentxSubAgentInGetNexts}, NULL, SnmpAgentxSubAgentInGetNextsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,SnmpAgentxSubAgentInGetBulks}, NULL, SnmpAgentxSubAgentInGetBulksGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,SnmpAgentxSubAgentInTestSets}, NULL, SnmpAgentxSubAgentInTestSetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,SnmpAgentxSubAgentInCommits}, NULL, SnmpAgentxSubAgentInCommitsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,SnmpAgentxSubAgentInCleanups}, NULL, SnmpAgentxSubAgentInCleanupsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,SnmpAgentxSubAgentInUndos}, NULL, SnmpAgentxSubAgentInUndosGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,SnmpAgentxSubAgentOutResponse}, NULL, SnmpAgentxSubAgentOutResponseGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,SnmpAgentxSubAgentInResponse}, NULL, SnmpAgentxSubAgentInResponseGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,SnmpAgentxSubAgentControl}, NULL, SnmpAgentxSubAgentControlGet, SnmpAgentxSubAgentControlSet, SnmpAgentxSubAgentControlTest, SnmpAgentxSubAgentControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,SnmpAgentxContextName}, NULL, SnmpAgentxContextNameGet, SnmpAgentxContextNameSet, SnmpAgentxContextNameTest, SnmpAgentxContextNameDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{9,SnmpInRollbackErrs}, NULL, SnmpInRollbackErrsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,SnmpProxyListenTrapPort}, NULL, SnmpProxyListenTrapPortGet, SnmpProxyListenTrapPortSet, SnmpProxyListenTrapPortTest, SnmpProxyListenTrapPortDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "162"},

{{11,FsSnmpProxyMibName}, GetNextIndexFsSnmpProxyTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsSnmpProxyTableINDEX, 1, 0, 0, NULL},

{{11,FsSnmpProxyMibType}, GetNextIndexFsSnmpProxyTable, FsSnmpProxyMibTypeGet, FsSnmpProxyMibTypeSet, FsSnmpProxyMibTypeTest, FsSnmpProxyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnmpProxyTableINDEX, 1, 0, 0, NULL},

{{11,FsSnmpProxyMibId}, GetNextIndexFsSnmpProxyTable, FsSnmpProxyMibIdGet, FsSnmpProxyMibIdSet, FsSnmpProxyMibIdTest, FsSnmpProxyTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, FsSnmpProxyTableINDEX, 1, 0, 0, NULL},

{{11,FsSnmpProxyMibTargetParamsIn}, GetNextIndexFsSnmpProxyTable, FsSnmpProxyMibTargetParamsInGet, FsSnmpProxyMibTargetParamsInSet, FsSnmpProxyMibTargetParamsInTest, FsSnmpProxyTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsSnmpProxyTableINDEX, 1, 0, 0, NULL},

{{11,FsSnmpProxyMibSingleTargetOut}, GetNextIndexFsSnmpProxyTable, FsSnmpProxyMibSingleTargetOutGet, FsSnmpProxyMibSingleTargetOutSet, FsSnmpProxyMibSingleTargetOutTest, FsSnmpProxyTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsSnmpProxyTableINDEX, 1, 0, 0, NULL},

{{11,FsSnmpProxyMibMultipleTargetOut}, GetNextIndexFsSnmpProxyTable, FsSnmpProxyMibMultipleTargetOutGet, FsSnmpProxyMibMultipleTargetOutSet, FsSnmpProxyMibMultipleTargetOutTest, FsSnmpProxyTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsSnmpProxyTableINDEX, 1, 0, 0, NULL},

{{11,FsSnmpProxyMibStorageType}, GetNextIndexFsSnmpProxyTable, FsSnmpProxyMibStorageTypeGet, FsSnmpProxyMibStorageTypeSet, FsSnmpProxyMibStorageTypeTest, FsSnmpProxyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnmpProxyTableINDEX, 1, 0, 0, "3"},

{{11,FsSnmpProxyMibRowStatus}, GetNextIndexFsSnmpProxyTable, FsSnmpProxyMibRowStatusGet, FsSnmpProxyMibRowStatusSet, FsSnmpProxyMibRowStatusTest, FsSnmpProxyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnmpProxyTableINDEX, 1, 0, 1, NULL},

{{9,FsSnmpListenAgentPort}, NULL, FsSnmpListenAgentPortGet, FsSnmpListenAgentPortSet, FsSnmpListenAgentPortTest, FsSnmpListenAgentPortDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "161"},

{{12,FsSnmpTrapFilterOID}, GetNextIndexFsSnmpTrapFilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_NOACCESS, FsSnmpTrapFilterTableINDEX, 1, 0, 0, NULL},

{{12,FsSnmpTrapFilterRowStatus}, GetNextIndexFsSnmpTrapFilterTable, FsSnmpTrapFilterRowStatusGet, FsSnmpTrapFilterRowStatusSet, FsSnmpTrapFilterRowStatusTest, FsSnmpTrapFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnmpTrapFilterTableINDEX, 1, 0, 1, NULL},

{{9,FsSnmpEngineID}, NULL, FsSnmpEngineIDGet, FsSnmpEngineIDSet, FsSnmpEngineIDTest, FsSnmpEngineIDDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsSnmpTargetHostName}, GetNextIndexFsSnmpTargetAddrTable, FsSnmpTargetHostNameGet, FsSnmpTargetHostNameSet, FsSnmpTargetHostNameTest, FsSnmpTargetAddrTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsSnmpTargetAddrTableINDEX, 1, 0, 0, NULL},
};
tMibData fssnmpEntry = { 65, fssnmpMibEntry };

#endif /* _FSSNMPDB_H */
#endif /* _FSSNMPDB_H */

