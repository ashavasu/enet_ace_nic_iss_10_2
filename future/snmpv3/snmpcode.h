
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: snmpcode.h,v 1.5 2017/11/20 13:11:26 siva Exp $
 *
 * Description:macros and prototypes for snmpcode.c
 *******************************************************************/

#ifndef _SNMPCODE_H
#define _SNMPCODE_H

UINT1               gau1OutDat[MAX_PKT_LENGTH];
UINT1               gau1RevDat[MAX_PKT_LENGTH];
UINT1               gau1CommunityData[SNMP_MAX_OCTETSTRING_SIZE];
UINT1               gau1AgentAddress[SNMP_MAX_OCTETSTRING_SIZE];
UINT4 gau4NotInTimeWindowOID[]={1,3,6,1,6,3,15,1,1,2,0};
tSNMP_OCTET_STRING_TYPE Community;

tSNMP_TRAP_PDU  gV1TrapPdu;
tSNMP_OCTET_STRING_TYPE AgentAddress;
tSNMP_OID_TYPE EnterpriseOid;
UINT4 EnterpriseOidList[MAX_OID_LENGTH];

tSNMP_NORMAL_PDU    gRxPdu;
UINT1 gau1MsgEngineID[SNMP_MAX_OCTETSTRING_SIZE];
UINT1 gau1MsgUsrName[SNMP_MAX_OCTETSTRING_SIZE];
UINT1 gau1MsgAuthParam[SNMP_MAX_OCTETSTRING_SIZE];
UINT1 gau1MsgPrivParam[SNMP_MAX_OCTETSTRING_SIZE];
UINT1 gau1MsgFlag[SNMP_MAX_OCTETSTRING_SIZE];
UINT1 gau1ContextID[SNMP_MAX_OCTETSTRING_SIZE];
UINT1 gau1ContextName[SNMP_MAX_OCTETSTRING_SIZE];
INT4 SNMPDecodeSecurityParams (UINT1 **, UINT1 *, tSNMP_V3PDU *);
UINT1 *SNMPWriteSecurityParams (UINT1 **, tSNMP_V3PDU *,tSnmpUsmEntry *);
INT4 SNMPValidateTimeSync(tSNMP_V3PDU *pV3Pdu);

INT2 SNMPAddVarbindList (tSNMP_NORMAL_PDU *msg_ptr,
                            tSNMP_VAR_BIND * vb_ptr);
VOID SNMPFreeNormalMsg (tSNMP_NORMAL_PDU *msg_ptr);
VOID SNMPFreeV1TrapMsg(tSNMP_TRAP_PDU *pPdu);
tSNMP_OCTET_STRING_TYPE *
SNMPFormOctetString (UINT1 *u1_string,INT4 i4_length,
                             tSNMP_OCTET_STRING_TYPE *octetstring);
INT4 SNMPEstimateOidLength (tSNMP_OID_TYPE *oid_ptr);
INT4 SNMPEstimateOctetstringLength (tSNMP_OCTET_STRING_TYPE *os_ptr);
INT4 SNMPWriteGetMsg (UINT1 **ppu1_curr_ptr, 
                              tSNMP_NORMAL_PDU * msg_ptr, INT4 i4TypeOfMsg);
INT4 SNMPWriteLength (UINT1 **ppu1_curr_ptr,INT4 i4_length);
INT4 SNMPWriteOctetstring (UINT1 **ppu1_curr_ptr,
                              tSNMP_OCTET_STRING_TYPE * os_ptr, INT2 i2_type);
INT4 SNMPWriteUnsignedint (UINT1 **ppu1_curr_ptr, UINT4 u4_value,
                              INT2 i2_type);
INT4 SNMPEstimateUnsignedintLength (UINT4 u4_value);
INT4 SNMPEstimateLengthLength (INT4 i4_length);
INT4 SNMPFindLength (INT4 i4_len);
INT4 SNMPEstimateCounter64Length (tSNMP_COUNTER64_TYPE u8_value);
INT4 SNMPEstimateSignedintLength (INT4 i4_value);
VOID SNMPReverseVarBindList (tSNMP_VAR_BIND **pVarBindList);
INT4 SNMPWriteVarbind (UINT1 **ppu1_curr_ptr,tSNMP_VAR_BIND * vb_ptr);
INT4 SNMPWriteCounter64 (UINT1 **ppu1_curr_ptr,
                            tSNMP_COUNTER64_TYPE u8_value, INT2 i2_type);
INT4 SNMPWriteSignedint (UINT1 **pu1_curr, INT4 i4_value, INT2 i2_type);
INT4 SNMPWriteOid (UINT1 **ppu1_curr_ptr, tSNMP_OID_TYPE * oid_ptr);
VOID SNMPWriteNull (UINT1 **ppu1_curr_ptr);
VOID SNMPWriteException (UINT1 **ppu1_curr_ptr, INT2 i2_DataType);
INT4 SNMPDecodeSequence (UINT1 **ppu1_curr_ptr,UINT1 * pu1_end_ptr);
INT4 SNMPDecodeTypeLen (UINT1 **ppu1_curr_ptr, const UINT1 *pu1_end_ptr, 
                          INT2 *i2_type);
INT4 SNMPDecodeInteger (UINT1 **pu1_curr, UINT1 *pu1_end, INT2 *i2_type,
                           INT2 i2_unsign_or_sign);
tSNMP_OCTET_STRING_TYPE *
SNMPDecodeOctetstring (UINT1 **ppu1_curr_ptr, UINT1 *pu1_end_ptr, 
                               INT2 *i2_type,tSNMP_OCTET_STRING_TYPE * os_ptr);
tSNMP_OID_TYPE * SNMPDecodeOid (UINT1 **ppu1_curr_ptr, 
                       UINT1 *pu1_end_ptr, tSNMP_OID_TYPE *oid_ptr);
tSNMP_VAR_BIND *
SNMPDecodeVarbind (UINT1 **ppu1_curr_ptr,UINT1 * pu1_end_ptr);
VOID SNMPDecodeCounter64Value (UINT1 **pu1_curr, UINT1 *pu1_end, 
      INT2 *i2_type,tSNMP_COUNTER64_TYPE * u8_value);
INT2 SNMPDecodeNull (UINT1 **ppu1_curr_ptr,UINT1 * pu1_end_ptr,
                        INT2 * pi2_type);
INT4 SNMPReversePacket (UINT1 *pu1_in_ptr, UINT1 **ppu1_out_ptr,
                           INT4  i4_length);
INT4 SNMPWriteTrapMsg (UINT1 **, tSNMP_TRAP_PDU *);
INT4 SNMPDynamicReversePacket (UINT1 *, UINT1 **, INT4);

#endif /* _SNMPCODE_H */
