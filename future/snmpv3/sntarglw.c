/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: sntarglw.c,v 1.9 2017/11/20 13:11:27 siva Exp $
*
* Description: snmp target table Low Level Routines
*********************************************************************/
#include  "lr.h"
#include  "snmpcmn.h"
#include  "sntarglw.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetSnmpTargetSpinLock
 Input       :  The Indices

                The Object 
                retValSnmpTargetSpinLock
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpTargetSpinLock (INT4 *pi4RetValSnmpTargetSpinLock)
{
    UNUSED_PARAM (pi4RetValSnmpTargetSpinLock);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetSnmpTargetSpinLock
 Input       :  The Indices

                The Object 
                setValSnmpTargetSpinLock
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpTargetSpinLock (INT4 i4SetValSnmpTargetSpinLock)
{
    UNUSED_PARAM (i4SetValSnmpTargetSpinLock);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2SnmpTargetSpinLock
 Input       :  The Indices

                The Object 
                testValSnmpTargetSpinLock
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpTargetSpinLock (UINT4 *pu4ErrorCode,
                             INT4 i4TestValSnmpTargetSpinLock)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValSnmpTargetSpinLock);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2SnmpTargetSpinLock
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SnmpTargetSpinLock (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : SnmpTargetAddrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceSnmpTargetAddrTable
 Input       :  The Indices
                SnmpTargetAddrName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceSnmpTargetAddrTable (tSNMP_OCTET_STRING_TYPE *
                                             pSnmpTargetAddrName)
{

    if ((pSnmpTargetAddrName->i4_Length <= SNMP_ZERO) ||
        (pSnmpTargetAddrName->i4_Length > SNMP_MAX_OCTETSTRING_SIZE))

    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexSnmpTargetAddrTable
 Input       :  The Indices
                SnmpTargetAddrName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexSnmpTargetAddrTable (tSNMP_OCTET_STRING_TYPE *
                                     pSnmpTargetAddrName)
{
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    pSnmpTgtAddrEntry = SNMPGetFirstTgtAddrEntry ();
    if (pSnmpTgtAddrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    SNMPCopyOctetString (pSnmpTargetAddrName, &(pSnmpTgtAddrEntry->AddrName));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexSnmpTargetAddrTable
 Input       :  The Indices
                SnmpTargetAddrName
                nextSnmpTargetAddrName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexSnmpTargetAddrTable (tSNMP_OCTET_STRING_TYPE *
                                    pSnmpTargetAddrName,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pNextSnmpTargetAddrName)
{
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    pSnmpTgtAddrEntry = SNMPGetNextTgtAddrEntry (pSnmpTargetAddrName);
    if (pSnmpTgtAddrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    SNMPCopyOctetString (pNextSnmpTargetAddrName,
                         &(pSnmpTgtAddrEntry->AddrName));
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetSnmpTargetAddrTDomain
 Input       :  The Indices
                SnmpTargetAddrName

                The Object 
                retValSnmpTargetAddrTDomain
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpTargetAddrTDomain (tSNMP_OCTET_STRING_TYPE * pSnmpTargetAddrName,
                             tSNMP_OID_TYPE * pRetValSnmpTargetAddrTDomain)
{
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    pSnmpTgtAddrEntry = SNMPGetTgtAddrEntry (pSnmpTargetAddrName);
    if (pSnmpTgtAddrEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOid (pRetValSnmpTargetAddrTDomain, &(pSnmpTgtAddrEntry->Domain));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpTargetAddrTAddress
 Input       :  The Indices
                SnmpTargetAddrName

                The Object 
                retValSnmpTargetAddrTAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpTargetAddrTAddress (tSNMP_OCTET_STRING_TYPE * pSnmpTargetAddrName,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValSnmpTargetAddrTAddress)
{
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    UINT2               u2Port = 0;

    pSnmpTgtAddrEntry = SNMPGetTgtAddrEntry (pSnmpTargetAddrName);

    if (pSnmpTgtAddrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    SNMPCopyOctetString (pRetValSnmpTargetAddrTAddress,
                         &(pSnmpTgtAddrEntry->Address));

    if (pRetValSnmpTargetAddrTAddress->i4_Length == SNMP_IPADDR_LEN)
    {
        u2Port = OSIX_NTOHS (pSnmpTgtAddrEntry->u2Port);
        MEMCPY ((pRetValSnmpTargetAddrTAddress->pu1_OctetList +
                 SNMP_IPADDR_LEN), &u2Port, SNMP_TWO);
        pRetValSnmpTargetAddrTAddress->i4_Length = SNMP_IPADDR_LEN + SNMP_TWO;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpTargetAddrTimeout
 Input       :  The Indices
                SnmpTargetAddrName

                The Object
                retValSnmpTargetAddrTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpTargetAddrTimeout (tSNMP_OCTET_STRING_TYPE * pSnmpTargetAddrName,
                             INT4 *pi4RetValSnmpTargetAddrTimeout)
{
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    pSnmpTgtAddrEntry = SNMPGetTgtAddrEntry (pSnmpTargetAddrName);
    if (pSnmpTgtAddrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValSnmpTargetAddrTimeout = pSnmpTgtAddrEntry->i4TimeOut;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpTargetAddrRetryCount
 Input       :  The Indices
                SnmpTargetAddrName

                The Object
                retValSnmpTargetAddrRetryCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpTargetAddrRetryCount (tSNMP_OCTET_STRING_TYPE * pSnmpTargetAddrName,
                                INT4 *pi4RetValSnmpTargetAddrRetryCount)
{
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    pSnmpTgtAddrEntry = SNMPGetTgtAddrEntry (pSnmpTargetAddrName);
    if (pSnmpTgtAddrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValSnmpTargetAddrRetryCount = pSnmpTgtAddrEntry->i4RetryCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpTargetAddrTagList
 Input       :  The Indices
                SnmpTargetAddrName

                The Object 
                retValSnmpTargetAddrTagList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpTargetAddrTagList (tSNMP_OCTET_STRING_TYPE * pSnmpTargetAddrName,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValSnmpTargetAddrTagList)
{
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    pSnmpTgtAddrEntry = SNMPGetTgtAddrEntry (pSnmpTargetAddrName);
    if (pSnmpTgtAddrEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pRetValSnmpTargetAddrTagList,
                         &(pSnmpTgtAddrEntry->TagList));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpTargetAddrParams
 Input       :  The Indices
                SnmpTargetAddrName

                The Object 
                retValSnmpTargetAddrParams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpTargetAddrParams (tSNMP_OCTET_STRING_TYPE * pSnmpTargetAddrName,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValSnmpTargetAddrParams)
{
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    pSnmpTgtAddrEntry = SNMPGetTgtAddrEntry (pSnmpTargetAddrName);
    if (pSnmpTgtAddrEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pRetValSnmpTargetAddrParams,
                         &(pSnmpTgtAddrEntry->AddrParams));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpTargetAddrStorageType
 Input       :  The Indices
                SnmpTargetAddrName

                The Object 
                retValSnmpTargetAddrStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpTargetAddrStorageType (tSNMP_OCTET_STRING_TYPE * pSnmpTargetAddrName,
                                 INT4 *pi4RetValSnmpTargetAddrStorageType)
{
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    pSnmpTgtAddrEntry = SNMPGetTgtAddrEntry (pSnmpTargetAddrName);
    if (pSnmpTgtAddrEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValSnmpTargetAddrStorageType = pSnmpTgtAddrEntry->i4StorageType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpTargetAddrRowStatus
 Input       :  The Indices
                SnmpTargetAddrName

                The Object 
                retValSnmpTargetAddrRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpTargetAddrRowStatus (tSNMP_OCTET_STRING_TYPE * pSnmpTargetAddrName,
                               INT4 *pi4RetValSnmpTargetAddrRowStatus)
{
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    pSnmpTgtAddrEntry = SNMPGetTgtAddrEntry (pSnmpTargetAddrName);
    if (pSnmpTgtAddrEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValSnmpTargetAddrRowStatus = pSnmpTgtAddrEntry->i4Status;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetSnmpTargetAddrTDomain
 Input       :  The Indices
                SnmpTargetAddrName

                The Object 
                setValSnmpTargetAddrTDomain
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpTargetAddrTDomain (tSNMP_OCTET_STRING_TYPE * pSnmpTargetAddrName,
                             tSNMP_OID_TYPE * pSetValSnmpTargetAddrTDomain)
{
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    pSnmpTgtAddrEntry = SNMPGetTgtAddrEntry (pSnmpTargetAddrName);
    if (pSnmpTgtAddrEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOid (&(pSnmpTgtAddrEntry->Domain), pSetValSnmpTargetAddrTDomain);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpTargetAddrTAddress
 Input       :  The Indices
                SnmpTargetAddrName

                The Object 
                setValSnmpTargetAddrTAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpTargetAddrTAddress (tSNMP_OCTET_STRING_TYPE * pSnmpTargetAddrName,
                              tSNMP_OCTET_STRING_TYPE *
                              pSetValSnmpTargetAddrTAddress)
{
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    pSnmpTgtAddrEntry = SNMPGetTgtAddrEntry (pSnmpTargetAddrName);

    if (pSnmpTgtAddrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pSetValSnmpTargetAddrTAddress->i4_Length == SNMP_IPADDR_LEN + SNMP_TWO)
    {
        MEMCPY (pSnmpTgtAddrEntry->Address.pu1_OctetList,
                pSetValSnmpTargetAddrTAddress->pu1_OctetList, SNMP_IPADDR_LEN);

        MEMCPY (&(pSnmpTgtAddrEntry->u2Port),
                (pSetValSnmpTargetAddrTAddress->pu1_OctetList +
                 SNMP_IPADDR_LEN), SNMP_TWO);
        pSnmpTgtAddrEntry->u2Port = OSIX_HTONS (pSnmpTgtAddrEntry->u2Port);
        pSnmpTgtAddrEntry->Address.i4_Length = SNMP_IPADDR_LEN;
    }
    else
    {
        MEMCPY (pSnmpTgtAddrEntry->Address.pu1_OctetList,
                pSetValSnmpTargetAddrTAddress->pu1_OctetList, SNMP_IP6ADDR_LEN);

        MEMCPY (&(pSnmpTgtAddrEntry->u2Port),
                (pSetValSnmpTargetAddrTAddress->pu1_OctetList +
                 SNMP_IP6ADDR_LEN), SNMP_TWO);
        pSnmpTgtAddrEntry->Address.i4_Length = SNMP_IP6ADDR_LEN;

    }
    pSnmpTgtAddrEntry->b1HostName = SNMP_FALSE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpTargetAddrTimeout
 Input       :  The Indices
                SnmpTargetAddrName

                The Object
                setValSnmpTargetAddrTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpTargetAddrTimeout (tSNMP_OCTET_STRING_TYPE * pSnmpTargetAddrName,
                             INT4 i4SetValSnmpTargetAddrTimeout)
{
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    pSnmpTgtAddrEntry = SNMPGetTgtAddrEntry (pSnmpTargetAddrName);
    if (pSnmpTgtAddrEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pSnmpTgtAddrEntry->i4TimeOut = i4SetValSnmpTargetAddrTimeout;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpTargetAddrRetryCount
 Input       :  The Indices
                SnmpTargetAddrName

                The Object
                setValSnmpTargetAddrRetryCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpTargetAddrRetryCount (tSNMP_OCTET_STRING_TYPE * pSnmpTargetAddrName,
                                INT4 i4SetValSnmpTargetAddrRetryCount)
{
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    pSnmpTgtAddrEntry = SNMPGetTgtAddrEntry (pSnmpTargetAddrName);
    if (pSnmpTgtAddrEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pSnmpTgtAddrEntry->i4RetryCount = i4SetValSnmpTargetAddrRetryCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpTargetAddrTagList
 Input       :  The Indices
                SnmpTargetAddrName

                The Object 
                setValSnmpTargetAddrTagList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpTargetAddrTagList (tSNMP_OCTET_STRING_TYPE * pSnmpTargetAddrName,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValSnmpTargetAddrTagList)
{
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    pSnmpTgtAddrEntry = SNMPGetTgtAddrEntry (pSnmpTargetAddrName);
    if (pSnmpTgtAddrEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (&(pSnmpTgtAddrEntry->TagList),
                         pSetValSnmpTargetAddrTagList);
    pSnmpTgtAddrEntry->TagList.pu1_OctetList
        [pSnmpTgtAddrEntry->TagList.i4_Length] = SNMP_ZERO;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpTargetAddrParams
 Input       :  The Indices
                SnmpTargetAddrName

                The Object 
                setValSnmpTargetAddrParams
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpTargetAddrParams (tSNMP_OCTET_STRING_TYPE * pSnmpTargetAddrName,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValSnmpTargetAddrParams)
{
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    pSnmpTgtAddrEntry = SNMPGetTgtAddrEntry (pSnmpTargetAddrName);
    if (pSnmpTgtAddrEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (&(pSnmpTgtAddrEntry->AddrParams),
                         pSetValSnmpTargetAddrParams);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpTargetAddrStorageType
 Input       :  The Indices
                SnmpTargetAddrName

                The Object 
                setValSnmpTargetAddrStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpTargetAddrStorageType (tSNMP_OCTET_STRING_TYPE *
                                 pSnmpTargetAddrName,
                                 INT4 i4SetValSnmpTargetAddrStorageType)
{
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    pSnmpTgtAddrEntry = SNMPGetTgtAddrEntry (pSnmpTargetAddrName);
    if (pSnmpTgtAddrEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pSnmpTgtAddrEntry->i4StorageType = i4SetValSnmpTargetAddrStorageType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpTargetAddrRowStatus
 Input       :  The Indices
                SnmpTargetAddrName

                The Object 
                setValSnmpTargetAddrRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpTargetAddrRowStatus (tSNMP_OCTET_STRING_TYPE * pSnmpTargetAddrName,
                               INT4 i4SetValSnmpTargetAddrRowStatus)
{
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    pSnmpTgtAddrEntry = SNMPGetTgtAddrEntry (pSnmpTargetAddrName);
    switch (i4SetValSnmpTargetAddrRowStatus)
    {
        case SNMP_ROWSTATUS_CREATEANDWAIT:
            if (pSnmpTgtAddrEntry != NULL)
            {
                return SNMP_FAILURE;
            }
            pSnmpTgtAddrEntry = SNMPCreateTgtAddrEntry (pSnmpTargetAddrName);
            if (pSnmpTgtAddrEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            break;
        case SNMP_ROWSTATUS_ACTIVE:
            if (pSnmpTgtAddrEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            if (pSnmpTgtAddrEntry->i4Status != UNDER_CREATION)
            {
                return SNMP_FAILURE;
            }
            pSnmpTgtAddrEntry->i4Status = SNMP_ACTIVE;
            break;
        case SNMP_ROWSTATUS_NOTINSERVICE:
            if (pSnmpTgtAddrEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            pSnmpTgtAddrEntry->i4Status = UNDER_CREATION;
            break;
        case SNMP_ROWSTATUS_DESTROY:
            if (pSnmpTgtAddrEntry != NULL)
            {
                if (SNMPDeleteTgtAddrEntry (pSnmpTargetAddrName) ==
                    SNMP_FAILURE)
                {
                    return SNMP_FAILURE;
                }
            }
            break;
        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2SnmpTargetAddrTDomain
 Input       :  The Indices
                SnmpTargetAddrName

                The Object 
                testValSnmpTargetAddrTDomain
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpTargetAddrTDomain (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE * pSnmpTargetAddrName,
                                tSNMP_OID_TYPE * pTestValSnmpTargetAddrTDomain)
{
    if (SNMPGetTgtAddrEntry (pSnmpTargetAddrName) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (pTestValSnmpTargetAddrTDomain->u4_Length == SNMP_ZERO)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpTargetAddrTAddress
 Input       :  The Indices
                SnmpTargetAddrName

                The Object 
                testValSnmpTargetAddrTAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpTargetAddrTAddress (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE * pSnmpTargetAddrName,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pTestValSnmpTargetAddrTAddress)
{
    UINT4               u4IpAddr = 0;
    if (SNMPGetTgtAddrEntry (pSnmpTargetAddrName) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((pTestValSnmpTargetAddrTAddress->i4_Length !=
         SNMP_IPADDR_LEN + SNMP_TWO)
        && (pTestValSnmpTargetAddrTAddress->i4_Length !=
            SNMP_IP6ADDR_LEN + SNMP_TWO))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    else if (pTestValSnmpTargetAddrTAddress->i4_Length == 6)
    {
        u4IpAddr =
            *(UINT4 *) (VOID *) pTestValSnmpTargetAddrTAddress->pu1_OctetList;
        if (((u4IpAddr & 0xff000000) < 0x01000000)
            || ((u4IpAddr & 0xff000000) > 0xfe000000))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        if (!((IP_IS_ADDR_CLASS_A (OSIX_HTONL (u4IpAddr)) ?
               (IP_IS_ZERO_NETWORK (OSIX_HTONL (u4IpAddr)) ? 0 : 1) : 0) ||
              (IP_IS_ADDR_CLASS_B (OSIX_HTONL (u4IpAddr))) ||
              (IP_IS_ADDR_CLASS_C (OSIX_HTONL (u4IpAddr)))))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        if ((u4IpAddr & 0x0000007f) == 0x0000007f)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpTargetAddrTimeout
 Input       :  The Indices
                SnmpTargetAddrName

                The Object
                testValSnmpTargetAddrTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpTargetAddrTimeout (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE * pSnmpTargetAddrName,
                                INT4 i4TestValSnmpTargetAddrTimeout)
{
    if (SNMPGetTgtAddrEntry (pSnmpTargetAddrName) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValSnmpTargetAddrTimeout <= 0) ||
        (i4TestValSnmpTargetAddrTimeout > SNMP_INFM_MAX_TIMEOUT_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpTargetAddrRetryCount
 Input       :  The Indices
                SnmpTargetAddrName

                The Object
                testValSnmpTargetAddrRetryCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpTargetAddrRetryCount (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pSnmpTargetAddrName,
                                   INT4 i4TestValSnmpTargetAddrRetryCount)
{
    if (SNMPGetTgtAddrEntry (pSnmpTargetAddrName) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValSnmpTargetAddrRetryCount <= 0) ||
        (i4TestValSnmpTargetAddrRetryCount > SNMP_INFM_DEF_RETRY_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpTargetAddrTagList
 Input       :  The Indices
                SnmpTargetAddrName

                The Object 
                testValSnmpTargetAddrTagList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpTargetAddrTagList (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE * pSnmpTargetAddrName,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValSnmpTargetAddrTagList)
{
    UINT4               u4Count = SNMP_ZERO;
    UINT1              *pu1Name = NULL;

    pu1Name = pTestValSnmpTargetAddrTagList->pu1_OctetList;

    if (SNMPGetTgtAddrEntry (pSnmpTargetAddrName) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if ((pTestValSnmpTargetAddrTagList->i4_Length < SNMP_ZERO) ||
        (pTestValSnmpTargetAddrTagList->i4_Length > MAX_STR_OCTET_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    /* Tag value will not have a delimiter character. Delimiter 
     * characters are defined to be one of the following: 
     * An ASCII space character = (0x20) 
     * An ASCII TAB character = (0x09)
     * An ASCII carriage return (CR) character = (0x0D)
     * An ASCII line feed (LF) character = (0x0A)           
     */

    for (u4Count = SNMP_ZERO; u4Count <
         (UINT4) pTestValSnmpTargetAddrTagList->i4_Length; u4Count++)
    {
        if (pu1Name[u4Count] < 0x21 || pu1Name[u4Count] > 0x7E)
        {
            if (!(pu1Name[u4Count] == 0x20 || pu1Name[u4Count] == 0x09 ||
                  pu1Name[u4Count] == 0x0d || pu1Name[u4Count] == 0x0a))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
        }
    }
    for (u4Count = SNMP_ZERO; u4Count <
         (UINT4) pTestValSnmpTargetAddrTagList->i4_Length; u4Count++)
    {
        if (u4Count == SNMP_ZERO || u4Count ==
            ((UINT4) pTestValSnmpTargetAddrTagList->i4_Length - 1))
        {
            if ((pu1Name[u4Count] == 0x20 || pu1Name[u4Count] == 0x09 ||
                 pu1Name[u4Count] == 0x0d || pu1Name[u4Count] == 0x0a))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
        }
        if (u4Count != ((UINT4) pTestValSnmpTargetAddrTagList->i4_Length - 1))
        {
            if ((pu1Name[u4Count] == 0x20 || pu1Name[u4Count] == 0x09 ||
                 pu1Name[u4Count] == 0x0d || pu1Name[u4Count] == 0x0a))
            {
                if ((pu1Name[u4Count + 1] == 0x20
                     || pu1Name[u4Count + 1] == 0x09
                     || pu1Name[u4Count + 1] == 0x0d
                     || pu1Name[u4Count + 1] == 0x0a))
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }
            }
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpTargetAddrParams
 Input       :  The Indices
                SnmpTargetAddrName

                The Object 
                testValSnmpTargetAddrParams
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpTargetAddrParams (UINT4 *pu4ErrorCode,
                               tSNMP_OCTET_STRING_TYPE * pSnmpTargetAddrName,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValSnmpTargetAddrParams)
{
    if (SNMPGetTgtAddrEntry (pSnmpTargetAddrName) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if ((pTestValSnmpTargetAddrParams->i4_Length <= SNMP_ZERO) ||
        (pTestValSnmpTargetAddrParams->i4_Length > SNMP_MAX_TGT_PARAM_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpTargetAddrStorageType
 Input       :  The Indices
                SnmpTargetAddrName

                The Object 
                testValSnmpTargetAddrStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpTargetAddrStorageType (UINT4 *pu4ErrorCode,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pSnmpTargetAddrName,
                                    INT4 i4TestValSnmpTargetAddrStorageType)
{
    if (SNMPGetTgtAddrEntry (pSnmpTargetAddrName) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if ((i4TestValSnmpTargetAddrStorageType < SNMP3_STORAGE_TYPE_OTHER) ||
        (i4TestValSnmpTargetAddrStorageType > SNMP3_STORAGE_TYPE_READONLY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValSnmpTargetAddrStorageType != SNMP3_STORAGE_TYPE_VOLATILE) &&
        (i4TestValSnmpTargetAddrStorageType != SNMP3_STORAGE_TYPE_NONVOLATILE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpTargetAddrRowStatus
 Input       :  The Indices
                SnmpTargetAddrName

                The Object 
                testValSnmpTargetAddrRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpTargetAddrRowStatus (UINT4 *pu4ErrorCode,
                                  tSNMP_OCTET_STRING_TYPE * pSnmpTargetAddrName,
                                  INT4 i4TestValSnmpTargetAddrRowStatus)
{
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    pSnmpTgtAddrEntry = SNMPGetTgtAddrEntry (pSnmpTargetAddrName);

    switch (i4TestValSnmpTargetAddrRowStatus)
    {
        case SNMP_ROWSTATUS_CREATEANDWAIT:
            if (pSnmpTgtAddrEntry != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            break;
        case SNMP_ROWSTATUS_ACTIVE:
            if (pSnmpTgtAddrEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            if (pSnmpTgtAddrEntry->b1HostName == SNMP_TRUE)
            {
                if (pSnmpTgtAddrEntry->i4Status != UNDER_CREATION ||
                    pSnmpTgtAddrEntry->AddrParams.i4_Length == SNMP_ZERO)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
            else
            {
                if (pSnmpTgtAddrEntry->i4Status != UNDER_CREATION ||
                    pSnmpTgtAddrEntry->Address.i4_Length == SNMP_ZERO ||
                    pSnmpTgtAddrEntry->AddrParams.i4_Length == SNMP_ZERO)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
            break;
        case SNMP_ROWSTATUS_NOTINSERVICE:
            if (pSnmpTgtAddrEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            break;
        case SNMP_ROWSTATUS_DESTROY:
            if (pSnmpTgtAddrEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2SnmpTargetAddrTable
 Input       :  The Indices
                SnmpTargetAddrName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SnmpTargetAddrTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : SnmpTargetParamsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceSnmpTargetParamsTable
 Input       :  The Indices
                SnmpTargetParamsName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceSnmpTargetParamsTable (tSNMP_OCTET_STRING_TYPE *
                                               pSnmpTargetParamsName)
{
    if (pSnmpTargetParamsName->i4_Length == SNMP_ZERO)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexSnmpTargetParamsTable
 Input       :  The Indices
                SnmpTargetParamsName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexSnmpTargetParamsTable (tSNMP_OCTET_STRING_TYPE *
                                       pSnmpTargetParamsName)
{
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    pSnmpTgtParamEntry = SNMPGetFirstTgtParamEntry ();
    if (pSnmpTgtParamEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pSnmpTargetParamsName,
                         &(pSnmpTgtParamEntry->ParamName));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexSnmpTargetParamsTable
 Input       :  The Indices
                SnmpTargetParamsName
                nextSnmpTargetParamsName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexSnmpTargetParamsTable (tSNMP_OCTET_STRING_TYPE *
                                      pSnmpTargetParamsName,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pNextSnmpTargetParamsName)
{
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    pSnmpTgtParamEntry = SNMPGetNextTgtParamEntry (pSnmpTargetParamsName);
    if (pSnmpTgtParamEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pNextSnmpTargetParamsName,
                         &(pSnmpTgtParamEntry->ParamName));
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetSnmpTargetParamsMPModel
 Input       :  The Indices
                SnmpTargetParamsName

                The Object 
                retValSnmpTargetParamsMPModel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpTargetParamsMPModel (tSNMP_OCTET_STRING_TYPE * pSnmpTargetParamsName,
                               INT4 *pi4RetValSnmpTargetParamsMPModel)
{
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    pSnmpTgtParamEntry = SNMPGetTgtParamEntry (pSnmpTargetParamsName);
    if (pSnmpTgtParamEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValSnmpTargetParamsMPModel =
        (INT4) pSnmpTgtParamEntry->i2ParamMPModel;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpTargetParamsSecurityModel
 Input       :  The Indices
                SnmpTargetParamsName

                The Object 
                retValSnmpTargetParamsSecurityModel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpTargetParamsSecurityModel (tSNMP_OCTET_STRING_TYPE *
                                     pSnmpTargetParamsName,
                                     INT4
                                     *pi4RetValSnmpTargetParamsSecurityModel)
{
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    pSnmpTgtParamEntry = SNMPGetTgtParamEntry (pSnmpTargetParamsName);
    if (pSnmpTgtParamEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValSnmpTargetParamsSecurityModel =
        (INT4) pSnmpTgtParamEntry->i2ParamSecModel;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpTargetParamsSecurityName
 Input       :  The Indices
                SnmpTargetParamsName

                The Object 
                retValSnmpTargetParamsSecurityName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpTargetParamsSecurityName (tSNMP_OCTET_STRING_TYPE *
                                    pSnmpTargetParamsName,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValSnmpTargetParamsSecurityName)
{
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    pSnmpTgtParamEntry = SNMPGetTgtParamEntry (pSnmpTargetParamsName);
    if (pSnmpTgtParamEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pRetValSnmpTargetParamsSecurityName,
                         &(pSnmpTgtParamEntry->ParamSecName));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpTargetParamsSecurityLevel
 Input       :  The Indices
                SnmpTargetParamsName

                The Object 
                retValSnmpTargetParamsSecurityLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpTargetParamsSecurityLevel (tSNMP_OCTET_STRING_TYPE *
                                     pSnmpTargetParamsName,
                                     INT4
                                     *pi4RetValSnmpTargetParamsSecurityLevel)
{
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    pSnmpTgtParamEntry = SNMPGetTgtParamEntry (pSnmpTargetParamsName);
    if (pSnmpTgtParamEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValSnmpTargetParamsSecurityLevel =
        pSnmpTgtParamEntry->i4ParamSecLevel;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpTargetParamsStorageType
 Input       :  The Indices
                SnmpTargetParamsName

                The Object 
                retValSnmpTargetParamsStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpTargetParamsStorageType (tSNMP_OCTET_STRING_TYPE *
                                   pSnmpTargetParamsName,
                                   INT4 *pi4RetValSnmpTargetParamsStorageType)
{
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    pSnmpTgtParamEntry = SNMPGetTgtParamEntry (pSnmpTargetParamsName);
    if (pSnmpTgtParamEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValSnmpTargetParamsStorageType =
        (INT4) pSnmpTgtParamEntry->i2ParamStorageType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpTargetParamsRowStatus
 Input       :  The Indices
                SnmpTargetParamsName

                The Object 
                retValSnmpTargetParamsRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpTargetParamsRowStatus (tSNMP_OCTET_STRING_TYPE *
                                 pSnmpTargetParamsName,
                                 INT4 *pi4RetValSnmpTargetParamsRowStatus)
{
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    pSnmpTgtParamEntry = SNMPGetTgtParamEntry (pSnmpTargetParamsName);
    if (pSnmpTgtParamEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValSnmpTargetParamsRowStatus = (INT4) pSnmpTgtParamEntry->i2Status;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetSnmpTargetParamsMPModel
 Input       :  The Indices
                SnmpTargetParamsName

                The Object 
                setValSnmpTargetParamsMPModel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpTargetParamsMPModel (tSNMP_OCTET_STRING_TYPE *
                               pSnmpTargetParamsName,
                               INT4 i4SetValSnmpTargetParamsMPModel)
{
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    pSnmpTgtParamEntry = SNMPGetTgtParamEntry (pSnmpTargetParamsName);
    if (pSnmpTgtParamEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pSnmpTgtParamEntry->i2ParamMPModel = (INT2) i4SetValSnmpTargetParamsMPModel;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpTargetParamsSecurityModel
 Input       :  The Indices
                SnmpTargetParamsName

                The Object 
                setValSnmpTargetParamsSecurityModel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpTargetParamsSecurityModel (tSNMP_OCTET_STRING_TYPE *
                                     pSnmpTargetParamsName,
                                     INT4 i4SetValSnmpTargetParamsSecurityModel)
{
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    pSnmpTgtParamEntry = SNMPGetTgtParamEntry (pSnmpTargetParamsName);
    if (pSnmpTgtParamEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pSnmpTgtParamEntry->i2ParamSecModel = (INT2)
        i4SetValSnmpTargetParamsSecurityModel;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpTargetParamsSecurityName
 Input       :  The Indices
                SnmpTargetParamsName

                The Object 
                setValSnmpTargetParamsSecurityName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpTargetParamsSecurityName (tSNMP_OCTET_STRING_TYPE *
                                    pSnmpTargetParamsName,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pSetValSnmpTargetParamsSecurityName)
{
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    pSnmpTgtParamEntry = SNMPGetTgtParamEntry (pSnmpTargetParamsName);
    if (pSnmpTgtParamEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (&(pSnmpTgtParamEntry->ParamSecName),
                         pSetValSnmpTargetParamsSecurityName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpTargetParamsSecurityLevel
 Input       :  The Indices
                SnmpTargetParamsName

                The Object 
                setValSnmpTargetParamsSecurityLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpTargetParamsSecurityLevel (tSNMP_OCTET_STRING_TYPE *
                                     pSnmpTargetParamsName,
                                     INT4 i4SetValSnmpTargetParamsSecurityLevel)
{
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    pSnmpTgtParamEntry = SNMPGetTgtParamEntry (pSnmpTargetParamsName);
    if (pSnmpTgtParamEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pSnmpTgtParamEntry->i4ParamSecLevel = i4SetValSnmpTargetParamsSecurityLevel;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpTargetParamsStorageType
 Input       :  The Indices
                SnmpTargetParamsName

                The Object 
                setValSnmpTargetParamsStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpTargetParamsStorageType (tSNMP_OCTET_STRING_TYPE *
                                   pSnmpTargetParamsName,
                                   INT4 i4SetValSnmpTargetParamsStorageType)
{
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    pSnmpTgtParamEntry = SNMPGetTgtParamEntry (pSnmpTargetParamsName);
    if (pSnmpTgtParamEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pSnmpTgtParamEntry->i2ParamStorageType = (INT2)
        i4SetValSnmpTargetParamsStorageType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpTargetParamsRowStatus
 Input       :  The Indices
                SnmpTargetParamsName

                The Object 
                setValSnmpTargetParamsRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpTargetParamsRowStatus (tSNMP_OCTET_STRING_TYPE *
                                 pSnmpTargetParamsName,
                                 INT4 i4SetValSnmpTargetParamsRowStatus)
{
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    pSnmpTgtParamEntry = SNMPGetTgtParamEntry (pSnmpTargetParamsName);
    switch (i4SetValSnmpTargetParamsRowStatus)
    {
        case SNMP_ROWSTATUS_CREATEANDWAIT:
            if (pSnmpTgtParamEntry != NULL)
            {
                return SNMP_FAILURE;
            }
            pSnmpTgtParamEntry =
                SNMPCreateTgtParamEntry (pSnmpTargetParamsName);
            if (pSnmpTgtParamEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            break;
        case SNMP_ROWSTATUS_ACTIVE:
            if (pSnmpTgtParamEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            if (pSnmpTgtParamEntry->i2Status != UNDER_CREATION)
            {
                return SNMP_FAILURE;
            }
            pSnmpTgtParamEntry->i2Status = SNMP_ACTIVE;
            break;
        case SNMP_ROWSTATUS_NOTINSERVICE:
            if (pSnmpTgtParamEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            pSnmpTgtParamEntry->i2Status = UNDER_CREATION;
            break;
        case SNMP_ROWSTATUS_DESTROY:
            if (pSnmpTgtParamEntry != NULL)
            {
                if (SNMPDeleteTgtParamEntry (pSnmpTargetParamsName) ==
                    SNMP_FAILURE)
                {
                    return SNMP_FAILURE;
                }
            }
            break;
        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2SnmpTargetParamsMPModel
 Input       :  The Indices
                SnmpTargetParamsName

                The Object 
                testValSnmpTargetParamsMPModel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpTargetParamsMPModel (UINT4 *pu4ErrorCode,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pSnmpTargetParamsName,
                                  INT4 i4TestValSnmpTargetParamsMPModel)
{
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    pSnmpTgtParamEntry = SNMPGetTgtParamEntry (pSnmpTargetParamsName);
    if (SnmpIsModeSecure () == SNMP_SUCCESS)
    {
        if ((i4TestValSnmpTargetParamsMPModel == SNMP_MPMODEL_V1) ||
            (i4TestValSnmpTargetParamsMPModel == SNMP_MPMODEL_V2C))
        {
            *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
            return SNMP_FAILURE;
        }
    }
    if (pSnmpTgtParamEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (i4TestValSnmpTargetParamsMPModel < SNMP_MPMODEL_V1 ||
        i4TestValSnmpTargetParamsMPModel > SNMP_MPMODEL_V3)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpTargetParamsSecurityModel
 Input       :  The Indices
                SnmpTargetParamsName

                The Object 
                testValSnmpTargetParamsSecurityModel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpTargetParamsSecurityModel (UINT4 *pu4ErrorCode,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pSnmpTargetParamsName,
                                        INT4
                                        i4TestValSnmpTargetParamsSecurityModel)
{
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    pSnmpTgtParamEntry = SNMPGetTgtParamEntry (pSnmpTargetParamsName);
    if (SnmpIsModeSecure () == SNMP_SUCCESS)
    {
        if ((i4TestValSnmpTargetParamsSecurityModel == SNMP_SECMODEL_V1) ||
            (i4TestValSnmpTargetParamsSecurityModel == SNMP_SECMODEL_V2C))
        {
            *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
            return SNMP_FAILURE;
        }
    }
    if (pSnmpTgtParamEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (i4TestValSnmpTargetParamsSecurityModel < SNMP_SECMODEL_V1 ||
        i4TestValSnmpTargetParamsSecurityModel > SNMP_SECMODEL_V3)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpTargetParamsSecurityName
 Input       :  The Indices
                SnmpTargetParamsName

                The Object 
                testValSnmpTargetParamsSecurityName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpTargetParamsSecurityName (UINT4 *pu4ErrorCode,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pSnmpTargetParamsName,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pTestValSnmpTargetParamsSecurityName)
{
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    pSnmpTgtParamEntry = SNMPGetTgtParamEntry (pSnmpTargetParamsName);
    if (pSnmpTgtParamEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if ((pTestValSnmpTargetParamsSecurityName->i4_Length <= SNMP_ZERO) ||
        (pTestValSnmpTargetParamsSecurityName->i4_Length >
         SNMP_MAX_OCTETSTRING_SIZE))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpTargetParamsSecurityLevel
 Input       :  The Indices
                SnmpTargetParamsName

                The Object 
                testValSnmpTargetParamsSecurityLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpTargetParamsSecurityLevel (UINT4 *pu4ErrorCode,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pSnmpTargetParamsName,
                                        INT4
                                        i4TestValSnmpTargetParamsSecurityLevel)
{
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    pSnmpTgtParamEntry = SNMPGetTgtParamEntry (pSnmpTargetParamsName);
    if (pSnmpTgtParamEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (i4TestValSnmpTargetParamsSecurityLevel < SNMP_NOAUTH_NOPRIV ||
        i4TestValSnmpTargetParamsSecurityLevel > SNMP_AUTH_PRIV)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpTargetParamsStorageType
 Input       :  The Indices
                SnmpTargetParamsName

                The Object 
                testValSnmpTargetParamsStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpTargetParamsStorageType (UINT4 *pu4ErrorCode,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pSnmpTargetParamsName,
                                      INT4 i4TestValSnmpTargetParamsStorageType)
{
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    pSnmpTgtParamEntry = SNMPGetTgtParamEntry (pSnmpTargetParamsName);
    if (pSnmpTgtParamEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if ((i4TestValSnmpTargetParamsStorageType < SNMP3_STORAGE_TYPE_OTHER) ||
        (i4TestValSnmpTargetParamsStorageType > SNMP3_STORAGE_TYPE_READONLY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValSnmpTargetParamsStorageType != SNMP3_STORAGE_TYPE_VOLATILE) &&
        (i4TestValSnmpTargetParamsStorageType !=
         SNMP3_STORAGE_TYPE_NONVOLATILE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpTargetParamsRowStatus
 Input       :  The Indices
                SnmpTargetParamsName

                The Object 
                testValSnmpTargetParamsRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpTargetParamsRowStatus (UINT4 *pu4ErrorCode,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pSnmpTargetParamsName,
                                    INT4 i4TestValSnmpTargetParamsRowStatus)
{
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    pSnmpTgtParamEntry = SNMPGetTgtParamEntry (pSnmpTargetParamsName);
    switch (i4TestValSnmpTargetParamsRowStatus)
    {
        case SNMP_ROWSTATUS_CREATEANDWAIT:
            if (pSnmpTgtParamEntry != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            break;
        case SNMP_ROWSTATUS_ACTIVE:
            if (pSnmpTgtParamEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            if (pSnmpTgtParamEntry->i2Status != UNDER_CREATION ||
                pSnmpTgtParamEntry->ParamSecName.i4_Length == SNMP_ZERO ||
                pSnmpTgtParamEntry->i2ParamSecModel == SNMP_ZERO ||
                pSnmpTgtParamEntry->i4ParamSecLevel == SNMP_ZERO)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
        case SNMP_ROWSTATUS_NOTINSERVICE:
            if (pSnmpTgtParamEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            break;
        case SNMP_ROWSTATUS_DESTROY:
            if (pSnmpTgtParamEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            /* Associated filter must be deleted first */
            if (pSnmpTgtParamEntry->pFilterProfileTable != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2SnmpTargetParamsTable
 Input       :  The Indices
                SnmpTargetParamsName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SnmpTargetParamsTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetSnmpUnavailableContexts
 Input       :  The Indices

                The Object 
                retValSnmpUnavailableContexts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpUnavailableContexts (UINT4 *pu4RetValSnmpUnavailableContexts)
{
    *pu4RetValSnmpUnavailableContexts = gSnmpStat.u4SnmpUnavailableContexts;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpUnknownContexts
 Input       :  The Indices

                The Object 
                retValSnmpUnknownContexts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpUnknownContexts (UINT4 *pu4RetValSnmpUnknownContexts)
{
    *pu4RetValSnmpUnknownContexts = gSnmpStat.u4SnmpUnknownContexts;
    return SNMP_SUCCESS;
}
