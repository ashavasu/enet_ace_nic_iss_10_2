
#ifndef _H_i_SNMP_FRAMEWORK_MIB
#define _H_i_SNMP_FRAMEWORK_MIB
/* $Id: snmmpdnc.h,v 1.1 2015/12/29 11:53:33 siva Exp $
    ISS Wrapper header
    module SNMP-FRAMEWORK-MIB

 */

/* $Id: snmmpdnc.h,v 1.1 2015/12/29 11:53:33 siva Exp $
    ISS Wrapper header
    module SNMP-FRAMEWORK-MIB

 */

/********************************************************************
* FUNCTION NcSnmpEngineIDGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcSnmpEngineIDGet (
                UINT1 *psnmpEngineID );

/********************************************************************
* FUNCTION NcSnmpEngineBootsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcSnmpEngineBootsGet (
                INT4 *pi4snmpEngineBoots );

/********************************************************************
* FUNCTION NcSnmpEngineTimeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcSnmpEngineTimeGet (
                INT4 *pi4snmpEngineTime );

/********************************************************************
* FUNCTION NcSnmpEngineMaxMessageSizeGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcSnmpEngineMaxMessageSizeGet (
                INT4 *pi4snmpEngineMaxMessageSize );

/* END i_SNMP_FRAMEWORK_MIB.c */


#endif
