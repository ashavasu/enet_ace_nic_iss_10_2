/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: snmmpdwr.c,v 1.4 2015/04/28 12:35:02 siva Exp $
*
* Description: snmp stats wrapper Routines
*********************************************************************/

# include  "snmpcmn.h"
# include  "snmmpdlw.h"
# include  "snmmpdwr.h"
# include  "snmmpddb.h"

VOID
RegisterSNMMPD ()
{
    SNMPRegisterMib (&snmmpdOID, &snmmpdEntry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&snmmpdOID, (const UINT1 *) "snmmpd");
}

VOID
UnRegisterSNMMPD ()
{
    SNMPUnRegisterMib (&snmmpdOID, &snmmpdEntry);
    SNMPDelSysorEntry (&snmmpdOID, (const UINT1 *) "snmmpd");
}

INT4
SnmpUnknownSecurityModelsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpUnknownSecurityModels (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpInvalidMsgsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpInvalidMsgs (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpUnknownPDUHandlersGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpUnknownPDUHandlers (&(pMultiData->u4_ULongValue)));
}

INT4
SnmpEngineIDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpEngineID (pMultiData->pOctetStrValue));
}

INT4
SnmpEngineBootsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpEngineBoots (&(pMultiData->i4_SLongValue)));
}

INT4
SnmpEngineTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpEngineTime (&(pMultiData->i4_SLongValue)));
}

INT4
SnmpEngineMaxMessageSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpEngineMaxMessageSize (&(pMultiData->i4_SLongValue)));
}
