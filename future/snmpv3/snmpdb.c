/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: snmpdb.c,v 1.18 2018/01/02 09:36:15 siva Exp $
 *
 * Description: Routines for DB access module 
 *******************************************************************/
#include "snmpcmn.h"
#include "snmpdb.h"

#include "fssyslog.h"
#include "fswebnm.h"

INT1                gi1SnmpUserName[MAX_AUDIT_USER_NAME_LEN];
PRIVATE INT4        SNMPGetIndices (tSNMP_OID_TYPE OID, UINT4 u4Len,
                                    const tMbDbEntry * pDbEntry,
                                    tSnmpIndex ** pIndex);

/************************************************************************
 *  Function Name   : SNMPGetIndices 
 *  Description     : Get Index in a Index array from OID
 *  Input           : OID - Received Oid
 *                    u4Len - Length From index fetching starts
 *                    pDbEntry - DB entry where the actual Oid is available
 *  Output          : pIndex - Index array where index to be stored  
 *  Returns         : SUCCESS,NOINDEX or FAILURE 
 ************************************************************************/
PRIVATE INT4
SNMPGetIndices (tSNMP_OID_TYPE OID, UINT4 u4Len,
                const tMbDbEntry * pDbEntry, tSnmpIndex ** pIndex)
{
    UINT4               u4Count = 0, u4I = 0;
    tSNMP_MULTI_DATA_TYPE *pMulti = NULL;
    UINT4               u4IpAddress = 0;
    UINT4               u4CountTemp = 0;
    UINT4               u4WrongValue = SNMP_FAILURE;

    (*pIndex)->u4No = pDbEntry->u4NoIndices;
    /*----------------------------------------
     *  if the MbDb Entry passed is a Scalar
     *  and the length of OID is not greater than
     *  MBDB Entry OID lenth by One 
     *  the last instance is not Zero
     *  return SNMP_FAILURE
     *  The instance ID is not stored in MBDB
     *  Hence the length will differ by one
     *---------------------------------------------- */
    if (IS_SCALAR (pDbEntry))
    {
        if ((u4Len != OID.u4_Length - SNMP_ONE) ||
            (OID.pu4_OidList[u4Len] != SNMP_ZERO))
        {
            return SNMP_FAILURE;
        }
        else
        {
            return NO_INDEX;
        }
    }
    /*--------If table to take care of the first instance---*/
    if (IS_TABLE (pDbEntry) && (u4Len >= OID.u4_Length))
    {
        return NO_INDEX;
    }
    /*-------------Decoding the indices (to be refined )---*/
    for (u4Count = 0, u4CountTemp = 0; u4CountTemp < (*pIndex)->u4No;
         u4Count++, u4CountTemp++)
    {
        if (u4Len >= OID.u4_Length)
        {
            (*pIndex)->u4No = u4Count;
        }
        (*pIndex)->pIndex[u4CountTemp].i2_DataType =
            pDbEntry->pu1IndexType[u4Count];
        /* Select the Appropriate Index to update */
        pMulti = (tSNMP_MULTI_DATA_TYPE *) & ((*pIndex)->pIndex[u4CountTemp]);

        switch (pDbEntry->pu1IndexType[u4Count])
        {
            case SNMP_DATA_TYPE_UNSIGNED32:
            case SNMP_DATA_TYPE_COUNTER:
            case SNMP_DATA_TYPE_TIME_TICKS:
                pMulti->u4_ULongValue = OID.pu4_OidList[u4Len++];
                break;
            case SNMP_DATA_TYPE_INTEGER32:
                if (OID.pu4_OidList[u4Len] > SNMP_MAX_INT)
                {
                    OID.pu4_OidList[u4Len] = SNMP_MAX_INT;
                    u4WrongValue = SNMP_SUCCESS;
                }
                pMulti->i4_SLongValue = (INT4) (OID.pu4_OidList[u4Len++]);
                break;

            case SNMP_DATA_TYPE_OCTET_PRIM:
            case SNMP_DATA_TYPE_OPAQUE:
                pMulti->pOctetStrValue->i4_Length =
                    (INT4) (OID.pu4_OidList[u4Len++]);
                /* work around for Fixedlen octet str illegal 
                 * input case.
                 */
                if ((pMulti->pOctetStrValue->i4_Length < 0) ||
                    (pMulti->pOctetStrValue->i4_Length >
                     SNMP_MAX_OCTETSTRING_SIZE))
                    /*Invalid length str 
                     * The maximum allowable length of octet string is
                     * SNMP_MAX_OCTETSTRING_SIZE */
                {
                    return NO_INDEX_LEN_MATCH;
                }

                if ((UINT4) (pMulti->pOctetStrValue->i4_Length) >
                    (OID.u4_Length - u4Len))
                {
                    return NO_INDEX_LEN_MATCH;
                }
                for (u4I = 0; u4I < (UINT4) pMulti->pOctetStrValue->i4_Length;
                     u4I++)
                {
                    if (u4Len > OID.u4_Length)
                    {
                        /* Length cannot be more than OID's length */
                        break;
                    }
                    if (OID.pu4_OidList[u4Len] > MAX_STR_OCTET_VAL)
                    {
                        OID.pu4_OidList[u4Len] = MAX_STR_OCTET_VAL;
                        u4WrongValue = SNMP_SUCCESS;
                    }
                    pMulti->pOctetStrValue->pu1_OctetList[u4I]
                        = (UINT1) OID.pu4_OidList[u4Len++];
                }
                break;
            case SNMP_DATA_TYPE_IMP_OCTET_PRIM:

                if (OID.u4_Length < u4Len)    /*Invalid length str */
                    return NO_INDEX_LEN_MATCH;

                pMulti->pOctetStrValue->i4_Length =
                    (INT4) (OID.u4_Length - u4Len);
                if (pMulti->pOctetStrValue->i4_Length >
                    SNMP_MAX_OCTETSTRING_SIZE)
                {
                    return SNMP_FAILURE;
                }
                for (u4I = SNMP_ZERO;
                     u4I < (UINT4) pMulti->pOctetStrValue->i4_Length; u4I++)
                {
                    pMulti->pOctetStrValue->pu1_OctetList[u4I]
                        = (UINT1) OID.pu4_OidList[u4Len++];
                }
                break;
            case SNMP_DATA_TYPE_IP_ADDR_PRIM:
                for (u4I = u4Len; u4I < u4Len + 4; u4I++)
                {
                    if (u4Len > OID.u4_Length)
                    {
                        /* Length cannot be more than OID's length */
                        break;
                    }
                    if (OID.pu4_OidList[u4I] >= MAX_STR_OCTET_VAL)
                    {
                        OID.pu4_OidList[u4I] = MAX_STR_OCTET_VAL;
                        u4WrongValue = SNMP_SUCCESS;
                    }
                }
                u4IpAddress = OID.pu4_OidList[u4Len++];
                u4IpAddress =
                    (u4IpAddress << SNMP_EIGHT) | OID.pu4_OidList[u4Len++];
                u4IpAddress =
                    (u4IpAddress << SNMP_EIGHT) | OID.pu4_OidList[u4Len++];
                u4IpAddress =
                    (u4IpAddress << SNMP_EIGHT) | OID.pu4_OidList[u4Len++];
                pMulti->u4_ULongValue = u4IpAddress;
                break;
            case SNMP_DATA_TYPE_OBJECT_ID:
                pMulti->pOidValue->u4_Length = OID.pu4_OidList[u4Len++];
                if (pMulti->pOidValue->u4_Length > MAX_OID_LENGTH)
                {
                    return NO_INDEX_LEN_MATCH;
                }
                for (u4I = SNMP_ZERO;
                     u4I < (UINT4) pMulti->pOidValue->u4_Length; u4I++)
                {
                    pMulti->pOidValue->pu4_OidList[u4I]
                        = OID.pu4_OidList[u4Len++];
                }
                break;
            case SNMP_DATA_TYPE_IMP_OBJECT_ID:
                pMulti->pOidValue->u4_Length = OID.u4_Length - u4Len;
                if (pMulti->pOidValue->u4_Length > MAX_OID_LENGTH)
                {
                    return SNMP_FAILURE;
                }

                for (u4I = SNMP_ZERO;
                     u4I < (UINT4) pMulti->pOidValue->u4_Length; u4I++)
                {
                    pMulti->pOidValue->pu4_OidList[u4I]
                        = OID.pu4_OidList[u4Len++];
                }
                break;
            case SNMP_FIXED_LENGTH_OCTET_STRING:
                /* To Handle Fixed OctetString
                 *  Get the length from protocol db.h (generated 
                 *  using midgen tool)
                 *  Copy only the specified no of octets
                 *  (length as in db.h file) 
                 *  Read Next Index    */
                pMulti->pOctetStrValue->i4_Length =
                    pDbEntry->pu1IndexType[u4Count + 1];
                for (u4I = 0; u4I < (UINT4) pMulti->pOctetStrValue->i4_Length;
                     u4I++)
                {
                    if (u4Len > OID.u4_Length)
                    {
                        /* Length cannot be more than OID's length */
                        break;
                    }
                    if (OID.pu4_OidList[u4Len] > MAX_STR_OCTET_VAL)
                    {
                        OID.pu4_OidList[u4Len] = MAX_STR_OCTET_VAL;
                        u4WrongValue = SNMP_SUCCESS;
                    }
                    pMulti->pOctetStrValue->pu1_OctetList[u4I]
                        = (UINT1) OID.pu4_OidList[u4Len++];
                }
                u4Count++;
                break;
            default:
                return SNMP_FAILURE;
                break;
        }
    }
    /* Wrong Values are given so we have updated with the Max Values for these
     * and returning NO_INDEX_LEN_MATCH.
     */
    if (u4WrongValue == SNMP_SUCCESS)
    {
        return NO_INDEX_LEN_MATCH;
    }
    /*---------------------------------------------------
      In case of Table after decoding 
      if there are still OID left out length
      or if thr processed length is more
      than OID length then the OID is invalid
      ----------------------------------------------------*/
    if (u4Len != OID.u4_Length)
    {
        return SNMP_ERR_NO_CREATION;
    }
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : SNMPGet  
 *  Description     : Get Routine for getting snmp value for a OID
 *  Input           : OID - OID for which get should be called
 *                    pIndex - Index Array pointer. Allocated by application
 *                    and snmp will use the memory. This just to avoid memory
 *                    allocation in DB module.
 *  Output          : pData - Multidata in which data will updated
 *                    pError - Error Number incase failure
 *  Returns         : SUCCESS or FAILURE 
 ************************************************************************/
INT4
SNMPGet (tSNMP_OID_TYPE OID, tSNMP_MULTI_DATA_TYPE * pData, UINT4 *pError,
         tSnmpIndex * pIndex, UINT4 u4VcNum)
{
    UINT4               u4Len = 0;
    INT4                i4Result = SNMP_FAILURE;
    tMbDbEntry         *pCur = NULL, *pNext = NULL;
    tSNMP_OID_TYPE      TempOid;
    tSnmpDbEntry        SnmpDbEntry;
    tIssCustSnmpConfInfo IssCustSnmpConfInfo;

    /*Perform required custom validations if any  */
    MEMSET (&IssCustSnmpConfInfo, 0, sizeof (tIssCustSnmpConfInfo));

    TempOid.pu4_OidList = (UINT4 *) MemAllocMemBlk (gSnmpOidListPoolId);
    if (TempOid.pu4_OidList == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (TempOid.pu4_OidList, 0, MAX_OID_LENGTH * sizeof (UINT4));

    TempOid.u4_Length = OID.u4_Length;
    MEMCPY (TempOid.pu4_OidList, OID.pu4_OidList,
            OID.u4_Length * sizeof (UINT4));

    GetMbDbEntry (OID, &pNext, &u4Len, &SnmpDbEntry);
    pCur = SnmpDbEntry.pMibEntry;
    if (pCur != NULL)
    {
        if ((pCur->Get == NULL) || (pCur->u4OIDAccess == SNMP_NOACCESS)
            || (pCur->u4OIDAccess == SNMP_ACCESSIBLEFORNOTIFY))
        {
            SNMP_INR_OUT_NO_SUCH_NAME;
            *pError = SNMP_EXCEPTION_NO_SUCH_INSTANCE;
            MemReleaseMemBlock (gSnmpOidListPoolId,
                                (UINT1 *) TempOid.pu4_OidList);
            return SNMP_FAILURE;
        }
        i4Result = SNMPGetIndices (OID, u4Len, pCur, &pIndex);
        if ((i4Result == SNMP_SUCCESS) || (i4Result == NO_INDEX))
        {
            if ((i4Result == NO_INDEX) && (IS_TABLE (pCur)))
            {
                SNMP_INR_OUT_NO_SUCH_NAME;
                *pError = SNMP_EXCEPTION_NO_SUCH_INSTANCE;
                MemReleaseMemBlock (gSnmpOidListPoolId,
                                    (UINT1 *) TempOid.pu4_OidList);
                return SNMP_FAILURE;
            }
            if (pCur->u4OIDType == SNMP_DATA_TYPE_MAC_ADDRESS)
                pCur->u4OIDType = SNMP_DATA_TYPE_OCTET_PRIM;

            pData->i2_DataType = (INT2) pCur->u4OIDType;
            *pError = SNMP_ERR_NO_ERROR;

            IssCustSnmpConfInfo.pSnmpIndex = (VOID *) pIndex;
            IssCustSnmpConfInfo.pSnmpOId = (VOID *) &OID;
#ifdef ISS_WANTED
            if (IssCustConfControlSnmpGet (&IssCustSnmpConfInfo) == ISS_FAILURE)
            {
                *pError = SNMP_ERR_AUTHORIZATION_ERROR;
                return SNMP_FAILURE;
            }
#endif

            i4Result =
                SNMPAccessGet (&SnmpDbEntry, pData, pIndex, pError, u4VcNum);
            if (i4Result == SNMP_FAILURE)
            {
                if ((*pError) == SNMP_ERR_NO_ERROR)
                {
                    SNMP_INR_OUT_NO_SUCH_NAME;
                    *pError = SNMP_EXCEPTION_NO_SUCH_INSTANCE;
                }
            }
            else
            {
                *pError = SNMP_ERR_NO_ERROR;
            }
        }
        else
        {
            /* Return Index should have same value as given by the user */
            if ((i4Result == NO_INDEX_LEN_MATCH) ||
                (i4Result == SNMP_ERR_NO_CREATION))
            {
                MEMSET (OID.pu4_OidList, 0, OID.u4_Length * sizeof (UINT4));
                OID.u4_Length = TempOid.u4_Length;
                MEMCPY (OID.pu4_OidList, TempOid.pu4_OidList,
                        TempOid.u4_Length * sizeof (UINT4));
            }
            SNMP_INR_OUT_NO_SUCH_NAME;
            *pError = SNMP_EXCEPTION_NO_SUCH_INSTANCE;
            MemReleaseMemBlock (gSnmpOidListPoolId,
                                (UINT1 *) TempOid.pu4_OidList);
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (MsrGetSaveStatus () != ISS_TRUE)
        {
            SNMP_INR_OUT_NO_SUCH_NAME;
        }
        *pError = SNMP_EXCEPTION_NO_SUCH_OBJECT;
    }
    MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *) TempOid.pu4_OidList);
    return i4Result;
}

/************************************************************************
 *  Function Name   : SNMPGetNextOID 
 *  Description     : Get the next OID and Value for the given OID
 *  Input           : OID - Given Oid
 *                    pCurIndex, pNextIndex - Index Array pointer. Index
 *                    memory is allocated at application level. This is
 *                    to avoid memory allocation at DB access level.
 *  Output          : pNextOid - Next OID 
 *                    pNextData - Value of next oid
 *                    pError - Error Number incase failure
 *  Returns         : SUCCESS or FAILURE 
 ************************************************************************/
INT4
SNMPGetNextOID (tSNMP_OID_TYPE OID, tSNMP_OID_TYPE * pNextOid,
                tSNMP_MULTI_DATA_TYPE * pNextData,
                UINT4 *pError, tSnmpIndex * pCurIndex, tSnmpIndex * pNextIndex,
                UINT4 u4VcNum)
{
#define CONTINUE 0x01
#define BREAK    0x00
    tSnmpDbEntry        SnmpDbEntry;
    tMbDbEntry         *pCur = NULL, *pNext = NULL;
    tSNMP_OID_TYPE     *pOID = NULL;
    UINT4               u4Len = 0;
    INT4                i4Result = SNMP_FAILURE;
    UINT4               u4MatchLen = 0;
    UINT4               u4SkipGetNext = SNMP_FALSE;
    pOID = &OID;

    GetMbDbEntry (*pOID, &pNext, &u4Len, &SnmpDbEntry);
    SNMPUpdateGetNextEOID (pOID, &pNext, &u4Len, &SnmpDbEntry);
    pCur = SnmpDbEntry.pMibEntry;
    /*------------------------------------------
      The current entry 
      If Scalar or NULL (No match)
      if the next entry then process the next entry
      If table 
      get the indices values from OID
      get the next index value
      get the instance
      if there is no instance 
      get the next entry
      if the next entry is table 
      process the table steps 
      till an instance is got 
      or end of Mib is reached 
      or A scalar is reached
      and Process Scaler
      ---------------------------------------------*/
    if ((IS_SCALAR (pCur) && (!(pCur->ObjectID.u4_Length == pOID->u4_Length)))
        || pCur == NULL)
    {

        if (pCur != NULL)
        {
            while (u4MatchLen < pCur->ObjectID.u4_Length)
            {
                if (pCur->ObjectID.pu4_OidList[u4MatchLen]
                    == pOID->pu4_OidList[u4MatchLen])
                {
                    u4MatchLen++;
                }
                else
                    break;
            }
            /* To ignore the Padded OIDs */
            if (u4MatchLen == pCur->ObjectID.u4_Length)
            {
                pOID->u4_Length = pCur->ObjectID.u4_Length;
            }
            GetMbDbEntry (*pOID, &pNext, &u4Len, &SnmpDbEntry);
            SNMPUpdateGetNextEOID (pOID, &pNext, &u4Len, &SnmpDbEntry);
            pCur = SnmpDbEntry.pMibEntry;
            UNUSED_PARAM (pCur);
        }

        while ((pNext != NULL)
               && ((pNext->u4OIDAccess == SNMP_NOACCESS)
                   || (pNext->u4OIDAccess == SNMP_ACCESSIBLEFORNOTIFY)))
        {
            pOID = &(pNext->ObjectID);
            GetMbDbEntry (*pOID, &pNext, &u4Len, &SnmpDbEntry);
            SNMPUpdateGetNextEOID (pOID, &pNext, &u4Len, &SnmpDbEntry);
            if ((pNext != NULL)
                && ((pNext->u4OIDAccess != SNMP_NOACCESS)
                    && (pNext->u4OIDAccess != SNMP_ACCESSIBLEFORNOTIFY)))
            {
                break;
            }
        }
        pCur = pNext;
        SnmpDbEntry.pMibEntry = pCur;
        SnmpDbEntry.pLockPointer = SnmpDbEntry.pNextLockPointer;
        SnmpDbEntry.pUnlockPointer = SnmpDbEntry.pNextUnlockPointer;
        SnmpDbEntry.pSetContextPointer = SnmpDbEntry.pNextSetContextPointer;
        SnmpDbEntry.pReleaseContextPointer =
            SnmpDbEntry.pNextReleaseContextPointer;
        if (pNext != NULL)
        {
            u4Len = pNext->ObjectID.u4_Length;
        }
    }

    while (SNMP_ONE)
    {
        if (IS_TABLE (pCur))
        {                        /*kloc */
            while (pCur != NULL
                   && ((pCur->u4OIDAccess == SNMP_NOACCESS)
                       || (pCur->u4OIDAccess == SNMP_ACCESSIBLEFORNOTIFY)))
            {
                pOID = &(pCur->ObjectID);
                GetMbDbEntry (*pOID, &pNext, &u4Len, &SnmpDbEntry);
                SNMPUpdateGetNextEOID (pOID, &pNext, &u4Len, &SnmpDbEntry);
                if ((pNext != NULL)
                    && ((pNext->u4OIDAccess != SNMP_NOACCESS)
                        && (pNext->u4OIDAccess != SNMP_ACCESSIBLEFORNOTIFY)))
                {
                    pCur = pNext;
                    SnmpDbEntry.pMibEntry = pCur;
                    SnmpDbEntry.pLockPointer = SnmpDbEntry.pNextLockPointer;
                    SnmpDbEntry.pUnlockPointer = SnmpDbEntry.pNextUnlockPointer;
                    SnmpDbEntry.pSetContextPointer =
                        SnmpDbEntry.pNextSetContextPointer;
                    SnmpDbEntry.pReleaseContextPointer =
                        SnmpDbEntry.pNextReleaseContextPointer;
                    break;
                }
                else
                {
                    pCur = pNext;
                }
            }
            if (IS_SCALAR (pCur))
            {
                SnmpDbEntry.pMibEntry = pCur;
                SnmpDbEntry.pLockPointer = SnmpDbEntry.pNextLockPointer;
                SnmpDbEntry.pUnlockPointer = SnmpDbEntry.pNextUnlockPointer;
                SnmpDbEntry.pSetContextPointer =
                    SnmpDbEntry.pNextSetContextPointer;
                SnmpDbEntry.pReleaseContextPointer =
                    SnmpDbEntry.pNextReleaseContextPointer;
                continue;
            }
            if (pCur != NULL)    /*kloc */
            {
                pNextIndex->u4No = pCur->u4NoIndices;
                i4Result = SNMPGetIndices (*pOID, u4Len, pCur, &pCurIndex);
            }
            if (i4Result == SNMP_FAILURE)
            {
                *pError = SNMP_ERR_GEN_ERR;
                return SNMP_FAILURE;
            }
            if ((NULL == pCur) || (pCur->GetNextIndex == NULL))
            {
                SNMPTrace ("\n FATAL DB Error :");
                *pError = SNMP_ERR_GEN_ERR;
                return SNMP_FAILURE;
            }
#if 0
            if (pCur->GetNextIndex == NULL)
            {                    /*kloc */
                SNMPTrace ("\n FATAL DB Error :");
                *pError = SNMP_ERR_GEN_ERR;
                return SNMP_FAILURE;
            }
#endif
            if (i4Result == NO_INDEX)
            {
                i4Result = SNMPAccessGetNext (&SnmpDbEntry, NULL, pNextIndex,
                                              u4VcNum);
                if (i4Result == SNMP_FAILURE)
                {
                    /* If first instance of a table is not present , then
                     * have to skip all the columnar objects in that table,  
                     * and must do a get next for the object outside 
                     * the table. So set the flag u4SkipGetNext. */
                    u4SkipGetNext = SNMP_TRUE;
                }
            }
            else if (i4Result == SNMP_ERR_NO_CREATION)
            {
                /* check for partial indexing */
                if (pCurIndex->u4No != pCur->u4NoIndices)
                {
                    i4Result = SNMPAccessGet (&SnmpDbEntry, pNextData,
                                              pCurIndex, pError, u4VcNum);

                }
                if (i4Result == SNMP_SUCCESS)
                {
                    *pError = SNMP_ERR_NO_ERROR;
                    if (pCur->u4OIDType == SNMP_DATA_TYPE_MAC_ADDRESS)
                        pCur->u4OIDType = SNMP_DATA_TYPE_OCTET_PRIM;
                    pNextData->i2_DataType = (INT2) pCur->u4OIDType;
                    SNMPFormOid (pNextOid, pCur, pCurIndex);
                    *pError = SNMP_ERR_NO_ERROR;
                    return i4Result;
                }
                else
                {
                    i4Result =
                        SNMPAccessGetNext (&SnmpDbEntry, pCurIndex, pNextIndex,
                                           u4VcNum);
                }
            }
            else
            {
                i4Result =
                    SNMPAccessGetNext (&SnmpDbEntry, pCurIndex, pNextIndex,
                                       u4VcNum);
            }
            if ((i4Result == SNMP_SUCCESS) && pCur != NULL
                && (pCur->Get != NULL))
            {
                *pError = SNMP_ERR_NO_ERROR;

                i4Result = SNMPAccessGet (&SnmpDbEntry, pNextData,
                                          pNextIndex, pError, u4VcNum);

                if (pCur->u4OIDType == SNMP_DATA_TYPE_MAC_ADDRESS)
                    pCur->u4OIDType = SNMP_DATA_TYPE_OCTET_PRIM;
                pNextData->i2_DataType = (INT2) pCur->u4OIDType;
                SNMPFormOid (pNextOid, pCur, pNextIndex);
                if (i4Result == SNMP_SUCCESS)
                {
                    *pError = SNMP_ERR_NO_ERROR;
                    return i4Result;
                }
                else if ((*pError) != SNMP_UNKNOWN_CONTEXT_NAME)
                {
                    *pError = SNMP_EXCEPTION_END_OF_MIB_VIEW;
                    return i4Result;
                }
            }
            if (pNext == NULL)
            {
                pCur = NULL;
                UNUSED_PARAM (pCur);
                *pError = SNMP_EXCEPTION_END_OF_MIB_VIEW;
                return SNMP_FAILURE;
            }
            do
            {
                if ((pNext != NULL) &&
                    (pCur->GetNextIndex != pNext->GetNextIndex))
                {
                    pCur = pNext;
                    pOID = &(pCur->ObjectID);
                    GetMbDbEntry (*pOID, &pNext, &u4Len, &SnmpDbEntry);
                    SNMPUpdateGetNextEOID (pOID, &pNext, &u4Len, &SnmpDbEntry);
                    break;
                }

                pCur = pNext;
                pOID = &(pCur->ObjectID);
                GetMbDbEntry (*pOID, &pNext, &u4Len, &SnmpDbEntry);
                SNMPUpdateGetNextEOID (pOID, &pNext, &u4Len, &SnmpDbEntry);
                /* Skip all the columnar objects in the table if the
                 * table is empty. */
                if (u4SkipGetNext == SNMP_FALSE)
                {
                    break;
                }

            }
            while ((pNext != NULL)
                   && (pCur->GetNextIndex == pNext->GetNextIndex));

            u4SkipGetNext = SNMP_FALSE;
            pCur = SnmpDbEntry.pMibEntry;
            if (pCur == NULL && pNext == NULL)
            {
                *pError = SNMP_EXCEPTION_END_OF_MIB_VIEW;
                return SNMP_FAILURE;
            }
            if (pCur != NULL)
            {
                pOID = &(pCur->ObjectID);
            }
            else if (pCur == NULL && pNext != NULL)
            {
                pOID = &(pNext->ObjectID);
                GetMbDbEntry (*pOID, &pNext, &u4Len, &SnmpDbEntry);
                SNMPUpdateGetNextEOID (pOID, &pNext, &u4Len, &SnmpDbEntry);
                pCur = SnmpDbEntry.pMibEntry;
            }
        }

        if (IS_SCALAR (pCur))
        {
            while (pCur != NULL
                   && ((pCur->u4OIDAccess == SNMP_NOACCESS)
                       || (pCur->u4OIDAccess == SNMP_ACCESSIBLEFORNOTIFY)))
            {
                pOID = &(pCur->ObjectID);
                GetMbDbEntry (*pOID, &pNext, &u4Len, &SnmpDbEntry);
                SNMPUpdateGetNextEOID (pOID, &pNext, &u4Len, &SnmpDbEntry);
                if ((pNext != NULL)
                    && ((pNext->u4OIDAccess != SNMP_NOACCESS)
                        && (pNext->u4OIDAccess != SNMP_ACCESSIBLEFORNOTIFY)))
                {
                    pCur = pNext;
                    SnmpDbEntry.pMibEntry = pCur;
                    SnmpDbEntry.pLockPointer = SnmpDbEntry.pNextLockPointer;
                    SnmpDbEntry.pUnlockPointer = SnmpDbEntry.pNextUnlockPointer;
                    SnmpDbEntry.pSetContextPointer =
                        SnmpDbEntry.pNextSetContextPointer;
                    SnmpDbEntry.pReleaseContextPointer =
                        SnmpDbEntry.pNextReleaseContextPointer;
                    break;
                }
                else
                {
                    pCur = pNext;
                }
            }
            if (IS_TABLE (pCur))
            {
                SnmpDbEntry.pMibEntry = pCur;
                SnmpDbEntry.pLockPointer = SnmpDbEntry.pNextLockPointer;
                SnmpDbEntry.pUnlockPointer = SnmpDbEntry.pNextUnlockPointer;
                SnmpDbEntry.pSetContextPointer =
                    SnmpDbEntry.pNextSetContextPointer;
                SnmpDbEntry.pReleaseContextPointer =
                    SnmpDbEntry.pNextReleaseContextPointer;
                continue;
            }

            if (NULL == pCur)
            {
                SNMPTrace ("\n FATAL DB Error :");
                *pError = SNMP_ERR_GEN_ERR;
                return SNMP_FAILURE;
            }
            SNMPFormOid (pNextOid, pCur, NULL);
            if (pCur->u4OIDType == SNMP_DATA_TYPE_MAC_ADDRESS)
                pCur->u4OIDType = SNMP_DATA_TYPE_OCTET_PRIM;

            pNextData->i2_DataType = (INT2) pCur->u4OIDType;
            if (pCur->Get == NULL)
            {
                SNMPTrace ("\n FATAL DB Error :");
                *pError = SNMP_ERR_GEN_ERR;
                return SNMP_FAILURE;
            }
            *pError = SNMP_ERR_NO_ERROR;
            i4Result =
                SNMPAccessGet (&SnmpDbEntry, pNextData, NULL, pError, u4VcNum);
            if (i4Result == SNMP_SUCCESS)
            {
                *pError = SNMP_ERR_NO_ERROR;
                return SNMP_SUCCESS;
            }
            else if ((*pError) != SNMP_UNKNOWN_CONTEXT_NAME)
            {
                *pError = SNMP_ERR_GEN_ERR;
                return SNMP_FAILURE;
            }

            if (pNext == NULL)
            {
                pCur = NULL;
                UNUSED_PARAM (pCur);
                *pError = SNMP_EXCEPTION_END_OF_MIB_VIEW;
                return SNMP_FAILURE;
            }
            pCur = pNext;
            pOID = &(pCur->ObjectID);
            GetMbDbEntry (*pOID, &pNext, &u4Len, &SnmpDbEntry);
            SNMPUpdateGetNextEOID (pOID, &pNext, &u4Len, &SnmpDbEntry);
            pCur = SnmpDbEntry.pMibEntry;
            if (pCur == NULL && pNext == NULL)
            {
                *pError = SNMP_EXCEPTION_END_OF_MIB_VIEW;
                return SNMP_FAILURE;
            }
            if (pCur != NULL)
            {
                pOID = &(pCur->ObjectID);
            }
            else if (pCur == NULL && pNext != NULL)
            {
                pOID = &(pNext->ObjectID);
                GetMbDbEntry (*pOID, &pNext, &u4Len, &SnmpDbEntry);
                SNMPUpdateGetNextEOID (pOID, &pNext, &u4Len, &SnmpDbEntry);
                pCur = SnmpDbEntry.pMibEntry;
            }
        }
        if (pCur == NULL)
        {
            break;
        }
    }
    *pError = SNMP_EXCEPTION_END_OF_MIB_VIEW;
    return i4Result;
}

/************************************************************************
 *  Function Name   : SNMPGetNextReadWriteOID 
 *  Description     : Get the next OID and Value for the given OID
 *  Input           : OID - Given Oid
 *                    pCurIndex, pNextIndex - Index Array pointer. Index
 *                    memory is allocated at application level. This is
 *                    to avoid memory allocation at DB access level.
 *  Output          : pNextOid - Next OID 
 *                    pNextData - Value of next oid
 *                    pError - Error Number incase failure
 *  Returns         : SUCCESS or FAILURE 
 ************************************************************************/
INT4
SNMPGetNextReadWriteOID (tSNMP_OID_TYPE OID, tSNMP_OID_TYPE * pNextOid,
                         tSNMP_MULTI_DATA_TYPE * pNextData,
                         UINT4 *pError, tSnmpIndex * pCurIndex,
                         tSnmpIndex * pNextIndex, UINT4 u4VcNum)
{
#define CONTINUE 0x01
#define BREAK    0x00
    tSnmpDbEntry        SnmpDbEntry;
    tMbDbEntry         *pCur = NULL, *pNext = NULL;
    tSNMP_OID_TYPE     *pOID = NULL;
    UINT4               u4Len = 0;
    INT4                i4Result = SNMP_FAILURE;
    UINT4               u4MatchLen = 0;
    UINT4               u4SkipGetNext = SNMP_FALSE;
    pOID = &OID;

    GetMbDbEntry (*pOID, &pNext, &u4Len, &SnmpDbEntry);
    SNMPUpdateGetNextEOID (pOID, &pNext, &u4Len, &SnmpDbEntry);
    pCur = SnmpDbEntry.pMibEntry;
    /*------------------------------------------
      The current entry 
      If Scalar or NULL (No match)
      if the next entry then process the next entry
      If table 
      get the indices values from OID
      get the next index value
      get the instance
      if there is no instance 
      get the next entry
      if the next entry is table 
      process the table steps 
      till an instance is got 
      or end of Mib is reached 
      or A scalar is reached
      and Process Scaler
      ---------------------------------------------*/
    if ((IS_SCALAR (pCur) && (!(pCur->ObjectID.u4_Length == pOID->u4_Length)))
        || pCur == NULL)
    {

        if (pCur != NULL)
        {
            while (u4MatchLen < pCur->ObjectID.u4_Length)
            {
                if (pCur->ObjectID.pu4_OidList[u4MatchLen]
                    == pOID->pu4_OidList[u4MatchLen])
                {
                    u4MatchLen++;
                }
                else
                    break;
            }
            /* To ignore the Padded OIDs */
            if (u4MatchLen == pCur->ObjectID.u4_Length)
            {
                pOID->u4_Length = pCur->ObjectID.u4_Length;
            }
            GetMbDbEntry (*pOID, &pNext, &u4Len, &SnmpDbEntry);
            SNMPUpdateGetNextEOID (pOID, &pNext, &u4Len, &SnmpDbEntry);
            pCur = SnmpDbEntry.pMibEntry;
            UNUSED_PARAM (pCur);
        }

        while ((pNext != NULL)
               && ((pNext->u4OIDAccess == SNMP_NOACCESS)
                   || (pNext->u4OIDAccess == SNMP_ACCESSIBLEFORNOTIFY)
                   || (pNext->u4OIDAccess == SNMP_READONLY)))
        {
            pOID = &(pNext->ObjectID);
            GetMbDbEntry (*pOID, &pNext, &u4Len, &SnmpDbEntry);
            SNMPUpdateGetNextEOID (pOID, &pNext, &u4Len, &SnmpDbEntry);
            if ((pNext != NULL)
                && ((pNext->u4OIDAccess != SNMP_NOACCESS)
                    && (pNext->u4OIDAccess != SNMP_ACCESSIBLEFORNOTIFY)
                    && (pNext->u4OIDAccess != SNMP_READONLY)))
            {
                break;
            }
        }
        pCur = pNext;
        SnmpDbEntry.pMibEntry = pCur;
        SnmpDbEntry.pLockPointer = SnmpDbEntry.pNextLockPointer;
        SnmpDbEntry.pUnlockPointer = SnmpDbEntry.pNextUnlockPointer;
        SnmpDbEntry.pSetContextPointer = SnmpDbEntry.pNextSetContextPointer;
        SnmpDbEntry.pReleaseContextPointer =
            SnmpDbEntry.pNextReleaseContextPointer;
        if (pNext != NULL)
        {
            u4Len = pNext->ObjectID.u4_Length;
        }
    }

    while (SNMP_ONE)
    {
        if (IS_TABLE (pCur))
        {                        /*kloc */
            while (pCur != NULL
                   && ((pCur->u4OIDAccess == SNMP_NOACCESS)
                       || (pCur->u4OIDAccess == SNMP_ACCESSIBLEFORNOTIFY)
                       || (pCur->u4OIDAccess == SNMP_READONLY)))
            {
                pOID = &(pCur->ObjectID);
                GetMbDbEntry (*pOID, &pNext, &u4Len, &SnmpDbEntry);
                SNMPUpdateGetNextEOID (pOID, &pNext, &u4Len, &SnmpDbEntry);
                if ((pNext != NULL)
                    && ((pNext->u4OIDAccess != SNMP_NOACCESS)
                        && (pNext->u4OIDAccess != SNMP_ACCESSIBLEFORNOTIFY)
                        && (pNext->u4OIDAccess != SNMP_READONLY)))
                {
                    pCur = pNext;
                    SnmpDbEntry.pMibEntry = pCur;
                    SnmpDbEntry.pLockPointer = SnmpDbEntry.pNextLockPointer;
                    SnmpDbEntry.pUnlockPointer = SnmpDbEntry.pNextUnlockPointer;
                    SnmpDbEntry.pSetContextPointer =
                        SnmpDbEntry.pNextSetContextPointer;
                    SnmpDbEntry.pReleaseContextPointer =
                        SnmpDbEntry.pNextReleaseContextPointer;
                    break;
                }
                else
                {
                    pCur = pNext;
                }
            }
            if (IS_SCALAR (pCur))
            {
                SnmpDbEntry.pMibEntry = pCur;
                SnmpDbEntry.pLockPointer = SnmpDbEntry.pNextLockPointer;
                SnmpDbEntry.pUnlockPointer = SnmpDbEntry.pNextUnlockPointer;
                SnmpDbEntry.pSetContextPointer =
                    SnmpDbEntry.pNextSetContextPointer;
                SnmpDbEntry.pReleaseContextPointer =
                    SnmpDbEntry.pNextReleaseContextPointer;
                continue;
            }
            if (pCur != NULL)    /*kloc */
            {
                pNextIndex->u4No = pCur->u4NoIndices;
                i4Result = SNMPGetIndices (*pOID, u4Len, pCur, &pCurIndex);
            }
            if (i4Result == SNMP_FAILURE)
            {
                *pError = SNMP_ERR_GEN_ERR;
                return SNMP_FAILURE;
            }
            if ((NULL == pCur) || (pCur->GetNextIndex == NULL))
            {
                SNMPTrace ("\n FATAL DB Error :");
                *pError = SNMP_ERR_GEN_ERR;
                return SNMP_FAILURE;
            }
            if (i4Result == NO_INDEX)
            {
                i4Result = SNMPAccessGetNext (&SnmpDbEntry, NULL, pNextIndex,
                                              u4VcNum);
                if (i4Result == SNMP_FAILURE)
                {
                    /* If first instance of a table is not present , then
                     * have to skip all the columnar objects in that table,  
                     * and must do a get next for the object outside 
                     * the table. So set the flag u4SkipGetNext. */
                    u4SkipGetNext = SNMP_TRUE;
                }
            }
            else if (i4Result == SNMP_ERR_NO_CREATION)
            {
                /* check for partial indexing */
                if (pCurIndex->u4No != pCur->u4NoIndices)
                {
                    /*  To avoid Snmp Access Get for read-only oid in write startup */
                    if (pCur->u4OIDAccess == SNMP_READONLY)
                    {
                        /* To continue the flow for read-only oid in write startup */
                        i4Result = SNMP_SUCCESS;
                    }
                    else
                    {

                        i4Result = SNMPAccessGet (&SnmpDbEntry, pNextData,
                                                  pCurIndex, pError, u4VcNum);
                    }

                }
                if (i4Result == SNMP_SUCCESS)
                {
                    *pError = SNMP_ERR_NO_ERROR;
                    if (pCur->u4OIDType == SNMP_DATA_TYPE_MAC_ADDRESS)
                        pCur->u4OIDType = SNMP_DATA_TYPE_OCTET_PRIM;
                    pNextData->i2_DataType = (INT2) pCur->u4OIDType;
                    SNMPFormOid (pNextOid, pCur, pCurIndex);
                    *pError = SNMP_ERR_NO_ERROR;
                    return i4Result;
                }
                else
                {
                    i4Result =
                        SNMPAccessGetNext (&SnmpDbEntry, pCurIndex, pNextIndex,
                                           u4VcNum);
                }
            }
            else
            {
                i4Result =
                    SNMPAccessGetNext (&SnmpDbEntry, pCurIndex, pNextIndex,
                                       u4VcNum);
            }
            if ((i4Result == SNMP_SUCCESS) && pCur != NULL
                && (pCur->Get != NULL))
            {
                *pError = SNMP_ERR_NO_ERROR;
                /*  To avoid Snmp Access Get for read-only oid in write startup */
                if (pCur->u4OIDAccess == SNMP_READONLY)
                {
                    /* To continue the flow for read-only oid in write startup */
                    i4Result = SNMP_SUCCESS;
                }
                else
                {

                    i4Result = SNMPAccessGet (&SnmpDbEntry, pNextData,
                                              pNextIndex, pError, u4VcNum);
                }

                if (pCur->u4OIDType == SNMP_DATA_TYPE_MAC_ADDRESS)
                    pCur->u4OIDType = SNMP_DATA_TYPE_OCTET_PRIM;
                pNextData->i2_DataType = (INT2) pCur->u4OIDType;
                SNMPFormOid (pNextOid, pCur, pNextIndex);
                if (i4Result == SNMP_SUCCESS)
                {
                    *pError = SNMP_ERR_NO_ERROR;
                    return i4Result;
                }
                else if ((*pError) != SNMP_UNKNOWN_CONTEXT_NAME)
                {
                    *pError = SNMP_EXCEPTION_END_OF_MIB_VIEW;
                    return i4Result;
                }
            }
            if (pNext == NULL)
            {
                pCur = NULL;
                UNUSED_PARAM (pCur);
                *pError = SNMP_EXCEPTION_END_OF_MIB_VIEW;
                return SNMP_FAILURE;
            }
            do
            {
                if ((pNext != NULL) &&
                    (pCur->GetNextIndex != pNext->GetNextIndex))
                {
                    pCur = pNext;
                    pOID = &(pCur->ObjectID);
                    GetMbDbEntry (*pOID, &pNext, &u4Len, &SnmpDbEntry);
                    SNMPUpdateGetNextEOID (pOID, &pNext, &u4Len, &SnmpDbEntry);
                    break;
                }

                pCur = pNext;
                pOID = &(pCur->ObjectID);
                GetMbDbEntry (*pOID, &pNext, &u4Len, &SnmpDbEntry);
                SNMPUpdateGetNextEOID (pOID, &pNext, &u4Len, &SnmpDbEntry);
                /* Skip all the columnar objects in the table if the
                 * table is empty. */
                if (u4SkipGetNext == SNMP_FALSE)
                {
                    break;
                }

            }
            while ((pNext != NULL)
                   && (pCur->GetNextIndex == pNext->GetNextIndex));

            u4SkipGetNext = SNMP_FALSE;
            pCur = SnmpDbEntry.pMibEntry;
            if (pCur == NULL && pNext == NULL)
            {
                *pError = SNMP_EXCEPTION_END_OF_MIB_VIEW;
                return SNMP_FAILURE;
            }
            if (pCur != NULL)
            {
                pOID = &(pCur->ObjectID);
            }
            else if (pCur == NULL && pNext != NULL)
            {
                pOID = &(pNext->ObjectID);
                GetMbDbEntry (*pOID, &pNext, &u4Len, &SnmpDbEntry);
                SNMPUpdateGetNextEOID (pOID, &pNext, &u4Len, &SnmpDbEntry);
                pCur = SnmpDbEntry.pMibEntry;
            }
        }

        if (IS_SCALAR (pCur))
        {
            while (pCur != NULL
                   && ((pCur->u4OIDAccess == SNMP_NOACCESS)
                       || (pCur->u4OIDAccess == SNMP_ACCESSIBLEFORNOTIFY)
                       || (pCur->u4OIDAccess == SNMP_READONLY)))
            {
                pOID = &(pCur->ObjectID);
                GetMbDbEntry (*pOID, &pNext, &u4Len, &SnmpDbEntry);
                SNMPUpdateGetNextEOID (pOID, &pNext, &u4Len, &SnmpDbEntry);
                if ((pNext != NULL)
                    && ((pNext->u4OIDAccess != SNMP_NOACCESS)
                        && (pNext->u4OIDAccess != SNMP_ACCESSIBLEFORNOTIFY)
                        && (pNext->u4OIDAccess != SNMP_READONLY)))
                {
                    pCur = pNext;
                    SnmpDbEntry.pMibEntry = pCur;
                    SnmpDbEntry.pLockPointer = SnmpDbEntry.pNextLockPointer;
                    SnmpDbEntry.pUnlockPointer = SnmpDbEntry.pNextUnlockPointer;
                    SnmpDbEntry.pSetContextPointer =
                        SnmpDbEntry.pNextSetContextPointer;
                    SnmpDbEntry.pReleaseContextPointer =
                        SnmpDbEntry.pNextReleaseContextPointer;
                    break;
                }
                else
                {
                    pCur = pNext;
                }
            }
            if (IS_TABLE (pCur))
            {
                SnmpDbEntry.pMibEntry = pCur;
                SnmpDbEntry.pLockPointer = SnmpDbEntry.pNextLockPointer;
                SnmpDbEntry.pUnlockPointer = SnmpDbEntry.pNextUnlockPointer;
                SnmpDbEntry.pSetContextPointer =
                    SnmpDbEntry.pNextSetContextPointer;
                SnmpDbEntry.pReleaseContextPointer =
                    SnmpDbEntry.pNextReleaseContextPointer;
                continue;
            }

            if (NULL == pCur)
            {
                SNMPTrace ("\n FATAL DB Error :");
                *pError = SNMP_ERR_GEN_ERR;
                return SNMP_FAILURE;
            }
            SNMPFormOid (pNextOid, pCur, NULL);
            if (pCur->u4OIDType == SNMP_DATA_TYPE_MAC_ADDRESS)
                pCur->u4OIDType = SNMP_DATA_TYPE_OCTET_PRIM;

            pNextData->i2_DataType = (INT2) pCur->u4OIDType;
            if (pCur->Get == NULL)
            {
                SNMPTrace ("\n FATAL DB Error :");
                *pError = SNMP_ERR_GEN_ERR;
                return SNMP_FAILURE;
            }
            *pError = SNMP_ERR_NO_ERROR;
            i4Result =
                SNMPAccessGet (&SnmpDbEntry, pNextData, NULL, pError, u4VcNum);
            if (i4Result == SNMP_SUCCESS)
            {
                *pError = SNMP_ERR_NO_ERROR;
                return SNMP_SUCCESS;
            }
            else if ((*pError) != SNMP_UNKNOWN_CONTEXT_NAME)
            {
                *pError = SNMP_ERR_GEN_ERR;
                return SNMP_FAILURE;
            }

            if (pNext == NULL)
            {
                pCur = NULL;
                UNUSED_PARAM (pCur);
                *pError = SNMP_EXCEPTION_END_OF_MIB_VIEW;
                return SNMP_FAILURE;
            }
            pCur = pNext;
            pOID = &(pCur->ObjectID);
            GetMbDbEntry (*pOID, &pNext, &u4Len, &SnmpDbEntry);
            SNMPUpdateGetNextEOID (pOID, &pNext, &u4Len, &SnmpDbEntry);
            pCur = SnmpDbEntry.pMibEntry;
            if (pCur == NULL && pNext == NULL)
            {
                *pError = SNMP_EXCEPTION_END_OF_MIB_VIEW;
                return SNMP_FAILURE;
            }
            if (pCur != NULL)
            {
                pOID = &(pCur->ObjectID);
            }
            else if (pCur == NULL && pNext != NULL)
            {
                pOID = &(pNext->ObjectID);
                GetMbDbEntry (*pOID, &pNext, &u4Len, &SnmpDbEntry);
                SNMPUpdateGetNextEOID (pOID, &pNext, &u4Len, &SnmpDbEntry);
                pCur = SnmpDbEntry.pMibEntry;
            }
        }
        if (pCur == NULL)
        {
            break;
        }
    }
    *pError = SNMP_EXCEPTION_END_OF_MIB_VIEW;
    return i4Result;
}

/************************************************************************
 *  Function Name   : SNMPOIDIsRowStatus 
 *  Description     : Function to get rowstatus for oid
 *  Input           : OID - Given Oid
 *                   
 *  Output          : u4RowStatus - rowtstatus of SnmpDbEntry
 *
 *  Returns         :SNMP_SUCCESS or SNMP_FAILURE
 ************************************************************************/
INT4
SNMPOIDIsRowStatus (tSNMP_OID_TYPE OID, UINT4 *pu4RowStatus)
{
    tSnmpDbEntry        SnmpDbEntry;
    UINT4               u4Len = 0;
    tMbDbEntry         *pNext = NULL;

    GetMbDbEntry (OID, &pNext, &u4Len, &SnmpDbEntry);
    if (SnmpDbEntry.pMibEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    else
    {
        *pu4RowStatus = SnmpDbEntry.pMibEntry->u2RowStatus;
        return SNMP_SUCCESS;
    }
}

/************************************************************************ 
 *  Function Name   : SNMPSet 
 *  Description     : Function to do snmp set operation for a given oid
 *  Input           : OID - Given Oid
 *                    pIndex - Index Array pointer. Memory is allocated
 *                    by application. This is just to avoid memory 
 *                    allocation at DB access level.
 *                    pVal - Value to be set.
 *  Output          : pError - Error number in case failure
 *  Returns         : SUCCESS or FAILURE
 ************************************************************************/
INT4
SNMPSet (tSNMP_OID_TYPE OID, tSNMP_MULTI_DATA_TYPE * pVal, UINT4 *pError,
         tSnmpIndex * pIndex, UINT4 u4VcNum)
{
    tSnmpDbEntry        SnmpDbEntry;
    tMbDbEntry         *pCur = NULL, *pNext = NULL;
    struct tm          *pTime;
    tAuditInfo         *pTempAudit = NULL;
    time_t              t;
    UINT1              *pu1Oid = NULL;
    UINT1              *pu1Data = NULL;
    INT4                i4Result = SNMP_FAILURE;
    UINT4               u4Len = 0;
    INT4                ai4AuditDisableOid[] =
        { 1, 3, 6, 1, 4, 1, 2076, 81, 1, 48 };
    UINT1               au1UserName[ENM_MAX_USERNAME_LEN];
    CHR1                au1SnmpBufMsg[SNMP_MSG_BUF_LEN];
    tUtlInAddr          InAddr;
    tUtlIn6Addr         In6Addr;
    tMgrIpAddr          ManagerIpAddr;

    MEMSET (au1UserName, 0, ENM_MAX_USERNAME_LEN);
    MEMSET (au1SnmpBufMsg, 0, SNMP_MSG_BUF_LEN);
    MEMSET (&InAddr, 0, sizeof (tUtlInAddr));
    MEMSET (&In6Addr, 0, sizeof (tUtlIn6Addr));
    MEMSET (&ManagerIpAddr, 0, sizeof (tMgrIpAddr));

    GetMbDbEntry (OID, &pNext, &u4Len, &SnmpDbEntry);
    pCur = SnmpDbEntry.pMibEntry;
    if (pCur != NULL)
    {
        if (pCur->Set == NULL)
        {
            *pError = SNMP_ERR_NO_ACCESS;
            return SNMP_FAILURE;
        }

        i4Result = SNMPGetIndices (OID, u4Len, pCur, &pIndex);
        if ((i4Result == SNMP_FAILURE) || (i4Result == NO_INDEX_LEN_MATCH)
            || (i4Result == SNMP_ERR_NO_CREATION))
        {
            *pError = SNMP_ERR_GEN_ERR;
            return SNMP_FAILURE;
        }
        else if ((i4Result == NO_INDEX) && (IS_TABLE (pCur)))
        {
            SNMP_INR_OUT_NO_SUCH_NAME;
            *pError = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
        if (SNMPAccessSet (&SnmpDbEntry, pVal, pIndex, u4VcNum) == SNMP_SUCCESS)
        {
            pu1Oid = MemAllocMemBlk (gSnmpMultiOidPoolId);
            if (pu1Oid == NULL)
            {
                return SNMP_FAILURE;
            }
            MEMSET (pu1Oid, 0, sizeof (tSnmpMultiOidBlock));
            SNMPGetOidString (OID.pu4_OidList, OID.u4_Length, pu1Oid);
            SnmpRevertEoidString (pu1Oid);

            pu1Data = MemAllocMemBlk (gSnmpDataPoolId);
            if (pu1Data == NULL)
            {
                MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1Oid);
                return SNMP_FAILURE;
            }
            MEMSET (pu1Data, 0, MAX_SNMP_DATA_LENGTH);
            SNMPConvertDataToString (pVal, pu1Data, (UINT2) pVal->i2_DataType);

            if (MsrGetRestorationStatus () != ISS_TRUE)
            {
                if (WebnmGetSnmpQueryFromWebStatus () == OSIX_TRUE)
                {
                    WebnmGetWebUsrNameForSysLogMsg (au1UserName);
                    SYS_LOG_CMD ((SYSLOG_DEBUG_LEVEL, SYSLOG_DEF_USER_INDEX,
                                  "WEBNM %s %s %s", au1UserName, pu1Oid,
                                  pu1Data));
                }
                else
                {
                    /* Gathering Manager IP address */
                    SNMPGetRemoteManagerIpAddr (&ManagerIpAddr);
                    if (ManagerIpAddr.u4AddrType == IPVX_ADDR_FMLY_IPV4)
                    {
                        MEMCPY (&InAddr, &(ManagerIpAddr.uIpAddr.u4Ip4Addr),
                                sizeof (tUtlInAddr));
                        SPRINTF (au1SnmpBufMsg, "from %s",
                                 UtlInetNtoa (InAddr));
                    }
                    else if (ManagerIpAddr.u4AddrType == IPVX_ADDR_FMLY_IPV6)
                    {
                        MEMCPY (&(In6Addr.u1addr),
                                &(ManagerIpAddr.uIpAddr.au1Ip6Addr),
                                sizeof (tUtlIn6Addr));
                        SPRINTF (au1SnmpBufMsg, "from %s",
                                 UtlInetNtoa6 (In6Addr));
                    }
                    SYS_LOG_CMD ((SYSLOG_DEBUG_LEVEL, SYSLOG_DEF_USER_INDEX,
                                  "SNMP %s SET %s value %s %s SUCCESS",
                                  gi1SnmpUserName, pu1Oid, pu1Data,
                                  au1SnmpBufMsg));
                }
            }

            if ((ISS_IS_AUDIT_ENABLED () == ISS_TRUE) ||
                ((SnmpDbEntry.pMibEntry->ObjectID.u4_Length ==
                  sizeof (ai4AuditDisableOid) / sizeof (INT4)) &&
                 (MEMCMP (SnmpDbEntry.pMibEntry->ObjectID.pu4_OidList,
                          ai4AuditDisableOid,
                          sizeof (ai4AuditDisableOid))) == 0))
            {
                pTempAudit = MemAllocMemBlk (gSnmpAuditInfoPoolId);
                if (pTempAudit == NULL)
                {
                    MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1Oid);
                    MemReleaseMemBlock (gSnmpDataPoolId, pu1Data);
                    return SNMP_FAILURE;
                }
                MEMSET (pTempAudit, 0, sizeof (tAuditInfo));
                STRCPY (pTempAudit->TempSnmp.au4_OidList, "Audit:");
                STRNCAT (pTempAudit->TempSnmp.au4_OidList, pu1Oid,
                         STRLEN (pu1Oid));
                MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1Oid);
                pTempAudit->TempSnmp.pu1ObjVal = pu1Data;
                STRNCPY (pTempAudit->TempSnmp.ai1SnmpUser, gi1SnmpUserName,
                         sizeof (pTempAudit->TempSnmp.ai1SnmpUser));
                pTempAudit->TempSnmp.u4MngrIpAddr = SNMPGetManagerAddr ();
                STRCPY (pTempAudit->TempSnmp.ai1Status, "Succeeded");
                pTime = (localtime (&t));
                if (pTime != NULL)
                {
                    STRNCPY (pTempAudit->au1Time, asctime (pTime),
                             sizeof (pTempAudit->au1Time));
                }
                pTempAudit->i2Flag = AUDIT_SNMP_MSG;
                if (MSRSendAuditInfo (pTempAudit) != MSR_SUCCESS)
                {
                    SNMPTrace ("Error in Audit queue send from SNMP Set\n");
                    MemReleaseMemBlock (gSnmpAuditInfoPoolId,
                                        (UINT1 *) pTempAudit);
                    MemReleaseMemBlock (gSnmpDataPoolId, pu1Data);
                    return SNMP_FAILURE;
                }
                MemReleaseMemBlock (gSnmpAuditInfoPoolId, (UINT1 *) pTempAudit);
            }
            else
            {
                MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1Oid);
                MemReleaseMemBlock (gSnmpDataPoolId, pu1Data);
            }
            return SNMP_SUCCESS;
        }
        else
        {
            *pError = SNMP_ERR_COMMIT_FAILED;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pError = SNMP_ERR_GEN_ERR;
        return SNMP_FAILURE;
    }
}

/************************************************************************
 *  Function Name   : SNMPTest 
 *  Description     : Function will test  the value to be set for a given oid
 *  Input           : OID - Given Oid
 *                    pIndex - Index Array pointer. Memory is allocated
 *                    by application. This is just to avoid memory 
 *                    allocation at DB access level.
 *                    pVal - Value to be set.
 *  Output          : pError - Error number in case failure
 *  Returns         : SUCCESS or FAILURE
 ************************************************************************/
INT4
SNMPTest (tSNMP_OID_TYPE OID, tSNMP_MULTI_DATA_TYPE * pVal, UINT4 *pError,
          tSnmpIndex * pIndex, UINT4 u4VcNum)
{
    tSnmpDbEntry        SnmpDbEntry;
    tMbDbEntry         *pCur = NULL, *pNext = NULL;
    UINT4               u4Len = 0;
    INT4                i4Result = SNMP_FAILURE;
    tIssCustSnmpConfInfo IssCustSnmpConfInfo;

    GetMbDbEntry (OID, &pNext, &u4Len, &SnmpDbEntry);
    pCur = SnmpDbEntry.pMibEntry;
    if (pCur != NULL)
    {
        if (pCur->Test == NULL)
        {
            *pError = SNMP_ERR_NOT_WRITABLE;
            SNMP_INR_OUT_READ_ONLY;
            return SNMP_FAILURE;
        }
        i4Result = SNMPGetIndices (OID, u4Len, pCur, &pIndex);
        if ((i4Result == SNMP_FAILURE) || (i4Result == NO_INDEX_LEN_MATCH))
        {
            *pError = SNMP_EXCEPTION_NO_SUCH_INSTANCE;
            SNMP_INR_OUT_GEN_ERROR;
            return SNMP_FAILURE;
        }
        else if (i4Result == SNMP_ERR_NO_CREATION)
        {
            *pError = (UINT4) i4Result;
            SNMP_INR_OUT_GEN_ERROR;
            return SNMP_FAILURE;
        }
        else if ((i4Result == NO_INDEX) && (IS_TABLE (pCur)))
        {
            SNMP_INR_OUT_NO_SUCH_NAME;
            *pError = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
        else
        {
            if (pCur->u4OIDType == SNMP_DATA_TYPE_MAC_ADDRESS)
                pCur->u4OIDType = SNMP_DATA_TYPE_OCTET_PRIM;
            if (pVal->i2_DataType != (INT2) pCur->u4OIDType)
            {
                *pError = SNMP_ERR_WRONG_TYPE;
                SNMP_INR_OUT_GEN_ERROR;
                return SNMP_FAILURE;
            }
            *pError = SNMP_ERR_NO_ERROR;
            /*Check whether the configuartion is allowed or not */
            IssCustSnmpConfInfo.pMidLevelFuncPtr =
                (VOID *) SnmpDbEntry.pMibEntry->Test;
            IssCustSnmpConfInfo.pSnmpMultiDataVal = (VOID *) pVal;
            IssCustSnmpConfInfo.pSnmpIndex = (VOID *) pIndex;
            IssCustSnmpConfInfo.pSnmpOId = (VOID *) &OID;
#ifdef ISS_WANTED
            if (IssCustConfControlSnmp (&IssCustSnmpConfInfo) == ISS_FAILURE)
            {
                *pError = SNMP_ERR_AUTHORIZATION_ERROR;
                return SNMP_FAILURE;
            }
#endif
            i4Result = SNMPAccessTest (&SnmpDbEntry, pVal, pIndex, pError,
                                       u4VcNum);
            if (i4Result == SNMP_FAILURE)
            {
                /*  
                 *  The Case has been added to return GEN_ERR if the low 
                 *  level test routine doesn't update the Error Code, when
                 *  returning Failure. This has to be properly taken care
                 *  in Low level Test Routines
                 */
                if (*pError == SNMP_ERR_NO_ERROR)
                {
                    *pError = SNMP_ERR_GEN_ERR;
                    SNMP_INR_OUT_GEN_ERROR;
                    SNMPTrace ("Low level Test routine hasn't updated the"
                               "error code properly...\n");
                }
            }
            return i4Result;
        }
    }
    SNMP_INR_OUT_NO_SUCH_NAME;
    *pError = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/************************************************************************
 *  Function Name   : SNMPDependency
 *  Description     : Function will test  the value to be set for a given oid
 *  Input           : OID - Given Oid
 *                VarBindList -  List of varbind in set PDU
 *                pError    - Pointer to the Error code to be set in case of failure
 *              IndexList - Pointer to list of indices for each varbind in set PDU
 *              u4VcNum -   number of SNMP Context for the current context
 *  Output          : pError - Error number in case failure
 *  Returns         : SUCCESS or FAILURE
 ************************************************************************/
INT4
SNMPDependency (tSNMP_OID_TYPE OID, tSNMP_VAR_BIND * VarBindList, UINT4 *pError,
                tSnmpIndexList * IndexList, UINT4 u4VcNum)
{
    tSnmpDbEntry        SnmpDbEntry;
    tMbDbEntry         *pCur = NULL, *pNext = NULL;
    UINT4               u4Len = 0;
    INT4                i4Result = SNMP_FAILURE;
    tIssCustSnmpConfInfo IssCustSnmpConfInfo;

    GetMbDbEntry (OID, &pNext, &u4Len, &SnmpDbEntry);
    pCur = SnmpDbEntry.pMibEntry;
    if (pCur != NULL)
    {
        if (pCur->Dep == NULL)
        {
            *pError = SNMP_ERR_NOT_WRITABLE;
            SNMP_INR_OUT_READ_ONLY;
            return SNMP_FAILURE;
        }
        else
        {
            *pError = SNMP_ERR_NO_ERROR;
            /*Check whether the configuartion is allowed or not */
            IssCustSnmpConfInfo.pMidLevelFuncPtr =
                (VOID *) SnmpDbEntry.pMibEntry->Dep;
            /*IssCustSnmpConfInfo.pSnmpMultiDataVal = (VOID *) pVal; */
            IssCustSnmpConfInfo.pSnmpIndex = (VOID *) IndexList->pIndexList;
            IssCustSnmpConfInfo.pSnmpOId = (VOID *) &OID;

#ifdef ISS_WANTED
            if (IssCustConfControlSnmp (&IssCustSnmpConfInfo) == ISS_FAILURE)
            {
                *pError = SNMP_ERR_AUTHORIZATION_ERROR;
                return SNMP_FAILURE;
            }
#endif
            i4Result =
                SNMPAccessDep (&SnmpDbEntry, pError, IndexList, VarBindList,
                               u4VcNum);
            if (i4Result == SNMP_FAILURE)
            {
                /*  
                 *  The Case has been added to return GEN_ERR if the low 
                 *  level test routine doesn't update the Error Code, when
                 *  returning Failure. This has to be properly taken care
                 *  in Low level Dep Routines
                 */
                if (*pError == SNMP_ERR_NO_ERROR)
                {
                    *pError = SNMP_ERR_GEN_ERR;
                    SNMP_INR_OUT_GEN_ERROR;
                    SNMPTrace ("Low level Dep routine hasn't updated the"
                               "error code properly...\n");
                }
            }
            return i4Result;
        }
    }
    SNMP_INR_OUT_NO_SUCH_NAME;
    *pError = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/************************************************************************
 *  Function Name   : SNMPFormOid 
 *  Description     : Function to form OID from index array and actual OID
 *  Input           : pIndex - Index Array
 *                    pCur - DB entry of a  OID for which full OID to be 
 *                    formed
 *  Output          : pOid - Full Oid (Oid + Instance)
 *  Returns         : None
 ************************************************************************/
VOID
SNMPFormOid (tSNMP_OID_TYPE * pOid, const tMbDbEntry * pCur,
             const tSnmpIndex * pIndex)
{
    UINT4               u4Count = 0;
    UINT4               u4CountTemp;
    UINT1               au1Address[4];
    UINT4               u4Address = 0, u4Index = 0;

    memcpy (pOid->pu4_OidList, pCur->ObjectID.pu4_OidList,
            pCur->ObjectID.u4_Length * sizeof (UINT4));
    pOid->u4_Length = pCur->ObjectID.u4_Length;
    if (pIndex != NULL)
    {
        for (u4Count = SNMP_ZERO, u4CountTemp = SNMP_ZERO;
             u4Count < pCur->u4NoIndices; u4Count++, u4CountTemp++)
        {
            switch (pCur->pu1IndexType[u4CountTemp])
            {
                case SNMP_DATA_TYPE_UNSIGNED32:
                case SNMP_DATA_TYPE_COUNTER:
                case SNMP_DATA_TYPE_TIME_TICKS:
                    pOid->pu4_OidList[pOid->u4_Length++] =
                        pIndex->pIndex[u4Count].u4_ULongValue;
                    break;
                case SNMP_DATA_TYPE_INTEGER32:
                    pOid->pu4_OidList[pOid->u4_Length++] =
                        (UINT4) (pIndex->pIndex[u4Count].i4_SLongValue);
                    break;
                case SNMP_DATA_TYPE_IP_ADDR_PRIM:
                    u4Address = pIndex->pIndex[u4Count].u4_ULongValue;
                    u4Address = OSIX_NTOHL (u4Address);
                    MEMCPY (au1Address, &u4Address, sizeof (UINT4));
                    pOid->pu4_OidList[pOid->u4_Length++] = au1Address[0];
                    pOid->pu4_OidList[pOid->u4_Length++] = au1Address[1];
                    pOid->pu4_OidList[pOid->u4_Length++] = au1Address[2];
                    pOid->pu4_OidList[pOid->u4_Length++] = au1Address[3];
                    break;
                case SNMP_DATA_TYPE_OCTET_PRIM:
                case SNMP_DATA_TYPE_OPAQUE:
                    pOid->pu4_OidList[pOid->u4_Length++] =
                        (UINT4) (pIndex->pIndex[u4Count].pOctetStrValue->
                                 i4_Length);
                case SNMP_DATA_TYPE_IMP_OCTET_PRIM:
                    for (u4Index = SNMP_ZERO; u4Index <
                         (UINT4) pIndex->pIndex[u4Count].pOctetStrValue->
                         i4_Length; u4Index++)
                    {
                        if (pOid->u4_Length < MAX_OID_LENGTH)
                        {
                            pOid->pu4_OidList[pOid->u4_Length++] =
                                pIndex->pIndex[u4Count].pOctetStrValue->
                                pu1_OctetList[u4Index];
                        }
                    }
                    break;
                case SNMP_DATA_TYPE_OBJECT_ID:
                    pOid->pu4_OidList[pOid->u4_Length] =
                        pIndex->pIndex[u4Count].pOidValue->u4_Length;
                    pOid->u4_Length++;
                case SNMP_DATA_TYPE_IMP_OBJECT_ID:
                    for (u4Index = SNMP_ZERO; u4Index <
                         (UINT4) pIndex->pIndex[u4Count].pOidValue->u4_Length;
                         u4Index++)
                    {
                        pOid->pu4_OidList[pOid->u4_Length++] =
                            pIndex->pIndex[u4Count].pOidValue->
                            pu4_OidList[u4Index];
                    }
                    break;
                case SNMP_FIXED_LENGTH_OCTET_STRING:
                    /* To handle Fixed OctetString Index 
                     *  Get the length from protocol db.h file(generated 
                     *  using midgen tool)  
                     *  Copy only the specified no of octets to form OID 
                     *  Read Next Index    */
                    for (u4Index = 0; u4Index <
                         (UINT4) pCur->pu1IndexType[u4CountTemp + 1]; u4Index++)
                    {
                        pOid->pu4_OidList[pOid->u4_Length++] =
                            pIndex->pIndex[u4Count].pOctetStrValue->
                            pu1_OctetList[u4Index];
                    }
                    u4CountTemp++;
                    break;
                default:
                    SNMPTrace ("Data Type Unknown\n");
            }
        }
    }
    else
    {
        if (pCur->u4NoIndices == SNMP_ZERO)
        {
            pOid->pu4_OidList[pOid->u4_Length] = SNMP_ZERO;
            pOid->u4_Length++;
        }
    }
}

/********* ******* END OF FILE *******************************/
