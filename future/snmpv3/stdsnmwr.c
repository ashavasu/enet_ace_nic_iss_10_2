/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: stdsnmwr.c,v 1.4 2015/04/28 12:35:03 siva Exp $
 *
 * Description:Wrapper functions generated by midgen-v2  
 *******************************************************************/

#include "lr.h"
#include "fssnmp.h"
#include "stdsnmwr.h"
#include "stdsnmlw.h"
#include "stdsnmdb.h"
#include "snmpcmn.h"
VOID
RegisterSNMPMIB ()
{
    SNMPRegisterMib (&stdsnmOID, &stdsnmEntry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&stdsnmOID, (const UINT1 *) "snmpMIB");
}

INT4
SysDescrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSysDescr (pMultiData->pOctetStrValue));
}

INT4
SysObjectIDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSysObjectID (pMultiData->pOidValue));
}

INT4
SysUpTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSysUpTime (&pMultiData->u4_ULongValue));
}

INT4
SysContactTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SysContact (pu4Error, pMultiData->pOctetStrValue));
}

INT4
SysContactSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSysContact (pMultiData->pOctetStrValue));
}

INT4
SysContactGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSysContact (pMultiData->pOctetStrValue));
}

INT4
SysNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SysName (pu4Error, pMultiData->pOctetStrValue));
}

INT4
SysNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSysName (pMultiData->pOctetStrValue));
}

INT4
SysNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSysName (pMultiData->pOctetStrValue));
}

INT4
SysLocationTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SysLocation (pu4Error, pMultiData->pOctetStrValue));
}

INT4
SysLocationSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSysLocation (pMultiData->pOctetStrValue));
}

INT4
SysLocationGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSysLocation (pMultiData->pOctetStrValue));
}

INT4
SysServicesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSysServices (&pMultiData->i4_SLongValue));
}

INT4
SysORLastChangeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSysORLastChange (&pMultiData->u4_ULongValue));
}

INT4
SysContactDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SysContact (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
SysNameDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SysName (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
SysLocationDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SysLocation (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
SysORIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceSysORTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[0].i4_SLongValue;
    return SNMP_SUCCESS;
}

INT4
SysORIDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceSysORTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSysORID (pMultiIndex->pIndex[0].i4_SLongValue,
                           pMultiData->pOidValue));
}

INT4
SysORDescrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceSysORTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSysORDescr (pMultiIndex->pIndex[0].i4_SLongValue,
                              pMultiData->pOctetStrValue));
}

INT4
SysORUpTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceSysORTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetSysORUpTime (pMultiIndex->pIndex[0].i4_SLongValue,
                               &pMultiData->u4_ULongValue));
}

INT4
SnmpInPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpInPkts (&pMultiData->u4_ULongValue));
}

INT4
SnmpOutPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpOutPkts (&pMultiData->u4_ULongValue));
}

INT4
SnmpInBadVersionsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpInBadVersions (&pMultiData->u4_ULongValue));
}

INT4
SnmpInBadCommunityNamesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpInBadCommunityNames (&pMultiData->u4_ULongValue));
}

INT4
SnmpInBadCommunityUsesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpInBadCommunityUses (&pMultiData->u4_ULongValue));
}

INT4
SnmpInASNParseErrsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpInASNParseErrs (&pMultiData->u4_ULongValue));
}

INT4
SnmpInTooBigsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpInTooBigs (&pMultiData->u4_ULongValue));
}

INT4
SnmpInNoSuchNamesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpInNoSuchNames (&pMultiData->u4_ULongValue));
}

INT4
SnmpInBadValuesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpInBadValues (&pMultiData->u4_ULongValue));
}

INT4
SnmpInReadOnlysGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpInReadOnlys (&pMultiData->u4_ULongValue));
}

INT4
SnmpInGenErrsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpInGenErrs (&pMultiData->u4_ULongValue));
}

INT4
SnmpInTotalReqVarsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpInTotalReqVars (&pMultiData->u4_ULongValue));
}

INT4
SnmpInTotalSetVarsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpInTotalSetVars (&pMultiData->u4_ULongValue));
}

INT4
SnmpInGetRequestsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpInGetRequests (&pMultiData->u4_ULongValue));
}

INT4
SnmpInGetNextsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpInGetNexts (&pMultiData->u4_ULongValue));
}

INT4
SnmpInSetRequestsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpInSetRequests (&pMultiData->u4_ULongValue));
}

INT4
SnmpInGetResponsesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpInGetResponses (&pMultiData->u4_ULongValue));
}

INT4
SnmpInTrapsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpInTraps (&pMultiData->u4_ULongValue));
}

INT4
SnmpOutTooBigsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpOutTooBigs (&pMultiData->u4_ULongValue));
}

INT4
SnmpOutNoSuchNamesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpOutNoSuchNames (&pMultiData->u4_ULongValue));
}

INT4
SnmpOutBadValuesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpOutBadValues (&pMultiData->u4_ULongValue));
}

INT4
SnmpOutGenErrsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpOutGenErrs (&pMultiData->u4_ULongValue));
}

INT4
SnmpOutGetRequestsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpOutGetRequests (&pMultiData->u4_ULongValue));
}

INT4
SnmpOutGetNextsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpOutGetNexts (&pMultiData->u4_ULongValue));
}

INT4
SnmpOutSetRequestsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpOutSetRequests (&pMultiData->u4_ULongValue));
}

INT4
SnmpOutGetResponsesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpOutGetResponses (&pMultiData->u4_ULongValue));
}

INT4
SnmpOutTrapsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpOutTraps (&pMultiData->u4_ULongValue));
}

INT4
SnmpEnableAuthenTrapsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SnmpEnableAuthenTraps
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
SnmpEnableAuthenTrapsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSnmpEnableAuthenTraps (pMultiData->i4_SLongValue));
}

INT4
SnmpEnableAuthenTrapsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpEnableAuthenTraps (&pMultiData->i4_SLongValue));
}

INT4
SnmpSilentDropsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpSilentDrops (&pMultiData->u4_ULongValue));
}

INT4
SnmpProxyDropsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpProxyDrops (&pMultiData->u4_ULongValue));
}

INT4
SnmpEnableAuthenTrapsDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SnmpEnableAuthenTraps
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
SnmpSetSerialNoTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SnmpSetSerialNo (pu4Error, pMultiData->i4_SLongValue));
}

INT4
SnmpSetSerialNoSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSnmpSetSerialNo (pMultiData->i4_SLongValue));
}

INT4
SnmpSetSerialNoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSnmpSetSerialNo (&pMultiData->i4_SLongValue));
}

INT4
GetNextIndexSysORTable (tSnmpIndex * pFirstMultiIndex,
                        tSnmpIndex * pNextMultiIndex)
{
    INT4                i4sysORIndex;
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexSysORTable (&i4sysORIndex) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexSysORTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4sysORIndex) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4sysORIndex;
    return SNMP_SUCCESS;
}

INT4
SnmpSetSerialNoDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SnmpSetSerialNo (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
