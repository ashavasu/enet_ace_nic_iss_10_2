
/********************************************************************
 * Copyright (C) Future Software Limited,2003 
 *
 * $Id: snmfslnx.c,v 1.6 2015/04/28 12:35:02 siva Exp $
 *
 * Description:Routines for Snmp Agent Init and main Module specific for
 *              Linux sockets.
 *******************************************************************/
#include "snmpcmn.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <unistd.h>

#define MAX_SNMP_PKT_LEN 1024
#define SNMP_FOREVER()     for(;;)

PRIVATE INT4        SnmpSockInit (INT4 *pi4Socket);
PRIVATE VOID        SnmpMain (INT1 *);
INT4                gi4Socket = SNMP_NOT_OK;
INT4                gi4ProxySocket = SNMP_NOT_OK;
UINT4               gu4ManagerAddr = SNMP_ZERO;
UINT4               gu4ManagerPort = SNMP_ZERO;
/************************************************************************
 *  Function Name   : SNMPProtoInit
 *  Description     : Function to Create SNMP Agent Task. Called from main
 *  Input           : None
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
SNMPProtoInit (VOID)
{
    tOsixTaskId         u4SnmpTaskId;
    if (OsixCreateTask ((const UINT1 *) SNMP_TASK_NAME, SNMP_TASK_PRIORITY,
                        SNMP_STACK_SIZE, SnmpMain, NULL, OSIX_DEFAULT_TASK_MODE,
                        &u4SnmpTaskId) != OSIX_SUCCESS)
    {
        SNMPTrace ("Error During Creation of SNMP Agent Task \n");
    }
}

/************************************************************************
 *  Function Name   : SnmpSockInit 
 *  Description     : Function to create snmp agent socket and bind to
 *                    snmp agent port
 *  Input           : None
 *  Output          : None
 *  Returns         : Socket descriptor or FAILURE
 ************************************************************************/
PRIVATE INT4
SnmpSockInit (INT4 *pi4Socket)
{
    INT4                i4Sock = SNMP_NOT_OK;
    struct sockaddr_in  SockAddr;

    i4Sock = socket (AF_INET, SOCK_DGRAM, SNMP_ZERO);
    if (i4Sock < SNMP_ZERO)
    {
        SNMPTrace ("SNMP:Unable to Open Socket\n");
        return SNMP_FAILURE;
    }
    MEMSET (&SockAddr, SNMP_ZERO, sizeof (SockAddr));
    SockAddr.sin_family = AF_INET;
    SockAddr.sin_port = OSIX_HTONS (SNMP_PORT);
    SockAddr.sin_addr.s_addr = OSIX_HTONL (SNMP_ZERO);

    if (bind (i4Sock, (struct sockaddr *) &SockAddr, sizeof (SockAddr))
        < SNMP_ZERO)
    {
        close (i4Sock);
        SNMPTrace ("SNMP:Unable to Bind Socket\n");
        return SNMP_FAILURE;
    }
    *pi4Socket = i4Sock;
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : SnmpProxySockInit 
 *  Description     : Function to create snmp Proxy socket and bind to
 *                    snmp Proxy Trap port
 *  Input           : None
 *  Output          : None
 *  Returns         : Socket descriptor or FAILURE
 ************************************************************************/
PRIVATE INT4
SnmpProxySockInit (INT4 *pi4Socket)
{
    INT4                i4Sock = SNMP_NOT_OK;
    struct sockaddr_in  SockAddr;

    i4Sock = socket (AF_INET, SOCK_DGRAM, SNMP_ZERO);
    if (i4Sock < SNMP_ZERO)
    {
        SNMPTrace ("SNMP:Unable to Open Socket\n");
        return SNMP_FAILURE;
    }
    MEMSET (&SockAddr, SNMP_ZERO, sizeof (SockAddr));
    SockAddr.sin_family = AF_INET;
    SockAddr.sin_port = OSIX_HTONS (SNMP_MANAGER_PORT);
    SockAddr.sin_addr.s_addr = OSIX_HTONL (SNMP_ZERO);

    if (bind (i4Sock, (struct sockaddr *) &SockAddr, sizeof (SockAddr))
        < SNMP_ZERO)
    {
        close (i4Sock);
        SNMPTrace ("SNMP:Unable to Bind Socket\n");
        return SNMP_FAILURE;
    }
    *pi4Socket = i4Sock;
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : SnmpMain 
 *  Description     : Snmp Agent Task Entry Point Function
 *  Input           : pParam - Not in use
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
PRIVATE VOID
SnmpMain (INT1 *pParam)
{
    UINT1               au1PktArray[MAX_SNMP_PKT_LEN], *pu1SendPkt = NULL;
    INT4                i4PktSize = SNMP_ZERO, i4SendSize = SNMP_ZERO;
    INT4                i4Return = SNMP_ZERO, i4Version = SNMP_ZERO;
    INT4                socket = SNMP_ZERO, i4IsAgentSock = SNMP_FALSE;
    UINT4               u4Len = SNMP_ZERO;
    struct sockaddr_in  SnmpMgrAddr;
    fd_set              rfds;
    struct timeval      tv;
    tv.tv_sec = SNMP_ZERO;
    tv.tv_usec = SNMP_ZERO;

    UNUSED_PARAM (pParam);
    if (SnmpSockInit (&gi4Socket) == SNMP_FAILURE)
    {
        return;
    }
    if (SnmpProxySockInit (&gi4ProxySocket) == SNMP_FAILURE)
    {
        return;
    }
    SNMPInitSystemObjects ();
    SNMP_FOREVER ()
    {
        FD_ZERO (&rfds);
        FD_SET (gi4Socket, &rfds);
        FD_SET (gi4ProxySocket, &rfds);
        i4IsAgentSock = SNMP_FALSE;

        if (gi4ProxySocket > gi4Socket)
        {
            socket = gi4ProxySocket;
        }
        else
        {
            socket = gi4Socket;
        }

        /*if (select (gi4Socket + SNMP_ONE, &rfds, NULL, NULL, &tv) > SNMP_ZERO) */
        if (select (socket + SNMP_ONE, &rfds, NULL, NULL, &tv) > SNMP_ZERO)
        {
            MEMSET (&SnmpMgrAddr, SNMP_ZERO, sizeof (SnmpMgrAddr));
            SnmpMgrAddr.sin_family = AF_INET;
            SnmpMgrAddr.sin_port = OSIX_HTONS (SNMP_ZERO);
            SnmpMgrAddr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);
            u4Len = sizeof (SnmpMgrAddr);
            if (FD_ISSET (gi4Socket, &rfds))
            {
                i4PktSize = recvfrom (gi4Socket, au1PktArray, MAX_SNMP_PKT_LEN,
                                      SNMP_ZERO,
                                      (struct sockaddr *) &SnmpMgrAddr,
                                      (socklen_t *) & u4Len);
                i4IsAgentSock = SNMP_TRUE;
            }
            else if (FD_ISSET (gi4ProxySocket, &rfds))
            {
                i4PktSize =
                    recvfrom (gi4ProxySocket, au1PktArray, MAX_SNMP_PKT_LEN,
                              SNMP_ZERO, (struct sockaddr *) &SnmpMgrAddr,
                              (socklen_t *) & u4Len);
            }

            if (i4PktSize > SNMP_ZERO)
            {
                SNMPMemInit ();
                gu4ManagerAddr = OSIX_NTOHL (SnmpMgrAddr.sin_addr.s_addr);
                gu4ManagerPort = OSIX_NTOHS (SnmpMgrAddr.sin_port);
                SNMP_INR_IN_PKTS;
                i4Version = SNMPVersionCheck (au1PktArray, i4PktSize);
                if (i4Version == VERSION1 || i4Version == VERSION2)
                {
                    i4Return = SNMPProcess (au1PktArray, i4PktSize,
                                            &pu1SendPkt, (UINT4 *) &i4SendSize);
                }
                else if (i4Version == VERSION3)
                {
                    i4Return = SNMPV3Process (au1PktArray, i4PktSize,
                                              &pu1SendPkt, (UINT4 *)
                                              &i4SendSize);
                }
                else
                {
                    SNMP_INR_BAD_ASN_PARSE;
                    SNMP_INR_SILENT_DROPS;
                    continue;
                }
                if (i4Return == SNMP_SUCCESS)
                {
                    SNMP_INR_OUT_PKTS;
                    if (i4IsAgentSock == SNMP_TRUE)
                    {
                        sendto (gi4Socket, pu1SendPkt, i4SendSize, SNMP_ZERO,
                                (struct sockaddr *) &SnmpMgrAddr, u4Len);
                    }
                    else
                    {
                        sendto (gi4ProxySocket, pu1SendPkt, i4SendSize,
                                SNMP_ZERO, (struct sockaddr *) &SnmpMgrAddr,
                                u4Len);
                    }
                }
            }
        }
        else
        {
            OsixDelayTask (SNMP_ONE);
        }
    }
    return;
}

/************************************************************************
 *  Function Name   : SNMPGetManagerAddr 
 *  Description     : This will return a Manager Address 
 *  Input           : None 
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
UINT4
SNMPGetManagerAddr ()
{
    return gu4ManagerAddr;
}

/************************************************************************
 *  Function Name   : SNMPGetManagerPort 
 *  Description     : This will return a Manager Port 
 *  Input           : None 
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
UINT4
SNMPGetManagerPort ()
{
    return gu4ManagerPort;
}

/************************************************************************
 *  Function Name   : SNMPSendTrapToManager 
 *  Description     : Function to open a socket and send the trap to manager
 *  Input           : pu1Msg - Data Pointer
 *                    i4MsgLen - Data Length, u4Addr - Manager Address 
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
SNMPSendTrapToManager (UINT1 *pu1Msg, INT4 i4MsgLen, UINT4 u4Addr)
{
    INT4                i4Sock = SNMP_NOT_OK;
    struct sockaddr_in  SockAddr;

    i4Sock = socket (AF_INET, SOCK_DGRAM, SNMP_ZERO);
    if (i4Sock < SNMP_ZERO)
    {
        SNMPTrace ("SNMP:Unable to Open Socket for Trap\n");
        SNMP_INR_OUT_GEN_ERROR;
        return;
    }

    MEMSET (&SockAddr, SNMP_ZERO, sizeof (SockAddr));
    SockAddr.sin_family = AF_INET;
    SockAddr.sin_port = OSIX_HTONS (SNMP_MANAGER_PORT);
    SockAddr.sin_addr.s_addr = OSIX_HTONL (u4Addr);
    if (sendto (i4Sock, pu1Msg, i4MsgLen, SNMP_ZERO,
                (struct sockaddr *) &SockAddr, sizeof (SockAddr))
        == SNMP_NOT_OK)
    {
        SNMP_INR_OUT_GEN_ERROR;
        SNMPTrace ("SNMP: Unable to send trap to manager\n");
    }
    else
    {
        SNMP_INR_OUT_TRAP_SUCCESS;
    }
    close (i4Sock);
    return;
}

/************************************************************************
 *  Function Name   : SNMPChangeListenPort 
 *  Description     : Function to change the Listen Port
 *  Input           : i4Port - Port to be Change 
 *  Output          : None 
 *  Returns         : SNMP_SUCCESS 
 ************************************************************************/
INT4
SNMPChangeListenPort (INT4 i4Port)
{
    INT4                i4RetVal = SNMP_SUCCESS;
    INT4                i4LocalSockId = SNMP_NOT_OK;
    INT2                i2Port = (INT2) i4Port;

    struct sockaddr_in  sinLocalAddress;

    MEMSET (&sinLocalAddress, SNMP_ZERO, sizeof (sinLocalAddress));
    sinLocalAddress.sin_family = AF_INET;
    sinLocalAddress.sin_addr.s_addr = OSIX_HTONL (SNMP_ZERO);
    sinLocalAddress.sin_port = (UINT2) OSIX_HTONS (i2Port);
    if ((i4LocalSockId = socket (AF_INET, SOCK_DGRAM, SNMP_ZERO)) < SNMP_ZERO)
    {
        SNMPTrace ("SNMP:Unable to Create Socket\n");
        return SNMP_FAILURE;
    }
    i4RetVal = bind (i4LocalSockId,
                     (struct sockaddr *) &sinLocalAddress,
                     sizeof (sinLocalAddress));
    if (i4RetVal < SNMP_ZERO)
    {
        close (i4LocalSockId);
        SNMPTrace ("SNMP:Unable to Bind Socket\n");
        return SNMP_FAILURE;
    }
    close (gi4Socket);
    gi4Socket = i4LocalSockId;
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : SNMPGetTime 
 *  Description     : Function to Get Current System Time 
 *  Input           : pu4Time - Time variable to update 
 *  Output          : None 
 *  Returns         : None 
 ************************************************************************/
VOID
SNMPGetTime (UINT4 *pu4Time)
{
    OsixGetSysTime (pu4Time);
}

/************************************************************************
 *  Function Name   : SNMPShutdown 
 *  Description     : Function to Shutdown Agent Operations 
 *  Input           : None 
 *  Output          : None 
 *  Returns         : None 
 ************************************************************************/
VOID
SNMPShutdown ()
{
    UINT4               u4Count = SNMP_ZERO;
    /* Delete the SNMP Task */
    if (OsixDeleteTask (SELF, (const UINT1 *) SNMP_TASK_NAME) == OSIX_FAILURE)
    {
        SNMPTrace ("SNMP:Unable to Delete the SNMP Agent Task\n");
        return;
    }
    /* close the agent socket Descriptor */
    close (gi4Socket);
    close (gi4ProxySocket);

    /* Memset the Sysor Table */
    MEMSET (gaSnmpSysorTable, SNMP_ZERO, sizeof (gaSnmpSysorTable));
    for (u4Count = SNMP_ZERO; u4Count < MAX_SYSOR_TABLE_ENTRY; u4Count++)
    {
        gaSnmpSysorTable[u4Count].i4Status = SNMP_INACTIVE;
    }
}

/************************************************************************
 *  Function Name   : ip_src_addr_to_use_for_dest 
 *  Description     : Gets src the IP address for particular destination
 *  Input           : u4DestAddr,pu4SrcAddr 
 *  Output          : None 
 *  Returns         : None 
 ************************************************************************/

#ifndef IP_WANTED
INT4
ip_src_addr_to_use_for_dest (UINT4 u4DestAddr, UINT4 *pu4SrcAddr)
{
    UNUSED_PARAM (u4DestAddr);
    UNUSED_PARAM (pu4SrcAddr);
    return SNMP_SUCCESS;
}
#endif
