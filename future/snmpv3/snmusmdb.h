/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: snmusmdb.h,v 1.4 2015/04/28 12:35:03 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _SNMUSMDB_H
#define _SNMUSMDB_H

UINT1 UsmUserTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM};

UINT4 snmusm [] ={1,3,6,1,6,3,15};
tSNMP_OID_TYPE snmusmOID = {7, snmusm};


UINT4 UsmStatsUnsupportedSecLevels [ ] ={1,3,6,1,6,3,15,1,1,1};
UINT4 UsmStatsNotInTimeWindows [ ] ={1,3,6,1,6,3,15,1,1,2};
UINT4 UsmStatsUnknownUserNames [ ] ={1,3,6,1,6,3,15,1,1,3};
UINT4 UsmStatsUnknownEngineIDs [ ] ={1,3,6,1,6,3,15,1,1,4};
UINT4 UsmStatsWrongDigests [ ] ={1,3,6,1,6,3,15,1,1,5};
UINT4 UsmStatsDecryptionErrors [ ] ={1,3,6,1,6,3,15,1,1,6};
UINT4 UsmUserSpinLock [ ] ={1,3,6,1,6,3,15,1,2,1};
UINT4 UsmUserEngineID [ ] ={1,3,6,1,6,3,15,1,2,2,1,1};
UINT4 UsmUserName [ ] ={1,3,6,1,6,3,15,1,2,2,1,2};
UINT4 UsmUserSecurityName [ ] ={1,3,6,1,6,3,15,1,2,2,1,3};
UINT4 UsmUserCloneFrom [ ] ={1,3,6,1,6,3,15,1,2,2,1,4};
UINT4 UsmUserAuthProtocol [ ] ={1,3,6,1,6,3,15,1,2,2,1,5};
UINT4 UsmUserAuthKeyChange [ ] ={1,3,6,1,6,3,15,1,2,2,1,6};
UINT4 UsmUserOwnAuthKeyChange [ ] ={1,3,6,1,6,3,15,1,2,2,1,7};
UINT4 UsmUserPrivProtocol [ ] ={1,3,6,1,6,3,15,1,2,2,1,8};
UINT4 UsmUserPrivKeyChange [ ] ={1,3,6,1,6,3,15,1,2,2,1,9};
UINT4 UsmUserOwnPrivKeyChange [ ] ={1,3,6,1,6,3,15,1,2,2,1,10};
UINT4 UsmUserPublic [ ] ={1,3,6,1,6,3,15,1,2,2,1,11};
UINT4 UsmUserStorageType [ ] ={1,3,6,1,6,3,15,1,2,2,1,12};
UINT4 UsmUserStatus [ ] ={1,3,6,1,6,3,15,1,2,2,1,13};


tMbDbEntry snmusmMibEntry[]= {

{{10,UsmStatsUnsupportedSecLevels}, NULL, UsmStatsUnsupportedSecLevelsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,UsmStatsNotInTimeWindows}, NULL, UsmStatsNotInTimeWindowsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,UsmStatsUnknownUserNames}, NULL, UsmStatsUnknownUserNamesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,UsmStatsUnknownEngineIDs}, NULL, UsmStatsUnknownEngineIDsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,UsmStatsWrongDigests}, NULL, UsmStatsWrongDigestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,UsmStatsDecryptionErrors}, NULL, UsmStatsDecryptionErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,UsmUserSpinLock}, NULL, UsmUserSpinLockGet, UsmUserSpinLockSet, UsmUserSpinLockTest, UsmUserSpinLockDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,UsmUserEngineID}, GetNextIndexUsmUserTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, UsmUserTableINDEX, 2, 0, 0, NULL},

{{12,UsmUserName}, GetNextIndexUsmUserTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, UsmUserTableINDEX, 2, 0, 0, NULL},

{{12,UsmUserSecurityName}, GetNextIndexUsmUserTable, UsmUserSecurityNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, UsmUserTableINDEX, 2, 0, 0, NULL},

{{12,UsmUserCloneFrom}, GetNextIndexUsmUserTable, UsmUserCloneFromGet, UsmUserCloneFromSet, UsmUserCloneFromTest, UsmUserTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, UsmUserTableINDEX, 2, 0, 0, NULL},

{{12,UsmUserAuthProtocol}, GetNextIndexUsmUserTable, UsmUserAuthProtocolGet, UsmUserAuthProtocolSet, UsmUserAuthProtocolTest, UsmUserTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, UsmUserTableINDEX, 2, 0, 0, "1,3,6,1,6,3,10,1,1,1"},

{{12,UsmUserAuthKeyChange}, GetNextIndexUsmUserTable, UsmUserAuthKeyChangeGet, UsmUserAuthKeyChangeSet, UsmUserAuthKeyChangeTest, UsmUserTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, UsmUserTableINDEX, 2, 0, 0, "0"},

{{12,UsmUserOwnAuthKeyChange}, GetNextIndexUsmUserTable, UsmUserOwnAuthKeyChangeGet, UsmUserOwnAuthKeyChangeSet, UsmUserOwnAuthKeyChangeTest, UsmUserTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, UsmUserTableINDEX, 2, 0, 0, "0"},

{{12,UsmUserPrivProtocol}, GetNextIndexUsmUserTable, UsmUserPrivProtocolGet, UsmUserPrivProtocolSet, UsmUserPrivProtocolTest, UsmUserTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, UsmUserTableINDEX, 2, 0, 0, "1,3,6,1,6,3,10,1,2,1"},

{{12,UsmUserPrivKeyChange}, GetNextIndexUsmUserTable, UsmUserPrivKeyChangeGet, UsmUserPrivKeyChangeSet, UsmUserPrivKeyChangeTest, UsmUserTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, UsmUserTableINDEX, 2, 0, 0, "0"},

{{12,UsmUserOwnPrivKeyChange}, GetNextIndexUsmUserTable, UsmUserOwnPrivKeyChangeGet, UsmUserOwnPrivKeyChangeSet, UsmUserOwnPrivKeyChangeTest, UsmUserTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, UsmUserTableINDEX, 2, 0, 0, "0"},

{{12,UsmUserPublic}, GetNextIndexUsmUserTable, UsmUserPublicGet, UsmUserPublicSet, UsmUserPublicTest, UsmUserTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, UsmUserTableINDEX, 2, 0, 0, "0"},

{{12,UsmUserStorageType}, GetNextIndexUsmUserTable, UsmUserStorageTypeGet, UsmUserStorageTypeSet, UsmUserStorageTypeTest, UsmUserTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, UsmUserTableINDEX, 2, 0, 0, "3"},

{{12,UsmUserStatus}, GetNextIndexUsmUserTable, UsmUserStatusGet, UsmUserStatusSet, UsmUserStatusTest, UsmUserTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, UsmUserTableINDEX, 2, 0, 1, NULL},
};
tMibData snmusmEntry = { 20, snmusmMibEntry };
#endif /* _SNMUSMDB_H */

