/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: snmmpdwr.h,v 1.4 2015/04/28 12:35:02 siva Exp $
*
* Description: Proto types for snmp stats wrapper  Routines
*********************************************************************/

#ifndef _SNMMPDWR_H
#define _SNMMPDWR_H

VOID RegisterSNMMPD(VOID);

VOID UnRegisterSNMMPD(VOID);
INT4 SnmpUnknownSecurityModelsGet(tSnmpIndex *, tRetVal *);
INT4 SnmpInvalidMsgsGet(tSnmpIndex *, tRetVal *);
INT4 SnmpUnknownPDUHandlersGet(tSnmpIndex *, tRetVal *);
INT4 SnmpEngineIDGet(tSnmpIndex *, tRetVal *);
INT4 SnmpEngineBootsGet(tSnmpIndex *, tRetVal *);
INT4 SnmpEngineTimeGet(tSnmpIndex *, tRetVal *);
INT4 SnmpEngineMaxMessageSizeGet(tSnmpIndex *, tRetVal *);
#endif /* _SNMMPDWR_H */
