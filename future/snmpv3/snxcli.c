/*****************************************************************************
 * $Id: snxcli.c,v 1.7 2016/04/06 10:21:46 siva Exp $
 *
 * Description: This file contains CLI SET/GET/TEST and GETNEXT
 *              routines for the Agentx MIB objects specified in fssnmp3.mib
 ****************************************************************************/
#ifndef __SNXAGTXCLI_C__
#define __SNXAGTXCLI_C__
#include "snxinc.h"
#include "snmp3cli.h"
#include "fssocket.h"
#include "fssnmp3wr.h"
#include "fssnmp3lw.h"
#include "fssnmpcli.h"

/****************************************************************************
 *
 *    FUNCTION NAME    :  cli_process_snx_cmd
 *
 *    DESCRIPTION      :  Protocol CLI message handler function
 *
 *    INPUT            :  CliHandle - CliContext ID                           
 *                        u4Command - Command identifier                      
 *                        ... -Variable command argument list
 *
 *    OUTPUT           :  None
 *
 *    RETURNS          :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/

INT4
cli_process_snx_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *pau1args[CLI_SNX_MAX_ARGS];
    UINT1              *pIp6Addr = NULL;
    INT1                i1argno = 0;
    INT4                i4RetStatus = CLI_FAILURE;
    UINT4               u4ErrCode = 0;
    UINT4               u4Ip4Addr = 0;
    tSNMP_OCTET_STRING_TYPE MasterIpAddr;
    tSNMP_OCTET_STRING_TYPE ContextName;

    va_start (ap, u4Command);

    /* Walk through the rest of the arguments and store in pau1args array.
     * Store 16 arguments at the max. This is because vlan commands do not
     * take more than sixteen inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give
     * second input. In that case first arg will be null and second arg only
     * has value */
    while (1)
    {
        pau1args[i1argno++] = va_arg (ap, UINT1 *);
        if (i1argno == CLI_SNX_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);

    MEMSET (&MasterIpAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    switch (u4Command)
    {
        case CLI_SNX_SUBAGENT_ENABLE:
            if (SnxUtlAllocOctetString (&MasterIpAddr) == NULL)
            {
                CliPrintf (CliHandle,
                           "\r\n%% Failed allocating memory for MasterAddress \r\n");
                i4RetStatus = CLI_FAILURE;
                break;
            }
            if (SnxUtlAllocOctetString (&ContextName) == NULL)
            {
                CliPrintf (CliHandle,
                           "\r\n%% Failed allocating memory for Context Name \r\n");
                SnxUtlFreeOctetString (&MasterIpAddr, OSIX_FALSE);
                i4RetStatus = CLI_FAILURE;
                break;
            }

            if (CLI_PTR_TO_U4 (pau1args[3]) == CLI_SNX_IP6)
            {
                pIp6Addr = (UINT1 *) str_to_ip6addr (pau1args[1]);

                if (pIp6Addr == NULL)
                {
                    CliPrintf (CliHandle,
                               "\r\n%% Failed in getting MasterAddress \r\n");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }

                MEMCPY (MasterIpAddr.pu1_OctetList, pIp6Addr, SNX_IP6_LEN);
                MasterIpAddr.i4_Length = SNX_IP6_LEN;
            }
            else if (CLI_PTR_TO_U4 (pau1args[3]) == CLI_SNX_IP4)
            {
                u4Ip4Addr = *(UINT4 *) (VOID *) pau1args[1];
                MEMCPY (MasterIpAddr.pu1_OctetList, &u4Ip4Addr, SNX_IP4_LEN);
                MasterIpAddr.i4_Length = SNX_IP4_LEN;
            }

            if (pau1args[4] != NULL)
            {
                MEMCPY (ContextName.pu1_OctetList, pau1args[4],
                        STRLEN (pau1args[4]));
                ContextName.i4_Length = (INT4) STRLEN (pau1args[4]);
            }
            else
            {
                ContextName.i4_Length = 0;
            }
            i4RetStatus =
                SnxCliActivateAgentxSubagent (CliHandle, &MasterIpAddr,
                                              CLI_PTR_TO_U4 (pau1args[2]),
                                              &ContextName);
            SnxUtlFreeOctetString (&ContextName, OSIX_FALSE);
            SnxUtlFreeOctetString (&MasterIpAddr, OSIX_FALSE);
            break;
        case CLI_SNX_SUBAGENT_DISABLE:
            i4RetStatus = SnxCliDeActivateAgentxSubagent (CliHandle);
            break;
        case CLI_SNX_SHOW_INFO:
            i4RetStatus = SnxCliShowGlobalInfo (CliHandle);
            break;
        case CLI_SNX_SHOW_STATISTICS:
            i4RetStatus = SnxCliShowStatistics (CliHandle);
            break;
        default:
            CliPrintf (CliHandle, "\r%% Invalid Command\r\n");
            break;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_SNMP_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%% %s", SnxCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    return i4RetStatus;
}

/****************************************************************************
 *
 *    FUNCTION NAME    :  SnxCliActivateAgentxSubagent
 *
 *    DESCRIPTION      :  Enables Agentx SubAgent Capabilities
 *
 *    INPUT            :  MasterIP Address
 *                        Master Port Number
 *                        Ip address type( IPv4 or IPv6)
 *                        pContextName - Context name used during Registration
 *
 *    OUTPUT           :  None
 *
 *    RETURNS          :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT4
SnxCliActivateAgentxSubagent (tCliHandle CliHandle,
                              tSNMP_OCTET_STRING_TYPE * pMasterIpAddr,
                              UINT4 u4Port,
                              tSNMP_OCTET_STRING_TYPE * pContextName)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2SnmpAgentxMasterAgentAddr (&u4ErrorCode, pMasterIpAddr) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetSnmpAgentxMasterAgentAddr (pMasterIpAddr) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (nmhTestv2SnmpAgentxMasterAgentPortNo (&u4ErrorCode, u4Port) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetSnmpAgentxMasterAgentPortNo (u4Port) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2SnmpAgentxContextName (&u4ErrorCode,
                                        pContextName) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetSnmpAgentxContextName (pContextName) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (nmhTestv2SnmpAgentxSubAgentControl (&u4ErrorCode,
                                            SNMP_AGENTX_ENABLE) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetSnmpAgentxSubAgentControl (SNMP_AGENTX_ENABLE) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    :  SnxCliDeActivateAgentxSubagent
 *
 *    DESCRIPTION      :  Disables Agentx SubAgent Capabilities
 *
 *    INPUT            :  None
 *
 *    OUTPUT           :  None
 *
 *    RETURNS          :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT4
SnxCliDeActivateAgentxSubagent (tCliHandle CliHandle)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2SnmpAgentxSubAgentControl (&u4ErrorCode,
                                            SNMP_AGENTX_DISABLE)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetSnmpAgentxSubAgentControl (SNMP_AGENTX_DISABLE) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    :  SnxCliShowGlobalInfo
 *
 *    DESCRIPTION      :  Displays Agentx SubAgent Global Information
 *    
 *    INPUT            :  CliHandle - CliContext ID
 *
 *    OUTPUT           :  None
 *
 *    RETURNS          :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT4
SnxCliShowGlobalInfo (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE MasterIpAddr;
    tSNMP_OCTET_STRING_TYPE ContextName;
    INT4                i4SnmpAgentxStatus = 0;
    INT4                i4AgtxTransDomain = 0;
    UINT4               u4PortNo = 0;
    CHR1               *pc1IpAddress = NULL;
    UINT1               au1IpAddress[INET_ADDRSTRLEN];
    UINT4               u4IpAddress = 0;
    tUtlIn6Addr         In6Addr;

    MEMSET (&MasterIpAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&ContextName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&In6Addr, 0, sizeof (tUtlIn6Addr));
    MEMSET (&au1IpAddress, 0, INET_ADDRSTRLEN);

    nmhGetSnmpAgentxSubAgentControl (&i4SnmpAgentxStatus);

    if (i4SnmpAgentxStatus == SNMP_AGENTX_ENABLE)
    {
        CliPrintf (CliHandle, "%s\r\n", "Agentx Subagent is enabled");

        nmhGetSnmpAgentxTransportDomain (&i4AgtxTransDomain);

        if (i4AgtxTransDomain == SNX_TDOMAIN_TCP)
        {
            CliPrintf (CliHandle, "%-18s:%-7s\n", "TransportDomain", "TCP");

            if (SnxUtlAllocOctetString (&MasterIpAddr) == NULL)
            {
                return CLI_FAILURE;
            }
            if (SnxUtlAllocOctetString (&ContextName) == NULL)
            {
                SnxUtlFreeOctetString (&MasterIpAddr, OSIX_FALSE);
                return CLI_FAILURE;
            }
            nmhGetSnmpAgentxMasterAgentAddr (&MasterIpAddr);
            if (MasterIpAddr.i4_Length == SNX_IP4_LEN)
            {
                u4IpAddress =
                    OSIX_NTOHL (*(UINT4 *) (VOID *) MasterIpAddr.pu1_OctetList);

                INET_NTOP (AF_INET, &u4IpAddress,
                           au1IpAddress, INET_ADDRSTRLEN);
                CliPrintf (CliHandle, "%-18s:%-20s\n",
                           "Master IP Address", au1IpAddress);
            }
            else if (MasterIpAddr.i4_Length == SNX_IP6_LEN)
            {
                MEMCPY (In6Addr.u1addr, MasterIpAddr.pu1_OctetList,
                        SNX_IP6_LEN);
                pc1IpAddress = INET_NTOA6 (In6Addr);
                CliPrintf (CliHandle, "%-18s:%-20s\n",
                           "Master IP Address", pc1IpAddress);
            }
            SnxUtlFreeOctetString (&MasterIpAddr, OSIX_FALSE);

            nmhGetSnmpAgentxMasterAgentPortNo (&u4PortNo);
            CliPrintf (CliHandle, "%-18s:%-8d\n", "Master PortNo", u4PortNo);

            nmhGetSnmpAgentxContextName (&ContextName);
            CliPrintf (CliHandle, "Context name : %s\n",
                       ContextName.pu1_OctetList);
            SnxUtlFreeOctetString (&ContextName, OSIX_FALSE);
        }
    }
    else
    {
        CliPrintf (CliHandle, "\r%% Snmp Agentx Subagent Disabled\r\n");
    }
    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    :  SnxCliShowStatistics
 *
 *    DESCRIPTION      :  Displays Agentx SubAgent Statistics
 *    
 *    INPUT            :  CliHandle - CliContext ID
 *
 *    OUTPUT           :  None
 *
 *    RETURNS          :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT4
SnxCliShowStatistics (tCliHandle CliHandle)
{
    UINT4               u4AgtxSubAgentPkts = 0;
    INT4                i4SnmpAgentxStatus = 0;

    nmhGetSnmpAgentxSubAgentControl (&i4SnmpAgentxStatus);
    if (i4SnmpAgentxStatus == SNMP_AGENTX_ENABLE)
    {
        CliPrintf (CliHandle, "\nTx Statistics\n");

        nmhGetSnmpAgentxSubAgentOutPkts (&u4AgtxSubAgentPkts);
        CliPrintf (CliHandle, "  %-34s:%-8d\r\n", "Transmitted Packets",
                   u4AgtxSubAgentPkts);

        nmhGetSnmpAgentxSubAgentOpenPktCnt (&u4AgtxSubAgentPkts);
        CliPrintf (CliHandle, "  %-34s:%-8d\r\n", "Open PDU",
                   u4AgtxSubAgentPkts);

        nmhGetSnmpAgentxSubAgentIdAllocPktCnt (&u4AgtxSubAgentPkts);
        CliPrintf (CliHandle, "  %-34s:%-8d\r\n", "Index Allocate PDU",
                   u4AgtxSubAgentPkts);

        nmhGetSnmpAgentxSubAgentIdDllocPktCnt (&u4AgtxSubAgentPkts);
        CliPrintf (CliHandle, "  %-34s:%-8d\r\n", "Index DeAllocate PDU",
                   u4AgtxSubAgentPkts);

        nmhGetSnmpAgentxSubAgentRegPktCnt (&u4AgtxSubAgentPkts);
        CliPrintf (CliHandle, "  %-34s:%-8d\r\n", "Register PDU",
                   u4AgtxSubAgentPkts);

        nmhGetSnmpAgentxSubAgentAddCapsCnt (&u4AgtxSubAgentPkts);
        CliPrintf (CliHandle, "  %-34s:%-8d\r\n", "Add Agent Capabilities PDU",
                   u4AgtxSubAgentPkts);

        nmhGetSnmpAgentxSubAgentNotifyPktCnt (&u4AgtxSubAgentPkts);
        CliPrintf (CliHandle, "  %-34s:%-8d\r\n", "Notify PDU",
                   u4AgtxSubAgentPkts);

        nmhGetSnmpAgentxSubAgentPingCnt (&u4AgtxSubAgentPkts);
        CliPrintf (CliHandle, "  %-34s:%-8d\r\n", "Ping PDU",
                   u4AgtxSubAgentPkts);

        nmhGetSnmpAgentxSubAgentRemCapsCnt (&u4AgtxSubAgentPkts);
        CliPrintf (CliHandle, "  %-34s:%-8d\r\n",
                   "Remove Agent Capabilities PDU", u4AgtxSubAgentPkts);

        nmhGetSnmpAgentxSubAgentUnRegPktCnt (&u4AgtxSubAgentPkts);
        CliPrintf (CliHandle, "  %-34s:%-8d\r\n", "UnRegister PDU",
                   u4AgtxSubAgentPkts);

        nmhGetSnmpAgentxSubAgentOutClosePktCnt (&u4AgtxSubAgentPkts);
        CliPrintf (CliHandle, "  %-34s:%-8d\r\n", "Close PDU",
                   u4AgtxSubAgentPkts);

        nmhGetSnmpAgentxSubAgentOutResponse (&u4AgtxSubAgentPkts);
        CliPrintf (CliHandle, "  %-34s:%-8d\r\n", "Response PDU",
                   u4AgtxSubAgentPkts);

        CliPrintf (CliHandle, "\nRx Statistics\n");

        nmhGetSnmpAgentxSubAgentInPkts (&u4AgtxSubAgentPkts);
        CliPrintf (CliHandle, "  %-34s:%-8d\r\n", "Rx Packets",
                   u4AgtxSubAgentPkts);

        nmhGetSnmpAgentxSubAgentInGets (&u4AgtxSubAgentPkts);
        CliPrintf (CliHandle, "  %-34s:%-8d\r\n", "Get PDU",
                   u4AgtxSubAgentPkts);

        nmhGetSnmpAgentxSubAgentInGetNexts (&u4AgtxSubAgentPkts);
        CliPrintf (CliHandle, "  %-34s:%-8d\r\n", "GetNext PDU",
                   u4AgtxSubAgentPkts);

        nmhGetSnmpAgentxSubAgentInGetBulks (&u4AgtxSubAgentPkts);
        CliPrintf (CliHandle, "  %-34s:%-8d\r\n", "GetBulk PDU",
                   u4AgtxSubAgentPkts);

        nmhGetSnmpAgentxSubAgentInTestSets (&u4AgtxSubAgentPkts);
        CliPrintf (CliHandle, "  %-34s:%-8d\r\n", "TestSet PDU",
                   u4AgtxSubAgentPkts);

        nmhGetSnmpAgentxSubAgentInCommits (&u4AgtxSubAgentPkts);
        CliPrintf (CliHandle, "  %-34s:%-8d\r\n", "Commit PDU",
                   u4AgtxSubAgentPkts);

        nmhGetSnmpAgentxSubAgentInCleanups (&u4AgtxSubAgentPkts);
        CliPrintf (CliHandle, "  %-34s:%-8d\r\n", "Cleanup PDU",
                   u4AgtxSubAgentPkts);

        nmhGetSnmpAgentxSubAgentInUndos (&u4AgtxSubAgentPkts);
        CliPrintf (CliHandle, "  %-34s:%-8d\r\n", "Undo PDU",
                   u4AgtxSubAgentPkts);

        nmhGetSnmpAgentxSubAgentPktDrops (&u4AgtxSubAgentPkts);
        CliPrintf (CliHandle, "  %-34s:%-8d\r\n", "Dropped Packets",
                   u4AgtxSubAgentPkts);

        nmhGetSnmpAgentxSubAgentParseDrops (&u4AgtxSubAgentPkts);
        CliPrintf (CliHandle, "  %-34s:%-8d\r\n", "Parse Drop Errors",
                   u4AgtxSubAgentPkts);

        nmhGetSnmpAgentxSubAgentInOpenFail (&u4AgtxSubAgentPkts);
        CliPrintf (CliHandle, "  %-34s:%-8d\r\n", "Open Fail Errors",
                   u4AgtxSubAgentPkts);

        nmhGetSnmpAgentxSubAgentInClosePktCnt (&u4AgtxSubAgentPkts);
        CliPrintf (CliHandle, "  %-34s:%-8d\r\n", "Close PDU",
                   u4AgtxSubAgentPkts);

        nmhGetSnmpAgentxSubAgentInResponse (&u4AgtxSubAgentPkts);
        CliPrintf (CliHandle, "  %-34s:%-8d\r\n", "Response PDU",
                   u4AgtxSubAgentPkts);
    }
    else
    {
        CliPrintf (CliHandle, "\r%% Snmp Agentx Subagent Disabled\r\n");
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    :  SnxShowRunningConfig
 *
 *    DESCRIPTION      :  Displays the current configurations of Agnetx
 *                        subagent
 *    
 *    INPUT            :  CliHandle - CliContext ID
 *
 *    OUTPUT           :  None
 *
 *    RETURNS          :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT4
SnxCliShowRunningConfig (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE MasterIpAddr;
    tSNMP_OCTET_STRING_TYPE ContextName;
    INT4                i4SnmpAgentxStatus = 0;
    UINT4               u4PortNo;
    CHR1               *pc1IpAddress = NULL;
    UINT1               au1IpAddress[INET_ADDRSTRLEN];
    UINT4               u4IpAddress = 0;
    INT4                i4AgtxTransDomain = 0;
    tUtlIn6Addr         In6Addr;

    MEMSET (&MasterIpAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&ContextName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&In6Addr, 0, sizeof (tUtlIn6Addr));
    MEMSET (&au1IpAddress, 0, INET_ADDRSTRLEN);

    nmhGetSnmpAgentxSubAgentControl (&i4SnmpAgentxStatus);
    if (i4SnmpAgentxStatus != SNMP_AGENTX_DISABLE)
    {
        if (SnxUtlAllocOctetString (&MasterIpAddr) == NULL)
        {
            return CLI_FAILURE;
        }
        if (SnxUtlAllocOctetString (&ContextName) == NULL)
        {
            SnxUtlFreeOctetString (&MasterIpAddr, OSIX_FALSE);
            return CLI_FAILURE;
        }

        CliPrintf (CliHandle, "enable snmpsubagent ");
        nmhGetSnmpAgentxTransportDomain (&i4AgtxTransDomain);
        if (i4AgtxTransDomain == SNX_TDOMAIN_TCP)
        {
            nmhGetSnmpAgentxMasterAgentAddr (&MasterIpAddr);
            if (MasterIpAddr.i4_Length == SNX_IP4_LEN)
            {

                u4IpAddress =
                    OSIX_NTOHL (*(UINT4 *) (VOID *) MasterIpAddr.pu1_OctetList);

                INET_NTOP (AF_INET, &u4IpAddress,
                           au1IpAddress, INET_ADDRSTRLEN);
                CliPrintf (CliHandle, "master ip4 %s ", au1IpAddress);
            }
            else if (MasterIpAddr.i4_Length == SNX_IP6_LEN)
            {
                MEMCPY (In6Addr.u1addr, MasterIpAddr.pu1_OctetList,
                        SNX_IP6_LEN);
                pc1IpAddress = INET_NTOA6 (In6Addr);
                CliPrintf (CliHandle, "master ip6 %s ", pc1IpAddress);
            }
            nmhGetSnmpAgentxMasterAgentPortNo (&u4PortNo);
            if (u4PortNo != CLI_SNX_DEFAULT_MASTER_PORT)
            {
                CliPrintf (CliHandle, "port %d", u4PortNo);
            }
            nmhGetSnmpAgentxContextName (&ContextName);
            if (ContextName.i4_Length != 0)
            {
                CliPrintf (CliHandle, "context %s", ContextName.pu1_OctetList);
            }
            SnxUtlFreeOctetString (&MasterIpAddr, OSIX_FALSE);
            SnxUtlFreeOctetString (&ContextName, OSIX_FALSE);
        }
    }
    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

#endif /* __SNXAGTXCLI_C__ */
/***************************  End of snxcli.c ********************************/
