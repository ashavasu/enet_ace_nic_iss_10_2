/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: snmpusm.c,v 1.18 2017/11/20 13:11:26 siva Exp $
 *
 * Description: routines for User Based Security Model 
 *******************************************************************/
#include "snmpcmn.h"
#include "snmpusm.h"
#include "snmusmwr.h"
#include "snmusmlw.h"
#include "cryartdf.h"
#include "stdsnucli.h"
#include "shaarinc.h"
#include "arHmac_api.h"
#include   "arMD5_api.h"
#include "utilalgo.h"
#include "utilrand.h"
extern UINT1 DesArKeyScheduler PROTO ((const UINT1 *, unArCryptoKey *));
extern UINT1 TDesArKeySchedule PROTO ((const UINT1 *, unArCryptoKey *));
static UINT1        au1PreIV[SNMP_AES_OUT_BUFF_LEN] = { SNMP_ZERO };

/*********************************************************************
*  Function Name : SNMPInitUsmTable
*  Description   : Function Init User Based Security Model Table
*  Parameter(s)  : None
*  Return Values : None
*********************************************************************/
VOID
SNMPInitUsmTable ()
{
    UINT4               u4Index = SNMP_ZERO;
    MEMSET (gSnmpUsmTable, SNMP_ZERO, sizeof (tSnmpUsmEntry));
    MEMSET (gSnmpUsmTable, SNMP_ZERO, sizeof (gSnmpUsmTable));
    MEMSET (au1UsmEngineID, SNMP_ZERO, sizeof (au1UsmEngineID));
    MEMSET (au1UsmSecName, SNMP_ZERO, sizeof (au1UsmSecName));
    MEMSET (au1UsmAuthKeyChange, SNMP_ZERO, sizeof (au1UsmAuthKeyChange));
    MEMSET (au1UsmOwnAuthKeyChange, SNMP_ZERO, sizeof (au1UsmOwnAuthKeyChange));
    MEMSET (au1UsmPrivKeyChange, SNMP_ZERO, sizeof (au1UsmPrivKeyChange));
    MEMSET (au1UsmOwnPrivKeyChange, SNMP_ZERO, sizeof (au1UsmOwnPrivKeyChange));
    MEMSET (au1UsmUserAuthPassword, SNMP_ZERO, sizeof (au1UsmUserAuthPassword));
    MEMSET (au1UsmUserPrivPassword, SNMP_ZERO, sizeof (au1UsmUserPrivPassword));
    MEMSET (au1UsmPublic, SNMP_ZERO, sizeof (au1UsmPublic));

    TMO_SLL_Init (&gSnmpUsmSll);
    for (u4Index = SNMP_ZERO; u4Index < SNMP_MAX_USM_ENTRY; u4Index++)
    {
        gSnmpUsmTable[u4Index].UsmUserEngineID.pu1_OctetList =
            au1UsmEngineID[u4Index];
        gSnmpUsmTable[u4Index].UsmUserName.pu1_OctetList = au1UsmName[u4Index];
        gSnmpUsmTable[u4Index].UsmUserSecName.pu1_OctetList =
            au1UsmSecName[u4Index];
        gSnmpUsmTable[u4Index].UsmUserAuthKeyChange.pu1_OctetList =
            au1UsmAuthKeyChange[u4Index];
        gSnmpUsmTable[u4Index].UsmUserOwnAuthKeyChange.pu1_OctetList =
            au1UsmOwnAuthKeyChange[u4Index];
        gSnmpUsmTable[u4Index].UsmUserPrivKeyChange.pu1_OctetList =
            au1UsmPrivKeyChange[u4Index];
        gSnmpUsmTable[u4Index].UsmUserOwnPrivKeyChange.pu1_OctetList =
            au1UsmOwnPrivKeyChange[u4Index];
        gSnmpUsmTable[u4Index].UsmUserAuthPassword.pu1_OctetList =
            au1UsmUserAuthPassword[u4Index];
        gSnmpUsmTable[u4Index].UsmUserPrivPassword.pu1_OctetList =
            au1UsmUserPrivPassword[u4Index];
        gSnmpUsmTable[u4Index].UsmUserPublic.pu1_OctetList =
            au1UsmPublic[u4Index];
        gSnmpUsmTable[u4Index].u4UsmUserStatus = SNMP_INACTIVE;
    }

}

/*********************************************************************
*  Function Name : SNMPCreateUsmEntry
*  Description   : Function to create USM Entry in the USM Table
*  Parameter(s)  : pSnmpEngineID - Snmp Engine ID
*                  pSnmpUserName - Snmp User Name
*  Return Values : If Success returns UsmEntry Pointer otherwise NULL 
*********************************************************************/
tSnmpUsmEntry      *
SNMPCreateUsmEntry (tSNMP_OCTET_STRING_TYPE * pSnmpEngineID,
                    tSNMP_OCTET_STRING_TYPE * pSnmpUserName)
{
    UINT4               u4Index = SNMP_ZERO;
    UINT1               au1AuditLogMsg[AUDIT_LOG_SIZE];

    MEMSET (au1AuditLogMsg, 0, AUDIT_LOG_SIZE);
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function entry\r\n", __func__);

    if (SNMPGetUsmEntry (pSnmpEngineID, pSnmpUserName) != NULL)
    {
        return NULL;
    }
    if (pSnmpUserName->i4_Length == SNMP_ZERO)
    {
        return NULL;
    }
    for (u4Index = SNMP_ZERO; u4Index < SNMP_MAX_USM_ENTRY; u4Index++)
    {
        if (gSnmpUsmTable[u4Index].u4UsmUserStatus == SNMP_INACTIVE)
        {
            gSnmpUsmTable[u4Index].u4UsmUserStatus = UNDER_CREATION;
            SNMPCopyOctetString (&(gSnmpUsmTable[u4Index].UsmUserEngineID),
                                 pSnmpEngineID);
            SNMPCopyOctetString (&(gSnmpUsmTable[u4Index].UsmUserName),
                                 pSnmpUserName);
            SNMPCopyOctetString (&(gSnmpUsmTable[u4Index].UsmUserSecName),
                                 pSnmpUserName);
            gSnmpUsmTable[u4Index].u4UsmUserPrivProtocol = SNMP_NO_PRIV;
            gSnmpUsmTable[u4Index].u4UsmUserAuthProtocol = SNMP_NO_AUTH;
            gSnmpUsmTable[u4Index].UsmUserAuthKeyChange.i4_Length = SNMP_ZERO;
            gSnmpUsmTable[u4Index].UsmUserOwnAuthKeyChange.i4_Length =
                SNMP_ZERO;
            gSnmpUsmTable[u4Index].UsmUserPrivKeyChange.i4_Length = SNMP_ZERO;
            gSnmpUsmTable[u4Index].UsmUserOwnPrivKeyChange.i4_Length =
                SNMP_ZERO;
            gSnmpUsmTable[u4Index].UsmUserPublic.i4_Length = SNMP_ZERO;
            gSnmpUsmTable[u4Index].u4UsmUserCloneFrom = SNMP_ZERO;
            gSnmpUsmTable[u4Index].u4UsmUserStorageType =
                SNMP3_STORAGE_TYPE_NONVOLATILE;
            SNMPAddUsmSll (&(gSnmpUsmTable[u4Index].Link));
            SNMP_TRC1 (SNMP_DEBUG_TRC, "SNMPCreateUsmEntry: "
                       "Successfully added the user entry for [%s]\n",
                       pSnmpUserName->pu1_OctetList);
            if (ISS_IS_AUDIT_ENABLED () == ISS_TRUE)
            {
                /*Send log to Audit log file */
                SPRINTF ((CHR1 *) au1AuditLogMsg,
                         "New User [%s] Creation SUCCESS",
                         (CHR1 *) pSnmpUserName->pu1_OctetList);
                MsrAuditSendLogInfo (au1AuditLogMsg, AUDIT_INFO_LEVEL);
            }
            return (&(gSnmpUsmTable[u4Index]));
        }
    }
    SNMP_TRC1 (SNMP_FAILURE_TRC, "SNMPCreateUsmEntry: "
               "User creation failed for [%s]\n", pSnmpUserName->pu1_OctetList);
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function exit\r\n", __func__);
    if (ISS_IS_AUDIT_ENABLED () == ISS_TRUE)
    {
        /*Send log to Audit log file */
        SPRINTF ((CHR1 *) au1AuditLogMsg, "New User [%s] Creation FAIL",
                 (CHR1 *) pSnmpUserName->pu1_OctetList);
        MsrAuditSendLogInfo (au1AuditLogMsg, AUDIT_INFO_LEVEL);
    }
    return NULL;
}

/*********************************************************************
*  Function Name : SNMPGetUsmEntry
*  Description   : Function get a Usm Entry from Usm Table 
*  Parameter(s)  : pSnmpEngineID - Snmp Engine Id
*                  pSnmpUserName - Snmp User Name
*  Return Values : If entry exists then returns Usm Entry Pointer 
*                  otherwise returns NULL
*********************************************************************/
tSnmpUsmEntry      *
SNMPGetUsmEntry (tSNMP_OCTET_STRING_TYPE * pSnmpEngineID,
                 tSNMP_OCTET_STRING_TYPE * pSnmpUserName)
{
    tTMO_SLL_NODE      *pLstNode = NULL;
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    TMO_SLL_Scan (&gSnmpUsmSll, pLstNode, tTMO_SLL_NODE *)
    {
        pSnmpUsmEntry = (tSnmpUsmEntry *) pLstNode;
        if ((SNMPCompareOctetString (pSnmpEngineID,
                                     &(pSnmpUsmEntry->UsmUserEngineID))
             == SNMP_EQUAL) &&
            (SNMPCompareOctetString (pSnmpUserName,
                                     &(pSnmpUsmEntry->UsmUserName))
             == SNMP_EQUAL))
        {
            return pSnmpUsmEntry;
        }

    }
    return NULL;
}

/*********************************************************************
*  Function Name : SNMPDeleteUsmEntry
*  Description   : Function to delete a Usm Entry
*  Parameter(s)  : pSnmpEngineID - Snmp Engine Id
*                  pSnmpUserName - Snmp User Name
*  Return Values : If Deleted successfully the return SNMP_SUCCESS
*                  otherwise returns SNMP_FAILURE
*********************************************************************/
INT4
SNMPDeleteUsmEntry (tSNMP_OCTET_STRING_TYPE * pSnmpEngineID,
                    tSNMP_OCTET_STRING_TYPE * pSnmpUserName)
{
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT1               au1AuditLogMsg[AUDIT_LOG_SIZE];

    MEMSET (au1AuditLogMsg, 0, AUDIT_LOG_SIZE);

    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function entry\r\n", __func__);
    TMO_SLL_Scan (&gSnmpUsmSll, pLstNode, tTMO_SLL_NODE *)
    {
        pSnmpUsmEntry = (tSnmpUsmEntry *) pLstNode;
        if ((SNMPCompareOctetString (pSnmpEngineID,
                                     &(pSnmpUsmEntry->UsmUserEngineID))
             == SNMP_EQUAL) &&
            (SNMPCompareOctetString (pSnmpUserName,
                                     &(pSnmpUsmEntry->UsmUserName))
             == SNMP_EQUAL))
        {
            pSnmpUsmEntry->u4UsmUserStatus = SNMP_INACTIVE;
            SNMPDelUsmSll (&(pSnmpUsmEntry->Link));
            if (ISS_IS_AUDIT_ENABLED () == ISS_TRUE)
            {
                /*Send log to Audit log file */
                SPRINTF ((CHR1 *) au1AuditLogMsg, "User [%s] Deletion SUCCESS",
                         (CHR1 *) pSnmpUserName->pu1_OctetList);
                MsrAuditSendLogInfo (au1AuditLogMsg, AUDIT_INFO_LEVEL);
            }
            SNMP_TRC1 (SNMP_DEBUG_TRC, "SNMPDeleteUsmEntry: "
                       " Successfully deleted the user entry for %s\n",
                       pSnmpUserName->pu1_OctetList);
            return SNMP_SUCCESS;
        }

    }
    if (ISS_IS_AUDIT_ENABLED () == ISS_TRUE)
    {
        /*Send log to Audit log file */
        SPRINTF ((CHR1 *) au1AuditLogMsg, "User [%s] Deletion FAIL",
                 (CHR1 *) pSnmpUserName->pu1_OctetList);
        MsrAuditSendLogInfo (au1AuditLogMsg, AUDIT_INFO_LEVEL);
    }
    SNMP_TRC1 (SNMP_FAILURE_TRC, "SNMPDeleteUsmEntry: "
               "User deletion failed for %s\n", pSnmpUserName->pu1_OctetList);
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function exit\r\n", __func__);
    return SNMP_FAILURE;
}

/*********************************************************************
*  Function Name : SNMPGetFirstUsmEntry
*  Description   : Function get the first Usm Entry from Usm Table
*  Parameter(s)  : None
*  Return Values : If Table has entry then returns First Usm Table 
*                  entry pointer
*********************************************************************/
tSnmpUsmEntry      *
SNMPGetFirstUsmEntry ()
{
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    pSnmpUsmEntry = (tSnmpUsmEntry *) TMO_SLL_First (&gSnmpUsmSll);
    return pSnmpUsmEntry;
}

/*********************************************************************
*  Function Name : SNMPGetNextUsmEntry
*  Description   : Function to get the next Usm Entry for the given
*                  Engine ID and User Name from the Usm Table.
*  Parameter(s)  : pSnmpEngineID - Snmp Engine ID
*                  pSnmpUserName - Snmp User Name
*  Return Values : If Usm has next entry for the given entry then this 
*                  will return pointer to the next entry otherwise
*                  returns NULL
*********************************************************************/
tSnmpUsmEntry      *
SNMPGetNextUsmEntry (tSNMP_OCTET_STRING_TYPE * pSnmpEngineID,
                     tSNMP_OCTET_STRING_TYPE * pSnmpUserName)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    INT4                i4Result;

    TMO_SLL_Scan (&gSnmpUsmSll, pListNode, tTMO_SLL_NODE *)
    {
        pSnmpUsmEntry = (tSnmpUsmEntry *) pListNode;

        i4Result = SNMPCompareOctetString
            (pSnmpEngineID, &(pSnmpUsmEntry->UsmUserEngineID));

        if (i4Result == SNMP_EQUAL)
        {
            if (SNMPCompareOctetString
                (pSnmpUserName, &(pSnmpUsmEntry->UsmUserName)) == SNMP_EQUAL)
            {
                return (tSnmpUsmEntry *) TMO_SLL_Next (&gSnmpUsmSll, pListNode);
            }
        }
    }
    return NULL;
}

/*********************************************************************
*  Function Name : SNMPAddUsmSll
*  Description   : Function to insert a Usm entry to the sll in 
*                  ascending order. This function is to make 
*                  snmp getnext operation support easy in usm table.
*  Parameter(s)  : pNode - Usm Node Pointer
*  Return Values : None
*********************************************************************/
VOID
SNMPAddUsmSll (tTMO_SLL_NODE * pNode)
{
    tSnmpUsmEntry      *pCurrEntry = NULL;
    tSnmpUsmEntry      *pInEntry = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pPrevLstNode = NULL;
    INT4                i4RetVal = 0;
    pInEntry = (tSnmpUsmEntry *) pNode;
    TMO_SLL_Scan (&gSnmpUsmSll, pLstNode, tTMO_SLL_NODE *)
    {
        pCurrEntry = (tSnmpUsmEntry *) pLstNode;

        i4RetVal = SNMPCompareOctetString
            (&(pInEntry->UsmUserEngineID), &(pCurrEntry->UsmUserEngineID));

        if (i4RetVal == SNMP_LESSER)
        {
            break;
        }
        else if (i4RetVal == SNMP_GREATER)
        {
            pPrevLstNode = pLstNode;
            continue;
        }

        if (SNMPCompareOctetString
            (&(pInEntry->UsmUserName), &(pCurrEntry->UsmUserName))
            == SNMP_LESSER)
        {
            break;
        }
        pPrevLstNode = pLstNode;
    }
    TMO_SLL_Insert (&gSnmpUsmSll, pPrevLstNode, pNode);
}

/*********************************************************************
*  Function Name : SNMPDelUsmSll
*  Description   : Remove a node form Usm sll list
*  Parameter(s)  : pNode - Node Pointer to be removed
*  Return Values : None
*********************************************************************/
VOID
SNMPDelUsmSll (tTMO_SLL_NODE * pNode)
{
    TMO_SLL_Delete (&gSnmpUsmSll, pNode);
    return;
}

/*********************************************************************
*  Function Name : SNMPKeyGenerator
*  Description   : Function to generate Key from user string. 
*                  This is from RFC 3414.
*  Parameter(s)  : pu1Passwd - User Name is ASCII string Format
*                  i4PasswdLen - User Password Length in Bytes
*                  u4Auth - Which Hmac Should be used for Key
*                  generation.  ie., MD5 or SHA
*                  pEngineID - Pointer to Engine Id used for Key
*                  generation
*                  pOctetString - Pointer to Octet string to store
*                  the generated Key.
*  Return Values :  SNMP_SUCCESS or SNMP_FAILURE
*********************************************************************/
INT4
SNMPKeyGenerator (UINT1 *pu1Passwd, INT4 i4PasswdLen, UINT4 u4Auth,
                  tSNMP_OCTET_STRING_TYPE * pEngineID,
                  tSNMP_OCTET_STRING_TYPE * pOctetString)
{
    UINT1               au1PasswdBuf[SNMP_KEY_MAX_PASSWD_LEN];
    UINT1               au1TempBuf[SNMP_MAX_PASSWD_LEN];
    UINT1              *pu1Key = NULL;
    UINT1               au1AuditLogMsg[AUDIT_LOG_SIZE];
    unUtilAlgo          UtilAlgo;

    MEMSET (au1PasswdBuf, 0, SNMP_KEY_MAX_PASSWD_LEN);
    MEMSET (au1TempBuf, 0, SNMP_MAX_PASSWD_LEN);
    pu1Key = pOctetString->pu1_OctetList;

    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function entry\r\n", __func__);
    if ((u4Auth == SNMP_MD5) && (i4PasswdLen != SNMP_ZERO))
    {
        /*The Function used to generate the Key from the configured Password */
        if (OSIX_SUCCESS != (SnmpUtilKeyGen (pu1Passwd, i4PasswdLen,
                                             u4Auth, pu1Key)))
        {
            SNMP_TRC2 (SNMP_FAILURE_TRC, "SNMPKeyGenerator: Failed to generate"
                       " key in SnmpUtilKeyGen: invalid auth [%d] or Pwd length"
                       " [%d]\r\n", u4Auth, i4PasswdLen);
            return SNMP_FAILURE;
        }
        MEMCPY (au1PasswdBuf, pu1Key, SNMP_AUTH_MD5_KEY_LEN);
        MEMCPY (au1PasswdBuf + SNMP_AUTH_MD5_KEY_LEN, pEngineID->pu1_OctetList,
                pEngineID->i4_Length);
        MEMCPY (au1PasswdBuf + SNMP_AUTH_MD5_KEY_LEN + pEngineID->i4_Length,
                pu1Key, SNMP_AUTH_MD5_KEY_LEN);
        MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));
        UtilAlgo.UtilMd5Algo.pu1Md5InBuf = (UINT1 *) au1PasswdBuf;
        UtilAlgo.UtilMd5Algo.u4Md5InBufLen =
            (SNMP_TWO * SNMP_AUTH_MD5_KEY_LEN) + ((UINT4) pEngineID->i4_Length);
        UtilAlgo.UtilMd5Algo.pu1Md5OutDigest = pu1Key;
        if (OSIX_SUCCESS != UtilMac (ISS_UTIL_ALGO_MD5_ALGO, &UtilAlgo))
        {
            SNMP_TRC2 (SNMP_FAILURE_TRC, "SNMPKeyGenerator: Failed to generate"
                       " key in UtilMac: invalid auth [%d] or Pwd length [%d]\r\n",
                       u4Auth, i4PasswdLen);
            return SNMP_FAILURE;
        }
        pOctetString->i4_Length = SNMP_AUTH_MD5_KEY_LEN;
        /*In case of AES 192/256 generated key should have 24/32 bytes respective
           so calculate hasing logic with final 20-byte output key and uppend
           20 bytes of final output with the previously stage output */
        MEMCPY (au1TempBuf, pu1Key, SNMP_AUTH_MD5_KEY_LEN);

        MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));
        UtilAlgo.UtilMd5Algo.pu1Md5InBuf = (UINT1 *) au1TempBuf;
        UtilAlgo.UtilMd5Algo.u4Md5InBufLen = SNMP_AUTH_MD5_KEY_LEN;
        UtilAlgo.UtilMd5Algo.pu1Md5OutDigest = au1TempBuf;
        if (OSIX_SUCCESS != UtilMac (ISS_UTIL_ALGO_MD5_ALGO, &UtilAlgo))
        {
            SNMP_TRC2 (SNMP_FAILURE_TRC, "SNMPKeyGenerator: "
                       "Failed to generate key in UtilMac: invalid auth [%d] or Pwd length [%d]\r\n",
                       u4Auth, i4PasswdLen);
            return SNMP_FAILURE;
        }
        MEMCPY (pu1Key + SNMP_AUTH_MD5_KEY_LEN, au1TempBuf,
                SNMP_AUTH_MD5_KEY_LEN);
        pOctetString->i4_Length = SNMP_AUTH_MD5_KEY_LEN;
    }
    else if ((u4Auth == SNMP_SHA) && (i4PasswdLen != SNMP_ZERO))
    {
        /*The Function used to generate the Key from the configured Password */
        if (OSIX_SUCCESS !=
            (SnmpUtilKeyGen (pu1Passwd, i4PasswdLen, u4Auth, pu1Key)))

        {
            SNMP_TRC2 (SNMP_FAILURE_TRC, "SNMPKeyGenerator: Failed to generate"
                       " key in SnmpUtilKeyGen: invalid auth [%d] or Pwd length"
                       " [%d]\r\n", u4Auth, i4PasswdLen);
            return SNMP_FAILURE;
        }
        MEMCPY (au1PasswdBuf, pu1Key, SNMP_AUTH_SHA_KEY_LEN);
        MEMCPY (au1PasswdBuf + SNMP_AUTH_SHA_KEY_LEN, pEngineID->pu1_OctetList,
                pEngineID->i4_Length);
        MEMCPY (au1PasswdBuf + SNMP_AUTH_SHA_KEY_LEN + pEngineID->i4_Length,
                pu1Key, SNMP_AUTH_SHA_KEY_LEN);

        MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));
        UtilAlgo.UtilSha1Algo.pu1Sha1Buf = (UINT1 *) au1PasswdBuf;
        UtilAlgo.UtilSha1Algo.u4Sha1BufLen =
            (SNMP_TWO * SNMP_AUTH_SHA_KEY_LEN) + ((UINT4) pEngineID->i4_Length);
        UtilAlgo.UtilSha1Algo.pu1Sha1Digest = pu1Key;
        if (OSIX_SUCCESS != UtilMac (ISS_UTIL_ALGO_SHA1_ALGO, &UtilAlgo))
        {
            SNMP_TRC2 (SNMP_FAILURE_TRC, "SNMPKeyGenerator: Failed to generate"
                       " key in UtilMac: invalid auth [%d] or Pwd length"
                       " [%d]\r\n", u4Auth, i4PasswdLen);
            return SNMP_FAILURE;
        }
        pOctetString->i4_Length = SNMP_AUTH_SHA_KEY_LEN;

        /*In case of AES 192/256 generated key should have 24/32 bytes respectively
           so calculate hasing logic with final 20-byte output key and uppend
           20 bytes of final output with the previously stage output */
        MEMCPY (au1TempBuf, pu1Key, SNMP_AUTH_SHA_KEY_LEN);
        MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));
        UtilAlgo.UtilSha1Algo.pu1Sha1Buf = (UINT1 *) au1TempBuf;
        UtilAlgo.UtilSha1Algo.u4Sha1BufLen = SNMP_AUTH_SHA_KEY_LEN;
        UtilAlgo.UtilSha1Algo.pu1Sha1Digest = au1TempBuf;
        if (OSIX_SUCCESS != UtilMac (ISS_UTIL_ALGO_SHA1_ALGO, &UtilAlgo))
        {
            SNMP_TRC2 (SNMP_FAILURE_TRC, "SNMPKeyGenerator: Failed to generate"
                       " key2 in UtilMac invalid auth [%d] or Pwd length [%d]\r\n",
                       u4Auth, i4PasswdLen);
            return SNMP_FAILURE;
        }
        MEMCPY (pu1Key + SNMP_AUTH_SHA_KEY_LEN, au1TempBuf,
                SNMP_AUTH_SHA_KEY_LEN);
        pOctetString->i4_Length = SNMP_AUTH_SHA_KEY_LEN;
    }
    else if ((u4Auth == SNMP_SHA256) && (i4PasswdLen != SNMP_ZERO))
    {
        /*The Function used to generate the Key from the configured Password */
        if (OSIX_SUCCESS !=
            (SnmpUtilKeyGen (pu1Passwd, i4PasswdLen, u4Auth, pu1Key)))
        {
            SNMP_TRC2 (SNMP_FAILURE_TRC, "SNMPKeyGenerator: Failed to generate"
                       " key in SnmpUtilKeyGen: invalid auth [%d] or Pwd length"
                       " [%d]\r\n", u4Auth, i4PasswdLen);
            return SNMP_FAILURE;

        }
        MEMCPY (au1PasswdBuf, pu1Key, SNMP_AUTH_SHA256_KEY_LEN);
        MEMCPY (au1PasswdBuf + SNMP_AUTH_SHA256_KEY_LEN,
                pEngineID->pu1_OctetList, pEngineID->i4_Length);
        MEMCPY (au1PasswdBuf + SNMP_AUTH_SHA256_KEY_LEN + pEngineID->i4_Length,
                pu1Key, SNMP_AUTH_SHA256_KEY_LEN);
        MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));
        UtilAlgo.UtilSha2Algo.pu1Sha2InBuf = au1PasswdBuf;
        UtilAlgo.UtilSha2Algo.i4Sha2BufLen =
            (INT4) ((SNMP_TWO * SNMP_AUTH_SHA256_KEY_LEN) +
                    ((UINT4) pEngineID->i4_Length));
        UtilAlgo.UtilSha2Algo.pu1Sha2MsgDigest = pu1Key;
        if (OSIX_SUCCESS != UtilMac (ISS_UTIL_ALGO_SHA256_ALGO, &UtilAlgo))
        {
            SNMP_TRC2 (SNMP_FAILURE_TRC, "SNMPKeyGenerator: Failed to generate"
                       " key in UtilMac: invalid auth [%d] or Pwd length [%d]\r\n",
                       u4Auth, i4PasswdLen);
            return SNMP_FAILURE;
        }
        pOctetString->i4_Length = SNMP_AUTH_SHA256_KEY_LEN;
    }
    else if ((u4Auth == SNMP_SHA384) && (i4PasswdLen != SNMP_ZERO))
    {
        /*The Function used to generate the Key from the configured Password */
        if (OSIX_SUCCESS !=
            (SnmpUtilKeyGen (pu1Passwd, i4PasswdLen, u4Auth, pu1Key)))
        {
            SNMP_TRC2 (SNMP_FAILURE_TRC, "SNMPKeyGenerator: Failed to generate"
                       " key in SnmpUtilKeyGen: invalid auth [%d] or Pwd length"
                       " [%d]\r\n", u4Auth, i4PasswdLen);
            return SNMP_FAILURE;
        }
        MEMCPY (au1PasswdBuf, pu1Key, SNMP_AUTH_SHA384_KEY_LEN);
        MEMCPY (au1PasswdBuf + SNMP_AUTH_SHA384_KEY_LEN,
                pEngineID->pu1_OctetList, pEngineID->i4_Length);
        MEMCPY (au1PasswdBuf + SNMP_AUTH_SHA384_KEY_LEN + pEngineID->i4_Length,
                pu1Key, SNMP_AUTH_SHA384_KEY_LEN);
        MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));
        UtilAlgo.UtilSha2Algo.pu1Sha2InBuf = au1PasswdBuf;
        UtilAlgo.UtilSha2Algo.i4Sha2BufLen =
            (INT4) ((SNMP_TWO * SNMP_AUTH_SHA384_KEY_LEN) +
                    ((UINT4) pEngineID->i4_Length));
        UtilAlgo.UtilSha2Algo.pu1Sha2MsgDigest = pu1Key;
        if (OSIX_SUCCESS != UtilMac (ISS_UTIL_ALGO_SHA384_ALGO, &UtilAlgo))
        {
            SNMP_TRC2 (SNMP_FAILURE_TRC, "SNMPKeyGenerator: Failed to generate"
                       " key in UtilMac: invalid auth [%d] or Pwd length [%d]\r\n",
                       u4Auth, i4PasswdLen);
            return SNMP_FAILURE;
        }
        pOctetString->i4_Length = SNMP_AUTH_SHA384_KEY_LEN;
    }
    else if ((u4Auth == SNMP_SHA512) && (i4PasswdLen != SNMP_ZERO))
    {
        /*The Function used to generate the Key from the configured Password */
        if (OSIX_SUCCESS !=
            (SnmpUtilKeyGen (pu1Passwd, i4PasswdLen, u4Auth, pu1Key)))
        {
            SNMP_TRC2 (SNMP_FAILURE_TRC, "SNMPKeyGenerator: Failed to generate"
                       " key in SnmpUtilKeyGen: invalid auth [%d] or Pwd length"
                       " [%d]\r\n", u4Auth, i4PasswdLen);
            return SNMP_FAILURE;

        }
        MEMCPY (au1PasswdBuf, pu1Key, SNMP_AUTH_SHA512_KEY_LEN);
        MEMCPY (au1PasswdBuf + SNMP_AUTH_SHA512_KEY_LEN,
                pEngineID->pu1_OctetList, pEngineID->i4_Length);
        MEMCPY (au1PasswdBuf + SNMP_AUTH_SHA512_KEY_LEN + pEngineID->i4_Length,
                pu1Key, SNMP_AUTH_SHA512_KEY_LEN);
        MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));
        UtilAlgo.UtilSha2Algo.pu1Sha2InBuf = au1PasswdBuf;
        UtilAlgo.UtilSha2Algo.i4Sha2BufLen =
            (INT4) ((SNMP_TWO * SNMP_AUTH_SHA512_KEY_LEN) +
                    ((UINT4) pEngineID->i4_Length));
        UtilAlgo.UtilSha2Algo.pu1Sha2MsgDigest = pu1Key;
        if (OSIX_SUCCESS != UtilMac (ISS_UTIL_ALGO_SHA512_ALGO, &UtilAlgo))
        {
            SNMP_TRC2 (SNMP_FAILURE_TRC, "SNMPKeyGenerator: Failed to generate"
                       " key in UtilMac: invalid auth [%d] or Pwd length [%d]\r\n",
                       u4Auth, i4PasswdLen);
            return SNMP_FAILURE;
        }
        pOctetString->i4_Length = SNMP_AUTH_SHA512_KEY_LEN;
    }
    else
    {
        if (ISS_IS_AUDIT_ENABLED () == ISS_TRUE)
        {
            /*Send log to Audit log file */
            MEMSET (au1AuditLogMsg, 0, AUDIT_LOG_SIZE);
            SPRINTF ((CHR1 *) au1AuditLogMsg,
                     "SNMP Key Genearation Failed for Auth:%d len:%d !!",
                     u4Auth, i4PasswdLen);
            MsrAuditSendLogInfo (au1AuditLogMsg, AUDIT_CRITICAL_LEVEL);
        }
        SNMP_TRC2 (SNMP_FAILURE_TRC, "SNMPKeyGenerator: "
                   "Failed to generate key: invalid auth [%d] or Pwd length [%d]\r\n",
                   u4Auth, i4PasswdLen);
        return SNMP_FAILURE;
    }
    SNMP_TRC (SNMP_DEBUG_TRC, "SNMPKeyGenerator: "
              "Successfully generated the key from user string\n");
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function exit\r\n", __func__);
    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : SNMPCompareOctetString
*  Description   : Function to compare two octet strings
*  Parameter(s)  : pFirstOctetStr - Pointer to First Octet String
*                  pSecondOctetStr - Pointer to Second Octet String
*  Return Values : SNMP_EQUAL/SNMP_LESSER/SNMP_GREATER
*********************************************************************/
INT4
SNMPCompareOctetString (tSNMP_OCTET_STRING_TYPE * pFirstOctetStr,
                        tSNMP_OCTET_STRING_TYPE * pSecondOctetStr)
{
    INT4                i4Return = SNMP_EQUAL;
    if (pFirstOctetStr->i4_Length != pSecondOctetStr->i4_Length)
    {
        if (pFirstOctetStr->i4_Length < pSecondOctetStr->i4_Length)
        {
            return SNMP_LESSER;
        }
        return SNMP_GREATER;
    }
    if (0 == pFirstOctetStr->i4_Length)
    {
        return SNMP_EQUAL;
    }
    i4Return = MEMCMP (pFirstOctetStr->pu1_OctetList,
                       pSecondOctetStr->pu1_OctetList,
                       MEM_MAX_BYTES (pFirstOctetStr->i4_Length,
                                      SNMP_MAX_OCTETSTRING_SIZE));
    if (i4Return > SNMP_ZERO)
    {
        return SNMP_GREATER;
    }
    else if (i4Return < SNMP_ZERO)
    {
        return SNMP_LESSER;
    }
    return SNMP_EQUAL;
}

/*********************************************************************
*  Function Name : SNMPCompareImpliedOctetString
*  Description   : Function to compare two octet strings
*  Parameter(s)  : pFirstOctetStr - Pointer to First Octet String
*                  pSecondOctetStr - Pointer to Second Octet String
*  Return Values : SNMP_EQUAL/SNMP_LESSER/SNMP_GREATER
*********************************************************************/
INT4
SNMPCompareImpliedOctetString (tSNMP_OCTET_STRING_TYPE * pFirstOctetStr,
                               tSNMP_OCTET_STRING_TYPE * pSecondOctetStr)
{
    INT4                i4Return = SNMP_EQUAL;
    INT4                i4CompareLen = SNMP_ZERO;

    if (pFirstOctetStr->i4_Length <= pSecondOctetStr->i4_Length)
    {
        i4CompareLen = pFirstOctetStr->i4_Length;
    }
    else
    {
        i4CompareLen = pSecondOctetStr->i4_Length;
    }
    if (0 == pFirstOctetStr->i4_Length)
    {
        return SNMP_EQUAL;
    }
    i4Return = MEMCMP (pFirstOctetStr->pu1_OctetList,
                       pSecondOctetStr->pu1_OctetList, i4CompareLen);
    if (i4Return > SNMP_ZERO)
    {
        return SNMP_GREATER;
    }
    else if (i4Return < SNMP_ZERO)
    {
        return SNMP_LESSER;
    }

    /* Content are same till i4CompareLen So, check the length */
    if (pFirstOctetStr->i4_Length > pSecondOctetStr->i4_Length)
    {
        return SNMP_GREATER;
    }
    else if (pFirstOctetStr->i4_Length < pSecondOctetStr->i4_Length)
    {
        return SNMP_LESSER;
    }
    return SNMP_EQUAL;
}

/*********************************************************************
*  Function Name : SNMPCompareExactMatchOctetString
*  Description   : Function to compare two octet strings for exact match
*  Parameter(s)  : pFirstOctetStr - Pointer to First Octet String
*                  pSecondOctetStr - Pointer to Second Octet String
*  Return Values : SNMP_EQUAL/SNMP_NOT_EQUAL
*********************************************************************/
INT4
SNMPCompareExactMatchOctetString (tSNMP_OCTET_STRING_TYPE * pFirstOctetStr,
                                  tSNMP_OCTET_STRING_TYPE * pSecondOctetStr)
{
    INT4                i4Return = SNMP_EQUAL;
    /* Proceed only if Length equal */
    if (pFirstOctetStr->i4_Length != pSecondOctetStr->i4_Length)
    {
        return SNMP_NOT_EQUAL;
    }
    if ((0 == pFirstOctetStr->i4_Length) && (0 == pSecondOctetStr->i4_Length))
    {
        return SNMP_EQUAL;
    }
    i4Return = MEMCMP (pFirstOctetStr->pu1_OctetList,
                       pSecondOctetStr->pu1_OctetList,
                       MEM_MAX_BYTES (pFirstOctetStr->i4_Length,
                                      SNMP_MAX_OCTETSTRING_SIZE));

    if (i4Return == SNMP_ZERO)
    {
        return SNMP_EQUAL;
    }

    return SNMP_NOT_EQUAL;
}

/*********************************************************************
*  Function Name : SNMPCopyOctetString
*  Description   : Function to copy one octet string to another one
*  Parameter(s)  : pFirstOctetStr - Destination octet string pointer
*                  pSecondOctetStr - Source octet string pointer
*  Return Values : None
*********************************************************************/
VOID
SNMPCopyOctetString (tSNMP_OCTET_STRING_TYPE * pFirstOctetStr,
                     tSNMP_OCTET_STRING_TYPE * pSecondOctetStr)
{
    MEMCPY (pFirstOctetStr->pu1_OctetList, pSecondOctetStr->pu1_OctetList,
            MEM_MAX_BYTES (pSecondOctetStr->i4_Length,
                           SNMP_MAX_OCTETSTRING_SIZE));
    pFirstOctetStr->i4_Length = pSecondOctetStr->i4_Length;
    return;
}

/*********************************************************************
*  Function Name : SNMPCopyOid
*  Description   : Function to copy one Oid to another one.
*  Parameter(s)  : pFirstOid - Destination Oid Pointer
*                  pSecondOid - Source Oid Pointer
*  Return Values : None
*********************************************************************/
VOID
SNMPCopyOid (tSNMP_OID_TYPE * pFirstOid, tSNMP_OID_TYPE * pSecondOid)
{
    MEMCPY (pFirstOid->pu4_OidList, pSecondOid->pu4_OidList,
            pSecondOid->u4_Length * sizeof (UINT4));
    pFirstOid->u4_Length = pSecondOid->u4_Length;
}

/*********************************************************************
*  Function Name : SNMPGenerateAuthSecurity
*  Description   : Function to generate auth digest 
*  Parameter(s)  : pu1PktPtr - SNMP Packet Pointer
*                  i4PktLen - SNMP Packet Lenght in Bytes
*                  pu1DataPtr - Pointer where the Digest to be copied
*                  pSnmpUsmEntry - Pointer to the Usm TableEntry
*  Return Values : SNMP_SUCCESS or SNMP_FAILURE
*********************************************************************/
INT4
SNMPGenerateAuthSecurity (UINT1 *pu1PktPtr, INT4 i4PktLen,
                          UINT1 *pu1DataPtr, tSnmpUsmEntry * pSnmpUsmEntry)
{
    UINT1              *pu1Key = NULL;
    UINT1               au1Digest[SNMP_MAX_PASSWD_LEN];
    INT4                i4AuthDigestLen = 0;
    unUtilAlgo          UtilAlgo;

    MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));
    MEMSET (au1Digest, 0, SNMP_MAX_PASSWD_LEN);

    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function entry\r\n", __func__);
    pu1Key = pSnmpUsmEntry->UsmUserAuthKeyChange.pu1_OctetList;

    UtilAlgo.UtilHmacAlgo.pu1HmacKey = pu1Key;
    UtilAlgo.UtilHmacAlgo.pu1HmacPktDataPtr = pu1PktPtr;
    UtilAlgo.UtilHmacAlgo.i4HmacPktLength = i4PktLen;
    UtilAlgo.UtilHmacAlgo.pu1HmacOutDigest = au1Digest;
    if (pSnmpUsmEntry->u4UsmUserAuthProtocol == SNMP_MD5)
    {
        i4AuthDigestLen = SNMP_AUTH_DIGEST_LEN;
        UtilAlgo.UtilHmacAlgo.u4HmacKeyLen = SNMP_AUTH_MD5_KEY_LEN;
        UtilHash (ISS_UTIL_ALGO_HMAC_MD5, &UtilAlgo);
    }
    else if (pSnmpUsmEntry->u4UsmUserAuthProtocol == SNMP_SHA)
    {
        UtilAlgo.UtilHmacAlgo.u4HmacKeyLen = SNMP_AUTH_SHA_KEY_LEN;
        i4AuthDigestLen = SNMP_AUTH_DIGEST_LEN;
        UtilHash (ISS_UTIL_ALGO_HMAC_SHA1, &UtilAlgo);
    }
    else if (pSnmpUsmEntry->u4UsmUserAuthProtocol == SNMP_SHA256)
    {
        i4AuthDigestLen = SNMP_AUTH_DIGEST_LEN_SHA256;
        UtilAlgo.UtilHmacAlgo.u4HmacKeyLen = SNMP_AUTH_SHA256_KEY_LEN;
        UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA256_ALGO;
        UtilHash (ISS_UTIL_ALGO_HMAC_SHA2, &UtilAlgo);
    }
    else if (pSnmpUsmEntry->u4UsmUserAuthProtocol == SNMP_SHA384)
    {
        i4AuthDigestLen = SNMP_AUTH_DIGEST_LEN_SHA384;
        UtilAlgo.UtilHmacAlgo.u4HmacKeyLen = SNMP_AUTH_SHA384_KEY_LEN;
        UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA384_ALGO;
        UtilHash (ISS_UTIL_ALGO_HMAC_SHA2, &UtilAlgo);
    }
    else if (pSnmpUsmEntry->u4UsmUserAuthProtocol == SNMP_SHA512)
    {
        i4AuthDigestLen = SNMP_AUTH_DIGEST_LEN_SHA512;
        UtilAlgo.UtilHmacAlgo.u4HmacKeyLen = SNMP_AUTH_SHA512_KEY_LEN;
        UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA512_ALGO;
        UtilHash (ISS_UTIL_ALGO_HMAC_SHA2, &UtilAlgo);
    }
    else
    {
        SNMP_TRC (SNMP_FAILURE_TRC, "SNMPGenerateAuthSecurity: "
                  "SNMP V3 NULL Authentication Type\r\n");
        return SNMP_FAILURE;
    }
    MEMCPY (pu1DataPtr, au1Digest, i4AuthDigestLen);
    SNMP_TRC (SNMP_DEBUG_TRC, "SNMPGenerateAuthSecurity: "
              "Successfully generated the auth digest\r\n");
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function exit\r\n", __func__);
    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : SNMPVerifyAuthSecurity
*  Description   : Function to verify the Usm Auth digest 
*  Parameter(s)  : pu1PktPtr - SNMP Packet Pointer
*                  i4PktLen - SNMP Packet Length in Bytes
*                  pV3Pdu - SNMP V3 Information Pointer
*                  pSnmpUsmEntry - Usm Table Entry Pointer
*  Return Values : SNMP_SUCCESS or SNMP_FAILURE
*********************************************************************/
INT4
SNMPVerifyAuthSecurity (UINT1 *pu1PktPtr, INT4 i4PktLen,
                        tSNMP_V3PDU * pV3Pdu, tSnmpUsmEntry * pSnmpUsmEntry)
{
    UINT1              *pu1Key = NULL;
    UINT1               au1Digest[SNMP_MAX_PASSWD_LEN];
    unUtilAlgo          UtilAlgo;
    MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));
    MEMSET (au1Digest, 0, SNMP_MAX_PASSWD_LEN);

    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function entry\r\n", __func__);
    pu1Key = pSnmpUsmEntry->UsmUserAuthKeyChange.pu1_OctetList;
    UtilAlgo.UtilHmacAlgo.pu1HmacKey = pu1Key;
    UtilAlgo.UtilHmacAlgo.pu1HmacPktDataPtr = pu1PktPtr;
    UtilAlgo.UtilHmacAlgo.i4HmacPktLength = i4PktLen;
    UtilAlgo.UtilHmacAlgo.pu1HmacOutDigest = au1Digest;
    if (pSnmpUsmEntry->u4UsmUserAuthProtocol == SNMP_MD5)
    {
        UtilAlgo.UtilHmacAlgo.u4HmacKeyLen = SNMP_AUTH_MD5_KEY_LEN;
        UtilHash (ISS_UTIL_ALGO_HMAC_MD5, &UtilAlgo);
    }
    else if (pSnmpUsmEntry->u4UsmUserAuthProtocol == SNMP_SHA)
    {
        UtilAlgo.UtilHmacAlgo.u4HmacKeyLen = SNMP_AUTH_SHA_KEY_LEN;
        UtilHash (ISS_UTIL_ALGO_HMAC_SHA1, &UtilAlgo);
    }
    else if (pSnmpUsmEntry->u4UsmUserAuthProtocol == SNMP_SHA256)
    {
        UtilAlgo.UtilHmacAlgo.u4HmacKeyLen = SNMP_AUTH_SHA256_KEY_LEN;
        UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA256_ALGO;
        UtilHash (ISS_UTIL_ALGO_HMAC_SHA2, &UtilAlgo);
    }
    else if (pSnmpUsmEntry->u4UsmUserAuthProtocol == SNMP_SHA384)
    {
        UtilAlgo.UtilHmacAlgo.u4HmacKeyLen = SNMP_AUTH_SHA384_KEY_LEN;
        UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA384_ALGO;
        UtilHash (ISS_UTIL_ALGO_HMAC_SHA2, &UtilAlgo);
    }
    else if (pSnmpUsmEntry->u4UsmUserAuthProtocol == SNMP_SHA512)
    {
        UtilAlgo.UtilHmacAlgo.u4HmacKeyLen = SNMP_AUTH_SHA512_KEY_LEN;
        UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA512_ALGO;
        UtilHash (ISS_UTIL_ALGO_HMAC_SHA2, &UtilAlgo);
    }
    else
    {
        SNMP_INR_INVALID_MSG;
        SNMP_TRC (SNMP_FAILURE_TRC, "SNMPVerifyAuthSecurity: "
                  "SNMP V3 NULL Authentication Type\r\n");
        return SNMP_FAILURE;
    }
    if (MEMCMP (au1Digest, (pV3Pdu->MsgSecParam.MsgAuthParam.pu1_OctetList),
                pV3Pdu->MsgSecParam.MsgAuthParam.i4_Length) == SNMP_ZERO)
    {
        SNMP_TRC (SNMP_DEBUG_TRC, "SNMPVerifyAuthSecurity: "
                  "Successfully verified the auth digest\r\n");
        return SNMP_SUCCESS;
    }
    SNMP_INC_USM_WRONGDIGESTS;
    SNMP_TRC (SNMP_FAILURE_TRC, "SNMPVerifyAuthSecurity: "
              "invalid auth digest\r\n");
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function exit\r\n", __func__);
    return (SNMP_FAILURE);
}

/******************************************************************************* 
 * Function Name :SnmpIVAesCfb128
 * Description   :Determine the initialization vector for AES encryption.
 *                (draft-bluementhal-aes-usm-03.txt,3.1.2.2)
 *                iv is defined as the concatenation of engine boots,engine time
 *                and a 64 bit salt integer is incremented.
 *                the resulting salt is copied in to the salt buffer.
 *                the iv result is returned individually for further use.
 *Parameter(s)   :pu1IV               pointer to buffer containing IV
                  i4IVLen             length of IV
                  u4EngineBoots       engineboot value
                  u4EngineTime        engine time value
                  pu1Salt             pointer to outgoing buffer
 *Return Values  : SNMP_SUCCESS or SNMP_FAILURE 
 *********************************************************************************/

INT4
SnmpIVAesCfb128 (UINT1 *pu1IV, UINT4 u4EngineBoots,
                 UINT4 u4EngineTime, UINT1 *pu1Salt)
{
    PRIVATE UINT4       u4SnmpSalt1 = SNMP_ZERO;
    PRIVATE UINT4       u4SnmpSalt2 = SNMP_ZERO;
    UINT4               u4Temp1 = SNMP_ZERO, u4Temp2 = SNMP_ZERO;

    if (pu1IV == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((u4SnmpSalt1 == SNMP_ZERO) && (u4SnmpSalt2 == SNMP_ZERO))
    {
        u4SnmpSalt1 = (UINT4) RAND ();
        u4SnmpSalt2 = (UINT4) RAND ();
    }
    else
    {
        u4SnmpSalt1 += 1;
        u4SnmpSalt2 += 1;
    }

    /* Just to make sure that Random value never be zero
     * any time
     */
    if ((u4SnmpSalt1 + 1) == 0)
        u4SnmpSalt1 += 1;
    if ((u4SnmpSalt2 + 1) == 0)
        u4SnmpSalt2 += 1;

    u4EngineBoots = OSIX_HTONL (u4EngineBoots);
    u4EngineTime = OSIX_HTONL (u4EngineTime);
    u4Temp1 = OSIX_HTONL (u4SnmpSalt1);
    u4Temp2 = OSIX_HTONL (u4SnmpSalt2);

    /*generating IV according to the RFC3826 */
    MEMCPY (pu1IV, &u4EngineBoots, 4);
    MEMCPY (pu1IV + 4, &u4EngineTime, 4);
    MEMCPY (pu1IV + 8, &u4Temp1, 4);
    MEMCPY (pu1IV + 12, &u4Temp2, 4);

    MEMCPY (pu1Salt, pu1IV + 8, 8);
    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : SNMPDecryptePDU
*  Description   : Function to decrypte the SNMP PDU
*  Parameter(s)  : pu1StartPtr - PDU Start Pointer
*                  i4Len - Encrypted PDU Length in Bytes
*                  pV3Pdu - SNMP V3 Information Pointer
*                  pSnmpUsmEntry - Usm Table Entry Pointer
*  Return Values : SNMP_SUCCESS or SNMP_FAILURE
*********************************************************************/
INT4
SNMPDecryptePDU (UINT1 *pu1StartPtr, INT4 i4Len, tSNMP_V3PDU * pV3Pdu,
                 tSnmpUsmEntry * pSnmpUsmEntry)
{
    /* Variable Declaration */
    unArCryptoKey       ArCryptoKey;
    UINT1              *pu1Temp = SNMP_ZERO;
    INT4                i4Count = SNMP_ZERO;
    UINT1               au1Salt[SNMP_SALT_LEN] = { SNMP_ZERO };
    CONST INT4          au1ID = SNMP_ZERO;
    UINT2               u2Aes = SNMP_ZERO;
    UINT1               au1IV[SNMP_AES_IV_LEN] = { SNMP_ZERO };
    UINT4               u4MsgEngineBoot = SNMP_ZERO, u4MsgEngineTime =
        SNMP_ZERO;
    UINT4               u4Index = SNMP_ZERO, u4Index1 = SNMP_SALT_LEN;
    unUtilAlgo          UtilAlgo;

    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function entry\r\n", __func__);
    MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));
    MEMSET (au1PreIV, 0, SNMP_AES_OUT_BUFF_LEN);
    if (pSnmpUsmEntry == NULL)
    {
        SNMP_TRC (SNMP_CRITICAL_TRC | SNMP_FAILURE_TRC, "SNMPDecryptePDU: "
                  "SNMP V3 Decryption Failed\r\n");
        SNMP_INC_USM_DECRYPTIONERRORS;
        return SNMP_FAILURE;
    }

    pu1Temp = pSnmpUsmEntry->UsmUserPrivKeyChange.pu1_OctetList;
    if (i4Len >= SNMP_AES_OUT_BUFF_LEN)
    {
        SNMP_TRC (SNMP_CRITICAL_TRC | SNMP_FAILURE_TRC, "SNMPDecryptePDU: "
                  "SNMP V3 Encryption Failed\r\n");
        return SNMP_FAILURE;
    }

    switch (pSnmpUsmEntry->u4UsmUserPrivProtocol)
    {
        case SNMP_DES_CBC:
            /* RFC 3414 sec.8.3.2 
             * 1)If the privParameters field is not an 8-octet OCTET STRING, then
             * an error indication (decryptionError) is returned to the calling
             * module */
            if (pV3Pdu->MsgSecParam.MsgPrivParam.i4_Length != SNMP_EIGHT)
            {
                SNMP_INC_USM_DECRYPTIONERRORS;
                return SNMP_FAILURE;
            }

            MEMCPY (au1Salt, pV3Pdu->MsgSecParam.MsgPrivParam.pu1_OctetList,
                    SNMP_ENCRYPT_BYTE_ALIGN);
            MEMCPY (au1PreIV, pu1Temp + SNMP_ENCRYPT_BYTE_ALIGN,
                    SNMP_ENCRYPT_BYTE_ALIGN);
            for (i4Count = SNMP_ZERO; i4Count < SNMP_ENCRYPT_BYTE_ALIGN;
                 i4Count++)
            {
                au1Salt[i4Count] ^= au1PreIV[i4Count];
            }
            if (DesArKeyScheduler (pu1Temp, &ArCryptoKey) == DES_FAILURE)
            {
                SNMPTrace ("SNMP  V3 Decryption Failed...\n");
                SNMP_INC_USM_DECRYPTIONERRORS;
                return SNMP_FAILURE;
            }
            /* RFC 3414 sec.8.3.2
             * 5)If the encryptedPDU cannot be decrypted, then an error indication
             * (decryptionError) is returned to the calling module */

            UtilAlgo.UtilDesAlgo.pu1DesInUserKey = pu1Temp;
            UtilAlgo.UtilDesAlgo.u4DesInKeyLen =
                (UINT4) pSnmpUsmEntry->UsmUserPrivKeyChange.i4_Length;
            UtilAlgo.UtilDesAlgo.u4DesInitVectLen = SNMP_ENCRYPT_BYTE_ALIGN;

            UtilAlgo.UtilDesAlgo.pu1DesInBuffer = pu1StartPtr;
            UtilAlgo.UtilDesAlgo.u4DesInBufSize = (UINT4) i4Len;
            UtilAlgo.UtilDesAlgo.punDesArCryptoKey = &ArCryptoKey;
            UtilAlgo.UtilDesAlgo.pu1DesInitVect = au1Salt;
            UtilAlgo.UtilDesAlgo.i4DesEnc = au1ID;

            if (UtilDecrypt (ISS_UTIL_ALGO_DES_CBC, &UtilAlgo) == OSIX_FAILURE)
            {
                SNMPTrace ("SNMP  V3 Decryption Failed...\n");
                SNMP_INC_USM_DECRYPTIONERRORS;
                return SNMP_FAILURE;
            }
            break;

        case SNMP_AESCFB128:
        case SNMP_AESCFB192:
        case SNMP_AESCFB256:

            if (pSnmpUsmEntry->u4UsmUserPrivProtocol == SNMP_AESCFB128)
            {
                u2Aes = SNMP_AES_128_KEY_LENGTH;
            }
            else if (pSnmpUsmEntry->u4UsmUserPrivProtocol == SNMP_AESCFB192)
            {
                u2Aes = SNMP_AES_192_KEY_LENGTH;
            }
            else if (pSnmpUsmEntry->u4UsmUserPrivProtocol == SNMP_AESCFB256)
            {
                u2Aes = SNMP_AES_256_KEY_LENGTH;
            }
            else
            {
                SNMPTrace ("SNMP  V3 Invalid Key length...\n");
                SNMP_INC_USM_DECRYPTIONERRORS;
                return SNMP_FAILURE;
            }
            u4MsgEngineBoot = OSIX_HTONL (pV3Pdu->MsgSecParam.u4MsgEngineBoot);
            u4MsgEngineTime = OSIX_HTONL (pV3Pdu->MsgSecParam.u4MsgEngineTime);

            MEMCPY (au1IV, &u4MsgEngineBoot, SNMP_FOUR);
            MEMCPY (au1IV + SNMP_FOUR, &u4MsgEngineTime, SNMP_FOUR);

            for (u4Index = SNMP_ZERO;
                 u4Index < (UINT4) pV3Pdu->MsgSecParam.MsgPrivParam.i4_Length;
                 u4Index++, u4Index1++)
            {
                au1IV[u4Index1] =
                    pV3Pdu->MsgSecParam.MsgPrivParam.pu1_OctetList[u4Index];
            }

            if (AesArSetDecryptKey (pu1Temp, u2Aes, &ArCryptoKey) ==
                AES_FAILURE)
            {
                SNMPTrace ("SNMPV3 Key Generation Failed...\n");
                SNMP_INC_USM_DECRYPTIONERRORS;
                return SNMP_FAILURE;
            }

            UtilAlgo.UtilAesAlgo.pu1AesInUserKey = pu1Temp;
            UtilAlgo.UtilAesAlgo.u4AesInKeyLen = (UINT4) u2Aes;
            UtilAlgo.UtilAesAlgo.u4AesInitVectLen = SNMP_AES_IV_LEN;

            UtilAlgo.UtilAesAlgo.pu1AesInBuf = pu1StartPtr;
            UtilAlgo.UtilAesAlgo.pu1AesOutBuf = au1PreIV;
            UtilAlgo.UtilAesAlgo.u4AesInBufSize = (UINT4) i4Len;
            UtilAlgo.UtilAesAlgo.punAesArCryptoKey = &ArCryptoKey;
            UtilAlgo.UtilAesAlgo.pu1AesInitVector = au1IV;
            UtilAlgo.UtilAesAlgo.pu4AesNum = (UINT4 *) &i4Count;
            UtilAlgo.UtilAesAlgo.i4AesEnc = au1ID;

            if (UtilDecrypt (ISS_UTIL_ALGO_AES_CFB128, &UtilAlgo) ==
                OSIX_FAILURE)
            {
                SNMPTrace ("SNMPV3 Decryption Failed...\n");
                SNMP_INC_USM_DECRYPTIONERRORS;
                return SNMP_FAILURE;
            }

            MEMSET (pu1StartPtr, SNMP_ZERO, i4Len);
            for (u4Index = SNMP_ZERO; u4Index < (UINT4) i4Len; u4Index++)
            {
                pu1StartPtr[u4Index] = au1PreIV[u4Index];
            }
            break;

        case SNMP_TDES_CBC:
            if (pV3Pdu->MsgSecParam.MsgPrivParam.i4_Length != SNMP_EIGHT)
            {
                SNMP_INC_USM_DECRYPTIONERRORS;
                return SNMP_FAILURE;
            }

            MEMCPY (au1Salt, pV3Pdu->MsgSecParam.MsgPrivParam.pu1_OctetList,
                    SNMP_ENCRYPT_BYTE_ALIGN);
            MEMCPY (au1PreIV, pu1Temp + SNMP_ENCRYPT_BYTE_ALIGN,
                    SNMP_ENCRYPT_BYTE_ALIGN);
            for (i4Count = SNMP_ZERO; i4Count < SNMP_ENCRYPT_BYTE_ALIGN;
                 i4Count++)
            {
                au1Salt[i4Count] ^= au1PreIV[i4Count];
            }
            if (TDesArKeySchedule (pu1Temp, &ArCryptoKey) == DES_FAILURE)
            {
                SNMPTrace ("SNMP  V3 Decryption Failed...\n");
                SNMP_INC_USM_DECRYPTIONERRORS;
                return SNMP_FAILURE;
            }

            UtilAlgo.UtilDesAlgo.pu1DesInUserKey = pu1Temp;
            UtilAlgo.UtilDesAlgo.u4DesInKeyLen =
                (UINT4) pSnmpUsmEntry->UsmUserPrivKeyChange.i4_Length;
            UtilAlgo.UtilDesAlgo.u4DesInitVectLen = SNMP_ENCRYPT_BYTE_ALIGN;

            UtilAlgo.UtilDesAlgo.pu1DesInBuffer = pu1StartPtr;
            UtilAlgo.UtilDesAlgo.u4DesInBufSize = (UINT4) i4Len;
            UtilAlgo.UtilDesAlgo.punDesArCryptoKey = &ArCryptoKey;
            UtilAlgo.UtilDesAlgo.pu1DesInitVect = au1Salt;
            UtilAlgo.UtilDesAlgo.i4DesEnc = au1ID;

            if (UtilDecrypt (ISS_UTIL_ALGO_TDES_CBC, &UtilAlgo) == OSIX_FAILURE)
            {
                SNMPTrace ("SNMP  V3 Decryption Failed...\n");
                SNMP_INC_USM_DECRYPTIONERRORS;
                return SNMP_FAILURE;
            }
            break;

        default:
        {
            SNMPTrace ("SNMPV3 NULL Decryption Type...\n");
            return SNMP_FAILURE;
        }
    }
    SNMP_TRC (SNMP_DEBUG_TRC, "SNMPDecryptePDU: "
              "Successfully decrypted the PDU\r\n");
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function exit\r\n", __func__);
    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : SNMPEncryptePDU 
*  Description   : Function Encrypte a Plain PDU
*  Parameter(s)  : pu1StartPtr - Plain PDU Pointer
*                  i4Len - PDU Length in Bytes
*                  pSnmpUsmEntry - Usm Table Entry Pointer
*                  pV3Pdu - SNMP V3 Inforamtion Pointer
*  Return Values : SNMP_SUCCESS or SNMP_FAILURE
*********************************************************************/
INT4
SNMPEncryptePDU (UINT1 *pu1StartPtr, INT4 i4Len, tSNMP_V3PDU * pV3Pdu,
                 tSnmpUsmEntry * pSnmpUsmEntry)
{
    unArCryptoKey       ArCryptoKey;
    UINT1              *pu1Temp = SNMP_ZERO;
    UINT4               u4MsgEngineBoot = pV3Pdu->MsgSecParam.u4MsgEngineBoot;
    INT4                u4MsgEngineTime =
        (INT4) pV3Pdu->MsgSecParam.u4MsgEngineTime;
    INT4                u4Index = SNMP_ZERO, u4Index1 = SNMP_SALT_LEN;
    CONST INT4          i4ID = SNMP_SALT_LEN;
    UINT2               u2Aes = SNMP_ZERO;
    UINT1               au1Salt[SNMP_ENCRYPT_BYTE_ALIGN] = { SNMP_ZERO };
    UINT1               au1IV[SNMP_AES_IV_LEN] = { SNMP_ZERO };
    unUtilAlgo          UtilAlgo;

    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function entry\r\n", __func__);
    MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));
    MEMSET (au1PreIV, 0, SNMP_AES_OUT_BUFF_LEN);

    if (pV3Pdu->MsgFlag.pu1_OctetList[0] & 0x01)
    {
        if (!pSnmpUsmEntry)
        {
            SNMPTrace ("Encode SNMP Usm Entry Param Failed\n");
            return SNMP_FAILURE;
        }
    }

    pu1Temp = pSnmpUsmEntry->UsmUserPrivKeyChange.pu1_OctetList;
    MEMSET (au1Salt, SNMP_ZERO, SNMP_ENCRYPT_BYTE_ALIGN);

    if (i4Len >= SNMP_AES_OUT_BUFF_LEN)
    {
        SNMP_TRC (SNMP_CRITICAL_TRC | SNMP_FAILURE_TRC, "SNMPEncryptePDU: "
                  "SNMP V3 Encryption Failed\r\n");
        return SNMP_FAILURE;
    }

    switch (pSnmpUsmEntry->u4UsmUserPrivProtocol)
    {
        case SNMP_DES_CBC:

            MEMCPY (au1Salt, pV3Pdu->MsgSecParam.MsgPrivParam.pu1_OctetList,
                    SNMP_ENCRYPT_BYTE_ALIGN);
            MEMCPY (au1PreIV, pu1Temp + SNMP_ENCRYPT_BYTE_ALIGN,
                    SNMP_ENCRYPT_BYTE_ALIGN);
            for (u4Index = SNMP_ZERO; u4Index < SNMP_ENCRYPT_BYTE_ALIGN;
                 u4Index++)
            {
                au1Salt[u4Index] ^= au1PreIV[u4Index];
            }
            DesArKeyScheduler (pu1Temp, &ArCryptoKey);

            UtilAlgo.UtilDesAlgo.pu1DesInUserKey = pu1Temp;
            UtilAlgo.UtilDesAlgo.u4DesInKeyLen =
                (UINT4) pSnmpUsmEntry->UsmUserPrivKeyChange.i4_Length;
            UtilAlgo.UtilDesAlgo.u4DesInitVectLen = SNMP_ENCRYPT_BYTE_ALIGN;

            UtilAlgo.UtilDesAlgo.pu1DesInBuffer = pu1StartPtr;
            UtilAlgo.UtilDesAlgo.u4DesInBufSize = (UINT4) i4Len;
            UtilAlgo.UtilDesAlgo.punDesArCryptoKey = &ArCryptoKey;
            UtilAlgo.UtilDesAlgo.pu1DesInitVect = au1Salt;
            UtilAlgo.UtilDesAlgo.i4DesEnc = i4ID;

            if (UtilEncrypt (ISS_UTIL_ALGO_DES_CBC, &UtilAlgo) == OSIX_FAILURE)
            {
                SNMPTrace ("SNMP  V3 Encryption Failed...\n");
                return SNMP_FAILURE;
            }
            break;

        case SNMP_AESCFB128:
        case SNMP_AESCFB192:
        case SNMP_AESCFB256:

            if (pSnmpUsmEntry->u4UsmUserPrivProtocol == SNMP_AESCFB128)
            {
                u2Aes = SNMP_AES_128_KEY_LENGTH;
            }
            else if (pSnmpUsmEntry->u4UsmUserPrivProtocol == SNMP_AESCFB192)
            {
                u2Aes = SNMP_AES_192_KEY_LENGTH;
            }
            else if (pSnmpUsmEntry->u4UsmUserPrivProtocol == SNMP_AESCFB256)
            {
                u2Aes = SNMP_AES_256_KEY_LENGTH;
            }
            else
            {
                SNMPTrace ("SNMP  V3 Encryption Failed...\n");
                return SNMP_FAILURE;
            }

            u4MsgEngineBoot = OSIX_HTONL (pV3Pdu->MsgSecParam.u4MsgEngineBoot);
            u4MsgEngineTime = OSIX_HTONL (pV3Pdu->MsgSecParam.u4MsgEngineTime);

            MEMCPY (au1IV, &u4MsgEngineBoot, SNMP_FOUR);
            MEMCPY (au1IV + SNMP_FOUR, &u4MsgEngineTime, SNMP_FOUR);

            u4Index1 = SNMP_SALT_LEN;
            pV3Pdu->MsgSecParam.MsgPrivParam.i4_Length = u4Index1;
            for (u4Index = SNMP_ZERO;
                 u4Index < pV3Pdu->MsgSecParam.MsgPrivParam.i4_Length;
                 u4Index++, u4Index1++)
            {
                au1IV[u4Index1] =
                    pV3Pdu->MsgSecParam.MsgPrivParam.pu1_OctetList[u4Index];
            }

            u4Index = SNMP_ZERO;
            AesArSetEncryptKey (pu1Temp, u2Aes, &ArCryptoKey);
            UtilAlgo.UtilAesAlgo.pu1AesInUserKey = pu1Temp;
            UtilAlgo.UtilAesAlgo.u4AesInKeyLen = (UINT4) u2Aes;
            UtilAlgo.UtilAesAlgo.u4AesInitVectLen = (UINT4) SNMP_AES_IV_LEN;

            UtilAlgo.UtilAesAlgo.pu1AesInBuf = pu1StartPtr;
            UtilAlgo.UtilAesAlgo.pu1AesOutBuf = au1PreIV;
            UtilAlgo.UtilAesAlgo.u4AesInBufSize = (UINT4) i4Len;
            UtilAlgo.UtilAesAlgo.punAesArCryptoKey = &ArCryptoKey;
            UtilAlgo.UtilAesAlgo.pu1AesInitVector = au1IV;
            UtilAlgo.UtilAesAlgo.pu4AesNum = (UINT4 *) &u4Index;
            UtilAlgo.UtilAesAlgo.i4AesEnc = i4ID;

            if (UtilEncrypt (ISS_UTIL_ALGO_AES_CFB128, &UtilAlgo) ==
                OSIX_FAILURE)
            {
                SNMPTrace ("SNMP  V3 Encryption Failed...\n");
                return SNMP_FAILURE;
            }

            MEMSET (pu1StartPtr, SNMP_ZERO, i4Len);
            for (u4Index = SNMP_ZERO; u4Index < i4Len; u4Index++)
            {
                pu1StartPtr[u4Index] = au1PreIV[u4Index];
            }
            break;

        case SNMP_TDES_CBC:

            MEMCPY (au1Salt, pV3Pdu->MsgSecParam.MsgPrivParam.pu1_OctetList,
                    SNMP_ENCRYPT_BYTE_ALIGN);
            MEMCPY (au1PreIV, pu1Temp + SNMP_ENCRYPT_BYTE_ALIGN,
                    SNMP_ENCRYPT_BYTE_ALIGN);
            for (u4Index = SNMP_ZERO; u4Index < SNMP_ENCRYPT_BYTE_ALIGN;
                 u4Index++)
            {
                au1Salt[u4Index] ^= au1PreIV[u4Index];
            }
            TDesArKeySchedule (pu1Temp, &ArCryptoKey);

            UtilAlgo.UtilDesAlgo.pu1DesInUserKey = pu1Temp;
            UtilAlgo.UtilDesAlgo.u4DesInKeyLen =
                (UINT4) pSnmpUsmEntry->UsmUserPrivKeyChange.i4_Length;
            UtilAlgo.UtilDesAlgo.u4DesInitVectLen = SNMP_ENCRYPT_BYTE_ALIGN;

            UtilAlgo.UtilDesAlgo.pu1DesInBuffer = pu1StartPtr;
            UtilAlgo.UtilDesAlgo.u4DesInBufSize = (UINT4) i4Len;
            UtilAlgo.UtilDesAlgo.punDesArCryptoKey = &ArCryptoKey;
            UtilAlgo.UtilDesAlgo.pu1DesInitVect = au1Salt;
            UtilAlgo.UtilDesAlgo.i4DesEnc = i4ID;

            if (UtilEncrypt (ISS_UTIL_ALGO_TDES_CBC, &UtilAlgo) == OSIX_FAILURE)
            {
                SNMPTrace ("SNMP  V3 Encryption Failed...\n");
                return SNMP_FAILURE;
            }
            break;

        default:
            SNMPTrace ("SNMP  V3 NULL Encryption Type...\n");
            return SNMP_FAILURE;
    }
    SNMP_TRC (SNMP_DEBUG_TRC, "SNMPEncryptePDU: "
              "Successfully encrypted the PDU\r\n");
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function exit\r\n", __func__);
    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : SNMPValidateEngineId 
*  Description   : Function to Validate the Engine Id
*  Parameter(s)  : pSnmpEngineID - Engine ID 
*  Return Values : SNMP_SUCCESS or SNMP_FAILURE
*********************************************************************/

INT4
SNMPValidateEngineId (tSNMP_OCTET_STRING_TYPE * pSnmpEngineID)
{
    tTMO_SLL_NODE      *pLstNode = NULL;
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;

    TMO_SLL_Scan (&gSnmpUsmSll, pLstNode, tTMO_SLL_NODE *)
    {
        pSnmpUsmEntry = (tSnmpUsmEntry *) pLstNode;
        if (SNMPCompareOctetString (pSnmpEngineID,
                                    &(pSnmpUsmEntry->UsmUserEngineID))
            == SNMP_EQUAL)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/*********************************************************************
*  Function Name : SnmpGetUsmUserPassword
*  Description   : Function to return the UserAuth and UserPriv 
*                  passwords
*  Parameter(s)  : pSnmpEngineID - Snmp Engine Id
*                  pUsmUserName  - User name
*  Return Values : SNMP_SUCCESS - When the USM user entry exists
*                  otherwise returns SNMP_FAILURE
*********************************************************************/

INT4
SnmpGetUsmUserPassword (tSNMP_OCTET_STRING_TYPE * pUsmUserEngineId,
                        tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                        UINT1 *pu1UsmUserAuthPassword,
                        UINT1 *pu1UsmUserPrivPassword)
{
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;

    pSnmpUsmEntry = SNMPGetUsmEntry (pUsmUserEngineId, pUsmUserName);
    if (pSnmpUsmEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pSnmpUsmEntry->UsmUserAuthPassword.i4_Length != SNMP_ZERO)
    {
        MEMCPY (pu1UsmUserAuthPassword,
                pSnmpUsmEntry->UsmUserAuthPassword.pu1_OctetList,
                pSnmpUsmEntry->UsmUserAuthPassword.i4_Length);
    }

    if (pSnmpUsmEntry->UsmUserPrivPassword.i4_Length != SNMP_ZERO)
    {
        MEMCPY (pu1UsmUserPrivPassword,
                pSnmpUsmEntry->UsmUserPrivPassword.pu1_OctetList,
                pSnmpUsmEntry->UsmUserPrivPassword.i4_Length);
    }
    return SNMP_SUCCESS;

}

/*********************************************************************
*  Function Name : SnmpGetUsmUserProtocol
*  Description   : Function to return the UserAuth and UserPriv 
*                  Protocol
*  Input(s)      : pSnmpEngineID          - Snmp Engine Id
*                  pUsmUserName           - User name
*  Output(s)     : pu4UsmUserAuthProtocol - User Auth Protocol
*                  pu4UsmUserPrivProtocol - User Priv Protocol
*  Return Values : SNMP_SUCCESS - When the USM user entry exists
*                  otherwise returns SNMP_FAILURE
*********************************************************************/

INT4
SnmpGetUsmUserProtocol (tSNMP_OCTET_STRING_TYPE * pUsmUserEngineId,
                        tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                        UINT4 *pu4UsmUserAuthProtocol,
                        UINT4 *pu4UsmUserPrivProtocol)
{
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;

    pSnmpUsmEntry = SNMPGetUsmEntry (pUsmUserEngineId, pUsmUserName);
    if (pSnmpUsmEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4UsmUserAuthProtocol = pSnmpUsmEntry->u4UsmUserAuthProtocol;
    *pu4UsmUserPrivProtocol = pSnmpUsmEntry->u4UsmUserPrivProtocol;
    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : SnmpGetUsmUserAuthPrivKeyChange
*  Description   : Function to return the UserAuth and UserPriv 
*                  Key Change.
*  Input(s)      : pSnmpEngineID         - Snmp Engine Id
*                  pUsmUserName          - User name
*  Output(s)     : pUsmUserAuthKeyChange - Hashed Auth key password
*                  pUsmUserPrivKeyChange - Hashed Priv Key password
*  Return Values : SNMP_SUCCESS - When the USM user entry exists
*                  otherwise returns SNMP_FAILURE
*********************************************************************/

INT4
SnmpGetUsmUserAuthPrivKeyChange (tSNMP_OCTET_STRING_TYPE * pUsmUserEngineId,
                                 tSNMP_OCTET_STRING_TYPE * pUsmUserName,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pUsmUserAuthKeyChange,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pUsmUserPrivKeyChange)
{
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;

    pSnmpUsmEntry = SNMPGetUsmEntry (pUsmUserEngineId, pUsmUserName);
    if (pSnmpUsmEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pUsmUserAuthKeyChange->i4_Length =
        pSnmpUsmEntry->UsmUserAuthKeyChange.i4_Length;
    pUsmUserPrivKeyChange->i4_Length =
        pSnmpUsmEntry->UsmUserPrivKeyChange.i4_Length;

    if (pUsmUserAuthKeyChange->i4_Length != SNMP_ZERO)
    {
        MEMCPY (pUsmUserAuthKeyChange->pu1_OctetList,
                pSnmpUsmEntry->UsmUserAuthKeyChange.pu1_OctetList,
                pUsmUserAuthKeyChange->i4_Length);
    }

    if (pUsmUserPrivKeyChange->i4_Length != SNMP_ZERO)
    {
        MEMCPY (pUsmUserPrivKeyChange->pu1_OctetList,
                pSnmpUsmEntry->UsmUserPrivKeyChange.pu1_OctetList,
                pUsmUserPrivKeyChange->i4_Length);
    }
    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : SnmpUpdateUserEntries
*  Description   : Function to update the userentries 
*                  with new snmp engineID
*  Input(s)      : pPrevEngineID   - Previous Snmp Engine ID
*  Return Values : SNMP_SUCCESS - If updation is a successs
*                  otherwise returns SNMP_FAILURE
*********************************************************************/
INT1
SnmpUpdateUserEntries (INT1 *pPrevEngineID)
{
    tSnmpUsmEntry      *pUsmEntry = NULL;
    INT1                ai1EngineID[SNMP_MAX_STR_ENGINEID_LEN + 1];
    MEMSET (ai1EngineID, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);

    pUsmEntry = SNMPGetFirstUsmEntry ();
    if (pUsmEntry == NULL)
    {
        return SNMP_SUCCESS;
    }

    do
    {
        /* UsmUserEngineID should be updated only for active users 
         * which has UsmUserEngineID same as Previous gSnmpEngineID */
        SnmpConvertOctetToString (&(pUsmEntry->UsmUserEngineID), ai1EngineID);
        if ((STRCMP (ai1EngineID, pPrevEngineID) == 0)
            && (pUsmEntry->u4UsmUserStatus == SNMP_ROWSTATUS_ACTIVE))
        {
            pUsmEntry->u4UsmUserStatus = UNDER_CREATION;
            SNMPCopyOctetString (&(pUsmEntry->UsmUserEngineID), &gSnmpEngineID);

            if (pUsmEntry->u4UsmUserAuthProtocol != SNMP_NO_AUTH)
            {
                nmhSetUsmUserAuthKeyChange (&(pUsmEntry->UsmUserEngineID),
                                            &(pUsmEntry->UsmUserName),
                                            &(pUsmEntry->UsmUserAuthPassword));

                if (pUsmEntry->u4UsmUserPrivProtocol != SNMP_NO_PRIV)
                {
                    nmhSetUsmUserPrivKeyChange (&(pUsmEntry->UsmUserEngineID),
                                                &(pUsmEntry->UsmUserName),
                                                &(pUsmEntry->
                                                  UsmUserPrivPassword));

                }
            }

            if ((pUsmEntry->u4UsmUserAuthProtocol != SNMP_NO_AUTH) &&
                (pUsmEntry->UsmUserAuthKeyChange.i4_Length == SNMP_ZERO))
            {
                /* Cannot be made active until the corresponding
                 * usmUserAuthKeyChange have been set. */
                return SNMP_FAILURE;
            }
            pUsmEntry->u4UsmUserStatus = SNMP_ROWSTATUS_ACTIVE;
        }
        MEMSET (ai1EngineID, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);
    }
    while ((pUsmEntry =
            SNMPGetNextUsmEntry (&(pUsmEntry->UsmUserEngineID),
                                 &(pUsmEntry->UsmUserName))) != NULL);

    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : SnmpUpdateCommunity
*  Description   : Function to update the community context engine id
*                  with the new snmp engineID 
*  Input(s)      : pPrevEngineID   - Previous Snmp Engine ID
*  Return Values : SNMP_SUCCESS 
*                  
*********************************************************************/
INT1
SnmpUpdateCommunity (INT1 *pPrevEngineID)
{
    tCommunityMappingEntry *pCommEntry = NULL;
    INT1                ai1EngineID[SNMP_MAX_STR_ENGINEID_LEN + 1];
    MEMSET (ai1EngineID, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);
    if ((pCommEntry = SNMPGetFirstCommunity ()) == NULL)
    {
        return SNMP_SUCCESS;
    }
    do
    {
        SnmpConvertOctetToString (&(pCommEntry->ContextEngineID), ai1EngineID);

        /* ContextEngineID should be updated only for active CommEntry
         * which has ContextEngineID same as previous gSnmpEngineID */

        if ((STRCMP (ai1EngineID, pPrevEngineID) == 0)
            && (pCommEntry->i4Status == SNMP_ROWSTATUS_ACTIVE))
        {
            pCommEntry->i4Status = SNMP_ROWSTATUS_NOTINSERVICE;
            SNMPCopyOctetString (&(pCommEntry->ContextEngineID),
                                 &gSnmpEngineID);
            pCommEntry->i4Status = SNMP_ROWSTATUS_ACTIVE;
        }
        MEMSET (ai1EngineID, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);
        pCommEntry = SNMPGetNextCommunityEntry (&(pCommEntry->CommIndex));
    }
    while (pCommEntry != NULL);
    return SNMP_SUCCESS;
}

#ifdef EXT_CRYPTO_WANTED
/*********************************************************************
*  Function Name : SnmpEncryptUserKey
*  Description   : Function to Encrypt the SNMP User keys 
*                  using remote IP as input key. 
*  Input(s)      : u4Datalen   - Length of the input key.
*                     pu1UserKey  - User Key for Encryption.
*  Onput(s)      : pu1data     - Encrypted data output.
*  Return Values : SNMP_SUCCESS / SNMP_FAILURE 
*                  
*********************************************************************/
INT4
SnmpEncryptUserKey (UINT1 *pu1data, UINT4 u4Datalen, UINT1 *pu1UserKey)
{
    unArCryptoKey       ArCryptoKey;
    unUtilAlgo          UtilAlgo;
    UINT1               au1IV[SNMP_AES_IV_LEN] = { SNMP_ZERO };
    UINT2               u2Aes = AES_128_KEY_LENGTH;
    UINT4               u4Index = SNMP_ZERO;
    UINT4               u4Index1 = SNMP_SALT_LEN;
    CONST INT4          i4ID = SNMP_ONE;

    MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));
    MEMSET (au1IV, 0, SNMP_AES_IV_LEN);
    MEMSET (au1PreIV, 0, SNMP_AES_OUT_BUFF_LEN);

    for (u4Index = SNMP_ZERO; u4Index < SNMP_SALT_LEN; u4Index++, u4Index1++)
    {
        au1IV[u4Index1] = pu1UserKey[u4Index];
    }
    u4Index = SNMP_ZERO;
    AesArSetEncryptKey (pu1UserKey, u2Aes, &ArCryptoKey);
    UtilAlgo.UtilAesAlgo.pu1AesInUserKey = pu1UserKey;
    UtilAlgo.UtilAesAlgo.u4AesInKeyLen = (UINT4) u2Aes;
    UtilAlgo.UtilAesAlgo.u4AesInitVectLen = SNMP_AES_IV_LEN;

    UtilAlgo.UtilAesAlgo.pu1AesInBuf = pu1data;
    UtilAlgo.UtilAesAlgo.pu1AesOutBuf = au1PreIV;
    UtilAlgo.UtilAesAlgo.u4AesInBufSize = (UINT4) u4Datalen;
    UtilAlgo.UtilAesAlgo.punAesArCryptoKey = &ArCryptoKey;
    UtilAlgo.UtilAesAlgo.pu1AesInitVector = au1IV;
    UtilAlgo.UtilAesAlgo.pu4AesNum = (UINT4 *) &u4Index;
    UtilAlgo.UtilAesAlgo.i4AesEnc = i4ID;

    if (UtilEncrypt (ISS_UTIL_ALGO_AES_CFB128, &UtilAlgo) == OSIX_FAILURE)
    {
        SNMPTrace ("SNMPV3 Encryption Failed...\n");
        return SNMP_FAILURE;
    }
    /* Copy the output back to the input. */
    MEMSET (pu1data, SNMP_ZERO, u4Datalen);
    for (u4Index = SNMP_ZERO; u4Index < u4Datalen; u4Index++)
    {
        pu1data[u4Index] = au1PreIV[u4Index];
    }
    return SNMP_SUCCESS;

}

/*********************************************************************
*  Function Name : SnmpDecryptUserKey
*  Description   : Function to Decrypt the SNMP User keys received
*                  using self IP as the key input. 
*  Input(s)      : u4Datalen   - Length of the input key.
*                     pu1UserKey  - User Key fro Encryption.
*  Onput(s)      : pu1data     - Encrypted data output.
*  Return Values : SNMP_SUCCESS / SNMP_FAILURE 
*                  
*********************************************************************/
INT4
SnmpDecryptUserKey (UINT1 *pu1data, UINT4 u4Datalen, UINT1 *pu1UserKey)
{
    unArCryptoKey       ArCryptoKey;
    unUtilAlgo          UtilAlgo;
    UINT1               au1IV[SNMP_AES_IV_LEN] = { SNMP_ZERO };
    UINT2               u2Aes = AES_128_KEY_LENGTH;
    UINT4               u4Index = SNMP_ZERO;
    UINT4               u4Index1 = SNMP_SALT_LEN;
    INT4                i4Count = SNMP_ZERO;
    CONST INT4          i4ID = SNMP_ZERO;

    MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));
    MEMSET (au1IV, 0, SNMP_AES_IV_LEN);
    MEMSET (au1PreIV, 0, SNMP_AES_OUT_BUFF_LEN);

    for (u4Index = SNMP_ZERO; u4Index < SNMP_SALT_LEN; u4Index++, u4Index1++)
    {
        au1IV[u4Index1] = pu1UserKey[u4Index];
    }
    if (AesArSetDecryptKey (pu1UserKey, u2Aes, &ArCryptoKey) == AES_FAILURE)
    {
        SNMPTrace ("SNMPV3 Key Generation Failed...\n");
        return SNMP_FAILURE;

    }
    UtilAlgo.UtilAesAlgo.pu1AesInUserKey = pu1UserKey;
    UtilAlgo.UtilAesAlgo.u4AesInKeyLen = (UINT4) u2Aes;
    UtilAlgo.UtilAesAlgo.u4AesInitVectLen = SNMP_AES_IV_LEN;

    UtilAlgo.UtilAesAlgo.pu1AesInBuf = pu1data;
    UtilAlgo.UtilAesAlgo.pu1AesOutBuf = au1PreIV;
    UtilAlgo.UtilAesAlgo.u4AesInBufSize = u4Datalen;
    UtilAlgo.UtilAesAlgo.punAesArCryptoKey = &ArCryptoKey;
    UtilAlgo.UtilAesAlgo.pu1AesInitVector = au1IV;
    UtilAlgo.UtilAesAlgo.pu4AesNum = (UINT4 *) &i4Count;
    UtilAlgo.UtilAesAlgo.i4AesEnc = i4ID;

    if (UtilDecrypt (ISS_UTIL_ALGO_AES_CFB128, &UtilAlgo) == OSIX_FAILURE)
    {
        SNMPTrace ("SNMPV3 Decryption Failed...\n");
        return SNMP_FAILURE;
    }
    /* Copy the output back to the input. */
    MEMSET (pu1data, SNMP_ZERO, u4Datalen);
    for (u4Index = SNMP_ZERO; u4Index < u4Datalen; u4Index++)
    {
        pu1data[u4Index] = au1PreIV[u4Index];
    }
    return SNMP_SUCCESS;

}
#endif
/********************  END OF FILE   ***************************************/
