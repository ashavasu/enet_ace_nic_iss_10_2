/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: snmusmwr.h,v 1.4 2015/04/28 12:35:03 siva Exp $
*
* Description: Proto types for Wrapper  Routines
*********************************************************************/
#ifndef _SNMUSMWR_H
#define _SNMUSMWR_H

VOID RegisterSNMUSM(VOID);

INT4 UsmStatsUnsupportedSecLevelsGet(tSnmpIndex *, tRetVal *);
INT4 UsmStatsNotInTimeWindowsGet(tSnmpIndex *, tRetVal *);
INT4 UsmStatsUnknownUserNamesGet(tSnmpIndex *, tRetVal *);
INT4 UsmStatsUnknownEngineIDsGet(tSnmpIndex *, tRetVal *);
INT4 UsmStatsWrongDigestsGet(tSnmpIndex *, tRetVal *);
INT4 UsmStatsDecryptionErrorsGet(tSnmpIndex *, tRetVal *);
INT4 UsmUserSpinLockGet(tSnmpIndex *, tRetVal *);
INT4 UsmUserSpinLockSet(tSnmpIndex *, tRetVal *);
INT4 UsmUserSpinLockTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 UsmUserSpinLockDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

INT4 GetNextIndexUsmUserTable(tSnmpIndex *, tSnmpIndex *);
INT4 UsmUserEngineIDGet(tSnmpIndex *, tRetVal *);
INT4 UsmUserNameGet(tSnmpIndex *, tRetVal *);
INT4 UsmUserSecurityNameGet(tSnmpIndex *, tRetVal *);
INT4 UsmUserCloneFromGet(tSnmpIndex *, tRetVal *);
INT4 UsmUserAuthProtocolGet(tSnmpIndex *, tRetVal *);
INT4 UsmUserAuthKeyChangeGet(tSnmpIndex *, tRetVal *);
INT4 UsmUserOwnAuthKeyChangeGet(tSnmpIndex *, tRetVal *);
INT4 UsmUserPrivProtocolGet(tSnmpIndex *, tRetVal *);
INT4 UsmUserPrivKeyChangeGet(tSnmpIndex *, tRetVal *);
INT4 UsmUserOwnPrivKeyChangeGet(tSnmpIndex *, tRetVal *);
INT4 UsmUserPublicGet(tSnmpIndex *, tRetVal *);
INT4 UsmUserStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 UsmUserStatusGet(tSnmpIndex *, tRetVal *);
INT4 UsmUserCloneFromSet(tSnmpIndex *, tRetVal *);
INT4 UsmUserAuthProtocolSet(tSnmpIndex *, tRetVal *);
INT4 UsmUserAuthKeyChangeSet(tSnmpIndex *, tRetVal *);
INT4 UsmUserOwnAuthKeyChangeSet(tSnmpIndex *, tRetVal *);
INT4 UsmUserPrivProtocolSet(tSnmpIndex *, tRetVal *);
INT4 UsmUserPrivKeyChangeSet(tSnmpIndex *, tRetVal *);
INT4 UsmUserOwnPrivKeyChangeSet(tSnmpIndex *, tRetVal *);
INT4 UsmUserPublicSet(tSnmpIndex *, tRetVal *);
INT4 UsmUserStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 UsmUserStatusSet(tSnmpIndex *, tRetVal *);
INT4 UsmUserCloneFromTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 UsmUserAuthProtocolTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 UsmUserAuthKeyChangeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 UsmUserOwnAuthKeyChangeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 UsmUserPrivProtocolTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 UsmUserPrivKeyChangeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 UsmUserOwnPrivKeyChangeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 UsmUserPublicTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 UsmUserStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 UsmUserStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 UsmUserTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);










#endif /* _SNMUSMWR_H */
