/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: snmplock.c,v 1.12 2017/11/20 13:11:26 siva Exp $
 *
 * Description: Routines to call Lock and Unlock functions before 
 *              calling Wrapper(midlevel) routines. 
 *******************************************************************/
#include "snmpcmn.h"
#include "msr.h"

#ifdef L2RED_WANTED
#include "snmpred.h"
#endif

INT4
SNMPAccessGet (tSnmpDbEntry * pSnmpDbEntry,
               tSNMP_MULTI_DATA_TYPE * pData, tSnmpIndex * pIndex,
               UINT4 *pu4Error, UINT4 u4VcNum)
{
    INT4                i4Result = SNMP_FAILURE;
    tSNMP_OID_TYPE     *pEntRetOid = NULL;

    if (pSnmpDbEntry->pLockPointer != NULL)
    {
        if (pSnmpDbEntry->pLockPointer () == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    if (pSnmpDbEntry->pSetContextPointer != NULL)
    {
        if (u4VcNum >= MAX_SNMP_CONTEXT)
        {

            if (pSnmpDbEntry->pUnlockPointer != NULL)
            {
                pSnmpDbEntry->pUnlockPointer ();
            }
            return SNMP_FAILURE;
        }
        if (pSnmpDbEntry->pSetContextPointer (u4VcNum) == SNMP_FAILURE)
        {
            if (pSnmpDbEntry->pUnlockPointer != NULL)
            {
                pSnmpDbEntry->pUnlockPointer ();
            }
            *pu4Error = SNMP_UNKNOWN_CONTEXT_NAME;
            return SNMP_FAILURE;
        }
    }
    pEntRetOid = alloc_oid (SNMP_MAX_OID_LENGTH);
    if (pEntRetOid == NULL)
    {
        UtlTrcLog (1, 1, "SNMP", "OID alloc Failed\n");
        return SNMP_FAILURE;
    }
    i4Result = pSnmpDbEntry->pMibEntry->Get (pIndex, pData);
    if (((pData->i2_DataType == SNMP_DATA_TYPE_OBJECT_ID)
         && pData->pOidValue->u4_Length > EOID_OFFSET)
        || (pData->i2_DataType == SNMP_DATA_TYPE_NULL))
    {
        SNMPRevertEOID (pData->pOidValue, pEntRetOid);
        SNMPOIDCopy (pData->pOidValue, pEntRetOid);
    }
    free_oid (pEntRetOid);

    if (pSnmpDbEntry->pReleaseContextPointer != NULL)
    {
        pSnmpDbEntry->pReleaseContextPointer ();
    }
    if (pSnmpDbEntry->pUnlockPointer != NULL)
    {
        if (pSnmpDbEntry->pUnlockPointer () == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return i4Result;
}

INT4
SNMPAccessGetNext (tSnmpDbEntry * pSnmpDbEntry,
                   tSnmpIndex * pCurIndex, tSnmpIndex * pNextIndex,
                   UINT4 u4VcNum)
{
    INT4                i4Result = SNMP_FAILURE;
    if (pSnmpDbEntry->pLockPointer != NULL)
    {
        if (pSnmpDbEntry->pLockPointer () == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    if (pSnmpDbEntry->pSetContextPointer != NULL)
    {
        if (u4VcNum >= MAX_SNMP_CONTEXT)
        {
            if (pSnmpDbEntry->pUnlockPointer != NULL)
            {
                pSnmpDbEntry->pUnlockPointer ();
            }
            return SNMP_FAILURE;
        }
        if (pSnmpDbEntry->pSetContextPointer (u4VcNum) == SNMP_FAILURE)
        {
            if (pSnmpDbEntry->pUnlockPointer != NULL)
            {
                pSnmpDbEntry->pUnlockPointer ();
            }
            return SNMP_FAILURE;
        }
    }
    i4Result = pSnmpDbEntry->pMibEntry->GetNextIndex (pCurIndex, pNextIndex);
    if (pSnmpDbEntry->pReleaseContextPointer != NULL)
    {
        pSnmpDbEntry->pReleaseContextPointer ();
    }
    if (pSnmpDbEntry->pUnlockPointer != NULL)
    {
        if (pSnmpDbEntry->pUnlockPointer () == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    return i4Result;
}

INT4
SNMPAccessSet (tSnmpDbEntry * pSnmpDbEntry,
               tSNMP_MULTI_DATA_TYPE * pData, tSnmpIndex * pIndex,
               UINT4 u4VcNum)
{
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;
#ifdef L2RED_WANTED
    tRmNotifConfChg     RmNotifConfChg;
    UINT4               u4SeqNo = 0;
    UINT4               u4IgnoreAbortSeqNo = 0;
    UINT4               u4AcceptAbortSeqNo = 0;
#endif
    INT4                i4Result = SNMP_FAILURE;
    UINT4               indx = 0;
    UINT1               au1Temp[SNMP_MAX_OID_LENGTH], *pau1Temp = NULL;
    UINT1              *pOid = NULL;

    if (pSnmpDbEntry->pLockPointer != NULL)
    {
        if (pSnmpDbEntry->pLockPointer () == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    if (pSnmpDbEntry->pSetContextPointer != NULL)
    {
        if (u4VcNum >= MAX_SNMP_CONTEXT)
        {
            if (pSnmpDbEntry->pUnlockPointer != NULL)
            {
                pSnmpDbEntry->pUnlockPointer ();
            }
            return SNMP_FAILURE;
        }
        if (pSnmpDbEntry->pSetContextPointer (u4VcNum) == SNMP_FAILURE)
        {
            if (pSnmpDbEntry->pUnlockPointer != NULL)
            {
                pSnmpDbEntry->pUnlockPointer ();
            }
            return SNMP_FAILURE;
        }
    }

#ifdef L2RED_WANTED
    if ((pSnmpDbEntry->SnmpMsrTgr == SNMP_MSR_TGR_TRUE) &&
        (SNMP_NUM_STANDBY_NODES () != 0) &&
        (SNMP_NODE_STATUS () == SNMP_NODE_ACTIVE))
    {
        RM_GET_SEQ_NUM (&u4SeqNo);
    }
    /* Setting row status of an entry to destroy/not-in-service, may cause 
     * dynamic information change and may result in syncing up dynamic infomration.
     * This may lead to the following issue:
     *          In standby, the dynamic messages will be applied after static 
     *          message is applied (as the sequence number is reserved before 
     *          configuration in active node).
     *          But if the static message is applied, the entry will be deleted in 
     *          the standby node.
     *          After this when the dynamic messages for this entry is applied, it 
     *          will lead to a failure in standby node.
     *
     * To avoid this failure the following is done for row status objects, when 
     * the value is set to destroy/not-in-service:
     *        - After reserving the seq no for the configuration, reserve a one 
     *          more seq no for new message ("Ignore-Abort").
     *          After the configuration is applied in active, send the 
     *          ignore-abort message.
     *          Then get a new sequence no and send accept-abort message.
     *
     *       By this way, the standby will get the static sync up message first and 
     *       will apply it.
     *       Then it will get the ignore-abort message (before the dynamic sync 
     *       generated due to this configuration).
     *       After getting the ignore-abort message, standby will start ignoring the 
     *       sync=up apply failures from the protocols until the accept-abort message 
     *       comes. By this way the harmless sync up failures will be ignored.
     *
     */

    if (u4SeqNo != 0)
    {
        if ((pSnmpDbEntry->pMibEntry->u2RowStatus == OSIX_TRUE) &&
            ((pData->i4_SLongValue == DESTROY) ||
             (pData->i4_SLongValue == NOT_IN_SERVICE)))
        {
            /* Get Ignore sequence number before calling nmhSet */
            RM_GET_SEQ_NUM (&u4IgnoreAbortSeqNo);
        }

        if (pSnmpDbEntry->pMibEntry->Set == IfMainBrgPortTypeSet)
        {
            /* In case of bridge port type configuration, deletion/creation 
             * will happen. since it is not a rowstatus object, explicit sequencing should
             * be done for this.*/
            RM_GET_SEQ_NUM (&u4IgnoreAbortSeqNo);
        }
    }
#endif

    i4Result = pSnmpDbEntry->pMibEntry->Set (pIndex, pData);
    if (pSnmpDbEntry->pReleaseContextPointer != NULL)
    {
        pSnmpDbEntry->pReleaseContextPointer ();
    }

    if (pSnmpDbEntry->pUnlockPointer != NULL)
    {
        if (pSnmpDbEntry->pUnlockPointer () == SNMP_FAILURE)
        {
            return i4Result;
        }
    }

    /* Send notification to MSR and RM about the configuration change. MSR
     * notification needs to be given only if the Set returns SUCCESS whereas
     * for RM notification has to be send for both SUCCESS and FAILURE.
     */
#if defined (ISS_WANTED) || defined (L2RED_WANTED)

    /* This check is addded because for MI supported modules, notification
     * will be send from inside the nmhSet function itself.*/

    if (pSnmpDbEntry->SnmpMsrTgr == SNMP_MSR_TGR_TRUE)
    {
        MEMSET (au1Temp, 0, SNMP_MAX_OID_LENGTH);
        pau1Temp = au1Temp;

        for (indx = 0; indx < (pSnmpDbEntry->pMibEntry->ObjectID.u4_Length - 1);
             indx++)
        {
            SPRINTF ((CHR1 *) pau1Temp, "%u.",
                     pSnmpDbEntry->pMibEntry->ObjectID.pu4_OidList[indx]);
            pau1Temp += STRLEN (pau1Temp);
        }

        SPRINTF ((CHR1 *) pau1Temp, "%u",
                 pSnmpDbEntry->pMibEntry->ObjectID.pu4_OidList[indx]);

        pau1Temp += STRLEN (pau1Temp);

#ifdef L2RED_WANTED
        if (u4SeqNo != 0)
        {
            MEMSET (&RmNotifConfChg, 0, sizeof (tRmNotifConfChg));
            if (SnmpAllocMultiIndexAndData (&(RmNotifConfChg.pMultiIndex),
                                            pIndex->u4No,
                                            &(RmNotifConfChg.pMultiData),
                                            &(RmNotifConfChg.pu1ObjectId),
                                            STRLEN (au1Temp)) == SUCCESS)
            {

                STRCPY (RmNotifConfChg.pu1ObjectId, au1Temp);

                if (SnmpCopyMultiIndexAndData (RmNotifConfChg.pMultiIndex,
                                               pIndex,
                                               RmNotifConfChg.pMultiData,
                                               pData) == SNMP_SUCCESS)
                {
                    RmNotifConfChg.u4SeqNo = u4SeqNo;
                    RmNotifConfChg.i4OidLen = (INT4) STRLEN (au1Temp);
                    RmNotifConfChg.i1ConfigSetStatus = (INT1) i4Result;
                    RmNotifConfChg.i1MsgType = RM_UNUSED;
                    RmApiSyncStaticConfig (&RmNotifConfChg);
                }
                else
                {
                    MSRUpdateMemFree (RmNotifConfChg.pMultiIndex,
                                      RmNotifConfChg.pMultiData,
                                      RmNotifConfChg.pu1ObjectId);
                }
            }
            else
            {
                printf ("Memory allocation for RM notification failed in"
                        "SNMPAccessSet\n");
            }
        }

        RmNotifConfChg.pMultiIndex = NULL;
        RmNotifConfChg.pMultiData = NULL;
        RmNotifConfChg.pu1ObjectId = NULL;
        RmNotifConfChg.i4OidLen = 0;

        if ((i4Result != SNMP_SUCCESS) && (u4IgnoreAbortSeqNo != 0))
        {
            /* In case of rowstatus destroy, the snmpset has been failed.
             * so just send across the dummy message since we had already
             * allocated the sequence number */
            RmNotifConfChg.u4SeqNo = u4IgnoreAbortSeqNo;
            RmNotifConfChg.i1ConfigSetStatus = (INT1) i4Result;
            RmNotifConfChg.i1MsgType = RM_UNUSED;
            RmApiSyncStaticConfig (&RmNotifConfChg);
        }

        if ((i4Result == SNMP_SUCCESS) && (u4IgnoreAbortSeqNo != 0))
        {
            /* In case of sucessful row status destroy, we need to send two
             * extra messages namely ignore abort to get rid of failure of dynamic
             * sync up messages and accept abort to resume the ignore messages
             * in the standby node */
            RmNotifConfChg.u4SeqNo = u4IgnoreAbortSeqNo;
            RmNotifConfChg.i1ConfigSetStatus = (INT1) i4Result;
            RmNotifConfChg.i1MsgType = RM_CONFIG_IGNORE_ABORT;
            RmApiSyncStaticConfig (&RmNotifConfChg);

            RM_GET_SEQ_NUM (&u4AcceptAbortSeqNo);
            RmNotifConfChg.u4SeqNo = u4AcceptAbortSeqNo;
            RmNotifConfChg.i1ConfigSetStatus = (INT1) i4Result;
            RmNotifConfChg.i1MsgType = RM_CONFIG_ACCEPT_ABORT;
            RmApiSyncStaticConfig (&RmNotifConfChg);
        }
#endif

        if ((MsrGetIncrementalSaveStatus () == ISS_FALSE) ||
            (MsrGetRestorationStatus () == ISS_TRUE))
        {
            /* When Incremental save is disabled, no need to send notification 
             * to MSR. */
            return i4Result;
        }

        if (SNMP_SUCCESS == i4Result)
        {
            if (SnmpAllocMultiIndexAndData (&pMultiIndex, pIndex->u4No,
                                            &pMultiData, &pOid,
                                            STRLEN (au1Temp)) == FAILURE)
            {
                SNMPTrace ("\n\r Allocation of MultiIndex and Data failed\n");
                return i4Result;
            }

            STRCPY (pOid, au1Temp);

            if (SnmpCopyMultiIndexAndData (pMultiIndex, pIndex, pMultiData,
                                           pData) == SNMP_SUCCESS)
            {
                MSRNotifyConfChg (pMultiIndex,
                                  (BOOL1) pSnmpDbEntry->pMibEntry->u2RowStatus,
                                  pMultiData, pOid);
            }
            else
            {
                MSRUpdateMemFree (pMultiIndex, pMultiData, pOid);
                SNMPTrace ("\n\r Indication to MSR failed");
            }
        }
    }
#else
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    UNUSED_PARAM (indx);
    UNUSED_PARAM (au1Temp);
    UNUSED_PARAM (pau1Temp);
    UNUSED_PARAM (pOid);
#endif

    return i4Result;
}

INT4
SNMPAccessTest (tSnmpDbEntry * pSnmpDbEntry,
                tSNMP_MULTI_DATA_TYPE * pData, tSnmpIndex * pIndex,
                UINT4 *pu4Error, UINT4 u4VcNum)
{
    INT4                i4Result = SNMP_FAILURE;
    if (pSnmpDbEntry->pLockPointer != NULL)
    {
        if (pSnmpDbEntry->pLockPointer () == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    if (pSnmpDbEntry->pSetContextPointer != NULL)
    {
        if (u4VcNum >= MAX_SNMP_CONTEXT)
        {
            if (pSnmpDbEntry->pUnlockPointer != NULL)
            {
                pSnmpDbEntry->pUnlockPointer ();
            }
            return SNMP_FAILURE;
        }
        if (pSnmpDbEntry->pSetContextPointer (u4VcNum) == SNMP_FAILURE)
        {
            if (pSnmpDbEntry->pUnlockPointer != NULL)
            {
                pSnmpDbEntry->pUnlockPointer ();
            }
            *pu4Error = SNMP_UNKNOWN_CONTEXT_NAME;
            return SNMP_FAILURE;
        }
    }
    i4Result = pSnmpDbEntry->pMibEntry->Test (pu4Error, pIndex, pData);

    if (IssuGetMaintModeOperation () == OSIX_TRUE)
    {
        /* During ISSU maintenance mode, allow
         * only ISSU and related OID's */
        if (IssuIsOidAllowed ((pSnmpDbEntry->pMibEntry->ObjectID))
            != ISSU_SUCCESS)
        {
            i4Result = SNMP_FAILURE;
        }
    }
#ifdef RM_WANTED
    else if (RmRetrieveNodeState () == RM_STANDBY)
    {
        /* Standby can be made accessible through OOB.
         * Since Standby is accessible through OOB secondary ip
         * the SNMPSET calls need to be restricted to allowed OID's*/
        if (IssuIsOidAllowedInStandby ((pSnmpDbEntry->pMibEntry->ObjectID))
            != ISSU_SUCCESS)
        {
            i4Result = SNMP_FAILURE;
        }
    }
#endif

    if (pSnmpDbEntry->pReleaseContextPointer != NULL)
    {
        pSnmpDbEntry->pReleaseContextPointer ();
    }
    if (pSnmpDbEntry->pUnlockPointer != NULL)
    {
        if (pSnmpDbEntry->pUnlockPointer () == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return i4Result;
}

INT4
SNMPAccessDep (tSnmpDbEntry * pSnmpDbEntry,
               UINT4 *pu4Error, tSnmpIndexList * IndexList,
               tSNMP_VAR_BIND * VarBindList, UINT4 u4VcNum)
{
    INT4                i4Result = SNMP_FAILURE;
    if (pSnmpDbEntry->pLockPointer != NULL)
    {
        if (pSnmpDbEntry->pLockPointer () == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    if (pSnmpDbEntry->pSetContextPointer != NULL)
    {
        if (u4VcNum >= MAX_SNMP_CONTEXT)
        {
            if (pSnmpDbEntry->pUnlockPointer != NULL)
            {
                pSnmpDbEntry->pUnlockPointer ();
            }
            return SNMP_FAILURE;
        }
        if (pSnmpDbEntry->pSetContextPointer (u4VcNum) == SNMP_FAILURE)
        {
            if (pSnmpDbEntry->pUnlockPointer != NULL)
            {
                pSnmpDbEntry->pUnlockPointer ();
            }
            *pu4Error = SNMP_UNKNOWN_CONTEXT_NAME;
            return SNMP_FAILURE;
        }
    }
    i4Result = pSnmpDbEntry->pMibEntry->Dep (pu4Error, IndexList, VarBindList);
    if (pSnmpDbEntry->pReleaseContextPointer != NULL)
    {
        pSnmpDbEntry->pReleaseContextPointer ();
    }
    if (pSnmpDbEntry->pUnlockPointer != NULL)
    {
        if (pSnmpDbEntry->pUnlockPointer () == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    return i4Result;
}

INT1
SnmpCopyMultiIndexAndData (tSnmpIndex * pMultiIndex, tSnmpIndex * pIndex,
                           tSNMP_MULTI_DATA_TYPE * pMultiData,
                           tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4Indx = 0;

    for (u4Indx = 0; u4Indx < pIndex->u4No; u4Indx++)
    {
        switch (pIndex->pIndex[u4Indx].i2_DataType)
        {
            case SNMP_DATA_TYPE_TIME_TICKS:
            case SNMP_DATA_TYPE_UNSIGNED32:
                pMultiIndex->pIndex[u4Indx].i2_DataType =
                    SNMP_DATA_TYPE_UNSIGNED32;
                pMultiIndex->pIndex[u4Indx].u4_ULongValue =
                    pIndex->pIndex[u4Indx].u4_ULongValue;
                break;
            case SNMP_DATA_TYPE_INTEGER32:
                pMultiIndex->pIndex[u4Indx].i2_DataType =
                    SNMP_DATA_TYPE_INTEGER32;
                pMultiIndex->pIndex[u4Indx].i4_SLongValue =
                    pIndex->pIndex[u4Indx].i4_SLongValue;
                break;
            case SNMP_DATA_TYPE_IP_ADDR_PRIM:
                pMultiIndex->pIndex[u4Indx].i2_DataType =
                    SNMP_DATA_TYPE_IP_ADDR_PRIM;
                pMultiIndex->pIndex[u4Indx].u4_ULongValue =
                    pIndex->pIndex[u4Indx].u4_ULongValue;
                break;
            case SNMP_DATA_TYPE_OBJECT_ID:
            case SNMP_DATA_TYPE_IMP_OBJECT_ID:
                pMultiIndex->pIndex[u4Indx].i2_DataType =
                    SNMP_DATA_TYPE_OBJECT_ID;
                pMultiIndex->pIndex[u4Indx].pOidValue->u4_Length =
                    pIndex->pIndex[u4Indx].pOidValue->u4_Length;
                MEMCPY (pMultiIndex->pIndex[u4Indx].pOidValue->pu4_OidList,
                        pIndex->pIndex[u4Indx].pOidValue->pu4_OidList,
                        pIndex->pIndex[u4Indx].pOidValue->u4_Length *
                        sizeof (UINT4));
                break;
            case SNMP_DATA_TYPE_OCTET_PRIM:
            case SNMP_DATA_TYPE_IMP_OCTET_PRIM:
            case SNMP_FIXED_LENGTH_OCTET_STRING:
                pMultiIndex->pIndex[u4Indx].i2_DataType =
                    SNMP_DATA_TYPE_OCTET_PRIM;
                pMultiIndex->pIndex[u4Indx].pOctetStrValue->i4_Length =
                    pIndex->pIndex[u4Indx].pOctetStrValue->i4_Length;
                MEMCPY (pMultiIndex->pIndex[u4Indx].pOctetStrValue->
                        pu1_OctetList,
                        pIndex->pIndex[u4Indx].pOctetStrValue->pu1_OctetList,
                        pIndex->pIndex[u4Indx].pOctetStrValue->i4_Length);
                break;
            default:
                return SNMP_FAILURE;
        }
    }
    switch (pData->i2_DataType)
    {
        case SNMP_DATA_TYPE_TIME_TICKS:
        case SNMP_DATA_TYPE_UNSIGNED32:
            pMultiData->i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pMultiData->u4_ULongValue = pData->u4_ULongValue;
            break;
        case SNMP_DATA_TYPE_INTEGER32:
            pMultiData->i2_DataType = SNMP_DATA_TYPE_INTEGER32;
            pMultiData->i4_SLongValue = pData->i4_SLongValue;
            break;
        case SNMP_DATA_TYPE_IP_ADDR_PRIM:
            pMultiData->i2_DataType = SNMP_DATA_TYPE_IP_ADDR_PRIM;
            pMultiData->u4_ULongValue = pData->u4_ULongValue;
            break;
        case SNMP_DATA_TYPE_OBJECT_ID:
            pMultiData->i2_DataType = SNMP_DATA_TYPE_OBJECT_ID;
            pMultiData->pOidValue->u4_Length = pData->pOidValue->u4_Length;
            MEMCPY (pMultiData->pOidValue->pu4_OidList,
                    pData->pOidValue->pu4_OidList,
                    pData->pOidValue->u4_Length * sizeof (UINT4));
            break;
        case SNMP_DATA_TYPE_OCTET_PRIM:
            pMultiData->i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            pMultiData->pOctetStrValue->i4_Length =
                pData->pOctetStrValue->i4_Length;
            MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
                    pData->pOctetStrValue->pu1_OctetList,
                    pData->pOctetStrValue->i4_Length);
            break;
        case SNMP_FIXED_LENGTH_OCTET_STRING:
            pMultiData->i2_DataType = SNMP_FIXED_LENGTH_OCTET_STRING;
            pMultiData->pOctetStrValue->i4_Length =
                pData->pOctetStrValue->i4_Length;
            MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
                    pData->pOctetStrValue->pu1_OctetList,
                    pData->pOctetStrValue->i4_Length);

            break;

        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
