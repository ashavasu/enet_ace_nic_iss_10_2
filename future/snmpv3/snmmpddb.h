/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: snmmpddb.h,v 1.4 2015/04/28 12:35:02 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _SNMMPDDB_H
#define _SNMMPDDB_H


UINT4 snmmpd [] ={1,3,6,1,6,3,10,2,1};
tSNMP_OID_TYPE snmmpdOID = {9, snmmpd};


UINT4 SnmpUnknownSecurityModels [ ] ={1,3,6,1,6,3,11,2,1,1};
UINT4 SnmpInvalidMsgs [ ] ={1,3,6,1,6,3,11,2,1,2};
UINT4 SnmpUnknownPDUHandlers [ ] ={1,3,6,1,6,3,11,2,1,3};
UINT4 SnmpEngineID [ ] ={1,3,6,1,6,3,10,2,1,1};
UINT4 SnmpEngineBoots [ ] ={1,3,6,1,6,3,10,2,1,2};
UINT4 SnmpEngineTime [ ] ={1,3,6,1,6,3,10,2,1,3};
UINT4 SnmpEngineMaxMessageSize [ ] ={1,3,6,1,6,3,10,2,1,4};


tMbDbEntry snmmpdMibEntry[]= {

{{10,SnmpEngineID}, NULL, SnmpEngineIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,SnmpEngineBoots}, NULL, SnmpEngineBootsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,SnmpEngineTime}, NULL, SnmpEngineTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,SnmpEngineMaxMessageSize}, NULL, SnmpEngineMaxMessageSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,SnmpUnknownSecurityModels}, NULL, SnmpUnknownSecurityModelsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,SnmpInvalidMsgs}, NULL, SnmpInvalidMsgsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,SnmpUnknownPDUHandlers}, NULL, SnmpUnknownPDUHandlersGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData snmmpdEntry = { 7, snmmpdMibEntry };
#endif /* _SNMMPDDB_H */

