/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: snmpmsg.c,v 1.14 2017/11/20 13:11:26 siva Exp $
 *
 * Description: routines for SNMP Message Process 
 *******************************************************************/
#include "snmpcmn.h"
#include "snmpmsg.h"
#include "snprxtrn.h"
#include "utilrand.h"
#include "fssyslog.h"

/************************************************************************
 *  Function Name   : SNMPProcess 
 *  Description     : Function to decode and process the input data from
 *                    Manager  
 *  Input           : pData - Data Pointer
 *                    u4Length - Data Length
 *  Output          : pOut - Pointer contains Data to be sent to manager 
 *                    u4OutLen - Output Data Length      
 *  Returns         : SNMP_SUCCESS or SNMP_FAILURE 
 ************************************************************************/
INT4
SNMPProcess (UINT1 *pData, UINT4 u4Length, UINT1 **pOut, UINT4 *u4OutLen)
{
    tSNMP_NORMAL_PDU   *pPtr = NULL;
    tSNMP_TRAP_PDU     *pTrapPdu = NULL;
    INT4                i4Result = SNMP_FAILURE;
    UINT4               u4Err = SNMP_ERR_NO_ERROR, i4Ind = 0;
    tSnmpVacmInfo       VacmInfo;
    UINT1               au1ContextName[SNMP_MAX_OCTETSTRING_SIZE];
    INT4                i4IsProxy = SNMP_FALSE;
    INT4                i4SendFailResp = SNMP_FALSE;
    tSNMP_VAR_BIND     *pVarPtr = NULL;
    tSNMP_OID_TYPE      ProxyDropOid;
    UINT4               ui4SnmpProxyDrops[] = { 1, 3, 6, 1, 2, 1, 11, 32 };
    INT2                i2_PduType = SNMP_ZERO;
    tCommunityMappingEntry *pCommEntry = NULL;
    VOID               *pProxy = NULL;
    UINT4               u4_Version = SNMP_ZERO;
    VOID               *pDecodedPtr = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    UINT1              *pu1Oid = NULL;
    UINT1              *pu1Data = NULL;
    UINT1               u1LoggingError = SNMP_NO_LOGGING_ERROR;

    CHR1                au1SnmpBufMsg[SNMP_MSG_BUF_LEN];
    tUtlInAddr          InAddr;
    tUtlIn6Addr         In6Addr;
    tMgrIpAddr          ManagerIpAddr;
    INT4                i4ConfigAllowed = SNMP_SUCCESS;
#ifdef L2RED_WANTED
    INT4                i4BulkUpdtResult = RM_SUCCESS;
#endif

    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function Entry\r\n", __func__);
    MEMSET (au1SnmpBufMsg, 0, SNMP_MSG_BUF_LEN);
    MEMSET (&InAddr, 0, sizeof (tUtlInAddr));
    MEMSET (&In6Addr, 0, sizeof (tUtlIn6Addr));
    MEMSET (&ManagerIpAddr, 0, sizeof (tMgrIpAddr));

    STRCPY (au1ContextName, "");
    *pOut = NULL;
    *u4OutLen = 0;
    pDecodedPtr =
        (VOID *) SNMPDecodePacket (pData, (INT4) u4Length, &i2_PduType);
    if (pDecodedPtr == NULL)
    {
        SNMP_INR_BAD_ASN_PARSE;
        SNMP_TRC (SNMP_FAILURE_TRC, "SNMPProcess: " "Decode packet is NULL");
        return SNMP_FAILURE;
    }

    if (SNMP_PDU_TYPE_TRAP == i2_PduType)
    {
        pTrapPdu = (tSNMP_TRAP_PDU *) pDecodedPtr;
        u4_Version = pTrapPdu->u4_Version;
    }
    else
    {
        pPtr = (tSNMP_NORMAL_PDU *) pDecodedPtr;
        u4_Version = pPtr->u4_Version;
        pCommEntry = SNMPGetCommunityEntryFromName (pPtr->pCommunityStr);
        if (pCommEntry == NULL)
        {
            SNMP_INR_BAD_COMMUNITY;
            SNMP_TRC (SNMP_FAILURE_TRC, "SNMPProcess: "
                      "[PROXY]:No Matching in the community"
                      " Mapping Table\r\n");
            i4Result = SNMP_FAILURE;
            pPtr->i4_ErrorStatus = SNMP_ERR_BAD_VALUE;
            if (gSnmpStat.i4SnmpEnableAuthTraps == AUTHTRAP_ENABLE)
            {
                /* SnmpSendAuthFailureTrap (); */
                SnmpSendAuthFailureTrap (pPtr);
            }
        }
    }
    if (pPtr == NULL)
    {
        SNMP_INR_SILENT_DROPS;
        SNMP_TRC (SNMP_FAILURE_TRC, "SNMPProcess: "
                  "Unable to process the pdu\r\n");
        return SNMP_FAILURE;
    }
    if ((u4_Version != VERSION1) && (u4_Version != VERSION2))
    {
        SNMP_INR_BAD_VERSION;
        SNMP_INR_SILENT_DROPS;
        SNMP_TRC (SNMP_FAILURE_TRC, "SNMPProcess: "
                  "Unable to process the pdu\r\n");
        SNMPPduFree (pDecodedPtr);
        return SNMP_FAILURE;
    }

    /* If Incoming message is response then Match with the cache and 
     * check if the response is to be handled as a proxy
     */
    if ((SNMP_PDU_TYPE_GET_RESPONSE == i2_PduType) ||
        (SNMP_PDU_TYPE_TRAP == i2_PduType))
    {
        if (SNMP_FAILURE ==
            SNMPProxyPduProcess (i2_PduType, pDecodedPtr, pOut, u4OutLen,
                                 &i4IsProxy, &i4SendFailResp))
        {
            SNMP_TRC1 (SNMP_FAILURE_TRC, "SNMPProcess: "
                       "Proxy pdu process failed for the type %d", i2_PduType);
            return SNMP_FAILURE;
        }
        else if ((SNMP_TRUE == i4IsProxy) &&
                 (SNMP_PDU_TYPE_GET_RESPONSE == i2_PduType))
        {
            SNMP_TRC1 (SNMP_DEBUG_TRC, "SNMPProcess: "
                       "proxy handling is taken place for type %d", i2_PduType);
            return SNMP_SUCCESS;
        }
        else if (SNMP_PDU_TYPE_TRAP == i2_PduType)
        {
            SNMP_TRC1 (SNMP_DEBUG_TRC, "SNMPProcess: "
                       "proxy handling is taken place for type %d", i2_PduType);
            return SNMP_SUCCESS;
        }
        /* else agent handling shall be done */
    }
    /* Rest of the code shall proceed for - Requests, Responses 
     * which are not to be handled as a proxy and Traps other than V1 trap. 
     * For all these cases tSNMP_NORMAL_PDU is used and hence pPtr shall be used
     */

    if (pPtr->i4_ErrorStatus != SNMP_ERR_TOO_BIG)
    {
        SNMPSetRequestID (pPtr->u4_RequestID);

        /* Additional check is put so that the same function is not called
         * twice for responses
         */
        if (SNMP_PDU_TYPE_GET_RESPONSE != i2_PduType)
        {
            i4Result =
                SNMPProxyPduProcess (i2_PduType, pDecodedPtr, pOut, u4OutLen,
                                     &i4IsProxy, &i4SendFailResp);
        }
        else
        {
            /* Response for agent */
            i4IsProxy = SNMP_FALSE;
            i4Result = SNMP_SUCCESS;
        }

        /* Proxy handling has taken place */
        if ((SNMP_SUCCESS == i4Result) && (i4IsProxy == SNMP_TRUE))
        {
            return SNMP_SUCCESS;
        }
        /* Act as a agent */
        else if ((SNMP_SUCCESS == i4Result) && (i4IsProxy == SNMP_FALSE))
        {
            pCommEntry = SNMPGetCommunityEntryFromName (pPtr->pCommunityStr);
            if (pCommEntry)
            {
                pProxy =
                    (tProxyTableEntry *)
                    SNMPGetProxyEntryFromCtxtId (&pCommEntry->ContextEngineID);
                if (pProxy == NULL)
                {
                    if ((SNMPCompareOctetString (&pCommEntry->ContextEngineID,
                                                 &gSnmpEngineID) != SNMP_EQUAL))
                    {
                        pVarPtr = pPtr->pVarBindList;
                        if (pVarPtr)
                        {
                            pProxy =
                                (tPrpProxyTableEntry *)
                                SNMPGetPrpProxyEntryFromMibId (pVarPtr->
                                                               pObjName);
                        }
                    }
                }
                if (pProxy &&
                    (pPtr->i2_PduType == SNMP_PDU_TYPE_V2_INFORM_RESPONSE))
                {
                    SNMP_INFORM_LOCK ();
                    i4Result = SNMPProxyInformRespProcess ((VOID *) pPtr,
                                                           VERSION2);
                    if (i4Result == SNMP_FAILURE)
                    {
                        SNMP_INR_SILENT_DROPS;
                    }
                    SNMP_INFORM_UNLOCK ();
                    SNMPPduFree (pPtr);
                    return SNMP_FAILURE;
                }
            }

            if (SNMPGetVacmInfoFromCommMappingTable (pPtr->pCommunityStr,
                                                     pPtr->u4_Version,
                                                     &VacmInfo) == SNMP_SUCCESS)
            {
#ifdef L2RED_WANTED
                if (pPtr->i2_PduType == SNMP_PDU_TYPE_SET_REQUEST)
                {
                    if (RmGetPeerNodeCount () != 0)
                    {
                        i4BulkUpdtResult = RmIsBulkUpdateComplete ();
                        if (i4BulkUpdtResult == RM_FAILURE)
                        {
                            SNMPTrace ("\rBulk Update not yet Completed \r\n");
                            pPtr->i4_ErrorStatus = SNMP_ERR_NO_ACCESS;
                            i4ConfigAllowed = SNMP_FAILURE;
                        }
                        UNUSED_PARAM (i4BulkUpdtResult);
                    }
                }
#endif

                if (STRCMP (VacmInfo.pUserName->pu1_OctetList, "none") != 0)
                {
                    /*check if the community index and user name are associated with 
                     * an snmp v2 or v1 user*/
                    if (VACMGetSecGrp ((INT4) VacmInfo.u4SecModel,
                                       VacmInfo.pUserName) == NULL)
                    {
                        i4Result = SNMP_FAILURE;
                        pPtr->i4_ErrorStatus = SNMP_ERR_NO_ACCESS;
                    }
                    else
                    {
                        i4Result = SNMP_SUCCESS;
                    }
                }
                if ((i4ConfigAllowed != SNMP_FAILURE)
                    && (i4Result == SNMP_SUCCESS))
                {
                    MGMT_LOCK ();
                    i4Result = SNMPPDUProcess (pPtr, &VacmInfo);
                    MGMT_UNLOCK ();
                }
                if (pPtr->pVarBindList == NULL)
                {
                    /*Not to update the error status if NULL varbind is received */
                    i4Result = SNMP_SUCCESS;
                }

                /* To construct a syslog message and send it during failed,
                   SNTP GET, GETNEXT, SET, BULK cases */
                if (i4Result == SNMPDROP || i4Result == SNMP_FAILURE)
                {
                    pOid = alloc_oid (SNMP_MAX_OID_LENGTH);
                    if (pOid == NULL)
                    {
                        u1LoggingError = SNMP_LOGGING_ERROR;
                    }

                    if (u1LoggingError != SNMP_LOGGING_ERROR)
                    {
                        pu1Oid = MemAllocMemBlk (gSnmpMultiOidPoolId);
                        if (pu1Oid == NULL)
                        {
                            free_oid (pOid);
                            u1LoggingError = SNMP_LOGGING_ERROR;
                        }
                    }

                    if (u1LoggingError != SNMP_LOGGING_ERROR)
                    {
                        if (pPtr->pVarBindList->pObjName == NULL)
                        {
                            free_oid (pOid);
                            MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1Oid);
                            return SNMP_FAILURE;
                        }
                        /* Parsing the OID info */
                        SNMPUpdateEOID (pPtr->pVarBindList->pObjName, pOid,
                                        FALSE);
                        SNMPGetOidString (pOid->pu4_OidList, pOid->u4_Length,
                                          pu1Oid);

                        /* Gathering Manager IP address */
                        SNMPGetRemoteManagerIpAddr (&ManagerIpAddr);
                        if (ManagerIpAddr.u4AddrType == IPVX_ADDR_FMLY_IPV4)
                        {
                            MEMCPY (&InAddr, &(ManagerIpAddr.uIpAddr.u4Ip4Addr),
                                    sizeof (tUtlInAddr));
                            SPRINTF (au1SnmpBufMsg, "from %s",
                                     UtlInetNtoa (InAddr));
                        }
                        else if (ManagerIpAddr.u4AddrType ==
                                 IPVX_ADDR_FMLY_IPV6)
                        {
                            MEMCPY (&(In6Addr.u1addr),
                                    &(ManagerIpAddr.uIpAddr.au1Ip6Addr),
                                    sizeof (tUtlIn6Addr));
                            SPRINTF (au1SnmpBufMsg, "from %s",
                                     UtlInetNtoa6 (In6Addr));
                        }

                        /* Sending the syslog message according to the SNTP request type */
                        switch ((UINT2) pPtr->i2_PduType)
                        {
                            case SNMP_PDU_TYPE_GET_REQUEST:
                                SYS_LOG_CMD ((SYSLOG_DEBUG_LEVEL,
                                              SYSLOG_DEF_USER_INDEX,
                                              "SNMP %s GET %s %s FAILED. ERROR CODE: %d ",
                                              gi1SnmpUserName, pu1Oid,
                                              au1SnmpBufMsg,
                                              pPtr->i4_ErrorStatus));
                                break;

                            case SNMP_PDU_TYPE_GET_NEXT_REQUEST:
                                SYS_LOG_CMD ((SYSLOG_DEBUG_LEVEL,
                                              SYSLOG_DEF_USER_INDEX,
                                              "SNMP %s GETNEXT %s %s FAILED. ERROR CODE: %d",
                                              gi1SnmpUserName, pu1Oid,
                                              au1SnmpBufMsg,
                                              pPtr->i4_ErrorStatus));
                                break;

                            case SNMP_PDU_TYPE_SET_REQUEST:

                                pu1Data = MemAllocMemBlk (gSnmpDataPoolId);
                                if (pu1Data == NULL)
                                {
                                    u1LoggingError = SNMP_LOGGING_ERROR;
                                    break;
                                }

                                MEMSET (pu1Data, 0, MAX_SNMP_DATA_LENGTH);
                                SNMPConvertDataToString (&
                                                         (pPtr->pVarBindList->
                                                          ObjValue), pu1Data,
                                                         (UINT2) pPtr->
                                                         pVarBindList->ObjValue.
                                                         i2_DataType);

                                SYS_LOG_CMD ((SYSLOG_DEBUG_LEVEL,
                                              SYSLOG_DEF_USER_INDEX,
                                              "SNMP %s SET %s value %s %s FAILED. ERROR CODE: %d",
                                              gi1SnmpUserName, pu1Oid, pu1Data,
                                              au1SnmpBufMsg,
                                              pPtr->i4_ErrorStatus));

                                MemReleaseMemBlock (gSnmpDataPoolId, pu1Data);

                                break;

                            case SNMP_PDU_TYPE_GET_BULK_REQUEST:
                                SYS_LOG_CMD ((SYSLOG_DEBUG_LEVEL,
                                              SYSLOG_DEF_USER_INDEX,
                                              "SNMP %s BULK %s %s FAILED. ERROR CODE: %d",
                                              gi1SnmpUserName, pu1Oid,
                                              au1SnmpBufMsg,
                                              pPtr->i4_ErrorStatus));
                                break;

                            default:
                                break;
                        }

                        free_oid (pOid);
                        MemReleaseMemBlock (gSnmpMultiOidPoolId, pu1Oid);
                    }
                }
                if (i4Result == SNMPDROP)
                {
                    SNMPPduFree (pPtr);
                    SNMP_INR_SILENT_DROPS;
                    return SNMP_FAILURE;
                }

                if (pPtr->i2_PduType == SNMP_PDU_TYPE_V2_INFORM_RESPONSE)
                {
                    SNMPPduFree (pPtr);
                    return SNMP_FAILURE;
                }
                if (i4Result == SNMP_FAILURE)
                {
                    /*--------------------------------------- 
                     * In this case Error Pdu is to be sent
                     * hence copy the error status and index at local variable
                     * decode original packet 
                     * and update the error status and error index
                     * ---------------------------------*/
                    u4Err = (UINT4) pPtr->i4_ErrorStatus;
                    i4Ind = (UINT4) pPtr->i4_ErrorIndex;
                    SNMPPduFree (pPtr);
                    pPtr =
                        (tSNMP_NORMAL_PDU *) (VOID *) SNMPDecodePacket (pData,
                                                                        (INT4)
                                                                        u4Length,
                                                                        &i2_PduType);
                    if (pPtr == NULL)
                    {
                        SNMP_INR_SILENT_DROPS;
                        return SNMP_FAILURE;
                    }
                    pPtr->i4_ErrorStatus = (INT4) u4Err;
                    pPtr->i4_ErrorIndex = (INT4) i4Ind;

                }
            }
            else                /* If community check fails */
            {
                if (pPtr->i2_PduType == SNMP_PDU_TYPE_V2_INFORM_RESPONSE)
                {
                    SNMPPduFree (pPtr);
                    return SNMP_FAILURE;
                }
                else
                {

                    if (gSnmpStat.i4SnmpEnableAuthTraps == AUTHTRAP_ENABLE)
                    {
                        /* SnmpSendAuthFailureTrap (); */
                        SnmpSendAuthFailureTrap (pPtr);
                    }
                    SNMPPduFree (pPtr);
                    SNMP_INR_SILENT_DROPS;
                    return SNMP_FAILURE;
                }
            }
        }                        /* End of Agent Proessing */
        /* Failure handling */
        else if (SNMP_FAILURE == i4Result)
        {
            if (i4SendFailResp == SNMP_TRUE)
            {
                u4Err = (UINT4) pPtr->i4_ErrorStatus;
                i4Ind = (UINT4) pPtr->i4_ErrorIndex;
                SNMPPduFree (pPtr);
                pPtr =
                    (tSNMP_NORMAL_PDU *) (VOID *)
                    SNMPDecodePacket (pData, (INT4) u4Length, &i2_PduType);
                if (pPtr == NULL)
                {
                    return SNMP_FAILURE;
                }
                pPtr->i4_ErrorStatus = (INT4) u4Err;
                pPtr->i4_ErrorIndex = (INT4) i4Ind;

                /* Filling the ProxyDrop OID and its value */
                pVarPtr = pPtr->pVarBindList;
                if (NULL == pVarPtr)
                {
                    SNMPPduFree (pPtr);
                    SNMP_INR_SILENT_DROPS;
                    return SNMP_FAILURE;
                }

                ProxyDropOid.u4_Length = 8;
                ProxyDropOid.pu4_OidList = ui4SnmpProxyDrops;

                SNMPCopyOid (pVarPtr->pObjName, &ProxyDropOid);

                pVarPtr->ObjValue.i2_DataType = SNMP_DATA_TYPE_COUNTER32;
                pVarPtr->ObjValue.u4_ULongValue = gSnmpStat.u4SnmpProxyDrops;
            }
            else if ((i4SendFailResp == SNMP_FALSE) &&
                     (pPtr->i4_ErrorStatus == SNMP_ERR_BAD_VALUE))
            {
                    /*--------------------------------------- 
                     * In this case Error Pdu is to be sent
                     * hence copy the error status and index at local variable
                     * decode original packet 
                     * and update the error status and error index
                     * ---------------------------------*/
                u4Err = (UINT4) pPtr->i4_ErrorStatus;
                i4Ind = (UINT4) pPtr->i4_ErrorIndex;
                SNMPPduFree (pPtr);
                pPtr =
                    (tSNMP_NORMAL_PDU *) (VOID *) SNMPDecodePacket (pData,
                                                                    (INT4)
                                                                    u4Length,
                                                                    &i2_PduType);
                if (pPtr == NULL)
                {
                    SNMP_INR_SILENT_DROPS;
                    return SNMP_FAILURE;
                }
                pPtr->i4_ErrorStatus = (INT4) u4Err;
                pPtr->i4_ErrorIndex = (INT4) i4Ind;
            }
            else
            {
                SNMPPduFree (pPtr);
                SNMP_INR_SILENT_DROPS;
                return SNMP_FAILURE;
            }
        }

        i4Result = SNMPEncodeGetPacket (pPtr, pOut, (INT4 *) u4OutLen,
                                        (UINT4) (SNMP_PDU_GET_RESPONSE_MASK &
                                                 SNMP_PDU_TYPE_GET_RESPONSE));
        if (i4Result == SNMP_NOT_OK)
        {
            i4Result = SNMP_FAILURE;
        }
        else
        {
            SNMP_INR_OUT_GET_RES_SUCCESS;
            i4Result = SNMP_SUCCESS;
        }
    }                            /* End of ErrorStatus Check */

    if (pPtr == NULL)
    {
        SNMP_INR_SILENT_DROPS;
        return SNMP_FAILURE;
    }

    /*-----This is to check whether the Packet Length is 
      less than MAX_PKT_LENGTH -------------------*/
    /* for messsages other than Inform Responses */
    if ((pPtr->i4_ErrorStatus == SNMP_ERR_TOO_BIG) &&
        (pPtr->i2_PduType != SNMP_PDU_TYPE_V2_INFORM_RESPONSE))
    {
        SNMP_INR_OUT_TOO_BIG;
        if (pPtr != NULL)
        {
            SNMPPduFree (pPtr);
        }
        pPtr =
            (tSNMP_NORMAL_PDU *) (VOID *) SNMPDecodePacket (pData,
                                                            (INT4) u4Length,
                                                            &i2_PduType);
        if (pPtr == NULL)
        {
            SNMP_INR_SILENT_DROPS;
            return SNMP_FAILURE;
        }
        pPtr->i4_ErrorStatus = SNMP_ERR_TOO_BIG;
        pPtr->pVarBindList = NULL;
        i4Result = SNMPEncodeGetPacket (pPtr, pOut, (INT4 *) u4OutLen,
                                        (UINT4) (SNMP_PDU_GET_RESPONSE_MASK &
                                                 SNMP_PDU_TYPE_GET_RESPONSE));
        if ((i4Result == SNMP_NOT_OK) || (*u4OutLen > MAX_PKT_LENGTH))
        {
            if (pPtr != NULL)
            {
                SNMPPduFree (pPtr);
            }
            SNMP_INR_SILENT_DROPS;
            return SNMP_FAILURE;
        }
        else
        {
            SNMP_INR_OUT_GET_RES_SUCCESS;
            i4Result = SNMP_SUCCESS;
        }

    }
    /* For INFORM Response Messages with Too BIG Error */
    else if ((pPtr->i4_ErrorStatus == SNMP_ERR_TOO_BIG) &&
             (pPtr->i2_PduType == SNMP_PDU_TYPE_V2_INFORM_RESPONSE))
    {
        /* Increment statistics */
        SNMP_INR_OUT_TOO_BIG;
        SNMP_INR_INFORM_RESPS;
        SNMP_INFORM_LOCK ();
        /* Always this function should return SNMP_FAILURE */
        i4Result = SNMPInformResponseProcess (pPtr);
        SNMP_INFORM_UNLOCK ();
    }

    SNMPPduFree (pPtr);
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function Exit\r\n", __func__);
    return i4Result;
}

/************************************************************************
 *  Function Name   : SNMPGetRequestID 
 *  Description     : function to return current request id. used for trap
 *  Input           : None 
 *  Output          : None
 *  Returns         : return id value
 ************************************************************************/
UINT4
SNMPGetRequestID ()
{
    /* generate a random number for request ID */
    gu4RequestID = (UINT4) RAND ();

    return gu4RequestID;
}

/************************************************************************
 *  Function Name   : SNMPSetRequestID 
 *  Description     : function to set current request id. used for trap
 *  Input           : u4RequestID - Request ID
 *  Output          : None
 *  Returns         : return id value
 ************************************************************************/
VOID
SNMPSetRequestID (UINT4 u4RequestID)
{
    gu4RequestID = u4RequestID;
    return;
}

UINT4
SNMPGetMsgID ()
{
    return gu4MsgID;
}

/************************************************************************
 *  Function Name   : SNMPV3Process 
 *  Description     : Function to decode and process the input data from
 *                    Manager  
 *  Input           : pData - Data Pointer
 *                    u4Length - Data Length
 *  Output          : pOut - Pointer contains Data to be sent to manager 
 *                    u4OutLen - Output Data Length      
 *  Returns         : SNMP_SUCCESS or SNMP_FAILURE 
 ************************************************************************/
INT4
SNMPV3Process (UINT1 *pData, UINT4 u4Length, UINT1 **pOut, UINT4 *u4OutLen)
{
    tSNMP_NORMAL_PDU   *pPtr = NULL;
    tSNMP_V3PDU         V3Pdu;
    INT4                i4Result = SNMP_FAILURE;
    UINT4               u4Err = SNMP_ERR_NO_ERROR, i4Ind = 0;
    INT4                i4PduType = SNMP_ZERO;
    tSnmpVacmInfo       VacmInfo;
    tSNMP_OCTET_STRING_TYPE *pSingleTargetOut = NULL;
    tSNMP_OCTET_STRING_TYPE *pMulTargetOut = NULL;
    INT4                i4IsProxy = SNMP_FALSE;
    INT4                i4SendFailResp = SNMP_FALSE;
    tSNMP_VAR_BIND     *pVarPtr = NULL;
    tSNMP_OID_TYPE      ProxyDropOid;
    UINT4               ui4SnmpProxyDrops[] = { 1, 3, 6, 1, 2, 1, 11, 32 };
    tContext           *pContext = NULL;
    VOID               *pProxy = NULL;
    tSNMP_PROXY_CACHE  *pCacheNode = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    UINT1              *pu1Oid = NULL;
    UINT1              *pu1Data = NULL;
    UINT1               u1LoggingError = SNMP_NO_LOGGING_ERROR;

    CHR1                au1SnmpBufMsg[SNMP_MSG_BUF_LEN];
    tUtlInAddr          InAddr;
    tUtlIn6Addr         In6Addr;
    tMgrIpAddr          ManagerIpAddr;
    INT4                i4ConfigAllowed = SNMP_SUCCESS;
#ifdef L2RED_WANTED
    INT4                i4BulkUpdtResult = RM_SUCCESS;
    UNUSED_PARAM (i4BulkUpdtResult);
#endif

    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function Entry\r\n", __func__);
    MEMSET (au1SnmpBufMsg, 0, SNMP_MSG_BUF_LEN);
    MEMSET (&InAddr, 0, sizeof (tUtlInAddr));
    MEMSET (&In6Addr, 0, sizeof (tUtlIn6Addr));
    MEMSET (&ManagerIpAddr, 0, sizeof (tMgrIpAddr));

    *pOut = NULL;
    *u4OutLen = 0;
    MEMSET (gi1SnmpUserName, 0, MAX_AUDIT_USER_NAME_LEN);
    /* Decode the SNMP V3  Packet */
    MEMSET (&V3Pdu, 0, sizeof (tSNMP_V3PDU));
    pPtr = &(V3Pdu.Normalpdu);
    i4PduType = (INT4) (SNMP_PDU_GET_RESPONSE_MASK &
                        SNMP_PDU_TYPE_GET_RESPONSE);
    MEMCPY (au1SnmpRequest, pData, u4Length);
    i4Result = SNMPDecodeV3Request (pData, (INT4) u4Length, &V3Pdu);

    /* Gathering Manager IP address */
    SNMPGetRemoteManagerIpAddr (&ManagerIpAddr);

    if (ManagerIpAddr.u4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (&InAddr, &(ManagerIpAddr.uIpAddr.u4Ip4Addr),
                sizeof (tUtlInAddr));
    }
    else if (ManagerIpAddr.u4AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        MEMCPY (&(In6Addr.u1addr), &(ManagerIpAddr.uIpAddr.au1Ip6Addr),
                sizeof (tUtlIn6Addr));
    }
    SPRINTF (au1SnmpBufMsg, "SNMP Request from %s FAILED. ERROR:",
             UtlInetNtoa (InAddr));
    if (i4Result == SNMP_FAILURE)
    {
        SYS_LOG_CMD ((SYSLOG_DEBUG_LEVEL, SYSLOG_DEF_USER_INDEX,
                      "SNMP Failed to decode the packet from %s",
                      au1SnmpBufMsg));
        SNMP_TRC1 (SNMP_FAILURE_TRC, "SNMPV3Process: "
                   "SNMP Failed to decode the packet from [%s]\r\n",
                   au1SnmpBufMsg);
        SNMP_INR_BAD_ASN_PARSE;
        return SNMP_FAILURE;
    }
    if (i4Result == SNMP_NOT_TIMEWINDOW)
    {
        if ((V3Pdu.MsgFlag.pu1_OctetList[SNMP_ZERO] & 0x04) == 0x04)
        {
            i4PduType = (INT4) (SNMP_PDU_GET_RESPONSE_MASK &
                                SNMP_PDU_TYPE_GET_REPORT);
            V3Pdu.Normalpdu.pVarBindList = SNMPAllocVarBind ();
            if (V3Pdu.Normalpdu.pVarBindList == NULL)
            {
                SNMP_TRC (SNMP_FAILURE_TRC, "SNMPV3Process: "
                          "Unable to allocate Varbinds\r\n");
                return SNMP_FAILURE;
            }
            MEMCPY (V3Pdu.Normalpdu.pVarBindList->pObjName->pu4_OidList,
                    gau4UsmTimeWindowOID, sizeof (gau4UsmTimeWindowOID));
            V3Pdu.Normalpdu.pVarBindList->pObjName->u4_Length =
                sizeof (gau4UsmTimeWindowOID) / sizeof (UINT4);
            V3Pdu.Normalpdu.pVarBindList->ObjValue.i2_DataType =
                SNMP_DATA_TYPE_COUNTER;
            V3Pdu.Normalpdu.pVarBindList->ObjValue.u4_ULongValue =
                SNMP_GET_USM_NOTINTIMEWINDOWS;
            i4Result = SNMPEncodeV3Message
                (&V3Pdu, pOut, (INT4 *) u4OutLen, i4PduType);
            SNMPPduFree (pPtr);
            return i4Result;
        }
    }
    if (i4Result == SNMP_AGENTDISCOVERY)
    {
        if ((V3Pdu.MsgFlag.pu1_OctetList[SNMP_ZERO] & 0x04) == 0x04)
        {
            i4PduType = (INT4) (SNMP_PDU_GET_RESPONSE_MASK &
                                SNMP_PDU_TYPE_GET_REPORT);
            V3Pdu.Normalpdu.pVarBindList = SNMPAllocVarBind ();
            if (V3Pdu.Normalpdu.pVarBindList == NULL)
            {
                SNMP_TRC (SNMP_FAILURE_TRC, "SNMPV3Process: "
                          "Unable to allocate Varbinds\r\n");
                return SNMP_FAILURE;
            }
            MEMCPY (V3Pdu.Normalpdu.pVarBindList->pObjName->pu4_OidList,
                    gau4UnknowEngineOID, sizeof (gau4UnknowEngineOID));
            V3Pdu.Normalpdu.pVarBindList->pObjName->u4_Length =
                sizeof (gau4UnknowEngineOID) / sizeof (UINT4);
            V3Pdu.Normalpdu.pVarBindList->ObjValue.i2_DataType =
                SNMP_DATA_TYPE_COUNTER;
            V3Pdu.Normalpdu.pVarBindList->ObjValue.u4_ULongValue =
                SNMP_GET_USM_UNKNOWNENGINEIDS;
            SNMPCopyOctetString (&(V3Pdu.MsgSecParam.MsgEngineID),
                                 &(gSnmpEngineID));
            i4Result = SNMPEncodeV3Message
                (&V3Pdu, pOut, (INT4 *) u4OutLen, i4PduType);
            SNMPPduFree (pPtr);
            return i4Result;
        }
        return SNMP_FAILURE;
    }
    if (i4Result == SNMP_WRONGDIGESTS)
    {
        SYS_LOG_CMD ((SYSLOG_DEBUG_LEVEL, SYSLOG_DEF_USER_INDEX,
                      "%s Authentication failure "
                      "(incorrect password, community or key) for %s",
                      au1SnmpBufMsg, V3Pdu.MsgSecParam.MsgUsrName));
        SNMP_TRC2 (SNMP_FAILURE_TRC,
                   "SNMPV3Process: [%s] Authentication failure "
                   "(incorrect password, community or key) for [%s]\r\n",
                   au1SnmpBufMsg, V3Pdu.MsgSecParam.MsgUsrName);
        i4Result = SNMPWrongDigestReport (V3Pdu, pOut, u4OutLen);
        SNMPPduFree (pPtr);
        return i4Result;
    }

    if (i4Result == SNMP_UNKNOWNUSERNAME)
    {
        SYS_LOG_CMD ((SYSLOG_DEBUG_LEVEL, SYSLOG_DEF_USER_INDEX,
                      "%s Unknown User Name %s", au1SnmpBufMsg,
                      V3Pdu.MsgSecParam.MsgUsrName));
        SNMP_TRC2 (SNMP_FAILURE_TRC,
                   "SNMPV3Process: [%s] Unknown User Name [%s]\r\n",
                   au1SnmpBufMsg, V3Pdu.MsgSecParam.MsgUsrName);
        i4Result = SNMPUnknownUserNameReport (V3Pdu, pOut, u4OutLen);
        SNMPPduFree (pPtr);
        return i4Result;
    }

    if (i4Result == SNMP_UNSUPPORTEDSECLEVEL)
    {
        SYS_LOG_CMD ((SYSLOG_DEBUG_LEVEL, SYSLOG_DEF_USER_INDEX,
                      "%s Unsupported Security Level", au1SnmpBufMsg));
        SNMP_TRC1 (SNMP_FAILURE_TRC, "SNMPV3Process: "
                   "[%s] Unsupported Security Level\r\n", au1SnmpBufMsg);
        i4Result = SNMPUnsuppSecLevelReport (V3Pdu, pOut, u4OutLen);
        SNMPPduFree (pPtr);
        return i4Result;
    }
    if (i4Result == SNMP_UNKNOWNENGINEID)
    {
        SYS_LOG_CMD ((SYSLOG_DEBUG_LEVEL, SYSLOG_DEF_USER_INDEX,
                      "%s Invalid Engine Id", au1SnmpBufMsg));
        SNMP_TRC1 (SNMP_FAILURE_TRC, "SNMPV3Process: "
                   "[%s] Invalid Engine Id\r\n", au1SnmpBufMsg);
        i4Result = SNMPUnknownEngineidReport (V3Pdu, pOut, u4OutLen);
        SNMPPduFree (pPtr);
        return i4Result;
    }
    if (i4Result == SNMP_UNKNOWN_CONTEXT_NAME)
    {
        SYS_LOG_CMD ((SYSLOG_DEBUG_LEVEL, SYSLOG_DEF_USER_INDEX,
                      "%s Invalid Context name", au1SnmpBufMsg));
        SNMP_TRC1 (SNMP_FAILURE_TRC, "SNMPV3Process: "
                   "[%s] Invalid Context name\r\n", au1SnmpBufMsg);
        i4Result = SNMPUnknownContextReport (V3Pdu, pOut, u4OutLen);
        SNMPPduFree (pPtr);
        return i4Result;
    }
    if (i4Result == SNMP_DECRYPTIONERRORS)
    {
        SYS_LOG_CMD ((SYSLOG_DEBUG_LEVEL, SYSLOG_DEF_USER_INDEX,
                      "%s Decryption error", au1SnmpBufMsg));
        SNMP_TRC1 (SNMP_FAILURE_TRC, "SNMPV3Process: "
                   "[%s] Decryption error\r\n", au1SnmpBufMsg);
        i4Result = SNMPDecryptionErrorReport (V3Pdu, pOut, u4OutLen);
        SNMPPduFree (pPtr);
        return i4Result;
    }
    if (i4Result == SNMPDROP)
    {
        SNMPPduFree (pPtr);
        SNMP_INR_SILENT_DROPS;
        return SNMP_FAILURE;
    }

    if (pPtr->i4_ErrorStatus != SNMP_ERR_TOO_BIG)
    {
        if (pPtr->u4_Version != VERSION3)
        {
            SNMP_INR_BAD_VERSION;
            SNMP_INR_SILENT_DROPS;
            SNMP_TRC1 (SNMP_FAILURE_TRC, "SNMPV3Process: "
                       "Version[%d] error\r\n", pPtr->u4_Version);
            SNMPPduFree (pPtr);
            return SNMP_FAILURE;
        }

        SNMPSetRequestID ((UINT4) pPtr->u4_RequestID);

        /* If Incoming message is response then Match with the cache and 
         * check if the response is to be handled as a proxy
         */
        if (pPtr->i2_PduType == SNMP_PDU_TYPE_GET_RESPONSE)
        {
            pCacheNode = SNMPProxySearchNode (pPtr->u4_RequestID);
            if (NULL != pCacheNode)
            {
                SNMP_TRC (SNMP_DEBUG_TRC, "SNMPV3Process: "
                          "[PROXY]:Cache Node Found\r\n");
                SNMP_INR_GET_RESPONSE;
                /* Stop timer */
                TmrStopTimer (SnmpPxyTimerListId,
                              &pCacheNode->ProxyCmnCache.CacheTmr);
                if (SNMP_FAILURE ==
                    SNMPProxyHandler ((VOID *) &V3Pdu, pPtr->u4_Version,
                                      pPtr->i2_PduType, NULL, pCacheNode, pOut,
                                      u4OutLen))
                {
                    SNMP_INR_SILENT_DROPS;
                    SNMPPduFree (pPtr);
                    return SNMP_FAILURE;
                }
                SNMPPduFree (pPtr);
                return SNMP_SUCCESS;
            }
            else
            {
                SNMP_TRC (SNMP_FAILURE_TRC, "SNMPV3Process: "
                          "[PROXY]:Cache Node not Found\r\n");
            }
        }

        /* The condition will work for GET_RESPONSE also because
           GET_RESPONSE and INFORM_RESPONSE are same */
        if ((pPtr->i2_PduType != SNMP_PDU_TYPE_V2_INFORM_RESPONSE) &&
            (pPtr->i2_PduType != SNMP_PDU_TYPE_GET_REPORT))
        {
            /* Check whether to act as a Proxy or as an agent */
            i4Result = SNMPIsProxyOrAgent ((VOID *) &V3Pdu,
                                           (INT4) pPtr->u4_Version,
                                           pPtr->i2_PduType,
                                           &pSingleTargetOut,
                                           &pMulTargetOut,
                                           &i4IsProxy, &i4SendFailResp);
        }
        else
        {
            i4Result = SNMP_SUCCESS;
            /* Hit the Normal Agent Behavior */
            i4IsProxy = SNMP_FALSE;
        }

        if (i4Result == SNMP_SUCCESS)
        {
            if (i4IsProxy == SNMP_TRUE)    /* Act as a Proxy */
            {
                if ((pPtr->i2_PduType == SNMP_PDU_TYPE_V2_INFORM_REQUEST) ||
                    (pPtr->i2_PduType == SNMP_PDU_TYPE_SNMPV2_TRAP))
                {
                    i4Result = SNMPProxyNotifyHandle ((VOID *) &V3Pdu,
                                                      VERSION3, pMulTargetOut,
                                                      SNMPProxyInformCallback);
                    SNMPPduFree (pPtr);
                    return SNMP_FAILURE;
                }
                /* Translation of one SNMP version to other SNMP version */
                else if (SNMP_FAILURE == SNMPProxyHandler ((VOID *) &V3Pdu,
                                                           pPtr->u4_Version,
                                                           pPtr->i2_PduType,
                                                           pSingleTargetOut,
                                                           NULL, pOut,
                                                           u4OutLen))
                {
                    /* Counter to drop the message */
                    SNMP_INR_SILENT_DROPS;
                    SNMPPduFree (pPtr);
                    return SNMP_FAILURE;
                }

                SNMPPduFree (pPtr);
                return SNMP_SUCCESS;
            }
            else                /* Act as an Agent */
            {
                pProxy = SNMPGetProxyEntryFromCtxtId (&(V3Pdu.ContextID));
                if (pProxy == NULL)
                {
                    if ((SNMPCompareOctetString (&V3Pdu.ContextID,
                                                 &gSnmpEngineID) != SNMP_EQUAL))
                    {
                        pVarPtr = pPtr->pVarBindList;
                        if (pVarPtr)
                        {
                            pProxy = (tPrpProxyTableEntry *)
                                SNMPGetPrpProxyEntryFromMibId (pVarPtr->
                                                               pObjName);
                        }
                    }
                }

                if (pProxy && ((pPtr->i2_PduType ==
                                SNMP_PDU_TYPE_V2_INFORM_RESPONSE) ||
                               (pPtr->i2_PduType == SNMP_PDU_TYPE_GET_REPORT)))
                {
                    SNMP_INR_INFORM_RESPS;
                    SNMP_INFORM_LOCK ();
                    i4Result = SNMPProxyInformRespProcess ((VOID *) &V3Pdu,
                                                           VERSION3);
                    if (i4Result == SNMP_FAILURE)
                    {
                        SNMP_INR_SILENT_DROPS;
                    }
                    SNMP_INFORM_UNLOCK ();
                    SNMPPduFree (pPtr);
                    return SNMP_FAILURE;
                }
                else
                {
                    if (SNMPCompareOctetString (&(V3Pdu.ContextID),
                                                &gSnmpEngineID) != SNMP_EQUAL)
                    {
                        /* it should actually be compared against ContextEngineId
                         * But we currently support only single Context
                         * So we take SnmpEngineId as default EngineId
                         */
                        SNMP_TRC (SNMP_FAILURE_TRC, "SNMPV3Process: "
                                  "SNMP V3 Context ID Matching Failed\r\n");
                        i4Result = SNMPDROP;
                    }
                    if (V3Pdu.ContextName.i4_Length != SNMP_ZERO)
                    {
                        pContext = VACMGetContext (&(V3Pdu.ContextName));
                        if (pContext == NULL)
                        {
                            SNMP_TRC (SNMP_FAILURE_TRC, "SNMPV3Process: "
                                      "SNMP V3 Context Name  Matching Failed\r\n");
                            i4Result = SNMPDROP;
                        }
                    }
                    if (i4Result == SNMPDROP)
                    {
                        SNMPPduFree (pPtr);
                        SNMP_INR_SILENT_DROPS;
                        return SNMP_FAILURE;
                    }

                    /* Change the Version to SNMP V2 */
                    pPtr->u4_Version = VERSION2;
                    gu4MsgID = V3Pdu.u4MsgID;

                    if (pPtr->pVarBindList != NULL)
                    {
                        VacmInfo.pUserName = &(V3Pdu.MsgSecParam.MsgUsrName);
                        MEMCPY (gi1SnmpUserName,
                                VacmInfo.pUserName->pu1_OctetList,
                                VacmInfo.pUserName->i4_Length);

                        VacmInfo.pContextName = &(V3Pdu.ContextName);
                        VacmInfo.u4SecModel = SNMP_USM;
                        if (V3Pdu.MsgFlag.pu1_OctetList[SNMP_ZERO] == 0x05)
                        {
                            /* Minimun security should be None or Authenticated */
                            if ((gu4SnmpMinimumSecurity == SNMP_MIN_SEC_AUTH) ||
                                (gu4SnmpMinimumSecurity == SNMP_MIN_SEC_NONE))
                            {
                                VacmInfo.u4SecLevel = SNMP_AUTH_NOPRIV;
                            }
                            else
                            {
                                SNMPPduFree (pPtr);
                                SNMP_INR_SILENT_DROPS;
                                return SNMP_FAILURE;
                            }
                        }
                        else if (V3Pdu.MsgFlag.pu1_OctetList[SNMP_ZERO] == 0x07)
                        {
                            /* Authentication and Encryption are present */
                            VacmInfo.u4SecLevel = SNMP_AUTH_PRIV;
                        }
                        else
                        {
                            /* Minimun security should be None */
                            if (gu4SnmpMinimumSecurity == SNMP_MIN_SEC_NONE)
                            {
                                VacmInfo.u4SecLevel = SNMP_NOAUTH_NOPRIV;
                            }
                            else
                            {
                                SNMPPduFree (pPtr);
                                SNMP_INR_SILENT_DROPS;
                                return SNMP_FAILURE;
                            }
                        }
#ifdef L2RED_WANTED
                        if (pPtr->i2_PduType == SNMP_PDU_TYPE_SET_REQUEST)
                        {
                            if (RmGetPeerNodeCount () != 0)
                            {
                                i4BulkUpdtResult = RmIsBulkUpdateComplete ();
                                if (i4BulkUpdtResult == RM_FAILURE)
                                {
                                    SNMP_TRC (SNMP_FAILURE_TRC,
                                              "SNMPV3Process: "
                                              "Overall Bulk Update and MBSM"
                                              " NP Programming is not yet Completed\r\n");
                                    pPtr->i4_ErrorStatus =
                                        SNMP_ERR_PROP_BULK_INPROGRESS;
                                    i4ConfigAllowed = SNMP_FAILURE;
                                }
                            }
                        }
#endif
                        if ((i4ConfigAllowed != SNMP_FAILURE) &&
                            (VACMGetSecGrp
                             (SNMP_SECMODEL_V3, VacmInfo.pUserName) != NULL))
                        {
                            MGMT_LOCK ();
                            i4Result = SNMPPDUProcess (pPtr, &VacmInfo);
                            MGMT_UNLOCK ();
                        }
                        else if ((VACMGetSecGrp
                                  (SNMP_SECMODEL_V3,
                                   VacmInfo.pUserName) == NULL))
                        {
                            i4Result = SNMP_FAILURE;
                            pPtr->i4_ErrorStatus = SNMP_ERR_NO_ACCESS;
                        }

                        /* To construct a syslog message and send it during failed,
                           SNTP GET, GETNEXT, SET, BULK cases */
                        if (i4Result == SNMPDROP || i4Result == SNMP_FAILURE)
                        {
                            pOid = alloc_oid (SNMP_MAX_OID_LENGTH);
                            if (pOid == NULL)
                            {
                                u1LoggingError = SNMP_LOGGING_ERROR;
                            }

                            if (u1LoggingError != SNMP_LOGGING_ERROR)
                            {
                                pu1Oid = MemAllocMemBlk (gSnmpMultiOidPoolId);
                                if (pu1Oid == NULL)
                                {
                                    free_oid (pOid);
                                    u1LoggingError = SNMP_LOGGING_ERROR;
                                }
                            }

                            if (u1LoggingError != SNMP_LOGGING_ERROR)
                            {
                                if (pPtr->pVarBindList->pObjName == NULL)
                                {
                                    free_oid (pOid);
                                    MemReleaseMemBlock (gSnmpMultiOidPoolId,
                                                        pu1Oid);
                                    return SNMP_FAILURE;
                                }
                                /* Parsing the OID info */
                                SNMPUpdateEOID (pPtr->pVarBindList->pObjName,
                                                pOid, FALSE);
                                SNMPGetOidString (pOid->pu4_OidList,
                                                  pOid->u4_Length, pu1Oid);

                                /* Gathering Manager IP address */
                                SNMPGetRemoteManagerIpAddr (&ManagerIpAddr);
                                if (ManagerIpAddr.u4AddrType ==
                                    IPVX_ADDR_FMLY_IPV4)
                                {
                                    MEMCPY (&InAddr,
                                            &(ManagerIpAddr.uIpAddr.u4Ip4Addr),
                                            sizeof (tUtlInAddr));
                                    SPRINTF (au1SnmpBufMsg, "from %s",
                                             UtlInetNtoa (InAddr));
                                }
                                else if (ManagerIpAddr.u4AddrType ==
                                         IPVX_ADDR_FMLY_IPV6)
                                {
                                    MEMCPY (&(In6Addr.u1addr),
                                            &(ManagerIpAddr.uIpAddr.au1Ip6Addr),
                                            sizeof (tUtlIn6Addr));
                                    SPRINTF (au1SnmpBufMsg, "from %s",
                                             UtlInetNtoa6 (In6Addr));
                                }

                                /* Sending the syslog message according to the SNTP request type */
                                switch (pPtr->i2_PduType)
                                {
                                    case SNMP_PDU_TYPE_GET_REQUEST:
                                        SYS_LOG_CMD ((SYSLOG_DEBUG_LEVEL,
                                                      SYSLOG_DEF_USER_INDEX,
                                                      "SNMP %s GET %s %s FAILED. ERROR CODE: %d ",
                                                      gi1SnmpUserName, pu1Oid,
                                                      au1SnmpBufMsg,
                                                      pPtr->i4_ErrorStatus));
                                        break;

                                    case SNMP_PDU_TYPE_GET_NEXT_REQUEST:
                                        SYS_LOG_CMD ((SYSLOG_DEBUG_LEVEL,
                                                      SYSLOG_DEF_USER_INDEX,
                                                      "SNMP %s GETNEXT %s %s FAILED. ERROR CODE: %d",
                                                      gi1SnmpUserName, pu1Oid,
                                                      au1SnmpBufMsg,
                                                      pPtr->i4_ErrorStatus));
                                        break;

                                    case SNMP_PDU_TYPE_SET_REQUEST:

                                        pu1Data =
                                            MemAllocMemBlk (gSnmpDataPoolId);
                                        if (pu1Data == NULL)
                                        {
                                            u1LoggingError = SNMP_LOGGING_ERROR;
                                            break;
                                        }

                                        MEMSET (pu1Data, 0,
                                                MAX_SNMP_DATA_LENGTH);
                                        SNMPConvertDataToString (&
                                                                 (pPtr->
                                                                  pVarBindList->
                                                                  ObjValue),
                                                                 pu1Data,
                                                                 (UINT2) pPtr->
                                                                 pVarBindList->
                                                                 ObjValue.
                                                                 i2_DataType);

                                        SYS_LOG_CMD ((SYSLOG_DEBUG_LEVEL,
                                                      SYSLOG_DEF_USER_INDEX,
                                                      "SNMP %s SET %s value %s %s FAILED. ERROR CODE: %d",
                                                      gi1SnmpUserName, pu1Oid,
                                                      pu1Data, au1SnmpBufMsg,
                                                      pPtr->i4_ErrorStatus));

                                        MemReleaseMemBlock (gSnmpDataPoolId,
                                                            pu1Data);

                                        break;

                                    case SNMP_PDU_TYPE_GET_BULK_REQUEST:
                                        SYS_LOG_CMD ((SYSLOG_DEBUG_LEVEL,
                                                      SYSLOG_DEF_USER_INDEX,
                                                      "SNMP %s BULK %s %s FAILED. ERROR CODE: %d",
                                                      gi1SnmpUserName, pu1Oid,
                                                      au1SnmpBufMsg,
                                                      pPtr->i4_ErrorStatus));
                                        break;

                                    default:
                                        break;
                                }

                                free_oid (pOid);
                                MemReleaseMemBlock (gSnmpMultiOidPoolId,
                                                    pu1Oid);
                            }
                        }

                    }
                }
                if (i4Result == SNMPDROP)
                {
                    SNMPPduFree (pPtr);
                    SNMP_INR_SILENT_DROPS;
                    return SNMP_FAILURE;
                }
                if (i4Result == SNMP_FAILURE)
                {
                    u4Err = (UINT4) pPtr->i4_ErrorStatus;
                    i4Ind = (UINT4) pPtr->i4_ErrorIndex;
                    SNMPPduFree (pPtr);
                    MEMCPY (pData, au1SnmpRequest, u4Length);
                    if (SNMPDecodeV3Request (pData, (INT4) u4Length, &V3Pdu)
                        == SNMP_FAILURE)
                    {
                        SNMP_INR_SILENT_DROPS;
                        return SNMP_FAILURE;
                    }
                    pPtr->i4_ErrorStatus = (INT4) u4Err;
                    pPtr->i4_ErrorIndex = (INT4) i4Ind;
                }
            }
        }
        else
        {
            if (i4SendFailResp == SNMP_TRUE)
            {
                u4Err = (UINT4) pPtr->i4_ErrorStatus;
                i4Ind = (UINT4) pPtr->i4_ErrorIndex;
                SNMPPduFree (pPtr);
                MEMCPY (pData, au1SnmpRequest, u4Length);
                if (SNMPDecodeV3Request (pData, (INT4) u4Length, &V3Pdu)
                    == SNMP_FAILURE)
                {
                    return SNMP_FAILURE;
                }
                pPtr->i4_ErrorStatus = (INT4) u4Err;
                pPtr->i4_ErrorIndex = (INT4) i4Ind;

                /* Filling the ProxyDrop OID and its value */
                pVarPtr = pPtr->pVarBindList;

                if (NULL == pVarPtr)
                {
                    SNMPPduFree (pPtr);
                    SNMP_INR_SILENT_DROPS;
                    return SNMP_FAILURE;
                }

                ProxyDropOid.u4_Length = 8;
                ProxyDropOid.pu4_OidList = ui4SnmpProxyDrops;

                SNMPCopyOid (pVarPtr->pObjName, &ProxyDropOid);

                pVarPtr->ObjValue.i2_DataType = SNMP_DATA_TYPE_COUNTER32;
                pVarPtr->ObjValue.u4_ULongValue = gSnmpStat.u4SnmpProxyDrops;
            }
            else
            {
                SNMPPduFree (pPtr);
                SNMP_INR_SILENT_DROPS;
                return SNMP_FAILURE;
            }

        }

        i4Result = SNMPEncodeV3Message (&V3Pdu, pOut, (INT4 *) u4OutLen,
                                        i4PduType);
    }

    /*-----This is to check whether the Packet Length is 
      less than MAX_PKT_LENGTH -------------------*/
    if (pPtr->i4_ErrorStatus == SNMP_ERR_TOO_BIG)
    {
        SNMP_INR_OUT_TOO_BIG;
        SNMPPduFree (pPtr);
        if (u4Length < MAX_PKT_LENGTH)    /*kloc */
            MEMCPY (pData, au1SnmpRequest, u4Length);
        if (SNMPDecodeV3Request (pData, (INT4) u4Length, &V3Pdu) ==
            SNMP_FAILURE)
        {
            SNMP_INR_SILENT_DROPS;
            SNMP_TRC (SNMP_FAILURE_TRC, "SNMPV3Process: "
                      "SNMP V3 message decoding Failed\r\n");
            return SNMP_FAILURE;
        }
        pPtr->i4_ErrorStatus = SNMP_ERR_TOO_BIG;
        pPtr->pVarBindList = NULL;
        i4Result = SNMPEncodeV3Message (&V3Pdu, pOut, (INT4 *) u4OutLen,
                                        i4PduType);

        if ((i4Result != SNMP_SUCCESS) || (*u4OutLen > MAX_PKT_LENGTH))
        {
            SNMPPduFree (pPtr);
            SNMP_INR_SILENT_DROPS;
            SNMP_TRC (SNMP_FAILURE_TRC, "SNMPV3Process: "
                      "SNMP V3 message encoding Failed\r\n");
            return SNMP_FAILURE;
        }
    }
    MEMSET (gi1SnmpUserName, 0, MAX_AUDIT_USER_NAME_LEN);
    SNMPPduFree (pPtr);
    SNMP_INR_OUT_GET_RES_SUCCESS;
    SNMP_TRC1 (SNMP_INFO_TRC, "%s: function Exit\r\n", __func__);
    return i4Result;
}

/*********************************************************************
*  Function Name : SNMPWrongDigestReport 
*  Description   : Function to send Wrong Digest Report 
*  Parameter(s)  : pV3Pdu - SNMP V3 Packet Structure
*                  ppu1DataOut - Output Bytes steam pointer
*                  *pi4PktLen - Output Byte Steam length
*                  i4TypeOfMsg - Encoding Message Type(Report)
*  Return Values : SNMP_SUCCESS or SNMP_FAILURE
*********************************************************************/

INT4
SNMPWrongDigestReport (tSNMP_V3PDU V3Pdu, UINT1 **pOut, UINT4 *u4OutLen)
{
    INT4                i4PduType = SNMP_ZERO;

    if ((V3Pdu.MsgFlag.pu1_OctetList[SNMP_ZERO] & 0x04) == 0x04)
    {
        i4PduType = (INT4) (SNMP_PDU_GET_RESPONSE_MASK &
                            SNMP_PDU_TYPE_GET_REPORT);
        V3Pdu.Normalpdu.pVarBindList = SNMPAllocVarBind ();
        if (V3Pdu.Normalpdu.pVarBindList == NULL)
        {
            SNMPTrace ("Allocation of VarBindList Failed \n");
            return SNMP_FAILURE;
        }
        MEMCPY (V3Pdu.Normalpdu.pVarBindList->pObjName->pu4_OidList,
                gau4UsmWrongDigestOID, sizeof (gau4UsmWrongDigestOID));
        V3Pdu.Normalpdu.pVarBindList->pObjName->u4_Length =
            sizeof (gau4UsmWrongDigestOID) / sizeof (UINT4);
        V3Pdu.Normalpdu.pVarBindList->ObjValue.i2_DataType =
            SNMP_DATA_TYPE_COUNTER;
        V3Pdu.Normalpdu.pVarBindList->ObjValue.u4_ULongValue =
            SNMP_GET_USM_WRONGDIGESTS;
        return (SNMPEncodeV3ReportPdu (&V3Pdu, pOut, (INT4 *)
                                       u4OutLen, i4PduType));
    }
    return SNMP_FAILURE;

}

/*********************************************************************
*  Function Name : SNMPUnknownUserNameReport 
*  Description   : Function to send Unknown User Report
*  Parameter(s)  : pV3Pdu - SNMP V3 Packet Structure
*                  ppu1DataOut - Output Bytes steam pointer
*                  *pi4PktLen - Output Byte Steam length
*                  i4TypeOfMsg - Encoding Message Type(Response/Report)
*  Return Values : SNMP_SUCCESS or SNMP_FAILURE
*********************************************************************/

INT4
SNMPUnknownUserNameReport (tSNMP_V3PDU V3Pdu, UINT1 **pOut, UINT4 *u4OutLen)
{
    INT4                i4PduType = SNMP_ZERO;

    if ((V3Pdu.MsgFlag.pu1_OctetList[SNMP_ZERO] & 0x04) == 0x04)
    {
        i4PduType = (INT4) (SNMP_PDU_GET_RESPONSE_MASK &
                            SNMP_PDU_TYPE_GET_REPORT);
        V3Pdu.Normalpdu.pVarBindList = SNMPAllocVarBind ();
        if (V3Pdu.Normalpdu.pVarBindList == NULL)
        {
            SNMPTrace ("Unable to allocate Varbinds\n");
            return SNMP_FAILURE;
        }
        MEMCPY (V3Pdu.Normalpdu.pVarBindList->pObjName->pu4_OidList,
                gau4UnknowUserNameOID, sizeof (gau4UnknowUserNameOID));
        V3Pdu.Normalpdu.pVarBindList->pObjName->u4_Length =
            sizeof (gau4UnknowUserNameOID) / sizeof (UINT4);
        V3Pdu.Normalpdu.pVarBindList->ObjValue.i2_DataType =
            SNMP_DATA_TYPE_COUNTER;
        V3Pdu.Normalpdu.pVarBindList->ObjValue.u4_ULongValue =
            SNMP_GET_USM_UNKNOWNUSERNAMES;
        return (SNMPEncodeV3ReportPdu
                (&V3Pdu, pOut, (INT4 *) u4OutLen, i4PduType));
    }
    return SNMP_FAILURE;
}

/*********************************************************************
*  Function Name : SNMPUnsuppSecLevelReport 
*  Description   : Function to send Unsupported Security Level Report
*  Parameter(s)  : pV3Pdu - SNMP V3 Packet Structure
*                  ppu1DataOut - Output Bytes steam pointer
*                  *pi4PktLen - Output Byte Steam length
*                  i4TypeOfMsg - Encoding Message Type(Response/Report)
*  Return Values : SNMP_SUCCESS or SNMP_FAILURE
*********************************************************************/

INT4
SNMPUnsuppSecLevelReport (tSNMP_V3PDU V3Pdu, UINT1 **pOut, UINT4 *u4OutLen)
{
    INT4                i4PduType = SNMP_ZERO;

    if ((V3Pdu.MsgFlag.pu1_OctetList[SNMP_ZERO] & 0x04) == 0x04)
    {
        i4PduType = (INT4) (SNMP_PDU_GET_RESPONSE_MASK &
                            SNMP_PDU_TYPE_GET_REPORT);
        V3Pdu.Normalpdu.pVarBindList = SNMPAllocVarBind ();
        if (V3Pdu.Normalpdu.pVarBindList == NULL)
        {
            SNMPTrace ("Unable to allocate Varbinds\n");
            return SNMP_FAILURE;
        }
        MEMCPY (V3Pdu.Normalpdu.pVarBindList->pObjName->pu4_OidList,
                gau4UnSuppSecLevelOID, sizeof (gau4UnSuppSecLevelOID));
        V3Pdu.Normalpdu.pVarBindList->pObjName->u4_Length =
            sizeof (gau4UnSuppSecLevelOID) / sizeof (UINT4);
        V3Pdu.Normalpdu.pVarBindList->ObjValue.i2_DataType =
            SNMP_DATA_TYPE_COUNTER;
        V3Pdu.Normalpdu.pVarBindList->ObjValue.u4_ULongValue =
            SNMP_GET_USM_UNSUPPORTSECLEVELS;
        return (SNMPEncodeV3ReportPdu
                (&V3Pdu, pOut, (INT4 *) u4OutLen, i4PduType));
    }
    return SNMP_FAILURE;

}

/*********************************************************************
*  Function Name : SNMPUnknownEngineidReport 
*  Description   : Function to send Unknown Engine ID Report
*  Parameter(s)  : pV3Pdu - SNMP V3 Packet Structure
*                  ppu1DataOut - Output Bytes steam pointer
*                  *pi4PktLen - Output Byte Steam length
*                  i4TypeOfMsg - Encoding Message Type(Response/Report)
*  Return Values : SNMP_SUCCESS or SNMP_FAILURE
*********************************************************************/
INT4
SNMPUnknownEngineidReport (tSNMP_V3PDU V3Pdu, UINT1 **pOut, UINT4 *u4OutLen)
{
    INT4                i4PduType = SNMP_ZERO;
    if ((V3Pdu.MsgFlag.pu1_OctetList[SNMP_ZERO] & 0x04) == 0x04)
    {
        i4PduType = (INT4) (SNMP_PDU_GET_RESPONSE_MASK &
                            SNMP_PDU_TYPE_GET_REPORT);
        V3Pdu.Normalpdu.pVarBindList = SNMPAllocVarBind ();
        if (V3Pdu.Normalpdu.pVarBindList == NULL)
        {
            SNMPTrace ("Unable to allocate Varbinds\n");
            return SNMP_FAILURE;
        }
        MEMCPY (V3Pdu.Normalpdu.pVarBindList->pObjName->pu4_OidList,
                gau4UnknowEngineOID, sizeof (gau4UnknowEngineOID));
        V3Pdu.Normalpdu.pVarBindList->pObjName->u4_Length =
            sizeof (gau4UnknowEngineOID) / sizeof (UINT4);
        V3Pdu.Normalpdu.pVarBindList->ObjValue.i2_DataType =
            SNMP_DATA_TYPE_COUNTER;
        V3Pdu.Normalpdu.pVarBindList->ObjValue.u4_ULongValue =
            SNMP_GET_USM_UNKNOWNENGINEIDS;
        return (SNMPEncodeV3ReportPdu
                (&V3Pdu, pOut, (INT4 *) u4OutLen, i4PduType));
    }
    return SNMP_FAILURE;
}

/*********************************************************************
*  Function Name : SNMPUnknownContextReport
*  Description   : Function to send Unknown Engine ID Report
*  Parameter(s)  : pV3Pdu - SNMP V3 Packet Structure
*                  ppu1DataOut - Output Bytes steam pointer
*                  *pi4PktLen - Output Byte Steam length
*                  i4TypeOfMsg - Encoding Message Type(Response/Report)
*  Return Values : SNMP_SUCCESS or SNMP_FAILURE
*********************************************************************/
INT4
SNMPUnknownContextReport (tSNMP_V3PDU V3Pdu, UINT1 **pOut, UINT4 *u4OutLen)
{
    INT4                i4PduType = SNMP_ZERO;
    if ((V3Pdu.MsgFlag.pu1_OctetList[SNMP_ZERO] & 0x04) == 0x04)
    {
        i4PduType = (INT4) (SNMP_PDU_GET_RESPONSE_MASK &
                            SNMP_PDU_TYPE_GET_REPORT);
        V3Pdu.Normalpdu.pVarBindList = SNMPAllocVarBind ();
        if (V3Pdu.Normalpdu.pVarBindList == NULL)
        {
            SNMPTrace ("Unable to allocate Varbinds\n");
            return SNMP_FAILURE;
        }
        MEMCPY (V3Pdu.Normalpdu.pVarBindList->pObjName->pu4_OidList,
                gau4UnknownCtxtOID, sizeof (gau4UnknownCtxtOID));
        V3Pdu.Normalpdu.pVarBindList->pObjName->u4_Length =
            sizeof (gau4UnknownCtxtOID) / sizeof (UINT4);
        V3Pdu.Normalpdu.pVarBindList->ObjValue.i2_DataType =
            SNMP_DATA_TYPE_COUNTER;
        V3Pdu.Normalpdu.pVarBindList->ObjValue.u4_ULongValue =
            SNMP_GET_USM_UNKNOWNCONTEXTS;
        return (SNMPEncodeV3ReportPdu
                (&V3Pdu, pOut, (INT4 *) u4OutLen, i4PduType));
    }
    return SNMP_FAILURE;
}

/*********************************************************************
*  Function Name : SNMPDecryptionErrorReport
*  Description   : Function to send Decryption Error Report
*  Parameter(s)  : pV3Pdu - SNMP V3 Packet Structure
*                  ppu1DataOut - Output Bytes steam pointer
*                  *pi4PktLen - Output Byte Steam length
*                  i4TypeOfMsg - Encoding Message Type(Response/Report)
*  Return Values : SNMP_SUCCESS or SNMP_FAILURE
*********************************************************************/
INT4
SNMPDecryptionErrorReport (tSNMP_V3PDU V3Pdu, UINT1 **pOut, UINT4 *u4OutLen)
{
    INT4                i4PduType = SNMP_ZERO;
    if ((V3Pdu.MsgFlag.pu1_OctetList[SNMP_ZERO] & 0x04) == 0x04)
    {
        i4PduType = (INT4) (SNMP_PDU_GET_RESPONSE_MASK &
                            SNMP_PDU_TYPE_GET_REPORT);
        V3Pdu.Normalpdu.pVarBindList = SNMPAllocVarBind ();
        if (V3Pdu.Normalpdu.pVarBindList == NULL)
        {
            SNMPTrace ("Unable to allocate Varbinds\n");
            return SNMP_FAILURE;
        }
        MEMCPY (V3Pdu.Normalpdu.pVarBindList->pObjName->pu4_OidList,
                gau4DecryptionErrorOID, sizeof (gau4DecryptionErrorOID));
        V3Pdu.Normalpdu.pVarBindList->pObjName->u4_Length =
            sizeof (gau4DecryptionErrorOID) / sizeof (UINT4);
        V3Pdu.Normalpdu.pVarBindList->ObjValue.i2_DataType =
            SNMP_DATA_TYPE_COUNTER;
        V3Pdu.Normalpdu.pVarBindList->ObjValue.u4_ULongValue =
            SNMP_GET_USM_DECRYPTIONERRORS;
        return (SNMPEncodeV3ReportPdu
                (&V3Pdu, pOut, (INT4 *) u4OutLen, i4PduType));
    }
    return SNMP_FAILURE;
}

/************************************************************************
 *  Function Name   : SNMPInformResponseProcess
 *  Description     : Function to process Responses type of INFORM
 *  Input           : pPdu - Protocol Data Unit
 *  Output          : None
 *  Returns         : SNMP_SUCCESS or SNMP_FAILURE
 ************************************************************************/
INT4
SNMPInformResponseProcess (tSNMP_NORMAL_PDU * pPdu)
{
    tSNMP_VAR_BIND     *pVarPtr = pPdu->pVarBindList;
    INT4                i4Result = SNMP_FAILURE;
    UINT4               u4IpAddr = 0;
    tInformNode        *pInformNode = NULL;
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    UINT4               u4IsCallBack = SNMP_TRUE;
    UINT4               u4CbStatus = SNMP_INFORM_ACK_RECEIVED;
    /* Callback fn must NOT be executed
     * when Inform node is removed as
     * Acknowledgement for the Inform
     * message sent is received
     */

    UNUSED_PARAM (pVarPtr);

    /* Got the Manager Address from Response is recd. */
    u4IpAddr = SNMPGetManagerAddr ();

    /* INFORM Responses cannot be Version 1 */
    if (pPdu->u4_Version == VERSION1)
        return i4Result;

    /* Check for ErrorIndex as ZERO. ErrorStatus could be NO_ERROR/TOO_BIG */
    if ((pPdu->i4_ErrorIndex == SNMP_ZERO) &&
        ((pPdu->i4_ErrorStatus == SNMP_ERR_NO_ERROR) ||
         (pPdu->i4_ErrorStatus == SNMP_ERR_TOO_BIG)))
    {
        /* Scan for the Trap table Index and Request Id in pending requests. */
        pSnmpTgtAddrEntry = SnmpGetManagerTargetAddr (u4IpAddr);

        if (pSnmpTgtAddrEntry != NULL)
        {
            /* Get the Inform Request Node */
            pInformNode =
                SnmpGetInformNode (pPdu->u4_RequestID, pSnmpTgtAddrEntry);

            if (pInformNode != NULL)
            {

                /* Compare the Var Binds. If they are matching, drop the PDU,
                 * update counters.,  */

                /* Delete the Inform Req Node */
                /* and execute CALLBACK with SNMP_INFORM_ACK_RECEIVED */
                SnmpRemoveInformNode (pSnmpTgtAddrEntry, pPdu->u4_RequestID,
                                      u4IsCallBack, u4CbStatus);

                /* Increase INFORM ACK response counter */
                SNMP_MGR_INR_INFORM_ACK (pSnmpTgtAddrEntry);

                /* Decrease Await-ACK counter */
                SNMP_DEC_INFORM_AWAIT_ACK;

                /* Decrease the Manager's AWAIT-ACK counter */
                SNMP_MGR_DEC_INFORM_AWAIT_ACK (pSnmpTgtAddrEntry);

                /* NOTE: Intentionally not setting the Return value as SNMP_SUCCESS */
            }                    /* end of if pInformNode */
        }                        /* end of if */
    }                            /* end of if */

    return i4Result;
}

/************************************************************************
 *  Function Name   : SNMPProxyInformRespProcess
 *  Description     : Function to process Responses type of V3 INFORM
 *  Input           : pV3Pdu - V3 Protocol Data Unit
 *  Output          : None
 *  Returns         : SNMP_SUCCESS or SNMP_FAILURE
 ************************************************************************/
INT4
SNMPProxyInformRespProcess (VOID *pPtr, INT4 i4pduVersion)
{
    tSNMP_VAR_BIND     *pVarPtr = NULL;
    tSNMP_NORMAL_PDU   *pPdu = NULL;
    tSNMP_V3PDU        *pV3Pdu = NULL;
    INT4                i4Result = SNMP_FAILURE;
    UINT4               u4IpAddr = 0;
    tInformNode        *pInformNode = NULL;
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    UINT4               u4IsCallBack = SNMP_TRUE;
    UINT4               u4CbStatus = SNMP_INFORM_ACK_RECEIVED;
    /* Callback fn must NOT be executed
     * when Inform node is removed as
     * Acknowledgement for the Inform
     * message sent is received
     */

    if (i4pduVersion == VERSION2)
    {
        pPdu = (tSNMP_NORMAL_PDU *) pPtr;
    }
    else if (i4pduVersion == VERSION3)
    {
        pV3Pdu = (tSNMP_V3PDU *) pPtr;
        pPdu = &(pV3Pdu->Normalpdu);
    }
    else
    {
        SNMPTrace ("[PROXY]: Invalid Version \n");
        return i4Result;
    }

    pVarPtr = pPdu->pVarBindList;

    UNUSED_PARAM (pVarPtr);

    /* Got the Manager Address from Response is recd. */
    u4IpAddr = SNMPGetManagerAddr ();

    /* INFORM Responses cannot be Version 1 */
    if (pPdu->u4_Version == VERSION1)
        return i4Result;

    /* If Report then we will ignore it unless and until it comes with an
       error status, in which case we have to remove the Inform Node */
    if ((pPdu->i2_PduType != SNMP_PDU_TYPE_GET_REPORT) ||
        ((pPdu->i2_PduType == SNMP_PDU_TYPE_GET_REPORT) &&
         (pPdu->i4_ErrorStatus != SNMP_ERR_NO_ERROR)))
    {
        /* Scan for the Trap table Index and Request Id in pending requests. */
        pSnmpTgtAddrEntry = SnmpGetManagerTargetAddr (u4IpAddr);

        if (pSnmpTgtAddrEntry != NULL)
        {
            /* Get the Inform Request Node */
            pInformNode =
                SnmpGetInformNode (pPdu->u4_RequestID, pSnmpTgtAddrEntry);

            if (pInformNode != NULL)
            {

                /*Response is inform response */
                SNMP_INR_INFORM_RESPS;

                /* Compare the Var Binds. If they are matching, drop the PDU,
                 * update counters.,  */

                /* Delete the Inform Req Node */
                /* and execute CALLBACK with SNMP_INFORM_ACK_RECEIVED */
                i4Result = SnmpProxyRemoveInformNode (pSnmpTgtAddrEntry,
                                                      pPdu->u4_RequestID,
                                                      pPdu, u4IsCallBack,
                                                      u4CbStatus);

                /* Increase INFORM ACK response counter */
                SNMP_MGR_INR_INFORM_ACK (pSnmpTgtAddrEntry);

                /* Decrease Await-ACK counter */
                SNMP_DEC_INFORM_AWAIT_ACK;

                /* Decrease the Manager's AWAIT-ACK counter */
                SNMP_MGR_DEC_INFORM_AWAIT_ACK (pSnmpTgtAddrEntry);

                /* NOTE: Intentionally not setting the Return value as SNMP_SUCCESS */
            }                    /* end of if pInformNode */
        }                        /* end of if */
    }                            /* end of if */

    return i4Result;
}

/************************************************************************
 *  Function Name   : SNMPProxyPduProcess 
 *  Description     : Function handles and process the input data from
 *                    Manager  
 *  Returns         : SNMP_SUCCESS or SNMP_FAILURE 
 ************************************************************************/
INT4
SNMPProxyPduProcess (INT2 i2_PduType,
                     VOID *pPtr,
                     UINT1 **pOut,
                     UINT4 *pOutLen, INT4 *pIsProxy, INT4 *pSendFailureResp)
{
    tSNMP_PROXY_CACHE  *pCacheNode = NULL;
    tSNMP_NORMAL_PDU   *pNormalPdu = NULL;
    tSNMP_TRAP_PDU     *pTrapPdu = NULL;
    tSNMP_OCTET_STRING_TYPE *pSingleTargetOut = NULL;
    tSNMP_OCTET_STRING_TYPE *pMulTargetOut = NULL;
    INT4                i4Result = SNMP_FAILURE;

    if ((NULL == pPtr) || (NULL == pOut) || (NULL == pOutLen))
        return SNMP_FAILURE;

    switch (i2_PduType)
    {
        case SNMP_PDU_TYPE_GET_RESPONSE:
        {
            pNormalPdu = (tSNMP_NORMAL_PDU *) pPtr;

            pCacheNode = SNMPProxySearchNode (pNormalPdu->u4_RequestID);
            if (NULL != pCacheNode)
            {
                SNMPTrace ("[PROXY]:Cache Node Found \n");
                *pIsProxy = SNMP_TRUE;
                SNMP_INR_GET_RESPONSE;

                /* Stop timer */
                TmrStopTimer (SnmpPxyTimerListId,
                              &pCacheNode->ProxyCmnCache.CacheTmr);

                if (SNMP_FAILURE == SNMPProxyHandler (pPtr,
                                                      pNormalPdu->u4_Version,
                                                      i2_PduType,
                                                      NULL,
                                                      pCacheNode,
                                                      pOut, pOutLen))
                {
                    /* Counter to drop the message */
                    SNMP_INR_SILENT_DROPS;
                    SNMPPduFree (pNormalPdu);
                    return SNMP_FAILURE;
                }

                SNMPPduFree (pPtr);
                return SNMP_SUCCESS;
            }
            else
            {
                SNMPTrace ("[PROXY]:Cache Node not Found \n");
                *pIsProxy = SNMP_FALSE;
                return SNMP_SUCCESS;
            }
        }
        case SNMP_PDU_TYPE_TRAP:
        {
            pTrapPdu = (tSNMP_TRAP_PDU *) pPtr;

            if (SNMP_SUCCESS == SNMPIsProxyOrAgent (pPtr,
                                                    (INT4) pTrapPdu->u4_Version,
                                                    i2_PduType,
                                                    &pSingleTargetOut,
                                                    &pMulTargetOut,
                                                    pIsProxy, pSendFailureResp))
            {
                /* If Incoming Request is Trap then Proxy it to the 
                 * Targets
                 */
                i4Result = SNMPProxyNotifyHandle ((VOID *) pTrapPdu,
                                                  VERSION1, pMulTargetOut,
                                                  SNMPProxyInformCallback);
            }
            else
            {
                SNMP_INR_SILENT_DROPS;
            }

            SNMPPduFree (pPtr);
            return SNMP_FAILURE;
        }
        default:
        {
            pNormalPdu = (tSNMP_NORMAL_PDU *) pPtr;

            if (i2_PduType != SNMP_PDU_TYPE_V2_INFORM_RESPONSE)
            {
                /* Check whether to act as a Proxy or as an agent */
                i4Result = SNMPIsProxyOrAgent (pPtr,
                                               (INT4) pNormalPdu->u4_Version,
                                               i2_PduType,
                                               &pSingleTargetOut,
                                               &pMulTargetOut,
                                               pIsProxy, pSendFailureResp);
            }
            else
            {
                i4Result = SNMP_SUCCESS;
                /* Hit the Normal Agent Behavior */
                *pIsProxy = SNMP_FALSE;
            }

            /* Act as a Proxy */
            if ((i4Result == SNMP_SUCCESS) && (SNMP_TRUE == *pIsProxy))
            {
                if ((i2_PduType == SNMP_PDU_TYPE_SNMPV2_TRAP) ||
                    (i2_PduType == SNMP_PDU_TYPE_V2_INFORM_REQUEST))
                {
                    i4Result =
                        SNMPProxyNotifyHandle (pPtr, VERSION2, pMulTargetOut,
                                               SNMPProxyInformCallback);

                    SNMPPduFree (pPtr);
                    /* Return Failure intentionally */
                    return SNMP_FAILURE;
                }
                /* Translation of one SNMP version to other SNMP version */
                else
                {
                    if (SNMP_FAILURE == SNMPProxyHandler (pPtr,
                                                          pNormalPdu->
                                                          u4_Version,
                                                          i2_PduType,
                                                          pSingleTargetOut,
                                                          NULL, pOut, pOutLen))
                    {
                        SNMP_INR_SILENT_DROPS;
                        SNMPPduFree (pPtr);
                        return SNMP_FAILURE;
                    }

                    SNMPPduFree (pPtr);
                    return SNMP_SUCCESS;
                }
            }

            return i4Result;
        }                        /* default */
    }                            /* switch */
}

/********************  END OF FILE   ***************************************/
