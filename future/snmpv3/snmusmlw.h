/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: snmusmlw.h,v 1.6 2015/04/28 12:35:03 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

#ifndef _SNMUSMLW_H
#define _SNMUSMLW_H

#define SNMP_STD_AUTH_OID_LEN 9
#define SNMP_STD_PRIV_OID_LEN 9
#define SNMP_STD_USM_OID_LEN 11
#define MAX_UNIT1_VALUE       255
#define USM_MAX_AUTHN_SIZE        32

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetUsmStatsUnsupportedSecLevels ARG_LIST((UINT4 *));

INT1
nmhGetUsmStatsNotInTimeWindows ARG_LIST((UINT4 *));

INT1
nmhGetUsmStatsUnknownUserNames ARG_LIST((UINT4 *));

INT1
nmhGetUsmStatsUnknownEngineIDs ARG_LIST((UINT4 *));

INT1
nmhGetUsmStatsWrongDigests ARG_LIST((UINT4 *));

INT1
nmhGetUsmStatsDecryptionErrors ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetUsmUserSpinLock ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetUsmUserSpinLock ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2UsmUserSpinLock ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2UsmUserSpinLock ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for UsmUserTable. */
INT1
nmhValidateIndexInstanceUsmUserTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for UsmUserTable  */

INT1
nmhGetFirstIndexUsmUserTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexUsmUserTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetUsmUserSecurityName ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetUsmUserCloneFrom ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,tSNMP_OID_TYPE * ));

INT1
nmhGetUsmUserAuthProtocol ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,tSNMP_OID_TYPE * ));

INT1
nmhGetUsmUserAuthKeyChange ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetUsmUserOwnAuthKeyChange ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetUsmUserPrivProtocol ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,tSNMP_OID_TYPE * ));

INT1
nmhGetUsmUserPrivKeyChange ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetUsmUserOwnPrivKeyChange ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetUsmUserPublic ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetUsmUserStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetUsmUserStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetUsmUserCloneFrom ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OID_TYPE *));

INT1
nmhSetUsmUserAuthProtocol ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OID_TYPE *));

INT1
nmhSetUsmUserAuthKeyChange ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetUsmUserOwnAuthKeyChange ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetUsmUserPrivProtocol ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OID_TYPE *));

INT1
nmhSetUsmUserPrivKeyChange ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetUsmUserOwnPrivKeyChange ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetUsmUserPublic ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetUsmUserStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetUsmUserStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2UsmUserCloneFrom ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OID_TYPE *));

INT1
nmhTestv2UsmUserAuthProtocol ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OID_TYPE *));

INT1
nmhTestv2UsmUserAuthKeyChange ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2UsmUserOwnAuthKeyChange ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2UsmUserPrivProtocol ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OID_TYPE *));

INT1
nmhTestv2UsmUserPrivKeyChange ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2UsmUserOwnPrivKeyChange ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2UsmUserPublic ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2UsmUserStorageType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2UsmUserStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2UsmUserTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
#endif /* _SNMUSMLW_H */
