/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: snmpmsg.h,v 1.4 2015/04/28 12:35:02 siva Exp $
 *
 * Description: Proto types and macros for SNMP Message Process 
 *******************************************************************/
#ifndef _SNMPMSG_H
#define _SNMPMSG_H

UINT4 gu4RequestID = SNMP_ZERO;
UINT4 gu4MsgID = SNMP_ZERO;
UINT4 gau4UnSuppSecLevelOID[] = {1,3,6,1,6,3,15,1,1,1,0};
UINT4 gau4UsmTimeWindowOID[]  = {1,3,6,1,6,3,15,1,1,2,0};
UINT4 gau4UnknowUserNameOID[] = {1,3,6,1,6,3,15,1,1,3,0};
UINT4 gau4UnknowEngineOID[]   = {1,3,6,1,6,3,15,1,1,4,0};
UINT4 gau4UsmWrongDigestOID[] = {1,3,6,1,6,3,15,1,1,5,0};
UINT4 gau4UnknownCtxtOID[]    = {1,3,6,1,6,3,12,1,5,0};
UINT4 gau4DecryptionErrorOID[]    = {1,3,6,1,6,3,15,1,1,6,0};

UINT1 au1SnmpRequest[MAX_PKT_LENGTH];
VOID SNMPSetRequestID (UINT4);
INT4 SNMPWrongDigestReport (tSNMP_V3PDU, UINT1 **, UINT4 *);
INT4 SNMPUnknownUserNameReport (tSNMP_V3PDU, UINT1 **, UINT4 *);
INT4 SNMPUnknownEngineidReport (tSNMP_V3PDU, UINT1 **, UINT4 *);
INT4 SNMPNotInTimeWindowReport (tSNMP_V3PDU, UINT1 **, UINT4 *);
INT4 SNMPUnsuppSecLevelReport  (tSNMP_V3PDU, UINT1 **, UINT4 *);
INT4 SNMPUnknownContextReport (tSNMP_V3PDU, UINT1 **, UINT4 *);
INT4 SNMPDecryptionErrorReport (tSNMP_V3PDU, UINT1 **, UINT4 *);
INT4 SNMPProxyPduProcess(
        INT2                i2_PduType,
		VOID                *pPtr,
        UINT1               **pOut,
        UINT4               *pOutLen,
        INT4                *pIsProxy,
        INT4                *pSendFailureResp);


#endif /* _SNMPMSG_H */
