/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: snmmpdlw.c,v 1.4 2015/04/28 12:35:02 siva Exp $
*
* Description: snmp stats low level Routines
*********************************************************************/
# include  "snmpcmn.h"
# include  "snmmpdlw.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetSnmpUnknownSecurityModels
 Input       :  The Indices

                The Object 
                retValSnmpUnknownSecurityModels
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpUnknownSecurityModels (UINT4 *pu4RetValSnmpUnknownSecurityModels)
{
    *pu4RetValSnmpUnknownSecurityModels = gSnmpStat.u4SnmpUnkownSecModel;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpInvalidMsgs
 Input       :  The Indices

                The Object 
                retValSnmpInvalidMsgs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpInvalidMsgs (UINT4 *pu4RetValSnmpInvalidMsgs)
{
    *pu4RetValSnmpInvalidMsgs = gSnmpStat.u4SnmpInvalidMsg;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpUnknownPDUHandlers
 Input       :  The Indices

                The Object 
                retValSnmpUnknownPDUHandlers
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpUnknownPDUHandlers (UINT4 *pu4RetValSnmpUnknownPDUHandlers)
{
    *pu4RetValSnmpUnknownPDUHandlers = gSnmpStat.u4SnmpUnknownPduHandler;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetSnmpEngineID
 Input       :  The Indices

                The Object 
                retValSnmpEngineID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpEngineID (tSNMP_OCTET_STRING_TYPE * pRetValSnmpEngineID)
{
    SNMPCopyOctetString (pRetValSnmpEngineID, &gSnmpEngineID);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpEngineBoots
 Input       :  The Indices

                The Object 
                retValSnmpEngineBoots
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpEngineBoots (INT4 *pi4RetValSnmpEngineBoots)
{
    *pi4RetValSnmpEngineBoots = (INT4) (gSnmpSystem.u4SnmpBootCount);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpEngineTime
 Input       :  The Indices

                The Object 
                retValSnmpEngineTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpEngineTime (INT4 *pi4RetValSnmpEngineTime)
{
    UINT4               u4CurrTime = 0;
    GET_TIME_TICKS (&u4CurrTime);
    *pi4RetValSnmpEngineTime =
        (INT4) ((u4CurrTime -
                 gSnmpSystem.u4SysUpTime) / SYS_NUM_OF_TIME_UNITS_IN_A_SEC);
    if ((*pi4RetValSnmpEngineTime >= SNMP_ENGINE_MAX))
    {
        gSnmpSystem.u4SnmpBootCount++;
        if (gSnmpSystem.u4SnmpBootCount <= SNMP_ENGINE_MAX)
        {
            IssSetSnmpEngineBootsToNvRam (gSnmpSystem.u4SnmpBootCount);
            gSnmpSystem.u4SysUpTime = u4CurrTime;
            *pi4RetValSnmpEngineTime = 0;
        }
        else
        {
            SNMPTrace ("Engine Boot exceeded Max Count\n");
            return SNMP_FAILURE;
        }

    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpEngineMaxMessageSize
 Input       :  The Indices

                The Object 
                retValSnmpEngineMaxMessageSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpEngineMaxMessageSize (INT4 *pi4RetValSnmpEngineMaxMessageSize)
{
    *pi4RetValSnmpEngineMaxMessageSize = MAX_PKT_LENGTH;
    return SNMP_SUCCESS;
}
