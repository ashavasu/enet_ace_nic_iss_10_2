/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: snmpapi.c,v 1.1 2015/04/28 12:35:02 siva Exp $
 *
 * Description: This file contains Snmp Api routines which are 
 *              invoked from other modules
 *                 
 *******************************************************************/

#include "snmpcmn.h"
#include "stdsnmlw.h"
#include "snmputil.h"
/************************************************************************
 *  Function Name   : SnmpApiSetCommunityCtrl
 *  Description     : This function will set the community control.
 *                    If this is set, then the default communities
 *                    cannot be created/deleted, also a new 
 *                    community cannot be created.
 *  Input           : None
 *  Output          : None
 *  Returns         : None
 ************************************************************************/

VOID
SnmpApiSetCommunityCtrl (UINT1 u1CtrlFlag)
{
    if (OSIX_TRUE == u1CtrlFlag)
    {
        gi4SnmpCommunityCtrl = SNMP_TRUE;
    }
    else                        /* Unset the Community Control Flag */
    {
        gi4SnmpCommunityCtrl = SNMP_FALSE;
    }
    return;
}

/************************************************************************        * Function     :   SnmpApiClearData                                             *
 * Description  :   This function releases the memory intialised
 *                  for the pointer being used in msr.
 *
 * Input        :   pu1Data - Pointer to the data                                *
 * Output       :   None
 *
 * Returns      :   None
 *
 *************************************************************************/
VOID
SnmpApiClearData (UINT1 *pu1Data)
{
    MemReleaseMemBlock (gSnmpDataPoolId, pu1Data);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : SnmpSetSysName                                    */
/*                                                                          */
/*    Description        : This function is invoked to set the System-name  */
/*                                                                          */
/*    Input(s)           : pSwitchName -pointer to the switch name          */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : SNMP_SUCCESS or SNMP_FAILURE.                                            */
/****************************************************************************/
INT4
SnmpApiSetSysName (tSNMP_OCTET_STRING_TYPE * pSwitchName)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2SysName (&u4ErrorCode, pSwitchName) == SNMP_FAILURE)
    {
        if (u4ErrorCode == SNMP_ERR_WRONG_LENGTH)
        {
            SNMPTrace ("SnmpApiSetSysName: Invalid string length\r\n");
            return SNMP_FAILURE;
        }
        else
        {
            SNMPTrace ("SnmpApiSetSysName: Invalid string value\r\n");
            return SNMP_FAILURE;
        }
    }
    else
    {
        nmhSetSysName (pSwitchName);
        return SNMP_SUCCESS;
    }
}

/*****************************************************************************/
/* Function Name      : SnmpCallBackRegister                                 */
/*                                                                           */
/* Description        : This function registers the call backs from          */
/*                      application                                          */
/*                                                                           */
/* Input(s)           : u4Event - Event for which callback is registered     */
/*                      pFsCbInfo - Call Back Function                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS/SNMP_FAILURE                                */
/*****************************************************************************/
INT4
SnmpCallBackRegister (UINT4 u4Event, tFsCbInfo * pFsCbInfo)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    switch (u4Event)
    {
        case SNMP_UPDATE_EOID_EVENT:
            SNMP_CALLBACK[u4Event].pSnmpUpdateEOIDCallBack =
                pFsCbInfo->pSnmpUpdateEOIDCallBack;
            break;

        case SNMP_REVERT_EOID_EVENT:
            SNMP_CALLBACK[u4Event].pSnmpRevertEOIDCallBack =
                pFsCbInfo->pSnmpRevertEOIDCallBack;
            break;

        case SNMP_UPDATE_GETNEXT_EOID_EVENT:
            SNMP_CALLBACK[u4Event].pSnmpUpdateGetNextEOIDCallBack =
                pFsCbInfo->pSnmpUpdateGetNextEOIDCallBack;
            break;

        default:
            i4RetVal = SNMP_FAILURE;
            break;
    }

    return i4RetVal;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : SnmpApiSetSNMPUpdateDefault                      */
/*                                                                          */
/*    Description        : This function is invoked to set the global       */
/*                           variable gu1SNMPUpdateDefault. The vaule is set  */
/*                            to OSIX_TRUE to make snmp task to initialise     */
/*                         the defaults. The value is set to OSIX_FALSE     */
/*                         if default values need not be initialised        */
/*                         by snmp task and will be taken care in           */
/*                         Customized bringup                               */
/*                                                                          */
/*    Input(s)           : u1Status- OSIX_TRUE or OSIX_FALSE                */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : SNMP_SUCCESS or SNMP_FAILURE.                    */
/****************************************************************************/
INT4
SnmpApiSetSNMPUpdateDefault (UINT1 u1Status)
{
    gu1SNMPUpdateDefault = u1Status;
    return SNMP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : SnmpApiGetSNMPUpdateDefault                      */
/*                                                                          */
/*    Description        : This function is invoked to get the global       */
/*                           variable gu1SNMPUpdateDefault                    */
/*                                                                          */
/*    Input(s)           : VOID                                                */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Value of gu1SNMPUpdateDefault                    */
/****************************************************************************/
UINT1
SnmpApiGetSNMPUpdateDefault (VOID)
{
    return (gu1SNMPUpdateDefault);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : SnmpApiUpdateEngineId                            */
/*                                                                          */
/*    Description        : This function is invoked to update the snmp      */
/*                         engine id from issnvram file                     */
/*                                                                          */
/*    Input(s)           : VOID                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            :SNMP_SUCCESS or SNMP_FAILURE                      */
/****************************************************************************/
INT4
SnmpApiUpdateEngineId (VOID)
{
#ifdef ISS_WANTED
    INT1               *pi1EngineID = NULL;
    pi1EngineID = IssGetSnmpEngineID ();
    gSnmpEngineID.pu1_OctetList = gau1LocalSnmpEngineID;
    if (SnmpConvertStringToOctet (pi1EngineID, &gSnmpEngineID) == SNMP_FAILURE
        || ((gSnmpEngineID.i4_Length < SNMP_MIN_ENGINE_ID_LEN)
            || (gSnmpEngineID.i4_Length > SNMP_MAX_ENGINE_ID_LEN)))
    {
        MEMCPY (gSnmpEngineID.pu1_OctetList, gau1SnmpEngineID,
                SNMP_DEFAULT_ENGINEID_LEN);
        gSnmpEngineID.i4_Length = SNMP_DEFAULT_ENGINEID_LEN;
        IssSetSnmpEngineIDToNvRam (NULL);
    }
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : SnmpApiDecrementCounter                          */
/*                                                                          */
/*    Description        : This function decrements no such name error      */
/*                         counter                                          */
/*                                                                          */
/*                                                                          */
/*    Input(s)           : Error value                                      */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
SnmpApiDecrementCounter (UINT4 u4Error)
{
    if ((u4Error == SNMP_EXCEPTION_NO_SUCH_OBJECT)
        || (u4Error == SNMP_EXCEPTION_NO_SUCH_INSTANCE))
    {
        SNMP_DCR_OUT_NO_SUCH_NAME;
    }
    return;
}
