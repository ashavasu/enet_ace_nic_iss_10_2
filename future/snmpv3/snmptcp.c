/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: snmptcp.c,v 1.1 2015/04/28 12:35:02 siva Exp $
 *
 * Description:Routines for Snmp Over Tcp.   
 *******************************************************************/

#include "snmpcmn.h"
#include "fssocket.h"

extern tSnmpTcp     gSnmpTcpGblInfo;
extern INT4         SNMPDecodeSequence (UINT1 **ppu1_curr_ptr,
                                        UINT1 *pu1_end_ptr);
extern INT4         SNMPDecodeInteger (UINT1 **pu1_curr, UINT1 *pu1_end,
                                       INT2 *i2_type, INT2 i2_unsign_or_sign);
/***************************************************************************
 * Function           : SnmpTcpSetTcpStatus
 * Input(s)           : i4TcpStatus - TcpStatus.
 * Output(s)          : None.
 * Returns            : SNMP_SUCCESS/SNMP_FAILURE.
 * Action             : This function enables/disables SNMP over TCP status
 *                      It sends SNMPTCP_ENABLE_EVT/SNMPTCP_DISABLE_EVT to 
                        snmp main task 
 * ******************************************************************/

INT4
Snmp3SetTcpStatus (INT4 i4TcpStatus)
{
    if (i4TcpStatus == SNMP3_OVER_TCP_ENABLE)
    {
        if (OsixSendEvent (SELF, (const UINT1 *) SNMP_TASK_NAME,
                           SNMPTCP_ENABLE_EVT) != OSIX_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }
    if (i4TcpStatus == SNMP3_OVER_TCP_DISABLE)
    {
        if (OsixSendEvent (SELF, (const UINT1 *) SNMP_TASK_NAME,
                           SNMPTCP_DISABLE_EVT) != OSIX_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/***************************************************************************
 * Function           : SnmpTcpHandleNewConnEvt
 * Input(s)           : u4Events.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function handles when new connection is 
 *                      initiated from SNMP manager
 * * ********************************************************************/
VOID
SnmpTcpHandleNewConnEvt (UINT4 u4Events)
{
    INT4                i4SockFd = SNMP_ZERO;
    INT4                i4CliSockFd = SNMP_ZERO;
    tIPvXAddr           CliAddr;
    tSnmpTcpSsn        *pSnmpTcpSsn = NULL;

    if (u4Events & SNMPTCP_V4_NEWCONN_EVT)
    {
        i4SockFd = gSnmpTcpGblInfo.i4TcpV4SockId;
    }
#ifdef IP6_WANTED
    else if (u4Events & SNMPTCP_V6_NEWCONN_EVT)
    {
        i4SockFd = gSnmpTcpGblInfo.i4TcpV6SockId;
    }
#endif
    MEMSET (&CliAddr, SNMP_ZERO, sizeof (tIPvXAddr));

    if ((SnmpTcpAcceptNewConnection
         (i4SockFd, &i4CliSockFd, &CliAddr) == SNMP_SUCCESS))
    {
        /* Session table creation */
        pSnmpTcpSsn = (tSnmpTcpSsn *) MemAllocMemBlk
            (gSnmpTcpGblInfo.TcpSsnPoolId);
        if (pSnmpTcpSsn == NULL)
        {
            SNMPTrace ("SNMP:Cannot allocate memory for new Session!");
            return;
        }
        pSnmpTcpSsn->i4SockFd = i4CliSockFd;
        MEMCPY (&pSnmpTcpSsn->MgrAddr, &CliAddr, sizeof (tIPvXAddr));

        /* Adding the session node to the session list */
        if (SNMP_SUCCESS == SnmpTcpInitAndAddSsn (pSnmpTcpSsn))
        {
            gSnmpTcpGblInfo.u4TcpTotalSsns++;

            if (SelAddFd (i4CliSockFd, SnmpTcpPktRcvd) < SNMP_ZERO)
            {
                SNMPTrace
                    ("SNMPTCP: SelAddFd for getting the packet from the TCP"
                     "session failed\r\n");
            }

        }
        else
        {
            SnmpTcpFreeSsnNode (pSnmpTcpSsn);
            SNMPTrace ("SNMP: Cannot update Session Table\r\n");
        }
    }

    if (CliAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        if (SelAddFd (i4SockFd, SnmpTcpV4NewConnCallBk) < SNMP_ZERO)
        {
            SNMPTrace ("SNMPTCP: SelAddFd for new connection failed\r\n");
        }
    }
    else if (CliAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        if (SelAddFd (i4SockFd, SnmpTcpV6NewConnCallBk) < SNMP_ZERO)
        {
            SNMPTrace ("SNMPTCP: SelAddFd for new connection failed\r\n");
        }
    }
    KW_FALSEPOSITIVE_FIX (pSnmpTcpSsn);
    return;
}

/***************************************************************************
 * Function           : SnmpAgentTcpInit
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function Initialises the memory required for 
                         SNMP over TCP  
**********************************************************************/
INT4
SnmpAgentTcpInit (VOID)
{
    if (OsixQueCrt ((UINT1 *) SNMPTCP_CLIENT_Q, OSIX_MAX_Q_MSG_LEN,
                    MAX_SNMP_TCP_CLIENT_Q_DEPTH, &gSnmpTcpGblInfo.SnmpTcpQId)
        != OSIX_SUCCESS)
    {
        SNMPTrace ("\r\nSnmpAgentTcpInit: Unable to create Beep Client Queue");
        return SNMP_FAILURE;
    }

    gSnmpTcpGblInfo.TcpSsnPoolId =
        SNMPV3MemPoolIds[MAX_SNMP_TCP_CLIENTS_SIZING_ID];
    gSnmpTcpGblInfo.TcpCntrlPoolId =
        SNMPV3MemPoolIds[MAX_SNMP_TCP_CLIENT_Q_DEPTH_SIZING_ID];
    gSnmpTcpGblInfo.TcpSsnRcvBufPoolId =
        SNMPV3MemPoolIds[MAX_SNMP_TCP_SSN_RCV_BUF_BLOCKS_SIZING_ID];

    gSnmpTcpGblInfo.u2TcpPort = SNMP_PORT;
    gSnmpTcpGblInfo.u2TcpTrapPort = SNMP_MANAGER_PORT;
    gSnmpTcpGblInfo.i4TcpStatus = SNMPTCP_DISABLE;
    gSnmpTcpGblInfo.i4TcpTrapStatus = SNMPTCP_DISABLE;
    gSnmpTcpGblInfo.i4TcpV4SockId = SNMPTCP_INVALID_SOCKFD;
    gSnmpTcpGblInfo.i4TcpV6SockId = SNMPTCP_INVALID_SOCKFD;

    return SNMP_SUCCESS;
}

/***************************************************************************
 * Function           : SnmpTcpAllocSsnNode
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function allocates the memory required for SNMP                            over TCP Session
**************************************************************************/
tSnmpTcpSsn        *
SnmpTcpAllocSsnNode (VOID)
{
    tSnmpTcpSsn        *pTcpSsn = NULL;
    pTcpSsn = (tSnmpTcpSsn *) MemAllocMemBlk (gSnmpTcpGblInfo.TcpSsnPoolId);
    return pTcpSsn;
}

/***************************************************************************
 * Function           : SnmpTcpFreeSsnNode
 * Input(s)           : pTcpSsnNode - Tcp session node.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function releases the memory allocated for TCP 
                        Session
**************************************************************************/
VOID
SnmpTcpFreeSsnNode (tSnmpTcpSsn * pTcpSsnNode)
{
    tSnmpTcpTxNode     *pTxBufNode = NULL;

    /* release the Receive Buf */
    if (pTcpSsnNode->pRcvBuf != NULL)
    {
        MemReleaseMemBlock (gSnmpTcpGblInfo.TcpSsnRcvBufPoolId,
                            (UINT1 *) (pTcpSsnNode->pRcvBuf));
        pTcpSsnNode->pRcvBuf = NULL;
    }

    /* Free the Tx node in Tx list */
    while ((pTxBufNode = (tSnmpTcpTxNode *)
            TMO_SLL_First (&(pTcpSsnNode->TxList))) != NULL)
    {
        TMO_SLL_Delete (&(pTcpSsnNode->TxList),
                        (tTMO_SLL_NODE *) & pTxBufNode->TxNode);
        MemReleaseMemBlock (gSnmpTcpTxNodePoolId, (UINT1 *) pTxBufNode);
    }
    /* Release the Tcp session */
    MemReleaseMemBlock (gSnmpTcpGblInfo.TcpSsnPoolId, (UINT1 *) pTcpSsnNode);
}

/***************************************************************************
 * Function           : SnmpTcpInitAndAddSsn
 * Input(s)           : pTcpSsnNode - Pointer to TCP session node.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function allocates the memory required for SNMP                          over TCP Session
**************************************************************************/
INT4
SnmpTcpInitAndAddSsn (tSnmpTcpSsn * pTcpSsnNode)
{
    UINT4               u4Cnt = 0;

    if (NULL == pTcpSsnNode)
    {
        return SNMP_FAILURE;
    }
    /* allocate memory for receive buffer */
    pTcpSsnNode->pRcvBuf = MemAllocMemBlk (gSnmpTcpGblInfo.TcpSsnRcvBufPoolId);
    if (pTcpSsnNode->pRcvBuf == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Init (&pTcpSsnNode->TxList);

    for (; u4Cnt < MAX_SNMP_TCP_CLIENTS_LIMIT; u4Cnt++)
    {
        if (gSnmpTcpGblInfo.apSnmpTcpSsn[u4Cnt] == NULL)
        {
            gSnmpTcpGblInfo.apSnmpTcpSsn[u4Cnt] = pTcpSsnNode;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/***************************************************************************
 * Function           : SnmpTcpFindSsnNode
 * Input(s)           : i4SockFd - Socket ID for the session.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function finds the session node using Sock Id
 *                      in the Tcp sessions maintained for SNMP
**************************************************************************/

tSnmpTcpSsn        *
SnmpTcpFindSsnNode (INT4 i4SockFd)
{
    INT4                i4Cnt = 0;
    tSnmpTcpSsn        *pTcpSsn = NULL;

    for (; i4Cnt < MAX_SNMP_TCP_CLIENTS_LIMIT; i4Cnt++)
    {
        pTcpSsn = gSnmpTcpGblInfo.apSnmpTcpSsn[i4Cnt];
        if (pTcpSsn->i4SockFd == i4SockFd)
        {
            return pTcpSsn;
        }
    }
    return NULL;
}

/******************************************************************************
 * Function           : SnmpTcpHandleRdWrEvt
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function processes the event sent when the 
                        SelAddFd or SelWriteFd is hit.
 ******************************************************************************/
VOID
SnmpTcpHandleRdWrEvt (VOID)
{
    tSnmpTcpMsg        *pMsg = NULL;

    if (OsixQueRecv (gSnmpTcpGblInfo.SnmpTcpQId, (UINT1 *) &pMsg,
                     OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
    {
        if (pMsg != NULL)
        {
            if (pMsg->i4Cmd == SNMPTCP_PKT_READ)
            {
                if (SnmpTcpProcessReadEvt (pMsg->i4SockFd) == SNMP_SUCCESS)
                {

                    if (SelAddFd (pMsg->i4SockFd, SnmpTcpPktRcvd) < SNMP_ZERO)
                    {
                        SNMPTrace
                            ("SNMPTCP: SelAddFd failed in SnmpTcpProcessEvt");
                    }
                }
            }
            if (pMsg->i4Cmd == SNMPTCP_PKT_WRITE)
            {
                SnmpTcpProcessWriteEvt (pMsg->i4SockFd);
            }
        }

        MemReleaseMemBlock (gSnmpTcpGblInfo.TcpCntrlPoolId, (UINT1 *) pMsg);
        return;
    }
}

/******************************************************************************
 * Function           : SnmpTcpProcessReadEvt
 * Input(s)           : i4SockFd - Sock Id in which the data is received.
 * Output(s)          : None.
 * Returns            : SNMP_SUCCESS or SNMP_FAILURE.
 * Action             : This function receives the data from the socket and.
 *                      process the data. 
 ***************************************************************************/
INT4
SnmpTcpProcessReadEvt (INT4 i4SockFd)
{
    INT4                i4DataRcvd = SNMP_ZERO;
    INT4                i4RetVal = SNMP_ZERO;
    INT4                i4BufSize = SNMP_ZERO;
    INT4                i4LenExpected = SNMP_ZERO;
    INT4                i4PktLen = SNMP_ZERO;
    INT4                i4Version = SNMP_ZERO;
    INT4                i4DataLen = SNMP_ZERO;

    tSnmpTcpSsn        *pTcpSsnNode = NULL;
    UINT1              *pDataBuf = NULL;
    UINT1              *pStartPtr = NULL;

    SNMPTrace ("Entering the Function SnmpTcpProcessReadEvt\r\n");
    pTcpSsnNode = SnmpTcpFindSsnNode (i4SockFd);

    if (pTcpSsnNode == NULL)
    {
        SNMPTrace ("SNMPTCP: Unable to get Snmp Tcp Session\r\n");
        return SNMP_FAILURE;
    }

    pDataBuf = MemAllocMemBlk (gSnmpTcpRcvBufPoolId);

    if (pDataBuf == NULL)
    {
        SNMPTrace ("SNMPTCP: Unable to allocate buffer for receiving data\r\n");
        return SNMP_FAILURE;
    }
    i4BufSize = SNMPTCP_RCV_BUF_LEN - pTcpSsnNode->i4LenRcvd;

    /* Read the data from socket */
    i4RetVal =
        SnmpTcpSockRcv (i4SockFd, pDataBuf, (UINT2) i4BufSize, &i4DataRcvd);

    if ((i4RetVal == SNMP_FAILURE) || (i4DataRcvd == 0))
    {
        /* Socket read failed, close the socket and remove the session node */
        MemReleaseMemBlock (gSnmpTcpRcvBufPoolId, (UINT1 *) pDataBuf);
        SnmpTcpForceCloseSsn (pTcpSsnNode);
        SNMPTrace ("SnmpTcpProcessReadEvt: read failure, closing the session");
        SNMPTrace ("SNMPTCP: Exiting SnmpTcpProcessReadEvt");
        return SNMP_FAILURE;
    }

    pStartPtr = pTcpSsnNode->pRcvBuf + pTcpSsnNode->i4LenRcvd;

    MEMCPY (pStartPtr, pDataBuf, i4DataRcvd);

    if (pTcpSsnNode->i4PartialData == SNMP_TRUE)
    {
        i4LenExpected = pTcpSsnNode->i4DataLen - pTcpSsnNode->i4LenRcvd;

        /* If the required data not received */
        if (i4DataRcvd < i4LenExpected)
        {
            pTcpSsnNode->i4LenRcvd = pTcpSsnNode->i4LenRcvd + i4DataRcvd;
            MemReleaseMemBlock (gSnmpTcpRcvBufPoolId, (UINT1 *) pDataBuf);
            return SNMP_SUCCESS;
        }

        /* If the required data is received */
        if (i4DataRcvd == i4LenExpected)
        {
            pTcpSsnNode->i4LenRcvd = pTcpSsnNode->i4LenRcvd + i4DataRcvd;

            /* required packet received, process the received packet */
            if (SnmpTcpProcessRcvdPkt
                (i4SockFd, i4Version, pTcpSsnNode->pRcvBuf,
                 pTcpSsnNode->i4LenRcvd) == SNMP_FAILURE)
            {
                MemReleaseMemBlock (gSnmpTcpRcvBufPoolId, (UINT1 *) pDataBuf);
                SnmpTcpForceCloseSsn (pTcpSsnNode);
                SNMPTrace ("\r\n SnmpTcpProcessRcvdPkt failed");
                return SNMP_FAILURE;
            }
            MEMMOVE (pTcpSsnNode->pRcvBuf,
                     pTcpSsnNode->pRcvBuf + pTcpSsnNode->i4LenRcvd,
                     pTcpSsnNode->i4LenRcvd);
            pTcpSsnNode->i4PartialData = SNMP_ZERO;
            pTcpSsnNode->i4LenRcvd = SNMP_ZERO;
            pTcpSsnNode->i4DataLen = SNMP_ZERO;

            MemReleaseMemBlock (gSnmpTcpRcvBufPoolId, (UINT1 *) pDataBuf);
            return SNMP_SUCCESS;
        }
        if (i4DataRcvd > i4LenExpected)
        {
            pTcpSsnNode->i4LenRcvd += i4LenExpected;

            if (SnmpTcpProcessRcvdPkt
                (i4SockFd, i4Version, pTcpSsnNode->pRcvBuf,
                 pTcpSsnNode->i4LenRcvd) == SNMP_FAILURE)
            {
                MemReleaseMemBlock (gSnmpTcpRcvBufPoolId, (UINT1 *) pDataBuf);
                SnmpTcpForceCloseSsn (pTcpSsnNode);
                SNMPTrace ("\r\n SnmpTcpProcessRcvdPkt failed");
                return SNMP_FAILURE;
            }

            MEMMOVE (pTcpSsnNode->pRcvBuf,
                     pTcpSsnNode->pRcvBuf + pTcpSsnNode->i4LenRcvd,
                     pTcpSsnNode->i4LenRcvd);
            i4DataRcvd = i4DataRcvd - i4LenExpected;
            pTcpSsnNode->i4LenRcvd += i4DataRcvd;
        }
    }
    else
    {
        pTcpSsnNode->i4LenRcvd = i4DataRcvd;
    }

    do
    {
        i4DataLen = pTcpSsnNode->i4LenRcvd;

        if (i4DataLen < 5)
        {
            SNMPTrace ("\r\nSnmpTcpProcessReadEvt: Not enough data received"
                       "to process the SNMP request");
            MemReleaseMemBlock (gSnmpTcpRcvBufPoolId, (UINT1 *) pDataBuf);
            return SNMP_SUCCESS;
        }

        /* Check the version and len of the remaining data read from socket */
        SnmpTcpGetLenAndVersion (pTcpSsnNode->pRcvBuf, i4DataLen, &i4PktLen,
                                 &i4Version);

        if (i4PktLen == SNMP_NOT_OK)
        {
            MemReleaseMemBlock (gSnmpTcpRcvBufPoolId, (UINT1 *) pDataBuf);
            SnmpTcpForceCloseSsn (pTcpSsnNode);
            SNMPTrace ("\r\n SNMPTCP: Packet received with invalid Len,"
                       "Closing the session");
            return SNMP_FAILURE;
        }
        if (i4Version == SNMP_NOT_OK)
        {
            SnmpTcpForceCloseSsn (pTcpSsnNode);
            SNMPTrace ("\r\n SNMPTCP: Packet received with invalid Version,"
                       "Closing the session ");
            MemReleaseMemBlock (gSnmpTcpRcvBufPoolId, (UINT1 *) pDataBuf);
            return SNMP_FAILURE;
        }
        /* Send the SNMP frames present in the data received from socket */
        if (i4PktLen <= i4DataLen)
        {
            if (SnmpTcpProcessRcvdPkt
                (i4SockFd, i4Version, pTcpSsnNode->pRcvBuf,
                 i4PktLen) == SNMP_FAILURE)
            {
                MemReleaseMemBlock (gSnmpTcpRcvBufPoolId, (UINT1 *) pDataBuf);
                SnmpTcpForceCloseSsn (pTcpSsnNode);
                SNMPTrace ("\r\n SnmpTcpProcessRcvdPkt failed");
                return SNMP_FAILURE;
            }
            MEMMOVE (pTcpSsnNode->pRcvBuf,
                     pTcpSsnNode->pRcvBuf + i4PktLen, i4PktLen);
            pTcpSsnNode->i4LenRcvd -= i4PktLen;
            i4DataLen = i4DataLen - i4PktLen;
        }

        /* If not enough data received, store the data in ssn node as 
         * partial data*/
        else
        {
            pTcpSsnNode->i4PartialData = SNMP_TRUE;
            pTcpSsnNode->i4DataLen = i4PktLen;
            MemReleaseMemBlock (gSnmpTcpRcvBufPoolId, (UINT1 *) pDataBuf);
            return SNMP_SUCCESS;
        }
    }
    while (i4DataLen != SNMP_ZERO);

    MemReleaseMemBlock (gSnmpTcpRcvBufPoolId, (UINT1 *) pDataBuf);
    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : SnmpTcpGetLenAndVersion
*  Description   : Function gets Len and Version of incomming SNMP 
*                  Request
*  Parameter(s)  : pu1Pkt - Pointer to Packet Content
*                  i4PktSize - Packet Length in Bytes 
                   pi4Len - Pointer to get packet len
                   pi4Version - Pointer to get version 
*  Return Values : None
*  Output        : Incoming packet Length and version
*********************************************************************/
VOID
SnmpTcpGetLenAndVersion (UINT1 *pu1Pkt, INT4 i4PktSize, INT4 *pi4Len,
                         INT4 *pi4Version)
{
    UINT1              *pu1Start = NULL, *pu1End = NULL;
    INT2                i2Type = SNMP_ZERO;

    pu1Start = pu1Pkt;
    pu1End = pu1Start + i4PktSize;
    *pi4Len = SNMPDecodeSequence (&pu1Start, pu1End);
    if (*pi4Len == SNMP_NOT_OK)
    {
        SNMPTrace ("Decode Snmp Message Length Failed\n");
        return;
    }

    if (*pi4Len < 0x80)
    {
        /* increase 2 bytes for len and version for non authenticated user */
        *pi4Len = *pi4Len + SNMP_TWO;
    }
    else if (*pi4Len < 0xf7)
    {
        *pi4Len = *pi4Len + SNMP_THREE;
    }
    else
    {
        *pi4Len = *pi4Len + SNMP_FOUR;
    }
    *pi4Version = (INT4) SNMPDecodeInteger (&pu1Start, pu1End,
                                            &i2Type, SNMP_UNSIGNED);
    if ((i2Type == SNMP_NOT_OK) || (i2Type != SNMP_DATA_TYPE_INTEGER32))
    {
        SNMPTrace ("Decode  Version Number Failed \n");
        *pi4Version = SNMP_NOT_OK;
        return;
    }
    return;
}

/******************************************************************************
 * Function           : SnmpTcpProcessRcvdPkt
 * Input(s)           : i4SockFd - Sock Id in which the data is received.
                        i4Version - SNMP version of the packet
 *                      pu1Pkt - Packet to process
 *                      i4Len - Length of the packet   
 * Output(s)          : None.
 * Returns            : SNMP_SUCCESS or SNMP_FAILURE.
 * Action             : This function receives the data from the socket and.
 *                      process the data. 
 ***************************************************************************/
INT4
SnmpTcpProcessRcvdPkt (INT4 i4SockId, INT4 i4Version, UINT1 *pu1RcvPkt,
                       INT4 i4Len)
{
    UINT4               u4SendSize = SNMP_ZERO;
    INT4                i4SentLen = SNMP_ZERO;
    INT4                i4Return = SNMP_ZERO;
    INT4                i4RetVal = SNMP_ZERO;
    UINT1              *pu1Pkt = NULL;
    UINT1              *pu1OutPkt = NULL;
    UINT1              *pu1TmpOutPkt = NULL;

    pu1Pkt = MemAllocMemBlk (gSnmpPktPoolId);

    if (pu1Pkt == NULL)
    {
        SNMPTrace ("\r\n Malloc failed in SnmpTcpProcessPkt");
        return SNMP_FAILURE;
    }
    MEMSET (pu1Pkt, 0, i4Len);
    MEMCPY (pu1Pkt, pu1RcvPkt, i4Len);

    if (i4Version == VERSION1 || i4Version == VERSION2)
    {
        /* V!-V2 or V1-V2-V3 Should be configured */
        if ((gu4SnmpAllowedVersion != SNMP_V1V2_PDU) &&
            (gu4SnmpAllowedVersion != SNMP_V1V2V3_PDU))
        {
            SNMP_INR_BAD_VERSION;
            SNMP_INR_SILENT_DROPS;
        }

        i4Return = SNMPProcess (pu1Pkt, (UINT4) i4Len, &pu1OutPkt, &u4SendSize);
    }
    else if (i4Version == VERSION3)
    {
        /* V3 or V1-V2-V3 Should be configured */
        if ((gu4SnmpAllowedVersion != SNMP_V3_PDU) &&
            (gu4SnmpAllowedVersion != SNMP_V1V2V3_PDU))
        {
            SNMP_INR_BAD_VERSION;
            SNMP_INR_SILENT_DROPS;
        }
        i4Return =
            SNMPV3Process (pu1Pkt, (UINT4) i4Len, &pu1OutPkt, &u4SendSize);
    }
    else
    {
        MemReleaseMemBlock (gSnmpPktPoolId, (UINT1 *) pu1Pkt);
        SNMP_INR_BAD_ASN_PARSE;
        SNMP_INR_BAD_VERSION;
        SNMP_INR_SILENT_DROPS;
        return SNMP_FAILURE;
    }
    if ((i4Return != SNMP_SUCCESS) || (pu1OutPkt == NULL))
    {
        SNMPTrace ("\r\n SNMPTCP: SNMPV3Process failed");
        MemReleaseMemBlock (gSnmpPktPoolId, (UINT1 *) pu1Pkt);

        /* If pu1OutPkt !=NULL, it could either have been allocated
         * from gSnmpPktPoolId or could have been assigend with address
         * denoted by array gau1RevDat. So taking care to free pu1OutPkt
         * in case of dynamic allocation only*/
        if ((pu1OutPkt != NULL) && (pu1OutPkt != gau1RevDat))
        {
            MemReleaseMemBlock (gSnmpPktPoolId, (UINT1 *) pu1OutPkt);
        }
        SNMP_INR_SILENT_DROPS;
        return SNMP_FAILURE;
    }
    /* If the Tx list of the socket i 0, Send the response via socket. 
     * Else add the pkt in Tx list */
    if (SnmpTcpGetTxListCnt (i4SockId) == SNMP_ZERO)
    {
        i4RetVal = SnmpTcpSockSend (i4SockId, pu1OutPkt,
                                    (UINT2) u4SendSize, &i4SentLen);
        if (i4RetVal == SNMP_FAILURE)
        {
            SNMPTrace ("SNMPTCP: Sending SNMP message over TCP failed");
            MemReleaseMemBlock (gSnmpPktPoolId, (UINT1 *) pu1Pkt);
            if (pu1OutPkt != gau1RevDat)
            {
                MemReleaseMemBlock (gSnmpPktPoolId, (UINT1 *) pu1OutPkt);
            }
            SNMP_INR_SILENT_DROPS;
            return SNMP_FAILURE;
        }
        if (i4SentLen < (INT4) u4SendSize)
        {
            SNMPTrace ("SNMPTCP: Tcp Send success with partial data");
            pu1TmpOutPkt = pu1OutPkt;
            pu1TmpOutPkt += i4SentLen;
            u4SendSize = u4SendSize - (UINT4) i4SentLen;
            SnmpTcpAddPktInTxList (i4SockId, pu1TmpOutPkt, u4SendSize);

            /* Add the residual data in SelAddWrFd */
            SelAddWrFd (i4SockId, SnmpTcpWriteCallBackFn);
        }
        SNMPTrace ("SNMPTCP: Tcp Send success");
    }
    else
    {
        /* Residual data there in TxList, so add the packet in TxList */
        SnmpTcpAddPktInTxList (i4SockId, pu1OutPkt, u4SendSize);
    }
    SNMP_INR_IN_PKTS;
    MemReleaseMemBlock (gSnmpPktPoolId, (UINT1 *) pu1Pkt);
    /*Since pu1OutPkt is either sent via TCP or contents are copied to 
     * another pointer and added to Tx list, we need to free pu1OutPkt
     * if it was dynamically allocated in SNMPProcess or SNMPV3Process*/
    if (pu1OutPkt != gau1RevDat)
    {
        MemReleaseMemBlock (gSnmpPktPoolId, (UINT1 *) pu1OutPkt);
    }

    return SNMP_SUCCESS;
}

/******************************************************************************
 * Function           : SnmpTcpForceCloseAllSsn
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : SNMP_SUCCESS or SNMP_FAILURE.
 * Action             : This function closes all the tcp sessions created for 
 *                      SNMP. 
 ***************************************************************************/
VOID
SnmpTcpForceCloseAllSsn (VOID)
{
    INT4                i4Cnt = SNMP_ZERO;
    tSnmpTcpSsn        *pTcpSsn = NULL;

    for (; i4Cnt < MAX_SNMP_TCP_CLIENTS_LIMIT; i4Cnt++)
    {
        pTcpSsn = gSnmpTcpGblInfo.apSnmpTcpSsn[i4Cnt];

        if (pTcpSsn != NULL)
        {
            SnmpTcpForceCloseSsn (pTcpSsn);
        }
    }
    return;
}

/******************************************************************************
 * Function           : SnmpTcpForceCloseSsn
 * Input(s)           : pTcpSsn - Pointer to the session node 
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function closes the given TCP session. 
 ***************************************************************************/
VOID
SnmpTcpForceCloseSsn (tSnmpTcpSsn * pTcpSsn)
{
    INT4                i4Cnt = 0;

    for (; i4Cnt < MAX_SNMP_TCP_CLIENTS_LIMIT; i4Cnt++)
    {
        if (pTcpSsn == gSnmpTcpGblInfo.apSnmpTcpSsn[i4Cnt])
        {
            gSnmpTcpGblInfo.apSnmpTcpSsn[i4Cnt] = NULL;
        }
    }

    SelRemoveFd(pTcpSsn->i4SockFd);
    close (pTcpSsn->i4SockFd);

    SnmpTcpFreeSsnNode (pTcpSsn);
    return;
}

/******************************************************************************
 * Function           : SnmpTcpProcessWriteEvt
 * Input(s)           : i4SockFd - Sock Id for which the write buffer is free.
 * Output(s)          : None.
 * Returns            : SNMP_SUCCESS or SNMP_FAILURE.
 * Action             : This function writes the residual data in the session 
                        node in the given socket. 
 ***************************************************************************/
INT4
SnmpTcpProcessWriteEvt (INT4 i4SockFd)
{
    INT4                i4SentLen = SNMP_ZERO;
    INT4                i4RetVal = SNMP_ZERO;
    tSnmpTcpSsn        *pTcpSsn = NULL;
    tSnmpTcpTxNode     *pTcpTxNode = NULL;
    UINT1              *pu1CurrPkt = NULL;

    pTcpSsn = SnmpTcpFindSsnNode (i4SockFd);

    if (pTcpSsn == NULL)
    {
        SNMPTrace ("\r\n SNMPTCP: Malloc failed in SnmpTcpProcessWriteEvt");
        return SNMP_FAILURE;
    }

    if (SnmpTcpGetTxListCnt (i4SockFd) == SNMP_ZERO)
    {
        SNMPTrace ("\r\n SNMPTCP: SnmpTcpProcessWriteEvt failed since Tx List"
                   "empty");
        return SNMP_FAILURE;
    }

    pTcpTxNode = (tSnmpTcpTxNode *) TMO_SLL_First (&pTcpSsn->TxList);

    while (pTcpTxNode)
    {
        pu1CurrPkt = pTcpTxNode->pu1Msg + pTcpTxNode->u4MsgOffset;
        i4RetVal = SnmpTcpSockSend (i4SockFd, pu1CurrPkt,
                                    (UINT2) pTcpTxNode->u4MsgSize, &i4SentLen);
        if (i4RetVal == SNMP_FAILURE)
        {
            SNMPTrace
                ("\r\nSnmpTcpProcessWriteEvt:Sending SNMP message over TCP"
                 " failed");
            SNMP_INR_SILENT_DROPS;
            return SNMP_FAILURE;
        }
        if (i4SentLen < (INT4) pTcpTxNode->u4MsgSize)
        {
            SNMPTrace ("\r\nSnmpTcpProcessWriteEvt: Tcp Send success with"
                       " partial data");

            pTcpTxNode->u4MsgOffset += (UINT4) i4SentLen;
            pTcpTxNode->u4MsgSize -= (UINT4) i4SentLen;
            /* Add the residual data in SelAddWrFd */
            SelAddWrFd (i4SockFd, SnmpTcpWriteCallBackFn);
            return SNMP_SUCCESS;
        }
        SNMPTrace ("\r\nSnmpTcpProcessWriteEvt: Tcp Send success");
        SNMP_INR_OUT_PKTS;

        TMO_SLL_Delete (&pTcpSsn->TxList,
                        (tTMO_SLL_NODE *) & pTcpTxNode->TxNode);

        MemReleaseMemBlock (gSnmpPktPoolId, (UINT1 *) pTcpTxNode->pu1Msg);
        MemReleaseMemBlock (gSnmpTcpTxNodePoolId, (UINT1 *) pTcpTxNode);

        pTcpTxNode = (tSnmpTcpTxNode *) TMO_SLL_First (&pTcpSsn->TxList);
    }
    return SNMP_SUCCESS;
}

/******************************************************************************
 * Function           : SnmpTcpAddPktInTxList 
 * Input(s)           : i4SockFd - Sock Id in which the data is received.
                        pu1Buf - Data to store in the TX list 
 * Output(s)          : None.
 * Returns            : SNMP_SUCCESS or SNMP_FAILURE.
 * Action             : This function adds the data in TxList of session node. 
 ***************************************************************************/
INT4
SnmpTcpAddPktInTxList (INT4 i4SockFd, UINT1 *pu1Buf, UINT4 u4Size)
{
    tSnmpTcpSsn        *pTcpSsn = NULL;
    tSnmpTcpTxNode     *pTcpTxNode = NULL;
    UINT1              *pu1Pkt = NULL;

    pTcpTxNode = MemAllocMemBlk (gSnmpTcpTxNodePoolId);
    pu1Pkt = MemAllocMemBlk (gSnmpPktPoolId);

    if ((pTcpTxNode == NULL) || (pu1Pkt == NULL))
    {
        SNMPTrace ("\r\n SNMPTCP: Malloc failed in SnmpTcpAddPktInTxList");
        MemReleaseMemBlock (gSnmpTcpTxNodePoolId, (UINT1 *) pTcpTxNode);
        MemReleaseMemBlock (gSnmpPktPoolId, (UINT1 *) pu1Pkt);
        return SNMP_FAILURE;
    }
    MEMSET (pTcpTxNode, SNMP_ZERO, sizeof (tSnmpTcpTxNode));
    MEMSET (pu1Pkt, SNMP_ZERO, u4Size);

    MEMCPY (pu1Pkt, pu1Buf, u4Size);
    pTcpTxNode->pu1Msg = pu1Pkt;
    pTcpTxNode->u4MsgSize = u4Size;
    pTcpTxNode->u4MsgOffset = SNMP_ZERO;

    pTcpSsn = SnmpTcpFindSsnNode (i4SockFd);

    if (pTcpSsn == NULL)
    {
        SNMPTrace ("\r\n SNMPTCP: Adding Data Tx List Failed");
        MemReleaseMemBlock (gSnmpTcpTxNodePoolId, (UINT1 *) pTcpTxNode);
        MemReleaseMemBlock (gSnmpPktPoolId, (UINT1 *) pu1Pkt);
        return SNMP_FAILURE;
    }

    TMO_SLL_Add (&pTcpSsn->TxList, (tTMO_SLL_NODE *) & pTcpTxNode->TxNode);
    return SNMP_SUCCESS;
}

 /******************************************************************************
 * Function           : SnmpTcpRemovePktInTxList 
 * Input(s)           : i4SockFd - Sock Id in which the data is received.
                        pNode - Tx Buf Node
 * Output(s)          : None.
 * Returns            : SNMP_SUCCESS or SNMP_FAILURE.
 * Action             : This function removes the data in TxList of session node. 
 ***************************************************************************/
INT4
SnmpTcpRemovePktInTxList (tSnmpTcpTxNode * pNode, INT4 i4SockFd)
{
    tSnmpTcpSsn        *pTcpSsn = NULL;

    pTcpSsn = SnmpTcpFindSsnNode (i4SockFd);

    if (pTcpSsn == NULL)
    {
        SNMPTrace ("\r\n SNMPTCP: Removing Data from Tx List Failed");
        return SNMP_FAILURE;
    }

    TMO_SLL_Delete (&pTcpSsn->TxList, (tTMO_SLL_NODE *) & pNode->TxNode);

    MemReleaseMemBlock (gSnmpPktPoolId, (UINT1 *) pNode->pu1Msg);
    MemReleaseMemBlock (gSnmpTcpTxNodePoolId, (UINT1 *) pNode);
    return SNMP_SUCCESS;
}

 /******************************************************************************
 * Function           : SnmpTcpGetTxListCnt 
 * Input(s)           : i4SockFd - Sock Id in which the data is received.
 * Output(s)          : None.
 * Returns            : SNMP_SUCCESS or SNMP_FAILURE.
 * Action             : This function returns the Number of nodes in TxList. 
 ***************************************************************************/
INT4
SnmpTcpGetTxListCnt (INT4 i4SockFd)
{
    INT4                i4Cnt = SNMP_ZERO;
    tSnmpTcpSsn        *pTcpSsn = NULL;
    for (; i4Cnt < MAX_SNMP_TCP_CLIENTS_LIMIT; i4Cnt++)
    {
        pTcpSsn = gSnmpTcpGblInfo.apSnmpTcpSsn[i4Cnt];
        if (pTcpSsn->i4SockFd == i4SockFd)
        {
            return ((INT4) TMO_SLL_Count (&pTcpSsn->TxList));
        }
    }
    return 0;
}
