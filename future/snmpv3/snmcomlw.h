/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: snmcomlw.h,v 1.5 2015/04/28 12:35:02 siva Exp $
*
* Description: Proto types for community mapping table Low Level  Routines
*********************************************************************/
#ifndef _SNMCOMLW_H
#define _SNMCOMLW_H
/* Proto Validate Index Instance for SnmpCommunityTable. */
INT1
nmhValidateIndexInstanceSnmpCommunityTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for SnmpCommunityTable  */

INT1
nmhGetFirstIndexSnmpCommunityTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexSnmpCommunityTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetSnmpCommunityName ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetSnmpCommunitySecurityName ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetSnmpCommunityContextEngineID ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetSnmpCommunityContextName ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetSnmpCommunityTransportTag ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetSnmpCommunityStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetSnmpCommunityStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetSnmpCommunityName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetSnmpCommunitySecurityName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetSnmpCommunityContextEngineID ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetSnmpCommunityContextName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetSnmpCommunityTransportTag ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetSnmpCommunityStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetSnmpCommunityStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2SnmpCommunityName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2SnmpCommunitySecurityName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2SnmpCommunityContextEngineID ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2SnmpCommunityContextName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2SnmpCommunityTransportTag ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2SnmpCommunityStorageType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2SnmpCommunityStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2SnmpCommunityTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for SnmpTargetAddrExtTable. */
INT1
nmhValidateIndexInstanceSnmpTargetAddrExtTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for SnmpTargetAddrExtTable  */

INT1
nmhGetFirstIndexSnmpTargetAddrExtTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexSnmpTargetAddrExtTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetSnmpTargetAddrTMask ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetSnmpTargetAddrMMS ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetSnmpTargetAddrTMask ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetSnmpTargetAddrMMS ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2SnmpTargetAddrTMask ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2SnmpTargetAddrMMS ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2SnmpTargetAddrExtTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));



/* Proto type for Low Level GET Routine All Objects.  */
#endif
