
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: snmpcode.c,v 1.15 2017/11/20 13:11:26 siva Exp $
 *
 * Description: routines for BER encoding/decoding. 
 *******************************************************************/
#include "snmpcmn.h"
#include "snmpcode.h"

/*********************************************************************
*  Function Name :SNMPAddVarbindList
*  Description   :This procedure makes a varbind list.It i
*                 connects the incoming pVbPtr to the end of the list.
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT2
SNMPAddVarbindList (tSNMP_NORMAL_PDU * pMsgPtr, tSNMP_VAR_BIND * pVbPtr)
{
    if (pVbPtr == NULL)
    {
        return (SNMP_NOT_OK);
    }
    if ((pMsgPtr->pVarBindList == NULL) || (pMsgPtr->pVarBindEnd == NULL))
    {
        pMsgPtr->pVarBindList = pVbPtr;
        pMsgPtr->pVarBindEnd = pVbPtr;
        return (SNMP_OK);
    }
    /* Add varbind to the beginning */
    pMsgPtr->pVarBindEnd->pNextVarBind = pVbPtr;
    pMsgPtr->pVarBindEnd = pVbPtr;
    pVbPtr->pNextVarBind = NULL;
    return (SNMP_OK);
}

/*********************************************************************
*  Function Name :SNMPFreeNormalMsg
*  Description   :This procedure Frees the data structure of 
*                 type tSNMP_NORMAL_PDU allocated.
*
*  Parameter(s)  : 
*
*  Return Values : VOID
*********************************************************************/

VOID
SNMPFreeNormalMsg (tSNMP_NORMAL_PDU * pMsgPtr)
{
    if (pMsgPtr != NULL)
    {
        SNMPFreeVarbindList (pMsgPtr->pVarBindList);
    }
}

/*********************************************************************
*  Function Name :SNMPFreeV1TrapMsg
*  Description   :This procedure Frees the data structure of 
*                 type tSNMP_TRAP_PDU allocated.
*
*  Parameter(s)  : 
*
*  Return Values : VOID
*********************************************************************/
VOID
SNMPFreeV1TrapMsg (tSNMP_TRAP_PDU * pPdu)
{
    if (pPdu != NULL)
    {
        SNMPFreeVarbindList (pPdu->pVarBindList);
    }
}

/*********************************************************************
*  Function Name :SNMPFormOctetString 
*  Description   :This procedure creates the internal datastructure of type 
*            OctetString and puts the values in to it.
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/
tSNMP_OCTET_STRING_TYPE *
SNMPFormOctetString (UINT1 *pu1String, INT4 i4Length,
                     tSNMP_OCTET_STRING_TYPE * pOctetstring)
{
    if (i4Length > SNMP_MAX_OCTETSTRING_SIZE)
    {
        SNMPTrace ("FormOctet String: Length > Max Length\n");
        i4Length = SNMP_MAX_OCTETSTRING_SIZE;
    }
    if (pOctetstring == NULL)
    {
        return (NULL);
    }
    pOctetstring->i4_Length = i4Length;
    if (i4Length != SNMP_ZERO)
    {
        MEMCPY ((VOID *) pOctetstring->pu1_OctetList,
                (VOID *) pu1String, i4Length);
    }
    return (pOctetstring);
}

/*********************************************************************
*  Function Name :SNMPFreeVarbindList
*  Description   :This procedure Frees the data structure of type  tSNMP_VAR_BIND
*                       allocated
*
*
*  Parameter(s)  :
*
*  Return Values : VOID
*********************************************************************/
VOID
SNMPFreeVarbindList (tSNMP_VAR_BIND * pVbPtr)
{
    tSNMP_VAR_BIND     *pCur = pVbPtr, *pNext = NULL;
    while (pCur != NULL)
    {
        pNext = pCur->pNextVarBind;
        SNMPFreeVarBind (pCur);
        pCur = pNext;
    }
}

/*********************************************************************
*  Function Name :SNMPEncodeGetPacket
*  Description   : This procedure encodes the GET-Response Message in to BER 
*           encoded format.
*           It writes the opcodes at relevant places to distinguish the type 
*           of objects.As the SNMP Message does not have fixed length format
*           so it allows for encoding variable size data and variable number
*           of objects.The lengths of the values precede the value in the
*           data packet. The encoding is done from the last object and put 
*           in the array    backwards.Later on the entire array is reversed.
*
*  Parameter(s)  :
*
*  Return Values :returns pdu ptr
*********************************************************************/

INT4
SNMPEncodeGetPacket (tSNMP_NORMAL_PDU * pMsgPtr, UINT1 **ppu1DataOut,
                     INT4 *pi4PktLen, INT4 i4TypeOfMsg)
{
    INT4                i4MsgLen = 0;
    INT4                i4TotLen = 0;
    UINT1              *pu1DataPtr = NULL;
    UINT1              *pu1StartPtr = NULL;
    INT4                i4EstLen = 0;

    if (pMsgPtr == NULL)
    {
        return (SNMP_NOT_OK);
    }
    i4EstLen = SNMPEstimateNormalPacketLength (pMsgPtr);

    if (i4EstLen > MAX_PKT_LENGTH)
    {
        *pi4PktLen = i4EstLen;
        /* Return a too big Error status,
         * when asked for a larger sized response that exceeds the 
         * maximum allowed packet length.
         */
        pMsgPtr->i4_ErrorStatus = SNMP_ERR_TOO_BIG;
        SNMPTrace ("EncodeGet: Packet Size exceed failed\n");
        return SNMP_NOT_OK;
    }
    else
        pu1DataPtr = gau1OutDat;

    pu1StartPtr = pu1DataPtr;
    if (SNMPWriteGetMsg (&pu1DataPtr, pMsgPtr, i4TypeOfMsg) == SNMP_NOT_OK)
    {
        SNMPTrace ("EncodeGet: WriteGet failed\n");
        return (SNMP_NOT_OK);
    }
    i4MsgLen = pu1DataPtr - pu1StartPtr;
    if (SNMPWriteLength (&pu1DataPtr, i4MsgLen) == SNMP_NOT_OK)
    {
        SNMPTrace ("EncodeGet: SNMP_AGT_WriteLength failed!\n");
        return (SNMP_NOT_OK);
    }

    *pu1DataPtr = SNMP_GET_LAST_BYTE (i4TypeOfMsg);
    /*  *pu1DataPtr = (UINT1) (0xff & i4TypeOfMsg); */
    pu1DataPtr++;
    if (SNMPWriteOctetstring (&pu1DataPtr, pMsgPtr->pCommunityStr,
                              SNMP_DATA_TYPE_OCTET_PRIM) == SNMP_NOT_OK)
    {
        SNMPTrace ("EncodeGet: WriteOctectString failed!\n");
        return (SNMP_NOT_OK);
    }
    if (SNMPWriteUnsignedint (&pu1DataPtr, pMsgPtr->u4_Version,
                              SNMP_DATA_TYPE_INTEGER32) == SNMP_NOT_OK)
    {
        SNMPTrace ("EncodeGet: WriteUInt failed!\n");
        return (SNMP_NOT_OK);
    }
    i4TotLen = pu1DataPtr - pu1StartPtr;
    if (SNMPWriteLength (&pu1DataPtr, i4TotLen) == SNMP_NOT_OK)
    {
        SNMPTrace ("EncodeGet: WriteTotalLength failed!\n");
        return (SNMP_NOT_OK);
    }
    *pu1DataPtr = SNMP_DATA_TYPE_SEQUENCE;
    pu1DataPtr++;
    *pi4PktLen = pu1DataPtr - pu1StartPtr;
    SNMPReversePacket (pu1StartPtr, ppu1DataOut, *pi4PktLen);
    return (SNMP_OK);
}

/*********************************************************************
*  Function Name :SNMPEstimateNormalPacketLength
*  Description   :This function estimates the length of tSNMP_NORMAL_PDU Message
*
*
*  Parameter(s)  :pdu ptr
*
*  Return Values :length of the pdu
*********************************************************************/

INT4
SNMPEstimateNormalPacketLength (tSNMP_NORMAL_PDU * pMsgPtr)
{
    INT4                i4VbLen = 0, i4MsgLen = 0;
    INT4                i4Counter = 0;
    i4VbLen = SNMPEstimateVarbindLength (pMsgPtr->pVarBindList);
    i4Counter = i4VbLen + SNMPEstimateLengthLength (i4VbLen);
    i4Counter++;                /* SNMP_DATA_TYPE_SEQUENCE  */
    i4Counter += SNMPEstimateSignedintLength (pMsgPtr->i4_ErrorIndex);
    i4Counter += SNMPEstimateSignedintLength (pMsgPtr->i4_ErrorStatus);
    i4Counter =
        (INT4) (i4Counter +
                SNMPEstimateSignedintLength ((INT4) pMsgPtr->u4_RequestID));
    i4MsgLen = i4Counter;
    i4Counter += SNMPEstimateLengthLength (i4MsgLen);
    i4Counter++;                /* (0xff & type_of_message)  */
    if (pMsgPtr->pCommunityStr != NULL)
    {
        i4Counter += SNMPEstimateOctetstringLength (pMsgPtr->pCommunityStr);
    }
    i4Counter += SNMPEstimateUnsignedintLength (pMsgPtr->u4_Version);
    i4Counter += SNMPEstimateLengthLength (i4Counter);
    i4Counter++;                /* SNMP_DATA_TYPE_SEQUENCE  */
    return (i4Counter);
}

/*********************************************************************
*  Function Name :SNMPEstimateVarbindLength
*  Description   :This procedure estimates the length of the tSNMP_VAR_BIND 
*
*
*  Parameter(s)  : variable ptr
*
*  Return Values : length
*********************************************************************/

INT4
SNMPEstimateVarbindLength (tSNMP_VAR_BIND * pVbPtr)
{
    INT4                i4VbLen = 0;
    INT4                i4Counter = 0;

    i4Counter = 0;
    while (pVbPtr != NULL)
    {
        i4VbLen = i4Counter;
        switch (pVbPtr->ObjValue.i2_DataType)
        {
            case SNMP_DATA_TYPE_COUNTER32:
            case SNMP_DATA_TYPE_GAUGE32:
                /* this is moved here since TimeTicks is an unsigned value */
            case SNMP_DATA_TYPE_TIME_TICKS:
                i4Counter += SNMPEstimateUnsignedintLength
                    (pVbPtr->ObjValue.u4_ULongValue);
                break;

            case SNMP_DATA_TYPE_COUNTER64:
                i4Counter += SNMPEstimateCounter64Length
                    (pVbPtr->ObjValue.u8_Counter64Value);
                break;
            case SNMP_EXCEPTION_NO_SUCH_OBJECT:
            case SNMP_EXCEPTION_NO_SUCH_INSTANCE:
            case SNMP_EXCEPTION_END_OF_MIB_VIEW:
                i4Counter += SNMP_TWO;
                break;

            case SNMP_DATA_TYPE_INTEGER32:
                i4Counter += SNMPEstimateSignedintLength
                    (pVbPtr->ObjValue.i4_SLongValue);
                break;
            case SNMP_DATA_TYPE_OBJECT_ID:
                i4Counter += SNMPEstimateOidLength (pVbPtr->ObjValue.pOidValue);
                break;
            case SNMP_DATA_TYPE_OCTET_PRIM:
            case SNMP_DATA_TYPE_OPAQUE:
                i4Counter += SNMPEstimateOctetstringLength
                    (pVbPtr->ObjValue.pOctetStrValue);
                break;

            case SNMP_DATA_TYPE_IP_ADDR_PRIM:
                pVbPtr->ObjValue.pOctetStrValue->i4_Length = SNMP_IPADDR_LEN;
                i4Counter += SNMPEstimateOctetstringLength
                    (pVbPtr->ObjValue.pOctetStrValue);
                break;
            case SNMP_DATA_TYPE_NULL:
                i4Counter += SNMP_TWO;    /* 0x00  SNMP_DATA_TYPE_NULL */
                break;
            default:
                return (SNMP_NOT_OK);
        }                        /* end of switch */
        i4Counter += SNMPEstimateOidLength (pVbPtr->pObjName);
        i4VbLen = i4Counter - i4VbLen;
        i4Counter += SNMPEstimateLengthLength (i4VbLen);
        i4Counter++;            /*  SNMP_DATA_TYPE_SEQUENCE */
        pVbPtr = pVbPtr->pNextVarBind;
    }
    return (i4Counter);
}

/*********************************************************************
*  Function Name :SNMPEstimateOidLength
*  Description   :This procedure estimates the length of OID
*
*
*  Parameter(s)  :oid ptr
*
*  Return Values : length of the OID
*********************************************************************/

INT4
SNMPEstimateOidLength (tSNMP_OID_TYPE * pOidPtr)
{
    INT4                i4Seq = 0, i4Counter = 0;
    UINT4               i4Check = 0;
    INT4                i4Len = 0, i4Count = 0;

    if (pOidPtr->u4_Length > SNMP_TWO)
    {
        for (i4Seq = (INT4) pOidPtr->u4_Length - SNMP_ONE; i4Seq > SNMP_ONE;
             i4Seq--)
        {
            i4Counter++;
            i4Count = SNMP_ONELONG;
            i4Check = 0x80;
            while ((pOidPtr->pu4_OidList[i4Seq] >= (UINT4) i4Check) &&
                   (i4Count < 5))
            {
                i4Check <<= 7;
                i4Counter++;
                i4Count++;
            }
        }
    }
    i4Counter++;
    i4Len = i4Counter;
    i4Counter += SNMPEstimateLengthLength (i4Len);
    i4Counter++;
    return (i4Counter);
}

/*********************************************************************
*  Function Name :SNMPEstimateOctetstringLength
*  Description   :This procedure eestimates the length of OctetString
*
*
*  Parameter(s)  : octet string ptr
*
*  Return Values : length
*********************************************************************/

INT4
SNMPEstimateOctetstringLength (tSNMP_OCTET_STRING_TYPE * pOctetString)
{
    INT4                i4Seq = 0, i4Counter = 0;
    for (i4Seq = pOctetString->i4_Length - SNMP_ONE;
         i4Seq >= SNMP_ZERO; i4Seq--)
    {
        i4Counter++;
    }
    i4Counter += SNMPEstimateLengthLength (pOctetString->i4_Length);
    i4Counter++;
    return (i4Counter);
}

/*********************************************************************
*  Function Name :SNMPWriteGetMsg
*  Description   :This procedure encodes the fields corresponding to 
*                 GET-Response  Message. 
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT4
SNMPWriteGetMsg (UINT1 **ppu1CurrPtr, tSNMP_NORMAL_PDU * pMsgPtr,
                 INT4 i4TypeOfMsg)
{
    UINT1              *pu1StartPtr = NULL;
    INT4                i4VbLen = 0;

    pu1StartPtr = *ppu1CurrPtr;

    /*
     * Before Writing VarBind Lists, Reverse the VarBinds
     * in Place, so that the PDU Reversal later would encode
     * the VarBind in the Correct Order.
     */
    SNMPReverseVarBindList (&(pMsgPtr->pVarBindList));
    SNMPWriteVarbind (ppu1CurrPtr, pMsgPtr->pVarBindList);
    i4VbLen = *ppu1CurrPtr - pu1StartPtr;
    if (SNMPWriteLength (ppu1CurrPtr, i4VbLen) == SNMP_NOT_OK)
    {
        return (SNMP_NOT_OK);
    }
    *(*ppu1CurrPtr)++ = SNMP_DATA_TYPE_SEQUENCE;
    if ((pMsgPtr->i2_PduType == SNMP_PDU_TYPE_GET_BULK_REQUEST) &&
        (i4TypeOfMsg != SNMP_PDU_TYPE_GET_RESPONSE))
    {
        if ((SNMPWriteSignedint
             (ppu1CurrPtr, pMsgPtr->i4_MaxRepetitions,
              SNMP_DATA_TYPE_INTEGER32) == SNMP_NOT_OK)
            ||
            (SNMPWriteSignedint
             (ppu1CurrPtr, pMsgPtr->i4_NonRepeaters,
              SNMP_DATA_TYPE_INTEGER32) == SNMP_NOT_OK))
        {
            return (SNMP_NOT_OK);
        }
    }
    else
    {

        if ((SNMPWriteSignedint
             (ppu1CurrPtr, pMsgPtr->i4_ErrorIndex,
              SNMP_DATA_TYPE_INTEGER32) == SNMP_NOT_OK)
            ||
            (SNMPWriteSignedint
             (ppu1CurrPtr, pMsgPtr->i4_ErrorStatus,
              SNMP_DATA_TYPE_INTEGER32) == SNMP_NOT_OK))
        {
            return (SNMP_NOT_OK);
        }
    }
    if (SNMPWriteSignedint (ppu1CurrPtr, (INT4) pMsgPtr->u4_RequestID,
                            SNMP_DATA_TYPE_INTEGER32) == SNMP_NOT_OK)
    {
        return (SNMP_NOT_OK);
    }
    SNMPReverseVarBindList (&(pMsgPtr->pVarBindList));
    return (SNMP_OK);

}

/*********************************************************************
*  Function Name :SNMPWriteLength
*  Description   :This procedure encodes the length of the values
*          The length is written in the data according to the BER format.
*          The values are written backwards.
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT4
SNMPWriteLength (UINT1 **ppu1CurrPtr, INT4 i4Len)
{
    INT4                i4LenOfLen = SNMP_NOT_OK, i4Seq = 0;

    i4LenOfLen = (INT4) SNMPFindLength (i4Len);
    if (i4LenOfLen == SNMP_NOT_OK)
    {
        return (SNMP_NOT_OK);
    }
    if (i4LenOfLen == SNMP_ONE)
    {
        *(*ppu1CurrPtr)++ = (UINT1) i4Len;
        return (SNMP_OK);
    }
    for (i4Seq = SNMP_ONE; i4Seq < i4LenOfLen; i4Seq++)
    {
        *(*ppu1CurrPtr)++ = (UINT1) (((UINT4) i4Len >>
                                      (SNMP_EIGHT *
                                       ((UINT4) i4Seq - SNMP_ONE))) & 0x0FF);
    }
    *(*ppu1CurrPtr)++ = (UINT1) ((UINT1) 0x80 + (UINT1) i4LenOfLen - SNMP_ONE);
    return (SNMP_OK);
}

/*********************************************************************
*  Function Name :SNMPWriteOctetstring
*  Description   :This procedure encodes the OctetString structure
*          The values are written backwards
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT4
SNMPWriteOctetstring (UINT1 **ppu1CurrPtr,
                      tSNMP_OCTET_STRING_TYPE * pOctetString, INT2 i2Type)
{
    INT4                i4Seq = 0;
    for (i4Seq = pOctetString->i4_Length - SNMP_ONE;
         i4Seq >= SNMP_ZERO; i4Seq--)
    {
        *(*ppu1CurrPtr)++ = pOctetString->pu1_OctetList[i4Seq];
    }
    SNMPWriteLength (ppu1CurrPtr, pOctetString->i4_Length);
    *(*ppu1CurrPtr)++ = (UINT1) (0xff & i2Type);
    return (SNMP_OK);
}

/*********************************************************************
*  Function Name :SNMPWriteUnsignedint
*  Description   : This procedure encodes the unsigned integers
*           The values are written backwards.
*
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/
INT4
SNMPWriteUnsignedint (UINT1 **ppu1CurrPtr, UINT4 u4Value, INT2 i2Type)
{
    UINT4               u4Count = 0;
    UINT4               u4Check = 0, u4TempValue = 0;

    u4Count = SNMP_ONELONG;
    u4Check = 0xff;
    /* WINSNMP Warnigs */
    *(*ppu1CurrPtr)++ = (UINT1) u4Value & 0xff;
    u4TempValue = u4Value & 0xff;
    while ((u4TempValue != u4Value) && (u4Count < SNMP_FOUR))
    {
        /* WINSNMP Warnigs */
        *(*ppu1CurrPtr)++ = (UINT1) (u4Value >> SNMP_EIGHT * u4Count) & 0xff;
        u4TempValue |= (u4Check << (SNMP_EIGHT * u4Count)) & u4Value;
        u4Count++;
    }
    if (((u4Value >> SNMP_EIGHT * (u4Count - SNMP_ONE)) & 0x80) != 0)
    {
        *(*ppu1CurrPtr)++ = SNMP_ZERO;
        u4Count++;
    }
    SNMPWriteLength (ppu1CurrPtr, (INT4) u4Count);
    *(*ppu1CurrPtr)++ = (UINT1) (0xff & i2Type);
    return (SNMP_OK);
}

/*********************************************************************
*  Function Name :SNMPEstimateUnsignedintLength
*  Description   :This procedure estimates the length of unsigned integers.
*
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT4
SNMPEstimateUnsignedintLength (UINT4 u4Value)
{
    INT4                i4Counter = 0;
    UINT4               u4Check = 0, u4TempValue = 0;
    UINT4               i4Count = 0;
    i4Count = SNMP_ONELONG;
    u4Check = 0xff;
    i4Counter++;
    u4TempValue = u4Value & 0xff;
    while ((u4TempValue != u4Value) && (i4Count < SNMP_FOUR))
    {
        i4Counter++;
        u4TempValue |= (u4Check << (SNMP_EIGHT * i4Count)) & u4Value;
        i4Count++;
    }
    if (((u4Value >> SNMP_EIGHT * (i4Count - SNMP_ONE)) & 0x80) != 0)
    {
        i4Counter++;
        i4Count++;
    }
    i4Counter = (INT4) (i4Counter + SNMPEstimateLengthLength ((INT4) i4Count));
    i4Counter++;
    return (i4Counter);

}

/*********************************************************************
*  Function Name :SNMPEstimateLengthLength
*  Description   :This procedure estimates the length of the 
*                 length of the values.
*
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT4
SNMPEstimateLengthLength (INT4 i4Len)
{
    INT4                i4LenOfLen, i4Seq, i4Counter = 0;

    i4LenOfLen = (INT4) SNMPFindLength (i4Len);
    if (i4LenOfLen == SNMP_ONE)
    {
        i4Counter++;
        return (i4Counter);
    }
    for (i4Seq = SNMP_ONE; i4Seq < i4LenOfLen; i4Seq++)
    {
        i4Counter++;
    }
    i4Counter++;
    return (i4Counter);

}

/*********************************************************************
*  Function Name :SNMPFindLength
*  Description   :This procedure finds the length of the values
*
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT4
SNMPFindLength (INT4 i4Len)
{
    /* single octet */
    if (i4Len < 128)
    {
        return (SNMP_ONE);
    }
    if (i4Len < 0x0100)
    {
        return (SNMP_TWO);
    }
    if (i4Len < 0x010000)
    {
        return (3);
    }
    if (i4Len < 0x01000000)
    {
        return (SNMP_FOUR);
    }
    return (SNMP_FOUR);
}

/*********************************************************************
*  Function Name :SNMPEstimateCounter64Length 
*  Description   :This procedure estimates the length of Counter64
*
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT4
SNMPEstimateCounter64Length (tSNMP_COUNTER64_TYPE u8Value)
{
    INT4                i4Count = 0, i4Counter = 0;
    UINT4               u4Check = 0, u4TempValue = 0;
    UINT4               u4Count2 = 0, u4Count1 = 0;

    u4Count1 = SNMP_ONELONG;
    u4Check = 0xff;
    i4Counter++;
    u4TempValue = u8Value.lsn & 0xff;
    while ((u4TempValue != u8Value.lsn) && (u4Count1 < SNMP_FOUR))
    {
        i4Counter++;
        u4TempValue |= (u4Check << (SNMP_EIGHT * u4Count1)) & u8Value.lsn;
        u4Count1++;
    }

    u4Count2 = 0L;

    if (u8Value.msn > SNMP_ZERO)
    {
        u4Count2 = SNMP_ONELONG;
        u4Check = 0xff;
        i4Counter++;
        u4TempValue = u8Value.msn & 0xff;
        while ((u4TempValue != u8Value.msn) && (u4Count2 < SNMP_FOUR))
        {
            i4Counter++;
            u4TempValue |= (u4Check << (SNMP_EIGHT * u4Count2)) & u8Value.msn;
            u4Count2++;
        }
    }

    if (((u8Value.msn >> SNMP_EIGHT * (u4Count2 - SNMP_ONE)) & 0x80) !=
        SNMP_ZERO)
    {
        i4Counter++;
        u4Count2++;
    }
    else
    {
        if (((u8Value.lsn >> SNMP_EIGHT * (u4Count1 - SNMP_ONE)) & 0x80) !=
            SNMP_ZERO)
        {
            i4Counter++;
            u4Count2++;
        }
    }

    i4Count = (INT4) (u4Count1 + u4Count2);

    i4Counter += SNMPEstimateLengthLength (i4Count);
    i4Counter++;
    return (i4Counter);

}

/*********************************************************************
*  Function Name :SNMPEstimateSignedintLength
*  Description   :This procedure estimates the length of signed integers
*
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT4
SNMPEstimateSignedintLength (INT4 i4Value)
{
    UINT4               u4Count = 0, u4Times = 0;
    INT4                i4Counter = 0;
    INT4                i4TempValue = 0;
    INT4                i4Temp = 0;
    UINT4               u4Check = 0;

    u4Count = SNMP_ONE;
    u4Check = 0xff;

    if ((((UINT4) i4Value & 0xff000000) == 0xff000000)
        && (i4Value & 0x00800000))
    {
        u4Times = 3;
    }
    else
    {
        u4Times = SNMP_FOUR;
    }
    i4Counter++;
    i4TempValue = (INT4) ((UINT4) i4Value & u4Check);
    i4Temp = SNMP_ZERO;
    while ((i4TempValue != i4Value) && (u4Count < u4Times))
    {
        i4Counter++;
        i4TempValue =
            (INT4) ((UINT4) i4TempValue |
                    ((u4Check << (SNMP_EIGHT * u4Count)) & (UINT4) i4Value));
        u4Count++;
        i4Temp = (i4Value >> SNMP_EIGHT * u4Count) & (INT4) u4Check;
    }
    if ((i4Temp == SNMP_ZERO) && (u4Count != SNMP_FOUR))
    {
        if (((i4Value >> (SNMP_EIGHT * (u4Count - SNMP_ONE))) & 0x80) !=
            SNMP_ZERO)
        {
            i4Counter++;
            u4Count++;
        }
    }
    i4Counter += SNMPEstimateLengthLength ((INT4) u4Count);
    i4Counter++;
    return (i4Counter);

}

/*********************************************************************
*  Function Name :SNMPReverseVarBindList 
*  Description   :This Procedure Reverses the Var-Bind Lists present
*          in the Output Message
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

VOID
SNMPReverseVarBindList (tSNMP_VAR_BIND ** pVarBindList)
{
    tSNMP_VAR_BIND     *pTmp = NULL, *pStart = NULL, *pNewStart = NULL;

    pNewStart = NULL;
    pStart = *pVarBindList;

    while (pStart != NULL)
    {
        pTmp = pStart;
        pStart = pStart->pNextVarBind;
        if (pNewStart == NULL)
        {
            pNewStart = pTmp;
            pNewStart->pNextVarBind = NULL;
        }
        else
        {
            pTmp->pNextVarBind = pNewStart;
            pNewStart = pTmp;
        }
    }
    *pVarBindList = pNewStart;
}

/*********************************************************************
*  Function Name :SNMPWriteVarbind
*  Description   :This procedure encodes the tSNMP_VAR_BIND structure.
*          The values are written backwards.    
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT4
SNMPWriteVarbind (UINT1 **ppu1CurrPtr, tSNMP_VAR_BIND * pVbPtr)
{
    INT4                u4RetVal = 0;
    UINT1              *pu1StartPtr = NULL;
    INT4                i4VbLen = 0;
    while (pVbPtr != NULL)
    {
        pu1StartPtr = *ppu1CurrPtr;
        switch (pVbPtr->ObjValue.i2_DataType)
        {
            case SNMP_DATA_TYPE_COUNTER32:
            case SNMP_DATA_TYPE_GAUGE32:
            case SNMP_DATA_TYPE_TIME_TICKS:
                u4RetVal = SNMPWriteUnsignedint (ppu1CurrPtr,
                                                 pVbPtr->ObjValue.u4_ULongValue,
                                                 pVbPtr->ObjValue.i2_DataType);
                break;

            case SNMP_DATA_TYPE_COUNTER64:
                SNMPWriteCounter64 (ppu1CurrPtr,
                                    pVbPtr->ObjValue.u8_Counter64Value,
                                    pVbPtr->ObjValue.i2_DataType);
                break;

            case SNMP_DATA_TYPE_INTEGER32:
                u4RetVal = SNMPWriteSignedint (ppu1CurrPtr,
                                               pVbPtr->ObjValue.i4_SLongValue,
                                               pVbPtr->ObjValue.i2_DataType);
                break;
            case SNMP_DATA_TYPE_OBJECT_ID:
                if (SNMPWriteOid (ppu1CurrPtr, pVbPtr->ObjValue.pOidValue)
                    == SNMP_NOT_OK)
                {
                    return (SNMP_NOT_OK);
                }
                break;
            case SNMP_DATA_TYPE_OCTET_PRIM:
            case SNMP_DATA_TYPE_OPAQUE:
                u4RetVal = SNMPWriteOctetstring (ppu1CurrPtr,
                                                 pVbPtr->ObjValue.
                                                 pOctetStrValue,
                                                 pVbPtr->ObjValue.i2_DataType);
                break;
            case SNMP_DATA_TYPE_IP_ADDR_PRIM:
                pVbPtr->ObjValue.u4_ULongValue =
                    OSIX_HTONL (pVbPtr->ObjValue.u4_ULongValue);
                memcpy (pVbPtr->ObjValue.pOctetStrValue->pu1_OctetList,
                        &(pVbPtr->ObjValue.u4_ULongValue), SNMP_FOUR);
                pVbPtr->ObjValue.pOctetStrValue->i4_Length = SNMP_FOUR;
                u4RetVal = SNMPWriteOctetstring (ppu1CurrPtr,
                                                 pVbPtr->ObjValue.
                                                 pOctetStrValue,
                                                 pVbPtr->ObjValue.i2_DataType);
                break;
            case SNMP_DATA_TYPE_NULL:
                SNMPWriteNull (ppu1CurrPtr);
                break;

            case SNMP_EXCEPTION_NO_SUCH_OBJECT:
            case SNMP_EXCEPTION_NO_SUCH_INSTANCE:
            case SNMP_EXCEPTION_END_OF_MIB_VIEW:
                SNMPWriteException (ppu1CurrPtr, pVbPtr->ObjValue.i2_DataType);
                break;

            default:
                return (SNMP_NOT_OK);
        }
        /* end of switch */

        if (SNMPWriteOid (ppu1CurrPtr, pVbPtr->pObjName) == SNMP_NOT_OK)
        {
            return (SNMP_NOT_OK);
        }
        i4VbLen = *ppu1CurrPtr - pu1StartPtr;
        if (SNMPWriteLength (ppu1CurrPtr, i4VbLen) == SNMP_NOT_OK)
        {
            return (SNMP_NOT_OK);
        }
        *(*ppu1CurrPtr)++ = SNMP_DATA_TYPE_SEQUENCE;
        pVbPtr = pVbPtr->pNextVarBind;
    }
    UNUSED_PARAM (u4RetVal);
    return (SNMP_OK);
}

/*********************************************************************
*  Function Name :SNMPWriteCounter64
*  Description   :This procedure encodes the Counter64 Value
*          The values are written backwards
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT4
SNMPWriteCounter64 (UINT1 **ppu1CurrPtr,
                    tSNMP_COUNTER64_TYPE u8Value, INT2 i2Type)
{
    INT4                i4Count = 0;
    UINT4               u4Count1 = 0, u4Count2 = 0;
    UINT4               u4Check = 0, u4TempValue = 0;

    u4Count1 = SNMP_ONELONG;
    u4Check = 0xff;
    *(*ppu1CurrPtr)++ = (UINT1) u8Value.lsn & 0xff;
    u4TempValue = u8Value.lsn & 0xff;
    while (u4Count1 < SNMP_FOUR)
    {
        *(*ppu1CurrPtr)++ =
            (UINT1) (u8Value.lsn >> SNMP_EIGHT * u4Count1) & 0xff;
        u4TempValue |= (u4Check << (SNMP_EIGHT * u4Count1)) & u8Value.lsn;
        u4Count1++;
    }

    u4Count2 = 0L;

    if (u8Value.msn > SNMP_ZERO)
    {
        u4Count2 = SNMP_ONELONG;
        u4Check = 0xff;
        *(*ppu1CurrPtr)++ = (UINT1) u8Value.msn & 0xff;
        u4TempValue = u8Value.msn & 0xff;
        while (u4Count2 < SNMP_FOUR)
        {
            *(*ppu1CurrPtr)++ =
                (UINT1) (u8Value.msn >> SNMP_EIGHT * u4Count2) & 0xff;
            u4TempValue |= (u4Check << (SNMP_EIGHT * u4Count2)) & u8Value.msn;
            u4Count2++;
        }
    }

    if ((((u8Value.msn >> SNMP_EIGHT) * (u4Count2 - SNMP_ONE)) & 0x80) !=
        SNMP_ZERO)
    {
        *(*ppu1CurrPtr)++ = SNMP_ZERO;
        u4Count2++;
    }
    else
    {
        if (((u8Value.lsn >> SNMP_EIGHT * (u4Count1 - SNMP_ONE)) & 0x80) !=
            SNMP_ZERO)
        {
            *(*ppu1CurrPtr)++ = SNMP_ZERO;
            u4Count2++;
        }
    }

    i4Count = (INT4) (u4Count1 + u4Count2);

    SNMPWriteLength (ppu1CurrPtr, i4Count);
    *(*ppu1CurrPtr)++ = (UINT1) (0xff & i2Type);
    return (SNMP_OK);
}

/*********************************************************************
*  Function Name :SNMPWriteSignedint
*  Description   :This procedure encodes the signed integers.The values 
*          are written backwards
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT4
SNMPWriteSignedint (UINT1 **pu1Curr, INT4 i4Value, INT2 i2Type)
{
    UINT4               u4Count = 0, u4Times = 0;
    UINT4               u4TempValue = 0;
    INT4                i4Temp = 0;
    UINT4               u4Check = 0;

    u4Count = SNMP_ONE;
    u4Check = 0xff;

    /*
     * Checking Whether the Number has the First 9 Bytes as One's or
     * Zero's. If so then the First Octet (which is 0xFF) is not
     * needed.  So if First 9 Bytes are One's then We Encode only
     * 3 Bytes and leave the First Octet (which is 0xFF) else we
     * encode all the 4 bytes.
     */
    if ((((UINT4) i4Value & 0xff000000) == 0xff000000)
        && (i4Value & 0x00800000))
    {
        u4Times = 3;
    }
    else
    {
        u4Times = SNMP_FOUR;
    }
    *(*pu1Curr)++ = (UINT1) i4Value & 0xff;
    u4TempValue = (UINT4) i4Value & u4Check;
    i4Temp = SNMP_ZERO;
    while ((u4TempValue != (UINT4) i4Value) && (u4Count < u4Times))
    {
        *(*pu1Curr)++ = (UINT1) (i4Value >> SNMP_EIGHT * u4Count) & 0xff;
        u4TempValue |= (u4Check << (SNMP_EIGHT * u4Count)) & (UINT4) i4Value;
        u4Count++;
        i4Temp = (i4Value >> SNMP_EIGHT * u4Count) & (INT4) u4Check;
    }
    if ((i4Temp == SNMP_ZERO) && (u4Count != SNMP_FOUR))
    {
        if (((i4Value >> SNMP_EIGHT * (u4Count - SNMP_ONE)) & 0x80) !=
            SNMP_ZERO)
        {
            *(*pu1Curr)++ = 00;
            u4Count++;
        }
    }
    SNMPWriteLength (pu1Curr, (INT4) u4Count);
    *(*pu1Curr)++ = (UINT1) (0xff & i2Type);
    return (SNMP_OK);
}

/*********************************************************************
*  Function Name :SNMPWriteOid
*  Description   :This procedure encodes the OID structure.
*          The values are written backwards.     
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT4
SNMPWriteOid (UINT1 **ppu1CurrPtr, tSNMP_OID_TYPE * pOidPtr)
{
    UINT1              *pu1StartPtr = NULL;
    UINT4               u4Cnt = 0, u4Check = 0, u4Count = 0;
    INT4                i4Len = 0;

    pu1StartPtr = *ppu1CurrPtr;
    if (pOidPtr->u4_Length > SNMP_TWO)
    {
        for (u4Cnt = pOidPtr->u4_Length - SNMP_ONE; u4Cnt > SNMP_ONE; u4Cnt--)
        {
            *(*ppu1CurrPtr)++ = (UINT1) pOidPtr->pu4_OidList[u4Cnt] & 0x7f;
            u4Count = SNMP_ONELONG;
            u4Check = 0x80;
            while ((pOidPtr->pu4_OidList[u4Cnt] >= u4Check) && (u4Count < 5))
            {
                u4Check <<= 7;
                *(*ppu1CurrPtr)++ = (UINT1) (((pOidPtr->pu4_OidList[u4Cnt]) >>
                                              7 * (u4Count)) | 0x80);
                u4Count++;
            }
        }
    }
    if (pOidPtr->u4_Length < SNMP_TWO)
    {
        *(*ppu1CurrPtr)++ = (UINT1) (pOidPtr->pu4_OidList[SNMP_ZERO] * 40);
    }
    else
    {
        *(*ppu1CurrPtr)++ = (UINT1) ((pOidPtr->pu4_OidList[SNMP_ZERO] * 40) +
                                     pOidPtr->pu4_OidList[SNMP_ONE]);
    }
    i4Len = *ppu1CurrPtr - pu1StartPtr;
    if (SNMPWriteLength (ppu1CurrPtr, i4Len) == SNMP_NOT_OK)
    {
        return (SNMP_NOT_OK);
    }
    *(*ppu1CurrPtr)++ = SNMP_DATA_TYPE_OBJECT_ID;
    return (SNMP_OK);
}

/*********************************************************************
*  Function Name :SNMPWriteNull
*  Description   : This procedure encodes the NULL type values
*
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

VOID
SNMPWriteNull (UINT1 **ppu1CurrPtr)
{
    *(*ppu1CurrPtr)++ = 0x00;
    *(*ppu1CurrPtr)++ = SNMP_DATA_TYPE_NULL;
}

/*********************************************************************
*  Function Name :SNMPWriteException
*  Description   :This procedure encodes the NULL type values
*
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

VOID
SNMPWriteException (UINT1 **ppu1CurrPtr, INT2 i2DataType)
{
    *(*ppu1CurrPtr)++ = 0x00;
    *(*ppu1CurrPtr)++ = (UINT1) (i2DataType & 0xff);
}

/*********************************************************************
*  Function Name :SNMPDecodePacket
*  Description   :This procedure decodes the raw data that comes 
*                 to the SNMP Agent. This looks at the opcode and 
*                 the length of the data and decodes the packet 
*                 accordingly. The decoded packet is put in the 
*                 internal datastructure.
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT1               *
SNMPDecodePacket (UINT1 *pu1PktPtr, INT4 i4PktLen, INT2 *pPduType)
{
    tSNMP_TRAP_PDU     *pV1Trap = NULL;
    tSNMP_NORMAL_PDU   *pMsgPtr = NULL;
    tSNMP_VAR_BIND     *pVarBindPtr = NULL;
    UINT1              *pu1CurrPtr = NULL;
    UINT1              *pu1EndPtr = NULL;
    UINT1              *pu1NewEndPtr = NULL;
    UINT4               u4Version = 0;
    INT4                i4TotalLength = 0;
    INT4                i4Len = 0;
    INT2                i2Type = 0;
    INT2                i2MsgType = 0;

    pu1CurrPtr = pu1PktPtr;
    pu1EndPtr = pu1CurrPtr + i4PktLen;
    i4TotalLength = SNMPDecodeSequence (&pu1CurrPtr, pu1EndPtr);
    if (i4TotalLength == SNMP_NOT_OK)
    {
        SNMPTrace ("Decode Seq Failure\n");
        return (NULL);
    }
    pu1NewEndPtr = pu1CurrPtr + i4TotalLength;
    if (pu1NewEndPtr != pu1EndPtr)
    {
        pu1EndPtr = pu1NewEndPtr;
    }
    u4Version = (UINT4) SNMPDecodeInteger (&pu1CurrPtr, pu1EndPtr,
                                           &i2Type, SNMP_UNSIGNED);
    if ((i2Type == SNMP_NOT_OK) || (i2Type != SNMP_DATA_TYPE_INTEGER32))
    {
        SNMPTrace ("Decode  Integer Failure\n");
        return (NULL);
    }
    Community.pu1_OctetList = gau1CommunityData;
    MEMSET (gau1CommunityData, SNMP_ZERO, sizeof (gau1CommunityData));
    if ((SNMPDecodeOctetstring (&pu1CurrPtr, pu1EndPtr,
                                &i2Type, &Community)) == NULL)
    {
        SNMPTrace ("Decode  community string Failure\n");
        return (NULL);
    }
    if (i2Type != SNMP_DATA_TYPE_OCTET_PRIM)
    {
        SNMPTrace ("Decode  Octet String Failure\n");
        return (NULL);
    }
    i4Len = SNMPDecodeTypeLen (&pu1CurrPtr, pu1EndPtr, &i2MsgType);

    if ((i4Len == SNMP_NOT_OK) || (i4Len == SNMP_ZERO))
    {
        SNMPTrace ("Decode  type or len  Failure\n");
        return (NULL);
    }

    if (i2MsgType == SNMP_NOT_OK)
    {
        SNMPTrace ("Msg type wrong\n");
        return (NULL);
    }
    /*condition detects pointer overflow and wrap around */
    if ((pu1CurrPtr + i4Len > pu1EndPtr) || (pu1CurrPtr + i4Len < pu1CurrPtr))    /*silvercreek */
    {
        SNMPTrace ("Msg overflow\n");
        return (NULL);
    }

    *pPduType = (INT2) i2MsgType;

    switch ((INT4) i2MsgType)
    {
        case SNMP_PDU_TYPE_GET_REQUEST:
        case SNMP_PDU_TYPE_GET_NEXT_REQUEST:
        case SNMP_PDU_TYPE_SET_REQUEST:
        case SNMP_PDU_TYPE_GET_BULK_REQUEST:
        case SNMP_PDU_TYPE_GET_RESPONSE:
        case SNMP_PDU_TYPE_SNMPV2_TRAP:
        case SNMP_PDU_TYPE_V2_INFORM_REQUEST:

            pMsgPtr = &gRxPdu;
            pMsgPtr->pCommunityStr = &Community;
            pMsgPtr->pCommunityStr->pu1_OctetList = gau1CommunityData;
            pMsgPtr->pVarBindList = NULL;
            pMsgPtr->pVarBindEnd = NULL;
            pMsgPtr->i4_ErrorStatus = 0;
            pMsgPtr->i4_ErrorIndex = 0;
            pMsgPtr->u4_RequestID =
                (UINT4) SNMPDecodeInteger (&pu1CurrPtr, pu1EndPtr,
                                           &i2Type, SNMP_SIGNED);
            if ((i2Type == SNMP_NOT_OK) || (i2Type != SNMP_DATA_TYPE_INTEGER32))
            {
                SNMPTrace ("Req Id not ok\n");
                SNMPFreeNormalMsg (pMsgPtr);
                return (NULL);
            }
            pMsgPtr->i4_ErrorStatus =
                (INT4) SNMPDecodeInteger (&pu1CurrPtr, pu1EndPtr,
                                          &i2Type, SNMP_SIGNED);
            if ((i2Type == SNMP_NOT_OK) || (i2Type != SNMP_DATA_TYPE_INTEGER32))
            {
                SNMPTrace ("Decode  Integer Status Failure\n");
                SNMPFreeNormalMsg (pMsgPtr);
                return (NULL);
            }

            if (i2MsgType == SNMP_PDU_TYPE_GET_BULK_REQUEST)
            {
                pMsgPtr->i4_NonRepeaters = pMsgPtr->i4_ErrorStatus;
                pMsgPtr->i4_ErrorStatus = SNMP_ZERO;
            }

            pMsgPtr->i4_ErrorIndex = SNMPDecodeInteger (&pu1CurrPtr, pu1EndPtr,
                                                        &i2Type, SNMP_SIGNED);
            if ((i2Type == SNMP_NOT_OK) || (i2Type != SNMP_DATA_TYPE_INTEGER32))
            {
                SNMPTrace ("Decode  Integer Index Failure\n");
                SNMPFreeNormalMsg (pMsgPtr);
                return (NULL);
            }

            if (i2MsgType == SNMP_PDU_TYPE_GET_BULK_REQUEST)
            {
                pMsgPtr->i4_MaxRepetitions = pMsgPtr->i4_ErrorIndex;
                pMsgPtr->i4_ErrorIndex = SNMP_ZERO;
            }

            if (SNMPDecodeSequence (&pu1CurrPtr, pu1EndPtr) == SNMP_NOT_OK)
            {
                SNMPTrace ("Decode  Sequence Failure\n");
                SNMPFreeNormalMsg (pMsgPtr);
                return (NULL);
            }
            pMsgPtr->u4_Version = u4Version;
            pMsgPtr->pCommunityStr = &Community;
            pMsgPtr->i2_PduType = i2MsgType;
            break;
            /* V1 Trap */
        case SNMP_PDU_TYPE_TRAP:
            MEMSET (&gV1TrapPdu, 0, sizeof (gV1TrapPdu));
            pV1Trap = &gV1TrapPdu;
            pV1Trap->pAgentAddr = &AgentAddress;
            pV1Trap->pAgentAddr->pu1_OctetList = gau1AgentAddress;
            pV1Trap->pEnterprise = &EnterpriseOid;
            pV1Trap->pEnterprise->pu4_OidList = EnterpriseOidList;

            if (NULL ==
                SNMPDecodeOid (&pu1CurrPtr, pu1EndPtr, pV1Trap->pEnterprise))
            {
                SNMPFreeV1TrapMsg (pV1Trap);
                return NULL;
            }

            if (NULL == SNMPDecodeOctetstring (&pu1CurrPtr, pu1EndPtr,
                                               &i2Type, pV1Trap->pAgentAddr))
            {
                SNMPFreeV1TrapMsg (pV1Trap);
                return NULL;
            }

            pV1Trap->i4_GenericTrap =
                (INT4) SNMPDecodeInteger (&pu1CurrPtr, pu1EndPtr, &i2Type,
                                          SNMP_SIGNED);
            if ((i2Type == SNMP_NOT_OK) || (i2Type != SNMP_DATA_TYPE_INTEGER32))
            {
                SNMPTrace ("Decode  Integer Failure\n");
                SNMPFreeV1TrapMsg (pV1Trap);
                return (NULL);
            }

            pV1Trap->i4_SpecificTrap =
                (INT4) SNMPDecodeInteger (&pu1CurrPtr, pu1EndPtr, &i2Type,
                                          SNMP_SIGNED);
            if ((i2Type == SNMP_NOT_OK) || (i2Type != SNMP_DATA_TYPE_INTEGER32))
            {
                SNMPTrace ("Decode  Integer Failure\n");
                SNMPFreeV1TrapMsg (pV1Trap);
                return (NULL);
            }

            pV1Trap->u4_TimeStamp =
                (UINT4) SNMPDecodeInteger (&pu1CurrPtr, pu1EndPtr, &i2Type,
                                           SNMP_UNSIGNED);
            if ((i2Type == SNMP_NOT_OK)
                || (i2Type != SNMP_DATA_TYPE_TIME_TICKS))
            {
                SNMPTrace ("Decode  Integer Failure\n");
                SNMPFreeV1TrapMsg (pV1Trap);
                return (NULL);
            }

            if (SNMPDecodeSequence (&pu1CurrPtr, pu1EndPtr) == SNMP_NOT_OK)
            {
                SNMPTrace ("Decode  Sequence Failure\n");
                SNMPFreeV1TrapMsg (pV1Trap);
                return (NULL);
            }
            pV1Trap->u4_Version = u4Version;
            pV1Trap->pCommunityStr = &Community;
            pV1Trap->i2_PduType = i2MsgType;
            break;

        default:
            SNMPTrace ("Wrong Tag type\n");
            return (NULL);
    }                            /* end of switch(msg_type) */

    while (pu1CurrPtr < pu1EndPtr)
    {
        pVarBindPtr = SNMPDecodeVarbind (&pu1CurrPtr, pu1EndPtr);
        if (pVarBindPtr == NULL)
        {
            SNMPTrace ("Decode  varbind Failure\n");
            if (pMsgPtr)
                SNMPFreeNormalMsg (pMsgPtr);
            else if (pV1Trap)
                SNMPFreeV1TrapMsg (pV1Trap);

            return (NULL);
        }
        if (pMsgPtr)
        {
            if (pMsgPtr->pVarBindList == NULL)
            {
                pMsgPtr->pVarBindList = pVarBindPtr;
                pMsgPtr->pVarBindEnd = pVarBindPtr;
            }
            else
            {
                pMsgPtr->pVarBindEnd->pNextVarBind = pVarBindPtr;
                pMsgPtr->pVarBindEnd = pVarBindPtr;
            }
        }
        else if (pV1Trap)
        {
            if (pV1Trap->pVarBindList == NULL)
            {
                pV1Trap->pVarBindList = pVarBindPtr;
                pV1Trap->pVarBindEnd = pVarBindPtr;
            }
            else
            {
                if (NULL != pV1Trap->pVarBindEnd)
                {
                    pV1Trap->pVarBindEnd->pNextVarBind = pVarBindPtr;
                    pV1Trap->pVarBindEnd = pVarBindPtr;
                }
            }
        }
        pVarBindPtr = NULL;
    }

    if (pMsgPtr)
        return ((INT1 *) pMsgPtr);
    else
        return ((INT1 *) pV1Trap);
}

/*********************************************************************
*  Function Name :SNMPDecodeSequence
*  Description   :This procedure decodes the Sequence Tags in the SNMP Message.
*
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT4
SNMPDecodeSequence (UINT1 **ppu1CurrPtr, UINT1 *pu1EndPtr)
{
    INT4                i4Len = SNMP_NOT_OK;
    INT2                i2Type = 0;

    i4Len = SNMPDecodeTypeLen (ppu1CurrPtr, pu1EndPtr, &i2Type);
    if (i4Len == SNMP_NOT_OK)
    {
        SNMPTrace ("Decode Sequence Length Failure ..\n");
        return (SNMP_NOT_OK);
    }
    if (i2Type != SNMP_DATA_TYPE_SEQUENCE)
    {
        SNMPTrace ("Decode Sequence Type != SNMP_DATA_TYPE_SEQUENCE\n");
        return (SNMP_NOT_OK);
    }
    return (i4Len);
}

/*********************************************************************
*  Function Name :SNMPDecodeTypeLen
*  Description   :This procedure decodes the Type of the tag in the 
*                 SNMP Message.The length of the value, is present 
*                 before the value in the BER encoded data.
*                 This length of the value following is decoded.
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT4
SNMPDecodeTypeLen (UINT1 **ppu1CurrPtr, const UINT1 *pu1EndPtr, INT2 *i2Type)
{
    INT4                i4Len = 0;
    INT4                i4Lenlen = 0;
    INT4                i4Seq = 0;

    if (*ppu1CurrPtr > pu1EndPtr)    /*silver creek */
    {
        SNMPTrace ("Msg Overflow in Decode Type Length .....\n");
        return (SNMP_NOT_OK);
    }
    *i2Type = *(*ppu1CurrPtr)++;

    if (*ppu1CurrPtr > pu1EndPtr)    /*silver creek */
    {
        SNMPTrace ("Msg Overflow in Decode Type Length .....\n");
        return (SNMP_NOT_OK);
    }
    i4Len = (INT4) *(*ppu1CurrPtr)++;
    if (*ppu1CurrPtr > pu1EndPtr)
    {
        SNMPTrace ("Msg Overflow in Decode Type Length .....\n");
        return (SNMP_NOT_OK);
    }
    switch (*i2Type)
    {
        case SNMP_DATA_TYPE_INTEGER32:
        case SNMP_DATA_TYPE_OCTET_PRIM:
        case SNMP_DATA_TYPE_OPAQUE:
        case SNMP_DATA_TYPE_NULL:
        case SNMP_DATA_TYPE_OBJECT_ID:
        case SNMP_DATA_TYPE_SEQUENCE:
        case SNMP_DATA_TYPE_IP_ADDR_PRIM:
        case SNMP_DATA_TYPE_COUNTER32:

        case SNMP_DATA_TYPE_COUNTER64:

        case SNMP_DATA_TYPE_GAUGE32:
        case SNMP_DATA_TYPE_TIME_TICKS:
        case SNMP_PDU_TYPE_GET_REQUEST:
        case SNMP_PDU_TYPE_GET_NEXT_REQUEST:
        case SNMP_PDU_TYPE_GET_RESPONSE:
        case SNMP_PDU_TYPE_SET_REQUEST:
        case SNMP_PDU_TYPE_TRAP:

        case SNMP_PDU_TYPE_GET_BULK_REQUEST:
        case SNMP_PDU_TYPE_SNMPV2_TRAP:
        case SNMP_PDU_TYPE_V2_INFORM_REQUEST:
        case SNMP_PDU_TYPE_GET_REPORT:

            break;
        default:
            SNMPTrace ("Decode Type len UnKnown Tag Type \n");
            return (SNMP_NOT_OK);
    }
    if (i4Len < 0x80)
    {
        return (i4Len);
    }
    i4Lenlen = i4Len & 0x7f;
    if ((i4Lenlen > SNMP_FOUR) || (i4Lenlen < SNMP_ONE))
    {
        SNMPTrace ("Decode Typelen bad lenlen ....\n");
        return (SNMP_NOT_OK);
    }
    i4Len = SNMP_ZERO;
    for (i4Seq = SNMP_ZERO; i4Seq < i4Lenlen; i4Seq++)
    {
        i4Len = (i4Len << SNMP_EIGHT) + *(*ppu1CurrPtr)++;
    }
    /*Check is added to avoid wrap around */
    if (i4Len > (pu1EndPtr - (*ppu1CurrPtr)))
    {
        SNMPTrace ("Invalid Decode Typelen\n");
        return (SNMP_NOT_OK);
    }

    /* For Negative Values */
    if (i4Len < SNMP_ZERO)
    {
        SNMPTrace ("Decode Length is Negative value\n");
        return (SNMP_NOT_OK);
    }
    if (*ppu1CurrPtr > pu1EndPtr)
    {
        SNMPTrace ("Decode Message Overflow at end\n");
        return (SNMP_NOT_OK);
    }
    return (i4Len);
}                                /* end of Decodei2Typelen */

/*********************************************************************
*  Function Name :SNMPDecodeInteger
*  Description   :This procedure decodes the Integer type data 
*                 in the SNMP Message
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT4
SNMPDecodeInteger (UINT1 **ppu1Curr, UINT1 *pu1End, INT2 *i2Type,
                   INT2 i2UnsignOrSign)
{
    INT4                i4Value = 0;
    INT4                i4Len = 0;
    INT4                i4Seq = 0;
    UINT4               u4Sign = 0;
    i4Value = 0L;
    i4Len = SNMPDecodeTypeLen (ppu1Curr, pu1End, i2Type);
    if ((i4Len == SNMP_NOT_OK) || (i4Len == SNMP_ZERO))
    {
        *i2Type = (INT2) SNMP_NOT_OK;
        return (SNMP_OK);
    }
    if (i2UnsignOrSign == SNMP_ONE)
    {
        if (i4Len > SNMP_FOUR)
        {
            *i2Type = (INT2) SNMP_NOT_OK;    /*kloc */
            return (SNMP_OK);
        }
    }
    else
    {
        if ((i4Len > 5) || ((i4Len > SNMP_FOUR) && (*(*ppu1Curr) != 0x00)))
        {
            *i2Type = (INT2) SNMP_NOT_OK;    /*kloc */
            return (SNMP_OK);
        }
    }
    if (i2UnsignOrSign == SNMP_ONE)
    {
        u4Sign = ((*(*ppu1Curr) & 0x80) == 0x00) ? 0x00 : 0xff;
    }

    for (i4Seq = SNMP_ZERO; i4Seq < i4Len; i4Seq++)
    {
        i4Value = (i4Value << SNMP_EIGHT) + (INT4) *(*ppu1Curr)++;
    }
    /*
     * now fill in the upper bits with the appropriate sign extension.
     */
    if (i2UnsignOrSign == SNMP_ONE)
    {
        for (i4Seq = i4Len; i4Seq < SNMP_FOUR; i4Seq++)
        {
            i4Value = i4Value + ((INT4) u4Sign << (i4Seq * SNMP_EIGHT));
        }
    }
    if (*ppu1Curr > pu1End)
    {
        *i2Type = (INT2) SNMP_NOT_OK;    /*kloc */
        return (SNMP_OK);
    }
    return (i4Value);
}

/*********************************************************************
*  Function Name :SNMPDecodeOctetstring
*  Description   :This procedure decodes the OctetString type data in the SNMP
*  Parameter(s)  :
*  Return Values :
*********************************************************************/

tSNMP_OCTET_STRING_TYPE *
SNMPDecodeOctetstring (UINT1 **ppu1CurrPtr, UINT1 *pu1EndPtr,
                       INT2 *i2Type, tSNMP_OCTET_STRING_TYPE * pOctetString)
{
    INT4                i4Len = SNMP_NOT_OK;

    if (((i4Len = SNMPDecodeTypeLen (ppu1CurrPtr, pu1EndPtr,
                                     i2Type)) == SNMP_NOT_OK) ||
        (*i2Type == SNMP_NOT_OK))
    {
        return (NULL);
    }
    if ((((*ppu1CurrPtr) + i4Len) > pu1EndPtr) || ((*ppu1CurrPtr) + i4Len) < (*ppu1CurrPtr))    /*silvercreek */
    {
        *i2Type = (INT2) SNMP_NOT_OK;    /*kloc */
        return (NULL);
    }

    if ((SNMPFormOctetString (*ppu1CurrPtr, i4Len, pOctetString)) == NULL)
    {
        *i2Type = (INT2) SNMP_NOT_OK;    /*kloc */
        return (NULL);
    }

    (*ppu1CurrPtr) += i4Len;

    if (*ppu1CurrPtr > pu1EndPtr)
    {
        *i2Type = (INT2) SNMP_NOT_OK;    /*kloc */
        return (NULL);
    }
    return (pOctetString);
}                                /* end of Decodeoctetstring() */

/*********************************************************************
*  Function Name :SNMPDecodeOid
*  Description   :This procedure decodes the OIDs of the SNMP Message
*  Parameter(s)  :
*  Return Values :
*********************************************************************/
tSNMP_OID_TYPE     *
SNMPDecodeOid (UINT1 **ppu1CurrPtr, UINT1 *pu1EndPtr, tSNMP_OID_TYPE * pOidPtr)
{
    INT4                i4Len = SNMP_NOT_OK, i4Seq = 0;
    INT2                i2Type = 0;
    i4Len = SNMPDecodeTypeLen (ppu1CurrPtr, pu1EndPtr, &i2Type);
    if (i4Len == SNMP_NOT_OK)
    {
        SNMPTrace ("Decode OID: Decode type len failed\n");
        return (NULL);
    }
    if (i2Type != SNMP_DATA_TYPE_OBJECT_ID)
    {
        SNMPTrace ("Decode OID: type is != Object id tag :return null\n");
        return (NULL);
    }
    if (i4Len == SNMP_ZERO)
    {
        pOidPtr->u4_Length = SNMP_ZERO;
        return (pOidPtr);
    }

    if ((((*ppu1CurrPtr) + i4Len) > pu1EndPtr) || ((*ppu1CurrPtr) + i4Len) < *ppu1CurrPtr)    /*silvercreek */
    {
        return (NULL);
    }

    pOidPtr->pu4_OidList[SNMP_ZERO] = (UINT4) (*(*ppu1CurrPtr) / 40);
    pOidPtr->pu4_OidList[SNMP_ONE] = (UINT4) (*(*ppu1CurrPtr)++ -
                                              (pOidPtr->pu4_OidList[SNMP_ZERO] *
                                               40));
    pOidPtr->u4_Length = SNMP_TWO;
    pOidPtr->pu4_OidList[SNMP_TWO] = SNMP_ZERO;
    for (i4Seq = SNMP_ZERO; i4Seq < i4Len - SNMP_ONE; i4Seq++)
    {
        if (pOidPtr->u4_Length < MAX_OID_LENGTH)
        {
            pOidPtr->pu4_OidList[pOidPtr->u4_Length] =
                (pOidPtr->pu4_OidList[pOidPtr->u4_Length] << 7) +
                (*(*ppu1CurrPtr) & 0x7F);
            if ((*(*ppu1CurrPtr)++ & 0x80) == SNMP_ZERO)
            {
                pOidPtr->u4_Length++;
                if (i4Seq < i4Len - SNMP_TWO)
                {
                    pOidPtr->pu4_OidList[pOidPtr->u4_Length] = SNMP_ZERO;
                }
            }
        }
        else
        {
            /*OIDs sub-identifier length exceeds MAX_OID_LENGTH
             * drop the request*/
            return (NULL);
        }
    }
    if (*ppu1CurrPtr > pu1EndPtr)
    {

        return (NULL);
    }
    return (pOidPtr);
}

/*********************************************************************
*  Function Name :SNMPDecodeVarbind
*  Description   :This procedure decodes the varbind part of the 
*                 SNMP Message The data is stored in the 
*                 internal datastructure of type tSNMP_VAR_BIND.
*  Parameter(s)  :
*  Return Values :
*********************************************************************/

tSNMP_VAR_BIND     *
SNMPDecodeVarbind (UINT1 **ppu1CurrPtr, UINT1 *pu1EndPtr)
{
    tSNMP_VAR_BIND     *pVarBindPtr = NULL;

    if (SNMPDecodeSequence (ppu1CurrPtr, pu1EndPtr) == SNMP_NOT_OK)
    {
        SNMPTrace ("Decode seq failed\n");
        return (NULL);
    }
    pVarBindPtr = SNMPAllocVarBind ();
    if (pVarBindPtr == NULL)
    {
        return NULL;
    }
    pVarBindPtr->pNextVarBind = NULL;
    if ((SNMPDecodeOid (ppu1CurrPtr, pu1EndPtr, pVarBindPtr->pObjName)) == NULL)
    {
        SNMPTrace ("Decode oid failed\n");
        SNMPFreeVarBind (pVarBindPtr);
        return (NULL);
    }

    switch (*(*ppu1CurrPtr))
    {
        case SNMP_DATA_TYPE_COUNTER32:
        case SNMP_DATA_TYPE_GAUGE32:
        case SNMP_DATA_TYPE_TIME_TICKS:
            pVarBindPtr->ObjValue.u4_ULongValue =
                (UINT4) SNMPDecodeInteger (ppu1CurrPtr, pu1EndPtr,
                                           &pVarBindPtr->ObjValue.
                                           i2_DataType, SNMP_UNSIGNED);
            if (pVarBindPtr->ObjValue.i2_DataType == SNMP_NOT_OK)
            {
                SNMPTrace ("type is not ok\n");
                SNMPFreeVarBind (pVarBindPtr);
                return (NULL);
            }
            break;

        case SNMP_DATA_TYPE_COUNTER64:
            SNMPDecodeCounter64Value (ppu1CurrPtr,
                                      pu1EndPtr,
                                      &pVarBindPtr->ObjValue.
                                      i2_DataType,
                                      &(pVarBindPtr->ObjValue.
                                        u8_Counter64Value));
            if (pVarBindPtr->ObjValue.i2_DataType == SNMP_NOT_OK)
            {
                SNMPTrace ("type is not ok\n");
                SNMPFreeVarBind (pVarBindPtr);
                return (NULL);
            }
            break;
        case SNMP_DATA_TYPE_INTEGER32:    /* handle signed integers */
            pVarBindPtr->ObjValue.i4_SLongValue =
                (INT4) SNMPDecodeInteger (ppu1CurrPtr, pu1EndPtr,
                                          &pVarBindPtr->ObjValue.
                                          i2_DataType, SNMP_SIGNED);
            if (pVarBindPtr->ObjValue.i2_DataType == SNMP_NOT_OK)
            {
                SNMPFreeVarBind (pVarBindPtr);
                SNMPTrace ("Integer tag received\n");
                SNMPTrace ("Type not SNMP_OK\n");
                return (NULL);
            }
            break;
        case SNMP_DATA_TYPE_OBJECT_ID:
            pVarBindPtr->ObjValue.i2_DataType = SNMP_DATA_TYPE_OBJECT_ID;
            if ((SNMPDecodeOid (ppu1CurrPtr,
                                pu1EndPtr,
                                pVarBindPtr->ObjValue.pOidValue)) == NULL)
            {
                SNMPFreeVarBind (pVarBindPtr);
                return (NULL);
            }
            break;
        case SNMP_DATA_TYPE_OCTET_PRIM:
        case SNMP_DATA_TYPE_OPAQUE:
            if ((SNMPDecodeOctetstring (ppu1CurrPtr, pu1EndPtr,
                                        &pVarBindPtr->ObjValue.
                                        i2_DataType,
                                        pVarBindPtr->ObjValue.
                                        pOctetStrValue)) == NULL)
            {
                SNMPFreeVarBind (pVarBindPtr);
                return (NULL);
            }
            break;
        case SNMP_DATA_TYPE_IP_ADDR_PRIM:
            if ((SNMPDecodeOctetstring (ppu1CurrPtr, pu1EndPtr,
                                        &pVarBindPtr->ObjValue.
                                        i2_DataType,
                                        pVarBindPtr->ObjValue.
                                        pOctetStrValue)) == NULL)
            {
                SNMPFreeVarBind (pVarBindPtr);
                return (NULL);
            }
            memcpy (&(pVarBindPtr->ObjValue.u4_ULongValue),
                    pVarBindPtr->ObjValue.pOctetStrValue->pu1_OctetList,
                    SNMP_FOUR);
            pVarBindPtr->ObjValue.u4_ULongValue =
                OSIX_HTONL (pVarBindPtr->ObjValue.u4_ULongValue);
            break;
        case SNMP_DATA_TYPE_NULL:
            if (SNMPDecodeNull (ppu1CurrPtr, pu1EndPtr,
                                &pVarBindPtr->ObjValue.i2_DataType) ==
                SNMP_NOT_OK)
            {
                SNMPFreeVarBind (pVarBindPtr);
                return (NULL);
            }
            break;

        case SNMP_EXCEPTION_NO_SUCH_OBJECT:
        case SNMP_EXCEPTION_NO_SUCH_INSTANCE:
        case SNMP_EXCEPTION_END_OF_MIB_VIEW:
            pVarBindPtr->ObjValue.i2_DataType = (*(*ppu1CurrPtr));
            *ppu1CurrPtr = *ppu1CurrPtr + SNMP_TWO;
            break;

        default:
            SNMPTrace ("Unknown tag received\n");
            SNMPFreeVarBind (pVarBindPtr);
            return (NULL);
    }                            /* end of switch */
    if (*ppu1CurrPtr > pu1EndPtr)
    {
        SNMPFreeVarBind (pVarBindPtr);
        return (NULL);
    }
    return (pVarBindPtr);
}

/*********************************************************************
*  Function Name :SNMPDecodeCounter64Value
*  Description   :This procedure decodes the Counter64 type data in 
*            the SNMP Message.
*  Parameter(s)  :
*  Return Values :
*********************************************************************/

VOID
SNMPDecodeCounter64Value (UINT1 **ppu1Curr, UINT1 *pu1End,
                          INT2 *i2Type, tSNMP_COUNTER64_TYPE * pu8Value)
{
    tSNMP_COUNTER64_TYPE u8_zero_value;
    INT4                i4Len = 0;
    INT4                i4Seq = 0;

    pu8Value->lsn = 0L;
    pu8Value->msn = 0L;

    u8_zero_value.lsn = 0L;
    u8_zero_value.msn = 0L;

    i4Len = SNMPDecodeTypeLen (ppu1Curr, pu1End, i2Type);
    if (i4Len == SNMP_NOT_OK)
    {
        *i2Type = (INT2) SNMP_NOT_OK;    /*kloc */
        pu8Value->msn = u8_zero_value.msn;
        pu8Value->lsn = u8_zero_value.lsn;
        return;
    }
    if ((i4Len > 9) || ((i4Len > 8) && (*(*ppu1Curr) != 0x00)))
    {
        *i2Type = (INT2) SNMP_NOT_OK;    /*kloc */
        pu8Value->msn = u8_zero_value.msn;
        pu8Value->lsn = u8_zero_value.lsn;
        return;
    }

    if (i4Len > SNMP_FOUR)
    {
        for (i4Seq = SNMP_ZERO; i4Seq < (i4Len - SNMP_FOUR); i4Seq++)
        {
            pu8Value->msn =
                (pu8Value->msn << SNMP_EIGHT) + (UINT4) *(*ppu1Curr)++;
        }
        for (i4Seq = i4Len - SNMP_FOUR; i4Seq < i4Len; i4Seq++)
        {
            pu8Value->lsn =
                (pu8Value->lsn << SNMP_EIGHT) + (UINT4) *(*ppu1Curr)++;
        }
    }
    else
    {
        for (i4Seq = SNMP_ZERO; i4Seq < i4Len; i4Seq++)
        {
            pu8Value->lsn =
                (pu8Value->lsn << SNMP_EIGHT) + (UINT4) *(*ppu1Curr)++;
        }
    }

    if (*ppu1Curr > pu1End)
    {
        *i2Type = (INT2) SNMP_NOT_OK;    /*kloc */
        pu8Value->msn = u8_zero_value.msn;
        pu8Value->lsn = u8_zero_value.lsn;
        return;
    }
    return;
}

/*********************************************************************
*  Function Name :SNMPDecodeNull
*  Description   :This procedure decodes the NULL Type values of the 
*            SNMP Message.
*  Parameter(s)  :
*  Return Values :
*********************************************************************/

INT2
SNMPDecodeNull (UINT1 **ppu1CurrPtr, UINT1 *pu1EndPtr, INT2 *pi2Type)
{
    INT4                i4Len = SNMP_NOT_OK;

    i4Len = SNMPDecodeTypeLen (ppu1CurrPtr, pu1EndPtr, pi2Type);
    if (i4Len == SNMP_NOT_OK)
    {
        return (SNMP_NOT_OK);
    }
    if (*pi2Type != SNMP_DATA_TYPE_NULL)
    {
        *pi2Type = (INT2) SNMP_NOT_OK;    /*kloc */
        return (SNMP_NOT_OK);
    }
    *ppu1CurrPtr = *ppu1CurrPtr + i4Len;
    if (*ppu1CurrPtr > pu1EndPtr)
    {
        return (SNMP_NOT_OK);
    }
    return (SNMP_OK);
}

/*********************************************************************
*  Function Name :SNMPReversePacket
*  Description   :This procedure reverses the encoded packet
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT4
SNMPReversePacket (UINT1 *pu1InPtr, UINT1 **ppu1OutPtr, INT4 i4Len)
{
    INT4                i4Seq = 0;
    *ppu1OutPtr = gau1RevDat;
    for (i4Seq = i4Len - SNMP_ONE; i4Seq >= SNMP_ZERO; i4Seq--)
    {
        (*ppu1OutPtr)[i4Len - (i4Seq + SNMP_ONE)] = pu1InPtr[i4Seq];
    }
    return (SNMP_OK);
}

/*********************************************************************
*  Function Name :SNMPEncodeV1TrapPacket
*  Description   :This procedure to encode the V1 Trap packet
*  Parameter(s)  :
*  Return Values :
*********************************************************************/

INT4
SNMPEncodeV1TrapPacket (tSNMP_TRAP_PDU * pMsgPtr,
                        UINT1 **ppu1DataOutPtr, INT4 *pi4PktLen)
{
    INT4                i4TrapmsgLength = 0;
    INT4                i4TotLen = 0;
    UINT1              *pu1DataPtr = NULL;
    UINT1              *pu1StartPtr = NULL;

    if (pMsgPtr == NULL)
    {
        return (SNMP_NOT_OK);
    }
    pu1StartPtr = MemAllocMemBlk (gSnmpPktPoolId);
    if (pu1StartPtr == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pu1StartPtr, 0, MAX_PKT_LENGTH);

    pu1DataPtr = pu1StartPtr;
    if (SNMPWriteTrapMsg (&pu1DataPtr, pMsgPtr) == SNMP_NOT_OK)
    {
        MemReleaseMemBlock (gSnmpPktPoolId, pu1StartPtr);
        return (SNMP_NOT_OK);
    }
    i4TrapmsgLength = pu1DataPtr - pu1StartPtr;
    if (SNMPWriteLength (&pu1DataPtr, i4TrapmsgLength) == SNMP_NOT_OK)
    {
        MemReleaseMemBlock (gSnmpPktPoolId, pu1StartPtr);
        return (SNMP_NOT_OK);
    }
    *pu1DataPtr = (UINT1) (0xff & SNMP_PDU_TYPE_TRAP);
    pu1DataPtr++;
    if (SNMPWriteOctetstring (&pu1DataPtr, pMsgPtr->pCommunityStr,
                              SNMP_DATA_TYPE_OCTET_PRIM) == SNMP_NOT_OK)
    {
        MemReleaseMemBlock (gSnmpPktPoolId, pu1StartPtr);
        return (SNMP_NOT_OK);
    }
    if (SNMPWriteUnsignedint (&pu1DataPtr, pMsgPtr->u4_Version,
                              SNMP_DATA_TYPE_INTEGER32) == SNMP_NOT_OK)
    {
        MemReleaseMemBlock (gSnmpPktPoolId, pu1StartPtr);
        return (SNMP_NOT_OK);
    }
    i4TotLen = pu1DataPtr - pu1StartPtr;
    if (SNMPWriteLength (&pu1DataPtr, i4TotLen) == SNMP_NOT_OK)
    {
        MemReleaseMemBlock (gSnmpPktPoolId, pu1StartPtr);
        return (SNMP_NOT_OK);
    }
    *pu1DataPtr = SNMP_DATA_TYPE_SEQUENCE;
    pu1DataPtr++;
    *pi4PktLen = pu1DataPtr - pu1StartPtr;
    SNMPDynamicReversePacket (pu1StartPtr, ppu1DataOutPtr, *pi4PktLen);
    MemReleaseMemBlock (gSnmpPktPoolId, pu1StartPtr);
    return (SNMP_OK);
}

/*********************************************************************
*  Function Name :SNMPWriteTrapMsg
*  Description   :This procedure encodes the fields corresponding to 
*                 TRAP Message.
*  Parameter(s)  :
*  Return Values :
*********************************************************************/

INT4
SNMPWriteTrapMsg (UINT1 **ppu1CurrPtr, tSNMP_TRAP_PDU * pMsgPtr)
{
    UINT1              *pu1StartPtr = NULL;
    INT4                i4VbLen = 0;

    pu1StartPtr = *ppu1CurrPtr;
    /*
     * Before Writing VarBind Lists, Reverse the VarBinds
     * in Place, so that the PDU Reversal later would encode
     * the VarBind in the Correct Order.
     */
    SNMPReverseVarBindList (&(pMsgPtr->pVarBindList));
    SNMPWriteVarbind (ppu1CurrPtr, pMsgPtr->pVarBindList);
    i4VbLen = *ppu1CurrPtr - pu1StartPtr;
    if (SNMPWriteLength (ppu1CurrPtr, i4VbLen) == SNMP_NOT_OK)
    {
        return (SNMP_NOT_OK);
    }
    *(*ppu1CurrPtr)++ = SNMP_DATA_TYPE_SEQUENCE;
    if (SNMPWriteUnsignedint (ppu1CurrPtr, pMsgPtr->u4_TimeStamp,
                              SNMP_DATA_TYPE_TIME_TICKS) == SNMP_NOT_OK)
    {
        return (SNMP_NOT_OK);
    }
    if (SNMPWriteSignedint (ppu1CurrPtr, pMsgPtr->i4_SpecificTrap,
                            SNMP_DATA_TYPE_INTEGER32) == SNMP_NOT_OK)
    {
        return (SNMP_NOT_OK);
    }
    if (SNMPWriteSignedint (ppu1CurrPtr, pMsgPtr->i4_GenericTrap,
                            SNMP_DATA_TYPE_INTEGER32) == SNMP_NOT_OK)
    {
        return (SNMP_NOT_OK);
    }
    if (SNMPWriteOctetstring (ppu1CurrPtr, pMsgPtr->pAgentAddr,
                              SNMP_DATA_TYPE_IP_ADDR_PRIM) == SNMP_NOT_OK)
    {
        return (SNMP_NOT_OK);
    }
    if (SNMPWriteOid (ppu1CurrPtr, pMsgPtr->pEnterprise) == SNMP_NOT_OK)
    {
        return (SNMP_NOT_OK);
    }
    SNMPReverseVarBindList (&(pMsgPtr->pVarBindList));
    return (SNMP_OK);
}

/*********************************************************************
*  Function Name :SNMPEncodeV2TrapPacket
*  Description   :This procedure to encode the V2 Trap packet
*  Parameter(s)  :
*  Return Values :
*********************************************************************/

INT4
SNMPEncodeV2TrapPacket (tSNMP_NORMAL_PDU * pMsgPtr, UINT1 **ppu1DataOut,
                        INT4 *pi4PktLen, INT4 i4TypeOfMsg)
{
    INT4                i4MsgLen = 0;
    INT4                i4TotLen = 0;
    UINT1              *pu1DataPtr = NULL;
    UINT1              *pu1StartPtr = NULL;
    INT4                i4EstLen = 0;

    if (pMsgPtr == NULL)
    {
        return (SNMP_NOT_OK);
    }

    pu1StartPtr = MemAllocMemBlk (gSnmpPktPoolId);
    if (pu1StartPtr == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pu1StartPtr, 0, MAX_PKT_LENGTH);
    pu1DataPtr = pu1StartPtr;
    i4EstLen = SNMPEstimateNormalPacketLength (pMsgPtr);
    if (i4EstLen > MAX_PKT_LENGTH)
    {
        MemReleaseMemBlock (gSnmpPktPoolId, pu1StartPtr);
        return SNMP_NOT_OK;
    }

    if (SNMPWriteGetMsg (&pu1DataPtr, pMsgPtr, i4TypeOfMsg) == SNMP_NOT_OK)
    {
        SNMPTrace ("EncodeGet: WriteGet failed\n");
        MemReleaseMemBlock (gSnmpPktPoolId, pu1StartPtr);
        return (SNMP_NOT_OK);
    }
    i4MsgLen = pu1DataPtr - pu1StartPtr;
    if (SNMPWriteLength (&pu1DataPtr, i4MsgLen) == SNMP_NOT_OK)
    {
        SNMPTrace ("EncodeGet: SNMP_AGT_WriteLength failed!\n");
        MemReleaseMemBlock (gSnmpPktPoolId, pu1StartPtr);
        return (SNMP_NOT_OK);
    }
    *pu1DataPtr = (UINT1) (0xff & i4TypeOfMsg);
    pu1DataPtr++;
    if (SNMPWriteOctetstring (&pu1DataPtr, pMsgPtr->pCommunityStr,
                              SNMP_DATA_TYPE_OCTET_PRIM) == SNMP_NOT_OK)
    {
        SNMPTrace ("EncodeGet: WriteOctectString failed!\n");
        MemReleaseMemBlock (gSnmpPktPoolId, pu1StartPtr);
        return (SNMP_NOT_OK);
    }
    if (SNMPWriteUnsignedint (&pu1DataPtr, pMsgPtr->u4_Version,
                              SNMP_DATA_TYPE_INTEGER32) == SNMP_NOT_OK)
    {
        SNMPTrace ("EncodeGet: WriteUInt failed!\n");
        MemReleaseMemBlock (gSnmpPktPoolId, pu1StartPtr);
        return (SNMP_NOT_OK);
    }
    i4TotLen = pu1DataPtr - pu1StartPtr;
    if (SNMPWriteLength (&pu1DataPtr, i4TotLen) == SNMP_NOT_OK)
    {
        SNMPTrace ("EncodeGet: WriteTotalLength failed!\n");
        MemReleaseMemBlock (gSnmpPktPoolId, pu1StartPtr);
        return (SNMP_NOT_OK);
    }
    *pu1DataPtr = SNMP_DATA_TYPE_SEQUENCE;
    pu1DataPtr++;
    *pi4PktLen = pu1DataPtr - pu1StartPtr;
    SNMPDynamicReversePacket (pu1StartPtr, ppu1DataOut, *pi4PktLen);
    MemReleaseMemBlock (gSnmpPktPoolId, pu1StartPtr);
    return (SNMP_OK);
}

/*********************************************************************
*  Function Name :SNMPDynamicReversePacket
*  Description   :This procedure reverses the encoded Trap packet.
*  Parameter(s)  :
*  Return Values :
*********************************************************************/
INT4
SNMPDynamicReversePacket (UINT1 *pu1InPtr, UINT1 **ppu1OutPtr, INT4 i4Length)
{
    INT4                i4Count = 0;
    *ppu1OutPtr = MemAllocMemBlk (gSnmpPktPoolId);
    if (*ppu1OutPtr == NULL)
    {
        return SNMP_NOT_OK;
    }
    for (i4Count = i4Length - SNMP_ONE; i4Count >= SNMP_ZERO; i4Count--)
    {
        (*ppu1OutPtr)[i4Length - (i4Count + SNMP_ONE)] = pu1InPtr[i4Count];
    }
    return (SNMP_OK);
}

/*********************************************************************
*  Function Name : SNMPVersionCheck
*  Description   : Function to Check the version of incomming SNMP 
*                  Request
*  Parameter(s)  : pu1Pkt - Pointer to Packet Content
*                  i4PktSize - Packet Length in Bytes  
*  Return Values : if Success returns Version Number otherwise 
*                  returns SNMP_NOT_OK
*********************************************************************/
INT4
SNMPVersionCheck (UINT1 *pu1Pkt, INT4 i4PktSize)
{
    UINT1              *pu1Start = NULL, *pu1End = NULL;
    INT4                i4Length = SNMP_ZERO, i4Version = SNMP_ZERO;
    INT2                i2Type = SNMP_ZERO;
    pu1Start = pu1Pkt;
    pu1End = pu1Start + i4PktSize;
    i4Length = SNMPDecodeSequence (&pu1Start, pu1End);
    if (i4Length == SNMP_NOT_OK)
    {
        SNMPTrace ("Decode Snmp Msg Length Failed\n");
        return (SNMP_NOT_OK);
    }
    i4Version = (INT4) SNMPDecodeInteger (&pu1Start, pu1End,
                                          &i2Type, SNMP_UNSIGNED);
    if ((i2Type == SNMP_NOT_OK) || (i2Type != SNMP_DATA_TYPE_INTEGER32))
    {
        SNMPTrace ("Decode  Version Number Failed \n");
        return (SNMP_NOT_OK);
    }
    return i4Version;
}

/*********************************************************************
*  Function Name : SNMPEncodeV3Message 
*  Description   : Function Encode SNMP V3 response in Byte stream
*  Parameter(s)  : pV3Pdu - SNMP V3 Packet Structure
*                  ppu1DataOut - Output Bytes steam pointer
*                  *pi4PktLen - Output Byte Steam length
*                  i4TypeOfMsg - Encoding Message Type(Response/Report)
*  Return Values : SNMP_SUCCESS or SNMP_FAILURE
*********************************************************************/
INT4
SNMPEncodeV3Message (tSNMP_V3PDU * pV3Pdu, UINT1 **ppu1DataOut,
                     INT4 *pi4PktLen, INT4 i4TypeOfMsg)
{
    INT4                i4MsgLen = SNMP_ZERO, i4EstLen = SNMP_ZERO;
    INT4                i4TotLen = SNMP_ZERO, i4Residual = SNMP_ZERO;
    INT4                i4Offset = SNMP_ZERO, i4EncyDataLen = SNMP_ZERO;
    UINT1              *pu1DataPtr = NULL, *pu1AuthDataPtr = NULL;
    UINT1              *pu1StartPtr = NULL, *pu1TempPtr = NULL;
    UINT4               u4CurrTime = SNMP_ZERO;
    UINT1              *pu1V3Data = NULL;
    tSNMP_NORMAL_PDU   *pMsgPtr = NULL;
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;

    pMsgPtr = &(pV3Pdu->Normalpdu);

    i4EstLen = SNMPEstimateNormalPacketLength (pMsgPtr);
    i4EstLen += SNMP_V3_HEADER_LENGTH;
    if (i4EstLen > MAX_PKT_LENGTH)
    {
        SNMPTrace ("Unable to Encode Packet:Error : Too Big  \n");
        *pi4PktLen = i4EstLen;
        pMsgPtr->i4_ErrorStatus = SNMP_ERR_TOO_BIG;
        return SNMP_FAILURE;
    }
    pu1V3Data = MemAllocMemBlk (gSnmpPktPoolId);
    if (pu1V3Data == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pu1V3Data, 0, MAX_PKT_LENGTH);
    GET_TIME_TICKS (&u4CurrTime);
    if (((pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO] & SNMP_ENCY_BIT) ==
         SNMP_ENCY_BIT)
        || ((pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO] & SNMP_AUTH_BIT) ==
            SNMP_AUTH_BIT))
    {
        pSnmpUsmEntry = SNMPGetUsmEntry (&(pV3Pdu->MsgSecParam.MsgEngineID),
                                         &(pV3Pdu->MsgSecParam.MsgUsrName));
        if (pSnmpUsmEntry == NULL)
        {
            SNMP_INC_USM_UNKNOWNUSERNAMES;
            MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
            return SNMP_FAILURE;
        }
        if (pSnmpUsmEntry->u4UsmUserStatus == UNDER_CREATION)
        {
            /* RFC 3414 Section 3.1
             * b)based on the securityName, information concerning the user at
             * the destination snmpEngineID, specified by the
             * securityEngineID, is extracted from the Local Configuration
             * Datastore (LCD, usmUserTable).  If information about the user
             * is absent from the LCD, then an error indication
             * (unknownSecurityName) is returned to the calling module */
            SNMP_INC_USM_UNKNOWNUSERNAMES;
            MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
            return SNMP_UNKNOWNUSERNAME;
        }
    }
    pV3Pdu->MsgSecParam.u4MsgEngineBoot = gSnmpSystem.u4SnmpBootCount;
    pV3Pdu->MsgSecParam.u4MsgEngineTime =
        (u4CurrTime - gSnmpSystem.u4SysUpTime) / SYS_NUM_OF_TIME_UNITS_IN_A_SEC;
    pu1DataPtr = pu1V3Data;
    pu1StartPtr = pu1DataPtr;
    if (SNMPWriteGetMsg (&pu1DataPtr, pMsgPtr, i4TypeOfMsg) == SNMP_NOT_OK)
    {
        SNMPTrace ("Encode SNMP V3 Varbinds Failed\n");
        MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
        return (SNMP_FAILURE);
    }
    i4MsgLen = pu1DataPtr - pu1StartPtr;
    if (SNMPWriteLength (&pu1DataPtr, i4MsgLen) == SNMP_NOT_OK)
    {
        SNMPTrace ("Encode SNMP V3 Varbinds Length Failed!\n");
        MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
        return (SNMP_FAILURE);
    }

    *pu1DataPtr = SNMP_GET_LAST_BYTE (i4TypeOfMsg);
    pu1DataPtr++;
    if (SNMPWriteOctetstring (&pu1DataPtr, &(pV3Pdu->ContextName),
                              SNMP_DATA_TYPE_OCTET_PRIM) == SNMP_NOT_OK)
    {
        SNMP_INR_UNKNOWN_CONTEXTS;
        SNMPTrace ("Encode SNMP V3 Context Name Failed\n");
        MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
        return (SNMP_FAILURE);
    }

    if (SNMPWriteOctetstring (&pu1DataPtr, &(pV3Pdu->ContextID),
                              SNMP_DATA_TYPE_OCTET_PRIM) == SNMP_NOT_OK)
    {
        SNMP_INR_UNKNOWN_CONTEXTS;
        SNMPTrace ("Encode SNMP V3 Context ID Failed\n");
        MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
        return (SNMP_FAILURE);
    }

    i4TotLen = pu1DataPtr - pu1StartPtr;
    if (SNMPWriteLength (&pu1DataPtr, i4TotLen) == SNMP_NOT_OK)
    {
        SNMPTrace ("Encode SNMP V3 PDU Length Failed\n");
        MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
        return (SNMP_FAILURE);
    }
    *pu1DataPtr = SNMP_DATA_TYPE_SEQUENCE;
    pu1DataPtr++;

    /* Encode Encryption as Octet String */
    if ((pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO] & SNMP_ENCY_BIT) ==
        SNMP_ENCY_BIT)
    {
        i4EncyDataLen = pu1DataPtr - pu1StartPtr;
        if ((i4EncyDataLen % SNMP_ENCRYPT_BYTE_ALIGN) != SNMP_ZERO)
        {
            i4Residual = (SNMP_ENCRYPT_BYTE_ALIGN -
                          (i4EncyDataLen % SNMP_ENCRYPT_BYTE_ALIGN));
            i4EncyDataLen += i4Residual;
        }
        SNMPWriteLength (&pu1DataPtr, i4EncyDataLen);
        *pu1DataPtr = (UINT1) SNMP_DATA_TYPE_OCTET_PRIM;
        pu1DataPtr++;
    }
    pu1AuthDataPtr =
        SNMPWriteSecurityParams (&pu1DataPtr, pV3Pdu, pSnmpUsmEntry);
    if (pu1AuthDataPtr == NULL)
    {
        SNMPTrace ("Encode SNMP V3 Message Security Param Failed\n");
        MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
        return (SNMP_FAILURE);
    }
    pu1TempPtr = pu1DataPtr;
    if (SNMPWriteUnsignedint (&pu1DataPtr, pV3Pdu->u4MsgSecModel,
                              SNMP_DATA_TYPE_INTEGER32) == SNMP_NOT_OK)
    {
        SNMPTrace ("Encode SNMP V3 Message Security Model\n");
        MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
        return (SNMP_FAILURE);
    }

    /* Reset the Response Required BIT */
    pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO] &= SNMP_RESET_REPEAT_BIT;

    if (SNMPWriteOctetstring (&pu1DataPtr, &(pV3Pdu->MsgFlag),
                              SNMP_DATA_TYPE_OCTET_PRIM) == SNMP_NOT_OK)
    {
        SNMPTrace ("Encode SNMP V3 Message Flag Failed\n");
        MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
        return (SNMP_FAILURE);
    }

    if (SNMPWriteUnsignedint (&pu1DataPtr, pV3Pdu->u4MsgMaxSize,
                              SNMP_DATA_TYPE_INTEGER32) == SNMP_NOT_OK)
    {
        SNMPTrace ("Encode SNMP V3 Message Maximum Failed\n");
        MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
        return (SNMP_FAILURE);
    }

    if (SNMPWriteUnsignedint (&pu1DataPtr, pV3Pdu->u4MsgID,
                              SNMP_DATA_TYPE_INTEGER32) == SNMP_NOT_OK)
    {
        SNMPTrace ("Encode SNMP V3 Message ID Failed\n");
        MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
        return (SNMP_FAILURE);
    }
    i4TotLen = pu1DataPtr - pu1TempPtr;
    if (SNMPWriteLength (&pu1DataPtr, i4TotLen) == SNMP_NOT_OK)
    {
        SNMPTrace ("EncodeGet: WriteTotalLength failed!\n");
        MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
        return (SNMP_NOT_OK);
    }
    *pu1DataPtr = SNMP_DATA_TYPE_SEQUENCE;
    pu1DataPtr++;
    pV3Pdu->Normalpdu.u4_Version = VERSION3;
    if (SNMPWriteUnsignedint (&pu1DataPtr, pV3Pdu->Normalpdu.u4_Version,
                              SNMP_DATA_TYPE_INTEGER32) == SNMP_NOT_OK)
    {
        SNMPTrace ("Encode SNMP V3 Version Failed\n");
        MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
        return (SNMP_FAILURE);
    }

    i4TotLen = pu1DataPtr - pu1StartPtr;
    i4TotLen += i4Residual;
    if (SNMPWriteLength (&pu1DataPtr, i4TotLen) == SNMP_NOT_OK)
    {
        SNMPTrace ("EncodeGet: WriteTotalLength failed!\n");
        MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
        return (SNMP_NOT_OK);
    }
    *pu1DataPtr = SNMP_DATA_TYPE_SEQUENCE;
    pu1DataPtr++;
    *pi4PktLen = pu1DataPtr - pu1StartPtr;
    if ((i4TypeOfMsg == SNMP_PDU_TYPE_SNMPV2_TRAP) ||
        (i4TypeOfMsg == SNMP_PDU_TYPE_V2_INFORM_REQUEST) ||
        (i4TypeOfMsg == SNMP_PDU_TYPE_TRAP))
    {
        SNMPDynamicReversePacket (pu1StartPtr, ppu1DataOut, *pi4PktLen);
    }
    else
    {
        SNMPReversePacket (pu1StartPtr, ppu1DataOut, *pi4PktLen);
    }

    if (pV3Pdu->MsgSecParam.MsgUsrName.i4_Length == SNMP_ZERO)
    {
        MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
        return SNMP_SUCCESS;
    }

    /* Encryption */
    if ((pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO] & SNMP_ENCY_BIT) ==
        SNMP_ENCY_BIT)
    {
        (*pi4PktLen) += i4Residual;
        i4Offset = (*pi4PktLen) - i4EncyDataLen;
        if (SNMPEncryptePDU ((*ppu1DataOut) + i4Offset, i4EncyDataLen,
                             pV3Pdu, pSnmpUsmEntry) == SNMP_FAILURE)
        {
            MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
            return SNMP_FAILURE;
        }

    }

    /* Authentication */
    if ((pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO] & SNMP_AUTH_BIT) ==
        SNMP_AUTH_BIT)
    {
        i4Offset = pu1DataPtr - pu1AuthDataPtr;
        if (pSnmpUsmEntry->u4UsmUserAuthProtocol == SNMP_SHA256)
        {
            i4Offset -= SNMP_AUTH_DIGEST_LEN_SHA256;
        }
        else if (pSnmpUsmEntry->u4UsmUserAuthProtocol == SNMP_SHA384)
        {
            i4Offset -= SNMP_AUTH_DIGEST_LEN_SHA384;
        }
        else if (pSnmpUsmEntry->u4UsmUserAuthProtocol == SNMP_SHA512)
        {
            i4Offset -= SNMP_AUTH_DIGEST_LEN_SHA512;
        }
        else if ((pSnmpUsmEntry->u4UsmUserAuthProtocol == SNMP_SHA)
                 || (pSnmpUsmEntry->u4UsmUserAuthProtocol == SNMP_MD5))
        {
            i4Offset -= SNMP_AUTH_DIGEST_LEN;
        }

        if (SNMPGenerateAuthSecurity (*ppu1DataOut, *pi4PktLen,
                                      ((*ppu1DataOut) + i4Offset),
                                      pSnmpUsmEntry) == SNMP_FAILURE)
        {
            MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
            return SNMP_FAILURE;
        }
    }
    MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : SNMPDecodeV3Request
*  Description   : Function Decode SNMP V3 Request
*  Parameter(s)  : pu1PktPtr - SNMP Packet Pointer
*                  i4PktLen  - Packet Length in bytes
*                  pV3Pdu - Pointer to Store V3 Informations
*  Return Values : SNMP_SUCCESS or SNMP_FAILURE
*********************************************************************/
INT4
SNMPDecodeV3Request (UINT1 *pu1PktPtr, INT4 i4PktLen, tSNMP_V3PDU * pV3Pdu)
{
    UINT1              *pu1CurrPtr = NULL, *pu1EndPtr = NULL;
    UINT1              *pu1NewEndPtr = NULL;
    INT2                i2Type = SNMP_ZERO, i2MsgType = SNMP_ZERO;
    INT4                i4TotalLength = SNMP_ZERO, i4PduLength = SNMP_ZERO;
    INT4                i4Len = SNMP_ZERO, i4AgentDiscovery = SNMP_ZERO;
    tSNMP_VAR_BIND     *pVarBindPtr = NULL;
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    UINT1               au1ContextName[SNMP_MAX_OCTETSTRING_SIZE];
    /* tContext           *pContext = NULL; */

    STRCPY (au1ContextName, "");

    pV3Pdu->MsgSecParam.MsgEngineID.i4_Length = SNMP_ZERO;
    pV3Pdu->MsgSecParam.MsgEngineID.pu1_OctetList = gau1MsgEngineID;
    pV3Pdu->MsgSecParam.MsgUsrName.i4_Length = SNMP_ZERO;
    MEMSET (gau1MsgUsrName, SNMP_ZERO, sizeof (gau1MsgUsrName));
    pV3Pdu->MsgSecParam.MsgUsrName.pu1_OctetList = gau1MsgUsrName;
    pV3Pdu->MsgSecParam.MsgAuthParam.i4_Length = SNMP_ZERO;
    pV3Pdu->MsgSecParam.MsgAuthParam.pu1_OctetList = gau1MsgAuthParam;
    pV3Pdu->MsgSecParam.MsgPrivParam.i4_Length = SNMP_ZERO;
    pV3Pdu->MsgSecParam.MsgPrivParam.pu1_OctetList = gau1MsgPrivParam;
    pV3Pdu->MsgFlag.i4_Length = SNMP_ZERO;
    pV3Pdu->MsgFlag.pu1_OctetList = gau1MsgFlag;
    pV3Pdu->ContextName.i4_Length = SNMP_ZERO;
    pV3Pdu->ContextName.pu1_OctetList = gau1ContextName;
    pV3Pdu->ContextID.i4_Length = SNMP_ZERO;
    pV3Pdu->ContextID.pu1_OctetList = gau1ContextID;

    pu1CurrPtr = pu1PktPtr;
    pu1EndPtr = pu1CurrPtr + i4PktLen;
    i4TotalLength = SNMPDecodeSequence (&pu1CurrPtr, pu1EndPtr);
    if (i4TotalLength == SNMP_NOT_OK)
    {
        SNMPTrace ("Decode SNMP V3 Packet Length Failed\n");
        return (SNMP_FAILURE);
    }
    pu1NewEndPtr = pu1CurrPtr + i4TotalLength;
    if (pu1NewEndPtr != pu1EndPtr)
    {
        pu1EndPtr = pu1NewEndPtr;
    }

    pV3Pdu->Normalpdu.u4_Version = (UINT4) SNMPDecodeInteger (&pu1CurrPtr,
                                                              pu1EndPtr,
                                                              &i2Type,
                                                              SNMP_UNSIGNED);
    if (i2Type == SNMP_NOT_OK || (i2Type != SNMP_DATA_TYPE_INTEGER32))
    {
        SNMPTrace ("Decode  SNMP V3 Version Failed\n");
        return (SNMP_FAILURE);
    }

    if (SNMPDecodeSequence (&pu1CurrPtr, pu1EndPtr) == SNMP_NOT_OK)
    {
        SNMPTrace ("Decode SNMP V3 Packet Header Sequence Failed\n");
        return (SNMP_FAILURE);
    }

    pV3Pdu->u4MsgID = (UINT4) SNMPDecodeInteger (&pu1CurrPtr, pu1EndPtr,
                                                 &i2Type, SNMP_UNSIGNED);
    if (i2Type == SNMP_NOT_OK || (i2Type != SNMP_DATA_TYPE_INTEGER32))
    {
        SNMPTrace ("Decode  SNMP V3 Message ID  Failed\n");
        return (SNMP_FAILURE);
    }

    pV3Pdu->u4MsgMaxSize = (UINT4) SNMPDecodeInteger (&pu1CurrPtr, pu1EndPtr,
                                                      &i2Type, SNMP_UNSIGNED);
    if (i2Type == SNMP_NOT_OK || (i2Type != SNMP_DATA_TYPE_INTEGER32))
    {
        SNMPTrace ("Decode  SNMP V3 Maximum Message Size Failed\n");
        return (SNMP_FAILURE);
    }

    if ((SNMPDecodeOctetstring (&pu1CurrPtr, pu1EndPtr,
                                &i2Type, &(pV3Pdu->MsgFlag))) == NULL)
    {
        SNMPTrace ("Decode SNMP V3 Message Flag Failed\n");
        return (SNMP_FAILURE);
    }

    if (i2Type != SNMP_DATA_TYPE_OCTET_PRIM)
    {
        SNMPTrace ("Decode V3 Message Flag Type is not OctetString\n");
        return (SNMP_FAILURE);
    }

    pV3Pdu->u4MsgSecModel = (UINT4) SNMPDecodeInteger (&pu1CurrPtr,
                                                       pu1EndPtr, &i2Type,
                                                       SNMP_UNSIGNED);
    if (i2Type == SNMP_NOT_OK || (i2Type != SNMP_DATA_TYPE_INTEGER32))
    {
        SNMPTrace ("Decode  SNMP V3 Message Security Model Failed\n");
        return (SNMP_FAILURE);
    }

    if (pV3Pdu->u4MsgSecModel != SNMP_USM)
    {
        SNMP_INR_UNKNOWN_SECMODEL;
        SNMP_INR_INVALID_MSG;
        SNMPTrace ("Decode SNMP V3 Non USM Security Model.\n");
        return SNMP_FAILURE;
    }
    if (((pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO]
          & SNMP_AUTH_BIT) == SNMP_ZERO) &&
        ((pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO]
          & SNMP_ENCY_BIT) == SNMP_ENCY_BIT))
    {
        /* RFC 3412 Section 7.2.5 
         * d) If the authFlag is not set and privFlag is 
         * set, then the snmpInvalidMsgs counter is 
         * incremented, the message is discarded without 
         * further processing, and a FAILURE result is 
         * returned.  SNMPv3 Message Processing is complete
         */
        SNMP_INR_INVALID_MSG;
        SNMPTrace ("Decode SNMP V3 Invalid Msg received.\n");
        return SNMP_FAILURE;
    }

    if ((SNMPDecodeSecurityParams (&pu1CurrPtr, pu1EndPtr, pV3Pdu)) ==
        SNMP_NOT_OK)
    {
        SNMPTrace ("Decode SNMP V3 Message Security Params\n");
        return (SNMP_FAILURE);
    }

    if (SNMPValidateEngineId (&(pV3Pdu->MsgSecParam.MsgEngineID))
        == SNMP_FAILURE)
    {
        /* RFC 3414 Section 3.2 
         * 3.b) the usmStatsUnknownEngineIDs counter is incremented, and an
         * error indication (unknownEngineID) together with the OID and
         * value of the incremented counter is returned to the calling
         * module. */
        if (pV3Pdu->MsgSecParam.MsgEngineID.i4_Length != SNMP_ZERO)
        {
            /*Since it is not agent discover process, send the error
               report to manager */
            pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO] &=
                (UINT1) (~SNMP_RESET_REPEAT_BIT);
            SNMP_INC_USM_UNKNOWNENGINEIDS;
            return SNMP_UNKNOWNENGINEID;
        }
    }
    else
    {
        pSnmpUsmEntry = SNMPGetUsmEntry (&(pV3Pdu->MsgSecParam.MsgEngineID),
                                         &(pV3Pdu->MsgSecParam.MsgUsrName));
    }
    if (pV3Pdu->MsgSecParam.MsgEngineID.i4_Length == SNMP_ZERO)
    {
        i4AgentDiscovery = SNMP_AGENTDISCOVERY;
        if ((pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO] & SNMP_SEC_BITS) !=
            SNMP_ZERO)
        {
            SNMPTrace ("SNMP V3 Agent Discovery Packet with Security\n");
            SNMP_INC_USM_UNKNOWNENGINEIDS;
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (pSnmpUsmEntry == NULL ||
            pSnmpUsmEntry->u4UsmUserStatus != SNMP_ACTIVE)
        {
            /* RFC 3414 Section 3.2 
             * 4)Information about the value of the msgUserName and
             * msgAuthoritativeEngineID fields is extracted from the Local
             * Configuration Datastore (LCD, usmUserTable).  If no information
             * is available for the user, then the usmStatsUnknownUserNames
             * counter is incremented and an error indication
             * (unknownSecurityName) together with the OID and value of the
             * incremented counter is returned to the calling module*/
            pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO] &=
                (UINT1) (~SNMP_RESET_REPEAT_BIT);
            SNMP_INC_USM_UNKNOWNUSERNAMES;
            return SNMP_UNKNOWNUSERNAME;
        }
        else
        {
            /* Incomming Security BIT is set but no Security Support */
            if (((pSnmpUsmEntry->u4UsmUserAuthProtocol == SNMP_NO_AUTH) &&
                 (pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO] & SNMP_AUTH_BIT) ==
                 SNMP_AUTH_BIT) || (pSnmpUsmEntry->u4UsmUserPrivProtocol ==
                                    SNMP_NO_PRIV
                                    && (pV3Pdu->MsgFlag.
                                        pu1_OctetList[SNMP_ZERO] &
                                        SNMP_ENCY_BIT) == SNMP_ENCY_BIT))
            {
                /*Send the report to the manager  */
                pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO] &=
                    (UINT1) (~SNMP_RESET_REPEAT_BIT);
                SNMP_INC_USM_UNSUPPORTSECLEVELS;
                return SNMP_UNSUPPORTEDSECLEVEL;
            }
        }
    }
    /* Verify Auth Security */

    if (pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO] & SNMP_AUTH_BIT)
    {
        if (pSnmpUsmEntry == NULL ||
            pSnmpUsmEntry->u4UsmUserStatus != SNMP_ACTIVE)
        {
            /* RFC 3414 Section 3.2 
             * 4)Information about the value of the msgUserName and
             * msgAuthoritativeEngineID fields is extracted from the Local
             * Configuration Datastore (LCD, usmUserTable). If no information
             * is available for the user, then the usmStatsUnknownUserNames
             * counter is incremented and an error indication
             * (unknownSecurityName) together with the OID and value of the
             * incremented counter is returned to the calling module*/
            pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO] &=
                (UINT1) (~SNMP_RESET_REPEAT_BIT);
            SNMP_INC_USM_UNKNOWNUSERNAMES;
            return SNMP_UNKNOWNUSERNAME;
        }
        if (pSnmpUsmEntry->u4UsmUserAuthProtocol == SNMP_NO_AUTH)
        {
            SNMPTrace (" User has auth disabled ....\n");
            return SNMP_FAILURE;
        }
        if (SNMPVerifyAuthSecurity
            (pu1PktPtr, i4PktLen, pV3Pdu, pSnmpUsmEntry) == SNMP_FAILURE)
        {
            pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO] &=
                (UINT1) (~SNMP_RESET_REPEAT_BIT);
            return SNMP_WRONGDIGESTS;
        }
    }

    if (pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO] & SNMP_ENCY_BIT)
    {
        i4Len = SNMPDecodeTypeLen (&pu1CurrPtr, pu1EndPtr, &i2Type);
        if ((i4Len == SNMP_NOT_OK) || (i2Type != SNMP_DATA_TYPE_OCTET_PRIM))
        {
            SNMPTrace ("SNMP V3 Encrypted PDU is not a Octet String type \n");
            SNMP_INR_INVALID_MSG;
            return (SNMP_NOT_OK);
        }
        if (SNMPDecryptePDU (pu1CurrPtr, i4Len, pV3Pdu, pSnmpUsmEntry)
            == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    i4PduLength = SNMPDecodeSequence (&pu1CurrPtr, pu1EndPtr);
    if (i4PduLength == SNMP_NOT_OK)
    {
        /* Check Whether Encryption is Enabled,And increment the counter 
         * because not able to decode the packet due to data was encrypted 
         * with wrong key */
        if (pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO] & SNMP_ENCY_BIT)
        {
            pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO] &=
                (UINT1) (~SNMP_RESET_REPEAT_BIT);
            SNMP_INC_USM_DECRYPTIONERRORS;
            return SNMP_DECRYPTIONERRORS;
        }
        SNMPTrace ("Decode SNMP V3 Packet PDU Length Failed\n");
        return (SNMP_FAILURE);
    }
    if ((pu1CurrPtr + i4PduLength) < pu1EndPtr)
    {
        pu1EndPtr = pu1CurrPtr + i4PduLength;
    }

    if (((SNMPDecodeOctetstring (&pu1CurrPtr, pu1EndPtr,
                                 &i2Type, &(pV3Pdu->ContextID))) == NULL) &&
        (i2Type != SNMP_DATA_TYPE_OCTET_PRIM))
    {
        SNMPTrace ("Decode SNMP V3 Context ID Failed\n");
        SNMP_INR_UNKNOWN_CONTEXTS;
        return SNMP_UNKNOWN_CONTEXT_NAME;
    }

    if (((SNMPDecodeOctetstring (&pu1CurrPtr, pu1EndPtr,
                                 &i2Type, &(pV3Pdu->ContextName))) == NULL) &&
        (i2Type != SNMP_DATA_TYPE_OCTET_PRIM))
    {
        SNMP_INR_UNKNOWN_CONTEXTS;
        SNMPTrace ("Decode SNMP V3 Context Name Failed\n");
        return (SNMP_FAILURE);
    }

    i4Len = SNMPDecodeTypeLen (&pu1CurrPtr, pu1EndPtr, &i2MsgType);
    if ((i4Len == SNMP_NOT_OK) || (i4Len == SNMP_ZERO) ||
        (i2MsgType == SNMP_NOT_OK))
    {
        SNMPTrace ("Decode SNMP V3 PDU  Type or Length  Failure\n");
        return (SNMP_FAILURE);
    }

    if ((pu1CurrPtr + i4Len > pu1EndPtr) || (pu1CurrPtr + i4Len < pu1CurrPtr))    /*silvercreek */
    {
        SNMPTrace ("Decode SNMP V3 Message Overflow\n");
        return (SNMP_FAILURE);
    }
    pV3Pdu->Normalpdu.i2_PduType = i2MsgType;
    switch ((INT4) i2MsgType)
    {
        case SNMP_PDU_TYPE_GET_REQUEST:
        case SNMP_PDU_TYPE_GET_NEXT_REQUEST:
        case SNMP_PDU_TYPE_SET_REQUEST:
        case SNMP_PDU_TYPE_GET_BULK_REQUEST:
        case SNMP_PDU_TYPE_GET_RESPONSE:
        case SNMP_PDU_TYPE_SNMPV2_TRAP:
        case SNMP_PDU_TYPE_V2_INFORM_REQUEST:
        case SNMP_PDU_TYPE_GET_REPORT:
            pV3Pdu->Normalpdu.pVarBindList = NULL;
            pV3Pdu->Normalpdu.pVarBindEnd = NULL;
            pV3Pdu->Normalpdu.i4_ErrorStatus = 0;
            pV3Pdu->Normalpdu.i4_ErrorIndex = 0;
            pV3Pdu->Normalpdu.u4_RequestID =
                (UINT4) SNMPDecodeInteger (&pu1CurrPtr, pu1EndPtr,
                                           &i2Type, SNMP_SIGNED);
            if ((i2Type == SNMP_NOT_OK) || (i2Type != SNMP_DATA_TYPE_INTEGER32))
            {
                SNMPTrace ("Decode SNMP V3 PDU Req Id Not ok\n");
                SNMPFreeNormalMsg (&pV3Pdu->Normalpdu);
                return (SNMP_FAILURE);
            }
            pV3Pdu->Normalpdu.i4_ErrorStatus =
                (INT4) SNMPDecodeInteger (&pu1CurrPtr, pu1EndPtr,
                                          &i2Type, SNMP_SIGNED);
            if ((i2Type == SNMP_NOT_OK) || (i2Type != SNMP_DATA_TYPE_INTEGER32))
            {
                SNMPTrace ("Decode  SNMP V3 PDU Error Status Failed\n");
                SNMPFreeNormalMsg (&pV3Pdu->Normalpdu);
                return (SNMP_FAILURE);
            }

            if (i2MsgType == SNMP_PDU_TYPE_GET_BULK_REQUEST)
            {
                pV3Pdu->Normalpdu.i4_NonRepeaters =
                    pV3Pdu->Normalpdu.i4_ErrorStatus;
                pV3Pdu->Normalpdu.i4_ErrorStatus = SNMP_ZERO;
            }

            pV3Pdu->Normalpdu.i4_ErrorIndex = SNMPDecodeInteger (&pu1CurrPtr,
                                                                 pu1EndPtr,
                                                                 &i2Type,
                                                                 SNMP_SIGNED);
            if ((i2Type == SNMP_NOT_OK) || (i2Type != SNMP_DATA_TYPE_INTEGER32))
            {
                SNMPTrace ("Decode  SNMP V3 PDU Error Index Failed\n");
                SNMPFreeNormalMsg (&pV3Pdu->Normalpdu);
                return (SNMP_FAILURE);
            }

            if (i2MsgType == SNMP_PDU_TYPE_GET_BULK_REQUEST)
            {
                pV3Pdu->Normalpdu.i4_MaxRepetitions =
                    pV3Pdu->Normalpdu.i4_ErrorIndex;
                pV3Pdu->Normalpdu.i4_ErrorIndex = SNMP_ZERO;
            }

            if (SNMPDecodeSequence (&pu1CurrPtr, pu1EndPtr) == SNMP_NOT_OK)
            {
                SNMPTrace ("Decode  SNMP V3 PDU Sequence Failed\n");
                SNMPFreeNormalMsg (&pV3Pdu->Normalpdu);
                return (SNMP_FAILURE);
            }
            break;
        default:
            SNMP_INR_UNKNOWN_PDUHANDLER;
            SNMPTrace ("Wrong SNMP V3 PDU Type\n");
            return (SNMP_FAILURE);
    }
    if (i4TotalLength > MAX_PKT_LENGTH)
    {
        pV3Pdu->Normalpdu.i4_ErrorStatus = SNMP_ERR_TOO_BIG;
        return (SNMP_FAILURE);
    }

    while (pu1CurrPtr < pu1EndPtr)
    {
        pVarBindPtr = SNMPDecodeVarbind (&pu1CurrPtr, pu1EndPtr);
        if (pVarBindPtr == NULL)
        {
            SNMPTrace ("Decode SNMP V3 PDU  Varbind Failed\n");
            SNMPFreeNormalMsg (&pV3Pdu->Normalpdu);
            return (SNMP_FAILURE);
        }
        if (pV3Pdu->Normalpdu.pVarBindList == NULL)
        {
            pV3Pdu->Normalpdu.pVarBindList = pVarBindPtr;
            pV3Pdu->Normalpdu.pVarBindEnd = pVarBindPtr;
        }
        else
        {
            pV3Pdu->Normalpdu.pVarBindEnd->pNextVarBind = pVarBindPtr;
            pV3Pdu->Normalpdu.pVarBindEnd = pVarBindPtr;
        }
        pVarBindPtr = NULL;
    }
    if (i4AgentDiscovery == SNMP_AGENTDISCOVERY)
    {
        return SNMP_AGENTDISCOVERY;
    }

    if (pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO] & SNMP_AUTH_BIT)
    {
        return (SNMPValidateTimeSync (pV3Pdu));
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/*********************************************************************
*  Function Name : SNMPValidateTimeSync
*  Description   : Function Verify SNMP V3 Request Time Sync
*  Parameter(s)  : pV3Pdu - SNMP Request 
*  Return Values : SNMP_SUCCESS or SNMP_FAILURE
*********************************************************************/
INT4
SNMPValidateTimeSync (tSNMP_V3PDU * pV3Pdu)
{
    UINT4               u4CurrTime;
    INT4                u4LocalSnmpEngineTime = 0;
    INT4                i4DiffSnmpEngineTime = 0;

    if (gSnmpSystem.u4SnmpBootCount > SNMP_ENGINE_MAX)
    {
        SNMPTrace ("Engine Boot exceeded Max Count\n");
        SNMP_INC_USM_NOTINTIMEWINDOWS;
        return SNMP_NOT_TIMEWINDOW;
    }
    if (pV3Pdu->MsgSecParam.u4MsgEngineBoot != gSnmpSystem.u4SnmpBootCount)
    {
        SNMP_INC_USM_NOTINTIMEWINDOWS;
        return SNMP_NOT_TIMEWINDOW;
    }
    GET_TIME_TICKS (&u4CurrTime);

    u4LocalSnmpEngineTime =
        (INT4) ((u4CurrTime -
                 gSnmpSystem.u4SysUpTime) / SYS_NUM_OF_TIME_UNITS_IN_A_SEC);
    /* This check is done according to RFC 3414 section 2.2.2 
     * If snmpEngineTime ever reaches its maximum value (2147483647),
     * then snmpEngineBoots is incremented as if the SNMP engine has re-booted 
     * and snmpEngineTime is reset to zero and starts incrementing again*/
    if (u4LocalSnmpEngineTime >= SNMP_ENGINE_MAX)
    {
        gSnmpSystem.u4SnmpBootCount++;
        if (gSnmpSystem.u4SnmpBootCount <= SNMP_ENGINE_MAX)
        {
            IssSetSnmpEngineBootsToNvRam (gSnmpSystem.u4SnmpBootCount);
            gSnmpSystem.u4SysUpTime = u4CurrTime;
            u4LocalSnmpEngineTime = 0;
        }
        else
        {
            SNMPTrace ("Engine Boot exceeded Max Count\n");
            SNMP_INC_USM_NOTINTIMEWINDOWS;
            return SNMP_NOT_TIMEWINDOW;
        }
    }

    i4DiffSnmpEngineTime =
        (u4LocalSnmpEngineTime - (INT4) pV3Pdu->MsgSecParam.u4MsgEngineTime);

    /* This check is according to RFC-3414 section 3.2-7.a point-3
     * -the value of the msgAuthoritativeEngineTime field differs
     * from the local notion of snmpEngineTime by more than +/- 150
     * seconds.
     */
    if ((i4DiffSnmpEngineTime > 150) || (i4DiffSnmpEngineTime < -150))
    {
        SNMP_INC_USM_NOTINTIMEWINDOWS;
        return SNMP_NOT_TIMEWINDOW;
    }
    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : SNMPDecodeSecurityParams
*  Description   : Function Decode SNMP V3 USM Security Params
*  Parameter(s)  : ppu1CurrPtr - SNMP Packet Pointer from where SNMP
*                  Security Param Starts.
*                  pu1EndPtr - SNMP Packet End Pointer
*                  pV3Pdu - Pointer to Store V3 Params
*  Return Values : SNMP_OK or SNMP_NOT_OK
*********************************************************************/
INT4
SNMPDecodeSecurityParams (UINT1 **ppu1CurrPtr, UINT1 *pu1EndPtr,
                          tSNMP_V3PDU * pV3Pdu)
{
    INT4                i4Len = SNMP_NOT_OK;
    INT4                i4Seq = 0;
    INT2                i2Type = 0;

    i4Len = SNMPDecodeTypeLen (ppu1CurrPtr, pu1EndPtr, &i2Type);
    if ((i4Len == SNMP_NOT_OK) || (i2Type == SNMP_NOT_OK)
        || i2Type != SNMP_DATA_TYPE_OCTET_PRIM)
    {
        return (SNMP_NOT_OK);
    }

    /* Decode Sequence */
    if (SNMPDecodeSequence (ppu1CurrPtr, pu1EndPtr) == SNMP_NOT_OK)
    {
        SNMPTrace ("Decode SNMP V3 Packet Security Param Sequence Failed\n");
        return (SNMP_NOT_OK);
    }
    /* Decode Message Engine ID as Octet String */
    if ((SNMPDecodeOctetstring (ppu1CurrPtr, pu1EndPtr,
                                &i2Type,
                                &(pV3Pdu->MsgSecParam.MsgEngineID)) == NULL)
        || (i2Type != SNMP_DATA_TYPE_OCTET_PRIM))
    {
        SNMPTrace ("Decode SNMP V3 Msg Engine ID Failed\n");
        return (SNMP_NOT_OK);
    }

    /* Decode Message Engine Boot as Integer */
    pV3Pdu->MsgSecParam.u4MsgEngineBoot =
        (UINT4) SNMPDecodeInteger (ppu1CurrPtr, pu1EndPtr, &i2Type,
                                   SNMP_UNSIGNED);
    if (i2Type == SNMP_NOT_OK || (i2Type != SNMP_DATA_TYPE_INTEGER32))
    {
        SNMPTrace ("Decode  SNMP V3 Engine Boot Failed\n");
        return (SNMP_NOT_OK);
    }

    /* Decode Message Engine Time as Integer */
    pV3Pdu->MsgSecParam.u4MsgEngineTime =
        (UINT4) SNMPDecodeInteger (ppu1CurrPtr, pu1EndPtr, &i2Type,
                                   SNMP_UNSIGNED);
    if (i2Type == SNMP_NOT_OK || (i2Type != SNMP_DATA_TYPE_INTEGER32))
    {
        SNMPTrace ("Decode  SNMP V3 Engine Time Failed\n");
        return (SNMP_NOT_OK);
    }

    /* Decode User Name as Octet String */
    if ((SNMPDecodeOctetstring (ppu1CurrPtr, pu1EndPtr,
                                &i2Type,
                                &(pV3Pdu->MsgSecParam.MsgUsrName)) == NULL)
        || (i2Type != SNMP_DATA_TYPE_OCTET_PRIM))
    {
        SNMPTrace ("Decode SNMP V3 Message User Name Failed\n");
        return (SNMP_NOT_OK);
    }

    /* Decode Auth Digest as Octet String  and make it Zero */
    i4Len = SNMPDecodeTypeLen (ppu1CurrPtr, pu1EndPtr, &i2Type);
    if ((i4Len == SNMP_NOT_OK) || (i2Type == SNMP_NOT_OK) ||
        (i4Len > SNMP_MAX_OCTETSTRING_SIZE) ||
        (i2Type != SNMP_DATA_TYPE_OCTET_PRIM))
    {
        SNMPTrace ("Decode SNMP V3 Message Auth Params Failed\n");
        return (SNMP_NOT_OK);
    }

    if ((SNMPFormOctetString
         (*ppu1CurrPtr, i4Len, &(pV3Pdu->MsgSecParam.MsgAuthParam))) == NULL)
    {
        SNMPTrace ("Decode SNMP V3 Message Auth Params Failed\n");
        return (SNMP_NOT_OK);
    }
    pV3Pdu->MsgSecParam.MsgAuthParam.i4_Length = i4Len;
    for (i4Seq = SNMP_ZERO; i4Seq < i4Len; i4Seq++)
    {
        **ppu1CurrPtr = 0;
        (*ppu1CurrPtr)++;
    }

    /* Decode Privacy IV as Octet String */
    if ((SNMPDecodeOctetstring (ppu1CurrPtr, pu1EndPtr,
                                &i2Type,
                                &(pV3Pdu->MsgSecParam.MsgPrivParam)) == NULL)
        || (i2Type != SNMP_DATA_TYPE_OCTET_PRIM))
    {
        SNMPTrace ("Decode SNMP V3 Message Privacy Params Failed\n");
        return (SNMP_NOT_OK);
    }

    if (*ppu1CurrPtr > pu1EndPtr)
    {
        return (SNMP_NOT_OK);
    }
    return (SNMP_OK);
}

/*********************************************************************
*  Function Name : SNMPWriteSecurityParams
*  Description   : Function Write Security Params to Outgoing response
*  Parameter(s)  : ppu1CurrPtr - Pointer to write security Params
*                  pV3Pdu - Pointer to read security params
*  Return Values : if Success returns Authentuication Data pointer 
*                  otherwise returns NULL. This Authentication Data
*                  pointer is used to fill the Auth data at the end
*                  of encoding process. 
*********************************************************************/
UINT1              *
SNMPWriteSecurityParams (UINT1 **ppu1CurrPtr, tSNMP_V3PDU * pV3Pdu,
                         tSnmpUsmEntry * pSnmpUsmEntry)
{
    UINT1              *pu1AuthDataPtr = NULL;
    UINT1              *pu1Start = NULL;
    INT4                i4Len = 0;
    INT4                i4AuthMsgLen = 0;
    pu1Start = *ppu1CurrPtr;

    if (pV3Pdu->MsgFlag.pu1_OctetList[0] & 0x01)
    {
        if (!pSnmpUsmEntry)
        {
            SNMPTrace ("Encode SNMP Usm Entry Param Failed\n");
            return (NULL);
        }
    }

    /* Write Privacy IV as Octet String */
    if (SNMPWriteOctetstring (ppu1CurrPtr, &(pV3Pdu->MsgSecParam.MsgPrivParam),
                              SNMP_DATA_TYPE_OCTET_PRIM) == SNMP_NOT_OK)
    {
        SNMPTrace ("Encode SNMP V3 Message Privacy Param Failed\n");
        return (NULL);
    }

    /* Write Auth Params as Octet String */
    if (pV3Pdu->MsgFlag.pu1_OctetList[0] & 0x01)
    {
        pV3Pdu->MsgSecParam.MsgAuthParam.i4_Length = 12;
        if (pSnmpUsmEntry->u4UsmUserAuthProtocol == SNMP_SHA256)
        {
            i4AuthMsgLen = SNMP_AUTH_DIGEST_LEN_SHA256;
        }
        else if (pSnmpUsmEntry->u4UsmUserAuthProtocol == SNMP_SHA384)
        {
            i4AuthMsgLen = SNMP_AUTH_DIGEST_LEN_SHA384;
        }
        else if (pSnmpUsmEntry->u4UsmUserAuthProtocol == SNMP_SHA512)
        {
            i4AuthMsgLen = SNMP_AUTH_DIGEST_LEN_SHA512;
        }
        else if ((pSnmpUsmEntry->u4UsmUserAuthProtocol == SNMP_SHA)
                 || (pSnmpUsmEntry->u4UsmUserAuthProtocol == SNMP_MD5))
        {
            i4AuthMsgLen = 12;
        }
        MEMSET (pV3Pdu->MsgSecParam.MsgAuthParam.pu1_OctetList, 0,
                i4AuthMsgLen);
        pV3Pdu->MsgSecParam.MsgAuthParam.i4_Length = i4AuthMsgLen;
    }
    else
    {
        pV3Pdu->MsgSecParam.MsgAuthParam.i4_Length = 0;
    }
    pu1AuthDataPtr = (*ppu1CurrPtr);
    if (SNMPWriteOctetstring (ppu1CurrPtr, &(pV3Pdu->MsgSecParam.MsgAuthParam),
                              SNMP_DATA_TYPE_OCTET_PRIM) == SNMP_NOT_OK)
    {
        SNMPTrace ("Encode SNMP V3 Message Atuh Param Failed\n");
        return (NULL);
    }

    /* Write User Name as Octet String */
    if (SNMPWriteOctetstring (ppu1CurrPtr, &(pV3Pdu->MsgSecParam.MsgUsrName),
                              SNMP_DATA_TYPE_OCTET_PRIM) == SNMP_NOT_OK)
    {
        SNMPTrace ("Encode SNMP V3 Message User Name Failed\n");
        return (NULL);
    }

    /* Write Engine Time as Integer */
    if (SNMPWriteUnsignedint
        (ppu1CurrPtr, (pV3Pdu->MsgSecParam.u4MsgEngineTime),
         SNMP_DATA_TYPE_INTEGER32) == SNMP_NOT_OK)
    {
        SNMPTrace ("Encode SNMP V3 Engine Time Failed\n");
        return (NULL);
    }

    /* Write Engine Boot as Integer */
    if (SNMPWriteUnsignedint (ppu1CurrPtr, pV3Pdu->MsgSecParam.u4MsgEngineBoot,
                              SNMP_DATA_TYPE_INTEGER32) == SNMP_NOT_OK)
    {
        SNMPTrace ("Encode SNMP V3 Engine Boot Failed\n");
        return (NULL);
    }

    /* Write Engine ID as Octet String */
    if (SNMPWriteOctetstring (ppu1CurrPtr, &(pV3Pdu->MsgSecParam.MsgEngineID),
                              SNMP_DATA_TYPE_OCTET_PRIM) == SNMP_NOT_OK)
    {
        SNMPTrace ("Encode SNMP V3 Message Engine ID Failed\n");
        return (NULL);
    }

    /* Write Sequence  */
    i4Len = (*ppu1CurrPtr) - pu1Start;
    if (SNMPWriteLength (ppu1CurrPtr, i4Len) == SNMP_NOT_OK)
    {
        SNMPTrace ("Encode SNMP V3 Message Security Param Sequence Failed!\n");
        return (NULL);
    }
    **ppu1CurrPtr = SNMP_DATA_TYPE_SEQUENCE;
    (*ppu1CurrPtr)++;

    /* Write Octet String Length and Type  */
    i4Len = (*ppu1CurrPtr) - pu1Start;
    if (SNMPWriteLength (ppu1CurrPtr, i4Len) == SNMP_NOT_OK)
    {
        SNMPTrace ("Encode SNMP V3 Message Security Param Length Failed!\n");
        return (NULL);
    }
    *(*ppu1CurrPtr)++ = (UINT1) (0xff & SNMP_DATA_TYPE_OCTET_PRIM);
    return (pu1AuthDataPtr);
}

/*********************************************************************
*  Function Name : SNMPEncodeV3ReportPdu
*  Description   : Function Encode SNMP V3 Report Pdu in Byte stream
*  Parameter(s)  : pV3Pdu - SNMP V3 Packet Structure
*                  ppu1DataOut - Output Bytes steam pointer
*                  *pi4PktLen - Output Byte Steam length
*                  i4TypeOfMsg - Encoding Message Type(Response/Report)
*  Return Values : SNMP_SUCCESS or SNMP_FAILURE
*********************************************************************/
INT4
SNMPEncodeV3ReportPdu (tSNMP_V3PDU * pV3Pdu, UINT1 **ppu1DataOut,
                       INT4 *pi4PktLen, INT4 i4TypeOfMsg)
{
    INT4                i4MsgLen = SNMP_ZERO, i4EstLen = SNMP_ZERO;
    INT4                i4TotLen = SNMP_ZERO, i4Residual = SNMP_ZERO;
    INT4                i4EncyDataLen = SNMP_ZERO;
    UINT1              *pu1DataPtr = NULL, *pu1AuthDataPtr = NULL;
    UINT1              *pu1StartPtr = NULL, *pu1TempPtr = NULL;
    UINT4               u4CurrTime = SNMP_ZERO;
    UINT1              *pu1V3Data = NULL;
    tSNMP_NORMAL_PDU   *pMsgPtr = NULL;
    tContext           *pContext = NULL;
    pMsgPtr = &(pV3Pdu->Normalpdu);
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;

    i4EstLen = SNMPEstimateNormalPacketLength (pMsgPtr);
    i4EstLen += SNMP_V3_HEADER_LENGTH;
    if (i4EstLen > MAX_PKT_LENGTH)
    {
        SNMPTrace ("Unable to Encode Packet:Error : Too Big  \n");
        *pi4PktLen = i4EstLen;
        pMsgPtr->i4_ErrorStatus = SNMP_ERR_TOO_BIG;
        return SNMP_FAILURE;
    }
    if (((pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO] & SNMP_ENCY_BIT) ==
         SNMP_ENCY_BIT)
        || ((pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO] & SNMP_AUTH_BIT) ==
            SNMP_AUTH_BIT))
    {
        pSnmpUsmEntry = SNMPGetUsmEntry (&(pV3Pdu->MsgSecParam.MsgEngineID),
                                         &(pV3Pdu->MsgSecParam.MsgUsrName));
        if (pSnmpUsmEntry == NULL)
        {
            /* RFC 3414 Section 3.1
             * b)based on the securityName, information concerning the user at
             * the destination snmpEngineID, specified by the
             * securityEngineID, is extracted from the Local Configuration
             * Datastore (LCD, usmUserTable).  If information about the user
             * is absent from the LCD, then an error indication
             * (unknownSecurityName) is returned to the calling module */
            SNMP_INC_USM_UNKNOWNUSERNAMES;
            MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
            return SNMP_UNKNOWNUSERNAME;
        }
        if (pSnmpUsmEntry->u4UsmUserStatus == UNDER_CREATION)
        {
            SNMP_INC_USM_UNKNOWNUSERNAMES;
            MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
            return SNMP_UNKNOWNUSERNAME;
        }
    }

    pu1V3Data = MemAllocMemBlk (gSnmpPktPoolId);
    if (pu1V3Data == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pu1V3Data, 0, MAX_PKT_LENGTH);
    GET_TIME_TICKS (&u4CurrTime);
    pV3Pdu->MsgSecParam.u4MsgEngineBoot = gSnmpSystem.u4SnmpBootCount;
    pV3Pdu->MsgSecParam.u4MsgEngineTime =
        (u4CurrTime - gSnmpSystem.u4SysUpTime) / SYS_NUM_OF_TIME_UNITS_IN_A_SEC;
    pu1DataPtr = pu1V3Data;
    pu1StartPtr = pu1DataPtr;
    if (SNMPWriteGetMsg (&pu1DataPtr, pMsgPtr, i4TypeOfMsg) == SNMP_NOT_OK)
    {
        SNMPTrace ("Encode SNMP V3 Varbinds Failed\n");
        MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
        return (SNMP_FAILURE);
    }
    i4MsgLen = pu1DataPtr - pu1StartPtr;
    if (SNMPWriteLength (&pu1DataPtr, i4MsgLen) == SNMP_NOT_OK)
    {
        SNMPTrace ("Encode SNMP V3 Varbinds Length Failed!\n");
        MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
        return (SNMP_FAILURE);
    }

    *pu1DataPtr = SNMP_GET_LAST_BYTE (i4TypeOfMsg);
    pu1DataPtr++;
    pContext = VACMGetFirstContext ();
    if (pContext != NULL)
    {
        SNMPCopyOctetString (&(pV3Pdu->ContextName), &(pContext->ContextName));
    }
    if (SNMPWriteOctetstring (&pu1DataPtr, &(pV3Pdu->ContextName),
                              SNMP_DATA_TYPE_OCTET_PRIM) == SNMP_NOT_OK)
    {
        SNMP_INR_UNKNOWN_CONTEXTS;
        SNMPTrace ("Encode SNMP V3 Context Name Failed\n");
        MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
        return (SNMP_FAILURE);
    }

    if (SNMPWriteOctetstring (&pu1DataPtr, &(pV3Pdu->ContextID),
                              SNMP_DATA_TYPE_OCTET_PRIM) == SNMP_NOT_OK)
    {
        SNMP_INR_UNKNOWN_CONTEXTS;
        SNMPTrace ("Encode SNMP V3 Context ID Failed\n");
        MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
        return (SNMP_FAILURE);
    }

    i4TotLen = pu1DataPtr - pu1StartPtr;
    if (SNMPWriteLength (&pu1DataPtr, i4TotLen) == SNMP_NOT_OK)
    {
        SNMPTrace ("Encode SNMP V3 PDU Length Failed\n");
        MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
        return (SNMP_FAILURE);
    }
    *pu1DataPtr = SNMP_DATA_TYPE_SEQUENCE;
    pu1DataPtr++;

    /* Encode Encryption as Octet String */
    if ((pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO] & SNMP_ENCY_BIT) ==
        SNMP_ENCY_BIT)
    {
        i4EncyDataLen = pu1DataPtr - pu1StartPtr;
        if ((i4EncyDataLen % SNMP_ENCRYPT_BYTE_ALIGN) != SNMP_ZERO)
        {
            i4Residual = (SNMP_ENCRYPT_BYTE_ALIGN -
                          (i4EncyDataLen % SNMP_ENCRYPT_BYTE_ALIGN));
            i4EncyDataLen += i4Residual;
        }
        SNMPWriteLength (&pu1DataPtr, i4EncyDataLen);
        *pu1DataPtr = (UINT1) SNMP_DATA_TYPE_OCTET_PRIM;
        pu1DataPtr++;
    }
    pu1AuthDataPtr =
        SNMPWriteSecurityParams (&pu1DataPtr, pV3Pdu, pSnmpUsmEntry);
    if (pu1AuthDataPtr == NULL)
    {
        SNMPTrace ("Encode SNMP V3 Message Security Param Failed\n");
        MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
        return (SNMP_FAILURE);
    }
    pu1TempPtr = pu1DataPtr;
    if (SNMPWriteUnsignedint (&pu1DataPtr, pV3Pdu->u4MsgSecModel,
                              SNMP_DATA_TYPE_INTEGER32) == SNMP_NOT_OK)
    {
        SNMPTrace ("Encode SNMP V3 Message Security Model\n");
        MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
        return (SNMP_FAILURE);
    }

    /* Reset the Response Required BIT */
    pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO] &= SNMP_RESET_REPEAT_BIT;

    if (SNMPWriteOctetstring (&pu1DataPtr, &(pV3Pdu->MsgFlag),
                              SNMP_DATA_TYPE_OCTET_PRIM) == SNMP_NOT_OK)
    {
        SNMPTrace ("Encode SNMP V3 Message Flag Failed\n");
        MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
        return (SNMP_FAILURE);
    }

    if (SNMPWriteUnsignedint (&pu1DataPtr, pV3Pdu->u4MsgMaxSize,
                              SNMP_DATA_TYPE_INTEGER32) == SNMP_NOT_OK)
    {
        SNMPTrace ("Encode SNMP V3 Message Maximum Failed\n");
        MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
        return (SNMP_FAILURE);
    }
    if (SNMPWriteUnsignedint (&pu1DataPtr, pV3Pdu->u4MsgID,
                              SNMP_DATA_TYPE_INTEGER32) == SNMP_NOT_OK)
    {
        SNMPTrace ("Encode SNMP V3 Message ID Failed\n");
        MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
        return (SNMP_FAILURE);
    }
    i4TotLen = pu1DataPtr - pu1TempPtr;
    if (SNMPWriteLength (&pu1DataPtr, i4TotLen) == SNMP_NOT_OK)
    {
        SNMPTrace ("EncodeGet: WriteTotalLength failed!\n");
        MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
        return (SNMP_NOT_OK);
    }
    *pu1DataPtr = SNMP_DATA_TYPE_SEQUENCE;
    pu1DataPtr++;
    pV3Pdu->Normalpdu.u4_Version = VERSION3;
    if (SNMPWriteUnsignedint (&pu1DataPtr, pV3Pdu->Normalpdu.u4_Version,
                              SNMP_DATA_TYPE_INTEGER32) == SNMP_NOT_OK)
    {
        SNMPTrace ("Encode SNMP V3 Version Failed\n");
        MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
        return (SNMP_FAILURE);
    }

    i4TotLen = pu1DataPtr - pu1StartPtr;
    i4TotLen += i4Residual;
    if (SNMPWriteLength (&pu1DataPtr, i4TotLen) == SNMP_NOT_OK)
    {
        SNMPTrace ("EncodeGet: WriteTotalLength failed!\n");
        MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
        return (SNMP_NOT_OK);
    }
    *pu1DataPtr = SNMP_DATA_TYPE_SEQUENCE;
    pu1DataPtr++;
    *pi4PktLen = pu1DataPtr - pu1StartPtr;
    SNMPReversePacket (pu1StartPtr, ppu1DataOut, *pi4PktLen);

    if (pV3Pdu->MsgSecParam.MsgUsrName.i4_Length == SNMP_ZERO)
    {
        MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
        return SNMP_SUCCESS;
    }
    MemReleaseMemBlock (gSnmpPktPoolId, pu1V3Data);
    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name :SNMPCheckForNVTChars
*  Description   :This procedure finds if any non-printable characters
*                 present in the given string ( includes special chars,
*                 non-US ascii chars). Useful to check for valid inputs for
*                 DisplayString Type objects.(as per RFC: 1903).
*
*  Parameter(s)  : pu1TestStr - pointer to string to be validated,
*                  u4Length   - Length upto which the string will be
*                               validated. 
*
*  Return Values : OSIX_SUCCESS - if there are no such characters present.
*                  OSIX_FAILURE - if at all any non-printable characters 
*                                 present in the given string.
*********************************************************************/
UINT4
SNMPCheckForNVTChars (UINT1 *pu1TestStr, INT4 i4Length)
{
    INT4                i4Len = 0;

    for (i4Len = 0; i4Len < i4Length; i4Len++)
    {
        /* Check if the char falls between (0-127) decimal and (32-126) US ASCII.
         */
        if (pu1TestStr[i4Len] > 0x7F)
        {
            /* *pu4ErrorCode = SNMP_ERR_WRONG_VALUE; */
            return OSIX_FAILURE;
        }
        /* Check if always CR (0x0D) followed by LF (0x0A) or
         * CR followed by NUL (0x00).So if only CR is received,
         * return ERROR.
         */
        if (pu1TestStr[i4Len] == 0x0D)
        {
            if ((i4Len + 1) == i4Length)
            {
                return OSIX_FAILURE;
            }
            if ((pu1TestStr[i4Len + 1] != 0x0A) && (pu1TestStr[i4Len + 1] != 0))
            {
                /* *pu4ErrorCode = SNMP_ERR_WRONG_VALUE; */
                return OSIX_FAILURE;
            }
        }
    }
    return OSIX_SUCCESS;
}

INT4
SNMPConvertStringToOid (tSNMP_OID_TYPE * pOid, UINT1 *pu1Data, UINT1 u1HexFlag)
{
    UINT4               u4Count = 0, u4Dot = 0;
    UINT4               u4Value = 0;
    UINT1              *pu1String = pu1Data;
    INT1                i1RetVal = 0;

    pOid->u4_Length = 0;
    for (u4Count = 0; pu1String[u4Count] != '\0'; u4Count++)
    {
        if (pu1String[u4Count] == '.')
            u4Dot++;
    }
    for (u4Count = 0; u4Count < (u4Dot + 1); u4Count++)
    {
        if (isdigit ((INT4) *pu1String))
        {
            if (u1HexFlag == 0)
                i1RetVal = SNMPHexConvertSubOid (&pu1String, &u4Value);
            else
                i1RetVal = SNMPConvertSubOid (&pu1String, &u4Value);
            if (i1RetVal == -1)
            {
                return SNMP_FAILURE;
            }
            pOid->pu4_OidList[u4Count] = u4Value;
            pOid->u4_Length++;
        }
        else
        {
            return SNMP_FAILURE;
        }
        if (*pu1String == '.')
        {
            pu1String++;
        }
        else if (*pu1String != '\0')
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : SNMPConvertSubOid 
 *  Description     : Convert String pointer to decemial integer32
 *  Input           : ppu1String - String Pointer
 *  Output          : pu4Value - integer equivalent of ppu1String
 *  Returns         : 0 or -1
 ************************************************************************/

INT1
SNMPConvertSubOid (UINT1 **ppu1String, UINT4 *pu4Value)
{
    UINT4               u4Count = 0, u4Value = 0;
    UINT1               u1Value = 0;
    for (u4Count = 0; ((u4Count < 11) && (**ppu1String != '.') &&
                       (**ppu1String != '\0')); u4Count++)
    {
        if (!isdigit (**ppu1String))
        {
            return -1;
        }
        MEMCPY (&u1Value, *ppu1String, 1);
        u4Value = (u4Value * 10) + (0x0f & (UINT4) u1Value);
        (*ppu1String)++;
    }
    *pu4Value = u4Value;

    return 0;
}

/************************************************************************
 *  Function Name   : SNMPHexConvertSubOid 
 *  Description     : Convert String pointer to Hexa integer32
 *  Input           : ppu1String - String Pointer
 *  Output          : pu4Value - integer equivalent of ppu1String
 *  Returns         : 0 or -1
 ************************************************************************/

INT1
SNMPHexConvertSubOid (UINT1 **ppu1String, UINT4 *pu4Value)
{
    UINT4               u4Count = 0, u4Value = 0;
    UINT1               u1Value = 0;
    for (u4Count = 0; ((u4Count < 11) && (**ppu1String != '.') &&
                       (**ppu1String != '\0')); u4Count++)
    {
        if (!((**ppu1String >= 'a' && **ppu1String < 'g')
              || isdigit (**ppu1String)))
        {
            return -1;
        }
        else
        {
            if (!isdigit (**ppu1String))
            {
                **ppu1String = (UINT1) (10 + ((**ppu1String) - 'a'));
            }
        }
        MEMCPY (&u1Value, *ppu1String, 1);
        u4Value = (u4Value * 16) + (0x0f & (UINT4) u1Value);
        (*ppu1String)++;
    }
    *pu4Value = u4Value;

    return 0;
}

VOID
SNMPConvertcolonstrToOidString (UINT1 *pCStr, UINT1 *pDStr)
{
    UINT4               u4Count = 0;
    UINT1              *pSrcStr = pCStr;
    UINT1              *pDestStr = pDStr;

    for (u4Count = 0; u4Count < MAX_OID_LEN; u4Count++)
    {
        if (*pSrcStr == '\0')
        {
            *pDestStr = *pSrcStr;
            break;
        }
        else if (*pSrcStr == ',')
        {
            *pDestStr = '.';
        }
        else
        {
            *pDestStr = *pSrcStr;
        }
        pDestStr++;
        pSrcStr++;
    }
}

/************************************************************************
 *  Function Name   : SNMPGetDefaultValueForObj
 *  Description     : Sends update event trigger to MSR if Set operation 
 *              for a varbind is successful inside nmhSet
 *  Input           : pMultiIndex - This variable contains the number of 
 *              index in the received OID and index value in case of 
 *              tabular OID and in case of scalar num index is 0
 *                    is_row_status_present- in case of tabular 
 *            True when row status oid recived in update.
 *            False when other object is received.
 *                    pMultiData - This variable contains the OID ,
 *             OID tye and its value. In case of tabular OID 
 *            shall be Rowstatus OID can be  ACTIVE or DESTROY.
 *  Output          : pOid - Full Oid (Oid + Instance)
 *  Returns         : None
 ************************************************************************/
INT4
SNMPGetDefaultValueForObj (tSNMP_OID_TYPE OID,
                           tSNMP_MULTI_DATA_TYPE * pVal, UINT4 *pError)
{
    tSnmpDbEntry        SnmpDbEntry;
    UINT4               u4Len = 0;
    tMbDbEntry         *pCur = NULL, *pNext = NULL;
    UINT4               u4Count1 = 0, u4_value = 0;
    UINT1               au1Tmp[MAX_OID_LENGTH];
    INT1                pi1DefVal[3];
    INT4                u4Index1 = 0, u4Index2 = 0, u4Index3 = 0;

    GetMbDbEntry (OID, &pNext, &u4Len, &SnmpDbEntry);
    pCur = SnmpDbEntry.pMibEntry;
    if (pCur != NULL)
    {
        if (pCur->pu1DefVal == NULL)
        {
            *pError = SNMP_EXCEPTION_NO_DEF_VAL;
            return (SNMP_FAILURE);
        }
        else
        {
            u4Len = strlen ((const char *) pCur->pu1DefVal);

            pVal->i2_DataType = (INT2) pCur->u4OIDType;    /*kloc */
            switch (pCur->u4OIDType)
            {
                case SNMP_DATA_TYPE_COUNTER32:
                case SNMP_DATA_TYPE_GAUGE32:
                case SNMP_DATA_TYPE_TIME_TICKS:
                    pVal->u4_ULongValue = (UINT4) ATOI (pCur->pu1DefVal);
                    break;

                case SNMP_DATA_TYPE_COUNTER64:
                    pVal->u8_Counter64Value.msn =
                        (UINT4) ATOI (pCur->pu1DefVal);
                    pVal->u8_Counter64Value.lsn =
                        (UINT4) ATOI (pCur->pu1DefVal + sizeof (UINT4));
                    pVal->i2_DataType = SNMP_DATA_TYPE_COUNTER64;
                    break;

                case SNMP_DATA_TYPE_INTEGER32:    /* handle signed integers */
                    pVal->i4_SLongValue = ATOI (pCur->pu1DefVal);
                    break;
                case SNMP_DATA_TYPE_OBJECT_ID:
                    MEMSET (au1Tmp, SNMP_ZERO, MAX_OID_LENGTH);

                    SNMPConvertcolonstrToOidString ((UINT1 *) pCur->pu1DefVal,
                                                    au1Tmp);
                    SNMPConvertStringToOid (pVal->pOidValue, au1Tmp, 1);

                    break;
                case SNMP_DATA_TYPE_OCTET_PRIM:
                case SNMP_DATA_TYPE_OPAQUE:
                    for (u4Count1 = 0; u4Count1 < u4Len; u4Count1++)
                    {
                        pVal->pOctetStrValue->pu1_OctetList[u4Count1]
                            = (UINT1) pCur->pu1DefVal[u4Count1];
                    }
                    pVal->pOctetStrValue->i4_Length = (INT4) u4Count1;
                    break;

                case SNMP_DATA_TYPE_IP_ADDR_PRIM:
                    pVal->u4_ULongValue = (UINT4) ATOI (pCur->pu1DefVal);
                    break;

                case SNMP_DATA_TYPE_MAC_ADDRESS:
                    memset (pVal->pOctetStrValue->pu1_OctetList, 0, 6);
                    while (u4Index1 <
                           (INT4) strlen ((const char *) pCur->pu1DefVal))
                    {
                        u4Index2 = 0;
                        pi1DefVal[u4Index2++] = pCur->pu1DefVal[u4Index1++];
                        pi1DefVal[u4Index2++] = pCur->pu1DefVal[u4Index1++];
                        pi1DefVal[u4Index2] = '\0';
                        sscanf ((const char *) pi1DefVal, "%x", &u4_value);
                        memcpy (pVal->pOctetStrValue->pu1_OctetList + u4Index3,
                                &u4_value, 1);
                        u4Index3++;
                    }
                    break;

                default:
                    SNMPTrace ("Unknown tag received\n");
                    return (SNMP_FAILURE);
            }
        }
    }
    else
    {
        *pError = SNMP_EXCEPTION_NO_SUCH_OBJECT;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/********************  END OF FILE   ***************************************/
