/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: snmpweb.c,v 1.1 2015/04/28 12:35:03 siva Exp $
 *
 * Description: This file contains SNMP  web interface routines.
 *
 *******************************************************************/

#ifndef __SNMPWEB_C__
#define __SNMPWEB_C__

#include "snmpcmn.h"
#include "fswebnm.h"
#include "snmpweb.h"
#include "fssnmp.h"
#include "fssnmp3lw.h"
#include "snmusmlw.h"
#include "stdvaclw.h"
#include "snmcomlw.h"
#include "snmpnolw.h"
#include "sntarglw.h"

/*********************************************************************
*  Function Name : IssProcessSnmpBasicConfPage
*  Description   : This function processes the request for the SNMP   
*                  basic conf page
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessSnmpBasicConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessSnmpBasicConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessSnmpBasicConfPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessSnmpBasicConfPageGet
*  Description   : This function processes the request to set SNMP 
*                  basic conf page 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessSnmpBasicConfPageGet (tHttp * pHttp)
{
    INT4                i4RetVal = 0;

    STRCPY (pHttp->au1KeyString, "SNMP_AGENT_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetSnmpAgentControl (&i4RetVal);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%ld", i4RetVal);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "SNMP_VERSION_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetSnmpAllowedPduVersions (&i4RetVal);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%ld", i4RetVal);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "MIN_SECURITY_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetSnmpMinimumSecurityRequired (&i4RetVal);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%ld", i4RetVal);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
*  Function Name : IssProcessSnmpBasicConfPageSet
*  Description   : This function processes the request to set SNMP
*                  basic conf page 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessSnmpBasicConfPageSet (tHttp * pHttp)
{
    INT4                i4Value = 0;
    UINT4               u4ErrorCode = 0;

    STRCPY (pHttp->au1Name, "SNMP_AGENT");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Value = ATOI (pHttp->au1Value);

    if (nmhTestv2SnmpAgentControl (&u4ErrorCode, i4Value) != SNMP_FAILURE)
    {
        nmhSetSnmpAgentControl (i4Value);
    }

    STRCPY (pHttp->au1Name, "SNMP_VERSION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Value = ATOI (pHttp->au1Value);
    if (nmhTestv2SnmpAllowedPduVersions (&u4ErrorCode, i4Value) != SNMP_FAILURE)
    {
        nmhSetSnmpAllowedPduVersions (i4Value);
    }

    STRCPY (pHttp->au1Name, "MIN_SECURITY");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Value = ATOI (pHttp->au1Value);
    if (nmhTestv2SnmpMinimumSecurityRequired (&u4ErrorCode, i4Value)
        != SNMP_FAILURE)
    {
        nmhSetSnmpMinimumSecurityRequired (i4Value);
    }
    IssProcessSnmpBasicConfPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssProcessSnmpUserConfPage
*  Description   : This function processes the request for the SNMP   
*                  User config page
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessSnmpUserConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessSnmpUserConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessSnmpUserConfPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessSnmpUserConfPageGet
*  Description   : This function processes the request to set SNMP 
*                  User config page 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessSnmpUserConfPageGet (tHttp * pHttp)
{
    UINT1               au1UsmUserEngineID[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1NextUsmUserEngineID[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1UsmUserName[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1NextUsmUserName[SNMP_MAX_OCTETSTRING_SIZE];
    UINT4               au4Protocol[SNMP_MAX_OCTETSTRING_SIZE];
    UINT4               u4Temp = 0;
    pHttp->i4Write = 0;
    tSNMP_OCTET_STRING_TYPE UsmUserEngineID;
    tSNMP_OCTET_STRING_TYPE NextUsmUserEngineID;
    tSNMP_OCTET_STRING_TYPE UsmUserName;
    tSNMP_OCTET_STRING_TYPE NextUsmUserName;
    tSNMP_OID_TYPE      Protocol;

    UsmUserEngineID.pu1_OctetList = &au1UsmUserEngineID[0];
    NextUsmUserEngineID.pu1_OctetList = &au1NextUsmUserEngineID[0];
    UsmUserName.pu1_OctetList = &au1UsmUserName[0];
    NextUsmUserName.pu1_OctetList = &au1NextUsmUserName[0];
    Protocol.pu4_OidList = &au4Protocol[0];

    MEMSET (UsmUserEngineID.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET (NextUsmUserEngineID.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET (UsmUserName.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET (NextUsmUserName.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET (Protocol.pu4_OidList, 0, sizeof (au4Protocol));

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    if (nmhGetFirstIndexUsmUserTable (&NextUsmUserEngineID, &NextUsmUserName)
        != SNMP_SUCCESS)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }
    u4Temp = pHttp->i4Write;
    do
    {
        pHttp->i4Write = u4Temp;
        MEMSET (UsmUserEngineID.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
        MEMSET (UsmUserName.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);

        STRCPY (pHttp->au1KeyString, "USER_NAME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                 NextUsmUserName.pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));

        STRCPY (pHttp->au1KeyString, "ENGINE_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        MEMSET (pHttp->au1DataString, 0, (NextUsmUserEngineID.i4_Length + 1));
        SnmpConvertOctetToString (&NextUsmUserEngineID, pHttp->au1DataString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "AUTH_PROTO_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetUsmUserAuthProtocol (&NextUsmUserEngineID,
                                   &NextUsmUserName, &Protocol);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%ld",
                 Protocol.pu4_OidList[Protocol.u4_Length - 1]);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "PRIV_KEY_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetUsmUserPrivProtocol (&NextUsmUserEngineID,
                                   &NextUsmUserName, &Protocol);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%ld",
                 Protocol.pu4_OidList[Protocol.u4_Length - 1]);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

        MEMCPY (UsmUserName.pu1_OctetList,
                NextUsmUserName.pu1_OctetList, NextUsmUserName.i4_Length);
        MEMCPY (UsmUserEngineID.pu1_OctetList,
                NextUsmUserEngineID.pu1_OctetList,
                NextUsmUserEngineID.i4_Length);

        UsmUserName.i4_Length = NextUsmUserName.i4_Length;
        UsmUserEngineID.i4_Length = NextUsmUserEngineID.i4_Length;

        MEMSET (NextUsmUserEngineID.pu1_OctetList, 0,
                SNMP_MAX_OCTETSTRING_SIZE);
        MEMSET (NextUsmUserName.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);

    }
    while (nmhGetNextIndexUsmUserTable (&UsmUserEngineID, &NextUsmUserEngineID,
                                        &UsmUserName, &NextUsmUserName)
           == SNMP_SUCCESS);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
*  Function Name : IssProcessSnmpUserConfPageSet
*  Description   : This function processes the request to set SNMP
*                  User config page 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessSnmpUserConfPageSet (tHttp * pHttp)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4AuthProto = 0;
    INT4                i4RowStatus = 0;
    UINT4               u4IsCreated = FALSE;
    tSNMP_OCTET_STRING_TYPE EngineID;
    tSNMP_OCTET_STRING_TYPE UserName;
    tSNMP_OCTET_STRING_TYPE AuthPassword;
    tSNMP_OCTET_STRING_TYPE PrivPassword;
    tSNMP_OID_TYPE      Protocol;
    UINT1               au1UserName[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1EngineId[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1AuthKey[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1PrivKey[SNMP_MAX_OCTETSTRING_SIZE];
    UINT4               au4Protocol[SNMP_MAX_OCTETSTRING_SIZE];

    MEMSET (au1UserName, 0, SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET (au1EngineId, 0, SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET (au1AuthKey, 0, SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET (au1PrivKey, 0, SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET (au4Protocol, 0, sizeof (au4Protocol));

    UserName.pu1_OctetList = &au1UserName[0];
    EngineID.pu1_OctetList = &au1EngineId[0];
    Protocol.pu4_OidList = &au4Protocol[0];
    AuthPassword.pu1_OctetList = &au1AuthKey[0];
    PrivPassword.pu1_OctetList = &au1PrivKey[0];

    STRCPY (pHttp->au1Name, "USER_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRCPY (UserName.pu1_OctetList, pHttp->au1Value);
    UserName.i4_Length = STRLEN (UserName.pu1_OctetList);

    SNMPCopyOctetString (&EngineID, &gSnmpEngineID);

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Delete") == 0)
    {
        if (nmhTestv2UsmUserStatus (&u4ErrorCode, &EngineID, &UserName, DESTROY)
            != SNMP_SUCCESS)
        {
            IssSendError (pHttp, (INT1 *) "Delete User Name");
            return;
        }
        nmhSetUsmUserStatus (&EngineID, &UserName, DESTROY);
    }
    else if (STRCMP (pHttp->au1Value, "Add") == 0)
    {
        if (nmhGetUsmUserStatus (&EngineID, &UserName, &i4RowStatus)
            != SNMP_SUCCESS)
        {
            if (nmhTestv2UsmUserStatus (&u4ErrorCode, &EngineID, &UserName,
                                        CREATE_AND_WAIT) != SNMP_SUCCESS)
            {
                IssSendError (pHttp, (INT1 *) "Unable to Add User");
                return;
            }
            nmhSetUsmUserStatus (&EngineID, &UserName, CREATE_AND_WAIT);
            u4IsCreated = TRUE;
        }
        else
        {
            if (nmhTestv2UsmUserStatus (&u4ErrorCode, &EngineID, &UserName,
                                        NOT_IN_SERVICE) != SNMP_SUCCESS)
            {
                IssSendError (pHttp, (INT1 *) "Unable to Modify User");
                return;
            }
            nmhSetUsmUserStatus (&EngineID, &UserName, NOT_IN_SERVICE);
        }

        STRCPY (pHttp->au1Name, "AUTH_PROTO");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4AuthProto = ATOI (pHttp->au1Value);
        if (u4AuthProto != 1)
        {
            MEMCPY (Protocol.pu4_OidList, au4StdAuthOid,
                    ISS_WEB_SNMP_PROTO_OID_LEN * sizeof (UINT4));
            Protocol.pu4_OidList[ISS_WEB_SNMP_PROTO_OID_LEN] = u4AuthProto;
            Protocol.u4_Length = ISS_WEB_SNMP_PROTO_OID_LEN + 1;

            if (nmhTestv2UsmUserAuthProtocol (&u4ErrorCode, &EngineID,
                                              &UserName, &Protocol)
                != SNMP_SUCCESS)
            {
                if (u4IsCreated == TRUE)
                {
                    nmhSetUsmUserStatus (&EngineID, &UserName, DESTROY);
                }
                IssSendError (pHttp, (INT1 *) "Unable to Set Auth Protocol");
                return;
            }
            nmhSetUsmUserAuthProtocol (&EngineID, &UserName, &Protocol);

            STRCPY (pHttp->au1Name, "AUTH_KEY");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            if (STRLEN (pHttp->au1Value) == 0)
            {
                if (u4IsCreated == TRUE)
                {
                    nmhSetUsmUserStatus (&EngineID, &UserName, DESTROY);
                }
                IssSendError (pHttp, (INT1 *) "Auth Password Not Given");
                return;
            }
            MEMCPY (AuthPassword.pu1_OctetList, pHttp->au1Value,
                    STRLEN (pHttp->au1Value));
            AuthPassword.i4_Length = STRLEN (pHttp->au1Value);
            if (nmhTestv2UsmUserAuthKeyChange (&u4ErrorCode, &EngineID,
                                               &UserName, &AuthPassword)
                != SNMP_SUCCESS)
            {
                if (u4IsCreated == TRUE)
                {
                    nmhSetUsmUserStatus (&EngineID, &UserName, DESTROY);
                }
                IssSendError (pHttp, (INT1 *) "Unable to Set Auth Password");
                return;
            }
            nmhSetUsmUserAuthKeyChange (&EngineID, &UserName, &AuthPassword);

            STRCPY (pHttp->au1Name, "PRIV_KEY");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            if (STRLEN (pHttp->au1Value) != 0)
            {
                MEMCPY (Protocol.pu4_OidList, au4StdPrivOid,
                        ISS_WEB_SNMP_PROTO_OID_LEN * sizeof (UINT4));
                Protocol.pu4_OidList[ISS_WEB_SNMP_PROTO_OID_LEN] =
                    ISS_WEB_SNMP_DES_CBC;
                Protocol.u4_Length = ISS_WEB_SNMP_PROTO_OID_LEN + 1;

                if (nmhTestv2UsmUserPrivProtocol (&u4ErrorCode, &EngineID,
                                                  &UserName, &Protocol)
                    != SNMP_SUCCESS)
                {
                    if (u4IsCreated == TRUE)
                    {
                        nmhSetUsmUserStatus (&EngineID, &UserName, DESTROY);
                    }
                    IssSendError (pHttp,
                                  (INT1 *) "Unable to Set Priv Protocol");
                    return;
                }
                nmhSetUsmUserPrivProtocol (&EngineID, &UserName, &Protocol);

                MEMCPY (PrivPassword.pu1_OctetList, pHttp->au1Value,
                        STRLEN (pHttp->au1Value));
                PrivPassword.i4_Length = STRLEN (pHttp->au1Value);
                if (nmhTestv2UsmUserPrivKeyChange (&u4ErrorCode, &EngineID,
                                                   &UserName, &PrivPassword)
                    != SNMP_SUCCESS)
                {
                    if (u4IsCreated == TRUE)
                    {
                        nmhSetUsmUserStatus (&EngineID, &UserName, DESTROY);
                    }
                    IssSendError (pHttp,
                                  (INT1 *) "Unable to Set Priv Password");
                    return;
                }
                nmhSetUsmUserPrivKeyChange (&EngineID, &UserName,
                                            &PrivPassword);
            }
        }
        if (nmhTestv2UsmUserStorageType (&u4ErrorCode, &EngineID, &UserName,
                                         ISS_WEB_SNMP_NONVOLATILE)
            != SNMP_SUCCESS)
        {
            if (u4IsCreated == TRUE)
            {
                nmhSetUsmUserStatus (&EngineID, &UserName, DESTROY);
            }
            IssSendError (pHttp, (INT1 *) "Unable to Set Storage Type");
            return;
        }
        nmhSetUsmUserStorageType (&EngineID, &UserName,
                                  ISS_WEB_SNMP_NONVOLATILE);

        if (nmhTestv2UsmUserStatus (&u4ErrorCode, &EngineID, &UserName,
                                    ACTIVE) != SNMP_SUCCESS)
        {
            IssSendError (pHttp, (INT1 *) "Unable to Add User");
            return;
        }
        nmhSetUsmUserStatus (&EngineID, &UserName, ACTIVE);
    }
    IssProcessSnmpUserConfPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssProcessSnmpGroupConfPage
*  Description   : This function processes the request for the SNMP   
*                  group config page
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessSnmpGroupConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessSnmpGroupConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        if (IssProcessPost (pHttp) == SUCCESS)
        {
            IssProcessSnmpGroupConfPageGet (pHttp);
        }
    }
}

/*********************************************************************
*  Function Name : IssPrintAllSnmpUsers 
*  Description   : This function prints all the snmp users 
*                  in the system.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssPrintAllSnmpUsers (tHttp * pHttp)
{
    UINT1               au1UsmUserEngineID[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1NextUsmUserEngineID[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1UsmUserName[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1NextUsmUserName[SNMP_MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE UsmUserEngineID;
    tSNMP_OCTET_STRING_TYPE NextUsmUserEngineID;
    tSNMP_OCTET_STRING_TYPE UsmUserName;
    tSNMP_OCTET_STRING_TYPE NextUsmUserName;

    UsmUserEngineID.pu1_OctetList = &au1UsmUserEngineID[0];
    NextUsmUserEngineID.pu1_OctetList = &au1NextUsmUserEngineID[0];
    UsmUserName.pu1_OctetList = &au1UsmUserName[0];
    NextUsmUserName.pu1_OctetList = &au1NextUsmUserName[0];

    MEMSET (UsmUserEngineID.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET (NextUsmUserEngineID.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET (UsmUserName.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET (NextUsmUserName.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);

    if (nmhGetFirstIndexUsmUserTable (&NextUsmUserEngineID, &NextUsmUserName)
        != SNMP_SUCCESS)
    {
        return;
    }
    STRCPY (pHttp->au1KeyString, "<! SNMP_USERS>");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    do
    {
        MEMSET (UsmUserEngineID.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
        MEMSET (UsmUserName.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);

        SPRINTF ((CHR1 *) pHttp->au1DataString,
                 "<option value = \"%s\">%s \n",
                 NextUsmUserName.pu1_OctetList, NextUsmUserName.pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));

        MEMCPY (UsmUserName.pu1_OctetList,
                NextUsmUserName.pu1_OctetList, NextUsmUserName.i4_Length);
        MEMCPY (UsmUserEngineID.pu1_OctetList,
                NextUsmUserEngineID.pu1_OctetList,
                NextUsmUserEngineID.i4_Length);

        UsmUserName.i4_Length = NextUsmUserName.i4_Length;
        UsmUserEngineID.i4_Length = NextUsmUserEngineID.i4_Length;

        MEMSET (NextUsmUserEngineID.pu1_OctetList, 0,
                SNMP_MAX_OCTETSTRING_SIZE);
        MEMSET (NextUsmUserName.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);

    }
    while (nmhGetNextIndexUsmUserTable (&UsmUserEngineID, &NextUsmUserEngineID,
                                        &UsmUserName, &NextUsmUserName)
           == SNMP_SUCCESS);
}

/*********************************************************************
*  Function Name : IssPrintAllSnmpGroups 
*  Description   : This function prints all the snmp groups 
*                  in the system.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssPrintAllSnmpGroups (tHttp * pHttp)
{
    UINT1               au1SecName[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1NextSecName[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1UsmGroupName[SNMP_MAX_OCTETSTRING_SIZE];
    INT4                i4SecModel = 0;
    INT4                i4NextSecModel = 0;
    tSNMP_OCTET_STRING_TYPE SecName;
    tSNMP_OCTET_STRING_TYPE NextSecName;
    tSNMP_OCTET_STRING_TYPE UsmGroupName;

    SecName.pu1_OctetList = &au1SecName[0];
    NextSecName.pu1_OctetList = &au1NextSecName[0];
    UsmGroupName.pu1_OctetList = &au1UsmGroupName[0];

    MEMSET (SecName.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET (NextSecName.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET (UsmGroupName.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);

    if (nmhGetFirstIndexVacmSecurityToGroupTable (&i4NextSecModel, &NextSecName)
        != SNMP_SUCCESS)
    {
        return;
    }
    STRCPY (pHttp->au1KeyString, "<! SNMP_GROUPS>");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    do
    {
        MEMSET (SecName.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
        MEMSET (UsmGroupName.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);

        if (i4NextSecModel == 3)    /* SNMMP V3 */
        {
            nmhGetVacmGroupName (i4NextSecModel, &NextSecName, &UsmGroupName);

            SPRINTF ((CHR1 *) pHttp->au1DataString,
                     "<option value = \"%s\">%s \n",
                     UsmGroupName.pu1_OctetList, UsmGroupName.pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
        }

        MEMCPY (SecName.pu1_OctetList,
                NextSecName.pu1_OctetList, NextSecName.i4_Length);

        SecName.i4_Length = NextSecName.i4_Length;
        i4SecModel = i4NextSecModel;

        MEMSET (NextSecName.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);

    }
    while (nmhGetNextIndexVacmSecurityToGroupTable (i4SecModel, &i4NextSecModel,
                                                    &SecName, &NextSecName)
           == SNMP_SUCCESS);
}

/*********************************************************************
 *  Function Name : IssProcessSnmpGroupConfPageGet
 *  Description   : This function processes the request to set SNMP
 *                  Group config page 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessSnmpGroupConfPageGet (tHttp * pHttp)
{
    UINT1               au1SecurityName[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1NextSecurityName[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1GroupName[SNMP_MAX_OCTETSTRING_SIZE];
    INT4                i4SecurityModel = 0;
    INT4                i4NextSecurityModel = 0;
    INT4                i4Storage = 0;
    UINT4               u4Temp = 0;
    tSNMP_OCTET_STRING_TYPE SecurityName;
    tSNMP_OCTET_STRING_TYPE NextSecurityName;
    tSNMP_OCTET_STRING_TYPE GroupName;

    pHttp->i4Write = 0;

    SecurityName.pu1_OctetList = &au1SecurityName[0];
    NextSecurityName.pu1_OctetList = &au1NextSecurityName[0];
    GroupName.pu1_OctetList = &au1GroupName[0];

    IssPrintAllSnmpUsers (pHttp);

    MEMSET (NextSecurityName.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    if (nmhGetFirstIndexVacmSecurityToGroupTable (&i4NextSecurityModel,
                                                  &NextSecurityName)
        != SNMP_SUCCESS)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }
    u4Temp = pHttp->i4Write;
    do
    {
        pHttp->i4Write = u4Temp;
        MEMSET (SecurityName.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
        MEMSET (GroupName.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
        if (i4NextSecurityModel == 3)    /* SNMMP V3 */
        {
            STRCPY (pHttp->au1KeyString, "1.3.6.1.6.3.16.1.2.1.1_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%ld", i4NextSecurityModel);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "1.3.6.1.6.3.16.1.2.1.2_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     NextSecurityName.pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "1.3.6.1.6.3.16.1.2.1.3_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetVacmGroupName (i4NextSecurityModel, &NextSecurityName,
                                 &GroupName);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     GroupName.pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "1.3.6.1.6.3.16.1.2.1.4_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetVacmSecurityToGroupStorageType (i4NextSecurityModel,
                                                  &NextSecurityName,
                                                  &i4Storage);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%ld", i4Storage);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        }
        else
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        }
        i4SecurityModel = i4NextSecurityModel;
        MEMCPY (SecurityName.pu1_OctetList, NextSecurityName.pu1_OctetList,
                NextSecurityName.i4_Length);
        SecurityName.i4_Length = NextSecurityName.i4_Length;
        MEMSET (NextSecurityName.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
    }
    while (nmhGetNextIndexVacmSecurityToGroupTable (i4SecurityModel,
                                                    &i4NextSecurityModel,
                                                    &SecurityName,
                                                    &NextSecurityName)
           == SNMP_SUCCESS);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
*  Function Name : IssProcessSnmpAccessConfPage
*  Description   : This function processes the request for the SNMP   
*                  Accessconfig page
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessSnmpAccessConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessSnmpAccessConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        if (IssProcessPost (pHttp) == SUCCESS)
        {
            IssProcessSnmpAccessConfPageGet (pHttp);
        }
    }
}

/*********************************************************************
*  Function Name : IssProcessSnmpAccessConfPageGet
*  Description   : This function processes the request to set SNMP
*                  Access config page 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessSnmpAccessConfPageGet (tHttp * pHttp)
{

    UINT1               au1GroupName[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1NextGroupName[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1Context[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1NextContext[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1Retval[SNMP_MAX_OCTETSTRING_SIZE];
    UINT4               u4Temp = 0;
    INT4                i4RetVal = 0;
    INT4                i4SecLevel = 0;
    INT4                i4NextSecLevel = 0;
    INT4                i4SecMode = 0;
    INT4                i4NextSecMode = 0;
    tSNMP_OCTET_STRING_TYPE GroupName;
    tSNMP_OCTET_STRING_TYPE NextGroupName;
    tSNMP_OCTET_STRING_TYPE Context;
    tSNMP_OCTET_STRING_TYPE NextContext;
    tSNMP_OCTET_STRING_TYPE Retval;

    pHttp->i4Write = 0;

    GroupName.pu1_OctetList = &au1GroupName[0];
    NextGroupName.pu1_OctetList = &au1NextGroupName[0];
    Context.pu1_OctetList = &au1Context[0];
    NextContext.pu1_OctetList = &au1NextContext[0];
    Retval.pu1_OctetList = &au1Retval[0];

    MEMSET (NextGroupName.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET (NextContext.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);

    IssPrintAllSnmpGroups (pHttp);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    if (nmhGetFirstIndexVacmAccessTable (&NextGroupName, &NextContext,
                                         &i4NextSecMode, &i4NextSecLevel)
        != SNMP_SUCCESS)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }
    u4Temp = pHttp->i4Write;
    do
    {
        pHttp->i4Write = u4Temp;

        MEMSET (GroupName.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
        MEMSET (Context.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);

        if (i4NextSecMode == 3)    /* SNMPV3 */
        {
            STRCPY (pHttp->au1KeyString, "1.3.6.1.6.3.16.1.2.1.3_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     NextGroupName.pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "1.3.6.1.6.3.16.1.4.1.1_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     NextContext.pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "1.3.6.1.6.3.16.1.4.1.2_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%ld", i4NextSecMode);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "1.3.6.1.6.3.16.1.4.1.3_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%ld", i4NextSecLevel);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "1.3.6.1.6.3.16.1.4.1.5_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            MEMSET (Retval.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
            nmhGetVacmAccessReadViewName (&NextGroupName, &NextContext,
                                          i4NextSecMode, i4NextSecLevel,
                                          &Retval);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", Retval.pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "1.3.6.1.6.3.16.1.4.1.6_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            MEMSET (Retval.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
            nmhGetVacmAccessWriteViewName (&NextGroupName, &NextContext,
                                           i4NextSecMode, i4NextSecLevel,
                                           &Retval);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", Retval.pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "1.3.6.1.6.3.16.1.4.1.7_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            MEMSET (Retval.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
            nmhGetVacmAccessNotifyViewName (&NextGroupName, &NextContext,
                                            i4NextSecMode, i4NextSecLevel,
                                            &Retval);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", Retval.pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "1.3.6.1.6.3.16.1.4.1.8_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetVacmAccessStorageType (&NextGroupName, &NextContext,
                                         i4NextSecMode, i4NextSecLevel,
                                         &i4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%ld", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        }
        else
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        }

        MEMCPY (GroupName.pu1_OctetList,
                NextGroupName.pu1_OctetList, NextGroupName.i4_Length);
        MEMCPY (Context.pu1_OctetList,
                NextContext.pu1_OctetList, NextContext.i4_Length);

        Context.i4_Length = NextContext.i4_Length;
        GroupName.i4_Length = NextGroupName.i4_Length;
        i4SecMode = i4NextSecMode;
        i4SecLevel = i4NextSecLevel;

        MEMSET (NextGroupName.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
        MEMSET (NextContext.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);

    }
    while (nmhGetNextIndexVacmAccessTable (&GroupName, &NextGroupName,
                                           &Context, &NextContext,
                                           i4SecMode, &i4NextSecMode,
                                           i4SecLevel, &i4NextSecLevel)
           == SNMP_SUCCESS);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
*  Function Name : IssPrintAllSnmpViews
*  Description   : This function prints all the snmp groups 
*                  in the system.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssPrintAllSnmpViews (tHttp * pHttp)
{
    UINT1               au1GroupName[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1NextGroupName[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1Context[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1NextContext[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1Retval[SNMP_MAX_OCTETSTRING_SIZE];
    INT4                i4SecLevel = 0;
    INT4                i4NextSecLevel = 0;
    INT4                i4SecMode = 0;
    INT4                i4NextSecMode = 0;
    tSNMP_OCTET_STRING_TYPE GroupName;
    tSNMP_OCTET_STRING_TYPE NextGroupName;
    tSNMP_OCTET_STRING_TYPE Context;
    tSNMP_OCTET_STRING_TYPE NextContext;
    tSNMP_OCTET_STRING_TYPE Retval;

    GroupName.pu1_OctetList = &au1GroupName[0];
    NextGroupName.pu1_OctetList = &au1NextGroupName[0];
    Context.pu1_OctetList = &au1Context[0];
    NextContext.pu1_OctetList = &au1NextContext[0];
    Retval.pu1_OctetList = &au1Retval[0];

    MEMSET (NextGroupName.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET (NextContext.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);

    if (nmhGetFirstIndexVacmAccessTable (&NextGroupName, &NextContext,
                                         &i4NextSecMode, &i4NextSecLevel)
        != SNMP_SUCCESS)
    {
        return;
    }
    STRCPY (pHttp->au1KeyString, "<! SNMP_VIEWS>");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    do
    {
        if (i4NextSecMode == 3)    /* SNMPV3 */
        {
            MEMSET (GroupName.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
            MEMSET (Context.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);

            MEMSET (Retval.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
            nmhGetVacmAccessReadViewName (&NextGroupName, &NextContext,
                                          i4NextSecMode, i4NextSecLevel,
                                          &Retval);
            if (Retval.i4_Length > 0)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString,
                         "<option value = \"%s\">%s \n",
                         Retval.pu1_OctetList, Retval.pu1_OctetList);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
            }
            MEMSET (Retval.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
            nmhGetVacmAccessWriteViewName (&NextGroupName, &NextContext,
                                           i4NextSecMode, i4NextSecLevel,
                                           &Retval);
            if (Retval.i4_Length > 0)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString,
                         "<option value = \"%s\">%s \n",
                         Retval.pu1_OctetList, Retval.pu1_OctetList);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
            }

            MEMSET (Retval.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
            nmhGetVacmAccessNotifyViewName (&NextGroupName, &NextContext,
                                            i4NextSecMode, i4NextSecLevel,
                                            &Retval);
            if (Retval.i4_Length > 0)
            {

                SPRINTF ((CHR1 *) pHttp->au1DataString,
                         "<option value = \"%s\">%s \n",
                         Retval.pu1_OctetList, Retval.pu1_OctetList);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
            }
        }

        MEMCPY (GroupName.pu1_OctetList,
                NextGroupName.pu1_OctetList, NextGroupName.i4_Length);
        MEMCPY (Context.pu1_OctetList,
                NextContext.pu1_OctetList, NextContext.i4_Length);

        Context.i4_Length = NextContext.i4_Length;
        GroupName.i4_Length = NextGroupName.i4_Length;
        i4SecMode = i4NextSecMode;
        i4SecLevel = i4NextSecLevel;

        MEMSET (NextGroupName.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
        MEMSET (NextContext.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);

    }
    while (nmhGetNextIndexVacmAccessTable (&GroupName, &NextGroupName,
                                           &Context, &NextContext,
                                           i4SecMode, &i4NextSecMode,
                                           i4SecLevel, &i4NextSecLevel)
           == SNMP_SUCCESS);
}

/*********************************************************************
*  Function Name : IssProcessSnmpViewConfPage
*  Description   : This function processes the request for the SNMP   
*                  View page
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessSnmpViewConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessSnmpViewConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        if (IssProcessPost (pHttp) == SUCCESS)
        {
            IssProcessSnmpViewConfPageGet (pHttp);
        }
    }
}

/*********************************************************************
*  Function Name : IssProcessSnmpViewConfPageGet
*  Description   : This function processes the request for the SNMP   
*                  View page
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessSnmpViewConfPageGet (tHttp * pHttp)
{
    IssPrintAllSnmpViews (pHttp);
    IssProcessGet (pHttp);
}

/*********************************************************************
*  Function Name : IssProcessSnmpCommunityPage
*  Description   : This function processes the request for the SNMP   
*                  community page
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessSnmpCommunityPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessSnmpCommunityPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessSnmpCommunityPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessSnmpCommunityPageGet
*  Description   : This function processes the request for the SNMP   
*                  community page
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessSnmpCommunityPageGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    UINT4               u4Type = 0;
    UINT1               au1SnmpIndex[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1NextSnmpIndex[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1SecurityName[SNMP_MAX_OCTETSTRING_SIZE];

    tSNMP_OCTET_STRING_TYPE SnmpIndex;
    tSNMP_OCTET_STRING_TYPE NextSnmpIndex;
    tSNMP_OCTET_STRING_TYPE SecurityName;

    pHttp->i4Write = 0;
    SnmpIndex.pu1_OctetList = &au1SnmpIndex[0];
    NextSnmpIndex.pu1_OctetList = &au1NextSnmpIndex[0];
    SecurityName.pu1_OctetList = &au1SecurityName[0];
    MEMSET (NextSnmpIndex.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    if (nmhGetFirstIndexSnmpCommunityTable (&NextSnmpIndex) != SNMP_SUCCESS)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }
    u4Temp = pHttp->i4Write;
    do
    {
        pHttp->i4Write = u4Temp;

        STRCPY (pHttp->au1KeyString, "COMM_NAME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                 NextSnmpIndex.pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        MEMSET (SecurityName.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
        nmhGetSnmpCommunitySecurityName (&NextSnmpIndex, &SecurityName);
        STRCPY (pHttp->au1KeyString, "ACCESS_TYPE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        if (STRCMP (ISS_WEB_DEF_RO_SECURITY, SecurityName.pu1_OctetList) != 0)
        {
            u4Type = 2;
        }
        else
        {
            u4Type = 1;
        }
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%ld", u4Type);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

        MEMSET (SnmpIndex.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
        MEMCPY (SnmpIndex.pu1_OctetList, NextSnmpIndex.pu1_OctetList,
                NextSnmpIndex.i4_Length);
        SnmpIndex.i4_Length = NextSnmpIndex.i4_Length;
        MEMSET (NextSnmpIndex.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);

    }
    while (nmhGetNextIndexSnmpCommunityTable (&SnmpIndex, &NextSnmpIndex)
           == SNMP_SUCCESS);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
*  Function Name : IssProcessSnmpCommunityPageSet
*  Description   : This function processes the request for the SNMP   
*                  community page
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessSnmpCommunityPageSet (tHttp * pHttp)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4StorageType = 0;
    UINT1               au1CommunityIndex[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1CommunityName[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1SecurityName[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1ContextName[] = "none";
    UINT1               au1TagId[] = "";
    tSNMP_OCTET_STRING_TYPE CommunityIndex;

    STRCPY (pHttp->au1Name, "COMM_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);

    STRCPY (au1CommunityIndex, pHttp->au1Value);
    STRCPY (au1CommunityName, pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Delete") == 0)
    {
        CommunityIndex.pu1_OctetList = &au1CommunityIndex[0];
        CommunityIndex.i4_Length = STRLEN (au1CommunityIndex);
        if (SNMPDeleteCommunityEntry (&CommunityIndex) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (INT1 *) "Unable to Delete Snmp Community");
            return;
        }
    }
    else
    {
        STRCPY (pHttp->au1Name, "ACCESS_TYPE");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        if (1 == ATOI (pHttp->au1Value))
        {
            STRCPY (au1SecurityName, ISS_WEB_DEF_RO_SECURITY);
        }
        else
        {
            STRCPY (au1SecurityName, ISS_WEB_DEF_RW_SECURITY);
        }

        if (Snmp3CommunityConfig (au1CommunityIndex, au1CommunityName,
                                  au1SecurityName, au1ContextName, au1TagId,
                                  u4StorageType, &u4ErrorCode) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (INT1 *) "Unable to Add Snmp Community");
            return;
        }
    }
    IssProcessSnmpCommunityPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssPrintAllSnmpCommunity
*  Description   : This function prints all the snmp communities 
*                  in the system.
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssPrintAllSnmpCommunity (tHttp * pHttp)
{
    UINT1               au1SnmpIndex[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1NextSnmpIndex[SNMP_MAX_OCTETSTRING_SIZE];

    tSNMP_OCTET_STRING_TYPE SnmpIndex;
    tSNMP_OCTET_STRING_TYPE NextSnmpIndex;

    SnmpIndex.pu1_OctetList = &au1SnmpIndex[0];
    NextSnmpIndex.pu1_OctetList = &au1NextSnmpIndex[0];
    MEMSET (NextSnmpIndex.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
    if (nmhGetFirstIndexSnmpCommunityTable (&NextSnmpIndex) != SNMP_SUCCESS)
    {
        return;
    }
    STRCPY (pHttp->au1KeyString, "<! SNMP_COMMUNITY>");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    do
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString,
                 "<option value = \"%s\">%s \n",
                 NextSnmpIndex.pu1_OctetList, NextSnmpIndex.pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));

        MEMSET (SnmpIndex.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
        MEMCPY (SnmpIndex.pu1_OctetList, NextSnmpIndex.pu1_OctetList,
                NextSnmpIndex.i4_Length);
        SnmpIndex.i4_Length = NextSnmpIndex.i4_Length;
        MEMSET (NextSnmpIndex.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);

    }
    while (nmhGetNextIndexSnmpCommunityTable (&SnmpIndex, &NextSnmpIndex)
           == SNMP_SUCCESS);
    return;
}

/*********************************************************************
*  Function Name : IssConvertOctetToDecimalString 
*  Description   : This function converts the octet value to decimal 
*                  string.
*                  
*                     
*  Input(s)      : pOctet - Octet value. 
*                  u4OctetLen - Length of octet value.  
*  Output(s)     : pu1Temp - resultant string.
*  Return Values : None. 
*********************************************************************/
VOID
IssConvertOctetToDottedDecimal (UINT1 *pOctet, UINT4 u4OctetLen, UINT1 *pu1Temp)
{
    UINT1               au1Temp[ISS_MAX_NAME_LEN];
    UINT4               u4Count;
    pu1Temp[0] = '\0';
    MEMSET (au1Temp, 0, ISS_MAX_NAME_LEN);
    for (u4Count = 0; u4Count < u4OctetLen; u4Count++)
    {
        if (u4Count == u4OctetLen - 1)
        {
            SPRINTF ((CHR1 *) au1Temp, "%02d", pOctet[u4Count]);
        }
        else
        {
            SPRINTF ((CHR1 *) au1Temp, "%02d.", pOctet[u4Count]);
        }
        STRCAT (pu1Temp, au1Temp);
    }
}

/*********************************************************************
*  Function Name : IssGetSnmpNotificationName
*  Description   : This function is used to get SNMP notification   
*                  name from the target list
*                     
*  Input(s)      : pTargetList- Pointer to traget list.
*                  pu1CommunityName -Pointer to notification name
*  Output(s)     : None.
*  Return Values : SNMP_SUCCESS/SNMP_FAILURE
*********************************************************************/
INT4
IssGetSnmpNotificationName (tSNMP_OCTET_STRING_TYPE * pTargetList,
                            UINT1 *pu1CommunityName)
{
    UINT1               au1SnmpNotifyName[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1NextSnmpNotifyName[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1SnmpNotifyTag[SNMP_MAX_OCTETSTRING_SIZE];

    tSNMP_OCTET_STRING_TYPE SnmpNotifyName;
    tSNMP_OCTET_STRING_TYPE NextSnmpNotifyName;
    tSNMP_OCTET_STRING_TYPE SnmpNotifyTag;

    SnmpNotifyName.pu1_OctetList = au1SnmpNotifyName;
    NextSnmpNotifyName.pu1_OctetList = au1NextSnmpNotifyName;
    SnmpNotifyTag.pu1_OctetList = au1SnmpNotifyTag;
    if (nmhGetFirstIndexSnmpNotifyTable (&NextSnmpNotifyName) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    do
    {
        nmhGetSnmpNotifyTag (&NextSnmpNotifyName, &SnmpNotifyTag);

        if (MEMCMP (SnmpNotifyTag.pu1_OctetList, pTargetList->pu1_OctetList,
                    SnmpNotifyTag.i4_Length) == 0)
        {
            MEMCPY (pu1CommunityName, NextSnmpNotifyName.pu1_OctetList,
                    NextSnmpNotifyName.i4_Length);
            return SNMP_SUCCESS;
        }
        SNMPCopyOctetString (&SnmpNotifyName, &NextSnmpNotifyName);
    }
    while (nmhGetNextIndexSnmpNotifyTable (&SnmpNotifyName,
                                           &NextSnmpNotifyName)
           == SNMP_SUCCESS);
    return SNMP_FAILURE;
}

/*********************************************************************
*  Function Name : IssProcessSnmpManagerPage
*  Description   : This function processes the request for the SNMP   
*                  Manager page
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessSnmpManagerPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessSnmpManagerPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessSnmpManagerPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessSnmpManagerPageGet
*  Description   : This function processes the request for the SNMP   
*                  manager page
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessSnmpManagerPageGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    UINT4               u4SecModel = 0;
    UINT4               u4SecLevel = 1;
    UINT4               u4NotifyType = 0;
    UINT4               u4RetransCount = 0;
    UINT4               u4RetransGap = 0;

    UINT1               au1TargetAddrName[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1NextTargetAddrName[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1TargetAddress[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1TargetParams[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1TargetList[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1CommunityName[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1UserName[SNMP_MAX_OCTETSTRING_SIZE];

    tSNMP_OCTET_STRING_TYPE TargetAddrName;
    tSNMP_OCTET_STRING_TYPE NextTargetAddrName;
    tSNMP_OCTET_STRING_TYPE TargetParams;
    tSNMP_OCTET_STRING_TYPE TargetAddress;
    tSNMP_OCTET_STRING_TYPE TargetList;
    tSNMP_OCTET_STRING_TYPE UserName;
    tSNMP_OCTET_STRING_TYPE NotifyName;

    TargetAddrName.pu1_OctetList = au1TargetAddrName;
    NextTargetAddrName.pu1_OctetList = au1NextTargetAddrName;
    TargetAddress.pu1_OctetList = au1TargetAddress;
    TargetParams.pu1_OctetList = au1TargetParams;
    TargetList.pu1_OctetList = au1TargetList;
    UserName.pu1_OctetList = au1UserName;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    MEMSET (NextTargetAddrName.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
    if (nmhGetFirstIndexSnmpTargetAddrTable (&NextTargetAddrName) !=
        SNMP_SUCCESS)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }
    u4Temp = pHttp->i4Write;
    do
    {
        pHttp->i4Write = u4Temp;

        STRCPY (pHttp->au1KeyString, "MANAGER_NAME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                 NextTargetAddrName.pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "MANAGER_ADDR_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        MEMSET (TargetAddress.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
        nmhGetSnmpTargetAddrTAddress (&NextTargetAddrName, &TargetAddress);
        IssConvertOctetToDottedDecimal (TargetAddress.pu1_OctetList,
                                        TargetAddress.i4_Length,
                                        pHttp->au1DataString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "SNMP_VERSION_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        MEMSET (TargetParams.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
        nmhGetSnmpTargetAddrParams (&NextTargetAddrName, &TargetParams);
        nmhGetSnmpTargetParamsSecurityModel (&TargetParams, &u4SecModel);
        if (u4SecModel != 3)
        {
            u4SecModel = 2;
        }
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%ld", u4SecModel);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));

        STRCPY (pHttp->au1KeyString, "TGT_PARAM_NAME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                 TargetParams.pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        MEMSET (TargetList.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
        nmhGetSnmpTargetAddrTagList (&NextTargetAddrName, &TargetList);

        STRCPY (pHttp->au1KeyString, "COMMUNITY_NAME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        MEMSET (au1CommunityName, 0, SNMP_MAX_OCTETSTRING_SIZE);
        if (u4SecModel == 2)
        {
            IssGetSnmpNotificationName (&TargetList, &au1CommunityName[0]);
        }
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1CommunityName);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));

        STRCPY (pHttp->au1KeyString, "TGT_LIST_NAME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", TargetList.pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        if (u4SecModel == 3)
        {
            IssGetSnmpNotificationName (&TargetList, &au1CommunityName[0]);
            nmhGetSnmpTargetParamsSecurityLevel (&TargetParams, &u4SecLevel);
        }

        STRCPY (pHttp->au1KeyString, "USER_NAME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        MEMSET (UserName.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
        nmhGetSnmpTargetParamsSecurityName (&TargetParams, &UserName);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", UserName.pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "SEC_LEVEL_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        MEMSET (UserName.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%ld", u4SecLevel);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        MEMSET (au1CommunityName, 0, SNMP_MAX_OCTETSTRING_SIZE);
        if (IssGetSnmpNotificationName (&TargetList, &au1CommunityName[0])
            == SNMP_SUCCESS)
        {
            STRCPY (pHttp->au1KeyString, "TRAP_TYPE_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            NotifyName.pu1_OctetList = au1CommunityName;
            NotifyName.i4_Length = STRLEN (au1CommunityName);
            nmhGetSnmpNotifyType (&NotifyName, &u4NotifyType);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%ld", u4NotifyType);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
        }
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "INFORM_INT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetSnmpTargetAddrTimeout (&NextTargetAddrName, &u4RetransGap);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%ld", (u4RetransGap / 100));
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "INFORM_CNT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetSnmpTargetAddrRetryCount (&NextTargetAddrName, &u4RetransCount);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%ld", u4RetransCount);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

        MEMSET (TargetAddrName.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
        MEMCPY (TargetAddrName.pu1_OctetList, NextTargetAddrName.pu1_OctetList,
                NextTargetAddrName.i4_Length);
        TargetAddrName.i4_Length = NextTargetAddrName.i4_Length;
        MEMSET (NextTargetAddrName.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);

    }
    while (nmhGetNextIndexSnmpTargetAddrTable (&TargetAddrName,
                                               &NextTargetAddrName)
           == SNMP_SUCCESS);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
*  Function Name : IssSNMPManagersVerifyNotifyTable
*  Description   : This function Verifies weather any other manager 
*                  is linked with the same notify name and returns 
*                  SUCCESS if there exists one more manager.
*  Input(s)      : Notify Name of the SNMP Trap Manager For which 
*                  Request has come.
*  Output(s)     : None.
*  Return Values : SUCCESS / FAILURE
*********************************************************************/
INT1
IssSNMPManagersVerifyNotifyTable (tSNMP_OCTET_STRING_TYPE * pSnmpNotifyName)
{
    UINT1               au1TargetAddrName[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1NextTargetAddrName[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1TargetList[SNMP_MAX_OCTETSTRING_SIZE];

    tSNMP_OCTET_STRING_TYPE TargetAddrName;
    tSNMP_OCTET_STRING_TYPE NextTargetAddrName;
    tSNMP_OCTET_STRING_TYPE TargetList;

    TargetAddrName.pu1_OctetList = au1TargetAddrName;
    NextTargetAddrName.pu1_OctetList = au1NextTargetAddrName;
    TargetList.pu1_OctetList = au1TargetList;

    MEMSET (au1TargetAddrName, 0, SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET (au1NextTargetAddrName, 0, SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET (au1TargetList, 0, SNMP_MAX_OCTETSTRING_SIZE);

    if (nmhGetFirstIndexSnmpTargetAddrTable (&NextTargetAddrName)
        == SNMP_FAILURE)
    {
        return SUCCESS;
    }

    do
    {
        if (nmhGetSnmpTargetAddrTagList (&NextTargetAddrName, &TargetList)
            == SNMP_SUCCESS)
        {
            if (STRCMP
                (TargetList.pu1_OctetList, pSnmpNotifyName->pu1_OctetList) == 0)
            {
                return FAILURE;
            }
        }

        SNMPCopyOctetString (&TargetAddrName, &NextTargetAddrName);

    }
    while (nmhGetNextIndexSnmpTargetAddrTable
           (&TargetAddrName, &NextTargetAddrName) == SNMP_SUCCESS);

    return SUCCESS;
}

/*********************************************************************
*  Function Name : IssProcessSnmpManagerPageSet
*  Description   : This function processes the request for the SNMP   
*                  manager page
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
VOID
IssProcessSnmpManagerPageSet (tHttp * pHttp)
{
    UINT4               u4SecModel = 0;
    UINT4               u4MpModel = 0;
    UINT4               u4SecLevel = 1;
    UINT4               u4NotifyType = 0;
    UINT4               u4RetransCount = 0;
    UINT4               u4RetransGap = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4RowStatus = 0;
    UINT4               u4IsCreated = FALSE;

    UINT1               au1TargetAddrName[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1TargetAddress[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1TargetParams[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1TargetList[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1SecurityName[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1UserName[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1NotifyName[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1TempNotifyName[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1Action[20];
    UINT1               au1None[5] = "none";

    tSNMP_OCTET_STRING_TYPE TargetAddrName;
    tSNMP_OCTET_STRING_TYPE SecurityName;
    tSNMP_OCTET_STRING_TYPE TargetParams;
    tSNMP_OCTET_STRING_TYPE TargetAddress;
    tSNMP_OCTET_STRING_TYPE TargetList;
    tSNMP_OCTET_STRING_TYPE UserName;
    tSNMP_OCTET_STRING_TYPE NotifyName;
    tSNMP_OCTET_STRING_TYPE TempNotifyName;

    TargetAddrName.pu1_OctetList = au1TargetAddrName;
    SecurityName.pu1_OctetList = au1SecurityName;
    TargetAddress.pu1_OctetList = au1TargetAddress;
    TargetParams.pu1_OctetList = au1TargetParams;
    TargetList.pu1_OctetList = au1TargetList;
    UserName.pu1_OctetList = au1UserName;
    TempNotifyName.pu1_OctetList = au1TempNotifyName;

    MEMSET (au1TargetAddrName, 0, SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET (au1SecurityName, 0, SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET (au1TargetAddress, 0, SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET (au1TargetParams, 0, SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET (au1TargetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET (au1UserName, 0, SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET (au1TempNotifyName, 0, SNMP_MAX_OCTETSTRING_SIZE);

    STRCPY (pHttp->au1Name, "MANAGER_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);
    STRCPY (TargetAddrName.pu1_OctetList, pHttp->au1Value);
    TargetAddrName.i4_Length = STRLEN (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    MEMSET (au1Action, 0, sizeof (au1Action));
    STRCPY (au1Action, pHttp->au1Value);

    SNMPCopyOctetString (&TargetParams, &TargetAddrName);

    STRCPY (pHttp->au1Name, "MANAGER_ADDR");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4IpAddr = INET_ADDR (pHttp->au1Value);
    MEMCPY (TargetAddress.pu1_OctetList, &u4IpAddr, 4);
    TargetAddress.i4_Length = 4;

    STRCPY (pHttp->au1Name, "INFORM_INT");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4RetransGap = ATOI (pHttp->au1Value);
    u4RetransGap = (u4RetransGap * 100);

    STRCPY (pHttp->au1Name, "INFORM_CNT");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4RetransCount = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "SNMP_VERSION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4SecModel = ATOI (pHttp->au1Value);

    if (u4SecModel == 2)        /* It Is SNMPv2 */
    {
        u4MpModel = 1;            /* Set Message processing model as SNMPv2 */
        STRCPY (SecurityName.pu1_OctetList, au1None);
        STRCPY (pHttp->au1Name, "COMMUNITY_NAME");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        issDecodeSpecialChar (pHttp->au1Value);
        STRCPY (au1NotifyName, pHttp->au1Value);
    }
    else                        /* It Is SNMPv3 */
    {
        u4MpModel = 3;            /* Set Message processing model as SNMPv3 */
        STRCPY (pHttp->au1Name, "USER_NAME");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        STRCPY (SecurityName.pu1_OctetList, pHttp->au1Value);
        STRCPY (au1NotifyName, pHttp->au1Value);

        STRCPY (pHttp->au1Name, "SEC_LEVEL");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4SecLevel = ATOI (pHttp->au1Value);
    }

    NotifyName.pu1_OctetList = au1NotifyName;
    NotifyName.i4_Length = STRLEN (au1NotifyName);
    SNMPCopyOctetString (&TargetList, &NotifyName);

    STRCPY (pHttp->au1Name, "TRAP_TYPE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4NotifyType = ATOI (pHttp->au1Value);

    if (STRCMP (au1Action, "Delete") == 0)
    {
        i4RowStatus = DESTROY;
        if (nmhTestv2SnmpTargetAddrRowStatus (&u4ErrorCode, &TargetAddrName,
                                              i4RowStatus) != SNMP_SUCCESS)
        {
            IssSendError (pHttp, (INT1 *) "Unable to Set Delete Manager");
            return;
        }
        if (nmhSetSnmpTargetAddrRowStatus (&TargetAddrName, i4RowStatus)
            != SNMP_SUCCESS)
        {
            IssSendError (pHttp, (INT1 *) "Unable to Delete Manager ");
            return;
        }
        if (nmhTestv2SnmpTargetParamsRowStatus (&u4ErrorCode, &TargetParams,
                                                i4RowStatus) != SNMP_SUCCESS)
        {
            IssSendError (pHttp, (INT1 *) "Unable to Delete Manager Values");
            return;
        }
        if (nmhSetSnmpTargetParamsRowStatus (&TargetParams, i4RowStatus)
            != SNMP_SUCCESS)
        {
            IssSendError (pHttp, (INT1 *) "Unable to Delete Manager Values");
            return;
        }

        if (IssSNMPManagersVerifyNotifyTable (&NotifyName) == SUCCESS)
        {
            if (nmhTestv2SnmpNotifyRowStatus (&u4ErrorCode, &NotifyName,
                                              i4RowStatus) != SNMP_SUCCESS)
            {
                IssSendError (pHttp, (INT1 *) "Unable to Delete Notify View");
                return;
            }
            if (nmhSetSnmpNotifyRowStatus (&NotifyName, i4RowStatus)
                != SNMP_SUCCESS)
            {
                IssSendError (pHttp, (INT1 *) "Unable to Delete Notify View");
                return;
            }
        }
        IssProcessSnmpManagerPageGet (pHttp);
        return;
    }
    else if (STRCMP (au1Action, "Add") == 0)
    {
        i4RowStatus = CREATE_AND_WAIT;
        u4IsCreated = TRUE;
    }
    else
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhTestv2SnmpTargetAddrRowStatus (&u4ErrorCode, &TargetAddrName,
                                          i4RowStatus) != SNMP_SUCCESS)
    {
        IssSendError (pHttp, (INT1 *) "Unable to Set Manager Status");
        return;
    }
    if (nmhSetSnmpTargetAddrRowStatus (&TargetAddrName, i4RowStatus)
        != SNMP_SUCCESS)
    {
        IssSendError (pHttp, (INT1 *) "Unable to Set Manager Status");
        return;
    }

    if (nmhTestv2SnmpTargetAddrTAddress (&u4ErrorCode, &TargetAddrName,
                                         &TargetAddress) != SNMP_SUCCESS)
    {
        if (u4IsCreated == TRUE)
        {
            nmhSetSnmpTargetAddrRowStatus (&TargetAddrName, DESTROY);
        }
        IssSendError (pHttp, (INT1 *) "Unable to Set Manager Ip Address");
        return;
    }
    if (nmhSetSnmpTargetAddrTAddress (&TargetAddrName, &TargetAddress)
        != SNMP_SUCCESS)
    {
        if (u4IsCreated == TRUE)
        {
            nmhSetSnmpTargetAddrRowStatus (&TargetAddrName, DESTROY);
        }
        IssSendError (pHttp, (INT1 *) "Unable to Set Manager Ip Address");
        return;
    }

    if (nmhTestv2SnmpTargetAddrParams (&u4ErrorCode, &TargetAddrName,
                                       &TargetParams) != SNMP_SUCCESS)
    {
        if (u4IsCreated == TRUE)
        {
            nmhSetSnmpTargetAddrRowStatus (&TargetAddrName, DESTROY);
        }
        IssSendError (pHttp, (INT1 *) "Unable to Set Manager Params");
        return;
    }
    if (nmhSetSnmpTargetAddrParams (&TargetAddrName, &TargetParams)
        != SNMP_SUCCESS)
    {
        if (u4IsCreated == TRUE)
        {
            nmhSetSnmpTargetAddrRowStatus (&TargetAddrName, DESTROY);
        }
        IssSendError (pHttp, (INT1 *) "Unable to Set Manager Params");
        return;
    }

    if (IssSNMPManagersVerifyNotifyTable (&NotifyName) == SUCCESS)
    {
        if (Snmp3NotifyConfig (au1NotifyName, TargetList.pu1_OctetList,
                               u4NotifyType, ISS_WEB_SNMP_NONVOLATILE,
                               &u4ErrorCode) == SNMP_FAILURE)
        {
            if (u4IsCreated == TRUE)
            {
                nmhSetSnmpTargetAddrRowStatus (&TargetAddrName, DESTROY);
            }
            IssSendError (pHttp, (INT1 *) "Unable to Configure Notification");
            return;
        }
    }

    STRCPY (pHttp->au1Name, "TGT_LIST_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);
    STRCPY (TempNotifyName.pu1_OctetList, pHttp->au1Value);
    TempNotifyName.i4_Length = STRLEN (pHttp->au1Value);

    if (IssSNMPManagersVerifyNotifyTable (&TempNotifyName) == SUCCESS)
    {
        if (nmhTestv2SnmpNotifyRowStatus (&u4ErrorCode, &TempNotifyName,
                                          DESTROY) != SNMP_SUCCESS)
        {
            return;
        }
        if (nmhSetSnmpNotifyRowStatus (&TempNotifyName, DESTROY)
            != SNMP_SUCCESS)
        {
            return;
        }
    }

    if (nmhTestv2SnmpTargetAddrTagList (&u4ErrorCode, &TargetAddrName,
                                        &TargetList) != SNMP_SUCCESS)
    {
        if (u4IsCreated == TRUE)
        {
            nmhSetSnmpTargetAddrRowStatus (&TargetAddrName, DESTROY);
        }
        IssSendError (pHttp, (INT1 *) "Unable to Set Manager List");
        return;
    }
    if (nmhSetSnmpTargetAddrTagList (&TargetAddrName, &TargetList)
        != SNMP_SUCCESS)
    {
        if (u4IsCreated == TRUE)
        {
            nmhSetSnmpTargetAddrRowStatus (&TargetAddrName, DESTROY);
        }
        IssSendError (pHttp, (INT1 *) "Unable to Set Manager List");
        return;
    }
    if (u4RetransGap != 0)
    {
        if (nmhTestv2SnmpTargetAddrTimeout (&u4ErrorCode, &TargetAddrName,
                                            u4RetransGap) != SNMP_SUCCESS)
        {
            if (u4IsCreated == TRUE)
            {
                nmhSetSnmpTargetAddrRowStatus (&TargetAddrName, DESTROY);
            }
            IssSendError (pHttp, (INT1 *) "Unable to Set Manager Timeout");
            return;
        }
        if (nmhSetSnmpTargetAddrTimeout (&TargetAddrName, u4RetransGap)
            != SNMP_SUCCESS)
        {
            if (u4IsCreated == TRUE)
            {
                nmhSetSnmpTargetAddrRowStatus (&TargetAddrName, DESTROY);
            }
            IssSendError (pHttp, (INT1 *) "Unable to Set Manager Timeout");
            return;
        }
    }
    if (u4RetransCount != 0)
    {
        if (nmhTestv2SnmpTargetAddrRetryCount (&u4ErrorCode, &TargetAddrName,
                                               u4RetransCount) != SNMP_SUCCESS)
        {
            if (u4IsCreated == TRUE)
            {
                nmhSetSnmpTargetAddrRowStatus (&TargetAddrName, DESTROY);
            }
            IssSendError (pHttp, (INT1 *) "Unable to Set Retry Count");
            return;
        }
        if (nmhSetSnmpTargetAddrRetryCount (&TargetAddrName, u4RetransCount)
            != SNMP_SUCCESS)
        {
            if (u4IsCreated == TRUE)
            {
                nmhSetSnmpTargetAddrRowStatus (&TargetAddrName, DESTROY);
            }
            IssSendError (pHttp, (INT1 *) "Unable to Set Retry Count");
            return;
        }
    }
    if (nmhSetSnmpTargetAddrStorageType (&TargetAddrName,
                                         ISS_WEB_SNMP_NONVOLATILE)
        != SNMP_SUCCESS)
    {
        if (u4IsCreated == TRUE)
        {
            nmhSetSnmpTargetAddrRowStatus (&TargetAddrName, DESTROY);
        }
        IssSendError (pHttp, (INT1 *) "Unable to Set Storage Type");
        return;
    }
    if (nmhSetSnmpTargetAddrRowStatus (&TargetAddrName, ACTIVE) != SNMP_SUCCESS)
    {
        if (u4IsCreated == TRUE)
        {
            nmhSetSnmpTargetAddrRowStatus (&TargetAddrName, DESTROY);
        }
        IssSendError (pHttp, (INT1 *) "Unable to Set Manager Status");
        return;
    }
    if (Snmp3TargetParamConfig (TargetParams.pu1_OctetList, u4MpModel,
                                u4SecModel, SecurityName.pu1_OctetList,
                                u4SecLevel, ISS_WEB_SNMP_NONVOLATILE,
                                &u4ErrorCode) == SNMP_FAILURE)
    {
        if (u4IsCreated == TRUE)
        {
            nmhSetSnmpTargetAddrRowStatus (&TargetAddrName, DESTROY);
        }
        IssSendError (pHttp, (INT1 *) "Unable to Configure Manager Parameters");
        return;
    }

    IssProcessSnmpManagerPageGet (pHttp);
}
#endif /*  __SNMPWEB_C__ */
