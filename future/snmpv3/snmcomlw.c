/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: snmcomlw.c,v 1.10 2015/04/28 12:35:02 siva Exp $
*
* Description: snmpcommunity mapping table Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "snmpcmn.h"
# include  "snmcomlw.h"

/* LOW LEVEL Routines for Table : SnmpCommunityTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceSnmpCommunityTable
 Input       :  The Indices
                SnmpCommunityIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceSnmpCommunityTable (tSNMP_OCTET_STRING_TYPE *
                                            pSnmpCommunityIndex)
{
    tCommunityMappingEntry *pCommunityEntry = NULL;
    pCommunityEntry = SNMPGetCommunityEntry (pSnmpCommunityIndex);
    if (pCommunityEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexSnmpCommunityTable
 Input       :  The Indices
                SnmpCommunityIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexSnmpCommunityTable (tSNMP_OCTET_STRING_TYPE *
                                    pSnmpCommunityIndex)
{
    tCommunityMappingEntry *pFirst = NULL;
    pFirst = SNMPGetFirstCommunity ();
    if (pFirst == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pSnmpCommunityIndex, &(pFirst->CommIndex));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexSnmpCommunityTable
 Input       :  The Indices
                SnmpCommunityIndex
                nextSnmpCommunityIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexSnmpCommunityTable (tSNMP_OCTET_STRING_TYPE *
                                   pSnmpCommunityIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextSnmpCommunityIndex)
{
    tCommunityMappingEntry *pNext = NULL;
    if (pSnmpCommunityIndex->i4_Length >= MAX_STR_OCTET_VAL)
    {
        return SNMP_FAILURE;
    }
    pNext = SNMPGetNextCommunityEntry (pSnmpCommunityIndex);
    if (pNext == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pNextSnmpCommunityIndex, &(pNext->CommIndex));
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetSnmpCommunityName
 Input       :  The Indices
                SnmpCommunityIndex

                The Object 
                retValSnmpCommunityName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpCommunityName (tSNMP_OCTET_STRING_TYPE * pSnmpCommunityIndex,
                         tSNMP_OCTET_STRING_TYPE * pRetValSnmpCommunityName)
{
    tCommunityMappingEntry *pCommunityEntry = NULL;
    pCommunityEntry = SNMPGetCommunityEntry (pSnmpCommunityIndex);
    if (pCommunityEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pRetValSnmpCommunityName,
                         &(pCommunityEntry->CommunityName));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpCommunitySecurityName
 Input       :  The Indices
                SnmpCommunityIndex

                The Object 
                retValSnmpCommunitySecurityName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpCommunitySecurityName (tSNMP_OCTET_STRING_TYPE * pSnmpCommunityIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValSnmpCommunitySecurityName)
{
    tCommunityMappingEntry *pCommunityEntry = NULL;
    pCommunityEntry = SNMPGetCommunityEntry (pSnmpCommunityIndex);
    if (pCommunityEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pRetValSnmpCommunitySecurityName,
                         &(pCommunityEntry->SecurityName));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpCommunityContextEngineID
 Input       :  The Indices
                SnmpCommunityIndex

                The Object 
                retValSnmpCommunityContextEngineID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpCommunityContextEngineID (tSNMP_OCTET_STRING_TYPE *
                                    pSnmpCommunityIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValSnmpCommunityContextEngineID)
{
    tCommunityMappingEntry *pCommunityEntry = NULL;
    tSNMP_OCTET_STRING_TYPE *pContextID;

    pCommunityEntry = SNMPGetCommunityEntry (pSnmpCommunityIndex);
    if (pCommunityEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pContextID = &(pCommunityEntry->ContextEngineID);

    if (pContextID->i4_Length == SNMP_ZERO)
    {
        SNMPCopyOctetString (pRetValSnmpCommunityContextEngineID,
                             &gSnmpEngineID);
    }
    else
    {
        SNMPCopyOctetString (pRetValSnmpCommunityContextEngineID,
                             &(pCommunityEntry->ContextEngineID));
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpCommunityContextName
 Input       :  The Indices
                SnmpCommunityIndex

                The Object 
                retValSnmpCommunityContextName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpCommunityContextName (tSNMP_OCTET_STRING_TYPE * pSnmpCommunityIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValSnmpCommunityContextName)
{
    tCommunityMappingEntry *pCommunityEntry = NULL;
    tSNMP_OCTET_STRING_TYPE *pContextName;

    MEMSET (pRetValSnmpCommunityContextName->pu1_OctetList, 0,
            MAX_STR_OCTET_VAL);
    pRetValSnmpCommunityContextName->i4_Length = 0;

    pCommunityEntry = SNMPGetCommunityEntry (pSnmpCommunityIndex);
    if (pCommunityEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pContextName = &(pCommunityEntry->ContextName);

    if (pContextName->i4_Length != SNMP_ZERO)
    {
        SNMPCopyOctetString (pRetValSnmpCommunityContextName,
                             &(pCommunityEntry->ContextName));
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpCommunityTransportTag
 Input       :  The Indices
                SnmpCommunityIndex

                The Object 
                retValSnmpCommunityTransportTag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpCommunityTransportTag (tSNMP_OCTET_STRING_TYPE * pSnmpCommunityIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValSnmpCommunityTransportTag)
{
    tCommunityMappingEntry *pCommunityEntry = NULL;
    pCommunityEntry = SNMPGetCommunityEntry (pSnmpCommunityIndex);
    if (pCommunityEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pRetValSnmpCommunityTransportTag,
                         &(pCommunityEntry->TransTag));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpCommunityStorageType
 Input       :  The Indices
                SnmpCommunityIndex

                The Object 
                retValSnmpCommunityStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpCommunityStorageType (tSNMP_OCTET_STRING_TYPE * pSnmpCommunityIndex,
                                INT4 *pi4RetValSnmpCommunityStorageType)
{
    tCommunityMappingEntry *pCommunityEntry = NULL;
    pCommunityEntry = SNMPGetCommunityEntry (pSnmpCommunityIndex);
    if (pCommunityEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValSnmpCommunityStorageType = pCommunityEntry->i4Storage;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpCommunityStatus
 Input       :  The Indices
                SnmpCommunityIndex

                The Object 
                retValSnmpCommunityStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpCommunityStatus (tSNMP_OCTET_STRING_TYPE * pSnmpCommunityIndex,
                           INT4 *pi4RetValSnmpCommunityStatus)
{
    tCommunityMappingEntry *pCommunityEntry = NULL;
    pCommunityEntry = SNMPGetCommunityEntry (pSnmpCommunityIndex);
    if (pCommunityEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pCommunityEntry->i4Status == UNDER_CREATION)
    {
        *pi4RetValSnmpCommunityStatus = pCommunityEntry->i4Status;
    }
    if (pCommunityEntry->i4Status == SNMP_ACTIVE)
    {
        *pi4RetValSnmpCommunityStatus = SNMP_ROWSTATUS_ACTIVE;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetSnmpCommunityName
 Input       :  The Indices
                SnmpCommunityIndex

                The Object 
                setValSnmpCommunityName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpCommunityName (tSNMP_OCTET_STRING_TYPE * pSnmpCommunityIndex,
                         tSNMP_OCTET_STRING_TYPE * pSetValSnmpCommunityName)
{
    tCommunityMappingEntry *pCommunityEntry = NULL;
    pCommunityEntry = SNMPGetCommunityEntry (pSnmpCommunityIndex);
    if (pCommunityEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (&(pCommunityEntry->CommunityName),
                         pSetValSnmpCommunityName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpCommunitySecurityName
 Input       :  The Indices
                SnmpCommunityIndex

                The Object 
                setValSnmpCommunitySecurityName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpCommunitySecurityName (tSNMP_OCTET_STRING_TYPE *
                                 pSnmpCommunityIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValSnmpCommunitySecurityName)
{
    tCommunityMappingEntry *pCommunityEntry = NULL;
    pCommunityEntry = SNMPGetCommunityEntry (pSnmpCommunityIndex);
    if (pCommunityEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (&(pCommunityEntry->SecurityName),
                         pSetValSnmpCommunitySecurityName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpCommunityContextEngineID
 Input       :  The Indices
                SnmpCommunityIndex

                The Object 
                setValSnmpCommunityContextEngineID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpCommunityContextEngineID (tSNMP_OCTET_STRING_TYPE *
                                    pSnmpCommunityIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pSetValSnmpCommunityContextEngineID)
{
    tCommunityMappingEntry *pCommunityEntry = NULL;
    pCommunityEntry = SNMPGetCommunityEntry (pSnmpCommunityIndex);
    if (pCommunityEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (&(pCommunityEntry->ContextEngineID),
                         pSetValSnmpCommunityContextEngineID);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpCommunityContextName
 Input       :  The Indices
                SnmpCommunityIndex

                The Object 
                setValSnmpCommunityContextName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpCommunityContextName (tSNMP_OCTET_STRING_TYPE * pSnmpCommunityIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValSnmpCommunityContextName)
{
    tCommunityMappingEntry *pCommunityEntry = NULL;
    pCommunityEntry = SNMPGetCommunityEntry (pSnmpCommunityIndex);
    if (pCommunityEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (&(pCommunityEntry->ContextName),
                         pSetValSnmpCommunityContextName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpCommunityTransportTag
 Input       :  The Indices
                SnmpCommunityIndex

                The Object 
                setValSnmpCommunityTransportTag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpCommunityTransportTag (tSNMP_OCTET_STRING_TYPE *
                                 pSnmpCommunityIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValSnmpCommunityTransportTag)
{
    tCommunityMappingEntry *pCommunityEntry = NULL;
    pCommunityEntry = SNMPGetCommunityEntry (pSnmpCommunityIndex);
    if (pCommunityEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (&(pCommunityEntry->TransTag),
                         pSetValSnmpCommunityTransportTag);

    if (pCommunityEntry->TransTag.i4_Length < SNMP_MAX_OCTETSTRING_SIZE)
    {
        pCommunityEntry->TransTag.pu1_OctetList
            [pCommunityEntry->TransTag.i4_Length] = SNMP_ZERO;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpCommunityStorageType
 Input       :  The Indices
                SnmpCommunityIndex

                The Object 
                setValSnmpCommunityStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpCommunityStorageType (tSNMP_OCTET_STRING_TYPE * pSnmpCommunityIndex,
                                INT4 i4SetValSnmpCommunityStorageType)
{
    tCommunityMappingEntry *pCommunityEntry = NULL;
    pCommunityEntry = SNMPGetCommunityEntry (pSnmpCommunityIndex);
    if (pCommunityEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pCommunityEntry->i4Storage = i4SetValSnmpCommunityStorageType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpCommunityStatus
 Input       :  The Indices
                SnmpCommunityIndex

                The Object 
                setValSnmpCommunityStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpCommunityStatus (tSNMP_OCTET_STRING_TYPE * pSnmpCommunityIndex,
                           INT4 i4SetValSnmpCommunityStatus)
{
    UINT4               u4Return = SNMP_FAILURE;
    tCommunityMappingEntry *pCommunityEntry = NULL;
    tSNMP_OCTET_STRING_TYPE *pContextID;

    switch (i4SetValSnmpCommunityStatus)
    {
        case SNMP_ROWSTATUS_CREATEANDGO:
        case SNMP_ROWSTATUS_CREATEANDWAIT:
        {
            u4Return = (UINT4) SNMPAddCommunityEntry (pSnmpCommunityIndex);
            if (u4Return != SNMP_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            pCommunityEntry = SNMPGetCommunityEntry (pSnmpCommunityIndex);
            if (pCommunityEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            pContextID = &(pCommunityEntry->ContextEngineID);
            if (pContextID->i4_Length == SNMP_ZERO)
            {
                SNMPCopyOctetString (&(pCommunityEntry->ContextEngineID),
                                     &gSnmpEngineID);
            }
            return SNMP_SUCCESS;
        }

        case SNMP_ROWSTATUS_ACTIVE:
        {
            pCommunityEntry = SNMPGetCommunityEntry (pSnmpCommunityIndex);
            if (pCommunityEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            pCommunityEntry->i4Status = SNMP_ROWSTATUS_ACTIVE;
            return SNMP_SUCCESS;

        }
        case SNMP_ROWSTATUS_DESTROY:
        {
            u4Return = (UINT4) SNMPDeleteCommunityEntry (pSnmpCommunityIndex);
            if (u4Return != SNMP_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        }

        case SNMP_ROWSTATUS_NOTINSERVICE:
        {
            pCommunityEntry = SNMPGetCommunityEntry (pSnmpCommunityIndex);
            if (pCommunityEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            if (pCommunityEntry->i4Status == SNMP_ROWSTATUS_ACTIVE)
            {
                pCommunityEntry->i4Status = SNMP_ROWSTATUS_NOTINSERVICE;
                return SNMP_SUCCESS;
            }
        }
        default:
            break;

    }
    return SNMP_FAILURE;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2SnmpCommunityName
 Input       :  The Indices
                SnmpCommunityIndex

                The Object 
                testValSnmpCommunityName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2SnmpCommunityName (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pSnmpCommunityIndex,
                            tSNMP_OCTET_STRING_TYPE * pTestValSnmpCommunityName)
{
    tCommunityMappingEntry *pCommEntry = NULL;

    if (nmhValidateIndexInstanceSnmpCommunityTable (pSnmpCommunityIndex)
        == SNMP_SUCCESS)
    {
        if ((pTestValSnmpCommunityName->i4_Length < SNMP_ZERO) ||
            (pTestValSnmpCommunityName->i4_Length > SNMP_MAX_OCTETSTRING_SIZE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
            return SNMP_FAILURE;
        }

        /* Check entry for given CommunityName. If entry is already present, 
         * CommunityIndex should be eqaul to given SnmpCommunityIndex */
        pCommEntry = SNMPGetCommunityEntryFromName (pTestValSnmpCommunityName);

        if ((pCommEntry != NULL) &&
            (SNMPCompareOctetString (pSnmpCommunityIndex,
                                     &(pCommEntry->CommIndex)) != SNMP_EQUAL))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2SnmpCommunitySecurityName
 Input       :  The Indices
                SnmpCommunityIndex

                The Object 
                testValSnmpCommunitySecurityName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpCommunitySecurityName (UINT4 *pu4ErrorCode,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pSnmpCommunityIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValSnmpCommunitySecurityName)
{
    if (nmhValidateIndexInstanceSnmpCommunityTable (pSnmpCommunityIndex)
        == SNMP_SUCCESS)
    {
        if ((pTestValSnmpCommunitySecurityName->i4_Length <= SNMP_ZERO) ||
            (pTestValSnmpCommunitySecurityName->i4_Length >
             MAX_COMM_SEC_NAME_LEN))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2SnmpCommunityContextEngineID
 Input       :  The Indices
                SnmpCommunityIndex

                The Object 
                testValSnmpCommunityContextEngineID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#define SNMP_MIN_CONTEXT_ENGINEID 5
#define SNMP_MAX_CONTEXT_ENGINEID 32
INT1
nmhTestv2SnmpCommunityContextEngineID (UINT4 *pu4ErrorCode,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pSnmpCommunityIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pTestValSnmpCommunityContextEngineID)
{
    if (nmhValidateIndexInstanceSnmpCommunityTable (pSnmpCommunityIndex)
        == SNMP_SUCCESS)
    {
        if ((pTestValSnmpCommunityContextEngineID->i4_Length <
             SNMP_MIN_CONTEXT_ENGINEID) ||
            (pTestValSnmpCommunityContextEngineID->i4_Length >
             SNMP_MAX_CONTEXT_ENGINEID))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2SnmpCommunityContextName
 Input       :  The Indices
                SnmpCommunityIndex

                The Object 
                testValSnmpCommunityContextName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpCommunityContextName (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pSnmpCommunityIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValSnmpCommunityContextName)
{
    if (nmhValidateIndexInstanceSnmpCommunityTable (pSnmpCommunityIndex)
        == SNMP_SUCCESS)
    {
        if ((pTestValSnmpCommunityContextName->i4_Length < SNMP_ZERO) ||
            (pTestValSnmpCommunityContextName->i4_Length >
             SNMP_MAX_CONTEXT_NAME))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

}

#define SNMP_MIN_TRANSTAG_STRING 0
#define SNMP_MAX_TRANSTAG_STRING 255
/****************************************************************************
 Function    :  nmhTestv2SnmpCommunityTransportTag
 Input       :  The Indices
                SnmpCommunityIndex

                The Object 
                testValSnmpCommunityTransportTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2SnmpCommunityTransportTag (UINT4 *pu4ErrorCode,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pSnmpCommunityIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValSnmpCommunityTransportTag)
{
    if (nmhValidateIndexInstanceSnmpCommunityTable (pSnmpCommunityIndex) ==
        SNMP_SUCCESS)
    {
        if ((pTestValSnmpCommunityTransportTag->i4_Length <
             SNMP_MIN_TRANSTAG_STRING) ||
            (pTestValSnmpCommunityTransportTag->i4_Length >
             SNMP_MAX_TRANSTAG_STRING))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2SnmpCommunityStorageType
 Input       :  The Indices
                SnmpCommunityIndex

                The Object 
                testValSnmpCommunityStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpCommunityStorageType (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pSnmpCommunityIndex,
                                   INT4 i4TestValSnmpCommunityStorageType)
{
    if (nmhValidateIndexInstanceSnmpCommunityTable (pSnmpCommunityIndex)
        == SNMP_SUCCESS)
    {
        if ((i4TestValSnmpCommunityStorageType < SNMP3_STORAGE_TYPE_OTHER) ||
            (i4TestValSnmpCommunityStorageType > SNMP3_STORAGE_TYPE_READONLY))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        if ((i4TestValSnmpCommunityStorageType != SNMP3_STORAGE_TYPE_VOLATILE)
            && (i4TestValSnmpCommunityStorageType !=
                SNMP3_STORAGE_TYPE_NONVOLATILE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;

    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2SnmpCommunityStatus
 Input       :  The Indices
                SnmpCommunityIndex

                The Object 
                testValSnmpCommunityStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpCommunityStatus (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE * pSnmpCommunityIndex,
                              INT4 i4TestValSnmpCommunityStatus)
{

    /* If Community Control is set, Communities cannot be created */
    if (((SNMP_ROWSTATUS_CREATEANDGO == i4TestValSnmpCommunityStatus) ||
         (SNMP_ROWSTATUS_CREATEANDWAIT == i4TestValSnmpCommunityStatus)) &&
        (SNMP_TRUE == gi4SnmpCommunityCtrl))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValSnmpCommunityStatus == SNMP_ROWSTATUS_CREATEANDGO) ||
        (i4TestValSnmpCommunityStatus == SNMP_ROWSTATUS_NOTREADY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValSnmpCommunityStatus < SNMP_ROWSTATUS_ACTIVE) ||
        (i4TestValSnmpCommunityStatus > SNMP_ROWSTATUS_DESTROY))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (nmhValidateIndexInstanceSnmpCommunityTable (pSnmpCommunityIndex)
        == SNMP_SUCCESS)
    {
        if (i4TestValSnmpCommunityStatus == SNMP_ROWSTATUS_CREATEANDWAIT)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        }
        else
        {
            return SNMP_SUCCESS;
        }
    }
    else
    {
        if (i4TestValSnmpCommunityStatus != SNMP_ROWSTATUS_CREATEANDWAIT)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        else
        {
            return SNMP_SUCCESS;
        }
    }
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2SnmpCommunityTable
 Input       :  The Indices
                SnmpCommunityIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SnmpCommunityTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : SnmpTargetAddrExtTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceSnmpTargetAddrExtTable
 Input       :  The Indices
                SnmpTargetAddrName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceSnmpTargetAddrExtTable (tSNMP_OCTET_STRING_TYPE *
                                                pSnmpTargetAddrName)
{
    UNUSED_PARAM (pSnmpTargetAddrName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexSnmpTargetAddrExtTable
 Input       :  The Indices
                SnmpTargetAddrName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexSnmpTargetAddrExtTable (tSNMP_OCTET_STRING_TYPE *
                                        pSnmpTargetAddrName)
{
    UNUSED_PARAM (pSnmpTargetAddrName);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexSnmpTargetAddrExtTable
 Input       :  The Indices
                SnmpTargetAddrName
                nextSnmpTargetAddrName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexSnmpTargetAddrExtTable (tSNMP_OCTET_STRING_TYPE *
                                       pSnmpTargetAddrName,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextSnmpTargetAddrName)
{
    UNUSED_PARAM (pSnmpTargetAddrName);
    UNUSED_PARAM (pNextSnmpTargetAddrName);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetSnmpTargetAddrTMask
 Input       :  The Indices
                SnmpTargetAddrName

                The Object 
                retValSnmpTargetAddrTMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpTargetAddrTMask (tSNMP_OCTET_STRING_TYPE * pSnmpTargetAddrName,
                           tSNMP_OCTET_STRING_TYPE * pRetValSnmpTargetAddrTMask)
{
    UNUSED_PARAM (pSnmpTargetAddrName);
    UNUSED_PARAM (pRetValSnmpTargetAddrTMask);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpTargetAddrMMS
 Input       :  The Indices
                SnmpTargetAddrName

                The Object 
                retValSnmpTargetAddrMMS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpTargetAddrMMS (tSNMP_OCTET_STRING_TYPE * pSnmpTargetAddrName,
                         INT4 *pi4RetValSnmpTargetAddrMMS)
{
    UNUSED_PARAM (pSnmpTargetAddrName);
    UNUSED_PARAM (pi4RetValSnmpTargetAddrMMS);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetSnmpTargetAddrTMask
 Input       :  The Indices
                SnmpTargetAddrName

                The Object 
                setValSnmpTargetAddrTMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpTargetAddrTMask (tSNMP_OCTET_STRING_TYPE * pSnmpTargetAddrName,
                           tSNMP_OCTET_STRING_TYPE * pSetValSnmpTargetAddrTMask)
{
    UNUSED_PARAM (pSnmpTargetAddrName);
    UNUSED_PARAM (pSetValSnmpTargetAddrTMask);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpTargetAddrMMS
 Input       :  The Indices
                SnmpTargetAddrName

                The Object 
                setValSnmpTargetAddrMMS
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpTargetAddrMMS (tSNMP_OCTET_STRING_TYPE * pSnmpTargetAddrName,
                         INT4 i4SetValSnmpTargetAddrMMS)
{
    UNUSED_PARAM (pSnmpTargetAddrName);
    UNUSED_PARAM (i4SetValSnmpTargetAddrMMS);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2SnmpTargetAddrTMask
 Input       :  The Indices
                SnmpTargetAddrName

                The Object 
                testValSnmpTargetAddrTMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpTargetAddrTMask (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE * pSnmpTargetAddrName,
                              tSNMP_OCTET_STRING_TYPE *
                              pTestValSnmpTargetAddrTMask)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpTargetAddrName);
    UNUSED_PARAM (pTestValSnmpTargetAddrTMask);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpTargetAddrMMS
 Input       :  The Indices
                SnmpTargetAddrName

                The Object 
                testValSnmpTargetAddrMMS
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpTargetAddrMMS (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pSnmpTargetAddrName,
                            INT4 i4TestValSnmpTargetAddrMMS)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpTargetAddrName);
    UNUSED_PARAM (i4TestValSnmpTargetAddrMMS);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2SnmpTargetAddrExtTable
 Input       :  The Indices
                SnmpTargetAddrName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SnmpTargetAddrExtTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
