
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: snmpcfg.c,v 1.28 2017/11/20 13:11:26 siva Exp $
 *
 *
 * Description:Routines for Snmp Agent configuration Module 
 *******************************************************************/
#include "snmpcmn.h"
#include "snmputil.h"
#include "snmcomwr.h"
#include "snmcomlw.h"
#include "stdvacwr.h"
#include "stdvaclw.h"
#include "sntargwr.h"
#include "sntarglw.h"
#include "snmpnowr.h"
#include "snmpnolw.h"
#include "snmusmwr.h"
#include "snmusmlw.h"
#include "fssnmp3wr.h"
#include "fssnmp3lw.h"
#include "snmp3cli.h"
#include "snmproxwr.h"
#include "snmproxlw.h"
#include "stdvaccli.h"
#include "stdsnpcli.h"
#include "stdsnccli.h"
#include "stdsnncli.h"
#include "stdsntcli.h"
#include "fssnmpcli.h"
#include "snmpmbdb.h"
#include "snmmpdwr.h"
#include "fips.h"

tSNMP_SYSTEM        gSnmpSystem;
tSNMP_SYSORTABLE    gaSnmpSysorTable[MAX_SYSOR_TABLE_ENTRY];
tSNMP_STAT          gSnmpStat;
tOsixSemId          gu4InformSemId;

tSNMP_DEFAULT_TABLE gaSnmpCommunityDefault[] = {
    {SNMP_DEF_COMMUNITY_1}
    ,
    {SNMP_DEF_COMMUNITY_2}
    ,
    {NULL}
};

tSNMP_DEFAULT_TABLE gaSnmpNotifyDefault[] = {
    {SNMP_DEF_NOTIFY_1}
    ,
    {SNMP_DEF_NOTIFY_2}
    ,
    {NULL}
};

tSNMP_DEFAULT_TABLE gaSnmpTgtParamDefault[] = {
    {SNMP_DEF_TGT_PARAM_1}
    ,
    {SNMP_DEF_TGT_PARAM_2}
    ,
    {NULL}
};

tSNMP_DEFAULT_TABLE gaSnmpViewDefault[] = {
    {SNMP_DEF_VIEW_1}
    ,
    {SNMP_DEF_VIEW_2}
    ,
    {NULL}
};

tSNMP_DEFAULT_TABLE gaSnmpGrpDefault[] = {
    {SNMP_DEF_GROUP_1}
    ,
    {SNMP_DEF_GROUP_2}
    ,
    {NULL}
};

tSNMP_DEFAULT_TABLE gaSnmpUserDefault[] = {
    {SNMP_DEF_USER_1}
    ,
    {SNMP_DEF_USER_2}
    ,
    {SNMP_DEF_USER_3}
    ,
    {NULL}
};

UINT4
     
     
     
     
     
     
      gaau4TrapOidList[SNMP_MAX_LAST_TIME_CHANGE_TRAPS][SNMP_MAX_OID_LENGTH] = {
    {1, 3, 6, 1, 4, 1, 2076, 81, 100, 6, 2},
    {1, 3, 6, 1, 4, 1, 2076, 81, 100, 6, 3},
    {1, 3, 6, 1, 4, 1, 2076, 27, 4, 0, 4},
    {1, 3, 6, 1, 4, 1, 2076, 27, 4, 0, 5},
    {1, 3, 6, 1, 4, 1, 2076, 93, 2, 1},
    {1, 3, 6, 1, 4, 1, 2076, 93, 2, 2},
    {1, 3, 6, 1, 4, 1, 2076, 112, 20},
    {1, 3, 6, 1, 4, 1, 2076, 112, 20}
};

UINT1               gau1TrapType[SNMP_MAX_LAST_TIME_CHANGE_TRAPS] =
    { 2, 3, 1, 2, 1, 2, 2, 3 };

tTimerListId        SnmpAgtTimerListId;
tTimerListId        SnmpPxyTimerListId;
extern tInformMsgList gInformMsgList;
tSnmpEntGlobalInfo  gSnmpEntGlobalInfo;
tOsixSemId          gu4SnmpDbSemId;

/************************************************************************
 *  Function Name   : SNMPInitSystemObjects 
 *  Description     : Fucntion to init snmp system objects
 *  Input           : None
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
SNMPInitSystemObjects ()
{
    UINT4               au4SysObjectId[] = { 1, 3, 6, 1, 2, 1 };

    STRCPY (gSnmpSystem.au1SysDescr, SNMP_SYS_DESCR);
    MEMCPY (gSnmpSystem.au4SysObjectId, au4SysObjectId,
            sizeof (au4SysObjectId));
    gSnmpSystem.u4OidLen = SNMP_SYS_OID_LEN;
    GET_TIME_TICKS (&gSnmpSystem.u4SysUpTime);
    STRCPY (gSnmpSystem.au1SysContact, SNMP_SYS_CONTACT);
    STRCPY (gSnmpSystem.au1SysName, SNMP_SYS_NAME);
    STRCPY (gSnmpSystem.au1SysLocation, SNMP_SYS_LOCATION);
    gSnmpSystem.u4SysServices = 0x48;    /* Refer RFC 1907 */
    gSnmpSystem.u2ListenTrapPort = SNMP_MANAGER_PORT;
    gSnmpSystem.u2ProxyListenTrapPort = SNMP_MANAGER_PORT;
    gSnmpSystem.u2ListenAgentPort = SNMP_PORT;

    gSnmpDefaultEngineID.pu1_OctetList = gau1SnmpEngineID;
    gSnmpDefaultEngineID.i4_Length = SNMP_DEFAULT_ENGINEID_LEN;

    /* Init Statistics Objects */
    MEMSET (&gSnmpStat, SNMP_ZERO, sizeof (gSnmpStat));
    gSnmpStat.i4SnmpEnableAuthTraps = AUTHTRAP_DISABLE;
    gSnmpStat.i4SnmpColdStartTraps = COLDTRAP_ENABLE;

    /* Initialize global inform message list */
    TMO_SLL_Init (&gInformMsgList);

    /* Create timer list for Inform timer */
    if (TmrCreateTimerList
        ((const UINT1 *) SNMP_TASK_NAME, SNMP_AGT_TMR_EXP_EVT, NULL,
         &SnmpAgtTimerListId) != TMR_SUCCESS)
    {
        SNMPTrace ("Creation of 1 sec TimerList for SNMP Agent failed \n");
        return;
    }

    if (TmrCreateTimerList
        ((const UINT1 *) SNMP_TASK_NAME, SNMP_PXY_TMR_EXP_EVT, NULL,
         &SnmpPxyTimerListId) != TMR_SUCCESS)
    {
        SNMPTrace ("Creation of TimerList for SNMP Proxy failed \n");
        return;
    }
    gSnmpEntGlobalInfo.b1TimeChangeFlag = SNMP_FALSE;
    if (TmrCreateTimerList
        ((const UINT1 *) SNMP_TASK_NAME, SNMP_THROTTLE_TMR_EXP_EVT, NULL,
         &gSnmpEntGlobalInfo.ThrottlingTmrListId) != TMR_SUCCESS)
    {
        SNMPTrace
            ("Creation of 5 sec TimerList for Throttling Timer failed \n");
        return;
    }
    /* Init Snmp Usm Table */
    SNMPInitUsmTable ();

    /* Init VACM Tables */
    VACMInitSecGrp ();
    VACMInitAccess ();
    VACMContextInit ();
    VACMInitViewTree ();

    /* Community Table Init */
    SnmpInitCommunityTable ();

    /* Notify Table Init */
    SNMPInitNotifyTable ();
    SNMPInitNotifyFilterProfileTable ();
    SNMPInitNotifyFilterTable ();

    /* Target Tables Init */
    SNMPTgtParamTableInit ();
    SNMPTgtAddrTableInit ();

    /* Proxy Table Init */
    SnmpInitProxyTable ();

    /* Proxy Table Init */
    SnmpInitPrpProxyTable ();

    /*To initialize the cache data */
    SnmpInitProxyCache ();

    /* If the mode is secure CNSA/FIPS then intialzie only the SNMP V3
     * users alone and return without performing  other Init.*/
    if (SnmpIsModeSecure () == SNMP_SUCCESS)
    {
        SnmpV3DefSecureUsers ();
        /* snmp trap filter table init */
        SNMPInitTrapFilterTable ();
        return;
    }
    /* If SNMP Default update is required, then SNMPV3DefaultInit() will
     * be called. If customised way of default initialisation is required, then based
     * on this flag, basic default initialisation will be skipped here.
     */
    if (SnmpApiGetSNMPUpdateDefault () == OSIX_TRUE)
    {
        SNMPV3DefaultInit ();
    }

    /* snmp v3  default Group config */
    Snmp3DefGroupConfig ();

    /* snmp v3  default Access config */
    Snmp3DefAccessConfig ();

    /* snmp v3  default Community config */
    Snmp3DefCommunityConfig ();

    /* snmp v3  default Notify config */
    Snmp3DefNotifyConfig ();

    /* snmp v3  default Target Params config */
    Snmp3DefTargetParamConfig ();

    /* snmp trap filter table init */
    SNMPInitTrapFilterTable ();

    /* snmp v3  addl default config's */
    Snmp3Def1GroupConfig ();
    Snmp3Def1AccessConfig ();
    Snmp3Def1CommunityConfig ();
    Snmp3Def1TargetParamConfig ();
}

/************************************************************************
 *  Function Name   : SNMPRegisterSNMPV3MIBS 
 *  Description     : Function to register SNMP MIBs with SNMP Agent  
 *  Input           : None 
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
SNMPRegisterSNMPV3MIBS ()
{
    RegisterSNMUSM ();            /* SNMP USM MIB */
    RegisterSTDVAC ();            /* SNMP VACM MIB */
    RegisterSNMPNO ();            /* SNMP NOTIFY MIB */
    RegisterSNTARG ();            /* SNMP TARGET MIB */
    RegisterSNMMPD ();            /* SNMP FRAMEWORK AND MPD MIB */
    RegisterSNMPROX ();            /* SNMP PROXY MIB */
}

/************************************************************************
 *  Function Name   : RegisterFUTURESNMP 
 *  Description     : This function maps the old v2 community MIB register
 *                    function to the new community Mapping MIB  
 *  Input           : None 
 *  Output          : None
 *  Returns         : None
 ************************************************************************/

VOID
RegisterFUTURESNMP ()
{
    RegisterSNMCOM ();            /* SNMP COMMUNITY MAPPING MIB */
    RegisterFSSNMP3 ();            /* SNMP Inform stats counters MIB */
}

/************************************************************************
 *  Function Name   : SNMPV3DefaultInit 
 *  Description     : Function to init v3 tables default entries  
 *  Input           : None 
 *  Output          : None
 *  Returns         : None
 ************************************************************************/

VOID
SNMPV3DefaultInit ()
{
    tSNMP_OCTET_STRING_TYPE UserName;
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    UINT1               au1UserName[SNMP_MAX_ADMIN_STR_LENGTH];

    INT4                i4Return = 0;
    tAccessEntry       *pAccEntry = NULL;
    tViewTree          *pViewEntry = NULL;
    tSecGrp            *pSecEntry = NULL;
    UINT1               au1GrpName[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1AccContext[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1AccName[SNMP_MAX_ADMIN_STR_LENGTH];

    tSNMP_OCTET_STRING_TYPE GrpName;
    tSNMP_OCTET_STRING_TYPE AccContext;
    tSNMP_OCTET_STRING_TYPE AccName;
    tSNMP_OID_TYPE      SubTree;
    INT4                i4SecLevel = SNMP_MPMODEL_V2C;
#ifdef  ISS_WANTED
    INT1               *pi1EngineID = NULL;
#endif
    UserName.pu1_OctetList = au1UserName;

#ifdef ISS_WANTED
    pi1EngineID = IssGetSnmpEngineID ();
    gSnmpEngineID.pu1_OctetList = gau1LocalSnmpEngineID;
    if (SnmpConvertStringToOctet (pi1EngineID, &gSnmpEngineID) == SNMP_FAILURE
        || ((gSnmpEngineID.i4_Length < SNMP_MIN_ENGINE_ID_LEN)
            || (gSnmpEngineID.i4_Length > SNMP_MAX_ENGINE_ID_LEN)))
    {
        MEMCPY (gSnmpEngineID.pu1_OctetList, gau1SnmpEngineID,
                SNMP_DEFAULT_ENGINEID_LEN);
        gSnmpEngineID.i4_Length = SNMP_DEFAULT_ENGINEID_LEN;
        PRINTF ("Error in the engineID given\n");
        IssSetSnmpEngineIDToNvRam (NULL);
    }

    /* Updating SNMP Engine Boot Count */
    gSnmpSystem.u4SnmpBootCount = IssGetSnmpEngineBoots ();

#else

    gSnmpEngineID.pu1_OctetList = gau1SnmpEngineID;
    gSnmpEngineID.i4_Length = SNMP_DEFAULT_ENGINEID_LEN;

    gSnmpSystem.u4SnmpBootCount = gu4SnmpEngineBootCount;
#endif

#ifndef EXT_CRYPTO_WANTED
    /* SNMP_DEF_USER_1  */
    UserName.i4_Length = (INT4) STRLEN (SNMP_DEF_USER_1);
    MEMCPY (UserName.pu1_OctetList, SNMP_DEF_USER_1, UserName.i4_Length);
    pSnmpUsmEntry = SNMPCreateUsmEntry (&gSnmpEngineID, &UserName);
    if (pSnmpUsmEntry == NULL)
    {
        SNMPTrace ("Unable to Create Snmp Usm Default Entry\n");
        return;
    }
    pSnmpUsmEntry->u4UsmUserStatus = SNMP_ACTIVE;

    /* SNMP_DEF_USER_2  */
    UserName.i4_Length = (INT4) STRLEN (SNMP_DEF_USER_2);
    MEMCPY (UserName.pu1_OctetList, SNMP_DEF_USER_2, UserName.i4_Length);
    pSnmpUsmEntry = SNMPCreateUsmEntry (&gSnmpEngineID, &UserName);
    if (pSnmpUsmEntry == NULL)
    {
        SNMPTrace ("Unable to Create Snmp Usm Default Entry\n");
        return;
    }
    pSnmpUsmEntry->u4UsmUserAuthProtocol = SNMP_MD5;
    SNMPCopyOctetString (&(pSnmpUsmEntry->UsmUserAuthPassword), &UserName);
    SNMPKeyGenerator (UserName.pu1_OctetList,
                      UserName.i4_Length,
                      pSnmpUsmEntry->u4UsmUserAuthProtocol,
                      &(pSnmpUsmEntry->UsmUserEngineID),
                      &(pSnmpUsmEntry->UsmUserAuthKeyChange));
    pSnmpUsmEntry->u4UsmUserStatus = SNMP_ACTIVE;
#endif

    /* SNMP_DEF_USER_3  */
    UserName.i4_Length = (INT4) STRLEN (SNMP_DEF_USER_3);
    MEMCPY (UserName.pu1_OctetList, SNMP_DEF_USER_3, UserName.i4_Length);
    pSnmpUsmEntry = SNMPCreateUsmEntry (&gSnmpEngineID, &UserName);
    if (pSnmpUsmEntry == NULL)
    {
        SNMPTrace ("Unable to Create Snmp Usm Default Entry\n");
        return;
    }
    pSnmpUsmEntry->u4UsmUserAuthProtocol = SNMP_SHA;
    pSnmpUsmEntry->u4UsmUserPrivProtocol = SNMP_AESCFB128;
    SNMPCopyOctetString (&(pSnmpUsmEntry->UsmUserAuthPassword), &UserName);
    SNMPCopyOctetString (&(pSnmpUsmEntry->UsmUserPrivPassword), &UserName);
    SNMPKeyGenerator (UserName.pu1_OctetList,
                      UserName.i4_Length,
                      pSnmpUsmEntry->u4UsmUserAuthProtocol,
                      &(pSnmpUsmEntry->UsmUserEngineID),
                      &(pSnmpUsmEntry->UsmUserAuthKeyChange));
    SNMPKeyGenerator (UserName.pu1_OctetList,
                      UserName.i4_Length,
                      pSnmpUsmEntry->u4UsmUserAuthProtocol,
                      &(pSnmpUsmEntry->UsmUserEngineID),
                      &(pSnmpUsmEntry->UsmUserPrivKeyChange));
    pSnmpUsmEntry->u4UsmUserStatus = SNMP_ACTIVE;

    /* VACM Entries */
    GrpName.pu1_OctetList = au1GrpName;
    AccContext.pu1_OctetList = au1AccContext;
    MEMCPY (AccContext.pu1_OctetList, "\0", 1);
    AccContext.i4_Length = 0;
    AccName.pu1_OctetList = au1AccName;
    SubTree.pu4_OidList = (UINT4 *) MemAllocMemBlk (gSnmpOidListPoolId);
    if (SubTree.pu4_OidList == NULL)
    {
        return;
    }
    MEMSET (SubTree.pu4_OidList, 0, MAX_OID_LENGTH);

    /* VACM Security Table Entry SNMP_DEF_USER_1 */
    GrpName.i4_Length = (INT4) STRLEN (SNMP_DEF_USER_1);
    MEMCPY (GrpName.pu1_OctetList, SNMP_DEF_USER_1, GrpName.i4_Length);
    i4Return = VACMAddSecGrp (SNMP_USM, &GrpName);
    if (i4Return == SNMP_FAILURE)
    {
        MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *) SubTree.pu4_OidList);
        return;
    }
    pSecEntry = VACMGetSecGrp (SNMP_USM, &GrpName);
    if (pSecEntry == NULL)
    {
        MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *) SubTree.pu4_OidList);
        return;
    }
    else
    {
        pSecEntry->i4Status = SNMP_ROWSTATUS_ACTIVE;
        SNMPCopyOctetString (&(pSecEntry->Group), &(GrpName));
    }

    /* VACM Security Table entry SNMP_DEF_USER_2 */
    GrpName.i4_Length = (INT4) STRLEN (SNMP_DEF_USER_2);
    MEMCPY (GrpName.pu1_OctetList, SNMP_DEF_USER_2, GrpName.i4_Length);
    i4Return = VACMAddSecGrp (SNMP_USM, &GrpName);
    if (i4Return == SNMP_FAILURE)
    {
        MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *) SubTree.pu4_OidList);
        return;
    }
    pSecEntry = VACMGetSecGrp (SNMP_USM, &GrpName);
    if (pSecEntry == NULL)
    {
        MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *) SubTree.pu4_OidList);
        return;
    }
    else
    {
        pSecEntry->i4Status = SNMP_ROWSTATUS_ACTIVE;
        GrpName.i4_Length = (INT4) STRLEN (SNMP_DEF_GROUP_2);
        MEMCPY (GrpName.pu1_OctetList, SNMP_DEF_GROUP_2, GrpName.i4_Length);
        SNMPCopyOctetString (&(pSecEntry->Group), &(GrpName));
    }

    /* VACM Security Table entry SNMP_DEF_USER_3 */
    GrpName.i4_Length = (INT4) STRLEN (SNMP_DEF_USER_3);
    MEMCPY (GrpName.pu1_OctetList, SNMP_DEF_USER_3, GrpName.i4_Length);
    i4Return = VACMAddSecGrp (SNMP_USM, &GrpName);
    if (i4Return == SNMP_FAILURE)
    {
        MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *) SubTree.pu4_OidList);
        return;
    }
    pSecEntry = VACMGetSecGrp (SNMP_USM, &GrpName);
    if (pSecEntry == NULL)
    {
        MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *) SubTree.pu4_OidList);
        return;
    }
    else
    {
        pSecEntry->i4Status = SNMP_ROWSTATUS_ACTIVE;
        GrpName.i4_Length = (INT4) STRLEN (SNMP_DEF_GROUP_2);
        MEMCPY (GrpName.pu1_OctetList, SNMP_DEF_GROUP_2, GrpName.i4_Length);
        SNMPCopyOctetString (&(pSecEntry->Group), &(GrpName));
    }

    /* VACM Access Table noAtuhNoPriv */
    i4SecLevel = SNMP_NOAUTH_NOPRIV;
    GrpName.i4_Length = (INT4) STRLEN (SNMP_DEF_GROUP_2);
    MEMCPY (GrpName.pu1_OctetList, SNMP_DEF_GROUP_2, GrpName.i4_Length);
    AccName.i4_Length = (INT4) STRLEN (SNMP_DEF_VIEW_2);
    MEMCPY (AccName.pu1_OctetList, SNMP_DEF_VIEW_2, AccName.i4_Length);
    i4Return = VACMAddAccess (&GrpName, &AccContext, SNMP_USM, i4SecLevel);
    if (i4Return == SNMP_FAILURE)
    {
        MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *) SubTree.pu4_OidList);
        return;
    }
    pAccEntry = VACMGetAccess (&GrpName, &AccContext, SNMP_USM, i4SecLevel);
    if (pAccEntry == NULL)
    {
        MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *) SubTree.pu4_OidList);
        return;
    }
    else
    {
        SNMPCopyOctetString (&(pAccEntry->ReadName), &AccName);
        SNMPCopyOctetString (&(pAccEntry->WriteName), &AccName);
        SNMPCopyOctetString (&(pAccEntry->NotifyName), &AccName);
        pAccEntry->i4Status = SNMP_ROWSTATUS_ACTIVE;
    }

    /* VACM Access Table AtuhNoPriv */
    i4SecLevel = SNMP_AUTH_NOPRIV;
    AccName.i4_Length = (INT4) STRLEN (SNMP_DEF_ACCESS_1);
    MEMCPY (AccName.pu1_OctetList, SNMP_DEF_ACCESS_1, AccName.i4_Length);
    GrpName.i4_Length = (INT4) STRLEN (SNMP_DEF_GROUP_2);
    MEMCPY (GrpName.pu1_OctetList, SNMP_DEF_GROUP_2, GrpName.i4_Length);
    i4Return = VACMAddAccess (&GrpName, &AccContext, SNMP_USM, i4SecLevel);
    if (i4Return == SNMP_FAILURE)
    {
        MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *) SubTree.pu4_OidList);
        return;
    }
    pAccEntry = VACMGetAccess (&GrpName, &AccContext, SNMP_USM, i4SecLevel);
    if (pAccEntry == NULL)
    {
        MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *) SubTree.pu4_OidList);
        return;
    }
    else
    {
        SNMPCopyOctetString (&(pAccEntry->ReadName), &AccName);
        SNMPCopyOctetString (&(pAccEntry->WriteName), &AccName);
        SNMPCopyOctetString (&(pAccEntry->NotifyName), &AccName);
        pAccEntry->i4Status = SNMP_ROWSTATUS_ACTIVE;
    }

    /* VACM Access Table AtuhPriv */
    i4SecLevel = SNMP_AUTH_PRIV;
    AccName.i4_Length = (INT4) STRLEN (SNMP_DEF_ACCESS_1);
    MEMCPY (AccName.pu1_OctetList, SNMP_DEF_ACCESS_1, AccName.i4_Length);
    GrpName.i4_Length = (INT4) STRLEN (SNMP_DEF_GROUP_2);
    MEMCPY (GrpName.pu1_OctetList, SNMP_DEF_GROUP_2, GrpName.i4_Length);
    i4Return = VACMAddAccess (&GrpName, &AccContext, SNMP_USM, i4SecLevel);
    if (i4Return == SNMP_FAILURE)
    {
        MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *) SubTree.pu4_OidList);
        return;
    }
    pAccEntry = VACMGetAccess (&GrpName, &AccContext, SNMP_USM, i4SecLevel);
    if (pAccEntry == NULL)
    {
        MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *) SubTree.pu4_OidList);
        return;
    }
    else
    {
        SNMPCopyOctetString (&(pAccEntry->ReadName), &AccName);
        SNMPCopyOctetString (&(pAccEntry->WriteName), &AccName);
        SNMPCopyOctetString (&(pAccEntry->NotifyName), &AccName);
        pAccEntry->i4Status = SNMP_ROWSTATUS_ACTIVE;
    }

    /* VACM View Tree Table  SNMP_DEF_ACCESS_1 */
    AccName.i4_Length = (INT4) STRLEN (SNMP_DEF_ACCESS_1);
    MEMCPY (AccName.pu1_OctetList, SNMP_DEF_ACCESS_1, AccName.i4_Length);
    SubTree.pu4_OidList[0] = 1;
    SubTree.u4_Length = 1;
    i4Return = VACMViewTreeAdd (&AccName, &SubTree);
    if (i4Return == SNMP_FAILURE)
    {
        MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *) SubTree.pu4_OidList);
        return;
    }
    pViewEntry = VACMViewTreeGet (&AccName, &SubTree);
    if (pViewEntry == NULL)
    {
        MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *) SubTree.pu4_OidList);
        return;
    }
    else
    {
        pViewEntry->i4Status = SNMP_ROWSTATUS_ACTIVE;
    }

    /* VACM View Tree Table  SNMP_DEF_VIEW_2 */
    AccName.i4_Length = (INT4) STRLEN (SNMP_DEF_VIEW_2);
    MEMCPY (AccName.pu1_OctetList, SNMP_DEF_VIEW_2, AccName.i4_Length);
    SubTree.pu4_OidList[0] = 1;
    SubTree.u4_Length = 1;
    i4Return = VACMViewTreeAdd (&AccName, &SubTree);
    if (i4Return == SNMP_FAILURE)
    {
        MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *) SubTree.pu4_OidList);
        return;
    }
    pViewEntry = VACMViewTreeGet (&AccName, &SubTree);
    if (pViewEntry == NULL)
    {
        MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *) SubTree.pu4_OidList);
        return;
    }
    else
    {
        pViewEntry->i4Status = SNMP_ROWSTATUS_ACTIVE;
    }
    AccName.i4_Length = (INT4) STRLEN ("default");
    MEMCPY (AccName.pu1_OctetList, "default", AccName.i4_Length);
    VACMAddContext (&AccName);
    MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *) SubTree.pu4_OidList);
}

/************************************************************************
 *  Function Name   : SNMPAddSysorEntry 
 *  Description     : Function add a protocol to sysor table
 *  Input           : pMibOid - Protocol Oid pointer
 *                    pu1MibName - Protocol Name Pointer 
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
SNMPAddSysorEntry (tSNMP_OID_TYPE * pMibOid, const UINT1 *pu1MibName)
{
    UINT4               u4Index = SNMP_ZERO;
    for (u4Index = SNMP_ZERO; u4Index < MAX_SYSOR_TABLE_ENTRY; u4Index++)
    {
        if (gaSnmpSysorTable[u4Index].i4Status != SNMP_ACTIVE)
        {
            break;
        }
    }
    if (u4Index < MAX_SYSOR_TABLE_ENTRY)
    {
        MEMSET (&gaSnmpSysorTable[u4Index], SNMP_ZERO,
                sizeof (tSNMP_SYSORTABLE));
        gaSnmpSysorTable[u4Index].i4Status = SNMP_ACTIVE;
        STRNCPY (gaSnmpSysorTable[u4Index].au1SysOrDescr, pu1MibName,
                 sizeof (gaSnmpSysorTable[u4Index].au1SysOrDescr) - SNMP_ONE);
        MEMCPY (gaSnmpSysorTable[u4Index].au4SysOrId, pMibOid->pu4_OidList,
                (pMibOid->u4_Length * sizeof (UINT4)));
        gaSnmpSysorTable[u4Index].u4OidLen = pMibOid->u4_Length;
        GET_TIME_TICKS (&gaSnmpSysorTable[u4Index].u4SysOrUpTime);
    }
}

/************************************************************************
 *  Function Name   : SNMPDelSysorEntry 
 *  Description     : Function Delete a protocol from sysor table
 *  Input           : pMibOid - Protocol Oid pointer
 *                    pu1MibName - Protocol Name Pointer 
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
SNMPDelSysorEntry (tSNMP_OID_TYPE * pMibOid, const UINT1 *pu1MibName)
{
    UINT4               u4Index = SNMP_ZERO;
    for (u4Index = SNMP_ZERO; u4Index < MAX_SYSOR_TABLE_ENTRY; u4Index++)
    {
        if ((gaSnmpSysorTable[u4Index].i4Status == SNMP_ACTIVE) &&
            (STRCMP (gaSnmpSysorTable[u4Index].au1SysOrDescr, pu1MibName)
             == SNMP_ZERO) &&
            (pMibOid->u4_Length == gaSnmpSysorTable[u4Index].u4OidLen) &&
            (MEMCMP (gaSnmpSysorTable[u4Index].au4SysOrId, pMibOid->pu4_OidList,
                     (pMibOid->u4_Length * sizeof (UINT4))) == SNMP_ZERO))
        {
            MEMSET (&gaSnmpSysorTable[u4Index], SNMP_ZERO,
                    sizeof (tSNMP_SYSORTABLE));
            gaSnmpSysorTable[u4Index].i4Status = SNMP_INACTIVE;
            break;
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3GroupConfig                                   */
/*                                                                           */
/*     DESCRIPTION      : Function to configure the Group Table              */
/*                                                                           */
/*     INPUT            : pu1GroupName  -  Group Name                        */
/*                        pu1SecName    -  Security Name                     */
/*                        i4SecModel    -  Security Model                    */
/*                        u4StorageType - Storage type in memory             */
/*                        pu4ErrCode    - Error code pointer                 */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
Snmp3GroupConfig (UINT1 *pu1GroupName, UINT1 *pu1SecName, INT4 i4SecModel,
                  UINT4 u4StorageType, UINT4 *pu4ErrCode)
{
    UINT4               u4ErrorCode;
    UINT1               au1GroupName[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1SecurityName[SNMP_MAX_ADMIN_STR_LENGTH];

    tSecGrp            *pSecGrp = NULL;
    tSNMP_OCTET_STRING_TYPE SecurityName;
    tSNMP_OCTET_STRING_TYPE GroupName;

    /* verify validity of inputs */
    if ((SnmpCheckString (pu1SecName) == SNMP_FAILURE) ||
        (SnmpCheckString (pu1GroupName) == SNMP_FAILURE))
    {
        *pu4ErrCode = CLI_SNMP3_INVALID_INPUT_ERR;
        return SNMP_FAILURE;
    }

    MEMSET (au1SecurityName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    STRNCPY (au1SecurityName, pu1SecName, (sizeof (au1SecurityName) - 1));

    SecurityName.pu1_OctetList = au1SecurityName;
    SecurityName.i4_Length = (INT4) STRLEN (au1SecurityName);

    MEMSET (au1GroupName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    STRNCPY (au1GroupName, pu1GroupName, (sizeof (au1GroupName) - 1));

    GroupName.pu1_OctetList = au1GroupName;
    GroupName.i4_Length = (INT4) STRLEN (au1GroupName);

    /* check if an entry exists already */
    if ((pSecGrp = VACMGetSecGrp (i4SecModel, &SecurityName)) != NULL)
    {
        nmhSetVacmSecurityToGroupStatus (i4SecModel, &SecurityName,
                                         SNMP_ROWSTATUS_NOTINSERVICE);

        /* entry exists - if any value in table is changed, modify */
        /* Test Inputs */
        if (nmhTestv2VacmGroupName (&u4ErrorCode, i4SecModel, &SecurityName,
                                    &GroupName) == SNMP_FAILURE)
        {
            if (u4ErrorCode == SNMP_ERR_NO_ACCESS)
            {
                *pu4ErrCode = CLI_SNMP3_GROUP_SECURE_MODE_ERR;
                return (Snmp3GroupCreateError (&SecurityName, i4SecModel,
                                               &pu4ErrCode));
            }
            else
            {
                return (Snmp3GroupCreateError (&SecurityName, i4SecModel,
                                               &pu4ErrCode));
            }
        }

        /* Set Inputs */
        if (nmhSetVacmGroupName (i4SecModel, &SecurityName, &GroupName)
            == SNMP_FAILURE)
        {
            return (Snmp3GroupCreateError (&SecurityName, i4SecModel,
                                           &pu4ErrCode));
        }

        if ((u4StorageType != 0) &&
            ((INT4) u4StorageType != pSecGrp->i4Storage))
        {
            /* Storage Type modified, save new value */
            if (nmhSetVacmSecurityToGroupStorageType (i4SecModel,
                                                      &SecurityName,
                                                      (INT4) u4StorageType)
                == SNMP_FAILURE)
            {
                return (Snmp3GroupModifyError (pSecGrp, &pu4ErrCode));
            }
        }

        nmhSetVacmSecurityToGroupStatus (i4SecModel, &SecurityName,
                                         SNMP_ROWSTATUS_ACTIVE);
        /* Entry modified */
    }
    else
    {
        /* entry does not exist - ADD */
        if (nmhSetVacmSecurityToGroupStatus (i4SecModel, &SecurityName,
                                             SNMP_ROWSTATUS_CREATEANDWAIT)
            == SNMP_FAILURE)
        {
            *pu4ErrCode = CLI_SNMP3_NEW_ENTRY_ERR;
            return SNMP_FAILURE;
        }

        /* Test Inputs */
        if (nmhTestv2VacmGroupName (&u4ErrorCode, i4SecModel, &SecurityName,
                                    &GroupName) == SNMP_FAILURE)
        {
            if (u4ErrorCode == SNMP_ERR_NO_ACCESS)
            {
                *pu4ErrCode = CLI_SNMP3_GROUP_SECURE_MODE_ERR;
                return (Snmp3GroupCreateError (&SecurityName, i4SecModel,
                                               &pu4ErrCode));
            }
            else
            {
                return (Snmp3GroupCreateError (&SecurityName, i4SecModel,
                                               &pu4ErrCode));
            }
        }

        /* Set Inputs */
        if (nmhSetVacmGroupName (i4SecModel, &SecurityName, &GroupName)
            == SNMP_FAILURE)
        {
            return (Snmp3GroupCreateError (&SecurityName, i4SecModel,
                                           &pu4ErrCode));
        }

        if (u4StorageType == 0)
        {
            /* Storage type not given  - make default as non-volatile type */
            u4StorageType = SNMP3_STORAGE_TYPE_NONVOLATILE;
        }

        if (nmhSetVacmSecurityToGroupStorageType (i4SecModel, &SecurityName,
                                                  (INT4) u4StorageType)
            == SNMP_FAILURE)
        {
            return (Snmp3GroupCreateError (&SecurityName, i4SecModel,
                                           &pu4ErrCode));
        }

        nmhSetVacmSecurityToGroupStatus (i4SecModel, &SecurityName,
                                         SNMP_ROWSTATUS_ACTIVE);
        /* Entry created */
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3DefGroupConfig                                */
/*                                                                           */
/*     DESCRIPTION      : Function to configure a default group entry during */
/*                        initialization                                     */
/*                                                                           */
/*     INPUT            : pu4ErrCode    - Error code pointer                 */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
Snmp3DefGroupConfig (VOID)
{
    UINT1               au1GroupName[SNMP_MAX_ADMIN_STR_LENGTH] =
        SNMP_DEF_GROUP_1;
    UINT1               au1SecName[SNMP_MAX_ADMIN_STR_LENGTH] = "none";
    INT4                i4SecModel = SNMP_SECMODEL_V2C;
    UINT4               u4StorageType = SNMP3_STORAGE_TYPE_NONVOLATILE;
    UINT4               u4ErrCode;

    UNUSED_PARAM (u4ErrCode);

    /* Create default entry */
    Snmp3GroupConfig (au1GroupName, au1SecName, i4SecModel,
                      u4StorageType, &u4ErrCode);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3Def1GroupConfig                               */
/*                                                                           */
/*     DESCRIPTION      : Function to configure a default group entry during */
/*                        initialization                                     */
/*                                                                           */
/*     INPUT            : pu4ErrCode    - Error code pointer                 */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
Snmp3Def1GroupConfig (VOID)
{
    UINT1               au1GroupName[SNMP_MAX_ADMIN_STR_LENGTH] =
        SNMP_DEF_GROUP_1;
    UINT1               au1SecName[SNMP_MAX_ADMIN_STR_LENGTH] = "none";
    INT4                i4SecModel = SNMP_SECMODEL_V1;
    UINT4               u4StorageType = SNMP3_STORAGE_TYPE_NONVOLATILE;
    UINT4               u4ErrCode;

    /* Create default entry */
    Snmp3GroupConfig (au1GroupName, au1SecName, i4SecModel,
                      u4StorageType, &u4ErrCode);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : SnmpCheckString                                    */
/*                                                                           */
/*     DESCRIPTION      : Function to check whether a given string contains  */
/*                        alphanumric characters                             */
/*                                                                           */
/*     INPUT            : pu1Ptr - String                                    */
/*                        pu4ErrCode    - Error code pointer                 */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT1
SnmpCheckString (UINT1 *pu1Ptr)
{
    UINT4               u4Count = 0;
    for (u4Count = 0; pu1Ptr[u4Count] != '\0'; u4Count++)
    {
        if ((isalnum (pu1Ptr[u4Count]) == 0) && (pu1Ptr[u4Count] != 32))
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/********************************************************************************/
/*                                                                              */
/*     FUNCTION NAME    : SnmpCheckDefConfig                                    */
/*                                                                              */
/*     DESCRIPTION      : Function to check whether config is default or not    */
/*                                                                              */
/*     INPUT            : Snmpparam         - Usm User Table Index        */
/*                        UsmAuthProtocol     - Usm User Auth Protocol    */
/*                        UsmPrivProtocol     - Usm User Priv Protocol    */
/*                        i4RetStorageType     - Usm User Storage Type        */
/*                                                                               */
/*     RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                   */
/*                        SNMP_SUCCESS will be returned in case of default user */
/*                        configurations otherwise SNMP_FAILURE                 */
/*                        will be returned                                      */
/*                                                                               */
/********************************************************************************/
INT1
SnmpCheckDefConfig (tSNMP_OCTET_STRING_TYPE * SnmpIndex,
                    tSNMP_OCTET_STRING_TYPE * Snmpparam,
                    tSNMP_OID_TYPE * UsmAuthProtocol,
                    tSNMP_OID_TYPE * UsmPrivProtocol, INT4 i4RetStorageType)
{
    UINT4               u4Count = 0;
    INT1                ai1OID1[MAX_OID_LENGTH];
    INT1                ai1OID2[MAX_OID_LENGTH];

    INT1               *pu1AuthOID = ai1OID1;
    INT1               *pu1PrivOID = ai1OID2;

    MEMSET (ai1OID1, 0, MAX_OID_LENGTH);
    MEMSET (ai1OID2, 0, MAX_OID_LENGTH);
    /* UNUSED_PARAM (i4RetStorageType); */

    if ((STRCMP (Snmpparam->pu1_OctetList, SNMP_DEF_USER_1) != 0) &&
        (STRCMP (Snmpparam->pu1_OctetList, SNMP_DEF_USER_2) != 0) &&
        (STRCMP (Snmpparam->pu1_OctetList, SNMP_DEF_USER_3) != 0))
    {
        return SNMP_FAILURE;
    }

    for (u4Count = 0; u4Count < (UsmAuthProtocol->u4_Length - 1); u4Count++)
    {
        SPRINTF ((CHR1 *) pu1AuthOID + STRLEN (pu1AuthOID), "%u.",
                 UsmAuthProtocol->pu4_OidList[u4Count]);
    }
    SPRINTF ((CHR1 *) pu1AuthOID + STRLEN (pu1AuthOID), "%u",
             UsmAuthProtocol->pu4_OidList[u4Count]);
    pu1AuthOID[STRLEN (pu1AuthOID)] = '\0';

    for (u4Count = 0; u4Count < (UsmPrivProtocol->u4_Length - 1); u4Count++)
    {
        SPRINTF ((CHR1 *) pu1PrivOID + STRLEN (pu1PrivOID), "%u.",
                 UsmPrivProtocol->pu4_OidList[u4Count]);
    }
    SPRINTF ((CHR1 *) pu1PrivOID + STRLEN (pu1PrivOID), "%u",
             UsmPrivProtocol->pu4_OidList[u4Count]);
    pu1PrivOID[STRLEN (pu1PrivOID)] = '\0';

    if ((STRCMP (Snmpparam->pu1_OctetList, SNMP_DEF_USER_1) == 0) &&
        (STRCMP (pu1AuthOID, SNMP_USER1_AUTH_OID) == 0) &&
        (STRCMP (pu1PrivOID, SNMP_USER1_PRIV_OID) == 0) &&
        (i4RetStorageType == SNMP3_STORAGE_TYPE_NONVOLATILE))
    {
        if (SNMP_EQUAL == SNMPCompareOctetString (SnmpIndex, &gSnmpEngineID))
        {
            return SNMP_SUCCESS;
        }
    }

    if ((STRCMP (Snmpparam->pu1_OctetList, SNMP_DEF_USER_2) == 0) &&
        (STRCMP (pu1AuthOID, SNMP_USER2_AUTH_OID) == 0) &&
        (STRCMP (pu1PrivOID, SNMP_USER2_PRIV_OID) == 0) &&
        (i4RetStorageType == SNMP3_STORAGE_TYPE_NONVOLATILE))
    {
        if (SNMP_EQUAL == SNMPCompareOctetString (SnmpIndex, &gSnmpEngineID))

        {
            return SNMP_SUCCESS;
        }
    }

    if ((STRCMP (Snmpparam->pu1_OctetList, SNMP_DEF_USER_3) == 0) &&
        (STRCMP (pu1AuthOID, SNMP_USER3_AUTH_OID) == 0) &&
        (STRCMP (pu1PrivOID, SNMP_USER3_PRIV_OID) == 0) &&
        (i4RetStorageType == SNMP3_STORAGE_TYPE_NONVOLATILE))
    {
        if (SNMP_EQUAL == SNMPCompareOctetString (SnmpIndex, &gSnmpEngineID))
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3GroupModifyError                              */
/*                                                                           */
/*     DESCRIPTION      : Function to set Group Table modification error     */
/*                        and set the row status of the entry to ACTIVE      */
/*                        the entry is set to ACTIVE as modification to the  */
/*                        existing entry failed                              */
/*                                                                           */
/*     INPUT            : pSecGrp  - pointer to Security Group entry         */
/*                        pu4ErrCode - pointer to Error code                 */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT1
Snmp3GroupModifyError (tSecGrp * pSecGrp, UINT4 **pu4ErrCode)
{
    **pu4ErrCode = CLI_SNMP3_ENTRY_MODIFICATION_ERR;
    pSecGrp->i4Status = SNMP_ACTIVE;

    return SNMP_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3GroupCreateError                              */
/*                                                                           */
/*     DESCRIPTION      : Function to set Group Table entry creation err     */
/*                        and to delete the entry created                    */
/*                                                                           */
/*     INPUT            : pSecName   - Security Name                         */
/*                        i4SecModel - Security Model                        */
/*                        pu4ErrCode    - Error code pointer                 */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT1
Snmp3GroupCreateError (tSNMP_OCTET_STRING_TYPE * pSecName, INT4 i4SecModel,
                       UINT4 **pu4ErrCode)
{
    if (**pu4ErrCode != CLI_SNMP3_GROUP_SECURE_MODE_ERR)
    {
        **pu4ErrCode = CLI_SNMP3_ENTRY_CREATION_ERR;
    }
    if (VACMDeleteSecGrp (i4SecModel, pSecName) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3AccessConfig                                  */
/*                                                                           */
/*     DESCRIPTION      : Function to configure the Access Table             */
/*                                                                           */
/*     INPUT            : pu1Group      - Group Name                         */
/*                        pu1Context    - Context Name                       */
/*                        i4SecLevel    - Security Level                     */
/*                        i4SecModel    - Security Model                     */
/*                        pu1ReadName   - Read Name                          */
/*                        pu1WriteName  - Write Name                         */
/*                        pu1NotifyName - Notify Name                        */
/*                        u4StorageType - Storage type in memory             */
/*                        pu4ErrCode    - Error code pointer                 */
/*                                                                           */
/*     OUTPUT           : None.                                              */
/*                                                                           */
/*     RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
Snmp3AccessConfig (UINT1 *pu1Group, UINT1 *pu1Context, INT4 i4SecLevel,
                   INT4 i4SecModel, UINT1 *pu1ReadName,
                   UINT1 *pu1WriteName, UINT1 *pu1NotifyName,
                   UINT4 u4StorageType, UINT4 *pu4ErrCode)
{
    UINT4               u4ErrorCode;
    UINT1               u1ReadFlag = SNMP_FALSE;
    UINT1               u1WriteFlag = SNMP_FALSE;
    UINT1               u1NotifyFlag = SNMP_FALSE;
    UINT1               au1GroupName[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1ContextName[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1ReadViewName[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1WriteViewName[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1NotifyViewName[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1ClearView[SNMP_MAX_ADMIN_STR_LENGTH];

    tSNMP_OCTET_STRING_TYPE GroupName;
    tSNMP_OCTET_STRING_TYPE ContextName;
    tSNMP_OCTET_STRING_TYPE ReadViewName;
    tSNMP_OCTET_STRING_TYPE WriteViewName;
    tSNMP_OCTET_STRING_TYPE NotifyViewName;
    tSNMP_OCTET_STRING_TYPE ClearView;
    tAccessEntry       *pAccess = NULL;

    /* verify validity of inputs */
    if ((SnmpCheckString (pu1Context) == SNMP_FAILURE) ||
        (SnmpCheckString (pu1Group) == SNMP_FAILURE) ||
        (SnmpCheckString (pu1ReadName) == SNMP_FAILURE) ||
        (SnmpCheckString (pu1WriteName) == SNMP_FAILURE) ||
        (SnmpCheckString (pu1NotifyName) == SNMP_FAILURE))
    {
        *pu4ErrCode = CLI_SNMP3_INVALID_INPUT_ERR;
        return SNMP_FAILURE;
    }

    MEMSET (au1GroupName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    STRNCPY (au1GroupName, pu1Group, (sizeof (au1GroupName) - 1));

    GroupName.pu1_OctetList = au1GroupName;
    GroupName.i4_Length = (INT4) STRLEN (au1GroupName);

    MEMSET (au1ContextName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    STRNCPY (au1ContextName, pu1Context, (sizeof (au1ContextName) - 1));
    ContextName.pu1_OctetList = au1ContextName;
    ContextName.i4_Length = (INT4) STRLEN (pu1Context);

    ReadViewName.i4_Length = 0;    /*kloc */
    WriteViewName.i4_Length = 0;    /*kloc */
    NotifyViewName.i4_Length = 0;    /*kloc */

    if ((STRCMP (pu1ReadName, "noview") != 0) &&
        (STRCMP (pu1ReadName, "clearview") != 0))
    {
        MEMSET (au1ReadViewName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
        STRNCPY (au1ReadViewName, pu1ReadName, (sizeof (au1ReadViewName) - 1));

        ReadViewName.pu1_OctetList = au1ReadViewName;
        ReadViewName.i4_Length = (INT4) STRLEN (au1ReadViewName);

        u1ReadFlag = SNMP_TRUE;
    }

    if ((STRCMP (pu1WriteName, "noview") != 0) &&
        (STRCMP (pu1WriteName, "clearview") != 0))
    {
        MEMSET (au1WriteViewName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
        STRNCPY (au1WriteViewName, pu1WriteName,
                 (sizeof (au1WriteViewName) - 1));

        WriteViewName.pu1_OctetList = au1WriteViewName;
        WriteViewName.i4_Length = (INT4) STRLEN (au1WriteViewName);

        u1WriteFlag = SNMP_TRUE;
    }

    if ((STRCMP (pu1NotifyName, "noview") != 0) &&
        (STRCMP (pu1NotifyName, "clearview") != 0))
    {
        MEMSET (au1NotifyViewName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
        STRNCPY (au1NotifyViewName, pu1NotifyName,
                 (sizeof (au1NotifyViewName) - 1));

        NotifyViewName.pu1_OctetList = au1NotifyViewName;
        NotifyViewName.i4_Length = (INT4) STRLEN (au1NotifyViewName);

        u1NotifyFlag = SNMP_TRUE;
    }

    /* check if an entry exists already */
    if ((pAccess = VACMGetAccess (&GroupName, &ContextName,
                                  i4SecModel, i4SecLevel)) != NULL)
    {
        nmhSetVacmAccessStatus (&GroupName, &ContextName,
                                i4SecModel, i4SecLevel,
                                SNMP_ROWSTATUS_NOTINSERVICE);

        MEMSET (au1ClearView, 0, SNMP_MAX_ADMIN_STR_LENGTH);

        ClearView.pu1_OctetList = au1ClearView;
        ClearView.i4_Length = 0;

        /* entry exists - if any value in table is changed, modify */
        if (STRCMP (pu1ReadName, "clearview") == 0)
        {
            /* Context cleared, clear existing value */
            if (nmhSetVacmAccessReadViewName (&GroupName, &ContextName,
                                              i4SecModel, i4SecLevel,
                                              &ClearView) == SNMP_FAILURE)
            {
                return (Snmp3AccessModifyError (&GroupName, &ContextName,
                                                i4SecModel, i4SecLevel,
                                                &pu4ErrCode));
            }
        }
        else
        {
            if ((STRCMP (pu1ReadName, "noview") != 0) &&
                (SNMPCompareOctetString (&ReadViewName,
                                         &(pAccess->ReadName)) != SNMP_EQUAL))
            {
                /* Readview name modified, test new value */
                if (nmhTestv2VacmAccessReadViewName (&u4ErrorCode, &GroupName,
                                                     &ContextName, i4SecModel,
                                                     i4SecLevel, &ReadViewName)
                    == SNMP_FAILURE)
                {
                    return (Snmp3AccessModifyError (&GroupName, &ContextName,
                                                    i4SecModel, i4SecLevel,
                                                    &pu4ErrCode));
                }

                /* clear previous context name */
                if (nmhSetVacmAccessReadViewName (&GroupName, &ContextName,
                                                  i4SecModel, i4SecLevel,
                                                  &ClearView) == SNMP_FAILURE)
                {
                    return (Snmp3AccessModifyError (&GroupName, &ContextName,
                                                    i4SecModel, i4SecLevel,
                                                    &pu4ErrCode));
                }

                /* save new context name */
                if (nmhSetVacmAccessReadViewName (&GroupName, &ContextName,
                                                  i4SecModel, i4SecLevel,
                                                  &ReadViewName) ==
                    SNMP_FAILURE)
                {
                    return (Snmp3AccessModifyError (&GroupName, &ContextName,
                                                    i4SecModel, i4SecLevel,
                                                    &pu4ErrCode));
                }
            }
        }

        if (STRCMP (pu1WriteName, "clearview") == 0)
        {
            /* Context cleared, clear existing value */
            if (nmhSetVacmAccessWriteViewName (&GroupName, &ContextName,
                                               i4SecModel, i4SecLevel,
                                               &ClearView) == SNMP_FAILURE)
            {
                return (Snmp3AccessModifyError (&GroupName, &ContextName,
                                                i4SecModel, i4SecLevel,
                                                &pu4ErrCode));
            }
        }
        else
        {
            if ((STRCMP (pu1WriteName, "noview") != 0) &&
                (SNMPCompareOctetString (&WriteViewName,
                                         &(pAccess->WriteName)) != SNMP_EQUAL))
            {
                /* Writeview name modified, test new value */
                if (nmhTestv2VacmAccessWriteViewName (&u4ErrorCode, &GroupName,
                                                      &ContextName, i4SecModel,
                                                      i4SecLevel,
                                                      &WriteViewName) ==
                    SNMP_FAILURE)
                {
                    if (u4ErrorCode == SNMP_ERROR_WRTIE_ACCESS_DENIED)
                    {
                        *pu4ErrCode = CLI_SNMP3_WRTIE_ACCESS_DENIED;
                        return SNMP_FAILURE;
                    }
                    else
                    {
                        return (Snmp3AccessModifyError
                                (&GroupName, &ContextName, i4SecModel,
                                 i4SecLevel, &pu4ErrCode));
                    }
                }

                /* clear previous context name */
                if (nmhSetVacmAccessWriteViewName (&GroupName, &ContextName,
                                                   i4SecModel, i4SecLevel,
                                                   &ClearView) == SNMP_FAILURE)
                {
                    return (Snmp3AccessModifyError (&GroupName, &ContextName,
                                                    i4SecModel, i4SecLevel,
                                                    &pu4ErrCode));
                }

                /* save new context name */
                if (nmhSetVacmAccessWriteViewName (&GroupName, &ContextName,
                                                   i4SecModel, i4SecLevel,
                                                   &WriteViewName) ==
                    SNMP_FAILURE)
                {
                    return (Snmp3AccessModifyError (&GroupName, &ContextName,
                                                    i4SecModel, i4SecLevel,
                                                    &pu4ErrCode));
                }
            }
        }

        if (STRCMP (pu1NotifyName, "clearview") == 0)
        {
            /* Notify view cleared, clear existing value */
            if (nmhSetVacmAccessNotifyViewName (&GroupName, &ContextName,
                                                i4SecModel, i4SecLevel,
                                                &ClearView) == SNMP_FAILURE)
            {
                return (Snmp3AccessModifyError (&GroupName, &ContextName,
                                                i4SecModel, i4SecLevel,
                                                &pu4ErrCode));
            }
        }
        else
        {
            if ((STRCMP (pu1NotifyName, "noview") != 0) &&
                (SNMPCompareOctetString (&NotifyViewName,
                                         &(pAccess->NotifyName)) != SNMP_EQUAL))
            {
                /* Notifyview name modified, test new value */
                if (nmhTestv2VacmAccessNotifyViewName (&u4ErrorCode, &GroupName,
                                                       &ContextName, i4SecModel,
                                                       i4SecLevel,
                                                       &NotifyViewName) ==
                    SNMP_FAILURE)
                {
                    return (Snmp3AccessModifyError (&GroupName, &ContextName,
                                                    i4SecModel, i4SecLevel,
                                                    &pu4ErrCode));
                }

                /* clear previous context name */
                if (nmhSetVacmAccessNotifyViewName (&GroupName, &ContextName,
                                                    i4SecModel, i4SecLevel,
                                                    &ClearView) == SNMP_FAILURE)
                {
                    return (Snmp3AccessModifyError (&GroupName, &ContextName,
                                                    i4SecModel, i4SecLevel,
                                                    &pu4ErrCode));
                }

                /* save new notify name */
                if (nmhSetVacmAccessNotifyViewName (&GroupName, &ContextName,
                                                    i4SecModel, i4SecLevel,
                                                    &NotifyViewName) ==
                    SNMP_FAILURE)
                {
                    return (Snmp3AccessModifyError (&GroupName, &ContextName,
                                                    i4SecModel, i4SecLevel,
                                                    &pu4ErrCode));
                }
            }
        }

        if ((u4StorageType != 0) &&
            ((INT4) u4StorageType != pAccess->i4Storage))
        {
            /* Storage Type modified, save new value */
            if (nmhSetVacmAccessStorageType (&GroupName, &ContextName,
                                             i4SecModel, i4SecLevel,
                                             (INT4) u4StorageType)
                == SNMP_FAILURE)
            {
                return (Snmp3AccessModifyError (&GroupName, &ContextName,
                                                i4SecModel, i4SecLevel,
                                                &pu4ErrCode));
            }
        }

        nmhSetVacmAccessStatus (&GroupName, &ContextName,
                                i4SecModel, i4SecLevel, SNMP_ROWSTATUS_ACTIVE);
        /* Entry modified */
    }
    else
    {
        /* entry does not exist - ADD */
        if (nmhSetVacmAccessStatus (&GroupName, &ContextName,
                                    i4SecModel, i4SecLevel,
                                    SNMP_ROWSTATUS_CREATEANDWAIT)
            == SNMP_FAILURE)
        {
            *pu4ErrCode = CLI_SNMP3_NEW_ENTRY_ERR;
            return SNMP_FAILURE;
        }

        /* Test Inputs */
        if (u1ReadFlag == SNMP_TRUE)
        {
            if (nmhTestv2VacmAccessReadViewName (&u4ErrorCode, &GroupName,
                                                 &ContextName, i4SecModel,
                                                 i4SecLevel, &ReadViewName)
                == SNMP_FAILURE)
            {
                return (Snmp3AccessCreateError (&GroupName, &ContextName,
                                                i4SecModel, i4SecLevel,
                                                &pu4ErrCode));
            }
        }

        if (u1WriteFlag == SNMP_TRUE)
        {
            if (nmhTestv2VacmAccessWriteViewName (&u4ErrorCode, &GroupName,
                                                  &ContextName, i4SecModel,
                                                  i4SecLevel, &WriteViewName)
                == SNMP_FAILURE)
            {
                if (u4ErrorCode == SNMP_ERROR_WRTIE_ACCESS_DENIED)
                {
                    *pu4ErrCode = CLI_SNMP3_WRTIE_ACCESS_DENIED;
                    return SNMP_FAILURE;
                }
                else
                {
                    return (Snmp3AccessCreateError (&GroupName, &ContextName,
                                                    i4SecModel, i4SecLevel,
                                                    &pu4ErrCode));
                }
            }
        }

        if (u1NotifyFlag == SNMP_TRUE)
        {
            if (nmhTestv2VacmAccessNotifyViewName (&u4ErrorCode, &GroupName,
                                                   &ContextName, i4SecModel,
                                                   i4SecLevel, &NotifyViewName)
                == SNMP_FAILURE)
            {
                return (Snmp3AccessCreateError (&GroupName, &ContextName,
                                                i4SecModel, i4SecLevel,
                                                &pu4ErrCode));
            }
        }

        /* Set Inputs */
        if (u1ReadFlag == SNMP_TRUE)
        {
            if (nmhSetVacmAccessReadViewName (&GroupName, &ContextName,
                                              i4SecModel, i4SecLevel,
                                              &ReadViewName) == SNMP_FAILURE)
            {
                return (Snmp3AccessCreateError (&GroupName, &ContextName,
                                                i4SecModel, i4SecLevel,
                                                &pu4ErrCode));
            }
        }

        if (u1WriteFlag == SNMP_TRUE)
        {
            if (nmhSetVacmAccessWriteViewName (&GroupName, &ContextName,
                                               i4SecModel, i4SecLevel,
                                               &WriteViewName) == SNMP_FAILURE)
            {
                return (Snmp3AccessCreateError (&GroupName, &ContextName,
                                                i4SecModel, i4SecLevel,
                                                &pu4ErrCode));
            }
        }

        if (u1NotifyFlag == SNMP_TRUE)
        {
            if (nmhSetVacmAccessNotifyViewName (&GroupName, &ContextName,
                                                i4SecModel, i4SecLevel,
                                                &NotifyViewName) ==
                SNMP_FAILURE)
            {
                return (Snmp3AccessCreateError (&GroupName, &ContextName,
                                                i4SecModel, i4SecLevel,
                                                &pu4ErrCode));
            }
        }

        if (u4StorageType == 0)
        {
            /* Storage type not given  - make default as non-volatile type */
            u4StorageType = SNMP3_STORAGE_TYPE_NONVOLATILE;
        }

        if (nmhSetVacmAccessStorageType (&GroupName, &ContextName,
                                         i4SecModel, i4SecLevel,
                                         (INT4) u4StorageType) == SNMP_FAILURE)
        {
            return (Snmp3AccessCreateError (&GroupName, &ContextName,
                                            i4SecModel, i4SecLevel,
                                            &pu4ErrCode));
        }

        nmhSetVacmAccessStatus (&GroupName, &ContextName,
                                i4SecModel, i4SecLevel, SNMP_ROWSTATUS_ACTIVE);
        /* Entry created */
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3DefAccessConfig                               */
/*                                                                           */
/*     DESCRIPTION      : Function to configure a default access entry       */
/*                        during initialization                              */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
Snmp3DefAccessConfig ()
{
    UINT1               au1Group[SNMP_MAX_ADMIN_STR_LENGTH] = SNMP_DEF_ACCESS_1;
    UINT1               au1Context[SNMP_MAX_ADMIN_STR_LENGTH] = "";
    INT4                i4SecLevel = SNMP_NOAUTH_NOPRIV;
    INT4                i4SecModel = SNMP_SECMODEL_V2C;
    UINT4               u4StorageType = SNMP3_STORAGE_TYPE_NONVOLATILE;
    UINT4               u4ErrCode;
    UINT1               au1ReadName[SNMP_MAX_ADMIN_STR_LENGTH] =
        SNMP_DEF_READ_NAME;
    UINT1               au1WriteName[SNMP_MAX_ADMIN_STR_LENGTH] =
        SNMP_DEF_WRITE_NAME;
    UINT1               au1NotifyName[SNMP_MAX_ADMIN_STR_LENGTH] =
        SNMP_DEF_NOTIFY_NAME;

    UNUSED_PARAM (u4ErrCode);

    /* Create default entry */
    Snmp3AccessConfig (au1Group, au1Context, i4SecLevel,
                       i4SecModel, au1ReadName, au1WriteName,
                       au1NotifyName, u4StorageType, &u4ErrCode);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3Def1AccessConfig                              */
/*                                                                           */
/*     DESCRIPTION      : Function to configure a default access entry       */
/*                        during initialization                              */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
Snmp3Def1AccessConfig ()
{
    UINT1               au1Group[SNMP_MAX_ADMIN_STR_LENGTH] = SNMP_DEF_ACCESS_1;
    UINT1               au1Context[SNMP_MAX_ADMIN_STR_LENGTH] = "";
    INT4                i4SecLevel = SNMP_NOAUTH_NOPRIV;
    INT4                i4SecModel = SNMP_SECMODEL_V1;
    UINT4               u4ErrCode;
    UINT4               u4StorageType = SNMP3_STORAGE_TYPE_NONVOLATILE;
    UINT1               au1ReadName[SNMP_MAX_ADMIN_STR_LENGTH] =
        SNMP_DEF_READ_NAME;
    UINT1               au1WriteName[SNMP_MAX_ADMIN_STR_LENGTH] =
        SNMP_DEF_WRITE_NAME;
    UINT1               au1NotifyName[SNMP_MAX_ADMIN_STR_LENGTH] =
        SNMP_DEF_NOTIFY_NAME;

    UNUSED_PARAM (u4ErrCode);

    /* Create default entry */
    Snmp3AccessConfig (au1Group, au1Context, i4SecLevel,
                       i4SecModel, au1ReadName, au1WriteName,
                       au1NotifyName, u4StorageType, &u4ErrCode);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3AccessModifyError                             */
/*                                                                           */
/*     DESCRIPTION      : Function to set Access Table modification error    */
/*                        and set the row status of the entry to ACTIVE      */
/*                        the entry is set to ACTIVE as modification to the  */
/*                        existing entry failed                              */
/*                                                                           */
/*     INPUT            : pAccess  - pointer to Access Group entry           */
/*                        pu4ErrCode    - Error code pointer                 */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT1
Snmp3AccessModifyError (tSNMP_OCTET_STRING_TYPE * pGroupName,
                        tSNMP_OCTET_STRING_TYPE * pContextName,
                        INT4 i4SecModel, INT4 i4SecLevel, UINT4 **pu4ErrCode)
{
    **pu4ErrCode = CLI_SNMP3_ENTRY_MODIFICATION_ERR;

    nmhSetVacmAccessStatus (pGroupName, pContextName,
                            i4SecModel, i4SecLevel, SNMP_ROWSTATUS_ACTIVE);

    return SNMP_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3AccessCreateError                             */
/*                                                                           */
/*     DESCRIPTION      : Function to set Access Table entry creation err    */
/*                        and to delete the entry created                    */
/*                                                                           */
/*     INPUT            : pGroupName   - Group Name                          */
/*                        pContextName - Context Name                        */
/*                        i4SecModel   - Security Model                      */
/*                        i4SecLevel   - Security Level                      */
/*                        pu4ErrCode    - Error code pointer                 */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT1
Snmp3AccessCreateError (tSNMP_OCTET_STRING_TYPE * pGroupName,
                        tSNMP_OCTET_STRING_TYPE * pContextName,
                        INT4 i4SecModel, INT4 i4SecLevel, UINT4 **pu4ErrCode)
{
    **pu4ErrCode = CLI_SNMP3_ENTRY_CREATION_ERR;

    nmhSetVacmAccessStatus (pGroupName, pContextName, i4SecModel,
                            i4SecLevel, SNMP_ROWSTATUS_DESTROY);

    return SNMP_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3ProxyConfig                                  */
/*                                                                           */
/*     DESCRIPTION      : Function to set Proxy Table entry creation err    */
/*                        and to delete the entry created                    */
/*                                                                           */
/*     INPUT            : pProxyName   - Proxy Name                          */
/*                        u4ProxyType  - Proxy Type                          */
/*                        pu1ProxyContextEngineID  - Context Name            */
/*                        pu1ProxyTargetParamsIn   - Target paramIn          */
/*                        pu1ProxyTargetOut        - Target Out              */
/*                        pu1ProxyContextName      - Context Name            */
/*                        u4StorageType            - Storage Type            */
/*                        pu4ErrCode    - Error code pointer                 */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
Snmp3ProxyConfig (UINT1 *pu1ProxyName, UINT4 u4ProxyType,
                  UINT1 *pu1ProxyContextEngineID, UINT1 *pu1ProxyTargetParamsIn,
                  UINT1 *pu1ProxyTargetOut, UINT1 *pu1ProxyContextName,
                  UINT4 u4StorageType, UINT4 *pu4ErrCode)
{

    UINT1               au1ProxyName[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1ProxyContextEngineID[SNMP_MAX_STR_ENGINEID_LEN];
    UINT1               au1ProxyContextName[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1ProxyTargetParamsIn[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1ProxyTargetOut[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1ProxyClear[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1ProxyClearContextEngineId[SNMP_MAX_STR_ENGINEID_LEN];

    INT4                i4PreviousRowStatus = 0;
    UINT4               u4NumOctets = 0;

    tProxyTableEntry   *pProxyEntry = NULL;
    tSNMP_OCTET_STRING_TYPE ProxyName;
    tSNMP_OCTET_STRING_TYPE ProxyContextEngineID;
    tSNMP_OCTET_STRING_TYPE ProxyContextName;
    tSNMP_OCTET_STRING_TYPE ProxyTargetParamsIn;
    tSNMP_OCTET_STRING_TYPE ProxyTargetOut;

    tSNMP_OCTET_STRING_TYPE ProxyClear;
    tSNMP_OCTET_STRING_TYPE ProxyClearContextEngineId;

    MEMSET (au1ProxyClear, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    ProxyClear.pu1_OctetList = au1ProxyClear;
    ProxyClear.i4_Length = SNMP_MAX_ADMIN_STR_LENGTH;

    MEMSET (au1ProxyClearContextEngineId, 0, SNMP_MAX_STR_ENGINEID_LEN);
    ProxyClearContextEngineId.pu1_OctetList = au1ProxyClearContextEngineId;
    ProxyClearContextEngineId.i4_Length = SNMP_MAX_STR_ENGINEID_LEN;

    ProxyContextEngineID.pu1_OctetList = au1ProxyContextEngineID;

    MEMSET (au1ProxyName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    MEMSET (au1ProxyContextEngineID, 0, SNMP_MAX_STR_ENGINEID_LEN);
    MEMSET (au1ProxyTargetParamsIn, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    ProxyContextEngineID.pu1_OctetList = au1ProxyContextEngineID;
    /* To remove klockworks warning */
    ProxyContextEngineID.i4_Length = 0;
    ProxyTargetParamsIn.i4_Length = 0;

    if (STRCMP (pu1ProxyName, "") != 0)
    {
        STRNCPY (au1ProxyName, pu1ProxyName, (sizeof (au1ProxyName) - 1));

        ProxyName.pu1_OctetList = au1ProxyName;
        ProxyName.i4_Length = (INT4) STRLEN (au1ProxyName);
    }

    if (STRCMP (pu1ProxyContextEngineID, "") != 0)
    {

        u4NumOctets = SnmpGetNumOctets ((INT1 *) pu1ProxyContextEngineID);
        if ((u4NumOctets < SNMP_MIN_ENGINE_ID_LEN) ||
            (u4NumOctets > SNMP_MAX_ENGINE_ID_LEN))
        {
            return CLI_FAILURE;
        }

        if (SnmpConvertStringToOctet ((INT1 *) pu1ProxyContextEngineID,
                                      &ProxyContextEngineID) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

    }

    if (STRCMP (pu1ProxyTargetParamsIn, "") != 0)
    {
        STRNCPY (au1ProxyTargetParamsIn, pu1ProxyTargetParamsIn,
                 (sizeof (au1ProxyTargetParamsIn) - 1));

        ProxyTargetParamsIn.pu1_OctetList = au1ProxyTargetParamsIn;
        ProxyTargetParamsIn.i4_Length = (INT4) STRLEN (au1ProxyTargetParamsIn);
    }

    MEMSET (au1ProxyContextName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    STRNCPY (au1ProxyContextName, pu1ProxyContextName,
             (sizeof (au1ProxyContextName) - 1));

    ProxyContextName.pu1_OctetList = au1ProxyContextName;
    ProxyContextName.i4_Length = (INT4) STRLEN (au1ProxyContextName);

    MEMSET (au1ProxyTargetOut, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    STRNCPY (au1ProxyTargetOut, pu1ProxyTargetOut,
             (sizeof (au1ProxyTargetOut) - 1));

    ProxyTargetOut.pu1_OctetList = au1ProxyTargetOut;
    ProxyTargetOut.i4_Length = (INT4) STRLEN (au1ProxyTargetOut);

    /* if entry already exist */
    if ((pProxyEntry = SNMPGetProxyEntryFromName (&ProxyName)) != NULL)
    {
        i4PreviousRowStatus = pProxyEntry->i4Status;
        if (nmhSetSnmpProxyRowStatus
            (&ProxyName, (INT4) SNMP_ROWSTATUS_NOTINSERVICE) == SNMP_FAILURE)
        {
            return Snmp3ProxyModifyError (pProxyEntry, &pu4ErrCode);

        }
        if (nmhTestv2SnmpProxyType (pu4ErrCode, &ProxyName, (INT4) u4ProxyType)
            == SNMP_FAILURE)
        {
            nmhSetSnmpProxyRowStatus (&ProxyName, i4PreviousRowStatus);
            return SNMP_FAILURE;

        }
        if (nmhTestv2SnmpProxyContextEngineID
            (pu4ErrCode, &ProxyName, &ProxyContextEngineID) == SNMP_FAILURE)
        {
            nmhSetSnmpProxyRowStatus (&ProxyName, i4PreviousRowStatus);
            return ((INT4) *pu4ErrCode);

        }
        if (nmhTestv2SnmpProxyContextName
            (pu4ErrCode, &ProxyName, &ProxyContextName) == SNMP_FAILURE)
        {
            nmhSetSnmpProxyRowStatus (&ProxyName, i4PreviousRowStatus);
            return SNMP_FAILURE;

        }
        if (nmhTestv2SnmpProxyTargetParamsIn
            (pu4ErrCode, &ProxyName, &ProxyTargetParamsIn) == SNMP_FAILURE)
        {
            nmhSetSnmpProxyRowStatus (&ProxyName, i4PreviousRowStatus);
            return SNMP_FAILURE;

        }
        if (nmhTestv2SnmpProxyStorageType
            (pu4ErrCode, &ProxyName, (INT4) u4StorageType) == SNMP_FAILURE)
        {
            nmhSetSnmpProxyRowStatus (&ProxyName, i4PreviousRowStatus);
            return SNMP_FAILURE;

        }
        if (nmhSetSnmpProxySingleTargetOut (&ProxyName, &ProxyClear) ==
            SNMP_FAILURE)
        {
            return Snmp3ProxyModifyError (pProxyEntry, &pu4ErrCode);

        }

        if (nmhSetSnmpProxyMultipleTargetOut (&ProxyName, &ProxyClear) ==
            SNMP_FAILURE)
        {
            return Snmp3ProxyModifyError (pProxyEntry, &pu4ErrCode);

        }

        /*modify  storage type. */
        if ((INT4) u4StorageType != pProxyEntry->i4Storage)
        {
            if (nmhSetSnmpProxyStorageType (&ProxyName, (INT4) u4StorageType) ==
                SNMP_FAILURE)
            {
                return Snmp3ProxyModifyError (pProxyEntry, &pu4ErrCode);

            }
        }

        if ((u4ProxyType == PROXY_TYPE_READ)
            || (u4ProxyType == PROXY_TYPE_WRITE))
        {
            if (nmhTestv2SnmpProxySingleTargetOut
                (pu4ErrCode, &ProxyName, &ProxyTargetOut) == SNMP_FAILURE)
            {
                nmhSetSnmpProxyRowStatus (&ProxyName, i4PreviousRowStatus);
                return SNMP_FAILURE;

            }
            if (nmhSetSnmpProxySingleTargetOut (&ProxyName, &ProxyTargetOut) ==
                SNMP_FAILURE)
            {
                return Snmp3ProxyModifyError (pProxyEntry, &pu4ErrCode);

            }
        }
        else
        {
            if (nmhTestv2SnmpProxyMultipleTargetOut
                (pu4ErrCode, &ProxyName, &ProxyTargetOut) == SNMP_FAILURE)
            {
                nmhSetSnmpProxyRowStatus (&ProxyName, i4PreviousRowStatus);
                return SNMP_FAILURE;

            }
            if (nmhSetSnmpProxyMultipleTargetOut (&ProxyName, &ProxyTargetOut)
                == SNMP_FAILURE)
            {
                return Snmp3ProxyModifyError (pProxyEntry, &pu4ErrCode);

            }
        }

        /*set all the fields */

        if ((INT4) u4ProxyType != pProxyEntry->ProxyType)
        {
            if (nmhSetSnmpProxyType (&ProxyName, (INT4) u4ProxyType) ==
                SNMP_FAILURE)
            {
                return Snmp3ProxyModifyError (pProxyEntry, &pu4ErrCode);

            }
        }

        if (SNMPCompareOctetString
            (&ProxyContextEngineID,
             &(pProxyEntry->PrxContextEngineID)) != SNMP_EQUAL)
        {
            if (nmhSetSnmpProxyContextEngineID
                (&ProxyName, &ProxyClearContextEngineId) == SNMP_FAILURE)
            {
                return Snmp3ProxyModifyError (pProxyEntry, &pu4ErrCode);

            }
            if (nmhSetSnmpProxyContextEngineID
                (&ProxyName, &ProxyContextEngineID) == SNMP_FAILURE)
            {
                return Snmp3ProxyModifyError (pProxyEntry, &pu4ErrCode);

            }
        }
        if (SNMPCompareOctetString
            (&ProxyContextName, &(pProxyEntry->PrxContextName)) != SNMP_EQUAL)
        {
            if (nmhSetSnmpProxyContextName (&ProxyName, &ProxyClear) ==
                SNMP_FAILURE)
            {
                return Snmp3ProxyModifyError (pProxyEntry, &pu4ErrCode);

            }
            if (nmhSetSnmpProxyContextName (&ProxyName, &ProxyContextName) ==
                SNMP_FAILURE)
            {
                return Snmp3ProxyModifyError (pProxyEntry, &pu4ErrCode);

            }

        }
        if (SNMPCompareOctetString
            (&ProxyTargetParamsIn,
             &(pProxyEntry->PrxTargetParamsIn)) != SNMP_EQUAL)
        {
            if (nmhSetSnmpProxyTargetParamsIn (&ProxyName, &ProxyClear) ==
                SNMP_FAILURE)
            {
                return Snmp3ProxyModifyError (pProxyEntry, &pu4ErrCode);

            }
            if (nmhSetSnmpProxyTargetParamsIn (&ProxyName, &ProxyTargetParamsIn)
                == SNMP_FAILURE)
            {
                return Snmp3ProxyModifyError (pProxyEntry, &pu4ErrCode);

            }
        }
        if (nmhTestv2SnmpProxyRowStatus
            (pu4ErrCode, &ProxyName,
             (INT4) SNMP_ROWSTATUS_ACTIVE) == SNMP_FAILURE)
        {
            return Snmp3ProxyModifyError (pProxyEntry, &pu4ErrCode);

        }

        if (nmhSetSnmpProxyRowStatus (&ProxyName, (INT4) SNMP_ROWSTATUS_ACTIVE)
            == SNMP_FAILURE)
        {
            return Snmp3ProxyModifyError (pProxyEntry, &pu4ErrCode);

        }

    }
    else
    {
        if (nmhSetSnmpProxyRowStatus
            (&ProxyName, (INT4) SNMP_ROWSTATUS_CREATEANDWAIT) == SNMP_FAILURE)
        {
            return Snmp3ProxyModifyError (pProxyEntry, &pu4ErrCode);

        }

        if (nmhTestv2SnmpProxyType
            (pu4ErrCode, &ProxyName, (INT4) u4ProxyType) == SNMP_FAILURE)
        {
            return Snmp3ProxyCreateError (&ProxyName, &pu4ErrCode);
        }
        if (nmhTestv2SnmpProxyContextEngineID
            (pu4ErrCode, &ProxyName, &ProxyContextEngineID) == SNMP_FAILURE)
        {
            return Snmp3ProxyCreateError (&ProxyName, &pu4ErrCode);

        }
        if (nmhTestv2SnmpProxyContextName
            (pu4ErrCode, &ProxyName, &ProxyContextName) == SNMP_FAILURE)
        {
            return Snmp3ProxyCreateError (&ProxyName, &pu4ErrCode);

        }
        if (nmhTestv2SnmpProxyTargetParamsIn
            (pu4ErrCode, &ProxyName, &ProxyTargetParamsIn) == SNMP_FAILURE)
        {
            return Snmp3ProxyCreateError (&ProxyName, &pu4ErrCode);

        }
        if (nmhTestv2SnmpProxyStorageType
            (pu4ErrCode, &ProxyName, (INT4) u4StorageType) == SNMP_FAILURE)
        {
            return Snmp3ProxyCreateError (&ProxyName, &pu4ErrCode);

        }

        if (nmhSetSnmpProxyStorageType
            (&ProxyName, (INT4) u4StorageType) == SNMP_FAILURE)
        {
            return Snmp3ProxyCreateError (&ProxyName, &pu4ErrCode);
        }

        if ((u4ProxyType == PROXY_TYPE_READ)
            || (u4ProxyType == PROXY_TYPE_WRITE))
        {
            if (nmhTestv2SnmpProxySingleTargetOut
                (pu4ErrCode, &ProxyName, &ProxyTargetOut) == SNMP_FAILURE)
            {
                return Snmp3ProxyCreateError (&ProxyName, &pu4ErrCode);

            }
            if (nmhSetSnmpProxySingleTargetOut
                (&ProxyName, &ProxyTargetOut) == SNMP_FAILURE)
            {
                return Snmp3ProxyCreateError (&ProxyName, &pu4ErrCode);

            }
        }
        else if ((u4ProxyType == PROXY_TYPE_TRAP)
                 || (u4ProxyType == PROXY_TYPE_INFORM))
        {
            if (nmhTestv2SnmpProxyMultipleTargetOut
                (pu4ErrCode, &ProxyName, &ProxyTargetOut) == SNMP_FAILURE)
            {
                return Snmp3ProxyCreateError (&ProxyName, &pu4ErrCode);
            }
            if (nmhSetSnmpProxyMultipleTargetOut
                (&ProxyName, &ProxyTargetOut) == SNMP_FAILURE)
            {
                return Snmp3ProxyCreateError (&ProxyName, &pu4ErrCode);

            }
        }
        else
        {
            return Snmp3ProxyCreateError (&ProxyName, &pu4ErrCode);

        }
        if (nmhSetSnmpProxyType (&ProxyName, (INT4) u4ProxyType) ==
            SNMP_FAILURE)
        {
            return Snmp3ProxyCreateError (&ProxyName, &pu4ErrCode);

        }

        if (nmhSetSnmpProxyContextEngineID
            (&ProxyName, &ProxyContextEngineID) == SNMP_FAILURE)
        {
            return Snmp3ProxyCreateError (&ProxyName, &pu4ErrCode);

        }

        if (nmhSetSnmpProxyContextName (&ProxyName, &ProxyContextName)
            == SNMP_FAILURE)
        {
            return Snmp3ProxyCreateError (&ProxyName, &pu4ErrCode);

        }
        if (nmhSetSnmpProxyTargetParamsIn
            (&ProxyName, &ProxyTargetParamsIn) == SNMP_FAILURE)
        {
            return Snmp3ProxyCreateError (&ProxyName, &pu4ErrCode);

        }
        if (nmhTestv2SnmpProxyRowStatus
            (pu4ErrCode, &ProxyName,
             (INT4) SNMP_ROWSTATUS_ACTIVE) == SNMP_FAILURE)
        {
            return Snmp3ProxyCreateError (&ProxyName, &pu4ErrCode);
        }

        if (nmhSetSnmpProxyRowStatus
            (&ProxyName, (INT4) SNMP_ROWSTATUS_ACTIVE) == SNMP_FAILURE)
        {
            return Snmp3ProxyCreateError (&ProxyName, &pu4ErrCode);

        }
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3PropProxyConfig                               */
/*                                                                           */
/*     DESCRIPTION      : Function to set Proxy Table entry creation err     */
/*                        and to delete the entry created                    */
/*                                                                           */
/*     INPUT            : pProxyName   - Proxy Name                          */
/*                        u4ProxyType  - Proxy Type                          */
/*                        pu1MibID     - MIB Id                              */
/*                        pu1ProxyTargetParamsIn   - Target paramIn          */
/*                        pu1ProxyTargetOut        - Target Out              */
/*                        u4StorageType            - Storage Type            */
/*                        pu4ErrCode    - Error code pointer                 */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
Snmp3PropProxyConfig (UINT1 *pu1ProxyName, UINT4 u4ProxyType, UINT1 *pu1MibID,
                      UINT1 *pu1ProxyTargetParamsIn, UINT1 *pu1ProxyTargetOut,
                      UINT4 u4StorageType, UINT4 *pu4ErrCode)
{

    UINT1               au1ProxyName[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1ProxyMibID[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               au1ProxyTargetParamsIn[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1ProxyTargetOut[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1ProxyClear[SNMP_MAX_ADMIN_STR_LENGTH];
    INT4                i4PreviousRowStatus = 0;
    tPrpProxyTableEntry *pProxyEntry = NULL;
    tSNMP_OCTET_STRING_TYPE ProxyName;
    tSNMP_OCTET_STRING_TYPE ProxyTargetParamsIn;
    tSNMP_OCTET_STRING_TYPE ProxyTargetOut;
    tSNMP_OCTET_STRING_TYPE ProxyClear;
    tSNMP_OID_TYPE     *pMibId = NULL;

    MEMSET (au1ProxyClear, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    ProxyClear.pu1_OctetList = au1ProxyClear;
    ProxyClear.i4_Length = SNMP_MAX_ADMIN_STR_LENGTH;

    pMibId = SNMP_AGT_GetOidFromString ((INT1 *) pu1MibID);
    if (pMibId == NULL)
    {
        *pu4ErrCode = CLI_SNMP3_MIBID_ERR;
        return SNMP_FAILURE;
    }

    MEMSET (au1ProxyName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    MEMSET (au1ProxyMibID, 0, SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET (au1ProxyTargetParamsIn, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    /* To remove klockworks warning */
    ProxyTargetParamsIn.i4_Length = 0;

    if (STRCMP (pu1ProxyName, "") != 0)
    {
        STRNCPY (au1ProxyName, pu1ProxyName, (sizeof (au1ProxyName) - 1));

        ProxyName.pu1_OctetList = au1ProxyName;
        ProxyName.i4_Length = (INT4) STRLEN (au1ProxyName);
    }

    if (STRCMP (pu1ProxyTargetParamsIn, "") != 0)
    {
        STRNCPY (au1ProxyTargetParamsIn, pu1ProxyTargetParamsIn,
                 (sizeof (au1ProxyTargetParamsIn) - 1));

        ProxyTargetParamsIn.pu1_OctetList = au1ProxyTargetParamsIn;
        ProxyTargetParamsIn.i4_Length = (INT4) STRLEN (au1ProxyTargetParamsIn);
    }

    MEMSET (au1ProxyTargetOut, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    STRNCPY (au1ProxyTargetOut, pu1ProxyTargetOut,
             (sizeof (au1ProxyTargetOut) - 1));

    ProxyTargetOut.pu1_OctetList = au1ProxyTargetOut;
    ProxyTargetOut.i4_Length = (INT4) STRLEN (au1ProxyTargetOut);

    /* if entry already exist */
    if ((pProxyEntry = SNMPGetPrpProxyEntryFromName (&ProxyName)) != NULL)
    {
        i4PreviousRowStatus = pProxyEntry->i4Status;
        if (nmhSetFsSnmpProxyMibRowStatus
            (&ProxyName, (INT4) SNMP_ROWSTATUS_NOTINSERVICE) == SNMP_FAILURE)
        {
            free_oid (pMibId);
            *pu4ErrCode = CLI_SNMP3_PROXY_ENTRY_MODIFICATION_ERR;
            return SNMP_FAILURE;

        }
        if (nmhTestv2FsSnmpProxyMibType
            (pu4ErrCode, &ProxyName, (INT4) u4ProxyType) == SNMP_FAILURE)
        {
            nmhSetFsSnmpProxyMibRowStatus (&ProxyName, i4PreviousRowStatus);
            free_oid (pMibId);
            *pu4ErrCode = CLI_SNMP3_PROXY_ENTRY_MODIFICATION_ERR;
            return SNMP_FAILURE;
        }
        if (nmhTestv2FsSnmpProxyMibId (pu4ErrCode, &ProxyName, pMibId) ==
            SNMP_FAILURE)
        {
            nmhSetFsSnmpProxyMibRowStatus (&ProxyName, i4PreviousRowStatus);
            free_oid (pMibId);
            *pu4ErrCode = CLI_SNMP3_PROXY_ENTRY_MODIFICATION_ERR;
            return SNMP_FAILURE;
        }
        if (nmhTestv2FsSnmpProxyMibTargetParamsIn
            (pu4ErrCode, &ProxyName, &ProxyTargetParamsIn) == SNMP_FAILURE)
        {
            nmhSetFsSnmpProxyMibRowStatus (&ProxyName, i4PreviousRowStatus);
            free_oid (pMibId);
            *pu4ErrCode = CLI_SNMP3_PROXY_ENTRY_MODIFICATION_ERR;
            return SNMP_FAILURE;
        }
        if (nmhTestv2FsSnmpProxyMibStorageType
            (pu4ErrCode, &ProxyName, (INT4) u4StorageType) == SNMP_FAILURE)
        {
            nmhSetFsSnmpProxyMibRowStatus (&ProxyName, i4PreviousRowStatus);
            free_oid (pMibId);
            *pu4ErrCode = CLI_SNMP3_PROXY_ENTRY_MODIFICATION_ERR;
            return SNMP_FAILURE;
        }
        if (nmhSetFsSnmpProxyMibSingleTargetOut (&ProxyName, &ProxyClear) ==
            SNMP_FAILURE)
        {
            nmhSetFsSnmpProxyMibRowStatus (&ProxyName, i4PreviousRowStatus);
            free_oid (pMibId);
            *pu4ErrCode = CLI_SNMP3_PROXY_ENTRY_MODIFICATION_ERR;
            return SNMP_FAILURE;
        }

        if (nmhSetFsSnmpProxyMibMultipleTargetOut (&ProxyName, &ProxyClear) ==
            SNMP_FAILURE)
        {
            nmhSetFsSnmpProxyMibRowStatus (&ProxyName, i4PreviousRowStatus);
            free_oid (pMibId);
            *pu4ErrCode = CLI_SNMP3_PROXY_ENTRY_MODIFICATION_ERR;
            return SNMP_FAILURE;
        }

        if ((u4ProxyType == PROXY_TYPE_READ)
            || (u4ProxyType == PROXY_TYPE_WRITE))
        {
            if (nmhTestv2FsSnmpProxyMibSingleTargetOut
                (pu4ErrCode, &ProxyName, &ProxyTargetOut) == SNMP_FAILURE)
            {
                nmhSetFsSnmpProxyMibRowStatus (&ProxyName, i4PreviousRowStatus);
                free_oid (pMibId);
                *pu4ErrCode = CLI_SNMP3_PROXY_ENTRY_MODIFICATION_ERR;
                return SNMP_FAILURE;
            }
            if (nmhSetFsSnmpProxyMibSingleTargetOut
                (&ProxyName, &ProxyTargetOut) == SNMP_FAILURE)
            {
                nmhSetFsSnmpProxyMibRowStatus (&ProxyName, i4PreviousRowStatus);
                free_oid (pMibId);
                *pu4ErrCode = CLI_SNMP3_PROXY_ENTRY_MODIFICATION_ERR;
                return SNMP_FAILURE;
            }
        }
        else
        {
            if (nmhTestv2FsSnmpProxyMibMultipleTargetOut
                (pu4ErrCode, &ProxyName, &ProxyTargetOut) == SNMP_FAILURE)
            {
                nmhSetFsSnmpProxyMibRowStatus (&ProxyName, i4PreviousRowStatus);
                free_oid (pMibId);
                *pu4ErrCode = CLI_SNMP3_PROXY_ENTRY_MODIFICATION_ERR;
                return SNMP_FAILURE;
            }
            if (nmhSetFsSnmpProxyMibMultipleTargetOut
                (&ProxyName, &ProxyTargetOut) == SNMP_FAILURE)
            {
                nmhSetFsSnmpProxyMibRowStatus (&ProxyName, i4PreviousRowStatus);
                free_oid (pMibId);
                *pu4ErrCode = CLI_SNMP3_PROXY_ENTRY_MODIFICATION_ERR;
                return SNMP_FAILURE;
            }
        }

        /*modify  storage type. */
        if ((INT4) u4StorageType != pProxyEntry->i4Storage)
        {
            if (nmhSetFsSnmpProxyMibStorageType
                (&ProxyName, (INT4) u4StorageType) == SNMP_FAILURE)
            {
                nmhSetFsSnmpProxyMibRowStatus (&ProxyName, i4PreviousRowStatus);
                free_oid (pMibId);
                *pu4ErrCode = CLI_SNMP3_PROXY_ENTRY_MODIFICATION_ERR;
                return SNMP_FAILURE;
            }
        }

        /*set all the fields */

        if ((INT4) u4ProxyType != pProxyEntry->i4PrpPrxMibType)
        {
            if (nmhSetFsSnmpProxyMibType (&ProxyName, (INT4) u4ProxyType) ==
                SNMP_FAILURE)
            {
                nmhSetFsSnmpProxyMibRowStatus (&ProxyName, i4PreviousRowStatus);
                free_oid (pMibId);
                *pu4ErrCode = CLI_SNMP3_PROXY_ENTRY_MODIFICATION_ERR;
                return SNMP_FAILURE;
            }
        }

        if (nmhSetFsSnmpProxyMibId (&ProxyName, pMibId) == SNMP_FAILURE)
        {
            nmhSetFsSnmpProxyMibRowStatus (&ProxyName, i4PreviousRowStatus);
            free_oid (pMibId);
            *pu4ErrCode = CLI_SNMP3_PROXY_ENTRY_MODIFICATION_ERR;
            return SNMP_FAILURE;

        }

        if (SNMPCompareOctetString
            (&ProxyTargetParamsIn,
             &(pProxyEntry->PrpPrxTargetParamsIn)) != SNMP_EQUAL)
        {
            if (nmhSetFsSnmpProxyMibTargetParamsIn (&ProxyName, &ProxyClear) ==
                SNMP_FAILURE)
            {
                nmhSetFsSnmpProxyMibRowStatus (&ProxyName, i4PreviousRowStatus);
                free_oid (pMibId);
                *pu4ErrCode = CLI_SNMP3_PROXY_ENTRY_MODIFICATION_ERR;
                return SNMP_FAILURE;

            }
            if (nmhSetFsSnmpProxyMibTargetParamsIn
                (&ProxyName, &ProxyTargetParamsIn) == SNMP_FAILURE)
            {
                nmhSetFsSnmpProxyMibRowStatus (&ProxyName, i4PreviousRowStatus);
                free_oid (pMibId);
                *pu4ErrCode = CLI_SNMP3_PROXY_ENTRY_MODIFICATION_ERR;
                return SNMP_FAILURE;
            }
        }
        if (nmhTestv2FsSnmpProxyMibRowStatus
            (pu4ErrCode, &ProxyName,
             (INT4) SNMP_ROWSTATUS_ACTIVE) == SNMP_FAILURE)
        {
            nmhSetFsSnmpProxyMibRowStatus (&ProxyName, i4PreviousRowStatus);
            free_oid (pMibId);
            *pu4ErrCode = CLI_SNMP3_PROXY_ENTRY_MODIFICATION_ERR;
            return SNMP_FAILURE;
        }

        if (nmhSetFsSnmpProxyMibRowStatus
            (&ProxyName, (INT4) SNMP_ROWSTATUS_ACTIVE) == SNMP_FAILURE)
        {
            nmhSetFsSnmpProxyMibRowStatus (&ProxyName, i4PreviousRowStatus);
            free_oid (pMibId);
            *pu4ErrCode = CLI_SNMP3_PROXY_ENTRY_MODIFICATION_ERR;
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhSetFsSnmpProxyMibRowStatus
            (&ProxyName, (INT4) SNMP_ROWSTATUS_CREATEANDWAIT) == SNMP_FAILURE)
        {
            *pu4ErrCode = CLI_SNMP3_PROXY_ENTRY_CREATION_ERR;
            free_oid (pMibId);
            return SNMP_FAILURE;
        }

        if (nmhTestv2FsSnmpProxyMibType
            (pu4ErrCode, &ProxyName, (INT4) u4ProxyType) == SNMP_FAILURE)
        {
            free_oid (pMibId);
            return Snmp3PropProxyCreateError (&ProxyName, &pu4ErrCode);
        }
        if (nmhTestv2FsSnmpProxyMibId (pu4ErrCode, &ProxyName, pMibId) ==
            SNMP_FAILURE)
        {
            free_oid (pMibId);
            return Snmp3PropProxyCreateError (&ProxyName, &pu4ErrCode);

        }
        if (nmhTestv2FsSnmpProxyMibTargetParamsIn
            (pu4ErrCode, &ProxyName, &ProxyTargetParamsIn) == SNMP_FAILURE)
        {
            free_oid (pMibId);
            return Snmp3PropProxyCreateError (&ProxyName, &pu4ErrCode);

        }
        if (nmhTestv2FsSnmpProxyMibStorageType
            (pu4ErrCode, &ProxyName, (INT4) u4StorageType) == SNMP_FAILURE)
        {
            free_oid (pMibId);
            return Snmp3PropProxyCreateError (&ProxyName, &pu4ErrCode);

        }

        if ((u4ProxyType == PROXY_TYPE_READ)
            || (u4ProxyType == PROXY_TYPE_WRITE))
        {
            if (nmhTestv2FsSnmpProxyMibSingleTargetOut
                (pu4ErrCode, &ProxyName, &ProxyTargetOut) == SNMP_FAILURE)
            {
                free_oid (pMibId);
                return Snmp3PropProxyCreateError (&ProxyName, &pu4ErrCode);

            }
            if (nmhSetFsSnmpProxyMibSingleTargetOut
                (&ProxyName, &ProxyTargetOut) == SNMP_FAILURE)
            {
                free_oid (pMibId);
                return Snmp3PropProxyCreateError (&ProxyName, &pu4ErrCode);

            }
        }
        else if ((u4ProxyType == PROXY_TYPE_TRAP)
                 || (u4ProxyType == PROXY_TYPE_INFORM))
        {
            if (nmhTestv2FsSnmpProxyMibMultipleTargetOut
                (pu4ErrCode, &ProxyName, &ProxyTargetOut) == SNMP_FAILURE)
            {
                free_oid (pMibId);
                return Snmp3PropProxyCreateError (&ProxyName, &pu4ErrCode);
            }
            if (nmhSetFsSnmpProxyMibMultipleTargetOut
                (&ProxyName, &ProxyTargetOut) == SNMP_FAILURE)
            {
                free_oid (pMibId);
                return Snmp3PropProxyCreateError (&ProxyName, &pu4ErrCode);
            }
        }
        else
        {
            free_oid (pMibId);
            return Snmp3PropProxyCreateError (&ProxyName, &pu4ErrCode);
        }
        if (nmhSetFsSnmpProxyMibType (&ProxyName, (INT4) u4ProxyType) ==
            SNMP_FAILURE)
        {
            free_oid (pMibId);
            return Snmp3PropProxyCreateError (&ProxyName, &pu4ErrCode);
        }

        if (nmhSetFsSnmpProxyMibId (&ProxyName, pMibId) == SNMP_FAILURE)
        {
            free_oid (pMibId);
            return Snmp3PropProxyCreateError (&ProxyName, &pu4ErrCode);
        }

        if (nmhSetFsSnmpProxyMibTargetParamsIn
            (&ProxyName, &ProxyTargetParamsIn) == SNMP_FAILURE)
        {
            free_oid (pMibId);
            return Snmp3PropProxyCreateError (&ProxyName, &pu4ErrCode);
        }
        if (nmhSetFsSnmpProxyMibStorageType (&ProxyName, (INT4) u4StorageType)
            == SNMP_FAILURE)
        {
            free_oid (pMibId);
            return Snmp3PropProxyCreateError (&ProxyName, &pu4ErrCode);
        }
        if (nmhTestv2FsSnmpProxyMibRowStatus
            (pu4ErrCode, &ProxyName,
             (INT4) SNMP_ROWSTATUS_ACTIVE) == SNMP_FAILURE)
        {
            free_oid (pMibId);
            return Snmp3PropProxyCreateError (&ProxyName, &pu4ErrCode);
        }

        if (nmhSetFsSnmpProxyMibRowStatus
            (&ProxyName, (INT4) SNMP_ROWSTATUS_ACTIVE) == SNMP_FAILURE)
        {
            free_oid (pMibId);
            return Snmp3PropProxyCreateError (&ProxyName, &pu4ErrCode);
        }
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CommunityConfig                               */
/*                                                                           */
/*     DESCRIPTION      : Function to configure the Community Table          */
/*                                                                           */
/*     INPUT            : pu1CommIndex   - Community Index                   */
/*                        pu1CommName    - Community Name                    */
/*                        pu1SecName     - Security string                   */
/*                        pu1ContextName - View identifier                   */
/*                        pu1Tag         - Transport tag identifier          */
/*                        u4StorageType  - Storage type in memory            */
/*                        pu4ErrCode    - Error code pointer                 */
/*                                                                           */
/*     OUTPUT           : None.                                              */
/*                                                                           */
/*     RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
Snmp3CommunityConfig (UINT1 *pu1CommIndex, UINT1 *pu1CommName,
                      UINT1 *pu1SecName, UINT1 *pu1ContextName,
                      UINT1 *pu1Tag, UINT4 u4StorageType, UINT1 *pu1EngineId,
                      UINT4 *pu4ErrCode)
{
    UINT4               u4ErrorCode;
    INT4                i4RetVal;
    UINT1               u1ContextFlag = SNMP_FALSE;
    UINT1               u1TagFlag = SNMP_FALSE;
    UINT1               au1CommunityIndex[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1              *pu1CommunityName = NULL;
    UINT1               au1SecurityName[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1ContextName[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1ClearContext[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1TagId[MAX_STR_OCTET_VAL];
    UINT1               au1ClearTag[MAX_STR_OCTET_VAL];
    UINT1               au1EngineId[SNMP_MAX_STR_ENGINEID_LEN];

    tCommunityMappingEntry *pCommEntry = NULL;
    tSNMP_OCTET_STRING_TYPE CommIndex;
    tSNMP_OCTET_STRING_TYPE CommName;
    tSNMP_OCTET_STRING_TYPE SecurityName;
    tSNMP_OCTET_STRING_TYPE ContextName;
    tSNMP_OCTET_STRING_TYPE ClearContext;
    tSNMP_OCTET_STRING_TYPE TagId;
    tSNMP_OCTET_STRING_TYPE ClearTag;
    tSNMP_OCTET_STRING_TYPE EngineId;

    MEMSET (au1EngineId, 0, SNMP_MAX_STR_ENGINEID_LEN);
    EngineId.pu1_OctetList = au1EngineId;

    /* verify validity of inputs */
    if ((SnmpCheckString (pu1CommIndex) == SNMP_FAILURE) ||
        (SnmpCheckString (pu1CommName) == SNMP_FAILURE) ||
        (SnmpCheckString (pu1SecName) == SNMP_FAILURE) ||
        (SnmpCheckString (pu1ContextName) == SNMP_FAILURE) ||
        (SnmpCheckString (pu1Tag) == SNMP_FAILURE))
    {
        *pu4ErrCode = CLI_SNMP3_INVALID_INPUT_ERR;
        return SNMP_FAILURE;
    }
    if (STRCMP (pu1EngineId, "") != 0)
    {

        if (SnmpConvertStringToOctet ((INT1 *) pu1EngineId, &EngineId) ==
            SNMP_FAILURE)
        {
            *pu4ErrCode = CLI_SNMP3_ENGINE_ID_ERR;
            return SNMP_FAILURE;
            return CLI_FAILURE;
        }

    }
    else
    {
        SNMPCopyOctetString (&EngineId, &gSnmpEngineID);
    }

    MEMSET (au1CommunityIndex, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    STRNCPY (au1CommunityIndex, pu1CommIndex, (sizeof (au1CommunityIndex) - 1));

    CommIndex.pu1_OctetList = au1CommunityIndex;
    CommIndex.i4_Length = (INT4) STRLEN (au1CommunityIndex);

    ContextName.i4_Length = 0;    /*kloc */
    TagId.i4_Length = 0;        /*kloc */

    if ((STRCMP (pu1ContextName, "none") != 0) &&
        (STRCMP (pu1ContextName, "clear") != 0))
    {
        MEMSET (au1ContextName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
        STRNCPY (au1ContextName, pu1ContextName, (sizeof (au1ContextName) - 1));

        ContextName.pu1_OctetList = au1ContextName;
        ContextName.i4_Length = (INT4) STRLEN (au1ContextName);

        u1ContextFlag = SNMP_TRUE;
    }

    if ((STRCMP (pu1Tag, "none") != 0) && (STRCMP (pu1Tag, "clear") != 0))
    {
        MEMSET (au1TagId, 0, MAX_STR_OCTET_VAL);
        STRNCPY (au1TagId, pu1Tag, (sizeof (au1TagId) - 1));

        TagId.pu1_OctetList = au1TagId;
        TagId.i4_Length = (INT4) STRLEN (au1TagId);

        u1TagFlag = SNMP_TRUE;
    }

    /* check if an entry exists already */
    if ((pCommEntry = SNMPGetCommunityEntry (&CommIndex)) != NULL)
    {
        nmhSetSnmpCommunityStatus (&CommIndex, SNMP_ROWSTATUS_NOTINSERVICE);

        if (SNMPCompareOctetString
            (&EngineId, &(pCommEntry->ContextEngineID)) != SNMP_EQUAL)
        {
            if (nmhTestv2SnmpCommunityContextEngineID
                (&u4ErrorCode, &CommIndex, &EngineId) == SNMP_FAILURE)
            {
                return (Snmp3CommunityModifyError (pCommEntry, &pu4ErrCode));
            }
            if (nmhSetSnmpCommunityContextEngineID (&CommIndex, &EngineId) ==
                SNMP_FAILURE)
            {
                return (Snmp3CommunityModifyError (pCommEntry, &pu4ErrCode));
            }
        }

        /* entry exists - if any value in table is changed, modify */
        if (STRCMP (pu1ContextName, "clear") == 0)
        {
            /* Context cleared, clear existing value */
            MEMSET (au1ClearContext, 0, SNMP_MAX_ADMIN_STR_LENGTH);

            ClearContext.pu1_OctetList = au1ClearContext;
            ClearContext.i4_Length = SNMP_MAX_ADMIN_STR_LENGTH;

            if (nmhSetSnmpCommunityContextName (&CommIndex, &ClearContext)
                == SNMP_FAILURE)
            {
                return (Snmp3CommunityModifyError (pCommEntry, &pu4ErrCode));
            }

        }
        else
        {
            if ((STRCMP (pu1ContextName, "none") != 0) &&
                (SNMPCompareOctetString
                 (&ContextName, &(pCommEntry->ContextName)) != SNMP_EQUAL))
            {
                /* Context modified, test new value */
                if (nmhTestv2SnmpCommunityContextName (&u4ErrorCode,
                                                       &CommIndex,
                                                       &ContextName)
                    == SNMP_FAILURE)
                {
                    return (Snmp3CommunityModifyError (pCommEntry,
                                                       &pu4ErrCode));
                }

                /* clear previous context name */
                MEMSET (au1ClearContext, 0, SNMP_MAX_ADMIN_STR_LENGTH);

                ClearContext.pu1_OctetList = au1ContextName;
                ClearContext.i4_Length = SNMP_MAX_ADMIN_STR_LENGTH;

                if (nmhSetSnmpCommunityContextName (&CommIndex, &ClearContext)
                    == SNMP_FAILURE)
                {
                    return (Snmp3CommunityModifyError (pCommEntry,
                                                       &pu4ErrCode));
                }

                /* save new context name */
                if (nmhSetSnmpCommunityContextName (&CommIndex, &ContextName)
                    == SNMP_FAILURE)
                {
                    return (Snmp3CommunityModifyError (pCommEntry,
                                                       &pu4ErrCode));
                }
            }
        }

        if (STRCMP (pu1Tag, "clear") == 0)
        {
            /* Tag cleared, clear existing value */
            MEMSET (au1ClearTag, 0, MAX_STR_OCTET_VAL);

            ClearTag.pu1_OctetList = au1ClearTag;
            ClearTag.i4_Length = MAX_STR_OCTET_VAL;

            if (nmhSetSnmpCommunityTransportTag (&CommIndex, &ClearTag)
                == SNMP_FAILURE)
            {
                return (Snmp3CommunityModifyError (pCommEntry, &pu4ErrCode));
            }
        }
        else
        {
            if ((STRCMP (pu1Tag, "none") != 0) &&
                (SNMPCompareOctetString (&TagId,
                                         &pCommEntry->TransTag) != SNMP_EQUAL))
            {
                /* Tag identifier modified, test new value */
                if (nmhTestv2SnmpCommunityTransportTag (&u4ErrorCode,
                                                        &CommIndex,
                                                        &TagId) == SNMP_FAILURE)
                {
                    return (Snmp3CommunityModifyError (pCommEntry,
                                                       &pu4ErrCode));
                }

                /* clear previous tag */
                MEMSET (au1ClearTag, 0, MAX_STR_OCTET_VAL);

                ClearTag.pu1_OctetList = au1ClearTag;
                ClearTag.i4_Length = MAX_STR_OCTET_VAL;

                if (nmhSetSnmpCommunityTransportTag (&CommIndex, &ClearTag)
                    == SNMP_FAILURE)
                {
                    return (Snmp3CommunityModifyError (pCommEntry,
                                                       &pu4ErrCode));
                }

                /* save new tag */
                if (nmhSetSnmpCommunityTransportTag (&CommIndex, &TagId)
                    == SNMP_FAILURE)
                {
                    return (Snmp3CommunityModifyError (pCommEntry,
                                                       &pu4ErrCode));
                }
            }
        }

        if ((u4StorageType != 0) &&
            ((INT4) u4StorageType != pCommEntry->i4Storage))
        {
            /* Storage Type modified, save new value */
            if (nmhSetSnmpCommunityStorageType (&CommIndex,
                                                (INT4) u4StorageType)
                == SNMP_FAILURE)
            {
                return (Snmp3CommunityModifyError (pCommEntry, &pu4ErrCode));
            }
        }

        pu1CommunityName = MemAllocMemBlk (gSnmpOctetListPoolId);
        if (pu1CommunityName == NULL)
        {
            return SNMP_FAILURE;
        }
        MEMSET (pu1CommunityName, 0, SNMP_MAX_OCTETSTRING_SIZE);
        STRNCPY (pu1CommunityName, pu1CommName,
                 (sizeof (tSnmpOctetListBlock) - 1));

        CommName.pu1_OctetList = pu1CommunityName;
        CommName.i4_Length = (INT4) STRLEN (pu1CommunityName);

        if (SNMPCompareOctetString (&CommName, &pCommEntry->CommunityName)
            != SNMP_EQUAL)
        {
            if (nmhTestv2SnmpCommunityName (&u4ErrorCode, &CommIndex,
                                            &CommName) == SNMP_FAILURE)
            {
                i4RetVal = Snmp3CommunityCreateError (&CommIndex, &pu4ErrCode);
                MemReleaseMemBlock (gSnmpOctetListPoolId, pu1CommunityName);
                return i4RetVal;
            }
            if (nmhSetSnmpCommunityName (&CommIndex, &CommName) == SNMP_FAILURE)
            {
                i4RetVal = Snmp3CommunityCreateError (&CommIndex, &pu4ErrCode);
                MemReleaseMemBlock (gSnmpOctetListPoolId, pu1CommunityName);
                return i4RetVal;
            }
        }
        MemReleaseMemBlock (gSnmpOctetListPoolId, pu1CommunityName);

        MEMSET (au1SecurityName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
        STRNCPY (au1SecurityName, pu1SecName, (sizeof (au1SecurityName) - 1));

        SecurityName.pu1_OctetList = au1SecurityName;
        SecurityName.i4_Length = (INT4) STRLEN (pu1SecName);

        if (SNMPCompareOctetString (&SecurityName, &pCommEntry->SecurityName)
            != SNMP_EQUAL)
        {
            if (nmhTestv2SnmpCommunitySecurityName (&u4ErrorCode, &CommIndex,
                                                    &SecurityName) ==
                SNMP_FAILURE)
            {
                return (Snmp3CommunityCreateError (&CommIndex, &pu4ErrCode));
            }

            if (nmhSetSnmpCommunitySecurityName (&CommIndex, &SecurityName)
                == SNMP_FAILURE)
            {
                return (Snmp3CommunityCreateError (&CommIndex, &pu4ErrCode));
            }
        }

        nmhSetSnmpCommunityStatus (&CommIndex, SNMP_ROWSTATUS_ACTIVE);
        /* Entry modified */
    }
    else
    {
        /* entry does not exist - ADD */
        if (SNMP_FAILURE == nmhTestv2SnmpCommunityStatus
            (pu4ErrCode, &CommIndex, SNMP_ROWSTATUS_CREATEANDWAIT))
        {
            *pu4ErrCode = CLI_SNMP3_NEW_ENTRY_ERR;
            return SNMP_FAILURE;
        }

        if (nmhSetSnmpCommunityStatus (&CommIndex,
                                       SNMP_ROWSTATUS_CREATEANDWAIT)
            == SNMP_FAILURE)
        {
            *pu4ErrCode = CLI_SNMP3_NEW_ENTRY_ERR;
            return SNMP_FAILURE;
        }

        pu1CommunityName = MemAllocMemBlk (gSnmpOctetListPoolId);
        if (pu1CommunityName == NULL)
        {
            return SNMP_FAILURE;
        }

        MEMSET (pu1CommunityName, 0, SNMP_MAX_OCTETSTRING_SIZE);
        STRNCPY (pu1CommunityName, pu1CommName,
                 (sizeof (tSnmpOctetListBlock) - 1));

        CommName.pu1_OctetList = pu1CommunityName;
        CommName.i4_Length = (INT4) STRLEN (pu1CommunityName);

        MEMSET (au1SecurityName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
        STRNCPY (au1SecurityName, pu1SecName, (sizeof (au1SecurityName) - 1));

        SecurityName.pu1_OctetList = au1SecurityName;
        SecurityName.i4_Length = (INT4) STRLEN (au1SecurityName);

        /* Test Inputs */
        if (nmhTestv2SnmpCommunityName (&u4ErrorCode, &CommIndex, &CommName)
            == SNMP_FAILURE)
        {
            i4RetVal = Snmp3CommunityCreateError (&CommIndex, &pu4ErrCode);
            MemReleaseMemBlock (gSnmpOctetListPoolId, pu1CommunityName);
            return i4RetVal;
        }

        if (nmhTestv2SnmpCommunitySecurityName (&u4ErrorCode, &CommIndex,
                                                &SecurityName) == SNMP_FAILURE)
        {
            i4RetVal = Snmp3CommunityCreateError (&CommIndex, &pu4ErrCode);
            MemReleaseMemBlock (gSnmpOctetListPoolId, pu1CommunityName);
            return i4RetVal;
        }

        if (u1ContextFlag == SNMP_TRUE)
        {
            if (nmhTestv2SnmpCommunityContextName (&u4ErrorCode, &CommIndex,
                                                   &ContextName) ==
                SNMP_FAILURE)
            {
                i4RetVal = Snmp3CommunityCreateError (&CommIndex, &pu4ErrCode);
                MemReleaseMemBlock (gSnmpOctetListPoolId, pu1CommunityName);
                return i4RetVal;
            }
        }

        if (u1TagFlag == SNMP_TRUE)
        {
            if (nmhTestv2SnmpCommunityTransportTag (&u4ErrorCode, &CommIndex,
                                                    &TagId) == SNMP_FAILURE)
            {
                i4RetVal = Snmp3CommunityCreateError (&CommIndex, &pu4ErrCode);
                MemReleaseMemBlock (gSnmpOctetListPoolId, pu1CommunityName);
                return i4RetVal;
            }

        }

        /* Set Inputs */
        if (nmhSetSnmpCommunityName (&CommIndex, &CommName) == SNMP_FAILURE)
        {
            i4RetVal = Snmp3CommunityCreateError (&CommIndex, &pu4ErrCode);
            MemReleaseMemBlock (gSnmpOctetListPoolId, pu1CommunityName);
            return i4RetVal;
        }
        MemReleaseMemBlock (gSnmpOctetListPoolId, pu1CommunityName);

        if (nmhSetSnmpCommunitySecurityName (&CommIndex, &SecurityName)
            == SNMP_FAILURE)
        {
            return (Snmp3CommunityCreateError (&CommIndex, &pu4ErrCode));
        }
        if (nmhTestv2SnmpCommunityContextEngineID
            (&u4ErrorCode, &CommIndex, &EngineId) == SNMP_FAILURE)
        {
            return (Snmp3CommunityCreateError (&CommIndex, &pu4ErrCode));
        }

        if (nmhSetSnmpCommunityContextEngineID (&CommIndex, &EngineId)
            == SNMP_FAILURE)
        {
            return (Snmp3CommunityCreateError (&CommIndex, &pu4ErrCode));
        }

        if (u1ContextFlag == SNMP_TRUE)
        {
            if (nmhSetSnmpCommunityContextName (&CommIndex, &ContextName)
                == SNMP_FAILURE)
            {
                return (Snmp3CommunityCreateError (&CommIndex, &pu4ErrCode));
            }
        }

        if (u1TagFlag == SNMP_TRUE)
        {
            if (nmhSetSnmpCommunityTransportTag (&CommIndex, &TagId)
                == SNMP_FAILURE)
            {
                return (Snmp3CommunityCreateError (&CommIndex, &pu4ErrCode));
            }
        }

        if (u4StorageType == 0)
        {
            /* Storage type not given - make default as non-volatile type */
            u4StorageType = SNMP3_STORAGE_TYPE_NONVOLATILE;
        }

        if (nmhSetSnmpCommunityStorageType (&CommIndex, (INT4) u4StorageType)
            == SNMP_FAILURE)
        {
            return (Snmp3CommunityCreateError (&CommIndex, &pu4ErrCode));
        }

        nmhSetSnmpCommunityStatus (&CommIndex, SNMP_ROWSTATUS_ACTIVE);
        /* Entry created */
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3DefCommunityConfig                            */
/*                                                                           */
/*     DESCRIPTION      : Function to configure a default community entry    */
/*                        during initialization                              */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
Snmp3DefCommunityConfig ()
{
    UINT1               au1CommunityIndex[SNMP_MAX_ADMIN_STR_LENGTH] =
        SNMP_DEF_COMMUNITY_1;
    UINT1               au1CommunityName[SNMP_MAX_ADMIN_STR_LENGTH] =
        SNMP_DEF_COMMUNITY_1;
    UINT1               au1SecName[SNMP_MAX_ADMIN_STR_LENGTH] = "none";
    UINT1               au1ContextName[SNMP_MAX_ADMIN_STR_LENGTH] = "none";
    UINT1               au1Tag[SNMP_MAX_ADMIN_STR_LENGTH] = "none";

    UINT4               u4StorageType = SNMP3_STORAGE_TYPE_NONVOLATILE;
    UINT1               au1EngineId[SNMP_MAX_STR_ENGINEID_LEN] = "";
    UINT4               u4ErrCode;

    UNUSED_PARAM (u4ErrCode);

    /* If Community Control is set, Default communities cannot be created */
    if (SNMP_FALSE == gi4SnmpCommunityCtrl)
    {
        /* Create default entry */
        Snmp3CommunityConfig (au1CommunityIndex, au1CommunityName, au1SecName,
                              au1ContextName, au1Tag, u4StorageType,
                              au1EngineId, &u4ErrCode);
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3Def1CommunityConfig                           */
/*                                                                           */
/*     DESCRIPTION      : Function to configure a default community entry    */
/*                        during initialization                              */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
Snmp3Def1CommunityConfig ()
{
    UINT1               au1CommunityIndex[SNMP_MAX_ADMIN_STR_LENGTH] =
        SNMP_DEF_COMMUNITY_2;
    UINT1               au1CommunityName[SNMP_MAX_ADMIN_STR_LENGTH] =
        SNMP_DEF_COMMUNITY_2;
    UINT1               au1SecName[SNMP_MAX_ADMIN_STR_LENGTH] = "none";
    UINT1               au1ContextName[SNMP_MAX_ADMIN_STR_LENGTH] = "none";
    UINT1               au1Tag[SNMP_MAX_ADMIN_STR_LENGTH] = "none";
    UINT1               au1EngineId[SNMP_MAX_STR_ENGINEID_LEN] = "";
    UINT4               u4StorageType = SNMP3_STORAGE_TYPE_NONVOLATILE;
    UINT4               u4ErrCode;

    UNUSED_PARAM (u4ErrCode);

    /* If Community Control is set, Default communities cannot be created */
    if (SNMP_FALSE == gi4SnmpCommunityCtrl)
    {
        /* Create default entry */
        Snmp3CommunityConfig (au1CommunityIndex, au1CommunityName, au1SecName,
                              au1ContextName, au1Tag, u4StorageType,
                              au1EngineId, &u4ErrCode);
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CommunityModifyError                          */
/*                                                                           */
/*     DESCRIPTION      : Function to set Community Table modification error */
/*                        and set the row status of the entry to ACTIVE      */
/*                        the entry is set to ACTIVE as modification to the  */
/*                        existing entry failed                              */
/*                                                                           */
/*     INPUT            : pCommEntry - pointer to Community table entry      */
/*                        pu4ErrCode    - Error code pointer                 */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT1
Snmp3CommunityModifyError (tCommunityMappingEntry * pCommEntry,
                           UINT4 **pu4ErrCode)
{
    **pu4ErrCode = CLI_SNMP3_ENTRY_MODIFICATION_ERR;
    pCommEntry->i4Status = SNMP_ACTIVE;

    return SNMP_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3ProxyModifyError                          */
/*                                                                           */
/*     DESCRIPTION      : Function to set Proxy Table modification error */
/*                        and set the row status of the entry to ACTIVE      */
/*                        the entry is set to ACTIVE as modification to the  */
/*                        existing entry failed                              */
/*                                                                           */
/*     INPUT            : pProxyTableEntry - pointer to Proxy table entry      */
/*                        pu4ErrCode    - Error code pointer                 */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

INT1
Snmp3ProxyModifyError (tProxyTableEntry * pProxyEntry, UINT4 **pu4ErrCode)
{
    UNUSED_PARAM (pProxyEntry);
    **pu4ErrCode = CLI_SNMP3_PROXY_ENTRY_MODIFICATION_ERR;

    return SNMP_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3PropProxyCreateError                          */
/*                                                                           */
/*     Description      : function to set proxy table modification error     */
/*                        and set the row status of the entry to destroy     */
/*                                                                           */
/*     Input            : pProxyName    - Proxy Name                         */
/*                        pu4errcode    - error code pointer                 */
/*                                                                           */
/*     Output           : none                                               */
/*                                                                           */
/*     Returns          : SNMP_SUCCESS/SNMP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

INT1
Snmp3PropProxyCreateError (tSNMP_OCTET_STRING_TYPE * pProxyName,
                           UINT4 **pu4ErrCode)
{
    nmhSetFsSnmpProxyMibRowStatus (pProxyName, SNMP_ROWSTATUS_DESTROY);
    **pu4ErrCode = CLI_SNMP3_PROXY_ENTRY_CREATION_ERR;

    return SNMP_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3ProxyCreateError                          */
/*                                                                           */
/*     DESCRIPTION      : Function to set Proxy Table entry creation err */
/*                        and to delete the entry created                    */
/*                                                                           */
/*     INPUT            : pProxyName - Community Index                       */
/*                        pu4ErrCode    - Error code pointer                 */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT1
Snmp3ProxyCreateError (tSNMP_OCTET_STRING_TYPE * pProxyName, UINT4 **pu4ErrCode)
{

    **pu4ErrCode = CLI_SNMP3_PROXY_ENTRY_CREATION_ERR;
    nmhSetSnmpProxyRowStatus (pProxyName, (INT4) SNMP_ROWSTATUS_DESTROY);
    return SNMP_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3CommunityCreateError                          */
/*                                                                           */
/*     DESCRIPTION      : Function to set Community Table entry creation err */
/*                        and to delete the entry created                    */
/*                                                                           */
/*     INPUT            : pCommIndex - Community Index                       */
/*                        pu4ErrCode    - Error code pointer                 */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT1
Snmp3CommunityCreateError (tSNMP_OCTET_STRING_TYPE * pCommIndex,
                           UINT4 **pu4ErrCode)
{
    **pu4ErrCode = CLI_SNMP3_ENTRY_CREATION_ERR;

    if (SNMPDeleteCommunityEntry (pCommIndex) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3NotifyConfig                                  */
/*                                                                           */
/*     DESCRIPTION      : Function to configure the Notify Name              */
/*                                                                           */
/*     INPUT            : pu1NotifyName -  Notify Name                       */
/*                        pu1Tag        -  Notify Tag value                  */
/*                        u4NotifyType  -  Notification type                 */
/*                        u4StorageType - Storage type in memory             */
/*                        pu4ErrCode    - Error code pointer                 */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
Snmp3NotifyConfig (UINT1 *pu1NotifyName, UINT1 *pu1Tag, UINT4 u4NotifyType,
                   UINT4 u4StorageType, UINT4 *pu4ErrCode)
{
    UINT4               u4ErrorCode;
    UINT1               u1TagFlag = SNMP_FALSE;
    UINT1               au1NotifyName[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1NotifyTag[MAX_STR_OCTET_VAL];
    UINT1               au1ClearTag[MAX_STR_OCTET_VAL];

    tSNMP_OCTET_STRING_TYPE NotifyName;
    tSNMP_OCTET_STRING_TYPE NotifyTag;
    tSNMP_OCTET_STRING_TYPE ClearTag;
    tSnmpNotifyEntry   *pNotifyEntry = NULL;

    NotifyTag.i4_Length = 0;    /*kloc */
    /* verify validity of inputs */
    if ((SnmpCheckString (pu1NotifyName) == SNMP_FAILURE) ||
        (SnmpCheckString (pu1Tag) == SNMP_FAILURE))
    {
        *pu4ErrCode = CLI_SNMP3_INVALID_INPUT_ERR;
        return SNMP_FAILURE;
    }

    MEMSET (au1NotifyName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    STRNCPY (au1NotifyName, pu1NotifyName, (sizeof (au1NotifyName) - 1));

    NotifyName.pu1_OctetList = au1NotifyName;
    NotifyName.i4_Length = (INT4) STRLEN (au1NotifyName);

    if ((STRCMP (pu1Tag, "none") != 0) && (STRCMP (pu1Tag, "clear") != 0))
    {
        MEMSET (au1NotifyTag, 0, MAX_STR_OCTET_VAL);
        STRNCPY (au1NotifyTag, pu1Tag, (sizeof (au1NotifyTag) - 1));

        NotifyTag.pu1_OctetList = au1NotifyTag;
        NotifyTag.i4_Length = (INT4) STRLEN (au1NotifyTag);

        u1TagFlag = SNMP_TRUE;
    }

    /* check if an entry exists already */
    if ((pNotifyEntry = SNMPGetNotifyEntry (&NotifyName)) != NULL)
    {
        nmhSetSnmpNotifyRowStatus (&NotifyName, SNMP_ROWSTATUS_NOTINSERVICE);

        /* entry exists - if any value in table is changed, modify */
        if (STRCMP (pu1Tag, "clear") == 0)
        {
            /* Tag cleared, clear existing value */
            MEMSET (au1ClearTag, 0, MAX_STR_OCTET_VAL);

            ClearTag.pu1_OctetList = au1ClearTag;
            ClearTag.i4_Length = MAX_STR_OCTET_VAL;

            if (nmhSetSnmpNotifyTag (&NotifyName, &ClearTag) == SNMP_FAILURE)
            {
                return (Snmp3NotifModifyError (&NotifyName, &pu4ErrCode));
            }
        }
        else
        {
            if ((STRCMP (pu1Tag, "none") != 0) &&
                (SNMPCompareOctetString
                 (&NotifyTag, &(pNotifyEntry->NotifyTag)) != SNMP_EQUAL))
            {
                /* Tag identifier modified, test new value */
                if (nmhTestv2SnmpNotifyTag (&u4ErrorCode, &NotifyName,
                                            &NotifyTag) == SNMP_FAILURE)
                {
                    return (Snmp3NotifModifyError (&NotifyName, &pu4ErrCode));
                }

                /* clear previous tag */
                MEMSET (au1ClearTag, 0, MAX_STR_OCTET_VAL);

                ClearTag.pu1_OctetList = au1ClearTag;
                ClearTag.i4_Length = MAX_STR_OCTET_VAL;

                if (nmhSetSnmpNotifyTag (&NotifyName, &ClearTag)
                    == SNMP_FAILURE)
                {
                    return (Snmp3NotifModifyError (&NotifyName, &pu4ErrCode));
                }

                /* save new tag */
                if (nmhSetSnmpNotifyTag (&NotifyName, &NotifyTag)
                    == SNMP_FAILURE)
                {
                    return (Snmp3NotifModifyError (&NotifyName, &pu4ErrCode));
                }
            }
        }

        if ((u4StorageType != 0) &&
            ((INT4) u4StorageType != pNotifyEntry->i4NotifyStorageType))
        {
            /* Storage Type modified, save new value */
            if (nmhSetSnmpNotifyStorageType
                (&NotifyName, (INT4) u4StorageType) == SNMP_FAILURE)
            {
                return (Snmp3NotifModifyError (&NotifyName, &pu4ErrCode));
            }
        }

        if ((INT4) u4NotifyType != pNotifyEntry->i4NotifyType)
        {
            if (nmhTestv2SnmpNotifyType (&u4ErrorCode, &NotifyName,
                                         (INT4) u4NotifyType) == SNMP_FAILURE)
            {
                return (Snmp3NotifModifyError (&NotifyName, &pu4ErrCode));
            }
            if (nmhSetSnmpNotifyType (&NotifyName, u4NotifyType) ==
                SNMP_FAILURE)
            {
                return (Snmp3NotifModifyError (&NotifyName, &pu4ErrCode));
            }
        }

        nmhSetSnmpNotifyRowStatus (&NotifyName, SNMP_ROWSTATUS_ACTIVE);
        /* Entry modified */
    }
    else
    {
        /* entry does not exist - ADD */
        if (nmhSetSnmpNotifyRowStatus (&NotifyName,
                                       SNMP_ROWSTATUS_CREATEANDWAIT)
            == SNMP_FAILURE)
        {
            *pu4ErrCode = CLI_SNMP3_NEW_ENTRY_ERR;
            return SNMP_FAILURE;
        }

        /* Test Inputs */
        if (nmhTestv2SnmpNotifyTag (&u4ErrorCode, &NotifyName, &NotifyTag)
            == SNMP_FAILURE)
        {
            return (Snmp3NotifCreateError (&NotifyName, &pu4ErrCode));
        }

        if (nmhTestv2SnmpNotifyType (&u4ErrorCode, &NotifyName,
                                     (INT4) u4NotifyType) == SNMP_FAILURE)
        {
            return (Snmp3NotifCreateError (&NotifyName, &pu4ErrCode));
        }

        /* Set Inputs */
        if (u1TagFlag == SNMP_TRUE)
        {
            if (nmhSetSnmpNotifyTag (&NotifyName, &NotifyTag) == SNMP_FAILURE)
            {
                return (Snmp3NotifCreateError (&NotifyName, &pu4ErrCode));
            }
        }

        if (nmhSetSnmpNotifyType (&NotifyName, u4NotifyType) == SNMP_FAILURE)
        {
            return (Snmp3NotifCreateError (&NotifyName, &pu4ErrCode));
        }

        if (u4StorageType == 0)
        {
            /* Storage type not given  - make default as non-volatile type */
            u4StorageType = SNMP3_STORAGE_TYPE_NONVOLATILE;
        }

        if (nmhSetSnmpNotifyStorageType (&NotifyName, (INT4) u4StorageType)
            == SNMP_FAILURE)
        {
            return (Snmp3NotifCreateError (&NotifyName, &pu4ErrCode));
        }

        if (nmhTestv2SnmpNotifyRowStatus (&u4ErrorCode, &NotifyName,
                                          SNMP_ROWSTATUS_ACTIVE)
            == SNMP_FAILURE)
        {
            return (Snmp3NotifCreateError (&NotifyName, &pu4ErrCode));
        }

        if (nmhSetSnmpNotifyRowStatus (&NotifyName,
                                       SNMP_ROWSTATUS_ACTIVE) == SNMP_FAILURE)
        {
            return (Snmp3NotifCreateError (&NotifyName, &pu4ErrCode));
        }

        /* Entry created */
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3DefNotifyConfig                               */
/*                                                                           */
/*     DESCRIPTION      : Function to configure a default Notification entry */
/*                        during initialization                              */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
Snmp3DefNotifyConfig ()
{
    UINT1               au1NotifyName[SNMP_MAX_ADMIN_STR_LENGTH] =
        SNMP_DEF_NOTIFY_1;
    UINT1               au1Tag[SNMP_MAX_ADMIN_STR_LENGTH] = SNMP_DEF_NOTIFY_1;
    UINT1               au1NotifyName1[SNMP_MAX_ADMIN_STR_LENGTH] =
        SNMP_DEF_NOTIFY_2;
    UINT1               au1Tag1[SNMP_MAX_ADMIN_STR_LENGTH] = SNMP_DEF_NOTIFY_2;

    UINT4               u4ErrCode;
    UINT4               u4NotifyType = SNMP3_NOTIFY_TYPE_TRAP;
    UINT4               u4StorageType = SNMP3_STORAGE_TYPE_NONVOLATILE;

    UNUSED_PARAM (u4ErrCode);
    /* Create default entries */
    Snmp3NotifyConfig (au1NotifyName, au1Tag, u4NotifyType, u4StorageType,
                       &u4ErrCode);
    Snmp3NotifyConfig (au1NotifyName1, au1Tag1, u4NotifyType, u4StorageType,
                       &u4ErrCode);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3NotifModifyError                              */
/*                                                                           */
/*     DESCRIPTION      : Function to set Notification Table modification    */
/*                        err and set the row status of the entry to ACTIVE  */
/*                        the entry is set to ACTIVE as modification to the  */
/*                        existing entry failed                              */
/*                                                                           */
/*     INPUT            : pNotifyEntry - pointer to Notif Table entry        */
/*                        pu4ErrCode   - Error code pointer                  */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT1
Snmp3NotifModifyError (tSNMP_OCTET_STRING_TYPE * pNotifyName,
                       UINT4 **pu4ErrCode)
{
    **pu4ErrCode = CLI_SNMP3_ENTRY_MODIFICATION_ERR;
    nmhSetSnmpNotifyRowStatus (pNotifyName, SNMP_ROWSTATUS_ACTIVE);

    return SNMP_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3NotifCreateError                              */
/*                                                                           */
/*     DESCRIPTION      : Function to set Notification Table entry creation  */
/*                        error and to delete the entry created              */
/*                                                                           */
/*     INPUT            : pNotifyName - Notification Name                    */
/*                        pu4ErrCode  - Error code pointer                   */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT1
Snmp3NotifCreateError (tSNMP_OCTET_STRING_TYPE * pNotifyName,
                       UINT4 **pu4ErrCode)
{
    **pu4ErrCode = CLI_SNMP3_ENTRY_CREATION_ERR;

    nmhSetSnmpNotifyRowStatus (pNotifyName, SNMP_ROWSTATUS_DESTROY);

    return SNMP_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3TargetParamConfig                             */
/*                                                                           */
/*     DESCRIPTION      : Function to configure the Target Param Table       */
/*                                                                           */
/*     INPUT            : pu1ParamName  - Param Name                         */
/*                        i4MpModel     - Message Process Model              */
/*                        i4SecModel    - Security Model                     */
/*                        pu1SecName    - Secutiry Name                      */
/*                        i4SecLevel    - Security Level                     */
/*                        u4StorageType - Storage type in memory             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
Snmp3TargetParamConfig (UINT1 *pu1ParamName, INT4 i4MpModel,
                        INT4 i4SecModel, UINT1 *pu1SecName,
                        INT4 i4SecLevel, UINT4 u4StorageType, UINT4 *pu4ErrCode)
{
    UINT4               u4ErrorCode;
    UINT1               au1ParamName[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1SecurityName[SNMP_MAX_ADMIN_STR_LENGTH];

    tSNMP_OCTET_STRING_TYPE ParamName;
    tSNMP_OCTET_STRING_TYPE SecurityName;
    tSnmpTgtParamEntry *pParamEntry = NULL;

    /* verify validity of inputs */
    if ((SnmpCheckString (pu1ParamName) == SNMP_FAILURE) ||
        (SnmpCheckString (pu1SecName) == SNMP_FAILURE))
    {
        *pu4ErrCode = CLI_SNMP3_INVALID_INPUT_ERR;
        return SNMP_FAILURE;
    }

    if (((i4SecModel == SNMP_SECMODEL_V1) || (i4SecModel == SNMP_SECMODEL_V2C))
        && ((i4SecLevel == SNMP_AUTH_NOPRIV) || (i4SecLevel == SNMP_AUTH_PRIV)))
    {
        *pu4ErrCode = CLI_SNMP3_SECMODEL_SECLEVEL_ERR;
        return SNMP_FAILURE;
    }

    MEMSET (au1ParamName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    STRNCPY (au1ParamName, pu1ParamName, (sizeof (au1ParamName) - 1));

    ParamName.pu1_OctetList = au1ParamName;
    ParamName.i4_Length = (INT4) STRLEN (au1ParamName);

    MEMSET (au1SecurityName, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    STRNCPY (au1SecurityName, pu1SecName, (sizeof (au1SecurityName) - 1));

    SecurityName.pu1_OctetList = au1SecurityName;
    SecurityName.i4_Length = (INT4) STRLEN (au1SecurityName);

    /* check if an entry exists already */
    if ((pParamEntry = SNMPGetTgtParamEntry (&ParamName)) != NULL)
    {
        nmhSetSnmpTargetParamsRowStatus (&ParamName,
                                         SNMP_ROWSTATUS_NOTINSERVICE);

        /* entry exists - if any value in table is changed, modify */
        if ((u4StorageType != 0) &&
            ((INT2) u4StorageType != pParamEntry->i2ParamStorageType))
        {
            /* Storage Type modified, save new value */
            if (nmhSetSnmpTargetParamsStorageType (&ParamName,
                                                   (INT2) u4StorageType)
                == SNMP_FAILURE)
            {
                return (Snmp3TargetParamModifyError (&ParamName, &pu4ErrCode));
            }
        }

        if ((INT2) i4MpModel != pParamEntry->i2ParamMPModel)
        {
            if (nmhTestv2SnmpTargetParamsMPModel (&u4ErrorCode, &ParamName,
                                                  i4MpModel) == SNMP_FAILURE)
            {
                if (u4ErrorCode == SNMP_ERR_NO_ACCESS)
                {
                    *pu4ErrCode = CLI_SNMP3_TRGTPARAM_SECURE_MODE_ERR;
                    return (Snmp3TargetParamCreateError
                            (&ParamName, &pu4ErrCode));
                }
                else
                {
                    return (Snmp3TargetParamCreateError
                            (&ParamName, &pu4ErrCode));
                }
            }
            if (nmhSetSnmpTargetParamsMPModel (&ParamName, i4MpModel)
                == SNMP_FAILURE)
            {
                return (Snmp3TargetParamModifyError (&ParamName, &pu4ErrCode));
            }
        }
        if ((INT2) i4SecModel != pParamEntry->i2ParamSecModel)
        {
            if (nmhTestv2SnmpTargetParamsSecurityModel (&u4ErrorCode,
                                                        &ParamName,
                                                        i4SecModel) ==
                SNMP_FAILURE)
            {
                if (u4ErrorCode == SNMP_ERR_NO_ACCESS)
                {
                    *pu4ErrCode = CLI_SNMP3_TRGTPARAM_SECURE_MODE_ERR;
                    return (Snmp3TargetParamCreateError
                            (&ParamName, &pu4ErrCode));
                }
                else
                {
                    return (Snmp3TargetParamCreateError
                            (&ParamName, &pu4ErrCode));
                }
            }
            if (nmhSetSnmpTargetParamsSecurityModel (&ParamName, i4SecModel)
                == SNMP_FAILURE)
            {
                return (Snmp3TargetParamModifyError (&ParamName, &pu4ErrCode));
            }
        }

        if (SNMPCompareOctetString (&SecurityName, &(pParamEntry->ParamSecName))
            != SNMP_EQUAL)
        {
            if (nmhTestv2SnmpTargetParamsSecurityName (&u4ErrorCode, &ParamName,
                                                       &SecurityName)
                == SNMP_FAILURE)
            {
                return (Snmp3TargetParamModifyError (&ParamName, &pu4ErrCode));
            }
            if (nmhSetSnmpTargetParamsSecurityName (&ParamName, &SecurityName)
                == SNMP_FAILURE)
            {
                return (Snmp3TargetParamModifyError (&ParamName, &pu4ErrCode));
            }
        }

        if (i4SecLevel != pParamEntry->i4ParamSecLevel)
        {
            if (i4SecModel == SNMP_MPMODEL_V3)
            {
                if (nmhTestv2SnmpTargetParamsSecurityLevel (&u4ErrorCode,
                                                            &ParamName,
                                                            i4SecLevel) ==
                    SNMP_FAILURE)
                {
                    return (Snmp3TargetParamModifyError (&ParamName,
                                                         &pu4ErrCode));
                }
                if (nmhSetSnmpTargetParamsSecurityLevel
                    (&ParamName, i4SecLevel) == SNMP_FAILURE)
                {
                    return (Snmp3TargetParamModifyError (&ParamName,
                                                         &pu4ErrCode));
                }
            }
        }

        nmhSetSnmpTargetParamsRowStatus (&ParamName, SNMP_ROWSTATUS_ACTIVE);
        /* Entry modified */
    }
    else
    {
        /* entry does not exist - ADD */
        if (nmhSetSnmpTargetParamsRowStatus (&ParamName,
                                             SNMP_ROWSTATUS_CREATEANDWAIT)
            == SNMP_FAILURE)
        {
            *pu4ErrCode = CLI_SNMP3_NEW_ENTRY_ERR;
            return SNMP_FAILURE;
        }

        /* Test Inputs */
        if (nmhTestv2SnmpTargetParamsMPModel (&u4ErrorCode, &ParamName,
                                              i4MpModel) == SNMP_FAILURE)
        {
            if (u4ErrorCode == SNMP_ERR_NO_ACCESS)
            {
                *pu4ErrCode = CLI_SNMP3_TRGTPARAM_SECURE_MODE_ERR;
                return (Snmp3TargetParamCreateError (&ParamName, &pu4ErrCode));
            }
            else
            {
                return (Snmp3TargetParamCreateError (&ParamName, &pu4ErrCode));
            }
        }

        if (nmhTestv2SnmpTargetParamsSecurityModel (&u4ErrorCode, &ParamName,
                                                    i4SecModel) == SNMP_FAILURE)
        {
            if (u4ErrorCode == SNMP_ERR_NO_ACCESS)
            {
                *pu4ErrCode = CLI_SNMP3_TRGTPARAM_SECURE_MODE_ERR;
                return (Snmp3TargetParamCreateError (&ParamName, &pu4ErrCode));
            }
            else
            {
                return (Snmp3TargetParamCreateError (&ParamName, &pu4ErrCode));
            }
        }

        if (nmhTestv2SnmpTargetParamsSecurityName (&u4ErrorCode, &ParamName,
                                                   &SecurityName)
            == SNMP_FAILURE)
        {
            return (Snmp3TargetParamCreateError (&ParamName, &pu4ErrCode));
        }

        if (i4SecModel == SNMP_MPMODEL_V3)
        {
            if (nmhTestv2SnmpTargetParamsSecurityLevel (&u4ErrorCode,
                                                        &ParamName, i4SecLevel)
                == SNMP_FAILURE)
            {
                return (Snmp3TargetParamCreateError (&ParamName, &pu4ErrCode));
            }
        }

        /* Set Inputs */
        if (nmhSetSnmpTargetParamsMPModel (&ParamName, i4MpModel)
            == SNMP_FAILURE)
        {
            return (Snmp3TargetParamCreateError (&ParamName, &pu4ErrCode));
        }

        if (nmhSetSnmpTargetParamsSecurityModel (&ParamName, i4SecModel)
            == SNMP_FAILURE)
        {
            return (Snmp3TargetParamCreateError (&ParamName, &pu4ErrCode));
        }

        if (nmhSetSnmpTargetParamsSecurityName (&ParamName, &SecurityName)
            == SNMP_FAILURE)
        {
            return (Snmp3TargetParamCreateError (&ParamName, &pu4ErrCode));
        }

        if (i4SecModel == SNMP_MPMODEL_V3)
        {
            if (nmhSetSnmpTargetParamsSecurityLevel (&ParamName, i4SecLevel)
                == SNMP_FAILURE)
            {
                return (Snmp3TargetParamCreateError (&ParamName, &pu4ErrCode));
            }
        }

        if (u4StorageType == 0)
        {
            /* Storage type not given  - make default as non-volatile type */
            u4StorageType = SNMP3_STORAGE_TYPE_NONVOLATILE;
        }

        if (nmhSetSnmpTargetParamsStorageType (&ParamName,
                                               (INT2) u4StorageType)
            == SNMP_FAILURE)
        {
            return (Snmp3TargetParamCreateError (&ParamName, &pu4ErrCode));
        }

        if (nmhTestv2SnmpTargetParamsRowStatus (&u4ErrorCode, &ParamName,
                                                SNMP_ROWSTATUS_ACTIVE)
            == SNMP_FAILURE)
        {
            return (Snmp3TargetParamCreateError (&ParamName, &pu4ErrCode));
        }

        if (nmhSetSnmpTargetParamsRowStatus (&ParamName,
                                             SNMP_ROWSTATUS_ACTIVE)
            == SNMP_FAILURE)
        {
            return (Snmp3TargetParamCreateError (&ParamName, &pu4ErrCode));
        }
        /* Entry created */
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3DefTargetParamConfig                          */
/*                                                                           */
/*     DESCRIPTION      : Function to configure a default Address param entry*/
/*                        during initialization                              */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
Snmp3DefTargetParamConfig ()
{
    UINT1               au1ParamName[SNMP_MAX_ADMIN_STR_LENGTH] =
        SNMP_DEF_TGT_PARAM_1;
    INT4                i4MpModel = SNMP_MPMODEL_V2C;
    INT4                i4SecModel = SNMP_SECMODEL_V2C;
    UINT1               au1SecName[SNMP_MAX_ADMIN_STR_LENGTH] = "none";
    INT4                i4SecLevel = SNMP_NOAUTH_NOPRIV;

    UINT4               u4ErrCode;
    UINT4               u4StorageType = SNMP3_STORAGE_TYPE_NONVOLATILE;

    UNUSED_PARAM (u4ErrCode);
    /* Create default entry */
    Snmp3TargetParamConfig (au1ParamName, i4MpModel, i4SecModel,
                            au1SecName, i4SecLevel, u4StorageType, &u4ErrCode);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3Def1TargetParamConfig                         */
/*                                                                           */
/*     DESCRIPTION      : Function to configure a default Address param entry*/
/*                        during initialization                              */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
Snmp3Def1TargetParamConfig ()
{
    UINT1               au1ParamName[SNMP_MAX_ADMIN_STR_LENGTH] =
        SNMP_DEF_TGT_PARAM_2;
    INT4                i4MpModel = SNMP_SECMODEL_V1;
    INT4                i4SecModel = SNMP_SECMODEL_V1;
    UINT1               au1SecName[SNMP_MAX_ADMIN_STR_LENGTH] = "none";
    INT4                i4SecLevel = SNMP_NOAUTH_NOPRIV;

    UINT4               u4ErrCode;
    UINT4               u4StorageType = SNMP3_STORAGE_TYPE_NONVOLATILE;

    UNUSED_PARAM (u4ErrCode);
    /* Create default entry */
    Snmp3TargetParamConfig (au1ParamName, i4MpModel, i4SecModel,
                            au1SecName, i4SecLevel, u4StorageType, &u4ErrCode);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3TargetParamModifyError                        */
/*                                                                           */
/*     DESCRIPTION      : Function to set Target Params Table modification   */
/*                        err and set the row status of the entry to ACTIVE  */
/*                        the entry is set to ACTIVE as modification to the  */
/*                        existing entry failed                              */
/*                                                                           */
/*     INPUT            : pParamEntry  - pointer to Param Table entry        */
/*                        pu4ErrCode   - Error code pointer                  */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT1
Snmp3TargetParamModifyError (tSNMP_OCTET_STRING_TYPE * pTargetParamName,
                             UINT4 **pu4ErrCode)
{
    **pu4ErrCode = CLI_SNMP3_ENTRY_MODIFICATION_ERR;
    nmhSetSnmpTargetParamsRowStatus (pTargetParamName, SNMP_ROWSTATUS_ACTIVE);

    return SNMP_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Snmp3TargetParamCreateError                        */
/*                                                                           */
/*     DESCRIPTION      : Function to set Target Params Table entry creation */
/*                        error and to delete the entry created              */
/*                                                                           */
/*     INPUT            : pTargetParamName - Target Params Name              */
/*                        pu4ErrCode   - Error code pointer                  */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT1
Snmp3TargetParamCreateError (tSNMP_OCTET_STRING_TYPE * pTargetParamName,
                             UINT4 **pu4ErrCode)
{
    if (**pu4ErrCode != CLI_SNMP3_TRGTPARAM_SECURE_MODE_ERR)
    {
        **pu4ErrCode = CLI_SNMP3_ENTRY_CREATION_ERR;
    }
    nmhSetSnmpTargetParamsRowStatus (pTargetParamName, SNMP_ROWSTATUS_DESTROY);

    return SNMP_FAILURE;
}

/************************************************************************
 *  Function Name   : SNMPV3DefaultDelete 
 *  Description     : Function Removes all the SNMPv3 default table entries
 *  Input           : None
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
SNMPV3DefaultDelete ()
{

    UINT1               au1Buf[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1Buf2[SNMP_MAX_ADMIN_STR_LENGTH];
    tSNMP_OCTET_STRING_TYPE TempVar;
    tSNMP_OCTET_STRING_TYPE TempVar2;
    tSNMP_OID_TYPE     *pOid;

    TempVar.pu1_OctetList = au1Buf;
    TempVar2.pu1_OctetList = au1Buf2;

    /* If Community Control is set, Default communities cannot be created 
     * Hence it cannot be deleted also */
    if (SNMP_FALSE == gi4SnmpCommunityCtrl)
    {
        MEMCPY (au1Buf, SNMP_DEF_COMMUNITY_1, STRLEN (SNMP_DEF_COMMUNITY_1));
        TempVar.i4_Length = (INT4) STRLEN (SNMP_DEF_COMMUNITY_1);
        SNMPDeleteCommunityEntry (&TempVar);

        MEMCPY (au1Buf, SNMP_DEF_COMMUNITY_2, STRLEN (SNMP_DEF_COMMUNITY_2));
        TempVar.i4_Length = (INT4) STRLEN (SNMP_DEF_COMMUNITY_2);
        SNMPDeleteCommunityEntry (&TempVar);
    }
    MEMCPY (au1Buf, SNMP_DEF_USER_1, STRLEN (SNMP_DEF_USER_1));
    TempVar.i4_Length = (INT4) STRLEN (SNMP_DEF_USER_1);
    SNMPDeleteUsmEntry (&gSnmpEngineID, &TempVar);

    MEMCPY (au1Buf, SNMP_DEF_USER_2, STRLEN (SNMP_DEF_USER_2));
    TempVar.i4_Length = (INT4) STRLEN (SNMP_DEF_USER_2);
    SNMPDeleteUsmEntry (&gSnmpEngineID, &TempVar);

    MEMCPY (au1Buf, SNMP_DEF_USER_3, STRLEN (SNMP_DEF_USER_3));
    TempVar.i4_Length = (INT4) STRLEN (SNMP_DEF_USER_3);
    SNMPDeleteUsmEntry (&gSnmpEngineID, &TempVar);

    MEMCPY (au1Buf, SNMP_DEF_NOTIFY_1, STRLEN (SNMP_DEF_NOTIFY_1));
    TempVar.i4_Length = (INT4) STRLEN (SNMP_DEF_NOTIFY_1);
    SNMPDeleteNotifyEntry (&TempVar);

    MEMCPY (au1Buf, SNMP_DEF_NOTIFY_2, STRLEN (SNMP_DEF_NOTIFY_2));
    TempVar.i4_Length = (INT4) STRLEN (SNMP_DEF_NOTIFY_2);
    SNMPDeleteNotifyEntry (&TempVar);

    pOid = SNMP_AGT_GetOidFromString ((INT1 *) "1");

    if (pOid != NULL)
    {
        MEMCPY (au1Buf, SNMP_DEF_VIEW_1, STRLEN (SNMP_DEF_VIEW_1));
        TempVar.i4_Length = (INT4) STRLEN (SNMP_DEF_VIEW_1);
        VACMDeleteViewTree (&TempVar, pOid);

        MEMCPY (au1Buf, SNMP_DEF_VIEW_2, STRLEN (SNMP_DEF_VIEW_2));
        TempVar.i4_Length = (INT4) STRLEN (SNMP_DEF_VIEW_2);
        VACMDeleteViewTree (&TempVar, pOid);

        SNMP_FreeOid (pOid);
    }

    MEMCPY (au1Buf, SNMP_DEF_ACCESS_1, STRLEN (SNMP_DEF_ACCESS_1));
    TempVar.i4_Length = (INT4) STRLEN (SNMP_DEF_ACCESS_1);
    MEMCPY (au1Buf2, "\0", 1);
    TempVar2.i4_Length = 0;
    VACMDeleteAccess (&TempVar, &TempVar2, SNMP_SECMODEL_V1,
                      SNMP_NOAUTH_NOPRIV);

    VACMDeleteAccess (&TempVar, &TempVar2, SNMP_SECMODEL_V2C,
                      SNMP_NOAUTH_NOPRIV);

    MEMCPY (au1Buf, SNMP_DEF_ACCESS_2, STRLEN (SNMP_DEF_ACCESS_2));
    TempVar.i4_Length = (INT4) STRLEN (SNMP_DEF_ACCESS_2);
    VACMDeleteAccess (&TempVar, &TempVar2, SNMP_SECMODEL_V3, SNMP_AUTH_PRIV);

    VACMDeleteAccess (&TempVar, &TempVar2, SNMP_SECMODEL_V3, SNMP_AUTH_NOPRIV);

    VACMDeleteAccess (&TempVar, &TempVar2, SNMP_SECMODEL_V3,
                      SNMP_NOAUTH_NOPRIV);

    MEMCPY (au1Buf, "none", STRLEN ("none"));
    TempVar.i4_Length = (INT4) STRLEN ("none");
    VACMDeleteSecGrp (SNMP_SECMODEL_V1, &TempVar);

    MEMCPY (au1Buf, "none", STRLEN ("none"));
    TempVar.i4_Length = (INT4) STRLEN ("none");
    VACMDeleteSecGrp (SNMP_SECMODEL_V2C, &TempVar);

    MEMCPY (au1Buf, SNMP_DEF_GROUP_2, STRLEN (SNMP_DEF_GROUP_2));
    TempVar.i4_Length = (INT4) STRLEN (SNMP_DEF_GROUP_2);
    VACMDeleteSecGrp (SNMP_SECMODEL_V3, &TempVar);

    MEMCPY (au1Buf, SNMP_DEF_USER_2, STRLEN (SNMP_DEF_USER_2));
    TempVar.i4_Length = (INT4) STRLEN (SNMP_DEF_USER_2);
    VACMDeleteSecGrp (SNMP_SECMODEL_V3, &TempVar);

    MEMCPY (au1Buf, SNMP_DEF_USER_3, STRLEN (SNMP_DEF_USER_3));
    TempVar.i4_Length = (INT4) STRLEN (SNMP_DEF_USER_3);
    VACMDeleteSecGrp (SNMP_SECMODEL_V3, &TempVar);

    MEMCPY (au1Buf, SNMP_DEF_TGT_PARAM_1, STRLEN (SNMP_DEF_TGT_PARAM_1));
    TempVar.i4_Length = (INT4) STRLEN (SNMP_DEF_TGT_PARAM_1);
    SNMPDeleteTgtParamEntry (&TempVar);

    MEMCPY (au1Buf, SNMP_DEF_TGT_PARAM_2, STRLEN (SNMP_DEF_TGT_PARAM_2));
    TempVar.i4_Length = (INT4) STRLEN (SNMP_DEF_TGT_PARAM_2);
    SNMPDeleteTgtParamEntry (&TempVar);
}

/*************************************************************************/
/* Function Name : SnmpGetNumOctets                                      */
/* Description   : To get the number of octets in the given dot string   */
/* Input(s)      : pi1String - Pointer to the string                     */
/* Output(s)     : None                                                  */
/* Returns       : u4NumOctets                                           */
/*************************************************************************/
UINT4
SnmpGetNumOctets (INT1 *pi1String)
{
    UINT4               u4NumOctets = 0;
    INT4                i4Count;
    INT4                i4Length;
    INT1                i1Flag = SNMP_ZERO;

    i4Length = (INT4) STRLEN (pi1String);

    for (i4Count = 0; i4Count < i4Length; i4Count++)
    {
        if (pi1String[i4Count] == '.')
        {
            u4NumOctets++;
            i1Flag = SNMP_ZERO;
        }
        else
        {
            i1Flag++;
        }
    }
    u4NumOctets++;
    return u4NumOctets;
}

/*************************************************************************/
/* Function Name : SnmpConvertStringToOctet                             */
/* Description   : To convert a given dotted string to octet string      */
/* Input(s)      : pi1String - Pointer to the string to be converted     */
/* Output(s)     : pOctet - Converted octet                              */
/* Returns       : SUCCESS/FAILURE                                       */
/*************************************************************************/
INT1
SnmpConvertStringToOctet (INT1 *pi1String, tSNMP_OCTET_STRING_TYPE * pOctet)
{
    INT4                i4Count;
    INT4                i4Length;
    INT1                ai1Hexa[SNMP_HEXA_STRLEN + 1];
    INT1                i1Flag = SNMP_ZERO;

    MEMSET (ai1Hexa, 0, SNMP_HEXA_STRLEN + 1);
    i4Length = (INT4) STRLEN (pi1String);

    pOctet->i4_Length = 0;

    for (i4Count = 0; i4Count < i4Length; i4Count++)
    {
        if (pi1String[i4Count] == '.')
        {
            if (i1Flag != SNMP_ZERO)
            {
                pOctet->pu1_OctetList[pOctet->i4_Length] =
                    (UINT1) strtol ((const char *) ai1Hexa, (char **) NULL,
                                    SNMP_HEXA_DECIMAL);
            }
            else
            {
                return SNMP_FAILURE;
            }
            pOctet->i4_Length++;
            ai1Hexa[0] = 0;
            ai1Hexa[1] = 0;
            i1Flag = SNMP_ZERO;
        }
        else
        {
            i1Flag++;
            if (i1Flag > SNMP_HEXA_STRLEN)
                return SNMP_FAILURE;
            if (isxdigit (pi1String[i4Count]) == 0)
                return SNMP_FAILURE;
            ai1Hexa[i1Flag - 1] = pi1String[i4Count];
        }
    }
    if (i1Flag == SNMP_ZERO || (i1Flag > SNMP_HEXA_STRLEN))
        return SNMP_FAILURE;
    pOctet->pu1_OctetList[pOctet->i4_Length] =
        (UINT1) strtol ((const char *) ai1Hexa, (char **) NULL,
                        SNMP_HEXA_DECIMAL);
    pOctet->i4_Length++;

    return SNMP_SUCCESS;
}

/*************************************************************************/
/* Function Name : SnmpConvertOctettoString                             */
/* Description   : To convert a given octet string to string in hexa     */
/* Input(s)      : pOctet - Pointer to octet that needs conversion       */
/* Output(s)     : pi1String - Pointer to the converted string           */
/* Returns       : SUCCESS/FAILURE                                       */
/*************************************************************************/
INT1
SnmpConvertOctetToString (tSNMP_OCTET_STRING_TYPE * pOctet, INT1 *pi1String)
{
    INT4                i4Length;

    for (i4Length = 0; i4Length < pOctet->i4_Length; i4Length++)
    {
        if (pOctet->pu1_OctetList[i4Length] < SNMP_HEXA_DECIMAL)
        {
            SPRINTF ((char *) pi1String + STRLEN (pi1String), "0%x",
                     pOctet->pu1_OctetList[i4Length]);
        }
        else
        {
            SPRINTF ((char *) pi1String + STRLEN (pi1String), "%x",
                     pOctet->pu1_OctetList[i4Length]);
        }
        if (i4Length != pOctet->i4_Length - 1)
        {
            SPRINTF ((char *) pi1String + STRLEN (pi1String), "%c", '.');
        }
    }
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : SnmpGetSysName
 *  
 *  Description     : This API returns the System Name.
 *  
 *  Pre-requisites  : NONE
 *  
 *  Input           : NONE
 *  
 *  Output          : pu1RetValSysName - System Name string is returned 
 *                                       through this pointer.
 *                                       
 *  Called By       : LldpPortGetSysName
 *
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
INT4
SnmpGetSysName (UINT1 *pu1RetValSysName)
{
    STRNCPY (pu1RetValSysName, gSnmpSystem.au1SysName,
             STRLEN (gSnmpSystem.au1SysName));
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : SnmpGetSysDescr
 *  
 *  Description     : This API returns the System Description.
 *  
 *  Pre-requisites  : NONE
 *  
 *  Input           : NONE
 *  
 *  Output          : pu1RetValSysDescr - System Description string is returned 
 *                                       through this pointer.
 *                                       
 *  Called By       : LldpPortGetSysDescr
 *
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
INT4
SnmpGetSysDescr (UINT1 *pu1RetValSysDescr)
{
    STRNCPY (pu1RetValSysDescr, gSnmpSystem.au1SysDescr,
             STRLEN (gSnmpSystem.au1SysDescr));
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : SnmpGetAgentParam
 *  
 *  Description     : This API returns the SNMP agent parameters.
 *  
 *  Pre-requisites  : NONE
 *  
 *  Input           : u4MIBSequence - Sequence of MIB in the list
 *  
 *  Output          : pAgentParam - SNMP agent parameters
 *                                       
 *  Returns         : SNMP_SUCCESS/SNMP_FAILURE
 ************************************************************************/

PUBLIC INT1
SnmpGetAgentParam (UINT4 u4MIBSequence, tSnmpAgentParam * pAgentParam)
{
    tSNMP_OID_TYPE     *pContextMIB = NULL;
    tSNMP_OID_TYPE     *pNonContextMIB = NULL;
    tMibReg            *pMibReg = NULL;
    UINT4               u4ContextMibCount = 0;
    UINT4               u4NonContextMibCount = 0;
    if (pAgentParam == NULL)
    {
        return SNMP_FAILURE;
    }
    /* Get the value for u4NumWithContextIdAndLock and
     * u4NumWithLock
     */
    /* Get the value for au1NameWithContextIdAndLock and
     * au1NameWithLock */
    pMibReg = gpMibReg;
    while (pMibReg != NULL)
    {
        if (pMibReg->pSetContextPointer != NULL)
        {
            u4ContextMibCount++;
        }
        else
        {
            u4NonContextMibCount++;
        }
        if ((u4MIBSequence != 0) &&
            (u4ContextMibCount == u4MIBSequence) && (pContextMIB == NULL))
        {
            pContextMIB = pMibReg->pMibID;
            SnmpGetMIBNameForBaseOid (pContextMIB,
                                      pAgentParam->au1NameWithContextIdAndLock);
        }
        if ((u4MIBSequence != 0) &&
            (u4NonContextMibCount == u4MIBSequence) && (pNonContextMIB == NULL))
        {
            pNonContextMIB = pMibReg->pMibID;
            SnmpGetMIBNameForBaseOid (pNonContextMIB,
                                      pAgentParam->au1NameWithLock);
        }
        pMibReg = pMibReg->pNextMib;
    }
    pAgentParam->u4NumWithContextIdAndLock = u4ContextMibCount;
    pAgentParam->u4NumWithLock = u4NonContextMibCount;
    /* Get the value of au1EngineId and u2EngineIdLength */
    MEMCPY (pAgentParam->au1EngineId, gSnmpEngineID.pu1_OctetList,
            gSnmpEngineID.i4_Length);
    pAgentParam->u2EngineIdLength = (UINT2) gSnmpEngineID.i4_Length;
    /* Get the value of u4LastChangeTime */
    pAgentParam->u4LastChangeTime = gSnmpEntGlobalInfo.u4LastChangeTime;
    pAgentParam->u2TransportAddrLength = 0;
    pAgentParam->u4TransportDomain = 0;
    return SNMP_SUCCESS;
}

/*****************************************************************************
 * Function Name      : SnmpGetMIBNameForBaseOid                                     *
 *                                                                           *
 * Description        : This function is used to get the Mib Name 
 *                      corresponding to the MIB OID
 *                                                                           *
 * Input(s)           : pMibOid - Mib Object Identifier value                *
 *                                                                           *
 * Output(s)          : pu1MibName - Mib Name corresponding to the MIB OID   *
 *                                                                           *
 * Returns       : SUCCESS/FAILURE                                           *
 ****************************************************************************/
PUBLIC INT1
SnmpGetMIBNameForBaseOid (tSNMP_OID_TYPE * pMibOid, UINT1 *pu1MibName)
{

    UINT4               u4Index = SNMP_ZERO;

    while (u4Index < MAX_SYSOR_TABLE_ENTRY)
    {
        if ((pMibOid->u4_Length * sizeof (UINT4) <= MAX_OID_LENGTH) &&
            (MEMCMP (pMibOid->pu4_OidList, gaSnmpSysorTable[u4Index].au4SysOrId,
                     pMibOid->u4_Length * sizeof (UINT4)) == 0))
        {
            MEMCPY (pu1MibName, gaSnmpSysorTable[u4Index].au1SysOrDescr,
                    STRLEN (gaSnmpSysorTable[u4Index].au1SysOrDescr));
            break;
        }
        else
        {
            u4Index = u4Index + 1;
        }
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************
 * Function Name      : SnmpThrottleTmrExpiry                                *
 *                                                                           *
 * Description        : This function handles the Throttle Timer Expiry
 *                      event 
 *                                                                           *
 * Input(s)           : None                                                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 ****************************************************************************/
PUBLIC VOID
SnmpThrottleTmrExpiry (VOID)
{

    if (gSnmpEntGlobalInfo.b1TimeChangeFlag == SNMP_TRUE)
    {
        gSnmpEntGlobalInfo.b1TimeChangeFlag = SNMP_FALSE;
        /*Send Trap */
        SnmpLastChangeNotify ();
        /*Start Timer */
        SnmpStartTimer (gSnmpEntGlobalInfo.ThrottlingTmrListId,
                        &(gSnmpEntGlobalInfo.ThrottlingTmr),
                        SNMP_THROTTLING_DEFAULT_INTERVAL);
    }
    return;
}

/*****************************************************************************
 * Function Name      : SnmpMatchTrapForLastChangeTime                       *
 *                                                                           *
 * Description        : This function matches the traps so that if it matches*
 *                      the list Last Change time value is to be updated     *
 *                                                                           *
 * Input(s)           : pOid - Oid of teh Trap                               *
 *                      u4SpecTrapType - Trap Type
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Returns       : SUCCESS/FAILURE                                           *
 ****************************************************************************/
PUBLIC INT1
SnmpMatchTrapForLastChangeTime (tSNMP_OID_TYPE * pOid, UINT4 u4SpecTrapType)
{

    UINT4               u4Count = 0;
    if (pOid == NULL)
    {
        return SNMP_FAILURE;
    }
    for (u4Count = 1; u4Count < SNMP_MAX_LAST_TIME_CHANGE_TRAPS; u4Count++)
    {
        if ((pOid->u4_Length * sizeof (UINT4) <= SNMP_MAX_OID_LENGTH) &&
            (MEMCMP (pOid->pu4_OidList, gaau4TrapOidList[u4Count - 1],
                     pOid->u4_Length * sizeof (UINT4)) == 0) &&
            (gau1TrapType[u4Count - 1] == u4SpecTrapType))
        {
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/*****************************************************************************
 * Function Name      : SnmpUpdateLastTimeChange                             *
 *                                                                           *
 * Description        : This function performs the action that should be 
 *                      performed on change of Last Time Change value        *
 *                                                                           *
 *                                                                           *
 * Input(s)           : None                                                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Returns       : SUCCESS/FAILURE                                           *
 ****************************************************************************/
PUBLIC INT1
SnmpUpdateLastTimeChange ()
{

    UINT4               u4RemainingTime = SNMP_ZERO;

    /* Update the Last Time Change with the timestamp value */
    GET_TIME_TICKS (&gSnmpEntGlobalInfo.u4LastChangeTime);

    if (TmrGetRemainingTime (gSnmpEntGlobalInfo.ThrottlingTmrListId,
                             &(gSnmpEntGlobalInfo.ThrottlingTmr),
                             &u4RemainingTime) != TMR_SUCCESS)
    {
        u4RemainingTime = SNMP_ZERO;
    }

    if (u4RemainingTime > 0)
    {
        gSnmpEntGlobalInfo.b1TimeChangeFlag = SNMP_TRUE;
    }
    else
    {
        gSnmpEntGlobalInfo.b1TimeChangeFlag = SNMP_FALSE;
        /*Send Trap */
        SnmpLastChangeNotify ();
        /*Start Timer */
        SnmpStartTimer (gSnmpEntGlobalInfo.ThrottlingTmrListId,
                        &(gSnmpEntGlobalInfo.ThrottlingTmr),
                        SNMP_THROTTLING_DEFAULT_INTERVAL);
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************
 * Function Name      : SnmpLastChangeNotify                                 *
 *                                                                           *
 * Description        : This function is used to initiate a trap for last    *
 *                      change                                               *
 *                                                                           *
 * Input(s)           : None                                                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 ****************************************************************************/
PUBLIC VOID
SnmpLastChangeNotify ()
{
#ifdef SNMP_2_WANTED
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    UINT4               u4GenTrapType;
    UINT4               u4SpecTrapType;
    UINT1               au1Buf[SNMP_ARRAY_SIZE_256];

    MEMSET (au1Buf, SNMP_ZERO, (SNMP_ARRAY_SIZE_256 * sizeof (UINT1)));

    /* Set Enterprise OID */
    pEnterpriseOid = SNMP_AGT_GetOidFromString ((INT1 *) SNMP_ENT_STD_TRAP_OID);

    if (pEnterpriseOid == NULL)
    {
        return;
    }

    /*  Set Trap type */
    u4GenTrapType = ENTERPRISE_SPECIFIC;
    u4SpecTrapType = SNMP_LAST_CHANGE_TIME_VAL;

    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    /* Get OID corresponding to the trap type mib object */
    pOid =
        SNMP_AGT_GetOidFromString ((INT1 *) SNMP_ENT_MIB_LAST_CHANGE_TIME_OID);

    if (pOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }

    /* Bind corresponding value to the OID */
    pVbList = SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_UNSIGNED32,
                                    (UINT4)
                                    (gSnmpEntGlobalInfo.u4LastChangeTime),
                                    0, NULL, NULL, SnmpCnt64Type);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }

    /* The following API sends the Trap info to the SNMP Agent. */
    SNMP_AGT_RIF_Notify_Trap (pEnterpriseOid, u4GenTrapType,
                              u4SpecTrapType, pVbList);

#endif
    return;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : SnmpSendTrap                                               */
/*                                                                           */
/* Description  : Routine to send trap to SNMP .                             */
/*                                                                           */
/* Input        : i4TrapId   : Trap Identifier                               */
/*                pi1TrapOid : Pointer to the trap OID                       */
/*                u1OidLen   : OID Length                                    */
/*                pTrapInfo  : Pointer to the trap information.              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
SnmpSendTrap (UINT1 u1TrapId, INT1 *pi1TrapOid, UINT1 u1OidLen, VOID *pTrapInfo)
{
#ifdef SNMP_2_WANTED
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid;
    tSNMP_OID_TYPE     *pOid = NULL;
    UINT4               u4GenTrapType;
    UINT4               u4SpecTrapType;
    UINT2              *pu2ListenPort = NULL;
    tSNMP_COUNTER64_TYPE SnmpCounter64Type;
    /* OID of object fsSnmpListenAgentPort */
    UINT4               au4SnmpListenAgentPort[] =
        { 1, 3, 6, 1, 4, 1, 2076, 112, 19 };

    UNUSED_PARAM (u1OidLen);

    pEnterpriseOid = (tSNMP_OID_TYPE *) SNMP_AGT_GetOidFromString (pi1TrapOid);
    if (pEnterpriseOid == NULL)
    {
        return;
    }
    u4GenTrapType = ENTERPRISE_SPECIFIC;
    u4SpecTrapType = (UINT4) u1TrapId;
    SnmpCounter64Type.msn = 0;
    SnmpCounter64Type.lsn = 0;

    switch (u1TrapId)
    {
        case SNMP_MIB_REG_TRAP:
        case SNMP_MIB_DEREG_TRAP:

            pu2ListenPort = (UINT2 *) pTrapInfo;

            if ((pOid = alloc_oid (SNMP_TRAP_OID_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            MEMCPY (pOid->pu4_OidList, au4SnmpListenAgentPort,
                    (SNMP_TRAP_OID_LEN - 2) * sizeof (UINT4));
            pOid->u4_Length = SNMP_TRAP_OID_LEN - 1;

            pVbList =
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                      (INT4) *pu2ListenPort,
                                      NULL, NULL, SnmpCounter64Type);
            pStartVb = pVbList;

            break;

        default:
            SNMP_FreeOid (pEnterpriseOid);
            return;
    }

    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_AGT_RIF_Notify_Trap (pEnterpriseOid, u4GenTrapType, u4SpecTrapType,
                              pStartVb);

#else /* SNMP_2_WANTED */
    UNUSED_PARAM (u1TrapId);
    UNUSED_PARAM (*pi1TrapOid);
    UNUSED_PARAM (u1OidLen);
    UNUSED_PARAM (pTrapInfo);
#endif /* SNMP_2_WANTED */

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : SnmpCreateVirtualAccessConfig                      */
/*                                                                           */
/*     DESCRIPTION      : Function to configure a  access entry  for teh     */
/*                        context created                                    */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
SnmpCreateVirtualAccessConfig (UINT1 *pu1Context)
{
    UINT1               au1Group[SNMP_MAX_ADMIN_STR_LENGTH] = SNMP_DEF_ACCESS_1;
    INT4                i4SecLevel = SNMP_NOAUTH_NOPRIV;
    INT4                i4SecModel = SNMP_SECMODEL_V2C;
    UINT4               u4StorageType = SNMP3_STORAGE_TYPE_NONVOLATILE;
    UINT4               u4ErrCode;
    UINT1               au1ReadName[SNMP_MAX_ADMIN_STR_LENGTH] =
        SNMP_DEF_READ_NAME;
    UINT1               au1WriteName[SNMP_MAX_ADMIN_STR_LENGTH] =
        SNMP_DEF_WRITE_NAME;
    UINT1               au1NotifyName[SNMP_MAX_ADMIN_STR_LENGTH] =
        SNMP_DEF_NOTIFY_NAME;

    UNUSED_PARAM (u4ErrCode);

    /* Create entry for teh newly created context */
    Snmp3AccessConfig (au1Group, pu1Context, i4SecLevel,
                       i4SecModel, au1ReadName, au1WriteName,
                       au1NotifyName, u4StorageType, &u4ErrCode);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : SnmpCreateVirtualCommunityConfig                   */
/*                                                                           */
/*     DESCRIPTION      : Function to configure a  community entry           */
/*                        for the context created                            */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
SnmpCreateVirtualCommunityConfig (UINT1 *pu1Context)
{
    UINT1               au1SecName[SNMP_MAX_ADMIN_STR_LENGTH] = "none";
    UINT1               au1Tag[SNMP_MAX_ADMIN_STR_LENGTH] = "none";

    UINT4               u4StorageType = SNMP3_STORAGE_TYPE_NONVOLATILE;
    UINT1               au1EngineId[SNMP_MAX_STR_ENGINEID_LEN] = "";
    UINT4               u4ErrCode;

    UNUSED_PARAM (u4ErrCode);
    /* Create entry for teh newly created context */
    Snmp3CommunityConfig (pu1Context, pu1Context, au1SecName,
                          pu1Context, au1Tag, u4StorageType, au1EngineId,
                          &u4ErrCode);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : SnmpGetSysOrDescriptor                             */
/*                                                                           */
/*     DESCRIPTION      : Function to get a Sysor Descriptor based on the    */
/*                        index passed.                                      */
/*                                                                           */
/*     INPUT            : i4SysORIndex                                       */
/*                                                                           */
/*     OUTPUT           : pRetValSysORDescr                                  */
/*                                                                           */
/*     RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

INT4
SnmpGetSysOrDescriptor (INT4 i4SysORIndex,
                        tSNMP_OCTET_STRING_TYPE * pRetValSysORDescr)
{
    if (gaSnmpSysorTable[i4SysORIndex].i4Status == SNMP_ACTIVE)
    {
        pRetValSysORDescr->i4_Length =
            (INT4) STRLEN (gaSnmpSysorTable[i4SysORIndex].au1SysOrDescr);
        if (pRetValSysORDescr->i4_Length < SNMP_MAX_OCTETSTRING_SIZE)

            MEMCPY (pRetValSysORDescr->pu1_OctetList,
                    gaSnmpSysorTable[i4SysORIndex].au1SysOrDescr,
                    pRetValSysORDescr->i4_Length);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : SnmpIsAuthSecAlgoSupported                         */
/*                                                                           */
/*     DESCRIPTION      : Function to check if Authentication algorithm      */
/*                        is supported in the current mode, if not supported */
/*                        FAILURE is returned.                               */
/*                                                                           */
/*     INPUT            : u4AlgoType                                         */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
SnmpIsAuthSecAlgoSupported (INT4 u4AlgoType)
{
    INT4                i4RetVal = SNMP_SUCCESS;
    INT4                u4SecureMode = SNMP_ZERO;

    u4SecureMode = FipsGetFipsCurrOperMode ();
    switch (u4SecureMode)
    {
        case LEGACY_MODE:
            i4RetVal = SNMP_SUCCESS;
            break;
#ifdef FIPS_WANTED
        case FIPS_MODE:
            if ((u4AlgoType == SNMP_MD5) || (u4AlgoType == SNMP_NO_AUTH))
            {
                SNMP_TRC3 (SNMP_CRITICAL_TRC, "[%s]:The Auth Alogrithm [%d] is "
                           "not supported in mode %d.", __func__, u4AlgoType,
                           u4SecureMode);
                i4RetVal = SNMP_FAILURE;
            }
            break;
        case CNSA_MODE:
            if ((u4AlgoType == SNMP_MD5) || (u4AlgoType == SNMP_SHA) ||
                (u4AlgoType == SNMP_SHA256) || (u4AlgoType == SNMP_NO_AUTH))
            {
                SNMP_TRC3 (SNMP_CRITICAL_TRC, "[%s]:The Auth Alogrithm [%d] is "
                           "not supported in mode %d.", __func__, u4AlgoType,
                           u4SecureMode);
                i4RetVal = SNMP_FAILURE;
            }
            break;
#endif
        default:
            SNMP_TRC2 (SNMP_FAILURE_TRC, "Invalid secure mode [%d] received "
                       " for Algotype [%d] \n", u4SecureMode, u4AlgoType);
            i4RetVal = SNMP_FAILURE;
            break;
    }
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : SnmpIsPrivSecAlgoSupported                         */
/*                                                                           */
/*     DESCRIPTION      : Function to check if Privacy algorithm  is         */
/*                        supported in the current mode, if not supported    */
/*                        FAILURE is returned.                               */
/*                                                                           */
/*     INPUT            : u4AlgoType                                         */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
SnmpIsPrivSecAlgoSupported (INT4 u4AlgoType)
{
    INT4                i4RetVal = SNMP_SUCCESS;
    INT4                u4SecureMode = SNMP_ZERO;

    u4SecureMode = FipsGetFipsCurrOperMode ();
    switch (u4SecureMode)
    {
        case LEGACY_MODE:
            if ((u4AlgoType == SNMP_AESCTR) || (u4AlgoType == SNMP_AESCTR192)
                || (u4AlgoType == SNMP_AESCTR256))
            {
                SNMP_TRC3 (SNMP_CRITICAL_TRC,
                           "[%s]:The Privacy Alogrithm [%d] is "
                           "not supported in mode %d.", __func__, u4AlgoType,
                           u4SecureMode);
                i4RetVal = SNMP_FAILURE;
            }
            break;
#ifdef FIPS_WANTED
        case FIPS_MODE:
            if ((u4AlgoType == SNMP_DES_CBC) || (u4AlgoType == SNMP_NO_PRIV) ||
                (u4AlgoType == SNMP_AESCTR) || (u4AlgoType == SNMP_AESCTR192)
                || (u4AlgoType == SNMP_AESCTR256))

            {
                SNMP_TRC3 (SNMP_CRITICAL_TRC,
                           "[%s]:The Privacy Alogrithm [%d] is "
                           "not supported in mode %d.", __func__, u4AlgoType,
                           u4SecureMode);
                i4RetVal = SNMP_FAILURE;
            }
            break;
        case CNSA_MODE:
            if ((u4AlgoType == SNMP_DES_CBC) || (u4AlgoType == SNMP_AESCFB128)
                || (u4AlgoType == SNMP_AESCFB192)
                || (u4AlgoType == SNMP_TDES_CBC) || (u4AlgoType == SNMP_AESCTR)
                || (u4AlgoType == SNMP_AESCTR192)
                || (u4AlgoType == SNMP_AESCTR256)
                || (u4AlgoType == SNMP_NO_PRIV))
            {
                SNMP_TRC3 (SNMP_CRITICAL_TRC,
                           "[%s]:The Privacy Alogrithm [%d] is "
                           "not supported in mode %d.", __func__, u4AlgoType,
                           u4SecureMode);
                i4RetVal = SNMP_FAILURE;
            }
            break;
#endif
        default:
            SNMP_TRC2 (SNMP_FAILURE_TRC, "Invalid secure mode [%d] received "
                       " for Algotype [%d] \n", u4SecureMode, u4AlgoType);
            i4RetVal = SNMP_FAILURE;
            break;
    }
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : SnmpIsModeSecure                                   */
/*                                                                           */
/*     DESCRIPTION      : Function to check if mode is secure mode. If the   */
/*                        operating mode is either FIPS or CSNA, SUCCESS     */
/*                        is returned.                                       */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
SnmpIsModeSecure (VOID)
{
    INT4                i4SecureMode = FipsGetFipsCurrOperMode ();

    if ((i4SecureMode == FIPS_MODE) || (i4SecureMode == CNSA_MODE))
    {
        /* The Mode is secure so return Success. */
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : SnmpV3DefSecureUsers                               */
/*                                                                           */
/*     DESCRIPTION      : Function to Initializie the Default SNMP V3 users  */
/*                        the operating mode is either FIPS or CSNA.*/
/*                        is returned.                                       */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
SnmpV3DefSecureUsers (VOID)
{
    tSNMP_OCTET_STRING_TYPE UserName;
    tSnmpUsmEntry      *pSnmpUsmEntry = NULL;
    UINT1               au1UserName[SNMP_MAX_ADMIN_STR_LENGTH];

    INT4                i4Return = 0;
    tAccessEntry       *pAccEntry = NULL;
    tViewTree          *pViewEntry = NULL;
    tSecGrp            *pSecEntry = NULL;
    UINT1               au1GrpName[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1AccContext[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1AccName[SNMP_MAX_ADMIN_STR_LENGTH];

    tSNMP_OCTET_STRING_TYPE GrpName;
    tSNMP_OCTET_STRING_TYPE AccContext;
    tSNMP_OCTET_STRING_TYPE AccName;
    tSNMP_OID_TYPE      SubTree;
    INT4                i4SecLevel = SNMP_MPMODEL_V3;
    INT4                i4SecureMode = FipsGetFipsCurrOperMode ();
    UINT4               u4UsmUserAuthProtocol = SNMP_SHA;
    UINT4               u4UsmUserPrivProtocol = SNMP_DES_CBC;
#ifdef  ISS_WANTED
    INT1               *pi1EngineID = NULL;
#endif
    UserName.pu1_OctetList = au1UserName;

#ifdef ISS_WANTED
    pi1EngineID = IssGetSnmpEngineID ();
    gSnmpEngineID.pu1_OctetList = gau1LocalSnmpEngineID;
    if (SnmpConvertStringToOctet (pi1EngineID, &gSnmpEngineID) == SNMP_FAILURE
        || ((gSnmpEngineID.i4_Length < SNMP_MIN_ENGINE_ID_LEN)
            || (gSnmpEngineID.i4_Length > SNMP_MAX_ENGINE_ID_LEN)))
    {
        MEMCPY (gSnmpEngineID.pu1_OctetList, gau1SnmpEngineID,
                SNMP_DEFAULT_ENGINEID_LEN);
        gSnmpEngineID.i4_Length = SNMP_DEFAULT_ENGINEID_LEN;
        IssSetSnmpEngineIDToNvRam (NULL);
    }

    /* Updating SNMP Engine Boot Count */
    gSnmpSystem.u4SnmpBootCount = IssGetSnmpEngineBoots ();

#else

    gSnmpEngineID.pu1_OctetList = gau1SnmpEngineID;
    gSnmpEngineID.i4_Length = SNMP_DEFAULT_ENGINEID_LEN;

    gSnmpSystem.u4SnmpBootCount = gu4SnmpEngineBootCount;
#endif

    /* Check the secure mode level and based on the level. */
    if (i4SecureMode == LEGACY_MODE)
    {
        u4UsmUserAuthProtocol = SNMP_SHA;
        u4UsmUserPrivProtocol = SNMP_AESCFB128;
    }
    else if ((i4SecureMode == FIPS_MODE) || (i4SecureMode == CNSA_MODE))
    {
        u4UsmUserAuthProtocol = SNMP_SHA384;
        u4UsmUserPrivProtocol = SNMP_AESCFB256;
    }
    /* SNMP_DEF_USER_3  */
    UserName.i4_Length = (INT4) STRLEN (SNMP_DEF_USER_3);
    MEMCPY (UserName.pu1_OctetList, SNMP_DEF_USER_3, UserName.i4_Length);
    pSnmpUsmEntry = SNMPCreateUsmEntry (&gSnmpEngineID, &UserName);
    if (pSnmpUsmEntry == NULL)
    {
        SNMPTrace ("Unable to Create Snmp Usm Default Entry\n");
        return SNMP_FAILURE;
    }
    pSnmpUsmEntry->u4UsmUserAuthProtocol = u4UsmUserAuthProtocol;
    pSnmpUsmEntry->u4UsmUserPrivProtocol = u4UsmUserPrivProtocol;
    SNMPCopyOctetString (&(pSnmpUsmEntry->UsmUserAuthPassword), &UserName);
    SNMPCopyOctetString (&(pSnmpUsmEntry->UsmUserPrivPassword), &UserName);
    SNMPKeyGenerator (UserName.pu1_OctetList,
                      UserName.i4_Length,
                      pSnmpUsmEntry->u4UsmUserAuthProtocol,
                      &(pSnmpUsmEntry->UsmUserEngineID),
                      &(pSnmpUsmEntry->UsmUserAuthKeyChange));
    SNMPKeyGenerator (UserName.pu1_OctetList,
                      UserName.i4_Length,
                      pSnmpUsmEntry->u4UsmUserAuthProtocol,
                      &(pSnmpUsmEntry->UsmUserEngineID),
                      &(pSnmpUsmEntry->UsmUserPrivKeyChange));
    pSnmpUsmEntry->u4UsmUserStatus = SNMP_ACTIVE;

    /* VACM Entries */
    GrpName.pu1_OctetList = au1GrpName;
    AccContext.pu1_OctetList = au1AccContext;
    MEMCPY (AccContext.pu1_OctetList, "\0", 1);
    AccContext.i4_Length = 0;
    AccName.pu1_OctetList = au1AccName;
    SubTree.pu4_OidList = (UINT4 *) MemAllocMemBlk (gSnmpOidListPoolId);
    if (SubTree.pu4_OidList == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (SubTree.pu4_OidList, 0, MAX_OID_LENGTH);

    /* VACM Security Table entry SNMP_DEF_USER_3 */
    GrpName.i4_Length = (INT4) STRLEN (SNMP_DEF_USER_3);
    MEMCPY (GrpName.pu1_OctetList, SNMP_DEF_USER_3, GrpName.i4_Length);
    i4Return = VACMAddSecGrp (SNMP_USM, &GrpName);
    if (i4Return == SNMP_FAILURE)
    {
        MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *) SubTree.pu4_OidList);
        return SNMP_FAILURE;
    }
    pSecEntry = VACMGetSecGrp (SNMP_USM, &GrpName);
    if (pSecEntry == NULL)
    {
        MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *) SubTree.pu4_OidList);
        return SNMP_FAILURE;
    }
    else
    {
        pSecEntry->i4Status = SNMP_ROWSTATUS_ACTIVE;
        GrpName.i4_Length = (INT4) STRLEN (SNMP_DEF_GROUP_2);
        MEMCPY (GrpName.pu1_OctetList, SNMP_DEF_GROUP_2, GrpName.i4_Length);
        SNMPCopyOctetString (&(pSecEntry->Group), &(GrpName));
    }

    /* VACM Access Table AuthPriv */
    i4SecLevel = SNMP_AUTH_PRIV;
    AccName.i4_Length = (INT4) STRLEN (SNMP_DEF_ACCESS_1);
    MEMCPY (AccName.pu1_OctetList, SNMP_DEF_ACCESS_1, AccName.i4_Length);
    GrpName.i4_Length = (INT4) STRLEN (SNMP_DEF_GROUP_2);
    MEMCPY (GrpName.pu1_OctetList, SNMP_DEF_GROUP_2, GrpName.i4_Length);
    i4Return = VACMAddAccess (&GrpName, &AccContext, SNMP_USM, i4SecLevel);
    if (i4Return == SNMP_FAILURE)
    {
        MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *) SubTree.pu4_OidList);
        return SNMP_FAILURE;
    }
    pAccEntry = VACMGetAccess (&GrpName, &AccContext, SNMP_USM, i4SecLevel);
    if (pAccEntry == NULL)
    {
        MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *) SubTree.pu4_OidList);
        return SNMP_FAILURE;
    }
    else
    {
        SNMPCopyOctetString (&(pAccEntry->ReadName), &AccName);
        SNMPCopyOctetString (&(pAccEntry->WriteName), &AccName);
        SNMPCopyOctetString (&(pAccEntry->NotifyName), &AccName);
        pAccEntry->i4Status = SNMP_ROWSTATUS_ACTIVE;
    }

    /* VACM View Tree Table  SNMP_DEF_ACCESS_1 */
    AccName.i4_Length = (INT4) STRLEN (SNMP_DEF_ACCESS_1);
    MEMCPY (AccName.pu1_OctetList, SNMP_DEF_ACCESS_1, AccName.i4_Length);
    SubTree.pu4_OidList[0] = 1;
    SubTree.u4_Length = 1;
    i4Return = VACMViewTreeAdd (&AccName, &SubTree);
    if (i4Return == SNMP_FAILURE)
    {
        MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *) SubTree.pu4_OidList);
        return SNMP_FAILURE;
    }
    pViewEntry = VACMViewTreeGet (&AccName, &SubTree);
    if (pViewEntry == NULL)
    {
        MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *) SubTree.pu4_OidList);
        return SNMP_FAILURE;
    }
    else
    {
        pViewEntry->i4Status = SNMP_ROWSTATUS_ACTIVE;
    }

    /* VACM View Tree Table  SNMP_DEF_VIEW_2 */
    AccName.i4_Length = (INT4) STRLEN (SNMP_DEF_VIEW_2);
    MEMCPY (AccName.pu1_OctetList, SNMP_DEF_VIEW_2, AccName.i4_Length);
    SubTree.pu4_OidList[0] = 1;
    SubTree.u4_Length = 1;
    i4Return = VACMViewTreeAdd (&AccName, &SubTree);
    if (i4Return == SNMP_FAILURE)
    {
        MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *) SubTree.pu4_OidList);
        return SNMP_FAILURE;
    }
    pViewEntry = VACMViewTreeGet (&AccName, &SubTree);
    if (pViewEntry == NULL)
    {
        MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *) SubTree.pu4_OidList);
        return SNMP_FAILURE;
    }
    else
    {
        pViewEntry->i4Status = SNMP_ROWSTATUS_ACTIVE;
    }
    AccName.i4_Length = (INT4) STRLEN ("default");
    MEMCPY (AccName.pu1_OctetList, "default", AccName.i4_Length);
    VACMAddContext (&AccName);
    MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *) SubTree.pu4_OidList);
    return SNMP_SUCCESS;
}
