
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: stdsnmwr.h,v 1.4 2015/04/28 12:35:03 siva Exp $
 *
 * Description: Prototypes for std snmp wrapper routines.
 *******************************************************************/
#ifndef _STDSNMWRAP_H 
#define _STDSNMWRAP_H 
INT4 SysDescrGet (tSnmpIndex *, tRetVal *);
INT4 SysObjectIDGet (tSnmpIndex *, tRetVal *);
INT4 SysUpTimeGet (tSnmpIndex *, tRetVal *);
INT4 SysContactTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SysContactSet (tSnmpIndex *, tRetVal *);
INT4 SysContactGet (tSnmpIndex *, tRetVal *);
INT4 SysNameTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SysNameSet (tSnmpIndex *, tRetVal *);
INT4 SysNameGet (tSnmpIndex *, tRetVal *);
INT4 SysLocationTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SysLocationSet (tSnmpIndex *, tRetVal *);
INT4 SysLocationGet (tSnmpIndex *, tRetVal *);
INT4 SysServicesGet (tSnmpIndex *, tRetVal *);
INT4 SysORLastChangeGet (tSnmpIndex *, tRetVal *);
INT4 SysContactDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 SysNameDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 SysLocationDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);



INT4 SysORIndexGet (tSnmpIndex *, tRetVal *);
INT4 SysORIDGet (tSnmpIndex *, tRetVal *);
INT4 SysORDescrGet (tSnmpIndex *, tRetVal *);
INT4 SysORUpTimeGet (tSnmpIndex *, tRetVal *);
INT4 SnmpInPktsGet (tSnmpIndex *, tRetVal *);
INT4 SnmpOutPktsGet (tSnmpIndex *, tRetVal *);
INT4 SnmpInBadVersionsGet (tSnmpIndex *, tRetVal *);
INT4 SnmpInBadCommunityNamesGet (tSnmpIndex *, tRetVal *);
INT4 SnmpInBadCommunityUsesGet (tSnmpIndex *, tRetVal *);
INT4 SnmpInASNParseErrsGet (tSnmpIndex *, tRetVal *);
INT4 SnmpInTooBigsGet (tSnmpIndex *, tRetVal *);
INT4 SnmpInNoSuchNamesGet (tSnmpIndex *, tRetVal *);
INT4 SnmpInBadValuesGet (tSnmpIndex *, tRetVal *);
INT4 SnmpInReadOnlysGet (tSnmpIndex *, tRetVal *);
INT4 SnmpInGenErrsGet (tSnmpIndex *, tRetVal *);
INT4 SnmpInTotalReqVarsGet (tSnmpIndex *, tRetVal *);
INT4 SnmpInTotalSetVarsGet (tSnmpIndex *, tRetVal *);
INT4 SnmpInGetRequestsGet (tSnmpIndex *, tRetVal *);
INT4 SnmpInGetNextsGet (tSnmpIndex *, tRetVal *);
INT4 SnmpInSetRequestsGet (tSnmpIndex *, tRetVal *);
INT4 SnmpInGetResponsesGet (tSnmpIndex *, tRetVal *);
INT4 SnmpInTrapsGet (tSnmpIndex *, tRetVal *);
INT4 SnmpOutTooBigsGet (tSnmpIndex *, tRetVal *);
INT4 SnmpOutNoSuchNamesGet (tSnmpIndex *, tRetVal *);
INT4 SnmpOutBadValuesGet (tSnmpIndex *, tRetVal *);
INT4 SnmpOutGenErrsGet (tSnmpIndex *, tRetVal *);
INT4 SnmpOutGetRequestsGet (tSnmpIndex *, tRetVal *);
INT4 SnmpOutGetNextsGet (tSnmpIndex *, tRetVal *);
INT4 SnmpOutSetRequestsGet (tSnmpIndex *, tRetVal *);
INT4 SnmpOutGetResponsesGet (tSnmpIndex *, tRetVal *);
INT4 SnmpOutTrapsGet (tSnmpIndex *, tRetVal *);
INT4 SnmpEnableAuthenTrapsTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpEnableAuthenTrapsSet (tSnmpIndex *, tRetVal *);
INT4 SnmpEnableAuthenTrapsGet (tSnmpIndex *, tRetVal *);
INT4 SnmpEnableAuthenTrapsDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

INT4 SnmpSilentDropsGet (tSnmpIndex *, tRetVal *);
INT4 SnmpProxyDropsGet (tSnmpIndex *, tRetVal *);
INT4 SnmpSetSerialNoTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SnmpSetSerialNoSet (tSnmpIndex *, tRetVal *);
INT4 SnmpSetSerialNoGet (tSnmpIndex *, tRetVal *);
/* Mid Level SET Routine for All Objects (for Incremental Save only).  */


 /*  GetNext Function Prototypes */

INT4 GetNextIndexSysORTable( tSnmpIndex *, tSnmpIndex *);
INT4 SnmpSetSerialNoDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
#endif
