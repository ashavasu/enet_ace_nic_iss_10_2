/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: snmpsz.c,v 1.1 2015/04/28 12:35:02 siva Exp $
 *
 * Description:Mempools creation/deletion functions
 *******************************************************************/
#define _SNMPV3SZ_C
#include "snmpcmn.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
Snmpv3SizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < SNMPV3_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            (INT4) MemCreateMemPool (FsSNMPV3SizingParams[i4SizingId].
                                     u4StructSize,
                                     FsSNMPV3SizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(SNMPV3MemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            Snmpv3SizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
Snmpv3SzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsSNMPV3SizingParams);
    IssSzRegisterModulePoolId (pu1ModName, SNMPV3MemPoolIds);
    return OSIX_SUCCESS;
}

VOID
Snmpv3SizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < SNMPV3_MAX_SIZING_ID; i4SizingId++)
    {
        if (SNMPV3MemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (SNMPV3MemPoolIds[i4SizingId]);
            SNMPV3MemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
