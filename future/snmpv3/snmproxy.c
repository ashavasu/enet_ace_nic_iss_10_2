/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: snmproxy.c,v 1.2 2017/11/20 13:11:26 siva Exp $
 *
 * Description:Routines for Snmp agent Proxy table 
 *******************************************************************/

#include "snmpcmn.h"
#include "snmproxy.h"
#include "snprxtrn.h"
#include "fssocket.h"
#include "vcm.h"

/*********************************************************************
*  Function Name : SnmpInitProxyTable
*  Description   : Function Init Proxy Table 
*  Parameter(s)  : None
*  Return Values : None
*********************************************************************/
INT4
SnmpInitProxyTable ()
{
    UINT4               u4Index = SNMP_ZERO;
    MEMSET (gaProxyTableEntry, SNMP_ZERO, sizeof (gaProxyTableEntry));
    MEMSET (au1ProxyName, SNMP_ZERO, sizeof (au1ProxyName));
    MEMSET (au1ProxCntxEngID, SNMP_ZERO, sizeof (au1ProxCntxEngID));
    MEMSET (au1ProxContName, SNMP_ZERO, sizeof (au1ProxContName));
    MEMSET (au1ProxTgtPrmIn, SNMP_ZERO, sizeof (au1ProxTgtPrmIn));
    MEMSET (au1ProxSglTgtOut, SNMP_ZERO, sizeof (au1ProxSglTgtOut));
    MEMSET (au1ProxMulTgtOut, SNMP_ZERO, sizeof (au1ProxMulTgtOut));
    TMO_SLL_Init (&gProxyTableSll);
    for (u4Index = SNMP_ZERO; u4Index < MAX_PROXY_TABLE_ENTRY; u4Index++)
    {
        gaProxyTableEntry[u4Index].ProxyName.pu1_OctetList =
            au1ProxyName[u4Index];
        gaProxyTableEntry[u4Index].PrxContextEngineID.pu1_OctetList =
            au1ProxCntxEngID[u4Index];
        gaProxyTableEntry[u4Index].PrxContextName.pu1_OctetList =
            au1ProxContName[u4Index];
        gaProxyTableEntry[u4Index].PrxTargetParamsIn.pu1_OctetList =
            au1ProxTgtPrmIn[u4Index];
        gaProxyTableEntry[u4Index].PrxSingleTargetOut.pu1_OctetList =
            au1ProxSglTgtOut[u4Index];
        gaProxyTableEntry[u4Index].PrxMultipleTargetOut.pu1_OctetList =
            au1ProxMulTgtOut[u4Index];
        gaProxyTableEntry[u4Index].i4Status = SNMP_INACTIVE;
    }
    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : SNMPAddProxyEntry 
*  Description   : Function to Add the Proxy from the table
*  Parameter(s)  : pProxyName - Proxy Name
*  Return Values : SNMP_SUCCESS/SNMP_FAILURE
*********************************************************************/

INT4
SNMPAddProxyEntry (tSNMP_OCTET_STRING_TYPE * pProxyName)
{
    UINT4               u4Index = SNMP_ZERO;
    tSNMP_OCTET_STRING_TYPE OctetString;

    if (SNMPGetProxyEntryFromName (pProxyName) != NULL)
    {
        return SNMP_FAILURE;
    }

    for (u4Index = SNMP_ZERO; u4Index < MAX_PROXY_TABLE_ENTRY; u4Index++)
    {
        if (gaProxyTableEntry[u4Index].i4Status == SNMP_INACTIVE)
        {
            gaProxyTableEntry[u4Index].i4Status = UNDER_CREATION;
            gaProxyTableEntry[u4Index].i4Storage =
                SNMP3_STORAGE_TYPE_NONVOLATILE;
            SNMPCopyOctetString (&(gaProxyTableEntry[u4Index].ProxyName),
                                 pProxyName);
            gaProxyTableEntry[u4Index].ProxyType = SNMP_ZERO;
            OctetString.pu1_OctetList = (UINT1 *) "";
            OctetString.i4_Length = SNMP_ZERO;
            SNMPCopyOctetString (&(gaProxyTableEntry[u4Index].PrxContextName),
                                 &OctetString);
            SNMPCopyOctetString (&
                                 (gaProxyTableEntry[u4Index].
                                  PrxContextEngineID), &OctetString);
            gaProxyTableEntry[u4Index].PrxTargetParamsIn.i4_Length = SNMP_ZERO;
            gaProxyTableEntry[u4Index].PrxSingleTargetOut.i4_Length = SNMP_ZERO;
            gaProxyTableEntry[u4Index].PrxMultipleTargetOut.i4_Length =
                SNMP_ZERO;
            SNMPAddProxyEntrySll (&(gaProxyTableEntry[u4Index].link));
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/*********************************************************************
*  Function Name : SNMPAddProxyEntrySll 
*  Description   : Function to add the Proxy entry to the list
*  Parameter(s)  : pNode - Node to be added 
*  Return Values : None
*********************************************************************/

VOID
SNMPAddProxyEntrySll (tTMO_SLL_NODE * pNode)
{
    tProxyTableEntry   *pCurEntry = NULL;
    tProxyTableEntry   *pInEntry = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pPrevNode = NULL;
    pInEntry = (tProxyTableEntry *) pNode;

    TMO_SLL_Scan (&gProxyTableSll, pLstNode, tTMO_SLL_NODE *)
    {
        pCurEntry = (tProxyTableEntry *) pLstNode;

        if (SNMPCompareImpliedOctetString (&(pInEntry->ProxyName),
                                           &(pCurEntry->ProxyName)) ==
            SNMP_LESSER)
        {
            break;
        }
        pPrevNode = pLstNode;
    }
    TMO_SLL_Insert (&gProxyTableSll, pPrevNode, pNode);
}

/*********************************************************************
*  Function Name : SNMPGetProxyEntryFromName 
*  Description   : Function to get the corresponding entry form Proxy Table
*  Parameter(s)  : pProxyName - Proxy Name
*  Return Values : On Success to return the Entry
*                  on Failure to return NULL
*********************************************************************/
tProxyTableEntry   *
SNMPGetProxyEntryFromName (tSNMP_OCTET_STRING_TYPE * pProxyName)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tProxyTableEntry   *pProxy = NULL;

    TMO_SLL_Scan (&gProxyTableSll, pListNode, tTMO_SLL_NODE *)
    {
        pProxy = (tProxyTableEntry *) pListNode;
        if (pProxy->i4Status != SNMP_INACTIVE)
        {
            if (SNMPCompareExactMatchOctetString
                (pProxyName, &(pProxy->ProxyName)) == SNMP_EQUAL)
            {
                return pProxy;
            }
        }
    }
    return NULL;
}

/*********************************************************************
*  Function Name : SNMPGetProxyEntryFromCtxtId 
*  Description   : Function to get the corresponding entry form Proxy Table
*  Parameter(s)  : pContextEngineID - Context Engine ID
*  Return Values : On Success to return the Entry
*                  on Failure to return NULL
*********************************************************************/
tProxyTableEntry   *
SNMPGetProxyEntryFromCtxtId (tSNMP_OCTET_STRING_TYPE * pContextEngineID)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tProxyTableEntry   *pProxy = NULL;

    TMO_SLL_Scan (&gProxyTableSll, pListNode, tTMO_SLL_NODE *)
    {
        pProxy = (tProxyTableEntry *) pListNode;
        if (pProxy->i4Status != SNMP_INACTIVE)
        {
            if (SNMPCompareExactMatchOctetString
                (pContextEngineID, &(pProxy->PrxContextEngineID)) == SNMP_EQUAL)
            {
                return pProxy;
            }
        }
    }
    return NULL;
}

/*********************************************************************
*  Function Name : SNMPGetEntryCountFromPrxType 
*  Description   : Function to get the corresponding entry form Proxy Table
*  Parameter(s)  : pContextEngineID - Context Engine ID
*  Return Values : On Success to return the Entry
*                  on Failure to return NULL
*********************************************************************/
INT4
SNMPGetEntryCountFromPrxType (INT4 ProxyType)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tProxyTableEntry   *pProxy = NULL;
    INT4                i4Count = SNMP_ZERO;

    TMO_SLL_Scan (&gProxyTableSll, pListNode, tTMO_SLL_NODE *)
    {
        pProxy = (tProxyTableEntry *) pListNode;
        if (pProxy->i4Status != SNMP_INACTIVE)
        {
            if (pProxy->ProxyType == ProxyType)
            {
                i4Count++;
            }
        }
    }
    return i4Count;
}

/*********************************************************************
*  Function Name : SNMPGetProxyEntry 
*  Description   : Function to get the corresponding entry form Proxy Table
*  Parameter(s)  : pContextEngineID - Context Engine ID
*                 : pContextName    - Context Name
*                 : i4ProxyType - PDU Type (Read, Write, Trap, Inform)
*                 : pProxyEntry - Array of Entry
*  Return Values : On Success to return the Array of Entry
*                  on Failure to return NULL
*********************************************************************/
INT4
SNMPGetProxyEntry (tSNMP_OCTET_STRING_TYPE * pContextEngineID,
                   tSNMP_OCTET_STRING_TYPE * pContextName,
                   INT4 i4ProxyType, tProxEntryArray * pProxyEntry)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tProxyTableEntry   *pProxy = NULL;
    INT4                count = SNMP_ZERO;

    pProxyEntry->i4Count = SNMP_ZERO;

    TMO_SLL_Scan (&gProxyTableSll, pListNode, tTMO_SLL_NODE *)
    {
        pProxy = (tProxyTableEntry *) pListNode;

        if ((pProxy->i4Status == SNMP_ROWSTATUS_ACTIVE) &&
            (pProxy->ProxyType == i4ProxyType) && (count < MAX_PROXY_PDU_ENTRY))
        {
            if (SNMPCompareExactMatchOctetString (pContextEngineID,
                                                  &(pProxy->
                                                    PrxContextEngineID)) ==
                SNMP_EQUAL)
            {
                if (pProxy->PrxContextName.i4_Length)
                {
                    if (SNMPCompareExactMatchOctetString (pContextName,
                                                          &(pProxy->
                                                            PrxContextName)) ==
                        SNMP_EQUAL)
                    {
                        pProxyEntry->pEntry[count] =
                            (tProxyTableEntry *) pProxy;
                        count++;
                    }
                }
                else
                {
                    pProxyEntry->pEntry[count] = (tProxyTableEntry *) pProxy;
                    count++;
                }
            }
        }
    }

    pProxyEntry->i4Count = count;

    if (count == SNMP_ZERO)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : SNMPIsValidProxyEntry 
*  Description   : Function to check whether is there any valid ACTIVE
*                 : entry in the Proxy Table or not.
*  Parameter(s)  : pProxyName - Proxy Name
*  Return Values : Success will return the Proxy Name
*                : Failure will return NULL
*********************************************************************/
INT4
SNMPIsValidProxyEntry ()
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tProxyTableEntry   *pProxyEntry = NULL;

    TMO_SLL_Scan (&gProxyTableSll, pListNode, tTMO_SLL_NODE *)
    {
        pProxyEntry = (tProxyTableEntry *) pListNode;
        if (pProxyEntry->i4Status == SNMP_ACTIVE)
        {
            return SNMP_TRUE;
        }
    }
    return SNMP_FALSE;
}

/*********************************************************************
*  Function Name : SNMPGetFirstProxy 
*  Description   : Function to get the First Entry from the Proxy Table 
*  Parameter(s)  : None 
*  Return Values : Proxy Entry 
*********************************************************************/

tProxyTableEntry   *
SNMPGetFirstProxy ()
{
    tProxyTableEntry   *pProxyEntry = NULL;
    pProxyEntry = (tProxyTableEntry *) TMO_SLL_First (&gProxyTableSll);
    return pProxyEntry;
}

/*********************************************************************
*  Function Name : SNMPGetNextProxyEntry 
*  Description   : Function to get the next entry form Proxy Table
*  Parameter(s)  : pProxyName - Proxy Name
*  Return Values : Success will return the Proxy Name
*                  Failure will return NULL
*********************************************************************/
tProxyTableEntry   *
SNMPGetNextProxyEntry (tSNMP_OCTET_STRING_TYPE * pProxyName)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tProxyTableEntry   *pProxyEntry = NULL;

    TMO_SLL_Scan (&gProxyTableSll, pListNode, tTMO_SLL_NODE *)
    {
        pProxyEntry = (tProxyTableEntry *) pListNode;
        if (pProxyEntry->i4Status != SNMP_INACTIVE)
        {
            if (SNMPCompareImpliedOctetString
                (pProxyName, &(pProxyEntry->ProxyName)) == SNMP_LESSER)
            {
                return pProxyEntry;
            }
        }
    }
    return NULL;
}

/*********************************************************************
*  Function Name : SNMPDeleteProxyEntry 
*  Description   : Function to delete the Proxy entry 
*  Parameter(s)  : pProxyName - Proxy Name
*  Return Values : SNMP_SUCCESS/SNMP_FAILURE 
*********************************************************************/

INT4
SNMPDeleteProxyEntry (tSNMP_OCTET_STRING_TYPE * pProxyName)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tProxyTableEntry   *pProxyEntry = NULL;

    TMO_SLL_Scan (&gProxyTableSll, pListNode, tTMO_SLL_NODE *)
    {
        pProxyEntry = (tProxyTableEntry *) pListNode;
        if (SNMPCompareOctetString (pProxyName,
                                    &(pProxyEntry->ProxyName)) == SNMP_EQUAL)
        {
            pProxyEntry->i4Status = SNMP_INACTIVE;
            SNMPDelProxyEntrySll (&(pProxyEntry->link));
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/*********************************************************************
*  Function Name : SNMPDelProxyEntrySll 
*  Description   : Function to delete an entry from the list 
*  Parameter(s)  : pNode - Node to be deleted
*  Return Values : None
*********************************************************************/
VOID
SNMPDelProxyEntrySll (tTMO_SLL_NODE * pNode)
{
    TMO_SLL_Delete (&gProxyTableSll, pNode);
    return;
}

/*********************************************************************
*  Function Name : SNMPGetSecLevel 
*  Description   : Function to get the Security Level from the PDU
*  Parameter(s)  : pV3Pdu - V3 PDU 
*  Return Values : INT4 --> Result
*********************************************************************/
UINT4
SNMPGetSecLevel (tSNMP_V3PDU * pV3Pdu)
{
    UINT4               u4SecurityLevel = SNMP_ZERO;

    if (pV3Pdu == NULL)
    {
        SNMPTrace ("[PROXY]:pV3Pdu is NULL\n");
        return u4SecurityLevel;
    }

    if (pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO] == 0x05)
    {
        u4SecurityLevel = SNMP_AUTH_NOPRIV;
    }
    else if (pV3Pdu->MsgFlag.pu1_OctetList[SNMP_ZERO] == 0x07)
    {
        u4SecurityLevel = SNMP_AUTH_PRIV;
    }
    else
    {
        u4SecurityLevel = SNMP_NOAUTH_NOPRIV;
    }

    return u4SecurityLevel;
}

/*********************************************************************
*  Function Name : SNMPIsProxyOrAgent  
*  Description   : Function to find out whether to act as Proxy or
*                 : as an Agent
*  Parameter(s)  : pPdu - Incoin g Request PDU
*                 : i4pduVersion    - PDU Version
*                 : pSingleTargetOut - Target where to proxy the req.
*                 : pMulTargetOut - List of Targets
*                 : pIsProxy - TRUE --> Proxy, FALSE --> Agent
*  Return Values : Success -> Proxy or Agent found out
*                : Failure -> Any Failure
*********************************************************************/
INT4
SNMPIsProxyOrAgent (VOID *pPdu,
                    INT4 i4pduVersion,
                    INT2 i2_PduType,
                    tSNMP_OCTET_STRING_TYPE ** pSingleTargetOut,
                    tSNMP_OCTET_STRING_TYPE ** pMulTargetOut,
                    INT4 *pIsProxy, INT4 *pSendFailResp)
{
    tSNMP_TRAP_PDU     *pTrapPdu = NULL;
    tSNMP_NORMAL_PDU   *pPtr = NULL;
    tSNMP_V3PDU        *pV3Pdu = NULL;
    tSNMP_VAR_BIND     *pVarPtr = NULL;
    tCommunityMappingEntry *pCommEntry = NULL;
    tSnmpTgtParamEntry *pTgtParamEntry = NULL;
    tSNMP_OCTET_STRING_TYPE *pContextEngineID = NULL;
    tSNMP_OCTET_STRING_TYPE *pContextName = NULL;
    tSNMP_OCTET_STRING_TYPE *pCommunityStr = NULL;
    tProxEntryArray     ProxyEntry;
    INT4                i4Result = SNMP_SUCCESS;
    INT4                i4ProxyType = SNMP_ZERO;
    INT4                i4IsMatchFound = SNMP_TRUE;
    INT4                i4IsTrap = SNMP_FALSE;
    INT4                i4IsValidProxyEntry = SNMP_FALSE;
    INT4                i4Count = SNMP_ZERO;

    if (pPdu == NULL)
    {
        SNMPTrace ("[PROXY]: pPdu is NULL\n");
        return SNMP_FAILURE;
    }

    if ((i2_PduType == SNMP_PDU_TYPE_TRAP) && (i4pduVersion == VERSION1))
    {
        pTrapPdu = (tSNMP_TRAP_PDU *) pPdu;
        pCommunityStr = pTrapPdu->pCommunityStr;
        pVarPtr = pTrapPdu->pVarBindList;
    }
    else if ((i4pduVersion == VERSION1) || (i4pduVersion == VERSION2))
    {
        pPtr = (tSNMP_NORMAL_PDU *) pPdu;
        pCommunityStr = pPtr->pCommunityStr;
        pVarPtr = pPtr->pVarBindList;
    }
    else if (i4pduVersion == VERSION3)
    {
        pV3Pdu = (tSNMP_V3PDU *) pPdu;
        pPtr = &(pV3Pdu->Normalpdu);
        pVarPtr = pPtr->pVarBindList;
    }
    else
    {
        SNMPTrace ("[PROXY]:INVALID Version \n");
        return SNMP_FAILURE;
    }

    if ((i2_PduType == SNMP_PDU_TYPE_GET_REQUEST) ||
        (i2_PduType == SNMP_PDU_TYPE_GET_NEXT_REQUEST) ||
        (i2_PduType == SNMP_PDU_TYPE_GET_BULK_REQUEST))
    {
        i4ProxyType = PROXY_TYPE_READ;
    }
    else if (i2_PduType == SNMP_PDU_TYPE_SET_REQUEST)
    {
        i4ProxyType = PROXY_TYPE_WRITE;
    }
    else if ((i2_PduType == SNMP_PDU_TYPE_TRAP) ||
             (i2_PduType == SNMP_PDU_TYPE_SNMPV2_TRAP))
    {
        i4ProxyType = PROXY_TYPE_TRAP;
        i4IsTrap = SNMP_TRUE;
    }
    else if (i2_PduType == SNMP_PDU_TYPE_V2_INFORM_REQUEST)
    {
        i4ProxyType = PROXY_TYPE_INFORM;
    }
    else
    {
        /* Report and Response PDU type should not be received here */
        SNMPTrace ("[PROXY]:INVALID PduType received \n");
        return SNMP_FAILURE;
    }

    ProxyEntry.i4Count = SNMP_ZERO;

    if (pV3Pdu)                    /* If SNMP Version 3 */
    {
        SNMPTrace ("[PROXY]: Verifying the V3 PDU \n");
        pContextEngineID = &pV3Pdu->ContextID;
        pContextName = &pV3Pdu->ContextName;
    }
    else                        /* IF SNMP Version 1 or Version 2  */
    {
        SNMPTrace ("[PROXY]: Verifying the V1/ V2 PDU \n");

        pCommEntry = SNMPGetCommunityEntryFromName (pCommunityStr);

        if (pCommEntry == NULL)
        {
            SNMP_INR_BAD_COMMUNITY;
            SNMPTrace ("[PROXY]:No Matching in the community"
                       " Mapping Table\n");
            i4IsMatchFound = SNMP_FALSE;
        }
        else
        {
            pContextEngineID = &pCommEntry->ContextEngineID;
            pContextName = &pCommEntry->ContextName;
        }
    }

    /* Search the entry in the Proxy Table */
    if (i4IsMatchFound == SNMP_TRUE)
    {
        i4IsMatchFound = SNMP_FALSE;
        i4Result = SNMPGetProxyEntry (pContextEngineID, pContextName,
                                      i4ProxyType, &ProxyEntry);
        if (i4Result == SNMP_SUCCESS)
        {
            for (i4Count = SNMP_ZERO; i4Count < ProxyEntry.i4Count; i4Count++)
            {
                pTgtParamEntry =
                    SNMPGetTgtParamEntry (&
                                          ((tProxyTableEntry *) ProxyEntry.
                                           pEntry[i4Count])->PrxTargetParamsIn);

                if (pTgtParamEntry == NULL)
                {
                    SNMPTrace ("[PROXY]:No Matching Entry in the"
                               " Target Address Table\n");
                    continue;
                }

                SNMPARGTrace ("Reading the [%d] Entry in ProxyArray \n",
                              i4Count);
                SNMPMatchProxyEntry (pPdu, i4pduVersion, i2_PduType,
                                     &i4IsMatchFound, pCommEntry,
                                     pTgtParamEntry);

                if (i4IsMatchFound == SNMP_TRUE)
                {
                    break;
                }
            }
        }
    }                            /* Search in Proxy Table Ends */

    if (i4IsMatchFound == SNMP_TRUE)
    {
        *pSingleTargetOut =
            &(((tProxyTableEntry *) ProxyEntry.pEntry[i4Count])->
              PrxSingleTargetOut);

        *pMulTargetOut = &(((tProxyTableEntry *) ProxyEntry.pEntry[i4Count])->
                           PrxMultipleTargetOut);

        *pIsProxy = SNMP_TRUE;
        return SNMP_SUCCESS;
    }
    /* Search the root OID in the MIB registration Table */
    else if (pContextEngineID && (SNMPCompareOctetString (pContextEngineID,
                                                          &gSnmpEngineID) ==
                                  SNMP_EQUAL))
    {
        *pIsProxy = SNMP_FALSE;
        return SNMP_SUCCESS;
    }
    else
    {
        /* Prop Proxy Entry */
        i4IsMatchFound = SNMP_FALSE;
        i4Result = SNMP_SUCCESS;

        if (pVarPtr != NULL)
        {
            if ((pV3Pdu == NULL) && (pCommEntry == NULL))
            {
                i4Result = SNMP_FAILURE;
            }

            if (i4Result == SNMP_SUCCESS)
            {
                i4Result = SNMPGetPrpProxyEntry (pVarPtr->pObjName,
                                                 i4ProxyType, &ProxyEntry);

                if (i4Result == SNMP_SUCCESS)
                {
                    for (i4Count = SNMP_ZERO; i4Count < ProxyEntry.i4Count;
                         i4Count++)
                    {
                        pTgtParamEntry =
                            SNMPGetTgtParamEntry (&
                                                  ((tPrpProxyTableEntry *)
                                                   ProxyEntry.pEntry[i4Count])->
                                                  PrpPrxTargetParamsIn);

                        if (pTgtParamEntry == NULL)
                        {
                            SNMPTrace ("[PROXY]:No Matching Entry in the"
                                       " Target Address Table\n");
                            continue;
                        }

                        SNMPARGTrace ("Reading the [%d] Entry in ProxyArray \n",
                                      i4Count);

                        SNMPMatchProxyEntry (pPdu, i4pduVersion, i2_PduType,
                                             &i4IsMatchFound, pCommEntry,
                                             pTgtParamEntry);

                        if (i4IsMatchFound == SNMP_TRUE)
                        {
                            break;
                        }
                    }
                }
            }
        }
    }

    if (i4IsMatchFound == SNMP_TRUE)
    {
        *pSingleTargetOut =
            &(((tPrpProxyTableEntry *) ProxyEntry.pEntry[i4Count])->
              PrpPrxSingleTargetOut);

        *pMulTargetOut =
            &(((tPrpProxyTableEntry *) ProxyEntry.pEntry[i4Count])->
              PrpPrxMultipleTargetOut);

        *pIsProxy = SNMP_TRUE;
        return SNMP_SUCCESS;
    }
    /* Search the root OID in the MIB registration Table */
    else if (pContextEngineID && (SNMPCompareOctetString (pContextEngineID,
                                                          &gSnmpEngineID) ==
                                  SNMP_EQUAL))
    {
        *pIsProxy = SNMP_FALSE;
        return SNMP_SUCCESS;
    }

    i4IsValidProxyEntry = SNMPIsValidProxyEntry ();
    if (i4IsValidProxyEntry == SNMP_FALSE)
    {
        i4IsValidProxyEntry = SNMPIsValidPrpProxyEntry ();
    }
    if ((i4IsValidProxyEntry == SNMP_TRUE) && (i4IsTrap == SNMP_FALSE))
    {
        SNMPTrace ("[PROXY]:Incrementing the Proxy Drops Counter \n");
        SNMP_INR_PROXY_DROPS;

        pPtr->i4_ErrorStatus = SNMP_ERR_INCONSISTENT_VALUE;
        pPtr->i4_ErrorIndex = 1;    /* snmpProxyDropCounter OID to be stored 
                                       at the first index */
        *pSendFailResp = SNMP_TRUE;
    }
    else
    {
        SNMPTrace
            ("[PROXY]:No entry in Proxy Table or a TRAP request received\n");
        SNMPTrace ("[PROXY]:Not incrementing the Proxy Drops Counter but"
                   " Silent Counter \n");
    }

    SNMPProxyLogMessage (pPdu, i4pduVersion, i2_PduType);

    return SNMP_FAILURE;
}

/*********************************************************************
*  Function Name : SNMPMatchProxyEntry
*  Description   : Function to Match the Proxy Entry with TargetParamIn
*  Parameter(s)  : pPdu - Incoming PDU
*                 : i4pduVersion - PDU Version
*                 : i2_PduType - PDU Type
*  Return Values : MatchFound = TRUE if Entry Matches
*********************************************************************/
VOID
SNMPMatchProxyEntry (VOID *pPdu,
                     INT4 i4pduVersion,
                     INT2 i2_PduType,
                     INT4 *pIsMatchFound,
                     tCommunityMappingEntry * pCommEntry,
                     tSnmpTgtParamEntry * pTgtParamEntry)
{
    tSNMP_V3PDU        *pV3Pdu = NULL;
    INT2                i2ParamMPModel = SNMP_ZERO;
    UINT4               u4SecLevel = SNMP_ZERO;

    if (pPdu == NULL)
    {
        SNMPTrace ("[PROXY]: pPdu is NULL\n");
        return;
    }

    if ((i2_PduType == SNMP_PDU_TYPE_TRAP) && (i4pduVersion == VERSION1))
    {
        i2ParamMPModel = SNMP_MPMODEL_V1;
    }
    else if ((i4pduVersion == VERSION1) || (i4pduVersion == VERSION2))
    {
        if (i4pduVersion == VERSION1)
        {
            i2ParamMPModel = SNMP_MPMODEL_V1;
        }
        else
        {
            i2ParamMPModel = SNMP_MPMODEL_V2C;
        }
    }
    else                        /* VERSION3 */
    {
        pV3Pdu = (tSNMP_V3PDU *) pPdu;
        i2ParamMPModel = SNMP_MPMODEL_V3;
    }

    if (pV3Pdu)                    /* If SNMP Version 3 */
    {
        if (pV3Pdu->u4MsgSecModel != (UINT4) pTgtParamEntry->i2ParamSecModel)
        {
            SNMPTrace ("[PROXY]: Security Model doesn't Matches\n");
        }
        else if (pTgtParamEntry->i2ParamMPModel != i2ParamMPModel)
        {
            SNMPTrace ("[PROXY]: MP Model doesn't Matches\n");
        }
        else if (SNMPCompareExactMatchOctetString
                 (&pTgtParamEntry->ParamSecName,
                  &(pV3Pdu->MsgSecParam.MsgUsrName)) != SNMP_EQUAL)
        {
            SNMPTrace ("[PROXY]: Security Name doesn't Matches\n");
        }
        else
        {
            u4SecLevel = SNMPGetSecLevel (pV3Pdu);
            if (u4SecLevel == (UINT4) pTgtParamEntry->i4ParamSecLevel)
            {
                SNMPTrace ("[PROXY]: Entry in the proxy Table Found. \n");
                *pIsMatchFound = SNMP_TRUE;
            }
            else
            {
                SNMPTrace ("[PROXY]: Security Name doesn't Matches\n");
            }
        }

    }
    else                        /* IF SNMP Version 1 or Version 2  */
    {
        if (SNMPCompareExactMatchOctetString (&pTgtParamEntry->ParamSecName,
                                              &(pCommEntry->SecurityName)) !=
            SNMP_EQUAL)
        {
            SNMPTrace ("[PROXY]: Security Name doesn't Matches\n");
        }
        else if (pTgtParamEntry->i4ParamSecLevel != SNMP_NOAUTH_NOPRIV)
        {
            SNMPTrace ("[PROXY]: Security Level doesn't Matches\n");
        }
        else
        {
            if (pTgtParamEntry->i2ParamMPModel == i2ParamMPModel)
            {
                SNMPTrace ("[PROXY]: Entry in the proxy Table Found. \n");
                *pIsMatchFound = SNMP_TRUE;
            }
            else
            {
                SNMPTrace ("[PROXY]: MP Model doesn't Matches\n");
            }
        }
    }

    return;
}

/*********************************************************************
*  Function Name : SNMPProxyNotifyHandle 
*  Description   : Function to Handle the Proxy Notify
*  Parameter(s)  : pPdu - Incoming PDU
*                 : i4pduVersion - PDU Version
*                 : pMulTargetOut - Multiple Target Out Parameter
*                 : AppCallback - Application CallBack Function
*  Return Values : Success -> If Successfully sent to the Targets
*                : Failure -> Any Failure
*********************************************************************/
INT4
SNMPProxyNotifyHandle (VOID *pPdu,
                       INT4 i4pduVersion,
                       tSNMP_OCTET_STRING_TYPE * pMulTargetOut,
                       SnmpAppNotifCb AppCallback)
{
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
    UINT4               u4AgentAddr = SNMP_ZERO;
#endif
    tSNMP_NORMAL_PDU   *pPtr = NULL;
    tSNMP_TRAP_PDU     *pTrapPdu = NULL;
    tSNMP_V3PDU        *pV3Pdu = NULL;
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    tSnmpTgtParamEntry *pSnmpTgtParamEntry = NULL;
    tSNMP_PROXY_CACHE  *pProxyCache = NULL;
    tProxyCookie        Cookie;
    INT4                i4Result = SNMP_SUCCESS;
    INT4                i4OutVersion;
    INT4                i4MsgLen = 0;
    INT4                i4IsEntryCached = SNMP_FALSE;
    INT2                i2_InPduType = SNMP_ZERO;
    INT2                i2OutPduType = SNMP_ZERO;
    INT2                i2IsInformSent = SNMP_FALSE;
    UINT4               u4ManagerAddr = SNMP_ZERO;
    UINT4               u4_RequestID = SNMP_ZERO;
    VOID               *pOutPtr = NULL, *pInformPdu = NULL;
    UINT1              *pu1Msg = NULL;
#ifdef IP6_WANTED
    tSNMP_OCTET_STRING_TYPE *pIp6ManagerAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pIp6AgentAddr = NULL;
#endif

    if ((pPdu == NULL) || (pMulTargetOut == NULL))
    {
        SNMP_TRC (SNMP_FAILURE_TRC, "SNMPProxyNotifyHandle: "
                  "pPdu or pMulTargetOut is NULL\r\n");
        return SNMP_FAILURE;
    }

    if (i4pduVersion == VERSION1)
    {
        pTrapPdu = (tSNMP_TRAP_PDU *) pPdu;
        i2_InPduType = SNMP_PDU_TYPE_TRAP;
    }
    else if (i4pduVersion == VERSION2)
    {
        pPtr = (tSNMP_NORMAL_PDU *) pPdu;
        i2_InPduType = pPtr->i2_PduType;
    }
    else if (i4pduVersion == VERSION3)
    {
        pV3Pdu = (tSNMP_V3PDU *) pPdu;
        pPtr = &(pV3Pdu->Normalpdu);
        i2_InPduType = pPtr->i2_PduType;
    }
    else
    {
        SNMP_TRC1 (SNMP_FAILURE_TRC, "SNMPProxyNotifyHandle: "
                   "INVALID Version[%d]\r\n", i4pduVersion);
        return SNMP_FAILURE;
    }

    u4_RequestID = SNMPGetRequestID ();

    while ((pSnmpTgtAddrEntry =
            SNMPGetTgtAddrTagEntry (pMulTargetOut, pSnmpTgtAddrEntry)) != NULL)
    {
        pSnmpTgtParamEntry = SNMPGetTgtParamEntry (&(pSnmpTgtAddrEntry->
                                                     AddrParams));

        if (pSnmpTgtParamEntry == NULL)
        {
            SNMP_TRC (SNMP_FAILURE_TRC, "SNMPProxyNotifyHandle: "
                      "SNMP Target Param is NULL\r\n");
            continue;
        }
        if (pSnmpTgtAddrEntry->Address.i4_Length == SNMP_IPADDR_LEN)
        {
            MEMCPY (&u4ManagerAddr,
                    pSnmpTgtAddrEntry->Address.pu1_OctetList, sizeof (UINT4));
            u4ManagerAddr = OSIX_NTOHL (u4ManagerAddr);
        }
        else if (pSnmpTgtAddrEntry->Address.i4_Length == SNMP_IP6ADDR_LEN)
        {
#ifdef IP6_WANTED
            /*Allocate octet string memory during first iteration. 
             * Next iteration onwards re-initialize with 
             * current pSnmpTgtAddrEntry->Address */

            if (pIp6ManagerAddr == NULL)
            {
                if ((pIp6ManagerAddr =
                     allocmem_octetstring (SNMP_MAX_OCTETSTRING_SIZE)) == NULL)
                {
                    SNMP_TRC (SNMP_FAILURE_TRC, "SNMPProxyNotifyHandle: "
                              "Memory allocation failed\r\n");
                    continue;
                }
            }
            MEMSET (pIp6ManagerAddr->pu1_OctetList, 0,
                    SNMP_MAX_OCTETSTRING_SIZE);
            SNMPCopyOctetString (pIp6ManagerAddr,
                                 &(pSnmpTgtAddrEntry->Address));
#endif
        }

        if (pSnmpTgtParamEntry->i2ParamMPModel == SNMP_MPMODEL_V1)
        {
            if (i4pduVersion != VERSION1)
            {
                continue;
            }
            i4OutVersion = VERSION1;
            i2OutPduType = SNMP_PDU_TYPE_TRAP;
        }
        else if (pSnmpTgtParamEntry->i2ParamMPModel == SNMP_MPMODEL_V2C)
        {
            if (i4pduVersion == VERSION3)
            {
                continue;
            }
            i4OutVersion = VERSION2;
            if ((i2_InPduType == SNMP_PDU_TYPE_TRAP) ||
                (i2_InPduType == SNMP_PDU_TYPE_SNMPV2_TRAP))
            {
                i2OutPduType = SNMP_PDU_TYPE_SNMPV2_TRAP;
            }
            else
            {
                i2OutPduType = SNMP_PDU_TYPE_V2_INFORM_REQUEST;
            }
        }
        else
        {
            i4OutVersion = VERSION3;
            if ((i2_InPduType == SNMP_PDU_TYPE_TRAP) ||
                (i2_InPduType == SNMP_PDU_TYPE_SNMPV2_TRAP))
            {
                i2OutPduType = SNMP_PDU_TYPE_SNMPV2_TRAP;
            }
            else
            {
                i2OutPduType = SNMP_PDU_TYPE_V2_INFORM_REQUEST;
            }
        }

        i4Result = SNMPProxyTranslation (pPdu,
                                         &pOutPtr,
                                         (UINT4) i4pduVersion,
                                         (UINT4) i4OutVersion,
                                         i2_InPduType,
                                         &i2OutPduType,
                                         &pSnmpTgtAddrEntry->AddrName,
                                         u4_RequestID, NULL);

        if (i4Result == SNMP_FAILURE)
        {
            SNMP_TRC (SNMP_FAILURE_TRC, "SNMPProxyNotifyHandle: "
                      "Translation Failed, moving with next entry\r\n");
            SNMPFreeTrapPdu (&pOutPtr, i4OutVersion);
            continue;
        }

        /* Make a Normal PDU for the passing the SNMPSendInformToManager()
           only for the first time in case of INFORM */
        if (i2OutPduType == SNMP_PDU_TYPE_V2_INFORM_REQUEST)
        {
            if (i4OutVersion == VERSION3)
            {
                i4Result = SNMPProxyTranslation (pPdu, &pInformPdu,    /* Making another V2 PDU */
                                                 (UINT4) i4pduVersion,
                                                 VERSION2,
                                                 i2_InPduType,
                                                 &i2OutPduType,
                                                 &pSnmpTgtAddrEntry->AddrName,
                                                 u4_RequestID, NULL);

                if (i4Result == SNMP_FAILURE)
                {
                    SNMP_TRC1 (SNMP_FAILURE_TRC, "SNMPProxyNotifyHandle: "
                               "Translation Failed for [%d], moving with next entry\r\n",
                               i2OutPduType);
                    SNMPFreeTrapPdu (&pOutPtr, i4OutVersion);
                    SNMPFreeTrapPdu (&pInformPdu, VERSION2);
                    pInformPdu = NULL;
                    continue;
                }
            }
            else                /* VERSION 2 */
            {
                pInformPdu = pOutPtr;
            }
        }

        if ((i4IsEntryCached == SNMP_FALSE) &&
            (i2OutPduType == SNMP_PDU_TYPE_V2_INFORM_REQUEST))
        {
            /* Call Hashing Function here only once to send INFORM
               for all the  Managers */
            i4Result = SNMPProxyFillCacheData (pPdu, (UINT4) i4pduVersion, i2_InPduType, pOutPtr, (UINT4) i4OutVersion, i2OutPduType, SNMPGetManagerAddr (),    /* Target from request have arrived */
                                               SNMPGetManagerPort (),
                                               SNMPGetRequestID (),
                                               &pProxyCache);

            if ((i4Result == SNMP_FAILURE) || (pProxyCache == NULL))
            {
                SNMP_TRC (SNMP_FAILURE_TRC, "SNMPProxyNotifyHandle: "
                          "Filling cache data Failed, moving with next entry\r\n");
                SNMPFreeTrapPdu (&pOutPtr, i4OutVersion);
                if (i4OutVersion != VERSION2)
                {
                    SNMPFreeTrapPdu (&pInformPdu, VERSION2);
                }
                continue;
            }
            else
            {
                MEMSET (&Cookie, 0, sizeof (Cookie));
                Cookie.pProxyCache = pProxyCache;
                Cookie.pRespPdu = NULL;
                Cookie.pInformNode = NULL;
                Cookie.pInformHostNode = NULL;
                i4IsEntryCached = SNMP_TRUE;
            }
        }
        else
        {
            SNMP_TRC (SNMP_FAILURE_TRC, "SNMPProxyNotifyHandle: "
                      "Not Caching as Entry Already Cached or"
                      "a TRAP Request\r\n");
        }

        if (i4OutVersion == VERSION1)
        {
            if (pSnmpTgtAddrEntry->Address.i4_Length == SNMP_IPADDR_LEN)
            {
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
                /* Get the source address of the agent entity depending upon
                 * Manager Address. This function to be ported if FutureIP
                 * is not used */
                if ((NetIpv4GetSrcAddrToUseForDest
                     (u4ManagerAddr, &u4AgentAddr)) == NETIPV4_FAILURE)
                {
                    SNMPFreeTrapPdu (&pOutPtr, i4OutVersion);
                    SNMPFreeTrapPdu (&pInformPdu, VERSION2);
#ifdef IP6_WANTED
                    free_octetstring (pIp6ManagerAddr);
#endif
                    return SNMP_FAILURE;
                }
                u4AgentAddr = OSIX_HTONL (u4AgentAddr);
                MEMCPY (pTrapPdu->pAgentAddr->pu1_OctetList,
                        (UINT1 *) &u4AgentAddr, sizeof (UINT4));
#endif
            }
            else if (pSnmpTgtAddrEntry->Address.i4_Length == SNMP_IP6ADDR_LEN)
            {
#ifdef IP6_WANTED
                if ((pIp6AgentAddr =
                     allocmem_octetstring (SNMP_IP6ADDR_LEN)) == NULL)
                {
                    SNMP_TRC (SNMP_FAILURE_TRC, "SNMPProxyNotifyHandle: "
                              "Memory allocation failed\r\n");
                    SNMPFreeTrapPdu (&pOutPtr, i4OutVersion);
                    SNMPFreeTrapPdu (&pInformPdu, VERSION2);
                    continue;
                }
                else
                {
                    NetIpv6GetSrcAddrForDestAddr (VCM_DEFAULT_CONTEXT,
                                                  (tIp6Addr *) (VOID *)
                                                  pIp6ManagerAddr->
                                                  pu1_OctetList,
                                                  (tIp6Addr *) (VOID *)
                                                  pIp6AgentAddr->pu1_OctetList);
                    SNMPCopyOctetString (pTrapPdu->pAgentAddr, pIp6AgentAddr);
                    free_octetstring (pIp6AgentAddr);
                }
#endif
            }

            SNMPEncodeV1TrapPacket ((tSNMP_TRAP_PDU *) pOutPtr, &pu1Msg,
                                    &i4MsgLen);
        }
        else if (i4OutVersion == VERSION2)
        {
            SNMPEncodeV2TrapPacket ((tSNMP_NORMAL_PDU *) pOutPtr, &pu1Msg,
                                    &i4MsgLen, (INT4) i2OutPduType);
        }
        else                    /* VERSION3 */
        {
            SNMPEncodeV3Message ((tSNMP_V3PDU *) pOutPtr, &pu1Msg, &i4MsgLen,
                                 (INT4) i2OutPduType);
        }

        /* Dont free the OutPtr in case V2 and INFORM for the first time
           because we will be using the pOutPtr as pInformPdu in that case */
        if (!((i4OutVersion == VERSION2) &&
              (i2OutPduType == SNMP_PDU_TYPE_V2_INFORM_REQUEST)))
        {
            SNMPFreeTrapPdu (&pOutPtr, i4OutVersion);
        }

        if (pu1Msg != NULL)
        {
            if (pSnmpTgtAddrEntry->Address.i4_Length == SNMP_IP6ADDR_LEN)
            {
#ifdef IP6_WANTED
                SNMPSendTrap6ToManager (pu1Msg, i4MsgLen, pIp6ManagerAddr);
#endif
            }
            else if (pSnmpTgtAddrEntry->Address.i4_Length == SNMP_IPADDR_LEN)
            {
                if ((i2_InPduType == SNMP_PDU_TYPE_TRAP) ||
                    (i2_InPduType == SNMP_PDU_TYPE_SNMPV2_TRAP))
                {
                    SNMPSendTrapToManager (pu1Msg, i4MsgLen, u4ManagerAddr,
                                           pSnmpTgtAddrEntry->u2Port);
                }
                else
                {
                    SNMP_INFORM_LOCK ();
                    i4Result = (INT4) SNMPSendInformToManager
                        (pu1Msg,
                         i4MsgLen,
                         pSnmpTgtAddrEntry,
                         NULL,
                         u4_RequestID,
                         SNMP_ZERO,
                         SNMP_FIRST_TRANSMIT,
                         pInformPdu, AppCallback, (VOID *) &Cookie);

                    if (i4Result == SNMP_SUCCESS)
                    {
                        i2IsInformSent = SNMP_TRUE;
                    }

                    SNMP_INFORM_UNLOCK ();
                }
            }

            MemReleaseMemBlock (gSnmpPktPoolId, (UINT1 *) pu1Msg);
            pu1Msg = NULL;

        }

        SNMPFreeTrapPdu (&pInformPdu, VERSION2);
        continue;
    }

#ifdef IP6_WANTED
    free_octetstring (pIp6ManagerAddr);
#endif

    if (i2OutPduType == SNMP_PDU_TYPE_V2_INFORM_REQUEST)
    {
        if ((i4IsEntryCached == SNMP_TRUE) && (i2IsInformSent == SNMP_FALSE))
        {
            SNMPProxyCacheDeleteNode (pProxyCache->
                                      ProxyCmnCache.u4_NewRequestID);
        }

        if ((i4IsEntryCached == SNMP_FALSE) || (i2IsInformSent == SNMP_FALSE))
        {
            SNMP_INR_SILENT_DROPS;
        }
    }

    return SNMP_FAILURE;
}

/*********************************************************************
*  Function Name : SNMPFreeTrapPdu 
*  Description   : Function to Free the Given PDU
*  Parameter(s)  : pPdu - PDU to be Free
*                 : i4pduVersion - Version of the PDU
*                 : i2_PduType - PDU Type
*  Return Values : Success -> If Successfully sent to the Targets
*                : Failure -> Any Failure
*********************************************************************/
INT4
SNMPFreeTrapPdu (VOID **pPdu, INT4 i4pduVersion)
{
    tSNMP_TRAP_PDU     *pTrapPdu = NULL;
    tSNMP_NORMAL_PDU   *pPtr = NULL;
    tSNMP_V3PDU        *pV3Pdu = NULL;

    if ((pPdu == NULL) || (*pPdu == NULL))
    {
        return SNMP_SUCCESS;
    }

    if (i4pduVersion == VERSION1)
    {
        pTrapPdu = (tSNMP_TRAP_PDU *) * pPdu;
        SNMPFreeV1Trap (pTrapPdu);
    }
    else if (i4pduVersion == VERSION2)
    {
        pPtr = (tSNMP_NORMAL_PDU *) * pPdu;
        SNMPFreeV2Trap (pPtr);
    }
    else if (i4pduVersion == VERSION3)
    {
        pV3Pdu = (tSNMP_V3PDU *) * pPdu;
        pPtr = &(pV3Pdu->Normalpdu);
        FREE_SNMP_VARBIND_LIST (pPtr->pVarBindList);
        /* SNMPFreeV2Trap (pPtr); */
        MemReleaseMemBlock (gSnmpV3PduPoolId, (UINT1 *) pV3Pdu);
    }
    else
    {
        SNMPTrace ("[PROXY]:INVALID Version \n");
        return SNMP_FAILURE;
    }

    *pPdu = NULL;

    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : SNMPProxyInformCallback
 *  Description     : Callback Function to handle the Proxy Inform 
 *  Input           : pV3Pdu - V3 Protocol Data Unit
 *  Output          : None
 *  Returns         : SNMP_SUCCESS or SNMP_FAILURE
 ************************************************************************/
VOID
SNMPProxyInformCallback (tSnmpInfmStatusInfo * pSnmpInfmStatusInfo)
{

    INT4                i4MsgLen = 0, i4Result = SNMP_FAILURE;
    INT4                i4Version = SNMP_ZERO;
    INT4                i4CheckClearCache = SNMP_FALSE;
    INT4                i4ClearNode = SNMP_FALSE;
    tSNMP_PROXY_CACHE  *pProxyCache = NULL;
    tProxyCookie       *pCookie = NULL;
    VOID               *pOutPtr = NULL;
    tSNMP_NORMAL_PDU   *pPdu = NULL;
    UINT1              *pu1Msg = NULL;
    INT2                i2OutPduType = SNMP_ZERO;
    tSnmpInformMsgNode *pInformMsgNode = NULL;
    tInformNode        *pInformHostNode = NULL;
    tTMO_SLL           *pInformList = NULL;
    UINT4               u4IpAddr = 0;
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    struct sockaddr_in  SockAddr;

    if (pSnmpInfmStatusInfo == NULL)
    {
        SNMPTrace ("[PROXY]: pSnmpInfmStatusInfo is NULL \n");
        return;
    }

    pCookie = (tProxyCookie *) pSnmpInfmStatusInfo->pAppCookie;

    if (pCookie == NULL)
    {
        SNMPTrace ("[PROXY]: pCookie is NULL \n");
        return;
    }

    pProxyCache = pCookie->pProxyCache;
    pPdu = pCookie->pRespPdu;
    pInformMsgNode = pCookie->pInformNode;
    pInformHostNode = pCookie->pInformHostNode;
    i2OutPduType = SNMP_PDU_TYPE_V2_INFORM_RESPONSE;

    SNMPARGTrace ("[PROXY]: pSnmpInfmStatusInfo->u4AckStatus = %u \n",
                  pSnmpInfmStatusInfo->u4AckStatus);

    if (pSnmpInfmStatusInfo->u4AckStatus == SNMP_INFORM_ACK_RECEIVED)
    {
        if ((pPdu == NULL) || (pProxyCache == NULL))
        {
            SNMPTrace ("[PROXY]: Input PDU or pProxyCache is NULL \n");
            return;
        }

        if (pPdu->i4_ErrorStatus == SNMP_ERR_NO_ERROR)
        {
            if (pProxyCache->ProxyCmnCache.u1_ReqResendFlag != SNMP_TRUE)
            {

                i4Result = SNMPProxyTranslation (pPdu,
                                                 &pOutPtr,
                                                 VERSION2,
                                                 pProxyCache->ProxyCmnCache.
                                                 u4_RecVersion,
                                                 pPdu->i2_PduType,
                                                 &i2OutPduType, NULL,
                                                 pProxyCache->ProxyCmnCache.
                                                 u4_RecRequestID, pProxyCache);

            }
            else
            {
                SNMPTrace ("[PROXY]: Response already sent \n");
            }

            /* Encode here and then send it to the Agent */
            if (i4Result == SNMP_SUCCESS)
            {
                i4Version = (INT4) pProxyCache->ProxyCmnCache.u4_RecVersion;

                if (i4Version == VERSION2)
                {
                    SNMPEncodeGetPacket ((tSNMP_NORMAL_PDU *) pOutPtr, &pu1Msg,
                                         &i4MsgLen, (INT4) i2OutPduType);
                }
                else            /* VERSION3 */
                {
                    SNMPEncodeV3Message ((tSNMP_V3PDU *) pOutPtr, &pu1Msg,
                                         &i4MsgLen, (INT4) i2OutPduType);
                }

                MEMSET (&SockAddr, SNMP_ZERO, sizeof (SockAddr));
                SockAddr.sin_family = AF_INET;
                SockAddr.sin_addr.s_addr =
                    OSIX_HTONL (pProxyCache->ProxyCmnCache.u4_TargetInAddr);
                SockAddr.sin_port =
                    OSIX_HTONS (pProxyCache->ProxyCmnCache.u4_TargetInPort);

                if (sendto (gi4ProxySocket, pu1Msg, i4MsgLen, SNMP_ZERO,
                            (struct sockaddr *) &SockAddr,
                            sizeof (SockAddr)) == SNMP_NOT_OK)
                {
                    SNMPTrace
                        ("[PROXY]: Unable to send Notification Response to Agent\n");
                }
                else
                {
                    SNMPTrace ("[PROXY]: Inform Response sent to Agent \n");
                    pProxyCache->ProxyCmnCache.u1_ReqResendFlag = SNMP_TRUE;
                }
                MemReleaseMemBlock (gSnmpPktPoolId, (UINT1 *) pu1Msg);

            }
            else
            {
                if (pProxyCache->ProxyCmnCache.u1_ReqResendFlag != SNMP_TRUE)
                {
                    SNMPTrace
                        ("[PROXY]: INFORM Resp Failure due to Translation Failure \n");
                }
            }
        }

        SNMPFreeTrapPdu (&pOutPtr, i4Version);
        i4CheckClearCache = SNMP_TRUE;
    }
    else if (pSnmpInfmStatusInfo->u4AckStatus == SNMP_INFORM_NOT_ACKED)
    {
        i4CheckClearCache = SNMP_TRUE;
    }
    else if (pSnmpInfmStatusInfo->u4AckStatus == SNMP_INFORM_MSG_TX)
    {
        pCookie->pInformHostNode = NULL;
        SNMPTrace ("[PROXY]: Message sent to the Manager \n");
    }
    else if (pSnmpInfmStatusInfo->u4AckStatus == SNMP_INFORM_MSG_RE_TX)
    {
        pCookie->pInformHostNode = NULL;
        SNMPTrace ("[PROXY]: Message re-transmitted to the Manager \n");
    }
    else if (pSnmpInfmStatusInfo->u4AckStatus == SNMP_INFORM_NOT_SENT)
    {
        SNMPTrace ("[PROXY]:  Failed to send the Message to the Manager \n");
        if (pInformHostNode)
        {
            i4CheckClearCache = SNMP_TRUE;
            i4ClearNode = SNMP_TRUE;
            /* There could be cases where pInformMsgNode might be NULL,
               so fetch the Node from pInformHostNode */
            pInformMsgNode = pInformHostNode->pInformNode;
        }
    }
    else
    {
        SNMPTrace ("[PROXY]: Invalid AckStatus received \n");
        return;
    }

    if (i4CheckClearCache == SNMP_TRUE)
    {
        if (pInformMsgNode)        /* Node in the Global List */
        {
            SNMPARGTrace ("[PROXY]: InformNode MrgCnt = %u \n",
                          pInformMsgNode->u4MgrCnt);
            if (pInformMsgNode->u4MgrCnt == SNMP_ONE)
            {
                if (pProxyCache)
                {
                    SNMPTrace ("[PROXY]: Deleting the cache Entry \n");
                    SNMPProxyCacheDeleteNode (pProxyCache->
                                              ProxyCmnCache.u4_NewRequestID);
                }

            }

            if (i4ClearNode == SNMP_TRUE)
            {
                /* Scan for the Trap table Index and Request Id in pending requests. */
                u4IpAddr = OSIX_NTOHL (pSnmpInfmStatusInfo->u4MgrAddr);
                pSnmpTgtAddrEntry = SnmpGetManagerTargetAddr (u4IpAddr);

                /* First delete the Inform Msg Node from Global List */
                SnmpRemoveInformRequestMsgNode (pInformHostNode->pInformNode);
                pInformHostNode->pInformNode = NULL;
                if (pSnmpTgtAddrEntry)
                {
                    pInformList = &(pSnmpTgtAddrEntry->pInformList);
                    if (pInformList)
                    {
                        TMO_SLL_Delete (pInformList,
                                        &pInformHostNode->NextEntry);
                    }
                }

                MemReleaseMemBlock (gSnmpInformNodePoolId,
                                    (UINT1 *) pInformHostNode);
                pInformHostNode = NULL;
            }
        }
        else
        {
            SNMPTrace ("[PROXY]: pInformMsgNode is NULL \n");
        }
    }

    return;
}

/*********************************************************************
*  Function Name : SNMPProxyLogMessage 
*  Description   : Function to Log the PDU received
*  Parameter(s)  : pPdu - PDU to be print
*                 : i4pduVersion - Pdu Version
*                 : i2_PduType - Pdu Version
*  Return Values : VOID
*********************************************************************/
VOID
SNMPProxyLogMessage (VOID *pPdu, INT4 i4pduVersion, INT2 i2_PduType)
{
    tSNMP_TRAP_PDU     *pTrapPdu = NULL;
    tSNMP_NORMAL_PDU   *pPtr = NULL;
    tSNMP_V3PDU        *pV3Pdu = NULL;

    if (pPdu == NULL)
    {
        SNMPTrace (" pPdu is NULL\n");
        return;
    }

    if ((i2_PduType == SNMP_PDU_TYPE_TRAP) && (i4pduVersion == VERSION1))
    {
        pTrapPdu = (tSNMP_TRAP_PDU *) pPdu;
    }
    else if ((i4pduVersion == VERSION1) || (i4pduVersion == VERSION2))
    {
        pPtr = (tSNMP_NORMAL_PDU *) pPdu;
    }
    else if (i4pduVersion == VERSION3)
    {
        pV3Pdu = (tSNMP_V3PDU *) pPdu;
        pPtr = &(pV3Pdu->Normalpdu);
    }
    else
    {
        SNMPTrace ("INVALID Version \n");
        return;
    }

    SNMPTrace ("\n ---------------DROPPED MESSAGE--------------- \n");

    if (pTrapPdu)
    {
        SNMPPrintTrapPdu (pTrapPdu);
    }
    else if (pV3Pdu)
    {
        SNMPPrintV3Pdu (pV3Pdu);
    }
    else                        /* pPtr */
    {
        SNMPPrintV2Pdu (pPtr);
    }

    return;
}

/*********************************************************************
*  Function Name : SNMPPrintTrapPdu 
*  Description   : Function to Log the TRAP PDU received
*  Parameter(s)  : pPdu - PDU to be print
*  Return Values : VOID
*********************************************************************/
VOID
SNMPPrintTrapPdu (tSNMP_TRAP_PDU * pTrapPdu)
{
    if (pTrapPdu == NULL)
    {
        return;
    }

    SNMPARGTrace (" u4_Version = %u \n", pTrapPdu->u4_Version);
    SNMPARGTrace (" u4_TimeStamp = %u \n", pTrapPdu->u4_TimeStamp);
    SNMPARGTrace (" i4_GenericTrap = %d \n", pTrapPdu->i4_GenericTrap);
    SNMPARGTrace (" i4_SpecificTrap = %u \n", pTrapPdu->i4_SpecificTrap);
    SNMPTrace ("CommunityStr ");
    SNMPPrintOctetString (pTrapPdu->pCommunityStr);
    SNMPTrace ("Enterprise ");
    SNMPPrintOIDType (pTrapPdu->pEnterprise);
    SNMPTrace ("AgentAddr ");
    SNMPPrintOctetString (pTrapPdu->pAgentAddr);
    SNMPPrintVarbindList (pTrapPdu->pVarBindList, pTrapPdu->pVarBindEnd);
    SNMPARGTrace (" i2_PduType = %d \n", pTrapPdu->i2_PduType);

    return;
}

/*********************************************************************
*  Function Name : SNMPPrintV3Pdu 
*  Description   : Function to Log the V3 PDU received
*  Parameter(s)  : pPdu - PDU to be print
*  Return Values : VOID
*********************************************************************/
VOID
SNMPPrintV3Pdu (tSNMP_V3PDU * pV3Pdu)
{
    if (pV3Pdu == NULL)
    {
        return;
    }

    SNMPARGTrace (" u4MsgID = %u \n", pV3Pdu->u4MsgID);
    SNMPARGTrace (" u4MsgMaxSize = %u \n", pV3Pdu->u4MsgMaxSize);
    SNMPARGTrace (" u4MsgSecModel = %u \n", pV3Pdu->u4MsgSecModel);
    SNMPTrace ("MsgFlag ");
    SNMPPrintOctetString (&pV3Pdu->MsgFlag);

    SNMPTrace (" Security Params --> \n");
    SNMPTrace ("MsgEngineID ");
    SNMPPrintOctetString (&pV3Pdu->MsgSecParam.MsgEngineID);
    SNMPARGTrace ("u4MsgEngineBoot = %u \n",
                  pV3Pdu->MsgSecParam.u4MsgEngineBoot);
    SNMPARGTrace ("u4MsgEngineTime = %u \n",
                  pV3Pdu->MsgSecParam.u4MsgEngineTime);
    SNMPTrace ("MsgUsrName ");
    SNMPPrintOctetString (&pV3Pdu->MsgSecParam.MsgUsrName);
    SNMPTrace ("MsgAuthParam ");
    SNMPPrintOctetString (&pV3Pdu->MsgSecParam.MsgAuthParam);
    SNMPTrace ("MsgPrivParam ");
    SNMPPrintOctetString (&pV3Pdu->MsgSecParam.MsgPrivParam);
    SNMPTrace ("\n");

    SNMPTrace ("ContextID ");
    SNMPPrintOctetString (&pV3Pdu->ContextID);
    SNMPTrace ("ContextName ");
    SNMPPrintOctetString (&pV3Pdu->ContextName);
    SNMPPrintV2Pdu (&pV3Pdu->Normalpdu);
    SNMPTrace ("\n");

    return;
}

/*********************************************************************
*  Function Name : SNMPPrintV2Pdu 
*  Description   : Function to Log the V2 PDU received
*  Parameter(s)  : pPdu - PDU to be print
*  Return Values : VOID
*********************************************************************/
VOID
SNMPPrintV2Pdu (tSNMP_NORMAL_PDU * pPtr)
{
    if (pPtr == NULL)
    {
        return;
    }

    SNMPARGTrace (" u4_Version = %u \n", pPtr->u4_Version);
    SNMPARGTrace (" u4_RequestID = %u \n", pPtr->u4_RequestID);
    SNMPARGTrace (" i4_ErrorStatus = %d \n", pPtr->i4_ErrorStatus);
    SNMPARGTrace (" i4_ErrorIndex = %d \n", pPtr->i4_ErrorIndex);
    SNMPARGTrace (" i4_NonRepeaters = %d \n", pPtr->i4_NonRepeaters);
    SNMPARGTrace (" i4_MaxRepetitions = %d \n", pPtr->i4_MaxRepetitions);
    SNMPTrace ("CommunityStr ");
    SNMPPrintOctetString (pPtr->pCommunityStr);
    SNMPPrintVarbindList (pPtr->pVarBindList, pPtr->pVarBindEnd);
    SNMPARGTrace (" i2_PduType = %d \n", pPtr->i2_PduType);

    return;
}

/*********************************************************************
*  Function Name : SNMPPrintOctetString 
*  Description   : Function to Log OctetString
*  Parameter(s)  : pOctetStr
*  Return Values : VOID
*********************************************************************/
VOID
SNMPPrintOctetString (tSNMP_OCTET_STRING_TYPE * pOctetStr)
{
    INT4                i4Count = SNMP_ZERO;

    if (pOctetStr == NULL)
    {
        return;
    }

    for (i4Count = SNMP_ZERO; (i4Count < pOctetStr->i4_Length) &&
         (i4Count < SNMP_MAX_OCTETSTRING_SIZE); i4Count++)
    {
        SNMPARGTrace ("%c", pOctetStr->pu1_OctetList[i4Count]);
    }
    SNMPTrace ("\n");

    return;
}

/*********************************************************************
*  Function Name : SNMPPrintOIDType
*  Description   : Function to Log the OID
*  Parameter(s)  : pOid
*  Return Values : VOID
*********************************************************************/
VOID
SNMPPrintOIDType (tSNMP_OID_TYPE * pOid)
{
    UINT4               u4Count = SNMP_ZERO;

    if (pOid == NULL)
    {
        return;
    }

    for (u4Count = SNMP_ZERO; (u4Count < pOid->u4_Length) &&
         (u4Count < MAX_OID_LEN); u4Count++)
    {
        if ((u4Count == (pOid->u4_Length - 1)) ||
            (u4Count == (MAX_OID_LEN - 1)))
        {
            SNMPARGTrace ("%u", pOid->pu4_OidList[u4Count]);
        }
        else
        {
            SNMPARGTrace ("%u.", pOid->pu4_OidList[u4Count]);
        }
    }
    SNMPTrace ("\n");

    return;
}

/*********************************************************************
*  Function Name : SNMPPrintVarbindList
*  Description   : Function to Log the OID
*  Parameter(s)  : pOid
*  Return Values : VOID
*********************************************************************/
VOID
SNMPPrintVarbindList (tSNMP_VAR_BIND * pVarbind, tSNMP_VAR_BIND * pVarBindEnd)
{
    tSNMP_VAR_BIND     *pTempVarbind = NULL;
    INT4                i4Count = SNMP_ZERO;

    if (pVarbind == NULL)
    {
        return;
    }

    pTempVarbind = pVarbind;
    if (pTempVarbind == pVarBindEnd)
    {
        SNMPTrace (" Only One Varbind \n");
    }
    else
    {
        SNMPTrace (" Multiple Varbinds \n");
    }

    SNMPTrace (" Varbind List --> \n");
    while (pTempVarbind)
    {
        SNMPARG2Trace ("\n [%d] %x \n", ++i4Count, pTempVarbind);
        SNMPTrace ("Obj Name");
        SNMPPrintOIDType (pTempVarbind->pObjName);
        SNMPTrace ("Obj Value --> ");
        SNMPPrintMultiDataType (&pTempVarbind->ObjValue);
        pTempVarbind = pTempVarbind->pNextVarBind;
        if (pTempVarbind)
        {
            SNMPARGTrace (" pNextVarBind =  %x \n", pTempVarbind);
        }
        else
        {
            SNMPTrace (" pNextVarBind = NULL\n");
        }
    }

    return;
}

/*********************************************************************
*  Function Name : SNMPPrintMultiDataType
*  Description   : Function to Log the OID
*  Parameter(s)  : pOid
*  Return Values : VOID
*********************************************************************/
VOID
SNMPPrintMultiDataType (tSNMP_MULTI_DATA_TYPE * pObjValue)
{

    if (pObjValue == NULL)
    {
        return;
    }

    switch (pObjValue->i2_DataType)
    {
        case SNMP_DATA_TYPE_COUNTER32:
        case SNMP_DATA_TYPE_GAUGE32:
        case SNMP_DATA_TYPE_TIME_TICKS:
        {
            SNMPARGTrace (" %u \n", pObjValue->u4_ULongValue);
        }
            break;
        case SNMP_DATA_TYPE_INTEGER32:
        {
            SNMPARGTrace (" %d \n", pObjValue->i4_SLongValue);
        }
            break;
        case SNMP_DATA_TYPE_OBJECT_ID:
        {
            SNMPPrintOIDType (pObjValue->pOidValue);
        }
            break;
        case SNMP_DATA_TYPE_OCTET_PRIM:
        case SNMP_DATA_TYPE_OPAQUE:
        case SNMP_DATA_TYPE_IP_ADDR_PRIM:
        {
            SNMPPrintOctetString (pObjValue->pOctetStrValue);
        }
            break;
        case SNMP_DATA_TYPE_COUNTER64:
        {
            SNMPARG2Trace (" msn = %u, lsn = %u",
                           pObjValue->u8_Counter64Value.msn,
                           pObjValue->u8_Counter64Value.lsn);
        }
            break;
        default:
            SNMPTrace (" \n Invalid Data Type \n");
    }

    SNMPTrace ("\n");

    return;
}
