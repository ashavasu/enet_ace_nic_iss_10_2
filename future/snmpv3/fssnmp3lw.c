/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssnmp3lw.c,v 1.11 2017/11/20 13:11:25 siva Exp $
*
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include "snxinc.h"
#include "fssnmp.h"
#include "snmpcmn.h"
#include "fssnmp3wr.h"
#include "fssnmp3lw.h"
#include "snmp3cli.h"
#include "sntarglw.h"

extern tSnmpTcp     gSnmpTcpGblInfo;
extern tRBTree      gSnmpTrapFilterTable;
extern tMemPoolId   gSnmpTrapFilterPoolId;

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetSnmpInRollbackErrs
 Input       :  The Indices

                The Object 
                retValSnmpInRollbackErrs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpInRollbackErrs (UINT4 *pu4RetValSnmpInRollbackErrs)
{
    *pu4RetValSnmpInRollbackErrs = gSnmpStat.u4SnmpRollbackErrs;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpAgentxSubAgentControl
 Input       :  The Indices

                The Object
                retValSnmpAgentxSubAgentControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpAgentxSubAgentControl (INT4 *pi4RetValSnmpAgentxSubAgentControl)
{
    *pi4RetValSnmpAgentxSubAgentControl = gi4SnmpAgentxSubAgtStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpAgentxSubAgentControl
 Input       :  The Indices

                The Object
                testValSnmpAgentxSubAgentControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpAgentxSubAgentControl (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValSnmpAgentxSubAgentControl)
{
    if ((i4TestValSnmpAgentxSubAgentControl != SNMP_AGENTX_ENABLE) &&
        (i4TestValSnmpAgentxSubAgentControl != SNMP_AGENTX_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValSnmpAgentxSubAgentControl == SNMP_AGENTX_ENABLE)
        && (gu4SnmpAgentControl == SNMP_AGENT_ENABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_AGENT_ENABLE_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpAgentxSubAgentControl
 Input       :  The Indices

                The Object
                setValSnmpAgentxSubAgentControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpAgentxSubAgentControl (INT4 i4SetValSnmpAgentxSubAgentControl)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (i4SetValSnmpAgentxSubAgentControl == gi4SnmpAgentxSubAgtStatus)
    {
        return i1RetVal;
    }
    if (i4SetValSnmpAgentxSubAgentControl == SNMP_AGENTX_ENABLE)
    {
        i1RetVal = (INT1) SnxMainAgentxEnable ();
    }
    else
    {
        i1RetVal = (INT1) SnxMainAgentxDisable ();
    }
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetSnmpInInformResponses
 Input       :  The Indices

                The Object 
                retValSnmpInInformResponses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpInInformResponses (UINT4 *pu4RetValSnmpInInformResponses)
{
    *pu4RetValSnmpInInformResponses = gSnmpStat.u4SnmpInInformResponses;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpOutInformRequests
 Input       :  The Indices

                The Object 
                retValSnmpOutInformRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpOutInformRequests (UINT4 *pu4RetValSnmpOutInformRequests)
{
    *pu4RetValSnmpOutInformRequests = gSnmpStat.u4SnmpOutInformRequests;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpInformDrops
 Input       :  The Indices

                The Object 
                retValSnmpInformDrops
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpInformDrops (UINT4 *pu4RetValSnmpInformDrops)
{
    *pu4RetValSnmpInformDrops = gSnmpStat.u4SnmpInformDrops;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpInformAwaitingAck
 Input       :  The Indices

                The Object 
                retValSnmpInformAwaitingAck
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpInformAwaitingAck (UINT4 *pu4RetValSnmpInformAwaitingAck)
{
    *pu4RetValSnmpInformAwaitingAck = gSnmpStat.u4SnmpInformAwaitAck;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpListenTrapPort
 Input       :  The Indices

                The Object 
                retValSnmpListenTrapPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpListenTrapPort (UINT4 *pu4RetValSnmpListenTrapPort)
{
    *pu4RetValSnmpListenTrapPort = (UINT4) gSnmpSystem.u2ListenTrapPort;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpOverTcpStatus
 Input       :  The Indices

                The Object
                retValSnmpOverTcpStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpOverTcpStatus (INT4 *pi4RetValSnmpOverTcpStatus)
{
    *pi4RetValSnmpOverTcpStatus = gSnmpTcpGblInfo.i4TcpStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpListenTcpPort
 Input       :  The Indices

                The Object
                retValSnmpListenTcpPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpListenTcpPort (UINT4 *pu4RetValSnmpListenTcpPort)
{
    *pu4RetValSnmpListenTcpPort = gSnmpTcpGblInfo.u2TcpPort;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpTrapOverTcpStatus
 Input       :  The Indices

                The Object
                retValSnmpTrapOverTcpStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpTrapOverTcpStatus (INT4 *pi4RetValSnmpTrapOverTcpStatus)
{
    *pi4RetValSnmpTrapOverTcpStatus = gSnmpTcpGblInfo.i4TcpTrapStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpListenTcpTrapPort
 Input       :  The Indices

                The Object
                retValSnmpListenTcpTrapPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpListenTcpTrapPort (UINT4 *pu4RetValSnmpListenTcpTrapPort)
{
    *pu4RetValSnmpListenTcpTrapPort = (UINT4) gSnmpTcpGblInfo.u2TcpTrapPort;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpProxyListenTrapPort
 Input       :  The Indices

                The Object 
                retValSnmpProxyListenTrapPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpProxyListenTrapPort (UINT4 *pu4RetValSnmpProxyListenTrapPort)
{
    *pu4RetValSnmpProxyListenTrapPort =
        (UINT4) gSnmpSystem.u2ProxyListenTrapPort;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetSnmpListenTrapPort
 Input       :  The Indices

                The Object 
                setValSnmpListenTrapPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpListenTrapPort (UINT4 u4SetValSnmpListenTrapPort)
{
    gSnmpSystem.u2ListenTrapPort = (UINT2) u4SetValSnmpListenTrapPort;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpOverTcpStatus
 Input       :  The Indices

                The Object
                setValSnmpOverTcpStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpOverTcpStatus (INT4 i4SetValSnmpOverTcpStatus)
{
    if (Snmp3SetTcpStatus (i4SetValSnmpOverTcpStatus) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    gSnmpTcpGblInfo.i4TcpStatus = i4SetValSnmpOverTcpStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpListenTcpPort
 Input       :  The Indices

                The Object
                setValSnmpListenTcpPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpListenTcpPort (UINT4 u4SetValSnmpListenTcpPort)
{
    gSnmpTcpGblInfo.u2TcpPort = (UINT2) u4SetValSnmpListenTcpPort;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpTrapOverTcpStatus
 Input       :  The Indices

                The Object
                setValSnmpTrapOverTcpStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpTrapOverTcpStatus (INT4 i4SetValSnmpTrapOverTcpStatus)
{
    gSnmpTcpGblInfo.i4TcpTrapStatus = i4SetValSnmpTrapOverTcpStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpListenTcpTrapPort
 Input       :  The Indices

                The Object
                setValSnmpListenTcpTrapPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpListenTcpTrapPort (UINT4 u4SetValSnmpListenTcpTrapPort)
{
    gSnmpTcpGblInfo.u2TcpTrapPort = (UINT2) u4SetValSnmpListenTcpTrapPort;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpProxyListenTrapPort
 Input       :  The Indices

                The Object 
                setValSnmpProxyListenTrapPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpProxyListenTrapPort (UINT4 u4SetValSnmpProxyListenTrapPort)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    gSnmpSystem.u2ProxyListenTrapPort = (UINT2) u4SetValSnmpProxyListenTrapPort;
    i1RetVal =
        (INT1)
        SNMPChangeProxyListenPort ((INT2) (gSnmpSystem.u2ProxyListenTrapPort));
    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2SnmpListenTrapPort
 Input       :  The Indices

                The Object 
                testValSnmpListenTrapPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpListenTrapPort (UINT4 *pu4ErrorCode,
                             UINT4 u4TestValSnmpListenTrapPort)
{
    /* Inputted value should be in the range of 1-65535 */
    if ((u4TestValSnmpListenTrapPort >= SNMP_MIN_PORT_NUM) &&
        (u4TestValSnmpListenTrapPort <= SNMP_MAX_PORT_NUM))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpOverTcpStatus
 Input       :  The Indices

                The Object
                testValSnmpOverTcpStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpOverTcpStatus (UINT4 *pu4ErrorCode,
                            INT4 i4TestValSnmpOverTcpStatus)
{
    /* Inputted value should be in the range of 1-65535 */
    if ((i4TestValSnmpOverTcpStatus == SNMP3_OVER_TCP_ENABLE) ||
        (i4TestValSnmpOverTcpStatus == SNMP3_OVER_TCP_DISABLE))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpListenTcpPort
 Input       :  The Indices

                The Object
                testValSnmpListenTcpPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpListenTcpPort (UINT4 *pu4ErrorCode,
                            UINT4 u4TestValSnmpListenTcpPort)
{
    /* Inputted value should be in the range of 1-65535 */
    if ((u4TestValSnmpListenTcpPort >= SNMP_MIN_PORT_NUM) &&
        (u4TestValSnmpListenTcpPort <= SNMP_MAX_PORT_NUM))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpTrapOverTcpStatus
 Input       :  The Indices

                The Object
                testValSnmpTrapOverTcpStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpTrapOverTcpStatus (UINT4 *pu4ErrorCode,
                                INT4 i4TestValSnmpTrapOverTcpStatus)
{
    /* Inputted value should be in the range of 1-65535 */
    if ((i4TestValSnmpTrapOverTcpStatus == SNMP3_OVER_TCP_ENABLE) ||
        (i4TestValSnmpTrapOverTcpStatus == SNMP3_OVER_TCP_DISABLE))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpListenTcpTrapPort
 Input       :  The Indices

                The Object
                testValSnmpListenTcpTrapPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpListenTcpTrapPort (UINT4 *pu4ErrorCode,
                                UINT4 u4TestValSnmpListenTcpTrapPort)
{
    /* Inputted value should be in the range of 1-65535 */
    if ((u4TestValSnmpListenTcpTrapPort >= SNMP_MIN_PORT_NUM) &&
        (u4TestValSnmpListenTcpTrapPort <= SNMP_MAX_PORT_NUM))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpProxyListenTrapPort
 Input       :  The Indices

                The Object 
                testValSnmpProxyListenTrapPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpProxyListenTrapPort (UINT4 *pu4ErrorCode,
                                  UINT4 u4TestValSnmpProxyListenTrapPort)
{
    /* Checking whether port is already in use */
    if ((u4TestValSnmpProxyListenTrapPort != SNMP_MANAGER_PORT) &&
        (u4TestValSnmpProxyListenTrapPort ==
         (UINT4) gSnmpSystem.u2ListenAgentPort))
    {
        CLI_SET_ERR (CLI_SNMP3_PORT_IN_USE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Inputted value should be in the range of 1-65535 */
    if ((u4TestValSnmpProxyListenTrapPort >= SNMP_MIN_PORT_NUM) &&
        (u4TestValSnmpProxyListenTrapPort <= SNMP_MAX_PORT_NUM))
    {
        if (u4TestValSnmpProxyListenTrapPort != SNMP_PORT)
        {
            return SNMP_SUCCESS;
        }
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2SnmpListenTrapPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SnmpListenTrapPort (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2SnmpProxyListenTrapPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SnmpProxyListenTrapPort (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : SnmpInformCntTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceSnmpInformCntTable
 Input       :  The Indices
                SnmpInformTgtAddrName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceSnmpInformCntTable (tSNMP_OCTET_STRING_TYPE
                                            * pSnmpInformTgtAddrName)
{
    if (pSnmpInformTgtAddrName->i4_Length == SNMP_ZERO)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexSnmpInformCntTable
 Input       :  The Indices
                SnmpInformTgtAddrName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexSnmpInformCntTable (tSNMP_OCTET_STRING_TYPE
                                    * pSnmpInformTgtAddrName)
{
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;

    pSnmpTgtAddrEntry = SNMPGetFirstTgtAddrEntry ();
    if (pSnmpTgtAddrEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pSnmpInformTgtAddrName,
                         &(pSnmpTgtAddrEntry->AddrName));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexSnmpInformCntTable
 Input       :  The Indices
                SnmpInformTgtAddrName
                nextSnmpInformTgtAddrName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexSnmpInformCntTable (tSNMP_OCTET_STRING_TYPE
                                   * pSnmpInformTgtAddrName,
                                   tSNMP_OCTET_STRING_TYPE
                                   * pNextSnmpInformTgtAddrName)
{
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;

    pSnmpTgtAddrEntry = SNMPGetNextTgtAddrEntry (pSnmpInformTgtAddrName);
    if (pSnmpTgtAddrEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOctetString (pNextSnmpInformTgtAddrName,
                         &(pSnmpTgtAddrEntry->AddrName));
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetSnmpInformSent
 Input       :  The Indices
                SnmpInformTgtAddrName

                The Object 
                retValSnmpInformSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpInformSent (tSNMP_OCTET_STRING_TYPE * pSnmpInformTgtAddrName,
                      UINT4 *pu4RetValSnmpInformSent)
{
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;

    pSnmpTgtAddrEntry = SNMPGetTgtAddrEntry (pSnmpInformTgtAddrName);
    if (pSnmpTgtAddrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValSnmpInformSent = pSnmpTgtAddrEntry->pInformStat->u4InformSent;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpInformAwaitAck
 Input       :  The Indices
                SnmpInformTgtAddrName

                The Object 
                retValSnmpInformAwaitAck
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpInformAwaitAck (tSNMP_OCTET_STRING_TYPE * pSnmpInformTgtAddrName,
                          UINT4 *pu4RetValSnmpInformAwaitAck)
{
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;

    pSnmpTgtAddrEntry = SNMPGetTgtAddrEntry (pSnmpInformTgtAddrName);
    if (pSnmpTgtAddrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValSnmpInformAwaitAck = pSnmpTgtAddrEntry->pInformStat->u4InformSent;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpInformRetried
 Input       :  The Indices
                SnmpInformTgtAddrName

                The Object 
                retValSnmpInformRetried
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpInformRetried (tSNMP_OCTET_STRING_TYPE * pSnmpInformTgtAddrName,
                         UINT4 *pu4RetValSnmpInformRetried)
{
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;

    pSnmpTgtAddrEntry = SNMPGetTgtAddrEntry (pSnmpInformTgtAddrName);
    if (pSnmpTgtAddrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValSnmpInformRetried
        = pSnmpTgtAddrEntry->pInformStat->u4InformRetried;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpInformDropped
 Input       :  The Indices
                SnmpInformTgtAddrName

                The Object 
                retValSnmpInformDropped
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpInformDropped (tSNMP_OCTET_STRING_TYPE * pSnmpInformTgtAddrName,
                         UINT4 *pu4RetValSnmpInformDropped)
{
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;

    pSnmpTgtAddrEntry = SNMPGetTgtAddrEntry (pSnmpInformTgtAddrName);
    if (pSnmpTgtAddrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValSnmpInformDropped = pSnmpTgtAddrEntry->pInformStat->u4InformDrop;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpInformFailed
 Input       :  The Indices
                SnmpInformTgtAddrName

                The Object 
                retValSnmpInformFailed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpInformFailed (tSNMP_OCTET_STRING_TYPE * pSnmpInformTgtAddrName,
                        UINT4 *pu4RetValSnmpInformFailed)
{
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;

    pSnmpTgtAddrEntry = SNMPGetTgtAddrEntry (pSnmpInformTgtAddrName);
    if (pSnmpTgtAddrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValSnmpInformFailed = pSnmpTgtAddrEntry->pInformStat->u4InformFail;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpInformResponses
 Input       :  The Indices
                SnmpInformTgtAddrName

                The Object 
                retValSnmpInformResponses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpInformResponses (tSNMP_OCTET_STRING_TYPE * pSnmpInformTgtAddrName,
                           UINT4 *pu4RetValSnmpInformResponses)
{
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;

    pSnmpTgtAddrEntry = SNMPGetTgtAddrEntry (pSnmpInformTgtAddrName);
    if (pSnmpTgtAddrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValSnmpInformResponses
        = pSnmpTgtAddrEntry->pInformStat->u4InformResp;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpAgentControl
 Input       :  The Indices

                The Object 
                retValSnmpAgentControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpAgentControl (INT4 *pi4RetValSnmpAgentControl)
{
    *pi4RetValSnmpAgentControl = (INT4) gu4SnmpAgentControl;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpColdStartTrapControl
 Input       :  The Indices

                The Object 
                retValSnmpColdStartTrapControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpColdStartTrapControl (INT4 *pi4RetValSnmpColdStartTrapControl)
{
    *pi4RetValSnmpColdStartTrapControl = gSnmpStat.i4SnmpColdStartTraps;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpAllowedPduVersions
 Input       :  The Indices

                The Object 
                retValSnmpAllowedPduVersions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpAllowedPduVersions (INT4 *pi4RetValSnmpAllowedPduVersions)
{
    *pi4RetValSnmpAllowedPduVersions = (INT4) gu4SnmpAllowedVersion;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpMinimumSecurityRequired
 Input       :  The Indices

                The Object 
                retValSnmpMinimumSecurityRequired
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpMinimumSecurityRequired (INT4 *pi4RetValSnmpMinimumSecurityRequired)
{
    *pi4RetValSnmpMinimumSecurityRequired = (INT4) gu4SnmpMinimumSecurity;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetSnmpAgentControl
 Input       :  The Indices

                The Object 
                setValSnmpAgentControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpAgentControl (INT4 i4SetValSnmpAgentControl)
{
    INT1                i1RetVal = SNMP_SUCCESS;
    if (i4SetValSnmpAgentControl == (INT4) gu4SnmpAgentControl)
    {
        return i1RetVal;
    }

    if (i4SetValSnmpAgentControl == SNMP_AGENT_ENABLE)
    {
        i1RetVal = (INT1) SNMPAgentInit ();
        if (i1RetVal == SNMP_FAILURE)
        {
            i1RetVal = (INT1) SNMPAgentDeInit ();
            UNUSED_PARAM (i1RetVal);
            return SNMP_FAILURE;
        }

    }
    else
    {
        i1RetVal = (INT1) SNMPAgentDeInit ();
    }

    gu4SnmpAgentControl = (UINT4) i4SetValSnmpAgentControl;
    if (i4SetValSnmpAgentControl == SNMP_AGENT_ENABLE)
    {
        /* Intialize to default value */
        SNMPChangeListenPort (gSnmpSystem.u2ListenAgentPort);
        SNMPChangeProxyListenPort (gSnmpSystem.u2ProxyListenTrapPort);
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetSnmpColdStartTrapControl
 Input       :  The Indices

                The Object 
                setValSnmpColdStartTrapControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpColdStartTrapControl (INT4 i4SetValSnmpColdStartTrapControl)
{
    gSnmpStat.i4SnmpColdStartTraps = i4SetValSnmpColdStartTrapControl;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpAllowedPduVersions
 Input       :  The Indices

                The Object 
                setValSnmpAllowedPduVersions
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpAllowedPduVersions (INT4 i4SetValSnmpAllowedPduVersions)
{
    gu4SnmpAllowedVersion = (UINT4) i4SetValSnmpAllowedPduVersions;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpMinimumSecurityRequired
 Input       :  The Indices

                The Object 
                setValSnmpMinimumSecurityRequired
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpMinimumSecurityRequired (INT4 i4SetValSnmpMinimumSecurityRequired)
{
    gu4SnmpMinimumSecurity = (UINT4) i4SetValSnmpMinimumSecurityRequired;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpAgentControl
 Input       :  The Indices

                The Object 
                testValSnmpAgentControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpAgentControl (UINT4 *pu4ErrorCode, INT4 i4TestValSnmpAgentControl)
{
    if ((i4TestValSnmpAgentControl != SNMP_AGENT_ENABLE) &&
        (i4TestValSnmpAgentControl != SNMP_AGENT_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValSnmpAgentControl == SNMP_AGENT_ENABLE)
        && (gi4SnmpAgentxSubAgtStatus == SNMP_AGENTX_ENABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_SNMP3_AGENTX_ENABLE_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpColdStartTrapControl
 Input       :  The Indices

                The Object 
                testValSnmpColdStartTrapControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpColdStartTrapControl (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValSnmpColdStartTrapControl)
{
    if ((i4TestValSnmpColdStartTrapControl == COLDTRAP_ENABLE) ||
        (i4TestValSnmpColdStartTrapControl == COLDTRAP_DISABLE))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpAllowedPduVersions
 Input       :  The Indices

                The Object 
                testValSnmpAllowedPduVersions
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpAllowedPduVersions (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValSnmpAllowedPduVersions)
{
    if ((i4TestValSnmpAllowedPduVersions == SNMP_V3_PDU) ||
        (i4TestValSnmpAllowedPduVersions == SNMP_V1V2_PDU) ||
        (i4TestValSnmpAllowedPduVersions == SNMP_V1V2V3_PDU))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpMinimumSecurityRequired
 Input       :  The Indices

                The Object 
                testValSnmpMinimumSecurityRequired
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpMinimumSecurityRequired (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValSnmpMinimumSecurityRequired)
{
    if ((i4TestValSnmpMinimumSecurityRequired == SNMP_MIN_SEC_NONE) ||
        (i4TestValSnmpMinimumSecurityRequired == SNMP_MIN_SEC_AUTH) ||
        (i4TestValSnmpMinimumSecurityRequired == SNMP_MIN_SEC_PRIV))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2SnmpColdStartTrapControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SnmpColdStartTrapControl (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2SnmpOverTcpStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SnmpOverTcpStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2SnmpListenTcpPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SnmpListenTcpPort (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2SnmpTrapOverTcpStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SnmpTrapOverTcpStatus (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2SnmpListenTcpTrapPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SnmpListenTcpTrapPort (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2SnmpAgentControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SnmpAgentControl (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2SnmpAllowedPduVersions
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SnmpAllowedPduVersions (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2SnmpMinimumSecurityRequired
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SnmpMinimumSecurityRequired (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSnmpListenAgentPort
 Input       :  The Indices

                The Object 
                retValFsSnmpListenAgentPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSnmpListenAgentPort (UINT4 *pu4RetValFsSnmpListenAgentPort)
{
    *pu4RetValFsSnmpListenAgentPort = (UINT4) gSnmpSystem.u2ListenAgentPort;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsSnmpEngineID
 Input       :  The Indices

                The Object 
                retValFsSnmpEngineID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSnmpEngineID (tSNMP_OCTET_STRING_TYPE * pRetValFsSnmpEngineID)
{
    SNMPCopyOctetString (pRetValFsSnmpEngineID, &gSnmpEngineID);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSnmpListenAgentPort
 Input       :  The Indices

                The Object 
                setValFsSnmpListenAgentPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSnmpListenAgentPort (UINT4 u4SetValFsSnmpListenAgentPort)
{
    if (u4SetValFsSnmpListenAgentPort == (UINT4) gSnmpSystem.u2ListenAgentPort)
    {
        return SNMP_SUCCESS;
    }

    if ((SNMPChangeListenPort ((INT4) u4SetValFsSnmpListenAgentPort) ==
         SNMP_SUCCESS))
    {
        gSnmpSystem.u2ListenAgentPort = (UINT2) u4SetValFsSnmpListenAgentPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsSnmpEngineID
 Input       :  The Indices

                The Object 
                setValFsSnmpEngineID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSnmpEngineID (tSNMP_OCTET_STRING_TYPE * pSetValFsSnmpEngineID)
{
#ifdef ISS_WANTED
    UINT4               u4BootCount = SNMP_ZERO;
#endif
    INT1                ai1EngineID[SNMP_MAX_STR_ENGINEID_LEN + 1];
    INT1                ai1PrevEngineID[SNMP_MAX_STR_ENGINEID_LEN + 1];
    INT1                ai1SetValEngineID[SNMP_MAX_STR_ENGINEID_LEN + 1];
    MEMSET (ai1EngineID, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);
    MEMSET (ai1PrevEngineID, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);
    MEMSET (ai1SetValEngineID, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);

/*check whether the value to be set is the same as the value present in gSnmpEngineID */

    SnmpConvertOctetToString (&gSnmpEngineID, ai1PrevEngineID);
    SnmpConvertOctetToString (pSetValFsSnmpEngineID, ai1SetValEngineID);
    if (STRCMP (ai1PrevEngineID, ai1SetValEngineID) == 0)
    {
        return SNMP_SUCCESS;
    }
    SNMPCopyOctetString (&gSnmpEngineID, pSetValFsSnmpEngineID);

#ifdef ISS_WANTED
    SnmpConvertOctetToString (&gSnmpEngineID, ai1EngineID);
    IssSetSnmpEngineIDToNvRam (ai1EngineID);
    IssSetSnmpEngineBootsToNvRam (u4BootCount);
    gSnmpSystem.u4SnmpBootCount = u4BootCount;
#endif

    /* updating the engine id of the users with default engineid */

    if (SnmpUpdateUserEntries (ai1PrevEngineID) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /*updating the context engine id for the communities holding
     * default context engine id  */

    if (SnmpUpdateCommunity (ai1PrevEngineID) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSnmpListenAgentPort
 Input       :  The Indices

                The Object 
                testValFsSnmpListenAgentPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSnmpListenAgentPort (UINT4 *pu4ErrorCode,
                                UINT4 u4TestValFsSnmpListenAgentPort)
{

    /* Checking whether port is already in use */
    if ((u4TestValFsSnmpListenAgentPort != SNMP_MANAGER_PORT) &&
        (u4TestValFsSnmpListenAgentPort ==
         (UINT4) gSnmpSystem.u2ProxyListenTrapPort))
    {
        CLI_SET_ERR (CLI_SNMP3_PORT_IN_USE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Inputted value should be in the range of 1-65535 */
    if ((u4TestValFsSnmpListenAgentPort >= SNMP_MIN_PORT_NUM) &&
        (u4TestValFsSnmpListenAgentPort <= SNMP_MAX_PORT_NUM))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_SNMP3_PORT_NOT_IN_RANGE_ERR);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsSnmpEngineID
 Input       :  The Indices

                The Object 
                testValFsSnmpEngineID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSnmpEngineID (UINT4 *pu4ErrorCode,
                         tSNMP_OCTET_STRING_TYPE * pTestValFsSnmpEngineID)
{
    INT1                ai1EngineID[SNMP_MAX_STR_ENGINEID_LEN + 1];
    UINT4               u4NumOctets = 0;

    MEMSET (ai1EngineID, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);
    if (SnmpConvertOctetToString (pTestValFsSnmpEngineID, ai1EngineID) ==
        SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    u4NumOctets = SnmpGetNumOctets (ai1EngineID);
    if ((u4NumOctets < SNMP_MIN_ENGINE_ID_LEN) ||
        (u4NumOctets > SNMP_MAX_ENGINE_ID_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSnmpListenAgentPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSnmpListenAgentPort (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSnmpEngineID
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSnmpEngineID (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetSnmpAgentxTransportDomain
 Input       :  The Indices

                The Object 
                retValSnmpAgentxTransportDomain
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpAgentxTransportDomain (INT4 *pi4RetValSnmpAgentxTransportDomain)
{
    *pi4RetValSnmpAgentxTransportDomain = gAgtxGlobalInfo.u1TDomain;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpAgentxMasterAgentAddr
 Input       :  The Indices

                The Object 
                retValSnmpAgentxMasterAgentAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpAgentxMasterAgentAddr (tSNMP_OCTET_STRING_TYPE *
                                 pRetValSnmpAgentxMasterAgentAddr)
{
    SNMPCopyOctetString (pRetValSnmpAgentxMasterAgentAddr,
                         &gAgtxGlobalInfo.MasterIpAddr);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpAgentxMasterAgentPortNo
 Input       :  The Indices

                The Object 
                retValSnmpAgentxMasterAgentPortNo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpAgentxMasterAgentPortNo (UINT4 *pu4RetValSnmpAgentxMasterAgentPortNo)
{
    *pu4RetValSnmpAgentxMasterAgentPortNo = gAgtxGlobalInfo.u2PortNo;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetSnmpAgentxSubAgentInPkts
 Input       :  The Indices

                The Object 
                retValSnmpAgentxSubAgentInPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpAgentxSubAgentInPkts (UINT4 *pu4RetValSnmpAgentxSubAgentInPkts)
{
    *pu4RetValSnmpAgentxSubAgentInPkts = gAgtxStats.u4InPkts;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpAgentxSubAgentOutPkts
 Input       :  The Indices

                The Object 
                retValSnmpAgentxSubAgentOutPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpAgentxSubAgentOutPkts (UINT4 *pu4RetValSnmpAgentxSubAgentOutPkts)
{
    *pu4RetValSnmpAgentxSubAgentOutPkts = gAgtxStats.u4OutPks;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpAgentxSubAgentPktDrops
 Input       :  The Indices

                The Object 
                retValSnmpAgentxSubAgentPktDrops
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpAgentxSubAgentPktDrops (UINT4 *pu4RetValSnmpAgentxSubAgentPktDrops)
{
    *pu4RetValSnmpAgentxSubAgentPktDrops = gAgtxStats.u4PktDrops;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpAgentxSubAgentParseDrops
 Input       :  The Indices

                The Object 
                retValSnmpAgentxSubAgentParseDrops
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpAgentxSubAgentParseDrops (UINT4
                                    *pu4RetValSnmpAgentxSubAgentParseDrops)
{
    *pu4RetValSnmpAgentxSubAgentParseDrops = gAgtxStats.u4ParseDrops;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpAgentxSubAgentInOpenFail
 Input       :  The Indices

                The Object 
                retValSnmpAgentxSubAgentInOpenFail
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpAgentxSubAgentInOpenFail (UINT4
                                    *pu4RetValSnmpAgentxSubAgentInOpenFail)
{
    *pu4RetValSnmpAgentxSubAgentInOpenFail = gAgtxStats.u4OpenFails;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpAgentxSubAgentOpenPktCnt
 Input       :  The Indices

                The Object 
                retValSnmpAgentxSubAgentOpenPktCnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpAgentxSubAgentOpenPktCnt (UINT4
                                    *pu4RetValSnmpAgentxSubAgentOpenPktCnt)
{
    *pu4RetValSnmpAgentxSubAgentOpenPktCnt = gAgtxStats.u4OpenPduSent;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpAgentxSubAgentInClosePktCnt
 Input       :  The Indices

                The Object 
                retValSnmpAgentxSubAgentInClosePktCnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpAgentxSubAgentInClosePktCnt (UINT4
                                       *pu4RetValSnmpAgentxSubAgentInClosePktCnt)
{
    *pu4RetValSnmpAgentxSubAgentInClosePktCnt = gAgtxStats.u4ClosePduRcvd;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpAgentxSubAgentOutClosePktCnt
 Input       :  The Indices

                The Object 
                retValSnmpAgentxSubAgentOutClosePktCnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpAgentxSubAgentOutClosePktCnt (UINT4
                                        *pu4RetValSnmpAgentxSubAgentOutClosePktCnt)
{
    *pu4RetValSnmpAgentxSubAgentOutClosePktCnt = gAgtxStats.u4ClosePduSent;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpAgentxSubAgentIdAllocPktCnt
 Input       :  The Indices

                The Object 
                retValSnmpAgentxSubAgentIdAllocPktCnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpAgentxSubAgentIdAllocPktCnt (UINT4
                                       *pu4RetValSnmpAgentxSubAgentIdAllocPktCnt)
{
    *pu4RetValSnmpAgentxSubAgentIdAllocPktCnt = gAgtxStats.u4IndxAllocPduSent;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpAgentxSubAgentIdDllocPktCnt
 Input       :  The Indices

                The Object 
                retValSnmpAgentxSubAgentIdDllocPktCnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpAgentxSubAgentIdDllocPktCnt (UINT4
                                       *pu4RetValSnmpAgentxSubAgentIdDllocPktCnt)
{
    *pu4RetValSnmpAgentxSubAgentIdDllocPktCnt = gAgtxStats.u4IndxDAllocPduSent;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpAgentxSubAgentRegPktCnt
 Input       :  The Indices

                The Object 
                retValSnmpAgentxSubAgentRegPktCnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpAgentxSubAgentRegPktCnt (UINT4 *pu4RetValSnmpAgentxSubAgentRegPktCnt)
{
    *pu4RetValSnmpAgentxSubAgentRegPktCnt = gAgtxStats.u4RegPduSent;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpAgentxSubAgentUnRegPktCnt
 Input       :  The Indices

                The Object 
                retValSnmpAgentxSubAgentUnRegPktCnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpAgentxSubAgentUnRegPktCnt (UINT4
                                     *pu4RetValSnmpAgentxSubAgentUnRegPktCnt)
{
    *pu4RetValSnmpAgentxSubAgentUnRegPktCnt = gAgtxStats.u4UnRegPduSent;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpAgentxSubAgentAddCapsCnt
 Input       :  The Indices

                The Object 
                retValSnmpAgentxSubAgentAddCapsCnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpAgentxSubAgentAddCapsCnt (UINT4
                                    *pu4RetValSnmpAgentxSubAgentAddCapsCnt)
{
    *pu4RetValSnmpAgentxSubAgentAddCapsCnt = gAgtxStats.u4AddCapsPduSent;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpAgentxSubAgentRemCapsCnt
 Input       :  The Indices

                The Object 
                retValSnmpAgentxSubAgentRemCapsCnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpAgentxSubAgentRemCapsCnt (UINT4
                                    *pu4RetValSnmpAgentxSubAgentRemCapsCnt)
{
    *pu4RetValSnmpAgentxSubAgentRemCapsCnt = gAgtxStats.u4RemoveCapsPduSent;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpAgentxSubAgentNotifyPktCnt
 Input       :  The Indices

                The Object 
                retValSnmpAgentxSubAgentNotifyPktCnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpAgentxSubAgentNotifyPktCnt (UINT4
                                      *pu4RetValSnmpAgentxSubAgentNotifyPktCnt)
{
    *pu4RetValSnmpAgentxSubAgentNotifyPktCnt = gAgtxStats.u4NotifyPduSent;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpAgentxSubAgentPingCnt
 Input       :  The Indices

                The Object 
                retValSnmpAgentxSubAgentPingCnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpAgentxSubAgentPingCnt (UINT4 *pu4RetValSnmpAgentxSubAgentPingCnt)
{
    *pu4RetValSnmpAgentxSubAgentPingCnt = gAgtxStats.u4PingPduSent;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpAgentxSubAgentInGets
 Input       :  The Indices

                The Object 
                retValSnmpAgentxSubAgentInGets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpAgentxSubAgentInGets (UINT4 *pu4RetValSnmpAgentxSubAgentInGets)
{
    *pu4RetValSnmpAgentxSubAgentInGets = gAgtxStats.u4GetPduRcvd;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpAgentxSubAgentInGetNexts
 Input       :  The Indices

                The Object 
                retValSnmpAgentxSubAgentInGetNexts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpAgentxSubAgentInGetNexts (UINT4
                                    *pu4RetValSnmpAgentxSubAgentInGetNexts)
{
    *pu4RetValSnmpAgentxSubAgentInGetNexts = gAgtxStats.u4GetNextPduRcvd;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpAgentxSubAgentInGetBulks
 Input       :  The Indices

                The Object 
                retValSnmpAgentxSubAgentInGetBulks
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpAgentxSubAgentInGetBulks (UINT4
                                    *pu4RetValSnmpAgentxSubAgentInGetBulks)
{
    *pu4RetValSnmpAgentxSubAgentInGetBulks = gAgtxStats.u4GetBulkPduRcvd;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpAgentxSubAgentInTestSets
 Input       :  The Indices

                The Object 
                retValSnmpAgentxSubAgentInTestSets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpAgentxSubAgentInTestSets (UINT4
                                    *pu4RetValSnmpAgentxSubAgentInTestSets)
{
    *pu4RetValSnmpAgentxSubAgentInTestSets = gAgtxStats.u4TestSetPduRcvd;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpAgentxSubAgentInCommits
 Input       :  The Indices

                The Object 
                retValSnmpAgentxSubAgentInCommits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpAgentxSubAgentInCommits (UINT4 *pu4RetValSnmpAgentxSubAgentInCommits)
{
    *pu4RetValSnmpAgentxSubAgentInCommits = gAgtxStats.u4CommitSetPduRcvd;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpAgentxSubAgentInCleanups
 Input       :  The Indices

                The Object 
                retValSnmpAgentxSubAgentInCleanups
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpAgentxSubAgentInCleanups (UINT4
                                    *pu4RetValSnmpAgentxSubAgentInCleanups)
{
    *pu4RetValSnmpAgentxSubAgentInCleanups = gAgtxStats.u4CleanupSetPduRcvd;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpAgentxSubAgentInUndos
 Input       :  The Indices

                The Object 
                retValSnmpAgentxSubAgentInUndos
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpAgentxSubAgentInUndos (UINT4 *pu4RetValSnmpAgentxSubAgentInUndos)
{
    *pu4RetValSnmpAgentxSubAgentInUndos = gAgtxStats.u4UndoSetPduRcvd;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpAgentxSubAgentOutResponse
 Input       :  The Indices

                The Object 
                retValSnmpAgentxSubAgentOutResponse
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpAgentxSubAgentOutResponse (UINT4
                                     *pu4RetValSnmpAgentxSubAgentOutResponse)
{
    *pu4RetValSnmpAgentxSubAgentOutResponse = gAgtxStats.u4RespPduSent;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpAgentxSubAgentInResponse
 Input       :  The Indices

                The Object 
                retValSnmpAgentxSubAgentInResponse
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpAgentxSubAgentInResponse (UINT4
                                    *pu4RetValSnmpAgentxSubAgentInResponse)
{
    *pu4RetValSnmpAgentxSubAgentInResponse = gAgtxStats.u4RespPduRcvd;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetSnmpAgentxTransportDomain
 Input       :  The Indices

                The Object 
                setValSnmpAgentxTransportDomain
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpAgentxTransportDomain (INT4 i4SetValSnmpAgentxTransportDomain)
{
    gAgtxGlobalInfo.u1TDomain = (UINT1) i4SetValSnmpAgentxTransportDomain;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpAgentxMasterAgentAddr
 Input       :  The Indices

                The Object 
                setValSnmpAgentxMasterAgentAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpAgentxMasterAgentAddr (tSNMP_OCTET_STRING_TYPE *
                                 pSetValSnmpAgentxMasterAgentAddr)
{
    SNMPCopyOctetString (&gAgtxGlobalInfo.MasterIpAddr,
                         pSetValSnmpAgentxMasterAgentAddr);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpAgentxMasterAgentPortNo
 Input       :  The Indices

                The Object 
                setValSnmpAgentxMasterAgentPortNo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpAgentxMasterAgentPortNo (UINT4 u4SetValSnmpAgentxMasterAgentPortNo)
{
    gAgtxGlobalInfo.u2PortNo = (UINT2) u4SetValSnmpAgentxMasterAgentPortNo;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2SnmpAgentxTransportDomain
 Input       :  The Indices

                The Object 
                testValSnmpAgentxTransportDomain
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpAgentxTransportDomain (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValSnmpAgentxTransportDomain)
{
    if (i4TestValSnmpAgentxTransportDomain != SNX_TDOMAIN_TCP)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (gu4SnmpAgentControl == SNMP_AGENT_ENABLE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_AGENT_ENABLE_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpAgentxMasterAgentAddr
 Input       :  The Indices

                The Object 
                testValSnmpAgentxMasterAgentAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpAgentxMasterAgentAddr (UINT4 *pu4ErrorCode,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValSnmpAgentxMasterAgentAddr)
{
    UINT4               u4Ip4Addr;

    if ((pTestValSnmpAgentxMasterAgentAddr->i4_Length != SNX_IP4_LEN) &&
        (pTestValSnmpAgentxMasterAgentAddr->i4_Length != SNX_IP6_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (gu4SnmpAgentControl == SNMP_AGENT_ENABLE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_AGENT_ENABLE_ERR);
        return SNMP_FAILURE;
    }

    if (gi4SnmpAgentxSubAgtStatus == SNMP_AGENTX_ENABLE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_AGENTX_ENABLE_ERR);
        return SNMP_FAILURE;
    }
    if (pTestValSnmpAgentxMasterAgentAddr->i4_Length == SNX_IP4_LEN)
    {
        MEMCPY (&u4Ip4Addr, pTestValSnmpAgentxMasterAgentAddr->pu1_OctetList,
                SNX_IP4_LEN);
#ifndef SNMP_LNXIP_WANTED
        if (NetIpv4IfIsOurAddress (u4Ip4Addr) == NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_INVALID_IP_ADDR);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
#endif
        if (NetIpv4IpIsLocalNet (u4Ip4Addr) != NETIPV4_SUCCESS)
        {
#ifdef SNMP_LNXIP_WANTED
            /* Dont return error if the IP is 127.0.0.1
             * since it is allowed as a valid SNMP master
             * IP address */
            if (u4Ip4Addr != ISS_INADDR_LOOPBACK)
#endif
            {

                CLI_SET_ERR (CLI_INVALID_IP_ADDR);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
        }
        if (IP_ADDRESS_ABOVE_CLASS_C (u4Ip4Addr))
        {
            CLI_SET_ERR (CLI_INVALID_IP_ADDR);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpAgentxMasterAgentPortNo
 Input       :  The Indices

                The Object 
                testValSnmpAgentxMasterAgentPortNo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpAgentxMasterAgentPortNo (UINT4 *pu4ErrorCode,
                                      UINT4
                                      u4TestValSnmpAgentxMasterAgentPortNo)
{
    if (gu4SnmpAgentControl == SNMP_AGENT_ENABLE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_AGENT_ENABLE_ERR);
        return SNMP_FAILURE;
    }
    if (gi4SnmpAgentxSubAgtStatus == SNMP_AGENTX_ENABLE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_AGENTX_ENABLE_ERR);
        return SNMP_FAILURE;
    }

    if (u4TestValSnmpAgentxMasterAgentPortNo <= 0xFFFF)
    {
        return SNMP_SUCCESS;
    }
    CLI_SET_ERR (CLI_INVALID_PORT_NO);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2SnmpAgentxTransportDomain
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SnmpAgentxTransportDomain (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2SnmpAgentxMasterAgentAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SnmpAgentxMasterAgentAddr (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2SnmpAgentxMasterAgentPortNo
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SnmpAgentxMasterAgentPortNo (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2SnmpAgentxSubAgentControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SnmpAgentxSubAgentControl (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsSnmpProxyTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsSnmpProxyTable
 Input       :  The Indices
                FsSnmpProxyMibName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsSnmpProxyTable (tSNMP_OCTET_STRING_TYPE *
                                          pFsSnmpProxyMibName)
{
    tPrpProxyTableEntry *pProxyEntry = NULL;

    pProxyEntry = SNMPGetPrpProxyEntryFromName (pFsSnmpProxyMibName);
    if (pProxyEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsSnmpProxyTable
 Input       :  The Indices
                FsSnmpProxyMibName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsSnmpProxyTable (tSNMP_OCTET_STRING_TYPE * pFsSnmpProxyMibName)
{
    tPrpProxyTableEntry *pFirst = NULL;

    pFirst = SNMPGetFirstPrpProxy ();
    if (pFirst == NULL)
    {
        return SNMP_FAILURE;
    }

    SNMPCopyOctetString (pFsSnmpProxyMibName, &(pFirst->PrpPrxMibName));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsSnmpProxyTable
 Input       :  The Indices
                FsSnmpProxyMibName
                nextFsSnmpProxyMibName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsSnmpProxyTable (tSNMP_OCTET_STRING_TYPE * pFsSnmpProxyMibName,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pNextFsSnmpProxyMibName)
{
    tPrpProxyTableEntry *pNext = NULL;

    if (pFsSnmpProxyMibName->i4_Length >= SNMP_MAX_OCTETSTRING_SIZE)
    {
        return SNMP_FAILURE;
    }

    pNext = SNMPGetNextPrpProxyEntry (pFsSnmpProxyMibName);

    if (pNext == NULL)
    {
        return SNMP_FAILURE;
    }

    SNMPCopyOctetString (pNextFsSnmpProxyMibName, &(pNext->PrpPrxMibName));

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSnmpProxyMibType
 Input       :  The Indices
                FsSnmpProxyMibName

                The Object 
                retValFsSnmpProxyMibType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSnmpProxyMibType (tSNMP_OCTET_STRING_TYPE * pFsSnmpProxyMibName,
                          INT4 *pi4RetValFsSnmpProxyMibType)
{
    tPrpProxyTableEntry *pProxyEntry = NULL;

    pProxyEntry = SNMPGetPrpProxyEntryFromName (pFsSnmpProxyMibName);
    if (pProxyEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsSnmpProxyMibType = pProxyEntry->i4PrpPrxMibType;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSnmpProxyMibId
 Input       :  The Indices
                FsSnmpProxyMibName

                The Object 
                retValFsSnmpProxyMibId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSnmpProxyMibId (tSNMP_OCTET_STRING_TYPE * pFsSnmpProxyMibName,
                        tSNMP_OID_TYPE * pRetValFsSnmpProxyMibId)
{
    tPrpProxyTableEntry *pProxyEntry = NULL;

    pProxyEntry = SNMPGetPrpProxyEntryFromName (pFsSnmpProxyMibName);
    if (pProxyEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    SNMPCopyOid (pRetValFsSnmpProxyMibId, &(pProxyEntry->PrpPrxMibID));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSnmpProxyMibTargetParamsIn
 Input       :  The Indices
                FsSnmpProxyMibName

                The Object 
                retValFsSnmpProxyMibTargetParamsIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSnmpProxyMibTargetParamsIn (tSNMP_OCTET_STRING_TYPE *
                                    pFsSnmpProxyMibName,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValFsSnmpProxyMibTargetParamsIn)
{
    tPrpProxyTableEntry *pProxyEntry = NULL;

    pProxyEntry = SNMPGetPrpProxyEntryFromName (pFsSnmpProxyMibName);
    if (pProxyEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    SNMPCopyOctetString (pRetValFsSnmpProxyMibTargetParamsIn,
                         &(pProxyEntry->PrpPrxTargetParamsIn));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSnmpProxyMibSingleTargetOut
 Input       :  The Indices
                FsSnmpProxyMibName

                The Object 
                retValFsSnmpProxyMibSingleTargetOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSnmpProxyMibSingleTargetOut (tSNMP_OCTET_STRING_TYPE *
                                     pFsSnmpProxyMibName,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsSnmpProxyMibSingleTargetOut)
{
    tPrpProxyTableEntry *pProxyEntry = NULL;

    pProxyEntry = SNMPGetPrpProxyEntryFromName (pFsSnmpProxyMibName);
    if (pProxyEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    SNMPCopyOctetString (pRetValFsSnmpProxyMibSingleTargetOut,
                         &(pProxyEntry->PrpPrxSingleTargetOut));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSnmpProxyMibMultipleTargetOut
 Input       :  The Indices
                FsSnmpProxyMibName

                The Object 
                retValFsSnmpProxyMibMultipleTargetOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSnmpProxyMibMultipleTargetOut (tSNMP_OCTET_STRING_TYPE *
                                       pFsSnmpProxyMibName,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pRetValFsSnmpProxyMibMultipleTargetOut)
{
    tPrpProxyTableEntry *pProxyEntry = NULL;

    pProxyEntry = SNMPGetPrpProxyEntryFromName (pFsSnmpProxyMibName);
    if (pProxyEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    SNMPCopyOctetString (pRetValFsSnmpProxyMibMultipleTargetOut,
                         &(pProxyEntry->PrpPrxMultipleTargetOut));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSnmpProxyMibStorageType
 Input       :  The Indices
                FsSnmpProxyMibName

                The Object 
                retValFsSnmpProxyMibStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSnmpProxyMibStorageType (tSNMP_OCTET_STRING_TYPE * pFsSnmpProxyMibName,
                                 INT4 *pi4RetValFsSnmpProxyMibStorageType)
{
    tPrpProxyTableEntry *pProxyEntry = NULL;

    pProxyEntry = SNMPGetPrpProxyEntryFromName (pFsSnmpProxyMibName);
    if (pProxyEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsSnmpProxyMibStorageType = pProxyEntry->i4Storage;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSnmpProxyMibRowStatus
 Input       :  The Indices
                FsSnmpProxyMibName

                The Object 
                retValFsSnmpProxyMibRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSnmpProxyMibRowStatus (tSNMP_OCTET_STRING_TYPE * pFsSnmpProxyMibName,
                               INT4 *pi4RetValFsSnmpProxyMibRowStatus)
{
    tPrpProxyTableEntry *pProxyEntry = NULL;

    pProxyEntry = SNMPGetPrpProxyEntryFromName (pFsSnmpProxyMibName);
    if (pProxyEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pProxyEntry->i4Status == UNDER_CREATION)
    {
        *pi4RetValFsSnmpProxyMibRowStatus = pProxyEntry->i4Status;
    }

    if (pProxyEntry->i4Status == SNMP_ACTIVE)
    {
        *pi4RetValFsSnmpProxyMibRowStatus = SNMP_ROWSTATUS_ACTIVE;
    }

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSnmpProxyMibType
 Input       :  The Indices
                FsSnmpProxyMibName

                The Object 
                setValFsSnmpProxyMibType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSnmpProxyMibType (tSNMP_OCTET_STRING_TYPE * pFsSnmpProxyMibName,
                          INT4 i4SetValFsSnmpProxyMibType)
{
    tPrpProxyTableEntry *pProxyEntry = NULL;

    pProxyEntry = SNMPGetPrpProxyEntryFromName (pFsSnmpProxyMibName);
    if (pProxyEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pProxyEntry->i4PrpPrxMibType = i4SetValFsSnmpProxyMibType;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsSnmpProxyMibId
 Input       :  The Indices
                FsSnmpProxyMibName

                The Object 
                setValFsSnmpProxyMibId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSnmpProxyMibId (tSNMP_OCTET_STRING_TYPE * pFsSnmpProxyMibName,
                        tSNMP_OID_TYPE * pSetValFsSnmpProxyMibId)
{
    tPrpProxyTableEntry *pProxyEntry = NULL;

    pProxyEntry = SNMPGetPrpProxyEntryFromName (pFsSnmpProxyMibName);
    if (pProxyEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    SNMPOIDCopy (&(pProxyEntry->PrpPrxMibID), pSetValFsSnmpProxyMibId);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsSnmpProxyMibTargetParamsIn
 Input       :  The Indices
                FsSnmpProxyMibName

                The Object 
                setValFsSnmpProxyMibTargetParamsIn
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSnmpProxyMibTargetParamsIn (tSNMP_OCTET_STRING_TYPE *
                                    pFsSnmpProxyMibName,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pSetValFsSnmpProxyMibTargetParamsIn)
{
    tPrpProxyTableEntry *pProxyEntry = NULL;

    pProxyEntry = SNMPGetPrpProxyEntryFromName (pFsSnmpProxyMibName);
    if (pProxyEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    SNMPCopyOctetString (&(pProxyEntry->PrpPrxTargetParamsIn),
                         pSetValFsSnmpProxyMibTargetParamsIn);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsSnmpProxyMibSingleTargetOut
 Input       :  The Indices
                FsSnmpProxyMibName

                The Object 
                setValFsSnmpProxyMibSingleTargetOut
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSnmpProxyMibSingleTargetOut (tSNMP_OCTET_STRING_TYPE *
                                     pFsSnmpProxyMibName,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pSetValFsSnmpProxyMibSingleTargetOut)
{
    tPrpProxyTableEntry *pProxyEntry = NULL;

    pProxyEntry = SNMPGetPrpProxyEntryFromName (pFsSnmpProxyMibName);
    if (pProxyEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    SNMPCopyOctetString (&(pProxyEntry->PrpPrxSingleTargetOut),
                         pSetValFsSnmpProxyMibSingleTargetOut);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsSnmpProxyMibMultipleTargetOut
 Input       :  The Indices
                FsSnmpProxyMibName

                The Object 
                setValFsSnmpProxyMibMultipleTargetOut
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSnmpProxyMibMultipleTargetOut (tSNMP_OCTET_STRING_TYPE *
                                       pFsSnmpProxyMibName,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pSetValFsSnmpProxyMibMultipleTargetOut)
{
    tPrpProxyTableEntry *pProxyEntry = NULL;

    pProxyEntry = SNMPGetPrpProxyEntryFromName (pFsSnmpProxyMibName);
    if (pProxyEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    SNMPCopyOctetString (&(pProxyEntry->PrpPrxMultipleTargetOut),
                         pSetValFsSnmpProxyMibMultipleTargetOut);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsSnmpProxyMibStorageType
 Input       :  The Indices
                FsSnmpProxyMibName

                The Object 
                setValFsSnmpProxyMibStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSnmpProxyMibStorageType (tSNMP_OCTET_STRING_TYPE * pFsSnmpProxyMibName,
                                 INT4 i4SetValFsSnmpProxyMibStorageType)
{
    tPrpProxyTableEntry *pProxyEntry = NULL;

    pProxyEntry = SNMPGetPrpProxyEntryFromName (pFsSnmpProxyMibName);
    if (pProxyEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pProxyEntry->i4Storage = i4SetValFsSnmpProxyMibStorageType;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsSnmpProxyMibRowStatus
 Input       :  The Indices
                FsSnmpProxyMibName

                The Object 
                setValFsSnmpProxyMibRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSnmpProxyMibRowStatus (tSNMP_OCTET_STRING_TYPE * pFsSnmpProxyMibName,
                               INT4 i4SetValFsSnmpProxyMibRowStatus)
{
    INT4                i4Return = SNMP_FAILURE;

    switch (i4SetValFsSnmpProxyMibRowStatus)
    {
        case SNMP_ROWSTATUS_CREATEANDWAIT:
        {
            i4Return = SNMPAddPrpProxyEntry (pFsSnmpProxyMibName);
            if (i4Return != SNMP_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        }
        case SNMP_ROWSTATUS_ACTIVE:
        {
            tPrpProxyTableEntry *pProxyEntry = NULL;

            pProxyEntry = SNMPGetPrpProxyEntryFromName (pFsSnmpProxyMibName);
            if (pProxyEntry == NULL)
            {
                return SNMP_FAILURE;
            }

            pProxyEntry->i4Status = SNMP_ROWSTATUS_ACTIVE;

            /* Add the Mib Entry in the Mib Id List */
            SNMPAddPrpPrxMibIdEntrySll (pProxyEntry);
            return SNMP_SUCCESS;

        }
        case SNMP_ROWSTATUS_DESTROY:
        {
            i4Return = SNMPDeletePrpProxyEntry (pFsSnmpProxyMibName);
            if (i4Return != SNMP_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        }

        case SNMP_ROWSTATUS_NOTINSERVICE:
        {
            tPrpProxyTableEntry *pProxyEntry = NULL;
            pProxyEntry = SNMPGetPrpProxyEntryFromName (pFsSnmpProxyMibName);
            if (pProxyEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            if (pProxyEntry->i4Status == SNMP_ROWSTATUS_ACTIVE)
            {
                /* Delete the Mib Entry from the MibId List */
                SNMPDelPrpPrxMibIdEntry (pProxyEntry);

                pProxyEntry->i4Status = SNMP_ROWSTATUS_NOTINSERVICE;
                return SNMP_SUCCESS;
            }
        }
        default:
            break;
    }

    return SNMP_FAILURE;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSnmpProxyMibType
 Input       :  The Indices
                FsSnmpProxyMibName

                The Object 
                testValFsSnmpProxyMibType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSnmpProxyMibType (UINT4 *pu4ErrorCode,
                             tSNMP_OCTET_STRING_TYPE * pFsSnmpProxyMibName,
                             INT4 i4TestValFsSnmpProxyMibType)
{
    tPrpProxyTableEntry *pProxyEntry = NULL;
    pProxyEntry = SNMPGetPrpProxyEntryFromName (pFsSnmpProxyMibName);

    if (pProxyEntry != NULL)
    {
        if ((i4TestValFsSnmpProxyMibType < PROXY_TYPE_READ)
            || (i4TestValFsSnmpProxyMibType > PROXY_TYPE_INFORM))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        if (pProxyEntry->i4Status == SNMP_ROWSTATUS_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2FsSnmpProxyMibId
 Input       :  The Indices
                FsSnmpProxyMibName

                The Object 
                testValFsSnmpProxyMibId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSnmpProxyMibId (UINT4 *pu4ErrorCode,
                           tSNMP_OCTET_STRING_TYPE * pFsSnmpProxyMibName,
                           tSNMP_OID_TYPE * pTestValFsSnmpProxyMibId)
{
    tPrpProxyTableEntry *pProxyEntry = NULL;
    pProxyEntry = SNMPGetPrpProxyEntryFromName (pFsSnmpProxyMibName);

    if (pProxyEntry != NULL)
    {
        if ((pTestValFsSnmpProxyMibId->u4_Length < SNMP_ONE)
            || (pTestValFsSnmpProxyMibId->u4_Length > MAX_OID_LENGTH))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
            return SNMP_FAILURE;
        }

        if (pProxyEntry->i4Status == SNMP_ROWSTATUS_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsSnmpProxyMibTargetParamsIn
 Input       :  The Indices
                FsSnmpProxyMibName

                The Object 
                testValFsSnmpProxyMibTargetParamsIn
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSnmpProxyMibTargetParamsIn (UINT4 *pu4ErrorCode,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsSnmpProxyMibName,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pTestValFsSnmpProxyMibTargetParamsIn)
{
    tPrpProxyTableEntry *pProxyEntry = NULL;
    pProxyEntry = SNMPGetPrpProxyEntryFromName (pFsSnmpProxyMibName);

    if (pProxyEntry != NULL)
    {
        if ((pTestValFsSnmpProxyMibTargetParamsIn->i4_Length <= SNMP_ZERO) ||
            (pTestValFsSnmpProxyMibTargetParamsIn->i4_Length >
             SNMP_MAX_TGT_PARAM_LEN))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
            return SNMP_FAILURE;
        }

        if (pProxyEntry->i4Status == SNMP_ROWSTATUS_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2FsSnmpProxyMibSingleTargetOut
 Input       :  The Indices
                FsSnmpProxyMibName

                The Object 
                testValFsSnmpProxyMibSingleTargetOut
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSnmpProxyMibSingleTargetOut (UINT4 *pu4ErrorCode,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsSnmpProxyMibName,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pTestValFsSnmpProxyMibSingleTargetOut)
{
    tPrpProxyTableEntry *pProxyEntry = NULL;
    pProxyEntry = SNMPGetPrpProxyEntryFromName (pFsSnmpProxyMibName);

    if (pProxyEntry != NULL)
    {
        if ((pTestValFsSnmpProxyMibSingleTargetOut->i4_Length < SNMP_ZERO) ||
            (pTestValFsSnmpProxyMibSingleTargetOut->i4_Length >
             SNMP_MAX_OCTETSTRING_SIZE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
            return SNMP_FAILURE;
        }

        if (pProxyEntry->i4Status == SNMP_ROWSTATUS_ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2FsSnmpProxyMibMultipleTargetOut
 Input       :  The Indices
                FsSnmpProxyMibName

                The Object 
                testValFsSnmpProxyMibMultipleTargetOut
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSnmpProxyMibMultipleTargetOut (UINT4 *pu4ErrorCode,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsSnmpProxyMibName,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pTestValFsSnmpProxyMibMultipleTargetOut)
{
    tPrpProxyTableEntry *pProxyEntry = NULL;

    pProxyEntry = SNMPGetPrpProxyEntryFromName (pFsSnmpProxyMibName);

    if (pProxyEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pProxyEntry->i4Status == SNMP_ROWSTATUS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pTestValFsSnmpProxyMibMultipleTargetOut->i4_Length < SNMP_ZERO) ||
        (pTestValFsSnmpProxyMibMultipleTargetOut->i4_Length >
         SNMP_MAX_OCTETSTRING_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsSnmpProxyMibStorageType
 Input       :  The Indices
                FsSnmpProxyMibName

                The Object 
                testValFsSnmpProxyMibStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSnmpProxyMibStorageType (UINT4 *pu4ErrorCode,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsSnmpProxyMibName,
                                    INT4 i4TestValFsSnmpProxyMibStorageType)
{

    tPrpProxyTableEntry *pProxyEntry = NULL;

    pProxyEntry = SNMPGetPrpProxyEntryFromName (pFsSnmpProxyMibName);
    if (pProxyEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsSnmpProxyMibStorageType != SNMP3_STORAGE_TYPE_VOLATILE)
        && (i4TestValFsSnmpProxyMibStorageType !=
            SNMP3_STORAGE_TYPE_NONVOLATILE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsSnmpProxyMibRowStatus
 Input       :  The Indices
                FsSnmpProxyMibName

                The Object 
                testValFsSnmpProxyMibRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSnmpProxyMibRowStatus (UINT4 *pu4ErrorCode,
                                  tSNMP_OCTET_STRING_TYPE * pFsSnmpProxyMibName,
                                  INT4 i4TestValFsSnmpProxyMibRowStatus)
{
    tPrpProxyTableEntry *pProxyEntry = NULL;
    pProxyEntry = SNMPGetPrpProxyEntryFromName (pFsSnmpProxyMibName);

    switch (i4TestValFsSnmpProxyMibRowStatus)
    {
        case SNMP_ROWSTATUS_CREATEANDWAIT:
            if (pProxyEntry != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            break;
        case SNMP_ROWSTATUS_ACTIVE:
            if (pProxyEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }

            if ((pProxyEntry->PrpPrxMibID.u4_Length == SNMP_ZERO) ||
                (pProxyEntry->PrpPrxTargetParamsIn.i4_Length == SNMP_ZERO) ||
                (pProxyEntry->i4PrpPrxMibType == SNMP_ZERO))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if (((pProxyEntry->i4PrpPrxMibType == PROXY_TYPE_READ) ||
                 (pProxyEntry->i4PrpPrxMibType == PROXY_TYPE_WRITE)) &&
                (!pProxyEntry->PrpPrxSingleTargetOut.i4_Length))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if (((pProxyEntry->i4PrpPrxMibType == PROXY_TYPE_TRAP) ||
                 (pProxyEntry->i4PrpPrxMibType == PROXY_TYPE_INFORM)) &&
                (!pProxyEntry->PrpPrxMultipleTargetOut.i4_Length))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            break;
        case SNMP_ROWSTATUS_NOTINSERVICE:
            if (pProxyEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            if (pProxyEntry->i4Status != SNMP_ROWSTATUS_ACTIVE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
        case SNMP_ROWSTATUS_DESTROY:
            if (pProxyEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            break;
        case SNMP_ROWSTATUS_CREATEANDGO:
            /* CreateAndGo can never come directly because CreateAndGo
               will be broken into CreateAndWait and then ACTIVE,
               so if CreateAndGo comes to us then we will return failure */
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsSnmpProxyTable
 Input       :  The Indices
                FsSnmpProxyMibName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSnmpProxyTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSnmpAgentxContextName
 Input       :  The Indices

                The Object 
                retValSnmpAgentxContextName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSnmpAgentxContextName (tSNMP_OCTET_STRING_TYPE *
                             pRetValSnmpAgentxContextName)
{
    SNMPCopyOctetString (pRetValSnmpAgentxContextName,
                         &gAgtxGlobalInfo.ContextName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSnmpAgentxContextName
 Input       :  The Indices

                The Object 
                setValSnmpAgentxContextName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSnmpAgentxContextName (tSNMP_OCTET_STRING_TYPE *
                             pSetValSnmpAgentxContextName)
{
    SNMPCopyOctetString (&gAgtxGlobalInfo.ContextName,
                         pSetValSnmpAgentxContextName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SnmpAgentxContextName
 Input       :  The Indices

                The Object 
                testValSnmpAgentxContextName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SnmpAgentxContextName (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValSnmpAgentxContextName)
{
    if (gu4SnmpAgentControl == SNMP_AGENT_ENABLE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_AGENT_ENABLE_ERR);
        return SNMP_FAILURE;
    }
    if (gi4SnmpAgentxSubAgtStatus == SNMP_AGENTX_ENABLE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_AGENTX_ENABLE_ERR);
        return SNMP_FAILURE;
    }
    if (pTestValSnmpAgentxContextName->i4_Length >= MAX_SNMP_AGTX_CTXT_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_AGENTX_CTXT_NAME_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2SnmpAgentxContextName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SnmpAgentxContextName (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsSnmpTrapFilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsSnmpTrapFilterTable
 Input       :  The Indices
                FsSnmpTrapFilterOID
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsSnmpTrapFilterTable (tSNMP_OID_TYPE *
                                               pFsSnmpTrapFilterOID)
{
    tSnmpTrapFilterEntry *pSnmpTrapFilterEntry = NULL;
    tSnmpTrapFilterEntry SnmpTrapFilterEntry;
    UINT4               au4Oid[SNMP_MAX_OID_LENGTH];

    MEMSET (au4Oid, SNMP_ZERO, SNMP_MAX_OID_LENGTH);
    MEMSET (&SnmpTrapFilterEntry, SNMP_ZERO, sizeof (tSnmpTrapFilterEntry));

    SnmpTrapFilterEntry.TrapFilterOID.pu4_OidList = au4Oid;
    SNMPCopyOid (&(SnmpTrapFilterEntry.TrapFilterOID), pFsSnmpTrapFilterOID);
    pSnmpTrapFilterEntry = (tSnmpTrapFilterEntry *)
        RBTreeGet (gSnmpTrapFilterTable, (tRBElem *) & SnmpTrapFilterEntry);

    if (pSnmpTrapFilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsSnmpTrapFilterTable
 Input       :  The Indices
                FsSnmpTrapFilterOID
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsSnmpTrapFilterTable (tSNMP_OID_TYPE * pFsSnmpTrapFilterOID)
{
    tSnmpTrapFilterEntry *pSnmpTrapFilterEntry = NULL;

    pSnmpTrapFilterEntry = (tSnmpTrapFilterEntry *)
        RBTreeGetFirst (gSnmpTrapFilterTable);
    if (pSnmpTrapFilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    SNMPCopyOid (pFsSnmpTrapFilterOID, &(pSnmpTrapFilterEntry->TrapFilterOID));
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsSnmpTrapFilterTable
 Input       :  The Indices
                FsSnmpTrapFilterOID
                nextFsSnmpTrapFilterOID
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsSnmpTrapFilterTable (tSNMP_OID_TYPE * pFsSnmpTrapFilterOID,
                                      tSNMP_OID_TYPE * pNextFsSnmpTrapFilterOID)
{
    tSnmpTrapFilterEntry *pSnmpTrapFilterEntry = NULL;
    tSnmpTrapFilterEntry SnmpTrapFilterEntry;

    MEMSET (&SnmpTrapFilterEntry, SNMP_ZERO, sizeof (tSnmpTrapFilterEntry));

    SnmpTrapFilterEntry.TrapFilterOID.pu4_OidList =
        (UINT4 *) MemAllocMemBlk (gSnmpOidListPoolId);

    if (SnmpTrapFilterEntry.TrapFilterOID.pu4_OidList == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (SnmpTrapFilterEntry.TrapFilterOID.pu4_OidList, SNMP_ZERO,
            sizeof (tSnmpOidListBlock));
    SNMPCopyOid (&(SnmpTrapFilterEntry.TrapFilterOID), pFsSnmpTrapFilterOID);
    pSnmpTrapFilterEntry = (tSnmpTrapFilterEntry *)
        RBTreeGetNext (gSnmpTrapFilterTable,
                       &SnmpTrapFilterEntry, SnmpTrapFilterTblCmpFn);

    if (pSnmpTrapFilterEntry == NULL)
    {
        MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *)
                            SnmpTrapFilterEntry.TrapFilterOID.pu4_OidList);
        return SNMP_FAILURE;
    }

    SNMPCopyOid (pNextFsSnmpTrapFilterOID,
                 &(pSnmpTrapFilterEntry->TrapFilterOID));

    MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *)
                        SnmpTrapFilterEntry.TrapFilterOID.pu4_OidList);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSnmpTrapFilterRowStatus
 Input       :  The Indices
                FsSnmpTrapFilterOID

                The Object 
                retValFsSnmpTrapFilterRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSnmpTrapFilterRowStatus (tSNMP_OID_TYPE * pFsSnmpTrapFilterOID,
                                 INT4 *pi4RetValFsSnmpTrapFilterRowStatus)
{
    tSnmpTrapFilterEntry *pSnmpTrapFilterEntry = NULL;
    tSnmpTrapFilterEntry SnmpTrapFilterEntry;

    MEMSET (&SnmpTrapFilterEntry, SNMP_ZERO, sizeof (tSnmpTrapFilterEntry));

    SnmpTrapFilterEntry.TrapFilterOID.pu4_OidList =
        (UINT4 *) MemAllocMemBlk (gSnmpOidListPoolId);

    if (SnmpTrapFilterEntry.TrapFilterOID.pu4_OidList == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (SnmpTrapFilterEntry.TrapFilterOID.pu4_OidList, SNMP_ZERO,
            sizeof (tSnmpOidListBlock));

    SNMPCopyOid (&(SnmpTrapFilterEntry.TrapFilterOID), pFsSnmpTrapFilterOID);
    pSnmpTrapFilterEntry = (tSnmpTrapFilterEntry *)
        RBTreeGet (gSnmpTrapFilterTable, (tRBElem *) & SnmpTrapFilterEntry);

    if (pSnmpTrapFilterEntry == NULL)
    {
        MemReleaseMemBlock (gSnmpOidListPoolId,
                            (UINT1 *) SnmpTrapFilterEntry.
                            TrapFilterOID.pu4_OidList);
        return SNMP_FAILURE;
    }
    *pi4RetValFsSnmpTrapFilterRowStatus = pSnmpTrapFilterEntry->i4Status;
    MemReleaseMemBlock (gSnmpOidListPoolId,
                        (UINT1 *) SnmpTrapFilterEntry.
                        TrapFilterOID.pu4_OidList);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSnmpTrapFilterRowStatus
 Input       :  The Indices
                FsSnmpTrapFilterOID

                The Object 
                setValFsSnmpTrapFilterRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSnmpTrapFilterRowStatus (tSNMP_OID_TYPE * pFsSnmpTrapFilterOID,
                                 INT4 i4SetValFsSnmpTrapFilterRowStatus)
{
    tSnmpTrapFilterEntry *pSnmpTrapFilterEntry = NULL;
    tSnmpTrapFilterEntry SnmpTrapFilterEntry;

    MEMSET (&SnmpTrapFilterEntry, SNMP_ZERO, sizeof (tSnmpTrapFilterEntry));

    switch (i4SetValFsSnmpTrapFilterRowStatus)
    {
        case SNMP_ROWSTATUS_CREATEANDWAIT:    /* Intentional fall through for MSR */
        case SNMP_ROWSTATUS_CREATEANDGO:

            /* Memblock creation and initialization is done */
            pSnmpTrapFilterEntry = (tSnmpTrapFilterEntry *)
                MemAllocMemBlk (gSnmpTrapFilterPoolId);
            if (pSnmpTrapFilterEntry == NULL)
            {
                return SNMP_FAILURE;
            }

            pSnmpTrapFilterEntry->TrapFilterOID.pu4_OidList =
                MemAllocMemBlk (gSnmpOidListPoolId);
            if (pSnmpTrapFilterEntry->TrapFilterOID.pu4_OidList == NULL)
            {
                MemReleaseMemBlock (gSnmpTrapFilterPoolId,
                                    (UINT1 *) pSnmpTrapFilterEntry);
                return SNMP_FAILURE;
            }

            MEMSET (pSnmpTrapFilterEntry->TrapFilterOID.pu4_OidList,
                    SNMP_ZERO, SNMP_MAX_OID_LENGTH);
            SNMPCopyOid (&(pSnmpTrapFilterEntry->TrapFilterOID),
                         pFsSnmpTrapFilterOID);
            pSnmpTrapFilterEntry->i4Status = SNMP_ROWSTATUS_ACTIVE;

            /* Add this new entry to gSnmpTrapFilterTable */

            if (RBTreeAdd (gSnmpTrapFilterTable,
                           (tRBElem *) pSnmpTrapFilterEntry) != RB_SUCCESS)
            {
                MemReleaseMemBlock (gSnmpOidListPoolId,
                                    (UINT1
                                     *) (pSnmpTrapFilterEntry->TrapFilterOID.
                                         pu4_OidList));
                pSnmpTrapFilterEntry->TrapFilterOID.pu4_OidList = NULL;
                MemReleaseMemBlock (gSnmpTrapFilterPoolId,
                                    (UINT1 *) pSnmpTrapFilterEntry);
                return SNMP_FAILURE;
            }
            break;

        case SNMP_ROWSTATUS_DESTROY:

            MEMSET (&SnmpTrapFilterEntry, SNMP_ZERO,
                    sizeof (tSnmpTrapFilterEntry));
            SnmpTrapFilterEntry.TrapFilterOID.pu4_OidList =
                (UINT4 *) MemAllocMemBlk (gSnmpOidListPoolId);

            if (SnmpTrapFilterEntry.TrapFilterOID.pu4_OidList == NULL)
            {
                return SNMP_FAILURE;
            }
            MEMSET (SnmpTrapFilterEntry.TrapFilterOID.pu4_OidList, SNMP_ZERO,
                    sizeof (tSnmpOidListBlock));
            SNMPCopyOid (&(SnmpTrapFilterEntry.TrapFilterOID),
                         pFsSnmpTrapFilterOID);

            pSnmpTrapFilterEntry = (tSnmpTrapFilterEntry *)
                RBTreeGet (gSnmpTrapFilterTable,
                           (tRBElem *) & SnmpTrapFilterEntry);
            if (pSnmpTrapFilterEntry == NULL)
            {
                MemReleaseMemBlock (gSnmpOidListPoolId,
                                    (UINT1 *) SnmpTrapFilterEntry.
                                    TrapFilterOID.pu4_OidList);
                return SNMP_SUCCESS;
            }

            /* Remove Entry from the RBTree */
            RBTreeRemove (gSnmpTrapFilterTable, (UINT1 *) pSnmpTrapFilterEntry);

            /* Release the Removed Entry's memory */
            MemReleaseMemBlock (gSnmpOidListPoolId,
                                (UINT1 *) (pSnmpTrapFilterEntry->
                                           TrapFilterOID.pu4_OidList));
            pSnmpTrapFilterEntry->TrapFilterOID.pu4_OidList = NULL;
            MemReleaseMemBlock (gSnmpTrapFilterPoolId,
                                (UINT1 *) (pSnmpTrapFilterEntry));
            MemReleaseMemBlock (gSnmpOidListPoolId,
                                (UINT1 *) SnmpTrapFilterEntry.
                                TrapFilterOID.pu4_OidList);
            break;

        case SNMP_ROWSTATUS_ACTIVE:
            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSnmpTrapFilterRowStatus
 Input       :  The Indices
                FsSnmpTrapFilterOID

                The Object 
                testValFsSnmpTrapFilterRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSnmpTrapFilterRowStatus (UINT4 *pu4ErrorCode,
                                    tSNMP_OID_TYPE * pFsSnmpTrapFilterOID,
                                    INT4 i4TestValFsSnmpTrapFilterRowStatus)
{
    tSnmpTrapFilterEntry *pSnmpTrapFilterEntry = NULL;
    tSnmpTrapFilterEntry SnmpTrapFilterEntry;

    MEMSET (&SnmpTrapFilterEntry, SNMP_ZERO, sizeof (tSnmpTrapFilterEntry));

    SnmpTrapFilterEntry.TrapFilterOID.pu4_OidList =
        (UINT4 *) MemAllocMemBlk (gSnmpOidListPoolId);

    if (SnmpTrapFilterEntry.TrapFilterOID.pu4_OidList == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (SnmpTrapFilterEntry.TrapFilterOID.pu4_OidList, SNMP_ZERO,
            sizeof (tSnmpOidListBlock));
    SNMPCopyOid (&(SnmpTrapFilterEntry.TrapFilterOID), pFsSnmpTrapFilterOID);

    pSnmpTrapFilterEntry = (tSnmpTrapFilterEntry *)
        RBTreeGet (gSnmpTrapFilterTable, (tRBElem *) & SnmpTrapFilterEntry);

    switch (i4TestValFsSnmpTrapFilterRowStatus)
    {
        case CREATE_AND_WAIT:    /* Intentional fall through for MSR */
        case SNMP_ROWSTATUS_CREATEANDGO:
            if (pSnmpTrapFilterEntry != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MemReleaseMemBlock (gSnmpOidListPoolId,
                                    (UINT1 *) SnmpTrapFilterEntry.
                                    TrapFilterOID.pu4_OidList);
                return (SNMP_FAILURE);
            }
            break;

        case SNMP_ROWSTATUS_DESTROY:
            if (pSnmpTrapFilterEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MemReleaseMemBlock (gSnmpOidListPoolId,
                                    (UINT1 *) SnmpTrapFilterEntry.
                                    TrapFilterOID.pu4_OidList);
                return SNMP_FAILURE;
            }
            break;

        case SNMP_ROWSTATUS_ACTIVE:
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MemReleaseMemBlock (gSnmpOidListPoolId,
                                (UINT1 *) SnmpTrapFilterEntry.
                                TrapFilterOID.pu4_OidList);
            return SNMP_FAILURE;
    }
    MemReleaseMemBlock (gSnmpOidListPoolId,
                        (UINT1 *) SnmpTrapFilterEntry.
                        TrapFilterOID.pu4_OidList);
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsSnmpTrapFilterTable
 Input       :  The Indices
                FsSnmpTrapFilterOID
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSnmpTrapFilterTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsSnmpTargetAddrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsSnmpTargetAddrTable                                           Input       :  The Indices
                SnmpTargetAddrName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsSnmpTargetAddrTable (tSNMP_OCTET_STRING_TYPE
                                               * pSnmpTargetAddrName)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhValidateIndexInstanceSnmpTargetAddrTable
        (pSnmpTargetAddrName);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsSnmpTargetAddrTable
 Input       :  The Indices
                SnmpTargetAddrName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsSnmpTargetAddrTable (tSNMP_OCTET_STRING_TYPE *
                                       pSnmpTargetAddrName)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFirstIndexSnmpTargetAddrTable (pSnmpTargetAddrName);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsSnmpTargetAddrTable
 Input       :  The Indices
                SnmpTargetAddrName
                nextSnmpTargetAddrName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsSnmpTargetAddrTable (tSNMP_OCTET_STRING_TYPE
                                      * pSnmpTargetAddrName,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pNextSnmpTargetAddrName)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetNextIndexSnmpTargetAddrTable (pSnmpTargetAddrName,
                                                   pNextSnmpTargetAddrName);
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSnmpTargetHostName
 Input       :  The Indices
                SnmpTargetAddrName

                The Object
                retValFsSnmpTargetHostName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSnmpTargetHostName (tSNMP_OCTET_STRING_TYPE * pSnmpTargetAddrName,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsSnmpTargetHostName)
{
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;

    pSnmpTgtAddrEntry = SNMPGetTgtAddrEntry (pSnmpTargetAddrName);

    if (pSnmpTgtAddrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    STRNCPY (pRetValFsSnmpTargetHostName->pu1_OctetList,
             pSnmpTgtAddrEntry->au1Hostname,
             STRLEN (&pSnmpTgtAddrEntry->au1Hostname));

    pRetValFsSnmpTargetHostName->i4_Length =
        (INT4) STRLEN (pSnmpTgtAddrEntry->au1Hostname);

    pRetValFsSnmpTargetHostName->pu1_OctetList
        [STRLEN (pSnmpTgtAddrEntry->au1Hostname)] = '\0';

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSnmpTargetHostName
 Input       :  The Indices
                SnmpTargetAddrName

                The Object
                setValFsSnmpTargetHostName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSnmpTargetHostName (tSNMP_OCTET_STRING_TYPE * pSnmpTargetAddrName,
                            tSNMP_OCTET_STRING_TYPE
                            * pSetValFsSnmpTargetHostName)
{
    tSnmpTgtAddrEntry  *pSnmpTgtAddrEntry = NULL;
    pSnmpTgtAddrEntry = SNMPGetTgtAddrEntry (pSnmpTargetAddrName);

    if (pSnmpTgtAddrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /*This check is to avoid the default value is 
     * getting configured while in the process of bulk sync-up*/
#ifdef L2RED_WANTED
    if ((RmIsBulkUpdateComplete () != RM_TRUE) &&
        (*pSetValFsSnmpTargetHostName->pu1_OctetList == 0))
    {
        return SNMP_SUCCESS;
    }
#endif
    if (pSetValFsSnmpTargetHostName->i4_Length != 0)
    {
        MEMCPY (pSnmpTgtAddrEntry->au1Hostname,
                pSetValFsSnmpTargetHostName->pu1_OctetList,
                (pSetValFsSnmpTargetHostName->i4_Length - SNMP_TWO));

        MEMCPY (&(pSnmpTgtAddrEntry->u2Port),
                (pSetValFsSnmpTargetHostName->pu1_OctetList +
                 pSetValFsSnmpTargetHostName->i4_Length - SNMP_TWO), SNMP_TWO);

        pSnmpTgtAddrEntry->Address.i4_Length =
            pSetValFsSnmpTargetHostName->i4_Length - SNMP_TWO;

        pSnmpTgtAddrEntry->au1Hostname[pSetValFsSnmpTargetHostName->i4_Length] =
            '\0';

        pSnmpTgtAddrEntry->b1HostName = SNMP_TRUE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSnmpTargetHostName
 Input       :  The Indices
                SnmpTargetAddrName

                The Object
                testValFsSnmpTargetHostName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSnmpTargetHostName (UINT4 *pu4ErrorCode, tSNMP_OCTET_STRING_TYPE
                               * pSnmpTargetAddrName, tSNMP_OCTET_STRING_TYPE
                               * pTestValFsSnmpTargetHostName)
{
    if (SNMPGetTgtAddrEntry (pSnmpTargetAddrName) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((pTestValFsSnmpTargetHostName->i4_Length != 6)
        && (pTestValFsSnmpTargetHostName->i4_Length != 16)
        && (pTestValFsSnmpTargetHostName->i4_Length > DNS_MAX_QUERY_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsSnmpTargetAddrTable
 Input       :  The Indices
                SnmpTargetAddrName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSnmpTargetAddrTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
