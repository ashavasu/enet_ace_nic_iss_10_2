/********************************************************************
 * * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * *
 * * $Id: snxextn.h,v 1.6 2016/03/18 13:04:29 siva Exp $
 * *
 * * Description: snxextn.h
 * *********************************************************************/

#ifndef _SNXEXTN_H 
#define _SNXEXTN_H

PUBLIC tSnmpAgentxStats gAgtxStats; /* Agentx Statistics */ 
PUBLIC tAgentxInfo      gAgtxGlobalInfo;
PUBLIC tMibReg          *gpMibReg;
#endif
