/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: vacmview.c,v 1.14 2016/03/18 13:04:29 siva Exp $
 *
 * Description: routines for VACM View Table Database Model 
 *******************************************************************/

#include "snmpcmn.h"
#include "vacmview.h"
#include "snmputil.h"

/*********************************************************************
*  Function Name : VACMInitViewTree
*  Description   : Initialises the View Tree Table 
*  Parameter(s)  : None 
*  Return Values : None 
*********************************************************************/

INT4
VACMInitViewTree ()
{
    UINT4               u4Index = SNMP_ZERO;
    MEMSET (gaViewTree, SNMP_ZERO, sizeof (gaViewTree));
    MEMSET (au1Mask, SNMP_ZERO, sizeof (au1Mask));
    MEMSET (au4SubOid, SNMP_ZERO, sizeof (au4SubOid));
    TMO_SLL_Init (&gSnmpViewTree);
    for (u4Index = SNMP_ZERO; u4Index < SNMP_MAX_VIEW_TREE; u4Index++)
    {
        gaViewTree[u4Index].i4Status = SNMP_INACTIVE;
        gaViewTree[u4Index].Mask.pu1_OctetList = au1Mask[u4Index];
        gaViewTree[u4Index].SubTree.pu4_OidList = au4SubOid[u4Index];
    }
    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : VACMViewTreeAdd 
*  Description   : Function to add in the View Tree Table 
*  Parameter(s)  : 
*  Return Values : 
*********************************************************************/

INT4
VACMViewTreeAdd (tSNMP_OCTET_STRING_TYPE * pName, tSNMP_OID_TYPE * pSubTree)
{
    UINT4               u4Index = SNMP_ZERO;
    tTMO_SLL_NODE      *pListNode = NULL;
    tAccessEntry       *pAccessEntry = NULL;
    if (VACMViewTreeGet (pName, pSubTree) != NULL)
    {
        return SNMP_FAILURE;
    }

    for (u4Index = SNMP_ZERO; u4Index < SNMP_MAX_VIEW_TREE; u4Index++)
    {
        if (gaViewTree[u4Index].i4Status == SNMP_INACTIVE)
        {
            TMO_SLL_Scan (&gSnmpAccessEntry, pListNode, tTMO_SLL_NODE *)
            {
                pAccessEntry = (tAccessEntry *) pListNode;
                gaViewTree[u4Index].pName = NULL;
                if (SNMPCompareOctetString (&(pAccessEntry->ReadName),
                                            pName) == SNMP_EQUAL)
                {
                    gaViewTree[u4Index].pName = &(pAccessEntry->ReadName);
                    break;
                }
                if (SNMPCompareOctetString (&(pAccessEntry->WriteName),
                                            pName) == SNMP_EQUAL)
                {
                    gaViewTree[u4Index].pName = &(pAccessEntry->WriteName);
                    break;
                }
                if (SNMPCompareOctetString (&(pAccessEntry->NotifyName),
                                            pName) == SNMP_EQUAL)
                {
                    gaViewTree[u4Index].pName = &(pAccessEntry->NotifyName);
                    break;
                }
            }
            if (gaViewTree[u4Index].pName == NULL)
            {
                return SNMP_FAILURE;
            }
            SNMPOIDCopy (&(gaViewTree[u4Index].SubTree), pSubTree);
            MEMSET (gaViewTree[u4Index].Mask.pu1_OctetList, 1,
                    SNMP_MAX_OCTETSTRING_SIZE);
            gaViewTree[u4Index].Mask.i4_Length = (INT4) pSubTree->u4_Length;
            gaViewTree[u4Index].i4Type = SNMP_VACM_VIEW_TREE_TYPE_INCLUDED;
            gaViewTree[u4Index].i4Storage = SNMP3_STORAGE_TYPE_NONVOLATILE;
            gaViewTree[u4Index].i4Status = UNDER_CREATION;
            VACMAddSnmpViewSll (&(gaViewTree[u4Index].link));
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/*********************************************************************
*  Function Name : VACMViewTreeGetFirst 
*  Description   : Function to get the first entry  form view tree table 
*  Parameter(s)  : None 
*  Return Values : None 
*********************************************************************/

tViewTree          *
VACMViewTreeGetFirst ()
{
    tViewTree          *pFirstViewTree = NULL;
    pFirstViewTree = (tViewTree *) TMO_SLL_First (&gSnmpViewTree);
    return pFirstViewTree;
}

/*********************************************************************
*  Function Name : VACMViewTreeGetNext 
*  Description   : Function to get the Next entry  form view tree table 
*  Parameter(s)  : pName    - Tree Name
                   pSubTree - Subtree Oid   
*  Return Values : SUCCESS/NULL 
*********************************************************************/

tViewTree          *
VACMViewTreeGetNext (tSNMP_OCTET_STRING_TYPE * pName, tSNMP_OID_TYPE * pSubTree)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tViewTree          *pViewEntry = NULL;
    INT4                i4Result;
    UINT4               u4Count;

    TMO_SLL_Scan (&gSnmpViewTree, pListNode, tTMO_SLL_NODE *)
    {
        pViewEntry = (tViewTree *) pListNode;

        i4Result = SNMPCompareOctetString (pName, pViewEntry->pName);

        if (i4Result == SNMP_GREATER)
        {
            continue;
        }
        else if (i4Result == SNMP_LESSER)
        {
            return pViewEntry;
        }
        if (OIDCompare (*pSubTree, pViewEntry->SubTree,
                        &u4Count) == SNMP_LESSER)
        {
            return pViewEntry;
        }
    }
    return NULL;
}

/*********************************************************************
*  Function Name : VACMAddSnmpViewSll 
*  Description   : Function to add node to the  list
*  Parameter(s)  : pNode - Node to be add 
*  Return Values : None 
*********************************************************************/

VOID
VACMAddSnmpViewSll (tTMO_SLL_NODE * pNode)
{
    tViewTree          *pCurView = NULL;
    tViewTree          *pInView = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pPrevNode = NULL;
    UINT4               u4Match = SNMP_ZERO;
    UINT4               u4RetVal = SNMP_ZERO;
    pInView = (tViewTree *) pNode;

    TMO_SLL_Scan (&gSnmpViewTree, pLstNode, tTMO_SLL_NODE *)
    {
        pCurView = (tViewTree *) pLstNode;
        u4RetVal =
            (UINT4) SNMPCompareOctetString (pInView->pName, pCurView->pName);
        if (u4RetVal == SNMP_LESSER)
        {
            break;
        }
        else if (u4RetVal == SNMP_GREATER)
        {
            pPrevNode = pLstNode;
            continue;
        }

        u4RetVal = OIDCompare (pInView->SubTree, pCurView->SubTree, &u4Match);
        if (u4RetVal == SNMP_LESSER)
        {
            break;
        }
        pPrevNode = pLstNode;
    }
    TMO_SLL_Insert (&gSnmpViewTree, pPrevNode, pNode);
}

/*********************************************************************
*  Function Name : VACMViewTreeGetLongestMatch 
*  Description   : Function to get longest match in view tree table 
*  Parameter(s)  : pName      - View Name
                   pInObject  - OID to be searched
                   pu4Error   - Returns SNMP_ERR_NO_ACCESS 
                                or returns SNMP_VIEW_ALONE_MATCHED
                                or returns SNMP_ERR_NO_ERR
*  Return Values : SNMP_SUCCESS/SNMP_FAILURE  
*********************************************************************/

tViewTree          *
VACMViewTreeGetLongestMatch (tSNMP_OCTET_STRING_TYPE * pName,
                             tSNMP_OID_TYPE * pInObject, UINT4 *pu4Error)
{

    UINT4               u4Match = SNMP_ZERO;
    tViewTree          *pViewEntry = NULL;
    tViewTree          *pLongEntry = NULL;
    tTMO_SLL_NODE      *pListNode = NULL;
    INT4                i4Found = SNMP_ZERO;
    INT4                i4Mask = 0x80;
    INT4                i4MaskPos = SNMP_ZERO;
    INT4                i4OidPos = SNMP_ZERO;
    INT4                i4TypeInclude = OSIX_FALSE;
    
    /*Initialise the error */
    *pu4Error = SNMP_ERR_NO_ERROR;

    TMO_SLL_Scan (&gSnmpViewTree, pListNode, tTMO_SLL_NODE *)
    {
        pViewEntry = (tViewTree *) pListNode;
        if (SNMPCompareOctetString (pViewEntry->pName, pName) == SNMP_EQUAL)
        {
		if (pInObject->u4_Length >= pViewEntry->SubTree.u4_Length)
		{
                        if (pViewEntry->i4Type ==  SNMP_VACM_VIEW_TREE_TYPE_INCLUDED)
                        {
                             i4TypeInclude = OSIX_TRUE;
                        }
		        *pu4Error = SNMP_VIEW_ALONE_MATCHED;
			i4Mask = 1;
			i4MaskPos = 0;
			i4OidPos = 0;
			i4Found = 1;
			for (i4OidPos = 0; ((UINT4) i4OidPos <
						pViewEntry->SubTree.u4_Length); i4OidPos++)
			{
				if ((pViewEntry->Mask.pu1_OctetList[i4MaskPos] & i4Mask) != 0)
				{
					if (pInObject->pu4_OidList[i4OidPos] !=
							pViewEntry->SubTree.pu4_OidList[i4OidPos])
					{
						i4Found = 0;
					}
				}
				i4MaskPos++;
			}
			if (i4Found)
			{
				if ((pLongEntry == NULL || pViewEntry->SubTree.u4_Length >
							pLongEntry->SubTree.u4_Length) ||
						(pViewEntry->SubTree.u4_Length ==
						 pLongEntry->SubTree.u4_Length &&
						 OIDCompare (pViewEntry->SubTree, pLongEntry->SubTree,
							 &u4Match) != SNMP_LESSER))
				{
					pLongEntry = pViewEntry;
				}
			}
		}
        }
	else
	{
               /* Avoid overwriting the error if view is already
                * matched in previous loops
                */
		if (*pu4Error != SNMP_VIEW_ALONE_MATCHED)
		{
			*pu4Error = SNMP_ERR_NO_ACCESS;
		}
	}

    }
    if (pLongEntry != NULL)
    {
	    /* Reset the error because Longest match found */
	    *pu4Error = SNMP_ERR_NO_ERROR;
    }
 

    /* if the given view name is not having any INCLUDE oid's, then we will reject this walk/get operation */ 
    if (i4TypeInclude == OSIX_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_ACCESS;
        return NULL;
    }

    return pLongEntry;
}

/*********************************************************************
*  Function Name : VACMViewTreeGet 
*  Description   : Function to get the view tree entry 
*  Parameter(s)  : pName    - View Name
                   pSubTree - OID 
*  Return Values : NULL/pViewEntry 
*********************************************************************/

tViewTree          *
VACMViewTreeGet (tSNMP_OCTET_STRING_TYPE * pName, tSNMP_OID_TYPE * pSubTree)
{
    tViewTree          *pViewEntry = NULL;
    tTMO_SLL_NODE      *pListNode = NULL;
    UINT4               u4Match = SNMP_ZERO;

    TMO_SLL_Scan (&gSnmpViewTree, pListNode, tTMO_SLL_NODE *)
    {
        pViewEntry = (tViewTree *) pListNode;
        if (SNMPCompareOctetString (pViewEntry->pName, pName) == SNMP_EQUAL)
        {
            if (OIDCompare (pViewEntry->SubTree, *pSubTree,
                            &u4Match) == SNMP_EQUAL)
            {
                return pViewEntry;
            }
        }
    }
    return NULL;
}

/*********************************************************************
*  Function Name : VACMDeleteViewTree 
*  Description   : Function to delete an entry from view tree table
*  Parameter(s)  : pName    - View Name
                   pSubTree - OID 
*  Return Values : SNMP_SUCCESS/SNMP_FAILURE 
*********************************************************************/

INT4
VACMDeleteViewTree (tSNMP_OCTET_STRING_TYPE * pName, tSNMP_OID_TYPE * pSubTree)
{
    UINT4               u4RetVal = 0;
    tTMO_SLL_NODE      *pListNode = NULL;
    tViewTree          *pViewEntry = NULL;
    UINT4               u4Match = SNMP_ZERO;
    TMO_SLL_Scan (&gSnmpViewTree, pListNode, tTMO_SLL_NODE *)
    {
        pViewEntry = (tViewTree *) pListNode;
        if (SNMPCompareOctetString (pViewEntry->pName, pName) == SNMP_EQUAL)
        {
            u4RetVal = OIDCompare (pViewEntry->SubTree, *pSubTree, &u4Match);
            if (pSubTree->u4_Length == u4Match)
            {
                pViewEntry->i4Status = SNMP_INACTIVE;
                SNMPDelVacmViewEntrySll (&(pViewEntry->link));
                return SNMP_SUCCESS;
            }
        }
    }
    UNUSED_PARAM (u4RetVal);
    return SNMP_FAILURE;
}

/*********************************************************************
*  Function Name : SNMPDelVacmViewEntrySll 
*  Description   : Delete the node for list
*  Parameter(s)  : pNode - Node to be deleted
*  Return Values : None
*********************************************************************/

VOID
SNMPDelVacmViewEntrySll (tTMO_SLL_NODE * pNode)
{
    TMO_SLL_Delete (&gSnmpViewTree, pNode);
    return;
}

/*********************************************************************
*  Function Name : SNMPVACMIsAccessAllowed 
*  Description   : Check the access for particular OID 
*  Parameter(s)  : pVacmInfo   - VACM Information 
                   i4ViewType  - View Type
                   pOID        - OID
                   pu4Err      - Error code 
*  Return Values : SNMP_SUCCESS/SNMP_FAILURE 
*********************************************************************/

INT4
SNMPVACMIsAccessAllowed (tSnmpVacmInfo * pVacmInfo,
                         INT4 i4ViewType, tSNMP_OID_TYPE * pOID, UINT4 *pu4Err)
{
    tContext           *pContext = NULL;
    tSecGrp            *pSecGrp = NULL;
    tAccessEntry       *pAccess = NULL;
    tViewTree          *pViewTree = NULL;
    tSNMP_OCTET_STRING_TYPE *pViewName = NULL;

    if (pVacmInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pVacmInfo->pContextName->i4_Length != SNMP_ZERO)
    {
        pContext = VACMGetContext (pVacmInfo->pContextName);
        if (pContext == NULL)
        {
            *pu4Err = SNMP_ERR_NO_CONTEXT;
            SNMP_INR_UNAVIAL_CONTEXTS;
            return SNMP_FAILURE;
        }
    }

    pSecGrp =
        VACMGetSecGrp ((INT4) pVacmInfo->u4SecModel, pVacmInfo->pUserName);
    if (pSecGrp == NULL)
    {
        *pu4Err = SNMP_ERR_NO_GROUP_NAME;
        return SNMP_FAILURE;
    }

    pAccess = VACMGetAccess (&(pSecGrp->Group), pVacmInfo->pContextName,
                             (INT4) pVacmInfo->u4SecModel,
                             (INT4) pVacmInfo->u4SecLevel);
    if (pAccess == NULL)
    {
        *pu4Err = SNMP_ERR_NO_ACCESS_ENTRY;
        return SNMP_FAILURE;
    }
    /* For Write View alone */
    if ((i4ViewType == SNMP_READONLY) &&
        (pAccess->ReadName.i4_Length == SNMP_ZERO) &&
        (pAccess->WriteName.i4_Length != SNMP_ZERO))
    {
        i4ViewType = SNMP_READWRITE;
    }
    switch (i4ViewType)
    {
        case SNMP_READONLY:
            pViewName = &(pAccess->ReadName);
            break;
        case SNMP_READWRITE:
            pViewName = &(pAccess->WriteName);
            break;
        case SNMP_ACCESSIBLEFORNOTIFY:
            pViewName = &(pAccess->NotifyName);
            break;
        default:
            return SNMP_NO_SUCH_VIEW;
    }

    if (pViewName->i4_Length == SNMP_ZERO)
    {
        *pu4Err = SNMP_NO_SUCH_VIEW;
        return SNMP_FAILURE;
    }

    pViewTree = VACMViewTreeGetLongestMatch (pViewName, pOID, pu4Err);

    if (pViewTree == NULL ||
        pViewTree->i4Type == SNMP_VACM_VIEW_TREE_TYPE_EXCLUDED)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*********************************************************************
*  Function Name : VACMCheckAccessTree 
*  Description   : Function to check the entry from Acces table
*  Parameter(s)  : pName    - View Name
*  Return Values : SNMP_SUCCESS/SNMP_FAILURE 
*********************************************************************/
INT4
VACMCheckAccessTree (tSNMP_OCTET_STRING_TYPE * pName)
{
    UINT4               u4Index = SNMP_ZERO;
    tTMO_SLL_NODE      *pListNode = NULL;
    tAccessEntry       *pAccessEntry = NULL;
    UINT4               u4MatchFound = SNMP_ZERO;

    for (u4Index = SNMP_ZERO; u4Index < SNMP_MAX_VIEW_TREE; u4Index++)
    {
        if (gaViewTree[u4Index].i4Status == SNMP_INACTIVE)
        {
            TMO_SLL_Scan (&gSnmpAccessEntry, pListNode, tTMO_SLL_NODE *)
            {
                pAccessEntry = (tAccessEntry *) pListNode;
                gaViewTree[u4Index].pName = NULL;
                if (SNMPCompareOctetString (&(pAccessEntry->ReadName),
                                            pName) == SNMP_EQUAL)
                {
                    gaViewTree[u4Index].pName = &(pAccessEntry->ReadName);
                    u4MatchFound = SNMP_ONE;
                    break;
                }
                if (SNMPCompareOctetString (&(pAccessEntry->WriteName),
                                            pName) == SNMP_EQUAL)
                {
                    gaViewTree[u4Index].pName = &(pAccessEntry->WriteName);
                    u4MatchFound = SNMP_ONE;
                    break;
                }
                if (SNMPCompareOctetString (&(pAccessEntry->NotifyName),
                                            pName) == SNMP_EQUAL)
                {
                    gaViewTree[u4Index].pName = &(pAccessEntry->NotifyName);
                    u4MatchFound = SNMP_ONE;
                    break;
                }
            }
        }
        if (u4MatchFound == SNMP_ONE)
        {
            break;
        }

    }
    if (u4Index == SNMP_MAX_VIEW_TREE)
    {
        return SNMP_FAILURE;
    }

    if (u4Index < SNMP_MAX_VIEW_TREE && gaViewTree[u4Index].pName == NULL)
    {                            /*kloc */
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/********************  END OF FILE   ***************************************/
