#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                     |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX ( Slackware 1.2.1 )                     |
# |                                                                          |
# |   DATE                   : 17 Dec 2008                                   |
# |                                                                          |
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################

TOTAL_OPNS =  ${BEEP_SWITCHES} $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################
BEEP_BASE_DIR  = ${BASE_DIR}/beep
BEEP_OBJ_DIR   = ${BEEP_BASE_DIR}/obj

BPCMN_BASE_DIR  = ${BEEP_BASE_DIR}/beepcmn
BPCMN_INC_DIR   = ${BPCMN_BASE_DIR}/inc
BPCMN_SRC_DIR   = ${BPCMN_BASE_DIR}/src
BPCMN_OBJ_DIR   = ${BPCMN_BASE_DIR}/obj

BPCLT_BASE_DIR  = ${BEEP_BASE_DIR}/beepclt
BPCLT_INC_DIR   = ${BPCLT_BASE_DIR}/inc
BPCLT_SRC_DIR   = ${BPCLT_BASE_DIR}/src
BPCLT_OBJ_DIR   = ${BPCLT_BASE_DIR}/obj


BPSRV_BASE_DIR  = ${BEEP_BASE_DIR}/beepsrv
BPSRV_INC_DIR   = ${BPSRV_BASE_DIR}/inc
BPSRV_SRC_DIR   = ${BPSRV_BASE_DIR}/src
BPSRV_OBJ_DIR   = ${BPSRV_BASE_DIR}/obj

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${BPCMN_INC_DIR} 
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################
