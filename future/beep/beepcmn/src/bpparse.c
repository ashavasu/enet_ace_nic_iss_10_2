/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bpparse.c,v 1.7 2012/04/20 11:33:35 siva Exp $
 *
 * Description: This file parse the beep frame syntax.
 *********************************************************************/
#ifndef __BP_FRAME_PF__
#define __BP_FRAME_PF__
#include "bpcmn.h"
/***********************************************************************
 *      Function Name        : BeepParseFrameHdr                       *
 *      Role of the functio  : Parse the Beep Frame header             *
 *      Input  Parameters    : pi1Data - Pointer to the beep frame     *
 *                             tBeepFrameHdr - Frame header struct     *
 *      Output  Parameter    : None                                    *
 *      Return Value         : BEEP_FAILURE/BEEP_SUCCESS               *
 **********************************************************************/
INT1
BeepParseFrameHdr (INT1 *pi1Data, tBeepFrameHdr * pBeepFrameHdr)
{
    INT1               *pi1FrameHeader = NULL;
    INT1               *pi1Ptr = NULL;
    INT1               *pi1MsgType = NULL;
    INT4                i4Cnt = BEEP_ZERO;
    INT1               *pi1Char = NULL;
    INT1               *pi1Payload = NULL;
    INT1               *pi1Dummy = NULL;
    UINT1              *pu1Char = NULL;
    INT1               *pi1LastPoint = NULL;

    pu1Char = MemAllocMemBlk (gu4BeepCmnPoolId);
    if (pu1Char == NULL)
    {
        return BEEP_FAILURE;
    }
    MEMSET (pu1Char, BEEP_ZERO, BEEP_MAX_FRAME_SIZE);
    pi1Ptr = pi1Data;
    STRCPY (pu1Char, "\n");
    /*Frame is divided into three parts HEADER,PAYLOAD and TRAILER */
    pi1MsgType = (INT1 *) STRTOK (pi1Ptr, pu1Char);
    if (pi1MsgType == NULL)
    {
        MemReleaseMemBlock (gu4BeepCmnPoolId, pu1Char);
        return BEEP_FAILURE;
    }
    /*Header is extracted from frame */
    pi1FrameHeader = (INT1 *) MemAllocMemBlk (gu4BeepCmnPoolId);
    if (pi1FrameHeader == NULL)
    {
        MemReleaseMemBlock (gu4BeepCmnPoolId, pu1Char);
        return BEEP_FAILURE;
    }
    MEMSET (pi1FrameHeader, BEEP_ZERO, BEEP_MAX_FRAME_SIZE);
    pi1LastPoint = &pi1FrameHeader[MAX_FRAME_SIZE - BEEP_ONE];
    MEMCPY (pi1FrameHeader, pi1MsgType, (INT4) (pi1LastPoint - pi1FrameHeader));
    while (pi1FrameHeader[i4Cnt] != '\0')
    {
        if (i4Cnt > (MAX_FRAME_SIZE - BEEP_THREE))
        {
            MemReleaseMemBlock (gu4BeepCmnPoolId, pu1Char);
            MemReleaseMemBlock (gu4BeepCmnPoolId, (UINT1 *) pi1FrameHeader);
            return BEEP_FAILURE;
        }
        i4Cnt++;
    }
    pi1FrameHeader[i4Cnt++] = '\n';
    pi1FrameHeader[i4Cnt] = '\0';

    /*strtok func replaces the delimiter with the '\0'
     *as result of this the "C" present in the frame is replaced by '\0'
     *inorder to replace '\0' with 'C' the below operation is performed*/

    /*pi1Ptr - contains PAYLOAD and TRAILER */
    pi1Ptr = pi1Ptr + STRLEN (pi1FrameHeader);

    if (BeepParseHeader (pi1FrameHeader, pBeepFrameHdr) == BEEP_FAILURE)
    {
        MemReleaseMemBlock (gu4BeepCmnPoolId, pu1Char);
        MemReleaseMemBlock (gu4BeepCmnPoolId, (UINT1 *) pi1FrameHeader);
        return BEEP_FAILURE;
    }
    /*Frame with Message type as NUL wont have any payload
     *This case is handled separately*/
    if (pBeepFrameHdr->i4FrameHeadType == BEEP_NUL)
    {
        *pi1Ptr = '\0';
        /*Check whether TRAILOR ends with \r\n or not,
         *if not return failure*/
        if ((*(--pi1Ptr) != '\n') || (*(--pi1Ptr) != '\r') ||
            (*(--pi1Ptr) != 'D') || (*(--pi1Ptr) != 'N') ||
            (*(--pi1Ptr) != 'E'))
        {
            MemReleaseMemBlock (gu4BeepCmnPoolId, pu1Char);
            MemReleaseMemBlock (gu4BeepCmnPoolId, (UINT1 *) pi1FrameHeader);
            return BEEP_FAILURE;
        }
        MemReleaseMemBlock (gu4BeepCmnPoolId, pu1Char);
        MemReleaseMemBlock (gu4BeepCmnPoolId, (UINT1 *) pi1FrameHeader);
        return BEEP_SUCCESS;
    }

    /*Copy Payload and trailor */
    MEMCPY (pi1FrameHeader, pi1Ptr, (INT4) (pi1LastPoint - pi1FrameHeader));

    /*Delimiter for payload */
    STRCPY (pu1Char, "END");

    /*Check for the presence of trailor "END" in the frame */
    pi1Dummy = (INT1 *) STRSTR (pi1Ptr, pu1Char);
    if (pi1Dummy == NULL)
    {
        MemReleaseMemBlock (gu4BeepCmnPoolId, pu1Char);
        MemReleaseMemBlock (gu4BeepCmnPoolId, (UINT1 *) pi1FrameHeader);
        return BEEP_FAILURE;
    }
    MemReleaseMemBlock (gu4BeepCmnPoolId, pu1Char);
    /*Extract the payload */
    pi1MsgType = pi1Ptr;
    pi1Ptr = (INT1 *) STRSTR (pi1Ptr, "END\r\n");
    if (pi1Ptr == NULL)
    {
        MemReleaseMemBlock (gu4BeepCmnPoolId, (UINT1 *) pi1FrameHeader);
        return BEEP_FAILURE;
    }
    *pi1Ptr = '\0';
    /*Store the payload in *pi1Data */
    pi1Payload = (INT1 *) MemAllocMemBlk (gu4BeepCmnPoolId);
    if (pi1Payload == NULL)
    {
        MemReleaseMemBlock (gu4BeepCmnPoolId, (UINT1 *) pi1FrameHeader);
        return BEEP_FAILURE;
    }
    MEMSET (pi1Payload, BEEP_ZERO, BEEP_MAX_FRAME_SIZE);
    MEMCPY (pi1Payload, pi1MsgType, BEEP_MAX_FRAME_SIZE);
    pi1Payload[MAX_FRAME_SIZE - 1] = '\0';
    pi1Char = pi1FrameHeader;
    /* Here +3 is added because the delimiter "E" of "END" is replaced
     * by '\0',i.e pi1MsgType + STRLEN (i1MsgType) + 1 - contains '\0' 
     *            pi1MsgType + STRLEN (i1MsgType) + 2 - contains 'N'
     *            pi1MsgType + STRLEN (i1MsgType) + 3 - contains 'D'
     *            pi1MsgType + STRLEN (i1MsgType) + 4 - contains '\r'
     *            pi1MsgType + STRLEN (i1MsgType) + 5 - contains '\n'*/
    pi1Char = pi1Char + STRLEN (pi1MsgType) + BEEP_THREE;

    MEMSET (pi1Data, '\0', MAX_FRAME_SIZE);
    STRCPY (pi1Data, pi1Payload);

    /*Validating the payload size
     * + BEEP_THREE is added because, i4size = payload + trailer*/
    if (STRLEN (pi1Payload) != (UINT4) pBeepFrameHdr->i4Size)
    {
        MemReleaseMemBlock (gu4BeepCmnPoolId, (UINT1 *) pi1Payload);
        MemReleaseMemBlock (gu4BeepCmnPoolId, (UINT1 *) pi1FrameHeader);
        return BEEP_FAILURE;
    }
    MemReleaseMemBlock (gu4BeepCmnPoolId, (UINT1 *) pi1Payload);
    MemReleaseMemBlock (gu4BeepCmnPoolId, (UINT1 *) pi1FrameHeader);
    return BEEP_SUCCESS;
}

/**********************************************************************
 *      Function Name        : BeepParseHeader                        *
 *      Role of the functio  : Parse the Beep header                  *
 *      Input  Parameters    : i1Header - Pointer to the beep header  *
 *                             tBeepFrameHdr - Frame header struct    *
 *      Output  Parameter    : None                                   *
 *      Return Value         : BEEP_FAILURE or BEEP_SUCCESS           *
 **********************************************************************/
INT4
BeepParseHeader (INT1 *i1Header, tBeepFrameHdr * pBeepFrameHdr)
{
    INT1                ai1Size[BEEP_SIZE];
    INT1               *pi1Ptr = NULL;
    INT1               *pi1MsgType = NULL;
    INT4                i4Cnt = BEEP_ZERO;
    INT1               *pi1Char = NULL;
    UINT4               u4Token = BEEP_ZERO;
    UINT1               au1Char[BEEP_SIZE];
    INT4                i4Tkn = BEEP_ZERO;
    MEMSET (ai1Size, BEEP_ZERO, BEEP_SIZE);
    MEMSET (&au1Char, BEEP_ZERO, BEEP_SIZE);
    STRCPY (au1Char, " ");

    pi1Ptr = i1Header;
    pi1Char = (INT1 *) au1Char;
    /*Extract the message type */
    pi1MsgType = (INT1 *) STRTOK (pi1Ptr, pi1Char);
    if (pi1MsgType == NULL)
    {
        return BEEP_FAILURE;
    }

    if ((STRCMP (pi1MsgType, "MSG")) == BEEP_ZERO)
    {
        pBeepFrameHdr->i4FrameHeadType = BEEP_MSG;
    }
    if ((STRCMP (pi1MsgType, "RPY")) == BEEP_ZERO)
    {
        pBeepFrameHdr->i4FrameHeadType = BEEP_RPY;
    }
    if ((STRCMP (pi1MsgType, "ANS")) == BEEP_ZERO)
    {
        pBeepFrameHdr->i4FrameHeadType = BEEP_ANS;
    }
    if ((STRCMP (pi1MsgType, "NUL")) == BEEP_ZERO)
    {
        pBeepFrameHdr->i4FrameHeadType = BEEP_NUL;
    }
    if ((STRCMP (pi1MsgType, "ERR")) == BEEP_ZERO)
    {
        pBeepFrameHdr->i4FrameHeadType = BEEP_ERR;
    }
    /*Fill the Header type */

    /*Get the Channel Number from the header */
    pi1Ptr = pi1Ptr + STRLEN (pi1MsgType) + BEEP_ONE;
    /*Checking for the space */
    if (*pi1Ptr == ' ')
    {
        return BEEP_FAILURE;
    }
    pi1MsgType = (INT1 *) STRTOK (pi1Ptr, pi1Char);
    if (pi1MsgType == NULL)
    {
        return BEEP_FAILURE;
    }

    /*Check for the Valid Number */
    if (IsNumber (pi1MsgType) == BEEP_FAILURE)
    {
        return BEEP_FAILURE;
    }
    if ((pi1MsgType == pi1Char) || (pi1MsgType == NULL))
    {
        return BEEP_FAILURE;
    }
    /*Check whether Channel NUM is within the specified range */
    i4Tkn = ATOL (pi1MsgType);
    if ((i4Tkn >= BEEP_ZERO)
        && (STRCMP (pi1MsgType, BEEP_MAX_INT_STR) < BEEP_ZERO))
    {
        pBeepFrameHdr->i4ChnlNum = i4Tkn;
    }
    else
    {
        return BEEP_FAILURE;
    }

    /*Get the Message Number from the header */
    pi1Ptr = pi1Ptr + STRLEN (pi1MsgType) + BEEP_ONE;
    if (*pi1Ptr == ' ')
    {
        return BEEP_FAILURE;
    }
    pi1MsgType = (INT1 *) STRTOK (pi1Ptr, pi1Char);
    /*Check for the Valid Number */
    if (pi1MsgType == NULL)
    {
        return BEEP_FAILURE;
    }

    if (IsNumber (pi1MsgType) == BEEP_FAILURE)
    {
        return BEEP_FAILURE;
    }
    if ((pi1MsgType == pi1Char) || (pi1MsgType == NULL))
    {
        return BEEP_FAILURE;
    }
    /*Check whether Message NUM is within the specified range */
    i4Tkn = ATOL (pi1MsgType);
    if ((i4Tkn >= BEEP_ZERO)
        && (STRCMP (pi1MsgType, BEEP_MAX_INT_STR) < BEEP_ZERO))
    {
        pBeepFrameHdr->i4MsgNo = i4Tkn;
    }
    else
    {
        return BEEP_FAILURE;
    }

    /*Get the more field from the header */
    pi1Ptr = pi1Ptr + STRLEN (pi1MsgType) + BEEP_ONE;
    if (*pi1Ptr == ' ')
    {
        return BEEP_FAILURE;
    }
    pi1MsgType = (INT1 *) STRTOK (pi1Ptr, pi1Char);
    if ((pi1MsgType == pi1Char) || (pi1MsgType == NULL))
    {
        return BEEP_FAILURE;
    }
    if ((STRCMP (pi1MsgType, ".")) == BEEP_ZERO)
    {
        pBeepFrameHdr->i4More = BEEP_DOT;
    }
    if ((STRCMP (pi1MsgType, "*")) == BEEP_ZERO)
    {
        pBeepFrameHdr->i4More = BEEP_STAR;
    }

    /*Get the Sequence Number from the header */
    pi1Ptr = pi1Ptr + STRLEN (pi1MsgType) + BEEP_ONE;
    if (*pi1Ptr == ' ')
    {
        return BEEP_FAILURE;
    }
    pi1MsgType = (INT1 *) STRTOK (pi1Ptr, pi1Char);
    /*Check whether Sequence NUM is within the specified range */
    if ((pi1MsgType == pi1Char) || (pi1MsgType == NULL))
    {
        return BEEP_FAILURE;
    }
    if (IsNumber (pi1MsgType) == BEEP_FAILURE)
    {
        return BEEP_FAILURE;
    }

    if (STRLEN (pi1MsgType) > STRLEN (BEEP_MAX_UINT))
    {
        return BEEP_FAILURE;
    }

    if (STRLEN (pi1MsgType) == STRLEN (BEEP_MAX_UINT))
    {
        if ((STRCMP (pi1MsgType, BEEP_MAX_UINT)) > BEEP_ZERO)
        {
            return BEEP_FAILURE;
        }
    }

    pBeepFrameHdr->u4SeqNo = ATOL (pi1MsgType);

    if (pBeepFrameHdr->i4FrameHeadType != BEEP_ANS)
    {
        /*Get the Size of the frame from the header
         *and check whether header ends with \r\n,
         *if not return failure*/
        pi1Ptr = pi1Ptr + STRLEN (pi1MsgType) + BEEP_ONE;
        if (*pi1Ptr == ' ')
        {
            return BEEP_FAILURE;
        }
        while (*pi1Ptr != '\r')
        {
            ai1Size[i4Cnt] = *pi1Ptr;
            i4Cnt++;
            pi1Ptr++;
        }
        /*Terminate the string with '\0' */
        ai1Size[i4Cnt] = '\0';
        /*Inc the pointer and check whether \n is followed by \r
         *if not return failure*/
        pi1Ptr++;
        if (*pi1Ptr != '\n')
        {
            return BEEP_FAILURE;
        }
        /*Check whether Size is within the specified range */
        u4Token = ATOL (ai1Size);
        /*Check for the Valid Number */
        if (IsNumber (ai1Size) == BEEP_FAILURE)
        {
            return BEEP_FAILURE;
        }
        if ((u4Token > BEEP_ZERO) && (u4Token <= BEEP_MAX_INT))
        {
            pBeepFrameHdr->i4Size = ATOL (ai1Size);
        }
        else
        {
            return BEEP_FAILURE;
        }
        pBeepFrameHdr->i4AnsNo = BEEP_INVALID;
    }
    else if (pBeepFrameHdr->i4FrameHeadType == BEEP_ANS)
    {
        /*Get the Size from the header */
        pi1Ptr = pi1Ptr + STRLEN (pi1MsgType) + BEEP_ONE;
        if (*pi1Ptr == ' ')
        {
            return BEEP_FAILURE;
        }
        pi1MsgType = (INT1 *) STRTOK (pi1Ptr, pi1Char);
        /*Check for the Valid Number */
        if (pi1MsgType == NULL)
        {
            return BEEP_FAILURE;
        }

        if (IsNumber (pi1MsgType) == BEEP_FAILURE)
        {
            return BEEP_FAILURE;
        }
        if ((pi1MsgType == pi1Char) || (pi1MsgType == NULL))
        {
            return BEEP_FAILURE;
        }
        /*Check whether SIZE is within the specified range */
        u4Token = ATOL (pi1MsgType);
        if ((u4Token > BEEP_ZERO) && (u4Token <= BEEP_MAX_INT))
        {
            pBeepFrameHdr->i4Size = u4Token;
        }
        else
        {
            return BEEP_FAILURE;
        }
        /*Get the  ANS NUM of the frame from the header
         *and check whether header ends with \r\n,
         *if not return failure*/
        pi1Ptr = pi1Ptr + STRLEN (pi1MsgType) + BEEP_ONE;
        if (*pi1Ptr == ' ')
        {
            return BEEP_FAILURE;
        }
        while (*pi1Ptr != '\r')
        {
            ai1Size[i4Cnt] = *pi1Ptr;
            i4Cnt++;
            pi1Ptr++;
        }
        ai1Size[i4Cnt] = '\0';
        /*Inc the pointer and check whether \n is followed by \r
         *if not return failure*/
        pi1Ptr++;
        if (*pi1Ptr != '\n')
        {
            return BEEP_FAILURE;
        }
        /*Check whether ANS NUM is within the specified range */
        u4Token = ATOL (ai1Size);
        /*Check for the Valid Number */
        if (IsNumber (ai1Size) == BEEP_FAILURE)
        {
            return BEEP_FAILURE;
        }
        if (u4Token <= BEEP_MAX_INT)
        {
            pBeepFrameHdr->i4AnsNo = ATOL (ai1Size);
        }
        else
        {
            return BEEP_FAILURE;
        }
    }
    return BEEP_SUCCESS;
}

/**********************************************************************
 *      Function Name        : IsNumber                               *
 *      Role of the functio  : Checks for the valid Number            *
 *      Input  Parameters    : i1Token - Pointer to the token         *
 *      Output  Parameter    : None                                   *
 *      Return Value         : BEEP_FAILURE or BEEP_SUCCESS           *
 **********************************************************************/
INT4
IsNumber (INT1 *pi1Token)
{
    while (*pi1Token != '\0')
    {
        if ((*pi1Token >= '0') && (*pi1Token <= '9'))
        {
            pi1Token++;
        }
        else
        {
            return BEEP_FAILURE;
        }
    }
    return BEEP_SUCCESS;
}
#endif
