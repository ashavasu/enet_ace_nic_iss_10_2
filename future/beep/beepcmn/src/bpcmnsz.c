
/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: bpcmnsz.c,v 1.2 2013/11/29 11:04:11 siva Exp $
*
* Description:Mempool creation for Beep common files 
******************************************************************/
#define _BEEP_CMNSZ_C
#include "bpcmn.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
Beep_cmnSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < BEEP_CMN_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsBEEP_CMNSizingParams[i4SizingId].u4StructSize,
                              FsBEEP_CMNSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(BEEP_CMNMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            Beep_cmnSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
Beep_cmnSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsBEEP_CMNSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, BEEP_CMNMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
Beep_cmnSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < BEEP_CMN_MAX_SIZING_ID; i4SizingId++)
    {
        if (BEEP_CMNMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (BEEP_CMNMemPoolIds[i4SizingId]);
            BEEP_CMNMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
