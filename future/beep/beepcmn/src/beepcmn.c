/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: beepcmn.c,v 1.5 2014/01/25 13:55:32 siva Exp $
 *
 * Description:This file contains the functions which are common to 
 *              Beep server and Beep Client.   
 *
 *******************************************************************/

#include "lr.h"
#include "bpcmn.h"

UINT4               gu4BeepCmnPoolId;

/**************************************************************************/
/*   Function Name   : BeepCmnInit                                        */
/*   Description     :  This Function is used to create mempools          */
/*                                                                        */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                          */
/*   Return Value    : BEEP_SUCCESS or BEEP_FAILURE                       */
/**************************************************************************/
INT4
BeepCmnInit (VOID)
{
    if (gu4BeepCmnPoolId == 0)
    {
        if (Beep_cmnSizingMemCreateMemPools () == OSIX_FAILURE)
        {
            return BEEP_FAILURE;
        }

        gu4BeepCmnPoolId = BEEP_CMNMemPoolIds[BEEP_MAX_FRAME_BLOCKS_SIZING_ID];
    }
    return BEEP_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : BeepAddMimeHdr                                     */
/*   Description     :  This Function adds mime header to the string      */
/*                                                                        */
/*   Input(s)        : Paylod                                             */
/*   Output(s)       : Payload with Mime Hdr                              */
/*   Return Value    : BEEP_SUCCESS or BEEP_FAILURE                       */
/**************************************************************************/

INT4
BeepAddMimeHdr (INT1 *pi1String)
{
    INT1               *pi1TmpStr = NULL;

    pi1TmpStr = (INT1 *) MemAllocMemBlk (gu4BeepCmnPoolId);
    if (pi1TmpStr == NULL)
    {
        return BEEP_FAILURE;
    }
    MEMSET (pi1TmpStr, BEEP_ZERO, BEEP_MAX_FRAME_SIZE);
    STRCPY (pi1TmpStr, "Content-Type: application/beep+xml\r\n");
    STRNCAT (pi1TmpStr, pi1String,
             MEM_MAX_BYTES (STRLEN (pi1String),
                            (BEEP_MAX_FRAME_SIZE - STRLEN (pi1TmpStr))));
    MEMSET (pi1String, BEEP_ZERO, BEEP_MAX_FRAME_SIZE);
    MEMCPY ((UINT1 *) pi1String, (UINT1 *) pi1TmpStr, BEEP_MAX_FRAME_SIZE);
    MemReleaseMemBlock (gu4BeepCmnPoolId, (UINT1 *) pi1TmpStr);
    return BEEP_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : BeepMakeFrame                                      */
/*   Description     : MAkes a Beep frame with the given Payload          */
/*                                                                        */
/*   Input(s)        : Paylod                                             */
/*   Output(s)       : Payload with Mime Hdr                              */
/*   Return Value    : BEEP_SUCCESS or BEEP_FAILURE                       */
/**************************************************************************/

INT4
BeepMakeFrame (INT4 i4BeepHdr, tBeepChnlDb * pBeepChnl, INT1 *pi1Msg)
{
    INT4                i4Size = BEEP_ZERO;
    INT1               *pi1TmpBuff = NULL;

    pi1TmpBuff = (INT1 *) MemAllocMemBlk (gu4BeepCmnPoolId);
    if (pi1TmpBuff == NULL)
    {
        return BEEP_FAILURE;
    }
    MEMSET ((UINT1 *) pi1TmpBuff, BEEP_ZERO, BEEP_MAX_BUFF_SIZE);
    i4Size = STRLEN (pi1Msg);

    switch (i4BeepHdr)
    {
        case BEEP_MSG_FRAME:
            pBeepChnl->i4LastMsg++;
            SPRINTF ((CHR1 *) pi1TmpBuff, "MSG %d %d . %u %d\r\n",
                     pBeepChnl->i4ChnlNum,
                     pBeepChnl->i4LastMsg, pBeepChnl->u4SeqNoToSend, i4Size);
            pBeepChnl->u4SeqNoToSend = pBeepChnl->u4SeqNoToSend + i4Size;
            break;

        case BEEP_RPY_FRAME:
            pBeepChnl->i4LastRpy = pBeepChnl->i4LastMsg;
            SPRINTF ((CHR1 *) pi1TmpBuff,
                     "RPY %d %d . %u %d\r\n", pBeepChnl->i4ChnlNum,
                     pBeepChnl->i4LastRpy, pBeepChnl->u4SeqNoToSend, i4Size);
            pBeepChnl->u4SeqNoToSend = pBeepChnl->u4SeqNoToSend + i4Size;
            break;

        case BEEP_ERR_FRAME:
            pBeepChnl->i4LastRpy = pBeepChnl->i4LastMsg;
            SPRINTF ((CHR1 *) pi1TmpBuff,
                     "ERR %d %d . %u %d\r\n", pBeepChnl->i4ChnlNum,
                     pBeepChnl->i4LastRpy, pBeepChnl->u4SeqNoToSend, i4Size);
            pBeepChnl->u4SeqNoToSend = pBeepChnl->u4SeqNoToSend + i4Size;
            break;

        case BEEP_ANS_FRAME:
            pBeepChnl->i4LastAnsNum++;
            pBeepChnl->i4LastRpy = pBeepChnl->i4LastMsg;
            SPRINTF ((CHR1 *) pi1TmpBuff,
                     "ANS %d %d . %u %d %d\r\n", pBeepChnl->i4ChnlNum,
                     pBeepChnl->i4LastRpy, pBeepChnl->u4SeqNoToSend,
                     i4Size, pBeepChnl->i4LastAnsNum);
            pBeepChnl->u4SeqNoToSend = pBeepChnl->u4SeqNoToSend + i4Size;

            break;

        case BEEP_NUL_FRAME:
            pBeepChnl->i4LastRpy = pBeepChnl->i4LastMsg;
            SPRINTF ((CHR1 *) pi1TmpBuff,
                     "NUL %d %d . %u 0\r\n", pBeepChnl->i4ChnlNum,
                     pBeepChnl->i4LastRpy, pBeepChnl->u4SeqNoToSend);
            pBeepChnl->i4LastAnsNum = BEEP_MINUS_ONE;
            break;

        default:
            break;
    }

    STRCAT (pi1TmpBuff, pi1Msg);
    MEMSET ((UINT1 *) pi1Msg, BEEP_ZERO, BEEP_MAX_BUFF_SIZE);
    SPRINTF ((CHR1 *) pi1Msg, "%sEND\r\n", (CHR1 *) pi1TmpBuff);
    MemReleaseMemBlock (gu4BeepCmnPoolId, (UINT1 *) pi1TmpBuff);
    return BEEP_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : FormStartMsg                                       */
/*   Description     :  This Function forms the start message             */
/*                                                                        */
/*   Input(s)        : Channel No, Profile                                */
/*   Output(s)       : Payload with Start Message                         */
/*   Return Value    : BEEP_SUCCESS or BEEP_FAILURE                       */
/**************************************************************************/

INT4
FormStartMsg (INT4 i4ChNo, INT4 i4Profile, UINT1 *pu1WriteBuff)
{

    SPRINTF ((CHR1 *) pu1WriteBuff, "<start number='%d'>\r\n<profile uri=",
             i4ChNo);
    /* Addition of new profile needs updation here */
    if (i4Profile == BEEP_RAW_PROFILE)
    {
        STRCAT (pu1WriteBuff, BEEP_RAW_NAME);
    }

    if (i4Profile == BEEP_TLS_PROFILE)
    {
        STRCAT (pu1WriteBuff, BEEP_TLS_NAME);
    }

    if (i4Profile == BEEP_DIGEST_MD5_PROFILE)
    {
        STRCAT (pu1WriteBuff, BEEP_DIGEST_MD5_NAME);
    }

    STRCAT (pu1WriteBuff, " />\r\n</start>\r\n");
    return BEEP_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : FormCloseMsg                                       */
/*   Description     : This Function forms the close message              */
/*                                                                        */
/*   Input(s)        : Channel No, code                                   */
/*   Output(s)       : Payload with Close Message                         */
/*   Return Value    : BEEP_SUCCESS or BEEP_FAILURE                       */
/**************************************************************************/

INT4
FormCloseMsg (INT4 i4ChNo, INT4 i4Code, UINT1 *pu1WriteBuff)
{
    SPRINTF ((CHR1 *) pu1WriteBuff,
             "<close number='%d' code='%d' />\r\n", i4ChNo, i4Code);
    return BEEP_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : FormErrMsg                                         */
/*   Description     : This Function forms the error message              */
/*                                                                        */
/*   Input(s)        : Error code, Error String                           */
/*   Output(s)       : Payload with Error Message                         */
/*   Return Value    : BEEP_SUCCESS or BEEP_FAILURE                       */
/**************************************************************************/

INT4
FormErrMsg (INT4 i4Code, UINT1 *pu1ErrString, UINT1 *pu1WriteBuff)
{
    if (pu1ErrString != NULL)
    {
        SPRINTF ((CHR1 *) pu1WriteBuff,
                 "<error code='%d'>%s</error>\r\n", i4Code, pu1ErrString);
    }
    else
    {
        SPRINTF ((CHR1 *) pu1WriteBuff, "<error code='%d' />", i4Code);
    }
    return BEEP_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : FormProfileMsg                                     */
/*   Description     : This Function forms the profile message            */
/*                                                                        */
/*   Input(s)        : Profile                                            */
/*   Output(s)       : Payload with profile Message                         */
/*   Return Value    : BEEP_SUCCESS or BEEP_FAILURE                       */
/**************************************************************************/

INT4
FormProfileMsg (INT4 i4Profile, UINT1 *pu1WriteBuff)
{
    SPRINTF ((CHR1 *) pu1WriteBuff, "<profile uri=");
    /* Addition of new profile needs updation here */
    if (i4Profile == BEEP_RAW_PROFILE)
    {
        STRCAT (pu1WriteBuff, BEEP_RAW_NAME);
    }

    if (i4Profile == BEEP_TLS_PROFILE)
    {
        STRCAT (pu1WriteBuff, BEEP_TLS_NAME);
    }

    if (i4Profile == BEEP_DIGEST_MD5_PROFILE)
    {
        STRCAT (pu1WriteBuff, BEEP_DIGEST_MD5_NAME);
    }

    STRCAT (pu1WriteBuff, " />\r\n");
    return BEEP_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : FormBlobMsg                                        */
/*   Description     : This Function forms the Blob message               */
/*                                                                        */
/*   Input(s)        : Blob                                               */
/*   Output(s)       : Payload with blob Message                          */
/*   Return Value    : BEEP_SUCCESS or BEEP_FAILURE                       */
/**************************************************************************/

INT4
FormBlobMsg (UINT1 *pu1Blob, UINT1 *pu1WriteBuff)
{
    SPRINTF ((CHR1 *) pu1WriteBuff, "<blob>%s</blob>\r\n", pu1Blob);
    return BEEP_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : FormClntGreetingMsg                                */
/*   Description     : This Function forms the Client Greeting message    */
/*                                                                        */
/*   Input(s)        : None                                               */
/*   Output(s)       : Payload with blob Message                          */
/*   Return Value    : BEEP_SUCCESS or BEEP_FAILURE                       */
/**************************************************************************/

INT4
FormClntGreetingMsg (UINT1 *pu1WriteBuff)
{
    SPRINTF ((CHR1 *) pu1WriteBuff, "<greeting />\r\n");
    return BEEP_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : FormOkMsg                                          */
/*   Description     : This Function forms the Ok message                 */
/*                                                                        */
/*   Input(s)        : pu1WriteBuff                                       */
/*   Output(s)       : Payload with blob Message                          */
/*   Return Value    : BEEP_SUCCESS or BEEP_FAILURE                       */
/**************************************************************************/

INT4
FormOkMsg (UINT1 *pu1WriteBuff)
{
    SPRINTF ((CHR1 *) pu1WriteBuff, "<ok />\r\n");
    return BEEP_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : BeepValidateFrame                               */
/*   Description     : This function validates the information in the     */
/*                     frame headre of the payload                        */
/*   Input(s)        : pFrameHdr - HEader details                         */
/*   Output(s)       : None                                               */
/*   Return Value    : BEEP_SUCCCESS / BEEP_FAILURE                     */
/**************************************************************************/

INT4
BeepValidateFrame (tBeepFrameHdr * pFrameHdr, tBeepChnlDb * pChnlNode)
{
    /* Checking whether the channel is active */
    if (pChnlNode == NULL)
    {
        return BEEP_FAILURE;
    }

    /* Checking all the the parameters in the header */
    /* Verifying the message number */
    if ((pFrameHdr->i4FrameHeadType == BEEP_RPY_FRAME) ||
        (pFrameHdr->i4FrameHeadType == BEEP_ANS_FRAME) ||
        (pFrameHdr->i4FrameHeadType == BEEP_ERR_FRAME))
    {
        if (pChnlNode->i4LastMsg != pFrameHdr->i4MsgNo)
        {
            return BEEP_FAILURE;
        }
        pChnlNode->i4LastRpy = pFrameHdr->i4MsgNo;
    }

    if (pFrameHdr->i4FrameHeadType == BEEP_MSG_FRAME)
    {
        if (pChnlNode->i4LastMsg >= pFrameHdr->i4MsgNo)
        {
            return BEEP_FAILURE;
        }
        pChnlNode->i4LastMsg = pFrameHdr->i4MsgNo;
    }

    if (pFrameHdr->i4FrameHeadType == BEEP_ANS_FRAME)
    {
        if (pChnlNode->i4LastAnsNum >= pFrameHdr->i4AnsNo)
        {
            return BEEP_FAILURE;
        }
        pChnlNode->i4LastAnsNum = pFrameHdr->i4AnsNo;
    }

    /* Verifying the sequence number */
    if (pFrameHdr->u4SeqNo != pChnlNode->u4ExpectSeqNo)
    {
        return BEEP_FAILURE;
    }

    pChnlNode->u4ExpectSeqNo = pChnlNode->u4ExpectSeqNo + pFrameHdr->i4Size;

    return BEEP_SUCCESS;

}
