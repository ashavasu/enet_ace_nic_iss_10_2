/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bpcmn.h,v 1.4 2012/04/20 11:33:34 siva Exp $
 *
 * Description: Header file contains data structures for beep server
 *
 *******************************************************************/


#ifndef __BP_CMN_H_
#define __BP_CMN_H_
#include "lr.h"
#include "bpcmnsz.h"

#define BEEP_ENABLE 1
#define BEEP_DISABLE 2

#define BEEP_TRUE  1
#define BEEP_FALSE  0

/* Numbers to be used */
#define BEEP_ZERO          0
#define BEEP_ONE   1
#define BEEP_TWO   2
#define BEEP_THREE 3
#define BEEP_SUCCESS 1
#define BEEP_FAILURE -1

/* Sizing parameters  */
#define BEEP_MAX_SOK_WRITE_LEN 1024
#define BEEP_MAX_BUFF_SIZE     BEEP_MAX_FRAME_SIZE
#define BEEP_MIN_CHNL_NUM      0
#define BEEP_MAX_CHNL_NUM      255


/* Frame Related */
#define BEEP_MSG_FRAME        1
#define BEEP_RPY_FRAME        2
#define BEEP_ERR_FRAME        3
#define BEEP_ANS_FRAME        4
#define BEEP_NUL_FRAME        5
#define BEEP_SEQ_FRAME        6

/* Logging severity level */
#define BEEP_EMERGENCY      0     /* Emergency: system is unusable*/
#define BEEP_ALERT          1     /* Alert: action must be taken immediately*/
#define BEEP_CRITICAL       2     /* Critical: critical conditions */
#define BEEP_ERROR          3     /* Error: error conditions */
#define BEEP_WARNING        4     /* Warning: warning conditions */
#define BEEP_NOTICE         5     /* Notice:normal but significant condition*/
#define BEEP_INFO           6     /* Informational: informational messages*/
#define BEEP_DEBUG          7     /* Debug: debug-level messages*/

/* Profile Names (New profile to be ported should be appended here)*/
#define BEEP_CH_MGMT_PROFILE       0
#define BEEP_RAW_PROFILE           1
#define BEEP_TLS_PROFILE           2
#define BEEP_DIGEST_MD5_PROFILE    3

#define BEEP_RAW_NAME "'http://xml.resource.org/profiles/syslog/RAW'"
#define BEEP_TLS_NAME "'http://iana.org/beep/TLS'"
#define BEEP_DIGEST_MD5_NAME "'http://iana.org/beep/SASL/DIGEST-MD5'"

#define BEEP_MAX_CHAR  250
#define BEEP_MAX_BLOB_SIZE 500
#define BEEP_MAX_PROFILE 5
#define BEEP_MAX_DIGEST_LEN 500


#define BEEP_SIZE          20

#define BEEP_INVALID      -1
#define BEEP_NONE         -1
#define BEEP_GREETING      1
#define BEEP_PROFILE       2
#define BEEP_START         4
#define BEEP_CLOSE         5
#define BEEP_OK            6

#define BEEP_NOT_RCVD      0
#define BEEP_BLOB          1
#define BEEP_CODE          2
#define BEEP_MINUS_ONE     -1
#define BEEP_READY         1
#define BEEP_PROCEED       2
#define BEEP_MAX_SIZE      200
#define BEEP_MAX_ERR_SIZE  BEEP_MAX_SIZE

#define BEEP_MAX_PROFILE   5
#define BEEP_MAX_INT       2147483647
#define BEEP_MAX_UINT      "4294967295"
#define BEEP_MAX_INT_STR   "2147483647"
#define MAX_FRAME_SIZE     2000
#define BEEP_MAX_DIGIT     10

#define BEEP_MSG     BEEP_MSG_FRAME      
#define BEEP_RPY     BEEP_RPY_FRAME      
#define BEEP_ANS     BEEP_ANS_FRAME
#define BEEP_NUL     BEEP_NUL_FRAME      
#define BEEP_ERR     BEEP_ERR_FRAME

#define BEEP_DOT           1
#define BEEP_STAR          2

#define BEEP_INT_TYPE      1
#define BEEP_UINT_TYPE     2


/* Beep Error codes */
#define BEEP_SERVICE_NOT_AVAIL 421
#define BEEP_SUCCESS_CLOSE     200
#define BEEP_TRANSACTION_FAIL  554
#define BEEP_INVALID_XML       501



/* Type defs used by the functions */

typedef struct BeepFrameHdr
{
    INT4    i4ChnlNum;
    INT4    i4MsgNo;
    INT4    i4Size;
    INT4    i4AnsNo;
    INT4    i4FrameHeadType; /* MSG -1 RPY-2 ANS-3 NUL-4 SEQ-5 */
    INT4    i4More;
    UINT4   u4SeqNo;
}tBeepFrameHdr;



typedef struct BeepChnlDb
{
    INT4 i4ChnlNum; /* Channel Number*/
    INT4 i4Profile; /* describes the channel profile.E.g 1 =RAW*/
    INT4 i4ProfileState; /* current state of the channel*/
    INT4 i4LastMsg; /* last msg num sent or received */
    INT4 i4LastRpy; /* last reply msg num sent or received */
    UINT4 u4SeqNoToSend; /* next seq num of the octet to be
                            sent on the channel*/
    UINT4 u4ExpectSeqNo; /* valid next seq num to be received on the channel*/
    INT4 i4LastAnsNum; /* valid next Answer number */
} tBeepChnlDb;


extern UINT4   gu4BeepCmnPoolId;
/* Proto types */
INT4 BeepCmnInit (VOID);
INT1 BeepParseFrameHdr (INT1 *i1data, tBeepFrameHdr *pBeepFrameHdr);
INT4 BeepParseHeader (INT1 *i1Header, tBeepFrameHdr *pBeepFrameHdr);
INT4 IsNumber(INT1 *i1Token);
INT4 BeepAddMimeHdr (INT1* pi1String);
INT4 BeepMakeFrame(INT4 i4BeepHdr, tBeepChnlDb *pBeepChnl, INT1 *pi1Msg);
INT4 FormStartMsg(INT4 i4ChNo, INT4 i4Profile, UINT1 *pu1WriteBuff);
INT4 FormCloseMsg(INT4 i4ChNo, INT4 i4Code, UINT1 *pu1WriteBuff);
INT4 FormErrMsg(INT4 i4Code,UINT1 *pu1ErrString, UINT1 *pu1WriteBuff);
INT4 FormProfileMsg(INT4 i4Profile, UINT1 *pu1WriteBuff);
INT4 FormBlobMsg(UINT1 *pu1Blob, UINT1 *pu1WriteBuff);
INT4 FormClntGreetingMsg (UINT1 *pu1WriteBuff);
INT4 FormOkMsg (UINT1 *pu1WriteBuff);
INT4 BeepValidateFrame PROTO ((tBeepFrameHdr *pFrameHdr, 
                               tBeepChnlDb *pChnlNode));

#endif
