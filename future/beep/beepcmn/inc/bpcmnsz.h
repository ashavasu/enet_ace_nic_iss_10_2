
/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: bpcmnsz.h,v 1.1 2012/04/20 11:40:39 siva Exp $
*
* Description:macros and prototypes for bpcmnsz.c
******************************************************************/
enum {
    BEEP_MAX_FRAME_BLOCKS_SIZING_ID,
    BEEP_CMN_MAX_SIZING_ID
};


#ifdef  _BEEP_CMNSZ_C
tMemPoolId BEEP_CMNMemPoolIds[ BEEP_CMN_MAX_SIZING_ID];
INT4  Beep_cmnSizingMemCreateMemPools(VOID);
VOID  Beep_cmnSizingMemDeleteMemPools(VOID);
INT4  Beep_cmnSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _BEEP_CMNSZ_C  */
extern tMemPoolId BEEP_CMNMemPoolIds[ ];
extern INT4  Beep_cmnSizingMemCreateMemPools(VOID);
extern VOID  Beep_cmnSizingMemDeleteMemPools(VOID);
#endif /*  _BEEP_CMNSZ_C  */


#ifdef  _BEEP_CMNSZ_C
tFsModSizingParams FsBEEP_CMNSizingParams [] = {
{ "INT1[BEEP_MAX_FRAME_SIZE]", "BEEP_MAX_FRAME_BLOCKS", sizeof(INT1[BEEP_MAX_FRAME_SIZE]),BEEP_MAX_FRAME_BLOCKS, BEEP_MAX_FRAME_BLOCKS,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _BEEP_CMNSZ_C  */
extern tFsModSizingParams FsBEEP_CMNSizingParams [];
#endif /*  _BEEP_CMNSZ_C  */


