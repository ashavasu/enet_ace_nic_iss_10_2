/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bpsrvmd5.c,v 1.3 2012/01/13 12:16:48 siva Exp $
 *
 * Description:This file contains the functions to handle the messages
 *             coming for DIGEST-MD5 profile associated channel
 *             in beep server module
 *
 *******************************************************************/

#include "bpsrvinc.h"

/**************************************************************************/
/*   Function Name   : BeepSrvMd5Start                                    */
/*   Description     : This function starts a new channel asssociated with*/
/*                     DIGEST-MD5 profile                                 */
/*   Input(s)        : u4ChNo                                             */
/*   Output(s)       : None                                               */
/*   Return Value    : BPSRV_SUCCESS or BPSRV_FAILURE                     */
/**************************************************************************/

INT4
BeepSrvMd5Start (UINT4 u4ChNo)
{

    BPSRV_CURR_CHDB[u4ChNo] = (tBeepChnlDb *)
        MemAllocMemBlk (BPSRV_CHNL_MEMPOOL_ID);
    if (BPSRV_CURR_CHDB[u4ChNo] == NULL)
    {
        return BPSRV_FAILURE;
    }
    BPSRV_CURR_CHDB[u4ChNo]->i4ChnlNum = u4ChNo;
    BPSRV_CURR_CHDB[u4ChNo]->i4Profile = DIGEST_MD5_PROFILE;
    BPSRV_CURR_CHDB[u4ChNo]->i4ProfileState = BPSRV_CH_OPENED;
    BPSRV_CURR_CHDB[u4ChNo]->i4LastMsg = BEEP_MINUS_ONE;
    BPSRV_CURR_CHDB[u4ChNo]->i4LastRpy = BEEP_MINUS_ONE;
    BPSRV_CURR_CHDB[u4ChNo]->u4SeqNoToSend = BEEP_ZERO;
    BPSRV_CURR_CHDB[u4ChNo]->u4ExpectSeqNo = BEEP_ZERO;
    BPSRV_CURR_CHDB[u4ChNo]->i4LastAnsNum = BEEP_MINUS_ONE;
    return BPSRV_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : BeepSrvMd5ProfileHdlr                              */
/*   Description     : This function handles the date coming on to the    */
/*                     Chnl asso. with DIGEST-MD5 profile                 */
/*   Input(s)        : pFrameHdr, pi1ReadBuff                             */
/*   Output(s)       : None                                               */
/*   Return Value    : BPSRV_SUCCESS or BPSRV_FAILURE                     */
/**************************************************************************/

INT4
BeepSrvDigestMd5Hdlr (tBeepFrameHdr * pFrameHdr, INT1 *pi1ReadBuff)
{
    UINT1               au1BeepDigest[BEEP_MAX_DIGEST_LEN];
    UINT1               au1AuthKey[BPSRV_MAX_AUTH_KEY_SIZE];
    tBeepSrvMd5Data    *pMd5ChnlData = NULL;

    MEMSET (au1BeepDigest, BEEP_ZERO, BEEP_MAX_DIGEST_LEN);
    MEMSET (au1AuthKey, BEEP_ZERO, BPSRV_MAX_AUTH_KEY_SIZE);
    pMd5ChnlData =
        (tBeepSrvMd5Data *) MemAllocMemBlk (BPSRV_MD5_CHNL_DATA_MEMPOOL_ID);

    if (pMd5ChnlData != NULL)
    {
        MEMSET (pMd5ChnlData, BEEP_ZERO, sizeof (tBeepSrvMd5Data));
    }
    else
    {
        BPSRV_ACTIVE_SESSION->i1SsnEvtFlag = BPSRV_UNEXPECTED;
        return BPSRV_SUCCESS;
    }

    if (pFrameHdr->i4FrameHeadType != BEEP_MSG_FRAME)
    {
        BPSRV_ACTIVE_SESSION->i1SsnEvtFlag = BPSRV_UNEXPECTED;
        MemReleaseMemBlock (BPSRV_MD5_CHNL_DATA_MEMPOOL_ID,
                            (UINT1 *) pMd5ChnlData);
        return BPSRV_SUCCESS;
    }
    BeepSrvParseMd5data (pMd5ChnlData, pi1ReadBuff);
    BPSRV_LOCK ();
    STRCPY (au1AuthKey, gBeepSrvGblInfo.au1BeepSrvAuthKey);
    BPSRV_UNLOCK ();
    BeepSrvMd5GetDigest (BPSRV_ACTIVE_SESSION->au1Md5ChallengeSent,
                         au1AuthKey, au1BeepDigest);
    if (!(STRCMP (pMd5ChnlData->au1Blob, au1BeepDigest)))
    {
        MEMSET (BPSRV_ACTIVE_SESSION->ai1WriteBuff,
                BEEP_ZERO, BEEP_MAX_BUFF_SIZE);
        SPRINTF ((CHR1 *) BPSRV_ACTIVE_SESSION->ai1WriteBuff,
                 "<blob status='complete' />\r\n");
        BeepAddMimeHdr (BPSRV_ACTIVE_SESSION->ai1WriteBuff);
        BeepMakeFrame (BEEP_RPY_FRAME, BPSRV_CURR_CHDB[pFrameHdr->i4ChnlNum],
                       BPSRV_ACTIVE_SESSION->ai1WriteBuff);
        BPSRV_ACTIVE_SESSION->i1SsnEvtFlag = BPSRV_DIGEST_MD5_OK;

    }
    else
    {
        MEMSET (BPSRV_ACTIVE_SESSION->ai1WriteBuff,
                BEEP_ZERO, BEEP_MAX_BUFF_SIZE);
        SPRINTF ((CHR1 *) BPSRV_ACTIVE_SESSION->ai1WriteBuff,
                 "<error code='550' />\r\n");
        BeepAddMimeHdr (BPSRV_ACTIVE_SESSION->ai1WriteBuff);
        BeepMakeFrame (BEEP_RPY_FRAME, BPSRV_CURR_CHDB[pFrameHdr->i4ChnlNum],
                       BPSRV_ACTIVE_SESSION->ai1WriteBuff);
        BPSRV_ACTIVE_SESSION->i1SsnEvtFlag = BPSRV_DIGEST_MD5_FAIL;
    }

    BeepSrvSendClient (BPSRV_ACTIVE_SESSION->ai1WriteBuff);
    MemReleaseMemBlock (BPSRV_MD5_CHNL_DATA_MEMPOOL_ID, (UINT1 *) pMd5ChnlData);
    return BPSRV_SUCCESS;

}

/**************************************************************************/
/*   Function Name   : BeepSrvMd5GenChallenge                             */
/*   Description     : This function generates random Md5 Challenge to be */
/*                     sent across to the beep client                     */
/*   Input(s)        : None                                               */
/*   Output(s)       : Random Md5 Challenge                               */
/*   Return Value    : BPSRV_SUCCESS or BPSRV_FAILURE                     */
/**************************************************************************/

INT4
BeepSrvMd5GenChallenge (UINT1 *pu1Md5Challenge)
{

    UINT1               u1Val1 = BEEP_ZERO;
    UINT1               u1Val2 = BEEP_ZERO;
    UINT2               u2Cnt = BEEP_ZERO;

    for (u2Cnt = BEEP_ZERO; u2Cnt < BPSRV_MAX_CHALLENGE_LEN; u2Cnt++)
    {
        u1Val1 = (UINT1) (BEEP_ONE +
                          (UINT1) (255.0 * rand () / (RAND_MAX + 1.0)));
        u1Val2 = (UINT1) (BEEP_ONE +
                          (UINT1) (255.0 * rand () / (RAND_MAX + 1.0)));
        *pu1Md5Challenge++ = (UINT1) ((u1Val1 + u1Val2) / BEEP_TWO);
    }
    pu1Md5Challenge[BPSRV_MAX_CHALLENGE_LEN] = '\0';
    return BEEP_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : BeepSrvMd5GetMd5Digest                             */
/*   Description     : This function generates random Md5 Digest to be    */
/*                     expected from the beep client                      */
/*   Input(s)        : pu1Md5ChallengeSent, pu1AuthKey                    */
/*   Output(s)       : Md5 Digest                                         */
/*   Return Value    : BPSRV_SUCCESS or BPSRV_FAILURE                     */
/**************************************************************************/

INT4 
     
     
     
     
     
     
     
    BeepSrvMd5GetDigest
    (UINT1 *pu1Md5ChallengeSent, UINT1 *pu1AuthKey, UINT1 *pu1BeepDigest)
{
    UNUSED_PARAM (pu1Md5ChallengeSent);
    UNUSED_PARAM (pu1AuthKey);
    UNUSED_PARAM (pu1BeepDigest);
    return BPSRV_SUCCESS;

}

/**************************************************************************/
/*   Function Name   : BeepSrvMd5GetMd5Digest                             */
/*   Description     : This function generates random Md5 Digest to be    */
/*                     expected from the beep client                      */
/*   Input(s)        : pMd5ChnlData                                       */
/*   Output(s)       : Md5 Digest                                         */
/*   Return Value    : BPSRV_SUCCESS or BPSRV_FAILURE                     */
/**************************************************************************/

INT4
BeepSrvParseMd5data (tBeepSrvMd5Data * pMd5ChnlData, INT1 *pi1ReadBuff)
{
    UNUSED_PARAM (pMd5ChnlData);
    UNUSED_PARAM (pi1ReadBuff);
    return BPSRV_SUCCESS;
}
