# include  "lr.h"
# include  "fssnmp.h"
# include  "fsbpsrlw.h"
# include  "fsbpsrwr.h"
# include  "fsbpsrdb.h"

VOID
RegisterFSBPSR ()
{
    SNMPRegisterMib (&fsbpsrOID, &fsbpsrEntry, SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fsbpsrOID, (const UINT1 *) "fsbpsrv");
}

VOID
UnRegisterFSBPSR ()
{
    SNMPUnRegisterMib (&fsbpsrOID, &fsbpsrEntry);
    SNMPDelSysorEntry (&fsbpsrOID, (const UINT1 *) "fsbpsrv");
}

INT4
FsBeepServerAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsBeepServerAdminStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsBeepServerRawProfileGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsBeepServerRawProfile (&(pMultiData->i4_SLongValue)));
}

INT4
FsBeepServerIpv4PortNumGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsBeepServerIpv4PortNum (&(pMultiData->i4_SLongValue)));
}

INT4
FsBeepServerIpv6PortNumGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsBeepServerIpv6PortNum (&(pMultiData->i4_SLongValue)));
}

INT4
FsBeepServerAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsBeepServerAdminStatus (pMultiData->i4_SLongValue));
}

INT4
FsBeepServerIpv4PortNumSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsBeepServerIpv4PortNum (pMultiData->i4_SLongValue));
}

INT4
FsBeepServerIpv6PortNumSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsBeepServerIpv6PortNum (pMultiData->i4_SLongValue));
}

INT4
FsBeepServerAdminStatusTest (UINT4 *pu4Error,
                             tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsBeepServerAdminStatus
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsBeepServerIpv4PortNumTest (UINT4 *pu4Error,
                             tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsBeepServerIpv4PortNum
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsBeepServerIpv6PortNumTest (UINT4 *pu4Error,
                             tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsBeepServerIpv6PortNum
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsBeepServerAdminStatusDep (UINT4 *pu4Error,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsBeepServerAdminStatus (pu4Error,
                                             pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsBeepServerIpv4PortNumDep (UINT4 *pu4Error,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsBeepServerIpv4PortNum (pu4Error,
                                             pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsBeepServerIpv6PortNumDep (UINT4 *pu4Error,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsBeepServerIpv6PortNum (pu4Error,
                                             pSnmpIndexList, pSnmpvarbinds));
}
