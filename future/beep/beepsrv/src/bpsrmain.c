/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: bpsrmain.c,v 1.15 2012/04/20 11:33:33 siva Exp $
 * 
 * Description: This file contains the Init, Memory allocation, Task
 *              creation, Queue creation for the Beep Server Module
 * *******************************************************************/

#include "bpsrvinc.h"

#ifndef _BPSRV_MAIN_C_
#define _BPSRV_MAIN_C_

tBeepSrvGbl         gBeepSrvGblInfo;
UINT2               gu2BpSrvTrcModule;

/**************************************************************************/
/*   Function Name   : BeepSrvTaskMain                                    */
/*   Description     : This function awaits server to be enabled by user  */
/*                              in syslog module                          */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : BPSRV_SUCCESS or BPSRV_FAILURE                     */
/**************************************************************************/

VOID
BpSrvTaskMain (INT1 *pi1Param)
{
    INT4                i4SrvState = BPSRV_DISABLE;
    INT4                i4RetVal = BPSRV_FAILURE;
    UINT4               u4EventReceived = BEEP_ZERO;
    INT4                i4SysLogId = BEEP_ZERO;
    INT4                i4CliSockFd = BPSRV_INV_SOCK_FD;
    INT4                i4BeepSrvPort = BPSRV_DEFAULT_PORT;
    INT4                i4BeepSrv6Port = BPSRV_DEFAULT_PORT;
    tBeepSrvSessionDb  *pBeepSrvSessionDb = NULL;

    UNUSED_PARAM (pi1Param);

    /* Getting Task Id of Self and initializing the global Data structure */
    if (OsixTskIdSelf (&gBeepSrvGblInfo.BeepSrvTaskId) == OSIX_FAILURE)
    {
        BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_INIT_SHUT_TRC, BPSRV_NAME,
                   "Exiting Function BeepCltTaskMain \n");

        /* Indicate the status of initialization to the main routine */
        BPSRV_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Creation of memory pools, timerlists, semaphores */
    /*Initialization of Globals */

    i4RetVal = BeepSrvInit ();

#ifdef SYSLOG_WANTED
    /* Register with SysLog Server to Log Error Messages. */
    i4SysLogId =
        SYS_LOG_REGISTER ((CONST UINT1 *) "BEEP_SRV", SYSLOG_ALERT_LEVEL);

    if (i4SysLogId < 0)
    {
        /* Indicate the status of initialization to the main routine */
        BPSRV_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    BPSRV_ID = i4SysLogId;
#else
    UNUSED_PARAM (i4SysLogId);
#endif

    /* Return if server init fails */

    if (i4RetVal == BPSRV_FAILURE)
    {
        BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_INIT_SHUT_TRC, BPSRV_NAME,
                   "Unable to create Memory for Beep Data Structures \n");
        OsixTskDel (gBeepSrvGblInfo.BeepSrvTaskId);
        BPSRV_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    BPSRV_INIT_COMPLETE (OSIX_SUCCESS);

#ifdef SNMP_2_WANTED
    RegisterFSBPSR ();
#endif

    /* The Main Loop of the task which awaits various events  */
    for (; BPSRV_SUCCESS;)
    {
        gBeepSrvGblInfo.i4BeepSrvActiveSock = BPSRV_INV_SOCK_FD;
        gBeepSrvGblInfo.BeepSrvActiveSsn = NULL;

        /* Receive the event */
        u4EventReceived = BEEP_ZERO;
        OsixEvtRecv (BPSRV_TASK_ID, (BPSRV_CONF_CHANGE_EVENT |
                                     BPSRV_TCP_NEW_CONN_RCVD |
                                     BPSRV_TCP6_NEW_CONN_RCVD |
                                     BPSRV_TURNED_ON |
                                     BPSRV_TURNED_OFF |
                                     BPSRV_PKT_RCVD),
                     BPSRV_EVENT_WAIT_FLAG, &u4EventReceived);

        if (u4EventReceived & BPSRV_TURNED_ON)
        {
            i4SrvState = BPSRV_ENABLED;
        }

        if (u4EventReceived & BPSRV_TURNED_OFF)
        {
            if (gBeepSrvGblInfo.i4SessionCount > BEEP_ZERO)
            {
                BeepSrvForceCloseAllSsn ();
                gBeepSrvGblInfo.i4SessionCount = BEEP_ZERO;
            }
            BeepSrvSelRemoveFd (gBeepSrvGblInfo.i4BeepSrvListenSock);
            BeepSrvCloseSocket (gBeepSrvGblInfo.i4BeepSrvListenSock);
            /*   close (gBeepSrvGblInfo.i4BeepSrvListenSock); */
#ifdef IP6_WANTED
            /* Socket needs to be closed only incase of IP6_WANTED.
             * */
            BeepSrvSelRemoveFd (gBeepSrvGblInfo.i4BeepSrvListen6Sock);
            BeepSrvCloseSocket (gBeepSrvGblInfo.i4BeepSrvListen6Sock);
#endif

            i4SrvState = BPSRV_DISABLED;
            continue;
        }

        if (i4SrvState == BPSRV_DISABLED)
        {
            continue;
        }

        if ((u4EventReceived & BPSRV_CONF_CHANGE_EVENT) ||
            (u4EventReceived & BPSRV_TURNED_ON))
        {
            if (u4EventReceived & BPSRV_CONF_CHANGE_EVENT)
            {
                if (gBeepSrvGblInfo.i4SessionCount > BEEP_ZERO)
                {
                    BeepSrvForceCloseAllSsn ();
                    gBeepSrvGblInfo.i4SessionCount = BEEP_ZERO;
                }

                BeepSrvSelRemoveFd (gBeepSrvGblInfo.i4BeepSrvListenSock);
                BeepSrvCloseSocket (gBeepSrvGblInfo.i4BeepSrvListenSock);
#ifdef IP6_WANTED
                /* Socket needs to be closed only incase of IP6_WANTED.
                 * */
                BeepSrvSelRemoveFd (gBeepSrvGblInfo.i4BeepSrvListen6Sock);
                BeepSrvCloseSocket (gBeepSrvGblInfo.i4BeepSrvListen6Sock);
#endif
            }

            i4BeepSrvPort = gBeepSrvGblInfo.i4BeepSrvPort;
            i4BeepSrv6Port = gBeepSrvGblInfo.i4BeepSrv6Port;
            i4SrvState = gBeepSrvGblInfo.i4BeepSrvState;

            BeepSrvSockInit (i4BeepSrvPort);
#ifdef IP6_WANTED
            BeepSrvSock6Init (i4BeepSrv6Port);
#endif
        }

        /* EVent of New client connects to the server through ipv4 */
        if (u4EventReceived & BPSRV_TCP_NEW_CONN_RCVD)
        {

            i4CliSockFd =
                BeepSrvAcceptNewConnection (gBeepSrvGblInfo.
                                            i4BeepSrvListenSock);
            if (BPSRV_FAILURE != i4CliSockFd)
            {
                /* Session table creation */
                pBeepSrvSessionDb = BeepSrvAllocSsnDb ();
                if (pBeepSrvSessionDb != NULL)
                {
                    gBeepSrvGblInfo.i4BeepSrvActiveSock = i4CliSockFd;
                    BeepSrvInitSsnDb (pBeepSrvSessionDb);
                    pBeepSrvSessionDb->i4SockFd = i4CliSockFd;
                }
                else
                {
                    BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_OS_RESOURCE_TRC,
                               BPSRV_NAME,
                               "Cannot Init memory for new Session!");
                    continue;
                }

                /* Adding the session node to the session list */
                BeepSrvAddSession (pBeepSrvSessionDb);

                /* Set the session pointer as current session */
                if (BPSRV_FAILURE == BeepSrvSetPtrToGbl (i4CliSockFd))
                {
                    BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                               " Cannot set Ptr to Global\r\n");
                    continue;
                }

                /* Send the greetings message as soon as the connection 
                 * is established */
                if (BPSRV_SUCCESS == BeepSrvSendGreeting ())
                {
                    BeepSrvSelAddFd (i4CliSockFd, BeepSrvPktRcvd);
                    BeepSrvSelAddFd (gBeepSrvGblInfo.i4BeepSrvListenSock,
                                     BeepSrvSockCallBk);
                    BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_CONTROL_PATH_TRC,
                               BPSRV_NAME, "New connection added \r\n");
                }
                else
                {
                    BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                               "Greetings send Failed!\r\n");
                    BeepSrvForceCloseSsn (i4CliSockFd);
                }
                gBeepSrvGblInfo.i4SessionCount++;
            }

        }

        /* EVent of New client connects to the server through ipv6 */
        if (u4EventReceived & BPSRV_TCP6_NEW_CONN_RCVD)
        {
            if (gBeepSrvGblInfo.i4SessionCount == BPSRV_MAX_SESSION)
            {
                BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_DATA_PATH_TRC, BPSRV_NAME,
                           "Maximum Sessions Reached: Cannot accept conn!");
            }

            i4CliSockFd =
                BeepSrvAcceptNewConnection (gBeepSrvGblInfo.
                                            i4BeepSrvListen6Sock);
            if (i4CliSockFd != BPSRV_FAILURE)
            {
                /* Session table creation */
                pBeepSrvSessionDb = BeepSrvAllocSsnDb ();
                if (pBeepSrvSessionDb != NULL)
                {
                    TMO_SLL_Init_Node (&pBeepSrvSessionDb->Next);
                    gBeepSrvGblInfo.i4BeepSrvActiveSock = i4CliSockFd;
                    BeepSrvInitSsnDb (pBeepSrvSessionDb);
                    pBeepSrvSessionDb->i4SockFd = i4CliSockFd;
                }
                else
                {
                    BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_OS_RESOURCE_TRC,
                               BPSRV_NAME,
                               "Cannot Init memory for new Session!");
                    continue;
                }

                /* Adding the session node to the session list */
                BeepSrvAddSession (pBeepSrvSessionDb);

                /* Set the session pointer as current session */
                if (BPSRV_FAILURE == BeepSrvSetPtrToGbl (i4CliSockFd))
                {
                    BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                               " Cannot set Ptr to Global\r\n");
                    continue;
                }

                /* Send the greetings message as soon as the connection
                 * is established */
                if (BPSRV_SUCCESS == BeepSrvSendGreeting ())
                {
                    BeepSrvSelAddFd (gBeepSrvGblInfo.i4BeepSrvListen6Sock,
                                     BeepSrvSock6CallBk);
                    BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_CONTROL_PATH_TRC,
                               BPSRV_NAME, "New connection added \r\n");
                }
                else
                {
                    BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                               "Greetings send Failed!\r\n");
                    BeepSrvForceCloseSsn (i4CliSockFd);
                }
                gBeepSrvGblInfo.i4SessionCount++;
            }

        }

        /* When a packet is received from the client */

        if (u4EventReceived & BPSRV_PKT_RCVD)
        {
            do
            {
                i4CliSockFd = BeepSrvDeQueSock ();
                if (i4CliSockFd == BPSRV_FAILURE)
                {
                    continue;
                }

                gBeepSrvGblInfo.i4BeepSrvActiveSock = i4CliSockFd;

                if (BPSRV_FAILURE == BeepSrvSetPtrToGbl (i4CliSockFd))
                {
                    BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                               "Cannot find the session DB for the socket given!");
                    continue;
                }

                /* Start the session activity with the packet received from 
                 * the client */
                if (BPSRV_FAILURE == BeepSrvSsnHdlr ())
                {
                    BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                               "Session Not Available  \r\n");
                }
            }
            while (i4CliSockFd != BPSRV_FAILURE);
        }

    }

}

/**************************************************************************/
/*   Function Name   : BeepSrvInit                                        */
/*   Description     : This function allcoates & initializes the          */
/*                     memory and other resources                         */
/*                     required for the BEEP server                       */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : BPSRV_SUCCESS or BPSRV_FAILURE                     */
/**************************************************************************/

INT4
BeepSrvInit (VOID)
{
    /* Create the semaphore identifier to access the BEEP 
     * server datastructures */
    if (OsixSemCrt (BPSRV_SEM_NAME,
                    &(gBeepSrvGblInfo.BeepSrvSemId)) != OSIX_SUCCESS)
    {
        return BPSRV_FAILURE;
    }
    OsixSemGive (gBeepSrvGblInfo.BeepSrvSemId);

    /* Beep Cmn Mempool creation */
    if (BeepCmnInit () == BEEP_FAILURE)
    {
        return BPSRV_FAILURE;
    }

    if (Beep_serverSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        return BPSRV_FAILURE;
    }

    gBeepSrvGblInfo.BeepSrvSsnMemPoolId =
        BEEP_SERVERMemPoolIds[BPSRV_MAX_SRV_SESS_DB_SIZING_ID];
    gBeepSrvGblInfo.BeepSrvChnlMemPoolId =
        BEEP_SERVERMemPoolIds[BPSRV_MAX_CHNL_DB_SIZING_ID];
    gBeepSrvGblInfo.BeepSrvMsgDbMemPoolId =
        BEEP_SERVERMemPoolIds[BPSRV_MAX_SRV_MSG_DB_SIZING_ID];
    gBeepSrvGblInfo.BeepSrvMd5ChnlDataMemPoolId =
        BEEP_SERVERMemPoolIds[BPSRV_MAX_MD5_CHNL_DATA_SIZING_ID];
    gBeepSrvGblInfo.BeepSrvMgmntChnlMemPoolId =
        BEEP_SERVERMemPoolIds[BPSRV_MAX_CHNL_DATA_SIZING_ID];
    gBeepSrvGblInfo.BeepSrvBufferMemPoolId =
        BEEP_SERVERMemPoolIds[BPSRV_MAX_BUFFER_BLOCKS_SIZING_ID];

    /* Create Queue for receiving the sock fds on pkt received event hit */
    if (OsixQueCrt ((UINT1 *) BPSRV_Q, OSIX_MAX_Q_MSG_LEN,
                    BPSRV_Q_DEPTH, &gBeepSrvGblInfo.BeepSrvQId) != OSIX_SUCCESS)
    {
        BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_INIT_SHUT_TRC, BPSRV_NAME,
                   "Unable to create Beep Server Queue \n");
        /* BPSRV_DBG (BPSRV_DBG_EXIT, "Exiting Function BeepSrvTaskMain \n"); */
        Beep_serverSizingMemDeleteMemPools ();
        /* Indicate the status of initialization to the main routine */
        return BPSRV_FAILURE;
    }
    /* Initializing Globals(MIB) */

    gBeepSrvGblInfo.i4BeepSrvState = BPSRV_DISABLE;
    gBeepSrvGblInfo.i4BeepSrvTlsFlg = BPSRV_DISABLE;
    gBeepSrvGblInfo.i4BeepSrvMd5Flg = BPSRV_DISABLE;
    gBeepSrvGblInfo.i4BeepSrvRawFlg = BPSRV_ENABLE;
    gBeepSrvGblInfo.i4BeepSrvPort = BPSRV_DEFAULT_PORT;
    gBeepSrvGblInfo.i4BeepSrv6Port = BPSRV_DEFAULT_PORT;
    gBeepSrvGblInfo.i4SessionCount = BEEP_ZERO;
    gBeepSrvGblInfo.i4BeepSrvActiveSock = BPSRV_INV_SOCK_FD;
    TMO_SLL_Init (&gBeepSrvGblInfo.BeepSrvSsnList);
    return BPSRV_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : BeepSrvLock                                        */
/*   Description     : This function takes the Beep Server protocol       */
/*                     semaphore                                          */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : SNMP_SUCCESS or SNMP_FAILURE                       */
/**************************************************************************/
INT4
BeepSrvLock (VOID)
{
    INT4                i4RetVal = SNMP_FAILURE;
    if (OsixSemTake (gBeepSrvGblInfo.BeepSrvSemId) != OSIX_FAILURE)
    {
        i4RetVal = SNMP_SUCCESS;
    }
    else
    {
        BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                   "Beep Server Semaphore Lock failed.\n");
    }
    return i4RetVal;
}

/**************************************************************************/
/*   Function Name   : BeepSrvUnLock                                      */
/*   Description     : This function releases the Beep Server protocl     */
/*   semaphore                                                            */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : BPSRV_SUCCESS or BPSRV_FAILURE                     */
/**************************************************************************/
INT4
BeepSrvUnLock (VOID)
{
    OsixSemGive (gBeepSrvGblInfo.BeepSrvSemId);
    return SNMP_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : BeepSrvSetPtrToGbl                                 */
/*   Description     : This function sets the required sesson Db          */
/*                     as global                                          */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : BPSRV_SUCCESS or BPSRV_FAILURE                     */
/**************************************************************************/
INT4
BeepSrvSetPtrToGbl (INT4 i4Sock)
{
    /* set the active session to be processed */
    tBeepSrvSessionDb  *pTmpSsn = NULL;
    if ((pTmpSsn = BeepSrvGetSsnDb (i4Sock)) != NULL)
    {
        BPSRV_ACTIVE_SESSION = pTmpSsn;
        return BPSRV_SUCCESS;
    }
    else
    {
        BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                   "Cannot find the Session");
        return BPSRV_FAILURE;
    }

}

/**************************************************************************/
/*   Function Name   : BeepSrvDeQueSock                                   */
/*   Description     : This function pops out the socket in the           */
/*                     Beep Server Queue                                  */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : Sock Fd  or BPSRV_FAILURE                          */
/**************************************************************************/
INT4
BeepSrvDeQueSock ()
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    INT4                i4SockFd = BPSRV_INV_SOCK_FD;
    tBeepSrvCltMsg     *pMsg = NULL;
    if (OsixQueRecv (BPSRV_Q_ID, (UINT1 *) &pBuf,
                     OSIX_DEF_MSG_LEN, BEEP_ZERO) == OSIX_SUCCESS)
    {
        pMsg = (tBeepSrvCltMsg *) BPSRV_GET_MODULE_DATA_PTR (pBuf);
        i4SockFd = pMsg->i4SockId;
        return i4SockFd;
    }
    return BPSRV_FAILURE;

}

/**************************************************************************/
/*   Function Name   : BeepSrvShutDown                                    */
/*   Description     : This function Shute down Beep Server               */
/*                     Deletes Mem pools, Semaphores and Queues           */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/
VOID
BeepSrvShutDown ()
{
    Beep_serverSizingMemDeleteMemPools ();
    OsixQueDel (BPSRV_Q_ID);
    OsixSemDel (BPSRV_SEM_ID);
    OsixTskDel (BPSRV_TASK_ID);

    return;

}
#endif
