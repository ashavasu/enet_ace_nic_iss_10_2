/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bpsrvchm.c,v 1.11 2014/03/16 11:22:48 siva Exp $
 *
 * Description:This file contains the functions needed for
 *            Beep Server Channel management profile
 *
 *******************************************************************/

#include "bpsrvinc.h"

/**************************************************************************/
/*   Function Name   : BeepSrvChMgmtHdlr                                  */
/*   Description     : This function takes and reads the active session   */
/*                     socket and parse and pass the data to profile hdlrs*/
/*   Input(s)        : pFrameHdr - Frame Details                          */
/*                     pi1Frame  - paylod                                 */
/*   Output(s)       : None                                               */
/*   Return Value    : BPSRV_SUCCESS or BPSRV_FAILURE                     */
/**************************************************************************/

INT4
BeepSrvChMgmtHdlr (tBeepFrameHdr * pFrameHdr, INT1 *pi1Frame)
{

    switch (BPSRV_CURR_CHDB[pFrameHdr->i4ChnlNum]->i4ProfileState)
    {
        case BPSRV_GREETINGS_SENT:
            BeepSrvGrtSent (pFrameHdr, pi1Frame);
            break;

        case BPSRV_GREETINGS_RCVD:
            BeepSrvGrtRcvd (pFrameHdr, pi1Frame);
            break;

        case BPSRV_ON_PROCESS:
            BeepSrvOnProcess (pFrameHdr, pi1Frame);
            break;

        case BPSRV_CLOSE_SENT:
            BPSRV_ACTIVE_SESSION->i1SsnEvtFlag = BPSRV_OVER;
            break;

        default:
            BPSRV_ACTIVE_SESSION->i1SsnEvtFlag = BPSRV_UNEXPECTED;
            break;
    }

    return BPSRV_SUCCESS;

}

/**************************************************************************/
/*   Function Name   : BeepSrvGrtSent                                     */
/*   Description     : This function handles the data when arrived at     */
/*                      Greetings Sent state in channel zero              */
/*   Input(s)        : Frame Details and Payload                          */
/*   Output(s)       : None                                               */
/*   Return Value    : BPSRV_SUCCESS or BPSRV_FAILURE                     */
/**************************************************************************/

INT4
BeepSrvGrtSent (tBeepFrameHdr * pFrameHdr, INT1 *pi1Frame)
{
    tBeepSrvChMgmtData *pBeepSrvFrameData = NULL;

    pBeepSrvFrameData =
        (tBeepSrvChMgmtData *) MemAllocMemBlk (gBeepSrvGblInfo.
                                               BeepSrvMgmntChnlMemPoolId);
    if (pBeepSrvFrameData == NULL)
    {
        return BPSRV_FAILURE;
    }
    MEMSET (pBeepSrvFrameData, BEEP_ZERO, sizeof (tBeepSrvChMgmtData));

    if (pFrameHdr->i4FrameHeadType == BEEP_RPY_FRAME)
    {
        if (BPSRV_SUCCESS ==
            BeepSrvParseChMgmtData (pi1Frame, pBeepSrvFrameData))
        {
            if (pBeepSrvFrameData->i4KeyElementType == BPSRV_GREETING_ELT)
            {
                BPSRV_CURR_CHDB[CH_MGMT]->i4ProfileState = BPSRV_GREETINGS_RCVD;
                MemReleaseMemBlock (gBeepSrvGblInfo.BeepSrvMgmntChnlMemPoolId,
                                    (UINT1 *) pBeepSrvFrameData);
                return BPSRV_SUCCESS;
            }

        }

    }
    SYS_LOG_MSG ((BEEP_ERROR, BPSRV_ID,
                  "Closing Session: No greetings from client"));
    BPSRV_ACTIVE_SESSION->i1SsnEvtFlag = BPSRV_UNEXPECTED;
    MemReleaseMemBlock (gBeepSrvGblInfo.BeepSrvMgmntChnlMemPoolId,
                        (UINT1 *) pBeepSrvFrameData);
    return BPSRV_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : BeepSrvGrtRcvd                                     */
/*   Description     : This function handles the data when arrived  at    */
/*                     greetings received state in channel zero           */
/*   Input(s)        : pFrameHdr - Frame Details                          */
/*                     pi1Frame - Payload                                 */
/*   Output(s)       : None                                               */
/*   Return Value    : BPSRV_SUCCESS or BPSRV_FAILURE                     */
/**************************************************************************/

INT4
BeepSrvGrtRcvd (tBeepFrameHdr * pFrameHdr, INT1 *pi1Frame)
{
    tBeepSrvChMgmtData *pBeepSrvFrameData = NULL;

    pBeepSrvFrameData =
        (tBeepSrvChMgmtData *) MemAllocMemBlk (gBeepSrvGblInfo.
                                               BeepSrvMgmntChnlMemPoolId);
    if (pBeepSrvFrameData == NULL)
    {
        return BPSRV_FAILURE;
    }
    MEMSET (pBeepSrvFrameData, BEEP_ZERO, sizeof (tBeepSrvChMgmtData));
    if (pFrameHdr->i4FrameHeadType == BEEP_MSG_FRAME)
    {
        if (BPSRV_SUCCESS ==
            BeepSrvParseChMgmtData (pi1Frame, pBeepSrvFrameData))
        {
            BPSRV_CURR_CHDB[CH_MGMT]->i4ProfileState = BPSRV_ON_PROCESS;
            switch (pBeepSrvFrameData->i4KeyElementType)
            {
                case BPSRV_CLOSE_ELT:
                    BeepSrvCloseEltHdlr (pBeepSrvFrameData);
                    MemReleaseMemBlock (gBeepSrvGblInfo.
                                        BeepSrvMgmntChnlMemPoolId,
                                        (UINT1 *) pBeepSrvFrameData);
                    return BPSRV_SUCCESS;

                case BPSRV_START_ELT:
                    BeepSrvStartEltHdlr (pBeepSrvFrameData);
                    MemReleaseMemBlock (gBeepSrvGblInfo.
                                        BeepSrvMgmntChnlMemPoolId,
                                        (UINT1 *) pBeepSrvFrameData);
                    return BPSRV_SUCCESS;

                default:
                    break;

            }

        }

    }

    SYS_LOG_MSG ((BEEP_ERROR, BPSRV_ID,
                  "BEEP Server: Closing Session - Unexpected data from client"));
    BPSRV_ACTIVE_SESSION->i1SsnEvtFlag = BPSRV_UNEXPECTED;
    MemReleaseMemBlock (gBeepSrvGblInfo.BeepSrvMgmntChnlMemPoolId,
                        (UINT1 *) pBeepSrvFrameData);
    return BPSRV_SUCCESS;

}

/**************************************************************************/
/*   Function Name   : BeepSrvOnProcess                                   */
/*   Description     : This function hadnles the data when arrived at     */
/*                     on process state in channel zero                   */
/*   Input(s)        : pFrameHdr - Frame Header                           */
/*                     pi1Frame - Payload                                 */
/*   Output(s)       : None                                               */
/*   Return Value    : BPSRV_SUCCESS or BPSRV_FAILURE                     */
/**************************************************************************/

INT4
BeepSrvOnProcess (tBeepFrameHdr * pFrameHdr, INT1 *pi1Frame)
{
    tBeepSrvChMgmtData *pBeepSrvFrameData = NULL;

    pBeepSrvFrameData =
        (tBeepSrvChMgmtData *) MemAllocMemBlk (gBeepSrvGblInfo.
                                               BeepSrvMgmntChnlMemPoolId);
    if (pBeepSrvFrameData == NULL)
    {
        return BPSRV_FAILURE;
    }
    MEMSET (pBeepSrvFrameData, BEEP_ZERO, sizeof (tBeepSrvChMgmtData));
    /* Parse the data in the frame and identify the elements */
    if (BPSRV_SUCCESS == BeepSrvParseChMgmtData (pi1Frame, pBeepSrvFrameData))
    {
        /* switch on frame header and handle expected elements accordingly */
        switch (pFrameHdr->i4FrameHeadType)
        {
            case BEEP_MSG_FRAME:
                if (pBeepSrvFrameData->i4KeyElementType == BPSRV_START_ELT)
                {
                    BeepSrvStartEltHdlr (pBeepSrvFrameData);
                    MemReleaseMemBlock (gBeepSrvGblInfo.
                                        BeepSrvMgmntChnlMemPoolId,
                                        (UINT1 *) pBeepSrvFrameData);
                    return BPSRV_SUCCESS;
                }
                if (pBeepSrvFrameData->i4KeyElementType == BPSRV_CLOSE_ELT)
                {
                    BeepSrvStartEltHdlr (pBeepSrvFrameData);
                    MemReleaseMemBlock (gBeepSrvGblInfo.
                                        BeepSrvMgmntChnlMemPoolId,
                                        (UINT1 *) pBeepSrvFrameData);
                    return BPSRV_SUCCESS;
                }
                break;

            case BEEP_RPY_FRAME:
                if (pBeepSrvFrameData->i4KeyElementType == BPSRV_PROFILE_ELT)
                {
                    BeepSrvProfileEltHdlr (pBeepSrvFrameData);
                    MemReleaseMemBlock (gBeepSrvGblInfo.
                                        BeepSrvMgmntChnlMemPoolId,
                                        (UINT1 *) pBeepSrvFrameData);
                    return BPSRV_SUCCESS;
                }
                if (pBeepSrvFrameData->i4KeyElementType == BPSRV_OK_ELT)
                {
                    BeepSrvOkEltHdlr (pFrameHdr);
                    MemReleaseMemBlock (gBeepSrvGblInfo.
                                        BeepSrvMgmntChnlMemPoolId,
                                        (UINT1 *) pBeepSrvFrameData);
                    return BPSRV_SUCCESS;
                }
                break;

            case BEEP_ERR_FRAME:
                if (pBeepSrvFrameData->i4KeyElementType == BPSRV_ERR_ELT)
                {
                    BeepSrvErrEltHdlr (pBeepSrvFrameData);
                    MemReleaseMemBlock (gBeepSrvGblInfo.
                                        BeepSrvMgmntChnlMemPoolId,
                                        (UINT1 *) pBeepSrvFrameData);
                    return BPSRV_SUCCESS;
                }
                break;

            default:
                break;

        }                        /*End Of Switch case */

    }                            /*End if */

    SYS_LOG_MSG ((BEEP_ERROR, BPSRV_ID,
                  "Closing Session: Unexpected data from client"));
    BPSRV_ACTIVE_SESSION->i1SsnEvtFlag = BPSRV_UNEXPECTED;
    MemReleaseMemBlock (gBeepSrvGblInfo.BeepSrvMgmntChnlMemPoolId,
                        (UINT1 *) pBeepSrvFrameData);
    return BPSRV_SUCCESS;

}

/**************************************************************************/
/*   Function Name   : BeepSrvStartEltHdlr                                */
/*   Description     : This function handles the situation when a start   */
/*                     element and decides on starting a channel          */
/*   Input(s)        : pBeepSrvFrameData - Frame Payload details          */
/*   Output(s)       : None                                               */
/*   Return Value    : BPSRV_SUCCESS or BPSRV_FAILURE                     */
/**************************************************************************/

INT4
BeepSrvStartEltHdlr (tBeepSrvChMgmtData * pBeepSrvFrameData)
{
    INT4                i4Code = BEEP_MINUS_ONE;
    if ((!(pBeepSrvFrameData->u4Num % BEEP_TWO)) |
        (pBeepSrvFrameData->u4Num > BPSRV_MAX_CHANNELS))
    {
        SYS_LOG_MSG ((BEEP_ERROR, BPSRV_ID,
                      "Unsupported channel open request by Client"));
        BPSRV_ACTIVE_SESSION->i1SsnEvtFlag = BPSRV_UNEXPECTED;
        return BPSRV_SUCCESS;
    }

    if (BPSRV_CURR_CHDB[pBeepSrvFrameData->u4Num] != NULL)
    {
        SYS_LOG_MSG ((BEEP_ERROR, BPSRV_ID,
                      "Unavailable Channel open request by Client"));
        BPSRV_ACTIVE_SESSION->i1SsnEvtFlag = BPSRV_UNEXPECTED;
        return BPSRV_SUCCESS;
    }
    /* If the Profile requested is TLS */
    if (!(STRCMP (pBeepSrvFrameData->ai1Profile[BEEP_ZERO], BPSRV_TLS_NAME)))
    {
        if (BPSRV_ACTIVE_SESSION->i1TlsFlag == BPSRV_FLG_ACTIVE)
        {
            MEMSET (BPSRV_ACTIVE_SESSION->ai1WriteBuff,
                    BEEP_ZERO, BPSRV_MAX_BUFF_SIZE);
            /*SPRINTF(BPSRV_ACTIVE_SESSION->ai1WriteBuff,
               "<error code='421'/>\r\n"); */
            i4Code = BEEP_SERVICE_NOT_AVAIL;
            FormErrMsg (i4Code, NULL,
                        (UINT1 *) BPSRV_ACTIVE_SESSION->ai1WriteBuff);
            BeepAddMimeHdr (BPSRV_ACTIVE_SESSION->ai1WriteBuff);
            BeepMakeFrame (BEEP_ERR_FRAME, BPSRV_CURR_CHDB[CH_MGMT],
                           BPSRV_ACTIVE_SESSION->ai1WriteBuff);
            BeepSrvSendClient (BPSRV_ACTIVE_SESSION->ai1WriteBuff);

        }

        if (BPSRV_ACTIVE_SESSION->i1TlsFlag == BPSRV_FLG_NOT_ACTIVE)
        {
            MEMSET (BPSRV_ACTIVE_SESSION->ai1WriteBuff,
                    BEEP_ZERO, BPSRV_MAX_BUFF_SIZE);
            SPRINTF ((CHR1 *) BPSRV_ACTIVE_SESSION->ai1WriteBuff,
                     "<profile uri='http://iana.org/beep/TLS'");
            if (pBeepSrvFrameData->i1CdataRcvd == BPSRV_CDATA_ELT_CODE)
            {
                if (pBeepSrvFrameData->Cdata.i4ElementCode ==
                    BPSRV_ELT_CODE_READY)
                {
                    STRCAT (BPSRV_ACTIVE_SESSION->ai1WriteBuff,
                            ">\r\n<![CDATA[<proceed />]]>\r\n </profile>\r\n");
                }
            }
            else
            {
                STRCAT (BPSRV_ACTIVE_SESSION->ai1WriteBuff, "/>\r\n");
            }

            BeepAddMimeHdr (BPSRV_ACTIVE_SESSION->ai1WriteBuff);
            BeepMakeFrame (BEEP_RPY_FRAME, BPSRV_CURR_CHDB[CH_MGMT],
                           BPSRV_ACTIVE_SESSION->ai1WriteBuff);
            if (BPSRV_SUCCESS ==
                BeepSrvSendClient (BPSRV_ACTIVE_SESSION->ai1WriteBuff))
            {
                BPSRV_ACTIVE_SESSION->i1SsnEvtFlag = BPSRV_TLS_OK;
            }
        }
        return BPSRV_SUCCESS;
    }

    /* If the profile requested is MD5 */
    if (!(STRCMP (pBeepSrvFrameData->ai1Profile[BEEP_ZERO],
                  BPSRV_DIGEST_MD5_NAME)))
    {
        if (BPSRV_ACTIVE_SESSION->i1Md5Flag == BPSRV_FLG_ACTIVE)
        {
            MEMSET (BPSRV_ACTIVE_SESSION->ai1WriteBuff,
                    BEEP_ZERO, BPSRV_MAX_BUFF_SIZE);
            SPRINTF ((CHR1 *) BPSRV_ACTIVE_SESSION->ai1WriteBuff,
                     "<error code='421'/>\r\n");
            BeepAddMimeHdr (BPSRV_ACTIVE_SESSION->ai1WriteBuff);
            BeepMakeFrame (BEEP_ERR_FRAME, BPSRV_CURR_CHDB[CH_MGMT],
                           BPSRV_ACTIVE_SESSION->ai1WriteBuff);
            BeepSrvSendClient (BPSRV_ACTIVE_SESSION->ai1WriteBuff);

        }

        if (BPSRV_ACTIVE_SESSION->i1Md5Flag == BPSRV_FLG_NOT_ACTIVE)
        {
            MEMSET (BPSRV_ACTIVE_SESSION->ai1WriteBuff,
                    BEEP_ZERO, BPSRV_MAX_BUFF_SIZE);
            SPRINTF ((CHR1 *) BPSRV_ACTIVE_SESSION->ai1WriteBuff,
                     "<profile uri='http://iana.org/beep/SASL/DIGEST-MD5'>");

            /* Generate a MD5 challenge and send it */
            BeepSrvMd5GenChallenge (BPSRV_ACTIVE_SESSION->au1Md5ChallengeSent);
            STRCAT (BPSRV_ACTIVE_SESSION->ai1WriteBuff, "\r\n<![CDATA[<blob>");
            STRCAT (BPSRV_ACTIVE_SESSION->ai1WriteBuff,
                    BPSRV_ACTIVE_SESSION->au1Md5ChallengeSent);
            STRCAT (BPSRV_ACTIVE_SESSION->ai1WriteBuff,
                    "</blob>]]>\r\n </profile>\r\n");
            BeepAddMimeHdr (BPSRV_ACTIVE_SESSION->ai1WriteBuff);
            BeepMakeFrame (BEEP_RPY_FRAME, BPSRV_CURR_CHDB[CH_MGMT],
                           BPSRV_ACTIVE_SESSION->ai1WriteBuff);
            if (BPSRV_SUCCESS ==
                BeepSrvSendClient (BPSRV_ACTIVE_SESSION->ai1WriteBuff))
            {
                BPSRV_ACTIVE_SESSION->i1SsnEvtFlag = BPSRV_DIGEST_MD5_OK;
                BeepSrvMd5Start (pBeepSrvFrameData->u4Num);
            }
        }
        return BPSRV_SUCCESS;
    }

    /* If the requested profile is RAW */
    if (!(STRCMP (pBeepSrvFrameData->ai1Profile[BEEP_ZERO], BPSRV_RAW_NAME)))
    {
        MEMSET (BPSRV_ACTIVE_SESSION->ai1WriteBuff,
                BEEP_ZERO, BPSRV_MAX_BUFF_SIZE);
        /*SPRINTF(BPSRV_ACTIVE_SESSION->ai1WriteBuff,
           "<profile uri='http://xml.resource.org/profiles/syslog/RAW'/>"); */
        FormProfileMsg (BEEP_RAW_PROFILE,
                        (UINT1 *) BPSRV_ACTIVE_SESSION->ai1WriteBuff);
        BeepAddMimeHdr (BPSRV_ACTIVE_SESSION->ai1WriteBuff);
        BeepMakeFrame (BEEP_RPY_FRAME, BPSRV_CURR_CHDB[CH_MGMT],
                       BPSRV_ACTIVE_SESSION->ai1WriteBuff);
        BeepSrvSendClient (BPSRV_ACTIVE_SESSION->ai1WriteBuff);
        BeepSrvRawStart (pBeepSrvFrameData->u4Num);
    }
    return BPSRV_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : BeepSrvCloseEltHdlr                                */
/*   Description     : This function handles the situation when a close   */
/*                     element and decides on closing  a channel          */
/*   Input(s)        : pBeepSrvFrameData - Frame Payload details          */
/*   Output(s)       : None                                               */
/*   Return Value    : BPSRV_SUCCESS or BPSRV_FAILURE                     */
/**************************************************************************/

INT4
BeepSrvCloseEltHdlr (tBeepSrvChMgmtData * pBeepSrvFrameData)
{
    if (BPSRV_CURR_CHDB[pBeepSrvFrameData->u4Num] == NULL)
    {
        BPSRV_ACTIVE_SESSION->i1SsnEvtFlag = BPSRV_UNEXPECTED;
        BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                   "Unexpected frame received: Closing session");
        return BPSRV_SUCCESS;
    }

    if (pBeepSrvFrameData->i4Code != BEEP_SUCCESS_CLOSE)
    {
        MEMSET (BPSRV_ACTIVE_SESSION->ai1WriteBuff,
                BEEP_ZERO, sizeof (BPSRV_ACTIVE_SESSION->ai1WriteBuff));
        SPRINTF ((CHR1 *) BPSRV_ACTIVE_SESSION->ai1WriteBuff, "<ok />\r\n");
        BeepAddMimeHdr (BPSRV_ACTIVE_SESSION->ai1WriteBuff);
        BeepMakeFrame (BEEP_RPY_FRAME, BPSRV_CURR_CHDB[CH_MGMT],
                       BPSRV_ACTIVE_SESSION->ai1WriteBuff);
        BeepSrvSendClient (BPSRV_ACTIVE_SESSION->ai1WriteBuff);
        if (pBeepSrvFrameData->u4Num != BEEP_ZERO)
        {
            MemReleaseMemBlock (BPSRV_CHNL_MEMPOOL_ID,
                                (UINT1 *) BPSRV_CURR_CHDB
                                [pBeepSrvFrameData->u4Num]);
        }
        else
        {
            BPSRV_ACTIVE_SESSION->i1SsnEvtFlag = BPSRV_OVER;
        }
        SYS_LOG_MSG ((BEEP_ERR, BPSRV_ID,
                      "Session forcively Closed by Client returning error code"));
        return BPSRV_SUCCESS;
    }

    if (pBeepSrvFrameData->i4Code == BEEP_SUCCESS_CLOSE)
    {
        if (BPSRV_ACTIVE_SESSION->i4ChOpenNeed != BEEP_ZERO)
        {
            MEMSET (BPSRV_ACTIVE_SESSION->ai1WriteBuff,
                    BEEP_ZERO, sizeof (BPSRV_ACTIVE_SESSION->ai1WriteBuff));
            SPRINTF ((CHR1 *) BPSRV_ACTIVE_SESSION->ai1WriteBuff,
                     "<error code='550'> Still Working </error>\r\n");
            BeepAddMimeHdr (BPSRV_ACTIVE_SESSION->ai1WriteBuff);
            BeepMakeFrame (BEEP_ERR_FRAME,
                           BPSRV_CURR_CHDB[CH_MGMT],
                           BPSRV_ACTIVE_SESSION->ai1WriteBuff);
            BeepSrvSendClient (BPSRV_ACTIVE_SESSION->ai1WriteBuff);
            return BPSRV_SUCCESS;
        }

        MEMSET (BPSRV_ACTIVE_SESSION->ai1WriteBuff,
                BEEP_ZERO, sizeof (BPSRV_ACTIVE_SESSION->ai1WriteBuff));
        /*SPRINTF(BPSRV_ACTIVE_SESSION->ai1WriteBuff,"<ok />\r\n"); */
        FormOkMsg ((UINT1 *) BPSRV_ACTIVE_SESSION->ai1WriteBuff);
        BeepAddMimeHdr (BPSRV_ACTIVE_SESSION->ai1WriteBuff);
        BeepMakeFrame (BEEP_RPY_FRAME, BPSRV_CURR_CHDB[CH_MGMT],
                       BPSRV_ACTIVE_SESSION->ai1WriteBuff);
        BeepSrvSendClient (BPSRV_ACTIVE_SESSION->ai1WriteBuff);
        if (pBeepSrvFrameData->u4Num != BEEP_ZERO)
        {
            MemReleaseMemBlock (BPSRV_CHNL_MEMPOOL_ID,
                                (UINT1 *) BPSRV_CURR_CHDB
                                [pBeepSrvFrameData->u4Num]);
        }
        else
        {
            BPSRV_ACTIVE_SESSION->i1SsnEvtFlag = BPSRV_OVER;
        }

    }
    return BPSRV_SUCCESS;

}

/**************************************************************************/
/*   Function Name   : BeepSrvSendGreeting                                */
/*   Description     : This function sends the greetings message during   */
/*                     during session establishment through channel zero  */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : BPSRV_SUCCESS or BPSRV_FAILURE                     */
/**************************************************************************/

INT4
BeepSrvSendGreeting (VOID)
{
    INT1               *pi1Str = NULL;

    BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_DATA_PATH_TRC, BPSRV_NAME,
               "BeepSrvSendGreeting : ENTRY\r\n");

    /* Add greeting tag */
    pi1Str = (INT1 *) MemAllocMemBlk (gBeepSrvGblInfo.BeepSrvBufferMemPoolId);
    if (pi1Str == NULL)
    {
        return BPSRV_FAILURE;
    }
    MEMSET (pi1Str, BEEP_ZERO, BPSRV_MAX_BUFF_SIZE);
    SPRINTF ((CHR1 *) pi1Str, "<greeting>\r\n");

    /* Add Tls profile if enabled */
    if ((BPSRV_ACTIVE_SESSION->i1TlsFlag != BPSRV_FLG_ACTIVE) &&
        (gBeepSrvGblInfo.i4BeepSrvTlsFlg != BPSRV_DISABLE))
    {
        STRCAT (pi1Str, "<profile uri=");
        STRCAT (pi1Str, BPSRV_TLS_NAME);
        STRCAT (pi1Str, " />\r\n");
    }

    /*Add Md5 Prfoile if enabled */
    if ((BPSRV_ACTIVE_SESSION->i1Md5Flag != BPSRV_FLG_ACTIVE) &&
        (gBeepSrvGblInfo.i4BeepSrvMd5Flg != BPSRV_DISABLE))
    {
        STRCAT (pi1Str, "<profile uri=");
        STRCAT (pi1Str, BPSRV_DIGEST_MD5_NAME);
        STRCAT (pi1Str, " />\r\n");
    }

    /* Add RAW profile if enabled */

    if (gBeepSrvGblInfo.i4BeepSrvRawFlg != BPSRV_DISABLE)
    {
        STRCAT (pi1Str, "<profile uri=");
        STRCAT (pi1Str, BPSRV_RAW_NAME);
        STRCAT (pi1Str, " />\r\n");
    }
    /* <--- New prfoile that needs to be ported has to be updated here ---> */

    STRCAT (pi1Str, "</greeting>\r\n");
    BeepAddMimeHdr (pi1Str);

    /* Forming a RPY frame and updatind channel DB */
    /*SPRINTF(ai1SendStr,"RPY 0 0 . 0 %d\r\n",u4Size);
       STRCAT(ai1SendStr, pi1Str);
       STRCAT(ai1SendStr, "END\r\n");
       BPSRV_CURR_CHDB[CH_MGMT]->u4SeqNoToSend = u4Size; */

    BeepMakeFrame (BEEP_RPY_FRAME, BPSRV_CURR_CHDB[CH_MGMT], pi1Str);
    if (BPSRV_FAILURE == BeepSrvSendClient (pi1Str))
    {
        BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                   "Sending failed");
        MemReleaseMemBlock (gBeepSrvGblInfo.BeepSrvBufferMemPoolId,
                            (UINT1 *) pi1Str);
        return BPSRV_FAILURE;
    }
    BPSRV_ACTIVE_SESSION->i4ChOpenNeed = BEEP_ZERO;
    BPSRV_CURR_CHDB[CH_MGMT]->i4ProfileState = BPSRV_GREETINGS_SENT;
    BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_DATA_PATH_TRC, BPSRV_NAME,
               "BeepSrvSendGreeting : EXIT\r\n");
    MemReleaseMemBlock (gBeepSrvGblInfo.BeepSrvBufferMemPoolId,
                        (UINT1 *) pi1Str);
    return BPSRV_SUCCESS;

}

/**************************************************************************/
/*   Function Name   : BeepSrvOkEltHdlr                                   */
/*   Description     : This function handles the situation when a ok      */
/*                     element and decides on closing  a channel          */
/*   Input(s)        : pFrameHdr -  Frame Hdr Details                     */
/*   Output(s)       : None                                               */
/*   Return Value    : BPSRV_SUCCESS or BPSRV_FAILURE                     */
/**************************************************************************/

INT4
BeepSrvOkEltHdlr (tBeepFrameHdr * pFrameHdr)
{
    tBeepSrvMsgDb      *pMsgDb;
    tBeepSrvMsgDb      *pTmp = NULL;
    INT1                i1FoundFlg = BPSRV_FLG_NOT_ACTIVE;
    INT4                i4ChNo = BEEP_ZERO;

    pMsgDb = NULL;

    TMO_SLL_Scan (&(BPSRV_ACTIVE_SESSION->BeepSrvMsgList),
                  pTmp, tBeepSrvMsgDb *)
    {
        pMsgDb = pTmp;
        if ((pMsgDb->i4MsgNo == pFrameHdr->i4MsgNo) &&
            (pMsgDb->i4Msg == CLOSE_MSG))
        {
            i1FoundFlg = BPSRV_FLG_ACTIVE;
            i4ChNo = pMsgDb->i4ChNo;
            break;
        }
    }

    if (i1FoundFlg == BPSRV_FLG_NOT_ACTIVE)
    {
        BPSRV_ACTIVE_SESSION->i1SsnEvtFlag = BPSRV_UNEXPECTED;
        return BPSRV_SUCCESS;
    }
    else
    {
        if (i4ChNo == CH_MGMT)
        {
            BPSRV_ACTIVE_SESSION->i1SsnEvtFlag = BPSRV_OVER;
            return BPSRV_SUCCESS;
        }
        TMO_SLL_Delete (&(BPSRV_ACTIVE_SESSION->BeepSrvMsgList), &pMsgDb->Next);
        MemReleaseMemBlock (BPSRV_MSG_DB_MEMPOOL_ID, (UINT1 *) pMsgDb);
        BeepSrvCloseChnl (i4ChNo);
        return BPSRV_SUCCESS;
    }
}

/**************************************************************************/
/*   Function Name   : BeepSrvCloseCnnl                                   */
/*   Description     : This function deinits the Channel Memory           */
/*   Input(s)        : i4ChNo -  Channel Number                           */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/

VOID
BeepSrvCloseChnl (INT4 i4ChNo)
{
    MemReleaseMemBlock (BPSRV_CHNL_MEMPOOL_ID,
                        (UINT1 *) BPSRV_CURR_CHDB[i4ChNo]);
    BPSRV_CURR_CHDB[i4ChNo] = NULL;
    return;
}

/**************************************************************************/
/*   Function Name   : BeepSrvErrEltHdlr                                  */
/*   Description     : This function handles when a error element is      */
/*                     received from the Beep Client                      */
/*   Input(s)        : pFrameData -  Frame Data                           */
/*   Output(s)       : None                                               */
/*   Return Value    : BPSRV_SUCCESS / BPSRV_FAILURE                      */
/**************************************************************************/

INT4
BeepSrvErrEltHdlr (tBeepSrvChMgmtData * pFrameData)
{
    INT1                ai1SysLogMsg[BPSRV_MAX_SYSLOG_MSG_LEN];

    BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_DATA_PATH_TRC, BPSRV_NAME,
               "BeepSrvErrEltHdlr : ENTRY \r\n");
    MEMSET (ai1SysLogMsg, BEEP_ZERO, BPSRV_MAX_SYSLOG_MSG_LEN);
    SPRINTF ((CHR1 *) ai1SysLogMsg,
             "Beep Server: Error code %d received from client ",
             pFrameData->i4Code);

    /* Check whether any error string is received */
    STRCAT (ai1SysLogMsg, pFrameData->au1ErrStr);
    STRCAT (ai1SysLogMsg, " and Session is closed");
    SYS_LOG_MSG ((BEEP_ERROR, BPSRV_ID, (CHR1 *) ai1SysLogMsg));
    BPSRV_ACTIVE_SESSION->i1SsnEvtFlag = BPSRV_UNEXPECTED;
    BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_DATA_PATH_TRC, BPSRV_NAME,
               "BeepSrvErrEltHdlr : EXIT \r\n");
    return BPSRV_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : BeepSrvProfileEltHdlr                              */
/*   Description     : This function handles when a profile element is    */
/*                     received from the Beep Client                      */
/*   Input(s)        : pFrameData -  Frame Data                           */
/*   Output(s)       : None                                               */
/*   Return Value    : BPSRV_SUCCESS / BPSRV_FAILURE                      */
/**************************************************************************/
INT4
BeepSrvProfileEltHdlr (tBeepSrvChMgmtData * pFrameData)
{
    UNUSED_PARAM (pFrameData);
    BPSRV_ACTIVE_SESSION->i1SsnEvtFlag = BPSRV_UNEXPECTED;
    return BPSRV_SUCCESS;
}
