/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bpsrvraw.c,v 1.9 2014/01/25 13:55:33 siva Exp $
 *
 * Description: Routines for HTTP Server Module
 *
 ********************************************************************/

/********************************************************************
 * Description:This file contains the Functions related to RAW profile
 *             used by syslog protocol
 *
 ********************************************************************/

#include "bpsrvinc.h"

/**************************************************************************/
/*   Function Name   : BeepSrvRawStart                                    */
/*   Description     : This function starts the RAW channel               */
/*                     Initializes the RAW channel DB                     */
/*   Input(s)        : 1. i4ChNo-  Channel Number                         */
/*                                                                        */
/*   Output(s)       : None                                               */
/*   Return Value    : BPSRV_SUCCESS or BPSRV_FAILURE                     */
/**************************************************************************/
INT4
BeepSrvRawStart (INT4 i4ChNo)
{
    INT1               *pi1Temp = NULL;

    pi1Temp = (INT1 *) MemAllocMemBlk (gBeepSrvGblInfo.BeepSrvBufferMemPoolId);
    if (pi1Temp == NULL)
    {
        return BPSRV_FAILURE;
    }
    MEMSET (pi1Temp, BEEP_ZERO, BPSRV_MAX_BUFF_SIZE);

    /*Allocate the memory for the channel */
    BPSRV_ACTIVE_SESSION->BeepSrvChnl[i4ChNo]
        = (tBeepChnlDb *) MemAllocMemBlk (BPSRV_CHNL_MEMPOOL_ID);
    MEMSET (BPSRV_ACTIVE_SESSION->BeepSrvChnl[i4ChNo],
            BEEP_ZERO, sizeof (tBeepChnlDb));

    /* Initialize the Channel DB */
    BPSRV_ACTIVE_SESSION->BeepSrvChnl[i4ChNo]->i4ChnlNum = i4ChNo;
    BPSRV_ACTIVE_SESSION->BeepSrvChnl[i4ChNo]->i4Profile = RAW_PROFILE;
    BPSRV_ACTIVE_SESSION->BeepSrvChnl[i4ChNo]->i4ProfileState
        = BPSRV_RAW_CH_OPENED;
    BPSRV_ACTIVE_SESSION->BeepSrvChnl[i4ChNo]->i4LastMsg = BEEP_MINUS_ONE;
    BPSRV_ACTIVE_SESSION->BeepSrvChnl[i4ChNo]->i4LastRpy = BEEP_MINUS_ONE;
    BPSRV_ACTIVE_SESSION->BeepSrvChnl[i4ChNo]->u4SeqNoToSend = BEEP_ZERO;
    BPSRV_ACTIVE_SESSION->BeepSrvChnl[i4ChNo]->u4ExpectSeqNo = BEEP_ZERO;
    BPSRV_ACTIVE_SESSION->BeepSrvChnl[i4ChNo]->i4LastAnsNum = BEEP_MINUS_ONE;

    /* Form the Dummy Message */
    STRCPY (pi1Temp,
            "\r\nCentral Services. This has not been a recording.\r\n");
    BeepMakeFrame (BEEP_MSG_FRAME, BPSRV_CURR_CHDB[i4ChNo], pi1Temp);

    /* Send it to the Beep Client */
    if (BPSRV_SUCCESS != BeepSrvSendClient (pi1Temp))
    {
        BPSRV_ACTIVE_SESSION->i1SsnEvtFlag = BPSRV_TCP_ABORT;
        BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                   "RAW Starter: Message Sending failure!");
        MemReleaseMemBlock (gBeepSrvGblInfo.BeepSrvBufferMemPoolId,
                            (UINT1 *) pi1Temp);
        return BPSRV_FAILURE;
    }
    BPSRV_ACTIVE_SESSION->BeepSrvChnl[i4ChNo]->i4ProfileState
        = BPSRV_FIRST_MSG_SENT;
    MemReleaseMemBlock (gBeepSrvGblInfo.BeepSrvBufferMemPoolId,
                        (UINT1 *) pi1Temp);
    return BPSRV_SUCCESS;

}

/**************************************************************************/
/*   Function Name   : BeepSrvRawHdlr                                     */
/*   Description     : This function handles the data what ever comes for */
/*                     channel associated with RAW profile                */
/*   Input(s)        : 1. pFrameHdr - Frame Header Ptr                    */
/*                     2. pi1Frame - The Frame Payload Ptr                */
/*   Output(s)       : None                                               */
/*   Return Value    : BPSRV_SUCCESS or BPSRV_FAILURE                     */
/**************************************************************************/

INT4
BeepSrvRawHdlr (tBeepFrameHdr * pFrameHdr, INT1 *pi1Frame)
{
    INT1               *pi1Msg = NULL;
    tBeepSrvMsgDb      *pMsgDb = NULL;

    pi1Msg = (INT1 *) MemAllocMemBlk (gBeepSrvGblInfo.BeepSrvBufferMemPoolId);
    if (pi1Msg == NULL)
    {
        return BPSRV_FAILURE;
    }
    MEMSET (pi1Msg, BEEP_ZERO, BPSRV_MAX_BUFF_SIZE);
    if ((BPSRV_ACTIVE_SESSION->BeepSrvChnl[pFrameHdr->i4ChnlNum]->
         i4ProfileState == BPSRV_FIRST_MSG_SENT) ||
        (BPSRV_ACTIVE_SESSION->BeepSrvChnl[pFrameHdr->i4ChnlNum]->
         i4ProfileState == BPSRV_ANS_RCVD))
    {
        switch (pFrameHdr->i4FrameHeadType)
        {
            case BEEP_ANS_FRAME:
                BPSRV_ACTIVE_SESSION->BeepSrvChnl[pFrameHdr->i4ChnlNum]->
                    i4ProfileState = BPSRV_ANS_RCVD;
                /* Syslog message is handed over to syslog */
                if (BPSRV_SUCCESS != SyslogEnQue (pi1Frame, STRLEN (pi1Frame)))
                {
                    BPSRV_ACTIVE_SESSION->i1SsnEvtFlag = BPSRV_APP_FAILURE;
                }

                MemReleaseMemBlock (gBeepSrvGblInfo.BeepSrvBufferMemPoolId,
                                    (UINT1 *) pi1Msg);
                return BPSRV_SUCCESS;

            case BEEP_ERR_FRAME:
                SYS_LOG_MSG ((BEEP_NOTICE, BPSRV_ID,
                              "Closing Channel: Transaction Failed"));
                FormCloseMsg (pFrameHdr->i4ChnlNum,
                              BEEP_TRANSACTION_FAIL, (UINT1 *) pi1Msg);
                BeepAddMimeHdr (pi1Msg);
                BeepMakeFrame (BEEP_MSG_FRAME, BPSRV_CURR_CHDB[CH_MGMT],
                               pi1Msg);
                if (BPSRV_FAILURE == BeepSrvSendClient (pi1Msg))
                {
                    BPSRV_ACTIVE_SESSION->i1SsnEvtFlag = BPSRV_TCP_ABORT;
                    MemReleaseMemBlock (gBeepSrvGblInfo.BeepSrvBufferMemPoolId,
                                        (UINT1 *) pi1Msg);
                    return BPSRV_SUCCESS;

                }
                BPSRV_CURR_CHDB[pFrameHdr->i4ChnlNum]->i4ProfileState
                    = BPSRV_CLOSE_SENT;
                pMsgDb =
                    (tBeepSrvMsgDb *) MemAllocMemBlk (BPSRV_MSG_DB_MEMPOOL_ID);
                if (pMsgDb == NULL)
                {
                    BPSRV_ACTIVE_SESSION->i1SsnEvtFlag = BPSRV_UNEXPECTED;
                    MemReleaseMemBlock (gBeepSrvGblInfo.BeepSrvBufferMemPoolId,
                                        (UINT1 *) pi1Msg);
                    return BPSRV_SUCCESS;
                }
                ;
                pMsgDb->i4MsgNo =
                    BPSRV_CURR_CHDB[pFrameHdr->i4ChnlNum]->i4LastMsg;
                pMsgDb->i4Msg = BPSRV_CLOSE_ELT;
                pMsgDb->i4ChNo = pFrameHdr->i4ChnlNum;
                TMO_SLL_Add (&(BPSRV_ACTIVE_SESSION->BeepSrvMsgList),
                             &pMsgDb->Next);
                break;

            case BEEP_NUL_FRAME:

                FormCloseMsg (pFrameHdr->i4ChnlNum, BEEP_SUCCESS_CLOSE,
                              (UINT1 *) pi1Msg);
                BeepAddMimeHdr (pi1Msg);
                BeepMakeFrame (BEEP_MSG_FRAME, BPSRV_CURR_CHDB[CH_MGMT],
                               pi1Msg);
                if (BPSRV_FAILURE == BeepSrvSendClient (pi1Msg))
                {
                    BPSRV_ACTIVE_SESSION->i1SsnEvtFlag = BPSRV_TCP_ABORT;
                    MemReleaseMemBlock (gBeepSrvGblInfo.BeepSrvBufferMemPoolId,
                                        (UINT1 *) pi1Msg);
                    return BPSRV_SUCCESS;
                }
                BPSRV_CURR_CHDB[pFrameHdr->i4ChnlNum]->i4ProfileState
                    = BPSRV_CLOSE_SENT;
                pMsgDb =
                    (tBeepSrvMsgDb *) MemAllocMemBlk (BPSRV_MSG_DB_MEMPOOL_ID);
                if (pMsgDb == NULL)
                {
                    BPSRV_ACTIVE_SESSION->i1SsnEvtFlag = BPSRV_UNEXPECTED;
                    MemReleaseMemBlock (gBeepSrvGblInfo.BeepSrvBufferMemPoolId,
                                        (UINT1 *) pi1Msg);
                    return BPSRV_SUCCESS;
                }
                pMsgDb->i4MsgNo =
                    BPSRV_CURR_CHDB[pFrameHdr->i4ChnlNum]->i4LastMsg;
                pMsgDb->i4Msg = BPSRV_START_ELT;
                pMsgDb->i4ChNo = pFrameHdr->i4ChnlNum;
                TMO_SLL_Add (&(BPSRV_ACTIVE_SESSION->BeepSrvMsgList),
                             &pMsgDb->Next);
                break;

            default:
                SYS_LOG_MSG ((BEEP_NOTICE, BPSRV_ID,
                              "Closing Channel: Invlid XML Received"));
                FormCloseMsg (pFrameHdr->i4ChnlNum, BEEP_INVALID_XML,
                              (UINT1 *) pi1Msg);
                BeepAddMimeHdr (pi1Msg);
                BeepMakeFrame (BEEP_MSG_FRAME, BPSRV_CURR_CHDB[CH_MGMT],
                               pi1Msg);
                if (BPSRV_FAILURE == BeepSrvSendClient (pi1Msg))
                {
                    BPSRV_ACTIVE_SESSION->i1SsnEvtFlag = BPSRV_TCP_ABORT;
                    MemReleaseMemBlock (gBeepSrvGblInfo.BeepSrvBufferMemPoolId,
                                        (UINT1 *) pi1Msg);
                    return BPSRV_SUCCESS;
                }
                BPSRV_CURR_CHDB[pFrameHdr->i4ChnlNum]->i4ProfileState
                    = BPSRV_CLOSE_SENT;
                pMsgDb =
                    (tBeepSrvMsgDb *) MemAllocMemBlk (BPSRV_MSG_DB_MEMPOOL_ID);;
                if (pMsgDb == NULL)
                {
                    BPSRV_ACTIVE_SESSION->i1SsnEvtFlag = BPSRV_UNEXPECTED;
                    MemReleaseMemBlock (gBeepSrvGblInfo.BeepSrvBufferMemPoolId,
                                        (UINT1 *) pi1Msg);
                    return BPSRV_SUCCESS;
                }

                pMsgDb->i4MsgNo =
                    BPSRV_CURR_CHDB[pFrameHdr->i4ChnlNum]->i4LastMsg;
                pMsgDb->i4Msg = BPSRV_START_ELT;
                pMsgDb->i4ChNo = pFrameHdr->i4ChnlNum;
                TMO_SLL_Add (&(BPSRV_ACTIVE_SESSION->BeepSrvMsgList),
                             &pMsgDb->Next);
                break;

        }
    }
    else
    {
        BPSRV_ACTIVE_SESSION->i1SsnEvtFlag = BPSRV_UNEXPECTED;
        MemReleaseMemBlock (gBeepSrvGblInfo.BeepSrvBufferMemPoolId,
                            (UINT1 *) pi1Msg);
        return BPSRV_SUCCESS;

    }
    MemReleaseMemBlock (gBeepSrvGblInfo.BeepSrvBufferMemPoolId,
                        (UINT1 *) pi1Msg);
    return BPSRV_SUCCESS;

}

/**************************************************************************/
/*   Function Name   : SyslogEnQue                                        */
/*   Description     : This function Enques the syslog messages           */
/*                     extracted to the Syslog Queue by a Callback        */
/*                     registered by Syslog                               */
/*   Input(s)        : 1. pi1Frame - Syslog Message                       */
/*                     2. i4Size   - The Length of Msg                    */
/*   Output(s)       : None                                               */
/*   Return Value    : BPSRV_SUCCESS or BPSRV_FAILURE                     */
/**************************************************************************/

INT4
SyslogEnQue (INT1 *pi1Frame, INT4 i4Size)
{
    UNUSED_PARAM (i4Size);
    if (BPSRV_SEND_MSG_TO_APPLN (BPSRV_SYSLOG_ID) != NULL)
    {
        BPSRV_SEND_MSG_TO_APPLN (BPSRV_SYSLOG_ID) ((UINT1 *) pi1Frame);
        return BPSRV_SUCCESS;
    }
    return BPSRV_FAILURE;
}
