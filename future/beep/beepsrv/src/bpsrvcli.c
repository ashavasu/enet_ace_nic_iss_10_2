/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bpsrvcli.c,v 1.4 2011/09/12 06:47:11 siva Exp $
 *
 * Description: This file contains definitions required for
 * the state machine operation in the bfd module.
 *******************************************************************/

/********************************************************************
 *
 * Description:This file contains the Init, Memory allocation, Task
 *             creation, Queue creation for the Beep Server Module   
 *
 *******************************************************************/
#ifndef __BPSRV_CLI_C__
#define __BPSRV_CLI_C__

#include "bpsrvinc.h"
#include "bpsrvcli.h"

/**************************************************************************/
/*   Function Name   : cli_process_bpsrv_cmd                              */
/*   Description     :  This function handles all the beep server         */
/*                      commands typed through cli                        */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : CLI_SUCCESS or CLI_FAILURE                         */
/**************************************************************************/

INT4
cli_process_bpsrv_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{

    va_list             ap;
    UINT4              *args[BPSRV_CLI_MAX_ARGS];
    INT1                argno = BEEP_ZERO;
    INT4                i4RetVal;
    INT4                i4CliState = CLI_FAILURE;
    UINT4               u4ErrCode = BEEP_ZERO;
    INT4                i4PortNum = BEEP_ZERO;

    CliRegisterLock (CliHandle, BeepSrvLock, BeepSrvUnLock);
    BPSRV_LOCK ();
    va_start (ap, u4Command);
    while (BEEP_ONE)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == BPSRV_CLI_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);

    switch (u4Command)
    {
        case CLI_BPSRV_SERVER:

            nmhGetFsBeepServerAdminStatus (&i4RetVal);

            if (i4RetVal != BPSRV_DISABLED)
            {
                u4ErrCode = BEEP_SERVER_ALREADY_ENABLED_ERR;
                i4CliState = CLI_FAILURE;
            }
            else
            {
                nmhSetFsBeepServerAdminStatus (BPSRV_ENABLED);
                i4CliState = CLI_SUCCESS;
            }
            break;

        case CLI_BPSRV_NO_SERVER:

            nmhGetFsBeepServerAdminStatus (&i4RetVal);

            if (i4RetVal == BPSRV_DISABLED)
            {
                u4ErrCode = BEEP_SERVER_ALREADY_DISABLED_ERR;
                i4CliState = CLI_FAILURE;
            }
            else
            {
                nmhSetFsBeepServerAdminStatus (BPSRV_DISABLED);
                i4CliState = CLI_SUCCESS;
            }

            break;

        case CLI_BPSRV_PORT:

            /*i4PortNum = CLI_PTR_TO_I4(args[BEEP_ONE]); */

            i4PortNum = *((INT4 *) args[BEEP_ONE]);
            if (SNMP_FAILURE ==
                nmhTestv2FsBeepServerIpv4PortNum (&u4ErrCode, i4PortNum))
            {
                u4ErrCode = BPSRV_INVALID_PORT_ERR;
                i4CliState = CLI_FAILURE;
            }
            else
            {
                nmhSetFsBeepServerIpv4PortNum (i4PortNum);
                i4CliState = CLI_SUCCESS;
            }

            break;

        case CLI_BPSRV_6PORT:

            i4PortNum = *((INT4 *) args[BEEP_ONE]);

            if (SNMP_FAILURE ==
                nmhTestv2FsBeepServerIpv6PortNum (&u4ErrCode, i4PortNum))
            {
                u4ErrCode = BPSRV_INVALID_PORT_ERR;
                i4CliState = CLI_FAILURE;
            }
            else
            {
                nmhSetFsBeepServerIpv6PortNum (i4PortNum);
                i4CliState = CLI_SUCCESS;
            }

            break;

        case CLI_BPSRV_SHOW_CONF:

            if (BPSRV_FAILURE == BeepShowSrvConf (CliHandle))
            {
                u4ErrCode = BPSRV_SHOW_FAIL_ERR;
                i4CliState = CLI_FAILURE;
            }
            else
            {
                i4CliState = CLI_SUCCESS;
            }

            break;

        default:
            u4ErrCode = BPSRV_INVALID_CMD_ERR;
            i4CliState = CLI_FAILURE;
            break;
    }

    if ((u4ErrCode > BEEP_ZERO) && (u4ErrCode < CLI_BPSRV_MAX_ERR))
    {
        CliPrintf (CliHandle, "%s", gaBeepSrvCliErrString[u4ErrCode]);
    }

    BPSRV_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return i4CliState;
}

/**************************************************************************/
/*   Function Name   : BeepShowSrvConf                                    */
/*   Description     :  This function displays the current configuration  */
/*                      the Beep Server                                   */
/*   Input(s)        : 1. CliHandle - CLI context ID                      */
/*   Output(s)       : None                                               */
/*   Return Value    : BPSRV_SUCCESS or BPSRV_FAILURE                     */
/**************************************************************************/

INT4
BeepShowSrvConf (tCliHandle CliHandle)
{
    INT4                i4BpSrvState = BEEP_ZERO;
/*  tSNMP_OCTET_STRING_TYPE BpSrvMd5AuthKey; 
    UINT1 au1AuthKey[BPSRV_MAX_AUTH_KEY_LENGTH + 1];
    INT4 i4TlsReq=BEEP_ZERO;
    INT4 i4Md5Req=BEEP_ZERO; */
    INT4                i4RawReq = BEEP_ZERO;
    INT4                i4Ipv4Port = BEEP_ZERO;
    INT4                i4Ipv6Port = BEEP_ZERO;

    /* Memset for tSNMP_OCTET_STRING_TYPE variable */

/*  MEMSET (au1AuthKey, BEEP_ZERO,BPSRV_MAX_AUTH_KEY_LENGTH + BEEP_ONE);
    MEMSET (&BpSrvMd5AuthKey, BEEP_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    BpSrvMd5AuthKey.i4_Length = BEEP_ZERO;*/

    /* Gettiing all the MIB values */
/*
    nmhGetFsBeepServerAuthType(&i4Md5Req);
    nmhGetFsBeepServerEncryptionType(&i4TlsReq); */
    nmhGetFsBeepServerIpv4PortNum (&i4Ipv4Port);
    nmhGetFsBeepServerIpv6PortNum (&i4Ipv6Port);
    nmhGetFsBeepServerRawProfile (&i4RawReq);
    nmhGetFsBeepServerAdminStatus (&i4BpSrvState);
/*  nmhGetFsBeepServerMd5AuthKey(&BpSrvMd5AuthKey);*/

    CliPrintf (CliHandle, "\r *** Beep Server Configuration *** \r\n\n");

    if (i4BpSrvState == BPSRV_ENABLED)
    {
        CliPrintf (CliHandle, "\r Beep Server       : Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r Beep Server      : Halted\r\n");
    }

    if (i4RawReq == BPSRV_ENABLED)
    {
        CliPrintf (CliHandle, "\r Raw Profile       : Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r Raw Profile      : Disabled\r\n");
    }

    if (i4Ipv4Port != BEEP_ZERO)
    {
        CliPrintf (CliHandle, "\r Beep Server ipv4 Port : %d\r\n", i4Ipv4Port);
    }

    if (i4Ipv6Port != BEEP_ZERO)
    {
        CliPrintf (CliHandle, "\r Beep Server ipv6 Port : %d\r\n", i4Ipv6Port);
    }

/*    if (BpSrvMd5AuthKey.i4_Length != 0)
    {
         CliPrintf(CliHandle,"\r SERVER MD5 AUTH_KEY : ******\r\n");
    }*/

    if ((i4BpSrvState) && (i4RawReq) && (i4Ipv4Port) && (i4Ipv6Port))
    {
        return BPSRV_SUCCESS;
    }
    else
    {
        return BPSRV_FAILURE;
    }

}

/**************************************************************************/
/*   Function Name   :  BeepSrvShowRunningConfig                          */
/*   Description     :  This function executes the show running config    */
/*                      for beep server                                   */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : CLI_SUCCESS or CLI_FAILURE                         */
/**************************************************************************/
INT4
BeepSrvShowRunningConfig (tCliHandle CliHandle)
{

    INT4                i4BpSrvState = BEEP_ZERO;
    INT4                i4Ipv4Port = BEEP_ZERO;
    INT4                i4Ipv6Port = BEEP_ZERO;

    nmhGetFsBeepServerAdminStatus (&i4BpSrvState);

    if (i4BpSrvState == BPSRV_ENABLED)
    {
        CliPrintf (CliHandle, "beep server active \r\n");
    }

    nmhGetFsBeepServerIpv4PortNum (&i4Ipv4Port);
    if (i4Ipv4Port != BPSRV_DEFAULT_PORT)
    {
        CliPrintf (CliHandle, "beep server ipv4-port %d\r\n", i4Ipv4Port);
    }

    nmhGetFsBeepServerIpv6PortNum (&i4Ipv6Port);
    if (i4Ipv6Port != BPSRV_DEFAULT_PORT)
    {
        CliPrintf (CliHandle, "beep server ipv6-port %d\r\n", i4Ipv6Port);
    }
    return CLI_SUCCESS;

}

#endif
