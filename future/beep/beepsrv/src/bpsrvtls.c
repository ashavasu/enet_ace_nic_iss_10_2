/********************************************************************
 *
 * Description:This file contains the functions needed for
 *             establishing, Sending, Receiving and closing of TLS
 *
 *******************************************************************/

#include "bpsrvinc.h"
#include "httpssl.h"

/******************************************************************************
 * Function           : BeepSrvTlsRecv
 * Input(s)           : pi1ReadBuff - MEssage pointer to store the read msg.
 *                      pBeepSsl- VOID type converted ssl pointer
 *                      pu4Len - Address of length variable
 * Output(s)          : Read string
 * Returns            : BPSRV_SUCCESS/BPSRV_FAILURE
 * Action             : Routine to send the data to the client through
 *                      appropriate socket
 ******************************************************************************/

INT4
BeepSrvTlsRecv (VOID *pBeepSsl, INT1 *pi1ReadBuf, INT4 *pi4Len)
{
#ifdef SSL_WANTED
    if (BEEP_FAILURE ==
        SslArRead (pBeepSsl, (UINT1 *) pi1ReadBuf, (UINT4 *) pi4Len))
    {
        return BPSRV_FAILURE;
    }
#else
    UNUSED_PARAM (pBeepSsl);
    UNUSED_PARAM (pi1ReadBuf);
    UNUSED_PARAM (pi4Len);
#endif
    return BPSRV_SUCCESS;
}

/******************************************************************************
 * Function           : BeepSrvTlsSend
 * Input(s)           : pi1String - MEssage to be sent.
 * Output(s)          : None.
 * Returns            : BPSRV_SUCCESS/BPSRV_FAILURE
 * Action             : Routine to send the data to the client through
 *                      appropriate SSL connection
 ******************************************************************************/

INT4
BeepSrvTlsSend (INT1 *pi1String)
{
    INT1               *pi1Buffer = NULL;
    INT4                i4Size = BEEP_ZERO;
    INT4                i4Written = BEEP_ZERO;
    INT4                i4WriteLen = BEEP_ZERO;

    i4Size = STRLEN (pi1String);
    pi1Buffer = pi1String;
    while (i4Size > BPSRV_MAX_SOC_WRITE_LEN)
    {
        i4WriteLen = BPSRV_MAX_SOC_WRITE_LEN;
#ifdef SSL_WANTED
        if (SslArWrite
            (BPSRV_ACTIVE_SESSION->pBeepSsl, (UINT1 *) pi1Buffer,
             (UINT4 *) &i4WriteLen) < BEEP_ZERO)
        {
            return BPSRV_FAILURE;
        }
#endif
        pi1Buffer += i4WriteLen;
        i4Size -= i4WriteLen;
        i4Written += i4WriteLen;
    }

    while (i4Size > BEEP_ZERO)
    {
        i4WriteLen = i4Size;
#ifdef SSL_WANTED
        if (SslArWrite
            (BPSRV_ACTIVE_SESSION->pBeepSsl, (UINT1 *) pi1Buffer,
             (UINT4 *) &i4WriteLen) < BEEP_ZERO)
        {
            return BPSRV_FAILURE;
        }
#endif
        pi1Buffer += i4WriteLen;
        i4Size -= i4WriteLen;
        i4Written += i4WriteLen;
    }

    return BPSRV_SUCCESS;

}

/******************************************************************************
 * Function           : BeepSrvHandleTlsOk
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : BPSRV_SUCCESS/BPSRV_FAILURE
 * Action             : Routine to establish TLS negotiation and start 
 *                      TLS profile
 ******************************************************************************/

INT4
BeepSrvHandleTlsOk (VOID)
{
    INT4                i4Count = BEEP_ZERO;

    for (i4Count = BEEP_ZERO; i4Count < BPSRV_MAX_CHANNELS; i4Count++)
    {
        if (BPSRV_CURR_CHDB[i4Count] != NULL)
        {
            MemReleaseMemBlock (BPSRV_CHNL_MEMPOOL_ID,
                                (UINT1 *) BPSRV_CURR_CHDB[i4Count]);
        }
    }
    BeepSrvInitSsnDb (BPSRV_ACTIVE_SESSION);
#ifdef SSL_WANTED
    BPSRV_ACTIVE_SESSION->pBeepSsl = SslArAccept (BPSRV_ACTIVE_SOCK);
#endif
    if (BPSRV_ACTIVE_SESSION->pBeepSsl != NULL)
    {
        BPSRV_ACTIVE_SESSION->i1TlsFlag = BPSRV_FLG_ACTIVE;
    }
    else
    {
        SYS_LOG_MSG ((BEEP_CRITICAL, BPSRV_ID,
                      "Cannot Initialize TLS session"));
    }

    if (BPSRV_SUCCESS == BeepSrvSendGreeting ())
    {
        BeepSrvSelAddFd (BPSRV_ACTIVE_SOCK, BeepSrvPktRcvd);
        BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_CONTROL_PATH_TRC, BPSRV_NAME,
                   "New SSL connection added \r\n");
    }
    else
    {
        BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                   "Greetings send Failed!\r\n");
        BeepSrvForceCloseSsn (BPSRV_ACTIVE_SOCK);
        return BPSRV_SUCCESS;
    }
    return BPSRV_SUCCESS;

}

/**************************************************************************/
/*   Function Name   : BeepSrvCloseTlsSsn                                 */
/*   Description     : This Function closes the Tls session               */
/*   Input(s)        : VOID pointer                                       */
/*   Output(s)       : None                                               */
/*   Return Value    : BPSRV_SUCCESS or BPSRV_FAILURE                     */
/**************************************************************************/

VOID
BeepSrvCloseTlsSsn (VOID *pBeepSsl)
{
#ifdef SSL_WANTED
    SslArClose (pBeepSsl);
#else
    UNUSED_PARAM (pBeepSsl);
#endif
    return;
}
