/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * Description: Header file contains channel management parser
 *              for beep server
 *
 *******************************************************************/

#include "bpsrvinc.h"

/******************************************************************************
 * Function           : BeepSrvParseChMgmtData
 * Input(s)           : pi1Frame - Frame Payload
 * Output(s)          : pFrameData - Data Structure with payload details
 * Returns            : BPSRV_SUCCESS/BPSRV_FAILURE
 * Action             : Routine to parse the frame paylod and extract details
 *                      in the payload received
 ******************************************************************************/

INT4
BeepSrvParseChMgmtData (INT1 *pi1Frame, tBeepSrvChMgmtData * pFrameData)
{

    if (RmvMimeHdr (pi1Frame) == BPSRV_SUCCESS)
    {
        MvSpcAndLines (pi1Frame);
    }
    else
    {
        return BPSRV_FAILURE;
    }

    switch (FindKeyEltType (pi1Frame))
    {
        case BPSRV_ERR_ELT:
            pFrameData->i4KeyElementType = BPSRV_ERR_ELT;
            FillErrDetails (pi1Frame, pFrameData);
            break;

        case BPSRV_GREETING_ELT:
            pFrameData->i4KeyElementType = BPSRV_GREETING_ELT;
            FillGrtDetails (pi1Frame, pFrameData);
            break;

        case BPSRV_PROFILE_ELT:
            pFrameData->i4KeyElementType = BPSRV_PROFILE_ELT;
            FillProfDetails (pi1Frame, pFrameData);
            break;

        case BPSRV_START_ELT:
            pFrameData->i4KeyElementType = BPSRV_START_ELT;
            FillStartDetails (pi1Frame, pFrameData);
            break;

        case BPSRV_CLOSE_ELT:
            pFrameData->i4KeyElementType = BPSRV_CLOSE_ELT;
            FillCloseDetails (pi1Frame, pFrameData);
            break;

        case BPSRV_OK_ELT:
            pFrameData->i4KeyElementType = BPSRV_OK_ELT;
            break;

        default:
            return BPSRV_FAILURE;
    }

    return BPSRV_SUCCESS;

}

/******************************************************************************
 * Function           : FindKeyEltType
 * Input(s)           : pi1Frame - Frame Payload
 * Output(s)          : None
 * Returns            : Key Element Type
 * Action             : Routine to parse the frame paylod and extract details
 *                      of key element type or BPSRV_FAILURE
 ******************************************************************************/

INT4
FindKeyEltType (INT1 *pi1Frame)
{
    INT4                i4Size;

    i4Size = STRLEN (pi1Frame);

    if (!(MEMCMP (pi1Frame, "<start ", STRLEN ("<start "))))
    {
        memmove (pi1Frame, pi1Frame + STRLEN ("<start "),
                 i4Size - STRLEN ("<start ") + BEEP_ONE);
        return BPSRV_START_ELT;
    }

    if (!(MEMCMP (pi1Frame, "<close ", STRLEN ("<close "))))
    {
        memmove (pi1Frame, pi1Frame + STRLEN ("<close "),
                 i4Size - STRLEN ("<close ") + BEEP_ONE);
        return BPSRV_CLOSE_ELT;
    }

    if (!(MEMCMP (pi1Frame, "<greeting />", STRLEN ("<greeting />"))))
    {
        memmove (pi1Frame, pi1Frame + STRLEN ("<greeting />"),
                 i4Size - STRLEN ("<greeting />") + BEEP_ONE);
        return BPSRV_GREETING_ELT;
    }

    if (!(MEMCMP (pi1Frame, "<ok />", STRLEN ("<ok />"))))
    {
        memmove (pi1Frame, pi1Frame + STRLEN ("<ok />"),
                 i4Size - STRLEN ("<ok />") + BEEP_ONE);
        return BPSRV_OK_ELT;
    }

    if (!(MEMCMP (pi1Frame, "<error ", STRLEN ("<error "))))
    {
        memmove (pi1Frame, pi1Frame + STRLEN ("<error "),
                 i4Size - STRLEN ("<error ") + BEEP_ONE);
        return BPSRV_ERR_ELT;
    }

    if (!(MEMCMP (pi1Frame, "<profile =", STRLEN ("<profile ="))))
    {
        memmove (pi1Frame, pi1Frame + STRLEN ("<profile "),
                 i4Size - STRLEN ("<profile ") + BEEP_ONE);
        return BPSRV_PROFILE_ELT;
    }

    return BPSRV_FAILURE;

}

/******************************************************************************
 * Function           : MvSpcAndLines
 * Input(s)           : pi1Str - String
 * Output(s)          : None
 * Returns            : BPSRV_SUCCESS/BPSRV_FAILURE
 * Action             : Routine to parse the string and remove the white 
 *                      space and lines at the beginning of the string
 ******************************************************************************/

VOID
MvSpcAndLines (INT1 *pi1Str)
{
    INT4                i4Size = BEEP_ZERO;
    INT4                i4Count = BEEP_ZERO;
    INT4                i4PtrPos = BEEP_ZERO;

    i4Size = STRLEN (pi1Str);

    for (i4Count = BEEP_ZERO; i4Count <= i4Size; i4Count++)
    {
        /* Checking for white spaces and newlines */
        if ((pi1Str[i4Count] == ' ') || (pi1Str[i4Count] == '\t')
            || (pi1Str[i4Count] == '\n') || (pi1Str[i4Count] == '\r'))
        {
            i4PtrPos++;
        }
        else
        {
            break;
        }
    }

    memmove (pi1Str, pi1Str + i4PtrPos, i4Size - i4PtrPos + BEEP_ONE);
    return;
}

/******************************************************************************
 * Function           : FillErrDetails
 * Input(s)           : pi1Str - Frame Payload
 * Output(s)          : pFrameData - Data Structure with payload details
 * Returns            : BPSRV_SUCCESS/BPSRV_FAILURE
 * Action             : Routine to parse the frame paylod n which the key
 *                      element is error and fill the data structure
 ******************************************************************************/

INT4
FillErrDetails (INT1 *pi1Str, tBeepSrvChMgmtData * pFrameData)
{
    INT4                i4Size = BEEP_ZERO;
    INT4                i4Tmp = BEEP_ZERO;
    INT1                ai1Code[MAX_DIGITS];
    INT1                ai1Tmp[BPSRV_TEMP_STR_LEN];
    INT1               *pi1Pos = NULL;
    INT1               *pi1Frame = NULL;
    INT4                i4ErrStrLen = BEEP_ZERO;

    pi1Frame = pi1Str;
    MEMSET (ai1Code, BEEP_ZERO, MAX_DIGITS);
    MEMSET (ai1Tmp, BEEP_ZERO, BPSRV_TEMP_STR_LEN);
    i4Size = STRLEN (pi1Frame);
    if (i4Size < ERR_MIN_LENGTH)
    {
        return BPSRV_FAILURE;
    }

    /* Check if code attribute exists */
    STRCPY (ai1Tmp, "code='");
    i4Tmp = STRLEN (ai1Tmp);
    if ((MEMCMP (pi1Frame, ai1Tmp, i4Tmp)))
    {
        return BPSRV_FAILURE;
    }
    pi1Frame += i4Tmp;
    i4Size = i4Size - i4Tmp;
    MEMCPY (ai1Code, pi1Frame, ERR_CODE_LEN);
    ai1Code[ERR_CODE_LEN] = STR_DLM;
    pi1Frame += ERR_CODE_LEN;

    if (!(((ISDIGIT (ai1Code[BEEP_ONE])) && (ISDIGIT (ai1Code[BEEP_TWO]))
           && (ISDIGIT (ai1Code[BEEP_THREE])))))
    {
        return BPSRV_FAILURE;
    }

    i4Size -= ERR_CODE_LEN;
    pFrameData->i4Code = ATOI (ai1Code);
    if (!MEMCMP (pi1Frame, "'>", BEEP_TWO))
    {
        pi1Frame += BEEP_TWO;
        pi1Pos = (INT1 *) STRSTR (pi1Frame, "</error>");
        if (pi1Pos == NULL)
        {
            return BPSRV_FAILURE;
        }
        i4Size -= BEEP_TWO;
        i4ErrStrLen = pi1Pos - pi1Frame;
        MEMCPY (pFrameData->au1ErrStr, pi1Frame, i4ErrStrLen);
        pFrameData->au1ErrStr[pi1Pos - pi1Frame] = STR_DLM;
        pi1Frame += i4ErrStrLen;
        MvSpcAndLines (pi1Frame);
        if (STRLEN (pi1Frame) != BEEP_ZERO)
        {
            return BPSRV_FAILURE;
        }
    }
    else
    {
        if (!MEMCMP (pi1Frame, " />", BEEP_THREE))
        {
            pi1Frame += BEEP_THREE;
            MvSpcAndLines (pi1Frame);
            if (STRLEN (pi1Frame) != BEEP_ZERO)
            {
                return BPSRV_FAILURE;
            }
        }
        else
        {
            return BPSRV_FAILURE;
        }
    }
    return BPSRV_SUCCESS;

}

/******************************************************************************
 * Function           : FillStartDetails
 * Input(s)           : pi1Str - Frame Payload
 * Output(s)          : pFrameData - Data Structure with payload details
 * Returns            : BPSRV_SUCCESS/BPSRV_FAILURE
 * Action             : Routine to parse the frame paylod in which the key
 *                      element is start and fill the data structure
 ******************************************************************************/

INT4
FillStartDetails (INT1 *pi1Str, tBeepSrvChMgmtData * pFrameData)
{
    INT4                i4Size = BEEP_ZERO;
    INT4                i4Tmp = BEEP_ZERO;
    INT4                i4Cnt = BEEP_ZERO;
    INT1                i1CdataFlg = BPSRV_FLG_NOT_ACTIVE;
    INT1                ai1NumStr[MAX_DIGITS];
    INT1               *pi1Frame = NULL;
    INT1               *pi1Tmp = NULL;

    pi1Frame = pi1Str;
    MEMSET (ai1NumStr, BEEP_ZERO, MAX_DIGITS);

    MvSpcAndLines (pi1Frame);
    pFrameData->i4ProfCount = BEEP_ZERO;
    i4Size = STRLEN (pi1Frame);
    if (MEMCMP (pi1Frame, "number='", STRLEN ("number='")))
    {
        return BPSRV_FAILURE;
    }
    i4Tmp = STRLEN ("number='");
    pi1Frame += i4Tmp;
    i4Size = i4Size - i4Tmp;
    /* Check for Channel number */
    for (i4Cnt = BEEP_ZERO; ISDIGIT (pi1Frame[i4Cnt]); i4Cnt++)
    {
        ai1NumStr[i4Cnt] = pi1Frame[i4Cnt];
    }

    if (i4Cnt == BEEP_ZERO)
    {
        return BPSRV_FAILURE;
    }

    pi1Frame += i4Cnt;
    i4Size = i4Size - i4Cnt;
    ai1NumStr[i4Cnt] = STR_DLM;
    pFrameData->u4Num = ATOI (ai1NumStr);

    if ((INT1 *) STRSTR (pi1Frame, "'>") != pi1Frame)
    {
        return BPSRV_FAILURE;
    }
    i4Size = i4Size - BEEP_TWO;
    pi1Frame += BEEP_TWO;
    MvSpcAndLines (pi1Frame);
    i1CdataFlg = BPSRV_FLG_NOT_ACTIVE;
    /* Check for the profiles available and load the profile names */
    while (!MEMCMP (pi1Frame, "<profile uri=", STRLEN ("<profile uri=")))
    {
        i1CdataFlg = BPSRV_FLG_NOT_ACTIVE;
        pi1Frame += STRLEN ("<profile uri=");
        pi1Tmp = (INT1 *) STRSTR (pi1Frame, " />");
        if (pi1Tmp == NULL)
        {
            pi1Tmp = (INT1 *) STRSTR (pi1Frame, ">");
            if (pi1Tmp == NULL)
            {
                return BPSRV_FAILURE;
            }
            i1CdataFlg = BPSRV_FLG_ACTIVE;

        }
        i4Tmp = pi1Tmp - pi1Frame;
        MEMCPY (pFrameData->ai1Profile[pFrameData->i4ProfCount], pi1Frame,
                i4Tmp);
        pFrameData->ai1Profile[pFrameData->i4ProfCount][i4Tmp] = STR_DLM;
        pFrameData->i4ProfCount++;
        if (i1CdataFlg == BPSRV_FLG_ACTIVE)
        {
            pi1Frame = pi1Frame + i4Tmp + BEEP_ONE;
            MvSpcAndLines (pi1Frame);
            if (!MEMCMP (pi1Frame, "<![CDATA[<ready />]]>",
                         STRLEN ("<![CDATA[<ready />]]>")))
            {
                pi1Frame += STRLEN ("<![CDATA[<ready />]]>");
                MvSpcAndLines (pi1Frame);
                if (!MEMCMP (pi1Frame, "<profile />", STRLEN ("<profile />")))
                {
                    pi1Frame += STRLEN ("<profile />");
                    MvSpcAndLines (pi1Frame);
                    pFrameData->i1CdataRcvd = BPSRV_CDATA_ELT_CODE;
                    pFrameData->Cdata.i4ElementCode = BPSRV_ELT_CODE_READY;
                }
            }

        }
        else
        {
            pi1Frame = pi1Frame + i4Tmp + BEEP_THREE;
        }
        MvSpcAndLines (pi1Frame);

    }

    /* Check for atleast one profile got */
    if (pFrameData->i4ProfCount == BEEP_ZERO)
    {
        return BPSRV_FAILURE;
    }

    if (pi1Frame != (INT1 *) STRSTR (pi1Frame, "</start>"))
    {
        return BPSRV_FAILURE;
    }

    pi1Frame = pi1Frame + STRLEN ("</start>");

    MvSpcAndLines (pi1Frame);
    if (STRLEN (pi1Frame) != BEEP_ZERO)
    {
        return BPSRV_FAILURE;
    }

    return BPSRV_SUCCESS;

}

/******************************************************************************
 * Function           : FillGrtDetails
 * Input(s)           : pi1Str - Frame Payload
 * Output(s)          : None
 * Returns            : BPSRV_SUCCESS/BPSRV_FAILURE
 * Action             : Routine to parse the frame paylod to see
 *                      any element follows greeting from client
 ******************************************************************************/

INT4
FillGrtDetails (INT1 *pi1Str, tBeepSrvChMgmtData * pFrameData)
{
    UNUSED_PARAM (pFrameData);
    MvSpcAndLines (pi1Str);
    if (STRLEN (pi1Str) != BEEP_ZERO)
    {
        return BPSRV_SUCCESS;
    }

    return BPSRV_FAILURE;

}

/******************************************************************************
 * Function           : FillOkDetails
 * Input(s)           : pi1Str - Frame Payload
 * Output(s)          : pFrameData - Data Structure with payload details
 * Returns            : BPSRV_SUCCESS/BPSRV_FAILURE
 * Action             : Routine to parse the frame paylod to see
 *                      any element follows ok
 ******************************************************************************/

INT4
FillOkDetails (INT1 *pi1Str, tBeepSrvChMgmtData * pFrameData)
{
    UNUSED_PARAM (pFrameData);
    MvSpcAndLines (pi1Str);
    if (STRLEN (pi1Str) != BEEP_ZERO)
    {
        return BPSRV_SUCCESS;
    }

    return BPSRV_FAILURE;

}

/******************************************************************************
 * Function           : FillCloseDetails
 * Input(s)           : pi1Str - Frame Payload
 * Output(s)          : pFrameData - Data Structure with payload details
 * Returns            : BPSRV_SUCCESS/BPSRV_FAILURE
 * Action             : Routine to parse the frame paylod in which the key
 *                      element is close and fill the data structure
 ******************************************************************************/

INT4
FillCloseDetails (INT1 *pi1Str, tBeepSrvChMgmtData * pFrameData)
{
    INT4                i4Size = BEEP_ZERO;
    INT4                i4Tmp = BEEP_ZERO;
    INT4                i4Cnt = BEEP_ZERO;
    INT1                ai1NumStr[MAX_DIGITS];
    INT1                ai1Code[MAX_DIGITS];
    INT1               *pi1Frame = NULL;

    pi1Frame = pi1Str;
    MEMSET (ai1NumStr, BEEP_ZERO, MAX_DIGITS);
    MEMSET (ai1Code, BEEP_ZERO, MAX_DIGITS);
    MvSpcAndLines (pi1Frame);
    pFrameData->i4ProfCount = BEEP_ZERO;
    i4Size = STRLEN (pi1Frame);
    if ((INT1 *) STRSTR (pi1Frame, "number='") != pi1Frame)
    {
        return BPSRV_FAILURE;
    }
    i4Tmp = STRLEN ("number='");
    pi1Frame += i4Tmp;
    i4Size = i4Size - i4Tmp;
    /* Check for Channel number */
    for (i4Cnt = BEEP_ZERO; ISDIGIT (pi1Frame[i4Cnt]); i4Cnt++)
    {
        ai1NumStr[i4Cnt] = pi1Frame[i4Cnt];
    }

    if (i4Cnt == BEEP_ZERO)
    {
        return BPSRV_FAILURE;
    }

    pi1Frame += i4Cnt;
    i4Size = i4Size - i4Cnt;
    ai1NumStr[i4Cnt] = STR_DLM;
    pFrameData->u4Num = ATOI (ai1NumStr);

    if ((INT1 *) STRSTR (pi1Frame, " code='") != pi1Frame)
    {
        return BPSRV_FAILURE;
    }
    i4Tmp = STRLEN (" code='");
    pi1Frame += i4Tmp;
    i4Size = i4Size - i4Tmp;
    MEMCPY (ai1Code, pi1Frame, ERR_CODE_LEN);

    /* Check for Channel number */
    if (!((ISDIGIT (ai1Code[BEEP_ONE])) && (ISDIGIT (ai1Code[BEEP_TWO]))
          && (ISDIGIT (ai1Code[BEEP_THREE]))))
    {
        return BPSRV_FAILURE;
    }

    i4Size -= ERR_CODE_LEN;
    pFrameData->i4Code = ATOI (ai1Code);
    pi1Frame += ERR_CODE_LEN;

    if ((INT1 *) STRSTR (pi1Frame, "' />") != pi1Frame)
    {
        return BPSRV_FAILURE;
    }

    pi1Frame += STRLEN ("' />");
    MvSpcAndLines (pi1Frame);
    if (STRLEN (pi1Frame) != BEEP_ZERO)
    {
        return BPSRV_FAILURE;
    }

    return BPSRV_SUCCESS;

}

/******************************************************************************
 *  Function           : RmvMimeHdr
 *  Input(s)           : Frame Payload
 *  Output(s)          : None
 *  Returns            : BPSRV_SUCCESS / BPSRV_FAILURE
 *  Action             : Routine to parse the frame paylod and remove 
 *                       the mime header
 ******************************************************************************/

INT4
RmvMimeHdr (INT1 *pi1Frame)
{
    INT4                i4Len = BEEP_ZERO;
    INT4                i4MimeHdrLen = BEEP_ZERO;

    i4MimeHdrLen = STRLEN (BPSRV_MIME_HDR);
    i4Len = STRLEN (pi1Frame);
    if (!MEMCMP (pi1Frame, BPSRV_MIME_HDR, i4MimeHdrLen))
    {
        i4Len = i4Len - i4MimeHdrLen;
        memmove (pi1Frame, pi1Frame + i4MimeHdrLen, i4Len);
        pi1Frame[i4Len] = STR_DLM;
        return BPSRV_SUCCESS;
    }
    return BPSRV_FAILURE;
}

/******************************************************************************
 * Function           : FillProfDetails
 * Input(s)           : Frame Payload
 * Output(s)          : Data Structure with payload details
 * Returns            : BPSRV_SUCCESS/BPSRV_FAILURE
 * Action             : Routine to parse the frame paylod in which the key
 *                      element is profile and fill the data structure
 ******************************************************************************/

INT4
FillProfDetails (INT1 *pi1Str, tBeepSrvChMgmtData * pFrameData)
{
    UNUSED_PARAM (pi1Str);
    UNUSED_PARAM (pFrameData);
    return BPSRV_FAILURE;

}
