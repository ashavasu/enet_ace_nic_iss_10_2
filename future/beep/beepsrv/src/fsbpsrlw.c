/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsbpsrlw.c,v 1.8 2014/03/16 11:22:48 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "fssnmp.h"
# include  "bpsrvinc.h"
# include "rmgr.h"

extern UINT4        FsBeepServerAdminStatus[11];
extern UINT4        FsBeepServerRawProfile[11];
extern UINT4        FsBeepServerIpv4PortNum[11];
extern UINT4        FsBeepServerIpv6PortNum[11];

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsBeepServerAdminStatus
 Input       :  The Indices

                The Object 
                retValFsBeepServerAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBeepServerAdminStatus (INT4 *pi4RetValFsBeepServerAdminStatus)
{
    *pi4RetValFsBeepServerAdminStatus = gBeepSrvGblInfo.i4BeepSrvState;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsBeepServerRawProfile
 Input       :  The Indices

                The Object 
                retValFsBeepServerRawProfile
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBeepServerRawProfile (INT4 *pi4RetValFsBeepServerRawProfile)
{
    *pi4RetValFsBeepServerRawProfile = gBeepSrvGblInfo.i4BeepSrvRawFlg;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsBeepServerIpv4PortNum
 Input       :  The Indices

                The Object 
                retValFsBeepServerIpv4PortNum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBeepServerIpv4PortNum (INT4 *pi4RetValFsBeepServerIpv4PortNum)
{
    *pi4RetValFsBeepServerIpv4PortNum = gBeepSrvGblInfo.i4BeepSrvPort;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsBeepServerIpv6PortNum
 Input       :  The Indices

                The Object 
                retValFsBeepServerIpv6PortNum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsBeepServerIpv6PortNum (INT4 *pi4RetValFsBeepServerIpv6PortNum)
{
    *pi4RetValFsBeepServerIpv6PortNum = gBeepSrvGblInfo.i4BeepSrv6Port;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsBeepServerAdminStatus
 Input       :  The Indices

                The Object 
                setValFsBeepServerAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsBeepServerAdminStatus (INT4 i4SetValFsBeepServerAdminStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = BEEP_ZERO;

    if (gBeepSrvGblInfo.i4BeepSrvState == i4SetValFsBeepServerAdminStatus)
    {
        return (SNMP_SUCCESS);
    }
    gBeepSrvGblInfo.i4BeepSrvState = i4SetValFsBeepServerAdminStatus;

    if (i4SetValFsBeepServerAdminStatus == BPSRV_DISABLED)
    {
        OsixEvtSend (BPSRV_TASK_ID, BPSRV_TURNED_OFF);
    }
    else if (i4SetValFsBeepServerAdminStatus == BPSRV_ENABLED)
    {
        OsixEvtSend (BPSRV_TASK_ID, BPSRV_TURNED_ON);
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsBeepServerAdminStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (FsBeepServerAdminStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = BEEP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.i2Rsvd = 0;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFsBeepServerAdminStatus));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsBeepServerIpv4PortNum
 Input       :  The Indices

                The Object 
                setValFsBeepServerIpv4PortNum
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsBeepServerIpv4PortNum (INT4 i4SetValFsBeepServerIpv4PortNum)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = BEEP_ZERO;

    if (gBeepSrvGblInfo.i4BeepSrvPort != i4SetValFsBeepServerIpv4PortNum)
    {
        gBeepSrvGblInfo.i4BeepSrvPort = i4SetValFsBeepServerIpv4PortNum;
        OsixEvtSend (BPSRV_TASK_ID, BPSRV_CONF_CHANGE_EVENT);

        RM_GET_SEQ_NUM (&u4SeqNum);
        SnmpNotifyInfo.pu4ObjectId = FsBeepServerIpv4PortNum;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsBeepServerIpv4PortNum) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = NULL;
        SnmpNotifyInfo.pUnLockPointer = NULL;
        SnmpNotifyInfo.u4Indices = BEEP_ZERO;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SnmpNotifyInfo.i2Rsvd = 0;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                          i4SetValFsBeepServerIpv4PortNum));
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsBeepServerIpv6PortNum
 Input       :  The Indices

                The Object 
                setValFsBeepServerIpv6PortNum
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsBeepServerIpv6PortNum (INT4 i4SetValFsBeepServerIpv6PortNum)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = BEEP_ZERO;

    if (gBeepSrvGblInfo.i4BeepSrv6Port != i4SetValFsBeepServerIpv6PortNum)
    {
        gBeepSrvGblInfo.i4BeepSrv6Port = i4SetValFsBeepServerIpv6PortNum;
        OsixEvtSend (BPSRV_TASK_ID, BPSRV_CONF_CHANGE_EVENT);

        RM_GET_SEQ_NUM (&u4SeqNum);
        SnmpNotifyInfo.pu4ObjectId = FsBeepServerIpv6PortNum;
        SnmpNotifyInfo.u4OidLen =
            sizeof (FsBeepServerIpv6PortNum) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = u4SeqNum;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = NULL;
        SnmpNotifyInfo.pUnLockPointer = NULL;
        SnmpNotifyInfo.u4Indices = BEEP_ZERO;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SnmpNotifyInfo.i2Rsvd = 0;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                          i4SetValFsBeepServerIpv6PortNum));
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsBeepServerAdminStatus
 Input       :  The Indices

                The Object 
                testValFsBeepServerAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsBeepServerAdminStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFsBeepServerAdminStatus)
{
    if ((i4TestValFsBeepServerAdminStatus != BPSRV_ENABLED) &&
        (i4TestValFsBeepServerAdminStatus != BPSRV_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsBeepServerIpv4PortNum
 Input       :  The Indices

                The Object 
                testValFsBeepServerIpv4PortNum
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsBeepServerIpv4PortNum (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFsBeepServerIpv4PortNum)
{
    if ((i4TestValFsBeepServerIpv4PortNum > BPSRV_MAX_PORT) ||
        (i4TestValFsBeepServerIpv4PortNum < BPSRV_MIN_PORT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsBeepServerIpv6PortNum
 Input       :  The Indices

                The Object 
                testValFsBeepServerIpv6PortNum
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsBeepServerIpv6PortNum (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFsBeepServerIpv6PortNum)
{
    if ((i4TestValFsBeepServerIpv6PortNum > BPSRV_MAX_PORT) ||
        (i4TestValFsBeepServerIpv6PortNum < BPSRV_MIN_PORT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsBeepServerAdminStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsBeepServerAdminStatus (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsBeepServerIpv4PortNum
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsBeepServerIpv4PortNum (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsBeepServerIpv6PortNum
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsBeepServerIpv6PortNum (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
