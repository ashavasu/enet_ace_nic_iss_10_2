/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bpsrvssn.c,v 1.10 2014/03/03 12:13:01 siva Exp $
 * 
 * Description: This file contains the Init, Memory allocation, Task
 *              creation, Queue creation for the Beep Server Module
 *
 ********************************************************************/

#include "bpsrvinc.h"

/**************************************************************************/
/*   Function Name   : BeepSrvSsnHdlr                                     */
/*   Description     : This function takes and reads the active session   */
/*                     socket and parse and pass the data to profile hdlrs*/
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : BPSRV_SUCCESS or BPSRV_FAILURE                     */
/**************************************************************************/

INT4
BeepSrvSsnHdlr (VOID)
{
    tBeepFrameHdr       FrameHdr;

    BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_DATA_PATH_TRC, BPSRV_NAME,
               "Entry of Session Handler \r\n");
    /* Check whether Session DB is available */

    if (BPSRV_ACTIVE_SESSION == NULL)
    {
        BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                   "Session Handler: Session Not Available  \r\n");
        return BPSRV_FAILURE;
    }

    /* Receive frame from the socket */
    if (BPSRV_SUCCESS != BeepSrvRcvSock ())
    {
        BeepSrvForceCloseSsn (BPSRV_ACTIVE_SOCK);
        return BPSRV_SUCCESS;
    }

    /* Checking whether full frame is received */
    if (BPSRV_ACTIVE_SESSION->RxFrame.i1FullFlag != BPSRV_FULL_FRAME)
    {
        BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_DATA_PATH_TRC, BPSRV_NAME,
                   " Partial Frame Received:");
        return BPSRV_SUCCESS;
    }
    else
    {
        MEMSET (BPSRV_ACTIVE_SESSION->ai1ReadBuff, BEEP_ZERO,
                BPSRV_MAX_FRAME_SIZE);
        MEMCPY (BPSRV_ACTIVE_SESSION->ai1ReadBuff,
                BPSRV_ACTIVE_SESSION->RxFrame.ai1Frame,
                BPSRV_ACTIVE_SESSION->RxFrame.i4Size);
        BPSRV_ACTIVE_SESSION->
            ai1ReadBuff[BPSRV_ACTIVE_SESSION->RxFrame.i4Size] = STR_DLM;
    }

    MEMSET (&FrameHdr, BEEP_ZERO, sizeof (tBeepFrameHdr));

    /* Parse the Frame Header */
    if (BPSRV_FAILURE ==
        BeepParseFrameHdr (BPSRV_ACTIVE_SESSION->ai1ReadBuff, &FrameHdr))
    {
        BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                   "Poorly Formed Frame Received:Closing Session !!");
        BeepSrvForceCloseSsn (BPSRV_ACTIVE_SOCK);
        return BPSRV_SUCCESS;
    }

    if (BPSRV_FAILURE == BeepSrvValidateFrame (&FrameHdr))
    {
        BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                   "Invalid Frame Received :Closing Session !!");
        BeepSrvForceCloseSsn (BPSRV_ACTIVE_SOCK);
        return BPSRV_SUCCESS;
    }

    /* Call the Coprresponding Profile Handlers. When new
     * applications needs to use BEEP server, add more cases
     * and their corresponding Profile handler calls here*/

    switch (BPSRV_CURR_CHDB[FrameHdr.i4ChnlNum]->i4Profile)
    {
        case CH_MGMT:
            BeepSrvChMgmtHdlr (&FrameHdr, BPSRV_ACTIVE_SESSION->ai1ReadBuff);
            break;

        case RAW_PROFILE:
            BeepSrvRawHdlr (&FrameHdr, BPSRV_ACTIVE_SESSION->ai1ReadBuff);
            break;

        case DIGEST_MD5_PROFILE:
            BeepSrvDigestMd5Hdlr (&FrameHdr, BPSRV_ACTIVE_SESSION->ai1ReadBuff);
            break;

        default:
            break;
    }

    /* Free the Frame Header memory */
    BPSRV_ACTIVE_SESSION->RxFrame.i1FullFlag = BPSRV_PARTIAL_FRAME;
    MEMSET (BPSRV_ACTIVE_SESSION->RxFrame.ai1Frame,
            BEEP_ZERO, BPSRV_MAX_FRAME_SIZE);
    BPSRV_ACTIVE_SESSION->RxFrame.i4Size = BEEP_ZERO;

    /* Switch on Event variable */
    switch (BPSRV_ACTIVE_SESSION->i1SsnEvtFlag)
    {
        case BPSRV_TCP_ABORT:
            BeepSrvForceCloseSsn (BPSRV_ACTIVE_SOCK);
            return BPSRV_SUCCESS;

        case BPSRV_APP_FAILURE:
            BeepSrvForceCloseSsn (BPSRV_ACTIVE_SOCK);
            return BPSRV_SUCCESS;

        case BPSRV_DIGEST_MD5_FAIL:
            BeepSrvForceCloseSsn (BPSRV_ACTIVE_SOCK);
            return BPSRV_SUCCESS;

        case BPSRV_UNEXPECTED:
            BeepSrvForceCloseSsn (BPSRV_ACTIVE_SOCK);
            return BPSRV_SUCCESS;

            /* Closing on Authentication fail */
        case BPSRV_TLS_FAIL:
            BeepSrvForceCloseSsn (BPSRV_ACTIVE_SOCK);
            return BPSRV_SUCCESS;

            /* Session is over */
        case BPSRV_OVER:
            BeepSrvForceCloseSsn (BPSRV_ACTIVE_SOCK);
            return BPSRV_SUCCESS;

        case BPSRV_DIGEST_MD5_OK:
            BPSRV_ACTIVE_SESSION->i1Md5Flag = BPSRV_FLG_ACTIVE;
            break;

        case BPSRV_TLS_OK:
            BeepSrvHandleTlsOk ();
            break;

        default:
            break;

    }
    BPSRV_ACTIVE_SESSION->i1SsnEvtFlag = BPSRV_NO_EVENT;
    BeepSrvSelAddFd (BPSRV_ACTIVE_SOCK, BeepSrvPktRcvd);
    return BPSRV_SUCCESS;

}

/**************************************************************************/
/*   Function Name   : BeepSrvForceCloseSsn                               */
/*   Description     : This DeInit Session DB and removes the session     */
/*                     table entry                                        */
/*   Input(s)        : i4Sock - Sock Fd                                   */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/

VOID
BeepSrvForceCloseSsn (INT4 i4Sock)
{

    tBeepSrvSessionDb  *pTmpSsn = NULL;
    if (i4Sock == BEEP_MINUS_ONE)
    {
        return;
    }
    BeepSrvSelRemoveFd (i4Sock);
    BeepSrvCloseSocket (i4Sock);
    if (gBeepSrvGblInfo.i4SessionCount == BEEP_ZERO)
    {
        return;
    }
    gBeepSrvGblInfo.i4SessionCount--;
    if ((pTmpSsn = BeepSrvGetSsnDb (i4Sock)) == NULL)
    {
        BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                   "DeInit Session DB: Session entry not available");
        return;
    }

    BeepSrvDeInitSsnDb (pTmpSsn);

    BeepSrvDeleteSession (pTmpSsn);

    BeepSrvDeAllocSsnDb (pTmpSsn);

    return;

}

/**************************************************************************/
/*   Function Name   : BeepSrvDeInitSsnDb                                 */
/*   Description     : This DeInit Session DB Memory                      */
/*   Input(s)        : TmpSsnDb - Pointer to Corresponding session Node   */
/*   Output(s)       : None                                               */
/*   Return Value    : BPSRV_SUCCESS or BPSRV_FAILURE                     */
/**************************************************************************/

INT4
BeepSrvDeInitSsnDb (tBeepSrvSessionDb * TmpSsnDb)
{
    INT4                i4Count = BEEP_ZERO;
    tBeepSrvMsgDb      *pMsgDb = NULL;

    if (TmpSsnDb == NULL)
    {
        return BPSRV_FAILURE;
    }

    /* Close SSL session if active */
    if (TmpSsnDb->i1TlsFlag == BPSRV_FLG_ACTIVE)
    {
        BeepSrvCloseTlsSsn (TmpSsnDb->pBeepSsl);
    }

    /* Close the tcp socket */
    BeepSrvCloseSocket (TmpSsnDb->i4SockFd);

    /* Release Channel memory */
    for (i4Count = BEEP_ZERO; i4Count < BPSRV_MAX_CHANNELS; i4Count++)
    {
        if (TmpSsnDb->BeepSrvChnl[i4Count] != NULL)
        {
            MemReleaseMemBlock (BPSRV_CHNL_MEMPOOL_ID,
                                (UINT1 *) TmpSsnDb->BeepSrvChnl[i4Count]);
            TmpSsnDb->BeepSrvChnl[i4Count] = NULL;
        }
    }

    /* Delete Message Db list */

    while ((pMsgDb = (tBeepSrvMsgDb *)
            TMO_SLL_First (&(BPSRV_ACTIVE_SESSION->BeepSrvMsgList))) != NULL)
    {
        TMO_SLL_Delete (&(BPSRV_ACTIVE_SESSION->BeepSrvMsgList), &pMsgDb->Next);
        MemReleaseMemBlock (BPSRV_MSG_DB_MEMPOOL_ID, (UINT1 *) pMsgDb);
    }

    return BPSRV_SUCCESS;

}

/**************************************************************************/
/*   Function Name   : BeepSrvGetSsnDb                                    */
/*   Description     : This function gets the session Db ptr              */
/*                     from the session table                             */
/*   Input(s)        : i4Sock -Socket Descriptor                          */
/*                     TmpSsnDb - Empty Ptr to retrieve the value         */
/*   Output(s)       : Ptr to session Db                                  */
/*   Return Value    : BPSRV_SUCCESS or BPSRV_FAILURE                     */
/**************************************************************************/

tBeepSrvSessionDb  *
BeepSrvGetSsnDb (INT4 i4Sock)
{
    tBeepSrvSessionDb  *pTmpSsnDb = NULL;
    tBeepSrvSessionDb  *pTmp = NULL;
    if (i4Sock == BPSRV_INV_SOCK_FD)
    {
        return NULL;
    }

    TMO_SLL_Scan (&(gBeepSrvGblInfo.BeepSrvSsnList), pTmp, tBeepSrvSessionDb *)
    {
        pTmpSsnDb = pTmp;
        if (pTmpSsnDb->i4SockFd == i4Sock)
        {
            return pTmpSsnDb;
        }
    }

    return NULL;

}

/**************************************************************************/
/*   Function Name   : BeepSrvAddSession                                  */
/*   Description     : This functions is used to add the session node in  */
/*                     the Beep server Session list                       */
/*                     Table                                              */
/*   Input(s)        : pSrvNode - Ptr to Session Db                       */
/*   Output(s)       : None                                               */
/*   Return Value    : BPSRV_SUCCESS or BPSRV_FAILURE                     */
/**************************************************************************/

INT4
BeepSrvAddSession (tBeepSrvSessionDb * pSrvNode)
{

    TMO_SLL_Add (&(gBeepSrvGblInfo.BeepSrvSsnList), &pSrvNode->Next);
    return BPSRV_SUCCESS;

}

/**************************************************************************/
/*   Function Name   : BeepSrvDeleteSession                               */
/*   Description     : This functions is used to del the session node in  */
/*                     the Beep server Session list                       */
/*                     Table                                              */
/*                                                                        */
/*   Input(s)        : pSrvNode - Empty pointer                           */
/*   Output(s)       : None                                               */
/*   Return Value    : BPSRV_SUCCESS or BPSRV_FAILURE                     */
/**************************************************************************/

INT4
BeepSrvDeleteSession (tBeepSrvSessionDb * pSrvNode)
{

    TMO_SLL_Delete (&(gBeepSrvGblInfo.BeepSrvSsnList), &pSrvNode->Next);
    return BPSRV_SUCCESS;

}

/**************************************************************************/
/*   Function Name   : BeepSrvAllocSsnDb                                  */
/*   Description     : This functions is used to Allocate memory for the  */
/*                     the Beep server Session Db                         */
/*                                                                        */
/*                                                                        */
/*   Input(s)        : pSrvNode - Ptr to session db                       */
/*   Output(s)       : None                                               */
/*   Return Value    : BPSRV_SUCCESS or BPSRV_FAILURE                     */
/**************************************************************************/

tBeepSrvSessionDb  *
BeepSrvAllocSsnDb (VOID)
{
    tBeepSrvSessionDb  *pSrvNode;

    pSrvNode = (tBeepSrvSessionDb *) MemAllocMemBlk (BPSRV_SESSION_MEMPOOL_ID);
    if (pSrvNode != NULL)
    {
        MEMSET (pSrvNode, BEEP_ZERO, sizeof (tBeepSrvSessionDb));
    }
    return pSrvNode;

}

/**************************************************************************/
/*   Function Name   : BeepSrvDeAllocSsnDb                                */
/*   Description     : This functions is used to release the session      */
/*                     Db memory to mem pool                              */
/*                                                                        */
/*   Input(s)        : pSrvNode - Ptr to Session Db                       */
/*   Output(s)       : None                                               */
/*   Return Value    : BPSRV_SUCCESS or BPSRV_FAILURE                     */
/**************************************************************************/

INT4
BeepSrvDeAllocSsnDb (tBeepSrvSessionDb * pSrvNode)
{
    MemReleaseMemBlock (BPSRV_SESSION_MEMPOOL_ID, (UINT1 *) pSrvNode);
    return BPSRV_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : BeepSrvInitSsnDb                                   */
/*   Description     : This functions is used to Allocate memory for the  */
/*                     the Beep server Session Db                         */
/*   Input(s)        : pSrvNode - Session Node                            */
/*   Output(s)       : None                                               */
/*   Return Value    : BPSRV_SUCCESS or BPSRV_FAILURE                     */
/**************************************************************************/

INT4
BeepSrvInitSsnDb (tBeepSrvSessionDb * pSrvNode)
{
    pSrvNode->i1TlsFlag = BPSRV_FLG_NOT_ACTIVE;
    pSrvNode->i1Md5Flag = BPSRV_FLG_NOT_ACTIVE;
    pSrvNode->i1SsnEvtFlag = BPSRV_NO_EVENT;

    MEMSET (pSrvNode->ai1ReadBuff, BEEP_ZERO, BPSRV_MAX_FRAME_SIZE);
    MEMSET (pSrvNode->ai1WriteBuff, BEEP_ZERO, BPSRV_MAX_FRAME_SIZE);
    MEMSET (pSrvNode->ai1TempBuff, BEEP_ZERO, BPSRV_MAX_FRAME_SIZE);
    pSrvNode->BeepSrvChnl[CH_MGMT] = (tBeepChnlDb *)
        MemAllocMemBlk (BPSRV_CHNL_MEMPOOL_ID);
    pSrvNode->BeepSrvChnl[CH_MGMT]->i4ChnlNum = BEEP_ZERO;
    pSrvNode->BeepSrvChnl[CH_MGMT]->i4Profile = CH_MGMT;
    pSrvNode->BeepSrvChnl[CH_MGMT]->i4ProfileState = BPSRV_CH_OPENED;
    pSrvNode->BeepSrvChnl[CH_MGMT]->i4LastMsg = BEEP_ZERO;
    pSrvNode->BeepSrvChnl[CH_MGMT]->i4LastRpy = BEEP_MINUS_ONE;
    pSrvNode->BeepSrvChnl[CH_MGMT]->u4SeqNoToSend = BEEP_ZERO;
    pSrvNode->BeepSrvChnl[CH_MGMT]->u4ExpectSeqNo = BEEP_ZERO;
    pSrvNode->BeepSrvChnl[CH_MGMT]->i4LastAnsNum = BEEP_MINUS_ONE;
    MEMSET (pSrvNode->RxFrame.ai1Frame, BEEP_ZERO, BPSRV_MAX_FRAME_SIZE);
    TMO_SLL_Init (&pSrvNode->BeepSrvMsgList);
    return BPSRV_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : BeepSrvForceCloseAllSsn                            */
/*   Description     : This DeInit all Session DB and removes the session */
/*                     table entry                                        */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/

VOID
BeepSrvForceCloseAllSsn (VOID)
{
    INT4                i4ChNo = CH_MGMT;
    INT4                i4Code = BEEP_SERVICE_NOT_AVAIL;
    tBeepSrvSessionDb  *pTmpSsn = NULL;
    /* Scanning all the sessions available and close it */
    while ((pTmpSsn = (tBeepSrvSessionDb *)
            TMO_SLL_First (&(gBeepSrvGblInfo.BeepSrvSsnList))) != NULL)
    {
        BPSRV_ACTIVE_SESSION = pTmpSsn;
        MEMSET (pTmpSsn->ai1WriteBuff, BEEP_ZERO,
                sizeof (pTmpSsn->ai1WriteBuff));
        FormCloseMsg (i4ChNo, i4Code, (UINT1 *) pTmpSsn->ai1WriteBuff);
        BeepAddMimeHdr (pTmpSsn->ai1WriteBuff);
        BeepMakeFrame (BEEP_MSG_FRAME, pTmpSsn->BeepSrvChnl[CH_MGMT],
                       pTmpSsn->ai1WriteBuff);
        BeepSrvSendClient (pTmpSsn->ai1WriteBuff);
        BeepSrvForceCloseSsn (pTmpSsn->i4SockFd);
    }
    return;
}

/**************************************************************************/
/*   Function Name   : BeepSrvValidateFrame                               */
/*   Description     : This function validates the information in the     */
/*                     frame headre of the payload                        */
/*   Input(s)        : pFrameHdr - HEader details                         */
/*   Output(s)       : None                                               */
/*   Return Value    : BPSRV_SUCCCESS / BPSRV_FAILURE                     */
/**************************************************************************/

INT4
BeepSrvValidateFrame (tBeepFrameHdr * pFrameHdr)
{
    INT4                i4ChNo = BEEP_ZERO;
    i4ChNo = pFrameHdr->i4ChnlNum;
    /* Checking whether the channel is active */
    if (BPSRV_CURR_CHDB[i4ChNo] == NULL)
    {
        return BPSRV_FAILURE;
    }

    /* Checking all the the parameters in the header */
    /* Verifying the message number */
    if ((pFrameHdr->i4FrameHeadType == BEEP_RPY_FRAME) ||
        (pFrameHdr->i4FrameHeadType == BEEP_ANS_FRAME) ||
        (pFrameHdr->i4FrameHeadType == BEEP_ERR_FRAME))
    {
        if (BPSRV_CURR_CHDB[i4ChNo]->i4LastMsg != pFrameHdr->i4MsgNo)
        {
            return BPSRV_FAILURE;
        }
        BPSRV_CURR_CHDB[i4ChNo]->i4LastRpy = pFrameHdr->i4MsgNo;
    }

    if (pFrameHdr->i4FrameHeadType == BEEP_MSG_FRAME)
    {
        if (BPSRV_CURR_CHDB[i4ChNo]->i4LastMsg >= pFrameHdr->i4MsgNo)
        {
            return BPSRV_FAILURE;
        }
        BPSRV_CURR_CHDB[i4ChNo]->i4LastMsg = pFrameHdr->i4MsgNo;
    }

    if (pFrameHdr->i4FrameHeadType == BEEP_ANS_FRAME)
    {
        if (BPSRV_CURR_CHDB[i4ChNo]->i4LastAnsNum >= pFrameHdr->i4AnsNo)
        {
            return BPSRV_FAILURE;
        }
        BPSRV_CURR_CHDB[i4ChNo]->i4LastAnsNum = pFrameHdr->i4AnsNo;
    }

    /* Verifying the sequence number */
    if (pFrameHdr->u4SeqNo != BPSRV_CURR_CHDB[i4ChNo]->u4ExpectSeqNo)
    {
        return BPSRV_FAILURE;
    }

    BPSRV_CURR_CHDB[i4ChNo]->u4ExpectSeqNo =
        BPSRV_CURR_CHDB[i4ChNo]->u4ExpectSeqNo + pFrameHdr->i4Size;

    return BPSRV_SUCCESS;

}
