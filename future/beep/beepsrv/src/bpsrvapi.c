
#include "bpsrvinc.h"

/*****************************************************************************/
/* Function     : BeepServerRegister                                         */
/*                                                                           */
/* Description  : This function used to register the applications            */
/*                (syslog, dhcp ..) with Beep server  module. Beep server    */
/*                invokes the call back function if any syslog message       */
/*                received from beepclient (syslog device/relay)             */
/*                                                                           */
/* Input        : pu2RegnId - Application Id                                 */
/*                SendToApplication - Callback function                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : BPSRV_SUCCESS/BPSRV_FAILURE                                */
/*****************************************************************************/
INT4
BeepServerRegister (UINT2 u2RegnId, INT4 (*SendToApplication) (UINT1 *pu1Msg))
{
    BPSRV_SEND_MSG_TO_APPLN (u2RegnId) = SendToApplication;
    return BPSRV_SUCCESS;
}

/*****************************************************************************/
/* Function     : BeepServerDeRegister                                       */
/*                                                                           */
/* Description  : This function used to deregister the applications          */
/*                (syslog, dhcp..) with Beep server module                   */
/*                                                                           */
/* Input        : pu2RegnId - Application Id                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : BPSRV_SUCCESS/BPSRV_FAILURE                                */
/*****************************************************************************/
INT4
BeepServerDeRegister (UINT2 u2RegnId)
{
    BPSRV_SEND_MSG_TO_APPLN (u2RegnId) = NULL;
    return BPSRV_SUCCESS;
}
