/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bpsrvsz.c,v 1.3 2013/11/29 11:04:11 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _BEEP_SERVERSZ_C
#include "bpsrvinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
Beep_serverSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < BEEP_SERVER_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsBEEP_SERVERSizingParams[i4SizingId].
                              u4StructSize,
                              FsBEEP_SERVERSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(BEEP_SERVERMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            Beep_serverSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
Beep_serverSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsBEEP_SERVERSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, BEEP_SERVERMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
Beep_serverSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < BEEP_SERVER_MAX_SIZING_ID; i4SizingId++)
    {
        if (BEEP_SERVERMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (BEEP_SERVERMemPoolIds[i4SizingId]);
            BEEP_SERVERMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
