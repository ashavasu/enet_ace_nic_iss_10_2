/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bpsrsock.c,v 1.13 2014/03/16 11:22:48 siva Exp $
 *
 * Description: Header file contains data structures for beep server
 *
 *******************************************************************/
/*******************************************************************
* Description: Beep Server socket interface functions.
*********************************************************************/
#include "bpsrvinc.h"

#include "fssocket.h"

/******************************************************************************
 * Function           : BeepSrvSockInit
 * Input(s)           : 1.u4Port - to bind the socket with ipv4Addr family
 * Output(s)          : TCP server socket descriptor
 * Returns            : BPSRV_SUCCESS/BPSRV_FAILURE
 * Action             : Routine to create a TCP server scoket,set socket options
 *                      for accepting the connections from peer.
 ******************************************************************************/

INT4
BeepSrvSockInit (INT4 i4Port)
{
    UINT2               u2Port = BPSRV_DEFAULT_PORT;
    INT4                i4SockFd = BPSRV_INV_SOCK_FD;
    INT4                i4Flags = BEEP_MINUS_ONE;
    INT4                i4Ret = BPSRV_SUCCESS;
    INT4                i4RetVal = BEEP_ZERO;
    struct sockaddr_in  serv_addr;
    u2Port = (UINT2) i4Port;

    BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_DATA_PATH_TRC, BPSRV_NAME,
               "BeepSrvSockInit: ENTRY \r\n");
    /* Open the tcp socket */
    i4SockFd = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
    /* i4SockFd = socket (AF_INET, SOCK_STREAM, BEEP_ZERO); */
    if (i4SockFd < BEEP_ZERO)
    {
        BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                   "BeepSrvSockInit: TCP server socket creation failure !!!\n");
        i4Ret = BPSRV_FAILURE;
    }
    if (i4Ret != BPSRV_FAILURE)
    {
        MEMSET (&serv_addr, BEEP_ZERO, sizeof (serv_addr));
        serv_addr.sin_family = AF_INET;
        serv_addr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);
        serv_addr.sin_port = OSIX_HTONS (u2Port);

        i4RetVal = bind (i4SockFd, (struct sockaddr *) &serv_addr,
                         sizeof (serv_addr));
        if (i4RetVal < BEEP_ZERO)
        {
            BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                       "BeepSrvSockInit: Unable to bind with TCP server"
                       "address and port for socket\n");
            perror ("BeepSrvSockInit: Unable to bind with TCP server"
                    "address and port for socket \r\n");
            gBeepSrvGblInfo.i4BeepSrvState = BPSRV_DISABLED;
            BeepSrvCloseSocket (i4SockFd);
            i4Ret = BPSRV_FAILURE;
        }
    }
    /* Get current socket flags */
    if (i4Ret != BPSRV_FAILURE)
    {
        if ((i4Flags = fcntl (i4SockFd, F_GETFL, BEEP_ZERO)) < BEEP_ZERO)
        {
            BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                       "BeepSrvSockInit:TCP server Fcntl GET Failure !!!\n");
            BeepSrvCloseSocket (i4SockFd);
            i4Ret = BPSRV_FAILURE;
        }
    }

    if (i4Ret != BPSRV_FAILURE)
    {
        /* Set the socket is non-blocking mode */
        i4Flags |= O_NONBLOCK;
        if (fcntl (i4SockFd, F_SETFL, i4Flags) < BEEP_ZERO)
        {
            BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                       "BeepSrvSockInit: TCP server Fcntl SET Failure !!!\n");
            BeepSrvCloseSocket (i4SockFd);
            i4Ret = BPSRV_FAILURE;
        }
    }
    /* Initializing the port and Binding the socket */
    if (i4Ret != BPSRV_FAILURE)
    {
        i4RetVal = listen (i4SockFd, BPSRV_MAX_LISTEN);
        if (i4RetVal < BEEP_ZERO)
        {
            BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                       "BeepSrvSockInit: Unable to listen for the socket\n");
            BeepSrvCloseSocket (i4SockFd);
            i4Ret = BPSRV_FAILURE;
        }
    }
    if (i4Ret == BPSRV_FAILURE)
    {
        BeepSrvCloseSocket (i4SockFd);
        BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                   "BeepSrvSockInit: Server socket creation failed \r\n");
        return (BPSRV_FAILURE);
    }
    gBeepSrvGblInfo.i4BeepSrvListenSock = i4SockFd;
    /* Add this SockFd to SELECT library */
    BeepSrvSelAddFd (i4SockFd, BeepSrvSockCallBk);
    BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_DATA_PATH_TRC, BPSRV_NAME,
               "BeepSrvSockInit: EXIT \r\n");
    return (BPSRV_SUCCESS);
}

/******************************************************************************
 * Function           : BeepSrvSock6Init
 * Input(s)           : u4Port - Port to bind the socket with ipv6 addr family
 * Output(s)          : TCP server socket descriptor
 * Returns            : BPSRV_SUCCESS/BPSRV_FAILURE
 * Action             : Routine to create a TCP server scoket,set socket options
 *                      for accepting the connections from peer.
 ******************************************************************************/
#ifdef IP6_WANTED
INT4
BeepSrvSock6Init (INT4 u4Port)
{
    INT4                i4SockFd = BPSRV_INV_SOCK_FD;
    INT4                i4Flags = BEEP_MINUS_ONE;
    INT4                i4OptVal = BEEP_FALSE;
    INT4                i4Ret = BPSRV_SUCCESS;
    INT4                i4RetVal = BEEP_ZERO;
    struct sockaddr_in6 serv_addr;

    BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_DATA_PATH_TRC, BPSRV_NAME,
               "BeepSrvSockInit: ENTRY \r\n");
    /* Open the tcp socket with INET6 addr family */
    i4SockFd = socket (AF_INET6, SOCK_STREAM, IPPROTO_TCP);
    if (i4SockFd < BEEP_ZERO)
    {
        BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                   "BeepSrvSockInit: TCP server socket creation failure !!!\n");
        i4Ret = BPSRV_FAILURE;
    }

    if (i4Ret != BPSRV_FAILURE)
    {
        i4OptVal = BEEP_TRUE;
        if (setsockopt (i4SockFd, IPPROTO_IPV6, IPV6_V6ONLY, &i4OptVal,
                        sizeof (i4OptVal)))
        {
            BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                       "BeepSrvSock6Init: Socket option IPV6_V6ONLY"
                       " failure !!!\n");
            BeepSrvCloseSocket (i4SockFd);
            i4Ret = BPSRV_FAILURE;
        }
    }

    /* Get current socket flags */
    if (i4Ret != BPSRV_FAILURE)
    {
        if ((i4Flags = fcntl (i4SockFd, F_GETFL, BEEP_ZERO)) < BEEP_ZERO)
        {
            BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                       "BeepSrvSock6Init: TCP server Fcntl GET Failure !!!\n");
            BeepSrvCloseSocket (i4SockFd);
            i4Ret = BPSRV_FAILURE;
        }
    }

    if (i4Ret != BPSRV_FAILURE)
    {
        /* Set the socket is non-blocking mode */
        i4Flags |= O_NONBLOCK;
        if (fcntl (i4SockFd, F_SETFL, i4Flags) < BEEP_ZERO)
        {
            BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                       "BeepSrvSock6Init: TCP server Fcntl SET Failure !!!\n");
            BeepSrvCloseSocket (i4SockFd);
            i4Ret = BPSRV_FAILURE;
        }
    }

    /* Initializing and binding the port with the socket */
    if (i4Ret != BPSRV_FAILURE)
    {
        MEMSET (&serv_addr, BEEP_ZERO, sizeof (serv_addr));
        serv_addr.sin6_family = AF_INET6;
        /*  serv_addr.sin6_addr.s6_addr = OSIX_HTONL (INADDR_ANY); */
        inet_pton (AF_INET6, (const CHR1 *) "0::0",
                   &serv_addr.sin6_addr.s6_addr);
        serv_addr.sin6_port = OSIX_HTONS (u4Port);

        i4RetVal = bind (i4SockFd, (struct sockaddr *) &serv_addr,
                         sizeof (serv_addr));

        if (i4RetVal < BEEP_ZERO)
        {
            BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                       "BeepSrvSockInit: Unable to bind with TCP server"
                       "address and port for socket\n");
            perror ("BeepSrvSockInit: Unable to bind with TCP server"
                    "address and port for socket \r\n");
            gBeepSrvGblInfo.i4BeepSrvState = BPSRV_DISABLED;

            BeepSrvCloseSocket (i4SockFd);
            i4Ret = BPSRV_FAILURE;
        }
    }
    if (i4Ret != BPSRV_FAILURE)
    {
        i4RetVal = listen (i4SockFd, BPSRV_MAX_LISTEN);
        if (i4RetVal < BEEP_ZERO)
        {
            BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                       "BeepSrvSockInit: Unable to listen for the socket\n");
            BeepSrvCloseSocket (i4SockFd);
            i4Ret = BPSRV_FAILURE;
        }
    }
    if (i4Ret == BPSRV_FAILURE)
    {
        BeepSrvCloseSocket (i4SockFd);
        BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                   "BeepSrvSockInit: Server socket creation failed \r\n");
        return (BPSRV_FAILURE);
    }
    gBeepSrvGblInfo.i4BeepSrvListen6Sock = i4SockFd;
    /* Add this SockFd to SELECT library */
    BeepSrvSelAddFd (i4SockFd, BeepSrvSock6CallBk);
    BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_DATA_PATH_TRC, BPSRV_NAME,
               "BeepSrvSockInit: EXIT \r\n");
    return (BPSRV_SUCCESS);
}
#endif

/******************************************************************************
 * Function           : BeepSrvAcceptNewConnection
 * Input(s)           : i4SrvSockFd - Server socket fd
 * Output(s)          : New connection identifier 
 * Returns            : i4CliSockFd on SUCCESS/ BPSRV_FAILURE on failure
 * Action             : This routine accepts the 
 *                      connection from the TCP peer. 
 ******************************************************************************/

INT4
BeepSrvAcceptNewConnection (INT4 i4SrvSockFd)
{
    INT4                i4Val;
    INT4                i4CliSockFd = BPSRV_INV_SOCK_FD;
    INT4                i4Flags = BEEP_ZERO;
    UINT4               u4CliLen = sizeof (struct sockaddr_in);
    struct sockaddr_in  CliSockAddr;

    BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_DATA_PATH_TRC, BPSRV_NAME,
               "BeepSrvAcceptNewConnection: ENTRY \r\n");
    if (i4SrvSockFd == BPSRV_INV_SOCK_FD)
    {
        BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                   "BeepSrvAcceptNewConnection:Invalid server socket id \r\n");
        return BPSRV_FAILURE;
    }
    MEMSET (&CliSockAddr, BEEP_ZERO, u4CliLen);

    i4CliSockFd = accept (i4SrvSockFd, (struct sockaddr *) &CliSockAddr,
                          &u4CliLen);
    if (i4CliSockFd < BEEP_ZERO)
    {
        BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                   "BeepSrvAcceptNewConnection:Connection accept failed !!!\r\n");
        BeepSrvSelAddFd (i4SrvSockFd, BeepSrvSockCallBk);
        return BPSRV_FAILURE;
    }
    /* Set the socket is non-blocking mode */
    i4Flags |= O_NONBLOCK;
    i4Val = fcntl (i4CliSockFd, F_SETFL, i4Flags);
    if (i4Val < BEEP_ZERO)
    {
        BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                   "BeepSrvAcceptNewConnection: TCP server"
                   " Fcntl SET Failure !!!\r\n");
        BeepSrvSelAddFd (i4SrvSockFd, BeepSrvSockCallBk);
        BeepSrvCloseSocket (i4CliSockFd);
        return BPSRV_FAILURE;
    }
    /* Adding the accepted socket to the select library */
    if (BeepSrvSelAddFd (i4CliSockFd, BeepSrvPktRcvd) == BPSRV_FAILURE)
    {
        BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                   "BeepSrvAcceptNewConnection :"
                   " BeepSrvSelAddFd Failure while adding BPSRV TCP socket\r\n");
        BeepSrvSelAddFd (i4SrvSockFd, BeepSrvSockCallBk);
        BeepSrvCloseSocket (i4CliSockFd);
        return BPSRV_FAILURE;
    }
    return i4CliSockFd;
}

/******************************************************************************
 * Function           : BeepSrvSockCallBk
 * Input(s)           : i4SockFd - socket descriptor.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Callback function registered to SELECT library.
 *                      This function is invoked when a pakcet arrives on
 *                      TCP socket for Ipv4.
 ******************************************************************************/

VOID
BeepSrvSockCallBk (INT4 i4SockFd)
{
    UNUSED_PARAM (i4SockFd);

    BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_CONTROL_PATH_TRC, BPSRV_NAME,
               "BeepSrvSockCallBk: ENTRY \r\n");

    OsixEvtSend (BPSRV_TASK_ID, BPSRV_TCP_NEW_CONN_RCVD);
    BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_CONTROL_PATH_TRC, BPSRV_NAME,
               "BeepSrvSockCallBk: EXIT \r\n");
}

/******************************************************************************
 * Function           : BeepSrvSock6CallBk
 * Input(s)           : i4SockFd - socket descriptor.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Callback function registered to SELECT library.
 *                      This function is invoked when a pakcet arrives on
 *                      TCP socket for Ipv6.
 ******************************************************************************/

VOID
BeepSrvSock6CallBk (INT4 i4SockFd)
{
    UNUSED_PARAM (i4SockFd);

    BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_CONTROL_PATH_TRC, BPSRV_NAME,
               "BeepSrvSock6CallBk: ENTRY \r\n");

    OsixEvtSend (BPSRV_TASK_ID, BPSRV_TCP6_NEW_CONN_RCVD);
    BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_CONTROL_PATH_TRC, BPSRV_NAME,
               "BeepSrvSock6CallBk: EXIT \r\n");
}

/******************************************************************************
 * Function           : BeepSrvPktRcvd
 * Input(s)           : i4SockFd - socket descriptor.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Callback function registered to SELECT library.
 *                      This function is invoked when a pakcet arrives on
 *                      TCP socket.
 ******************************************************************************/

VOID
BeepSrvPktRcvd (INT4 i4SockFd)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tBeepSrvCltMsg     *pCltMsg = NULL;

    BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_DATA_PATH_TRC, BPSRV_NAME,
               "BeepPktRcvd: ENTRY \r\n");
    /* CRU Buff Allocation */
    pBuf = CRU_BUF_Allocate_MsgBufChain ((sizeof (tBeepSrvCltMsg)), BEEP_ZERO);
    if (pBuf == NULL)
    {
        BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_DATA_PATH_TRC, BPSRV_NAME,
                   "CRU Buffer Allocation Failed\n");
        return;
    }
    /* Copying the socket fd to the allocated CRU buff */
    pCltMsg = (tBeepSrvCltMsg *) BPSRV_GET_MODULE_DATA_PTR (pBuf);
    pCltMsg->i4SockId = i4SockFd;

    /* Send the Data to the queue */
    if (OsixQueSend (BPSRV_Q_ID, (UINT1 *) &pBuf,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                   "Send To BEEP server Q Failed\n");
        return;
    }

    BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_DATA_PATH_TRC, BPSRV_NAME,
               "BeepPktRcvd: Message sent to Q  \r\n");
    /* Send Event to the task */
    OsixEvtSend (BPSRV_TASK_ID, BPSRV_PKT_RCVD);

}

/******************************************************************************
 * Function           : BeepSrvSelAddFd
 * Input(s)           : i4SockFd - Socket descriptor.
 *                      pCallBk - Callback function
 * Output(s)          : None.
 * Returns            : BPSRV_SUCCESS/BPSRV_FAILURE
 * Action             : Routine to add the FD in select for reading
 ******************************************************************************/
INT4
BeepSrvSelAddFd (INT4 i4SockFd, VOID (*pCallBk) (INT4))
{
    INT4                i4RetVal = BEEP_ZERO;
    i4RetVal = SelAddFd (i4SockFd, pCallBk);
    if (i4RetVal == OSIX_FAILURE)
    {
        BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                   "!!! select read fd add failed !!!\n");
        return BPSRV_FAILURE;
    }
    return BPSRV_SUCCESS;
}

/******************************************************************************
 * Function           : BeepSrvSendClient
 * Input(s)           : pi1String - MEssage to be sent.
 * Output(s)          : None.
 * Returns            : BPSRV_SUCCESS/BPSRV_FAILURE
 * Action             : Routine to send the data to the client through 
 *                      appropriate socket
 ******************************************************************************/
INT4
BeepSrvSendClient (INT1 *pi1String)
{
    INT4                i4RetVal = BEEP_ZERO;
    INT1               *pi1Buffer = NULL;
    INT4                i4Size = BEEP_ZERO;
    INT4                i4Written = BEEP_ZERO;

    i4Size = STRLEN (pi1String);
    pi1Buffer = pi1String;
    if (!i4Size)
    {
        return BPSRV_SUCCESS;
    }

    /* If Tls is active for the current session, send data via encryption */
    if (BPSRV_ACTIVE_SESSION->i1TlsFlag == BPSRV_FLG_ACTIVE)
    {
        if (BPSRV_SUCCESS != BeepSrvTlsSend (pi1String))
        {
            BPSRV_ACTIVE_SESSION->i1SsnEvtFlag = BPSRV_TLS_FAIL;
            return BPSRV_FAILURE;
        }
        return BPSRV_SUCCESS;
    }
    else
    {
        /* If the size is heavier than the max buff size, split and send */
        while (i4Size > BPSRV_MAX_SOC_WRITE_LEN)
        {
            i4RetVal = send (BPSRV_ACTIVE_SOCK, pi1Buffer,
                             BPSRV_MAX_SOC_WRITE_LEN, BEEP_ZERO);
            if (i4RetVal < BEEP_ZERO)
            {
                /* If there is an interrupt, try sending after a second */
                if (errno == EINTR)
                {
                    OsixTskDelay (BEEP_ONE * SYS_NUM_OF_TIME_UNITS_IN_A_SEC);
                    continue;
                }
                BPSRV_ACTIVE_SESSION->i1SsnEvtFlag = BPSRV_TCP_ABORT;
                return BPSRV_FAILURE;
            }
            pi1Buffer += i4RetVal;
            i4Size -= i4RetVal;
            i4Written += i4RetVal;
        }

        while (i4Size > BEEP_ZERO)
        {
            i4RetVal = send (BPSRV_ACTIVE_SOCK, pi1Buffer, i4Size, BEEP_ZERO);
            if (i4RetVal < BEEP_ZERO)
            {
                if (errno == EINTR)
                {
                    OsixTskDelay (BEEP_ONE * SYS_NUM_OF_TIME_UNITS_IN_A_SEC);
                    continue;
                }
                BPSRV_ACTIVE_SESSION->i1SsnEvtFlag = BPSRV_TCP_ABORT;
                return BPSRV_FAILURE;
            }
            pi1Buffer += i4RetVal;
            i4Size -= i4RetVal;
            i4Written += i4RetVal;
        }

    }
    return BPSRV_SUCCESS;
}

/******************************************************************************
 * Function           : BeepSrvSelRemoveFd
 * Input(s)           : i4SockFd - Socket descriptor.
 * Output(s)          : None.
 * Returns            : BPSRV_SUCCESS/BPSRV_FAILURE
 * Action             : Routine to delete the FD in select for reading
 ******************************************************************************/

INT4
BeepSrvSelRemoveFd (INT4 i4SockFd)
{
    SelRemoveFd (i4SockFd);
    return BPSRV_SUCCESS;
}

/******************************************************************************
 * Function           : BeepSrvRcvSock
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : BPSRV_SUCCESS/BPSRV_FAILURE
 * Action             : Routine to delete the FD in select for reading
 ******************************************************************************/

INT4
BeepSrvRcvSock (VOID)
{
    INT4                i4Ret = BEEP_ZERO;
    INT1               *pi1Sub = NULL;
    INT4                i4Size = BEEP_ZERO;
    INT1               *pi1Temp = NULL;

    pi1Temp = (INT1 *) MemAllocMemBlk (gBeepSrvGblInfo.BeepSrvBufferMemPoolId);
    if (pi1Temp == NULL)
    {
        return BPSRV_FAILURE;
    }
    MEMSET (pi1Temp, BEEP_ZERO, BPSRV_MAX_BUFF_SIZE);

    BPSRV_ACTIVE_SESSION->RxFrame.i1FullFlag = BPSRV_PARTIAL_FRAME;
    BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_DATA_PATH_TRC, BPSRV_NAME,
               "\r\nReceive Frame : FUNCTION ENTRY\r\n");
    if (BPSRV_ACTIVE_SESSION->i1TlsFlag == BPSRV_FLG_ACTIVE)
    {
        if (BPSRV_SUCCESS != BeepSrvTlsRecv (BPSRV_ACTIVE_SESSION->pBeepSsl,
                                             pi1Temp, &i4Size))
        {
            BPSRV_TRC (BPSRV_TRC_FLAG, BPSRV_FAILURE_TRC, BPSRV_NAME,
                       "REceive Frame: TLS rcv failed!\r\n");
            MemReleaseMemBlock (gBeepSrvGblInfo.BeepSrvBufferMemPoolId,
                                (UINT1 *) pi1Temp);
            return BPSRV_FAILURE;
        }
    }
    else
    {
        /* Receive data from the tcp socket */
        i4Ret =
            recv (BPSRV_ACTIVE_SOCK, pi1Temp, BPSRV_MAX_BUFF_SIZE, BEEP_ZERO);
        if (i4Ret == BEEP_MINUS_ONE)
        {
            if (errno != EWOULDBLOCK)
            {
                MemReleaseMemBlock (gBeepSrvGblInfo.BeepSrvBufferMemPoolId,
                                    (UINT1 *) pi1Temp);
                return BPSRV_FAILURE;
            }
        }

        /* Append to temp buff */
        STRNCAT (BPSRV_ACTIVE_SESSION->ai1TempBuff, pi1Temp, STRLEN (pi1Temp));
        BPSRV_ACTIVE_SESSION->
            ai1TempBuff[sizeof (BPSRV_ACTIVE_SESSION->ai1TempBuff) - 1] = '\0';
        if (STRLEN (BPSRV_ACTIVE_SESSION->ai1TempBuff) < BEEP_THREE)
        {
            MemReleaseMemBlock (gBeepSrvGblInfo.BeepSrvBufferMemPoolId,
                                (UINT1 *) pi1Temp);
            return BEEP_SUCCESS;
        }

        /* Check if header received is proper */
        if ((!(MEMCMP (BPSRV_ACTIVE_SESSION->ai1TempBuff, "MSG", BEEP_THREE)))
            ||
            (!(MEMCMP (BPSRV_ACTIVE_SESSION->ai1TempBuff, "RPY", BEEP_THREE)))
            ||
            (!(MEMCMP (BPSRV_ACTIVE_SESSION->ai1TempBuff, "ERR", BEEP_THREE)))
            ||
            (!(MEMCMP (BPSRV_ACTIVE_SESSION->ai1TempBuff, "ANS", BEEP_THREE)))
            ||
            (!(MEMCMP (BPSRV_ACTIVE_SESSION->ai1TempBuff, "NUL", BEEP_THREE)))
            ||
            (!(MEMCMP (BPSRV_ACTIVE_SESSION->ai1TempBuff, "SEQ", BEEP_THREE))))
        {
            if (!
                (MEMCMP (BPSRV_ACTIVE_SESSION->ai1TempBuff, "SEQ", BEEP_THREE)))
            {
                pi1Sub = (INT1 *) STRSTR (BPSRV_ACTIVE_SESSION->ai1TempBuff,
                                          "\r\n");
                if (pi1Sub != NULL)
                {
                    pi1Sub += BEEP_TWO;
                    memmove (BPSRV_ACTIVE_SESSION->ai1TempBuff, pi1Sub,
                             STRLEN (BPSRV_ACTIVE_SESSION->ai1TempBuff) -
                             (pi1Sub - BPSRV_ACTIVE_SESSION->ai1TempBuff) +
                             BEEP_ONE);
                }
                if (STRLEN (BPSRV_ACTIVE_SESSION->ai1TempBuff) != BEEP_ZERO)
                {
                    BeepSrvPktRcvd (BPSRV_ACTIVE_SOCK);
                    MemReleaseMemBlock (gBeepSrvGblInfo.BeepSrvBufferMemPoolId,
                                        (UINT1 *) pi1Temp);
                    return BPSRV_SUCCESS;
                }

            }
        }
        else
        {
            MemReleaseMemBlock (gBeepSrvGblInfo.BeepSrvBufferMemPoolId,
                                (UINT1 *) pi1Temp);
            return BPSRV_FAILURE;
        }

        pi1Sub = (INT1 *) STRSTR (BPSRV_ACTIVE_SESSION->ai1TempBuff, "END\r\n");
        if (pi1Sub != NULL)
        {
            i4Ret = pi1Sub - BPSRV_ACTIVE_SESSION->ai1TempBuff;
            MEMSET (BPSRV_ACTIVE_SESSION->RxFrame.ai1Frame,
                    BEEP_ZERO, BPSRV_MAX_FRAME_SIZE);
            MEMCPY (BPSRV_ACTIVE_SESSION->RxFrame.ai1Frame,
                    BPSRV_ACTIVE_SESSION->ai1TempBuff,
                    i4Ret + STRLEN ("END\r\n"));
            memmove (BPSRV_ACTIVE_SESSION->ai1TempBuff,
                     BPSRV_ACTIVE_SESSION->ai1TempBuff + i4Ret +
                     STRLEN ("END\r\n"),
                     BPSRV_MAX_FRAME_SIZE - (i4Ret + STRLEN ("END\r\n")));
            BPSRV_ACTIVE_SESSION->RxFrame.i1FullFlag = BPSRV_FULL_FRAME;
            BPSRV_ACTIVE_SESSION->RxFrame.i4Size = i4Ret + STRLEN ("END\r\n");

        }

    }
    if (STRLEN (BPSRV_ACTIVE_SESSION->ai1TempBuff) != BEEP_ZERO)
    {
        BeepSrvPktRcvd (BPSRV_ACTIVE_SOCK);
    }

    MemReleaseMemBlock (gBeepSrvGblInfo.BeepSrvBufferMemPoolId,
                        (UINT1 *) pi1Temp);
    /* Add the fd to SELECT Library */
    return BPSRV_SUCCESS;
}

/******************************************************************************
 * Function           : BeepSrvCloseSocket
 * Input(s)           : i4CliSockFd - Sock Fd
 * Output(s)          : None.
 * Returns            : BPSRV_SUCCESS/BPSRV_FAILURE
 * Action             : Routine to Close Tcp connection
 ******************************************************************************/

VOID
BeepSrvCloseSocket (INT4 i4CliSockFd)
{
    close (i4CliSockFd);
    return;
}
