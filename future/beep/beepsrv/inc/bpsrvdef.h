/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bpsrvdef.h,v 1.3 2012/01/13 12:16:46 siva Exp $
 *
 * Description: Header file contains data structures for beep server
 *
 *******************************************************************/
#ifndef _BPSRVDEF_H__
#define _BPSRVDEF_H__

#define BPSRV_INIT_COMPLETE(u4Status) lrInitComplete(u4Status)

#define BPSRV_EVENT_NO_WAIT_FLAG  OSIX_NO_WAIT
#define BPSRV_EVENT_WAIT_FLAG  OSIX_WAIT

#define BPSRV_MODULE  0x29
#define BPSRV_SEND_MSG_TO_APPLN(u2RegnId) \
gBeepSrvGblInfo.aBeepSrvRegTable[u2RegnId].SendToAppln

#define BPSRV_FLG_ACTIVE     1
#define BPSRV_FLG_NOT_ACTIVE 0
#define BPSRV_SUCCESS    1
#define BPSRV_FAILURE    -1
#define BPSRV_ENABLE     1
#define BPSRV_DISABLE    2
#define BPSRV_MAX_LISTEN 5
#define BPSRV_INV_SOCK_FD -1
#define BPSRV_RAW_NAME   BEEP_RAW_NAME
#define BPSRV_TLS_NAME   BEEP_TLS_NAME
#define BPSRV_DIGEST_MD5_NAME BEEP_DIGEST_MD5_NAME
#define BPSRV_NAME   "BEEP_SERVER"

#define BPSRV_NONE                 1
#define BPSRV_AUTH_TYPE_DIGEST_MD5 2
#define BPSRV_ENCR_TYPE_TLS        2

#define BPSRV_DEFAULT_PORT 601
#define BPSRV_MIN_PORT     1
#define BPSRV_MAX_PORT     4096
#define BPSRV_Q       "BPSQ"

/* Semaphore Lock Macros */

#define BPSRV_LOCK() BeepSrvLock() 
#define BPSRV_UNLOCK() BeepSrvUnLock()
/* Global Macros */
#define BPSRV_CHNL_MEMPOOL_ID gBeepSrvGblInfo.BeepSrvChnlMemPoolId
#define BPSRV_SESSION_MEMPOOL_ID gBeepSrvGblInfo.BeepSrvSsnMemPoolId
#define BPSRV_MSG_DB_MEMPOOL_ID  gBeepSrvGblInfo.BeepSrvMsgDbMemPoolId
#define BPSRV_MD5_CHNL_DATA_MEMPOOL_ID gBeepSrvGblInfo.BeepSrvMd5ChnlDataMemPoolId
#define BPSRV_TASK_ID gBeepSrvGblInfo.BeepSrvTaskId
#define BPSRV_ACTIVE_SESSION gBeepSrvGblInfo.BeepSrvActiveSsn
#define BPSRV_ACTIVE_SOCK    gBeepSrvGblInfo.i4BeepSrvActiveSock
#define BPSRV_CURR_CHDB  BPSRV_ACTIVE_SESSION->BeepSrvChnl
#define BPSRV_SEM_NAME   ((UINT1 *) "BPSRSEM" )
#define BPSRV_Q_ID gBeepSrvGblInfo.BeepSrvQId
#define BPSRV_SEM_ID gBeepSrvGblInfo.BeepSrvSemId
#define BPSRV_ID  gBeepSrvGblInfo.i4SyslogId

/* Frame Related */

#define BPSRV_FULL_FRAME     1
#define BPSRV_PARTIAL_FRAME  2

/* Profile and Channel Related */
#define CH_MGMT             0
#define RAW_PROFILE         BEEP_RAW_PROFILE
#define DIGEST_MD5_PROFILE  BEEP_DIGEST_MD5_PROFILE
#define TLS_PROFILE         BEEP_TLS_PROFILE


/* Osix Event Macros */

#define BPSRV_TCP_NEW_CONN_RCVD  0x01
#define BPSRV_PKT_RCVD           0x02
#define BPSRV_CONF_CHANGE_EVENT  0x04
#define BPSRV_TCP6_NEW_CONN_RCVD 0x08
#define BPSRV_TURNED_OFF         0x10
#define BPSRV_TURNED_ON          0x20
/* Event Variable Macros */
#define BPSRV_NO_EVENT        0
#define BPSRV_TCP_ABORT       1
#define BPSRV_APP_FAILURE     2
#define BPSRV_DIGEST_MD5_OK   3
#define BPSRV_DIGEST_MD5_FAIL 4
#define BPSRV_UNEXPECTED      5
#define BPSRV_TLS_OK          6
#define BPSRV_TLS_FAIL        7
#define BPSRV_OVER            8

/* Channnel Management Profile for server - states */

#define BPSRV_CH_OPENED         0
#define BPSRV_GREETINGS_SENT    1
#define BPSRV_GREETINGS_RCVD    2
#define BPSRV_ON_PROCESS        3
#define BPSRV_CLOSE_SENT        99


/* Element types for channel Zero */

#define BPSRV_ERR_ELT      0
#define BPSRV_GREETING_ELT 1
#define BPSRV_PROFILE_ELT  2
#define BPSRV_START_ELT    3
#define BPSRV_CLOSE_ELT    4
#define BPSRV_OK_ELT       5

/* Channel zero components */
#define BPSRV_CDATA_NOT_RCVD 0
#define BPSRV_CDATA_BLOB     1
#define BPSRV_CDATA_ELT_CODE 2

#define BPSRV_ELT_CODE_READY    1
#define BPSRV_ELT_CODE_PROCEED  2

/*Msg Db Message type */
#define START_MSG 1
#define CLOSE_MSG 2


/* Frame Related */
#define BPSRV_MSG_FRAME BEEP_MSG_FRAME
#define BPSRV_RPY_FRAME BEEP_RPY_FRAME
#define BPSRV_ERR_FRAME BEEP_ERR_FRAME
#define BPSRV_ANS_FRAME BEEP_ANS_FRAME
#define BPSRV_NUL_FRAME BEEP_NUL_FRAME
#define BPSRV_SEQ_FRAME BEEP_SEQ_FRAME

#define BPSRV_MAX_APPLNS 10
#define MALLOC(a)  malloc(a)

/* CRU BUFFER RELATED */
#define   BPSRV_GET_MODULE_DATA_PTR(pBuf) \
              &(pBuf->ModuleData)
/* BACK PTR FOR LISTS */
#define MSGDB_OFFSET(x,y) ((UINT4)(&((x *)0)->y))
#define GET_MSGDB(x) ((tBeepSrvMsgDb *)((UINT1 *)x-MSGDB_OFFSET(tBeepSrvMsgDb,Next)))

#define SSNDB_OFFSET(x,y) ((UINT4)(&((x *)0)->y))
#define GET_SSNDB(x) ((tBeepSrvSessionDb *)((UINT1 *)x-SSNDB_OFFSET(tBeepSrvSessionDb,Next)))





#endif
