/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bpsrtdfs.h,v 1.7 2012/03/30 13:17:18 siva Exp $
 *
 * Description: Header file contains data structures for beep server
 *
 *******************************************************************/
#include "bpsrvdef.h"
#include "bpcmn.h"
#include "bpsrvmax.h"

#ifndef _BEEPSRV_TDFS_H_
#define _BEEPSRV_TDFS_H_



typedef struct {
       UINT2  u2RegnId;
       UINT1  u1Pad[BEEP_TWO];
       INT4  (*SendToAppln)( UINT1 *pu1Msg);
}tBpSrvRegnInfo;


typedef struct BeepSrvFrame
{
    INT1 ai1Frame[BPSRV_MAX_FRAME_SIZE];
    INT4 i4Size;
    INT1 i1FullFlag; /* Full Frame - 1   Not FUll- 2 */
    UINT1 u1Pad[BEEP_THREE];
}tBeepSrvFrame;




typedef struct BeepSrvSessionDb /* Session info for the Beep Server */
{
    tTMO_SLL_NODE Next;
    UINT1 au1Md5ChallengeSent[BPSRV_MAX_CHALLENGE_LEN + 1];
    INT1 i1SsnEvtFlag;
    INT1 i1TlsFlag;
    INT1 i1Md5Flag;
    tBeepChnlDb *BeepSrvChnl[BPSRV_MAX_CHANNELS];
    INT4 i4ChOpenNeed;
    INT4 i4SockFd;
    tTMO_SLL      BeepSrvMsgList;
    tBeepSrvFrame RxFrame;
    INT1 ai1ReadBuff[BPSRV_MAX_FRAME_SIZE];
    INT1 ai1WriteBuff[BPSRV_MAX_FRAME_SIZE];
    INT1 ai1TempBuff[BPSRV_MAX_FRAME_SIZE];
    VOID *pBeepSsl;
}tBeepSrvSessionDb;




typedef struct BeepSrvGbl
{
    UINT4 u4GblTrc;
    UINT4 u4GblDbg;
   tBpSrvRegnInfo  aBeepSrvRegTable[BPSRV_MAX_APPLNS]; 
   INT4 i4BeepSrvState;
   INT4 i4SessionCount;
   tOsixQId       BeepSrvQId;
   tOsixTaskId    BeepSrvTaskId;
   tOsixSemId     BeepSrvSemId;
   INT4 i4BeepSrvTlsFlg;/* True means Encryption enabled for the client*/
   INT4 i4BeepSrvMd5Flg; /* True means authentication enabled for the client*/
   INT4 i4BeepSrvRawFlg; /* When it is true, relay is enable.*/
   INT4 i4BeepSrvPort;
   INT4 i4BeepSrv6Port;
   INT4 i4BeepSrvActiveSock;
   tMemPoolId BeepSrvSsnMemPoolId;
   tMemPoolId BeepSrvChnlMemPoolId;
   tMemPoolId BeepSrvMsgDbMemPoolId;
   tMemPoolId BeepSrvMd5ChnlDataMemPoolId;
   tMemPoolId BeepSrvMgmntChnlMemPoolId;
   tMemPoolId BeepSrvBufferMemPoolId;
   tBeepSrvSessionDb *BeepSrvActiveSsn;
   tTMO_SLL BeepSrvSsnList;
   INT4 i4BeepSrvListenSock; /*Tcp sock fd that would listen for 
                               incoming connection */
   INT4 i4BeepSrvListen6Sock; /*Tcp sock fd that would listen for
                               incoming connection */
   UINT1 au1BeepSrvAuthKey[BPSRV_MAX_AUTH_KEY_LENGTH]; /* Variable to store 
                                                         the Server auth key */ 
   INT4 i4SyslogId; /*Syslog Register Id */
}tBeepSrvGbl;



typedef struct BeepSrvMsgDb
{
    tTMO_SLL_NODE Next;
    INT4   i4MsgNo;
    INT4   i4Msg;  /* close /start */
    INT4   i4ChNo;
}tBeepSrvMsgDb;



typedef struct BeepSrvFrameHdr
{
    INT4  i4More;
    INT4  i4ChnlNum;
    UINT4 u4SeqNo;
    INT4  i4MsgNo;
    INT4  i4Size;
    INT4  i4AnsNo;
    INT4  i4FrameHeadType; /* MSG -1 RPY-2 ANS-3 NUL-4 SEQ-5 */
}tBeepSrvFrameHdr;



/* Data Struct for Channel Mnangement */

typedef struct BeepSrvChMgmtData
{
    INT4 i4KeyElementType; /* 0- Err  1 -greeting   2- profile   
                              3- Start   3 - Close    4 - ok  */
    union
    {
         UINT1 au1blob[BEEP_MAX_CHAR + 2];
         INT4 i4ElementCode; /* 1.ready  2- proceed */
    }Cdata;
    INT4 i4ProfCount;
    UINT1 ai1Profile[ BEEP_MAX_PROFILE ][BEEP_MAX_CHAR];
    INT1 i1CdataRcvd; /* 0 not rcvd : 1 blob : 2  Code */
    INT1 i1Pad;
    UINT4 u4Num;
    INT4 i4Code;
    UINT1 au1ErrStr[BEEP_MAX_CHAR];
    INT1 ai1Pad[2];

}tBeepSrvChMgmtData;
 

typedef struct BeepSrvMd5Data
{
    INT4 i4BlobStatus; /* 1.Complete   2.Abort  3.Continue */
    UINT1 au1Blob[BEEP_MAX_BLOB_SIZE];
}tBeepSrvMd5Data;



#endif

