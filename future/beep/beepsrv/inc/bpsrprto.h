/********************************************************************
 * Copyright  (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bpsrprto.h,v 1.4 2014/03/03 12:13:01 siva Exp $
 *
 * Description: This file contains global prototypes of Beep Server
 *              functions
 *
 *******************************************************************/

#include "bpcmn.h"

#ifndef   __BPSRV_PROTO_H__
#define   __BPSRV_PROTO_H__

INT4 BeepSrvInit                 PROTO((VOID));
INT4 BeepSrvSetPtrToGbl          PROTO((INT4));
INT4 BeepSrvSockInit             PROTO((INT4));
#ifdef IP6_WANTED
INT4 BeepSrvSock6Init            PROTO((INT4));
#endif
INT4 BeepSrvAcceptNewConnection  PROTO((INT4));
VOID BeepSrvSockCallBk           PROTO((INT4));
VOID BeepSrvSock6CallBk          PROTO((INT4));
VOID BeepSrvPktRcvd              PROTO((INT4));
INT4 BeepSrvSelAddFd             PROTO((INT4, VOID (*pCallBk)(INT4)));
INT4 BeepSrvSendClient           PROTO((INT1 *));
INT4 BeepSrvSelRemoveFd          PROTO((INT4));
INT4 BeepSrvRcvSock              PROTO((VOID));
INT4 BeepSrvSsnHdlr              PROTO((VOID));
VOID BeepSrvForceCloseSsn        PROTO((INT4));
INT4 BeepSrvDeInitSsnDb          PROTO((tBeepSrvSessionDb *));
tBeepSrvSessionDb * BeepSrvGetSsnDb   PROTO((INT4 ));
INT4 BeepSrvAddSession           PROTO((tBeepSrvSessionDb *));
INT4 BeepSrvDeleteSession        PROTO((tBeepSrvSessionDb *));
tBeepSrvSessionDb  * BeepSrvAllocSsnDb           PROTO((VOID));
INT4 BeepSrvDeAllocSsnDb         PROTO((tBeepSrvSessionDb *));
INT4 BeepSrvInitSsnDb            PROTO((tBeepSrvSessionDb *));
VOID BeepSrvForceCloseAllSsn     PROTO((VOID));
INT4 BeepSrvValidateFrame        PROTO((tBeepFrameHdr *));
INT4 BeepSrvChMgmtHdlr           PROTO((tBeepFrameHdr *, INT1 *));
INT4 BeepSrvGrtSent              PROTO((tBeepFrameHdr *, INT1 *));
INT4 BeepSrvGrtRcvd              PROTO((tBeepFrameHdr *, INT1 *));
INT4 BeepSrvOnProcess            PROTO((tBeepFrameHdr *, INT1 *));
INT4 BeepSrvStartEltHdlr         PROTO((tBeepSrvChMgmtData *));
INT4 BeepSrvCloseEltHdlr         PROTO((tBeepSrvChMgmtData *));
INT4 BeepSrvSendGreeting         PROTO((VOID));
INT4 BeepSrvOkEltHdlr            PROTO((tBeepFrameHdr *));
INT4 BeepSrvProfileEltHdlr       PROTO((tBeepSrvChMgmtData *));
VOID BeepSrvCloseChnl            PROTO((INT4 ));
INT4 BeepSrvErrEltHdlr           PROTO((tBeepSrvChMgmtData *));
INT4 BeepSrvRawStart             PROTO((INT4 ));
INT4 BeepSrvRawHdlr              PROTO((tBeepFrameHdr *, INT1 * ));
INT4 BeepShowSrvConf             PROTO((tCliHandle));
INT4 BeepSrvParseChMgmtData      PROTO((INT1 *, tBeepSrvChMgmtData *));
INT4 FindKeyEltType              PROTO((INT1 *));
VOID MvSpcAndLines               PROTO((INT1 *));
INT4 RmvMimeHdr                  PROTO((INT1 *));

INT4 FillErrDetails              PROTO((INT1 *, tBeepSrvChMgmtData *));
INT4 FillStartDetails            PROTO((INT1 *, tBeepSrvChMgmtData *));
INT4 FillGrtDetails              PROTO((INT1 *, tBeepSrvChMgmtData *));
INT4 FillProfDetails             PROTO((INT1 *, tBeepSrvChMgmtData *));

INT4 FillOkDetails               PROTO((INT1 *, tBeepSrvChMgmtData *));
INT4 FillCloseDetails            PROTO((INT1 *, tBeepSrvChMgmtData *));
INT4 BeepSrvDeQueSock            PROTO((VOID));

INT4 BeepSrvMd5GenChallenge      PROTO((UINT1 *));
INT4 BeepSrvMd5Start             PROTO((UINT4));
INT4 BeepSrvParseMd5data         PROTO((tBeepSrvMd5Data *, INT1 *));
INT4 BeepSrvDigestMd5Hdlr        PROTO((tBeepFrameHdr *, INT1 *));
INT4 BeepSrvMd5GetDigest         PROTO((UINT1 *,UINT1 *,UINT1 *));
INT4 SyslogEnQue                 PROTO((INT1 *,INT4 ));
VOID BeepSrvCloseSocket          PROTO((INT4));
VOID BeepSrvCloseTlsSsn          PROTO((VOID *));

INT4 BeepSrvTlsSend              PROTO((INT1 * ));
INT4 BeepSrvTlsRecv              PROTO((VOID *,INT1 *,INT4 *));
INT4 BeepSrvHandleTlsOk          PROTO((VOID));


#endif
