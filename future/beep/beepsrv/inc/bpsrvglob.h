/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bpsrvglob.h,v 1.1.1.1 2009/01/09 13:25:56 prabuc-iss Exp $
 *
 * Description:This file contains the global declarations that are 
 *             used by the Beep server module
 *
 *******************************************************************/


#ifndef _BPSRV_GBL_H__
#define _BPSRV_GBL_H__

tBeepSrvGbl  gBeepSrvGblInfo;
UINT2        gu2BpSrvTrcModule;
#endif
