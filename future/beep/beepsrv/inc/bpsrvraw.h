/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bpsrvraw.h,v 1.1.1.1 2009/01/09 13:25:56 prabuc-iss Exp $
 *
 * Description:This file contains the MACROS related to RAW profile
 *             used by syslog protocol
 *
 ********************************************************************/

#ifndef _BPSRV_RAW_H__
#define _BPSRV_RAW_H__

/* Profile states */

#define BPSRV_RAW_CH_OPENED   0
#define BPSRV_FIRST_MSG_SENT  1
#define BPSRV_ANS_RCVD        2




#endif
