/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsbpsrdb.h,v 1.1.1.1 2009/01/09 13:25:56 prabuc-iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSBPSRDB_H
#define _FSBPSRDB_H


UINT4 fsbpsr [] ={1,3,6,1,4,1,29601,2,18};
tSNMP_OID_TYPE fsbpsrOID = {9, fsbpsr};


UINT4 FsBeepServerAdminStatus [ ] ={1,3,6,1,4,1,29601,2,18,1,1};
UINT4 FsBeepServerRawProfile [ ] ={1,3,6,1,4,1,29601,2,18,1,2};
UINT4 FsBeepServerIpv4PortNum [ ] ={1,3,6,1,4,1,29601,2,18,1,3};
UINT4 FsBeepServerIpv6PortNum [ ] ={1,3,6,1,4,1,29601,2,18,1,4};


tMbDbEntry fsbpsrMibEntry[]= {

{{11,FsBeepServerAdminStatus}, NULL, FsBeepServerAdminStatusGet, FsBeepServerAdminStatusSet, FsBeepServerAdminStatusTest, FsBeepServerAdminStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsBeepServerRawProfile}, NULL, FsBeepServerRawProfileGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, "1"},

{{11,FsBeepServerIpv4PortNum}, NULL, FsBeepServerIpv4PortNumGet, FsBeepServerIpv4PortNumSet, FsBeepServerIpv4PortNumTest, FsBeepServerIpv4PortNumDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "601"},

{{11,FsBeepServerIpv6PortNum}, NULL, FsBeepServerIpv6PortNumGet, FsBeepServerIpv6PortNumSet, FsBeepServerIpv6PortNumTest, FsBeepServerIpv6PortNumDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "601"},
};
tMibData fsbpsrEntry = { 4, fsbpsrMibEntry };
#endif /* _FSBPSRDB_H */

