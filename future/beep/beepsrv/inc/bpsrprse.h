/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bpsrprse.h,v 1.1.1.1 2009/01/09 13:25:56 prabuc-iss Exp $
 *
 * Description: Header file contains data structures for beep server
 *              Channel Management Parser
 *
 *******************************************************************/
#ifndef _BEEPSRV_CHM_PARSE_H_
#define _BEEPSRV_CHM_PARSE__H_

#define ERR_MIN_LENGTH      14
#define MAX_DIGITS          11
#define ERR_CODE_LEN        3
#define STR_DLM             '\0'
#define BPSRV_TEMP_STR_LEN  15

#define BPSRV_MIME_HDR      "Content-Type: application/beep+xml\r\n"

#endif
