/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsbpsrlw.h,v 1.1.1.1 2009/01/09 13:25:56 prabuc-iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsBeepServerAdminStatus ARG_LIST((INT4 *));

INT1
nmhGetFsBeepServerRawProfile ARG_LIST((INT4 *));

INT1
nmhGetFsBeepServerIpv4PortNum ARG_LIST((INT4 *));

INT1
nmhGetFsBeepServerIpv6PortNum ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsBeepServerAdminStatus ARG_LIST((INT4 ));

INT1
nmhSetFsBeepServerIpv4PortNum ARG_LIST((INT4 ));

INT1
nmhSetFsBeepServerIpv6PortNum ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsBeepServerAdminStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsBeepServerIpv4PortNum ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsBeepServerIpv6PortNum ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsBeepServerAdminStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsBeepServerIpv4PortNum ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsBeepServerIpv6PortNum ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
