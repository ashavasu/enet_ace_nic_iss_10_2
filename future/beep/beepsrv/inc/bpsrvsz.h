/********************************************************************
 * * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * *
 * * $Id: bpsrvsz.h,v 1.3 2012/03/30 13:17:18 siva Exp $
 * *
 * * Description:macros and prototypes for bpsrvsz.c
 * ******************************************************************/
enum {
    BPSRV_MAX_BUFFER_BLOCKS_SIZING_ID,
    BPSRV_MAX_CHNL_DATA_SIZING_ID,
    BPSRV_MAX_CHNL_DB_SIZING_ID,
    BPSRV_MAX_MD5_CHNL_DATA_SIZING_ID,
    BPSRV_MAX_SRV_MSG_DB_SIZING_ID,
    BPSRV_MAX_SRV_SESS_DB_SIZING_ID,
    BEEP_SERVER_MAX_SIZING_ID
};


#ifdef  _BEEP_SERVERSZ_C
tMemPoolId BEEP_SERVERMemPoolIds[ BEEP_SERVER_MAX_SIZING_ID];
INT4  Beep_serverSizingMemCreateMemPools(VOID);
VOID  Beep_serverSizingMemDeleteMemPools(VOID);
INT4  Beep_serverSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _BEEP_SERVERSZ_C  */
extern tMemPoolId BEEP_SERVERMemPoolIds[ ];
extern INT4  Beep_serverSizingMemCreateMemPools(VOID);
extern VOID  Beep_serverSizingMemDeleteMemPools(VOID);
extern INT4  Beep_serverSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _BEEP_SERVERSZ_C  */


#ifdef  _BEEP_SERVERSZ_C
tFsModSizingParams FsBEEP_SERVERSizingParams [] = {
{ "INT1[BPSRV_MAX_BUFF_SIZE]", "BPSRV_MAX_BUFFER_BLOCKS", sizeof(INT1[BPSRV_MAX_BUFF_SIZE]),BPSRV_MAX_BUFFER_BLOCKS, BPSRV_MAX_BUFFER_BLOCKS,0 },
{ "tBeepSrvChMgmtData", "BPSRV_MAX_CHNL_DATA", sizeof(tBeepSrvChMgmtData),BPSRV_MAX_CHNL_DATA, BPSRV_MAX_CHNL_DATA,0 },
{ "tBeepChnlDb", "BPSRV_MAX_CHNL_DB", sizeof(tBeepChnlDb),BPSRV_MAX_CHNL_DB, BPSRV_MAX_CHNL_DB,0 },
{ "tBeepSrvMd5Data", "BPSRV_MAX_MD5_CHNL_DATA", sizeof(tBeepSrvMd5Data),BPSRV_MAX_MD5_CHNL_DATA, BPSRV_MAX_MD5_CHNL_DATA,0 },
{ "tBeepSrvMsgDb", "BPSRV_MAX_SRV_MSG_DB", sizeof(tBeepSrvMsgDb),BPSRV_MAX_SRV_MSG_DB, BPSRV_MAX_SRV_MSG_DB,0 },
{ "tBeepSrvSessionDb", "BPSRV_MAX_SRV_SESS_DB", sizeof(tBeepSrvSessionDb),BPSRV_MAX_SRV_SESS_DB, BPSRV_MAX_SRV_SESS_DB,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _BEEP_SERVERSZ_C  */
extern tFsModSizingParams FsBEEP_SERVERSizingParams [];
#endif /*  _BEEP_SERVERSZ_C  */


