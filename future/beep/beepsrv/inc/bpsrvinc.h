/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bpsrvinc.h,v 1.2 2012/01/13 12:16:46 siva Exp $
 *
 * Description:Contains all the include files
 *
 *******************************************************************/
#ifndef _BPSRV_INC_H_
#define _BPSRV_INC_H_

#include "lr.h"
#include "cli.h"
#include "fssyslog.h"
#include "trace.h"
#include "bpcmn.h"
#include "bpsrtdfs.h"
#include "bpsrvdef.h"
#include "bpsrvcli.h"
#include "bpsrvraw.h"
#include "bpsrprse.h"
#include "bpsrvmax.h"
#include "bpsrprto.h"
#include "bpsrvtrc.h"
#include "fsbpsrlw.h"
#include "fsbpsrwr.h"
#include "beepsrv.h"
#include "bpsrvext.h"
#include "bpsrvsz.h"



#endif /*End of define*/

