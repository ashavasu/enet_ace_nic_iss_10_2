/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bpsrvtrc.h,v 1.2 2014/03/16 11:22:48 siva Exp $
 * Description:Provides general purpose macros for tracing 
 *
 *******************************************************************/
#ifndef _BPSRV_TRACE_H_
#define _BPSRV_TRACE_H_

/* Trace and debug flags */
#define  BPSRV_TRC_FLAG   gBeepSrvGblInfo.u4GblTrc 
#define  BPSRV_DBG_FLAG   gBeepSrvGblInfo.u4GblDbg 

/* Trace definitions */


#define  BPSRV_TRC(flg, mod, modname, fmt) \
             MOD_TRC(flg, mod, modname, fmt)
#define  BPSRV_TRC_ARG1(flg, mod, modname, fmt, arg1) \
             MOD_TRC_ARG1(flg, mod, modname, fmt, arg1)
#define  BPSRV_TRC_ARG2(flg, mod, modname, fmt ,arg1 ,arg2) \
             MOD_TRC_ARG2(flg, mod, modname, fmt, arg1, arg2)
#define  BPSRV_TRC_ARG3(flg, mod, modname, fmt ,arg1 ,arg2, arg3) \
             MOD_TRC_ARG3(flg, mod, modname, fmt, arg1, arg2, arg3)
#define  BPSRV_TRC_ARG4(flg, mod, modname, fmt ,arg1 ,arg2, arg3, arg4) \
             MOD_TRC_ARG4(flg, mod, modname, fmt, arg1, arg2, arg3, arg4)

#define  BPSRV_INIT_SHUT_TRC     INIT_SHUT_TRC     
#define  BPSRV_MGMT_TRC          MGMT_TRC          
#define  BPSRV_DATA_PATH_TRC     DATA_PATH_TRC     
#define  BPSRV_CONTROL_PATH_TRC     CONTROL_PLANE_TRC 
#define  BPSRV_DUMP_TRC          DUMP_TRC          
#define  BPSRV_OS_RESOURCE_TRC    OS_RESOURCE_TRC   
#define  BPSRV_FAILURE_TRC    ALL_FAILURE_TRC   
#define  BPSRV_BUFFER_TRC        BUFFER_TRC        
#define  BPSRV_SOCK_TRACE         BPSRV_DATA_PATH_TRC


#endif /* _TRACE_H_ */

/****************************************************************************/
/*           How to use the Trace Statement                                 */
/*                                                                          */
/* 1) Define a UINT4 Trace variable. (eg :u4xxx)                            */
/*                                                                          */
/* 2) The Trace statements can be used as                                   */
/*                                                                          */
/*           MOD_TRC(u4DbgVar, mask, module name, fmt)                      */
/*           MOD_TRC(u4xxx, INIT_SHUT_TRC, XXX, "Trace Statement")          */
/*                                                                          */
/****************************************************************************/

