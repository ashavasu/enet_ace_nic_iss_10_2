#ifndef _FSBPSRWR_H
#define _FSBPSRWR_H

VOID RegisterFSBPSR(VOID);

VOID UnRegisterFSBPSR(VOID);
INT4 FsBeepServerAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsBeepServerRawProfileGet(tSnmpIndex *, tRetVal *);
INT4 FsBeepServerIpv4PortNumGet(tSnmpIndex *, tRetVal *);
INT4 FsBeepServerIpv6PortNumGet(tSnmpIndex *, tRetVal *);
INT4 FsBeepServerAdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsBeepServerIpv4PortNumSet(tSnmpIndex *, tRetVal *);
INT4 FsBeepServerIpv6PortNumSet(tSnmpIndex *, tRetVal *);
INT4 FsBeepServerAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsBeepServerIpv4PortNumTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsBeepServerIpv6PortNumTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsBeepServerAdminStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsBeepServerIpv4PortNumDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsBeepServerIpv6PortNumDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
#endif /* _FSBPSRWR_H */
