/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bpcltdfs.h,v 1.5 2012/03/30 13:17:15 siva Exp $
 *
 * Description: Header file contains data structures for beep client
 *
 *******************************************************************/
#ifndef BPCLNT_TDFS_H_
#define BPCLNT_TDFS_H_
#define BPCLT_ID  gBpCltGblInfo.i4SyslogId

typedef tTimerListId tBpCltTmrListId;

typedef struct BpCltGbl
{
   INT4               i4SyslogId;
   tOsixTaskId        BpCltTaskId;
   tOsixSemId         BpCltSemId;
   tOsixQId           BpCltSyslogQId; 
   tOsixQId           BpCltSrvQId; 
   tMemPoolId         BpSrvPoolId;  
   tMemPoolId         BpSsnPoolId;  
   tMemPoolId         BpChnlPoolId;  
   tMemPoolId         BpSyslogMsgPoolId;  
   tMemPoolId         BpCltPendingLogPoolId;  
   tMemPoolId         BpSrvMsgPoolId;
   tMemPoolId         BpCltMgmtChnlPoolId;
   tMemPoolId         BpCltFramePoolId;
   tMemPoolId         BpCltMsgPoolId;
   tTimerListId       BpCltTmrId;
   INT4 i4BpCltTlsFlg;  /* Encryption enabled/disabled */
   INT4 i4BpCltMd5Flg; /*  Authentication enabled/disabled */
   INT4 i4BpCltRawFlg; /*  RAW profile enabled/disabled */
   tTMO_SLL  AuthKeyList; /* Auth key table */ 
   tTMO_SLL  SrvSessionList;
    
}tBpCltGbl;


typedef struct BpCltTmr
{
    tTmrAppTimer  TimerNode;
    tIPvXAddr     SrvAddr;
}tBpCltTmr;

typedef struct BpCltMsgHdr
{
    UINT4       u4Cmd;
    UINT4       u4Port;
    tIPvXAddr  SrvAddr;
}tBpCltMsgHdr;

typedef tBeepChnlDb tBpCltChnlNode;

typedef struct BpCltFrame
{
    INT1 i1FullFlag; /* Full Frame - 1   Not FUll- 2 */
    INT1 ai1Pad[3];
    INT1 ai1Frame[BEEP_MAX_BUFF_SIZE];
    INT4 i4Size;
}tBpCltFrame;

typedef struct BpCltSessionNode
{
    tTMO_SLL_NODE Next;
    tBpCltChnlNode *aBpCltChnlList[BEEP_MAX_CHANNELS];   
    INT1         i1TlsFlag;
    INT1         i1Md5Flag;
    INT1         ai1Pad[2];
    INT4         i4SockId;
    INT4         i4SessionActive;
    UINT4        u4ProfileBitMask;   
    tIPvXAddr    SrvAddr;
    tBpCltTmr    Timer;
    tBpCltFrame  RxFrame; 
    tTMO_SLL     BpCltMsgList;
    tTMO_SLL     BpCltPendingLogList;    
    INT1         ai1ReadBuf[BEEP_MAX_BUFF_SIZE];
    INT1         ai1WriteBuf[BEEP_MAX_BUFF_SIZE];
    INT1         ai1TempBuf[BEEP_MAX_BUFF_SIZE];
}tBpCltSsnNode;

typedef struct BpCltAuthNode
{
    UINT1    au1SrvAuthKey[16];
    INT4     i4RowStatus;
    tIPvXAddr SrvAddr;
}tBpCltAuthNode;

typedef struct BpMsg
{
    tTMO_SLL_NODE Next;
    INT4   i4MsgNo;
    INT4   i4Msg;  /* close/start */
    INT4   i4ChnlNo;
}tBpCltMsg;

typedef tBeepFrameHdr tBpCltFrameHdr;

typedef struct BpCltMgmtChnlData
{
     INT1 au1ErrStr[BEEP_MAX_CHAR];
     INT1 i1CdataRcvd; /* 0 not rcvd : 1 blob : 2  Code */
     INT1 i1Pad;
     INT1 ai1Profile[BPCLT_MAX_PROFILES][BEEP_MAX_CHAR];
     INT4 i4KeyElementType; /* 0- Err  1 -greeting   2- profile   3- Start   3 - Close    4 - ok  */
     INT4 i4ProfCount;
     INT4 i4Code;
     UINT4 u4Num;
     union
     {
          UINT1 au1Blob[BEEP_MAX_CHAR + 2];
          INT4 i4ElementCode; /* 1.ready  2- proceed */
     }Cdata;
}tBpCltMgmtChnlData;
/*
typedef struct BpCltSysLogMsg
{
 UINT1 au1SysLog[BEEP_MAX_CHAR];
 INT4  i4Port;
 tIPvXAddr    SrvAddr;
 INT4  i4Profile;
}tBpCltSysLogMsg;
*/ 
typedef struct BeepSrvMd5Data
{
    INT4 i4BlobStatus; /* 1.Complete   2.Abort  3.Continue */
    INT4 i4ErrCode;
    UINT1 au1ErrStr[BEEP_MAX_BLOB_SIZE];
}tBeepSrvMd5Data;

typedef struct BpCltPendingMsg
{
    tTMO_SLL_NODE Next;
    UINT1 au1LogMsg[BEEP_MAX_BUFF_SIZE];
}tBpCltPendingMsg;


#endif

