/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bpcltmac.h,v 1.7 2014/03/03 12:13:00 siva Exp $
 *
 * Description: Header file contains data structures for beep client
 *
 *******************************************************************/

#ifndef BPCLNT_MAC_H_
#define BPCLNT_MAC_H_

#define BPCLT_SYSLOG_Q           "BCSY"
#define BPCLT_SRV_Q              "BCSR"
#define BPCLT_PEND_Q             "BCSP"
#define BPCLT_TASK               "BPCL"
#define BPCLT_NAME               "BPCL"
#define BPCLT_SEM_NAME           ((UINT1 *) "BPCL_SEM")

#define BPCLT_ENABLE  BEEP_ENABLE
#define BPCLT_DISABLE BEEP_DISABLE

#define BPCLT_SUCCESS  0
#define BPCLT_FAILURE  1
#define BPCLT_UNEXPECTED  2
#define BPCLT_TLS_OK  3
#define BPCLT_TCP_ABORT 4
#define BPCLT_APP_ABORT 5
#define BPCLT_DIGEST_FAIL 6
#define BPCLT_TLS_FAIL 7
#define BPCLT_DIGEST_OK  8
#define BPCLT_OVER  9

#define BPCLT_FLG_ACTIVE     1  /* lakshmivm: can be made common */
#define BPCLT_FLG_NOT_ACTIVE 0

#define BPCLT_FOUND  1
#define BPCLT_NOT_FOUND  2

#define BPCLT_FULL_FRAME  1
#define BPCLT_PARTIAL_FRAME  2

#define BPCLT_INIT_COMPLETE(u4Status)     lrInitComplete(u4Status)
#define BPCLT_PENDING_LOG_Q_DEPTH         30      

#define BPCLT_MAX_BUF_SIZE                BEEP_MAX_BUFF_SIZE   
#define BPCLT_MAX_APPLNS                  2
#define BPCLT_MAX_PROFILES                4 
#define BPCLT_MAX_AUTHKEY_SIZE            16
#define BPCLT_MAX_PROFILE_SIZE            255
#define BPCLT_MAX_SOC_WRITE_LEN           2500
 
#define BPCLT_SESSION_TIMEOUT_VAL          1800/* seconds */ 
#define BPCLT_MAX_SYSLOG_MSG_LEN           1024


/* Beep Client Events */
#define BPCLT_SYSLOG_PKT_RCV_EVENT 1
#define BPCLT_SRV_PKT_RCV_EVENT    2
#define BPCLT_TLS_EVENT            3
#define BPCLT_SESSION_TMR_EVENT    4

/* Profile supported in Beep Client */
#define BPCLT_MGMT_CHNL            0
#define BPCLT_TLS_CHNL             2
#define BPCLT_MD5_CHNL             3 
#define BPCLT_RAW_CHNL             1

/* Syslog Notifications */
#define BPCLT_SYSLOG_MSG         1
#define BPCLT_SYSLOG_CFG_CHG     2

/* Channel States */
#define BPCLT_CHNL_OPENED          0
#define BPCLT_NUL_SENT             1
#define BPCLT_DUMMY_RCVD           2

/* Beep Client RPY Frame Key Element Type */
#define  BPCLT_ERR_TYPE            0
#define  BPCLT_GREETING_TYPE       1
#define  BPCLT_PROFILE_TYPE        2
#define  BPCLT_START_TYPE          3
#define  BPCLT_CLOSE_TYPE          4
#define  BPCLT_OK_TYPE             5

/* RPY frame CDATA values */
#define BPCLT_RPY_BLOB    1
#define BPCLT_RPY_CODE    2

/* RPY frame CDATA Element code values */
#define BPCLT_ELEMENT_CODE_READY    1
#define BPCLT_ELEMENT_CODE_PROCEED    2


/* Message type in channel 0 Msg DB */
#define BPCLT_CLOSE_MSG   1
#define BPCLT_START_MSG   2


/* Beep client profiles supported */
#define BPCLT_TLS_REQUIRED        0x01
#define BPCLT_MD5_REQUIRED        0x02
#define BPCLT_RAW_REQUIRED        0x04

/* Beep client error messages */
#define BPCLT_SERVICE_SUCCESS        200
#define BPCLT_SERVICE_NOT_AVAILABLE  421
#define BPCLT_ACTION_NOT_TAKEN       550

/* Beep Client Profile State */
#define BPCLT_GREETINGS_SENT      1
#define BPCLT_TLS_ON_PROGRESS      2
#define BPCLT_MD5_ON_PROGRESS      3
#define BPCLT_APPLN_ON_PROGRESS      4
#define BPCLT_CLOSE_SENT      5
#define BPCLT_ANS_SENT      6


/* Channel Mgmt Parser Macros */
#define BPCLT_MIME_HDR             "Content-Type: application/beep+xml\r\n"
#define BPCLT_STR_DLM              '\0'
#define BPCLT_ERR_CODE_LEN         3
#define BPCLT_MAX_DIGITS           15
#define BPCLT_TEMP_STR_LEN         50
#define BPCLT_ERR_MIN_LENGTH       14

/* Memory */
#define MALLOC(a)  malloc(a)

#define   BPCLT_ALLOCATE_BUF(u4Size, u4ValidOffset)                 \
          CRU_BUF_Allocate_MsgBufChain((u4Size), (u4ValidOffset))

#define   BPCLT_RELEASE_BUF(pBuf, u1ForceFlag)                      \
          CRU_BUF_Release_MsgBufChain((pBuf), (u1ForceFlag))

#define   BPCLT_COPY_TO_BUF(pBuf, pSrc, u4Offset, u4Size)           \
          CRU_BUF_Copy_OverBufChain((pBuf),(UINT1 *)(pSrc),(u4Offset),(u4Size))

#define   BPCLT_GET_MODULE_DATA_PTR(pBuf) \
          &(pBuf->ModuleData)

/* DEclarations required for main function */
#define BPCLT_MOD_NAME  "BEEP_CLIENT"
#define BPCLT_EVENT_WAIT_FLAG   OSIX_WAIT



/* End of MAIN function declarations */

#define   BPCLT_SYSLOG_MSG_Q      gBpCltGblInfo.BpCltSyslogQId
#define   BPCLT_SRV_MSG_Q         gBpCltGblInfo.BpCltSrvQId

#define   BPCLT_TASK_ID           gBpCltGblInfo.BpCltTaskId
#define   BPCLT_SEM_ID            gBpCltGblInfo.BpCltSemId

#define   BPCLT_SYSLOG_POOL_ID    gBpCltGblInfo.BpSyslogMsgPoolId
#define   BPCLT_PENDING_LOG_POOL_ID    gBpCltGblInfo.BpCltPendingLogPoolId
#define   BPCLT_SRV_POOL_ID       gBpCltGblInfo.BpSrvPoolId
#define   BPCLT_SSN_POOL_ID       gBpCltGblInfo.BpSsnPoolId
#define   BPCLT_CHNL_POOL_ID      gBpCltGblInfo.BpChnlPoolId
#define   BPCLT_MGMT_CHNL_POOL_ID gBpCltGblInfo.BpCltMgmtChnlPoolId
#define   BPCLT_FRAME_POOL_ID     gBpCltGblInfo.BpCltFramePoolId
#define   BPCLT_SYSLOG_MSG_POOL_ID  gBpCltGblInfo.BpCltMsgPoolId

#define   BPCLT_AUTH_KEY_LIST     gBpCltGblInfo.AuthKeyList
#define   BPCLT_SRV_SESSION_LIST  gBpCltGblInfo.SrvSessionList
#define   BPCLT_SSN_TIMER_ID      gBpCltGblInfo.BpCltTmrId

#define   IS_BPCLT_AUTH_ENABLED   gBpCltGblInfo.bBpCltMd5Flg;

#define BPCLT_CREATE_MEM_POOL(u4BlkSize, u4NumBlks, pPoolId) \
   MemCreateMemPool((u4BlkSize),              \
                    (u4NumBlks),              \
                    MEM_DEFAULT_MEMORY_TYPE,  \
                    (pPoolId))


#define BPCLT_DELETE_MEM_POOL(pPoolId) \
   MemDeleteMemPool((pPoolId))

#define BPCLT_ALLOC_IF_REC_MEM(pu1Block) \
   (pu1Block = (tRipIfaceRec *)(MemAllocMemBlk ((tMemPoolId)(IpRipMemory.IfQId))))

#define BPCLT_RELEASE_IF_REC_MEM(ppu1Block) \
   MemReleaseMemBlock ((tMemPoolId)(IpRipMemory.IfQId) ,(UINT1 *) ppu1Block)


#define BPCLT_CREATE_TMR_LIST(au1TaskName, u4Event, pTmrListId)  \
       TmrCreateTimerList((au1TaskName), (u4Event), NULL, (pTmrListId))

#define  BPCLT_OFFSET(x,y)        FSAP_OFFSETOF (x,y)

#define BPCLT_GET_ADDR_INFO_FROM_TIMER(x,y) \
          (y *)(VOID *)((UINT1 *)(x) - BPCLT_OFFSET(y, Timer))

/* BACK PTR FOR LISTS */
#define MSGLST_OFFSET(x,y) ((UINT4)(&((x *)0)->y))
#define GET_MSGLST(x) ((tBpCltMsg *)((UINT1 *)x-MSGLST_OFFSET(tBpCltMsg,Next)))

#define SSNLST_OFFSET(x,y) ((UINT4)(&((x *)0)->y))
#define GET_SSNLST(x) ((tBpCltSsnNode *)((UINT1 *)x-SSNLST_OFFSET(tBpCltSsnNode,Next)))

#define PENDLST_OFFSET(x,y) ((UINT4)(&((x *)0)->y))
#define GET_PENDLST(x) ((tBpCltPendingMsg *)((UINT1 *)x-PENDLST_OFFSET(tBpCltPendingMsg,Next)))





#endif
