/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: bpclntsz.h,v 1.2 2012/03/30 13:17:15 siva Exp $
*
* Description:macros and prototypes for bpclntsz.c
******************************************************************/
enum {
    BPCLT_MAX_CHNL_NODE_SIZING_ID,
    BPCLT_MAX_FRAME_BLOCKS_SIZING_ID,
    BPCLT_MAX_MGMT_CHNL_NODE_SIZING_ID,
    BPCLT_MAX_MSG_BLOCKS_SIZING_ID,
    BPCLT_MAX_MSG_SIZING_ID,
    BPCLT_MAX_PENDING_MSG_SIZING_ID,
    BPCLT_MAX_SSN_NODE_SIZING_ID,
    BPCLT_MAX_SYSLOG_MSG_SIZING_ID,
    BEEP_CLIENT_MAX_SIZING_ID
};


#ifdef  _BEEP_CLIENTSZ_C
tMemPoolId BEEP_CLIENTMemPoolIds[ BEEP_CLIENT_MAX_SIZING_ID];
INT4  Beep_clientSizingMemCreateMemPools(VOID);
VOID  Beep_clientSizingMemDeleteMemPools(VOID);
INT4  Beep_clientSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _BEEP_CLIENTSZ_C  */
extern tMemPoolId BEEP_CLIENTMemPoolIds[ ];
extern INT4  Beep_clientSizingMemCreateMemPools(VOID);
extern VOID  Beep_clientSizingMemDeleteMemPools(VOID);
extern INT4  Beep_clientSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _BEEP_CLIENTSZ_C  */


#ifdef  _BEEP_CLIENTSZ_C
tFsModSizingParams FsBEEP_CLIENTSizingParams [] = {
{ "tBpCltChnlNode", "BPCLT_MAX_CHNL_NODE", sizeof(tBpCltChnlNode),BPCLT_MAX_CHNL_NODE, BPCLT_MAX_CHNL_NODE,0 },
{ "INT1[BEEP_MAX_FRAME_SIZE]", "BPCLT_MAX_FRAME_BLOCKS", sizeof(INT1[BEEP_MAX_FRAME_SIZE]),BPCLT_MAX_FRAME_BLOCKS, BPCLT_MAX_FRAME_BLOCKS,0 },
{ "tBpCltMgmtChnlData", "BPCLT_MAX_MGMT_CHNL_NODE", sizeof(tBpCltMgmtChnlData),BPCLT_MAX_MGMT_CHNL_NODE, BPCLT_MAX_MGMT_CHNL_NODE,0 },
{ "CHR1[BPCLT_MAX_SYSLOG_MSG_LEN]", "BPCLT_MAX_MSG_BLOCKS", sizeof(CHR1[BPCLT_MAX_SYSLOG_MSG_LEN]),BPCLT_MAX_MSG_BLOCKS, BPCLT_MAX_MSG_BLOCKS,0 },
{ "tBpCltMsg", "BPCLT_MAX_MSG", sizeof(tBpCltMsg),BPCLT_MAX_MSG, BPCLT_MAX_MSG,0 },
{ "tBpCltPendingMsg", "BPCLT_MAX_PENDING_MSG", sizeof(tBpCltPendingMsg),BPCLT_MAX_PENDING_MSG, BPCLT_MAX_PENDING_MSG,0 },
{ "tBpCltSsnNode", "BPCLT_MAX_SSN_NODE", sizeof(tBpCltSsnNode),BPCLT_MAX_SSN_NODE, BPCLT_MAX_SSN_NODE,0 },
{ "tBpCltSyslogMsg", "BPCLT_MAX_SYSLOG_MSG", sizeof(tBpCltSyslogMsg),BPCLT_MAX_SYSLOG_MSG, BPCLT_MAX_SYSLOG_MSG,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _BEEP_CLIENTSZ_C  */
extern tFsModSizingParams FsBEEP_CLIENTSizingParams [];
#endif /*  _BEEP_CLIENTSZ_C  */


