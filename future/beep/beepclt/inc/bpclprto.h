/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bpclprto.h,v 1.4 2014/03/16 11:22:47 siva Exp $
 *
 * Description: This file contains definitions required for
 * the state machine operation in the bfd module.
 *******************************************************************/


#ifndef _BPCLT_PROTO_H_
#define _BPCLT_PROTO_H_
VOID BpCltDelMemInit PROTO((VOID));

VOID BpCltSyslogMsgHdlr PROTO((VOID));


INT4 BpCltStartTimer PROTO((tTmrAppTimer * pAppTimer, UINT4 u4_Duration));

INT4 BpCltStopTimer PROTO((tTmrAppTimer * pAppTimer));

VOID BpCltSsnTimeoutHdlr PROTO((VOID));

INT4 BpCltGetAuthKey PROTO((tIPvXAddr *pSrvAddr, UINT1 *pAuthKey));

INT4 BpCltCreateMD5Channel PROTO((INT4 i4ChnlNo, tBpCltSsnNode *pSsnNode));

INT4 BpCltCheckAuthServer PROTO((tIPvXAddr *pSrvAddr));

INT4 BpCltRawChnlHdlr PROTO((tBpCltFrameHdr *pRcvFrameHdr, INT1 *pi1Frame,
                       INT4 i4SockId));

tBpCltChnlNode* BpCltCreateChannel PROTO((INT4 i4ChnlNum, INT4 i4Profile));
INT4 BpCltAddChannel PROTO((tBpCltChnlNode *pChnlNode, 
                            tBpCltSsnNode *pSsnPtr));
INT4 BpCltDeleteChannel PROTO((tBpCltChnlNode *pChnlNode, 
                               tBpCltSsnNode *pSsnPtr));

tBpCltChnlNode* BpCltGetChannelNode PROTO((tBpCltSsnNode *pSsnPtr, 
                                        INT4 i4ChnlNum));

INT4 BpCltMgmtChnlHdlr PROTO((tBpCltFrameHdr *pRcvFrameHdr, INT1 *pi1RcvFrame,
                              INT4 i4SockFd));

INT4 BpCltProcessSyslogMsg PROTO((tBpCltSyslogMsg *pu1Buf));
INT4 BpCltProcessPktOnGrtgsSent PROTO((tBpCltFrameHdr *pRcvFrameHdr, 
                                      tBpCltMgmtChnlData *pMgmtChnlData,
                                      INT4 i4SockFd));
INT4 BpCltProcessPktOnTLSProgress PROTO((tBpCltFrameHdr *pRcvFrameHdr,
                          tBpCltMgmtChnlData *pMgmtChnlData,
                          INT4 i4SockFd));

INT4 BpCltProcessPktOnMD5Progress PROTO((tBpCltFrameHdr *pRcvFrameHdr,
                          tBpCltMgmtChnlData *pMgmtChnlData,
                          INT4 i4SockFd));


INT4 BpCltProcessPktOnAppProgress PROTO((tBpCltFrameHdr *pRcvFrameHdr,
                          tBpCltMgmtChnlData *pMgmtChnlData,
                          INT4 i4SockFd));

INT4 BpCltProcessPktOnCloseSent PROTO((tBpCltFrameHdr *pRcvFrameHdr,
                          tBpCltMgmtChnlData *pMgmtChnlData,
                          INT4 i4SockFd, tBpCltSsnNode *pSsnNode));

INT4 BpCltGetProfilesSupported PROTO((UINT4 u4BitMask, INT1 *pi1Profile, 
                                      INT4 *pi4Profile));
INT4 BpCltCheckProfilesSupported PROTO((UINT4 u4ProfileCnt, 
                                        UINT4 u4ProfileBitMask,
                                        INT1 *pi1Profile));

INT4 BpCltGetNewChnlNum PROTO((tBpCltSsnNode *pSsnNode, UINT4 *pChnlNum));

INT4 BpCltProfileElementHdlr PROTO((tBpCltSsnNode *pSsnNode, INT4 i4ChnlNum,
                              INT4 i4Profile));

INT4 BpCltOkElementHdlr PROTO((tBpCltSsnNode *pSsnNode, 
                               tBpCltFrameHdr *pFrameHdr));

tBpCltSsnNode *BpCltCreateSession PROTO((INT4 i4Port, tIPvXAddr *pSrvAddr));

INT4 BpCltAddSession PROTO((tBpCltSsnNode *pSrvNode));

INT4 BpCltDeleteSession PROTO ((tBpCltSsnNode *pSrvNode));

tBpCltSsnNode* BpCltGetSessionNode PROTO((tIPvXAddr *pSrvAddr, INT4 i4SockId));

tBpCltChnlNode* BpCltGetRawChnlInSession PROTO((tBpCltSsnNode *pSession));

VOID BpCltServerMsgHdlr PROTO((tCRU_BUF_CHAIN_HEADER *pu1Buf));

INT4 BpCltRcvServerMsg PROTO((INT4 i4SockId,tBpCltSsnNode * pSsnNode));

VOID BpCltForceCloseSsn PROTO((INT4 i4Sock));

VOID BeepSrvForceCloseAllSsn PROTO((VOID));

INT4 BpCltSendToServer PROTO ((tBpCltSsnNode *pSsnNode, INT1 *pi1String));

VOID BpCltProcessTlsMsg PROTO ((VOID));

VOID BpCltCloseSocket PROTO ((INT4 i4SockId));

INT4 BpCltSockCreate PROTO((INT4 i4Port, UINT4 u4Addr, INT4 *pi4SockId));
#ifdef IP6_WANTED
INT4 BpCltSock6Create PROTO ((INT4 i4Port, tIPvXAddr *pAddr, INT4 *pi4SockId));
#endif

INT4 BpCltSockInit PROTO((INT4 i4Port, tIPvXAddr *pSrvAddr, INT4 *pi4SockId));

INT4 BpCltTlsSend PROTO((tBpCltSsnNode *pSsnNode, INT1 *pi1String));

VOID BpCltProcessSyslogCfgChg PROTO ((tBpCltSyslogMsg *));

INT4 BpCltSendlogMessage PROTO ((tBpCltSsnNode *,tBeepChnlDb *,UINT1 *));

INT4 BpCltSendGreetings PROTO ((tBpCltSsnNode *));

INT4 BpCltParseMgmtChnlData PROTO ((tBpCltMgmtChnlData *, INT1 *));

INT4 BpCltFindKeyEltType PROTO ((INT1 *));

VOID BpCltMvSpcAndLines PROTO ((INT1 *pi1Str));

INT4 BpCltRmvMimeHdr PROTO ((INT1 *));

INT4 BpCltFillErrDetails PROTO ((INT1 *, tBpCltMgmtChnlData *));

INT4 BpCltFillStartDetails PROTO ((INT1 *, tBpCltMgmtChnlData *));

INT4 BpCltFillGrtDetails PROTO ((INT1 *, tBpCltMgmtChnlData *));

INT4 BpCltFillOkDetails PROTO ((INT1 *, tBpCltMgmtChnlData *));

INT4 BpCltFillCloseDetails PROTO((INT1 *, tBpCltMgmtChnlData *));

INT4 BpCltFillProfDetails PROTO ((INT1 *, tBpCltMgmtChnlData *));

INT4  BpCltGenerateMD5Digest PROTO ((UINT1 *,UINT1 *));

INT4  BpCltCloseEltHdlr PROTO ((tBpCltSsnNode *, tBpCltMgmtChnlData *,
                                tBpCltFrameHdr *));

INT4  BpCltStartEltHdlr PROTO ((tBpCltSsnNode *, tBpCltMgmtChnlData *,
                                tBpCltFrameHdr *));

INT4 BpCltProfileEltHdlr PROTO ((tBpCltSsnNode *, tBpCltMgmtChnlData *,
                                 tBpCltFrameHdr *));

INT4  BpCltDeInitSession PROTO ((tBpCltSsnNode *pSsnNode));

INT4  BpCltValidateFrame PROTO ((tBpCltFrameHdr *pFrameHdr, 
                                  tBeepChnlDb *pChnlNode));
VOID BpCltForceCloseAllSsn PROTO((VOID));

#endif

