/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bpcltinc.h,v 1.3 2012/01/13 12:16:43 siva Exp $
 *
 * Description: Header file contains all Beep client header files
 *
 *******************************************************************/

#ifndef BPCLNT_INC_H_
#define BPCLNT_INC_H_

#include "lr.h"
#include "utilipvx.h"
#include "bpcmn.h"
#include "beepclt.h"  
#include "bpclttrc.h"
#include "bpcltmac.h"
#include "bpcltdfs.h"
#include "bpcltext.h"
#include "bpclprto.h"
#include "fssyslog.h"
#include "bpclntsz.h"
#endif /* */

