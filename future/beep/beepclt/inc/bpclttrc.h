/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 *
 * Description:Provides general purpose macros for tracing 
 *
 *******************************************************************/
#include "trace.h"
#ifndef _BPCLT_TRACE_H_
#define _BPCLT_TRACE_H_

/* Trace and debug flags */
#define  BPCLT_TRC_FLAG   gu4BpCltGblTrc 

/* Trace definitions */

#define  BPCLT_TRC(flg, mod, modname, fmt) \
             MOD_TRC(flg, mod, modname, fmt)
#define  BPCLT_TRC_ARG1(flg, mod, modname, fmt, arg1) \
             MOD_TRC_ARG1(flg, mod, modname, fmt, arg1)
#define  BPCLT_TRC_ARG2(flg, mod, modname, fmt ,arg1 ,arg2) \
             MOD_TRC_ARG2(flg, mod, modname, fmt, arg1, arg2)
#define  BPCLT_TRC_ARG3(flg, mod, modname, fmt ,arg1 ,arg2, arg3) \
             MOD_TRC_ARG3(flg, mod, modname, fmt, arg1, arg2, arg3)
#define  BPCLT_TRC_ARG4(flg, mod, modname, fmt ,arg1 ,arg2, arg3, arg4) \
             MOD_TRC_ARG4(flg, mod, modname, fmt, arg1, arg2, arg3, arg4)

#define  BPCLT_INIT_SHUT_TRC     INIT_SHUT_TRC     
#define  BPCLT_MGMT_TRC          MGMT_TRC          
#define  BPCLT_DATA_PATH_TRC     DATA_PATH_TRC     
#define  BPCLT_CONTROL_PATH_TRC  CONTROL_PLANE_TRC 
#define  BPCLT_DUMP_TRC          DUMP_TRC          
#define  BPCLT_OS_RESOURCE_TRC   OS_RESOURCE_TRC   
#define  BPCLT_FAILURE_TRC       ALL_FAILURE_TRC   
#define  BPCLT_BUFFER_TRC        BUFFER_TRC        

#endif /* _TRACE_H_ */

/****************************************************************************/
/*           How to use the Trace Statement                                 */
/*                                                                          */
/* 1) Define a UINT4 Trace variable. (eg :u4xxx)                            */
/*                                                                          */
/* 2) The Trace statements can be used as                                   */
/*                                                                          */
/*           MOD_TRC(u4DbgVar, mask, module name, fmt)                      */
/*           MOD_TRC(u4xxx, INIT_SHUT_TRC, XXX, "Trace Statement")          */
/*                                                                          */
/****************************************************************************/

