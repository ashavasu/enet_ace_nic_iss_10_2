/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bpclntsz.c,v 1.3 2013/11/29 11:04:11 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _BEEP_CLIENTSZ_C
#include "bpcltinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
Beep_clientSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < BEEP_CLIENT_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsBEEP_CLIENTSizingParams[i4SizingId].
                              u4StructSize,
                              FsBEEP_CLIENTSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(BEEP_CLIENTMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            Beep_clientSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
Beep_clientSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsBEEP_CLIENTSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, BEEP_CLIENTMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
Beep_clientSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < BEEP_CLIENT_MAX_SIZING_ID; i4SizingId++)
    {
        if (BEEP_CLIENTMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (BEEP_CLIENTMemPoolIds[i4SizingId]);
            BEEP_CLIENTMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
