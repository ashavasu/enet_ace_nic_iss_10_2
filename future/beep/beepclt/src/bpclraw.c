/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bpclraw.c,v 1.2 2012/03/30 13:17:16 siva Exp $
 *
 * Description:This file contains the functions to handle raw profile
 * for Beep Client 
 *
 *******************************************************************/

#include "bpcltinc.h"

/**************************************************************************/
/*   Function Name   : BpCltRawChnlHdlr                                   */
/*   Description     : This function takes and reads the active session   */
/*                     socket and parse and pass the data to profile hdlrs*/
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : BPCLT_SUCCESS or BPCLT_FAILURE                     */
/**************************************************************************/

INT4
BpCltRawChnlHdlr (tBpCltFrameHdr * pRcvFrameHdr, INT1 *pi1Frame, INT4 i4SockId)
{

    tBpCltPendingMsg   *pMsg = NULL;
    tBpCltPendingMsg   *pTmp = NULL;
    tBpCltChnlNode     *pChnlNode = NULL;
    tBpCltSsnNode      *pSsnNode = NULL;

    if ((pRcvFrameHdr == NULL) || (pi1Frame == NULL))
    {
        return BPCLT_FAILURE;
    }


    pSsnNode = BpCltGetSessionNode (NULL, i4SockId);

    if (pSsnNode != NULL)
    {
        pChnlNode = BpCltGetChannelNode (pSsnNode, pRcvFrameHdr->i4ChnlNum);
        if (pChnlNode == NULL)
        {
            BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC, BPCLT_NAME,
                       "Poorly Formed Frame Received:Closing Session !!");
            return BPCLT_UNEXPECTED;
        }
    }

    if (pChnlNode == NULL)
    {
        return BPCLT_FAILURE;
    }

    /* Check the received message is Dummy message and the RAW channel 
     * profile state is channel opened */
    if (pChnlNode->i4ProfileState != BPCLT_CHNL_OPENED)
    {
        return BPCLT_FAILURE;
    }

    /* Check the received message is dummy */
    if (pRcvFrameHdr->i4FrameHeadType == BEEP_MSG_FRAME)
    {
        pChnlNode->i4ProfileState = BPCLT_DUMMY_RCVD;
        /* Get the Messages from the Pending Log list and send */
        TMO_SLL_Scan (&pSsnNode->BpCltPendingLogList, pTmp, tBpCltPendingMsg *)
        {
            pMsg = pTmp;
            if (TMO_SLL_Count (&pSsnNode->BpCltPendingLogList) == BEEP_ZERO)
            {
                break;
            }

            /* send the syslog log message in ANS frame */
            BpCltSendlogMessage (pSsnNode, pChnlNode, pMsg->au1LogMsg);    /* TODO */

            /* Delete the node from list */
            TMO_SLL_Delete (&(pSsnNode->BpCltPendingLogList),
                            (tTMO_SLL_NODE *) & pMsg->Next);
            /* Free the pMsg */
            MemReleaseMemBlock (BPCLT_PENDING_LOG_POOL_ID, (UINT1 *) pMsg);
        }
    }

    pChnlNode->i4ProfileState = BPCLT_ANS_SENT;
    return BPCLT_SUCCESS;
}
