/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bpclssn.c,v 1.13 2014/03/16 11:22:48 siva Exp $
 *
 * Description:This file contains the utility functions for Beep Client 
 * session management for Beep Server.    
 *
 *******************************************************************/

#include "bpcltinc.h"
#include "fssocket.h"

/**************************************************************************/
/*   Function Name   : BpCltCreateSession                                 */
/*   Description     : This functions is used to create session for Beep  */
/*                     server                                             */
/*                                                                        */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : Pointer to a session if the session is created     */
/*                     successfully, NULL otherwise                       */
/**************************************************************************/

tBpCltSsnNode      *
BpCltCreateSession (INT4 i4Port, tIPvXAddr * pSrvAddr)
{
    INT4                i4SockDesc = OSIX_FAILURE;

    tBpCltChnlNode     *pChnlNode = NULL;
    tBpCltSsnNode      *pSsnNode = NULL;

    UNUSED_PARAM (i4Port);

    /* Creation a session in the Beep Session table */
    pSsnNode = (tBpCltSsnNode *) MemAllocMemBlk (BPCLT_SSN_POOL_ID);
    if (pSsnNode == NULL)
    {
        BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_MGMT_TRC, BPCLT_NAME,
                   "Malloc for Beep Session Node Entry %s failed. \n");
        close (i4SockDesc);
        return NULL;
    }

    BpCltAddSession (pSsnNode);
    pSsnNode->i1TlsFlag = BPCLT_FLG_NOT_ACTIVE;
    pSsnNode->i1Md5Flag = BPCLT_FLG_NOT_ACTIVE;
    if (gBpCltGblInfo.i4BpCltTlsFlg == BPCLT_ENABLE)
    {
        pSsnNode->u4ProfileBitMask = BPCLT_TLS_REQUIRED;
    }
    if (gBpCltGblInfo.i4BpCltMd5Flg == BPCLT_ENABLE)
    {
        pSsnNode->u4ProfileBitMask |= BPCLT_MD5_REQUIRED;
    }

    MEMSET (pSsnNode->ai1ReadBuf, BEEP_ZERO, BPCLT_MAX_BUF_SIZE);
    MEMSET (pSsnNode->ai1WriteBuf, BEEP_ZERO, BPCLT_MAX_BUF_SIZE);
    MEMSET (pSsnNode->ai1TempBuf, BEEP_ZERO, BPCLT_MAX_BUF_SIZE);
    MEMSET (pSsnNode->aBpCltChnlList, BEEP_ZERO, BPCLT_MAX_PROFILES);
    MEMSET (pSsnNode->RxFrame.ai1Frame, BEEP_ZERO, BPCLT_MAX_BUF_SIZE);

    TMO_SLL_Init (&pSsnNode->BpCltMsgList);
    TMO_SLL_Init (&pSsnNode->BpCltPendingLogList);
    MEMCPY (&pSsnNode->SrvAddr, pSrvAddr, sizeof (tIPvXAddr));

    pSsnNode->i4SessionActive = BPCLT_FLG_ACTIVE;

    /* Create a Management Channel */
    pChnlNode = BpCltCreateChannel (BEEP_ZERO, BPCLT_MGMT_CHNL);

    /* Add the Channel node in Session */
    pSsnNode->aBpCltChnlList[BEEP_ZERO] = pChnlNode;

    /* Start a timer for closing the server session */
    MEMCPY (&pSsnNode->Timer.SrvAddr, pSrvAddr, sizeof (tIPvXAddr));

    BpCltStartTimer (&pSsnNode->Timer.TimerNode, BPCLT_SESSION_TIMEOUT_VAL);

    return pSsnNode;
}

/**************************************************************************/
/*   Function Name   : BpCltAddSession                                    */
/*   Description     : This functions is used to add the session node in  */
/*                     the Beep server Session list                       */
/*                     Table                                              */
/*                                                                        */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : BPCLT_SUCCESS or BPCLT_FAILURE                     */
/**************************************************************************/

INT4
BpCltAddSession (tBpCltSsnNode * pSrvNode)
{
    TMO_SLL_Add (&(gBpCltGblInfo.SrvSessionList), &pSrvNode->Next);
    return BPCLT_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : BpCltDeInitSession                                    */
/*   Description     : This functions is used to Deinit the given session */
/*                     node                                               */
/*                                                                        */
/*   Input(s)        : pSsnNode  - Pointer to a session node              */
/*   Output(s)       : None                                               */
/*   Return Value    : BPCLT_SUCCESS or BPCLT_FAILURE                     */
/**************************************************************************/

INT4
BpCltDeInitSession (tBpCltSsnNode * pSsnNode)
{
    INT4                i4Count = BEEP_ZERO;
    tBpCltMsg          *pMsgDb = NULL;

    /* Release Channel memory */
    for (i4Count = BEEP_ZERO; i4Count < BPCLT_MAX_PROFILES; i4Count++)
    {
        if (pSsnNode->aBpCltChnlList[i4Count] != NULL)
        {
            MemReleaseMemBlock (BPCLT_CHNL_POOL_ID,
                                (UINT1 *) pSsnNode->aBpCltChnlList[i4Count]);
            pSsnNode->aBpCltChnlList[i4Count] = NULL;
        }
    }

    /* Delete Message Db list */
    while ((pMsgDb = (tBpCltMsg *)
            TMO_SLL_First (&(pSsnNode->BpCltMsgList))) != NULL)
    {
        TMO_SLL_Delete (&(pSsnNode->BpCltMsgList), &pMsgDb->Next);
        MemReleaseMemBlock (BPCLT_SRV_POOL_ID, (UINT1 *) pMsgDb);
    }

    /* Free the read, write and temp buff */
    return BPCLT_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : BpCltDeleteSession                                  */
/*   Description     : This functions is used to add the session node in  */
/*                     the Beep server Session list                       */
/*                     Table                                              */
/*                                                                        */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : BPCLT_SUCCESS or BPCLT_FAILURE                     */
/**************************************************************************/

INT4
BpCltDeleteSession (tBpCltSsnNode * pSrvNode)
{

    BpCltStopTimer (&pSrvNode->Timer.TimerNode);
    TMO_SLL_Delete (&(gBpCltGblInfo.SrvSessionList), &pSrvNode->Next);
    return BPCLT_SUCCESS;

}

/**************************************************************************/
/*   Function Name   : BpCltGetSessionNode                                */
/*   Description     : This functions is used to check whether the session*/
/*                     exists for Beep server */
/*                                                                        */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : Pointer to a session if the session is created     */
/*                     successfully, NULL otherwise                       */
/**************************************************************************/

tBpCltSsnNode      *
BpCltGetSessionNode (tIPvXAddr * pSrvAddr, INT4 i4SockId)
{
    tBpCltSsnNode      *pSrvNode = NULL;
    tBpCltSsnNode      *pTmp = NULL;

    TMO_SLL_Scan (&(gBpCltGblInfo.SrvSessionList), pTmp, tBpCltSsnNode *)
    {
        pSrvNode = pTmp;
        /* If both Server Addr and Sock Id is given, check both */
        if ((pSrvAddr != NULL) && (i4SockId != BEEP_ZERO))
        {
            if ((MEMCMP (&pSrvNode->SrvAddr, pSrvAddr,
                         sizeof (tIPvXAddr)) == BEEP_ZERO)
                && (pSrvNode->i4SockId == i4SockId))
            {
                return pSrvNode;
            }
        }
        else if ((pSrvAddr == NULL) && (i4SockId != BEEP_ZERO))
        {
            if (pSrvNode->i4SockId == i4SockId)
            {
                return pSrvNode;
            }

        }
        else if ((pSrvNode->SrvAddr.u1AddrLen <= IPVX_MAX_INET_ADDR_LEN)
                 && (pSrvAddr != NULL))
        {
            if (MEMCMP (pSrvNode->SrvAddr.au1Addr, pSrvAddr->au1Addr,
                        (INT4) (pSrvNode->SrvAddr.u1AddrLen)) == BEEP_ZERO)
            {
                return pSrvNode;
            }
        }
    }
    return NULL;
}

/**************************************************************************/
/*   Function Name   : BpCltForceCloseSsn                                 */
/*   Description     : This function deinitialises and closes the given   */
/*                     session node entry                                 */
/*   Input(s)        : i4Sock - Sock Fd                                   */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/

VOID
BpCltForceCloseSsn (INT4 i4Sock)
{

    tBpCltSsnNode      *pTmpSsn = NULL;

    SelRemoveFd (i4Sock);
    close (i4Sock);

    pTmpSsn = BpCltGetSessionNode (NULL, i4Sock);
    if (pTmpSsn == NULL)
    {
        BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_FAILURE_TRC, BPCLT_NAME,
                   "DeInit Session DB: Session entry not available");
        return;
    }

    BpCltDeInitSession (pTmpSsn);

    BpCltDeleteSession (pTmpSsn);

    MemReleaseMemBlock (BPCLT_SSN_POOL_ID, (UINT1 *) pTmpSsn);
    return;
}

/**************************************************************************/
/*   Function Name   : BpCltForceCloseAllSsn                              */
/*   Description     : This DeInit all Session DB and removes the session */
/*                     table entry                                        */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/

VOID
BpCltForceCloseAllSsn (VOID)
{
    INT4                i4ChNo = BPCLT_MGMT_CHNL;
    INT4                i4Code = BPCLT_SERVICE_NOT_AVAILABLE;
    tBpCltSsnNode      *pTmpSsn = NULL;

    /* Scanning all the sessions available and close it */
    while ((pTmpSsn = (tBpCltSsnNode *) TMO_SLL_First
            (&(gBpCltGblInfo.SrvSessionList))) != NULL)
    {
        MEMSET (pTmpSsn->ai1WriteBuf, BEEP_ZERO, BPCLT_MAX_BUF_SIZE);
        FormCloseMsg (i4ChNo, i4Code, (UINT1 *) pTmpSsn->ai1WriteBuf);
        BeepAddMimeHdr (pTmpSsn->ai1WriteBuf);
        BeepMakeFrame (BEEP_MSG_FRAME, pTmpSsn->aBpCltChnlList[BPCLT_MGMT_CHNL],
                       pTmpSsn->ai1WriteBuf);
        BpCltSendToServer (pTmpSsn, pTmpSsn->ai1WriteBuf);
        BpCltForceCloseSsn (pTmpSsn->i4SockId);
    }
    return;
}

/**************************************************************************/
/*   Function Name   : BpCltGetRawChnlInSession                           */
/*   Description     : This functions is used to get first RAW channel in */
/*                     given session  session                             */
/*                                                                        */
/*   Input(s)        : pSsnNode                                           */
/*   Output(s)       : None                                               */
/*   Return Value    : pChnlNode if exists, NULL otherwise                */
/**************************************************************************/

tBpCltChnlNode     *
BpCltGetRawChnlInSession (tBpCltSsnNode * pSession)
{
    UINT4               u4Cnt = BEEP_ONE;
    tBpCltChnlNode     *pChnlNode = NULL;

    for (; pSession->aBpCltChnlList[u4Cnt] != NULL; u4Cnt = u4Cnt + BEEP_TWO)
    {
        pChnlNode = pSession->aBpCltChnlList[u4Cnt];

        if (pChnlNode->i4Profile == BEEP_RAW_PROFILE)
        {
            return pChnlNode;
        }
    }
    return NULL;
}

/**************************************************************************/
/*   Function Name   : BpCltServerMsgHdlr                                 */
/*   Description     : This function takes and reads the active session   */
/*                     socket and parse and pass the data to profile hdlrs*/
/*   Input(s)        : pu1Buf - Pointer to the CRU Buf                    */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/
VOID
BpCltServerMsgHdlr (tCRU_BUF_CHAIN_HEADER * pu1Buf)
{
    INT4                i4SockId = BEEP_ZERO;
    INT4                i4EvtFlag = BEEP_ZERO;
    tBpCltFrameHdr      RcvFrameHdr;
    tBpCltSrvMsg       *pMsg = NULL;
    tBpCltSsnNode      *pSsnNode = NULL;
    tBpCltChnlNode     *pChnlNode = NULL;

    MEMSET (&RcvFrameHdr, BEEP_ZERO, sizeof (tBpCltFrameHdr));

    pMsg = (tBpCltSrvMsg *) BPCLT_GET_MODULE_DATA_PTR (pu1Buf);

    i4SockId = pMsg->i4SockId;
    pSsnNode = BpCltGetSessionNode (NULL, i4SockId);
    if (pSsnNode == NULL)
    {
        return;
    }

    /* Receive frame from the socket */
    if (BpCltRcvServerMsg (pMsg->i4SockId, pSsnNode) != BPCLT_SUCCESS)
    {
        BpCltForceCloseSsn (pSsnNode->i4SockId);
        return;
    }

    /* Checking whether full frame is received */
    if (pSsnNode->RxFrame.i1FullFlag != BPCLT_FULL_FRAME)
    {
        return;
    }
    else
    {
        MEMSET (pSsnNode->ai1ReadBuf, BEEP_ZERO, BPCLT_MAX_BUF_SIZE);
        MEMCPY (pSsnNode->ai1ReadBuf, pSsnNode->RxFrame.ai1Frame,
                pSsnNode->RxFrame.i4Size);
        /*   pSsnNode->ai1ReadBuf = pSsnNode->RxFrame.ai1Frame; */
    }

    /* Parse the Frame Header */
    if (BEEP_FAILURE == BeepParseFrameHdr (pSsnNode->ai1ReadBuf, &RcvFrameHdr))
    {
        BpCltForceCloseSsn (pSsnNode->i4SockId);
        return;
    }

    pChnlNode = pSsnNode->aBpCltChnlList[RcvFrameHdr.i4ChnlNum];
    if (BEEP_FAILURE == BeepValidateFrame (&RcvFrameHdr, pChnlNode))
    {
        BpCltForceCloseSsn (pSsnNode->i4SockId);
        return;
    }

    /* Call the Coprresponding Profile Handlers. When new
     * applications needs to use BEEP client, add more cases
     * and their corresponding Profile handler calls here*/

    pChnlNode = BpCltGetChannelNode (pSsnNode, RcvFrameHdr.i4ChnlNum);
    if (pChnlNode == NULL)
    {
        BpCltForceCloseSsn (pSsnNode->i4SockId);
        return;
    }

    switch (pChnlNode->i4Profile)
    {
        case BPCLT_MGMT_CHNL:
            i4EvtFlag = BpCltMgmtChnlHdlr (&RcvFrameHdr, pSsnNode->ai1ReadBuf,
                                           i4SockId);
            break;

        case BPCLT_RAW_CHNL:
            i4EvtFlag = BpCltRawChnlHdlr (&RcvFrameHdr, pSsnNode->ai1ReadBuf,
                                          i4SockId);
            break;

        case BPCLT_MD5_CHNL:
            /*   i4EvtFlag = BpCltMd5ChnlHdlr(&RcvFrameHdr, pSsnNode->ai1ReadBuf, 
               i4SockId); */
            break;

        default:
            break;
    }

    /* Switch on Event variable */
    switch (i4EvtFlag)
    {
        case BPCLT_TCP_ABORT:
        case BPCLT_APP_ABORT:
        case BPCLT_DIGEST_FAIL:
        case BPCLT_UNEXPECTED:
        case BPCLT_TLS_FAIL:
        case BPCLT_OVER:
            BpCltForceCloseSsn (i4SockId);
            return;

/* This can be enabled when support for Digest MD5 profile and TLS profile is provided*/
            /*    case BPCLT_DIGEST_OK:
               pSsnNode->i1Md5Flag = BEEP_ONE;
               break;

               case BPCLT_TLS_OK:
               BpCltHandleTlsOk(); 
               break; */

        default:
            break;

    }
    SelAddFd (i4SockId, BpCltSrvMsgNotify);
    return;
}

/**************************************************************************/
/*   Function Name   : BpCltRcvServerMsg                                  */
/*   Description     : This functions is used to get the frame from the   */
/*                     the given socket                                   */
/*                                                                        */
/*   Input(s)        : i4SockId,pSsnNode                                  */
/*   Output(s)       : None                                               */
/*   Return Value    : BPCLT_SUCCESS / BPCLT_FAILURE                      */
/**************************************************************************/
INT4
BpCltRcvServerMsg (INT4 i4SockId, tBpCltSsnNode * pSsnNode)
{
    INT4                i4RetVal = BEEP_ZERO;
    INT1               *pi1TempBuffer = NULL;
    INT1               *pi1Temp = NULL;
    INT4                i4Size = BEEP_ZERO;
    UINT4               u4Len = 0;
    pSsnNode->RxFrame.i1FullFlag = BPCLT_PARTIAL_FRAME;
    if (pSsnNode->i1TlsFlag == BEEP_ONE)
    {
        pSsnNode->i1TlsFlag = BEEP_ZERO;
    }
    else
    {
        pi1TempBuffer = MemAllocMemBlk (BPCLT_FRAME_POOL_ID);
        if (pi1TempBuffer == NULL)
        {
            return BPCLT_FAILURE;
        }
        MEMSET (pi1TempBuffer, BEEP_ZERO, BEEP_MAX_BUFF_SIZE);
        /* Receive data from the tcp socket */
        i4RetVal =
            recv (i4SockId, pi1TempBuffer, BEEP_MAX_BUFF_SIZE, BEEP_ZERO);

        if (i4RetVal == BEEP_MINUS_ONE)
        {
            if (errno != EWOULDBLOCK)
            {
                MemReleaseMemBlock (BPCLT_FRAME_POOL_ID,
                                    (UINT1 *) pi1TempBuffer);
                return BPCLT_FAILURE;
            }
        }
        if (i4RetVal == BEEP_ZERO)
        {
            if (errno == EWOULDBLOCK)
            {
                MemReleaseMemBlock (BPCLT_FRAME_POOL_ID,
                                    (UINT1 *) pi1TempBuffer);
                return BPCLT_TCP_ABORT;
            }
        }

        /* Append to temp buff */
        u4Len = (STRLEN (pi1TempBuffer) < sizeof (pSsnNode->ai1TempBuf)) ?
            STRLEN (pi1TempBuffer) : (sizeof (pSsnNode->ai1TempBuf)
                                      - STRLEN (pSsnNode->ai1TempBuf));

        STRNCAT (pSsnNode->ai1TempBuf, pi1TempBuffer, u4Len);
        pSsnNode->ai1TempBuf[sizeof (pSsnNode->ai1TempBuf) - 1] = '\0';

        MemReleaseMemBlock (BPCLT_FRAME_POOL_ID, (UINT1 *) pi1TempBuffer);

        if (STRLEN (pSsnNode->ai1TempBuf) < BEEP_THREE)
        {
            return BPCLT_SUCCESS;
        }

        /* Check if header received is proper */
        if ((!(MEMCMP (pSsnNode->ai1TempBuf, "MSG", BEEP_THREE))) ||
            (!(MEMCMP (pSsnNode->ai1TempBuf, "RPY", BEEP_THREE))) ||
            (!(MEMCMP (pSsnNode->ai1TempBuf, "ERR", BEEP_THREE))) ||
            (!(MEMCMP (pSsnNode->ai1TempBuf, "ANS", BEEP_THREE))) ||
            (!(MEMCMP (pSsnNode->ai1TempBuf, "SEQ", BEEP_THREE))))
        {
            if (!(MEMCMP (pSsnNode->ai1TempBuf, "SEQ", BEEP_THREE)))
            {
                pi1Temp = (INT1 *) STRSTR (pSsnNode->ai1TempBuf, "\r\n");
                if (pi1Temp != NULL)
                {
                    pi1Temp += BEEP_TWO;
                    memmove (pSsnNode->ai1TempBuf, pi1Temp,
                             STRLEN (pSsnNode->ai1TempBuf) -
                             (pi1Temp - pSsnNode->ai1TempBuf) + BEEP_ONE);
                }
                if (STRLEN (pSsnNode->ai1TempBuf) != BEEP_ZERO)
                {
                    BpCltSrvMsgNotify (i4SockId);
                    return BPCLT_SUCCESS;
                }

            }
        }
        else
        {
            return BPCLT_FAILURE;
        }
        /* Check if a full frame is received */
        pi1Temp = (INT1 *) STRSTR (pSsnNode->ai1TempBuf, "END\r\n");

        if (pi1Temp != NULL)
        {
            i4Size = STRLEN ("END\r\n");
            i4RetVal = pi1Temp - pSsnNode->ai1TempBuf;

            MEMSET (pSsnNode->RxFrame.ai1Frame, BEEP_ZERO, BPCLT_MAX_BUF_SIZE);
            MEMCPY (pSsnNode->RxFrame.ai1Frame, pSsnNode->ai1TempBuf,
                    i4RetVal + i4Size);
            pSsnNode->RxFrame.ai1Frame[i4RetVal + i4Size] = '\0';
            pSsnNode->RxFrame.i1FullFlag = BPCLT_FULL_FRAME;
            pSsnNode->RxFrame.i4Size = STRLEN (pSsnNode->RxFrame.ai1Frame);

            memmove (pSsnNode->ai1TempBuf,
                     pSsnNode->ai1TempBuf + i4RetVal + i4Size,
                     (sizeof (pSsnNode->ai1TempBuf) - (i4RetVal + i4Size)));
        }

    }

    if (STRLEN (pSsnNode->ai1TempBuf) != BEEP_ZERO)
    {
        BpCltSrvMsgNotify (i4SockId);
    }
    return BPCLT_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : BpCltTlsChnlHdlr                                   */
/*   Description     : TLS Channel Handler                                */
/*                                                                        */
/*   Input(s)        : i4SockId, pRcvFrameHdr, pi1Frame                   */
/*   Output(s)       : None                                               */
/*   Return Value    : BPCLT_SUCCESS / BPCLT_FAILURE                      */
/**************************************************************************/
/*
INT4 BpCltTlsChnlHdlr(tBpCltFrameHdr *pRcvFrameHdr, INT1 *pi1Frame,
                      INT4 i4SockId)
{
    UNUSED_PARAM(pRcvFrameHdr);
    UNUSED_PARAM(pi1Frame);
    UNUSED_PARAM(i4SockId);

}
*/

/**************************************************************************/
/*   Function Name   : BpCltSendGreetings                                 */
/*   Description     : This function is used to send greetings            */
/*                                                                        */
/*   Input(s)        : pSsnNode                                           */
/*   Output(s)       : None                                               */
/*   Return Value    : BPCLT_SUCCESS / BPCLT_FAILURE                      */
/**************************************************************************/

INT4
BpCltSendGreetings (tBpCltSsnNode * pSsnNode)
{
    FormClntGreetingMsg ((UINT1 *) pSsnNode->ai1WriteBuf);
    BeepAddMimeHdr (pSsnNode->ai1WriteBuf);
    pSsnNode->aBpCltChnlList[BEEP_ZERO]->i4LastMsg = BEEP_ZERO;
    BeepMakeFrame (BEEP_RPY_FRAME, pSsnNode->aBpCltChnlList[BEEP_ZERO],
                   pSsnNode->ai1WriteBuf);
    return (BpCltSendToServer (pSsnNode, pSsnNode->ai1WriteBuf));
}

/**************************************************************************/
/*   Function Name   : BpCltValidataFrame                                 */
/*   Description     : This function is used to validate frame header     */
/*                                                                        */
/*   Input(s)        : pFrameHdr, pChnlNode                               */
/*   Output(s)       : None                                               */
/*   Return Value    : BPCLT_SUCCESS / BPCLT_FAILURE                      */
/**************************************************************************/

INT4
BpCltValidateFrame (tBpCltFrameHdr * pFrameHdr, tBeepChnlDb * pChnlNode)
{
    UNUSED_PARAM (pFrameHdr);
    UNUSED_PARAM (pChnlNode);
    return BPCLT_SUCCESS;
}
