/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: bpclmain.c,v 1.12 2014/03/03 12:13:00 siva Exp $
 * 
 * Description: This file contains the Init, Memory allocation, Task
 *                 creation, Queue creation for the Beep Client Module
 ********************************************************************/

#include "bpcltinc.h"
#include "fssocket.h"
#include "fssnmp.h"

#ifndef BPCLT_MAIN_C
#define BPCLT_MAIN_C

UINT4               gu4BpCltGblTrc;
tBpCltGbl           gBpCltGblInfo;

/**************************************************************************/
/*   Function Name   : BpCltTaskInit                                      */
/*   Description     : This function creates semaphore, creates mempools  */
/*                     initializes the link list for storing the messages */
/*                     created the timer list for session ageout          */
/*                                                                        */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : BPCLT_SUCCESS or BPCLT_FAILURE                     */
/**************************************************************************/

INT4
BpCltTaskInit (VOID)
{
    UINT4               u4RetVal;
    /* Create the semaphore identifier to access the Beep datastructures */
    if (OsixSemCrt (BPCLT_SEM_NAME, &(BPCLT_SEM_ID)) != OSIX_SUCCESS)
    {
        return BPCLT_FAILURE;
    }

    OsixSemGive (BPCLT_SEM_ID);

    /* Beep Cmn Mempool creation */
    if (BeepCmnInit () == BEEP_FAILURE)
    {
        return BPCLT_FAILURE;
    }

    if (Beep_clientSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        return BPCLT_FAILURE;
    }

    BPCLT_SSN_POOL_ID = BEEP_CLIENTMemPoolIds[BPCLT_MAX_SSN_NODE_SIZING_ID];
    BPCLT_CHNL_POOL_ID = BEEP_CLIENTMemPoolIds[BPCLT_MAX_CHNL_NODE_SIZING_ID];
    BPCLT_SYSLOG_POOL_ID =
        BEEP_CLIENTMemPoolIds[BPCLT_MAX_SYSLOG_MSG_SIZING_ID];
    BPCLT_SRV_POOL_ID = BEEP_CLIENTMemPoolIds[BPCLT_MAX_MSG_SIZING_ID];
    BPCLT_PENDING_LOG_POOL_ID =
        BEEP_CLIENTMemPoolIds[BPCLT_MAX_PENDING_MSG_SIZING_ID];
    BPCLT_MGMT_CHNL_POOL_ID =
        BEEP_CLIENTMemPoolIds[BPCLT_MAX_MGMT_CHNL_NODE_SIZING_ID];
    BPCLT_FRAME_POOL_ID =
        BEEP_CLIENTMemPoolIds[BPCLT_MAX_FRAME_BLOCKS_SIZING_ID];
    BPCLT_SYSLOG_MSG_POOL_ID =
        BEEP_CLIENTMemPoolIds[BPCLT_MAX_MSG_BLOCKS_SIZING_ID];

    /* Initializing Global Variables */
    gBpCltGblInfo.i4BpCltRawFlg = BPCLT_ENABLE;
    gBpCltGblInfo.i4BpCltMd5Flg = BPCLT_DISABLE;
    gBpCltGblInfo.i4BpCltTlsFlg = BPCLT_DISABLE;

    TMO_SLL_Init (&gBpCltGblInfo.SrvSessionList);
    TMO_SLL_Init (&gBpCltGblInfo.AuthKeyList);

    /* Create the timerlist for server sessions */
    u4RetVal =
        BPCLT_CREATE_TMR_LIST ((const UINT1 *) BPCLT_TASK,
                               BPCLT_SESSION_TMR_EVENT, &BPCLT_SSN_TIMER_ID);
    UNUSED_PARAM (u4RetVal);
    return BPCLT_SUCCESS;
}

/****************************************************************************
 Function    :  BpCltDelMemInit
 Description :  Deletes the Allocated memory pools.
 Input       :  None.
 Output      :  Deletes the Allocates Memory Pools.
 Returns     :  None.
****************************************************************************/
VOID
BpCltDelMemInit ()
{
    /* Delete the pools which are valid */
    Beep_clientSizingMemDeleteMemPools ();

}

/****************************************************************************
 Function    :  BpCltShutdown
 Description :  Deletes the Allocated memory pools, Removes the timer list
                removes the Queue, and deletes the Beep task.
 Input       :  None.
 Output      :  Deletes the Allocates Memory Pools.
 Returns     :  None.
****************************************************************************/
VOID
BpCltShutdown ()
{

    BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_OS_RESOURCE_TRC, BPCLT_MOD_NAME,
               "Deleting BeepClt Mempools \n");

    BpCltDelMemInit ();

    /* Delete the Timer List */
    if (BPCLT_SSN_TIMER_ID != BEEP_ZERO)
    {
        if (TmrDeleteTimerList (BPCLT_SSN_TIMER_ID) != BEEP_ZERO)
        {
            BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_OS_RESOURCE_TRC,
                       BPCLT_MOD_NAME, "Deleting Beep client timer list \n");
        }
    }

    BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_OS_RESOURCE_TRC, BPCLT_MOD_NAME,
               "Deleting BeepClt Queues \n");

    /* Remove the Queues allocated for Beep */
    OsixQueDel (BPCLT_SYSLOG_MSG_Q);
    OsixQueDel (BPCLT_SRV_MSG_Q);

    BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_OS_RESOURCE_TRC, BPCLT_MOD_NAME,
               "Deleting BeepClt Semaphore\n");

    /* Delete the semaphore */
    OsixSemDel (BPCLT_SEM_ID);

    BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_OS_RESOURCE_TRC, BPCLT_MOD_NAME,
               "Deleting BeepClt Task\n");
    OsixTskDel (BPCLT_TASK_ID);
}

/*-------------------------------------------------------------------+
 * Function           : BpCltTaskMain
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : None
 * Action             : This function initializes the global data structures
 *                      of the Beep Client Module. This function also waits 
 *                      for the messages from the syslog protocols.
-------------------------------------------------------------------*/
VOID
BpCltTaskMain (INT1 *pi1TaskParam)
{
    INT4                i4SysLogId = BEEP_ZERO;
    UINT4               u4EventReceived = BEEP_ZERO;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UNUSED_PARAM (pi1TaskParam);

    if (OsixTskIdSelf (&gBpCltGblInfo.BpCltTaskId) == OSIX_FAILURE)
    {
        /* Indicate the status of initialization to the main routine */
        BPCLT_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Create a Q to send Syslog Msg to Beep Client */
    if (OsixQueCrt ((UINT1 *) BPCLT_SYSLOG_Q, OSIX_MAX_Q_MSG_LEN,
                    BPCLT_Q_DEPTH, &gBpCltGblInfo.BpCltSyslogQId)
        != OSIX_SUCCESS)
    {
        BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC, BPCLT_NAME,
                   "Unable to create Beep Client Queue \n");
        OsixTskDel (gBpCltGblInfo.BpCltTaskId);
        /* Indicate the status of initialization to the main routine */
        BPCLT_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Create a Q to send Beep server  Msg to Beep Client */
    if (OsixQueCrt ((UINT1 *) BPCLT_SRV_Q, OSIX_MAX_Q_MSG_LEN,
                    BPCLT_Q_DEPTH, &gBpCltGblInfo.BpCltSrvQId) != OSIX_SUCCESS)
    {
        BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC, BPCLT_NAME,
                   "Unable to create Beep Client Queue \n");
        OsixQueDel (gBpCltGblInfo.BpCltSyslogQId);
        OsixTskDel (gBpCltGblInfo.BpCltTaskId);
        /* Indicate the status of initialization to the main routine */
        BPCLT_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (BpCltTaskInit () != BPCLT_SUCCESS)
    {
        OsixQueDel (gBpCltGblInfo.BpCltSrvQId);
        OsixQueDel (gBpCltGblInfo.BpCltSyslogQId);
        OsixTskDel (gBpCltGblInfo.BpCltTaskId);
        /* Indicate the status of initialization to the main routine */
        BPCLT_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    BPCLT_INIT_COMPLETE (OSIX_SUCCESS);
#ifdef SYSLOG_WANTED
    /* Register with SysLog Server to Log Error Messages. */
    i4SysLogId =
        SYS_LOG_REGISTER ((CONST UINT1 *) "BEEP_CLT", SYSLOG_ALERT_LEVEL);

    if (i4SysLogId < 0)
    {
        /* Indicate the status of initialization to the main routine */
        BPCLT_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    BPCLT_ID = i4SysLogId;
#else
    UNUSED_PARAM (i4SysLogId);
#endif

    while (BEEP_ONE)
    {
        u4EventReceived = BEEP_ZERO;
        /* Wait and receive for the event */
        OsixEvtRecv (gBpCltGblInfo.BpCltTaskId,
                     (BPCLT_SYSLOG_PKT_RCV_EVENT |
                      BPCLT_SRV_PKT_RCV_EVENT |
                      BPCLT_SESSION_TMR_EVENT),
                     BPCLT_EVENT_WAIT_FLAG, &u4EventReceived);

        if (u4EventReceived & BPCLT_SYSLOG_PKT_RCV_EVENT)
        {
            /* Syslog message received from Syslog device */
            BpCltSyslogMsgHdlr ();
        }
        else if (u4EventReceived & BPCLT_SRV_PKT_RCV_EVENT)
        {
            /* Received message from Beep server  */
            while (OsixQueRecv (BPCLT_SRV_MSG_Q, (UINT1 *) &pBuf,
                                OSIX_DEF_MSG_LEN, BEEP_ZERO) == OSIX_SUCCESS)
            {
                BpCltServerMsgHdlr (pBuf);
                CRU_BUF_Release_MsgBufChain (pBuf, BEEP_ZERO);
            }
        }
        else if (u4EventReceived & BPCLT_SESSION_TMR_EVENT)
        {
            /* Process the Server session expiry timer event */
            BpCltSsnTimeoutHdlr ();
        }
    }
}

/**************************************************************************/
/*   Function Name   : BpCltSyslogMsgHandler                              */
/*   Description     : This function processes the syslog Messages        */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : NONE                                               */
/**************************************************************************/
VOID
BpCltSyslogMsgHdlr (VOID)
{
    tBpCltSyslogMsg    *pHdr = NULL;

    while (OsixQueRecv (BPCLT_SYSLOG_MSG_Q, (UINT1 *) &pHdr,
                        OSIX_DEF_MSG_LEN, BEEP_ZERO) == OSIX_SUCCESS)
    {
        switch (pHdr->u4MsgType)
        {
            case SYSLOG_MSG_EVENT:

                BpCltProcessSyslogMsg (pHdr);

            case BPCLT_SYSLOG_CFG_CHG:

                BpCltProcessSyslogCfgChg (pHdr);

            default:
                break;
        }
        MemReleaseMemBlock (BPCLT_SYSLOG_POOL_ID, (UINT1 *) pHdr);
    }
}

/**************************************************************************/
/*   Function Name   : BpCltSrvMsgNotify                                  */
/*   Description     : This is the call back function for SelAddFd        */
/*                     This function sends the BPCLT_SRV_PKT_RCVD event  */
/*                     to Beep client main task                           */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : BEEP_SUCCESS or BEEP_FAILURE                       */
/**************************************************************************/
VOID
BpCltSrvMsgNotify (INT4 i4SockId)
{
    tBpCltSrvMsg       *pSrvMsg = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuffer = NULL;

    pBuffer = CRU_BUF_Allocate_MsgBufChain (sizeof (tBpCltSrvMsg), 0);

    /* Allocation Fails */
    if (pBuffer == NULL)
    {
        BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC, BPCLT_NAME,
                   "Unable to allocate memory to receive server msg \n");
        return;
    }

    pSrvMsg = (tBpCltSrvMsg *) BPCLT_GET_MODULE_DATA_PTR (pBuffer);
    pSrvMsg->i4SockId = i4SockId;

    /* post to Beep Client Server Msg queue */
    if (OsixQueSend (BPCLT_SRV_MSG_Q, (UINT1 *) &pBuffer,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC, BPCLT_NAME,
                   "Sending Server Msg to Queue failed\n");
        return;
    }

    OsixEvtSend (BPCLT_TASK_ID, BPCLT_SRV_PKT_RCV_EVENT);
    return;
}

/**************************************************************************/
/*   Function Name   : BeepProcessSyslogMsg                               */
/*   Description     : This function processes the syslog Messages        */
/*                     This API is called from Syslog. This function      */
/*                     posts an event BPCLT_SYSLOG_EVENT to Beep Client   */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : BPCLT_SUCCESS or BPCLT_FAILURE                     */
/**************************************************************************/
INT4
BpCltProcessSyslogMsg (tBpCltSyslogMsg * pBuf)
{
    UINT1               au1AuthKey[BPCLT_MAX_AUTHKEY_SIZE];
    CHR1               *pc1SysMsg = NULL;
    INT4                i4SockId = BEEP_ZERO;
    INT4                i4Status = BPCLT_FAILURE;
    tBpCltPendingMsg   *pPendingMsg = NULL;
    tBpCltSyslogMsg    *pSyslogInfo = NULL;
    tBpCltSsnNode      *pSsnNode = NULL;
    tBpCltChnlNode     *pChnlNode = NULL;

    pSyslogInfo = pBuf;

    MEMSET (au1AuthKey, BEEP_ZERO, BPCLT_MAX_AUTHKEY_SIZE);

    /* Check if the session already existing for the given server */
    pSsnNode = BpCltGetSessionNode (&pSyslogInfo->SrvAddr, BEEP_ZERO);

    if (pSsnNode != NULL)        /* Already session exists */
    {
        BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC, BPCLT_MOD_NAME,
                   "Channel not yet Opened to send the syslog msg  !!");

        /* Get the session node and RAW channel in that session */
        if (pSsnNode->i4SessionActive != BPCLT_FLG_ACTIVE)
        {
            pc1SysMsg = (CHR1 *) MemAllocMemBlk (BPCLT_SYSLOG_MSG_POOL_ID);
            if (pc1SysMsg == NULL)
            {
                return BPCLT_FAILURE;
            }
            MEMSET (pc1SysMsg, BEEP_ZERO, BPCLT_MAX_SYSLOG_MSG_LEN);
            BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC, BPCLT_MOD_NAME,
                       "Closing the channel since the timer expires ");
            MEMCPY (pc1SysMsg, pBuf, STRLEN (pBuf));
            SYS_LOG_MSG ((BEEP_ERROR, BPCLT_ID, pc1SysMsg));
            MemReleaseMemBlock (BPCLT_SYSLOG_POOL_ID, (UINT1 *) pBuf);
            MemReleaseMemBlock (BPCLT_SYSLOG_MSG_POOL_ID, (UINT1 *) pc1SysMsg);
            return BPCLT_FAILURE;
        }

        pChnlNode = BpCltGetRawChnlInSession (pSsnNode);

        if (pChnlNode == NULL)
        {
            BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC, BPCLT_MOD_NAME,
                       "Channel Node doesnot existto  send the syslog msg  !!");
            MemReleaseMemBlock (BPCLT_SYSLOG_POOL_ID, (UINT1 *) pBuf);
            return BPCLT_FAILURE;
        }
        /* Check the received message is Dummy message and the RAW channel
         * profile state is channel opened */
        if (pChnlNode->i4ProfileState == BPCLT_CHNL_OPENED)
        {
            /* Store the log message in Pending Log Q */
            if ((pPendingMsg =
                 (tBpCltPendingMsg *)
                 MemAllocMemBlk (BPCLT_PENDING_LOG_POOL_ID)) == NULL)
            {
                BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC, BPCLT_NAME,
                           "Memory allocation for Syslog Msg failed\n");
                MemReleaseMemBlock (BPCLT_SYSLOG_POOL_ID, (UINT1 *) pBuf);
                return BPCLT_FAILURE;
            }
            MEMSET (pPendingMsg->au1LogMsg, BEEP_ZERO,
                    sizeof (pPendingMsg->au1LogMsg));

            MEMCPY (pPendingMsg->au1LogMsg, pBuf->au1LogMsg,
                    STRLEN (pBuf->au1LogMsg));
            TMO_SLL_Add (&pSsnNode->BpCltPendingLogList, &pPendingMsg->Next);

            BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC, BPCLT_MOD_NAME,
                       "Channel not yet Opened to send the syslog msg  !!");
            return BPCLT_SUCCESS;
        }
        if (pChnlNode->i4ProfileState == BPCLT_ANS_SENT)
        {
            /* send the syslog log message in ANS frame */
            if (BPCLT_SUCCESS !=
                BpCltSendlogMessage (pSsnNode, pChnlNode, pBuf->au1LogMsg))
            {
                BpCltForceCloseSsn (pSsnNode->i4SockId);
            }
            /* Free the pMsg */
            MemReleaseMemBlock (BPCLT_SYSLOG_POOL_ID, (UINT1 *) pBuf);
            return BPCLT_SUCCESS;
        }
    }
    else
    {
        /* New server session creation */
        /* Check the max session limit */
        i4Status = TMO_SLL_Count (&(gBpCltGblInfo.SrvSessionList));
        if (i4Status < BEEP_MAX_SESSIONS)
        {
            i4Status = BEEP_TRUE;
        }
        else
        {
            i4Status = BEEP_FALSE;
        }

        if (i4Status == BEEP_FALSE)
        {
            MemReleaseMemBlock (BPCLT_SYSLOG_POOL_ID, (UINT1 *) pBuf);
            return BPCLT_FAILURE;
        }

        /* Check if Authentication required and check Auth key os present  
         * * for Authemtication */
        MEMSET (au1AuthKey, BEEP_ZERO, BPCLT_MAX_AUTHKEY_SIZE);
        pSyslogInfo = pBuf;
        if (gBpCltGblInfo.i4BpCltMd5Flg == BEEP_TRUE)
        {
            if (BpCltGetAuthKey (&pSyslogInfo->SrvAddr, au1AuthKey) ==
                BPCLT_FAILURE)
            {
                BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC, BPCLT_MOD_NAME,
                           "Auth Key not present to send the syslog msg  !!");
                MemReleaseMemBlock (BPCLT_SYSLOG_POOL_ID, (UINT1 *) pBuf);
                return BPCLT_FAILURE;
            }
        }

        /* Create and initialise the socket */
        if (BpCltSockInit
            (pSyslogInfo->i4Port, &pSyslogInfo->SrvAddr,
             &i4SockId) == BPCLT_FAILURE)
        {
            BPCLT_TRC_ARG1 (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC, BPCLT_MOD_NAME,
                            "Socket creation failed for the port %d !!",
                            pSyslogInfo->i4Port);

            MemReleaseMemBlock (BPCLT_SYSLOG_POOL_ID, (UINT1 *) pBuf);
            return BPCLT_FAILURE;
        }
        /* Create a beep session to communicate wih server */
        pSsnNode = BpCltCreateSession (pSyslogInfo->i4Port,
                                       &pSyslogInfo->SrvAddr);
        if (pSsnNode == NULL)
        {
            /* Log the failure using syslog demon */
            /* SysLogMsg(BEEP_ERROR,BPCLT_ID,"BEEP Client: Cannot Log the "
             * * "message: ",);*/

            MemReleaseMemBlock (BPCLT_SYSLOG_POOL_ID, (UINT1 *) pBuf);
            return BPCLT_FAILURE;
        }
        if (pSyslogInfo->i4Profile == RAW_PROFILE)
        {
            pSsnNode->u4ProfileBitMask |= BPCLT_RAW_REQUIRED;
        }
        pSsnNode->i4SockId = i4SockId;

        /* Store the log message in Pending Log Q */
        if ((pPendingMsg =
             (tBpCltPendingMsg *) MemAllocMemBlk (BPCLT_PENDING_LOG_POOL_ID)) ==
            NULL)
        {
            BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC, BPCLT_NAME,
                       "Memory allocation for Syslog Msg failed\n");
            BpCltForceCloseSsn (i4SockId);

            MemReleaseMemBlock (BPCLT_SYSLOG_POOL_ID, (UINT1 *) pBuf);
            return BPCLT_FAILURE;
        }

        MEMSET (pPendingMsg->au1LogMsg, BEEP_ZERO,
                sizeof (pPendingMsg->au1LogMsg));
        MEMCPY (pPendingMsg->au1LogMsg, pBuf->au1LogMsg,
                STRLEN (pBuf->au1LogMsg));

        TMO_SLL_Add (&pSsnNode->BpCltPendingLogList, &pPendingMsg->Next);

        /* Send Greetings to Beep Server */
        if (BpCltSendGreetings (pSsnNode) == BPCLT_SUCCESS)
        {
            /* Set the Mgmt Channel, Profile State as BPCLT_GREETINGS_SENT */
            pChnlNode = pSsnNode->aBpCltChnlList[BEEP_ZERO];

            pChnlNode->i4ProfileState = BPCLT_GREETINGS_SENT;

            /* Add the socket to SelAddFd to get packet from server */
            SelAddFd (pSsnNode->i4SockId, BpCltSrvMsgNotify);
        }
        else
        {
            BpCltForceCloseSsn (i4SockId);
        }
    }
    return BPCLT_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : BpCltSyslogMsgNotify                               */
/*   Description     : This is the call back function registered with     */
/*                     syslog to receive syslog messages to Beep. It posts*/
/*                     an event BPCLT_SYSLOG_PKT_RCVD to Beep Client      */
/*   Input(s)        : pSyslogMsg, pu1Msg                                 */
/*   Output(s)       : None                                               */
/*   Return Value    : SYSLOG_FAILURE or SYSLOG_SUCCESS                   */
/**************************************************************************/
INT4
BpCltSyslogMsgNotify (tBpCltSyslogMsg * pSyslogMsg, UINT1 *pu1Msg)
{
    tBpCltSyslogMsg    *pHdr = NULL;

    if ((pSyslogMsg->i4Profile == RAW_PROFILE) &&
        (gBpCltGblInfo.i4BpCltRawFlg != BPCLT_ENABLE))
    {
        return BPCLT_FAILURE;
    }
    pHdr = (tBpCltSyslogMsg *) MemAllocMemBlk (BPCLT_SYSLOG_POOL_ID);

    if (pHdr == NULL)
    {
        return BPCLT_FAILURE;
    }
    MEMCPY (pHdr, pSyslogMsg, sizeof (tBpCltSyslogMsg));
    STRNCPY (pHdr->au1LogMsg, pu1Msg, sizeof (pHdr->au1LogMsg));

    /* EnQue the Data to syslog message Queue and trigger 
     * and event to main task*/
    if (OsixQueSend (BPCLT_SYSLOG_MSG_Q, (UINT1 *) &pHdr,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (BPCLT_SYSLOG_POOL_ID, (UINT1 *) pHdr);
        BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC, BPCLT_NAME,
                   "Sending Syslog Msg to Queue failed\n");
        return (BPCLT_FAILURE);
    }

    OsixEvtSend (BPCLT_TASK_ID, BPCLT_SYSLOG_PKT_RCV_EVENT);
    return (BPCLT_SUCCESS);
}

/**************************************************************************/
/*   Function Name   : BpCltLock                                          */
/*   Description     : This function takes the Beep Client  protocol      */
/*                     semaphore                                          */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : BPCLT_SUCCESS or BPCLT_FAILURE                     */
/**************************************************************************/
INT4
BpCltLock (VOID)
{
    INT4                i4RetVal = SNMP_FAILURE;

    if (OsixSemTake (gBpCltGblInfo.BpCltSemId) != OSIX_FAILURE)
    {
        i4RetVal = SNMP_SUCCESS;
    }
    else
    {
        BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC, BPCLT_NAME,
                   "Beep Client Semaphore creation failed.\n");
    }
    return i4RetVal;
}

/**************************************************************************/
/*   Function Name   : BpCltUnLock                                        */
/*   Description     : This function releases the Bp Client protocol lock */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : BPCLT_SUCCESS or BPCLT_FAILURE                     */
/**************************************************************************/
INT4
BpCltUnLock (VOID)
{
    OsixSemGive (gBpCltGblInfo.BpCltSemId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 * DESCRIPTION : This function is called whenever a Timer specified by
 *               TimerID needs to be started for the specified duration.
 *
 * INPUTS      : ID of the timer which needs to be started (u1_Timerid),
 *               Reference parameter which will be used on processing
 *               the timer expiry (u4_Ref),
 *               Duration of the Timer (u4_Duration).
 *
 * OUTPUTS     : None.
 *
 * RETURNS     : BPCLT_SUCCESS if the operation is successful,
 *               BPCLT_FAILURE if it fails.
 *
 * NOTE        : This function needs to be changed while porting.
 *
 *****************************************************************************/

INT4
BpCltStartTimer (tTmrAppTimer * pAppTimer, UINT4 u4_Duration)
{

    u4_Duration *= SYS_NUM_OF_TIME_UNITS_IN_A_SEC;
    if (TmrStartTimer (BPCLT_SSN_TIMER_ID,    /* Timer List Id */
                       pAppTimer,    /* reference */
                       u4_Duration) == TMR_FAILURE)
    {
        return (BPCLT_FAILURE);
    }

    return (BPCLT_SUCCESS);
}

/****************************************************************************
 * DESCRIPTION : This function is called to stop the specified timer.
 *
 * INPUTS      : ID of the timer which needs to be stopped (u1_Timerid),
 *
 * OUTPUTS     : None.
 *
 * RETURNS     : BPCLT_SUCCESS if the operation is successful,
 *               BPCLT_FAILURE if it fails.
 *
 * NOTE        : This function needs to be changed while porting.
 *
 *****************************************************************************/
INT4
BpCltStopTimer (tTmrAppTimer * pAppTimer)
{
    if (TmrStopTimer (BPCLT_SSN_TIMER_ID, pAppTimer) == TMR_FAILURE)
    {
        return (BPCLT_FAILURE);
    }
    return (BPCLT_SUCCESS);
}

/**************************************************************************/
/*   Function Name   : BpCltSsnTimeoutHdlr                                */
/*   Description     : This function takes the Beep Client  protocol      */
/*                     semaphore                                          */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/
VOID
BpCltSsnTimeoutHdlr (VOID)
{
    UINT4               u4Cnt = BEEP_ZERO;
    tTmrAppTimer       *pBpCltTimer = NULL;
    tBpCltSsnNode      *pSsnNode = NULL;
    tBpCltChnlNode     *pChnlNode = NULL;

    pBpCltTimer = TmrGetNextExpiredTimer (BPCLT_SSN_TIMER_ID);

    while (pBpCltTimer != NULL)
    {
        pSsnNode = BPCLT_GET_ADDR_INFO_FROM_TIMER (pBpCltTimer, tBpCltSsnNode);

        /* Send NUL frame to all the RAW channels in this session */

        /* NOTE:  for new application profile, Timeout handler should be 
         * updated as did for RAW profile */

        for (u4Cnt = BEEP_ONE; pSsnNode->aBpCltChnlList[u4Cnt] != NULL;
             u4Cnt = u4Cnt + BEEP_TWO)
        {
            pChnlNode = pSsnNode->aBpCltChnlList[u4Cnt];

            if (pChnlNode->i4Profile == BPCLT_RAW_CHNL)
            {
                MEMSET (pSsnNode->ai1WriteBuf, BEEP_ZERO, BPCLT_MAX_BUF_SIZE);
                BeepMakeFrame (BEEP_NUL_FRAME, pChnlNode,
                               pSsnNode->ai1WriteBuf);
                if (BPCLT_SUCCESS ==
                    BpCltSendToServer (pSsnNode, pSsnNode->ai1WriteBuf))
                {
                    pChnlNode->i4ProfileState = BPCLT_NUL_SENT;
                    pSsnNode->i4SessionActive = BPCLT_FLG_NOT_ACTIVE;
                    continue;
                }
                else
                {
                    BpCltForceCloseSsn (pSsnNode->i4SockId);
                    continue;
                }

            }
        }
        pBpCltTimer = TmrGetNextExpiredTimer (BPCLT_SSN_TIMER_ID);
    }
    /* Send NUL Message for RAW and MD5 channel */
}

/**************************************************************************/
/*   Function Name   : BpCltProcessSyslogCfgChg                           */
/*   Description     : This function  process the syslog configuration    */
/*                     change event                                       */
/*   Input(s)        : pBuf                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/

VOID
BpCltProcessSyslogCfgChg (tBpCltSyslogMsg * pBuf)
{
    UNUSED_PARAM (pBuf);
    return;
}

/**************************************************************************/
/*   Function Name   : BpCltSendlogMessage                                */
/*   Description     : This function makes the ANS frame and send it      */
/*                     to the Beep Server                                 */
/*   Input(s)        : pSsnNode,pChnl,pu1Buf                              */
/*   Output(s)       : None                                               */
/*   Return Value    : Return Value from BpCltSendToServer                */
/**************************************************************************/

INT4
BpCltSendlogMessage (tBpCltSsnNode * pSsnNode,
                     tBeepChnlDb * pChnl, UINT1 *pu1Buf)
{
    STRNCPY (pSsnNode->ai1WriteBuf, pu1Buf, sizeof (pSsnNode->ai1WriteBuf));
    BeepMakeFrame (BEEP_ANS_FRAME, pChnl, pSsnNode->ai1WriteBuf);
    return (BpCltSendToServer (pSsnNode, pSsnNode->ai1WriteBuf));
}

#endif
