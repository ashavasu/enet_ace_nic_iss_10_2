/********************************************************************
 *
 * $Id: clchprse.c,v 1.2 2014/01/25 13:55:32 siva Exp $
 * Description: File contains channel management parser
 *              for beep client
 *
 *******************************************************************/
#include "bpcltinc.h"

/******************************************************************************
 * Function           : BeepCltParseChMgmtData
 * Input(s)           : Frame Payload
 * Output(s)          : Data Structure with payload details
 * Returns            : BPCLT_SUCCESS/BPCLT_FAILURE
 * Action             : Routine to parse the frame paylod and extract details
 *                      in the payload received
 ******************************************************************************/

INT4
BpCltParseMgmtChnlData (tBpCltMgmtChnlData * pFrameData, INT1 *pi1Frame)
{

    if (BpCltRmvMimeHdr (pi1Frame) == BPCLT_SUCCESS)
    {
        BpCltMvSpcAndLines (pi1Frame);
    }
    else
    {
        return BPCLT_FAILURE;
    }

    switch (BpCltFindKeyEltType (pi1Frame))
    {
        case BPCLT_ERR_TYPE:
            pFrameData->i4KeyElementType = BPCLT_ERR_TYPE;
            BpCltFillErrDetails (pi1Frame, pFrameData);
            break;

        case BPCLT_GREETING_TYPE:
            pFrameData->i4KeyElementType = BPCLT_GREETING_TYPE;
            BpCltFillGrtDetails (pi1Frame, pFrameData);
            break;

        case BPCLT_PROFILE_TYPE:
            pFrameData->i4KeyElementType = BPCLT_PROFILE_TYPE;
            BpCltFillProfDetails (pi1Frame, pFrameData);
            break;

        case BPCLT_START_TYPE:
            pFrameData->i4KeyElementType = BPCLT_START_TYPE;
            BpCltFillStartDetails (pi1Frame, pFrameData);
            break;

        case BPCLT_CLOSE_TYPE:
            pFrameData->i4KeyElementType = BPCLT_CLOSE_TYPE;
            BpCltFillCloseDetails (pi1Frame, pFrameData);
            break;

        case BPCLT_OK_TYPE:
            pFrameData->i4KeyElementType = BPCLT_OK_TYPE;
            break;

        default:
            return BPCLT_FAILURE;
    }

    return BPCLT_SUCCESS;

}

/******************************************************************************
 * Function           : BpCltFindKeyEltType
 * Input(s)           : Frame Payload
 * Output(s)          : None
 * Returns            : Key Element Type
 * Action             : Routine to parse the frame paylod and extract details
 *                      of key element type or BPCLT_FAILURE
 ******************************************************************************/

INT4
BpCltFindKeyEltType (INT1 *pi1Frame)
{
    INT4                i4Size = BEEP_ZERO;

    i4Size = STRLEN (pi1Frame);

    if (!MEMCMP (pi1Frame, "<start ", STRLEN ("<start ")))
    {
        memmove (pi1Frame, pi1Frame + STRLEN ("<start "),
                 i4Size - STRLEN ("<start ") + BEEP_ONE);
        return BPCLT_START_TYPE;
    }

    if (!MEMCMP (pi1Frame, "<close ", STRLEN ("<close ")))
    {
        memmove (pi1Frame, pi1Frame + STRLEN ("<close "),
                 i4Size - STRLEN ("<close ") + BEEP_ONE);
        return BPCLT_CLOSE_TYPE;
    }

    if (!MEMCMP (pi1Frame, "<greeting>", STRLEN ("<greeting>")))
    {
        memmove (pi1Frame, pi1Frame + STRLEN ("<greeting>"),
                 i4Size - STRLEN ("<greeting>") + BEEP_ONE);
        return BPCLT_GREETING_TYPE;
    }

    if (!MEMCMP (pi1Frame, "<ok />", STRLEN ("<ok />")))
    {
        memmove (pi1Frame, pi1Frame + STRLEN ("<ok />"),
                 i4Size - STRLEN ("<ok />") + BEEP_ONE);
        return BPCLT_OK_TYPE;
    }

    if (!MEMCMP (pi1Frame, "<error ", STRLEN ("<error ")))
    {
        memmove (pi1Frame, pi1Frame + STRLEN ("<error "),
                 i4Size - STRLEN ("<error ") + BEEP_ONE);
        return BPCLT_ERR_TYPE;
    }

    if (!MEMCMP (pi1Frame, "<profile uri=", STRLEN ("<profile uri=")))
    {
        memmove (pi1Frame, pi1Frame + STRLEN ("<profile uri="),
                 i4Size - STRLEN ("<profile uri=") + BEEP_ONE);
        return BPCLT_PROFILE_TYPE;
    }

    return BPCLT_FAILURE;

}

/******************************************************************************
 * Function           : BpCltMvSpcAndLines
 * Input(s)           : String
 * Output(s)          : None
 * Returns            : BPCLT_SUCCESS/BPCLT_FAILURE
 * Action             : Routine to parse the string and remove the white 
 *                      space and lines at the beginning of the string
 ******************************************************************************/

VOID
BpCltMvSpcAndLines (INT1 *pi1Str)
{
    INT4                i4Size = BEEP_ZERO;
    INT4                i4Count = BEEP_ZERO;
    INT4                i4PtrPos = BEEP_ZERO;

    i4Size = STRLEN (pi1Str);

    for (i4Count = BEEP_ZERO; i4Count <= i4Size; i4Count++)
    {
        /* Checking for white spaces and newlines */
        if ((pi1Str[i4Count] == ' ') || (pi1Str[i4Count] == '\t')
            || (pi1Str[i4Count] == '\n') || (pi1Str[i4Count] == '\r'))
        {
            i4PtrPos++;
        }
        else
        {
            break;
        }
    }

    memmove (pi1Str, pi1Str + i4PtrPos, i4Size - i4PtrPos + BEEP_ONE);
    return;
}

/******************************************************************************
 *  Function           : BpCltRmvMimeHdr
 *  Input(s)           : Frame Payload
 *  Output(s)          : None
 *  Returns            : BPSRV_SUCCESS / BPSRV_FAILURE
 *  Action             : Routine to parse the frame paylod and remove
 *                       the mime header
 ******************************************************************************/

INT4
BpCltRmvMimeHdr (INT1 *pi1Frame)
{
    INT4                i4Len = BEEP_ZERO;
    INT4                i4MimeHdrLen = BEEP_ZERO;

    i4MimeHdrLen = STRLEN (BPCLT_MIME_HDR);
    i4Len = STRLEN (pi1Frame);
    if (!MEMCMP (pi1Frame, BPCLT_MIME_HDR, i4MimeHdrLen))
    {
        i4Len = i4Len - i4MimeHdrLen;
        memmove (pi1Frame, pi1Frame + i4MimeHdrLen, i4Len);
        pi1Frame[i4Len] = BPCLT_STR_DLM;
        return BPCLT_SUCCESS;
    }
    return BPCLT_FAILURE;
}

/******************************************************************************
 * Function           : BpCltFillErrDetails
 * Input(s)           : Frame Payload
 * Output(s)          : Data Structure with payload details
 * Returns            : BPCLT_SUCCESS/BPCLT_FAILURE
 * Action             : Routine to parse the frame paylod n which the key
 *                      element is error and fill the data structure
 ******************************************************************************/

INT4
BpCltFillErrDetails (INT1 *pi1Str, tBpCltMgmtChnlData * pFrameData)
{
    INT4                i4Size = BEEP_ZERO;
    INT4                i4Tmp = BEEP_ZERO;
    INT1                ai1Code[BPCLT_MAX_DIGITS];
    INT1                ai1Tmp[BPCLT_TEMP_STR_LEN];
    INT1               *pi1Pos = NULL;
    INT1               *pi1Frame = NULL;
    INT4                i4ErrStrLen = BEEP_ZERO;

    pi1Frame = pi1Str;
    MEMSET (ai1Code, BEEP_ZERO, BPCLT_MAX_DIGITS);
    MEMSET (ai1Tmp, BEEP_ZERO, BPCLT_TEMP_STR_LEN);

    i4Size = STRLEN (pi1Frame);
    if (i4Size < BPCLT_ERR_MIN_LENGTH)
    {
        return BPCLT_FAILURE;
    }

    /* Check if code attribute exists */
    STRCPY (ai1Tmp, "code='");
    i4Tmp = STRLEN (ai1Tmp);
    if (MEMCMP (pi1Frame, ai1Tmp, i4Tmp))
    {
        return BPCLT_FAILURE;
    }
    pi1Frame += i4Tmp;
    i4Size = i4Size - i4Tmp;
    MEMCPY (ai1Code, pi1Frame, BPCLT_ERR_CODE_LEN);
    ai1Code[BPCLT_ERR_CODE_LEN] = BPCLT_STR_DLM;
    pi1Frame += BPCLT_ERR_CODE_LEN;

    if (!((ISDIGIT (ai1Code[BEEP_ONE])) && (ISDIGIT (ai1Code[BEEP_TWO]))
          && (ISDIGIT (ai1Code[BEEP_THREE]))))
    {
        return BPCLT_FAILURE;
    }

    i4Size -= BPCLT_ERR_CODE_LEN;
    pFrameData->i4Code = ATOI (ai1Code);
    if (!MEMCMP (pi1Frame, "'>", BEEP_TWO))
    {
        pi1Frame += BEEP_TWO;
        pi1Pos = (INT1 *) STRSTR (pi1Frame, "</error>");
        if (pi1Pos == NULL)
        {
            return BPCLT_FAILURE;
        }
        i4Size -= BEEP_TWO;
        i4ErrStrLen = pi1Pos - pi1Frame;
        MEMCPY (pFrameData->au1ErrStr, pi1Frame, i4ErrStrLen);
        pFrameData->au1ErrStr[pi1Pos - pi1Frame] = BPCLT_STR_DLM;
        pi1Frame += i4ErrStrLen;
        BpCltMvSpcAndLines (pi1Frame);
        if (STRLEN (pi1Frame) != BEEP_ZERO)
        {
            return BPCLT_FAILURE;
        }
    }
    else
    {
        if (!MEMCMP (pi1Frame, " />", BEEP_THREE))
        {
            pi1Frame += BEEP_THREE;
            BpCltMvSpcAndLines (pi1Frame);
            if (STRLEN (pi1Frame) != BEEP_ZERO)
            {
                return BPCLT_FAILURE;
            }
        }
        else
        {
            return BPCLT_FAILURE;
        }
    }
    return BPCLT_SUCCESS;

}

/******************************************************************************
 * Function           : BpCltFillStartDetails
 * Input(s)           : Frame Payload
 * Output(s)          : Data Structure with payload details
 * Returns            : BPCLT_SUCCESS/BPCLT_FAILURE
 * Action             : Routine to parse the frame paylod in which the key
 *                      element is start and fill the data structure
 ******************************************************************************/

INT4
BpCltFillStartDetails (INT1 *pi1Str, tBpCltMgmtChnlData * pFrameData)
{
    INT4                i4Size = BEEP_ZERO;
    INT4                i4Tmp = BEEP_ZERO;
    INT4                i4Cnt = BEEP_ZERO;
    INT1                i1CdataFlg = BEEP_ZERO;
    INT1                ai1NumStr[BPCLT_MAX_DIGITS];
    INT1               *pi1Frame = NULL;
    INT1               *pi1Tmp = NULL;

    pi1Frame = pi1Str;
    MEMSET (ai1NumStr, BEEP_ZERO, BPCLT_MAX_DIGITS);
    BpCltMvSpcAndLines (pi1Frame);
    pFrameData->i4ProfCount = BEEP_ZERO;
    i4Size = STRLEN (pi1Frame);
    if (pi1Frame != (INT1 *) STRSTR (pi1Frame, "number='"))
    {
        return BPCLT_FAILURE;
    }
    i4Tmp = STRLEN ("number='");
    pi1Frame += i4Tmp;
    i4Size = i4Size - i4Tmp;
    /* Check for Channel number */
    for (i4Cnt = BEEP_ZERO; ISDIGIT (pi1Frame[i4Cnt]); i4Cnt++)
    {
        ai1NumStr[i4Cnt] = pi1Frame[i4Cnt];
    }

    if (i4Cnt == BEEP_ZERO)
    {
        return BPCLT_FAILURE;
    }

    pi1Frame += i4Cnt;
    i4Size = i4Size - i4Cnt;
    ai1NumStr[i4Cnt] = BPCLT_STR_DLM;
    pFrameData->u4Num = ATOI (ai1NumStr);

    if (pi1Frame != (INT1 *) STRSTR (pi1Frame, "'>"))
    {
        return BPCLT_FAILURE;
    }
    i4Size = i4Size - BEEP_TWO;
    pi1Frame += BEEP_TWO;
    BpCltMvSpcAndLines (pi1Frame);
    i1CdataFlg = BPCLT_FLG_NOT_ACTIVE;
    /* Check for the profiles available and load the profile names */
    while (pi1Frame == (INT1 *) STRSTR (pi1Frame, "<profile uri="))
    {
        i1CdataFlg = BPCLT_FLG_NOT_ACTIVE;
        pi1Frame += STRLEN ("<profile uri=");
        pi1Tmp = (INT1 *) STRSTR (pi1Frame, " />");
        if (pi1Tmp == NULL)
        {
            pi1Tmp = (INT1 *) STRSTR (pi1Frame, ">");
            if (pi1Tmp == NULL)
            {
                return BPCLT_FAILURE;
            }
            i1CdataFlg = BPCLT_FLG_ACTIVE;

        }
        i4Tmp = pi1Tmp - pi1Frame;
        MEMCPY (pFrameData->ai1Profile[pFrameData->i4ProfCount], pi1Frame,
                i4Tmp);
        pFrameData->ai1Profile[pFrameData->i4ProfCount][i4Tmp] = BPCLT_STR_DLM;
        pFrameData->i4ProfCount++;
        if (i1CdataFlg == BPCLT_FLG_ACTIVE)
        {
            pi1Frame = pi1Frame + i4Tmp + BEEP_ONE;
            BpCltMvSpcAndLines (pi1Frame);
            if (!MEMCMP (pi1Frame, "<![CDATA[<ready />]]>",
                         STRLEN ("<![CDATA[<ready />]]>")))
            {
                pi1Frame += STRLEN ("<![CDATA[<ready />]]>");
                BpCltMvSpcAndLines (pi1Frame);
                if (!MEMCMP (pi1Frame, "<profile />", STRLEN ("<profile />")))
                {
                    pi1Frame += STRLEN ("<profile />");
                    BpCltMvSpcAndLines (pi1Frame);
                    pFrameData->i1CdataRcvd = BPCLT_RPY_CODE;
                    pFrameData->Cdata.i4ElementCode = BPCLT_ELEMENT_CODE_READY;
                }
            }

        }
        else
        {
            pi1Frame = pi1Frame + i4Tmp + BEEP_THREE;
        }
        BpCltMvSpcAndLines (pi1Frame);

    }

    /* Check for atleast one profile got */
    if (pFrameData->i4ProfCount == BEEP_ZERO)
    {
        return BPCLT_FAILURE;
    }

    if (pi1Frame != (INT1 *) STRSTR (pi1Frame, "</start>"))
    {
        return BPCLT_FAILURE;
    }

    BpCltMvSpcAndLines (pi1Frame);
    if (STRLEN (pi1Frame) != BEEP_ZERO)
    {
        return BPCLT_FAILURE;
    }

    return BPCLT_SUCCESS;

}

/******************************************************************************
 * Function           : BpCltFillGrtDetails
 * Input(s)           : Frame Payload
 * Output(s)          : None
 * Returns            : BPCLT_SUCCESS/BPCLT_FAILURE
 * Action             : Routine to parse the frame paylod to see
 *                      any element follows greeting from client
 ******************************************************************************/

INT4
BpCltFillGrtDetails (INT1 *pi1Str, tBpCltMgmtChnlData * pFrameData)
{
    INT1               *pi1Frame = NULL;
    INT1               *pi1Tmp = NULL;
    INT4                i4Tmp = BEEP_ZERO;

    pi1Frame = pi1Str;
    BpCltMvSpcAndLines (pi1Frame);
    /* Check for the profiles available and load the profile names */
    while (pi1Frame == (INT1 *) STRSTR (pi1Frame, "<profile uri="))
    {
        pi1Frame += STRLEN ("<profile uri=");
        if ((pi1Tmp = (INT1 *) STRSTR (pi1Frame, " />")) == NULL)
        {
            return BPCLT_FAILURE;

        }
        i4Tmp = pi1Tmp - pi1Frame;
        MEMCPY (pFrameData->ai1Profile[pFrameData->i4ProfCount], pi1Frame,
                i4Tmp);
        pFrameData->ai1Profile[pFrameData->i4ProfCount][i4Tmp] = BPCLT_STR_DLM;
        pFrameData->i4ProfCount++;
        pi1Frame = pi1Frame + i4Tmp + BEEP_THREE;
        BpCltMvSpcAndLines (pi1Frame);

    }

    /* Check for atleast one profile got */
    if (pFrameData->i4ProfCount == BEEP_ZERO)
    {
        return BPCLT_FAILURE;
    }

    if (pi1Frame != (INT1 *) STRSTR (pi1Frame, "</greeting>"))
    {
        return BPCLT_FAILURE;
    }
    memmove (pi1Frame, pi1Frame + STRLEN ("</greeting>"),
             STRLEN (pi1Frame) - STRLEN ("</greeting>") + BEEP_ONE);

    BpCltMvSpcAndLines (pi1Frame);
    if (STRLEN (pi1Frame) != BEEP_ZERO)
    {
        return BPCLT_FAILURE;
    }

    return BPCLT_SUCCESS;

}

/******************************************************************************
 * Function           : BpCltFillOkDetails
 * Input(s)           : Frame Payload
 * Output(s)          : Data Structure with payload details
 * Returns            : BPCLT_SUCCESS/BPCLT_FAILURE
 * Action             : Routine to parse the frame paylod to see
 *                      any element follows ok
 ******************************************************************************/

INT4
BpCltFillOkDetails (INT1 *pi1Str, tBpCltMgmtChnlData * pFrameData)
{
    UNUSED_PARAM (pFrameData);

    BpCltMvSpcAndLines (pi1Str);
    if (STRLEN (pi1Str) == BEEP_ZERO)
    {
        return BPCLT_SUCCESS;
    }
    return BPCLT_FAILURE;

}

/******************************************************************************
 * Function           : BpCltFillCloseDetails
 * Input(s)           : Frame Payload
 * Output(s)          : Data Structure with payload details
 * Returns            : BPCLT_SUCCESS/BPCLT_FAILURE
 * Action             : Routine to parse the frame paylod in which the key
 *                      element is close and fill the data structure
 ******************************************************************************/

INT4
BpCltFillCloseDetails (INT1 *pi1Str, tBpCltMgmtChnlData * pFrameData)
{
    INT4                i4Size = BEEP_ZERO;
    INT4                i4Tmp = BEEP_ZERO;
    INT4                i4Cnt = BEEP_ZERO;
    INT1                ai1NumStr[BPCLT_MAX_DIGITS];
    INT1                ai1Code[BPCLT_MAX_DIGITS];
    INT1               *pi1Frame = NULL;

    pi1Frame = pi1Str;
    MEMSET (ai1NumStr, BEEP_ZERO, BPCLT_MAX_DIGITS);
    MEMSET (ai1Code, BEEP_ZERO, BPCLT_MAX_DIGITS);

    BpCltMvSpcAndLines (pi1Frame);
    pFrameData->i4ProfCount = BEEP_ZERO;
    i4Size = STRLEN (pi1Frame);
    if (pi1Frame != (INT1 *) STRSTR (pi1Frame, "number='"))
    {
        return BPCLT_FAILURE;
    }
    i4Tmp = STRLEN ("number='");
    pi1Frame += i4Tmp;
    i4Size = i4Size - i4Tmp;
    /* Check for Channel number */
    for (i4Cnt = BEEP_ZERO; ISDIGIT (pi1Frame[i4Cnt]); i4Cnt++)
    {
        ai1NumStr[i4Cnt] = pi1Frame[i4Cnt];
    }

    if (i4Cnt == BEEP_ZERO)
    {
        return BPCLT_FAILURE;
    }

    pi1Frame += i4Cnt;
    i4Size = i4Size - i4Cnt;
    ai1NumStr[i4Cnt] = BPCLT_STR_DLM;
    pFrameData->u4Num = ATOI (ai1NumStr);

    if (pi1Frame != (INT1 *) STRSTR (pi1Frame, " code='"))
    {
        return BPCLT_FAILURE;
    }
    i4Tmp = STRLEN (" code='");
    pi1Frame += i4Tmp;
    i4Size = i4Size - i4Tmp;
    MEMCPY (ai1Code, pi1Frame, BPCLT_ERR_CODE_LEN);

    /* Check for Channel number */
    if (!((ISDIGIT (ai1Code[BEEP_ONE])) && (ISDIGIT (ai1Code[BEEP_TWO]))
          && (ISDIGIT (ai1Code[BEEP_THREE]))))
    {
        return BPCLT_FAILURE;
    }

    i4Size -= BPCLT_ERR_CODE_LEN;
    pFrameData->i4Code = ATOI (ai1Code);
    pi1Frame += BPCLT_ERR_CODE_LEN;

    if (pi1Frame != (INT1 *) STRSTR (pi1Frame, "' />"))
    {
        return BPCLT_FAILURE;
    }

    pi1Frame += STRLEN ("' />");
    BpCltMvSpcAndLines (pi1Frame);
    if (STRLEN (pi1Frame) != BPCLT_STR_DLM)
    {
        return BPCLT_FAILURE;
    }

    return BPCLT_SUCCESS;

}

/******************************************************************************
 * Function           : BpCltFillCloseDetails
 * Input(s)           : pi1Frame -Frame Payload, pFrameData - Frame Data
 * Output(s)          : Data Structure with payload details
 * Returns            : BPCLT_SUCCESS/BPCLT_FAILURE
 * Action             : Routine to parse the frame paylod in which the key
 *                      element is profile and fill the data structure
 ******************************************************************************/

INT4
BpCltFillProfDetails (INT1 *pi1Frame, tBpCltMgmtChnlData * pFrameData)
{
    INT4                i4Tmp = BEEP_ZERO;
    INT1                i1CdataFlg = BPCLT_FLG_NOT_ACTIVE;
    INT1               *pi1Tmp = NULL;

    if ((pi1Tmp = (INT1 *) STRSTR (pi1Frame, " />")) == NULL)
    {
        if ((pi1Tmp = (INT1 *) STRSTR (pi1Frame, ">")) == NULL)
        {
            return BPCLT_FAILURE;
        }
        i1CdataFlg = BPCLT_FLG_ACTIVE;

    }

    i4Tmp = pi1Tmp - pi1Frame;
    MEMCPY (pFrameData->ai1Profile[BEEP_ZERO], (UINT1 *) pi1Frame, i4Tmp);
    pFrameData->ai1Profile[BEEP_ZERO][i4Tmp] = BPCLT_STR_DLM;
    if (i1CdataFlg == BPCLT_FLG_ACTIVE)
    {
        pi1Frame = pi1Frame + i4Tmp + BEEP_ONE;
        BpCltMvSpcAndLines (pi1Frame);
        if (!MEMCMP (pi1Frame, "<![CDATA[<proceed />]]>",
                     STRLEN ("<![CDATA[<proceed />]]>")))
        {
            pi1Frame += STRLEN ("<![CDATA[<proceed />]]>");
            BpCltMvSpcAndLines (pi1Frame);
            if (!MEMCMP (pi1Frame, "</profile>", STRLEN ("</profile>")))
            {
                pi1Frame += STRLEN ("</profile>");
                BpCltMvSpcAndLines (pi1Frame);
                pFrameData->i1CdataRcvd = BPCLT_RPY_CODE;
                pFrameData->Cdata.i4ElementCode = BPCLT_ELEMENT_CODE_PROCEED;
            }
            else
            {
                return BPCLT_FAILURE;
            }
        }
        else
        {
            return BPCLT_FAILURE;
        }

    }
    else
    {
        pi1Frame = pi1Frame + i4Tmp + BEEP_THREE;
    }
    BpCltMvSpcAndLines (pi1Frame);

    if (STRLEN (pi1Frame) != BEEP_ZERO)
    {
        return BPCLT_FAILURE;
    }

    return BPCLT_SUCCESS;
}
