#include "bpcltinc.h"
#include "fssocket.h"

/**************************************************************************/
/*   Function Name   : BpCltCloseSocket                                   */
/*   Description     : This function Closes the socket created            */
/*   Input(s)        : i4SockId - Socket Identifier                       */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/
VOID
BpCltCloseSocket (INT4 i4SockId)
{
    close (i4SockId);
}

/**************************************************************************/
/*   Function Name   : BpCltSockCreate                                    */
/*   Description     : This function Creates a IPv4 socket and initialises*/
/*                     the socket                                         */
/*   Input(s)        : i4Port - Port number                               */
/*                     pSrvAddr - Beep Server Address                     */
/*                     pi4SockId - Pointer to the created sockId          */
/*   Output(s)       : None                                               */
/*   Return Value    : BPCLT_SUCCESS or BPCLT_FAILURE                     */
/**************************************************************************/
INT4
BpCltSockCreate (INT4 i4Port, UINT4 u4Addr, INT4 *pi4SockId)
{
    UINT2               u2Port = BEEP_ZERO;
    INT4                i4Sock = BEEP_MINUS_ONE;
    INT4                i4RetVal = BPCLT_FAILURE;
    INT4                i4Flags = BEEP_ZERO;
    struct sockaddr_in  SrvAddr;

    BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_DATA_PATH_TRC, BPCLT_NAME,
               "BpCltSockInit: ENTRY \r\n");

    /* Open the tcp socket */
    *pi4SockId = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
    i4Sock = *pi4SockId;

    if (*pi4SockId < BEEP_ZERO)
    {
        BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_FAILURE_TRC, BPCLT_NAME,
                   "BpCltSockInit: TCP server socket creation failure !!!\n");
        return BPCLT_FAILURE;
    }

    u2Port = (UINT2) i4Port;

    MEMSET (&SrvAddr, BEEP_ZERO, sizeof (SrvAddr));
    SrvAddr.sin_family = AF_INET;
    SrvAddr.sin_addr.s_addr = u4Addr;
    SrvAddr.sin_port = OSIX_HTONS (u2Port);

    i4RetVal = connect (i4Sock, (struct sockaddr *) &SrvAddr, sizeof (SrvAddr));
    if (i4RetVal < BEEP_ZERO)
    {
        if (errno == EINPROGRESS)
        {
            return BPCLT_SUCCESS;
        }
        BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_FAILURE_TRC, BPCLT_NAME,
                   "BpCltSockInit: Socket bind failure !!!\n");
        BpCltCloseSocket (*pi4SockId);
        return BPCLT_FAILURE;

    }

    if ((i4Flags = fcntl (*pi4SockId, F_GETFL, BEEP_ZERO)) < BEEP_ZERO)
    {
        BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_FAILURE_TRC, BPCLT_NAME,
                   "BpCltSockInit:TCP server fcntl get failure !!!\n");
        BpCltCloseSocket (*pi4SockId);
        return BPCLT_FAILURE;
    }

    i4Flags |= O_NONBLOCK;
    if (fcntl (*pi4SockId, F_SETFL, i4Flags) < BEEP_ZERO)
    {
        BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_FAILURE_TRC, BPCLT_NAME,
                   "BpCltSockInit:TCP server fcntl set failure !!!\n");
        BpCltCloseSocket (*pi4SockId);
        return BPCLT_FAILURE;
    }
    return BPCLT_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : BpCltSock6Create                                   */
/*   Description     : This function Creates a IPv6 socket and initialises*/
/*                     the socket                                         */
/*   Input(s)        : i4Port - Port number                               */
/*                     pSrvAddr - Beep Server Address                     */
/*                     pi4SockId - Pointer to the created sockId          */
/*   Output(s)       : None                                               */
/*   Return Value    : BPCLT_SUCCESS or BPCLT_FAILURE                     */
/**************************************************************************/
#ifdef IP6_WANTED
INT4
BpCltSock6Create (INT4 i4Port, tIPvXAddr * pAddr, INT4 *pi4SockId)
{
    INT4                i4OptVal = BEEP_FALSE;
    INT4                i4Flags = BEEP_ZERO;
    INT4                i4RetVal = BPCLT_FAILURE;
    struct sockaddr_in6 SrvAddr;

    BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_DATA_PATH_TRC, BPCLT_NAME,
               "BpCltSockInit: ENTRY \r\n");

    /* Open the tcp socket */
    *pi4SockId = socket (AF_INET6, SOCK_STREAM, IPPROTO_TCP);
    if (*pi4SockId < 0)
    {
        BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_FAILURE_TRC, BPCLT_NAME,
                   "BpCltSockInit: TCP6 server socket creation failure !!!\n");
        return BPCLT_FAILURE;
    }

    i4OptVal = BEEP_TRUE;
    if (setsockopt (*pi4SockId, IPPROTO_IPV6, IPV6_V6ONLY, (char *) &i4OptVal,
                    sizeof (i4OptVal)))
    {
        BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_FAILURE_TRC, BPCLT_NAME,
                   "BpCltSock6Create:Socket option IPV6_V6ONLY failure !!!\n");
        BpCltCloseSocket (*pi4SockId);
        return BPCLT_FAILURE;
    }

    MEMSET (&SrvAddr, BEEP_ZERO, sizeof (SrvAddr));
    SrvAddr.sin6_family = AF_INET6;
    SrvAddr.sin6_port = OSIX_HTONS (i4Port);
    MEMCPY (&SrvAddr.sin6_addr.s6_addr, pAddr->au1Addr,
            sizeof (pAddr->au1Addr));
    i4RetVal =
        connect (*pi4SockId, (struct sockaddr *) &SrvAddr, sizeof (SrvAddr));
    if (i4RetVal < BEEP_ZERO)
    {
        if (errno == EINPROGRESS)
        {
            return BPCLT_SUCCESS;
        }
        BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_FAILURE_TRC, BPCLT_NAME,
                   "BpCltSockInit: Socket bind failure !!!\n");
        BpCltCloseSocket (*pi4SockId);
        return BPCLT_FAILURE;
    }
    if ((i4Flags = fcntl (*pi4SockId, F_GETFL, BEEP_ZERO)) < BEEP_ZERO)
    {
        BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_FAILURE_TRC, BPCLT_NAME,
                   "BpCltSockInit:TCP6 server fcntl get failure !!!\n");
        BpCltCloseSocket (*pi4SockId);
        return BPCLT_FAILURE;
    }

    i4Flags |= O_NONBLOCK;
    if (fcntl (*pi4SockId, F_SETFL, i4Flags) < BEEP_ZERO)
    {
        BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_FAILURE_TRC, BPCLT_NAME,
                   "BpCltSockInit:TCP6 server fcntl set failure !!!\n");
        BpCltCloseSocket (*pi4SockId);
        return BPCLT_FAILURE;
    }
    return BPCLT_SUCCESS;
}
#endif

/**************************************************************************/
/*   Function Name   : BpCltSockInit                                      */
/*   Description     : This function Creates a socket and initialises     */
/*                     the socket                                         */
/*   Input(s)        : i4Port - Port number                               */
/*                     pSrvAddr - Beep Server Address                     */
/*   Output(s)       : None                                               */
/*   Return Value    : BPCLT_SUCCESS or BPCLT_FAILURE                     */
/**************************************************************************/
INT4
BpCltSockInit (INT4 i4Port, tIPvXAddr * pSrvAddr, INT4 *pi4SockId)
{
    UINT4               u4Addr = BEEP_ZERO;

    if (pSrvAddr->u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (&u4Addr, pSrvAddr->au1Addr, IPVX_IPV4_ADDR_LEN);
        return (BpCltSockCreate (i4Port, u4Addr, pi4SockId));
    }

    if (pSrvAddr->u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
#ifdef IP6_WANTED
        return (BpCltSock6Create (i4Port, pSrvAddr, pi4SockId));
#endif
    }
    return BPCLT_FAILURE;
}

/******************************************************************************
 * Function           : BpCltTlsSend
 * Input(s)           : pSsnNode - Pointer to the session node
 *                      pi1String - MEssage to be sent.
 * Output(s)          : None.
 * Returns            : BPCLT_SUCCESS/BPCLT_FAILURE
 * Action             : Routine to send the data to the client through 
 *                      appropriate socket
 ******************************************************************************/
INT4
BpCltTlsSend (tBpCltSsnNode * pSsnNode, INT1 *pi1String)
{
    /* should be completed when the Beep with TLS encryption supported */
    UNUSED_PARAM (pSsnNode);
    UNUSED_PARAM (pi1String);
    return BPCLT_SUCCESS;
}

/******************************************************************************
 * Function           : BpCltTlsSend
 * Input(s)           : pSsnNode - Pointer to session node
 *                      pi1String - MEssage to be sent.
 * Output(s)          : None.
 * Returns            : BPCLT_SUCCESS/BPCLT_FAILURE
 * Action             : Routine to send the data to the client through 
 *                      appropriate socket
 ******************************************************************************/
INT4
BpCltSendToServer (tBpCltSsnNode * pSsnNode, INT1 *pi1String)
{
    INT4                i4RetVal = BEEP_ZERO;
    INT4                i4Size = BEEP_ZERO;
    INT4                i4Written = BEEP_ZERO;
    INT1               *pi1Buffer = NULL;

    i4Size = STRLEN (pi1String);
    pi1Buffer = pi1String;

    if (!i4Size)
    {
        return BPCLT_SUCCESS;
    }

    /* If Tls is active for the current session, send data via encryption */
    if (pSsnNode->i1TlsFlag == BPCLT_FLG_ACTIVE)
    {
        BpCltTlsSend (pSsnNode, pi1String);
        return BPCLT_SUCCESS;
    }
    else
    {
        /* If the size is heavier than the max buff size, split and send */
        while (i4Size > BPCLT_MAX_SOC_WRITE_LEN)
        {
            i4RetVal = send (pSsnNode->i4SockId, pi1Buffer,
                             BPCLT_MAX_SOC_WRITE_LEN, BEEP_ZERO);
            if (i4RetVal < BEEP_ZERO)
            {
                /* If there is an interrupt, try sending after a second */
                if (errno == EINTR)
                {
                    OsixTskDelay (BEEP_ONE * SYS_NUM_OF_TIME_UNITS_IN_A_SEC);
                    continue;
                }
                return BPCLT_TCP_ABORT;
            }
            pi1Buffer += i4RetVal;
            i4Size -= i4RetVal;
            i4Written += i4RetVal;
        }

        while (i4Size > BEEP_ZERO)
        {
            i4RetVal = send (pSsnNode->i4SockId, pi1Buffer, i4Size, BEEP_ZERO);
            if (i4RetVal < BEEP_ZERO)
            {
                if (errno == EINTR)
                {
                    OsixTskDelay (BEEP_ONE * SYS_NUM_OF_TIME_UNITS_IN_A_SEC);
                    continue;
                }
                return BPCLT_TCP_ABORT;
            }
            pi1Buffer += i4RetVal;
            i4Size -= i4RetVal;
            i4Written += i4RetVal;
        }

    }
    return BPCLT_SUCCESS;
}
