/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bpclchnl.c,v 1.7 2012/03/30 13:17:16 siva Exp $
 *
 * Description:This file contains the functions to handle channel  
 * for Beep Client Session
 *
 *******************************************************************/

#include "bpcltinc.h"

/**************************************************************************/
/*   Function Name   : BpCltCreateChannel                                 */
/*   Description     : This functions is used to create Channel for Beep  */
/*                     for a session                                      */
/*   Input(s)        : i4ChnlNum - Channel Number                         */
/*                     i4Profile - profile of the channel                 */
/*   Output(s)       : None                                               */
/*   Return Value    : Pointer to a session if the session is created     */
/*                     successfully, NULL otherwise                       */
/**************************************************************************/

tBpCltChnlNode     *
BpCltCreateChannel (INT4 i4ChnlNum, INT4 i4Profile)
{
    tBpCltChnlNode     *pChnlNode = NULL;

    pChnlNode = (tBpCltChnlNode *) MemAllocMemBlk (BPCLT_CHNL_POOL_ID);

    if (pChnlNode == NULL)
    {
        BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_MGMT_TRC, BPCLT_NAME,
                   "Malloc for Beep Channel Node Entry failed. \n");
        return NULL;
    }

    MEMSET (pChnlNode, BEEP_ZERO, sizeof (tBpCltChnlNode));

    pChnlNode->i4Profile = i4Profile;
    pChnlNode->i4ProfileState = BPCLT_CHNL_OPENED;
    pChnlNode->i4ChnlNum = i4ChnlNum;
    pChnlNode->i4LastMsg = BEEP_MINUS_ONE;
    pChnlNode->i4LastRpy = BEEP_ZERO;
    pChnlNode->u4SeqNoToSend = BEEP_ZERO;
    pChnlNode->u4ExpectSeqNo = BEEP_ZERO;
    pChnlNode->i4LastAnsNum = BEEP_MINUS_ONE;
    return pChnlNode;
}

/**************************************************************************/
/*   Function Name   : BpCltAddChannel                                    */
/*   Description     : This functions is used to add the channel node in  */
/*                     the Beep channel list for a session                */
/*                     Table                                              */
/*   Input(s)        : pChnlNode - Pointer to the channel node            */
/*                     pSsnPtr - Pointer to the session node where the    */
/*                     where the channel node to be added                 */
/*   Output(s)       : None                                               */
/*   Return Value    : BPCLT_SUCCESS or BPCLT_FAILURE                     */
/**************************************************************************/

INT4
BpCltAddChannel (tBpCltChnlNode * pChnlNode, tBpCltSsnNode * pSsnPtr)
{
    if ((pChnlNode->i4ChnlNum < BEEP_MIN_CHNL_NUM) ||
        (pChnlNode->i4ChnlNum > BEEP_MAX_CHNL_NUM))
    {
        return BPCLT_FAILURE;
    }
    pSsnPtr->aBpCltChnlList[pChnlNode->i4ChnlNum] = pChnlNode;
    return BPCLT_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : BpCltDeleteChannel                                 */
/*   Description     : This functions is used to Delete the Channel node  */
/*                     for a Beep Client Session                          */
/*                     Table                                              */
/*                                                                        */
/*   Input(s)        : pChnlNode - Pointer to a channel node              */
/*                     pSsnPtr - Pointer to beep session                  */
/*   Output(s)       : None                                               */
/*   Return Value    : BPCLT_SUCCESS or BPCLT_FAILURE                     */
/**************************************************************************/
INT4
BpCltDeleteChannel (tBpCltChnlNode * pChnlNode, tBpCltSsnNode * pSsnPtr)
{
    if ((pChnlNode->i4ChnlNum < BEEP_MIN_CHNL_NUM) ||
        (pChnlNode->i4ChnlNum > BEEP_MAX_CHNL_NUM))
    {
        return BPCLT_FAILURE;
    }

    pSsnPtr->aBpCltChnlList[pChnlNode->i4ChnlNum] = NULL;
    return BPCLT_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : BpCltGetChannelNode                                */
/*   Description     : This functions is used to get the channel node for */
/*                     given channel num and session pointer              */
/*                                                                        */
/*   Input(s)        : pSsnPtr - Pointer to the session node              */
/*                     i4ChnlNum - channel number                         */
/*   Output(s)       : None                                               */
/*   Return Value    : Pointer to a session if the session is created     */
/*                     successfully, NULL otherwise                       */
/**************************************************************************/

tBpCltChnlNode     *
BpCltGetChannelNode (tBpCltSsnNode * pSsnPtr, INT4 i4ChnlNum)
{
    tBpCltChnlNode     *pChnlNode = NULL;

    if ((i4ChnlNum < BEEP_MIN_CHNL_NUM) || (i4ChnlNum > BEEP_MAX_CHNL_NUM))
    {
        return NULL;
    }

    pChnlNode = pSsnPtr->aBpCltChnlList[i4ChnlNum];
    return pChnlNode;
}

/**************************************************************************/
/*   Function Name   : BpCltMgmtChnlHdlr                                  */
/*                                                                        */
/*   Description     : This functions is used to handle the Mgmt channel  */
/*                     funtionalities. This function checks the profile   */
/*                     state of the received channel and calls the        */
/*                     respective function                                */
/*                                                                        */
/*   Input(s)        : pRcvFrameHdr - Pointer to the received frame hdr   */
/*                     pi1RcvFrame - Pointer to the received frame from   */
/*                                   beep server                          */
/*                     i4SockId     - Socket Id of the received session   */
/*                                                                        */
/*   Output(s)       : None                                               */
/*                                                                        */
/*   Return Value    : BPCLT_SUCCESS  for successful completion           */
/*                     BPCLT_FAILURE   for failure                        */
/**************************************************************************/

INT4
BpCltMgmtChnlHdlr (tBpCltFrameHdr * pRcvFrameHdr, INT1 *pi1RcvFrame,
                   INT4 i4SockId)
{
    INT4                i4RetState = BEEP_ZERO;
    tBpCltMgmtChnlData  *pMgmtChnlData = NULL;
    tBpCltChnlNode     *pChnlNode = NULL;
    tBpCltSsnNode      *pSsnNode = NULL;

    if (pRcvFrameHdr == NULL || pi1RcvFrame == NULL)
    {
        return BPCLT_FAILURE;
    }
    
    pMgmtChnlData = (tBpCltMgmtChnlData *)MemAllocMemBlk(BPCLT_MGMT_CHNL_POOL_ID);
    if (pMgmtChnlData == NULL)
    {
        return BPCLT_FAILURE;
    }
    MEMSET (pMgmtChnlData, BEEP_ZERO, sizeof (tBpCltMgmtChnlData));

    /* Parse the packet to get the Channel Data */
    if (BpCltParseMgmtChnlData (pMgmtChnlData, pi1RcvFrame) == BPCLT_FAILURE)
    {
        BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC, BPCLT_NAME,
                   "Poorly Formed Frame Received:Closing Session !!");
        MemReleaseMemBlock(BPCLT_MGMT_CHNL_POOL_ID, (UINT1 *)pMgmtChnlData);
        return BPCLT_UNEXPECTED;
    }

    pSsnNode = BpCltGetSessionNode (NULL, i4SockId);

    if (pSsnNode != NULL)
    {
        pChnlNode = BpCltGetChannelNode (pSsnNode, pRcvFrameHdr->i4ChnlNum);
        if (pChnlNode == NULL)
        {
            BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC, BPCLT_NAME,
                       "Poorly Formed Frame Received:Closing Session !!");
            MemReleaseMemBlock(BPCLT_MGMT_CHNL_POOL_ID, (UINT1 *)pMgmtChnlData);
            return BPCLT_UNEXPECTED;
        }
    }
    else
    {
        MemReleaseMemBlock(BPCLT_MGMT_CHNL_POOL_ID, (UINT1 *)pMgmtChnlData);
        return BPCLT_FAILURE;
    }

    switch (pChnlNode->i4ProfileState)
    {
        case BPCLT_GREETINGS_SENT:
            i4RetState = BpCltProcessPktOnGrtgsSent
                (pRcvFrameHdr, pMgmtChnlData, i4SockId);
            break;
        case BPCLT_TLS_ON_PROGRESS:
            i4RetState = BpCltProcessPktOnTLSProgress
                (pRcvFrameHdr, pMgmtChnlData, i4SockId);
            break;
        case BPCLT_MD5_ON_PROGRESS:
            i4RetState = BpCltProcessPktOnMD5Progress
                (pRcvFrameHdr, pMgmtChnlData, i4SockId);
            break;
        case BPCLT_APPLN_ON_PROGRESS:
            i4RetState = BpCltProcessPktOnAppProgress
                (pRcvFrameHdr, pMgmtChnlData, i4SockId);
            break;
        case BPCLT_CLOSE_SENT:
            i4RetState = BpCltProcessPktOnCloseSent
                (pRcvFrameHdr, pMgmtChnlData, i4SockId, pSsnNode);
            break;
        default:
            break;
    }
    MemReleaseMemBlock(BPCLT_MGMT_CHNL_POOL_ID, (UINT1 *)pMgmtChnlData);
    return (i4RetState);
}

/**************************************************************************/
/*   Function Name   : BpCltProcessPktOnGrtgsSent                         */
/*                                                                        */
/*   Description     : This functions process the packet when the profile */
/*                     state is greetings sent.                           */
/*                     If the reply frame is received for a profile, if   */
/*                     the profile is supported it sends start message for*/
/*                     pkt                                                */
/*                                                                        */
/*   Input(s)        : pRcvFrameHdr - Pointer to the received frame hdr   */
/*                     pi1RcvFrame - Pointer to the received frame from   */
/*                                   beep server                          */
/*                     i4SockId     - Socket Id of the received session   */
/*                                                                        */
/*   Output(s)       : None                                               */
/*                                                                        */
/*   Return Value    : BPCLT_SUCCESS  for successful completion           */
/*                     BPCLT_FAILURE   for failure                        */
/**************************************************************************/
INT4
BpCltProcessPktOnGrtgsSent (tBpCltFrameHdr * pRcvFrameHdr,
                            tBpCltMgmtChnlData * pMgmtChnlData, INT4 i4SockId)
{
    INT1                *pi1Buf = NULL;
    UINT4               u4ReplyCode = BEEP_ZERO;
    INT4                i4ChnlNum = BEEP_ZERO;
    tBpCltChnlNode     *pChnlNode = NULL;
    tBpCltSsnNode      *pSsnNode = NULL;
    tBpCltMsg          *pBpMsg = NULL;

    if ((pRcvFrameHdr == NULL) || (pMgmtChnlData == NULL))
    {
        return BPCLT_FAILURE;
    }

    pSsnNode = BpCltGetSessionNode (NULL, i4SockId);
    if (pSsnNode == NULL)
    {
        return BPCLT_FAILURE;
    }

    pChnlNode = BpCltGetChannelNode (pSsnNode, pRcvFrameHdr->i4ChnlNum);
    if (pChnlNode == NULL)
    {
        return BPCLT_FAILURE;
    }

    pi1Buf = (INT1 *)MemAllocMemBlk(BPCLT_FRAME_POOL_ID);
    if (pi1Buf == NULL)
    {
        return BPCLT_FAILURE;
    }
    MEMSET (pi1Buf, BEEP_ZERO, BEEP_MAX_FRAME_SIZE);

    if (pRcvFrameHdr->i4FrameHeadType == BEEP_RPY_FRAME)
    {
        /* Check the greetings message received with profiles list */
        if ((pMgmtChnlData->i4KeyElementType == BPCLT_GREETING_TYPE) &&
            (pMgmtChnlData->i4ProfCount > BEEP_ZERO))
        {
            if (BpCltCheckProfilesSupported
                (pMgmtChnlData->i4ProfCount, pSsnNode->u4ProfileBitMask,
                 pMgmtChnlData->ai1Profile[BEEP_ZERO]) != BPCLT_SUCCESS)
            {
                /* Send the CLOSE message with Reply code service not 
                 * available ie. 421*/
                i4ChnlNum = BEEP_ZERO;
                u4ReplyCode = BPCLT_SERVICE_NOT_AVAILABLE;
                FormCloseMsg (BEEP_ZERO, u4ReplyCode, (UINT1 *) pi1Buf);
                BeepAddMimeHdr (pi1Buf);
                BeepMakeFrame (BEEP_ERR, pChnlNode, pi1Buf);
                BpCltSendToServer (pSsnNode, pi1Buf);
                MemReleaseMemBlock(BPCLT_FRAME_POOL_ID, (UINT1 *)pi1Buf);
                return BPCLT_UNEXPECTED;

            }

            /* if TLS encryption required, send the START message */
            if (pSsnNode->u4ProfileBitMask & BPCLT_TLS_REQUIRED)
            {
                /* Get the next avaialable channel for TLS profile */
                if (BpCltGetNewChnlNum (pSsnNode, (UINT4 *) &i4ChnlNum)
                    == BPCLT_FAILURE)
                {
                    BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC,
                               BPCLT_NAME, "Couldnot get the new channel !!");
                    MemReleaseMemBlock(BPCLT_FRAME_POOL_ID, (UINT1 *)pi1Buf);
                    return BPCLT_UNEXPECTED;
                }

                /* Send the START message */
                FormStartMsg (i4ChnlNum, BEEP_TLS_PROFILE, (UINT1 *) pi1Buf);
                BeepAddMimeHdr (pi1Buf);
                BeepMakeFrame (BEEP_MSG, pChnlNode, pi1Buf);
                BpCltSendToServer (pSsnNode, pi1Buf);
                MemReleaseMemBlock(BPCLT_FRAME_POOL_ID, (UINT1 *)pi1Buf);
                pChnlNode->i4ProfileState = BPCLT_TLS_ON_PROGRESS;

                pBpMsg = (tBpCltMsg *) MemAllocMemBlk (BPCLT_SRV_POOL_ID);
                if (pBpMsg == NULL)
                {
                    return BPCLT_FAILURE;
                }

                TMO_SLL_Add (&(pSsnNode->BpCltMsgList), &pBpMsg->Next);
                pBpMsg->i4ChnlNo = i4ChnlNum;
                pBpMsg->i4Msg = BPCLT_START_MSG;
                pBpMsg->i4MsgNo = pChnlNode->i4LastMsg;
                return BPCLT_SUCCESS;
            }

            /* if Authentication required with MD5, send the START message */
            if (pSsnNode->u4ProfileBitMask & BPCLT_MD5_REQUIRED)
            {
                /* Get the next avaialable channel for MD5 profile */
                if (BpCltGetNewChnlNum (pSsnNode, (UINT4 *) &i4ChnlNum)
                    == BPCLT_FAILURE)
                {
                    BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC,
                               BPCLT_NAME, "Couldnot get the new channel !!");
                    MemReleaseMemBlock(BPCLT_FRAME_POOL_ID, (UINT1 *)pi1Buf);
                    return BPCLT_UNEXPECTED;
                }
                /* Send the START message */
                FormStartMsg (i4ChnlNum, BEEP_DIGEST_MD5_PROFILE,
                              (UINT1 *) pi1Buf);
                BeepAddMimeHdr (pi1Buf);
                BeepMakeFrame (BEEP_MSG, pChnlNode, pi1Buf);
                BpCltSendToServer (pSsnNode, pi1Buf);
                MemReleaseMemBlock(BPCLT_FRAME_POOL_ID, (UINT1 *)pi1Buf);
                pChnlNode->i4ProfileState = BPCLT_MD5_ON_PROGRESS;

                pBpMsg = (tBpCltMsg *) MemAllocMemBlk (BPCLT_SRV_POOL_ID);

                if (pBpMsg == NULL)
                {
                    return BPCLT_FAILURE;
                }

                TMO_SLL_Add (&(pSsnNode->BpCltMsgList), &pBpMsg->Next);
                pBpMsg->i4ChnlNo = i4ChnlNum;
                pBpMsg->i4Msg = BPCLT_START_MSG;
                pBpMsg->i4MsgNo = pChnlNode->i4LastMsg;
                return BPCLT_SUCCESS;
            }

            /* if RAW profile required send the START message for 
             * RAW channel */

            if (pSsnNode->u4ProfileBitMask & BPCLT_RAW_REQUIRED)
            {
                /* Get the next avaialable channel for MD5 profile */
                if (BpCltGetNewChnlNum (pSsnNode, (UINT4 *) &i4ChnlNum)
                    == BPCLT_FAILURE)
                {
                    BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC,
                               BPCLT_NAME, "Couldnot get the new channel !!");
                    MemReleaseMemBlock(BPCLT_FRAME_POOL_ID, (UINT1 *)pi1Buf);
                    return BPCLT_UNEXPECTED;
                }
                /* Send the START message */
                FormStartMsg (i4ChnlNum, BEEP_RAW_PROFILE, (UINT1 *) pi1Buf);
                BeepAddMimeHdr (pi1Buf);
                BeepMakeFrame (BEEP_MSG, pChnlNode, pi1Buf);
                BpCltSendToServer (pSsnNode, pi1Buf);
                MemReleaseMemBlock(BPCLT_FRAME_POOL_ID, (UINT1 *)pi1Buf);
                
		pChnlNode->i4ProfileState = BPCLT_APPLN_ON_PROGRESS;
                pBpMsg = (tBpCltMsg *) MemAllocMemBlk (BPCLT_SRV_POOL_ID);

                if (pBpMsg == NULL)
                {
                    return BPCLT_FAILURE;
                }

                TMO_SLL_Add (&(pSsnNode->BpCltMsgList), &pBpMsg->Next);
                pBpMsg->i4ChnlNo = i4ChnlNum;
                pBpMsg->i4Msg = BPCLT_START_MSG;
                pBpMsg->i4MsgNo = pChnlNode->i4LastMsg;
                return BPCLT_SUCCESS;
            }
            /* Session node profile not matched, so send close on chnl 0 */
            i4ChnlNum = BEEP_ZERO;
            u4ReplyCode = BPCLT_SERVICE_NOT_AVAILABLE;
            FormCloseMsg (BEEP_ZERO, u4ReplyCode, (UINT1 *) pi1Buf);
            BeepAddMimeHdr (pi1Buf);
            BeepMakeFrame (BEEP_ERR, pChnlNode, pi1Buf);
            BpCltSendToServer (pSsnNode, pi1Buf);

            pChnlNode->i4ProfileState = BPCLT_APPLN_ON_PROGRESS;
            MemReleaseMemBlock(BPCLT_FRAME_POOL_ID, (UINT1 *)pi1Buf);
            return BPCLT_SUCCESS;
        }
    }

    MemReleaseMemBlock(BPCLT_FRAME_POOL_ID, (UINT1 *)pi1Buf);
    BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC, BPCLT_NAME,
               "Frame with unknown frame type Received Closing Session !!");
    return BPCLT_UNEXPECTED;
}

/**************************************************************************/
/*   Function Name   : BpCltProcessPktOnTLSProgress                       */
/*                                                                        */
/*   Description     : This functions process the packet when the profile */
/*                     state is start for TLS profile sent.               */
/*                     If the reply frame is received for a profile, it   */
/*                     checks proceed or error received, if proceed       */
/*                     enables encryption in raw chnl, else sends err msg */
/*                                                                        */
/*   Input(s)        : pRcvFrameHdr - Pointer to the received frame hdr   */
/*                     pi1RcvFrame - Pointer to the received frame from   */
/*                                   beep server                          */
/*                     i4SockId     - Socket Id of the received session   */
/*                                                                        */
/*   Output(s)       : None                                               */
/*                                                                        */
/*   Return Value    : BPCLT_SUCCESS  for successful completion           */
/*                     BPCLT_FAILURE   for failure                        */
/**************************************************************************/

INT4
BpCltProcessPktOnTLSProgress (tBpCltFrameHdr * pRcvFrameHdr,
                              tBpCltMgmtChnlData * pMgmtData, INT4 i4SockId)
{
    INT1                *pi1Buf=NULL;
    INT1                ai1TLSProfile[BPCLT_MAX_PROFILE_SIZE];
    UINT4               u4ReplyCode = BEEP_ZERO;
    tBpCltChnlNode     *pChnlNode = NULL;
    tBpCltSsnNode      *pSsnNode = NULL;

    MEMSET (ai1TLSProfile, BEEP_ZERO, BPCLT_MAX_PROFILE_SIZE);

    if (pRcvFrameHdr == NULL || pMgmtData == NULL)
    {
        return BPCLT_FAILURE;
    }

    pSsnNode = BpCltGetSessionNode (NULL, i4SockId);
    if (pSsnNode == NULL)
    {
        return BPCLT_FAILURE;
    }

    pChnlNode = BpCltGetChannelNode (pSsnNode, pRcvFrameHdr->i4ChnlNum);
    if (pChnlNode == NULL)
    {
        return BPCLT_FAILURE;
    }

    if (pRcvFrameHdr->i4FrameHeadType == BEEP_RPY_FRAME)
    {
        /* Check that the reply frame is for TLS profile and 
         * proceed is received */
        if (pMgmtData->i4KeyElementType == BPCLT_PROFILE_TYPE)
        {
            if ((pMgmtData->i4ProfCount < BEEP_ONE) &&
                (MEMCMP (pMgmtData->ai1Profile, ai1TLSProfile,
                         sizeof (ai1TLSProfile)) == BEEP_ZERO) &&
                (pMgmtData->Cdata.i4ElementCode == BPCLT_ELEMENT_CODE_PROCEED))
            {
                return BPCLT_TLS_OK;
            }
        }
        else
        {
            BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC, BPCLT_NAME,
                       "Invalid Profile received !!");
            return BPCLT_UNEXPECTED;
        }

    }
    else if (pRcvFrameHdr->i4FrameHeadType == BEEP_MSG_FRAME)
    {
        /* Check if close is received for chnl 0 */
        if ((pMgmtData->i4KeyElementType == BPCLT_CLOSE_TYPE) &&
            (pMgmtData->i1CdataRcvd == BEEP_ZERO))
        {
	    pi1Buf = (INT1 *)MemAllocMemBlk(BPCLT_FRAME_POOL_ID);
    	    if (pi1Buf == NULL)
    	    {
        	return BPCLT_FAILURE;
    	    }
    	    MEMSET (pi1Buf, BEEP_ZERO, BEEP_MAX_FRAME_SIZE);

            /* check if the close code is 200 (success) */
            if (pMgmtData->i4Code != BPCLT_SERVICE_SUCCESS)
            {
                /* Send the RPY frame with OK */
                FormOkMsg ((UINT1 *) pi1Buf);
                BeepAddMimeHdr (pi1Buf);
                BeepMakeFrame (BEEP_RPY, pChnlNode, pi1Buf);
                BpCltSendToServer (pSsnNode, pi1Buf);
            }
            else
            {
                /* Send ERR frame with element code 550 */
                u4ReplyCode = BPCLT_ACTION_NOT_TAKEN;
                FormErrMsg (u4ReplyCode, NULL, (UINT1 *) pi1Buf);
                BeepAddMimeHdr (pi1Buf);
                BeepMakeFrame (BEEP_ERR, pChnlNode, pi1Buf);
                BpCltSendToServer (pSsnNode, pi1Buf);

            }
            MemReleaseMemBlock(BPCLT_FRAME_POOL_ID, (UINT1 *)pi1Buf);
            return BPCLT_SUCCESS;
        }
        else
        {
            BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC, BPCLT_NAME,
                       "Frame with unknown frame type Received:Closing Session !!");
            return BPCLT_UNEXPECTED;
        }
    }
    else
    {
        BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC, BPCLT_NAME,
                   "Invalid Profile received !!");
        return BPCLT_UNEXPECTED;
    }
    return BPCLT_SUCCESS;
}

/**************************************************************************/
/*   Function Name   :BpCltProcessPktOnMD5Progress                        */
/*                                                                        */
/*   Description     : This functions process the packet when the profile */
/*                     state is MD5 start msg sent                        */
/*                     If the reply frame is received with MD5 challange  */
/*                     it calculates the MD5 digest and sends to server   */
/*                                                                        */
/*   Input(s)        : pRcvFrameHdr - Pointer to the received frame hdr   */
/*                     pi1RcvFrame - Pointer to the received frame from   */
/*                                   beep server                          */
/*                     i4SockId - Socket Id of the received session      */
/*                                                                        */
/*   Output(s)       : None                                               */
/*                                                                        */
/*   Return Value    : BPCLT_SUCCESS  for successful completion           */
/*                     BPCLT_FAILURE   for failure                        */
/**************************************************************************/

INT4
BpCltProcessPktOnMD5Progress (tBpCltFrameHdr * pRcvFrameHdr,
                              tBpCltMgmtChnlData * pMgmtData, INT4 i4SockId)
{
    INT1                *pi1Buf = NULL;
    INT1                ai1MD5Profile[BPCLT_MAX_PROFILE_SIZE];
    UINT1               au1AuthKey[BPCLT_MAX_AUTHKEY_SIZE];
    UINT4               u4ReplyCode = BEEP_ZERO;
    tBpCltChnlNode     *pChnlNode = NULL;
    tBpCltSsnNode      *pSsnNode = NULL;

    MEMSET (ai1MD5Profile, BEEP_ZERO, BPCLT_MAX_PROFILE_SIZE);
    MEMSET (au1AuthKey, BEEP_ZERO, BPCLT_MAX_AUTHKEY_SIZE);

    if (pRcvFrameHdr == NULL || pMgmtData == NULL)
    {
        return BPCLT_FAILURE;
    }

    pSsnNode = BpCltGetSessionNode (NULL, i4SockId);
    if (pSsnNode == NULL)
    {
        return BPCLT_FAILURE;
    }
    if (pRcvFrameHdr->i4ChnlNum >= BEEP_MAX_CHANNELS)
    {
        return BPCLT_UNEXPECTED;
    }

    pChnlNode = BpCltGetChannelNode (pSsnNode, pRcvFrameHdr->i4ChnlNum);
    if (pChnlNode == NULL)
    {
        return BPCLT_FAILURE;
    }

    pi1Buf = (INT1 *)MemAllocMemBlk(BPCLT_FRAME_POOL_ID);
    if (pi1Buf == NULL)
    {
        return BPCLT_FAILURE;
    }
    MEMSET (pi1Buf, BEEP_ZERO, BEEP_MAX_FRAME_SIZE);
    if (pRcvFrameHdr->i4FrameHeadType == BEEP_RPY_FRAME)
    {
        /* Check that the reply frame is for MD5 profile and 
         * blob element received */
        if (pMgmtData->i4KeyElementType == BPCLT_PROFILE_TYPE)
        {
            if ((pMgmtData->i4ProfCount < BEEP_ONE) &&
                (MEMCMP (pMgmtData->ai1Profile, ai1MD5Profile,
                         sizeof (ai1MD5Profile)) == BEEP_ZERO) &&
                (pMgmtData->i1CdataRcvd == BPCLT_RPY_BLOB))
            {
                /*  Generate MD5 digest using auth key for this server and 
                 *  challenge received from    server in blob element */
                /* TODO It can be made common */
                if (BpCltGetAuthKey (&pSsnNode->SrvAddr, au1AuthKey) ==
                    BPCLT_FAILURE)
                {
                    BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC, BPCLT_NAME,
                               "Error in getting the Auth Key!!");
                    MemReleaseMemBlock(BPCLT_FRAME_POOL_ID, (UINT1 *)pi1Buf);
                    return BPCLT_UNEXPECTED;

                }
                BpCltGenerateMD5Digest (au1AuthKey, pMgmtData->Cdata.au1Blob);
                /* Create the Channel DB for MD5 profile and initialize it */
                BpCltCreateMD5Channel (pRcvFrameHdr->i4ChnlNum, pSsnNode);
                /* Send the MD5 digest in blob element */
                FormBlobMsg (pMgmtData->Cdata.au1Blob, (UINT1 *) pi1Buf);
                BeepAddMimeHdr (pi1Buf);
                BeepMakeFrame (BEEP_ERR, pChnlNode, pi1Buf);
                BpCltSendToServer (pSsnNode, pi1Buf);
                MemReleaseMemBlock(BPCLT_FRAME_POOL_ID, (UINT1 *)pi1Buf);
                return BPCLT_SUCCESS;
            }

        }
        /* Check if rhe keyelement is OK */
        else if (pMgmtData->i4KeyElementType == BPCLT_OK_TYPE)
        {
            BpCltOkElementHdlr (pSsnNode, pRcvFrameHdr);
        }
    }
    else if (pRcvFrameHdr->i4FrameHeadType == BEEP_MSG_FRAME)
    {
        /* Check if close is received for chnl 0 */
        if ((pMgmtData->i4KeyElementType == BPCLT_CLOSE_TYPE) &&
            (pMgmtData->i1CdataRcvd == BEEP_ZERO))
        {
            /* check if the close code is 200 (success) */
            if (pMgmtData->i4Code != BPCLT_SERVICE_SUCCESS)
            {
                /* Send the RPY frame with OK */
                FormOkMsg ((UINT1 *) pi1Buf);
                BeepAddMimeHdr (pi1Buf);
                BeepMakeFrame (BEEP_RPY, pChnlNode, pi1Buf);
                BpCltSendToServer (pSsnNode, pi1Buf);
            }
            else
            {
                /* Send ERR frame with element code 550 */
                u4ReplyCode = BPCLT_ACTION_NOT_TAKEN;
                FormErrMsg (u4ReplyCode, NULL, (UINT1 *) pi1Buf);

                BeepAddMimeHdr (pi1Buf);
                BeepMakeFrame (BEEP_ERR, pChnlNode, pi1Buf);
                BpCltSendToServer (pSsnNode, pi1Buf);
            }
        }
        else
        {
            BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC, BPCLT_NAME,
                       "Frame with unknown frame type"
                       "Received:Closing Session!!");
            MemReleaseMemBlock(BPCLT_FRAME_POOL_ID, (UINT1 *)pi1Buf);
            return BPCLT_UNEXPECTED;
        }
    }
    else
    {
        BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC, BPCLT_NAME,
                   "Invalid Profile receved !!");
        MemReleaseMemBlock(BPCLT_FRAME_POOL_ID, (UINT1 *)pi1Buf);
        return BPCLT_UNEXPECTED;
    }
    MemReleaseMemBlock(BPCLT_FRAME_POOL_ID, (UINT1 *)pi1Buf);
    return BPCLT_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : BpCltProcessPktOnAppProgress                       */
/*   Description     : This functions is used to handle the Mgmt channel  */
/*                     funtionalities                                     */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : Pointer to a session if the session is created     */
/*                     successfully, NULL otherwise                       */
/**************************************************************************/

INT4
BpCltProcessPktOnAppProgress (tBpCltFrameHdr * pRcvFrameHdr,
                              tBpCltMgmtChnlData * pMgmtData, INT4 i4SockId)
{

    INT1                *pi1Buf = NULL;
    INT4                i4Profile = BEEP_ZERO;
    UINT4               u4Flag = BEEP_ZERO;
    INT4                i4ChnlNum = BEEP_ZERO;
    UINT4               u4ReplyCode = BEEP_ZERO;
    tBpCltChnlNode     *pChnlNode = NULL;
    tBpCltChnlNode     *pTmpChnlNode = NULL;
    tBpCltSsnNode      *pSsnNode = NULL;
    tBpCltMsg          *pBpMsg = NULL;
    tBpCltMsg          *pTmpMsg = NULL;


    if (pRcvFrameHdr == NULL || pMgmtData == NULL)
    {
        return BPCLT_FAILURE;
    }

    pSsnNode = BpCltGetSessionNode (NULL, i4SockId);
    if (pSsnNode == NULL)
    {
        return BPCLT_FAILURE;
    }

    pChnlNode = BpCltGetChannelNode (pSsnNode, pRcvFrameHdr->i4ChnlNum);
    if (pChnlNode == NULL)
    {
        return BPCLT_FAILURE;
    }

    i4ChnlNum = pChnlNode->i4ChnlNum;

    if (pRcvFrameHdr->i4FrameHeadType == BEEP_RPY_FRAME)
    {
        /* Check that the reply frame is for MD5 profile and 
         * blob element received */
        if (pMgmtData->i4KeyElementType == BPCLT_PROFILE_TYPE)
        {
            /* Look for the start entry in message db for channel 0 */
            TMO_SLL_Scan (&(pSsnNode->BpCltMsgList), pTmpMsg, tBpCltMsg *)
            {
                pBpMsg = pTmpMsg;
                if ((pRcvFrameHdr->i4MsgNo == pBpMsg->i4MsgNo) &&
                    (pBpMsg->i4Msg == BPCLT_START_MSG))
                {
                    /* Check if the profile is supported and call the 
                       profile element handler */
                    if (BpCltGetProfilesSupported
                        (pSsnNode->u4ProfileBitMask,
                         pMgmtData->ai1Profile[BEEP_ZERO],
                         &i4Profile) == BPCLT_SUCCESS)
                    {
                        TMO_SLL_Delete (&(pSsnNode->BpCltMsgList),
                                        &pBpMsg->Next);
                        if (BpCltProfileElementHdlr (pSsnNode, pBpMsg->i4ChnlNo,
                                                     i4Profile) ==
                            BPCLT_SUCCESS)
                        {
                            MemReleaseMemBlock (BPCLT_SRV_POOL_ID,
                                                (UINT1 *) pBpMsg);
                            return BPCLT_SUCCESS;
                        }
                        MemReleaseMemBlock (BPCLT_SRV_POOL_ID,
                                            (UINT1 *) pBpMsg);
                    }

                }
            }

        }
        else if (pMgmtData->i4KeyElementType == BPCLT_OK_TYPE)
        {
            /* Look for the close entry in message db for channel 0 */
            TMO_SLL_Scan (&(pSsnNode->BpCltMsgList), pTmpMsg, tBpCltMsg *)
            {
                pBpMsg = pTmpMsg;
                if ((pBpMsg->i4ChnlNo == BEEP_ZERO) &&
                    (pBpMsg->i4Msg == BPCLT_CLOSE_MSG))
                {
                    /* BpCltOkHandler( ); */
                    BpCltForceCloseSsn (pSsnNode->i4SockId);
                    return BPCLT_SUCCESS;

                }
            }
        }
        BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC, BPCLT_NAME,
                   "Invalid Profile received !!");
        return BPCLT_UNEXPECTED;

    }
    else if (pRcvFrameHdr->i4FrameHeadType == BEEP_MSG_FRAME)
    {
        if (pMgmtData->i4KeyElementType == BPCLT_START_TYPE)
        {
            pi1Buf = (INT1 *)MemAllocMemBlk(BPCLT_FRAME_POOL_ID);
            if (pi1Buf == NULL)
            {
                return BPCLT_FAILURE;
            }
            MEMSET (pi1Buf, BEEP_ZERO, BEEP_MAX_FRAME_SIZE);
            if (BpCltCheckProfilesSupported
                (pMgmtData->i4ProfCount, pSsnNode->u4ProfileBitMask,
                 pMgmtData->ai1Profile[BEEP_ZERO]) != BPCLT_SUCCESS)
            {
                /* Send the CLOSE message with Reply code service not 
                 * available ie. 421*/
                i4ChnlNum = BEEP_ZERO;
                u4ReplyCode = BPCLT_SERVICE_NOT_AVAILABLE;
                FormCloseMsg (BEEP_ZERO, u4ReplyCode, (UINT1 *) pi1Buf);
                BeepAddMimeHdr (pi1Buf);
                BeepMakeFrame (BEEP_ERR, pChnlNode, pi1Buf);
                BpCltSendToServer (pSsnNode, pi1Buf);
            }

            /* if TLS encryption required, send the START message */
            if (pSsnNode->u4ProfileBitMask & BPCLT_TLS_REQUIRED)
            {
                /* Get the next avaialable channel for TLS profile */
                if (BpCltGetNewChnlNum (pSsnNode, (UINT4 *) &i4ChnlNum)
                    == BPCLT_FAILURE)
                {
                    BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC,
                               BPCLT_NAME, "Couldnot get the new channel !!");
                    MemReleaseMemBlock(BPCLT_FRAME_POOL_ID, (UINT1 *)pi1Buf);
                    return BPCLT_UNEXPECTED;
                }

                /* Send the START message */
                FormStartMsg (i4ChnlNum, BEEP_TLS_PROFILE, (UINT1 *) pi1Buf);
                BeepAddMimeHdr (pi1Buf);
                BeepMakeFrame (BEEP_RPY, pChnlNode, pi1Buf);
                BpCltSendToServer (pSsnNode, pi1Buf);
                pChnlNode->i4ProfileState = BPCLT_TLS_ON_PROGRESS;
                MemReleaseMemBlock(BPCLT_FRAME_POOL_ID, (UINT1 *)pi1Buf);
                return BPCLT_SUCCESS;
            }

            /* if Authentication required with MD5, send the START message */
            if (pSsnNode->u4ProfileBitMask & BPCLT_MD5_REQUIRED)
            {
                /* Get the next avaialable channel for MD5 profile */
                if (BpCltGetNewChnlNum (pSsnNode, (UINT4 *) &i4ChnlNum)
                    == BPCLT_FAILURE)
                {
                    BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC,
                               BPCLT_NAME, "Couldnot get the new channel !!");
                    MemReleaseMemBlock(BPCLT_FRAME_POOL_ID, (UINT1 *)pi1Buf);
                    return BPCLT_UNEXPECTED;
                }

                /* Send the START message */
                FormStartMsg (i4ChnlNum, BEEP_DIGEST_MD5_PROFILE,
                              (UINT1 *) pi1Buf);
                BeepAddMimeHdr (pi1Buf);
                BeepMakeFrame (BEEP_RPY, pChnlNode, pi1Buf);
                BpCltSendToServer (pSsnNode, pi1Buf);
                pChnlNode->i4ProfileState = BPCLT_MD5_ON_PROGRESS;
                MemReleaseMemBlock(BPCLT_FRAME_POOL_ID, (UINT1 *)pi1Buf);
                return BPCLT_SUCCESS;
            }

            /* if RAW profile required send the START message for 
             * RAW channel */
            if (pChnlNode->i4ProfileState & BPCLT_RAW_REQUIRED)
            {
                /* Get the next avaialable channel for MD5 profile */
                if (BpCltGetNewChnlNum (pSsnNode, (UINT4 *) &i4ChnlNum)
                    == BPCLT_FAILURE)
                {
                    BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC,
                               BPCLT_NAME, "Couldnot get the new channel !!");
                    MemReleaseMemBlock(BPCLT_FRAME_POOL_ID, (UINT1 *)pi1Buf);
                    return BPCLT_UNEXPECTED;
                }
                /* Send the START message */
                FormStartMsg (i4ChnlNum, BEEP_RAW_PROFILE, (UINT1 *) pi1Buf);
                BeepAddMimeHdr (pi1Buf);
                BeepMakeFrame (BEEP_MSG, pChnlNode, pi1Buf);
                BpCltSendToServer (pSsnNode, pi1Buf);
                pChnlNode->i4ProfileState = BPCLT_APPLN_ON_PROGRESS;
                MemReleaseMemBlock(BPCLT_FRAME_POOL_ID, (UINT1 *)pi1Buf);
                return BPCLT_SUCCESS;
            }

            /* Session node profile not matched, so send close on chnl 0 */
            i4ChnlNum = BEEP_ZERO;
            u4ReplyCode = BPCLT_SERVICE_NOT_AVAILABLE;
            FormCloseMsg (BEEP_ZERO, u4ReplyCode, (UINT1 *) pi1Buf);
            BeepAddMimeHdr (pi1Buf);
            BeepMakeFrame (BEEP_ERR, pChnlNode, pi1Buf);
            BpCltSendToServer (pSsnNode, pi1Buf);

            pChnlNode->i4ProfileState = BPCLT_APPLN_ON_PROGRESS;
            MemReleaseMemBlock(BPCLT_FRAME_POOL_ID, (UINT1 *)pi1Buf);
            return BPCLT_SUCCESS;
        }
        else if (pMgmtData->i4KeyElementType == BPCLT_CLOSE_TYPE)
        {
            if (pMgmtData->u4Num >= BEEP_MAX_CHANNELS)
            {
                return BPCLT_UNEXPECTED;
            }

            pTmpChnlNode = BpCltGetChannelNode (pSsnNode, pMgmtData->u4Num);
            if (pTmpChnlNode != NULL)
            {
                return (BpCltCloseEltHdlr (pSsnNode, pMgmtData, pRcvFrameHdr));
            }
        }
        else
        {
            BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC, BPCLT_NAME,
                       "Invalid Profile received !!");
            return BPCLT_UNEXPECTED;
        }
    }
    else if (pRcvFrameHdr->i4FrameHeadType == BEEP_ERR)
    {
        if (pMgmtData->i4KeyElementType == BEEP_ERR)
        {
            /* Look for the start entry in message db for channel 0 */
            TMO_SLL_Scan (&(pSsnNode->BpCltMsgList), pTmpMsg, tBpCltMsg *)
            {
                pBpMsg = pTmpMsg;
                if ((pBpMsg->i4ChnlNo == BEEP_ZERO) &&
                    (pBpMsg->i4Msg == BPCLT_START_MSG))
                {
                    /* Error received for the start element sent, revert back
                       to previous state */
                    u4Flag = BPCLT_FOUND;
                    break;
                }
            }

            if (u4Flag == BPCLT_FOUND)
            {
                pChnlNode->i4ProfileState =
                    pChnlNode->i4ProfileState - BEEP_ONE;
                TMO_SLL_Delete (&pSsnNode->BpCltMsgList, &pBpMsg->Next);
                MemReleaseMemBlock (BPCLT_SRV_POOL_ID, (UINT1 *) pBpMsg);
                return BPCLT_SUCCESS;
            }
        }
        return BPCLT_UNEXPECTED;
    }
    return BPCLT_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : BpCltProcessPktOnCloseSent                         */
/*   Description     : This functions is used to handle the Mgmt channel  */
/*                     funtionalities                                     */
/*                                                                        */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : Pointer to a session if the session is created     */
/*                     successfully, NULL otherwise                       */
/**************************************************************************/
INT4
BpCltProcessPktOnCloseSent (tBpCltFrameHdr * pRcvFrameHdr,
                            tBpCltMgmtChnlData * pMgmtData,
                            INT4 i4SockId, tBpCltSsnNode * pSsnNode)
{
    UNUSED_PARAM (i4SockId);

    if (pRcvFrameHdr->i4FrameHeadType == BEEP_MSG_FRAME)
    {
        if (pMgmtData->i4KeyElementType == BPCLT_CLOSE_TYPE)
        {
            return (BpCltCloseEltHdlr (pSsnNode, pMgmtData, pRcvFrameHdr));
        }
        else if (pMgmtData->i4KeyElementType == BPCLT_START_TYPE)
        {
            return (BpCltStartEltHdlr (pSsnNode, pMgmtData, pRcvFrameHdr));
        }
    }
    else if (pRcvFrameHdr->i4FrameHeadType == BEEP_RPY_FRAME)
    {
        if (pMgmtData->i4KeyElementType == BPCLT_OK_TYPE)
        {
            return (BpCltOkElementHdlr (pSsnNode, pRcvFrameHdr));
        }
        else if (pMgmtData->i4KeyElementType == BPCLT_PROFILE_TYPE)
        {
            return BPCLT_UNEXPECTED;
        }
    }
    else if (pRcvFrameHdr->i4FrameHeadType == BEEP_ERR_FRAME)
    {
        return BPCLT_UNEXPECTED;
    }

    BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC, BPCLT_NAME,
               "Invalid Profile received !!");
    return BPCLT_UNEXPECTED;
}

/**************************************************************************/
/*   Function Name   : BpCltGetProfilesSupported                          */
/*   Description     : This Gets the profiles required for beep client    */
/*                     supported by server                                */
/*                                                                        */
/*   Input(s)        : u4BitMask - Profile list supported for this session*/
/*                     pi1Profile - Profile received from server          */
/*                     pi4Profile - Pointer to profile in pi1Profile      */
/*   Output(s)       : None                                               */
/*   Return Value    : BPCLT_SUCCESS or BPCLT_FAILURE                     */
/**************************************************************************/
INT4
BpCltGetProfilesSupported (UINT4 u4BitMask, INT1 *pi1Profile, INT4 *pi4Profile)
{
    INT1                ai1Profile[BPCLT_MAX_PROFILE_SIZE];
    /* BEEP_RAW_PROFILE */ ;

    MEMSET (ai1Profile, BEEP_ZERO, BPCLT_MAX_PROFILE_SIZE);
    SPRINTF ((CHR1 *) ai1Profile, BEEP_RAW_NAME);

    if (STRCMP (pi1Profile, ai1Profile) == BEEP_ZERO)
    {
        if (u4BitMask & BPCLT_RAW_REQUIRED)
        {
            *pi4Profile = BEEP_RAW_PROFILE;
            return BPCLT_SUCCESS;
        }
    }

    MEMSET (ai1Profile, BEEP_ZERO, BPCLT_MAX_PROFILE_SIZE);
    SPRINTF ((CHR1 *) ai1Profile, BEEP_TLS_NAME);

    if (STRCMP (pi1Profile, ai1Profile) == BEEP_ZERO)
    {
        if (u4BitMask & BPCLT_TLS_REQUIRED)
        {
            *pi4Profile = BEEP_TLS_PROFILE;
            return BPCLT_SUCCESS;
        }
    }

    MEMSET (ai1Profile, BEEP_ZERO, BPCLT_MAX_PROFILE_SIZE);
    SPRINTF ((CHR1 *) ai1Profile, BEEP_DIGEST_MD5_NAME);

    if (STRCMP (pi1Profile, ai1Profile) == BEEP_ZERO)
    {
        if (u4BitMask & BPCLT_MD5_REQUIRED)
        {
            *pi4Profile = BEEP_DIGEST_MD5_PROFILE;
            return BPCLT_SUCCESS;
        }
    }
    return BPCLT_FAILURE;
}

/**************************************************************************/
/*   Function Name   : BpCltCheckProfilesSupported                        */
/*   Description     : This checks the profiles required for beep client  */
/*                     supported by server or not                         */
/*                                                                        */
/*   Input(s)        : u4ProfileCnt - Profile Cnt from Server message     */
/*                     u4ProfileBitMask - Profile bit mask supported      */
/*                     pi1Profile - Profile received from server          */
/*   Output(s)       : None                                               */
/*   Return Value    : BPCLT_SUCCESS or BPCLT_FAILURE                     */
/**************************************************************************/
INT4
BpCltCheckProfilesSupported (UINT4 u4ProfileCnt, UINT4 u4ProfileBitMask,
                             INT1 *pi1Profile)
{
    INT1                ai1RawProfile[BPCLT_MAX_PROFILE_SIZE];
    /* BEEP_RAW_PROFILE */
    INT1                ai1TlsProfile[BPCLT_MAX_PROFILE_SIZE];
    /* BEEP_TLS_PROFILE */
    INT1                ai1Md5Profile[BPCLT_MAX_PROFILE_SIZE];
    /* BEEP_MD5_PROFILE */
    UINT4               u4BitMask = BEEP_ZERO;
    UINT4               u4Cnt = BEEP_ZERO;

    MEMSET (ai1RawProfile, BEEP_ZERO, BPCLT_MAX_PROFILE_SIZE);
    MEMSET (ai1TlsProfile, BEEP_ZERO, BPCLT_MAX_PROFILE_SIZE);
    MEMSET (ai1Md5Profile, BEEP_ZERO, BPCLT_MAX_PROFILE_SIZE);

    SPRINTF ((CHR1 *) ai1RawProfile, BEEP_RAW_NAME);
    SPRINTF ((CHR1 *) ai1TlsProfile, BEEP_TLS_NAME);
    SPRINTF ((CHR1 *) ai1Md5Profile, BEEP_DIGEST_MD5_NAME);

    for (; u4Cnt < u4ProfileCnt; u4Cnt++)
    {
        if (STRCMP (pi1Profile, ai1RawProfile) == BEEP_ZERO)
        {
            u4BitMask = u4BitMask | BPCLT_RAW_REQUIRED;
            pi1Profile += BEEP_MAX_CHAR;
            continue;
        }
        if (STRCMP (pi1Profile, ai1TlsProfile) == BEEP_ZERO)
        {
            u4BitMask = u4BitMask | BPCLT_TLS_REQUIRED;
            pi1Profile += BEEP_MAX_CHAR;
            continue;
        }
        if (STRCMP (pi1Profile, ai1Md5Profile) == BEEP_ZERO)
        {
            u4BitMask = u4BitMask | BPCLT_MD5_REQUIRED;
            pi1Profile += BEEP_MAX_CHAR;
            /*pi1Profile = pi1Profile + sizeof(BPCLT_MD5_REQUIRED); */
            continue;
        }
    }
    if (u4BitMask == u4ProfileBitMask)
    {
        return BPCLT_SUCCESS;
    }
    return BPCLT_FAILURE;
}

/**************************************************************************/
/*   Function Name   : BpCltGetNewChnlNum                                 */
/*   Description     : This gives the new channel number to create a      */
/*                     channel                                            */
/*                                                                        */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : BPCLT_SUCCESS or BPCLT_FAILURE                     */
/**************************************************************************/
INT4
BpCltGetNewChnlNum (tBpCltSsnNode * pSsnNode, UINT4 *pChnlNum)
{
    tBpCltMsg          *pBpMsg = NULL;
    tBpCltMsg          *pTmpMsg = NULL;
    UINT4               u4Cnt = BEEP_ONE;
    INT4                i4Avail = BEEP_ZERO;

    /* Get the free channel number */
    for (; u4Cnt < BEEP_MAX_CHANNELS; u4Cnt = u4Cnt + BEEP_TWO)
    {
        /* Check that the any message in message db has that channel number */
        if (pSsnNode->aBpCltChnlList[u4Cnt] == NULL)
        {
            TMO_SLL_Scan (&(pSsnNode->BpCltMsgList), pTmpMsg, tBpCltMsg *)
            {
                pBpMsg = pTmpMsg;
                if (pBpMsg->i4ChnlNo == (INT4) u4Cnt)
                {
                    i4Avail = BEEP_ONE;
                }
            }
            if (i4Avail == BEEP_ZERO)
            {
                break;
            }
        }
        i4Avail = BEEP_ZERO;
    }

    if (u4Cnt == BEEP_MAX_CHANNELS)
    {
        return BPCLT_FAILURE;
    }

    *pChnlNum = u4Cnt;
    return BPCLT_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : BpCltProfileElementHdlr                            */
/*   Description     : This function handles the profile element in RPY   */
/*                     frame in chnl 0 when the chnl 0 state is appln     */
/*                     on progress                                        */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : BPCLT_SUCCESS or BPCLT_FAILURE                     */
/**************************************************************************/

INT4
BpCltProfileElementHdlr (tBpCltSsnNode * pSsnNode, INT4 i4ChnlNum,
                         INT4 i4Profile)
{

    tBpCltChnlNode     *pChnlNode = NULL;

    pChnlNode = BpCltCreateChannel (i4ChnlNum, i4Profile);

    if (i4Profile == BEEP_RAW_PROFILE)
    {
        pSsnNode->u4ProfileBitMask &= ~BPCLT_RAW_REQUIRED;
    }

    if (i4Profile == BEEP_TLS_PROFILE)
    {
        pSsnNode->u4ProfileBitMask &= ~BPCLT_TLS_REQUIRED;
    }

    if (i4Profile == BEEP_DIGEST_MD5_PROFILE)
    {
        pSsnNode->u4ProfileBitMask &= ~BPCLT_MD5_REQUIRED;
    }

    if (pChnlNode != NULL)
    {
        BpCltAddChannel (pChnlNode, pSsnNode);
        return BPCLT_SUCCESS;
    }
    return BPCLT_FAILURE;
}

/**************************************************************************/
/*   Function Name   : BpCltOkElementHdlr                                 */
/*   Description     : This function handles the situation when a ok      */
/*                     element is received and decides on closing  a      */
/*                     channel                                            */
/*   Input(s)        : Frame Payload details and Frame Hdr Details        */
/*   Output(s)       : None                                               */
/*   Return Value    : BPSRV_SUCCESS or BPSRV_FAILURE                     */
/**************************************************************************/

INT4
BpCltOkElementHdlr (tBpCltSsnNode * pSsnNode, tBpCltFrameHdr * pFrameHdr)
{
    INT1                i1FoundFlg = BPCLT_NOT_FOUND;
    INT4                i4ChnlNo = BEEP_ZERO;
    tBpCltMsg          *pMsg = NULL;
    tBpCltMsg          *pTmp = NULL;

    TMO_SLL_Scan (&(pSsnNode->BpCltMsgList), pTmp, tBpCltMsg *)
    {
        pMsg = pTmp;
        if ((pMsg->i4MsgNo == pFrameHdr->i4MsgNo) &&
            (pMsg->i4Msg == BPCLT_CLOSE_TYPE))
        {
            i1FoundFlg = BPCLT_FOUND;
            i4ChnlNo = pMsg->i4ChnlNo;
            break;
        }
    }

    if (i1FoundFlg != BPCLT_FOUND)
    {
        return BPCLT_UNEXPECTED;
    }
    else
    {
        if (i4ChnlNo == BEEP_ZERO)
        {
            return BPCLT_OVER;
        }
        TMO_SLL_Delete (&(pSsnNode->BpCltMsgList), &pMsg->Next);

        if ((i4ChnlNo < BEEP_MIN_CHNL_NUM) || (i4ChnlNo > BEEP_MAX_CHNL_NUM))
        {
            return BPCLT_FAILURE;
        }

        BpCltDeleteChannel (pSsnNode->aBpCltChnlList[i4ChnlNo], pSsnNode);
        return BPCLT_SUCCESS;
    }
}

/**************************************************************************/
/*   Function Name   : BpCltProcessTlsMsg                                 */
/*   Description     : This function processes the TLS message            */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : BPCLT_SUCCESS or BPCLT_FAILURE                     */
/**************************************************************************/
VOID
BpCltProcessTlsMsg (VOID)
{
    /* This should be filled when TLS support is provided to BEEP */
    return;
}

/**************************************************************************/
/*   Function Name   : BpBpCltGenerateMD5Digest                           */
/*   Description     : This function Creates digest                       */
/*   Input(s)        : pu1AuthKey, pu1Blob                                */
/*   Output(s)       : None                                               */
/*   Return Value    : BPCLT_SUCCESS or BPCLT_FAILURE                     */
/**************************************************************************/
INT4
BpCltGenerateMD5Digest (UINT1 *pu1AuthKey, UINT1 *pu1Blob)
{
    UNUSED_PARAM (pu1AuthKey);
    UNUSED_PARAM (pu1Blob);
    return BPCLT_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : BpCltCloseEltHdlr                                  */
/*   Description     : This function handles the receipt of close element */
/*   Input(s)        : pSsnNode, pTmpChnlNode                             */
/*   Output(s)       : None                                               */
/*   Return Value    : BPCLT_SUCCESS or BPCLT_FAILURE                     */
/**************************************************************************/

INT4
BpCltCloseEltHdlr (tBpCltSsnNode * pSsnNode, tBpCltMgmtChnlData * pChData,
                   tBpCltFrameHdr * pRcvFrameHdr)
{
    tBpCltMsg          *pMsg = NULL;
    UNUSED_PARAM (pRcvFrameHdr);

    FormOkMsg ((UINT1 *) pSsnNode->ai1WriteBuf);
    BeepAddMimeHdr (pSsnNode->ai1WriteBuf);
    BeepMakeFrame (BEEP_RPY_FRAME, pSsnNode->aBpCltChnlList[BEEP_ZERO],
                   pSsnNode->ai1WriteBuf);

    if (BPCLT_SUCCESS != BpCltSendToServer (pSsnNode, pSsnNode->ai1WriteBuf))
    {
        return BPCLT_TCP_ABORT;
    }
    else
    {
        if ((pChData->u4Num == BEEP_ZERO) ||
            (pChData->u4Num >= BEEP_MAX_CHANNELS) ||
            (pSsnNode->aBpCltChnlList[pChData->u4Num] == NULL))
        {
            return BPCLT_OVER;
        }
        else
        {
            MemReleaseMemBlock (BPCLT_CHNL_POOL_ID, (UINT1 *)
                                pSsnNode->aBpCltChnlList[pChData->u4Num]);
            pSsnNode->aBpCltChnlList[pChData->u4Num] = NULL;
            if (pSsnNode->u4ProfileBitMask == BEEP_ZERO)
            {
                FormCloseMsg (BEEP_ZERO, BEEP_SUCCESS_CLOSE,
                              (UINT1 *) pSsnNode->ai1WriteBuf);
                BeepAddMimeHdr (pSsnNode->ai1WriteBuf);
                BeepMakeFrame (BEEP_MSG_FRAME, pSsnNode->
                               aBpCltChnlList[BEEP_ZERO],
                               pSsnNode->ai1WriteBuf);
                if (BPCLT_SUCCESS != BpCltSendToServer (pSsnNode,
                                                        pSsnNode->ai1WriteBuf))
                {
                    return BPCLT_TCP_ABORT;
                }
                pMsg = (tBpCltMsg *) MemAllocMemBlk (BPCLT_SRV_POOL_ID);
                if (pMsg == NULL)
                {
                    return BPCLT_FAILURE;
                }
                pMsg->i4ChnlNo = BEEP_ZERO;
                pMsg->i4MsgNo = pSsnNode->aBpCltChnlList[BEEP_ZERO]->i4LastMsg;
                pMsg->i4Msg = BPCLT_CLOSE_MSG;
                TMO_SLL_Add (&(pSsnNode->BpCltMsgList), &pMsg->Next);
                return BPCLT_SUCCESS;
            }

        }
    }
    return BPCLT_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : BpCltStartEltHdlr                                  */
/*   Description     : This function handles the receipt of start element */
/*   Input(s)        : pSsnNode, pTmpChnlNode                             */
/*   Output(s)       : None                                               */
/*   Return Value    : BPCLT_SUCCESS or BPCLT_FAILURE                     */
/**************************************************************************/
INT4
BpCltStartEltHdlr (tBpCltSsnNode * pSsnNode,
                   tBpCltMgmtChnlData * pChData, tBpCltFrameHdr * pRcvFrameHdr)
{
    UNUSED_PARAM (pSsnNode);
    UNUSED_PARAM (pChData);
    UNUSED_PARAM (pRcvFrameHdr);
    return BPCLT_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : BpCltStartEltHdlr                                  */
/*   Description     : This function handles the receipt of start element */
/*   Input(s)        : pSsnNode, pTmpChnlNode                             */
/*   Output(s)       : None                                               */
/*   Return Value    : BPCLT_SUCCESS or BPCLT_FAILURE                     */
/**************************************************************************/
INT4                BpCltProfileEltHdlr
PROTO ((tBpCltSsnNode * pSsnNode,
        tBpCltMgmtChnlData * pChData, tBpCltFrameHdr * pFrameHdr))
{
    UNUSED_PARAM (pSsnNode);
    UNUSED_PARAM (pChData);
    UNUSED_PARAM (pFrameHdr);
    return BPCLT_SUCCESS;
}
