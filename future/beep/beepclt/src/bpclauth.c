/********************************************************************
 * Description:This file contains the functions to handle authentication
 * for Beep Client 
 *
 *******************************************************************/

#include "bpcltinc.h"

/**************************************************************************/
/*   Function Name   : BpCltGetAuthKey                                    */
/*   Description     : This functions is used to get the Auth Key for     */
/*                     server                                             */
/*                                                                        */
/*   Input(s)        : pSrvAddr - Beep server address                     */
/*                     pAuthKey - Authkey of the given server             */
/*   Output(s)       : None                                               */
/*   Return Value    : BPCLT_SUCCESS or BPCLT_FAILURE                     */
/**************************************************************************/

INT4
BpCltGetAuthKey (tIPvXAddr * pSrvAddr, UINT1 *pAuthKey)
{

    tBpCltAuthNode     *pAuthNode = NULL;

    TMO_SLL_Scan (&(gBpCltGblInfo.AuthKeyList), pAuthNode, tBpCltAuthNode *)
    {
        if (MEMCMP (&pAuthNode->SrvAddr, pSrvAddr,
                    sizeof (tIPvXAddr)) == BEEP_ZERO)
        {
            MEMCPY (pAuthKey, pAuthNode->au1SrvAuthKey, BPCLT_MAX_AUTHKEY_SIZE);
            return BPCLT_SUCCESS;
        }

    }
    return BPCLT_FAILURE;
}

/**************************************************************************/
/*   Function Name   : BpCltCreateMD5Channel                              */
/*   Description     : This function starts a new channel asssociated with*/
/*                     DIGEST-MD5 profile                                 */
/*   Input(s)        : i4ChnlNo - channel number of the channel to be     */
/*                     created                                            */
/*                     pSsnNode - Pointer to the session node where the   */
/*                     channel to be created                              */
/*   Output(s)       : None                                               */
/*   Return Value    : BPCLT_SUCCESS or BPCLT_FAILURE                     */
/**************************************************************************/

INT4
BpCltCreateMD5Channel (INT4 i4ChnlNo, tBpCltSsnNode * pSsnNode)
{
    UNUSED_PARAM (i4ChnlNo);
    UNUSED_PARAM (pSsnNode);
/*  NEEDED WHEN WE SUPPORT DIGEST MD5 PROFILE */
/*

    tBpCltChnlNode *pChnlNode = NULL;

    if (MemAllocateMemBlock (BPCLT_CHNL_POOL_ID, (UINT1 **)&pChnlNode) == 
        MEM_FAILURE)
    {
        BPCLT_TRC (BPCLT_TRC_FLAG, BPCLT_INIT_SHUT_TRC, BPCLT_NAME,
                   "Memory allocation for Chnl Node failed\n");
        return BPCLT_FAILURE;
    }

    pSsnNode->aBpCltChnlList[i4ChnlNo] = pChnlNode ;

    if (pSsnNode->aBpCltChnlList[i4ChnlNo] == NULL)
    {
        return BPCLT_FAILURE;
    }
    pChnlNode->i4ChnlNum = i4ChnlNo;
    pChnlNode->i4Profile = BEEP_DIGEST_MD5_PROFILE;
    pChnlNode->i4ProfileState = BPCLT_CHNL_OPENED;
    pChnlNode->i4LastMsg = 0;
    pChnlNode->i4LastRpy = 0;
    pChnlNode->u4SeqNoToSend = 0;
    pChnlNode->u4ExpectSeqNo = 0;
    pChnlNode->i4LastAnsNum = 0;
*/
    return BPCLT_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : BpCltCheckAuthServer                               */
/*   Description     : This functions checks the validity of Auth server  */
/*                     server                                             */
/*                                                                        */
/*   Input(s)        : pSrvAddr - Beep server address                     */
/*   Output(s)       : None                                               */
/*   Return Value    : BPCLT_SUCCESS or BPCLT_FAILURE                     */
/**************************************************************************/

INT4
BpCltCheckAuthServer (tIPvXAddr * pSrvAddr)
{
    UNUSED_PARAM (pSrvAddr);
    return BPCLT_SUCCESS;
/* NEEDED FOR DIGEST MD5 */
/*    tIPvXAddr TempAddr;
    tBpCltAuthNode *pAuthNode = NULL;   

    MEMSET(&TempAddr, 0, sizeof(tIPvXAddr));
*/
    /* Check for NULL Addr */
/*    if (MEMCMP(&TempAddr, pSrvAddr, sizeof(tIPvXAddr) == 0))
    {
        return BPCLT_FAILURE;
    }
*/
    /* Check the Authkey already exist for this server */
/* 
    TMO_SLL_Scan (&(gBpCltGblInfo.AuthKeyList), pAuthNode, 
                  tBpCltAuthNode *)
    {
        if( MEMCMP (&pAuthNode->SrvAddr, pSrvAddr, sizeof(tIPvXAddr)) == 0 )
        {
            return BPCLT_FAILURE;
        }

    }
    return BPCLT_SUCCESS;
*/
}
