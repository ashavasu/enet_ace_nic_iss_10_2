/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: sshcmn.h,v 1.6 2014/03/03 12:13:17 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/

#ifndef __SSHCOMMON_H__
#define __SSHCOMMON_H__

#include "lr.h"
#include "cust.h"
#include "sshfs.h"
#include "iss.h"
#include "ip.h"
#include "osxstd.h"
#include "srmbuf.h"
#include "osxsys.h"
#include "osxprot.h"

extern tOsixTaskId         gSshMainTaskId;
#define   CLI_SSH_TASK_ID            gSshMainTaskId
#define SSH_ON                          1
#define SSH_OFF                         0


#define SSH_SNMP_TRUE                   1
#define SSH_SNMP_FALSE                  2
#endif
