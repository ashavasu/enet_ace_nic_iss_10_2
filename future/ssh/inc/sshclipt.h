/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: sshclipt.h,v 1.7 2012/12/12 15:09:15 siva Exp $ 
 *
 * Description: This has proto types of fucntions used in SSH-CLI sub module.
 *
 *******************************************************************************/

#ifndef __SSHCLIPT_H__
#define __SSHCLIPT_H__

/* Prototype declarations for SSL CLI */
INT4 SshCliShowIpSsh PROTO ((tCliHandle));
INT4 SshCliTrace PROTO ((tCliHandle, INT4, UINT1));
INT4 SshCliSetVersionCompatibility PROTO ((tCliHandle, INT4));
INT4 SshCliSetCipherList PROTO ((tCliHandle, INT4, UINT1));
INT4 SshCliSetMacList PROTO ((tCliHandle, INT4, UINT1));
INT4 SshShowRunningConfig(tCliHandle CliHandle);
VOID IssSshShowDebugging (tCliHandle);
INT4 SshCliSetStatus PROTO ((tCliHandle, INT4));
INT4 SshCliSetMaxByte (tCliHandle CliHandle,INT4 i4MaxByteAllow);
INT4 SshCliServerPub (tCliHandle CliHandle, INT1 *pi1ServerCert);
INT4 SshCliServerPubDel (tCliHandle CliHandle, INT1 *pi1ServerCert);
INT4 SshCliShowSshConfig (tCliHandle CliHandle);
INT4 SshCliSetServerConf (tCliHandle CliHandle, UINT1 *pu1CliIssSshIpAddr,UINT4 u4SshPort);
#endif /* __SSHCLIPT_H__ */
