/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssshmlw.h,v 1.5 2012/12/12 15:09:15 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetSshVersionCompatibility ARG_LIST((INT4 *));

INT1
nmhGetSshCipherList ARG_LIST((INT4 *));

INT1
nmhGetSshMacList ARG_LIST((INT4 *));

INT1
nmhGetSshTrace ARG_LIST((INT4 *));

INT1
nmhGetSshStatus ARG_LIST((INT4 *));

INT1
nmhGetSshSrvBindAddr ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetSshServerBindPortNo ARG_LIST((UINT4 *));

INT1
nmhGetSshTransportMaxAllowedBytes ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetSshVersionCompatibility ARG_LIST((INT4 ));

INT1
nmhSetSshCipherList ARG_LIST((INT4 ));

INT1
nmhSetSshMacList ARG_LIST((INT4 ));

INT1
nmhSetSshTrace ARG_LIST((INT4 ));

INT1
nmhSetSshStatus ARG_LIST((INT4 ));

INT1
nmhSetSshSrvBindAddr ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetSshServerBindPortNo ARG_LIST((UINT4 ));

INT1
nmhSetSshTransportMaxAllowedBytes ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2SshVersionCompatibility ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2SshCipherList ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2SshMacList ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2SshTrace ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2SshStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2SshSrvBindAddr ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2SshServerBindPortNo ARG_LIST((UINT4 *  ,UINT4 ));


INT1
nmhTestv2SshTransportMaxAllowedBytes ARG_LIST((UINT4 *  ,INT4 ));

INT1 SshSetCipherAlgoList(UINT2 u2CipherAlgos);

INT1 SshSetMacAlgoList(UINT2 u2MacAlgos);
/* Low Level DEP Routines for.  */

INT1
nmhDepv2SshVersionCompatibility ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2SshCipherList ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2SshMacList ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2SshTrace ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2SshStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2SshSrvBindAddr ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2SshServerBindPortNo ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


INT1
nmhDepv2SshTransportMaxAllowedBytes ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
