/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssshmdb.h,v 1.5 2012/12/12 15:09:14 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSSSHMDB_H
#define _FSSSHMDB_H


UINT4 fssshm [] ={1,3,6,1,4,1,2076,97};
tSNMP_OID_TYPE fssshmOID = {8, fssshm};

UINT4 SshVersionCompatibility [ ] ={1,3,6,1,4,1,2076,97,1,1};
UINT4 SshCipherList [ ] ={1,3,6,1,4,1,2076,97,1,2};
UINT4 SshMacList [ ] ={1,3,6,1,4,1,2076,97,1,3};
UINT4 SshTrace [ ] ={1,3,6,1,4,1,2076,97,1,4};
UINT4 SshStatus [ ] ={1,3,6,1,4,1,2076,97,1,5};
UINT4 SshTransportMaxAllowedBytes [ ] ={1,3,6,1,4,1,2076,97,1,6};
UINT4 SshSrvBindAddr [ ] ={1,3,6,1,4,1,2076,97,1,7};
UINT4 SshServerBindPortNo [ ] ={1,3,6,1,4,1,2076,97,1,8};




tMbDbEntry fssshmMibEntry[]= {

{{10,SshVersionCompatibility}, NULL, SshVersionCompatibilityGet, SshVersionCompatibilitySet, SshVersionCompatibilityTest, SshVersionCompatibilityDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,SshCipherList}, NULL, SshCipherListGet, SshCipherListSet, SshCipherListTest, SshCipherListDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,SshMacList}, NULL, SshMacListGet, SshMacListSet, SshMacListTest, SshMacListDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,SshTrace}, NULL, SshTraceGet, SshTraceSet, SshTraceTest, SshTraceDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,SshStatus}, NULL, SshStatusGet, SshStatusSet, SshStatusTest, SshStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,SshTransportMaxAllowedBytes}, NULL, SshTransportMaxAllowedBytesGet, SshTransportMaxAllowedBytesSet, SshTransportMaxAllowedBytesTest, SshTransportMaxAllowedBytesDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,SshSrvBindAddr}, NULL, SshSrvBindAddrGet, SshSrvBindAddrSet, SshSrvBindAddrTest, SshSrvBindAddrDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "127.0.0.1"},

{{10,SshServerBindPortNo}, NULL, SshServerBindPortNoGet, SshServerBindPortNoSet, SshServerBindPortNoTest, SshServerBindPortNoDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "22"},
};
tMibData fssshmEntry = { 8, fssshmMibEntry };

#endif /* _FSSSHMDB_H */

