/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: sftpapi.h,v 1.6 2014/07/15 12:49:36 siva Exp $
 *
 * Description:Contains API functions to process SFTP requests.
 *
 *******************************************************************/

#ifndef SFTPAPI_H
#define SFTPAPI_H

#include "sshcmn.h"
#include "utilipvx.h"

INT1
sftpcArRecvFile (UINT1 *pu1SrcUserName, UINT1 *pu1SrcPassWd, tIPvXAddr SrcIpAddress, UINT1 *pu1SrcFileName, UINT1 *pu1LocalDstFile);

INT1
sftpcArSendFile (UINT1 *pu1DstUserName, UINT1 *pu1DstPassWd, tIPvXAddr DstIpAddress, UINT1 *pu1DstFileName, UINT1 *pu1LocalSrcFile);

VOID 
SftpcArGetCopyStatus (UINT1 *pu1CopyStatus);
#endif
