/********************************************************************
 * * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * *
 * * $Id: fssshmwr.h,v 1.5 2012/12/12 15:09:15 siva Exp $
 * *
 * * Description: Protocol Wrapper Routines
 * *********************************************************************/


#ifndef _FSSSHMWR_H
#define _FSSSHMWR_H

VOID RegisterFSSSHM(VOID);

VOID UnRegisterFSSSHM(VOID);
INT4 SshVersionCompatibilityGet(tSnmpIndex *, tRetVal *);
INT4 SshCipherListGet(tSnmpIndex *, tRetVal *);
INT4 SshMacListGet(tSnmpIndex *, tRetVal *);
INT4 SshTraceGet(tSnmpIndex *, tRetVal *);
INT4 SshStatusGet(tSnmpIndex *, tRetVal *);
INT4 SshTransportMaxAllowedBytesGet(tSnmpIndex *, tRetVal *);
INT4 SshSrvBindAddrGet(tSnmpIndex *, tRetVal *);
INT4 SshServerBindPortNoGet(tSnmpIndex *, tRetVal *);
INT4 SshVersionCompatibilitySet(tSnmpIndex *, tRetVal *);
INT4 SshCipherListSet(tSnmpIndex *, tRetVal *);
INT4 SshMacListSet(tSnmpIndex *, tRetVal *);
INT4 SshTraceSet(tSnmpIndex *, tRetVal *);
INT4 SshStatusSet(tSnmpIndex *, tRetVal *);
INT4 SshTransportMaxAllowedBytesSet(tSnmpIndex *, tRetVal *);
INT4 SshSrvBindAddrSet(tSnmpIndex *, tRetVal *);
INT4 SshServerBindPortNoSet(tSnmpIndex *, tRetVal *);
INT4 SshVersionCompatibilityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SshCipherListTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SshMacListTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SshTraceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SshStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SshTransportMaxAllowedBytesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SshSrvBindAddrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SshServerBindPortNoTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SshVersionCompatibilityDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 SshCipherListDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 SshMacListDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 SshTraceDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 SshStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 SshTransportMaxAllowedBytesDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 SshSrvBindAddrDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 SshServerBindPortNoDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
#endif /* _FSSSHMWR_H */
