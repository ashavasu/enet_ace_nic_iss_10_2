/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: sshapi.h,v 1.11 2013/09/18 09:32:12 siva Exp $
 *
 * Description: This has interface macros and prototypes for ssh. 
 *
 ***********************************************************************/

#ifndef __SSHAPI_H__
#define __SSHAPI_H__

#include "sshcmn.h"

void                SshKeyGen           PROTO ((tOsixTaskId));
void                SSHInit             PROTO ((tOsixTaskId));
signed char         SshReadServerKey    PROTO ((void));

/* Returns the SSH Listen Socket Descriptor */
int                SSHStartListener    PROTO((void));
int                SSHV6StartListener    PROTO((void));
int                SSHCreateDummySocket PROTO((void));
int                SSHV6CreateDummySocket PROTO((void));

/* Returns the Socket Descriptor of the Accept Call */
int                SSHAccept           PROTO((int SockFd,int i4Family));

/*Returns the socket descriptor for the given connection index */
int                SSHGetSockfd  PROTO((int i4ConnIndex));
/* Starts SSH Negotiations */
void SSHSessionStart (int i4ConnIndex, tOsixTaskId TaskId,
                 unsigned char *pu1TaskName, unsigned char *u1PubUsrName);
/* For a given connection, i4ConnIndex, returns the Character Read */
unsigned char               SSHRead             PROTO((int i4ConnIndex));

/* Writes the Message to the Remote Console */
int                SSHWrite            PROTO((int i4ConnIndex,
                                               unsigned char *pMessage,unsigned int u4Len));

/* Closes a Connection referred to by i4ConnIndex */
void                SSHClose            PROTO((int i4ConnIndex));
void                SSHTearDownConnection PROTO((int i4ConnIndex));

void SSHUpdateSessionTimeOut (int i4ConnIndex, int i4SessionTimeOut);

void SSHSendTaskId (int ConnIndex,tOsixTaskId sTaskId);
int SSHGetServSockId(void);
int SSHGetServ6SockId(void);
int GetServDummySockId(void);
int SSHConnectPassiveSocket (void);
int SSHConnectPassiveV6Socket (void);
int SSHDisable(void);
int SSHEnable(void);
void SSHGetVersionCompatibility (unsigned char * pu1VersionCompatibility);
void SSHSetVersionCompatibility (unsigned char u1VersionCompatibility);
void SSHGetServerStatus (unsigned char * pu1ServerStatus);
void SSHSetServerStatus (unsigned char u1ServerStatus);
char SSHGetCipherAlgoList (unsigned short * u2CipherAlgos);
char SSHSetCipherAlgoList (unsigned short);
char SSHGetMacAlgoList (unsigned short * u2MacAlgos);
char SSHSetMacAlgoList (unsigned short);
void SSHSetSshTrace (unsigned short u2Trace);
void SSHGetSshTrace (unsigned short * u2Trace);
void  SSHUpdateMaxPktLen(int );
void SSHReciveMaxPktLen(unsigned short *pu2ReciveByte);
#endif  /*__SSHAPI_H__*/
