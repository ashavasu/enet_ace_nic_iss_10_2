/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssshmwr.c,v 1.5 2012/12/12 15:09:17 siva Exp $
*
* Description: Protocol Wrapper Routines
*********************************************************************/

# include  "lr.h"
# include  "fssnmp.h"
# include  "fssshmlw.h"
# include  "fssshmwr.h"
# include  "fssshmdb.h"

VOID
RegisterFSSSHM ()
{
    SNMPRegisterMib (&fssshmOID, &fssshmEntry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fssshmOID, (const UINT1 *) "fssshmib");
}

VOID
UnRegisterFSSSHM ()
{
    SNMPUnRegisterMib (&fssshmOID, &fssshmEntry);
    SNMPDelSysorEntry (&fssshmOID, (const UINT1 *) "fssshmib");
}

INT4
SshVersionCompatibilityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSshVersionCompatibility (&(pMultiData->i4_SLongValue)));
}

INT4
SshCipherListGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSshCipherList (&(pMultiData->i4_SLongValue)));
}

INT4
SshMacListGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSshMacList (&(pMultiData->i4_SLongValue)));
}

INT4
SshTraceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSshTrace (&(pMultiData->i4_SLongValue)));
}

INT4
SshStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSshStatus (&(pMultiData->i4_SLongValue)));
}

INT4
SshTransportMaxAllowedBytesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSshTransportMaxAllowedBytes (&(pMultiData->i4_SLongValue)));
}

INT4
SshSrvBindAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSshSrvBindAddr (pMultiData->pOctetStrValue));
}

INT4
SshServerBindPortNoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSshServerBindPortNo (&(pMultiData->u4_ULongValue)));
}

INT4
SshVersionCompatibilitySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSshVersionCompatibility (pMultiData->i4_SLongValue));
}

INT4
SshCipherListSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSshCipherList (pMultiData->i4_SLongValue));
}

INT4
SshMacListSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSshMacList (pMultiData->i4_SLongValue));
}

INT4
SshTraceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSshTrace (pMultiData->i4_SLongValue));
}

INT4
SshTransportMaxAllowedBytesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSshTransportMaxAllowedBytes (pMultiData->i4_SLongValue));
}

INT4
SshSrvBindAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSshSrvBindAddr (pMultiData->pOctetStrValue));
}

INT4
SshServerBindPortNoSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSshServerBindPortNo (pMultiData->u4_ULongValue));
}

INT4
SshVersionCompatibilityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SshVersionCompatibility
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
SshStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSshStatus (pMultiData->i4_SLongValue));
}

INT4
SshCipherListTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SshCipherList (pu4Error, pMultiData->i4_SLongValue));
}

INT4
SshMacListTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SshMacList (pu4Error, pMultiData->i4_SLongValue));
}

INT4
SshTraceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SshTrace (pu4Error, pMultiData->i4_SLongValue));
}

INT4
SshStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SshStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
SshTransportMaxAllowedBytesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SshTransportMaxAllowedBytes
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
SshSrvBindAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SshSrvBindAddr (pu4Error, pMultiData->pOctetStrValue));
}

INT4
SshServerBindPortNoTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2SshServerBindPortNo (pu4Error, pMultiData->u4_ULongValue));
}

INT4
SshVersionCompatibilityDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SshVersionCompatibility
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
SshCipherListDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SshCipherList (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
SshMacListDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SshMacList (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
SshTraceDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SshTrace (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
SshStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SshStatus (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
SshTransportMaxAllowedBytesDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SshTransportMaxAllowedBytes
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
SshSrvBindAddrDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SshSrvBindAddr (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
SshServerBindPortNoDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2SshServerBindPortNo
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
