/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssshmlw.c,v 1.18 2015/08/25 13:20:15 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fssshmlw.h"
# include   "sshcli.h"
# include   "sshapi.h"
#include    "fssocket.h"

int                 gSshPort = SSH_DEFAULT_PORT;
unsigned int        gSshIpAddr = INADDR_ANY;

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetSshVersionCompatibility
 Input       :  The Indices

                The Object 
                retValSshVersionCompatibility
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSshVersionCompatibility (INT4 *pi4RetValSshVersionCompatibility)
{
    UINT1               u1VersionCompatability;

    SshArGetVersionCompatibility (&u1VersionCompatability);
    if (u1VersionCompatability == SSH_OFF)
    {
        *pi4RetValSshVersionCompatibility = SSH_SNMP_FALSE;
    }
    else if (u1VersionCompatability == SSH_ON)
    {
        *pi4RetValSshVersionCompatibility = SSH_SNMP_TRUE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSshCipherList
 Input       :  The Indices

                The Object 
                retValSshCipherList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSshCipherList (INT4 *pi4RetValSshCipherList)
{
    UINT2               u2CipherList = 0;

    if (SshArGetCipherAlgoList (&u2CipherList) == SSH_SUCCESS)
    {
        *pi4RetValSshCipherList = (INT4) u2CipherList;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetSshMacList
 Input       :  The Indices

                The Object 
                retValSshMacList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSshMacList (INT4 *pi4RetValSshMacList)
{
    UINT2               u2MacList = 0;
    if (SshArGetMacAlgoList (&u2MacList) == SSH_SUCCESS)
    {
        *pi4RetValSshMacList = u2MacList;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetSshTrace
 Input       :  The Indices

                The Object 
                retValSshTrace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSshTrace (INT4 *pi4RetValSshTrace)
{
    UINT2               u2Temp;
    SshArGetSshTrace (&u2Temp);
    *pi4RetValSshTrace = u2Temp;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSshStatus
 Input       :  The Indices

                The Object 
                retValSshStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSshStatus (INT4 *pi4RetValSshStatus)
{
    UINT1               u1SshStatus = 0;

    SshArGetSshServerStatus (&u1SshStatus);
    *pi4RetValSshStatus = (INT4) u1SshStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSshSrvBindAddr
 Input       :  The Indices

                The Object
                retValSshSrvBindAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSshSrvBindAddr (tSNMP_OCTET_STRING_TYPE * pRetValSshSrvBindAddr)
{
    tUtlInAddr          InAddr;

    MEMSET (&InAddr, 0, sizeof (tUtlInAddr));
    InAddr.u4Addr = gSshIpAddr;
    InAddr.u4Addr = OSIX_NTOHL (InAddr.u4Addr);

    MEMCPY (pRetValSshSrvBindAddr->pu1_OctetList, &InAddr.u4Addr,
            IPVX_IPV4_ADDR_LEN);
    if (InAddr.u4Addr != 0)
    {
        pRetValSshSrvBindAddr->i4_Length = IPVX_IPV4_ADDR_LEN;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSshServerBindPortNo
 Input       :  The Indices

                The Object
                retValSshServerBindPortNo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSshServerBindPortNo (UINT4 *pu4RetValSshServerBindPortNo)
{
    *pu4RetValSshServerBindPortNo = gSshPort;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetSshTransportMaxAllowedBytes
 Input       :  The Indices

                The Object 
                retValSshTransportMaxAllowedBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSshTransportMaxAllowedBytes (INT4 *pi4RetValSshTransportMaxAllowedBytes)
{
    UINT2               u2SshGetByte = 0;

    SshArGetSshPacketByte (&u2SshGetByte);
    *pi4RetValSshTransportMaxAllowedBytes = u2SshGetByte;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetSshVersionCompatibility
 Input       :  The Indices

                The Object 
                setValSshVersionCompatibility
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSshVersionCompatibility (INT4 i4SetValSshVersionCompatibility)
{
    INT4                i4VersionCompatability = SSH_ON;
    if (i4SetValSshVersionCompatibility == SSH_SNMP_FALSE)
    {
        i4VersionCompatability = SSH_OFF;
    }
    if (i4SetValSshVersionCompatibility == SSH_SNMP_TRUE)
    {
        i4VersionCompatability = SSH_ON;
    }
    SshArSetVersionCompatibility (i4VersionCompatability);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSshCipherList
 Input       :  The Indices

                The Object 
                setValSshCipherList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSshCipherList (INT4 i4SetValSshCipherList)
{
    if (SshArSetCipherAlgoList (i4SetValSshCipherList) == SSH_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetSshMacList
 Input       :  The Indices

                The Object 
                setValSshMacList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSshMacList (INT4 i4SetValSshMacList)
{
    if (SshArSetMacAlgoList (i4SetValSshMacList) == SSH_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetSshTrace
 Input       :  The Indices

                The Object 
                setValSshTrace
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSshTrace (INT4 i4SetValSshTrace)
{
    SshArSetSshTrace ((UINT2) i4SetValSshTrace);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSshStatus
 Input       :  The Indices

                The Object 
                setValSshStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSshStatus (INT4 i4SetValSshStatus)
{
    INT4                i4RetStats = SNMP_FAILURE;
    UINT1               u1GetValSshStatus;
    UINT1               u1SshStatus = i4SetValSshStatus;

    SshArGetSshServerStatus (&u1GetValSshStatus);

    if (u1GetValSshStatus == u1SshStatus)
    {

        return (SNMP_SUCCESS);

    }

    if (u1SshStatus == SSH_ENABLE)
    {
        i4RetStats = SshArEnable ();

        if (i4RetStats != SSH_SUCCESS)
        {

            return (SNMP_FAILURE);
        }

    }
    else
    {
#ifdef CLI_WANTED
        if (CliSetSshServerStatus (SSH_DISABLE) != CLI_SUCCESS)
        {
            return SNMP_FAILURE;
        }
#else
        i4RetStats = SshArDisable ();

        if (i4RetStats != SSH_SUCCESS)
        {
            return (SNMP_FAILURE);
        }
#endif

    }

    SshArSetSshServerStatus (i4SetValSshStatus);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSshSrvBindAddr
 Input       :  The Indices

                The Object
                setValSshSrvBindAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSshSrvBindAddr (tSNMP_OCTET_STRING_TYPE * pSetValSshSrvBindAddr)
{
    PTR_FETCH4 (gSshIpAddr, pSetValSshSrvBindAddr->pu1_OctetList);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSshServerBindPortNo
 Input       :  The Indices

                The Object
                setValSshServerBindPortNo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSshServerBindPortNo (UINT4 u4SetValSshServerBindPortNo)
{
    gSshPort = u4SetValSshServerBindPortNo;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetSshTransportMaxAllowedBytes
 Input       :  The Indices

                The Object 
                setValSshTransportMaxAllowedBytes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSshTransportMaxAllowedBytes (INT4 i4SetValSshTransportMaxAllowedBytes)
{
    SshArUpdateMaxPktLen (i4SetValSshTransportMaxAllowedBytes);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2SshVersionCompatibility
 Input       :  The Indices

                The Object 
                testValSshVersionCompatibility
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SshVersionCompatibility (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValSshVersionCompatibility)
{
    if ((i4TestValSshVersionCompatibility != SSH_SNMP_TRUE) &&
        (i4TestValSshVersionCompatibility != SSH_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SSH_INVALID_VERSION);
        return SNMP_FAILURE;
    }
    if (SSH_CALLBACK_FN[SSH_CUST_MODE_CHK].pSshCustCheckCustMode != NULL)
    {
        if (SSH_CALLBACK_FN[SSH_CUST_MODE_CHK].pSshCustCheckCustMode ()
            == SSH_TRUE)
        {
            if (i4TestValSshVersionCompatibility != SSH_SNMP_FALSE)
            {
                return SNMP_FAILURE;
            }
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SshCipherList
 Input       :  The Indices

                The Object 
                testValSshCipherList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SshCipherList (UINT4 *pu4ErrorCode, INT4 i4TestValSshCipherList)
{
    if ((i4TestValSshCipherList <= 0)
        || (i4TestValSshCipherList > MAX_SSH_CIPHER_LIST))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SSH_INVALID_CIPHER_ALGO);
        return SNMP_FAILURE;
    }
    if (SSH_CALLBACK_FN[SSH_CUST_MODE_CHK].pSshCustCheckCustMode != NULL)
    {
        if (SSH_CALLBACK_FN[SSH_CUST_MODE_CHK].pSshCustCheckCustMode ()
            == SSH_TRUE)
        {
            if ((i4TestValSshCipherList != SSH_3DES_CBC) &&
                (i4TestValSshCipherList != SSH_AES_CBC_128) &&
                (i4TestValSshCipherList != SSH_AES_CBC_256))
            {
                return SNMP_FAILURE;
            }
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SshMacList
 Input       :  The Indices

                The Object 
                testValSshMacList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SshMacList (UINT4 *pu4ErrorCode, INT4 i4TestValSshMacList)
{
    if ((i4TestValSshMacList <= 0) || (i4TestValSshMacList > 3))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SSH_INVALID_MAC_ALGO);
        return SNMP_FAILURE;
    }
    if (SSH_CALLBACK_FN[SSH_CUST_MODE_CHK].pSshCustCheckCustMode != NULL)
    {
        if (SSH_CALLBACK_FN[SSH_CUST_MODE_CHK].pSshCustCheckCustMode ()
            == SSH_TRUE)
        {
            if (i4TestValSshMacList != SSH_HMAC_SHA1)
            {
                return SNMP_FAILURE;
            }
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SshTrace
 Input       :  The Indices

                The Object 
                testValSshTrace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SshTrace (UINT4 *pu4ErrorCode, INT4 i4TestValSshTrace)
{
    if ((i4TestValSshTrace < SSH_DISABLE_ALL_TRC_MASK) ||
        (i4TestValSshTrace > SSH_ENABLE_ALL_TRC_MASK))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_SSH_INVALID_TRC);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SshStatus
 Input       :  The Indices

                The Object 
                testValSshStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SshStatus (UINT4 *pu4ErrorCode, INT4 i4TestValSshStatus)
{
    if ((i4TestValSshStatus != SSH_DISABLE) &&
        (i4TestValSshStatus != SSH_ENABLE))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        CLI_SET_ERR (CLI_SSH_ERR_INVALID_SESSION_STATUS);

        return (SNMP_FAILURE);

    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SshSrvBindAddr
 Input       :  The Indices

                The Object
                testValSshSrvBindAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SshSrvBindAddr (UINT4 *pu4ErrorCode,
                         tSNMP_OCTET_STRING_TYPE * pTestValSshSrvBindAddr)
{

    UINT4               u4Ip4Addr;

    if (pTestValSshSrvBindAddr->i4_Length == IPVX_IPV4_ADDR_LEN)
    {
        MEMCPY (&u4Ip4Addr, pTestValSshSrvBindAddr->pu1_OctetList,
                IPVX_IPV4_ADDR_LEN);
        u4Ip4Addr = OSIX_NTOHL (u4Ip4Addr);
#ifdef SNMP_LNXIP_WANTED
        if (NetIpv4IfIsOurAddress (u4Ip4Addr) == NETIPV4_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
#endif
        if (NetIpv4IpIsLocalNet (u4Ip4Addr) != NETIPV4_SUCCESS)
        {
#ifdef SNMP_LNXIP_WANTED
            /* Dont return error if the IP is 127.0.0.1
             * since it is allowed as a valid SNMP master
             * IP address */
            if (u4Ip4Addr != ISS_INADDR_LOOPBACK)
#endif
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
        }
        if (IP_ADDRESS_ABOVE_CLASS_C (u4Ip4Addr))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;

    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SshServerBindPortNo
 Input       :  The Indices

                The Object
                testValSshServerBindPortNo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SshServerBindPortNo (UINT4 *pu4ErrorCode,
                              UINT4 u4TestValSshServerBindPortNo)
{
    if ((u4TestValSshServerBindPortNo > SSH_MAX_PORT_NUMBER)||
        (u4TestValSshServerBindPortNo == 0))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2SshTransportMaxAllowedBytes
 Input       :  The Indices

                The Object 
                testValSshTransportMaxAllowedBytes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2SshTransportMaxAllowedBytes (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValSshTransportMaxAllowedBytes)
{
    INT1                i1Status;
    i1Status = SNMP_FAILURE;
    if ((i4TestValSshTransportMaxAllowedBytes < TRANSPORT_MIN_ALLOWED_BYTE)
        || (i4TestValSshTransportMaxAllowedBytes > TRANSPORT_MAX_ALLOWED_BYTE))
    {
        CLI_SET_ERR (CLI_SSH_INVALID_BYTE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    else
    {
        i1Status = SNMP_SUCCESS;
    }
    return i1Status;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2SshVersionCompatibility
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SshVersionCompatibility (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2SshCipherList
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SshCipherList (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2SshMacList
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SshMacList (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2SshTrace
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SshTrace (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2SshStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SshStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2SshTransportMaxAllowedBytes
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SshTransportMaxAllowedBytes (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2SshSrvBindAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SshSrvBindAddr (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2SshServerBindPortNo
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2SshServerBindPortNo (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
