/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: sshcli.c,v 1.38 2016/04/09 09:57:10 siva Exp $ 
 *
 * Description: This has functions for SSH CLI SubModule
 *
 ************************************************************************/
#ifndef __SSHCLI_C__
#define __SSHCLI_C__

#include "sshcmn.h"
#include "cli.h"
#include "sshcli.h"
#include "sshclipt.h"
#include "fssshmlw.h"
#include "fssshmwr.h"
#include "sshfs.h"
#include "fssshmcli.h"

static INT1         gi1count = 1;
static              CHR1
    gu1TempPubStorage[MAX_SSH_CONNECTIONS][SSH_MAX_PUB_KEY_SIZE];
/**************************************************************************/
/*  Function Name   : cli_process_ssh_cmd                                 */
/*  Description     : Protocol CLI message handler function               */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    u4Command - Command identifier                      */
/*                    ... -Variable command argument list                 */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/

INT4
cli_process_ssh_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[SSH_CLI_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4ErrCode;
    UINT4               u4CmdType;
    INT4                i4CipherAlgos;
    INT4                i4MacAlgos;
    INT4                i4RespStatus = CLI_SUCCESS;
    UINT1              *pu1Inst;
    INT1                ai1Prompt[] = "Enter Public Key: ";
    static INT1         ai1ServerPubKey[SSH_MAX_PUB_KEY_SIZE + 1];

    MEMSET (ai1ServerPubKey, 0, sizeof (ai1ServerPubKey));

    va_start (ap, u4Command);

    /* second arguement is always (interface name/index) NULL for ssh module cmds */
    pu1Inst = va_arg (ap, UINT1 *);

    /* This third argument in the variable argument list should be always NULL
     * for commands that doesn't uses Interface argument and hence the following 
     * check is used to verify that this module doesn't uses the 3rd argument
     */
    if (pu1Inst != NULL)
    {
        CliPrintf (CliHandle, "Passing interface argument which is UNUSED\r\n");
        va_end (ap);
        return CLI_FAILURE;
    }

    /* Walk through the rest of the arguements and store in args array. 
     * Store SSH_CLI_MAX_ARGS arguements at the max. 
     */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == SSH_CLI_MAX_ARGS)
            break;
    }
    va_end (ap);

    switch (u4Command)
    {
        case CLI_SSH_SHOW_IPSSH:
        {
            /* No addition command arguments, so ignore the args[] array */
            i4RespStatus = SshCliShowIpSsh (CliHandle);
            break;
        }
        case CLI_ISS_SHOW_SSH_CONF:
        {
            i4RespStatus = SshCliShowSshConfig (CliHandle);
            break;
        }
        case CLI_SSH_SERVER_PUB:
        {
            if (CliReadLine (ai1Prompt, ai1ServerPubKey, SSH_MAX_PUB_KEY_SIZE)
                == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r %%  Input read error  \r\n");
                i4RespStatus = CLI_FAILURE;
            }
            else
            {
                CliPrintf (CliHandle, "\r\n");
                i4RespStatus = SshCliServerPub (CliHandle, ai1ServerPubKey);
            }
            break;
        }
        case CLI_SSH_SERVER_PUB_DELETE:
        {
            if (CliReadLine (ai1Prompt, ai1ServerPubKey, SSH_MAX_PUB_KEY_SIZE)
                == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r %%  Input read error  \r\n");
                i4RespStatus = CLI_FAILURE;
            }
            else
            {
                i4RespStatus = SshCliServerPubDel (CliHandle, ai1ServerPubKey);
            }
            break;
        }
        case CLI_SSH_DEBUG:
        {
            /* args[0] -  Debug Trace level to set */
            i4RespStatus = SshCliTrace (CliHandle, CLI_PTR_TO_I4 (args[0]),
                                        CLI_ENABLE);
            break;
        }
        case CLI_SSH_NO_DEBUG:
        {
            /* args[0] -  Debug Trace level to re-set */

            i4RespStatus = SshCliTrace (CliHandle, CLI_PTR_TO_I4 (args[0]),
                                        CLI_DISABLE);
            break;
        }
        case CLI_SSH_IPSSH:
        {
            /* args[0] -  CmdType for 'ip ssh' command.
             * args[1] -  Will contain the following values based on the given CmdTypes
             *            Value              --    CmdType
             *            Cipher Algo Type         CLI_SSH_CIPHER_ALGO_LIST
             *            Auth Mac Algo Type       CLI_SSH_MAC_ALGO_LIST
             */

            u4CmdType = CLI_PTR_TO_U4 (args[0]);

            if (u4CmdType == CLI_SSH_VERSION)
            {
                i4RespStatus =
                    SshCliSetVersionCompatibility (CliHandle, SSH_SNMP_TRUE);
            }
            else if (u4CmdType == CLI_SSH_CIPHER_ALGO_LIST)
            {
                i4RespStatus =
                    SshCliSetCipherList (CliHandle, CLI_PTR_TO_I4 (args[1]),
                                         CLI_ENABLE);
            }
            else if (u4CmdType == CLI_SSH_MAC_ALGO_LIST)
            {
                i4RespStatus =
                    SshCliSetMacList (CliHandle, CLI_PTR_TO_I4 (args[1]),
                                      CLI_ENABLE);
            }

            break;
        }
        case CLI_SSH_NO_IPSSH:
        {
            /* args[0] -  CmdType for 'no ip ssh' command.
             * args[1] -  Will contain the following values based on the given CmdTypes
             *            Value              --    CmdType
             *            Cipher Algo Type         CLI_SSH_CIPHER_ALGO_LIST
             *            Auth Mac Algo Type       CLI_SSH_MAC_ALGO_LIST
             */

            u4CmdType = CLI_PTR_TO_U4 (args[0]);

            if (u4CmdType == CLI_SSH_VERSION)
            {
                i4RespStatus =
                    SshCliSetVersionCompatibility (CliHandle, SSH_SNMP_FALSE);
            }
            else if (u4CmdType == CLI_SSH_CIPHER_ALGO_LIST)
            {
                i4CipherAlgos = CLI_PTR_TO_I4 (args[1]);

                i4RespStatus =
                    SshCliSetCipherList (CliHandle, i4CipherAlgos, CLI_DISABLE);
            }
            else if (u4CmdType == CLI_SSH_MAC_ALGO_LIST)
            {
                i4MacAlgos = CLI_PTR_TO_I4 (args[1]);

                i4RespStatus =
                    SshCliSetMacList (CliHandle, i4MacAlgos, CLI_DISABLE);
            }
            break;
        }
        case CLI_SSH_SET_STATUS:
        {
            /* argno[0] = enable */
            /* argno[0] = disable */
            if (CLI_PTR_TO_I4 (args[0]) == SSH_ENABLE)
            {
                i4RespStatus = SshCliSetStatus (CliHandle, SSH_ENABLE);
            }
            else
            {
                i4RespStatus = SshCliSetStatus (CliHandle, SSH_DISABLE);
            }

            break;

        }
        case CLI_SSH_SERVER_CONF:
        {
            i4RespStatus =
                SshCliSetServerConf (CliHandle, args[0],
                                     CLI_PTR_TO_U4 (args[1]));
            break;
        }
        case CLI_SSH_MAX_ALLOWED_BYTE:
        {
            u4CmdType = CLI_PTR_TO_U4 (args[0]);
            i4RespStatus =
                SshCliSetMaxByte (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;
        }
        default:
            CliPrintf (CliHandle, "%% Invalid Command\r\n");
            i4RespStatus = CLI_FAILURE;
            break;
    }
    if ((i4RespStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_SSH_MAX_ERR))
        {
            CliPrintf (CliHandle, "%s", SshCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RespStatus);

    return i4RespStatus;
}

/**************************************************************************/
/*  Function Name   : SshCliShowSshConfig                                 */
/*  Description     : Display SSH server listening IP and port information*/
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                                                                        */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/
INT4
SshCliShowSshConfig (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE IssSshSrvBindAddr;
    UINT1               au1IpAddr[4];
    tUtlInAddr          InAddr;
    UINT4               u4SshServerBindPriPortNo;

    IssSshSrvBindAddr.pu1_OctetList = au1IpAddr;
    nmhGetSshSrvBindAddr (&IssSshSrvBindAddr);
    nmhGetSshServerBindPortNo (&u4SshServerBindPriPortNo);

    MEMCPY (&InAddr, IssSshSrvBindAddr.pu1_OctetList, IPVX_IPV4_ADDR_LEN);

    IssSshSrvBindAddr.pu1_OctetList = (UINT1 *) CLI_INET_NTOA (InAddr);

    CliPrintf (CliHandle, "\r\n SSH Listening IP %s \r\n Port %d \r\n",
               IssSshSrvBindAddr.pu1_OctetList, u4SshServerBindPriPortNo);

    return (CLI_SUCCESS);
}

/**************************************************************************/
/*  Function Name   : SshCliShowIpSsh                                     */
/*  Description     : Display SSH server information                      */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                                                                        */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/

INT4
SshCliShowIpSsh (tCliHandle CliHandle)
{
    INT4                i4VersionCompatibility;
    INT4                i4CipherAlgos;
    INT4                i4MacAlgos;
    INT4                i4SshTransportMaxAllowedBytes = 0;
    INT4                i4SshStatus = 0;
    UINT4               u4TraceLevel;

    nmhGetSshStatus (&i4SshStatus);
    if (i4SshStatus == SSH_ENABLE)
    {
        CliPrintf (CliHandle, "\r\nStatus         : SSH is Enabled");
    }
    else
    {
        CliPrintf (CliHandle, "\r\nStatus        : SSH is Disabled");
    }

    nmhGetSshVersionCompatibility (&i4VersionCompatibility);

    if (i4VersionCompatibility == SSH_SNMP_FALSE)
    {
        CliPrintf (CliHandle, "\r\nVersion          : 2\r\n");
    }
    else if (i4VersionCompatibility == SSH_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "\r\nVersion          : Both\r\n");
    }

    nmhGetSshCipherList (&i4CipherAlgos);
    CliPrintf (CliHandle, "Cipher Algorithm : ");
    if (i4CipherAlgos & SSH_3DES_CBC)
    {
        CliPrintf (CliHandle, "3DES-CBC");
    }
    if (i4CipherAlgos & SSH_DES_CBC)
    {
        if (i4CipherAlgos & SSH_3DES_CBC)
            CliPrintf (CliHandle, ", ");

        CliPrintf (CliHandle, "DES-CBC");
    }

    if (i4CipherAlgos & SSH_AES_CBC_128)
    {
        if ((i4CipherAlgos & SSH_3DES_CBC) || (i4CipherAlgos & SSH_DES_CBC))
            CliPrintf (CliHandle, ", ");

        CliPrintf (CliHandle, "AES128-CBC");
    }

    if (i4CipherAlgos & SSH_AES_CBC_256)
    {
        if ((i4CipherAlgos & SSH_3DES_CBC) || (i4CipherAlgos & SSH_DES_CBC) ||
            (i4CipherAlgos & SSH_AES_CBC_128))
            CliPrintf (CliHandle, ", ");

        CliPrintf (CliHandle, "AES256-CBC");
    }

    nmhGetSshMacList (&i4MacAlgos);

    CliPrintf (CliHandle, "\r\nAuthentication   : ");
    if (i4MacAlgos & SSH_HMAC_SHA1)
    {
        CliPrintf (CliHandle, "HMAC-SHA1");
    }
    if ((i4MacAlgos & SSH_HMAC_MD5) && (i4MacAlgos & SSH_HMAC_SHA1))
    {
        CliPrintf (CliHandle, ", ");
    }
    if (i4MacAlgos & SSH_HMAC_MD5)
    {
        CliPrintf (CliHandle, "HMAC-MD5");
    }

    nmhGetSshTrace ((INT4 *) &u4TraceLevel);
    CliPrintf (CliHandle, "\r\nTrace Level      : ");

    if ((u4TraceLevel & SSH_ENABLE_ALL_TRC_MASK) == SSH_ENABLE_ALL_TRC_MASK)
    {
        CliPrintf (CliHandle, "All\r\n");
    }
    else if (u4TraceLevel == SSH_DISABLE_ALL_TRC_MASK)
    {
        CliPrintf (CliHandle, "None\r\n");
    }
    else
    {
        if ((u4TraceLevel & SSH_INIT_SHUT_TRC_MASK) == SSH_INIT_SHUT_TRC_MASK)
        {
            CliPrintf (CliHandle, "shut ");
        }
        if ((u4TraceLevel & SSH_MGMT_TRC_MASK) == SSH_MGMT_TRC_MASK)
        {
            CliPrintf (CliHandle, "mgmt ");
        }
        if ((u4TraceLevel & SSH_DATA_PATH_TRC_MASK) == SSH_DATA_PATH_TRC_MASK)
        {
            CliPrintf (CliHandle, "data ");
        }
        if ((u4TraceLevel & SSH_CNTRL_PLANE_TRC_MASK) ==
            SSH_CNTRL_PLANE_TRC_MASK)
        {
            CliPrintf (CliHandle, "ctrl ");
        }
        if ((u4TraceLevel & SSH_DUMP_TRC_MASK) == SSH_DUMP_TRC_MASK)
        {
            CliPrintf (CliHandle, "dump ");
        }
        if ((u4TraceLevel & SSH_OS_RESOURCE_TRC_MASK) ==
            SSH_OS_RESOURCE_TRC_MASK)
        {
            CliPrintf (CliHandle, "resource ");
        }
        if ((u4TraceLevel & SSH_ALL_FAILURE_TRC_MASK) == SSH_ALL_FAILURE_TRC_MASK)
        {
            CliPrintf (CliHandle, "failure ");
        }
        if ((u4TraceLevel & SSH_BUFFER_TRC_MASK) == SSH_BUFFER_TRC_MASK)
        {
            CliPrintf (CliHandle, "buffer ");
        }
        if ((u4TraceLevel & SSH_SERVER_HANDLE_MASK) == SSH_SERVER_HANDLE_MASK)
        {
            CliPrintf (CliHandle, "server ");
        }
        CliPrintf (CliHandle, "\r\n");
    }
    nmhGetSshTransportMaxAllowedBytes (&i4SshTransportMaxAllowedBytes);
    CliPrintf (CliHandle, "\r\nMax Byte Allowed :%d\r\n",
               i4SshTransportMaxAllowedBytes);
    CliPrintf (CliHandle, "\r\n");
    return (CLI_SUCCESS);
}

/**************************************************************************/
/*  Function Name   : SshCliServerPub                                     */
/*  Description     : COnfigure client publick key in FLASH     .         */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    pi1ServerCert contain the puclic key information    */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/
INT4
SshCliServerPub (tCliHandle CliHandle, INT1 *pi1ServerCert)
{
    FILE               *pFp = NULL;
    CHR1                cwd[CURRENT_DIRECTORY_SIZE];
    if (gi1count > MAX_SSH_CONNECTIONS)
    {
        CliPrintf (CliHandle,
                   "\r\n%% Unable to save certificate Reached Maxmium\r\n");
        return CLI_FAILURE;
    }
    SNPRINTF (cwd, sizeof (cwd), "%s", SSH_PUBL_SAVE_FILE);
    if (NULL !=
        SSH_CALLBACK_FN[SSH_CUST_SERVER_PUBLIC_KEY].pSshCustSavePublicKey)
    {
        (SSH_CALLBACK_FN[SSH_CUST_SERVER_PUBLIC_KEY].pSshCustSavePublicKey)
            (ISS_SECURE_MEM_RESTORE, ISS_FILE, SSH_PUBL_SAVE_FILE);
    }
    pFp = FOPEN (cwd, "a");

    if (pFp == NULL)
    {
        CliPrintf (CliHandle, "\r\n%% Unable to open pFp\r\n");
        return (CLI_FAILURE);
    }
    /* Writing to the file */
    if (FWRITE (pi1ServerCert, STRLEN (pi1ServerCert), 1, pFp) != 1)
    {
        CliPrintf (CliHandle, "\r\n%% Unable to save certificate\r\n");
        FCLOSE (pFp);
        return (CLI_FAILURE);
    }
    fprintf (pFp, "\n");
    gi1count++;
    FCLOSE (pFp);

    if (NULL !=
        SSH_CALLBACK_FN[SSH_CUST_SERVER_PUBLIC_KEY].pSshCustSavePublicKey)
    {
        (SSH_CALLBACK_FN[SSH_CUST_SERVER_PUBLIC_KEY].pSshCustSavePublicKey)
            (ISS_SECURE_MEM_SAVE, ISS_FILE,
             SSH_PUBL_SAVE_FILE, pi1ServerCert, STRLEN (pi1ServerCert));
    }

    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : SshCliServerPubDel                                  */
/*  Description     : Delete client publick key in FLASH                  */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    pi1ServerCert contain the puclic key information    */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/
INT4
SshCliServerPubDel (tCliHandle CliHandle, INT1 *pi1ServerCert)
{
    FILE               *pFt = NULL;
    CHR1                cwd[CURRENT_DIRECTORY_SIZE];
    CHR1                au1TempStr[SSH_MAX_PUB_KEY_SIZE];
    INT1                i1Count = 0;
    INT1                i1DummyCount = 0;

    MEMSET (au1TempStr, 0, sizeof (au1TempStr));
    MEMSET (gu1TempPubStorage, 0, sizeof (gu1TempPubStorage));

    SNPRINTF (cwd, sizeof (cwd), "%s", SSH_PUBL_SAVE_FILE);
    if (NULL !=
        SSH_CALLBACK_FN[SSH_CUST_SERVER_PUBLIC_KEY].pSshCustSavePublicKey)
    {
        (SSH_CALLBACK_FN[SSH_CUST_SERVER_PUBLIC_KEY].pSshCustSavePublicKey)
            (ISS_SECURE_MEM_RESTORE, ISS_FILE, SSH_PUBL_SAVE_FILE);
    }

    pFt = FOPEN (cwd, "r");
    if (pFt == NULL)
    {
        CliPrintf (CliHandle, "\r\n%% Unable to open pFp\r\n");
        return (CLI_FAILURE);
    }

/* Through "fgets" reading authorised line by line storing in "au1TempStr" */
    while (fgets (au1TempStr, sizeof (au1TempStr), pFt) != NULL)
    {
        i1DummyCount++;
/* Camparing the aurhorised key with given key to be deleted */
	if (STRNCMP (au1TempStr, pi1ServerCert, STRLEN (au1TempStr)-1) != 0)
        {
/* saving the authorised key in local buffer "gu1TempPubStorage" 
 * which is not matching with given key */
            STRNCPY (gu1TempPubStorage[i1Count], au1TempStr,
                     sizeof (gu1TempPubStorage[i1Count]));
            i1Count++;
        }
        else
        {
/* Updating the Globale varible when Authorised key is match
 * with the given key to be deleted. it should equal to 5*/
            gi1count--;
        }
    }
    if (i1DummyCount == i1Count)
    {
    CliPrintf (CliHandle, "\r\n%% Invalid Key to be deleted \r\n");
    }

    FCLOSE (pFt);
    pFt = FOPEN (cwd, "w");
    if (pFt == NULL)
    {
        CliPrintf (CliHandle, "\r\n%% Unable to open pFp\r\n");
        return (CLI_FAILURE);
    }
    for (i1Count = 0; i1Count < MAX_SSH_CONNECTIONS; i1Count++)
    {
        /* if the local buffer does not containg any authorised
         * key it should break from the loop */
        if (STRLEN (gu1TempPubStorage[i1Count]) == 0)
            break;
        /* Writing to file which key is not matched */
        if (FWRITE
            (gu1TempPubStorage[i1Count], STRLEN (gu1TempPubStorage[i1Count]), 1,
             pFt) != 1)
        {
            CliPrintf (CliHandle, "\r\n%% Unable to save certificate\r\n");
            FCLOSE (pFt);
            return CLI_FAILURE;
        }
    }
    if (NULL !=
        SSH_CALLBACK_FN[SSH_CUST_SERVER_PUBLIC_KEY].pSshCustSavePublicKey)
    {
        (SSH_CALLBACK_FN[SSH_CUST_SERVER_PUBLIC_KEY].pSshCustSavePublicKey)
            (ISS_SECURE_MEM_DELETE, ISS_FILE, SSH_PUBL_SAVE_FILE);
    }

    FCLOSE (pFt);
    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : SshShowRunningConfig                                */
/*  Description     : Shows current running configurationsof SSH.         */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                                                                        */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/
INT4
SshShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4VersionCompatibility = 0;
    INT4                i4CipherAlgos = 0;
    INT4                i4MacAlgos = 0;
    INT4                i4RetValSshStatus;
    INT4                i4SshTransportMaxAllowedBytes;
    INT4                i4Flag = 0;
    INT4                i4DefFlag = 0;
    INT4                i4DbgLevel = 0;
    tSNMP_OCTET_STRING_TYPE IssSshSrvBindAddr;
    UINT1               au1IpAddr[4];
    tUtlInAddr          InAddr;
    UINT4               u4SshServerBindPriPortNo;

    IssSshSrvBindAddr.pu1_OctetList = au1IpAddr;
    IssSshSrvBindAddr.i4_Length = 0;
    if (nmhGetSshStatus (&i4RetValSshStatus) == SNMP_SUCCESS)
    {
        /*ssh enable is default value. It should not be shown */
        if (i4RetValSshStatus != SSH_ENABLE)
        {
            CliPrintf (CliHandle, "\r\nssh disable ");
        }
    }
    nmhGetSshTrace (&i4DbgLevel);

    nmhGetSshSrvBindAddr (&IssSshSrvBindAddr);
    nmhGetSshServerBindPortNo (&u4SshServerBindPriPortNo);

    MEMCPY (&InAddr, IssSshSrvBindAddr.pu1_OctetList, IPVX_IPV4_ADDR_LEN);

    IssSshSrvBindAddr.pu1_OctetList = (UINT1 *) CLI_INET_NTOA (InAddr);
    if (IssSshSrvBindAddr.i4_Length != 0)
    {
        if (u4SshServerBindPriPortNo != SSH_DEF_PORT)
        {
            CliPrintf (CliHandle, "\r\n ssh server-address %s port %d \r\n",
                       IssSshSrvBindAddr.pu1_OctetList, u4SshServerBindPriPortNo);
        }
        else
        {
            CliPrintf (CliHandle, "\r\nssh server-address %s \r\n",
                       IssSshSrvBindAddr.pu1_OctetList);
        } 
    }

    nmhGetSshVersionCompatibility (&i4VersionCompatibility);

    if (i4VersionCompatibility != SSH_SNMP_FALSE)
    {
        CliPrintf (CliHandle, "\r\nip ssh version compatibility");
    }

    nmhGetSshCipherList (&i4CipherAlgos);

    if (i4CipherAlgos & SSH_DES_CBC)
    {
        CliPrintf (CliHandle, "\r\nip ssh cipher ");
        i4Flag = 1;
        CliPrintf (CliHandle, "des-cbc ");
        if (i4CipherAlgos & SSH_3DES_CBC)
        {
            CliPrintf (CliHandle, "3des-cbc ");
            i4DefFlag = 1;
        }
    }
    if (i4CipherAlgos & SSH_AES_CBC_128)
    {
        if (i4Flag != 1)
        {
            CliPrintf (CliHandle, "\r\nip ssh cipher ");
            i4Flag = 1;
        }
        if ((i4CipherAlgos & SSH_3DES_CBC) || (i4CipherAlgos & SSH_DES_CBC))
        {
            if ((i4DefFlag != 1) && (i4CipherAlgos & SSH_3DES_CBC))
            {
                CliPrintf (CliHandle, "3des-cbc ");
                i4DefFlag = 1;
            }
        }
        CliPrintf (CliHandle, "aes128-cbc ");
    }
    if (i4CipherAlgos & SSH_AES_CBC_256)
    {
        if (i4Flag != 1)
        {
            CliPrintf (CliHandle, "\r\nip ssh cipher ");
            i4Flag = 1;
        }
        if ((i4CipherAlgos & SSH_3DES_CBC) || (i4CipherAlgos & SSH_DES_CBC) ||
            (i4CipherAlgos & SSH_AES_CBC_128))
        {
            if ((i4DefFlag != 1) && (i4CipherAlgos & SSH_3DES_CBC))
            {
                CliPrintf (CliHandle, "3des-cbc ");
                i4DefFlag = 1;
            }
        }
        CliPrintf (CliHandle, "aes256-cbc ");
    }

    nmhGetSshMacList (&i4MacAlgos);
    
    if ((i4MacAlgos & SSH_HMAC_MD5) && (i4MacAlgos & SSH_HMAC_SHA1))
    {
        CliPrintf (CliHandle, "\r\nip ssh auth hmac-md5 hmac-sha1\r\n");
    }
    else if (i4MacAlgos & SSH_HMAC_MD5)
    {
        CliPrintf (CliHandle, "\r\nip ssh auth hmac-md5\r\n");
    }

    nmhGetSshTransportMaxAllowedBytes (&i4SshTransportMaxAllowedBytes);
    /* Check for the default value 32768 assigned to gSshMaxParam in fsssh.c */
    if (i4SshTransportMaxAllowedBytes != TRANSPORT_MAX_ALLOWED_BYTE)
    {   
        CliPrintf (CliHandle, "\r\nip ssh transport-max-allowed bytes %d\r\n",
                   i4SshTransportMaxAllowedBytes);
    }
    
    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : SshCliSetCipherList                                 */
/*  Description     : This function configures the Cipher List            */
/*                                                                        */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    i4CipherAlgos - Cipher Algo list to set             */
/*                    u1Flag        - CLI_ENABLE/CLI_DISABLE              */
/*                                                                        */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/

INT4
SshCliSetCipherList (tCliHandle CliHandle, INT4 i4CipherAlgos, UINT1 u1Flag)
{
    UINT4               u4ErrCode;
    INT4                i4CipherList;

    UNUSED_PARAM (CliHandle);
    if (u1Flag == CLI_DISABLE)
    {
        nmhGetSshCipherList (&i4CipherList);
        i4CipherAlgos = i4CipherList & (~i4CipherAlgos);

        /* Reset the bit positions, for which the value represents */
        if (((i4CipherAlgos & SSH_3DES_CBC) != SSH_3DES_CBC) &&
            ((i4CipherAlgos & SSH_DES_CBC) != SSH_DES_CBC))
        {
            /* if 0, Set the algo to default algo SSH_3DES_CBC */
            i4CipherAlgos = SSH_3DES_CBC;
        }
    }

    if (nmhTestv2SshCipherList (&u4ErrCode, i4CipherAlgos) == SNMP_SUCCESS)
    {
        if (nmhSetSshCipherList (i4CipherAlgos) == SNMP_SUCCESS)
        {
            return (CLI_SUCCESS);
        }
    }

    CLI_SET_ERR (CLI_SSH_INVALID_CIPHER_ALGO);
    return (CLI_FAILURE);
}

/**************************************************************************/
/*  Function Name   : SshCliSetMacList                                    */
/*  Description     : This function configures the Mac algo list          */
/*                                                                        */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    i4MacAlgos - Mac algo list to set                   */
/*                    u1Flag        - CLI_ENABLE/CLI_DISABLE              */
/*                                                                        */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/

INT4
SshCliSetMacList (tCliHandle CliHandle, INT4 i4MacAlgos, UINT1 u1Flag)
{
    UINT4               u4ErrCode;
    INT4                i4MacList;

    UNUSED_PARAM (CliHandle);
    if (u1Flag == CLI_DISABLE)
    {
        nmhGetSshMacList (&i4MacList);
        i4MacAlgos = i4MacList & (~i4MacAlgos);

        /* Reset the bit positions, for which the value represents */
        if (((i4MacAlgos & SSH_HMAC_SHA1) != SSH_HMAC_SHA1) &&
            ((i4MacAlgos & SSH_HMAC_MD5) != SSH_HMAC_MD5))
        {
            /* if 0, Set the algo to default algo SSH_HMAC_SHA1 */
            i4MacAlgos = SSH_HMAC_SHA1;
        }
    }

    if (nmhTestv2SshMacList (&u4ErrCode, i4MacAlgos) == SNMP_SUCCESS)
    {
        if (nmhSetSshMacList (i4MacAlgos) == SNMP_SUCCESS)
        {
            return (CLI_SUCCESS);
        }
    }
    CLI_SET_ERR (CLI_SSH_INVALID_MAC_ALGO);
    return (CLI_FAILURE);
}

/**************************************************************************/
/*  Function Name   : SshCliTrace                                         */
/*  Description     : This function configures SSH Trace level            */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    i4TraceLevel - Trace level to set                   */
/*                    u1TraceFlag  - flag to check for set/re-set         */
/*                                                                        */
/*  Output(s)       : NONE                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/

INT4
SshCliTrace (tCliHandle CliHandle, INT4 i4TraceLevel, UINT1 u1TraceFlag)
{
    UINT4               u4ErrCode;
    INT4                i4SshTrace;

    if (u1TraceFlag == CLI_DISABLE)
    {
        nmhGetSshTrace (&i4SshTrace);
        i4TraceLevel = (i4SshTrace & (~i4TraceLevel));
    }
    else
    {
        nmhGetSshTrace (&i4SshTrace);
        i4TraceLevel |= i4SshTrace;
    }

    if (nmhTestv2SshTrace (&u4ErrCode, i4TraceLevel) == SNMP_SUCCESS)
    {
        if (nmhSetSshTrace (i4TraceLevel) == SNMP_SUCCESS)
        {
            return (CLI_SUCCESS);
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }
    return (CLI_FAILURE);
}

/**************************************************************************/
/*  Function Name  : SshCliSetVersionCompatibility                        */
/*  Description    : This function switches on or off version             */
/*                   compatibil-ity for SSH protocol version 1.0.         */
/*                                                                        */
/*  Input(s)       : CliHandle - CliContext ID                            */
/*                   i4VersionCompatibility -SSH_SNMP_TRUE/SSH_SNMP_FALSE */
/*                                                                        */
/*  Output(s)      : NONE                                                 */
/*                                                                        */
/*  Returns        : CLI_SUCCESS/CLI_FAILURE                              */
/**************************************************************************/

INT4
SshCliSetVersionCompatibility (tCliHandle CliHandle,
                               INT4 i4VersionCompatibility)
{
    UINT4               u4ErrCode;

    UNUSED_PARAM (CliHandle);
    if (nmhTestv2SshVersionCompatibility (&u4ErrCode, i4VersionCompatibility) ==
        SNMP_SUCCESS)
    {
        if (nmhSetSshVersionCompatibility (i4VersionCompatibility) ==
            SNMP_SUCCESS)
        {
            return (CLI_SUCCESS);
        }
    }
    CLI_SET_ERR (CLI_SSH_INVALID_VERSION);
    return (CLI_FAILURE);
}

/*****************************************************************************/
/* Function Name      : SshCliSetStatus                                      */
/* Description        : This function is used to set the System status of    */
/*                      the Ssh Module as Enable or Disable.                 */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : i4Status  - Enable / Disable                         */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_ssh_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
SshCliSetStatus (tCliHandle CliHandle, INT4 i4Status)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Test the Given Value if it success then Set */
    i4RetStatus = nmhTestv2SshStatus (&u4ErrorCode, i4Status);

    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetSshStatus (i4Status);

    if (i4RetStatus == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r Failed to set SSH Server Status \r\n"
                   "Warning: Disabling of Server Status is not allowed from"
                   "SSH Clients\r\n");
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);

}

/*****************************************************************************/
/* Function Name      : SshCliSetListenIpAddr                                */
/* Description        : This function is used to set the Ssh Ip address on   */
/*                      which the switch listens                             */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : pu1CliIssSshIpAddr - Ip address on which Ssh listens */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_ssh_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
SshCliSetServerConf (tCliHandle CliHandle, UINT1 *pu1CliIssSshIpAddr,
                     UINT4 u4SshPort)
{
    UINT4               u4ErrorCode = 0;
    UINT1               au1SetValIssSrvBindAddr[IPVX_IPV4_ADDR_LEN];
    UINT4               u4Ip4Addr = 0;
    tSNMP_OCTET_STRING_TYPE SshIpAddr;

    SshIpAddr.pu1_OctetList = au1SetValIssSrvBindAddr;
    MEMSET (SshIpAddr.pu1_OctetList, 0, IPVX_IPV4_ADDR_LEN);

    u4Ip4Addr = *(UINT4 *) (VOID *) pu1CliIssSshIpAddr;
    u4Ip4Addr = OSIX_NTOHL (u4Ip4Addr);
    MEMCPY (SshIpAddr.pu1_OctetList, &u4Ip4Addr, IPVX_IPV4_ADDR_LEN);
    SshIpAddr.i4_Length = IPVX_IPV4_ADDR_LEN;

    if (nmhTestv2SshSrvBindAddr (&u4ErrorCode, &SshIpAddr) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r Invalid Ip address \r\n");
        return (CLI_FAILURE);
    }
    if (nmhSetSshSrvBindAddr (&SshIpAddr) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r Failed to set SSH Server Ip address \r\n");
        return (CLI_FAILURE);
    }
    CliPrintf (CliHandle, "\r%% Configuration Will Get Effect"
               " Only On SSH Service Restart\r\n");

    if (u4SshPort != 0)
    {
        if (nmhTestv2SshServerBindPortNo (&u4ErrorCode, u4SshPort) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r Invalid Port \r\n");
            return (CLI_FAILURE);
        }

        if (nmhSetSshServerBindPortNo (u4SshPort) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r Failed to set SSH Server port\r\n");
            return (CLI_FAILURE);
        }
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : SshCliSetMaxByte                                   */
/*                                                                           */
/*     DESCRIPTION      : This function configure the Max byte               */
/*                        number of bytes allowed in an SSH transport        */
/*                        connection SSH.                             */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
SshCliSetMaxByte (tCliHandle CliHandle, INT4 i4MaxByteAllow)
{

    UINT4               u4Error = 0;
    if (nmhTestv2SshTransportMaxAllowedBytes (&u4Error, i4MaxByteAllow)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    nmhSetSshTransportMaxAllowedBytes (i4MaxByteAllow);
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssSshShowDebugging                                */
/*                                                                           */
/*     DESCRIPTION      : This function prints the SSH debug level           */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
VOID
IssSshShowDebugging (tCliHandle CliHandle)
{
    INT4                i4DbgLevel = 0;

    nmhGetSshTrace (&i4DbgLevel);
    if (i4DbgLevel == 0)
    {
        return;
    }

    CliPrintf (CliHandle, "\rSSH :");

    if ((i4DbgLevel & SSH_INIT_SHUT_TRC_MASK) != 0)
    {
        CliPrintf (CliHandle, "\r\n  SSH init and shutdown debugging is on");
    }
    if ((i4DbgLevel & SSH_MGMT_TRC_MASK) != 0)
    {
        CliPrintf (CliHandle, "\r\n  SSH management debugging is on");
    }
    if ((i4DbgLevel & SSH_DATA_PATH_TRC_MASK) != 0)
    {
        CliPrintf (CliHandle, "\r\n  SSH data path debugging is on");
    }
    if ((i4DbgLevel & SSH_CNTRL_PLANE_TRC_MASK) != 0)
    {
        CliPrintf (CliHandle, "\r\n  SSH control path debugging is on");
    }
    if ((i4DbgLevel & SSH_DUMP_TRC_MASK) != 0)
    {
        CliPrintf (CliHandle, "\r\n  SSH packet dump debugging is on");
    }
    if ((i4DbgLevel & SSH_OS_RESOURCE_TRC_MASK) != 0)
    {
        CliPrintf (CliHandle, "\r\n  SSH resources debugging is on");
    }
    if ((i4DbgLevel & SSH_ALL_FAILURE_TRC_MASK) != 0)
    {
        CliPrintf (CliHandle, "\r\n  SSH error debugging is on");
    }
    if ((i4DbgLevel & SSH_BUFFER_TRC_MASK) != 0)
    {
        CliPrintf (CliHandle, "\r\n  SSH buffer debugging is on");
    }
    if ((i4DbgLevel & SSH_SERVER_HANDLE_MASK) != 0)
    {
        CliPrintf (CliHandle, "\r\n  SSH server message debugging is on");
    }

    CliPrintf (CliHandle, "\r\n");
    return;
}

#endif /* __SSHCLI_C__ */
