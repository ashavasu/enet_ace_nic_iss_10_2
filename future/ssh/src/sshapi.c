/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: sshapi.c,v 1.17 2016/06/01 09:54:58 siva Exp $
 *
 * Description: This file has CLI support functions for SSH server.
 *
 ***********************************************************************/

#include "sshcmn.h"

#ifdef OPENSSH_WANTED
#include "sshapi.h"
#endif
unSshCallBackEntry  gaSshCallBack[SSH_MAX_CALLBACK_EVENTS];
VOID
SshArInit (tOsixTaskId TaskId)
{
#ifdef OPENSSH_WANTED
    SSHInit (TaskId);
    return;
#else
    return;
    UNUSED_PARAM (TaskId);
#endif
}

INT4
SshArReadServerKey (VOID)
{
#ifdef OPENSSH_WANTED
    return (SshReadServerKey ());
#else
    return SSH_SUCCESS;
#endif
}

INT4
SshArEnable (VOID)
{
#ifdef OPENSSH_WANTED
    return (SSHEnable ());
#else
    return SSH_SUCCESS;
#endif
}

INT4
SshArDisable (VOID)
{
#ifdef OPENSSH_WANTED
    return (SSHDisable ());
#else
    return SSH_SUCCESS;
#endif
}

INT4
SshArStartListener (VOID)
{
#ifdef OPENSSH_WANTED
    return (SSHStartListener ());
#else
    return -1;
#endif
}

VOID
SshArSendTaskId (INT4 i4ConnIndex, tOsixTaskId TaskId)
{
#ifdef OPENSSH_WANTED
    SSHSendTaskId (i4ConnIndex, TaskId);
#else
    UNUSED_PARAM (i4ConnIndex);
    UNUSED_PARAM (TaskId);
    return;
#endif
}

VOID
SshArSessionStart (INT4 i4ConnIndex, tOsixTaskId TaskId, UINT1 *pu1TaskName,
                   UINT1 *u1PubUsrName)
{
#ifdef OPENSSH_WANTED
    SSHSessionStart (i4ConnIndex, TaskId, pu1TaskName, u1PubUsrName);
#else
    UNUSED_PARAM (i4ConnIndex);
    UNUSED_PARAM (TaskId);
    UNUSED_PARAM (pu1TaskName);
    UNUSED_PARAM (u1PubUsrName);
    return;
#endif
}

INT4
SshArAccept (INT4 SockFd, INT4 i4Family)
{
#ifdef OPENSSH_WANTED
    return (SSHAccept (SockFd, i4Family));
#else
    UNUSED_PARAM (SockFd);
    UNUSED_PARAM (i4Family);
    return -1;
#endif
}

INT4
SshArGetServSockId (void)
{
#ifdef OPENSSH_WANTED
    return (SSHGetServSockId ());
#else
    return -1;
#endif
}

INT4
SshArGetServ6SockId (void)
{
#ifdef OPENSSH_WANTED
    return (SSHGetServ6SockId ());
#else
    return -1;
#endif
}

VOID
SshArUpdateSessionTimeOut (INT4 i4ConnIndex, INT4 i4SessionTimeOut)
{
#ifdef OPENSSH_WANTED
    SSHUpdateSessionTimeOut (i4ConnIndex, i4SessionTimeOut);
#else
    UNUSED_PARAM (i4ConnIndex);
    UNUSED_PARAM (i4SessionTimeOut);
    return;
#endif
}

INT4
SshArGetSockfd (INT4 i4ConnIndex)
{
#ifdef OPENSSH_WANTED
    return (SSHGetSockfd (i4ConnIndex));
#else
    UNUSED_PARAM (i4ConnIndex);
    return -1;
#endif
}

UINT1
SshArRead (INT4 i4ConnIndex)
{
#ifdef OPENSSH_WANTED
    return (SSHRead (i4ConnIndex));
#else
    UNUSED_PARAM (i4ConnIndex);
    return -1;
#endif
}

INT4
SshArWrite (INT4 i4ConnIndex, UINT1 *pMessage, UINT4 u4Len)
{
#ifdef OPENSSH_WANTED
    return (SSHWrite (i4ConnIndex, pMessage, u4Len));
#else
    UNUSED_PARAM (i4ConnIndex);
    UNUSED_PARAM (pMessage);
    UNUSED_PARAM (u4Len);
    return -1;
#endif
}

INT4
SshArConnectPassiveSocket (VOID)
{
#ifdef OPENSSH_WANTED
    return (SSHConnectPassiveSocket ());
#else
    return -1;
#endif
}

#ifdef IP6_WANTED
INT4
SshArConnectPassiveV6Socket (VOID)
{
#ifdef OPENSSH_WANTED
    return (SSHConnectPassiveV6Socket ());
#else
    return -1;
#endif
}
#endif

VOID
SshArSetVersionCompatibility (UINT1 u1VersionCompatibility)
{
#ifdef OPENSSH_WANTED
    SSHSetVersionCompatibility (u1VersionCompatibility);
#else
    UNUSED_PARAM (u1VersionCompatibility);
    return;
#endif
}

VOID
SshArGetVersionCompatibility (UINT1 *pu1VersionCompatibility)
{
#ifdef OPENSSH_WANTED
    SSHGetVersionCompatibility (pu1VersionCompatibility);
#else

    UNUSED_PARAM (pu1VersionCompatibility);
    return;
#endif
}

INT1
SshArSetCipherAlgoList (UINT2 u2CipherAlgos)
{
#ifdef OPENSSH_WANTED
    return (SSHSetCipherAlgoList (u2CipherAlgos));
#else
    UNUSED_PARAM (u2CipherAlgos);
    return SSH_SUCCESS;
#endif
}

INT1
SshArGetCipherAlgoList (UINT2 *u2CipherAlgos)
{
#ifdef OPENSSH_WANTED
    return (SSHGetCipherAlgoList (u2CipherAlgos));
#else
    UNUSED_PARAM (u2CipherAlgos);
    return SSH_SUCCESS;
#endif
}

INT1
SshArSetMacAlgoList (UINT2 u2MacAlgos)
{
#ifdef OPENSSH_WANTED
    return (SSHSetMacAlgoList (u2MacAlgos));
#else
    UNUSED_PARAM (u2MacAlgos);
    return SSH_SUCCESS;
#endif
}

INT1
SshArGetMacAlgoList (UINT2 *pu2MacAlgos)
{
#ifdef OPENSSH_WANTED
    return (SSHGetMacAlgoList (pu2MacAlgos));
#else
    UNUSED_PARAM (pu2MacAlgos);
    return SSH_SUCCESS;
#endif
}

VOID
SshArTearDownConnection (INT4 i4ConnIndex)
{
#ifdef OPENSSH_WANTED
    SSHTearDownConnection (i4ConnIndex);
#else
    UNUSED_PARAM (i4ConnIndex);
    return;
#endif
}

VOID
SshArClose (INT4 i4ConnIndex)
{
#ifdef OPENSSH_WANTED
    SSHClose (i4ConnIndex);
#else
    UNUSED_PARAM (i4ConnIndex);
    return;
#endif
}

VOID
SshArGetSshServerStatus (UINT1 *pu1ServerStatus)
{
#ifdef OPENSSH_WANTED
    SSHGetServerStatus (pu1ServerStatus);
#else
    UNUSED_PARAM (pu1ServerStatus);
    return;
#endif
}

VOID
SshArSetSshServerStatus (UINT1 u1ServerStatus)
{
#ifdef OPENSSH_WANTED
    SSHSetServerStatus (u1ServerStatus);
#else
    UNUSED_PARAM (u1ServerStatus);
    return;
#endif
}

VOID
SshArGetSshTrace (UINT2 *pu2Trace)
{
#ifdef OPENSSH_WANTED
    SSHGetSshTrace (pu2Trace);
#else
    UNUSED_PARAM (pu2Trace);
    return;
#endif
}

VOID
SshArSetSshTrace (UINT2 u2Trace)
{
#ifdef OPENSSH_WANTED
    SSHSetSshTrace (u2Trace);
#else
    UNUSED_PARAM (u2Trace);
    return;
#endif
}

VOID
SshArUpdateMaxPktLen (INT4 i4AlloweByte)
{
#ifdef OPENSSH_WANTED
    SSHUpdateMaxPktLen (i4AlloweByte);
#else
    UNUSED_PARAM (i4AlloweByte);
    return;
#endif
}

VOID
SshArGetSshPacketByte (UINT2 *pu2ReciveByte)
{
#ifdef OPENSSH_WANTED
    SSHReciveMaxPktLen (pu2ReciveByte);
#else
    UNUSED_PARAM (pu2ReciveByte);
    return;
#endif
}

VOID
SshArKeyGen (tOsixTaskId TaskId)
{
#ifdef OPENSSH_WANTED
    SshKeyGen (TaskId);
#else
    UNUSED_PARAM (TaskId);
    return;
#endif
}

/*****************************************************************************/
/* Function Name      : SshRegisterCallBackforSSHModule                      */
/*                                                                           */
/* Description        : This function is invoked to register the call backs  */
/*                      from  application.                                   */
/*                                                                           */
/* Input(s)           : u4Event - Event for which callback is registered     */
/*                      pFsCbInfo - Call Back Function                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SSH__SUCCESS/SSH__FAILURE                            */
/*****************************************************************************/

INT4
SshRegisterCallBackforSSHModule (UINT4 u4Event, tFsCbInfo * pFsCbInfo)
{
    INT4                i4RetVal = SSH_SUCCESS;
    switch (u4Event)
    {
        case SSH_CUST_MODE_CHK:
            SSH_CALLBACK_FN[u4Event].pSshCustCheckCustMode =
                pFsCbInfo->pIssCustCheckSshCustMode;
            break;
        case SSH_CUST_SERVER_KEYS_SAVE_PROCESS:
            SSH_CALLBACK_FN[u4Event].pSshCustSaveServerKeys =
                pFsCbInfo->pIssCustSecureProcess;
            break;
        case SSH_CUST_SERVER_PUBLIC_KEY:
            SSH_CALLBACK_FN[u4Event].pSshCustSavePublicKey =
                pFsCbInfo->pIssCustSecureProcess;
            break;
        case SSH_CUST_SRAM_READ_PUBLIC_KEY:
            SSH_CALLBACK_FN[u4Event].pSshCustSramReadPublicKey =
                pFsCbInfo->pIssCustSramReadPublicKey;
            break;
        default:
            i4RetVal = SSH_FAILURE;
            break;
    }
    return i4RetVal;
}
