/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: sftpapi.c,v 1.4
 *
 * Description:Contains API functions to process SFTP requests.
 *
 *******************************************************************/

#include "sftpapi.h"
#include "sftpc.h"

/**************************************************************************/
/*  Function Name   : sftpcArRecvFile                                     */
/*  Description     : This function receives file using SFTP              */
/*                                                                        */
/*                                                                        */
/*  Returns         : True/False                                          */
/**************************************************************************/
INT1
sftpcArRecvFile (UINT1 *pu1SrcUserName, UINT1 *pu1SrcPassWd,
                 tIPvXAddr SrcIpAddress, UINT1 *pu1SrcFileName,
                 UINT1 *pu1LocalDstFile)
{

#ifdef OPENSSH_WANTED
    return (sftpcRecvFile (pu1SrcUserName, pu1SrcPassWd, SrcIpAddress,
                           pu1SrcFileName, pu1LocalDstFile));
#else
    UNUSED_PARAM (pu1SrcUserName);
    UNUSED_PARAM (pu1SrcPassWd);
    UNUSED_PARAM (SrcIpAddress);
    UNUSED_PARAM (pu1SrcFileName);
    UNUSED_PARAM (pu1LocalDstFile);
    return SFTP_SUCCESS;
#endif
}

/**************************************************************************/
/*  Function Name   : sftpcArSendFile                                     */
/*  Description     : This function sends file using SFTP                 */
/*                                                                        */
/*                                                                        */
/*  Returns         : True/False                                          */
/**************************************************************************/

INT1
sftpcArSendFile (UINT1 *pu1DstUserName, UINT1 *pu1DstPassWd,
                 tIPvXAddr DstIpAddress, UINT1 *pu1DstFileName,
                 UINT1 *pu1LocalSrcFile)
{
#ifdef OPENSSH_WANTED
    return (sftpcSendFile (pu1DstUserName, pu1DstPassWd, DstIpAddress,
                           pu1DstFileName, pu1LocalSrcFile));
#else
    UNUSED_PARAM (pu1DstUserName);
    UNUSED_PARAM (pu1DstPassWd);
    UNUSED_PARAM (DstIpAddress);
    UNUSED_PARAM (pu1DstFileName);
    UNUSED_PARAM (pu1LocalSrcFile);
    return SFTP_SUCCESS;
#endif

}

/**************************************************************************/
/*  Function Name   : sftpcArSendLogBuffer                                */
/*  Description     : This function sends log buffer  using SFTP          */
/*                                                                        */
/*                                                                        */
/*  Returns         : True/False                                          */
/**************************************************************************/
INT1
sftpcArSendLogBuffer (UINT1 *pu1DstUserName, UINT1 *pu1DstPassWd,
                      tIPvXAddr DstIpAddress, UINT1 *pu1DstFileName,
                      UINT1 *pu1LogBuffer, UINT4 u4LogBufSize)
{
#ifdef OPENSSH_WANTED
    return (sftpcSendLogBuffer (pu1DstUserName, pu1DstPassWd,
                                DstIpAddress, pu1DstFileName,
                                pu1LogBuffer, u4LogBufSize));
#else
    UNUSED_PARAM (pu1DstUserName);
    UNUSED_PARAM (pu1DstPassWd);
    UNUSED_PARAM (DstIpAddress);
    UNUSED_PARAM (pu1DstFileName);
    UNUSED_PARAM (pu1LogBuffer);
    UNUSED_PARAM (u4LogBufSize);

    return SFTP_SUCCESS;
#endif

}

/**************************************************************************/
/*  Function Name   : sftpcArSendMultipleBuffers                          */
/*  Description     : This function sends config files using SFTP         */
/*                                                                        */
/*                                                                        */
/*  Returns         : True/False                                          */
/**************************************************************************/
INT1
sftpcArSendMultipleBuffers (UINT1 *pu1DstUserName, UINT1 *pu1DstPassWd,
                            tIPvXAddr DstIpAddress, UINT1 *pu1DstFileName,
                            UINT1 *pu1LogBuffer, UINT4 u4LogBufSize,
                            UINT4 u4ConnStat)
{
#ifdef OPENSSH_WANTED
    return (sftpcSendMultipleBuffers (pu1DstUserName, pu1DstPassWd,
                                      DstIpAddress, pu1DstFileName,
                                      pu1LogBuffer, u4LogBufSize, u4ConnStat));

#else
    UNUSED_PARAM (pu1DstUserName);
    UNUSED_PARAM (pu1DstPassWd);
    UNUSED_PARAM (DstIpAddress);
    UNUSED_PARAM (pu1DstFileName);
    UNUSED_PARAM (pu1LogBuffer);
    UNUSED_PARAM (u4LogBufSize);
    UNUSED_PARAM (u4ConnStat);
    return SFTP_SUCCESS;
#endif

}

/**************************************************************************/
/*  Function Name   : SftpcArErrString                                    */
/*  Description     : This function is used to get the errror message     */
/*                                                                        */
/*                                                                        */
/*  Returns         : NONE                                                */
/**************************************************************************/

VOID
SftpcArErrString (CHR1 * i1SftpcErrString)
{
#ifdef OPENSSH_WANTED
    UNUSED_PARAM (i1SftpcErrString);
#else
    UNUSED_PARAM (i1SftpcErrString);
#endif
}

VOID
SftpcArGetCopyStatus (UINT1 *pu1Copystatus)
{
#ifdef OPENSSH_WANTED
    SftpcGetCopyStatus (pu1Copystatus);
#else
    UNUSED_PARAM (pu1Copystatus);
#endif
}
