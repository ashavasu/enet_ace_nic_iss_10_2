/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: isssz.c,v 1.113 2017/05/29 13:38:22 siva Exp $
*
* Description: <File description>
*
********************************************************************/
#ifndef _ISSSZ_C
#define _ISSSZ_C
#include "lr.h"
#include "iss.h"
#include "stdarg.h"
#include "issszcli.h"
#include "sizereg.h"
/* Definitions, structures */
#define   MAX_ISS_MODULES    128
#define   ISS_SIZING_FILE_NAME_LEN 32
#define   MAX_ISS_SIZING_FILE_LINE_LEN  160

/* Typedefs */
typedef struct _IssSzParams
{
    CHR1                ModName[16];
    tFsModSizingParams *pModSizingParams;
    tMemPoolId         *pModPoolId;
} tIssSz;

/* Globals */
tIssSz              gIssSzParams[MAX_ISS_MODULES];
BOOL1               gb1SizingTrcFlag = OSIX_FALSE;
/* Prototypes */
INT4                IssSzGetValueFromSzFile (CHR1 * pu1String);
UINT2               IssSzGetModIdFromModName (CHR1 * pu1String);
INT4                IssSzUpdateSizingParamsForModule (UINT2 u2ModId,
                                                      CHR1 * MacroName,
                                                      UINT4 u4Value);
CHR1               *IssSzReadLine (FILE * fp, CHR1 * IssSzLine);
CHR1               *IssSzParseLine (CHR1 * IssSzLine, CHR1 * ModName,
                                    CHR1 * MacroName, UINT4 *pu4Value);

INT4
IssSzRegisterAllModuleSizingParams (VOID)
{
    /* Register sizing parameters for all ISS modules */
#ifdef ISS_WANTED
    SystemSzRegisterModuleSizingParams ("SYSTEM");
    MsrSzRegisterModuleSizingParams ("MSR");
#endif
#ifdef IGS_WANTED
    SnpSzRegisterModuleSizingParams ("SNOOP");
#endif
#ifdef DCBX_WANTED
    DcbxSzRegisterModuleSizingParams ("DCBX");
    EtsSzRegisterModuleSizingParams ("ETS");
    PfcSzRegisterModuleSizingParams ("PFC");
    AppSzRegisterModuleSizingParams ("APPPRI");
    CeeSzRegisterModuleSizingParams ("CEE");
#endif
#ifdef CN_WANTED
    CnSzRegisterModuleSizingParams ("CN");
#endif
#ifdef RBRG_WANTED
    RbrgSzRegisterModuleSizingParams ("RBRG");
#endif
#ifdef LLDP_WANTED
    LldpSzRegisterModuleSizingParams ("LLDP");
#endif
#ifdef ELPS_WANTED
    ElpsSzRegisterModuleSizingParams ("ELPS");
#endif
#ifdef LA_WANTED
    LaSzRegisterModuleSizingParams ("LA");
#if defined (DLAG_WANTED) || defined (ICCH_WANTED)
    LaDlagSzRegisterModuleSizingParams ("DLAG");
#endif
#endif
#ifdef CFA_WANTED
    L2dsSzRegisterModuleSizingParams ("L2DS");
    IpdbSzRegisterModuleSizingParams ("IPDB");
    CfaSzRegisterModuleSizingParams ("CFA");
#endif
#ifdef SNTP_WANTED
    SntpSzRegisterModuleSizingParams ("SNTP");
#endif
#ifdef VCM_WANTED
    VcmSzRegisterModuleSizingParams ("VCM");
#endif
#ifdef OSPF_WANTED
    OspfSzRegisterModuleSizingParams ("OSPF");
#endif
#ifdef OSPFTE_WANTED
    OspfteSzRegisterModuleSizingParams ("OSPFTE");
    OstermSzRegisterModuleSizingParams ("OSTERM");
#endif
#ifdef HOTSPOT2_WANTED
    Hotspot2SzRegisterModuleSizingParams ("HOTSPOT2");
#endif

#ifdef RIP_WANTED
    RipSzRegisterModuleSizingParams ("RIP");
#endif

#ifdef TACACS_WANTED
    TacacsSzRegisterModuleSizingParams ("TACACS");
#endif

#ifdef MRP_WANTED
    MrpSzRegisterModuleSizingParams ("MRP");
#endif

#ifdef RADIUS_WANTED
    RadSzRegisterModuleSizingParams ("RADIUS");
#endif
#ifdef VRRP_WANTED
    VrrpSzRegisterModuleSizingParams ("VRRP");
#endif
#ifdef NAT_WANTED
    NatSzRegisterModuleSizingParams ("NAT");
#endif
#ifdef IKE_WANTED
    IkeSzRegisterModuleSizingParams ("IKE");
#endif
#ifdef VPN_WANTED
    VpnSzRegisterModuleSizingParams ("VPN");
#endif
#ifdef FIREWALL_WANTED
    FirewallSzRegisterModuleSizingParams ("FIREWALL");
#endif
#ifdef EOAM_WANTED
    EoamSzRegisterModuleSizingParams ("EOAM");
#endif
#ifdef PTP_WANTED
    PtpSzRegisterModuleSizingParams ("PTP");
#endif
#ifdef DHCPC_WANTED
    DhcSzRegisterModuleSizingParams ("DHCP-CLIENT");
#endif
#ifdef DHCP_RLY_WANTED
    DhrlSzRegisterModuleSizingParams ("DHCP-RELAY");
#endif
#ifdef DHCP_SRV_WANTED
    DhcpsrvSzRegisterModuleSizingParams ("DHCP-SERVER");
#endif

#ifdef ROUTEMAP_WANTED
    RmapSzRegisterModuleSizingParams ("ROUTEMAP");
#endif

#ifdef RMON_WANTED
    RmonSzRegisterModuleSizingParams ("RMON");
#endif

#ifdef RMON2_WANTED
    Rmon2SzRegisterModuleSizingParams ("RMON2");
#endif

#ifdef BGP_WANTED
    BgpSzRegisterModuleSizingParams ("BGP");
#endif

#ifdef OSPF3_WANTED
    Ospfv3SzRegisterModuleSizingParams ("OSPFV3");
#endif

#ifdef IGMP_WANTED
    IgmpSzRegisterModuleSizingParams ("IGMP");
#endif

#ifdef IGMPPRXY_WANTED
    IgpSzRegisterModuleSizingParams ("IGP");
#endif

#ifdef RSTP_WANTED
    AstSzRegisterModuleSizingParams ("AST");
#endif
#ifdef ISIS_WANTED
    IsisSzRegisterModuleSizingParams ("ISIS");
#endif

#ifdef DVMRP_WANTED
    DvmrpSzRegisterModuleSizingParams ("DVMRP");
#endif
#ifdef RIP6_WANTED
    Rip6SzRegisterModuleSizingParams ("RIP6");
#endif

#ifdef ARP_WANTED
    ArpSzRegisterModuleSizingParams ("ARP");
#endif

#ifdef IP_WANTED
    RtmSzRegisterModuleSizingParams ("RTM");
#endif

#ifdef IP_WANTED
    IpSzRegisterModuleSizingParams ("IP");
    RarpSzRegisterModuleSizingParams ("RARP");
    UdpSzRegisterModuleSizingParams ("UDP");

#ifdef DHCP6_SRV_WANTED
    D6srSzRegisterModuleSizingParams ("DHCP6-SERVER");
#endif
#ifdef DHCP6_CLNT_WANTED
    D6clSzRegisterModuleSizingParams ("DHCP6-CLIENT");

#endif
#ifdef DHCP6_RLY_WANTED
    D6rlSzRegisterModuleSizingParams ("DHCP6-RELAY");
#endif

#endif
#ifdef IP6_WANTED
#ifndef LNXIP4_WANTED
    Ip6SzRegisterModuleSizingParams ("IP6");
    Ping6SzRegisterModuleSizingParams ("PING6");
#endif
    Rtm6SzRegisterModuleSizingParams ("RTM6");
#if defined (IPSECv6_WANTED)
    Secv6SzRegisterModuleSizingParams ("IPSEC6");
#endif /* IPSECv6_WANTED */
#endif /* IP6_WANTED */

#ifdef PIM_WANTED
    PimSzRegisterModuleSizingParams ("PIM");
#endif

#ifdef PNAC_WANTED
    PnacSzRegisterModuleSizingParams ("PNAC");
#endif

#ifdef TLM_WANTED
    TlmSzRegisterModuleSizingParams ("TLM");
#endif

#ifdef OPENFLOW_WANTED
    OfcIndexmgrSzRegisterModuleSizingParams ("OFCINDEXMGR");
    OfcSzRegisterModuleSizingParams ("OFC");
#endif

#ifdef MPLS_WANTED
    MplsdbSzRegisterModuleSizingParams ("MPLSDB");
    TeSzRegisterModuleSizingParams ("TE");
    L2vpnSzRegisterModuleSizingParams ("L2VPN");
#ifdef MPLS_SIG_WANTED
    LdpSzRegisterModuleSizingParams ("LDP");
    RsvpteSzRegisterModuleSizingParams ("RSVPTE");
#endif
    MplsrtrSzRegisterModuleSizingParams ("MPLSRTR");
    IndexmgrSzRegisterModuleSizingParams ("INDEXMGR");
    LblmgrSzRegisterModuleSizingParams ("LBLMGR");
    TcSzRegisterModuleSizingParams ("TC");
#ifdef LANAI_WANTED
    LanaiSzRegisterModuleSizingParams ("LANAI");
#endif
    MplsoamSzRegisterModuleSizingParams ("MPLSOAM");
#ifdef LSPP_WANTED
    LsppSzRegisterModuleSizingParams ("LSPP");
#endif
#ifdef RFC6374_WANTED
    R6374SzRegisterModuleSizingParams ("RFC6374");
#endif
#endif

#ifdef BFD_WANTED
    BfdSzRegisterModuleSizingParams ("BFD");
#endif

#ifdef IPVX_WANTED
    TrcrtSzRegisterModuleSizingParams ("TRCRT");
#endif

#ifdef ELMI_WANTED
    ElmiSzRegisterModuleSizingParams ("ELMI");
#endif

#ifdef ECFM_WANTED
    EcfmSzRegisterModuleSizingParams ("ECFM");
#endif

#ifdef IDS_WANTED
    SecidsSzRegisterModuleSizingParams ("SECIDS");
#endif

#ifdef PBBTE_WANTED
    PbbteSzRegisterModuleSizingParams ("PBBTE");
#endif

#ifdef EVCPRO_WANTED
    EvcproSzRegisterModuleSizingParams ("EVCPRO");
#endif

#ifdef ERPS_WANTED
    ErpsSzRegisterModuleSizingParams ("ERPS");
#endif

#ifdef ESAT_WANTED
    EsatSzRegisterModuleSizingParams ("ESAT");
#endif

#ifdef GARP_WANTED
    GarpSzRegisterModuleSizingParams ("GARP");
#endif

#ifdef PBB_WANTED
    PbbSzRegisterModuleSizingParams ("PBB");
#endif

#ifdef DNS_WANTED
    DnsSzRegisterModuleSizingParams ("DNS");
#endif

#ifdef MSDP_WANTED
    MsdpSzRegisterModuleSizingParams ("MSDP");
#endif

#ifdef VLAN_WANTED
    VlanSzRegisterModuleSizingParams ("VLAN");
#ifdef L2RED_WANTED
    VlanredSzRegisterModuleSizingParams ("VLANRED");
#endif /* L2RED_WANTED */
#endif

#ifdef SLI_WANTED
    SliSzRegisterModuleSizingParams ("SLI");
#endif

#ifdef TCP_WANTED
    TcpSzRegisterModuleSizingParams ("TCP");
#endif

#ifdef BEEP_SERVER_WANTED
    Beep_serverSzRegisterModuleSizingParams ("BEEP_SERVER");
#endif

#ifdef BEEP_CLIENT_WANTED
    Beep_clientSzRegisterModuleSizingParams ("BEEP_CLIENT");
#endif

    TriSzRegisterModuleSizingParams ("TRIE");
    TrieSzRegisterModuleSizingParams ("TRIE2");
    UtilSzRegisterModuleSizingParams ("UTIL");

#ifdef MFWD_WANTED
    MfwdSzRegisterModuleSizingParams ("MFWD");
#endif
#ifdef RM_WANTED
    RmSzRegisterModuleSizingParams ("RM");
#endif
#ifdef SYSLOG_WANTED
    SyslogSzRegisterModuleSizingParams ("SYSLOG");
#endif
#ifdef CLI_WANTED
    CliSzRegisterModuleSizingParams ("CLI");
#endif
#ifdef SNMP_3_WANTED
    Snmpv3SzRegisterModuleSizingParams ("SNMP");
#endif
#ifdef WINTEGRA
    WintegraSzRegisterModuleSizingParams ("WTGA");
#endif
#ifdef SYNCE_WANTED
    SynceSzRegisterModuleSizingParams ("SYNCE");
#endif
#ifdef MEF_WANTED
    MefSzRegisterModuleSizingParams ("MEF");
#endif
#ifdef BCMX_WANTED
    BcmxSzRegisterModuleSizingParams ("BCMX");
#endif

#ifdef WSSUSER_WANTED
    WssuserSzRegisterModuleSizingParams ("WSSUSER");
#endif

#ifdef CAPWAP_WANTED
#ifdef WEBNM_WANTED
    WebnmSzRegisterModuleSizingParams ("WEBNM");
#endif /* WEBNM_WANTED */
    CapwapSzRegisterModuleSizingParams ("CAPWAP");
#endif
#ifdef WTP_WANTED
    AphdlrSzRegisterModuleSizingParams ("APHDLR");
    WssifstawtpSzRegisterModuleSizingParams ("WSSIFSTAWTP");
    WssifwtpwlanSzRegisterModuleSizingParams ("WSSIFWTPWLAN");
#endif

#ifdef WLC_WANTED
    WlchdlrSzRegisterModuleSizingParams ("WLCHDLR");
    WssifauthSzRegisterModuleSizingParams ("WSSIFAUTH");
    WssifwebauthSzRegisterModuleSizingParams ("WSSIFWEBAUTH");
    WssifwlcwlanSzRegisterModuleSizingParams ("WSSIFWLCWLAN");
#endif
#if ((defined (WLC_WANTED)) || (defined (WTP_WANTED)))
    WsspmSzRegisterModuleSizingParams ("PM");
#endif
#ifdef RSNA_WANTED
    RsnaSzRegisterModuleSizingParams ("RSNA");
#endif
#ifdef QOSX_WANTED
    QosxSzRegisterModuleSizingParams ("QOSX");
#endif

#ifdef BCNMGR_WANTED
    BcnmgrSzRegisterModuleSizingParams ("BCNMGR");
#endif

#ifdef RFMGMT_WANTED
#ifdef WLC_WANTED
    RfmgmtacSzRegisterModuleSizingParams ("RFMGMTAC");
#endif
    RfmgmtSzRegisterModuleSizingParams ("RFMGMT");
#endif

#ifdef WSSAUTH_WANTED
    WssauthSzRegisterModuleSizingParams ("WSSAUTH");
#endif

#ifdef WSSCFG_WANTED
    WsscfgSzRegisterModuleSizingParams ("WSSCFG");
#endif

#ifdef WSSIF_WANTED
    WssifcapSzRegisterModuleSizingParams ("WSSIF");
    WssifradioSzRegisterModuleSizingParams ("WSSIFRADIO");
#endif

#ifdef WSSSTA_WANTED
    WssstawlcSzRegisterModuleSizingParams ("WSSSTA");
    WssstawebSzRegisterModuleSizingParams ("WSSSTAWEB");
#endif
#ifdef MBSM_WANTED
    MbsmSzRegisterModuleSizingParams ("MBSM");
#endif
#ifdef VXLAN_WANTED
    VxlanSzRegisterModuleSizingParams ("VXLAN");
#endif
    return OSIX_SUCCESS;
}

INT4
IssSzGetValueFromSzFile (CHR1 * pu1String)
{
    INT4                i;
    INT4                i4Value = 0;    /* long */

    for (i = 0; i < (INT4) STRLEN ((INT1 *) pu1String); i++)
    {
        if (!isdigit (pu1String[i]))
            return (-1);
    }
    i4Value = atoi (pu1String);
    return (i4Value);
}

UINT2
IssSzGetModIdFromModName (CHR1 * pu1String)
{
    UINT2               u2ModId = 0;
    UINT2               u2TempModId = 0xff;

    for (u2ModId = 0; u2ModId < MAX_ISS_MODULES; u2ModId++)
    {
        if (gIssSzParams[u2ModId].pModSizingParams == NULL)
        {
            if (u2TempModId == 0xff)
            {
                u2TempModId = u2ModId;
            }
        }
        else if (STRCMP (gIssSzParams[u2ModId].ModName, pu1String) == 0)
        {
            return u2ModId;
        }
    }
    return u2TempModId;
}

INT4
IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                 tFsModSizingParams * pModSizingParams)
{
    UINT2               u2ModId = 0;

    u2ModId = IssSzGetModIdFromModName (pu1ModName);

    if (u2ModId != 0xff)
    {
        STRNCPY (gIssSzParams[u2ModId].ModName, pu1ModName,
                 sizeof (gIssSzParams[u2ModId].ModName));
        gIssSzParams[u2ModId].
            ModName[(sizeof (gIssSzParams[u2ModId].ModName) - 1)] = '\0';
        /* Set the sizing params information pointer */
        gIssSzParams[u2ModId].pModSizingParams = pModSizingParams;
        return OSIX_SUCCESS;
    }
    else
    {
        UtlTrcPrint ("Unable to register Module Sizing parameters\n");
        return OSIX_FAILURE;
    }
}

INT4
IssSzRegisterModulePoolId (CHR1 * pu1ModName, tMemPoolId * pModPoolId)
{
    UINT2               u2ModId = 0;

    u2ModId = IssSzGetModIdFromModName (pu1ModName);

    if ((u2ModId < MAX_ISS_MODULES) && (u2ModId != 0xFF) &&
        (STRCMP (gIssSzParams[u2ModId].ModName, pu1ModName) == 0))
    {
        /* Set the sizing params information pointer */
        gIssSzParams[u2ModId].pModPoolId = pModPoolId;
        return OSIX_SUCCESS;
    }
    UtlTrcPrint ("Unable to register Module PoolId\n");
    return OSIX_FAILURE;
}

INT4
IssSzUpdateDefaultSizingInfo ()
{
    tFsModSizingParams *pModSizingParams = NULL;
    UINT4               u4ModSzField = 0;
    UINT2               u2ModId = 0;

    for (u2ModId = 0; u2ModId < MAX_ISS_MODULES; u2ModId++)
    {
        pModSizingParams = gIssSzParams[u2ModId].pModSizingParams;
        if (pModSizingParams != NULL)
        {
            for (u4ModSzField = 0;
                 pModSizingParams[u4ModSzField].u4StructSize != 0;
                 u4ModSzField++)
            {
                pModSizingParams[u4ModSzField].u4PreAllocatedUnits =
                    pModSizingParams[u4ModSzField].u4DefaultUnits;
            }
        }
    }
    return OSIX_SUCCESS;
}

INT4
IssSzUpdateSizingParamsForModule (UINT2 u2ModId, CHR1 * MacroName,
                                  UINT4 u4Value)
{
    tFsModSizingParams *pModSizingParams = NULL;
    UINT4               u4ModSzField = 0;

    if (u2ModId >= MAX_ISS_MODULES)
    {
        return (INT4) OSIX_FAILURE;
    }

    pModSizingParams = gIssSzParams[u2ModId].pModSizingParams;
    if (pModSizingParams != NULL)
    {
        for (u4ModSzField = 0; pModSizingParams[u4ModSzField].u4StructSize != 0;
             u4ModSzField++)
        {
            if (STRCMP (pModSizingParams[u4ModSzField].SizingParamId, MacroName)
                == 0)
            {
                pModSizingParams[u4ModSzField].u4PreAllocatedUnits = u4Value;
                return (INT4) OSIX_SUCCESS;
            }
        }
    }
    else
    {
        if (gb1SizingTrcFlag == OSIX_TRUE)
        {
            UtlTrcLog (1, 1, "ISSSZ",
                       " Unable to identify module name from MACRO value %s\n",
                       MacroName);

        }
    }

    return (INT4) OSIX_FAILURE;
}

FILE               *
IssSzOpenFile (CHR1 * fname)
{
    return (fopen (fname, "r"));
}

CHR1               *
IssSzReadLine (FILE * fp, CHR1 * IssSzLine)
{
    return fgets (IssSzLine, MAX_ISS_SIZING_FILE_LINE_LEN, fp);
}

CHR1               *
IssSzParseLine (CHR1 * IssSzLine, CHR1 * ModName, CHR1 * MacroName,
                UINT4 *pu4Value)
{
    int                 i = 0, i1First = OSIX_FALSE;
    CHR1                Value[100];

    /* Skip leading spaces and empty lines */
    while (*IssSzLine != '\0')
    {
        if (*IssSzLine == ' ' || *IssSzLine == '\t' || *IssSzLine == '\n')
            IssSzLine++;
        else
            break;
    }

    if (*IssSzLine == '\0')
        return NULL;
    MacroName[i] = *IssSzLine;

    /*  Extract Macro Name */
    while (*IssSzLine != '\0')
    {
        if (*IssSzLine == ' ' || *IssSzLine == '\t' || *IssSzLine == '\n')
        {
            MacroName[i] = '\0';
            i = 0;
            break;
        }
        else
        {
            MacroName[i] = *IssSzLine;
            i++;
            IssSzLine++;
        }
    }

    /* Skip spaces */
    while (*IssSzLine != '\0')
    {
        if (*IssSzLine == ' ' || *IssSzLine == '\t' || *IssSzLine == '\n')
            IssSzLine++;
        else
            break;
    }

    if (*IssSzLine == '\0')
    {
        strncpy (ModName, MacroName, STRLEN (MacroName));
        ModName[STRLEN (MacroName)] = '\0';
        memset (MacroName, 0, 200);
        return IssSzLine;
    }

    /* Extract Value */
    i = 0;
    while (*IssSzLine != '\0')
    {
        if (*IssSzLine == ' ' || *IssSzLine == '\t' || *IssSzLine == '\n')
        {
            Value[i] = '\0';
            break;
        }
        else
        {
            Value[i] = *IssSzLine;
            i++;
            IssSzLine++;
        }
    }
    *pu4Value = (UINT4) IssSzGetValueFromSzFile (Value);

    /* EXtract ModuleName from MacroName */
    i = 0;
    while (*MacroName != '\0')
    {
        if (*MacroName == '_')
        {
            if (i1First == TRUE)
            {
                ModName[i] = '\0';
                break;
            }
            MacroName++;
            i1First = OSIX_TRUE;
        }
        else
        {
            if (i1First == OSIX_FALSE)
            {
                MacroName++;
            }
            else
            {
                ModName[i] = *MacroName;
                i++;
                MacroName++;
            }
        }
    }
    return (IssSzLine);

}

void
IssSzUpdateSizingParams (FILE * fp)
{
    CHR1                IssSzLine[MAX_ISS_SIZING_FILE_LINE_LEN] = { 0 };
    CHR1                ModName[20] = { 0 };
    CHR1                MacroName[200] = { 0 };
    UINT4               u4Value = 0;
    UINT2               u2ModId = 0;

    while (!feof (fp))
    {
        if (IssSzReadLine (fp, IssSzLine) != NULL)
        {

            if (IssSzParseLine (IssSzLine, ModName, MacroName, &u4Value)
                != NULL)
            {
                if ((STRNCMP (ModName, "#", 1) != 0) &&
                    (STRNCMP (MacroName, "#", 1) != 0))
                {
                    u2ModId = IssSzGetModIdFromModName (ModName);
                    if (u2ModId != 0xff)
                    {
                        IssSzUpdateSizingParamsForModule (u2ModId, MacroName,
                                                          u4Value);
                    }
                }

            }
            memset (ModName, 0, sizeof (ModName));
            memset (MacroName, 0, sizeof (MacroName));
        }
    }
}
void
IssSzGenerateCsvFiles (tCliHandle CliHandle)
{
    tFsModSizingParams *pModSizingParams = NULL;
    CHR1                CsvFileName[100];
    UINT4               u4ModSzField = 0;
    UINT2               u2ModId = 0;
    FILE               *fp = NULL;
    CliPrintf (CliHandle, "STARTED GENERATING CSV FILES !!!!!!!\n\n");
    for (u2ModId = 0; u2ModId < MAX_ISS_MODULES; u2ModId++)
    {
        memset (CsvFileName, 0, sizeof (CsvFileName));
        pModSizingParams = gIssSzParams[u2ModId].pModSizingParams;
        SPRINTF (CsvFileName, "%s%s%s", "../../isssz/memest/",
                 gIssSzParams[u2ModId].ModName, ".csv");
        fp = fopen (CsvFileName, "w");
        if (fp == NULL)
        {
            UtlTrcLog (1, 1, "ISSSZ", "Unable to create file !!!!\n");

            return;
        }
        if (pModSizingParams != NULL)
        {
            fprintf (fp,
                     ",\"MODULE TOTAL MEMORY\",,,,,,\"=SUM(INDIRECT(ADDRESS( ROW()+1,COLUMN())) :INDIRECT(ADDRESS(ROW()+5000, COLUMN())))\"\n");
            fprintf (fp,
                     ",Sl.No,STRUCTURE NAME,STRUCTURE SIZE(BYTES),SIZING PARAMETER MACRO,NO.OF.BLOCKS,TOTAL MEMORY ALLOCATED(BYTES),,\n");
            for (u4ModSzField = 0;
                 pModSizingParams[u4ModSzField].u4StructSize != 0;
                 u4ModSzField++)
            {
                fprintf (fp,
                         ",%d,%s,%d,%s,=%s,\"=INDIRECT(ADDRESS( ROW(),COLUMN()-1))* INDIRECT(ADDRESS(ROW(), COLUMN()-3))\"\n",
                         u4ModSzField + 1,
                         pModSizingParams[u4ModSzField].StructName,
                         pModSizingParams[u4ModSzField].u4StructSize,
                         pModSizingParams[u4ModSzField].SizingParamId,
                         pModSizingParams[u4ModSzField].SizingParamId);
            }
            fprintf (fp,
                     ",TOTAL DYNAMIC MEMORY,,,,,,\"=SUM(INDIRECT(ADDRESS( ROW()-%d-1,COLUMN()-1)) :INDIRECT(ADDRESS(ROW()-1, COLUMN()-1)))\"\n",
                     u4ModSzField - 1);
            fprintf (fp, ",,,,,,,\n");
        }
        fclose (fp);
    }
#ifdef OPENPUTTY_WANTED
    SPRINTF (CsvFileName, "%s", "../../isssz/memest/OPENPUTTY.csv");
    fp = fopen (CsvFileName, "w");
    if (fp == NULL)
    {
        CliPrintf (CliHandle, "Unable to create OpenPutty csv file  !!!!\n");
        return;
    }
    fprintf (fp,
             ",\"MODULE TOTAL MEMORY\",,,,,,\"=SUM(INDIRECT(ADDRESS( ROW()+1,COLUMN())) :INDIRECT(ADDRESS(ROW()+5000, COLUMN())))\"\n");
    fprintf (fp,
             ",Sl.No,,Memory Per Session (BYTES),SIZING PARAMETER MACRO,MAX SUPPORTED SESSION,TOTAL MEMORY ALLOCATED(BYTES),,\n");
    fprintf (fp,
             ",1,,3145728,CLI_MAX_SESSIONS,8,\"=INDIRECT(ADDRESS( ROW(),COLUMN()-1))* INDIRECT(ADDRESS(ROW(), COLUMN()-3))\"\n");
    fprintf (fp,
             ",TOTAL DYNAMIC MEMORY,,,,,,\"=SUM(INDIRECT(ADDRESS( ROW()-1,COLUMN()-1)) :INDIRECT(ADDRESS(ROW()-1, COLUMN()-1)))\"\n");
    fprintf (fp, ",,,,,,,\n");
    fclose (fp);
#endif

    CliPrintf (CliHandle, "CSV FILES GENERATED SUCCESSFULLY!!!!!!!\n\n");
    return;
}

void
IssSzShowModuleMemory (tCliHandle CliHandle, CHR1 * pu1ModName)
{
    tFsModSizingParams  TempSizingParams = { "\0", "\0", 0, 0, 0, 0 };
    tFsModSizingParams *pModSizingParams = NULL;
    tMemPoolId         *pModPoolId = NULL;
    UINT4               u4ModSzField = 0;
    UINT4               u4ModTotalSize = 0;
    UINT4               u4IsModuleAvailable = 0;
    UINT4               u4FreeUnitsCount = 0;
    UINT2               u2ModId = 0;
    if (STRCMP (pu1ModName, "HR") == 0)
    {
        IssSzGenerateHRCsvFiles (CliHandle);
        return;
    }
    for (u2ModId = 0; u2ModId < MAX_ISS_MODULES; u2ModId++)
    {
        pModSizingParams = gIssSzParams[u2ModId].pModSizingParams;
        pModPoolId = gIssSzParams[u2ModId].pModPoolId;

        if ((STRCMP (gIssSzParams[u2ModId].ModName, pu1ModName)) == 0)
        {
            if ((pModSizingParams != NULL) && (pModPoolId != NULL))
            {
                u4ModTotalSize = 0;
                u4IsModuleAvailable = 1;
                CliPrintf (CliHandle, "Module Name: %s\n",
                           gIssSzParams[u2ModId].ModName);
                CliPrintf (CliHandle,
                           "================================================================================\n");
                CliPrintf (CliHandle,
                           "|No|              STRUCTURE              | SIZE  | NO.OF|  TOTAL  | FREE |POOLID\n");
                CliPrintf (CliHandle,
                           "|  |                                     |       |BLOCKS|         |BLOCKS|      \n");
                CliPrintf (CliHandle,
                           "================================================================================\n");

                for (u4ModSzField = 0;
                     (MEMCMP
                      ((pModSizingParams + u4ModSzField), &TempSizingParams,
                       sizeof (tFsModSizingParams)) != 0); u4ModSzField++)
                {
                    u4FreeUnitsCount =
                        MemGetFreeUnits (pModPoolId[u4ModSzField]);
                    CliPrintf (CliHandle,
                               "|%2d|%36s |%6d |%5d |%8d |%5d |%5d\n",
                               u4ModSzField + 1,
                               pModSizingParams[u4ModSzField].StructName,
                               pModSizingParams[u4ModSzField].u4StructSize,
                               pModSizingParams[u4ModSzField].
                               u4PreAllocatedUnits,
                               (pModSizingParams[u4ModSzField].u4StructSize *
                                pModSizingParams[u4ModSzField].
                                u4PreAllocatedUnits), u4FreeUnitsCount,
                               pModPoolId[u4ModSzField]);

                    u4ModTotalSize +=
                        (pModSizingParams[u4ModSzField].u4StructSize *
                         pModSizingParams[u4ModSzField].u4PreAllocatedUnits);
                }
                CliPrintf
                    (CliHandle,
                     "================================================================================\n");
                CliPrintf (CliHandle,
                           "| Total memory                     :    %23.4f MB|\n",
                           (DBL8) u4ModTotalSize / 1048576);
                CliPrintf (CliHandle,
                           "================================================================================\n");
            }
        }
    }
    if (u4IsModuleAvailable == 0)
    {
        CliPrintf (CliHandle,
                   "\r\n %%Incorrect module name Or Module is disabled\r\n");
        CliPrintf (CliHandle, "NOTE: Please enter module name in upper case\n");
    }
    return;
}

void
IssSzShowModuleName (tCliHandle CliHandle)
{
    UINT2               u2ModId = 0;

    CliPrintf (CliHandle, "==============================\n");
    CliPrintf (CliHandle, "|S.No| MODULE NAME |DESCRIPTION] |\n");
    CliPrintf (CliHandle, "==============================\n");

    for (u2ModId = 0; u2ModId < MAX_ISS_MODULES; u2ModId++)
    {
        if (gIssSzParams[u2ModId].pModSizingParams != NULL)
        {
            CliPrintf (CliHandle, "|%2d|%15s|Displays %2s Memory\n",
                       u2ModId + 1, gIssSzParams[u2ModId].ModName,
                       gIssSzParams[u2ModId].ModName);
        }
    }

}
void
IssSzShowIssMemoryStatus (tCliHandle CliHandle)
{
    tFsModSizingParams  TempSizingParams = { "\0", "\0", 0, 0, 0, 0 };
    tFsModSizingParams *pModSizingParams = NULL;
    UINT4               u4ModSzField = 0;
    DBL8                d8ModTotalSize = 0;
    DBL8                d8IssTotalSize = 0;
    UINT2               u2ModId = 0;

    CliPrintf (CliHandle, "==============================\n");
    CliPrintf (CliHandle, "|S.No| MODULE NAME |MEMORY[MB] |\n");
    CliPrintf (CliHandle, "==============================\n");
    for (u2ModId = 0; u2ModId < MAX_ISS_MODULES; u2ModId++)
    {
        pModSizingParams = gIssSzParams[u2ModId].pModSizingParams;

        if (pModSizingParams != NULL)
        {
            d8ModTotalSize = 0;
            for (u4ModSzField = 0;
                 (MEMCMP
                  ((pModSizingParams + u4ModSzField), &TempSizingParams,
                   sizeof (tFsModSizingParams)) != 0); u4ModSzField++)
            {
                d8ModTotalSize += (pModSizingParams[u4ModSzField].u4StructSize
                                   *
                                   pModSizingParams[u4ModSzField].
                                   u4PreAllocatedUnits);
            }
            CliPrintf (CliHandle, "|%2d|%15s|%10.4f\n", u2ModId + 1,
                       gIssSzParams[u2ModId].ModName, d8ModTotalSize / 1048576);
            d8IssTotalSize += d8ModTotalSize;
        }
    }
    CliPrintf (CliHandle, "==============================\n");
    CliPrintf (CliHandle, "TOTAL ISS MEMORY SIZE : %.4f MB\n",
               d8IssTotalSize / 1048576);
    return;
}

/*****************************************************************************/
/* Function Name      : cli_process_isssz_cmd                                */
/* Description        : This is CLI Message handler function                 */
/* Input(s)           : CliHandle -  CliContext ID                           */
/*                      u4Command -  Command Identifier                      */
/*                      ...       -  Variable Command Argument List          */
/* Output(s)          : None.                                                */
/* Return Value(s)    : CLI_SUCCESS / CLI_FAILURE                            */
/*****************************************************************************/
PUBLIC INT4
cli_process_isssz_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *apu4Args[ISSSZ_CLI_MAX_ARGS] = { 0 };
    INT1                i1argno = 0;

    UNUSED_PARAM (CliHandle);
    MEMSET (apu4Args, 0, ISSSZ_CLI_MAX_ARGS);

    /* To Parse the Variable number of arguments passed */
    va_start (ap, u4Command);
    va_arg (ap, UINT4);            /* Dummy Argument passed for backward
                                   Compatibility */
    /* Walking through the rest of the Arguments in a Infinite loop 
     * which breaks when the number of arg read is equal to the 
     * Maximum Cli Arguments that can be Passed */

    while (1)
    {
        apu4Args[i1argno++] = va_arg (ap, UINT4 *);
        if (i1argno == ISSSZ_CLI_MAX_ARGS)
        {
            break;
        }
    }

    va_end (ap);

    /* Calling proper Cli Functions using the 2 arg, u4Command */
    switch (u4Command)
    {
            /* To display memory details of all modules */
        case CLI_ISSSZ_SHOW_MOD_SIZE_ALL:
            IssSzShowIssMemoryStatus (CliHandle);
            break;

            /* To display memory details of specific module */
        case CLI_ISSSZ_SHOW_MOD_SIZE:
            IssSzShowModuleMemory (CliHandle, (CHR1 *) apu4Args[0]);
            break;

            /* To generate csv files */
        case CLI_ISSSZ_GEN_CSV_FILES:
            IssSzGenerateCsvFiles (CliHandle);
            break;

            /* To display  Module name details */
        case CLI_ISSSZ_SHOW_MOD_NAME:
            IssSzShowModuleName (CliHandle);
            break;

        default:
            UtlTrcLog (1, 1, "ISSSZ", "\r\n %% Wrong Command \r\n");

    }

    return CLI_SUCCESS;
}

 /*Hitless Restart */
/*****************************************************************************/
/* Function Name      : IssSzInitSizingInfoForHR                             */
/* Description        : Initiating all modules structure size as Zero        */
/*                      For Hitless Restart Support                          */
/* Input(s)           : None                                                 */
/* Output(s)          : None.                                                */
/* Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE                          */
/*****************************************************************************/
INT4
IssSzInitSizingInfoForHR ()
{
    tFsModSizingParams *pModSizingParams = NULL;
    UINT4               u4ModSzField = 0;
    UINT2               u2ModId = 0;

    for (u2ModId = 0; u2ModId < MAX_ISS_MODULES; u2ModId++)
    {
        pModSizingParams = gIssSzParams[u2ModId].pModSizingParams;
        if (pModSizingParams != NULL)
        {
            for (u4ModSzField = 0;
                 pModSizingParams[u4ModSzField].u4StructSize != 0;
                 u4ModSzField++)
            {
                pModSizingParams[u4ModSzField].u4StructSize = 0;
            }
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssSzUpdateSizingInfoForHR                           */
/* Description        : This Function is called by Hitless restart support   */
/*                      for the modules to find the memory has taken at the  */
/*                      time of bulk update                                  */
/* Input(s)           : pu1ModName     ----> ISS Module name                 */
/*                      pu1StName      ----> ISS Module Structure Name       */
/*                      u4BulkUnitSize ----> Memory taken to store bulk msg  */
/* Output(s)          : None.                                                */
/* Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE                          */
/*****************************************************************************/
INT4
IssSzUpdateSizingInfoForHR (CHR1 * pu1ModName, CHR1 * pu1StName,
                            UINT4 u4BulkUnitSize)
{
    tFsModSizingParams *pModSizingParams = NULL;
    UINT4               u4ModSzField = 0;
    UINT2               u2ModId = 0;

    u2ModId = IssSzGetModIdFromModName (pu1ModName);
    if (u2ModId >= MAX_ISS_MODULES)
    {
        return (INT4) OSIX_FAILURE;
    }
    pModSizingParams = gIssSzParams[u2ModId].pModSizingParams;

    if (pModSizingParams != NULL)
    {
        for (u4ModSzField = 0;
             pModSizingParams[u4ModSzField].u4StructSize != 0; u4ModSzField++)
        {
            if (STRCMP (pModSizingParams[u4ModSzField].StructName, pu1StName)
                == 0)
            {
                pModSizingParams[u4ModSzField].u4StructSize += u4BulkUnitSize;
                return (INT4) OSIX_SUCCESS;
            }
        }
    }
    else
    {
        if (gb1SizingTrcFlag == OSIX_TRUE)
        {

            UtlTrcLog (1, 1, "ISSSZ",
                       " To identify module name from MACRO value \n");
        }
    }
    return (INT4) OSIX_FAILURE;
}

/*****************************************************************************/
/* Function Name      : IssSzGenerateHRCsvFiles                              */
/* Description        : This function used to generate Csv file for          */
/*                      Hitless Restart                                      */
/* Input(s)           : None                                                 */
/* Output(s)          : Display Memory size                                  */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
void
IssSzGenerateHRCsvFiles (tCliHandle CliHandle)
{
    tFsModSizingParams *pModSizingParams = NULL;
    tFsModSizingParams  TempSizingParams = { "\0", "\0", 0, 0, 0, 0 };
    CHR1                CsvFileName[100];
    UINT4               u4ModSzField = 0;
    UINT2               u2ModId = 0;
    UINT4               u4RowCount = 0;
    FILE               *fp = NULL;

    CliPrintf (CliHandle, "STARTED GENERATING CSV FILES FOR HR !!!!!!!\n\n");

    memset (CsvFileName, 0, sizeof (CsvFileName));
    SPRINTF (CsvFileName, "%s%s%s", "../../isssz/memest/", "FlashMemory",
             ".csv");

    fp = fopen (CsvFileName, "w");
    if (fp == NULL)
    {
        UtlTrcLog (1, 1, "ISSSZ", "Unable to create file !!!!\n");
        return;
    }

    fprintf (fp, ",\"FLASH MEMORY\"\n");

    fprintf (fp, ",\"HITLESS RESTART FEATURE\"\n");
    fprintf (fp,
             ",\"MODULE TOTAL MEMORY\",,,,,,\"=SUM(INDIRECT(ADDRESS( ROW()+1,COLUMN())) :INDIRECT(ADDRESS(ROW()+5000, COLUMN())))\"\n");
    fprintf (fp,
             ",Sl.No,STRUCTURE NAME,STRUCTURE SIZE(BYTES),SIZING PARAMETER MACRO,NO.OF.BLOCKS,TOTAL MEMORY ALLOCATED(BYTES),,\n");

    for (u2ModId = 0; u2ModId < MAX_ISS_MODULES; u2ModId++)
    {
        pModSizingParams = gIssSzParams[u2ModId].pModSizingParams;

        if (pModSizingParams == NULL)
        {
            continue;
        }
        for (u4ModSzField = 0;
             (MEMCMP
              ((pModSizingParams + u4ModSzField), &TempSizingParams,
               sizeof (tFsModSizingParams)) != 0); u4ModSzField++)
        {

            if (pModSizingParams[u4ModSzField].u4StructSize != 0)
            {
                fprintf (fp,
                         ",%s,%s,%d,%s,=%s,\"=INDIRECT(ADDRESS( ROW(),COLUMN()-1))* INDIRECT(ADDRESS(ROW(), COLUMN()-3))\"\n",
                         gIssSzParams[u2ModId].ModName,
                         pModSizingParams[u4ModSzField].StructName,
                         pModSizingParams[u4ModSzField].u4StructSize,
                         pModSizingParams[u4ModSzField].SizingParamId,
                         pModSizingParams[u4ModSzField].SizingParamId);
                u4RowCount++;
            }
        }
    }
    fprintf (fp,
             ",TOTAL FLASH MEMORY,,,,,,\"=SUM(INDIRECT(ADDRESS( ROW()-%d-1,COLUMN()-1)) :INDIRECT(ADDRESS(ROW()-1, COLUMN()-1)))\"\n",
             u4RowCount - 1);
    CliPrintf (CliHandle, "u1Count :%d\n", u4RowCount);
    fprintf (fp, ",,,,,,,\n");
    fclose (fp);
    CliPrintf (CliHandle, "HR CSV FILE GENERATED SUCCESSFULLY!!!!!!!\n\n");
    return;
}

/*****************************************************************************/
/* Function Name      : IssSzGetSizingMacroValue                             */
/* Description        : This routine fetches the value for Macro passed      */
/*                      from the system.size file                            */
/* Input(s)           : pu1Macroname                                         */
/* Output(s)          : None                                                 */
/* Return Value(s)    : u4Value - Value of the Macro in system size          */
/*****************************************************************************/

UINT4
IssSzGetSizingMacroValue (CHR1 * pu1MacroName)
{
    CHR1                IssSzLine[MAX_ISS_SIZING_FILE_LINE_LEN] = { 0 };
    CHR1                ModName[20] = { 0 };
    CHR1                MacroName[200] = { 0 };
    FILE               *fp = NULL;
    UINT4               u4Value = 0;
    CHR1                gau1SizingFileName[STRLEN (ISS_SIZING_FILE_NAME) + 1];

    MEMSET (gau1SizingFileName, 0, (STRLEN (ISS_SIZING_FILE_NAME) + 1));
    STRNCPY (gau1SizingFileName, ISS_SIZING_FILE_NAME,
             STRLEN (ISS_SIZING_FILE_NAME));
    fp = IssSzOpenFile (gau1SizingFileName);

    if (fp == NULL)
    {
        return 0;
    }
    while (!feof (fp))
    {
        if (IssSzReadLine (fp, IssSzLine) != NULL)
        {
            if (IssSzParseLine (IssSzLine, ModName, MacroName, &u4Value)
                != NULL)
            {
                if (STRCMP (pu1MacroName, MacroName) == 0)
                {
                    fclose (fp);
                    return u4Value;
                }

            }
            MEMSET (ModName, 0, sizeof (ModName));
            MEMSET (MacroName, 0, sizeof (MacroName));
        }
    }
    fclose (fp);
    return 0;
}

/*****************************************************************************/
/* Function Name      : IssSzGetSizingParamsForModule                        */
/* Description        : This function is to get the maximum number of memory */
/*                      blocks allocated for the given sizing Macro          */
/* Input(s)           : pu1ModName   - Pointer to the module name            */
/*                      pu1MacroName - Pointer to the sizing Macro name      */
/* Output(s)          : u4Value - Maximum number of Memory blocks allocated  */
/*                                for the given sizing macro                 */
/* Return Value(s)    : u4Value                                              */
/*****************************************************************************/
UINT4
IssSzGetSizingParamsForModule (CHR1 * pu1ModName, CHR1 * MacroName)
{
    tFsModSizingParams *pModSizingParams = NULL;
    UINT4               u4ModSzField = 0;
    UINT4               u4Value = 0;
    UINT2               u2ModId = 0;

    u2ModId = IssSzGetModIdFromModName (pu1ModName);

    if (u2ModId == 0xff)
    {

        UtlTrcLog (1, 1, "ISSSZ",
                   " Unable to get the Module Id from Module name %s\n",
                   pu1ModName);

        return u4Value;
    }
    pModSizingParams = gIssSzParams[u2ModId].pModSizingParams;

    if (pModSizingParams != NULL)
    {
        for (u4ModSzField = 0; pModSizingParams[u4ModSzField].u4StructSize != 0;
             u4ModSzField++)
        {
            if (STRCMP (pModSizingParams[u4ModSzField].SizingParamId, MacroName)
                == 0)
            {
                u4Value = pModSizingParams[u4ModSzField].u4PreAllocatedUnits;
                return u4Value;
            }
        }
    }
    else
    {
        if (gb1SizingTrcFlag == OSIX_TRUE)
        {
            UtlTrcLog (1, 1, "ISSSZ", " Unable to get the value of  MACRO %s\n",
                       MacroName);

            return u4Value;
        }
    }
    return u4Value;
}

#endif
