/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: genFormula.c,v 1.1 2011/06/14 06:09:28 siva Exp $
*
* Description: File used to generate the required formula to
*              calculate size based on SYS_DEF_MAX_PORTS_PER_CONTEXT.
*
********************************************************************/
#include<stdio.h>
#include<stdlib.h>
int
main (int argc, char *argv[])
{
    float               max_ports_old = 0, max_ports_new = 0;
    float               old_struct = 0, new_struct = 0;
    float               struct1 = 0, Actual_size = 0;
    if (argc != 5)
        printf ("Enter all inputs\n");
    max_ports_old = atoi (argv[1]);
    max_ports_new = atoi (argv[2]);
    old_struct = atoi (argv[3]);
    new_struct = atoi (argv[4]);
    struct1 = ((new_struct - old_struct) / (max_ports_new - max_ports_old));
    Actual_size = (old_struct - (max_ports_old * struct1));
    printf ("\"=ROUND(0-%d + %.4f * SYS_DEF_MAX_PORTS_PER_CONTEXT,0)\"\n",
            (int) (old_struct - Actual_size), struct1);
    return 0;
}
