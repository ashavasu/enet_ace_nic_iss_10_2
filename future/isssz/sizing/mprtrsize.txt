tMplsRmFrame				       MAX_MPLSRTR_RM_FRAME
tMplsRtEntryInfo                  MAX_MPLSRTR_RT_CHG_ENTRIES
tMplsIfEntryInfo                   MAX_MPLSRTR_IF_CHG_ENTRIES
tMplsDiffServParamsSize		               MAX_MPLSRTR_DIFFSERV_PARAMS_TABLE
tMplsDiffServParamTokenStack		       MAX_MPLSRTR_DIFFSERV_PARAMS_TOKEN_STACK
tElspMapRowSize				       MAX_MPLSRTR_ELSPMAP_TABLE
tElspMapTokenStackSize			       MAX_MPLSRTR_ELSPMAP_TOKEN_STACK
tElspMapRow				       MAX_MPLSRTR_ELSPMAP_ROW_ENTRIES
tMplsDiffServParams			       MAX_MPLSRTR_MPLS_DIFFSERV_PARAMS
tMplsRegParams                                 MAX_MPLSRTR_MPLS_APPLICATIONS
tMplsApiInInfo                                 MAX_MPLSRTR_MPLS_API_IN_INFO
tMplsApiOutInfo                                MAX_MPLSRTR_MPLS_API_OUT_INFO
tOamFsMplsTpMeEntry                            MAX_MPLSRTR_OAM_FSMPLS_TP_ME_TABLE
tMplsEventNotif                                MAX_MPLSRTR_MPLS_EVENT_NOTIF
tBfdReqParams                                  MAX_MPLSRTR_BFD_REQ_PARAMS
UINT1[CFA_ENET_MTU]                            MAX_CFA_ENET_MTU

