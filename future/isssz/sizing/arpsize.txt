t_ARP_CACHE         MAX_ARP_ENTRIES
tArpQMsg            MAX_ARP_PKT_QUE_SIZE
tArpCxt             MAX_ARP_CONTEXTS
tArpRmMsg           MAX_ARP_RM_QUE_SIZE
tArpRedTable        MAX_ARP_DYN_MSG_SIZE
tArpEthernetEntry   MAX_ARP_ENET_ENTRIES
tArpIcchData        MAX_ARP_ICCH_QUE_SIZE
