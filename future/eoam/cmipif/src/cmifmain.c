/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cmifmain.c,v 1.6 2007/04/11 05:33:33 iss Exp $
 *
 * Description: This file contains task initialization and system routines
 *              of CMIP Interface module
 *****************************************************************************/
#ifndef CMIFMAIN_C
#define CMIFMAIN_C

#include "cmifinc.h"

/******************************************************************************
 * Function Name      : CmifMainTask
 *
 * Description        : This function is the main entry point function for
 *                      the CMIP Interface module task
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
VOID
CmifMainTask (INT1 *pArg)
{
    UINT4               u4Events = 0;

    UNUSED_PARAM (pArg);
    if (CmifMainTaskInit () == OSIX_FAILURE)
    {
        CmifMainHandleInitFailure ();
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    /* Indicate the status of initialization to the main routine */
    lrInitComplete (OSIX_SUCCESS);

    /* Initialize index pool used for SNMPGet */
    CmifRetrieveInit ();

    while (1)
    {
        if (OsixEvtRecv (gCmipIntfGlobalInfo.TaskId, CMIF_QMSG_EVENT,
                         OSIX_WAIT, &u4Events) == OSIX_SUCCESS)
        {
            if (u4Events & CMIF_QMSG_EVENT)
            {
                CmifMainProcessQMsg ();
            }
        }
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : CmifMainTaskInit 
 *                                                                          
 *    DESCRIPTION      : This function creates the task queue, allocates 
 *                       MemPools for task queue messages and creates protocol
 *                       semaphore. 
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
INT4
CmifMainTaskInit (VOID)
{
    tEoamRegParams      EoamReg;

    MEMSET (&gCmipIntfGlobalInfo, 0, sizeof (tCmipIntfGlobalInfo));
    OsixTskIdSelf (&(gCmipIntfGlobalInfo.TaskId));

    /* Create Mutual exclusion semaphore */
    if (OsixSemCrt ((UINT1 *) CMIF_SEM_NAME, &(gCmipIntfGlobalInfo.SemId))
        != OSIX_SUCCESS)
    {
        CMIF_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "CmifMainTaskInit: CMIP Interface mutual exclusion "
                  "semaphore creation failed\r\n");
        return OSIX_FAILURE;
    }

    OsixSemGive (gCmipIntfGlobalInfo.SemId);

    /* CmipIntf Task Q */
    if (OsixQueCrt ((UINT1 *) CMIF_QUEUE_NAME, OSIX_MAX_Q_MSG_LEN,
                    CMIF_QUEUE_DEPTH, &(gCmipIntfGlobalInfo.QId))
        == OSIX_FAILURE)
    {
        CMIF_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                  "CmifMainTaskInit: CMIP Interface Queue creation FAILED\n");
        return OSIX_FAILURE;
    }

    /* Q Msg Mempool */
    if (MemCreateMemPool (CMIF_QMSG_MEMBLK_SIZE,
                          CMIF_QMSG_MEMBLK_COUNT,
                          MEM_HEAP_MEMORY_TYPE,
                          &(gCmipIntfGlobalInfo.QMsgPoolId)) == MEM_FAILURE)
    {
        CMIF_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                  "CmifMainTaskInit: QMsg Mempool creation FAILED !!!\n");
        return OSIX_FAILURE;
    }

    gCmipIntfGlobalInfo.u1SystemCtrlStatus = CMIF_START;

    /* Registration with EOAM to get called when an Variable
     * request is received */
    MEMSET (&EoamReg, 0, sizeof (tEoamRegParams));
    EoamReg.u4EntId = EOAM_CMIP_APPL_ID;
    EoamReg.u4Bitmap = EOAM_NOTIFY_VAR_REQ_RCVD;
    EoamReg.pNotifyCbFunc = CmifEoamVariableRetrieve;

    if (EoamApiRegisterModules (&EoamReg) == OSIX_FAILURE)
    {
        CMIF_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                  "Registration with EOAM FAILED\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : CmifMainHandleInitFailure 
 *                                                                          
 *    DESCRIPTION      : Function called when the initialization of CMIP
 *                       Interface task fails at any point during 
 *                       initialization.  
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
VOID
CmifMainHandleInitFailure (VOID)
{
    tCmipIntfQMsg      *pQMsg = NULL;

    if (gCmipIntfGlobalInfo.QId != 0)
    {
        /* Dequeue all messages */
        while (OsixQueRecv (gCmipIntfGlobalInfo.QId, (UINT1 *) &pQMsg,
                            OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
        {
            if (pQMsg == NULL)
            {
                continue;
            }

            if (MemReleaseMemBlock (gCmipIntfGlobalInfo.QMsgPoolId,
                                    (UINT1 *) pQMsg) == MEM_FAILURE)
            {
                CMIF_TRC (INIT_SHUT_TRC, "CmifMainHandleInitFailure: QMsg Free "
                          "Mem Block FAILED !!!\n");
            }
        }

        /* Delete Q */
        OsixQueDel (gCmipIntfGlobalInfo.QId);
    }

    /* Delete QMsg Mempool */
    if (gCmipIntfGlobalInfo.QMsgPoolId != 0)
    {
        if (MemDeleteMemPool (gCmipIntfGlobalInfo.QMsgPoolId) == MEM_FAILURE)
        {
            CMIF_TRC (INIT_SHUT_TRC, "CmifMainHandleInitFailure: QMsg"
                      " MemPool Delete FAILED\n");
        }

        gCmipIntfGlobalInfo.QMsgPoolId = 0;
    }

    /* Delete Semaphore */
    if (gCmipIntfGlobalInfo.SemId != 0)
    {
        OsixSemDel (gCmipIntfGlobalInfo.SemId);
    }

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : CmifMainProcessQMsg 
 *                                                                          
 *    DESCRIPTION      : This function process the queue messages received 
 *                       by CMIP Interface task. 
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
VOID
CmifMainProcessQMsg (VOID)
{
    tCmipIntfQMsg      *pQMsg = NULL;

    /* Event received, dequeue messages for processing */
    while (OsixQueRecv (gCmipIntfGlobalInfo.QId, (UINT1 *) &pQMsg,
                        OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
    {
        CmifRetrieveVariable (pQMsg->u4Port, pQMsg->u1Branch,
                              pQMsg->u2Leaf, pQMsg->u4ReqId);
        /* Release the buffer to pool */
        MemReleaseMemBlock (gCmipIntfGlobalInfo.QMsgPoolId, (UINT1 *) pQMsg);
    }
    return;
}

#endif /* CMIFMAIN_C */
/*-----------------------------------------------------------------------*/
/*                       End of the file  cmifmain.c                     */
/*-----------------------------------------------------------------------*/
