/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cmifret.c,v 1.8 2010/11/18 04:51:41 siva Exp $
 *
 * Description: This file contains routines to retrieve CMIP MIB variables 
 *              for Ethernet OAM
 *****************************************************************************/
#ifndef CMIFRET_C
#define CMIFRET_C

#include "cmifinc.h"

PRIVATE INT4 CmifRetCopyRelevantData PROTO ((tSNMP_MULTI_DATA_TYPE *, UINT1 *));
PRIVATE INT1 CmifRetConvertSubOid PROTO ((UINT1 **, UINT4 *));
PRIVATE INT4 CmifRetConvertStringToOid PROTO ((tSNMP_OID_TYPE *, UINT1 *));
PRIVATE VOID        CmifHandleRemoteState (tSNMP_MULTI_DATA_TYPE * pData);
PRIVATE VOID        CmifHandleLocalState (tSNMP_MULTI_DATA_TYPE * pData);
PRIVATE INT1        CmifHandleRemoteConfiguration (tSNMP_MULTI_DATA_TYPE *
                                                   pData, UINT4 u4Port);
PRIVATE INT1        CmifHandleFramesTransmittedOK (tSNMP_MULTI_DATA_TYPE *
                                                   pData, UINT4 u4Port);
PRIVATE INT1        CmifHandleFramesReceivedOK (tSNMP_MULTI_DATA_TYPE * pData,
                                                UINT4 u4Port);
PRIVATE INT1        CmifHandleLocalRemoteFlagsField (tSNMP_MULTI_DATA_TYPE *
                                                     pData, UINT4 u4Port);
PRIVATE VOID        CmifHandleVarWithMultObjMapping (tSNMP_MULTI_DATA_TYPE *
                                                     pData, UINT4 u4Port,
                                                     UINT1 *pu1DataString,
                                                     UINT2 u2Leaf);
PRIVATE INT1        CmifRetrieveFramesTransmittedOK (tSNMP_MULTI_DATA_TYPE *
                                                     pData, UINT4 u4Port,
                                                     UINT4
                                                     *pu4FramesTransmittedOK);
PRIVATE INT1        CmifRetrieveFramesReceivedOK (tSNMP_MULTI_DATA_TYPE * pData,
                                                  UINT4 u4Port,
                                                  UINT4
                                                  *pu4FramesTransmittedOK);
PRIVATE INT1        CmifHandleOctetTransmittedOK (tSNMP_MULTI_DATA_TYPE * pData,
                                                  UINT4 u4Port);

PRIVATE INT1        CmifHandleOctetReceivedOK (tSNMP_MULTI_DATA_TYPE * pData,
                                               UINT4 u4Port);

/******************************************************************************
 * Function Name      : CmifRetrieveInit
 *
 * Description        : This function is initializes the Index pool used to
 *                      retrieve variables from SNMP
 *
 * Input(s)           : u4Port - Interface number
 *                      u1Branch - CMIP Branch
 *                      u2Leaf - CMIP Leaf
 *                      u4ReqId - Unique Request Id which will be posted
 *                                along with the value to the EOAM task
 *
 * Output(s)          : None.
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
CmifRetrieveInit (VOID)
{
    UINT4               u4Count = 0;

    /* Initialize Index Pool */
    for (u4Count = 0; u4Count < CMIF_MAX_INDICES; u4Count++)
    {
        gaCmipIndexPool[u4Count].pIndex = &(gaMultiPool[u4Count]);
        gaMultiPool[u4Count].pOctetStrValue = &(gaMultiOctetPool[u4Count]);
        gaMultiPool[u4Count].pOidValue = &(gaMultiOIDPool[u4Count]);
        gaMultiPool[u4Count].pOctetStrValue->pu1_OctetList =
            (UINT1 *) gaau1MultiData[u4Count];
        gaMultiPool[u4Count].pOidValue->pu4_OidList =
            (UINT4 *) (VOID *) gaau1MultiData[u4Count];
    }

    return;
}

/******************************************************************************
 * Function Name      : CmifRetrieveVariable
 *
 * Description        : This function is invoked from EOAM module to retrieve
 *                      the value of a CMIP variable. The CMIP Interface
 *                      module retrieves the value and posts it to the EOAM
 *                      task queue.
 *
 * Input(s)           : u4Port - Interface number
 *                      u1Branch - CMIP Branch
 *                      u2Leaf - CMIP Leaf
 *                      u4ReqId - Unique Request Id which will be posted
 *                                along with the value to the EOAM task.
 *                                FutureSoft EOAM assigns a unique id 
 *                                for each variable request received. It
 *                                also expects the CMIP Interface to return
 *                                the response with the same id so that
 *                                the response can be matched with the
 *                                corresponding request
 *
 * Output(s)          : None.
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
CmifRetrieveVariable (UINT4 u4Port, UINT1 u1Branch, UINT2 u2Leaf, UINT4 u4ReqId)
{
    tEoamVarReqResp     VarResponse;
    tSNMP_OID_TYPE      SnmpOid;
    tSNMP_OID_TYPE      MultiDataOidType;
    tSNMP_MULTI_DATA_TYPE MultiData;
    tSNMP_OCTET_STRING_TYPE MultiDataOctetStr;
    tSnmpIndex         *pCmipIndexPool = NULL;
    UINT4               u4Error = 0;
    UINT4               au4Oid[MAX_OID_LEN];
    UINT1              *pu1Oid = NULL;
    UINT1               au1DataString[MAX_OCTETSTRING_SIZE];
    UINT1               au1OctetString[MAX_OCTETSTRING_SIZE];
    UINT1               au1OidString[MAX_OID_LEN];
    UINT4               au4OidType[MAX_OID_LEN];
    INT4                i4RetVal = OSIX_SUCCESS;

    MEMSET (au4Oid, 0, MAX_OID_LEN);
    MEMSET (&SnmpOid, 0, sizeof (tSNMP_OID_TYPE));
    MEMSET (&MultiDataOidType, 0, sizeof (tSNMP_OID_TYPE));
    MEMSET (&MultiData, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    MEMSET (&MultiDataOctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&VarResponse, 0, sizeof (tEoamVarReqResp));
    MEMSET (au1DataString, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (au1OctetString, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (au4OidType, 0, MAX_OID_LEN);

    SnmpOid.pu4_OidList = au4Oid;
    pCmipIndexPool = gaCmipIndexPool;
    MultiDataOctetStr.pu1_OctetList = au1OctetString;
    MultiDataOidType.pu4_OidList = au4OidType;
    MultiData.pOctetStrValue = &(MultiDataOctetStr);
    MultiData.pOidValue = &(MultiDataOidType);

    /* Packages and objects are not supported */
    switch (u1Branch)
    {
        case CMIF_BRANCH_ATTRIBUTE:
            if ((u2Leaf >= CMIF_DOT3_STAT_ATTR_START) &&
                (u2Leaf <= CMIF_DOT3_STAT_ATTR_END))
            {
                pu1Oid = gaau1Dot3StatAttr[u2Leaf - CMIF_DOT3_STAT_ATTR_START];
            }
            else
            {
                if ((u2Leaf >= CMIF_OAM_ATTR1_START) &&
                    (u2Leaf <= CMIF_OAM_ATTR1_END))
                {
                    pu1Oid = gaau1Dot3OAMAttr1[u2Leaf - CMIF_OAM_ATTR1_START];
                }
                else
                {
                    if ((u2Leaf >= CMIF_OAM_ATTR2_START) &&
                        (u2Leaf <= CMIF_OAM_ATTR2_END))
                    {
                        pu1Oid = gaau1Dot3OAMAttr2[u2Leaf -
                                                   CMIF_OAM_ATTR2_START];
                    }
                }
            }

            if ((pu1Oid != NULL) && (STRLEN (pu1Oid) > 0))
            {

                SPRINTF ((CHR1 *) au1OidString, "%s.%u", pu1Oid, u4Port);
                CmifRetConvertStringToOid (&SnmpOid, au1OidString);

                if (SNMPGet (SnmpOid, &MultiData,
                             &u4Error, pCmipIndexPool, SNMP_DEFAULT_CONTEXT)
                    == SNMP_SUCCESS)
                {
                    if (u2Leaf == CMIF_OAM_REMOTE_STATE)
                    {
                        CmifHandleRemoteState (&MultiData);
                    }
                    if (u2Leaf == CMIF_OAM_LOCAL_STATE)
                    {
                        CmifHandleLocalState (&MultiData);
                    }
                    /* Copy the retrieved data in MultiData to 
                     * au1DataString depending on data type */
                    if (CmifRetCopyRelevantData (&MultiData, au1DataString) !=
                        OSIX_SUCCESS)
                    {
                        au1DataString[0] = CMIF_ERR | CMIF_ATT_UNDETERMINED;
                    }
                }
                else
                {
                    au1DataString[0] = CMIF_ERR | CMIF_ATT_UNDETERMINED;
                }

            }
            /* If the Mib Variable Requires more than One SNMP Get to
             * retreive the Requested data */
            else
            {
                CmifHandleVarWithMultObjMapping (&MultiData, u4Port,
                                                 au1DataString, u2Leaf);
            }
            break;

        case CMIF_BRANCH_OBJECT:
            au1DataString[0] = CMIF_ERR | CMIF_OBJ_NOT_SUPP;
            break;

        case CMIF_BRANCH_PACKAGE:
            au1DataString[0] = CMIF_ERR | CMIF_PKG_NOT_SUPP;
            break;

        default:
            i4RetVal = OSIX_FAILURE;
    }

    VarResponse.u4ReqId = u4ReqId;
    VarResponse.u2Leaf = u2Leaf;
    VarResponse.u1Branch = u1Branch;
    VarResponse.RespData.pu1Data = au1DataString;

    if ((au1DataString[0] & CMIF_ERR) == 0)
    {
        /* No error, first byte contains valid data length.
         * u4DataLen includes date length + first byte */
        VarResponse.RespData.u4DataLen = au1DataString[0] + 1;
    }
    else
    {
        VarResponse.RespData.u4DataLen = 1;
    }

    EoamApiSendVarResponseToPeer (u4Port, &VarResponse);
    return i4RetVal;
}

/***************************************************************************
 *  Function Name   : CmifRetCopyRelevantData
 *  Description     : Identifies data type and Copies the relevant data
 *                    from given multidata to string that EOAM can
 *                    send to peer.
 *                    Also fills the first byte of the data string with
 *                    appropriate data length
 *  Input           : pData - Pointer to multidata
 *  Output          : pu1String - Pointer to a string where the result
 *                    will be written. Memory should be already allocated
 *                    to this string
 *  Returns         : OSIX_SUCCESS / OSIX_FAILURE
 **************************************************************************/
PRIVATE INT4
CmifRetCopyRelevantData (tSNMP_MULTI_DATA_TYPE * pData, UINT1 *pu1String)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT1               u1Type = 0;
    UINT4               u4TempVal = 0;

    FSAP_ASSERT (pu1String != NULL);
    u1Type = (UINT1) pData->i2_DataType;

    switch (u1Type)
    {
        case SNMP_DATA_TYPE_INTEGER32:
            /* Copy length, Make sure first bit is zero */
            pu1String[0] = (UINT1) (CMIF_NO_ERR & (sizeof (INT4)));
            /* Copy value by converting it to Network Byte order. */
            u4TempVal = OSIX_HTONL (pData->i4_SLongValue);
            MEMCPY (pu1String + 1, &u4TempVal, sizeof (INT4));
            break;
        case SNMP_DATA_TYPE_UNSIGNED32:
        case SNMP_DATA_TYPE_TIME_TICKS:
        case SNMP_DATA_TYPE_COUNTER:
            /* Copy length, Make sure first bit is zero */
            pu1String[0] = (UINT1) (CMIF_NO_ERR & (sizeof (UINT4)));
            /* Copy value by converting it to Network Byte order. */
            u4TempVal = OSIX_HTONL (pData->u4_ULongValue);
            MEMCPY (pu1String + 1, &u4TempVal, sizeof (UINT4));
            break;
        case SNMP_DATA_TYPE_COUNTER64:
            /* Copy length, Make sure first bit is zero */
            pu1String[0] = (UINT1)
                (CMIF_NO_ERR & (sizeof (tSNMP_COUNTER64_TYPE)));
            /* Copy value */
            MEMCPY (pu1String + 1, &(pData->u8_Counter64Value), 8);
            break;

        case SNMP_DATA_TYPE_OCTET_PRIM:
        case SNMP_DATA_TYPE_IMP_OCTET_PRIM:
            if (pData->pOctetStrValue->i4_Length != 0)
            {
                /* Variable length should not exceed max supported length */
                if (pData->pOctetStrValue->i4_Length <= CMIF_MAX_VAR_LEN)
                {
                    /* Copy length, Make sure first bit is zero */
                    pu1String[0] =
                        (UINT1) (CMIF_NO_ERR & pData->pOctetStrValue->
                                 i4_Length);
                    /* Copy value */
                    MEMCPY (pu1String + 1, pData->pOctetStrValue->pu1_OctetList,
                            pData->pOctetStrValue->i4_Length);
                }
                else
                {
                    /* Value exceeded max allowed length */
                    pu1String[0] = CMIF_ERR | CMIF_ATT_OVERFLOW;
                }
            }
            else
            {
                /* No error but data length is zero */
                pu1String[0] = CMIF_NO_ERR;
            }
            break;
        default:
            i4RetVal = OSIX_FAILURE;
    }

    return i4RetVal;
}

/************************************************************************
 *  Function Name   : CmifRetConvertSubOid 
 *  
 *  Description     : Convert String pointer to decimal integer32
 *  
 *  Input           : ppu1String - String Pointer
 *  
 *  Output          : pu4Value 
 *  
 *  Returns         : 0 or -1
 ************************************************************************/
PRIVATE INT1
CmifRetConvertSubOid (UINT1 **ppu1String, UINT4 *pu4Value)
{
    UINT4               u4Count = 0, u4Value = 0;
    UINT1               u1Value = 0;
    for (u4Count = 0; ((u4Count < 11) && (**ppu1String != '.') &&
                       (**ppu1String != '\0')); u4Count++)
    {
        if (ISDIGIT (**ppu1String) == 0)
        {
            return -1;
        }
        MEMCPY (&u1Value, *ppu1String, 1);
        u4Value = (u4Value * 10) + (0x0f & (UINT4) u1Value);
        (*ppu1String)++;
    }
    *pu4Value = u4Value;

    return 0;
}

/************************************************************************
 *  Function Name   : CmifRetConvertStringToOid 
 *  Description     : Convert string to OID Type
 *  Input           : pu1Data - String to be converted into oid
 *  Output          : pOid - Pointer where the OID will be written
 *  Returns         : SUCCESS or FAILURE
 ************************************************************************/
PRIVATE INT4
CmifRetConvertStringToOid (tSNMP_OID_TYPE * pOid, UINT1 *pu1Data)
{
    UINT4               u4Count = 0, u4Dot = 0;
    UINT4               u4Value = 0;
    UINT1              *pu1String = pu1Data;
    INT4                i4RetVal = OSIX_SUCCESS;
    INT1                i1RetStatus = 0;

    pOid->u4_Length = 0;
    for (u4Count = 0; pu1String[u4Count] != '\0'; u4Count++)
    {
        if (pu1String[u4Count] == '.')
        {
            u4Dot++;
        }
    }
    for (u4Count = 0; u4Count < (u4Dot + 1); u4Count++)
    {
        if (ISDIGIT ((INT4) *pu1String) != 0)
        {
            i1RetStatus = CmifRetConvertSubOid (&pu1String, &u4Value);
            if (i1RetStatus == -1)
            {
                i4RetVal = OSIX_FAILURE;
                break;
            }
            pOid->pu4_OidList[u4Count] = u4Value;
            pOid->u4_Length++;
        }
        else
        {
            i4RetVal = OSIX_FAILURE;
            break;
        }
        if (*pu1String == '.')
        {
            pu1String++;
        }
        else if (*pu1String != '\0')
        {
            i4RetVal = OSIX_FAILURE;
            break;
        }
    }
    return i4RetVal;
}

/************************************************************************
 *  Function Name   : CmifHandleLocalState
 *  Description     : This function handles the MIB variable Request
 *                    for aOAMLocalState
 *  Input           : pData - SNMP Multidata which contains the 
 *                            LoopbackStatus
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
PRIVATE VOID
CmifHandleLocalState (tSNMP_MULTI_DATA_TYPE * pData)
{
    /*
       Value           |    Parser     Mux
       ======================================
       noLoopback      |     FWD       FWD 
       initLoopback    |    DISCARD   DISCARD
       rmtLoopback     |    DISCARD    FWD 
       tmtngLoopback   |    DISCARD   DISCARD
       lclLoopback     |    LPBK      DISCARD
       unknown         |   ***   any other combination   ***
     */

    switch (pData->i4_SLongValue)
    {
        case EOAM_NO_LOOPBACK:
            pData->i4_SLongValue = EOAM_PAR_FWD_STATE | EOAM_MUX_FWD_STATE;
            break;
        case EOAM_INITIATING_LOOPBACK:
        case EOAM_TERMINATING_LOOPBACK:
            pData->i4_SLongValue =
                EOAM_PAR_DISCARD_STATE | EOAM_MUX_DISCARD_STATE;
            break;
        case EOAM_REMOTE_LOOPBACK:
            pData->i4_SLongValue = EOAM_PAR_DISCARD_STATE | EOAM_MUX_FWD_STATE;
            break;
        case EOAM_LOCAL_LOOPBACK:
            pData->i4_SLongValue = EOAM_PAR_LB_STATE | EOAM_MUX_DISCARD_STATE;
            break;
    }
}

/************************************************************************
 *  Function Name   : CmifHandleRemoteState
 *  Description     : This function handles the MIB variable Request
 *                    for aOAMRemoteState
 *  Input           : pData - SNMP Multidata which contains the 
 *                            LoopbackStatus
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
PRIVATE VOID
CmifHandleRemoteState (tSNMP_MULTI_DATA_TYPE * pData)
{

    /*
       Value           |    Parser     Mux
       ======================================
       noLoopback      |     FWD       FWD
       initLoopback    |     FWD       FWD
       rmtLoopback     |    LPBK    DISCARD
       tmtngLoopback   |    LPBK    DISCARD
       lclLoopback     |   DISCARD     FWD
       unknown         |   ***   any other combination   ***
     */

    switch (pData->i4_SLongValue)
    {
        case EOAM_NO_LOOPBACK:
        case EOAM_INITIATING_LOOPBACK:
            pData->i4_SLongValue = EOAM_PAR_FWD_STATE | EOAM_MUX_FWD_STATE;
            break;
        case EOAM_REMOTE_LOOPBACK:
        case EOAM_TERMINATING_LOOPBACK:
            pData->i4_SLongValue = EOAM_PAR_LB_STATE | EOAM_MUX_DISCARD_STATE;
            break;
        case EOAM_LOCAL_LOOPBACK:
            pData->i4_SLongValue = EOAM_PAR_DISCARD_STATE | EOAM_MUX_FWD_STATE;
            break;
    }
}

/************************************************************************
 *  Function Name   : CmifHandleVarWithMultObjMapping
 *  Description     : This function handles the MIB variable Request
 *                    for Variables which are Mapped to more than
 *                    one MIB Objects
 *  Input           : pData  - Pointer to multidata
 *                    u4Port - Interface Number 
 *                    u2Leaf - Input Leaf Number
 *  Output          : pu1String - Pointer to a string where the result
 *                    will be written. Memory should be already allocated
 *                    to this string
 *  Returns         : None
 ************************************************************************/
PRIVATE VOID
CmifHandleVarWithMultObjMapping (tSNMP_MULTI_DATA_TYPE * pData,
                                 UINT4 u4Port, UINT1 *pu1DataString,
                                 UINT2 u2Leaf)
{
    INT1                i1RetVal = OSIX_SUCCESS;
    switch (u2Leaf)
    {
        case CMIF_OAM_REMOTE_CONFIGURATION:
            i1RetVal = CmifHandleRemoteConfiguration (pData, u4Port);
            break;

        case CMIF_OAM_LOCAL_FLAGS:
        case CMIF_OAM_REMOTE_FLAGS:
            i1RetVal = CmifHandleLocalRemoteFlagsField (pData, u4Port);
            break;

        case CMIF_OAM_FRAMES_TX_OK:
            i1RetVal = CmifHandleFramesTransmittedOK (pData, u4Port);
            break;

        case CMIF_OAM_FRAMES_RX_OK:
            i1RetVal = CmifHandleFramesReceivedOK (pData, u4Port);
            break;

        case CMIF_OAM_OCTETS_TX_OK:
            i1RetVal = CmifHandleOctetTransmittedOK (pData, u4Port);
            break;

        case CMIF_OAM_OCTETS_RX_OK:
            i1RetVal = CmifHandleOctetReceivedOK (pData, u4Port);
            break;

        default:
            *pu1DataString = CMIF_ERR | CMIF_ATT_NOT_SUPP;
            return;

    }
    if (i1RetVal == OSIX_SUCCESS)
    {
        if (CmifRetCopyRelevantData (pData, pu1DataString) != OSIX_SUCCESS)
        {
            *pu1DataString = CMIF_ERR | CMIF_ATT_UNDETERMINED;
        }
    }
    else
    {
        *pu1DataString = CMIF_ERR | CMIF_ATT_UNDETERMINED;
    }

}

/************************************************************************
 *  Function Name   : CmifHandleRemoteConfiguration
 *  Description     : This function handles the MIB variable Request
 *                    for aOAMRemoteConfiguration
 *  Input           : pData  - Pointer to multidata
 *                    u4Port - Interface Number
 *  Output          : None
 *  Returns         : OSIX_SUCCESS / OSIX_FAILURE
 ************************************************************************/
PRIVATE INT1
CmifHandleRemoteConfiguration (tSNMP_MULTI_DATA_TYPE * pData, UINT4 u4Port)
{
    tSNMP_OID_TYPE      SnmpOid;
    UINT4               au4Oid[MAX_OID_LEN];
    tSnmpIndex         *pCmipIndexPool = NULL;
    UINT4               u4Error = 0;
    UINT1               au1OidString[MAX_OID_LEN];
    UINT1               u1PeerMode = 0;
    UINT1               u1PeerFncSupp = 0;
    UINT1               u1RemoteConfiguration = 0;
    INT1                i1RetVal = OSIX_FAILURE;

    MEMSET (&SnmpOid, 0, sizeof (tSNMP_OID_TYPE));
    MEMSET (au4Oid, 0, MAX_OID_LEN);

    SnmpOid.pu4_OidList = au4Oid;
    pCmipIndexPool = gaCmipIndexPool;

    /* SNMP Get for the Object dot3OamPeerMode */
    SPRINTF ((CHR1 *) au1OidString, CMIF_OAM_PEER_MODE_OID ".%u", u4Port);
    CmifRetConvertStringToOid (&SnmpOid, au1OidString);

    if (SNMPGet (SnmpOid, pData,
                 &u4Error, pCmipIndexPool, SNMP_DEFAULT_CONTEXT)
        == SNMP_SUCCESS)
    {
        if (pData->i4_SLongValue == EOAM_MODE_ACTIVE)
        {
            u1PeerMode = u1PeerMode | EOAM_MODE_BITMASK;
        }
        i1RetVal = OSIX_SUCCESS;
    }
    /* SNMP Get for the Object dot3OamPeerFunctionsSupported */
    SPRINTF ((CHR1 *) au1OidString, CMIF_OAM_PEER_FNC_SUPP_OID ".%u", u4Port);
    CmifRetConvertStringToOid (&SnmpOid, au1OidString);

    if (SNMPGet (SnmpOid, pData,
                 &u4Error, pCmipIndexPool, SNMP_DEFAULT_CONTEXT)
        == SNMP_SUCCESS)
    {
        u1PeerFncSupp = (UINT1) *((pData->pOctetStrValue)->pu1_OctetList);
        i1RetVal = OSIX_SUCCESS;
    }

    /* According to the RFC 4878, the Managed Object aOAMRemoteConfiguration
     * is Mapped to (dot3OamPeerFunctionsSupported, dot3OamPeerMode) */
    u1RemoteConfiguration = u1PeerFncSupp << 1;
    pData->i4_SLongValue = u1RemoteConfiguration | u1PeerMode;
    pData->i2_DataType = SNMP_DATA_TYPE_INTEGER32;

    return i1RetVal;
}

/************************************************************************
 *  Function Name   : CmifRetrieveFramesTransmittedOK
 *  Description     : This function handles the MIB variable Request
 *                    for aFramesTransmittedOK 
 *  Input           : pData  - Pointer to multidata
 *                    u4Port - Interface Number 
 *  Output          : pu4FramesTransmittedOK - FramesTransmittedOK Value
 *  Returns         : OSIX_SUCCESS / OSIX_FAILURE
 ************************************************************************/
PRIVATE INT1
CmifRetrieveFramesTransmittedOK (tSNMP_MULTI_DATA_TYPE * pData,
                                 UINT4 u4Port, UINT4 *pu4FramesTransmittedOK)
{
    tSNMP_OID_TYPE      SnmpOid;
    UINT4               au4Oid[MAX_OID_LEN];
    tSnmpIndex         *pCmipIndexPool = NULL;
    UINT4               u4Error = 0;
    UINT1               au1OidString[MAX_OID_LEN];
    UINT4               u4IfOutUcastPkts = 0;
    UINT4               u4IfOutMcastPkts = 0;
    UINT4               u4IfOutBcastPkts = 0;
    UINT4               u4Dot3OutPauseFrames = 0;
    UINT4               u4IfOutErrors = 0;
    UINT4               u4IfOutDiscards = 0;
    INT1                i1RetVal = OSIX_FAILURE;

    MEMSET (&SnmpOid, 0, sizeof (tSNMP_OID_TYPE));
    MEMSET (au4Oid, 0, MAX_OID_LEN);

    SnmpOid.pu4_OidList = au4Oid;
    pCmipIndexPool = gaCmipIndexPool;

    /* SNMP Get for the Mib Object ifOutUcastPkts */
    SPRINTF ((CHR1 *) au1OidString, CMIF_IFOUT_UCAST_PKT_OID ".%u", u4Port);
    CmifRetConvertStringToOid (&SnmpOid, au1OidString);

    if (SNMPGet (SnmpOid, pData,
                 &u4Error, pCmipIndexPool, SNMP_DEFAULT_CONTEXT)
        == SNMP_SUCCESS)
    {
        u4IfOutUcastPkts = pData->u4_ULongValue;
        i1RetVal = OSIX_SUCCESS;
    }

    /* SNMP Get for the Mib Object ifOutMulticastPkts */
    SPRINTF ((CHR1 *) au1OidString, CMIF_IFOUT_MCAST_PKT_OID ".%u", u4Port);
    CmifRetConvertStringToOid (&SnmpOid, au1OidString);

    if (SNMPGet (SnmpOid, pData,
                 &u4Error, pCmipIndexPool, SNMP_DEFAULT_CONTEXT)
        == SNMP_SUCCESS)
    {
        u4IfOutMcastPkts = pData->u4_ULongValue;
        i1RetVal = OSIX_SUCCESS;
    }

    /* SNMP Get for the Mib Object ifOutBroadcastPkts */
    SPRINTF ((CHR1 *) au1OidString, CMIF_IFOUT_BCAST_PKT_OID ".%u", u4Port);
    CmifRetConvertStringToOid (&SnmpOid, au1OidString);

    if (SNMPGet (SnmpOid, pData,
                 &u4Error, pCmipIndexPool, SNMP_DEFAULT_CONTEXT)
        == SNMP_SUCCESS)
    {
        u4IfOutBcastPkts = pData->u4_ULongValue;
        i1RetVal = OSIX_SUCCESS;
    }

    /* SNMP Get for the Mib Object dot3OutPauseFrames */
    SPRINTF ((CHR1 *) au1OidString, CMIF_DOT3_OUT_PAUSE_FR_OID ".%u", u4Port);
    CmifRetConvertStringToOid (&SnmpOid, au1OidString);

    if (SNMPGet (SnmpOid, pData,
                 &u4Error, pCmipIndexPool, SNMP_DEFAULT_CONTEXT)
        == SNMP_SUCCESS)
    {
        u4Dot3OutPauseFrames = pData->u4_ULongValue;
        i1RetVal = OSIX_SUCCESS;
    }

    /* SNMP Get for the Mib Object ifOutErrors */
    SPRINTF ((CHR1 *) au1OidString, CMIF_IFOUT_ERRORS_OID ".%u", u4Port);
    CmifRetConvertStringToOid (&SnmpOid, au1OidString);

    if (SNMPGet (SnmpOid, pData,
                 &u4Error, pCmipIndexPool, SNMP_DEFAULT_CONTEXT)
        == SNMP_SUCCESS)
    {
        u4IfOutErrors = pData->u4_ULongValue;
        i1RetVal = OSIX_SUCCESS;
    }

    /* SNMP Get for the Mib Object ifOutDiscards */
    SPRINTF ((CHR1 *) au1OidString, CMIF_IFOUT_DISCARDS_OID ".%u", u4Port);
    CmifRetConvertStringToOid (&SnmpOid, au1OidString);

    if (SNMPGet (SnmpOid, pData,
                 &u4Error, pCmipIndexPool, SNMP_DEFAULT_CONTEXT)
        == SNMP_SUCCESS)
    {
        u4IfOutDiscards = pData->u4_ULongValue;
        i1RetVal = OSIX_SUCCESS;
    }

    /* According to Section 3.2.6 in RFC 3635, Which says 
     * aFramesTransmittedOK = ifOutUcastPkts + ifOutMulticastPkts +
     *                        ifOutBroadcastPkts + dot3OutPauseFrames -
     *                        (ifOutErrors + ifOutDiscards)         */

    *pu4FramesTransmittedOK = (u4IfOutUcastPkts + u4IfOutMcastPkts +
                               u4IfOutBcastPkts + u4Dot3OutPauseFrames) -
        (u4IfOutErrors + u4IfOutDiscards);
    return i1RetVal;
}

/************************************************************************
 *  Function Name   : CmifRetrieveFramesReceivedOK
 *  Description     : This function handles the MIB variable Request
 *                    for aFramesReceivedOK 
 *  Input           : pData  - Pointer to multidata
 *                    u4Port - Interface Number 
 *  Output          : pu4FramesReceivedOK - FramesReceivedOK Value
 *  Returns         : OSIX_SUCCESS / OSIX_FAILURE
 ************************************************************************/
PRIVATE INT1
CmifRetrieveFramesReceivedOK (tSNMP_MULTI_DATA_TYPE * pData,
                              UINT4 u4Port, UINT4 *pu4FramesReceivedOK)
{
    tSNMP_OID_TYPE      SnmpOid;
    UINT4               au4Oid[MAX_OID_LEN];
    tSnmpIndex         *pCmipIndexPool = NULL;
    UINT4               u4Error = 0;
    UINT1               au1OidString[MAX_OID_LEN];
    UINT4               u4IfInUcastPkts = 0;
    UINT4               u4IfInMcastPkts = 0;
    UINT4               u4IfInBcastPkts = 0;
    UINT4               u4Dot3InPauseFrames = 0;
    UINT4               u4IfInDiscards = 0;
    UINT4               u4IfInUnknownProtos = 0;
    INT1                i1RetVal = OSIX_FAILURE;

    MEMSET (&SnmpOid, 0, sizeof (tSNMP_OID_TYPE));
    MEMSET (au4Oid, 0, MAX_OID_LEN);

    SnmpOid.pu4_OidList = au4Oid;
    pCmipIndexPool = gaCmipIndexPool;

    /* SNMP Get for the Mib Object ifInUcastPkts */
    SPRINTF ((CHR1 *) au1OidString, CMIF_IFIN_UCAST_PKT_OID ".%u", u4Port);
    CmifRetConvertStringToOid (&SnmpOid, au1OidString);

    if (SNMPGet (SnmpOid, pData,
                 &u4Error, pCmipIndexPool, SNMP_DEFAULT_CONTEXT)
        == SNMP_SUCCESS)
    {
        u4IfInUcastPkts = pData->u4_ULongValue;
        i1RetVal = OSIX_SUCCESS;
    }

    /* SNMP Get for the Mib Object ifInMulticastPkts */
    SPRINTF ((CHR1 *) au1OidString, CMIF_IFIN_MCAST_PKT_OID ".%u", u4Port);
    CmifRetConvertStringToOid (&SnmpOid, au1OidString);

    if (SNMPGet (SnmpOid, pData,
                 &u4Error, pCmipIndexPool, SNMP_DEFAULT_CONTEXT)
        == SNMP_SUCCESS)
    {
        u4IfInMcastPkts = pData->u4_ULongValue;
        i1RetVal = OSIX_SUCCESS;
    }

    /* SNMP Get for the Mib Object ifInBroadcastPkts */
    SPRINTF ((CHR1 *) au1OidString, CMIF_IFIN_BCAST_PKT_OID ".%u", u4Port);
    CmifRetConvertStringToOid (&SnmpOid, au1OidString);

    if (SNMPGet (SnmpOid, pData,
                 &u4Error, pCmipIndexPool, SNMP_DEFAULT_CONTEXT)
        == SNMP_SUCCESS)
    {
        u4IfInBcastPkts = pData->u4_ULongValue;
        i1RetVal = OSIX_SUCCESS;
    }

    /* SNMP Get for the Mib Object dot3InPauseFrames */
    SPRINTF ((CHR1 *) au1OidString, CMIF_DOT3_IN_PAUSE_FR_OID ".%u", u4Port);
    CmifRetConvertStringToOid (&SnmpOid, au1OidString);

    if (SNMPGet (SnmpOid, pData,
                 &u4Error, pCmipIndexPool, SNMP_DEFAULT_CONTEXT)
        == SNMP_SUCCESS)
    {
        u4Dot3InPauseFrames = pData->u4_ULongValue;
        i1RetVal = OSIX_SUCCESS;
    }

    /* SNMP Get for the Mib Object ifInDiscards */
    SPRINTF ((CHR1 *) au1OidString, CMIF_IFIN_DISCARDS_OID ".%u", u4Port);
    CmifRetConvertStringToOid (&SnmpOid, au1OidString);

    if (SNMPGet (SnmpOid, pData,
                 &u4Error, pCmipIndexPool, SNMP_DEFAULT_CONTEXT)
        == SNMP_SUCCESS)
    {
        u4IfInDiscards = pData->u4_ULongValue;
        i1RetVal = OSIX_SUCCESS;
    }

    /* SNMP Get for the Mib Object ifInUnknownProtos */
    SPRINTF ((CHR1 *) au1OidString, CMIF_IFIN_UNKNOWN_PROTOS_OID ".%u", u4Port);
    CmifRetConvertStringToOid (&SnmpOid, au1OidString);

    if (SNMPGet (SnmpOid, pData,
                 &u4Error, pCmipIndexPool, SNMP_DEFAULT_CONTEXT)
        == SNMP_SUCCESS)
    {
        u4IfInUnknownProtos = pData->u4_ULongValue;
        i1RetVal = OSIX_SUCCESS;
    }

    /* According to Section 3.2.6 in RFC 3635, which defines 
     * aFramesReceivedOK = ifInUcastPkts + ifInMulticastPkts +
     *                     ifInBroadcastPkts + dot3InPauseFrames +
     *                     ifInDiscards + ifInUnknownProtos    */

    *pu4FramesReceivedOK = u4IfInUcastPkts + u4IfInMcastPkts + u4IfInBcastPkts +
        u4Dot3InPauseFrames + u4IfInDiscards + u4IfInUnknownProtos;
    return i1RetVal;
}

/************************************************************************
 *  Function Name   : CmifHandleFramesTransmittedOK
 *  Description     : This function handles the MIB variable Request
 *                    for aFramesTransmittedOK 
 *  Input           : pData  - Pointer to multidata
 *                    u4Port - Interface Number 
 *  Output          : None
 *  Returns         : OSIX_SUCCESS / OSIX_FAILURE
  ************************************************************************/
PRIVATE INT1
CmifHandleFramesTransmittedOK (tSNMP_MULTI_DATA_TYPE * pData, UINT4 u4Port)
{
    UINT4               u4FramesTransmittedOK = 0;
    INT1                i1RetVal = OSIX_FAILURE;

    if (CmifRetrieveFramesTransmittedOK (pData, u4Port, &u4FramesTransmittedOK)
        == OSIX_SUCCESS)
    {
        pData->u4_ULongValue = u4FramesTransmittedOK;
        pData->i2_DataType = SNMP_DATA_TYPE_COUNTER;
        i1RetVal = OSIX_SUCCESS;
    }
    return i1RetVal;
}

/************************************************************************
 *  Function Name   : CmifHandleFramesReceivedOK
 *  Description     : This function handles the MIB variable Request
 *                    for aFramesReceivedOK 
 *  Input           : pData  - Pointer to multidata
 *                    u4Port - Interface Number 
 *  Output          : None
 *  Returns         : OSIX_SUCCESS / OSIX_FAILURE
 ************************************************************************/
PRIVATE INT1
CmifHandleFramesReceivedOK (tSNMP_MULTI_DATA_TYPE * pData, UINT4 u4Port)
{
    UINT4               u4FramesReceivedOK = 0;
    INT1                i1RetVal = OSIX_FAILURE;

    if (CmifRetrieveFramesReceivedOK (pData, u4Port, &u4FramesReceivedOK)
        == OSIX_SUCCESS)
    {
        pData->u4_ULongValue = u4FramesReceivedOK;
        pData->i2_DataType = SNMP_DATA_TYPE_COUNTER;
        i1RetVal = OSIX_SUCCESS;
    }
    return i1RetVal;
}

/************************************************************************
 *  Function Name   : CmifHandleLocalRemoteFlagsField
 *  Description     : This function handles the MIB variable Request
 *                    for aOAMLocalFlagsField, aOAMRemoteFlagsField
 *  Input           : pData  - Pointer to multidata
 *                    u4Port - Interface Number 
 *  Output          : None
 *  Returns         : OSIX_SUCCESS / OSIX_FAILURE
 ************************************************************************/
PRIVATE INT1
CmifHandleLocalRemoteFlagsField (tSNMP_MULTI_DATA_TYPE * pData, UINT4 u4Port)
{

    tSNMP_OID_TYPE      SnmpOid;
    UINT4               au4Oid[MAX_OID_LEN];
    tSnmpIndex         *pCmipIndexPool = NULL;
    UINT4               u4Error = 0;
    UINT1               au1OidString[MAX_OID_LEN];
    UINT4               u4EventLogType = 0;
    UINT1               u1OperState = 0;
    UINT1               u1LocalRemoteFlagsField = 0;
    UINT1               u1LogEntryId = 0;
    INT1                i1RetVal = OSIX_FAILURE;

    MEMSET (&SnmpOid, 0, sizeof (tSNMP_OID_TYPE));
    MEMSET (au4Oid, 0, MAX_OID_LEN);

    SnmpOid.pu4_OidList = au4Oid;
    pCmipIndexPool = gaCmipIndexPool;

    /* Scanning the dot3OamEventLogTable and Obtaining dot3OamEventLogType */
    while (u1LogEntryId < EOAM_MAX_LOG_ENTRIES)
    {
        SPRINTF ((CHR1 *) au1OidString, CMIF_OAM_EVENT_LOGTYPE_OID ".%u.%u",
                 u4Port, u1LogEntryId);
        CmifRetConvertStringToOid (&SnmpOid, au1OidString);

        u4EventLogType = 0;
        if (SNMPGet (SnmpOid, pData,
                     &u4Error, pCmipIndexPool, SNMP_DEFAULT_CONTEXT)
            == SNMP_SUCCESS)
        {
            u4EventLogType = pData->u4_ULongValue;
            i1RetVal = OSIX_SUCCESS;
        }
        /* Setting the First Bit which Refers Link Fault Event */
        if (u4EventLogType == EOAM_LINK_FAULT)
        {
            u1LocalRemoteFlagsField |= EOAM_LINK_FAULT_BITMASK;
        }
        /* Setting the Second Bit which Refers Dying Gasp Event */
        else if (u4EventLogType == EOAM_DYING_GASP)
        {
            u1LocalRemoteFlagsField |= EOAM_DYING_GASP_BITMASK;
        }

        /* Setting the Third Bit which Refers Critical Event */
        else if (u4EventLogType == EOAM_CRITICAL_EVENT)
        {
            u1LocalRemoteFlagsField |= EOAM_CRITICAL_EVENT_BITMASK;
        }
        u1LogEntryId = u1LogEntryId + 1;

    }

    /* SNMP Get for the Mib Object dot3OamOperStatus */
    SPRINTF ((CHR1 *) au1OidString, CMIF_OAM_OPER_STATE_OID ".%u", u4Port);
    CmifRetConvertStringToOid (&SnmpOid, au1OidString);

    if (SNMPGet (SnmpOid, pData,
                 &u4Error, pCmipIndexPool, SNMP_DEFAULT_CONTEXT)
        == SNMP_SUCCESS)
    {
        u1OperState = pData->i4_SLongValue;
        i1RetVal = OSIX_SUCCESS;
    }
    switch (u1OperState)
    {
        case EOAM_OPER_DISABLE:
        case EOAM_OPER_PASSIVEWAIT:
        case EOAM_OPER_ACTIVE_SENDLOCAL:
        case EOAM_OPER_PEERINGLOCALLYREJECTED:
            /* No need to Fill Local Stable & Evaluating and
             * Remote Stable & Evaluating */
            break;

        case EOAM_OPER_HALF_DUPLEX:
        case EOAM_OPER_LINKFAULT:
            /* Need to fill only Local Evaluating */
            u1LocalRemoteFlagsField |= EOAM_LOCAL_EVAL_BITMASK;
            break;

        case EOAM_OPER_SENDLOCALREMOTE:
            /* Need to fill Local & Remote Evaluating */
            u1LocalRemoteFlagsField |=
                EOAM_LOCAL_EVAL_BITMASK | EOAM_REMOTE_EVAL_BITMASK;
            break;

        case EOAM_OPER_SENDLOCALREMOTEOK:
        case EOAM_OPER_PEERINGREMOTELYREJECTED:
        case EOAM_OPER_OPERATIONAL:
            /* Need to fill Local & Remote Stable */
            u1LocalRemoteFlagsField |=
                EOAM_LOCAL_STABLE_BITMASK | EOAM_REMOTE_STABLE_BITMASK;
            break;
    }

    /* As defined in the RFC 4878, the Managed Objects 
     * aOAMLocalFlagsField  and aOAMRemoteFlagsField is mapped 
     * to two MIB objects (dot3OamOperStatus, dot3OamEventLogEntry) */
    pData->i4_SLongValue = u1LocalRemoteFlagsField;
    pData->i2_DataType = SNMP_DATA_TYPE_INTEGER32;

    return i1RetVal;
}

/************************************************************************
 *  Function Name   : CmifHandleOctetTransmittedOK
 *  Description     : This function handles the MIB variable Request
 *                    for aOctetTransmittedOK 
 *  Input           : pData  - Pointer to multidata
 *                    u4Port - Interface Number 
 *  Output          : None
 *  Returns         : OSIX_SUCCESS / OSIX_FAILURE
 ************************************************************************/
PRIVATE INT1
CmifHandleOctetTransmittedOK (tSNMP_MULTI_DATA_TYPE * pData, UINT4 u4Port)
{
    UINT4               u4FramesTransmittedOK = 0;
    tSNMP_OID_TYPE      SnmpOid;
    UINT4               au4Oid[MAX_OID_LEN];
    tSnmpIndex         *pCmipIndexPool = NULL;
    UINT4               u4Error = 0;
    UINT1               au1OidString[MAX_OID_LEN];
    UINT4               u4IfOutOctets = 0;
    UINT4               u4OctetTransmittedOK = 0;
    INT1                i1RetVal = OSIX_FAILURE;

    MEMSET (&SnmpOid, 0, sizeof (tSNMP_OID_TYPE));
    MEMSET (au4Oid, 0, MAX_OID_LEN);

    SnmpOid.pu4_OidList = au4Oid;
    pCmipIndexPool = gaCmipIndexPool;

    /* SNMP Get for the Mib Object ifOutOctets */
    SPRINTF ((CHR1 *) au1OidString, CMIF_IFOUT_OCTETS_OID ".%u", u4Port);
    CmifRetConvertStringToOid (&SnmpOid, au1OidString);

    if (SNMPGet (SnmpOid, pData,
                 &u4Error, pCmipIndexPool, SNMP_DEFAULT_CONTEXT)
        == SNMP_SUCCESS)
    {
        u4IfOutOctets = pData->u4_ULongValue;
        i1RetVal = OSIX_SUCCESS;
    }

    /* Retreiving the Value of aFramesTransmittedOK */
    if (CmifRetrieveFramesTransmittedOK (pData, u4Port, &u4FramesTransmittedOK)
        == OSIX_SUCCESS)
    {
        i1RetVal = OSIX_SUCCESS;
    }

    /* According to Section 3.2.6 of RFC 3635, we obtain the following Equation 
     * ifOutOctets = aOctetsTransmittedOK + (18 * aFramesTransmittedOK) */
    u4OctetTransmittedOK = (UINT4) u4IfOutOctets - (18 * u4FramesTransmittedOK);

    pData->i4_SLongValue = u4OctetTransmittedOK;
    pData->i2_DataType = SNMP_DATA_TYPE_INTEGER32;

    return i1RetVal;
}

/************************************************************************
 *  Function Name   : CmifHandleOctetReceivedOK
 *  Description     : This function handles the MIB variable Request
 *                    for aOctetReceivedOK 
 *  Input           : pData  - Pointer to multidata
 *                    u4Port - Interface Number 
 *  Output          : None
 *  Returns         : OSIX_SUCCESS / OSIX_FAILURE
 *************************************************************************/
PRIVATE INT1
CmifHandleOctetReceivedOK (tSNMP_MULTI_DATA_TYPE * pData, UINT4 u4Port)
{
    UINT4               u4FramesReceivedOK;
    tSNMP_OID_TYPE      SnmpOid;
    UINT4               au4Oid[MAX_OID_LEN];
    tSnmpIndex         *pCmipIndexPool = NULL;
    UINT4               u4Error = 0;
    UINT1               au1OidString[MAX_OID_LEN];
    UINT4               u4IfInOctets = 0;
    UINT4               u4OctetReceivedOK = 0;
    INT1                i1RetVal = OSIX_FAILURE;

    MEMSET (&SnmpOid, 0, sizeof (tSNMP_OID_TYPE));
    MEMSET (au4Oid, 0, MAX_OID_LEN);

    SnmpOid.pu4_OidList = au4Oid;
    pCmipIndexPool = gaCmipIndexPool;

    /* SNMP Get for the Mib Object ifInOctets */
    SPRINTF ((CHR1 *) au1OidString, CMIF_IFIN_OCTETS_OID ".%u", u4Port);
    CmifRetConvertStringToOid (&SnmpOid, au1OidString);

    if (SNMPGet (SnmpOid, pData,
                 &u4Error, pCmipIndexPool, SNMP_DEFAULT_CONTEXT)
        == SNMP_SUCCESS)
    {
        u4IfInOctets = pData->u4_ULongValue;
        i1RetVal = OSIX_SUCCESS;
    }

    /* Retreiving the Value of aFramesReceivedOK */
    if (CmifRetrieveFramesReceivedOK (pData, u4Port, &u4FramesReceivedOK)
        == OSIX_SUCCESS)
    {
        i1RetVal = OSIX_SUCCESS;
    }

    /* According to Section 3.2.6 of RFC 3635, we obtain the following Equation 
     * ifInOctets = aOctetsReceivedOK + (18 * aFramesReceivedOK) */
    u4OctetReceivedOK = (UINT4) (u4IfInOctets - (18 * u4FramesReceivedOK));

    pData->i4_SLongValue = u4OctetReceivedOK;
    pData->i2_DataType = SNMP_DATA_TYPE_INTEGER32;

    return i1RetVal;
}

#endif /* CMIFRET_C */
/*-----------------------------------------------------------------------*/
/*                       End of the file  cmifret.c                      */
/*-----------------------------------------------------------------------*/
