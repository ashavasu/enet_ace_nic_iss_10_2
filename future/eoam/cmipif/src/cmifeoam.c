/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cmifeoam.c
 *
 * Description: This file contains the callback function of FutureSoft CMIP
 *              interface module that will be registered FutureSoft EOAM module
 *****************************************************************************/
#include "cmifinc.h"

/******************************************************************************
 * Function Name      : CmifEoamVariableRetrieve
 *
 * Description        : This API posts a message to CMIP Interface task
 *                      queue requesting CMIP Interface to retrieve a CMIP MIB 
 *                      variable
 *
 * Input(s)           : pCallbackInfo - Callback info from EOAM
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
INT4
CmifEoamVariableRetrieve (tEoamCallbackInfo * pCallbackInfo)
{
    tCmipIntfQMsg      *pMsg = NULL;

    CMIF_TRC_ARG1 (CONTROL_PLANE_TRC,
                   "CmifEoamVariableRetrieve: Requesting MIB Variable "
                   "retrieval on port %d\n", pCallbackInfo->u4Port);

    if (gCmipIntfGlobalInfo.u1SystemCtrlStatus == CMIF_SHUTDOWN)
    {
        CMIF_TRC (ALL_FAILURE_TRC, "CMIP Interface is not started\n");
        return OSIX_FAILURE;
    }

    if (pCallbackInfo->u4EventType != EOAM_NOTIFY_VAR_REQ_RCVD)
    {
        return OSIX_FAILURE;
    }

    /* Allocate Interface message buffer from the pool */
    pMsg = (tCmipIntfQMsg *) MemAllocMemBlk (gCmipIntfGlobalInfo.QMsgPoolId);

    if (pMsg == NULL)
    {
        CMIF_TRC_ARG1 (BUFFER_TRC | ALL_FAILURE_TRC,
                       "CmifEoamVariableRetrieve: Port %d :"
                       " QMsg MemAllocateMemBlock FAILED\n",
                       pCallbackInfo->u4Port);
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tCmipIntfQMsg));

    pMsg->u4Port = pCallbackInfo->u4Port;
    pMsg->u1Branch = pCallbackInfo->VarRequest.u1Branch;
    pMsg->u2Leaf = pCallbackInfo->VarRequest.u2Leaf;
    pMsg->u4ReqId = pCallbackInfo->VarRequest.u4ReqId;

    if (OsixQueSend (gCmipIntfGlobalInfo.QId, (UINT1 *) &pMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CMIF_TRC_ARG1 (ALL_FAILURE_TRC,
                       "CmifEoamVariableRetrieve: Port %d :"
                       "Enqueue Message FAILED\n", pCallbackInfo->u4Port);
        if (MemReleaseMemBlock (gCmipIntfGlobalInfo.QMsgPoolId, (UINT1 *) pMsg)
            != MEM_SUCCESS)
        {

            CMIF_TRC_ARG1 (BUFFER_TRC | ALL_FAILURE_TRC,
                           "CmifEoamVariableRetrieve: Port %d:"
                           "MemReleaseMemBlock FAILED\n",
                           pCallbackInfo->u4Port);
        }
        return OSIX_FAILURE;
    }

    if (OsixEvtSend (gCmipIntfGlobalInfo.TaskId, CMIF_QMSG_EVENT) !=
        OSIX_SUCCESS)
    {
        CMIF_TRC_ARG1 (ALL_FAILURE_TRC,
                       "CmifEoamVariableRetrieve:"
                       " Port %u :SendEvent FAILED\n", pCallbackInfo->u4Port);
        /* Cannot undo enqueue.. */
    }

    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  cmifeoam.c                     */
/*-----------------------------------------------------------------------*/
