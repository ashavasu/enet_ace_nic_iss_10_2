/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cmifdefn.h,v 1.6 2010/11/18 04:51:37 siva Exp $
 *
 * Description: This file contains Macro definitions used in CMIP
 *              Interface module
 *********************************************************************/
#ifndef _CMIFDEFN_H
#define _CMIFDEFN_H

#define  CMIF_TASK_NAME         ((const UINT1 *)"CMIT")
#define  CMIF_QUEUE_NAME        "CMIQ"
#define  CMIF_SEM_NAME          "CMIF"

/* Events that will be received by CMIF Task */
#define  CMIF_QMSG_EVENT        0x02 /* Bit 1 */

#define  CMIF_QUEUE_DEPTH       (10 * EOAM_MAX_PORTS)  

/* Count on number of messages to be allocated from MemPool.
 * If the count exceeds 50, memory will be allocated from the heap. 
 * The number 50 is chosen considering the chassis where a single
 * LM has 48 ports. When all the 48 interfaces are created at a time,
 * memory can be obtained from the pool. */
#define CMIF_QMSG_COUNT               50

/* MemPool for Q messages: used for forming Q messages for
 * interfacing with CMIP Interface module. */
#define  CMIF_QMSG_MEMBLK_SIZE         sizeof(tCmipIntfQMsg) 
#define  CMIF_QMSG_MEMBLK_COUNT        CMIF_QMSG_COUNT

#define  CMIF_MAX_DATA_LEN      1024
#define  CMIF_MAX_VAR_LEN       127

#define  CMIF_BRANCH_ATTRIBUTE  0x07
#define  CMIF_BRANCH_PACKAGE    0x04
#define  CMIF_BRANCH_OBJECT     0x03

/* Error codes that should be filled in the first byte
 * of the response */
#define  CMIF_ERR               0x80
#define  CMIF_NO_ERR            0x7f
#define  CMIF_OBJ_NOT_SUPP      0x42
#define  CMIF_PKG_NOT_SUPP      0x62
#define  CMIF_ATT_NOT_SUPP      0x21
#define  CMIF_ATT_UNDETERMINED  0x20
#define  CMIF_ATT_OVERFLOW      0x24

#define  CMIF_MAX_INDICES       SNMP_MAX_INDICES 
#define  CMIF_MAX_DATA          255

/* Range of Leaf values of supported attributes defined in global array */
#define CMIF_DOT3_STAT_ATTR_START   1
#define CMIF_DOT3_STAT_ATTR_END     16
#define CMIF_OAM_ATTR1_START        236
#define CMIF_OAM_ATTR1_END          263
#define CMIF_OAM_ATTR2_START        333
#define CMIF_OAM_ATTR2_END          340

/* CMIP Leaf Id of Mib Variables with more than one Variable Mapping */
#define CMIF_OAM_FRAMES_TX_OK           2
#define CMIF_OAM_FRAMES_RX_OK           5
#define CMIF_OAM_OCTETS_TX_OK           8
#define CMIF_OAM_OCTETS_RX_OK           14
#define CMIF_OAM_REMOTE_CONFIGURATION   240
#define CMIF_OAM_LOCAL_FLAGS            242
#define CMIF_OAM_REMOTE_FLAGS           243
#define CMIF_OAM_REMOTE_STATE           245
#define CMIF_OAM_LOCAL_STATE            337

/* SNMP OID Required for Mib Variables which requires more than One SNMP Get */
#define CMIF_OAM_PEER_MODE_OID         "1.3.6.1.2.1.158.1.2.1.4"
#define CMIF_OAM_PEER_FNC_SUPP_OID     "1.3.6.1.2.1.158.1.2.1.7"
#define CMIF_IFOUT_UCAST_PKT_OID       "1.3.6.1.2.1.2.2.1.17"
#define CMIF_IFOUT_MCAST_PKT_OID       "1.3.6.1.2.1.31.1.1.1.4"
#define CMIF_IFOUT_BCAST_PKT_OID       "1.3.6.1.2.1.31.1.1.1.5"
#define CMIF_DOT3_OUT_PAUSE_FR_OID     "1.3.6.1.2.1.10.7.10.1.4"
#define CMIF_IFOUT_ERRORS_OID          "1.3.6.1.2.1.2.2.1.20"
#define CMIF_IFOUT_DISCARDS_OID        "1.3.6.1.2.1.2.2.1.19"
#define CMIF_IFIN_UCAST_PKT_OID        "1.3.6.1.2.1.2.2.1.11"
#define CMIF_IFIN_MCAST_PKT_OID        "1.3.6.1.2.1.31.1.1.1.2"
#define CMIF_IFIN_BCAST_PKT_OID        "1.3.6.1.2.1.31.1.1.1.3"
#define CMIF_DOT3_IN_PAUSE_FR_OID      "1.3.6.1.2.1.10.7.10.1.3"
#define CMIF_IFIN_DISCARDS_OID         "1.3.6.1.2.1.2.2.1.13"
#define CMIF_IFIN_UNKNOWN_PROTOS_OID   "1.3.6.1.2.1.2.2.1.15"
#define CMIF_OAM_EVENT_LOGTYPE_OID     "1.3.6.1.2.1.158.1.6.1.4"
#define CMIF_OAM_OPER_STATE_OID        "1.3.6.1.2.1.158.1.1.1.2"
#define CMIF_IFIN_OCTETS_OID           "1.3.6.1.2.1.2.2.1.10"
#define CMIF_IFOUT_OCTETS_OID          "1.3.6.1.2.1.2.2.1.16"

/* Trace and debug CMIP Interface */
#define  CMIF_FLAG            gCmipIntfGlobalInfo.u4TraceOption

/* Module names */
#define   CMIF_MOD_NAME            ((const char *)"CMIF")

/* Trace definitions */
#ifdef TRACE_WANTED

#define CMIF_TRC(TraceType, Str)                                       \
        MOD_TRC(CMIF_FLAG, TraceType, CMIF_MOD_NAME, (const char *)Str)

#define CMIF_TRC_ARG1(TraceType, Str, Arg1)                             \
        MOD_TRC_ARG1(CMIF_FLAG, TraceType, CMIF_MOD_NAME, \
                     (const char *)Str, Arg1)

#define CMIF_TRC_ARG2(TraceType, Str, Arg1, Arg2)                       \
        MOD_TRC_ARG2(CMIF_FLAG, TraceType, CMIF_MOD_NAME,\
                     (const char *)Str, Arg1, Arg2)

#define CMIF_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)                 \
        MOD_TRC_ARG3(CMIF_FLAG, TraceType, CMIF_MOD_NAME,               \
                     (const char *)Str, Arg1, Arg2, Arg3)

#define CMIF_TRC_ARG4(TraceType, Str, Arg1, Arg2, Arg3, Arg4)           \
        MOD_TRC_ARG4(CMIF_FLAG, TraceType, CMIF_MOD_NAME,               \
                     (const char *)Str, Arg1, Arg2, Arg3,Arg4)

#else  /* TRACE_WANTED */

#define CMIF_TRC(TraceType, Str)
#define CMIF_TRC_ARG1(TraceType, Str, Arg1)
#define CMIF_TRC_ARG2(TraceType, Str, Arg1, Arg2)
#define CMIF_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)
#define CMIF_TRC_ARG4(TraceType, Str, Arg1, Arg2, Arg3, Arg4)

#endif /* TRACE_WANTED */

#endif /* _CMIFDEFN_H*/
