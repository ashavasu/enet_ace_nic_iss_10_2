/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cmifinc.h,v 1.3 2010/05/19 07:12:14 prabuc Exp $
 *
 * Description: This file contains header files included in CMIP 
 *              Interface module.
 *********************************************************************/
#ifndef _CMIFINC_H
#define _CMIFINC_H

#include "lr.h"
#include "cfa.h"
#include "eoam.h"
#include "fssnmp.h"

#include "cmipif.h"

#include "cmifdefn.h"
#include "cmiftdfs.h"
#include "cmifglob.h"
#include "cmifprot.h"

#include "emdefn.h"
#include "emtdfs.h"
#endif /* _CMIFTDFS_H*/
