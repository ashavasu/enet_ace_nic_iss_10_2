/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cmifprot.h,v 1.3 2007/02/01 14:47:21 iss Exp $
 *
 * Description: This file contains function prototypes defined in
 *              CMIP Interface module
 *********************************************************************/
#ifndef _CMIFPROT_H
#define _CMIFPROT_H

/* cmifeoam.c */
/* Prototype of API to retrieve the value of a variable */
INT4  CmifEoamVariableRetrieve PROTO ((tEoamCallbackInfo *pCallbackInfo));

/* cmifmain.c */
PUBLIC INT4  CmifMainTaskInit PROTO ((VOID));
PUBLIC VOID  CmifMainHandleInitFailure PROTO ((VOID));
PUBLIC VOID  CmifMainProcessQMsg PROTO ((VOID));

/* cmifret.c */
PUBLIC VOID CmifRetrieveInit PROTO ((VOID));
PUBLIC INT4 CmifRetrieveVariable PROTO ((UINT4 ,UINT1 ,UINT2 ,UINT4 ));
#endif /* _CMIFPROT_H*/
