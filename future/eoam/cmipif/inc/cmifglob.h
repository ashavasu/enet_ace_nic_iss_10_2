/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cmifglob.h,v 1.6 2010/05/19 07:12:14 prabuc Exp $
 *
 * Description: This file contains global variables used in CMIP
 *              Interface module
 *********************************************************************/
#ifndef _CMIFGLOB_H
#define _CMIFGLOB_H

#ifdef CMIFMAIN_C
tCmipIntfGlobalInfo    gCmipIntfGlobalInfo;
#else
PUBLIC tCmipIntfGlobalInfo    gCmipIntfGlobalInfo;
#endif /* CMIFMAIN_C */

#ifdef CMIFRET_C
/* The following arrays map CMIP Leaf to Corresponding
 * SNMP MIB Object. Only the following CMIP Attributes are supported.
 * Objects and Packages are  *NOT* supported */

UINT1  gaau1Dot3StatAttr[][MAX_OID_LEN] = {
    /* SNMP Oid,             CMIP Leaf, Attribute */
    "1.3.6.1.2.1.2.2.1.1",     /* 1,  aMACID */
    "",                        /* 2,  aFramesTransmittedOK - unsupported */
    "1.3.6.1.2.1.10.7.2.1.4",  /* 3,  aSingleCollisionFrames */
    "1.3.6.1.2.1.10.7.2.1.5",  /* 4,  aMultipleCollisionFrames */
    "",                        /* 5,  aFramesReceivedOK - unsupported */
    "1.3.6.1.2.1.10.7.2.1.3",  /* 6,  aFrameCheckSequenceErrors */
    "1.3.6.1.2.1.10.7.2.1.2",  /* 7,  aAlignmentErrors */
    "",                        /* 8,  aOctetsTransmittedOK */
    "1.3.6.1.2.1.10.7.2.1.7",  /* 9,  aFramesWithDeferredXmissions */
    "1.3.6.1.2.1.10.7.2.1.8",  /* 10, aLateCollisions */
    "1.3.6.1.2.1.10.7.2.1.9",  /* 11, aFramesAbortedDueToXSColls */
    "1.3.6.1.2.1.10.7.2.1.10", /* 12, aFramesLostDueToIntMACXmitError */
    "1.3.6.1.2.1.10.7.2.1.11", /* 13, aCarrierSenseErrors */
    "",                        /* 14, aOctetsReceivedOK */
    "1.3.6.1.2.1.10.7.2.1.16", /* 15, aFramesLostDueToIntMACRcvError */
    "1.3.6.1.2.1.31.1.1.1.16"  /* 16, aPromiscuousStatus */
};

/* OAM objects */
UINT1  gaau1Dot3OAMAttr1[][MAX_OID_LEN] = {
    /* SNMP Oid,                  CMIP Leaf, Attribute */
    "1.3.6.1.2.1.2.2.1.1",          /* 236, aOAMID */
    "1.3.6.1.2.1.158.1.1.1.1", /* 237, aOAMAdminState */
    "1.3.6.1.2.1.158.1.1.1.3", /* 238, aOAMMode */
    "1.3.6.1.2.1.158.1.2.1.1", /* 239, aOAMRemoteMACAddress */
    "",                             /* 240  unsupported */
    "1.3.6.1.2.1.158.1.2.1.5", /* 241, aOAMRemotePDUConfiguration */
    "",                             /* 242  unsupported */
    "",                             /* 243  unsupported */
    "1.3.6.1.2.1.158.1.2.1.6", /* 244, aOAMRemoteRevision */
    "1.3.6.1.2.1.158.1.3.1.1", /* 245, aOAMRemoteState */
    "1.3.6.1.2.1.158.1.2.1.2", /* 246, aOAMRemoteVendorOUI */
    "1.3.6.1.2.1.158.1.2.1.3", /* 247, aOAMRemoteVendorSpecificInfo */
    "",                             /* 248  unsupported */
    "",                             /* 249  unsupported */
    "1.3.6.1.2.1.158.1.4.1.16",/* 250, aOAMUnsupportedCodesRx */
    "1.3.6.1.2.1.158.1.4.1.1", /* 251, aOAMInformationTx */
    "1.3.6.1.2.1.158.1.4.1.2", /* 252, aOAMInformationRx */
    "",                             /* 253  unsupported */
    "1.3.6.1.2.1.158.1.4.1.4", /* 254, aOAMUniqueEventNotificationRx */
    "1.3.6.1.2.1.158.1.4.1.6", /* 255, aOAMDuplicateEventNotificationRx */
    "1.3.6.1.2.1.158.1.4.1.7", /* 256, aOAMLoopbackControlTx */
    "1.3.6.1.2.1.158.1.4.1.8", /* 257, aOAMLoopbackControlRx */
    "1.3.6.1.2.1.158.1.4.1.9", /* 258, aOAMVariableRequestTx */
    "1.3.6.1.2.1.158.1.4.1.10",/* 259, aOAMVariableRequestRx */
    "1.3.6.1.2.1.158.1.4.1.11",/* 260, aOAMVariableResponseTx */
    "1.3.6.1.2.1.158.1.4.1.12",/* 261, aOAMVariableResponseRx */
    "1.3.6.1.2.1.158.1.4.1.13",/* 262, aOAMOrganizationSpecificTx */
    "1.3.6.1.2.1.158.1.4.1.14" /* 263, aOAMOrganizationSpecificRx */
};

/* OAM objects  contd. */
UINT1  gaau1Dot3OAMAttr2[][MAX_OID_LEN] = {
    /* SNMP Oid,                  CMIP Leaf, Attribute */
    "1.3.6.1.2.1.158.1.1.1.2", /* 333, aOAMDiscoveryState */
    "1.3.6.1.2.1.158.1.1.1.6", /* 334, aOAMLocalConfiguration */
    "1.3.6.1.2.1.158.1.1.1.4", /* 335, aOAMLocalPDUConfiguration */
    "1.3.6.1.2.1.158.1.1.1.5", /* 336, aOAMLocalRevision */
    "1.3.6.1.2.1.158.1.3.1.1", /* 337, aOAMLocalState */
    "1.3.6.1.2.1.158.1.4.1.15",/* 338, aOAMUnsupportedCodesTx */
    "1.3.6.1.2.1.158.1.4.1.3", /* 339, aOAMUniqueEventNotificationTx */
    "1.3.6.1.2.1.158.1.4.1.5", /* 340, aOAMDuplicateEventNotificationTx */
};

tSnmpIndex               gaCmipIndexPool [CMIF_MAX_INDICES];
tSNMP_MULTI_DATA_TYPE    gaMultiPool [CMIF_MAX_INDICES];
tSNMP_OCTET_STRING_TYPE  gaMultiOctetPool [CMIF_MAX_INDICES];
tSNMP_OID_TYPE           gaMultiOIDPool [CMIF_MAX_INDICES];
UINT1                    gaau1MultiData [CMIF_MAX_INDICES][CMIF_MAX_DATA];
#endif /* CMIFRET_C */

#endif /* _CMIFGLOB_H*/
