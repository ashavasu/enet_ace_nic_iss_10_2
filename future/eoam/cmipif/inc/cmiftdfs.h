/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cmiftdfs.h,v 1.2 2007/02/01 14:47:21 iss Exp $
 *
 * Description: This file contains Type definitions used in CMIP
 *              Interface module
 *********************************************************************/
#ifndef _CMIFTDFS_H
#define _CMIFTDFS_H

typedef enum {
	CMIF_START    = 1,
	CMIF_SHUTDOWN = 2
}eCmipIntfSysControl;

typedef struct _CmipIntfGlobalInfo
{
    tOsixTaskId       TaskId;
    tOsixQId          QId;
    tOsixSemId        SemId;
    tMemPoolId        QMsgPoolId; 
    UINT4             u4TraceOption;
    UINT1             u1SystemCtrlStatus;
    UINT1             au1Pad[3];
}tCmipIntfGlobalInfo;  

/* The structure of message passed from other modules to CMIP Interface */
typedef struct _CmipIntfQMsg
{
    UINT4 u4Port;
    UINT4 u4ReqId;
    UINT2 u2Leaf;
    UINT1 u1Branch;
    UINT1 u1Pad;
} tCmipIntfQMsg;

#endif /* _CMIFTDFS_H*/
