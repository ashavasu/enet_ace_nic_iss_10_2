/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emfminc.h,v 1.3 2012/06/01 10:48:33 siva Exp $
 *
 * Description: This file contains header files included in 
 *              FM module.
 *********************************************************************/
#ifndef _FMINC_H
#define _FMINC_H

#include <assert.h>
#include "lr.h"
#include "cfa.h"
#include "iss.h"
#include "msr.h"
#include "fsbuddy.h"
#include "fssyslog.h"
#include "eoam.h"
#include "eoamfm.h"
#include "snmctdfs.h"
#include "snmccons.h"
#include "snmputil.h"
#include "trace.h"
#include "fsvlan.h"
#include "l2iwf.h"
#include "emfmcnst.h"
#include "emfmmac.h"
#include "emfmtdfs.h"
#include "emfmglob.h"
#include "emfmext.h"
#include "emfmtrc.h"
#include "emfmpt.h"
#include "fsfmlw.h"
#ifdef SNMP_2_WANTED
#include "fsfmwr.h"
#endif

#ifdef NPAPI_WANTED
#include "fmnp.h"
#include "nputil.h"
#endif
#endif   /* _FMINC_H  */
/*-----------------------------------------------------------------------*/
/*                       End of the file  emfminc.h                        */
/*-----------------------------------------------------------------------*/

