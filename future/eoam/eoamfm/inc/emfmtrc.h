/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emfmtrc.h,v 1.1.1.1 2008/06/03 14:31:04 iss Exp $
 *
 * Description:This file contains procedures and definitions 
 *             used for debugging.
 *******************************************************************/


#ifndef _FMTRC_H_
#define _FMTRC_H_

#define  FM_TRC_FLAG  gFmGlobalInfo.u4TraceOption
#define  FM_NAME      "FM"               

#ifdef TRACE_WANTED

#define  FM_TRC(x)       FmTrcPrint( __FILE__, __LINE__, FmTrc x)

#ifdef __GNUC__
#define  FM_TRC_FN_ENTRY()  \
    if (FM_TRC_FLAG & FM_FN_ENTRY_TRC) \
    {\
        printf ("FM: %s Entered %s\n", __FILE__, __FUNCTION__);\
    }

#define  FM_TRC_FN_EXIT()   \
    if (FM_TRC_FLAG & FM_FN_EXIT_TRC) \
    {\
        printf ("FM: %s Exiting %s\n", __FILE__, __FUNCTION__);\
    }
#else /* __GNUC__ */
#define  FM_TRC_FN_ENTRY()  \
    if (FM_TRC_FLAG & FM_FN_ENTRY_TRC) \
    {\
        printf ("FM: Entered %s at Line %d\n", __FILE__, __LINE__);\
    }

#define  FM_TRC_FN_EXIT()
#endif /* __GNUC__ */

#define FM_PKT_DUMP(TraceType, pBuf, Length, Str)                       \
        MOD_PKT_DUMP(FM_TRC_FLAG, TraceType, FM_NAME, pBuf, Length,   \
                                          (const char *)Str)

#else /* TRACE_WANTED */

#define  FM_TRC(x) 
#define  FM_TRC_FN_ENTRY()
#define  FM_TRC_FN_EXIT()
#define  FM_PKT_DUMP(TraceType, pBuf, Length, Str)

#endif /* TRACE_WANTED */


#endif /* _FMTRC_H */
/*-----------------------------------------------------------------------*/
/*                       End of the file  emfmtrc.h                        */
/*-----------------------------------------------------------------------*/
