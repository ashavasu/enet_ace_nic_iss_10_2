
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emfmext.h,v 1.3 2013/11/05 10:57:17 siva Exp $
 *
 * Description: This file contains extern definitions used in FM.
 *********************************************************************/
#ifndef _FMEXTERN_H
#define _FMEXTERN_H

#ifndef FMMAIN_C
PUBLIC tFmGlobalInfo  gFmGlobalInfo;

/* Sizing Params Structure. */
extern tFsModSizingParams gFsEmFmSizingParams [];
extern tFsModSizingInfo gFsEmFmSizingInfo;
#endif /* FMMAIN_C */
#ifdef NPAPI_WANTED
extern INT4 EoamNpHandleLocalLoopBack PROTO ((UINT4, tMacAddr, tMacAddr, UINT1));
#define  EOAM_BLOCK_ENABLE         4
#define  EOAM_BLOCK_DISABLE        5
#endif
#endif /*_FMEXTERN_H*/
/*-----------------------------------------------------------------------*/
/*                       End of the file  emfmext.h                     */
/*-----------------------------------------------------------------------*/
