/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emfmcnst.h,v 1.2 2009/12/02 15:35:16 prabuc Exp $
 *
 * Description: This file contains macros definitions for constants
 *              used in FM.
 *********************************************************************/

#ifndef _FMCONST_H
#define _FMCONST_H
/* SEM related definitions */

#define FM_SEM_NAME              "FMSP"
#define FM_QUEUE_NAME            "FMTQ"
#define FM_TASK_NAME             ((const UINT1 *)"FMGT")
/* Depth of FM Msg Q: 
 * When Loopback testing is in progress, FM can receive a maximum
 * of 1000 loopbacked packets from CFA on an interface. Hence the
 * queue size is assumed as 1000. When more than one port is in
 * loopback, more than 1000 packets can be received from CFA. In
 * that case the queue size need to be increased or the maximum
 * number of packets supported in loopback test should be reduced.
 */
#define FM_QUEUE_DEPTH           1000

#define FM_MIN_PORTS             1 
#define FM_MAX_PORTS             EOAM_MAX_PORTS

/* Count on number of messages to be allocated from MemPool.
 * If the count exceeds 50, memory will be allocated from the heap. 
 * The number 50 is chosen considering the chassis where a single
 * LM has 48 ports. When all the 48 interfaces receive events at 
 * the same time, memory can be obtained from the pool. */
#define FM_QMSG_COUNT               50

/* MemPool for Q messages: used for forming Q messages for
 * interfacing with FM module. */
#define FM_QMSG_MEMBLK_SIZE         sizeof(tFmQMsg) 
#define FM_QMSG_MEMBLK_COUNT        FM_QMSG_COUNT

/* Minimum Buddy size for organization specific data and variable response */
#define FM_MIN_DATA_BLOCK_SIZE            4

/* Maximum ExtData size is the max PDU Size*/
#define FM_MAX_DATA_BLOCK_SIZE         1500

/* Max number of org specific data is assumed to be 10 * number of interfaces */
#define FM_DATA_BLOCKS           (10 * FM_MAX_PORTS)

/* Events that will be received by FM Task */
#define FM_TMR_EXPIRY_EVENT       0x01 /* Bit 0 */
#define FM_QMSG_EVENT             0x02 /* Bit 1 */
#define FM_ALL_EVENTS             (FM_TMR_EXPIRY_EVENT |\
                                   FM_QMSG_EVENT)    

/* Size occupied by SRC and DEST MAC */
#define FM_SRC_DEST_MAC_SIZE             12

/* Descriptor consists of Branch (1 byte) and Leaf (2 bytes) */
#define FM_DESCRIPTOR_LENGTH              3

/* Ascii String for descriptor */
#define FM_STR_DESCRIPTOR_LENGTH          6


/* Minimum and Maximum number of descriptors as defined in the MIB*/
#define FM_MIN_DESCRIPTORS                1
#define FM_MAX_DESCRIPTORS              100

/* Buddy requires that the minimum memory block should atleast be 4 bytes.
 * So, eventhough one descriptor is only 3 bytes long, 4 bytes are allocated */
#define FM_MIN_VAR_BLOCK_SIZE              4

/* Maximum variable response size is the max PDU Size*/
#define FM_MAX_VAR_BLOCK_SIZE           1500

/* FM is provisioned to store the latest ten variable responses */
#define FM_MAX_RESPONSES                  10
#define FM_VAR_RESP_BLOCKS  (FM_MAX_RESPONSES * FM_MAX_PORTS)
#define FM_VAR_REQ_BLOCKS  (1 * FM_MAX_PORTS)
#define FM_VAR_REQ_RESP_BLOCKS  (FM_VAR_RESP_BLOCKS + FM_VAR_REQ_BLOCKS)

/* By default memory is allocated to hold 10 descriptors */
#define FM_DEFAULT_NUM_DESCRIPTORS       10

/* A Descriptor value is taken in as an Octet-string. The Branch will always 
 * be a single character (1 byte) and the leaf can take values upto 65535 - 
 * UINT2 maximum value => 5 characters (5bytes)*/
#define FM_DESCRIPTOR_STRING_MAX_LENGTH   6
#define FM_DESCRIPTOR_STRING_MIN_LENGTH   2

/* Maximum number of characters that can be displyed by SNMP for an object*/
#define FM_VAR_RESP_MAX_CHUNK_LENGTH    256

/* Loopback Test constants*/
#define FM_DEFAULT_TEST_PKT_SIZE         64
#define FM_MAX_TEST_PKT_SIZE           1500
#define FM_LB_TEST_PATTERN_LENGTH         4
#define FM_LB_TEST_PKT_MIN_COUNT          1 
#define FM_LB_TEST_PKT_MAX_COUNT       1000
#define FM_DEFAULT_TEST_PKT_COUNT        10
#define FM_DEFAULT_TEST_PATTERN  0xf0f0f0f0
#define FM_DEFAULT_TEST_WAIT_TIME         5

/* After transmitting the test packets, FM waits for a specific time after 
 * which it finalizes the received and lost packet statistics. By default,
 * this wait time is kept as 10 seconds */
#define FM_MAX_TEST_WAIT_TIME            10 

/* Maximum Number of bytes required to hold the date information */
#define FM_MAX_DATE_LEN                  40

/* Maximum trace message length */
#define FM_MAX_LOG_STR_LEN               256

/* Invalid value */
#define FM_INVALID_VALUE                  -1

/*If the received frame includes FCS, the data packet length should 
  be obtained by subtracing 4 bytes (FCS length). This needs to 
  be taken care based on the target environment.The macro 
  FM_FCS_SIZE needs to be modified accordingly.*/

#ifdef NPAPI_WANTED
#define FM_FCS_SIZE                        4
#else
#define FM_FCS_SIZE                        0
#endif

#define FM_EOAM_REG_EVENTS              (EOAM_NOTIFY_ERRORED_SYMBOL |\
                                         EOAM_NOTIFY_ERRORED_FRAME |\
                                         EOAM_NOTIFY_ERRORED_FRAME_PERIOD |\
                                         EOAM_NOTIFY_ERRORED_FRAME_SECONDS |\
                                         EOAM_NOTIFY_ORG_SPECIFIC_EVENT |\
                                         EOAM_NOTIFY_LINK_FAULT |\
                                         EOAM_NOTIFY_DYING_GASP |\
                                         EOAM_NOTIFY_CRITICAL_EVENT |\
                                         EOAM_NOTIFY_ORG_INFO_RCVD |\
                                         EOAM_NOTIFY_VAR_RESP_RCVD |\
                                         EOAM_NOTIFY_LB_ACK_RCVD |\
                                         EOAM_NOTIFY_LB_ACK_NOT_RCVD |\
                                         EOAM_NOTIFY_LB_TESTDATA_RCVD |\
                                         EOAM_NOTIFY_PORT_CREATE | \
                                         EOAM_NOTIFY_PORT_DELETE)
#endif /*_FMCONST_H*/
/*-----------------------------------------------------------------------*/
/*                       End of the file  emfmcnst.h                      */
/*-----------------------------------------------------------------------*/
