  /********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emfmcmd.def,v 1.7 2015/05/13 11:02:17 siva Exp $
 *
 * Description: This file contains system CLI commands.
 *
 *******************************************************************/

/******************************EMFM COMMANDS**********************************/

/*****************************************************************************/
/*                         EMFM GLOBALCFG COMMANDS                           */
/*****************************************************************************/

DEFINE GROUP : FM_GLBCFG_CMDS

   COMMAND : no shutdown fault-management
   ACTION  :
             {
                 cli_process_fm_cmd (CliHandle, FM_CLI_SYS_CTRL, NULL, 
                                     FM_START);
             }
   SYNTAX  : no shutdown fault-management
   PRVID   : 15
   HELP    : Start fault management on the system.
  CXT_HELP : no Disables the configuration / deletes the entry / resets to default value|
             shutdown Shuts down the feature|
             fault-management Fault management related configuration|
             <CR> Start fault management on the system.

   COMMAND : shutdown fault-management
   ACTION  :
             {
                 cli_process_fm_cmd (CliHandle, FM_CLI_SYS_CTRL, NULL, 
                                     FM_SHUTDOWN);
             }
   SYNTAX  : shutdown fault-management
   PRVID   : 15
   HELP    : Shutdown fault management on the system.
  CXT_HELP : shutdown Shuts down the feature|
             fault-management Fault management related configuration|
             <CR> Shutdown fault management on the system.

   COMMAND : set fault-management {enable | disable}
   ACTION  :
             {
                if($2 != NULL)
                {
                    cli_process_fm_cmd (CliHandle, FM_CLI_MOD_STAT, NULL, 
                                        FM_ENABLED);
                }
                else
                {
                    cli_process_fm_cmd (CliHandle, FM_CLI_MOD_STAT, NULL, 
                                        FM_DISABLED);
                }
             }
   SYNTAX  : set fault-management {enable | disable}
   PRVID   : 15
   HELP    : Enable/Disable fault management on the system.
  CXT_HELP : set Configures the parameter|
             fault-management Fault management related configuration|
             enable Enables fault management|
             disable Disable fault management|
             <CR> Enable/Disable fault management on the system.

END GROUP

/*****************************************************************************/
/*                        EMFM PEXCFG COMMANDS                               */
/*****************************************************************************/

DEFINE GROUP : FM_PEXCFG_CMDS
   
   COMMAND : debug fault-management [all] [init] [mgmt] [critical] [pkt] [failure] [buffer] [resource] [loopback] [event-trig] [event-rx] [var-reqresp] [ctrl] [func-entry] [func-exit]
   ACTION  :
             {
                UINT4 u4Debug = 0;
                if($3 != NULL)
                {
                  u4Debug |= INIT_SHUT_TRC; 
                }
                if($4 != NULL)
                {
                  u4Debug |= MGMT_TRC; 
                }
                if($5 != NULL)
                {
                  u4Debug |= FM_CRITICAL_TRC; 
                }
                if($6 != NULL)
                {
                  u4Debug |= DUMP_TRC; 
                }
                if($7 != NULL)
                {
                  u4Debug |= ALL_FAILURE_TRC; 
                }
                if($8 != NULL)
                {
                  u4Debug |= BUFFER_TRC; 
                }
                if($9 != NULL)
                {
                  u4Debug |= OS_RESOURCE_TRC; 
                }
                if($10 != NULL)
                {
                  u4Debug |= FM_LOOPBACK_TRC; 
                }
                if($11 != NULL)
                {
                  u4Debug |= FM_EVENTTRIG_TRC; 
                }
                if($12 != NULL)
                {
                  u4Debug |= FM_EVENTRX_TRC; 
                }
                if($13 != NULL)
                {
                  u4Debug |= FM_VAR_REQRESP_TRC; 
                }
                if($14 != NULL)
                {
                  u4Debug |= CONTROL_PLANE_TRC; 
                }
                if($15 != NULL)
                {
                   u4Debug |= FM_FN_ENTRY_TRC;
                }
                if($16 != NULL)
                {
                   u4Debug |= FM_FN_EXIT_TRC;
                }
                if($2 != NULL)
                {
                  u4Debug = FM_ALL_TRC; 
                }
             
                if(u4Debug == 0)
                {
                   cli_process_fm_cmd (CliHandle, FM_CLI_DEBUG_SHOW, NULL, 
                                       u4Debug);
                }
                else
                {
                   cli_process_fm_cmd (CliHandle, FM_CLI_DEBUG, NULL, u4Debug);
                }
             }
   SYNTAX  : debug fault-management [all] [init] [mgmt] [critical] [pkt] [failure] [buffer] [resource] [loopback] [event-trig] [event-rx] [var-reqresp] [ctrl] [func-entry] [func-exit]
   PRVID   : 15
   HELP    : Specify debug level for FM module. When no arguments are given, displays current debug level.
  CXT_HELP : debug Configures trace for the protocol|
             fault-management Fault management related configuration|
             all All traces|
             init Init and shutdown traces|
             mgmt Management traces|
             critical Critical traces|
             pkt Packet dump traces|
             failure All failure traces|
             buffer Buffer allocation and release traces|
             resource Traces related to all resources except buffer|
             loopback FM remote loopback traces|
             event-trig Event transmission traces|
             event-rx Event reception traces|
             var-reqresp Variable requestresponse traces|
             ctrl Control plane traces|
             func-entry Function entry traces|
             func-exit Function exit traces|
             <CR> Specify debug level for FM module. When no arguments are given, displays current debug level.

   COMMAND : no debug fault-management all
   ACTION  :
             {
                UINT4 u4Debug = 0;
                  u4Debug = FM_ALL_TRC; 
                u4Debug = ~u4Debug;
                cli_process_fm_cmd (CliHandle, FM_CLI_NO_DEBUG, NULL, u4Debug);
                }
   SYNTAX  : no debug fault-management all
   PRVID   : 15
   HELP    : Disable debug option for FM module.
   CXT_HELP: no Disables the configuration / deletes the entry / resets to default value|
             debug Debug traces|
             fault-management Fault management related configuration|
             all All traces|
             <CR> Disable debug option for FM module.

   COMMAND : no debug fault-management ([init] [mgmt] [critical] [pkt] [failure] [buffer] [resource] [loopback] [event-trig] [event-rx] [var-reqresp] [ctrl] [func-entry] [func-exit])
   ACTION  :
                {
                    UINT4 u4Debug = 0;
                    if($3 != NULL)
                    {
                        u4Debug |= INIT_SHUT_TRC; 
                    }
                    if($4 != NULL)
                    {
                        u4Debug |= MGMT_TRC; 
                    }
                    if($5 != NULL)
                    {
                        u4Debug |= FM_CRITICAL_TRC; 
                    }
                    if($6 != NULL)
                    {
                        u4Debug |= DUMP_TRC; 
                    }
                    if($7 != NULL)
                    {
                        u4Debug |= ALL_FAILURE_TRC; 
                    }
                    if($8 != NULL)
                    {
                        u4Debug |= BUFFER_TRC; 
                    }
                    if($9 != NULL)
                    {
                        u4Debug |= OS_RESOURCE_TRC; 
                    }
                    if($10 != NULL)
                    {
                        u4Debug |= FM_LOOPBACK_TRC; 
                    }
                    if($11 != NULL)
                    {
                        u4Debug |= FM_EVENTTRIG_TRC; 
                    }
                    if($12 != NULL)
                    {
                        u4Debug |= FM_EVENTRX_TRC; 
                    }
                    if($13 != NULL)
                    {
                        u4Debug |= FM_VAR_REQRESP_TRC; 
                    }
                    if($14 != NULL)
                    {
                        u4Debug |= CONTROL_PLANE_TRC;
                    }
                    if($15 != NULL)
                    {
                        u4Debug |= FM_FN_ENTRY_TRC;
                    }
                    if($16 != NULL)
                    {
                        u4Debug |= FM_FN_EXIT_TRC;
                    }
                
                u4Debug = ~u4Debug;
                cli_process_fm_cmd (CliHandle, FM_CLI_NO_DEBUG, NULL, u4Debug);
             }
   SYNTAX  : no debug fault-management ([init] [mgmt] [critical] [pkt] [failure] [buffer] [resource] [loopback] [event-trig] [event-rx] [var-reqresp] [ctrl] [func-entry] [func-exit])
   PRVID   : 15
   HELP    : Disable debug option for FM module.
  CXT_HELP : no Disables the configuration / deletes the entry / resets to default value|
             debug Debug traces|
             fault-management Fault management related configuration|
             init Init and shutdown traces|
             mgmt Management traces|
             critical Critical traces|
             pkt Packet dump traces|
             failure All failure traces|
             buffer Buffer allocation and release traces|
             resource Traces related to all resources except buffer|
             loopback FM remote loopback traces|
             event-trig Event transmission traces|
             event-rx Event reception traces|
             var-reqresp Variable requestresponse traces|
             ctrl Control plane traces|
             func-entry Function entry traces|
             func-exit Function exit traces|
             <CR>  Disable debug option for FM module.

   COMMAND : clear port fault-management ethernet-oam [<iftype> <ifnum>] mib-variable response
   ACTION  :
             {
                 UINT4 u4IfIndex = 0;
                 if (($4 != NULL) && ($5 != NULL))
                 {
                     if (CfaCliGetIfIndex ($4, $5, &u4IfIndex) == CLI_FAILURE)
                     {
                         CliPrintf (CliHandle, "\r%% Invalid Interface \r\n");
                         return CLI_FAILURE;
                     }
                 }
                 cli_process_fm_cmd (CliHandle, FM_CLI_CLEAR_MIB_RESP, 
                                     u4IfIndex);
             }
   SYNTAX  :  clear port fault-management ethernet-oam [<interface-type> <interface-id>] mib-variable response
   PRVID   : 15
   HELP    : Clearing MIB variable response received from the peer.
  CXT_HELP : clear Performs clear operation|
             port Port related configuration|
             fault-management Fault management related configuration|
             ethernet-oam Ethernet oam related configuration|
             DYNiftype|
             DYNifnum|
             mib-variable Mib variable related configuration|
             response Mib variable response related configuration|
             <CR> Clearing mib variable response received from the peer.

END GROUP

/*****************************************************************************/
/*                       EMFM SHOW COMMANDS                                  */
/*****************************************************************************/

DEFINE GROUP : FM_SHOW_CMDS

   COMMAND : show fault-management global information
   ACTION  :
             {
                cli_process_fm_cmd (CliHandle, FM_CLI_SHOW_GLOBAL_INFO);
             }
   SYNTAX  : show fault-management global information
   PRVID   : 1
   HELP    : Displaying fault-management global information.
  CXT_HELP : show Displays the configuration / statistics / general information|
             fault-management Fault management related configuration|
             global Global configuration|
             information Fault management global information|
             <CR> Displaying fault-management global information.

   COMMAND : show port fault-management ethernet-oam [<iftype> <ifnum>] mib-variable response
   ACTION  :
             {
                 UINT4 u4IfIndex = 0;
                 if (($4 != NULL) && ($5 != NULL))
                 {
                     if (CfaCliGetIfIndex ($4, $5, &u4IfIndex) == CLI_FAILURE)
                     {
                         CliPrintf (CliHandle, "\r%% Invalid Interface \r\n");
                         return CLI_FAILURE;
                     }
                 }
                 cli_process_fm_cmd (CliHandle, FM_CLI_SHOW_MIB_RESP, 
                                     u4IfIndex);
             }
   SYNTAX  :  show port fault-management ethernet-oam [<interface-type> <interface-id>] mib-variable response
   PRVID   : 1
   HELP    : Displaying MIB variable response.
  CXT_HELP : show Displays the configuration / statistics / general information|
             port Port related configuration|
             fault-management Fault management related configuration|
             ethernet-oam Ethernet oam related configuration|
             DYNiftype|
             DYNifnum|
             mib-variable Mib variable related configuration|
             response Mib variable response related configuration|
             <CR> Displaying mib variable response.


   COMMAND : show port fault-management ethernet-oam [<iftype> <ifnum>] 
   ACTION  :
             {
                 UINT4 u4IfIndex = 0;
                 if (($4 != NULL) && ($5 != NULL))
                 {
                     if (CfaCliGetIfIndex ($4, $5, &u4IfIndex) == CLI_FAILURE)
                     {
                         CliPrintf (CliHandle, "\r%% Invalid Interface \r\n");
                         return CLI_FAILURE;
                     }
                 }
                 cli_process_fm_cmd (CliHandle, FM_CLI_SHOW_PORT_CONFIG, 
                                     u4IfIndex);
             }
   SYNTAX  :  show port fault-management ethernet-oam [<interface-type> <interface-id>]
   PRVID   : 1
   HELP    : Displaying EOAM link event actions and max descriptors per variable request.
  CXT_HELP : show Displays the configuration / statistics / general information|
             port Port related configuration|
             fault-management Fault management related configuration|
             ethernet-oam Ethernet oam related configuration|
             DYNiftype|
             DYNifnum|
             <CR> Displaying eoam link event actions and max descriptors per variable request.

   COMMAND : show port fault-management ethernet-oam [<iftype> <ifnum>] remote-loopback {current-session | last-session} [detail]
   ACTION  :
             {
                 UINT4 u4IfIndex = 0;
                 if (($4 != NULL) && ($5 != NULL))
                 {
                     if (CfaCliGetIfIndex ($4, $5, &u4IfIndex) == CLI_FAILURE)
                     {
                         CliPrintf (CliHandle, "\r%% Invalid Interface \r\n");
                         return CLI_FAILURE;
                     }
                 }
                 if($7 != NULL)
                 {
                    if($9 != NULL)
                    {
                       cli_process_fm_cmd (CliHandle, FM_CLI_SHOW_LB_CUR_DETAIL,
                                           u4IfIndex);
                    }
                    else
                    {
                       cli_process_fm_cmd (CliHandle, FM_CLI_SHOW_LB_CUR, 
                                           u4IfIndex);
                    }
                 }
                 else
                 {
                    if($9 != NULL)
                    {
                       cli_process_fm_cmd (CliHandle, 
                                           FM_CLI_SHOW_LB_LAST_DETAIL, 
                                           u4IfIndex);
                    }
                    else
                    {
                       cli_process_fm_cmd (CliHandle, FM_CLI_SHOW_LB_LAST, 
                                           u4IfIndex);
                    }
                 }
             }

   SYNTAX  :  show port fault-management ethernet-oam [<interface-type> <interface-id>] remote-loopback {current-session | last-session} [detail]
   PRVID   : 1
   HELP    : Displaying Ethernet OAM loopback statistics.
  CXT_HELP : show Displays the configuration / statistics / general information|
             port Port related configuration|
             fault-management Fault management related configuration|
             ethernet-oam Ethernet oam related configuration|
             DYNiftype|
             DYNifnum|
             remote-loopback Ethernet oam remote loopback test related configuration|
             current-session Current remote loopback session related information|
             last-session Last remote loopback session related information|
             detail Detailed information of current and last session|
             <CR> Displaying Ethernet oam loopback statistics.




END GROUP

/*****************************************************************************/
/*                       EMFM INTFCFG COMMANDS                               */
/*****************************************************************************/

DEFINE GROUP : FM_INTFCFG_CMDS
   
   COMMAND : fault-management ethernet-oam remote-loopback ([test] [count <integer(1-1000)>] [packet <integer(64-1500)>] [pattern <hex_str>] [wait-time <integer(1-10)>])
   ACTION  :
             {
                 UINT4  u4TestPktCnt = 0;
                 UINT4  u4TestPktSize = 0;
                 INT4   i4TestWaitTime = 0;
                 INT4   i4TestCommand = FALSE;
                 
                 if ($3 != NULL)
                 {
                    i4TestCommand = TRUE;
                 }
                     
                 if ($5 != NULL)
                 {
                     u4TestPktCnt = *(UINT4 *)($5);
                 }
      
                 if ($7 != NULL)
                 {
                     u4TestPktSize = *(UINT4 *)($7);
                 }
      
                 /* Check for the test pattern length. */
                 /* Must not exceed the maximum string length */
                 if ($9 != NULL)
                 {
                     if (CLI_STRLEN ($9) > FM_CLI_TST_PTRN_STR_LEN)
                     {
                         CliPrintf(CliHandle, 
                         "\r%% pattern<hex_string(9)>\r\n");
                         return CLI_FAILURE;
                     }
                 }
                if ($11 != NULL)
                {
                    i4TestWaitTime = *(INT4 *)($11);
                }
          
                cli_process_fm_cmd (CliHandle, FM_CLI_LB_TEST, NULL, 
                                    u4TestPktCnt, u4TestPktSize, $9, 
                                    i4TestWaitTime, i4TestCommand);
             }
   SYNTAX  : fault-management ethernet-oam remote-loopback ([test] [count <no of packets(1-1000)>] [packet <size(64-1500)>] [pattern <hex_string(8)>] [wait-time <integer(1-10)>])
   PRVID   : 15
   HELP    : Specifying number of packets and packet size for Ethernet OAM loopback test. This command also triggers the loop back test.
  CXT_HELP : fault-management Configures fault management related information|
             ethernet-oam Ethernet oam related configuration|
             remote-loopback Ethernet oam remote loopback test related configuration|
             test Initiates remote loopback test process|
             count Number of Test Packets to be generated|
             (1-1000) Number of packets|
             packet Size of the test packets to be generated|
             (64-1500) Test packet size|
             pattern Loopback test data pattern|
             <hex_str> Test pattern|
             wait-time FM module wait time for the reception of loopback test data|
             (1-10) Wait time value|
             <CR> Specifying number of packets and packet size for Ethernet oam loopback test. This command also triggers the loop back test.





   COMMAND : fault-management ethernet-oam link-monitor {symbol-period | frame | frame-period | frame-sec-summary} action {none | warning}
   ACTION  :
             {
                if($3 != NULL)
                {
                   if($8 != NULL)
                   {
                    cli_process_fm_cmd (CliHandle, FM_CLI_LM_SYM, NULL, 
                                        FM_ACTION_NONE);
                   }
                   else
                   {
                    cli_process_fm_cmd (CliHandle, FM_CLI_LM_SYM, NULL, 
                                        FM_ACTION_WARNING);
                   }
                }
                else if($4 != NULL)
                {
                   if($8 != NULL)
                   {
                    cli_process_fm_cmd (CliHandle, FM_CLI_LM_FRAME, NULL, 
                                        FM_ACTION_NONE);
                   }
                   else
                   {
                    cli_process_fm_cmd (CliHandle, FM_CLI_LM_FRAME, NULL, 
                                        FM_ACTION_WARNING);
                   }
                }
                else if($5 != NULL)
                {
                   if($8 != NULL)
                   {
                    cli_process_fm_cmd (CliHandle, FM_CLI_LM_FRAME_PERIOD, 
                                        NULL, FM_ACTION_NONE);
                   }
                   else
                   {
                    cli_process_fm_cmd (CliHandle, FM_CLI_LM_FRAME_PERIOD, 
                                        NULL, FM_ACTION_WARNING);
                   }
                }
                else
                {
                   if($8 != NULL)
                   {
                    cli_process_fm_cmd (CliHandle, FM_CLI_LM_FRAME_SEC_SUM, 
                                        NULL, FM_ACTION_NONE);
                   }
                   else
                   {
                    cli_process_fm_cmd (CliHandle, FM_CLI_LM_FRAME_SEC_SUM, 
                                        NULL, FM_ACTION_WARNING);
                   }
                }
             }
   SYNTAX  : fault-management ethernet-oam link-monitor {symbol-period | frame | frame-period | frame-sec-summary} action {none | warning}
   PRVID   : 15
   HELP    : Specifying the action for the Link monitoring threshold crossing events received from local.
  CXT_HELP : fault-management Configures fault management related information|
             ethernet-oam Ethernet oam related configuration|
             link-monitor Link monitoring threshold related configuration|
             symbol-period Symbol period event notification|
             frame Frame event notification|
             frame-period Frame-period event notification|
             frame-sec-summary Frame-seconds summary event notification|
             action Action to be taken when a particular event is received|
             none No action to be taken when a particular event is received|
             warning Syslog warning message will be generated when an event is received|
             <CR> Specifying the action for the link monitoring threshold crossing events received from local.


   COMMAND : fault-management ethernet-oam {critical-event | dying-gasp | link-fault} action {none | warning | block }
   ACTION  :
             {
                if($2 != NULL)
                {
                   if($6 != NULL)
                   {
                    cli_process_fm_cmd (CliHandle, FM_CLI_CRITICAL_EVENT, 
                                        NULL, FM_ACTION_NONE);
                   }
                   else if($7 != NULL)
                   {
                    cli_process_fm_cmd (CliHandle, FM_CLI_CRITICAL_EVENT, 
                                        NULL, FM_ACTION_WARNING);
                   }
                   else 
                   {
                       cli_process_fm_cmd (CliHandle, FM_CLI_CRITICAL_EVENT,
                               NULL, FM_ACTION_BLOCK);
                   }
                }
                else if($3 != NULL)
                {
                   if($6 != NULL)
                   {
                    cli_process_fm_cmd (CliHandle, FM_CLI_DYING_GASP, 
                                        NULL, FM_ACTION_NONE);
                   }
                   else if($7 != NULL)
                   {
                    cli_process_fm_cmd (CliHandle, FM_CLI_DYING_GASP, 
                                        NULL, FM_ACTION_WARNING);
                   }
                   else 
                   {
                       cli_process_fm_cmd (CliHandle, FM_CLI_DYING_GASP,
                               NULL, FM_ACTION_BLOCK);
                   }
                }
                else
                {
                   if($6 != NULL)
                   {
                    cli_process_fm_cmd (CliHandle, FM_CLI_LINK_FAULT, 
                                        NULL, FM_ACTION_NONE);
                   }
                   else if($7 != NULL)
                   {
                    cli_process_fm_cmd (CliHandle, FM_CLI_LINK_FAULT, 
                                        NULL, FM_ACTION_WARNING);
                   }
                   else 
                   {
                       cli_process_fm_cmd (CliHandle, FM_CLI_LINK_FAULT,
                               NULL, FM_ACTION_BLOCK);
                   }
                }
             }
   SYNTAX  : fault-management ethernet-oam {critical-event | dying-gasp | link-fault} action {none | warning | block }
   PRVID   : 15
   HELP    : Specifying the action for the critical events received from local.
  CXT_HELP : fault-management Configures fault management related information|
             ethernet-oam Ethernet oam related configuration|
             critical-event Critical event fault indication related information|
             dying-gasp Dying gasp fault indication related information|
             link-fault Link-fault indication related information|
             action Action to be taken when a particular event is received|
             none No action will be taken when an event is received|
             warning Syslog warning message will be generated when an event is received|
             block Drop all traffic except OAM pdu when an event is received|
             <CR> Specifying the action for the critical events received from local.

   COMMAND : fault-management ethernet-oam mib-variable count <integer(1-100)>
   ACTION  :
             {
                cli_process_fm_cmd (CliHandle, FM_CLI_VAR_REQ_COUNT, NULL, $4);
             }
   SYNTAX  : fault-management ethernet-oam mib-variable count <count(1-100)>
   PRVID   : 15
   HELP    : Sets the maximum MIB variables that be sent in one OAM variable request pdu.
  CXT_HELP : fault-management Configures fault management related information|
             ethernet-oam Ethernet oam related configuration|
             mib-variable Maximum mib variable related information|
             count Mib variable count related information|
             (1-100) Mib variable count|
             <CR> Sets the maximum MIB variables that be sent in one OAM variable request pdu.



   COMMAND : set fault-management ethernet-oam mib-request <string>
   ACTION  :
             {
                cli_process_fm_cmd (CliHandle, FM_CLI_VAR_REQ, NULL, $4);
             }
   SYNTAX  : set fault-management ethernet-oam mib-request <branchleaf:branchleaf:...>
   PRVID   : 15
   HELP    : Send MIB variable request request to peer.
  CXT_HELP : set Configures the parameter|
             fault-management Fault management related configuration|
             ethernet-oam Ethernet oam related configuration|
             mib-request Mib variable request related configuration|
             <string> Branch leaf|
             <CR> Send mib variable request to peer.

END GROUP

/*---------------------------------------------------------------------------*/
/*                         End of the file emfmcmd.def                       */
/*---------------------------------------------------------------------------*/
