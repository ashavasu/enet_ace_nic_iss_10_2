/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emfmmac.h,v 1.3 2011/07/13 13:03:17 siva Exp $
 *
 * Description: This file contains macro definitions used in
 *              FM module
 *********************************************************************/
#ifndef _FMMACRO_H
#define _FMMACRO_H

#define FM_IF_ENTRY(u4IfIndex) gFmGlobalInfo.aFmPortEntry[u4IfIndex - 1]

#define  FM_OFFSET(x,y)  (&(((x *)0)->y))

#define FM_RESP_BUDDY_ALLOC(n)  \
               MemBuddyAlloc ((UINT1) gFmGlobalInfo.i4VarRetrievalBuddyId, n)
#define FM_RESP_BUDDY_FREE(p)   \
               MemBuddyFree ((UINT1) gFmGlobalInfo.i4VarRetrievalBuddyId, p)

#define FM_EXT_BUDDY_ALLOC(n)  \
               MemBuddyAlloc ((UINT1) gFmGlobalInfo.i4ExtDataBuddyId, n)
#define FM_EXT_BUDDY_FREE(p)   \
               MemBuddyFree ((UINT1) gFmGlobalInfo.i4ExtDataBuddyId, p)

#define FM_PUT_4BYTE(pu1Buf, pu1Val)                  \
        {                                             \
          *((UINT4 *)(VOID *) pu1Buf) =               \
            OSIX_HTONL(*((UINT4 *) (VOID *) pu1Val)); \
           pu1Buf += 4;                               \
        }
#define FM_GET_4BYTE(u4Val, pu1Buf)                   \
        {                                             \
           MEMCPY (&u4Val, pu1Buf, 4);                \
           u4Val = (UINT4) (OSIX_NTOHL(u4Val));       \
           pu1Buf += 4;                               \
        }

#define FM_IS_VALID_DESCRIPTOR(u1Branch, u2Leaf) \
         ((((u1Branch != EOAM_ATTRIBUTE) && (u1Branch != EOAM_PACKAGE) &&  \
          (u1Branch != EOAM_OBJECT)) || (u2Leaf < 1))? \
          OSIX_FALSE : OSIX_TRUE)

#define FM_CALCULATE_VAR_RESP_CHUNKS(u4Length, u2Chunks)  \
{                                                         \
    u2Chunks = (UINT2) (u4Length / FM_VAR_RESP_MAX_CHUNK_LENGTH); \
    if (u4Length % FM_VAR_RESP_MAX_CHUNK_LENGTH)          \
        u2Chunks ++;                                      \
}
#endif /*_FMMACRO_H*/
/*-----------------------------------------------------------------------*/
/*                       End of the file  emfmmac.h                      */
/*-----------------------------------------------------------------------*/
