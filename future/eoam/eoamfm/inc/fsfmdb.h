/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsfmdb.h,v 1.3 2008/08/20 14:16:08 premap-iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSFMDB_H
#define _FSFMDB_H

UINT1 FsFmLinkEventTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsFmLoopbackTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsFmLBStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsFmVarRetrievalTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsFmVarResponseTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fsfm [] ={1,3,6,1,4,1,2076,122};
tSNMP_OID_TYPE fsfmOID = {8, fsfm};


UINT4 FsFmSystemControl [ ] ={1,3,6,1,4,1,2076,122,1,1};
UINT4 FsFmModuleStatus [ ] ={1,3,6,1,4,1,2076,122,1,2};
UINT4 FsFmTraceOption [ ] ={1,3,6,1,4,1,2076,122,1,3};
UINT4 FsFmSymPeriodAction [ ] ={1,3,6,1,4,1,2076,122,2,1,1,1};
UINT4 FsFmFrameAction [ ] ={1,3,6,1,4,1,2076,122,2,1,1,2};
UINT4 FsFmFramePeriodAction [ ] ={1,3,6,1,4,1,2076,122,2,1,1,3};
UINT4 FsFmFrameSecSummAction [ ] ={1,3,6,1,4,1,2076,122,2,1,1,4};
UINT4 FsFmCriticalEventAction [ ] ={1,3,6,1,4,1,2076,122,2,1,1,5};
UINT4 FsFmDyingGaspAction [ ] ={1,3,6,1,4,1,2076,122,2,1,1,6};
UINT4 FsFmLinkFaultAction [ ] ={1,3,6,1,4,1,2076,122,2,1,1,7};
UINT4 FsFmLoopbackStatus [ ] ={1,3,6,1,4,1,2076,122,3,1,1,1};
UINT4 FsFmLBTestPattern [ ] ={1,3,6,1,4,1,2076,122,3,1,1,2};
UINT4 FsFmLBTestPktSize [ ] ={1,3,6,1,4,1,2076,122,3,1,1,3};
UINT4 FsFmLBTestCount [ ] ={1,3,6,1,4,1,2076,122,3,1,1,4};
UINT4 FsFmLBTestWaitTime [ ] ={1,3,6,1,4,1,2076,122,3,1,1,5};
UINT4 FsFmLBTestCommand [ ] ={1,3,6,1,4,1,2076,122,3,1,1,6};
UINT4 FsFmLBTestStatus [ ] ={1,3,6,1,4,1,2076,122,3,1,1,7};
UINT4 FsFmLBTestStartTimestamp [ ] ={1,3,6,1,4,1,2076,122,3,1,1,8};
UINT4 FsFmLBTestEndTimestamp [ ] ={1,3,6,1,4,1,2076,122,3,1,1,9};
UINT4 FsFmLBTestTxCount [ ] ={1,3,6,1,4,1,2076,122,3,1,1,10};
UINT4 FsFmLBTestRxCount [ ] ={1,3,6,1,4,1,2076,122,3,1,1,11};
UINT4 FsFmLBTestMatchCount [ ] ={1,3,6,1,4,1,2076,122,3,1,1,12};
UINT4 FsFmLBStatsStartTimestamp [ ] ={1,3,6,1,4,1,2076,122,3,2,1,1};
UINT4 FsFmLBStatsEndTimestamp [ ] ={1,3,6,1,4,1,2076,122,3,2,1,2};
UINT4 FsFmLBStatsTxCount [ ] ={1,3,6,1,4,1,2076,122,3,2,1,3};
UINT4 FsFmLBStatsRxCount [ ] ={1,3,6,1,4,1,2076,122,3,2,1,4};
UINT4 FsFmLBStatsMatchCount [ ] ={1,3,6,1,4,1,2076,122,3,2,1,5};
UINT4 FsFmVarRetrievalMaxVar [ ] ={1,3,6,1,4,1,2076,122,4,1,1,1};
UINT4 FsFmVarRetrievalRequest [ ] ={1,3,6,1,4,1,2076,122,4,1,1,2};
UINT4 FsFmVarRetrievalClearResponse [ ] ={1,3,6,1,4,1,2076,122,4,1,1,3};
UINT4 FsFmVarResponseId [ ] ={1,3,6,1,4,1,2076,122,4,3,1,1};
UINT4 FsFmVarResponseRx1 [ ] ={1,3,6,1,4,1,2076,122,4,3,1,2};
UINT4 FsFmVarResponseRx2 [ ] ={1,3,6,1,4,1,2076,122,4,3,1,3};
UINT4 FsFmVarResponseRx3 [ ] ={1,3,6,1,4,1,2076,122,4,3,1,4};
UINT4 FsFmVarResponseRx4 [ ] ={1,3,6,1,4,1,2076,122,4,3,1,5};
UINT4 FsFmVarResponseRx5 [ ] ={1,3,6,1,4,1,2076,122,4,3,1,6};
UINT4 FsFmVarResponseRx6 [ ] ={1,3,6,1,4,1,2076,122,4,3,1,7};


tMbDbEntry fsfmMibEntry[]= {

{{10,FsFmSystemControl}, NULL, FsFmSystemControlGet, FsFmSystemControlSet, FsFmSystemControlTest, FsFmSystemControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsFmModuleStatus}, NULL, FsFmModuleStatusGet, FsFmModuleStatusSet, FsFmModuleStatusTest, FsFmModuleStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsFmTraceOption}, NULL, FsFmTraceOptionGet, FsFmTraceOptionSet, FsFmTraceOptionTest, FsFmTraceOptionDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "262144"},

{{12,FsFmSymPeriodAction}, GetNextIndexFsFmLinkEventTable, FsFmSymPeriodActionGet, FsFmSymPeriodActionSet, FsFmSymPeriodActionTest, FsFmLinkEventTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsFmLinkEventTableINDEX, 1, 0, 0, "2"},

{{12,FsFmFrameAction}, GetNextIndexFsFmLinkEventTable, FsFmFrameActionGet, FsFmFrameActionSet, FsFmFrameActionTest, FsFmLinkEventTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsFmLinkEventTableINDEX, 1, 0, 0, "2"},

{{12,FsFmFramePeriodAction}, GetNextIndexFsFmLinkEventTable, FsFmFramePeriodActionGet, FsFmFramePeriodActionSet, FsFmFramePeriodActionTest, FsFmLinkEventTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsFmLinkEventTableINDEX, 1, 0, 0, "2"},

{{12,FsFmFrameSecSummAction}, GetNextIndexFsFmLinkEventTable, FsFmFrameSecSummActionGet, FsFmFrameSecSummActionSet, FsFmFrameSecSummActionTest, FsFmLinkEventTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsFmLinkEventTableINDEX, 1, 0, 0, "2"},

{{12,FsFmCriticalEventAction}, GetNextIndexFsFmLinkEventTable, FsFmCriticalEventActionGet, FsFmCriticalEventActionSet, FsFmCriticalEventActionTest, FsFmLinkEventTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsFmLinkEventTableINDEX, 1, 0, 0, "2"},

{{12,FsFmDyingGaspAction}, GetNextIndexFsFmLinkEventTable, FsFmDyingGaspActionGet, FsFmDyingGaspActionSet, FsFmDyingGaspActionTest, FsFmLinkEventTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsFmLinkEventTableINDEX, 1, 0, 0, "2"},

{{12,FsFmLinkFaultAction}, GetNextIndexFsFmLinkEventTable, FsFmLinkFaultActionGet, FsFmLinkFaultActionSet, FsFmLinkFaultActionTest, FsFmLinkEventTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsFmLinkEventTableINDEX, 1, 0, 0, "2"},

{{12,FsFmLoopbackStatus}, GetNextIndexFsFmLoopbackTable, FsFmLoopbackStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsFmLoopbackTableINDEX, 1, 0, 0, "3"},

{{12,FsFmLBTestPattern}, GetNextIndexFsFmLoopbackTable, FsFmLBTestPatternGet, FsFmLBTestPatternSet, FsFmLBTestPatternTest, FsFmLoopbackTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsFmLoopbackTableINDEX, 1, 0, 0, NULL},

{{12,FsFmLBTestPktSize}, GetNextIndexFsFmLoopbackTable, FsFmLBTestPktSizeGet, FsFmLBTestPktSizeSet, FsFmLBTestPktSizeTest, FsFmLoopbackTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsFmLoopbackTableINDEX, 1, 0, 0, "64"},

{{12,FsFmLBTestCount}, GetNextIndexFsFmLoopbackTable, FsFmLBTestCountGet, FsFmLBTestCountSet, FsFmLBTestCountTest, FsFmLoopbackTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsFmLoopbackTableINDEX, 1, 0, 0, "10"},

{{12,FsFmLBTestWaitTime}, GetNextIndexFsFmLoopbackTable, FsFmLBTestWaitTimeGet, FsFmLBTestWaitTimeSet, FsFmLBTestWaitTimeTest, FsFmLoopbackTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsFmLoopbackTableINDEX, 1, 0, 0, "5"},

{{12,FsFmLBTestCommand}, GetNextIndexFsFmLoopbackTable, FsFmLBTestCommandGet, FsFmLBTestCommandSet, FsFmLBTestCommandTest, FsFmLoopbackTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsFmLoopbackTableINDEX, 1, 0, 0, NULL},

{{12,FsFmLBTestStatus}, GetNextIndexFsFmLoopbackTable, FsFmLBTestStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsFmLoopbackTableINDEX, 1, 0, 0, NULL},

{{12,FsFmLBTestStartTimestamp}, GetNextIndexFsFmLoopbackTable, FsFmLBTestStartTimestampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsFmLoopbackTableINDEX, 1, 0, 0, NULL},

{{12,FsFmLBTestEndTimestamp}, GetNextIndexFsFmLoopbackTable, FsFmLBTestEndTimestampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsFmLoopbackTableINDEX, 1, 0, 0, NULL},

{{12,FsFmLBTestTxCount}, GetNextIndexFsFmLoopbackTable, FsFmLBTestTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsFmLoopbackTableINDEX, 1, 0, 0, NULL},

{{12,FsFmLBTestRxCount}, GetNextIndexFsFmLoopbackTable, FsFmLBTestRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsFmLoopbackTableINDEX, 1, 0, 0, NULL},

{{12,FsFmLBTestMatchCount}, GetNextIndexFsFmLoopbackTable, FsFmLBTestMatchCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsFmLoopbackTableINDEX, 1, 0, 0, NULL},

{{12,FsFmLBStatsStartTimestamp}, GetNextIndexFsFmLBStatsTable, FsFmLBStatsStartTimestampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsFmLBStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsFmLBStatsEndTimestamp}, GetNextIndexFsFmLBStatsTable, FsFmLBStatsEndTimestampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsFmLBStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsFmLBStatsTxCount}, GetNextIndexFsFmLBStatsTable, FsFmLBStatsTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsFmLBStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsFmLBStatsRxCount}, GetNextIndexFsFmLBStatsTable, FsFmLBStatsRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsFmLBStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsFmLBStatsMatchCount}, GetNextIndexFsFmLBStatsTable, FsFmLBStatsMatchCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsFmLBStatsTableINDEX, 1, 0, 0, NULL},

{{12,FsFmVarRetrievalMaxVar}, GetNextIndexFsFmVarRetrievalTable, FsFmVarRetrievalMaxVarGet, FsFmVarRetrievalMaxVarSet, FsFmVarRetrievalMaxVarTest, FsFmVarRetrievalTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsFmVarRetrievalTableINDEX, 1, 0, 0, "10"},

{{12,FsFmVarRetrievalRequest}, GetNextIndexFsFmVarRetrievalTable, FsFmVarRetrievalRequestGet, FsFmVarRetrievalRequestSet, FsFmVarRetrievalRequestTest, FsFmVarRetrievalTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsFmVarRetrievalTableINDEX, 1, 0, 0, NULL},

{{12,FsFmVarRetrievalClearResponse}, GetNextIndexFsFmVarRetrievalTable, FsFmVarRetrievalClearResponseGet, FsFmVarRetrievalClearResponseSet, FsFmVarRetrievalClearResponseTest, FsFmVarRetrievalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsFmVarRetrievalTableINDEX, 1, 0, 0, NULL},

{{12,FsFmVarResponseId}, GetNextIndexFsFmVarResponseTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsFmVarResponseTableINDEX, 2, 0, 0, NULL},

{{12,FsFmVarResponseRx1}, GetNextIndexFsFmVarResponseTable, FsFmVarResponseRx1Get, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsFmVarResponseTableINDEX, 2, 0, 0, NULL},

{{12,FsFmVarResponseRx2}, GetNextIndexFsFmVarResponseTable, FsFmVarResponseRx2Get, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsFmVarResponseTableINDEX, 2, 0, 0, NULL},

{{12,FsFmVarResponseRx3}, GetNextIndexFsFmVarResponseTable, FsFmVarResponseRx3Get, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsFmVarResponseTableINDEX, 2, 0, 0, NULL},

{{12,FsFmVarResponseRx4}, GetNextIndexFsFmVarResponseTable, FsFmVarResponseRx4Get, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsFmVarResponseTableINDEX, 2, 0, 0, NULL},

{{12,FsFmVarResponseRx5}, GetNextIndexFsFmVarResponseTable, FsFmVarResponseRx5Get, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsFmVarResponseTableINDEX, 2, 0, 0, NULL},

{{12,FsFmVarResponseRx6}, GetNextIndexFsFmVarResponseTable, FsFmVarResponseRx6Get, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsFmVarResponseTableINDEX, 2, 0, 0, NULL},
};
tMibData fsfmEntry = { 37, fsfmMibEntry };
#endif /* _FSFMDB_H */

