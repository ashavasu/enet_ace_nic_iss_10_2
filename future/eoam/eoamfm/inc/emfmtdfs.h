/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emfmtdfs.h,v 1.3 2010/05/19 07:12:17 prabuc Exp $
 *
 * Description: This file contains data structures defined for
 *              FM module.
 *********************************************************************/

#ifndef _FMTDFS_H
#define _FMTDFS_H


/* Timer Types */
typedef enum
{
FM_LB_TEST_TMR_TYPE = 0,
FM_MAX_TIMERS
} eFmTimerTypes;

typedef enum
{
FM_RESP_NOT_CLEARED = 1,
FM_RESP_CLEAR
} eFmRespClearStatus;

typedef enum
{
FM_NO_LB_TEST = 1,
FM_START_LB_TEST
} eFmCommand;

typedef enum
{
    FM_NOT_INITIATED = 1,
    FM_IN_PROGRESS,
    FM_COMPLETED
} eFMLBTestStatus;

/* Loopback Status */
enum
{
    FM_NO_LB = 1,
    FM_REMOTE_LB,
    FM_UNKNOWN
};

/* To indicate Loopback timer status */
typedef enum
{
    FM_LB_TMR_NOT_RUNNING = 0,
    FM_LB_TMR_RUNNING = 1
} eFmLBTmrFlag;

/* To check for the valid entry in FM */
typedef enum
{
    FM_INVALID_ENTRY = 0,
    FM_VALID_ENTRY = 1
}eFmValidEntry;

/* Message types received by FM module */
typedef enum
{
    FM_EOAM_MSG = 1
} eFmMsgType;

typedef struct FmEventInfo
{
    eFmEventAction SymPeriodAction;     
    eFmEventAction FrameAction;     
    eFmEventAction FramePeriodAction;     
    eFmEventAction FrameSecSummAction;     
    eFmEventAction CriticalEventAction;     
    eFmEventAction DyingGaspAction;     
    eFmEventAction LinkFaultAction;     
} tFmEventInfo;

typedef struct FMLBTestStats
{
    tOsixSysTime              StartTime;  
    tOsixSysTime              EndTime;  
    UINT4                     u4TxPktCount;
    UINT4                     u4RxPktCount;
    UINT4                     u4MatchPktCount;
} tFmLBTestStats;

typedef struct FmLoopbackTestInfo
{
    tFmLBTestStats  CurrSessionStats;
    UINT4           u4TestPktSize;
    UINT4           u4TestPktCount;      
    UINT4           u4TestWaitTime;      
    UINT1           au1TestPattern[FM_LB_TEST_PATTERN_LENGTH];
    UINT1           u1LoopbackStatus;
    UINT1           u1TestCommand;
    UINT1           u1TestStatus;
    UINT1           u1Pad;
} tFmLoopbackTestInfo;

typedef struct FmVarRetrievalInfo
{
    eFmRespClearStatus  RespClearStatus;
    UINT2               u2MaxDescriptors;
    UINT2               u2DescriptorsFilled;
    UINT1              *pu1VarReqData;
} tFmVarRetrievalInfo;

typedef struct FmPortInfo
{
    tTmrBlk               LBTestAppTmr;
    tFmEventInfo          EventInfo;
    tFmLoopbackTestInfo   LBTestInfo;
    tFmLBTestStats        PrevSessionStats;
    tFmVarRetrievalInfo   VarRetrievalInfo;  
    tEoamExtData          VarResp[FM_MAX_RESPONSES];
    eFmLBTmrFlag          FmLBTmrFlag;
    eFmValidEntry         FmValidEntry;
    UINT4                 u4Port;
    UINT2                 au2VarRespChunks[FM_MAX_RESPONSES];
    UINT2                 u2Position;
    UINT1                 u1EventFlag;
    UINT1                 au1Pad[1];
}tFmPortEntry;

typedef struct FmGlobalInfo
{
    tFmPortEntry       aFmPortEntry[FM_MAX_PORTS];  /* Portwise Info */
    tTimerListId       FmTmrListId;
    tOsixTaskId        TaskId;
    tOsixQId           QId;
    tOsixSemId         FmSemId;
    tMemPoolId         QMsgPoolId;            /* Q Msg Mempool Id */
    tTmrDesc           aFmTmrDesc[FM_MAX_TIMERS]; 
    UINT4              u4TraceOption;
    UINT4              u4SysLogId;
    INT4               i4VarRetrievalBuddyId; /* Buddy Id for storing Variable
                                                 response data in MIB response
                                                 table */
    INT4               i4ExtDataBuddyId;      /* Buddy Id to copy and post
                                                incoming Variable response
                                                and Org spec data to FM task */
    UINT1              u1SystemCtrlStatus;
    UINT1              u1ModuleStatus;
    UINT1              au1Pad[2];
} tFmGlobalInfo;

/* The structure of message passed from other modules to FM. */
typedef struct
{
    UINT4 u4MsgType;                     /* Message type */
    UINT4 u4Port;                        /* Port number */
    UINT4 u4EventType;                   /* Type of Event*/ 
    union
    {
        UINT1               u1State;     /* State denoting 'ENABLE'
                                            or 'DISABLE'. Used for Dying gasp,
                                            Link fault, critical events*/ 
        tEoamThresEventInfo LMEvents;    /* Contains structure of threshold 
                                            crossing events' information*/ 
        tEoamExtData        EoamExtData; /* contains a data pointer used for 
                                            Org Specific data */
        tCRU_BUF_CHAIN_HEADER *pBuf;     /* Contains pointer to loopback test
                                            data */
    }uFmEventInfo;
#define FmEventState     uFmEventInfo.u1State
#define FmThresEventInfo uFmEventInfo.LMEvents    
#define FmExtData        uFmEventInfo.EoamExtData
#define FmLBTestData     uFmEventInfo.pBuf

    UINT2 u2Location;                    /* EOAM_LOCAL_ENTRY or 
                                            EOAM_REMOTE_ENTRY */
    UINT1 au1Pad[2];
}tFmQMsg;


/* Sizing Params enum */
enum
{
    FM_Q_MSG_SIZING_ID,
    FM_EXT_BUDDY_SIZING_ID,
    FM_VAR_RETR_BUDDY_SIZING_ID,
    FM_NO_SIZING_ID
};
#endif /*FMTDFS_H*/
/*-----------------------------------------------------------------------*/
/*                       End of the file  emfmtdfs.h                      */
/*-----------------------------------------------------------------------*/
