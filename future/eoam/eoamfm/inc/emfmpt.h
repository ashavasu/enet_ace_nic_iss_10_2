/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emfmpt.h,v 1.1.1.1 2008/06/03 14:31:04 iss Exp $
 * 
 * Description: This file contains prototypes for functions
 *              defined in FM.
 *******************************************************************/
#ifndef _FMPROTO_H
#define _FMPROTO_H

/* fmmain.c */
PUBLIC INT4 FmMainStart PROTO ((VOID));
PUBLIC INT4 FmMainShutDown PROTO ((VOID));
PUBLIC INT4 FmMainEnable PROTO ((VOID));
PUBLIC INT4 FmMainDisable PROTO ((VOID));

/* fmque.c */
PUBLIC VOID FmQueMsgHandler PROTO ((VOID));
PUBLIC INT4 FmQueEnqAppMsg PROTO ((tFmQMsg *));

/* fmif.c */
PUBLIC INT4 FmIfCreateAllPorts PROTO ((VOID));
PUBLIC INT4 FmIfDelete PROTO ((UINT4));
PUBLIC INT4 FmIfCreate PROTO ((UINT4));
PUBLIC VOID FmIfClearInfo PROTO ((UINT4));

/* fmfunc.c */
PUBLIC INT4 FmFuncHandleEventFromEoam PROTO ((tFmQMsg *));
PUBLIC INT4 FmFuncPerformLoopbackTest PROTO ((tFmPortEntry *));
/* Loopback test data validation function */
VOID FmFuncHandleIncomingPacket PROTO((tCRU_BUF_CHAIN_HEADER *pBuf,
                                       UINT4 u4PortIndex));

/* fmutil.c */
PUBLIC INT4 FmUtilExtractDescriptor PROTO ((UINT1 **, UINT1 *, UINT2 *));
PUBLIC INT4 FmUtilCopyDescriptorsFromStr PROTO ((UINT1 *, UINT1 *));
PUBLIC INT4 FmUtilGetDescriptorLength PROTO ((UINT1 **, UINT4 *));
PUBLIC UINT4 FmUtilGetDescriptorCntFromStr PROTO ((UINT1 *pu1String));

/* fmtimer.c */
PUBLIC INT4 FmTimerInit PROTO ((VOID));
PUBLIC INT4 FmTimerDeInit PROTO ((VOID));
PUBLIC VOID FmTimerExpHandler PROTO ((VOID));

/* fmtrc.c */
CHR1  *FmTrc       PROTO(( UINT4 u4Trc,  const char *fmt, ...));
VOID   FmTrcPrint  PROTO(( const CHR1 *fname, UINT4 u4Line, CHR1 *s));

#endif /*_FMPROTO_H*/
/*-----------------------------------------------------------------------*/
/*                       End of the file  emfmpt.h                      */
/*-----------------------------------------------------------------------*/
