/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsfmwr.h,v 1.3 2008/08/20 14:16:08 premap-iss Exp $
*
* Description: Proto types for wrapper routines
*********************************************************************/
#ifndef _FSFMWR_H
#define _FSFMWR_H

VOID RegisterFSFM(VOID);
INT4 FsFmSystemControlGet(tSnmpIndex *, tRetVal *);
INT4 FsFmModuleStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsFmTraceOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsFmSystemControlSet(tSnmpIndex *, tRetVal *);
INT4 FsFmModuleStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsFmTraceOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsFmSystemControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsFmModuleStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsFmTraceOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsFmSystemControlDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsFmModuleStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsFmTraceOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);



INT4 GetNextIndexFsFmLinkEventTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsFmSymPeriodActionGet(tSnmpIndex *, tRetVal *);
INT4 FsFmFrameActionGet(tSnmpIndex *, tRetVal *);
INT4 FsFmFramePeriodActionGet(tSnmpIndex *, tRetVal *);
INT4 FsFmFrameSecSummActionGet(tSnmpIndex *, tRetVal *);
INT4 FsFmCriticalEventActionGet(tSnmpIndex *, tRetVal *);
INT4 FsFmDyingGaspActionGet(tSnmpIndex *, tRetVal *);
INT4 FsFmLinkFaultActionGet(tSnmpIndex *, tRetVal *);
INT4 FsFmSymPeriodActionSet(tSnmpIndex *, tRetVal *);
INT4 FsFmFrameActionSet(tSnmpIndex *, tRetVal *);
INT4 FsFmFramePeriodActionSet(tSnmpIndex *, tRetVal *);
INT4 FsFmFrameSecSummActionSet(tSnmpIndex *, tRetVal *);
INT4 FsFmCriticalEventActionSet(tSnmpIndex *, tRetVal *);
INT4 FsFmDyingGaspActionSet(tSnmpIndex *, tRetVal *);
INT4 FsFmLinkFaultActionSet(tSnmpIndex *, tRetVal *);
INT4 FsFmSymPeriodActionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsFmFrameActionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsFmFramePeriodActionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsFmFrameSecSummActionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsFmCriticalEventActionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsFmDyingGaspActionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsFmLinkFaultActionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsFmLinkEventTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);







INT4 GetNextIndexFsFmLoopbackTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsFmLoopbackStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsFmLBTestPatternGet(tSnmpIndex *, tRetVal *);
INT4 FsFmLBTestPktSizeGet(tSnmpIndex *, tRetVal *);
INT4 FsFmLBTestCountGet(tSnmpIndex *, tRetVal *);
INT4 FsFmLBTestWaitTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsFmLBTestCommandGet(tSnmpIndex *, tRetVal *);
INT4 FsFmLBTestStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsFmLBTestStartTimestampGet(tSnmpIndex *, tRetVal *);
INT4 FsFmLBTestEndTimestampGet(tSnmpIndex *, tRetVal *);
INT4 FsFmLBTestTxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsFmLBTestRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsFmLBTestMatchCountGet(tSnmpIndex *, tRetVal *);
INT4 FsFmLBTestPatternSet(tSnmpIndex *, tRetVal *);
INT4 FsFmLBTestPktSizeSet(tSnmpIndex *, tRetVal *);
INT4 FsFmLBTestCountSet(tSnmpIndex *, tRetVal *);
INT4 FsFmLBTestWaitTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsFmLBTestCommandSet(tSnmpIndex *, tRetVal *);
INT4 FsFmLBTestPatternTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsFmLBTestPktSizeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsFmLBTestCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsFmLBTestWaitTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsFmLBTestCommandTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsFmLoopbackTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);





INT4 GetNextIndexFsFmLBStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsFmLBStatsStartTimestampGet(tSnmpIndex *, tRetVal *);
INT4 FsFmLBStatsEndTimestampGet(tSnmpIndex *, tRetVal *);
INT4 FsFmLBStatsTxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsFmLBStatsRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsFmLBStatsMatchCountGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsFmVarRetrievalTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsFmVarRetrievalMaxVarGet(tSnmpIndex *, tRetVal *);
INT4 FsFmVarRetrievalRequestGet(tSnmpIndex *, tRetVal *);
INT4 FsFmVarRetrievalClearResponseGet(tSnmpIndex *, tRetVal *);
INT4 FsFmVarRetrievalMaxVarSet(tSnmpIndex *, tRetVal *);
INT4 FsFmVarRetrievalRequestSet(tSnmpIndex *, tRetVal *);
INT4 FsFmVarRetrievalClearResponseSet(tSnmpIndex *, tRetVal *);
INT4 FsFmVarRetrievalMaxVarTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsFmVarRetrievalRequestTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsFmVarRetrievalClearResponseTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsFmVarRetrievalTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 GetNextIndexFsFmVarResponseTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsFmVarResponseRx1Get(tSnmpIndex *, tRetVal *);
INT4 FsFmVarResponseRx2Get(tSnmpIndex *, tRetVal *);
INT4 FsFmVarResponseRx3Get(tSnmpIndex *, tRetVal *);
INT4 FsFmVarResponseRx4Get(tSnmpIndex *, tRetVal *);
INT4 FsFmVarResponseRx5Get(tSnmpIndex *, tRetVal *);
INT4 FsFmVarResponseRx6Get(tSnmpIndex *, tRetVal *);
#endif /* _FSFMWR_H */
