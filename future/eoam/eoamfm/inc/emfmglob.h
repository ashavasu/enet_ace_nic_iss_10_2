/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emfmglob.h,v 1.2 2009/12/02 15:35:16 prabuc Exp $
 *
 * Description: This file contains global variables used in FM.
 *********************************************************************/
#ifndef _FMGLOB_H
#define _FMGLOB_H

#ifdef FMMAIN_C
tFmGlobalInfo  gFmGlobalInfo;

/* Sizing Params Structure. */
tFsModSizingParams gFsEmFmSizingParams [] =
{
    {"FM_QMSG_MEMBLK_SIZE", "FM_QMSG_MEMBLK_COUNT", FM_QMSG_MEMBLK_SIZE,
     FM_QMSG_MEMBLK_COUNT, FM_QMSG_MEMBLK_COUNT, 0},
  
    {"FM_MAX_DATA_BLOCK_SIZE", "FM_DATA_BLOCKS", FM_MAX_DATA_BLOCK_SIZE,
     FM_DATA_BLOCKS, FM_DATA_BLOCKS, 0},

    {"FM_MAX_VAR_BLOCK_SIZE", "FM_VAR_REQ_RESP_BLOCKS", FM_MAX_VAR_BLOCK_SIZE,
     FM_VAR_REQ_RESP_BLOCKS, FM_VAR_REQ_RESP_BLOCKS, 0},

    {"0", "0", 0,0,0,0}
};

tFsModSizingInfo gFsEmFmSizingInfo;


#endif /* FMMAIN_C */ 

#ifdef FMFUNC_C
UINT1   gau1LinearBuf[FM_MAX_TEST_PKT_SIZE]; 
#endif /* FMFUNC_C */

#endif /*_FMGLOB_H*/
/*-----------------------------------------------------------------------*/
/*                       End of the file  emfmglob.h                       */
/*-----------------------------------------------------------------------*/
