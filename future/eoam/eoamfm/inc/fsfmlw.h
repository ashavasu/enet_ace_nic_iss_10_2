/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsfmlw.h,v 1.3 2008/08/20 14:16:08 premap-iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsFmSystemControl ARG_LIST((INT4 *));

INT1
nmhGetFsFmModuleStatus ARG_LIST((INT4 *));

INT1
nmhGetFsFmTraceOption ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsFmSystemControl ARG_LIST((INT4 ));

INT1
nmhSetFsFmModuleStatus ARG_LIST((INT4 ));

INT1
nmhSetFsFmTraceOption ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsFmSystemControl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsFmModuleStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsFmTraceOption ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsFmSystemControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsFmModuleStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsFmTraceOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsFmLinkEventTable. */
INT1
nmhValidateIndexInstanceFsFmLinkEventTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsFmLinkEventTable  */

INT1
nmhGetFirstIndexFsFmLinkEventTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsFmLinkEventTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsFmSymPeriodAction ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsFmFrameAction ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsFmFramePeriodAction ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsFmFrameSecSummAction ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsFmCriticalEventAction ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsFmDyingGaspAction ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsFmLinkFaultAction ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsFmSymPeriodAction ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsFmFrameAction ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsFmFramePeriodAction ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsFmFrameSecSummAction ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsFmCriticalEventAction ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsFmDyingGaspAction ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsFmLinkFaultAction ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsFmSymPeriodAction ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsFmFrameAction ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsFmFramePeriodAction ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsFmFrameSecSummAction ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsFmCriticalEventAction ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsFmDyingGaspAction ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsFmLinkFaultAction ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsFmLinkEventTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsFmLoopbackTable. */
INT1
nmhValidateIndexInstanceFsFmLoopbackTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsFmLoopbackTable  */

INT1
nmhGetFirstIndexFsFmLoopbackTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsFmLoopbackTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsFmLoopbackStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsFmLBTestPattern ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsFmLBTestPktSize ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsFmLBTestCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsFmLBTestWaitTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsFmLBTestCommand ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsFmLBTestStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsFmLBTestStartTimestamp ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsFmLBTestEndTimestamp ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsFmLBTestTxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsFmLBTestRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsFmLBTestMatchCount ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsFmLBTestPattern ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsFmLBTestPktSize ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsFmLBTestCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsFmLBTestWaitTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsFmLBTestCommand ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsFmLBTestPattern ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsFmLBTestPktSize ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsFmLBTestCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsFmLBTestWaitTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsFmLBTestCommand ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsFmLoopbackTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsFmLBStatsTable. */
INT1
nmhValidateIndexInstanceFsFmLBStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsFmLBStatsTable  */

INT1
nmhGetFirstIndexFsFmLBStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsFmLBStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsFmLBStatsStartTimestamp ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsFmLBStatsEndTimestamp ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsFmLBStatsTxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsFmLBStatsRxCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsFmLBStatsMatchCount ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for FsFmVarRetrievalTable. */
INT1
nmhValidateIndexInstanceFsFmVarRetrievalTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsFmVarRetrievalTable  */

INT1
nmhGetFirstIndexFsFmVarRetrievalTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsFmVarRetrievalTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsFmVarRetrievalMaxVar ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsFmVarRetrievalRequest ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsFmVarRetrievalClearResponse ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsFmVarRetrievalMaxVar ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsFmVarRetrievalRequest ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsFmVarRetrievalClearResponse ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsFmVarRetrievalMaxVar ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsFmVarRetrievalRequest ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsFmVarRetrievalClearResponse ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsFmVarRetrievalTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsFmVarResponseTable. */
INT1
nmhValidateIndexInstanceFsFmVarResponseTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsFmVarResponseTable  */

INT1
nmhGetFirstIndexFsFmVarResponseTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsFmVarResponseTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsFmVarResponseRx1 ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsFmVarResponseRx2 ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsFmVarResponseRx3 ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsFmVarResponseRx4 ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsFmVarResponseRx5 ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsFmVarResponseRx6 ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
