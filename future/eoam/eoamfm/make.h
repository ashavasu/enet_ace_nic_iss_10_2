#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                     |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 10 May 2006                                   |
# |                                                                          |
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################


TOTAL_OPNS =  $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

CFA_BASE_DIR    = ${BASE_DIR}/cfa2
CFA_INCD        = ${CFA_BASE_DIR}/inc

FM_BASE_DIR      = ${BASE_DIR}/fm
EOAM_FM_BASE_DIR = ${BASE_DIR}/eoam/eoamfm
EOAM_FM_SRC_DIR  = ${EOAM_FM_BASE_DIR}/src
EOAM_FM_INC_DIR  = ${EOAM_FM_BASE_DIR}/inc
FM_INC_DIR       = ${FM_BASE_DIR}/inc
EOAM_FM_OBJ_DIR  = ${EOAM_FM_BASE_DIR}/obj
EOAM_INC_DIR     = ${BASE_DIR}/eoam/inc

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${EOAM_FM_INC_DIR} -I${FM_INC_DIR} -I${CFA_INCD} 
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################
