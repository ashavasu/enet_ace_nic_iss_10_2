/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emfmtmr.c,v 1.2 2011/07/13 13:03:19 siva Exp $
 * 
 * Description: This file contains all the timer related functions of Fault
 *              Management. 
 *****************************************************************************/
#include "emfminc.h"

PRIVATE VOID FmTimerInitTmrDesc PROTO ((VOID));
PRIVATE VOID FmTimerLBTestFunc PROTO ((VOID *pArg));

/***************************************************************************
 *     FUNCTION NAME    :  FmTimerInit 
 *                                                                           
 *     DESCRIPTION      :  This function creates a new timer list and stores
 *                         the returned Timer ID in the global structure.
 *                                                                           
 *     INPUT            :  None. 
 *                                                                           
 *     OUTPUT           :  None.                                             
 *                                                                           
 *     RETURNS          :  OSIX_SUCCESS / OSIX_FAILURE 
 ****************************************************************************/
INT4
FmTimerInit (VOID)
{
    FM_TRC_FN_ENTRY ();
    /* Creating the FM Timer List and storing the Timer List Id in the FM global
     * structure */

    if (TmrCreateTimerList (FM_TASK_NAME, FM_TMR_EXPIRY_EVENT, NULL,
                            &(gFmGlobalInfo.FmTmrListId)) != TMR_SUCCESS)
    {
        FM_TRC ((INIT_SHUT_TRC | OS_RESOURCE_TRC | CONTROL_PLANE_TRC |
                 ALL_FAILURE_TRC, "TMR: Timer List Creation FAILED\n"));
        return OSIX_FAILURE;
    }

    FmTimerInitTmrDesc ();
    FM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : FmTimerInitTmrDesc 
 *                                                                          
 *    DESCRIPTION      : This function intializes the timer desc for all 
 *                       the timers in FM module.  
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
FmTimerInitTmrDesc (VOID)
{
    FM_TRC_FN_ENTRY ();
    gFmGlobalInfo.aFmTmrDesc[FM_LB_TEST_TMR_TYPE].i2Offset =
        PTR_TO_I4 (FM_OFFSET (tFmPortEntry, LBTestAppTmr));
    gFmGlobalInfo.aFmTmrDesc[FM_LB_TEST_TMR_TYPE].TmrExpFn = FmTimerLBTestFunc;

    FM_TRC_FN_EXIT ();

    return;
}

/***************************************************************************
 *     FUNCTION NAME    :  FmTimerDeInit 
 *                                                                           
 *     DESCRIPTION      :  Thid function stops all running timers and deletes 
 *                         the created timer lists for the module. 
 *                                                                           
 *     INPUT            :  None. 
 *                                                                           
 *     OUTPUT           :  None.                                             
 *                                                                           
 *     RETURNS          :  OSIX_SUCCESS / OSIX_FAILURE 
 ****************************************************************************/
INT4
FmTimerDeInit (VOID)
{
    FM_TRC_FN_ENTRY ();
    if (gFmGlobalInfo.FmTmrListId != 0)
    {
        /* Deleting the Timer List */
        if (TmrDeleteTimerList (gFmGlobalInfo.FmTmrListId) != TMR_SUCCESS)
        {
            FM_TRC ((INIT_SHUT_TRC | OS_RESOURCE_TRC | CONTROL_PLANE_TRC |
                     ALL_FAILURE_TRC, "TMR: Timer List Deletion FAILED\n"));
            return OSIX_FAILURE;
        }
    }
    FM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*******************************************************************************
 *     FUNCTION NAME    :  FmTimerExpHandler
 *                                                                           
 *     DESCRIPTION      :  This function is called whenever a timer expiry 
 *                         message is received by the FM task.
 *                                                                           
 *     INPUT            :  None
 *                                                                           
 *     OUTPUT           :  None.                                             
 *                                                                           
 *     RETURNS          :  None. 
*******************************************************************************/
VOID
FmTimerExpHandler (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    UINT1               u1TimerId = 0;
    INT2                i2Offset;

    FM_TRC_FN_ENTRY ();

    if (gFmGlobalInfo.u1ModuleStatus == FM_DISABLED)
    {
        FM_TRC ((OS_RESOURCE_TRC, "FmTimerExpHandler: FM module status"
                 " disabled\n"));
        return;
    }

    FM_TRC ((OS_RESOURCE_TRC, "FmTimerExpHandler: Timer expiry to be"
             " handled\n"));

    while ((pExpiredTimers =
            TmrGetNextExpiredTimer (gFmGlobalInfo.FmTmrListId)) != NULL)
    {
        FM_TRC ((OS_RESOURCE_TRC, "Timer to be processed %x\n",
                 pExpiredTimers));

        u1TimerId = ((tTmrBlk *) pExpiredTimers)->u1TimerId;
        i2Offset = gFmGlobalInfo.aFmTmrDesc[u1TimerId].i2Offset;

        FM_TRC ((OS_RESOURCE_TRC, "TimerId: %d Offset: %d\n",
                 u1TimerId, i2Offset));

        if (i2Offset == FM_INVALID_VALUE)
        {
            /* The timer function does not take any parameter. */
            (*(gFmGlobalInfo.aFmTmrDesc[u1TimerId].TmrExpFn)) (NULL);
        }
        else
        {
            (*(gFmGlobalInfo.aFmTmrDesc[u1TimerId].TmrExpFn))
                ((UINT1 *) pExpiredTimers - i2Offset);
        }
    }                            /* End of while */

    FM_TRC_FN_EXIT ();

    return;
}

/*******************************************************************************
 *     FUNCTION NAME    :  FmTimerLBTestFunc 
 *                                                                           
 *     DESCRIPTION      :  This function is called when the loopback test timer
 *                         expires.
 *                                                                           
 *     INPUT            :  pArg - pointer to the port entry struct whose
 *                         timer has expired.
 *                                                                           
 *     OUTPUT           :  None.                                             
 *                                                                           
 *     RETURNS          :  None. 
*******************************************************************************/
PRIVATE VOID
FmTimerLBTestFunc (VOID *pArg)
{
    tFmPortEntry       *pPortEntry = NULL;
    FM_TRC_FN_ENTRY ();

    /* This function accesses FM port entry. Hence the 
     * lock is taken before accessing FM port entry */

    pPortEntry = (tFmPortEntry *) pArg;
    pPortEntry->LBTestInfo.u1TestStatus = FM_COMPLETED;
    pPortEntry->FmLBTmrFlag = FM_LB_TMR_NOT_RUNNING;
    FM_TRC ((CONTROL_PLANE_TRC,
             "FmTimerLBTestFunc :Port %u Timer expired\n", pPortEntry->u4Port));

    FM_TRC_FN_EXIT ();
    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  emfmtmr.c                     */
/*-----------------------------------------------------------------------*/
