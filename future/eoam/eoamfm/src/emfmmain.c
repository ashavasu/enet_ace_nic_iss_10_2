/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emfmmain.c,v 1.5 2012/08/08 10:02:27 siva Exp $
 * 
 * Description: This file contains all the system functions of Fault
 *              Management. 
 *****************************************************************************/
#ifndef FMMAIN_C
#define FMMAIN_C
#include "emfminc.h"

PRIVATE INT4 FmMainTaskInit PROTO ((VOID));
PRIVATE VOID FmMainHandleInitFailure PROTO ((VOID));
PRIVATE VOID FmMainMemClear PROTO ((VOID));
PRIVATE INT4 FmMainMemInit PROTO ((VOID));

/***************************************************************************
 *     FUNCTION NAME    :  FmMainTask
 *                                                                           
 *     DESCRIPTION      :  This function creates the protocol semaphore.
 *                                                                           
 *     INPUT            :  pArg - Pointer to the arguments 
 *                                                                           
 *     OUTPUT           :  None.                                             
 *                                                                           
 *     RETURNS          :  OSIX_SUCCESS / OSIX_FAILURE                       
 ****************************************************************************/
VOID
FmMainTask (INT1 *pArg)
{
    UINT4               u4Events = 0;

    FM_TRC_FN_ENTRY ();
    UNUSED_PARAM (pArg);

    MEMSET (&gFmGlobalInfo, 0, sizeof (tFmGlobalInfo));
    gFmGlobalInfo.u1SystemCtrlStatus = FM_SHUTDOWN;
    gFmGlobalInfo.u1ModuleStatus = FM_DISABLED;

    if (FmMainTaskInit () == OSIX_FAILURE)
    {
        FM_TRC ((FM_CRITICAL_TRC, "!!!!! FM TASK INIT FAILURE !!!!!"));
        FmMainHandleInitFailure ();

        /* Indicate the status of initialization to main routine */
        lrInitComplete (OSIX_FAILURE);
        return;
    }
    /* Indicate the status of initialization to main routine */
    lrInitComplete (OSIX_SUCCESS);
#ifdef SNMP_2_WANTED
    /* Register the FM protocol MIB with SNMP */
    RegisterFSFM ();
#endif /*SNMP_2_WANTED */

    while (1)
    {
        if (OsixEvtRecv (gFmGlobalInfo.TaskId, FM_ALL_EVENTS,
                         OSIX_WAIT, &u4Events) == OSIX_SUCCESS)
        {
            /* Mutual exclusion flag ON */
            FmLock ();

            FM_TRC ((CONTROL_PLANE_TRC, "Rx Event %d\n", u4Events));

            if (u4Events & FM_QMSG_EVENT)
            {
                FM_TRC ((CONTROL_PLANE_TRC, "Rx MsgQ If Event\n"));
                FmQueMsgHandler ();
            }

            if (u4Events & FM_TMR_EXPIRY_EVENT)
            {
                FM_TRC ((CONTROL_PLANE_TRC, "Rx Tmr Exp Event\n"));
                FmTimerExpHandler ();
            }

            /* Mutual exclusion flag OFF */
            FmUnLock ();
        }
    }                            /* end of while */
}

/***************************************************************************
 *     FUNCTION NAME    :  FmMainTaskInit 
 *                                                                           
 *     DESCRIPTION      : This function creates the task queue, allocates 
  *                       MemPools for task queue messages and creates
 *                        protocol semaphore. 
 *                                                                           
 *     INPUT            :  None. 
 *                                                                           
 *     OUTPUT           :  None.                                             
 *                                                                           
 *     RETURNS          :  OSIX_SUCCESS / OSIX_FAILURE                       
 ****************************************************************************/
PRIVATE INT4
FmMainTaskInit (VOID)
{

    OsixTskIdSelf (&(gFmGlobalInfo.TaskId));

    /* FM Task Q */
    if (OsixQueCrt ((UINT1 *) FM_QUEUE_NAME, OSIX_MAX_Q_MSG_LEN,
                    FM_QUEUE_DEPTH, &(gFmGlobalInfo.QId)) == OSIX_FAILURE)
    {
        FM_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC | FM_CRITICAL_TRC,
                 "FmMainTaskInit: FM Queue creation FAILED !!!\n"));
        return OSIX_FAILURE;
    }

    /* Create Mutual exclusion semaphore */
    if (OsixSemCrt ((UINT1 *) FM_SEM_NAME, &(gFmGlobalInfo.FmSemId))
        != OSIX_SUCCESS)
    {
        FM_TRC ((INIT_SHUT_TRC | FM_CRITICAL_TRC | ALL_FAILURE_TRC,
                 "FmMainTaskInit: Mutual exclusion semaphore creation "
                 "failed\r\n"));
        return OSIX_FAILURE;
    }

    /* Semaphore is by default created with initial value 0. So GiveSem
     * is called before using the semaphore. */
    OsixSemGive (gFmGlobalInfo.FmSemId);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : FmMainHandleInitFailure 
 *                                                                          
 *    DESCRIPTION      : Function called when the initialization of FM
 *                       fails at any point during initialization.  
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
FmMainHandleInitFailure (VOID)
{
    FM_TRC_FN_ENTRY ();

    FmMainMemClear ();

    /* Delete Q */
    if (gFmGlobalInfo.QId != 0)
    {
        OsixQueDel (gFmGlobalInfo.QId);
    }

    /* Delete Semaphore */
    if (gFmGlobalInfo.FmSemId != 0)
    {
        OsixSemDel (gFmGlobalInfo.FmSemId);
    }

    FM_TRC_FN_EXIT ();

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : FmMainMemClear 
 *
 *    DESCRIPTION      : Function that dequeues task queue and deletes 
 *                       the mempool and buddy
 *
 *    INPUT            : None 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None 
 *
 ****************************************************************************/
PRIVATE VOID
FmMainMemClear (VOID)
{
    tFmQMsg            *pQMsg = NULL;

    FM_TRC_FN_ENTRY ();

    if (gFmGlobalInfo.QId != 0)
    {
        /* Dequeue all messages */
        while (OsixQueRecv (gFmGlobalInfo.QId, (UINT1 *) &pQMsg,
                            OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
        {
            if (pQMsg == NULL)
            {
                continue;
            }

            if (MemReleaseMemBlock (gFmGlobalInfo.QMsgPoolId, (UINT1 *) pQMsg)
                == MEM_FAILURE)
            {
                FM_TRC ((BUFFER_TRC | ALL_FAILURE_TRC,
                         "QMsg Free Mem" " Block FAILED !!!\n"));
            }
        }
    }

    if (gFmGlobalInfo.QMsgPoolId != 0)
    {
        if (MemDeleteMemPool (gFmGlobalInfo.QMsgPoolId) == MEM_FAILURE)
        {
            FM_TRC ((BUFFER_TRC | ALL_FAILURE_TRC,
                     "QMsg MemPool Delete FAILED\n"));
        }

        gFmGlobalInfo.QMsgPoolId = 0;
    }

    if (gFmGlobalInfo.i4ExtDataBuddyId != -1)
    {
        MemBuddyDestroy ((UINT1) gFmGlobalInfo.i4ExtDataBuddyId);
        gFmGlobalInfo.i4ExtDataBuddyId = -1;
    }
    if (gFmGlobalInfo.i4VarRetrievalBuddyId != -1)
    {
        MemBuddyDestroy ((UINT1) gFmGlobalInfo.i4VarRetrievalBuddyId);
        gFmGlobalInfo.i4VarRetrievalBuddyId = -1;
    }

    FM_TRC_FN_EXIT ();
    return;
}

/***************************************************************************
 *     FUNCTION NAME    :  FmMainStart
 *                                                                           
 *     DESCRIPTION      :  This function initializes the global structure 
 *                                                                           
 *     INPUT            :  None. 
 *                                                                           
 *     OUTPUT           :  None.                                             
 *                                                                           
 *     RETURNS          :  OSIX_SUCCESS / OSIX_FAILURE                       
 ****************************************************************************/
INT4
FmMainStart (VOID)
{
    FM_TRC_FN_ENTRY ();

    /* Assign Trace Option */
    gFmGlobalInfo.u4TraceOption = FM_CRITICAL_TRC;

    /* Create Mempool and Buddy memory  */
    if (FmMainMemInit () == OSIX_FAILURE)
    {
        FM_TRC ((FM_CRITICAL_TRC |
                 BUFFER_TRC | ALL_FAILURE_TRC,
                 "Memory Initialization failed\n"));
        return OSIX_FAILURE;
    }

    if (FmTimerInit () == OSIX_FAILURE)
    {
        FM_TRC ((FM_CRITICAL_TRC | ALL_FAILURE_TRC,
                 "Timer Initialization failed\n"));
        return OSIX_FAILURE;
    }

    gFmGlobalInfo.u1SystemCtrlStatus = FM_START;

    if (FmIfCreateAllPorts () == OSIX_FAILURE)
    {
        FM_TRC ((BUFFER_TRC | FM_CRITICAL_TRC |
                 ALL_FAILURE_TRC, "FmIfCreateAllPorts failed\n"));
        return OSIX_FAILURE;
    }

    FM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***************************************************************************
 *     FUNCTION NAME    :  FmMainMemInit 
 *                                                                           
 *     DESCRIPTION      :  This function creates mempools and creates buddy 
 *                         memory
 *
 *     INPUT            :  None. 
 *                                                                           
 *     OUTPUT           :  None.                                             
 *                                                                           
 *     RETURNS          :  OSIX_SUCCESS / OSIX_FAILURE                       
 ****************************************************************************/
PRIVATE INT4
FmMainMemInit (VOID)
{
    MEMSET (&gFsEmFmSizingInfo, 0, sizeof (tFsModSizingInfo));
    MEMCPY (gFsEmFmSizingInfo.ModName, "EOAMFM", STRLEN ("EOAMFM"));
    gFsEmFmSizingInfo.u4ModMemPreAllocated = 0;
    gFsEmFmSizingInfo.ModSizingParams = gFsEmFmSizingParams;

    FsUtlSzCalculateModulePreAllocatedMemory (&gFsEmFmSizingInfo);
    /* Q Msg Mempool */
    if (MemCreateMemPool
        (FM_QMSG_MEMBLK_SIZE,
         gFsEmFmSizingParams[FM_Q_MSG_SIZING_ID].u4PreAllocatedUnits,
         MEM_HEAP_MEMORY_TYPE, &(gFmGlobalInfo.QMsgPoolId)) == MEM_FAILURE)
    {
        FM_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC,
                 "QMsg Mempool creation FAILED !!!\n"));
        return (OSIX_FAILURE);
    }

    /* Create Memory Buddy for storing Org specific data */
    if ((gFmGlobalInfo.i4ExtDataBuddyId =
         MemBuddyCreate
         (FM_MAX_DATA_BLOCK_SIZE, FM_MIN_DATA_BLOCK_SIZE,
          gFsEmFmSizingParams[FM_EXT_BUDDY_SIZING_ID].u4PreAllocatedUnits,
          BUDDY_CONT_BUF)) == BUDDY_FAILURE)
    {
        FM_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC,
                 "MemBuddyCreate failed for Org spec data\n"));
        return OSIX_FAILURE;
    }

    /* Create Memory Buddy for storing the variable requests and responses */
    if ((gFmGlobalInfo.i4VarRetrievalBuddyId =
         MemBuddyCreate
         (FM_MAX_VAR_BLOCK_SIZE, FM_MIN_VAR_BLOCK_SIZE,
          gFsEmFmSizingParams[FM_VAR_RETR_BUDDY_SIZING_ID].u4PreAllocatedUnits,
          BUDDY_NON_CONT_BUF)) == BUDDY_FAILURE)
    {
        FM_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC,
                 "MemBuddyCreate failed for Var Resp\n"));
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 *     FUNCTION NAME    :  FmMainShutDown 
 *                                                                           
 *     DESCRIPTION      :  This function frees all allocated memory concerned 
 *                         with the Fault Management Module
 *
 *     INPUT            :  None. 
 *                                                                           
 *     OUTPUT           :  None.                                             
 *                                                                           
 *     RETURNS          :  OSIX_SUCCESS / OSIX_FAILURE                       
 ****************************************************************************/
INT4
FmMainShutDown (VOID)
{
    UINT4               u4PortIndex = 0;

    FM_TRC_FN_ENTRY ();
    FmMainDisable ();
    FmTimerDeInit ();
    for (u4PortIndex = FM_MIN_PORTS; u4PortIndex <= FM_MAX_PORTS; u4PortIndex++)
    {
        if (FM_IF_ENTRY (u4PortIndex).FmValidEntry == FM_INVALID_ENTRY)
        {
            continue;
        }
        FmIfClearInfo (u4PortIndex);
    }

    FmMainMemClear ();

    gFmGlobalInfo.u1SystemCtrlStatus = FM_SHUTDOWN;
    FM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***************************************************************************
 *     FUNCTION NAME    :  FmMainEnable 
 *                                                                           
 *     DESCRIPTION      :  This function performs all registrations required
 *                         for the functioning of FM.
 *                                                                           
 *     INPUT            :  None. 
 *                                                                           
 *     OUTPUT           :  None.                                             
 *                                                                           
 *     RETURNS          :  OSIX_SUCCESS / OSIX_FAILURE                       
 ****************************************************************************/
INT4
FmMainEnable (VOID)
{
    tEoamRegParams      EoamReg;
    INT4                i4SysLogId;

    FM_TRC_FN_ENTRY ();

    i4SysLogId = SYS_LOG_REGISTER ((CONST UINT1 *) "EMFM", SYSLOG_ALERT_LEVEL);
    if (i4SysLogId <= 0)
    {
        FM_TRC ((INIT_SHUT_TRC | FM_CRITICAL_TRC |
                 ALL_FAILURE_TRC, "Registration with Syslog FAILED\n"));
        return OSIX_FAILURE;
    }
    gFmGlobalInfo.u4SysLogId = (UINT4) i4SysLogId;

#ifdef NPAPI_WANTED
    EoamfmFmNpRegisterForFailureIndications ();
#endif

    gFmGlobalInfo.u1ModuleStatus = FM_ENABLED;

    /* Registration with EOAM should be done only after enabling
     * Fault Management module. During registration EOAM will
     * update the current loopback status only if FM is enabled */
    MEMSET (&EoamReg, 0, sizeof (tEoamRegParams));
    EoamReg.u4EntId = EOAM_FM_APPL_ID;
    EoamReg.pNotifyCbFunc = FmApiCallbackFromEoam;
    EoamReg.u4Bitmap = FM_EOAM_REG_EVENTS;

    if (EoamApiRegisterModules (&EoamReg) == OSIX_FAILURE)
    {
        FM_TRC ((FM_CRITICAL_TRC | ALL_FAILURE_TRC,
                 "Registration with EOAM FAILED\n"));
        return OSIX_FAILURE;
    }

    FM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***************************************************************************
 *     FUNCTION NAME    :  FmMainDisable 
 *                                                                           
 *     DESCRIPTION      :  This function deregisters with all the registered
 *                         modules
 *                                                                           
 *     INPUT            :  None. 
 *                                                                           
 *     OUTPUT           :  None.                                             
 *                                                                           
 *     RETURNS          :  OSIX_SUCCESS / OSIX_FAILURE                       
 ****************************************************************************/
INT4
FmMainDisable (VOID)
{
    tFmPortEntry       *pPortEntry = NULL;
    UINT4               u4PortIndex = 0;

    FM_TRC_FN_ENTRY ();

    SYS_LOG_DEREGISTER (gFmGlobalInfo.u4SysLogId);

    for (u4PortIndex = FM_MIN_PORTS; u4PortIndex <= FM_MAX_PORTS; u4PortIndex++)
    {

        if (FM_IF_ENTRY (u4PortIndex).FmValidEntry == FM_INVALID_ENTRY)
        {
            continue;
        }
        pPortEntry = &(gFmGlobalInfo.aFmPortEntry[(UINT4) u4PortIndex - 1]);
        if (pPortEntry->LBTestInfo.u1LoopbackStatus == FM_REMOTE_LB)
        {
            if (pPortEntry->FmLBTmrFlag == FM_LB_TMR_RUNNING)
            {
                TmrStop (gFmGlobalInfo.FmTmrListId,
                         &(pPortEntry->LBTestAppTmr));
                pPortEntry->FmLBTmrFlag = FM_LB_TMR_NOT_RUNNING;
            }
            pPortEntry->LBTestInfo.u1LoopbackStatus = FM_NO_LB;
            OsixGetSysTime (&(pPortEntry->LBTestInfo.CurrSessionStats.EndTime));
            MEMCPY (&(pPortEntry->PrevSessionStats),
                    &(pPortEntry->LBTestInfo.CurrSessionStats),
                    sizeof (tFmLBTestStats));
            MEMSET (&(pPortEntry->LBTestInfo.CurrSessionStats), 0,
                    sizeof (tFmLBTestStats));
        }
    }

    if (EoamApiDeRegisterModule (EOAM_FM_APPL_ID) == OSIX_FAILURE)
    {
        FM_TRC ((FM_CRITICAL_TRC | ALL_FAILURE_TRC,
                 "Deregistration with EOAM FAILED\n"));
    }
    gFmGlobalInfo.u1ModuleStatus = FM_DISABLED;

    FM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : FmLock
 *
 *    DESCRIPTION      : Function to take the mutual exclusion protocol
 *                       semaphore.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 *
 ****************************************************************************/
INT4
FmLock (VOID)
{
    FM_TRC_FN_ENTRY ();
    if (gFmGlobalInfo.FmSemId != 0)
    {
        if (OsixSemTake (gFmGlobalInfo.FmSemId) == OSIX_FAILURE)
        {
            FM_TRC ((OS_RESOURCE_TRC, "FmLock: Take sem FAILED !!!\n"));
            return (SNMP_FAILURE);
        }
    }
    FM_TRC_FN_EXIT ();
    return (SNMP_SUCCESS);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : FmUnLock
 *
 *    DESCRIPTION      : Function to release the mutual exclusion protocol
 *                       semaphore.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 *
 ****************************************************************************/
INT4
FmUnLock (VOID)
{
    FM_TRC_FN_ENTRY ();
    if (gFmGlobalInfo.FmSemId != 0)
    {
        OsixSemGive (gFmGlobalInfo.FmSemId);
    }
    FM_TRC_FN_EXIT ();
    return (SNMP_SUCCESS);
}

#endif /* FMMAIN_C */
/*-----------------------------------------------------------------------*/
/*                       End of the file  emfmmain.c                     */
/*-----------------------------------------------------------------------*/
