/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emfmif.c,v 1.2 2010/05/19 07:12:19 prabuc Exp $
 * 
 * Description: This file contains procedures for
 *              - creating/deleting interface structures. 
 *****************************************************************************/
#include "emfminc.h"

PRIVATE VOID        FmIfInit (tFmPortEntry * pPortEntry);

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : FmIfCreateAllPorts
 *                                                                          
 *    DESCRIPTION      : This function creates all valid ports in FM module. 
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
FmIfCreateAllPorts (VOID)
{
    UINT4               u4PortIndex = 0;

    FM_TRC_FN_ENTRY ();

    for (u4PortIndex = FM_MIN_PORTS; u4PortIndex <= FM_MAX_PORTS; u4PortIndex++)
    {
        if (CfaValidateIfIndex (u4PortIndex) == CFA_FAILURE)
        {
            continue;
        }
        if (FmIfCreate (u4PortIndex) == OSIX_FAILURE)
        {
            FM_TRC ((CONTROL_PLANE_TRC | FM_CRITICAL_TRC | ALL_FAILURE_TRC,
                     "Port %u: FmIfCreate failed\n", u4PortIndex));
            return OSIX_FAILURE;
        }
    }

    FM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***************************************************************************
 *     FUNCTION NAME    :  FmIfCreate  
 *                                                                           
 *     DESCRIPTION      :  This function allocates memory for port entry
 *                         and initializes the port entry 
 *                         structure
 *                                                                           
 *     INPUT            :  u4PortIndex - Interface Index
 *                                                                           
 *     OUTPUT           :  None.                                             
 *                                                                           
 *     RETURNS          :  OSIX_SUCCESS / OSIX_FAILURE                       
 ****************************************************************************/
PUBLIC INT4
FmIfCreate (UINT4 u4PortIndex)
{
    tFmPortEntry       *pPortEntry = NULL;

    FM_TRC_FN_ENTRY ();

    if (gFmGlobalInfo.u1SystemCtrlStatus == FM_SHUTDOWN)
    {
        FM_TRC ((INIT_SHUT_TRC, "FM module is shut down\n"));
        return OSIX_FAILURE;
    }

    /* Initialize the port entry from the port table */
    if (u4PortIndex != 0)
    {
        pPortEntry = &(gFmGlobalInfo.aFmPortEntry[(UINT4) u4PortIndex - 1]);

        MEMSET (pPortEntry, 0, sizeof (tFmPortEntry));
        gFmGlobalInfo.aFmPortEntry[(UINT4) u4PortIndex - 1].u4Port =
            u4PortIndex;

        FmIfInit (pPortEntry);
        /* This buddy memory is allocated to store values of 10 descriptors 
         * (default value). If the maximum number of descriptors is configured
         * to be other than the default value, this memory is freed and a new
         * buddy is allocated according to the configured value. This buddy
         * memory will not be freed everytime the variable request data has
         * been posted to EOAM task */
        if ((pPortEntry->VarRetrievalInfo.pu1VarReqData =
             FM_RESP_BUDDY_ALLOC (FM_DEFAULT_NUM_DESCRIPTORS *
                                  FM_DESCRIPTOR_LENGTH)) == NULL)
        {
            FM_TRC ((CONTROL_PLANE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                     "Port %u: No Memory for pu1VarReqData\n",
                     pPortEntry->u4Port));
            return OSIX_FAILURE;
        }
        MEMSET (pPortEntry->VarRetrievalInfo.pu1VarReqData, 0,
                (FM_DEFAULT_NUM_DESCRIPTORS * FM_DESCRIPTOR_LENGTH));
        pPortEntry = NULL;
    }

    FM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : FmIfDelete
 *
 *    DESCRIPTION      : This function releases the memory and 
 *                       clears the particular port entry information.
 *                       
 *    INPUT            : u4Port - Port Index
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
FmIfDelete (UINT4 u4Port)
{
    FM_TRC_FN_ENTRY ();

    if (gFmGlobalInfo.u1SystemCtrlStatus == FM_SHUTDOWN)
    {
        FM_TRC ((INIT_SHUT_TRC, "FM module is shut down\n"));
        return OSIX_FAILURE;
    }

    /* Delete the port entry from the port table */
    if (u4Port != 0)
    {
        if (FM_IF_ENTRY (u4Port).FmLBTmrFlag == FM_LB_TMR_RUNNING)
        {
            TmrStop (gFmGlobalInfo.FmTmrListId,
                     &(FM_IF_ENTRY (u4Port).LBTestAppTmr));

            FM_IF_ENTRY (u4Port).FmLBTmrFlag = FM_LB_TMR_NOT_RUNNING;
        }
        FmIfClearInfo (u4Port);
    }

    FM_TRC_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***************************************************************************
 *     FUNCTION NAME    :  FmIfInit  
 *                                                                           
 *     DESCRIPTION      :  This function initializes the port entry
 *                                                                           
 *     INPUT            :  pPortEntry - pointer to the port entry
 *                                                                           
 *     OUTPUT           :  None.                                             
 *                                                                           
 *     RETURNS          :  None.                       
 ****************************************************************************/
PRIVATE VOID
FmIfInit (tFmPortEntry * pPortEntry)
{
    UINT4               u4TestPattern = FM_DEFAULT_TEST_PATTERN;

    FM_TRC_FN_ENTRY ();

    pPortEntry->EventInfo.SymPeriodAction = FM_ACTION_WARNING;
    pPortEntry->EventInfo.FrameAction = FM_ACTION_WARNING;
    pPortEntry->EventInfo.FramePeriodAction = FM_ACTION_WARNING;
    pPortEntry->EventInfo.FrameSecSummAction = FM_ACTION_WARNING;
    pPortEntry->EventInfo.CriticalEventAction = FM_ACTION_WARNING;
    pPortEntry->EventInfo.DyingGaspAction = FM_ACTION_WARNING;
    pPortEntry->EventInfo.LinkFaultAction = FM_ACTION_WARNING;

    pPortEntry->LBTestInfo.u4TestPktSize = FM_DEFAULT_TEST_PKT_SIZE;
    pPortEntry->LBTestInfo.u4TestPktCount = FM_DEFAULT_TEST_PKT_COUNT;
    pPortEntry->LBTestInfo.u4TestWaitTime = FM_DEFAULT_TEST_WAIT_TIME;
    pPortEntry->LBTestInfo.u1LoopbackStatus = FM_NO_LB;
    pPortEntry->LBTestInfo.u1TestCommand = FM_NO_LB_TEST;
    pPortEntry->LBTestInfo.u1TestStatus = FM_NOT_INITIATED;
    MEMCPY (pPortEntry->LBTestInfo.au1TestPattern, &u4TestPattern,
            FM_LB_TEST_PATTERN_LENGTH);
    pPortEntry->VarRetrievalInfo.u2MaxDescriptors = FM_DEFAULT_NUM_DESCRIPTORS;
    pPortEntry->VarRetrievalInfo.RespClearStatus = FM_RESP_NOT_CLEARED;
    pPortEntry->FmLBTmrFlag = FM_LB_TMR_NOT_RUNNING;
    pPortEntry->FmValidEntry = FM_VALID_ENTRY;
    pPortEntry->u1EventFlag = FM_INVALID_ENTRY;
    FM_TRC_FN_EXIT ();
    return;
}

/***************************************************************************
 *     FUNCTION NAME    :  FmIfClearInfo 
 *                                                                           
 *     DESCRIPTION      :  This function is used to clear the port
 *                         info in the global structure. This is called
 *                         whenever the port is deleted.
 *
 *     INPUT            :  u4PortIndex - Interface Index. 
 *                                                                           
 *     OUTPUT           :  None.                                             
 *                                                                           
 *     RETURNS          :  None.                       
 ****************************************************************************/
PUBLIC VOID
FmIfClearInfo (UINT4 u4PortIndex)
{
    tFmPortEntry       *pPortEntry = NULL;
    UINT2               u2Count = 0;

    FM_TRC_FN_ENTRY ();
    pPortEntry = &(gFmGlobalInfo.aFmPortEntry[(UINT4) u4PortIndex - 1]);

    if (pPortEntry->VarRetrievalInfo.pu1VarReqData != NULL)
    {
        FM_RESP_BUDDY_FREE (pPortEntry->VarRetrievalInfo.pu1VarReqData);
        pPortEntry->VarRetrievalInfo.pu1VarReqData = NULL;
    }
    for (u2Count = 0; u2Count < FM_MAX_RESPONSES; u2Count++)
    {
        if (pPortEntry->VarResp[u2Count].pu1Data != NULL)
        {
            FM_RESP_BUDDY_FREE (pPortEntry->VarResp[u2Count].pu1Data);
            pPortEntry->VarResp[u2Count].pu1Data = NULL;
            pPortEntry->VarResp[u2Count].u4DataLen = 0;
            pPortEntry->au2VarRespChunks[u2Count] = 0;
        }
    }
    pPortEntry->FmLBTmrFlag = FM_LB_TMR_NOT_RUNNING;
    pPortEntry->FmValidEntry = FM_INVALID_ENTRY;
    MEMSET (pPortEntry, 0, sizeof (tFmPortEntry));
    FM_TRC_FN_EXIT ();
    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  emfmif.c                       */
/*-----------------------------------------------------------------------*/
