/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: emfmutl.c,v 1.2 2011/11/21 13:40:11 siva Exp $
 * 
 * Description: This file contains all the utility routines of Fault
 *              Management. 
 *****************************************************************************/
#include "emfminc.h"
/************************************************************************
 *  Function Name   : FmUtilExtractDescriptor 
 *  
 *  Description     : Gets Branch and Leaf values from string 
 *  
 *  Input           : ppu1String - String Pointer
 *  
 *  Output          : pu1Branch - pointer to the branch value 
 *                    pu2Leaf   - pointer to the leaf value
 *  
 *  Returns         : OSIX_SUCCESS / OSIX_FAILURE 
 ************************************************************************/
PUBLIC INT4
FmUtilExtractDescriptor (UINT1 **ppu1String, UINT1 *pu1Branch, UINT2 *pu2Leaf)
{
    UINT4               u4Count = 0;
    UINT4               u4DescriptorLen = 0;
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT1              *pu1BasePtr = NULL;
    UINT1               au1Value[2] = { 0 };

    FSAP_ASSERT (*ppu1String != NULL);
    pu1BasePtr = *ppu1String;
    *pu2Leaf = 0;
    *pu1Branch = 0;
    if (FmUtilGetDescriptorLength (&(*ppu1String), &u4DescriptorLen) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if ((u4DescriptorLen > FM_DESCRIPTOR_STRING_MAX_LENGTH) ||
        (u4DescriptorLen < FM_DESCRIPTOR_STRING_MIN_LENGTH))
    {
        return OSIX_FAILURE;
    }
    for (u4Count = 1; u4Count <= u4DescriptorLen; u4Count++)
    {
        if (u4Count == 1)
        {
            MEMCPY (pu1Branch, pu1BasePtr, 1);
            *pu1Branch = (UINT1) ATOI (pu1Branch);
        }
        else
        {
            MEMCPY (&au1Value[0], pu1BasePtr, 1);
            au1Value[0] = (UINT1) ATOI (&au1Value[0]);
            *pu2Leaf = (UINT2) ((*pu2Leaf * 10) + au1Value[0]);
        }
        pu1BasePtr++;
    }
    return i4RetVal;
}

/************************************************************************
 *  Function Name   : FmUtilCopyDescriptorsFromStr 
 *  
 *  Description     : Convert string to OID Type
 *
 *  Input           : pu1String - String to be converted into oid
 *
 *  Output          : pu1ReqData - Pointer to memory where the 
 *                                 descriptors are stored
 *
 *  Returns         : OSIX_SUCCESS or OSIX_FAILURE
 ************************************************************************/
PUBLIC INT4
FmUtilCopyDescriptorsFromStr (UINT1 *pu1ReqData, UINT1 *pu1String)
{
    UINT4               u4Count = 0;
    UINT4               u4Descriptors = 0;
    UINT2               u2Leaf = 0;
    UINT1               u1Branch = 0;
    INT4                i4RetVal = OSIX_SUCCESS;

    u4Descriptors = FmUtilGetDescriptorCntFromStr (pu1String);
    for (u4Count = 0; u4Count < u4Descriptors; u4Count++)
    {
        if (ISDIGIT (*pu1String) != 0)
        {
            if (FmUtilExtractDescriptor (&pu1String, &u1Branch, &u2Leaf) ==
                OSIX_FAILURE)
            {
                i4RetVal = OSIX_FAILURE;
                break;
            }
            *pu1ReqData = u1Branch;
            pu1ReqData++;
            MEMCPY (pu1ReqData, &u2Leaf, sizeof (UINT2));
            pu1ReqData += 2;
        }
        else
        {
            i4RetVal = OSIX_FAILURE;
            break;
        }
        if (*pu1String == ':')
        {
            pu1String++;
        }
        else if (*pu1String != '\0')
        {
            i4RetVal = OSIX_FAILURE;
            break;
        }
    }
    return i4RetVal;
}

/************************************************************************
 *  Function Name   : FmUtilGetDescriptorCntFromStr
 *  
 *  Description     : This utility gets the number of descriptors from
 *                    the string
 *
 *  Input           : pu1String - pointer to String from which descriptor 
 *                                has to be obtained
 *
 *  Output          : NONE 
 *
 *  Returns         : OSIX_SUCCESS or OSIX_FAILURE
 ************************************************************************/
PUBLIC UINT4
FmUtilGetDescriptorCntFromStr (UINT1 *pu1String)
{
    UINT4               u4Count = 0;
    UINT4               u4Delimitors = 0;

    for (u4Count = 0; pu1String[u4Count] != '\0'; u4Count++)
    {
        if (pu1String[u4Count] == ':')
        {
            u4Delimitors++;
        }
    }
    return (u4Delimitors + 1);
}

/************************************************************************
 *  Function Name   : FmUtilGetDescriptorCntFromStr
 *  
 *  Description     : This utility function gets the descriptor length
 *                    from the string 
 *
 *  Input           : pu1String - String for which length length
 *                                has to be found
 *
 *  Output          : pu4Length - pointer to the Length of the descriptor 
 *
 *  Returns         : OSIX_SUCCESS or OSIX_FAILURE
 ************************************************************************/
PUBLIC INT4
FmUtilGetDescriptorLength (UINT1 **ppu1String, UINT4 *pu4Length)
{
    UINT4               u4Count = 0;
    for (u4Count = 1; ((**ppu1String != ':') && (**ppu1String != '\0'));
         u4Count++)
    {
        if (ISDIGIT (**ppu1String) == 0)
        {
            return OSIX_FAILURE;
        }
        (*ppu1String)++;
        *pu4Length = u4Count;
    }
    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  emfmutl.c                     */
/*-----------------------------------------------------------------------*/
