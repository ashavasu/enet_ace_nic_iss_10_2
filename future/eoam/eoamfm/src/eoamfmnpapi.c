/**********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: eoamfmnpapi.c,v 1.2 2012/12/13 14:36:15 siva Exp $
 *
 * Description: All  Network Processor API Function calls are done here.
 *
 ***************************************************************************/

#ifndef _EOAM_FM_NPAPI_C_
#define _EOAM_FM_NPAPI_C_

#include "emfminc.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : EoamfmFmNpRegisterForFailureIndications                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FmNpRegisterForFailureIndications
 *                                                                          
 *    Input(s)            : Arguments of FmNpRegisterForFailureIndications
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EoamfmFmNpRegisterForFailureIndications ()
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_EOAMFM_MOD,    /* Module ID */
                         FM_NP_REGISTER_FOR_FAILURE_INDICATIONS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : EoamfmNpFmFailureIndicationCallbackFunc                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes NpFmFailureIndicationCallbackFunc
 *                                                                          
 *    Input(s)            : Arguments of NpFmFailureIndicationCallbackFunc
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
EoamfmNpFmFailureIndicationCallbackFunc (UINT4 u4IfIndex, UINT1 u1EventType,
                                         UINT1 u1Status)
{
    tFsHwNp             FsHwNp;
    tEoamfmNpModInfo   *pEoamfmNpModInfo = NULL;
    tEoamfmNpWrNpFmFailureIndicationCallbackFunc *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_EOAMFM_MOD,    /* Module ID */
                         NP_FM_FAILURE_INDICATION_CALLBACK_FUNC,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pEoamfmNpModInfo = &(FsHwNp.EoamfmNpModInfo);
    pEntry = &pEoamfmNpModInfo->EoamfmNpNpFmFailureIndicationCallbackFunc;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1EventType = u1EventType;
    pEntry->u1Status = u1Status;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

#endif /* _EOAM_FM_NPAPI_C_ */
